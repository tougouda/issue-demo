#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');


use strict;
use lib '../../inc', '../../lib';

# Traitement des variables d'environnement
use LOC::Env;

use Archive::Zip::SimpleZip;
use File::Basename;
use File::Copy;
use Time::Local;

# Modules internes
use LOC::Globals;
use LOC::Util;


# Constants: Récupération des informations
# COMMONFILES_PATH    - Répertoire des fichiers communs
# ARCHIVE_NAME        - Nom des archives
# ARCHIVE_METHOD      - Méthode de compression
# ARCHIVE_COMPRESSION - Taux de compression
# ARCHIVE_DELAY       - Délai au bout du quel les fichiers sont déplacés dans l'espace d'archivage (en mois)
# ARCHIVEFILES_PATH   - Chemin vers l'espace d'archivage
use constant
{
    COMMONFILES_PATH    => &LOC::Globals::get('commonFilesPath'),
    ARCHIVE_NAME        => 'common_files_location_%04d-%02d.zip',
    ARCHIVE_METHOD      => Archive::Zip::SimpleZip::ZIP_CM_LZMA,
    ARCHIVE_COMPRESSION => 0,
    ARCHIVE_DELAY       => 2,
    ARCHIVEFILES_PATH   => $ENV{'FILE_HOME'} . '/common_files/location'
};


# Répertoires à archiver
my @tabDirectories = ('locale', 'logs', 'subRent', 'transport');


# Statut de retour
my $status = 0;


# Date de référence (premier jour du mois en cours)
my @localtime = localtime();
my $refdate = sprintf('%04d-%02d-%02d', $localtime[5] + 1900, $localtime[4] + 1, 1);

# Mois de l'archive
my $month = $localtime[4];
my $year = $localtime[5] + 1900;
if ($month == 0)
{
    $month = 12;
    $year--;
}

my $output = '';

# Recherche des fichiers à archiver
my @tabFiles = ();
foreach my $directory (@tabDirectories)
{
    &getFiles(COMMONFILES_PATH . '/' . $directory, \@tabFiles, $refdate);
}

# Création de l'archive
if (@tabFiles)
{
    &createArchive(\@tabFiles, $month, $year);
}

# Suppression des fichiers archivés
my $nbErrors = &removeFiles(\@tabFiles);
if ($nbErrors)
{
    $output .= sprintf('%d erreurs de suppression de fichiers', $nbErrors) . "\n";
    $status = ($status || $nbErrors);
}

# Transfert des archives de plus d'un mois (EXP)
if (&LOC::Globals::getEnv() eq 'exp')
{
    my $nbErrors = &transferArchives();
    if ($nbErrors)
    {
        $output .= sprintf('%d erreurs de transfert d\'archives', $nbErrors) . "\n";
        $status = ($status || $nbErrors);
    }
}
# Suppression des archives de plus d'un mois (autres environnements)
else
{
    my $nbErrors = &removeArchives();
    if ($nbErrors)
    {
        $output .= sprintf('%d erreurs de suppression d\'archives', $nbErrors) . "\n";
        $status = ($status || $nbErrors);
    }
}


print "\n\n" . $output;


# Function: getFiles
# Alimente la liste des fichiers d'un répertoire dont la date de modification est antérieure à la date de référence
#
# Parameters:
# string   $directory - Chemin vers le répertoire
# arrayref $output    - Liste des fichiers
# string   $date      - Date de référence
#
# Returns:
# void
sub getFiles
{
    my ($directory, $output, $date) = @_;

    unless (defined $date)
    {
        my @localtime = localtime();
        $date = sprintf('%04d-%02d-%02d', $localtime[5] + 1900, $localtime[4] + 1, $localtime[3]);
    }
    my ($year, $mon, $mday) = split(/-/, $date);
    my $reftime = timelocal(0, 0, 0, $mday, $mon - 1, $year);

    opendir(my $dh, $directory);
    while (my $file = readdir($dh))
    {
        if ($file ne '.' && $file ne '..')
        {
            if (-d $directory . '/' . $file)
            {
                &getFiles($directory . '/' . $file, $output, $date);
            }
            else
            {
                my $mtime = (stat($directory . '/' . $file))[9];
                if ($mtime < $reftime)
                {
                    push(@$output, $directory . '/' . $file);
                }
            }
        }
    }
    closedir($dh);
}


# Function: createArchive
# Crée l'archive
#
# Parameters:
# arrayref $tabFiles - Liste des fichiers à ajouter à l'archive
# int      $month    - Mois de l'archive
# int      $year     - Année de l'archive
#
# Returns:
# void
sub createArchive
{
    my ($tabFiles, $month, $year) = @_;

    my $comment = sprintf('Sauvegarde des fichiers communs de la Gestion des locations du mois de %02d/%04d', $month, $year);

    my $tabParams = {
            'ZipComment' => $comment,           # Commentaire
            'Method'     => ARCHIVE_METHOD,     # Méthode LZMA
            'Preset'     => ARCHIVE_COMPRESSION # Taux de compression
        };

    my $archivename = sprintf(ARCHIVE_NAME, $year, $month);

    my $zip = new Archive::Zip::SimpleZip(COMMONFILES_PATH . '/' . $archivename, $tabParams);


    # Ajout des fichiers à l'archive
    foreach my $file (@tabFiles)
    {
        my $commonFilesPath = COMMONFILES_PATH;
        $file =~ /^$commonFilesPath(.+)$/;
        print 'Ajout de ' . $1 . ' ';
        $zip->add($file, {'Name' => $1});
        print '[OK]' . "\n";
    }

    $zip->close();
}


# Function: removeFiles
# Supprime les fichiers et les répertoires qui les contiennent s'ils sont vides
#
# Parameters:
# arrayref $tabFiles - Liste des fichiers à supprimer
#
# Returns:
# int - Nombre d'erreurs de suppression
sub removeFiles
{
    my ($tabFiles) = @_;

    my $nbErrors = 0;
    my @tabDirectories = ();

    foreach my $file (@tabFiles)
    {
        # Suppression du fichier
        print 'Suppression de ' . $file . ' ';
        if (unlink($file))
        {
            print '[OK]' . "\n";

            # Répertoire contenant
            my $dirname = &File::Basename::dirname($file);
            if (!LOC::Util::in_array($dirname, \@tabDirectories))
            {
                push(@tabDirectories, $dirname);
            }
        }
        else
        {
            print '[Échec]' . "\n";
        }
    }

    foreach my $directory (@tabDirectories)
    {
        # Suppression du répertoire s'il est vide
        if (&isDirectoryEmpty($directory))
        {
            print 'Suppression de ' . $directory . ' ';
            if (rmdir($directory))
            {
                print '[OK]' . "\n";
            }
            else
            {
                $nbErrors++;
                print '[Échec]' . "\n";
            }
        }
    }

    return $nbErrors;
}


# Function: isDirectoryEmpty
# Vérifie si un répertoire est vide
#
# Parameters:
# string $directory - Chemin vers le répertoire
#
# Returns:
# bool -
sub isDirectoryEmpty
{
    my ($directory) = @_;

    opendir(my $dh, $directory);
    my @tabFiles = grep { !/^\.{1,2}$/ } readdir($dh);
    closedir($dh);

    return (@tabFiles == 0) * 1;
}


# Function: transferArchives
# Transfère les vieilles archives vers l'espace de stockage
#
# Returns:
# int - Nombre d'erreurs de transfert
sub transferArchives
{
    my $pattern = &getArchiveNamePattern();
    my $refMonth = &getRefMonth();

    my $nbErrors = 0;

    opendir(my $dh, COMMONFILES_PATH);
    while (my $file = readdir($dh))
    {
        # On exclut . et ..
        # On ne prend que les fichiers ayant un nom correspondant à une archive souhaitée
        # On ne prend que les archives assez vieilles
        if (&isArchiveRemoveable($file, $pattern, $refMonth))
        {
            my $source = COMMONFILES_PATH . '/' . $file;
            if (!-d $source)
            {
                my $destination = ARCHIVEFILES_PATH . '/' . $file;
                my $counter = 1;
                while (-e $destination && $counter <= 50)
                {
                    my $tmpName = $file;
                    $tmpName =~ s/(_(\d+))?(\..+)$/_$counter$3/;

                    $destination = ARCHIVEFILES_PATH . '/' . $tmpName;
                    $counter++;
                }
                print 'Déplacement de ' . $source . ' vers ' . $destination . ' ';
                if (&File::Copy::move($source, $destination) && $counter <= 50)
                {
                    print '[OK]' . "\n";
                }
                else
                {
                    $nbErrors++;
                    print '[Échec]' . "\n";
                }
            }
        }
    }
    closedir($dh);

    return $nbErrors;
}


# Function: removeArchives
# Supprime les vieilles archives
#
# Returns:
# int - Nombre d'erreurs de suppression
sub removeArchives
{
    my $pattern = &getArchiveNamePattern();
    my $refMonth = &getRefMonth();

    my $nbErrors = 0;

    opendir(my $dh, COMMONFILES_PATH);
    while (my $file = readdir($dh))
    {
        # On exclut . et ..
        # On ne prend que les fichiers ayant un nom correspondant à une archive souhaitée
        # On ne prend que les archives assez vieilles
        if (&isArchiveRemoveable($file, $pattern, $refMonth))
        {
            my $source = COMMONFILES_PATH . '/' . $file;
            if (!-d $source)
            {
                print 'Suppression de ' . $source . ' ';
                if (unlink($source))
                {
                    print '[OK]' . "\n";
                }
                else
                {
                    $nbErrors++;
                    print '[Échec]' . "\n";
                }
            }
        }
    }
    closedir($dh);

    return $nbErrors;
}


# Function: getArchiveNamePattern
# Retourne le motif d'expression régulière correspondant au nom du fichier d'archive
#
# Returns:
# string -
sub getArchiveNamePattern
{
    my $pattern = ARCHIVE_NAME;
    $pattern =~ s/\./\\./g;
    $pattern =~ s/%.+?d-%.+?d/(.+?)/g;

    return $pattern;
}


# Function: getRefMonth
# Retourne le mois au delà duquel une archive est considérée comme vieille
#
# Returns:
# string -
sub getRefMonth
{
    my @localtime = localtime();
    my $year = $localtime[5] + 1900;
    my $month = $localtime[4] + 1 - ARCHIVE_DELAY;
    if ($month <= 0)
    {
        $year--;
        $month += 12;
    }

    return sprintf('%04d-%02d', $year, $month);
}


# Function: isArchiveRemoveable
# Vérifie si un fichier est une archive pouvant être supprimée
#
# Parameters:
# string $file     - Nom du fichier
# string $pattern  - Motif d'expression régulière correspondant au nom du fichier d'archive
# string $refMonth - Mois au delà duquel une archive est considérée comme vieille
#
# Returns:
# bool -
sub isArchiveRemoveable
{
    my ($file, $pattern, $refMonth) = @_;

    return ($file ne '.' && $file ne '..' && $file =~ /$pattern/ && $1 le $refMonth);
}



# Statut de retout
exit($status);