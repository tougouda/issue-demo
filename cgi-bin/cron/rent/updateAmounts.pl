#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../../inc', '../../lib';

# Traitement des variables d'environnement
use LOC::Env;

# Modules internes
use LOC::Agency;
use LOC::Contract::Rent;
use LOC::Globals;
use LOC::Log;
use LOC::Session;
use LOC::Tariff::Rent;
use LOC::Date;

# Modules externes
use Data::Dumper;
use SOAP::Lite;


our @tabCountries = ('FR', 'ES', 'PT', 'MA');


# Mise à jour des devis
#&updateEstimates();

# Mise à jour des contrats
&updateContracts();



# Function: updateEstimates
# Met à jour les devis
sub updateEstimates
{
    our @tabCountries;

    # Log
    my $logName = 'activation_Estimates';
    my $log = 'Début de la mise à jour des montants sur les devis ---------------------------------------------------------------------' . "\n";
    &LOC::Log::writeFileLog('updates', $log, $logName);

    foreach my $countryId (@tabCountries)
    {
        # Log
        $log = '## PAYS : ' . $countryId . "\n";
        &LOC::Log::writeFileLog('updates', $log, $logName);

        # Connexion à la base de données
        my $db = &LOC::Db::getConnection('location', $countryId);

        # Y'a-t-il une transaction en cours ?
        my $hasTransaction = $db->hasTransaction();

        # Démarre la transaction
        if (!$hasTransaction)
        {
            $db->beginTransaction();
        }



        # Récupération de la devise
        my $tabCountryInfos = &LOC::Country::getInfos($countryId);
        my $currencyId = $tabCountryInfos->{'currency.id'};



        # Récupération des devis à mettre à jour
        my $query = '
SELECT
    tbl_f_estimate.DEVISAUTO AS `id`,
    tbl_f_estimate.CLAUTO AS `customer.id`,
    tbl_f_estimate.DEVISDATECREATION AS `creationDate`
FROM DEVIS tbl_f_estimate';
        my $tabEstimates = $db->fetchAssoc($query, {}, 'id');

        foreach my $tabEstimate (values %$tabEstimates)
        {
            # Données
            my $id           = &LOC::Util::toNumber($tabEstimate->{'id'});
            my $customerId   = &LOC::Util::toNumber($tabEstimate->{'customer.id'});
            my $creationDate = $tabEstimate->{'creationDate'};

            # Trace console
            our $stdout = 'Calcul des montants du devis ' . $id;
            print $stdout . '          ' . "\r";

            my $query = '
SELECT
    tbl_f_estimateline.DETAILSDEVISAUTO AS `id`,
    tbl_f_estimateline.MOMAAUTO AS `model.id`,
    tbl_f_estimateline.DETAILSDEVISDUREE AS `duration`,
    tbl_f_estimateline.DETAILSDEVISQTE AS `quantity`,
    tbl_f_estimateline.DETAILSDEVISTYPEMONTANT AS `amountType`,
    tbl_f_estimateline.DETAILSDEVISPXJOURSFR AS `amount`,
    tbl_f_estimateline.DETAILSDEVISREMISE AS `standardReductionPercent`,
    tbl_f_estimateline.DETAILSDEVISREMISEEX AS `specialReductionPercent`
FROM DETAILSDEVIS tbl_f_estimateline
WHERE tbl_f_estimateline.DEVISAUTO = ' . $id;
            my $tabEstimateLines = $db->fetchAssoc($query, {}, 'id');

            foreach my $tabRow (values %$tabEstimateLines)
            {
                # Données
                my $lineId        = &LOC::Util::toNumber($tabRow->{'id'});
                my $modelId       = &LOC::Util::toNumber($tabRow->{'model.id'});
                my $type          = ($tabRow->{'amountType'} == 3 ? 'fixed' : ($tabRow->{'amountType'} == 2 ? 'monthly' : 'day'));
                my $duration      = &LOC::Util::toNumber($tabRow->{'duration'}) * ($type eq 'monthly' ? LOC::Date::AVERAGE_MONTH_WORKING_DAYS : 1);
                my $quantity      = &LOC::Util::toNumber($tabRow->{'quantity'});
                my $finalPrice    = &LOC::Util::toNumber($tabRow->{'amount'});
                my $stdRedPc      = &LOC::Util::toNumber($tabRow->{'standardReductionPercent'});
                my $specRedPc     = &LOC::Util::toNumber($tabRow->{'specialReductionPercent'});
                my $priceAfterStdRed;
                my $baseTariff;
                my $newStdRedPc;
                my $newSpecRedPc;
                my $newPriceAfterStdRed;
                my $newFinalPrice;

                # Récupération des tarifs du client
                my $tabCustomerTariffs = &LOC::Tariff::Rent::getCustomerTariffs($countryId, $customerId,
                                                                                {'model.id' => $modelId});

                # Calcul du tarif de base, de la remise standard et de la remise exceptionnelle
                my $tabRentTariffInfos = &calculateRentTariffInfos($tabCustomerTariffs, $duration, $stdRedPc, $specRedPc,
                                                                   $finalPrice);
                if ($tabRentTariffInfos == -1)
                {
                    next;
                }
                ($baseTariff, $newStdRedPc, $newPriceAfterStdRed, $newSpecRedPc, $newFinalPrice) = @$tabRentTariffInfos;

                # Calcul de la remise maximale
                my $newMaxRedPc = &calculateMaxReduction($tabCustomerTariffs, $duration, $newStdRedPc, $newSpecRedPc);



                # Log
#                if ($finalPrice != $newFinalPrice)
#                if ($specRedPc != $newSpecRedPc)
#                {
                    $log = $id . ' (ligne ' . $lineId . ') :' . "\n";
                    $log .= '  - tarif de base : => ' . $baseTariff . "\n";
                    $log .= '  - remise max. : => ' . $newMaxRedPc . "\n";
                    $log .= '  - remise std : ' . $stdRedPc . ' => ' . $newStdRedPc . "\n";
                    $log .= '  - remise exc. : ' . $specRedPc . ' => ' . $newSpecRedPc . "\n";
                    $log .= '  - prix final (' . $type . ') : ' . $finalPrice . ' => ' . $newFinalPrice . "\n";
                    &LOC::Log::writeFileLog('updates', $log, $logName);
#                }



                # Récupération du taux de conversion à l'euro
                my $euroRate = &getEuroRate($countryId, $currencyId, $creationDate);

                # Mise à jour de la ligne de devis
                my $update = &updateEstimateLine($db, $countryId, $euroRate, $lineId, $baseTariff, $newStdRedPc,
                                                 $specRedPc, $newSpecRedPc, $newMaxRedPc, $newFinalPrice, $duration, $quantity, $type);
                # Erreur à la mise à jour => on arrête tout
                if ($update == -1)
                {
                    if (!$hasTransaction)
                    {
                        # Annule la transaction
                        $db->rollBack();
                    }
                    last;
                }
            }

            # Mise à jour du devis
            # Erreur à la mise à jour => on arrête tout
            if (&LOC::Estimate::Rent::_updateAmounts($db, $countryId, $id, 0) == 0)
            {
                if (!$hasTransaction)
                {
                    # Annule la transaction
                    $db->rollBack();
                }
                last;
            }

            print $stdout . ' [OK]          ' . "\r";
        }

        # Valide la transaction
        if (!$hasTransaction)
        {
            if (!$db->commit())
            {
                $db->rollBack();
            }
        }
    }

    # Log
    $log = 'Fin de la mise à jour des montants sur les devis -----------------------------------------------------------------------' . "\n";
    &LOC::Log::writeFileLog('updates', $log, $logName);
}



# Function: updateContracts
# Met à jour les contrats
sub updateContracts
{
    our @tabCountries;

    # Log
    my $logName = 'activation_Contracts';
    my $log = 'Début de la mise à jour des montants sur les contrats ------------------------------------------------------------------' . "\n";
    &LOC::Log::writeFileLog('updates', $log, $logName);

    foreach my $countryId (@tabCountries)
    {
        # Log
        $log = '## PAYS : ' . $countryId . "\n";
        &LOC::Log::writeFileLog('updates', $log, $logName);

        # Connexion à la base de données
        my $db = &LOC::Db::getConnection('location', $countryId);

        # Y'a-t-il une transaction en cours ?
        my $hasTransaction = $db->hasTransaction();

        # Démarre la transaction
        if (!$hasTransaction)
        {
            $db->beginTransaction();
        }



        # Récupération de la devise
        my $tabCountryInfos = &LOC::Country::getInfos($countryId);
        my $currencyId = $tabCountryInfos->{'currency.id'};



        # Récupération des contrats à mettre à jour
        # Location seulement !
        my $query = '
SELECT
    tbl_f_rentcontract.CONTRATAUTO AS `id`,
    tbl_f_rentcontract.CONTRATCODE AS `code`,
    tbl_f_rentcontract.CLAUTO AS `customer.id`,
    tbl_f_rentcontract.MOMAAUTO AS `model.id`,
    tbl_f_rentcontract.CONTRATDUREE AS `duration`,
    tbl_f_rentcontract.CONTRATDR AS `endDate`,
    tbl_f_amount.MOAUTO AS `amount.id`,
    tbl_f_amount.MOPXJOURFR AS `amount.dailyPrice`,
    tbl_f_amount.MOTARIFMSFR AS `amount.monthlyPrice`,
    tbl_f_amount.MOFORFAITFR AS `amount.fixedPrice`,
    tbl_f_amount.MOREMISE AS `amount.standardReductionPercent`,
    tbl_f_amount.MOREMISEEX AS `amount.specialReductionPercent`
FROM tbl_f_rentcontract
LEFT JOIN MONTANT tbl_f_amount
ON tbl_f_rentcontract.MOAUTO = tbl_f_amount.MOAUTO
WHERE tbl_f_rentcontract.SOLOAUTO = 0';
        my $tabContracts = $db->fetchAssoc($query, {}, 'id');

        foreach my $tabRow (values %$tabContracts)
        {
            # Données
            my $id         = &LOC::Util::toNumber($tabRow->{'id'});
            my $code       = $tabRow->{'code'};
            my $customerId = &LOC::Util::toNumber($tabRow->{'customer.id'});
            my $modelId    = &LOC::Util::toNumber($tabRow->{'model.id'});
            my $duration   = &LOC::Util::toNumber($tabRow->{'duration'});
            my $endDate    = $tabRow->{'endDate'};
            my $amountId   = &LOC::Util::toNumber($tabRow->{'amount.id'});
            my $stdRedPc   = &LOC::Util::toNumber($tabRow->{'amount.standardReductionPercent'});
            my $specRedPc  = &LOC::Util::toNumber($tabRow->{'amount.specialReductionPercent'});
            my $priceAfterStdRed;
            my $baseTariff;
            my $newStdRedPc;
            my $newSpecRedPc;
            my $newPriceAfterStdRed;
            my $newFinalPrice;

            # Trace console
            our $stdout = 'Calcul des montants du contrat ' . $code;
            print $stdout . '          ' . "\r";

#            # Détermination du prix final et du type de montant
#            my ($finalPrice, $type) = &getFinalPrice($tabRow->{'amount.dailyPrice'}, $tabRow->{'amount.monthlyPrice'},
#                                                     $tabRow->{'amount.fixedPrice'});
#
#            # Récupération des tarifs du client
#            my $tabCustomerTariffs = &LOC::Tariff::Rent::getCustomerTariffs($countryId, $customerId,
#                                                                            {'model.id' => $modelId});
#
#            # Calcul du tarif de base, de la remise standard et de la remise exceptionnelle
#            my $tabRentTariffInfos = &calculateRentTariffInfos($tabCustomerTariffs, $duration, $stdRedPc, $specRedPc,
#                                                               $finalPrice);
#            if ($tabRentTariffInfos == -1)
#            {
#                next;
#            }
#            ($baseTariff, $newStdRedPc, $newPriceAfterStdRed, $newSpecRedPc, $newFinalPrice) = @$tabRentTariffInfos;
#
#            # Calcul de la remise maximale
#            my $newMaxRedPc = &calculateMaxReduction($tabCustomerTariffs, $duration, $newStdRedPc, $newSpecRedPc);



            # Log
#            if ($finalPrice != $newFinalPrice)
#            if ($specRedPc != $newSpecRedPc)
#            {
#                $log = $code . ' :' . "\n";
#                $log .= '  - tarif de base : => ' . $baseTariff . "\n";
#                $log .= '  - remise max. : => ' . $newMaxRedPc . "\n";
#                $log .= '  - remise std : ' . $stdRedPc . ' => ' . $newStdRedPc . "\n";
#                $log .= '  - remise exc. : ' . $specRedPc . ' => ' . $newSpecRedPc . "\n";
#                $log .= '  - prix final (' . $type . ') : ' . $finalPrice . ' => ' . $newFinalPrice . "\n";
#                &LOC::Log::writeFileLog('updates', $log, $logName);
#            }



            # Récupération du taux de conversion à l'euro
            my $euroRate = &getEuroRate($countryId, $currencyId, $endDate);

            # Mise à jour du contrat
            my $update = &updateContract($db, $countryId, $euroRate, $id, $amountId, $baseTariff, $newStdRedPc,
                                         $specRedPc, $newSpecRedPc, 0, $newFinalPrice, '');
#            my $update = &updateContract($db, $countryId, $euroRate, $id, $amountId, $baseTariff, $newStdRedPc,
#                                         $specRedPc, $newSpecRedPc, $newMaxRedPc, $newFinalPrice, $type);
            # Erreur à la mise à jour => on arrête tout
            if ($update == -1)
            {
                if (!$hasTransaction)
                {
                    # Annule la transaction
                    $db->rollBack();
                }
                last;
            }

            print $stdout . ' [OK]          ' . "\r";
        }

        # Valide la transaction
        if (!$hasTransaction)
        {
            if (!$db->commit())
            {
                $db->rollBack();
            }
        }
    }

    # Log
    $log = 'Fin de la mise à jour des montants sur les contrats --------------------------------------------------------------------' . "\n";
    &LOC::Log::writeFileLog('updates', $log, $logName);
}



# Function: getFinalPrice
# Récupère le prix final
#
# Parameters:
# float $dailyPrice     - Prix journalier
# float $monthlyPrice   - Prix mensuel
# float $fixedPrice - Prix forfaitaire
#
# Returns:
# arrayref - Tableau contenant :
#            - le prix final ;
#            - le type de montant.
sub getFinalPrice
{
    my ($dailyPrice, $monthlyPrice, $fixedPrice) = @_;

    my $type = 'day';
    my $finalPrice = $dailyPrice;
    if ($monthlyPrice != 0)
    {
        $type = 'month';
        $finalPrice = $monthlyPrice;
    }
    elsif ($fixedPrice != 0)
    {
        $type = 'fixed';
        $finalPrice = $fixedPrice;
    }
    $finalPrice = &LOC::Util::toNumber($finalPrice);

    return ($finalPrice, $type);
}

# Function: calculateRentTariffInfos
# Calcule les informations tarifaires de location
#
# Parameters:
# hashref $tabCustomerTariffs  - Tableau contenant les tarifs du client
# float   $duration            - Durée
# float   $stdRedPc            - Remise standard (en pourcentage)
# float   $specRedPc           - Remise exceptionnelle (en pourcentage)
# float   $finalPrice          - Prix final
#
# Returns:
# arrayref - Tableau contenant :
#            - le tarif de base ;
#            - la nouvelle remise standard (en pourcentage) ;
#            - le nouveau prix après remise standard ;
#            - la nouvelle remise exceptionnelle (en pourcentage) ;
#            - le nouveau prix final.
sub calculateRentTariffInfos
{
    my ($tabCustomerTariffs, $duration, $stdRedPc, $specRedPc, $finalPrice) = @_;

    our $stdout;

    my $baseTariff;
    my $newStdRedPc;
    my $newPriceAfterStdRed;
    my $newSpecRedPc;
    my $newFinalPrice;

    # Cas d'un tarif à 0
    if ($finalPrice == 0)
    {
        # Tarif de base en vigueur
        foreach my $tabTariff (@$tabCustomerTariffs)
        {
            if ($duration >= $tabTariff->{'minDays'} || ($duration < 1 && $duration >= $tabTariff->{'minDays'} == 1))
            {
                $baseTariff = $tabTariff->{'baseAmount'};
            }
        }

        # Remises et montants
        $newStdRedPc = 100;
        $newSpecRedPc = 0;
        $newPriceAfterStdRed = 0;
        $newFinalPrice = 0;
    }
    # Cas général
    else
    {
        # Calcul du prix après remise standard et avant remise exceptionnelle
        my $priceAfterStdRed = &_calculatePriceAfterStdRed($specRedPc, $finalPrice);
        if ($priceAfterStdRed eq 'ERR')
        {
            return -1;
        }

        # Calcul du tarif de base
        $baseTariff = &_calculateBaseTariff($stdRedPc, $priceAfterStdRed);
        if ($baseTariff eq 'ERR')
        {
            return -1;
        }

        # Calcul des remises
        my $tabReductions = &_calculateReductions($baseTariff, $priceAfterStdRed, $finalPrice);
        ($newStdRedPc, $newSpecRedPc) = @$tabReductions;

        # Recalcul des remises à partir des différents prix
        my $tabNewRentTariffInfos = &_recalculateRentTariffInfos($baseTariff, $newStdRedPc, $newSpecRedPc,
                                                                 $priceAfterStdRed, $finalPrice);
        ($newPriceAfterStdRed, $newFinalPrice) = @$tabNewRentTariffInfos;
    }

    return [$baseTariff, $newStdRedPc, $newPriceAfterStdRed, $newSpecRedPc, $newFinalPrice];
}

# Function: calculateMaxReduction
# Calcule la remise maximale
#
# Parameters:
# hashref $tabCustomerTariffs  - Tableau contenant les tarifs du client
# float   $duration            - Durée
# float   $newStdRedPc         - Nouvelle remise standard (en pourcentage)
# float   $newSpecRedPc        - Nouvelle remise exceptionnelle (en pourcentage)
#
# Returns:
# float - Remise maximale (pourcentage)
sub calculateMaxReduction
{
    my ($tabCustomerTariffs, $duration, $newStdRedPc, $newSpecRedPc) = @_;

    my $newMaxRedPc = 0;
    if ($newSpecRedPc)
    {
        $newMaxRedPc = $newStdRedPc;
    }
    else
    {
        foreach my $tabTariff (@$tabCustomerTariffs)
        {
            if ($duration >= $tabTariff->{'minDays'} || ($duration < 1 && $duration >= $tabTariff->{'minDays'} == 1))
            {
                $newMaxRedPc = $tabTariff->{'maxiReduction'};
            }
        }
        # Remise maximale du client inférieure à la remise standard => on prend la remise standard
        if ($newMaxRedPc < $newStdRedPc)
        {
            $newMaxRedPc = $newStdRedPc;
        }
    }

    return $newMaxRedPc;
}

# Function: getEuroRate
# Récupère le taux de conversion à l'euro
#
# Parameters:
# string $countryId  - Identifiant du pays
# string $currencyId - Identifiant de la devise
# string $date       - Date
#
# Returns:
# float - Taux de conversion à l'euro
sub getEuroRate
{
    my ($countryId, $currencyId, $date) = @_;

    my $euroRate = 1;
    if ($currencyId ne 'EUR')
    {
        $euroRate = &LOC::Country::getEuroRate($countryId, $date);
    }

    return $euroRate;
}

# Function: updateEstimateLine
# Met à jour le devis
#
# Parameters:
# LOC::Db::Adapter $db  - Objet base de données
# string           $countryId     - Identifiant du pays
# float            $euroRate      - Taux de conversion à l'euro
# int              $lineId        - Identifiant de la ligne de devis
# float            $baseTariff    - Tarif de base
# float            $newStdRedPc   - Nouvelle remise standard (en pourcentage)
# float            $specRedPc     - Remise exceptionnelle (en pourcentage)
# float            $newSpecRedPc  - Nouvelle remise exceptionnelle (en pourcentage)
# float            $newMaxRedPc   - Nouvelle remise maximale (en pourcentage)
# float            $newFinalPrice - Nouvau prix final
# float            $duration      - Durée
# int              $quantity      - Quantité
# string           $type          - Type de montant
#
# Returns:
# int - -1 en cas d'erreur, 1 sinon
sub updateEstimateLine
{
    my ($db, $countryId, $euroRate, $lineId, $baseTariff, $newStdRedPc, $specRedPc, $newSpecRedPc,
        $newMaxRedPc, $newFinalPrice, $duration, $quantity, $type) = @_;
    our $stdout;

    # Coefficient multiplicateur dépendant du type de montant
    my $coeff = 1;
    if ($type eq 'monthly')
    {
        $coeff = LOC::Date::AVERAGE_MONTH_WORKING_DAYS;
    }
    if ($type eq 'fixed')
    {
        $coeff = $duration;
    }

    # Montant total de la location
    my $rentAmount = 0;
    if ($duration != 0)
    {
        $rentAmount = $newFinalPrice / $coeff * $duration * $quantity;
    }

    # Mise à jour des montants
    my $tabUpdates = {
                      'DETAILSDEVISTARIFBASE'   => $baseTariff,
                      'DETAILSDEVISTARIFBASEEU' => $baseTariff * $euroRate,
                      'DETAILSDEVISREMISE'      => $newStdRedPc,
                      'DETAILSDEVISREMISEEX'    => $newSpecRedPc,
                      'DETAILSDEVISREMISEMAX'   => $newMaxRedPc,
                      'DETAILSDEVISPXJOURSFR'   => $newFinalPrice,
                      'DETAILSDEVISPXJOUREU'    => $newFinalPrice * $euroRate,
                      'DETAILSDEVISMONTANTFR'   => $rentAmount,
                      'DETAILSDEVISMONTANTEU'   => $rentAmount * $euroRate,
                     };
    my $tabWheres  = {'DETAILSDEVISAUTO' => $lineId};

    # Y'a-t-il une transaction en cours ?
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    if ($db->update('DETAILSDEVIS', $tabUpdates, $tabWheres) == -1)
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        print $stdout . ' [ERR] Problème de mise à jour des données' . "\n";
        return -1;
    }

    # Mise à jour des remises exceptionnelles
    &_updateSpecialReductionAlerts($db, $countryId, $lineId, 'estimate', $specRedPc, $newSpecRedPc);


    # Recalcul de l'assurance de la ligne de devis
    if (&LOC::Estimate::Rent::_updateLineAmounts($db, $countryId, $lineId) == 0)
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        print $stdout . ' [ERR] Problème de mise à jour de l\'assurance de la ligne' . "\n";
        return -1;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();

            print $stdout . ' [ERR] Problème de mise à jour de l\'assurance de la ligne' . "\n";
            return -1;
        }
    }

    return 1;
}

# Function: updateContract
# Met à jour le contrat
#
# Parameters:
# LOC::Db::Adapter $db  - Objet base de données
# string           $countryId     - Identifiant du pays
# float            $euroRate      - Taux de conversion à l'euro
# int              $contractId    - Identifiant du contrat
# int              $amountId      - Identifiant de la ligne de montant
# float            $baseTariff    - Tarif de base
# float            $newStdRedPc   - Nouvelle remise standard (en pourcentage)
# float            $specRedPc     - Remise exceptionnelle (en pourcentage)
# float            $newSpecRedPc  - Nouvelle remise exceptionnelle (en pourcentage)
# float            $newMaxRedPc   - Nouvelle remise maximale (en pourcentage)
# float            $newFinalPrice - Nouvau prix final
# string           $type          - Type de montant
#
# Returns:
# int - -1 en cas d'erreur, 1 sinon
sub updateContract
{
    my ($db, $countryId, $euroRate, $contractId, $amountId, $baseTariff, $newStdRedPc, $specRedPc, $newSpecRedPc,
        $newMaxRedPc, $newFinalPrice, $type) = @_;
    our $stdout;

    # Mise à jour des montants
#    my $tabUpdates = {
#                      'MOTARIFBASE'   => $baseTariff,
#                      'MOTARIFBASEEU' => $baseTariff * $euroRate,
#                      'MOREMISE'      => $newStdRedPc,
#                      'MOREMISEEX'    => $newSpecRedPc,
#                      'MOREMISEMAX'   => $newMaxRedPc,
#                      'MOPXJOURFR'    => ($type eq 'day' ? $newFinalPrice : 0),
#                      'MOPXJOUREU'    => ($type eq 'day' ? $newFinalPrice * $euroRate : 0),
#                      'MOTARIFMSFR'   => ($type eq 'month' ? $newFinalPrice : 0),
#                      'MOTARIFMSEU'   => ($type eq 'month' ? $newFinalPrice * $euroRate : 0),
#                      'MOFORFAITFR'   => ($type eq 'fixed' ? $newFinalPrice : 0),
#                      'MOFORFAITEU'   => ($type eq 'fixed' ? $newFinalPrice * $euroRate : 0),
#                     };
#    my $tabWheres  = {'MOAUTO' => $amountId};

    # Y'a-t-il une transaction en cours ?
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

#    if ($db->update('MONTANT', $tabUpdates, $tabWheres) == -1)
#    {
#        if (!$hasTransaction)
#        {
#            # Annule la transaction
#            $db->rollBack();
#        }
#
#        print $stdout . ' [ERR] Problème de mise à jour des données' . "\n";
#        return -1;
#    }
#
#    # Mise à jour des remises exceptionnelles
#    &_updateSpecialReductionAlerts($db, $countryId, $contractId, 'contract', $specRedPc, $newSpecRedPc);

    # Recalcul du montant total du contrat
    my $tabErrors;
    if (&LOC::Contract::Rent::updateAmounts($countryId, $contractId, $tabErrors) == 0)
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        print $stdout . ' [ERR] Problème de recalcul du montant total' . "\n";
        return -1;
    }
    if ($tabErrors)
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        print $stdout . ' [ERR] Problème de recalcul du montant total :' . "\n";
        print &Data::Dumper::Dumper($tabErrors);
        return -1;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();

            print $stdout . ' [ERR] Problème de recalcul du montant total :' . "\n";
            print &Data::Dumper::Dumper($tabErrors);
            return 0;
        }
    }

    return 1;
}



# Function: _calculatePriceAfterStdRed
# Calcule le prix après remise standard
#
# Parameters:
# float $specRedPc  - Remise exceptionnelle (en pourcentage)
# float $finalPrice - Prix final
#
# Returns:
# float|string - Prix après remise standard ou 'ERR' en cas d'erreur
sub _calculatePriceAfterStdRed
{
    my ($specRedPc, $finalPrice) = @_;

    our $stdout;

    my $priceAfterStdRed;

    if ($specRedPc == 100)
    {
        print $stdout . ' [ERR] Division par 0 : rem. exc. égale à 100' . "\n";
        return 'ERR';
    }
    # Remise exceptionnelle à 0 => prix après remise standard = prix final
    if ($specRedPc == 0)
    {
        $priceAfterStdRed = $finalPrice;
    }
    # Sinon, calcul et arrondi à 0 décimale
    else
    {
        $priceAfterStdRed  = &LOC::Util::round($finalPrice / (1 - $specRedPc / 100), 0);
    }
    if ($priceAfterStdRed == 0)
    {
        print $stdout . ' [ERR] Prix après rem. std à 0' . "\n";
        return 'ERR';
    }

    return $priceAfterStdRed;
}

# Function: _calculateBaseTariff
# Calcule le tarif de base
#
# Parameters:
# float $stdRedPc         - Remise standard (en pourcentage)
# float $priceAfterStdRed - Prix après remise standard
#
# Returns:
# float|string - Tarif de base ou 'ERR' en cas d'erreur
sub _calculateBaseTariff
{
    my ($stdRedPc,$priceAfterStdRed) = @_;

    our $stdout;

    my $baseTariff;

    if ($stdRedPc == 100)
    {
        print $stdout . ' [ERR] Division par 0 : rem. std égale à 100' . "\n";
        return 'ERR';
    }
    # Remise standard à 0 => tarif de base = prix après remise standard
    if ($stdRedPc == 0)
    {
        $baseTariff = $priceAfterStdRed;
    }
    # Sinon, calcul et arrondi à 0 décimale
    else
    {
        $baseTariff = &LOC::Util::round($priceAfterStdRed / (1 - $stdRedPc / 100), 0);
    }
    if ($baseTariff == 0)
    {
        print $stdout . ' [ERR] Tarif de base à 0' . "\n";
        return 'ERR';
    }

    return $baseTariff;
}

# Function: _calculateReductions
# Calcule les remises
#
# Parameters:
# float $baseTariff       - Tarif de base
# float $priceAfterStdRed - Prix après remise standard
# float $finalPrice       - Prix final
#
# Returns:
# arrayref - Tableau contenant la remise standard et la remise exceptionnelle (en pourcentage)
sub _calculateReductions
{
    my ($baseTariff, $priceAfterStdRed, $finalPrice) = @_;

    my $newStdRedPc = &LOC::Util::round((1 - $priceAfterStdRed / $baseTariff) * 100, 6);
    my $newSpecRedPc = &LOC::Util::round((1 - $finalPrice / $priceAfterStdRed) * 100, 6);

    return [$newStdRedPc, $newSpecRedPc];
}

# Function: _recalculateRentTariffInfos
# Recalcule le prix après remise standard et le prix final
#
# Parameters:
# float $baseTariff       - Tarif de base
# float $newStdRedPc      - Remise standard (en pourcentage)
# float $newSpecRedPc     - Remise exceptionnelle (en pourcentage)
# float $priceAfterStdRed - Prix après remise standard
# float $finalPrice       - Prix final
#
# Returns:
# arrayref|string - Tableau contenant le prix après remise standard et le prix final recalculés à partir du tarif de
#                   base et des remises ou 'ERR' en cas d'erreur
sub _recalculateRentTariffInfos
{
    my ($baseTariff, $newStdRedPc, $newSpecRedPc, $priceAfterStdRed, $finalPrice) = @_;

    our $stdout;

    # Recalcul du prix après remise standard avec la méthode normale (en partant du tarif de base)
    if ($newStdRedPc == 100)
    {
        print $stdout . ' [ERR] Division par 0 : nouvelle rem. std égale à 100' . "\n";
        return 'ERR';
    }
    my $newPriceAfterStdRed = &LOC::Util::round($baseTariff * (1 - $newStdRedPc / 100), 0);

    # Recalcul du prix final avec la méthode normale (en partant du prix après remise standard)
    if ($newSpecRedPc == 100)
    {
        print $stdout . ' [ERR] Division par 0 : nouvelle rem. exc. égale à 100' . "\n";
        return 'ERR';
    }
    my $newFinalPrice = &LOC::Util::round($newPriceAfterStdRed * (1 - $newSpecRedPc / 100), 0);

    # Si le prix final calculé est égal au prix final stocké arrondi à 0 décimale,
    # on considère qu'il ne faut pas le mettre à jour (cas des contrats ayant des prix avec décimales)
    if ($newFinalPrice == &LOC::Util::round(&LOC::Util::round($finalPrice, 2), 0) ||
        abs($newFinalPrice - $finalPrice) <= .5)
    {
        $newFinalPrice = $finalPrice;
    }

    return [$newPriceAfterStdRed, $newFinalPrice];
}

# Function: _updateSpecialReductionAlerts
# Met à jour les alertes de remises exceptionnelles
#
# Parameters:
# LOC::Db::Adapter $db           - Objet base de données
# string           $countryId    - Identifiant du pays
# int              $id           - Identifiant de la ligne de devis ou du contrat
# string           $type         - Type ('estimate' ou 'contract')
# float            $specRedPc    - Remise exceptionnelle (en pourcentage)
# float            $newSpecRedPc - Nouvelle remise exceptionnelle (en pourcentage)
#
# Returns:
# arrayref|string - Tableau contenant le prix après remise standard et le prix final recalculés à partir du tarif de
#                   base et des remises ou 'ERR' en cas d'erreur
sub _updateSpecialReductionAlerts
{
    my ($db, $countryId, $id, $type, $specRedPc, $newSpecRedPc) = @_;

    our $stdout;

    if ($specRedPc != $newSpecRedPc)
    {
        my $label = sprintf('# MAJ remise suite à la ME de %f à %f',
                            &LOC::Util::toNumber($specRedPc), &LOC::Util::toNumber($newSpecRedPc));

        my $tabUpdates = {
                          'REMISEACC' => $newSpecRedPc,
                          'ALMOTIF*'  => 'CONCAT(ALMOTIF, "\n", ' . $db->quote($label) . ')',
                         };
        my $whereField;
        if ($type eq 'estimate')
        {
            $whereField = 'DETAILSDEVISAUTO';
        }
        elsif ($type eq 'contract')
        {
            $whereField = 'CONTRATAUTO';
        }
        else
        {
            print $stdout . ' [ERR] Problème de mise à jour de l\'alerte de tarification' . "\n";
            return -1;
        }
        my $tabWheres = {$whereField => $id};

        # Y'a-t-il une transaction en cours ?
        my $hasTransaction = $db->hasTransaction();

        # Démarre la transaction
        if (!$hasTransaction)
        {
            $db->beginTransaction();
        }

        if ($db->update('GC_ALERTES', $tabUpdates, $tabWheres, 'GESTIONTARIF' . $countryId) == -1)
        {
            if (!$hasTransaction)
            {
                # Annule la transaction
                $db->rollBack();
            }

            print $stdout . ' [ERR] Problème de mise à jour de l\'alerte de tarification' . "\n";
            return -1;
        }
        if ($db->update('GC_ARCHIVES', $tabUpdates, $tabWheres, 'GESTIONTARIF' . $countryId) == -1)
        {
            if (!$hasTransaction)
            {
                # Annule la transaction
                $db->rollBack();
            }

            print $stdout . ' [ERR] Problème de mise à jour de l\'alerte de tarification' . "\n";
            return -1;
        }

        # Valide la transaction
        if (!$hasTransaction)
        {
            if (!$db->commit())
            {
                $db->rollBack();

                print $stdout . ' [ERR] Problème de mise à jour de l\'alerte de tarification' . "\n";
                return 0;
            }
        }
    }

    return 1;
}
