#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../../inc', '../../lib';

# Traitement des variables d'environnement
use LOC::Env;

# Modules internes
use LOC::Agency;
use LOC::Contract::Rent;
use LOC::Estimate::Rent;
use LOC::Globals;
use LOC::Log;
use LOC::Mail;
use LOC::Request;
use LOC::Session;
use LOC::Transport;


# Mise à jour des transports et commandes
&renewTransports();


# Récupérer et mettre à jour les transports
sub renewTransports
{
    my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);

    foreach my $countryId (@$tabCountries)
    {
        my $deltaSendOrder = &LOC::Characteristic::getCountryValueByCode('DELTACMDRECUP', $countryId);
        my @tabDocuments;

        my %now          = &LOC::Date::getHashDate();
        my ($year, $month, $day) = &Date::Calc::Add_Delta_Days($now{'y'}, $now{'m'}, $now{'d'}, $deltaSendOrder);

        my %limitDateTab = ('d' => $day, 'm' => $month, 'y' => $year);
        my $limitDate    = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE, %limitDateTab);

        my @tabToRenew;
        my $tabDocs = {
            'estimateLine' => [],
            'contract'     => []
        };

        # ------ CONTRAT -------
        # Récupération des contrats
        my $tabFilters = {
            'endDate'      => [&LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE), $limitDate],
            'stateId'      => [LOC::Contract::Rent::STATE_PRECONTRACT, LOC::Contract::Rent::STATE_CONTRACT, LOC::Contract::Rent::STATE_STOPPED]
        };
        my $tabContractIds = &LOC::Contract::Rent::getList($countryId, LOC::Util::GETLIST_IDS, $tabFilters);
        my $nbContractIds = @$tabContractIds;

        if ($nbContractIds > 0)
        {
            my $tabFiltersTransp = {
                'isSelf' => 0,
                'from'   => {
                    'type' => LOC::Transport::FROMTOTYPE_CONTRACT,
                    'id'   => $tabContractIds
                }
            };
            my $tabTransport = &LOC::Transport::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFiltersTransp);

            # Transports à mettre à jour pour les contrats
            foreach my $transportInfos (values(%$tabTransport))
            {
                if (!$transportInfos->{'order.id'})
                {
                    push(@tabToRenew, $transportInfos->{'id'});
                    push(@{$tabDocs->{'contract'}}, $transportInfos->{'from.id'});
                }
            }
        }

        # ------ LIGNES DE DEVIS -------
        # Récupération des lignes de devis
        my $tabFilters = {
            'isAccepted'   => 1,
            'endDate'      => [&LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE), $limitDate],
            'stateId'      => [LOC::Estimate::Rent::LINE_STATE_ACTIVE]
        };
        my $tabEstimateLineIds = &LOC::Estimate::Rent::getLinesList($countryId, LOC::Util::GETLIST_IDS, $tabFilters);
        my $nbEstimateLineIds = @$tabEstimateLineIds;

        if ($nbEstimateLineIds > 0)
        {
            my $tabFiltersTransp = {
                'isSelf' => 0,
                'from'   => {
                    'type' => LOC::Transport::FROMTOTYPE_ESTIMATELINE,
                    'id'   => $tabEstimateLineIds
                }
            };
            my $tabTransport = &LOC::Transport::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFiltersTransp);

            # Transports à mettre à jour pour les lignes de devis
            foreach my $transportInfos (values(%$tabTransport))
            {
                if (!$transportInfos->{'order.id'})
                {
                    push(@tabToRenew, $transportInfos->{'id'});
                    push(@{$tabDocs->{'estimateLine'}}, $transportInfos->{'from.id'});
                }
            }
        }

        # Récupération de la connexion à la base de données
        my $db = &LOC::Db::getConnection('location', $countryId);
        $db->beginTransaction();

        # ------ MISE A JOUR DES TRANSPORTS -------
        foreach my $transportId (@tabToRenew)
        {
            $db->addTransactionAction('commit.pre', sub {
                    return &LOC::Transport::renew($countryId, {'id' => $transportId}, {'lastSending' => 1});
                },
                {'category' => 'transport',
                 'priority' => -1000,
                 'id' => 'LOC::Transport::renew_' . $countryId . '-id:' . $transportId}
            );
        }

        # Valide la transaction
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }

        # Sortie
        print "Identifiants des contrats mis à jour\n\n";
        foreach my $contractId (@{$tabDocs->{'contract'}})
        {
            print $contractId . "\n";
        }

        print "\n\n";
        print "Identifiants des lignes de devis mis à jour\n\n";
        foreach my $estimateLineId (@{$tabDocs->{'estimateLine'}})
        {
            print $estimateLineId . "\n";
        }
    }
}