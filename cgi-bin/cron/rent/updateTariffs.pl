#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../../inc', '../../lib';

# Traitement des variables d'environnement
use LOC::Env;

# Modules internes
use LOC::Agency;
use LOC::Characteristic;
use LOC::Country;
use LOC::Date;
use LOC::Globals;
use LOC::Locale;
use LOC::Log;
use LOC::Mail;
use LOC::Request;
use LOC::Session;
use LOC::TariffFamily;
use LOC::Tariff::Rent;

# Modules externes
use File::Temp;
use JSON::XS;
use Number::Format;
use SOAP::Lite;

print "Content-type: text/plain; charset=UTF-8\n\n";


# Destinataires du mail
my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_PAIRS);
our $tabRecipient = {};
foreach my $countryId (keys(%$tabCountries))
{
    my @tabRecipients = split(',', &LOC::Characteristic::getCountryValueByCode('DESTYIELD', $countryId));
    push(@{$tabRecipient->{$countryId}}, @tabRecipients);
}



# Calcul des grilles tarifaires yieldées
&updateTariffs();



# Function: updateTariffs
# Calcule les grilles tarifaires de référence de location yieldées
sub updateTariffs
{
    # Date du jour
    my $date = &LOC::Date::getMySQLDate();

    # Pays concernés par le yield
    my $tabCountries = &getCountries();

    foreach my $countryId (keys(%$tabCountries))
    {
        # Liste des fichiers joints au mail
        my $tabFiles = {};

        # Catégories de clients
        my $tabCustomerCategories = &getCustomerCategories($countryId);

        # Tranches de durée
        my $tabDurationRanges = &LOC::Tariff::Rent::getDurationRangesList($countryId);

        # Tarif de base
        my $tabBaseTariffs = &getBaseTariffs($countryId);

        # Familles tarifaires
        my $tabTariffFamilies = &LOC::TariffFamily::getList($countryId, LOC::Util::GETLIST_ASSOC);

        # Récupération des taux d'utilisation
        my $tabUseRates = &getUseRates($countryId);

        # HTML
        my $html = '<hr />' . "\n";

        foreach my $category (@$tabCustomerCategories)
        {
            # Récupération des tarifs
            my $tabTariffs = &getReferenceTariffs($countryId, $category->{'id'});

            # Recalcul des tarifs
            my $tabNewTariffs = &calculateNewTariffs($tabTariffFamilies, $tabDurationRanges, $tabUseRates, $tabBaseTariffs,
                                                     $tabTariffs);

            # Génération du CSV
            my $filename = sprintf('%s_%s.csv', $category->{'name'}, $date);
            $tabFiles->{$filename} = &generateCSV($filename, $tabNewTariffs);

            my $description = $category->{'description'};
            $description =~ s/</&lt;/g;
            $description =~ s/>/&gt;/g;
            $html .= '
<h1>' . $category->{'name'} . ' (' . $description . ')</h1>' . "\n";
            $html .= &getTariffHtml($tabCountries->{$countryId}->{'locale.id'}, $tabDurationRanges, $tabTariffFamilies,
                                    $tabBaseTariffs, $tabTariffs, $tabNewTariffs, $tabUseRates);
        }

        my $mailBody = &getMailBody($tabCountries->{$countryId}->{'locale.id'});
        $mailBody = sprintf($mailBody, $html);

        # Envoi du mail
        &sendMail($countryId, $tabCountries->{$countryId}->{'locale.id'}, $mailBody, $tabFiles);
    }
}



# Function: getCountries
# Récupère la liste des pays à mettre à jour
#
# Returns:
# array - Liste des pays à mettre à jour
sub getCountries
{
    my $tabResult = {};
    my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_ASSOC);

    foreach my $country (values(%$tabCountries))
    {
        my $charac = &LOC::Characteristic::getCountryValueByCode('YIELDLOC', $country->{'id'});
        if ($charac == 1)
        {
            $tabResult->{$country->{'id'}} = $country
        }
    }

    return $tabResult;
}

# Function: getBaseTariffs
# Récupère le tarif de base
#
# Parameters:
# string $countryId     - Identifiant du pays
#
# Returns:
# hashref - Informations du tarif de base
sub getBaseTariffs
{
    my ($countryId) = @_;

    my $tabBaseTariffs = &LOC::Tariff::Rent::getBaseTariffs($countryId);
    my $tabResult = {};
    foreach my $tariff (@$tabBaseTariffs)
    {
        my $rangeLower = $tariff->{'minDays'} * 1;
        $tabResult->{$tariff->{'tariffFamily.id'}}->{$rangeLower} = {'amount' => $tariff->{'amount'} * 1};
    }

    return $tabResult;
}

# Function: getCustomerCategories
# Récupère les catégories de clients
#
# Parameters:
# string $countryId - Identifiant du pays
#
# Returns:
# hashref - Informations sur les catégories de clients
sub getCustomerCategories
{
    my ($countryId) = @_;

    # Connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT CT.CLIENTTARIFAUTO AS `id`, CT.TARIFCLIENTREF AS `name`, CT.TARIFCLIENTLIB AS `description`
FROM CLIENTTARIF CT
WHERE CT.TARIFCLIENTACTIF = 1
ORDER BY `name`';
    my $tabResult = $db->fetchAssoc($query);

    return $tabResult;
}

# Function: getReferenceTariffs
# Récupère une grille de référence
#
# Parameters:
# string $countryId  - Identifiant du pays
# int    $categoryId - Identifiant de la catégorie de client
#
# Returns:
# hashref - Informations de la grille de référence
sub getReferenceTariffs
{
    my ($countryId, $categoryId) = @_;

    # Connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = 'GESTIONTARIF' . $countryId;

    my $query = '
SELECT PR.FAMTARAUTO, IF(PR.GRNBJOURSMIN = 1,0,PR.GRNBJOURSMIN) AS GRNBJOURSMIN, PR.REMISERECO, PR.REMISEMAX,
    PR.PRIXPLANCHER, PR.PRIXCIBLE
FROM ' . $schemaName . '.GC_PRIXREF PR
WHERE PR.CLIENTTARIFAUTO = ' . $categoryId;
    my $tabTariffs = $db->fetchAssoc($query);

    # Assenblage du tableau de résultat
    my $tabResult = {};
    foreach my $tariff (@$tabTariffs)
    {
        $tabResult->{$tariff->{'FAMTARAUTO'}}->{$tariff->{'GRNBJOURSMIN'}} =
            {'recoReduction' => $tariff->{'REMISERECO'} * 1,
             'maxiReduction' => $tariff->{'REMISEMAX'} * 1,
             'floorPrice'    => $tariff->{'PRIXPLANCHER'} * 1,
             'targetPrice'   => $tariff->{'PRIXCIBLE'} * 1
            };
    }

    return $tabResult;
}

# Function: getUseRates
# Récupère les tranches de durée
#
# Parameters:
# string $countryId     - Identifiant du pays
#
# Returns:
# hashref - Taux d'utilisation nationaux
sub getUseRates
{
    my ($countryId) = @_;

    my $charac = &LOC::Characteristic::getCountryValueByCode('VISUTU', $countryId);
    my @tabTarFam = ();
    if ($charac ne '')
    {
        my $decodedCarac = JSON::XS->new->decode($charac);
        @tabTarFam = @{$decodedCarac->{'tariffFamiliesToExclude'}};
    }

    # Connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT
    MM.FAMTARAUTO AS `tariffFamily.id`,
    SUM(STMOLOU) AS `rented`,
    SUM(STMOLIV) AS `delivery`,
    SUM(STMOTOTAL) AS `total`
FROM AUTH.STOCKMODELE SM
INNER JOIN AUTH.MODELEMACHINE MM ON SM.MOMAAUTO = MM.MOMAAUTO
LEFT JOIN frmwrk.p_agency ON CONVERT(SM.AGAUTO USING utf8) = agc_id AND
   agc_type = ' . $db->quote(LOC::Agency::TYPE_AGENCY) . ' AND
   agc_sta_id <> ' . $db->quote(LOC::Agency::STATE_DELETED) . '
WHERE agc_cty_id = "' . $countryId . '"';
    if (@tabTarFam > 0)
    {
        $query .= '
AND MM.FAMTARAUTO NOT IN (' . join(', ', @tabTarFam) . ')';
    };
    $query .= '
GROUP BY MM.FAMTARAUTO';
    my $tabResult = $db->fetchAssoc($query, {}, 'tariffFamily.id');
    foreach my $line (values(%$tabResult))
    {
        $line->{'useRate'} = ($line->{'total'} == 0 ? 0 :
                                &LOC::Util::round(($line->{'rented'} + $line->{'delivery'}) / $line->{'total'}, 2));
    }

    return $tabResult;
}

# Function: generateCSV
# Génération du fichier CSV
#
# Parameters:
# hashref $tabTariffs - Tarifs
#
# Returns:
# string - Chemin complet vers le fichier CSV
sub generateCSV
{
    my ($filename, $tabTariffs) = @_;

    my ($fileHandle, $filePath) = &File::Temp::tempfile(UNLINK => 1);

    my $format = new Number::Format(-decimal_point => ',', -decimal_digits => 6);

    open(FILE, '>' . $filePath);
    foreach my $tariffFamilyId (keys(%$tabTariffs))
    {
        foreach my $rangeLower (keys(%{$tabTariffs->{$tariffFamilyId}}))
        {
            my $recoReduction = $tabTariffs->{$tariffFamilyId}->{$rangeLower}->{'recoReduction'} / 100;
            my $maxiReduction = $tabTariffs->{$tariffFamilyId}->{$rangeLower}->{'maxiReduction'} / 100;

            printf FILE "%s;%s;%s;%s\r\n", $tariffFamilyId,
                                           ($rangeLower == 0 ? 1 : $rangeLower),
                                           $format->format_number($recoReduction),
                                           $format->format_number($maxiReduction);
        }
    }
    close(FILE);

    return $filePath;
}

# Function: getTariffHtml
# Corps du mail
#
# Parameters:
# string $localeId - Identifiant de la locale
#
# Returns:
# string - Tarif au format HTML
sub getTariffHtml
{
    my ($localeId, $tabDurationRanges, $tabTariffFamilies, $tabBaseTariffs, $tabTariffs, $tabNewTariffs, $tabUseRates) = @_;

    my $locale = &LOC::Locale::getLocale($localeId);

    my $html = '<table>
<tr>
    <th colspan="4" rowspan="2"></th>';
    foreach my $range (@$tabDurationRanges)
    {
        $html .= '
    <th colspan="2">' . $range->{'shortLabel'} . '</th>';
    }
    $html .= '
</tr>
<tr>';
    foreach my $range (@$tabDurationRanges)
    {
        $html .= '
    <th class="subheader">' . $locale->t('Cible') . '</th>
    <th class="subheader">' . $locale->t('Mini') . '</th>';
    }
    $html .= '
</tr>';

    my $previousOrder = 0;

    foreach my $tabTariffFamily (values(%$tabTariffFamilies))
    {
        my $useRate = &LOC::Util::round($tabUseRates->{$tabTariffFamily->{'id'}}->{'useRate'} * 100, 2);

        $html .= '
<tr>';
        if (&LOC::Util::round($tabTariffFamily->{'order'} / 100, 0) != &LOC::Util::round($previousOrder / 100, 0))
        {
            $html .= '
    <td rowspan="' . $tabTariffFamily->{'nbElements'} . '">' . $tabTariffFamily->{'label'} . '</td>';
        }
        $html .= '
    <td>' . $tabTariffFamily->{'sublabel'} . '</td>
    <td>' . ($tabTariffFamily->{'isYieldable'} ? '' : '*') . '</td>
    <td class="useRate">(' . $useRate . ' %)</td>';
        foreach my $range (@$tabDurationRanges)
        {
            my $rangeLower = $range->{'lower'} * 1;

            my $basePrice = $tabBaseTariffs->{$tabTariffFamily->{'id'}}->{$rangeLower}->{'amount'};

            my $recoPrice = &LOC::Util::round($basePrice - $basePrice * $tabTariffs->{$tabTariffFamily->{'id'}}->{$rangeLower}->{'recoReduction'} / 100, 0);
            my $miniPrice = &LOC::Util::round($basePrice - $basePrice * $tabTariffs->{$tabTariffFamily->{'id'}}->{$rangeLower}->{'maxiReduction'} / 100, 0);

            my $newRecoPrice = &LOC::Util::round($basePrice - $basePrice * $tabNewTariffs->{$tabTariffFamily->{'id'}}->{$rangeLower}->{'recoReduction'} / 100, 0);
            my $newMiniPrice = &LOC::Util::round($basePrice - $basePrice * $tabNewTariffs->{$tabTariffFamily->{'id'}}->{$rangeLower}->{'maxiReduction'} / 100, 0);

            my $class = '';
            if ($newRecoPrice != $recoPrice || $newMiniPrice != $miniPrice)
            {
                $class = ' class="modif"';
            }

            if ($newRecoPrice == $recoPrice)
            {
                $html .= '
    <td' . $class . '>' . $recoPrice . '</td>';
            }
            else
            {
                $html .= '
    <td' . $class . '><span class="linethrough">' . $recoPrice . '</span><br />' . $newRecoPrice . '</td>';
            }
            if ($newMiniPrice == $miniPrice)
            {
                $html .= '
    <td' . $class . '>' . $miniPrice . '</td>';
            }
            else
            {
                $html .= '
    <td' . $class . '><span class="linethrough">' . $miniPrice . '</span><br />' . $newMiniPrice . '</td>';
            }
        }
        $html .= '
</tr>';

        $previousOrder = $tabTariffFamily->{'order'};
    }
    $html .= '</table>
* ' . $locale->t('Famille tarifaire non yieldable') . "\n";

    return $html;
}

# Function: calculateNewTariffs
# Calcul des nouveaux tarifs
#
# Parameters:
# hashref $tabTariffFamilies - Liste des familles tarifaires
# hashref $tabDurationRanges - Liste des tranches de durée
# hashref $tabUseRates       - Liste des taux d'utilisation
# hashref $tabBaseTariffs    - Tarifs de base
# hashref $tabTariffs        - Tarifs en vigueur
#
# Returns:
# hashref - Nouveaux tarifs
sub calculateNewTariffs
{
    my ($tabTariffFamilies, $tabDurationRanges, $tabUseRates, $tabBaseTariffs, $tabTariffs) = @_;

    my $tabNewTariffs = {};
    foreach my $tabTariffFamily (values(%$tabTariffFamilies))
    {
        foreach my $range (@$tabDurationRanges)
        {
            my $rangeLower = $range->{'lower'} * 1;

            my $basePrice     = $tabBaseTariffs->{$tabTariffFamily->{'id'}}->{$rangeLower}->{'amount'};
            my $maxiReduction = $tabTariffs->{$tabTariffFamily->{'id'}}->{$rangeLower}->{'maxiReduction'};
            my $recoReduction = $tabTariffs->{$tabTariffFamily->{'id'}}->{$rangeLower}->{'recoReduction'};
            my $targetPrice   = $tabTariffs->{$tabTariffFamily->{'id'}}->{$rangeLower}->{'targetPrice'};
            my $floorPrice    = $tabTariffs->{$tabTariffFamily->{'id'}}->{$rangeLower}->{'floorPrice'};

            # Calcul seulement si la famille tarifaire est yieldable
            if ($tabTariffFamily->{'isYieldable'})
            {
                my $newAmount = $floorPrice;
                my $newRecoReduction = ($basePrice == 0 ? 0 : 1 - $targetPrice / $basePrice);

                # Plus de 5 jours
                if ($rangeLower >= 5)
                {
                    my $useRate = $tabUseRates->{$tabTariffFamily->{'id'}}->{'useRate'};

                    # Taux d'utilisation national <= 50 %
                    if ($useRate <= 0.5)
                    {
                        $newAmount = &LOC::Util::round($floorPrice, 0);
                    }
                    # Taux d'utilisation national >= 65 %
                    elsif ($useRate >= 0.65)
                    {
                        $newAmount        = &LOC::Util::round($targetPrice, 0);
                    }
                    # Taux d'utilisation national entre 50 % et 65 %
                    else
                    {
                        $newAmount = &LOC::Util::round($floorPrice + ($targetPrice - $floorPrice) * ($useRate - 0.5) / 0.15, 0);
                    }
                }
                # Moins de 5 jours
                else
                {
                    $newAmount        = &LOC::Util::round($targetPrice, 0);
                }

                my $newMaxiReduction = ($basePrice == 0 ? 0 : 1 - $newAmount / $basePrice);
                if ($newRecoReduction >= $newMaxiReduction)
                {
                    $newRecoReduction = $newMaxiReduction;
                }
                $tabNewTariffs->{$tabTariffFamily->{'id'}}->{$rangeLower}->{'recoReduction'} = $newRecoReduction * 100;
                $tabNewTariffs->{$tabTariffFamily->{'id'}}->{$rangeLower}->{'maxiReduction'} = $newMaxiReduction * 100;
            }
            else
            {
                $tabNewTariffs->{$tabTariffFamily->{'id'}}->{$rangeLower}->{'recoReduction'} = $recoReduction;
                $tabNewTariffs->{$tabTariffFamily->{'id'}}->{$rangeLower}->{'maxiReduction'} = $maxiReduction;
            }
        }
    }

    return $tabNewTariffs;
}

# Function: getMailBody
# Corps du mail
#
# Parameters:
# string $localeId - Identifiant de la locale
#
# Returns:
# string - Code HTML
sub getMailBody
{
    my ($localeId) = @_;

    my $locale = &LOC::Locale::getLocale($localeId);

    my $html = '<html>
<head>
<style>
body
{
    font-family: "Segoe UI", Verdana, Arial, Helvetica, Sans-serif;
    font-size: 9pt;
}

h1
{
    font-family: "Segoe UI", Arial, Helvetica, Sans-serif;
    font-size: 12pt;
}

a:link, a:active, a:visited
{
    color: #669ACB;
    text-decoration: none;
    border: none;
}
a:hover
{
    text-decoration: underline;
    border-bottom: 1px solid #BFDDEF;
    color: #007AC2;
    border: none;
}

table
{
    font-size: 8pt;
    border-top: 1px solid #66AFDA;
    border-bottom: 1px solid #007AC2;
    border-left: 1px solid #66AFDA;
    border-right: 1px solid #007AC2;
    vertical-align: middle;
    text-align: left;
    border-spacing: 0px;
    border-collapse: collapse;
    font-size: 8pt;
}
table:hover
{
    border-color: #007AC2;
}

th
{
    color: black;
    font-weight: normal;
    font-style: italic;
    margin: 0;
    padding: 3px 5px 4px 5px;
    background-color: #E8F3F9;
    white-space: nowrap;
    text-align: left;
}

th.subheader
{
    font-size: 6pt;
}

td
{
    border-bottom: 1px solid #CCE4F2;
    padding: 0 5px 0 5px;
    white-space: nowrap;
}


.useRate
{
    color: gray;
    font-size: 6pt;
    text-align: center;
}

.modif
{
    background-color: #FFFFDD;
}

.linethrough
{
    text-decoration: line-through;
}
</style>
</head>

<body>
<p>' . $locale->t('Bonjour,') . '</p>
<p>' . $locale->t('Veuillez trouver ci-joint les grilles tarifaires de référence de location mises à jour en fonction des taux d\'utilisation du %s.', $locale->getDateFormat(undef, LOC::Locale::FORMAT_DATETIME_NUMERIC2)) . '</p>
<p>' . $locale->t('Vous pouvez mettre à jour informatiquement ces grilles en vous rendant, dans l\'application "Gestion de la tarification", dans le menu Superviseur / Tarifs de référence. Vous pourrez alors coller le contenu des fichiers CSV joints.') . '</p>
<p>' . $locale->t('Cordialement.') . '</p>
%s
</body>
</html>
';

    return $html;
}

# Function: sendMail
# Envoi l'e-mail
#
# Parameters:
# string  $countryId - Identifiant du pays
# string  $html      - Code HTML du corps de l'e-mail
# hashref $tabFiles  - Liste des fichiers joints
sub sendMail
{
    my ($countryId, $localeId, $html, $tabFiles) = @_;

    our $tabRecipient;

    my $locale = &LOC::Locale::getLocale($localeId);

    # Mail
    my $mailObj = LOC::Mail->new();

    # Encodage
    $mailObj->setEncodingType(LOC::Mail::ENCODING_TYPE_UTF8);

    # Expéditeurs
    my $mailsSender = &LOC::Globals::get('mailsSender');
    $mailObj->setSender(%$mailsSender);

    # Destinataires
    my %tabDefaultRecipient = ('archive.dev@acces-industrie.com' => $locale->t('Développeurs'));
    if (@{$tabRecipient->{$countryId}} > 0)
    {
        # Envoi aux destinataires en prod
        if (&LOC::Globals::getEnv() eq 'exp')
        {
            foreach my $mail (@{$tabRecipient->{$countryId}})
            {
                $mailObj->addRecipient($mail);
            }
        }
        # Mais pas sur les autres environnements
        else
        {
            my $recipients = join (', ', @{$tabRecipient->{$countryId}});
            $html =~ s/<body>/<body>\n<b><em>Destinataires d'origine :<\/em><br> $recipients<\/b>/;

            $mailObj->addRecipient(%tabDefaultRecipient);
        }
    }
    else
    {
        $mailObj->addRecipient(%tabDefaultRecipient);
    }

    # Corps
    $mailObj->setBodyType(LOC::Mail::BODY_TYPE_HTML);
    $mailObj->setBody($html);

    # Pièces jointes
    foreach my $file (sort keys(%$tabFiles))
    {
        $mailObj->addLocalFileAttachment($tabFiles->{$file}, $file);
    }

    # Objet
    my $subject = $locale->t('Grilles tarifaires de référence de location mises à jour') . ' - ' . $countryId;
    $mailObj->setSubject($subject);

    $mailObj->send();

    my $errors = $mailObj->getErrors();
    if ($errors)
    {
        print STDERR $errors;
    }
}
