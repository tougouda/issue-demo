#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../../inc', '../../lib';

# Traitement des variables d'environnement
use LOC::Env;

# Modules internes
use LOC::Agency;
use LOC::Contract::Rent;
use LOC::Estimate::Rent;
use LOC::Transport;
use LOC::Globals;
use LOC::Log;
use LOC::Mail;
use LOC::Request;
use LOC::Session;
use LOC::Tariff::Rent;

# Modules externes
use SOAP::Lite;



# Mise à jour des devis
&updateEstimates();



# Function: updateEstimates
# Met à jour les devis
sub updateEstimates
{
    my @tabCountries = &getCountries();
    my @tabStates = &getStates();

    # Nombre de lignes de devis mis à jour par pays
    my %tabUpdatedEstimateLines = ();

    # Indicateur de présence d'erreurs
    my $hasErrors = 0;

    # Contenu HTML du mail
    my %html = ();

    foreach my $countryId (@tabCountries)
    {
        # Connexion à la base de données
        my $db = &LOC::Db::getConnection('location', $countryId);

        # Y'a-t-il une transaction en cours ?
        my $hasTransaction = $db->hasTransaction();

        # Démarre la transaction
        if (!$hasTransaction)
        {
            $db->beginTransaction();
        }


        # Identifiant du motif de validation
        # location
        my $rentLabelId = &LOC::Characteristic::getCountryValueByCode('LIBDEVDESLOC', $countryId);
        my $tspLabelId  = &LOC::Characteristic::getCountryValueByCode('LIBDEVDESTSP', $countryId);

        # Récupération du nombre de jours de validité
        my $nbValidDays = &LOC::Characteristic::getCountryValueByCode('JRSVALIDDEVIS', $countryId);
        $nbValidDays = &LOC::Util::toNumber($nbValidDays);

        # Calcul de la date jusqu'à laquelle remonter
        my @tabLocaltime = localtime();
        my @tabValidDate = &Date::Calc::Add_Delta_Days($tabLocaltime[5] + 1900, $tabLocaltime[4] + 1, $tabLocaltime[3],
                                                       - $nbValidDays);
        my $validDate = sprintf('%04d-%02d-%02d', $tabValidDate[0], $tabValidDate[1], $tabValidDate[2]);
        my $formatValidDate = sprintf('%02d/%02d/%04d', $tabValidDate[2], $tabValidDate[1], $tabValidDate[0]);


        # Initialisation du nombre de lignes mises à jour
        $tabUpdatedEstimateLines{$countryId} = 0;

        # Initialisation du HTML
        $html{$countryId} = '
<p>Lignes de devis de plus de ' . $nbValidDays . ' jours (créées avant le ' . $formatValidDate . ') :</p>
<table>
<tr>
<th>Devis</th>
<th>Création de la ligne</th>
<th>Action</th>
</tr>';


        # Récupération des devis à mettre à jour
        my $query = '
SELECT
    tbl_f_estimate.DEVISAUTO AS `id`,
    tbl_f_estimateline.DETAILSDEVISAUTO AS `line.id`,
    IF (tbl_f_estimateline.DETAILSDEVISREMISEEX > 0,
        ELT(1-tbl_f_estimateline.DETAILSDEVISREMISEOK, "' . LOC::Tariff::Rent::REDUCTIONSTATE_REFUSED . '", "' . LOC::Tariff::Rent::REDUCTIONSTATE_PENDING . '", "' . LOC::Tariff::Rent::REDUCTIONSTATE_VALIDATED . '"),
        IF (tbl_f_rentestim2.VALIDOK = 0, "' . LOC::Tariff::Rent::REDUCTIONSTATE_REFUSED . '", "")) AS `rentTariff.specialReductionStatus`,
    IFNULL(tbl_f_trspspcreduc.ETAT, "") AS `transportTariff.specialReductionStatus`,
    tbl_f_trspestim.REMISEEX AS `transportTariff.specialReductionId`,
    tbl_f_trspspcreduc.ALERTE AS `transportTariff.alertId`,
    tbl_f_estimateline.DETAILSDEVISDATECREATION AS `creationDate`,
    DATE_FORMAT(tbl_f_estimateline.DETAILSDEVISDATECREATION, "%d/%m/%Y %H:%i:%s") AS `formatCreationDate`,
    (SELECT GROUP_CONCAT(tbl_f_estimateline.DETAILSDEVISAUTO SEPARATOR ";") FROM DETAILSDEVIS tbl_f_estimateline WHERE tbl_f_estimateline.DEVISAUTO = tbl_f_estimate.DEVISAUTO) AS `linesId`
FROM DEVIS tbl_f_estimate
LEFT JOIN DETAILSDEVIS tbl_f_estimateline
ON tbl_f_estimate.DEVISAUTO = tbl_f_estimateline.DEVISAUTO
LEFT JOIN GESTIONTARIF' . $countryId . '.GC_ARCHIVES tbl_f_rentestim2
ON tbl_f_rentestim2.DETAILSDEVISAUTO = tbl_f_estimateline.DETAILSDEVISAUTO
LEFT JOIN GESTIONTRANS' . $countryId . '.TRANSPORT tbl_f_trspestim
ON tbl_f_trspestim.TYPE = "DEVIS" AND
   tbl_f_trspestim.VALEUR = tbl_f_estimateline.DETAILSDEVISAUTO AND
   tbl_f_trspestim.ETAT <> "' . LOC::Tariff::Transport::REDUCTIONSTATE_CANCELED . '"
LEFT JOIN GESTIONTRANS' . $countryId . '.REMISEEX tbl_f_trspspcreduc
ON tbl_f_trspspcreduc.ID = tbl_f_trspestim.REMISEEX
WHERE tbl_f_estimate.ETCODE IN (' . $db->quote(@tabStates) . ')
    AND tbl_f_estimateline.DETAILSDEVISACCEPTE = 0
    AND DATE(tbl_f_estimateline.DETAILSDEVISDATECREATION) < ' . $db->quote($validDate) . '
    AND tbl_f_estimateline.DETAILSDEVISOK = 0
    AND !((tbl_f_estimateline.DETAILSDEVISPROFORMAOK & ' . LOC::Proforma::DOCFLAG_EXISTACTIVED . ') AND !(tbl_f_estimateline.DETAILSDEVISPROFORMAOK & ' . LOC::Proforma::DOCFLAG_EXISTACTIVEDEXPIRED . '))
ORDER BY tbl_f_estimate.DEVISAUTO;';
        my $tabEstimateLines = $db->fetchAssoc($query, {}, 'line.id');

        foreach my $tabLine (values %$tabEstimateLines)
        {
            # Données
            my $id                 = &LOC::Util::toNumber($tabLine->{'id'});
            my $lineId             = &LOC::Util::toNumber($tabLine->{'line.id'});
            my $creationDate       = $tabLine->{'creationDate'};
            my $formatCreationDate = $tabLine->{'formatCreationDate'};
            my @tabLinesId         = split(/;/, $tabLine->{'linesId'});
            my $lineIndex = &getLineIndex($lineId, @tabLinesId);

            # HTML
            my $url = &LOC::Request::createRequestUri('rent:rentEstimate:view', {'estimateId' => $id, 'estimateLineId' => $lineId});
            $html{$countryId} .= '
<tr class="lines">
<td><a href="' . $url . '" target="_blank">' . $id . ' #' . $lineIndex . '</a></th>
<td>' . $formatCreationDate . '</td>';


            $tabUpdatedEstimateLines{$countryId}++;

            # Mise à jour de la ligne de devis
            my $tabUpdates = {};
            my $tabWheres  = {};
            if (!&LOC::Estimate::Rent::updateLine($countryId, $lineId, {'@action' => 'update',
                                                                        '@subAction' => 'deactivate'}, 1))
            {
                if (!$hasTransaction)
                {
                    # Annule la transaction
                    $db->rollBack();

                    $hasErrors = 1;

                    # HTML
                    $html{$countryId} .= '
<td class="error">Problème de mise à jour des informations de la ligne</td>';

                    last;
                }
            }
            else
            {
                # Bloquage des transports
                &LOC::Transport::lock($countryId, {'from-to' => {'type' => LOC::Transport::FROMTOTYPE_ESTIMATELINE,
                                                                 'id'   => $lineId}}, 1);

                # Validation de la RE de location
                if ($tabLine->{'rentTariff.specialReductionStatus'} eq LOC::Tariff::Rent::REDUCTIONSTATE_PENDING)
                {
                    # DETAILSDEVIS
                    $tabUpdates = {'DETAILSDEVISREMISEOK' => -2};
                    $tabWheres  = {'DETAILSDEVISAUTO' => $lineId};
                    if ($db->update('DETAILSDEVIS', $tabUpdates, $tabWheres) == -1)
                    {
                        # Annule la transaction
                        $db->rollBack();

                        $hasErrors = 1;

                        # HTML
                        $html{$countryId} .= '
<td class="error">Problème de validation de la RE de location</td>';

                        last;
                    }
                    # GC_ARCHIVES
                    my $query = '
INSERT INTO GESTIONTARIF' . $countryId . '.GC_ARCHIVES
(ALAUTO, ALERTEDATE, ALDATEVAL, ALMOTIF, AGAUTO, PEAUTO, CLAUTO, VPEAUTO, DETAILSDEVISAUTO, CONTRATAUTO, REMISEREQ,
    REMISEACC, ALLIBELLE, VALIDOK, LIBALAUTO, PEAUTOTRAITEMENT)
SELECT ALAUTO, ALDATE, NOW(), ALMOTIF, AGAUTO, PEAUTO, CLAUTO, 0, DETAILSDEVISAUTO, CONTRATAUTO, REMISEREQ,
    REMISEACC, ALLIBELLE, 1, ' . $db->quote($rentLabelId) . ', PEAUTOTRAITEMENT
FROM GESTIONTARIF' . $countryId . '.GC_ALERTES
WHERE DETAILSDEVISAUTO = ' . $lineId;
                    if ($db->do($query) == -1)
                    {
                        # Annule la transaction
                        $db->rollBack();

                        $hasErrors = 1;

                        # HTML
                        $html{$countryId} .= '
<td class="error">Problème de validation de la RE de location</td>';

                        last;
                    }
                    # GC_ALERTES
                    $tabWheres  = {'DETAILSDEVISAUTO' => $lineId};
                    if ($db->delete('GC_ALERTES', $tabWheres, 'GESTIONTARIF' . $countryId) == -1)
                    {
                        # Annule la transaction
                        $db->rollBack();

                        $hasErrors = 1;

                        # HTML
                        $html{$countryId} .= '
<td class="error">Problème de validation de la RE de location</td>';

                        last;
                    }
                }

                # Validation de la RE de transport
                if ($tabLine->{'transportTariff.specialReductionStatus'} eq LOC::Tariff::Transport::REDUCTIONSTATE_PENDING)
                {
                    $tabUpdates = {'REMEXACC*' => 'REMEXDEM',
                                   'ETAT'      => LOC::Tariff::Transport::REDUCTIONSTATE_VALIDATED,
                                   'LIBMOTIF'  => $tspLabelId};
                    $tabWheres  = {'ID' => $tabLine->{'transportTariff.specialReductionId'}};
                    if ($db->update('REMISEEX', $tabUpdates, $tabWheres, 'GESTIONTRANS' . $countryId) == -1)
                    {
                        # Annule la transaction
                        $db->rollBack();

                        $hasErrors = 1;

                        # HTML
                        $html{$countryId} .= '
<td class="error">Problème de validation de la RE de transport</td>';

                        last;
                    }

                    # Suppression de l'alerte s'il n'y a plus de RE en attente
                    my $query = '
SELECT COUNT(*)
FROM GESTIONTRANS' . $countryId . '.REMISEEX
WHERE ALERTE = ' . $tabLine->{'transportTariff.alertId'} . ' AND ETAT = "REM02"';
                    my $nbPendingSpecialReductions = $db->fetchOne($query);

                    if ($nbPendingSpecialReductions == 0)
                    {
                        $tabUpdates = {'ETAT' => 'ALE02'};
                        $tabWheres  = {'ID' => $tabLine->{'transportTariff.alertId'}};
                        if ($db->update('ALERTE', $tabUpdates, $tabWheres, 'GESTIONTRANS' . $countryId) == -1)
                        {
                            # Annule la transaction
                            $db->rollBack();

                            $hasErrors = 1;

                            # HTML
                            $html{$countryId} .= '
<td class="error">Problème de validation de la RE de transport</td>';

                            last;
                        }
                    }
                }

                # Mise à jour du montant total du devis
                if (!&LOC::Estimate::Rent::_updateAmounts($db, $countryId, $id, 0))
                {
                    # Annule la transaction
                    $db->rollBack();

                    $hasErrors = 1;

                    # HTML
                    $html{$countryId} .= '
<td class="error">Problème de mise à jour du montant total du devis</td>';

                    last;
                }

                # HTML
                $html{$countryId} .= '
<td>Annulée</td>';
            }

            # HTML
            $html{$countryId} .= '
</tr>';
        }

        # Valide la transaction
        if (!$hasTransaction)
        {
            if (!$db->commit())
            {
                $db->rollBack();

                $hasErrors = 1;
            }
        }

        # Fin du HTML
        $html{$countryId} .= '
</table>';
    }

    # HTML
    my $html = &getMailBody(\%tabUpdatedEstimateLines, \%html);
    &sendMail($html, $hasErrors);
}



# Function: getStates
# Récupère la liste des états concernés par la mise à jour
#
# Returns:
# array - Liste des états
sub getStates
{
    return (
            LOC::Estimate::Rent::STATE_ACTIVE,
            LOC::Estimate::Rent::STATE_CONTRACT,
            LOC::Estimate::Rent::STATE_ABORTED,
           );
}

# Function: getCountries
# Récupère la liste des pays à mettre à jour
#
# Returns:
# array - Liste des pays à mettre à jour
sub getCountries
{
    my @tabResult = ();
    my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_PAIRS);

    foreach my $countryId (keys(%$tabCountries))
    {
        my $charac = &LOC::Characteristic::getCountryValueByCode('YIELDLOC', $countryId);
        if ($charac == 1)
        {
            push(@tabResult, $countryId);
        }
    }

    return @tabResult;
}

# Function: getLineIndex
# Retourne le numéro de la ligne de devis
#
# Parameters:
# int   $lineId     - Identifiant de la ligne de devis
# array @tabLinesId - Liste des identifiants des lignes du devis
#
# Returns:
# int - Numéro de la ligne de devis
sub getLineIndex
{
    my ($lineId, @tabLinesId) = @_;

    my $index = undef;
    for (my $i = 0; $i < @tabLinesId; $i++)
    {
        if ($tabLinesId[$i] == $lineId)
        {
            $index = $i + 1;
            last;
        }
    }

    return $index;
}

# Function: getMailBody
# Recalcule le prix après remise standard et le prix final
#
# Parameters:
# hashref $tabUpdatedEstimateLines - Tableau contenant le nombre de lignes de devis mises à jour par pays
# hashref $tabHtml                 - Tableau contenant le code HTML des mises à jour effectuées par pays
#
# Returns:
# string - Code HTML
sub getMailBody
{
    my ($tabUpdatedEstimateLines, $tabHtml) = @_;

    # Liste des pays
    my @tabCountries = sort(keys(%$tabUpdatedEstimateLines));

    my $html = '
<style>
body
{
    font-family: Verdana, Arial, Helvetica, Sans-serif;
    font-size: 8pt;
}

h1
{
    font-family: "Segoe UI", Arial, Helvetica, Sans-serif;
    font-size: 11pt;
}

a:link, a:active, a:visited
{
    color: #669ACB;
    text-decoration: none;
    border: none;
}
a:hover
{
    text-decoration: underline;
    border-bottom: 1px solid #BFDDEF;
    color: #007AC2;
    border: none;
}

table
{
    font-size: 8pt;
    border-top: 1px solid #66AFDA;
    border-bottom: 1px solid #007AC2;
    border-left: 1px solid #66AFDA;
    border-right: 1px solid #007AC2;
    vertical-align: middle;
    text-align: left;
    border-spacing: 0px;
    border-collapse: collapse;
}
table:hover
{
    border-color: #007AC2;
}

th
{
    color: black;
    font-weight: normal;
    font-style: italic;
    margin: 0;
    padding: 3px 5px 4px 5px;
    background-color: #E8F3F9;
    white-space: nowrap;
    text-align: left;
}

td
{
    border-bottom: 1px solid #CCE4F2;
    padding: 0 5px 0 5px;
}

tr.lines td
{
    font-size: 7pt;
}

tr.lines td.left, tr.lines td.right
{
    white-space: nowrap;
}

tr.lines td.left
{
    text-align: left;
}

tr.lines td.right
{
    text-align: right;
}
</style>

<p>Nombre de lignes de devis mises à jour :</p>
<table>
<tr>';
    foreach my $countryId (@tabCountries)
    {
        $html .= '
<th>' . $countryId . '</th>';
    }
    $html .= '
</tr>
<tr>';
    foreach my $countryId (@tabCountries)
    {
        $html .= '
<td style="text-align: right">' . $tabUpdatedEstimateLines->{$countryId} . '</td>';
    }
    $html .= '
</tr>
</table>
';
    foreach my $countryId (@tabCountries)
    {
        if ($tabUpdatedEstimateLines->{$countryId} > 0)
        {
            $html .= '<h1>' . $countryId . '</h1>';
            $html .= $tabHtml->{$countryId};
        }
    }

    return $html;
}

# Function: sendMail
# Envoi l'e-mail
#
# Parameters:
# string $html      - Code HTML du corps de l'e-mail
# bool   $hasErrors - Indicateur d'erreurs (0 : pas d'erreur, 1 : il y a des erreurs)
sub sendMail
{
    my ($html, $hasErrors) = @_;

    # Mail
    my $mailObj = LOC::Mail->new();

    # Encodage
    $mailObj->setEncodingType(LOC::Mail::ENCODING_TYPE_UTF8);

    # Expéditeurs
    my $mailsSender = &LOC::Globals::get('mailsSender');
    $mailObj->setSender(%$mailsSender);

    # Corps
    $mailObj->setBodyType(LOC::Mail::BODY_TYPE_HTML);
    $mailObj->setBody($html);

    # Objet
    my $subject = 'Mise à jour des devis de location';
    if ($hasErrors)
    {
        $subject .= ' (ERREUR)';
    }
    $mailObj->setSubject($subject);

    # Priorité
    if ($hasErrors)
    {
        $mailObj->setMailPriority(LOC::Mail::MAIL_PRIORITY_HIGH);
    }

    # Destinataires
    my %tabRecipients = ('archive.dev@acces-industrie.com' => 'Développeurs');
    $mailObj->addRecipient(%tabRecipients);
    $mailObj->send();

    my $errors = $mailObj->getErrors();
    if ($errors)
    {
        print STDERR $errors;
    }
}
