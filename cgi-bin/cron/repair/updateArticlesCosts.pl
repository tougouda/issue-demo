#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../../inc', '../../lib';

# Traitement des variables d'environnement
use LOC::Env;

# Modules internes
use LOC::Globals;
use LOC::Repair::Article;

print "Content-type: text/plain; charset=UTF-8\n\n";

my @tabCountry = ('FR', 'ES', 'PT', 'MA');

foreach my $countryId (@tabCountry)
{
    my $return = &LOC::Repair::Article::updateAllCosts($countryId);
    if ($return != 1)
    {
        die ('Erreur');
    }
}
