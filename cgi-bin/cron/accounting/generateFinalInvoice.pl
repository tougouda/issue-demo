#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../../inc', '../../lib';

# Traitement des variables d'environnement
use LOC::Env;

# Modules internes
use LOC::Agency;
use LOC::Contract::Rent;
use LOC::Globals;
use LOC::Log;
use LOC::Session;

# Modules externes
use SOAP::Lite;


&LOC::Session::load();
my $tabUserInfos = &LOC::Session::getUserInfos();

sub SOAP::Transport::HTTP::Client::get_basic_credentials
{
    return $tabUserInfos->{'login'} => $tabUserInfos->{'password'};
}


# Log
my $log = 'Début de la génération des factures finales manquantes -----------------------------------------------------------------' . "\n";
&LOC::Log::writeFileLog('invoice', $log);

foreach my $countryId ('FR', 'ES', 'PT', 'MA')
{
    # Log
    $log = '## PAYS : ' . $countryId . "\n";
    &LOC::Log::writeFileLog('invoice', $log);

    # Connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Passage de l'utilisteur dans le pays concerné
    my $tabAgencies = &LOC::Agency::getList(LOC::Util::GETLIST_PAIRS, {'country' => $countryId});
    my @tabAgencyId = keys(%$tabAgencies);
    $db->update('PAPE', {'PACODE' => $countryId}, {'PEAUTO' => $tabUserInfos->{'id'}}, 'AUTH');
    $db->update('PERSONNEL', {'AGAUTO' => $tabAgencyId[0]}, {'PEAUTO' => $tabUserInfos->{'id'}}, 'AUTH');

    # Y'a-t-il une transaction en cours ?
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Récupération de la locale par défaut du pays courant
    my $query = '
SELECT
    cty_lcl_id
FROM frmwrk.p_country
WHERE cty_id = ' . $db->quote($countryId);
    my $localeId = $db->fetchOne($query);

    # Locale
    my $locale = new LOC::Locale($localeId);
    &POSIX::setlocale(&POSIX::LC_ALL, 'C');

    # Récupération des contrats pour lesquels il faut générer une facture finale
    my $query = '
SELECT
    tbl_f_rentcontract.CONTRATAUTO AS `id`,
    tbl_f_rentcontract.CONTRATCODE AS `code`
FROM CONTRAT tbl_f_rentcontract
WHERE tbl_f_rentcontract.SOLOAUTO = 0
    AND tbl_f_rentcontract.ETCODE = ' . $db->quote(LOC::Contract::Rent::STATE_BILLED) . '
    AND tbl_f_rentcontract.ETATFACTURE != ' . $db->quote(LOC::Contract::Rent::INVOICESTATE_FINAL) . ';';
    my $tabContracts = $db->fetchPairs($query);

    # Génération des factures finales
    foreach my $id (keys(%$tabContracts))
    {
        printf('Génération de la facture finale du contrat %s           ' . "\r", $tabContracts->{$id});

        # Log
        $log = $tabContracts->{$id} . ' ';
        &LOC::Log::writeFileLog('invoice', $log);

        # Récupération des informations du contrat
        my $tabContractInfos = &LOC::Contract::Rent::getInfos($countryId, $id);

        my $hasErrors = 0;

        # Mise à jour du numéro de téléphone
        if (&updatePhone($countryId, $locale, $id) == 0)
        {
            if (!$hasTransaction)
            {
                # Annule la transaction
                $db->rollBack();
            }

            $hasErrors = 1;
        }

        # Génération de la facture
        if (!$hasErrors && &LOC::Contract::Rent::_generateFinalInvoice($countryId, $id, $tabContractInfos) == 0)
        {
            if (!$hasTransaction)
            {
                # Annule la transaction
                $db->rollBack();
            }

            $hasErrors = 1;
        }

        if ($hasErrors)
        {
            print '[ERR]' . "\n";

            # Log
            $log = '[ERR]' . "\n";
            &LOC::Log::writeFileLog('invoice', $log);
        }
        else
        {
            printf('Génération de la facture finale du contrat %s [OK]' . "\r", $tabContracts->{$id});

            # Log
            $log = '[OK]' . "\n";
            &LOC::Log::writeFileLog('invoice', $log);
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            $hasErrors = 1;
        }
    }

    # Retour de l'utilisateur dans son agence d'origine
    $db->update('PAPE', {'PACODE' => $tabUserInfos->{'nomadCountry.id'}}, {'PEAUTO' => $tabUserInfos->{'id'}}, 'AUTH');
    $db->update('PERSONNEL', {'AGAUTO' => $tabUserInfos->{'nomadAgency.id'}}, {'PEAUTO' => $tabUserInfos->{'id'}}, 'AUTH');
}

# Log
$log = 'Fin de la génération des factures finales manquantes -------------------------------------------------------------------' . "\n";
&LOC::Log::writeFileLog('invoice', $log);



sub updatePhone
{
    my ($countryId, $locale, $contractId) = @_;

    # Connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Y'a-t-il une transaction en cours ?
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Récupération des informations du chantier
    my $query = '
SELECT
    tbl_f_site.CHAUTO AS `id`,
    tbl_f_site.CHTELEPHONE AS `sitePhone`,
    tbl_f_rentcontract.CONTRATTELEPHONECOMM AS `orderPhone`
FROM CONTRAT tbl_f_rentcontract
LEFT JOIN CHANTIER tbl_f_site
ON tbl_f_rentcontract.CHAUTO = tbl_f_site.CHAUTO
WHERE tbl_f_rentcontract.CONTRATAUTO = ' . $contractId;
    my $tabPhone = $db->fetchAssoc($query, {}, 'id');

    foreach my $tabRow (values %$tabPhone)
    {
        my $sitePhone  = $locale->getPhoneNumberFormat($tabRow->{'sitePhone'});
        my $orderPhone = $locale->getPhoneNumberFormat($tabRow->{'orderPhone'});

        my $tabUpdates = {'CHTELEPHONE' => $sitePhone};
        my $tabWheres  = {'CHAUTO' => $tabRow->{'id'}};

        if ($db->update('CHANTIER', $tabUpdates, $tabWheres) == -1)
        {
            if (!$hasTransaction)
            {
                # Annule la transaction
                $db->rollBack();
            }

            return 0;
        }
        my $tabUpdates = {'CONTRATTELEPHONECOMM' => $orderPhone};
        my $tabWheres  = {'CONTRATAUTO' => $contractId};
        if ($db->update('CONTRAT', $tabUpdates, $tabWheres) == -1)
        {
            if (!$hasTransaction)
            {
                # Annule la transaction
                $db->rollBack();
            }

            return 0;
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    return 1;
}