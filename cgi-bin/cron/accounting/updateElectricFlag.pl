#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../../inc', '../../lib';

# Traitement des variables d'environnement
use LOC::Env;

use Array::Diff;
use Date::Calc;

# Modules internes
use LOC::Globals;
use LOC::Date;
use LOC::Db;

use LOC::MachinesFamily;



# Liste des énergies à utiliser
my @tabEnergies = (
    LOC::MachinesFamily::ENERGY_GAS,
    LOC::MachinesFamily::ENERGY_ELECTRIC,
    LOC::MachinesFamily::ENERGY_DIESEL,
    LOC::MachinesFamily::ENERGY_HYBRID
);


# Pour la France uniquement aujourd'hui
my $countryId = 'FR';


# Calcul de la période
my %tabCurrentDate = &LOC::Date::getHashDate();
my ($endYear, $endMonth, $endDay) = &Date::Calc::Add_Delta_YM($tabCurrentDate{'y'},
                                                              $tabCurrentDate{'m'},
                                                              $tabCurrentDate{'d'},
                                                              0,
                                                              -1);
my ($beginYear, $beginMonth, $beginDay) = &Date::Calc::Add_Delta_YM($tabCurrentDate{'y'},
                                                                    $tabCurrentDate{'m'},
                                                                    $tabCurrentDate{'d'},
                                                                    -1,
                                                                    0);


# connexion à AUTH
my $db = &LOC::Db::getConnection('location', $countryId);


# ------------------------------------------------------------------------------------------------
# Récupération de la liste des clients ayant fait des contrats sur les 12 derniers mois
# On calcul le chiffre d'affaires total ainsi que les chiffres d'affaires par énergie des machines
# ------------------------------------------------------------------------------------------------
my $query = '
SELECT
    icc_cus_id AS `customer.id`,
    icc_totalturnover as `amount`,
    tbl_f_machinefamily.FAMAAUTO AS `machineFamily.id`,
    tbl_f_machinefamily.FAMAENERGIE AS `machineFamily.energy`,
    tbl_f_machinefamily.FAMILLE AS `machineFamily.family`
FROM statistics.h_cash_invoicecost
LEFT JOIN CONTRAT tbl_f_rentcontract
ON tbl_f_rentcontract.CONTRATCODE = CONVERT(icc_contractcode USING latin1) AND tbl_f_rentcontract.CONTRATOK = -1
LEFT JOIN AUTH.MACHINE tbl_f_machine
ON tbl_f_machine.MAAUTO = tbl_f_rentcontract.MAAUTO
LEFT JOIN AUTH.MODELEMACHINE tbl_f_model
ON tbl_f_model.MOMAAUTO = IFNULL(tbl_f_machine.MOMAAUTO, tbl_f_rentcontract.MOMAAUTO)
LEFT JOIN FAMILLEMACHINE tbl_f_machinefamily
ON tbl_f_machinefamily.FAMAAUTO = tbl_f_model.FAMAAUTO
WHERE icc_periodtypecode = "PER12"
AND icc_beginyear = ' . $beginYear . '
AND icc_beginmonth = ' . $beginMonth . '
AND icc_endingyear = ' . $endYear . '
AND icc_endingmonth = ' . $endMonth . '
ORDER BY icc_cus_id;';

my $stmt = $db->query($query);
if ($stmt->getRowsCount() == 0)
{
    printf('La période du %02d/%04d au %02d/%04d n\'existe pas dans les statistiques. Veuillez relancer la mise à jour ultérieurement...', $beginMonth, $beginYear, $endMonth, $endYear);
    exit();
}

my $tabCustomerToFlag = [];

my $tabCustomerInfos = {'id' => 0};
while (my $tabRow = $stmt->fetch(LOC::Db::FETCH_ASSOC))
{
    if ($tabCustomerInfos->{'id'} != $tabRow->{'customer.id'})
    {
        # Vérification des totaux pour le client en cours
        if ($tabCustomerInfos->{'id'} != 0)
        {
            if ($tabCustomerInfos->{'total'} >= 30000 && $tabCustomerInfos->{'total'} < 50000)
            {
                # Calcul de la somme des chiffres d'affaires pour les machines gaz et électriques
                my $sum = $tabCustomerInfos->{'tabTotalByEnergy'}->{LOC::MachinesFamily::ENERGY_GAS} +
                          $tabCustomerInfos->{'tabTotalByEnergy'}->{LOC::MachinesFamily::ENERGY_ELECTRIC};

                # Si le ratio de cette somme sur le chiffre d'affaires total est supérieur à 0.5 alors c'est un client électrique
                if (($sum / $tabCustomerInfos->{'total'}) > 0.5)
                {
                    push(@$tabCustomerToFlag, $tabCustomerInfos->{'id'});
                }
            }
        }

        # Réinitialisation des montants pour le nouveau client
        $tabCustomerInfos = {
            'id'               => $tabRow->{'customer.id'},
            'total'            => 0,
            'tabTotalByEnergy' => {}
        };

        my $energiesCount = @tabEnergies;
        for (my $i = 0; $i < $energiesCount; $i++)
        {
            $tabCustomerInfos->{'tabTotalByEnergy'}->{$tabEnergies[$i]} = 0;
        }
    }

    # Récupération des valeurs
    my $machineFamilyId     = $tabRow->{'machineFamily.id'} * 1;
    my $machineFamilyEnergy = $tabRow->{'machineFamily.energy'};
    my $machineFamilyFamily = $tabRow->{'machineFamily.family'};
    my $amount = $tabRow->{'amount'} * 1;

    # Traitements des exceptions
    if ($countryId eq 'FR')
    {
        if ($machineFamilyId == 148 || $machineFamilyId == 149)
        {
            $machineFamilyFamily = 'MINI-PELLE';
        }
        elsif ($machineFamilyId == 150)
        {
            $machineFamilyFamily = 'CHARIOT FRONTAL';
        }

        if ($machineFamilyFamily eq 'PUSH' || $machineFamilyFamily eq 'MINI-PELLE' || $machineFamilyFamily eq 'DUMPER')
        {
            $machineFamilyEnergy = LOC::MachinesFamily::ENERGY_DIESEL;
        }
        elsif ($machineFamilyFamily eq 'FLECHE' || $machineFamilyFamily eq 'COMPACTEUR')
        {
            $machineFamilyEnergy = LOC::MachinesFamily::ENERGY_ELECTRIC;
        }
    }


    # Ajout des montants
    $tabCustomerInfos->{'total'} += $amount;
    $tabCustomerInfos->{'tabTotalByEnergy'}->{$machineFamilyEnergy} += $amount;
}


# ------------------------------------------------------------
# Récupération de la liste des clients avec le flag électrique
# ------------------------------------------------------------
my $query = '
SELECT
    tbl_f_customer.CLAUTO
FROM CLIENT tbl_f_customer
WHERE tbl_f_customer.CLUSEELECT <> 0
ORDER BY tbl_f_customer.CLAUTO;';
my $tabCustomerAlreadyFlagged = $db->fetchCol($query);



# Calcul des changements sur les flags
my $diff = Array::Diff->diff($tabCustomerAlreadyFlagged, $tabCustomerToFlag);


# ---------------------
# Mise à jour des flags
# ---------------------
my $tabAdded = $diff->added();
if (@$tabAdded > 0)
{
    $db->update('CLIENT', {'CLUSEELECT' => -1}, {'CLAUTO' => $tabAdded});
    print @$tabAdded . ' clients flaggés [' . join(', ', @$tabAdded) . ']' . "\n";
}

my $tabDeleted = $diff->deleted();
if (@$tabDeleted > 0)
{
    $db->update('CLIENT', {'CLUSEELECT' => 0},  {'CLAUTO' => $tabDeleted});
    print @$tabDeleted . ' clients déflaggés [' . join(', ', @$tabDeleted) . ']' . "\n";
}

