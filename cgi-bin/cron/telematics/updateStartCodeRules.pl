#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../../inc', '../../lib';

# Traitement des variables d'environnement
use LOC::Env;

# Modules internes
use LOC::Globals;
use LOC::StartCode;
use LOC::StartCode::Static;
use LOC::StartCode::Contract::Rent;
use LOC::Country;
use LOC::Request;
use LOC::User;
use LOC::Util;

print "Content-type: text/plain; charset=UTF-8\n\n";



# Récupération des paramètres de la ligne de commande
my @tabFunctions = @ARGV;

# Récupération des paramètres de l'URL
if (!@tabFunctions)
{
    my $get = &LOC::Request::get(undef, undef, LOC::Request::SRC_GET);
    @tabFunctions = keys(%$get);
}

# Liste des fonctions à exécuter
if (!@tabFunctions)
{
    @tabFunctions = ('clean', 'cleanLinks', 'queueExpiredRules', 'synchronizeAll');
}

# Liste des pays
my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);

my $hasErrors = 0;
foreach my $countryId (@$tabCountries)
{
    # Suppression des codes de démarrage pour les machines qui ne sont plus équipées télématique
    if (&LOC::Util::in_array('clean', \@tabFunctions) &&
        !&LOC::StartCode::clean($countryId, LOC::User::ADMIN_ID))
    {
        $hasErrors++;
    }

    # Suppression des liens avec les contrats sur des codes obsolètes
    if (&LOC::Util::in_array('cleanLinks', \@tabFunctions) &&
        !&LOC::StartCode::Contract::Rent::cleanLinks($countryId, LOC::User::ADMIN_ID))
    {
        $hasErrors++;
    }

    # Suppression des règles périmées sur les boitiers
    if (&LOC::Util::in_array('queueExpiredRules', \@tabFunctions) &&
        !&LOC::StartCode::queueExpiredRules($countryId, LOC::User::ADMIN_ID))
    {
        $hasErrors++;
    }

    # Mise à jour des codes statiques sur les machines qui ne les ont pas
    if (&LOC::Util::in_array('synchronizeAll', \@tabFunctions) &&
        !&LOC::StartCode::Static::synchronizeAll($countryId, LOC::User::ADMIN_ID))
    {
        $hasErrors++;
    }
}

if ($hasErrors > 0)
{
    die ('Erreur lors de la mise à jour des règles des codes de démarrage.');
}