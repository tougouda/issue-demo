#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../../inc', '../../lib';

# Traitement des variables d'environnement
use LOC::Env;

# Modules internes
use LOC::Globals;
use LOC::StartCode;
use LOC::StartCode::Contract::Rent;
use LOC::Country;
use LOC::User;

print "Content-type: text/plain; charset=UTF-8\n\n";



# Liste des pays
my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);

my $hasErrors = 0;
foreach my $countryId (@$tabCountries)
{
    # Mise à jour des codes statiques sur les machines qui ne les ont pas
    if (!&LOC::StartCode::Contract::Rent::freeEndedCodes($countryId, LOC::User::ADMIN_ID))
    {
        $hasErrors++;
    }
}

if ($hasErrors > 0)
{
    die ('Erreur lors de la libération des codes de démarrage.');
}