#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../../inc', '../../lib';

# Traitement des variables d'environnement
use LOC::Env;

# Modules internes
use LOC::Globals;
use LOC::Machine;

print "Content-type: text/plain; charset=UTF-8\n\n";

# Mise à jour des valeurs des équipements
my $return = &LOC::Machine::updateAdditionalInfosValues();
if ($return != 1)
{
    die ('Erreur lors de la mise à jour des équipements.');
}