#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../inc', '../lib';

# Traitement des variables d'environnement
use LOC::Env;

use LOC::TransportRequest;
use LOC::Util;

my ($requestId, $countryId) = @ARGV;

my $errors = '';
while (my $stdin = <STDIN>)
{
    chomp($stdin);
    if ($stdin ne '')
    {
        $errors .= $stdin . "\n";
    }
}

# Mise à jour des erreurs en base
if ($errors ne '')
{
    &LOC::TransportRequest::setErrors($countryId, $requestId, $errors);
    &LOC::TransportRequest::setState($countryId, $requestId, LOC::TransportRequest::STATE_FAILED);
}

# Suppression du fichier de log s'il est vide
my $logPath = &LOC::Util::getCommonFilesPath('logs/transport/');

my $command = 'find ' . $logPath . '/* -empty -exec rm {} \;';

system($command);
