#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../inc', '../lib';

# Traitement des variables d'environnement
use LOC::Env;

use LOC::InvoicingRequest;

my ($requestId, $countryId) = @ARGV;

my $errors = '';
while (my $stdin = <STDIN>)
{
    chomp($stdin);
    if ($stdin ne '')
    {
        $errors .= $stdin . "\n";
    }
}

# Mise à jour des erreurs en base
if ($errors ne '')
{
    &LOC::InvoicingRequest::setErrors($countryId, $requestId, $errors);
    &LOC::InvoicingRequest::setState($countryId, $requestId, LOC::InvoicingRequest::STATE_FAILED);
}

# Suppression du fichier de log s'il est vide
my $logPath = &LOC::Util::getCommonFilesPath('logs/invoice/');

my $command = 'find ' . $logPath . '/* -empty -exec rm {} \;';

system($command);
