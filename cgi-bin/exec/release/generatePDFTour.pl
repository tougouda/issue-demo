#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../../inc', '../../lib';

# Traitement des variables d'environnement
use LOC::Env;

use Data::Dumper;

# Modules internes
use LOC::Globals;
use LOC::Db;
use LOC::Timer;
use LOC::Util;
use LOC::Date;


# # # # # #
#
# Récupération des PDF des tournées terminées non modifiables
#
# # # # # #


my @start = &LOC::Timer::start();


# Arguments
# - 0 : countryId
# - 1 : nombre de fichiers à générer
# - 2 : génération finale pour les dernières tournées modifiables
# - 3 : login
# - 4 : password

my ($countryId, $nbFilesToGen, $isFinal, $login, $password) = @ARGV;

my $env = &LOC::Globals::get('env');
my $rootFolder = $ENV{'WEBDATA_HOME'} . '/location/tour';

if ($env ne 'exp')
{
    $rootFolder = '/mnt/nas/paulo';
}

my $logError   = &LOC::Util::getCommonFilesPath('logs/transport/');
my $printUrl   = &LOC::Globals::get('printUrl');
my $dev        = (&LOC::Globals::get('dev') ne '' ? '&dev=' . &LOC::Globals::get('dev') : '');

# Fichier d'erreur
open(FILEERROR, '>>' . $logError . 'errorPDFTour.txt');

# Entête
print "\n";
print FILEERROR '### ' . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . "\n";
print FILEERROR '## ' . $countryId . " ##\n";

# Connexion à la base location
my $db = &LOC::Db::getConnection('location', $countryId);
my $schemaName = &LOC::Db::getSchemaName($countryId);

# Changement de pays pour l'utilisateur
my $query = '
SELECT
    P.PEAUTO AS `id`,
    PACODE AS `countryId`
FROM AUTH.PAPE PP
INNER JOIN AUTH.PERSONNEL P ON (P.PEAUTO = PP.PEAUTO
                                AND P.PELOGIN = "' . $login . '"
                                AND P.PEPASSWORD = "' . $password . '");';
my $oldUserInfos = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
$db->update('PAPE', {'PACODE' => $countryId}, {'PEAUTO' => $oldUserInfos->{'id'}}, 'AUTH');


my $selectTour = '
SELECT T.TOAUTO
FROM TOURNEE T
INNER JOIN CONTRATTOURNEE CT ON CT.TOAUTO = T.TOAUTO
INNER JOIN CONTRAT C ON C.CONTRATAUTO = CT.CONTRATAUTO
INNER JOIN CONTRAT C2 ON C.CONTRATCODE = C2.CONTRATCODE AND C2.CONTRATOK = -1
INNER JOIN AUTH.MACHINE M ON M.MAAUTO = C.MAAUTO

WHERE
  TOOK = -1 AND TOVALIDATION = -1';

if (!$isFinal)
{
    $selectTour .='
  AND
  (
    C.CONTRATOK = 0
    OR C2.ETCODE IN ("CON06", "CON07")
    OR M.CONTRATAUTO != C.CONTRATAUTO
  )';
}

$selectTour .= '
GROUP BY T.TOAUTO
ORDER BY T.TOAUTO';

my @tabTour = $db->fetchCol($selectTour);

my @start = &LOC::Timer::start();
my $cpt = 0;
foreach my $toauto (@tabTour)
{
    if ($cpt == $nbFilesToGen)
    {
        last;
    }
#    my @start = &LOC::Timer::start();
    my $stringToauto = sprintf('%06d', $toauto);
    my $tabOldState = {};
    my $query = '
SELECT
    CONTRATTOURNEEAUTO,
    CONTRATAUTO,
    COTOVALIDATION,
    COTODATELIVRAISON
FROM CONTRATTOURNEE
WHERE TOAUTO = ' . $toauto;
    my $tabContractsInfos = $db->fetchAll($query);


    if (@$tabContractsInfos > 0 && !(-e $rootFolder . '/' . $countryId . '_' . $stringToauto . '.pdf'))
    {
        $cpt++;
        my $count = @$tabContractsInfos;
        my @tabContractsId;

        #sauvegarde des états des lignes de tournée
        foreach my $contractInfo (@$tabContractsInfos)
        {
            $tabOldState->{$contractInfo->{'CONTRATTOURNEEAUTO'}} = {
                'contratauto'       => $contractInfo->{'CONTRATAUTO'},
                'cotovalidation'    => $contractInfo->{'COTOVALIDATION'}, 
                'cotodatelivraison' => $contractInfo->{'COTODATELIVRAISON'}
            };
            push(@tabContractsId, $contractInfo->{'CONTRATAUTO'});
        }

        # remise en cours du pointage pour chaque ligne
        my $result = $db->update('CONTRATTOURNEE', {'COTOVALIDATION' => 0, 'COTODATELIVRAISON' => undef}, {'TOAUTO' => $toauto}, $schemaName);
        if ($result == -1)
        {
            print FILEERROR 'erreur lors de la remise en cours pour TOAUTO = ' . $toauto . "\n";
        }
        else
        {
            # affichage liens
            my $contractsId = join(',', @tabContractsId);
            system 'wget -q --http-user=' . $login . ' --http-passwd=' . $password . ' -O ' . $rootFolder . '/' . $countryId . '_' . $stringToauto . '.pdf "' . $printUrl . '/c/impression.php?num=' . $contractsId . '&type=tournee&action=1' . $dev . '"';

            # remise des pointages sur les lignes de tournée
            foreach my $key (keys %$tabOldState)
            {
                my $result = $db->update('CONTRATTOURNEE', {'COTOVALIDATION' => $tabOldState->{$key}->{'cotovalidation'}, 
                                                'COTODATELIVRAISON' => $tabOldState->{$key}->{'cotodatelivraison'}}, 
                                               {'CONTRATTOURNEEAUTO' => $key}, 
                                               $schemaName);
                if ($result == -1)
                {
                    print FILEERROR 'erreur lors de la remise du pointage d\'origine sur la ligne de tournée : ';
                    print FILEERROR 'CONTRATTOURNEEAUTO = ' . $key;
                    print FILEERROR ' - CONTRATAUTO = ' . $tabOldState->{$key}->{'contratauto'};
                    print FILEERROR ' - COTOVALIDATION = ' . $tabOldState->{$key}->{'cotovalidation'};
                    print FILEERROR ' - COTODATELIVRAISON = ' . $tabOldState->{$key}->{'cotodatelivraison'} . "\n";
                }
            }
        }
    }
#    my $end = &LOC::Timer::end(@start);
#    print "\nTemps écoulé pour traitement d'une tournee : " . $end . 's';
}

# Remise du pays d'origine à l'utilisateur
$db->update('PAPE', {'PACODE' => $oldUserInfos->{'countryId'}}, {'PEAUTO' => $oldUserInfos->{'id'}}, 'AUTH');


print FILEERROR "\n";
close(FILEERROR);
print "\n" . $cpt . ' tournées imprimées';

my $end = &LOC::Timer::end(@start);
print "\nTemps écoulé : " . $end . 's';
