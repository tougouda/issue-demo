#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../inc', '../lib';

# Traitement des variables d'environnement
use LOC::Env;

use LOC::Globals;
use LOC::Db;
use LOC::Request;
use LOC::Country;
use LOC::Contract::Rent;
use LOC::Day;
use LOC::Day::InvoiceDuration;
use LOC::Timer;
use LOC::User;

use CGI::Carp qw(fatalsToBrowser);
$| = 1;

my @start = &LOC::Timer::start();

print "Content-type: text/plain; charset=UTF-8\n\n";
print 'Début de la mise à jour : ' . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . "\n\n";

if (&LOC::Globals::getEnv() =~ /^dev/)
{
    my $dev = &LOC::Globals::get('dev');
    print "Base utilisée : DB$dev\n";
}

# Récupération des paramètres
my $year = &LOC::Request::getInteger('year', undef, LOC::Request::SRC_GET);

# Fichier de log
my $commonFilesPath = &LOC::Util::getCommonFilesPath('logs/release/');
my $fileName = (defined $year ? 'migrateDays_' . $year . '.txt' : 'migrateDays.txt');
open(FILE, '>' . $commonFilesPath . $fileName);
print FILE '### Début de la mise à jour : ' . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . "\n\n";
print FILE "-----\n\n";

# # # # # #
#
# Migration des jours+/-
#
# # # # # #
my $tabErrors = {'fatal' => []};

my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);

foreach my $countryId (@$tabCountries)
{
    print FILE '### ' . $countryId . " ###\n";

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    $db->beginTransaction();

    # - Durées possibles de facturation
    my $tabInvoiceDurationsList = &LOC::Day::InvoiceDuration::getList($countryId, LOC::Util::GETLIST_ASSOC);
    my $tabInvoiceDurations = {};
    foreach my $tabInvoiceDurationInfos (values(%$tabInvoiceDurationsList))
    {
        if ($tabInvoiceDurationInfos->{'duration'} == LOC::Day::InvoiceDuration::DURATION_HALFDAY)
        {
            $tabInvoiceDurations->{'halfDay'} = $tabInvoiceDurationInfos->{'id'};
        }
        elsif ($tabInvoiceDurationInfos->{'duration'} == LOC::Day::InvoiceDuration::DURATION_DAY)
        {
            $tabInvoiceDurations->{'day'} = $tabInvoiceDurationInfos->{'id'};
        }
        elsif ($tabInvoiceDurationInfos->{'duration'} == LOC::Day::InvoiceDuration::DURATION_DOUBLEDAY)
        {
            $tabInvoiceDurations->{'doubleDay'} = $tabInvoiceDurationInfos->{'id'};
        }
    }

    # - Contrats
    my $query = '
SELECT
    CONTRATAUTO AS `id`,
    CONTRATCODE AS `code`,
    CONTRATJOURSPLUS AS `additionalDaysUnbilled`,
    CONTRATJOURSMOINS AS `deductedDaysUnbilled`,
    CONTRATJOURPLUSFACT AS `additionalDaysBilled`,
    CONTRATJOURMOINSFACT AS `deductedDaysBilled`,
    CONTRATDATECLOTURE AS `closureDate`,
    AGAUTO AS `agencyId`
FROM CONTRAT
WHERE (CONTRATJOURSPLUS > 0 OR CONTRATJOURSMOINS > 0 OR CONTRATJOURPLUSFACT > 0 OR CONTRATJOURMOINSFACT > 0)';
    if (defined $year)
    {
        $query .= ' AND YEAR(CONTRATDATECREATION) = ' . $year;
    }
    $query .= ';';

    my $stmt = $db->query($query);
    while (my $tabContractInfos = $stmt->fetch(LOC::Db::FETCH_ASSOC))
    {
        my $additionalDaysUnbilled = $tabContractInfos->{'additionalDaysUnbilled'};
        my $deductedDaysUnbilled   = $tabContractInfos->{'deductedDaysUnbilled'};
        my $additionalDaysBilled   = $tabContractInfos->{'additionalDaysBilled'};
        my $deductedDaysBilled     = $tabContractInfos->{'deductedDaysBilled'};
        my $agencyId               = $tabContractInfos->{'agencyId'};

        # Récupération des informations sur les factures contenant des J+/J-
        my $tabInvoicesData = &getInvoicesData($db, $tabContractInfos->{'id'});

        # - Jour + à compléter
        &insertDays(
            $db,
            $countryId,
            $agencyId,
            $additionalDaysUnbilled,
            LOC::Day::TYPE_ADDITIONAL,
            1,
            0,
            undef,
            $tabInvoiceDurations,
            'contract',
            $tabContractInfos->{'id'}
        );

        # - Jour + facturés
        my $number = 0;
        foreach my $invoicingDate (keys %{$tabInvoicesData->{LOC::Day::TYPE_ADDITIONAL}})
        {
            &insertDays(
                $db,
                $countryId,
                $agencyId,
                $tabInvoicesData->{LOC::Day::TYPE_ADDITIONAL}->{$invoicingDate},
                LOC::Day::TYPE_ADDITIONAL,
                1,
                1,
                $invoicingDate,
                $tabInvoiceDurations,
                'contract',
                $tabContractInfos->{'id'}
            );
            $number += $tabInvoicesData->{LOC::Day::TYPE_ADDITIONAL}->{$invoicingDate};
        }
        # S'il reste des jours facturés à insérer, ils le sont avec la date de clôture du contrat
        &insertDays(
            $db,
            $countryId,
            $agencyId,
            $additionalDaysBilled - $number,
            LOC::Day::TYPE_ADDITIONAL,
            1,
            1,
            $tabContractInfos->{'closureDate'},
            $tabInvoiceDurations,
            'contract',
            $tabContractInfos->{'id'}
        );

        # - Jour - à compléter
        &insertDays(
            $db,
            $countryId,
            $agencyId,
            $deductedDaysUnbilled,
            LOC::Day::TYPE_DEDUCTED,
            1,
            0,
            undef,
            $tabInvoiceDurations,
            'contract',
            $tabContractInfos->{'id'}
        );

        # - Jour - facturé
        my $number = 0;
        foreach my $invoicingDate (keys %{$tabInvoicesData->{LOC::Day::TYPE_DEDUCTED}})
        {
            &insertDays(
                $db,
                $countryId,
                $agencyId,
                $tabInvoicesData->{LOC::Day::TYPE_DEDUCTED}->{$invoicingDate},
                LOC::Day::TYPE_DEDUCTED,
                1,
                1,
                $invoicingDate,
                $tabInvoiceDurations,
                'contract',
                $tabContractInfos->{'id'}
            );
            $number += $tabInvoicesData->{LOC::Day::TYPE_DEDUCTED}->{$invoicingDate};
        }
        # S'il reste des jours facturés à insérer, ils le sont avec la date de clôture du contrat
        &insertDays(
            $db,
            $countryId,
            $agencyId,
            $deductedDaysBilled - $number,
            LOC::Day::TYPE_DEDUCTED,
            1,
            1,
            $tabContractInfos->{'closureDate'},
            $tabInvoiceDurations,
            'contract',
            $tabContractInfos->{'id'}
        );
    }


    # - Devis
    my $query = '
SELECT
    DETAILSDEVISAUTO AS `id`,
    DETAILSDEVISJOURSPLUS AS `additionalDays`,
    DETAILSDEVISJOURSMOINS AS `deductedDays`,
    AGAUTO AS `agencyId`
FROM DETAILSDEVIS tbl_f_estimateline
INNER JOIN DEVIS tbl_f_estimate ON tbl_f_estimate.DEVISAUTO = tbl_f_estimateline.DEVISAUTO
WHERE (DETAILSDEVISJOURSPLUS > 0 OR DETAILSDEVISJOURSMOINS > 0)';
    if (defined $year)
    {
        $query .= ' AND YEAR(DEVISDATECREATION) = ' . $year;
    }
    $query .= ';';

    my $stmt = $db->query($query);

    while (my $tabEstimateLineInfos = $stmt->fetch(LOC::Db::FETCH_ASSOC))
    {
        my $additionalDaysUnbilled = $tabEstimateLineInfos->{'additionalDays'};
        my $deductedDaysUnbilled   = $tabEstimateLineInfos->{'deductedDays'};
        my $agencyId               = $tabEstimateLineInfos->{'agencyId'};

        # - Jour + à compléter
        &insertDays($db, $countryId, $agencyId, $additionalDaysUnbilled, LOC::Day::TYPE_ADDITIONAL, 1, 0, undef, $tabInvoiceDurations, 'estimateLine', $tabEstimateLineInfos->{'id'});

        # - Jour - à compléter
        &insertDays($db, $countryId, $agencyId, $deductedDaysUnbilled, LOC::Day::TYPE_DEDUCTED, 1, 0, undef, $tabInvoiceDurations, 'estimateLine', $tabEstimateLineInfos->{'id'});
    }

    # Valide la transaction
    if (!$db->commit())
    {
        # Erreur fatale !
        push(@{$tabErrors->{'fatal'}}, 'Erreur lors du commit de la transaction');
    }
}

print FILE join("\n", @{$tabErrors->{'fatal'}});


my $end = &LOC::Timer::end(@start);
print "\n\nFin de la mise à jour : " . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . " (Temps écoulé : " . $end . "s).\n\n";
print FILE "### Fin de la mise à jour : " . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . " (Temps écoulé : " . $end . "s).";
close(FILE);

# Function: addError
#
sub addError
{
    my ($countryId, $entity, $entityId) = @_;

    # Affichage de l'erreur !
    my $fatalErrMsg = 'Erreur fatale : ' . $countryId . ' - ' . $entity . ' : ' . $entityId . "\n";
    push(@{$tabErrors->{'fatal'}}, $fatalErrMsg);
}

# Function: getInvoicesData
sub getInvoicesData
{
    my ($db, $contractId) = @_;

    my $query = '
SELECT
    EF.ENTETEDATEFAC AS `invoicingDate`,
    LF.LIGNEREFARTICLE AS `article`,
    SUM(IF(LF.LIGNEREFARTICLE = "J-", -LF.LIGNEQTITE, LF.LIGNEQTITE)) AS `count`
FROM ENTETEFACTURE EF
INNER JOIN LIGNEFACTURE LF
ON EF.ENTETEAUTO = LF.ENTETEAUTO
WHERE
    EF.CONTRATAUTO = ' . $contractId . '
    AND LF.LIGNEREFARTICLE IN ("J+", "J-")
GROUP BY EF.ENTETEAUTO, LF.LIGNEREFARTICLE;';

    my $stmt = $db->query($query);

    my $tabData = {};
    while (my $tabInvoiceInfos = $stmt->fetch(LOC::Db::FETCH_ASSOC))
    {
        my $key = ($tabInvoiceInfos->{'article'} eq 'J+' ? LOC::Day::TYPE_ADDITIONAL : LOC::Day::TYPE_DEDUCTED);

        $tabData->{$key}->{$tabInvoiceInfos->{'invoicingDate'}} += $tabInvoiceInfos->{'count'};
    }

    return $tabData;
}

# Function: insertDays
#
sub insertDays
{
    my ($db, $countryId, $agencyId, $nbDays, $type, $isInvoiceable, $isInvoiced, $invoicingDate, $tabInvoiceDurations, $entity, $entityId) = @_;

    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my $entityField = ($entity eq 'contract' ? 'day_rct_id' : 'day_rel_id');

    my $nbDaysToInt = int($nbDays);
    if ($nbDaysToInt > 0)
    {
        for (my $i = 0; $i < $nbDaysToInt; $i++)
        {
            my $tabUpdates = {
                'day_type'            => $type,
                'day_agc_id'          => $agencyId,
                $entityField          => $entityId,
                'day_did_id'          => $tabInvoiceDurations->{'day'},
                'day_is_invoiceable'  => $isInvoiceable,
                'day_is_invoiced'     => $isInvoiced,
                'day_date_invoicing'  => $invoicingDate,
                'day_is_migrated'     => 1,
                'day_date_creation*'  => 'NOW()',
                'day_usr_id_creator'  => LOC::User::ADMIN_ID,
                'day_sta_id'          => LOC::Day::STATE_ACTIVE
            };

            my $dayId = $db->insert('f_day', $tabUpdates, $schemaName);
            if ($dayId == -1)
            {
                # Erreur fatale !
                &addError($countryId, $entity, $entityId);
            }
        }
    }
    if ($nbDays - $nbDaysToInt > 0)
    {
        my $tabUpdates = {
                'day_type'            => $type,
                'day_agc_id'          => $agencyId,
                $entityField          => $entityId,
                'day_did_id'          => $tabInvoiceDurations->{'halfDay'},
                'day_is_invoiceable'  => $isInvoiceable,
                'day_is_invoiced'     => $isInvoiced,
                'day_date_invoicing'  => $invoicingDate,
                'day_is_migrated'     => 1,
                'day_date_creation*'  => 'NOW()',
                'day_usr_id_creator'  => LOC::User::ADMIN_ID,
                'day_sta_id'          => LOC::Day::STATE_ACTIVE
            };

            my $dayId = $db->insert('f_day', $tabUpdates, $schemaName);
            if ($dayId == -1)
            {
                # Erreur fatale !
                &addError($countryId, $entity, $entityId);
            }
    }
};