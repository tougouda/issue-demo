#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../../inc', '../../lib';

# Traitement des variables d'environnement
use LOC::Env;

use LOC::Globals;
use LOC::Request;
use LOC::Country;
use LOC::Transport;
use LOC::Timer;

use CGI::Carp qw(fatalsToBrowser);
$| = 1;

my @start = &LOC::Timer::start();

print "Content-type: text/plain; charset=UTF-8\n\n";
print 'Début de la mise à jour : ' . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . "\n\n";

if (&LOC::Globals::getEnv() =~ /^dev/)
{
    my $dev = &LOC::Globals::get('dev');
    print "Base utilisée : DB$dev\n";
}

# Fichier de log
my $commonFilesPath = &LOC::Util::getCommonFilesPath('logs/release/');
open(FILE, '>' . $commonFilesPath . 'updateLockForSitesTransfer.txt');
print FILE '### Début de la mise à jour : ' . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . "\n\n";
print FILE "-----\n\n";

# # # # # #
#
# Déverrouillage des transferts inter-chantiers
# où le déchargement n'est pas encore facturé et peut donc être encore modifié
#
# # # # # #

my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);

foreach my $countryId (@$tabCountries)
{
    print FILE '### ' . $countryId . " ###\n";

    my $tabFilters = {
        'type'     => LOC::Transport::TYPE_SITESTRANSFER,
        'isLocked' => 1,
        'stateId'  => LOC::Transport::STATE_ACTIVE,
        'to'       => {
            'type'           => LOC::Transport::FROMTOTYPE_CONTRACT,
            'invoiceStateId' => LOC::Contract::Rent::INVOICESTATE_NONE
        }
    };

    my $result = (&LOC::Transport::unlock($countryId, $tabFilters, undef, undef) ? 'OK' : 'KO');

    print FILE $result . "\n";
    print FILE "-----\n\n";
}

my $end = &LOC::Timer::end(@start);
print "\n\nFin de la mise à jour : " . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . " (Temps écoulé : " . $end . "s).\n\n";
print FILE "### Fin de la mise à jour : " . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . " (Temps écoulé : " . $end . "s).";
close(FILE);