#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../../inc', '../../lib';

# Traitement des variables d'environnement
use LOC::Env;

use LOC::Globals;
use LOC::Db;
use LOC::Country;
use LOC::Timer;

use CGI::Carp qw(fatalsToBrowser);
$| = 1;

my @start = &LOC::Timer::start();


print "Content-type: text/plain; charset=UTF-8\n\n";
print "Début de la mise à jour...\n";
print "\n";

# Fichier de log
my $commonFilesPath = &LOC::Util::getCommonFilesPath('logs/release/');
# Ouverture du fichier de log
open(FILE, '>' . $commonFilesPath . 'updateLastControlDate.txt');
# Entête
print FILE '### Début de la mise à jour : ' . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . "\n\n";

# connexion à Kimoce
my $kimoceConn = &LOC::Db::getConnection('kimoce');

my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);

foreach my $countryId (@$tabCountries)
{
    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    print FILE '## ' . $countryId . " ##\n";

    # Récupération des machines à mettre à jour
    my $query = '
SELECT
    M.MAAUTO,
    MANOPARC
FROM AUTH.MACHINE M
INNER JOIN CONTROLEMACHINE CM ON CM.MAAUTO = M.MAAUTO';

    my $tabParkNumbers = $db->fetchPairs($query);

    my $c = 0;

    foreach my $machineId (keys(%$tabParkNumbers))
    {
        # Récupération des dates VGP dans Kimoce
        my $query = '
SELECT 
    ObjCharVal
FROM p_objchar
LEFT JOIN r_char
ON r_char.CharInCde = p_objchar.CharInCde
LEFT JOIN p_dos
ON p_dos.DosInCde = p_objchar.DosInCde
WHERE p_dos.ObjIdentVal = "' . $tabParkNumbers->{$machineId} . '"
AND CharExCde = "DTEVGP";';

        my $lastControlDate = $kimoceConn->fetchOne($query);

        # Formatage de la date
        my $year  = substr($lastControlDate, 0, 4);
        my $month = substr($lastControlDate, 4, 2);
        my $day   = substr($lastControlDate, 6, 2);
        $lastControlDate = $year . '-' . $month . '-' . $day;

        if ($lastControlDate == "--")
        {
            $lastControlDate = undef;
        }
        $lastControlDate =~ s/\\r\\n/ /;

        my $tabUpdates = {
            'COMADAPAVE' => $lastControlDate
        };
        my $tabWheres = {
            'MAAUTO' => $machineId
        };

        if ($db->update('CONTROLEMACHINE', $tabUpdates, $tabWheres) == -1)
        {
            print FILE $countryId . ' - MAAUTO ' . $machineId . " non mis à jour\n";
        }
        else
        {
            $c++;
        }
    }

    print FILE "-----\n" . $countryId . ' - ' . $c . " lignes mises à jour\n\n";

}

my $end = &LOC::Timer::end(@start);
print "Mise à jour terminée (Temps écoulé : " . $end . "s).";
print FILE "### Fin de la mise à jour : " . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . " (Temps écoulé : " . $end . "s).";
close(FILE);