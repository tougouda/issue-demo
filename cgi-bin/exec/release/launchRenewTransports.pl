#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../inc', '../lib';

# Traitement des variables d'environnement
use LOC::Env;

use LOC::Globals;
use LOC::Db;
use LOC::Country;
use LOC::Timer;
use LOC::Transport;
use LOC::Session;

use CGI::Carp qw(fatalsToBrowser);
$| = 1;

my @start = &LOC::Timer::start();

# Initialisation de la session
&LOC::Session::load();
my $tabUserInfos = &LOC::Session::getUserInfos();
my $countryId    = 'FR';


print "Content-type: text/plain; charset=UTF-8\n\n";
print "Début de la mise à jour...\n";
print "\n";

# Fichier de log
my $commonFilesPath = &LOC::Util::getCommonFilesPath('logs/release/');
# Ouverture du fichier de log
open(FILE, '>' . $commonFilesPath . 'launchRenewTransports.txt');
# Entête
print FILE '### Début de la mise à jour : ' . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . "\n\n";


# Récupération de la connexion à la base de données
my $db = &LOC::Db::getConnection('location', $countryId);

print FILE '## ' . $countryId . " ##\n";

# Récupération des transports à mettre à jour
# - sur contrats et transferts inter-agences
# - sur lignes de devis
my $query = '
(SELECT
    tsp_id
FROM f_transport
WHERE tsp_sta_id = ' . $db->quote(LOC::Transport::STATE_ACTIVE) . '
AND (tsp_is_loadingdone = 0 OR tsp_is_unloadingdone = 0)
AND tsp_rel_id_from IS NULL
AND tsp_rel_id_to IS NULL
AND tsp_is_locked = 0
)
UNION
(SELECT
    tsp_id
FROM f_transport
INNER JOIN DETAILSDEVIS tbl_f_estimatedetail ON  tbl_f_estimatedetail.DETAILSDEVISAUTO = tsp_rel_id_from
WHERE tsp_sta_id = ' . $db->quote(LOC::Transport::STATE_ACTIVE) . '
AND tsp_rel_id_from IS NOT NULL
AND tbl_f_estimatedetail.DETAILSDEVISACCEPTE = -1
AND tsp_is_locked = 0
)
UNION
(SELECT
    tsp_id
FROM f_transport
INNER JOIN DETAILSDEVIS tbl_f_estimatedetail ON  tbl_f_estimatedetail.DETAILSDEVISAUTO = tsp_rel_id_to
WHERE tsp_sta_id = ' . $db->quote(LOC::Transport::STATE_ACTIVE) . '
AND tsp_rel_id_to IS NOT NULL
AND tbl_f_estimatedetail.DETAILSDEVISACCEPTE = -1
AND tsp_is_locked = 0
)
';

my @tabTransportsIds = $db->fetchCol($query);
my $ignoreHash       = 1;

# Lancement de la mise à jour des trames
my $requestId = &LOC::Transport::launchRenew($countryId, \@tabTransportsIds, $ignoreHash, $tabUserInfos);
my $stateRequest = undef;
my $total = 0;

while ($stateRequest ne LOC::TransportRequest::STATE_SUCCESS && $stateRequest ne LOC::TransportRequest::STATE_FAILED)
{
    my $tabRequestInfos = &LOC::TransportRequest::getInfos($countryId, $requestId);
    print FILE sprintf("Progression : %s%% - Transports traités : %s\n", $tabRequestInfos->{'progression'}, $tabRequestInfos->{'treatedTransportsNo'});
    $stateRequest = $tabRequestInfos->{'state.id'};
    $total = $tabRequestInfos->{'treatedTransportsNo'};
    sleep(3);
}

print FILE "-----\n" . $countryId . ' - ' . $total . " lignes mises à jour\n\n";



my $end = &LOC::Timer::end(@start);
print "Mise à jour terminée (Temps écoulé : " . $end . "s).";
print FILE "### Fin de la mise à jour : " . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . " (Temps écoulé : " . $end . "s).";
close(FILE);