#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../../inc', '../../lib';

# Traitement des variables d'environnement
use LOC::Env;

use LOC::Globals;
use LOC::Db;
use LOC::Request;
use LOC::Country;
use LOC::Machine;
use LOC::Transport;
use LOC::Timer;

use CGI::Carp qw(fatalsToBrowser);
$| = 1;

my @start = &LOC::Timer::start();

print "Content-type: text/plain; charset=UTF-8\n\n";
print 'Début de la mise à jour : ' . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . "\n\n";

if (&LOC::Globals::getEnv() =~ /^dev/)
{
    my $dev = &LOC::Globals::get('dev');
    print "Base utilisée : DB$dev\n";
}

# Fichier de log
my $commonFilesPath = &LOC::Util::getCommonFilesPath('logs/release/');
open(FILE, '>' . $commonFilesPath . 'migrateTransport.txt');
print FILE '### Début de la mise à jour : ' . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . "\n\n";
print FILE "-----\n\n";

# Récupération des paramètres
my $userId = &LOC::Request::getInteger('userId', 558, LOC::Request::SRC_GET);

# # # # # #
#
# Mise à jour des transports suite à la migration
#
# # # # # #

my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);

foreach my $countryId (@$tabCountries)
{
    print FILE '### ' . $countryId . " ###\n";
 
    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    ### Mise à jour des machines sur un transfert inter-chantiers en cours
    my $query = '
SELECT
    tsp_mac_id AS `id`,
    tsp_is_self AS `isSelf`,
    tsp_rct_id_from AS `from.id`,
    tsp_is_loadingdone AS `loading.isDone`,
    -C1.CONTRATMACHSUPRETOUR AS `loading.isAddMachine`,
    tsp_rct_id_to AS `to.id`,
    tsp_is_unloadingdone AS `unloading.isDone`,
    -C2.CONTRATMACHSUPALLER AS `unloading.isAddMachine`
FROM f_transport
INNER JOIN CONTRAT C1
ON C1.CONTRATAUTO = tsp_rct_id_from
INNER JOIN CONTRAT C2
ON C2.CONTRATAUTO = tsp_rct_id_to
WHERE tsp_sta_id = "' . LOC::Transport::STATE_ACTIVE . '"
AND tsp_type = "' . LOC::Transport::TYPE_SITESTRANSFER . '"
AND (tsp_is_unloadingdone = 0 OR tsp_is_loadingdone = 0);';

    my $tabMachinesTsf = $db->fetchAssoc($query, {}, 'id');
    if ($tabMachinesTsf)
    {
        print FILE '# Mise à jour des machines ';

        foreach my $machineId (keys(%$tabMachinesTsf))
        {
            print ".";

            my $tabUpdatesMachine      = {}; # updates pour la machine
            my $tabUpdatesContractTo   = {}; # updates pour le contrat de destination
            my $tabUpdatesContractFrom = {}; # updates pour le contrat de provenance

            # si la récupération n'est pas faite
            if (!$tabMachinesTsf->{$machineId}->{'loading.isDone'})
            {
                # Mise à jour de la machine sur le contrat de provenance
                $tabUpdatesMachine->{'state.id'} = LOC::Machine::STATE_RECOVERY;
                $tabUpdatesMachine->{'contract.id'} = $tabMachinesTsf->{$machineId}->{'from.id'};
                # Allocation de la machine sur le contrat de destination
                $tabUpdatesContractTo->{'CONTRATMACHATTR'} = 0;
                # Repasse le contrat de destination en précontrat
                $tabUpdatesContractTo->{'ETCODE'} = 'CON01';
                $tabUpdatesContractTo->{'LIETCONTRATAUTO'} = '1';
            }
            # sinon, si la livraison n'est pas faite
            elsif ($tabMachinesTsf->{$machineId}->{'loading.isDone'} &&
                    !$tabMachinesTsf->{$machineId}->{'unloading.isDone'})
            {
                # Mise à jour de la machine sur le contrat de destination
                $tabUpdatesMachine->{'state.id'} = LOC::Machine::STATE_DELIVERY;
            }

            # dans le cas d'une reprise sur chantier, on enlève l'information de machine supplémentaire
            if ($tabMachinesTsf->{$machineId}->{'isSelf'})
            {
                if ($tabMachinesTsf->{$machineId}->{'loading.isAddMachine'})
                {
                    $tabUpdatesContractFrom->{'CONTRATMACHSUPRETOUR'} = 0;
                }
                if ($tabMachinesTsf->{$machineId}->{'unloading.isAddMachine'})
                {
                    $tabUpdatesContractTo->{'CONTRATMACHSUPALLER'} = 0;
                }
            }

            if (keys(%$tabUpdatesContractTo) > 0)
            {
                my $return = $db->update('CONTRAT', $tabUpdatesContractTo,
                                                    {'CONTRATAUTO*' => $tabMachinesTsf->{$machineId}->{'to.id'}});
                if ($return == -1)
                {
                    print FILE "\n";
                    print FILE "  - machine non allouée sur le contrat " . $tabMachinesTsf->{$machineId}->{'to.id'} . ' (CONTRATMACHATTR = 0)';
                }
            }
            if (keys(%$tabUpdatesContractFrom) > 0)
            {
                my $return = $db->update('CONTRAT', $tabUpdatesContractFrom,
                                                    {'CONTRATAUTO*' => $tabMachinesTsf->{$machineId}->{'from.id'}});
                if ($return == -1)
                {
                    print FILE "\n";
                    print FILE "  - machine sup. non supprimée sur le contrat " . $tabMachinesTsf->{$machineId}->{'from.id'};
                }
            }
            if (keys(%$tabUpdatesMachine) > 0)
            {
                my $tabErrors = {};
                my $return = &LOC::Machine::update($countryId, $machineId, $tabUpdatesMachine, $userId, {}, $tabErrors);
                if ($return == 0)
                {
                    print FILE "\n";
                    print FILE '  - machine ' . $machineId . ' non mise à jour : état = ' . $tabUpdatesMachine->{'state.id'} . ' - ';
                    if ($tabUpdatesMachine->{'contract.id'})
                    {
                        print FILE "\n";
                        print FILE "  - contrat = " . $tabMachinesTsf->{$machineId}->{'from.id'};
                    }
                    print FILE "\n";
                    print &LOC::Json::toJson($tabErrors);
                }
            }
        }
    }

    print FILE "\n\n";

    ### Mise à jour des machines attribuées à un pré-contrat -> MAC12

    # - Récupération des machines à mettre à jour
    my $query = '
SELECT
    M.MAAUTO
FROM AUTH.MACHINE M
INNER JOIN CONTRAT C
ON C.MAAUTO = M.MAAUTO
WHERE C.CONTRATOK = -1
AND C.ETCODE = "' . LOC::Contract::Rent::STATE_PRECONTRACT . '"
AND C.MAAUTO != 0
AND M.ETCODE != "' . LOC::Machine::STATE_DELIVERY . '"
AND C.CONTRATMACHATTR = -1;';
    my $tabMachines = $db->fetchCol($query);

    if ($tabMachines)
    {
        print FILE '# Mise à jour des machines attribuées à un pré-contrat ';

        foreach my $machineId (@$tabMachines)
        {
            print ".";

            my $tabErrors = {};
            my $return = &LOC::Machine::update($countryId, $machineId, {'state.id' => LOC::Machine::STATE_DELIVERY}, $userId, {}, $tabErrors);
            if ($return == 0)
            {
                print FILE "\n";
                print FILE '  - machine (MAAUTO) ' . $machineId . " non mise à jour sur un préco : état " . LOC::Machine::STATE_DELIVERY . "\n";
                print FILE &LOC::Json::toJson($tabErrors);
            }
        }
    }

    print FILE "\n\n";


    ### Remise en cours des livraisons non faites mais sur un contrat dont la machine est louée (TSP02 suite à la migration)
    my $query = '
SELECT
    tsp_rct_id_to
FROM f_transport
WHERE tsp_type = "' . LOC::Transport::TYPE_DELIVERY . '"
AND tsp_sta_id = "' . LOC::Transport::STATE_INACTIVE . '";';
    my $tabContractsToUndo = $db->fetchCol($query);

    if ($tabContractsToUndo)
    {
        print  FILE '# Remise en cours des livraisons non faites sur un contrat dont la machine est louée ';
        foreach my $contractId (@$tabContractsToUndo)
        {
            print ".";

            # remet en cours les livraison
            my $tabErrors = {};
            my $return = &LOC::Transport::undoDocumentDelivery($countryId, 'contract', $contractId, 0, $userId, $tabErrors);
            if ($return == 0)
            {
                print FILE "\n";
                print FILE '  - contrat (CONTRATAUTO) ' . $contractId . " : livraison non remise en cours\n";
                print FILE &LOC::Json::toJson($tabErrors);
            }
            else
            {
                $db->update('f_transport', {'tsp_sta_id' => LOC::Transport::STATE_ACTIVE}, {'tsp_rct_id_to' => $contractId});
            }
        }

        print FILE "\n\n";
    }

    ### Remise en arrêt des contrats en facturation avec tournée de récupération en cours
    my $query = '
SELECT
    tsp_rct_id_from
FROM f_transport
WHERE tsp_type IN ("' . LOC::Transport::TYPE_RECOVERY . '", "' . LOC::Transport::TYPE_SITESTRANSFER . '")
AND tsp_sta_id = "' . LOC::Transport::STATE_INACTIVE . '"';
    my $tabContractsToStop = $db->fetchCol($query);

    if ($tabContractsToStop)
    {
        print FILE '# Remise en arrêt des contrats en facturation avec tournée de récupération en cours ';
        foreach my $contractId (@$tabContractsToStop)
        {
            print ".";

            # remet en arrêt les contrats
            my $tabData = {
                'subAction' => 'restop'
            };
            my $tabErrors = {};
            my $return = &LOC::Contract::Rent::update($countryId, $contractId, $tabData, $userId, $tabErrors);
            if ($return == 0)
            {
                print FILE "\n";
                print FILE '  - contrat (CONTRATAUTO) ' . $contractId . " non remis en arrêt (récupération en cours)\n";
                print FILE &LOC::Json::toJson($tabErrors);
            }
            else
            {
                $db->update('f_transport', {'tsp_sta_id' => LOC::Transport::STATE_ACTIVE}, {'tsp_rct_id_from' => $contractId});
            }
        }

        print FILE "\n\n";
    }

    ### Remise en arrêt des contrats en facturation sans tournée de récupération avec machine MAC03 toujours sur le contrat
    my $query = '
SELECT
    C.CONTRATAUTO
FROM CONTRAT C
INNER JOIN AUTH.MACHINE M
ON C.MAAUTO = M.MAAUTO
LEFT JOIN f_transport t1
ON t1.tsp_rct_id_from = C.CONTRATAUTO
WHERE C.ETCODE = "' . LOC::Contract::Rent::STATE_BILLED . '"
AND CONTRATOK = -1
AND M.ETCODE = "' . LOC::Machine::STATE_RECOVERY . '"
AND C.CONTRATAUTO = M.CONTRATAUTO
AND t1.tsp_id IS NULL
ORDER BY C.CONTRATAUTO;';
    my $tabContractsToStop = $db->fetchCol($query);

    if ($tabContractsToStop)
    {
        print FILE '# Remise en arrêt des contrats en facturation sans tournée de récupération avec machine Louée toujours sur le contrat ';
        foreach my $contractId (@$tabContractsToStop)
        {
            print ".";

            # remet en arrêt les contrats
            my $tabData = {
                'subAction' => 'restop'
            };
            my $tabErrors = {};
            my $return = &LOC::Contract::Rent::update($countryId, $contractId, $tabData, $userId, $tabErrors);
            if ($return == 0)
            {
                print FILE "\n";
                print FILE '  - contrat (CONTRATAUTO) ' . $contractId . " non remis en arrêt (aucune récupération)\n";
                print FILE &LOC::Json::toJson($tabErrors);
            }
        }

        print FILE "\n\n";
    }


    ### Mise à jour des transports : recalcul des faisabilités, des erreurs, des commandes

    # - Réalisation
    # on récupère les transports dont le déchargement est en cours
    my $tabFilters = {
        'stateId'          => [LOC::Transport::STATE_ACTIVE],
        'isUnloadingDone'  => 0
    };
    my $tabTspToRenew = &LOC::Transport::getList($countryId, LOC::Util::GETLIST_IDS, $tabFilters);

    if ($tabTspToRenew)
    {
        print FILE '# Recalcul des faisabilités, génération des commandes ';
        foreach my $tspId (@$tabTspToRenew)
        {
            print ".";

            my $return = &LOC::Transport::renew($countryId, {'id' => $tspId});
            if ($return == 0)
            {
                print FILE "\n";
                print FILE "  - Faisabilité et erreurs non mises à jour pour le transport " . $tspId . "\n";
            }
        }

        print FILE "\n";
    }

    print FILE "\n";


    ### Vérifications
    print FILE "# Vérifications :\n";
    # - Nombre de contrats non annulés
    my $query = '
SELECT
    COUNT(*)
FROM CONTRAT C
WHERE C.ETCODE != "' . LOC::Contract::Rent::STATE_CANCELED . '"
AND SOLOAUTO = 0
AND C.CONTRATOK = -1;';

    print FILE ' - Nombre de contrats non annulés = ' . $db->fetchOne($query) . "\n";

    # - Transports sur les contrats
    my $query = '
SELECT
    IF (t1.tsp_rct_id_from IS NOT NULL, 1, 0) +  IF (t2.tsp_rct_id_to IS NOT NULL, 1, 0) AS `nbTsp`,
    COUNT(*)
FROM CONTRAT
LEFT JOIN f_transport t1
ON t1.tsp_rct_id_from = CONTRATAUTO
LEFT JOIN f_transport t2
ON t2.tsp_rct_id_to = CONTRATAUTO
WHERE ETCODE != "' . LOC::Contract::Rent::STATE_CANCELED . '"
AND CONTRATOK = -1
AND SOLOAUTO = 0
GROUP BY nbTsp
ORDER BY nbTsp DESC;';

    my $tabResult = $db->fetchPairs($query);

    if ($tabResult)
    {
        foreach my $nb (keys %$tabResult)
        {
            if ($nb >= 2)
            {
                if ($tabResult->{$nb} >= 2)
                {
                    printf FILE ('  - %d contrats non annulés avec %d transports', $tabResult->{$nb}, $nb);
                }
                else
                {
                    printf FILE ('  - %d contrat non annulé avec %d transports', $tabResult->{$nb}, $nb);
                }
            }
            else
            {
                if ($tabResult->{$nb} >= 2)
                {
                    printf FILE ('  - %d contrats non annulés avec %d transport', $tabResult->{$nb}, $nb);
                }
                else
                {
                    printf FILE ('  - %d contrat non annulé avec %d transport', $tabResult->{$nb}, $nb);
                }
            }
            print FILE "\n";
        }
    }

    print FILE "\n";
    print FILE "-----\n\n";
}

my $end = &LOC::Timer::end(@start);
print "\n\nFin de la mise à jour : " . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . " (Temps écoulé : " . $end . "s).\n\n";
print FILE "### Fin de la mise à jour : " . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . " (Temps écoulé : " . $end . "s).";
close(FILE);