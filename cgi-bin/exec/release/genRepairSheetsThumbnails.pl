#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../../inc', '../../lib';

# Traitement des variables d'environnement
use LOC::Env;

use LOC::Globals;

# Modules internes
use Image::Resize;
use File::PathInfo;


#print "Content-Type: text/plain; charset=UTF-8\n\n";


my $rootFolder = &LOC::Util::getCommonFilesPath('repairSheetPhotos/');




#pour chaque dossier dans repairSheetPhotos Pays
opendir (my $countryfolders, $rootFolder);
my @countryFolders = grep { !/^\.\.?$/ } readdir($countryfolders);
closedir ($countryfolders);
foreach my $CountryFoldersName (@countryFolders)
{
    #pour chaque dossier d'un pays Sheet
    opendir (my $sheetfolder, $rootFolder . '/' . $CountryFoldersName);
    my @sheetFolders = grep { !/^\.\.?$/ } readdir($sheetfolder);
    closedir ($sheetfolder);
    
        
    foreach my $sheetFoldersName (@sheetFolders)
    {
        #pour chaque fichier dans le dossier d'une feuille
        
        opendir (my $imageFolder, $rootFolder . '/' . $CountryFoldersName . '/' . $sheetFoldersName);
        my @fileList = grep { !/^\.\.?$/ } readdir($imageFolder);
        closedir ($imageFolder);
        foreach my $fileName (@fileList)
        {
            #création de la miniature
            $fileName = &LOC::Util::decodeUtf8($fileName);
            my $prefix = substr $fileName, 0, 5;
            if($prefix ne 'thumb')
            {
                my $fi = new File::PathInfo($rootFolder . '/' . $CountryFoldersName . '/' . $sheetFoldersName . '/' . $fileName );
                my $fileNameThumb = 'thumb_' . $fi->filename_only();
                if ($fi->ext() ne 'bmp')
                {
                    my $gdImage = new GD::Image($rootFolder . '/' . $CountryFoldersName . '/' . $sheetFoldersName . '/' . $fileName );
                    my $image = new Image::Resize($rootFolder . '/' . $CountryFoldersName . '/' . $sheetFoldersName . '/' . $fileName );
                    my $resizePhotoFile = $image->resize((48 * $gdImage->width)/$gdImage->height, 48);
                    
                    
                    open(PICT, '>' . $rootFolder . '/' . $CountryFoldersName . '/' . $sheetFoldersName . '/' . $fileNameThumb . '.png');
                    binmode PICT, ":raw";
                    print PICT $resizePhotoFile->png;
                    close(PICT);
                }
            }
        }
    }
}
