#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../inc', '../lib';

# Traitement des variables d'environnement
use LOC::Env;

use LOC::Db;
use LOC::Invoice;
use LOC::InvoicingRequest;
use LOC::Json;
use LOC::Log;
use LOC::Session;

# Initialisation de la session
&LOC::Session::load();

# Récupération des paramètres
my ($requestId, $countryId) = @ARGV;

# Récupération des informations de la requête
my $tabRequestData = &LOC::InvoicingRequest::getInfos($requestId, $countryId);

if (defined $tabRequestData)
{
    my $tabRequestDataParams = &LOC::Json::fromJson($tabRequestData->{'parameters'});
    $tabRequestData->{'parameters'} = $tabRequestDataParams;

    my $tabUserInfos = {'id'              => $tabRequestData->{'user.id'},
                        'nomadCountry.id' => $tabRequestData->{'parameters'}->{'userNomadCountryId'},
                        'nomadAgency.id'  => $tabRequestData->{'parameters'}->{'userNomadAgencyId'}};

    # Mise à jour du PID
    &LOC::InvoicingRequest::setPid($countryId, $requestId, $$);

    # Mise à jour de la date de début
    &LOC::InvoicingRequest::setStartDate($countryId, $requestId);

    # Mise à jour de l'état
    &LOC::InvoicingRequest::setState($countryId, $requestId, LOC::InvoicingRequest::STATE_PROGRESS);

    # Lancement de la facturation
    my $tabInvoice = &LOC::Invoice::invoice($countryId, $tabUserInfos,
                                            $tabRequestData->{'parameters'}->{'type'},
                                            $tabRequestData->{'parameters'}->{'rentType'},
                                            $tabRequestData->{'parameters'}->{'date'},
                                            $tabRequestData->{'parameters'}->{'no'},
                                            $requestId);

    my $isSuccess = ($tabInvoice != -1 && $tabInvoice->{'readyToInvoice'});
    my $stateId = ($isSuccess ? LOC::InvoicingRequest::STATE_SUCCESS : LOC::InvoicingRequest::STATE_FAILED);

    if ($isSuccess)
    {
        # Mise à jour de la progression
        &LOC::InvoicingRequest::setProgression($countryId, $requestId, 100);

        # Mise à jour du nombre de factures générées
        &LOC::InvoicingRequest::setGeneratedInvoicesNo($countryId, $requestId, $tabInvoice->{'nbGeneratedDocuments'});
    }

    # Mise à jour de l'état
    &LOC::InvoicingRequest::setState($countryId, $requestId, $stateId);

    # Mise à jour de la date de fin
    &LOC::InvoicingRequest::setEndDate($countryId, $requestId);

    # Stockage du tableau de résultat
    &LOC::InvoicingRequest::setResult($countryId, $requestId, $tabInvoice);

    # Trace
    my $invoicingId = &LOC::Invoice::getId($countryId, $tabRequestData->{'parameters'}->{'date'},
                                           $tabRequestData->{'parameters'}->{'type'});
    &LOC::Log::writeDBLog('FACTURATION', $invoicingId, LOC::Log::EventType::LAUNCHINVOICING, {'content' => ''});
}
