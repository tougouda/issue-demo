#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../inc', '../lib';

# Traitement des variables d'environnement
use LOC::Env;

use LOC::Globals;
use LOC::Db;
use LOC::Country;
use LOC::Model;
use LOC::Otd;
use LOC::Characteristic;
use LOC::Session;
use LOC::Timer;

use CGI::Carp qw(fatalsToBrowser);
$| = 1;

my @start = &LOC::Timer::start();

print "Content-type: text/plain; charset=UTF-8\n\n";

if (&LOC::Globals::getEnv() =~ /^dev/)
{
    my $dev = &LOC::Globals::get('dev');
    print "Base utilisée : DB$dev\n";
}

# Fichier de log
my $commonFilesPath = &LOC::Util::getCommonFilesPath('logs/release/');
open(FILE, '>' . $commonFilesPath . 'migrateMachineModel.txt');
print FILE '### Début de la mise à jour : ' . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . "\n\n";
print FILE "-----\n";

print "Début de la mise à jour...\n";
print "\n";

# # # # # #
#
# Création de la trame des modèles de machine
#
# # # # # #

# Initialisation de la session
&LOC::Session::load();
my $tabUserInfos = &LOC::Session::getUserInfos();

# Vérification de la gestion des produits
my $isProductManageActive = &LOC::Characteristic::getCountryValueByCode('TRSPGENCMD', $tabUserInfos->{'nomadCountry.id'});
if ($isProductManageActive)
{
    # Nombre de trame envoyée
    my $cpt = 0;

    # Récupération de la liste des modèles de machine
    my $tabModels = &LOC::Model::getList($tabUserInfos->{'nomadCountry.id'}, LOC::Util::GETLIST_ASSOC);
    foreach my $tabModelInfos (values(%$tabModels))
    {
        my $tab = {
                   'id'         => $tabModelInfos->{'id'},
                   'label'      => $tabModelInfos->{'label'} . ' (' . $tabModelInfos->{'manufacturer.label'} . ')',
                   'type.id'    => $tabModelInfos->{'family.id'},
                   'type.label' => $tabModelInfos->{'family.label'}
        };

        # Envoi de la trame à OTD
        my $return = &LOC::Otd::setProduct($tabUserInfos->{'nomadCountry.id'}, $tab, {});
        if ($return == 0)
        {
            print FILE "Trame non créée pour le modèle de machine " . $tab->{'id'} . "\n";
        }
        else
        {
            $cpt++;
        }
    }

    print FILE "\nNombre de trame(s) envoyée(s) : " . $cpt . "\n\n";
    print "\nNombre de trame(s) envoyée(s) : " . $cpt . "\n\n";

}
else
{
    print FILE "Les trames ne doivent pas être envoyées à OTD.\n";
    print "Les trames ne doivent pas être envoyées à OTD.\n";
}

my $end = &LOC::Timer::end(@start);
print "Mise à jour terminée (Temps écoulé : " . $end . "s).";
print FILE "-----\n\n";
print FILE "### Fin de la mise à jour : " . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME) . " (Temps écoulé : " . $end . "s).";
close(FILE);