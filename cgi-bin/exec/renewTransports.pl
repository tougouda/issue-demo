#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
use lib '../inc', '../lib';

# Traitement des variables d'environnement
use LOC::Env;

use LOC::Db;
use LOC::Transport;
use LOC::TransportRequest;
use LOC::Json;
use LOC::Log;
use LOC::Session;
use Data::Dumper;
# Initialisation de la session
&LOC::Session::load();

# Récupération des paramètres
my ($requestId, $countryId) = @ARGV;

# Récupération des informations de la requête
my $tabRequestData = &LOC::TransportRequest::getInfos($countryId, $requestId);

if (defined $tabRequestData)
{
    my $tabRequestDataParams = &LOC::Json::fromJson($tabRequestData->{'parameters'});
    $tabRequestData->{'parameters'} = $tabRequestDataParams;

    my $tabUserInfos = {'id'              => $tabRequestData->{'user.id'},
                        'nomadCountry.id' => $tabRequestData->{'parameters'}->{'userNomadCountryId'},
                        'nomadAgency.id'  => $tabRequestData->{'parameters'}->{'userNomadAgencyId'}};

    # Mise à jour du PID
    &LOC::TransportRequest::setPid($countryId, $requestId, $$);

    # Mise à jour de la date de début
    &LOC::TransportRequest::setStartDate($countryId, $requestId);

    # Mise à jour de l'état
    &LOC::TransportRequest::setState($countryId, $requestId, LOC::TransportRequest::STATE_PROGRESS);

    # Lancement de la mise à jour des transports
    my @tabIds = @{$tabRequestData->{'parameters'}->{'tabTransportIds'}};
    my $treatedTransportsNo = 0;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    my $isSuccess = 1;
    foreach my $tspId (@tabIds)
    {

        # Vérification que le transport n'est pas locké
        my $tabInfos = &LOC::Transport::getInfos($countryId, $tspId);
        my $isLocked = $tabInfos->{'isLocked'};

        if (!$isLocked)
        {
            # Démarre la transaction
            if (!$hasTransaction)
            {
                $db->beginTransaction();
            }

            if (&LOC::Transport::renew($countryId, {'id' => $tspId * 1}, {'ignoreHash' => $tabRequestData->{'parameters'}->{'ignoreHash'}}))
            {
                $treatedTransportsNo++;
                &LOC::TransportRequest::setTreatedTransportsNo($countryId, $requestId,$treatedTransportsNo);
            }
            else
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                $isSuccess = 0;
                last;
            }

            # Valide la transaction
            if (!$hasTransaction)
            {
                if (!$db->commit())
                {
                    $db->rollBack();
                    $isSuccess = 0;
                    last;
                }
            }
        }
    }

    my $stateId = ($isSuccess ? LOC::InvoicingRequest::STATE_SUCCESS : LOC::InvoicingRequest::STATE_FAILED);

    if ($isSuccess)
    {
        # Mise à jour de la progression
        &LOC::TransportRequest::setProgression($countryId, $requestId, 100);

        # Mise à jour du nombre de transports mis à jour
        &LOC::TransportRequest::setUpdatedTransportsNo($countryId, $requestId, $treatedTransportsNo);
    }

    # Mise à jour de l'état
    &LOC::TransportRequest::setState($countryId, $requestId, $stateId);

    # Mise à jour de la date de fin
    &LOC::TransportRequest::setEndDate($countryId, $requestId);
}
