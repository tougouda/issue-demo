#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

print "Content-type: text/plain; charset=UTF-8\n\n";

use strict;
use lib '../../inc', '../../lib';

# Traitement des variables d'environnement
use LOC::Env;

use LOC::Globals;
use LOC::Contract::Rent;

if (&LOC::Globals::getEnv() ne 'exp')
{
    print "DEBUT - Mise à jour des carburants...\n\n";

    my $countryId = 'FR';
    my $userId = 448;
    my $quantity = 0;

    my $db = &LOC::Db::getConnection('location', $countryId);

    # Liste des agences
    my @tabAgencies = &LOC::Agency::getList(LOC::Util::GETLIST_IDS, {'country' => 'FR', 'type' => LOC::Agency::TYPE_AGENCY});

    foreach my $agency (@tabAgencies)
    {
        my %tabContracts = &LOC::Contract::Rent::getFuelList($countryId, $agency, LOC::Util::GETLIST_ASSOC);

        foreach my $contractId (keys(%tabContracts))
        {
            print "Mise à jour du carburant du contrat $contractId ... ";
            if (&LOC::Contract::Rent::updateFuel($countryId, $contractId, $quantity, $userId))
            {
                print "[OK]\n";
            }
            else
            {
                print "[ERREUR]\n";
            }
        }
    }

    print "\nFIN";
}
