use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Customer
# Module permettant d'obtenir les informations sur les clients
package LOC::Customer;


# Constants: Récupération des informations
# GETINFOS_ALL    - Toutes les informations
# GETINFOS_TARIFF - Les informations relatives aux tarifs de location et de transport
# GETINFOS_FLAGS  -
# GETINFOS_CRC    - Informations provenant de la fiche CRC
use constant
{
    GETINFOS_BASE   => 0x0000,
    GETINFOS_ALL    => 0xFFFF,
    GETINFOS_TARIFF => 0x0001,
    GETINFOS_FLAGS  => 0x0002,
    GETINFOS_CRC    => 0x0004
};

# Constants: Types de gestionnaires du client
# MANAGER_MAIN          - Gestionnaire principal
# MANAGER_SUBSTITUTE    - Binôme
# MANAGER_TEMP          - Remplaçant temporaire du gestionnaire principal
use constant
{
    MANAGER_MAIN       => 'main',
    MANAGER_SUBSTITUTE => 'substitute',
    MANAGER_TEMP       => 'temp'
};

# Constants: Types d'actions CRC
# ACTION_AREAEXEMPTION   - Dérogation RR
use constant
{
    ACTION_AREAEXEMPTION => 17
};


use strict;

# Modules internes
use LOC::Characteristic;
use LOC::Db;
use LOC::Globals;
use LOC::Proforma;
use LOC::EndUpdates;
use LOC::User;
use LOC::Transport;
use LOC::Model;
use LOC::Insurance::Type;
use LOC::Template;


my $fatalErrMsg = 'Erreur fatale: module LOC::Customer, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";



# Function: getInfos
# Retourne les infos d'un client
#
# Contenu du hashage :
# - id : identifiant du client ;
# - agency.id : identifiant de l'agence ;
# - code : code ;
# - name : nom ou raison sociale ;
# - address1 : adresse ;
# - address2 : complément d'adresse ;
# - locality.id : identifiant de la ville du client ;
# - locality.postalCode : code postal de la ville du client ;
# - locality.name : nom de la ville du client ;
# - locality.country.name : nom du pays de la ville du client ;
# - locality.country.code : code du pays de la ville du client ;
# - telephone : numéro de téléphone ;
# - fax : numéro de fax ;
# - mobile : numéro de téléphone portable ;
# - email : adresse e-mail ;
# - hasEmail : booléen indiquant si le client possède une adresse e-mail ;
# - statCode : code SIRET (ou équivalent étranger) ;
# - statCodeCpl : complément du code SIRET ;
# - contact.name : nom du contact ;
# - comments : commentaires ;
# - isVerified : booléen indiquant si le client est vérifié ou non,
# - verificationStatus : état de vérification (1 non vérifié depuis un devis, -1 non vérifié depuis un contrat),
# - validityStatus : état de validité (0 prospect devient client, 1 vérifié depuis un devis, -1 non valide),
# - isLocked : booléen indiquant si le client est bloqué (CLSAGE) ;
# - isTempUnlock : booléen indiquant si le client est débloqué temporairement ;
# - accountingCode : code collectif ;
# - isNewAccount : booléen indiquant q'il s'agit d'un nouveau compte ;
# - url : URL de la fiche client Gesloc
# - appCRM.url : URL de la fiche client CRM
# - statement : RIB ;
# - paymentMode.id : identifiant du type de paiement ;
# - paymentMode.label : libellé du type de paiement ;
# - activity.statCode : code NAF (ou équivalent étranger) ;
# - activity.id : identifiant de l'activité (notion Acces industrie) ;
# - bank : banque ;
# - accountingCategory : Catégorie comptable (1: TVA, 0: pas de TVA !) ;
# - invoicesNo : nombre de factures à produire ;
# - CLFACTGRP ;
# - isProformaRequired : booléen indiquant si l'édition de facture proforma est obligatoire ;
# - isOrderNoOnInvoice : booléen indiquant si le numéro de commande est obligatoire sur la facture ;
# - areGeneralConditionsApproved : booléen indiquant si le client a approuvé les conditions générales de location ;
# - isCleaningInvoiced : booléen indiquant si le nettoyage est facturé au client ;
# - deliveryRecoveryNotesRequired : booléen indiquant si l'envoi des BL/BR avec la facture est obligatoire ;
# - class : classe ;
# - potential : potentiel ;
# - insuranceType.id : identifiant du type d'assurance
# - insurance.rate : taux d'assurance (quand elle est facturée) ;
# - insurance.beginDate : date de début d'application du nouveau taux d'assurance ;
# - insurance.validityDate : date de fin de validité de l'attestion d'assurance du client ;
# - wishedInsuranceType.id : identifiant du type d'assurance souhaitée
# - insurance.wishedRate : taux d'assurance souhaité
# - appealType.id : Identifiant du type de gestion de recours
# - appeal.beginDate : Date de début d'application du nouveau type de gestion de recours
# - wishedAppealType.id : Identifiant du type de gestion de recours souhaitée
# - baseTariff.id : identifiant de la grille tarifaire de location de base ;
# - rentTariffGroup.joinDate : date de rattachement à un groupe tarifaire de location ;
# - transportTariffGroup.label : nom du groupe tarifaire de transport ;
# - frameworkContract.id : identifiant du contrat-cadre ;
# - creator.id : identifiant de l'utilisateur ayant créé le client ;
# - creationDate : date de création :
# - modificationDate : date de modification.
#
# Si le flag GETINFOS_TARIFF est activé, les informations suivantes sont disponibles :
# - rentTariffGroup.id : identifiant du groupe tarifaire de location ;
# - rentTariffGroup.label : nom du groupe tarifaire de location ;
# - transportTariffGroup.id : identifiant du groupe tarifaire de transport ;
# - frameworkContract.label : nom du contrat-cadre ;
# - rentTariff.grid.id : identifiant de la grille tarifaire de location ;
# - rentTariff.grid.label : nom de la grille tarifaire de location ;
# - rentTariff.grid.url : URL de la fiche de visualisation de la grille tarifaire de location ;
# - transportTariff.grid.id : identifiant de la grille tarifaire de transport ;
# - transportTariff.grid.label : nom de la grille tarifaire de transport ;
# - frameworkContract.url : URL de la fiche du contrat-cadre.
# - frameworkContract.fuelUnitPrice : Prix du carburant du contrat-cadre.
#
# Si le flag GETINFOS_FLAGS est activé, les informations suivantes sont disponibles :
# - isElectric: indique si le client loue de l'électrique
# - isTemporaryGrid : indique si le client a une grille provisoire
#
# Si le flag GETINFOS_CRC est activé, récupération des informations suivantes :
# - hasAreaExemption: indique si une action de type Dérogation RR est en cours sur le client
# - refAgencyId: identifiant de l'agence référente
#
# Parameters:
# string  $id         - Identifiant du modèle
# string  $countryId  - Identifiant du pays
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;

    $id *= 1;
    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Retourne la liste des clients
#
# Parameters:
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres ('id' : Recherche par l'identifiant,
#                     'name' : recherche dans la raison sociale,
#                     'code' : recherche dans le code,
#                     'search' : recherche dans l'identifiant, la raison sociale ou le code,
#                     'accountingCode' : codes collectifs à exclure de la recherche)
# int     $flags      - Flag de récupération des données
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Liste des modèles (mêmes clés que pour la méthode getInfos)
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;

    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $rentTariffSchemaName = 'GESTIONTARIF' . $countryId;
    my $trspTariffSchemaName = 'GESTIONTRANS' . $countryId;

    my $query = '';

    # Filtres
    my $joinQuery = '';
    my $whereQuery = '';
    # - Recherche par l'id
    if ($tabFilters->{'id'})
    {
        $whereQuery .= '
AND tbl_f_customer.CLAUTO = ' . $db->quote($tabFilters->{'id'});
    }
    # - Recherche par le nom
    if ($tabFilters->{'name'} ne '')
    {
        my $search = $tabFilters->{'name'};
        # Ajout des % si le filtre n'en contient pas
        if ($search !~ /%/)
        {
            $search = '%' . $tabFilters->{'name'} . '%';
        }
        $whereQuery .= '
AND tbl_f_customer.CLSTE LIKE ' . $db->quote($search);
    }
    # - Recherche par le code
    if ($tabFilters->{'code'} ne '')
    {
        my $search = $tabFilters->{'code'};
        # Ajout des % si le filtre n'en contient pas
        if ($search !~ /%/)
        {
            $search = '%' . $tabFilters->{'code'} . '%';
        }
        $whereQuery .= '
AND tbl_f_customer.CLCODE LIKE ' . $db->quote($search);
    }
    if ($tabFilters->{'search'} ne '')
    {
        $joinQuery .= '
LEFT JOIN AUTH.VICL tbl_p_customerlocality
ON tbl_f_customer.VICLAUTO = tbl_p_customerlocality.VICLAUTO';

        my @search = split(/ /, $tabFilters->{'search'});

        foreach my $search (@search)
        {
            $search = &LOC::Util::trim($search);
            $search = &LOC::Util::escapeRegexForSqlRegex($search);

            # Transformation en expression rationnelle
            # Recherche du type %XXX -> XXX($|\n)
            if ($search =~ /^%/ && $search !~ /%$/)
            {
                # suppression du %
                $search = substr($search, 1);
                $search .= '($|\n)';
            }
            # Recherche du type XXX% -> (^|\n)XXX
            if ($search !~ /^%/ && $search =~ /%$/)
            {
                # suppression du %
                $search = substr($search, 0, length($search) - 1);
                $search = '(^|\n)' . $search;
            }

            # remplacement des % en milieu de chaîne
            $search =~ s/%/.*/g;
            # remplacement des guillemets
            $search =~ s/"/\\"/g;

            if ($search ne '')
            {
                $whereQuery .= '
AND CONCAT_WS("\n", tbl_f_customer.CLCODE, tbl_f_customer.CLSTE, tbl_p_customerlocality.DEP, tbl_p_customerlocality.VICLCP, tbl_p_customerlocality.VICLNOM) REGEXP "' . $search . '"';
            }
        }
    }
    # - Recherche excluant des codes collectifs
    if ((ref($tabFilters->{'accountingCode'}) eq '' && $tabFilters->{'accountingCode'} ne '') ||
        (ref($tabFilters->{'accountingCode'}) eq 'ARRAY' && @{$tabFilters->{'accountingCode'}} != 0) ||
        (ref($tabFilters->{'accountingCode'}) eq 'HASH' && %{$tabFilters->{'accountingCode'}} != 0))
    {
        $whereQuery .= '
AND tbl_f_customer.CLCOLLECTIF NOT IN (' . $db->quote($tabFilters->{'accountingCode'}) . ')';
    }


    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    tbl_f_customer.CLAUTO AS `id`,
    tbl_f_customer.AGAUTO AS `agency.id`,
    "' . $countryId . '" AS `country.id`,
    tbl_f_customer.CLCODE AS `code`,
    tbl_f_customer.CLSTE AS `name`,
    tbl_f_customer.CLADRESSE AS `address1`,
    tbl_f_customer.CLADRESSE2 AS `address2`,
    IF(tbl_p_customerlocality.VICLAUTO IS NULL,
       CONCAT_WS(" ", tbl_f_customer.CLCP, tbl_f_customer.CLVILLE),
       CONCAT_WS(" ", tbl_p_customerlocality.VICLCP, tbl_p_customerlocality.VICLNOM)) AS `address3`,
    tbl_f_customer.VICLAUTO AS `locality.id`,
    IFNULL(tbl_p_customerlocality.VICLCP, tbl_f_customer.CLCP) AS `locality.postalCode`,
    IFNULL(tbl_p_customerlocality.VICLNOM, tbl_f_customer.CLVILLE) AS `locality.name`,
    tbl_p_customerlocality.VICLPAYS AS `locality.country.name`,
    tbl_p_customerlocality.VICLCODEPAYS AS `locality.country.code`,
    tbl_f_customer.CLTEL AS `telephone`,
    tbl_f_customer.CLFAX AS `fax`,
    tbl_f_customer.CLPORTABLE AS `mobile`,
    tbl_f_customer.CLEMAIL AS `email`,
    tbl_f_customer.CLHASEMAIL AS `hasEmail`,
    tbl_f_customer.CLSIREN AS `statCode`,
    tbl_f_customer.CLSIRETCPL AS `statCodeCpl`,
    tbl_f_customer.CLCONTACT AS `contact.name`,
    tbl_f_customer.CLCOMMENTAIRE AS `comments`,
    IF (ABS(tbl_f_customer.CLVERIFIER) = 1, 0, 1) AS `isVerified`,
    IF (tbl_f_customer.CLVERIFIER = 1, "estimate", IF (tbl_f_customer.CLVERIFIER = -1, "contract", "")) AS `prospectSource`,
    tbl_f_customer.CLVERIFIER AS `verificationStatus`,
    tbl_f_customer.CLNONVALIDE AS `validityStatus`,
    IF (tbl_f_customer.CLSAGE = -2, 1, 0) AS `isLocked`,
    IF (tbl_f_customer.CLTMPUNLOCK AND tbl_f_customer.CLSAGE = 0, 1, 0) AS `isTempUnlock`,
    tbl_f_customer.CLCOLLECTIF AS `accountingCode`,
    tbl_f_customer.CLRIB AS `statement`,
    tbl_f_customer.MOPAAUTO AS `paymentMode.id`,
    tbl_p_paymenttype.MOPALIBELLE AS `paymentMode.label`,
    tbl_f_customer.CLAPE AS `activity.statCode`,
    tbl_f_customer.METIERAUTO AS `activity.id`,
    tbl_f_customer.CLBANQUE AS `bank`,
    tbl_f_customer.CLCEE AS `accountingCategory`,
    tbl_f_customer.CLNBFACT AS `invoicesNo`,
    tbl_f_customer.CLFACTGRP,
    -tbl_f_customer.CLPROFORMA AS `isProformaRequired`,
    -tbl_f_customer.CLNUMCMDOBLI AS `isOrderNoOnInvoice`,
    -tbl_f_customer.BCJOIN AS `isOrderRequired`,
    -tbl_f_customer.CLCGVENTE AS `areGeneralConditionsApproved`,
    -tbl_f_customer.CLNETTOYAGE AS `isCleaningInvoiced`,
    -tbl_f_customer.CLDECHETSFACT AS `isResiduesInvoiced`,
    -tbl_f_customer.CLBONSOBLIG AS `deliveryRecoveryNotesRequired`,
    tbl_f_customer.CLCLASSE AS `class`,
    tbl_f_customer.CLPOTENTIEL AS `potential`,
    tbl_f_customer.CLTYPEASSURANCE AS `insuranceType.id`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISINVOICED) . ', tbl_f_customer.CLPASSURANCE, 0) AS `insurance.rate`,
    p_insurancetype.ity_flags AS `insurance.flags`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISINVOICED) . ', 1, 0) AS `insurance.isInvoiced`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISINCLUDED) . ', 1, 0) AS `insurance.isIncluded`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISCUSTOMER) . ', 1, 0) AS `insurance.isCustomer`,
    tbl_f_customer.CLDATEDEBASS AS `insurance.beginDate`,
    tbl_f_customer.CLDATEVALASS AS `insurance.validityDate`,
    tbl_f_customer.CLNVASSURANCE AS `wishedInsuranceType.id`,
    tbl_f_customer.CLNVPASSURANCE AS `insurance.wishedRate`,
    tbl_f_customer.CLTYPERECOURS AS `appealType.id`,
    tbl_f_customer.CLDATEDEBTYPERECOURS AS `appeal.beginDate`,
    tbl_f_customer.CLNVTYPERECOURS AS `wishedAppealType.id`,
    tbl_f_customer.CLIENTTARIFAUTO AS `baseTariff.id`,
    tbl_f_customer.CLGROUPEAUTO AS `rentTariffGroup.id`,
    tbl_f_customer.CCAUTO AS `frameworkContract.id`,
    tbl_f_customer.CLDATEAJOUTGROUPE AS `rentTariffGroup.joinDate`,';

        if ($flags & GETINFOS_TARIFF)
        {
            $query .= '
    tbl_p_rentgroup.CLGRLIBELLE AS `rentTariffGroup.label`,
    tbl_p_transportgrid.ID AS `transportTariffGroup.id`,
    tbl_p_transportgrid.VALEUR AS `transportTariffGroup.label`,
    tbl_p_frameworkcontract.CCLIBELLE AS `frameworkContract.label`,
    tbl_p_frameworkcontract.CCCARBURANT AS `frameworkContract.fuelUnitPrice`,';
        }
        if ($flags & GETINFOS_FLAGS)
        {
            $query .= '
    -tbl_f_customer.CLUSEELECT AS `isElectric`,
    IF (tbl_f_customer.CLCLASSE <> "A" AND tbl_f_customer.CLUSEELECT = 0 AND tbl_f_customergrid.CLAUTO IS NOT NULL, 1, 0) AS `isTemporaryGrid`,';
        }
        $query .= '
    tbl_f_customer.CLPEAUTOCREATE AS `creator.id`,
    tbl_f_customer.CLJOINDOCMACH AS `isJoinDoc`,
    tbl_f_customer.CLDATECREATION AS `creationDate`,
    tbl_f_customer.CLCREATION AS `modificationDate`
FROM CLIENT tbl_f_customer
LEFT JOIN AUTH.VICL tbl_p_customerlocality
ON tbl_f_customer.VICLAUTO = tbl_p_customerlocality.VICLAUTO
LEFT JOIN MODEPAIE tbl_p_paymenttype
ON tbl_f_customer.MOPAAUTO = tbl_p_paymenttype.MOPAAUTO
LEFT JOIN p_insurancetype
ON p_insurancetype.ity_id = tbl_f_customer.CLTYPEASSURANCE';
        if ($flags & GETINFOS_TARIFF)
        {
            $query .= '
LEFT JOIN ' . $rentTariffSchemaName . '.GC_CLGROUPE tbl_p_rentgroup
ON tbl_f_customer.CLGROUPEAUTO = tbl_p_rentgroup.CLGROUPEAUTO
LEFT JOIN ' . $rentTariffSchemaName . '.GC_CONTRATCADRE tbl_p_frameworkcontract
ON tbl_f_customer.CCAUTO = tbl_p_frameworkcontract.CCAUTO
LEFT JOIN ' . $rentTariffSchemaName . '.GC_LIEGRILLE tbl_f_customergrid
ON tbl_f_customergrid.CLAUTO = tbl_f_customer.CLAUTO
LEFT JOIN ' . $trspTariffSchemaName . '.CLIENTGROUPE tbl_f_customer_transportgroup
ON tbl_f_customer.CLAUTO = tbl_f_customer_transportgroup.CLIENT
LEFT JOIN ' . $trspTariffSchemaName . '.GRILLE tbl_p_transportgrid
ON tbl_f_customer_transportgroup.GRILLE = tbl_p_transportgrid.ID';
        }
        $query .= '
WHERE 1' . $whereQuery . '
ORDER BY `name`, `locality.country.code`, `locality.postalCode`, `locality.name`;';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*)
FROM CLIENT tbl_f_customer' . $joinQuery . '
WHERE 1' . $whereQuery . ';';
    }
    else
    {
        $query = '
SELECT
    tbl_f_customer.CLAUTO,
    tbl_f_customer.CLSTE AS `name`
FROM CLIENT tbl_f_customer' . $joinQuery . '
WHERE 1' . $whereQuery . '
ORDER BY `name`;';
    }

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            # Formatage des données
            $tabRow->{'id'}                   *= 1;
            $tabRow->{'locality.id'}          *= 1;
            $tabRow->{'paymentMode.id'}       *= 1;
            $tabRow->{'activity.id'}          *= 1;
            $tabRow->{'insuranceType.id'}     *= 1;
            $tabRow->{'wishedInsuranceType.id'} = (defined $tabRow->{'wishedInsuranceType.id'} ? $tabRow->{'wishedInsuranceType.id'} * 1 : undef);
            $tabRow->{'insurance.rate'}       *= 1;
            $tabRow->{'baseTariff.id'}        *= 1;
            $tabRow->{'rentTariffGroup.id'}   *= 1;
            $tabRow->{'creator.id'}           *= 1;
            $tabRow->{'frameworkContract.id'} *= 1;
            $tabRow->{'hasEmail'}             *= 1;
            $tabRow->{'isProformaRequired'}   *= 1;
            $tabRow->{'isCleaningInvoiced'}   *= 1;
            $tabRow->{'isResiduesInvoiced'}   *= 1;
            $tabRow->{'isLocked'}             *= 1;
            $tabRow->{'isTempUnlock'}         *= 1;
            $tabRow->{'isOrderNoOnInvoice'}   *= 1;
            $tabRow->{'isOrderRequired'}      *= 1;
            $tabRow->{'invoicesNo'}           *= 1;
            $tabRow->{'deliveryRecoveryNotesRequired'} *= 1;
            $tabRow->{'areGeneralConditionsApproved'}  *= 1;
            $tabRow->{'address'}            = $tabRow->{'address1'} . "\n" .
                                              $tabRow->{'address2'} . "\n" .
                                              $tabRow->{'address3'};
            $tabRow->{'isVerified'}         *= 1;
            $tabRow->{'accountingCategory'} *= 1;
            $tabRow->{'isJoinDoc'}          = ($tabRow->{'isJoinDoc'} == -1 ? 1 : 0);

            $tabRow->{'insurance.flags'}      *= 1;
            $tabRow->{'insurance.isInvoiced'} *= 1;
            $tabRow->{'insurance.isIncluded'} *= 1;
            $tabRow->{'insurance.isCustomer'} *= 1;

            $tabRow->{'appealType.id'} *= 1;
            $tabRow->{'wishedAppealType.id'} = (defined $tabRow->{'wishedAppealType.id'} ? $tabRow->{'wishedAppealType.id'} * 1 : undef);

            # Récupération du niveau de blocage
            $tabRow->{'lockLevel'} = &getLockLevel($countryId, $tabRow->{'accountingCode'}, $tabRow->{'isLocked'});
            # Récupération de la urveillance du client
            $tabRow->{'isUnderSurveillance'} = &isUnderSurveillance($countryId, $tabRow->{'accountingCode'});

            # Récupération de l'URL de la fiche client
            $tabRow->{'url'} = &getUrl($tabRow->{'id'});

            # Récupération de l'URL de la fiche client CRM
            $tabRow->{'appCRM.url'} = &LOC::Request::createRequestUri('CRM>default:accounts:view', {'customerId' => $tabRow->{'id'}});

            # Récupération de l'URL de l'historique client
            $tabRow->{'historyUrl'} = &getHistoryUrl($tabRow->{'id'});

            # Ouverture de compte client à faire
            $tabRow->{'isNewAccount'} = &isNewAccount($countryId, $tabRow->{'code'}, $tabRow->{'accountingCode'});

            # Gestionnaires CRC
            my $tabManagers = $tabRow->{'tabManagers'} = &getManagers($countryId, $tabRow->{'id'});

            $tabRow->{'unlockRequestUrl'} = (!$tabRow->{'lockLevel'} || $tabRow->{'isTempUnlock'} ? '#' :
                    &LOC::Request::createRequestUri('accounting:customer:sendUnlockRequest', {'countryId' => $tabRow->{'country.id'},
                                                                                              'id'        => $tabRow->{'id'}}));

            # Informations provenant du CRC
            if ($flags & GETINFOS_CRC)
            {
                # Actions CRC
                $tabRow->{'hasAreaExemption'} = &hasCRCAction($countryId, $tabRow->{'id'}, ACTION_AREAEXEMPTION);

                # Agence de référence dans le CRC
                $tabRow->{'refAgencyId'} = &getCRCRefAgencyId($countryId, $tabRow->{'id'});
            }

            if ($flags & GETINFOS_TARIFF)
            {
                # Récupération de la grille tarifaire de location
                my $tabRentTariffGrid = &getRentTariffGrid($countryId, $tabRow->{'id'});
                $tabRow->{'rentTariff.grid.id'}    = $tabRentTariffGrid->{'id'};
                $tabRow->{'rentTariff.grid.label'} = $tabRentTariffGrid->{'label'};
                $tabRow->{'rentTariff.grid.url'}   = $tabRentTariffGrid->{'url'};

                # Récupération de la grille tarifaire de transport
                my $tabTransportTariffGrid = &getTransportTariffGrid($countryId, $tabRow->{'id'});
                $tabRow->{'transportTariff.grid.id'}    = $tabTransportTariffGrid->{'id'};
                $tabRow->{'transportTariff.grid.label'} = $tabTransportTariffGrid->{'label'};
                $tabRow->{'transportTariff.grid.type'}  = $tabTransportTariffGrid->{'type'};

                $tabRow->{'frameworkContract.fuelUnitPrice'} *= 1;

                # Récupération de l'URL du contrat-cadre
                $tabRow->{'frameworkContract.url'} = &getFrameworkContractUrl($tabRow->{'frameworkContract.id'});
            }

            if ($flags & GETINFOS_FLAGS)
            {
                $tabRow->{'isElectric'}      *= 1;
                $tabRow->{'isTemporaryGrid'} *= 1;
            }
        }
    }

    return $tab;
}



# Function: getLockLevel
# Retourne le niveau de blocage du client
#
# Parameters:
# string $countryId      - Pays
# string $accountingCode - Code collectif du client
# int    $isLocked       - État de blocage du client (CLSAGE)
#
# Returns:
# int - 0 : client non bloqué,
#       1 : client bloqué au contrat (orange),
#       2 : client bloqué au devis et au contrat (rouge),
#       3 : client bloqué au devis et au contrat (rouge) mais prolongation de 5 jours ou moins autorisée sur le contrat.
sub getLockLevel
{
    my ($countryId, $accountingCode, $isLocked) = @_;

    # Niveau 1 (client bloqué au contrat, orange)
    my $regExp = &LOC::Characteristic::getCountryValueByCode('COLBLC', $countryId);
    if ($regExp ne '' && $accountingCode =~ /^$regExp$/)
    {
        return 1;
    }

    # Niveau 2 (client bloqué au contrat
    # mais prolongation de 5 jours ou moins autorisée sur le contrat, orange)
    my $regExp = &LOC::Characteristic::getCountryValueByCode('COLBLCP', $countryId);
    if ($regExp ne '' && $accountingCode =~ /^$regExp$/)
    {
        return 2;
    }

    # Niveau 3 (client bloqué au devis et au contrat, rouge)
    my $regExp = &LOC::Characteristic::getCountryValueByCode('COLBLDC', $countryId);
    if ($regExp ne '' && $accountingCode =~ /^$regExp$/)
    {
        return 3;
    }

    # Niveau 4 (client bloqué au devis et au contrat
    # mais prolongation de 5 jours ou moins autorisée sur le contrat, rouge)
    my $regExp = &LOC::Characteristic::getCountryValueByCode('COLBLDCP', $countryId);
    if ($regExp ne '' && $accountingCode =~ /^$regExp$/)
    {
        return 4;
    }

    # Si ca ne rentre dans aucun des autres cas, niveau 1, rouge
    if ($isLocked)
    {
        return 1;
    }

    return 0;
}


# Function: getManagers
# Récupère la liste des gestionnaires pour le client (gestionnaire principal-e et son binôme)
#
# Parameters:
# string $countryId - Identifiant du pays
# int    $id        - Identifiant du client
#
# Returns:
# hashref
sub getManagers
{
    my ($countryId, $id) = @_;

    if ($countryId eq '')
    {
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = 'crc_' . lc($countryId);

    my $query = '
SELECT
    tbl_f_mainuser.usr_id AS `main.id`,
    CONCAT_WS(" ", tbl_f_mainuser.usr_firstname, tbl_f_mainuser.usr_name) AS `main.fullName`,
    tbl_f_mainuser.usr_mail AS `main.email`,
    tbl_f_substituteuser.usr_id AS `substitute.id`,
    CONCAT_WS(" ", tbl_f_substituteuser.usr_firstname, tbl_f_substituteuser.usr_name) AS `substitute.fullName`,
    tbl_f_substituteuser.usr_mail AS `substitute.email`,
    tbl_f_tempuser.usr_id AS `temp.id`,
    CONCAT_WS(" ", tbl_f_tempuser.usr_firstname, tbl_f_tempuser.usr_name) AS `temp.fullName`,
    tbl_f_tempuser.usr_mail AS `temp.email`
FROM CLIENT tbl_f_customer
INNER JOIN ' . $schemaName . '.f_customeraccountex ON cuax_cus_id = tbl_f_customer.CLAUTO
INNER JOIN ' . $schemaName . '.f_customeraccount ON cua_id = cuax_cua_id
INNER JOIN ' . $schemaName . '.p_agencyaccount ON cua_account = aga_account
LEFT JOIN frmwrk.f_user tbl_f_mainuser ON tbl_f_mainuser.usr_id = aga_usr_id
LEFT JOIN frmwrk.f_user tbl_f_substituteuser ON tbl_f_substituteuser.usr_id = aga_usr_id_2
LEFT JOIN ' . $schemaName . '.f_userex ON usrx_usr_id = tbl_f_mainuser.usr_id
LEFT JOIN frmwrk.f_user tbl_f_tempuser ON tbl_f_tempuser.usr_id = usrx_usr_id_temp
where tbl_f_customer.CLAUTO = ' . $id . ';';

    my $tabResult = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    $tabResult->{'main.id'} *= 1;
    $tabResult->{'substitute.id'} *= 1;

    my $tabManagers = {
        MANAGER_MAIN()       => undef,
        MANAGER_SUBSTITUTE() => undef,
        MANAGER_TEMP()       => undef
    };

    if ($tabResult->{'main.id'} != 0)
    {
        $tabManagers->{&MANAGER_MAIN} = {
            'id'       => $tabResult->{'main.id'},
            'fullName' => $tabResult->{'main.fullName'},
            'email'     => $tabResult->{'main.email'}
        };
    }
    if ($tabResult->{'substitute.id'} != 0)
    {
        $tabManagers->{&MANAGER_SUBSTITUTE} = {
            'id'       => $tabResult->{'substitute.id'},
            'fullName' => $tabResult->{'substitute.fullName'},
            'email'    => $tabResult->{'substitute.email'}
        };
    }
    if ($tabResult->{'temp.id'} != 0)
    {
        $tabManagers->{&MANAGER_TEMP} = {
            'id'       => $tabResult->{'temp.id'},
            'fullName' => $tabResult->{'temp.fullName'},
            'email'     => $tabResult->{'temp.email'}
        };
    }
    return $tabManagers;
}


# Function: getUrl
# Retourne l'URL de l'historique du client
#
# Parameters:
# string $id - Identifiant du client
#
# Returns:
# string - URL de l'historique du client
sub getHistoryUrl
{
    my ($id) = @_;

    my $dev = &LOC::Globals::get('dev');
    return &LOC::Globals::get('locationUrl') .
           '/cgi-bin/old/location/modification.cgi?client=' . $id . ($dev eq '' ? '' : '&dev=' . $dev);
}

# Function: getUrl
# Retourne l'URL de la fiche du client
#
# Parameters:
# int $id - Identifiant du client
#
# Returns:
# string - URL de la fiche du client
sub getUrl
{
    my ($id) = @_;

    my $dev = &LOC::Globals::get('dev');
    return &LOC::Globals::get('locationUrl') . '/cgi-bin/old/location/infoclient.cgi?value=' . $id . ($dev eq '' ? '' : '&dev=' . $dev);
}

# Function: getCRCUrl
# Retourne l'URL de la fiche du client dans le CRC
#
# Parameters:
# int $id - Identifiant GESLOC du client
#
# Returns:
# string - URL de la fiche CRC du client
sub getCRCUrl
{
    my ($countryId, $id) = @_;

    if ($countryId eq '')
    {
        return '#';
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('crc', $countryId);

    my $query = '
SELECT
    cuax_cua_id
FROM f_customeraccountex
WHERE cuax_cus_id = ' . $id . ';';

    my $customerId = $db->fetchOne($query);

    return &LOC::Request::createRequestUri('CRC>default:accounts:view', {'customerId' => $customerId});
}

# Function: getCRCRefAgencyId
# Retourne l'agence référente du client dans le CRC
#
# Parameters:
# string $countryId - Identifiant du pays
# int $id           - Identifiant GESLOC du client
#
# Returns:
# string - URL de la fiche CRC du client
sub getCRCRefAgencyId
{
    my ($countryId, $id) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('crc', $countryId);

    my $query = '
SELECT
    aga_agc_id
FROM p_agencyaccount
INNER JOIN f_customeraccount
ON cua_account = aga_account
INNER JOIN f_customeraccountex
ON cuax_cua_id = cua_id
WHERE cuax_cus_id = ' . $id . ';';

    return $db->fetchOne($query);
}

# Function: getFrameworkContractUrl
# Retourne l'URL de la fiche du contrat-cadre
#
# Parameters:
# string $id - Identifiant du contrat-cadre
#
# Returns:
# string - URL de la fiche du contrat-cadre
sub getFrameworkContractUrl
{
    my ($id) = @_;

    my $dev = &LOC::Globals::get('dev');
    return ($id ? $ENV{"TFL_URL"} . '/c/contrat_cadre_info.php?ccauto=' . $id . ($dev eq '' ? '' : '&dev=' . $dev) : '');
}



# Function: getRentTariffGrid
# Retourne les informations concernant la grille tarifaire de location du client
#
# Parameters:
# string $countryId - Identifiant du pays
# int    $id        - Identifiant du client
#
# Returns:
# hashref - Référence de table de hachage contenant :
# - id : l'identifiant de la grille tarifaire de transport ;
# - label : son nom.
sub getRentTariffGrid
{
    my ($countryId, $id) = @_;

    if ($countryId eq '')
    {
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $rentTariffSchemaName = 'GESTIONTARIF' . $countryId;

    # Grille perso
    my $query = '
SELECT
    tbl_f_customer.CLAUTO AS `id`,
    tbl_f_customer.CLIENTTARIFAUTO AS `baseTariff.id`,
    tbl_p_basetariff.TARIFCLIENTREF AS `baseTariff.label`,
    tbl_f_customer.CLGROUPEAUTO AS `tariffGroup.id`,
    tbl_p_rentgroup.CLGRLIBELLE AS `tariffGroup.label`,
    tbl_p_rentgrid.CLAUTO AS `personalTariff.id`,
    IF (tbl_p_rentgrid.CLAUTO > 0, tbl_f_customer.CLSTE, NULL) AS `personalTariff.label`
FROM CLIENT tbl_f_customer
LEFT JOIN CLIENTTARIF tbl_p_basetariff
    ON (tbl_f_customer.CLIENTTARIFAUTO = tbl_p_basetariff.CLIENTTARIFAUTO AND tbl_p_basetariff.TARIFCLIENTACTIF = 1)
LEFT JOIN ' . $rentTariffSchemaName . '.GC_CLGROUPE tbl_p_rentgroup
    ON tbl_f_customer.CLGROUPEAUTO = tbl_p_rentgroup.CLGROUPEAUTO
LEFT JOIN ' . $rentTariffSchemaName . '.GC_GRILLE tbl_p_rentgrid
    ON tbl_f_customer.CLAUTO = tbl_p_rentgrid.CLAUTO
WHERE tbl_f_customer.CLAUTO = ' . $db->quote($id) . '
GROUP BY tbl_f_customer.CLAUTO';

    my $tabResult = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);

    my $tabReturn = {
                     'url' => $ENV{'TFL_URL'} . '/c/grille_consult.php?clauto=' . $id,
                    };

    $tabResult->{'baseTariff.id'}     = $tabResult->{'baseTariff.id'} * 1;
    $tabResult->{'tariffGroup.id'}    = $tabResult->{'tariffGroup.id'} * 1;
    $tabResult->{'personalTariff.id'} = $tabResult->{'personalTariff.id'} * 1;

    if ($tabResult->{'personalTariff.id'} != 0)
    {
        $tabReturn->{'id'}    = $tabResult->{'personalTariff.id'};
        $tabReturn->{'label'} = $tabResult->{'personalTariff.label'};
    }
    elsif ($tabResult->{'tariffGroup.id'} != 0)
    {
        $tabReturn->{'id'}    = $tabResult->{'tariffGroup.id'};
        $tabReturn->{'label'} = $tabResult->{'tariffGroup.label'};
    }
    else
    {
        $tabReturn->{'id'}    = $tabResult->{'baseTariff.id'};
        $tabReturn->{'label'} = $tabResult->{'baseTariff.label'};
    }

    return $tabReturn;
}



# Function: getTransportTariffGrid
# Retourne les informations concernant la grille tarifaire de transport du client
#
# Parameters:
# string $countryId - Identifiant du pays
# int    $id        - Identifiant du client
#
# Returns:
# hashref - Référence de table de hachage contenant :
# - id : l'identifiant de la grille tarifaire de transport ;
# - label : son nom.
sub getTransportTariffGrid
{
    my ($countryId, $id) = @_;

    if ($countryId eq '')
    {
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $trspTariffSchemaName = 'GESTIONTRANS' . $countryId;

    my $tabResult;

    # Grille perso
    my $query = '
SELECT
    tbl_p_transportgrid.ID AS `id`,
    CONCAT(tbl_p_transportgrid.TYPEGRILLE, tbl_p_transportgrid.VALEUR) AS `label`,
    tbl_p_transportgrid.TYPEGRILLE AS `type`
FROM CLIENT tbl_f_customer
INNER JOIN ' . $trspTariffSchemaName . '.GRILLE tbl_p_transportgrid
    ON (tbl_p_transportgrid.TYPEGRILLE = "PERSO" AND
        tbl_f_customer.CLAUTO = tbl_p_transportgrid.VALEUR AND
        tbl_p_transportgrid.ETAT = "ETAT01")
WHERE tbl_f_customer.CLAUTO = ' . $db->quote($id);

    $tabResult = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if ($tabResult)
    {
        $tabResult->{'id'} = $tabResult->{'id'} * 1;
        return $tabResult;
    }

    # Grille de groupe
    $query = '
SELECT
    tbl_p_transportgrid.ID AS `id`,
    CONCAT_WS(" ", tbl_p_transportgrid.TYPEGRILLE, tbl_p_transportgrid.VALEUR) AS `label`,
    tbl_p_transportgrid.TYPEGRILLE AS `type`
FROM CLIENT tbl_f_customer
INNER JOIN ' . $trspTariffSchemaName . '.CLIENTGROUPE tbl_f_customer_transportgroup
    ON tbl_f_customer.CLAUTO = tbl_f_customer_transportgroup.CLIENT
INNER JOIN ' . $trspTariffSchemaName . '.GRILLE tbl_p_transportgrid
    ON (tbl_p_transportgrid.TYPEGRILLE = "GROUPE" AND
        tbl_f_customer_transportgroup.GRILLE = tbl_p_transportgrid.ID AND
        tbl_p_transportgrid.ETAT = "ETAT01")
WHERE tbl_f_customer.CLAUTO = ' . $db->quote($id);

    $tabResult = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if ($tabResult)
    {
        $tabResult->{'id'} = $tabResult->{'id'} * 1;
        return $tabResult;
    }

    # Grille de base
    $query = '
SELECT
    tbl_p_transportgrid.ID AS `id`,
    CONCAT(tbl_p_transportgrid.TYPEGRILLE, tbl_p_transportgrid.VALEUR) AS `label`,
    tbl_p_transportgrid.TYPEGRILLE AS `type`
FROM CLIENT tbl_f_customer
INNER JOIN ' . $trspTariffSchemaName . '.GRILLE tbl_p_transportgrid
    ON (tbl_p_transportgrid.TYPEGRILLE = "GT" AND
        IF(tbl_f_customer.CLCLASSE = "P", "C", tbl_f_customer.CLCLASSE) = tbl_p_transportgrid.VALEUR AND
        tbl_p_transportgrid.ETAT = "ETAT01")
WHERE tbl_f_customer.CLAUTO = ' . $db->quote($id);

    $tabResult = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if ($tabResult)
    {
        $tabResult->{'id'} = $tabResult->{'id'} * 1;
        return $tabResult;
    }
}

# Function: hasCRCAction
# Indique si le client a un type d'action spécifique actif sur la fiche CRC
#
# Parameters:
# string $countryId   - Pays
# string $id          - Identifiant du client
# string $actionId    - Identifiant du type d'action
sub hasCRCAction
{
    my ($countryId, $id, $actionId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('crc', $countryId);

    my $query = '
SELECT count(*)
FROM f_dataentry
INNER JOIN f_customeraccountex ON cuax_cua_id = dat_cua_id
WHERE cuax_cus_id = ' . $id . '
AND dat_slo_id = ' . $actionId . '
AND dat_state = 1;';

    my $result = $db->fetchOne($query);

    return ($result > 0 ? 1 : 0);
}

# Function: insert
# Crée un nouveau client
#
# Parameters:
# string  $countryId   - Identifiant du pays
# hashref $tabData    - Données à insérer
# int     $userId     - Utilisateur responsable de la modification
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Mises à jour de fin
# 
sub insert
{
    my ($countryId, $tabData, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Paramètres
    my $statCode           = (exists $tabData->{'statCode'} ? $tabData->{'statCode'} : undef);
    my $label              = (exists $tabData->{'label'} ? $tabData->{'label'} : undef);
    my $address            = (exists $tabData->{'address'} ? $tabData->{'address'} : undef);
    my $address2           = (exists $tabData->{'address2'} ? $tabData->{'address2'} : undef);
    my $localityId         = (exists $tabData->{'locality.id'} ? $tabData->{'locality.id'} : 0);
    my $telephone          = (exists $tabData->{'telephone'} ? $tabData->{'telephone'} : undef);
    my $mobile             = (exists $tabData->{'mobile'} ? $tabData->{'mobile'} : undef);
    my $fax                = (exists $tabData->{'fax'} ? $tabData->{'fax'} : undef);
    my $email              = (exists $tabData->{'email'} ? $tabData->{'email'} : undef);
    my $contactName        = (exists $tabData->{'contactName'} ? $tabData->{'contactName'} : undef);
    my $comment            = (exists $tabData->{'comment'} ? $tabData->{'comment'} : undef);
    my $verificationStatus = (exists $tabData->{'verificationStatus'} ? $tabData->{'verificationStatus'} : undef);
    my $statement          = (exists $tabData->{'statement'} ? $tabData->{'statement'} : undef);
    my $paymentModeId      = (exists $tabData->{'paymentMode.id'} ? $tabData->{'paymentMode.id'} : 0);
    my $activityStatCode   = (exists $tabData->{'activity.statCode'} ? $tabData->{'activity.statCode'} : 0);
    my $bank               = (exists $tabData->{'bank'} ? $tabData->{'bank'} : undef);
    my $activityId         = (exists $tabData->{'activity.id'} ? $tabData->{'activity.id'} : 0);
    my $baseTariffId       = (exists $tabData->{'baseTariff.id'} ? $tabData->{'baseTariff.id'} : 0);
    my $rentTariffGroupId  = (exists $tabData->{'rentTariffGroup.id'} ? $tabData->{'rentTariffGroup.id'} : 0);
    my $class              = (exists $tabData->{'class'} ? $tabData->{'class'} : undef);
    my $definition         = (exists $tabData->{'definition'} ? $tabData->{'definition'} : undef);
    my $creatorId          = (exists $tabData->{'creator.id'} ? $tabData->{'creator.id'} : $userId);
    my $creationAgencyId   = (exists $tabData->{'creationAgency.id'} ? $tabData->{'creationAgency.id'} : undef);
    my $isJoinDoc          = (exists $tabData->{'isJoinDoc'} ? $tabData->{'isJoinDoc'} : 0);


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Agence du client = Agence de l'utilisateur responsable par défaut
    my $tabUserInfos = &LOC::User::getInfos($userId);
    my $agencyId = $tabUserInfos->{'agency.id'};

    # Valeur par défaut de la coche "Paiement avant livraison"
    my $isProformaRequired = &LOC::Characteristic::getCountryValueByCode('CLPROFORMA', $countryId, undef, 0) * 1;
    $isProformaRequired = ($isProformaRequired ? -1 : 0);

    # Valeur par défaut de la coche "Facturation de la participation au recyclage"
    my $isResiduesInvoiced = &LOC::Characteristic::getCountryValueByCode('DECHETSMODE', $countryId);
    $isResiduesInvoiced = ($isResiduesInvoiced ? -1 : 0);

    # Valeur par défaut de l'assurance
    my $insuranceTypeId = &LOC::Insurance::Type::getDefaultId($countryId);

    # Valeur par défaut de la "Facturation de la gestion de recours"
    my $appealTypeId = undef;
    my $isAppealInvoiced = &LOC::Characteristic::getCountryValueByCode('RECOURSMODE', $countryId);
    if ($isAppealInvoiced)
    {
        my $tabInsuranceInfos = &LOC::Insurance::Type::getInfos($countryId, $insuranceTypeId);
        $appealTypeId = $tabInsuranceInfos->{'appeal.id'};
    }

    my $tabUpdates = {
        'AGAUTO'          => $agencyId,
        'CLSIREN'         => $statCode,
        'CLSTE'           => $label,
        'CLADRESSE'       => $address,
        'CLADRESSE2'      => $address2,
        'VICLAUTO'        => $localityId,
        'CLTEL'           => $telephone,
        'CLPORTABLE'      => $mobile,
        'CLFAX'           => $fax,
        'CLEMAIL'         => $email,
        'CLCONTACT'       => $contactName,
        'CLCOMMENTAIRE'   => $comment,
        'CLVERIFIER'      => $verificationStatus,
        'CLRIB'           => $statement,
        'MOPAAUTO'        => $paymentModeId,
        'CLAPE'           => $activityStatCode,
        'CLBANQUE'        => $bank,
        'METIERAUTO'      => $activityId,
        'CLIENTTARIFAUTO' => $baseTariffId,
        'CLGROUPEAUTO'    => $rentTariffGroupId,
        'CLDATECREATION*' => 'NOW()',
        'CLCLASSE'        => $class,
        'CLPEAUTOCREATE'  => $userId,
        'CLQUALIFICATION' => $definition,
        'CLDECHETSFACT'   => $isResiduesInvoiced,
        'CLPROFORMA'      => $isProformaRequired,
        'PEAUTOCREATEUR'  => $creatorId,
        'AGAUTOCREATION'  => $creationAgencyId,
        'CLJOINDOCMACH'   => $isJoinDoc,
        'CLTYPEASSURANCE' => $insuranceTypeId,
        'CLTYPERECOURS'   => $appealTypeId
    };


    # Insertion du client
    my $customerId = $db->insert('CLIENT', $tabUpdates, $schemaName);
    if ($customerId == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        print STDERR sprintf($fatalErrMsg, 'insert', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }
    
    # Mise à jour alerte "Clients à vérifier"
    &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::CUSTOMERSTOCHECK, +1);

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }
    
    return $customerId;
}

# Function: isNewAccount
# Indique s'il s'agit d'un nouveau compte
#
# Parameters:
# string $countryId      - Pays
# string $code           - Code du client
# string $accountingCode - Collectif du client
#
# Returns:
# bool
sub isNewAccount
{
    my ($countryId, $code, $accountingCode) = @_;

    # Ouverture de compte client à faire
    my $regCtrlAccounts = &LOC::Characteristic::getCountryValueByCode('OUVCPTE', $countryId);
    return ($code eq '' || $accountingCode =~ /^$regCtrlAccounts$/ ? 1 : 0);
}

# Function: isUnderSurveillance
# Le client est-il sous surveillance ?
#
# Parameters:
# string $countryId      - Identifiant du pays
# string $accountingCode - Code collectif du client
#
# Returns:
# bool
sub isUnderSurveillance
{
    my ($countryId, $accountingCode) = @_;
    my $regExp = &LOC::Characteristic::getCountryValueByCode('COLSURD', $countryId);
    if ($regExp ne '' && $accountingCode =~ /^$regExp$/)
    {
        return 1;
    }

    return 0;
}


# Function: logLockingCustomer
# Enregistre dans le fichier trace/TraceBlocagesClients_AAAAMM.csv les modifications sur le blocage de client
#
# Parameters:
# int $userId               - Utilisateur responsable de la modification
# string $code              - Code du client (CLCODE)
# int $id                   - Id du client (CLAUTO)
# int $oldIsLocked          - Client bloqué avant modif (CLSAGE)
# int $newIsLocked          - Client bloqué après modif (CLSAGE)
# int $oldIsTempUnlock      - Client débloqué temporairement avant modif (CLTMPUNLOCK)
# int $newIsTempUnlock      - Client débloqué temporairement après modif (CLTMPUNLOCK)
# string $oldAccountingCode - Code collectif du client avant modif (CLCOLLECTIF)
# string $newAccountingCode - Code collectif du client après modif (CLCOLLECTIF)
# string $functionName      - Nom de la fonction dans laquelle la modif est faite
#
# Returns:
# bool - 1 si insertion réussie, 0 sinon
sub logLockingCustomer
{
    my $sep = ';';
    my $userId = $_[0];

    # Informations sur l'utilisateur responsable de la modification
    my $tabUserInfos;
    if (defined $userId)
    {
        if ($userId > 0)
        {
            $tabUserInfos = &LOC::User::getInfos($userId);
        }
        else
        {
            $tabUserInfos = {
                'login' => 'nobody',
                'nomadAgency.id' => '---'
            };
        }
    }
    else
    {
        $tabUserInfos = &LOC::Session::getUserInfos();
    }

    my ($sec, $min, $hr, $jourmr, $mois, $an, $joursem, $jouran, $ete) = localtime;
    $an += 1900;
    $mois++;
    my $date = sprintf('%02d/%02d/%04d %02d:%02d:%02d', $jourmr, $mois, $an, $hr, $min, $sec);
    my $commonFilesPath = &LOC::Util::getCommonFilesPath('logs/customer/');
    my $nomFich = 'LocksCustomer_' . sprintf('%04d-%02d',$an,$mois) . '.csv';
    my $nomCompletFich = $commonFilesPath . '/' . $nomFich;
    my $lastParamIndex = (@_ - 1);
    my @tabColumns = @_[1..$lastParamIndex];
    my $line = '';

    open (TRACE, ">>$nomCompletFich")
        or return 0;

    # Si le fichier est vide : écrire la ligne d'en-tête.
    if (-z $nomCompletFich)
    {
        $line = 'DATE' . $sep . 'UTILISATEUR' . $sep . 'AGENCE' . $sep . 'CLCODE' . $sep . 'CLAUTO'
            . $sep . 'CLSAGE 1' . $sep . 'CLSAGE 2' . $sep . 'CLTMPUNLOCK 1' . $sep . 'CLTMPUNLOCK 2'
            . $sep . 'CLCOLLECTIF 1' . $sep . 'CLCOLLECTIF 2' . $sep . qq{FICHIER \r\n};
        print TRACE $line;
    }
    # Ecriture des lignes de trace
    $line = $date . $sep . $tabUserInfos->{'login'} . $sep . $tabUserInfos->{'nomadAgency.id'} . $sep . join($sep, @tabColumns) . qq{\r\n};
    print TRACE $line;
    close TRACE;

    return 1;
}

# Function: sendUnlockRequest
# Envoie une demande de déblocage temporaire du client aux gestionnaires CRC
#
# Parameters:
# string  $countryId        - Identifiant du pays
# int     $id               - Identifiant du client
# string  $type             - Type de demande (create: nouveau, update: prolongation)
# int     $tabDocumentInfos - Données sur le document (devis ou contrat)
# hashref $tabData          - Données pour la demande
# int     $userId           - Identifiant de l'utilisateur demandeur
sub sendUnlockRequest
{
    my ($countryId, $id, $type, $tabDocumentInfos, $tabData, $userId) = @_;

    my $db = &LOC::Db::getConnection('location', $countryId);

    # Récupération des informations nécessaires pour le mail
    my $tabCustomerInfos = &getInfos($countryId, $id);

    # Lien vers la fiche client CRC
    $tabCustomerInfos->{'appCRC.url'} = &getCRCUrl($countryId, $id);

    my $tabModelInfos = {};
    if ($tabDocumentInfos->{'modelId'})
    {
        $tabModelInfos = &LOC::Model::getInfos($countryId, $tabDocumentInfos->{'modelId'});
    }

    # dans le cas d'une mise à jour, récupération des anciennes infos du document
    if ($type eq 'update')
    {
        if ($tabDocumentInfos->{'type'} eq 'contract')
        {
            my $tabContractInfos = &LOC::Contract::Rent::getInfos($countryId, $tabDocumentInfos->{'id'});
            if ($tabContractInfos)
            {
                $tabDocumentInfos->{'oldBeginDate'} = $tabContractInfos->{'beginDate'};
                $tabDocumentInfos->{'oldEndDate'}   = $tabContractInfos->{'endDate'};
                $tabDocumentInfos->{'oldDuration'}  = $tabContractInfos->{'duration'};
                $tabDocumentInfos->{'oldAmount'} = $tabContractInfos->{'total'};
            }
        }
        else
        {
            my $tabEstimateInfos = &LOC::Estimate::Rent::getInfos($countryId, $tabDocumentInfos->{'id'});
            if ($tabEstimateInfos)
            {
                $tabDocumentInfos->{'oldAmount'} = $tabEstimateInfos->{'total'};
            }
        }
    }

    # Informations sur l'utilisateur (avec optimisation)
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Informations de l'agence de la personne connnectée
    my $tabAgencyInfos = &LOC::Agency::getInfos($tabUserInfos->{'nomadAgency.id'});

    # Création d'un mail
    my $mailObj = LOC::Mail->new();

    # Encodage
    $mailObj->setEncodingType(LOC::Mail::ENCODING_TYPE_UTF8);

    # Expéditeur
    my %tabSender = ($tabUserInfos->{'email'} => $tabUserInfos->{'firstName'} . ' ' . $tabUserInfos->{'name'});
    $mailObj->setSender(%tabSender);

    # Corps
    my $file = &LOC::Globals::get('cgiPath') . '/cfg/templates/' . $countryId . '/customer/unlockRequest.html';
    open(TPL, $file);
    my $template = join('', <TPL>);
    close(TPL);

    my $documentUrl = '';
    if ($type eq 'update')
    {
        $documentUrl = ($tabDocumentInfos->{'type'} eq 'contract' ? &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $tabDocumentInfos->{'id'}}):
                                                                    &LOC::Request::createRequestUri('rent:rentEstimate:view', {'estimateId' => $tabDocumentInfos->{'id'}}));
    }
    # Données pour le template
    my $tabReplaces = {
        'customer.code'         => $tabCustomerInfos->{'code'},
        'customer.name'         => $tabCustomerInfos->{'name'},
        'customer.url'          => $tabCustomerInfos->{'appCRC.url'},
        'unlock.type'           => $type,
        'document.agency'       => $tabDocumentInfos->{'agencyId'},
        'document.url'          => $documentUrl,
        'document.type'         => $tabDocumentInfos->{'type'},
        'model.name'            => $tabModelInfos->{'label'},
        'document.code'         => $tabDocumentInfos->{'code'},
        'document.oldBeginDate' => $tabDocumentInfos->{'oldBeginDate'},
        'document.newBeginDate' => $tabDocumentInfos->{'newBeginDate'},
        'document.oldEndDate'   => $tabDocumentInfos->{'oldEndDate'},
        'document.newEndDate'   => $tabDocumentInfos->{'newEndDate'},
        'document.oldDuration'  => $tabDocumentInfos->{'oldDuration'},
        'document.newDuration'  => $tabDocumentInfos->{'newDuration'},
        'document.oldAmount'    => $tabDocumentInfos->{'oldAmount'},
        'document.newAmount'    => $tabDocumentInfos->{'amount'},
        'document.orderNo'      => $tabDocumentInfos->{'orderNo'},
        'comment'               => $tabData->{'comment'},
        'user.firstName'        => $tabUserInfos->{'firstName'},
        'user.name'             => $tabUserInfos->{'name'},
        'agency.label'          => $tabAgencyInfos->{'label'},
        'agency.telephone'      => $tabAgencyInfos->{'telephone'},
        'agency.fax'            => $tabAgencyInfos->{'fax'},
        'agency.address'        => $tabAgencyInfos->{'address'},
        'agency.postalCode'     => $tabAgencyInfos->{'postalCode'},
        'agency.city'           => $tabAgencyInfos->{'city'}
    };

    # Formatage et remplacement des données dans le template
    $template =~ s/(\<%([a-z]{3})\:(.*?)\>)/@{[&_formatTemplate($2, $3, $tabReplaces)]}/g;

    while ($template =~ m/\<\%(.*?)\>/)
    {
        my $replace = '';
        if (exists $tabReplaces->{$1})
        {
            $replace = $tabReplaces->{$1};
        }
        $template =~ s/<%$1>/$replace/g;
    }

    # Formatage et remplacement des données dans le sujet
    my $subject = ($template =~ m/\<title\>(.*?)\<\/title\>/ ? $1 : '');

    if ($template =~ /"cid:(.+)"/ && -e &LOC::Globals::get('imgPath') . 'print/gui/' . $1)
    {
        $mailObj->addInlineAttachment(&LOC::Globals::get('imgUrl') . 'print/gui/' . $1, $1);
    }

    $mailObj->setBodyType(LOC::Mail::BODY_TYPE_HTML);

    # Récupération des destinataires (gestionnaires)
    # - Tri des types de gestionnaires
    my @tabManagerTypeRecipients;
    my @tabManagerTypeCCRecipients;
    if ($tabCustomerInfos->{'tabManagers'}->{&MANAGER_TEMP})
    {
        push(@tabManagerTypeRecipients, MANAGER_TEMP);
        push(@tabManagerTypeCCRecipients, (MANAGER_MAIN, MANAGER_SUBSTITUTE));
    }
    else
    {
        push(@tabManagerTypeRecipients, MANAGER_MAIN);
        push(@tabManagerTypeCCRecipients, MANAGER_SUBSTITUTE);
    }

    my %tabRecipients;
    my %tabCCRecipients;
    foreach my $managerType (keys(%{$tabCustomerInfos->{'tabManagers'}}))
    {
        if (&LOC::Util::in_array($managerType, \@tabManagerTypeRecipients))
        {
            $tabRecipients{$tabCustomerInfos->{'tabManagers'}->{$managerType}->{'email'}} = $tabCustomerInfos->{'tabManagers'}->{$managerType}->{'fullName'};
        }
        elsif (&LOC::Util::in_array($managerType, \@tabManagerTypeCCRecipients))
        {
            $tabCCRecipients{$tabCustomerInfos->{'tabManagers'}->{$managerType}->{'email'}} = $tabCustomerInfos->{'tabManagers'}->{$managerType}->{'fullName'};
        }
    }
    # - Ajout en copie du demandeur
    $tabCCRecipients{$tabUserInfos->{'email'}} = $tabUserInfos->{'firstName'} . ' ' . $tabUserInfos->{'name'};

    my %tabFinalRecipients;
    my %tabFinalCCRecipients;
    my $templatePlus = '';

    if (&LOC::Globals::getEnv() ne 'exp')
    {
        $subject = '[TEST] ' . $subject;

        # Informations sur l'utilisateur
        my $tabCurrentUserInfos = &LOC::Session::getUserInfos();

        if (defined $tabCurrentUserInfos)
        {
            %tabFinalRecipients = ($tabCurrentUserInfos->{'email'} => $tabCurrentUserInfos->{'firstName'} . ' ' . $tabCurrentUserInfos->{'name'});
        }
        else
        {
            # En cas !
            %tabFinalRecipients = ('archive.dev@acces-industrie.com' => 'Développeurs');
        }

        # Destinataires d'origines
        my $recipients = '';
        my $ccRecipients = '';
        foreach my $email (keys %tabRecipients)
        {
            $recipients .= $tabRecipients{$email} . ' &lt;' . $email . '&gt;<br />';
        }
        foreach my $email (keys %tabCCRecipients)
        {
            $ccRecipients .= $tabCCRecipients{$email} . ' &lt;' . $email . '&gt;<br />';
        }

        $templatePlus  = 'Destinataires d\'origine : <br/>' . $recipients;
        $templatePlus .= 'Destinataires en copie : <br />' . $ccRecipients;
    }
    else
    {
        %tabFinalRecipients   = %tabRecipients;
        %tabFinalCCRecipients = %tabCCRecipients;
    }

    $mailObj->setBody($templatePlus . $template);

    $mailObj->addRecipients(%tabFinalRecipients);

    foreach my $ccRecipientMail (keys %tabFinalCCRecipients)
    {
        $mailObj->addCCRecipient($ccRecipientMail, $tabFinalCCRecipients{$ccRecipientMail});
    }

    $mailObj->setSubject($subject);

    $mailObj->send();

    return 1;
}


# Function: update
# Mettre à jour un client
#
# Parameters:
# string   $countryId  - Pays
# string   $id         - Identifiant du client
# hashref  $tabData    - Données à mettre à jour
# int      $userId     - Utilisateur responsable de la modification
# hashref  $tabOptions - Tableau des options supplémentaires (facultatif)
# arrayref $tabErrors  - Tableau des erreurs
# hashref  $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub update
{
    my ($countryId, $id, $tabData, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération des informations du client
    my $tabInfos = &getInfos($countryId, $id, GETINFOS_BASE);
    if (!$tabInfos)
    {
        return 0;
    }

    # Paramètres

    # - Code client
    my $oldCode = $tabInfos->{'code'};
    my $newCode = (exists $tabData->{'code'} ? $tabData->{'code'} : $oldCode);

    # - Raison sociale
    my $oldName = $tabInfos->{'name'};
    my $newName = (exists $tabData->{'name'} ? $tabData->{'name'} : $oldName);

    # - Adresse 1
    my $oldAddress1 = $tabInfos->{'address1'};
    my $newAddress1 = (exists $tabData->{'address1'} ? $tabData->{'address1'} : $oldAddress1);

    # - Adresse 2
    my $oldAddress2 = $tabInfos->{'address2'};
    my $newAddress2 = (exists $tabData->{'address2'} ? $tabData->{'address2'} : $oldAddress2);

    # - Ville
    my $oldLocalityId = $tabInfos->{'locality.id'};
    my $newLocalityId = (exists $tabData->{'locality.id'} ? $tabData->{'locality.id'} : $oldLocalityId);

    # - Numéro de téléphone
    my $oldTelephone = $tabInfos->{'telephone'};
    my $newTelephone = (exists $tabData->{'telephone'} ? $tabData->{'telephone'} : $oldTelephone);

    # - Numéro de fax
    my $oldFax = $tabInfos->{'fax'};
    my $newFax = (exists $tabData->{'fax'} ? $tabData->{'fax'} : $oldFax);

    # - Numéro de SIRET
    my $oldStatCode = $tabInfos->{'statCode'};
    my $newStatCode = (exists $tabData->{'statCode'} ? $tabData->{'statCode'} : $oldStatCode);

    # - Complément du numéro de SIRET (incrément)
    my $oldStatCodeCpl = $tabInfos->{'statCodeCpl'};
    my $newStatCodeCpl = (exists $tabData->{'statCodeCpl'} ? $tabData->{'statCodeCpl'} : $oldStatCodeCpl);

    # - État de vérification
    my $oldVerificationStatus = $tabInfos->{'verificationStatus'};
    my $newVerificationStatus = (exists $tabData->{'verificationStatus'} ? $tabData->{'verificationStatus'}
                                                                         : $oldVerificationStatus);

    # - État de validité
    my $oldValidityStatus = $tabInfos->{'validityStatus'};
    my $newValidityStatus = (exists $tabData->{'validityStatus'} ? $tabData->{'validityStatus'} : $oldValidityStatus);

    # - Paiement avant livraison
    my $oldIsPfmReq = $tabInfos->{'isProformaRequired'};
    my $newIsPfmReq = (exists $tabData->{'isProformaRequired'} ? ($tabData->{'isProformaRequired'} ? 1 : 0)
                                                               : $oldIsPfmReq);

    # - A-t-il une adresse e-mail ?
    my $oldHasEmail = $tabInfos->{'hasEmail'};
    my $newHasEmail = (exists $tabData->{'hasEmail'} ? ($tabData->{'hasEmail'} ? 1 : 0) : $oldHasEmail);

    # - Adresse e-mail
    my $oldEmail = $tabInfos->{'email'};
    my $newEmail = (exists $tabData->{'email'} ? &LOC::Util::trim($tabData->{'email'}) : $oldEmail);
    if ($newEmail ne '')
    {
        $newHasEmail = 1;
    }

    # - Collectif
    my $oldAccountingCode = $tabInfos->{'accountingCode'};
    my $newAccountingCode = (exists $tabData->{'accountingCode'} ? &LOC::Util::trim($tabData->{'accountingCode'})
                                                                 : $oldAccountingCode);

    # - Blocage
    my $oldIsLocked = $tabInfos->{'isLocked'};
    my $newIsLocked = (exists $tabData->{'isLocked'} ? ($tabData->{'isLocked'} ? 1 : 0) : $oldIsLocked);

    # - Blocage temporaire
    my $oldIsTempUnlock = $tabInfos->{'isTempUnlock'};
    my $newIsTempUnlock = (exists $tabData->{'isTempUnlock'} ? ($tabData->{'isTempUnlock'} ? 1 : 0) : $oldIsTempUnlock);

    # - Mode de paiement
    my $oldPaymentModeId = $tabInfos->{'paymentMode.id'};
    my $newPaymentModeId = (exists $tabData->{'paymentMode.id'} ? $tabData->{'paymentMode.id'} * 1 : $oldPaymentModeId);

    # - Commentaire
    my $oldComments = $tabInfos->{'comments'};
    my $newComments = (exists $tabData->{'comments'} ? &LOC::Util::trim($tabData->{'comments'}) : $oldComments);
    if (exists $tabData->{'*comments'})
    {
        # Ajout avant
        $newComments = $tabData->{'*comments'} . $newComments;
    }
    if (exists $tabData->{'comments*'})
    {
        # Ajout après
        $newComments .= $tabData->{'comments*'};
    }

    # - Ouverture de compte ?
    my $oldIsNewAccount = $tabInfos->{'isNewAccount'};
    my $newIsNewAccount = &isNewAccount($countryId, $newCode, $newAccountingCode);

    # - Classe
    my $oldClass = $tabInfos->{'class'};
    my $newClass = (exists $tabData->{'class'} ? $tabData->{'class'} : $oldClass);

    # - Potentiel
    my $oldPotential = $tabInfos->{'potential'};
    my $newPotential = (exists $tabData->{'potential'} ? $tabData->{'potential'} : $oldPotential);

    # - Assurance
    my $oldInsuranceTypeId = $tabInfos->{'insuranceType.id'};
    my $oldInsuranceRate   = $tabInfos->{'insurance.rate'};
    my $oldInsuranceValidityDate = $tabInfos->{'insurance.validityDate'};
    my $oldInsuranceBeginDate    = $tabInfos->{'insurance.beginDate'};
    my $oldWishedInsuranceTypeId = $tabInfos->{'wishedInsuranceType.id'};
    my $oldWishedInsuranceRate   = $tabInfos->{'insurance.wishedRate'};

    my $newInsuranceTypeId = (exists $tabData->{'insuranceType.id'} ? $tabData->{'insuranceType.id'} : $oldInsuranceTypeId);
    my $newInsuranceRate   = (exists $tabData->{'insurance.rate'} ? $tabData->{'insurance.rate'} : $oldInsuranceRate);
    my $newInsuranceValidityDate = (exists $tabData->{'insurance.validityDate'} ? $tabData->{'insurance.validityDate'} : $oldInsuranceValidityDate);
    my $newInsuranceBeginDate    = (exists $tabData->{'insurance.beginDate'} ? $tabData->{'insurance.beginDate'} : $oldInsuranceBeginDate);
    my $newWishedInsuranceTypeId = (exists $tabData->{'wishedInsuranceType.id'} ? $tabData->{'wishedInsuranceType.id'} : $oldInsuranceTypeId);
    my $newWishedInsuranceRate   = (exists $tabData->{'insurance.wishedRate'} ? $tabData->{'insurance.wishedRate'} : $oldInsuranceRate);


    # - Gestion de recours
    my $oldAppealTypeId       = $tabInfos->{'appealType.id'};
    my $oldAppealBeginDate    = $tabInfos->{'appeal.beginDate'};
    my $oldWishedAppealTypeId = $tabInfos->{'wishedAppealType.id'};

    my $newAppealTypeId    = (exists $tabData->{'appealType.id'} ? $tabData->{'appealType.id'} : $oldAppealTypeId);
    my $newAppealBeginDate = (exists $tabData->{'appeal.beginDate'} ? $tabData->{'appeal.beginDate'} : $oldAppealBeginDate);
    my $newWishedAppealTypeId = (exists $tabData->{'wishedAppealType.id'} ? $tabData->{'wishedAppealType.id'} : $oldWishedAppealTypeId);


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Tableau des mise à jour à effectuer
    my $tabUpdates = {};
    my $tabChanges = {};


    # Mise à jour de la case "Paiement avant livraison"
    if ($oldIsPfmReq != $newIsPfmReq)
    {
        $tabUpdates->{'CLPROFORMA'} = ($newIsPfmReq ? -1 : 0);

        # Mise à jour des pro formas du client si la case "Paiement avant livraison" est décochée
        if (!$newIsPfmReq)
        {
            # Abandon des toutes les pro formas du client
            if (!&LOC::Proforma::abortAllForCustomer($countryId, $id, $userId, {'doJustPossible' => 1}, {}, $tabEndUpds))
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return 0;
            }
        }
        $tabChanges->{'isProformaRequired'} = {'old' => $oldIsPfmReq, 'new' => $newIsPfmReq};
    }
    # Code client
    if ($oldCode ne $newCode)
    {
        $tabUpdates->{'CLCODE'} = $newCode;
        $tabChanges->{'code'} = {'old' => $oldCode, 'new' => $newCode};
    }
    # Raison sociale
    if ($oldName ne $newName)
    {
        $tabUpdates->{'CLSTE'} = $newName;
        $tabChanges->{'name'} = {'old' => $oldName, 'new' => $newName};
    }
    # Adresse 1
    if ($oldAddress1 ne $newAddress1)
    {
        $tabUpdates->{'CLADRESSE'} = $newAddress1;
        $tabChanges->{'address1'} = {'old' => $oldAddress1, 'new' => $newAddress1};
    }
    # Adresse 2
    if ($oldAddress2 ne $newAddress2)
    {
        $tabUpdates->{'CLADRESSE2'} = $newAddress2;
        $tabChanges->{'address2'} = {'old' => $oldAddress2, 'new' => $newAddress2};
    }
    # Ville
    if ($oldLocalityId ne $newLocalityId)
    {
        $tabUpdates->{'VICLAUTO'} = $newLocalityId;
        $tabUpdates->{'CLCP'} = undef;
        $tabUpdates->{'CLVILLE'} = undef;
        $tabChanges->{'locality.id'} = {'old' => $oldLocalityId, 'new' => $newLocalityId};
    }
    # Numéro de téléphone
    if ($oldTelephone ne $newTelephone)
    {
        $tabUpdates->{'CLTEL'} = $newTelephone;
        $tabChanges->{'telephone'} = {'old' => $oldTelephone, 'new' => $newTelephone};
    }
    # Numéro de fax
    if ($oldFax ne $newFax)
    {
        $tabUpdates->{'CLFAX'} = $newFax;
        $tabChanges->{'fax'} = {'old' => $oldFax, 'new' => $newFax};
    }
    # Numéro de SIRET
    if ($oldStatCode ne $newStatCode)
    {
        $tabUpdates->{'CLSIREN'} = $newStatCode;
        $tabChanges->{'statCode'} = {'old' => $oldStatCode, 'new' => $newStatCode};
    }
    # Complément du numéro de SIRET (incrément)
    if ($oldStatCodeCpl ne $newStatCodeCpl)
    {
        $tabUpdates->{'CLSIRETCPL'} = $newStatCodeCpl;
        $tabChanges->{'statCodeCpl'} = {'old' => $oldStatCodeCpl, 'new' => $newStatCodeCpl};
    }
    # État de vérification
    if ($oldVerificationStatus != $newVerificationStatus)
    {
        $tabUpdates->{'CLVERIFIER'} = $newVerificationStatus;
        $tabChanges->{'verificationStatus'} = {'old' => $oldVerificationStatus, 'new' => $newVerificationStatus};
    }
    # État de validité
    if ($oldValidityStatus != $newValidityStatus)
    {
        $tabUpdates->{'CLNONVALIDE'} = $newValidityStatus;
        $tabChanges->{'validityStatus'} = {'old' => $oldValidityStatus, 'new' => $newValidityStatus};
    }
    # A-t-il une adresse e-mail ?
    if ($oldHasEmail ne $newHasEmail)
    {
        $tabUpdates->{'CLHASEMAIL'} = ($newHasEmail ? 1 : 0);
        $tabChanges->{'hasEmail'} = {'old' => $oldHasEmail, 'new' => $newHasEmail};
    }
    # Adresse e-mail
    if ($oldEmail ne $newEmail)
    {
        $tabUpdates->{'CLEMAIL'} = $newEmail;
        $tabChanges->{'email'} = {'old' => $oldEmail, 'new' => $newEmail};
    }
    # Collectif
    if ($oldAccountingCode ne $newAccountingCode)
    {
        $tabUpdates->{'CLCOLLECTIF'} = $newAccountingCode;
        $tabChanges->{'accountingCode'} = {'old' => $oldAccountingCode, 'new' => $newAccountingCode};
    }
    # - Blocage
    if ($oldIsLocked != $newIsLocked)
    {
        $tabUpdates->{'CLSAGE'} = ($newIsLocked ? -2 : 0);
        $tabChanges->{'isLocked'} = {'old' => $oldIsLocked, 'new' => $newIsLocked};
    }
    # - Blocage temporaire
    if ($oldIsTempUnlock != $newIsTempUnlock)
    {
        $tabUpdates->{'CLTMPUNLOCK'} = ($newIsTempUnlock ? 1 : 0);
        $tabChanges->{'isTempUnlock'} = {'old' => $oldIsTempUnlock, 'new' => $newIsTempUnlock};
    }
    # - Mode de règlement
    if ($oldPaymentModeId != $newPaymentModeId)
    {
        $tabUpdates->{'MOPAAUTO'} = $newPaymentModeId;
        $tabChanges->{'paymentMode.id'} = {'old' => $oldPaymentModeId, 'new' => $newPaymentModeId};
    }
    # - Commentaire
    if ($oldComments ne $newComments)
    {
        $tabUpdates->{'CLCOMMENTAIRE'} = $newComments;
        $tabChanges->{'comments'} = {'old' => $oldComments, 'new' => $newComments};
    }
    # - Ouverture de compte ?
    if ($oldIsNewAccount ne $newIsNewAccount)
    {
        $tabChanges->{'isNewAccount'} = {'old' => $oldIsNewAccount, 'new' => $newIsNewAccount};
    }
    # - Classe
    if ($oldClass ne $newClass)
    {
        $tabUpdates->{'CLCLASSE'} = $newClass;
        $tabChanges->{'class'} = {'old' => $oldClass, 'new' => $newClass};
    }
    # - Potentiel
    if ($oldPotential ne $newPotential)
    {
        $tabUpdates->{'CLPOTENTIEL'} = $newPotential;
        $tabChanges->{'potential'} = {'old' => $oldPotential, 'new' => $newPotential};
    }
    # - Assurance souhaitée / Date d'application souhaitée
    if ($oldWishedInsuranceTypeId ne $newWishedInsuranceTypeId ||
        $oldInsuranceBeginDate ne $newInsuranceBeginDate)
    {
        # -- aucune nouvelle assurance sélectionnée
        if (!$newWishedInsuranceTypeId)
        {
            $newWishedInsuranceTypeId = undef;
            $newWishedInsuranceRate   = 0;
            $newInsuranceBeginDate    = undef;
        }
        else
        {
            my $tabNewWishedInsuranceTypeInfos = &LOC::Insurance::Type::getInfos($countryId, $newWishedInsuranceTypeId);
            # Mise à jour du taux d'assurance souhaitée
            # dans le cas où le type d'assurance n'est pas "assurance facturée"
            if (!$tabNewWishedInsuranceTypeInfos->{'isInvoiced'})
            {
                $newWishedInsuranceRate = 0;
            }

            # Si date d'application de l'assurance souhaitée non renseignée :
            # assurance actuelle devient l'assurance souhaitée
            if (!$newInsuranceBeginDate)
            {
                $newInsuranceTypeId       = $newWishedInsuranceTypeId;
                $newInsuranceRate         = $newWishedInsuranceRate;
                $newWishedInsuranceTypeId = undef;
                $newWishedInsuranceRate   = 0;
                # Mise à jour de la gestion de recours
                # dans le cas où le type d'assurance n'est pas "assurée par le client"
                if (!$tabNewWishedInsuranceTypeInfos->{'isAppealActivated'})
                {
                    $newAppealTypeId       = undef;
                    $newWishedAppealTypeId = undef;
                    $newAppealBeginDate    = undef;
                }
            }
            # Date d'application de l'assurance souhaitée sur une assurance non "Gestion de recours"
            # ET on avait une date de gestion de recours souhaitée
            elsif (!$tabNewWishedInsuranceTypeInfos->{'isAppealActivated'} && $oldAppealBeginDate)
            {
                $newAppealBeginDate    = $newInsuranceBeginDate;
            }
        }
        $tabUpdates->{'CLNVASSURANCE'}  = $newWishedInsuranceTypeId;
        $tabUpdates->{'CLNVPASSURANCE'} = $newWishedInsuranceRate;
        $tabUpdates->{'CLDATEDEBASS'}   = $newInsuranceBeginDate;
    }
    # - Assurance
    if ($oldInsuranceTypeId ne $newInsuranceTypeId ||
        $oldInsuranceRate ne $newInsuranceRate)
    {
        $tabUpdates->{'CLTYPEASSURANCE'} = $newInsuranceTypeId;
        $tabUpdates->{'CLPASSURANCE'}    = $newInsuranceRate;
        $tabChanges->{'insurance'} = {
            'old' => {
                'type' => $oldInsuranceTypeId,
                'rate' => $oldInsuranceRate
            },
            'new' => {
                'type' => $newInsuranceTypeId,
                'rate' => $newInsuranceRate
            }
        };
    }
    # - Date de validité de l'assurance
    if ($oldInsuranceValidityDate ne $newInsuranceValidityDate)
    {
        $tabUpdates->{'CLDATEVALASS'}   = $newInsuranceValidityDate;
    }

    # - Gestion de recours souhaitée
    if ($oldWishedAppealTypeId ne $newWishedAppealTypeId ||
        $oldAppealBeginDate ne $newAppealBeginDate)
    {

        # - Date de gestion de recours non liée à la date d'assurance souhaitée
        if (!$newInsuranceBeginDate)
        {
            if (!$newWishedAppealTypeId)
            {
                $newWishedAppealTypeId = undef;
                $newAppealBeginDate    = undef;
            }
            else
            {
                # Si date d'application de la gestion de recours souhaitée non renseignée :
                # gestion de recours actuelle devient gestion de recours souhaitée
                if (!$newAppealBeginDate)
                {
                    $newAppealTypeId = $newWishedAppealTypeId;
                    $newWishedAppealTypeId = undef;
                    $newAppealBeginDate    = undef;
                }
                else
                {
                    my $tabNewInsuranceInfos = &LOC::Insurance::Type::getInfos($countryId, $newInsuranceTypeId);
                    # L'assurance permet la gestion de recours
                    # - ET il n'y avait pas de gestion de recours
                    # - ET la nouvelle gestion de recours est programmée
                    # => on met la gestion de recours par défaut en attendant
                    if (!$oldWishedAppealTypeId && $tabNewInsuranceInfos->{'isAppealActivated'})
                    {
                        $newAppealTypeId = &LOC::Appeal::Type::getDefaultId($countryId);
                    }
                }
            }
        }
        # - Date d'assurance
        # - ET pas de date de gestion de recours ou date de gestion de recours antérieure à la date d'assurance
        elsif (!$newAppealBeginDate ||
               $newAppealBeginDate lt $newInsuranceBeginDate ||
               !$newWishedAppealTypeId)
        {
            # -> la date de gestion de recours est mise à la même date que la date d'assurance souhaitée
            $newAppealBeginDate = $newInsuranceBeginDate;
        }
        $tabUpdates->{'CLNVTYPERECOURS'}      = $newWishedAppealTypeId;
        $tabUpdates->{'CLDATEDEBTYPERECOURS'} = $newAppealBeginDate;
    }
    # - Gestion de recours
    if ($oldAppealTypeId ne $newAppealTypeId)
    {
        $tabUpdates->{'CLTYPERECOURS'}      = $newAppealTypeId;
        $tabChanges->{'appeal'} = {'old' => $oldAppealTypeId, 'new' => $newAppealTypeId};
    }

    if (keys %$tabUpdates > 0)
    {
        if ($db->update('CLIENT', $tabUpdates, {'CLAUTO*' => $id}, $schemaName) == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }
    }

    if (keys %$tabChanges > 0)
    {
        # Changements pouvant impacter le transport ? => on le lui communique
        &LOC::Transport::onExternalChanges($countryId, {
            'customer' => {
                'id'    => $id,
                'event' => 'update',
                'props' => $tabChanges
            }
        }, $userId, undef, $tabEndUpds);

        # Changements pouvant impacter les factures ? => on le lui communique
        &LOC::Invoice::onExternalChanges($countryId, {
            'customer' => {
                'id'    => $id,
                'event' => 'update',
                'props' => $tabChanges
            }
        }, $userId, undef, $tabEndUpds);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    # Historisation
    # - Si le collectif a changé ou le blocage ou le blocage temporaire
    if ($oldIsLocked != $newIsLocked ||
        $oldIsTempUnlock != $newIsTempUnlock ||
        $oldAccountingCode ne $newAccountingCode)
    {
        &logLockingCustomer($userId,
                            $tabInfos->{'code'}, $tabInfos->{'id'},
                            ($oldIsLocked ? -2 : 0),
                            ($newIsLocked ? -2 : 0),
                            $oldIsTempUnlock,
                            $newIsTempUnlock,
                            $oldAccountingCode,
                            $newAccountingCode,
                            (exists $tabOptions->{'caller'} ? $tabOptions->{'caller'} : 'LOC::Customer::update()'));
    }

    return 1;
}


# Function: _formatTemplate
# formate une chaîne de caractères
#
# Params:
# string    $code          - quel formatage à appliquer
# string    $value         - la chaîne template à formater
# hashref   $tabReplaces   - les valeurs de la chaîne à mettre dans le template
#
# Return:
# string - la chaîne formatée
sub _formatTemplate
{
    my ($code, $value, $tabReplaces) = @_;

    my @params = split(/\|/, $value);

    if ($code eq 'cur')
    {
        return &LOC::Template::formatCurrency($value, $tabReplaces);
    }
    if ($code eq 'dte')
    {
        if ($tabReplaces->{$params[0]} ne '')
        {
            return &LOC::Template::formatDate($value, $tabReplaces);
        }
        else
        {
            return '';
        }
    }

    # affichage conditionnel (classe hidden)
    if ($code eq 'hid')
    {
        my $documentType = $tabReplaces->{'document.type'};
        my $type         = $tabReplaces->{'unlock.type'};
        my $hasNewDates  = ($tabReplaces->{'document.newBeginDate'} ne '' ? 'dates' : 'emptyDates');
        my @tabConditions = split(/,/, $params[1]);

        if (&LOC::Util::in_array($documentType, \@tabConditions) ||
            &LOC::Util::in_array($type, \@tabConditions) ||
            &LOC::Util::in_array($hasNewDates, \@tabConditions))
        {
            return $params[0];
        }
        else
        {
            return '';
        }
    }

    # Durée
    if ($code eq 'dur')
    {
        my $duration = $tabReplaces->{$params[0]} * 1;
        if ($duration < 2)
        {
            return $duration . ' ' . $params[1];
        }
        else
        {
            return $duration . ' ' . $params[2];
        }
    }

    # Type
    if ($code eq 'typ')
    {
        if ($tabReplaces->{'unlock.type'} eq $params[1])
        {
            return $params[0]
        }
        else
        {
            return '';
        }
    }

    # Document
    if ($code eq 'doc')
    {
        if ($tabReplaces->{'document.type'} eq $params[1])
        {
            return $params[0]
        }
        else
        {
            return '';
        }
    }

    return $value;
}

1;
