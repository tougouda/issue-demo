use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Model
# Module permettant d'obtenir les informations sur les modèles de machines
package LOC::Model;


# Constants: Récupération d'informations sur les modèles
# GETINFOS_ALL  - Récupération de l'ensemble des informations
# GETINFOS_BASE - Récupération des informations de base
use constant
{
    GETINFOS_ALL  => 0xFFFFFFFF,
    GETINFOS_BASE => 0x00000000
};


# Constantes: Etats des encombrements de modèles
# LOADSTATE_ACTIVE  - actif
# LOADSTATE_SLEEP   - en sommeil
# LOADSTATE_DELETED - supprimé
use constant
{
    LOADSTATE_ACTIVE  => 'ETAT01',
    LOADSTATE_SLEEP   => 'ETAT02',
    LOADSTATE_DELETED => 'ETAT03'
};

# Constantes: Typages
# TYPE_ACCESSORY - accessoire
# TYPE_SUBRENT   - sous-location
# TYPE_CATEGORYB - catégorie B
# TYPE_SALE      - vente
use constant
{
    TYPE_ACCESSORY => 'accessory',
    TYPE_SUBRENT   => 'subrent',
    TYPE_CATEGORYB => 'B',
    TYPE_SALE      => 'sale'
};

# Constantes: Groupes statistiques
# STATGROUP_MACHINES - Machines
# STATGROUP_VARIOUS  - Divers
use constant
{
    STATGROUP_MACHINES => 0,
    STATGROUP_VARIOUS  => 1
};

# Constants: Fournisseurs définis
# MANUFACTURER_SUBRENTID - Fournisseur pour la sous-location
use constant
{
    MANUFACTURER_SUBRENTID  => 41
};

# Constants: Suffixes de libellés
# SUFFIX_SUBRENT - Sous-location
use constant
{
    SUFFIX_SUBRENT => ' - SL'
};


use strict;

# Modules internes
use LOC::Agency;
use LOC::Db;
use LOC::MachinesFamily;
use LOC::TariffFamily;
use LOC::EndUpdates;


my $fatalErrMsg = 'Erreur fatale: module LOC::Model, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";

# Liste des erreurs:
#
# 0x0001 => Erreur fatale
# 0x0002 => Champs obligatoires non renseignés
# 0x0003 => Typage SL et B impossible simultanément
# 0x0004 => Typage A et B impossible simultanément
# 0x0005 => Typage SL et V impossible simultanément
# 0x0006 => Typage A et V impossible simultanément
# 0x0007 => Typage B et V impossible simultanément


# Function: getInfos
# Retourne les infos d'un modèle
#
# Contenu du hashage :
# - id => identifiant du modèle
# - fullName => libellé complet (famille de machines, constructeur, libellé du modèle)
# - label => libellé du modèle
# - workingHeight => hauteur de travail
# - floorHeight => hauteur de plancher
# - backwardation => déport
# - capacity => capacité
# - length => longueur
# - width => largeur
# - restHeight => hauteur au repos
# - weight => poids
# - stowedHeight => hauteur repliée
# - stabilizers => stabilisateurs (0 : non, 1 : oui)
# - rotation => rotation
# - motor => motricité
# - platformLength => longueur de plateforme
# - maxCapacity => capacité maximale
# - maxRange => portée maximale
# - withoutWheelsHeight => hauteur sans roulette
# - withWheelsHeight => hauteur avec roulette
# - manufacturer.id => identifiant du constructeur
# - manufacturer.label => nom du constructeur (si le pays est passé en paramètre)
# - machinesFamily.id => identifiant de la famille de machines
# - machinesFamily.fullName => nom complet de la famille de machines (si le pays est passé en paramètre)
# - tariffFamily.id => identifiant de la famille tarifaire
# - tariffFamily.fullName => nom complet de la famille tarifaire
# - localCurrencyCost => coût en devise locale
# - euroCost => coût en euros
# - isAccessory => accessoire (0 : non, 1 : oui)
# - isCleaningPossible => Le nettoyage est-il possible ? (0 : non, 1 : oui)
# - frenchRatio => ratio pour la France
# - spanishRatio => ratio pour l'Espagne
# - portugueseRatio => ratio pour le Portugal
# - moroccanRatio => ratio pour le Maroc
# - italianRatio => ratio pour l'Italie
# - load => encombrement
#
#
# Parameters:
# string  $countryId  - Identifiant du pays
# string  $id         - Identifiant du modèle
# int     $flags      - Flags de récupération
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);
    return ($tab && exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getLoadsList
# Retourne la liste des encombrements
#
# Parameters:
# string  $countryId  - Identifiant du pays
# hashref $tabFilters - Filtres de recherche
#
# Returns:
# hash|hashref|0
sub getLoadsList
{
    my ($countryId, $tabFilters) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = 'GESTIONTRANS' . $countryId;

    my $whereQuery = '';
    my $whereQueryAddId = '';
    # Ajout d'un encombrement
    if ($tabFilters->{'addId'})
    {
        $whereQueryAddId = ' OR ID = ' . $db->quote($tabFilters->{'addId'});
    }
    if ($tabFilters->{'id'} ne '')
    {
        $whereQuery .= 'AND ID IN (' . $db->quote($tabFilters->{'id'}) . ')';
    }
    else
    {
        $whereQuery .= 'AND ETAT = "' . LOADSTATE_ACTIVE . '"';
    }

    my $query = '
SELECT
    ID,
    LIBELLE
FROM ' . $schemaName . '.ENCOMBREMENT
WHERE 1
    ' . $whereQuery . $whereQueryAddId . ';';

   my $tab = $db->fetchPairs($query);

    return (wantarray ? %$tab : $tab);
}

# Function: getManufacturersList
# Retourne la liste des fournisseurs
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres à appliquer
# int     $flags      - Flags de récupération
#
# Returns:
# hash|hashref|0 - Liste des familles de machines
sub getManufacturersList
{
    my ($countryId, $format, $tabFilters, $flags) = @_;
    unless (defined $flags)
    {
        $flags = 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Filtres
    my $whereQuery = '';

    if ($tabFilters->{'id'} ne '')
    {
        $whereQuery .= ' AND F.FOAUTO IN (' . $db->quote($tabFilters->{'id'}) . ')';
    }
    elsif ($tabFilters->{'label'} ne '')
    {
        $whereQuery .= ' AND F.FOLIBELLE like \'%' . $tabFilters->{'label'} . '%\'';
    }

    if ($whereQuery ne '')
    {
        $whereQuery = ' AND (' . substr($whereQuery, 4) . ')';
    }

    my $query;
    if ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*)
FROM FOURNISSEUR F';
        $query .= '
WHERE 1 ' .
$whereQuery . ';';
    }
    else
    {
        $query = '
SELECT
    F.FOAUTO AS `id`,
    F.FOLIBELLE AS `label`
FROM FOURNISSEUR F
WHERE 1 ' .
$whereQuery . '
ORDER BY F.FOLIBELLE;';
    }

    my $tab = $db->fetchList($format, $query, {}, 'id');

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        # Formatage des données
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}                *= 1;
        }
    }

    return (wantarray ? %$tab : $tab);
}

# Function: getList
# Retourne la liste des modèles
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres ('machinesFamily' : liste des identifiants de familles de machines,
#                       'tariffFamily' : liste des identifiants de familles tarifaires,
#                       'rentable' : seulement les machines louables dans l'agence précisée,
#                       'id' : Recherche par l'identifiant,
#                       'fullName' : recherche dans le nom du modèle, celui de la famille, l'élévation, l'énergie
#                       et le nom du constructeur,
#                       'addId' : Forcer l'ajout d'un modèle de machine)
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|0 - Liste des modèles (mêmes clés que pour la méthode getInfos)
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }

    # Id de la locale pour les traductions
    my $localeId = ($tabOptions->{'locale.id'} ? $tabOptions->{'locale.id'} : 'fr_FR');


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Filtres
    my $whereQuery = '';
    my $whereQueryAddId = '';

    # - Ajout d'un modèle de machine
    if ($tabFilters->{'addId'})
    {
        $whereQueryAddId = ' OR MM.MOMAAUTO = ' . $tabFilters->{'addId'};
    }
    # - Familles de machines
    if ($tabFilters->{'machinesFamily'} ne '')
    {
        $whereQuery .= ' AND FM.FAMAAUTO IN (' . $db->quote($tabFilters->{'machinesFamily'}) . ')';
    }
    # - Familles tarifaires
    if ($tabFilters->{'tariffFamily'} ne '')
    {
        $whereQuery .= ' AND FT.FAMTARAUTO IN (' . $db->quote($tabFilters->{'tariffFamily'}) . ')';
    }
    # - Modèles louables
    if ($tabFilters->{'rentable'} ne '')
    {
        $whereQuery .= ' AND (STMORESERVABLE > 0 AND STMORESERVABLE > STMORESERVE)';
    }
    # - Recherche par l'id ou par le nom
    if ($tabFilters->{'id'})
    {
        $whereQuery .= ' AND MM.MOMAAUTO = ' . $tabFilters->{'id'};
    }
    elsif ($tabFilters->{'fullName'} ne '')
    {
        my @tabElts = split(' ', $tabFilters->{'fullName'});
        foreach my $elt (@tabElts)
        {
            my $search = $db->quote('%|' . $elt . '|%');
            $whereQuery .= ' AND REPLACE(CONCAT_WS("|", "|", MOMADESIGNATION, FM.FAMAMODELE, FM.FAMAENERGIE, FM.FAMAELEVATION, FO.FOLIBELLE, "|"), " ", "|") LIKE ' . $search;
        }
    }

    if ($whereQuery ne '')
    {
        $whereQuery = ' AND (' . substr($whereQuery, 4) . ')';
    }

    my $query;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        my $tabFuelEnergies = &LOC::MachinesFamily::getFuelEnergiesList();
        $query = '
SELECT
    MM.MOMAAUTO AS `id`,
    CONCAT_WS(" ", FM.FAMAMODELE, FM.FAMAENERGIE, FM.FAMAELEVATION, FO.FOLIBELLE, MM.MOMADESIGNATION) AS `fullName`,
    MM.MOMADESIGNATION AS `label`,
    MM.MOMAHTTRAVAIL AS `workingHeight`,
    MM.MOMAHTPLANCHER AS `floorHeight`,
    MM.MOMADEPORT AS `backwardation`,
    MM.MOMACAPACITE AS `capacity`,
    MM.MOMALNG AS `length`,
    MM.MOMALG AS `width`,
    MM.MOMAHTREPOS AS `restHeight`,
    MM.MOMAPOIDS AS `weight`,
    MM.MOMAHTPLIEE AS `stowedHeight`,
    IF(MM.MOMASTABILISATEUR = "NON" OR MM.MOMASTABILISATEUR IS NULL, 0, 1) AS `hasStabilizers`,
    MM.MOMASTABILISATEUR AS `stabilizers`,
    MM.MOMAROTATION AS `rotation`,
    MM.MOMAMOTRICITE AS `motor`,
    MM.MOMALNGPLATEFORME AS `platformLength`,
    MM.MOMACAMAX AS `maxCapacity`,
    MM.MOMACAPORTEEMAX AS `maxRange`,
    MM.MOMAHTSANSROULETTE AS `withoutWheelsHeight`,
    MM.MOMAHTAVECROULETTE AS `withWheelsHeight`,
    MM.FOAUTO AS `manufacturer.id`,
    FO.FOLIBELLE AS `manufacturer.label`,
    MM.FAMAAUTO AS `machinesFamily.id`,
    CONCAT_WS(" ", FM.FAMAMODELE, FM.FAMAENERGIE, FM.FAMAELEVATION) AS `machinesFamily.fullName`,
    FM.FAMAMODELE AS `machinesFamily.name`,
    FM.FAMAENERGIE AS `machinesFamily.energy`,
    ' . (@$tabFuelEnergies > 0 ? 'IF (FM.FAMAENERGIE IN ("' . join('", "', @$tabFuelEnergies) . '"), 1, 0)' : '0') . ' AS `machinesFamily.isFuelNeeded`,
    FM.FAMAELEVATION AS `machinesFamily.elevation`,
    MM.FAMTARAUTO AS `tariffFamily.id`,
    FT.FAMTARCODE AS `tariffFamily.code`,
    CONCAT_WS(" ", FT.FAMTARLIBELLE, FT.FAMTARSOUSLIBELLE) AS `tariffFamily.fullName`,
    FT.FAMTARCARBUQTEJOUR AS `tariffFamily.estimateFuel`,
    FT.FAMTARCARBUQTEMAX AS `tariffFamily.fuelMax`,
    p_family.fly_id AS `family.id`,
    fll_fullname AS `family.label`,
    MM.MOMACOUTFR AS `localCurrencyCost`,
    MM.MOMACOUTEU AS `euroCost`,
    IF(MM.MOMATYPAGE LIKE "%\"' . TYPE_ACCESSORY . '\"%", 1, 0) AS `isAccessory`,
    MM.MOMATYPAGE AS `tabTypes`,
    MM.MOMAGRP AS `statGroup`,';
        if ($tabFilters->{'rentable'} ne '')
        {
            $query .= '
    IF (STMORESERVABLE > 0 AND STMORESERVABLE > STMORESERVE, 1, 0) AS `isRentable`,';
        }
        $query .= '
    MM.MOMARATIOFR AS `frenchRatio`,
    MM.MOMARATIOES AS `spanishRatio`,
    MM.MOMARATIOPT AS `portugueseRatio`,
    MM.MOMARATIOMA AS `moroccanRatio`,
    MM.MOMARATIOIT AS `italianRatio`,';

        # Encombrement par pays
        my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);
        my $joins = '';
        foreach my $country (@$tabCountries)
        {
            $query .= '
    tbl_f_machineload_' . $country . '.ENCOMBREMENT AS `load_' .$country. '`,';
            $joins .= '
LEFT JOIN GESTIONTRANS' . $country . '.MACHINEENCOMBREMENT tbl_f_machineload_' . $country . '
ON tbl_f_machineload_' . $country . '.MOMAAUTO = MM.MOMAAUTO';
        }

        $query .= '
    MM.MOMACONTROLEVGP AS `isControlVgp`,
    MM.MOMAPERIODICITEVGP AS `periodicityVgp`
FROM AUTH.MODELEMACHINE MM';
        if ($tabFilters->{'rentable'} ne '')
        {
            $query .= '
LEFT JOIN AUTH.STOCKMODELE SM
ON (MM.MOMAAUTO = SM.MOMAAUTO AND SM.AGAUTO = ' . $db->quote($tabFilters->{'rentable'}) . ')';
        }
        $query .= '
LEFT JOIN FAMILLEMACHINE FM
ON MM.FAMAAUTO = FM.FAMAAUTO
LEFT JOIN FAMILLETARIFAIRE FT
ON MM.FAMTARAUTO = FT.FAMTARAUTO
LEFT JOIN FAMILLECOMMERCIALE tbl_p_businessfamily
ON FM.FACOMAUTO = tbl_p_businessfamily.FACOMAUTO
LEFT JOIN AUTH.p_family
ON tbl_p_businessfamily.fly_id = p_family.fly_id
LEFT JOIN AUTH.p_family_locale
ON (p_family.fly_id = fll_fly_id AND fll_lcl_id = "' . $localeId . '")
LEFT JOIN FOURNISSEUR FO
ON MM.FOAUTO = FO.FOAUTO
' . $joins . '
WHERE 1 ' .
$whereQuery . ' '. $whereQueryAddId . '
ORDER BY MM.MOMADESIGNATION';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*)
FROM AUTH.MODELEMACHINE MM';
        if ($tabFilters->{'rentable'} ne '')
        {
            $query .= '
LEFT JOIN AUTH.STOCKMODELE SM
ON (MM.MOMAAUTO = SM.MOMAAUTO AND SM.AGAUTO = ' . $db->quote($tabFilters->{'rentable'}) . ')';
        }
        $query .= '
LEFT JOIN FAMILLEMACHINE FM
ON MM.FAMAAUTO = FM.FAMAAUTO
LEFT JOIN FOURNISSEUR FO
ON MM.FOAUTO = FO.FOAUTO
WHERE 1 ' .
$whereQuery . ' '. $whereQueryAddId;
    }
    else
    {
        $query = '
SELECT
    MM.MOMAAUTO,
    MM.MOMADESIGNATION
FROM AUTH.MODELEMACHINE MM';
        if ($tabFilters->{'rentable'} ne '')
        {
            $query .= '
LEFT JOIN AUTH.STOCKMODELE SM
ON (MM.MOMAAUTO = SM.MOMAAUTO AND SM.AGAUTO = ' . $db->quote($tabFilters->{'rentable'}) . ')';
        }
        $query .= '
LEFT JOIN FAMILLEMACHINE FM
ON MM.FAMAAUTO = FM.FAMAAUTO
LEFT JOIN FOURNISSEUR FO
ON MM.FOAUTO = FO.FOAUTO
WHERE 1 ' .
$whereQuery . ' '. $whereQueryAddId . '
ORDER BY MM.MOMADESIGNATION';
    }

    if (defined $tabOptions->{'limit'})
    {
        $query .= '
LIMIT ';
        if (defined $tabOptions->{'limit'}->{'offset'})
        {
            $query .= $tabOptions->{'limit'}->{'offset'} . ', ';
        }
        $query .= $tabOptions->{'limit'}->{'count'};
    }

    $query .= ';';

    my $tab = $db->fetchList($format, $query, {}, 'id');

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        # Liste des pays
        my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);

        # Formatage des données
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}                 *= 1;
            $tabRow->{'tariffFamily.id'}    *= 1;
            $tabRow->{'machinesFamily.id'}  *= 1;
            $tabRow->{'manufacturer.id'}    *= 1;
            $tabRow->{'load'}               *= 1;

            # Champs comportant des nombres pouvant avoir comme valeur "undef"
            my @tabUndef = ('workingHeight', 'floorHeight', 'backwardation', 'capacity', 'length', 'width', 'restHeight',
                            'weight', 'stowedHeight', 'rotation', 'motor', 'maxCapacity', 'maxRange',
                            'withoutWheelsHeight', 'withWheelsHeight');

            foreach my $field (@tabUndef)
            {
                # s'il n'est pas undef, alors on le caste en nombre
                $tabRow->{$field} = ($tabRow->{$field} ? $tabRow->{$field} * 1 : $tabRow->{$field});
            }

            $tabRow->{'machinesFamily.isFuelNeeded'} *= 1;
            $tabRow->{'frenchRatio'}        *= 1;
            $tabRow->{'spanishRatio'}       *= 1;
            $tabRow->{'portugueseRatio'}    *= 1;
            $tabRow->{'moroccanRatio'}      *= 1;
            $tabRow->{'italianRatio'}       *= 1;
            $tabRow->{'hasStabilizers'}     *= 1;
            $tabRow->{'euroCost'}           *= 1;
            $tabRow->{'localCurrencyCost'}  *= 1;
            $tabRow->{'isAccessory'}        *= 1;
            $tabRow->{'isCleaningPossible'} = ($tabRow->{'isAccessory'} ? 0 : 1); # Pour le moment, le nettoyage est possible si ce n'est pas un accessoire

            $tabRow->{'tariffFamily.estimateFuel'} *= 1;
            $tabRow->{'tariffFamily.fuelMax'}      *= 1;

            if ($tabFilters->{'rentable'} ne '')
            {
                $tabRow->{'isRentable'} *= 1;
            }

            # Typages
            if ($tabRow->{'tabTypes'} ne '')
            {
                $tabRow->{'tabTypes'} = &LOC::Json::fromJson($tabRow->{'tabTypes'});
            }
            else
            {
                $tabRow->{'tabTypes'} = [];
            }

            # Encombrement pour le pays
            $tabRow->{'load'} = $tabRow->{'load_' . $countryId} * 1;
            # Encombrement pour tous les pays
            foreach my $country (@$tabCountries)
            {
                $tabRow->{'tabLoads'}->{$country} = $tabRow->{'load_' . $country};
            }

            # Modèle faisant l'objet de VGP
            if (!&LOC::Util::isNumeric($tabRow->{'isControlVgp'}))
            {
                my $tabIsControlVgp = &LOC::Json::fromJson($tabRow->{'isControlVgp'});
                $tabRow->{'isControlVgp'}    = $tabIsControlVgp->{$countryId};
                $tabRow->{'tabIsControlVgp'} = $tabIsControlVgp;
            }

            # Périodicité VGP
            if (defined $tabRow->{'periodicityVgp'})
            {
                if (!&LOC::Util::isNumeric($tabRow->{'periodicityVgp'}))
                {
                    my $tabPeriodicityVgp = &LOC::Json::fromJson($tabRow->{'periodicityVgp'});
                    $tabRow->{'periodicityVgp'}    = $tabPeriodicityVgp->{$countryId};
                    $tabRow->{'tabPeriodicityVgp'} = $tabPeriodicityVgp;
                }
                else
                {
                    $tabRow->{'tabPeriodicityVgp'} = {};
                    foreach my $country (@$tabCountries)
                    {
                        $tabRow->{'tabPeriodicityVgp'}->{$country} = $tabRow->{'periodicityVgp'};
                    }
                }
            }
        }
    }

    return (wantarray ? %$tab : $tab);
}

# Function: getStocksList
# Retourne le stock sur une agence
#
# Parameters:
# string  $countryId  - Identifiant du pays de connexion
# string  $agencyId   - Identifiant de l'agence
# hashref $tabFilters - Filtres ('model.id' : liste des identifiants des modèles de machines,
#                                'isRentable' : 1 => seulement les louables, 0 => seulement les non louables,
#                                'search' : recherche libre dans le libellé du modèle de machines,
#                                           le libellé de la famille de machines, l'énergie et l'élévation)
#
# Returns:
# hash|hashref|0 - Stock par modèle de machines :
# - id : identifiant de la ligne ;
# - model.id : identifiant du modèle de machines ;
# - agency.id : identifiant de l'agence ;
# - machinesFamily.label : libellé de la famille de machines ;
# - machinesFamily.elevation : élévation de la famille de machines ;
# - machinesFamily.energy : énergie de la famille de machines
# - model.label : libellé du modèle de machines ;
# - model.weight : poids du modèle de machines ;
# - nbBookable : nombre de machines réservables ;
# - nbBooked : nombre de machines réservées ;
# - nbTotal : nombre total de machines ;
# - nbRented : nombre de machines louées ;
# - nbAvailable : nombre de machines disponibles ;
# - nbRecovery : nombre de machines en récupération ;
# - nbReview : nombre de machines en révision ;
# - nbControl : nombre de machines en contrôle ;
# - nbBroken : nombre de machines en panne ;
# - nbCheck : nombre de machines en vérification ;
# - nbTransfer : nombre de machines en transfert ;
# - nbDelivery : nombre de machines en livraison ;
# - isRentable : indique si le modèle est louable sur l'agence.
sub getStocksList
{
    my ($countryId, $agencyId, $tabFilters) = @_;


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);


    my @tabWheres = ();

    # Filtre sur l'agence
    push(@tabWheres, 'SM.AGAUTO = ' . $db->quote($agencyId));

    # Filtre sur le modèle
    if (defined $tabFilters->{'model.id'})
    {
        push(@tabWheres, 'SM.MOMAAUTO IN (' . $db->quote($tabFilters->{'model.id'}) . ')');
    }
    # Filtre si louable ou non
    if (defined $tabFilters->{'isRentable'})
    {
        push(@tabWheres, ($tabFilters->{'isRentable'} ? '' : 'NOT ') .
                         '(SM.STMORESERVABLE > 0 AND SM.STMORESERVABLE > SM.STMORESERVE)');
    }
    # Filtre sur le libellé du modèle
    if (defined $tabFilters->{'search'})
    {
        my @tabSearch = split(/ /, $tabFilters->{'search'});

        foreach my $search (@tabSearch)
        {
            if ($search !~ /%/)
            {
                $search = '%' . $search . '%';
            }
            push(@tabWheres, '(FM.FAMAMODELE LIKE ' . $db->quote($search) . ' OR ' .
                              'FM.FAMAELEVATION LIKE ' . $db->quote($search) . ' OR ' .
                              'FM.FAMAENERGIE LIKE ' . $db->quote($search) . ' OR ' .
                              'MM.MOMADESIGNATION LIKE ' . $db->quote($search) . ')');
        }
    }

    my $query = '
SELECT
    SM.STMOAUTO AS `id`,
    SM.MOMAAUTO AS `model.id`,
    SM.AGAUTO AS `agency.id`,
    FM.FAMAMODELE AS `machinesFamily.label`,
    FM.FAMAELEVATION AS `machinesFamily.elevation`,
    FM.FAMAENERGIE AS `machinesFamily.energy`,
    MM.MOMADESIGNATION AS `model.label`,
    MM.MOMAPOIDS AS `model.weight`,
    SM.STMORESERVABLE AS `nbBookable`,
    SM.STMORESERVABLE - (SM.STMOREC + SM.STMOVER + SM.STMOREV + SM.STMODIS) AS `nbBookableFromOthersAgencies`,
    SM.STMORESERVE AS `nbBooked`,
    SM.STMOTOTAL AS `nbTotal`,
    SM.STMOLOU AS `nbRented`,
    SM.STMODIS AS `nbAvailable`,
    SM.STMOREC AS `nbRecovery`,
    SM.STMOREV AS `nbReview`,
    SM.STMOCON AS `nbControl`,
    SM.STMOPAN AS `nbBroken`,
    SM.STMOVER AS `nbCheck`,
    SM.STMOTRA AS `nbTransfer`,
    SM.STMOLIV AS `nbDelivery`,
    IF (SM.STMORESERVABLE > 0 AND SM.STMORESERVABLE > SM.STMORESERVE, 1, 0) AS `isRentable`
FROM AUTH.STOCKMODELE SM
INNER JOIN AUTH.MODELEMACHINE MM
ON MM.MOMAAUTO = SM.MOMAAUTO
LEFT JOIN FAMILLEMACHINE FM
ON MM.FAMAAUTO = FM.FAMAAUTO';
    if (@tabWheres > 0)
    {
        $query .= '
WHERE ' . join("\nAND ", @tabWheres);
    }
    $query .= '
ORDER BY FM.FAMAMODELE, FM.FAMAELEVATION, FM.FAMAENERGIE, MM.MOMADESIGNATION';

    my $tabResult = $db->fetchAssoc($query, {}, 'model.id');

    # Formatage des données
    foreach my $tabRow (values %$tabResult)
    {
        # Formatage des données
        $tabRow->{'id'}           *= 1;
        $tabRow->{'model.id'}     *= 1;
        $tabRow->{'model.weight'} *= 1;
        $tabRow->{'nbBookable'}   *= 1;
        $tabRow->{'nbBooked'}     *= 1;
        $tabRow->{'nbTotal'}      *= 1;
        $tabRow->{'nbRented'}     *= 1;
        $tabRow->{'nbAvailable'}  *= 1;
        $tabRow->{'nbRecovery'}   *= 1;
        $tabRow->{'nbReview'}     *= 1;
        $tabRow->{'nbControl'}    *= 1;
        $tabRow->{'nbBroken'}     *= 1;
        $tabRow->{'nbCheck'}      *= 1;
        $tabRow->{'nbTransfer'}   *= 1;
        $tabRow->{'nbDelivery'}   *= 1;
        $tabRow->{'isRentable'}   *= 1;
    }

    return (wantarray ? %$tabResult : $tabResult);
}


# Function: getStockInfos
# Retourne le stock pour un modèle de machine sur une agence
#
# Parameters:
# string  $countryId - Identifiant du pays
# string  $agencyId  - Identifiant de l'agence
# int     $modelId   - Identifiant du modèle de machine
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getStockInfos
{
    my ($countryId, $agencyId, $modelId) = @_;

    my $tab = &getStocksList($countryId, $agencyId, {'model.id' => $modelId});
    return ($tab && exists $tab->{$modelId} ? (wantarray ? %{$tab->{$modelId}} : $tab->{$modelId}) : undef);
}


# Function: getUseRate
# Retourne le taux d'utilisation instantané du modèle dans l'agence et dans le pays
#
# Parameters:
# string $id       - Identifiant du modèle
# string $agencyId - Identifiant de l'agence
#
# Returns:
# hash|float|0 - Retourne le taux d'utilisation ('useRate'), le nombre de machines louées ('nbRented') et
#                le nombre total de machines ('nbTotal') dans l'agence ('agency'),la région ('area')
#                et dans le pays ('country') ou 0 si pas de résultat
sub getUseRate
{
    my ($id, $agencyId) = @_;

    # connexion à AUTH
    my $db = &LOC::Db::getConnection('auth');

    # Calcul du taux d'utilisation instantané dans l'agence
    my $query = '
SELECT
    IFNULL(SUM(SM.STMOLOU) + SUM(SM.STMOLIV), 0) AS `nbRented`,
    IFNULL(SUM(SM.STMOLIV), 0) AS `nbDelivery`,
    IFNULL(SUM(SM.STMOTOTAL), 0) AS `nbTotal`,
    agc_cty_id AS `agency.country.id`,
    agc_are_id AS `agency.area.id`
FROM STOCKMODELE SM
INNER JOIN frmwrk.p_agency
ON agc_id = SM.AGAUTO
WHERE SM.MOMAAUTO = ' . $id . '
AND SM.AGAUTO = ' . $db->quote($agencyId) . ';';
    my $tab = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if (!$tab)
    {
        return 0;
    }

    # Retour sous forme de tableau ?
    my $useRate = &LOC::Util::round(($tab->{'nbTotal'} != 0 ? $tab->{'nbRented'} / $tab->{'nbTotal'} : 0), 4);
    if (wantarray)
    {
        my %tabResult = (
            'nbRented' => {
                'agency' => $tab->{'nbRented'} * 1,
                'area'   => undef,
                'country' => undef
            },
            'nbTotal'  => {
                'agency' => $tab->{'nbTotal'} * 1,
                'area'   => undef,
                'country' => undef
            },
            'useRate'  => {
                'agency' => $useRate,
                'area'   => undef,
                'country' => undef
            }
        );
        my $countryId = $tab->{'agency.country.id'};
        my $areaId    = $tab->{'agency.area.id'} * 1;

        # Calcul du taux d'utilisation instantané dans la région et le pays
        my $quotedAreaId    = $db->quote($areaId);
        my $quotedCountryId = $db->quote($countryId);
        $query = '
SELECT
    IFNULL(SUM(IF(agc_are_id = ' . $quotedAreaId . ', SM.STMOLOU, 0))
            + SUM(IF(agc_are_id = ' . $quotedAreaId . ', SM.STMOLIV, 0)), 0) AS `area.nbRented`,
    IFNULL(SUM(IF(agc_are_id = ' . $quotedAreaId . ', SM.STMOTOTAL, 0)), 0) AS `area.nbTotal`,
    IFNULL(SUM(IF(agc_cty_id = ' . $quotedCountryId . ', SM.STMOLOU, 0))
            + SUM(IF(agc_cty_id = ' . $quotedCountryId . ', SM.STMOLIV, 0)), 0) AS `country.nbRented`,
    IFNULL(SUM(IF(agc_cty_id = ' . $quotedCountryId . ', SM.STMOTOTAL, 0)), 0) AS `country.nbTotal`
FROM STOCKMODELE SM
INNER JOIN frmwrk.p_agency
ON agc_id = SM.AGAUTO AND
   (agc_are_id = ' . $quotedAreaId . ' OR agc_cty_id = ' . $quotedCountryId . ') AND
   agc_type = ' . $db->quote(LOC::Agency::TYPE_AGENCY) . ' AND
   agc_sta_id <> ' . $db->quote(LOC::Agency::STATE_DELETED) . '
WHERE SM.MOMAAUTO = ' . $id . ';';
        $tab = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
        if ($areaId)
        {
            $tabResult{'nbRented'}->{'area'} = $tab->{'area.nbRented'} * 1;
            $tabResult{'nbTotal'}->{'area'}  = $tab->{'area.nbTotal'} * 1;
            $tabResult{'useRate'}->{'area'}  = &LOC::Util::round(($tab->{'area.nbTotal'} != 0 ?
                                                    $tab->{'area.nbRented'} / $tab->{'area.nbTotal'} : 0), 4);
        }
        $tabResult{'nbRented'}->{'country'} = $tab->{'country.nbRented'} * 1;
        $tabResult{'nbTotal'}->{'country'}  = $tab->{'country.nbTotal'} * 1;
        $tabResult{'useRate'}->{'country'}  = &LOC::Util::round(($tab->{'country.nbTotal'} != 0 ?
                                                  $tab->{'country.nbRented'} / $tab->{'country.nbTotal'} : 0), 4);

        return %tabResult;
    }
    else
    {
        return $useRate;
    }
}


# Function: insert
# Ajoute un modèle de machines
#
# Parameters:
# string  $countryId  - Identifiant du pays
# hashref $tabData    - Données pour l'insertion du modèle
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Tableau des mises à jour de fin (facultatif)
#
# Returns:
# int|0 - Identifiant du modèle créé, 0 en cas d'erreur
sub insert
{
    my ($countryId, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Formatage des données
    my $machinesFamilyId  = (exists $tabData->{'machinesFamily.id'} ? $tabData->{'machinesFamily.id'} : 0);
    my $tariffFamilyId    = (exists $tabData->{'tariffFamily.id'} ? $tabData->{'tariffFamily.id'} : 0);
    my $manufacturerId    = (exists $tabData->{'manufacturer.id'} ? $tabData->{'manufacturer.id'} : 0);
    my $label             = (exists $tabData->{'label'} ? &LOC::Util::trim($tabData->{'label'}) : '');
    my $length            = (exists $tabData->{'length'} ? $tabData->{'length'} : undef);
    my $width             = (exists $tabData->{'width'} ? $tabData->{'width'} : undef);
    my $weight            = (exists $tabData->{'weight'} ? $tabData->{'weight'} : undef);
    my $workingHeight     = (exists $tabData->{'workingHeight'} ? $tabData->{'workingHeight'} : undef);
    my $floorHeight       = (exists $tabData->{'floorHeight'} ? $tabData->{'floorHeight'} : undef);
    my $restHeight        = (exists $tabData->{'restHeight'} ? $tabData->{'restHeight'} : undef);
    my $stowedHeight      = (exists $tabData->{'stowedHeight'} ? $tabData->{'stowedHeight'} : undef);
    my $backwardation     = (exists $tabData->{'backwardation'} ? $tabData->{'backwardation'} : undef);
    my $capacity          = (exists $tabData->{'capacity'} ? $tabData->{'capacity'} : undef);
    my $maxCapacity       = (exists $tabData->{'maxCapacity'} ? $tabData->{'maxCapacity'} : undef);
    my $maxRange          = (exists $tabData->{'maxRange'} ? $tabData->{'maxRange'} : undef);
    my $platformLength    = (exists $tabData->{'platformLength'} ? $tabData->{'platformLength'} : undef);
    my $rotation          = (exists $tabData->{'rotation'} ? $tabData->{'rotation'} : undef);
    my $stabilizers       = (exists $tabData->{'stabilizers'} ? $tabData->{'stabilizers'} : undef);
    my $motor             = (exists $tabData->{'motor'} ? $tabData->{'motor'} : undef);
    my $localCurrencyCost = (exists $tabData->{'localCurrencyCost'} ? $tabData->{'localCurrencyCost'} : 0);
    my $tabTypes          = (exists $tabData->{'tabTypes'} ? $tabData->{'tabTypes'} : []);
    my $tabLoads          = (exists $tabData->{'tabLoads'} ? $tabData->{'tabLoads'} : undef);
    my $periodicityVGP    = (exists $tabData->{'tabPeriodicityVgp'} ? $tabData->{'tabPeriodicityVgp'} : undef);
    my $statGroup         = (exists $tabData->{'statGroup'} ? $tabData->{'statGroup'} : undef);
    # Par défaut, MOMAGRP à 0 pour les machines et à 1 pour les accessoires
    if (!defined $statGroup)
    {
        $statGroup = (&LOC::Util::in_array(TYPE_ACCESSORY, $tabTypes) ? STATGROUP_VARIOUS : STATGROUP_MACHINES);
    }

    # Erreurs
    # - Champs obligatoires non renseignés
    if ($machinesFamilyId == 0 || $tariffFamilyId == 0 || $manufacturerId == 0 || $label eq '')
    {
        push(@{$tabErrors->{'fatal'}}, 0x0002);
        return 0;
    }
    # - SL et B en même temps
    if (&LOC::Util::in_array(TYPE_SUBRENT, $tabTypes) && &LOC::Util::in_array(TYPE_CATEGORYB, $tabTypes))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0003);
        return 0;
    }
    # - A et B en même temps
    if (&LOC::Util::in_array(TYPE_ACCESSORY, $tabTypes) && &LOC::Util::in_array(TYPE_CATEGORYB, $tabTypes))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0004);
        return 0;
    }
    # - SL et V en même temps
    if (&LOC::Util::in_array(TYPE_SUBRENT, $tabTypes) && &LOC::Util::in_array(TYPE_SALE, $tabTypes))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0005);
        return 0;
    }
    # - A et V en même temps
    if (&LOC::Util::in_array(TYPE_ACCESSORY, $tabTypes) && &LOC::Util::in_array(TYPE_SALE, $tabTypes))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0006);
        return 0;
    }
    # - B et V en même temps
    if (&LOC::Util::in_array(TYPE_CATEGORYB, $tabTypes) && &LOC::Util::in_array(TYPE_SALE, $tabTypes))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0007);
        return 0;
    }

    my $tabUpdates = {
        'FAMAAUTO'          => $machinesFamilyId,
        'FAMTARAUTO'        => $tariffFamilyId,
        'FOAUTO'            => $manufacturerId,
        'MOMADESIGNATION'   => $label,
        'MOMALNG'           => ($length ? $length : undef),
        'MOMALG'            => ($width ? $width : undef),
        'MOMAPOIDS'         => ($weight ? $weight : undef),
        'MOMAHTTRAVAIL'     => ($workingHeight ? $workingHeight : undef),
        'MOMAHTPLANCHER'    => ($floorHeight ? $floorHeight : undef),
        'MOMAHTREPOS'       => ($restHeight ? $restHeight : undef),
        'MOMAHTPLIEE'       => ($stowedHeight ? $stowedHeight : undef),
        'MOMADEPORT'        => ($backwardation ? $backwardation : undef),
        'MOMACAPACITE'      => ($capacity ? $capacity : undef),
        'MOMACAMAX'         => ($maxCapacity ? $maxCapacity : undef),
        'MOMACAPORTEEMAX'   => ($maxRange ? $maxRange : undef),
        'MOMALNGPLATEFORME' => ($platformLength ne '' ? $platformLength : undef),
        'MOMAROTATION'      => ($rotation ? $rotation : undef),
        'MOMASTABILISATEUR' => $stabilizers,
        'MOMAMOTRICITE'     => ($motor ? $motor : undef),
        'MOMACOUTFR'        => $localCurrencyCost,
        'MOMACOUTEU'        => $localCurrencyCost,
        'MOMATYPAGE'        => (@$tabTypes == 0 ? undef : &LOC::Json::toJson($tabTypes)),
        'MOMAGRP'           => $statGroup
    };

    # Calcul de "ENCOMBREMENT"
    my $tabUpdatesTariffTransport = {};
    foreach my $country (keys(%$tabLoads))
    {
        if (defined $tabLoads->{$country})
        {
            if (!$tabLoads->{$country})
            {
                push(@{$tabErrors->{'fatal'}}, 0x0002);
                return 0;
            }

            $tabUpdatesTariffTransport->{$country} = {
                'ENCOMBREMENT' => $tabLoads->{$country}
            };
        }
    }

    # Calcul de "MOMACONTROLEVGP" et "MOMAPERIODICITEVGP"
    my @tabValuesPeriodicity;
    foreach my $country (keys(%$periodicityVGP))
    {
        $periodicityVGP->{$country} *= 1;
        # Formatage si 0
        if ($periodicityVGP->{$country} == 0)
        {
            $periodicityVGP->{$country} = '';
        }
        push(@tabValuesPeriodicity, $periodicityVGP->{$country});
    }
    my @tabValuesPeriodicity = &LOC::Util::array_unique(\@tabValuesPeriodicity);
    my $nbValues = @tabValuesPeriodicity;
    # Tous les pays ont la même valeur pour la périodicité
    if ($nbValues == 1)
    {
        # pas de VGP
        if ($tabValuesPeriodicity[0] eq '')
        {
            $tabUpdates->{'MOMACONTROLEVGP'}    = 0;
            $tabUpdates->{'MOMAPERIODICITEVGP'} = undef;
        }
        # même périodicité pour tous les pays
        else
        {
            $tabUpdates->{'MOMACONTROLEVGP'}    = 1;
            $tabUpdates->{'MOMAPERIODICITEVGP'} = $tabValuesPeriodicity[0];
        }
    }
    # Valeurs différentes pour la périodicité
    else
    {
        # Tous les pays sont soumis au contrôle VGP
        if (!&LOC::Util::in_array('', \@tabValuesPeriodicity))
        {
            $tabUpdates->{'MOMACONTROLEVGP'}    = 1;
            $tabUpdates->{'MOMAPERIODICITEVGP'} = &LOC::Json::toJson($periodicityVGP);
        }
        # Une périodicité différente suivant les pays
        else
        {
            my $tabCountriesIsVGP = {};
            my $tabCountriesWithVGP = {};
            foreach my $country (keys(%$periodicityVGP))
            {
                if ($periodicityVGP->{$country} ne '')
                {
                    $tabCountriesIsVGP->{$country}   = 1;
                    $tabCountriesWithVGP->{$country} = $periodicityVGP->{$country};
                }
                else
                {
                    $tabCountriesIsVGP->{$country}   = 0;
                }
            }
            $tabUpdates->{'MOMACONTROLEVGP'}    = &LOC::Json::toJson($tabCountriesIsVGP);
            $tabUpdates->{'MOMAPERIODICITEVGP'} = &LOC::Json::toJson($tabCountriesWithVGP);
        }
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Insertion du modèle
    my $modelId = $db->insert('MODELEMACHINE', $tabUpdates, 'AUTH');
    if ($modelId == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        print STDERR sprintf($fatalErrMsg, 'insert', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Mise à jour de l'encombrement
    my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);
    foreach my $country (@$tabCountries)
    {
        $tabUpdatesTariffTransport->{$country}->{'MOMAAUTO'} = $modelId;
        # vérification que l'encombrement existe pour le pays
        my $isLoadActive = &LOC::Characteristic::getCountryValueByCode('TRSPTARIF', $country) * 1;
        if ($isLoadActive)
        {
            if ($db->insert('MACHINEENCOMBREMENT', $tabUpdatesTariffTransport->{$country}, 'GESTIONTRANS' . $country) == -1)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                print STDERR sprintf($fatalErrMsg, 'insert', __LINE__, __FILE__, '');
                push(@{$tabErrors->{'fatal'}}, 0x0001);
                return 0;
            }
        }
    }

    # Création des stocks
    my @tabAgencies = &LOC::Agency::getList(LOC::Util::GETLIST_IDS);
    my $tabUpdateStock = {
        'MOMAAUTO'       => $modelId,
        'STMORESERVABLE' => 0,
        'STMORESERVE'    => 0,
        'STMOTOTAL'      => 0,
        'STMOLOU'        => 0,
        'STMODIS'        => 0,
        'STMOREC'        => 0,
        'STMOREV'        => 0,
        'STMOCON'        => 0,
        'STMOPAN'        => 0,
        'STMOVER'        => 0,
        'STMOTRA'        => 0,
        'STMOLIV'        => 0
    };
    foreach my $agencyId (@tabAgencies)
    {
        $tabUpdateStock->{'AGAUTO'} = $agencyId;
        if ($db->insert('STOCKMODELE', $tabUpdateStock, 'AUTH') == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            print STDERR sprintf($fatalErrMsg, 'insert', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Envoi du modèle à OTD
    $db->addTransactionAction('commit.post', sub {
        return &LOC::Model::sendProduct($countryId, $modelId, {});
        },
        {'category' => 'product',
         'priority' => -1000,
         'id' => 'LOC::Model::sendProduct-id:' . $modelId}
    );

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            print STDERR sprintf($fatalErrMsg, 'insert', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Ajout des traces de création
    my $tabTrace = {
        'schema'  => 'AUTH',
        'code'    => LOC::Log::EventType::CREATION,
        'old'     => undef,
        'new'     => undef,
        'content' => ''
    };
    &LOC::EndUpdates::addTrace($tabEndUpds, 'model', $modelId, $tabTrace);

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return $modelId;
}

# Function: insertSubRent
# Ajoute un modèle de machines de sous-location
#
# Parameters:
# string  $countryId  - Identifiant du pays
# hashref $tabData    - Données pour l'insertion du modèle
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Tableau des mises à jour de fin (facultatif)
#
# Returns:
# int|0 - Identifiant du modèle créé, 0 en cas d'erreur
sub insertSubRent
{
    my ($countryId, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Formatage des données
    my $machinesFamilyLabel  = (exists $tabData->{'machinesFamily.label'} ? $tabData->{'machinesFamily.label'} : undef);
    my $machinesFamilyEnergy = (exists $tabData->{'machinesFamily.energy'} ? $tabData->{'machinesFamily.energy'} : undef);
    my $machinesFamilyElevation = (exists $tabData->{'machinesFamily.elevation'} ? $tabData->{'machinesFamily.elevation'} : undef);

    my $label   = (exists $tabData->{'label'} ? $tabData->{'label'} : undef);
    my $workingHeight = $machinesFamilyElevation;
    my $tabLoads          = (exists $tabData->{'tabLoads'} ? $tabData->{'tabLoads'} : undef);
    my $periodicityVGP    = (exists $tabData->{'tabPeriodicityVgp'} ? $tabData->{'tabPeriodicityVgp'} : undef);


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Création de la famille de machines
    my $tabMachineFamilyData = {
        'label'     => $machinesFamilyLabel,
        'elevation' => $machinesFamilyElevation,
        'energy'    => $machinesFamilyEnergy
    };
    my $machinesFamilyId = &LOC::MachinesFamily::insertSubRent($countryId, $tabMachineFamilyData, $userId, $tabErrors, $tabEndUpds);
    if (!$machinesFamilyId)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Création du modèle de machines
    my $tabModelData = {
        'machinesFamily.id' => $machinesFamilyId,
        'tariffFamily.id'   => LOC::TariffFamily::ID_SUBRENT,
        'manufacturer.id'    => MANUFACTURER_SUBRENTID,
        'label'             => $label . SUFFIX_SUBRENT,
        'workingHeight'     => $workingHeight,
        'tabTypes'          => [TYPE_SUBRENT],
        'tabLoads'          => $tabLoads,
        'tabPeriodicityVgp' => $periodicityVGP,
    };

    my $modelId = &insert($countryId, $tabModelData, $userId, $tabErrors, $tabEndUpds);
    if (!$modelId)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
            return 0;
        }
    }
    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            print STDERR sprintf($fatalErrMsg, 'insertSubRent', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    return $modelId;
}


# Function: isRentable
# Indique si un modèle est réservable dans une agence
#
# Parameters:
# string $countryId - Identifiant du pays
# string $agencyId  - Identifiant de l'agence
# int    $modelId   - Identifiant du modèle
#
# Returns:
# bool
sub isRentable
{
    my ($countryId, $agencyId, $modelId) = @_;

    # Récupération du stock actuel pour ce modèle sur l'agence
    my $tabModelStock = &getStockInfos($countryId, $agencyId, $modelId);
    if (!$tabModelStock)
    {
        return 0;
    }

    return $tabModelStock->{'isRentable'};
}

# Function: sendProduct
# Envoie un modèle de machine à OTD
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $productId  - Identifiant du modèle
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# bool
sub sendProduct
{
    my ($countryId, $productId, $tabOptions) = @_;

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Vérification de la gestion des produits
    my $isProductManageActive = &LOC::Characteristic::getCountryValueByCode('TRSPGENCMD', $countryId);
    if (!$isProductManageActive)
    {
        return 0;
    }

    # Récupération
    my $tabModelInfos = &getInfos($countryId, $productId, GETINFOS_BASE, {'locale.id' => 'fr_FR'});

    my $tab = {
        'id'         => $tabModelInfos->{'id'},
        'label'      => $tabModelInfos->{'label'} . ' (' . $tabModelInfos->{'manufacturer.label'} . ')',
        'type.id'    => $tabModelInfos->{'family.id'},
        'type.label' => $tabModelInfos->{'family.label'}
    };

    # Envoi de la trame à OTD
    return &LOC::Otd::setProduct($countryId, $tab, $tabOptions);
}

# Function: update
# Met à jour un modèle de machines
#
# Parameters
# string  $countryId  - Identifiant du pays
# int     $id         - Identifiant du modèle de machine
# hashref $tabData    - Données pour l'insertion du modèle
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Tableau des mises à jour de fin (facultatif)
sub update
{
    my ($countryId, $id, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Récupération des informations du modèle
    my $tabInfos = &getInfos($countryId, $id);
    if (!$tabInfos)
    {
        print STDERR sprintf($fatalErrMsg, 'update', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Paramètres
    # - Famille de machines
    my $oldMachinesFamilyId = $tabInfos->{'machinesFamily.id'};
    my $newMachinesFamilyId = (exists $tabData->{'machinesFamily.id'} ? $tabData->{'machinesFamily.id'} : $oldMachinesFamilyId);

    # - Famille tarifaire
    my $oldTariffFamilyId = $tabInfos->{'tariffFamily.id'};
    my $newTariffFamilyId = (exists $tabData->{'tariffFamily.id'} ? $tabData->{'tariffFamily.id'} : $oldTariffFamilyId);

    # - Fournisseur
    my $oldManufacturerId= $tabInfos->{'manufacturer.id'};
    my $newManufacturerId = (exists $tabData->{'manufacturer.id'} ? $tabData->{'manufacturer.id'} : $oldManufacturerId);

    # - Désignation
    my $oldLabel = $tabInfos->{'label'};
    my $newLabel = (exists $tabData->{'label'} ? $tabData->{'label'} : $oldLabel);

    # - Longueur
    my $oldLength = $tabInfos->{'length'};
    my $newLength = (exists $tabData->{'length'} ? $tabData->{'length'} : $oldLength);

    # - Largeur
    my $oldWidth = $tabInfos->{'width'};
    my $newWidth = (exists $tabData->{'width'} ? $tabData->{'width'} : $oldWidth);

    # - Poids
    my $oldWeight = $tabInfos->{'weight'};
    my $newWeight = (exists $tabData->{'weight'} ? $tabData->{'weight'} : $oldWeight);

    # - Hauteur de travail
    my $oldWorkingHeight = $tabInfos->{'workingHeight'};
    my $newWorkingHeight = (exists $tabData->{'workingHeight'} ? $tabData->{'workingHeight'} : $oldWorkingHeight);

    # - Hauteur plancher
    my $oldFloorHeight = $tabInfos->{'floorHeight'};
    my $newFloorHeight = (exists $tabData->{'floorHeight'} ? $tabData->{'floorHeight'} : $oldFloorHeight);

    # - Hauteur au repos
    my $oldRestHeight = $tabInfos->{'restHeight'};
    my $newRestHeight = (exists $tabData->{'restHeight'} ? $tabData->{'restHeight'} : $oldRestHeight);

    # - Hauteur pliée
    my $oldStowedHeight = $tabInfos->{'stowedHeight'};
    my $newStowedHeight = (exists $tabData->{'stowedHeight'} ? $tabData->{'stowedHeight'} : $oldStowedHeight);

    # - Déport
    my $oldBackwardation = $tabInfos->{'backwardation'};
    my $newBackwardation = (exists $tabData->{'backwardation'} ? $tabData->{'backwardation'} : $oldBackwardation);

    # - Capacité
    my $oldCapacity = $tabInfos->{'capacity'};
    my $newCapacity = (exists $tabData->{'capacity'} ? $tabData->{'capacity'} : $oldCapacity);

    # - Charge max à hauteur max
    my $oldMaxCapacity = $tabInfos->{'maxCapacity'};
    my $newMaxCapacity = (exists $tabData->{'maxCapacity'} ? $tabData->{'maxCapacity'} : $oldMaxCapacity);

    # - Charge max à portée max
    my $oldMaxRange = $tabInfos->{'maxRange'};
    my $newMaxRange = (exists $tabData->{'maxRange'} ? $tabData->{'maxRange'} : $oldMaxRange);

    # - Dimensions de la plateforme
    my $oldPlatformLength = $tabInfos->{'platformLength'};
    my $newPlatformLength = (exists $tabData->{'platformLength'} ? $tabData->{'platformLength'} : $oldPlatformLength);

    # - Angle de rotation maximal
    my $oldRotation = $tabInfos->{'rotation'};
    my $newRotation = (exists $tabData->{'rotation'} ? $tabData->{'rotation'} : $oldRotation);

    # - Stabilisateurs
    my $oldStabilizers = $tabInfos->{'stabilizers'};
    my $newStabilizers = (exists $tabData->{'stabilizers'} ? $tabData->{'stabilizers'} : $oldStabilizers);

    # - Nombre de roues motrices
    my $oldMotor = $tabInfos->{'motor'};
    my $newMotor = (exists $tabData->{'motor'} ? $tabData->{'motor'} : $oldMotor);

    # - Coût du modèle
    my $oldLocalCurrencyCost = $tabInfos->{'localCurrencyCost'};
    my $newLocalCurrencyCost = (exists $tabData->{'localCurrencyCost'} ? $tabData->{'localCurrencyCost'} : $oldLocalCurrencyCost);

    # - Typage
    my $oldTabTypes = $tabInfos->{'tabTypes'};
    my $newTabTypes = (exists $tabData->{'tabTypes'} ? $tabData->{'tabTypes'} : $oldTabTypes);

    # - Encombrement
    my $oldTabLoads = $tabInfos->{'tabLoads'};
    my $newTabLoads = (exists $tabData->{'tabLoads'} ? $tabData->{'tabLoads'} : $oldTabLoads);

    # - Périodicité VGP
    my $oldPeriodicityVGP = $tabInfos->{'tabPeriodicityVgp'};
    my $newPeriodicityVGP = (exists $tabData->{'tabPeriodicityVgp'} ? $tabData->{'tabPeriodicityVgp'} : $oldPeriodicityVGP);

    # - Groupe statistique
    my $oldStatGroup = $tabInfos->{'statGroup'};
    my $newStatGroup = (exists $tabData->{'statGroup'} ? $tabData->{'statGroup'} : undef);
    # Par défaut, MOMAGRP à 0 pour les machines et à 1 pour les accessoires
    if (!defined $newStatGroup)
    {
        $newStatGroup = (&LOC::Util::in_array(TYPE_ACCESSORY, $newTabTypes) ? STATGROUP_VARIOUS : STATGROUP_MACHINES);
    }

    # Tableau des mise à jour à effectuer
    my $tabUpdates                = {};
    my $tabUpdatesTariffTransport = {};

    my $tabChanges = {};

    my $tabTraces  = {'old' => {}, 'new' => {}};

    if ($oldMachinesFamilyId != $newMachinesFamilyId)
    {
        $tabUpdates->{'FAMAAUTO'} = $newMachinesFamilyId;
        $tabChanges->{'machinesFamily.id'} = {'old' => $oldMachinesFamilyId, 'new' => $newMachinesFamilyId};

        # Historisation
        my $tabTrace = {
            'schema'  => 'AUTH',
            'code'    => LOC::Log::EventType::UPDMACHINEFAMILY,
            'old'     => $oldMachinesFamilyId,
            'new'     => $newMachinesFamilyId,
            'content' => $oldMachinesFamilyId . ' → ' . $newMachinesFamilyId
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'model', $id, $tabTrace);
    }
    if ($oldTariffFamilyId != $newTariffFamilyId)
    {
        $tabUpdates->{'FAMTARAUTO'} = $newTariffFamilyId;
        $tabChanges->{'tariffFamily.id'} = {'old' => $oldTariffFamilyId, 'new' => $newTariffFamilyId};

        # Historisation
        my $tabTrace = {
            'schema'  => 'AUTH',
            'code'    => LOC::Log::EventType::UPDTARIFFFAMILY,
            'old'     => $oldTariffFamilyId,
            'new'     => $newTariffFamilyId,
            'content' => $oldTariffFamilyId . ' → ' . $newTariffFamilyId
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'model', $id, $tabTrace);
    }
    if ($oldManufacturerId != $newManufacturerId)
    {
        $tabUpdates->{'FOAUTO'} = $newManufacturerId;
        $tabChanges->{'manufacturer.id'} = {'old' => $oldManufacturerId, 'new' => $newManufacturerId};

        # Historisation
        my $tabTrace = {
            'schema'  => 'AUTH',
            'code'    => LOC::Log::EventType::UPDMANUFACTURER,
            'old'     => $oldManufacturerId,
            'new'     => $newManufacturerId,
            'content' => $oldManufacturerId . ' → ' . $newManufacturerId
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'model', $id, $tabTrace);
    }
    if ($oldLabel ne $newLabel)
    {
        $tabUpdates->{'MOMADESIGNATION'} = $newLabel;
        $tabChanges->{'label'} = {'old' => $oldLabel, 'new' => $newLabel};

        # Historisation
        my $tabTrace = {
            'schema'  => 'AUTH',
            'code'    => LOC::Log::EventType::UPDLABEL,
            'old'     => $oldLabel,
            'new'     => $newLabel,
            'content' => $oldLabel . ' → ' . $newLabel
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'model', $id, $tabTrace);
    }
    if ($oldLength != $newLength)
    {
        $tabUpdates->{'MOMALNG'} = ($newLength ? $newLength : undef);
        $tabChanges->{'length'} = {'old' => $oldLength, 'new' => $newLength};
        $tabTraces->{'old'}->{'length'} = $oldLength;
        $tabTraces->{'new'}->{'length'} = $newLength;
    }
    if ($oldWidth != $newWidth)
    {
        $tabUpdates->{'MOMALG'} = ($newWidth ? $newWidth : undef);
        $tabChanges->{'width'} = {'old' => $oldWidth, 'new' => $newWidth};
        $tabTraces->{'old'}->{'width'} = $oldWidth;
        $tabTraces->{'new'}->{'width'} = $newWidth;
    }
    if ($oldWeight != $newWeight)
    {
        $tabUpdates->{'MOMAPOIDS'} = ($newWeight ? $newWeight : undef);
        $tabChanges->{'weight'} = {'old' => $oldWeight, 'new' => $newWeight};
        $tabTraces->{'old'}->{'weight'} = $oldWeight;
        $tabTraces->{'new'}->{'weight'} = $newWeight;
    }
    if ($oldWorkingHeight != $newWorkingHeight)
    {
        $tabUpdates->{'MOMAHTTRAVAIL'} = ($newWorkingHeight ? $newWorkingHeight : undef);
        $tabChanges->{'workingHeight'} = {'old' => $oldWorkingHeight, 'new' => $newWorkingHeight};
        $tabTraces->{'old'}->{'workingHeight'} = $oldWorkingHeight;
        $tabTraces->{'new'}->{'workingHeight'} = $newWorkingHeight;
    }
    if ($oldFloorHeight != $newFloorHeight)
    {
        $tabUpdates->{'MOMAHTPLANCHER'} = ($newFloorHeight ? $newFloorHeight : undef);
        $tabChanges->{'floorHeight'} = {'old' => $oldFloorHeight, 'new' => $newFloorHeight};
        $tabTraces->{'old'}->{'floorHeight'} = $oldFloorHeight;
        $tabTraces->{'new'}->{'floorHeight'} = $newFloorHeight;
    }
    if ($oldRestHeight != $newRestHeight)
    {
        $tabUpdates->{'MOMAHTREPOS'} = ($newRestHeight ? $newRestHeight : undef);
        $tabChanges->{'restHeight'} = {'old' => $oldRestHeight, 'new' => $newRestHeight};
        $tabTraces->{'old'}->{'restHeight'} = $oldRestHeight;
        $tabTraces->{'new'}->{'restHeight'} = $newRestHeight;
    }
    if ($oldStowedHeight != $newStowedHeight)
    {
        $tabUpdates->{'MOMAHTPLIEE'} = ($newStowedHeight ? $newStowedHeight : undef);
        $tabChanges->{'stowedHeight'} = {'old' => $oldStowedHeight, 'new' => $newStowedHeight};
        $tabTraces->{'old'}->{'stowedHeight'} = $oldStowedHeight;
        $tabTraces->{'new'}->{'stowedHeight'} = $newStowedHeight;
    }
    if ($oldBackwardation != $newBackwardation)
    {
        $tabUpdates->{'MOMADEPORT'} = ($newBackwardation ? $newBackwardation : undef);
        $tabChanges->{'backwardation'} = {'old' => $oldBackwardation, 'new' => $newBackwardation};
        $tabTraces->{'old'}->{'backwardation'} = $oldBackwardation;
        $tabTraces->{'new'}->{'backwardation'} = $newBackwardation;
    }
    if ($oldCapacity != $newCapacity)
    {
        $tabUpdates->{'MOMACAPACITE'} = ($newCapacity ? $newCapacity : undef);
        $tabChanges->{'capacity'} = {'old' => $oldCapacity, 'new' => $newCapacity};
        $tabTraces->{'old'}->{'capacity'} = $oldCapacity;
        $tabTraces->{'new'}->{'capacity'} = $newCapacity;
    }
    if ($oldMaxCapacity != $newMaxCapacity)
    {
        $tabUpdates->{'MOMACAMAX'} = ($newMaxCapacity ? $newMaxCapacity : undef);
        $tabChanges->{'maxCapacity'} = {'old' => $oldMaxCapacity, 'new' => $newMaxCapacity};
        $tabTraces->{'old'}->{'maxCapacity'} = $oldMaxCapacity;
        $tabTraces->{'new'}->{'maxCapacity'} = $newMaxCapacity;
    }
    if ($oldMaxRange != $newMaxRange)
    {
        $tabUpdates->{'MOMACAPORTEEMAX'} = ($newMaxRange ? $newMaxRange : undef);
        $tabChanges->{'maxRange'} = {'old' => $oldMaxRange, 'new' => $newMaxRange};
        $tabTraces->{'old'}->{'maxRange'} = $oldMaxRange;
        $tabTraces->{'new'}->{'maxRange'} = $newMaxRange;
    }
    if ($oldPlatformLength ne $newPlatformLength)
    {
        $tabUpdates->{'MOMALNGPLATEFORME'} = ($newPlatformLength ne '' ? $newPlatformLength : undef);
        $tabChanges->{'platformLength'} = {'old' => $oldPlatformLength, 'new' => $newPlatformLength};
        $tabTraces->{'old'}->{'platformLength'} = $oldPlatformLength;
        $tabTraces->{'new'}->{'platformLength'} = $newPlatformLength;
    }
    if ($oldRotation != $newRotation)
    {
        $tabUpdates->{'MOMAROTATION'} = ($newRotation ? $newRotation : undef);
        $tabChanges->{'rotation'} = {'old' => $oldRotation, 'new' => $newRotation};
        $tabTraces->{'old'}->{'rotation'} = $oldRotation;
        $tabTraces->{'new'}->{'rotation'} = $newRotation;
    }
    if ($oldStabilizers ne $newStabilizers)
    {
        $tabUpdates->{'MOMASTABILISATEUR'} = $newStabilizers;
        $tabChanges->{'stabilizers'} = {'old' => $oldStabilizers, 'new' => $newStabilizers};
        $tabTraces->{'old'}->{'stabilizers'} = $oldStabilizers;
        $tabTraces->{'new'}->{'stabilizers'} = $newStabilizers;
    }
    if ($oldMotor != $newMotor)
    {
        $tabUpdates->{'MOMAMOTRICITE'} = ($newMotor ? $newMotor : undef);
        $tabChanges->{'motor'} = {'old' => $oldMotor, 'new' => $newMotor};
        $tabTraces->{'old'}->{'motor'} = $oldMotor;
        $tabTraces->{'new'}->{'motor'} = $newMotor;
    }

    # Ajout des traces de modification des caractéristiques
    if (keys(%{$tabTraces->{'old'}}) > 0)
    {
        my $tabTrace = {
            'schema'  => 'AUTH',
            'code'    => LOC::Log::EventType::UPDCHARACTERISTIC,
            'old'     => $tabTraces->{'old'},
            'new'     => $tabTraces->{'new'},
            'content' => ''
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'model', $id, $tabTrace);
    }

    if ($oldLocalCurrencyCost != $newLocalCurrencyCost)
    {
        $tabUpdates->{'MOMACOUTFR'} = $newLocalCurrencyCost;
        $tabUpdates->{'MOMACOUTEU'} = $newLocalCurrencyCost;
        $tabChanges->{'localCurrencyCost'} = {'old' => $oldLocalCurrencyCost, 'new' => $newLocalCurrencyCost};

        # Historisation
        my $tabTrace = {
            'schema'  => 'AUTH',
            'code'    => LOC::Log::EventType::UPDCOST,
            'old'     => $oldLocalCurrencyCost,
            'new'     => $newLocalCurrencyCost,
            'content' => $oldLocalCurrencyCost . ' → ' . $newLocalCurrencyCost
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'model', $id, $tabTrace);
    }
    if (!&LOC::Util::areArraysEquals($oldTabTypes, $newTabTypes))
    {
        if (&LOC::Util::in_array(TYPE_SUBRENT, $newTabTypes) && &LOC::Util::in_array(TYPE_CATEGORYB, $newTabTypes))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }
        if (&LOC::Util::in_array(TYPE_ACCESSORY, $newTabTypes) && &LOC::Util::in_array(TYPE_CATEGORYB, $newTabTypes))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0004);
            return 0;
        }
        if (&LOC::Util::in_array(TYPE_SUBRENT, $newTabTypes) && &LOC::Util::in_array(TYPE_SALE, $newTabTypes))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0005);
            return 0;
        }
        if (&LOC::Util::in_array(TYPE_ACCESSORY, $newTabTypes) && &LOC::Util::in_array(TYPE_SALE, $newTabTypes))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0006);
            return 0;
        }
        if (&LOC::Util::in_array(TYPE_CATEGORYB, $newTabTypes) && &LOC::Util::in_array(TYPE_SALE, $newTabTypes))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0007);
            return 0;
        }
        $tabUpdates->{'MOMATYPAGE'} = (@$newTabTypes == 0 ? undef : &LOC::Json::toJson($newTabTypes));
        $tabChanges->{'tabTypes'} = {'old' => $oldTabTypes, 'new' => $newTabTypes};

        # Historisation
        my $tabTrace = {
            'schema'  => 'AUTH',
            'code'    => LOC::Log::EventType::UPDMODELTYPE,
            'old'     => $oldTabTypes,
            'new'     => $newTabTypes,
            'content' => join(', ', @$oldTabTypes) . ' → ' . join(', ', @$newTabTypes)
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'model', $id, $tabTrace);
    }
    if ($oldStatGroup != $newStatGroup)
    {
        $tabUpdates->{'MOMAGRP'} = $newStatGroup;
        $tabChanges->{'statGroup'} = {'old' => $oldStatGroup, 'new' => $newStatGroup};

        # Historisation
        my $tabTrace = {
            'schema'  => 'AUTH',
            'code'    => LOC::Log::EventType::UPDSTATGROUP,
            'old'     => $oldStatGroup,
            'new'     => $newStatGroup,
            'content' => $oldStatGroup . ' → ' . $newStatGroup
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'model', $id, $tabTrace);
    }

    # Calcul de "ENCOMBREMENT"
    my $tabUpdatesTariffTransport;
    my $oldTraceLoad = {};
    my $newTraceLoad = {};
    my $hasChanged = 0;

    foreach my $country (keys(%$newTabLoads))
    {
        if ($oldTabLoads->{$country} != $newTabLoads->{$country})
        {
            $hasChanged = 1;
            $tabUpdatesTariffTransport->{$country} = {
                'ENCOMBREMENT' => $newTabLoads->{$country}
            };
            $oldTraceLoad->{$country} = $oldTabLoads->{$country};
            $newTraceLoad->{$country} = $newTabLoads->{$country};
        }
    }
    if ($hasChanged)
    {
        $tabChanges->{'load'} = {'old' => $oldTabLoads, 'new' => $newTabLoads};

        # Historisation
        my $oldContent = '';
        foreach my $country (keys(%$oldTabLoads))
        {
            $oldContent .= sprintf('%s : %s, ', $country, $oldTabLoads->{$country});
        }
        $oldContent = substr($oldContent, 0, -2);

        my $newContent = '';
        foreach my $country (keys(%$newTabLoads))
        {
            $newContent .= sprintf('%s : %s, ', $country, $newTabLoads->{$country});
        }
        $newContent = substr($newContent, 0, -2);

        my $tabTrace = {
            'schema'  => 'AUTH',
            'code'    => LOC::Log::EventType::UPDLOAD,
            'old'     => $oldTabLoads,
            'new'     => $newTabLoads,
            'content' => $oldContent . ' → ' . $newContent
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'model', $id, $tabTrace);
    }

    # Calcul de MOMACONTROLEVGP et MOMAPERIODICITEVGP
    my $hasChanged = 0;
    my @tabValuesPeriodicity;
    my $oldTraceVGP = {};
    my $newTraceVGP = {};
    foreach my $country (keys(%{$newPeriodicityVGP}))
    {
        $newPeriodicityVGP->{$country} *= 1;
        # Formatage si 0
        if ($newPeriodicityVGP->{$country} == 0)
        {
            $newPeriodicityVGP->{$country} = '';
        }
        if ($oldPeriodicityVGP->{$country} != $newPeriodicityVGP->{$country})
        {
            $oldTraceVGP->{$country} = $oldPeriodicityVGP->{$country};
            $newTraceVGP->{$country} = $newPeriodicityVGP->{$country};
            $hasChanged = 1;
        }
        push(@tabValuesPeriodicity, $newPeriodicityVGP->{$country});
    }
    if ($hasChanged == 1)
    {
        $tabTraces->{'old'}->{'periodicityVGP'} = $oldTraceVGP;
        $tabTraces->{'new'}->{'periodicityVGP'} = $newTraceVGP;
        $tabChanges->{'periodicityVGP'} = {'old' => $oldTraceVGP, 'new' => $newTraceVGP};

        # Historisation
        my $oldContent = '';
        foreach my $country (keys(%$oldTraceVGP))
        {
            $oldContent .= sprintf('%s : %s, ', $country, $oldTraceVGP->{$country});
        }
        $oldContent = substr($oldContent, 0, -2);

        my $newContent = '';
        foreach my $country (keys(%$newTraceVGP))
        {
            $newContent .= sprintf('%s : %s, ', $country, $newTraceVGP->{$country});
        }
        $newContent = substr($newContent, 0, -2);

        my $tabTrace = {
            'schema'  => 'AUTH',
            'code'    => LOC::Log::EventType::UPDCONTROLPERIODICTY,
            'old'     => $oldTraceVGP,
            'new'     => $newTraceVGP,
            'content' => $oldContent . ' → ' . $newContent
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'model', $id, $tabTrace);

        my @tabValuesPeriodicity = &LOC::Util::array_unique(\@tabValuesPeriodicity);
        my $nbValues = @tabValuesPeriodicity;
        # Tous les pays ont la même valeur pour la périodicité
        if ($nbValues == 1)
        {
            # pas de VGP
            if ($tabValuesPeriodicity[0] eq '')
            {
                $tabUpdates->{'MOMACONTROLEVGP'}    = 0;
                $tabUpdates->{'MOMAPERIODICITEVGP'} = undef;
            }
            # même périodicité pour tous les pays
            else
            {
                $tabUpdates->{'MOMACONTROLEVGP'}    = 1;
                $tabUpdates->{'MOMAPERIODICITEVGP'} = $tabValuesPeriodicity[0];
            }
        }
        # Valeurs différentes pour la périodicité
        else
        {
            # Tous les pays sont soumis au contrôle VGP
            if (!&LOC::Util::in_array('', \@tabValuesPeriodicity))
            {
                $tabUpdates->{'MOMACONTROLEVGP'}    = 1;
                $tabUpdates->{'MOMAPERIODICITEVGP'} = &LOC::Json::toJson($newPeriodicityVGP);
            }
            # Une périodicité différente suivant les pays
            else
            {
                my $tabCountriesIsVGP = {};
                my $tabCountriesWithVGP = {};
                foreach my $country (keys(%$newPeriodicityVGP))
                {
                    if ($newPeriodicityVGP->{$country} ne '')
                    {
                        $tabCountriesIsVGP->{$country}   = 1;
                        $tabCountriesWithVGP->{$country} = $newPeriodicityVGP->{$country};
                    }
                    else
                    {
                        $tabCountriesIsVGP->{$country}   = 0;
                    }
                }
                $tabUpdates->{'MOMACONTROLEVGP'}    = &LOC::Json::toJson($tabCountriesIsVGP);
                $tabUpdates->{'MOMAPERIODICITEVGP'} = &LOC::Json::toJson($tabCountriesWithVGP);
            }
        }
    }

    # Mise à jour des informations MODELEMACHINE
    if (keys %$tabUpdates > 0)
    {
        if ($db->update('MODELEMACHINE', $tabUpdates, {'MOMAAUTO*' => $id}, 'AUTH') == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            print STDERR sprintf($fatalErrMsg, 'update', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Mise à jour des informations MACHINEENCOMBREMENT
    if (keys %$tabUpdatesTariffTransport > 0)
    {
        my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);
        foreach my $country (@$tabCountries)
        {
            # vérification que l'encombrement existe pour le pays
            my $isLoadActive = &LOC::Characteristic::getCountryValueByCode('TRSPTARIF', $country) * 1;
            if ($isLoadActive && $tabUpdatesTariffTransport->{$country})
            {
                if ($db->insertOrUpdate('MACHINEENCOMBREMENT', $tabUpdatesTariffTransport->{$country}, {'MOMAAUTO*' => $id}, 'GESTIONTRANS' . $country) == -1)
                {
                    # Annule la transaction
                    if (!$hasTransaction)
                    {
                        $db->rollBack();
                    }
                    print STDERR sprintf($fatalErrMsg, 'update', __LINE__, __FILE__, '');
                    push(@{$tabErrors->{'fatal'}}, 0x0001);
                    return 0;
                }
            }
        }
    }

    # Envoi du modèle à OTD
    if (keys %$tabChanges > 0)
    {
        if ($tabChanges->{'label'} || $tabChanges->{'manufacturer.id'} || $tabChanges->{'machinesFamily.id'})
        {
            $db->addTransactionAction('commit.post', sub {
                return &LOC::Model::sendProduct($countryId, $id, {});
            },
            {'category' => 'product',
             'priority' => -1000,
             'id' => 'LOC::Model::sendProduct-id:' . $id}
            );
        }
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            print STDERR sprintf($fatalErrMsg, 'update', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: updateStock
# Met à jour le stock par modèle de machines
#
# Parameters:
# hashref $agencyId   - Identifiant de l'agence
# hashref $modelId    - Identifiant du modèle de machine
# hashref $tabUpdates - Tableau des mises à jour :
# - nbBookable : nombre de machines réservables ;
# - nbBooked : nombre de machines réservées ;
# - nbTotal : nombre total de machines ;
# - nbRented : nombre de machines louées ;
# - nbAvailable : nombre de machines disponibles ;
# - nbRecovery : nombre de machines en récupération ;
# - nbReview : nombre de machines en révision ;
# - nbControl : nombre de machines en contrôle ;
# - nbBroken : nombre de machines en panne ;
# - nbCheck : nombre de machines en vérification ;
# - nbTransfer : nombre de machines en transfert ;
# - nbDelivery : nombre de machines en livraison.
#
# Returns:
# int - 1 si la mise à jour c'est faite correctement sinon 0
sub updateStock
{
    my ($agencyId, $modelId, $tabUpdates) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('auth');

    # Correspondance avec les champs de la table en base de données
    my %tabProperties = (
        'nbBookable'  => 'STMORESERVABLE',
        'nbBooked'    => 'STMORESERVE',
        'nbTotal'     => 'STMOTOTAL',
        'nbRented'    => 'STMOLOU',
        'nbAvailable' => 'STMODIS',
        'nbRecovery'  => 'STMOREC',
        'nbReview'    => 'STMOREV',
        'nbControl'   => 'STMOCON',
        'nbBroken'    => 'STMOPAN',
        'nbCheck'     => 'STMOVER',
        'nbTransfer'  => 'STMOTRA',
        'nbDelivery'  => 'STMOLIV'
    );

    my %tabValues = ();
    while (my($property, $value) = each(%$tabUpdates))
    {
        $value *= 1;
        if ($value != 0)
        {
            $tabValues{$tabProperties{$property} . '*'} = $db->quoteIdentifier($tabProperties{$property}) .
                                                          ($value > 0 ? ' + ' : ' - ') . abs($value);
        }
    }

    # Mise à jour en base
    if (keys %tabValues > 0 && $db->update('STOCKMODELE', \%tabValues, {'MOMAAUTO*' => $modelId, 'AGAUTO' => $agencyId}, 'AUTH') == -1)
    {
        return 0;
    }

    return 1;
}

1;
