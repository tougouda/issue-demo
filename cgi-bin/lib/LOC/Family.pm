use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Family
# Module permettant d'obtenir les informations sur les familles
package LOC::Family;


# Constants: États
# STATE_ACTIVE  - Famille active
# STATE_DELETED - Famille supprimée
use constant
{
    STATE_ACTIVE  => 'GEN01',
    STATE_DELETED => 'GEN03',
};


use strict;

# Modules internes
use LOC::Db;


# Function: getInfos
# Retourne les infos d'une famille
# 
# Contenu du hashage :
# - id => identifiant de la famille
# - fullName => nom complet
# - shortName => nom court
# - order => ordre d'affichage
# - externalId => identifiant externe
# - creationDate => date de création
# - modificationDate => date de dernière modification
# - state.id => identifant de l'état
#
#
# Parameters:
# string $id       - Identifiant de la famille
# string $localeId - Identifiant de la locale
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getInfos
{
    my ($id, $localeId) = @_;
    unless (defined $localeId)
    {
        $localeId = LOC::Locale::DEFAULT_LOCALE_ID;
    }

    # connexion à AUTH
    my $db = &LOC::Db::getConnection('auth');

    # Récupère les données
    my $query = '
SELECT
    fly_id AS `id`,
    IFNULL(fll1.fll_fullname, fll2.fll_fullname) AS `fullName`,
    IFNULL(fll1.fll_shortname, fll2.fll_shortname) AS `shortName`,
    fly_order AS `order`,
    fly_externalid AS `externalId`,
    fly_date_creation AS `creationDate`,
    fly_date_modification AS `modificationDate`,
    fly_sta_id AS `state.id`
FROM p_family
LEFT JOIN p_family_locale fll1
ON (fly_id = fll1.fll_fly_id AND fll1.fll_lcl_id = ' . $db->quote($localeId) . ')
LEFT JOIN p_family_locale fll2
ON (fly_id = fll2.fll_fly_id AND fll2.fll_lcl_id = ' . $db->quote(LOC::Locale::DEFAULT_LOCALE_ID) . ')
WHERE fly_id = ' . $db->quote($id) . ';';

    my $tab = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if (!$tab)
    {
        return 0;
    }
    return (wantarray ? %$tab : $tab);
}

# Function: getList
# Retourne la liste des familles
#
# Parameters:
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres ('fullName' : recherche dans le libellé court ou le libellé complet)
# string  $localeId   - locale à utiliser pour les libellés
#
# Returns:
# hash|hashref|0 - Liste des familles (mêmes clés que pour la méthode getInfos)
sub getList
{
    my ($format, $tabFilters, $localeId) = @_;
    unless (defined $localeId)
    {
        $localeId = LOC::Locale::DEFAULT_LOCALE_ID;
    }
    
    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('auth');
    
    my $query = '';

    # Filtres
    my $whereQuery = '';
    if ($tabFilters->{'fullName'} ne '')
    {
        my @tabElts = split(' ', $tabFilters->{'fullName'});
        foreach my $elt (@tabElts)
        {
            my $search = $db->quote('%|' . $elt . '|%');
            $whereQuery .= '
AND REPLACE(CONCAT_WS("|", "|", fll_fullname, fll_shortname, "|"), " ", "|") LIKE ' . $search;
        }
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    fly_id AS `id`,
    IFNULL(fll1.fll_fullname, fll2.fll_fullname) AS `fullName`,
    IFNULL(fll1.fll_shortname, fll2.fll_shortname) AS `shortName`,
    fly_order AS `order`,
    fly_externalid AS `externalId`,
    fly_date_creation AS `creationDate`,
    fly_date_modification AS `modificationDate`,
    fly_sta_id AS `state.id`
FROM p_family
LEFT JOIN p_family_locale fll1
ON (fly_id = fll1.fll_fly_id AND fll1.fll_lcl_id = ' . $db->quote($localeId) . ')
LEFT JOIN p_family_locale fll2
ON (fly_id = fll2.fll_fly_id AND fll2.fll_lcl_id = ' . $db->quote(LOC::Locale::DEFAULT_LOCALE_ID) . ')
WHERE fly_sta_id != ' . $db->quote(STATE_DELETED) .
$whereQuery . '
ORDER BY `fullname`;';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*) AS `label`
FROM p_family
WHERE fly_sta_id != ' . $db->quote(STATE_DELETED) .
$whereQuery;
    }
    else
    {
        $query = '
SELECT
    fly_id AS `id`,
    fll_shortname AS `label`
FROM p_family
LEFT JOIN p_family_locale
ON (fly_id = fll_fly_id AND fll_lcl_id = ' . $db->quote($localeId) . ')
WHERE fly_sta_id != ' . $db->quote(STATE_DELETED) .
$whereQuery . '
ORDER BY `label`;';
    }

    return $db->fetchList($format, $query, {}, 'id');
}

1;
