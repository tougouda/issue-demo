use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Contract
# Module de fonctions communes aux diffrents types de contrats
package LOC::Contract;


# Constants: Types de contrats
# TYPE_RENT        - Location
# TYPE_SUBRENT     - Sous-location
# TYPE_FULLSERVICE - Full service
# TYPE_CATEGORYB   - Catégorie B
use constant
{
    TYPE_RENT        => 0x0000,
    TYPE_SUBRENT     => 0x0001,
    TYPE_FULLSERVICE => 0x0002,
    TYPE_CATEGORYB   => 0x0004,
    TYPE_SALE        => 0x0008
};

# Constants: Suffixes du code de contrat
# SUFFIX_SUBRENT     - Sous-location
# SUFFIX_FULLSERVICE - Full service
# SUFFIX_CATEGORYB   - Catégorie B
use constant
{
    SUFFIX_SUBRENT     => 'SL',
    SUFFIX_FULLSERVICE => 'FS',
    SUFFIX_CATEGORYB   => 'B',
    SUFFIX_SALE        => 'V'
};


use strict;


# Function: generateCode
# Générer le code d'un contrat
#
# Parameters:
# int    $contractId - Id du contrat
# string $agencyId   - Id de l'agence du devis
# int    $type       - Type de contrat
#
# Returns:
# string -
sub generateCode
{
    my ($contractId, $agencyId, $type) = @_;

    my @time = localtime();

    my @tabSuffix = ();
    if ($type & TYPE_SUBRENT)
    {
        push(@tabSuffix, SUFFIX_SUBRENT);
    }
    if ($type & TYPE_CATEGORYB)
    {
        push(@tabSuffix, SUFFIX_CATEGORYB);
    }
    if ($type & TYPE_SALE)
    {
        push(@tabSuffix, SUFFIX_SALE);
    }
    if ($type & TYPE_FULLSERVICE)
    {
        push(@tabSuffix, SUFFIX_FULLSERVICE);
    }

    my $pattern = '%s%02d%02d%06s%s';
    my $code = sprintf($pattern, $agencyId, $time[5] % 100, $time[4] + 1, $contractId, join('', @tabSuffix));
    return $code;
}

1;