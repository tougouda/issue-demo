use utf8;
use open (':encoding(UTF-8)');

# Package : LOC::Transport
# Module de gestion du transport et des commandes de transport
package LOC::Transport;


# Constants: Récupération d'informations sur les transports
# GETINFOS_ALL              - Récupération de l'ensemble des informations
# GETINFOS_BASE             - Récupération des informations de base
# GETINFOS_MODEL            - Récupération des informations sur le modèle de machine
# GETINFOS_MACHINE          - Récupération des informations sur la machine
# GETINFOS_FROM             - Récupération des informations sur la provenance (entité de départ)
# GETINFOS_TO               - Récupération des informations sur la destination (entité d'arrivée)
# GETINFOS_ORDER            - Récupération des informations sur la commande de transport associée (si existante)
# GETINFOS_DELIVERYSELFFORM - Récupération des informations sur les bordereaux d'enlèvement
# GETINFOS_RECOVERYSELFFORM - Récupération des informations sur les bordereaux de restitution
# GETINFOS_POSSIBILITIES    - Récupération des possibilités sur les transports
# GETINFOS_RIGHTS           - Récupération des information sur les droits
use constant
{
    GETINFOS_ALL              => 0xFFFFFFFF,
    GETINFOS_BASE             => 0x00000000,
    GETINFOS_MODEL            => 0x00000001,
    GETINFOS_MACHINE          => 0x00000002,
    GETINFOS_FROM             => 0x00000004,
    GETINFOS_TO               => 0x00000008,
    GETINFOS_ORDER            => 0x00000010,
    GETINFOS_DELIVERYSELFFORM => 0x00000020,
    GETINFOS_RECOVERYSELFFORM => 0x00000040,
    GETINFOS_POSSIBILITIES    => 0x00000080,
    GETINFOS_RIGHTS           => 0x00000100
};

# Constants: Récupération d'informations sur les commandes de transports
# GETORDERINFOS_ALL       - Récupération de l'ensemble des informations
# GETORDERINFOS_BASE      - Récupération des informations de base
# GETORDERINFOS_TRANSPORT - Récupération des informations du transport associé
use constant
{
    GETORDERINFOS_ALL       => 0xFFFFFFFF,
    GETORDERINFOS_BASE      => 0x00000000,
    GETORDERINFOS_TRANSPORT => 0x00000001
};

# Constants: Récupération d'informations sur les bordereaux d'enlèvements ou de restitutions
# GETSELFFORMINFOS_ALL       - Récupération de l'ensemble des informations
# GETSELFFORMINFOS_BASE      - Récupération des informations de base
# GETSELFFORMINFOS_TRANSPORT - Récupération des informations du transport associé
use constant
{
    GETSELFFORMINFOS_ALL       => 0xFFFFFFFF,
    GETSELFFORMINFOS_BASE      => 0x00000000,
    GETSELFFORMINFOS_TRANSPORT => 0x00000001
};

# Constants: Récupération d'informations sur les contrats éligibles pour un transfert inter-chantiers
# GETSTCTTINFOS_ALL  - Récupération de l'ensemble des informations
# GETSTCTTINFOS_BASE - Récupération des informations de base
use constant
{
    GETSTCTTINFOS_ALL  => 0xFFFFFFFF,
    GETSTCTTINFOS_BASE => 0x00000000
};

# Constants: Types de transport
# TYPE_DELIVERY         - Livraison standard
# TYPE_RECOVERY         - Récupération standard
# TYPE_SITESTRANSFER    - Transfert inter-chantiers
# TYPE_AGENCIESTRANSFER - Transfert inter-agences
use constant
{
    TYPE_DELIVERY         => 'D',
    TYPE_RECOVERY         => 'R',
    TYPE_SITESTRANSFER    => 'TS',
    TYPE_AGENCIESTRANSFER => 'TA'
};

# Constants: Actions
# ACTION_NOTDONE  - Pas de réalisation du transport
# ACTION_USERDONE - Réalisation par un utilisateur
# ACTION_DONE     - Réalisation définitive
use constant
{
    ACTION_NOTDONE  => 0,
    ACTION_USERDONE => 1,
    ACTION_DONE     => 2
};

# Constants: Types de prise en compte d'un transport
# CONSIDERTYPE_NONE      - Pas de prise en compte
# CONSIDERTYPE_NOKIMOCE  - Prise en compte sans demande d'intervention Kimoce
# CONSIDERTYPE_KIMOCE    - Prise en compte avec demande d'intervention Kimoce
use constant
{
    CONSIDERTYPE_NONE     => 0,
    CONSIDERTYPE_NOKIMOCE => 1,
    CONSIDERTYPE_KIMOCE   => 2
};

# Constants: Types d'entités de départ et d'arrivée
# FROMTOTYPE_AGENCY       - Agence
# FROMTOTYPE_CONTRACT     - Contrat
# FROMTOTYPE_ESTIMATELINE - Ligne de devis
use constant
{
    FROMTOTYPE_AGENCY       => 'agency',
    FROMTOTYPE_CONTRACT     => 'contract',
    FROMTOTYPE_ESTIMATELINE => 'estimateLine'
};

# Constants: Récupération d'informations sur les entités de départ et d'arrivée
# GETFROMTOINFOS_ALL     - Récupération de l'ensemble des informations
# GETFROMTOINFOS_BASE    - Récupération des informations de base
# GETFROMTOINFOS_DETAILS - Récupération des informations détaillées
use constant
{
    GETFROMTOINFOS_ALL     => 0xFFFFFFFF,
    GETFROMTOINFOS_BASE    => 0x00000000,
    GETFROMTOINFOS_DETAILS => 0x00000001
};

# Constants: Cotés
# SIDE_DELIVERY - Livraison
# SIDE_RECOVERY - Récupération
use constant
{
    SIDE_DELIVERY => 'D',
    SIDE_RECOVERY => 'R'
};

# Constants: États des transports
# STATE_ACTIVE   - Transport actif
# STATE_INACTIVE - Transport non actif
# STATE_DELETED  - Transport supprimé
# STATE_ARCHIVED - Transport archivé
# STATE_CONVERTED - Transport converti (ex : lorsque la ligne de devis a été passée en contrat)
use constant
{
    STATE_ACTIVE    => 'TSP01',
    STATE_INACTIVE  => 'TSP02',
    STATE_DELETED   => 'TSP03',
    STATE_ARCHIVED  => 'TSP04',
    STATE_CONVERTED => 'TSP05'
};

# Constants: États des commandes
# ORDER_STATE_ACTIVE  - Commande de transport active
# ORDER_STATE_CLOSED  - Commande de transport clôturée
# ORDER_STATE_DELETED - Commande de transport supprimée
use constant
{
    ORDER_STATE_ACTIVE  => 'TOR01',
    ORDER_STATE_CLOSED  => 'TOR02',
    ORDER_STATE_DELETED => 'TOR03'
};

# Constants: États des bordereaux d'enlèvements ou de restitutions
# SELFFORM_STATE_ACTIVE  - Bordereau actif
# SELFFORM_STATE_DELETED - Bordereau supprimé
use constant
{
    SELFFORM_STATE_ACTIVE  => 'TSF01',
    SELFFORM_STATE_DELETED => 'TSF03'
};

# Constants: Erreurs
# ERROR_NOMACHINE          - Machine non attribuée
# ERROR_PFNOTGEN           - Pro forma non générée
# ERROR_PFNOTUPTODATE      - Pro forma non à jour
# ERROR_PFNOTPAYED         - Pro forma non finalisée
# ERROR_RENTTARIFF         - Remise exceptionnelle de location en attente
# ERROR_TRSPTARIFF         - Remise exceptionnelle de transport en attente
# ERROR_CUSTNOTVERIF       - Client avec ouverture de compte en attente
# ERROR_DELIVERYNOTDONE    - Livraison non effectuée
# ERROR_DELIVERY           - Erreurs sur la livraison
# ERROR_RECOVERY           - Erreurs sur la récupération
# ERROR_NOMAINCONTRACT     - Pas de contrat principal sélectionné
# ERROR_MACHINESITEBLOCKED - Machine bloquée sur chantier
# ERROR_VISIBLEMACHINE     - Machine mise en visibilité sur une autre agence
# ERROR_RECOVERYTRANSFER   - Transfert inter-chantiers existant sur la récupération
# ERROR_TRANSFERREDMACHINE - Machine transférée dans une autre agence
# ERROR_INVOICEDDELIVERY   - Transport aller facturé
use constant
{
    ERROR_NOMACHINE            => 0x00000001,
    ERROR_PFNOTGEN             => 0x00000002,
    ERROR_PFNOTUPTODATE        => 0x00000004,
    ERROR_PFNOTPAYED           => 0x00000008,
    ERROR_RENTTARIFF           => 0x00000010,
    ERROR_TRSPTARIFF           => 0x00000020,
    ERROR_CUSTNOTVERIF         => 0x00000040,
    ERROR_DELIVERYNOTDONE      => 0x00000080,
    ERROR_DELIVERY             => 0x00000100,
    ERROR_RECOVERY             => 0x00000200,
    ERROR_NOMAINCONTRACT       => 0x00000400,
    ERROR_MACHINESITEBLOCKED   => 0x00000800,
    ERROR_MACHINENOTTRANSFERED => 0x00001000,

    ERROR_VISIBLEMACHINE       => 0x00010000,
    ERROR_RECOVERYTRANSFER     => 0x00020000,
    ERROR_TRANSFERREDMACHINE   => 0x00040000,
    ERROR_INVOICEDDELIVERY     => 0x00080000
};

# Constants: Valeurs par défaut
# DEFAULT_BEGINTIME             - Heure de début d'un transport par défaut envoyée à OTD.
# DEFAULT_ENDTIME               - Heure de fin d'un transport par défaut envoyée à OTD.
# DEFAULT_CONTRACT_TABINDEX     - Onglet à activer lors de l'ouverture du contrat de location depuis OTD.
# DEFAULT_ESTIMATELINE_TABINDEX - Onglet à activer lors de l'ouverture du devis de location depuis OTD.
use constant
{
    DEFAULT_BEGINTIME              => '00:00:00',
    DEFAULT_ENDTIME                => '23:59:59',
    DEFAULT_CONTRACT_TABINDEX      => 3,
    DEFAULT_ESTIMATELINE_TABINDEX  => 3
};


use strict;

# Module internes
use LOC::Db;

use LOC::Agency;
use LOC::Model;
use LOC::Machine;
use LOC::Site;
use LOC::Contract::Rent;
use LOC::Estimate::Rent;
use LOC::EndUpdates;
use LOC::Otd;
use LOC::Date;
use LOC::Proforma;
use LOC::Proforma::Type;
use LOC::Json;
use LOC::TransportRequest;
use LOC::Mail;
use LOC::Template;

my $fatalErrMsg = 'Erreur fatale: module LOC::Transport, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";

# Liste des erreurs:
#
# 0x0001 => Erreur fatale
# 0x0003 => Impossible de modifier cette information
#
# 0x0100 => Le contrat lié n'est plus disponible
# 0x0102 => Incohérence au niveau de la date et/ou de l'horaire sur le transfert inter-chantiers
# 0x0103 => Présence d'une pro forma de prolongation non finalisée
# 0x0106 => La réalisation du transport est impossible
# 0x0107 => L'annulation du transport est impossible
# 0x0108 => Transfert de la machine vers une autre agence est impossible car la machine est déjà mise en visibilité
# 0x0109 => Impossible de modifier "La machine est transférée à" car toutes les machines du modèle sont réservées

# 0x0201 => L'agence de destination n'est pas renseignée pour le transfert inter-agences
# 0x0202 => La date n'est pas renseignée pour le transfert inter-agences

# 0x0301 => La prise en compte du transport a déjà été effectuée



# Function: cancelAgenciesTransfer
# Annule un transfert inter-agence
#
# Parameters:
# string  $countryId    - Identifiant du pays
# int     $machineId    - Id de la machine
# int     $userId       - Utilisateur responsable
# hashref $tabErrors    - Tableau des erreurs
# hashref $tabEndUpds   - Mises à jour de fin (facultatif)
#
# Returns:
# bool
sub cancelAgenciesTransfer
{
    my ($countryId, $machineId, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Infos sur le transport
    my $tabOldInfos = &getAgenciesTransferInfos($countryId, $machineId, GETINFOS_BASE | GETINFOS_MACHINE,
                                                {'isSortDisabled' => 1,
                                                 'lastId'         => 1
                                                });
    if (!$tabOldInfos)
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'cancelAgenciesTransfer', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # nouvelles infos transport
    my $tabNewInfos = &LOC::Util::clone($tabOldInfos);

    # Le transfert existant est déjà réalisé
    # - annulation de la réalisation
    if ($tabOldInfos->{'unloading'}->{'isDone'})
    {
        if (!&_undoAgenciesTransfer($db, $countryId, $tabOldInfos, $userId, $tabEndUpds))
        {
            if (!$hasTransaction)
            {
                # Annule la transaction
                $db->rollBack();
            }
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'cancelAgenciesTransfer', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
        # Mise à jour du transport
        $tabNewInfos->{'machine.tabInfos'}      = &_getMachineInfos($countryId, $machineId);
        $tabNewInfos->{'unloading'}->{'isDone'} = ACTION_NOTDONE;
        $tabNewInfos->{'unloading'}->{'date'}   = undef;
        $tabNewInfos->{'loading'}->{'isDone'}   = ACTION_NOTDONE;
    }

    # Annulation du transfert
    # - suppression du transfert
    $tabNewInfos->{'state.id'} = STATE_DELETED;
    if (!&_set($db, $countryId, $tabNewInfos, {}))
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'cancelAgenciesTransfer', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    my $tabMachineOptions = {};

    # - Modification :
    # -- de l'état de la machine   : elle repasse en vérif
    # -- de la visibilité          : la machine n'est plus en visibilité
    my $tabUpdates = {
        'state.id'           => LOC::Machine::STATE_CHECK,
        'visibleOnAgency.id' => undef,
        'visibilityMode'     => LOC::Machine::VISIBILITYMODE_NONE
    };
    $tabMachineOptions->{'updateFrom'} = 'internal';

    if (!&LOC::Machine::update($countryId, $machineId, $tabUpdates, $userId, $tabMachineOptions, undef, $tabEndUpds))
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'cancelAgenciesTransfer', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();

            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'cancelAgenciesTransfer', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Mise à jour alertes "Transport en cours"
    &_updateAlerts($tabOldInfos, $tabNewInfos, {}, $tabEndUpds);

    # Trace de l'annulation du transfert inter-agences
    my $tabTrace = {
        'schema' => &LOC::Db::getSchemaName($countryId),
        'code'   => LOC::Log::EventType::CANCELAGENCIESTRANSFER
    };
    &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $tabOldInfos->{'machine.id'}, $tabTrace);

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    # Envoi de l'email
    my $tabData = {
        'tabOldInfos' => $tabOldInfos,
        'tabNewInfos' => $tabNewInfos,
        'tabModif'    => ['suppr']
    };
    &sendEmail($countryId, 'agenciesTransfer', $tabData, $userId);

    return 1;
}


# Function: unconsider
# Annule la prise en compte du transport
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Identifiant du transport
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub unconsider
{
    my ($countryId, $id, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Infos sur le transport
    my $tabOldInfos = &getInfos($countryId, $id,
                                    GETINFOS_FROM | GETINFOS_TO,
                                    {'getFromInfosFlags' => GETFROMTOINFOS_DETAILS,
                                     'getToInfosFlags'   => GETFROMTOINFOS_DETAILS});


    # Si la prise en compte a changé
    if ($tabOldInfos->{'isConsidered'} != CONSIDERTYPE_NONE)
    {
        my $tabNewInfos = &LOC::Util::clone($tabOldInfos);
        # Mise à jour suivant le type
        $tabNewInfos->{'isConsidered'} = CONSIDERTYPE_NONE;
        $tabNewInfos->{'consideredUser.id'} = undef;
        $tabNewInfos->{'consideredDate'}    = undef;

        # Mise à jour du transport
        if (!&_set($db, $countryId, $tabNewInfos))
        {
            if (!$hasTransaction)
            {
                # Annule la transaction
                $db->rollBack();
            }
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'unconsider', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }

        # Mise à jour des alertes transport
        &_updateAlerts($tabOldInfos, $tabNewInfos, {}, $tabEndUpds);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'unconsider', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: consider
# Prend en compte le transport
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Identifiant du transport
# int     $type       - Type de prise en compte
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub consider
{
    my ($countryId, $id, $type, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Vérification du type de prise en compte
    if (!&LOC::Util::in_array($type, [CONSIDERTYPE_KIMOCE, CONSIDERTYPE_NOKIMOCE]))
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'consider', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Infos sur le transport
    my $tabOldInfos = &getInfos($countryId, $id,
                                    GETINFOS_FROM | GETINFOS_TO | GETINFOS_MACHINE,
                                    {'getFromInfosFlags' => GETFROMTOINFOS_DETAILS,
                                     'getToInfosFlags'   => GETFROMTOINFOS_DETAILS});

     # Vérification du type de transport
    if (!&LOC::Util::in_array($tabOldInfos->{'type'}, [TYPE_SITESTRANSFER, TYPE_RECOVERY]) ||
        !&LOC::Util::in_array($tabOldInfos->{'from.tabInfos'}->{'tabDetails'}->{'state.id'},
                              [LOC::Contract::Rent::STATE_STOPPED,
                               LOC::Contract::Rent::STATE_BILLED]))
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'consider', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }


    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }
    # Récupération des droits
    my $tabRights = &_getRights($tabOldInfos, $tabUserInfos);

    # Vérification des droits si on vient de la fiche machine
    if (!&_hasRight($tabRights->{'actions'}, 'consider'))
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        push(@{$tabErrors->{'fatal'}}, 0x0003);
        return 0;
    }


    # Si la prise en compte a changé
    if ($tabOldInfos->{'isConsidered'} != $type)
    {
        my $tabNewInfos = &LOC::Util::clone($tabOldInfos);
        # Mise à jour suivant le type
        $tabNewInfos->{'isConsidered'}      = $type;
        $tabNewInfos->{'consideredUser.id'} = $userId;
        $tabNewInfos->{'consideredDate'}    = 'now';

        # Réalisation de la récupération
        if (!&_set($db, $countryId, $tabNewInfos))
        {
            if (!$hasTransaction)
            {
                # Annule la transaction
                $db->rollBack();
            }
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'consider', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }

        # Mise à jour des alertes transport
        &_updateAlerts($tabOldInfos, $tabNewInfos, {}, $tabEndUpds);

        # Mise à jour des traces
        my $tabCountryInfos = &LOC::Country::getInfos($countryId);
        my $locale = &LOC::Locale::getLocale($tabCountryInfos->{'locale.id'}, undef, [POSIX::LC_MESSAGES, POSIX::LC_CTYPE]);

        # - Trace de prise en compte
        if ($type != CONSIDERTYPE_NONE)
        {
            # - Mise à jour du compteur horaire s'il existe
            if (defined $tabOptions->{'updateTimeCounter'} && $tabOptions->{'updateTimeCounter'} > 0)
            {
                # Mise à jour du compteur "horaire" de Kimoce
                &LOC::EndUpdates::updateKimoceMachine($tabEndUpds, $tabOldInfos->{'machine.tabInfos'}->{'parkNumber'},
                                                          LOC::EndUpdates::KIMUPD_TIMECOUNTER,
                                                          {'timeCounter' => $tabOptions->{'updateTimeCounter'}});
            }

            # Pour une récupération :
            if ($tabOldInfos->{'type'} eq TYPE_RECOVERY)
            {
                # Trace de prise en compte de la récupération par l'atelier sur la machine
                &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $tabOldInfos->{'machine.id'},
                                   {'schema'  => $schemaName,
                                    'code'    => LOC::Log::EventType::CONSIDERRECOVERY,
                                    'old'     => $tabOldInfos->{'isConsidered'},
                                    'new'     => $type,
                                    'content' => $locale->t('Depuis %s', $tabOldInfos->{'from.tabInfos'}->{'code'})
                                   });

                # - Trace de génération de demande d'intervention Kimoce
                if ($type == CONSIDERTYPE_KIMOCE)
                {
                    # Trace de génération de demande d'entretien dans Kimoce
                    &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $tabOldInfos->{'machine.id'},
                                                {'schema' => $schemaName, 'code' => LOC::Log::EventType::GENKIMOCEMAINTENANCE});

                    # Mise à jour du compteur "récupérations" de Kimoce
                    &LOC::EndUpdates::updateKimoceMachine($tabEndUpds, $tabOldInfos->{'machine.tabInfos'}->{'parkNumber'},
                                                          LOC::EndUpdates::KIMUPD_RECOVERYCOUNTER);
                }
            }
            # Pour un transfert inter-chantiers
            elsif ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER)
            {
                # Trace de prise en compte du transfert inter-chantiers par l'atelier sur la machine
                &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $tabOldInfos->{'machine.id'},
                                   {'schema'  => $schemaName,
                                    'code'    => LOC::Log::EventType::CONSIDERSITETRANSFER,
                                    'old'     => $tabOldInfos->{'isConsidered'},
                                    'new'     => $type,
                                    'content' => $locale->t('De %s vers %s', $tabOldInfos->{'from.tabInfos'}->{'code'},
                                                                             $tabOldInfos->{'to.tabInfos'}->{'code'})
                                   });

                # - Trace de génération de demande d'intervention Kimoce
                if ($type == CONSIDERTYPE_KIMOCE)
                {
                    # Trace de génération de demande d'intervention dans Kimoce
                    &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $tabOldInfos->{'machine.id'},
                                                {'schema' => $schemaName, 'code' => LOC::Log::EventType::GENKIMOCEINTERVENTION});

                    # Mise à jour du compteur "transferts inter-chantiers" de Kimoce
                    &LOC::EndUpdates::updateKimoceMachine($tabEndUpds, $tabOldInfos->{'machine.tabInfos'}->{'parkNumber'},
                                                          LOC::EndUpdates::KIMUPD_SITETRANSFERCOUNTER);
                }
            }
        }
    }
    else
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'consider', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0301);
        return 0;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'consider', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: considerAntecedents
# Prend en compte les précédentes récupérations (ou transferts inter-chantiers) de la même machine et non prises en compte
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Identifiant du transport
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub considerAntecedents
{
    my ($countryId, $id, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!exists $tabErrors->{'fatal'})
    {
        $tabErrors->{'fatal'} = [];
    }

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);


    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }


    # Recherche des lignes de transport
    my $tabInfos = &getInfos($countryId, $id);
    if (!$tabInfos)
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'consider', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }


    # Recherche des récupérations sur la même machine et non prises en compte
    my $tabFilters = {
        'type'        => [LOC::Transport::TYPE_RECOVERY, LOC::Transport::TYPE_SITESTRANSFER],
        'from'         => {
            'type'    => FROMTOTYPE_CONTRACT,
            'stateId' => [LOC::Contract::Rent::STATE_STOPPED, LOC::Contract::Rent::STATE_BILLED]
        },
        'stateId'      => STATE_ACTIVE,
        'isConsidered' => 0,
        'machineId'    => $tabInfos->{'machine.id'},
        'idToExclude'  => $id
    };
    my $tabList = &getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters,
                           GETINFOS_FROM | GETINFOS_TO,
                           {'getFromInfosFlags' => GETFROMTOINFOS_DETAILS,
                            'getToInfosFlags'   => GETFROMTOINFOS_DETAILS});


    # Suppression des commandes associées
    foreach my $tabOldInfos (values %$tabList)
    {
        if ($tabOldInfos->{'isConsidered'} == CONSIDERTYPE_NONE)
        {
            my $tabNewInfos = &LOC::Util::clone($tabOldInfos);
            # Mise à jour suivant le type
            $tabNewInfos->{'isConsidered'}   = CONSIDERTYPE_NOKIMOCE;
            $tabNewInfos->{'consideredDate'} = 'now';

            # Réalisation de la récupération
            if (!&_set($db, $countryId, $tabNewInfos))
            {
                if (!$hasTransaction)
                {
                    # Annule la transaction
                    $db->rollBack();
                }
                # Erreur fatale !
                print STDERR sprintf($fatalErrMsg, 'consider', __LINE__, __FILE__, '');
                push(@{$tabErrors->{'fatal'}}, 0x0001);
                return 0;
            }

            # Mise à jour des alertes transport
            &_updateAlerts($tabOldInfos, $tabNewInfos, {}, $tabEndUpds);
        }
    }



    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'consider', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }


    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}



# Function: deleteAllForEstimateLine
# Supprime tous les transports (ainsi que leurs commandes) associées à une ligne de devis
#
# Parameters:
# string   $countryId  - Identifiant du pays
# int      $id         - Id de la ligne de devis
# int      $userId     - Id de l'utilisateur responsable
# arrayref $tabErrors  - Tableau d'erreurs
# hashref  $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub deleteAllForEstimateLine
{
    my ($countryId, $id, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!exists $tabErrors->{'fatal'})
    {
        $tabErrors->{'fatal'} = [];
    }

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Recherche des lignes de transport
    my $tabFilters = {
        'stateId' => [STATE_ACTIVE, STATE_DELETED],
        'type'    => [TYPE_DELIVERY, TYPE_RECOVERY],
        'from-to' => {'type' => FROMTOTYPE_ESTIMATELINE, 'id' => $id}
    };
    my $tabList = &getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters, GETINFOS_FROM | GETINFOS_TO |
                                                                              GETINFOS_MACHINE | GETINFOS_ORDER);

    my @tabIds = keys %$tabList;
    if (@tabIds > 0)
    {
        # Suppression des commandes associées
        foreach my $tabInfos (values %$tabList)
        {
            # Mise à jour des alertes
            &_updateAlerts($tabInfos, undef, {}, $tabEndUpds);

            # Suppression définitive de tous les bordereaux existants pour la ligne de devis (supprimés ou non)
            if ($db->delete('f_transportselfform', {'tsf_tsp_id*' => \@tabIds}, $schemaName) == -1)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }

                print STDERR sprintf($fatalErrMsg, 'deleteAllForEstimateLine', __LINE__, __FILE__, '');
                push(@{$tabErrors->{'fatal'}}, 0x0001);
                return 0;
            }

            # Suppression définitive de la commande
            if ($tabInfos->{'order.id'})
            {
                my $tabOrderInfos = $tabInfos->{'order.tabInfos'};

                # Envoi de la suppression à OTD
                $db->addTransactionAction('commit.post', sub {
                        my $tabSendOptions = {
                            'sendingsCount' => $tabOrderInfos->{'sendingsCount'},
                            'lastHash'      => $tabOrderInfos->{'lastHash'}
                        };
                        return &LOC::Otd::deleteOrder($countryId, $tabOrderInfos->{'code'}, $tabSendOptions);
                    },
                    {'category' => 'transport',
                     'priority' => -1000,
                     'id' => 'LOC::Otd::deleteOrder_' . $countryId . '_' . $tabOrderInfos->{'code'}}
                );

                # Suppression de la ligne de la commande
                if ($db->delete('f_transportorder', {'tor_id*' => $tabOrderInfos->{'id'}}, $schemaName) == -1)
                {
                    # Annule la transaction
                    if (!$hasTransaction)
                    {
                        $db->rollBack();
                    }

                    print STDERR sprintf($fatalErrMsg, 'deleteAllForEstimateLine', __LINE__, __FILE__, '');
                    push(@{$tabErrors->{'fatal'}}, 0x0001);
                    return 0;
                }
            }
        }


        # Suppression des lignes de commandes à l'état "Supprimée" (TSP03)
        if ($db->delete('f_transportorder', {'tor_tsp_id*' => \@tabIds}, $schemaName) == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }

            print STDERR sprintf($fatalErrMsg, 'deleteAllForEstimateLine', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }

        # Suppression des lignes de transport
        if ($db->delete('f_transport', {'tsp_id*' => \@tabIds}, $schemaName) == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }

            print STDERR sprintf($fatalErrMsg, 'deleteAllForEstimateLine', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }

        # Suppression des lignes de transport en cours
        if ($db->delete('f_pendingtransport', {'pdt_id*' => \@tabIds}, $schemaName) == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }

            print STDERR sprintf($fatalErrMsg, 'deleteAllForEstimateLine', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();

            print STDERR sprintf($fatalErrMsg, 'deleteAllForEstimateLine', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: doDocumentDelivery
# Réalise la livraison d'un document (ligne de devis ou contrat)
#
# Parameters:
# string  $countryId        - Identifiant du pays
# string  $documentType     - Type de document ('estimateLine': ligne de devis, 'contract' : contrat)
# int     $documentId       - Id du document
# bool    $doLink           - réaliser le transport du document lié (si transfert, sinon 0)
# string  $date             - Date de déchargement (yyyy-mm-dd hh:mm:ss)
# int     $userId           - Utilisateur responsable
# hashref $tabErrors        - Tableau des erreurs
# hashref $tabEndUpds       - Mises à jour de fin (facultatif)
#
# Returns:
# bool
sub doDocumentDelivery
{
    my ($countryId, $documentType, $documentId, $doLink, $date, $userId, $tabErrors, $tabEndUpds) = @_;

    if ($documentType ne 'contract')
    {
        return 1;
    }

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Infos du transport
    my $tabOldInfos = &getDocumentDeliveryInfos($countryId, $documentType, $documentId,
                                                GETINFOS_FROM | GETINFOS_TO,
                                                {'lockEntityDbRow'   => 1,
                                                 'getFromInfosFlags' => GETFROMTOINFOS_DETAILS,
                                                 'getToInfosFlags'   => GETFROMTOINFOS_DETAILS});

    if (!$tabOldInfos)
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'doDocumentDelivery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Vérifications
    # - faisable
    # - pas déjà réalisé ?
    if (($tabOldInfos->{'unloading'}->{'isDone'} || !$tabOldInfos->{'unloading'}->{'isDoable'}) ||
        ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER && $doLink &&
            ($tabOldInfos->{'loading'}->{'isDone'} || !$tabOldInfos->{'loading'}->{'isDoable'})) ||
        ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER && !$doLink && !$tabOldInfos->{'loading'}->{'isDone'}))
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Impossible de réaliser le transport
        push(@{$tabErrors->{'fatal'}}, 0x0106);
        return 0;
    }

    # Si on est dans la réalisation d'un transfert complet, il faut d'abord réaliser la récupération de ce transfert
    if ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER)
    {
        # Mise à jour de la date de modification du contrat
        if ($db->update('CONTRAT', {'CONTRATDATEMODIF*' => 'Now()'},
                                   {'CONTRATAUTO*' => $tabOldInfos->{'from.id'}}, $schemaName) == -1)
        {
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'doDocumentDelivery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }

        # Réalisation du chargement
        if ($doLink && !&_doLoading($db, $countryId, $tabOldInfos, $date, $userId, $tabEndUpds))
        {
            if (!$hasTransaction)
            {
                # Annule la transaction
                $db->rollBack();
            }
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'doDocumentDelivery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Mise à jour de la date de modification du contrat
    if ($db->update('CONTRAT', {'CONTRATDATEMODIF*' => 'Now()'},
                               {'CONTRATAUTO*' => $tabOldInfos->{'to.id'}}, $schemaName) == -1)
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'doDocumentDelivery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Réalisation du déchargement
    if (!&_doUnloading($db, $countryId, $tabOldInfos, $documentType, $date, $userId, $tabEndUpds))
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'doDocumentDelivery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();

            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'doDocumentDelivery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: doDocumentRecovery
# Réalise la récupération d'un document (ligne de devis ou contrat)
#
# Parameters:
# string  $countryId    - Identifiant du pays
# string  $documentType - Type de document ('estimateLine': ligne de devis, 'contract' : contrat)
# int     $documentId   - Id du document
# bool    $doLink       - faut-il réaliser le transport du document lié
# string  $date         - Date de chargement (yyyy-mm-dd hh:mm:ss)
# int     $userId       - Utilisateur responsable
# hashref $tabErrors    - Tableau des erreurs
# hashref $tabEndUpds   - Mises à jour de fin (facultatif)
#
# Returns:
# bool
sub doDocumentRecovery
{
    my ($countryId, $documentType, $documentId, $doLink, $date, $userId, $tabErrors, $tabEndUpds) = @_;

    if ($documentType ne 'contract')
    {
        return 1;
    }

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Infos sur le transport
    my $tabOldInfos = &getDocumentRecoveryInfos($countryId, $documentType, $documentId,
                                                GETINFOS_FROM | GETINFOS_TO,
                                                {'lockEntityDbRow'   => 1,
                                                 'getFromInfosFlags' => GETFROMTOINFOS_DETAILS,
                                                 'getToInfosFlags'   => GETFROMTOINFOS_DETAILS});

    if (!$tabOldInfos)
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'doDocumentRecovery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Vérifications
    # - faisable
    # - pas déjà réalisé ?
    if (($tabOldInfos->{'loading'}->{'isDone'} || !$tabOldInfos->{'loading'}->{'isDoable'}) ||
        ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER && $doLink &&
            ($tabOldInfos->{'unloading'}->{'isDone'} || !$tabOldInfos->{'unloading'}->{'isDoable'})))
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Impossible de réaliser le transport
        push(@{$tabErrors->{'fatal'}}, 0x0106);
        return 0;
    }

    # Mise à jour de la date de modification du contrat
    if ($db->update('CONTRAT', {'CONTRATDATEMODIF*' => 'Now()'},
                               {'CONTRATAUTO*' => $tabOldInfos->{'from.id'}}, $schemaName) == -1)
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'doDocumentRecovery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Réalisation du chargement
    if (!&_doLoading($db, $countryId, $tabOldInfos, $date, $userId, $tabEndUpds))
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'doDocumentRecovery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Si on est dans la réalisation d'un transfert complet, il faut ensuite réaliser la livraison de ce transfert
    if ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER)
    {
        # Mise à jour de la date de modification du contrat
        if ($db->update('CONTRAT', {'CONTRATDATEMODIF*' => 'Now()'},
                                   {'CONTRATAUTO*' => $tabOldInfos->{'to.id'}}, $schemaName) == -1)
        {
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'doDocumentRecovery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }

        # Réalisation du déchargement
        if ($doLink && !&_doUnloading($db, $countryId, $tabOldInfos, $documentType, $date, $userId, $tabEndUpds))
        {
            if (!$hasTransaction)
            {
                # Annule la transaction
                $db->rollBack();
            }
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'doDocumentRecovery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }
    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();

            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'doDocumentRecovery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: doAgenciesTransfer
# Réalise le transfert inter-agence
#
# Parameters:
# string  $countryId        - Identifiant du pays
# int     $machineId        - Id de la machine
# string  $date             - Date de déchargement (yyyy-mm-dd hh:mm:ss)
# int     $userId           - Utilisateur responsable
# hashref $tabErrors        - Tableau des erreurs
# hashref $tabEndUpds       - Mises à jour de fin (facultatif)
#
# Returns:
# bool
sub doAgenciesTransfer
{
    my ($countryId, $machineId, $date, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Infos du transfert inter-agence
    my $tabOldInfos = &getAgenciesTransferInfos($countryId, $machineId, GETINFOS_BASE | GETINFOS_MACHINE,
                                                {'isSortDisabled' => 1,
                                                 'lastId'         => 1
                                                });

    if (!$tabOldInfos)
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'doAgenciesTransfer', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Vérifications
    # - faisable
    # - pas déjà réalisé ?? -> pointage manuel
    if ($tabOldInfos->{'unloading'}->{'isDone'} ||
        (!$tabOldInfos->{'unloading'}->{'isDone'} && !$tabOldInfos->{'unloading'}->{'isDoable'}))
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'doAgenciesTransfer', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Réalisation du transfert inter-agence
    if (!&_doAgenciesTransfer($db, $countryId, $tabOldInfos, $date, $userId, $tabEndUpds))
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'doAgenciesTransfer', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'doAgenciesTransfer', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Si la machine du transfert inter-agences était déjà attribuée à un contrat
    # il faut verrouiller le transfert pour ne pas pouvoir l'annuler
    if ($tabOldInfos->{'machine.tabInfos'}->{'isAffectedOnPrecontract'})
    {
        &lock($countryId, {'id' => $tabOldInfos->{'id'}}, $userId, $tabEndUpds);
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;

}


# Function: generateSelfForm
# Génère le bordereau d'enlèvement ou de restitution
#
# Parameters:
# string  $countryId   - Identifiant du pays
# int     $transportId - Identifiant du transport
# string  $side        - Côté demandeur
# bool    $isForPrint  - Indique si il s'agit
# int     $userId      - Identifiant de l'utilisateur responsable
# hashref $tabEndUpds  - Tableau des mises à jour de fin (facultatif)
#
# Returns:
# int - Identifiant du bordereau généré, 0 si une erreur s'est produite
sub generateSelfForm
{
    my ($countryId, $transportId, $side, $isForPrint, $userId, $tabEndUpds) = @_;


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération des informations du transport
    my $tabTransportInfos = &getInfos($countryId, $transportId);


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $selfFormId;
    if ($side eq SIDE_DELIVERY && $tabTransportInfos->{'deliverySelfForm.id'} ||
        $side eq SIDE_RECOVERY && $tabTransportInfos->{'recoverySelfForm.id'})
    {
        # Le bordereau existe déjà
        $selfFormId = ($side eq SIDE_DELIVERY ? $tabTransportInfos->{'deliverySelfForm.id'} : $tabTransportInfos->{'recoverySelfForm.id'});
        if ($isForPrint)
        {
            if ($db->update('f_transportselfform',
                            {'tsf_printscount*' => 'tsf_printscount + 1'},
                            {'tsf_id' => $selfFormId},
                            $schemaName) == -1)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return 0;
            }
        }
    }
    else
    {
        # Le bordereau n'existe pas et doit-être généré
        # On vérifie si il est possible d'éditer un bordereau sur ce transport
        if (!&_isSelfFormPossible($tabTransportInfos))
        {
            return 0;
        }

        my $tabUpdates = {
            'tsf_tsp_id'       => $transportId,
            'tsf_side'         => $side,
            'tsf_date*'        => 'CURDATE()',
            'tsf_printscount'  => ($isForPrint ? 1 : 0),
            'tsf_date_create*' => 'Now()',
            'tsf_sta_id'       => SELFFORM_STATE_ACTIVE
        };

        $selfFormId = $db->insert('f_transportselfform', $tabUpdates, $schemaName);
        if ($selfFormId == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return $selfFormId;
}


# Function: getContractsForSitesTransfer
# Récupère la liste des contrats éligibles pour un transfert inter-chantiers
#
# Parameters:
# string  $countryId   - Identifiant du pays
# string  $agencyId    - Code de l'agence
# int     $modelId     - Identifiant du modèle de machine
# string  $side        - Dans quel bloc se trouve-t-on ? (livraison ou récupération)
# hashref $tabInfosRef - Dates et horaires à prendre en compte pour la recherche des contrats
#                               ('isAnticipatable' : La livraison est-elle anticipée?,
#                                'referenceDate'   : Date de début pour la livraison - Date de fin pour la récupération,
#                                'referenceHour'   : Heure de livraison pour la livraison - Heure de récupération pour la récupération)
# int     $format      - Format de retour de la liste
# hashref $tabFilters  - Filtres (facultatif)
# int     $flags       - Flags pour le choix des colonnes (facultatif)
# hashref $tabOptions  - Options supplémentaires (facultatif) ('idToInclude' : Id des contrats à prendre en compte (forcer le contrat),
#                                                              'idToExclude' : Id des contrats à ne pas prendre en compte,
#                                                              'visibleOnAgencyId' : Agence de visibilité de la machine à transférer (utilisée côté récup)
#
# Returns:
# hashref - Liste des contrats
sub getContractsForSitesTransfer
{
    my ($countryId, $agencyId, $modelId, $side, $tabInfosRef, $format, $tabFilters, $flags, $tabOptions) = @_;

    if (!defined $format)
    {
        $format = LOC::Util::GETLIST_ASSOC;
    }

    if (!defined $flags || $format != LOC::Util::GETLIST_ASSOC)
    {
        $flags = GETSTCTTINFOS_BASE;
    }

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }
    if (!defined $tabFilters)
    {
        $tabFilters = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my @tabWheres = ();

    # Récupération de la caractéristique pour la gestion du délai à prendre en compte
    my $tabCfgdeltaMaxTsf = &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('DELTAMAXTSF', $agencyId));

    # Délai de référence à prendre en compte pour la livraison
    my $referenceDeadLineDelivery = ($tabInfosRef->{'isAnticipatable'} ? $tabCfgdeltaMaxTsf->{'anticipate'} : $tabCfgdeltaMaxTsf->{'base'});

    # Conditions pour savoir si il y a une erreur fatale
    my $errorsField = ($side eq SIDE_DELIVERY ? 'tsp_loadingerrors' : 'tsp_unloadingerrors');

    # - Conditions pour savoir s'il y a une proforma de prolongation non finalisée
    my $errorsProforma = ($side eq SIDE_DELIVERY ? 'IF(pfm.endDate IS NOT NULL, ' . ERROR_PFNOTPAYED . ', 0)': 0) ;

    # - Conditions sur la propriété "Enlèvement"
    my $errorIsSelf = '';
    if (defined $tabOptions->{'isSelf'})
    {
        if ($side eq SIDE_DELIVERY)
        {
            $errorIsSelf = '(IF (tbl_f_machine.MASUPPAGAUTO = ' . $db->quote($agencyId) . ' && tsp_is_self NOT IN (' . $db->quote($tabOptions->{'isSelf'}) . '), 1, 0))';
        }
        if ($side eq SIDE_RECOVERY)
        {
            if ($tabOptions->{'visibleOnAgencyId'})
            {
                $errorIsSelf = '(IF (tsp_is_self NOT IN (' . $db->quote($tabOptions->{'isSelf'}) . '), 1, 0))';
            }
        }
    }

    my $fatalErrCond = $errorsField . ' & ' . ' (' . ERROR_CUSTNOTVERIF . '|' . $errorsProforma . ')';
    if ($errorIsSelf ne '')
    {
        $fatalErrCond = '(' . $fatalErrCond . ') OR ' . $errorIsSelf;
    }

    # Conditions sur les horaires limites
    my $limitHourCond = ($side eq SIDE_DELIVERY ? 'RPAD(IF (tbl_p_site.CHHEUREAR != "", tbl_p_site.CHHEUREAR,
                                                   (IF(tbl_p_site.CHPLAGEAR IS NOT NULL, LPAD(tts_r.tts_beginhour, 2, "0"), NULL))), 8, ":00")' :
                                                  'RPAD(IF (tbl_p_site.CHHEUREDP != "", tbl_p_site.CHHEUREDP,
                                                   (IF(tbl_p_site.CHPLAGEDP IS NOT NULL, LPAD(tts_d.tts_endhour, 2, "0"), NULL))), 8, ":00")');
    # Conditions sur la date de fin (Côté livraison, si une date de fin souhaitée d'une proforma de prolongation existe
    # elle est prise comme référence sinon on prend la date de fin du contrat)
    my $dateEndDeliveryCond = 'IF(pfm.endDate IS NOT NULL, pfm.endDate, tbl_f_contract.CONTRATDR)';
    # Conditions sur les dates calculées avec le délai correspondant à prendre en compte
    my $referenceDateCond = ($side eq SIDE_DELIVERY ? 'DATE_ADD(' . $db->quote($tabInfosRef->{'referenceDate'}) . ',
                                                       INTERVAL -' . $referenceDeadLineDelivery . ' DAY)' :
                                                      'DATE_ADD(' . $db->quote($tabInfosRef->{'referenceDate'}) . ',
                                                       INTERVAL IF(tbl_p_site.CHANTICIPATION = 1,
                                                       ' . $tabCfgdeltaMaxTsf->{'anticipate'} . ',
                                                       ' . $tabCfgdeltaMaxTsf->{'base'} . ') DAY)');
    # Conditions sur les horaires à prendre en compte
     my $referenceHourCond = $db->quote((length($tabInfosRef->{'referenceHour'}) == 5 ?
                                            $tabInfosRef->{'referenceHour'} . ':00' : sprintf('%02s', $tabInfosRef->{'referenceHour'}). ':00:00'));

    my $query;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    tbl_f_contract.CONTRATAUTO AS `id`,
    tbl_f_contract.CONTRATCODE AS `code`,
    tbl_f_contract.AGAUTO AS `agency.id`,
    tbl_f_agency.agc_label AS `agency.label`,
    tbl_p_site.CHLIBELLE AS `site.label`,
    tbl_p_sitelocality.VICP AS `site.postalCode`,
    tbl_p_sitelocality.VINOM AS `site.city`,
    tbl_f_machine.MANOPARC AS `machine.parkNumber`,
    0 AS `isPlanned`,
    tsp_is_self AS `isSelf`,';

        if ($side eq SIDE_DELIVERY)
        {
            $query .= '
    ' . $dateEndDeliveryCond . ' AS `limitDate`,
    ' . $limitHourCond . ' AS `limitHour`,
    0 AS `isAnticipatable`,
    tbl_f_contractamount.MOTRANSPORTRFR as `amount`,';
        }
        elsif ($side eq SIDE_RECOVERY)
        {
             $query .= '
    tbl_f_contract.CONTRATDD AS `limitDate`,
    ' . $limitHourCond . ' AS `limitHour`,
    tbl_p_site.CHANTICIPATION AS `isAnticipatable`,
    tbl_f_contractamount.MOTRANSPORTDFR as `amount`,';
        }
        $query .='
    ' . $errorsField . ' AS `tabErrors`,
    IF (' . $fatalErrCond . ', 1, 0) AS `isFatalError`
FROM CONTRAT tbl_f_contract
INNER JOIN frmwrk.p_agency tbl_f_agency
ON tbl_f_agency.agc_id = tbl_f_contract.AGAUTO
INNER JOIN CHANTIER tbl_p_site
ON tbl_f_contract.CHAUTO = tbl_p_site.CHAUTO
INNER JOIN VILLECHA tbl_p_sitelocality
ON tbl_p_sitelocality.VILLEAUTO = tbl_p_site.VILLEAUTO
INNER JOIN MONTANT tbl_f_contractamount
ON tbl_f_contract.MOAUTO = tbl_f_contractamount.MOAUTO
LEFT JOIN AUTH.MACHINE tbl_f_machine
ON tbl_f_machine.MAAUTO = tbl_f_contract.MAAUTO';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*)
FROM CONTRAT tbl_f_contract
INNER JOIN CHANTIER tbl_p_site
ON tbl_f_contract.CHAUTO = tbl_p_site.CHAUTO
LEFT JOIN AUTH.MACHINE tbl_f_machine
ON tbl_f_machine.MAAUTO = tbl_f_contract.MAAUTO';
    }
    else
    {
        $query = '
SELECT
    CONTRATAUTO,
    CONTRATCODE
FROM CONTRAT tbl_f_contract
INNER JOIN CHANTIER tbl_p_site
ON tbl_f_contract.CHAUTO = tbl_p_site.CHAUTO
LEFT JOIN AUTH.MACHINE tbl_f_machine
ON tbl_f_machine.MAAUTO = tbl_f_contract.MAAUTO';
    }

    push(@tabWheres, 'IF(tbl_f_contract.MAAUTO = 0, tbl_f_contract.MOMAAUTO = ' . $modelId . ', tbl_f_machine.MOMAAUTO = ' . $modelId . ')');

    # Filtre état contrat / machine / réalisation
    if ($side eq SIDE_DELIVERY)
    {
        $query .= '
INNER JOIN f_transport
ON (tsp_rct_id_from = tbl_f_contract.CONTRATAUTO AND tsp_sta_id <> ' . $db->quote(STATE_DELETED) . ')
LEFT JOIN p_transporttimeslot tts_r
ON tts_r.tts_id = tbl_p_site.CHPLAGEAR
LEFT JOIN
(SELECT
    pfm_rct_id AS contractId,
    pfm_date_end AS endDate
FROM f_proforma
WHERE pfm_pft_id = ' . $db->quote(LOC::Proforma::Type::ID_EXTENSION) . '
AND pfm_sta_id = ' . $db->quote(LOC::Proforma::STATE_ACTIVE) . '
AND pfm_is_expired = 0
AND pfm_is_justified = 0
AND pfm_is_validated = 0
AND pfm_sta_id_encashment != ' . $db->quote(LOC::Proforma::ENCASHMENTSTATE_RECEIVED) . ') AS pfm
ON pfm.contractId = tbl_f_contract.CONTRATAUTO';

        push(@tabWheres, 'tbl_f_contract.ETCODE = ' . $db->quote(LOC::Contract::Rent::STATE_STOPPED));
        push(@tabWheres, 'tbl_f_contract.CONTRATMACHATTR = -1');
        push(@tabWheres, 'tsp_type = ' . $db->quote(TYPE_RECOVERY));
        push(@tabWheres, 'tsp_is_loadingdone = 0');

        # Conditions sur les contraintes des dates et des horaires à respecter pour un transfert
        push (@tabWheres, '(
        (' . $dateEndDeliveryCond . ' BETWEEN  ' . $referenceDateCond . ' AND ' . $db->quote($tabInfosRef->{'referenceDate'}) . '
        AND ' . $dateEndDeliveryCond . ' != ' . $db->quote($tabInfosRef->{'referenceDate'}) . ')
        OR
        ((' . $dateEndDeliveryCond . ' = ' . $db->quote($tabInfosRef->{'referenceDate'}) . ')
        AND ((' . $limitHourCond . ' <= ' . $referenceHourCond  . ')
        OR (' . $limitHourCond . ' IS NULL)))
        )');

        # Filtre sur l'agence
        push(@tabWheres, '((tbl_f_contract.AGAUTO = ' . $db->quote($agencyId) . ' AND tbl_f_machine.MASUPPAGAUTO IS NULL)
                            OR (tbl_f_machine.MASUPPAGAUTO = ' . $db->quote($agencyId) . '))');

        # TODO : attention condition supplémentaire sur la commande : récup non planifiée

    }
    elsif ($side eq SIDE_RECOVERY)
    {
        $query .= '
INNER JOIN f_transport
ON (tsp_rct_id_to = tbl_f_contract.CONTRATAUTO AND tsp_sta_id <> ' . $db->quote(STATE_DELETED) . ')
LEFT JOIN p_transporttimeslot tts_d
ON tts_d.tts_id = tbl_p_site.CHPLAGEDP';

        push(@tabWheres, 'tbl_f_contract.ETCODE = ' . $db->quote(LOC::Contract::Rent::STATE_PRECONTRACT));
        push(@tabWheres, 'tbl_f_contract.MAAUTO = 0');
        push(@tabWheres, 'tsp_type = ' . $db->quote(TYPE_DELIVERY));
        push(@tabWheres, 'tsp_is_unloadingdone = 0');

        # Conditions sur les contraintes des dates et des horaires à respecter pour un transfert
        push(@tabWheres, '(
        (tbl_f_contract.CONTRATDD BETWEEN ' . $db->quote($tabInfosRef->{'referenceDate'}) . ' AND ' . $referenceDateCond . '
        AND tbl_f_contract.CONTRATDD != ' . $db->quote($tabInfosRef->{'referenceDate'}) . ')
        OR
        ((tbl_f_contract.CONTRATDD = ' . $db->quote($tabInfosRef->{'referenceDate'}) . ')
        AND ((' . $limitHourCond . ' >= ' . $referenceHourCond . ')
        OR (' . $referenceHourCond . ' IS NULL)))
        )'
        );

        # Filtre sur l'agence
        if ($tabOptions->{'visibleOnAgencyId'})
        {
            push(@tabWheres, '(tbl_f_contract.AGAUTO = ' . $db->quote($tabOptions->{'visibleOnAgencyId'}) . ')');
        }
        else
        {
            push(@tabWheres, '(tbl_f_contract.AGAUTO = ' . $db->quote($agencyId) . ')');
        }

        # TODO : attention condition supplémentaire sur la commande : livraison non planifiée

    }

    $query .= '
LEFT JOIN f_transportorder
ON tor_tsp_id = tsp_id AND tor_sta_id <> ' . $db->quote(ORDER_STATE_DELETED) . '
WHERE tbl_f_contract.CONTRATOK = -1';

    # - Filtres :
    # - sur l'identifiant
    if (defined $tabFilters->{'id'})
    {
        push(@tabWheres, 'tbl_f_contract.CONTRATAUTO IN (' . &LOC::Util::join(', ', $tabFilters->{'id'}) . ')');
    }

    # - On ne veut pas les cas avec une erreur fatale
    if ($tabFilters->{'noFatalError'})
    {
        $query .= '
AND !(' . $fatalErrCond . ')';
    }

    # - Options supplémentaires :

    # Liste des contrats à exclure
    if (defined $tabOptions->{'idToExclude'})
    {
        $query .= '
AND tbl_f_contract.CONTRATAUTO NOT IN (' . &LOC::Util::join(', ', $tabOptions->{'idToExclude'}) . ')';
    }

    $query .= '
AND ((' . join("\nAND ", @tabWheres) . ')';

    # Liste des contrats à inclure
    if (defined $tabOptions->{'idToInclude'})
    {
        $query .= ' OR tbl_f_contract.CONTRATAUTO IN (' . &LOC::Util::join(', ', $tabOptions->{'idToInclude'}) . ')';
    }
    $query .= ')
ORDER BY tbl_f_contract.CONTRATDD ASC, tbl_f_contract.CONTRATAUTO ASC;';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    # Formatage des champs
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}              *= 1;
            $tabRow->{'amount'}          *= 1;
            $tabRow->{'isPlanned'}       *= 1;
            $tabRow->{'isSelf'}          *= 1;
            $tabRow->{'isAnticipatable'} *= 1;
            $tabRow->{'tabErrors'}        = &_getErrors($tabRow->{'tabErrors'});

            $tabRow->{'isFatalError'}    *= 1;
        }
    }

    return $tab;
}


# Function: getDocumentDeliveryInfos
# Retourne les infos d'un transport de livraison d'un document (ligne de devis ou contrat)
#
# Parameters:
# string  $countryId    - Pays
# string  $documentType - Type de document ('estimateLine': ligne de devis, 'contract' : contrat)
# int     $documentId   - Id du document
# int     $flags        - Flags (facultatif)
# hashref $tabOptions   - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getDocumentDeliveryInfos
{
    my ($countryId, $documentType, $documentId, $flags, $tabOptions) = @_;

    my $toType;
    if ($documentType eq 'contract')
    {
        $toType = FROMTOTYPE_CONTRACT;
    }
    elsif ($documentType eq 'estimateLine')
    {
        $toType = FROMTOTYPE_ESTIMATELINE;
    }
    else
    {
        # Erreur fatale !
        return undef;
    }

    my $tabFilters = {
        'type' => [TYPE_DELIVERY, TYPE_SITESTRANSFER],
        'to'   => {'type' => $toType, 'id' => $documentId}
    };
    my $tabList = &getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters, $flags, $tabOptions);

    my @tabIds = keys %$tabList;
    if (@tabIds == 0)
    {
        return undef;
    }

    return (wantarray ? %{$tabList->{$tabIds[0]}} : $tabList->{$tabIds[0]});
}


# Function: getDocumentRecoveryInfos
# Retourne les infos d'un transport de récupération d'un document (ligne de devis ou contrat)
#
# Parameters:
# string  $countryId    - Pays
# string  $documentType - Type de document ('estimateLine': ligne de devis, 'contract' : contrat)
# int     $documentId   - Id du document
# int     $flags        - Flags (facultatif)
# hashref $tabOptions   - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getDocumentRecoveryInfos
{
    my ($countryId, $documentType, $documentId, $flags, $tabOptions) = @_;

    my $fromType;
    if ($documentType eq 'contract')
    {
        $fromType = FROMTOTYPE_CONTRACT;
    }
    elsif ($documentType eq 'estimateLine')
    {
        $fromType = FROMTOTYPE_ESTIMATELINE;
    }
    else
    {
        # Erreur fatale !
        return undef;
    }

    my $tabFilters = {
        'type' => [TYPE_RECOVERY, TYPE_SITESTRANSFER],
        'from' => {'type' => $fromType, 'id' => $documentId}
    };
    my $tabList = &getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters, $flags, $tabOptions);

    my @tabIds = keys %$tabList;
    if (@tabIds == 0)
    {
        return undef;
    }

    return (wantarray ? %{$tabList->{$tabIds[0]}} : $tabList->{$tabIds[0]});
}


# Function: getAgenciesTransferInfos
# Retourne les infos d'un transfert inter-agence
#
# Parameters:
# string  $countryId    - Pays
# int     $machineId    - Id de la machine
# int     $flags        - Flags (facultatif)
# hashref $tabOptions   - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getAgenciesTransferInfos
{
    my ($countryId, $machineId, $flags, $tabOptions) = @_;

    my $tabFilters = {
        'type'          => [TYPE_AGENCIESTRANSFER],
        'machineId'     => $machineId,
        'isLocked'      => 0
    };
    if (defined $tabOptions->{'isUnloadingDone'})
    {
        $tabFilters->{'isUnloadingDone'} = $tabOptions->{'isUnloadingDone'};
    }

    my $tabList = &getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters, $flags, $tabOptions);

    my @tabIds = sort {-($tabList->{$a}->{'date'} cmp $tabList->{$b}->{'date'})} keys(%$tabList);
    if (@tabIds == 0)
    {
        return undef;
    }

    return (wantarray ? %{$tabList->{$tabIds[0]}} : $tabList->{$tabIds[0]});
}


# Function: getInfos
# Retourne les infos d'un transport
#
# Parameters:
# string  $countryId  - Pays
# int     $id         - Id du transport
# int     $flags      - Flags (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);
    return ($tab && exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}


# Function: getList
# Retourne la liste des transports
#
# Contenu du hashage :
# - id => identifiant,
# - type => type de transport ('D', 'R', 'TS' ou 'TA' (voir constantes TYPE_*)),
# - agency.id => agence,
# - agency.label => libellé de l'agence,
# - delegatedAgency.id => agence déléguée,
# - delegatedAgency.label => libellé de l'agence déléguée,
# - isSelf => Est-ce un enlèvement sur place,
# - date => date du transport,
# - machine.id => id de la machine à transporter,
# - from.type => type de l'entité de départ ('agency', 'contract', 'estimateLine' (voir constantes FROMTOTYPE_*)),
# - from.id => id de l'entité de départ,
# - to.type => type de l'entité d'arrivée ('agency', 'contract', 'estimateLine' (voir constantes FROMTOTYPE_*)),
# - to.id => id de l'entité de d'arrivée,
# -- Un sous tableau "loading" pour le chargement :
# - isDone => Le chargement de la machine a-t-il été effectué ?,
# - isDoable => Le chargement est-il faisable/annulable ?,
# - errors => Erreurs bloquantes pour le chargement,
# - tabErrors => Erreurs bloquantes pour le chargement (sous forme de tableau),
# - date => Date de réalisation du chargement de la machine,
# -- Un sous tableau "unloading" pour le déchargement :
# - isDone => Le déchargement de la machine a-t-il été effectué ?,
# - isDoable => Le déchargement est-il faisable/annulable ?,
# - errors => Erreurs bloquantes pour le déchargement,
# - tabErrors => Erreurs bloquantes pour le déchargement (sous forme de tableau),
# - date => Date de réalisation du déchargement de la machine,
# - comments => commentaires,
# - order.id => identifiant de la commande de transport si il y en a une,
# - selfForm.id => identifiant du bordereau d'enlèvement ou de restitution si il y en a un,
# - isLocked => est-il bloqué en modification ?,
# - isSynchroDisabled => faut-il désactiver la synchronisation des commandes ?,
# - valueDate => Date de valeur,
# - creationDate => Date de création,
# - modificationDate => Date de modification,
# - state.id => état
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres (facultatif)
# int     $flags      - Flags pour le choix des colonnes (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Liste des transports
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;

    if (!defined $format)
    {
        $format = LOC::Util::GETLIST_ASSOC;
    }

    if (!defined $flags || $format != LOC::Util::GETLIST_ASSOC)
    {
        $flags = GETINFOS_BASE;
    }

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }
    if (!defined $tabOptions->{'orders'})
    {
        # Tri par défaut : par date croissante
        $tabOptions->{'orders'} = [
            {
                'field' => 'date',
                'dir'   => 'ASC'
            }
        ];
    }
    if (!defined $tabFilters)
    {
        $tabFilters = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $tabFields = {
        'id'        => 'tsp_id',
        'valueDate' => 'tsp_date_value',
        'date'      => 'tsp_date'
    };
    my @tabWheres = ();


    my $query;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    tsp_id AS `id`,
    tsp_type AS `type`,
    tsp_is_self AS `isSelf`,
    tsp_date AS `date`,
    tsp_agc_id AS `agency.id`,
    p_agency_main.agc_label AS `agency.label`,
    tsp_agc_id_delegated AS `delegatedAgency.id`,
    p_agency_delegated.agc_label AS `delegatedAgency.label`,
    tsp_agc_id_transferto AS `transferToAgency.id`,
    p_agency_transferto.agc_label AS `transferToAgency.label`,
    tsp_mod_id AS `model.id`,
    tsp_mac_id AS `machine.id`,
    IF (tsp_agc_id_from IS NULL, IF (tsp_rel_id_from IS NULL, "' . FROMTOTYPE_CONTRACT . '", "' . FROMTOTYPE_ESTIMATELINE . '"), "' . FROMTOTYPE_AGENCY . '") AS `from.type`,
    IF (tsp_agc_id_from IS NULL, IF (tsp_rel_id_from IS NULL, tsp_rct_id_from, tsp_rel_id_from), tsp_agc_id_from) AS `from.id`,
    IF (tsp_agc_id_to IS NULL, IF (tsp_rel_id_to IS NULL, "' . FROMTOTYPE_CONTRACT . '", "' . FROMTOTYPE_ESTIMATELINE . '"), "' . FROMTOTYPE_AGENCY . '") AS `to.type`,
    IF (tsp_agc_id_to IS NULL, IF (tsp_rel_id_to IS NULL, tsp_rct_id_to, tsp_rel_id_to), tsp_agc_id_to) AS `to.id`,
    tsp_is_loadingdone AS `loading.isDone`,
    tsp_is_loadingdoable AS `loading.isDoable`,
    tsp_loadingerrors AS `loading.errors`,
    tsp_date_loading AS `loading.date`,
    tsp_is_unloadingdone AS `unloading.isDone`,
    tsp_is_unloadingdoable AS `unloading.isDoable`,
    tsp_unloadingerrors AS `unloading.errors`,
    tsp_date_unloading AS `unloading.date`,
    tsp_comments AS `comments`,
    (SELECT tor_id FROM f_transportorder WHERE tor_tsp_id = tsp_id AND tor_sta_id <> ' . $db->quote(ORDER_STATE_DELETED) . ' LIMIT 1) AS `order.id`,
    (SELECT tsf_id FROM f_transportselfform
        WHERE tsf_tsp_id = tsp_id
        AND tsf_side = ' . $db->quote(SIDE_DELIVERY) . '
        AND tsf_sta_id <> ' . $db->quote(SELFFORM_STATE_DELETED) . ' LIMIT 1) AS `deliverySelfForm.id`,
    (SELECT tsf_id FROM f_transportselfform
        WHERE tsf_tsp_id = tsp_id
        AND tsf_side = ' . $db->quote(SIDE_RECOVERY) . '
        AND tsf_sta_id <> ' . $db->quote(SELFFORM_STATE_DELETED) . ' LIMIT 1) AS `recoverySelfForm.id`,
    tsp_is_locked AS `isLocked`,
    tsp_is_synchrodisabled AS `isSynchroDisabled`,
    tsp_considertype AS `isConsidered`,
    tsp_usr_id_considered AS `consideredUser.id`,
    tsp_date_considered AS `consideredDate`,
    tsp_date_value AS `valueDate`,
    tsp_date_create AS `creationDate`,
    tsp_date_modif AS `modificationDate`,
    tsp_sta_id AS `state.id`
FROM f_transport
INNER JOIN frmwrk.p_agency p_agency_main
ON p_agency_main.agc_id = tsp_agc_id
LEFT JOIN frmwrk.p_agency p_agency_delegated
ON p_agency_delegated.agc_id = tsp_agc_id_delegated
LEFT JOIN frmwrk.p_agency p_agency_transferto
ON p_agency_transferto.agc_id = tsp_agc_id_transferto';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*)
FROM f_transport';
    }
    else
    {
        $query = '
SELECT
    tsp_id,
    tsp_date
FROM f_transport';
    }


    # Filtres
    # - sur l'identifiant
    if (defined $tabFilters->{'id'})
    {
        push(@tabWheres, 'tsp_id IN (' . &LOC::Util::join(', ', $tabFilters->{'id'}) . ')');
    }

    # - identifiants à exclure
    if (defined $tabFilters->{'idToExclude'})
    {
        push(@tabWheres, 'tsp_id NOT IN (' . &LOC::Util::join(', ', $tabFilters->{'idToExclude'}) . ')');
    }

    # - sur le type
    if (defined $tabFilters->{'type'})
    {
        push(@tabWheres, 'tsp_type IN (' . $db->quote($tabFilters->{'type'}) . ')');
    }

    # - sur l'agence principale
    if (defined $tabFilters->{'agencyId'})
    {
        push(@tabWheres, 'tsp_agc_id IN (' . $db->quote($tabFilters->{'agencyId'}) . ')');
    }

    # - sur l'état
    if (defined $tabFilters->{'stateId'})
    {
        push(@tabWheres, 'tsp_sta_id IN (' . $db->quote($tabFilters->{'stateId'}) . ')');
    }
    else
    {
        push(@tabWheres, 'tsp_sta_id <> ' . $db->quote(STATE_DELETED));
    }

    # - sur l'enlèvement sur place
    if (defined $tabFilters->{'isSelf'})
    {
        push(@tabWheres, 'tsp_is_self IN (' . $db->quote($tabFilters->{'isSelf'}) . ')');
    }
    # - sur la machine
    if (defined $tabFilters->{'machineId'})
    {
        push(@tabWheres, 'tsp_mac_id IN (' . $db->quote($tabFilters->{'machineId'}) . ')');
    }
    # - sur le blocage
    if (defined $tabFilters->{'isLocked'})
    {
        push(@tabWheres, 'tsp_is_locked IN (' . $db->quote($tabFilters->{'isLocked'}) . ')');
    }
    # - sur la prise en compte
    if (defined $tabFilters->{'isConsidered'})
    {
        push(@tabWheres, 'tsp_considertype IN (' . $db->quote($tabFilters->{'isConsidered'}) . ')');
    }
    # - sur la date de transfert
    if (defined $tabFilters->{'transferDate'})
    {
        push(@tabWheres, 'tsp_date = ' . $db->quote($tabFilters->{'transferDate'}));
    }
    # - sur l'agence de provenance
    if (defined $tabFilters->{'agencyIdFrom'})
    {
        push(@tabWheres, 'tsp_agc_id_from IN (' . $db->quote($tabFilters->{'agencyIdFrom'}) . ')');
    }
    # - sur l'agence de destination
    if (defined $tabFilters->{'agencyIdTo'})
    {
        push(@tabWheres, 'tsp_agc_id_to IN (' . $db->quote($tabFilters->{'agencyIdTo'}) . ')');
    }


    # =============================
    # Recherche sur l'entité de départ et/ou d'arrivée
    my @tabFromToFilters = ();

    if (defined $tabFilters->{'from-to'})
    {
        push(@tabFromToFilters, {
            'types'  => ['from', 'to'],
            'filter' => $tabFilters->{'from-to'}
        });
    }
    if (defined $tabFilters->{'from'})
    {
        push(@tabFromToFilters, {
            'types'  => ['from'],
            'filter' => $tabFilters->{'from'}
        });
    }
    if (defined $tabFilters->{'to'})
    {
        push(@tabFromToFilters, {
            'types'  => ['to'],
            'filter' => $tabFilters->{'to'}
        });
    }


    foreach my $tabFromToFilter (@tabFromToFilters)
    {
        my @tabFromToWheres = ();
        foreach my $type (@{$tabFromToFilter->{'types'}})
        {
            my $flt = $tabFromToFilter->{'filter'};

            my @tabTypeWheres = ();
            if ($flt->{'type'} eq FROMTOTYPE_AGENCY)
            {
                if (exists $flt->{'id'})
                {
                    push(@tabTypeWheres, 'tsp_agc_id_' . $type . ' IN (' . $db->quote($flt->{'id'}) . ')');
                }
            }
            elsif ($flt->{'type'} eq FROMTOTYPE_CONTRACT)
            {
                if (exists $flt->{'id'})
                {
                    push(@tabTypeWheres, 'tsp_rct_id_' . $type . ' IN (' . &LOC::Util::join(', ', $flt->{'id'}) . ')');
                }

                # Sous conditions
                my @tabSubWheres = ();
                if (exists $flt->{'customerId'})
                {
                    push(@tabSubWheres, 'subrct_fromto.CLAUTO IN (' . &LOC::Util::join(', ', $flt->{'customerId'}) . ')');
                }
                if (exists $flt->{'agencyId'})
                {
                    push(@tabSubWheres, 'subrct_fromto.AGAUTO IN (' . $db->quote($flt->{'agencyId'}) . ')');
                }
                if (exists $flt->{'siteId'})
                {
                    push(@tabSubWheres, 'subrct_fromto.CHAUTO IN (' . &LOC::Util::join(', ', $flt->{'siteId'}) . ')');
                }
                if (exists $flt->{'stateId'})
                {
                    push(@tabSubWheres, 'subrct_fromto.ETCODE IN (' . $db->quote($flt->{'stateId'}) . ')');
                }
                if (exists $flt->{'invoiceStateId'})
                {
                    push(@tabSubWheres, 'subrct_fromto.ETATFACTURE IN (' . $db->quote($flt->{'invoiceStateId'}) . ')');
                }
                if (exists $flt->{'isClosed'})
                {
                    push(@tabSubWheres, 'subrct_fromto.CONTRATDATECLOTURE' . ($flt->{'isClosed'} ? ' IS NOT NULL' : ' IS NULL'));
                }

                if (@tabSubWheres > 0)
                {
                    my $subQuery = 'SELECT subrct_fromto.CONTRATAUTO FROM CONTRAT subrct_fromto WHERE ' . join(' AND ', @tabSubWheres);
                    push(@tabTypeWheres, 'tsp_rct_id_' . $type . ' IN (' . $subQuery . ')');
                }
            }
            elsif ($flt->{'type'} eq FROMTOTYPE_ESTIMATELINE)
            {
                if (exists $flt->{'id'})
                {
                    push(@tabTypeWheres, 'tsp_rel_id_' . $type . ' IN (' . &LOC::Util::join(', ', $flt->{'id'}) . ')');
                }

                # Sous conditions
                my @tabSubWheres = ();
                if (exists $flt->{'customerId'})
                {
                    push(@tabSubWheres, 'subres_fromto.CLAUTO IN (' . &LOC::Util::join(', ', $flt->{'customerId'}) . ')');
                }
                if (exists $flt->{'agencyId'})
                {
                    push(@tabSubWheres, 'subres_fromto.AGAUTO IN (' . $db->quote($flt->{'agencyId'}) . ')');
                }
                if (exists $flt->{'estimateId'})
                {
                    push(@tabSubWheres, 'subres_fromto.DEVISAUTO IN (' . &LOC::Util::join(', ', $flt->{'estimateId'}) . ')');
                }
                if (exists $flt->{'siteId'})
                {
                    push(@tabSubWheres, 'subres_fromto.CHAUTO IN (' . &LOC::Util::join(', ', $flt->{'siteId'}) . ')');
                }

                if (@tabSubWheres > 0)
                {
                    my $subQuery = 'SELECT subrel_fromto.DETAILSDEVISAUTO ' .
                                   'FROM DETAILSDEVIS subrel_fromto ' .
                                   'INNER JOIN DEVIS subres_fromto ' .
                                   'ON subres_fromto.DEVISAUTO = subrel_fromto.DEVISAUTO ' .
                                   'WHERE ' . join(' AND ', @tabSubWheres);
                    push(@tabTypeWheres, 'tsp_rel_id_' . $type . ' IN (' . $subQuery . ')');
                }
            }

            if (@tabTypeWheres > 0)
            {
                push(@tabFromToWheres, '(' . join(' AND ', @tabTypeWheres) . ')');
            }
        }

        if (@tabFromToWheres > 0)
        {
            push(@tabWheres, '(' . join(' OR ', @tabFromToWheres) . ')');
        }
    }

    # - sur le pointage du transport
    if (defined $tabFilters->{'isLoadingDone'})
    {
        push(@tabWheres, 'tsp_is_loadingdone = ' . $tabFilters->{'isLoadingDone'});
    }
    if (defined $tabFilters->{'isUnloadingDone'})
    {
        push(@tabWheres, 'tsp_is_unloadingdone = ' . $tabFilters->{'isUnloadingDone'});
    }



    if (@tabWheres > 0)
    {
        $query .= '
WHERE ' . join("\nAND ", @tabWheres);
    }

    if (!$tabOptions->{'isSortDisabled'})
    {
        my @orders;
        foreach my $order (@{$tabOptions->{'orders'}})
        {
            my $dir = ($order->{'dir'} ? $order->{'dir'} : '');
            push(@orders, $tabFields->{$order->{'field'}} . ' ' . $dir);
        }

        $query .= '
ORDER BY ' . join(', ', @orders);
    }
    if (defined $tabOptions->{'limit'})
    {
        $query .= '
LIMIT ' . $tabOptions->{'limit'};
    }

    if (defined $tabOptions->{'lastId'})
    {
        $query .= '
ORDER BY tsp_id DESC
LIMIT 1';
    }

    $query .= ';';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    # Formatage des champs
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}                 *= 1;
            $tabRow->{'isSelf'}             *= 1;
            $tabRow->{'delegatedAgency.id'}  = ($tabRow->{'delegatedAgency.id'} ne '' ? $tabRow->{'delegatedAgency.id'} : undef);
            $tabRow->{'transferToAgency.id'} = ($tabRow->{'transferToAgency.id'} ne '' ? $tabRow->{'transferToAgency.id'} : undef);
            $tabRow->{'isLocked'}           *= 1;
            $tabRow->{'isSynchroDisabled'}  *= 1;
            $tabRow->{'isConsidered'}       *= 1;

            # Modèle de machine à transporter
            $tabRow->{'model.id'} *= 1;
            if ($flags & GETINFOS_MODEL)
            {
                $tabRow->{'model.tabInfos'} = &_getModelInfos($countryId, $tabRow->{'model.id'});
            }

            # Machine à transporter
            $tabRow->{'machine.id'} = ($tabRow->{'machine.id'} ? $tabRow->{'machine.id'} * 1 : undef);
            if ($flags & GETINFOS_MACHINE)
            {
                if ($tabRow->{'machine.id'})
                {
                    $tabRow->{'machine.tabInfos'} = &_getMachineInfos($countryId, $tabRow->{'machine.id'});
                }
                else
                {
                    $tabRow->{'machine.tabInfos'} = undef;
                }
            }

            # Informations sur l'entité de départ
            if ($tabRow->{'from.type'} ne FROMTOTYPE_AGENCY)
            {
                $tabRow->{'from.id'} *= 1;
            }
            if ($flags & GETINFOS_FROM)
            {
                my $tabEntityOptions = {};
                if ($tabOptions->{'lockEntityDbRow'})
                {
                    $tabEntityOptions->{'lockDbRow'} = 1;
                }
                $tabRow->{'from.tabInfos'} = &_getFromToEntityInfos($countryId,
                                                                    $tabRow->{'from.type'},
                                                                    $tabRow->{'from.id'},
                                                                    SIDE_RECOVERY,
                                                                    $tabOptions->{'getFromInfosFlags'},
                                                                    $tabEntityOptions);
            }

            # Informations sur l'entité d'arrivée
            if ($tabRow->{'to.type'} ne FROMTOTYPE_AGENCY)
            {
                $tabRow->{'to.id'} *= 1;
            }
            if ($flags & GETINFOS_TO)
            {
                my $tabEntityOptions = {};
                if ($tabOptions->{'lockEntityDbRow'})
                {
                    $tabEntityOptions->{'lockDbRow'} = 1;
                }
                $tabRow->{'to.tabInfos'} = &_getFromToEntityInfos($countryId,
                                                                  $tabRow->{'to.type'},
                                                                  $tabRow->{'to.id'},
                                                                  SIDE_DELIVERY,
                                                                  $tabOptions->{'getToInfosFlags'},
                                                                  $tabEntityOptions);
            }

            # Chargement
            $tabRow->{'loading'} = {
                'isDone'    => $tabRow->{'loading.isDone'} * 1,
                'isDoable'  => $tabRow->{'loading.isDoable'} * 1,
                'errors'    => $tabRow->{'loading.errors'} * 1,
                'tabErrors' => &_getErrors($tabRow->{'loading.errors'}),
                'date'      => $tabRow->{'loading.date'}
            };

            delete $tabRow->{'loading.isDone'};
            delete $tabRow->{'loading.isDoable'};
            delete $tabRow->{'loading.errors'};
            delete $tabRow->{'loading.date'};


            # Déchargement
            $tabRow->{'unloading'} = {
                'isDone'    => $tabRow->{'unloading.isDone'} * 1,
                'isDoable'  => $tabRow->{'unloading.isDoable'} * 1,
                'errors'    => $tabRow->{'unloading.errors'} * 1,
                'tabErrors' => &_getErrors($tabRow->{'unloading.errors'}),
                'date'      => $tabRow->{'unloading.date'}
            };

            delete $tabRow->{'unloading.isDone'};
            delete $tabRow->{'unloading.isDoable'};
            delete $tabRow->{'unloading.errors'};
            delete $tabRow->{'unloading.date'};


            # Informations sur la commande
            $tabRow->{'order.id'} = ($tabRow->{'order.id'} ? $tabRow->{'order.id'} * 1 : undef);
            if ($flags & GETINFOS_ORDER)
            {
                if ($tabRow->{'order.id'})
                {
                    $tabRow->{'order.tabInfos'} = &getOrderInfos($countryId, $tabRow->{'order.id'},
                                                                 $tabOptions->{'getOrderInfosFlags'},
                                                                 $tabOptions->{'getOrderInfosOptions'});
                }
                else
                {
                    $tabRow->{'order.tabInfos'} = undef;
                }
            }

            # Informations sur le bordereau
            $tabRow->{'deliverySelfForm.id'} = ($tabRow->{'deliverySelfForm.id'} ? $tabRow->{'deliverySelfForm.id'} * 1 : undef);
            $tabRow->{'recoverySelfForm.id'} = ($tabRow->{'recoverySelfForm.id'} ? $tabRow->{'recoverySelfForm.id'} * 1 : undef);
            if ($flags & GETINFOS_DELIVERYSELFFORM)
            {
                $tabRow->{'deliverySelfForm.tabInfos'} = &getSelfFormInfos($countryId, $tabRow->{'deliverySelfForm.id'},
                                                                   $tabOptions->{'getDeliverySelfFormInfosFlags'},
                                                                   $tabOptions->{'getDeliverySelfFormInfosOptions'});
            }
            if ($flags & GETINFOS_RECOVERYSELFFORM)
            {
                $tabRow->{'recoverySelfForm.tabInfos'} = &getSelfFormInfos($countryId, $tabRow->{'recoverySelfForm.id'},
                                                                   $tabOptions->{'getRecoverySelfFormInfosFlags'},
                                                                   $tabOptions->{'getRecoverySelfFormInfosOptions'});
            }

            # Possibilités
            if ($flags & GETINFOS_POSSIBILITIES)
            {
                my $isEditPossible = 0;
                # Transport actif ET
                if (!$tabRow->{'isLocked'} &&
                    $tabRow->{'state.id'} eq STATE_ACTIVE &&
                        # - ni le chargement ni le déchargement ne sont faits
                        ((!$tabRow->{'loading'}->{'isDone'} && !$tabRow->{'unloading'}->{'isDone'}) ||
                        # - OU il s'agit d'un transfert avec récupération faite mais pas la livraison et
                        # on recherche les possibilités sur la livraison
                        ($tabRow->{'type'} eq TYPE_SITESTRANSFER &&
                            $tabRow->{'loading'}->{'isDone'} &&
                            !$tabRow->{'unloading'}->{'isDone'} && defined $tabFilters->{'to'})))
                {
                    $isEditPossible = 1;
                }

                $tabRow->{'possibilities'} = {
                    'edit'             => $isEditPossible,
                    'doLoading'        => $tabRow->{'loading'}->{'isDoable'},
                    'doUnloading'      => $tabRow->{'unloading'}->{'isDoable'},
                    'selfForm'         => &_isSelfFormPossible($tabRow)
                };
            }

            # Droits
            if ($flags & GETINFOS_RIGHTS)
            {
                # Informations sur l'utilisateur
                my $tabUserInfos = &LOC::Session::getUserInfos();
                if (defined $tabOptions->{'userId'} && $tabUserInfos->{'id'} != $tabOptions->{'userId'})
                {
                    $tabUserInfos = &LOC::User::getInfos($tabOptions->{'userId'});
                }
                $tabRow->{'tabRights'} = &_getRights($tabRow, $tabUserInfos);
            }
        }
    }

    return (wantarray ? %$tab : $tab);
}


# Function: getOrderInfos
# Retourne les infos d'une commande de transport
#
# Parameters:
# string  $countryId  - Pays
# int     $id         - Id de la commande
# int     $flags      - Flags (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getOrderInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETORDERINFOS_BASE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    my $tab = &getOrdersList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);
    return ($tab && exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}


# Function: getOrdersList
# Retourne la liste des commandes de transport
#
# Contenu du hashage :
# - id => identifiant de la commande de transport,
# - transport.id => identifiant du transport associé,
# - code => code de la commande de transport,
# - date => date de la commande de transport,
# - agency.id => identifiant de l'agence responsable,
# - agency.label => libellé de l'agence responsable,
# - agency.area.label => identifiant de la région de l'agence responsable,
# - agency.area.code => libellé de la région de l'agence responsable,
# - trip.id => identifiant de la tournée dans OTD,
# - trip.code => code de la tournée dans OTD,
# - trip.state.id => état de la tournée dans OTD,
# - carrierName => nom du chauffeur dans OTD,
# - sendingsCount => nombre d'envois de fichiers XML à OTD,
# - lastHash => empreinte du dernier fichier XML envoyé à OTD,
# - creationDate => date de création,
# - modificationDate => date de modification,
# - state.id => identifiant de l'état
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres (facultatif)
# int     $flags      - Flags pour le choix des colonnes (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|0 - Liste des commandes de transports
sub getOrdersList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;

    if (!defined $format)
    {
        $format = LOC::Util::GETLIST_ASSOC;
    }

    if (!defined $flags || $format != LOC::Util::GETLIST_ASSOC)
    {
        $flags = GETORDERINFOS_BASE;
    }

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }
    if (!defined $tabFilters)
    {
        $tabFilters = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);


    my @tabWheres = ();


    my $query;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    tor_id AS `id`,
    tor_tsp_id AS `transport.id`,
    tor_code AS `code`,
    tor_date AS `date`,
    tor_agc_id AS `agency.id`,
    agc_label AS `agency.label`,
    are_label AS `agency.area.label`,
    are_code AS `agency.area.code`,
    tor_sta_id_trip AS `trip.state.id`,
    tor_carriername AS `carrierName`,
    tor_sendingscount AS `sendingsCount`,
    tor_lasthash AS `lastHash`,
    tor_date_create AS `creationDate`,
    tor_date_modif AS `modificationDate`,
    tor_sta_id AS `state.id`
FROM f_transportorder
INNER JOIN frmwrk.p_agency
ON agc_id = tor_agc_id
LEFT JOIN frmwrk.p_area
ON are_id = agc_are_id';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*)
FROM f_transportorder';
    }
    else
    {
        $query = '
SELECT
    tor_id,
    tor_date
FROM f_transportorder';
    }


    # Filtres
    # - sur l'identifiant
    if (defined $tabFilters->{'id'})
    {
        push(@tabWheres, 'tor_id IN (' . &LOC::Util::join(', ', $tabFilters->{'id'}) . ')');
    }

    # - sur l'état
    if (defined $tabFilters->{'stateId'})
    {
        push(@tabWheres, 'tor_sta_id IN (' . $db->quote($tabFilters->{'stateId'}) . ')');
    }
    else
    {
        push(@tabWheres, 'tor_sta_id <> ' . $db->quote(ORDER_STATE_DELETED));
    }


    if (@tabWheres > 0)
    {
        $query .= '
WHERE ' . join("\nAND ", @tabWheres);
    }


    if (!$tabOptions->{'isSortDisabled'})
    {
        $query .= '
ORDER BY tor_date';
    }
    if (defined $tabOptions->{'limit'})
    {
        $query .= '
LIMIT ' . $tabOptions->{'limit'};
    }
    $query .= ';';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    # Formatage des champs
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}            *= 1;
            $tabRow->{'transport.id'}  *= 1;
            $tabRow->{'sendingsCount'} *= 1;

            # Modèle de machine à transporter
            if ($flags & GETORDERINFOS_TRANSPORT)
            {
                my $tabOrder = &getList($countryId, LOC::Util::GETLIST_ASSOC,
                                        {'id' => $tabRow->{'transport.id'},
                                         'stateId' => [STATE_ACTIVE, STATE_INACTIVE, STATE_DELETED, STATE_CONVERTED]},
                                        $tabOptions->{'getTransportInfosFlags'},
                                        $tabOptions->{'getTransportInfosOptions'});
                if ($tabOrder && exists $tabOrder->{$tabRow->{'transport.id'}})
                {
                    $tabRow->{'transport.tabInfos'} = $tabOrder->{$tabRow->{'transport.id'}};
                }
            }
        }
    }

    return (wantarray ? %$tab : $tab);
}


# Function: getRights
# Récupérer les droits sur un transport (par rapport à l'utilisateur)
#
# Parameters:
# string $countryId  - Pays
# string $agencyId   - Agence
# int    $id         - Id du transport
# int    $userId     - Id de l'utilisateur
#
# Returns:
# hash|hashref - Liste droits
sub getRights
{
    my ($countryId, $agencyId, $id, $userId) = @_;

    my $tabTransportInfos = {};;
    my $tabUserInfos;

    # Informations sur l'utilisateur
    $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Informations sur le transport
    if ($id)
    {
        $tabTransportInfos = &getInfos($countryId, $id);
    }

    return &_getRights($tabTransportInfos, $tabUserInfos);
}


# Function: getSelfFormInfos
# Retourne les infos d'un bordereau d'enlèvement ou de restitution
#
# Parameters:
# string  $countryId  - Pays
# int     $id         - Identifiant du bordereau
# int     $flags      - Flags (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getSelfFormInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETSELFFORMINFOS_BASE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    my $tab = &getSelfFormsList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);
    return ($tab && exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}


# Function: getSelfFormsList
# Retourne la liste des bordereaux d'enlèvements ou de restitutions
#
# Contenu du hashage :
# - id => identifiant du bordereau,
# - transport.id => identifiant du transport associé,
# - side => côté demandeur
# - date => date de génération du bordereau,
# - printsCount => nombre d'impressions du bordereau,
# - creationDate => date de création,
# - modificationDate => date de modification,
# - state.id => identifiant de l'état
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres (facultatif)
# int     $flags      - Flags pour le choix des colonnes (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|0 - Liste des bordereaux
sub getSelfFormsList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;

    if (!defined $format)
    {
        $format = LOC::Util::GETLIST_ASSOC;
    }

    if (!defined $flags || $format != LOC::Util::GETLIST_ASSOC)
    {
        $flags = GETSELFFORMINFOS_BASE;
    }

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }
    if (!defined $tabFilters)
    {
        $tabFilters = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);


    my @tabWheres = ();


    my $query;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    tsf_id AS `id`,
    tsf_tsp_id AS `transport.id`,
    tsf_side AS `side`,
    tsf_date AS `date`,
    tsf_printscount AS `printsCount`,
    tsf_date_create AS `creationDate`,
    tsf_date_modif AS `modificationDate`,
    tsf_sta_id AS `state.id`
FROM f_transportselfform';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*)
FROM f_transportselfform';
    }
    else
    {
        $query = '
SELECT
    tsf_id,
    tsf_date
FROM f_transportselfform';
    }


    # Filtres
    # - sur l'identifiant
    if (defined $tabFilters->{'id'})
    {
        push(@tabWheres, 'tsf_id IN (' . &LOC::Util::join(', ', $tabFilters->{'id'}) . ')');
    }

    # - sur le côté
    if (defined $tabFilters->{'side'})
    {
        push(@tabWheres, 'tsf_side IN (' . $db->quote($tabFilters->{'side'}) . ')');
    }

    # - sur l'état
    if (defined $tabFilters->{'stateId'})
    {
        push(@tabWheres, 'tsf_sta_id IN (' . $db->quote($tabFilters->{'stateId'}) . ')');
    }
    else
    {
        push(@tabWheres, 'tsf_sta_id <> ' . $db->quote(SELFFORM_STATE_DELETED));
    }


    if (@tabWheres > 0)
    {
        $query .= '
WHERE ' . join("\nAND ", @tabWheres);
    }


    if (!$tabOptions->{'isSortDisabled'})
    {
        $query .= '
ORDER BY tsf_date';
    }
    if (defined $tabOptions->{'limit'})
    {
        $query .= '
LIMIT ' . $tabOptions->{'limit'};
    }
    $query .= ';';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    # Formatage des champs
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}           *= 1;
            $tabRow->{'transport.id'} *= 1;
            $tabRow->{'printsCount'}  *= 1;

            # Modèle de machine à transporter
            if ($flags & GETSELFFORMINFOS_TRANSPORT)
            {
                $tabRow->{'transport.tabInfos'} = &getInfos($countryId, $tabRow->{'transport.id'},
                                                            $tabOptions->{'getTransportInfosFlags'},
                                                            $tabOptions->{'getTransportInfosOptions'});
            }
        }
    }

    return (wantarray ? %$tab : $tab);
}


# Function launchRenew
# Lance en tâche de fond la mise à jour de transports en masse
#
# Parameters
# string    $countryId     - Identifiant du pays
# arrayRef  $tabTransports - Tableau des transports à mettre à jour
# bool      $ignoreHash    - Forcer la mise à jour des commandes
# hashref   $tabUserInfos  - Informations sur l'utilisateur
sub launchRenew
{
    my ($countryId, $tabTransports, $ignoreHash, $tabUserInfos) = @_;

    # Vérification qu'aucune autre mise à jour n'est en cours
    my $tabCheckParams = {'stateId'    => [LOC::TransportRequest::STATE_CREATED,
                                           LOC::TransportRequest::STATE_PROGRESS
                                          ]
                         };
    my $nbRequest = &LOC::TransportRequest::getList($countryId, LOC::Util::GETLIST_COUNT, $tabCheckParams);
    if ($nbRequest > 0)
    {
        return 0;
    }
    my $countTransports = @$tabTransports;

    # Ajout d'une ligne en base
    my $tabParams = {'countryId'           => $countryId,
                     'userId'              => $tabUserInfos->{'id'},
                     'userNomadCountryId'  => $tabUserInfos->{'nomadCountry.id'},
                     'userNomadAgencyId'   => $tabUserInfos->{'nomadAgency.id'},
                     'tabTransportIds'     => $tabTransports,
                     'totalTransportsNo'   => $countTransports,
                     'ignoreHash'          => $ignoreHash
    };

    my $tabRequestParams = {
        'userId'              => $tabUserInfos->{'id'},
        'parameters'          => $tabParams,
        'treatedTransportsNo' => 0,
        'totalTransportsNo'   => $countTransports
    };
    my $requestId = &LOC::TransportRequest::insert($countryId, $tabRequestParams);

    if ($requestId <= 0)
    {
        return 0;
    }

    my $cgiPath         = &LOC::Globals::get('cgiPath');

    # Fichier de log
    my $logName = sprintf('launch_%s.txt', &LOC::Date::getMySQLDate());
    my $logPath = &LOC::Util::getCommonFilesPath('logs/transport/');

    # Export des variables
    my $exports = 'export REMOTE_USER="' . $tabUserInfos->{'login'} . '"' .
                  'export CONTENT_TYPE=""';

    # Placement dans le répertoire d'exécution
    my $execPath = $cgiPath . '/exec/';
    my $changeDir = 'cd "' . $execPath . '"';

    # Exécutable
    my $script1 = sprintf('perl renewTransports.pl "%s" "%s" 2>&1', $requestId, $countryId);
    my $script2 = sprintf('perl renewTransportsOutput.pl "%s" "%s"', $requestId, $countryId);
    my $scripts = $script1 . ' | ' . $script2;

    # Construction de la commande
    my $command  = '(' . $exports . ' && (' . $changeDir . ' && (' . $scripts . ')))';
    $command .= ' >> ' . $logPath . $logName;
    $command .= ' 2>&1 &';

    # Exécution
    system($command);

    return $requestId;
}



# Function: lock
# Verrouille des transports
#
# Parameters:
# string  $countryId  - Pays
# hashref $tabFilters - Filtres de sélection des transports (voir getList())
# int     $userId     - Utilisateur responsable
# hashref $tabEndUpds - Mises à jour de fin (facultatif)
#
# Returns:
# bool
sub lock
{
    my ($countryId, $tabFilters, $userId, $tabEndUpds) = @_;


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # On ne prend que les transports non verrouillés
    $tabFilters->{'isLocked'} = 0;

    # Récupération de la liste des transports concernés
    my $tabList = &getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters,
                                                                 GETINFOS_FROM | GETINFOS_TO,
                                                                 {'getFromInfosFlags' => GETFROMTOINFOS_DETAILS,
                                                                  'getToInfosFlags'   => GETFROMTOINFOS_DETAILS});


    # Pour chaque transport, on recalcule sa faisabilité et on renvoie la commande
    foreach my $tabInfos (values %$tabList)
    {
        $tabInfos->{'loading'}->{'isDoable'} = 0;
        $tabInfos->{'loading'}->{'errors'} = 0;
        $tabInfos->{'unloading'}->{'isDoable'} = 0;
        $tabInfos->{'unloading'}->{'errors'} = 0;
        $tabInfos->{'isLocked'} = 1;

        if (!&_set($db, $countryId, $tabInfos, {'isCalculateDoabilityDisabled' => 1, 'isUpdateOrderDisabled' => 1}))
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: onExternalChanges
# Exécute toutes les mises à jour nécessaires lors de la modification d'une entitée externe
#
# Parameters:
# string  $countryId  - Pays
# hashref $tabChanges - Informations sur les mises à jour des entitées externes
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub onExternalChanges
{
    my ($countryId, $tabChanges, $userId, $tabErrors, $tabEndUpds) = @_;

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my %tabToRenew  = ();

    # Impacts des clients sur les transports
    # ---------------------------------------------
    if (exists $tabChanges->{'customer'})
    {
        my $id = $tabChanges->{'customer'}->{'id'};

        my $tabProps = $tabChanges->{'customer'}->{'props'};
        if (keys %$tabProps > 0)
        {
            # Impact sur la faisabilité et/ou contenu de la commande :
            # - si l'état de validité du client a changé
            # - si l'état de vérification du client a changé
            # - si l'état de paiement avant livraison a changé
            # - si l'état d'ouverture de compte a changé
            if ($tabProps->{'validityStatus'} ||
                $tabProps->{'verificationStatus'} ||
                $tabProps->{'isNewAccount'} ||
                $tabProps->{'isProformaRequired'} ||
                $tabProps->{'name'})
            {
                # Transports liés aux contrats du client
                my $tab = &getList($countryId, LOC::Util::GETLIST_IDS,
                                   {'from-to' => {'type' => FROMTOTYPE_CONTRACT, 'customerId' => $id},
                                    'isLocked' => 0},
                                   GETINFOS_BASE, {'isSortDisabled' => 1});

                foreach my $transportId (@$tab)
                {
                    $tabToRenew{$transportId} = 1;
                }


                # Transports liés aux devis du client
                my $tab = &getList($countryId, LOC::Util::GETLIST_IDS,
                                   {'from-to' => {'type' => FROMTOTYPE_ESTIMATELINE, 'customerId' => $id},
                                    'isLocked' => 0},
                                   GETINFOS_BASE, {'isSortDisabled' => 1});

                foreach my $transportId (@$tab)
                {
                    $tabToRenew{$transportId} = 1;
                }
            }
        }
    }

    # Impacts des chantiers sur les transports
    # ---------------------------------------------
    if (exists $tabChanges->{'site'})
    {
        my $id = $tabChanges->{'site'}->{'id'};

        my $tabProps = $tabChanges->{'site'}->{'props'};
        if (keys %$tabProps > 0)
        {
            # Impact sur la faisabilité et/ou contenu de la commande :
            # Transports liés aux contrats du chantier
            my $tab = &getList($countryId, LOC::Util::GETLIST_IDS,
                               {'from-to' => {'type' => FROMTOTYPE_CONTRACT, 'siteId' => $id},
                                'isLocked' => 0},
                               GETINFOS_BASE, {'isSortDisabled' => 1});

            foreach my $transportId (@$tab)
            {
                $tabToRenew{$transportId} = 1;
            }


            # Transports liés aux lignes de devis du chantier
            my $tab = &getList($countryId, LOC::Util::GETLIST_IDS,
                               {'from-to' => {'type' => FROMTOTYPE_ESTIMATELINE, 'siteId' => $id},
                                'isLocked' => 0},
                               GETINFOS_BASE, {'isSortDisabled' => 1});

            foreach my $transportId (@$tab)
            {
                $tabToRenew{$transportId} = 1;
            }
        }
    }

    # Impacts des contrats sur les transports
    # ---------------------------------------------
    if (exists $tabChanges->{'contract'})
    {
        my $id = $tabChanges->{'contract'}->{'id'};

        my $tabProps = $tabChanges->{'contract'}->{'props'};
        if (keys %$tabProps > 0)
        {
            # Impact sur la faisabilité et/ou contenu de la commande :
            # - si les flags pro forma ont changé
            # - si le client d'origine a été supprimé et remplacé par un autre
            # - si l'état de facturation passe de vide à non vide ou inversement
            # - informations commande ont changé (numéro de commande, contact commande)
            if ($tabProps->{'proformaFlags'} || $tabProps->{'customer.id'} ||
                ($tabProps->{'invoiceState'} && ($tabProps->{'invoiceState'}->{'old'} eq '' ||
                                                 $tabProps->{'invoiceState'}->{'new'} eq '')) ||
                $tabProps->{'orderNo'} ||
                $tabProps->{'orderContact.fullName'} ||
                $tabProps->{'orderContact.telephone'})
            {
                my $tab = &getList($countryId, LOC::Util::GETLIST_IDS,
                                   {'from-to' => {'type' => FROMTOTYPE_CONTRACT, 'id' => $id},
                                    'isLocked' => 0},
                                   GETINFOS_BASE, {'isSortDisabled' => 1});

                foreach my $transportId (@$tab)
                {
                    $tabToRenew{$transportId} = 1;
                }
            }
        }
    }

    # Impacts des devis sur les transports
    # ---------------------------------------------
    if (exists $tabChanges->{'estimate'})
    {
        my $id = $tabChanges->{'estimate'}->{'id'};

        my $tabProps = $tabChanges->{'estimate'}->{'props'};
        if (keys %$tabProps > 0)
        {
            # Impact sur la faisabilité et/ou contenu de la commande :
            # - si les informations commande (contact commande et numéro) ont changé
            if ($tabProps->{'orderNo'} ||
                $tabProps->{'orderContact.fullName'} ||
                $tabProps->{'orderContact.telephone'})
            {
                my $tab = &getList($countryId, LOC::Util::GETLIST_IDS,
                                   {'from-to' => {'type' => FROMTOTYPE_ESTIMATELINE, 'estimateId' => $id},
                                    'isLocked' => 0},
                                   GETINFOS_BASE, {'isSortDisabled' => 1});

                foreach my $transportId (@$tab)
                {
                    $tabToRenew{$transportId} = 1;
                }
            }
        }
    }


    # Impacts des lignes de devis sur les transports
    # ---------------------------------------------
    if (exists $tabChanges->{'estimateLine'})
    {
        my $id = $tabChanges->{'estimateLine'}->{'id'};

        my $tabProps = $tabChanges->{'estimateLine'}->{'props'};
        if (keys %$tabProps > 0)
        {
            # Impact sur la faisabilité et/ou contenu de la commande :
            # - si les flags pro forma ont changé
            # - si le client d'origine a été supprimé et remplacé par un autre
            if ($tabProps->{'proformaFlags'} || ($tabProps->{'customer.id'}))
            {
                my $tab = &getList($countryId, LOC::Util::GETLIST_IDS,
                                   {'from-to' => {'type' => FROMTOTYPE_ESTIMATELINE, 'id' => $id},
                                    'isLocked' => 0},
                                   GETINFOS_BASE, {'isSortDisabled' => 1});

                foreach my $transportId (@$tab)
                {
                    $tabToRenew{$transportId} = 1;
                }
            }
        }
    }


    # Impacts des machines sur les transports
    # ---------------------------------------------
    if (exists $tabChanges->{'machine'})
    {
        my $id = $tabChanges->{'machine'}->{'id'};

        my $tabProps = $tabChanges->{'machine'}->{'props'};
        if (keys %$tabProps > 0)
        {
            # Impact sur la faisabilité et/ou contenu de la commande :
            # - si l'état de la machine a changé
            # - si l'agence de la machine a changé
            # - si le blocage de la machine a changé
            # - si le contrat sur la machine a changé
            # - si la visibilité de la machine a changé
            if ($tabProps->{'state.id'} || $tabProps->{'agency.id'} || $tabProps->{'isSiteBlocked'} ||
                $tabProps->{'contract.id'} || $tabProps->{'visibilityMode'})
            {
                my $tab = &getList($countryId, LOC::Util::GETLIST_IDS,
                                   {'machineId' => $id,
                                    'isLocked' => 0},
                                   GETINFOS_BASE, {'isSortDisabled' => 1});

                foreach my $transportId (@$tab)
                {
                    $tabToRenew{$transportId} = 1;
                }
            }
        }
    }


    # Renouvellement des transports
    foreach my $transportId (keys %tabToRenew)
    {
        $db->addTransactionAction('commit.pre', sub {
                return &renew($countryId, {'id' => $transportId});
            },
            {'category' => 'transport',
             'priority' => -1000,
             'id' => 'LOC::Transport::renew_' . $countryId . '-id:' . $transportId}
        );
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: renew
# Recalcule la faisabilité des commandes de transport
#
# Parameters:
# string  $countryId  - Pays
# hashref $tabFilters - Filtres de sélection des transports (voir getList())
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# bool
sub renew
{
    my ($countryId, $tabFilters, $tabOptions) = @_;

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    if (ref($tabFilters) ne 'HASH')
    {
        $tabFilters = {'id' => $tabFilters};
    }



    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }


    # Etats à récupérer
    $tabFilters->{'stateId'} = [STATE_ACTIVE];
    # Non bloqués
    $tabFilters->{'isLocked'} = 0;

    # Récupération de la liste des transports concernés
    my $tabList = &getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters,
                           GETINFOS_FROM | GETINFOS_TO | GETINFOS_MACHINE,
                           {'getFromInfosFlags' => GETFROMTOINFOS_DETAILS,
                            'getToInfosFlags'   => GETFROMTOINFOS_DETAILS});

    # Pour chaque transport, on recalcule sa faisabilité et on renvoie la commande
    foreach my $tabInfos (values %$tabList)
    {
        if (!&_set($db, $countryId, $tabInfos, $tabOptions))
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    return 1;
}


# Function: sendEmail
# Envoi mail concernant une action transport
#
# Parameters:
# string       $countryId  - Pays
# string       $type       - Type de mail ('agenciesTransfer', 'sitesTransfer')
# hashref      $tabData    - Données nécessaires au formatage du mail
# int          $userId     - Utilisateur responsable
#
# Returns:
# bool
sub sendEmail
{
    my ($countryId, $type, $tabData, $userId) = @_;

    # Informations sur l'utilisateur (avec optimisation)
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Informations de l'agence de la personne connnectée
    my $tabAgencyInfos = &LOC::Agency::getInfos($tabUserInfos->{'nomadAgency.id'});

    # Création d'un mail
    my $mailObj = LOC::Mail->new();

    # Encodage
    $mailObj->setEncodingType(LOC::Mail::ENCODING_TYPE_UTF8);

    # Expéditeur
    my %tabSender = ($tabUserInfos->{'email'} => $tabUserInfos->{'firstName'} . ' ' . $tabUserInfos->{'name'});
    $mailObj->setSender(%tabSender);

    # Corps
    my $file = &LOC::Globals::get('cgiPath') . '/cfg/templates/' . $countryId . '/' . $type . '.html';
    open(TPL, $file);
    my $template = join('', <TPL>);
    close(TPL);

    # Recherche des données pour le template
    my $mailData = {};
    if ($type eq 'agenciesTransfer')
    {
        my $tabOldInfos = $tabData->{'tabOldInfos'};
        my $tabNewInfos = $tabData->{'tabNewInfos'};
        my $tabModif    = $tabData->{'tabModif'};
        $mailData = &_getDataForMailAgenciesTransfer($countryId, $tabOldInfos, $tabNewInfos, $tabModif);
    }
    elsif ($type eq 'sitesTransfer')
    {
        $mailData = &_getDataForMailSitesTransfer($countryId, $tabData->{'id'});
    }

    my $tabReplaces = $mailData->{'tabReplaces'};
    $tabReplaces->{'user.firstName'}    = $tabUserInfos->{'firstName'};
    $tabReplaces->{'user.name'}         = $tabUserInfos->{'name'};
    $tabReplaces->{'agency.label'}      = $tabAgencyInfos->{'label'};
    $tabReplaces->{'agency.telephone'}  = $tabAgencyInfos->{'telephone'};
    $tabReplaces->{'agency.fax'}        = $tabAgencyInfos->{'fax'};
    $tabReplaces->{'agency.address'}    = $tabAgencyInfos->{'address'};
    $tabReplaces->{'agency.postalCode'} = $tabAgencyInfos->{'postalCode'};
    $tabReplaces->{'agency.city'}       = $tabAgencyInfos->{'city'};

    # Formatage et remplacement des données dans le template
    $template =~ s/(\<%([a-z]{3})\:(.*?)\>)/@{[&_formatTemplate($2, $3, $tabReplaces)]}/g;

    while ($template =~ m/\<\%(.*?)\>/)
    {
        my $replace = '';
        if (exists $tabReplaces->{$1})
        {
            $replace = $tabReplaces->{$1};
        }
        $template =~ s/<%$1>/$replace/g;
    }

    # Formatage et remplacement des données dans le sujet
    my $subject = ($template =~ m/\<title\>(.*?)\<\/title\>/ ? $1 : '');

    if ($template =~ /"cid:(.+)"/ && -e &LOC::Globals::get('imgPath') . '/print/gui/' . $1)
    {
        $mailObj->addInlineAttachment(&LOC::Globals::get('imgUrl') . '/print/gui/' . $1, $1);
    }

    $mailObj->setBodyType(LOC::Mail::BODY_TYPE_HTML);

    my %tabRecipients = %{$mailData->{'tabRecipients'}};
    my %tabRecipient;
    my $templatePlus = '';

    if (&LOC::Globals::getEnv() ne 'exp')
    {
        $subject = '[TEST] ' . $subject;

        # Informations sur l'utilisateur
        my $tabCurrentUserInfos = &LOC::Session::getUserInfos();

        if (defined $tabCurrentUserInfos)
        {
            %tabRecipient = ($tabCurrentUserInfos->{'email'} => $tabCurrentUserInfos->{'firstName'} . ' ' . $tabCurrentUserInfos->{'name'});
        }
        else
        {
            # En cas !
            %tabRecipient = ('archive.dev@acces-industrie.com' => 'Développeurs');
        }

        # Destinataires d'origines
        my $recipients = '';
        foreach my $email (keys %tabRecipients)
        {
            $recipients .= $tabRecipients{$email} . ' &lt;' . $email . '&gt;<br />';
        }


        $templatePlus  = 'Destinataire d\'origine : <br/>' . $tabUserInfos->{'firstName'} . ' ' . $tabUserInfos->{'name'} . ' &lt;' . $tabUserInfos->{'email'} . '&gt;<br />';
        $templatePlus .= $recipients;
    }
    else
    {
        %tabRecipient = %tabRecipients;
        $tabRecipient{$tabUserInfos->{'email'}} = $tabUserInfos->{'firstName'} . ' ' . $tabUserInfos->{'name'};
    }

    $mailObj->setBody($templatePlus . $template);

    $mailObj->addRecipients(%tabRecipient);

    $mailObj->setSubject($subject);

    $mailObj->send();

    return 1;
}


# Function: sendOrder
# Envoi une commande de transport à OTD
#
# Parameters:
# string       $countryId  - Pays
# int|arrayref $orderId    - Id de la commande (ou des commandes)
# hashref      $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# bool
sub sendOrder
{
    my ($countryId, $orderId, $tabOptions) = @_;

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Vérification de la gestion des commandes
    my $isOrderManageActive = &LOC::Characteristic::getCountryValueByCode('TRSPGENCMD', $countryId);
    if (!$isOrderManageActive)
    {
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);


    # Récupération des informations de la commande avec ses informations transport
    my $tabOrdersList = &getOrdersList($countryId,
                                       LOC::Util::GETLIST_ASSOC,
                                       {'id'      => $orderId,
                                        'stateId' => [ORDER_STATE_ACTIVE, ORDER_STATE_CLOSED, ORDER_STATE_DELETED]},
                                       GETORDERINFOS_TRANSPORT,
                                       {'getTransportInfosFlags' => GETINFOS_MODEL |
                                                                    GETINFOS_MACHINE |
                                                                    GETINFOS_TO |
                                                                    GETINFOS_FROM});
    if (!$tabOrdersList)
    {
        return 0;
    }

    foreach my $tabOrderInfos (values %$tabOrdersList)
    {
        my $tabTransportInfos = $tabOrderInfos->{'transport.tabInfos'};

        # Paramètres d'envois de la commande
        my $tabSendOptions = {
            'sendingsCount' => $tabOrderInfos->{'sendingsCount'},
            'lastHash'      => ($tabOptions->{'ignoreHash'} ? '' : $tabOrderInfos->{'lastHash'})
        };

        # Doit-on envoyer les commandes une dernière fois ?
        my $isSynchroDisabled = $tabTransportInfos->{'isSynchroDisabled'};
        if ($isSynchroDisabled && $tabOptions->{'lastSending'})
        {
            $isSynchroDisabled = 0;
            # Force la faisabilité dans la trame envoyée à OTD
            $tabTransportInfos->{'loading'}->{'isDoable'}   = 1;
            $tabTransportInfos->{'unloading'}->{'isDoable'} = 1;
        }


        if (!$isSynchroDisabled)
        {
            if ($tabOrderInfos->{'state.id'} ne ORDER_STATE_DELETED)
            {
                # Envoi de la commande à OTD
                &_sendSetOrder($countryId, $tabOrderInfos, $tabSendOptions);
            }
            else
            {
                # Envoi de la suppression à OTD
                &LOC::Otd::deleteOrder($countryId, $tabOrderInfos->{'code'}, $tabSendOptions);
            }

            if ($tabSendOptions->{'sendingsCount'} != $tabOrderInfos->{'sendingsCount'})
            {
                # Mise à jour du nombre d'envois
                my $tabUpdates = {
                    'tor_sendingscount' => $tabSendOptions->{'sendingsCount'},
                    'tor_lasthash'      => $tabSendOptions->{'lastHash'}
                };

                $db->update('f_transportorder', $tabUpdates, {'tor_id*' => $tabOrderInfos->{'id'}}, $schemaName);
            }
        }
    }

    return 1;
}


# Function: setDocumentDelivery
# Définit le transport de livraison pour un document (ligne de devis ou contrat)
#
# Parameters:
# string  $countryId    - Identifiant du pays
# string  $documentType - Type de document ('estimateLine': ligne de devis, 'contract' : contrat)
# int     $documentId   - Id du document
# hashref $tabData      - Données sur la livraison
# int     $userId       - Utilisateur responsable
# hashref $tabOptions   - Options supplémentaires
# hashref $tabErrors    - Tableau des erreurs
# hashref $tabEndUpds   - Mises à jour de fin (facultatif)
#
# Returns:
# bool
sub setDocumentDelivery
{
    my ($countryId, $documentType, $documentId, $tabData, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    my $result;

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }


    # Vérification des données en entrée
    if ($documentType ne 'contract' && $documentType ne 'estimateLine')
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'setDocumentDelivery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }


    # Récupération des données du document
    my $tabTo = {
        'type' => ($documentType eq 'contract' ? FROMTOTYPE_CONTRACT : FROMTOTYPE_ESTIMATELINE),
        'id'   => $documentId
    };
    $tabTo->{'tabInfos'} = &_getFromToEntityInfos($countryId, $tabTo->{'type'}, $tabTo->{'id'},
                                                  SIDE_DELIVERY, GETFROMTOINFOS_DETAILS, {'lockEntityDbRow' => 1});
    if (!$tabTo->{'tabInfos'})
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'setDocumentDelivery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Formatage des données en entrée
    my $stateId           = STATE_ACTIVE;
    my $isSelf            = ($tabData->{'isSelf'} ? 1 : 0);
    my $delegatedAgencyId = ($tabData->{'delegatedAgency.id'} ne '' ? $tabData->{'delegatedAgency.id'} : undef);
    my $contractId        = ($documentType eq 'contract' && $tabData->{'contract.id'} ? $tabData->{'contract.id'} * 1 : undef);
    my $isSynchroDisabled = ($tabData->{'isSynchroDisabled'} && !$isSelf ? 1 : 0);


    # Actions sur les contrats
    my @tabActions = ();
    my $isLastSendingActived = 0;

    # -------------------------------
    # LIVRAISON
    # -------------------------------

    # Recherche d'une livraison existante
    my $tabOldInfos = &getDocumentDeliveryInfos($countryId, $documentType, $documentId,
                                                GETINFOS_FROM | GETINFOS_POSSIBILITIES,
                                                {'lockEntityDbRow' => 1,
                                                 'getFromInfosFlags' => GETFROMTOINFOS_DETAILS});

    my $isEstimateLineToContract = 0;
    # Cas du passage d'une ligne de devis en contrat
    if ($tabOldInfos)
    {
        # Ajout des données du point d'arrivé
        $tabOldInfos->{'to.tabInfos'} = &LOC::Util::clone($tabTo->{'tabInfos'});
    }
    elsif ($documentType eq 'contract' && $tabTo->{'tabInfos'}->{'tabDetails'}->{'linkedEstimateLine.id'})
    {
        # Récupération des informations de la livraison de la ligne de devis
        $tabOldInfos = &getDocumentDeliveryInfos($countryId, 'estimateLine',
                                                 $tabTo->{'tabInfos'}->{'tabDetails'}->{'linkedEstimateLine.id'},
                                                 GETINFOS_FROM | GETINFOS_TO | GETINFOS_POSSIBILITIES,
                                                 {'lockEntityDbRow'   => 1,
                                                  'getFromInfosFlags' => GETFROMTOINFOS_DETAILS});

        # On passe la livraison du devis en "convertie"
        my $tabNewInfos = &LOC::Util::clone($tabOldInfos);
        $tabNewInfos->{'state.id'} = STATE_CONVERTED;

        # Mise à jour des alertes
        &_updateAlerts($tabOldInfos, $tabNewInfos, $tabOptions->{'tabDocumentChanges'}, $tabEndUpds);

        # Ajout de la mise à jour de suppression
        push(@tabActions, {
            'type'    => 'set',
            'data'    => $tabNewInfos,
            'options' => {
                'lastSending' => 0
            }
        });

        if ($tabOldInfos)
        {
            $tabOldInfos->{'id'} = undef;
        }

        $isEstimateLineToContract = 1;
    }

    # Si c'est une modification et qu'il est bloqué alors on ne fait rien
    if ($tabOldInfos && $tabOldInfos->{'isLocked'})
    {
        return 1;
    }

    if ($tabOldInfos)
    {
        # Données par défaut
        $stateId = $tabOldInfos->{'state.id'};
        if (!exists $tabData->{'isSelf'})
        {
            $isSelf = $tabOldInfos->{'isSelf'};
        }
        if (!exists $tabData->{'delegatedAgency.id'})
        {
            $delegatedAgencyId = $tabOldInfos->{'delegatedAgency.id'};
        }
        if (!exists $tabData->{'contract.id'})
        {
            $contractId = ($tabOldInfos->{'from.type'} eq FROMTOTYPE_CONTRACT ? $tabOldInfos->{'from.id'} : undef);
        }
        if (!exists $tabData->{'isSynchroDisabled'})
        {
            $isSynchroDisabled = $tabOldInfos->{'isSynchroDisabled'};
        }
    }

    # On calcule l'état du transport par rapport aux informations sur le document
    if ($stateId eq STATE_ACTIVE && (
          ($tabTo->{'type'} eq FROMTOTYPE_CONTRACT &&
           $tabTo->{'tabInfos'}->{'tabDetails'}->{'state.id'} eq LOC::Contract::Rent::STATE_CANCELED) ||
          ($tabTo->{'type'} eq FROMTOTYPE_ESTIMATELINE &&
           ($tabTo->{'tabInfos'}->{'tabDetails'}->{'estimate.state.id'} eq LOC::Estimate::Rent::STATE_ABORTED ||
            $tabTo->{'tabInfos'}->{'tabDetails'}->{'state.id'} ne LOC::Estimate::Rent::LINE_STATE_ACTIVE))))
    {
        $stateId = STATE_INACTIVE;
        $contractId = undef;
    }
    elsif ($stateId eq STATE_INACTIVE && (
              ($tabTo->{'type'} eq FROMTOTYPE_CONTRACT &&
               $tabTo->{'tabInfos'}->{'tabDetails'}->{'state.id'} ne LOC::Contract::Rent::STATE_CANCELED) ||
              ($tabTo->{'type'} eq FROMTOTYPE_ESTIMATELINE &&
               $tabTo->{'tabInfos'}->{'tabDetails'}->{'estimate.state.id'} ne LOC::Estimate::Rent::STATE_ABORTED &&
               $tabTo->{'tabInfos'}->{'tabDetails'}->{'state.id'} eq LOC::Estimate::Rent::LINE_STATE_ACTIVE)))
    {
        $stateId = STATE_ACTIVE;
    }


    # Récupération des données de l'entité de départ
    my $tabFrom;
    if (!$contractId)
    {
        # La provenance est une agence
        $tabFrom = {
            'type' => FROMTOTYPE_AGENCY,
            'id'   => $tabTo->{'tabInfos'}->{'agency.id'}
        };

        # Si on est en modification et que la livraison est réalisée, on récupère le point de départ précédent
        if ($tabOldInfos && $tabOldInfos->{'unloading'}->{'isDone'})
        {
            $tabFrom->{'id'} = $tabOldInfos->{'from.id'};
        }
        # Sinon, dans le cas où il y a une machine attribuée et qu'elle n'est pas en transfert, le point de départ
        # est l'agence physique de la machine
        elsif ($tabTo->{'tabInfos'}->{'machine.id'} &&
               $tabTo->{'tabInfos'}->{'machine.tabInfos'}->{'state.id'} ne LOC::Machine::STATE_TRANSFER)
        {
            $tabFrom->{'id'} = $tabTo->{'tabInfos'}->{'machine.tabInfos'}->{'agency.id'};
        }
    }
    else
    {
        # La provenance est un contrat
        $tabFrom = {
            'type' => FROMTOTYPE_CONTRACT,
            'id'   => $contractId
        };
    }
    $tabFrom->{'tabInfos'} = &_getFromToEntityInfos($countryId, $tabFrom->{'type'}, $tabFrom->{'id'},
                                                    SIDE_RECOVERY, GETFROMTOINFOS_DETAILS, {'lockEntityDbRow' => 1});
    if (!$tabFrom->{'tabInfos'})
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'setDocumentDelivery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Mise à jour principale
    my $tabNewInfos = {
        'isSelf'              => $isSelf,
        'agency.id'           => $tabTo->{'tabInfos'}->{'agency.id'},
        'delegatedAgency.id'  => $delegatedAgencyId,
        'transferToAgency.id' => undef,
        'model.id'            => $tabTo->{'tabInfos'}->{'model.id'},
        'machine.id'          => $tabTo->{'tabInfos'}->{'machine.id'},
        'from.type'           => $tabFrom->{'type'},
        'from.id'             => $tabFrom->{'id'},
        'from.tabInfos'       => $tabFrom->{'tabInfos'},
        'to.type'             => $tabTo->{'type'},
        'to.id'               => $tabTo->{'id'},
        'to.tabInfos'         => $tabTo->{'tabInfos'},
        'loading'             => {
            'isDone'   => 0,
            'isDoable' => 0,
            'errors'   => 0,
            'date'     => undef
        },
        'unloading'           => {
            'isDone'   => 0,
            'isDoable' => 0,
            'errors'   => 0,
            'date'     => undef
        },
        'order.id'            => undef,
        'deliverySelfForm.id' => undef,
        'recoverySelfForm.id' => undef,
        'state.id'            => $stateId,
        'isLocked'            => 0,
        'isSynchroDisabled'   => $isSynchroDisabled,
        'isConsidered'        => CONSIDERTYPE_NONE,
        'consideredUser.id'   => undef,
        'consideredDate'      => undef,
        'valueDate'           => &LOC::Date::getMySQLDate()
    };

    # Vérification des cas d'incohérence :
    # - incohérence type du point de départ et id du point de départ
    # (enlèvement en agence avec un id de contrat, reprise sur chantier avec un id d'agence,
    #  transfert inter-chantiers avec un id autre que celui d'un contrat, livraison standard avec un id de contrat)
    # - pas d'agence déléguée dans le cas de l'enlèvement
    if ($tabNewInfos->{'from.type'} eq FROMTOTYPE_CONTRACT && !LOC::Util::isNumeric($tabNewInfos->{'from.id'}) ||
        $tabNewInfos->{'from.type'} eq FROMTOTYPE_AGENCY && LOC::Util::isNumeric($tabNewInfos->{'from.id'}) ||
        $tabNewInfos->{'isSelf'} && $tabNewInfos->{'delegatedAgency.id'} ||
        $tabNewInfos->{'agency.id'} eq $tabNewInfos->{'delegatedAgency.id'})
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    my $isSendMailForSitesTransfer = 0;

    # Calcul des modifications à faire
    if ($tabFrom->{'type'} eq FROMTOTYPE_AGENCY)
    {
        # On veut faire une livraison standard
        # ------------------------------------
        $tabNewInfos->{'type'} = TYPE_DELIVERY;

        # Récupération des informations de la livraison d'origine si elle existe
        if ($tabOldInfos)
        {
            $tabNewInfos->{'loading'}->{'isDone'}   = $tabOldInfos->{'loading'}->{'isDone'};
            $tabNewInfos->{'loading'}->{'date'}     = $tabOldInfos->{'loading'}->{'date'};
            $tabNewInfos->{'unloading'}->{'isDone'} = $tabOldInfos->{'unloading'}->{'isDone'};
            $tabNewInfos->{'unloading'}->{'date'}   = $tabOldInfos->{'unloading'}->{'date'};

            if ($tabOldInfos->{'type'} eq TYPE_DELIVERY)
            {
                $tabNewInfos->{'id'}          = $tabOldInfos->{'id'};
                $tabNewInfos->{'order.id'}    = $tabOldInfos->{'order.id'};
                $tabNewInfos->{'deliverySelfForm.id'} = $tabOldInfos->{'deliverySelfForm.id'};
                $tabNewInfos->{'recoverySelfForm.id'} = $tabOldInfos->{'recoverySelfForm.id'};
                $tabNewInfos->{'valueDate'}   = $tabOldInfos->{'valueDate'};


                # Mise à jour des alertes
                &_updateAlerts($tabOldInfos, $tabNewInfos, $tabOptions->{'tabDocumentChanges'}, $tabEndUpds);


                # Mise à jour des traces
                # - Modification de la livraison
                if (!$isEstimateLineToContract)
                {
                    &_addTraces($countryId, SIDE_DELIVERY, $tabOldInfos, $tabNewInfos, $tabEndUpds);
                }
            }
            # Si la livraison était un transfert
            elsif ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER)
            {
                # On désattribue la machine
                $tabNewInfos->{'machine.id'} = undef;

                # Il faudra transformer le transfert d'origine en récupération standard
                my $tabNew2Infos = &LOC::Util::clone($tabOldInfos); # -> on reprend les données du transfert
                $tabNew2Infos->{'type'}        = TYPE_RECOVERY;
                $tabNew2Infos->{'to.type'}     = FROMTOTYPE_AGENCY;
                $tabNew2Infos->{'to.id'}       = ($tabNew2Infos->{'transferToAgency.id'} ?
                                                        $tabNew2Infos->{'transferToAgency.id'} :
                                                        $tabNew2Infos->{'from.tabInfos'}->{'agency.id'});
                $tabNew2Infos->{'to.tabInfos'} = &_getFromToEntityInfos($countryId,
                                                                        $tabNew2Infos->{'to.type'},
                                                                        $tabNew2Infos->{'to.id'},
                                                                        SIDE_DELIVERY,
                                                                        GETFROMTOINFOS_DETAILS);
                $tabNew2Infos->{'agency.id'}   = $tabNew2Infos->{'to.id'};
                $tabNew2Infos->{'valueDate'}   = &LOC::Date::getMySQLDate();
                if (!$isSynchroDisabled)
                {
                    $tabNew2Infos->{'isSynchroDisabled'} = 0;
                }

                # si le transfert d'origine était réalisé côté récupération :
                # - mise à jour des informations nécessaires pour le calcul de la faisabilité
                if ($tabOldInfos->{'loading'}->{'isDone'})
                {
                    $tabNew2Infos->{'unloading'}->{'isDone'} = 1;
                    $tabNew2Infos->{'unloading'}->{'date'}   = $tabOldInfos->{'loading'}->{'date'};
                    $tabNew2Infos->{'from.tabInfos'}->{'machine.tabInfos'}->{'state.id'} = LOC::Machine::STATE_CHECK;
                }

                # si le transfert d'origine était pris en compte
                # - remise à 0 des informations de prise en compte
                if ($tabOldInfos->{'isConsidered'} != CONSIDERTYPE_NONE)
                {
                    $tabNew2Infos->{'isConsidered'}      = CONSIDERTYPE_NONE;
                    $tabNew2Infos->{'consideredUser.id'} = undef;
                    $tabNew2Infos->{'consideredDate'}    = undef;
                }

                # Mise à jour des alertes
                &_updateAlerts($tabOldInfos, $tabNew2Infos, $tabOptions->{'tabDocumentChanges'}, $tabEndUpds);
                &_updateAlerts(undef, $tabNewInfos, {}, $tabEndUpds);


                # Mise à jour des traces
                # - Modification de la livraison du document
                &_addTraces($countryId, SIDE_DELIVERY, $tabOldInfos, $tabNewInfos, $tabEndUpds);
                # - Modification de la récupération du contrat qui était lié par transfert
                &_addTraces($countryId, SIDE_RECOVERY, $tabOldInfos, $tabNew2Infos, $tabEndUpds);


                # Ajout de la mise à jour secondaire
                push(@tabActions, {
                    'type'    => 'set',
                    'data'    => $tabNew2Infos,
                    'options' => {
                        'lastSending' => 0
                    }
                });

                # Dans le cas d'une ancienne reprise sur chantier,
                # il faut recalculer le montant pour le nouveau transport
                # On modifie les informations de tarification transport du contrat de départ
                my $tabTrspTariffUpds = {
                    'isTransferRecovery' => 0
                };

                push(@tabActions, {
                    'type' => 'updateContract',
                    'id'   => $tabNew2Infos->{'from.id'},
                    'data' => {
                        'tabTransport' => {
                            'tabTariff' => $tabTrspTariffUpds
                        }
                    }
                });

                # Désassocier ou désattribuer la machine
                if ($tabNewInfos->{'to.tabInfos'}->{'machine.id'})
                {
                    $tabNewInfos->{'to.tabInfos'}->{'machine.id'} = undef;

                    push(@tabActions, {
                        'type' => 'updateContract',
                        'id'   => $tabNewInfos->{'to.id'},
                        'data' => {
                            'machine.id' => undef
                        }
                    });
                }

                # si le transfert d'origine était réalisé côté récupération
                if ($tabOldInfos->{'loading'}->{'isDone'})
                {
                    # - mise à jour de l'état de la machine, elle passe en vérification
                    push(@tabActions, {
                        'type' => 'updateMachine',
                        'id'   => $tabOldInfos->{'from.tabInfos'}->{'machine.id'},
                        'data' => {
                            'state.id' => LOC::Machine::STATE_CHECK
                        }
                    });
                }
            }

            # Si la désynchro OTD est activée et qu'elle n'était pas activée sur l'ancienne livraison (ou transfert),
            # alors on envoie une dernière fois la commande
            $isLastSendingActived = ($isSynchroDisabled && !$tabOldInfos->{'isSynchroDisabled'} ? 1 : 0);
        }
        else
        {
            # Mise à jour des alertes
            &_updateAlerts(undef, $tabNewInfos, {}, $tabEndUpds);

            $isLastSendingActived = $isSynchroDisabled;
        }
    }
    else
    {
        # On veut faire un transfert inter-chantiers
        # ------------------------------------------
        $tabNewInfos->{'type'} = TYPE_SITESTRANSFER;

        # Un transfert sans machine attribuée sur le contrat de provenance est impossible !
        if (!$tabFrom->{'tabInfos'}->{'isFinalAllocation'})
        {
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'setDocumentDelivery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }

        # Vérification du contrat pour le transfert
        my $oldContractId = ($tabOldInfos && $tabOldInfos->{'from.type'} eq FROMTOTYPE_CONTRACT ? $tabOldInfos->{'from.id'} : undef);

        # Modèle de machines : celui de la machine attribuée ou, à défaut, celui demande sur le contrat
        my $modelId = $tabTo->{'tabInfos'}->{'model.id'};
        if ($tabTo->{'tabInfos'}->{'machine.tabInfos'})
        {
            $modelId = $tabTo->{'tabInfos'}->{'machine.tabInfos'}->{'model.id'};
        }

        # Si le contrat de départ n'a pas changé alors on ne fait pas de vérification
        if ($oldContractId != $tabFrom->{'id'} &&
            &getContractsForSitesTransfer($countryId, $tabTo->{'tabInfos'}->{'agency.id'}, $modelId, SIDE_DELIVERY,
                                          {'isAnticipatable' => $tabTo->{'tabInfos'}->{'isAnticipatable'},
                                           'referenceDate'   => $tabTo->{'tabInfos'}->{'beginDate'},
                                           'referenceHour'   => substr($tabTo->{'tabInfos'}->{'window'}->{'till'}, 11, 5)
                                          }, LOC::Util::GETLIST_COUNT, {'id' => $tabFrom->{'id'}, 'noFatalError' => 1}, GETSTCTTINFOS_BASE,
                                          {'isSelf' => $tabNewInfos->{'isSelf'}}) == 0)
        {
            # Contrat incorrect pour le transfert inter-chantiers
            push(@{$tabErrors->{'fatal'}}, 0x0100);
            return 0;
        }

        # L'entité de départ est un contrat donc on a besoin de connaître les informations sur sa récupération
        $tabFrom->{'tabOldRecoveryInfos'} = &getDocumentRecoveryInfos($countryId, 'contract', $tabFrom->{'id'}, GETINFOS_BASE);
        if (!$tabFrom->{'tabOldRecoveryInfos'})
        {
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'setDocumentDelivery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
        # On rempli les informations nécessaires aux prochains calculs
        $tabFrom->{'tabOldRecoveryInfos'}->{'from.tabInfos'} = $tabFrom->{'tabInfos'};
        if ($tabOldInfos && $tabOldInfos->{'id'} == $tabFrom->{'tabOldRecoveryInfos'}->{'id'})
        {
            # Si c'est le même transfert alors on récupère les informations au lieu de les recharger
            $tabFrom->{'tabOldRecoveryInfos'}->{'to.tabInfos'} = $tabOldInfos->{'to.tabInfos'};
        }
        else
        {
            $tabFrom->{'tabOldRecoveryInfos'}->{'to.tabInfos'} = &_getFromToEntityInfos(
                                                                    $countryId,
                                                                    $tabFrom->{'tabOldRecoveryInfos'}->{'to.type'},
                                                                    $tabFrom->{'tabOldRecoveryInfos'}->{'to.id'},
                                                                    SIDE_DELIVERY,
                                                                    GETFROMTOINFOS_DETAILS);
        }


        # Machine de la provenance
        $tabNewInfos->{'machine.id'} = $tabFrom->{'tabInfos'}->{'machine.id'};

        # On récupère certaines données de la récupération du contrat de provenance
        $tabNewInfos->{'id'}                  = $tabFrom->{'tabOldRecoveryInfos'}->{'id'};
        $tabNewInfos->{'transferToAgency.id'} = $tabFrom->{'tabOldRecoveryInfos'}->{'transferToAgency.id'};
        $tabNewInfos->{'order.id'}            = $tabFrom->{'tabOldRecoveryInfos'}->{'order.id'};
        $tabNewInfos->{'deliverySelfForm.id'} = $tabFrom->{'tabOldRecoveryInfos'}->{'deliverySelfForm.id'};
        $tabNewInfos->{'recoverySelfForm.id'} = $tabFrom->{'tabOldRecoveryInfos'}->{'recoverySelfForm.id'};
        $tabNewInfos->{'state.id'}            = $tabFrom->{'tabOldRecoveryInfos'}->{'state.id'};
        $tabNewInfos->{'loading'}->{'isDone'} = $tabFrom->{'tabOldRecoveryInfos'}->{'loading'}->{'isDone'};
        $tabNewInfos->{'loading'}->{'date'}   = $tabFrom->{'tabOldRecoveryInfos'}->{'loading'}->{'date'};

        # Récupération de la commande de la récupération du contrat de provenance (si elle existe) en priorité,
        # sinon celle de la livraison du contrat d'arrivée.
        if ($tabOldInfos)
        {
            if ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER)
            {
                # si le transfert n'a pas été changé
                if ($tabOldInfos->{'id'} == $tabFrom->{'tabOldRecoveryInfos'}->{'id'})
                {
                    $tabNewInfos->{'unloading'}->{'isDone'} = $tabFrom->{'tabOldRecoveryInfos'}->{'unloading'}->{'isDone'};
                    $tabNewInfos->{'unloading'}->{'date'}   = $tabFrom->{'tabOldRecoveryInfos'}->{'unloading'}->{'date'};
                    $tabNewInfos->{'valueDate'}             = $tabOldInfos->{'valueDate'};

                    if ($tabOldInfos->{'isSelf'} != $tabNewInfos->{'isSelf'})
                    {
                        $isSendMailForSitesTransfer = 1;
                    }
                    else
                    {
                        $tabNewInfos->{'isConsidered'}      = $tabOldInfos->{'isConsidered'};
                        $tabNewInfos->{'consideredUser.id'} = $tabOldInfos->{'consideredUser.id'};
                        $tabNewInfos->{'consideredDate'}    = $tabOldInfos->{'consideredDate'};
                    }
                }
                else
                {
                    # Changement de contrat de provenance

                    # On transforme l'ancien transfert en récupération standard
                    my $tabNew2Infos = &LOC::Util::clone($tabOldInfos);
                    $tabNew2Infos->{'type'}        = TYPE_RECOVERY;
                    $tabNew2Infos->{'to.type'}     = FROMTOTYPE_AGENCY;
                    $tabNew2Infos->{'to.id'}       = ($tabNew2Infos->{'transferToAgency.id'} ?
                                                        $tabNew2Infos->{'transferToAgency.id'} :
                                                        $tabNew2Infos->{'from.tabInfos'}->{'agency.id'});
                    $tabNew2Infos->{'to.tabInfos'} = &_getFromToEntityInfos($countryId,
                                                                            $tabNew2Infos->{'to.type'},
                                                                            $tabNew2Infos->{'to.id'},
                                                                            SIDE_DELIVERY,
                                                                            GETFROMTOINFOS_DETAILS);
                    $tabNew2Infos->{'agency.id'}   = $tabNew2Infos->{'to.id'};
                    $tabNew2Infos->{'valueDate'}   = &LOC::Date::getMySQLDate();
                    if (!$isSynchroDisabled)
                    {
                        $tabNew2Infos->{'isSynchroDisabled'} = 0;
                    }

                    # si le transfert d'origine était pris en compte
                    # - remise à 0 des informations de prise en compte
                    if ($tabOldInfos->{'isConsidered'} != CONSIDERTYPE_NONE)
                    {
                        $tabNew2Infos->{'isConsidered'}      = CONSIDERTYPE_NONE;
                        $tabNew2Infos->{'consideredUser.id'} = undef;
                        $tabNew2Infos->{'consideredDate'}    = undef;
                    }

                    # Mise à jour des alertes
                    &_updateAlerts($tabOldInfos, $tabNew2Infos, $tabOptions->{'tabDocumentChanges'}, $tabEndUpds);

                    # Mise à jour des traces
                    # - Modification de la récupération de l'ancien contrat lié par transfert
                    &_addTraces($countryId, SIDE_RECOVERY, $tabOldInfos, $tabNew2Infos, $tabEndUpds);


                    # Ajout de la mise à jour secondaire
                    push(@tabActions, {
                        'type'    => 'set',
                        'data'    => $tabNew2Infos,
                        'options' => {
                            'lastSending' => 0
                        }
                    });

                   $isSendMailForSitesTransfer = 1;
                }
            }
            elsif ($tabOldInfos->{'type'} eq TYPE_DELIVERY)
            {
                # On supprime l'ancienne livraison
                my $tabNew2Infos = &LOC::Util::clone($tabOldInfos);
                $tabNew2Infos->{'state.id'} = STATE_DELETED;
                if (!$isSynchroDisabled)
                {
                    $tabNew2Infos->{'isSynchroDisabled'} = 0;
                }

                # Mise à jour des alertes
                &_updateAlerts($tabOldInfos, $tabNew2Infos, $tabOptions->{'tabDocumentChanges'}, $tabEndUpds);


                # Ajout de la mise à jour secondaire
                push(@tabActions, {
                    'type'    => 'set',
                    'data'    => $tabNew2Infos,
                    'options' => {
                        'lastSending' => 0
                    }
                });
                $isSendMailForSitesTransfer = 1;
            }

            # Mise à jour des traces
            if (!$isEstimateLineToContract)
            {
                # - Modification de la livraison
                &_addTraces($countryId, SIDE_DELIVERY, $tabOldInfos, $tabNewInfos, $tabEndUpds);
            }
        }
        else
        {
            $isSendMailForSitesTransfer = 1;
        }

        # Si la désynchro OTD est activée
        # et qu'elle n'était pas activée sur l'ancienne récupération du nouveau contrat de provenance,
        # alors on envoie une dernière fois la commande
        $isLastSendingActived = ($isSynchroDisabled && !$tabFrom->{'tabOldRecoveryInfos'}->{'isSynchroDisabled'} ? 1 : 0);


        # Vérification des dates et des heures entre les deux contrats
        # - Récupération de la caractéristique du délai
        my $maxSitesTsfIntervals = &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('DELTAMAXTSF',
                                                                                                   $tabTo->{'tabInfos'}->{'agency.id'},
                                                                                                   $tabNewInfos->{'valueDate'}));
        # - Calcul du nombre de jours entre la récupération et la livraison
        my %endDate   = &LOC::Date::getHashDate($tabFrom->{'tabInfos'}->{'endDate'}); # fin du contrat 1
        my %beginDate = &LOC::Date::getHashDate($tabTo->{'tabInfos'}->{'beginDate'}); # début du contrat 2
        my $deltaDays = &Date::Calc::Delta_Days($endDate{'y'}, $endDate{'m'}, $endDate{'d'},
                                                $beginDate{'y'}, $beginDate{'m'}, $beginDate{'d'});

        if ($tabTo->{'tabInfos'}->{'window'}->{'till'} lt $tabFrom->{'tabInfos'}->{'window'}->{'from'} ||
            $deltaDays > ($tabTo->{'tabInfos'}->{'isAnticipatable'} ? $maxSitesTsfIntervals->{'anticipate'} : $maxSitesTsfIntervals->{'base'}))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0102);
            return 0;
        }

        # Modifications sur le contrat d'arrivée
        # --------------------------------------

        # Association de la machine
        if ($tabNewInfos->{'to.tabInfos'}->{'machine.id'} != $tabNewInfos->{'machine.id'})
        {
            $tabNewInfos->{'to.tabInfos'}->{'machine.id'} = $tabNewInfos->{'machine.id'};

            push(@tabActions, {
                'type' => 'updateContract',
                'id'   => $tabNewInfos->{'to.id'},
                'data' => {
                    'machine.id'        => $tabNewInfos->{'machine.id'},
                    'isFinalAllocation' => 0
                }
            });
        }

        # Modifications sur le contrat de départ
        # --------------------------------------

        # Si l'enlèvement sur place change
        if ($tabNewInfos->{'isSelf'} != $tabFrom->{'tabOldRecoveryInfos'}->{'isSelf'})
        {
            # On modifie les informations de tarification transport du contrat de départ
            my $tabTrspTariffUpds = {
                'isNoRecovery'       => $tabNewInfos->{'isSelf'},
                'isTransferRecovery' => 1,
                'isLinkedContract'   => 1
            };
            # On s'assure de bien décocher la machine supplémentaire dans le cas d'un enlèvement sur place
            if ($tabNewInfos->{'isSelf'})
            {
                $tabTrspTariffUpds->{'isAddMachineRecovery'} = 0;
            }

            push(@tabActions, {
                'type' => 'updateContract',
                'id'   => $tabNewInfos->{'from.id'},
                'data' => {
                    'tabTransport' => {
                        'tabTariff' => $tabTrspTariffUpds
                    }
                }
            });
        }
        else
        {
            # Mise à jour du contrat de départ par défaut
            push(@tabActions, {
                'type' => 'updateContractIndex',
                'id'   => $tabNewInfos->{'from.id'},
            });
        }


        # Mises à jour pour l'ancienne récupération du contrat de provenance
        # Mise à jour des alertes
        &_updateAlerts($tabFrom->{'tabOldRecoveryInfos'}, $tabNewInfos, {}, $tabEndUpds);

        # Mise à jour des traces
        # - Modification de la récupération du contrat lié par transfert
        &_addTraces($countryId, SIDE_RECOVERY, $tabFrom->{'tabOldRecoveryInfos'}, $tabNewInfos, $tabEndUpds);
    }


    # Mise à jour principale
    unshift(@tabActions, {
        'type'    => 'set',
        'data'    => $tabNewInfos,
        'options' => {
            'mainUpdate'  => 1,
            'lastSending' => $isLastSendingActived
        }
    });


    # Vérification des possibilités de modification
    if ($tabOldInfos && !&_isEditPossible(SIDE_DELIVERY, $tabOldInfos, $tabNewInfos, $tabData->{'rights'}))
    {
        # Impossible de modifier le transport
        push(@{$tabErrors->{'fatal'}}, 0x0003);
        return 0;
    }




    # -----------------------------------
    # MISE A JOUR DE LA BASE DE DONNEES
    # -----------------------------------

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();


    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $tspId = 0;

    # Mode debug
    &_debug(&LOC::Json::toJson(\@tabActions), 'setDocumentDelivery');

    # Exécution des actions
    foreach my $tabAction (@tabActions)
    {
        # Création/modification d'un transport
        if ($tabAction->{'type'} eq 'set')
        {
            $result = &_set($db, $countryId, $tabAction->{'data'}, $tabAction->{'options'});
            if ($tabAction->{'options'} && $tabAction->{'options'}->{'mainUpdate'})
            {
                $tspId  = $tabAction->{'data'}->{'id'};
            }
        }
        # Désassociation ou désattribution
        elsif ($tabAction->{'type'} eq 'updateContract')
        {
            $result = &LOC::Contract::Rent::update($countryId, $tabAction->{'id'}, $tabAction->{'data'}, $userId, undef, $tabEndUpds);
        }
        # Mise à jour de la machine
        elsif ($tabAction->{'type'} eq 'updateMachine')
        {
            $result = &LOC::Machine::update($countryId, $tabAction->{'id'}, $tabAction->{'data'}, $userId, {'updateFrom' => 'internal'}, undef, $tabEndUpds);
        }
        # Mise à jour indice du contrat
        elsif ($tabAction->{'type'} eq 'updateContractIndex')
        {
            my $tabUpdates = {
                'CONTRATINDICE*'     => 'CONTRATINDICE + 1', # Indice de modification du contrat
                'CONTRATDATEMODIF*'  => 'Now()'              # Force la modification de la date de modif
            };
            $result = ($db->update('CONTRAT', $tabUpdates, {'CONTRATAUTO*' => $tabAction->{'id'}}) == -1 ? 0 : 1);
        }


        if (!$result)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }

            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'setDocumentDelivery', __LINE__, __FILE__, ' ' . &LOC::Json::toJson($tabAction));
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Impacts sur la récupération
    if ($tabTo->{'tabInfos'}->{'tabDetails'}->{'recovery.id'})
    {
        $db->addTransactionAction('commit.pre', sub {
                return &renew($countryId, {'id' => $tabTo->{'tabInfos'}->{'tabDetails'}->{'recovery.id'}});
            },
            {'category' => 'transport',
             'priority' => -1000,
             'id' => 'LOC::Transport::renew_' . $countryId . '-id:' . $tabTo->{'tabInfos'}->{'tabDetails'}->{'recovery.id'}}
        );
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();

            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'setDocumentDelivery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }


    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    # Envoi d'un mail si on a un transfert inter-chantiers
    if ($isSendMailForSitesTransfer)
    {
        &sendEmail($countryId, 'sitesTransfer', {'id' => $tspId}, $userId);
    }

    return 1;
}


# Function: setDocumentRecovery
# Définit le transport de récupération pour un document (ligne de devis ou contrat)
#
# Parameters:
# string  $countryId    - Identifiant du pays
# string  $documentType - Type de document ('estimateLine': ligne de devis, 'contract' : contrat)
# int     $documentId   - Id du document
# hashref $tabData      - Données sur la récupération
# int     $userId       - Utilisateur responsable
# hashref $tabOptions   - Options supplémentaires
# hashref $tabErrors    - Tableau des erreurs
# hashref $tabEndUpds   - Mises à jour de fin (facultatif)
#
# Returns:
# bool
sub setDocumentRecovery
{
    my ($countryId, $documentType, $documentId, $tabData, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    my $result;

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Vérification des données en entrée
    if ($documentType ne 'contract' && $documentType ne 'estimateLine')
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'setDocumentRecovery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }


    # Récupération des données du document
    my $tabFrom = {
        'type' => ($documentType eq 'contract' ? FROMTOTYPE_CONTRACT : FROMTOTYPE_ESTIMATELINE),
        'id'   => $documentId
    };
    $tabFrom->{'tabInfos'} = &_getFromToEntityInfos($countryId, $tabFrom->{'type'}, $tabFrom->{'id'},
                                                    SIDE_RECOVERY, GETFROMTOINFOS_DETAILS, {'lockEntityDbRow' => 1});
    if (!$tabFrom->{'tabInfos'})
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'setDocumentRecovery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Formatage des données en entrée
    my $stateId            = STATE_ACTIVE;
    my $isSelf             = ($tabData->{'isSelf'} ? 1 : 0);
    my $delegatedAgencyId  = ($tabData->{'delegatedAgency.id'} ne '' ? $tabData->{'delegatedAgency.id'} : undef);
    my $transferToAgencyId = ($tabData->{'transferToAgency.id'} ne '' ? $tabData->{'transferToAgency.id'} : undef);
    my $contractId         = ($documentType eq 'contract' && $tabData->{'contract.id'} ? $tabData->{'contract.id'} * 1 : undef);
    my $isSynchroDisabled  = ($tabData->{'isSynchroDisabled'} && !$isSelf ? 1 : 0);

    # Actions sur les contrats
    my @tabActions = ();
    my $isLastSendingActived = 0;

    # -------------------------------
    # RECUPERATION
    # -------------------------------

    # Recherche d'une récupération existante
    my $tabOldInfos = &getDocumentRecoveryInfos($countryId, $documentType, $documentId,
                                                GETINFOS_TO | GETINFOS_POSSIBILITIES,
                                                {'lockEntityDbRow' => 1,
                                                 'getToInfosFlags' => GETFROMTOINFOS_DETAILS});

    my $isEstimateLineToContract = 0;
    # Cas du passage d'une ligne de devis en contrat
    if ($tabOldInfos)
    {
        # Ajout des données du point de départ
        $tabOldInfos->{'from.tabInfos'} = &LOC::Util::clone($tabFrom->{'tabInfos'});
    }
    elsif ($documentType eq 'contract' && $tabFrom->{'tabInfos'}->{'tabDetails'}->{'linkedEstimateLine.id'})
    {
        # Récupération des informations de la récupération de la ligne de devis
        $tabOldInfos = &getDocumentRecoveryInfos($countryId, 'estimateLine',
                                                 $tabFrom->{'tabInfos'}->{'tabDetails'}->{'linkedEstimateLine.id'},
                                                 GETINFOS_FROM | GETINFOS_TO | GETINFOS_POSSIBILITIES,
                                                 {'lockEntityDbRow' => 1,
                                                  'getToInfosFlags' => GETFROMTOINFOS_DETAILS});

        # On passe la récupération du devis en "convertie"
        my $tabNewInfos = &LOC::Util::clone($tabOldInfos);
        $tabNewInfos->{'state.id'} = STATE_CONVERTED;

        # Mise à jour des alertes
        &_updateAlerts($tabOldInfos, $tabNewInfos, $tabOptions->{'tabDocumentChanges'}, $tabEndUpds);

        # Ajout de la mise à jour de suppression
        push(@tabActions, {
            'type'    => 'set',
            'data'    => $tabNewInfos,
            'options' => {
                'lastSending' => 0
            }
        });

        if ($tabOldInfos)
        {
            $tabOldInfos->{'id'} = undef;
        }

        $isEstimateLineToContract = 1;
    }

    # Si c'est une modification et qu'il est bloqué alors on ne fait rien
    if ($tabOldInfos && $tabOldInfos->{'isLocked'})
    {
        return 1;
    }

    if ($tabOldInfos)
    {
        # Données par défaut
        $stateId  = $tabOldInfos->{'state.id'};
        if (!exists $tabData->{'isSelf'})
        {
            $isSelf = $tabOldInfos->{'isSelf'};
        }
        if (!exists $tabData->{'delegatedAgency.id'})
        {
            $delegatedAgencyId = $tabOldInfos->{'delegatedAgency.id'};
        }
        if (!exists $tabData->{'transferToAgency.id'})
        {
            $transferToAgencyId = $tabOldInfos->{'transferToAgency.id'};
        }
        if (!exists $tabData->{'contract.id'})
        {
            $contractId = ($tabOldInfos->{'to.type'} eq FROMTOTYPE_CONTRACT ? $tabOldInfos->{'to.id'} : undef);
        }
        if (!exists $tabData->{'isSynchroDisabled'})
        {
            $isSynchroDisabled = $tabOldInfos->{'isSynchroDisabled'};
        }
    }

    # On calcule l'état du transport par rapport aux informations sur le document
    if ($stateId eq STATE_ACTIVE && (
          ($tabFrom->{'type'} eq FROMTOTYPE_CONTRACT &&
           $tabFrom->{'tabInfos'}->{'tabDetails'}->{'state.id'} eq LOC::Contract::Rent::STATE_CANCELED) ||
          ($tabFrom->{'type'} eq FROMTOTYPE_ESTIMATELINE &&
           ($tabFrom->{'tabInfos'}->{'tabDetails'}->{'estimate.state.id'} eq LOC::Estimate::Rent::STATE_ABORTED ||
            $tabFrom->{'tabInfos'}->{'tabDetails'}->{'state.id'} ne LOC::Estimate::Rent::LINE_STATE_ACTIVE))))
    {
        $stateId = STATE_INACTIVE;
        $contractId = undef;
    }
    elsif ($stateId eq STATE_INACTIVE && (
              ($tabFrom->{'type'} eq FROMTOTYPE_CONTRACT &&
               $tabFrom->{'tabInfos'}->{'tabDetails'}->{'state.id'} ne LOC::Contract::Rent::STATE_CANCELED) ||
              ($tabFrom->{'type'} eq FROMTOTYPE_ESTIMATELINE &&
               $tabFrom->{'tabInfos'}->{'tabDetails'}->{'estimate.state.id'} ne LOC::Estimate::Rent::STATE_ABORTED &&
               $tabFrom->{'tabInfos'}->{'tabDetails'}->{'state.id'} eq LOC::Estimate::Rent::LINE_STATE_ACTIVE)))
    {
        $stateId = STATE_ACTIVE;
    }


    # Récupération des données de l'entité d'arrivée
    my $tabTo;
    if (!$contractId)
    {
        # La destination est une agence
        $tabTo = {
            'type' => FROMTOTYPE_AGENCY,
            'id'   => ($transferToAgencyId ne '' ? $transferToAgencyId : $tabFrom->{'tabInfos'}->{'agency.id'})
        };
    }
    else
    {
        # La destination est un contrat
        $tabTo = {
            'type' => FROMTOTYPE_CONTRACT,
            'id'   => $contractId
        };
    }
    $tabTo->{'tabInfos'} = &_getFromToEntityInfos($countryId, $tabTo->{'type'}, $tabTo->{'id'},
                                                  SIDE_DELIVERY, GETFROMTOINFOS_DETAILS, {'lockEntityDbRow' => 1});
    if (!$tabTo->{'tabInfos'})
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'setDocumentRecovery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }


    # Mise à jour principale
    my $tabNewInfos = {
        'isSelf'              => $isSelf,
        'agency.id'           => ($tabTo->{'type'} eq FROMTOTYPE_AGENCY ? $tabTo->{'id'} : $tabTo->{'tabInfos'}->{'agency.id'}),
        'delegatedAgency.id'  => $delegatedAgencyId,
        'transferToAgency.id' => $transferToAgencyId,
        'model.id'            => $tabFrom->{'tabInfos'}->{'model.id'},
        'machine.id'          => $tabFrom->{'tabInfos'}->{'machine.id'},
        'from.type'           => $tabFrom->{'type'},
        'from.id'             => $tabFrom->{'id'},
        'from.tabInfos'       => $tabFrom->{'tabInfos'},
        'to.type'             => $tabTo->{'type'},
        'to.id'               => $tabTo->{'id'},
        'to.tabInfos'         => $tabTo->{'tabInfos'},
        'loading'             => {
            'isDone'   => 0,
            'isDoable' => 0,
            'errors'   => 0,
            'date'     => undef
        },
        'unloading'           => {
            'isDone'   => 0,
            'isDoable' => 0,
            'errors'   => 0,
            'date'     => undef
        },
        'order.id'            => undef,
        'deliverySelfForm.id' => undef,
        'recoverySelfForm.id' => undef,
        'state.id'            => $stateId,
        'isLocked'            => 0,
        'isSynchroDisabled'   => $isSynchroDisabled,
        'isConsidered'        => CONSIDERTYPE_NONE,
        'consideredUser.id'   => undef,
        'consideredDate'      => undef,
        'valueDate'           => &LOC::Date::getMySQLDate()
    };

    # Vérification des cas d'incohérence :
    # - incohérence type du point de destination et id du point de destination
    # (enlèvement en agence avec un id de contrat, reprise sur chantier avec un id d'agence,
    #  transfert inter-chantiers avec un id autre que celui d'un contrat, récupération standard avec un id de contrat)
    # - du cas de l'enlèvement :
    #  - pas d'agence déléguée
    #  - destination de type agence mais id différent de l'agence du contrat
    if (($tabNewInfos->{'to.type'} eq FROMTOTYPE_CONTRACT && !LOC::Util::isNumeric($tabNewInfos->{'to.id'})) ||
        ($tabNewInfos->{'to.type'} eq FROMTOTYPE_AGENCY && LOC::Util::isNumeric($tabNewInfos->{'to.id'})) ||
        ($tabNewInfos->{'agency.id'} eq $tabNewInfos->{'delegatedAgency.id'}) ||
        ($tabNewInfos->{'isSelf'} && $tabNewInfos->{'delegatedAgency.id'}))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }
    if ($tabNewInfos->{'transferToAgency.id'} && (!$tabOldInfos || !$tabOldInfos->{'transferToAgency.id'}) &&
        (!$tabFrom->{'tabInfos'}->{'isFinalAllocation'} ||
         $tabFrom->{'tabInfos'}->{'machine.tabInfos'}->{'state.id'} ne LOC::Machine::STATE_RECOVERY))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Vérification de la cohérence pour la coche "transférée à"
    if ($tabNewInfos->{'transferToAgency.id'} && (!$tabOldInfos || !$tabOldInfos->{'transferToAgency.id'}) &&
        ($tabFrom->{'tabInfos'}->{'machine.tabInfos'}->{'visibilityMode'} == LOC::Machine::VISIBILITYMODE_MACHINE))
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'setDocumentRecovery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0108);
        return 0;
    }

    # Vérification des stocks pour la coche "transférée à"
    if ($tabOldInfos && $tabNewInfos->{'transferToAgency.id'} ne $tabOldInfos->{'transferToAgency.id'})
    {
        my $stockAgencyId = ($tabOldInfos->{'transferToAgency.id'} ? $tabOldInfos->{'transferToAgency.id'} :
                                                                     $tabOldInfos->{'from.tabInfos'}->{'agency.id'});
        if (!&LOC::Model::isRentable($countryId, $stockAgencyId, $tabNewInfos->{'model.id'}))
        {
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'setDocumentRecovery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0109);
            return 0;
        }
    }

    my $isSendMailForSitesTransfer = 0;

    # Calcul des modifications à faire
    if ($tabTo->{'type'} eq FROMTOTYPE_AGENCY)
    {
        # On veut faire une récupération standard
        # ---------------------------------------
        $tabNewInfos->{'type'} = TYPE_RECOVERY;

        # Récupération des informations de réalisation de la récupération si elle existe
        if ($tabOldInfos)
        {
            $tabNewInfos->{'id'}                    = $tabOldInfos->{'id'};
            $tabNewInfos->{'order.id'}              = $tabOldInfos->{'order.id'};
            $tabNewInfos->{'deliverySelfForm.id'}   = $tabOldInfos->{'deliverySelfForm.id'};
            $tabNewInfos->{'recoverySelfForm.id'}   = $tabOldInfos->{'recoverySelfForm.id'};
            $tabNewInfos->{'loading'}->{'isDone'}   = $tabOldInfos->{'loading'}->{'isDone'};
            $tabNewInfos->{'loading'}->{'date'}     = $tabOldInfos->{'loading'}->{'date'};
            $tabNewInfos->{'unloading'}->{'isDone'} = $tabOldInfos->{'unloading'}->{'isDone'};
            $tabNewInfos->{'unloading'}->{'date'}   = $tabOldInfos->{'unloading'}->{'date'};

            # Si c'était déjà une récupération standard
            if ($tabOldInfos->{'type'} eq TYPE_RECOVERY)
            {
                $tabNewInfos->{'valueDate'} = $tabOldInfos->{'valueDate'};

                # Dans le cas où il s'agit de la même récupération, on récupère l'état de prise en compte
                if ($tabNewInfos->{'to.id'} eq $tabOldInfos->{'to.id'})
                {
                    $tabNewInfos->{'isConsidered'}      = $tabOldInfos->{'isConsidered'};
                    $tabNewInfos->{'consideredUser.id'} = $tabOldInfos->{'consideredUser.id'};
                    $tabNewInfos->{'consideredDate'}    = $tabOldInfos->{'consideredDate'};
                }
            }
            # Si la récupération était un transfert
            if ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER)
            {
                # Erreur dans le cas où la case à cocher "transférée à" était cochée
                if ($tabOldInfos->{'transferToAgency.id'})
                {
                    push(@{$tabErrors->{'fatal'}}, 0x0001);
                    return 0;
                }


                # Il faudra créer la livraison standard pour l'ancien contrat de destination
                my $tabNew2Infos = &LOC::Util::clone($tabOldInfos); # -> On reprend les informations du transfert d'origine
                $tabNew2Infos->{'id'}            = undef;
                $tabNew2Infos->{'machine.id'}    = undef;
                $tabNew2Infos->{'type'}          = TYPE_DELIVERY;
                $tabNew2Infos->{'from.type'}     = FROMTOTYPE_AGENCY;
                $tabNew2Infos->{'from.id'}       = $tabOldInfos->{'to.tabInfos'}->{'agency.id'};
                $tabNew2Infos->{'from.tabInfos'} = &_getFromToEntityInfos($countryId,
                                                                          $tabNew2Infos->{'from.type'},
                                                                          $tabNew2Infos->{'from.id'},
                                                                          SIDE_RECOVERY,
                                                                          GETFROMTOINFOS_DETAILS);
                $tabNew2Infos->{'order.id'}      = undef;
                $tabNew2Infos->{'deliverySelfForm.id'} = undef;
                $tabNew2Infos->{'recoverySelfForm.id'} = undef;
                $tabNew2Infos->{'valueDate'}     = &LOC::Date::getMySQLDate();
                if (!$isSynchroDisabled)
                {
                    $tabNew2Infos->{'isSynchroDisabled'} = 0;
                }

                # si le transfert d'origine était pris en compte
                # - remise à 0 des informations de prise en compte
                if ($tabOldInfos->{'isConsidered'} != CONSIDERTYPE_NONE)
                {
                    $tabNew2Infos->{'isConsidered'}      = CONSIDERTYPE_NONE;
                    $tabNew2Infos->{'consideredUser.id'} = undef;
                    $tabNew2Infos->{'consideredDate'}    = undef;
                }

                # Mise à jour des alertes
                &_updateAlerts(undef, $tabNew2Infos, {}, $tabEndUpds);

                # Mise à jour des traces
                # - Modification de la livraison de l'ancien contrat lié par transfert
                &_addTraces($countryId, SIDE_DELIVERY, $tabOldInfos, $tabNew2Infos, $tabEndUpds);

                # Ajout de la mise à jour secondaire
                push(@tabActions, {
                    'type'    => 'set',
                    'data'    => $tabNew2Infos,
                    'options' => {
                        'lastSending' => 0
                    }
                });

                # Dans le cas d'une ancienne reprise sur chantier,
                # il faut recalculer le montant pour le nouveau transport
                # On modifie les informations de tarification transport du contrat de départ
                my $tabTrspTariffUpds = {
                    'isTransferDelivery' => 0
                };

                push(@tabActions, {
                    'type' => 'updateContract',
                    'id'   => $tabNew2Infos->{'to.id'},
                    'data' => {
                        'tabTransport' => {
                            'tabTariff' => $tabTrspTariffUpds
                        }
                    }
                });

                # Désassocier ou désattribuer la machine
                if ($tabNew2Infos->{'to.tabInfos'}->{'machine.id'})
                {
                    $tabNew2Infos->{'to.tabInfos'}->{'machine.id'} = undef;

                    push(@tabActions, {
                        'type' => 'updateContract',
                        'id'   => $tabNew2Infos->{'to.id'},
                        'data' => {
                            'machine.id'  => undef
                        }
                    });
                }
            }

            # Mise à jour des alertes
            &_updateAlerts($tabOldInfos, $tabNewInfos, $tabOptions->{'tabDocumentChanges'}, $tabEndUpds);

            # Mise à jour des traces
            # - Modification de la récupération
            if (!$isEstimateLineToContract)
            {
                &_addTraces($countryId, SIDE_RECOVERY, $tabOldInfos, $tabNewInfos, $tabEndUpds);
            }

            # Si la désynchro OTD est activée et qu'elle n'était pas activée sur l'ancienne récupération (ou transfert),
            # alors on envoie une dernière fois la commande
            $isLastSendingActived = ($isSynchroDisabled && !$tabOldInfos->{'isSynchroDisabled'} ? 1 : 0);
        }
        else
        {
            # Mise à jour des alertes
            &_updateAlerts(undef, $tabNewInfos, {}, $tabEndUpds);

            $isLastSendingActived = $isSynchroDisabled;
        }
    }
    else
    {
        # On veut faire un transfert inter-chantiers
        # ------------------------------------------
        $tabNewInfos->{'type'} = TYPE_SITESTRANSFER;

        # Un transfert sans machine attribuée sur le contrat de provenance est impossible !
        if (!$tabFrom->{'tabInfos'}->{'isFinalAllocation'})
        {
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'setDocumentRecovery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }

        # Vérification du contrat pour le transfert
        my $oldContractId = ($tabOldInfos && $tabOldInfos->{'to.type'} eq FROMTOTYPE_CONTRACT ? $tabOldInfos->{'to.id'} : undef);

        # Modèle de machines : celui de la machine attribuée ou, à défaut, celui demande sur le contrat
        my $modelId = $tabFrom->{'tabInfos'}->{'model.id'};
        if ($tabFrom->{'tabInfos'}->{'machine.tabInfos'})
        {
            $modelId = $tabFrom->{'tabInfos'}->{'machine.tabInfos'}->{'model.id'};
        }

        # Si le contrat de départ n'a pas changé alors on ne fait pas de vérification
        my $tabCttsOptions = {
            'isSelf' => $tabNewInfos->{'isSelf'}
        };
        # si "transférée à" est cochée ou qu'on a une visibilité machine
        if ($tabNewInfos->{'transferToAgency.id'} ||
            $tabFrom->{'tabInfos'}->{'machine.tabInfos'}->{'visibilityMode'} == LOC::Machine::VISIBILITYMODE_MACHINE)
        {
            $tabCttsOptions->{'visibleOnAgencyId'} = ($tabNewInfos->{'transferToAgency.id'} ? $tabNewInfos->{'transferToAgency.id'} : $tabFrom->{'tabInfos'}->{'machine.tabInfos'}->{'visibleOnAgency.id'});
        }
        if ($oldContractId != $tabTo->{'id'} &&
            &getContractsForSitesTransfer($countryId, $tabFrom->{'tabInfos'}->{'agency.id'}, $modelId, SIDE_RECOVERY,
                                          {'isAnticipatable' => 0,
                                           'referenceDate'   => $tabFrom->{'tabInfos'}->{'endDate'},
                                           'referenceHour'   => substr($tabFrom->{'tabInfos'}->{'window'}->{'from'}, 11, 5)
                                          }, LOC::Util::GETLIST_COUNT,
                                          {'id' => $tabTo->{'id'}, 'noFatalError' => 1},
                                          GETSTCTTINFOS_BASE, $tabCttsOptions) == 0)
        {
            # Contrat incorrect pour le transfert inter-chantiers
            push(@{$tabErrors->{'fatal'}}, 0x0100);
            return 0;
        }

        # Vérification s'il n'y a pas de pro forma de prolongation non finalisée :
        # s'il y a une pro forma non finalisée
        if ($tabFrom->{'tabInfos'}->{'tabDetails'}->{'isProformaNotPayed'})
        {
            # Est ce qu'il s'agit d'une pro forma de prolongation
            my $extensionsCount = &LOC::Proforma::getList($countryId, LOC::Util::GETLIST_COUNT,
                                  {'contractId' => $documentId,
                                   'typeId' => LOC::Proforma::Type::ID_EXTENSION,
                                   'stateId' => LOC::Proforma::STATE_ACTIVE,
                                   'isFinalized' => 0});
            if ($extensionsCount > 0)
            {
                push(@{$tabErrors->{'fatal'}}, 0x0103);
                return 0;
            }
        }


        # L'entité d'arrivée est un contrat donc on a besoin de connaître les informations sur sa livraison
        $tabTo->{'tabOldDeliveryInfos'} = &getDocumentDeliveryInfos($countryId, 'contract', $tabTo->{'id'}, GETINFOS_BASE);
        if (!$tabTo->{'tabOldDeliveryInfos'})
        {
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'setDocumentRecovery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
        # On rempli les informations nécessaires aux prochains calculs
        $tabTo->{'tabOldDeliveryInfos'}->{'to.tabInfos'} = $tabTo->{'tabInfos'};
        if ($tabOldInfos && $tabOldInfos->{'id'} == $tabTo->{'tabOldDeliveryInfos'}->{'id'})
        {
            # Si c'est le même transfert alors on récupère les informations au lieu de les recharger
            $tabTo->{'tabOldDeliveryInfos'}->{'from.tabInfos'} = $tabOldInfos->{'from.tabInfos'};
        }
        else
        {
            $tabTo->{'tabOldDeliveryInfos'}->{'from.tabInfos'} = &_getFromToEntityInfos(
                                                                    $countryId,
                                                                    $tabTo->{'tabOldDeliveryInfos'}->{'from.type'},
                                                                    $tabTo->{'tabOldDeliveryInfos'}->{'from.id'},
                                                                    SIDE_RECOVERY,
                                                                    GETFROMTOINFOS_DETAILS);
        }


        # Récupération de la commande de la récupération du contrat de provenance (si elle existe) en priorité,
        # sinon celle de la livraison du contrat d'arrivée.
        if ($tabOldInfos)
        {
            $tabNewInfos->{'id'}                  = $tabOldInfos->{'id'};
            $tabNewInfos->{'order.id'}            = $tabOldInfos->{'order.id'};
            $tabNewInfos->{'deliverySelfForm.id'} = $tabOldInfos->{'deliverySelfForm.id'};
            $tabNewInfos->{'recoverySelfForm.id'} = $tabOldInfos->{'recoverySelfForm.id'};
            $tabNewInfos->{'state.id'}            = $tabOldInfos->{'state.id'};
            $tabNewInfos->{'loading'}->{'isDone'} = $tabOldInfos->{'loading'}->{'isDone'};
            $tabNewInfos->{'loading'}->{'date'}   = $tabOldInfos->{'loading'}->{'date'};

            # si on a pas changé de contrat pour le transfert
            if ($tabOldInfos->{'id'} == $tabTo->{'tabOldDeliveryInfos'}->{'id'})
            {
                # Erreur dans le cas où la case à cocher "transférée à" était cochée et que l'on veut la décocher
                if ($tabOldInfos->{'transferToAgency.id'} && !$tabNewInfos->{'transferToAgency.id'})
                {
                    push(@{$tabErrors->{'fatal'}}, 0x0001);
                    return 0;
                }

                # Dans le cas où on modifie le transfert
                $tabNewInfos->{'unloading'}->{'isDone'} = $tabOldInfos->{'unloading'}->{'isDone'};
                $tabNewInfos->{'unloading'}->{'date'}   = $tabOldInfos->{'unloading'}->{'date'};
                $tabNewInfos->{'valueDate'}             = $tabOldInfos->{'valueDate'};

                if ($tabOldInfos->{'isSelf'} != $tabNewInfos->{'isSelf'})
                {
                    # Erreur dans le cas où la case à cocher "transférée à" était cochée
                    if ($tabOldInfos->{'transferToAgency.id'})
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x0001);
                        return 0;
                    }

                    $isSendMailForSitesTransfer = 1;
                }
                else
                {
                    $tabNewInfos->{'isConsidered'}      = $tabOldInfos->{'isConsidered'};
                    $tabNewInfos->{'consideredUser.id'} = $tabOldInfos->{'consideredUser.id'};
                    $tabNewInfos->{'consideredDate'}    = $tabOldInfos->{'consideredDate'};
                }

                # Mise à jour des alertes
                &_updateAlerts($tabOldInfos, $tabNewInfos, $tabOptions->{'tabDocumentChanges'}, $tabEndUpds);

                # Mise à jour des traces
                # - Modification de la livraison du contrat lié par transfert
                &_addTraces($countryId, SIDE_DELIVERY, $tabOldInfos, $tabNewInfos, $tabEndUpds);
            }
            else
            {
                # Erreur dans le cas où la case à cocher "transférée à" était cochée
                if ($tabNewInfos->{'transferToAgency.id'})
                {
                    push(@{$tabErrors->{'fatal'}}, 0x0001);
                    return 0;
                }

                # On supprime l'ancienne livraison sur le contrat de destination
                my $tabNew2Infos = &LOC::Util::clone($tabTo->{'tabOldDeliveryInfos'});
                $tabNew2Infos->{'state.id'} = STATE_DELETED;
                if (!$isSynchroDisabled)
                {
                    $tabNew2Infos->{'isSynchroDisabled'} = 0;
                }


                # Mise à jour des alertes
                &_updateAlerts($tabTo->{'tabOldDeliveryInfos'}, $tabNew2Infos, {}, $tabEndUpds);

                # Mise à jour des traces
                # - Modification de la livraison du contrat lié par transfert
                &_addTraces($countryId, SIDE_DELIVERY, $tabTo->{'tabOldDeliveryInfos'}, $tabNewInfos, $tabEndUpds);

                # Ajout de la mise à jour secondaire
                push(@tabActions, {
                    'type'    => 'set',
                    'data'    => $tabNew2Infos,
                    'options' => {
                        'lastSending' => 0
                    }
                });


                # Récupération des informations de réalisation du transport
                if ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER)
                {
                    # Il faudra créer la livraison standard pour l'ancien contrat de destination
                    my $tabNew3Infos = &LOC::Util::clone($tabOldInfos); # -> On reprend les informations du transfert d'origine
                    $tabNew3Infos->{'id'}            = undef;
                    $tabNew3Infos->{'machine.id'}    = undef;
                    $tabNew3Infos->{'type'}          = TYPE_DELIVERY;
                    $tabNew3Infos->{'from.type'}     = FROMTOTYPE_AGENCY;
                    $tabNew3Infos->{'from.id'}       = $tabOldInfos->{'to.tabInfos'}->{'agency.id'};
                    $tabNew3Infos->{'from.tabInfos'} = &_getFromToEntityInfos($countryId,
                                                                              $tabNew3Infos->{'from.type'},
                                                                              $tabNew3Infos->{'from.id'},
                                                                              SIDE_RECOVERY,
                                                                              GETFROMTOINFOS_DETAILS);
                    $tabNew3Infos->{'order.id'}      = undef;
                    $tabNew3Infos->{'deliverySelfForm.id'} = undef;
                    $tabNew3Infos->{'recoverySelfForm.id'} = undef;
                    $tabNew3Infos->{'valueDate'}     = &LOC::Date::getMySQLDate();
                    if (!$isSynchroDisabled)
                    {
                        $tabNew3Infos->{'isSynchroDisabled'} = 0;
                    }

                    # si le transfert d'origine était pris en compte
                    # - remise à 0 des informations de prise en compte
                    if ($tabOldInfos->{'isConsidered'} != CONSIDERTYPE_NONE)
                    {
                        $tabNew3Infos->{'isConsidered'}      = CONSIDERTYPE_NONE;
                        $tabNew3Infos->{'consideredUser.id'} = undef;
                        $tabNew3Infos->{'consideredDate'}    = undef;
                    }

                    # Mise à jour des alertes
                    &_updateAlerts(undef, $tabNew3Infos, {}, $tabEndUpds);

                    # Mise à jour des traces
                    # - Modification de la livraison de l'ancien contrat lié par transfert
                    &_addTraces($countryId, SIDE_DELIVERY, $tabOldInfos, $tabNew3Infos, $tabEndUpds);


                    # Ajout de la mise à jour secondaire
                    push(@tabActions, {
                        'type'    => 'set',
                        'data'    => $tabNew3Infos,
                        'options' => {
                            'lastSending' => 0
                        }
                    });

                    # Dans le cas d'une ancienne reprise sur chantier,
                    # il faut recalculer le montant pour le nouveau transport
                    # On modifie les informations de tarification transport du contrat de départ
                    my $tabTrspTariffUpds = {
                        'isTransferDelivery' => 0
                    };

                    push(@tabActions, {
                        'type' => 'updateContract',
                        'id'   => $tabNew3Infos->{'to.id'},
                        'data' => {
                            'tabTransport' => {
                                'tabTariff' => $tabTrspTariffUpds
                            }
                        }
                    });


                    if ($tabNew3Infos->{'to.tabInfos'}->{'machine.id'})
                    {
                        $tabNew3Infos->{'to.tabInfos'}->{'machine.id'} = undef;

                        # Désassocier ou déattibuer la machine
                        push(@tabActions, {
                            'type' => 'updateContract',
                            'id'   => $tabNew3Infos->{'to.id'},
                            'data' => {
                                'machine.id'  => undef
                            }
                        });
                    }
                }

                # Mise à jour des alertes
                &_updateAlerts($tabOldInfos, $tabNewInfos, $tabOptions->{'tabDocumentChanges'}, $tabEndUpds);

                $isSendMailForSitesTransfer = 1;
            }


            # Si la désynchro OTD est activée et qu'elle n'était pas activée sur la livraison du contrat de destination,
            # alors on envoie une dernière fois la commande
            $isLastSendingActived = ($isSynchroDisabled && !$tabOldInfos->{'isSynchroDisabled'} ? 1 : 0);

            # Mise à jour des traces
            # - Modification de la récupération
            if (!$isEstimateLineToContract)
            {
                &_addTraces($countryId, SIDE_RECOVERY, $tabOldInfos, $tabNewInfos, $tabEndUpds);
            }

        }
        else
        {
            # Il est impossible de générer un transfert inter-chantiers sur une récupération d'un contrat en création
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'setDocumentRecovery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }

        # Vérification des dates et des heures entre les deux contrats
        # - Récupération de la caractéristique du délai
        my $maxSitesTsfIntervals = &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('DELTAMAXTSF',
                                                                                                   $tabTo->{'tabInfos'}->{'agency.id'},
                                                                                                   $tabNewInfos->{'valueDate'}));
        # - Calcul du nombre de jours entre la récupération et la livraison
        my %endDate   = &LOC::Date::getHashDate($tabFrom->{'tabInfos'}->{'endDate'}); # fin du contrat 1
        my %beginDate = &LOC::Date::getHashDate($tabTo->{'tabInfos'}->{'beginDate'}); # début du contrat 2
        my $deltaDays = &Date::Calc::Delta_Days($endDate{'y'}, $endDate{'m'}, $endDate{'d'},
                                                $beginDate{'y'}, $beginDate{'m'}, $beginDate{'d'});

        if ($tabTo->{'tabInfos'}->{'window'}->{'till'} lt $tabFrom->{'tabInfos'}->{'window'}->{'from'} ||
            $deltaDays > ($tabTo->{'tabInfos'}->{'isAnticipatable'} ? $maxSitesTsfIntervals->{'anticipate'} : $maxSitesTsfIntervals->{'base'}))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0102);
            return 0;
        }

        # Modifications sur le contrat d'arrivée
        # --------------------------------------
        $tabTo->{'tabUpdates'} = {};

        # Si l'enlèvement sur place change
        if ($tabNewInfos->{'isSelf'} != $tabTo->{'tabOldDeliveryInfos'}->{'isSelf'})
        {
            # On modifie les informations de tarification transport du contrat d'arrivée
            my $tabTrspTariffUpds = {
                'isNoDelivery'       => $tabNewInfos->{'isSelf'},
                'isTransferDelivery' => 1,
                'isLinkedContract'   => 1
            };
            # On s'assure de bien décocher la machine supplémentaire dans le cas d'un enlèvement sur place
            if ($tabNewInfos->{'isSelf'})
            {
                $tabTrspTariffUpds->{'isAddMachineDelivery'} = 0;
            }

            $tabTo->{'tabUpdates'}->{'tabTransport'} = {
                'tabTariff' => $tabTrspTariffUpds
            };
        }

        # Association de la machine
        if ($tabNewInfos->{'to.tabInfos'}->{'machine.id'} != $tabNewInfos->{'machine.id'})
        {
            $tabNewInfos->{'to.tabInfos'}->{'machine.id'} = $tabNewInfos->{'machine.id'};

            $tabTo->{'tabUpdates'}->{'machine.id'}        = $tabNewInfos->{'machine.id'};
            $tabTo->{'tabUpdates'}->{'isFinalAllocation'} = 0;
        }

        # Mise à jour du contrat d'arrivée
        if (keys %{$tabTo->{'tabUpdates'}} > 0)
        {
            push(@tabActions, {
                'type' => 'updateContract',
                'id'   => $tabTo->{'id'},
                'data' => $tabTo->{'tabUpdates'}
            });
        }
        else
        {
            # Mise à jour du contrat de départ par défaut
            push(@tabActions, {
                'type' => 'updateContractIndex',
                'id'   => $tabTo->{'id'},
            });
        }
    }

    # Mise à jour de l'agence de transfert sur la machine
    if ($tabOldInfos && ($tabOldInfos->{'transferToAgency.id'} ne $tabNewInfos->{'transferToAgency.id'}))
    {
        my $tabMachineData = {
            'visibleOnAgency.id' => $tabNewInfos->{'transferToAgency.id'},
            'visibilityMode'     => ($tabNewInfos->{'transferToAgency.id'} ? LOC::Machine::VISIBILITYMODE_TRANSPORT :
                                                                             LOC::Machine::VISIBILITYMODE_NONE)
        };

        push(@tabActions, {
            'type' => 'updateMachine',
            'id'   => $tabNewInfos->{'machine.id'},
            'data' => $tabMachineData
        });
    }



    # Mise à jour principale
    unshift(@tabActions, {
        'type'    => 'set',
        'data'    => $tabNewInfos,
        'options' => {
            'mainUpdate'  => 1,
            'lastSending' => $isLastSendingActived
        }
    });

    # Vérification des possibilités de modification
    if ($tabOldInfos && !&_isEditPossible(SIDE_RECOVERY, $tabOldInfos, $tabNewInfos, $tabData->{'rights'}))
    {
        # Impossible de modifier le transport
        push(@{$tabErrors->{'fatal'}}, 0x0003);
        return 0;
    }


    # -----------------------------------
    # MISE A JOUR DE LA BASE DE DONNEES
    # -----------------------------------

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();


    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $tspId = 0;

    # Mode debug
    &_debug(&LOC::Json::toJson(\@tabActions), 'setDocumentRecovery');

    # Exécution des actions
    foreach my $tabAction (@tabActions)
    {
        # Création/modification d'un transport
        if ($tabAction->{'type'} eq 'set')
        {
            $result = &_set($db, $countryId, $tabAction->{'data'}, $tabAction->{'options'});
            if ($tabAction->{'options'} && $tabAction->{'options'}->{'mainUpdate'})
            {
                $tspId  = $tabAction->{'data'}->{'id'};
            }
        }
        # Désassociation ou désattribution
        elsif ($tabAction->{'type'} eq 'updateContract')
        {
            $result = &LOC::Contract::Rent::update($countryId, $tabAction->{'id'}, $tabAction->{'data'}, $userId, {}, $tabEndUpds);
        }
        # Mise à jour de la machine
        elsif ($tabAction->{'type'} eq 'updateMachine')
        {
            $result = &LOC::Machine::update($countryId, $tabAction->{'id'}, $tabAction->{'data'}, $userId, {'updateFrom' => 'internal'}, undef, $tabEndUpds);
        }
        # Mise à jour indice du contrat
        elsif ($tabAction->{'type'} eq 'updateContractIndex')
        {
            my $tabUpdates = {
                'CONTRATINDICE*'     => 'CONTRATINDICE + 1', # Indice de modification du contrat
                'CONTRATDATEMODIF*'  => 'Now()'              # Force la modification de la date de modif
            };
            $result = ($db->update('CONTRAT', $tabUpdates, {'CONTRATAUTO*' => $tabAction->{'id'}}) == -1 ? 0 : 1);
        }

        if (!$result)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }

            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'setDocumentRecovery', __LINE__, __FILE__,  ' ' . &LOC::Json::toJson($tabAction));
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Impacts sur la livraison
    if ($tabFrom->{'tabInfos'}->{'tabDetails'}->{'delivery.id'})
    {
        $db->addTransactionAction('commit.pre', sub {
                return &renew($countryId, {'id' => $tabFrom->{'tabInfos'}->{'tabDetails'}->{'delivery.id'}});
            },
            {'category' => 'transport',
             'priority' => -1000,
             'id' => 'LOC::Transport::renew_' . $countryId . '-id:' . $tabFrom->{'tabInfos'}->{'tabDetails'}->{'delivery.id'}}
        );
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();

            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'setDocumentRecovery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }


    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    # Envoi d'un mail si on a un transfert inter-chantiers
    if ($isSendMailForSitesTransfer)
    {
        &sendEmail($countryId, 'sitesTransfer', {'id' => $tspId}, $userId);
    }

    return 1;
}



# Function: setAgenciesTransfer
# Définit le transport inter-agence d'une machine
#
# Parameters:
# string  $countryId    - Identifiant du pays
# int     $machineId    - Identifiant de la machine
# hashref $tabData      - Données sur le transfert inter-agence
# int     $userId       - Utilisateur responsable
# hashref $tabOptions   - Options supplémentaires
# hashref $tabErrors    - Tableau des erreurs
# hashref $tabEndUpds   - Mises à jour de fin (facultatif)
#
# Returns:
# bool
sub setAgenciesTransfer
{
    my ($countryId, $machineId, $tabData, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    my $result;

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Vérification des données en entrée
    # A voir!

    # Récupération des données de la machine
    my $tabMachineInfos = &LOC::Machine::getInfos($countryId, $machineId, LOC::Machine::GETINFOS_BASE);

    # Recherche d'un transfert inter-agence existant sur cette machine
    my $tabOldInfos = &getAgenciesTransferInfos($countryId, $machineId, GETINFOS_BASE | GETINFOS_MACHINE,
                                                {'isSortDisabled'  => 1,
                                                 'lastId'          => 1,
                                                 'isUnloadingDone' => 0
                                                });

    # Si c'est une modification et qu'il est bloqué alors on ne fait rien
    if ($tabOldInfos && $tabOldInfos->{'isLocked'})
    {
        return 1;
    }
    # - Agence de destination doit être renseignée si nouveau transfert inter-agence
    if (!$tabOldInfos && !$tabData->{'to.id'})
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'setAgenciesTransfer', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0201);
        return 0;
    }
    # - Date de transfert doit être renseignée si nouveau transfert inter-agence
    # OU si transfert existant modifié
    if ((!$tabOldInfos && !$tabData->{'date'}) || ($tabOldInfos && !$tabData->{'date'} && $tabData->{'to.id'}))
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'setAgenciesTransfer', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0202);
        return 0;
    }

    # Droits et possibilités
    # - Transfert non autorisé car la destination n'est pas l'agence de visibilité
    if ($tabMachineInfos->{'visibleOnAgency.id'} && $tabMachineInfos->{'visibleOnAgency.id'} ne $tabData->{'to.id'})
    {
        # Erreur fatale !
        push(@{$tabErrors->{'fatal'}}, 0x0003);
        return 0;
    }

    # Formatage des données en entrée
    # Etat du transport
    my $stateId           = STATE_ACTIVE;
    # Identifiant de l'agence de destination
    my $toAgencyId        =  ($tabData->{'to.id'} ? $tabData->{'to.id'} : undef);
    # Date de transfert saisie
    my $transferDate      = ($tabData->{'date'} ? $tabData->{'date'} : undef);
    # Identifiant de l'agence déléguée
    my $delegatedAgencyId = ($tabData->{'delegatedAgency.id'} ? $tabData->{'delegatedAgency.id'} : undef);

    # On calcule l'état du transport par rapport aux informations sur la machine
    # - Transfert inter-agence existant et désélection de l'agence de transfert
    if ($tabOldInfos && !$tabData->{'to.id'})
    {
        $stateId = STATE_DELETED;
        $toAgencyId = $tabOldInfos->{'to.id'};
    }


    # Actions
    my @tabActions = ();

    # Mise à jour principale
    my $tabNewInfos = {
        'type'               => TYPE_AGENCIESTRANSFER,
        'isSelf'             => 0,
        'agency.id'          => $toAgencyId,
        'delegatedAgency.id' => $delegatedAgencyId,
        'model.id'           => $tabMachineInfos->{'model.id'},
        'machine.id'         => $tabMachineInfos->{'id'},
        'from.type'          => FROMTOTYPE_AGENCY,
        'from.id'            => $tabMachineInfos->{'agency.id'},
        'to.type'            => FROMTOTYPE_AGENCY,
        'to.id'              => $toAgencyId,
        'loading'            => {
            'isDone'   => 0,
            'isDoable' => 0,
            'errors'   => 0,
            'date'     => undef
        },
        'unloading'          => {
            'isDone'   => 0,
            'isDoable' => 0,
            'errors'   => 0,
            'date'     => undef
        },
        'order.id'           => undef,
        'deliverySelfForm.id' => undef,
        'recoverySelfForm.id' => undef,
        'state.id'           => $stateId,
        'isLocked'           => 0,
        'machine.tabInfos'   =>  &_getMachineInfos($countryId, $machineId),
        'date'               => $transferDate,
        'isSynchroDisabled'  => 0,
        'isConsidered'       => CONSIDERTYPE_NONE,
        'consideredUser.id'  => undef,
        'consideredDate'     => undef,
        'valueDate'          => $transferDate
    };

    # Modification pour l'envoi de l'email
    my @tabModif;

    # Options pour la maj des données de la machine
    my $tabMachineOptions = {};

    # Vérification des cas d'incohérence :
    # - incohérence type du point de départ et id du point de départ
    # transfert inter-agence avec un id autre que celui de l'agence
    # - l'agence déléguée est l'agence qui s'occupe déjà du transport
    if ($tabNewInfos->{'from.type'} eq FROMTOTYPE_AGENCY && LOC::Util::isNumeric($tabNewInfos->{'from.id'}) ||
        $tabNewInfos->{'agency.id'} eq $tabNewInfos->{'delegatedAgency.id'})
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }
    # - l'agence déléguée n'est pas dans le même pays que l'agence de destination
    if ($tabNewInfos->{'delegatedAgency.id'})
    {
        my $tabAgencyInfos          = &LOC::Agency::getInfos($tabNewInfos->{'agency.id'});
        my $tabDelegatedAgencyInfos = &LOC::Agency::getInfos($tabNewInfos->{'delegatedAgency.id'});
        if ($tabAgencyInfos->{'country.id'} ne $tabDelegatedAgencyInfos->{'country.id'} ||
            $tabDelegatedAgencyInfos->{'state.id'} ne LOC::Agency::STATE_ACTIVE)
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Récupération des informations du transfert inter-agence existant
    if ($tabOldInfos)
    {
        $tabNewInfos->{'loading'}->{'isDone'}   = $tabOldInfos->{'loading'}->{'isDone'};
        $tabNewInfos->{'loading'}->{'date'}     = $tabOldInfos->{'loading'}->{'date'};
        $tabNewInfos->{'unloading'}->{'isDone'} = $tabOldInfos->{'unloading'}->{'isDone'};
        $tabNewInfos->{'unloading'}->{'date'}   = $tabOldInfos->{'unloading'}->{'date'};
        $tabNewInfos->{'id'}                    = $tabOldInfos->{'id'};
        $tabNewInfos->{'order.id'}              = $tabOldInfos->{'order.id'};

        if ($stateId eq STATE_DELETED)
        {
            # Suppression du transfert inter-agence
            push(@tabModif, 'suppr');

            # - mise à jour de l'état de la machine, elle passe en vérif
            push(@tabActions, {
                'type' => 'updateMachine',
                'id'   => $machineId,
                'data' => {
                    'state.id' => LOC::Machine::STATE_CHECK
                }
            });
        }
        else
        {
            # Modification de transfert inter-agence
            push(@tabModif, 'modif');
        }

        # Mise à jour des alertes : a voir
        &_updateAlerts($tabOldInfos, $tabNewInfos, {}, $tabEndUpds);
    }
    else
    {
        # Création de transfert inter-agence
        push(@tabModif, 'create');

        # Mise à jour de la visibilité
        my $visibleOnAgencyId;
        my $visibilityMode;

        # Récupération de la liste des agences de visibilité du pays de la machine
        my $tabVisibilityAgencies = &LOC::Agency::getList(LOC::Util::GETLIST_PAIRS,
                                                          {'country'     => $tabMachineInfos->{'country.id'},
                                                           'exceptions'  => $tabMachineInfos->{'agency.id'}});

        my @tabVisibilityAgenciesKeys = keys %$tabVisibilityAgencies;

        # Mise à jour de l'agence de visibilité uniquement si l'agence de destination se trouve dans le pays de la machine
        if (&LOC::Util::in_array($toAgencyId, \@tabVisibilityAgenciesKeys))
        {
            $visibleOnAgencyId  = $toAgencyId;
            $visibilityMode     = LOC::Machine::VISIBILITYMODE_MACHINE;
        }

        # - mise à jour de l'état de la machine, elle passe en vérification
        # - mise à jour de la visibilité, elle est visible sur l'agence de destination
        push(@tabActions, {
            'type' => 'updateMachine',
            'id'   => $machineId,
            'data' => {
                'state.id'           => LOC::Machine::STATE_TRANSFER,
                'visibleOnAgency.id' => ($visibleOnAgencyId ? $visibleOnAgencyId : undef),
                'visibilityMode'     => ($visibilityMode ? $visibilityMode : undef)
            }
        });
        $tabMachineOptions->{'updateFrom'} = 'internal';

        # Mise à jour des alertes : a voir
        &_updateAlerts(undef, $tabNewInfos, {}, $tabEndUpds);
    }

    # Trace pour la création/modification/suppression du transfert inter-agences
    if (@tabModif > 0)
    {
        my $tabCountryInfos = &LOC::Country::getInfos($countryId);
        my $locale = &LOC::Locale::getLocale($tabCountryInfos->{'locale.id'}, undef, [POSIX::LC_MESSAGES, POSIX::LC_CTYPE]);
        my $schemaName = &LOC::Db::getSchemaName($countryId);

        # Formatage du contenu
        my $newContent = &_formatTraces(undef, LOC::Log::EventType::AGENCIESTRANSFER, $tabNewInfos, $locale);
        my $tabTrace = {
            'schema'  => $schemaName,
            'code'    => LOC::Log::EventType::AGENCIESTRANSFER,
            'old'     => {
                'from.id'            => $tabOldInfos->{'from.id'},
                'to.id'              => $tabOldInfos->{'to.id'},
                'date'               => $tabOldInfos->{'date'},
                'delegatedAgency.id' => $tabOldInfos->{'delegatedAgency.id'}
            },
            'new'     => {
                'from.id'            => $tabNewInfos->{'from.id'},
                'to.id'              => $tabNewInfos->{'to.id'},
                'date'               => $tabNewInfos->{'date'},
                'delegatedAgency.id' => $tabNewInfos->{'delegatedAgency.id'}
            },
            'content' => $newContent
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $machineId, $tabTrace);
    }

    # Mise à jour principale
    unshift(@tabActions, {
        'type'    => 'set',
        'data'    => $tabNewInfos,
        'options' => {}
    });

    # Vérification des possibilités de modification
    # A voir!

    # -----------------------------------
    # MISE A JOUR DE LA BASE DE DONNEES
    # -----------------------------------
    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Exécution des actions
    foreach my $tabAction (@tabActions)
    {
        # Création/modification d'un transport
        if ($tabAction->{'type'} eq 'set')
        {
            $result = &_set($db, $countryId, $tabAction->{'data'}, $tabAction->{'options'});
        }
        # Mise à jour de la machine
        elsif ($tabAction->{'type'} eq 'updateMachine')
        {
            $result = &LOC::Machine::update($countryId, $tabAction->{'id'}, $tabAction->{'data'}, $userId, $tabMachineOptions, undef, $tabEndUpds);
        }

        if (!$result)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }

            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'setAgenciesTransfer', __LINE__, __FILE__, ' ' . &LOC::Json::toJson($tabAction));
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();

            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'setAgenciesTransfer', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    # Envoi de l'email
    my $tabData = {
        'tabOldInfos' => $tabOldInfos,
        'tabNewInfos' => $tabNewInfos,
        'tabModif'    => \@tabModif
    };
    &sendEmail($countryId, 'agenciesTransfer', $tabData, $userId);

    return 1;
}


# Function: undoDocumentDelivery
# Annule la réalisation de la livraison d'un document (ligne de devis ou contrat)
#
# Parameters:
# string  $countryId    - Identifiant du pays
# string  $documentType - Type de document ('estimateLine': ligne de devis, 'contract' : contrat)
# int     $documentId   - Id du document
# int     $userId       - Utilisateur responsable
# hashref $tabErrors    - Tableau des erreurs
# hashref $tabEndUpds   - Mises à jour de fin (facultatif)
#
# Returns:
# bool
sub undoDocumentDelivery
{
    my ($countryId, $documentType, $documentId, $doLink, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Infos sur le transport
    my $tabOldInfos = &getDocumentDeliveryInfos($countryId, $documentType, $documentId,
                                                GETINFOS_FROM | GETINFOS_TO,
                                                {'lockEntityDbRow'   => 1,
                                                 'getFromInfosFlags' => GETFROMTOINFOS_DETAILS,
                                                 'getToInfosFlags'   => GETFROMTOINFOS_DETAILS});
    if (!$tabOldInfos)
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'undoDocumentDelivery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Vérifications
    # - réalisé manuellement ou enlèvement sur place
    # - contrat à l'état CON02
    # - machine toujours sur le contrat
    # - livraison ou transfert
    if (($tabOldInfos->{'unloading'}->{'isDone'} != ACTION_USERDONE || !$tabOldInfos->{'unloading'}->{'isDoable'}) ||
        ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER && $doLink &&
            (!$tabOldInfos->{'loading'}->{'isDone'} || !$tabOldInfos->{'loading'}->{'isDoable'})))
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Impossible d'annuler la réalisation du transport
        push(@{$tabErrors->{'fatal'}}, 0x0107);
        return 0;
    }

    # Mise à jour de la date de modification du contrat
    if ($db->update('CONTRAT', {'CONTRATDATEMODIF*' => 'Now()'},
                               {'CONTRATAUTO*' => $tabOldInfos->{'to.id'}}, $schemaName) == -1)
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'undoDocumentDelivery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Annulation de la réalisation du déchargement
    if (!&_undoUnloading($db, $countryId, $tabOldInfos, $documentType, $userId, $tabEndUpds))
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'undoDocumentDelivery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }


    # Si on est dans l'annulation d'un transfert complet, il faut ensuite annuler la récupération de ce transfert
    if ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER)
    {
        # Mise à jour de la date de modification du contrat
        if ($db->update('CONTRAT', {'CONTRATDATEMODIF*' => 'Now()'},
                                   {'CONTRATAUTO*' => $tabOldInfos->{'from.id'}}, $schemaName) == -1)
        {
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'undoDocumentDelivery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }

        # Annulation de la réalisation du chargement
        if ($doLink && !&_undoLoading($db, $countryId, $tabOldInfos, $documentType, $userId, $tabEndUpds))
        {
            if (!$hasTransaction)
            {
                # Annule la transaction
                $db->rollBack();
            }

            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'undoDocumentDelivery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();

            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'undoDocumentDelivery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: undoDocumentRecovery
# Annule la réalisation de la récupération d'un document (ligne de devis ou contrat)
#
# Parameters:
# string  $countryId    - Identifiant du pays
# string  $documentType - Type de document ('estimateLine': ligne de devis, 'contract' : contrat)
# int     $documentId   - Id du document
# int     $userId       - Utilisateur responsable
# hashref $tabErrors    - Tableau des erreurs
# hashref $tabEndUpds   - Mises à jour de fin (facultatif)
#
# Returns:
# bool
sub undoDocumentRecovery
{
    my ($countryId, $documentType, $documentId, $doLink, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Infos sur le transport
    my $tabOldInfos = &getDocumentRecoveryInfos($countryId, $documentType, $documentId,
                                                GETINFOS_FROM | GETINFOS_TO,
                                                {'lockEntityDbRow'   => 1,
                                                 'getFromInfosFlags' => GETFROMTOINFOS_DETAILS,
                                                 'getToInfosFlags'   => GETFROMTOINFOS_DETAILS});
    if (!$tabOldInfos)
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'undoDocumentRecovery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }


    # Vérifications
    # - réalisé manuellement ou enlèvement sur place
    # - contrat à l'état CON03
    # - si récupération normale, machine toujours sur le contrat
    # - si transfert, livraison non réalisée
    # - récupération ou transfert
    # - machine toujours dans l'agence du contrat
    # - machine état dispo ou vérif
    if (($tabOldInfos->{'loading'}->{'isDone'} != ACTION_USERDONE || !$tabOldInfos->{'loading'}->{'isDoable'}) ||
        ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER && $doLink &&
            (!$tabOldInfos->{'unloading'}->{'isDone'} || !$tabOldInfos->{'unloading'}->{'isDoable'})) ||
        ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER && !$doLink && $tabOldInfos->{'unloading'}->{'isDone'}))
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Impossible d'annuler la réalisation du transport
        push(@{$tabErrors->{'fatal'}}, 0x0107);
        return 0;
    }



    # Si on est dans l'annulation d'un transfert complet, il faut d'abord annuler la livraison de ce transfert
    if ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER)
    {
        # Mise à jour de la date de modification du contrat
        if ($db->update('CONTRAT', {'CONTRATDATEMODIF*' => 'Now()'},
                                   {'CONTRATAUTO*' => $tabOldInfos->{'to.id'}}, $schemaName) == -1)
        {
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'undoDocumentRecovery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }

        # Annulation de la réalisation du déchargement
        if ($doLink && !&_undoUnloading($db, $countryId, $tabOldInfos, $documentType, $userId, $tabEndUpds))
        {
            if (!$hasTransaction)
            {
                # Annule la transaction
                $db->rollBack();
            }
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'undoDocumentRecovery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Mise à jour de la date de modification du contrat
    if ($db->update('CONTRAT', {'CONTRATDATEMODIF*' => 'Now()'},
                               {'CONTRATAUTO*' => $tabOldInfos->{'from.id'}}, $schemaName) == -1)
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'undoDocumentRecovery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Annulation de la réalisation du chargement
    if (!&_undoLoading($db, $countryId, $tabOldInfos, $documentType, $userId, $tabEndUpds))
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'undoDocumentRecovery', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();

            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'undoDocumentRecovery', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: undoAgenciesTransfer
# Annule la réalisation du transfert inter-agence
#
# Parameters:
# string  $countryId    - Identifiant du pays
# int     $machineId    - Id de la machine
# int     $userId       - Utilisateur responsable
# hashref $tabErrors    - Tableau des erreurs
# hashref $tabEndUpds   - Mises à jour de fin (facultatif)
#
# Returns:
# bool
sub undoAgenciesTransfer
{
    my ($countryId, $machineId, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Infos sur le transport
    my $tabOldInfos = &getAgenciesTransferInfos($countryId, $machineId, GETINFOS_BASE | GETINFOS_MACHINE | GETINFOS_FROM | GETINFOS_TO,
                                                {'isSortDisabled' => 1,
                                                 'lastId'         => 1,
                                                 'getFromInfosFlags' => GETFROMTOINFOS_DETAILS,
                                                 'getToInfosFlags'   => GETFROMTOINFOS_DETAILS
                                                });
    if (!$tabOldInfos)
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'undoAgenciesTransfer', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Vérifications
    if ($tabOldInfos->{'unloading'}->{'isDone'} != ACTION_USERDONE ||
        ($tabOldInfos->{'unloading'}->{'isDone'} == ACTION_USERDONE && !$tabOldInfos->{'unloading'}->{'isDoable'})
        )
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'undoAgenciesTransfer', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    if (!&_undoAgenciesTransfer($db, $countryId, $tabOldInfos, $userId, $tabEndUpds))
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'undoAgenciesTransfer', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();

            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'undoAgenciesTransfer', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;

}



# Function: unlock
# Déverrouille des transports
#
# Parameters:
# string  $countryId  - Pays
# hashref $tabFilters - Filtres de sélection des transports (voir getList())
# int     $userId     - Utilisateur responsable
# hashref $tabEndUpds - Mises à jour de fin (facultatif)
#
# Returns:
# bool
sub unlock
{
    my ($countryId, $tabFilters, $userId, $tabEndUpds) = @_;

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }


    # On ne prend que les transports verrouillés
    $tabFilters->{'isLocked'} = 1;

    # Récupération de la liste des transports concernés
    my $tabList = &getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters,
                                                                 GETINFOS_FROM | GETINFOS_TO | GETINFOS_MACHINE,
                                                                 {'getFromInfosFlags' => GETFROMTOINFOS_DETAILS,
                                                                  'getToInfosFlags'   => GETFROMTOINFOS_DETAILS});

    # Pour chaque transport, on recalcule sa faisabilité et on renvoie la commande
    foreach my $tabInfos (values %$tabList)
    {
        $tabInfos->{'isLocked'} = 0;

        if (!&_set($db, $countryId, $tabInfos, undef))
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}

# Function: updatePendingTransport
# Met à jour la liste des transports en cours
#
# Informations complémentaires : lors de l'ajout d'un champ, il faut lancer une mise à jour complète de la table (renew)
#
# Parameters:
# string   $countryId - Identifiant du pays
# int      $id        - Identifiant du transport
# hashref  $tabInfos  - Informations sur le transport
#
# Returns:
# bool
sub _updatePendingTransport
{
    my ($db, $countryId, $id, $tabInfos) = @_;

    if (!defined $tabInfos)
    {
        return 0;
    }
    if (!defined $id)
    {
        return 0;
    }

    my $tabFromInfos = $tabInfos->{'from.tabInfos'};
    my $fromId       = $tabInfos->{'from.id'};
    my $tabToInfos   = $tabInfos->{'to.tabInfos'};
    my $toId         = $tabInfos->{'to.id'};

    # Suppression de la ligne
    # - si ligne supprimée ou inactive ou convertie
    # - ou si transport réalisé
    # - ou ligne de devis non acceptée
    if (&LOC::Util::in_array($tabInfos->{'state.id'}, [STATE_DELETED, STATE_INACTIVE, STATE_CONVERTED]) ||
        ($tabInfos->{'type'} eq TYPE_RECOVERY && $tabInfos->{'loading'}->{'isDone'}) ||
        (&LOC::Util::in_array($tabInfos->{'type'}, [TYPE_DELIVERY, TYPE_SITESTRANSFER, TYPE_AGENCIESTRANSFER]) &&
            $tabInfos->{'unloading'}->{'isDone'}) ||
        ($tabFromInfos->{'@type'} eq FROMTOTYPE_ESTIMATELINE && !$tabFromInfos->{'isAccepted'}) ||
        ($tabToInfos->{'@type'} eq FROMTOTYPE_ESTIMATELINE && !$tabToInfos->{'isAccepted'})
        )
    {
        if ($db->delete('f_pendingtransport', {'pdt_id*' => $id}) == -1)
        {
            return 0;
        }
        return 1;
    }

    # Récupération des informations
    my $agencyId = ($tabInfos->{'delegatedAgency.id'} ? $tabInfos->{'delegatedAgency.id'} : $tabInfos->{'agency.id'});
    my $tabAgencyInfos = &LOC::Agency::getInfos($agencyId);
    my $isMachineWithTelematics = 0;
    if ($tabInfos->{'machine.id'})
    {
        my $query = '
SELECT 
    `AUTH`.`f_machine_info`.`min_value` 
FROM `AUTH`.`f_machine_info` 
LEFT JOIN `AUTH`.`p_info` ON `AUTH`.`f_machine_info`.`min_inf_id` = `AUTH`.`p_info`.`inf_id`
WHERE
    `AUTH`.`f_machine_info`.`min_mac_id` = ' . $db->quote($tabInfos->{'machine.id'}) . '
    AND `AUTH`.`p_info`.`inf_externalcode` = ' . $db->quote(LOC::Machine::ADDINFO_TELEMATICS) . ';';

        $isMachineWithTelematics = $db->fetchOne($query);
    }

    # Récupération des informations spécifiques
    my $tab = {
        'from' => {},
        'to'   => {}
    };

    # -- Date
    $tab->{'from'}->{'date'} = $tabFromInfos->{'endDate'};
    $tab->{'to'}->{'date'}   = $tabToInfos->{'beginDate'};
    if ($tabInfos->{'type'} eq TYPE_AGENCIESTRANSFER)
    {
         $tab->{'from'}->{'date'} = $tabInfos->{'date'};
         $tab->{'to'}->{'date'}   = $tabInfos->{'date'};
    }

    # -- Récupération heure
    sub _getHour {
        my ($tabWindow) = @_;

        my $hour = undef;
        if (defined $tabWindow)
        {
            my @tabFrom = split(/ /, $tabWindow->{'from'});
            my @tabTill = split(/ /, $tabWindow->{'till'});
            if ($tabFrom[1] eq $tabTill[1])
            {
                $hour = substr($tabFrom[1], 0, 5);
            }
            else
            {
                $hour = sprintf('%s-%s', substr($tabFrom[1], 0, 5), substr($tabTill[1], 0, 5));
            }
        }
        return $hour;
    };
    $tab->{'from'}->{'hour'} = &_getHour($tabFromInfos->{'window'});
    $tab->{'to'}->{'hour'}   = &_getHour($tabToInfos->{'window'});

    # -- Code
    $tab->{'from'}->{'documentCode'} = $tabFromInfos->{'code'};
    $tab->{'to'}->{'documentCode'}   = $tabToInfos->{'code'};

    # -- Index
    sub _getIndex {
        my ($id, $tabDocInfos) = @_;
        if ($tabDocInfos->{'@type'} eq FROMTOTYPE_ESTIMATELINE)
        {
            my $query = '
        SELECT 
            (COUNT(`tbl_f_estimateline`.`DETAILSDEVISAUTO`) + 1) 
        FROM 
            `DETAILSDEVIS` `tbl_f_estimateline` 
        WHERE 
            `tbl_f_estimateline`.`DEVISAUTO` = ' . $db->quote($tabDocInfos->{'tabDetails'}->{'estimate.id'}) . '
            AND `tbl_f_estimateline`.`DETAILSDEVISAUTO` < ' . $db->quote($id) . ';';
    
           return $db->fetchOne($query);
        }
        return undef;
    };
    $tab->{'from'}->{'documentIndex'} = &_getIndex($fromId, $tabFromInfos);
    $tab->{'to'}->{'documentIndex'}   = &_getIndex($toId, $tabToInfos);
    

    # -- Contrat/Devis
    $tab->{'from'}->{'contractId'}     = $tabFromInfos->{'@type'} eq FROMTOTYPE_CONTRACT ? $fromId : undef;
    $tab->{'from'}->{'estimateLineId'} = $tabFromInfos->{'@type'} eq FROMTOTYPE_ESTIMATELINE ? $fromId : undef;
    $tab->{'to'}->{'contractId'}       = $tabToInfos->{'@type'} eq FROMTOTYPE_CONTRACT ? $toId : undef;
    $tab->{'to'}->{'estimateLineId'}   = $tabToInfos->{'@type'} eq FROMTOTYPE_ESTIMATELINE ? $toId : undef;

    # -- État document
    sub _getDocumentStateId {
        my ($tabDocInfos) = @_;
        if (&LOC::Util::in_array($tabDocInfos->{'@type'}, [FROMTOTYPE_CONTRACT, FROMTOTYPE_ESTIMATELINE]))
        {
            return $tabDocInfos->{'tabDetails'}->{'state.id'};
        }
        return undef;
    }
    $tab->{'from'}->{'documentStateId'} = &_getDocumentStateId($tabFromInfos);
    $tab->{'to'}->{'documentStateId'}   = &_getDocumentStateId($tabToInfos);

    # -- Type de document
    $tab->{'from'}->{'documentType'} = $tabFromInfos->{'@type'};
    $tab->{'to'}->{'documentType'}   = $tabToInfos->{'@type'};

    # -- Client
    $tab->{'from'}->{'customerId'}    = (defined $tabFromInfos->{'tabDetails'} ? $tabFromInfos->{'tabDetails'}->{'customer.id'} : undef);
    $tab->{'from'}->{'customerLabel'} = $tabFromInfos->{'customer.name'};
    $tab->{'to'}->{'customerId'}    = (defined $tabToInfos->{'tabDetails'} ? $tabToInfos->{'tabDetails'}->{'customer.id'} : undef);
    $tab->{'to'}->{'customerLabel'} = $tabToInfos->{'customer.name'};

    # -- Provenance / Destination
    if (&LOC::Util::in_array($tabInfos->{'type'}, [TYPE_DELIVERY, TYPE_AGENCIESTRANSFER]))
    {
        # -- Provenance agence
        $tab->{'from'}->{'label'}   = sprintf('Agence %s', $tabFromInfos->{'code'});
        $tab->{'from'}->{'address'} = sprintf('%s %s', $tabFromInfos->{'postalCode'},
                                                       $tabFromInfos->{'city'});
    }
    if (&LOC::Util::in_array($tabInfos->{'type'}, [TYPE_RECOVERY, TYPE_AGENCIESTRANSFER]))
    {
        # -- Destination agence
        $tab->{'to'}->{'label'}   = sprintf('Agence %s', $tabToInfos->{'code'});
        $tab->{'to'}->{'address'} = sprintf('%s %s', $tabToInfos->{'postalCode'},
                                                     $tabToInfos->{'city'});
    }
    if (&LOC::Util::in_array($tabInfos->{'type'}, [TYPE_RECOVERY, TYPE_SITESTRANSFER]))
    { 
        # -- Provenance document
        $tab->{'from'}->{'label'}   = $tabFromInfos->{'site.label'};
        $tab->{'from'}->{'address'} = sprintf('%s %s', $tabFromInfos->{'site.postalCode'},
                                                       $tabFromInfos->{'site.city'});
    }
    if (&LOC::Util::in_array($tabInfos->{'type'}, [TYPE_DELIVERY, TYPE_SITESTRANSFER]))
    { 
        # -- Destination document
        $tab->{'to'}->{'label'}   = $tabToInfos->{'site.label'};
        $tab->{'to'}->{'address'} = sprintf('%s %s', $tabToInfos->{'site.postalCode'},
                                                          $tabToInfos->{'site.city'});
    }

    # -- Amiante
    $tab->{'from'}->{'isAsbestos'} = $tabFromInfos->{'site.isAsbestos'};
    $tab->{'to'}->{'isAsbestos'}   = $tabToInfos->{'site.isAsbestos'};

    # -- Erreurs
    $tab->{'to'}->{'errors'}     = $tabInfos->{'unloading'}->{'errors'};
    $tab->{'to'}->{'isDoable'}   = $tabInfos->{'unloading'}->{'isDoable'};
    $tab->{'to'}->{'isDone'}     = $tabInfos->{'unloading'}->{'isDone'};
    $tab->{'from'}->{'errors'}   = $tabInfos->{'loading'}->{'errors'};
    $tab->{'from'}->{'isDoable'} = $tabInfos->{'loading'}->{'isDoable'};
    $tab->{'from'}->{'isDone'}   = $tabInfos->{'loading'}->{'isDone'};

    # Tableau des mises à jour
    my $tabMain = {};
    my $tabLink = {};
    if (&LOC::Util::in_array($tabInfos->{'type'}, [TYPE_DELIVERY, TYPE_AGENCIESTRANSFER, TYPE_SITESTRANSFER]))
    {
        $tabMain = $tab->{'to'};
    }
    elsif (&LOC::Util::in_array($tabInfos->{'type'}, [TYPE_RECOVERY]))
    {
        $tabMain = $tab->{'from'};
    }
    if (&LOC::Util::in_array($tabInfos->{'type'}, [TYPE_SITESTRANSFER]))
    {
        $tabLink = $tab->{'to'};
    }

    # Insertion ou mise à jour
    my $tabUpdates = {
        'pdt_is_self'     => $tabInfos->{'isSelf'},
        'pdt_agc_id'      => $agencyId,
        'pdt_are_id'      => $tabAgencyInfos->{'area.id'},
        'pdt_type'        => $tabInfos->{'type'},
        'pdt_tor_id'      => $tabInfos->{'order.id'},
        'pdt_mac_id'      => $tabInfos->{'machine.id'},
        'pdt_is_machinewithtelematic' => ($tabInfos->{'machine.id'} ? $isMachineWithTelematics : undef),
        'pdt_mod_id'        => $tabInfos->{'model.id'},
        'pdt_date'          => $tabMain->{'date'},
        'pdt_hour'          => $tabMain->{'hour'},
        'pdt_rct_id'        => $tabMain->{'contractId'},
        'pdt_rel_id'        => $tabMain->{'estimateLineId'},
        'pdt_documentcode'  => $tabMain->{'documentCode'},
        'pdt_documentindex' => $tabMain->{'documentIndex'},
        'pdt_is_asbestosdocument' => $tabMain->{'isAsbestos'},
        'pdt_rct_sta_id'    => $tabMain->{'documentStateId'},
        'pdt_documenttype'  => $tabMain->{'documentType'},
        'pdt_fromlabel'     => $tab->{'from'}->{'label'},
        'pdt_fromaddress'   => $tab->{'from'}->{'address'},
        'pdt_tolabel'       => $tab->{'to'}->{'label'},
        'pdt_toaddress'     => $tab->{'to'}->{'address'},
        'pdt_cus_id'        => $tabMain->{'customerId'},
        'pdt_customerlabel' => $tabMain->{'customerLabel'},
        'pdt_errors'        => $tabMain->{'errors'},
        'pdt_is_doable'     => $tabMain->{'isDoable'},
        'pdt_is_done'       => $tabMain->{'isDone'},
        'pdt_rct_id_link'   => $tabLink->{'contractId'},
        'pdt_is_asbestoslink'  => $tabLink->{'isAsbestos'},
        'pdt_linkcontractcode' => $tabLink->{'documentCode'},
        'pdt_rct_sta_id_link'  => $tabLink->{'contractStateId'},
        'pdt_date_link'     => $tabLink->{'date'},
        'pdt_linkhour'      => $tabLink->{'hour'},
        'pdt_cus_id_link'   => $tabLink->{'customerId'},
        'pdt_linkcustomerlabel' => $tabLink->{'customerLabel'},
        'pdt_linkerrors'    => $tabLink->{'errors'},
        'pdt_is_linkdone'   => $tabLink->{'isDone'},
        'pdt_date_modif*'   => 'Now()'
    };

    if ($db->insertOrUpdate('f_pendingtransport', $tabUpdates, {'pdt_id*' => $id}) == -1)
    {
        return 0;
    }

    return 1;
}


# Function: _formatTraces
# Formate le contenu des traces transport
#
# Parameters:
# string      $side      - Côté demandeur
# string      $typeTrace - Type de la trace
# hashref     $tabInfos  - Tableau d'informations pour le formatage
# LOC::Locale $locale    - Instance de la locale pour les traductions
#
# Returns
# string
sub _formatTraces
{
    my ($side, $typeTrace, $tabInfos, $locale) = @_;


    # Modification de transport
    if ($typeTrace eq 'CHTSP')
    {
        if ($tabInfos->{'type'} eq TYPE_DELIVERY || $tabInfos->{'type'} eq TYPE_RECOVERY)
        {
            if ($tabInfos->{'isSelf'})
            {
                return $locale->t('Enlèvement agence');
            }
            else
            {
                return $locale->t('Standard');
            }
        }
        elsif ($tabInfos->{'type'} eq TYPE_SITESTRANSFER)
        {
            my $code = $tabInfos->{($side eq SIDE_DELIVERY ? 'from' : 'to') . '.tabInfos'}->{'code'};

            if ($tabInfos->{'isSelf'})
            {
                return $locale->t('Reprise sur chantier avec %s', $code);
            }
            else
            {
                return $locale->t('Transfert inter-chantiers avec %s', $code);
            }
        }
    }
    # Agence déléguée
    elsif ($typeTrace eq 'AGDEL' && $tabInfos->{'delegatedAgency.id'})
    {
        my $tabAgencyInfo = &LOC::Agency::getInfos($tabInfos->{'delegatedAgency.id'});
        return $tabAgencyInfo->{'label'};
    }
    # Agence de transfert
    elsif ($typeTrace eq 'AGTSF' && $tabInfos->{'agency.id'})
    {
        my $tabAgencyInfo = &LOC::Agency::getInfos($tabInfos->{'agency.id'});
        return $tabAgencyInfo->{'label'};
    }
    # Transfert inter-agences
    elsif ($typeTrace eq LOC::Log::EventType::AGENCIESTRANSFER)
    {
        if ($tabInfos->{'state.id'} eq STATE_DELETED)
        {
            return $locale->t('Suppression');
        }
        else
        {
            my $fromAgencyLabel      = '';
            my $toAgencyLabel        = '';
            my $delegatedAgencyLabel = '';
            my $date            = $locale->getDateFormat($tabInfos->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC);
            if ($tabInfos->{'from.id'})
            {
                my $tabAgencyInfo = &LOC::Agency::getInfos($tabInfos->{'from.id'});
                $fromAgencyLabel   = $tabAgencyInfo->{'label'};
            }
            if ($tabInfos->{'to.id'})
            {
                my $tabAgencyInfo = &LOC::Agency::getInfos($tabInfos->{'to.id'});
                $toAgencyLabel =  $tabAgencyInfo->{'label'};
            }
            if ($tabInfos->{'delegatedAgency.id'})
            {
                my $tabAgencyInfo = &LOC::Agency::getInfos($tabInfos->{'delegatedAgency.id'});
                $delegatedAgencyLabel =  $tabAgencyInfo->{'label'};
            }
            return sprintf('%s → %s (%s' . ($tabInfos->{'delegatedAgency.id'} ? ' ' . $locale->t('délégué à %s', $delegatedAgencyLabel) . ')' : ')'),
                           $fromAgencyLabel, $toAgencyLabel, $date);

        }
    }

    return '';
}

# Function: _addTraces
# Ajoute les traces transport
#
# Parameters:
# string           $countryId   - Identifiant du pays
# string           $side        - Côté demandeur
# hashref|undef    $tabOldInfos - Ancienne informations du transport
# hashref|undef    $tabNewInfos - Nouvelle informations du transport
# hashref          $tabEndUpds  - Mises à jour de fin
#
# Returns:
# void
sub _addTraces
{
    my ($countryId, $side, $tabOldInfos, $tabNewInfos, $tabEndUpds) = @_;

    # Traductions dans la langue par défaut du pays
    my $tabCountryInfos = &LOC::Country::getInfos($countryId);
    my $locale = &LOC::Locale::getLocale($tabCountryInfos->{'locale.id'}, undef, [POSIX::LC_MESSAGES, POSIX::LC_CTYPE]);
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Côté Livraison
    if ($side eq SIDE_DELIVERY)
    {
        my $documentType = $tabOldInfos->{'to.type'};
        my $documentId   = $tabOldInfos->{'to.id'};

        # - Type de transport
        if (!$tabOldInfos ||
            ($tabOldInfos->{'type'} ne $tabNewInfos->{'type'} ||
                $tabOldInfos->{'isSelf'} != $tabNewInfos->{'isSelf'} ||
                ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER &&
                    $tabNewInfos->{'type'} eq TYPE_SITESTRANSFER &&
                    $tabOldInfos->{'from.id'} != $tabNewInfos->{'from.id'})))
        {
            # Formatage du contenu
            my $oldContent = &_formatTraces($side, 'CHTSP', $tabOldInfos, $locale);
            my $newContent = &_formatTraces($side, 'CHTSP', $tabNewInfos, $locale);

            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDDELIVERY,
                'old'     => {
                    'type'      => $tabOldInfos->{'type'},
                    'isSelf'    => $tabOldInfos->{'isSelf'},
                    'from.id'   => $tabOldInfos->{'from.id'},
                    'from.type' => $tabOldInfos->{'from.type'},
                    'from.code' => $tabOldInfos->{'from.tabInfos'}->{'code'}
                },
                'new'     => {
                    'type'      => $tabNewInfos->{'type'},
                    'isSelf'    => $tabNewInfos->{'isSelf'},
                    'from.id'   => $tabNewInfos->{'from.id'},
                    'from.type' => $tabNewInfos->{'from.type'},
                    'from.code' => $tabNewInfos->{'from.tabInfos'}->{'code'}
                },
                'content' => $oldContent . ' → ' . $newContent
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }

        # - Agence déléguée
        if ($tabOldInfos->{'delegatedAgency.id'} ne $tabNewInfos->{'delegatedAgency.id'})
        {
            # Formatage du contenu
            my $oldContent = &_formatTraces($side, 'AGDEL', $tabOldInfos, $locale);
            my $newContent = &_formatTraces($side, 'AGDEL', $tabNewInfos, $locale);

            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDDELIVERYDELEGAGENCY,
                'old'     => {
                    'agency.id' => $tabOldInfos->{'delegatedAgency.id'}
                },
                'new'     => {
                    'agency.id' => $tabNewInfos->{'delegatedAgency.id'}
                },
                'content' => $oldContent . ' → ' . $newContent
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }

        # - Désynchronisation/resynchronisation OTD
        if ($tabOldInfos->{'isSynchroDisabled'} != $tabNewInfos->{'isSynchroDisabled'})
        {
            my $tabTrace = {
                'schema' => $schemaName,
                'code'   => ($tabOldInfos->{'isSynchroDisabled'} ? LOC::Log::EventType::SYNCOTDDELIVERY : LOC::Log::EventType::UNSYNCOTDDELIVERY),
                'old'    => {
                    'isSynchroDisabled' => $tabOldInfos->{'isSynchroDisabled'}
                },
                'new'    => {
                    'isSynchroDisabled' => $tabNewInfos->{'isSynchroDisabled'}
                }
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }

    }
    # Côté Récupération
    if ($side eq SIDE_RECOVERY)
    {
        my $documentType = $tabOldInfos->{'from.type'};
        my $documentId   = $tabOldInfos->{'from.id'};

        # - Type de transport
        if (!$tabOldInfos ||
            ($tabOldInfos->{'type'} ne $tabNewInfos->{'type'} ||
                $tabOldInfos->{'isSelf'} != $tabNewInfos->{'isSelf'} ||
                ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER &&
                    $tabNewInfos->{'type'} eq TYPE_SITESTRANSFER &&
                    $tabOldInfos->{'to.id'} != $tabNewInfos->{'to.id'})))
        {
            # Formatage du contenu
            my $oldContent = &_formatTraces($side, 'CHTSP', $tabOldInfos, $locale);
            my $newContent = &_formatTraces($side, 'CHTSP', $tabNewInfos, $locale);

            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDRECOVERY,
                'old'     => {
                    'type'    => $tabOldInfos->{'type'},
                    'isSelf'  => $tabOldInfos->{'isSelf'},
                    'to.id'   => $tabOldInfos->{'to.id'},
                    'to.type' => $tabOldInfos->{'to.type'},
                    'to.code' => $tabOldInfos->{'to.tabInfos'}->{'code'}
                },
                'new'     => {
                    'type'    => $tabNewInfos->{'type'},
                    'isSelf'  => $tabNewInfos->{'isSelf'},
                    'to.id'   => $tabNewInfos->{'to.id'},
                    'to.type' => $tabNewInfos->{'to.type'},
                    'to.code' => $tabNewInfos->{'to.tabInfos'}->{'code'}
                },
                'content' => $oldContent . ' → ' . $newContent
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }

        # - Agence déléguée
        if ($tabOldInfos->{'delegatedAgency.id'} ne $tabNewInfos->{'delegatedAgency.id'})
        {
            # Formatage du contenu
            my $oldContent = &_formatTraces($side, 'AGDEL', $tabOldInfos, $locale);
            my $newContent = &_formatTraces($side, 'AGDEL', $tabNewInfos, $locale);

            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDRECOVERYDELEGAGENCY,
                'old'     => {
                    'agency.id' => $tabOldInfos->{'delegatedAgency.id'}
                },
                'new'     => {
                    'agency.id' => $tabNewInfos->{'delegatedAgency.id'}
                },
                'content' => $oldContent . ' → ' . $newContent
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }

        # - Agence de transfert
        if ($tabOldInfos->{'transferToAgency.id'} ne $tabNewInfos->{'transferToAgency.id'})
        {
            # Formatage du contenu
            my $oldContent = &_formatTraces($side, 'AGTSF', {'agency.id' => $tabOldInfos->{'transferToAgency.id'}}, $locale);
            my $newContent = &_formatTraces($side, 'AGTSF', {'agency.id' => $tabNewInfos->{'transferToAgency.id'}}, $locale);

            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDRECOVERYTSFAGENCY,
                'old'     => {
                    'agency.id'   => $tabOldInfos->{'transferToAgency.id'}
                },
                'new'     => {
                    'agency.id'   => $tabNewInfos->{'transferToAgency.id'}
                },
                'content' => $oldContent . ' → ' . $newContent
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }

        # - Désynchronisation/resynchronisation OTD
        if ($tabOldInfos->{'isSynchroDisabled'} != $tabNewInfos->{'isSynchroDisabled'})
        {
            my $tabTrace = {
                'schema' => $schemaName,
                'code'   => ($tabOldInfos->{'isSynchroDisabled'} ? LOC::Log::EventType::SYNCOTDRECOVERY : LOC::Log::EventType::UNSYNCOTDRECOVERY),
                'old'    => {
                    'isSynchroDisabled' => $tabOldInfos->{'isSynchroDisabled'}
                },
                'new'    => {
                    'isSynchroDisabled' => $tabNewInfos->{'isSynchroDisabled'}
                }
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }
    }
}


# Function: _calculateDoability
# Calcul de la faisabilité
#
# Parameters:
# string  $countryId - Identifiant du pays
# hashref $tabInfos  - Informations sur le transport
#
# Returns:
# void
sub _calculateDoability
{
    my ($countryId, $tabInfos) = @_;

    # Réinitialisation
    $tabInfos->{'loading'}->{'isDoable'} = 0;
    $tabInfos->{'loading'}->{'errors'} = 0;
    $tabInfos->{'unloading'}->{'isDoable'} = 0;
    $tabInfos->{'unloading'}->{'errors'} = 0;


    # Transport toujours non faisable
    # - pour une ligne de devis
    # - pour un contrat annulé
    if ($tabInfos->{'isLocked'} ||
        $tabInfos->{'from.type'} eq FROMTOTYPE_ESTIMATELINE ||
        $tabInfos->{'to.type'} eq FROMTOTYPE_ESTIMATELINE ||
        $tabInfos->{'to.tabInfos'}->{'tabDetails'}->{'state.id'} eq LOC::Contract::Rent::STATE_CANCELED ||
        $tabInfos->{'from.tabInfos'}->{'tabDetails'}->{'state.id'} eq LOC::Contract::Rent::STATE_CANCELED)
    {
        return;
    }


    #---------------------------------------------------------
    # Calcul des erreurs de faisabilité
    #---------------------------------------------------------
    if ($tabInfos->{'type'} eq TYPE_DELIVERY)
    {
        my $tabCttInfos = $tabInfos->{'to.tabInfos'};
        my $errors      = 0;

        if ($tabInfos->{'unloading'}->{'isDone'} == ACTION_USERDONE)
        {
            # Calcul des erreurs pour l'annulation de la réalisation de la livraison

            # Le transport aller est facturé
            if ($tabCttInfos->{'tabDetails'}->{'state.id'} eq LOC::Contract::Rent::STATE_CONTRACT &&
                $tabCttInfos->{'invoiceState'} ne LOC::Contract::Rent::INVOICESTATE_NONE)
            {
                $errors = $errors | ERROR_INVOICEDDELIVERY;
            }
        }
        elsif ($tabInfos->{'unloading'}->{'isDone'} == ACTION_NOTDONE)
        {
            # Calcul des erreurs pour la réalisation de la livraison

            # Y'a-t-il une machine ?
            if (!$tabCttInfos->{'machine.id'})
            {
                $errors = $errors | ERROR_NOMACHINE;
            }
            # Machine visible en cours de transfert sur l'agence
            elsif ($tabCttInfos->{'machine.tabInfos'}->{'state.id'} eq LOC::Machine::STATE_TRANSFER)
            {
                $errors = $errors | ERROR_MACHINENOTTRANSFERED;
            }

            # Machine supplémentaire
            if ($tabCttInfos->{'isAddMachine'} && !$tabCttInfos->{'addMachineLnkCtt.id'})
            {
                $errors = $errors | ERROR_NOMAINCONTRACT;
            }

            # Vérifications Pro forma
            # - Pro forma non générée
            if ($tabCttInfos->{'tabDetails'}->{'isProformaNotGen'})
            {
                $errors = $errors | ERROR_PFNOTGEN;
            }
            # - Pro forma non à jour
            if ($tabCttInfos->{'tabDetails'}->{'isProformaNotUpToDate'})
            {
                $errors = $errors | ERROR_PFNOTUPTODATE;
            }
            # - Pro forma non finalisée
            if ($tabCttInfos->{'tabDetails'}->{'isProformaNotPayed'})
            {
                $errors = $errors | ERROR_PFNOTPAYED;
            }

            # RE loc/transp
            if ($tabCttInfos->{'tabDetails'}->{'isRentTariffError'})
            {
                $errors = $errors | ERROR_RENTTARIFF;
            }
            if ($tabCttInfos->{'tabDetails'}->{'isTrspTariffError'})
            {
                $errors = $errors | ERROR_TRSPTARIFF;
            }

            # Client avec ouverture de compte en attente
            if ($tabCttInfos->{'tabDetails'}->{'isCustomerNotVerified'})
            {
                $errors = $errors | ERROR_CUSTNOTVERIF;
            }
        }

        $tabInfos->{'unloading'}->{'errors'} = $errors;
    }
    elsif ($tabInfos->{'type'} eq TYPE_RECOVERY)
    {
        my $tabCttInfos = $tabInfos->{'from.tabInfos'};
        my $errors      = 0;

        if ($tabInfos->{'loading'}->{'isDone'} == ACTION_USERDONE)
        {
            # Calcul des erreurs pour l'annulation de la réalisation de la récupération

            if ($tabCttInfos->{'tabDetails'}->{'state.id'} eq LOC::Contract::Rent::STATE_STOPPED &&
                $tabCttInfos->{'tabDetails'}->{'id'} == $tabCttInfos->{'machine.tabInfos'}->{'contract.id'})
            {
                # Machine mise en visibilité sur une autre agence
                if ($tabCttInfos->{'machine.tabInfos'}->{'visibilityMode'} == LOC::Machine::VISIBILITYMODE_MACHINE)
                {
                    $errors = $errors | ERROR_VISIBLEMACHINE;
                }
                # Machine transférée dans une autre agence
                if ($tabInfos->{'transferToAgency.id'})
                {
                    $errors = $errors | ERROR_TRANSFERREDMACHINE;
                }
            }
        }
        elsif ($tabInfos->{'loading'}->{'isDone'} == ACTION_NOTDONE)
        {
            # Calcul des erreurs pour la réalisation de la récupération

            # Machine bloquée sur chantier
            if ($tabCttInfos->{'machine.id'} && $tabCttInfos->{'machine.tabInfos'}->{'isSiteBlocked'})
            {
                $errors = $errors | ERROR_MACHINESITEBLOCKED;
            }

            # Machine supplémentaire
            if ($tabCttInfos->{'isAddMachine'} && !$tabCttInfos->{'addMachineLnkCtt.id'})
            {
                $errors = $errors | ERROR_NOMAINCONTRACT;
            }

            # La livraison n'a pas été réalisée
            if (!$tabCttInfos->{'isFinalAllocation'} ||
                ($tabCttInfos->{'machine.tabInfos'}->{'state.id'} ne LOC::Machine::STATE_RENTED &&
                 $tabCttInfos->{'machine.tabInfos'}->{'state.id'} ne LOC::Machine::STATE_RECOVERY))
            {
                $errors = $errors | ERROR_DELIVERYNOTDONE;
            }

            # Vérifications Pro forma
            # - Pro forma de prolongation non finalisée
            if ($tabCttInfos->{'tabDetails'}->{'isProformaExtNotPayed'})
            {
                $errors = $errors | ERROR_PFNOTPAYED;
            }
        }

        $tabInfos->{'loading'}->{'errors'} = $errors;
    }
    elsif ($tabInfos->{'type'} eq TYPE_SITESTRANSFER)
    {
        # Calcul des erreurs pour la réalisation du chargement d'un transfert
        my $tabCttInfos = $tabInfos->{'from.tabInfos'};
        my $errors      = 0;

        if ($tabInfos->{'loading'}->{'isDone'} == ACTION_USERDONE)
        {
            # Calcul des erreurs pour l'annulation de la réalisation du chargement d'un transfert

        }
        elsif ($tabInfos->{'loading'}->{'isDone'} == ACTION_NOTDONE)
        {
            # Machine bloquée sur chantier
            if ($tabCttInfos->{'machine.tabInfos'}->{'isSiteBlocked'})
            {
                $errors = $errors | ERROR_MACHINESITEBLOCKED;
            }

            # Machine supplémentaire
            if ($tabCttInfos->{'isAddMachine'} && !$tabCttInfos->{'addMachineLnkCtt.id'})
            {
                $errors = $errors | ERROR_NOMAINCONTRACT;
            }

            # La livraison n'a pas été réalisée
            if (!$tabInfos->{'loading'}->{'isDone'} &&
                (!$tabCttInfos->{'isFinalAllocation'} ||
                 ($tabCttInfos->{'machine.tabInfos'}->{'state.id'} ne LOC::Machine::STATE_RENTED &&
                  $tabCttInfos->{'machine.tabInfos'}->{'state.id'} ne LOC::Machine::STATE_RECOVERY)))
            {
                $errors = $errors | ERROR_DELIVERYNOTDONE;
            }

            # Vérifications Pro forma
            # - Pro forma de prolongation non finalisée
            if ($tabCttInfos->{'tabDetails'}->{'isProformaExtNotPayed'})
            {
                $errors = $errors | ERROR_PFNOTPAYED;
            }
        }

        $tabInfos->{'loading'}->{'errors'} = $errors;



        my $tabCttInfos = $tabInfos->{'to.tabInfos'};
        my $errors      = 0;

        if ($tabInfos->{'unloading'}->{'isDone'} == ACTION_USERDONE)
        {
            # Calcul des erreurs pour l'annulation de la réalisation du déchargement d'un transfert

            # Transfert inter-chantiers existant sur la récupération
            if ($tabCttInfos->{'tabDetails'}->{'state.id'} eq LOC::Contract::Rent::STATE_CONTRACT &&
                $tabCttInfos->{'tabDetails'}->{'recovery.tabInfos'}->{'type'} eq TYPE_SITESTRANSFER)
            {
                $errors = $errors | ERROR_RECOVERYTRANSFER;
            }
        }
        elsif ($tabInfos->{'unloading'}->{'isDone'} == ACTION_NOTDONE)
        {
            # Calcul des erreurs pour la réalisation du déchargement d'un transfert

            # Machine supplémentaire
            if ($tabCttInfos->{'isAddMachine'} && !$tabCttInfos->{'addMachineLnkCtt.id'})
            {
                $errors = $errors | ERROR_NOMAINCONTRACT;
            }

            # Vérifications Pro forma
            # - Pro forma non générée
            if ($tabCttInfos->{'tabDetails'}->{'isProformaNotGen'})
            {
                $errors = $errors | ERROR_PFNOTGEN;
            }
            # - Pro forma non à jour
            if ($tabCttInfos->{'tabDetails'}->{'isProformaNotUpToDate'})
            {
                $errors = $errors | ERROR_PFNOTUPTODATE;
            }
            # - Pro forma non finalisée
            if ($tabCttInfos->{'tabDetails'}->{'isProformaNotPayed'})
            {
                $errors = $errors | ERROR_PFNOTPAYED;
            }

            # RE loc/transp
            if ($tabCttInfos->{'tabDetails'}->{'isRentTariffError'})
            {
                $errors = $errors | ERROR_RENTTARIFF;
            }
            if ($tabCttInfos->{'tabDetails'}->{'isTrspTariffError'})
            {
                $errors = $errors | ERROR_TRSPTARIFF;
            }

            # Client avec ouverture de compte en attente
            if ($tabCttInfos->{'tabDetails'}->{'isCustomerNotVerified'})
            {
                $errors = $errors | ERROR_CUSTNOTVERIF;
            }

        }

        $tabInfos->{'unloading'}->{'errors'} = $errors;


        # Ajout de l'erreur "sur l'autre côté" du transfert
        if ($tabInfos->{'unloading'}->{'isDone'} == ACTION_NOTDONE &&
            $tabInfos->{'loading'}->{'isDone'} == ACTION_NOTDONE)
        {
            if ($tabInfos->{'loading'}->{'errors'} > 0)
            {
                $tabInfos->{'unloading'}->{'errors'} = $tabInfos->{'unloading'}->{'errors'} | ERROR_RECOVERY;
            }
            elsif ($tabInfos->{'unloading'}->{'errors'} > 0)
            {
                $tabInfos->{'loading'}->{'errors'} = $tabInfos->{'loading'}->{'errors'} | ERROR_DELIVERY;
            }
        }
    }


    #---------------------------------------------------------
    # Calcul de la faisabilité
    #---------------------------------------------------------
    if ($tabInfos->{'type'} eq TYPE_DELIVERY)
    {
        my $tabCttInfos = $tabInfos->{'to.tabInfos'};

        if ($tabInfos->{'unloading'}->{'isDone'} == ACTION_USERDONE)
        {
            # Calcul de la faisabilité de l'annulation
            # - Contrat à l'état "Contrat"
            # - Machine à l'état "Louée"
            # - Machine attribuée sur le contrat
            # - Etat de facturation du contrat à "Non facturé"
            # - Machine non bloquée sur le chantier
            $tabInfos->{'unloading'}->{'isDoable'} =
                ($tabInfos->{'unloading'}->{'errors'} == 0 &&
                 $tabCttInfos->{'tabDetails'}->{'state.id'} eq LOC::Contract::Rent::STATE_CONTRACT &&
                 $tabCttInfos->{'machine.tabInfos'}->{'state.id'} eq LOC::Machine::STATE_RENTED &&
                 $tabCttInfos->{'isFinalAllocation'} &&
                 $tabCttInfos->{'invoiceState'} eq LOC::Contract::Rent::INVOICESTATE_NONE &&
                 !$tabCttInfos->{'machine.tabInfos'}->{'isSiteBlocked'} ? 1 : 0);
        }
        elsif ($tabInfos->{'unloading'}->{'isDone'} == ACTION_NOTDONE)
        {
            # Calcul de la faisabilité de la réalisation
            # - Aucune erreur de déchargement
            # - Contrat à l'état "Pré-contrat"
            # - Machine à l'état "En livraison"
            # - Machine attribuée sur le contrat
            $tabInfos->{'unloading'}->{'isDoable'} =
                ($tabInfos->{'unloading'}->{'errors'} == 0 &&
                 $tabCttInfos->{'tabDetails'}->{'state.id'} eq LOC::Contract::Rent::STATE_PRECONTRACT &&
                 $tabCttInfos->{'machine.id'} &&
                 $tabCttInfos->{'machine.tabInfos'}->{'state.id'} eq LOC::Machine::STATE_DELIVERY &&
                 $tabCttInfos->{'isFinalAllocation'} ? 1 : 0);
        }
    }
    elsif ($tabInfos->{'type'} eq TYPE_RECOVERY)
    {
        my $tabCttInfos = $tabInfos->{'from.tabInfos'};

        if ($tabInfos->{'loading'}->{'isDone'} == ACTION_USERDONE)
        {
            # Calcul de la faisabilité de l'annulation
            # - Contrat à l'état "Arrêt"
            # - Machine à l'état "Disponible" ou "Vérification"
            # - Machine attribuée sur le contrat
            # - Agence de la machine et du contrat identique
            $tabInfos->{'loading'}->{'isDoable'} =
                ($tabInfos->{'loading'}->{'errors'} == 0 &&
                 $tabCttInfos->{'tabDetails'}->{'state.id'} eq LOC::Contract::Rent::STATE_STOPPED &&
                 ($tabCttInfos->{'machine.tabInfos'}->{'state.id'} eq LOC::Machine::STATE_AVAILABLE ||
                  $tabCttInfos->{'machine.tabInfos'}->{'state.id'} eq LOC::Machine::STATE_CHECK) &&
                 $tabCttInfos->{'isFinalAllocation'} &&
                 $tabCttInfos->{'invoiceState'} ne LOC::Contract::Rent::INVOICESTATE_FINAL &&
                 $tabCttInfos->{'machine.tabInfos'}->{'agency.id'} eq $tabInfos->{'agency.id'} ? 1 : 0);
        }
        elsif ($tabInfos->{'loading'}->{'isDone'} == ACTION_NOTDONE)
        {
            # Calcul de la faisabilité de la réalisation
            # - Aucune erreur de chargement
            # - Contrat à l'état "Arrêt"
            # - Machine à l'état "Récupération"
            # - Machine non bloquée sur le chantier
            # - Machine attribuée sur le contrat
            $tabInfos->{'loading'}->{'isDoable'} =
                ($tabInfos->{'loading'}->{'errors'} == 0 &&
                 $tabCttInfos->{'tabDetails'}->{'state.id'} eq LOC::Contract::Rent::STATE_STOPPED &&
                 $tabCttInfos->{'machine.id'} &&
                 $tabCttInfos->{'machine.tabInfos'}->{'state.id'} eq LOC::Machine::STATE_RECOVERY &&
                 !$tabCttInfos->{'machine.tabInfos'}->{'isSiteBlocked'} &&
                 $tabCttInfos->{'isFinalAllocation'} ? 1 : 0);
        }
    }
    elsif ($tabInfos->{'type'} eq TYPE_SITESTRANSFER)
    {
        # pour le chargement
        if ($tabInfos->{'loading'}->{'isDone'} == ACTION_USERDONE)
        {
            # Calcul de la faisabilité de l'annulation
            # - Contrat de provenance à l'état "Arrêt"
            # - Machine du contrat de destination à l'état :
            # -- "En livraison" OU
            # -- "Louée" et Non bloquée sur chantier de destination
            # - Machine attribuée sur le contrat de destination
            $tabInfos->{'loading'}->{'isDoable'} =
                ($tabInfos->{'loading'}->{'errors'} == 0 &&
                 $tabInfos->{'from.tabInfos'}->{'tabDetails'}->{'state.id'} eq LOC::Contract::Rent::STATE_STOPPED &&
                 ($tabInfos->{'to.tabInfos'}->{'machine.tabInfos'}->{'state.id'} eq LOC::Machine::STATE_DELIVERY ||
                  ($tabInfos->{'to.tabInfos'}->{'machine.tabInfos'}->{'state.id'} eq LOC::Machine::STATE_RENTED &&
                   !$tabInfos->{'to.tabInfos'}->{'machine.tabInfos'}->{'isSiteBlocked'})) &&
                 $tabInfos->{'from.tabInfos'}->{'invoiceState'} ne LOC::Contract::Rent::INVOICESTATE_FINAL &&
                 $tabInfos->{'to.tabInfos'}->{'isFinalAllocation'} ? 1 : 0);
        }
        elsif ($tabInfos->{'loading'}->{'isDone'} == ACTION_NOTDONE)
        {
            # Calcul de la faisabilité de la réalisation
            # - Aucune erreur de chargement
            # - Contrat de provenance à l'état "Arrêt"
            # - Machine du contrat de provenance à l'état "Récupération"
            # - Machine non bloquée sur le chantier de provenance
            # - Machine attribuée sur le contrat de provenance
            $tabInfos->{'loading'}->{'isDoable'} =
                ($tabInfos->{'loading'}->{'errors'} == 0 &&
                 $tabInfos->{'from.tabInfos'}->{'tabDetails'}->{'state.id'} eq LOC::Contract::Rent::STATE_STOPPED &&
                 $tabInfos->{'from.tabInfos'}->{'machine.id'} &&
                 $tabInfos->{'from.tabInfos'}->{'machine.tabInfos'}->{'state.id'} eq LOC::Machine::STATE_RECOVERY &&
                 !$tabInfos->{'from.tabInfos'}->{'machine.tabInfos'}->{'isSiteBlocked'} &&
                 $tabInfos->{'from.tabInfos'}->{'isFinalAllocation'} ? 1 : 0);
        }


        # pour le déchargement
        if ($tabInfos->{'unloading'}->{'isDone'} == ACTION_USERDONE)
        {
            # Calcul de la faisabilité de l'annulation
            # - Contrat de destination à l'état "Contrat"
            # - Machine du contrat de destination à l'état "Louée"
            # - Machine attribuée sur le contrat de destination
            # - Machine non bloquée sur le chantier de destination
            $tabInfos->{'unloading'}->{'isDoable'} =
                ($tabInfos->{'unloading'}->{'errors'} == 0 &&
                 $tabInfos->{'to.tabInfos'}->{'tabDetails'}->{'state.id'} eq LOC::Contract::Rent::STATE_CONTRACT &&
                 $tabInfos->{'to.tabInfos'}->{'machine.tabInfos'}->{'state.id'} eq LOC::Machine::STATE_RENTED &&
                 !$tabInfos->{'to.tabInfos'}->{'machine.tabInfos'}->{'isSiteBlocked'} &&
                 $tabInfos->{'to.tabInfos'}->{'isFinalAllocation'} &&
                 $tabInfos->{'to.tabInfos'}->{'invoiceState'} eq LOC::Contract::Rent::INVOICESTATE_NONE &&
                 $tabInfos->{'to.tabInfos'}->{'tabDetails'}->{'recovery.tabInfos'}->{'type'} ne TYPE_SITESTRANSFER ? 1 : 0);
        }
        elsif ($tabInfos->{'unloading'}->{'isDone'} == ACTION_NOTDONE)
        {
            # Calcul de la faisabilité de la réalisation
            # - Aucune erreur de déchargement
            # - Chargement de la machine non effectuée
            # - Contrat de provenance à l'état "Arrêt
            # - Machine non bloquée sur le contrat de provenance
            # OU
            # -- Chargement de la machine effectuée
            # -- Contrat de destination à l'état "Pré-contrat"
            # -- Machine à l'état "En livraison" sur le contrat de destination
            # -- Machine attribuée sur le contrat de destination
            $tabInfos->{'unloading'}->{'isDoable'} =
                (($tabInfos->{'unloading'}->{'errors'} == 0 &&
                  $tabInfos->{'loading'}->{'isDone'} == ACTION_NOTDONE &&
                  $tabInfos->{'from.tabInfos'}->{'tabDetails'}->{'state.id'} eq LOC::Contract::Rent::STATE_STOPPED &&
                  $tabInfos->{'from.tabInfos'}->{'machine.id'} &&
                  !$tabInfos->{'from.tabInfos'}->{'machine.tabInfos'}->{'isSiteBlocked'}) ||

                 ($tabInfos->{'loading'}->{'isDone'} != ACTION_NOTDONE &&
                  $tabInfos->{'to.tabInfos'}->{'tabDetails'}->{'state.id'} eq LOC::Contract::Rent::STATE_PRECONTRACT &&
                  $tabInfos->{'to.tabInfos'}->{'machine.id'} &&
                  $tabInfos->{'to.tabInfos'}->{'machine.tabInfos'}->{'state.id'} eq LOC::Machine::STATE_DELIVERY &&
                  $tabInfos->{'to.tabInfos'}->{'isFinalAllocation'}) ? 1 : 0);
        }
    }
    elsif ($tabInfos->{'type'} eq TYPE_AGENCIESTRANSFER)
    {
        if ($tabInfos->{'unloading'}->{'isDone'} == ACTION_USERDONE)
        {
            # Calcul de la faisabilité de l'annulation
            # - Le modèle de machine doit être réservable
            # - Dans tous les autres cas, le transfert est bloqué (isLocked) donc la faisabilité doit déjà être à 0
            $tabInfos->{'unloading'}->{'isDoable'} = ($tabInfos->{'machine.tabInfos'}->{'model.isRentable'} ? 1 : 0);
            $tabInfos->{'loading'}->{'isDoable'} = $tabInfos->{'unloading'}->{'isDoable'};
        }
        elsif ($tabInfos->{'unloading'}->{'isDone'} == ACTION_NOTDONE)
        {
            # Calcul de la faisabilité de la réalisation
            # - Pas d'erreur
            # - La machine doit etre dans l'état "En transfert"
            # - La machine doit être sur l'agence de départ
            $tabInfos->{'unloading'}->{'isDoable'} =
                ($tabInfos->{'machine.tabInfos'}->{'state.id'} eq LOC::Machine::STATE_TRANSFER  &&
                 $tabInfos->{'machine.tabInfos'}->{'agency.id'} eq $tabInfos->{'from.id'} ? 1 : 0);
            $tabInfos->{'loading'}->{'isDoable'} = $tabInfos->{'unloading'}->{'isDoable'};
        }
    }
}


# Function: _doUnloading
# Réalise le déchargement
#
# Parameters:
# LOC::Db::Adapter $db               - Connexion à la base de données
# string           $countryId        - Pays
# hashref          $tabOldInfos      - Informations sur le transport
# string           $documentType     - Type du document
# string           $date             - Date du déchargement
# int              $userId           - Utilisateur responsable
# hashref          $tabEndUpds       - Mises à jour de fin
#
# Returns:
# bool
sub _doUnloading
{
    my ($db, $countryId, $tabOldInfos, $documentType, $date, $userId, $tabEndUpds) = @_;

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # nouvelles infos transport
    my $tabNewInfos = &LOC::Util::clone($tabOldInfos);

    # Mise à jour de la machine
    my $tabUpdates = {
        'state.id'    => LOC::Machine::STATE_RENTED,
        'contract.id' => $tabOldInfos->{'to.id'},
        'agency.id'   => $tabOldInfos->{'to.tabInfos'}->{'agency.id'}
    };

    if (!&LOC::Machine::update($countryId, $tabOldInfos->{'machine.id'}, $tabUpdates, $userId, {}, undef, $tabEndUpds))
    {
        return 0;
    }

    # Mise à jour du contrat
    # - état contrat à CON02
    if ($db->update('CONTRAT', {'ETCODE'          => LOC::Contract::Rent::STATE_CONTRACT,
                                'LIETCONTRATAUTO' => substr(LOC::Contract::Rent::STATE_CONTRACT, -2) * 1},
                               {'CONTRATAUTO*' => $tabOldInfos->{'to.id'}}, $schemaName) == -1)
    {
        return 0;
    }

    # Mise à jour du transport
    $tabNewInfos->{'to.tabInfos'}->{'machine.tabInfos'}->{'state.id'} = LOC::Machine::STATE_RENTED;
    $tabNewInfos->{'to.tabInfos'}->{'status'}                         = LOC::Contract::Rent::STATE_CONTRACT;
    $tabNewInfos->{'to.tabInfos'}->{'tabDetails'}->{'state.id'}       = LOC::Contract::Rent::STATE_CONTRACT;
    $tabNewInfos->{'unloading'}->{'isDone'}                           = ACTION_USERDONE;
    if ($tabOldInfos->{'type'} ne TYPE_SITESTRANSFER)
    {
        $tabNewInfos->{'loading'}->{'isDone'} = ACTION_USERDONE;
    }
    if (defined $date)
    {
        $tabNewInfos->{'unloading'}->{'date'} = $date;
    }
    else
    {
        $tabNewInfos->{'unloading'}->{'date'} = 'now';
    }

    if (!&_set($db, $countryId, $tabNewInfos, {}))
    {
        return 0;
    }

    # Mise à jour de la récupération sur le même document
    my $tabRecoveryInfos = &getDocumentRecoveryInfos($countryId, $documentType, $tabOldInfos->{'to.id'},
                                                     GETINFOS_FROM | GETINFOS_TO | GETINFOS_MACHINE,
                                                     {'getFromInfosFlags' => GETFROMTOINFOS_DETAILS,
                                                      'getToInfosFlags'   => GETFROMTOINFOS_DETAILS});

    if (!&_set($db, $countryId, $tabRecoveryInfos, {}))
    {
        return 0;
    }

    # Mise à jour de la classe du client si celui était un prospect
    my $tabCustomerInfos = &LOC::Customer::getInfos($countryId, $tabOldInfos->{'to.tabInfos'}->{'tabDetails'}->{'customer.id'});
    if ($tabCustomerInfos->{'class'} eq 'P')
    {
        my $tabUpdates = {
            'class' => 'C'
        };
        if ($tabCustomerInfos->{'potential'} eq '')
        {
            $tabUpdates->{'potential'} = 'C';
        }
        if(!&LOC::Customer::update($countryId, $tabCustomerInfos->{'id'}, $tabUpdates, $userId, undef, {}, $tabEndUpds))
        {
            return 0;
        }
    }

    # Mise à jour des caractéristiques Kimoce de la machine si livraison standard
    if ($tabOldInfos->{'type'} eq TYPE_DELIVERY)
    {
        &LOC::EndUpdates::updateKimoceMachine($tabEndUpds, $tabRecoveryInfos->{'machine.tabInfos'}->{'parkNumber'},
                                              LOC::EndUpdates::KIMUPD_FEATURES);
    }

    # Mise à jour des alertes "Transports en cours" et "Enlèvements en attente"
    &_updateAlerts($tabOldInfos, $tabNewInfos, {}, $tabEndUpds);

    # Si la réalisation du déchargement a changé
    if ($tabOldInfos->{'unloading'}->{'isDone'} != $tabNewInfos->{'unloading'}->{'isDone'})
    {
        # Mise à jour des alertes "Précontrats"
        # - Décrémente l'alerte "Pré-contrats effectués"
        &LOC::EndUpdates::updateAlert($tabEndUpds, $tabNewInfos->{'agency.id'}, LOC::Alert::PRECONTRACTS, -1);

        # Mise à jour des traces
        my $tabTrace = {
            'schema' => $schemaName,
            'code'   => LOC::Log::EventType::DODELIVERY
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $tabOldInfos->{'to.id'}, $tabTrace);
    }



    return 1;
}


# Function: _doLoading
# Réalise le chargement
#
# Parameters:
# LOC::Db::Adapter $db              - Connexion à la base de données
# string           $countryId       - Pays
# hashref          $tabOldInfos     - Informations sur le transport
# string           $date            - Date de chargement (yyyy-mm-dd hh:mm:ss)
# int              $userId          - Utilisateur responsable
# hashref          $tabEndUpds      - Mises à jour de fin
#
# Returns:
# bool
sub _doLoading
{
    my ($db, $countryId, $tabOldInfos, $date, $userId, $tabEndUpds) = @_;

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # nouvelles infos du transport
    my $tabNewInfos = &LOC::Util::clone($tabOldInfos);

    #Infos de la machine
    my $tabMachineInfos = &LOC::Machine::getInfos($countryId, $tabOldInfos->{'machine.id'});

    # Mise à jour de la machine
    my $tabMachineUpdates = {};
    my $stateMachine = $tabMachineInfos->{'state.id'};

    # - si c'est une récupération normale - la machine passe en vérification
    if ($tabOldInfos->{'type'} eq TYPE_RECOVERY)
    {
        $stateMachine = LOC::Machine::STATE_CHECK;

        # mise à jour tabInfos pour le calcul de la faisabilité future
        $tabNewInfos->{'from.tabInfos'}->{'machine.tabInfos'}->{'state.id'}  = $stateMachine;

        # on met à jour l'agence de destination
        $tabMachineUpdates->{'agency.id'} = $tabOldInfos->{'to.id'};
        $tabNewInfos->{'from.tabInfos'}->{'machine.tabInfos'}->{'agency.id'} = $tabOldInfos->{'to.id'};
    }
    # - sinon c'est un transfert - la machine passe en livraison sur l'autre contrat
    elsif ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER)
    {
        $stateMachine = LOC::Machine::STATE_AVAILABLE;
    }

    $tabMachineUpdates->{'state.id'} = $stateMachine;

    if (!&LOC::Machine::update($countryId, $tabOldInfos->{'machine.id'}, $tabMachineUpdates, $userId, {}, undef, $tabEndUpds))
    {
        return 0;
    }


    # Mise à jour du transport
    $tabNewInfos->{'loading'}->{'isDone'} = ACTION_USERDONE;
    if (defined $date)
    {
        $tabNewInfos->{'loading'}->{'date'} = $date;
    }
    else
    {
        $tabNewInfos->{'loading'}->{'date'} = 'now';
    }
    if ($tabOldInfos->{'type'} ne TYPE_SITESTRANSFER)
    {
        $tabNewInfos->{'unloading'}->{'isDone'} = ACTION_USERDONE;
    }

    if (!&_set($db, $countryId, $tabNewInfos, {}))
    {
        return 0;
    }


    # Mise à jour du contrat de livraison si transfert
    if ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER)
    {
        # Mise à jour tabInfos pour le calcul de la faisabilité future
        $tabNewInfos->{'to.tabInfos'}->{'isFinalAllocation'}  = 1;

        my $tabContractUpdates = {
            'machine.id'        => $tabOldInfos->{'machine.id'},
            'isFinalAllocation' => 1
        };

        if (!&LOC::Contract::Rent::update($countryId, $tabOldInfos->{'to.id'}, $tabContractUpdates, $userId, undef, $tabEndUpds))
        {
            return 0;
        }
    }

    # Mise à jour des caractéristiques Kimoce de la machine si récupération standard (voir FA-13156)
    if ($tabOldInfos->{'type'} eq TYPE_RECOVERY)
    {
        &LOC::EndUpdates::updateKimoceMachine($tabEndUpds, $tabMachineInfos->{'parkNumber'},
                                              LOC::EndUpdates::KIMUPD_FEATURES);
    }

    # Mise à jour des alertes "Transports en cours" et "Enlèvements en attente"
    &_updateAlerts($tabOldInfos, $tabNewInfos, {}, $tabEndUpds);

    # Si la réalisation du chargement a changé
    if ($tabOldInfos->{'loading'}->{'isDone'} != $tabNewInfos->{'loading'}->{'isDone'})
    {
        # Mise à jour des traces
        my $tabTrace = {
            'schema' => $schemaName,
            'code'   => LOC::Log::EventType::DORECOVERY
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $tabOldInfos->{'from.id'}, $tabTrace);
    }

    # Mise à jour des infos d'origine
    $tabOldInfos->{'loading'}     = $tabNewInfos->{'loading'};
    $tabOldInfos->{'unloading'}   = $tabNewInfos->{'unloading'};
    $tabOldInfos->{'to.tabInfos'} = $tabNewInfos->{'to.tabInfos'};
    return 1;
}


# Function: _doAgenciesTransfer
# Réalisation d'un transfert inter-agence
#
# Parameters:
# LOC::Db::Adapter $db               - Connexion à la base de données
# string           $countryId        - Pays
# hashref          $tabOldInfos      - Informations sur le transport
# string           $date             - Date du déchargement
# int              $userId           - Utilisateur responsable
# hashref          $tabEndUpds       - Mises à jour de fin
#
# Returns:
# bool
sub _doAgenciesTransfer
{
    my ($db, $countryId, $tabOldInfos, $date, $userId, $tabEndUpds) = @_;

    # nouvelles infos transport
    my $tabNewInfos = &LOC::Util::clone($tabOldInfos);

    my $tabOldMachineInfos = $tabOldInfos->{'machine.tabInfos'};

    # Mise à jour de la machine
    my $tabUpdates = {
        'state.id'       => ($tabOldMachineInfos->{'isAffectedOnPrecontract'} ?
                                LOC::Machine::STATE_DELIVERY : LOC::Machine::STATE_CHECK),
        'agency.id'      => $tabOldInfos->{'to.id'},
        'isTransferDone' => 1 # Anticipation du pointage pour l'enregistrement du changement d'état
    };

    # Mise à jour de la machine
    # - Etat machine à MAC07 ou MAC12
    # - Agence de destination
    if (!&LOC::Machine::update($countryId, $tabOldInfos->{'machine.id'}, $tabUpdates, $userId, {}, undef, $tabEndUpds))
    {
        return 0;
    }

    # Mise à jour de Kimoce dans le cas où la machine est déjà attribuée à un contrat
    if ($tabOldMachineInfos->{'isAffectedOnPrecontract'})
    {
        &LOC::EndUpdates::updateKimoceMachine($tabEndUpds, $tabOldMachineInfos->{'parkNumber'},
                                              LOC::EndUpdates::KIMUPD_FEATURES);
    }

    # Mise à jour du transport
    $tabNewInfos->{'machine.tabInfos'}      = &_getMachineInfos($countryId, $tabOldInfos->{'machine.id'});
    $tabNewInfos->{'unloading'}->{'isDone'} = ACTION_USERDONE;
    $tabNewInfos->{'loading'}->{'isDone'}   = ACTION_USERDONE;

    # Calcul réel du stock réservable à cet instant (car le stock final ne sera mis à jour qu'à la fin de la transaction)
    # - comme la machine est passée en vérif,
    # - on regarde si le nombre de réservables + 1 est supérieur au nombre de réservés
    my $tabModelStock = &LOC::Model::getStockInfos($countryId,
                                                   $tabNewInfos->{'machine.tabInfos'}->{'agency.id'},
                                                   $tabNewInfos->{'machine.tabInfos'}->{'model.id'});
    if (!$tabModelStock)
    {
        return 0;
    }
    $tabNewInfos->{'machine.tabInfos'}->{'model.isRentable'} = ($tabModelStock->{'nbBookable'} + 1 > $tabModelStock->{'nbBooked'} ? 1 : 0);

    if (defined $date)
    {
        $tabNewInfos->{'unloading'}->{'date'} = $date;
    }
    else
    {
        $tabNewInfos->{'unloading'}->{'date'} = 'now';
    }

    if (!&_set($db, $countryId, $tabNewInfos, {}))
    {
        return 0;
    }

    # Mise à jour des commandes de contrôle si nécessaire s'il y a un changement de pays
    my $tabAgencyFromInfos = &LOC::Agency::getInfos($tabOldInfos->{'from.id'});
    my $tabAgencyToInfos   = &LOC::Agency::getInfos($tabOldInfos->{'to.id'});
    # - Récuperation du pays de départ
    my $countryFrom = $tabAgencyFromInfos->{'country.id'};
    # - Récuperation du pays d'arrivée
    my $countryTo = $tabAgencyToInfos->{'country.id'};
    if ($countryFrom ne $countryTo)
    {

        # Controle sur la machine existant?
        my $queryIsExistControl = '
SELECT
    COUNT(*)
FROM LOCATION' . ($countryTo eq 'FR' ? '' : $countryTo) . '.CONTROLEMACHINE
WHERE MAAUTO = ' . $tabOldInfos->{'machine.id'} . ';';

        my $isExistControl = $db->fetchOne($queryIsExistControl);

        if ($isExistControl eq '0')
        {
            # Création du controle machine dans le pays d'arrivée
            # - état contrat à CON02
            if ($db->insert('CONTROLEMACHINE',
                             {'MAAUTO'    => $tabOldInfos->{'machine.id'},
                              'COMAAPAVE' => '0000-00-00',
                              'COMAOK'    => 0
                             },
                             &LOC::Db::getSchemaName($countryTo)) == -1)
            {
                return 0;
            }

        }
    }

    # Mise à jour des alertes "Transports en cours" et "Enlèvements en attente"
    &_updateAlerts($tabOldInfos, $tabNewInfos, {}, $tabEndUpds);

    # Si la réalisation du déchargement a changé
    if ($tabOldInfos->{'unloading'}->{'isDone'} != $tabNewInfos->{'unloading'}->{'isDone'})
    {
        # Mise à jour des traces
        my $tabTrace = {
            'schema' => &LOC::Db::getSchemaName($countryId),
            'code'   => LOC::Log::EventType::DOAGENCIESTRANSFER
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $tabOldInfos->{'machine.id'}, $tabTrace);
    }

    return 1;
}



# Function: _getRights
# Récupérer les droits sur le transport pour un utilisateur donné
#
# Parameters:
# hashref $tabInfos      - Informations sur le transport
# hashref $tabUserInfos  - Informations sur l'utilisateur
#
# Returns:
# hashref - Tableau des droits
sub _getRights
{
    my ($tabInfos, $tabUserInfos) = @_;

    my $isUserAdmin        = $tabUserInfos->{'isAdmin'};
    my $isUserSuperv       = $tabUserInfos->{'isSuperv'};
    my $isUserTech         = &LOC::Util::in_array(LOC::User::GROUP_TECHNICAL, $tabUserInfos->{'tabGroups'});
    my $isUserChefAg       = &LOC::Util::in_array(LOC::User::GROUP_AGENCYMANAGER, $tabUserInfos->{'tabGroups'});
    my $isUserComm         = &LOC::Util::in_array(LOC::User::GROUP_COMMERCIAL, $tabUserInfos->{'tabGroups'});
    my $isUserCompta       = &LOC::Util::in_array(LOC::User::GROUP_ACCOUNTING, $tabUserInfos->{'tabGroups'});
    my $isUserTransp       = &LOC::Util::in_array(LOC::User::GROUP_TRANSPORT, $tabUserInfos->{'tabGroups'});
    my $isUserChefAt       = &LOC::Util::in_array(LOC::User::GROUP_FOREMAN, $tabUserInfos->{'tabGroups'});
    my $isUserRespTech     = &LOC::Util::in_array(LOC::User::GROUP_TECHNICALMANAGER, $tabUserInfos->{'tabGroups'});
    my $isUserTechPlatform = &LOC::Util::in_array(LOC::User::GROUP_TECHNICALPLATFORM, $tabUserInfos->{'tabGroups'});

    my $tabRights = {};
    my $tabComponents = {
        'actions' => ['consider', 'do-std', 'undo-std', 'do-self', 'undo-self', 'do-agtsf', 'undo-agtsf']
    };

    # Initialisation des droits
    &LOC::Util::setTreeValues($tabRights, $tabComponents, 0);

    # TECH, CHEFAT et PSTN
    if ($isUserTech || $isUserChefAt || $isUserTechPlatform)
    {
        &LOC::Util::setTreeValues($tabRights, {'actions' => ['consider', 'do-self', 'undo-self']}, 1);
    }

    # COMM et CHEFAG
    if ($isUserChefAg || $isUserComm)
    {
        &LOC::Util::setTreeValues($tabRights, {'actions' => ['do-self', 'undo-self']}, 1);
    }

    # TRANSP
    if ($isUserTransp)
    {
        &LOC::Util::setTreeValues($tabRights, {'actions' => ['do-std', 'undo-std', 'do-agtsf', 'undo-agtsf']}, 1);
    }

    # SUPERV et Admin
    if ($isUserSuperv || $isUserAdmin)
    {
        &LOC::Util::setTreeValues($tabRights, {'actions' => ['consider',
                                                             'do-agtsf', 'undo-agtsf',
                                                             'do-std', 'undo-std',
                                                             'do-self', 'undo-self']}, 1);
    }

    return $tabRights;
}



# Function: _hasRight
# Vérifier le droit à un champ
#
# Parameters:
# hashref $tab  - Tableau des droits
# string  $name - Nom du droit
#
# Returns:
# bool
sub _hasRight
{
    my ($tab, $name) = @_;

    return (ref($tab) eq 'HASH' ? ($tab->{$name} ? 1 : 0) : (index('|' . $tab . '|', '|' . $name . '|') == -1 ? 0 : 1));
}


# Function: _getAgencyInfos
# Retourne les informations d'une agence
#
# Contenu du hashage :
# - code => code de l'agence
# - label => nom de l'agence
# - address => adresse de l'agence,
# - postalCode => code postal,
# - city => ville
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Id de l'agence
# string  $side       - Dans quel bloc se trouve-t-on ? (livraison ou récupération)
# int     $flags      - Flags (facultatif),
# hahref  $tabOptions - Options supplémentaires (facultatif)
#
# avec le flag GETFROMTOINFOS_DETAILS activé, un sous-tableau "tabDetails" est ajouté contenant :
# - id => identifiant de l'agence
#
# Returns:
# hashref|undef - Informations sur l'agence
sub _getAgencyInfos
{
    my ($countryId, $id, $side, $flags, $tabOptions) = @_;

    # Récupération des informations de l'agence
    my $tabInfos = &LOC::Agency::getInfos($id);
    if (!$tabInfos)
    {
        return undef;
    }


    my %tab = (
        '@type'      => FROMTOTYPE_AGENCY,

        'code'       => $tabInfos->{'id'},
        'label'      => $tabInfos->{'label'},
        'address'    => $tabInfos->{'address'},
        'postalCode' => $tabInfos->{'postalCode'},
        'city'       => $tabInfos->{'city'},
    );


    # Récupération des informations détaillées
    if ($flags & GETFROMTOINFOS_DETAILS)
    {
        $tab{'tabDetails'} = {
            'id' => $tabInfos->{'id'}
        };
    }

    return \%tab;
}


# Function: _getContractInfos
# Retourne les informations nécessaire d'un contrat pour l'établissement d'un transport.
#
# Contenu du hashage :
# - code => code du contrat,
# - agency.id => identifiant de l'agence du contrat,
# - customer.name => raison sociale du client,
# - customer.url => URL du client,
# - beginDate => date de début de location,
# - endDate => date de fin de location,
# - isDatesToConfirm => indique si les dates de location sont à confirmer (vide pour le contrat),
# - site.id => identifiant du chantier,
# - site.label => libellé du chantier,
# - site.address => adresse du chantier,
# - site.postalCode => code postal du chantier,
# - site.city => ville chantier,
# - site.country.id => pays du chantier,
# - site.comments => commentaires chantier,
# - site.isHasWharf => est-ce que le chantier possède un quai ?,
# - isFinalAllocation => est-ce que l'attribution de la machine est définitive ?,
# - machine.id => identifiant de la machine attribuée ou associée,
# - machine.tabInfos => informations sur la machine attribuée ou associée,
# - wishedMachine.id => identifiant de la machine souhaitée,
# - status => état du contrat,
# - isAccepted => toujours vrai pour le contrat,
# - url => URL du contrat,
# - orderNo => numéro de commande du contrat,
# - orderContact.fullName => nom complet du contact commande,
# - orderContact.telephone => numéro de téléphone du contact commande,
# - site.contact.fullName => nom complet du contact chantier,
# - site.contact.telephone => numéro de téléphone du contact chantier,
#
# si $side = SIDE_DELIVERY :
# - amount => montant du transport aller,
# - window => heure de début et de fin du déchargement,
# - isAddMachine => machine supplémentaire pour la livraison ?,
# - isAnticipatable => livraison anticipée possible ?,
# - addMachineLnkCtt.id => id du contrat lié en cas de machine supplémentaire pour la livraison,
# - addMachineLnkCtt.url => URL du contrat lié en cas de machine supplémentaire pour la livraison,
#
# si $side = SIDE_RECOVERY :
# - amount => montant du transport retour,
# - window => Heure de début et de fin du chargement,
# - isAddMachine => machine supplémentaire pour la récupération ?,
# - addMachineLnkCtt.id => id du contrat lié à la machine supplémentaire pour la récupération,
# - addMachineLnkCtt.url => url du contrat lié à la machine supplémentaire pour la récupération,
#
# avec le flag GETFROMTOINFOS_DETAILS activé, un sous-tableau "tabDetails" est ajouté contenant :
# - id => identifiant du contrat
# - linkedEstimateLine.id => identifiant de la ligne de devis associée
# - customer.id => identifiant du client
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Id du contrat
# string  $side       - Dans quel bloc se trouve-t-on ? (livraison ou récupération)
# int     $flags      - Flags (facultatif)
# hahref  $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hashref|undef - Informations sur le contrat
sub _getContractInfos
{
    my ($countryId, $id, $side, $flags, $tabOptions) = @_;

    if (!defined $flags)
    {
        $flags = GETFROMTOINFOS_BASE;
    }

    # Récupération des informations du contrat
    my $contractFlags = LOC::Contract::Rent::GETINFOS_CUSTOMER |
                        LOC::Contract::Rent::GETINFOS_TARIFFTRSP;
    if ($flags & GETFROMTOINFOS_DETAILS)
    {
        $contractFlags |= LOC::Contract::Rent::GETINFOS_TARIFFRENT;
    }
    my $tabContractOptions = {};
    if ($tabOptions->{'lockDbRow'})
    {
        $tabContractOptions->{'lockDbRow'} = 1;
    }
    my $tabInfos = &LOC::Contract::Rent::getInfos($countryId, $id, $contractFlags, $tabContractOptions);
    if (!$tabInfos)
    {
        return undef;
    }

    # Informations sur le chantier
    my $tabSiteInfos = &LOC::Site::getInfos($countryId, $tabInfos->{'site.id'}, LOC::Site::GETINFOS_TIMESLOTS);
    if (!$tabSiteInfos)
    {
        return undef;
    }

    # Informations sur la machine
    my $tabMachineInfos = undef;
    my $wishedMachineId = undef;
    if ($tabInfos->{'state.id'} ne LOC::Contract::Rent::STATE_CANCELED)
    {
        if ($tabInfos->{'machine.id'})
        {
            $tabMachineInfos = &LOC::Machine::getInfos($countryId, $tabInfos->{'machine.id'}, LOC::Machine::GETINFOS_BASE, {});
        }
        $wishedMachineId = $tabInfos->{'wishedMachine.id'};
    }


    my %tab = (
        '@type'                   => FROMTOTYPE_CONTRACT,

        'code'                    => $tabInfos->{'code'},
        'agency.id'               => $tabInfos->{'agency.id'},
        'customer.name'           => $tabInfos->{'customer.name'},
        'customer.url'            => &LOC::Customer::getUrl($tabInfos->{'customer.id'}),
        'beginDate'               => $tabInfos->{'beginDate'},
        'endDate'                 => $tabInfos->{'endDate'},
        'isDatesToConfirm'        => undef,
        'invoiceState'            => $tabInfos->{'invoiceState'},
        'site.id'                 => $tabSiteInfos->{'id'},
        'site.label'              => $tabSiteInfos->{'label'},
        'site.address'            => $tabSiteInfos->{'address'},
        'site.addressUpdateCount' => $tabSiteInfos->{'addressUpdateCount'},
        'site.postalCode'         => $tabSiteInfos->{'locality.postalCode'},
        'site.city'               => $tabSiteInfos->{'locality.name'},
        'site.country.id'         => $tabSiteInfos->{'locality.country.id'},
        'site.comments'           => $tabSiteInfos->{'comments'},
        'site.isHasWharf'         => $tabSiteInfos->{'isHasWharf'},
        'site.isAsbestos'         => $tabSiteInfos->{'isAsbestos'},
        'isFinalAllocation'       => $tabInfos->{'isFinalAllocation'},
        'model.id'                => $tabInfos->{'model.id'},
        'machine.id'              => ($tabMachineInfos ? $tabMachineInfos->{'id'} : undef),
        'machine.tabInfos'        => ($tabMachineInfos ? {
            'state.id'              => $tabMachineInfos->{'state.id'},
            'agency.id'             => $tabMachineInfos->{'agency.id'},
            'contract.id'           => $tabMachineInfos->{'contract.id'},
            'isSiteBlocked'         => $tabMachineInfos->{'isSiteBlocked'},
            'visibilityMode'        => $tabMachineInfos->{'visibilityMode'},
            'visibleOnAgency.id'    => $tabMachineInfos->{'visibleOnAgency.id'},
            'visibleOnAgency.label' => $tabMachineInfos->{'visibleOnAgency.label'},
            'model.id'              => $tabMachineInfos->{'model.id'},
        } : undef),
        'wishedMachine.id'        => $wishedMachineId,
        'status'                  => $tabInfos->{'state.id'},
        'isAccepted'              => 1,
        'url'                     => &LOC::Request::createRequestUri('rent:rentContract:view',
                                                                     {'contractId' => $tabInfos->{'id'},
                                                                      'tabIndex'   => DEFAULT_CONTRACT_TABINDEX}),

        'orderNo'                 => $tabInfos->{'orderNo'},
        'orderContact.fullName'   => $tabInfos->{'orderContact.fullName'},
        'orderContact.telephone'  => $tabInfos->{'orderContact.telephone'},
        'site.contact.fullName'   => $tabSiteInfos->{'contact.fullName'},
        'site.contact.telephone'  => $tabSiteInfos->{'contact.telephone'},
    );

    if ($side eq SIDE_DELIVERY)
    {
        $tab{'amount'}               = $tabInfos->{'transportTariff.deliveryAmount'};
        $tab{'isAddMachine'}         = $tabInfos->{'transportTariff.isAddMachineDelivery'};
        $tab{'isAnticipatable'}      = $tabSiteInfos->{'isAnticipated'};
        $tab{'addMachineLnkCtt.id'}  = $tabInfos->{'transportTariff.addMachineDeliveryLnkCtt.id'};
        $tab{'addMachineLnkCtt.url'} = ($tabInfos->{'transportTariff.addMachineDeliveryLnkCtt.id'} ?
                                          &LOC::Request::createRequestUri('rent:rentContract:view',
                                                                          {'contractId' => $tabInfos->{'transportTariff.addMachineDeliveryLnkCtt.id'}}) : '');


        # Plage horaire de livraison
        if ($tabSiteInfos->{'deliveryHour'} ne '')
        {
            $tab{'window'} = {
                'from' => $tabInfos->{'beginDate'} . ' ' . $tabSiteInfos->{'deliveryHour'} . ':00',
                'till' => $tabInfos->{'beginDate'} . ' ' . $tabSiteInfos->{'deliveryHour'} . ':00'
            };
        }
        elsif ($tabSiteInfos->{'deliveryTimeSlot.id'})
        {
            $tab{'window'} = {
                'from' => $tabInfos->{'beginDate'} . ' ' . $tabSiteInfos->{'deliveryTimeSlot.beginHour'} . ':00',
                'till' => $tabInfos->{'beginDate'} . ' ' . $tabSiteInfos->{'deliveryTimeSlot.endHour'} . ':00'
            };
        }
        else
        {
            $tab{'window'} = {
                'from' => $tabInfos->{'beginDate'} . ' ' . DEFAULT_BEGINTIME,
                'till' => $tabInfos->{'beginDate'} . ' ' . DEFAULT_ENDTIME
            };
        }
    }
    elsif ($side eq SIDE_RECOVERY)
    {
        $tab{'amount'}               = $tabInfos->{'transportTariff.recoveryAmount'};
        $tab{'isAddMachine'}         = $tabInfos->{'transportTariff.isAddMachineRecovery'};
        $tab{'isAnticipatable'}      = 0;
        $tab{'addMachineLnkCtt.id'}  = $tabInfos->{'transportTariff.addMachineRecoveryLnkCtt.id'};
        $tab{'addMachineLnkCtt.url'} = ($tabInfos->{'transportTariff.addMachineRecoveryLnkCtt.id'} ?
                                          &LOC::Request::createRequestUri('rent:rentContract:view',
                                                                          {'contractId' => $tabInfos->{'transportTariff.addMachineRecoveryLnkCtt.id'}}) : '');


        # Plage horaire de récupération
        if ($tabSiteInfos->{'recoveryHour'} ne '')
        {
            $tab{'window'} = {
                'from' => $tabInfos->{'endDate'} . ' ' . $tabSiteInfos->{'recoveryHour'} . ':00',
                'till' => $tabInfos->{'endDate'} . ' ' . $tabSiteInfos->{'recoveryHour'} . ':00'
            };
        }
        elsif ($tabSiteInfos->{'recoveryTimeSlot.id'})
        {
            $tab{'window'} = {
                'from' => $tabInfos->{'endDate'} . ' ' . $tabSiteInfos->{'recoveryTimeSlot.beginHour'} . ':00',
                'till' => $tabInfos->{'endDate'} . ' ' . $tabSiteInfos->{'recoveryTimeSlot.endHour'} . ':00'
            };
        }
        else
        {
            # Modification de la date de fin de chargement dans le cas où il n'y a pas d'heure ou de plage renseignée
            my $addDaysNumber = &LOC::Characteristic::getAgencyValueByCode('OTDJRSUPRECUP', $tabInfos->{'agency.id'});
            my $newEndDate = &LOC::Date::addWorkingDays($tabInfos->{'endDate'}, $addDaysNumber, $tabInfos->{'agency.id'});
            $tab{'window'} = {
                'from' => $tabInfos->{'endDate'} . ' ' . DEFAULT_BEGINTIME,
                'till' => $newEndDate . ' ' . DEFAULT_ENDTIME
            };
        }
    }


    # Récupération des informations détaillées
    if ($flags & GETFROMTOINFOS_DETAILS)
    {
        my $proformaFlags = $tabInfos->{'proformaFlags'};

        # Informations détaillées
        $tab{'tabDetails'} = {
            'id'               => $tabInfos->{'id'},
            'state.id'         => $tabInfos->{'state.id'},
            'linkedEstimateLine.id' => $tabInfos->{'linkedEstimateLine.id'},
            'customer.id'      => $tabInfos->{'customer.id'},
            'duration'         => $tabInfos->{'duration'},
            'isProformaNotGen' => (($proformaFlags & LOC::Proforma::DOCFLAG_CT_ISCUSTOMERPFREQONCREATION &&
                                    $tabInfos->{'total'} > 0 &&  $tabInfos->{'customer.isProformaRequired'}) &&
                                   !($proformaFlags & LOC::Proforma::DOCFLAG_EXISTACTIVED) ? 1 : 0),
            'isProformaNotUpToDate' => ($proformaFlags & LOC::Proforma::DOCFLAG_EXISTACTIVEDEXPIRED ? 1 : 0),
            'isProformaNotPayed' => ($proformaFlags & LOC::Proforma::DOCFLAG_EXISTACTIVED &&
                                     !($proformaFlags & LOC::Proforma::DOCFLAG_EXISTACTIVEDEXPIRED) &&
                                     !($proformaFlags & LOC::Proforma::DOCFLAG_ALLACTIVEDFINALIZED) ? 1 : 0),
            'isProformaExtNotPayed' => ($proformaFlags & LOC::Proforma::DOCFLAG_EXISTACTIVEDEXTENSION &&
                                        !($proformaFlags & LOC::Proforma::DOCFLAG_EXISTACTIVEDEXPIRED) &&
                                        !($proformaFlags & LOC::Proforma::DOCFLAG_ALLACTIVEDFINALIZED) ? 1 : 0),
            'isCustomerNotVerified' => ($tabInfos->{'customer.isNewAccount'} || $tabInfos->{'isLocked'} ? 1 : 0),
            'isRentTariffError' => ($tabInfos->{'rentTariff.specialReductionStatus'} eq LOC::Tariff::Rent::REDUCTIONSTATE_PENDING ? 1 : 0),
            'isTrspTariffError' => ($tabInfos->{'transportTariff.specialReductionStatus'} eq LOC::Tariff::Transport::REDUCTIONSTATE_PENDING ? 1 : 0)
        };

        if ($side eq SIDE_DELIVERY)
        {
            # Informations sur la récupération pour ce contrat
            $tab{'tabDetails'}->{'recovery.id'} = undef;
            $tab{'tabDetails'}->{'recovery.tabInfos'} = undef;
            my $tabRecoveryInfos = &getDocumentRecoveryInfos($countryId, FROMTOTYPE_CONTRACT, $id, GETINFOS_BASE, {});
            if ($tabRecoveryInfos)
            {
                $tab{'tabDetails'}->{'recovery.id'} = $tabRecoveryInfos->{'id'};
                $tab{'tabDetails'}->{'recovery.tabInfos'} = {
                    'type' => $tabRecoveryInfos->{'type'}
                };
            }
        }
        elsif ($side eq SIDE_RECOVERY)
        {
            # Informations sur la livraison pour ce contrat
            $tab{'tabDetails'}->{'delivery.id'} = undef;
            $tab{'tabDetails'}->{'delivery.tabInfos'} = undef;
            my $tabDeliveryInfos = &getDocumentDeliveryInfos($countryId, FROMTOTYPE_CONTRACT, $id, GETINFOS_BASE, {});
            if ($tabDeliveryInfos)
            {
                $tab{'tabDetails'}->{'delivery.id'} = $tabDeliveryInfos->{'id'};
                $tab{'tabDetails'}->{'delivery.tabInfos'} = {
                    'type' => $tabDeliveryInfos->{'type'}
                };
            }
        }
    }

    return \%tab;
}


# Function: _getErrors
# Récupération des codes d'erreurs bloquantes pour le chargement ou le déchargement
#
# Parameters:
# int $errors - Erreurs
#
# Returns:
# arrayref
sub _getErrors
{
    my ($errors) = @_;

    my @tab = ();

    if ($errors & ERROR_NOMACHINE)
    {
        push(@tab, 'noMachine');
    }
    if ($errors & ERROR_PFNOTGEN)
    {
        push(@tab, 'proformaNotGen');
    }
    if ($errors & ERROR_PFNOTUPTODATE)
    {
        push(@tab, 'proformaNotUpToDate');
    }
    if ($errors & ERROR_PFNOTPAYED)
    {
        push(@tab, 'proformaNotPayed');
    }
    if ($errors & ERROR_RENTTARIFF)
    {
        push(@tab, 'rentTariff');
    }
    if ($errors & ERROR_TRSPTARIFF)
    {
        push(@tab, 'transportTariff');
    }
    if ($errors & ERROR_CUSTNOTVERIF)
    {
        push(@tab, 'customerNotVerified');
    }
    if ($errors & ERROR_DELIVERYNOTDONE)
    {
        push(@tab, 'deliveryNotDone');
    }
    if ($errors & ERROR_DELIVERY)
    {
        push(@tab, 'onDelivery');
    }
    if ($errors & ERROR_RECOVERY)
    {
        push(@tab, 'onRecovery');
    }
    if ($errors & ERROR_NOMAINCONTRACT)
    {
        push(@tab, 'noMainContract');
    }
    if ($errors & ERROR_MACHINESITEBLOCKED)
    {
        push(@tab, 'machineSiteBlocked');
    }
    if ($errors & ERROR_MACHINENOTTRANSFERED)
    {
        push(@tab, 'machineNotTransfered');
    }

    if ($errors & ERROR_VISIBLEMACHINE)
    {
        push(@tab, 'visibleMachine');
    }
    if ($errors & ERROR_RECOVERYTRANSFER)
    {
        push(@tab, 'recoveryTransfer');
    }
    if ($errors & ERROR_TRANSFERREDMACHINE)
    {
        push(@tab, 'transferredMachine');
    }
    if ($errors & ERROR_INVOICEDDELIVERY)
    {
        push(@tab, 'invoicedDelivery');
    }

    return \@tab;
}


# Function: _getEstimateLineInfos
# Retourne les informations d'une ligne de devis
#
# Contenu du hashage :
# - code => code du devis,
# - agency.id => identifiant de l'agence du devis,
# - customer.name => raison sociale du client,
# - customer.url => URL du client,
# - beginDate => date de début de location,
# - endDate => date de fin de location,
# - isDatesToConfirm => indique si les dates de location sont à confirmer,
# - site.id => id du chantier,
# - site.label => libellé du chantier,
# - site.address => adresse du chantier,
# - site.postalCode => code postal du chantier
# - site.city => ville chantier,
# - site.country.id => pays du chantier,
# - site.comments => commentaires chantier,
# - site.isHasWharf => est-ce que le chantier possède un quai ?,
# - isFinalAllocation => est-ce que l'attribution de la machine est définitive ?,
# - machine.id => identifiant de la machine attribuée (ou associée) = NON DEFINI,
# - machine.tabInfos => informations sur la machine attribuée ou associée = NON DEFINI,
# - wishedMachine.id => identifiant de la machine souhaitée = NON DEFINI,
# - status => état de la ligne ("acceptedEstimateLine"),
# - isAccepted => si la ligne est acceptée,
# - url => URL du devis
# - orderNo => numéro de commande du devis,
# - orderContact.fullName => nom complet du contact commande,
# - orderContact.telephone => numéro de téléphone du contact commande,
# - site.contact.fullName => nom complet du contact chantier,
# - site.contact.telephone => numéro de téléphone du contact chantier,
#
# si $side = SIDE_DELIVERY :
# - amount => montant du transport aller,
# - window => Heure de début et de fin du déchargement,
# - isAddMachine => machine supplémentaire pour la livraison ?,
# - isAnticipatable => livraison anticipée possible ?,
#
# si $side = SIDE_RECOVERY :
# - amount => montant du transport retour,
# - window => Heure de début et de fin du chargement,
# - isAddMachine => machine supplémentaire pour la récupération ?,
#
# avec le flag GETFROMTOINFOS_DETAILS activé, un sous-tableau "tabDetails" est ajouté contenant :
# - id => identifiant de la ligne de devis,
# - estimate.id => identifiant du devis,
# - customer.id => identifiant du client
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Id de la ligne de devis
# string  $side       - Dans quel bloc se trouve-t-on ? (livraison ou récupération)
# int     $flags      - Flags (facultatif)
# hahref  $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hashref|undef - Informations sur la ligne de devis
sub _getEstimateLineInfos
{
    my ($countryId, $id, $side, $flags, $tabOptions) = @_;

    if (!defined $flags)
    {
        $flags = GETFROMTOINFOS_BASE;
    }

    # Récupération des informations de la ligne de devis
    my $lineFlags = LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE |
                    LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE_CUSTOMER |
                    LOC::Estimate::Rent::GETLINEINFOS_TARIFFTRSP;
    if ($flags & GETFROMTOINFOS_DETAILS)
    {
        $lineFlags |= LOC::Estimate::Rent::GETLINEINFOS_TARIFFRENT;
    }

    my $tabInfos = &LOC::Estimate::Rent::getLineInfos($countryId, $id, $lineFlags);
    if (!$tabInfos)
    {
        return undef;
    }

    # Informations sur le chantier
    my $tabSiteInfos = &LOC::Site::getInfos($countryId, $tabInfos->{'estimate.site.id'}, LOC::Site::GETINFOS_TIMESLOTS);
    if (!$tabSiteInfos)
    {
        return undef;
    }


    my %tab = (
        '@type'                   => FROMTOTYPE_ESTIMATELINE,

        'code'                    => $tabInfos->{'estimate.code'},
        'agency.id'               => $tabInfos->{'estimate.agency.id'},
        'customer.name'           => $tabInfos->{'estimate.customer.name'},
        'customer.url'            => &LOC::Customer::getUrl($tabInfos->{'estimate.customer.id'}),
        'beginDate'               => $tabInfos->{'beginDate'},
        'endDate'                 => $tabInfos->{'endDate'},
        'isDatesToConfirm'        => $tabInfos->{'isDatesToConfirm'},
        'invoiceState'            => undef,
        'site.id'                 => $tabSiteInfos->{'id'},
        'site.label'              => $tabSiteInfos->{'label'},
        'site.address'            => $tabSiteInfos->{'address'},
        'site.addressUpdateCount' => $tabSiteInfos->{'addressUpdateCount'},
        'site.postalCode'         => $tabSiteInfos->{'locality.postalCode'},
        'site.city'               => $tabSiteInfos->{'locality.name'},
        'site.country.id'         => $tabSiteInfos->{'locality.country.id'},
        'site.comments'           => $tabSiteInfos->{'comments'},
        'site.isHasWharf'         => $tabSiteInfos->{'isHasWharf'},
        'site.isAsbestos'         => $tabSiteInfos->{'isAsbestos'},
        'isFinalAllocation'       => 0,
        'model.id'                => $tabInfos->{'model.id'},
        'machine.id'              => undef,
        'machine.tabInfos'        => undef,
        'wishedMachine.id'        => undef,
        'status'                  => 'acceptedEstimateLine',
        'isAccepted'              => $tabInfos->{'isAccepted'},
        'url'                     => &LOC::Request::createRequestUri('rent:rentEstimate:view',
                                                                     {'estimateId'     => $tabInfos->{'estimate.id'},
                                                                      'estimateLineId' => $tabInfos->{'id'},
                                                                      'tabIndex'       => DEFAULT_ESTIMATELINE_TABINDEX}),

        'orderNo'                 => $tabInfos->{'estimate.orderNo'},
        'orderContact.fullName'   => $tabInfos->{'estimate.orderContact.fullName'},
        'orderContact.telephone'  => $tabInfos->{'estimate.orderContact.telephone'},
        'site.contact.fullName'   => $tabSiteInfos->{'contact.fullName'},
        'site.contact.telephone'  => $tabSiteInfos->{'contact.telephone'},
    );

    if ($side eq SIDE_DELIVERY)
    {
        $tab{'amount'}                       = $tabInfos->{'transportTariff.deliveryAmount'};
        $tab{'isAddMachine'}                 = $tabInfos->{'transportTariff.isAddMachineDelivery'};
        $tab{'isAnticipatable'}              = $tabSiteInfos->{'isAnticipated'};

        # Plage horaire de livraison
        if ($tabSiteInfos->{'deliveryHour'} ne '')
        {
            $tab{'window'} = {
                'from' => $tabInfos->{'beginDate'} . ' ' . $tabSiteInfos->{'deliveryHour'} . ':00',
                'till' => $tabInfos->{'beginDate'} . ' ' . $tabSiteInfos->{'deliveryHour'} . ':00'
            };
        }
        elsif ($tabSiteInfos->{'deliveryTimeSlot.id'})
        {
            $tab{'window'} = {
                'from' => $tabInfos->{'beginDate'} . ' ' . $tabSiteInfos->{'deliveryTimeSlot.beginHour'} . ':00',
                'till' => $tabInfos->{'beginDate'} . ' ' . $tabSiteInfos->{'deliveryTimeSlot.endHour'} . ':00',
            };
        }
        else
        {
            $tab{'window'} = {
                'from' => $tabInfos->{'beginDate'} . ' ' . DEFAULT_BEGINTIME,
                'till' => $tabInfos->{'beginDate'} . ' ' . DEFAULT_ENDTIME
            };
        }
    }
    elsif ($side eq SIDE_RECOVERY)
    {
        $tab{'amount'}                       = $tabInfos->{'transportTariff.recoveryAmount'};
        $tab{'isAddMachine'}                 = $tabInfos->{'transportTariff.isAddMachineRecovery'};
        $tab{'isAnticipatable'}              = 0;

        # Plage horaire de livraison
        if ($tabSiteInfos->{'recoveryHour'} ne '')
        {
            $tab{'window'} = {
                'from' => $tabInfos->{'endDate'} . ' ' . $tabSiteInfos->{'recoveryHour'} . ':00',
                'till' => $tabInfos->{'endDate'} . ' ' . $tabSiteInfos->{'recoveryHour'} . ':00'
            };
        }
        elsif ($tabSiteInfos->{'recoveryTimeSlot.id'})
        {
            $tab{'window'} = {
                'from' => $tabInfos->{'endDate'} . ' ' . $tabSiteInfos->{'recoveryTimeSlot.beginHour'} . ':00',
                'till' => $tabInfos->{'endDate'} . ' ' . $tabSiteInfos->{'recoveryTimeSlot.endHour'} . ':00',
            };
        }
        else
        {
            # Modification de la date de fin de chargement dans le cas où il n'y a pas d'heure ou de plage renseignée
            my $addDaysNumber = &LOC::Characteristic::getAgencyValueByCode('OTDJRSUPRECUP', $tabInfos->{'estimate.agency.id'});
            my $newEndDate = &LOC::Date::addWorkingDays($tabInfos->{'endDate'}, $addDaysNumber, $tabInfos->{'estimate.agency.id'});
            $tab{'window'} = {
                'from' => $tabInfos->{'endDate'} . ' ' . DEFAULT_BEGINTIME,
                'till' => $newEndDate . ' ' . DEFAULT_ENDTIME
            };
        }
    }


    # Récupération des informations détaillées
    if ($flags & GETFROMTOINFOS_DETAILS)
    {
        $tab{'tabDetails'} = {
            'id'                => $tabInfos->{'id'},
            'estimate.id'       => $tabInfos->{'estimate.id'},
            'customer.id'       => $tabInfos->{'estimate.customer.id'},
            'state.id'          => $tabInfos->{'state.id'},
            'estimate.state.id' => $tabInfos->{'estimate.state.id'}
        };
    }

    return \%tab;
}


# Function: _getFromToEntityInfos
# Retourne les informations d'une entitée d'arrivée ou de départ (agence, contrat ou ligne de devis)
#
# Contenu du hashage : voir les méthodes _getAgencyInfos(), _getContractInfos() et _getEstimateLineInfos()
#
# Parameters:
# string  $countryId  - Identifiant du pays
# string  $type       - Type d'entitée ('agency', 'contract', 'estimateLine' (voir constantes FROMTOTYPE_*))
# int     $id         - Id de l'entitée
# string  $side       - Dans quel bloc se trouve-t-on ? (livraison ou récupération)
# int     $flags      - Flags (facultatif)
# hahref  $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hashref|undef - Informations sur l'entitée
sub _getFromToEntityInfos
{
    my ($countryId, $type, $id, $side, $flags, $tabOptions) = @_;

    if ($type eq FROMTOTYPE_AGENCY)
    {
        return &_getAgencyInfos($countryId, $id, $side, $flags, $tabOptions);
    }
    elsif ($type eq FROMTOTYPE_CONTRACT)
    {
        return &_getContractInfos($countryId, $id, $side, $flags, $tabOptions);
    }
    elsif ($type eq FROMTOTYPE_ESTIMATELINE)
    {
        return &_getEstimateLineInfos($countryId, $id, $side, $flags, $tabOptions);
    }
    return undef;
}


# Function: _getMachineInfos
# Retourne les informations d'une machine
#
# Contenu du hashage :
# - id => identifiant de la machine,
# - state.id => état de la machine,
# - agency.id => agence de la machine,
# - parkNumber => numéro de parc,
# - energy => énergie de la machine,
# - isSiteBlocked => 1 si la machine est bloquée sur chantier, sinon 0
# - isAffectedOnPrecontract => la machine est-elle attribuée ou associée à un précontrat ?
# - url => URL de la visu machine
# - model.id => identifiant du modèle de machine
# - model.isRentable => le modèle est réservable dans l'agence physique de la machine ?
#
# Parameters:
# string  $countryId - Identifiant du pays
# int     $id        - Id de la machine
#
# Returns:
# hashref|undef - Informations sur la machine
sub _getMachineInfos
{
    my ($countryId, $id) = @_;

    # Récupération des informations de la machine
    my $tabInfos = &LOC::Machine::getInfos($countryId, $id, LOC::Machine::GETINFOS_BASE, {'level' => LOC::Machine::LEVEL_MACHINESFAMILY});
    if (!$tabInfos)
    {
        return undef;
    }

    return {
        'id'                      => $tabInfos->{'id'},
        'state.id'                => $tabInfos->{'state.id'},
        'agency.id'               => $tabInfos->{'agency.id'},
        'parkNumber'              => $tabInfos->{'parkNumber'},
        'energy'                  => $tabInfos->{'machinesFamily.energy'},
        'isSiteBlocked'           => $tabInfos->{'isSiteBlocked'},
        'isAffectedOnPrecontract' => $tabInfos->{'isAffectedOnPrecontract'},
        'url'                     => &LOC::Machine::getUrl($id),
        'model.id'                => $tabInfos->{'model.id'},
        'model.isRentable'        => $tabInfos->{'model.isRentable'}
    };
}


# Function: _getDataForMailAgenciesTransfer
# Retourne les informations nécessaires au template du mail concernant les transferts inter-agences
#
# Contenu du hashage :
# - tabReplaces   => Données pour le remplacement dans le template,
# - tabRecipients => Destinataires du mail,
#
# Parameters:
# string  $countryId - Identifiant du pays
# hashref $tabOldInfos - Anciennes informations du transport
# hashref $tabNewInfos - Nouvelles informations du transport
# arrayre $tabModif    - Tableau des modifications effectuées. (create, modif, suppr)
#
# Returns:
# hashref - Informations pour le template
sub _getDataForMailAgenciesTransfer
{
    my ($countryId, $tabOldInfos, $tabNewInfos, $tabModif) = @_;

    # Agence de provenance
    my $tabAgencyFromInfos = &LOC::Agency::getInfos($tabNewInfos->{'from.id'});
    # Agence de destination
    my $tabAgencyToInfos = {};
    if ($tabNewInfos->{'to.id'} ne '')
    {
         $tabAgencyToInfos   = &LOC::Agency::getInfos($tabNewInfos->{'to.id'});
    }
    # Dans le cas d'une suppression
    else
    {
        $tabAgencyToInfos   = &LOC::Agency::getInfos($tabOldInfos->{'to.id'});
    }

    # Ancienne agence de destination
    my $tabAgencyToOldInfos = {};
    # Si changement d'agence de destinataire
    if ($tabOldInfos)
    {
        # Changement de destination
        if ($tabOldInfos->{'to.id'} ne $tabNewInfos->{'to.id'})
        {
             $tabAgencyToOldInfos   = &LOC::Agency::getInfos($tabOldInfos->{'to.id'});
        }
    }

    # Agence déléguée
    my $tabDelegatedAgencyInfos = &LOC::Agency::getInfos($tabNewInfos->{'delegatedAgency.id'});

    # Récupération des données de la machine
    my $tabMachineInfos = &LOC::Machine::getInfos($countryId, $tabNewInfos->{'machine.id'}, LOC::Machine::GETINFOS_BASE);
    # - URL de la machine
    $tabMachineInfos->{'urlMachine'} = &LOC::Machine::getUrlByParkNumber($tabMachineInfos->{'parkNumber'});

    # Tableau des données à remplacer
    my $tabReplaces = {
        'agenciesTransfer.modifications' => $tabModif,
        'machine.parkNumber'             => $tabMachineInfos->{'parkNumber'},
        'machine.url'                    => $tabMachineInfos->{'urlMachine'},
        'model.label'                    => $tabMachineInfos->{'model.label'},
        'agencyFrom.label'               => $tabAgencyFromInfos->{'label'},
        'agencyTo.label'                 => $tabAgencyToInfos->{'label'},
        'date.transfer'                  => ($tabNewInfos->{'date'} ne '' ? $tabNewInfos->{'date'} : $tabOldInfos->{'date'}),
        'delegatedAgency.label'          => ($tabDelegatedAgencyInfos ? $tabDelegatedAgencyInfos->{'label'} : '-')
    };

    # Destinataires
    # - Agence de départ
    # - Agence d'arrivée
    my %tabRecipients = (
        $tabAgencyFromInfos->{'email.site'}   => $tabAgencyFromInfos->{'label'},
        $tabAgencyFromInfos->{'email.transp'} => $tabAgencyFromInfos->{'label'},
        $tabAgencyToInfos->{'email.site'}     => $tabAgencyToInfos->{'label'},
        $tabAgencyToInfos->{'email.transp'}   => $tabAgencyToInfos->{'label'}
    );
    # - Agence déléguée
    if ($tabDelegatedAgencyInfos)
    {
        $tabRecipients{$tabDelegatedAgencyInfos->{'email.site'}}   = $tabDelegatedAgencyInfos->{'label'};
        $tabRecipients{$tabDelegatedAgencyInfos->{'email.transp'}} = $tabDelegatedAgencyInfos->{'label'};
    }
    # Ancienne agence de destination
    if ($tabAgencyToOldInfos->{'id'})
    {
        $tabRecipients{$tabAgencyToOldInfos->{'email.site'}}   = $tabAgencyToOldInfos->{'label'};
        $tabRecipients{$tabAgencyToOldInfos->{'email.transp'}} = $tabAgencyToOldInfos->{'label'};
    }

    # Données finales
    my $mailData = {
        'tabReplaces'  => $tabReplaces,
        'tabRecipients' => \%tabRecipients
    };
    return $mailData;
}

# Function: _getDataForMailSitesTransfer
# Retourne les informations nécessaires au template du mail concernant les transferts inter-chantiers
#
# Contenu du hashage :
# - tabReplaces   => Données pour le remplacement dans le template,
# - tabRecipients => Destinataires du mail,
#
# Parameters:
# string  $countryId - Identifiant du pays
# int     $id        - Identifiant du transport
#
# Returns:
# hashref - Informations pour le template
sub _getDataForMailSitesTransfer
{
    my ($countryId, $id) = @_;

    my $tabInfos = &getInfos($countryId, $id, LOC::Transport::GETINFOS_FROM |
                                              LOC::Transport::GETINFOS_TO |
                                              LOC::Transport::GETINFOS_MACHINE |
                                              LOC::Transport::GETINFOS_MODEL,
                                              {'getFromInfosFlags' => GETFROMTOINFOS_DETAILS,
                                               'getToInfosFlags'   => GETFROMTOINFOS_DETAILS});

    # Tableau des données à remplacer
    my $tabReplaces = {
        'isSelf'             => $tabInfos->{'isSelf'},
        'machine.parkNumber' => $tabInfos->{'machine.tabInfos'}->{'parkNumber'},
        'machine.url'        => $tabInfos->{'machine.tabInfos'}->{'url'},
        'model.label'        => $tabInfos->{'model.tabInfos'}->{'label'},
        'from.contract.code' => $tabInfos->{'from.tabInfos'}->{'code'},
        'from.contract.url'  => &LOC::Request::createRequestUri('rent:rentContract:view',
                                                                {'contractId' => $tabInfos->{'from.id'},
                                                                 'tabIndex'   => DEFAULT_CONTRACT_TABINDEX}),
        'from.endDate'       => $tabInfos->{'from.tabInfos'}->{'endDate'},
        'from.duration'      => $tabInfos->{'from.tabInfos'}->{'tabDetails'}->{'duration'},
        'from.customer.name' => $tabInfos->{'from.tabInfos'}->{'customer.name'},
        'from.customer.url'  => $tabInfos->{'from.tabInfos'}->{'customer.url'},
        'from.site.address'  => $tabInfos->{'from.tabInfos'}->{'site.label'} . ' - ' .
                                   $tabInfos->{'from.tabInfos'}->{'site.postalCode'} . ' ' .
                                   $tabInfos->{'from.tabInfos'}->{'site.city'},
        'to.contract.code'   => $tabInfos->{'to.tabInfos'}->{'code'},
        'to.contract.url'    => &LOC::Request::createRequestUri('rent:rentContract:view',
                                                                {'contractId' => $tabInfos->{'to.id'},
                                                                 'tabIndex'   => DEFAULT_CONTRACT_TABINDEX}),
        'to.beginDate'       => $tabInfos->{'to.tabInfos'}->{'beginDate'},
        'to.duration'        => $tabInfos->{'to.tabInfos'}->{'tabDetails'}->{'duration'},
        'to.customer.name'   => $tabInfos->{'to.tabInfos'}->{'customer.name'},
        'to.customer.url'    => $tabInfos->{'to.tabInfos'}->{'customer.url'},
        'to.site.address'    => $tabInfos->{'to.tabInfos'}->{'site.label'} . ' - ' .
                                   $tabInfos->{'to.tabInfos'}->{'site.postalCode'} . ' ' .
                                   $tabInfos->{'to.tabInfos'}->{'site.city'}
    };

    # Agence de destination
    my $tabAgencyToInfos = &LOC::Agency::getInfos($tabInfos->{'to.tabInfos'}->{'agency.id'});

    # Destinataires
    my %tabRecipients = ($tabAgencyToInfos->{'email.site'} => $tabAgencyToInfos->{'label'});

    # Données finales
    my $mailData = {
        'tabReplaces'  => $tabReplaces,
        'tabRecipients' => \%tabRecipients
    };
    return $mailData;
}


# Function: _getModelInfos
# Retourne les informations d'un modèle de machine
#
# Contenu du hashage :
# - id => identifiant du modèle de machine,
# - weight => poids,
# - height => hauteur,
# - length => longueur,
# - width => largeur,
# - label => désignation
#
# Parameters:
# string  $countryId - Identifiant du pays
# int     $id        - Id du modèle de machine
#
# Returns:
# hashref|undef - Informations sur le modèle de machine
sub _getModelInfos
{
    my ($countryId, $id) = @_;

    # Récupération des informations du modèle de machine
    my $tabInfos = &LOC::Model::getInfos($countryId, $id);
    if (!$tabInfos)
    {
        return undef;
    }


    my %tab = (
        'id'     => $tabInfos->{'id'},
        'weight' => $tabInfos->{'weight'},
        'height' => $tabInfos->{'stowedHeight'},
        'length' => $tabInfos->{'length'},
        'width'  => $tabInfos->{'width'},
        'label'  => $tabInfos->{'label'}
    );

    return \%tab;
}


# Function: _isEditPossible
# Indique s'il est possible de modifier le transport
#
# Parameters:
# string  $side        - Dans quel bloc se trouve-t-on ? (livraison ou récupération)
# hashref $tabOldInfos - Données du transport avant la modification
# hashref $tabNewInfos - Données du transport après la modification
# string  $rights      - Droits utilisateurs
#
# Returns:
# bool
sub _isEditPossible
{
    my ($side, $tabOldInfos, $tabNewInfos, $rights) = @_;

    my $tabPossibilities = $tabOldInfos->{'possibilities'};
    my $fromto = ($side eq SIDE_DELIVERY ? 'from' : 'to');

    # Possibilité sur enlèvement en agence
    if (!$tabPossibilities->{'edit'} || ($rights && !&_hasRight($rights, 'self')))
    {
        my $isOldChecked = ($tabOldInfos->{'isSelf'} && $tabOldInfos->{$fromto . '.type'} eq FROMTOTYPE_AGENCY ? 1 : 0);
        my $isNewChecked = ($tabNewInfos->{'isSelf'} && $tabNewInfos->{$fromto . '.type'} eq FROMTOTYPE_AGENCY ? 1 : 0);
        if ($isOldChecked != $isNewChecked)
        {
            return 0;
        }
    }

    # Possibilité sur reprise sur chantier
    if (!$tabPossibilities->{'edit'} || ($rights && !&_hasRight($rights, 'self')))
    {
        my $isOldChecked = ($tabOldInfos->{'isSelf'} && $tabOldInfos->{$fromto . '.type'} eq FROMTOTYPE_CONTRACT ? 1 : 0);
        my $isNewChecked = ($tabNewInfos->{'isSelf'} && $tabNewInfos->{$fromto . '.type'} eq FROMTOTYPE_CONTRACT ? 1 : 0);
        if ($isOldChecked != $isNewChecked || ($isOldChecked && $tabOldInfos->{$fromto . '.id'} != $tabNewInfos->{$fromto . '.id'}))
        {
            return 0;
        }
    }

    # Possibilité sur transfert inter-chantiers
    if (!$tabPossibilities->{'edit'} || ($rights && !&_hasRight($rights, 'transfer')))
    {
        my $isOldChecked = (!$tabOldInfos->{'isSelf'} && $tabOldInfos->{$fromto . '.type'} eq FROMTOTYPE_CONTRACT ? 1 : 0);
        my $isNewChecked = (!$tabNewInfos->{'isSelf'} && $tabNewInfos->{$fromto . '.type'} eq FROMTOTYPE_CONTRACT ? 1 : 0);
        if ($isOldChecked != $isNewChecked || ($isOldChecked && $tabOldInfos->{$fromto . '.id'} != $tabNewInfos->{$fromto . '.id'}))
        {
            return 0;
        }
    }

    # Possibilité sur délégation transport
    if (!$tabPossibilities->{'edit'} || ($rights && !&_hasRight($rights, 'delegate')))
    {
        if ($tabOldInfos->{'delegatedAgency.id'} ne $tabNewInfos->{'delegatedAgency.id'})
        {
            return 0;
        }
    }

    # Possibilité sur la coche transférer à
    if (!$tabPossibilities->{'edit'} || ($rights && !&_hasRight($rights, 'transferToAgency')))
    {
        if ($tabOldInfos->{'transferToAgency.id'} ne $tabNewInfos->{'transferToAgency.id'})
        {
            return 0;
        }
    }

    return 1;
}


# Function: _isOrderManaged
# Indique si la commande de transport est gérée ou non sur un transport

# Parameters:
# hashref $tabInfos - Données du transport
#
# Returns:
# bool
sub _isOrderManaged
{
    my ($tabInfos) = @_;

    # Est-on en mode OTD ?
    my $isOrderManageActive = &LOC::Characteristic::getAgencyValueByCode('TRSPGENCMD', $tabInfos->{'agency.id'});
    if (!$isOrderManageActive)
    {
        return 0;
    }

    # Est ce un enlèvement sur place ?
    # OU le transport n'est pas actif ou converti
    if (!&LOC::Util::in_array($tabInfos->{'state.id'}, [STATE_ACTIVE, STATE_CONVERTED]) || $tabInfos->{'isSelf'})
    {
        return 0;
    }

    # Est ce une livraison ?
    if ($tabInfos->{'type'} eq TYPE_DELIVERY)
    {
        # Est ce une ligne de devis non acceptée ou désactivée ?
        if ($tabInfos->{'to.type'} eq FROMTOTYPE_ESTIMATELINE &&
            (!$tabInfos->{'to.tabInfos'}->{'isAccepted'} ||
                $tabInfos->{'to.tabInfos'}->{'tabDetails'}->{'estimate.state.id'} eq LOC::Estimate::Rent::STATE_ABORTED ||
                $tabInfos->{'to.tabInfos'}->{'tabDetails'}->{'state.id'} eq LOC::Estimate::Rent::LINE_STATE_INACTIVE))
        {
            return 0;
        }
    }
    # Est ce une récupération ?
    elsif ($tabInfos->{'type'} eq TYPE_RECOVERY)
    {
        # Est ce une ligne de devis non acceptée ou désactivée ?
        if ($tabInfos->{'from.type'} eq FROMTOTYPE_ESTIMATELINE &&
            (!$tabInfos->{'from.tabInfos'}->{'isAccepted'} ||
                $tabInfos->{'from.tabInfos'}->{'tabDetails'}->{'estimate.state.id'} eq LOC::Estimate::Rent::STATE_ABORTED ||
                $tabInfos->{'from.tabInfos'}->{'tabDetails'}->{'state.id'} eq LOC::Estimate::Rent::LINE_STATE_INACTIVE))
        {
            return 0;
        }

        if (!$tabInfos->{'order.id'})
        {
            # L'état du contrat est "pré-contrat" ou "contrat" ou que c'est une ligne de devis
            # Vérification de la date de fin de location
            if (($tabInfos->{'from.type'} eq FROMTOTYPE_CONTRACT &&
                 ($tabInfos->{'from.tabInfos'}->{'tabDetails'}->{'state.id'} eq LOC::Contract::Rent::STATE_PRECONTRACT ||
                  $tabInfos->{'from.tabInfos'}->{'tabDetails'}->{'state.id'} eq LOC::Contract::Rent::STATE_CONTRACT)) ||
                $tabInfos->{'from.type'} eq FROMTOTYPE_ESTIMATELINE)
            {
                my $deltaMax = &LOC::Characteristic::getAgencyValueByCode('DELTACMDRECUP', $tabInfos->{'agency.id'}) * 1;
                # si le nombre de jours max pour l'envoi de la commande de récupération n'est pas "désactivé"
                if ($deltaMax != -1)
                {
                    # La date de fin de location est dans plus de 5 jours calendaires ?
                    my %date    = &LOC::Date::getHashDate();
                    my %endDate = &LOC::Date::getHashDate($tabInfos->{'from.tabInfos'}->{'endDate'});

                    my $deltaDays = &Date::Calc::Delta_Days($date{'y'}, $date{'m'}, $date{'d'},
                                                            $endDate{'y'}, $endDate{'m'}, $endDate{'d'});

                    if ($deltaDays > $deltaMax)
                    {
                        return 0;
                    }
                }
            }
        }
    }

    return 1;
}


# Function: _isSelfFormPossible
# Indique si l'édition du bordereau d'enlèvement ou de restitution est possible ou non sur un transport

# Parameters:
# hashref $tabInfos - Données du transport
#
# Returns:
# bool
sub _isSelfFormPossible
{
    my ($tabInfos) = @_;

    return ($tabInfos->{'isSelf'} && $tabInfos->{'machine.id'} ? 1 : 0);
}


# Function: _sendSetOrder
# Envoi une modification de commande de transport à OTD
#
# Parameters:
# string  $countryId     - Pays
# hashref $tabOrderInfos - Informations sur la commande
#
# Returns:
# bool
sub _sendSetOrder
{
    my ($countryId, $tabOrderInfos, $tabSendOptions) = @_;


    # Récupération
    my $tabTransportInfos = $tabOrderInfos->{'transport.tabInfos'};

    my $tab = {
        'id'   => $tabOrderInfos->{'code'},
        'date' => $tabOrderInfos->{'date'},

        'agency.id'        => $tabOrderInfos->{'agency.id'},
        'agency.area.code' => $tabOrderInfos->{'agency.area.code'},

        'model' => {
            'id'     => $tabTransportInfos->{'model.id'},
            'weight' => $tabTransportInfos->{'model.tabInfos'}->{'weight'},
            'height' => $tabTransportInfos->{'model.tabInfos'}->{'height'},
            'length' => $tabTransportInfos->{'model.tabInfos'}->{'length'},
            'width'  => $tabTransportInfos->{'model.tabInfos'}->{'width'}
        }
    };

    if ($tabTransportInfos->{'type'} eq TYPE_DELIVERY)
    {
        # Livraison standard
        # ------------------

        my $tabToInfos = $tabTransportInfos->{'to.tabInfos'};

        # Type de commande
        $tab->{'type'} = 'delivery';

        # Informations sur la machine
        $tab->{'machine'} = $tabTransportInfos->{'machine.tabInfos'};
        # - Dans le cas où il n'y a pas de machine attribuée ou associée alors on prend la machine souhaitée si elle existe
        if (!$tab->{'machine'} && $tabToInfos->{'wishedMachine.id'})
        {
            $tab->{'machine'} = &_getMachineInfos($countryId, $tabToInfos->{'wishedMachine.id'});
        }

        # Etat de la commande
        $tab->{'status'} = $tabToInfos->{'status'};

        # Adresse et horaires du chargement
        $tab->{'loading'} = {
            'address' => $tabTransportInfos->{'from.id'},
            'window'  => {'from' => '', 'till' => ''}
        };

        # Adresse et horaires du déchargement
        $tab->{'unloading'} = {
            'address'    => {
                'id'          => $tabToInfos->{'site.id'},
                'name'        => $tabToInfos->{'site.label'},
                'address'     => $tabToInfos->{'site.address'},
                'postalCode'  => $tabToInfos->{'site.postalCode'},
                'city'        => $tabToInfos->{'site.city'},
                'country'     => $tabToInfos->{'site.country.id'},
                'updateCount' => $tabToInfos->{'site.addressUpdateCount'}
            },
            'infos'      => {
                'type'                   => $tabToInfos->{'@type'},
                'code'                   => $tabToInfos->{'code'},
                'beginDate'              => $tabToInfos->{'beginDate'},
                'endDate'                => $tabToInfos->{'endDate'},
                'url'                    => $tabToInfos->{'url'},
                'customer.name'          => $tabToInfos->{'customer.name'},
                'orderNo'                => $tabToInfos->{'orderNo'},
                'orderContact.fullName'  => $tabToInfos->{'orderContact.fullName'},
                'orderContact.telephone' => $tabToInfos->{'orderContact.telephone'},
                'siteContact.fullName'   => $tabToInfos->{'site.contact.fullName'},
                'siteContact.telephone'  => $tabToInfos->{'site.contact.telephone'}
            },
            'window'     => $tabToInfos->{'window'},
            'isHasWharf' => $tabToInfos->{'site.isHasWharf'}
        };

        # Montant du transport
        $tab->{'amount'} = $tabToInfos->{'amount'};
        # Commentaires
        $tab->{'comments'} = $tabToInfos->{'site.comments'};
        # Amiante ?
        $tab->{'isAsbestos'} = $tabToInfos->{'site.isAsbestos'};
        # Livraison anticipée ?
        $tab->{'isAnticipatable'} = $tabToInfos->{'isAnticipatable'};
        # Machine supplémentaire ?
        $tab->{'isAddMachine'} = $tabToInfos->{'isAddMachine'};
        # Url du contrat lié à la machine supplémentaire ?
        $tab->{'addMachineLnkCttUrl'} = $tabToInfos->{'addMachineLnkCtt.url'};
        # Attribution ou association
        $tab->{'isFinalAllocation'} = $tabToInfos->{'isFinalAllocation'};
        # Dates de location à confirmer
        $tab->{'isDatesToConfirm'} = $tabToInfos->{'isDatesToConfirm'};

        # Faisabilité
        $tab->{'isDoable'} = ($tabTransportInfos->{'unloading'}->{'isDone'} || $tabTransportInfos->{'unloading'}->{'isDoable'} ? 1 : 0);
        # Report de réalisation
        $tab->{'doneDate'} = '';
        if ($tabTransportInfos->{'unloading'}->{'isDone'})
        {
            $tab->{'doneDate'} = $tabTransportInfos->{'unloading'}->{'date'};
        }
    }
    elsif ($tabTransportInfos->{'type'} eq TYPE_RECOVERY)
    {
        # Récupération standard
        # ---------------------

        my $tabFromInfos = $tabTransportInfos->{'from.tabInfos'};

        # Type de commande
        $tab->{'type'} = 'recovery';

        # Informations sur la machine
        $tab->{'machine'} = $tabTransportInfos->{'machine.tabInfos'};
        # - Dans le cas où il n'y a pas de machine attribuée ou associée alors on prend la machine souhaitée si elle existe
        if (!$tab->{'machine'} && $tabFromInfos->{'wishedMachine.id'})
        {
            $tab->{'machine'} = &_getMachineInfos($countryId, $tabFromInfos->{'wishedMachine.id'});
        }

        # Etat de la commande
        $tab->{'status'} = $tabFromInfos->{'status'};

        # Adresse et horaires du chargement
        $tab->{'loading'} = {
            'address'    => {
                'id'          => $tabFromInfos->{'site.id'},
                'name'        => $tabFromInfos->{'site.label'},
                'address'     => $tabFromInfos->{'site.address'},
                'postalCode'  => $tabFromInfos->{'site.postalCode'},
                'city'        => $tabFromInfos->{'site.city'},
                'country'     => $tabFromInfos->{'site.country.id'},
                'updateCount' => $tabFromInfos->{'site.addressUpdateCount'}
            },
            'infos'      => {
                'type'                   => $tabFromInfos->{'@type'},
                'code'                   => $tabFromInfos->{'code'},
                'beginDate'              => $tabFromInfos->{'beginDate'},
                'endDate'                => $tabFromInfos->{'endDate'},
                'url'                    => $tabFromInfos->{'url'},
                'customer.name'          => $tabFromInfos->{'customer.name'},
                'orderNo'                => $tabFromInfos->{'orderNo'},
                'orderContact.fullName'  => $tabFromInfos->{'orderContact.fullName'},
                'orderContact.telephone' => $tabFromInfos->{'orderContact.telephone'},
                'siteContact.fullName'   => $tabFromInfos->{'site.contact.fullName'},
                'siteContact.telephone'  => $tabFromInfos->{'site.contact.telephone'}
            },
            'window'     => $tabFromInfos->{'window'},
            'isHasWharf' => $tabFromInfos->{'site.isHasWharf'}
        };

        # Adresse et horaires du déchargement
        $tab->{'unloading'} = {
            'address' => $tabTransportInfos->{'to.id'},
            'window'  => {'from' => '', 'till' => ''}
        };

        # Montant du transport
        $tab->{'amount'} = $tabFromInfos->{'amount'};
        # Commentaires
        $tab->{'comments'} = $tabFromInfos->{'site.comments'};
        # Amiante ?
        $tab->{'isAsbestos'} = $tabFromInfos->{'site.isAsbestos'};
        # Livraison anticipée ?
        $tab->{'isAnticipatable'} = 0;
        # Machine supplémentaire ?
        $tab->{'isAddMachine'} = $tabFromInfos->{'isAddMachine'};
        # Url du contrat lié à la machine supplémentaire ?
        $tab->{'addMachineLnkCttUrl'} = $tabFromInfos->{'addMachineLnkCtt.url'};
        # Attribution ou association
        $tab->{'isFinalAllocation'} = $tabFromInfos->{'isFinalAllocation'};
        # Dates de location à confirmer
        $tab->{'isDatesToConfirm'} = $tabFromInfos->{'isDatesToConfirm'};

        # Faisabilité
        $tab->{'isDoable'} = ($tabTransportInfos->{'loading'}->{'isDone'} || $tabTransportInfos->{'loading'}->{'isDoable'} ? 1 : 0);
        # Report de réalisation
        $tab->{'doneDate'} = '';
        if ($tabTransportInfos->{'loading'}->{'isDone'})
        {
            $tab->{'doneDate'} = $tabTransportInfos->{'loading'}->{'date'};
        }
    }
    elsif ($tabTransportInfos->{'type'} eq TYPE_SITESTRANSFER)
    {
        # Transfert inter-chantiers
        # -------------------------

        my $tabFromInfos = $tabTransportInfos->{'from.tabInfos'};
        my $tabToInfos = $tabTransportInfos->{'to.tabInfos'};

        # Type de commande
        $tab->{'type'} = 'sitesTransfer';

        # Informations sur la machine
        $tab->{'machine'} = $tabTransportInfos->{'machine.tabInfos'};

        # Etat de la commande
        $tab->{'status'} = $tabFromInfos->{'status'};

        # Adresse et horaires du chargement
        $tab->{'loading'} = {
            'address'    => {
                'id'          => $tabFromInfos->{'site.id'},
                'name'        => $tabFromInfos->{'site.label'},
                'address'     => $tabFromInfos->{'site.address'},
                'postalCode'  => $tabFromInfos->{'site.postalCode'},
                'city'        => $tabFromInfos->{'site.city'},
                'country'     => $tabFromInfos->{'site.country.id'},
                'updateCount' => $tabFromInfos->{'site.addressUpdateCount'}
            },
            'infos'      => {
                'type'                   => $tabFromInfos->{'@type'},
                'code'                   => $tabFromInfos->{'code'},
                'beginDate'              => $tabFromInfos->{'beginDate'},
                'endDate'                => $tabFromInfos->{'endDate'},
                'url'                    => $tabFromInfos->{'url'},
                'customer.name'          => $tabFromInfos->{'customer.name'},
                'orderNo'                => $tabFromInfos->{'orderNo'},
                'orderContact.fullName'  => $tabFromInfos->{'orderContact.fullName'},
                'orderContact.telephone' => $tabFromInfos->{'orderContact.telephone'},
                'siteContact.fullName'   => $tabFromInfos->{'site.contact.fullName'},
                'siteContact.telephone'  => $tabFromInfos->{'site.contact.telephone'}
            },
            'window'     => $tabFromInfos->{'window'},
            'isHasWharf' => $tabFromInfos->{'site.isHasWharf'}
        };

        # Adresse et horaires du déchargement
        $tab->{'unloading'} = {
            'address'    => {
                'id'          => $tabToInfos->{'site.id'},
                'name'        => $tabToInfos->{'site.label'},
                'address'     => $tabToInfos->{'site.address'},
                'postalCode'  => $tabToInfos->{'site.postalCode'},
                'city'        => $tabToInfos->{'site.city'},
                'country'     => $tabToInfos->{'site.country.id'},
                'updateCount' => $tabToInfos->{'site.addressUpdateCount'}
            },
            'infos'      => {
                'type'                   => $tabToInfos->{'@type'},
                'code'                   => $tabToInfos->{'code'},
                'beginDate'              => $tabToInfos->{'beginDate'},
                'endDate'                => $tabToInfos->{'endDate'},
                'url'                    => $tabToInfos->{'url'},
                'customer.name'          => $tabToInfos->{'customer.name'},
                'orderNo'                => $tabToInfos->{'orderNo'},
                'orderContact.fullName'  => $tabToInfos->{'orderContact.fullName'},
                'orderContact.telephone' => $tabToInfos->{'orderContact.telephone'},
                'siteContact.fullName'   => $tabToInfos->{'site.contact.fullName'},
                'siteContact.telephone'  => $tabToInfos->{'site.contact.telephone'}
            },
            'window'     => $tabToInfos->{'window'},
            'isHasWharf' => $tabToInfos->{'site.isHasWharf'}
        };

        # Montant du transport
        $tab->{'amount'} = $tabFromInfos->{'amount'} + $tabToInfos->{'amount'};
        # Commentaires
        $tab->{'comments'} = $tabFromInfos->{'site.comments'} . "\n" . $tabToInfos->{'site.comments'};
        # Amiante ?
        $tab->{'isAsbestos'} = $tabFromInfos->{'site.isAsbestos'} || $tabToInfos->{'site.isAsbestos'};
        # Livraison anticipée ?
        $tab->{'isAnticipatable'} = $tabToInfos->{'isAnticipatable'};
        # Machine supplémentaire ?
        $tab->{'isAddMachine'} = $tabToInfos->{'isAddMachine'};
        # Url du contrat lié à la machine supplémentaire ?
        $tab->{'addMachineLnkCttUrl'} = $tabToInfos->{'addMachineLnkCtt.url'};
        # Dans le cas d'un transfert inter-chantiers, il s'agit forcément d'une attribution
        $tab->{'isFinalAllocation'} = 1;
        # Dates de location à confirmer
        $tab->{'isDatesToConfirm'} = undef;

        # Faisabilité
        $tab->{'isDoable'} = (($tabTransportInfos->{'loading'}->{'isDone'} || $tabTransportInfos->{'loading'}->{'isDoable'}) &&
                              ($tabTransportInfos->{'unloading'}->{'isDone'} || $tabTransportInfos->{'unloading'}->{'isDoable'}) ? 1 : 0);
        # Report de réalisation
        $tab->{'doneDate'} = '';
        if ($tabTransportInfos->{'unloading'}->{'isDone'})
        {
            $tab->{'doneDate'} = $tabTransportInfos->{'unloading'}->{'date'};
        }
    }
    elsif ($tabTransportInfos->{'type'} eq TYPE_AGENCIESTRANSFER)
    {
        # Transfert inter-agences
        # -----------------------

        # Etat de la commande
        $tab->{'status'} = 'agenciesTransfer';
        # Type de commande
        $tab->{'type'} = 'agenciesTransfer';
        # Montant du transport
        $tab->{'amount'} = 0;
        # Informations sur la machine
        $tab->{'machine'} = $tabTransportInfos->{'machine.tabInfos'};
        # Faisabilité
        $tab->{'isDoable'} = ($tabTransportInfos->{'unloading'}->{'isDone'} || $tabTransportInfos->{'unloading'}->{'isDoable'} ? 1 : 0);
        # Livraison anticipée ?
        $tab->{'isAnticipatable'} = undef;
        # Machine supplémentaire ?
        $tab->{'isAddMachine'} = undef;
        # Attribution définitive
        $tab->{'isFinalAllocation'} = undef;
        # Date du transport
        $tab->{'trspDate'} = $tabTransportInfos->{'date'};
        # Adresse et horaires du chargement
        $tab->{'loading'} = {
            'address' => $tabTransportInfos->{'from.id'},
            'window'  => {'from' => $tabTransportInfos->{'date'} . ' ' . DEFAULT_BEGINTIME,
                          'till' => $tabTransportInfos->{'date'} . ' ' . DEFAULT_ENDTIME}
        };
        # Adresse et horaires du déchargement
        $tab->{'unloading'} = {
            'address' => $tabTransportInfos->{'to.id'},
            'window'  => {'from' => $tabTransportInfos->{'date'} . ' ' . DEFAULT_BEGINTIME,
                          'till' => $tabTransportInfos->{'date'} . ' ' . DEFAULT_ENDTIME}
        };

        # Report de réalisation
        $tab->{'doneDate'} = '';
        if ($tabTransportInfos->{'unloading'}->{'isDone'})
        {
            $tab->{'doneDate'} = $tabTransportInfos->{'unloading'}->{'date'};
        }
    }

    # Envoi de la trame à OTD
    return &LOC::Otd::setOrder($countryId, $tab, $tabSendOptions);
}


# Function: _set
# Insertion ou modification d'une ligne de transport
#
# Parameters:
# LOC::Db::Adapter $db         - Connexion à la base de données
# string           $countryId  - Pays
# hashref          $tabInfos   - Informations sur le transport
# hashref          $tabOptions - Options supplémentaires
#
# Returns:
# bool
sub _set
{
    my ($db, $countryId, $tabInfos, $tabOptions) = @_;

    # Nom du schéma de base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Calcul de la faisabilité
    if (!$tabOptions->{'isCalculateDoabilityDisabled'})
    {
        &_calculateDoability($countryId, $tabInfos);
    }

    # Création du transport
    my $tabUpdates = {
        'tsp_type'               => $tabInfos->{'type'},
        'tsp_is_self'            => $tabInfos->{'isSelf'},
        'tsp_agc_id'             => $tabInfos->{'agency.id'},
        'tsp_agc_id_delegated'   => $tabInfos->{'delegatedAgency.id'},
        'tsp_agc_id_transferto'  => $tabInfos->{'transferToAgency.id'},
        'tsp_mod_id'             => $tabInfos->{'model.id'},
        'tsp_mac_id'             => ($tabInfos->{'machine.id'} ? $tabInfos->{'machine.id'} : undef),
        'tsp_agc_id_from'        => undef,
        'tsp_rel_id_from'        => undef,
        'tsp_rct_id_from'        => undef,
        'tsp_agc_id_to'          => undef,
        'tsp_rel_id_to'          => undef,
        'tsp_rct_id_to'          => undef,
        'tsp_is_loadingdone'     => $tabInfos->{'loading'}->{'isDone'},
        'tsp_is_loadingdoable'   => $tabInfos->{'loading'}->{'isDoable'},
        'tsp_loadingerrors'      => $tabInfos->{'loading'}->{'errors'},
        'tsp_is_unloadingdone'   => $tabInfos->{'unloading'}->{'isDone'},
        'tsp_is_unloadingdoable' => $tabInfos->{'unloading'}->{'isDoable'},
        'tsp_unloadingerrors'    => $tabInfos->{'unloading'}->{'errors'},
        'tsp_sta_id'             => $tabInfos->{'state.id'},
        'tsp_is_locked'          => ($tabInfos->{'isLocked'} ? 1 : 0),
        'tsp_is_synchrodisabled' => ($tabInfos->{'isSynchroDisabled'} ? 1 : 0),
        'tsp_considertype'      => ($tabInfos->{'isConsidered'} ? $tabInfos->{'isConsidered'} : CONSIDERTYPE_NONE),
        'tsp_usr_id_considered'  => $tabInfos->{'consideredUser.id'},
        'tsp_date_value'         => $tabInfos->{'valueDate'}
    };


    # Date de réalisation
    if ($tabInfos->{'loading'}->{'isDone'} == 0)
    {
        $tabUpdates->{'tsp_date_loading'} = undef;
    }
    elsif ($tabInfos->{'loading'}->{'date'} eq 'now')
    {
        $tabUpdates->{'tsp_date_loading*'} = 'Now()';
    }
    else
    {
        $tabUpdates->{'tsp_date_loading'} = $tabInfos->{'loading'}->{'date'};
    }

    if ($tabInfos->{'unloading'}->{'isDone'} == 0)
    {
        $tabUpdates->{'tsp_date_unloading'} = undef;
    }
    elsif ($tabInfos->{'unloading'}->{'date'} eq 'now')
    {
        $tabUpdates->{'tsp_date_unloading*'} = 'Now()';
    }
    else
    {
        $tabUpdates->{'tsp_date_unloading'} = $tabInfos->{'unloading'}->{'date'};
    }

    # Date de prise en compte
    if ($tabInfos->{'consideredDate'} eq 'now')
    {
        $tabUpdates->{'tsp_date_considered*'} = 'Now()';
    }
    else
    {
        $tabUpdates->{'tsp_date_considered'} = $tabInfos->{'consideredDate'};
    }

    # Provenance
    if ($tabInfos->{'from.type'} eq FROMTOTYPE_AGENCY)
    {
        $tabUpdates->{'tsp_agc_id_from'} = $tabInfos->{'from.id'};
    }
    elsif ($tabInfos->{'from.type'} eq FROMTOTYPE_ESTIMATELINE)
    {
        $tabUpdates->{'tsp_rel_id_from'} = $tabInfos->{'from.id'};
    }
    elsif ($tabInfos->{'from.type'} eq FROMTOTYPE_CONTRACT)
    {
        $tabUpdates->{'tsp_rct_id_from'} = $tabInfos->{'from.id'};
    }

    # Destination
    if ($tabInfos->{'to.type'} eq FROMTOTYPE_AGENCY)
    {
        $tabUpdates->{'tsp_agc_id_to'} = $tabInfos->{'to.id'};
    }
    elsif ($tabInfos->{'to.type'} eq FROMTOTYPE_ESTIMATELINE)
    {
        $tabUpdates->{'tsp_rel_id_to'} = $tabInfos->{'to.id'};
    }
    elsif ($tabInfos->{'to.type'} eq FROMTOTYPE_CONTRACT)
    {
        $tabUpdates->{'tsp_rct_id_to'} = $tabInfos->{'to.id'};
    }

    if ($tabInfos->{'date'})
    {
        $tabUpdates->{'tsp_date'}  = $tabInfos->{'date'};
    }
    elsif ($tabInfos->{'type'} eq TYPE_DELIVERY)
    {
        $tabUpdates->{'tsp_date'} = $tabInfos->{'to.tabInfos'}->{'beginDate'};
    }
    elsif ($tabInfos->{'type'} eq TYPE_RECOVERY || $tabInfos->{'type'} eq TYPE_SITESTRANSFER)
    {
        $tabUpdates->{'tsp_date'} = $tabInfos->{'from.tabInfos'}->{'endDate'};
    }


    if ($tabInfos->{'id'})
    {
        # Modification de la ligne en base de données
        my $id = $db->update('f_transport', $tabUpdates, {'tsp_id' => $tabInfos->{'id'}}, $schemaName);
        if ($id == -1)
        {
            return 0;
        }

        # Suppression du renouvellement du transport si demandé ultérieurement
        if (!$tabOptions->{'isCalculateDoabilityDisabled'})
        {
            $db->deleteTransactionActions('commit.pre', {
                'category' => 'transport',
                'id' => 'LOC::Transport::renew_' . $countryId . '-id:' . $tabInfos->{'id'}
            });
        }
    }
    elsif ($tabInfos->{'state.id'} ne STATE_DELETED)
    {
        # Création de la ligne en base de données
        $tabUpdates->{'tsp_considertype'}     = 0;
        $tabUpdates->{'tsp_usr_id_considered'} = undef;
        $tabUpdates->{'tsp_date_considered'}   = undef;
        $tabUpdates->{'tsp_is_loadingdone'}    = 0;
        $tabUpdates->{'tsp_is_unloadingdone'}  = 0;
        $tabUpdates->{'tsp_date_create*'}      = 'Now()';

        my $id = $db->insert('f_transport', $tabUpdates, $schemaName);
        if ($id == -1)
        {
            return 0;
        }

        $tabInfos->{'id'} = $id;
    }


    # Mise à jour du bordereau
    if (!$tabOptions->{'isUpdateSelfFormDisabled'})
    {
        # Si un bordereau existe
        my $deliverySelfFormId = $tabInfos->{'deliverySelfForm.id'};
        my $recoverySelfFormId = $tabInfos->{'recoverySelfForm.id'};

        if ($deliverySelfFormId || $recoverySelfFormId)
        {
            # Est-il possible d'éditer un bordereau ?, si non alors on supprimera le bordereau existant
            my $tabSelfFormUpdates = {};
            if (&_isSelfFormPossible($tabInfos))
            {
                # Mise à jour du transport associé
                $tabSelfFormUpdates->{'tsf_tsp_id'} = $tabInfos->{'id'};
            }
            else
            {
                # Suppression du bordereau
                $tabSelfFormUpdates->{'tsf_sta_id'} = SELFFORM_STATE_DELETED;
            }

            my @tabWhereValues;
            if ($deliverySelfFormId)
            {
                push(@tabWhereValues, $deliverySelfFormId);
            }
            if ($recoverySelfFormId)
            {
                push(@tabWhereValues, $recoverySelfFormId);
            }

            # Mise à jour de la commande
            if ($db->update('f_transportselfform',
                            $tabSelfFormUpdates,
                            {'tsf_id*' => \@tabWhereValues},
                            $schemaName) == -1)
            {
                return 0;
            }
        }
    }


    # Mise à jour de la commande
    my $orderIdToSend = 0;
    if (!$tabOptions->{'isUpdateOrderDisabled'})
    {
        # Faut-il gérer la commande ?, si non alors on supprimera la commande
        my $isOrderManaged = &_isOrderManaged($tabInfos);

        # Agence qui gère la commande
        my $orderAgencyId = ($tabInfos->{'delegatedAgency.id'} ne '' ? $tabInfos->{'delegatedAgency.id'} : $tabInfos->{'agency.id'});

        if (!$tabInfos->{'order.id'})
        {
            if ($isOrderManaged)
            {
                # Création de la commande
                my $tabOrderUpdates = {
                    'tor_tsp_id'        => $tabInfos->{'id'},
                    'tor_date*'         => 'Now()',
                    'tor_agc_id'        => $orderAgencyId,
                    'tor_sendingscount' => 0,
                    'tor_date_create*'  => 'Now()'
                };

                # Insertion de la commande
                my $orderId = $db->insert('f_transportorder', $tabOrderUpdates, $schemaName);
                if ($orderId == -1)
                {
                    return 0;
                }

                # Mise à jour du code de la commande
                my $orderCode = sprintf('%02s%07d', $countryId, $orderId);
                if ($db->update('f_transportorder', {'tor_code' => $orderCode}, {'tor_id*' => $orderId}, $schemaName) == -1)
                {
                    return 0;
                }

                # Envoi de la commande à OTD
                $orderIdToSend = $orderId;


                # Mise à jour de l'identifiant de la commande
                $tabInfos->{'order.id'} = $orderId;
            }
        }
        else
        {
            my $tabOrderUpdates;
            my $orderId = $tabInfos->{'order.id'};

            if ($isOrderManaged)
            {
                if ($tabInfos->{'state.id'} ne STATE_CONVERTED)
                {
                    # Modification de la commande
                    $tabOrderUpdates = {
                        'tor_tsp_id' => $tabInfos->{'id'},
                        'tor_agc_id' => $orderAgencyId
                    };
                }
            }
            else
            {
                # Suppression de la commande
                $tabOrderUpdates = {
                    'tor_sta_id' => ORDER_STATE_DELETED
                };

                # Mise à jour de l'identifiant de la commande
                $tabInfos->{'order.id'} = undef;
            }

            if (keys(%$tabOrderUpdates) > 0)
            {
                # Insertion de la commande
                if ($db->update('f_transportorder', $tabOrderUpdates, {'tor_id*' => $orderId}, $schemaName) == -1)
                {
                    return 0;
                }
            }

            # Envoi de la commande à OTD
            $orderIdToSend = $orderId;
        }
    }
    else
    {
        $orderIdToSend = $tabInfos->{'order.id'};
    }

    # Envoi de la commande à OTD
    if ($orderIdToSend)
    {
        # Envoi de la commande à OTD
        $db->addTransactionAction('commit.post', sub {
                return &sendOrder($countryId, $orderIdToSend, $tabOptions);
            },
            {'category' => 'transport',
             'priority' => -1000,
             'id' => 'LOC::Transport::sendOrder_' . $countryId . '-id:' . $orderIdToSend}
        );
    }

    # Mise à jour des transports en cours
    if ($tabInfos->{'id'})
    {
        if (!&_updatePendingTransport($db, $countryId, $tabInfos->{'id'}, $tabInfos))
        {
            return 0;
        }
    }

    return 1;
}



# Function: _undoUnloading
# Annule la réalisation du déchargement
#
# Parameters:
# LOC::Db::Adapter $db               - Connexion à la base de données
# string           $countryId        - Pays
# hashref          $tabOldInfos      - Informations sur le transport
# string           $documentType     - Type du document
# int              $userId           - Utilisateur responsable
# hashref          $tabEndUpds       - Mises à jour de fin (facultatif)
#
# Returns:
# bool
sub _undoUnloading
{
    my ($db, $countryId, $tabOldInfos, $documentType, $userId, $tabEndUpds) = @_;

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # nouvelles infos transport
    my $tabNewInfos = &LOC::Util::clone($tabOldInfos);

    # Mise à jour machine
    my $tabMachineUpdates = {
        'state.id'    => LOC::Machine::STATE_DELIVERY,
        'contract.id' => $tabOldInfos->{'to.id'}
    };
    my $tabMachineOptions = {};

    # Mises à jour supplémentaires concernant la visibilité
    my $fromAgencyId = ($tabOldInfos->{'type'} eq TYPE_DELIVERY ? $tabOldInfos->{'from.id'} : $tabOldInfos->{'from.tabInfos'}->{'agency.id'});
    my $toAgencyId = $tabOldInfos->{'to.tabInfos'}->{'agency.id'};

    $tabMachineUpdates->{'agency.id'} = $fromAgencyId;
    # si l'agence de provenance du transport est différente de l'agence du contrat de destination
    if ($fromAgencyId ne $toAgencyId)
    {
        $tabMachineOptions->{'updateFrom'} = 'internal';
        $tabMachineUpdates->{'visibleOnAgency.id'} = $toAgencyId;
        $tabMachineUpdates->{'visibilityMode'}     = ($tabOldInfos->{'transferToAgency.id'} ?
                                                        LOC::Machine::VISIBILITYMODE_TRANSPORT :
                                                        LOC::Machine::VISIBILITYMODE_MACHINE);
    }

    if (!&LOC::Machine::update($countryId, $tabOldInfos->{'machine.id'}, $tabMachineUpdates, $userId, $tabMachineOptions, undef, $tabEndUpds))
    {
        return 0;
    }

    # mise à jour contrat
    if ($db->update('CONTRAT', {'ETCODE'          => LOC::Contract::Rent::STATE_PRECONTRACT,
                                'LIETCONTRATAUTO' => substr(LOC::Contract::Rent::STATE_PRECONTRACT, -2) * 1},
                               {'CONTRATAUTO*' => $tabOldInfos->{'to.id'}}, $schemaName) == -1)
    {
        return 0;
    }

    # Mise à jour du transport
    $tabNewInfos->{'to.tabInfos'}->{'machine.tabInfos'}->{'state.id'} = LOC::Machine::STATE_DELIVERY;
    $tabNewInfos->{'to.tabInfos'}->{'status'}                         = LOC::Contract::Rent::STATE_PRECONTRACT;
    $tabNewInfos->{'to.tabInfos'}->{'tabDetails'}->{'state.id'}       = LOC::Contract::Rent::STATE_PRECONTRACT;
    $tabNewInfos->{'unloading'}->{'isDone'} = ACTION_NOTDONE;
    $tabNewInfos->{'unloading'}->{'date'}   = undef;
    if ($tabOldInfos->{'type'} ne TYPE_SITESTRANSFER)
    {
        $tabNewInfos->{'loading'}->{'isDone'} = ACTION_NOTDONE;
    }

    if (!&_set($db, $countryId, $tabNewInfos, {}))
    {
        return 0;
    }

    # Mise à jour de la récupération du document
    my $tabRecoveryInfos = &getDocumentRecoveryInfos($countryId, $documentType, $tabOldInfos->{'to.id'},
                                                     GETINFOS_FROM | GETINFOS_TO | GETINFOS_MACHINE,
                                                     {'getFromInfosFlags' => GETFROMTOINFOS_DETAILS,
                                                      'getToInfosFlags'   => GETFROMTOINFOS_DETAILS});

    if (!&_set($db, $countryId, $tabRecoveryInfos, {}))
    {
        return 0;
    }

    # Mise à jour des caractéristiques Kimoce de la machine si livraison standard
    if ($tabOldInfos->{'type'} eq TYPE_DELIVERY)
    {
        &LOC::EndUpdates::updateKimoceMachine($tabEndUpds, $tabRecoveryInfos->{'machine.tabInfos'}->{'parkNumber'},
                                              LOC::EndUpdates::KIMUPD_FEATURES);
    }

    # Mise à jour des caractéristiques Kimoce de la machine si livraison standard
    if ($tabOldInfos->{'type'} eq TYPE_DELIVERY)
    {
        &LOC::EndUpdates::updateKimoceMachine($tabEndUpds, $tabRecoveryInfos->{'machine.tabInfos'}->{'parkNumber'},
                                              LOC::EndUpdates::KIMUPD_FEATURES);
    }

    # Mise à jour alertes "Transports en cours" ou "Enlèvements en attente"
    &_updateAlerts($tabOldInfos, $tabNewInfos, {}, $tabEndUpds);

    # Si l'annulation du déchargement a changé
    if ($tabOldInfos->{'unloading'}->{'isDone'} != $tabNewInfos->{'unloading'}->{'isDone'})
    {
        # Mise à jour des alertes "Précontrats"
        # - Décrémente l'alerte "Pré-contrats effectués"
        &LOC::EndUpdates::updateAlert($tabEndUpds, $tabOldInfos->{'agency.id'}, LOC::Alert::PRECONTRACTS, +1);

        # Mise à jour des traces
        my $tabTrace = {
            'schema' => $schemaName,
            'code'   => LOC::Log::EventType::UNDODELIVERY
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $tabOldInfos->{'to.id'}, $tabTrace);
    }

    # Mise à jour des infos d'origine
    $tabOldInfos->{'loading'}       = $tabNewInfos->{'loading'};
    $tabOldInfos->{'unloading'}     = $tabNewInfos->{'unloading'};
    $tabOldInfos->{'to.tabInfos'}   = $tabNewInfos->{'to.tabInfos'};

    return 1;
}


# Function: _undoLoading
# Annule la réalisation du chargement
#
# Parameters:
# LOC::Db::Adapter $db               - Connexion à la base de données
# string           $countryId        - Pays
# hashref          $tabOldInfos      - Informations sur le transport
# string           $documentType     - Type du document
# int              $userId           - Utilisateur responsable
# hashref          $tabEndUpds       - Mises à jour de fin (facultatif)
#
# Returns:
# bool
sub _undoLoading
{
    my ($db, $countryId, $tabOldInfos, $documentType, $userId, $tabEndUpds) = @_;

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # nouvelles infos du transport
    my $tabNewInfos = &LOC::Util::clone($tabOldInfos);

    # informations sur la machine
    my $tabMachineInfos  = &LOC::Machine::getInfos($countryId, $tabOldInfos->{'machine.id'}, LOC::Machine::GETINFOS_BASE);


    # Mise à jour du contrat de livraison si transfert
    if($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER)
    {
        my $tabContractUpdates = {
            'machine.id'        => $tabOldInfos->{'machine.id'},
            'isFinalAllocation' => 0
        };
        if (!&LOC::Contract::Rent::update($countryId, $tabOldInfos->{'to.id'}, $tabContractUpdates, $userId, undef, $tabEndUpds))
        {
            return 0;
        }
    }

    # Mise à jour machine
    my $tabMachineUpdates = {
        'state.id'    => LOC::Machine::STATE_RECOVERY,
        'contract.id' => $tabOldInfos->{'from.id'}
    };
    my $tabMachineOptions = {};

    # Mises à jour supplémentaires concernant la visibilité
    # - dans le cas d'un transfert inter-chantiers
    if ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER)
    {
        $tabMachineUpdates->{'agency.id'} = $tabOldInfos->{'from.tabInfos'}->{'agency.id'};
        # si l'agence de provenance du transport est différente de l'agence du contrat de destination
        if ($tabOldInfos->{'from.tabInfos'}->{'agency.id'} ne $tabOldInfos->{'to.tabInfos'}->{'agency.id'})
        {
            $tabMachineOptions->{'updateFrom'} = 'internal';
            $tabMachineUpdates->{'visibleOnAgency.id'} = $tabOldInfos->{'to.tabInfos'}->{'agency.id'};
            $tabMachineUpdates->{'visibilityMode'}     = LOC::Machine::VISIBILITYMODE_TRANSPORT;
        }
    }

    if (!&LOC::Machine::update($countryId, $tabOldInfos->{'machine.id'}, $tabMachineUpdates, $userId, $tabMachineOptions, undef, $tabEndUpds))
    {
        return 0;
    }

    # Mise à jour transport
    my $tabDeliveryInfos = &getDocumentDeliveryInfos($countryId, $documentType, $tabOldInfos->{'from.id'}, GETINFOS_BASE);

    $tabNewInfos->{'loading'}->{'isDone'}                  = ACTION_NOTDONE;
    $tabNewInfos->{'loading'}->{'date'}                    = undef;

    if ($tabOldInfos->{'type'} ne TYPE_SITESTRANSFER)
    {
        $tabNewInfos->{'unloading'}->{'isDone'} = ACTION_NOTDONE;
    }
    $tabNewInfos->{'from.tabInfos'}->{'machine.tabInfos'}->{'state.id'} = LOC::Machine::STATE_RECOVERY;
    $tabNewInfos->{'from.tabInfos'}->{'isFinalAllocation'} = 1;

    if (!&_set($db, $countryId, $tabNewInfos, {}))
    {
        return 0;
    }

    # Mise à jour alertes "Transports en cours" ou "Enlèvements en attente"
    &_updateAlerts($tabOldInfos, $tabNewInfos, {}, $tabEndUpds);

    # Si la réalisation du déchargement a changé
    if ($tabOldInfos->{'loading'}->{'isDone'} != $tabNewInfos->{'loading'}->{'isDone'})
    {
        # Mise à jour des traces
        my $tabTrace = {
            'schema' => $schemaName,
            'code'   => LOC::Log::EventType::UNDORECOVERY
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $tabOldInfos->{'from.id'}, $tabTrace);
    }

    return 1;
}


# Function: _undoAgenciesTransfer
# Annule la réalisation du transfert inter-agence
#
# Parameters:
# LOC::Db::Adapter $db               - Connexion à la base de données
# string           $countryId        - Pays
# hashref          $tabOldInfos      - Informations sur le transport
# int              $userId           - Utilisateur responsable
# hashref          $tabEndUpds       - Mises à jour de fin (facultatif)
#
# Returns:
# bool
sub _undoAgenciesTransfer
{
    my ($db, $countryId, $tabOldInfos, $userId, $tabEndUpds) = @_;

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # nouvelles infos transport
    my $tabNewInfos = &LOC::Util::clone($tabOldInfos);

    # Mise à jour machine
    my $tabMachineUpdates = {
        'state.id'           => LOC::Machine::STATE_TRANSFER,
        'agency.id'          => $tabOldInfos->{'from.id'}};

    my $tabMachineOptions = {};

    # Récupération des informations de l'agence de provenance initiale
    my $tabOldAgencyFromInfos = &LOC::Agency::getInfos($tabOldInfos->{'from.id'});

    # Récupération de la liste des agences de visibilité se trouvant dans le pays de l'agence de provenance
    my $tabVisibilityAgencies = &LOC::Agency::getList(LOC::Util::GETLIST_PAIRS,
                                                      {'country'    => $tabOldAgencyFromInfos->{'country.id'},
                                                       'exceptions' => $tabOldInfos->{'from.id'}});

    my @tabVisibilityAgenciesKeys = keys %$tabVisibilityAgencies;

    # Mise à jour de la visibilité si l'agence de destination se trouve dans le pays de la machine
    if (&LOC::Util::in_array($tabOldInfos->{'to.id'}, \@tabVisibilityAgenciesKeys))
    {
        $tabMachineOptions->{'updateFrom'} = 'internal';
        $tabMachineUpdates->{'visibleOnAgency.id'} = $tabOldInfos->{'to.id'};
        $tabMachineUpdates->{'visibilityMode'}     = LOC::Machine::VISIBILITYMODE_MACHINE;
    }

    if (!&LOC::Machine::update($countryId, $tabOldInfos->{'machine.id'}, $tabMachineUpdates, $userId, $tabMachineOptions, undef, $tabEndUpds))
    {
        return 0;
    }

    # Mise à jour du transport
    $tabNewInfos->{'machine.tabInfos'}      = &_getMachineInfos($countryId, $tabOldInfos->{'machine.id'});
    $tabNewInfos->{'unloading'}->{'isDone'} = ACTION_NOTDONE;
    $tabNewInfos->{'unloading'}->{'date'}   = undef;
    $tabNewInfos->{'loading'}->{'isDone'}   = ACTION_NOTDONE;

    if (!&_set($db, $countryId, $tabNewInfos, {}))
    {
        return 0;
    }

    # Mise à jour alertes "Transports en cours"
    &_updateAlerts($tabOldInfos, $tabNewInfos, {}, $tabEndUpds);

    # Si l'annulation du déchargement a changé
    if ($tabOldInfos->{'unloading'}->{'isDone'} != $tabNewInfos->{'unloading'}->{'isDone'})
    {
        # Mise à jour des traces
        my $tabTrace = {
            'schema' => &LOC::Db::getSchemaName($countryId),
            'code'   => LOC::Log::EventType::UNDOAGENCIESTRANSFER
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $tabOldInfos->{'machine.id'}, $tabTrace);
    }

    # Mise à jour des infos d'origine
    $tabOldInfos->{'loading'}       = $tabNewInfos->{'loading'};
    $tabOldInfos->{'unloading'}     = $tabNewInfos->{'unloading'};

    return 1;

}

# Function: _updateAlerts
# Mise à jour des alertes transports
#
# Parameters:
# hashref|undef    $tabOldInfos        - Ancienne informations du transport
# hashref|undef    $tabNewInfos        - Nouvelle informations du transport
# hashref          $tabDocumentChanges - Tableau des modifications du document pouvant impacter le transport
# hashref          $tabEndUpds         - Mises à jour de fin
#
# Returns:
# void
sub _updateAlerts
{
    my ($tabOldInfos, $tabNewInfos, $tabDocumentChanges, $tabEndUpds) = @_;

    # Décrémentation des alertes de l'ancien transport
    # - on a un ancien transport ET il est actif
    if ($tabOldInfos && $tabOldInfos->{'state.id'} eq STATE_ACTIVE)
    {
        if (($tabOldInfos->{'to.type'} ne FROMTOTYPE_ESTIMATELINE || (exists $tabDocumentChanges->{'isAccepted'} ?
                                                                      $tabDocumentChanges->{'isAccepted'}->{'old'} :
                                                                      $tabOldInfos->{'to.tabInfos'}->{'isAccepted'})) &&
            ($tabOldInfos->{'from.type'} ne FROMTOTYPE_ESTIMATELINE || (exists $tabDocumentChanges->{'isAccepted'} ?
                                                                        $tabDocumentChanges->{'isAccepted'}->{'old'} :
                                                                        $tabOldInfos->{'from.tabInfos'}->{'isAccepted'})))
        {
            my $agencyId = ($tabOldInfos->{'delegatedAgency.id'} ne '' ? $tabOldInfos->{'delegatedAgency.id'} : $tabOldInfos->{'agency.id'});

            my $isDone = 0;
            # Condition sur la réalisation du transport
            if ($tabOldInfos->{'type'} eq TYPE_DELIVERY)
            {
                $isDone = $tabOldInfos->{'unloading'}->{'isDone'};
            }
            elsif ($tabOldInfos->{'type'} eq TYPE_RECOVERY)
            {
                $isDone = $tabOldInfos->{'loading'}->{'isDone'};
            }
            elsif ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER)
            {
                $isDone = ($tabOldInfos->{'unloading'}->{'isDone'} && $tabOldInfos->{'loading'}->{'isDone'});
            }
            elsif ($tabOldInfos->{'type'} eq TYPE_AGENCIESTRANSFER)
            {
                 $isDone = $tabOldInfos->{'unloading'}->{'isDone'};
            }
            if (!$isDone)
            {
                # Si le transport était un enlèvement sur place
                if ($tabOldInfos->{'isSelf'})
                {
                    # Mise à jour de l'alerte des enlèvements en attente
                    &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::PENDINGSELFTRANSPORTS, -1);
                }
                else
                {
                    # Mise à jour de l'alerte des transports en cours
                    &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::PENDINGTRANSPORTS, -1);
                }
            }

            # Alertes technique : transferts inter-chantiers et récupérations
            my $oldStateId = (exists $tabDocumentChanges->{'state.id'} ?
                                $tabDocumentChanges->{'state.id'}->{'old'} :
                                $tabOldInfos->{'from.tabInfos'}->{'tabDetails'}->{'state.id'});

            if ($tabOldInfos->{'isConsidered'} == CONSIDERTYPE_NONE &&
                &LOC::Util::in_array($oldStateId,
                                     [LOC::Contract::Rent::STATE_STOPPED,
                                      LOC::Contract::Rent::STATE_BILLED]))
            {
                if ($tabOldInfos->{'type'} eq TYPE_RECOVERY)
                {
                    # Mise à jour de l'alerte des récupérations
                    &LOC::EndUpdates::updateAlert($tabEndUpds, $tabOldInfos->{'to.id'}, LOC::Alert::ENDINGCONTRACTSRECOVERIES, -1);
                }
                elsif ($tabOldInfos->{'type'} eq TYPE_SITESTRANSFER)
                {
                    # Mise à jour de l'alerte des transferts inter-chantiers
                    &LOC::EndUpdates::updateAlert($tabEndUpds, $tabOldInfos->{'to.tabInfos'}->{'agency.id'}, LOC::Alert::ENDINGCONTRACTSTRANSFERS, -1);
                }
            }
        }
    }

    # Incrémentation des alertes du nouveau transport
    if ($tabNewInfos && $tabNewInfos->{'state.id'} eq STATE_ACTIVE)
    {
        if (($tabNewInfos->{'to.type'} ne FROMTOTYPE_ESTIMATELINE || $tabNewInfos->{'to.tabInfos'}->{'isAccepted'}) &&
            ($tabNewInfos->{'from.type'} ne FROMTOTYPE_ESTIMATELINE || $tabNewInfos->{'from.tabInfos'}->{'isAccepted'}))
        {
            my $agencyId = ($tabNewInfos->{'delegatedAgency.id'} ne '' ? $tabNewInfos->{'delegatedAgency.id'} : $tabNewInfos->{'agency.id'});

            my $isDone = 0;
            # Condition sur la réalisation du transport
            if ($tabNewInfos->{'type'} eq TYPE_DELIVERY)
            {
                $isDone = $tabNewInfos->{'unloading'}->{'isDone'};
            }
            elsif ($tabNewInfos->{'type'} eq TYPE_RECOVERY)
            {
                $isDone = $tabNewInfos->{'loading'}->{'isDone'};
            }
            elsif ($tabNewInfos->{'type'} eq TYPE_SITESTRANSFER)
            {
                $isDone = ($tabNewInfos->{'unloading'}->{'isDone'} && $tabNewInfos->{'loading'}->{'isDone'});
            }
            elsif ($tabNewInfos->{'type'} eq TYPE_AGENCIESTRANSFER)
            {
                 $isDone = $tabNewInfos->{'unloading'}->{'isDone'};
            }
            if (!$isDone)
            {
                # Si le transport est un enlèvement sur place
                if ($tabNewInfos->{'isSelf'})
                {
                    # Mise à jour de l'alerte des enlèvements en attente
                    &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::PENDINGSELFTRANSPORTS, +1);
                }
                else
                {
                    # Mise à jour de l'alerte des transports en cours
                    &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::PENDINGTRANSPORTS, +1);
                }
            }

            # Alertes technique : transferts inter-chantiers et récupérations
            my $newStateId = (exists $tabDocumentChanges->{'state.id'} ?
                                $tabDocumentChanges->{'state.id'}->{'new'} :
                                $tabNewInfos->{'from.tabInfos'}->{'tabDetails'}->{'state.id'});

            if ($tabNewInfos->{'isConsidered'} == CONSIDERTYPE_NONE &&
                &LOC::Util::in_array($newStateId,
                                     [LOC::Contract::Rent::STATE_STOPPED,
                                      LOC::Contract::Rent::STATE_BILLED]))
            {
                if ($tabNewInfos->{'type'} eq TYPE_RECOVERY)
                {
                    # Mise à jour de l'alerte des récupérations
                    &LOC::EndUpdates::updateAlert($tabEndUpds, $tabNewInfos->{'to.id'}, LOC::Alert::ENDINGCONTRACTSRECOVERIES, +1);
                }
                elsif ($tabNewInfos->{'type'} eq TYPE_SITESTRANSFER)
                {
                    # Mise à jour de l'alerte des transferts inter-chantiers
                    &LOC::EndUpdates::updateAlert($tabEndUpds, $tabNewInfos->{'to.tabInfos'}->{'agency.id'}, LOC::Alert::ENDINGCONTRACTSTRANSFERS, +1);
                }
            }
        }
    }
}


# Function: _formatTemplate
# formate une chaîne de caractères
#
# Params:
# string    $code          - quel formatage à appliquer
# string    $value         - la chaîne template à formater
# hashref   $tabReplaces   - les valeurs de la chaîne à mettre dans le template
#
# Return:
# string - la chaîne formatée
sub _formatTemplate
{
    my ($code, $value, $tabReplaces) = @_;

    my @params = split(/\|/, $value);

    if ($code eq 'dte')
    {
        return &LOC::Template::formatDate($value, $tabReplaces);
    }
    # Types de modification de pro forma
    if ($code eq 'mod')
    {
        my $tabModif = $tabReplaces->{'agenciesTransfer.modifications'};
        if ((&LOC::Util::in_array('create', $tabModif) && $params[1] eq 'create') ||
            (&LOC::Util::in_array('modif', $tabModif) && $params[1] eq 'modif')||
            (&LOC::Util::in_array('suppr', $tabModif) && $params[1] eq 'suppr')
            )
        {
            return $params[0];
        }
        else
        {
            return '';
        }
    }

    if ($code eq 'slf')
    {
        if ($tabReplaces->{'isSelf'} == $params[1])
        {
            return $params[0];
        }
        else
        {
            return '';
        }
    }
    return $value;
}

# Function: _debug
# Ecrit une trace de type debug
#
# Parameters:
# string $content Contenu de la trace
# string $routine Nom de la méthode appelante
sub _debug
{
    my ($content, $routine) = @_;
    my $debugMode = &LOC::Json::fromJson(&LOC::Characteristic::getGlobalValueByCode('DEBUG'));
    if ($debugMode->{'transport'})
    {
        &LOC::Log::debug('transport_' . $routine . ' ' . $content);
    }
}


1;