use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Insurance::Type
# Module permettant d'obtenir les types d'assurance
package LOC::Insurance::Type;


# Constants: Récupération d'informations sur les types d'assurance
# GETINFOS_ALL  - Récupération de l'ensemble des informations
# GETINFOS_BASE - Récupération des informations de base
use constant
{
    GETINFOS_ALL    => 0xFFFFFFFF,
    GETINFOS_BASE   => 0x00000000
};


# Constants: Flags
# FLAG_ISINVOICED        - Assurance facturée
# FLAG_ISCUSTOMER        - Assurance client
# FLAG_ISINCLUDED        - Assurance incluse
# FLAG_ISAPPEALACTIVATED - Gestion des recours activée ?
use constant
{
    FLAG_ISINVOICED        => 0x0001,
    FLAG_ISCUSTOMER        => 0x0002,
    FLAG_ISINCLUDED        => 0x0004,
    FLAG_ISAPPEALACTIVATED => 0x0010
};

# Constants: Mode de calcul
# MODE_NOTINVOICED     - Non facturé
# MODE_CALENDARADDAYS  - Pourcentage du prix de location jour fois jours calendaires + J+ - J-
# MODE_WORKING         - Pourcentage du prix de location jour fois jours ouvrés
# MODE_CALENDAR        - Pourcentage du prix de location jour fois jours calendaires

use constant
{
    MODE_NOTINVOICED    => 0,
    MODE_CALENDARADDAYS => 1,
    MODE_WORKING        => 2,
    MODE_CALENDAR       => 3
};


# Constants: États
# STATE_ACTIVE  - Actif
# STATE_DELETED - Supprimé
use constant
{
    STATE_ACTIVE  => 'GEN01',
    STATE_DELETED => 'GEN03'
};

use strict;

# Modules internes
use LOC::Db;
use LOC::Util;
use LOC::Appeal::Type;


# Function: getInfos
# Retourne les infos d'un type d'assurance
# 
# Parameters:
# string  $countryId  - Pays
# int     $idOrCode   - Identifiant ou code du type
# int     $flags      - Flags (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hashref|undef - Retourne un hashref suivant la demande, undef si pas de résultat
sub getInfos
{
    my ($countryId, $idOrCode, $flags, $tabOptions) = @_;

    my $tab;
    if (&LOC::Util::isNumeric($idOrCode))
    {
        $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $idOrCode}, $flags, $tabOptions);
    }
    else
    {
        $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'code' => $idOrCode}, $flags, $tabOptions);
    }

    my ($id, $tabInfos) = each(%$tab);

    return ($tabInfos ? (wantarray ? %$tabInfos : $tabInfos) : undef);
}

# Function: getList
# Récupérer la liste des assurances
#
# Contenu du hachage:
# - id         => id,
# - code       => code,
# - appeal.id  => identifiant de la gestion de recours associé
# - label      => libellé,
# - extraLabel => complément de libellé,
# - fullLabel  => libellé complet
# - mode       => mode de calcul,
# - flags      => flags
# - isDefault  => est-ce la valeur par défaut
# - state.id   => identifiant de l'état
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id, energy)
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (ref($tabFilters) ne 'HASH')
    {
        $tabFilters = {};
    }
    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query;
    my @tabWheres;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query .= '
SELECT
    ity_id AS `id`,
    ity_code AS `code`,
    ity_label AS `label`,
    ity_labelext AS `extraLabel`,
    ity_labelfull AS `fullLabelMask`,
    ity_mode AS `mode`,
    ity_flags AS `flags`,
    IF (ity_flags & ' . $db->quote(FLAG_ISINVOICED) . ', 1, 0) AS `isInvoiced`,
    IF (ity_flags & ' . $db->quote(FLAG_ISINCLUDED) . ', 1, 0) AS `isIncluded`,
    IF (ity_flags & ' . $db->quote(FLAG_ISCUSTOMER) . ', 1, 0) AS `isCustomer`,
    IF (ity_flags & ' . $db->quote(FLAG_ISAPPEALACTIVATED) . ', 1, 0) AS `isAppealActivated`,
    ity_is_default AS `isDefault`,
    ity_sta_id AS `state.id`';
    }
    else
    {
        $query = '
SELECT
    ity_id,
    ity_label';
    }
    
    $query .= '
FROM p_insurancetype';

    ## Filtres
    # Id
    if (defined $tabFilters->{'id'})
    {
        push(@tabWheres, 'ity_id IN (' . $db->quote($tabFilters->{'id'}) . ')');
    }
    # Code
    if (defined $tabFilters->{'code'})
    {
        push(@tabWheres, 'ity_code IN (' . $db->quote($tabFilters->{'code'}) . ')');
    }
    # - État
    if (exists $tabFilters->{'stateId'})
    {
        push(@tabWheres, 'ity_sta_id IN (' . $db->quote($tabFilters->{'stateId'}) . ')');
    }
    elsif (!exists $tabFilters->{'stateId'} && !defined $tabFilters->{'id'})
    {
        push(@tabWheres, 'ity_sta_id <> ' . $db->quote(STATE_DELETED));
    }

    if (@tabWheres > 0)
    {
        $query .= '
WHERE ' . join("\nAND ", @tabWheres);
    }

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}        *= 1;
            $tabRow->{'mode'}      *= 1;
            $tabRow->{'flags'}     *= 1;
            $tabRow->{'isDefault'} *= 1;

            $tabRow->{'isInvoiced'}        *= 1;
            $tabRow->{'isIncluded'}        *= 1;
            $tabRow->{'isCustomer'}        *= 1;
            $tabRow->{'isAppealActivated'} *= 1;

            if ($tabRow->{'fullLabelMask'} ne '')
            {
                my $insuranceLabel = $tabRow->{'fullLabelMask'};
                $insuranceLabel =~ s/<%label>/$tabRow->{'label'}/g;
                $insuranceLabel =~ s/<%extraLabel>/$tabRow->{'extraLabel'}/g;

                $tabRow->{'fullLabel'} = $insuranceLabel;
            }
            else
            {
                $tabRow->{'fullLabel'} = $tabRow->{'label'};
            }
        }
    }

    return (wantarray ? %$tab : $tab);
}

# Function: getDefaultId
# Retourne l'identifiant du type d'assurance par défaut
# 
# Parameters:
# string  $countryId  - Pays
#
# Returns:
# int
sub getDefaultId
{
    my ($countryId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT
    ity_id
FROM p_insurancetype
WHERE
    ity_is_default = 1
    AND ity_sta_id = ' . $db->quote(STATE_ACTIVE) . ';';

    return $db->fetchOne($query) * 1;
}

1;