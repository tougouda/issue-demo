use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::SiteLocality
# Module permettant d'obtenir les informations d'une ville chantier
package LOC::SiteLocality;
use strict;

# Modules internes
use LOC::Db;
use LOC::Util;
use LOC::Characteristic;

# Function: getList
# Retourne la liste des villes de chantier
#
# Filtres possibles :
# - id   : identifiant de la ville chantier
# - name : nom de la ville chantier
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres
# hashref $tabOptions - Options
#
# Returns:
# hash|hashref|undef - Liste des villes de chantier ou non défini
sub getList
{
    my ($countryId, $format, $tabFilters, $tabOptions) = @_;
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '';

    # Filtres
    my $whereQuery = '';

    # Filtres
    # - Recherche par l'id
    if ($tabFilters->{'id'})
    {
        $whereQuery .= '
AND tbl_p_sitelocality.VILLEAUTO IN (' . &LOC::Util::join(', ', $db->quote($tabFilters->{'id'})) . ')';
    }

    # - Recherche par le nom de ville
    if ($tabFilters->{'name'} ne '')
    {
        my $search = $tabFilters->{'name'};
        # Ajout des % si le filtre n'en contient pas
        if ($search !~ /%/)
        {
            $search = '%' . $tabFilters->{'name'} . '%';
        }
        $whereQuery .= '
AND tbl_p_sitelocality.VINOM LIKE  ' . $db->quote($search);
    }
    # - Recherche par le code postal
    if ($tabFilters->{'postalCode'} ne '')
    {
        my $search = $tabFilters->{'postalCode'};
        # Ajout des % si le filtre n'en contient pas
        if ($search !~ /%/)
        {
            $search = '%' . $tabFilters->{'postalCode'} . '%';
        }
        $whereQuery .= '
AND tbl_p_sitelocality.VICP LIKE ' . $db->quote($search);
    }
    # - Recherche par le département
    if ($tabFilters->{'department'} ne '')
    {
        my $search = $tabFilters->{'department'};
        # Ajout des % si le filtre n'en contient pas
        if ($search !~ /%/)
        {
            $search = '%' . $tabFilters->{'department'} . '%';
        }
        $whereQuery .= '
AND tbl_p_sitelocality.DEPVILLE LIKE ' . $db->quote($search);
    }

    if ($tabFilters->{'search'} ne '')
    {
        my @search = split(/ /, $tabFilters->{'search'});

        foreach my $search (@search)
        {
            $search = &LOC::Util::trim($search);
            $search = &LOC::Util::escapeRegexForSqlRegex($search);

            # Transformation en expression rationnelle
            # Recherche du type %XXX -> XXX($|\n)
            if ($search =~ /^%/ && $search !~ /%$/)
            {
                # suppression du %
                $search = substr($search, 1);
                $search .= '($|\n)';
            }
            # Recherche du type XXX% -> (^|\n)XXX
            if ($search !~ /^%/ && $search =~ /%$/)
            {
                # suppression du %
                $search = substr($search, 0, length($search) - 1);
                $search = '(^|\n)' . $search;
            }

            # remplacement des % en milieu de chaîne
            $search =~ s/%/.*/g;
            # remplacement des guillemets
            $search =~ s/"/\\"/g;

            if ($search ne '')
            {
                $whereQuery .= '
AND CONCAT_WS("\n", tbl_p_sitelocality.VINOM, tbl_p_sitelocality.VICP, tbl_p_sitelocality.DEPVILLE) REGEXP "' . $search . '"';
            }
        }
    }

    if ($format == LOC::Util::GETLIST_PAIRS)
    {
        $query = '
SELECT
    tbl_p_sitelocality.VILLEAUTO,
    CONCAT (tbl_p_sitelocality.VICP, " ", tbl_p_sitelocality.VINOM) AS `fullName`
FROM VILLECHA tbl_p_sitelocality
WHERE 1' . $whereQuery . ';';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*) AS `fullName`
FROM VILLECHA tbl_p_sitelocality
WHERE 1' . $whereQuery . ';';
    }
    else
    {
        my $transportAgc = '';
        if (defined $tabOptions->{'transportAgency'} && $tabOptions->{'transportAgency'} ne '' &&
            &LOC::Characteristic::getAgencyValueByCode('TRSPTARIF', $tabOptions->{'transportAgency'}))
        {
            $transportAgc = $tabOptions->{'transportAgency'};
        }
        $query = '
SELECT
    tbl_p_sitelocality.VILLEAUTO AS `id`,
    tbl_p_sitelocality.VINOM AS `name`,
    tbl_p_sitelocality.DEPVILLE AS `department`,
    tbl_p_sitelocality.VICP AS `postalCode`,
    CONCAT (tbl_p_sitelocality.VICP, " ", tbl_p_sitelocality.VINOM) AS `fullName`';
        if ($transportAgc ne  '')
        {
            $query .= ',
    IFNULL(tbl_f_localityagency.ZONE, 6) AS `zone`';
        }
        $query .= '
FROM VILLECHA tbl_p_sitelocality';
        if ($transportAgc ne  '')
        {
            $query .= '
LEFT JOIN GESTIONTRANS' . $countryId . '.COMMUNEAGENCE tbl_f_localityagency
ON tbl_f_localityagency.AGENCE = "' . $transportAgc . '" AND tbl_f_localityagency.CODEINSEE = tbl_p_sitelocality.CODEINSEE';
        }
        $query .= '
WHERE 1' . $whereQuery . '
ORDER BY `postalCode`, `fullName`;';

    }

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }
    return (wantarray ? %$tab : $tab);
}

1;