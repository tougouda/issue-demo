use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Invoice
# Module permettant d'obtenir les informations sur les factures
package LOC::Invoice;


# Constants: Types de factures
# TYPE_RENT      - Location
# TYPE_OLDREPAIR - Remise en état (ancien système)
# TYPE_REPAIR    - Remise en état
use constant
{
    TYPE_RENT      => 'RENT',
    TYPE_OLDREPAIR => 'OLDREPAIR',
    TYPE_REPAIR    => 'REPAIR',
};

# Constants: États
# STATE_NOTEXPORTED - Facture non exportée vers Sage
# STATE_EXPORTED    - Facture exportée vers Sage
use constant
{
    STATE_NOTEXPORTED => 'FAC01',
    STATE_EXPORTED    => 'FAC02',
};

# Constants: Fichier d'export vers Sage
# EXPORTFILE_VERSION  - Version du fichier d'export vers Sage
# EXPORTFILE_LINEEND  - Caractères de fins de lignes
# EXPORTFILE_ENCODING - Encodage
use constant
{
    EXPORTFILE_VERSION  => 13,
    EXPORTFILE_LINEEND  => "\r\n",
    EXPORTFILE_ENCODING => 'cp850',
};

# Constants: Fichier d'export vers Sage
# EXPORT_DOCTYPE - Encodage
use constant
{
    EXPORTDOCTYPE_DELIVERYNOTE => 4,
    EXPORTDOCTYPE_INVOICE      => 7,
};

# Constants: Flags pour les types de retour attendus pour les dernières facturations
# GETLASTEST_ALL  - Récupération de toutes les informations
# GETLASTEST_LOGS - Récupération des informations d'historiques
# GETLASTEST_BASE - Récupération des informations de base
use constant
{
    GETLASTEST_ALL            => 0xFFFFF,
    GETLASTEST_LOGS           => 0x00001,
    GETLASTEST_BASE           => 0x00000,
};

# Constants: Erreurs
# ERROR_FATAL                 - Erreur fatale
use constant
{
    ERROR_FATAL                 => 0x0001
};


use strict;

use LOC::Agency;
use LOC::Invoice::Rent;
use LOC::InvoicingRequest;
use LOC::Locale;
use LOC::Util;

my $fatalErrMsg = 'Erreur fatale: module LOC::Invoice, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";



# Function: getHistory
# Récupérer l'historique de la facturation
#
# Contenu d'une ligne du hashage :
# - id : identifiant de la ligne d'historique ;
# - eventType.code : code du type d'événement ;
# - eventType.label : libellé du type d'événement ;
# - user.id : identifiant de l'utilisateur ;
# - user.name : nom de l'utilisateur ;
# - user.firstName : prénom de l'utilisateur ;
# - user.fullName : nom complet de l'utilisateur ;
# - user.state.id : identifiant de l'état de l'utilisateur ;
# - invoicing.id : de la facturation ;
# - datetime : date et heure de la trace ;
# - content : contenu de la trace.
#
# Parameters:
# string $countryId - Pays
# string $date      - Date de la facturation (au format SQL)
# string $type      - Type de la facturation (TYPE_RENT par défaut, TYPE_OLDREPAIR ou TYPE_REPAIR)
# string $localeId  - Identifiant de la locale (LOC::Locale::DEFAULT_LOCALE_ID par défaut)
#
# Returns:
# hash|hashref - Historique de la facturation
sub getHistory
{
    my ($countryId, $date, $type, $localeId) = @_;

    unless (defined $type)
    {
        $type = LOC::Invoice::TYPE_RENT;
    }
    $date =~ s/\-//g;
    unless (defined $localeId)
    {
        $localeId = LOC::Locale::DEFAULT_LOCALE_ID;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('auth', $countryId);

    my $query = '
SELECT
    log_id AS `id`,
    etp_id AS `eventType.code`,
    IFNULL(etl_label, etp_label) AS `eventType.label`,
    log_usr_id AS `user.id`,
    usr_name AS `user.name`,
    usr_firstname AS `user.firstName`,
    CONCAT_WS(" ", usr_firstname, usr_name) AS `user.fullName`,
    usr_sta_id AS `user.state.id`,
    log_functionalityid AS `invoicing.id`,
    log_date AS `datetime`,
    log_content AS `content`
FROM AUTH.h_log
LEFT JOIN frmwrk.f_user
ON log_usr_id = usr_id
LEFT JOIN AUTH.p_eventtype
ON log_etp_id = etp_id
LEFT JOIN AUTH.p_eventtype_locale
ON (etp_id = etl_etp_id AND etl_lcl_id = ' . $db->quote($localeId) . ')
WHERE log_functionality = "FACTURATION"
AND log_functionalityid = ' . $db->quote($date . $type . $countryId) . '
ORDER BY `datetime` DESC;';
    my $tabHistory = $db->fetchList(LOC::Util::GETLIST_ASSOC, $query, {}, 'id');
    if (!$tabHistory)
    {
        return undef;
    }
    return (wantarray ? %$tabHistory : $tabHistory);
}

# Function: getId
# Retourne l'identifiant de la facturation
#
# Parameters:
# string $countryId - Pays
# string $date      - Date de la facturation (au format SQL)
# string $type      - Type de la facturation  (TYPE_RENT par défaut, TYPE_OLDREPAIR ou TYPE_REPAIR)
#
# Returns:
# string - Identifiant de la facturation
sub getId
{
    my ($countryId, $date, $type) = @_;

    unless (defined $type)
    {
        $type = LOC::Invoice::TYPE_RENT;
    }
    if ($countryId eq '' || $date eq '')
    {
        return undef;
    }

    $date =~ s/\-//g;

    return $date . $type . $countryId;
}

# Function: getLastest
# Retourne la liste des dernières facturations
#
# Contenu du hashage :
# - date : date de la facturation ;
# - number : nombre de factures générées ;
# - amount : montant total HT des factures générées
# - rentType : type de facturation de location (INVOICETYPE_MONTH ou INVOICETYPE_INTERMEDIATE)
# - isExportable : le fichier d'export vers Sage peut être regénéré ;
# - isCancellable : la facturation peut être annulée ;
# - details : détail par agence de la facturation ;
#   * number : nombre de factures générées pour l'agence ;
#   * amount : montant total HT des factures générées pour l'agence ;
# - history : historique des opérations effectuées (si GETLASTEST_LOGS fait partie des flags) :
#   * id : identifiant de la ligne d'historique ;
#   * eventType.code : code du type d'événement ;
#   * eventType.label : libellé du type d'événement ;
#   * user.id : identifiant de l'utilisateur ;
#   * user.name : nom de l'utilisateur ;
#   * user.firstName : prénom de l'utilisateur ;
#   * user.fullName : nom complet de l'utilisateur ;
#   * user.state.id : identifiant de l'état de l'utilisateur ;
#   * invoicing.id : identifiant de la facturation ;
#   * datetime : date et heure de la trace ;
#   * content : contenu de la trace.
# - assets : liste des avoirs :
#   * id : identifiant de la facture négative ;
#   * contract.id : identifiant du contrat ;
#   * contract.code : code du contrat ;
#   * customer.id : identifiant du client ;
#   * customer.code : code du client ;
#   * amount : montant.
#
# Parameters:
# string  $countryId - Identifiant du pays
# string  $localeId  - Identifiant de la locale
# string  $date      - Date jusqu'à laquelle rechercher les facturations (6 derniers mois par défaut)
# string  $flags     - Flags de récupération des données
#
# Returns:
# hash|hashref|0 - Liste des facturations
sub getLastest
{
    my ($countryId, $localeId, $date, $flags) = @_;

    my $tabRentLastest = &LOC::Invoice::Rent::getLastest($countryId, $localeId, $date, $flags);
    my $tabRepairLastest = &LOC::Invoice::Repair::getLastest($countryId, $localeId, $date, $flags);

    # Fusion des tableaux
    my %tabResult;
    foreach my $key (keys(%$tabRentLastest))
    {
        if (defined $tabRentLastest->{$key})
        {
            push(@{$tabResult{$key}}, $tabRentLastest->{$key});
        }
    }
    foreach my $key (keys(%$tabRepairLastest))
    {
        if (defined $tabRepairLastest->{$key})
        {
            push(@{$tabResult{$key}}, $tabRepairLastest->{$key});
        }
    }

    return \%tabResult;
}

# Function: generateDocumentDate
# Génère la date de facture en fonction de la date de facturation
#
# Parameters:
# string  $type     - Type de facturation (TYPE_RENT ou TYPE_REPAIR)
# string  $rentType - Type de facturation de location (INVOICETYPE_MONTH ou INVOICETYPE_INTERMEDIATE)
# string  $date     - Date de facturation
#
# Returns:
# string - Date de la facture (au format SQL)
sub generateDocumentDate
{
    my ($type, $rentType, $date) = @_;

    unless (defined $type)
    {
        $type = TYPE_RENT;
    }

    if ($type eq TYPE_RENT)
    {
        return &LOC::Invoice::Rent::generateDocumentDate($rentType, $date);
    }
    if ($type eq TYPE_REPAIR)
    {
        return &LOC::Invoice::Repair::generateDocumentDate($date);
    }
    return '';
}

# Function: getLastDocumentNo
# Récupère le dernier numéro de pièce enregistré
#
# Parameters:
# string  $countryId - Identifiant du pays
# string  $type     - Type de facturation (TYPE_RENT ou TYPE_REPAIR)
# string  $rentType - Type de facturation de location (INVOICETYPE_MONTH ou INVOICETYPE_INTERMEDIATE)
# string  $date      - Date
#
# Returns:
# string - Dernier numéro de pièce enregistré
sub getLastDocumentNo
{
    my ($countryId, $type, $rentType, $date) = @_;

    unless (defined $countryId)
    {
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Préfixe
    my $prefix = &getDocumentNoPrefix($countryId);

    # Date
    my $documentDate = &generateDocumentDate($type, $rentType, $date);
    my @tabDate = split(/\-/, $documentDate);
    my $year  = &LOC::Util::toNumber($tabDate[0]);
    my $month = &LOC::Util::toNumber($tabDate[1]);
    my $day   = &LOC::Util::toNumber($tabDate[2]);

    # Mois sur un caractère : chiffre du mois de janvier à septembre, O pour octobre, N pour novembre, D pour décembre
    my @tabMonths = (1..9, 'O', 'N', 'D');

    my $mask = sprintf('%s%02d%s%%', $prefix, $year % 100, $tabMonths[$month - 1]);

    # Montants totaux par facturation
    my $query = '
SELECT MAX(`num`) AS `documentNo`
FROM
((SELECT MAX(ENTETENUMFAC) AS `num`
FROM ENTETEFACTURE
WHERE ENTETENUMFAC LIKE ' . $db->quote($mask) . ')
UNION
(SELECT MAX(rpi_document) AS `num`
FROM f_repairinvoice
WHERE rpi_document LIKE ' . $db->quote($mask) . ')) `table`;';

    return $db->fetchOne($query);
}

# Function: getDocumentNoPrefix
# Récupère le préfixe du numéro de pièce
#
# Parameters:
# string  $countryId - Identifiant du pays
#
# Returns:
# string - Préfixe du numéro de pièce
sub getDocumentNoPrefix
{
    my ($countryId) = @_;

    unless (defined $countryId)
    {
        return 0;
    }

    # Type de document exporté : BL ou facture
    my $documentType = &LOC::Characteristic::getCountryValueByCode('TYPEDOCSAGE', $countryId);
    my $prefix = '';
    if ($documentType eq EXPORTDOCTYPE_INVOICE)
    {
        $prefix = &LOC::Characteristic::getCountryValueByCode('PREFIXEFAC', $countryId);
    }
    elsif ($documentType eq EXPORTDOCTYPE_DELIVERYNOTE)
    {
        $prefix = &LOC::Characteristic::getCountryValueByCode('PREFIXEBL', $countryId);
    }

    return $prefix;
}

# Function: generateDocumentNo
# Génère le numéro de pièce en fonction de la date de facturation et du numéro passé en paramètre
#
# Parameters:
# string  $countryId - Identifiant du pays
# string  $type      - Type de facturation (TYPE_RENT ou TYPE_REPAIR)
# string  $rentType  - Type de facturation de location (INVOICETYPE_MONTH ou INVOICETYPE_INTERMEDIATE)
# string  $date      - Date de facturation
# string  $no        - Prochain numéro de pièce
# string  $prefix    - Préfixe du numéro de pièce
#
# Returns:
# string - Numéro de pièce
sub generateDocumentNo
{
    my ($countryId, $type, $rentType, $date, $no, $prefix) = @_;

    unless (defined $countryId && defined $type && defined $date)
    {
        return 0;
    }

    if ($type eq TYPE_RENT && !defined $rentType)
    {
        return undef;
    }

    # Numérotation de la pièce :
    # FAAMXXXX avec F le caractère préfixe, AA l'année, M le mois (de 1 à 9, puis O, N et D), XXXX l'incrément
    # Récupération de la date du document
    my $documentDate = &generateDocumentDate($type, $rentType, $date);
    my @tabDate = split(/\-/, $documentDate);
    my $year  = &LOC::Util::toNumber($tabDate[0]);
    my $month = &LOC::Util::toNumber($tabDate[1]);
    my $day   = &LOC::Util::toNumber($tabDate[2]);

    # Mois sur un caractère : chiffre du mois de janvier à septembre, O pour octobre, N pour novembre, D pour décembre
    my @tabMonths = (1..9, 'O', 'N', 'D');

    return sprintf('%s%02d%s%04d', $prefix, $year % 100, $tabMonths[$month - 1], $no);
}

# Function: getExportFileContent
# Retourne le contenu du fichier d'export vers Sage
#
# Parameters:
# string $countryId - Identifiant du pays
# string $type      - Type de la facturation (TYPE_RENT par défaut, TYPE_OLDREPAIR ou TYPE_REPAIR)
# string $rentType  - Type de facturation (dans le cas où $type vaut TYPE_RENT seulement)
# string $date      - Date de facturation
#
# Returns:
# string - Contenu du fichier d'export vers Sage
sub getExportFileContent
{
    my ($countryId, $type, $rentType, $date) = @_;

    if ($type eq TYPE_RENT)
    {
        return &LOC::Invoice::Rent::getExportFileContent($countryId, $rentType, $date);
    }
    if ($type eq TYPE_REPAIR)
    {
        return &LOC::Invoice::Repair::getExportFileContent($countryId, $date);
    }
    return '';
}

# Function: getInvoicesToCancel
# Retourne la liste des factures à supprimer dans le cas d'une annulation de facturation
#
# Parameters:
# string $countryId - Identifiant du pays
# string $type      - Type de la facturation à annuler (TYPE_RENT par défaut, TYPE_OLDREPAIR ou TYPE_REPAIR)
# string $rentType  - Type de la facturation de location à annuler (dans le cas où $type vaut TYPE_RENT seulement)
# string $date      - Date de la facturation à annuler
#
# Returns:
# hashref - Liste des factures à supprimer
sub getInvoicesToCancel
{
    my ($countryId, $type, $rentType, $date) = @_;

    if ($type eq TYPE_RENT)
    {
        return &LOC::Invoice::Rent::getInvoicesToCancel($countryId, $rentType, $date);
    }
    if ($type eq TYPE_REPAIR)
    {
        return &LOC::Invoice::Repair::getInvoicesToCancel($countryId, $date);
    }
    return '';
}

# Function: cancelInvoicing
# Annule une facturation
#
# Parameters:
# string $countryId - Identifiant du pays
# string $type      - Type de la facturation à annuler (TYPE_RENT par défaut, TYPE_OLDREPAIR ou TYPE_REPAIR)
# string $rentType  - Type de la facturation de location à annuler (dans le cas où $type vaut TYPE_RENT seulement)
# string $date      - Date de la facturation à annuler
#
# Returns:
# int - Nombre de factures supprimées
sub cancelInvoicing
{
    my ($countryId, $type, $rentType, $date) = @_;

    if ($type eq TYPE_RENT)
    {
        return &LOC::Invoice::Rent::cancelInvoicing($countryId, $rentType, $date);
    }
    if ($type eq TYPE_REPAIR)
    {
        return &LOC::Invoice::Repair::cancelInvoicing($countryId, $date);
    }
    return '';
}

# Function: invoice
# Effectue une facturation
#
# Parameters:
# string $countryId - Identifiant du pays
# string $type      - Type de la facturation à annuler (TYPE_RENT par défaut, TYPE_OLDREPAIR ou TYPE_REPAIR)
# string $rentType  - Type de la facturation de location à annuler (dans le cas où $type vaut TYPE_RENT seulement)
# string $date      - Date de la facturation à annuler
# int    $no        - Numéro de la prochaine pièce
# int    $requestId - Identifiant de la requête de facturation
#
# Returns:
# hashref - Nombre de factures générées
sub invoice
{
    my ($countryId, $tabUserInfos, $type, $rentType, $date, $no, $requestId) = @_;

    # Connexion à la base de données
    my $db = &LOC::Db::getConnection('auth');

    # Passage de l'utilisteur dans le pays concerné
    my $tabAgencies = &LOC::Agency::getList(LOC::Util::GETLIST_PAIRS, {'country' => $countryId});
    my @tabAgencyId = keys(%$tabAgencies);
    $db->update('PAPE', {'PACODE' => $countryId}, {'PEAUTO' => $tabUserInfos->{'id'}});
    $db->update('PERSONNEL', {'AGAUTO' => $tabAgencyId[0]}, {'PEAUTO' => $tabUserInfos->{'id'}});

    my $result = '';
    if ($type eq LOC::Invoice::TYPE_RENT)
    {
        if ($rentType eq LOC::Invoice::Rent::INVOICETYPE_MONTH)
        {
            $result = &LOC::Invoice::Rent::monthlyInvoice($countryId, $date, $no, $requestId);
        }
        if ($rentType eq LOC::Invoice::Rent::INVOICETYPE_INTERMEDIATE)
        {
            $result = &LOC::Invoice::Rent::intermediateInvoice($countryId, $date, $no, $requestId);
        }
    }
    if ($type eq LOC::Invoice::TYPE_REPAIR)
    {
        $result = &LOC::Invoice::Repair::invoice($countryId, $date, $no, $requestId);
    }

    # Retour de l'utilisateur dans son agence d'origine
    $db->update('PAPE', {'PACODE' => $tabUserInfos->{'nomadCountry.id'}}, {'PEAUTO' => $tabUserInfos->{'id'}});
    $db->update('PERSONNEL', {'AGAUTO' => $tabUserInfos->{'nomadAgency.id'}}, {'PEAUTO' => $tabUserInfos->{'id'}});

    return $result;
}

# Function: launch
# Lance une facturation
#
# Parameters:
# string $countryId - Identifiant du pays
# string $type      - Type de la facturation à annuler (TYPE_RENT par défaut, TYPE_OLDREPAIR ou TYPE_REPAIR)
# string $rentType  - Type de la facturation de location à annuler (dans le cas où $type vaut TYPE_RENT seulement)
# string $date      - Date de la facturation à annuler
# string $no        - Numéro de la prochaine pièce
#
# Returns:
# int - Identifiant de la requête de facturation ou 0 en cas d'erreur
sub launch
{
    my ($countryId, $tabUserInfos, $type, $rentType, $date, $no) = @_;

    # Vérification qu'aucune autre facturation avec les mêmes paramètres n'est en cours
    my $tabCheckParams = {'parameters' => {'countryId' => $countryId,
                                           'type'      => $type,
                                           'rentType'  => $rentType,
                                           'date'      => $date
                                          },
                          'stateId'    => [LOC::InvoicingRequest::STATE_CREATED,
                                           LOC::InvoicingRequest::STATE_PROGRESS
                                          ]
                         };
    my $nbRequest = &LOC::InvoicingRequest::getList($countryId, LOC::Util::GETLIST_COUNT, $tabCheckParams);
    if ($nbRequest > 0)
    {
        return 0;
    }

    # Ajout d'une ligne en base
    my $invoicingId = &getId($countryId, &LOC::Date::getMySQLDate(), $type);
    my $tabParams = {'countryId'          => $countryId,
                     'userId'             => $tabUserInfos->{'id'},
                     'userNomadCountryId' => $tabUserInfos->{'nomadCountry.id'},
                     'userNomadAgencyId'  => $tabUserInfos->{'nomadAgency.id'},
                     'type'               => $type,
                     'date'               => $date,
                     'no'                 => $no,
                     'invoicingId'        => $invoicingId};
    if ($rentType ne '')
    {
        $tabParams->{'rentType'} = $rentType;
    }
    my $tabRequestParams = {'userId' => $tabUserInfos->{'id'}, 'parameters' => $tabParams};
    my $requestId = &LOC::InvoicingRequest::insert($countryId, $tabRequestParams);
    if ($requestId <= 0)
    {
        return 0;
    }

    my $cgiPath         = &LOC::Globals::get('cgiPath');

    # Fichier de log
    my $logName = sprintf('launch_%s.txt', $date);
    my $logPath = &LOC::Util::getCommonFilesPath('logs/invoice/');

    # Export des variables
    my $exports = 'export REMOTE_USER="' . $tabUserInfos->{'login'} . '"';

    # Placement dans le répertoire d'exécution
    my $execPath = $cgiPath . '/exec/';
    my $changeDir = 'cd "' . $execPath . '"';

    # Exécutable
    my $script1 = sprintf('perl invoice.pl "%s" "%s" 2>&1', $requestId, $countryId);
    my $script2 = sprintf('perl invoiceOutput.pl "%s" "%s"', $requestId, $countryId);
    my $scripts = $script1 . ' | ' . $script2;

    # Construction de la commande
    my $command  = '(' . $exports . ' && (' . $changeDir . ' && (' . $scripts . ')))';
    $command .= ' >> ' . $logPath . $logName;
    $command .= ' 2>&1 &';

    # Exécution
    system($command);

    return $requestId;
}

# Function: getSageFormat
# Retourne un nombre au format Sage avec le nombre de décimales spécifié
#
# Parameters:
# float  $number     - Nombre
# int    $nbDecimals - Nombre de décimales
#
# Returns:
# string - Nombre formatté
sub getSageFormat
{
    my ($number, $nbDecimals) = @_;

    my $number = &LOC::Util::round($number, $nbDecimals);
    $number =~ s/\./\,/g;

    return $number;
}


# Function: onExternalChanges
# Exécute toutes les mises à jour nécessaires lors de la modification d'une entitée externe
#
# Parameters:
# string  $countryId  - Pays
# hashref $tabChanges - Informations sur les mises à jour des entitées externes
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub onExternalChanges
{
    my ($countryId, $tabChanges, $userId, $tabErrors, $tabEndUpds) = @_;

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];
    $tabErrors->{'modules'} = {
        'rent'   => {},
        'repair' => {}
    };

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'onExternalChanges', $_[0], __FILE__, '');
        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }
        return 0;
    };

    # Impact sur les factures de location
    if (!&LOC::Invoice::Rent::onExternalChanges($countryId, $tabChanges, $userId,
                                                $tabErrors->{'modules'}->{'rent'}, $tabEndUpds))
    {
        # Erreur fatale !
        return $rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Impact sur les factures de remise en état
    if (!&LOC::Invoice::Repair::onExternalChanges($countryId, $tabChanges, $userId,
                                                  $tabErrors->{'modules'}->{'repair'}, $tabEndUpds))
    {
        # Erreur fatale !
        return $rollBackAndError(__LINE__, ERROR_FATAL);
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}

# Function: isDocumentExportable
# Indique si une facture peut être exportée ou non.
#
# Parameters:
# string  $countryId  - L'identifiant du pays.
# float   $amount     - Le montant de la facture.
#
# Returns:
# bool 1 si la facture peut être exportée, 0 sinon.
sub isDocumentExportable
{
    my ($countryId, $amount) = @_;

    # Récupération du montant minimum que doit avoir une facture pour être exportable
    my $minAmount = &LOC::Characteristic::getCountryValueByCode('MTMININV', $countryId);

    return ($amount >= $minAmount);
}


1;
