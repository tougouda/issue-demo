use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Contract::RepairService
# Module de gestion des services de remise en état
package LOC::Contract::RepairService;


# Constants: États des services de remise en état
# STATE_APPROVED - actif
# STATE_CANCELED - supprimé
# STATE_INVOICED - facturé
use constant
{
    STATE_APPROVED => 'CAS02',
    STATE_CANCELED => 'CAS04',
    STATE_INVOICED => 'CAS05'
};


use strict;

use LOC::Contract::Rent;

# Function: getInfos
# Retourne un hachage contenant les infos des services de remise en état sur le contrat de location
#
# Contenu d'une ligne du hashage : voir la méthode getList.
#
# Parameters:
# string $countryId - Pays
# int    $id        - Id du service de remise en état
# int    $flags     - Flags
#
# Returns:
# hash|hashref - Informations du service de remise en état sur le contrat
sub getInfos
{
    my ($countryId, $id, $flags) = @_;

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Récupérer la liste des services de remise en état
#
# Contenu d'une ligne du hashage :
# - id => identifiant ;
# - contract.id => id du contrat associé ;
# - repairsheet.id => identifiant dde la fiche de remise en état associée ;
# - currency.id => identifiant de la devise ;
# - currency.symbol => symbole local de la devise ;
# - state.id => identifiant de l'état associé ;
# - state.label => libellé de l'état associé ;
# - type => type de service de remise en état (DSP : déplacement, LAB : main d'oeuvre, TRT : temps de trajet, SRV : Autre service) ;
# - label => libellé du service de remise en état ;
# - unitAmount => prix unitaire ;
# - quantity => quantité ;
# - amount => montant ;
# - invoicingDate => date de facturation.
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hash|hashref - Liste des services de remise en état
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    crs_id AS `id`,
    crs_rct_id AS `contract.id`,
    crs_rps_id AS `repairsheet.id`,
    crs_cur_id AS `currency.id`,
    cur_symbol AS `currency.symbol`,
    crs_sta_id AS `state.id`,
    tbl_p_state.ETLIBELLE AS `state.label`,
    crs_type AS `type`,
    crs_label AS `label`,
    crs_unitamount AS `unitAmount`,
    crs_quantity AS `quantity`,
    crs_amount AS `amount`,
    crs_date_invoicing AS `invoicingDate`
FROM f_rentcontractrepairservice
LEFT JOIN frmwrk.p_currency
ON crs_cur_id = cur_id
LEFT JOIN ETATTABLE tbl_p_state
ON crs_sta_id = tbl_p_state.ETCODE';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query .= '
SELECT
    COUNT(*)
FROM f_rentcontractrepairservice';

    }
    else
    {
        $query .= '
SELECT
    crs_id AS `id`,
    crs_amount AS `amount`
FROM f_rentcontractrepairservice';
    }

    # Filtres
    $query .= '
WHERE 1 ';
    if (defined $tabFilters->{'id'})
    {
        $query .= '
        AND crs_id IN (' . $db->quote($tabFilters->{'id'}) . ')';
    }
    if (defined $tabFilters->{'contractId'})
    {
        $query .= '
        AND crs_rct_id IN (' . $db->quote($tabFilters->{'contractId'}) . ')';
    }

    # Options
    if (defined $tabOptions->{'orders'})
    {
        my @orders;
        foreach my $order (@{$tabOptions->{'orders'}})
        {
            my $dir = ($order->{'dir'} ? $order->{'dir'} : '');
            if ($order->{'field'} eq 'sheetId')
            {
                push(@orders, 'crs_rps_id' . ' ' . $dir);
            }
            elsif ($order->{'field'} eq 'id')
            {
                push(@orders, 'crs_id' . ' ' . $dir);
            }
        }
        $query .= '
ORDER BY ' . join(', ', @orders) . ';';
    }
    else
    {
        $query .= '
ORDER BY `repairsheet.id`;';
    }

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'} *= 1;
            $tabRow->{'contract.id'} *= 1;
            $tabRow->{'repairsheet.id'} *= 1;
            $tabRow->{'unitAmount'} *= 1;
            $tabRow->{'quantity'} *= 1;
            $tabRow->{'amount'} *= 1;
        }
    }
    return (wantarray ? %$tab : $tab);
}

# Function: set
# Met à jour (insert/update) toutes les infos d'une service
#
# Contenu du hachage à passer en paramètre:
# - id              => id
# - contract.id     => id du contrat
# - repairsheet.id  => id de la fiche de remise ne etat
# - currency.id     => id de la devise
# - number          => nombre de jours
# - amout           => montant
#
# Parameters:
# string    $countryId - Pays
# hashref   $values    - Valeurs du service
#
# Returns:
# integer
sub set
{
    my ($countryId, $values) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Mise à jour
    if (defined $values->{'id'} && $values->{'id'} != 0)
    {
        my $result = $db->update('f_rentcontractrepairservice', {
                                                 'crs_rct_id'         => $values->{'contract.id'},
                                                 'crs_rps_id'         => $values->{'repairsheet.id'},
                                                 'crs_cur_id'         => $values->{'currency.id'},
                                                 'crs_type'           => $values->{'type'},
                                                 'crs_label'          => $values->{'label'},
                                                 'crs_unitamount'     => $values->{'unitAmount'},
                                                 'crs_quantity'       => $values->{'quantity'},
                                                 'crs_amount'         => $values->{'type'},
                                                 'crs_date_invoicing' => $values->{'invoicingDate'},
                                                 },
                                                 {
                                                 'crs_id'                => $values->{'id'}
                                                 });

        if ($result == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;
        }

        my $updateAmounts = &LOC::Contract::Rent::updateAmounts($countryId, $values->{'contract.id'});
        if ($updateAmounts == 0)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;
        }



    }
    # Création
    else
    {
        my $crsId = $db->insert('f_rentcontractrepairservice', {
                                                 'crs_rct_id'         => $values->{'contract.id'},
                                                 'crs_rps_id'         => $values->{'repairsheet.id'},
                                                 'crs_sta_id'         => STATE_APPROVED,
                                                 'crs_cur_id'         => $values->{'currency.id'},
                                                 'crs_type'           => $values->{'type'},
                                                 'crs_label'          => $values->{'label'},
                                                 'crs_unitamount'     => $values->{'unitAmount'},
                                                 'crs_quantity'       => $values->{'quantity'},
                                                 'crs_amount'         => $values->{'amount'},
                                                 'crs_date_invoicing' => $values->{'invoicingDate'},
                                                 });


        if ($crsId == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;
        }

        # Mise à jour des montants
        my $updateAmounts = &LOC::Contract::Rent::updateAmounts($countryId, $values->{'contract.id'});
        if ($updateAmounts == 0)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }

    return 1;
}

1;