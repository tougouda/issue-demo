use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Contract::RepairDay
# Module de gestion des jours de remise en état
package LOC::Contract::RepairDay;


# Constants: États des jours de remise en état
# STATE_APPROVED - actif
# STATE_CANCELED - supprimé
# STATE_INVOICED - facturé
use constant
{
    STATE_APPROVED => 'CAS02',
    STATE_CANCELED => 'CAS04',
    STATE_INVOICED => 'CAS05'
};


use strict;

use LOC::Contract::Rent;

# Function: getInfos
# Retourne un hachage contenant les infos des jours de remise en état sur le contrat de location
#
# Contenu d'une ligne du hashage : voir la méthode getList.
#
# Parameters:
# string $countryId - Pays
# int    $id        - Id du jour de remise en état
# int    $flags     - Flags
#
# Returns:
# hash|hashref - Informations du jour de remise en état sur le contrat
sub getInfos
{
    my ($countryId, $id, $flags) = @_;

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Récupérer la liste des jours de remise en état
#
# Contenu d'une ligne du hashage :
# - id => identifiant ;
# - contract.id => id du contrat associé ;
# - repairsheet.id => identifiant dde la fiche de remise en état associée ;
# - currency.id => identifiant de la devise ;
# - currency.symbol => symbole local de la devise ;
# - state.id => identifiant de l'état associé;
# - state.label => libellé de l'état associé;
# - unitAmount => prix unitaire ;
# - number => nombre de jours de remise en état;
# - amount => montant ;
# - invoicingDate => date de facturation ;
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hash|hashref - Liste des jours de remise en état
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    crd_id AS `id`,
    crd_rct_id AS `contract.id`,
    crd_rps_id AS `repairsheet.id`,
    crd_cur_id AS `currency.id`,
    cur_symbol AS `currency.symbol`,
    crd_sta_id AS `state.id`,
    tbl_p_state.ETLIBELLE AS `state.label`,
    crd_unitamount AS `unitAmount`,
    crd_number AS `number`,
    crd_amount AS `amount`,
    crd_date_invoicing AS `invoicingDate`
FROM f_rentcontractrepairday
LEFT JOIN frmwrk.p_currency
ON crd_cur_id = cur_id
LEFT JOIN ETATTABLE tbl_p_state
ON crd_sta_id = tbl_p_state.ETCODE';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query .= '
SELECT
    COUNT(*)
FROM f_rentcontractrepairday';

    }
    else
    {
        $query .= '
SELECT
    crd_id AS `id`,
    crd_number AS `number`
FROM f_rentcontractrepairday';
    }

    # Filtres
    $query .= '
WHERE 1 ';
    if (defined $tabFilters->{'id'})
    {
        $query .= '
        AND crd_id IN (' . $db->quote($tabFilters->{'id'}) . ')';
    }
    if (defined $tabFilters->{'contractId'})
    {
        $query .= '
        AND crd_rct_id IN (' . $db->quote($tabFilters->{'contractId'}) . ')';
    }

    # Options
    if (defined $tabOptions->{'orders'})
    {
        my @orders;
        foreach my $order (@{$tabOptions->{'orders'}})
        {
            my $dir = ($order->{'dir'} ? $order->{'dir'} : '');
            if ($order->{'field'} eq 'sheetId')
            {
                push(@orders, 'crd_rps_id' . ' ' . $dir);
            }
            elsif ($order->{'field'} eq 'id')
            {
                push(@orders, 'crd_id' . ' ' . $dir);
            }
        }
        $query .= '
ORDER BY ' . join(', ', @orders) . ';';
    }
    else
    {
        $query .= '
ORDER BY `repairsheet.id`;';
    }

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'} *= 1;
            $tabRow->{'contract.id'} *= 1;
            $tabRow->{'repairsheet.id'} *= 1;
            $tabRow->{'number'} *= 1;
        }
    }
    return (wantarray ? %$tab : $tab);
}

# Function: set
# Met à jour (insert/update) toutes les infos des jours
#
# Contenu du hachage à passer en paramètre:
# - id              => id
# - contract.id     => id du contrat
# - repairsheet.id  => id de la fiche de remise ne etat
# - currency.id     => id de la devise
# - number          => nombre de jours
# - amout           => montant
#
# Parameters:
# string    $countryId - Pays
# hashref   $values    - Valeurs du service
#
# Returns:
# integer
sub set
{
    my ($countryId, $values) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $tabContract = &LOC::Contract::Rent::getInfos($countryId, $values->{'contract.id'},
                                                     LOC::Contract::Rent::GETINFOS_BASE);
    if ($tabContract->{'amountType'} ne 'day')
    {
        return -1;
    }

    # Mise à jour
    if (defined $values->{'id'} && $values->{'id'} != 0)
    {
        my $result = $db->update('f_rentcontractrepairday', {
                                                 'crd_rct_id'         => $values->{'contract.id'},
                                                 'crd_rps_id'         => $values->{'repairsheet.id'},
                                                 'crd_cur_id'         => $values->{'currency.id'},
                                                 'crd_unitamount'     => $values->{'unitAmount'},
                                                 'crd_number'         => $values->{'number'},
                                                 'crd_amount'         => $values->{'amount'},
                                                 'crd_date_invoicing' => $values->{'invoicingDate'},
                                                 },
                                                 {
                                                 'crd_id'                => $values->{'id'}
                                                 });
        if ($result == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;
        }


        if ($result != -1)
        {
            my $updateAmounts = &LOC::Contract::Rent::updateAmounts($countryId, $values->{'contract.id'});
            if ($updateAmounts == 0)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return -1;
            }
        }

    }
    # Création
    else
    {
        my $crdId = $db->insert('f_rentcontractrepairday', {
                                                 'crd_rct_id'         => $values->{'contract.id'},
                                                 'crd_rps_id'         => $values->{'repairsheet.id'},
                                                 'crd_sta_id'         => STATE_APPROVED,
                                                 'crd_cur_id'         => $values->{'currency.id'},
                                                 'crd_unitAmount'     => $values->{'unitAmount'},
                                                 'crd_number'         => $values->{'number'},
                                                 'crd_amount'         => $values->{'amount'},
                                                 'crd_date_invoicing' => $values->{'invoicingDate'},
                                                 });

        if ($crdId == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;
        }

        if ($crdId != -1)
        {
            # Mise à jour des montants
            my $updateAmounts = &LOC::Contract::Rent::updateAmounts($countryId, $values->{'contract.id'});
            if ($updateAmounts == 0)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return -1;
            }
        }

    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }

    return 1;
}

1;