use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Contract::Rent
# Module de gestion des contrats de location
package LOC::Contract::Rent;


# Constants: États
# STATE_PRECONTRACT - Pré-contrat
# STATE_ACTIVE      - Actif
# STATE_STOPPED     - Arrêté
# STATE_BILLING     - Mis en facturation
# STATE_CANCELED    - Annulé
use constant {
    STATE_PRECONTRACT => 'CON01',
    STATE_CONTRACT    => 'CON02',
    STATE_STOPPED     => 'CON03',
    STATE_ARCHIVE     => 'CON04',
    STATE_CANCELED    => 'CON06',
    STATE_BILLED      => 'CON07'
};

# Constants: Récupération d'informations sur les contrats
# GETINFOS_ALL                - Récupération de l'ensemble des informations
# GETINFOS_BASE               - Récupération des informations de base
# GETINFOS_CUSTOMER           - Récupération des informations du client
# GETINFOS_EXT_SITE           - Récupération des informations du chantier
# GETINFOS_MACHINE            - Récupération des informations de base de la machine
# GETINFOS_EXT_MACHINE        - Récupération des informations de la machine
# GETINFOS_EXT_WISHEDMACHINE  - Récupération des informations sur la machine souhaitée
# GETINFOS_EXT_TARIFFTRSP     - Récupération des informations complètes de la tarification transport
# GETINFOS_TARIFFTRSP         - Récupération des informations de base de la tarification transport
# GETINFOS_EXT_TARIFFRENT     - Récupération des informations complètes de la tarification location
# GETINFOS_TARIFFRENT         - Récupération des informations de base de la tarification location
# GETINFOS_CLEANING           - Récupération des informations sur le nettoyage
# GETINFOS_DURATIONS          - Récupération des informations sur les durées
# GETINFOS_INSURANCE          - Récupération des informations sur l'assurance
# GETINFOS_EXT_MODEL          - Récupération des informations complètes sur le modèle de machine
# GETINFOS_FUEL               - Récupération des informations sur le carburant
# GETINFOS_CREATOR            - Récupération des informations sur le créateur du contrat
# GETINFOS_EXT_REPAIRDAYS     - Récupération des informations sur les jours de remise en état
# GETINFOS_EXT_REPAIRSERVICES - Récupération des informations sur les services de remise en état
# GETINFOS_EXT_RENTSERVICES   - Récupération des informations sur les services de location/transport
# GETINFOS_POSSIBILITIES      - Récupération des possibilités du contrat
# GETINFOS_RESIDUES           - Récupération des informations sur la participation au recyclage
# GETINFOS_RIGHTS             - Récupération des informations sur les droits
# GETINFOS_PRINTABLE          - Récupération des informations sur les droits d'impression du contrat
# GETINFOS_NEGOTIATOR         - Récupération des informations sur le négociateur
# GETINFOS_VALUEDATE          - Récupération des informations sur la date de valeur
# GETINFOS_ACTIONSDATES       - Récupération des dates des actions (arrêt, mise en facturation, etc.)
# GETINFOS_EXT_INSURANCETYPE  - Récupération des informations sur le type d'assurance
# GETINFOS_APPEAL             - Récupération des informations sur la gestion de recours
use constant {
    GETINFOS_ALL                => 0xFFFFFFFF,
    GETINFOS_BASE               => 0x00000000,
    GETINFOS_CUSTOMER           => 0x00000001,
    GETINFOS_EXT_SITE           => 0x00000002,
    GETINFOS_MACHINE            => 0x00000004,
    GETINFOS_EXT_MACHINE        => 0x00000008,
    GETINFOS_EXT_WISHEDMACHINE  => 0x00000010,
    GETINFOS_EXT_TARIFFTRSP     => 0x00000020,
    GETINFOS_TARIFFTRSP         => 0x00000040,
    GETINFOS_EXT_TARIFFRENT     => 0x00000080,
    GETINFOS_TARIFFRENT         => 0x00000100,
    GETINFOS_CLEANING           => 0x00000200,
    GETINFOS_DURATIONS          => 0x00000400,
    GETINFOS_INSURANCE          => 0x00000800,
    GETINFOS_EXT_MODEL          => 0x00001000,
    GETINFOS_FUEL               => 0x00002000,
    GETINFOS_CREATOR            => 0x00004000,
    GETINFOS_EXT_REPAIRDAYS     => 0x00010000,
    GETINFOS_EXT_REPAIRSERVICES => 0x00020000,
    GETINFOS_EXT_RENTSERVICES   => 0x00040000,
    GETINFOS_POSSIBILITIES      => 0x00080000,
    GETINFOS_RESIDUES           => 0x00100000,
    GETINFOS_RIGHTS             => 0x00200000,
    GETINFOS_PRINTABLE          => 0x00400000,
    GETINFOS_NEGOTIATOR         => 0x00800000,
    GETINFOS_VALUEDATE          => 0x01000000,
    GETINFOS_ACTIONSDATES       => 0x02000000,
    GETINFOS_EXT_INSURANCETYPE  => 0x04000000,
    GETINFOS_APPEAL             => 0x08000000,
    GETINFOS_EXT_APPEALTYPE     => 0x10000000
};

# Constants: États de facturation
# INVOICESTATE_NONE         - Non facturé
# INVOICESTATE_PARTIAL      - Facturé partiellement
# INVOICESTATE_FINALPARTIAL - Toute la location facturée
# INVOICESTATE_FINAL        - Entièrement facturé
use constant {
    INVOICESTATE_NONE         => '',
    INVOICESTATE_PARTIAL      => 'P',
    INVOICESTATE_FINALPARTIAL => 'PF',
    INVOICESTATE_FINAL        => 'F'
};

# Constants: États des anciens services de location
# SERVICESTATE_UNBILLED - Non facturé
# SERVICESTATE_BILLED   - Facturé
# SERVICESTATE_DELETED  - Supprimé
use constant {
    SERVICESTATE_UNBILLED => 'SER01',
    SERVICESTATE_BILLED   => 'SER02',
    SERVICESTATE_DELETED  => 'SER03'
};

# Constants: Types d'impression
# PRINTTYPE_PRINT - Imprimante
# PRINTTYPE_PDF   - PDF
# PRINTTYPE_EMAIL - Mail
use constant {
    PRINTTYPE_PRINT => 'print',
    PRINTTYPE_PDF   => 'pdf',
    PRINTTYPE_EMAIL => 'email'
};

# Constants: Erreurs
# ERROR_FATAL                     - Erreur fatale
# ERROR_WRITELOCKED               - Impossible de modifier le contrat
# ERROR_RIGHTS                    - Impossible de modifier cette information du contrat
# ERROR_MODULE_SITE               - Erreur sur l'enregistrement du chantier
# ERROR_MODULE_MACHINE            - Erreur sur l'enregistrement de la machine
# ERROR_MODULE_TARIFFRENT         - Erreur sur l'enregistrement du tarif de location
# ERROR_MODULE_TARIFFTRSP         - Erreur sur l'enregistrement du tarif transport
# ERROR_MODULE_PROFORMA           - Erreur sur l'enregistrement des pro formas
# ERROR_MODULE_TRANSPORT          - Erreur sur l'enregistrement du transport
# ERROR_MODULE_TRANSPORT_DELIVERY - Erreur sur l'enregistrement du transport de livraison
# ERROR_MODULE_TRANSPORT_RECOVERY - Erreur sur l'enregistrement du transport de récupération
# ERROR_MODULE_STARTCODE          - Erreur sur l'enregistrement des codes de démarrage
# ERROR_MODULE_DOCUMENTS          - Erreur sur l'enregistrement des documents
# ERROR_MODULE_DAYS               - Erreur sur l'enregistrement des jours +/-
use constant
{
    ERROR_FATAL                     => 0x0001,
    ERROR_WRITELOCKED               => 0x0002,
    ERROR_RIGHTS                    => 0x0003,

    ERROR_MODULE_SITE               => 0x0130,
    ERROR_MODULE_MACHINE            => 0x0140,
    ERROR_MODULE_TARIFFRENT         => 0x0150,
    ERROR_MODULE_TARIFFTRSP         => 0x0160,
    ERROR_MODULE_PROFORMA           => 0x0170,
    ERROR_MODULE_TRANSPORT          => 0x0180,
    ERROR_MODULE_TRANSPORT_DELIVERY => 0x0181,
    ERROR_MODULE_TRANSPORT_RECOVERY => 0x0182,
    ERROR_MODULE_STARTCODE          => 0x0190,
    ERROR_MODULE_DOCUMENTS          => 0x0400,
    ERROR_MODULE_DAYS               => 0x0500
};

use strict;


my $fatalErrMsg = 'Erreur fatale: module LOC::Contract::Rent, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";

# Liste des erreurs:
#
# 0x0001 => Erreur fatale
# 0x0002 => Impossible de modifier le contrat
# 0x0003 => Impossible de modifier cette information du contrat
#
# 0x0010 => Négociateur non renseigné
# 0x0100 => Date de modification invalide
# 0x0101 => Impossible de passer la ligne en contrat
# 0x0102 => Agence invalide
# 0x0103 => Zone invalide
# 0x0104 => Client invalide
# 0x0105 => Client bloqué au contrat
# 0x0106 => Impossible de prolonger le contrat
# 0x0107 => Impossible de modifier le contrat
# 0x0108 => Impossible de prolonger le contrat sur un client PAL
# 0x0109 => Les dates de début et de fin ne sont pas renseignées
# 0x0110 => La zone ne peut pas être modifiée
# 0x0111 => Le modèle de machines n'est pas disponible
# 0x0112 => Impossible de prolonger un contrat arrêter à plus de x jours
# 0x0113 => L'horaire de livraison n'est pas renseigné
# 0x0114 => Le modèle de la machine souhaitée ne correspond pas au modèle sélectionné
# 0x0115 => Modèle de machine invalide
# 0x0116 => Durée invalide
# 0x0117 => La date de fin est inférieure à la date de début
# 0x0118 => La date de fin est inférieure à la date de dernière facturation
# 0x0119 => La date de début n'est pas modifiable
# 0x0120 => La date de fin n'est pas modifiable
# 0x0121 => La date de début ne doit pas être inférieure à 1 mois
# 0x0123 => Le nombre de jours ouvrés a changé
# 0x0124 => Le type d'assurance n'est pas modifiable
# 0x0125 => Le taux d'assurance n'est pas modifiable
# 0x0126 => Mode de calcul de la participation au recyclage non valide
# 0x0127 => Email du contact commande non valide
# 0x0128 => Informations du contact commande incomplètes
# 0x0129 => Mode de calcul de la gestion de recours non valide
# 0x0200 => Impossible d'arrêter le contrat
# 0x0201 => Impossible de remettre en cours le contrat
# 0x0202 => Impossible de mettre en facturation le contrat
# 0x0203 => Impossible de remettre en arrêt le contrat
# 0x0204 => Impossible d'annuler le contrat

# 0x0130 => Erreur sur l'enregistrement du chantier
# 0x0140 => Erreur sur l'enregistrement de la machine
# 0x0150 => Erreur sur l'enregistrement du tarif de location
# 0x0160 => Erreur sur l'enregistrement du tarif transport
# 0x0170 => Erreur sur l'enregistrement des pro formas
# 0x0180 => Erreur sur l'enregistrement du transport
# 0x0190 => Erreur sur l'enregistrement des codes de démarrage
# 0x0400 => Erreur sur l'enregistrement des documents

# 0x0301 => Une demande de remise exceptionnelle de location est en attente
# 0x0302 => Une demande de remise exceptionnelle de transport est en attente
# 0x0303 => L'ouverture du compte client est en attente
# 0x0304 => Aucune pro forma n'est active sur le contrat
# 0x0305 => Une pro forma non à jour est présente sur le contrat
# 0x0306 => Une pro forma n'est pas finalisée sur le contrat
# 0x0307 => La machine n'est pas disponible
# 0x0308 => Une machine est déjà associée/attribuée sur le contrat
# 0x0309 => Le modèle de la machine ne correspond pas à celui demandé sur le contrat
# 0x0310 => Impossible d'attribuer la machine sur un état différent de pré-contrat


use Date::Calc;
use LOC::Db;
use LOC::Locale;
use LOC::Util;
use LOC::Country;
use LOC::Site;
use LOC::Model;
use LOC::Tariff::Rent;
use LOC::Tariff::Transport;
use LOC::Date;
use LOC::Machine;
use LOC::Invoice::Rent;
use LOC::Invoice::Repair;
use LOC::Contract;
use LOC::Contract::RepairDay;
use LOC::Contract::RepairService;
use LOC::Common::RentService;
use LOC::Cleaning;
use LOC::Estimate::Rent;
use LOC::Log;
use LOC::Mail;
use LOC::Repair::Sheet;
use LOC::MachinesFamily;
use LOC::User;
use LOC::Common::Rent;
use LOC::Characteristic;
use LOC::Transport;
use LOC::Proforma;
use LOC::EndUpdates;
use LOC::StartCode::Contract::Rent;
use LOC::Attachments;
use LOC::MachineInventory;
use LOC::Day;
use LOC::Insurance::Type;


# Sauvegarde des contrats existants
my $_tabExists = {};


# Function: generateCode
# Générer le code d'un contrat
#
# Parameters:
# int    $contractId - Id du contrat
# string $agencyId   - Id de l'agence du contrat
# bool   $type       - Typage du contrat
#
# Returns:
# string -
sub generateCode
{
    my ($contractId, $agencyId, $type) = @_;

    return &LOC::Contract::generateCode($contractId, $agencyId, $type);
}


# Function: getInfos
# Retourne un hachage contenant les infos du contrat de location
#
# Contenu d'une ligne du hashage : voir la méthode getList.
#
# Parameters:
# string  $countryId  - Pays
# int     $id         - Id du contrat
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hash|hashref - Informations du contrat
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;

    if ($id)
    {
        if (!defined $flags)
        {
            $flags = GETINFOS_BASE;
        }
        if (!defined $tabOptions)
        {
            $tabOptions = {};
        }

        # Récupération de la connexion à la base de données
        my $db = &LOC::Db::getConnection('location', $countryId);

        my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);

        if ($tab && exists $tab->{$id} && (!$tabOptions->{'lockDbRow'} || &_lockDbRow($db, $id)))
        {
            return (wantarray ? %{$tab->{$id}} : $tab->{$id});
        }
    }
    return undef;
}


# Function: exists
# Vérifie si le contrat correspondant à l'identifiant passé en paramètre existe.
#
# Parameters:
# string  $id - Identifiant du contrat
#
# Returns:
# int - 1 si le contrat existe, 0 sinon.
sub exists
{
    my ($id) = @_;

    if ($id eq '')
    {
        return 0;
    }

    if (!defined $_tabExists->{$id})
    {
        $_tabExists->{$id} = (&getList(LOC::Util::GETLIST_COUNT, {'id' => $id}) > 0);
    }

    return $_tabExists->{$id};
}


# Function: getLinkedContractsInfos
# Récupérer les informations des contrats enfants et parent du contrat passé en paramètre
#
# Parameters:
# string $countryId  - Pays
# int    $contractId - Id du contrat
#
# Returns:
# hash|hashref - Informations contrats parent et enfants
sub getLinkedContractsInfos
{
    my ($countryId, $contractId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $tab = {'parent' => {}, 'children' => {}};

    my $query = '
SELECT
    tbl_f_contract.CONTRATCODE,
    tbl_f_contract.CONTRATPERE
FROM CONTRAT tbl_f_contract
WHERE tbl_f_contract.CONTRATAUTO = ' . $contractId . ';';
    my $tabInfos = $db->fetchRow($query, {}, LOC::Db::FETCH_NUM);
    if ($tabInfos)
    {
        my $code = $tabInfos->[0];
        my $parentCode = $tabInfos->[1];

        # todo : ajout famille renommage champs
        my $baseQuery = '
SELECT
    tbl_f_contract.CONTRATAUTO AS `id`,
    tbl_f_contract.CONTRATCODE AS `code`,
    tbl_f_machine.MAAUTO AS `machine.id`,
    tbl_f_machine.MANOPARC AS `machine.parkNumber`,
    tbl_p_model.MOMAAUTO AS `model.id`,
    tbl_p_model.MOMADESIGNATION AS `model.label`,
    CONCAT_WS(" ", tbl_p_machinefamily.FAMAMODELE,
                   tbl_p_machinefamily.FAMAENERGIE,
                   tbl_p_machinefamily.FAMAELEVATION) AS `machinesFamily.fullName`
FROM CONTRAT tbl_f_contract
LEFT JOIN AUTH.MACHINE tbl_f_machine
ON tbl_f_machine.MAAUTO = tbl_f_contract.MAAUTO
LEFT JOIN AUTH.MODELEMACHINE tbl_p_model
ON tbl_p_model.MOMAAUTO = tbl_f_machine.MOMAAUTO
LEFT JOIN FAMILLEMACHINE tbl_p_machinefamily
ON tbl_p_model.FAMAAUTO = tbl_p_machinefamily.FAMAAUTO';

        if ($parentCode ne '')
        {
            # Recherche du père
            $query = $baseQuery . '
WHERE tbl_f_contract.CONTRATCODE = "' . $parentCode . '";';
            $tab->{'parent'} = $db->fetchAssoc($query, {}, 'id');
            if (!$tab->{'parent'})
            {
                $tab->{'parent'} = {};
            }
        }
        elsif ($code ne '')
        {
            # Recherche des fils
            $query = $baseQuery . '
WHERE tbl_f_contract.CONTRATPERE = "' . $code . '";';
            $tab->{'children'} = $db->fetchAssoc($query, {}, 'id');
            if (!$tab->{'children'})
            {
                $tab->{'children'} = {};
            }
        }
    }
    return (wantarray ? %$tab : $tab);
}


# Function: getOldServices
# Récupérer les informations des services complémentaires du Portail avant la Gesloc
#
# Parameters:
# string $countryId  - Pays
# int    $contractId - Id du contrat
#
# Returns:
# hash|hashref - Informations service complémentaires Portail
sub getOldServices
{
    my ($countryId, $contractId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT
    tbl_f_contract_service.COSERAUTO AS `id`,
    CONCAT(IFNULL(CONCAT(tbl_p_service.SERLIBELLE, " "), ""), tbl_f_contract_service.COSERLIBELLE) AS `label`,
    IFNULL(tbl_f_contract_service.COSERMONTANT, tbl_p_service.SERMONTANT) AS `amount`,
    tbl_f_contract_service.COSERDATEFAC AS `invoicingDate`,
    tbl_f_contract_service.ETCODE AS `state.id`,
    tbl_p_state.ETLIBELLE AS `state.label`
FROM CONTRATSERVICES tbl_f_contract_service
LEFT JOIN SERVICES tbl_p_service
ON tbl_f_contract_service.SERAUTO = tbl_p_service.SERAUTO
LEFT JOIN ETATTABLE tbl_p_state
ON tbl_f_contract_service.ETCODE = tbl_p_state.ETCODE
WHERE tbl_f_contract_service.CONTRATAUTO = ' . $contractId . ';';

    my $tabResult = $db->fetchAssoc($query, {}, 'id');

    foreach my $tabRow (values %$tabResult)
    {
        # Formatage des données
        $tabRow->{'id'}     *= 1;
        $tabRow->{'amount'} *= 1;
    }

    return (wantarray ? %$tabResult : $tabResult);
}

# Function: getList
# Récupérer la liste des contrats de location
#
# Contenu d'une ligne du hashage :
# - id => identifiant ;
# - code => code ;
# - insuranceType.id => identifiant du type d'assurance ;
# - insurance.rate => taux d'assurance ;
# - beginDate => date de début ;
# - endDate => date de fn ;
# - duration => durée de location ;
# - tracking => notes (commentaires internes) ;
# - comments => commentaires (figurant sur le contrat) ;
# - isActive => ligne de contrat active (1) ou non (0) ;
# - index => indice de modification ;
# - isLocked => contrat bloqué (1) ou non (0) ;
# - additionalDaysUnbilled => nombre de jours additionnels non facturés ;
# - additionalDaysComment => commentaire sur les jours additionnels
# - deductedDaysUnbilled => nombre de jours déduits non facturés ;
# - deductedDaysComment => commentaire sur les jours déduits
# - additionalDaysBilled => nombre de jours additionnels facturés ;
# - deductedDaysBilled => nombre de jours déduits facturés ;
# - orderNo => numéro de commande ;
# - isPrinted => contrat imprimé (1) ou non (0) ;
# - isImperativeModel => modèle impératif (1) ou non (0) ;
# - agency.id => identifiant de l'agence ;
# - fatherContract.code => code du contrat père ;
# - hasDefaultAccessories => accessoires par défaut (1) ou non (0) ;
# - isOrderCompulsory => bon de commande obligatoire (1) ou non (0) ;
# - isOutOfPark => hors parc (1) ou non (0) ;
# - country.id => identifiant du pays ;
# - customer.id => identifiant du client ;
# - customer.code => code du client ;
# - customer.name => nom ou raison sociale du client ;
# - customer.siret => code SIRET (ou équivalent étranger) du client ;
# - customer.controlAccount => code collectif du client ;
# - customer.accountingCategory => catégorie comptable du client
# - customer.deliveryRecoveryNotesRequired => booléen indiquant si l'envoi des BL/BR avec la facture est obligatoire ;
# - customer.isNewAccount => s'agit-il d'un nouveau compte ?
# - residues.mode => Indique le mode de calcul de la participation au recyclage (0: non facturée, 1: forfait, 2: pourcentage)
# - residues.value => Valeur de la participation au recyclage pour le calcul (montant forfaitaire, pourcentage)
# - residues.amount => Montant de la participation au recyclage en monnaie locale
# - site.id => identifiant du chantier ;
# - site.tabInfos => Informations du chantier (si $flags & GETINFOS_EXT_SITE)
# - zone => zone de transport ;
# - isFullService => contrat full service (1) ou non (0) ;
# - model.id => identifiant du modèle de machines ;
# - machine.id => identifiant de la machine ;
# - machine.parkNumber => numéro de parc de la machine ;
# - machinesFamily.id => identifiant de la famille de machines ;
# - machinesFamily.label => libellé de la famille de machines ;
# - machinesFamily.energy => énergie de la famille de machines ;
# - machinesFamily.elevation => élévation de la famille de machines ;
# - state.id => identifiant de l'état ;
# - state.label => libellé de l'état ;
# - invoiceState => état courant de facturation ;
# - previousInvoiceState => état précédent de facturation ;
# - lastInvoiceDate => date jusqu'à laquelle a été facturée la location ;
# - creator.id => identifiant de l'utilisateur qui a créé le contrat ;
# - modificationDate => date de dernière modification ;
# - creationDate => date de création ;
# - transportTariff.isNoDelivery => enlèvement sur place pour la livraison ? ;
# - transportTariff.isAddMachineDelivery => machine supplémentaire pour la livraison ;
# - transportTariff.addMachineDeliveryLnkCtt.id => id du contrat lié en cas de machine supplémentaire pour la livraison ;
# - transportTariff.isNoRecovery => enlèvement sur place pour la récupération ? ;
# - transportTariff.isAddMachineRecovery => machine supplémentaire pour la récupération ;
# - transportTariff.addMachineRecoveryLnkCtt.id => id du contrat lié en cas de machine supplémentaire pour la récupération ;
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hash|hashref - Liste des contrats
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $tariffRentSchemaName = 'GESTIONTARIF' . $countryId;
    my $tariffTrspSchemaName = 'GESTIONTRANS' . $countryId;

    my $daysToStop = &LOC::Characteristic::getCountryValueByCode('DAYSTOSTOP', $countryId);

    my $query = '
SELECT ' . (defined $tabOptions->{'limit'} && exists ($tabOptions->{'limit'}->{'&total'}) ? 'SQL_CALC_FOUND_ROWS' : '');

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        # Pour savoir si il est possible d'arrêter le contrat, le mettre en factu, etc...
        # il faut les informations sur les estimations de location et transport
        if (($flags & GETINFOS_POSSIBILITIES) || ($flags & GETINFOS_PRINTABLE))
        {
            if (!($flags & GETINFOS_EXT_TARIFFRENT))
            {
                $flags = $flags | GETINFOS_TARIFFRENT;
            }
            if (!($flags & GETINFOS_EXT_TARIFFTRSP))
            {
                $flags = $flags | GETINFOS_TARIFFTRSP;
            }
            if (!($flags & GETINFOS_EXT_MACHINE))
            {
                $flags = $flags | GETINFOS_EXT_MACHINE;
            }
        }
        if (($flags & GETINFOS_RESIDUES) && !($flags & GETINFOS_EXT_MODEL))
        {
            $flags = $flags | GETINFOS_EXT_MODEL;
        }
        if (($flags & GETINFOS_EXT_INSURANCETYPE) && !($flags & GETINFOS_INSURANCE))
        {
            $flags = $flags | GETINFOS_INSURANCE;
        }
        if (($flags & GETINFOS_EXT_APPEALTYPE) && !($flags & GETINFOS_APPEAL))
        {
            $flags = $flags | GETINFOS_APPEAL;
        }

        $query .= '
    tbl_f_contract.CONTRATAUTO AS `id`,
    tbl_f_contract.DETAILSDEVISAUTO AS `linkedEstimateLine.id`,
    tbl_f_contract.CONTRATCODE AS `code`,
    tbl_f_contract.CONTRATDD AS `beginDate`,
    tbl_f_contract.CONTRATDR AS `endDate`,
    tbl_f_contract.CONTRATDUREE AS `duration`,
    tbl_f_contract.CONTRATNOTE AS `tracking`,
    tbl_f_contract.CONTRATCOMMENTAIRE AS `comments`,
    -tbl_f_contract.CONTRATOK AS `isActive`,
    tbl_f_contract.CONTRATINDICE AS `index`,
    -tbl_f_contract.CONTRATVERIFIER AS `isLocked`,
    tbl_f_contract.CONTRATJOURSPLUS AS `additionalDaysUnbilled`,
    tbl_f_contract.CONTRATJPCOMM AS `additionalDaysComment`,
    tbl_f_contract.CONTRATJOURSMOINS AS `deductedDaysUnbilled`,
    tbl_f_contract.CONTRATJMCOMM AS `deductedDaysComment`,
    tbl_f_contract.CONTRATJOURPLUSFACT AS `additionalDaysBilled`,
    tbl_f_contract.CONTRATJOURMOINSFACT AS `deductedDaysBilled`,
    tbl_f_contract.CONTRATNUMCOMM AS `orderNo`,
    tbl_f_contract.CONTRATCONTACTCOMM AS `orderContact.fullName`,
    tbl_f_contract.CONTRATTELEPHONECOMM AS `orderContact.telephone`,
    tbl_f_contract.CONTRATEMAILCOMM AS `orderContact.email`,
    tbl_f_contract.CONTRATIMP + 1 AS `isPrinted`,
    -tbl_f_contract.CONTRATMODIMP AS `isImperativeModel`,
    tbl_f_contract.AGAUTO AS `agency.id`,
    tbl_f_contract.CONTRATPERE AS `fatherContract.code`,
    tbl_f_contract.CONTRATACCDEFT AS `hasDefaultAccessories`,
    tbl_f_contract.BCENVOI AS `isOrderCompulsory`,
    tbl_f_contract.CONTRATHORSPARC AS `isOutOfPark`,
    "' . $countryId . '" AS `country.id`,
    tbl_f_contract.CLAUTO AS `customer.id`,
    tbl_f_contract.CHAUTO AS `site.id`,
    tbl_f_contract.ZONE AS `zone`,
    -tbl_f_contract.CONTRATFULLSERVICE AS `isFullService`,
    IF(tbl_f_contractamount.MOFORFAITFR = 0, IF(tbl_f_contractamount.MOTARIFMSFR = 0, "day", "month"), "fixed") AS `amountType`,
    IF(tbl_f_contractamount.MOFORFAITFR = 0, IF(tbl_f_contractamount.MOTARIFMSFR = 0, tbl_f_contractamount.MOPXJOURFR, tbl_f_contractamount.MOTARIFMSFR), tbl_f_contractamount.MOFORFAITFR) AS `unitPrice`,
    tbl_f_contractamount.MOHTFRS AS `total`,
    tbl_f_contract.CARBUAUTO AS `fuel.id`,
    -tbl_f_contract.CONTRATMACHATTR AS `isFinalAllocation`,
    tbl_f_contract.MAAUTO AS `machine.id`,';
    if ($flags & GETINFOS_MACHINE)
    {
        $query .= '
    CONCAT(IFNULL(tbl_f_machine.MANOPARC_AUX, ""), tbl_f_machine.MANOPARC) AS `machine.parkNumber`,
    tbl_f_machine.ETCODE AS `machine.state.id`,
    tbl_f_machine.AGAUTO AS `machine.agency.id`,
    tbl_f_machine.MOMAAUTO AS `machine.model.id`,
    -tbl_f_machine.MABLOQUECH AS `machine.isSiteBlocked`,';
    }
    $query .= '
    tbl_f_contract.MAAUTO_SOUHAITE AS `wishedMachine.id`,
    tbl_f_contract.MOMAAUTO AS `model.id`,
    -tbl_f_contract.CONTRATJOINDOCUMENT AS `isJoinDoc`,';
        if ($flags & GETINFOS_CUSTOMER)
        {
            $query .= '
    tbl_f_customer.CLCODE AS `customer.code`,
    tbl_f_customer.CLSTE AS `customer.name`,
    tbl_f_job.MEACTIVITE AS `customer.activity`,
    tbl_f_customer.CLSIREN AS `customer.siret`,
    tbl_f_customer.CLEMAIL AS `customer.email`,
    tbl_f_customer.CLCOLLECTIF AS `customer.controlAccount`,
    tbl_f_customer.CLCEE AS `customer.accountingCategory`,
    -tbl_f_customer.CLBONSOBLIG AS `customer.deliveryRecoveryNotesRequired`,
    -tbl_f_customer.CLPROFORMA AS `customer.isProformaRequired`,';
        }
        if ($flags & GETINFOS_RESIDUES)
        {
            $query .= '
    -tbl_f_contract.CONTRATDECHETSFACT AS `residues.mode`,
    tbl_f_contract.CONTRATDECHETSVAL AS `residues.value`,
    tbl_f_contractamount.MODECHETS AS `residues.amount`,';
        }
        if ($flags & GETINFOS_FUEL)
        {
            $query .= '
    tbl_f_fuel.CAQTE AS `fuel.quantity`,
    tbl_f_fuel.CAMONTANTFR AS `fuel.amount`,
    IF (tbl_f_fuel.CAQTE = 0, 0, tbl_f_fuel.CAMONTANTFR / tbl_f_fuel.CAQTE) AS `fuel.unitPrice`,';
        }
        if ($flags & GETINFOS_CLEANING)
        {
            $query .= '
    IFNULL(tbl_f_contract_cleaning.NETAUTO, 0) AS `cleaning.type.id`,
    IFNULL(tbl_p_cleaning.NETLIBELLE, "") AS `cleaning.type.label`,
    IFNULL(tbl_p_cleaning.NETENERGIE, "") AS `cleaning.type.energy`,
    IFNULL(tbl_f_contract_cleaning.CONETMONTANT, IFNULL(tbl_p_cleaning.NETMONTANT, 0)) AS `cleaning.amount`,';
        }
        if ($flags & GETINFOS_INSURANCE)
        {
            $query .= '
    tbl_f_contract.CONTRATTYPEASSURANCE AS `insuranceType.id`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISINVOICED) . ', tbl_f_contract.CONTRATPASSURANCE, 0) AS `insurance.rate`,
    tbl_f_contractamount.MOASSFR AS `insurance.amount`,
    p_insurancetype.ity_flags AS `insurance.flags`,
    p_insurancetype.ity_mode AS `insurance.mode`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISINVOICED) . ', 1, 0) AS `insurance.isInvoiced`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISINCLUDED) . ', 1, 0) AS `insurance.isIncluded`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISCUSTOMER) . ', 1, 0) AS `insurance.isCustomer`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISAPPEALACTIVATED) . ', 1, 0) AS `insurance.isAppealActivated`,';
        }
        if ($flags & GETINFOS_APPEAL)
        {
            $query .= '
    tbl_f_contract.CONTRATTYPERECOURS AS `appealType.id`,
    tbl_f_contract.CONTRATRECOURSVAL AS `appeal.value`,
    tbl_f_contractamount.MORECOURS AS `appeal.amount`,';
        }
        if ($flags & GETINFOS_TARIFFRENT)
        {
            $query .= '
    IF(tbl_f_contractamount.MOFORFAITFR = 0, IF(tbl_f_contractamount.MOTARIFMSFR = 0, "day", "month"), "fixed") AS `rentTariff.amountType`,
    tbl_f_contractamount.MOTARIFBASE AS `rentTariff.baseAmount`,
    tbl_f_contractamount.MOREMISE AS `rentTariff.standardReductionPercent`,
    tbl_f_contractamount.MOREMISEMAX AS `rentTariff.maxiReductionPercent`,
    tbl_f_contractamount.MOREMISEEX AS `rentTariff.specialReductionPercent`,
    IF (tbl_f_contractamount.MOREMISEEX > 0,
        ELT(1-tbl_f_contractamount.MOREMISEOK, "' . LOC::Tariff::Rent::REDUCTIONSTATE_REFUSED . '", "' . LOC::Tariff::Rent::REDUCTIONSTATE_PENDING . '", "' . LOC::Tariff::Rent::REDUCTIONSTATE_VALIDATED . '"),
        IF (tbl_f_rentestim2.VALIDOK = 0, "' . LOC::Tariff::Rent::REDUCTIONSTATE_REFUSED . '", "")) AS `rentTariff.specialReductionStatus`,';
        }
        if ($flags & GETINFOS_TARIFFTRSP)
        {
            $query .= '
    tbl_f_trspestim.TARIFBASE AS `transportTariff.baseAmount`,
    tbl_f_trspestim.REMISEAPP AS `transportTariff.standardReductionPercent`,
    tbl_f_trspestim.ZONE AS `transportTariff.zone`,
    tbl_f_trspestim.CLIENT AS `transportTariff.customer.id`,
    IF (tbl_f_trspspcreduc.ID IS NULL, 0, IF (tbl_f_trspspcreduc.ETAT <> "' . LOC::Tariff::Transport::REDUCTIONSTATE_PENDING . '", tbl_f_trspspcreduc.REMEXACC, tbl_f_trspspcreduc.REMEXDEM)) AS `transportTariff.specialReductionPercent`,
    IFNULL(tbl_f_trspspcreduc.ETAT, "") AS `transportTariff.specialReductionStatus`,
    tbl_f_contractamount.MOTRANSPORTDFR AS `transportTariff.deliveryAmount`,
    -tbl_f_contract.CONTRATENLVTDP AS `transportTariff.isNoDelivery`,
    -tbl_f_contract.CONTRATMACHSUPALLER AS `transportTariff.isAddMachineDelivery`,
    tbl_f_contract.CONTRATAUTO_MACHSUPALLER AS `transportTariff.addMachineDeliveryLnkCtt.id`,
    tbl_f_contractamount.MOTRANSPORTRFR AS `transportTariff.recoveryAmount`,
    -tbl_f_contract.CONTRATENLVTRT AS `transportTariff.isNoRecovery`,
    -tbl_f_contract.CONTRATMACHSUPRETOUR AS `transportTariff.isAddMachineRecovery`,
    tbl_f_contract.CONTRATAUTO_MACHSUPRETOUR AS `transportTariff.addMachineRecoveryLnkCtt.id`,
    IF (tbl_f_trspestim.ETAT = "' . LOC::Tariff::Transport::REDUCTIONSTATE_PENDING . '", 1, 0) AS `transportTariff.isNoSaved`,';
        }
        $query .= '
    tbl_f_contract.ETCODE AS `state.id`,
    tbl_p_state.ETLIBELLE AS `state.label`,
    tbl_f_contract.ETATFACTURE AS `invoiceState`,
    tbl_f_contract.ANCIENETATFACTURE AS `previousInvoiceState`,
    IF (tbl_f_contract.ETATFACTURE = "' . INVOICESTATE_NONE . '", "", tbl_f_contract.CONTRATDATEFACTURE) AS `lastInvoiceDate`,
    tbl_f_contract.PEAUTO AS `creator.id`,';
        if ($flags & GETINFOS_CREATOR)
        {
            $query .= '
    f_user_creator.usr_firstname AS `creator.firstName`,
    f_user_creator.usr_name AS `creator.name`,
    CONCAT_WS(" ", f_user_creator.usr_firstname, f_user_creator.usr_name) AS `creator.fullName`,';
        }
        $query .= '
    tbl_f_contract.PEAUTO_INTERLOC AS `negotiator.id`,';
        if ($flags & GETINFOS_NEGOTIATOR)
        {
            $query .= '
    f_user_negotiator.usr_firstname AS `negotiator.firstName`,
    f_user_negotiator.usr_name AS `negotiator.name`,
    CONCAT_WS(" ", f_user_negotiator.usr_firstname, f_user_negotiator.usr_name) AS `negotiator.fullName`,';
        }
        if ($flags & GETINFOS_POSSIBILITIES)
        {
            $query .= '
    IF (CURRENT_DATE() > DATE_SUB(tbl_f_contract.CONTRATDR, INTERVAL ' . $daysToStop . ' DAY), 1, 0) AS `isEndDateNear`,';
        }
        $query .= '
    -tbl_f_contract.CONTRATVERROU AS `isWriteLocked`,
    tbl_f_contract.CONTRATPROFORMAOK AS `proformaFlags`,
    tbl_f_contract.CONTRATDATECLOTURE AS `closureDate`,
    tbl_f_contract.CONTRATDATEMODIF AS `modificationDate`,
    tbl_f_contract.CONTRATDATECREATION AS `creationDate`
FROM CONTRAT tbl_f_contract
LEFT JOIN ETATTABLE tbl_p_state
ON tbl_f_contract.ETCODE = tbl_p_state.ETCODE';

        if ($flags & GETINFOS_CREATOR)
        {
            $query .= '
LEFT JOIN frmwrk.f_user f_user_creator
ON tbl_f_contract.PEAUTO = f_user_creator.usr_id';
        }
        if ($flags & GETINFOS_NEGOTIATOR)
        {
            $query .= '
LEFT JOIN frmwrk.f_user f_user_negotiator
ON tbl_f_contract.PEAUTO_INTERLOC = f_user_negotiator.usr_id';
        }
        if ($flags & GETINFOS_CUSTOMER)
        {
            $query .= '
LEFT JOIN CLIENT tbl_f_customer
ON tbl_f_customer.CLAUTO = tbl_f_contract.CLAUTO
LEFT JOIN METIER tbl_f_job
ON tbl_f_job.METIERAUTO = tbl_f_customer.METIERAUTO ';
        }
        $query .= '
LEFT JOIN MONTANT tbl_f_contractamount
ON tbl_f_contract.MOAUTO = tbl_f_contractamount.MOAUTO
LEFT JOIN AUTH.MACHINE tbl_f_machine
ON tbl_f_contract.MAAUTO = tbl_f_machine.MAAUTO';
        if ($flags & GETINFOS_FUEL)
        {
            $query .= '
LEFT JOIN CARBURANT tbl_f_fuel
ON tbl_f_contract.CARBUAUTO = tbl_f_fuel.CARBUAUTO';
        }
        if ($flags & GETINFOS_CLEANING)
        {
            $query .= '
LEFT JOIN CONTRATNETTOYAGE tbl_f_contract_cleaning
ON tbl_f_contract_cleaning.CONTRATAUTO = tbl_f_contract.CONTRATAUTO
LEFT JOIN NETTOYAGE tbl_p_cleaning
ON tbl_f_contract_cleaning.NETAUTO = tbl_p_cleaning.NETAUTO';
        }
        if ($flags & GETINFOS_TARIFFRENT)
        {
            $query .= '
LEFT JOIN ' . $tariffRentSchemaName . '.GC_ARCHIVES tbl_f_rentestim2
ON tbl_f_rentestim2.CONTRATAUTO = tbl_f_contract.CONTRATAUTO';
        }
        if ($flags & GETINFOS_TARIFFTRSP)
        {
            $query .= '
LEFT JOIN ' . $tariffTrspSchemaName . '.TRANSPORT tbl_f_trspestim
ON tbl_f_trspestim.TYPE = "CONTRAT" AND
   tbl_f_trspestim.VALEUR = tbl_f_contract.CONTRATAUTO AND
   tbl_f_trspestim.ETAT <> "' . LOC::Tariff::Transport::REDUCTIONSTATE_CANCELED . '"
LEFT JOIN ' . $tariffTrspSchemaName . '.REMISEEX tbl_f_trspspcreduc
ON tbl_f_trspspcreduc.ID = tbl_f_trspestim.REMISEEX';
        }
        if ($flags & GETINFOS_INSURANCE)
        {
            $query .= '
LEFT JOIN p_insurancetype
ON p_insurancetype.ity_id = tbl_f_contract.CONTRATTYPEASSURANCE';
        }
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query .= '
    COUNT(*)
FROM CONTRAT tbl_f_contract
LEFT JOIN AUTH.MACHINE tbl_f_machine
ON tbl_f_contract.MAAUTO = tbl_f_machine.MAAUTO';
    }
    else
    {
        $query .= '
    tbl_f_contract.CONTRATAUTO AS `id`,
    tbl_f_contract.CONTRATCODE AS `code`
FROM CONTRAT tbl_f_contract
LEFT JOIN AUTH.MACHINE tbl_f_machine
ON tbl_f_contract.MAAUTO = tbl_f_machine.MAAUTO';
    }


    # Jointures pour les recherches
    if (defined $tabFilters->{'siteLabel'} || defined $tabFilters->{'siteDepartment'})
    {
        $query .= '
LEFT JOIN CHANTIER tbl_f_site
ON tbl_f_contract.CHAUTO = tbl_f_site.CHAUTO';
    }
    if (defined $tabFilters->{'siteDepartment'})
    {
        $query .= '
LEFT JOIN VILLECHA tbl_p_sitelocality
ON tbl_f_site.VILLEAUTO = tbl_p_sitelocality.VILLEAUTO';
    }
    if (defined $tabFilters->{'tariffFamilyId'})
    {
        $query .= '
LEFT JOIN AUTH.MODELEMACHINE tbl_f_model
ON tbl_f_contract.MOMAAUTO = tbl_f_model.MOMAAUTO';
    }


    # Filtres
    $query .= '
WHERE tbl_f_contract.SOLOAUTO = 0';
    if (defined $tabFilters->{'id'})
    {
        $query .= '
AND tbl_f_contract.CONTRATAUTO IN (' . &LOC::Util::join(', ', $tabFilters->{'id'}) . ')';
    }
    if (defined $tabFilters->{'code'})
    {
        if ($tabFilters->{'code'} =~ /%/)
        {
            $query .= '
AND tbl_f_contract.CONTRATCODE LIKE ' . $db->quote($tabFilters->{'code'});
        }
        else
        {
            $query .= '
AND tbl_f_contract.CONTRATCODE IN (' . $db->quote($tabFilters->{'code'}) . ')';
        }
    }
    if (defined $tabFilters->{'machineId'})
    {
        $query .= '
AND tbl_f_contract.MAAUTO IN (' . &LOC::Util::join(', ', $tabFilters->{'machineId'}) . ')';
    }
    if (defined $tabFilters->{'agencyId'})
    {
        $query .= '
AND tbl_f_contract.AGAUTO IN (' . $db->quote($tabFilters->{'agencyId'}) . ')';
    }
    if (defined $tabFilters->{'customerId'})
    {
        $query .= '
AND tbl_f_contract.CLAUTO IN (' . &LOC::Util::join(', ', $tabFilters->{'customerId'}) . ')';
    }
    if (defined $tabFilters->{'userId'})
    {
        $query .= '
AND tbl_f_contract.PEAUTO IN (' . &LOC::Util::join(', ', $tabFilters->{'userId'}) . ')';
    }
    if (defined $tabFilters->{'negotiatorId'})
    {
        $query .= '
AND tbl_f_contract.PEAUTO_INTERLOC IN (' . &LOC::Util::join(', ', $tabFilters->{'negotiatorId'}) . ')';
    }
    if (defined $tabFilters->{'stateId'})
    {
        $query .= '
AND tbl_f_contract.ETCODE IN (' . $db->quote($tabFilters->{'stateId'}) . ')';
    }
    if (defined $tabFilters->{'parkNo'})
    {
        $tabFilters->{'parkNo'} =~ s/^A//;
        $query .= '
AND tbl_f_machine.MANOPARC IN (' . $db->quote($tabFilters->{'parkNo'}) . ')';
    }
    if (defined $tabFilters->{'siteLabel'})
    {
        if ($tabFilters->{'siteLabel'} =~ /%/)
        {
            $query .= '
AND tbl_f_site.CHLIBELLE LIKE ' . $db->quote($tabFilters->{'siteLabel'});
        }
        else
        {
            $query .= '
AND tbl_f_site.CHLIBELLE IN (' . $db->quote($tabFilters->{'siteLabel'}) . ')';
        }
    }
    if (defined $tabFilters->{'siteDepartment'})
    {
        $query .= '
AND tbl_p_sitelocality.DEPVILLE IN (' . $db->quote($tabFilters->{'siteDepartment'}) . ')';
    }
    if (defined $tabFilters->{'beginDate'})
    {
        if (ref($tabFilters->{'beginDate'}) ne 'ARRAY')
        {
            $tabFilters->{'beginDate'} = [$tabFilters->{'beginDate'}, undef];
        }

        if (defined $tabFilters->{'beginDate'}->[0])
        {
            $query .= '
AND DATE(tbl_f_contract.CONTRATDD) >= ' . $db->quote($tabFilters->{'beginDate'}->[0]);
        }
        if (defined $tabFilters->{'beginDate'}->[1])
        {
            $query .= '
AND DATE(tbl_f_contract.CONTRATDD) <= ' . $db->quote($tabFilters->{'beginDate'}->[1]);
        }
    }
    if (defined $tabFilters->{'endDate'})
    {
        if (ref($tabFilters->{'endDate'}) ne 'ARRAY')
        {
            $tabFilters->{'endDate'} = [undef, $tabFilters->{'endDate'}];
        }

        if (defined $tabFilters->{'endDate'}->[0])
        {
            $query .= '
AND DATE(tbl_f_contract.CONTRATDR) >= ' . $db->quote($tabFilters->{'endDate'}->[0]);
        }
        if (defined $tabFilters->{'endDate'}->[1])
        {
            $query .= '
AND DATE(tbl_f_contract.CONTRATDR) <= ' . $db->quote($tabFilters->{'endDate'}->[1]);
        }
    }

    # Filtre intervalle de durée entre la date de début et la date de fin
    if (defined $tabFilters->{'duration'})
    {
        if (ref($tabFilters->{'duration'}) eq 'ARRAY')
        {
            $query .= '
AND tbl_f_contract.CONTRATDR BETWEEN DATE_ADD(tbl_f_contract.CONTRATDD, INTERVAL ' . $tabFilters->{'duration'}->[0] . ') AND DATE_ADD(tbl_f_contract.CONTRATDD, INTERVAL ' . $tabFilters->{'duration'}->[1] . ')';
        }
        else
        {
            $query .= '
AND tbl_f_contract.CONTRATDR > DATE_ADD(tbl_f_contract.CONTRATDD, INTERVAL ' . $tabFilters->{'duration'} . ')';
        }
    }

    if (defined $tabFilters->{'isPrinted'})
    {
        $query .= '
AND tbl_f_contract.CONTRATIMP = ' . ($tabFilters->{'isPrinted'} ? 0 : -1);
    }
    if (defined $tabFilters->{'isFinalAllocation'})
    {
        $query .= '
AND tbl_f_contract.CONTRATMACHATTR = ' . ($tabFilters->{'isFinalAllocation'} ? -1 : 0);
    }
    if (defined $tabFilters->{'invoiceState'})
    {
        $query .= '
AND tbl_f_contract.ETATFACTURE IN (' . $db->quote($tabFilters->{'invoiceState'}) . ')';
    }
    if (defined $tabFilters->{'tariffFamilyId'})
    {
        $query .= '
AND tbl_f_model.FAMTARAUTO IN (' . $db->quote($tabFilters->{'tariffFamilyId'}) . ')';
    }
    # Contrat clôturé
    if (exists $tabFilters->{'isClosed'})
    {
        if ($tabFilters->{'isClosed'})
        {
            $query .= '
AND tbl_f_contract.CONTRATDATECLOTURE IS NOT NULL';
        }
        else
        {
            $query .= '
AND tbl_f_contract.CONTRATDATECLOTURE IS NULL';
        }
    }


    # Options
    if (!$tabOptions->{'isSortDisabled'})
    {
        if (defined $tabOptions->{'sorter'})
        {
            if ($tabOptions->{'sorter'} eq 'id' && defined $tabFilters->{'id'})
            {
                $query .= '
ORDER BY FIELD(tbl_f_contract.CONTRATAUTO, ' . &LOC::Util::join(', ', $tabFilters->{'id'}) . ');';
            }
        }
        elsif (defined $tabOptions->{'getLastContracts'})
        {
            $query .= '
ORDER BY tbl_f_contract.CONTRATDR DESC LIMIT ' . $tabOptions->{'getLastContracts'};
        }
        else
        {
            $query .= '
ORDER BY tbl_f_contract.CONTRATDR DESC';
        }
    }

    if (defined $tabOptions->{'limit'})
    {
        my $offset = 0;
        my $rowCount = 0;

        if (ref $tabOptions->{'limit'} eq 'HASH')
        {
            $offset = (defined $tabOptions->{'limit'}->{'offset'} ? $tabOptions->{'limit'}->{'offset'} : 0);
            $rowCount = (defined $tabOptions->{'limit'}->{'count'} ? $tabOptions->{'limit'}->{'count'} : 0);
        }
        else
        {
            $rowCount = $tabOptions->{'limit'};
        }

        $query .= '
LIMIT ' . $offset . ',' . $rowCount;
    }

    $query .= ';';
    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return 0;
    }

    # Récupération du total complet
    if (defined $tabOptions->{'limit'} && exists ($tabOptions->{'limit'}->{'&total'}))
    {
        $tabOptions->{'limit'}->{'&total'} = $db->fetchOne('SELECT FOUND_ROWS();');
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'} *= 1;
            $tabRow->{'linkedEstimateLine.id'} *= 1;
            $tabRow->{'duration'} *= 1;
            $tabRow->{'isActive'} *= 1;
            $tabRow->{'index'} *= 1;
            $tabRow->{'isLocked'} *= 1;
            $tabRow->{'additionalDaysUnbilled'} *= 1;
            $tabRow->{'deductedDaysUnbilled'} *= 1;
            $tabRow->{'additionalDaysBilled'} *= 1;
            $tabRow->{'deductedDaysBilled'} *= 1;
            $tabRow->{'isPrinted'} *= 1;
            $tabRow->{'isImperativeModel'} *= 1;
            $tabRow->{'hasDefaultAccessories'} *= 1;
            $tabRow->{'isOrderCompulsory'} *= 1;
            $tabRow->{'isOutOfPark'} *= 1;
            $tabRow->{'customer.id'} *= 1;
            $tabRow->{'customer.isProformaRequired'} *= 1;
            $tabRow->{'site.id'} *= 1;
            $tabRow->{'zone'} *= 1;
            $tabRow->{'isFullService'} *= 1;
            $tabRow->{'unitPrice'} *= 1;
            $tabRow->{'total'} *= 1;
            $tabRow->{'model.id'} *= 1;
            $tabRow->{'isJoinDoc'} *= 1;
            $tabRow->{'isWriteLocked'} *= 1;
            $tabRow->{'proformaFlags'} *= 1;

            # Calcul du prix par jour
            $tabRow->{'dailyPrice'} = &LOC::Common::Rent::getDailyPrice(
                $tabRow->{'amountType'},
                $tabRow->{'unitPrice'},
                $tabRow->{'duration'},
                $tabOptions->{'monthWorkingDays'}
            );

            # Client
            if ($flags & GETINFOS_CUSTOMER)
            {
                $tabRow->{'customer.isNewAccount'} = &LOC::Customer::isNewAccount($countryId, $tabRow->{'customer.code'},
                                                                                  $tabRow->{'customer.controlAccount'});
            }


            # Machine
            $tabRow->{'isFinalAllocation'} *= 1;
            $tabRow->{'machine.id'} *= 1;
            if ($flags & GETINFOS_MACHINE)
            {
                $tabRow->{'machine.model.id'} *= 1;
                $tabRow->{'machine.isSiteBlocked'} *= 1;
            }

            # Machine attribuée
            if ($flags & GETINFOS_EXT_MACHINE)
            {
                if ($tabRow->{'machine.id'})
                {
                    $tabRow->{'machine.tabInfos'} = &LOC::Machine::getInfos($countryId, $tabRow->{'machine.id'}, LOC::Machine::GETINFOS_ALL, {'level' => LOC::Machine::LEVEL_FAMILY});
                }
                else
                {
                    $tabRow->{'machine.tabInfos'} = {};
                }
            }

            # Machine souhaitée
            $tabRow->{'wishedMachine.id'} = ($tabRow->{'wishedMachine.id'} ? $tabRow->{'wishedMachine.id'} * 1 : undef);
            if ($flags & GETINFOS_EXT_WISHEDMACHINE)
            {
                if ($tabRow->{'wishedMachine.id'})
                {
                    $tabRow->{'wishedMachine.tabInfos'} = &LOC::Machine::getInfos($countryId, $tabRow->{'wishedMachine.id'}, LOC::Machine::GETINFOS_BASE);
                }
                else
                {
                    $tabRow->{'wishedMachine.tabInfos'} = undef;
                }
            }

            # Assurance
            if ($flags & GETINFOS_INSURANCE)
            {
                $tabRow->{'insuranceType.id'}     *= 1;
                $tabRow->{'insurance.rate'}       *= 1;
                $tabRow->{'insurance.amount'}     *= 1;
                $tabRow->{'insurance.flags'}      *= 1;
                $tabRow->{'insurance.mode'}       *= 1;
                $tabRow->{'insurance.isInvoiced'} *= 1;
                $tabRow->{'insurance.isIncluded'} *= 1;
                $tabRow->{'insurance.isCustomer'} *= 1;
            }
            if ($flags & GETINFOS_EXT_INSURANCETYPE)
            {
                $tabRow->{'insuranceType.tabInfos'} = &LOC::Insurance::Type::getInfos($countryId, $tabRow->{'insuranceType.id'});
            }

            # Gestion de recours
            if ($flags & GETINFOS_APPEAL)
            {
                $tabRow->{'appealType.id'}  = ($tabRow->{'appealType.id'} ne '' ? $tabRow->{'appealType.id'} * 1 : undef);
                $tabRow->{'appeal.value'}   = ($tabRow->{'appeal.value'} ne '' ? $tabRow->{'appeal.value'} * 1 : undef);
                $tabRow->{'appeal.amount'} *= 1;
            }
            if ($flags & GETINFOS_EXT_APPEALTYPE)
            {
                $tabRow->{'appealType.tabInfos'} = ($tabRow->{'appealType.id'} ? &LOC::Appeal::Type::getInfos($countryId, $tabRow->{'appealType.id'}) : undef);
            }

            # Ajout des informations du modèle de machine
            if ($flags & GETINFOS_EXT_MODEL)
            {
                $tabRow->{'model.tabInfos'} = &LOC::Model::getInfos($countryId, $tabRow->{'model.id'});
            }

            # Ajout du montant de la participation au recyclage si la participation au recyclage est facturée
            if ($flags & GETINFOS_RESIDUES)
            {
                $tabRow->{'residues.mode'}   *= 1;
                $tabRow->{'residues.value'}   = ($tabRow->{'residues.value'} ne '' ? $tabRow->{'residues.value'} * 1 : undef);
                $tabRow->{'residues.amount'} *= 1;
            }


            $tabRow->{'fuel.id'} *= 1;
            if ($flags & GETINFOS_FUEL)
            {
                $tabRow->{'fuel.quantity'}   = ($tabRow->{'fuel.id'} ? $tabRow->{'fuel.quantity'} * 1 : undef);
                $tabRow->{'fuel.amount'}    *= 1;
                $tabRow->{'fuel.unitPrice'} *= 1;
            }

            if ($flags & GETINFOS_CLEANING)
            {
                $tabRow->{'cleaning.type.id'} *= 1;
                $tabRow->{'cleaning.amount'}  *= 1;
            }

            if ($flags & GETINFOS_DURATIONS)
            {
                # Calcul de la durée de location
                $tabRow->{'realDuration'} = $tabRow->{'duration'} -
                                                ($tabRow->{'additionalDaysUnbilled'} + $tabRow->{'additionalDaysBilled'}) +
                                                ($tabRow->{'deductedDaysUnbilled'} + $tabRow->{'deductedDaysBilled'});

                # Est-ce que la durée de location a changé ?
                # Raison: Les jours fériés peuvent avoir changé
                my $realDuration = &LOC::Date::getRentDuration(
                                                                    $tabRow->{'beginDate'},
                                                                    $tabRow->{'endDate'},
                                                                    $tabRow->{'agency.id'});
                $tabRow->{'isRealDurationChanged'} = ($tabRow->{'realDuration'} == $realDuration ? 0 : 1);

                # Calcul de la durée calendaire
                $tabRow->{'calendarDuration'} = &LOC::Date::getDeltaDays(
                                                                $tabRow->{'beginDate'},
                                                                $tabRow->{'endDate'});
            }
            $tabRow->{'creator.id'} *= 1;
            $tabRow->{'negotiator.id'} = ($tabRow->{'negotiator.id'} ? $tabRow->{'negotiator.id'} * 1 : undef);


            # Ajout des informations sur les jours de remise en état
            if ($flags & GETINFOS_EXT_REPAIRDAYS)
            {
                # Récupération des informations
                my $tabRepairDays = &LOC::Contract::RepairDay::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                                       {'contractId' => $tabRow->{'id'}},
                                                                       undef,
                                                                       {'orders' => [
                                                                             {'field' => 'sheetId', 'dir' => 'DESC'},
                                                                             {'field' => 'id'}
                                                                         ]});

                # Réorganisation du tableau : regroupement des jours de remise en état par fiche de remise en état
                $tabRow->{'tabRepairDays'} = {};
                foreach my $tabRepairDayInfo (values %$tabRepairDays)
                {
                    push(@{$tabRow->{'tabRepairDays'}->{$tabRepairDayInfo->{'repairsheet.id'}}}, $tabRepairDayInfo);
                }
            }

            # Ajout des informations sur les services de remise en état
            if ($flags & GETINFOS_EXT_REPAIRSERVICES)
            {
                # Récupération des informations
                my $tabRepairServices = &LOC::Contract::RepairService::getList($countryId,
                                                                               LOC::Util::GETLIST_ASSOC,
                                                                               {'contractId' => $tabRow->{'id'}},
                                                                               undef,
                                                                               {'orders' => [
                                                                                     {'field' => 'sheetId', 'dir' => 'DESC'},
                                                                                     {'field' => 'id'}
                                                                                 ]});
                # Réorganisation du tableau : regroupement des services par fiche de remise en état
                $tabRow->{'tabRepairServices'} = {};
                foreach my $tabRepairServiceInfo (values %$tabRepairServices)
                {
                    push(@{$tabRow->{'tabRepairServices'}->{$tabRepairServiceInfo->{'repairsheet.id'}}}, $tabRepairServiceInfo);
                }
            }

            # Ajout des informations sur les services de remise en état
            if ($flags & GETINFOS_EXT_RENTSERVICES)
            {
                # Récupération des informations
                $tabRow->{'tabRentServices'} = &getRentServices($countryId, $tabRow->{'id'});
            }

            # Ajout des informations tarification location
            if ($flags & GETINFOS_TARIFFRENT)
            {
                $tabRow->{'rentTariff.baseAmount'}               *= 1;
                $tabRow->{'rentTariff.standardReductionPercent'} *= 1;
                $tabRow->{'rentTariff.maxiReductionPercent'}     *= 1;
                $tabRow->{'rentTariff.specialReductionPercent'}  *= 1;
            }
            if ($flags & GETINFOS_EXT_TARIFFRENT)
            {
                $tabRow->{'rentTariff.tabInfos'} = &LOC::Tariff::Rent::getInfos(
                                                       $countryId,
                                                       'contract',
                                                       $tabRow->{'id'},
                                                       LOC::Tariff::Rent::GETINFOS_ALL);
            }

            # Ajout des informations tarification transport
            if ($flags & GETINFOS_TARIFFTRSP)
            {
                $tabRow->{'transportTariff.baseAmount'}               *= 1;
                $tabRow->{'transportTariff.standardReductionPercent'} *= 1;
                $tabRow->{'transportTariff.specialReductionPercent'}  *= 1;
                $tabRow->{'transportTariff.deliveryAmount'}           *= 1;
                $tabRow->{'transportTariff.recoveryAmount'}           *= 1;
                $tabRow->{'transportTariff.isNoSaved'}                *= 1;
                $tabRow->{'transportTariff.zone'}                     *= 1;
                $tabRow->{'transportTariff.customer.id'}              *= 1;
            }
            if ($flags & GETINFOS_EXT_TARIFFTRSP)
            {
                # Récupération des informations sur l'estimation transport
                $tabRow->{'transportTariff.tabInfos'} = &LOC::Tariff::Transport::getInfos(
                                                            $countryId, 'contract',
                                                            $tabRow->{'id'},
                                                            LOC::Tariff::Transport::GETINFOS_ALL);
            }

            # Ajout des informations du chantier
            if ($flags & GETINFOS_EXT_SITE)
            {
                # Récupération des informations du chantier
                $tabRow->{'site.tabInfos'} = &LOC::Site::getInfos($countryId, $tabRow->{'site.id'});
            }

            # Est-il imprimable ?
            if ($flags & GETINFOS_PRINTABLE)
            {
                $tabRow->{'isPrintable'} = &isPrintable($countryId, $tabRow->{'id'}, $tabRow);
            }

            # Etat des boutons Arrêt, Remettre en cours, Facturer, Annuler et Imprimer
            if ($flags & GETINFOS_POSSIBILITIES)
            {
                $tabRow->{'isEndDateNear'} *= 1;

                # Récupération des informations sur les transports
                my $tabDeliveryInfos = &LOC::Transport::getDocumentDeliveryInfos($countryId, 'contract', $tabRow->{'id'});
                my $tabRecoveryInfos = &LOC::Transport::getDocumentRecoveryInfos($countryId, 'contract', $tabRow->{'id'});

                # Possibilités
                $tabRow->{'possibilities'} = {};
                &LOC::Util::setTreeValues($tabRow->{'possibilities'}, {
                    'equipment' => ['model'],
                    'actions'   => ['valid', 'stop', 'reactivate', 'invoice', 'restop', 'cancel']
                }, 0);

                &LOC::Util::setTreeValues($tabRow->{'possibilities'}, {
                    'duration' => ['setInvoiceableDay', 'setNonInvoiceableDay']
                }, 1);

                $tabRow->{'possibilities'}->{'actions'}->{'valid'} = 1;

                if ($tabRow->{'state.id'} eq STATE_PRECONTRACT &&
                    $tabDeliveryInfos->{'type'} ne LOC::Transport::TYPE_SITESTRANSFER &&
                    $tabRecoveryInfos->{'type'} ne LOC::Transport::TYPE_SITESTRANSFER)
                {
                    $tabRow->{'possibilities'}->{'equipment'}->{'model'} = 1;
                }

                # Possibilité de modifier la date de début :
                # seulement si la livraison n'est pas réalisée (contrat non annulé)
                if ($tabRow->{'state.id'} ne STATE_CANCELED && !$tabDeliveryInfos->{'unloading'}->{'isDone'})
                {
                    $tabRow->{'possibilities'}->{'duration'}->{'beginDate'} = 1;
                }

                # Peut-on modifier le contrat ?
                if ($tabRow->{'state.id'} ne STATE_CANCELED)
                {
                    # Etat des remises exceptionnelles de location et transport
                    my $rentSpRdcStatus = ($flags & GETINFOS_EXT_TARIFFRENT ? $tabRow->{'rentTariff.tabInfos'}->{'specialReductionStatus'} : $tabRow->{'rentTariff.specialReductionStatus'});
                    my $trspSpRdcStatus = ($flags & GETINFOS_EXT_TARIFFTRSP ? $tabRow->{'transportTariff.tabInfos'}->{'specialReductionStatus'} : $tabRow->{'transportTariff.specialReductionStatus'});
                    if ($tabRow->{'state.id'} eq STATE_CONTRACT)
                    {
                        # Peut-on arrêter le contrat ?
                        if ($tabDeliveryInfos->{'unloading'}->{'isDone'} &&
                            $tabRow->{'isEndDateNear'} &&
                            $rentSpRdcStatus ne LOC::Tariff::Rent::REDUCTIONSTATE_PENDING &&
                            $trspSpRdcStatus ne LOC::Tariff::Transport::REDUCTIONSTATE_PENDING &&
                            (
                                # Toutes les pro forma actives sont finalisées et aucune n'est expirée
                                ($tabRow->{'proformaFlags'} & LOC::Proforma::DOCFLAG_ALLACTIVEDFINALIZED &&
                                 !($tabRow->{'proformaFlags'} & LOC::Proforma::DOCFLAG_EXISTACTIVEDEXPIRED)) ||
                                # Le client est NON PAL et aucune pro forma active
                                (!($tabRow->{'customer.isProformaRequired'}) &&
                                 !($tabRow->{'proformaFlags'} & LOC::Proforma::DOCFLAG_EXISTACTIVED)) ||
                                # Le client est PAL
                                ($tabRow->{'customer.isProformaRequired'} &&
                                # Client PAL à la création du contrat et montant total du contrat à 0
                                (($tabRow->{'proformaFlags'} & LOC::Proforma::DOCFLAG_CT_ISCUSTOMERPFREQONCREATION &&
                                  $tabRow->{'total'} == 0) ||
                                # Client NON PAL à la création du contrat et montant total du contrat >= 0
                                (!($tabRow->{'proformaFlags'} & LOC::Proforma::DOCFLAG_CT_ISCUSTOMERPFREQONCREATION) &&
                                 $tabRow->{'total'} >= 0)) &&
                                # Aucune pro forma active
                                !($tabRow->{'proformaFlags'} & LOC::Proforma::DOCFLAG_EXISTACTIVED))
                           ))
                        {
                            $tabRow->{'possibilities'}->{'actions'}->{'stop'} = 1;
                        }
                    }
                    elsif ($tabRow->{'state.id'} eq STATE_STOPPED)
                    {
                        # Peut-on remettre en cours le contrat
                        if (!$tabRecoveryInfos->{'loading'}->{'isDone'} &&
                            ($tabRow->{'invoiceState'} eq INVOICESTATE_NONE ||
                             $tabRow->{'invoiceState'} eq INVOICESTATE_PARTIAL) &&
                            $tabRow->{'machine.tabInfos'}->{'state.id'} eq LOC::Machine::STATE_RECOVERY &&
                            $tabRow->{'machine.tabInfos'}->{'contract.id'} == $tabRow->{'id'} &&
                            $tabRow->{'machine.tabInfos'}->{'visibilityMode'} == LOC::Machine::VISIBILITYMODE_NONE &&
                            $tabRow->{'machine.tabInfos'}->{'model.id'} == $tabRow->{'model.id'} &&
                            &LOC::Model::isRentable($countryId, $tabRow->{'agency.id'}, $tabRow->{'model.id'}))
                        {
                            $tabRow->{'possibilities'}->{'actions'}->{'reactivate'} = 1;
                        }
                        # Peut-on mettre en facturation le contrat
                        if ($tabRecoveryInfos->{'loading'}->{'isDone'} &&
                            $rentSpRdcStatus ne LOC::Tariff::Rent::REDUCTIONSTATE_PENDING &&
                            $trspSpRdcStatus ne LOC::Tariff::Transport::REDUCTIONSTATE_PENDING &&
                            (
                                # Toutes les pro forma actives sont finalisées et aucune n'est expirée
                                ($tabRow->{'proformaFlags'} & LOC::Proforma::DOCFLAG_ALLACTIVEDFINALIZED &&
                                !($tabRow->{'proformaFlags'} & LOC::Proforma::DOCFLAG_EXISTACTIVEDEXPIRED)) ||
                                # Le client est NON PAL et aucune pro forma active
                                (!($tabRow->{'customer.isProformaRequired'}) &&
                                !($tabRow->{'proformaFlags'} & LOC::Proforma::DOCFLAG_EXISTACTIVED)) ||
                                # Le client est PAL
                                ($tabRow->{'customer.isProformaRequired'} &&
                                # Client PAL à la création du contrat et montant total du contrat à 0
                                (($tabRow->{'proformaFlags'} & LOC::Proforma::DOCFLAG_CT_ISCUSTOMERPFREQONCREATION &&
                                  $tabRow->{'total'} == 0) ||
                                # Client NON PAL à la création du contrat et montant total du contrat >= 0
                                (!($tabRow->{'proformaFlags'} & LOC::Proforma::DOCFLAG_CT_ISCUSTOMERPFREQONCREATION) &&
                                 $tabRow->{'total'} >= 0)) &&
                                # Aucune pro forma active
                                !($tabRow->{'proformaFlags'} & LOC::Proforma::DOCFLAG_EXISTACTIVED))
                            )
                           )
                        {
                            my $tabModelInfos = &LOC::Model::getInfos($countryId, $tabRow->{'model.id'});
                            my $contractEnergy = $tabModelInfos->{'machinesFamily.energy'};
                            my $tabFuelEnergies = &LOC::MachinesFamily::getFuelEnergiesList();
                            if (!&LOC::Util::in_array($contractEnergy, $tabFuelEnergies) || $tabRow->{'fuel.id'})
                            {
                                $tabRow->{'possibilities'}->{'actions'}->{'invoice'} = 1;
                            }
                        }
                    }
                    elsif ($tabRow->{'state.id'} eq STATE_BILLED)
                    {
                        # Peut-on remettre le contrat en arrêt
                        if ($tabRow->{'closureDate'} eq '')
                        {
                            $tabRow->{'possibilities'}->{'actions'}->{'restop'} = 1;
                        }
                    }

                    # Peut-on annuler le contrat
                    if ($tabRow->{'state.id'} eq STATE_PRECONTRACT)
                    {
                        $tabRow->{'possibilities'}->{'actions'}->{'cancel'} = 1;
                    }
                }
            }

            # Droits
            if ($flags & GETINFOS_RIGHTS)
            {
                # Informations sur l'utilisateur
                my $tabUserInfos = &LOC::Session::getUserInfos();
                if (defined $tabOptions->{'userId'} && $tabUserInfos->{'id'} != $tabOptions->{'userId'})
                {
                    $tabUserInfos = &LOC::User::getInfos($tabOptions->{'userId'});
                }
                $tabRow->{'tabRights'} = &_getRights($tabRow, $tabUserInfos);
            }

            # Date de valeur du contrat
            if ($flags & GETINFOS_VALUEDATE)
            {
                $tabRow->{'valueDate'} = &getValueDate($countryId, $tabRow->{'id'});
            }

            # Dates des actions (arrêt, mise en factu, etc.)
            if ($flags & GETINFOS_ACTIONSDATES)
            {
                my $stopDateQuery = '
SELECT
    log_date
FROM AUTH.h_log
WHERE log_etp_id = "' . LOC::Log::EventType::STOP . '"
AND log_schema = "' . $schemaName . '"
AND log_functionality = "' . LOC::Log::Functionality::CONTRACT . '"
AND log_functionalityid = "' . $tabRow->{'id'} . '"
ORDER BY log_id DESC
LIMIT 1';

                $tabRow->{'actionsDates'} = {
                    'stop' => $db->fetchOne($stopDateQuery)
                };
            }
        }
    }
    return (wantarray ? %$tab : $tab);
}

# Function: getAgencyId
# Récupérer l'agence du contrat de location
#
# Parameters:
# string  $countryId  - Pays
# int     $contractId - Identifiant du contrat de location
#
# Returns:
# string - Identifiant de l'agence
sub getAgencyId
{
    my ($countryId, $contractId) = @_;

    # Récupèration de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # La date de valeur du contrat est sa date de création par défaut
    my $query = '
SELECT
    AGAUTO
FROM CONTRAT tbl_f_contract
WHERE tbl_f_contract.CONTRATAUTO = ' . $contractId . ';';

    return $db->fetchOne($query);
}


# Function: getValueDate
# Récupérer la date de valeur du contrat de location
#
# Parameters:
# string $countryId  - Pays
# int    $contractId - Id du contrat de location
#
# Returns:
# string - Date de valeur
sub getValueDate
{
    my ($countryId, $contractId) = @_;

    # Récupèration de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # La date de valeur du contrat est sa date de création par défaut
    my $query = '
SELECT
    DATE(tbl_f_contract.CONTRATDATECREATION),
    tbl_f_estimatedetail.DEVISAUTO
FROM CONTRAT tbl_f_contract
LEFT JOIN DETAILSDEVIS tbl_f_estimatedetail
ON tbl_f_estimatedetail.DETAILSDEVISAUTO = tbl_f_contract.DETAILSDEVISAUTO
WHERE tbl_f_contract.CONTRATAUTO = ' . $contractId . '
AND tbl_f_contract.CONTRATOK = -1;';
    my ($valueDate, $estimateId) = @{$db->fetchRow($query, {}, LOC::Db::FETCH_NUM)};

    # Si le contrat provient d'une ligne de devis alors sa date de valeur est la date de valeur du devis
    if ($estimateId ne '')
    {
        $valueDate = &LOC::Estimate::Rent::getValueDate($countryId, $estimateId);
    }

    return $valueDate;
}


# Function: getRentServices
# Récupérer la liste des services de location et transport d'un contrat de location
#
# Contenu du hachage:
# - id          => id
# - currency.id => id de la devise
# - name        => libellé
# - code        => code
# - comment     => commentaire
# - type        => type d'imputation
# - amount      => montant
# - state.id    => code de l'état

#
# Parameters:
# string  $countryId  - Pays
# int     $contractId - id du contrat de location
# hashref $tabFilters - Liste des filtres (state (état))
#
# Returns:
# hash|hashref
sub getRentServices
{
    my ($countryId, $contractId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT
    rcs_id AS `id`,
    rsr_id AS `rentService.id`,
    rcs_sta_id AS `state.id`,
    ETLIBELLE AS `state.label`,
    rsr_name AS `rentService.name`,
    rcs_comment AS `comment`,
    rsr_code AS `rentService.code`,
    rsr_type AS `rentService.type`,
    rcs_amount AS `amount`,
    rcs_cur_id AS `currency.id`,
    rcs_date_invoicing AS `invoicingDate`
FROM f_rentcontract_rentservice
INNER JOIN p_rentservice
ON rsr_id = rcs_rsr_id
LEFT JOIN ETATTABLE p_state
ON p_state.ETCODE = rcs_sta_id
WHERE rcs_rct_id = ' . $contractId . '
ORDER BY rcs_id;';

    my $tab = $db->fetchAssoc($query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    foreach my $tabRow (values %$tab)
    {
        $tabRow->{'id'} *= 1;
        $tabRow->{'rentService.id'} *= 1;
        $tabRow->{'amount'} *= 1;
        $tabRow->{'isRemovable'} = ($tabRow->{'state.id'} ne LOC::Common::RentService::STATE_BILLED ? 1 : 0);
    }

    return (wantarray ? %$tab : $tab);
}


# Function: getActiveLineId
# Récupérer l'identifiant de la ligne de contrat active
#
# Parameters:
# string $countryId - Pays
# int    $id        - Identifiant d'une ligne du contrat
#
# Returns:
# int - Id du contrat
sub getActiveLineId
{
    my ($countryId, $id) = @_;

    return $id;
}


# Function: getFuelList
# Retourne la liste des carburants à saisir
#
# Parameters:
# string  $countryId   - Pays
# string  $agencyId    - Agence
# int     $format      - Format de retour de la liste
# hashref $tabFilters  - Filtres
#
# Returns:
# hash|hashref|0 - Liste des carburants à saisir
sub getFuelList
{
    my ($countryId, $agencyId, $format) = @_;

    # Liste des énergies à carburant
    my $tabFuelEnergies = &LOC::MachinesFamily::getFuelEnergiesList();
    if (@$tabFuelEnergies > 0)
    {
        # Récupération de la connexion à la base de données location
        my $db = &LOC::Db::getConnection('location', $countryId);
        #Connexion à la base de données GESTIONTARIF
        my $tariffRentSchemaName = 'GESTIONTARIF' . $countryId;

        my $query = '
SELECT
    tbl_f_contract.CONTRATAUTO AS `id`,
    tbl_f_contract.CONTRATCODE AS `code`,
    tbl_f_customer.CLAUTO AS `customer.id`,
    tbl_f_customer.CLSTE AS `customer.name`,
    tbl_f_machine.MAAUTO AS `machine.id`,
    tbl_p_machinesmodel.MOMADESIGNATION AS `model.label`,
    CONCAT_WS("", tbl_f_machine.MANOPARC_AUX, tbl_f_machine.MANOPARC) AS `machine.parkNumber`,
    tbl_f_contract.CONTRATDD AS `beginDate`,
    tbl_f_contract.CONTRATDR AS `endDate`,
    tbl_f_rentestim.CCCARBURANT AS `fuel`
FROM CONTRAT tbl_f_contract
LEFT JOIN AUTH.MODELEMACHINE tbl_p_machinesmodel
ON tbl_p_machinesmodel.MOMAAUTO = tbl_f_contract.MOMAAUTO
LEFT JOIN FAMILLEMACHINE tbl_p_machinefamily
ON tbl_p_machinefamily.FAMAAUTO = tbl_p_machinesmodel.FAMAAUTO
LEFT JOIN AUTH.MACHINE tbl_f_machine
ON tbl_f_machine.MAAUTO = tbl_f_contract.MAAUTO
LEFT JOIN CLIENT tbl_f_customer
ON tbl_f_customer.CLAUTO = tbl_f_contract.CLAUTO
LEFT JOIN ' . $tariffRentSchemaName . '.GC_CONTRATCADRE tbl_f_rentestim
ON tbl_f_rentestim.CCAUTO = tbl_f_customer.CCAUTO
LEFT JOIN frmwrk.p_agency
ON tbl_f_contract.AGAUTO = agc_id
WHERE tbl_f_contract.ETCODE = "' . LOC::Contract::Rent::STATE_STOPPED . '"
AND tbl_f_contract.CARBUAUTO = 0
AND tbl_f_contract.AGAUTO =  "' . $agencyId . '"
AND tbl_p_machinefamily.FAMAENERGIE IN ("' . join('", "', @$tabFuelEnergies) . '")
ORDER BY tbl_f_machine.MANOPARC';
        my $tab = $db->fetchAssoc($query, {}, 'id');
        if (!$tab)
        {
            return undef;
        }
        foreach my $tabRow (values %$tab)
        {
            my $tabFuelEstimate = &LOC::Proforma::getFuelEstimationInfos($countryId, 'contract', $tabRow->{'id'});
            $tabRow->{'fuelEstimate'} = $tabFuelEstimate->{'quantity'};
        }
        return (wantarray ? %$tab : $tab);
    }
    return (wantarray ? () : {});
}



# Function: getRentInvoicesList
# Récupérer la liste des factures de location
#
# Parameters:
# string $countryId    - Pays
# string $contractCode - Code du contrat
# bool   $numbered     - 1 : récupérer seulement les factures numérotées, 0 sinon
#
# Returns:
# hash|hashref - Liste des factures de location
sub getRentInvoicesList
{
    my ($countryId, $contractCode, $numbered) = @_;

    if (!$contractCode)
    {
        return undef;
    }
    if (!defined $numbered)
    {
        $numbered = 1;
    }

    my $tabParams = {'contractCode' => $contractCode};
    if ($numbered)
    {
        $tabParams->{'numbered'} = 1;
    }

    return &LOC::Invoice::Rent::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabParams);
}


# Function: getRepairInvoicesList
# Récupérer la liste des factures de remise en état
#
# Parameters:
# string $countryId    - Pays
# string $contractCode - Code du contrat
# bool   $numbered     - 1 : récupérer seulement les factures numérotées, 0 sinon
#
# Returns:
# hash|hashref - Liste des factures de remise en état
sub getRepairInvoicesList
{
    my ($countryId, $contractCode, $numbered) = @_;

    if (!$contractCode)
    {
        return undef;
    }
    if (!defined $numbered)
    {
        $numbered = 1;
    }

    my $tabParams = {'contractCode' => $contractCode};
    if ($numbered)
    {
        $tabParams->{'numbered'} = 1;
    }

    return &LOC::Invoice::Repair::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabParams);
}


# Function: getRights
# Récupérer les droits sur le contrat (par rapport à l'utilisateur)
#
# Parameters:
# string $countryId  - Pays
# string $agencyId   - Agence
# int    $id         - Id du contrat
# int    $userId     - Id de l'utilisateur
#
# Returns:
# hashref - Tableau des droits
sub getRights
{
    my ($countryId, $agencyId, $id, $userId) = @_;

    my $tabContractInfos;
    my $tabUserInfos;

    # Informations sur l'utilisateur
    $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Informations sur le contrat
    if ($id)
    {
        $tabContractInfos = &getInfos($countryId, $id);
    }
    else
    {
        $tabContractInfos = {
            'state.id'     => STATE_PRECONTRACT,
            'invoiceState' => INVOICESTATE_NONE,
            'agency.id'    => $agencyId
        };
    }

    return &_getRights($tabContractInfos, $tabUserInfos);
}


# Function: getHistory
# Récupérer l'historique du contrat
#
# Contenu d'une ligne du hashage :
# - id : identifiant de la ligne d'historique ;
# - eventType.code : code du type d'événement ;
# - eventType.label : libellé du type d'événement ;
# - user.id : identifiant de l'utilisateur ;
# - user.name : nom de l'utilisateur ;
# - user.firstName : prénom de l'utilisateur ;
# - user.fullName : nom complet de l'utilisateur ;
# - user.state.id : identifiant de l'état de l'utilisateur ;
# - datetime : date et heure de la trace ;
# - content : contenu de la trace.
#
# Parameters:
# string  $countryId  - Pays
# int     $contractId - Identifiant du contrat
# string  $localeId   - Identifiant de la locale pour les traductions
#
# Returns:
# array|arrayref|undef - Historique du contrat
sub getHistory
{
    my ($countryId, $contractId, $localeId) = @_;

    return &LOC::Log::getFunctionalityHistory($countryId, 'contract', $contractId, $localeId);
}


# Function: getStates
# Récupérer la liste des états de contrat
#
# Parameters:
# string  $countryId  - Pays
#
# Returns:
# hash|hashref -
sub getStates
{
    my ($countryId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT
    tbl_p_state.ETCODE,
    tbl_p_state.ETLIBELLE
FROM ETATTABLE tbl_p_state
WHERE tbl_p_state.ETCODE IN ("' . STATE_PRECONTRACT . '", "' . STATE_CONTRACT. '", ' .
                             '"' . STATE_STOPPED . '", "' . STATE_CANCELED . '", "' . STATE_BILLED . '")
ORDER BY tbl_p_state.ETLIBELLE';
    my $tab = $db->fetchPairs($query);
    if (!$tab)
    {
        return undef;
    }
    return (wantarray ? %$tab : $tab);
}


# Function: insert
# Création d'un contrat
#
# Parameters:
# string   $countryId  - Pays
# int      $customerId - Id du client
# hashref  $tabData    - Données à insérées
# int      $userId     - Utilisateur responsable de la création
# hashref  $tabErrors  - Tableau des erreurs
# hashref  $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool -
sub insert
{
    my ($countryId, $customerId, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'warning'} = [];
    $tabErrors->{'fatal'} = [];
    $tabErrors->{'modules'} = {
        'site' => {},
        'rentTariff' => {},
        'transportTariff' => {},
        'transport'       => {
            'fatal' => [],
            'modules' => {
                'delivery' => {},
                'recovery' => {}
            }
        },
        'proforma' => {},
        'documents' => [],
        'days' => {},
        'machine' => {},
        'startCode' => {}
    };


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = 'LOCATION' . ($countryId eq 'FR' ? '' : $countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'insert', $_[0], __FILE__, '');

        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }

        return 0;
    };


    # Récupération des droits
    my $tabRights = &getRights($countryId, $tabData->{'agency.id'}, undef, $userId);


    # Vérifie que l'utilisateur ait bien le droit de créer un contrat
    if (!&_hasRight($tabRights->{'actions'}, 'valid'))
    {
        return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
    }

    # Paramètres
    # ----------
    my $linkedEstimateLineId = (exists $tabData->{'linkedEstimateLine.id'} ? $tabData->{'linkedEstimateLine.id'} : 0);

    # Vérification de la ligne de devis à passer en contrat (si il y a)
    my $tabEstimateLineInfos = undef;
    if ($linkedEstimateLineId != 0)
    {
        # Récupération des informations sur la ligne de devis
        $tabEstimateLineInfos = &LOC::Estimate::Rent::getLineInfos($countryId, $linkedEstimateLineId, LOC::Estimate::Rent::GETLINEINFOS_CONVERTIBLE);

        if (!$tabEstimateLineInfos->{'isConvertible'})
        {
            return &$rollBackAndError(__LINE__, 0x0101);
        }
    }

    my $agencyId = (exists $tabData->{'agency.id'} ? $tabData->{'agency.id'} : '');
    my $negotiatorId = (defined $tabData->{'negotiator.id'} ? $tabData->{'negotiator.id'} : undef);
    my $comments = (exists $tabData->{'comments'} ? &LOC::Util::trim($tabData->{'comments'}) : '');
    my $tracking = (exists $tabData->{'tracking'} ? &LOC::Util::trim($tabData->{'tracking'}) : '');
    my $isFullService = (exists $tabData->{'isFullService'} ? ($tabData->{'isFullService'} ? 1 : 0) : 0);
    my $zone = (exists $tabData->{'tabSite'}->{'zone'} ? $tabData->{'tabSite'}->{'zone'} : 0);
    my $orderNo = (exists $tabData->{'tabCustomer'}->{'orderNo'} ? &LOC::Util::trim($tabData->{'tabCustomer'}->{'orderNo'}) : '');
    my $orderContactFullName  = (exists $tabData->{'tabCustomer'}->{'orderContact.fullName'} ? &LOC::Util::trim($tabData->{'tabCustomer'}->{'orderContact.fullName'}) : undef);
    my $orderContactTelephone = (exists $tabData->{'tabCustomer'}->{'orderContact.telephone'} ? $tabData->{'tabCustomer'}->{'orderContact.telephone'} : undef);
    my $orderContactEmail  = (exists $tabData->{'tabCustomer'}->{'orderContact.email'} ? &LOC::Util::trim($tabData->{'tabCustomer'}->{'orderContact.email'}) : undef);
    my $stateId = STATE_PRECONTRACT;


    # Onglet "Matériel"
    # -----------------
    my $modelId = (exists $tabData->{'model.id'} ? $tabData->{'model.id'} : 0);
    my $isImperativeModel = (exists $tabData->{'isImperativeModel'} ? $tabData->{'isImperativeModel'} : 0);
    my $isJoinDoc = (exists $tabData->{'isJoinDoc'} ? $tabData->{'isJoinDoc'} : 0);
    my $wishedMachineId = (exists $tabData->{'wishedMachine.id'} ? $tabData->{'wishedMachine.id'} : undef);


    # Onglet "Durée"
    # --------------
    my $beginDate   = (exists $tabData->{'beginDate'} ? $tabData->{'beginDate'} : '');
    my $endDate     = (exists $tabData->{'endDate'} ? $tabData->{'endDate'} : '');
    if ($beginDate eq '' || !&LOC::Date::checkMySQLDate($beginDate))
    {
        $beginDate = '';
    }
    if ($endDate eq '' || !&LOC::Date::checkMySQLDate($endDate))
    {
        $endDate = '';
    }
    # - Jours plus et jours moins
    my $additionalDaysComment  = (exists $tabData->{'additionalDaysComment'} ? $tabData->{'additionalDaysComment'} : undef);
    my $deductedDaysComment  = (exists $tabData->{'deductedDaysComment'} ? $tabData->{'deductedDaysComment'} : undef);

    my $tabDaysActions  = $tabData->{'tabDaysActions'};

    # - Calcul des jours ouvrés de location
    my $realDuration = &LOC::Date::getRentDuration($beginDate, $endDate, $agencyId);
    # - Calcul de la durée de location
    my $tabDiffDaysDuration = &LOC::Common::Rent::calculateDiffDaysByActions($countryId, $tabDaysActions);
    my $additionalDaysUnbilled = $tabDiffDaysDuration->{&LOC::Day::TYPE_ADDITIONAL};
    my $deductedDaysUnbilled   = $tabDiffDaysDuration->{&LOC::Day::TYPE_DEDUCTED};

    my $duration = $realDuration + ($additionalDaysUnbilled - $deductedDaysUnbilled);


    # Onglet "Autres services"
    # ------------------------
    my $fuelQuantity = (exists $tabData->{'fuelQuantity'} ? $tabData->{'fuelQuantity'} : undef);
    my $cleaningTypeId = (exists $tabData->{'tabCleaning'}->{'typeId'} ? $tabData->{'tabCleaning'}->{'typeId'} : 0);
    my $cleaningAmount = (exists $tabData->{'tabCleaning'}->{'amount'} ? $tabData->{'tabCleaning'}->{'amount'} : 0);
    # - Participation au recyclage
    my $residuesMode       = (exists $tabData->{'residues'}->{'mode'} ? $tabData->{'residues'}->{'mode'} : LOC::Common::RESIDUES_MODE_NONE);
    # - Récupération du montant minimum des services location et transport pour l'utilisateur connecté
    my $rentServiceMinAmount = &LOC::Characteristic::getAgencyValueByCode('MTMINSRV', $agencyId);
    my $rentServiceNoLimitUserIdList = &LOC::Characteristic::getAgencyValueByCode('USRNOMAXREMSRV', $agencyId);
    my @rentServiceNoLimitUserId = split(',', $rentServiceNoLimitUserIdList);
    # - Si l'utilisateur courant n'a pas de limite de services location et transport
    if (grep {$_ eq $userId} @rentServiceNoLimitUserId)
    {
        $rentServiceMinAmount = undef;
    }
    # - Services de location et transport
    my $tabRentServices = $tabData->{'tabRentServices'};
    # - Assurance
    my $defaultInsuranceTypeId = &LOC::Insurance::Type::getDefaultId($countryId);
    my $insuranceTypeId = (exists $tabData->{'tabInsurance'}->{'typeId'} ? $tabData->{'tabInsurance'}->{'typeId'} : $defaultInsuranceTypeId);
    my $insuranceRate    = (exists $tabData->{'tabInsurance'}->{'rate'} ? $tabData->{'tabInsurance'}->{'rate'} : 0);
    # -- Récup infos sur le type d'assurance
    my $tabInsuranceTypeInfos = &LOC::Insurance::Type::getInfos($countryId, $insuranceTypeId);
    # -- Gestion de recours
    my $defaultAppealTypeId = &LOC::Appeal::Type::getDefaultId($countryId);
    my $appealTypeId = (exists $tabData->{'tabAppeal'}->{'typeId'} ? $tabData->{'tabAppeal'}->{'typeId'} : undef);

    # Incrémente l'alerte "Pré-contrats effectués"
    &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::PRECONTRACTS, +1);


    # Vérification des données
    # ------------------------
    if (!$negotiatorId)
    {
        return &$rollBackAndError(__LINE__, 0x0010);
    }

    if ($customerId == 0)
    {
        return &$rollBackAndError(__LINE__, 0x0104);
    }
    # - Verifie que l'agence est valide
    if ($agencyId eq '')
    {
        return &$rollBackAndError(__LINE__, 0x0102);
    }
    # - Vérifie que l'horaire de livraison est renseigné
    if (!$tabData->{'tabSite'}->{'deliveryHour'} && !$tabData->{'tabSite'}->{'deliveryTimeSlot.id'})
    {
        return &$rollBackAndError(__LINE__, 0x0113);
    }
    # - Vérifie que le mail et le tél du contact commande sont renseignés
    if (!$orderContactEmail || !$orderContactTelephone)
    {
         push(@{$tabErrors->{'warning'}}, 0x0128);
    }
    # - Vérifie que le mail du contact commande est valide
    if ($orderContactEmail && !&LOC::Util::isValidEmail($orderContactEmail))
    {
        return &$rollBackAndError(__LINE__, 0x0127);
    }

    # - Vérifie que le modèle de machine est renseigné
    if ($modelId == 0)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0115);
    }
    else
    {
        # - Vérifie que le modèle de machine est réservable
        if (!&LOC::Model::isRentable($countryId, $agencyId, $modelId))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0111);
        }
        if ($beginDate eq '' || $endDate eq '')
        {
            push(@{$tabErrors->{'fatal'}}, 0x0109);
        }
        else
        {
            if ($duration eq '' || $duration < 0)
            {
                # Durée invalide
                push(@{$tabErrors->{'fatal'}}, 0x0116);
            }
            if ($endDate lt $beginDate)
            {
                # La date de fin est inférieure à la date de début
                push(@{$tabErrors->{'fatal'}}, 0x0117);
            }
            my %tabCreationDate = &LOC::Date::getHashDate();
            my ($tYear, $tMonth, $tDay) = &Date::Calc::Add_Delta_YM($tabCreationDate{'y'},
                                                                    $tabCreationDate{'m'},
                                                                    $tabCreationDate{'d'},
                                                                    0,
                                                                    -1);
            my $creationDateLess1Month = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE,
                                                                 ('y' => $tYear, 'm' => $tMonth, 'd' => $tDay));
            if ($beginDate le $creationDateLess1Month)
            {
                # La date de début ne doit pas être inférieure à 1 mois
                push(@{$tabErrors->{'fatal'}}, 0x0121);
            }
        }

        # Vérification du model de la machine souhaitée
        if ($wishedMachineId)
        {
            my $tabWishedMachineInfos = &LOC::Machine::getInfos($countryId, $wishedMachineId, LOC::Machine::GETINFOS_BASE);
            if (!$tabWishedMachineInfos)
            {
                return &$rollBackAndError(__LINE__, ERROR_FATAL);
            }
            if ($modelId != $tabWishedMachineInfos->{'model.id'})
            {
                push(@{$tabErrors->{'fatal'}}, 0x0114);
            }
        }

        # Mise à jour des stocks...
        &LOC::EndUpdates::updateModelStock($tabEndUpds, $agencyId, $modelId, 'nbBooked', +1);
    }

    # Si erreur de vérification de données alors on arrête !!
    if (@{$tabErrors->{'fatal'}} > 0)
    {
        return &$rollBackAndError(__LINE__);
    }


    # Paramètres pour la mise à jour du tarif de location
    my $tabRentTariffDatas = $tabData->{'tabRentTariff'};
    # Optimisations location
    $tabRentTariffDatas->{'contract.customer.id'} = $customerId;
    $tabRentTariffDatas->{'contract.agency.id'}   = $agencyId;
    $tabRentTariffDatas->{'contract.model.id'}    = $modelId;
    $tabRentTariffDatas->{'contract.linkedEstimateLine.id'} = $linkedEstimateLineId;
    $tabRentTariffDatas->{'editMode'}             = 'create';
    # Vérification du nombre de jours (en cas)
    if ($tabRentTariffDatas->{'days'} != $duration)
    {
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Paramètres pour la mise à jour du tarif transport
    my $tabTrspTariffDatas = $tabData->{'tabTransport'}->{'tabTariff'};
    # Optimisations transport
    $tabTrspTariffDatas->{'contract.customer.id'} = $customerId;
    $tabTrspTariffDatas->{'contract.agency.id'}   = $agencyId;
    $tabTrspTariffDatas->{'contract.zone'}        = $zone;
    $tabTrspTariffDatas->{'contract.linkedEstimateLine.id'} = $linkedEstimateLineId;
    $tabTrspTariffDatas->{'editMode'}             = 'create';

    # Paramètres pour l'enregistrement de la proforma
    my $tabProformaAction = $tabData->{'tabProformaAction'};
    if ($tabProformaAction)
    {
        my $type = $tabProformaAction->{'type'};
        # - Vérification des droits pour la proforma
        if (!&_hasRight($tabRights->{'proforma'}, $type))
        {
            return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
        }
    }

    # Paramètres pour l'enregistrement des documents
    my $tabDocumentsActions = $tabData->{'tabDocumentsActions'};

    # Informations sur le chantier
    my $tabSiteData = $tabData->{'tabSite'};

    # Récupération des informations du client
    my $tabCustomerInfos = &LOC::Customer::getInfos($countryId, $customerId);

    # Vérifie que le client n'est pas bloqué au contrat
    # bloqué si :
    #       - bloqué au contrat
    #       - pas en paiement avant livraison
    #       - pas de création de pro forma
    #       - Si c'est un passage de ligne de devis en contrat, il ne faut pas que le devis ait des pro formas actives

    if ((!$tabCustomerInfos->{'isTempUnlock'} && $tabCustomerInfos->{'lockLevel'} > 0) &&
        $tabCustomerInfos->{'isProformaRequired'} * 1 == 0 &&
        (!$tabProformaAction || $tabProformaAction->{'type'} ne 'add') &&
        ($linkedEstimateLineId == 0 || !($tabEstimateLineInfos->{'proformaFlags'} & &LOC::Proforma::DOCFLAG_EXISTACTIVED)))
    {
        return &$rollBackAndError(__LINE__, 0x0105);
    }


    # Vérification de l'assurance des contrats en FullService
    # - l'assurance doit être assurance incluse
    # - le type et le taux d'assurance ne sont pas modifiables
    # Vérification de l'assurance des contrats non FullService
    # -- le type et le taux négocié ne sont pas modifiables pour les clients en assurance facturée négociée (taux <> taux par défaut)
    # -- le taux par défaut est le seul taux possible
    if ($isFullService)
    {
        # Type non modifiable
        if (!$tabInsuranceTypeInfos->{'isIncluded'})
        {
            return &$rollBackAndError(__LINE__, 0x0124);
        }
        # Taux non modifiable
        if ($insuranceRate != 0)
        {
            return &$rollBackAndError(__LINE__, 0x0125);
        }
    }
    else
    {
        # Récupération des informations du pays
        my $tabCountryInfos = &LOC::Country::getInfos($countryId);

        # Vérification : le type et le taux ne doivent pas changer pour les clients en assurance facturée négociée
        if ($tabCustomerInfos->{'insurance.isInvoiced'} && $tabCustomerInfos->{'insurance.rate'} != $tabCountryInfos->{'insurance.defaultRate'})
        {
            # Type non modifiable
            if ($insuranceTypeId != $tabCustomerInfos->{'insuranceType.id'})
            {
                return &$rollBackAndError(__LINE__, 0x0124);
            }
            # Taux non modifiable
            if ($insuranceRate ne $tabCustomerInfos->{'insurance.rate'})
            {
                return &$rollBackAndError(__LINE__, 0x0125);
            }
        }
        # Vérification : le taux par défaut est le seul taux possible
        elsif ($tabInsuranceTypeInfos->{'isInvoiced'} && $insuranceRate != $tabCountryInfos->{'insurance.defaultRate'})
        {
            # Taux non modifiable
            return &$rollBackAndError(__LINE__, 0x0125);
        }
    }

    # Vérification de la gestion de recours
    # - Gestion de recours sur un type d'assurance non éligible
    if ($tabInsuranceTypeInfos)
    {
        # - Gestion de recours sur un type d'assurance non éligible
        if (!$tabInsuranceTypeInfos->{'isAppealActivated'} && defined $appealTypeId)
        {
            return &$rollBackAndError(__LINE__, 0x0129);
        }

        # - Pas de gestion de recours définie sur un type d'assurance éligible
        if ($tabInsuranceTypeInfos->{'isAppealActivated'} && !defined $appealTypeId)
        {
            return &$rollBackAndError(__LINE__, 0x0129);
        }
    }

    # Si le n° de commande est obligatoire sur la facture pour ce client et qu'il n'est pas saisi
    if ($tabCustomerInfos->{'isOrderNoOnInvoice'} && $orderNo eq '')
    {
        # Incrémente l'alerte "N° de commande à fournir [agence]"
        &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ORDERNUMTOPROVIDE_AGENCY, +1);
        # Incrémente l'alerte "N° de commande à fournir [siège]"
        &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ORDERNUMTOPROVIDE_ALL, +1);
    }

    # Si le bon de commande est nécessaire
    if ($tabCustomerInfos->{'isOrderRequired'})
    {
        # Incrémente l'alerte "BC client à fournir"
        &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ORDERCOMPULSORYTOPROVIDE, +1);
        # Incrémente l'alerte "BC client à réceptionner"
        &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ORDERCOMPULSORYTOTORECEIVE, +1);
    }


    # Indique si la tarification transport est activé ou non ?
    my $isTrspTarification = &LOC::Characteristic::getAgencyValueByCode('TRSPTARIF', $agencyId);


    # Mise à jour des champs CLVERIFIER et CLNONVALIDE ! et de l'email du client
    my $tabUpdates = {
        'CLNONVALIDE' => 0
    };
    my $isSendCreateAccountMail = 0;
    my $isCustomerVerified = $tabCustomerInfos->{'isVerified'};
    if ((!$isCustomerVerified && $tabCustomerInfos->{'prospectSource'} eq 'estimate') ||
        $tabCustomerInfos->{'code'} eq '')
    {
        $tabUpdates->{'CLVERIFIER'} = -1;
        $isCustomerVerified = 0;
        $isSendCreateAccountMail = 1;

        # Incrémente l'alerte "Client(s) à vérifier"
        &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::CUSTOMERSTOCHECK, +1);
    }

    # Clients inactifs à réactiver
    my $regAccount = &LOC::Characteristic::getCountryValueByCode('OUVCPTE', $countryId);
    my $isSendReactivateAccountMail = 0;
    if ($tabCustomerInfos->{'accountingCode'}  =~ /^$regAccount$/)
    {
        $isSendReactivateAccountMail  = 1;
    }

    if ($db->update('CLIENT', $tabUpdates, {'CLAUTO*' => $tabCustomerInfos->{'id'}}, $schemaName) == -1)
    {
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }


    # Mise à jour du chantier
    $tabSiteData->{'documentType'} = 'contract';
    $tabSiteData->{'documentId'}   = undef;

    $tabSiteData->{'contract.id'}  = 0;
    my $tabSiteErrors = $tabErrors->{'modules'}->{'site'};
    my $siteId = &LOC::Site::insert($countryId, $tabSiteData, $userId, $tabSiteErrors, undef);
    if (!$siteId)
    {
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabSiteErrors->{'fatal'}))
        {
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        }
        return &$rollBackAndError(__LINE__, ERROR_MODULE_SITE);
    }
    if (@{$tabSiteErrors->{'warning'}} > 0)
    {
        push(@{$tabErrors->{'warning'}}, ERROR_MODULE_SITE);
    }

    # Vérification de la zone
    if ($isTrspTarification && ($zone < 1 || $zone > 6))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0103);
    }


    # Enregistrement dans la table MONTANT
    $tabUpdates = {
        'PACODE'         => $countryId,
        'MOHTFRS'        => 0,
        'MOTTCFR'        => 0,
        'MOHTEU'         => 0,
        'MOTTCEU'        => 0,
        'MOASSFR'        => 0,
        'MOASSEU'        => 0,
        'MOPXJOURFR'     => 0,
        'MOPXJOUREU'     => 0,
        'MOTARIFMSFR'    => 0,
        'MOTARIFMSEU'    => 0,
        'MOTRANSPORTDFR' => 0,
        'MOTRANSPORTDEU' => 0,
        'MOTRANSPORTRFR' => 0,
        'MOTRANSPORTREU' => 0,
        'MOFORFAITFR'    => 0,
        'MOFORFAITEU'    => 0,
        'MOTARIFBASE'    => 0,
        'MOTARIFBASEEU'  => 0,
        'MOREMISE'       => 0,
        'MOREMISEEX'     => 0,
        'MOREMISEMAX'    => 0,
        'MOREMISEOK'     => 0,
        'MODECHETS'      => 0,
        'MODECHETSEU'    => 0
    };
    my $amountId = $db->insert('MONTANT', $tabUpdates, $schemaName);
    if ($amountId == -1)
    {
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }
    $amountId *= 1;

    # Calcul des flags pro forma
    my $proformaFlags = 0;

    if ($tabCustomerInfos->{'isProformaRequired'})
    {
        $proformaFlags |= LOC::Proforma::DOCFLAG_CT_ISCUSTOMERPFREQONCREATION;
    }

    # Enregistrement dans la base de données (Table CONTRAT)
    my $isContractLocked = ($isCustomerVerified ? 0 : 1);
    $tabUpdates = {
        # Pavé "Informations générales"
        'AGAUTO'             => $agencyId,
        'PEAUTO'             => $userId,
        'CONTRATFULLSERVICE' => ($isFullService ? -1 : 0),
        'PEAUTO_INTERLOC'    => $negotiatorId,

        # Pavé "Client"
        'CLAUTO'               => $customerId,
        'CONTRATNUMCOMM'       => substr($orderNo, 0, 25),
        'CONTRATCONTACTCOMM'   => $orderContactFullName,
        'CONTRATTELEPHONECOMM' => $orderContactTelephone,
        'CONTRATEMAILCOMM'     => $orderContactEmail,

        # Pavé "Chantier"
        'CHAUTO'             => $siteId,
        'ZONE'               => $zone,

        # Onglet "Matériel"
        'MOMAAUTO'            => $modelId,
        'FAMAAUTO*'           => '(SELECT FAMAAUTO FROM AUTH.MODELEMACHINE WHERE MOMAAUTO = ' . $modelId . ')',
        'CONTRATMODIMP'       => ($isImperativeModel ? -1 : 0),
        'CONTRATJOINDOCUMENT' => ($isJoinDoc ? -1 : 0),
        'MAAUTO_SOUHAITE'     => ($wishedMachineId ? $wishedMachineId : undef),

        # Onglet "Durée"
        'CONTRATJOURSPLUS'   => $additionalDaysUnbilled,
        'CONTRATJOURSMOINS'  => $deductedDaysUnbilled,
        'CONTRATJPCOMM'      => $additionalDaysComment,
        'CONTRATJMCOMM'      => $deductedDaysComment,
        'CONTRATDUREE'       => $duration,
        'CONTRATDD'          => $beginDate,
        'CONTRATDR'          => $endDate,

        # Onglet "Autres services"
        'CONTRATTYPEASSURANCE' => $insuranceTypeId,
        'CONTRATPASSURANCE'    => ($tabInsuranceTypeInfos->{'isInvoiced'} ? $insuranceRate : 0),
        'CONTRATDECHETSFACT'   => 0,

        # Pavé "Infos complémentaires"
        'CONTRATCOMMENTAIRE' => ($comments eq '' ? undef : $comments),
        'CONTRATNOTE'        => ($tracking eq '' ? undef : $tracking),


        # Infos internes
        'MOAUTO'               => $amountId,
        'CONTRATVERIFIER'      => ($isContractLocked ? -1 : 0),
        'CONTRATOK'            => -1,
        'CONTRATINDICE'        => 1, # Indice de modification du contrat
        'ETATFACTURE'          => INVOICESTATE_NONE,
        'ANCIENETATFACTURE'    => INVOICESTATE_NONE,
        'ETCODE'               => $stateId,
        'LIETCONTRATAUTO'      => substr($stateId, -2) * 1,
        'CONTRATDATECREATION*' => 'Now()',
        'CONTRATPROFORMAOK'    => $proformaFlags,

        # Id de la ligne de devis associée
        'DETAILSDEVISAUTO'   => ($linkedEstimateLineId != 0 ? $linkedEstimateLineId : undef)
    };

    my $contractId = $db->insert('CONTRAT', $tabUpdates, $schemaName);
    if ($contractId == -1)
    {
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }
    $contractId *= 1;

    # Récupération du typage du modèle de machines
    my $tabModelInfos = &LOC::Model::getInfos($countryId, $modelId);

    # Typage du contrat
    my $type = 0;
    if ($isFullService)
    {
        $type = $type | LOC::Contract::TYPE_FULLSERVICE;
    }
    if (&LOC::Util::in_array(LOC::Model::TYPE_SUBRENT, $tabModelInfos->{'tabTypes'}))
    {
        $type = $type | LOC::Contract::TYPE_SUBRENT;
    }
    if (&LOC::Util::in_array(LOC::Model::TYPE_CATEGORYB, $tabModelInfos->{'tabTypes'}))
    {
        $type = $type | LOC::Contract::TYPE_CATEGORYB;
    }
    if (&LOC::Util::in_array(LOC::Model::TYPE_SALE, $tabModelInfos->{'tabTypes'}))
    {
        $type = $type | LOC::Contract::TYPE_SALE;
    }

    # Ajout du code contrat associé
    my $contractCode = &generateCode($contractId, $agencyId, $type);
    if ($db->update('CONTRAT', {'CONTRATCODE' => $contractCode}, {'CONTRATAUTO' => $contractId}, $schemaName) == -1)
    {
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }



    # Enregistrement du tarif de location
    my $tabRentTariffErrors = $tabErrors->{'modules'}->{'rentTariff'};
    if (!&LOC::Tariff::Rent::save($countryId, 'contract', $contractId, $tabRentTariffDatas, $userId, $tabRentTariffErrors, undef))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_TARIFFRENT);
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabRentTariffErrors->{'fatal'}))
        {
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Enregistrement de l'estimation transport
    my $tabTrspTariffErrors = $tabErrors->{'modules'}->{'transportTariff'};
    if (!&LOC::Tariff::Transport::save($countryId, 'contract', $contractId, $tabTrspTariffDatas, $userId, $tabTrspTariffErrors, undef))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_TARIFFTRSP);
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabTrspTariffErrors->{'fatal'}))
        {
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Mise à jour des jours+/jours-
    if (@{$tabErrors->{'fatal'}} == 0)
    {
        my $tabDaysErrors = $tabErrors->{'modules'}->{'days'};
        if ($tabDaysActions && !&_execDaysActions($countryId, $contractId, $tabDaysActions, $userId, {}, $tabDaysErrors, $tabEndUpds))
        {
            push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_DAYS);

            # Erreur fatale ?
            if (&LOC::Util::in_array(0x0001, $tabDaysErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            }
        }
    }


    # Mise à jour du carburant
    if (@{$tabErrors->{'fatal'}} == 0)
    {
        &_updateFuel($db, $countryId, $contractId, $fuelQuantity,
                     &_hasRight($tabRights->{'services'}, 'fuel'), $userId, $tabErrors, undef);
    }

    # Mise à jour du nettoyage
    if (@{$tabErrors->{'fatal'}} == 0)
    {
        &_updateCleaning($db, $countryId, $contractId, $cleaningTypeId, $cleaningAmount, 1,
                         &_hasRight($tabRights->{'services'}, 'cleaning'), $userId, $tabErrors, undef);
    }

    # Mise à jour de la participation au recyclage
    if (@{$tabErrors->{'fatal'}} == 0)
    {
        &_updateResidues($db, $countryId, $contractId, $residuesMode, 1,
                         &_hasRight($tabRights->{'services'}, 'residues'), $userId, $tabErrors, undef);
    }
    # Mise à jour des services de location et de transport
    if (@{$tabErrors->{'fatal'}} == 0)
    {
        &_updateRentServices($db, $countryId, $contractId, $tabRentServices, $rentServiceMinAmount,
                             &_hasRight($tabRights->{'services'}, 'addRentServices'),
                             &_hasRight($tabRights->{'services'}, 'editRentServices'), $userId, $tabErrors, undef);
    }

    # Mise à jour de la gestion de recours
    if (@{$tabErrors->{'fatal'}} == 0)
    {
        &_updateAppeal($db, $countryId, $contractId, $appealTypeId,
                         &_hasRight($tabRights->{'services'}, 'appeal'), $userId, $tabErrors, undef);
    }

    # Mise à jour du montant total et de l'assurance
    if (@{$tabErrors->{'fatal'}} == 0 && !&_updateAmounts($db, $countryId, $contractId))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
    }


    # Incrémente l'alerte "Machines à attribuer"
    &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::MACHINESTOALLOCATE, +1);


    # Passage de la ligne de devis en contrat
    if (@{$tabErrors->{'fatal'}} == 0 && $linkedEstimateLineId != 0)
    {
        # Mise à jour du devis et de la ligne
        my $newEstimateStateId = LOC::Estimate::Rent::STATE_CONTRACT;

        # Le devis passe à l'état "En contrat"
        my $query = '
UPDATE ' . $schemaName . '.DETAILSDEVIS tbl_f_estimatedetail
INNER JOIN ' . $schemaName . '.DEVIS tbl_f_estimate
ON tbl_f_estimate.DEVISAUTO = tbl_f_estimatedetail.DEVISAUTO
SET tbl_f_estimatedetail.DETAILSDEVISOK = -1,
    tbl_f_estimate.ETCODE = "' . $newEstimateStateId . '"
WHERE tbl_f_estimatedetail.DETAILSDEVISAUTO = ' . $linkedEstimateLineId . ';';
        my $resultObj = $db->query($query);
        if (!$resultObj)
        {
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        }
        else
        {
            $resultObj->close();
        }

        # Mise à jour des alertes
        if ($tabEstimateLineInfos->{'isAccepted'})
        {
            # Décrémentation de l'alerte des lignes de devis acceptées
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ACCESPTEDESTIMATELINES, -1);
        }
        elsif ($tabEstimateLineInfos->{'isFollowed'})
        {
            # Décrémentation de l'alerte des lignes de devis à rappeler
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ESTIMATELINESTOCALL, -1);
        }
    }


    # Mises à jour pour la pro forma
    #-------------------------------
    my $tabProformaErrors = $tabErrors->{'modules'}->{'proforma'};
    # Récupération des proformas actives venant du devis
    if (@{$tabErrors->{'fatal'}} == 0 && $linkedEstimateLineId != 0)
    {
        if (!&LOC::Proforma::affectToContract($countryId, $linkedEstimateLineId, $contractId, $userId, $tabProformaErrors, $tabEndUpds))
        {
            push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_PROFORMA);

            # Erreur fatale ?
            if (&LOC::Util::in_array(0x0001, $tabProformaErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            }
        }
    }


    # Enregistrement des infos proforma
    if (@{$tabErrors->{'fatal'}} == 0 && $tabProformaAction)
    {
        if (!&_execProformaAction($countryId, $contractId, $tabProformaAction, $userId, $tabProformaErrors, $tabEndUpds))
        {
            push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_PROFORMA);

            # Erreur fatale ?
            if (&LOC::Util::in_array(0x0001, $tabProformaErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            }
        }
    }

    # Enregistrement des documents
    if (@{$tabErrors->{'fatal'}} == 0 && $tabDocumentsActions)
    {
        my $tabDocumentsErrors = $tabErrors->{'modules'}->{'documents'};
        if (!&_execDocumentsActions($countryId, $contractId, $tabDocumentsActions, $userId, $tabDocumentsErrors, $tabEndUpds))
        {
            push(@{$tabErrors->{'warning'}}, ERROR_MODULE_DOCUMENTS);
        }
    }


    # Enregistrement du transport
    my $tabTransportErrors = $tabErrors->{'modules'}->{'transport'};
    my $tabDeliveryErrors = $tabTransportErrors->{'modules'}->{'delivery'};
    my $tabTransportOptions = {};
    if (@{$tabErrors->{'fatal'}} == 0 && !&LOC::Transport::setDocumentDelivery($countryId, 'contract', $contractId, $tabData->{'tabTransport'}->{'tabDelivery'}->{'data'}, $userId, $tabTransportOptions, $tabDeliveryErrors, $tabEndUpds))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_TRANSPORT);
        push(@{$tabTransportErrors->{'fatal'}}, ERROR_MODULE_TRANSPORT_DELIVERY);

        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabDeliveryErrors->{'fatal'}))
        {
            push(@{$tabTransportErrors->{'fatal'}}, ERROR_FATAL);
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        }
        # Modifications interdites ?
        if (&LOC::Util::in_array(0x0003, $tabDeliveryErrors->{'fatal'}))
        {
            push(@{$tabTransportErrors->{'fatal'}}, ERROR_RIGHTS);
            push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
        }
    }

    my $tabRecoveryErrors = $tabTransportErrors->{'modules'}->{'recovery'};
    my $tabTransportOptions = {};
    if (@{$tabErrors->{'fatal'}} == 0 && !&LOC::Transport::setDocumentRecovery($countryId, 'contract', $contractId, $tabData->{'tabTransport'}->{'tabRecovery'}->{'data'}, $userId, $tabTransportOptions, $tabRecoveryErrors, $tabEndUpds))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_TRANSPORT);
        push(@{$tabTransportErrors->{'fatal'}}, ERROR_MODULE_TRANSPORT_RECOVERY);

        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabRecoveryErrors->{'fatal'}))
        {
            push(@{$tabTransportErrors->{'fatal'}}, ERROR_FATAL);
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        }
        # Modifications interdites ?
        if (&LOC::Util::in_array(0x0003, $tabRecoveryErrors->{'fatal'}))
        {
            push(@{$tabTransportErrors->{'fatal'}}, ERROR_RIGHTS);
            push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
        }
    }

    # Mise à jour des codes de démarrage
    my $tabStartCodeErrors = $tabErrors->{'modules'}->{'startCode'};
    if (@{$tabErrors->{'fatal'}} == 0 && !&LOC::StartCode::Contract::Rent::save($countryId, $contractId, $userId, {}, $tabStartCodeErrors, $tabEndUpds))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_STARTCODE);
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabStartCodeErrors->{'fatal'}))
        {
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Bloquage des transports de la ligne de devis
    if (@{$tabErrors->{'fatal'}} == 0 && $linkedEstimateLineId != 0)
    {
        &LOC::Transport::lock($countryId, {'from-to' => {'type' => LOC::Transport::FROMTOTYPE_ESTIMATELINE,
                                                         'id'   => $linkedEstimateLineId}}, $userId);
    }


    # Reblocage du client s'il était débloqué temporairement
    if (@{$tabErrors->{'fatal'}} == 0 && !&_lockCustomerAfterUpdate($countryId, $tabCustomerInfos, $userId, $tabEndUpds, 'insert'))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
    }

    # Si il y a des données invalides alors on arrête ici
    if (@{$tabErrors->{'fatal'}} > 0)
    {
        return &$rollBackAndError(__LINE__);
    }

    # Trace de création du contrat
    &LOC::EndUpdates::deleteTraces($tabEndUpds, 'contract', $contractId);

    my $tabTrace = {
        'schema'  => $schemaName,
        'code'    => LOC::Log::EventType::CREATION,
        'old'     => undef,
        'new'     => undef,
        'content' => ''
    };
    &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            return &$rollBackAndError(__LINE__);
        }
    }

    # Exécution des mises à jour de fin
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    # Envoi du mail d'ouverture de compte
    if ($isSendCreateAccountMail)
    {
        my $tabContractInfos = {
            'code'      => $contractCode,
            'beginDate' => $beginDate
        };
        &_sendCreateAccountMail($countryId, $tabCustomerInfos, $tabContractInfos, $userId);
    }

    # Envoi du mail de réactivation de compte
    if ($isSendReactivateAccountMail)
    {
        my $tabContractInfos = {
            'code'      => $contractCode,
            'beginDate' => $beginDate
        };
        &_sendReactivateAccountMail($countryId, $tabCustomerInfos, $tabContractInfos, $userId);
    }

    return $contractId;
}


# Function: isPrintable
#
# Indique si le contrat est imprimable ou non
#
# Parameters:
# string   $countryId     - Pays
# int      $contractId    - Id du contrat
# hashref  $tabCacheInfos - Tableau des données pour optimisations
#
# Returns:
# bool -
sub isPrintable
{
    my ($countryId, $contractId, $tabCacheInfos) = @_;

    my $nbErroneous = &LOC::Tariff::Transport::checkIfErrors($countryId, 'contract', $contractId);
    if ($nbErroneous > 0)
    {
        return 0;
    }

    my $tabRow;
    if (defined $tabCacheInfos)
    {
        $tabRow = $tabCacheInfos;
    }
    else
    {
        $tabRow = &getInfos($countryId, $contractId, GETINFOS_TARIFFRENT | GETINFOS_TARIFFTRSP);
        if (!$tabRow)
        {
            return 0;
        }
    }

    # Etat des remises exceptionnelles de location et transport
    my $rentSpRdcStatus = (defined $tabRow->{'rentTariff.tabInfos'} ? $tabRow->{'rentTariff.tabInfos'}->{'specialReductionStatus'} : $tabRow->{'rentTariff.specialReductionStatus'});
    my $trspSpRdcStatus = (defined $tabRow->{'transportTariff.tabInfos'} ? $tabRow->{'transportTariff.tabInfos'}->{'specialReductionStatus'} : $tabRow->{'transportTariff.specialReductionStatus'});

    if (!$tabRow ||
        $tabRow->{'state.id'} eq STATE_CANCELED ||
        $tabRow->{'machine.id'} == 0 ||
        $rentSpRdcStatus eq LOC::Tariff::Rent::REDUCTIONSTATE_PENDING ||
        $trspSpRdcStatus eq LOC::Tariff::Transport::REDUCTIONSTATE_PENDING)
    {
        return 0;
    }

    return 1;
}


# Function: print
#
# Met à jour l'indicateur d'impression sur le contrat
#
# Parameters:
# string       $countryId        - Pays
# arrayref|int $tabContractsIds  - Identifiants de contrats
# int          $userId           - Identifiant de l'utilisateur
# hashref      $tabParams        - Paramètres de l'impression
# hashref      $tabEndUpds       - Tableau des mises à jour de fin
#
# Returns:
# bool -
sub print
{
    my ($countryId, $tabContractsIds, $userId, $tabParams, $tabEndUpds) = @_;

    if (ref $tabContractsIds ne 'ARRAY')
    {
        $tabContractsIds = [$tabContractsIds];
    }

    if (!defined $tabParams->{'type'})
    {
        $tabParams->{'type'} = PRINTTYPE_PRINT;
    }


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }


    # Traitement des contrats
    foreach my $contractId (@$tabContractsIds)
    {
        my $db = &LOC::Db::getConnection('location', $countryId);
        my $hasTransaction = $db->hasTransaction();
        my $schemaName = 'LOCATION' . ($countryId eq 'FR' ? '' : $countryId);

        # Démarre la transaction
        if (!$hasTransaction)
        {
            $db->beginTransaction();
        }

        my $rollBackAndError = sub {
            if (!$hasTransaction)
            {
                # Annule la transaction
                $db->rollBack();
            }

            # Affichage de l'erreur !
            print STDERR sprintf($fatalErrMsg, 'print', $_[0], __FILE__, '');

            return 0;
        };


        # Récupération des informations du contrat
        my $tabInfos = &getInfos($countryId, $contractId, GETINFOS_TARIFFRENT | GETINFOS_TARIFFTRSP);
        if (!$tabInfos)
        {
            return &$rollBackAndError(__LINE__);
        }
        my $oldIsPrinted = $tabInfos->{'isPrinted'};

        # Nouvel état d'impression
        my $newIsPrinted = 1;


        # Pas de droit d'impression
        my $tabRights = &getRights($countryId, $tabInfos->{'agency.id'}, undef, $userId);
        if (!&_hasRight($tabRights->{'actions'}, 'print'))
        {
            return &$rollBackAndError(__LINE__);
        }


        # Document non imprimable
        if (!&isPrintable($countryId, $contractId, $tabInfos))
        {
            return &$rollBackAndError(__LINE__);
        }


        # Récupération de la connexion à la base de données
        # Si l'état de première impression a changé
        if ($oldIsPrinted != $newIsPrinted)
        {
            # Mise à jour
            if ($db->update('CONTRAT', {'CONTRATIMP' => $newIsPrinted - 1},
                                       {'CONTRATAUTO*' => $contractId}, $schemaName) == -1)
            {
                return &$rollBackAndError(__LINE__);
            }

            # Décrémentation de l'alerte des contrats à éditer
            &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::FIRSTPRINTRENTCONTRACTS, -1);
        }


        # Historique
        my $tabTraceCode = {
            LOC::Contract::Rent::PRINTTYPE_PRINT() => LOC::Log::EventType::PRINT,
            LOC::Contract::Rent::PRINTTYPE_PDF()   => LOC::Log::EventType::UPLOADPDF,
            LOC::Contract::Rent::PRINTTYPE_EMAIL() => LOC::Log::EventType::SENDMAIL
        };


        my $tabTraces = (defined $tabParams->{'traces'} ? $tabParams->{'traces'} : {});
        if (!defined $tabTraces->{'schema'})
        {
            $tabTraces->{'schema'} = $schemaName;
        }
        if (!defined $tabTraces->{'code'})
        {
            $tabTraces->{'code'} = $tabTraceCode->{$tabParams->{'type'}};
        }
        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTraces);


        # Valide la transaction
        if (!$hasTransaction)
        {
            if (!$db->commit())
            {
                return &$rollBackAndError(__LINE__);
            }
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: update
# Mise à jour du contrat
#
# Parameters:
# string   $countryId  - Pays
# int      $contractId - Id du contrat
# hashref  $tabData    - Données à mettre à jour
# int      $userId     - Utilisateur responsable de la modification
# hashref  $tabErrors  - Tableau des erreurs
# hashref  $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool -
sub update
{
    my ($countryId, $contractId, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'warning'} = [];
    $tabErrors->{'fatal'} = [];
    $tabErrors->{'modules'} = {
        'site' => {},
        'rentTariff' => {},
        'transportTariff' => {},
        'transport'       => {
            'fatal' => [],
            'modules' => {
                'delivery' => {},
                'recovery' => {}
            }
        },
        'proforma' => {},
        'documents' => [],
        'days' => {},
        'machine' => {},
        'startCode' => {}
    };


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Nom du schéma de connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'update', $_[0], __FILE__, '');

        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }

        return 0;
    };


    # Récupération des informations du contrat
    my $flags = GETINFOS_POSSIBILITIES |
                GETINFOS_INSURANCE |
                GETINFOS_APPEAL |
                GETINFOS_RESIDUES |
                GETINFOS_FUEL |
                GETINFOS_CLEANING |
                GETINFOS_EXT_SITE;
    my $tabOptions = {
        'lockDbRow' => 1
    };
    my $tabInfos = &getInfos($countryId, $contractId, $flags, $tabOptions);
    if (!$tabInfos)
    {
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }
    $contractId = $tabInfos->{'id'};

    # Vérification du blocage en écriture
    if ($tabInfos->{'isWriteLocked'})
    {
        return &$rollBackAndError(__LINE__, ERROR_WRITELOCKED);
    }

    # Vérification de la date de modification (accès concurentiel)
    if (exists $tabData->{'modificationDate'} && $tabData->{'modificationDate'} ne $tabInfos->{'modificationDate'})
    {
        return &$rollBackAndError(__LINE__, 0x0100);
    }

    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Récupération des droits
    my $tabRights = &_getRights($tabInfos, $tabUserInfos);


    # Paramètres
    # ----------
    my $contractCode = $tabInfos->{'code'};
    my $subAction    = ($tabData->{'subAction'} =~ m/^stop|reactivate|invoice|restop|cancel$/ ? $tabData->{'subAction'} : 'valid');
    my $customerId   = $tabInfos->{'customer.id'};
    my $agencyId     = $tabInfos->{'agency.id'};
    my $negotiatorId = (exists $tabData->{'negotiator.id'} ? $tabData->{'negotiator.id'} : $tabInfos->{'negotiator.id'});
    my $siteId       = $tabInfos->{'site.id'};
    my $comments     = (exists $tabData->{'comments'} ? &LOC::Util::trim($tabData->{'comments'}) : $tabInfos->{'comments'});
    my $tracking     = (exists $tabData->{'tracking'} ? &LOC::Util::trim($tabData->{'tracking'}) : $tabInfos->{'tracking'});
    my $oldZone      = $tabInfos->{'zone'};
    my $newZone      = (exists $tabData->{'tabSite'} && exists $tabData->{'tabSite'}->{'zone'} ? $tabData->{'tabSite'}->{'zone'} : $oldZone);
    my $orderNo      = (exists $tabData->{'tabCustomer'}->{'orderNo'} ? &LOC::Util::trim($tabData->{'tabCustomer'}->{'orderNo'}) : $tabInfos->{'orderNo'});
    my $orderContactFullName  = (exists $tabData->{'tabCustomer'}->{'orderContact.fullName'} ? &LOC::Util::trim($tabData->{'tabCustomer'}->{'orderContact.fullName'}) : $tabInfos->{'orderContact.fullName'});
    my $orderContactTelephone = (exists $tabData->{'tabCustomer'}->{'orderContact.telephone'} ? &LOC::Util::trim($tabData->{'tabCustomer'}->{'orderContact.telephone'}) : $tabInfos->{'orderContact.telephone'});
    my $orderContactEmail     = (exists $tabData->{'tabCustomer'}->{'orderContact.email'} ? &LOC::Util::trim($tabData->{'tabCustomer'}->{'orderContact.email'}) : $tabInfos->{'orderContact.email'});
    my $stateId      = $tabInfos->{'state.id'};
    my $invoiceState = $tabInfos->{'invoiceState'};
    my $updateFrom   = (exists $tabData->{'updateFrom'} ? $tabData->{'updateFrom'} : undef);

    my $oldIsAsbestos = $tabInfos->{'site.tabInfos'}->{'isAsbestos'};
    my $newIsAsbestos = (exists $tabData->{'tabSite'} && exists $tabData->{'tabSite'}->{'isAsbestos'} ? $tabData->{'tabSite'}->{'isAsbestos'} : $oldIsAsbestos);

    # Onglet "Matériel"
    # -----------------
    my $oldModelId = $tabInfos->{'model.id'};
    my $newModelId = (exists $tabData->{'model.id'} ? $tabData->{'model.id'} : $oldModelId);
    my $isModelChanged = ($newModelId != $oldModelId);

    my $oldWishedMachineId = $tabInfos->{'wishedMachine.id'};
    my $newWishedMachineId = (exists $tabData->{'wishedMachine.id'} ? $tabData->{'wishedMachine.id'} : ($isModelChanged ? undef : $oldWishedMachineId));

    my $machineId  = (exists $tabData->{'machine.id'} ? ($tabData->{'machine.id'} ? $tabData->{'machine.id'} * 1 : undef) : ($isModelChanged ? 0 : $tabInfos->{'machine.id'}));
    my $isFinalAllocation = (exists $tabData->{'isFinalAllocation'} ? ($tabData->{'isFinalAllocation'} ? 1 : 0) : $tabInfos->{'isFinalAllocation'});
    my $isImperativeModel = (exists $tabData->{'isImperativeModel'} ? $tabData->{'isImperativeModel'} : $tabInfos->{'isImperativeModel'});
    my $isJoinDoc = (exists $tabData->{'isJoinDoc'} ? $tabData->{'isJoinDoc'} : $tabInfos->{'isJoinDoc'});


    # Onglet "Durée"
    # --------------
    my $oldBeginDate = $tabInfos->{'beginDate'};
    my $newBeginDate = (exists $tabData->{'beginDate'} ? $tabData->{'beginDate'} : $oldBeginDate);
    my $oldEndDate   = $tabInfos->{'endDate'};
    my $newEndDate   = (exists $tabData->{'endDate'} ? $tabData->{'endDate'} : $oldEndDate);
    if ($newBeginDate eq '' || !&LOC::Date::checkMySQLDate($newBeginDate))
    {
        $newBeginDate = undef;
    }
    if ($newEndDate eq '' || !&LOC::Date::checkMySQLDate($newEndDate))
    {
        $newEndDate = undef;
    }
    # - Jours plus et jours moins
    my $oldAdditionalDaysUnbilled = $tabInfos->{'additionalDaysUnbilled'};
    my $additionalDaysBilled      = $tabInfos->{'additionalDaysBilled'};

    my $oldAdditionalDaysComment = $tabInfos->{'additionalDaysComment'};
    my $newAdditionalDaysComment = (exists $tabData->{'additionalDaysComment'} ? $tabData->{'additionalDaysComment'} :
                                                                                    $oldAdditionalDaysComment);

    my $oldDeductedDaysUnbilled = $tabInfos->{'deductedDaysUnbilled'};
    my $deductedDaysBilled      = $tabInfos->{'deductedDaysBilled'};

    my $oldDeductedDaysComment = $tabInfos->{'deductedDaysComment'};
    my $newDeductedDaysComment = (exists $tabData->{'deductedDaysComment'} ? $tabData->{'deductedDaysComment'} :
                                                                                $oldDeductedDaysComment);

    my $tabDaysActions  = $tabData->{'tabDaysActions'};

    # - Calcul des jours ouvrés de location
    my $oldDuration  = $tabInfos->{'duration'};
    my $oldRealDuration = $oldDuration - ($oldAdditionalDaysUnbilled + $additionalDaysBilled)
                                       + ($oldDeductedDaysUnbilled + $deductedDaysBilled);
    my $newRealDuration;
    if (&_hasRight($tabRights->{'duration'}, 'endDate') || $updateFrom eq 'validatePF')
    {
        $newRealDuration = &LOC::Date::getRentDuration($newBeginDate, $newEndDate, $agencyId);
    }
    else
    {
        $newRealDuration = $oldRealDuration;
    }
    # - Calcul de la durée de location
    # = Durée réelle + (J+ - J-)existants + (J+ - J-)actions
    my $newTabDiffDaysDuration = &LOC::Common::Rent::calculateDiffDaysByActions($countryId, $tabDaysActions);
    my $newAdditionalDaysUnbilled = $oldAdditionalDaysUnbilled + $newTabDiffDaysDuration->{LOC::Day::TYPE_ADDITIONAL()};
    my $newDeductedDaysUnbilled   = $oldDeductedDaysUnbilled + $newTabDiffDaysDuration->{LOC::Day::TYPE_DEDUCTED()};

    my $newDuration = $newRealDuration + ($newAdditionalDaysUnbilled + $additionalDaysBilled)
                                       - ($newDeductedDaysUnbilled + $deductedDaysBilled);


    # - Calcul de la durée calendaire
    my $oldCalDuration = &LOC::Date::getDeltaDays($oldBeginDate, $oldEndDate);
    my $newCalDuration = &LOC::Date::getDeltaDays($newBeginDate, $newEndDate);


    # Onglet "Autres services"
    # ------------------------
    my $fuelQuantity = (exists $tabData->{'fuelQuantity'} ? $tabData->{'fuelQuantity'} : $tabInfos->{'fuel.quantity'});
    my $cleaningTypeId = (exists $tabData->{'tabCleaning'}->{'typeId'} ? $tabData->{'tabCleaning'}->{'typeId'} : $tabInfos->{'cleaning.type.id'});
    my $cleaningAmount = (exists $tabData->{'tabCleaning'}->{'amount'} ? $tabData->{'tabCleaning'}->{'amount'} : $tabInfos->{'cleaning.amount'});
    # - Participation au recyclage
    my $residuesMode = (exists $tabData->{'residues'}->{'mode'} ? $tabData->{'residues'}->{'mode'} : $tabInfos->{'residues.mode'});
    # - Jours de remise en état
    my $tabRepairDays = (exists $tabData->{'tabRepairDays'} ? $tabData->{'tabRepairDays'} : []);
    # - Services de remise en état
    my $tabRepairServices = (exists $tabData->{'tabRepairServices'} ? $tabData->{'tabRepairServices'} : []);
    # - Récupération du montant minimum des services location et transport pour l'utilisateur connecté
    my $rentServiceMinAmount = &LOC::Characteristic::getAgencyValueByCode('MTMINSRV', $agencyId);
    my $rentServiceNoLimitUserIdList = &LOC::Characteristic::getAgencyValueByCode('USRNOMAXREMSRV', $agencyId);
    my @rentServiceNoLimitUserId = split(',', $rentServiceNoLimitUserIdList);
    # - Si l'utilisateur courant n'a pas de limite de services location et transport
    if (grep {$_ eq $userId} @rentServiceNoLimitUserId)
    {
        $rentServiceMinAmount = undef;
    }
    # - Fiches de remise en état à rouvrir
    my $tabRepairSheetsToReopen = (exists $tabData->{'tabRepairSheetsToReopen'} ? $tabData->{'tabRepairSheetsToReopen'} : []);
    # - Services de location et transport
    my $tabRentServices = (exists $tabData->{'tabRentServices'} ? $tabData->{'tabRentServices'} : []);
    # - Assurance
    my $oldInsuranceTypeId = $tabInfos->{'insuranceType.id'};
    my $oldInsuranceRate = $tabInfos->{'insurance.rate'};
    my $newInsuranceTypeId = (exists $tabData->{'tabInsurance'}->{'typeId'} ? $tabData->{'tabInsurance'}->{'typeId'} : $oldInsuranceTypeId);
    my $newInsuranceRate = (exists $tabData->{'tabInsurance'}->{'rate'} ? $tabData->{'tabInsurance'}->{'rate'} : $oldInsuranceRate);

    my $tabNewInsuranceTypeInfos = &LOC::Insurance::Type::getInfos($countryId, $newInsuranceTypeId);
    my $tabOldInsuranceTypeInfos = &LOC::Insurance::Type::getInfos($countryId, $oldInsuranceTypeId);

    # - Gestion de recours
    my $oldAppealTypeId = $tabInfos->{'appealType.id'};
    my $newAppealTypeId = (exists $tabData->{'tabAppeal'}->{'typeId'} ? $tabData->{'tabAppeal'}->{'typeId'} : $oldAppealTypeId);

    # Récupération des informations du client
    my $tabCustomerInfos = &LOC::Customer::getInfos($countryId, $customerId);

    my $hasLockableUpdates = 0;


    # Vérification des données
    # ------------------------
    if (!$negotiatorId)
    {
        return &$rollBackAndError(__LINE__, 0x0010);
    }

    # - Vérifie que l'horaire de livraison est renseignée si c'est un pré-contrat
    if (exists $tabData->{'tabSite'} && !$tabData->{'tabSite'}->{'deliveryHour'} && !$tabData->{'tabSite'}->{'deliveryTimeSlot.id'}
        && $stateId eq STATE_PRECONTRACT)
    {
        return &$rollBackAndError(__LINE__, 0x0113);
    }
    # - Vérifie que le mail et le tél du contact commande sont renseignés
    if (!$orderContactEmail || !$orderContactTelephone)
    {
         push(@{$tabErrors->{'warning'}}, 0x0128);
    }
    # - Vérifie que le mail du contact commande est valide
    if ($orderContactEmail && !&LOC::Util::isValidEmail($orderContactEmail))
    {
        return &$rollBackAndError(__LINE__, 0x0127);
    }

    if ($newModelId == 0)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0115);
    }
    else
    {
        if ($newBeginDate eq '' || $newEndDate eq '')
        {
            push(@{$tabErrors->{'fatal'}}, 0x0109);
        }
        else
        {
            if ($newDuration eq '' || $newDuration < 0)
            {
                # Durée invalide
                push(@{$tabErrors->{'fatal'}}, 0x0116);
            }
            elsif ($newDuration > $oldDuration)
            {
                # La prolongation de contrat doit rebloquer le client s'il était débloqué temporairement
                # La prolongation est interdite pour les clients bloqués
                $hasLockableUpdates = 1;

                # Si c'est un contrat en arrêt...
                if ($stateId eq STATE_STOPPED)
                {
                    # Nombre de jours de prolongation maximum pour un contrat en arrêt
                    my $nbDaysMaxExtensionStopCtt = &LOC::Characteristic::getAgencyValueByCode('CONPROLONGARRET', $agencyId);
                    if ($newDuration - $oldDuration > $nbDaysMaxExtensionStopCtt)
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x0112);
                    }
                }

                my $unitPrice = (exists $tabData->{'tabRentTariff'}->{'amount'} ? $tabData->{'tabRentTariff'}->{'amount'} : $tabInfos->{'unitPrice'});

                if ($updateFrom ne 'validatePF')
                {
                    # est-on dans le process pro forma ?
                    if (!($tabInfos->{'proformaFlags'} & LOC::Proforma::DOCFLAG_EXISTACTIVEDADVANCE) &&
                        (($tabInfos->{'proformaFlags'} & LOC::Proforma::DOCFLAG_CT_ISCUSTOMERPFREQONCREATION &&
                          $tabCustomerInfos->{'isProformaRequired'}) ||
                         $tabInfos->{'proformaFlags'} & LOC::Proforma::DOCFLAG_EXISTACTIVED) &&
                        $unitPrice > 0)
                    {
                        if ($newRealDuration - $oldRealDuration > 0)
                        {
                            # obligation de passer par une pro forma de prolongation
                            push(@{$tabErrors->{'fatal'}}, 0x0108);
                        }
                    }
                    # -Non
                    else
                    {
                        # sinon dépend du blocage du client
                        if ((!$tabCustomerInfos->{'isTempUnlock'} && $tabCustomerInfos->{'lockLevel'} > 0))
                        {
                            # Nombre de jours de prolongation maximum pour les clients non autorisés à la prolongation
                            my $nbDaysMax = 0;
                            if ($tabCustomerInfos->{'lockLevel'} == 4 || $tabCustomerInfos->{'lockLevel'} == 2)
                            {
                                # client bloqué avec prolongation max autorisée
                                $nbDaysMax = &LOC::Characteristic::getAgencyValueByCode('CONPROLONGMAX', $agencyId);
                            }
                            if ($newDuration - $oldDuration > $nbDaysMax)
                            {
                                push(@{$tabErrors->{'fatal'}}, 0x0106);
                            }
                        }
                    }
                }

            }

            if ($newEndDate lt $newBeginDate)
            {
                # La date de fin est inférieure à la date de début
                push(@{$tabErrors->{'fatal'}}, 0x0117);
            }



            # Si la date de début a été modifiée
            if ($newBeginDate ne $oldBeginDate)
            {
                if ($invoiceState ne INVOICESTATE_NONE)
                {
                    # La date de début n'est pas modifiable
                    push(@{$tabErrors->{'fatal'}}, 0x0119);
                }
                else
                {
                    # Date de création - 1 mois
                    my %tabCreationDate = &LOC::Date::getHashDate($tabInfos->{'creationDate'});
                    my ($tYear, $tMonth, $tDay) = &Date::Calc::Add_Delta_YM($tabCreationDate{'y'},
                                                                            $tabCreationDate{'m'},
                                                                            $tabCreationDate{'d'},
                                                                            0,
                                                                            -1);
                    my $creationDateLess1Month = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE, ('y' => $tYear, 'm' => $tMonth, 'd' => $tDay));

                    if ($newBeginDate le $creationDateLess1Month)
                    {
                        # La date de début ne doit pas être inférieure à 1 mois
                        push(@{$tabErrors->{'fatal'}}, 0x0121);
                    }
                }
            }
            # Si la date de fin a été modifiée
            if ($newEndDate ne $oldEndDate)
            {
                if ($invoiceState eq INVOICESTATE_FINALPARTIAL || $invoiceState eq INVOICESTATE_FINAL)
                {
                    # La date de fin n'est pas modifiable
                    push(@{$tabErrors->{'fatal'}}, 0x0120);
                }
                else
                {
                    if ($tabInfos->{'lastInvoiceDate'} ne '' && $newEndDate lt $tabInfos->{'lastInvoiceDate'})
                    {
                        # La date de fin est inférieure à la date de dernière facturation
                        push(@{$tabErrors->{'fatal'}}, 0x0118);
                    }
                }
            }
        }

        # Vérification du model de la machine souhaitée
        if ($newWishedMachineId && $oldWishedMachineId != $newWishedMachineId)
        {
            my $tabWishedMachineInfos = &LOC::Machine::getInfos($countryId, $newWishedMachineId, LOC::Machine::GETINFOS_BASE);
            if (!$tabWishedMachineInfos)
            {
                return &$rollBackAndError(__LINE__, ERROR_FATAL);
            }
            if ($newModelId != $tabWishedMachineInfos->{'model.id'})
            {
                push(@{$tabErrors->{'fatal'}}, 0x0114);
            }
        }
    }

    # Si erreur de vérification de données alors on arrête !!
    if (@{$tabErrors->{'fatal'}} > 0)
    {
        return &$rollBackAndError(__LINE__);
    }


    # Vérification et historisation des modifications
    # -------------------------------
    my $tabChanges = {};
    # - Bouton d'action
    if (!&_hasRight($tabRights->{'actions'}, $subAction) && $updateFrom ne 'validatePF')
    {
        return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
    }
    # - Modification de la date de début
    if ($oldBeginDate ne $newBeginDate)
    {
        # Vérification:
        if (!$tabInfos->{'possibilities'}->{'duration'}->{'beginDate'} ||
            !&_hasRight($tabRights->{'duration'}, 'beginDate'))
        {
            return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
        }

        # Historisation:
        my $tabTrace = {
            'schema'  => $schemaName,
            'code'    => LOC::Log::EventType::UPDBEGINDATE,
            'old'     => $oldBeginDate,
            'new'     => $newBeginDate,
            'content' => $oldBeginDate . ' → ' . $newBeginDate
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
    }
    # - Modification de la date de fin
    if ($oldEndDate ne $newEndDate)
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'duration'}, 'endDate') && $updateFrom ne 'validatePF')
        {
            return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
        }

        # Historisation:
        my $tabTrace = {
            'schema'  => $schemaName,
            'code'    => LOC::Log::EventType::UPDENDDATE,
            'old'     => $oldEndDate,
            'new'     => $newEndDate,
            'content' => $oldEndDate . ' → ' . $newEndDate
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
    }
    if ($oldEndDate ne $newEndDate || $oldIsAsbestos != $newIsAsbestos)
    {
        # Incrémente ou décrémente l'alerte "Contrat(s) à passer en facturation"
        if ($stateId eq STATE_STOPPED)
        {
            my %tabNowDate = &LOC::Date::getHashDate();
            my $nowDate = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE, %tabNowDate);
            if ($oldEndDate le $nowDate && $newEndDate gt $nowDate)
            {
                &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::CONTRACTSTOINVOICE, -1);
            }
            elsif ($oldEndDate gt $nowDate && $newEndDate le $nowDate)
            {
                &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::CONTRACTSTOINVOICE, +1);
            }
        }

        # Incrémente ou décrémente l'alerte "Contrat(s) à arrêter"
        if ($stateId eq STATE_CONTRACT)
        {
            my %tabNowDate = &LOC::Date::getHashDate();
            my $nowDate = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE, %tabNowDate);

            my $tabCfgsStopAlert = &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('CFGSTOPALERT', $agencyId));
            my $deltaDaysIsAsbestos = (defined $tabCfgsStopAlert->{'asbestosDays'} ? $tabCfgsStopAlert->{'asbestosDays'} : 0);
            my $shiftDate = &LOC::Date::addDays($nowDate, $deltaDaysIsAsbestos);

            my $delta = 0;

            # - Ancienne date de fin avant aujourd'hui
            # - ET Nouvelle date de fin après aujourd'hui
            if ($oldEndDate le $nowDate && $newEndDate gt $nowDate)
            {
                # si le nouveau n'est pas "amiante" OU que la date n'est pas dans la période autorisée pour l'amiante
                if (!$newIsAsbestos || $newEndDate gt $shiftDate)
                {
                    # -- Décrémentation
                    $delta = -1;
                }
            }
            # - Ancienne date de fin après ajourd'hui
            elsif ($oldEndDate gt $nowDate)
            {
                # - Nouvelle date de fin avant aujourd'hui
                if ($newEndDate le $nowDate)
                {
                    # - si l'ancien est non "amiante" OU hors de la période autorisée pour l'amiante
                    if (!$oldIsAsbestos || $oldEndDate gt $shiftDate)
                    {
                        # -- Incrémentation
                        $delta = +1;
                    }
                }
                # - Nouvelle date de fin après ajourd'hui
                else
                {
                    # - si l'ancien est "amiante" ET dans la période autorisée pour l'amiante
                    # - ET le nouveau est non "amiante" OU pas dans la période autorisée pour l'amiante
                    if ($oldIsAsbestos && $oldEndDate le $shiftDate &&
                        (!$newIsAsbestos || $newEndDate gt $shiftDate))
                    {
                        # -- Décrémentation
                        $delta = -1;
                    }
                    # - si l'ancien est non "amiante"
                    # - ET le nouveau est "amiante" ET dans la période autorisée pour l'amiante
                    elsif (!$oldIsAsbestos &&
                            $newIsAsbestos && $newEndDate le $shiftDate)
                    {
                        # -- Incrémentation
                        $delta = +1;
                    }
                }
            }
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::CONTRACTSTOSTOP, $delta);
        }
    }
    # - Modification des jours supplémentaires
    if ($newAdditionalDaysUnbilled != $oldAdditionalDaysUnbilled)
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'duration'}, 'additionalDays'))
        {
            return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
        }
    }
    # - Modification des jours déduits
    if ($newDeductedDaysUnbilled != $oldDeductedDaysUnbilled)
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'duration'}, 'deductedDays'))
        {
            return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
        }
    }
    # - Modification commentaire jours supplémentaires
    if ($newAdditionalDaysComment ne $oldAdditionalDaysComment)
    {
        # Vérification
        if (!&_hasRight($tabRights->{'duration'}, 'additionalDaysComment'))
        {
            return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
        }

        my $oldContent = $oldAdditionalDaysComment;
        $oldContent =~ s/[\r\n]{1,2}/ /g;
        my $newContent = $newAdditionalDaysComment;
        $newContent =~ s/[\r\n]{1,2}/ /g;

        my $tabTrace = {
            'schema'  => $schemaName,
            'code'    => LOC::Log::EventType::UPDADDEDDAYCOMMENT,
            'old'     => $oldAdditionalDaysComment,
            'new'     => $newAdditionalDaysComment,
            'content' => $oldContent . ' → ' . $newContent
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
    }
    # - Modification commentaire jours supplémentaires
    if ($newDeductedDaysComment ne $oldDeductedDaysComment)
    {
        # Vérification
        if (!&_hasRight($tabRights->{'duration'}, 'additionalDaysComment'))
        {
            return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
        }

        my $oldContent = $oldDeductedDaysComment;
        $oldContent =~ s/[\r\n]{1,2}/ /g;
        my $newContent = $newDeductedDaysComment;
        $newContent =~ s/[\r\n]{1,2}/ /g;

        my $tabTrace = {
            'schema'  => $schemaName,
            'code'    => LOC::Log::EventType::UPDDEDUCTEDDAYCOMMENT,
            'old'     => $oldDeductedDaysComment,
            'new'     => $newDeductedDaysComment,
            'content' => $oldContent . ' → ' . $newContent
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
    }
    # - Modification de la durée
    if ($newDuration != $oldDuration)
    {
        # Historisation:
        my $tabTrace = {
            'schema'  => $schemaName,
            'code'    => LOC::Log::EventType::UPDDURATION,
            'old'     => $oldDuration,
            'new'     => $newDuration,
            'content' => sprintf('%.1f → %.1f', $oldDuration, $newDuration)
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
    }
    #- Modification de la durée calendaire
    if ($newCalDuration != $oldCalDuration)
    {
        # Historisation:
        my $tabTrace = {
            'schema'  => $schemaName,
            'code'    => LOC::Log::EventType::UPDCALENDARDURATION,
            'old'     => $oldCalDuration,
            'new'     => $newCalDuration,
            'content' => sprintf('%.1f → %.1f', $oldCalDuration, $newCalDuration)
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
    }

    # - Changement de modèle de machines
    if ($isModelChanged)
    {
        # Vérification:
        if (!$tabInfos->{'possibilities'}->{'equipment'}->{'model'} || !&_hasRight($tabRights->{'machine'}, 'model'))
        {
            return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
        }

        # - Vérifie que le modèle de machine est disponible si il a changé
        if (!&LOC::Model::isRentable($countryId, $agencyId, $newModelId))
        {
            return &$rollBackAndError(__LINE__, 0x0111);
        }

        # Historisation:
        my $subQuery = '(SELECT CAST(CONCAT(MOMADESIGNATION, " {", MOMAAUTO,"}") AS CHAR CHARACTER SET utf8) FROM AUTH.MODELEMACHINE WHERE MOMAAUTO = %d)';
        my $query = 'CONCAT(' . sprintf($subQuery, $oldModelId) . ', " → ", ' . sprintf($subQuery, $newModelId) . ')';
        my $tabTrace = {
            'schema'   => $schemaName,
            'code'     => LOC::Log::EventType::CHANGEMODEL,
            'old'      => $oldModelId,
            'new'      => $newModelId,
            'content'  => '',
            'sqlQuery' => $query
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
    }
    #- Modification de la machine souhaitée
    if ($newWishedMachineId != $oldWishedMachineId)
    {
        # Historisation:
        my $tabTrace = {
            'schema'  => $schemaName,
            'code'    => LOC::Log::EventType::CHANGEWISHEDMACHINE,
            'old'     => $oldWishedMachineId,
            'new'     => $newWishedMachineId,
            'content' => $oldWishedMachineId . ' → ' . $newWishedMachineId
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
    }

    # - Modification de l'assurance
    if ($newInsuranceTypeId != $oldInsuranceTypeId || ($tabNewInsuranceTypeInfos->{'isInvoiced'} && $newInsuranceRate != $oldInsuranceRate))
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'services'}, 'insurance'))
        {
            return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
        }

        # Vérification de l'assurance des contrats en FullService
        # - l'assurance doit être assurance incluse
        # - le type et le taux d'assurance ne sont pas modifiables
        # Vérification de l'assurance des contrats non FullService
        # -- le type et le taux négocié ne sont pas modifiables pour les clients en assurance facturée négociée (taux <> taux par défaut)
        # -- le taux par défaut est le seul taux possible
        if ($tabInfos->{'isFullService'})
        {
            # Type non modifiable
            if (!$tabNewInsuranceTypeInfos->{'isIncluded'})
            {
                return &$rollBackAndError(__LINE__, 0x0124);
            }
            # Taux non modifiable
            if ($newInsuranceRate != 0)
            {
                return &$rollBackAndError(__LINE__, 0x0125);
            }
        }
        else
        {
            # Récupération des informations du pays
            my $tabCountryInfos = &LOC::Country::getInfos($countryId);

            # Vérification : le type et le taux ne doivent pas changer pour les clients en assurance facturée négociée
            if ($tabOldInsuranceTypeInfos->{'isInvoiced'} && $oldInsuranceRate != $tabCountryInfos->{'insurance.defaultRate'})
            {
                    # Type non modifiable
                    if ($newInsuranceTypeId != $oldInsuranceTypeId)
                    {
                        return &$rollBackAndError(__LINE__, 0x0124);
                    }
                    # Taux non modifiable
                    if ($newInsuranceRate ne $oldInsuranceRate)
                    {
                        return &$rollBackAndError(__LINE__, 0x0125);
                    }
            }
            # Vérification : le taux par défaut est le seul taux possible
            if ($tabOldInsuranceTypeInfos->{'isInvoiced'} &&
                $tabNewInsuranceTypeInfos->{'isInvoiced'} &&
                $newInsuranceRate != $tabCountryInfos->{'insurance.defaultRate'})
            {
                # Taux non modifiable
                return &$rollBackAndError(__LINE__, 0x0125);
            }
        }

        # Historisation:
        my $tabTrace = {
            'schema'  => $schemaName,
            'code'    => LOC::Log::EventType::UPDINSURANCE,
            'old' => {
                'type' => $oldInsuranceTypeId,
                'rate' => $oldInsuranceRate
            },
            'new' => {
                'type' => $newInsuranceTypeId,
                'rate' => $newInsuranceRate
            },
            'content' => 'type ' . $oldInsuranceTypeId . ', taux ' . ($oldInsuranceRate * 100) . ' %' .
                         ' → ' .
                         'type ' . $newInsuranceTypeId . ', taux ' . ($newInsuranceRate * 100) . ' %'
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
    }
    # - Modification des informations commande
    if ($orderNo ne $tabInfos->{'orderNo'})
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'customer'}, 'orderNo'))
        {
            return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
        }
        $tabChanges->{'orderNo'} = {'old' => $orderNo, 'new' => $tabInfos->{'orderNo'}};
    }
    if ($orderContactFullName ne $tabInfos->{'orderContact.fullName'})
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'customer'}, 'orderContact'))
        {
            return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
        }
        $tabChanges->{'orderContact.fullName'} = {'old' => $orderContactFullName, 'new' => $tabInfos->{'orderContact.fullName'}};
    }
    if ($orderContactTelephone ne $tabInfos->{'orderContact.telephone'})
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'customer'}, 'orderContact'))
        {
            return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
        }
        $tabChanges->{'orderContact.telephone'} = {'old' => $orderContactTelephone, 'new' => $tabInfos->{'orderContact.telephone'}};
    }
    if ($orderContactEmail ne $tabInfos->{'orderContact.email'})
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'customer'}, 'orderContact'))
        {
            return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
        }
        $tabChanges->{'orderContact.email'} = {'old' => $orderContactEmail, 'new' => $tabInfos->{'orderContact.email'}};
    }

    # Vérification de la gestion de recours
    # - Gestion de recours sur un type d'assurance non éligible
    if ($tabNewInsuranceTypeInfos)
    {
        # - Gestion de recours sur un type d'assurance non éligible
        if (!$tabNewInsuranceTypeInfos->{'isAppealActivated'} && defined $newAppealTypeId)
        {
            return &$rollBackAndError(__LINE__, 0x0129);
        }

        # - Pas de gestion de recours définie sur un type d'assurance éligible
        if ($tabNewInsuranceTypeInfos->{'isAppealActivated'} && !defined $newAppealTypeId)
        {
            return &$rollBackAndError(__LINE__, 0x0129);
        }
    }

    # - Modèle impératif
    # - Transfert de chantier +
    # - Modification du commentaire +
    # - Modification du suivi
    if (($isImperativeModel != $tabInfos->{'isImperativeModel'} && !&_hasRight($tabRights->{'machine'}, 'isImperativeModel')) ||
        ($comments ne $tabInfos->{'comments'} && !&_hasRight($tabRights->{'infoscpl'}, 'comments')) ||
        ($tracking ne $tabInfos->{'tracking'} && !&_hasRight($tabRights->{'infoscpl'}, 'tracking')))
    {
        return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
    }


    # Si le n° de commande est obligatoire sur la facture pour ce client
    if ($tabCustomerInfos->{'isOrderNoOnInvoice'} && ($orderNo ne $tabInfos->{'orderNo'}))
    {
        if ($orderNo eq '') # Si il est supprimé
        {
            # Incrémente l'alerte "N° de commande à fournir [agence]"
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ORDERNUMTOPROVIDE_AGENCY, +1);
            # Incrémente l'alerte "N° de commande à fournir [siège]"
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ORDERNUMTOPROVIDE_ALL, +1);
        }
        elsif ($tabInfos->{'orderNo'} eq '') # Si il est saisi
        {
            # Décrémente l'alerte "N° de commande à fournir [agence]"
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ORDERNUMTOPROVIDE_AGENCY, -1);
            # Décrémente l'alerte "N° de commande à fournir [siège]"
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ORDERNUMTOPROVIDE_ALL, -1);
        }
    }



    # Paramètres pour la mise à jour du tarif de location
    my $tabRentTariffDatas = $tabData->{'tabRentTariff'};
    # Optimisations location
    $tabRentTariffDatas->{'contract.customer.id'} = $customerId;
    $tabRentTariffDatas->{'contract.agency.id'}   = $agencyId;
    $tabRentTariffDatas->{'contract.model.id'}    = $newModelId;
    $tabRentTariffDatas->{'contract.linkedEstimateLine.id'} = 0;
    $tabRentTariffDatas->{'rights'}               = $tabRights->{'rentTariff'};
    $tabRentTariffDatas->{'editMode'}             = 'update';
    # Vérification du nombre de jours (en cas)
    if (exists $tabRentTariffDatas->{'days'} && $tabRentTariffDatas->{'days'} != $newDuration)
    {
        return &$rollBackAndError(__LINE__, 0x0123);
    }
    $tabRentTariffDatas->{'days'} = $newDuration;

    # Paramètres pour la mise à jour du tarif transport
    my $tabTrspTariffDatas = $tabData->{'tabTransport'}->{'tabTariff'};
    # Optimisations transport
    $tabTrspTariffDatas->{'contract.customer.id'} = $customerId;
    $tabTrspTariffDatas->{'contract.agency.id'}   = $agencyId;
    $tabTrspTariffDatas->{'contract.zone'}        = $newZone;
    $tabTrspTariffDatas->{'contract.linkedEstimateLine.id'} = 0;
    $tabTrspTariffDatas->{'rights'}               = $tabRights->{'transport'};
    $tabTrspTariffDatas->{'editMode'}             = 'update';

    # Paramètres pour l'enregistrement de la proforma
    my $tabProformaAction = $tabData->{'tabProformaAction'};
    if ($tabProformaAction)
    {
        my $type = $tabProformaAction->{'type'};
        # - Vérification des droits pour la proforma
        if (!&_hasRight($tabRights->{'proforma'}, $type))
        {
            return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
        }
    }

    # Paramètres pour l'enregistrement des documents
    my $tabDocumentsActions = $tabData->{'tabDocumentsActions'};

    # Indique si la tarification transport est activé ou non ?
    my $isTrspTarification = &LOC::Characteristic::getAgencyValueByCode('TRSPTARIF', $agencyId);

    # Mise à jour du chantier
    my $tabSiteData = (exists $tabData->{'tabSite'} ? $tabData->{'tabSite'} : {});

    $tabSiteData->{'documentType'} = 'contract';
    $tabSiteData->{'documentId'}   = $contractId;

    $tabSiteData->{'rights'}       = $tabRights->{'site'};
    $tabSiteData->{'contract.id'}  = $contractId;
    
    $tabSiteData->{'oldZone'} = $oldZone;
    $tabSiteData->{'newZone'} = $newZone;
    my $tabSiteErrors = $tabErrors->{'modules'}->{'site'};
    if (!&LOC::Site::update($countryId, $siteId, $tabSiteData, $userId, $tabSiteErrors, $tabEndUpds))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_SITE);
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabSiteErrors->{'fatal'}))
        {
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
        # Modifications interdites ?
        if (&LOC::Util::in_array(0x0003, $tabSiteErrors->{'fatal'}))
        {
            return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
        }
    }
    if (@{$tabSiteErrors->{'warning'}} > 0)
    {
        push(@{$tabErrors->{'warning'}}, ERROR_MODULE_SITE);
    }

    # Zone
    if ($isTrspTarification && ($newZone < 1 || $newZone > 6))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0103);
    }

    # Changement de modèle de machine
    if ($isModelChanged)
    {
        # Mettre à jour les stocks de l'ancien modèle de machine
        &LOC::EndUpdates::updateModelStock($tabEndUpds, $agencyId, $oldModelId, 'nbBooked', -1);

        # Mettre à jour les stocks du nouveau modèle de machine
        &LOC::EndUpdates::updateModelStock($tabEndUpds, $agencyId, $newModelId, 'nbBooked', +1);
    }


    # Enregistrement dans la base de données
    my $tabUpdates = {
        'PEAUTO_INTERLOC'    => $negotiatorId,

        # Pavé "Client"
        'CONTRATNUMCOMM'       => substr($orderNo, 0, 25),
        'CONTRATCONTACTCOMM'   => $orderContactFullName,
        'CONTRATTELEPHONECOMM' => $orderContactTelephone,
        'CONTRATEMAILCOMM'     => $orderContactEmail,

        # Pavé "Chantier"
        'ZONE'               => $newZone,

        # Onglet "Matériel"
        'MOMAAUTO'            => $newModelId,
        'FAMAAUTO*'           => '(SELECT FAMAAUTO FROM AUTH.MODELEMACHINE WHERE MOMAAUTO = ' . $newModelId . ')',
        'CONTRATMODIMP'       => ($isImperativeModel ? -1 : 0),
        'CONTRATJOINDOCUMENT' => ($isJoinDoc ? -1 : 0),
        'MAAUTO_SOUHAITE'     => ($newWishedMachineId ? $newWishedMachineId : undef),

        # Onglet "Durée"
        'CONTRATJOURSPLUS'   => $newAdditionalDaysUnbilled,
        'CONTRATJOURSMOINS'  => $newDeductedDaysUnbilled,
        'CONTRATJPCOMM'      => $newAdditionalDaysComment,
        'CONTRATJMCOMM'      => $newDeductedDaysComment,
        'CONTRATDUREE'       => $newDuration,
        'CONTRATDD'          => $newBeginDate,
        'CONTRATDR'          => $newEndDate,

        # Onglet "Autres services"
        'CONTRATTYPEASSURANCE' => $newInsuranceTypeId,
        'CONTRATPASSURANCE'  => $newInsuranceRate,

        # Pavé "Informations complémentaires"
        'CONTRATCOMMENTAIRE' => ($comments eq '' ? undef : $comments),
        'CONTRATNOTE'        => ($tracking eq '' ? undef : $tracking),


        # Informations internes
        'CONTRATINDICE*'     => 'CONTRATINDICE + 1', # Indice de modification du contrat
        'CONTRATDATEMODIF*'  => 'Now()'              # Force la modification de la date de modif
    };
    if (@{$tabErrors->{'fatal'}} == 0 && $db->update('CONTRAT', $tabUpdates, {'CONTRATAUTO*' => $contractId}, $schemaName) == -1)
    {
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }


    # Enregistrement du tarif de location
    my $tabRentTariffErrors = $tabErrors->{'modules'}->{'rentTariff'};
    if (!&LOC::Tariff::Rent::save($countryId, 'contract', $contractId, $tabRentTariffDatas, $userId, $tabRentTariffErrors, $tabEndUpds))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_TARIFFRENT);
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabRentTariffErrors->{'fatal'}))
        {
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
        # Modifications interdites ?
        if (&LOC::Util::in_array(0x0003, $tabRentTariffErrors->{'fatal'}))
        {
            return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
        }
    }

    # Enregistrement de l'estimation transport
    my $tabTrspTariffErrors = $tabErrors->{'modules'}->{'transportTariff'};
    if (!&LOC::Tariff::Transport::save($countryId, 'contract', $contractId, $tabTrspTariffDatas, $userId, $tabTrspTariffErrors, $tabEndUpds))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_TARIFFTRSP);
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabTrspTariffErrors->{'fatal'}))
        {
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
        # Modifications interdites ?
        if (&LOC::Util::in_array(0x0003, $tabTrspTariffErrors->{'fatal'}))
        {
            return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
        }
    }

    # Mise à jour des jours+/jours-
    if (@{$tabErrors->{'fatal'}} == 0)
    {
        my $tabDaysErrors = $tabErrors->{'modules'}->{'days'};
        if ($tabDaysActions && !&_execDaysActions($countryId, $contractId, $tabDaysActions, $userId, {}, $tabDaysErrors, $tabEndUpds))
        {
            push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_DAYS);

            # Erreur fatale ?
            if (&LOC::Util::in_array(0x0001, $tabDaysErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            }
        }
    }

    # Mise à jour du carburant
    if (@{$tabErrors->{'fatal'}} == 0)
    {
        &_updateFuel($db, $countryId, $contractId, $fuelQuantity,
                     &_hasRight($tabRights->{'services'}, 'fuel'), $userId, $tabErrors, $tabEndUpds);
    }

    # Mise à jour du nettoyage
    if (@{$tabErrors->{'fatal'}} == 0)
    {
        &_updateCleaning($db, $countryId, $contractId, $cleaningTypeId, $cleaningAmount, $isModelChanged,
                         &_hasRight($tabRights->{'services'}, 'cleaning'), $userId, $tabErrors, $tabEndUpds);
    }

    # Mise à jour de la participation au recyclage
    if (@{$tabErrors->{'fatal'}} == 0)
    {
        &_updateResidues($db, $countryId, $contractId, $residuesMode, $isModelChanged,
                         &_hasRight($tabRights->{'services'}, 'residues'), $userId, $tabErrors, $tabEndUpds);
    }

    # Mise à jour des jours de remise en état
    if (@{$tabErrors->{'fatal'}} == 0)
    {
        &_updateRepairDays($db, $countryId, $contractId, $tabRepairDays,
                           &_hasRight($tabRights->{'repair'}, 'invoiceDays'), $userId, $tabErrors, $tabEndUpds);
    }

    # Mise à jour des services de remise en état
    if (@{$tabErrors->{'fatal'}} == 0)
    {
        &_updateRepairServices($db, $countryId, $contractId, $tabRepairServices,
                               &_hasRight($tabRights->{'repair'}, 'invoiceServices'), $userId, $tabErrors, $tabEndUpds)
    }

    # Remise en cours des fiches de remise en état
    if (@{$tabErrors->{'fatal'}} == 0)
    {
        &_reopenRepairSheets($db, $countryId, $contractId, $tabRepairSheetsToReopen,
                             &_hasRight($tabRights->{'repair'}, 'reopenSheet'), $userId, $tabErrors, $tabEndUpds);
    }

    # Mise à jour des services de location et de transport
    if (@{$tabErrors->{'fatal'}} == 0)
    {
        &_updateRentServices($db, $countryId, $contractId, $tabRentServices, $rentServiceMinAmount,
                             &_hasRight($tabRights->{'services'}, 'addRentServices'),
                             &_hasRight($tabRights->{'services'}, 'editRentServices'), $userId, $tabErrors, $tabEndUpds);
    }

    # Mise à jour de la gestion de recours
    if (@{$tabErrors->{'fatal'}} == 0)
    {
        &_updateAppeal($db, $countryId, $contractId, $newAppealTypeId,
                         &_hasRight($tabRights->{'services'}, 'appeal'), $userId, $tabErrors, $tabEndUpds);
    }

    # - Modifications impactant les proforma existantes
    if (@{$tabErrors->{'fatal'}} == 0 && &_expireProformas($countryId, $contractId, $userId, $updateFrom, $tabEndUpds) == 0)
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
    }

    # Mise à jour du montant total et de l'assurance
    if (@{$tabErrors->{'fatal'}} == 0 && !&_updateAmounts($db, $countryId, $contractId))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
    }

    # Mise à jour de l'attribution de la machine sur le contrat
    if (@{$tabErrors->{'fatal'}} == 0)
    {
        &_updateMachineAllocation($db, $countryId, $contractId, $machineId, $isFinalAllocation, $isModelChanged,
                                  &_hasRight($tabRights->{'machine'}, 'machine'), $userId, $tabErrors, $tabEndUpds);
    }

    # Enregistrement des infos proforma
    if (@{$tabErrors->{'fatal'}} == 0 && $tabProformaAction)
    {
        my $tabProformaErrors = $tabErrors->{'modules'}->{'proforma'};
        if (!&_execProformaAction($countryId, $contractId, $tabProformaAction, $userId, $tabProformaErrors, $tabEndUpds))
        {
            push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_PROFORMA);

            # Erreur fatale ?
            if (&LOC::Util::in_array(0x0001, $tabProformaErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            }
        }
    }

    # Enregistrement des documents
    if (@{$tabErrors->{'fatal'}} == 0 && $tabDocumentsActions)
    {
        my $tabDocumentsErrors = $tabErrors->{'modules'}->{'documents'};
        if (!&_execDocumentsActions($countryId, $contractId, $tabDocumentsActions, $userId, $tabDocumentsErrors, $tabEndUpds))
        {
            push(@{$tabErrors->{'warning'}}, ERROR_MODULE_DOCUMENTS);
        }
    }

    # Indique s'il faut verrouiller les transports
    my $isLockTransport = 0;

    # Arrêt, remise en cours, mise en facturation, remise en arrêt et annulation du contrat
    if (@{$tabErrors->{'fatal'}} == 0 && $subAction ne 'valid')
    {
        my $result = 0;
        my $errorCode;
        if ($subAction eq 'stop')
        {
            $errorCode = 0x0200;
            $result = &_stop($db, $countryId, $contractId, $userId, $tabErrors, $tabEndUpds);
            $tabChanges->{'state.id'} = {'old' => $stateId, 'new' => STATE_STOPPED};
        }
        elsif ($subAction eq 'reactivate')
        {
            $errorCode = 0x0201;
            $result = &_reactivate($db, $countryId, $contractId, $userId, $tabErrors, $tabEndUpds);
            $tabChanges->{'state.id'} = {'old' => $stateId, 'new' => STATE_CONTRACT};
        }
        elsif ($subAction eq 'invoice')
        {
            $errorCode = 0x0202;
            $result = &_invoice($db, $countryId, $contractId, $userId, $tabErrors, $tabEndUpds);
            $tabChanges->{'state.id'} = {'old' => $stateId, 'new' => STATE_BILLED};
        }
        elsif ($subAction eq 'restop')
        {
            $errorCode = 0x0203;
            $result = &_restop($db, $countryId, $contractId, $userId, $tabErrors, $tabEndUpds);
            $tabChanges->{'state.id'} = {'old' => $stateId, 'new' => STATE_STOPPED};
        }
        elsif ($subAction eq 'cancel')
        {
            $errorCode = 0x0204;
            $result = &_cancel($db, $countryId, $contractId, $userId, $tabErrors, $tabEndUpds);
            $tabChanges->{'state.id'} = {'old' => $stateId, 'new' => STATE_CANCELED};
            # Verrouillage des transports pour les contrats annulés
            if ($result)
            {
                $isLockTransport = 1;
            }
        }

        if (!$result)
        {
            push(@{$tabErrors->{'fatal'}}, $errorCode);
        }
    }


    # Enregistrement du transport si le contrat n'est pas annulée
    my $tabTransportErrors = $tabErrors->{'modules'}->{'transport'};
    my $tabDeliveryErrors = $tabTransportErrors->{'modules'}->{'delivery'};
    $tabData->{'tabTransport'}->{'tabDelivery'}->{'rights'} = $tabRights->{'transport'};
    my $tabTransportOptions = {
        'tabDocumentChanges' => $tabChanges
    };
    if (@{$tabErrors->{'fatal'}} == 0 && !&LOC::Transport::setDocumentDelivery($countryId, 'contract', $contractId, $tabData->{'tabTransport'}->{'tabDelivery'}->{'data'}, $userId, $tabTransportOptions, $tabDeliveryErrors, $tabEndUpds))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_TRANSPORT);
        push(@{$tabTransportErrors->{'fatal'}}, ERROR_MODULE_TRANSPORT_DELIVERY);

        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabDeliveryErrors->{'fatal'}))
        {
            push(@{$tabTransportErrors->{'fatal'}}, ERROR_FATAL);
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        }
        # Modifications interdites ?
        if (&LOC::Util::in_array(0x0003, $tabDeliveryErrors->{'fatal'}))
        {
            push(@{$tabTransportErrors->{'fatal'}}, ERROR_RIGHTS);
            push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
        }
    }

    my $tabRecoveryErrors = $tabTransportErrors->{'modules'}->{'recovery'};
    $tabData->{'tabTransport'}->{'tabRecovery'}->{'rights'} = $tabRights->{'transport'};
    my $tabTransportOptions = {
        'tabDocumentChanges' => $tabChanges
    };
    if (@{$tabErrors->{'fatal'}} == 0 && !&LOC::Transport::setDocumentRecovery($countryId, 'contract', $contractId, $tabData->{'tabTransport'}->{'tabRecovery'}->{'data'}, $userId, $tabTransportOptions, $tabRecoveryErrors, $tabEndUpds))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_TRANSPORT);
        push(@{$tabTransportErrors->{'fatal'}}, ERROR_MODULE_TRANSPORT_RECOVERY);

        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabRecoveryErrors->{'fatal'}))
        {
            push(@{$tabTransportErrors->{'fatal'}}, ERROR_FATAL);
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        }
        # Modifications interdites ?
        if (&LOC::Util::in_array(0x0003, $tabRecoveryErrors->{'fatal'}))
        {
            push(@{$tabTransportErrors->{'fatal'}}, ERROR_RIGHTS);
            push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
        }
    }

    # Enregistrement de la réalisation/annulation coté Livraison
    my $tabDeliveryAction = $tabData->{'tabTransport'}->{'tabDelivery'}->{'action'};
    if (@{$tabErrors->{'fatal'}} == 0 && $tabDeliveryAction)
    {
        # Vérification des droits pour la réalisation/annulation d'une récupération
        my $rightType = ($tabData->{'tabTransport'}->{'tabDelivery'}->{'data'}->{'isSelf'} ? 'self' : 'std');

        # Vérification des droits pour la réalisation/annulation d'une livraison
        if (!&_hasRight($tabRights->{'transport'}, $tabDeliveryAction->{'type'} . '-' . $rightType))
        {
            # Modifications interdites
            push(@{$tabTransportErrors->{'fatal'}}, ERROR_RIGHTS);
            push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
        }
        elsif (!&_execTransportAction($countryId, 'contract', $contractId, LOC::Transport::SIDE_DELIVERY, $tabDeliveryAction, $userId, $tabDeliveryErrors, $tabEndUpds))
        {
            # Erreur transport
            push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_TRANSPORT);
            push(@{$tabTransportErrors->{'fatal'}}, ERROR_MODULE_TRANSPORT_DELIVERY);

            # Erreur fatale ?
            if (&LOC::Util::in_array(0x0001, $tabDeliveryErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                push(@{$tabTransportErrors->{'fatal'}}, ERROR_FATAL);
            }

            # Réalisation impossible
            if (&LOC::Util::in_array(0x0106, $tabDeliveryErrors->{'fatal'}))
            {
                push(@{$tabTransportErrors->{'fatal'}}, 0x0106);
            }

            # Annulation impossible
            if (&LOC::Util::in_array(0x0107, $tabDeliveryErrors->{'fatal'}))
            {
                push(@{$tabTransportErrors->{'fatal'}}, 0x0107);
            }

        }
    }

    # Enregistrement de la réalisation/annulation coté Récupération
    my $tabRecoveryAction = $tabData->{'tabTransport'}->{'tabRecovery'}->{'action'};
    if (@{$tabErrors->{'fatal'}} == 0 && $tabRecoveryAction)
    {
        # Vérification des droits pour la réalisation/annulation d'une récupération
        my $rightType = ($tabData->{'tabTransport'}->{'tabRecovery'}->{'data'}->{'isSelf'} ? 'self' : 'std');

        if (!&_hasRight($tabRights->{'transport'}, $tabRecoveryAction->{'type'} . '-' . $rightType))
        {
            # Modifications interdites
            push(@{$tabTransportErrors->{'fatal'}}, ERROR_RIGHTS);
            push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
        }
        elsif (!&_execTransportAction($countryId, 'contract', $contractId, LOC::Transport::SIDE_RECOVERY, $tabRecoveryAction, $userId, $tabRecoveryErrors, $tabEndUpds))
        {
            # Erreur transport
            push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_TRANSPORT);
            push(@{$tabTransportErrors->{'fatal'}}, ERROR_MODULE_TRANSPORT_RECOVERY);

            # Erreur fatale ?
            if (&LOC::Util::in_array(0x0001, $tabRecoveryErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                push(@{$tabTransportErrors->{'fatal'}}, ERROR_FATAL);
            }

            # Réalisationimpossible
            if (&LOC::Util::in_array(0x0106, $tabRecoveryErrors->{'fatal'}))
            {
                push(@{$tabTransportErrors->{'fatal'}}, 0x0106);
            }

            # Annulation impossible
            if (&LOC::Util::in_array(0x0107, $tabRecoveryErrors->{'fatal'}))
            {
                push(@{$tabTransportErrors->{'fatal'}}, 0x0107);
            }

        }
    }

    # Verrouillage des transports si le contrat est annulé
    if (@{$tabErrors->{'fatal'}} == 0 && $isLockTransport)
    {
        &LOC::Transport::lock($countryId, {'from-to' => {'type' => LOC::Transport::FROMTOTYPE_CONTRACT,
                                                         'id'   => $contractId}}, $userId);
    }

    # Mise à jour des codes de démarrage
    my $tabStartCodeErrors = $tabErrors->{'modules'}->{'startCode'};
    if (@{$tabErrors->{'fatal'}} == 0 && !&LOC::StartCode::Contract::Rent::save($countryId, $contractId, $userId, {}, $tabStartCodeErrors, $tabEndUpds))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_STARTCODE);
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabStartCodeErrors->{'fatal'}))
        {
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Reblocage du client s'il était débloqué temporairement
    if (@{$tabErrors->{'fatal'}} == 0 && $hasLockableUpdates > 0 && !&_lockCustomerAfterUpdate($countryId, $tabCustomerInfos, $userId, $tabEndUpds, 'update'))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
    }


    # Si il y a des données invalides alors on arrête ici
    if (@{$tabErrors->{'fatal'}} > 0)
    {
        return &$rollBackAndError(__LINE__);
    }

    if (keys %$tabChanges > 0)
    {
        # Changements pouvant impacter le transport ? => on le lui communique
        &LOC::Transport::onExternalChanges($countryId, {
            'contract' => {
                'id'    => $contractId,
                'event' => 'update',
                'props' => $tabChanges
            }
        }, $userId, undef, $tabEndUpds);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            return &$rollBackAndError(__LINE__);
        }
    }


    # Mise à jour systématique de kimoce (caractèristiques)
    if ($machineId)
    {
        my $tabMachineInfos = &LOC::Machine::getInfos($countryId, $machineId);
        &LOC::EndUpdates::updateKimoceMachine($tabEndUpds, $tabMachineInfos->{'parkNumber'}, LOC::EndUpdates::KIMUPD_FEATURES);
    }


    # Exécution des mises à jour de fin
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: updateAmounts
# Mise à jour du montant total sur un contrat
#
# Parameters:
# LOC::Db::Adapter $db         - Connexion à la base de données
# string           $countryId  - Pays
# int              $contractId - Id du contrat
# hashref          $tabErrors  - Tableau des erreurs
#
# Returns:
# bool -
sub updateAmounts
{
    my ($countryId, $contractId, $tabErrors) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'updateAmounts', $_[0], __FILE__, '');

        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }

        return 0;
    };


    # Récupération des informations du contrat
    my $tabInfos = &getInfos($countryId, $contractId, GETINFOS_BASE);
    if (!$tabInfos)
    {
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }
    # Vérification du blocage en écriture
    if ($tabInfos->{'isWriteLocked'})
    {
        return &$rollBackAndError(__LINE__, ERROR_WRITELOCKED);
    }


    # Mise à jour des montants du contrat
    my $result = &_updateAmounts($db, $countryId, $contractId);
    if (!$result)
    {
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            return &$rollBackAndError(__LINE__);
        }
    }

    return 1;
}


# Function: updateFuel
# Mise à jour du carburant
#
# Parameters:
# string   $countryId      - Pays
# int      $contractId     - Id du contrat
# int      $quantity       - Quantité de carburant
# int      $userId         - Utilisateur responsable de la modification
# bool     $isRightChecked - Vérifier les droits sur le contrat ? (oui par défaut)
# hashref  $tabErrors      - Tableau des erreurs
# hashref  $tabEndUpds     - Mises à jour de fin
#
# Returns:
# bool -
sub updateFuel
{
    my ($countryId, $contractId, $quantity, $userId, $isRightChecked, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Les droits sur le contrat sont vérifiés par défaut
    if (!defined $isRightChecked)
    {
        $isRightChecked = 1;
    }

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'updateFuel', $_[0], __FILE__, '');

        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }

        return 0;
    };


    # Récupération des informations du contrat
    my $tabInfos = &getInfos($countryId, $contractId, GETINFOS_BASE);
    if (!$tabInfos)
    {
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }
    # Vérification du blocage en écriture
    if ($tabInfos->{'isWriteLocked'})
    {
        return &$rollBackAndError(__LINE__, ERROR_WRITELOCKED);
    }

    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Récupération des droits
    my $hasRight = 1;
    if ($isRightChecked)
    {
        my $tabRights = &_getRights($tabInfos, $tabUserInfos);
        $hasRight = &_hasRight($tabRights->{'services'}, 'fuel');
    }


    # Mise à jour du carburant
    my $result = &_updateFuel($db, $countryId, $contractId, $quantity, $hasRight, $userId, $tabErrors, $tabEndUpds);
    if (!$result)
    {
        return &$rollBackAndError(__LINE__);
    }

    # Mise à jour des montants du contrat
    my $result = &_updateAmounts($db, $countryId, $contractId);
    if (!$result)
    {
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            return &$rollBackAndError(__LINE__);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: updateProformaFlags
# Mise à jour des flags pro forma
#
# Parameters:
# string  $countryId  - Pays
# int     $contractId - Id du contrat
# int     $flags      - Flags pro formas à mettre à jour
#
# Returns:
# bool -
sub updateProformaFlags
{
    my ($countryId, $contractId, $flags) = @_;


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'updateProformaFlags', $_[0], __FILE__, '');

        return 0;
    };


    # Lecture des flags pro formas sur le contrat
    my $query = '
SELECT
    tbl_f_contract.CONTRATPROFORMAOK
FROM ' . $schemaName . '.CONTRAT tbl_f_contract
WHERE tbl_f_contract.CONTRATAUTO = ' . $contractId . ';';
    my $oldFlags = $db->fetchOne($query) * 1;

    # Calcul des flags
    my $newFlags = ($oldFlags & 255) | $flags;


    # Si les flags ont changé :
    if ($newFlags != $oldFlags)
    {
        # Mise à jour
        if ($db->update('CONTRAT', {'CONTRATPROFORMAOK*' => '(CONTRATPROFORMAOK & 255) | ' . $flags},
                                   {'CONTRATAUTO*' => $contractId}, $schemaName) == -1)
        {
            return &$rollBackAndError(__LINE__);
        }

        # Changements pouvant impacter le transport ? => on le lui communique
        &LOC::Transport::onExternalChanges($countryId, {
            'contract' => {
                'id'    => $contractId,
                'event' => 'update',
                'props' => {
                    'proformaFlags' => {'old' => $oldFlags, 'new' => $newFlags}
                }
            }
        }, undef, undef, undef);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            return &$rollBackAndError(__LINE__);
        }
    }

    return 1;
}

# Function: _execDaysActions
# Exécuter l'action sur les jours+/- du contrat
#
# Parameters:
# string  $countryId      - Pays
# int     $contractId     - Identifiant du contrat
# arrayref $tabActions     - Paramètre des actions
# int     $userId         - Id de l'utilisateur
# hashref $tabErrors      - Tableau des erreurs
# hashref $tabEndUpds     - Mises à jour de fin
#
# Returns:
# bool -
sub _execDaysActions
{
    my ($countryId, $contractId, $tabActions, $userId, $tabOptions, $tabDaysErrors, $tabEndUpds) = @_;

    foreach my $tabAction (@$tabActions)
    {
        if ($tabAction->{'type'} eq 'add')
        {
            if (!&LOC::Day::insert($countryId, 'contract', $contractId, $tabAction->{'data'}, $userId, $tabOptions, $tabDaysErrors, $tabEndUpds))
            {
                return 0;
            }
        }
        elsif ($tabAction->{'type'} eq 'edit')
        {
            if (!&LOC::Day::update($countryId, $tabAction->{'id'}, $tabAction->{'data'}, $userId, $tabOptions, $tabDaysErrors, $tabEndUpds))
            {
                return 0;
            }
        }
        elsif ($tabAction->{'type'} eq 'remove')
        {
            if (!&LOC::Day::delete($countryId, $tabAction->{'id'}, $userId, $tabOptions, $tabDaysErrors, $tabEndUpds))
            {
                return 0;
            }
        }
        elsif ($tabAction->{'type'} eq 'setNonInvoiceable')
        {
            if (!&LOC::Day::setInvoiceable($countryId, $tabAction->{'id'}, 0, $userId, $tabOptions, $tabDaysErrors, $tabEndUpds))
            {
                return 0;
            }
        }
        elsif ($tabAction->{'type'} eq 'setInvoiceable')
        {
            if (!&LOC::Day::setInvoiceable($countryId, $tabAction->{'id'}, 1, $userId, $tabOptions, $tabDaysErrors, $tabEndUpds))
            {
                return 0;
            }
        }
    }

    return 1;
}



# Function: _execDocumentsActions
# Exécuter les actions sur les documents du contrat
#
# Parameters:
# string  $countryId      - Pays
# int     $contractId     - Identifiant du contrat
# arrayref $tabActions     - Paramètre des actions
# int     $userId         - Id de l'utilisateur
# hashref $tabErrors      - Tableau des erreurs
# hashref $tabEndUpds     - Mises à jour de fin
#
# Returns:
# bool -
sub _execDocumentsActions
{
    my ($countryId, $contractId, $tabActions, $userId, $tabActionsErrors, $tabEndUpds) = @_;

    my $result = 1;

    foreach my $tabAction (@$tabActions)
    {
        my ($docType, $docId) = split(':', $tabAction->{'id'});
        my $tabErrors = {};
        my @tabFatalErrors = ();

        # Actions sur les "attachments"
        if ($tabAction->{'type'} eq 'remove')
        {
            if ($docType eq 'att')
            {
                if (!&LOC::Attachments::delete($countryId, $docId,
                                               $userId, {}, $tabErrors, $tabEndUpds))
                {
                    $result = 0;
                }
            }
            else
            {
                $result = 0;
                push(@tabFatalErrors, ERROR_FATAL);
            }
        }
        elsif ($tabAction->{'type'} eq 'edit')
        {
            if ($docType eq 'att')
            {
                my $tabData = {
                    'description' => $tabAction->{'data'}->{'description'},
                    'title'       => $tabAction->{'data'}->{'title'}
                };
                if (!&LOC::Attachments::update($countryId, $docId,
                                               $tabData,
                                               $userId, {}, $tabErrors, $tabEndUpds))
                {
                    $result = 0;
                }
            }
            else
            {
                $result = 0;
                push(@tabFatalErrors, ERROR_FATAL);
            }
        }
        elsif ($tabAction->{'type'} eq 'add')
        {
            my $tabData = {
                'fileName'    => $tabAction->{'data'}->{'fileName'},
                'description' => $tabAction->{'data'}->{'description'},
                'title'       => $tabAction->{'data'}->{'title'},
                'filePath'    => &LOC::Util::getCommonFilesPath('temp/') . $tabAction->{'data'}->{'tempName'}
            };
            if (!&LOC::Attachments::insert($countryId, LOC::Attachments::MODULE_RENTCONTRACTDOCUMENT,
                                           $contractId, $tabData,
                                           $userId, {}, $tabErrors, $tabEndUpds))
            {
                $result = 0;
            }
            elsif (-e $tabData->{'filePath'})
            {
                # Suppression du fichier temporaire
                unlink($tabData->{'filePath'});
            }
        }

        # Toutes les erreurs des documents sont gérées comme des warning pour le contrat
        push(@$tabActionsErrors, {
            'fatal'   => \@tabFatalErrors,
            'warning' => $tabErrors->{'fatal'}
        });
    }

    return $result;
}


# Function: _execProformaAction
# Exécuter l'action sur les pro formas du contrat
#
# Parameters:
# string  $countryId      - Pays
# int     $contractId     - Identifiant du contrat
# hashref $tabAction      - Paramètre de l'action
# int     $userId         - Id de l'utilisateur
# hashref $tabErrors      - Tableau des erreurs
# hashref $tabEndUpds     - Mises à jour de fin
#
# Returns:
# bool -
sub _execProformaAction
{
    my ($countryId, $contractId, $tabAction, $userId, $tabErrors, $tabEndUpds) = @_;

    my $result = 0;
    if ($tabAction->{'type'} eq 'add')
    {
        $result = &LOC::Proforma::add($countryId, 'contract', $contractId, $tabAction->{'tab'}, $userId, $tabErrors, $tabEndUpds);
    }
    elsif ($tabAction->{'type'} eq 'edit')
    {
        $result = &LOC::Proforma::edit($countryId, $tabAction->{'id'}, $tabAction->{'tab'}, $userId, $tabErrors, $tabEndUpds);
    }
    elsif ($tabAction->{'type'} eq 'reactivate')
    {
        $result = &LOC::Proforma::reactivate($countryId, $tabAction->{'id'}, $userId, $tabErrors, $tabEndUpds);
    }
    elsif ($tabAction->{'type'} eq 'abort')
    {
        $result = &LOC::Proforma::abort($countryId, $tabAction->{'id'}, $userId, $tabErrors, $tabEndUpds);
    }
    return $result;
}

# Function: _expireProformas
# Vérifie s'il faut passer des pro formas en non à jour si des changements impactant sont intervenus sur le contrat
#
# Parameters:
#
# string           $countryId  - Pays
# int              $contractId - Id du contrat
# int              $userId     - Id de l'utilisateur
# string           $updateFrom - Origine de la mise à jour
# hashref          $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool -
sub _expireProformas
{
    my ($countryId, $contractId, $userId, $updateFrom, $tabEndUpds) = @_;

    # Liste des traces
    my $tabTraces = &LOC::EndUpdates::getTraces($tabEndUpds, 'contract', $contractId);

    my $tabImpactAmount       = [
        # Jours supplémentaires
        LOC::Log::EventType::ADDADDEDDAYS,
        LOC::Log::EventType::UPDADDEDDAY,
        LOC::Log::EventType::DELADDEDDAY,
        LOC::Log::EventType::INVOICEADDEDDAY,
        LOC::Log::EventType::UNINVOICEADDEDDAY,
        # Jours déduits
        LOC::Log::EventType::ADDDEDUCTEDDAYS,
        LOC::Log::EventType::UPDDEDUCTEDDAY,
        LOC::Log::EventType::DELDEDUCTEDDAY,
        LOC::Log::EventType::INVOICEDEDUCTEDDAY,
        LOC::Log::EventType::UNINVOICEDEDUCTEDDAY,
        # Durée
        LOC::Log::EventType::UPDDURATION,
        LOC::Log::EventType::UPDCALENDARDURATION,
        # Prix de location
        LOC::Log::EventType::UPDRENTAMOUNT,
        # Transport
        LOC::Log::EventType::UPDDELIVERYAMOUNT,
        LOC::Log::EventType::UPDRECOVERYAMOUNT,
        # Autres services
        LOC::Log::EventType::UPDINSURANCE,
        LOC::Log::EventType::UPDCLEANING,
        LOC::Log::EventType::UPDRESIDUESMODE,
        LOC::Log::EventType::UPDAPPEAL
    ];
    my $tabImpactRentServices = [
        LOC::Log::EventType::ADDRENTSERVICE,
        LOC::Log::EventType::DELRENTSERVICE,
        LOC::Log::EventType::UPDRENTSERVICE
    ];

    # Liste des groupes modifiables sans expiration
    my $tabGroups = [
        {
            'name' => 'modelOrPeriod',
            'traces' => [
                LOC::Log::EventType::CHANGEMODEL,
                LOC::Log::EventType::UPDBEGINDATE,
                LOC::Log::EventType::UPDENDDATE
            ],
            'count' => 0
        },
        {
            'name' => 'rentServices',
            'traces' => $tabImpactRentServices,
            'count' => 0
        }
    ];


    my $countImpactAmount = 0;
    my $diffRentServices  = 0;
    foreach my $lineUpd (@{$tabTraces})
    {
        # Impacts sur les montants autres que les services complémentaires
        if (&LOC::Util::in_array($lineUpd->{'code'}, $tabImpactAmount))
        {
            # Pas de modif de montant de location
            if ($lineUpd->{'code'} eq LOC::Log::EventType::UPDRENTAMOUNT && $lineUpd->{'old'}->{'amount'} == $lineUpd->{'new'}->{'amount'})
            {
                next;
            }
            # Modification d'un jour +/- sans modification de la durée du jour
            if (&LOC::Util::in_array($lineUpd->{'code'}, [LOC::Log::EventType::UPDADDEDDAY, LOC::Log::EventType::UPDDEDUCTEDDAY]))
            {
                my $oldInvoiceDuration = &LOC::Day::InvoiceDuration::getInfos($countryId, $lineUpd->{'old'}->{'invoiceDurationId'});
                my $newInvoiceDuration = &LOC::Day::InvoiceDuration::getInfos($countryId, $lineUpd->{'new'}->{'invoiceDurationId'});

                if ($oldInvoiceDuration == $newInvoiceDuration)
                {
                    next;
                }
            }
            # Ajout d'un jour + non facturé
            if ($lineUpd->{'code'} eq LOC::Log::EventType::ADDADDEDDAYS && !$lineUpd->{'new'}->{'isInvoiceable'})
            {
                next;
            }
            $countImpactAmount++;
        }
        # Impacts sur le montant des services complémentaires
        if (&LOC::Util::in_array($lineUpd->{'code'}, $tabImpactRentServices))
        {
            $diffRentServices += (defined $lineUpd->{'new'} ? $lineUpd->{'new'}->{'amount'} : 0) -
                                 (defined $lineUpd->{'old'} ? $lineUpd->{'old'}->{'amount'} : 0);
        }

        # Impacts sur les différents groupes
        foreach my $tabGroup (@$tabGroups)
        {
            if (&LOC::Util::in_array($lineUpd->{'code'}, $tabGroup->{'traces'}))
            {
                $tabGroup->{'count'}++;
            }
        }
    }

    # changement de modèle/durée sans changement de tarif et sans aucun autre changement
    if ($countImpactAmount == 0 && $diffRentServices == 0)
    {
        # Mise à jour des groupes détectés
        my $tabGroupsToUpdate = [];
        foreach my $tabGroup (@$tabGroups)
        {
            if ($tabGroup->{'count'} > 0)
            {
                push(@$tabGroupsToUpdate, $tabGroup->{'name'});
            }
        }

        my $tabFilters = {
            'stateId'        => [LOC::Proforma::STATE_ACTIVE, LOC::Proforma::STATE_ABORTED],
            'isExpired'      => 0
        };
        if (@$tabGroupsToUpdate > 0 && !&LOC::Proforma::updateGroupLines($countryId, 'contract', $contractId, $tabFilters, $tabGroupsToUpdate, {}))
        {
            return 0;
        }
    }
    elsif ($updateFrom ne 'validatePF')
    {
        if (!&LOC::Proforma::expireAll($countryId, 'contract', $contractId, $userId, {}, $tabEndUpds))
        {
            return 0;
        }
    }

    return 1;
}

# Function: _getRights
# Récupérer les droits sur un contrat pour un utilisateur donné
#
# Parameters:
# hashref $tabContractInfos - Informations sur le contrat
# hashref $tabUserInfos     - Informations sur l'utilisateur
#
# Returns:
# hashref - Tableau des droits
sub _getRights
{
    my ($tabContractInfos, $tabUserInfos) = @_;

    my $stateId      = $tabContractInfos->{'state.id'};
    my $invoiceState = $tabContractInfos->{'invoiceState'};
    my $agencyId     = $tabContractInfos->{'agency.id'};

    my $isUserAdmin        = $tabUserInfos->{'isAdmin'};
    my $isUserSuperv       = $tabUserInfos->{'isSuperv'};
    my $isUserTech         = &LOC::Util::in_array(LOC::User::GROUP_TECHNICAL, $tabUserInfos->{'tabGroups'});
    my $isUserChefAt       = &LOC::Util::in_array(LOC::User::GROUP_FOREMAN, $tabUserInfos->{'tabGroups'});
    my $isUserChefAg       = &LOC::Util::in_array(LOC::User::GROUP_AGENCYMANAGER, $tabUserInfos->{'tabGroups'});
    my $isUserComm         = &LOC::Util::in_array(LOC::User::GROUP_COMMERCIAL, $tabUserInfos->{'tabGroups'});
    my $isUserCompta       = &LOC::Util::in_array(LOC::User::GROUP_ACCOUNTING, $tabUserInfos->{'tabGroups'});
    my $isUserTransp       = &LOC::Util::in_array(LOC::User::GROUP_TRANSPORT, $tabUserInfos->{'tabGroups'});
    my $isUserTechPlatform = &LOC::Util::in_array(LOC::User::GROUP_TECHNICALPLATFORM, $tabUserInfos->{'tabGroups'});

    my $tabRights = {};

    # Initialisation des droits
    &LOC::Util::setTreeValues($tabRights, {
        'infos'      => ['isFullService', 'negotiator'],
        'customer'   => ['new', 'name', 'orderNo', 'orderContact', 'history'],
        'site'       => ['label', 'address', 'locality', 'contact', 'activateAsbestos', 'deactivateAsbestos', 'hours', 'wharf', 'anticipated', 'comments'],
        'machine'    => ['model', 'isImperativeModel', 'isJoinDoc'],
        'duration'   => ['beginDate', 'endDate', 'additionalDays', 'additionalDaysComment', 'deductedDays', 'deductedDaysComment'],
        'rentTariff' => ['amountType', 'reduc'],
        'transport'  => ['estimation', 'amount', 'self', 'addMachine', 'transfer', 'delegate', 'transferToAgency',
                         'do-std', 'undo-std', 'do-self', 'undo-self', 'synchro'],
        'repair'     => ['invoiceDays', 'invoiceServices', 'reopenSheet'],
        'services'   => ['insurance', 'appeal', 'cleaning', 'residues', 'fuel',
                         'addRentServices', 'editRentServices'],
        'proforma'   => ['print', 'forcePrint', 'add', 'abort', 'reactivate', 'edit'],
        'documents'  => ['add', 'edit', 'remove', 'sendMail'],
        'infoscpl'   => ['comments', 'tracking'],
        'actions'    => ['valid', 'stop', 'reactivate', 'invoice', 'restop', 'cancel', 'print', 'valorizationPrint']
    }, 0);

    # Vérifie si l'agence de l'utilisateur est identique
    if (!exists $tabUserInfos->{'tabAuthorizedAgencies'}->{$agencyId} && !$isUserAdmin)
    {
        return $tabRights;
    }

    # Tous les utilisateurs
    &LOC::Util::setTreeValues($tabRights, {'documents' => ['add', 'edit', 'remove', 'sendMail']}, 1);

    # TECH, CHEFAT et PSTN
    if ($isUserTech || $isUserChefAt || $isUserTechPlatform)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'machine'   => ['model'],
            'actions'   => ['print'],
            'transport' => ['do-self', 'undo-self']
        }, 1);
    }

    # COMPTA
    if ($isUserCompta)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'proforma' => ['print', 'forcePrint'],
            'actions'  => ['valorizationPrint']
        }, 1);
    }

    # TRANSP
    if ($isUserTransp)
    {
        &LOC::Util::setTreeValues($tabRights, {
             'site'      => ['label', 'address', 'locality', 'contact', 'activateAsbestos', 'hours', 'wharf', 'anticipated', 'comments'],
             'transport' => ['transfer', 'delegate', 'transferToAgency', 'do-std', 'undo-std']
        }, 1);
    }

    # COMM et CHEFAG
    if ($isUserChefAg || $isUserComm || $isUserSuperv || $isUserAdmin)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'infos'      => ['isFullService', 'negotiator'],
            'customer'   => ['new', 'name', 'history'],
            'machine'    => ['model', 'isImperativeModel', 'isJoinDoc'],
            'duration'   => ['beginDate', 'endDate', 'additionalDays','additionalDaysComment',  'deductedDays', 'deductedDaysComment'],
            'rentTariff' => ['amountType', 'reduc'],
            'transport'  => ['estimation', 'amount', 'addMachine', 'self', 'transfer', 'do-self', 'undo-self'],
            'repair'     => ['invoiceDays', 'invoiceServices', 'reopenSheet'],
            'services'   => ['insurance', 'appeal', 'cleaning', 'residues', 'fuel',
                             'addRentServices', 'editRentServices'],
            'proforma'   => ['print', 'add', 'abort', 'reactivate', 'edit'],
            'actions'    => ['stop', 'reactivate', 'invoice', 'restop', 'cancel', 'print', 'valorizationPrint']
        }, 1);

        if ($invoiceState ne INVOICESTATE_FINAL)
        {
            if ($isUserSuperv || $isUserAdmin || $stateId ne STATE_CANCELED)
            {
                &LOC::Util::setTreeValues($tabRights, {
                    'customer' => ['orderNo', 'orderContact'],
                    'infoscpl' => ['comments', 'tracking'],
                    'site'     => ['label', 'address', 'locality', 'contact', 'activateAsbestos', 'hours', 'wharf',
                                   'anticipated', 'comments']
                }, 1);
            }
            if ($isUserAdmin)
            {
                &LOC::Util::setTreeValues($tabRights, {
                    'site' => ['deactivateAsbestos']
                }, 1);
            }
        }
        else
        {
            if ($isUserSuperv || $isUserAdmin)
            {
                &LOC::Util::setTreeValues($tabRights, {
                    'customer' => ['orderNo', 'orderContact']}, 1);
            }
            &LOC::Util::setTreeValues($tabRights, {
                'infoscpl' => ['tracking']
            }, 1);
        }
    }

    # Superviseurs
    if ($isUserSuperv || $isUserAdmin)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'transport' => ['do-std', 'undo-std', 'delegate', 'transferToAgency']
        }, 1);
    }

    # Administrateurs
    if ($isUserAdmin)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'transport' => ['synchro'], 'proforma' => ['forcePrint']
        }, 1);
    }




    # Possibilités suivant les états de contrat et les états de facturation du contrat
    # TODO : Finir de gérer les possibilités dans le getList() pour supprimer ce morceau de code à terme
    # - Etats de contrat
    if ($stateId eq STATE_PRECONTRACT)
    {
        &LOC::Util::setTreeValues($tabRights, {'services' => ['fuel']}, 0);
    }
    if ($stateId eq STATE_CONTRACT)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'infos'    => ['isFullService'],
            'machine'  => ['isImperativeModel', 'isJoinDoc'],
            'services' => ['fuel']}, 0);
    }
    if ($stateId eq STATE_STOPPED)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'infos'    => ['isFullService'],
            'machine'  => ['isImperativeModel', 'isJoinDoc']}, 0);
    }
    if ($stateId eq STATE_BILLED)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'infos'    => ['isFullService'],
            'machine'  => ['isImperativeModel', 'isJoinDoc'],
            'proforma' => ['add', 'abort', 'reactivate', 'edit']}, 0);
    }

    if ($stateId eq STATE_CANCELED)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'infos'      => ['isFullService', 'negotiator'],
            'machine'    => ['isImperativeModel', 'isJoinDoc'],
            'duration'   => ['beginDate', 'endDate', 'additionalDays', 'additionalDaysComment', 'deductedDays', 'deductedDaysComment'],
            'rentTariff' => ['amountType', 'reduc'],
            'transport'  => ['estimation', 'amount', 'addMachine'],
            'repair'     => ['invoiceDays', 'invoiceServices', 'reopenSheet'],
            'services'   => ['insurance', 'appeal', 'cleaning', 'residues', 'fuel',
                             'addRentServices', 'editRentServices'],
            'proforma'   => ['print', 'forcePrint', 'add', 'abort', 'reactivate', 'edit'],
            'actions'    => ['stop', 'reactivate', 'invoice', 'restop', 'cancel', 'print', 'valorizationPrint']}, 0);
    }



    # - Etats de facturation
    if ($invoiceState eq INVOICESTATE_PARTIAL)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'duration'   => ['beginDate'],
            'rentTariff' => ['amountType']}, 0);
    }
    if ($invoiceState eq INVOICESTATE_FINALPARTIAL)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'duration'   => ['beginDate', 'endDate', 'deductedDays', 'deductedDaysComment'],
            'rentTariff' => ['amountType', 'reduc'],
            'services'   => ['insurance', 'appeal', 'cleaning', 'residues', 'fuel']}, 0);
    }
    if ($invoiceState eq INVOICESTATE_FINAL)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'site'       => ['label', 'address', 'locality', 'contact', 'hours', 'wharf', 'anticipated', 'comments'],
            'duration'   => ['beginDate', 'endDate', 'additionalDays', 'additionalDaysComment', 'deductedDays', 'deductedDaysComment'],
            'rentTariff' => ['amountType', 'reduc'],
            'repair'     => ['invoiceDays', 'invoiceServices', 'reopenSheet'],
            'services'   => ['insurance', 'appeal', 'cleaning', 'residues', 'fuel',
                             'addRentServices', 'editRentServices']}, 0);
    }

    # Doit-on activer le bouton "Valider" ?
    my $tabActiveValid = {
        'infos'      => ['isFullService', 'negotiator'],
        'customer'   => ['name', 'orderNo', 'orderContact'],
        'site'       => ['label', 'address', 'locality', 'contact', 'hours', 'wharf', 'anticipated', 'comments'],
        'machine'    => ['model', 'isImperativeModel', 'isJoinDoc'],
        'duration'   => ['beginDate', 'endDate', 'additionalDays', 'additionalDaysComment', 'deductedDays', 'deductedDaysComment'],
        'rentTariff' => ['amountType', 'reduc'],
        'transport'  => ['estimation', 'amount', 'self', 'addMachine', 'transfer', 'delegate', 'transferToAgency',
                         'do-std', 'undo-std', 'do-self', 'undo-self'],
        'repair'     => ['invoiceDays', 'invoiceServices', 'reopenSheet'],
        'services'   => ['insurance', 'appeal', 'cleaning', 'residues', 'fuel',
                         'addRentServices', 'editRentServices'],
        'proforma'   => ['add', 'abort', 'reactivate', 'edit'],
        'documents'  => ['add', 'edit', 'remove'],
        'infoscpl'   => ['comments', 'tracking'],
        'actions'    => ['valid', 'stop', 'reactivate', 'invoice', 'restop', 'cancel']
    };
    my $sum = 0;
    &LOC::Util::execTreeFunc($tabRights, $tabActiveValid, sub { $sum += $_[0]; });
    if ($sum > 0)
    {
        &LOC::Util::setTreeValues($tabRights, {'actions' => ['valid']}, 1);
    }

    return $tabRights;
}


# Function: _hasRight
# Vérifier le droit à un champ
#
# Parameters:
# hashref $tab  - Tableau des droits
# string  $name - Nom du droit
#
# Returns:
# bool
sub _hasRight
{
    my ($tab, $name) = @_;

    return ($tab->{$name} ? 1 : 0);
}


# Function: _cancel
# Annuler le contrat
#
# Parameters:
# LOC::Db::Adapter $db         - Connexion à la base de données
# string           $countryId  - Pays
# int              $contractId - Id du contrat
# int              $userId     - Id utilisateur
# hashref          $tabErrors  - Tableau des erreurs
# hashref          $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool -
sub _cancel
{
    my ($db, $countryId, $contractId, $userId, $tabErrors, $tabEndUpds) = @_;


    # Récupération des informations du contrat
    my $tabContractInfos = &getInfos($countryId, $contractId, GETINFOS_POSSIBILITIES);

    if (!$tabContractInfos->{'possibilities'}->{'actions'}->{'cancel'})
    {
        return 0;
    }

    my $agencyId = $tabContractInfos->{'agency.id'};

    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Mise à jour de l'état du contrat
    my $contractStateId = STATE_CANCELED;
    if ($db->update('CONTRAT', {'ETCODE' => $contractStateId,
                                'LIETCONTRATAUTO' => substr($contractStateId, -2) * 1},
                               {'CONTRATAUTO*' => $contractId},
                               $schemaName) == -1)
    {
        return 0;
    }

    # Mise à jour des alertes, kimoce, état machine...
    my $modelId = $tabContractInfos->{'model.id'};
    if ($tabContractInfos->{'isFinalAllocation'})
    {
        # Mise à jour de l'état de la machine => Disponible
        my $tabMachineErrors = $tabErrors->{'modules'}->{'machine'};
        if (!&LOC::Machine::update($countryId, $tabContractInfos->{'machine.id'}, {'state.id' => LOC::Machine::STATE_AVAILABLE}, $userId, {}, $tabMachineErrors, $tabEndUpds))
        {
            push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_MACHINE);
            # Erreur fatale ?
            if (&LOC::Util::in_array(0x0001, $tabMachineErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            }
            return 0;
        }
    }
    else
    {
        &LOC::EndUpdates::updateModelStock($tabEndUpds, $agencyId, $modelId, 'nbBooked', -1);

        if (!$tabContractInfos->{'machine.id'})
        {
            # Décrémente l'alerte "Machines à attribuer"
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::MACHINESTOALLOCATE, -1);
        }
    }

    # Impact sur les états des lieux
    &LOC::MachineInventory::onExternalChanges($countryId, {
        'contract' => {
            'id'    => $contractId,
            'event' => 'update',
            'props' => {
                'state.id' => {'old' => $tabContractInfos->{'state.id'}, 'new' => $contractStateId}
            }
        }
    }, $userId, undef, $tabEndUpds);

    # Décrémente l'alerte "Pré-contrats effectués"
    &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::PRECONTRACTS, -1);

    # Ajout de la trace
    my $tabTrace = {
        'schema'  => $schemaName,
        'code'    => LOC::Log::EventType::CANCEL,
        'old'     => undef,
        'new'     => undef,
        'content' => ''
    };
    &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);

    # Abandon des proformas du contrat
    &LOC::Proforma::abortAll($countryId, 'contract', $contractId, $userId, {'isForced' => 1}, {}, $tabEndUpds);

    return 1;
}


# Function: _invoice
# Mettre le contrat en facturation
#
# Parameters:
# LOC::Db::Adapter $db         - Connexion à la base de données
# string           $countryId  - Pays
# int              $contractId - Id du contrat
# int              $userId     - Id de l'utilisateur
# hashref          $tabErrors  - Tableau des erreurs
# hashref          $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool -
sub _invoice
{
    my ($db, $countryId, $contractId, $userId, $tabErrors, $tabEndUpds) = @_;

    # Récupération des informations du contrat
    my $tabContractInfos = &getInfos($countryId, $contractId, GETINFOS_POSSIBILITIES);

    if (!$tabContractInfos->{'possibilities'}->{'actions'}->{'invoice'})
    {
        return 0;
    }

    my $agencyId = $tabContractInfos->{'agency.id'};

    # Nom du schéma de base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Champs à mettre à jour sur le contrat
    my $contractStateId = STATE_BILLED;
    my $tabContractUpdates = {'ETCODE' => $contractStateId,
                              'LIETCONTRATAUTO' => substr($contractStateId, -2) * 1};

    if ($db->update('CONTRAT', $tabContractUpdates, {'CONTRATAUTO*' => $contractId}, $schemaName) == -1)
    {
        return 0;
    }

    # Génération de la facture finale
    my $finalInvoice = &_generateFinalInvoice($countryId, $contractId, $tabContractInfos);
    if (!$finalInvoice)
    {
        return 0;
    }

    # Décrémente l'alerte "Contrat(s) à passer en facturation"
    my %tabNowDate = &LOC::Date::getHashDate();
    my $nowDate = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE, %tabNowDate);
    if ($tabContractInfos->{'endDate'} le $nowDate)
    {
        &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::CONTRACTSTOINVOICE, -1);
    }

    # Ajout de la trace
    my $tabTrace = {
        'schema'  => $schemaName,
        'code'    => LOC::Log::EventType::INVOICE,
        'old'     => undef,
        'new'     => undef,
        'content' => ''
    };
    &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);

    return 1;
}


# Function: _generateFinalInvoice
# Générer la facture finale
#
# Parameters:
# string           $countryId        - Pays
# int              $contractId       - Id du contrat
# hashref          $tabContractInfos - Informations supplémentaires
#
# Returns:
# bool - 1 : si la facture est générée avec succès, 0 sinon
sub _generateFinalInvoice
{
    my ($countryId, $contractId, $tabContractInfos) = @_;

    # Préparation des paramètres à passer à la fonction de génération de la facture finale
    my %tabParams = {};

    # Début de la période de facturation : 1er jour du mois de la date de fin
    my @endDate = split(/-/, $tabContractInfos->{'endDate'});
    $tabParams{'beginMonthInvoiceDate'} = sprintf('%04d-%02d-01', $endDate[0], $endDate[1]);

    # Fin de la période de facturation : dernier jour du mois de la date de fin
    my $day = &Date::Calc::Days_in_Month($endDate[0], $endDate[1]);
    $tabParams{'endMonthInvoiceDate'} = sprintf('%04d-%02d-%02d', $endDate[0], $endDate[1], $day);

    # Date du jour
    $tabParams{'todayDate'} = &LOC::Date::getMySQLDate();

    my $tabCountryInfos = &LOC::Country::getInfos($countryId);

    # Identifiant de la locale
    $tabParams{'locale.id'} = $tabCountryInfos->{'locale.id'};

    # Taux d'assurance par défaut
    $tabParams{'insurance.defaultRate'} = $tabCountryInfos->{'insurance.defaultRate'};

    # Récupération du taux monétaire euro courant
    $tabParams{'euroRate'} = &LOC::Country::getEuroRate($countryId);

    # Récupération taux de TVA courant
    $tabParams{'tabVAT'} = &LOC::Country::getVATRatesList($countryId);

    # Centre de coût
    $tabParams{'costCenter'} = &LOC::Characteristic::getCountryValueByCode('CENTRCOUT', $countryId);

    # Libellé du dépot
    $tabParams{'deposit'} = &LOC::Characteristic::getCountryValueByCode('DEPOTSTOCK', $countryId);

    # Nombre de jours minimum à facturer (borne exclue)
    $tabParams{'minDaysToInvoice'} = &LOC::Characteristic::getCountryValueByCode('MINDAYSFACTM', $countryId);

    # Flag de mise à jour de l'alerte des BL/BR à signer
    $tabParams{'updateAlertRecoveryNote'} = &LOC::Characteristic::getCountryValueByCode('ALIMALBLBR', $countryId);

    # Récupération du type de document
    $tabParams{'invoiceType'}      = &LOC::Characteristic::getCountryValueByCode('TYPEDOCSAGE', $countryId);

    # Récupération des références articles pour la facturation
    my %tabArticleRefs = &LOC::Invoice::ArticleRef::getList($countryId, LOC::Util::GETLIST_ASSOC);

    # Construction du tableau des types de TVA indexé par code articles
    foreach my $tabRow (values %tabArticleRefs)
    {
        $tabParams{'tabArticleRefs'}{$tabRow->{'code'}} = $tabRow->{'value'};
        $tabParams{'tabArticleVAT'}{$tabRow->{'code'}} = $tabRow->{'vat.id'};
        $tabParams{'tabArticleLabels'}{$tabRow->{'code'}} = $tabRow->{'label'};
    }

    # Tableau des codes analytiques des agences
    $tabParams{'tabAgencyAnaCodes'} = &LOC::Agency::getAnalyticalCodesList();

    # Liste des énergies à carburant
    $tabParams{'tabFuelEnergies'} = &LOC::MachinesFamily::getFuelEnergiesList();

    # Tableau des codes analytiques des familles commerciales
    $tabParams{'tabBusinessFamAnaCodes'} = &LOC::BusinessFamily::getAnalyticalCodesList($countryId);

    my $invoice = &LOC::Invoice::Rent::contractInvoice($countryId, $contractId, undef, undef, \%tabParams, undef);
    if ($invoice == -1)
    {
        return 0;
    }
    return 1;
}


# Function: _lockCustomerAfterUpdate
# # Reblocage du client s'il était débloqué temporairement
#
# Parameters:
# string   $countryId        - Pays
# hashref  $tabCustomerInfos - Informations sur le client
# int      $userId           - Utilisateur responsable de la modification
# hashref  $tabEndUpds       - Mises à jour de fin
# string   $callerRoutine    - Routine appellante
#
# Returns:
# bool -
sub _lockCustomerAfterUpdate
{
    my ($countryId, $tabCustomerInfos, $userId, $tabEndUpds, $callerRoutine) = @_;

    # Reblocage du client s'il était débloqué temporairement
    if ($tabCustomerInfos->{'isTempUnlock'} == 1)
    {
        # Récupération des codes collectifs à ne pas rebloquer
        my $regEx = &LOC::Characteristic::getCountryValueByCode('COLTMPUNL', $countryId);

        if ($regEx eq '' || $tabCustomerInfos->{'accountingCode'} !~ /^$regEx$/)
        {
            my $tabData = {
                'isLocked'     => 1,
                'isTempUnlock' => 0
            };
            if (!&LOC::Customer::update($countryId, $tabCustomerInfos->{'id'}, $tabData, $userId,
                                        {'caller' => 'LOC::Contract::Rent::' . $callerRoutine . '()'},
                                        undef, $tabEndUpds))
            {
                return 0;
            }
        }
    }
    return 1;
}


# Function: _reactivate
# Remettre le contrat en cours
#
# Parameters:
# LOC::Db::Adapter $db         - Connexion à la base de données
# string           $countryId  - Pays
# int              $contractId - Id du contrat
# int              $userId     - Id utilisateur
# hashref          $tabErrors  - Tableau des erreurs
# hashref          $tabEndUpds - Mises à jour de fin
#
# Returns:
# int - Id de la nouvelle ligne du contrat
sub _reactivate
{
    my ($db, $countryId, $contractId, $userId, $tabErrors, $tabEndUpds) = @_;

    # Récupération des informations du contrat
    my $tabContractInfos = &getInfos($countryId, $contractId, GETINFOS_POSSIBILITIES | GETINFOS_FUEL | GETINFOS_EXT_SITE | GETINFOS_VALUEDATE);
    my $tabRecoveryInfos = &LOC::Transport::getDocumentRecoveryInfos($countryId, 'contract', $contractId);

    if (!$tabContractInfos->{'possibilities'}->{'actions'}->{'reactivate'})
    {
        return 0;
    }

    my $agencyId = $tabContractInfos->{'agency.id'};

    # Nom du schéma de base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Suppression du carburant
    my $fuelId = $tabContractInfos->{'fuel.id'};
    if ($fuelId)
    {
        if ($db->update('CONTRAT', {'CARBUAUTO*' => 0}, {'CONTRATAUTO*' => $contractId}, $schemaName) == -1)
        {
            return 0;
        }
        if ($db->delete('CARBURANT', {'CARBUAUTO*' => $fuelId}, $schemaName) == -1)
        {
            return 0;
        }

        # Modification du carburant
        my $oldFuelQuantity = $tabContractInfos->{'fuel.quantity'};
        my $oldFuelAmount   = &LOC::Util::round($tabContractInfos->{'fuel.amount'}, 2);
        if ($oldFuelQuantity != 0 || $oldFuelAmount != 0)
        {
            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDFUEL,
                'old'     => {
                    'amount'   => $oldFuelAmount,
                    'quantity' => $oldFuelAmount
                },
                'new'     => {
                    'amount'   => 0,
                    'quantity' => 0
                },
                'content' => $oldFuelAmount . ' (' . $oldFuelQuantity . ' L) → 0 (0 L)'
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
        }
    }
    else
    {
        # Récupération des informations du modèle de machines
        my $tabModelInfos = &LOC::Model::getInfos($countryId, $tabContractInfos->{'model.id'});

        my $contractEnergy  = $tabModelInfos->{'machinesFamily.energy'};
        my $tabFuelEnergies = &LOC::MachinesFamily::getFuelEnergiesList();
        if (&LOC::Util::in_array($contractEnergy, $tabFuelEnergies))
        {
            # Décrémente l'alerte "Carburants à saisir"
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::FUELS, -1);
        }
    }


    # Suppression de la prise en compte de la récupération
    if (!&LOC::Transport::unconsider($countryId, $tabRecoveryInfos->{'id'}, $userId, {}, undef, $tabEndUpds))
    {
        return 0;
    }


    # Mise à jour de l'état du contrat
    my $contractStateId = STATE_CONTRACT;
    if ($db->update('CONTRAT', {'ETCODE' => $contractStateId,
                                'LIETCONTRATAUTO' => substr($contractStateId, -2) * 1,
                                'CONTRATTRANSPLAN' => -1,
                                'CONTRATIMP' => 0},
                               {'CONTRATAUTO*' => $contractId},
                               $schemaName) == -1)
    {
        return 0;
    }

    # Mise à jour de l'état de la machine => Louée
    my $tabMachineErrors = $tabErrors->{'modules'}->{'machine'};
    if (!&LOC::Machine::update($countryId, $tabContractInfos->{'machine.id'}, {'state.id' => LOC::Machine::STATE_RENTED}, $userId, {}, $tabMachineErrors, $tabEndUpds))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_MACHINE);
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabMachineErrors->{'fatal'}))
        {
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        }
        return 0;
    }

    my %tabNowDate = &LOC::Date::getHashDate();
    my $nowDate = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE, %tabNowDate);
    if ($tabContractInfos->{'endDate'} le $nowDate)
    {
        # Décrémente l'alerte "Contrat(s) à passer en facturation"
        &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::CONTRACTSTOINVOICE, -1);

        # Incrémente l'alerte "Contrats à arrêter"
        &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::CONTRACTSTOSTOP, +1);
    }
    # Cas d'un contrat amiante
    elsif ($tabContractInfos->{'site.tabInfos'}->{'isAsbestos'})
    {
        my $tabCfgsStopAlert = &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('CFGSTOPALERT', $agencyId, $tabContractInfos->{'valueDate'}));
        my $deltaDays = (defined $tabCfgsStopAlert->{'asbestosDays'} ? $tabCfgsStopAlert->{'asbestosDays'} : 0);
        if ($tabContractInfos->{'endDate'} le &LOC::Date::addDays($nowDate, $deltaDays))
        {
            # Incrémente l'alerte "Contrats à arrêter"
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::CONTRACTSTOSTOP, +1);
        }
    }

    # Ajout de la trace
    my $tabTrace = {
        'schema'  => $schemaName,
        'code'    => LOC::Log::EventType::REACTIVATE,
        'old'     => undef,
        'new'     => undef,
        'content' => ''
    };
    &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);

    return 1;
}


# Function: _reopenRepairSheets
# Remise en cours des fiches de remise en état
#
# Parameters:
# LOC::Db::Adapter $db                - Connexion à la base de données
# string           $countryId         - Pays
# int              $contractId        - Id du contrat
# arrayref         $tabRepairSheetsToReopen - Tableau des fiches de remise en état
# bool             $hasRight          - Indique si on a droit ou pas à la remise en cours des fiches de remise en état
# int              $userId            - Utilisateur responsable de la modification
# hashref          $tabErrors         - Tableau des erreurs
# hashref          $tabEndUpds        - Mises à jour de fin
#
# Returns:
# bool - 1 si tout est ok, sinon 0
sub _reopenRepairSheets
{
    my ($db, $countryId, $contractId, $tabRepairSheetsToReopen, $hasRight, $userId, $tabErrors, $tabEndUpds) = @_;

    if (@$tabRepairSheetsToReopen == 0)
    {
        return 1;
    }

    # Vérification des droits
    if (!$hasRight)
    {
        # Pas les droits !
        push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my $query = '
SELECT
    rps_id AS `id`,
    rps_agc_id AS `agency.id`,
    rps_sta_id AS `state.id`
FROM ' . $schemaName . '.f_repairsheet
WHERE rps_id IN (' . join(', ', @$tabRepairSheetsToReopen) . ');';
    my $tabRepairSheets = $db->fetchAssoc($query, {}, 'id');
    if (!$tabRepairSheets)
    {
        # Erreur fatale !
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    foreach my $id (keys(%$tabRepairSheets))
    {
        if ($tabRepairSheets->{$id}->{'state.id'} eq LOC::Repair::Sheet::STATE_TURNEDIN)
        {
            my $tabUpdates = {'rps_sta_id' => LOC::Repair::Sheet::STATE_WAITING};

            # Suppression des jours de remise en état
            # et des services de remise en état sur le contrat
            if ($db->delete('f_rentcontractrepairday', {'crd_rps_id' => $id}, $schemaName) == -1 ||
                $db->delete('f_rentcontractrepairservice', {'crs_rps_id' => $id}, $schemaName) == -1)
            {
                # Erreur fatale !
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                return 0;
            }

            # Réouverture de la fiche de remise en état
            if (&LOC::Repair::Sheet::reopen($countryId, $tabRepairSheets->{$id}->{'agency.id'}, $id) == -1)
            {
                # Erreur fatale !
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                return 0;
            }

            # Trace
            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::REACTIVATEREPAIRSHEET,
                'old'     => undef,
                'new'     => $id,
                'content' => $id
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
        }
    }

    return 1;
}


# Function: _restop
# Remettre le contrat en arrêt
#
# Parameters:
# LOC::Db::Adapter $db         - Connexion à la base de données
# string           $countryId  - Pays
# int              $contractId - Id du contrat
# int              $userId     - Id utilisateur
# hashref          $tabErrors  - Tableau des erreurs
# hashref          $tabEndUpds - Mises à jour de fin
#
# Returns:
# int - Id de la nouvelle ligne du contrat
sub _restop
{
    my ($db, $countryId, $contractId, $userId, $tabErrors, $tabEndUpds) = @_;

    # Récupération des informations du contrat
    my $tabContractInfos = &getInfos($countryId, $contractId, GETINFOS_POSSIBILITIES);
    my $tabRecoveryInfos = &LOC::Transport::getDocumentRecoveryInfos($countryId, 'contract', $contractId);

    if (!$tabContractInfos->{'possibilities'}->{'actions'}->{'restop'})
    {
        return 0;
    }

    my $agencyId = $tabContractInfos->{'agency.id'};

    # Récupération des identifiants des factures non numérotées
    my $tabInvoicesToDelete = &LOC::Invoice::Rent::getList($countryId, LOC::Util::GETLIST_PAIRS,
                                                           {'contractCode' => $tabContractInfos->{'code'},
                                                            'numbered'     => 0});
    my @tabInvoicesToDelete = keys(%$tabInvoicesToDelete);
    my $nbInvoicesToDelete = @tabInvoicesToDelete;

    # Champs à mettre à jour sur le contrat
    my $contractStateId = STATE_STOPPED;
    my $tabContractUpdates = {
        'ETCODE'          => $contractStateId,
        'LIETCONTRATAUTO' => substr($contractStateId, -2) * 1
    };

    # Suppression des factures non numérotées
    if ($nbInvoicesToDelete > 0)
    {
        foreach my $id (@tabInvoicesToDelete)
        {
           if (!&LOC::Invoice::Rent::cancelInvoice($countryId, $id, 1))
           {
               return 0;
           }
        }
    }
    else
    {
         # Récupération des identifiants des factures numérotées
        my $tabInvoices = &LOC::Invoice::Rent::getList($countryId, LOC::Util::GETLIST_IDS,
                                                       {'contractCode' => $tabContractInfos->{'code'},
                                                        'numbered'     => 1});

        # Récupération des jours complémentaires figurant sur les factures numérotées existantes
        my $additionalDaysInvoiced = 0;
        my $deductedDaysInvoiced = 0;

        foreach my $invoiceId (@$tabInvoices)
        {
            # Informations sur la facture
            my $tabInfos = &LOC::Invoice::Rent::getInfos($countryId, $invoiceId);

            # On regarde les articles présents sur la facture pour comptabiliser les J+ et J-
            foreach my $tabLineInfos (@{$tabInfos->{'lines'}})
            {
                if ($tabLineInfos->{'article'} eq 'J+')
                {
                    $additionalDaysInvoiced += $tabLineInfos->{'quantity'};
                }
                elsif ($tabLineInfos->{'article'} eq 'J-')
                {
                    $deductedDaysInvoiced += $tabLineInfos->{'quantity'} * -1;
                }
            }
        }

        # Calcul des jours complémentaires
        $tabContractUpdates->{'CONTRATJOURPLUSFACT'} = $additionalDaysInvoiced;
        $tabContractUpdates->{'CONTRATJOURSPLUS'} = $tabContractInfos->{'additionalDaysUnBilled'} +
                                                    $tabContractInfos->{'additionalDaysBilled'} -
                                                    $tabContractUpdates->{'CONTRATJOURPLUSFACT'};

        $tabContractUpdates->{'CONTRATJOURMOINSFACT'} = $deductedDaysInvoiced;
        $tabContractUpdates->{'CONTRATJOURSMOINS'} = $tabContractInfos->{'deductedDaysUnbilled'} +
                                                     $tabContractInfos->{'deductedDaysBilled'} -
                                                     $tabContractUpdates->{'CONTRATJOURMOINSFACT'};


        # Etat de facturation
        $tabContractUpdates->{'ETATFACTURE'} = $tabContractInfos->{'previousInvoiceState'};
    }


    # Nom du schéma de base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Mise à jour de l'état du contrat
    if ($db->update('CONTRAT', $tabContractUpdates, {'CONTRATAUTO*' => $contractId}, $schemaName) == -1)
    {
        return 0;
    }

    # Incrémente l'alerte "Contrat(s) à passer en facturation"
    my %tabNowDate = &LOC::Date::getHashDate();
    my $nowDate = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE, %tabNowDate);
    if ($tabContractInfos->{'endDate'} le $nowDate)
    {
        &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::CONTRACTSTOINVOICE, +1);
    }

    # Ajout de la trace
    my $tabTrace = {
        'schema'  => $schemaName,
        'code'    => LOC::Log::EventType::RESTOP,
        'old'     => undef,
        'new'     => undef,
        'content' => ''
    };
    &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);


    # FA-13121: Montant incorrect sur un enlèvement agence lors de la remise en cours d'un contrat
    # Remise à zéro du montant de la récupération si il est supérieur à zéro et qu'il s'agit d'un enlèvement agence
    my $result = &LOC::Tariff::Transport::resetSelfRecoveryAmountOnContractRestop($countryId, $contractId, $userId, $tabErrors, $tabEndUpds);
    if (!$result)
    {
        return 0;
    }
    elsif ($result == 1)
    {
        # Dans le cas où une modification du montant de la récupération a été effectuée,
        # on remet à jour les montants totaux du contrat
        if (!&_updateAmounts($db, $countryId, $contractId))
        {
            return 0;
        }
    }

    return 1;
}


# Function: _sendCreateAccountMail
# Envoi du mail d'ouverture de compte
#
# Parameters:
# string  $countryId        - Pays
# hashref $tabCustomerInfos - Informations sur le client
# hasref  $tabContractInfos - Informations sur le contrat
# int     $userId           - Id du créateur
#
# Returns:
# bool -
sub _sendCreateAccountMail
{
    my ($countryId, $tabCustomerInfos, $tabContractInfos, $userId) = @_;

    # Informations sur l'utilisateur (avec optimisation)
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Informations de l'agence
    my $tabAgencyInfos = &LOC::Agency::getInfos($tabUserInfos->{'nomadAgency.id'});


    # Création d'un mail
    my $mailObj = LOC::Mail->new();

    # Encodage
    $mailObj->setEncodingType(LOC::Mail::ENCODING_TYPE_UTF8);

    # Expéditeur
    my %tabSender = ($tabUserInfos->{'email'} => $tabUserInfos->{'firstName'} . ' ' . $tabUserInfos->{'name'});
    $mailObj->setSender(%tabSender);

    # Corps
    my $file = &LOC::Globals::get('cgiPath') . '/cfg/templates/' . $countryId . '/newCustomer.html';
    open(TPL, $file);
    my $template = join('', <TPL>);
    close(TPL);

    # Remplacement dans le template
    my $tabReplaces = {
        'customer.name'      => $tabCustomerInfos->{'name'},
        'customer.statCode'  => $tabCustomerInfos->{'statCode'},
        'customer.address1'  => $tabCustomerInfos->{'address1'},
        'customer.address2'  => $tabCustomerInfos->{'address2'},
        'customer.address3'  => $tabCustomerInfos->{'address3'},
        'customer.telephone' => $tabCustomerInfos->{'telephone'},
        'customer.fax'       => $tabCustomerInfos->{'fax'},
        'contract.code'      => $tabContractInfos->{'code'},
        'contract.beginDate' => $tabContractInfos->{'beginDate'},
        'user.firstName'     => $tabUserInfos->{'firstName'},
        'user.name'          => $tabUserInfos->{'name'},
        'agency.label'       => $tabAgencyInfos->{'label'},
        'agency.telephone'   => $tabAgencyInfos->{'telephone'},
        'agency.fax'         => $tabAgencyInfos->{'fax'}
    };
    while ($template =~ m/\<\%(.*?)\>/)
    {
        my $replace = '';
        if (exists $tabReplaces->{$1})
        {
            $replace = $tabReplaces->{$1};
        }
        $template =~ s/<%$1>/$replace/g;
    }

    my $subject = ($template =~ m/\<title\>(.*?)\<\/title\>/ ? $1 : '');

    if ($template =~ /"cid:(.+)"/ && -e &LOC::Globals::get('imgPath') . '/print/gui/' . $1)
    {
        $mailObj->addInlineAttachment(&LOC::Globals::get('imgUrl') . '/print/gui/' . $1, $1);
    }

    $mailObj->setBodyType(LOC::Mail::BODY_TYPE_HTML);
    $mailObj->setBody($template);

    # Objet
    if (&LOC::Globals::getEnv() ne 'exp')
    {
        $mailObj->addRecipient(%tabSender);
    }
    else
    {
        my $recipients = &LOC::Characteristic::getCountryValueByCode('MAILNVCLIENT', $countryId);
        my @tabTo = split(/;/, $recipients);
        my $nbTo = @tabTo;

        for (my $i = 0; $i < $nbTo; $i++)
        {
            $mailObj->addRecipient($tabTo[$i]);
        }
    }
    $mailObj->setSubject($subject);

    $mailObj->send();

    return 1;
}

# Function: _sendReactivateAccountMail
# Envoi du mail pour les clients inactifs
#
# Parameters:
# string  $countryId        - Pays
# hashref $tabCustomerInfos - Informations sur le client
# hasref  $tabContractInfos - Informations sur le contrat
# int     $userId           - Id du créateur
#
# Returns:
# bool -
sub _sendReactivateAccountMail
{
    my ($countryId, $tabCustomerInfos, $tabContractInfos, $userId) = @_;

    # Informations sur l'utilisateur (avec optimisation)
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Informations de l'agence
    my $tabAgencyInfos = &LOC::Agency::getInfos($tabUserInfos->{'nomadAgency.id'});


    # Création d'un mail
    my $mailObj = LOC::Mail->new();

    # Encodage
    $mailObj->setEncodingType(LOC::Mail::ENCODING_TYPE_UTF8);

    # Expéditeur
    my %tabSender = ($tabUserInfos->{'email'} => $tabUserInfos->{'firstName'} . ' ' . $tabUserInfos->{'name'});
    $mailObj->setSender(%tabSender);

    # Corps
    my $file = &LOC::Globals::get('cgiPath') . '/cfg/templates/' . $countryId . '/reactivateCustomer.html';
    open(TPL, $file);
    my $template = join('', <TPL>);
    close(TPL);

    # Remplacement dans le template
    my $tabReplaces = {
        'customer.code'      => $tabCustomerInfos->{'code'},
        'customer.name'      => $tabCustomerInfos->{'name'},
        'customer.statCode'  => $tabCustomerInfos->{'statCode'},
        'customer.address1'  => $tabCustomerInfos->{'address1'},
        'customer.address2'  => $tabCustomerInfos->{'address2'},
        'customer.address3'  => $tabCustomerInfos->{'address3'},
        'customer.telephone' => $tabCustomerInfos->{'telephone'},
        'customer.fax'       => $tabCustomerInfos->{'fax'},
        'contract.code'      => $tabContractInfos->{'code'},
        'contract.beginDate' => $tabContractInfos->{'beginDate'},
        'user.firstName'     => $tabUserInfos->{'firstName'},
        'user.name'          => $tabUserInfos->{'name'},
        'agency.label'       => $tabAgencyInfos->{'label'},
        'agency.telephone'   => $tabAgencyInfos->{'telephone'},
        'agency.fax'         => $tabAgencyInfos->{'fax'}
    };
    while ($template =~ m/\<\%(.*?)\>/)
    {
        my $replace = '';
        if (exists $tabReplaces->{$1})
        {
            $replace = $tabReplaces->{$1};
        }
        $template =~ s/<%$1>/$replace/g;
    }

    my $subject = ($template =~ m/\<title\>(.*?)\<\/title\>/ ? $1 : '');

    if ($template =~ /"cid:(.+)"/ && -e &LOC::Globals::get('imgPath') . '/print/gui/' . $1)
    {
        $mailObj->addInlineAttachment(&LOC::Globals::get('imgUrl') . '/print/gui/' . $1, $1);
    }

    $mailObj->setBodyType(LOC::Mail::BODY_TYPE_HTML);
    $mailObj->setBody($template);

    # Objet
    if (&LOC::Globals::getEnv() ne 'exp')
    {
        $mailObj->addRecipient(%tabSender);
    }
    else
    {
        my $recipients = &LOC::Characteristic::getCountryValueByCode('MAILNVCLIENT', $countryId);
        my @tabTo = split(/;/, $recipients);
        my $nbTo = @tabTo;

        for (my $i = 0; $i < $nbTo; $i++)
        {
            $mailObj->addRecipient($tabTo[$i]);
        }
    }
    $mailObj->setSubject($subject);

    $mailObj->send();

    return 1;
}
# Function: _stop
# Arrêter le contrat
#
# Parameters:
# LOC::Db::Adapter $db         - Connexion à la base de données
# string           $countryId  - Pays
# int              $contractId - Id du contrat
# int              $userId     - Id utilisateur
# hashref          $tabErrors  - Tableau des erreurs
# hashref          $tabEndUpds - Mises à jour de fin
#
# Returns:
# int - Id de la nouvelle ligne du contrat
sub _stop
{
    my ($db, $countryId, $contractId, $userId, $tabErrors, $tabEndUpds) = @_;

    # Récupération des informations du contrat
    my $tabContractInfos = &getInfos($countryId, $contractId, GETINFOS_POSSIBILITIES | GETINFOS_EXT_SITE | GETINFOS_VALUEDATE);

    if (!$tabContractInfos->{'possibilities'}->{'actions'}->{'stop'})
    {
        return 0;
    }

    my $agencyId = $tabContractInfos->{'agency.id'};

    # Nom du schéma de base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);



    # Prise en compte des précédentes récupérations (ou transferts inter-chantiers) de la même machine et non prises en compte
    my $tabRecoveryInfos = &LOC::Transport::getDocumentRecoveryInfos($countryId, 'contract', $contractId);
    if (!&LOC::Transport::considerAntecedents($countryId, $tabRecoveryInfos->{'id'}, {}, undef, undef, $tabEndUpds))
    {
        return 0;
    }


    # Champs à mettre à jour sur le contrat
    my $contractStateId = STATE_STOPPED;
    my $tabContractUpdates = {'CONTRATTRANSPLAN' => 0,
                              'ETCODE' => $contractStateId,
                              'LIETCONTRATAUTO' => substr($contractStateId, -2) * 1};

    if ($db->update('CONTRAT', $tabContractUpdates, {'CONTRATAUTO*' => $contractId}, $schemaName) == -1)
    {
        return 0;
    }

    # Mise à jour de l'état de la machine => Récupération
    my $tabMachineErrors = $tabErrors->{'modules'}->{'machine'};
    if (!&LOC::Machine::update($countryId, $tabContractInfos->{'machine.id'}, {'state.id' => LOC::Machine::STATE_RECOVERY}, $userId, {}, $tabMachineErrors, $tabEndUpds))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_MACHINE);
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabMachineErrors->{'fatal'}))
        {
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        }
        return 0;
    }

    # Récupération des informations du modèle de machines
    my $tabModelInfos = &LOC::Model::getInfos($countryId, $tabContractInfos->{'model.id'});

    # Si l'énergie de la famille de machines est DIESEL ou BI-EN alors on incrémente l'alerte "Carburants à saisir"
    my $contractEnergy = $tabModelInfos->{'machinesFamily.energy'};
    my $tabFuelEnergies = &LOC::MachinesFamily::getFuelEnergiesList();
    if (&LOC::Util::in_array($contractEnergy, $tabFuelEnergies))
    {
        # Incrémente l'alerte "Carburants à saisir"
        &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::FUELS, +1);
    }

    my %tabNowDate = &LOC::Date::getHashDate();
    my $nowDate = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE, %tabNowDate);
    if ($tabContractInfos->{'endDate'} le $nowDate)
    {
        # Incrémente l'alerte "Contrat(s) à passer en facturation"
        &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::CONTRACTSTOINVOICE, +1);

        # Décrémente l'alerte "Contrats à arrêter"
        &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::CONTRACTSTOSTOP, -1);
    }
    # Cas d'un contrat amiante
    elsif ($tabContractInfos->{'site.tabInfos'}->{'isAsbestos'})
    {
        my $tabCfgsStopAlert = &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('CFGSTOPALERT', $agencyId, $tabContractInfos->{'valueDate'}));
        my $deltaDays = (defined $tabCfgsStopAlert->{'asbestosDays'} ? $tabCfgsStopAlert->{'asbestosDays'} : 0);
        if ($tabContractInfos->{'endDate'} le &LOC::Date::addDays($nowDate, $deltaDays))
        {
            # Incrémente l'alerte "Contrats à arrêter"
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::CONTRACTSTOSTOP, -1);
        }
    }

    # Ajout de la trace
    my $tabTrace = {
        'schema'  => $schemaName,
        'code'    => LOC::Log::EventType::STOP,
        'old'     => undef,
        'new'     => undef,
        'content' => ''
    };
    &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);

    return 1;
}


# Function: _updateAmounts
# Mise à jour du montant total sur un contrat
# IMPORTANT: Toutes les modifications de cette fonction doivent être dupliquées dans
# la mise à jour du montant total du contrat de la Gestion de tarification (alertes_val.php)
#
# Parameters:
# LOC::Db::Adapter $db         - Connexion à la base de données
# string           $countryId  - Pays
# int              $contractId - Id du contrat
#
# Returns:
# bool -
sub _updateAmounts
{
    my ($db, $countryId, $contractId) = @_;

    # Nom du schéma de la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Montant des nettoyages sur le contrat
    my $queryCleaningsAmount =
        'SELECT IFNULL(SUM(IFNULL(tbl_f_contract_cleaning.CONETMONTANT, IFNULL(tbl_p_cleaning.NETMONTANT, 0))), 0) ' .
        'FROM ' . $schemaName . '.CONTRATNETTOYAGE tbl_f_contract_cleaning ' .
        'LEFT JOIN ' . $schemaName . '.NETTOYAGE tbl_p_cleaning ' .
        'ON tbl_p_cleaning.NETAUTO = tbl_f_contract_cleaning.NETAUTO ' .
        'WHERE tbl_f_contract_cleaning.CONTRATAUTO = tbl_f_contract.CONTRATAUTO';
    # Montant des services sur le contrat
    my $queryServicesAmount =
        'SELECT IFNULL(SUM(IFNULL(tbl_f_contractservice.COSERMONTANT, tbl_p_service.SERMONTANT)), 0) ' .
        'FROM ' . $schemaName . '.CONTRATSERVICES tbl_f_contractservice ' .
        'LEFT JOIN ' . $schemaName . '.SERVICES tbl_p_service ' .
        'ON tbl_p_service.SERAUTO = tbl_f_contractservice.SERAUTO ' .
        'WHERE tbl_f_contractservice.CONTRATAUTO = tbl_f_contract.CONTRATAUTO ' .
        'AND tbl_f_contractservice.ETCODE <> "SER03"';
    # Montant du carburant
    my $queryFuelAmount =
        'SELECT IFNULL(tbl_f_fuel.CAMONTANTFR, 0) ' .
        'FROM ' . $schemaName . '.CARBURANT tbl_f_fuel ' .
        'WHERE tbl_f_fuel.CARBUAUTO = tbl_f_contract.CARBUAUTO';
    # Montant des transports aller et retour
    my $queryTransportsAmount = 'tbl_f_contractamount.MOTRANSPORTDFR + tbl_f_contractamount.MOTRANSPORTRFR';
    # Montant des jours de remise en état
    my $queryRepairDaysAmount =
        'SELECT IFNULL(SUM(crd_amount), 0) ' .
        'FROM ' . $schemaName . '.f_rentcontractrepairday ' .
        'WHERE crd_rct_id = tbl_f_contract.CONTRATAUTO ' .
        'AND crd_sta_id <> "' . LOC::Contract::RepairDay::STATE_CANCELED . '"';
    # Montant des services de remise en état
    my $queryRepairServicesAmount =
        'SELECT IFNULL(SUM(crs_amount), 0) ' .
        'FROM ' . $schemaName . '.f_rentcontractrepairservice ' .
        'WHERE crs_rct_id = tbl_f_contract.CONTRATAUTO ' .
        'AND crs_sta_id <> "' . LOC::Contract::RepairService::STATE_CANCELED . '"';
    # Montant des services de location et transport
    my $queryRentServicesAmount =
        'SELECT IFNULL(SUM(rcs_amount), 0) ' .
        'FROM ' . $schemaName . '.f_rentcontract_rentservice ' .
        'WHERE rcs_rct_id = tbl_f_contract.CONTRATAUTO ' .
        'AND rcs_sta_id <> "' . LOC::Common::RentService::STATE_DELETED . '"';

    my $query = '
SELECT
    tbl_f_contract.AGAUTO AS `agency.id`,
    tbl_f_contract.MOAUTO AS `amount.id`,
    tbl_f_contract.CONTRATDD AS `beginDate`,
    tbl_f_contract.CONTRATDR AS `endDate`,
    tbl_f_contract.CONTRATDUREE AS `duration`,
    tbl_f_contract.CONTRATTYPEASSURANCE AS `insuranceType.id`,
    tbl_f_contract.CONTRATPASSURANCE AS `insurance.rate`,
    -tbl_f_contract.CONTRATDECHETSFACT AS `residues.mode`,
    tbl_f_contract.CONTRATDECHETSVAL AS `residues.value`,
    tbl_f_contract.CONTRATTYPERECOURS AS `appealType.id`,
    tbl_f_contract.CONTRATRECOURSVAL AS `appeal.value`,
    (tbl_f_contract.CONTRATJOURSPLUS + tbl_f_contract.CONTRATJOURPLUSFACT) AS `additionalDays`,
    (tbl_f_contract.CONTRATJOURSMOINS + tbl_f_contract.CONTRATJOURMOINSFACT) AS `deductedDays`,

    IF(tbl_f_contractamount.MOFORFAITFR = 0, IF(tbl_f_contractamount.MOTARIFMSFR = 0, "day", "month"), "fixed") AS `amountType`,
    IF(tbl_f_contractamount.MOFORFAITFR = 0, IF(tbl_f_contractamount.MOTARIFMSFR = 0, tbl_f_contractamount.MOPXJOURFR, tbl_f_contractamount.MOTARIFMSFR), tbl_f_contractamount.MOFORFAITFR) AS `unitPrice`,

    (' . $queryCleaningsAmount . ') AS `cleaningsAmount`,
    (' . $queryServicesAmount . ') AS `servicesAmount`,
    (' . $queryFuelAmount . ') AS `fuelAmount`,
    (' . $queryTransportsAmount . ') AS `transportsAmount`,
    (' . $queryRepairDaysAmount . ') AS `repairDaysAmount`,
    (' . $queryRepairServicesAmount . ') AS `repairServicesAmount`,
    (' . $queryRentServicesAmount . ') AS `rentServicesAmount`

FROM ' . $schemaName . '.CONTRAT tbl_f_contract
INNER JOIN ' . $schemaName . '.MONTANT tbl_f_contractamount
ON tbl_f_contractamount.MOAUTO = tbl_f_contract.MOAUTO
WHERE tbl_f_contract.CONTRATAUTO = ' . $contractId . ';';
    my $tabRow = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);

    # Calcul du prix par jour
    $tabRow->{'dailyPrice'} = &LOC::Common::Rent::getDailyPrice(
        $tabRow->{'amountType'},
        $tabRow->{'unitPrice'},
        $tabRow->{'duration'}
    );

    if (!$tabRow)
    {
        return 0;
    }

    # Récupération des informations diverses
    my $agencyId        = $tabRow->{'agency.id'};
    my $amountId        = $tabRow->{'amount.id'} * 1;
    my $beginDate       = $tabRow->{'beginDate'};
    my $endDate         = $tabRow->{'endDate'};
    my $duration        = $tabRow->{'duration'} * 1;
    my $insuranceTypeId = $tabRow->{'insuranceType.id'} * 1;
    my $insuranceRate   = $tabRow->{'insurance.rate'} * 1;
    my $appealTypeId    = $tabRow->{'appealType.id'} * 1;
    my $appealValue     = $tabRow->{'appeal.value'} * 1;
    my $residuesMode    = $tabRow->{'residues.mode'} * 1;
    my $residuesValue   = $tabRow->{'residues.value'} * 1;
    my $amountType      = $tabRow->{'amountType'};
    my $unitPrice       = $tabRow->{'unitPrice'} * 1;
    my $dailyPrice      = $tabRow->{'dailyPrice'} * 1;
    my $additionalDays  = $tabRow->{'additionalDays'} * 1;
    my $deductedDays    = $tabRow->{'deductedDays'} * 1;

    # Récupération des montants
    my $cleaningsAmount      = $tabRow->{'cleaningsAmount'} * 1;
    my $servicesAmount       = $tabRow->{'servicesAmount'} * 1;
    my $fuelAmount           = $tabRow->{'fuelAmount'} * 1;
    my $transportsAmount     = $tabRow->{'transportsAmount'} * 1;
    my $repairDaysAmount     = $tabRow->{'repairDaysAmount'} * 1;
    my $repairServicesAmount = $tabRow->{'repairServicesAmount'} * 1;
    my $rentServicesAmount   = $tabRow->{'rentServicesAmount'} * 1;

    # Calcul du montant de l'assurance
    my $insuranceAmount = &LOC::Common::Rent::calculateInsurance($countryId, $agencyId, $insuranceTypeId, $insuranceRate,
                                                                 $dailyPrice, $beginDate, $endDate,
                                                                 $additionalDays, $deductedDays);

    # Calcul du montant de la gestion de recours
    my $appealAmount = 0;
    if ($appealTypeId)
    {
        $appealAmount = &LOC::Common::Rent::calculateAppeal($countryId, $agencyId, $appealTypeId, $appealValue,
                                                            $dailyPrice, $beginDate, $endDate,
                                                            $additionalDays, $deductedDays);
    }

    # Calcul du montant de la participation au recyclage
    my $repairAmount = $repairDaysAmount + $repairServicesAmount;
    my $residuesAmount = &LOC::Common::Rent::calculateResiduesAmount($agencyId, $residuesMode, $residuesValue,
                                                                     $dailyPrice, $beginDate, $endDate,
                                                                     $additionalDays, $deductedDays, $repairAmount);


    # Calcul du montant total du contrat:
    # Nettoyage + Services + Carburant + Transports + Assurance + Location + Jours de remise en état
    # + Services de remise en état + Participation au recyclage
    my $totalAmount = $cleaningsAmount +
                      $servicesAmount +
                      $fuelAmount +
                      $transportsAmount +
                      $insuranceAmount +
                      $appealAmount +
                      $repairDaysAmount +
                      $repairServicesAmount +
                      $rentServicesAmount +
                      $residuesAmount +
                      $duration * $dailyPrice;

    # Récupération du taux monétaire
    my $euroRate = &LOC::Country::getEuroRate($countryId);

    # Mise à jour des montants
    my $tabUpdates = {
        'MOASSFR'     => $insuranceAmount,
        'MOASSEU'     => $insuranceAmount * $euroRate,
        'MORECOURS'   => $appealAmount,
        'MORECOURSEU' => $appealAmount * $euroRate,
        'MODECHETS'   => $residuesAmount,
        'MODECHETSEU' => $residuesAmount * $euroRate,
        'MOHTFRS'     => $totalAmount,
        'MOHTEU'      => $totalAmount * $euroRate,
        'MOTTCFR'     => 0, # TODO: Mise à jour du montant total TTC à voir !
        'MOTTCEU'     => 0
    };
    # Exécution de la requête
    if ($db->update('MONTANT', $tabUpdates, {'MOAUTO*' => $amountId}, $schemaName) == -1)
    {
        return 0;
    }

    return 1;
}


# Function: _updateAppeal
# Mise à jour de la gestion de recours
#
# Parameters:
# LOC::Db::Adapter $db                 - Connexion à la base de données
# string           $countryId          - Pays
# int              $contractId         - Id du contrat
# int              $mode               - Mode de calcul de la participation au recyclage
# bool             $isModelChanged     - Indique si le modèle de machine a changé
# bool             $hasRight           - Indique si on a droit ou pas à la modification de la participation au recyclage
# int              $userId             - Utilisateur responsable de la modification
# hashref          $tabErrors          - Tableau des erreurs
# hashref          $tabEndUpds         - Mises à jour de fin
#
# Returns:
# bool - 1 si tout est ok, sinon 0
sub _updateAppeal
{
    my ($db, $countryId, $contractId, $appealTypeId, $hasRight, $userId, $tabErrors, $tabEndUpds) = @_;

    # Nom du schéma
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de l'agence et s'il s'agit d'un accessoire, pour le calcul du montant de la participation
    # au recyclage
    my $query = '
SELECT
    tbl_f_contract.AGAUTO AS `agency.id`,
    tbl_f_contract.CONTRATTYPERECOURS AS `appealType.id`,
    tbl_f_contract.CONTRATRECOURSVAL AS `value`
FROM ' . $schemaName . '.CONTRAT tbl_f_contract
WHERE tbl_f_contract.CONTRATAUTO = ' . $contractId . ';';
    my $tabRow = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if (!$tabRow)
    {
        # Erreur fatale !
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    my $agencyId              = $tabRow->{'agency.id'};
    my $oldAppealTypeId       = $tabRow->{'appealType.id'} * 1;

    my $oldValue              = $tabRow->{'value'} * 1;
    my $value                 = undef;

    my $tabOldAppealInfos = &LOC::Appeal::Type::getInfos($countryId, $oldAppealTypeId);
    my $tabNewAppealInfos = undef;

    # Y'a-t-il eu une modification ?
    if ($oldAppealTypeId != $appealTypeId)
    {
        # Vérification des droits de modification de la gestion de recours
        if (!$hasRight)
        {
            # Pas les droits !
            push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
            return 0;
        }


        if ($appealTypeId)
        {
            $tabNewAppealInfos = &LOC::Appeal::Type::getInfos($countryId, $appealTypeId);
    
            my $valueDate           = &getValueDate($countryId, $contractId);
            my $defaultAppealMode = &LOC::Characteristic::getAgencyValueByCode('RECOURSMODE', $agencyId, $valueDate);
            my $mode = $tabNewAppealInfos->{'mode'};
            # Vérification du nouveau mode par rapport aux possibilités sur le contrat
            if ($tabNewAppealInfos->{'mode'} != LOC::Appeal::Type::MODE_NOTINVOICED && $tabNewAppealInfos->{'mode'} != $defaultAppealMode)
            {
                # Pas les droits !
                push(@{$tabErrors->{'fatal'}}, 0x0129);
                return 0;
            }
    
            if ($defaultAppealMode == LOC::Common::APPEAL_MODE_PERCENT)
            {
                $value = &LOC::Characteristic::getAgencyValueByCode('PCRECOURS', $agencyId, $valueDate) * 1;
            }
        }


        # Mise à jour des tables
        if ($db->update('CONTRAT', {'CONTRATTYPERECOURS' => $appealTypeId,
                                    'CONTRATRECOURSVAL'  => $value},
                                   {'CONTRATAUTO*' => $contractId}, $schemaName) == -1)
        {
            # Erreur fatale !
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            return 0;
        }


        # Trace
        if (defined $tabEndUpds)
        {
            my $tabCountryInfos = &LOC::Country::getInfos($countryId);
            my $locale = &LOC::Locale::getLocale($tabCountryInfos->{'locale.id'}, undef, [POSIX::LC_MESSAGES, POSIX::LC_CTYPE]);

            my $oldLabel = $tabOldAppealInfos ? $tabOldAppealInfos->{'fullLabel'} : $locale->t('Non facturable');
            $oldLabel =~ s/<%percent>/%s %%/;
            my $newLabel = $tabNewAppealInfos ? $tabNewAppealInfos->{'fullLabel'} : $locale->t('Non facturable');
            $newLabel =~ s/<%percent>/%s %%/;

            # Modification de la gestion de recours
            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDAPPEAL,
                'old'     => {
                    'type'  => $oldAppealTypeId,
                    'value' => $oldValue
                },
                'new'     => {
                    'type'  => $appealTypeId,
                    'value' => $value
                },
                'content' => (sprintf($oldLabel, $oldValue * 100) .
                              ' → ' .
                              sprintf($newLabel, $value * 100))
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
        }
    }
    return 1;
}

# Function: _updateCleaning
# Mise à jour du nettoyage
#
# Parameters:
# LOC::Db::Adapter $db             - Connexion à la base de données
# string           $countryId      - Pays
# int              $contractId     - Id du contrat
# int              $typeId         - Id du type de nettoyage
# float            $amount         - Montant du nettoyage
# bool             $isModelChanged - Indique si le modèle de machine a changé
# bool             $hasRight       - Indique si on a droit ou pas à la modification du nettoyage
# int              $userId         - Utilisateur responsable de la modification
# hashref          $tabErrors      - Tableau des erreurs
# hashref          $tabEndUpds     - Mises à jour de fin
#
# Returns:
# bool - 1 si tout est ok, sinon 0
sub _updateCleaning
{
    my ($db, $countryId, $contractId, $typeId, $amount, $isModelChanged, $hasRight, $userId, $tabErrors, $tabEndUpds) = @_;

    # Si le modèle de machine a changé
    if ($isModelChanged)
    {
        $hasRight = 1;
    }

    # Formatage
    $amount = &LOC::Util::round($amount, 2);

    # Nom du schéma
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my $query = '
SELECT
    IFNULL(tbl_f_contract_cleaning.CONETAUTO, 0) AS `id`,
    IFNULL(tbl_f_contract_cleaning.NETAUTO, 0) AS `type.id`,
    IFNULL(tbl_f_contract_cleaning.CONETMONTANT, IFNULL(tbl_p_cleaning.NETMONTANT, 0)) AS `amount`
FROM ' . $schemaName . '.CONTRAT tbl_f_contract
LEFT JOIN ' . $schemaName . '.CONTRATNETTOYAGE tbl_f_contract_cleaning
ON tbl_f_contract.CONTRATAUTO = tbl_f_contract_cleaning.CONTRATAUTO
LEFT JOIN ' . $schemaName . '.NETTOYAGE tbl_p_cleaning
ON tbl_f_contract_cleaning.NETAUTO = tbl_p_cleaning.NETAUTO
WHERE tbl_f_contract.CONTRATAUTO = ' . $contractId . '
LIMIT 1;';
    my $tabRow = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if (!$tabRow)
    {
        # Erreur fatale !
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    my $id        = $tabRow->{'id'} * 1;
    my $oldTypeId = $tabRow->{'type.id'} * 1;
    my $oldAmount = $tabRow->{'amount'} * 1;

    my $hasModifications = 0;
    if ($typeId != 0)
    {
        if (!$id || $oldTypeId != $typeId || $oldAmount != $amount)
        {
            # Vérification des droits
            if (!$hasRight)
            {
                # Pas les droits !
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            $hasModifications = 1;

            # Récupération des informations sur ce type de nettoyage
            my $tabTypeInfos = &LOC::Cleaning::getTypeInfos($countryId, $typeId);

            # Récupération du taux monétaire
            my $euroRate = &LOC::Country::getEuroRate($countryId);

            my $tabUpdates = {};
            $tabUpdates->{'NETAUTO'} = $typeId * 1;
            if ($tabTypeInfos->{'amount'} != $amount)
            {
                $tabUpdates->{'CONETMONTANT'}   = &LOC::Util::round($amount, 2);
                $tabUpdates->{'CONETMONTANTEU'} = &LOC::Util::round($amount * $euroRate, 2);
            }
            else
            {
                $tabUpdates->{'CONETMONTANT'}   = undef;
                $tabUpdates->{'CONETMONTANTEU'} = undef;
            }

            if ($id)
            {
                if ($db->update('CONTRATNETTOYAGE', $tabUpdates, {'CONETAUTO' => $id}, $schemaName) == -1)
                {
                    # Erreur fatale !
                    push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                    return 0;
                }
            }
            else
            {
                $tabUpdates->{'CONTRATAUTO'} = $contractId * 1;
                $id = $db->insert('CONTRATNETTOYAGE', $tabUpdates, $schemaName);
                if ($id == -1)
                {
                    # Erreur fatale !
                    push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                    return 0;
                }
            }
        }
    }
    else
    {
        # Suppression du nettoyage
        if ($id)
        {
            $hasModifications = ($oldTypeId != 0);

            # Vérification des droits
            if ($hasModifications && !$hasRight)
            {
                # Pas les droits !
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }

            if ($db->delete('CONTRATNETTOYAGE', {'CONETAUTO' => $id}, $schemaName) == -1)
            {
                # Erreur fatale !
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                return 0;
            }
        }
    }

    # Trace
    if (defined $tabEndUpds && $hasModifications)
    {
        # Modification du nettoyage
        my $tabCountryInfos = &LOC::Country::getInfos($countryId);
        my $locale = &LOC::Locale::getLocale($tabCountryInfos->{'locale.id'}, undef, [POSIX::LC_MESSAGES, POSIX::LC_CTYPE]);

        my $subQuery = '(SELECT CAST(CONCAT("%s (", NETLIBELLE,")") AS CHAR CHARACTER SET utf8) FROM ' . $schemaName . '.NETTOYAGE WHERE NETAUTO = %d)';
        my $subQuery0 = '0 (' . $locale->t('Non facturé') . ')';
        my $query = '';
        if ($typeId == 0)
        {
            $query = 'CONCAT(' . sprintf($subQuery, $oldAmount, $oldTypeId) . ', " → ", "' . $subQuery0 . '")';
        }
        elsif ($oldTypeId == 0)
        {
            $query = 'CONCAT("' . $subQuery0 . '", " → ", ' . sprintf($subQuery, $amount, $typeId) . ')';
        }
        else
        {
            $query = 'CONCAT(' . sprintf($subQuery, $oldAmount, $oldTypeId) . ', " → ", ' . sprintf($subQuery, $amount, $typeId) . ')';
        }

        my $tabTrace = {
            'schema'   => $schemaName,
            'code'     => LOC::Log::EventType::UPDCLEANING,
            'old' => {
                'amount' => $oldAmount,
                'typeId' => $oldTypeId
            },
            'new' => {
                'amount' => $amount,
                'typeId' => $typeId
            },
            'content'  => '',
            'sqlQuery' => $query
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
    }

    return 1;
}


# Function: _updateResidues
# Mise à jour de la participation au recyclage
#
# Parameters:
# LOC::Db::Adapter $db                 - Connexion à la base de données
# string           $countryId          - Pays
# int              $contractId         - Id du contrat
# int              $mode               - Mode de calcul de la participation au recyclage
# bool             $isModelChanged     - Indique si le modèle de machine a changé
# bool             $hasRight           - Indique si on a droit ou pas à la modification de la participation au recyclage
# int              $userId             - Utilisateur responsable de la modification
# hashref          $tabErrors          - Tableau des erreurs
# hashref          $tabEndUpds         - Mises à jour de fin
#
# Returns:
# bool - 1 si tout est ok, sinon 0
sub _updateResidues
{
    my ($db, $countryId, $contractId, $mode, $isModelChanged, $hasRight, $userId, $tabErrors, $tabEndUpds) = @_;

    # Si le modèle de machine a changé
    if ($isModelChanged)
    {
        $hasRight = 1;
    }

    # Nom du schéma
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de l'agence et s'il s'agit d'un accessoire, pour le calcul du montant de la participation
    # au recyclage
    my $query = '
SELECT
    tbl_f_contract.AGAUTO AS `agency.id`,
    -tbl_f_contract.CONTRATDECHETSFACT AS `mode`,
    tbl_f_contract.CONTRATDECHETSVAL AS `value`
FROM ' . $schemaName . '.CONTRAT tbl_f_contract
WHERE tbl_f_contract.CONTRATAUTO = ' . $contractId . ';';
    my $tabRow = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if (!$tabRow)
    {
        # Erreur fatale !
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    my $agencyId              = $tabRow->{'agency.id'};
    my $oldMode               = $tabRow->{'mode'} * 1;

    # Y'a-t-il eu une modification ?
    if ($oldMode != $mode)
    {
        # Vérification des droits de modification de la participation au recyclage
        if (!$hasRight)
        {
            # Pas les droits !
            push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
            return 0;
        }

        my $oldValue            = $tabRow->{'value'} * 1;

        my $valueDate           = &getValueDate($countryId, $contractId);
        my $defaultResiduesMode = &LOC::Characteristic::getAgencyValueByCode('DECHETSMODE', $agencyId, $valueDate);
        # Vérification du nouveau mode par rapport aux possibilités sur le contrat
        if ($mode != LOC::Common::RESIDUES_MODE_NONE && $mode != $defaultResiduesMode)
        {
            # Pas les droits !
            push(@{$tabErrors->{'fatal'}}, 0x0126);
            return 0;
        }

        my $value = undef;
        if ($mode == LOC::Common::RESIDUES_MODE_PRICE)
        {
            $value = &LOC::Characteristic::getAgencyValueByCode('MTDECHETS', $agencyId, $valueDate) * 1;
        }
        elsif ($mode == LOC::Common::RESIDUES_MODE_PERCENT)
        {
            $value = &LOC::Characteristic::getAgencyValueByCode('PCDECHETS', $agencyId, $valueDate) * 1;
        }


        # Mise à jour des tables
        if ($db->update('CONTRAT', {'CONTRATDECHETSFACT' => $mode * -1,
                                    'CONTRATDECHETSVAL'  => $value},
                                   {'CONTRATAUTO*' => $contractId}, $schemaName) == -1)
        {
            # Erreur fatale !
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            return 0;
        }


        # Trace
        if (defined $tabEndUpds)
        {
            my $tabCountryInfos = &LOC::Country::getInfos($countryId);
            my $locale = &LOC::Locale::getLocale($tabCountryInfos->{'locale.id'}, undef, [POSIX::LC_MESSAGES, POSIX::LC_CTYPE]);

            my $tabOldResiduesInvoiceInfos = {};
            if ($oldMode == LOC::Common::RESIDUES_MODE_PRICE)
            {
                $tabOldResiduesInvoiceInfos = {
                    'type'    => $locale->t('Montant forfaitaire'),
                    'format'  => $oldValue
                };
            }
            elsif ($oldMode == LOC::Common::RESIDUES_MODE_PERCENT)
            {
                $tabOldResiduesInvoiceInfos = {
                    'type'    => $locale->t('Pourcentage'),
                    'format'  => $oldValue * 100 . '%'
                };
            }

            my $tabNewResiduesInvoiceInfos = {};
            if ($mode == LOC::Common::RESIDUES_MODE_PRICE)
            {
                $tabNewResiduesInvoiceInfos = {
                    'type'    => $locale->t('Montant forfaitaire'),
                    'format'  => $value
                };
            }
            elsif ($mode == LOC::Common::RESIDUES_MODE_PERCENT)
            {
                $tabNewResiduesInvoiceInfos = {
                    'type'    => $locale->t('Pourcentage'),
                    'format'  => $value * 100 . '%'
                };
            }

            # Modification de la participation au recyclage
            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDRESIDUESMODE,
                'old'     => {
                    'mode'  => $oldMode,
                    'value' => $oldValue
                },
                'new'     => {
                    'mode'  => $mode,
                    'value' => $value
                },
                'content' => ($oldMode ? sprintf('%s : %s', $tabOldResiduesInvoiceInfos->{'type'}, $tabOldResiduesInvoiceInfos->{'format'}) :
                                                      $locale->t('Non facturé'))  .
                             ' → ' .
                             ($mode ?  sprintf('%s : %s', $tabNewResiduesInvoiceInfos->{'type'}, $tabNewResiduesInvoiceInfos->{'format'}) :
                                                      $locale->t('Non facturé'))
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
        }
    }
    return 1;
}


# Function: _updateFuel
# Mise à jour du carburant
#
# Parameters:
# LOC::Db::Adapter $db         - Connexion à la base de données
# string           $countryId  - Pays
# int              $contractId - Id du contrat
# float            $quantity   - Quantité de carburant
# bool             $hasRight   - Indique si on a droit ou pas à la modification du carburant
# int              $userId     - Utilisateur responsable de la modification
# hashref          $tabErrors  - Tableau des erreurs
# hashref          $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool - 1 si tout est ok, sinon 0
sub _updateFuel
{
    my ($db, $countryId, $contractId, $quantity, $hasRight, $userId, $tabErrors, $tabEndUpds) = @_;

    # Nom du schéma de base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my $query = '
SELECT
    IFNULL(tbl_f_fuel.CARBUAUTO, 0) AS `id`,
    IFNULL(tbl_f_fuel.CAQTE, 0) AS `quantity`,
    IFNULL(tbl_f_fuel.CAMONTANTFR, 0) AS `amount`,
    tbl_p_machinefamily.FAMAENERGIE AS `energy`,
    tbl_f_contract.AGAUTO AS `agency.id`
FROM ' . $schemaName . '.CONTRAT tbl_f_contract
LEFT JOIN AUTH.MODELEMACHINE tbl_p_model
ON tbl_f_contract.MOMAAUTO = tbl_f_contract.MOMAAUTO
INNER JOIN FAMILLEMACHINE tbl_p_machinefamily
ON tbl_p_machinefamily.FAMAAUTO = tbl_p_model.FAMAAUTO
LEFT JOIN ' . $schemaName . '.CARBURANT tbl_f_fuel
ON tbl_f_contract.CARBUAUTO = tbl_f_fuel.CARBUAUTO
WHERE tbl_f_contract.CONTRATAUTO = ' . $contractId . ';';
    my $tabRow = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if (!$tabRow)
    {
        # Erreur fatale !
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    my $id          = $tabRow->{'id'} * 1;
    my $oldQuantity = $tabRow->{'quantity'} * 1;
    my $oldAmount   = &LOC::Util::round($tabRow->{'amount'} * 1, 2);
    my $energy      = $tabRow->{'energy'}; # Energie de la machine ou du modèle
    my $agencyId    = $tabRow->{'agency.id'};

    # Si le carburant n'a jamais été saisie auparavant et que rien n'est saisie alors on ne fait rien
    if ($quantity eq '' && !$id)
    {
        return 1;
    }

    # Formatage
    $quantity = ($quantity eq '' ? 0 : &LOC::Util::round($quantity, 2));

    if (!$id || $oldQuantity != $quantity)
    {
        # On ne peut saisir du carburant que sur les contrats portant sur un modèle le nécessitant
        my $tabFuelEnergies = &LOC::MachinesFamily::getFuelEnergiesList();
        if (!&LOC::Util::in_array($energy, $tabFuelEnergies))
        {
            # Erreur fatale !
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            return 0;
        }
        # Vérification des droits
        if (!$hasRight)
        {
            # Pas les droits !
            push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
            return 0;
        }

        # Montant du carburant
        my $tabCountryData = &LOC::Country::getInfos($countryId);
        my $amount = $tabCountryData->{'fuelUnitPrice'} * $quantity;

        # Récupération du taux monétaire
        my $euroRate = &LOC::Country::getEuroRate($countryId);

        my $tabUpdates = {
            'PACODE'      => $countryId,
            'CAQTE'       => $quantity,
            'CAMONTANTFR' => &LOC::Util::round($amount, 2),
            'CAMONTANTEU' => &LOC::Util::round($amount * $euroRate, 2)
        };
        if ($id)
        {
            if ($db->update('CARBURANT', $tabUpdates, {'CARBUAUTO' => $id}, $schemaName) == -1)
            {
                # Erreur fatale !
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                return 0;
            }
        }
        else
        {
            $id = $db->insert('CARBURANT', $tabUpdates, $schemaName);
            if ($id == -1)
            {
                # Erreur fatale !
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                return 0;
            }
            elsif ($db->update('CONTRAT', {'CARBUAUTO*' => $id}, {'CONTRATAUTO*' => $contractId}, $schemaName) == -1)
            {
                # Erreur fatale !
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                return 0;
            }

            # Décrémente l'alerte "Carburants à saisir"
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::FUELS, -1);
        }

        # Trace
        if (defined $tabEndUpds)
        {
            # Modification du carburant
            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDFUEL,
                'old'     => {
                    'amount' => $oldAmount,
                    'quantity' => $oldQuantity
                },
                'new'     => {
                    'amount' => $amount,
                    'quantity' => $quantity
                },
                'content' => $oldAmount . ' (' . $oldQuantity . ' L)' .
                             ' → ' .
                             $amount . ' (' . $quantity . ' L)'
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
        }
    }

    return 1;
}


# Function: _updateMachineAllocation
# Mise à jour de l'attribution de la machine sur le contrat
#
# Parameters:
# LOC::Db::Adapter $db                - Connexion à la base de données
# string           $countryId         - Pays
# int              $contractId        - Id du contrat
# int|undef        $machineId         - Id de la machine à atribuer/associer (ou vide pour désattribuer/désassocier)
# bool             $isFinalAllocation - indique s'il s'agit d'une attribution définitive
# bool             $isModelChanged    - Indique si le modèle de machine a changé
# bool             $hasRight          - Indique si on a droit ou pas à la modification de l'attribution de la machine
# int              $userId            - Utilisateur responsable de la modification
# hashref          $tabErrors         - Tableau des erreurs
# hashref          $tabEndUpds        - Mises à jour de fin
#
# Returns:
# bool - 1 si tout est ok, sinon 0
sub _updateMachineAllocation
{
    my ($db, $countryId, $contractId, $machineId, $isFinalAllocation, $isModelChanged, $hasRight, $userId, $tabErrors, $tabEndUpds) = @_;

    # Nom du schéma
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my $query = '
SELECT
    tbl_f_contract.AGAUTO AS `agency.id`,
    tbl_f_contract.CONTRATCODE AS `contract.code`,
    tbl_f_contract.ETCODE AS `state.id`,
    -tbl_f_contract.CONTRATMACHATTR AS `isFinalAllocation`,
    tbl_f_contract.MAAUTO_SOUHAITE AS `wishedMachine.id`,
    tbl_f_contract.MOMAAUTO AS `model.id`,
    tbl_f_contract.MAAUTO AS `machine.id`,
    tbl_f_contract.CONTRATVERIFIER AS `isLocked`,
    tbl_f_contract.CONTRATPROFORMAOK AS `proformaFlags`,
    tbl_f_customer.CLCODE AS `customer.code`,
    tbl_f_customer.CLCOLLECTIF AS `customer.controlAccount`,
    -tbl_f_customer.CLPROFORMA AS `customer.isProformaRequired`,
    tbl_f_amount.MOHTFRS AS `total`
FROM ' . $schemaName . '.CONTRAT tbl_f_contract
INNER JOIN ' . $schemaName . '.MONTANT tbl_f_amount
ON tbl_f_contract.MOAUTO = tbl_f_amount.MOAUTO
LEFT JOIN ' . $schemaName . '.CLIENT tbl_f_customer
ON tbl_f_contract.CLAUTO = tbl_f_customer.CLAUTO
WHERE tbl_f_contract.CONTRATAUTO = ' . $contractId . ';';

    my $tabRow = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if (!$tabRow)
    {
        # Erreur fatale !
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    my $agencyId             = $tabRow->{'agency.id'};
    my $contractCode         = $tabRow->{'contract.code'};
    my $stateId              = $tabRow->{'state.id'};
    my $oldMachineId         = ($tabRow->{'machine.id'} ? $tabRow->{'machine.id'} * 1 : undef);
    my $oldIsFinalAllocation = $tabRow->{'isFinalAllocation'} * 1;
    my $wishedMachineId      = $tabRow->{'wishedMachine.id'};
    my $modelId              = $tabRow->{'model.id'} * 1;
    my $isLocked             = $tabRow->{'isLocked'} * 1;
    my $proformaFlags        = $tabRow->{'proformaFlags'} * 1;
    my $isNewAccount         = &LOC::Customer::isNewAccount($countryId, $tabRow->{'customer.code'},
                                                            $tabRow->{'customer.controlAccount'});
    my $isProformaRequired   = $tabRow->{'customer.isProformaRequired'} * 1;
    my $total                = $tabRow->{'total'} * 1;

    # Si le modèle de machine a changé, alors on désattribue/désassocie la machine sur le contrat
    if (!$machineId)
    {
        $isFinalAllocation = 0;
    }

    # Association/Désassociation ou Attribution/Désattribution de la machine
    my $isMachineChanged = ($oldMachineId != $machineId);
    if ($isModelChanged || $isMachineChanged || $oldIsFinalAllocation != $isFinalAllocation)
    {
        my $tabMachineInfos;

        # Si on lie une machine
        if ($machineId)
        {
            # Vérification de l'état du contrat (doit être en "Pré-contrat")
            if ($stateId ne STATE_PRECONTRACT)
            {
                push(@{$tabErrors->{'fatal'}}, 0x0310);
                return 0;
            }

            if (!$isModelChanged && $isMachineChanged && $oldIsFinalAllocation)
            {
                # Pas possible de réattribuer une machine
                push(@{$tabErrors->{'fatal'}}, 0x0308);
                return 0;
            }

            # Récupération des informations sur la machine
            $tabMachineInfos = &LOC::Machine::getInfos(
                                        $countryId, $machineId,
                                        LOC::Machine::GETINFOS_BASE |
                                        LOC::Machine::GETINFOS_TELEMATICS);

            # Vérification du modèle de machine par rapport au contrat
            if ($tabMachineInfos->{'model.id'} != $modelId)
            {
                push(@{$tabErrors->{'fatal'}}, 0x0309);
                return 0;
            }

            # Il n'y avait pas de machine associée sur le contrat
            if (!$oldMachineId)
            {
                # Décrémente l'alerte des machines à attribuer
                &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::MACHINESTOALLOCATE, -1);
            }

            # Dans le cas d'une attribution de machine :
            if ($isFinalAllocation)
            {
                # Vérification
                # - Machine en Hors parc
                # - Machine non disponible et non en transfert
                # - Machine dispo avec :
                # -- agence de la machine différente de l'agence du contrat et pas de visibilité
                # -- agence de visibilité différente de l'agence du contrat
                # - Machine en transfert avec :
                # -- pas de visibilité
                # -- agence de visibilité différente de l'agence du contrat
                # -- déjà attribuée à un contrat
                if ($tabMachineInfos->{'isOutOfPark'} ||
                    ($tabMachineInfos->{'state.id'} ne LOC::Machine::STATE_AVAILABLE &&
                     $tabMachineInfos->{'state.id'} ne LOC::Machine::STATE_TRANSFER) ||
                     ($tabMachineInfos->{'state.id'} eq LOC::Machine::STATE_AVAILABLE &&
                      (($tabMachineInfos->{'agency.id'} ne $agencyId && !$tabMachineInfos->{'visibleOnAgency.id'}) ||
                           $tabMachineInfos->{'visibleOnAgency.id'} && $tabMachineInfos->{'visibleOnAgency.id'} ne $agencyId)) ||
                     ($tabMachineInfos->{'state.id'} eq LOC::Machine::STATE_TRANSFER &&
                      (!$tabMachineInfos->{'visibleOnAgency.id'} ||
                           $tabMachineInfos->{'visibleOnAgency.id'} && $tabMachineInfos->{'visibleOnAgency.id'} ne $agencyId ||
                           $tabMachineInfos->{'isAffectedOnPrecontract'})))
                {
                    push(@{$tabErrors->{'fatal'}}, 0x0307);
                }
                # si une machine n'était pas déjà attribuée (pas de machine ou machine associée), on vérifie que cela est possible
                if (!$oldIsFinalAllocation)
                {
                    # - RE loc/transp
                    my $tabRentTariffInfos = &LOC::Tariff::Rent::getInfos($countryId, 'contract', $contractId, LOC::Tariff::Rent::GETINFOS_BASE);
                    if ($tabRentTariffInfos->{'specialReductionStatus'} eq LOC::Tariff::Rent::REDUCTIONSTATE_PENDING)
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x0301);
                    }
                    my $tabTrspTariffInfos = &LOC::Tariff::Transport::getInfos($countryId, 'contract', $contractId, LOC::Tariff::Rent::GETINFOS_BASE);
                    if ($tabTrspTariffInfos->{'specialReductionStatus'} eq LOC::Tariff::Transport::REDUCTIONSTATE_PENDING)
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x0302);
                    }
                    # - Client avec ouverture de compte en attente
                    if ($isLocked || $isNewAccount)
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x0303);
                    }
                    # - vérification Pro forma
                    # -- Pro forma non générée
                    if (($proformaFlags & LOC::Proforma::DOCFLAG_CT_ISCUSTOMERPFREQONCREATION &&
                         $total > 0 && $isProformaRequired) &&
                         !($proformaFlags & LOC::Proforma::DOCFLAG_EXISTACTIVED))
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x0304);
                    }
                    # -- Pro forma non à jour
                    if ($proformaFlags & LOC::Proforma::DOCFLAG_EXISTACTIVEDEXPIRED)
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x0305);
                    }
                    # -- Pro forma non justifiée
                    if ($proformaFlags & LOC::Proforma::DOCFLAG_EXISTACTIVED &&
                        !($proformaFlags & LOC::Proforma::DOCFLAG_EXISTACTIVEDEXPIRED) &&
                        !($proformaFlags & LOC::Proforma::DOCFLAG_ALLACTIVEDFINALIZED))
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x0306);
                    }
                }

                if (@{$tabErrors->{'fatal'}} > 0)
                {
                    return 0;
                }

                # Mise à jour de l'état de la machine si elle n'est pas en Transfert => En livraison et mise à jour du dernier contrat sur lequel la machine a été attribuée
                my $tabMachineErrors = $tabErrors->{'modules'}->{'machine'};
                my $tabDataMachine = {
                    'contract.id' => $contractId
                };
                if ($tabMachineInfos->{'state.id'} ne LOC::Machine::STATE_TRANSFER)
                {
                     $tabDataMachine->{'state.id'} = LOC::Machine::STATE_DELIVERY;
                }
                if (!&LOC::Machine::update($countryId, $machineId, $tabDataMachine, $userId, {}, $tabMachineErrors, $tabEndUpds))
                {
                    push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_MACHINE);
                    # Erreur fatale ?
                    if (&LOC::Util::in_array(0x0001, $tabMachineErrors->{'fatal'}))
                    {
                        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                    }
                    return 0;
                }

                # Incrémente l'alerte des précontrats à éditer
                &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::FIRSTPRINTRENTCONTRACTS, +1);

                # Trace de l'attribution machine sur un contrat
                # - trace sur le contrat
                my $tabTrace = {
                    'schema'  => $schemaName,
                    'code'    => LOC::Log::EventType::MACHINEALLOCATION,
                    'old'     => '',
                    'new'     => $machineId,
                    'content' => '0 → ' . $machineId
                };
                &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
                # - trace sur la machine
                my $tabTrace = {
                    'schema'  => $schemaName,
                    'code'    => LOC::Log::EventType::CONTRACTALLOCATION,
                    'old'     => '',
                    'new'     => $contractId,
                    'content' => ' → ' . $contractCode
                };
                &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $machineId, $tabTrace);

                # Mise à jour des stocks du modèle de la nouvelle machine
                # - réservé
                &LOC::EndUpdates::updateModelStock($tabEndUpds, $agencyId, $tabMachineInfos->{'model.id'}, 'nbBooked', -1);
                # - réservable
                # -- Si la machine est en transfert, il faut l'enlever manuellement des stocks réservables de l'agence de visibilité
                if ($tabMachineInfos->{'state.id'} eq LOC::Machine::STATE_TRANSFER)
                {
                    &LOC::EndUpdates::updateModelStock($tabEndUpds, $tabMachineInfos->{'visibleOnAgency.id'},
                                                            $tabMachineInfos->{'model.id'}, 'nbBookable', -1);
                }

            }
        }


        # Dans le cas où une machine était attribuée :
        if ($oldMachineId)
        {
            if (!$machineId)
            {
                # Incrémente l'alerte des machines à attribuer
                &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::MACHINESTOALLOCATE, +1);
            }

            if ($oldIsFinalAllocation)
            {
                # Récupération des informations sur l'ancienne machine
                my $tabOldMachineInfos = &LOC::Machine::getInfos($countryId, $oldMachineId, LOC::Machine::GETINFOS_BASE);

                # Mise à jour de l'état de la machine => Disponible dans le cas où la machine n'est pas en transfert
                my $tabMachineErrors = $tabErrors->{'modules'}->{'machine'};
                if ($tabOldMachineInfos->{'state.id'} ne LOC::Machine::STATE_TRANSFER &&
                    !&LOC::Machine::update($countryId, $oldMachineId, {'state.id' => LOC::Machine::STATE_AVAILABLE}, $userId, {}, $tabMachineErrors, $tabEndUpds))
                {
                    push(@{$tabErrors->{'fatal'}}, ERROR_MODULE_MACHINE);
                    # Erreur fatale ?
                    if (&LOC::Util::in_array(0x0001, $tabMachineErrors->{'fatal'}))
                    {
                        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                    }
                    return 0;
                }

                # Mise à jour de la machine souhaitée
                # Si on désattribue la machine sur le contrat et qu'on a une machine souhaitée
                if (!$machineId && $wishedMachineId)
                {
                    # Récupère la liste des machines souhaitées possibles
                    my $tabFilters = {
                        'id'       => $wishedMachineId,
                        'modelId'  => $modelId,
                        'agencyId' => $agencyId,
                        'stateId'  => [
                            LOC::Machine::STATE_RENTED,
                            LOC::Machine::STATE_AVAILABLE,
                            LOC::Machine::STATE_RECOVERY,
                            LOC::Machine::STATE_REVIEW,
                            LOC::Machine::STATE_CONTROL,
                            LOC::Machine::STATE_BROKEN,
                            LOC::Machine::STATE_CHECK,
                            LOC::Machine::STATE_TRANSFER,
                            LOC::Machine::STATE_DELIVERY
                        ]
                    };
                    my $nbWishedMachines = &LOC::Machine::getList($countryId, LOC::Util::GETLIST_COUNT, $tabFilters, LOC::Machine::GETINFOS_BASE);
                    # si la machine souhaitée n'est pas dans la liste des machines souhaitées possibles
                    if ($nbWishedMachines == 0)
                    {
                        # Historisation:
                        my $tabTrace = {
                            'schema'  => $schemaName,
                            'code'    => LOC::Log::EventType::CHANGEWISHEDMACHINE,
                            'old'     => $wishedMachineId,
                            'new'     => undef,
                            'content' => $wishedMachineId . ' →'
                        };
                        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
                        $wishedMachineId = undef;
                    }
                }

                # Décrémente l'alerte des précontrats à éditer
                &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::FIRSTPRINTRENTCONTRACTS, -1);

                # Trace de l'attribution machine sur un contrat
                # - trace sur le contrat
                my $tabTrace = {
                    'schema'  => $schemaName,
                    'code'    => LOC::Log::EventType::MACHINEDEALLOCATION,
                    'old'     => $oldMachineId,
                    'new'     => '',
                    'content' => $oldMachineId . ' → 0'
                };
                &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
                # - trace sur la machine
                my $tabTrace = {
                    'schema'  => $schemaName,
                    'code'    => LOC::Log::EventType::CONTRACTDEALLOCATION,
                    'old'     => $contractId,
                    'new'     => '',
                    'content' => $contractCode . ' → '
                };
                &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $oldMachineId, $tabTrace);

                # Mise à jour des stocks du modèle de l'ancienne machine
                # - réservé
                &LOC::EndUpdates::updateModelStock($tabEndUpds, $agencyId, $tabOldMachineInfos->{'model.id'}, 'nbBooked', +1);
                # - réservable
                # -- Si la machine est en transfert, il faut la rajouter manuellement aux stocks réservables de l'agence de visibilité
                if ($tabOldMachineInfos->{'state.id'} eq LOC::Machine::STATE_TRANSFER)
                {
                    &LOC::EndUpdates::updateModelStock($tabEndUpds, $tabOldMachineInfos->{'visibleOnAgency.id'},
                                                            $tabOldMachineInfos->{'model.id'}, 'nbBookable', +1);
                }
            }
        }


        # Mise à jour sur le contrat
        my $tabUpdates = {
            'MAAUTO'          => ($machineId ? $machineId : 0),
            'CONTRATMACHATTR' => ($isFinalAllocation ? -1 : 0),
            'MAAUTO_SOUHAITE' => $wishedMachineId
        };

        if ($db->update('CONTRAT', $tabUpdates, {'CONTRATAUTO*' => $contractId}, $schemaName) == -1)
        {
            # Erreur fatale !
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            return 0;
        }

        # Impact sur les états des lieux
        if ($machineId != $oldMachineId)
        {
            &LOC::MachineInventory::onExternalChanges($countryId, {
                'contract' => {
                    'id'    => $contractId,
                    'event' => 'update',
                    'props' => {
                        'machine.id' => {'old' => $oldMachineId, 'new' => $machineId}
                    }
                }
            }, $userId, undef, $tabEndUpds);
        }

    }

    return 1;
}


# Function: _updateRentServices
# Mise à jour des services de location et de transport
#
# Parameters:
# LOC::Db::Adapter $db                   - Connexion à la base de données
# string           $countryId            - Pays
# int              $contractId           - Id du contrat
# arrayref         $tabRentServices      - Tableau des services de location et transport
# float            $rentServiceMinAmount - Montant minimum d'un service pour l'utilisateur en cours
# bool             $hasAddRight          - Indique si on a droit ou pas à l'ajout de services
# bool             $hasEditRight         - Indique si on a droit ou pas à la modification de services
# int              $userId               - Utilisateur responsable de la modification
# hashref          $tabErrors            - Tableau des erreurs
# hashref          $tabEndUpds           - Mises à jour de fin
#
# Returns:
# bool - 1 si tout est ok, sinon 0
sub _updateRentServices
{
    my ($db, $countryId, $contractId, $tabRentServices, $rentServiceMinAmount, $hasAddRight, $hasEditRight, $userId, $tabErrors, $tabEndUpds) = @_;

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my $query = '
SELECT
    `rcs_id` AS `id`,
    `rsr_id` AS `rentService.id`,
    `rcs_sta_id` AS `state.id`,
    `rsr_name` AS `rentService.name`,
    `rcs_comment` AS `comment`,
    `rcs_amount` AS `amount`,
     rcs_date_invoicing AS `invoicingDate`
FROM f_rentcontract_rentservice
LEFT JOIN ' . $schemaName . '.p_rentservice
ON rsr_id = rcs_rsr_id
WHERE rcs_rct_id = ' . $contractId . ';';
    my $tabOldRentServices = $db->fetchAssoc($query, {}, 'id');
    if (!$tabOldRentServices)
    {
        # Erreur fatale !
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }


    my $nbRentServices = @$tabRentServices;
    for (my $i = 0; $i < $nbRentServices; $i++)
    {
        my $id = $tabRentServices->[$i]->{'id'};
        if ($id && !exists $tabOldRentServices->{$id})
        {
            # Erreur fatale !
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            return 0;
        }

        # Formatage des données de comparaison
        $tabOldRentServices->{$id}->{'amount'} *= 1;
        $tabRentServices->[$i]->{'amount'} *= 1;
        $tabOldRentServices->{$id}->{'rentService.id'} *= 1;
        $tabRentServices->[$i]->{'rentService.id'} *= 1;
        $tabRentServices->[$i]->{'comment'} = &LOC::Util::trim($tabRentServices->[$i]->{'comment'});

        # Si c'est une mise à jour et que le montant est changé
        if ($id && $tabOldRentServices->{$id}->{'amount'} != $tabRentServices->[$i]->{'amount'})
        {
            # Vérification que le nouveau montant respecte le seuil minimum autorisé pour la personne
            if (defined($rentServiceMinAmount) && $tabRentServices->[$i]->{'amount'} < $rentServiceMinAmount)
            {
                # Erreur fatale !
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                return 0;
            }
        }

        my $hasModifications;
        if ($id)
        {
            $hasModifications = ($tabOldRentServices->{$id}->{'state.id'} ne $tabRentServices->[$i]->{'state.id'} ||
                                 $tabOldRentServices->{$id}->{'rentService.id'} != $tabRentServices->[$i]->{'rentService.id'} ||
                                 $tabOldRentServices->{$id}->{'comment'} ne $tabRentServices->[$i]->{'comment'} ||
                                 $tabOldRentServices->{$id}->{'amount'} != $tabRentServices->[$i]->{'amount'});
            if ($hasModifications)
            {
                # Vérification des droits
                if (!$hasEditRight)
                {
                    # Pas les droits !
                    push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                    return 0;
                }
                my $tabUpdates = {'rcs_sta_id'  => $tabRentServices->[$i]->{'state.id'},
                                  'rcs_rsr_id'  => $tabRentServices->[$i]->{'rentService.id'},
                                  'rcs_comment' => $tabRentServices->[$i]->{'comment'},
                                  'rcs_amount'  => $tabRentServices->[$i]->{'amount'}};
                if ($db->update('f_rentcontract_rentservice', $tabUpdates, {'rcs_id' => $id}, $schemaName) == -1)
                {
                    # Erreur fatale !
                    push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                    return 0;
                }

                # Traces
                if (defined($tabEndUpds))
                {
                    if ($tabOldRentServices->{$id}->{'state.id'} ne $tabRentServices->[$i]->{'state.id'} &&
                        $tabRentServices->[$i]->{'state.id'} eq LOC::Common::RentService::STATE_DELETED)
                    {
                        my $tabTrace = {
                            'schema'  => $schemaName,
                            'code'    => LOC::Log::EventType::DELRENTSERVICE,
                            'old'     => {
                                'name'   => $tabOldRentServices->{$id}->{'rentService.name'},
                                'amount' => $tabOldRentServices->{$id}->{'amount'}
                            },
                            'new'     => undef,
                            'content' => sprintf('%s (%s)', $tabOldRentServices->{$id}->{'rentService.name'},
                                                            $tabOldRentServices->{$id}->{'amount'})
                        };
                        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
                    }
                    else
                    {
                        my $tabTrace = {
                            'schema'  => $schemaName,
                            'code'    => LOC::Log::EventType::UPDRENTSERVICE,
                            'old'     => {
                                'name'    => $tabOldRentServices->{$id}->{'rentService.name'},
                                'comment' => $tabOldRentServices->{$id}->{'comment'},
                                'amount'  => $tabOldRentServices->{$id}->{'amount'}
                            },
                            'new'     => {
                                'name'    => $tabRentServices->[$i]->{'rentService.name'},
                                'comment' => $tabRentServices->[$i]->{'comment'},
                                'amount'  => $tabRentServices->[$i]->{'amount'}
                            },
                            'content' => sprintf('%s (%s) → %s (%s)', $tabOldRentServices->{$id}->{'rentService.name'},
                                                                       $tabOldRentServices->{$id}->{'amount'},
                                                                       $tabRentServices->[$i]->{'rentService.name'},
                                                                       $tabRentServices->[$i]->{'amount'})
                        };
                        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
                    }
                }
            }
        }
        else
        {
            $hasModifications = 1;

            # Vérification des droits
            if (!$hasAddRight)
            {
                # Pas les droits !
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            my $tabUpdates = {'rcs_sta_id'  => $tabRentServices->[$i]->{'state.id'},
                              'rcs_rsr_id'  => $tabRentServices->[$i]->{'rentService.id'},
                              'rcs_rct_id'  => $contractId,
                              'rcs_comment' => $tabRentServices->[$i]->{'comment'},
                              'rcs_amount'  => $tabRentServices->[$i]->{'amount'},
                              'rcs_cur_id*' => '(SELECT cty_cur_id FROM frmwrk.p_country WHERE cty_id = "' . $countryId . '")'};
            if ($db->insert('f_rentcontract_rentservice', $tabUpdates, $schemaName) == -1)
            {
                # Erreur fatale !
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                return 0;
            }

            # Traces
            if (defined($tabEndUpds))
            {
                if ($tabRentServices->[$i]->{'state.id'} eq LOC::Common::RentService::STATE_UNBILLED)
                {
                    my $tabTrace = {
                        'schema'  => $schemaName,
                        'code'    => LOC::Log::EventType::ADDRENTSERVICE,
                        'old'     => undef,
                        'new'     => {
                            'name'   => $tabRentServices->[$i]->{'rentService.name'},
                            'amount' => $tabRentServices->[$i]->{'amount'}
                        },
                        'content' => sprintf('%s (%s)', $tabRentServices->[$i]->{'rentService.name'},
                                                        $tabRentServices->[$i]->{'amount'})
                    };
                    &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
                }
            }
        }
    }

    return 1;
}


# Function: _updateRepairDays
# Mise à jour des jours de remise en état
#
# Parameters:
# LOC::Db::Adapter $db            - Connexion à la base de données
# string           $countryId     - Pays
# int              $contractId    - Id du contrat
# arrayref         $tabRepairDays - Tableau des jours de remise en état
# bool             $hasRight      - Indique si on a droit ou pas à la modification des jours de remise en état
# int              $userId        - Utilisateur responsable de la modification
# hashref          $tabErrors     - Tableau des erreurs
# hashref          $tabEndUpds    - Mises à jour de fin
#
# Returns:
# bool - 1 si tout est ok, sinon 0
sub _updateRepairDays
{
    my ($db, $countryId, $contractId, $tabRepairDays, $hasRight, $userId, $tabErrors, $tabEndUpds) = @_;

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my $query = '
SELECT
    crd_id AS `id`,
    crd_rps_id AS `sheet.id`,
    crd_number AS `number`,
    crd_date_invoicing AS `invoicingDate`,
    crd_sta_id AS `state.id`
FROM ' . $schemaName . '.CONTRAT tbl_f_contract
LEFT JOIN ' . $schemaName . '.f_rentcontractrepairday
ON tbl_f_contract.CONTRATAUTO = crd_rct_id
WHERE tbl_f_contract.CONTRATAUTO = ' . $contractId . ';';
    my $tabOldRepairDays = $db->fetchAssoc($query, {}, 'id');
    if (!$tabOldRepairDays)
    {
        # Erreur fatale !
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    my $nbRepairDays = @$tabRepairDays;
    for (my $i = 0; $i < $nbRepairDays; $i++)
    {
        my $id = $tabRepairDays->[$i]->{'id'};
        if (!$id || !exists $tabOldRepairDays->{$id})
        {
            # Erreur fatale !
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            return 0;
        }

        if ($tabOldRepairDays->{$id}->{'state.id'} ne $tabRepairDays->[$i]->{'state.id'})
        {
            # Vérification des droits
            if (!$hasRight)
            {
                # Pas les droits !
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }

            # Mise à jour de la ligne
            if ($db->update('f_rentcontractrepairday',
                            {'crd_sta_id' => $tabRepairDays->[$i]->{'state.id'}},
                            {'crd_id' => $id}, $schemaName) == -1)
            {
                # Erreur fatale !
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                return 0;
            }

            # Trace
            if ($tabOldRepairDays->{$id}->{'state.id'} ne $tabRepairDays->[$i]->{'state.id'})
            {
                if ($tabRepairDays->[$i]->{'state.id'} eq LOC::Contract::RepairDay::STATE_APPROVED)
                {
                    my $tabTrace = {
                        'schema'  => $schemaName,
                        'code'    => LOC::Log::EventType::INVOICEREPAIRDAY,
                        'old'     => {
                            'days' => $tabOldRepairDays->{$id}->{'number'},
                            'id'   => $tabOldRepairDays->{$id}->{'sheet.id'}
                        },
                        'new'     => undef,
                        'content' => sprintf('%d j (fiche %d)', $tabOldRepairDays->{$id}->{'number'},
                                                                $tabOldRepairDays->{$id}->{'sheet.id'})
                    };
                    &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
                }
                elsif ($tabRepairDays->[$i]->{'state.id'} eq LOC::Contract::RepairDay::STATE_CANCELED)
                {
                    my $tabTrace = {
                        'schema'  => $schemaName,
                        'code'    => LOC::Log::EventType::UNINVOICEREPAIRDAY,
                        'old'     => {
                            'days' => $tabOldRepairDays->{$id}->{'number'},
                            'id'   => $tabOldRepairDays->{$id}->{'sheet.id'}
                        },
                        'new'     => undef,
                        'content' => sprintf('%d j (fiche %d)', $tabOldRepairDays->{$id}->{'number'},
                                                                $tabOldRepairDays->{$id}->{'sheet.id'})
                    };
                    &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
                }
            }
        }
    }

    return 1;
}


# Function: _updateRepairServices
# Mise à jour des services de remise en état
#
# Parameters:
# LOC::Db::Adapter $db                - Connexion à la base de données
# string           $countryId         - Pays
# int              $contractId        - Id du contrat
# arrayref         $tabRepairServices - Tableau des services de remise en état
# bool             $hasRight          - Indique si on a droit ou pas à la modification des services de remise en état
# int              $userId            - Utilisateur responsable de la modification
# hashref          $tabErrors         - Tableau des erreurs
# hashref          $tabEndUpds        - Mises à jour de fin
#
# Returns:
# bool - 1 si tout est ok, sinon 0
sub _updateRepairServices
{
    my ($db, $countryId, $contractId, $tabRepairServices, $hasRight, $userId, $tabErrors, $tabEndUpds) = @_;

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my $query = '
SELECT
    crs_id AS `id`,
    crs_rps_id AS `sheet.id`,
    crs_label AS `label`,
    crs_sta_id AS `state.id`,
    crs_date_invoicing AS `invoicingDate`
FROM ' . $schemaName . '.f_rentcontractrepairservice
WHERE f_rentcontractrepairservice.crs_rct_id = ' . $contractId . ';';
    my $tabOldRepairServices = $db->fetchAssoc($query, {}, 'id');
    if (!$tabOldRepairServices)
    {
        # Erreur fatale !
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }


    # État de facturation de tous les autres services
    my $areServicesInvoiced = {};
    foreach my $tabOldService (values(%$tabOldRepairServices))
    {
        if ($tabOldService->{'state.id'} eq LOC::Contract::RepairService::STATE_INVOICED)
        {
            $areServicesInvoiced->{$tabOldService->{'sheet.id'}} = 1;
        }
    }

    my $nbRepairServices = @$tabRepairServices;
    for (my $i = 0; $i < $nbRepairServices; $i++)
    {
        my $id = $tabRepairServices->[$i]->{'id'};
        if (!$id || !exists $tabOldRepairServices->{$id})
        {
            # Erreur fatale !
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            return 0;
        }

        if ($tabOldRepairServices->{$id}->{'state.id'} ne $tabRepairServices->[$i]->{'state.id'})
        {
            # Vérification des droits
            if (!$hasRight)
            {
                # Pas les droits !
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }

            # Erreur si on essaye de modifier l'état alors qu'au moins un des services de la fiche est facturé
            if ($areServicesInvoiced->{$tabOldRepairServices->{$id}->{'sheet.id'}})
            {
                # Erreur fatale !
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                return 0;
            }

            # Mise à jour de l'état
            if ($db->update('f_rentcontractrepairservice',
                            {'crs_sta_id' => $tabRepairServices->[$i]->{'state.id'}},
                            {'crs_id' => $id}, $schemaName) == -1)
            {
                # Erreur fatale !
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                return 0;
            }

            # Trace
            if ($tabOldRepairServices->{$id}->{'state.id'} ne $tabRepairServices->[$i]->{'state.id'})
            {
                if ($tabRepairServices->[$i]->{'state.id'} eq LOC::Contract::RepairService::STATE_APPROVED)
                {
                    my $tabTrace = {
                        'schema'  => $schemaName,
                        'code'    => LOC::Log::EventType::INVOICEREPAIRSERVICE,
                        'old'     => {
                            'label' => $tabOldRepairServices->{$id}->{'label'},
                            'id'    => $tabOldRepairServices->{$id}->{'sheet.id'}
                        },
                        'new'     => undef,
                        'content' => sprintf('%s (fiche %d)', $tabOldRepairServices->{$id}->{'label'},
                                                              $tabOldRepairServices->{$id}->{'sheet.id'})
                    };
                    &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
                }
                elsif ($tabRepairServices->[$i]->{'state.id'} eq LOC::Contract::RepairService::STATE_CANCELED)
                {
                    my $tabTrace = {
                        'schema'  => $schemaName,
                        'code'    => LOC::Log::EventType::UNINVOICEREPAIRSERVICE,
                        'old'     => {
                            'label' => $tabOldRepairServices->{$id}->{'label'},
                            'id'    => $tabOldRepairServices->{$id}->{'sheet.id'}
                        },
                        'new'     => undef,
                        'content' => sprintf('%s (fiche %d)', $tabOldRepairServices->{$id}->{'label'},
                                                              $tabOldRepairServices->{$id}->{'sheet.id'})
                    };
                    &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);
                }
            }
        }
    }

    return 1;
}


# Function: _execTransportAction
# Exécuter une action (réalisation/annulation) sur l'un des côtés du transport du contrat
#
# Parameters:
# string  $countryId      - Pays
# string  $documentType   - Type de document ('estimateLine': ligne de devis, 'contract' : contrat)
# int     $contractId     - Identifiant du contrat
# string  $side           - Coté du transport (Livraison ou Récupération)
# hashref $tabAction      - Paramètre de l'action
# int     $userId         - Id de l'utilisateur
# hashref $tabErrors      - Tableau des erreurs
# hashref $tabEndUpds     - Mises à jour de fin
#
# Returns:
# bool -
sub _execTransportAction
{
    my ($countryId, $documentType, $contractId, $side, $tabAction, $userId, $tabErrors, $tabEndUpds) = @_;

    my $result = 0;
    if ($side eq LOC::Transport::SIDE_DELIVERY)
    {
        if ($tabAction->{'type'} eq 'do')
        {
            $result = &LOC::Transport::doDocumentDelivery($countryId, $documentType, $contractId, $tabAction->{'doLink'}, undef, $userId, $tabErrors, $tabEndUpds);
        }
        elsif ($tabAction->{'type'} eq 'undo')
        {
            $result = &LOC::Transport::undoDocumentDelivery($countryId, $documentType, $contractId, $tabAction->{'doLink'}, $userId, $tabErrors, $tabEndUpds);
        }
    }
    elsif ($side eq LOC::Transport::SIDE_RECOVERY)
    {
        if ($tabAction->{'type'} eq 'do')
        {
            $result = &LOC::Transport::doDocumentRecovery($countryId, $documentType, $contractId, $tabAction->{'doLink'}, undef, $userId, $tabErrors, $tabEndUpds);
        }
        elsif ($tabAction->{'type'} eq 'undo')
        {
            $result = &LOC::Transport::undoDocumentRecovery($countryId, $documentType, $contractId, $tabAction->{'doLink'}, $userId, $tabErrors, $tabEndUpds);
        }

    }

    return $result;
}


# Function: _lockDbRow
# Verrouille la ligne de contrat.
#
# Parameters:
# LOC::Db::Adapter $db         - La connexion à la base de données.
# int              $contractId - L'identifiant du contrat.
#
# Returns:
# bool
sub _lockDbRow
{
    my ($db, $contractId) = @_;

    # Verrou sur le contrat
    my $query = '
SELECT
    CONTRATAUTO
FROM CONTRAT
WHERE CONTRATAUTO = ' . $contractId . '
FOR UPDATE;';

    return ($db->query($query) ? 1 : 0);
}

1;