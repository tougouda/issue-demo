use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Cleaning
# Module permettant d'obtenir les informations sur les nettoyages
package LOC::Cleaning;


# Constants: Récupération d'informations sur les nettoyages
# GETINFOS_BASE - Récupération des informations de base
use constant
{
    GETINFOS_BASE => 0x00000000
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Util;


# Function: getTypeInfos
# Retourne les infos d'un nettoyage
# 
# Parameters:
# string  $countryId  - Pays
# int     $id         - Identifiant du nettoyage
# int     $flags      - Flags (facultatif)
#
# Returns:
# hashref|undef - Retourne un hashref suivant la demande, undef si pas de résultat
sub getTypeInfos
{
    my ($countryId, $id, $flags) = @_;

    my $tabResult = &getTypesList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags);
    return $tabResult->{$id};
}

# Function: getTypesList
# Récupérer la liste des nettoyages
#
# Contenu du hachage:
# - id         => id,
# - label      => libellé,
# - amount     => montant,
# - energy     => énergie,
# - isStandard => Le nettoyage est-il standard ?
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id, energy)
# int     $flags      - Flags
#
# Returns:
# hash|hashref|undef
sub getTypesList
{
    my ($countryId, $format, $tabFilters, $flags) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT
    tbl_p_cleaning.NETAUTO AS `id`,
    tbl_p_cleaning.NETLIBELLE AS `label`,
    tbl_p_cleaning.NETMONTANT AS `amount`,
    tbl_p_cleaning.NETENERGIE AS `energy`,
    -tbl_p_cleaning.NETSTANDARD AS `isStandard`
FROM NETTOYAGE tbl_p_cleaning
WHERE 1';
    if (defined $tabFilters->{'id'})
    {
        $query .= '
AND tbl_p_cleaning.NETAUTO IN (' . $db->quote($tabFilters->{'id'}) . ')';
    }
    if (defined $tabFilters->{'energy'})
    {
        $query .= '
AND (tbl_p_cleaning.NETENERGIE IN (' . $db->quote($tabFilters->{'energy'}) . ') OR tbl_p_cleaning.NETENERGIE IS NULL)';
    }
    $query .= '
ORDER BY `isStandard` DESC, `label`';
    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'} *= 1;
            $tabRow->{'amount'} *= 1;
            $tabRow->{'isStandard'} *= 1;
        }
    }

    return (wantarray ? %$tab : $tab);
}

1;