use utf8;
use open (':encoding(UTF-8)');

# Package : LOC::WebService
# Module permettant d'obtenir les informations sur les pro forma
package LOC::WebService;
use strict;

use LOC::Characteristic;
use LOC::Contract::Rent;
use LOC::Estimate::Rent;
use LOC::Proforma;
use LOC::Util;
use LOC::Customer;
use LOC::Transport;
use LOC::Invoice::ArticleRef;
use LOC::Common::Rent;
use LOC::MachineInventory;



# Function __multipleCalls
# Appeler plusieurs fonctions à la fois
#
# Parameters:
# arrayref $tabCalls - Liste des appels
#
# Returns:
# arrayref
sub __multipleCalls
{
    my ($soapUri, $tabCalls) = @_;

    my $tabResults = [];
    my $callsCount = @$tabCalls;
    for (my $i = 0; $i < $callsCount; $i++)
    {
        # Appel de la fonction
        my $method = $tabCalls->[$i]->{'method'};
        my @params = @{$tabCalls->[$i]->{'params'}};
        my $result = eval('&' . $method . '($soapUri, @params);');
        #no strict 'refs'; my $result = &$method($soapUri, @params);
        $tabResults->[$i] = $result;
    }

    return $tabResults;
}


# Function onChangeDocument
# Met à jour les informations d'un document (Contrat de location, ligne de devis de location)
#
# Parameters:
# string   $countryId    - Pays
# string   $documentType - Type de document (contrat/ligne de devis)
# int      $documentId   - Id du document
# arrayref $tabChanges   - Types de changements
# int      $userId       - Utilisateur responsable de la modification
#
# Returns:
# bool
sub onChangeDocument
{
    my ($soapUri, $countryId, $documentType, $documentId, $tabChanges, $userId) = @_;

    # Contrat
    if ($documentType eq 'rentContract')
    {
        my $contractId = $documentId;

        my $isTariffRentChange = &LOC::Util::in_array('tariffRent', $tabChanges);
        my $isTariffTrspChange = &LOC::Util::in_array('tariffTransport', $tabChanges);

        # Si les tarifs location ou transport changent alors on expire les pro formas actives
        if ($isTariffRentChange || $isTariffTrspChange)
        {
            # On expire les pro formas
            if (!&LOC::Proforma::expireAll($countryId, 'contract', $contractId, $userId, {}, undef))
            {
                return 0;
            }
        }

        # Mise à jour du montant de l'assurance du contrat si le tarif de location a changé
        if ($isTariffRentChange)
        {
            # &LOC::Contract::Rent::updateAmounts le fait pour le moment !
        }

        # Mise à jour du montant total du contrat si besoin
        if ($isTariffRentChange || $isTariffTrspChange)
        {
            my $tabErrors = {};
            if (!&LOC::Contract::Rent::updateAmounts($countryId, $contractId, $tabErrors))
            {
                return 0;
            }
        }
    }
    # Ligne d'un devis de location
    elsif ($documentType eq 'rentEstimateLine')
    {
        my $estimateLineId = $documentId;

        my $isTariffRentChange = &LOC::Util::in_array('tariffRent', $tabChanges);
        my $isTariffTrspChange = &LOC::Util::in_array('tariffTransport', $tabChanges);

        # Si les tarifs location ou transport changent alors on expire les pro formas actives
        if ($isTariffRentChange || $isTariffTrspChange)
        {
            # On expire les pro formas
            if (!&LOC::Proforma::expireAll($countryId, 'estimateLine', $estimateLineId, $userId, {}, undef))
            {
                return 0;
            }
        }

        # Mise à jour du montant de l'assurance de la ligne de devis si le tarif de location a changé
        if ($isTariffRentChange)
        {
            # C'est fait avant dans la Gestion de tarification location pour le moment !
        }

        # Mise à jour du montant total du devis si besoin
        if ($isTariffRentChange || $isTariffTrspChange)
        {
            my $tabErrors = {};
            if (!&LOC::Estimate::Rent::updateLineAmounts($countryId, $estimateLineId, $tabErrors))
            {
                return 0;
            }
        }
    }

    # Quelle que soit la modification, on met à jour les faisabilités des transports et commandes du document
    my $docTypeTransport = ($documentType eq 'rentContract' ?
                                LOC::Transport::FROMTOTYPE_CONTRACT : LOC::Transport::FROMTOTYPE_ESTIMATELINE);

    my $tabFilters = {
        'from-to' => {
            'type' => $docTypeTransport,
            'id'   => $documentId
        }
    };

    if (!&LOC::Transport::renew($countryId, $tabFilters, $userId, undef))
    {
        return 0;
    }

    return 1;
}


# Function: updateCustomer
# Mettre à jour un client
#
# Parameters:
# string   $countryId  - Pays
# string   $id         - Id du client
# hashref  $tabData    - Données à mettre à jour
# int      $userId     - Utilisateur responsable de la modification
# hashref  $tabOptions - Tableau des options supplémentaires (facultatif)
#
# Returns:
# bool
sub updateCustomer
{
    my ($soapUri, $countryId, $id, $tabData, $userId, $tabOptions) = @_;

    return &LOC::Customer::update($countryId, $id, $tabData, $userId, $tabOptions, {}, undef);
}


# Function: getCharacteristicValue
# Récupérer la valeur d'une caractéristique
#
# Parameters:
# string   $code      - Code de la caractéristique
# hashref  $tabParams - Paramètres de la caractéristique :
#                       - country : pays
#                       - agency : agence
#                       - date : date
#                       - default : valeur par défaut si aucune valeur n'est trouvée
#
# Returns:
# string
sub getCharacteristicValue
{
    my ($soapUri, $code, $tabParams) = @_;

    if (defined $tabParams->{'agency'})
    {
        return &LOC::Characteristic::getAgencyValueByCode(
            $code,
            $tabParams->{'agency'},
            $tabParams->{'date'},
            $tabParams->{'default'},
            1    # Cache désactivé
        );
    }

    if (defined $tabParams->{'country'})
    {
        return &LOC::Characteristic::getCountryValueByCode(
            $code,
            $tabParams->{'country'},
            $tabParams->{'date'},
            $tabParams->{'default'},
            1    # Cache désactivé
        );
    }

    return &LOC::Characteristic::getGlobalValueByCode(
        $code,
        $tabParams->{'date'},
        $tabParams->{'default'},
        1    # Cache désactivé
    );
}


# Function: getInvoiceArticleRefs
# Récupérer les références articles Sage à partir des codes Gesloc
#
# Parameters:
# string                  $countryId - Identifiant du pays
# string|arrayref|hashref $code      - Code ou liste des codes Gesloc
#
# Returns:
# hashref
sub getInvoiceArticleRefs
{
    my ($soapUri, $countryId, $code) = @_;

    if (ref $code eq 'HASH')
    {
        my @tabCodes = values(%$code);
        $code = \@tabCodes;
    }

    my $tabArticleRefs = &LOC::Invoice::ArticleRef::getList($countryId, LOC::Util::GETLIST_ASSOC, {'code' => $code});

    my @tabArticleRefsValues = values(%$tabArticleRefs);
    return \@tabArticleRefsValues;
}


# Function: getLegalHolidays
# Retourne une référence de tableau contenant les jours fériés entre les deux dates (incluses) dans l'agence concernée
#
# Parameters:
# string $beginDate - Date de début au format MySQL
# string $endDate   - Date de fin au format MySQL
# string $agency    - Identifiant de l'agence
#
# Returns:
# hashref -
sub getLegalHolidays
{
    my ($soapUri, $beginDate, $endDate , $agency) = @_;

    my $tabLegalHolidays = &LOC::Date::getLegalHolidays($beginDate, $endDate, $agency);

    return $tabLegalHolidays;
}


# Function: getDeltaWorkingDays
# Retourne un entier correspondant au nombre de jours ouvrables (sans samedi, dimanche et jours fériés) entre deux dates
# (incluses) dans l'agence concernée
#
#
# Parameters:
# string $beginDate - Date de début au format MySQL
# string $endDate   - Date de fin au format MySQL
# string $agency    - Identifiant de l'agence
#
# Returns:
# int -
sub getDeltaWorkingDays
{
    my ($soapUri, $beginDate, $endDate, $agency) = @_;

    return &LOC::Date::getDeltaWorkingDays($beginDate, $endDate, $agency);
}


# Function: getRentDuration
# Récupérer la durée de location entre deux dates
#
# Parameters:
# string $beginDate       - Date de début de la période au format MySQL
# string $endDate         - Date de fin de la période au format MySQL
# string $agency          - Identifiant de l'agence
# string $contractEndDate - Date de fin du contrat au format MySQL (vaut $endDate par défaut)
#
# Returns:
# int
sub getRentDuration
{
    my ($soapUri, $beginDate, $endDate, $agency, $contractEndDate) = @_;

    return &LOC::Date::getRentDuration($beginDate, $endDate, $agency, $contractEndDate);
}


# Function: getInsuranceAmount
# Récupérer le montant de l'assurance
#
# Parameters:
# string   $countryId       - Pays
# string   $agencyId        - Agence
# int      $insuranceTypeId - Type d'assurance
# float    $insuranceRate   - Taux d'assurance
# string   $dailyPrice      - Prix journalier
# string   $beginDate       - Date de début
# string   $endDate         - Date de fin
# int      $additionalDays  - Jours supplémentaires
# int      $deductedDays    - Jours déduits
# hashref  $tabOptions      - Tableau des options supplémentaires
#
# Returns:
# float - Montant de l'assurance
sub getInsuranceAmount
{
    my ($soapUri, $countryId, $agencyId, $insuranceTypeId, $insuranceRate, $dailyPrice,
        $beginDate, $endDate, $additionalDays, $deductedDays, $tabOptions) = @_;

    # Récupération uniquement du montant
    my $insuranceAmount = &LOC::Common::Rent::calculateInsurance($countryId, $agencyId, $insuranceTypeId, $insuranceRate, $dailyPrice,
        $beginDate, $endDate, $additionalDays, $deductedDays, $tabOptions);

    return $insuranceAmount;
}


# Function: getAppealAmount
# Récupérer le montant de la gestion de recours
#
# Parameters:
# string   $countryId       - Pays
# string   $agencyId        - Agence
# int      $appealTypeId    - Type de gestion de recours
# float    $appealRate      - Taux
# string   $dailyPrice      - Prix journalier
# string   $beginDate       - Date de début
# string   $endDate         - Date de fin
# int      $additionalDays  - Jours supplémentaires
# int      $deductedDays    - Jours déduits
# hashref  $tabOptions      - Options supplémentaires
#
# Returns:
# float - Montant de la gestion de recours
sub getAppealAmount
{
    my ($soapUri, $countryId, $agencyId, $appealTypeId, $appealRate, $dailyPrice,
        $beginDate, $endDate, $additionalDays, $deductedDays, $tabOptions) = @_;

    my $appealAmount = 0;
    if ($appealTypeId)
    {
        $appealAmount = &LOC::Common::Rent::calculateAppeal($countryId, $agencyId, $appealTypeId, $appealRate, $dailyPrice,
                                                            $beginDate, $endDate, $additionalDays, $deductedDays, $tabOptions);
    }
    return $appealAmount;
}


# Function: getRentContractsList
# Récupérer la liste des contrats de location
#
# Parameters:
# string  $countryId  - Pays
# hashref $tabFilters - Liste des filtres
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hashref - Liste des contrats
sub getRentContractsList
{
    my ($soapUri, $countryId, $tabFilters, $flags, $tabOptions) = @_;

    my $tabContracts = &LOC::Contract::Rent::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters, $flags, $tabOptions);

    my @tabContractsValues = values(%$tabContracts);
    return \@tabContractsValues;
}


# Function: getRentInvoicesList
# Récupérer la liste des factures de location
#
# Parameters:
# string  $countryId  - Pays
# hashref $tabFilters - Liste des filtres
#
# Returns:
# hashref - Liste des factures de location
sub getRentInvoicesList
{
    my ($soapUri, $countryId, $tabFilters) = @_;

    my $tabInvoices = &LOC::Contract::Rent::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters);

    my @tabInvoicesValues = values(%$tabInvoices);
    return \@tabInvoicesValues;
}


# Function: createMachineInventory
# Créer un nouvel état des lieux
#
# Parameters:
# string  $countryId  - Identifiant du pays
# hashref $tabData    - Tableau des données
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
#
# Returns:
# int - Identifiant de l'état des lieux créé
sub createMachineInventory
{
    my ($soapUri, $countryId, $tabData, $userId, $tabOptions) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!exists $tabOptions->{'photos'})
    {
        $tabOptions->{'photos'} = {};
    }

    $tabOptions->{'photos'}->{'isBase64Content'} = 1;

    my $tabErrors = {};
    my $inventoryId = &LOC::MachineInventory::insert($countryId, $tabData, $userId, $tabOptions, $tabErrors);
    if (!$inventoryId)
    {
        # Pays inconnu
        if (&LOC::Util::in_array(LOC::MachineInventory::ERROR_UNKNOWNCOUNTRY, $tabErrors->{'fatal'}))
        {
            die SOAP::Fault->faultcode('UnknownCountry')
                           ->faultstring('Le pays n\'existe pas');
        }
        # Utilisateur inconnu
        if (&LOC::Util::in_array(LOC::MachineInventory::ERROR_UNKNOWNUSER, $tabErrors->{'fatal'}))
        {
            die SOAP::Fault->faultcode('UnknownUser')
                           ->faultstring('L\'utilisateur n\'existe pas');
        }
        # Champs requis
        my @tabRequiredFields = ();
        if (&LOC::Util::in_array(LOC::MachineInventory::ERROR_TYPEREQUIRED, $tabErrors->{'fatal'}))
        {
            push(@tabRequiredFields, 'type');
        }
        if (&LOC::Util::in_array(LOC::MachineInventory::ERROR_PARKNUMBERREQUIRED, $tabErrors->{'fatal'}))
        {
            push(@tabRequiredFields, 'parkNumber');
        }
        if (&LOC::Util::in_array(LOC::MachineInventory::ERROR_CONTRACTCODEREQUIRED, $tabErrors->{'fatal'}))
        {
            push(@tabRequiredFields, 'contractCode');
        }
        if (@tabRequiredFields > 0)
        {
            die SOAP::Fault->faultcode('RequiredFields')
                           ->faultstring('Champs requis')
                           ->faultdetail(&LOC::Json::toJson({'properties' => \@tabRequiredFields}));
        }
        # Champs non valides
        my @tabInvalidFields = ();
        if (&LOC::Util::in_array(LOC::MachineInventory::ERROR_INVALIDTYPE, $tabErrors->{'fatal'}))
        {
            push(@tabInvalidFields, 'type');
        }
        if (&LOC::Util::in_array(LOC::MachineInventory::ERROR_INVALIDPARKNUMBER, $tabErrors->{'fatal'}))
        {
            push(@tabInvalidFields, 'parkNumber');
        }
        if (&LOC::Util::in_array(LOC::MachineInventory::ERROR_INVALIDCONTRACTCODE, $tabErrors->{'fatal'}))
        {
            push(@tabInvalidFields, 'contractCode');
        }
        if (@tabInvalidFields > 0)
        {
            die SOAP::Fault->faultcode('InvalidFields')
                           ->faultstring('Champs non valides')
                           ->faultdetail(&LOC::Json::toJson({'properties' => \@tabInvalidFields}));
        }
        # Erreur sur les photos
        if (&LOC::Util::in_array(LOC::MachineInventory::ERROR_REQUIREDPHOTOS, $tabErrors->{'fatal'}))
        {
            # Extension de fichier non autorisée
            die SOAP::Fault->faultcode('RequiredPhotos')
                           ->faultstring('Photos obligatoires');
        }
        if (&LOC::Util::in_array(LOC::MachineInventory::ERROR_INVALIDPHOTOEXTENSION, $tabErrors->{'fatal'}))
        {
            # Extension de fichier non autorisée
            die SOAP::Fault->faultcode('InvalidPhotoExtension')
                           ->faultstring('Extension de photo non valide');
        }
        if (&LOC::Util::in_array(LOC::MachineInventory::ERROR_INVALIDPHOTOTYPE, $tabErrors->{'fatal'}))
        {
            # Type de fichier non autorisé
            die SOAP::Fault->faultcode('InvalidPhotoType')
                           ->faultstring('Type de photo non valide');
        }
        if (&LOC::Util::in_array(LOC::MachineInventory::ERROR_PHOTOMAXSIZEREACHED, $tabErrors->{'fatal'}))
        {
            # Taille maximale dépassée
            die SOAP::Fault->faultcode('PhotoMaxSizeReached')
                           ->faultstring('Taille maximale de photo depassee');
        }
        if (&LOC::Util::in_array(LOC::MachineInventory::ERROR_PHOTONOTFOUND, $tabErrors->{'fatal'}))
        {
            # Fichier introuvable
            die SOAP::Fault->faultcode('PhotoNotFound')
                           ->faultstring('Photo introuvable');
        }
        if (&LOC::Util::in_array(LOC::MachineInventory::ERROR_INVALIDPHOTOCONTENT, $tabErrors->{'fatal'}))
        {
            # Contenu non valide
            die SOAP::Fault->faultcode('InvalidPhotoContent')
                           ->faultstring('Contenu de photo non valide');
        }

        # Erreur fatale par défaut
        die SOAP::Fault->faultcode('FatalError')
                       ->faultstring('Erreur fatale');
    }

    # État des lieux déjà existant
    if (&LOC::Util::in_array(LOC::MachineInventory::ERROR_ALREADYEXISTING, $tabErrors->{'fatal'}))
    {
        die SOAP::Fault->faultcode('AlreadyExisting')
                       ->faultstring('L\'état des lieux existe déjà')
                       ->faultdetail(&LOC::Json::toJson({'id' => $inventoryId}));
    }

    return $inventoryId;
}


# Function: unsuspendMachineTelematics
# Réactive la télématique de la machine
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Identifiant de la machine
# int     $userId     - Utilisateur responsable de la modification
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, 0 sinon
sub unsuspendMachineTelematics
{
    my ($soapUri, $countryId, $id, $userId) = @_;

    my $tabErrors = {};

    if (!&LOC::Machine::unsuspendTelematics($countryId, $id, $userId, {}, $tabErrors))
    {
        # Droits insuffisants
        if (&LOC::Util::in_array(0x0002, $tabErrors->{'fatal'}))
        {
            die SOAP::Fault->faultcode('AccessDenied')
                           ->faultstring('Droits insuffisants');
        }

        # Erreur fatale par défaut
        die SOAP::Fault->faultcode('FatalError')
                       ->faultstring('Erreur fatale');
    }

    return 1;
}


1;