use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Country
# Module permettant d'obtenir les informations d'un pays
package LOC::Country;


# Constants: Récupération d'informations sur les pays
# GETINFOS_ALL  - Récupération de l'ensemble des informations
# GETINFOS_BASE - Récupération des informations de base
use constant
{
    GETINFOS_ALL  => 0xFFFFFFFF,
    GETINFOS_BASE => 0x00000000
};


# Constants: États
# STATE_ACTIVE  - Pays actif
# STATE_DELETED - Pays supprimé
use constant
{
    STATE_ACTIVE       => 'GEN01',
    STATE_DELETED      => 'GEN03'
};

# Constants: Valeurs par défaut
# DEFAULT_COUNTRY_ID  - Identifiant du Pays par défaut
use constant
{
    DEFAULT_COUNTRY_ID => 'FR'
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Util;


# Sauvegarde des taux de conversion en euro pour les différents pays
my $_tabEuroRates = {};

# Sauvegarde des pays actifs
my $_tabStatuses = {};

# Sauvegarde des agences principales
my $_tabMainAgencyIds = {};


# Function: getInfos
# Retourne les infos d'un pays
#
# Parameters:
# string    $id        - Id du pays
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getInfos
{
    my ($id) = @_;

    my $tab = &getList(LOC::Util::GETLIST_ASSOC, {'id' => $id});
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: isActive
# Vérifie si le pays correspondant à l'identifiant passé en paramètre est actif.
#
# Parameters:
# string  $id - Identifiant du pays
#
# Returns:
# int - 1 si le pays est actif, 0 sinon.
sub isActive
{
    my ($id) = @_;

    if ($id eq '')
    {
        return 0;
    }

    if (!defined $_tabStatuses->{$id}->{'isActive'})
    {
        $_tabStatuses->{$id}->{'isActive'} = (&getList(LOC::Util::GETLIST_COUNT, {'id' => $id}) > 0);
    }

    return $_tabStatuses->{$id}->{'isActive'};
}

# Function: getList
# Retourne la liste des pays
#
# Contenu d'une ligne du hashage :
# - id : identifiant ;
# - label : libellé ;
# - subsidiaryName : nom de la filiale d'Acces industrie ;
# - locale.id : identifiant de la locale ;
# - language : nom de la langue ;
# - defaultRate.id : identifiant du taux de TVA par défaut ;
# - defaultRate.label : libellé du taux de TVA par défaut ;
# - defaultRate.fullLabel : libellé et valeur du taux de TVA par défaut ;
# - defaultRate.value : valeur du taux de TVA par défaut ;
# - insurance.rates : liste des taux d'assurance ;
# - currency.id : identifiant de la devise ;
# - currency.symbol : symbole local de la devise ;
# - euroRate : taux de conversion entre la devise locale et l'euro ;
# - fuelUnitPrice : prix du litre de carburant en devise locale ;
# - laborUnitPrice : prix de l'heure de main-d'oeuvre en devise locale ;
# - kmUnitPrice : prix du kilomètre de déplacement en devise locale ;
# - kimCode : Code Kimoce correspondant ;
#
# Parameters:
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|0 - Liste des pays
sub getList
{
    my ($format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }

    # connexion à AUTH
    my $db = &LOC::Db::getConnection('auth');

    # Filtres
    my $whereQuery = '';
    if ($tabFilters->{'id'} ne '')
    {
        $whereQuery .= '
AND cty_id = ' . $db->quote($tabFilters->{'id'});
    }
    if (!defined $tabFilters->{'stateId'})
    {
        $whereQuery .= '
AND cty_sta_id <> ' . $db->quote(STATE_DELETED);
    }
    else
    {
        $whereQuery .= '
AND cty_sta_id IN (' . $db->quote($tabFilters->{'stateId'}) . ')';
    }

    # Récupère les données concernant le pays
    my $query = '';
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    cty_id AS `id`,
    cty_label AS `label`,
    cty_subsidiary AS `subsidiaryName`,
    cty_lcl_id AS `locale.id`,
    lcl_label AS `language`,
    cty_vat_id AS `defaultRate.id`,
    vat_label AS `defaultRate.label`,
    CONCAT(vat_label, " (", vat_value, " %)") AS `defaultRate.fullLabel`,
    vat_value AS `defaultRate.value`,
    cty_agc_id AS `mainAgency.id`,
    PAASSURANCE AS `insurance.rates`,
    cty_cur_id AS `currency.id`,
    cur_symbol AS `currency.symbol`,
    cur_eurorate AS `euroRate`,
    PACARBURANT AS `fuelUnitPrice`,
    PAMO AS `laborUnitPrice`,
    PAKM AS `kmUnitPrice`,
    CpyInCde AS `kimCode`
FROM frmwrk.p_country
INNER JOIN frmwrk.p_currency
ON cty_cur_id = cur_id
INNER JOIN frmwrk.p_locale
ON cty_lcl_id = lcl_id
LEFT JOIN frmwrk.p_vat
ON vat_id = cty_vat_id AND vat_cty_id = cty_id
LEFT JOIN PAYS
ON cty_id = PACODE
WHERE 1 ' .
$whereQuery . '
ORDER BY `label`;';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*) AS `label`
FROM frmwrk.p_country
WHERE 1 ' .
$whereQuery . ';';
    }
    else
    {
        $query = '
SELECT
    cty_id AS `id`,
    cty_label AS `label`
FROM frmwrk.p_country
WHERE 1 ' .
$whereQuery . '
ORDER BY `label`;';
    }
    my $dataRef = $db->fetchList($format, $query, {}, 'id');
    unless ($dataRef)
    {
        return 0;
    }
    # Traitement pour les taux d'assurance
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach (keys %$dataRef)
        {
            $dataRef->{$_}->{'euroRate'} *= 1;
            $dataRef->{$_}->{'defaultRate.value'} *= 1;
            $dataRef->{$_}->{'fuelUnitPrice'} *= 1;
            $dataRef->{$_}->{'laborUnitPrice'} *= 1;
            $dataRef->{$_}->{'kmUnitPrice'} *= 1;
            $dataRef->{$_}->{'kimCode'} *= 1;
            if ($dataRef->{$_}->{'insurance.rates'} =~ /\:/)
            {
                my @tabInsurances = split(/\:/, $dataRef->{$_}->{'insurance.rates'});
                $dataRef->{$_}->{'insurance.defaultRate'} = $tabInsurances[0];
                $dataRef->{$_}->{'insurance.rates'} = \@tabInsurances;
            }
            else
            {
                $dataRef->{$_}->{'insurance.defaultRate'} = $dataRef->{$_}->{'insurance.rates'};
                $dataRef->{$_}->{'insurance.rates'} = [$dataRef->{$_}->{'insurance.rates'}];
            }
        }
    }

    # Tri en mettant le pays par défaut en premier
    if ($tabOptions->{'defaultFirst'})
    {
        my $sub = sub {
            my ($a, $b) = @_;

            if ($a eq DEFAULT_COUNTRY_ID && $b eq DEFAULT_COUNTRY_ID)
            {
                return 0;
            }
            elsif ($a eq DEFAULT_COUNTRY_ID)
            {
                return -1;
            }
            elsif ($b eq DEFAULT_COUNTRY_ID)
            {
                return 1;
            }
            return ($a cmp $b);
        };
        $dataRef = &LOC::Util::sortHashByKeys($dataRef, 0, $sub);
    }

    return (wantarray ? %$dataRef : $dataRef);
}

# Function: getEuroRate
# Retourne le taux de conversion de la monnaie locale en Euros
#
# Parameters:
# string  $countryId - Identifiant du pays
# string  $date      - Date de début d'application du taux de conversion
#
# Returns:
# float - Taux de conversion
sub getEuroRate
{
    my ($countryId, $date) = @_;

    my $rateStrId = $countryId . '_' . $date;
    if (!defined $_tabEuroRates->{$rateStrId})
    {
        # connexion à AUTH
        my $db = &LOC::Db::getConnection('frmwrk');

        my $query;

        # Récupération du taux à la date demandée
        my $dateRate;
        if (defined $date)
        {
            $query = '
SELECT
    crr_value
FROM p_country
LEFT JOIN p_currency
ON cty_cur_id = cur_id
LEFT JOIN h_currencyrate
ON cur_id = crr_cur_id
WHERE cty_id = ' . $db->quote($countryId) . '
AND crr_date_begin <= ' . $db->quote($date) . '
ORDER BY crr_date_begin DESC
LIMIT 1;';
            $dateRate = $db->fetchOne($query);
        }

        # Récupération du taux courant
        my $currentRate;
        if (!defined $dateRate)
        {
            $query = '
SELECT
    cur_eurorate
FROM p_country
LEFT JOIN p_currency
ON cty_cur_id = cur_id
WHERE cty_id = ' . $db->quote($countryId) . ';';
            $currentRate = $db->fetchOne($query);
        }

        $_tabEuroRates->{$rateStrId} = (defined $dateRate) ? $dateRate * 1 : $currentRate * 1;
    }

    return $_tabEuroRates->{$rateStrId};
}

# Function: getVATRatesList
# Retourne la liste des taux de TVA en vigueur
#
# Parameters:
# string  $countryId - Identifiant du pays
# string  $date      -  Date de début d'application du taux de TVA
#
# Returns:
# hash|hashref - Liste des taux de TVA en vigueur
sub getVATRatesList
{
    my ($countryId, $date) = @_;

    # connexion à AUTH
    my $db = &LOC::Db::getConnection('frmwrk');

    my $query;
    if (defined $date)
    {
        $query = '
SELECT
    vat_id AS `id`,
    IFNULL((SELECT
        vvl_value AS `value`
    FROM h_vatvalue
    WHERE vat_id = vvl_vat_id
    AND vat_cty_id = vvl_cty_id
    AND vvl_date_begin <= ' . $db->quote($date) . '
    ORDER BY vvl_date_begin DESC
    LIMIT 1), vat_value) AS `value`
FROM p_vat
WHERE vat_cty_id = ' . $db->quote($countryId);
    }
    else
    {
        $query = '
SELECT
    vat_id AS `id`,
    vat_value AS `value`
FROM p_vat
WHERE vat_cty_id = ' . $db->quote($countryId);
    }

    my $tabResult = $db->fetchAssoc($query, {}, 'id');

    foreach my $tabRow (values %$tabResult)
    {
        # Formatage des données
        $tabRow->{'value'} /= 100;
    }

    return (wantarray ? %$tabResult : $tabResult);
}

# Function: getKimCode
# Retourne le code Kimoce correspondant au pays
#
# Parameters:
# string  $countryId - Identifiant du pays
#
# Returns:
# int - Code du pays
sub getKimCode
{
    my ($countryId) = @_;

    # connexion à AUTH
    my $db = &LOC::Db::getConnection('auth');

    my $query = '
SELECT
    CpyInCde
FROM PAYS
WHERE PACODE = ' . $db->quote($countryId) . ';';

    return $db->fetchOne($query) * 1;
}

# Function: getCapital
# Retourne le montant du capital
#
# Parameters:
# string  $countryId - Identifiant du pays
#
# Returns:
# float - Montant du capital
sub getCapital
{
    my ($countryId) = @_;

    my $db = &LOC::Db::getConnection('frmwrk');

    my $constantName = 'CAPITAL_ACCES_' . uc($countryId);
    my $query = '
SELECT
    cst_value
FROM p_constant
WHERE cst_name = ' . $db->quote($constantName);

    return &LOC::Util::toNumber($db->fetchOne($query));
}


# Function: getMainAgencyId
# Retourne l'identifiant de l'agence principale d'un pays
#
# Parameters:
# string  $countryId - Identifiant du pays
#
# Returns:
# string | undef - Identifiant de l'agence principale si définie
sub getMainAgencyId
{
    my ($countryId) = @_;

    if ($countryId eq '')
    {
        return undef;
    }

    if (!exists $_tabMainAgencyIds->{$countryId})
    {
        my $db = &LOC::Db::getConnection('frmwrk');

        my $query = '
SELECT
    cty_agc_id
FROM p_country
WHERE cty_id = ' . $db->quote($countryId) . ';';

        $_tabMainAgencyIds->{$countryId} = $db->fetchOne($query);
    }

    return $_tabMainAgencyIds->{$countryId};
}

1;