use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::BusinessFamily
# Module permettant d'obtenir les informations sur les familles commerciales
package LOC::BusinessFamily;


# Constants: Récupération d'informations sur les familles commerciales
# GETINFOS_ALL  - Récupération de l'ensemble des informations
# GETINFOS_BASE - Récupération des informations de base
use constant
{
    GETINFOS_ALL  => 0xFFFFFFFF,
    GETINFOS_BASE => 0x00000000
};

# Constants: Familles commerciales définies
# ID_SUBRENT - Famille commerciale pour la sous-location
use constant
{
    ID_SUBRENT  => 45
};

use strict;

# Modules internes
use LOC::Db;


# Function: getInfos
# Retourne les infos d'une famille commerciale
# 
# Contenu du hashage :
# - id => identifiant de la famille commerciale
# - label => libellé
# - order => ordre d'affichage
# - analyticalCode => portion du code analytique associé aux familles commerciales
# - family.id => identifiant de la famille
#
#
# Parameters:
# string  $countryId  - Identifiant du pays
# string  $id         - Identifiant de la famille commerciale
# int     $flags      - Flags de récupération
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);
    return ($tab && exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Retourne la liste des familles commerciales
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres ('id' : Recherche par l'identifiant)
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|0 - Liste des familles commerciales
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }

    # connexion à la base
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Filtres
    my $whereQuery = '';

    # - Recherche par l'identifiant
    if ($tabFilters->{'id'})
    {
        $whereQuery .= ' AND tbl_p_businessfamily.FACOMAUTO = ' . $tabFilters->{'id'};
    }

    my $query;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    tbl_p_businessfamily.FACOMAUTO AS `id`,
    tbl_p_businessfamily.FACOMLIBELLE AS `label`,
    tbl_p_businessfamily.FACOMORDRE AS `order`,
    tbl_p_businessfamily.FACOMANALYTIQUE AS `analyticalCode`,
    tbl_p_businessfamily.fly_id AS `family.id`';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*) AS `nb`';
    }
    else
    {
        $query = '
SELECT
    tbl_p_businessfamily.FACOMAUTO AS `id`,
    tbl_p_businessfamily.FACOMLIBELLE AS `label`';
    }

    $query .= '
FROM FAMILLECOMMERCIALE tbl_p_businessfamily
WHERE 1
' . $whereQuery . ';';

    my $tab = $db->fetchList($format, $query, {}, 'id');

    # Formatage des données
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}        *= 1;
            $tabRow->{'order'}     *= 1;
            $tabRow->{'family.id'} *= 1;
        }
    }

    return (wantarray ? %$tab : $tab);
}

# Function: getAnalyticalCodesList
# Retourne la liste des codes analytiques par famille commerciale
#
# Parameters:
# string $countryId - Identifiant du pays
#
# Returns:
# hash|hashref|0 - Liste des codes analytiques par famille commerciale
sub getAnalyticalCodesList
{
    my ($countryId) = @_;

    my $tabList = &getList($countryId, LOC::Util::GETLIST_ASSOC);

    my $tabResult = {};
    foreach my $businessFamily (values(%$tabList))
    {
        $tabResult->{$businessFamily->{'id'}} = $businessFamily->{'analyticalCode'};
    }

    return (wantarray ? %$tabResult : $tabResult);
}

1;
