use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::InvoicingRequest
# Module permettant d'obtenir les informations sur les requêtes de facturation
package LOC::InvoicingRequest;


# Constants: États
# STATE_ACTIVE  - Pays actif
# STATE_DELETED - Pays supprimé
use constant
{
    STATE_CREATED  => 'REQ01',
    STATE_PROGRESS => 'REQ02',
    STATE_SUCCESS  => 'REQ03',
    STATE_FAILED   => 'REQ04',
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Json;
use LOC::Util;


# Function: getInfos
# Retourne les infos d'une requête de facturation
# 
# Contenu du hashage :
# - id : identifiant de la requête de facturation ;
# - pid : numéro du processus Unix associé à la requête ;
# - user.id : identifiant de l'utilisateur qui a lancé la requête ;
# - parameters : paramètres de la requête ;
# - startDate : date de début ;
# - endDate : date de fin ;
# - progression : pourcentage de progression ;
# - generatedInvoicesNo : nombre de factures générées ;
# - creationDate : date de création ;
# - errors : erreurs ;
# - state.id : identifiant de l'état.
#
#
# Parameters:
# string $id        - Identifiant de la requête de facturation
# string $countryId - Identifiant du pays
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getInfos
{
    my ($id, $countryId) = @_;

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id});
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Retourne la liste des requêtes de facturation
#
# Parameters:
# int     $format     - Format de retour de la liste
# string  $countryId  - Identifiant du pays
# hashref $tabFilters - Filtres
#
# Returns:
# hash|hashref|0 - Liste des requêtes de facturation
sub getList
{
    my ($countryId, $format, $tabFilters) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query;
    if ($format == LOC::Util::GETLIST_PAIRS)
    {
        $query = '
SELECT
    irq_id AS `id`,
    irq_pid AS `pid`
FROM f_invoicingrequest';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*) AS `id`
FROM f_invoicingrequest';
    }
    else
    {
        $query = '
SELECT
    irq_id AS `id`,
    irq_pid AS `pid`,
    irq_usr_id AS `user.id`,
    irq_parameters AS `parameters`,
    irq_date_start AS `startDate`,
    irq_date_end AS `endDate`,
    irq_treatedinvoices AS `treatedInvoicesNo`,
    irq_totalinvoices AS `totalInvoicesNo`,
    irq_progression AS `progression`,
    irq_generatedinvoices AS `generatedInvoicesNo`,
    irq_date_create AS `creationDate`,
    irq_stderr AS `errors`,
    irq_result AS `tabResult`,
    irq_sta_id AS `state.id`
FROM f_invoicingrequest';
    }
    $query .= '
WHERE TRUE';

    # Filtres
    # - suivant un identifiant de requête
    if ($tabFilters->{'id'} ne '')
    {
        $query .= '
AND irq_id = ' . $db->quote($tabFilters->{'id'});
    }
    if ($tabFilters->{'stateId'} ne '')
    {
        $query .= '
AND irq_sta_id IN (' . $db->quote($tabFilters->{'stateId'}) . ')';
    }
    # - suivant les paramètres
    if ($tabFilters->{'parameters'} ne '')
    {
        if (ref($tabFilters->{'parameters'}) eq 'HASH')
        {
            foreach my $key (keys(%{$tabFilters->{'parameters'}}))
            {
                my $search = &LOC::Json::toJson({$key => $tabFilters->{'parameters'}->{$key}});
                $search = substr(substr($search, 0, -1), 1);
                $query .= '
AND irq_parameters LIKE ' . $db->quote('%' . $search . '%');
            }
        }
        else
        {
            $query .= '
AND irq_parameters = ' . $db->quote($tabFilters->{'parameters'});
        }
    }
    $query .= '
ORDER BY `id`;';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    # Transformation du JSON en hashref
    foreach my $tabLine (values(%$tab))
    {
        if ($tabLine->{'tabResult'})
        {
            $tabLine->{'tabResult'} = &LOC::Json::fromJson($tabLine->{'tabResult'});
        }
    }

    return (wantarray ? %$tab : $tab);
}

# Function: insert
# Crée en base de données la requête de facturation
#
# Contenu du de la table de hashage à passer en paramètre :
# - pid : numéro du processus Unix ;
# - userId : identifiant de l'utilisateur qui a lancé la requête ;
# - parameters : paramètres de la requête ;
# - progression : pourcentage de progression ;
# - generatedInvoicesNo : nombre de factures générées ;
# - errors : erreurs ;
# - stateId : état ;
#
#
# Parameters:
# string   $countryId  - Identifiant du pays
# string   $tabParams  - table de hashage
#
# Returns:
# bool - Retourne 1 si l'insertion est correcte, 0 sinon
sub insert
{
    # Récupération des paramètres
    my ($countryId, $tabParams) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    unless (defined $tabParams->{'stateId'})
    {
        $tabParams->{'stateId'} = STATE_CREATED;
    }

    if (ref($tabParams->{'parameters'}) eq 'HASH')
    {
        $tabParams->{'parameters'} = &LOC::Json::toJson($tabParams->{'parameters'});
    }

    # Ligne dans la table f_invoicingrequest
    my $tabUpdates = {
        'irq_pid'             => $tabParams->{'pid'},
        'irq_usr_id'          => $tabParams->{'userId'},
        'irq_parameters'      => $tabParams->{'parameters'},
        'irq_treatedinvoices' => $tabParams->{'treatedInvoicesNo'},
        'irq_totalinvoices'   => $tabParams->{'totalInvoicesNo'},
        'irq_progression'     => &LOC::Util::toNumber($tabParams->{'progression'}),
        'irq_date_create*'    => 'NOW()',
        'irq_stderr'          => $tabParams->{'errors'},
        'irq_sta_id'          => $tabParams->{'stateId'},
    };

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # insertion de la ligne dans la table f_invoicingrequest
    my $requestId = $db->insert('f_invoicingrequest', $tabUpdates, $schemaName);
    if ($requestId == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return $requestId * 1;
}

# Function: setStartDate
# Met à jour la date de début
#
# Parameters:
# string $countryId   - Identifiant du pays
# int    $id          - Id de la requête
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setStartDate
{
    # Récupération des paramètres
    my ($countryId, $id) = @_;

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_invoicingrequest
    if ($db->update('f_invoicingrequest', {'irq_date_start*' => 'NOW()'}, {'irq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

# Function: setEndDate
# Met à jour la date de fin
#
# Parameters:
# string $countryId   - Identifiant du pays
# int    $id          - Id de la requête
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setEndDate
{
    # Récupération des paramètres
    my ($countryId, $id) = @_;

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_invoicingrequest
    if ($db->update('f_invoicingrequest', {'irq_date_end*' => 'NOW()'}, {'irq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

# Function: setPid
# Met à jour le PID
#
# Parameters:
# string $countryId - Identifiant du pays
# int    $id        - Id de la requête
# int    $pid       - PID
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setPid
{
    # Récupération des paramètres
    my ($countryId, $id, $pid) = @_;

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_invoicingrequest
    if ($db->update('f_invoicingrequest', {'irq_pid' => $pid}, {'irq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

# Function: setProgression
# Met à jour le pourcentage de progression
#
# Parameters:
# string $countryId   - Identifiant du pays
# int    $id          - Id de la requête
# int    $progression - Pourcentage de progression (compris entre 0 et 100)
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setProgression
{
    # Récupération des paramètres
    my ($countryId, $id, $progression) = @_;

    # Progression non valide
    if ($progression < 0 || $progression > 100)
    {
        return 0;
    }
    $progression = &LOC::Util::round($progression, 0);

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_invoicingrequest
    if ($db->update('f_invoicingrequest', {'irq_progression' => $progression}, {'irq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

# Function: setTreatedInvoicesNo
# Met à jour le nombre de factures traitées
#
# Parameters:
# string $countryId         - Identifiant du pays
# int    $id                - Id de la requête
# int    $treatedInvoicesNo - Nombre de factures traitées
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setTreatedInvoicesNo
{
    # Récupération des paramètres
    my ($countryId, $id, $treatedInvoicesNo) = @_;

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_invoicingrequest
    my $tabUpdates = {'irq_treatedinvoices' => $treatedInvoicesNo,
                      'irq_progression*'    => 'IFNULL(ROUND(irq_treatedinvoices / irq_totalinvoices * 100), 0)'
                     };
    if ($db->update('f_invoicingrequest', $tabUpdates, {'irq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

# Function: setGeneratedInvoicesNo
# Met à jour le nombre de factures générées
#
# Parameters:
# string $countryId           - Identifiant du pays
# int    $id                  - Id de la requête
# int    $generatedInvoicesNo - Nombre de factures générées
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setGeneratedInvoicesNo
{
    # Récupération des paramètres
    my ($countryId, $id, $generatedInvoicesNo) = @_;

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_invoicingrequest
    my $tabUpdates = {'irq_generatedinvoices' => $generatedInvoicesNo};
    if ($db->update('f_invoicingrequest', $tabUpdates, {'irq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

# Function: setTotalInvoicesNo
# Met à jour le nombre de factures traitées
#
# Parameters:
# string $countryId       - Identifiant du pays
# int    $id              - Id de la requête
# int    $totalInvoicesNo - Nombre total de factures
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setTotalInvoicesNo
{
    # Récupération des paramètres
    my ($countryId, $id, $totalInvoicesNo) = @_;

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_invoicingrequest
    my $tabUpdates = {'irq_totalinvoices' => $totalInvoicesNo,
                      'irq_progression*'  => 'IFNULL(ROUND(irq_treatedinvoices / irq_totalinvoices * 100), 0)'
                     };
    if ($db->update('f_invoicingrequest', $tabUpdates, {'irq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

# Function: setState
# Met à jour l'état
#
# Parameters:
# string $countryId - Identifiant du pays
# int    $id        - Id de la requête
# string $stateId   - Identifiant de l'état
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setState
{
    # Récupération des paramètres
    my ($countryId, $id, $stateId) = @_;

    # État non valide
    if (!grep(/^$stateId$/, STATE_CREATED, STATE_PROGRESS, STATE_SUCCESS, STATE_FAILED))
    {
        return 0;
    }

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_invoicingrequest
    if ($db->update('f_invoicingrequest', {'irq_sta_id' => $stateId}, {'irq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

# Function: setErrors
# Met à jour les erreurs
#
# Parameters:
# string $countryId - Identifiant du pays
# int    $id        - Id de la requête
# string $errors    - Erreurs
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setErrors
{
    # Récupération des paramètres
    my ($countryId, $id, $errors) = @_;

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_invoicingrequest
    if ($db->update('f_invoicingrequest', {'irq_stderr' => $errors}, {'irq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

# Function: setResult
# Met à jour le tableau de résultat
#
# Parameters:
# string  $countryId - Identifiant du pays
# int     $id        - Id de la requête
# hashref $tabResult - Tableau de résultat
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setResult
{
    # Récupération des paramètres
    my ($countryId, $id, $tabResult) = @_;

    unless (ref($tabResult) eq 'HASH')
    {
        return 0;
    }

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_invoicingrequest
    my $jsonResult = &LOC::Json::toJson($tabResult);
    if ($db->update('f_invoicingrequest', {'irq_result' => $jsonResult}, {'irq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

1;
