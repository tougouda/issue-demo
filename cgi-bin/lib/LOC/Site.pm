use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Site
# Module permettant d'obtenir les informations d'un chantier
package LOC::Site;


# Constants: Récupération d'informations sur les chantiers
# GETINFOS_ALL       - Récupération de l'ensemble des informations
# GETINFOS_BASE      - Récupération des informations de base
# GETINFOS_TIMESLOTS - Récupération des informations sur les tranches horaires
use constant {
    GETINFOS_ALL       => 0xFFFFFFFF,
    GETINFOS_BASE      => 0x00000000,
    GETINFOS_TIMESLOTS => 0x00000001
};

# Constants: Erreurs
# ERROR_FATAL           - Erreur fatale
# ERROR_RIGHTS          - Impossible de modifier cette information
# ERROR_CITYNOTVALID    - Ville incorrecte
# ERROR_LABELREQUIRED   - Libellé obligatoire
# ERROR_CONTACTREQUIRED - Informations sur le contact incomplètes
use constant {
    ERROR_FATAL               => 0x0001,
    ERROR_RIGHTS              => 0x0003,
    ERROR_CITYNOTVALID        => 0x0100,
    ERROR_LABELREQUIRED       => 0x0101,
    ERROR_CONTACTREQUIRED     => 0x0102,
    ERROR_CONTACTMAILNOTVALID => 0x0103
};
 

use strict;

# Modules internes
use LOC::Db;
use LOC::Util;
use LOC::EndUpdates;
use LOC::Transport;


# Function: getInfos
# Retourne les infos d'un chantier
#
# Contenu du hashage :
# - id => identifiant du chantier;
# - label => libellé du chantier;
# - address => adresse;
# - addressUpdateCount => Nombre de changement d'adresse effectués;
# - department => Département;
# - postalCode => code postal;
# - locality.id => Id de la ville;
# - locality.label => Nom de la ville;
# - locality.postalCode => CP de la ville;
# - contact.fullName => nom du contact;
# - contact.telephone => téléphone du contact;
# - orderContact.fullName => nom du contact commande;
# - orderContact.telephone => téléphone du contact commande;
# - height => hauteur du chantier;
# - surface => surface du chantier;
# - deliveryHour => heure de livraison;
# - recoveryHour => heure de récupération;
# - comments => commentaires sur le chantier;
# - isHasWharf => Présence d'un quai
# - isAnticipated => Présence d'une anticipation
# - isAsbestos => chantier avec amiante
#
# Parameters:
# string $countryId   - Identifiant du pays
# string $id          - Identifiant du chantier
# int    $flags       - Flags pour la récupération des colonnes (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;

    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }

    # Connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Récupère les données concernant l'agence
    my $query = '
SELECT
    tbl_f_site.CHAUTO AS `id`,
    tbl_f_site.VILLEAUTO AS `locality.id`,
    tbl_p_sitelocality.VINOM AS `locality.name`,
    tbl_p_sitelocality.DEPVILLE AS `locality.department`,
    tbl_p_sitelocality.VICP AS `locality.postalCode`,
    CONCAT (tbl_p_sitelocality.VICP, " ", tbl_p_sitelocality.VINOM) AS `locality.label`,
    tbl_p_sitelocality.VIPAYSCODE AS `locality.country.id`,
    tbl_f_site.CHLIBELLE AS `label`,
    tbl_f_site.CHADRESSE AS `address`,
    tbl_f_site.CHMODIFADRESSE AS `addressUpdateCount`,
    tbl_f_site.CHSURFACE AS `surface`,
    tbl_f_site.CHHT AS `height`,
    tbl_f_site.CHCONTACT  AS `contact.fullName`,
    tbl_f_site.CHTELEPHONE AS `contact.telephone`,
    tbl_f_site.CHEMAIL AS `contact.email`,
    tbl_f_site.CHHEUREDP AS `deliveryHour`,
    tbl_f_site.CHPLAGEDP AS `deliveryTimeSlot.id`,';
    if ($flags & GETINFOS_TIMESLOTS)
    {
        $query .= '
    tts_d.tts_beginhour AS `deliveryTimeSlot.beginHour`,
    tts_d.tts_endhour AS `deliveryTimeSlot.endHour`,';
    }
    $query .= '
    tbl_f_site.CHHEUREAR AS `recoveryHour`,
    tbl_f_site.CHPLAGEAR AS `recoveryTimeSlot.id`,';
    if ($flags & GETINFOS_TIMESLOTS)
    {
        $query .= '
    tts_r.tts_beginhour AS `recoveryTimeSlot.beginHour`,
    tts_r.tts_endhour AS `recoveryTimeSlot.endHour`,';
    }
    $query .= '
    tbl_f_site.CHANTICIPATION AS `isAnticipated`,
    tbl_f_site.CHQUAI AS `isHasWharf`,
    tbl_f_site.CHAMIANTE AS `isAsbestos`,
    tbl_f_site.CHCOMMENTAIRE AS `comments`
FROM CHANTIER tbl_f_site
LEFT JOIN VILLECHA tbl_p_sitelocality
ON tbl_f_site.VILLEAUTO = tbl_p_sitelocality.VILLEAUTO';
    if ($flags & GETINFOS_TIMESLOTS)
    {
        $query .= '
LEFT JOIN p_transporttimeslot tts_d
ON tts_d.tts_id = tbl_f_site.CHPLAGEDP
LEFT JOIN p_transporttimeslot tts_r
ON tts_r.tts_id = tbl_f_site.CHPLAGEAR';
    }
    $query .= '
WHERE tbl_f_site.CHAUTO = ' . $id . ';';

    my $tab = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if (!$tab)
    {
        return undef;
    }

    # Formatage des données
    $tab->{'id'}                 *= 1;
    $tab->{'locality.id'}        *= 1;
    $tab->{'addressUpdateCount'} *= 1;
    $tab->{'height'}             *= 1;
    $tab->{'isHasWharf'}         *= 1;
    $tab->{'isAnticipated'}      *= 1;
    $tab->{'isAsbestos'}         *= 1;


    $tab->{'deliveryHour'}  = &_toHour($tab->{'deliveryHour'});
    $tab->{'deliveryTimeSlot.id'} = ($tab->{'deliveryTimeSlot.id'} ? $tab->{'deliveryTimeSlot.id'} * 1 : undef);
    $tab->{'recoveryHour'}  = &_toHour($tab->{'recoveryHour'});
    $tab->{'recoveryTimeSlot.id'} = ($tab->{'recoveryTimeSlot.id'} ? $tab->{'recoveryTimeSlot.id'} * 1 : undef);

    if ($flags & GETINFOS_TIMESLOTS)
    {
        $tab->{'deliveryTimeSlot.beginHour'} = &_toHour($tab->{'deliveryTimeSlot.beginHour'});
        $tab->{'deliveryTimeSlot.endHour'}   = &_toHour($tab->{'deliveryTimeSlot.endHour'});
        $tab->{'recoveryTimeSlot.beginHour'} = &_toHour($tab->{'recoveryTimeSlot.beginHour'});
        $tab->{'recoveryTimeSlot.endHour'}   = &_toHour($tab->{'recoveryTimeSlot.endHour'});
    }

    return (wantarray ? %$tab : $tab);
}


# Function: insert
# Insertion d'un chantier
#
# Contenu du de la table de hashage à passer en paramètre:
# - documentType => type de document, 'estimate' ou 'contract' ,
# - label => libellé du chantier;
# - address => adresse;
# - locality.id => Nom de la ville;
# - contact.fullName => nom du contact;
# - contact.telephone => téléphone du contact;
# - height => hauteur du chantier;
# - surface => surface du chantier;
# - deliveryHour => heure de livraison;
# - recoveryHour => heure de récupération;
# - comments => commentaires sur le chantier.
#
#
# Parameters:
# string  $countryId  - Identifiant du pays
# string  $tabParams  - table de hashage
# int     $userId     - Id de l'utilisateur créateur
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool - Retourne 1 si l'insertion est correcte, 0 sinon
sub insert
{
    # récupération des paramètres
    my ($countryId, $tabParams, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'}   = [];
    $tabErrors->{'warning'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Paramètres
    my $documentType = $tabParams->{'documentType'};
    my $label        = &LOC::Util::trim($tabParams->{'label'});
    my $localityId   = $tabParams->{'locality.id'} * 1;

    my $contactName  = &LOC::Util::trim($tabParams->{'contact.fullName'});
    my $contactTel   = $tabParams->{'contact.telephone'};
    my $contactEmail = &LOC::Util::trim($tabParams->{'contact.email'});


    # Vérification des données

    # Tests sur la ville
    if ($localityId == 0)
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_CITYNOTVALID);
    }
    # Test sur le libellé chantier
    if ($label eq '')
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_LABELREQUIRED);
    }

    # Contact chantier
    if ($documentType eq 'contract' && $contactTel eq '')
    {
        push(@{$tabErrors->{'warning'}}, ERROR_CONTACTREQUIRED);
    }
    # Mail du contact chantier
    if ($contactEmail && !&LOC::Util::isValidEmail($contactEmail))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_CONTACTMAILNOTVALID);
    }

    # Si il y a des données invalides alors on arrête ici
    if (@{$tabErrors->{'fatal'}} > 0)
    {
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Modification de la ligne dans la table CHANTIER
    my $tabUpdates = {
        'CHLIBELLE'       => $label,
        'CHADRESSE'       => &LOC::Util::trim($tabParams->{'address'}),
        'CHMODIFADRESSE'  => 1,
        'CHSURFACE'       => undef,
        'CHHT'            => undef,
        'CHCONTACT'       => $contactName,
        'CHTELEPHONE'     => $contactTel,
        'CHEMAIL'         => $contactEmail,
        'CHHEUREDP'       => &_toHour($tabParams->{'deliveryHour'}),
        'CHPLAGEDP'       => ($tabParams->{'deliveryTimeSlot.id'} ? $tabParams->{'deliveryTimeSlot.id'} * 1 : undef),
        'CHHEUREAR'       => &_toHour($tabParams->{'recoveryHour'}),
        'CHPLAGEAR'       => ($tabParams->{'recoveryTimeSlot.id'} ? $tabParams->{'recoveryTimeSlot.id'} * 1 : undef),
        'CHCOMMENTAIRE'   => &LOC::Util::trim($tabParams->{'comments'}),
        'VILLEAUTO'       => $tabParams->{'locality.id'},
        'CHANTICIPATION'  => ($tabParams->{'isAnticipated'} ? 1 : 0),
        'CHQUAI'          => ($tabParams->{'isHasWharf'} ? 1 : 0),
        'CHAMIANTE'       => ($tabParams->{'isAsbestos'} ? 1 : 0)
    };

    # insertion de la ligne dans la table CHANTIER
    my $siteId = $db->insert('CHANTIER', $tabUpdates, $schemaName);
    if ($siteId == -1)
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }
    $siteId *= 1;

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return $siteId;
}

# Function: update
# Mise à jour des informations d'un chantier
#
# Contenu du de la table de hashage à passer en paramètre:
# - documentType => type de document, 'estimate' ou 'contract' ,
# - id => identifiant du chantier
# - label => libellé du chantier;
# - address => adresse;
# - locality.id => Nom de la ville;
# - contact.fullName => nom du contact;
# - contact.telephone => téléphone du contact;
# - height => hauteur du chantier;
# - surface => surface du chantier;
# - deliveryHour => heure de livraison;
# - recoveryHour => heure de récupération;
# - comments => commentaires sur le chantier.
#
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Id du chantier à modifier
# string  $tabParams  - Table de hashage de paramètres
# int     $userId     - Id de l'utilisateur modifieur
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub update
{
    # récupération des paramètres
    my ($countryId, $id, $tabParams, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'}   = [];
    $tabErrors->{'warning'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération des informations du chantier
    my $tabInfos = &getInfos($countryId, $id);
    if (!$tabInfos)
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    # Paramètres
    my $documentType = $tabParams->{'documentType'};
    my $documentId   = $tabParams->{'documentId'};
    my $rights       = $tabParams->{'rights'};

    my $oldLabel          = $tabInfos->{'label'};
    my $newLabel          = (exists $tabParams->{'label'} ? &LOC::Util::trim($tabParams->{'label'}) : $oldLabel);
    my $oldAddress        = $tabInfos->{'address'};
    my $newAddress        = (exists $tabParams->{'address'} ? &LOC::Util::trim($tabParams->{'address'}) : $oldAddress);
    my $oldLocalityId     = $tabInfos->{'locality.id'};
    my $newLocalityId     = (exists $tabParams->{'locality.id'} ? $tabParams->{'locality.id'} : $oldLocalityId);
    my $oldCttFullName    = $tabInfos->{'contact.fullName'};
    my $newCttFullName    = (exists $tabParams->{'contact.fullName'} ? &LOC::Util::trim($tabParams->{'contact.fullName'}) : $oldCttFullName);
    my $oldCttTel         = $tabInfos->{'contact.telephone'};
    my $newCttTel         = (exists $tabParams->{'contact.telephone'} ? $tabParams->{'contact.telephone'} : $oldCttTel);
    my $oldCttEmail       = $tabInfos->{'contact.email'};
    my $newCttEmail       = (exists $tabParams->{'contact.email'} ? &LOC::Util::trim($tabParams->{'contact.email'}) : $oldCttEmail);
    my $oldIsAsbestos     = $tabInfos->{'isAsbestos'};
    my $newIsAsbestos     = (exists $tabParams->{'isAsbestos'} ? $tabParams->{'isAsbestos'} : $oldIsAsbestos);
    my $oldDeliveryHour   = $tabInfos->{'deliveryHour'};
    my $newDeliveryHour   = (exists $tabParams->{'deliveryHour'} ? &_toHour($tabParams->{'deliveryHour'}) : $oldDeliveryHour);
    my $oldDeliveryTSltId = $tabInfos->{'deliveryTimeSlot.id'};
    my $newDeliveryTSltId = (exists $tabParams->{'deliveryTimeSlot.id'} ? ($tabParams->{'deliveryTimeSlot.id'} ? $tabParams->{'deliveryTimeSlot.id'} * 1 : undef) : $oldDeliveryTSltId);
    my $oldRecoveryHour   = $tabInfos->{'recoveryHour'};
    my $newRecoveryHour   = (exists $tabParams->{'recoveryHour'} ? &_toHour($tabParams->{'recoveryHour'}) : $oldRecoveryHour);
    my $oldRecoveryTSltId = $tabInfos->{'recoveryTimeSlot.id'};
    my $newRecoveryTSltId = (exists $tabParams->{'recoveryTimeSlot.id'} ? ($tabParams->{'recoveryTimeSlot.id'} ? $tabParams->{'recoveryTimeSlot.id'} * 1 : undef) : $oldRecoveryTSltId);
    my $oldComments       = $tabInfos->{'comments'};
    my $newComments       = (exists $tabParams->{'comments'} ? &LOC::Util::trim($tabParams->{'comments'}) : $oldComments);
    my $oldIsHasWharf     = $tabInfos->{'isHasWharf'};
    my $newIsHasWharf     = (exists $tabParams->{'isHasWharf'} ? $tabParams->{'isHasWharf'} : $oldIsHasWharf);
    my $oldIsAnticipated  = $tabInfos->{'isAnticipated'};
    my $newIsAnticipated  = (exists $tabParams->{'isAnticipated'} ? $tabParams->{'isAnticipated'} : $oldIsAnticipated);

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);


    my $tabChanges = {};
    # Vérification des droits
    if (defined $rights)
    {
        # - Libellé
        if ($oldLabel ne $newLabel)
        {
            if (!&_hasRight($rights, 'label'))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            $tabChanges->{'label'} = {'old' => $oldLabel, 'new' => $newLabel};
        }
        # - Adresse
        if ($oldAddress ne $newAddress)
        {
            if (!&_hasRight($rights, 'address'))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            $tabChanges->{'address'} = {'old' => $oldAddress, 'new' => $newAddress};
        }
        # - Ville
        if ($oldLocalityId != $newLocalityId)
        {
            if (!&_hasRight($rights, 'locality'))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            $tabChanges->{'locality'} = {'old' => $oldLocalityId, 'new' => $newLocalityId};
        }
        # - Contact - Nom
        if ($oldCttFullName ne $newCttFullName)
        {
            if (!&_hasRight($rights, 'contact'))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            $tabChanges->{'contactFullName'} = {'old' => $oldCttFullName, 'new' => $newCttFullName};
        }
        # - Contact - téléphone
        if ($oldCttTel ne $newCttTel)
        {
            if (!&_hasRight($rights, 'contact'))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            $tabChanges->{'contactTel'} = {'old' => $oldCttTel, 'new' => $newCttTel};
        }
        # - Contact - E-mail
        if ($oldCttEmail ne $newCttEmail)
        {
            if (!&_hasRight($rights, 'contact'))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            $tabChanges->{'contactEmail'} = {'old' => $oldCttEmail, 'new' => $newCttEmail};
        }
        # - Amiante
        if ($oldIsAsbestos != $newIsAsbestos)
        {
            if ($newIsAsbestos && !&_hasRight($rights, 'activateAsbestos') ||
                !$newIsAsbestos && !&_hasRight($rights, 'deactivateAsbestos'))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            $tabChanges->{'isAsbestos'} = {'old' => $oldIsAnticipated, 'new' => $newIsAnticipated};
        }
        # - Heures de livraison et de récupération
        # -- horaire livraison
        if ($oldDeliveryHour ne $newDeliveryHour)
        {
            if (!&_hasRight($rights, 'hours'))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            $tabChanges->{'hours'} = {'old' => $oldDeliveryHour, 'new' => $newDeliveryHour};
        }
        # -- horaire récupération
        if ($oldRecoveryHour ne $newRecoveryHour)
        {
            if (!&_hasRight($rights, 'hours'))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            $tabChanges->{'hours'} = {'old' => $oldRecoveryHour, 'new' => $newRecoveryHour};
        }
        # -- plage livraison
        if ($oldDeliveryTSltId ne $newDeliveryTSltId)
        {
            if (!&_hasRight($rights, 'hours'))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            $tabChanges->{'hours'} = {'old' => $oldDeliveryTSltId, 'new' => $newDeliveryTSltId};
        }
        # - plage récupération
        if ($oldRecoveryTSltId ne $newRecoveryTSltId)
        {
            if (!&_hasRight($rights, 'hours'))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            $tabChanges->{'hours'} = {'old' => $oldRecoveryTSltId, 'new' => $newRecoveryTSltId};
        }
        # - Quai
        if ($oldIsHasWharf != $newIsHasWharf)
        {
            if (!&_hasRight($rights, 'wharf'))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            $tabChanges->{'wharf'} = {'old' => $oldIsHasWharf, 'new' => $newIsHasWharf};
        }
        # - Anticipation
        if ($oldIsAnticipated != $newIsAnticipated)
        {
            if (!&_hasRight($rights, 'anticipated'))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            $tabChanges->{'isAnticipated'} = {'old' => $oldIsAnticipated, 'new' => $newIsAnticipated};
        }
        # - Commentaires
        if ($oldComments ne $newComments)
        {
            if (!&_hasRight($rights, 'comments'))
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            $tabChanges->{'comments'} = {'old' => $oldComments, 'new' => $newComments};
        }
    }
    else
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
        return 0;
    }

    # Tests sur la ville
    if ($newLocalityId == 0)
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_CITYNOTVALID);
    }
    # Test sur le libellé chantier
    if ($newLabel eq '')
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_LABELREQUIRED);
    }
    # Contact chantier
    if ($documentType eq 'contract' && $newCttTel eq '')
    {
        push(@{$tabErrors->{'warning'}}, ERROR_CONTACTREQUIRED);
    }
    # Mail du contact chantier
    if ($newCttEmail && !&LOC::Util::isValidEmail($newCttEmail))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_CONTACTMAILNOTVALID);
    }

    # Si il y a des données invalides alors on arrête ici
    if (@{$tabErrors->{'fatal'}} > 0)
    {
        return 0;
    }

    # Détection des modifications
    if (defined $tabEndUpds)
    {
        # Récupération de la locale
        my $tabCountryInfos = &LOC::Country::getInfos($countryId);
        my $locale = &LOC::Locale::getLocale($tabCountryInfos->{'locale.id'}, undef,
                                             [POSIX::LC_MESSAGES, POSIX::LC_CTYPE]);

        # Modification du libellé du chantier
        if ($oldLabel ne $newLabel)
        {
            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDSITELABEL,
                'old'     => $oldLabel,
                'new'     => $newLabel,
                'content' => $oldLabel . ' → ' . $newLabel
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }

        # Modification de l'adresse du chantier
        if ($oldAddress ne $newAddress)
        {
            my $oldContent = $oldAddress;
            $oldContent =~ s/[\r\n]{1,2}/ /g;
            my $newContent = $newAddress;
            $newContent =~ s/[\r\n]{1,2}/ /g;

            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDSITEADDRESS,
                'old'     => $oldAddress,
                'new'     => $newAddress,
                'content' => $oldContent . ' → ' . $newContent
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }

        # Modification du contact chantier
        if ($oldCttFullName ne $newCttFullName)
        {
            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UDPSITECONTACTNAME,
                'old'     => $oldCttFullName,
                'new'     => $newCttFullName,
                'content' => $oldCttFullName . ' → ' . $newCttFullName
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }

        # Modification du téléphone chantier
        if ($oldCttTel ne $newCttTel)
        {
            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDSITECONTACTTELEPHONE,
                'old'     => $oldCttTel,
                'new'     => $newCttTel,
                'content' => $oldCttTel . ' → ' . $newCttTel
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }

        # Modification du mail chantier
        if ($oldCttEmail ne $newCttEmail)
        {
            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDSITECONTACTEMAIL,
                'old'     => $oldCttEmail,
                'new'     => $newCttEmail,
                'content' => $oldCttEmail . ' → ' . $newCttEmail
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }

        # Modification de la présence d'amiante
        if ($oldIsAsbestos != $newIsAsbestos)
        {
            my $oldContent = ($oldIsAsbestos ? $locale->t('Oui') : $locale->t('Non'));
            my $newContent = ($newIsAsbestos ? $locale->t('Oui') : $locale->t('Non'));

            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDSITEASBESTOS,
                'old'     => $oldIsAsbestos,
                'new'     => $newIsAsbestos,
                'content' => $oldContent . ' → ' . $newContent
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }

        # Modification de l'horaire de livraison
        if ($oldDeliveryHour ne $newDeliveryHour || $oldDeliveryTSltId ne $newDeliveryTSltId)
        {
            my $tabValues = {
                'schema'    => $schemaName,
                'side'      => 'delivery',
                'oldHour'   => $oldDeliveryHour,
                'oldTSltId' => $oldDeliveryTSltId,
                'newHour'   => $newDeliveryHour,
                'newTSltId' => $newDeliveryTSltId
            };
            &_addHourTraces($tabEndUpds, $documentType, $documentId, $tabValues);
        }

        # Modification de l'horaire de récupération
        if ($oldRecoveryHour ne $newRecoveryHour || $oldRecoveryTSltId ne $newRecoveryTSltId)
        {
            my $tabValues = {
                'schema'    => $schemaName,
                'side'      => 'recovery',
                'oldHour'   => $oldRecoveryHour,
                'oldTSltId' => $oldRecoveryTSltId,
                'newHour'   => $newRecoveryHour,
                'newTSltId' => $newRecoveryTSltId
            };
            &_addHourTraces($tabEndUpds, $documentType, $documentId, $tabValues);
        }

        # Modification de la livraison anticipée possible
        if ($oldIsAnticipated ne $newIsAnticipated)
        {
            my $oldContent = ($oldIsAnticipated ? $locale->t('Oui') : $locale->t('Non'));
            my $newContent = ($newIsAnticipated ? $locale->t('Oui') : $locale->t('Non'));

            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDISDELIVERYANTICIPATED,
                'old'     => $oldIsAnticipated,
                'new'     => $newIsAnticipated,
                'content' => $oldContent . ' → ' . $newContent
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }

        # Modification du quai
        if ($oldIsHasWharf ne $newIsHasWharf)
        {
            my $oldContent = ($oldIsHasWharf ? $locale->t('Oui') : $locale->t('Non'));
            my $newContent = ($newIsHasWharf ? $locale->t('Oui') : $locale->t('Non'));

            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDHASWHARF,
                'old'     => $oldIsHasWharf,
                'new'     => $newIsHasWharf,
                'content' => $oldContent . ' → ' . $newContent
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }

        # Modification des commentaires du chantier
        if ($oldComments ne $newComments)
        {
            my $oldContent = $oldComments;
            $oldContent =~ s/[\r\n]{1,2}/ /g;
            my $newContent = $newComments;
            $newContent =~ s/[\r\n]{1,2}/ /g;

            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDSITECOMMENT,
                'old'     => $oldComments,
                'new'     => $newComments,
                'content' => $oldContent . ' → ' . $newContent
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }

        # Modification de la ville chantier (ville et zone)
        my $newZone = (exists $tabParams->{'newZone'} ? $tabParams->{'newZone'} : 0);
        my $oldZone = (exists $tabParams->{'oldZone'} ? $tabParams->{'oldZone'} : 0);
        if ($newZone != $oldZone || $oldLocalityId ne $newLocalityId)
        {
            my $subQuery = '(SELECT CAST(CONCAT(VICP, " ", VINOM, " (%d)") AS CHAR CHARACTER SET utf8) FROM ' . $schemaName . '.VILLECHA WHERE VILLEAUTO = %d)';
            my $query = 'CONCAT(' . sprintf($subQuery, $oldZone, $oldLocalityId) . ', " → ", ' . sprintf($subQuery, $newZone, $newLocalityId) . ')';
            my $tabTrace = {
                'schema'   => $schemaName,
                'code'     => LOC::Log::EventType::UPDATESITECITY,
                'old'      => {
                    'zone'       => $oldZone,
                    'localityId' => $oldLocalityId
                },
                'new'      => {
                    'type'       => $newZone,
                    'localityId' => $newLocalityId
                },
                'sqlQuery' => $query
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Modification de la ligne dans la table CHANTIER
    my $tabUpdates = {
        'CHLIBELLE'       => $newLabel,
        'CHADRESSE'       => $newAddress,
        'CHCONTACT'       => $newCttFullName,
        'CHTELEPHONE'     => $newCttTel,
        'CHEMAIL'         => $newCttEmail,
        'CHHEUREDP'       => $newDeliveryHour,
        'CHPLAGEDP'       => $newDeliveryTSltId,
        'CHHEUREAR'       => $newRecoveryHour,
        'CHPLAGEAR'       => $newRecoveryTSltId,
        'CHCOMMENTAIRE'   => $newComments,
        'VILLEAUTO'       => $newLocalityId,
        'CHANTICIPATION'  => ($newIsAnticipated ? 1 : 0),
        'CHQUAI'          => ($newIsHasWharf ? 1 : 0),
        'CHAMIANTE'       => ($newIsAsbestos ? 1 : 0)
    };

    # Incrémentation du nombre de changement d'adresse effectués
    if ($oldAddress ne $newAddress || $oldLocalityId != $newLocalityId)
    {
        $tabUpdates->{'CHMODIFADRESSE*'} = 'CHMODIFADRESSE + 1';
    }

    if ($db->update('CHANTIER', $tabUpdates, {'CHAUTO' => $id}, $schemaName) == -1)
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }


    if (keys %$tabChanges > 0)
    {
        # Changements pouvant impacter le transport ? => on le lui communique
        &LOC::Transport::onExternalChanges($countryId, {
            'site' => {
                'id'    => $id,
                'event' => 'update',
                'props' => $tabChanges
            }
        }, $userId, undef, $tabEndUpds);
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: _hasRight
# Vérifier le droit à un champ
#
# Parameters:
# hashref $tab  - Tableau des droits
# string  $name - Nom du droit
#
# Returns:
# bool
sub _hasRight
{
    my ($tab, $name) = @_;

    return (ref($tab) eq 'HASH' ? ($tab->{$name} ? 1 : 0) : (index('|' . $tab . '|', '|' . $name . '|') == -1 ? 0 : 1));
}

# Function: _toHour
# Renvoie l'heure dans le format hh:mm
#
# Parameters:
# string $hourStr - Heure à formater
#
# Returns:
# string - Heure formatée au format hh::mm
sub _toHour
{
    my $hourStr = $_[0];
    $hourStr =~ s/[^0-9\:]//g;
    if ($hourStr eq ':' || $hourStr eq '')
    {
        return '';
    }
    my @tab = split(/:/, $hourStr);
    my $hours   = (exists $tab[0] ? ($tab[0] > 23 ? 23 : $tab[0] * 1) : 0);
    my $minutes = (exists $tab[1] ? ($tab[1] > 59 ? 59 : $tab[1] * 1) : 0);
    return sprintf('%02d:%02d', $hours, $minutes);
}

# Function: _addHourTraces
# Ajoute une trace de modification de l'heure de livraison ou de récupération
#
# Parameters:
# hashref  $tabEndUpds   - Tableau des mises à jour de fin
# string   $documentType - Type de document (estimate ou contract)
# int      $documentId   - L'identifiant du devis ou du contrat
# hashref  $tabValues    - Les valeurs à inclure dans l'hitorique :
#                          * schema    : nom du schéma
#                          * side      : côté de transport (delivery ou recovery)
#                          * oldHour   : ancienne heure
#                          * oldTSltId : identifiant de l'ancienne tranche horaire
#                          * newHour   : nouvelle heure
#                          * newTSltId : identifiant de la nouvelle tranche horaire
#
# Return:
# bool
sub _addHourTraces
{
    my ($tabEndUpds, $documentType, $documentId, $tabValues) = @_;

    my $subQuery = '(SELECT CONCAT(LPAD(tts_beginhour, 2, "0"), "-", LPAD(tts_endhour, 2, "0")) FROM ' . $tabValues->{'schema'} . '.p_transporttimeslot WHERE tts_id = %d)';

    my @tabOldValues = ();
    if ($tabValues->{'oldHour'} ne '')
    {
        push(@tabOldValues, '"' . $tabValues->{'oldHour'} . '"');
    }
    if ($tabValues->{'oldTSltId'} ne '')
    {
        if (@tabOldValues)
        {
            push(@tabOldValues, '" | "');
        }
        push(@tabOldValues, sprintf($subQuery, $tabValues->{'oldTSltId'}));
    }
    my @tabNewValues = ();
    if ($tabValues->{'newHour'} ne '')
    {
        push(@tabNewValues, '"' . $tabValues->{'newHour'} . '"');
    }
    if ($tabValues->{'newTSltId'} ne '')
    {
        if (@tabNewValues)
        {
            push(@tabNewValues, '" | "');
        }
        push(@tabNewValues, sprintf($subQuery, $tabValues->{'newTSltId'}));
    }
    my $query = 'CONCAT(' . join(', ', @tabOldValues, '" → "', @tabNewValues) . ')';

    my $tabTrace = {
        'schema'   => $tabValues->{'schema'},
        'code'     => ($tabValues->{'side'} eq 'delivery' ? LOC::Log::EventType::UPDDELIVERYHOUR : LOC::Log::EventType::UPDRECOVERYHOUR),
        'old'      => {
            $tabValues->{'side'} . 'Hour'       => $tabValues->{'oldHour'},
            $tabValues->{'side'} . 'TimeSlotId' => $tabValues->{'oldTSltId'}
        },
        'new'      => {
            $tabValues->{'side'} . 'Hour'       => $tabValues->{'newHour'},
            $tabValues->{'side'} . 'TimeSlotId' => $tabValues->{'newTSltId'}
        },
        'sqlQuery' => $query
    };

    return &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
}


1;