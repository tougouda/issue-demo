use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Statistics::Budget
# Module permettant d'obtenir les informations sur les statistiques
package LOC::Statistics::Budget;


use strict;

# Modules internes
use LOC::Db;


# Function: getInfos
# Retourne une entrée
#
# Parameters:
# int $id - Identifiant de l'entrée
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getInfos
{
    my ($id) = @_;

    my $tab = &getList(LOC::Util::GETLIST_ASSOC, {'id' => $id});
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Retourne la liste des entrées
#
# Parameters:
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres :
#                         - 'id' : liste des entrées,
#                         - 'agency' : liste des agences,
#                         - 'date' : liste des dates,
#
# Returns:
# hash|hashref|0 - Liste des entrées
sub _getData
{
    my ($id) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('statistics');

    my $query = '
SELECT
    bdd_id AS `id`,
    bdd_bud_id AS `entry.id`,
    bdd_type AS `type`,
    bdd_subtype AS `subType`,
    bdd_machinescount AS `machinesCount`,
    bdd_rentaldays AS `rentalDays`,
    bdd_monthrentaldays AS `monthRentalDays`,
    bdd_monthrentalturnover AS `monthRentalTurnover`,
    bdd_monthtotalturnover AS `monthTotalTurnover`
FROM h_budgetdata
WHERE bdd_bud_id = ' . $db->quote($id) . ';';

    my $tab = $db->fetchAssoc($query);

    # Formatage des données
    foreach my $tabRow (values @$tab)
    {
        $tabRow->{'id'}                  *= 1;
        $tabRow->{'entry.id'}            *= 1;
        $tabRow->{'machinesCount'}       *= 1;
        $tabRow->{'rentalDays'}          *= 1;
        $tabRow->{'monthRentalDays'}     *= 1;
        $tabRow->{'monthRentalTurnover'} *= 1;
        $tabRow->{'monthTotalTurnover'}  *= 1;
    }

    return (wantarray ? %$tab : $tab);
}

# Function: getList
# Retourne la liste des entrées
#
# Parameters:
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres :
#                         - 'id' : liste des entrées,
#                         - 'agency' : liste des agences,
#                         - 'date' : liste des dates,
#
# Returns:
# hash|hashref|0 - Liste des entrées
sub getList
{
    my ($format, $tabFilters) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('statistics');

    # Filtres
    my $whereQuery = '';
    # - Id
    if (defined $tabFilters->{'id'})
    {
        $whereQuery .= '
AND bud_id IN (' . $db->quote($tabFilters->{'id'}) . ')';
    }
    # - Agence
    if (defined $tabFilters->{'agency'})
    {
        $whereQuery .= '
AND bud_agc_id IN (' . $db->quote($tabFilters->{'agency'}) . ')';
    }
    # - Date
    if (defined $tabFilters->{'date'})
    {
        $whereQuery .= '
AND bud_date IN (' . $db->quote($tabFilters->{'date'}) . ')';
    }

    my $query;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    bud_id AS `id`,
    bud_date AS `date`,
    bud_is_complete AS `isComplete`,
    bud_agc_id AS `agency.id`,
    bud_are_id AS `area.id`,
    bud_cty_id AS `country.id`,
    bud_workingdays AS `workingDays`,
    bud_monthworkingdays AS `monthWorkingDays`,
    bud_date_calculation AS `calculationDate`
FROM h_budget
WHERE 1 ' .
$whereQuery . '
ORDER BY bud_date;';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*)
FROM h_budget
WHERE 1 ' .
$whereQuery . ';';
    }
    else
    {
        $query = '
SELECT
    bud_id AS `id`,
    bud_date AS `date`
FROM h_budget';
        $query .= '
WHERE 1 ' .
$whereQuery . '
ORDER BY bud_date;';
    }

    my $tab = $db->fetchList($format, $query, {}, 'id');

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        # Formatage des données
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}               *= 1;
            $tabRow->{'isComplete'}       *= 1;
            $tabRow->{'area.id'}          *= 1;
            $tabRow->{'workingDays'}      *= 1;
            $tabRow->{'monthWorkingDays'} *= 1;

            $tabRow->{'data'} = &_getData($tabRow->{'id'});
        }
    }

    return (wantarray ? %$tab : $tab);
}

1;
