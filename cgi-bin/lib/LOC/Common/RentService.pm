use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Common::RentService
# Module de gestion des services de location et de transport
package LOC::Common::RentService;


# Constants: États des services
# STATE_ACTIVE   - type de service actif
# STATE_INACTIVE - type de service inactif
use constant
{
    STATE_ACTIVE   => 'GEN01',
    STATE_INACTIVE => 'GEN03'
};

# Constants: États des services sur un contrat
# STATE_UNBILLED - Service non facturé sur le contrat
# STATE_BILLED   - Service facturé sur le contrat
# STATE_DELETED  - Service supprimé
use constant
{
    STATE_UNBILLED => 'SER01',
    STATE_BILLED   => 'SER02',
    STATE_DELETED  => 'SER03'
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Util;


# Function: getInfos
# Retourne un hachage contenant les infos d'un service
#
# Parameters:
# string $countryId - Pays
# int    $id        - Id du service
#
# Returns:
# hash|hashref
sub getInfos
{
    my ($countryId, $id, $flags) = @_;

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Récupérer la liste des services de location et transport
#
# Contenu du hachage:
# - id          => id
# - currency.id => id de la devise
# - state.id    => code de l'état
# - name        => libellé
# - code        => code
# - type        => type d'imputation
# - amount      => montant
# - infos       => Informations sur le service
# - state.id    => état

#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id)
# int     $flags      - Flags
#
# Returns:
# hash|hashref
sub getList
{
    my ($countryId, $format, $tabFilters, $flags) = @_;
    unless (defined $flags)
    {
        $flags = 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '';

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    rsr_id AS `id`,
    rsr_cur_id AS `currency.id`,
    rsr_sta_id AS `state.id`,
    rsr_name AS `name`,
    rsr_code AS `code`,
    rsr_type AS `type`,
    rsr_comment AS `infos`,
    rsr_amount AS `amount`
FROM p_rentservice
WHERE rsr_sta_id <> ' . $db->quote(STATE_INACTIVE);
        if (defined $tabFilters->{'id'})
        {
            if (ref($tabFilters->{'id'}) eq 'ARRAY')
            {
                $query .= '
AND rsr_id IN (' . join(', ', @{$tabFilters->{'id'}}) . ')';
            }
            else
            {
                $query .= '
AND rsr_id = ' . $tabFilters->{'id'};
            }
        }
    }
    elsif($format == LOC::Util::GETLIST_PAIRS)
    {
        $query = '
SELECT
    rsr_id AS `id`,
    rsr_name AS `name`
FROM p_rentservice
WHERE rsr_sta_id <> ' . $db->quote(STATE_INACTIVE);
        if (defined $tabFilters->{'id'})
        {
            if (ref($tabFilters->{'id'}) eq 'ARRAY')
            {
                $query .= '
AND rsr_id IN (' . join(', ', @{$tabFilters->{'id'}}) . ')';
            }
            else
            {
                $query .= '
AND rsr_id = ' . $tabFilters->{'id'};
            }
        }
    }
    $query .= '
ORDER BY rsr_type, rsr_name;'; 

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    return (wantarray ? %$tab : $tab);
}

# Function: set
# Met à jour (insert/update) toutes les infos d'une service
#
# Contenu du hachage à passer en paramètre:
# - id          => id
# - currency.id => id de la devise
# - name        => libellé
# - code        => code
# - type        => type d'imputation
# - amount      => montant
# - infos       => informations sur le service
#
# Parameters:
# string    $countryId - Pays
# hashref   $values    - Valeurs du service
#
# Returns:
# integer
sub set
{
    my ($countryId, $values) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Mise à jour
    if (defined $values->{'id'} && $values->{'id'} != 0)
    {
        my $result = $db->update('p_rentservice', {
                                                 'rsr_name'     => $values->{'name'},
                                                 'rsr_cur_id'   => $values->{'currency.id'},
                                                 'rsr_code'     => $values->{'code'},
                                                 'rsr_type'     => $values->{'type'},
                                                 'rsr_amount'   => $values->{'amount'},
                                                 'rsr_comment'  => $values->{'infos'}
                                                 },
                                                 {
                                                 'rsr_id'                => $values->{'id'}
                                                 });
        return 1;
    }
    # Création
    else
    {
        my $rsvId = $db->insert('p_rentservice', {
                                                 'rsr_sta_id'   => STATE_ACTIVE,
                                                 'rsr_name'     => $values->{'name'},
                                                 'rsr_cur_id'   => $values->{'currency.id'},
                                                 'rsr_code'     => $values->{'code'},
                                                 'rsr_type'     => $values->{'type'},
                                                 'rsr_amount'   => $values->{'amount'},
                                                 'rsr_comment'  => $values->{'infos'}
                                                 });
        return $rsvId;
    }
}

# Function: delete
# Supprime d'un service (change l'état à GEN03)
#
# Parameters:
# string    $countryId   - Pays
# integer   $id          - Id du service
#
# Returns:
# integer
sub delete
{
    my ($countryId, $id) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    return $db->update('p_rentservice', {'rsr_sta_id' => STATE_INACTIVE}, {'rsr_id' => $id});
}

1;