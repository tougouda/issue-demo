use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Common::Rent
# Module contenant des méthodes et constantes pour la location
package LOC::Common::Rent;
use strict;

use LOC::Common;
use LOC::Characteristic;
use LOC::Date;
use LOC::Util;
use LOC::Insurance::Type;


# Function: getDailyPrice
# Calcul du prix journalier
#
# Parameters:
# string $amountType       - Type de montant
# float  $unitPrice        - Prix unitaire
# int    $duration         - Durée (jours ouvrés)
# int    $monthWorkingDays - Nombre de jours ouvrés dans le mois
#
# Returns:
# float - Montant journalier
sub getDailyPrice
{
    my ($amountType, $unitPrice, $duration, $monthWorkingDays) = @_;

    if (!defined $monthWorkingDays)
    {
        $monthWorkingDays = LOC::Date::AVERAGE_MONTH_WORKING_DAYS;
    }

    my $dailyPrice = 0;
    if ($amountType eq 'day')
    {
        $dailyPrice = $unitPrice;
    }
    elsif ($amountType eq 'month')
    {
        $dailyPrice = $unitPrice / $monthWorkingDays;
    }
    elsif ($amountType eq 'fixed')
    {
        $dailyPrice = $unitPrice / $duration;
    }

    return $dailyPrice;
}



# Function: calculateAppeal
# Calcul du montant de la gestion de recours
#
# Parameters:
# string   $countryId       - Identifiant du pays
# int      $appealTypeId    - Type de gestion de recours
# int      $mode            - Mode de calcul
# float    $value           - Valeur de la gestion de recours pour le calcul (pourcentage)
# float    $dailyPrice      - Prix journalier
# string   $beginDate       - Date de début
# string   $endDate         - Date de fin
# int      $additionalDays  - Jours supplémentaires
# int      $deductedDays    - Jours déduits
# hashref  $tabOptions      - Tableau des options supplémentaires
#
# Returns:
# hash|float - Montant de la gestion de recours
sub calculateAppeal
{
    my ($countryId, $agencyId, $appealTypeId, $value, $dailyPrice,
        $beginDate, $endDate, $additionalDays, $deductedDays, $tabOptions) = @_;

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    if (!defined $appealTypeId)
    {
        return (wantarray ? () : 0);
    }

    my $duration = &LOC::Date::getRentDuration($beginDate, $endDate, $agencyId, $tabOptions->{'contractEndDate'});
    $duration += $additionalDays - $deductedDays;

    my $tabAppealTypeInfos = &LOC::Appeal::Type::getInfos($countryId, $appealTypeId);
    my %tabAppealInfos     = &calculateAppealInfos($tabAppealTypeInfos->{'mode'},
                                                     $value, $dailyPrice,
                                                     $duration, $beginDate, $endDate,
                                                     $additionalDays, $deductedDays, $tabOptions);

    return (wantarray ? %tabAppealInfos : $tabAppealInfos{'amount'});
}


# Function: calculateAppealInfos
# Calcul des informations de la gestion de recours
#
# Parameters:
# int      $appealTypeInfos - Infos sur le type de gestion de recours
# float    $value           - Valeur de la gestion de recours pour le calcul (pourcentage)
# float    $dailyPrice      - Prix journalier
# int      $duration        - Durée (jours ouvrés)
# string   $beginDate       - Date de début
# string   $endDate         - Date de fin
# int      $additionalDays  - Jours supplémentaires
# int      $deductedDays    - Jours déduits
# hashref  $tabOptions      - Tableau des options supplémentaires
#
# Returns:
# hash - Informations sur la gestion de recours
sub calculateAppealInfos
{
    my ($appealMode, $appealRate, $dailyPrice, $duration,
        $beginDate, $endDate, $additionalDays, $deductedDays, $tabOptions) = @_;

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }
    if (!exists $tabOptions->{'round'})
    {
        $tabOptions->{'round'} = 2;
    }

    my $appealAmount = 0;
    my $appealDuration = 0;

    # Ajustement de la durée de gestion de recours par rapport au mode de calcul
    # - Jours calendaires
    if ($appealMode == LOC::Appeal::Type::MODE_CALENDAR)
    {
        # Calcul des jours calendaires de la période du contrat
        if ($beginDate && $endDate)
        {
            $appealDuration = &LOC::Date::getDeltaDays($beginDate, $endDate);
        }
    }

    $appealAmount   = $appealDuration * $dailyPrice * $appealRate;

    # Arrondi de la gestion de recours à 2 décimales
    if (defined $tabOptions->{'round'})
    {
        $appealAmount = &LOC::Util::round($appealAmount, $tabOptions->{'round'});
    }

    return ('amount' => $appealAmount, 'duration' => $appealDuration);
}


# Function: calculateInsurance
# Calcul de l'assurance
#
# Parameters:
# string   $countryId       - Pays
# string   $agencyId        - Agence
# int      $insuranceTypeId - Type d'assurance
# float    $insuranceRate   - Taux d'assurance
# float    $dailyPrice      - Prix journalier
# string   $beginDate       - Date de début
# string   $endDate         - Date de fin
# int      $additionalDays  - Jours supplémentaires
# int      $deductedDays    - Jours déduits
# hashref  $tabOptions      - Tableau des options supplémentaires
#
# Returns:
# hash|float - Montant de l'assurance
sub calculateInsurance
{
    my ($countryId, $agencyId, $insuranceTypeId, $insuranceRate, $dailyPrice,
        $beginDate, $endDate, $additionalDays, $deductedDays, $tabOptions) = @_;

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }
 
    my $tabInsuranceTypeInfos = &LOC::Insurance::Type::getInfos($countryId, $insuranceTypeId);

    my $duration = &LOC::Date::getRentDuration($beginDate, $endDate, $agencyId, $tabOptions->{'contractEndDate'});
    $duration += $additionalDays - $deductedDays;

    my %tabInsuranceInfos = &calculateInsuranceInfos($tabInsuranceTypeInfos->{'flags'}, $tabInsuranceTypeInfos->{'mode'},
                                                     $insuranceRate, $dailyPrice, $duration,
                                                     $beginDate, $endDate,
                                                     $additionalDays, $deductedDays, $tabOptions);

    return (wantarray ? %tabInsuranceInfos : $tabInsuranceInfos{'amount'});
}


# Function: calculateInsuranceInfos
# Calcul de l'assurance
#
# Parameters:
# int      $insuranceTypeFlags - Flags sur le type d'assurance
# int      $insuranceTypeMode  - Mode du type d'assurance
# float    $insuranceRate      - Taux d'assurance
# float    $dailyPrice         - Prix journalier
# int      $duration           - Durée (jours ouvrés)
# string   $beginDate          - Date de début
# string   $endDate            - Date de fin
# int      $additionalDays     - Jours supplémentaires
# int      $deductedDays       - Jours déduits
# hashref  $tabOptions         - Tableau des options supplémentaires
#
# Returns:
# hash - Informations sur l'assurance
sub calculateInsuranceInfos
{
    my ($insuranceTypeFlags, $insuranceTypeMode, $insuranceRate, $dailyPrice, $duration,
        $beginDate, $endDate, $additionalDays, $deductedDays, $tabOptions) = @_;

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }
    if (!exists $tabOptions->{'round'})
    {
        $tabOptions->{'round'} = 2;
    }

    my $insuranceAmount = 0;
    my $insuranceDuration = 0;

    if ($insuranceTypeFlags & LOC::Insurance::Type::FLAG_ISINVOICED)
    {
        # Ajustement de la durée d'assurance par rapport au mode de calcul
        # - Jours calendaires
        if (&LOC::Util::in_array($insuranceTypeMode, [LOC::Insurance::Type::MODE_CALENDARADDAYS, LOC::Insurance::Type::MODE_CALENDAR]))
        {
            # Calcul des jours calendaires de la période du contrat
            if ($beginDate && $endDate)
            {
                $insuranceDuration = &LOC::Date::getDeltaDays($beginDate, $endDate);
            }

            # La durée d'assurance est égale à la durée calendaire + les jours plus - les jours moins
            if ($insuranceTypeMode == LOC::Insurance::Type::MODE_CALENDARADDAYS)
            {
                $insuranceDuration = $insuranceDuration + $additionalDays - $deductedDays;
            }
        }
        # - Jours ouvrés
        elsif ($insuranceTypeMode == LOC::Insurance::Type::MODE_WORKING)
        {
            $insuranceDuration = $duration;
        }

        $insuranceAmount = $insuranceDuration * $dailyPrice * $insuranceRate;

        # Arrondi de l'assurance à 2 décimales
        if (defined $tabOptions->{'round'})
        {
            $insuranceAmount = &LOC::Util::round($insuranceAmount, $tabOptions->{'round'});
        }
    }

    return ('amount' => $insuranceAmount, 'duration' => $insuranceDuration);
}


# Function: calculateResiduesAmount
# Calcul du montant de la participation au recyclage
#
# Parameters:
# string   $agencyId       - Agence
# int      $mode           - Mode de calcul
# float    $value          - Valeur de la participation au recyclage pour le calcul (montant forfaitaire, pourcentage)
# float    $dailyPrice     - Prix journalier
# string   $beginDate      - Date de début
# string   $endDate        - Date de fin
# int      $additionalDays - Jours supplémentaires
# int      $deductedDays   - Jours déduits
# float    $repairAmount   - Montant de la remise en état
# hashref  $tabOptions      - Tableau des options supplémentaires
#
# Returns:
# float - Montant de la participation au recyclage
sub calculateResiduesAmount
{
    my ($agencyId, $mode, $value, $dailyPrice,
        $beginDate, $endDate, $additionalDays, $deductedDays, $repairAmount, $tabOptions) = @_;

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }
    if (!exists $tabOptions->{'round'})
    {
        $tabOptions->{'round'} = 2;
    }

    my $residuesAmount = 0;

    # Suivant le mode de calcul
    if ($mode == LOC::Common::RESIDUES_MODE_PRICE)
    {
        # Montant forfaitaire
        $residuesAmount = $value;
    }
    elsif ($mode == LOC::Common::RESIDUES_MODE_PERCENT)
    {
        # Montant calculé (pourcentage)
        my $rate = $value;
        my $residuesDuration = 0;
        if ($beginDate && $endDate)
        {
            $residuesDuration = &LOC::Date::getRentDuration($beginDate, $endDate, $agencyId, $tabOptions->{'contractEndDate'});
        }
        my $realResiduesDuration = $residuesDuration + $additionalDays - $deductedDays;

        my $residuesBaseAmount = $realResiduesDuration * $dailyPrice;
        # Prise en compte de la remise en état dans le montant de base du calcul
        if (defined $repairAmount)
        {
            $residuesBaseAmount += $repairAmount;
        }

        $residuesAmount = $residuesBaseAmount * $rate;
    }
    # Arrondi de la participation au recyclage à 2 décimales
    # /!\ Attention à l'arrondi avec un chiffre à plus de 2 décimales /!\
    # (10 * 95 x 0.0195 = 18.525 (=18.524999999998 pour la machine) et donc son arrondi à 2 décimales = 18.52 au lieu de 18.53)
    # Il faut donc ajouter un epsilon.
    if (defined $tabOptions->{'round'})
    {
        my $epsilon = $tabOptions->{'round'} + LOC::Util::ROUND_ADDDEC;
        $residuesAmount = &LOC::Util::round($residuesAmount, $tabOptions->{'round'}, $epsilon);
    }
    return $residuesAmount;
}


# Function: calculateDiffDaysByActions
# Retourne les différences de jours+/- à prendre en compte dans la durée
#
# Parameters:
# string   $countryId      - Identifiant du pays
# arrayref $tabDaysActions - Tableau des actions
#
# Return:
# hashref
sub calculateDiffDaysByActions
{
    my ($countryId, $tabDaysActions) = @_;

    my $nb = 0;
    my $tabInvoiceDurations = &LOC::Day::InvoiceDuration::getList($countryId, LOC::Util::GETLIST_ASSOC);

    my $tabDiffDays = {
        LOC::Day::TYPE_ADDITIONAL() => 0,
        LOC::Day::TYPE_DEDUCTED()   => 0
    };
    foreach my $dayAction (@$tabDaysActions)
    {
        my $tabData = $dayAction->{'data'};
        my $tabInfos = &LOC::Day::getInfos($countryId, $dayAction->{'id'});

        my $duration = (defined $tabInvoiceDurations->{$tabData->{'invoiceDurationId'}} ?
                                $tabInvoiceDurations->{$tabData->{'invoiceDurationId'}}->{'duration'} : 0);

        # - Ajout de durée
        # -- Action de type 'add'
        if ($dayAction->{'type'} eq 'add' && $tabData->{'isInvoiceable'})
        {
            $tabDiffDays->{$tabData->{'type'}} += $duration;
        }
        # -- Action de type 'edit' avec un état facturable
        if ($dayAction->{'type'} eq 'edit' && $tabInfos->{'isInvoiceable'})
        {
            $tabDiffDays->{$tabInfos->{'type'}} += $duration;
        }
        # - Ajout de durée
        # -- Action de type 'setInvoiceable'
        if ($dayAction->{'type'} eq 'setInvoiceable')
        {
            $tabDiffDays->{$tabInfos->{'type'}} += $tabInfos->{'invoiceDuration.duration'};
        }
        # - Suppression de durée
        # -- Action de type 'edit', 'delete' avec un état facturable
        # -- Action de type 'setNonInvoiceable'
        if ($tabInfos->{'isInvoiceable'} && &LOC::Util::in_array($dayAction->{'type'}, ['edit', 'remove']) ||
               $dayAction->{'type'} eq 'setNonInvoiceable')
        {
            # Enlève l'ancienne durée
            $tabDiffDays->{$tabInfos->{'type'}} -= $tabInfos->{'invoiceDuration.duration'};
        }
    }

    return $tabDiffDays;
}

1;