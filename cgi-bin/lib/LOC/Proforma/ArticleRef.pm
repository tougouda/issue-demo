use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Invoice::ArticleRef
# Module de gestion des références articles
package LOC::Proforma::ArticleRef;


# Constants: Récupération d'informations sur les articles pro forma
# GETINFOS_ALL  - Récupération de l'ensemble des informations
# GETINFOS_BASE - Récupération des informations de base
use constant
{
    GETINFOS_ALL  => 0xFFFFFFFF,
    GETINFOS_BASE => 0x00000000
};

# Constants: États
# STATE_ACTIVE   - Alerte active
# STATE_INACTIVE - Alerte inactive
use constant
{
    STATE_ACTIVE   => 'GEN01',
    STATE_INACTIVE => 'GEN03'
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Util;


# Function: getInfos
# Retourne un hachage contenant les infos d'une référence article
#
# Parameters:
# string  $countryId  - Pays
# string  $id         - Id de la référence
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Récupérer la liste des références articles
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id)
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT';

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query .= '
    pfa_id AS `id`,
    pfa_name AS `name`,
    pfa_label AS `label`,
    pfa_labelextra AS `labelExtra`,
    pfa_vat_id AS `vat.id`,
    pfa_sta_id AS `state.id`,
    pfa_order AS `order`';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query .= '
    COUNT(*)';
    }
    else
    {
        $query .= '
    pfa_id,
    pfa_name';
    }

    $query .= '
FROM p_proformaartref
WHERE pfa_sta_id <> ' . $db->quote(STATE_INACTIVE);

    # Recherche par id
    if (exists $tabFilters->{'id'})
    {
        $query .= '
AND pfa_id IN (' . $db->quote($tabFilters->{'id'}) . ')';
    }

    $query .= '
ORDER BY pfa_order ASC'; 

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    # Formatage des champs
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'order'} *= 1;
        }
    }

    return (wantarray ? %$tab : $tab);
}


# Function: set
# Met à jour (insert/update) toutes les infos d'une référence article en BDD
#
# Contenu du hachage à passer en paramètre:
# - id          => id
# - name        => nom
# - label       => libellé
# - labelExtra  => libellé complémentaire
# - vat.id      => id du taux de tva
#
# Parameters:
# string    $countryId - Pays
# hashref   $values    - Valeurs de la référence
#
# Returns:
# integer
sub set
{
    my ($countryId, $values) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = 'SELECT COUNT(*) FROM p_proformaartref WHERE pfa_id = ' . $db->quote($values->{'id'}) . ';';

    # Mise à jour
    my $result;
    if ($db->fetchOne($query) == 1)
    {
        $result = $db->update('p_proformaartref', {
                                                      'pfa_id'     => $values->{'id'},
                                                      'pfa_name'   => $values->{'name'},
                                                      'pfa_label'  => $values->{'label'},
                                                      'pfa_labelextra' => $values->{'labelExtra'},
                                                      'pfa_vat_id' => $values->{'vat.id'},
                                                      'pfa_sta_id' => STATE_ACTIVE
                                                  },
                                                  {
                                                      'pfa_id'     => $values->{'id'}
                                                  });
    }
    # Création
    else
    {
        $result = $db->insert('p_proformaartref', {
                                                      'pfa_id'     => $values->{'id'},
                                                      'pfa_name'   => $values->{'name'},
                                                      'pfa_label'  => $values->{'label'},
                                                      'pfa_labelextra' => $values->{'labelExtra'},
                                                      'pfa_vat_id' => $values->{'vat.id'},
                                                      'pfa_sta_id' => STATE_ACTIVE
                                                  });
    }

    return ($result == -1 ? 0 : 1);
}

# Function: delete
# Supprime une référence article
#
# Parameters:
# string   $countryId   - Pays
# string   $id          - Id de la référence
#
# Returns:
# integer
sub delete
{
    my ($countryId, $id) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    return $db->update('p_proformaartref', {'pfa_sta_id' => STATE_INACTIVE}, {'pfa_id' => $id});
}

1;