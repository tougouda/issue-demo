use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Proforma::Type
# Module de gestion des types de pro forma
package LOC::Proforma::Type;


# Constants: États des types de pro forma
# STATE_ACTIVE  - Actif
# STATE_DELETED - Supprimé
use constant
{
    STATE_ACTIVE  => 'GEN01',
    STATE_DELETED => 'GEN03'
};

# Constants: Récupération d'informations sur les types de pro forma
# GETINFOS_ALL  - Récupération de l'ensemble des informations
# GETINFOS_BASE - Récupération des informations de base
use constant
{
    GETINFOS_ALL  => 0xFFFF,
    GETINFOS_BASE => 0x0000
};

# Constants: Types de pro forma
# ID_PROFORMA  - Pro forma
# ID_ADVANCE   - Facture d'acompte
# ID_EXTENSION - Pro forma de prolongation
use constant
{
    ID_PROFORMA   => 1,
    ID_ADVANCE    => 2,
    ID_EXTENSION  => 3
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Util;




# Function: delete
# Supprime un type de pro forma
#
# Parameters:
# string    $countryId   - Pays
# integer   $id          - Id du type
#
# Returns:
# integer
sub delete
{
    my ($countryId, $id) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    return $db->update('p_proformatype', {'pft_sta_id' => STATE_DELETED}, {'pft_id' => $id});
}


# Function: getInfos
# Retourne un hachage contenant les infos d'un type de pro forma
#
# Parameters:
# string  $countryId  - Pays
# int     $id         - Id du type
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;

    $id *= 1;
    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC,
                       {'id' => $id, 'stateId' => [STATE_ACTIVE, STATE_DELETED]},
                       $flags, $tabOptions);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Récupérer la liste des types de pro forma
#
# Contenu du hachage:
# - id               => id
# - label            => libellé
# - flags            => flags
# - creationDate     => Date de création
# - modificationDate => Date de modification
# - state.id         => état
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id, code)
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT';

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query .= '
    pft_id AS `id`,
    pft_label AS `label`,
    pft_flags AS `flags`,
    pft_date_create AS `creationDate`,
    pft_date_modif AS `modificationDate`,
    pft_sta_id AS `state.id`';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query .= '
    COUNT(*)';
    }
    else
    {
        $query .= '
    pft_id,
    pft_label';
    }

    $query .= '
FROM p_proformatype';

    my @tabWheres = ();

    my $tabStates = (defined $tabFilters->{'stateId'} ? $tabFilters->{'stateId'} : [STATE_ACTIVE]);
    push(@tabWheres, 'pft_sta_id IN (' . $db->quote($tabStates) . ')');
    if (exists $tabFilters->{'id'})
    {
        push(@tabWheres, 'pft_id IN (' . &LOC::Util::join(', ', $tabFilters->{'id'}) . ')');
    }

    if (@tabWheres > 0)
    {
        $query .= '
WHERE ' . join("\nAND ", @tabWheres);
    }

    $query .= '
ORDER BY pft_label ASC;';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    # Formatage des champs
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}    *= 1;
            $tabRow->{'flags'} *= 1;
        }
    }

    return (wantarray ? %$tab : $tab);
}

# Function: set
# Met à jour (insert/update) toutes les infos d'un type de pro forma
#
# Contenu du hachage à passer en paramètre:
# - id          => id
# - label       => libellé
# - flags       => flags
#
# Parameters:
# string    $countryId  - Pays
# hashref   $tabUpdates - Valeurs
#
# Returns:
# integer
sub set
{
    my ($countryId, $tabUpdates) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Mise à jour
    if (defined $tabUpdates->{'id'} && $tabUpdates->{'id'} != 0)
    {
        $db->update('p_proformatype', {
                                          'pft_label' => $tabUpdates->{'label'},
                                          'pft_flags' => $tabUpdates->{'flags'}
                                      },
                                      {
                                          'pft_id'    => $tabUpdates->{'id'}
                                      });

        return 1;
    }
    # Création
    else
    {
        return $db->insert('p_proformatype', {
                                                 'pft_label'        => $tabUpdates->{'label'},
                                                 'pft_flags'        => $tabUpdates->{'flags'},
                                                 'pft_date_create*' => 'Now()'
                                             });

    }
}

1;