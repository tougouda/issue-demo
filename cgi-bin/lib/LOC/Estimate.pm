use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Estimate
# Module de fonctions communes aux diffrents types de devis
package LOC::Estimate;
use strict;

# Modules internes
use LOC::Db;
use LOC::Util;


# Function: getAbortInfos
# Retourne les infos d'un motif d'abandon
#
# Parameters:
# string $countryId - Pays
# int    $id        - Id du motif d'abandon
# int    $flags     - Flags
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, UNDEF si pas de résultat
sub getAbortInfos
{
    my ($countryId, $id, $flags) = @_;

    my $tab = &getAbortList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}


# Function: getAbortList
# Récupérer la liste des motifs d'abandon
#
# Contenu du hashage :
# - id               => Identifiant du motif d'abandon
# - label            => Libellé du motif
# - requiredComments => Indique si un commentaire est obligatoire pour ce motif
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres ('id' => Id des motifs d'abandon)
# int     $flags      - Flags
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, UNDEF si pas de résultat
sub getAbortList
{
    my ($countryId, $format, $tabFilters, $flags) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT 
    tbl_p_abortreason.MOTIFAUTO AS `id`, 
    tbl_p_abortreason.MOTIFLIBELLE AS `label`,
    -tbl_p_abortreason.MOTIFCOMMOBLIG AS `requiredComments`
FROM MOTIFANNUL tbl_p_abortreason
WHERE tbl_p_abortreason.MOTIFACTIF = -1';
    if (defined $tabFilters->{'id'})
    {
        $query .= '
AND tbl_p_abortreason.MOTIFAUTO IN (' . $db->quote($tabFilters->{'id'}) . ')';
    }
    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }
    return (wantarray ? %$tab : $tab);
}

1;