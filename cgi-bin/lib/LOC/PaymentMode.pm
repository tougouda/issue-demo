use utf8;
use open (':encoding(UTF-8)');

# Package : LOC::PaymentMode
# Module permettant d'obtenir les informations sur les modes de paiement
package LOC::PaymentMode;


# Constants: Récupération d'informations sur les modes de paiement
# GETINFOS_ALL  - Récupération de l'ensemble des informations
# GETINFOS_BASE - Récupération des informations de base
use constant
{
    GETINFOS_ALL  => 0xFFFFFFFF,
    GETINFOS_BASE => 0x00000000
};

# Constants: Drapeaux de typage
# FLAG_TRANSFER - Type virement
# FLAG_CHECK    - Type chèque
use constant
{
    FLAG_TRANSFER => 0x00000001,
    FLAG_CHECK    => 0x00000002
};


use strict;

# Module internes
use LOC::Db;



# Function: getInfos
# Retourne un hachage contenant les infos d'un mode de paiement
#
# Parameters:
# string  $countryId  - Pays
# int     $id         - Id du mode de paiement
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;

    $id *= 1;
    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Récupérer la liste des modes de paiement
#
# Contenu du hachage:
# - id               => id
# - label            => libellé
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id, code)
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT';

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query .= '
    tbl_p_paymentmode.MOPAAUTO AS `id`,
    tbl_p_paymentmode.MOPALIBELLE AS `label`,
    tbl_p_paymentmode.MOPAFLAGS AS `flags`';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query .= '
    COUNT(*)';
    }
    else
    {
        $query .= '
    tbl_p_paymentmode.MOPAAUTO,
    tbl_p_paymentmode.MOPALIBELLE';
    }

    $query .= '
FROM MODEPAIE tbl_p_paymentmode';

    my @tabWheres = ();

    if (exists $tabFilters->{'id'})
    {
        push(@tabWheres, 'tbl_p_paymentmode.MOPAAUTO IN (' . &LOC::Util::join(', ', $tabFilters->{'id'}) . ')');
    }

    if (exists $tabFilters->{'isForProforma'})
    {
        push(@tabWheres, 'tbl_p_paymentmode.MOPAPROFORMA = ' . ($tabFilters->{'isForProforma'} ? 1 : 0));
    }

    if (exists $tabFilters->{'flags'})
    {
        my $flags = $tabFilters->{'flags'} * 1;
        push(@tabWheres, '(tbl_p_paymentmode.MOPAFLAGS & ' . $flags . ') = ' . $flags);
    }

    if (@tabWheres > 0)
    {
        $query .= '
WHERE ' . join("\nAND ", @tabWheres);
    }

    $query .= '
ORDER BY tbl_p_paymentmode.MOPALIBELLE ASC;';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    # Formatage des champs
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}    *= 1;
            $tabRow->{'flags'} *= 1;
        }
    }

    return (wantarray ? %$tab : $tab);
}




1;