use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Invoice::Repair
# Module permettant d'obtenir les informations sur les factures de remise en état
package LOC::Invoice::Repair;

# Constants: Erreurs
# ERROR_FATAL                 - Erreur fatale
use constant
{
    ERROR_FATAL                 => 0x0001
};

use strict;

# Modules additionnels
use Encode;

# Modules internes
use LOC::Country;
use LOC::Db;
use LOC::Globals;
use LOC::Invoice;
use LOC::Repair::Estimate;

my $fatalErrMsg = 'Erreur fatale: module LOC::Invoice, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";



# Function: getInfos
# Retourne les infos d'une facture de remise en état
#
# Contenu du hashage :
# - id : identifiant de la facture
# - number : numéro de la facture
# - invoicingDate : date de génération de la facture ;
# - date : date de la facture ;
# - contract.id : identifiant du contrat ;
# - contract.code : code du contrat ;
# - contract.beginDate : date de début du contrat ;
# - customer.id : identifiant du client ;
# - customer.code : code du client ;
# - customer.name : raison sociale du client ;
# - customer.address1 : adresse du client ;
# - customer.address2 : complément d'adresse du client ;
# - customer.address3 : code postal et ville du client ;
# - header.line1 : première ligne d'en-tête ;
# - header.line2 : deuxième ligne d'en-tête ;
# - header.line3 : troisième ligne d'en-tête ;
# - header.line4 : quatrième ligne d'en-tête ;
# - generalAccount : code collectif ;
# - amount : montant total HT de la facture ;
# - agency.id : identifiant de l'agence ;
# - line : tableau associatif contenant les lignes de la facture :
#   * id : identifiant de la ligne ;
#   * article : code article ;
#   * designation : désignation ;
#   * price : prix unitaire ;
#   * quantity : quantité ;
#   * reduction : pourcentage de réduction ;
#   * vatRate : taux de TVA ;
#
# Parameters:
# string $id        - Identifiant du modèle
# string $countryId - Identifiant du pays
# string $type      - Type (LOC::Invoice::TYPE_REPAIR ou LOC::Invoice::TYPE_OLDREPAIR)
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getInfos
{
    my ($countryId, $id, $type) = @_;
    unless (defined $type)
    {
        $type = LOC::Invoice::TYPE_REPAIR;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $tabResult;

    # Ancien système de casse
    if ($type eq LOC::Invoice::TYPE_OLDREPAIR)
    {
        # Récupère les données d'en-tête
        my $query = '
SELECT
    tbl_f_repair.CASSEAUTO AS `id`,
    tbl_f_repair.CADATEFACT AS `invoicingDate`,
    DATE_SUB(CONCAT(YEAR(tbl_f_repair.CADATEFACT), "-", MONTH(tbl_f_repair.CADATEFACT), "-01"), INTERVAL 1 DAY) AS `date`,
    tbl_f_repair.CONTRATAUTO AS `contract.id`,
    tbl_f_contract.CONTRATCODE AS `contract.code`,
    tbl_f_contract.CONTRATDD AS `contract.beginDate`,
    tbl_f_customer.CLAUTO AS `customer.id`,
    tbl_f_customer.CLCODE AS `customer.code`,
    tbl_f_customer.CLSTE AS `customer.name`,
    tbl_f_customer.CLADRESSE AS `customer.address1`,
    tbl_f_customer.CLADRESSE2 AS `customer.address2`,
    IF(tbl_p_customerlocality.VICLAUTO IS NULL,
        CONCAT_WS(" ", tbl_f_customer.CLCP, tbl_f_customer.CLVILLE),
        CONCAT_WS(" ", tbl_p_customerlocality.VICLCP, tbl_p_customerlocality.VICLNOM)) AS `customer.address3`,
    tbl_f_repair.CANUM AS `header.line1`,
    SUBSTRING(tbl_f_site.CHLIBELLE, 1, 25) AS `header.line3`,
    SUBSTRING(tbl_f_site.CHLIBELLE, 26, 25) AS `header.line4`,
    tbl_f_customer.CLCOLLECTIF AS `generalAccount`,
    tbl_f_repair.CAPXFINALFR AS `amount`,
    tbl_f_contract.AGAUTO AS `agency.id`
FROM CASSE tbl_f_repair
LEFT JOIN CONTRAT tbl_f_contract
ON tbl_f_repair.CONTRATAUTO = tbl_f_contract.CONTRATAUTO
LEFT JOIN CHANTIER tbl_f_site
ON tbl_f_site.CHAUTO = tbl_f_contract.CHAUTO
LEFT JOIN CLIENT tbl_f_customer
ON tbl_f_contract.CLAUTO = tbl_f_customer.CLAUTO
LEFT JOIN AUTH.VICL tbl_p_customerlocality
ON tbl_f_customer.VICLAUTO = tbl_p_customerlocality.VICLAUTO
WHERE tbl_f_repair.CASSEAUTO = ' . $id . ';';

        my $tabHeader = $db->fetchAssoc($query, {}, 'id');

        $tabResult = $tabHeader->{$id};

        # Récupère les données des lignes
        my $query = '
SELECT
    tbl_f_rentinvoiceline.CASSEAUTO AS `id`,
    "SAV" AS `article`,
    tbl_f_rentinvoiceline.LIBDIVERS AS `designation`,
    tbl_f_rentinvoiceline.DETAILSCASSEPXE AS `price`,
    tbl_f_rentinvoiceline.DETAILSCASSEQTE AS `quantity`
FROM DETAILSCASSE tbl_f_rentinvoiceline
WHERE tbl_f_rentinvoiceline.CASSEAUTO = ' . $id . ';';

        my $tabLines = $db->fetchAssoc($query, {}, 'id');

        my $tabVAT = &LOC::Country::getVATRatesList($countryId, $tabResult->{'date'});
        my $vatRate = $tabVAT->{'TN'}->{'value'};

        $tabResult->{'lines'} = [];
        foreach my $tabRow (values %$tabLines)
        {
            # Formatage des données
            $tabRow->{'id'}         *= 1;
            $tabRow->{'price'}      *= 1;
            $tabRow->{'quantity'}   *= 1;
            $tabRow->{'reduction'}   = 0;
            $tabRow->{'netPrice'}    = $tabRow->{'price'};
            $tabRow->{'amount'}      = $tabRow->{'netPrice'} * $tabRow->{'quantity'};
            $tabRow->{'vatCode'}     = 1;

            # Stockage du taux et du montant de base pour le calcul de la TVA au taux de la ligne courante
            $tabResult->{'vat'}->{$tabRow->{'vatCode'}}->{'rate'}    = $vatRate;
            $tabResult->{'vat'}->{$tabRow->{'vatCode'}}->{'base'}   += $tabRow->{'amount'};

            push(@{$tabResult->{'lines'}}, $tabRow);
        }

        # Calcul du montant de la TVA pour chaque taux et totaux
        foreach my $vatCode (keys(%{$tabResult->{'vat'}}))
        {
            $tabResult->{'vat'}->{$vatCode}->{'amount'} = $tabResult->{'vat'}->{$vatCode}->{'base'}
                                                          * $tabResult->{'vat'}->{$vatCode}->{'rate'};
            $tabResult->{'vatAmount'} += $tabResult->{'vat'}->{$vatCode}->{'amount'};
        }
        $tabResult->{'taxesIncludedAmount'} = $tabResult->{'amount'} + $tabResult->{'vatAmount'};
    }
    # Nouveau système de remise en état
    else
    {
        # Récupère les données d'en-tête
        my $query = '
SELECT
    rpi_id AS `id`,
    rpi_document AS `number`,
    rpi_date_export AS `invoicingDate`,
    rpi_date AS `date`,
    tbl_f_contract.CONTRATAUTO AS `contract.id`,
    rpi_reference AS `contract.code`,
    tbl_f_contract.CONTRATDD AS `contract.beginDate`,
    tbl_f_customer.CLAUTO AS `customer.id`,
    tbl_f_customer.CLCODE AS `customer.code`,
    tbl_f_customer.CLSTE AS `customer.name`,
    tbl_f_customer.CLADRESSE AS `customer.address1`,
    tbl_f_customer.CLADRESSE2 AS `customer.address2`,
    IF(tbl_p_customerlocality.VICLAUTO IS NULL,
        CONCAT_WS(" ", tbl_f_customer.CLCP, tbl_f_customer.CLVILLE),
        CONCAT_WS(" ", tbl_p_customerlocality.VICLCP, tbl_p_customerlocality.VICLNOM)) AS `customer.address3`,
    rpi_header1 AS `header.line1`,
    rpi_header3 AS `header.line3`,
    rpi_header4 AS `header.line4`,
    rpi_generalaccount AS `generalAccount`,
    rpi_total AS `amount`,
    rpi_vat AS `vatAmount`,
    rpi_taxesincluded AS `taxesIncludedAmount`,
    tbl_f_contract.AGAUTO AS `agency.id`
FROM f_repairinvoice
LEFT JOIN f_repairestimate
ON rpe_id = rpi_rpe_id
LEFT JOIN CONTRAT tbl_f_contract
ON (rpi_reference = tbl_f_contract.CONTRATCODE AND tbl_f_contract.CONTRATOK = -1)
LEFT JOIN CLIENT tbl_f_customer
ON rpi_thirdparty = tbl_f_customer.CLCODE
LEFT JOIN AUTH.VICL tbl_p_customerlocality
ON tbl_f_customer.VICLAUTO = tbl_p_customerlocality.VICLAUTO
WHERE rpi_id = ' . $id . ';';

        my $tabHeader = $db->fetchAssoc($query, {}, 'id');

        $tabResult = $tabHeader->{$id};

        # Liste des taux de TVA utilisés dans la facture
        my @tabVATRates = ();

        # Récupère les données des lignes
        my $query = '
SELECT
    ril_id AS `id`,
    ril_articleref AS `article`,
    ril_label AS `designation`,
    ril_unitprice AS `price`,
    ril_quantity AS `quantity`,
    ril_reduction AS `reduction`,
    ril_vatrate1 AS `vatRate`
FROM f_repairinvoiceline
WHERE ril_rpi_id = ' . $id . ';';

        my $tabLines = $db->fetchList(LOC::Util::GETLIST_ASSOC, $query, {}, 'id');

        $tabResult->{'lines'} = [];
        foreach my $tabRow (values %$tabLines)
        {
            # Formatage des données
            $tabRow->{'id'}         *= 1;
            $tabRow->{'price'}      *= 1;
            $tabRow->{'quantity'}   *= 1;
            $tabRow->{'reduction'}  /= 100;
            $tabRow->{'netPrice'}    = &LOC::Util::round($tabRow->{'price'} * (1 - $tabRow->{'reduction'}), 2);
            $tabRow->{'amount'}      = $tabRow->{'netPrice'} * $tabRow->{'quantity'};

            # Récupération du code TVA
            my ($vatCode) = grep { $tabVATRates[$_] == $tabRow->{'vatRate'} / 100 } 0..@tabVATRates-1;
            if ($vatCode eq '')
            {
                $vatCode = @tabVATRates;
                push(@tabVATRates, $tabRow->{'vatRate'} / 100);
            }
            $vatCode++;
            $tabRow->{'vatCode'} = $vatCode;

            # Stockage du taux et du montant de base pour le calcul de la TVA au taux de la ligne courante
            $tabResult->{'vat'}->{$vatCode}->{'rate'}    = $tabRow->{'vatRate'} / 100;
            $tabResult->{'vat'}->{$vatCode}->{'base'}   += $tabRow->{'amount'};

            push(@{$tabResult->{'lines'}}, $tabRow);
        }

        # Calcul du montant de la TVA pour chaque taux
        foreach my $vatCode (keys(%{$tabResult->{'vat'}}))
        {
            $tabResult->{'vat'}->{$vatCode}->{'amount'} = $tabResult->{'vat'}->{$vatCode}->{'base'}
                                                          * $tabResult->{'vat'}->{$vatCode}->{'rate'};
        }
    }

    # Formatage des données
    $tabResult->{'customer.id'}         *= 1;
    $tabResult->{'amount'}              *= 1;
    $tabResult->{'vatAmount'}           *= 1;
    $tabResult->{'taxesIncludedAmount'} *= 1;

    return (wantarray ? %$tabResult : $tabResult);
}

# Function: getList
# Retourne la liste des factures de location
#
# Contenu du hashage :
# - id : identifiant de la facture ;
# - repairEstimate.id : identifiant du devis de remise en état ;
# - invoicingDate : date de génération de la facture ;
# - date : date de la facture ;
# - contract.id : identifiant du contrat ;
# - contract.code : code du contrat ;
# - customer.id : identifiant du client ;
# - customer.code : code du client ;
# - customer.name : raison sociale du client ;
# - amount : montant total de la facture.
#
# Parameters:
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres ('id' : Recherche par l'identifiant,
#                                'contractCode' : recherche par code de contrat,
#                                'invoicingId' : identifiant de la facturation,
#                                'invoicingDate' : date de facturation,
#                                'numbered' : facture non numérotées)
#
# Returns:
# hash|hashref|0 - Liste des factures (mêmes clés que pour la méthode getInfos)
sub getList
{
    my ($countryId, $format, $tabFilters) = @_;

    if ($countryId eq '')
    {
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Filtres
    my $whereQuery1 = ''; # Filtre table CASSE
    my $whereQuery2 = ''; # Filtre table f_repairinvoice
    # - Identifiant de la facture
    if ($tabFilters->{'id'} ne '')
    {
        $whereQuery1 .= '
AND tbl_f_repair.CASSEAUTO = ' . $db->quote($tabFilters->{'id'});
        $whereQuery2 .= '
AND rpi_id = ' . $db->quote($tabFilters->{'id'});
    }
    # - Code du contrat
    if ($tabFilters->{'contractCode'} ne '')
    {
        $whereQuery1 .= '
AND tbl_f_contract.CONTRATCODE IN (' . $db->quote($tabFilters->{'contractCode'}) . ')';
        $whereQuery2 .= '
AND tbl_f_contract.CONTRATCODE IN (' . $db->quote($tabFilters->{'contractCode'}) . ')';
    }
    # - client
    if ($tabFilters->{'customerId'} ne '')
    {
        $whereQuery1 .= '
AND tbl_f_contract.CLAUTO IN (' . $db->quote($tabFilters->{'customerId'}) . ')';
        $whereQuery2 .= '
AND rpi_cus_id IN (' . $db->quote($tabFilters->{'customerId'}) . ')';
    }
    # - Identifiant de la facturation
    if ($tabFilters->{'invoicingId'} ne '')
    {
        $whereQuery1 .= '
AND FALSE';
        $whereQuery2 .= '
AND rpi_invoicingid = ' . $db->quote($tabFilters->{'invoicingId'});
    }
    # - Date de facturation
    if ($tabFilters->{'invoicingDate'} ne '')
    {
        $whereQuery1 .= '
AND tbl_f_repair.CADATEFACT = ' . $db->quote($tabFilters->{'invoicingDate'});
        $whereQuery2 .= '
AND rpi_date_invoicing = ' . $db->quote($tabFilters->{'invoicingDate'});
    }
    # - Facture numérotée
    if ($tabFilters->{'numbered'} ne '')
    {
        if ($tabFilters->{'numbered'})
        {
            $whereQuery2 .= '
AND rpi_document IS NOT NULL';
        }
        else
        {
            $whereQuery2 .= '
AND rpi_document IS NULL';
        }
    }

    # Récupère les données concernant la facture
    my $query = '';
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
(SELECT
    tbl_f_repair.CASSEAUTO AS `id`,
    NULL AS `repairEstimate.id`,
    tbl_f_repair.CASSEAUTO AS `number`,
    tbl_f_repair.CADATEFACT AS `invoicingDate`,
    DATE_SUB(CONCAT(YEAR(tbl_f_repair.CADATEFACT), "-", MONTH(tbl_f_repair.CADATEFACT), "-01"), INTERVAL 1 DAY) AS `date`,
    tbl_f_contract.CONTRATAUTO AS `contract.id`,
    tbl_f_contract.CONTRATCODE AS `contract.code`,
    tbl_f_contract.CLAUTO AS `customer.id`,
    tbl_f_customer.CLCODE AS `customer.code`,
    tbl_f_customer.CLSTE AS `customer.name`,
    tbl_f_repair.CAPXFINALFR AS `amount`,
    "' . LOC::Invoice::TYPE_OLDREPAIR . '" AS `type`
FROM CASSE tbl_f_repair
LEFT JOIN CONTRAT tbl_f_contract
ON tbl_f_repair.CONTRATAUTO = tbl_f_contract.CONTRATAUTO
LEFT JOIN CLIENT tbl_f_customer
ON tbl_f_contract.CLAUTO = tbl_f_customer.CLAUTO
WHERE tbl_f_repair.ETCODE = "CAS05"' .
$whereQuery1 . '
GROUP BY tbl_f_repair.CASSEAUTO)
UNION
(SELECT
    rpi_id AS `id`,
    rpi_rpe_id AS `repairEstimate.id`,
    rpi_document AS `number`,
    rpi_date_invoicing AS `invoicingDate`,
    rpi_date AS `date`,
    tbl_f_contract.CONTRATAUTO AS `contract.id`,
    tbl_f_contract.CONTRATCODE AS `contract.code`,
    tbl_f_contract.CLAUTO AS `customer.id`,
    tbl_f_customer.CLCODE AS `customer.code`,
    tbl_f_customer.CLSTE AS `customer.name`,
    rpi_total AS `amount`,
    "' . LOC::Invoice::TYPE_REPAIR . '" AS `type`
FROM f_repairinvoice
LEFT JOIN f_repairestimate
ON rpi_rpe_id = rpe_id
LEFT JOIN f_repairsheet
ON rpe_rps_id = rps_id
LEFT JOIN CONTRAT tbl_f_contract
ON rps_rct_id = tbl_f_contract.CONTRATAUTO
LEFT JOIN CLIENT tbl_f_customer
ON tbl_f_contract.CLAUTO = tbl_f_customer.CLAUTO
WHERE 1' .
$whereQuery2 . '
GROUP BY rpi_id)
ORDER BY `invoicingDate`;';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
(SELECT
    COUNT(*) AS `nbInvoices`
FROM CASSE tbl_f_repair
WHERE 1' .
$whereQuery1 . ')
UNION
(SELECT
    COUNT(*) AS `nbInvoices`
FROM f_repairinvoice
WHERE 1' .
$whereQuery2 . ');';
    }
    else
    {
        $query = '
(SELECT
    tbl_f_rentinvoice.CASSEAUTO AS `id`,
    NULL AS `number`,
FROM CASSE tbl_f_repair
WHERE 1' .
$whereQuery1 . ')
UNION
(SELECT
    rpi_id AS `id`,
    rpi_document AS `number`,
FROM f_repairinvoice
WHERE 1' .
$whereQuery2 . ')
ORDER BY `number`;';
    }

    my $tabResult = $db->fetchList($format, $query, {}, 'id');

    if ($format eq LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tabResult)
        {
            # Formatage des données
            $tabRow->{'customer.id'} *= 1;
            $tabRow->{'amount'}      *= 1;
        }
    }

    return (wantarray ? %$tabResult : $tabResult);
}

# Function: getNextInvoicingDate
# Retourne la date de prochaine facturation de remise en état
#
# Parameters:
# string  $countryId - Identifiant du pays. Retourne undef si le pays n'est pas précisé
#
# Returns:
# undef|hashref - Tableau contenant la date de la prochaine facturation de location ('date') et son identifiant ('id'),
#                 ou undef si aucune date n'est trouvée
sub getNextInvoicingDate
{
    my ($countryId) = @_;

    unless (defined $countryId)
    {
        return undef;
    }

    # Récupération de la dernière date de facturation de remise en état
    my $lastInvoicing = &getLastInvoicingDate($countryId);

    # Calcul de la date de prochaine facturation
    my $tabNextInvoicingDate = {'type' => LOC::Invoice::TYPE_REPAIR};
    my @tabLocaltime = localtime();
    $tabLocaltime[5] += 1900;
    $tabLocaltime[4]++;
    my $today = sprintf('%04d-%02d-%02d', $tabLocaltime[5], $tabLocaltime[4], $tabLocaltime[3]);

    if ($lastInvoicing->{'date'} ge $today)
    {
        my @tabInvoiceDate = split(/\-/, $lastInvoicing->{'date'});
        $tabInvoiceDate[0] = &LOC::Util::toNumber($tabInvoiceDate[0]);
        $tabInvoiceDate[1] = &LOC::Util::toNumber($tabInvoiceDate[1]);
        $tabInvoiceDate[2] = &LOC::Util::toNumber($tabInvoiceDate[2]);

        # Calcul de la date du jour suivant
        my ($year, $month ,$day) = &Date::Calc::Add_Delta_Days($tabInvoiceDate[0],
                                                               $tabInvoiceDate[1],
                                                               $tabInvoiceDate[2],
                                                               1);
        $tabNextInvoicingDate->{'date'} = sprintf('%04d-%02d-%02d', $year, $month ,$day);
        $tabNextInvoicingDate->{'readyToInvoice'} = 0;
    }
    else
    {
        $tabNextInvoicingDate->{'date'}           = $today;
        $tabNextInvoicingDate->{'readyToInvoice'} = 1;
    }

    # Identifiant de la prochaine facturation
    $tabNextInvoicingDate->{'id'}   = &LOC::Invoice::getId($countryId, $tabNextInvoicingDate->{'date'},
                                                           LOC::Invoice::TYPE_REPAIR);

    return $tabNextInvoicingDate;
}

# Function: getLastInvoicingDate
# Retourne la date de dernière facturation de remise en état
#
# Parameters:
# string  $countryId - Identifiant du pays. Retourne undef si le pays n'est pas précisé
#
# Returns:
# undef|hashref - Tableau contenant la date de la dernière facturation de location ('date') et son identifiant ('id'),
#                 ou undef si aucune date n'est trouvée
sub getLastInvoicingDate
{
    my ($countryId) = @_;

    unless (defined $countryId)
    {
        return undef;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Récupération de la prochaine date de facturation de location paramétrée en base
    my $query = '
SELECT
    f_repairinvoice.rpi_date_invoicing AS `date`
FROM f_repairinvoice
ORDER BY `date` DESC
LIMIT 1;';

    my $tabPreviousInvoicingDate = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    $tabPreviousInvoicingDate->{'id'} = &LOC::Invoice::getId($countryId, $tabPreviousInvoicingDate->{'date'},
                                                             LOC::Invoice::TYPE_REPAIR);

    return $tabPreviousInvoicingDate;
}

# Function: getLastest
# Retourne la liste des dernières facturations de remise en état
#
# Contenu du hashage :
# - date : date de la facturation ;
# - number : nombre de factures générées ;
# - amount : montant total HT des factures générées
# - rentType : type de facturation de location (INVOICETYPE_MONTH ou INVOICETYPE_INTERMEDIATE)
# - isExportable : le fichier d'export vers Sage peut être regénéré ;
# - isCancellable : la facturation peut être annulée ;
# - details : détail par agence de la facturation ;
#   * number : nombre de factures générées pour l'agence ;
#   * amount : montant total HT des factures générées pour l'agence ;
# - history : historique des opérations effectuées (si GETLASTEST_LOGS fait partie des flags) :
#   * id : identifiant de la ligne d'historique ;
#   * eventType.code : code du type d'événement ;
#   * eventType.label : libellé du type d'événement ;
#   * user.id : identifiant de l'utilisateur ;
#   * user.name : nom de l'utilisateur ;
#   * user.firstName : prénom de l'utilisateur ;
#   * user.fullName : nom complet de l'utilisateur ;
#   * user.state.id : identifiant de l'état de l'utilisateur ;
#   * invoicing.id : identifiant de la facturation ;
#   * datetime : date et heure de la trace ;
#   * content : contenu de la trace.
# - assets : liste des avoirs :
#   * id : identifiant de la facture négative ;
#   * contract.id : identifiant du contrat ;
#   * contract.code : code du contrat ;
#   * customer.id : identifiant du client ;
#   * customer.code : code du client ;
#   * amount : montant.
#
# Parameters:
# string  $countryId - Identifiant du pays
# string  $localeId  - Identifiant de la locale
# string  $date      - Date jusqu'à laquelle rechercher les facturations (6 derniers mois par défaut)
# string  $flags     - Flags de récupération des données
#
# Returns:
# hash|hashref|0 - Liste des facturations
sub getLastest
{
    my ($countryId, $localeId, $date, $flags) = @_;

    if ($countryId eq '')
    {
        return 0;
    }
    unless (defined $flags)
    {
        $flags = LOC::Invoice::GETLASTEST_BASE;
    }

    if ($date eq '')
    {
        # Calcul de la date du jour moins 6 mois
        my @tabLocaltime = localtime();
        my ($year, $month, $day) = &Date::Calc::Add_Delta_YM($tabLocaltime[5] + 1900,
                                                             $tabLocaltime[4] + 1,
                                                             $tabLocaltime[3],
                                                             0,
                                                             -6);
        $date = sprintf('%04d-%02d-01', $year, $month);
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $tabLastInvoicingDate = &getLastInvoicingDate($countryId);

    # Montants totaux par facturation
    my $query = '
SELECT
    rpi_date_invoicing AS `date`,
    COUNT(*) AS `number`,
    SUM(IF(rpi_document IS NULL, 1, 0)) AS `unexportable`,
    SUM(rpi_total) AS `amount`,
    ' . $db->quote(LOC::Invoice::TYPE_REPAIR) . ' AS `type`,
    NULL AS `rentType`,
    1 AS `isExportable`
FROM f_repairinvoice
WHERE rpi_date_invoicing >= ' . $db->quote($date) . '
GROUP BY DATE(rpi_date_invoicing)
ORDER BY `date` DESC;';

    my $tabResult = $db->fetchAssoc($query, {}, 'date');

    # Montants par facturation et par agence
    my $query = '
SELECT
    CONCAT(rpi_date_invoicing, rpi_agc_id) AS `id`,
    rpi_date_invoicing AS `date`,
    rpi_agc_id AS `agency.id`,
    COUNT(*) AS `number`,
    SUM(rpi_total) AS `amount`
FROM f_repairinvoice
WHERE rpi_date_invoicing >= ' . $db->quote($date) . '
GROUP BY rpi_date_invoicing, f_repairinvoice.rpi_agc_id
ORDER BY rpi_date_invoicing DESC, f_repairinvoice.rpi_agc_id;';

    my $tabAgencyResult = $db->fetchAssoc($query, {}, 'id');
    foreach my $tabLine (values(%$tabAgencyResult))
    {
        $tabResult->{$tabLine->{'date'}}->{'id'} = &LOC::Invoice::getId($countryId, $tabLine->{'date'},
                                                                        $tabResult->{$tabLine->{'date'}}->{'type'});

        my $today = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE);
        $tabResult->{$tabLine->{'date'}}->{'isCancellable'} = ($tabLine->{'date'} eq $today);

        my $exportableMinAmount = &LOC::Characteristic::getCountryValueByCode('MTMININV', $countryId, $tabLine->{'date'});
        $tabResult->{$tabLine->{'date'}}->{'exportableMinAmount'} = $exportableMinAmount;

        $tabResult->{$tabLine->{'date'}}->{'details'}->{$tabLine->{'agency.id'}}->{'number'} = $tabLine->{'number'};
        $tabResult->{$tabLine->{'date'}}->{'details'}->{$tabLine->{'agency.id'}}->{'amount'} = $tabLine->{'amount'};
    }

    foreach my $date (keys(%$tabResult))
    {
        # Historique
        if ($flags & LOC::Invoice::GETLASTEST_LOGS)
        {
            my $tabHistory = &LOC::Invoice::getHistory($countryId, $date, LOC::Invoice::TYPE_REPAIR, $localeId);

            my $nbHistory = keys(%$tabHistory);
            if ($nbHistory > 0)
            {
                my @tabDates = keys(%$tabResult);
                if (!LOC::Util::in_array($date, \@tabDates))
                {
                    $tabResult->{$date} = {'date'          => $date,
                                           'number'        => 0,
                                           'unexportable'  => 0,
                                           'amount'        => 0,
                                           'type'          => $tabResult->{$date}->{'type'},
                                           'isExportable'  => 0,
                                           'isCancellable' => 0};
                }
                $tabResult->{$date}->{'history'} = $tabHistory;
            }
        }
    }

    # Historique de la facturation suivante (pré-contrôles)
    if ($flags & LOC::Invoice::GETLASTEST_LOGS)
    {
        my $tabNextInvoicing = &getNextInvoicingDate($countryId);

        # Traces
        my $tabHistory = &LOC::Invoice::getHistory($countryId, $tabNextInvoicing->{'date'}, LOC::Invoice::TYPE_REPAIR, $localeId);

        my $nbHistory = keys(%$tabHistory);
        if ($nbHistory > 0)
        {
            my @tabDates = keys(%$tabResult);
            if (!LOC::Util::in_array($tabNextInvoicing->{'date'}, \@tabDates))
            {
                $tabResult->{$tabNextInvoicing->{'date'}} = {'date'          => $tabNextInvoicing->{'date'},
                                                             'number'        => 0,
                                                             'unexportable'  => 0,
                                                             'amount'        => 0,
                                                             'type'          => $tabNextInvoicing->{'type'},
                                                             'isExportable'  => 0,
                                                             'isCancellable' => 0};
            }
            $tabResult->{$tabNextInvoicing->{'date'}}->{'history'} = $tabHistory;
        }
    }

    return (wantarray ? %$tabResult : $tabResult);
}

# Function: invoice
# Lance la facturation de remise en état
#
# Parameters:
# string  $countryId     - Identifiant du pays. Retourne undef si le pays n'est pas précisé
# string  $date          - Date de la facturation
# string  $repairNumber  - Prochain numéro de pièce
# int     $requestId     - Identifiant de la requête de facturation
#
# Returns:
# int - Nombre de factures concernées ou -1 en cas d'erreur
sub invoice
{
    my ($countryId, $date, $repairNumber, $requestId) = @_;

    # Vérification des paramètres passés
    unless ($countryId ne '' && $date ne '' && $repairNumber ne '')
    {
        return -1;
    }

    # A-t-on le droit de lancer la facturation
    my $tabNextInvoicing = &getNextInvoicingDate($countryId);
    if ($tabNextInvoicing->{'date'} ne $date || !$tabNextInvoicing->{'readyToInvoice'})
    {
        return -1;
    }

    # Identifiant de la facturation
    my $invoicingId = &LOC::Invoice::getId($countryId, $date, LOC::Invoice::TYPE_REPAIR);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Tableau de résultats
    my $tabInvoicingResult = {'readyToInvoice' => 1, 'nbGeneratedDocuments' => 0};

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Récupération du préfixe des numéros de pièce
    my $docNoPrefix = &LOC::Invoice::getDocumentNoPrefix($countryId);

    # Récupération des factures qui n'ont pas encore été traitées
    my $query = '
SELECT
    rpi_id AS `id`,
    rpi_rpe_id AS `repairEstimate.id`,
    rpi_total AS `amount`
FROM f_repairinvoice
WHERE rpi_date_invoicing IS NULL
AND rpi_sta_id = ' . $db->quote(LOC::Invoice::STATE_NOTEXPORTED) . ';';

    my $tabResult = $db->fetchAssoc($query, {}, 'id');
    my $nbInvoices = 0;

    # Mise à jour du nombre total de factures
    if ($requestId)
    {
        my $nbTotalInvoices = keys(%$tabResult);
        &LOC::InvoicingRequest::setTotalInvoicesNo($countryId, $requestId, $nbTotalInvoices);
    }

    foreach my $invoiceId (keys(%$tabResult))
    {
        # Génération de la date du document
        my $documentDate = &generateDocumentDate($date);

        # Génération du numéro de document
        my $isDocumentExportable = &LOC::Invoice::isDocumentExportable($countryId, $tabResult->{$invoiceId}->{'amount'});
        my $documentNo;
        if ($isDocumentExportable)
        {
            $documentNo = &LOC::Invoice::generateDocumentNo($countryId, LOC::Invoice::TYPE_REPAIR, '', $date,
                                                            $repairNumber, $docNoPrefix);
        }

        # Champs à mettre à jour sur la facture
        my $tabUpdates = {'rpi_document' => $documentNo, 'rpi_date' => $documentDate,
                          'rpi_invoicingid' => $invoicingId,
                          'rpi_date_invoicing*' => 'CURRENT_DATE()'};

        # Mise à jour des factures
        my $update = $db->update('f_repairinvoice', $tabUpdates, {'rpi_id' => $invoiceId}, $schemaName);

        if ($update == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;
        }

        # Passage du devis à l'état "facturé"
        my $estimateId = $tabResult->{$invoiceId}->{'repairEstimate.id'};
        if (&LOC::Repair::Estimate::setState($countryId, $estimateId, LOC::Repair::Estimate::STATE_INVOICED) == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;
        }
        $nbInvoices++;

        # Incrémentation du numéro
        $repairNumber++;

        # Mise à jour de la progression de la facturation
        if ($requestId)
        {
            &LOC::InvoicingRequest::setTreatedInvoicesNo($countryId, $requestId, $nbInvoices);
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }

    $tabInvoicingResult->{'nbGeneratedDocuments'} = $nbInvoices;
    return $tabInvoicingResult;
}

# Function: generateDocumentDate
# Génère la date de facture en fonction de la date de facturation
#
# Parameters:
# string  $date - Date de la facturation
#
# Returns:
# string - Date de la facture (au format SQL)
sub generateDocumentDate
{
    my ($date) = @_;

    unless (defined $date)
    {
        return 0;
    }

    # Calcul de la date du jour du mois précédent
    my @tabDate = split(/\-/, $date);
    my ($year, $month, $day) = &Date::Calc::Add_Delta_YM(&LOC::Util::toNumber($tabDate[0]),
                                                         &LOC::Util::toNumber($tabDate[1]),
                                                         &LOC::Util::toNumber($tabDate[2]),
                                                         0,    # -0 année
                                                         -1);  # -1 mois
    # Nombre de jours du mois précédent
    $day = &Date::Calc::Days_in_Month($year, $month);

    return sprintf('%04d-%02d-%02d', $year, $month, $day);
}

# Function: getExportFileContent
# Retourne le contenu du fichier d'export vers Sage
#
# Parameters:
# string $countryId - Identifiant du pays
# string $date      - Date de facturation
#
# Returns:
# string - Contenu du fichier d'export vers Sage
sub getExportFileContent
{
    my ($countryId, $date) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my @tabContent;   # Tableau stockant le contenu du fichier d'export

    # Variables nécessaires à la génération du contenu
    my $documentType = &LOC::Characteristic::getCountryValueByCode('TYPEDOCSAGE', $countryId);   # BL ou facture
    my $depot = &LOC::Characteristic::getCountryValueByCode('DEPOTSTOCK', $countryId);   # Dépôt de stockage

    # Récupération des formats Sage (quantité, prix et montant)
    my $charac = &LOC::Characteristic::getCountryValueByCode('FORMATSSAGE', $countryId);
    my $tabSageFormats = {'quantity' => 2, 'price' => 4, 'amount' => 2};
    if ($charac ne '')
    {
        $tabSageFormats = &LOC::Json::fromJson($charac);
    }

    # Début du fichier d'export
    my $version = '#VER ' . LOC::Invoice::EXPORTFILE_VERSION;
    push(@tabContent, $version);   # Version du fichier

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Récupération des informations d'en-tête des factures non encore exportées
    my $query = '
SELECT
    rpi_id AS `id`,
    rpi_document AS `document`,
    DATE_FORMAT(rpi_date, "%d%m%y") AS `date`,
    rpi_reference AS `reference`,
    rpi_thirdparty AS `thirdParty`,
    rpi_payer AS `payer`,
    rpi_header1 AS `header1`,
    rpi_header2 AS `header2`,
    rpi_header3 AS `header3`,
    rpi_header4 AS `header4`,
    rpi_affair AS `affair`,
    rpi_generalaccount AS `generalAccount`,
    tbl_f_customer.CLNBFACT AS `customer.invoicesNo`,
    tbl_f_customer.CLFACTGRP AS `customer.CLFACTGRP`,
    rpi_accountingcategory AS `accountingCategory`
FROM f_repairinvoice
LEFT JOIN CLIENT tbl_f_customer
ON rpi_cus_id = tbl_f_customer.CLAUTO
WHERE rpi_date_invoicing = ' . $db->quote($date) . '
AND rpi_document IS NOT NULL;';

    my $tabHeaderResult = $db->fetchAssoc($query, {}, 'id');
    my @tabInvoicesToUpdate = ();

    foreach my $tabInvoice (values(%$tabHeaderResult))
    {
        my $groupedInvoices = ($tabInvoice->{'customer.CLFACTGRP'} eq '1' ? '1' : '0');

        push(@tabContent, '#CHEN');   # Drapeau d'en-tête de document
        push(@tabContent, '1');   # Ligne 1 : domaine
        push(@tabContent, $documentType);   # Ligne 2 : type
        push(@tabContent, '1');   # Ligne 3 : provenance
        push(@tabContent, '1');   # Ligne 4 : souche
        push(@tabContent, $tabInvoice->{'document'});   # Ligne 5 : n° de pièce
        push(@tabContent, $tabInvoice->{'date'});   # Ligne 6 : date
        push(@tabContent, $tabInvoice->{'reference'});   # Ligne 7 : référence
        push(@tabContent, '');   # Ligne 8 : livraison
        push(@tabContent, $tabInvoice->{'thirdParty'});   # Ligne 9 : tiers
        push(@tabContent, $depot);   # Ligne 10 : dépôt de stockage
        push(@tabContent, '');   # Ligne 11 : dépôt de livraison
        push(@tabContent, '1');   # Ligne 12 : périodicité
        push(@tabContent, '0');   # Ligne 13 : devise
        push(@tabContent, '0,000000');   # Ligne 14 : cours
        push(@tabContent, $tabInvoice->{'payer'});   # Ligne 15 : payeur/encaisseur
        push(@tabContent, '1');   # Ligne 16 : expédition
        push(@tabContent, '1');   # Ligne 17 : condition livraison
        push(@tabContent, '0');   # Ligne 18 : langue
        push(@tabContent, '');   # Ligne 19 : nom représentant
        push(@tabContent, '');   # Ligne 20 : prénom représentant
        push(@tabContent, $tabInvoice->{'header1'});   # Ligne 21 : en-tête 1
        push(@tabContent, $tabInvoice->{'header2'});   # Ligne 22 : en-tête 2
        push(@tabContent, $tabInvoice->{'header3'});   # Ligne 23 : en-tête 3
        push(@tabContent, $tabInvoice->{'header4'});   # Ligne 24 : en-tête 4
        push(@tabContent, $tabInvoice->{'affair'});   # Ligne 25 : affaire
        push(@tabContent, '1');   # Ligne 26 : catégorie tarifaire
        push(@tabContent, '21');   # Ligne 27 : régime
        push(@tabContent, '11');   # Ligne 28 : transaction
        push(@tabContent, &LOC::Invoice::getSageFormat(1, $tabSageFormats->{'quantity'}));   # Ligne 29 : colisage (format quantité)
        push(@tabContent, '1');   # Ligne 30 : unité colisage
        push(@tabContent, $tabInvoice->{'customer.invoicesNo'});   # Ligne 31 : nombre exemplaires
        push(@tabContent, $groupedInvoices);   # Ligne 32 : 1 BL/Facture
        push(@tabContent, '0,0000');   # Ligne 33 : taux d'escompte
        push(@tabContent, '0,0000');   # Ligne 34 : écart valorisation
        push(@tabContent, $tabInvoice->{'accountingCategory'});   # Ligne 35 : catégorie comptable
        push(@tabContent, '0');   # Ligne 36 : frais
        push(@tabContent, '2');   # Ligne 37 : statut
        push(@tabContent, $tabInvoice->{'generalAccount'});   # Ligne 38 : compte général
        push(@tabContent, '00:00:00');   # Ligne 39 : heure
        push(@tabContent, '');   # Ligne 40 : caisse
        push(@tabContent, '');   # Ligne 41 : caissier nom
        push(@tabContent, '');   # Ligne 42 : caissier prénom
        push(@tabContent, '0');   # Ligne 43 : clôturé
        push(@tabContent, '');   # Ligne 44 : numéro de commande site marchand
        push(@tabContent, '');   # Ligne 45 : ventilation IFRS
        push(@tabContent, '0');   # Ligne 46 : type valeur calcul frais d'expédition
        push(@tabContent, '0,00');   # Ligne 47 : valeur frais d'expédition
        push(@tabContent, '0');   # Ligne 48 : type valeur frais d'expédition
        push(@tabContent, '0');   # Ligne 49 : type valeur calcul franco de port
        push(@tabContent, '0,00');   # Ligne 50 : valeur franco de port
        push(@tabContent, '0');   # Ligne 51 : type valeur franco de port
        push(@tabContent, '0,00');   # Ligne 52 : taux taxe 1
        push(@tabContent, '0');   # Ligne 53 : type taux taxe 1
        push(@tabContent, '0');   # Ligne 54 : type de taxe 1
        push(@tabContent, '0,00');   # Ligne 55 : taux taxe 2
        push(@tabContent, '0');   # Ligne 56 : type taux taxe 2
        push(@tabContent, '0');   # Ligne 57 : type de taxe 2
        push(@tabContent, '0,00');   # Ligne 58 : taux taxe 3
        push(@tabContent, '0');   # Ligne 59 : type taux taxe 3
        push(@tabContent, '0');   # Ligne 60 : type de taxe 3
        push(@tabContent, '');   # Ligne 61 : motif
        push(@tabContent, '');   # Ligne 62 : centrale d'achat
        push(@tabContent, '');   # Ligne 63 : contact

        # Récupération des informations des lignes
        my $query = '
SELECT
    ril_id AS `id`,
    rpi_reference AS `header.reference`,
    ril_articleref AS `articleReference`,
    ril_label AS `label`,
    ril_labelExtra AS `labelExtra`,
    ril_unitprice AS `unitPrice`,
    ril_quantity AS `quantity`,
    ril_reduction AS `reduction`,
    ril_affair AS `affair`,
    ril_vatrate1 AS `vatRate1`,
    ril_vatratetype1 AS `vatRateType1`,
    ril_vatrate2 AS `vatRate2`,
    ril_vatratetype2 AS `vatRateType2`,
    ril_vatrate3 AS `vatRate3`,
    ril_vatratetype3 AS `vatRateType3`,
    ril_thirdpartynumber AS `thirdPartyNumber`,
    ril_freeinfo2 AS `freeInfo2`,
    ril_freeinfo3 AS `freeInfo3`,
    ril_freeinfo4 AS `freeInfo4`,
    ril_freeinfo5 AS `freeInfo5`,
    ril_freeinfo8 AS `freeInfo8`
FROM f_repairinvoiceline
LEFT JOIN f_repairinvoice
ON ril_rpi_id = rpi_id
WHERE ril_rpi_id = ' . $db->quote($tabInvoice->{'id'}) . ';';

        my $tabLineResult = $db->fetchAssoc($query, {}, 'id');

        foreach my $tabLine (values(%$tabLineResult))
        {
            push(@tabContent, '#CHLI');   # Drapeau d'en-tête de ligne
            push(@tabContent, $tabLine->{'header.reference'});   # Ligne 1 : référence ligne
            push(@tabContent, $tabLine->{'articleReference'});   # Ligne 2 : référence article
            push(@tabContent, $tabLine->{'label'});   # Ligne 3 : désignation
            push(@tabContent, $tabLine->{'labelExtra'});   # Ligne 4 : texte complémentaire
            push(@tabContent, '');   # Ligne 5 : énuméré de gamme 1
            push(@tabContent, '');   # Ligne 6 : énuméré de gamme 2
            push(@tabContent, '');   # Ligne 7 : n° de série & lot
            push(@tabContent, '');   # Ligne 8 : complément série/lot
            push(@tabContent, '');   # Ligne 9 : date péremption
            push(@tabContent, '');   # Ligne 10 : date fabrication
            push(@tabContent, '0');   # Ligne 11 : type de prix
            push(@tabContent, &LOC::Invoice::getSageFormat($tabLine->{'unitPrice'}, $tabSageFormats->{'price'}));   # Ligne 12 : prix unitaire (format montant)
            push(@tabContent, &LOC::Invoice::getSageFormat(0, $tabSageFormats->{'price'}));   # Ligne 13 : prix unitaire en devise (format montant)
            push(@tabContent, &LOC::Invoice::getSageFormat($tabLine->{'quantity'}, $tabSageFormats->{'quantity'}));   # Ligne 14 : quantité (format quantité)
            push(@tabContent, &LOC::Invoice::getSageFormat(1, $tabSageFormats->{'quantity'}));   # Ligne 15 : quantité colisée (format quantité)
            push(@tabContent, '');   # Ligne 16 : conditionnement
            push(@tabContent, &LOC::Invoice::getSageFormat(0, $tabSageFormats->{'quantity'}));   # Ligne 17 : poids net global (format quantité)
            push(@tabContent, &LOC::Invoice::getSageFormat(0, $tabSageFormats->{'quantity'}));   # Ligne 18 : poids brut global (format quantité)
            push(@tabContent, &LOC::Invoice::getSageFormat(sprintf('%.2f%%', $tabLine->{'reduction'}), 2));   # Ligne 19 : remise
            push(@tabContent, '0');   # Ligne 20 : type de ligne
            push(@tabContent, '0,00');   # Ligne 21 : prix de revient unitaire
            push(@tabContent, &LOC::Invoice::getSageFormat(0, $tabSageFormats->{'amount'}));   # Ligne 22 : frais (format montant)
            push(@tabContent, &LOC::Invoice::getSageFormat(0, $tabSageFormats->{'price'}));   # Ligne 23 : CMUP (format prix)
            push(@tabContent, '0');   # Ligne 24 : provenance facture
            push(@tabContent, '');   # Ligne 25 : nom représentant
            push(@tabContent, '');   # Ligne 26 : prénom représentant
            push(@tabContent, '');   # Ligne 27 : date livraison
            push(@tabContent, $depot);   # Ligne 28 : dépôt de stockage
            push(@tabContent, $tabLine->{'affair'});   # Ligne 29 : affaire
            push(@tabContent, '1');   # Ligne 30 : valorisation
            push(@tabContent, '');   # Ligne 31 : référence composé
            push(@tabContent, '0');   # Ligne 32 : article non livré
            push(@tabContent, &LOC::Invoice::getSageFormat($tabLine->{'vatRate1'}, 2));   # Ligne 33 : taux taxe 1
            push(@tabContent, $tabLine->{'vatRateType1'});   # Ligne 34 : type taux taxe 1
            push(@tabContent, '0');   # Ligne 35 : type de taxe 1
            push(@tabContent, &LOC::Invoice::getSageFormat($tabLine->{'vatRate2'}, 2));   # Ligne 36 : taux taxe 2
            push(@tabContent, $tabLine->{'vatRateType2'});   # Ligne 37 : type taux taxe 2
            push(@tabContent, '0');   # Ligne 38 : type de taxe 2
            push(@tabContent, &LOC::Invoice::getSageFormat($tabLine->{'vatRate3'}, 2));   # Ligne 39 : taux taxe 3
            push(@tabContent, $tabLine->{'vatRateType3'});   # Ligne 40 : type taux taxe 3
            push(@tabContent, '0');   # Ligne 41 : type de taxe 3
            push(@tabContent, $tabLine->{'thirdPartyNumber'});   # Ligne 42 : numéro tiers
            push(@tabContent, '');   # Ligne 43 : référence fournisseur
            push(@tabContent, '');   # Ligne 44 : référence client
            push(@tabContent, '0');   # Ligne 45 : facturation sur le poids net
            push(@tabContent, '0');   # Ligne 46 : hors escompte
            push(@tabContent, '');   # Ligne 47 : numéro de colis
            push(@tabContent, '');   # Ligne 48 : code ressource
            push(@tabContent, '0');   # Ligne 49 : quantité ressource
            push(@tabContent, '0');   # Ligne 50 : existence agenda
            push(@tabContent, '000000');   # Ligne 51 : date avancement
            push(@tabContent, '1');   # Ligne 52 : code emplacement
            push(@tabContent, &LOC::Invoice::getSageFormat(1, $tabSageFormats->{'quantity'}));   # Ligne 53 : quantité emplacement (format quantité)

            push(@tabContent, '#CIVA');   # Drapeau d'en-tête des informations libres
            push(@tabContent, '');   # Ligne 1
            push(@tabContent, $tabLine->{'freeInfo2'});   # Ligne 2 : n° de parc
            push(@tabContent, $tabLine->{'freeInfo3'});   # Ligne 3 : modèle de machines
            push(@tabContent, $tabLine->{'freeInfo4'});   # Ligne 4 : n° de série
            push(@tabContent, $tabLine->{'freeInfo5'});   # Ligne 5 : agence
            push(@tabContent, '');   # Ligne 6
            push(@tabContent, '');   # Ligne 7
            push(@tabContent, $tabLine->{'freeInfo8'});   # Ligne 8 : famille commerciale
        }

        push(@tabInvoicesToUpdate, $tabInvoice->{'id'});
    }

    # Passage des factures à l'état "exportée"
    my $tabUpdates = {'rpi_sta_id' => LOC::Invoice::STATE_EXPORTED, 'rpi_date_export*' => 'NOW()'};
    my $update = $db->update('f_repairinvoice', $tabUpdates, {'rpi_id' => \@tabInvoicesToUpdate}, $schemaName);

    if ($update == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        @tabContent = ($version);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
        }
    }

    push(@tabContent, '#FIN');   # Drapeau de fin de fichier

    my $content = &LOC::Util::encodeUtf8(join(LOC::Invoice::EXPORTFILE_LINEEND, @tabContent));
    &Encode::from_to($content, 'utf8', LOC::Invoice::EXPORTFILE_ENCODING);

    return $content;
}

# Function: getInvoicesToCancel
# Retourne la liste des factures à supprimer dans le cas d'une annulation de facturation
#
# Parameters:
# string $countryId - Identifiant du pays
# string $date      - Date de la facturation à annuler
#
# Returns:
# hashref - Liste des factures à supprimer
sub getInvoicesToCancel
{
    my ($countryId, $date) = @_;

    my $invoicingId = &LOC::Invoice::getId($countryId, $date, LOC::Invoice::TYPE_REPAIR);
    return &getList($countryId, LOC::Util::GETLIST_ASSOC, {'invoicingId' => $invoicingId});
}

# Function: cancelInvoicing
# Annule une facturation
#
# Parameters:
# string $countryId - Identifiant du pays
# string $date      - Date de la facturation à annuler
#
# Returns:
# hashref - Liste des factures supprimées
sub cancelInvoicing
{
    my ($countryId, $date) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Liste des factures à supprimer
    my $tabInvoices = &getInvoicesToCancel($countryId, $date);
    my @tabInvoiceIds = ();
    my @tabEstimateIds = ();

    foreach my $tabInvoice (values(%$tabInvoices))
    {
        push(@tabInvoiceIds, $tabInvoice->{'id'});
        push(@tabEstimateIds, $tabInvoice->{'repairEstimate.id'});
    }

    # Passage des factures à l'état "non exportée"
    my $tabUpdates = {'rpi_sta_id'          => LOC::Invoice::STATE_NOTEXPORTED,
                      'rpi_document*'       => 'NULL',
                      'rpi_date_export*'    => 'NULL',
                      'rpi_date_invoicing*' => 'NULL',
                      'rpi_date*'           => 'NULL',
                      'rpi_invoicingid*'    => 'NULL'};
    my $tabWheres = {'rpi_id' => \@tabInvoiceIds};
    my $update = $db->update('f_repairinvoice', $tabUpdates, $tabWheres, $schemaName);

    if ($update == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return -1;
    }

    # Passage des devis de remise en état à l'état "facturé"
    if (&LOC::Repair::Estimate::setState($countryId, \@tabEstimateIds, LOC::Repair::Estimate::STATE_APPROVED) == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return -1;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }

    return $update;
}


# Function: update
# Met à jour une facture de remise en état
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $invoiceId  - Identifiant de la facture
# hashref $tabData    - Données à modifier
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub update
{
    my ($countryId, $invoiceId, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'update', $_[0], __FILE__, '');
        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }
        return 0;
    };

    # Récupération des informations
    my $tabInfos = &getInfos($countryId, $invoiceId);
    if (!$tabInfos)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Vérification des données
    my $oldGeneralAccount = $tabInfos->{'generalAccount'};
    my $newGeneralAccount = (exists $tabData->{'generalAccount'} ? $tabData->{'generalAccount'} : $oldGeneralAccount);

    my $tabUpdates = {};
    if ($oldGeneralAccount ne $newGeneralAccount)
    {
        $tabUpdates->{'rpi_generalaccount'} = $newGeneralAccount;
    }

    if (keys %$tabUpdates > 0)
    {
        if (!$db->update('f_repairinvoice', $tabUpdates, {'rpi_id' => $invoiceId}))
        {
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: onExternalChanges
# Exécute toutes les mises à jour nécessaires lors de la modification d'une entitée externe
#
# Parameters:
# string  $countryId  - Pays
# hashref $tabChanges - Informations sur les mises à jour des entitées externes
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub onExternalChanges
{
    my ($countryId, $tabChanges, $userId, $tabErrors, $tabEndUpds) = @_;

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'onExternalChanges', $_[0], __FILE__, '');
        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }
        return 0;
    };

    my %tabToUpdate = ();

    # Impacts des clients sur les transports
    # ---------------------------------------------
    if (exists $tabChanges->{'customer'})
    {
        my $id = $tabChanges->{'customer'}->{'id'};

        my $tabProps = $tabChanges->{'customer'}->{'props'};
        if (keys %$tabProps > 0)
        {
            my $tabCustomerInfos = &LOC::Customer::getInfos($countryId, $id);
            # Impact sur les factures non exportées
            if ($tabProps->{'accountingCode'})
            {
                my $tabInvoices = &LOC::Invoice::Repair::getList($countryId,
                                                                 LOC::Util::GETLIST_ASSOC,
                                                                 {'customerId' => $id,
                                                                  'numbered'   => 0
                                                                 });
                foreach my $invoiceId (keys %$tabInvoices)
                {
                    # Nouveau système de remise en état
                    if ($tabInvoices->{$invoiceId}->{'type'} eq LOC::Invoice::TYPE_REPAIR)
                    {
                        $tabToUpdate{$invoiceId} = {
                            'generalAccount' => $tabProps->{'accountingCode'}->{'new'}
                        };
                    }
                }
            }
        }
    }

    # Mise à jour des factures
    foreach my $invoiceId (keys %tabToUpdate)
    {
        if (!&LOC::Invoice::Repair::update($countryId, $invoiceId, $tabToUpdate{$invoiceId}))
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}

1;
