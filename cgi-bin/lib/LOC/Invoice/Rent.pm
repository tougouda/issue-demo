use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Invoice::Rent
# Module permettant d'obtenir les informations sur les factures de location
package LOC::Invoice::Rent;


# Constants: Flags pour les types de retour attendus
# GETINFOS_ALL - Récupération de toutes les informations
# GETINFOS_BASE - Récupération des informations de base
use constant
{
    GETINFOS_ALL            => 0xFFFFF,
    GETINFOS_BASE           => 0x00000,
};

# Constants: Types de facturation
# INVOICETYPE_MONTH - Facturation mensuelle
# INVOICETYPE_INTERMEDIATE - Facturation intermédiaire
use constant
{
    INVOICETYPE_MONTH        => 'M',
    INVOICETYPE_INTERMEDIATE => 'I',
};

# Constants: Origine de la facture
# ORIGIN_CONTRACT  - Facture générée à la mise en facturation du contrat
# ORIGIN_INVOICING - Facture générée au cours de la procédure de facturation
use constant
{
    ORIGIN_CONTRACT  => 'CONTRAT',
    ORIGIN_INVOICING => 'FACTU',
};

# Constants: Erreurs
# ERROR_FATAL                 - Erreur fatale
use constant
{
    ERROR_FATAL                 => 0x0001
};


use strict;

# Modules externes
use Date::Calc;
use Encode;
use POSIX;

# Modules internes
use LOC::Country;
use LOC::Db;
use LOC::Globals;
use LOC::Locale;
use LOC::Invoice;
use LOC::Invoice::ArticleRef;
use LOC::InvoicingRequest;
use LOC::Contract::Rent;
use LOC::Date;
use LOC::MachinesFamily;
use LOC::BusinessFamily;
use LOC::Util;
use LOC::Characteristic;
use LOC::Contract::RepairService;
use LOC::Common::RentService;
use LOC::Contract::RepairDay;
use LOC::Log;
use LOC::Alert;
use LOC::Json;
use LOC::Proforma;
use LOC::Common;
use LOC::Common::Rent;
use LOC::StartCode::Contract::Rent;
use LOC::Day;
use LOC::Transport;
use LOC::Insurance::Type;

my $fatalErrMsg = 'Erreur fatale: module LOC::Invoice, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";



# Function: getInfos
# Retourne les infos d'une facture de location
#
# Contenu du hashage :
# - id : identifiant de la facture
# - number : numéro de la facture
# - invoicingDate : date de génération de la facture ;
# - date : date de la facture ;
# - invoicingRentType : type de facturation (INVOICETYPE_MONTH ou INVOICETYPE_INTERMEDIATE) ;
# - contract.id : identifiant du contrat ;
# - contract.code : code du contrat ;
# - contract.beginDate : date de début du contrat ;
# - customer.id : identifiant du client ;
# - customer.code : code du client ;
# - customer.name : raison sociale du client ;
# - customer.address1 : adresse du client ;
# - customer.address2 : complément d'adresse du client ;
# - customer.address3 : code postal et ville du client ;
# - header.line1 : première ligne d'en-tête ;
# - header.line2 : deuxième ligne d'en-tête ;
# - header.line3 : troisième ligne d'en-tête ;
# - header.line4 : quatrième ligne d'en-tête ;
# - affair : code affaire ;
# - accountingCategory : catégorie comptable (CLCEE)
# - generalAccount : code collectif ;
# - beginDate : date de début de la période facturée ;
# - endDate : date de fin de la période facturée ;
# - amount : montant total HT de la facture en devise locale ;
# - amount.euros : montant total HT de la facture en euros ;
# - vatAmount : montant de la TVA en devise locale ;
# - vatAmount.euros : montant de la TVA en euros ;
# - taxesIncludedAmount : montant total TTC de la facture en devise locale ;
# - taxesIncludedAmount.euros : montant total TTC de la facture en euros ;
# - agency.id : identifiant de l'agence ;
# - creationDate : date de création ;
# - modificationDate : date de dernière modification ;
# - exportDate : date d'export ;
# - state.id : identifiant de l'état ;
# - line : tableau associatif contenant les lignes de la facture :
#   * id : identifiant de la ligne ;
#   * affair : code affaire ;
#   * thirdParty : tiers ;
#   * article : code article ;
#   * designation : désignation ;
#   * price : prix unitaire en devise locale ;
#   * price.euros : prix unitaire en euros ;
#   * reduction : pourcentage de réduction ;
#   * vat1 : TVA 1 ;
#   * vat1.type : type de TVA 1 ;
#   * vat2 : TVA 2 ;
#   * vat2.type : type de TVA 2 ;
#   * vat3 : TVA 3 ;
#   * vat3.type : type de TVA 3 ;
#   * quantity : quantité ;
#   * conditioning : conditionnement ;
#   * deposit : dépôt ;
#   * comments : commentaires ;
#   * freeInfo2 : informations libres 2 (n° de parc) ;
#   * freeInfo3 : informations libres 3 (modèle de machines) ;
#   * freeInfo4 : informations libres 4 (n° de série) ;
#   * freeInfo5 : informations libres 5 (agence) ;
#   * freeInfo8 : informations libres 8 (famille commerciale) ;
#
# Parameters:
# string $id        - Identifiant du modèle
# string $countryId - Identifiant du pays
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getInfos
{
    my ($countryId, $id) = @_;

    # Liste des taux de TVA utilisés dans la facture
    my @tabVATRates = ();

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Récupère les données d'en-tête
    my $query = '
SELECT
    tbl_f_rentinvoice.ENTETEAUTO AS `id`,
    tbl_f_rentinvoice.ENTETENUMFAC AS `number`,
    tbl_f_rentinvoice.ENTETEDATEFAC AS `invoicingDate`,
    tbl_f_rentinvoice.ENTETEDATEDOC AS `date`,
    tbl_f_rentinvoice.ENTETETYPEFACT AS `invoicingRentType`,
    tbl_f_contract.CONTRATAUTO AS `contract.id`,
    tbl_f_rentinvoice.CONTRATCODE AS `contract.code`,
    tbl_f_rentinvoice.ENTETECONTRATDD AS `contract.beginDate`,
    tbl_f_customer.CLAUTO AS `customer.id`,
    tbl_f_rentinvoice.CLCODE AS `customer.code`,
    tbl_f_customer.CLSTE AS `customer.name`,
    tbl_f_customer.CLADRESSE AS `customer.address1`,
    tbl_f_customer.CLADRESSE2 AS `customer.address2`,
    IF(tbl_p_customerlocality.VICLAUTO IS NULL,
        CONCAT_WS(" ", tbl_f_customer.CLCP, tbl_f_customer.CLVILLE),
        CONCAT_WS(" ", tbl_p_customerlocality.VICLCP, tbl_p_customerlocality.VICLNOM)) AS `customer.address3`,
    tbl_f_rentinvoice.ENTETE1REFCMMDE AS `header.line1`,
    tbl_f_rentinvoice.ENTETE2 AS `header.line2`,
    tbl_f_rentinvoice.ENTETE3ADR1 AS `header.line3`,
    tbl_f_rentinvoice.ENTETE4ADR2 AS `header.line4`,
    tbl_f_rentinvoice.ENTETEAFFAIRE AS `affair`,
    tbl_f_rentinvoice.ENTETECATEGORIE AS `accountingCategory`,
    tbl_f_rentinvoice.ENTETECOLLECTIF AS `generalAccount`,
    tbl_f_rentinvoice.ENTETEDATED AS `beginDate`,
    tbl_f_rentinvoice.ENTETEDATEF AS `endDate`,
    tbl_f_rentinvoice.ENTETEMONTANTFR AS `amount`,
    tbl_f_rentinvoice.ENTETEMONTANTEU AS `amount.euros`,
    tbl_f_rentinvoice.ENTETETVA AS `vatAmount`,
    tbl_f_rentinvoice.ENTETETVAEU AS `vatAmount.euros`,
    tbl_f_rentinvoice.ENTETETTC AS `taxesIncludedAmount`,
    tbl_f_rentinvoice.ENTETETTCEU AS `taxesIncludedAmount.euros`,
    tbl_f_rentinvoice.AGAUTO AS `agency.id`,
    tbl_f_rentinvoice.ENTETEDATECREATION AS `creationDate`,
    tbl_f_rentinvoice.ENTETEDATEMODIF AS `modificationDate`,
    tbl_f_rentinvoice.ENTETEDATEEXPORT AS `exportDate`,
    tbl_f_rentinvoice.ETCODE AS `state.id`
FROM ENTETEFACTURE tbl_f_rentinvoice
LEFT JOIN LIGNEFACTURE tbl_f_rentinvoiceline
ON tbl_f_rentinvoiceline.ENTETEAUTO = tbl_f_rentinvoice.ENTETEAUTO
LEFT JOIN CLIENT tbl_f_customer
ON tbl_f_rentinvoice.CLCODE = tbl_f_customer.CLCODE
LEFT JOIN AUTH.VICL tbl_p_customerlocality
ON tbl_f_customer.VICLAUTO = tbl_p_customerlocality.VICLAUTO
LEFT JOIN CONTRAT tbl_f_contract
ON (tbl_f_rentinvoice.CONTRATCODE = tbl_f_contract.CONTRATCODE AND tbl_f_contract.CONTRATOK = -1)
WHERE tbl_f_rentinvoice.ENTETEAUTO = ' . $id . ';';

    my $tabHeader = $db->fetchAssoc($query, {}, 'id');

    my $tabResult = $tabHeader->{$id};

    # Récupère les données des lignes
    my $query = '
SELECT
    tbl_f_rentinvoiceline.LIGNEAUTO AS `id`,
    tbl_f_rentinvoiceline.LIGNEAFFAIRE AS `affair`,
    tbl_f_rentinvoiceline.LIGNETIERS AS `thirdParty`,
    tbl_f_rentinvoiceline.LIGNEREFARTICLE AS `article`,
    tbl_f_rentinvoiceline.LIGNEDESIGNATION AS `designation`,
    tbl_f_rentinvoiceline.LIGNEPRIX AS `price`,
    tbl_f_rentinvoiceline.LIGNEPRIXEU AS `price.euros`,
    tbl_f_rentinvoiceline.LIGNEREMISE AS `reduction`,
    tbl_f_rentinvoiceline.LIGNETVA1 AS `vat1`,
    tbl_f_rentinvoiceline.LIGNETYPETVA1 AS `vat1.type`,
    tbl_f_rentinvoiceline.LIGNETVA2 AS `vat2`,
    tbl_f_rentinvoiceline.LIGNETYPETVA2 AS `vat2.type`,
    tbl_f_rentinvoiceline.LIGNETVA3 AS `vat3`,
    tbl_f_rentinvoiceline.LIGNETYPETVA3 AS `vat3.type`,
    tbl_f_rentinvoiceline.LIGNEQTITE AS `quantity`,
    tbl_f_rentinvoiceline.LIGNECONDITIONNEMENT AS `conditioning`,
    tbl_f_rentinvoiceline.LIGNEDEPOT AS `deposit`,
    tbl_f_rentinvoiceline.LIGNECOMMENTAIRE AS `comments`,
    tbl_f_rentinvoiceline.LIGNEINFOLIBRE2 AS `freeInfo2`,
    tbl_f_rentinvoiceline.LIGNEINFOLIBRE3 AS `freeInfo3`,
    tbl_f_rentinvoiceline.LIGNEINFOLIBRE4 AS `freeInfo4`,
    tbl_f_rentinvoiceline.LIGNEINFOLIBRE5 AS `freeInfo5`,
    tbl_f_rentinvoiceline.LIGNEINFOLIBRE8 AS `freeInfo8`
FROM LIGNEFACTURE tbl_f_rentinvoiceline
WHERE tbl_f_rentinvoiceline.ENTETEAUTO = ' . $id . ';';

    my $tabLines = $db->fetchAssoc($query, {}, 'id');

    $tabResult->{'lines'} = [];
    foreach my $tabRow (values %$tabLines)
    {
        # Formatage des données
        $tabRow->{'id'} *= 1;
        if (defined $tabRow->{'price'})
        {
            $tabRow->{'price'} *= 1;
        }
        if (defined $tabRow->{'price.euros'})
        {
            $tabRow->{'price.euros'} *= 1;
        }
        if (defined $tabRow->{'vat1'})
        {
            $tabRow->{'vat1'} /= 100;
        }
        if (defined $tabRow->{'vat1.type'})
        {
            $tabRow->{'vat1.type'} *= 1;
        }
        if (defined $tabRow->{'vat2'})
        {
            $tabRow->{'vat2'} /= 100;
        }
        if (defined $tabRow->{'vat2.type'})
        {
            $tabRow->{'vat2.type'} *= 1;
        }
        if (defined $tabRow->{'vat3'})
        {
            $tabRow->{'vat3'} /= 100;
        }
        if (defined $tabRow->{'vat3.type'})
        {
            $tabRow->{'vat3.type'} *= 1;
        }
        if (defined $tabRow->{'quantity'})
        {
            $tabRow->{'quantity'} *= 1;
        }
        if (defined $tabRow->{'reduction'})
        {
            $tabRow->{'reduction'} /= 100;
        }
        if (defined $tabRow->{'price'} || defined $tabRow->{'reduction'})
        {
            $tabRow->{'netPrice'} = &LOC::Util::round($tabRow->{'price'} * (1 - $tabRow->{'reduction'}), 4);
        }
        else
        {
            $tabRow->{'netPrice'} = undef;
        }
        if (defined $tabRow->{'netPrice'} || defined $tabRow->{'quantity'})
        {
            $tabRow->{'amount'} = $tabRow->{'netPrice'} * $tabRow->{'quantity'};
        }
        else
        {
            $tabRow->{'amount'} = undef;
        }

        # Récupération du code TVA
        foreach my $vatRate ($tabRow->{'vat1'}, $tabRow->{'vat2'}, $tabRow->{'vat3'})
        {
            if ($vatRate != 0)
            {
                my ($vatCode) = grep { $tabVATRates[$_] == $vatRate } 0..@tabVATRates-1;
                if ($vatCode eq '')
                {
                    $vatCode = @tabVATRates;
                    push(@tabVATRates, $vatRate);
                }
                $vatCode++;
                $tabRow->{'vatCode'} = $vatCode;

                # Stockage du taux et du montant de base pour le calcul de la TVA au taux de la ligne courante
                $tabResult->{'vat'}->{$vatCode}->{'rate'}    = $vatRate;
                $tabResult->{'vat'}->{$vatCode}->{'base'}   += $tabRow->{'amount'};
            }
        }

        push(@{$tabResult->{'lines'}}, $tabRow);
    }

    # Formatage des données
    $tabResult->{'id'}                        *= 1;
    $tabResult->{'contract.id'}               *= 1;
    $tabResult->{'customer.id'}               *= 1;
    $tabResult->{'amount'}                    *= 1;
    $tabResult->{'amount.euros'}              *= 1;
    $tabResult->{'vatAmount'}                 *= 1;
    $tabResult->{'vatAmount.euros'}           *= 1;
    $tabResult->{'taxesIncludedAmount'}       *= 1;
    $tabResult->{'taxesIncludedAmount.euros'} *= 1;

    # Calcul du montant de la TVA pour chaque taux
    foreach my $vatCode (keys(%{$tabResult->{'vat'}}))
    {
        $tabResult->{'vat'}->{$vatCode}->{'amount'} = $tabResult->{'vat'}->{$vatCode}->{'base'}
                                                      * $tabResult->{'vat'}->{$vatCode}->{'rate'};
    }

    return (wantarray ? %$tabResult : $tabResult);
}

# Function: getList
# Retourne la liste des factures de location
#
# Contenu du hashage :
# - id : identifiant de la facture ;
# - number : numéro de la facture ;
# - invoicingDate : date de génération de la facture ;
# - invoicingRentType : type de facturation de location (INVOICETYPE_MONTH ou INVOICETYPE_INTERMEDIATE) ;
# - invoicingOrigin : origine de la facture (ORIGIN_CONTRACT ou ORIGIN_INVOICING) ;
# - date : date de la facture ;
# - contract.id : identifiant du contrat ;
# - contract.code : code du contrat ;
# - customer.id : identifiant du client ;
# - customer.code : code du client ;
# - customer.label : raison sociale du client ;
# - beginDate : date de début de facturation ;
# - endDate : date de fin de facturation ;
# - amount : montant total de la facture.
#
# Parameters:
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres ('id' : Recherche par l'identifiant,
#                                'contractCode' : recherche par code de contrat,
#                                'rentType' : type de facturation (INVOICETYPE_MONTH ou INVOICETYPE_INTERMEDIATE),
#                                'invoicingId' : identifiant de la facturation,
#                                'invoicingDate' : date de génération de la facture,
#                                'numbered' : factures non numérotées)
#
# Returns:
# hash|hashref|0 - Liste des factures (mêmes clés que pour la méthode getInfos)
sub getList
{
    my ($countryId, $format, $tabFilters) = @_;

    if ($countryId eq '')
    {
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);


    # Jointures pour les recherches
    my $joinQuery = '';
    if (defined $tabFilters->{'tariffFamilyId'})
    {
        $joinQuery .= '
LEFT JOIN CONTRAT tbl_f_contract
ON tbl_f_rentinvoice.CONTRATAUTO = tbl_f_contract.CONTRATAUTO
LEFT JOIN AUTH.MODELEMACHINE tbl_f_model
ON tbl_f_contract.MOMAAUTO = tbl_f_model.MOMAAUTO';
    }


    # Filtres
    my $whereQuery = '';
    # - Identifiant de la facture
    if ($tabFilters->{'id'} ne '')
    {
        $whereQuery .= '
AND tbl_f_rentinvoice.ENTETEAUTO = ' . $db->quote($tabFilters->{'id'});
    }
    # - Code du contrat
    if ($tabFilters->{'contractCode'} ne '')
    {
        $whereQuery .= '
AND tbl_f_rentinvoice.CONTRATCODE IN (' . $db->quote($tabFilters->{'contractCode'}) . ')';
    }
    # - Code du client
    if ($tabFilters->{'customerCode'} ne '')
    {
        $whereQuery .= '
AND tbl_f_rentinvoice.CLCODE IN (' . $db->quote($tabFilters->{'customerCode'}) . ')';
    }
    # - Type de facturation de location
    if ($tabFilters->{'rentType'} ne '')
    {
        $whereQuery .= '
AND tbl_f_rentinvoice.ENTETETYPEFACT = ' . $db->quote($tabFilters->{'rentType'});
    }
    # - Identifiant de la facturation
    if ($tabFilters->{'invoicingId'} ne '')
    {
        $whereQuery .= '
AND tbl_f_rentinvoice.ENTETEIDFACTU = ' . $db->quote($tabFilters->{'invoicingId'});
    }
    # - Date de facturation
    if ($tabFilters->{'invoicingDate'} ne '')
    {
        $whereQuery .= '
AND tbl_f_rentinvoice.ENTETEDATEFAC = ' . $db->quote($tabFilters->{'invoicingDate'});
    }
    # - Date
    if ($tabFilters->{'numbered'} ne '')
    {
        if ($tabFilters->{'numbered'})
        {
            $whereQuery .= '
AND tbl_f_rentinvoice.ENTETENUMFAC IS NOT NULL AND tbl_f_rentinvoice.ENTETEIDFACTU IS NOT NULL';
        }
        else
        {
            $whereQuery .= '
AND (tbl_f_rentinvoice.ENTETENUMFAC IS NULL OR tbl_f_rentinvoice.ENTETEIDFACTU IS NULL)';
        }
    }
    # - Agence
    if (defined $tabFilters->{'agencyId'})
    {
        $whereQuery .= '
AND tbl_f_rentinvoice.AGAUTO IN (' . $db->quote($tabFilters->{'agencyId'}) . ')';
    }
    # - Date de facture
    if (defined $tabFilters->{'date'})
    {
        if (ref $tabFilters->{'date'} eq 'ARRAY')
        {
            $whereQuery .= '
AND tbl_f_rentinvoice.ENTETEDATEDOC BETWEEN ' . $db->quote($tabFilters->{'date'}->[0]) . ' AND ' . $db->quote($tabFilters->{'date'}->[1]);
        }
        else
        {
            $whereQuery .= '
AND tbl_f_rentinvoice.ENTETEDATEDOC = ' . $db->quote($tabFilters->{'date'});
        }
    }
    # - Famille tarifaire
    if (defined $tabFilters->{'tariffFamilyId'})
    {
        $whereQuery .= '
AND tbl_f_model.FAMAAUTO IN (' . $db->quote($tabFilters->{'tariffFamilyId'}) . ')';
    }

    # Récupère les données concernant la facture
    my $query = '';
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    tbl_f_rentinvoice.ENTETEAUTO AS `id`,
    tbl_f_rentinvoice.ENTETENUMFAC AS `number`,
    tbl_f_rentinvoice.ENTETEDATEFAC AS `invoicingDate`,
    tbl_f_rentinvoice.ENTETETYPEFACT AS `invoicingRentType`,
    tbl_f_rentinvoice.ENTETEORIGINE AS `invoicingOrigin`,
    tbl_f_rentinvoice.ENTETEDATEDOC AS `date`,
    tbl_f_rentinvoice.CONTRATAUTO AS `contract.id`,
    tbl_f_rentinvoice.CONTRATCODE AS `contract.code`,
    tbl_f_rentinvoice.ETATFACTURE AS `contract.invoiceState`,
    tbl_f_customer.CLAUTO AS `customer.id`,
    tbl_f_rentinvoice.CLCODE AS `customer.code`,
    tbl_f_customer.CLSTE AS `customer.name`,
    tbl_f_rentinvoice.ENTETEDATED AS `beginDate`,
    tbl_f_rentinvoice.ENTETEDATEF AS `endDate`,
    tbl_f_rentinvoice.ENTETEMONTANTFR AS `amount`
FROM ENTETEFACTURE tbl_f_rentinvoice
LEFT JOIN CLIENT tbl_f_customer
ON tbl_f_rentinvoice.CLCODE = tbl_f_customer.CLCODE' .
$joinQuery . '
WHERE 1' .
$whereQuery . '
GROUP BY tbl_f_rentinvoice.ENTETEAUTO
ORDER BY `invoicingDate`, `number`;';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*) AS `nbInvoices`
FROM ENTETEFACTURE tbl_f_rentinvoice
WHERE 1' .
$whereQuery . ';';
    }
    else
    {
        $query = '
SELECT
    tbl_f_rentinvoice.ENTETEAUTO AS `id`,
    tbl_f_rentinvoice.ENTETENUMFAC AS `number`
FROM ENTETEFACTURE tbl_f_rentinvoice
WHERE 1' .
$whereQuery . '
ORDER BY `number`;';
    }

    my $tabResult = $db->fetchList($format, $query, {}, 'id');

    if ($format eq LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tabResult)
        {
            # Formatage des données
            $tabRow->{'customer.id'} *= 1;
            $tabRow->{'amount'}      *= 1;
        }
    }

    return (wantarray ? %$tabResult : $tabResult);
}

# Function: getNextInvoicingDate
# Retourne la date de prochaine facturation de location
#
# Parameters:
# string  $countryId - Identifiant du pays. Retourne undef si le pays n'est pas précisé
# string  $rentType  - Type de facturation (INVOICETYPE_MONTH ou INVOICETYPE_INTERMEDIATE)
# Si non précisé, c'est la date de prochaine facturation de location tous types confondus qui est retournée
#
# Returns:
# undef|hashref - Tableau contenant la date de la prochaine facturation de location ('date'), son type ('type')
#                 et son identifiant ('id'), ou undef si aucune date n'est trouvée
sub getNextInvoicingDate
{
    my ($countryId, $rentType) = @_;

    unless (defined $countryId)
    {
        return undef;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('auth');

    my $where = '';
    if (defined $rentType)
    {
        $where .= ' AND ivd_type = ' . $db->quote($rentType);
    }

    # Récupération de la prochaine date de facturation de location paramétrée en base
    my $query = '
SELECT
    ivd_date AS `date`,
    ivd_type AS `rentType`
FROM p_invoicedate
WHERE ivd_done = 0
AND ivd_cty_id = ' . $db->quote($countryId) . '
' . $where . '
ORDER BY `date`
LIMIT 1;';

    my $tabNextInvoicingDate = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    $tabNextInvoicingDate->{'id'} = &LOC::Invoice::getId($countryId, $tabNextInvoicingDate->{'date'},
                                                         LOC::Invoice::TYPE_RENT);

#    return {'date' => '2011-01-03', 'rentType' => 'M', 'id' => '20110103RENTMA'};
    return $tabNextInvoicingDate;
}

# Function: getLastInvoicingDate
# Retourne la date de dernière facturation de location
#
# Parameters:
# string  $countryId - Identifiant du pays. Retourne undef si le pays n'est pas précisé
# string  $rentType - Type de facturation (LOC::Invoice::INVOICETYPE_MONTH ou LOC::Invoice::INVOICETYPE_INTERMEDIATE)
# Si non précisé, c'est la date de prochaine facturation de location tous types confondus qui est retournée
#
# Returns:
# undef|hashref - Tableau contenant la date de la dernière facturation de location ('date'), son type ('type')
#                 et son identifiant ('id'), ou undef si aucune date n'est trouvée
sub getLastInvoicingDate
{
    my ($countryId, $rentType) = @_;

    unless (defined $countryId)
    {
        return undef;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $where = '';
    if (defined $rentType)
    {
        $where .= ' AND tbl_f_rentinvoice.ENTETETYPEFACT = ' . $db->quote($rentType);
    }

    # Récupération de la prochaine date de facturation de location paramétrée en base
    my $query = '
SELECT
    CONCAT_WS("-", LEFT(tbl_f_rentinvoice.ENTETEIDFACTU, 4),
                   SUBSTRING(tbl_f_rentinvoice.ENTETEIDFACTU, 5, 2),
                   SUBSTRING(tbl_f_rentinvoice.ENTETEIDFACTU, 7, 2)) AS `date`,
    tbl_f_rentinvoice.ENTETETYPEFACT AS `rentType`
FROM ENTETEFACTURE tbl_f_rentinvoice
WHERE TRUE
' . $where . '
ORDER BY `date` DESC
LIMIT 1;';

    my $tabPreviousInvoicingDate = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    $tabPreviousInvoicingDate->{'id'} = &LOC::Invoice::getId($countryId, $tabPreviousInvoicingDate->{'date'},
                                                             LOC::Invoice::TYPE_RENT);

    return $tabPreviousInvoicingDate;
}

# Function: getRentalPeriodBeginDate
# Retourne la date de début de la période de facturation de la location correspondant à une facturation
#
# Parameters:
# string  $invoiceDate - Date de facturation
# string  $rentType    - Type de facturation (INVOICETYPE_MONTH ou INVOICETYPE_INTERMEDIATE)
#
# Returns:
# undef|string - Date de début de la période de facturation de la location. undef si l'un des deux paramètres est omis
sub getRentalPeriodBeginDate
{
    my ($invoiceDate, $rentType) = @_;

    unless (defined $invoiceDate && defined $rentType)
    {
        return undef;
    }

    my @tabInvoiceDate = split(/\-/, $invoiceDate);
    $tabInvoiceDate[0] *= 1;
    $tabInvoiceDate[1] *= 1;
    $tabInvoiceDate[2] *= 1;

    # Facturation mensuelle :
    # Date de début de la période facturée = Premier jour du mois précédent
    if ($rentType eq INVOICETYPE_MONTH)
    {
        # Calcul de la date du jour du mois précédent
        my ($year, $month, $day) = &Date::Calc::Add_Delta_YM($tabInvoiceDate[0],
                                                             $tabInvoiceDate[1],
                                                             $tabInvoiceDate[2],
                                                             0,
                                                             -1);

        return sprintf('%04d-%02d-%02d', $year, $month, 1);
    }
    # Pas de date début pour la facturation intermédiaire
    else
    {
        return undef;
    }
}

# Function: getRentalPeriodEndDate
# Retourne la date de fin de la période de facturation de la location correspondant à une facturation
#
# Parameters:
# string  $invoiceDate - Date de facturation
# string  $rentType    - Type de facturation (INVOICETYPE_MONTH ou INVOICETYPE_INTERMEDIATE)
#
# Returns:
# undef|string - Date de fin de la période de facturation de la location. undef si l'un des deux paramètres est omis
sub getRentalPeriodEndDate
{
    my ($invoiceDate, $rentType) = @_;

    unless (defined $invoiceDate && defined $rentType)
    {
        return undef;
    }

    my @tabInvoiceDate = split(/\-/, $invoiceDate);
    $tabInvoiceDate[0] = &LOC::Util::toNumber($tabInvoiceDate[0]);
    $tabInvoiceDate[1] = &LOC::Util::toNumber($tabInvoiceDate[1]);
    $tabInvoiceDate[2] = &LOC::Util::toNumber($tabInvoiceDate[2]);

    # Facturation mensuelle :
    # Date de fin de la période facturée = Dernier jour du mois précédent
    if ($rentType eq INVOICETYPE_MONTH)
    {
        # Calcul de la date du jour du mois précédent
        my ($year, $month, $day) = &Date::Calc::Add_Delta_YM($tabInvoiceDate[0],
                                                             $tabInvoiceDate[1],
                                                             $tabInvoiceDate[2],
                                                             0,
                                                             -1);
        # Nombre de jours du mois précédent
        $day = &Date::Calc::Days_in_Month($year, $month);

        return sprintf('%04d-%02d-%02d', $year, $month, $day);
    }
    # Facturation intermédiaire :
    # Date de fin de la période facturée = Jour précédent
    elsif ($rentType eq INVOICETYPE_INTERMEDIATE)
    {
        # Calcul de la date du jour précédent
        my ($year, $month ,$day) = &Date::Calc::Add_Delta_Days($tabInvoiceDate[0],
                                                               $tabInvoiceDate[1],
                                                               $tabInvoiceDate[2],
                                                               -1);

        return sprintf('%04d-%02d-%02d', $year, $month, $day);
    }
    else
    {
        return undef;
    }
}

# Function: checkContracts
# Lance le contrôle des contrats
#
# Contenu du hashage :
# - readyToInvoice : 0 impossible de facturer, 1 prêt à facturer (quel que soit le flag);
# - beginMonthInvoiceDate : date de début du mois de facturation au format MySQL (quel que soit le flag);
# - endMonthInvoiceDate : date de fin du mois de facturation au format MySQL (quel que soit le flag);
# - contractsToStop : liste des contrats à arrêter (flag GETINFOS_ALL seulement);
# - fuelsToType : liste des carburants à saisir (flag GETINFOS_ALL seulement);

# Parameters:
# string $countryId   - Identifiant du pays
# string $invoiceDate - Date de facturation
# int    $mode        - GETINFOS_ALL (récupération des contrats) ou GETINFOS_BASE par défaut (uniquement readyToInvoice)
# string $rentType    - Type de facturation (INVOICETYPE_INTERMEDIATE par défaut)
#
# Returns:
# hashref|0 - Tableau contenant les résultats ou 0 si il y a une erreur
sub checkContracts
{
    my ($countryId, $date, $mode, $rentType) = @_;

    unless (defined $rentType)
    {
        $rentType = INVOICETYPE_INTERMEDIATE;
    }

    if ($rentType eq INVOICETYPE_INTERMEDIATE)
    {
        return &checkContractsForIntermediateInvoicing($countryId, $date, $mode);
    }
    if ($rentType eq INVOICETYPE_MONTH)
    {
        return &checkContractsForMonthlyInvoicing($countryId, $date, $mode);
    }
    return 0;
}

# Function: getContractsToStop
# Récupère la liste des contrats à arrêter
#
# Parameters:
# string $countryId - Identifiant du pays
# string $date      - Date de facturation
#
# Returns:
# hashref|0 - Liste des contrats à arrêter ou 0 si il y a une erreur
sub getContractsToStop
{
    my ($countryId, $date) = @_;

    # Vérification des paramètres de la fonction
    if ($countryId eq '')
    {
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Récupération des contrats à arrêter par agence
    my $query = '
SELECT
    tbl_f_contract.CONTRATAUTO AS `id`,
    tbl_f_contract.CONTRATCODE AS `code`,
    tbl_f_contract.CONTRATDD AS `beginDate`,
    tbl_f_contract.CONTRATDR AS `endDate`,
    tbl_f_contract.AGAUTO AS `agency.id`,
    usr_firstname AS `user.firstname`,
    usr_name AS `user.name`,
    CONCAT_WS(" ", usr_firstname, usr_name) AS `user.fullName`,
    usr_sta_id AS `user.state.id`
FROM CONTRAT tbl_f_contract
LEFT JOIN frmwrk.f_user
ON usr_id = tbl_f_contract.PEAUTO
WHERE tbl_f_contract.ETCODE = ' . $db->quote(LOC::Contract::Rent::STATE_CONTRACT) . '
AND tbl_f_contract.CONTRATOK = -1
AND tbl_f_contract.CONTRATDR <= ' . $db->quote($date) . '
ORDER BY `agency.id`, `endDate`';

    return $db->fetchAssoc($query, {}, 'id');
}

# Function: getNoOfContractsToStopByAgency
# Récupère le nombre de contrats à arrêter par agence
#
# Parameters:
# string $countryId - Identifiant du pays
# string $date      - Date de facturation
#
# Returns:
# hashref|0 - Nombre de contrats à arrêter par agence ou 0 si il y a une erreur
sub getNoOfContractsToStopByAgency
{
    my ($countryId, $date) = @_;

    # Vérification des paramètres de la fonction
    if ($countryId eq '')
    {
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Récupération des contrats à arrêter par agence
    my $query = '
SELECT
    tbl_f_contract.AGAUTO AS `agency.id`, COUNT(*) AS `number`
FROM CONTRAT tbl_f_contract
WHERE tbl_f_contract.ETCODE = ' . $db->quote(LOC::Contract::Rent::STATE_CONTRACT) . '
AND tbl_f_contract.CONTRATOK = -1
AND tbl_f_contract.CONTRATDR <= ' . $db->quote($date) . '
GROUP BY `agency.id`
ORDER BY `agency.id`';

    return $db->fetchPairs($query);
}

# Function: getFuelsToType
# Récupère la liste des carburants à saisir
#
# Parameters:
# string $countryId - Identifiant du pays
# string $date      - Date de facturation
#
# Returns:
# hashref|0 - Liste des carburants à saisir ou 0 si il y a une erreur
sub getFuelsToType
{
    my ($countryId, $date) = @_;

    # Vérification des paramètres de la fonction
    if ($countryId eq '')
    {
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);


    my $additionalWhere;

    # Liste des énergies à carburant
    my $tabFuelEnergies = &LOC::MachinesFamily::getFuelEnergiesList();
    if (@$tabFuelEnergies > 0)
    {
        $additionalWhere = 'tbl_p_machinesfamily.FAMAENERGIE IN ("' . join('", "', @$tabFuelEnergies) . '")';
    }
    else
    {
        $additionalWhere = 'FALSE';
    }
    # Condition pour les pays qui facturent du carburant sur les contrats de sous-location
    my $subletFuelInvoice = &LOC::Characteristic::getCountryValueByCode('FCARBSL', $countryId);
    if ($subletFuelInvoice)
    {
        $additionalWhere .= ' OR tbl_f_contract.SOLOAUTO != 0';
    }

    my $query = '
SELECT
    tbl_f_contract.CONTRATAUTO AS `id`,
    tbl_f_contract.CONTRATCODE AS `code`,
    tbl_f_contract.CONTRATDD AS `beginDate`,
    tbl_f_contract.CONTRATDR AS `endDate`,
    tbl_f_contract.AGAUTO AS `agency.id`,
    usr_firstname AS `user.firstname`,
    usr_name AS `user.name`,
    CONCAT_WS(" ", usr_firstname, usr_name) AS `user.fullName`,
    usr_sta_id AS `user.state.id`,
    tbl_f_machine.MAAUTO AS `machine.id`,
    tbl_f_machine.MANOPARC AS `machine.parkNumber`
FROM CONTRAT tbl_f_contract
LEFT JOIN frmwrk.f_user
ON usr_id = tbl_f_contract.PEAUTO
LEFT JOIN frmwrk.p_agency
ON tbl_f_contract.AGAUTO = agc_id
LEFT JOIN AUTH.`MACHINE` tbl_f_machine
ON tbl_f_machine.MAAUTO = tbl_f_contract.MAAUTO
LEFT JOIN AUTH.`MODELEMACHINE` tbl_p_model
ON tbl_p_model.MOMAAUTO = tbl_f_machine.MOMAAUTO
LEFT JOIN FAMILLEMACHINE tbl_p_machinesfamily
ON tbl_p_model.FAMAAUTO = tbl_p_machinesfamily.FAMAAUTO
WHERE tbl_f_contract.ETCODE = ' . $db->quote(LOC::Contract::Rent::STATE_STOPPED) . '
AND tbl_f_contract.CONTRATOK = -1
AND tbl_f_contract.CONTRATDR <= ' . $db->quote($date) . '
AND tbl_f_contract.CARBUAUTO = 0
AND (' . $additionalWhere . ')
AND agc_type = ' . $db->quote(LOC::Agency::TYPE_AGENCY) . '
AND (tbl_p_model.MOMATYPAGE IS NULL OR tbl_p_model.MOMATYPAGE NOT LIKE "%\"' . LOC::Model::TYPE_ACCESSORY . '\"%")';

    return $db->fetchAssoc($query, {}, 'id');
}

# Function: getNoOfFuelsToTypeByAgency
# Récupère le nombre de carburants à saisir par agence
#
# Parameters:
# string $countryId - Identifiant du pays
# string $date      - Date de facturation
#
# Returns:
# hashref|0 - Nombre de carburants à saisir par agence ou 0 si il y a une erreur
sub getNoOfFuelsToTypeByAgency
{
    my ($countryId, $date) = @_;

    # Vérification des paramètres de la fonction
    if ($countryId eq '')
    {
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);


    my $additionalWhere;

    # Liste des énergies à carburant
    my $tabFuelEnergies = &LOC::MachinesFamily::getFuelEnergiesList();
    if (@$tabFuelEnergies > 0)
    {
        $additionalWhere = 'tbl_p_machinesfamily.FAMAENERGIE IN ("' . join('", "', @$tabFuelEnergies) . '")';
    }
    else
    {
        $additionalWhere = 'FALSE';
    }
    # Condition pour les pays qui facturent du carburant sur les contrats de sous-location
    my $subletFuelInvoice = &LOC::Characteristic::getCountryValueByCode('FCARBSL', $countryId);
    if ($subletFuelInvoice)
    {
        $additionalWhere .= ' OR tbl_f_contract.SOLOAUTO != 0';
    }

    my $query = '
SELECT
    tbl_f_contract.AGAUTO AS `agency.id`,
    COUNT(*) AS `number`
FROM CONTRAT tbl_f_contract
LEFT JOIN frmwrk.p_agency
ON tbl_f_contract.AGAUTO = agc_id
LEFT JOIN AUTH.`MACHINE` tbl_f_machine
ON tbl_f_machine.MAAUTO = tbl_f_contract.MAAUTO
LEFT JOIN AUTH.`MODELEMACHINE` tbl_p_model
ON tbl_p_model.MOMAAUTO = tbl_f_machine.MOMAAUTO
LEFT JOIN FAMILLEMACHINE tbl_p_machinesfamily
ON tbl_p_model.FAMAAUTO = tbl_p_machinesfamily.FAMAAUTO
WHERE tbl_f_contract.ETCODE = ' . $db->quote(LOC::Contract::Rent::STATE_STOPPED) . '
AND tbl_f_contract.CONTRATOK = -1
AND tbl_f_contract.CONTRATDR <= ' . $db->quote($date) . '
AND tbl_f_contract.CARBUAUTO = 0
AND (' . $additionalWhere . ')
AND agc_type = ' . $db->quote(LOC::Agency::TYPE_AGENCY) . '
AND (tbl_p_model.MOMATYPAGE IS NULL OR tbl_p_model.MOMATYPAGE NOT LIKE "%\"' . LOC::Model::TYPE_ACCESSORY . '\"%")
GROUP BY `agency.id`
ORDER BY `agency.id`';

    return $db->fetchPairs($query);
}

# Function: getNoOfContractsToInvoiceByAgency
# Récupère le nombre de contrats à mettre en facturation par agence
#
# Parameters:
# string $countryId - Identifiant du pays
# string $date      - Date de facturation
#
# Returns:
# hashref|0 - Nombre de contrats à mettre en facturation par agence ou 0 si il y a une erreur
sub getNoOfContractsToInvoiceByAgency
{
    my ($countryId, $date) = @_;

    # Vérification des paramètres de la fonction
    if ($countryId eq '')
    {
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT
    tbl_f_contract.AGAUTO AS `agency.id`, COUNT(*) AS `number`
FROM CONTRAT tbl_f_contract
WHERE tbl_f_contract.ETCODE = ' . $db->quote(LOC::Contract::Rent::STATE_STOPPED) . '
AND tbl_f_contract.CONTRATOK = -1
AND tbl_f_contract.CONTRATDR <= ' . $db->quote($date) . '
GROUP BY `agency.id`
ORDER BY `agency.id`';

    return $db->fetchPairs($query);
}

# Function: checkContractsForIntermediateInvoicing
# Contrôle des contrats en vue de la facturation intermédiaire
#
# Contenu du hashage :
# - readyToInvoice : 0 impossible de facturer, 1 prêt à facturer (quel que soit le flag);
# - control : 0 contrôle des contrats échoué, 1 contrôle des contrats OK (quel que soit le flag);
# - beginInvoiceDate : date de début du mois de facturation au format MySQL (quel que soit le flag);
# - endInvoiceDate : date de fin du mois de facturation au format MySQL (quel que soit le flag);
# - contractsToStop : liste des contrats à arrêter (flag GETINFOS_ALL seulement);
# - fuelsToType : liste des carburants à saisir (flag GETINFOS_ALL seulement);
#
# Parameters:
# string $countryId   - Identifiant du pays
# string $invoiceDate - Date de facturation
# int $mode           - GETINFOS_ALL (récupération des contrats) ou GETINFOS_BASE par défaut (uniquement readyToInvoice
#                       et control)
#
# Returns:
# hashref|0 - Tableau contenant le résultat des contrôles ou 0 si il y a une erreur
sub checkContractsForIntermediateInvoicing
{
    my ($countryId, $invoiceDate, $mode) = @_;

    # Vérification des paramètres de la fonction
    if ($countryId eq '')
    {
        return 0;
    }
    unless (defined $mode)
    {
        $mode = GETINFOS_BASE;
    }

    # Tableau de résultats en sortie
    my %tabResults = ();

    # Par défaut, on est prêt à facturer
    $tabResults{'readyToInvoice'} = 1;
    # Et le contrôle des contrats est OK
    $tabResults{'control'}        = 1;
    # Dates de début et de fin de la période facturée
    $tabResults{'beginInvoiceDate'} = &getRentalPeriodBeginDate($invoiceDate, INVOICETYPE_INTERMEDIATE);
    $tabResults{'endInvoiceDate'}   = &getRentalPeriodEndDate($invoiceDate, INVOICETYPE_INTERMEDIATE);

    # Si la date de facturation n'est pas passée, on ne peut pas facturer
    # Date du jour
    my $today = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE);
    if ($today lt $invoiceDate)
    {
        $tabResults{'readyToInvoice'} = 0;
    }

    # Si on est en mode de récupération des données
    if ($mode == GETINFOS_ALL)
    {
        # Récupération des contrats à arrêter par agence
        $tabResults{'contractsToStop'} = &getContractsToStop($countryId, $tabResults{'endInvoiceDate'});

        # Récupération des carburants à saisir par agence
        $tabResults{'fuelsToType'}    = &getFuelsToType($countryId, $tabResults{'endInvoiceDate'});
    }
    # Récupération des informations essentielles pour savoir si on peut facturer
    elsif ($mode == GETINFOS_BASE)
    {
        # Rien à faire dans ce cas
        # On peut toujours facturer en intermédiaire
    }

    return (wantarray ? %tabResults : \%tabResults);
}

# Function: checkContractsForMonthlyInvoicing
# Contrôle des contrats en vue de la facturation mensuelle
#
# Contenu du hashage :
# - readyToInvoice : 0 impossible de facturer, 1 prêt à facturer (quel que soit le flag);
# - control : 0 contrôle des contrats échoué, 1 contrôle des contrats OK (quel que soit le flag);
# - beginMonthInvoiceDate : date de début du mois de facturation au format MySQL (quel que soit le flag);
# - endMonthInvoiceDate : date de fin du mois de facturation au format MySQL (quel que soit le flag);
# - contractsToStop : liste des contrats à arrêter (flag GETINFOS_ALL seulement);
# - fuelsToType : liste des carburants à saisir (flag GETINFOS_ALL seulement);
#
# Parameters:
# string $countryId   - Identifiant du pays
# string $invoiceDate - Date de facturation
# int $mode           - GETINFOS_ALL (récupération des contrats) ou GETINFOS_BASE par défaut (uniquement readyToInvoice
#                       et control)
#
# Returns:
# hashref|0 - Tableau contenant le résultat des contrôles ou 0 si il y a une erreur
sub checkContractsForMonthlyInvoicing
{
    my ($countryId, $invoiceDate, $mode) = @_;

    # Vérification des paramètres de la fonction
    if ($countryId eq '')
    {
        return 0;
    }
    unless (defined $mode)
    {
        $mode = GETINFOS_BASE;
    }

    # Tableau de résultats en sortie
    my %tabResults = ();

    # Par défaut, on est prêt à facturer
    $tabResults{'readyToInvoice'} = 1;
    # Et le contrôle des contrats est OK
    $tabResults{'control'}        = 1;
    # Dates de début et de fin de la période facturée
    $tabResults{'beginMonthInvoiceDate'} = &getRentalPeriodBeginDate($invoiceDate, INVOICETYPE_MONTH);
    $tabResults{'endMonthInvoiceDate'}   = &getRentalPeriodEndDate($invoiceDate, INVOICETYPE_MONTH);

    # Si la date de facturation n'est pas passée, on ne peut pas facturer
    # Date du jour
    my $today = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE);
#   $today = '2011-01-03';

    if ($today lt $invoiceDate)
    {
        $tabResults{'readyToInvoice'} = 0;
    }

    # Si on est en mode de récupération des données
    if ($mode == GETINFOS_ALL)
    {
        # Récupération des contrats à arrêter par agence
        $tabResults{'contractsToStop'} = &getContractsToStop($countryId, $tabResults{'endMonthInvoiceDate'});

        # Récupération des carburants à saisir par agence
        $tabResults{'fuelsToType'}    = &getFuelsToType($countryId, $tabResults{'endMonthInvoiceDate'});
        $tabResults{'readyToInvoice'} = ($tabResults{'readyToInvoice'} && keys(%{$tabResults{'fuelsToType'}}) == 0);
        $tabResults{'control'}        = ($tabResults{'control'} && keys(%{$tabResults{'fuelsToType'}}) == 0);
    }
    # Récupération des informations essentielles pour savoir si on peut facturer
    elsif ($mode == GETINFOS_BASE)
    {
        # Récupération de la connexion à la base de données
        my $db = &LOC::Db::getConnection('location', $countryId);

        my $additionalWhere;

        # Liste des énergies à carburant
        my $tabFuelEnergies = &LOC::MachinesFamily::getFuelEnergiesList();
        if (@$tabFuelEnergies > 0)
        {
            $additionalWhere = 'FM.FAMAENERGIE IN ("' . join('", "', @$tabFuelEnergies) . '")';
        }
        else
        {
            $additionalWhere = 'FALSE';
        }
        # Condition pour les pays qui facturent du carburant sur les contrats de sous-location
        my $subletFuelInvoice = &LOC::Characteristic::getCountryValueByCode('FCARBSL', $countryId);
        if ($subletFuelInvoice)
        {
            $additionalWhere .= ' OR C.SOLOAUTO != 0';
        }

        # Récupération du nombre total de carburants à saisir (bloquant)
        my $queryNbFuel = '
SELECT
    COUNT(C.CONTRATAUTO)
FROM CONTRAT C
LEFT JOIN frmwrk.p_agency ON C.AGAUTO = agc_id
LEFT JOIN AUTH.`MACHINE` M ON M.MAAUTO=C.MAAUTO
LEFT JOIN AUTH.`MODELEMACHINE` MM ON MM.MOMAAUTO=M.MOMAAUTO
LEFT JOIN FAMILLEMACHINE FM ON MM.FAMAAUTO=FM.FAMAAUTO
WHERE C.ETCODE = ' . $db->quote(LOC::Contract::Rent::STATE_STOPPED) . '
AND C.CONTRATOK = -1
AND C.CONTRATDR <= ' . $db->quote($tabResults{'endMonthInvoiceDate'}) . '
AND C.CARBUAUTO = 0
AND (' . $additionalWhere . ')
AND agc_type = ' . $db->quote(LOC::Agency::TYPE_AGENCY) . '
AND (MM.MOMATYPAGE IS NULL OR MM.MOMATYPAGE NOT LIKE "%\"' . LOC::Model::TYPE_ACCESSORY . '\"%")';

        my $nbFuel = $db->fetchOne($queryNbFuel);

        # Si il reste des carburants à saisir, on ne peut pas facturer
        if ($nbFuel != 0)
        {
            $tabResults{'readyToInvoice'} = 0;
            $tabResults{'control'} = 0;
        }
    }

    return (wantarray ? %tabResults : \%tabResults);
}

# Function: readCustomersFile
# Lecture du fichier clients exporté de Sage
#
# Parameters:
# string $file - Chemin vers le fichier clients
#
# Returns:
# hashref|0 - Tableau contenant les résultats ou 0 si il y a une erreur
sub readCustomersFile
{
    my ($file) = @_;

    # Ouverture du fichier
    open(FILE, $file) or return 0;
    binmode FILE, ":raw";
    $/ = "\r\n";   # Caractères de fin de ligne à supprimer

    # Tableau contenant les informations des clients
    my $tabCustomers = {};

    # Lecture
    # Format v16
    my $isReadingCustomer = 0;   # Client en cours de lecture ou non
    my $fieldNo = 0;             # Numéro du champ après le drapeau #CCLI
    my $clcode = '';             # Code du client en cours de lecture
    while (my $line = <FILE>)
    {
        # Suppression des \n de fin de ligne
        chomp($line);

        # Début d'un bloc #CCLI
        if ($line =~ /\#CCLI/)
        {
            $isReadingCustomer = 1;
            $fieldNo = 0;
        }

        # Lecture des champs du bloc
        if ($isReadingCustomer)
        {
            # Lecture du code client (1er champ après le drapeau #CCLI)
            if ($fieldNo == 1)
            {
                $clcode = $line;
                $tabCustomers->{$clcode} = {};
            }
            # Lecture du code collectif (2e champ après le drapeau #CCLI)
            elsif ($fieldNo == 2 && $clcode ne '')
            {
                $tabCustomers->{$clcode}->{'generalAccount'} = $line;
            }
            # Lecture du nombre de factures (40e champ après le drapeau #CCLI)
            elsif ($fieldNo == 40 && $clcode ne '')
            {
                $tabCustomers->{$clcode}->{'invoicesNo'} = ($line eq '' ? 1 : $line);

                # Libération des variables pour lecture du client suivant
                $isReadingCustomer = 0;
                $clcode = '';
            }

            $fieldNo++;
        }
    }

    # Fermeture du fichier
    close(FILE);

    return $tabCustomers;
}

# Function: checkCustomers
# Contrôle de la cohérence des clients entre Sage et la Gestion des locations
#
# Parameters:
# string $countryId - Identifiant du pays
# string $date      - Date de facturation
# string $type      - Type de facturation (LOC::Invoice::TYPE_RENT par défaut)
# string $rentType  - Type de facturation de location (LOC::Invoice::Rent::INVOICETYPE_MONTH par défaut)
# string $file      - Chemin vers le fichier clients
#
# Returns:
# hashref|0 - Tableau contenant les résultats ou 0 si il y a une erreur
sub checkCustomers
{
    my ($countryId, $date, $type, $rentType, $file) = @_;

    if ($countryId eq '')
    {
        return 0;
    }

    unless (defined $type)
    {
        $type = LOC::Invoice::TYPE_RENT;
    }
    unless (defined $rentType)
    {
        $rentType = LOC::Invoice::Rent::INVOICETYPE_MONTH;
    }
    unless (defined $rentType)
    {
        $rentType = LOC::Invoice::Rent::INVOICETYPE_MONTH;
    }
    unless (defined $date)
    {
        $date = &getNextInvoicingDate($countryId, $rentType);
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query;
    if ($type eq LOC::Invoice::TYPE_RENT && $rentType eq LOC::Invoice::Rent::INVOICETYPE_MONTH)
    {
        # Date de fin de la période de location facturée
        my $rentalDate = &getRentalPeriodEndDate($date, $rentType);

        $query = '
(SELECT
    tbl_f_customer.CLAUTO AS `id`,
    tbl_f_customer.CLCODE AS `code`,
    tbl_f_customer.CLSTE AS `name`,
    tbl_f_customer.CLCOLLECTIF AS `accountingCode`,
    tbl_f_customer.CLNBFACT AS `invoicesNo`,
    tbl_f_customer.CLSAGE AS `isLocked`,
    tbl_f_customer.CLTMPUNLOCK AS `isTempUnlock`,
    tbl_f_contract.CONTRATAUTO AS `contract.id`,
    tbl_f_contract.CONTRATCODE AS `contract.code`,
    tbl_f_contract.AGAUTO AS `contract.agency.id`
FROM ENTETEFACTURE tbl_f_rentinvoice
LEFT JOIN CONTRAT tbl_f_contract
ON tbl_f_rentinvoice.CONTRATAUTO = tbl_f_contract.CONTRATAUTO
LEFT JOIN CLIENT tbl_f_customer
ON tbl_f_contract.CLAUTO = tbl_f_customer.CLAUTO
WHERE tbl_f_rentinvoice.ETCODE = ' . $db->quote(LOC::Invoice::STATE_NOTEXPORTED) . ')
UNION
(SELECT
    tbl_f_customer.CLAUTO AS `id`,
    tbl_f_customer.CLCODE AS `code`,
    tbl_f_customer.CLSTE AS `name`,
    tbl_f_customer.CLCOLLECTIF AS `accountingCode`,
    tbl_f_customer.CLNBFACT AS `invoicesNo`,
    tbl_f_customer.CLSAGE AS `isLocked`,
    tbl_f_customer.CLTMPUNLOCK AS `isTempUnlock`,
    tbl_f_contract.CONTRATAUTO AS `contract.id`,
    tbl_f_contract.CONTRATCODE AS `contract.code`,
    tbl_f_contract.AGAUTO AS `contract.agency.id`
FROM CONTRAT tbl_f_contract
LEFT JOIN MONTANT tbl_f_amount
ON tbl_f_contract.MOAUTO = tbl_f_amount.MOAUTO
LEFT JOIN GESTIONTRANS' . $countryId . '.TRANSPORT tbl_f_trspestim
ON (tbl_f_trspestim.TYPE = "CONTRAT"
    AND tbl_f_trspestim.VALEUR = tbl_f_contract.CONTRATAUTO
    AND (tbl_f_trspestim.ETAT NOT IN (' . $db->quote(LOC::Tariff::Transport::REDUCTIONSTATE_PENDING,
                                                     LOC::Tariff::Transport::REDUCTIONSTATE_CANCELED) . ')
        OR tbl_f_trspestim.ETAT IS NULL))
LEFT JOIN GESTIONTRANS' . $countryId . '.REMISEEX tbl_f_excepreduction
ON (tbl_f_trspestim.REMISEEX != 0 AND tbl_f_trspestim.REMISEEX = tbl_f_excepreduction.ID)
LEFT JOIN CLIENT tbl_f_customer
ON tbl_f_contract.CLAUTO = tbl_f_customer.CLAUTO
WHERE tbl_f_contract.CONTRATOK = -1
AND tbl_f_contract.SOLOAUTO = 0
AND tbl_f_contract.ETCODE IN (' . $db->quote(LOC::Contract::Rent::STATE_CONTRACT,
                                             LOC::Contract::Rent::STATE_STOPPED) . ')
AND tbl_f_contract.CONTRATDD <= ' . $db->quote($rentalDate) .'
AND tbl_f_contract.ETATFACTURE != ' . $db->quote(LOC::Contract::Rent::INVOICESTATE_FINAL) . '
AND (tbl_f_amount.MOREMISEOK = -2 OR tbl_f_amount.MOREMISEOK = 0)
AND (tbl_f_excepreduction.ETAT IN (' . $db->quote(LOC::Tariff::Transport::REDUCTIONSTATE_VALIDATED,
                                                  LOC::Tariff::Transport::REDUCTIONSTATE_REFUSED) . ')
    OR tbl_f_excepreduction.ETAT IS NULL))';
    }
    elsif ($type eq LOC::Invoice::TYPE_RENT && $rentType eq LOC::Invoice::Rent::INVOICETYPE_INTERMEDIATE)
    {
        # Récupération de la jointure et de la condition supplémentaires
        # (exclusion des clients ayant un certain mode de règlement par exemple)
        my $charac = &LOC::Characteristic::getCountryValueByCode('CLEXCLFACTI', $countryId);
        my ($join, $where);

        if ($charac ne '')
        {
            my $tabCharac = &LOC::Json::fromJson($charac);
            ($join, $where) = ($tabCharac->{'join'}, $tabCharac->{'where'});
        }

        $query = '
SELECT
    tbl_f_customer.CLAUTO AS `id`,
    tbl_f_customer.CLCODE AS `code`,
    tbl_f_customer.CLSTE AS `name`,
    tbl_f_customer.CLCOLLECTIF AS `accountingCode`,
    tbl_f_customer.CLNBFACT AS `invoicesNo`,
    tbl_f_customer.CLSAGE AS `isLocked`,
    tbl_f_customer.CLTMPUNLOCK AS `isTempUnlock`,
    tbl_f_contract.CONTRATAUTO AS `contract.id`,
    tbl_f_contract.CONTRATCODE AS `contract.code`,
    tbl_f_contract.AGAUTO AS `contract.agency.id`
FROM ENTETEFACTURE tbl_f_rentinvoice
LEFT JOIN CONTRAT tbl_f_contract
ON tbl_f_rentinvoice.CONTRATAUTO = tbl_f_contract.CONTRATAUTO
LEFT JOIN CLIENT tbl_f_customer
ON tbl_f_contract.CLAUTO = tbl_f_customer.CLAUTO
' . $join . '
WHERE tbl_f_rentinvoice.ETCODE = ' . $db->quote(LOC::Invoice::STATE_NOTEXPORTED) . '
' . $where;
    }
    elsif ($type eq LOC::Invoice::TYPE_REPAIR)
    {
        $query = '
SELECT
    tbl_f_customer.CLAUTO AS `id`,
    tbl_f_customer.CLCODE AS `code`,
    tbl_f_customer.CLSTE AS `name`,
    tbl_f_customer.CLCOLLECTIF AS `accountingCode`,
    tbl_f_customer.CLNBFACT AS `invoicesNo`,
    tbl_f_customer.CLSAGE AS `isLocked`,
    tbl_f_customer.CLTMPUNLOCK AS `isTempUnlock`,
    f_repairestimate.rpe_id AS `repairEstimate.id`,
    f_repairestimate.rpe_code AS `repairEstimate.code`,
    f_repairestimate.rpe_agc_id AS `repairEstimate.agency.id`
FROM f_repairinvoice
LEFT JOIN f_repairestimate
ON f_repairinvoice.rpi_rpe_id = f_repairestimate.rpe_id
LEFT JOIN f_repairsheet
ON f_repairestimate.rpe_rps_id = f_repairsheet.rps_id
LEFT JOIN CONTRAT tbl_f_contract
ON f_repairsheet.rps_rct_id = tbl_f_contract.CONTRATAUTO
LEFT JOIN CLIENT tbl_f_customer
ON tbl_f_contract.CLAUTO = tbl_f_customer.CLAUTO
WHERE f_repairinvoice.rpi_sta_id = ' . $db->quote(LOC::Invoice::STATE_NOTEXPORTED);
    }
    else
    {
        return 0;
    }

    my $tabCustomers = &readCustomersFile($file);
    my @tabClcodes = keys(%$tabCustomers);

    # Lancement du contrôle
    my $tabCustomersToCreateIntoSage = [];
    my $tabCustomersToUpdate = [];
    my $tabCustomersResult = $db->fetchAssoc($query, {}, 'id');
    foreach my $tabLine (values(%$tabCustomersResult))
    {
        # Clients à créer dans Sage
        if (!&LOC::Util::in_array($tabLine->{'code'}, \@tabClcodes))
        {
            push(@$tabCustomersToCreateIntoSage, {'id'                 => $tabLine->{'id'},
                                                  'code'               => $tabLine->{'code'},
                                                  'name'               => $tabLine->{'name'},
                                                  'accountingCode'     => $tabLine->{'accountingCode'},
                                                  'invoicesNo'         => $tabLine->{'invoicesNo'},
                                                  'isLocked'           => $tabLine->{'isLocked'},
                                                  'isTempUnlock'       => $tabLine->{'isTempUnlock'},
                                                  'contract.id'        => $tabLine->{'contract.id'},
                                                  'contract.code'      => $tabLine->{'contract.code'},
                                                  'contract.agency.id' => $tabLine->{'contract.agency.id'}});
        }
        # Clients dont le code collectif ou le nombre de factures est différent entre la Gestion des locations et Sage
        # Sage l'emporte
        if (&LOC::Util::in_array($tabLine->{'code'}, \@tabClcodes) &&
            ($tabLine->{'accountingCode'} ne $tabCustomers->{$tabLine->{'code'}}->{'generalAccount'} ||
             $tabLine->{'invoicesNo'} ne $tabCustomers->{$tabLine->{'code'}}->{'invoicesNo'}))
        {
            push(@$tabCustomersToUpdate, {'id'                 => $tabLine->{'id'},
                                          'code'               => $tabLine->{'code'},
                                          'name'               => $tabLine->{'name'},
                                          'accountingCode'     => $tabLine->{'accountingCode'},
                                          'accountingCode.new' => $tabCustomers->{$tabLine->{'code'}}->{'generalAccount'},
                                          'invoicesNo'         => $tabLine->{'invoicesNo'},
                                          'invoicesNo.new'     => $tabCustomers->{$tabLine->{'code'}}->{'invoicesNo'},
                                          'isLocked'           => $tabLine->{'isLocked'},
                                          'isTempUnlock'       => $tabLine->{'isTempUnlock'},
                                          'contract.id'        => $tabLine->{'contract.id'},
                                          'contract.code'      => $tabLine->{'contract.code'},
                                          'contract.agency.id' => $tabLine->{'contract.agency.id'}});
        }
    }

    return {'customersToCreate' => $tabCustomersToCreateIntoSage, 'customersToUpdate' => $tabCustomersToUpdate};
}

# Function: updateCustomers
# Met à jour les clients dans la Gestion des locations
#
# Parameters:
# string $countryId            - Identifiant du pays
# string $tabCustomersToUpdate - Tableau des clients à mettre à jour
#
# Returns:
# hashref|0 - Tableau contenant les résultats ou 0 si il y a une erreur
sub updateCustomers
{
    my ($countryId, $tabCustomersToUpdate) = @_;

    if ($countryId eq '')
    {
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    if (ref($tabCustomersToUpdate) eq '')
    {
        return 1;
    }
    if (ref($tabCustomersToUpdate) eq 'HASH')
    {
        my @tabValues = values(%$tabCustomersToUpdate);
        $tabCustomersToUpdate = \@tabValues;
    }

    my $tabUpdatedCustomers = [];

    foreach my $tabCustomer (@$tabCustomersToUpdate)
    {
        # Champ à mettre à jour
        my $tabUpdates = {'CLCOLLECTIF' => $tabCustomer->{'accountingCode.new'},
                          'CLNBFACT'    => $tabCustomer->{'invoicesNo.new'}};

        # Mise à jour du client
        my $update = $db->update('CLIENT', $tabUpdates, {'CLAUTO' => $tabCustomer->{'id'}});

        # Erreur
        if ($update == -1)
        {
            if (!$hasTransaction)
            {
                # Annule la transaction
                $db->rollBack();
            }
            return 0;
        }
        # Client réellement mis à jour
        elsif ($update > 0)
        {
            push(@$tabUpdatedCustomers, $tabCustomer);

            # Trace de blocage
            if ($tabCustomer->{'accountingCode'} ne $tabCustomer->{'accountingCode.new'})
            {
                my @tabCaller = caller(0);
                &LOC::Customer::logLockingCustomer(undef,
                                                   $tabCustomer->{'code'},
                                                   $tabCustomer->{'id'},
                                                   $tabCustomer->{'isLocked'},
                                                   $tabCustomer->{'isLocked'},
                                                   $tabCustomer->{'isTempUnlock'},
                                                   $tabCustomer->{'isTempUnlock'},
                                                   $tabCustomer->{'accountingCode'},
                                                   $tabCustomer->{'accountingCode.new'},
                                                   $tabCaller[3]);   # Nom de la fonction
            }
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    return $tabUpdatedCustomers;
}

# Function: getLastest
# Retourne la liste des dernières facturations de location
#
# Contenu du hashage :
# - date : date de la facturation ;
# - number : nombre de factures générées ;
# - amount : montant total HT des factures générées
# - rentType : type de facturation de location (INVOICETYPE_MONTH ou INVOICETYPE_INTERMEDIATE)
# - isExportable : le fichier d'export vers Sage peut être regénéré ;
# - isCancellable : la facturation peut être annulée ;
# - details : détail par agence de la facturation ;
#   * number : nombre de factures générées pour l'agence ;
#   * amount : montant total HT des factures générées pour l'agence ;
# - history : historique des opérations effectuées (si GETLASTEST_LOGS fait partie des flags) :
#   * id : identifiant de la ligne d'historique ;
#   * eventType.code : code du type d'événement ;
#   * eventType.label : libellé du type d'événement ;
#   * user.id : identifiant de l'utilisateur ;
#   * user.name : nom de l'utilisateur ;
#   * user.firstName : prénom de l'utilisateur ;
#   * user.fullName : nom complet de l'utilisateur ;
#   * user.state.id : identifiant de l'état de l'utilisateur ;
#   * invoicing.id : identifiant de la facturation ;
#   * datetime : date et heure de la trace ;
#   * content : contenu de la trace.
# - assets : liste des avoirs :
#   * id : identifiant de la facture négative ;
#   * contract.id : identifiant du contrat ;
#   * contract.code : code du contrat ;
#   * customer.id : identifiant du client ;
#   * customer.code : code du client ;
#   * amount : montant.
#
# Parameters:
# string  $countryId - Identifiant du pays
# string  $localeId  - Identifiant de la locale
# string  $date      - Date jusqu'à laquelle rechercher les facturations (6 derniers mois par défaut)
# string  $flags     - Flags de récupération des données
#
# Returns:
# hash|hashref|0 - Liste des facturations
sub getLastest
{
    my ($countryId, $localeId, $date, $flags) = @_;

    if ($countryId eq '')
    {
        return 0;
    }
    unless (defined $flags)
    {
        $flags = LOC::Invoice::GETLASTEST_BASE;
    }

    if ($date eq '')
    {
        # Calcul de la date du jour moins 6 mois
        my @tabLocaltime = localtime();
        my ($year, $month, $day) = &Date::Calc::Add_Delta_YM($tabLocaltime[5] + 1900,
                                                             $tabLocaltime[4] + 1,
                                                             $tabLocaltime[3],
                                                             0,
                                                             -6);
        $date = sprintf('%04d-%02d-01', $year, $month);
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Récupération des facturations en cours
    my $tabCheckParams = {'parameters' => {'countryId' => $countryId,
                                           'type'      => LOC::Invoice::TYPE_RENT
                                          },
                          'stateId'    => [LOC::InvoicingRequest::STATE_CREATED,
                                           LOC::InvoicingRequest::STATE_PROGRESS
                                          ]
                         };
    my $tabRentRequest = &LOC::InvoicingRequest::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabCheckParams);
    my @tabInvoicingId = ();
    foreach my $tabRequest (values(%$tabRentRequest))
    {
        my $tabParameters = &LOC::Json::fromJson($tabRequest->{'parameters'});
        push(@tabInvoicingId, $tabParameters->{'invoicingId'});
    }

    my $where = '';
    if (@tabInvoicingId > 0)
    {
        $where .= '    AND ENTETEIDFACTU NOT IN (' . $db->quote(@tabInvoicingId) . ')';
    }

    my $tabLastInvoicingDate = &getLastInvoicingDate($countryId);

    # Montants totaux par facturation
    my $query = '
SELECT
    CONCAT_WS("-", LEFT(tbl_f_rentinvoice.ENTETEIDFACTU, 4),
                   SUBSTRING(tbl_f_rentinvoice.ENTETEIDFACTU, 5, 2),
                   SUBSTRING(tbl_f_rentinvoice.ENTETEIDFACTU, 7, 2)) AS `date`,
    COUNT(*) AS `number`,
    SUM(IF(tbl_f_rentinvoice.ENTETENUMFAC IS NULL, 1, 0)) AS `unexportable`,
    SUM(tbl_f_rentinvoice.ENTETEMONTANTFR) AS `amount`,
    tbl_f_rentinvoice.ENTETETYPEFACT AS `rentType`,
    ' . $db->quote(LOC::Invoice::TYPE_RENT) . ' AS `type`,
    1 AS `isExportable`
FROM ENTETEFACTURE tbl_f_rentinvoice
WHERE tbl_f_rentinvoice.ENTETEDATEFAC >= ' . $db->quote($date) . '
    AND tbl_f_rentinvoice.ENTETEIDFACTU IS NOT NULL
' . $where . '
GROUP BY tbl_f_rentinvoice.ENTETEIDFACTU
ORDER BY tbl_f_rentinvoice.ENTETEIDFACTU DESC;';

    my $tabResult = $db->fetchAssoc($query, {}, 'date');

    # Montants par facturation et par agence
    my $query = '
SELECT
    CONCAT(tbl_f_rentinvoice.ENTETEIDFACTU, tbl_f_rentinvoice.AGAUTO) AS `id`,
    CONCAT_WS("-", LEFT(tbl_f_rentinvoice.ENTETEIDFACTU, 4),
                   SUBSTRING(tbl_f_rentinvoice.ENTETEIDFACTU, 5, 2),
                   SUBSTRING(tbl_f_rentinvoice.ENTETEIDFACTU, 7, 2)) AS `date`,
    tbl_f_rentinvoice.AGAUTO AS `agency.id`,
    COUNT(*) AS `number`,
    SUM(tbl_f_rentinvoice.ENTETEMONTANTFR) AS `amount`
FROM ENTETEFACTURE tbl_f_rentinvoice
WHERE tbl_f_rentinvoice.ENTETEDATEFAC >= ' . $db->quote($date) . '
    AND tbl_f_rentinvoice.ENTETEIDFACTU IS NOT NULL
' . $where . '
GROUP BY tbl_f_rentinvoice.ENTETEIDFACTU, tbl_f_rentinvoice.AGAUTO
ORDER BY tbl_f_rentinvoice.ENTETEIDFACTU DESC, tbl_f_rentinvoice.AGAUTO;';

    my $tabAgencyResult = $db->fetchAssoc($query, {}, 'id');
    foreach my $tabLine (values(%$tabAgencyResult))
    {
        $tabResult->{$tabLine->{'date'}}->{'id'} = &LOC::Invoice::getId($countryId, $tabLine->{'date'},
                                                                        $tabLine->{'type'});

        my $today = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE);
        $tabResult->{$tabLine->{'date'}}->{'isCancellable'} = ($tabLine->{'date'} eq $today);

        my $exportableMinAmount = &LOC::Characteristic::getCountryValueByCode('MTMININV', $countryId, $tabLine->{'date'});
        $tabResult->{$tabLine->{'date'}}->{'exportableMinAmount'} = $exportableMinAmount;

        $tabResult->{$tabLine->{'date'}}->{'details'}->{$tabLine->{'agency.id'}}->{'number'} = $tabLine->{'number'};
        $tabResult->{$tabLine->{'date'}}->{'details'}->{$tabLine->{'agency.id'}}->{'amount'} = $tabLine->{'amount'};
    }

    foreach my $date (keys(%$tabResult))
    {
        # Historique
        if ($flags & LOC::Invoice::GETLASTEST_LOGS)
        {
            my $tabHistory = &LOC::Invoice::getHistory($countryId, $date, LOC::Invoice::TYPE_RENT, $localeId);

            my $nbHistory = keys(%$tabHistory);
            if ($nbHistory > 0)
            {
                my @tabDates = keys(%$tabResult);
                if (!LOC::Util::in_array($date, \@tabDates))
                {
                    $tabResult->{$date} = {'date'          => $date,
                                           'number'        => 0,
                                           'unexportable'  => 0,
                                           'amount'        => 0,
                                           'type'          => $tabResult->{$date}->{'type'},
                                           'isExportable'  => 0,
                                           'isCancellable' => 0,
                                           'assets'        => {}};
                }
                $tabResult->{$date}->{'history'} = $tabHistory;
            }
        }

        # Avoirs
        my $query = '
SELECT
  tbl_f_rentinvoice.ENTETEAUTO AS `id`,
  tbl_f_rentinvoice.CONTRATAUTO AS `contract.id`,
  tbl_f_rentinvoice.CONTRATCODE AS `contract.code`,
  tbl_f_customer.CLAUTO AS `customer.id`,
  tbl_f_rentinvoice.CLCODE AS `customer.code`,
  tbl_f_rentinvoice.ENTETEMONTANTFR AS `amount`
FROM ENTETEFACTURE tbl_f_rentinvoice
LEFT JOIN CLIENT tbl_f_customer
ON tbl_f_rentinvoice.CLCODE = tbl_f_customer.CLCODE
WHERE tbl_f_rentinvoice.ENTETEDATEFAC = ' . $db->quote($date) . '
AND tbl_f_rentinvoice.ENTETEMONTANTFR < 0
ORDER BY tbl_f_rentinvoice.CONTRATCODE;';

        $tabResult->{$date}->{'assets'} = $db->fetchAssoc($query, {}, 'id');
    }

    # Historique de la facturation suivante (pré-contrôles)
    if ($flags & LOC::Invoice::GETLASTEST_LOGS)
    {
        my $tabNextInvoicing = &getNextInvoicingDate($countryId);

        # Traces
        my $tabHistory = &LOC::Invoice::getHistory($countryId, $tabNextInvoicing->{'date'}, LOC::Invoice::TYPE_RENT,
                                                   $localeId);

        my $nbHistory = keys(%$tabHistory);
        if ($nbHistory > 0)
        {
            my @tabDates = keys(%$tabResult);
            if (!LOC::Util::in_array($tabNextInvoicing->{'date'}, \@tabDates))
            {
                $tabResult->{$tabNextInvoicing->{'date'}} = {'date'          => $tabNextInvoicing->{'date'},
                                                             'number'        => 0,
                                                             'unexportable'  => 0,
                                                             'amount'        => 0,
                                                             'rentType'      => $tabNextInvoicing->{'rentType'},
                                                             'isExportable'  => 0,
                                                             'isCancellable' => 0,
                                                             'assets'        => {}, undef, 1};
            }
            $tabResult->{$tabNextInvoicing->{'date'}}->{'history'} = $tabHistory;
        }
    }

    return (wantarray ? %$tabResult : $tabResult);
}

# Function: insertHeader
# Insère en base de données l'entête de facture
#
# Parameters:
# string  $countryId - Identifiant du pays
# hashref $tab       - tableau des informations
#
# Returns:
# # int|0 - l'identifiant automatique si tout est ok ou 0 si il y a une erreur
sub insertHeader
{
    my ($countryId, $tab) = @_;

    # Vérification des paramètres de la fonction
    if ($countryId eq '')
    {
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $tabData = {
                   'CONTRATAUTO'         => $tab->{'contract.id'},
                   'ENTETEIDFACTU'       => $tab->{'id'},
                   'ENTETENUMFAC'        => $tab->{'document'},
                   'ENTETEDATEFAC'       => $tab->{'invoicingDate'},
                   'ENTETEORIGINE'       => $tab->{'origin'},
                   'ENTETEDATEDOC'       => $tab->{'date'},
                   'ENTETETYPEFACT'      => $tab->{'rentType'},
                   'CONTRATCODE'         => $tab->{'reference'},
                   'ENTETECONTRATDD'     => $tab->{'contract.beginDate'},
                   'ETATFACTURE'         => $tab->{'contract.invoiceState'},
                   'CLCODE'              => $tab->{'thirdParty'},
                   'ENTETE1REFCMMDE'     => $tab->{'header1'},
                   'ENTETE2'             => $tab->{'header2'},
                   'ENTETE3ADR1'         => $tab->{'header3'},
                   'ENTETE4ADR2'         => $tab->{'header4'},
                   'ENTETEAFFAIRE'       => $tab->{'affair'},
                   'ENTETECATEGORIE'     => $tab->{'accountingCategory'},
                   'ENTETECOLLECTIF'     => $tab->{'generalAccount'},
                   'ENTETEDATED'         => $tab->{'beginDate'},
                   'ENTETEDATEF'         => $tab->{'endDate'},
                   'ENTETEMONTANTFR'     => &LOC::Util::toNumber($tab->{'amount'}),
                   'ENTETEMONTANTEU'     => &LOC::Util::toNumber($tab->{'amount.euros'}),
                   'ENTETETVA'           => &LOC::Util::toNumber($tab->{'vatAmount'}),
                   'ENTETETVAEU'         => &LOC::Util::toNumber($tab->{'vatAmount.euros'}),
                   'ENTETETTC'           => &LOC::Util::toNumber($tab->{'taxesIncludedAmount'}),
                   'ENTETETTCEU'         => &LOC::Util::toNumber($tab->{'taxesIncludedAmount.euros'}),
                   'AGAUTO'              => $tab->{'agency.id'},
                   'ENTETEDATECREATION*' => 'NOW()',
                   'ENTETEABO'           => ($tab->{'amountType'} eq 'month' ? -1 : 0)
                  };
    my $invoiceHeaderId = $db->insert('ENTETEFACTURE', $tabData);

    if ($invoiceHeaderId == -1)
    {
        return 0;
    }
    return $invoiceHeaderId;
}

# Function: insertLine
# Insère en base de données une ligne de facture
#
# Parameters:
# string  $countryId       - Identifiant du pays
# string  $invoiceHeaderId - Identifiant de la facture
# hashref $tab             - tableau des informations
#
# Returns:
# # int - l'identifiant automatique si tout est ok ou 0 si il y a une erreur
sub insertLine
{
    my ($countryId, $invoiceHeaderId, $tab) = @_;

    # Vérification des paramètres de la fonction
    if ($countryId eq '' || $invoiceHeaderId eq '')
    {
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Formatage des nombres
    my $unitPrice      = (defined $tab->{'unitPrice'} ? &LOC::Util::toNumber($tab->{'unitPrice'}) : undef);
    my $unitPriceEuros = (defined $tab->{'unitPrice.euros'} ? &LOC::Util::toNumber($tab->{'unitPrice.euros'}) : undef);
    my $reduction      = (defined $tab->{'reduction'} ? &LOC::Util::toNumber($tab->{'reduction'}) : undef);
    my $VATRate1       = (defined $tab->{'VATRate1'} ? &LOC::Util::toNumber($tab->{'VATRate1'}) : undef);
    my $VATRateType1   = (defined $tab->{'VATRateType1'} ? &LOC::Util::toNumber($tab->{'VATRateType1'}) : undef);
    my $VATRate2       = (defined $tab->{'VATRate2'} ? &LOC::Util::toNumber($tab->{'VATRate2'}) : undef);
    my $VATRateType2   = (defined $tab->{'VATRateType2'} ? &LOC::Util::toNumber($tab->{'VATRateType2'}) : undef);
    my $VATRate3       = (defined $tab->{'VATRate3'} ? &LOC::Util::toNumber($tab->{'VATRate3'}) : undef);
    my $VATRateType3   = (defined $tab->{'VATRateType3'} ? &LOC::Util::toNumber($tab->{'VATRateType3'}) : undef);
    my $quantity       = (defined $tab->{'quantity'} ? &LOC::Util::toNumber($tab->{'quantity'}) : undef);

    my $invoiceLineId = $db->insert('LIGNEFACTURE', {'ENTETEAUTO'           => $invoiceHeaderId,
                                                     'LIGNEAFFAIRE'         => $tab->{'affair'},
                                                     'LIGNETIERS'           => $tab->{'thirdParty'},
                                                     'LIGNEREFARTICLE'      => $tab->{'articleRef'},
                                                     'LIGNEDESIGNATION'     => $tab->{'label'},
                                                     'LIGNEPRIX'            => $unitPrice,
                                                     'LIGNEPRIXEU'          => $unitPriceEuros,
                                                     'LIGNEREMISE'          => $reduction,
                                                     'LIGNETVA1'            => $VATRate1,
                                                     'LIGNETYPETVA1'        => $VATRateType1,
                                                     'LIGNETVA2'            => $VATRate2,
                                                     'LIGNETYPETVA2'        => $VATRateType2,
                                                     'LIGNETVA3'            => $VATRate3,
                                                     'LIGNETYPETVA3'        => $VATRateType3,
                                                     'LIGNEQTITE'           => $quantity,
                                                     'LIGNECONDITIONNEMENT' => $tab->{'packaging'},
                                                     'LIGNEDEPOT'           => $tab->{'deposite'},
                                                     'LIGNECOMMENTAIRE'     => $tab->{'labelExtra'},
                                                     'LIGNEINFOLIBRE2'      => $tab->{'freeInfo2'},
                                                     'LIGNEINFOLIBRE3'      => $tab->{'freeInfo3'},
                                                     'LIGNEINFOLIBRE4'      => $tab->{'freeInfo4'},
                                                     'LIGNEINFOLIBRE5'      => $tab->{'freeInfo5'},
                                                     'LIGNEINFOLIBRE8'      => $tab->{'freeInfo8'}});
    if ($invoiceLineId == -1)
    {
         # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }


    return $invoiceLineId;
}

# Function: intermediateInvoice
# Effectue la facturation intermédiaire et retourne le nombre de factures concernées
#
# Parameters:
# string $countryId   - Identifiant du pays
# string $invoiceDate - Date de facturation
# int    $number      - prochain numéro de pièce
# int    $requestId   - identifiant de la requête de facturation
#
# Returns:
# int - Nombre de factures concernées ou -1 en cas d'erreur
sub intermediateInvoice
{
    my ($countryId, $invoiceDate, $number, $requestId) = @_;

    # Vérification des paramètres passés
    unless ($countryId ne '' && $invoiceDate ne '' && $number ne '')
    {
        return -1;
    }

    # A-t-on le droit de lancer la facturation
    my $tabNextInvoicing = &getNextInvoicingDate($countryId, INVOICETYPE_INTERMEDIATE);
    if ($tabNextInvoicing->{'date'} ne $invoiceDate)
    {
        return -1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Tableau de résultats
    my $tabInvoicingResult = {'readyToInvoice' => 1, 'nbGeneratedDocuments' => 0};

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Factures générées à la mise en facturation des contrats
    my $tabRequestParams = {'id' => $requestId, 'nbTreatedInvoices' => 0, 'nbTotalInvoices'   => 0};
    my $nbInvoices = &flagInvoices($countryId, INVOICETYPE_INTERMEDIATE, $invoiceDate, \$number, $tabRequestParams);
    if ($nbInvoices == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return -1;
    }

    # Marquage de la facturation en effectuée
    if ($db->update('p_invoicedate', {'ivd_done' => -1}, {'ivd_cty_id' => $countryId,
                                                          'ivd_date' => $invoiceDate,
                                                          'ivd_type' => INVOICETYPE_INTERMEDIATE}, 'AUTH') == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return -1;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }

    $tabInvoicingResult->{'nbGeneratedDocuments'} = $nbInvoices;
    return $tabInvoicingResult;
}

# Function: flagInvoices
# Effectue la facturation intermédiaire et retourne le nombre de factures concernées
#
# Parameters:
# string  $countryId        - Identifiant du pays
# int     $mode             - Constantes, INVOICETYPE_MONTH (facturation mensuelle) ou INVOICETYPE_INTERMEDIATE (intermédiaire)
# string  $invoiceDate      - Date de facturation
# ref     $refNumber        - Référence du prochain numéro de pièce
# hashref $tabRequestParams - Paramètres de la requête de facturation (id : identifiant de la requête de facturation,
#                             nbTreatedInvoices : nombre de factures déjà traitées, nbTotalInvoices : nombre total
#                             de factures à traiter)
#
# Returns:
# int - Nombre de factures concernées ou -1 en cas d'erreur
sub flagInvoices
{
    my ($countryId, $mode, $invoiceDate, $refNumber, $tabRequestParams) = @_;

    # Vérification des paramètres passés
    unless ($countryId ne '' && $invoiceDate ne '' && ${$refNumber} ne '')
    {
        return -1;
    }

    unless (defined $mode)
    {
        $mode = INVOICETYPE_INTERMEDIATE;
    }

    my $requestId = $tabRequestParams->{'id'};

    # Identifiant de la facturation
    my $todayDate = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE);
    my $invoicingId = &LOC::Invoice::getId($countryId, $todayDate, LOC::Invoice::TYPE_RENT);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Récupération du préfixe des numéros de pièce
    my $docNoPrefix = &LOC::Invoice::getDocumentNoPrefix($countryId);

    # Récupération de la jointure et de la condition supplémentaires pour la facturation intermédiaire
    # (exclusion des clients ayant un certain mode de règlement par exemple)
    my ($join, $where);
    if ($mode eq INVOICETYPE_INTERMEDIATE)
    {
        my $charac = &LOC::Characteristic::getCountryValueByCode('CLEXCLFACTI', $countryId);

        if ($charac ne '')
        {
            my $tabCharac = &LOC::Json::fromJson($charac);
            ($join, $where) = ($tabCharac->{'join'}, $tabCharac->{'where'});
        }
    }

    # Récupération des factures qui n'ont pas encore été traitées
    my $query = '
SELECT
    tbl_f_rentinvoice.ENTETEAUTO AS `id`,
    tbl_f_rentinvoice.CONTRATAUTO AS `contract.id`,
    tbl_f_rentinvoice.ENTETEMONTANTFR AS `amount`
FROM ENTETEFACTURE tbl_f_rentinvoice
LEFT JOIN CONTRAT tbl_f_contract
ON tbl_f_rentinvoice.CONTRATAUTO = tbl_f_contract.CONTRATAUTO
LEFT JOIN CLIENT tbl_f_customer
ON tbl_f_contract.CLAUTO = tbl_f_customer.CLAUTO
' . $join . '
WHERE tbl_f_rentinvoice.ENTETEIDFACTU IS NULL
AND tbl_f_rentinvoice.ETCODE = ' . $db->quote(LOC::Invoice::STATE_NOTEXPORTED) . '
' . $where;

    my $tabResult = $db->fetchAssoc($query, {}, 'id');
    my $nbInvoices = 0;

    # Mise à jour de la progression de la facturation
    if ($requestId)
    {
        &LOC::InvoicingRequest::setTreatedInvoicesNo($countryId, $requestId,
                                                     $nbInvoices + $tabRequestParams->{'nbTreatedInvoices'});
        &LOC::InvoicingRequest::setTotalInvoicesNo($countryId, $requestId,
                                                   keys(%$tabResult) + $tabRequestParams->{'nbTotalInvoices'});
    }

    foreach my $invoiceId (keys(%$tabResult))
    {
        my $contractId = $tabResult->{$invoiceId}->{'contract.id'};

        # Génération de la date du document
        my $documentDate = &generateDocumentDate($mode, $invoiceDate);

        # Génération du numéro de document
        my $isDocumentExportable = &LOC::Invoice::isDocumentExportable($countryId, $tabResult->{$invoiceId}->{'amount'});
        my $documentNo;
        if ($isDocumentExportable)
        {
            $documentNo = &LOC::Invoice::generateDocumentNo($countryId, LOC::Invoice::TYPE_RENT,
                                                               $mode, $invoiceDate, ${$refNumber}, $docNoPrefix);
        }

        my $tabUpdates;
        my $tabWheres;

        # Mise à jour des J+/J- facturés
        $tabUpdates = {'day_date_invoicing*' => 'CURRENT_DATE()'};
        $tabWheres  = {'day_rct_id'          => $contractId,
                       'day_is_invoiced'     => 1,
                       'day_date_invoicing'  => undef};
        if ($db->update('f_day', $tabUpdates, $tabWheres, $schemaName) == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }

        # Mise à jour des jours de remise en état des contrats facturés
        $tabUpdates = {'crd_date_invoicing*' => 'CURRENT_DATE()'};
        $tabWheres  = {'crd_rct_id'          => $contractId,
                       'crd_sta_id'          => LOC::Contract::RepairDay::STATE_INVOICED,
                       'crd_date_invoicing'  => undef};
        if ($db->update('f_rentcontractrepairday', $tabUpdates, $tabWheres, $schemaName) == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }

        # Mise à jour des services de remise en état des contrats facturés
        $tabUpdates = {'crs_date_invoicing*' => 'CURRENT_DATE()'};
        $tabWheres  = {'crs_rct_id'          => $contractId,
                       'crs_sta_id'          => LOC::Contract::RepairService::STATE_INVOICED,
                       'crs_date_invoicing'  => undef};
        if ($db->update('f_rentcontractrepairservice', $tabUpdates, $tabWheres, $schemaName) == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }

        # Mise à jour des anciens services des contrats facturés
        $tabUpdates = {'COSERDATEFAC*' => 'CURRENT_DATE()'};
        $tabWheres  = {'CONTRATAUTO'   => $contractId,
                       'ETCODE'        => LOC::Common::RentService::STATE_BILLED,
                       'COSERDATEFAC'  => undef};
        if ($db->update('CONTRATSERVICES', $tabUpdates, $tabWheres, $schemaName) == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }

        # Mise à jour des services de location et de transport des contrats facturés
        $tabUpdates = {'rcs_date_invoicing*' => 'CURRENT_DATE()'};
        $tabWheres  = {'rcs_rct_id'          => $contractId,
                       'rcs_sta_id'          => LOC::Common::RentService::STATE_BILLED,
                       'rcs_date_invoicing'  => undef};
        if ($db->update('f_rentcontract_rentservice', $tabUpdates, $tabWheres, $schemaName) == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }

        # Champs à mettre à jour
        $tabUpdates = {
            'ENTETEIDFACTU'  => $invoicingId,
            'ENTETENUMFAC'   => $documentNo,
            'ENTETEDATEDOC'  => $documentDate,
            'ENTETETYPEFACT' => $mode,
            'ENTETEDATEFAC*' => 'CURRENT_DATE()'
        };

        # Mise à jour des factures
        if ($db->update('ENTETEFACTURE', $tabUpdates, {'ENTETEAUTO' => $invoiceId}, $schemaName) == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;
        }

        # Mise à jour de la date de clôture du contrat
        if ($db->update('CONTRAT', {'CONTRATDATECLOTURE*' => 'NOW()'}, {'CONTRATAUTO*' => $contractId}, $schemaName) == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }

        # Verrouillage des transports
        # - Types : livraison et récupération
        my $tabFilters = {
            'type'    => [LOC::Transport::TYPE_DELIVERY, LOC::Transport::TYPE_RECOVERY],
            'from-to' => {
                'type' => LOC::Transport::FROMTOTYPE_CONTRACT,
                'id'   => $contractId
            }
        };
        if (!&LOC::Transport::lock($countryId, $tabFilters, undef, undef))
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }

        # - Type : transfert inter-chantiers
        my $tabFilters = {
            'type' => [LOC::Transport::TYPE_SITESTRANSFER],
            'to'   => {
                'type' => LOC::Transport::FROMTOTYPE_CONTRACT,
                'id'   => $contractId
            }
        };
        if (!&LOC::Transport::lock($countryId, $tabFilters, undef, undef))
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }

        $nbInvoices++;

        # Mise à jour de la progression de la facturation
        if ($requestId)
        {
            &LOC::InvoicingRequest::setTreatedInvoicesNo($countryId, $requestId,
                                                         $nbInvoices + $tabRequestParams->{'nbTreatedInvoices'});
        }

        # Incrémentation du numéro
        ${$refNumber}++;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }

    return $nbInvoices;
}

# Function: monthlyInvoice
# Effectue la facturation mensuelle et retourne la liste des contrats facturés
#
# Contenu du hashage :
# -  : ;

#
# Parameters:
# string $countryId   - Identifiant du pays
# string $invoiceDate - Date de facturation
# int    $increment   - numéro d'incrémente servant de base au numéro de pièce à utiliser
# int    $requestId   - identifiant de la requête de facturation
#
# Returns:
# # int - Nombre de pièces générées
sub monthlyInvoice
{
    my ($countryId, $invoiceDate, $increment, $requestId) = @_;

    # Vérification des paramètres passés
    unless ($countryId ne '' && $invoiceDate ne '' && $increment ne '')
    {
        return -1;
    }

    # A-t-on le droit de lancer la facturation
    my $tabNextInvoicing = &getNextInvoicingDate($countryId, INVOICETYPE_MONTH);
    if ($tabNextInvoicing->{'date'} ne $invoiceDate)
    {
        return -1;
    }


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Tableau de résultats
    my $tabResult = {'readyToInvoice' => 1, 'nbGeneratedDocuments' => 0};

    # Nombre de documents générés
    my $numberGeneratedDocuments = 0;

    # début de transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Si le contrôle avant fracturation n'est pas ok, on retourne 0
    my %tabCheck = &checkContractsForMonthlyInvoicing($countryId, $invoiceDate, LOC::Invoice::Rent::GETINFOS_BASE);
    if ($tabCheck{'readyToInvoice'} == 0)
    {
        $tabResult->{'readyToInvoice'} = 0;
        return (wantarray ? %$tabResult : $tabResult);
    }

    # Dates de début et de fin de facturation ###
    my $beginMonthInvoiceDate = $tabCheck{'beginMonthInvoiceDate'};
    my $endMonthInvoiceDate = $tabCheck{'endMonthInvoiceDate'};

    # Récupération de la liste des contrats à facturer
    my $queryContractsToInvoice = '
SELECT
    CONTRAT.CONTRATAUTO AS `id`,
    CONTRAT.CONTRATCODE AS `code`
FROM CONTRAT
INNER JOIN MONTANT ON MONTANT.MOAUTO = CONTRAT.MOAUTO
LEFT JOIN GESTIONTARIF' . $countryId . '.GC_ARCHIVES GCA ON CONTRAT.CONTRATAUTO = GCA.CONTRATAUTO
LEFT JOIN GESTIONTRANS' . $countryId . '.TRANSPORT GT ON (GT.TYPE = "CONTRAT" and GT.VALEUR = CONTRAT.CONTRATAUTO
                                          and (GT.ETAT NOT IN ("' . LOC::Tariff::Transport::REDUCTIONSTATE_PENDING . '",
                                          "' . LOC::Tariff::Transport::REDUCTIONSTATE_CANCELED . '") OR GT.ETAT IS NULL))
LEFT JOIN GESTIONTRANS' . $countryId . '.REMISEEX GRE ON (GT.REMISEEX != 0 AND GT.REMISEEX=GRE.ID)
INNER JOIN CLIENT ON CLIENT.CLAUTO = CONTRAT.CLAUTO
WHERE CONTRATOK = -1
AND SOLOAUTO = 0
AND CONTRAT.ETCODE in (' . $db->quote(LOC::Contract::Rent::STATE_CONTRACT) . ',
                       ' . $db->quote(LOC::Contract::Rent::STATE_STOPPED) . ')
AND CONTRATDD <= ' . $db->quote($endMonthInvoiceDate) . '
AND (ETATFACTURE in (' . $db->quote(LOC::Contract::Rent::INVOICESTATE_FINALPARTIAL) . ',
                     ' . $db->quote(LOC::Contract::Rent::INVOICESTATE_PARTIAL) . ',
                     ' . $db->quote(LOC::Contract::Rent::INVOICESTATE_NONE) . ') OR ETATFACTURE IS NULL)
AND (MOREMISEOK = -2 OR MOREMISEEX = 0)
AND (GRE.ETAT IN (' . $db->quote(LOC::Tariff::Transport::REDUCTIONSTATE_VALIDATED,
                                 LOC::Tariff::Transport::REDUCTIONSTATE_REFUSED) . ') OR GRE.ETAT IS NULL)
AND (
     (((CLIENT.CLPROFORMA = -1 AND !(CONTRAT.CONTRATPROFORMAOK & ' . LOC::Proforma::DOCFLAG_CT_ISCUSTOMERPFREQONCREATION . '))
       OR CLIENT.CLPROFORMA = 0)
      AND !(CONTRAT.CONTRATPROFORMAOK & ' . LOC::Proforma::DOCFLAG_EXISTACTIVED . '))
     OR ((CONTRAT.CONTRATPROFORMAOK & ' . LOC::Proforma::DOCFLAG_EXISTACTIVED . ') AND
         (CONTRAT.CONTRATPROFORMAOK & ' . LOC::Proforma::DOCFLAG_ALLACTIVEDFINALIZED . ') AND
         !(CONTRAT.CONTRATPROFORMAOK & ' . LOC::Proforma::DOCFLAG_EXISTACTIVEDEXPIRED . '))
    )
ORDER BY `code`
FOR UPDATE
';

    # Tableau des contrats
    my $tabContractsToInvoice = $db->fetchList(LOC::Db::FETCH_ASSOC, $queryContractsToInvoice, {}, 'id');

    # Verrouillage des contrats pour éviter l'accès concurrent aux contrats bons à facturer
    foreach my $tabRow (values %$tabContractsToInvoice)
    {
        if ($db->update('CONTRAT', {'CONTRATVERROU' => -1}, {'CONTRATAUTO' => $tabRow->{'id'}}) == -1)
        {
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;
        }
    }
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }

    # Factures générées à la mise en facturation des contrats
    my $nbContractsToInvoice = keys(%$tabContractsToInvoice);
    my $tabRequestParams = {'id'                => $requestId,
                            'nbTreatedInvoices' => 0,
                            'nbTotalInvoices'   => $nbContractsToInvoice
                           };
    $numberGeneratedDocuments = &flagInvoices($countryId, INVOICETYPE_MONTH, $invoiceDate, \$increment,
                                              $tabRequestParams);
    if ($numberGeneratedDocuments == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return -1;
    }

    # Tableau des paramètres à passer à la fonction de facturation du contrat
    my %tabParams = {};
    $tabParams{'beginMonthInvoiceDate'} = $beginMonthInvoiceDate;
    $tabParams{'endMonthInvoiceDate'} = $endMonthInvoiceDate;

    my $tabCountryInfos = &LOC::Country::getInfos($countryId);

    # Récupération de la locale
    $tabParams{'locale.id'} = $tabCountryInfos->{'locale.id'};

    # Récupération du taux d'assurance par défaut
    $tabParams{'insurance.defaultRate'} = $tabCountryInfos->{'insurance.defaultRate'};

    # Date du jour au format MySQL
    $tabParams{'todayDate'} = &LOC::Date::getMySQLDate();

    # Récupération de la date du jour
    $tabParams{'todayDate'} = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE);
#   $tabParams{'todayDate'} = '2011-01-03';

    # Date de facture
    $tabParams{'documentDate'} = generateDocumentDate(INVOICETYPE_MONTH, $tabParams{'todayDate'});

    # Récupération du taux monétaire euro à la date de facture
    $tabParams{'euroRate'} = &LOC::Country::getEuroRate($countryId, $tabParams{'documentDate'});

    # Récupération taux de TVA à la date de facture
    $tabParams{'tabVAT'} = &LOC::Country::getVATRatesList($countryId, $tabParams{'documentDate'});

    # Centre de coût
    $tabParams{'costCenter'} = &LOC::Characteristic::getCountryValueByCode('CENTRCOUT', $countryId);

    # Libellé du dépot
    $tabParams{'deposit'} = &LOC::Characteristic::getCountryValueByCode('DEPOTSTOCK', $countryId);

    # Nombre de jours minimum à facturer (borne exclue)
    $tabParams{'minDaysToInvoice'} = &LOC::Characteristic::getCountryValueByCode('MINDAYSFACTM', $countryId);

    # Flag de mise à jour de l'alerte des BL/BR à signer
    $tabParams{'updateAlertRecoveryNote'} = &LOC::Characteristic::getCountryValueByCode('ALIMALBLBR', $countryId);

    # Liste des énergies à carburant
    $tabParams{'tabFuelEnergies'} = &LOC::MachinesFamily::getFuelEnergiesList();

    # Récupération du montant minimum de facture (hors avoirs)
    $tabParams{'invoiceType'}      = &LOC::Characteristic::getCountryValueByCode('TYPEDOCSAGE', $countryId);

    # Récupération des références articles pour la facturation
    my %tabArticleRefs = &LOC::Invoice::ArticleRef::getList($countryId, LOC::Util::GETLIST_ASSOC);

    #Construction du tableau des types de TVA indexé par code articles
    foreach my $tabRow (values %tabArticleRefs)
    {
        $tabParams{'tabArticleRefs'}{$tabRow->{'code'}}   = $tabRow->{'value'};
        $tabParams{'tabArticleVAT'}{$tabRow->{'code'}}    = $tabRow->{'vat.id'};
        $tabParams{'tabArticleLabels'}{$tabRow->{'code'}} = $tabRow->{'label'};
    }

    # Tableau des codes analytiques des agences
    $tabParams{'tabAgencyAnaCodes'} = &LOC::Agency::getAnalyticalCodesList();

    # Tableau des codes analytiques des familles commerciales
    $tabParams{'tabBusinessFamAnaCodes'} = &LOC::BusinessFamily::getAnalyticalCodesList($countryId);

    # Récupération du préfixe des numéros de pièce
    my $docNoPrefix = &LOC::Invoice::getDocumentNoPrefix($countryId);

    # début de transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Compteur d'erreur
    my $hasErrors = 0;

    # Nombre de factures traitées
    my $nbTreatedInvoices = $numberGeneratedDocuments;

    # Facturation des contrats
    foreach my $tabRow (values %$tabContractsToInvoice)
    {
        # Facturation du contrat
        my $resultContractInvoice = &contractInvoice($countryId, $tabRow->{'id'}, $increment, INVOICETYPE_MONTH,
                                                     \%tabParams, $docNoPrefix);
        if ($resultContractInvoice == -1)
        {
            $hasErrors++;
        }
        else
        {
            $numberGeneratedDocuments += ($resultContractInvoice - $increment);
            $increment = $resultContractInvoice;
        }

        # Déverrouillage du contrat après sa facturation
        if ($db->update('CONTRAT', {'CONTRATVERROU' => 0}, {'CONTRATAUTO' => $tabRow->{'id'}}) == -1)
        {
            $hasErrors++;
        }

        $nbTreatedInvoices++;

        # Mise à jour de la progression de la facturation
        if ($requestId)
        {
            &LOC::InvoicingRequest::setTreatedInvoicesNo($countryId, $requestId, $nbTreatedInvoices);
        }
    }

    # Marquage de la facturation en effectué
    if ($db->update('p_invoicedate', {'ivd_done' => -1}, {'ivd_cty_id' => $countryId,
                                                          'ivd_date'   => $invoiceDate,
                                                          'ivd_type'   => INVOICETYPE_MONTH}, 'AUTH') == -1)
    {
        $hasErrors++;
    }

    # Commit ou rollback si il y a des erreurs
    if ($hasErrors == 0)
    {
        if (!$hasTransaction)
        {
            if (!$db->commit())
            {
                $db->rollBack();
                return -1;
            }
        }
    }
    else
    {
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        # Déverrouillage des contrats verrouillés dans la transaction précédente
        foreach my $tabRow (values %$tabContractsToInvoice)
        {
            if ($db->update('CONTRAT', {'CONTRATVERROU' => 0}, {'CONTRATAUTO' => $tabRow->{'id'}}) == -1)
            {
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return -1;
            }
        }
        return -1;
    }

    $tabResult->{'nbGeneratedDocuments'} = $numberGeneratedDocuments;
    return (wantarray ? %$tabResult : $tabResult);
}

# Function: contractInvoice
# Effectue la facturation du contrat passé en paramètre
#
# Parameters:
# string  $countryId   - Identifiant du pays
# int     $contractId  - Identifiant du contrat
# int     $increment   - numéro d'incrément servant de base au numéro de pièce à utiliser
# int     $mode        - constantes, INVOICETYPE_MONTH (facturation mensuelle) ou INVOICETYPE_INTERMEDIATE (intermédiaire)
# hashref $tabparams   - tableau des paramètres nécessaires pour facturer
# string  $docNoPrefix - Préfixe des numéros de pièce
#
# Returns:
# int|-1 - Retourne le prochain numéro d'incrément de facture si la facturation s'est correctement déroulée, -1 sinon
sub contractInvoice
{
    my ($countryId, $contractId, $increment, $mode, $tabParams, $docNoPrefix) = @_;

    # Vérification des paramètres de la fonction
    if ($countryId eq '')
    {
        return -1;
    }

    # Récupération des formats Sage (quantité, prix et montant)
    my $charac = &LOC::Characteristic::getCountryValueByCode('FORMATSSAGE', $countryId);
    my $tabSageFormats = {'quantity' => 2, 'price' => 4, 'amount' => 2};
    if ($charac ne '')
    {
        $tabSageFormats = &LOC::Json::fromJson($charac);
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }
    # Locale
    my $localeId = $tabParams->{'locale.id'};
    my $locale = &LOC::Locale::getLocale($localeId);

    # Date du jour au format MySQL
    my $todayDate = $tabParams->{'todayDate'};

    # Date de facture
    my $documentDate = $tabParams->{'documentDate'};

    # Récupération du taux monétaire euro à la date de facture
    my $euroRate = $tabParams->{'euroRate'};

    # Récupération taux de TVA à la date de facture
    my $tabVAT = $tabParams->{'tabVAT'};

    # Récupération du tableau des références articles pour la facturation
    my $tabArticleVAT = $tabParams->{'tabArticleVAT'};

    # Centre de coût
    my $costCenter = $tabParams->{'costCenter'};

    # Nombre de jours minimum à facturer (borne exclue)
    my $minDaysToInvoice = $tabParams->{'minDaysToInvoice'};

    # Energies avec carburant à facturer
    my $tabFuelEnergies = $tabParams->{'tabFuelEnergies'};

    # flag pour la mise à jour de l'alerte des BL/BR à signer
    my $updateAlertRecoveryNote = $tabParams->{'updateAlertRecoveryNote'};

    # Récupération du montant minimum de facture (hors avoirs)
    my $invoiceType      = $tabParams->{'invoiceType'};

    # Indicateur qui spécifie si le contrat se termine dans le mois, 1 : vrai (par défaut), 0 : faux
    my $finishedInMonth = 1;
    # Récupération de toutes les informations du contrat
    my $flags = LOC::Contract::Rent::GETINFOS_BASE |
                LOC::Contract::Rent::GETINFOS_EXT_MACHINE |
                LOC::Contract::Rent::GETINFOS_EXT_MODEL |
                LOC::Contract::Rent::GETINFOS_INSURANCE |
                LOC::Contract::Rent::GETINFOS_APPEAL |
                LOC::Contract::Rent::GETINFOS_CUSTOMER |
                LOC::Contract::Rent::GETINFOS_EXT_TARIFFRENT |
                LOC::Contract::Rent::GETINFOS_EXT_TARIFFTRSP |
                LOC::Contract::Rent::GETINFOS_FUEL |
                LOC::Contract::Rent::GETINFOS_CLEANING |
                LOC::Contract::Rent::GETINFOS_EXT_REPAIRDAYS |
                LOC::Contract::Rent::GETINFOS_EXT_REPAIRSERVICES |
                LOC::Contract::Rent::GETINFOS_EXT_RENTSERVICES |
                LOC::Contract::Rent::GETINFOS_EXT_SITE |
                LOC::Contract::Rent::GETINFOS_RESIDUES |
                LOC::Contract::Rent::GETINFOS_VALUEDATE;
    my $tabOptions = {
        'lockDbRow' => 1
    };
    my $tabContractInfos = &LOC::Contract::Rent::getInfos($countryId, $contractId, $flags, $tabOptions);

    # Si le contrat est en état de facturation F, il n'y a rien à faire !
    if ($tabContractInfos->{'invoiceState'} eq LOC::Contract::Rent::INVOICESTATE_FINAL)
    {
        return $increment;
    }

    # Indicateur pour savoir si on doit facturer la TVA pour le client de ce contrat (oui par défaut)
    my $VATToInvoice = 1;

    # Catégorie de compte (0 devient 2 et TVA non facturée)
    my $accountingCategory = $tabContractInfos->{'customer.accountingCategory'};
    if ($accountingCategory == 0)
    {
        $accountingCategory = 2;
        $VATToInvoice = 0;
    }

    # affaire
    my $affair = $tabParams->{'tabAgencyAnaCodes'}->{$tabContractInfos->{'agency.id'}} .
                 $tabParams->{'tabBusinessFamAnaCodes'}->{$tabContractInfos->{'machine.tabInfos'}->{'businessFamily.id'}} .
                 $costCenter;

    # agence du contrat
    my $agencyId = $tabContractInfos->{'agency.id'};

    # Nombre de jours facturables dans le mois
    my $rentalDaysInMonth = &LOC::Date::getDeltaWorkingDays(
        $tabParams->{'beginMonthInvoiceDate'},
        $tabParams->{'endMonthInvoiceDate'},
        $agencyId
    );

    # Récupération des dates de facturation du contrat
    my $tabInvoiceDateInfos = &_getContractInvoiceDatesInfos($mode, $tabContractInfos, $tabParams, $tabSageFormats);

    # - Date de début de facturation
    my $beginInvoiceDate     = $tabInvoiceDateInfos->{'beginInvoiceDate'};
    my %beginInvoiceHashDate = &LOC::Date::getHashDate($beginInvoiceDate);

    # - Date de fin de facturation
    my $endInvoiceDate     = $tabInvoiceDateInfos->{'endInvoiceDate'};
    my %endInvoiceHashDate = &LOC::Date::getHashDate($endInvoiceDate);

    # - Contrat fini dans le mois ?
    $finishedInMonth = $tabInvoiceDateInfos->{'finishedInMonth'};

    # - Nb de jours à facturer sur le contrat (sans jours plus ni jours moins)
    my $nbRentalDays = $tabInvoiceDateInfos->{'nbRentalDays'};

    # Récupération des J+/J-
    my $getDays = sub {
        my ($type) = @_;

        my $tabFilters = {
            'contractId'    => $contractId,
            'isInvoiceable' => 1,
            'isInvoiced'    => 0,
            'type'          => $type
        };

        # Filtre de date supplémentaire :
        # on ne facture que les jours antérieurs à la date de fin de la période de location
        # ainsi que les jours sans date
        # lors d'une mensuelle sur les contrats non facturés ou en P
        if ($mode eq LOC::Invoice::Rent::INVOICETYPE_MONTH &&
            ($tabContractInfos->{'invoiceState'} eq LOC::Contract::Rent::INVOICESTATE_NONE ||
                $tabContractInfos->{'invoiceState'} eq LOC::Contract::Rent::INVOICESTATE_PARTIAL))
        {
            $tabFilters->{'date'} = {
                'includeEmpty' => 1,
                'values'       => [undef, $endInvoiceDate]
            };
        }

        return &LOC::Day::getList(
            $countryId,
            LOC::Util::GETLIST_ASSOC,
            $tabFilters,
            LOC::Day::GETINFOS_DURATION | LOC::Day::GETINFOS_APPLIEDTOCONTRACT
        );
    };

    my $tabAddDaysList = &$getDays(LOC::Day::TYPE_ADDITIONAL);
    my $tabDedDaysList = &$getDays(LOC::Day::TYPE_DEDUCTED);

    # Nombre de jours plus et moins à facturer
    my $getDaysCount = sub {
        my ($tabDays) = @_;

        my $count = 0;

        foreach my $day (values(%$tabDays))
        {
            $count += $day->{'invoiceDuration.duration'};
        }

        return $count;
    };

    my $additionalDays = &LOC::Util::round(&$getDaysCount($tabAddDaysList), $tabSageFormats->{'quantity'});
    my $deductedDays   = &LOC::Util::round(&$getDaysCount($tabDedDaysList), $tabSageFormats->{'quantity'});

    # Durée facturée (somme des jours à facturer en comptant les jours plus et moins à facturer
    my $durationToInvoice = $nbRentalDays + $additionalDays - $deductedDays;

    # FACTURATION MENSUELLE
    # Si le contrat ne se termine pas dans le mois et que la durée facturée est inférieure ou égale au nombre de jours
    # minimum à facturer et que le contrat ne se termine pas dans le mois, on ne facture pas le contrat et on retourne
    # le même incrément
    if ($mode eq LOC::Invoice::Rent::INVOICETYPE_MONTH &&
        $finishedInMonth == 0 &&
        $durationToInvoice <= $minDaysToInvoice)
    {
        return $increment;
    }

    # Format de date
    my $stringDate         = &LOC::Locale::getDateFormatStringById($localeId, LOC::Locale::FORMAT_DATE_NUMERIC);
    my $displayedBeginDate = &POSIX::strftime($stringDate, 0, 0, 0, $beginInvoiceHashDate{'d'},
                                                                    $beginInvoiceHashDate{'m'} - 1,
                                                                    $beginInvoiceHashDate{'y'} - 1900);
    my $displayedEndDate   = &POSIX::strftime($stringDate, 0, 0, 0, $endInvoiceHashDate{'d'},
                                                                    $endInvoiceHashDate{'m'} - 1,
                                                                    $endInvoiceHashDate{'y'} - 1900);

    # Type de tarif (month : mensuel, day : jour)
    my $amountType = $tabContractInfos->{'amountType'};

    # Récupération de l'article de location mensuel ou journalier
    my $rentArticleRef = '';
    if ($amountType eq 'day')
    {
        $rentArticleRef = 'LOCATJ';
    }
    elsif ($amountType eq 'month')
    {
        $rentArticleRef = 'LOCATM';
    }

    # Montant journalier de la location
    my $dailyBasePrice = 0;
    my $dailyFinalPrice = 0;

    # Tarification de location
    my $finalUnitPrice            = $tabContractInfos->{'unitPrice'};
    my $standardReductionPercent  = $tabContractInfos->{'rentTariff.tabInfos'}->{'standardReductionPercent'};
    my $specialReductionStatus    = $tabContractInfos->{'rentTariff.tabInfos'}->{'specialReductionStatus'};
    my $specialReductionPercent   = $tabContractInfos->{'rentTariff.tabInfos'}->{'specialReductionPercent'};
    my $specialReductionBillLabel = $tabContractInfos->{'rentTariff.tabInfos'}->{'specialReductionBillLabel'};
    my $baseAmount                = $tabContractInfos->{'rentTariff.tabInfos'}->{'baseAmount'};

    # Montant de location
    my $rentAmount    = 0;
    my $rentToInvoice = 0;
    my $rentAmountVAT = 0;
    my $rentVATRate    = $tabVAT->{$tabArticleVAT->{$rentArticleRef}}->{'value'} * 1;
    my $rentLineLabel;
    my $rentLineLabelExtra;

    # Calcul du prix de base par jour
    if ($amountType eq 'day')
    {
        $dailyBasePrice = $finalUnitPrice;
    }
    elsif ($amountType eq 'month' && $rentalDaysInMonth != 0)
    {
        $dailyBasePrice = ($finalUnitPrice / $rentalDaysInMonth);
    }
    else
    {
        $dailyBasePrice = 0;
    }
    $dailyBasePrice = &LOC::Util::round($dailyBasePrice, $tabSageFormats->{'price'});

    # Prix unitaire de location
    $dailyFinalPrice = $dailyBasePrice;

    # Remise exceptionnelle (les montants sont négatifs)
    my $specialReductionRentVATRate    = $tabVAT->{$tabArticleVAT->{'REMEX'}}->{'value'} * 1;
    my $specialReductionRentToInvoice  = 0;
    my $specialReductionRentUnitAmount = 0;
    my $specialReductionRentAmount     = 0;
    my $specialReductionRentAmountVAT  = 0;

    # Si on a des dates définies, on a de la location à facturer
    if ($beginInvoiceDate && $endInvoiceDate)
    {
        $rentToInvoice = 1;

        # Montant de location
        $rentAmount = $dailyBasePrice * $nbRentalDays;
        $rentAmount = &LOC::Util::round($rentAmount, $tabSageFormats->{'amount'});

        # Récupération du libellé en caractéristique
        my @tabRentArticleLabel = split(/\\n/, $tabParams->{'tabArticleLabels'}->{$rentArticleRef});
        my $rentArticleLabel = shift(@tabRentArticleLabel);
        my $rentArticleLabelExtra = join('\\n', @tabRentArticleLabel);

        # Libellé de la ligne de location
        $rentLineLabel = sprintf($rentArticleLabel, $displayedBeginDate, $displayedEndDate);

        # Libellé complémentaire
        $rentLineLabelExtra = sprintf($rentArticleLabelExtra,
                                      $tabContractInfos->{'model.tabInfos'}->{'tariffFamily.fullName'},
                                      $tabContractInfos->{'model.tabInfos'}->{'tariffFamily.code'},
                                      $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
                                      $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'});

        # Remise exceptionnelle
        if ($specialReductionStatus eq LOC::Tariff::Rent::REDUCTIONSTATE_VALIDATED && $specialReductionPercent != 0)
        {
            $specialReductionRentToInvoice = 1;

            if ($specialReductionPercent == 100)
            {
                # x -1 pour avoir un montant négatif
                $specialReductionRentUnitAmount = $dailyFinalPrice * -1;
            }
            else
            {
                my $standardReductionAmount = &LOC::Util::round($baseAmount * $standardReductionPercent / 100,0);
                my $specialReductionAmount  = &LOC::Util::round(($baseAmount - $standardReductionAmount) *
                                                                $specialReductionPercent / 100,0);
                if ($amountType eq 'day')
                {
                    $dailyBasePrice                  = ($baseAmount - $standardReductionAmount);
                    $specialReductionRentUnitAmount  = $specialReductionAmount * -1;
                }
                elsif ($amountType eq 'month')
                {
                    $dailyBasePrice                  = ($baseAmount - $standardReductionAmount) / $rentalDaysInMonth;
                    $specialReductionRentUnitAmount  = ($specialReductionAmount / $rentalDaysInMonth) * -1;
                }
                $dailyBasePrice = &LOC::Util::round($dailyBasePrice, $tabSageFormats->{'price'});
                $rentAmount = &LOC::Util::round($dailyBasePrice * $nbRentalDays, $tabSageFormats->{'amount'});
            }
            $specialReductionRentUnitAmount = &LOC::Util::round($specialReductionRentUnitAmount,
                                                                $tabSageFormats->{'price'});

            $specialReductionRentAmount = &LOC::Util::round($specialReductionRentUnitAmount * $nbRentalDays,
                                                            $tabSageFormats->{'amount'});

            # Si on facture de la TVA
            if ($VATToInvoice == 1)
            {
                $specialReductionRentAmountVAT = $specialReductionRentAmount * $specialReductionRentVATRate;
            }
        }

        # Si on facture de la TVA
        if ($VATToInvoice == 1)
        {
            $rentAmountVAT  = $rentAmount * $rentVATRate;
        }
    }

    # Construction du libellé complémentaire pour les jours plus et jours moins
    my $getDaysLabelExtra = sub {
        my ($tabDays) = @_;

        my @tabDates = ();
        foreach my $day (sort {$a->{'date'} cmp $b->{'date'}} values(%$tabDays))
        {
            # Pas de libellé complémentaire si au moins une des dates n'est pas remplie
            if ($day->{'date'} eq '')
            {
                return '';
            }

            my $date = $locale->getDateFormat($day->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC);

            my $label = '';
            if ($day->{'invoiceDuration.tabInfos'}->{'isDefault'})
            {
                if ($day->{'appliedToContract.id'})
                {
                    $label = $locale->t('  %s (pour le contrat %s)',
                            $date,
                            $day->{'appliedToContract.tabInfos'}->{'code'}
                        );
                }
                else
                {
                    $label = $locale->t('  %s', $date);
                }
            }
            else
            {
                if ($day->{'appliedToContract.id'})
                {
                    $label = $locale->t('  %s (%s pour le contrat %s)',
                            $date,
                            $day->{'invoiceDuration.tabInfos'}->{'label'},
                            $day->{'appliedToContract.tabInfos'}->{'code'}
                        );
                }
                else
                {
                    $label = $locale->t('  %s (%s)',
                            $date,
                            $day->{'invoiceDuration.tabInfos'}->{'label'}
                        );
                }
            }

            push(@tabDates, $label);
        }

        return join('\n', @tabDates);
    };

    # Jours plus à facturer
    my $additionalDaysVATRate   = $tabVAT->{$tabArticleVAT->{'JRSUP'}}->{'value'} * 1;
    my $additionalDaysToInvoice = 0;
    my $additionalDaysAmount    = 0;
    my $additionalDaysAmountVAT = 0;
    my $additionalDaysLabelExtra;

    if ($additionalDays != 0)
    {
        $additionalDaysToInvoice = 1;
        $additionalDaysAmount = $dailyFinalPrice * $additionalDays;
        # Si on facture de la TVA
        if ($VATToInvoice == 1)
        {
            $additionalDaysAmountVAT = $additionalDaysAmount * $additionalDaysVATRate;
        }

        # Libellé complémentaire
        $additionalDaysLabelExtra = &$getDaysLabelExtra($tabAddDaysList);
    }
    $additionalDaysAmount    = &LOC::Util::round($additionalDaysAmount, $tabSageFormats->{'amount'});

    # Jours moins à facturer
    my $deductedDaysVATRate   = $tabVAT->{$tabArticleVAT->{'JRDED'}}->{'value'} * 1;
    my $deductedDaysToInvoice = 0;
    my $deductedDaysAmount    = 0;
    my $deductedDaysAmountVAT = 0;
    my $deductedDaysLabelExtra;

    if ($deductedDays != 0)
    {
        $deductedDaysToInvoice = 1;
        $deductedDaysAmount = $dailyFinalPrice * $deductedDays;
        # Si on facture de la TVA
        if ($VATToInvoice == 1)
        {
            $deductedDaysAmountVAT = $deductedDaysAmount * $deductedDaysVATRate;
        }

        # Libellé complémentaire
        $deductedDaysLabelExtra = &$getDaysLabelExtra($tabDedDaysList);
    }
    $deductedDaysAmount    = &LOC::Util::round($deductedDaysAmount, $tabSageFormats->{'amount'});

    # Type, taux  et montant de l'assurance
    my $insuranceTypeId     = $tabContractInfos->{'insuranceType.id'};
    my $insuranceRate       = $tabContractInfos->{'insurance.rate'};
    my $isInsuranceInvoiced = $tabContractInfos->{'insurance.isInvoiced'};
    my $insuranceArticleRef = 'ASSUR';
    if ($isInsuranceInvoiced && $insuranceRate != $tabParams->{'insurance.defaultRate'})
    {
        $insuranceArticleRef = 'ASSURN';
    }
    my $insuranceVATRate    = $tabVAT->{$tabArticleVAT->{$insuranceArticleRef}}->{'value'} * 1;
    my $insuranceUnitPrice  = 0;
    my $insuranceAmount     = 0;
    my $insuranceAmountVAT  = 0;
    my $insuranceDuration   = 0;
    my $insuranceToInvoice  = 0;

    # Calcul de l'assurance
    if ($isInsuranceInvoiced)
    {
        $insuranceToInvoice = 1;

        my %insuranceAmountInfos = &LOC::Common::Rent::calculateInsurance($countryId, $tabContractInfos->{'agency.id'},
                                                                          $insuranceTypeId, $insuranceRate,
                                                                          $dailyFinalPrice,
                                                                          $beginInvoiceDate, $endInvoiceDate,
                                                                          $additionalDays, $deductedDays,
                                                                          {'round' => undef,
                                                                           'contractEndDate' => $tabContractInfos->{'endDate'}
                                                                          });
        $insuranceDuration = $insuranceAmountInfos{'duration'};

        # Si il y a la durée d'assurance est supérieure à 0, on facture de l'assurance
        if($insuranceDuration > 0)
        {
            # Montant de l'assurance
            $insuranceUnitPrice = $insuranceAmount = $insuranceAmountInfos{'amount'};

            # Si on facture de la TVA
            if ($VATToInvoice == 1)
            {
                $insuranceAmountVAT = $insuranceAmount * $insuranceVATRate;
            }

            # Arrondi de l'assurance
            $insuranceUnitPrice = &LOC::Util::round($insuranceUnitPrice, $tabSageFormats->{'price'});
            $insuranceAmount    = &LOC::Util::round($insuranceAmount, $tabSageFormats->{'amount'});
        }
        else
        {
            $insuranceToInvoice = 0;
        }
    }

    # Type, taux  et montant de la gestion de recours
    my $appealTypeId       = $tabContractInfos->{'appealType.id'};
    my $appealRate         = $tabContractInfos->{'appeal.value'};
    my $isAppealActivated  = $tabContractInfos->{'insurance.isAppealActivated'};
    my $appealArticleRef   = 'GDR';
    my $appealVATRate      = $tabVAT->{$tabArticleVAT->{$appealArticleRef}}->{'value'} * 1;
    my $appealUnitPrice    = 0;
    my $appealAmount       = 0;
    my $appealAmountVAT    = 0;
    my $appealDuration     = 0;
    my $appealToInvoice    = 0;

    # Calcul de la gestion de recours
    if ($isAppealActivated)
    {
        $appealToInvoice = 1;

        my %appealAmountInfos = &LOC::Common::Rent::calculateAppeal($countryId, $tabContractInfos->{'agency.id'},
                                                                    $appealTypeId, $appealRate,
                                                                    $dailyFinalPrice,
                                                                    $beginInvoiceDate, $endInvoiceDate,
                                                                    $additionalDays, $deductedDays, 
                                                                    {'round' => undef,
                                                                     'contractEndDate' => $tabContractInfos->{'endDate'}
                                                                    });
        $appealDuration = $appealAmountInfos{'duration'};

        # Si la durée de gestion de recours est supérieure à 0, on facture de la gestion de recours
        if($appealDuration > 0)
        {
            # Montant de la gestion de recours
            $appealUnitPrice = $appealAmount = $appealAmountInfos{'amount'};

            # Si on facture de la TVA
            if ($VATToInvoice == 1)
            {
                $appealAmountVAT = $appealAmount * $appealVATRate;
            }

            # Arrondi de l'assurance
            $appealUnitPrice = &LOC::Util::round($appealUnitPrice, $tabSageFormats->{'price'});
            $appealAmount    = &LOC::Util::round($appealAmount, $tabSageFormats->{'amount'});
        }
        else
        {
            $appealToInvoice = 0;
        }
    }

    # Tableau de la tarification transport
    my $tabTptTariff                  = $tabContractInfos->{'transportTariff.tabInfos'};
    my $specialReductionAutoValidated = 0;
    my $specialReductionNbOfMachine   = 0;

    # Transport aller à facturer
    my $deliveryVATRate                       = $tabVAT->{$tabArticleVAT->{'TRANA'}}->{'value'} * 1;
    my $deliveryToInvoice                     = 0;
    my $deliveryFinalAmount                   = 0;
    my $deliveryFinalAmountVAT                = 0;
    my $deliveryBaseAmount                    = 0;
    my $deliverySpecialReductionToInvoice     = 0;
    my $deliverySpecialReductionAmount        = 0;
    my $deliverySpecialReductionPercent       = 0;
    my $deliverySpecialReductionBillLabel     = '';
    my $deliverySpecialReductionArticleType   = 'REMEXTRA';
    my $deliverySpecialReductionLineLabel     = '';

    # Transport retour à facturer
    my $recoveryVATRate                       = $tabVAT->{$tabArticleVAT->{'TRANR'}}->{'value'} * 1;
    my $recoveryToInvoice                     = 0;
    my $recoveryFinalAmount                   = 0;
    my $recoveryFinalAmountVAT                = 0;
    my $recoveryBaseAmount                    = 0;
    my $recoverySpecialReductionToInvoice     = 0;
    my $recoverySpecialReductionAmount        = 0;
    my $recoverySpecialReductionPercent       = 0;
    my $recoverySpecialReductionBillLabel     = '';
    my $recoverySpecialReductionArticleType   = 'REMEXTRR';
    my $recoverySpecialReductionLineLabel     = '';

    # Carburant à facturer
    my $fuelVATRate   = $tabVAT->{$tabArticleVAT->{'CARBU'}}->{'value'} * 1;
    my $fuelToInvoice = 0;
    my $fuelUnitPrice = 0;
    my $fuelAmount    = 0;
    my $fuelAmountVAT = 0;

    # Participation au recyclage à facturer
    my $residuesVATRate   = $tabVAT->{$tabArticleVAT->{'DECHE'}}->{'value'} * 1;
    my $residuesToInvoice = 0;
    my $residuesUnitPrice = 0;
    my $residuesAmount    = 0;
    my $residuesAmountVAT = 0;

    # Nettoyage à facturer
    my $cleaningVATRate   = $tabVAT->{$tabArticleVAT->{'NETTO'}}->{'value'} * 1;
    my $cleaningToInvoice = 0;
    my $cleaningUnitPrice = 0;
    my $cleaningAmount    = 0;
    my $cleaningAmountVAT = 0;

    # Si il y a une remise exceptionnel de transport
    if ($tabTptTariff->{'specialReductionId'})
    {
        $specialReductionAutoValidated = $tabTptTariff->{'autoValidated'};
        $specialReductionNbOfMachine   = $tabTptTariff->{'numberOfMachines'};
    }

    # Facturation du transport aller si le contrat est dans l'état 'non facturé'
    if ($tabContractInfos->{'invoiceState'} eq LOC::Contract::Rent::INVOICESTATE_NONE)
    {
        $deliveryToInvoice   = 1;
        $deliveryBaseAmount  = &LOC::Util::round($tabTptTariff->{'deliveryAmount'}, $tabSageFormats->{'price'});
        $deliveryFinalAmount = &LOC::Util::round($tabTptTariff->{'deliveryAmount'}, $tabSageFormats->{'amount'});

        # Si il y a une estimation transport
        if ($tabTptTariff->{'id'})
        {
            $deliveryBaseAmount = &LOC::Util::round($tabTptTariff->{'baseAmount'}
                                                        - $tabTptTariff->{'standardReductionAmount'},
                                                    $tabSageFormats->{'price'});

            # Si il y a une remise exceptionnelle
            if ($tabTptTariff->{'specialReductionId'})
            {
                $deliverySpecialReductionToInvoice     = 1;
                $deliverySpecialReductionAmount        = $tabTptTariff->{'specialReductionAmount'};
                $deliverySpecialReductionPercent       = $tabTptTariff->{'specialReductionPercent'};
                $deliverySpecialReductionBillLabel     = $tabTptTariff->{'specialReductionBillLabel'};

                if ($specialReductionAutoValidated != 1 && $specialReductionNbOfMachine > 1)
                {
                    $deliverySpecialReductionLineLabel = $tabParams->{'tabArticleLabels'}->{'REMEXTRAC'};
                    $deliverySpecialReductionLineLabel =sprintf($deliverySpecialReductionLineLabel,
                                                                $deliverySpecialReductionPercent,
                                                                $specialReductionNbOfMachine);
                }
                elsif ($specialReductionAutoValidated != 1 && $specialReductionNbOfMachine == 1)
                {
                    $deliverySpecialReductionLineLabel = $tabParams->{'tabArticleLabels'}->{'REMEXTRA'};
                    $deliverySpecialReductionLineLabel = sprintf($deliverySpecialReductionLineLabel,
                                                                 $deliverySpecialReductionPercent);
                }
                elsif ($specialReductionAutoValidated == 1)
                {
                    $deliverySpecialReductionLineLabel = $tabParams->{'tabArticleLabels'}->{'REMTRA'};
                    $deliverySpecialReductionLineLabel = sprintf($deliverySpecialReductionLineLabel,
                                                                 $specialReductionNbOfMachine,
                                                                 $deliverySpecialReductionPercent);
                }
            }

            # Si le prix a été modifié manuellement, on recalcule
            if ($tabTptTariff->{'amount'} > $tabTptTariff->{'deliveryAmount'})
            {
                if ($deliveryBaseAmount != 0)
                {
                    $deliverySpecialReductionToInvoice = 1;
                    $deliverySpecialReductionPercent   = 100 - ($tabTptTariff->{'deliveryAmount'} * 100 / $deliveryBaseAmount);
                    $deliverySpecialReductionAmount    = $deliveryBaseAmount - $tabTptTariff->{'deliveryAmount'};
                    $deliverySpecialReductionBillLabel = '';
                    $deliverySpecialReductionLineLabel = $tabParams->{'tabArticleLabels'}->{'REMEXTRA'};
                    $deliverySpecialReductionLineLabel = sprintf($deliverySpecialReductionLineLabel,
                                                                 $locale->getNumberFormat($deliverySpecialReductionPercent, 2));
                }
                else
                {
                    $deliverySpecialReductionToInvoice = 0;
                }
            }
            elsif ($tabTptTariff->{'amount'} < $tabTptTariff->{'deliveryAmount'})
            {
                $deliverySpecialReductionToInvoice = 0;
                $deliveryBaseAmount = &LOC::Util::round($tabTptTariff->{'deliveryAmount'}, $tabSageFormats->{'price'});
            }

            $deliverySpecialReductionAmount = &LOC::Util::round($deliverySpecialReductionAmount,
                                                                $tabSageFormats->{'price'})
        }

        # Si on facture de la TVA
        if ($VATToInvoice == 1)
        {
            $deliveryFinalAmountVAT  = $deliveryFinalAmount * $deliveryVATRate;
        }
    }

    # Si le contrat est en arrêt et qu'il se termine avant la fin de mois, on facture le transport retour, le carburant
    # et la participation au recyclage s'il y a lieu de le faire
    if (($tabContractInfos->{'state.id'} eq LOC::Contract::Rent::STATE_STOPPED ||
            $tabContractInfos->{'state.id'} eq LOC::Contract::Rent::STATE_BILLED) &&
        $tabContractInfos->{'endDate'} le $tabParams->{'endMonthInvoiceDate'} &&
        $tabContractInfos->{'invoiceState'} ne LOC::Contract::Rent::INVOICESTATE_FINALPARTIAL)
    {
        # Transport retour
        $recoveryToInvoice   = 1;
        $recoveryBaseAmount  = &LOC::Util::round($tabTptTariff->{'recoveryAmount'}, $tabSageFormats->{'price'});
        $recoveryFinalAmount = &LOC::Util::round($tabTptTariff->{'recoveryAmount'}, $tabSageFormats->{'amount'});

        # Si il y a une estimation transport
        if ($tabTptTariff->{'id'})
        {
            $recoveryBaseAmount = &LOC::Util::round($tabTptTariff->{'baseAmount'}
                                                        - $tabTptTariff->{'standardReductionAmount'},
                                                    $tabSageFormats->{'price'});

            # Si il y a une remise exceptionnelle
            if ($tabTptTariff->{'specialReductionId'})
            {
                $recoverySpecialReductionToInvoice     = 1;
                $recoverySpecialReductionAmount        = $tabTptTariff->{'specialReductionAmount'};
                $recoverySpecialReductionPercent       = $tabTptTariff->{'specialReductionPercent'};
                $recoverySpecialReductionBillLabel     = $tabTptTariff->{'specialReductionBillLabel'};
                if ($specialReductionAutoValidated != 1 && $specialReductionNbOfMachine > 1)
                {
                    $recoverySpecialReductionLineLabel = $tabParams->{'tabArticleLabels'}->{'REMEXTRRC'};
                    $recoverySpecialReductionLineLabel = sprintf($recoverySpecialReductionLineLabel,
                                                                 $recoverySpecialReductionPercent,
                                                                 $specialReductionNbOfMachine);
                }
                elsif ($specialReductionAutoValidated != 1 && $specialReductionNbOfMachine == 1)
                {
                    $recoverySpecialReductionLineLabel = $tabParams->{'tabArticleLabels'}->{'REMEXTRR'};
                    $recoverySpecialReductionLineLabel = sprintf($recoverySpecialReductionLineLabel,
                                                                 $recoverySpecialReductionPercent);
                }
                elsif ($specialReductionAutoValidated == 1)
                {
                    $recoverySpecialReductionLineLabel = $tabParams->{'tabArticleLabels'}->{'REMTRR'};
                    $recoverySpecialReductionLineLabel = sprintf($recoverySpecialReductionLineLabel,
                                                                 $specialReductionNbOfMachine,
                                                                 $recoverySpecialReductionPercent);
                }
            }

            # Si le prix a été modifié manuellement, on recalcule
            if ($tabTptTariff->{'amount'} > $tabTptTariff->{'recoveryAmount'})
            {
                if ($recoveryBaseAmount != 0)
                {
                    $recoverySpecialReductionToInvoice = 1;
                    $recoverySpecialReductionPercent   = 100 - ($tabTptTariff->{'recoveryAmount'} * 100 / $recoveryBaseAmount);
                    $recoverySpecialReductionAmount    = $recoveryBaseAmount - $tabTptTariff->{'recoveryAmount'};
                    $recoverySpecialReductionBillLabel = '';
                    $recoverySpecialReductionLineLabel = $tabParams->{'tabArticleLabels'}->{'REMEXTRR'};
                    $recoverySpecialReductionLineLabel = sprintf($recoverySpecialReductionLineLabel,
                                                                 $locale->getNumberFormat($recoverySpecialReductionPercent, 2));
                }
                else
                {
                    $recoverySpecialReductionToInvoice = 0;
                }
            }
            elsif ($tabTptTariff->{'amount'} < $tabTptTariff->{'recoveryAmount'})
            {
                $recoverySpecialReductionToInvoice = 0;
                $recoveryBaseAmount = &LOC::Util::round($tabTptTariff->{'recoveryAmount'}, $tabSageFormats->{'price'});
            }
        }
        $recoverySpecialReductionAmount = &LOC::Util::round($recoverySpecialReductionAmount, $tabSageFormats->{'price'});

        # Carburant
        my $machineEnergy   = $tabContractInfos->{'machine.tabInfos'}->{'machinesFamily.energy'};
        if (&LOC::Util::in_array($machineEnergy, $tabFuelEnergies))
        {
            $fuelToInvoice = 1;
            $fuelUnitPrice = &LOC::Util::round($tabContractInfos->{'fuel.amount'}, $tabSageFormats->{'price'});
            $fuelAmount    = &LOC::Util::round($tabContractInfos->{'fuel.amount'}, $tabSageFormats->{'amount'});
        }


        # Participation au recyclage
        if ($tabContractInfos->{'residues.mode'} == LOC::Common::RESIDUES_MODE_PRICE)
        {
            $residuesToInvoice = 1;
            $residuesUnitPrice = $tabContractInfos->{'residues.value'};
            $residuesAmount    = $residuesUnitPrice;
        }
        $residuesUnitPrice = &LOC::Util::round($residuesUnitPrice, $tabSageFormats->{'price'});
        $residuesAmount    = &LOC::Util::round($residuesAmount, $tabSageFormats->{'amount'});

        # Nettoyage
        if ($tabContractInfos->{'cleaning.type.id'} || $tabContractInfos->{'cleaning.amount'} != 0)
        {
            $cleaningToInvoice = 1;
            $cleaningUnitPrice = $tabContractInfos->{'cleaning.amount'};
            $cleaningAmount    = $tabContractInfos->{'cleaning.amount'};
        }
        $cleaningUnitPrice = &LOC::Util::round($cleaningUnitPrice, $tabSageFormats->{'price'});
        $cleaningAmount    = &LOC::Util::round($cleaningAmount, $tabSageFormats->{'amount'});

        # Si on facture de la TVA
        if ($VATToInvoice == 1)
        {
            $recoveryFinalAmountVAT = $recoveryFinalAmount * $recoveryVATRate;
            $fuelAmountVAT          = $fuelAmount * $fuelVATRate;
            $residuesAmountVAT      = $residuesAmount * $residuesVATRate;
            $cleaningAmountVAT      = $cleaningAmount * $cleaningVATRate;
        }
    }

    # Anciens services de location et transport
    my $oldServicesVATRate   = $tabVAT->{$tabArticleVAT->{'ANCSC'}}->{'value'} * 1;
    my $oldServicesToInvoice = 0;
    my $oldServicesAmount    = 0;
    my $oldServicesAmountVAT = 0;
    my $tabOldServices       = &LOC::Contract::Rent::getOldServices($countryId, $contractId);
    foreach my $id (keys(%$tabOldServices))
    {
        if ($tabOldServices->{$id}->{'state.id'} eq LOC::Contract::Rent::SERVICESTATE_UNBILLED)
        {
            $oldServicesToInvoice = 1;
            my $amount = &LOC::Util::round($tabOldServices->{$id}->{'amount'}, $tabSageFormats->{'amount'});

            $oldServicesAmount += $amount;

            # Si on facture de la TVA
            if ($VATToInvoice == 1)
            {
                $oldServicesAmountVAT += $amount * $oldServicesVATRate;
            }
        }
    }
    $oldServicesAmount    = &LOC::Util::round($oldServicesAmount, $tabSageFormats->{'amount'});

    # nouveaux services de location et transport
    my $rentServicesToInvoice = 0;
    my $rentServicesAmount    = 0;
    my $rentServicesAmountVAT = 0;
    my $tabRentServices       = $tabContractInfos->{'tabRentServices'};
    foreach my $id (keys(%$tabRentServices))
    {
        if ($tabRentServices->{$id}->{'state.id'} eq LOC::Common::RentService::STATE_UNBILLED)
        {
            $rentServicesToInvoice = 1;
            my $amount = &LOC::Util::round($tabRentServices->{$id}->{'amount'}, $tabSageFormats->{'amount'});

            $rentServicesAmount += $amount;

            # Si on facture de la TVA
            if ($VATToInvoice == 1)
            {
                my $tva = $tabVAT->{$tabArticleVAT->{$tabRentServices->{$id}->{'rentService.type'}}}->{'value'} * 1;
                $rentServicesAmountVAT += $amount * $tva;
            }
        }
    }
    $rentServicesAmount    = &LOC::Util::round($rentServicesAmount, $tabSageFormats->{'amount'});

    # Services de remise en état
    my $repairServicesVATRate   = $tabVAT->{$tabArticleVAT->{'SERRE'}}->{'value'} * 1;
    my $repairServicesToInvoice = 0;
    my $repairServicesAmount    = 0;
    my $repairServicesAmountVAT = 0;
    my $tabRepairServices    = $tabContractInfos->{'tabRepairServices'};
    foreach my $repairSheetId (keys(%$tabRepairServices))
    {
        my $tmpTab = $tabRepairServices->{$repairSheetId};
        foreach my $rentService (@$tmpTab)
        {
            if ($rentService->{'state.id'} eq LOC::Contract::RepairService::STATE_APPROVED)
            {
                $repairServicesToInvoice = 1;
                my $amount = &LOC::Util::round($rentService->{'amount'}, $tabSageFormats->{'amount'});
                $repairServicesAmount += $amount;

                # Si on facture de la TVA
                if ($VATToInvoice == 1)
                {
                    $repairServicesAmountVAT += $amount * $repairServicesVATRate;
                }
            }
        }
    }
    $repairServicesAmount    = &LOC::Util::round($repairServicesAmount, $tabSageFormats->{'amount'});

    # Jours de remise en état
    my $repairDaysVATRate   = $tabVAT->{$tabArticleVAT->{'JRSRE'}}->{'value'} * 1;
    my $repairDaysToInvoice = 0;
    my $repairDaysAmount    = 0;
    my $repairDaysAmountVAT = 0;
    my $tabRepairDays       = $tabContractInfos->{'tabRepairDays'};
    foreach my $repairSheetId (keys(%$tabRepairDays))
    {
        my $tmpTab = $tabRepairDays->{$repairSheetId};
        foreach my $repairDay (@$tmpTab)
        {
            if ($repairDay->{'state.id'} eq LOC::Contract::RepairDay::STATE_APPROVED)
            {
                $repairDaysToInvoice = 1;

                my $unitPrice = &LOC::Util::round($repairDay->{'unitAmount'}, $tabSageFormats->{'price'});
                my $quantity  = &LOC::Util::round($repairDay->{'number'}, $tabSageFormats->{'quantity'});
                my $amount = &LOC::Util::round($unitPrice * $quantity, $tabSageFormats->{'amount'});
                $repairDaysAmount    += $amount;

                # Si on facture de la TVA
                if ($VATToInvoice == 1)
                {
                    $repairDaysAmountVAT += $amount * $repairDaysVATRate;
                }
            }
        }

    }
    $repairDaysAmount    = &LOC::Util::round($repairDaysAmount, $tabSageFormats->{'amount'});

    # Participation au recyclage
    if ($tabContractInfos->{'residues.mode'} == LOC::Common::RESIDUES_MODE_PERCENT)
    {
        $residuesToInvoice = 1;

        # montant de base du calcul
        $residuesAmount = &LOC::Common::Rent::calculateResiduesAmount($tabContractInfos->{'agency.id'}, $tabContractInfos->{'residues.mode'},
                                                                             $tabContractInfos->{'residues.value'}, $dailyFinalPrice,
                                                                             $beginInvoiceDate, $endInvoiceDate,
                                                                             $additionalDays, $deductedDays,
                                                                             ($repairDaysAmount + $repairServicesAmount),
                                                                             {'round' => undef, 
                                                                              'contractEndDate' => $tabContractInfos->{'endDate'}
                                                                             });

        # si le montant de base est supérieur à 0, on facture de la participation au recyclage
        if ($residuesAmount > 0)
        {
            # Montant de la participation au recyclage
            $residuesUnitPrice = $residuesAmount;

            # Si on facture la TVA
            if ($VATToInvoice == 1)
            {
                $residuesAmountVAT = $residuesAmount * $residuesVATRate;
            }

            # Arrondi de la participation au recyclage
            $residuesUnitPrice = &LOC::Util::round($residuesUnitPrice, $tabSageFormats->{'price'}, $tabSageFormats->{'price'} + LOC::Util::ROUND_ADDDEC);
            $residuesAmount    = &LOC::Util::round($residuesAmount, $tabSageFormats->{'amount'}, $tabSageFormats->{'amount'} + LOC::Util::ROUND_ADDDEC);
        }
        else
        {
            $residuesAmount = 0;
            $residuesToInvoice = 0;
        }
    }



    # montant total de la facture
    my $invoiceAmount      = $rentAmount + $specialReductionRentAmount + $insuranceAmount + $appealAmount +
                             $deliveryFinalAmount + $recoveryFinalAmount + $fuelAmount + $residuesAmount + $cleaningAmount +
                             $oldServicesAmount + $rentServicesAmount + $repairDaysAmount + $repairServicesAmount +
                             $additionalDaysAmount - $deductedDaysAmount;
    $invoiceAmount = &LOC::Util::round($invoiceAmount, $tabSageFormats->{'amount'});

    my $invoiceAmountEU    = $invoiceAmount * $euroRate;
    $invoiceAmountEU = &LOC::Util::round($invoiceAmountEU, $tabSageFormats->{'amount'});

    my $invoiceAmountVAT   = $rentAmountVAT + $specialReductionRentAmountVAT + $insuranceAmountVAT + $appealAmountVAT +
                             $deliveryFinalAmountVAT + $recoveryFinalAmountVAT + $fuelAmountVAT + $residuesAmountVAT +
                             $cleaningAmountVAT + $oldServicesAmountVAT + $rentServicesAmountVAT + $repairDaysAmountVAT
                             + $repairServicesAmountVAT +  $additionalDaysAmountVAT - $deductedDaysAmountVAT;
    $invoiceAmountVAT = &LOC::Util::round($invoiceAmountVAT, $tabSageFormats->{'amount'}, $tabSageFormats->{'amount'} + LOC::Util::ROUND_ADDDEC);

    my $invoiceAmountVATEU = $invoiceAmountVAT * $euroRate;
    $invoiceAmountVATEU = &LOC::Util::round($invoiceAmountVATEU, $tabSageFormats->{'amount'});

    my $log = "\n";
    $log .= "Informations generales ---------------------------------------------------------------------------\n";
    $log .= "   Code                                  : " . $tabContractInfos->{'code'} . "\n";
    $log .= '   Etat                                  : ' . $tabContractInfos->{'state.id'} . "\n";
    $log .= "   Dates contrat                         : " . $tabContractInfos->{'beginDate'} . ' -> ' .
                                                            $tabContractInfos->{'endDate'} . "\n";
    $log .= "   Etat facturation                      : " . $tabContractInfos->{'invoiceState'} . "\n";
    $log .= "   Derniere factu.                       : " . $tabContractInfos->{'lastInvoiceDate'} . "\n";
    $log .= "   Periode a facturer                    : " . $beginInvoiceDate . ' -> ' . $endInvoiceDate . "\n";
    $log .= '   Designation                           : ' . $rentLineLabel . "\n";
    $log .= '   Designation complémentaire            : ' . $rentLineLabelExtra . "\n";
    $log .= "Tarification location ---------------------------------------------------------------------------\n";
    $log .= '   Nb de jours                           : ' . $nbRentalDays . "\n";
    $log .= '   Prix de base grille                   : ' . $baseAmount . ' (' . $amountType . ")\n";
    $log .= '   Prix base jour                        : ' . $dailyBasePrice . "\n";
    $log .= '   Prix Final jour                       : ' . $dailyFinalPrice . "\n";
    $log .= '   % Rem. excep.                         : ' . $specialReductionPercent . "\n";
    $log .= '   Mtt Rem. ex. unitaire                 : ' . $specialReductionRentUnitAmount . "\n";
    $log .= '   Libelle Rem. excep.                   : ' . $specialReductionBillLabel . "\n";
    $log .= '   Prix unitaire final                   : ' . $finalUnitPrice . "\n";
    $log .= '   Etat Rem. excep                       : ' . $specialReductionStatus . "\n";
    $log .= '   Montant Location HT                   : ' . $rentAmount . "\n";
    $log .= '   Montant RE HT                         : ' . $specialReductionRentAmount . "\n";
    $log .= '   Montant TVA Location                  : ' . &LOC::Util::round($rentAmountVAT, $tabSageFormats->{'amount'}) . "\n";
    $log .= '   Montant TVA RE                        : ' . &LOC::Util::round($specialReductionRentAmountVAT, $tabSageFormats->{'amount'}) . "\n";
    $log .= '   Nb de jours moins                     : ' . $deductedDays . "\n";
    $log .= '   Montant jour moins                    : ' . $deductedDaysAmount . "\n";
    $log .= '   Montant TVA jr moins                  : ' . &LOC::Util::round($deductedDaysAmountVAT, $tabSageFormats->{'amount'}) . "\n";
    $log .= '   Nb de jours plus                      : ' . $additionalDays . "\n";
    $log .= '   Montant jour plus                     : ' . $additionalDaysAmount . "\n";
    $log .= '   Montant TVA jr plus                   : ' . &LOC::Util::round($additionalDaysAmountVAT, $tabSageFormats->{'amount'}) . "\n";
    $log .= "Assurance ---------------------------------------------------------------------------------------\n";
    $log .= '   Assurance a facturer                  : ' . $insuranceToInvoice . "\n";
    $log .= '   Duree assurance                       : ' . $insuranceDuration . "\n";
    $log .= '   Taux assurance                        : ' . $insuranceRate . "\n";
    $log .= '   Montant assurance                     : ' . $insuranceAmount . "\n";
    $log .= "Gestion de recours-------------------------------------------------------------------------------\n";
    $log .= '   Gestion de recours a facturer         : ' . $appealToInvoice . "\n";
    $log .= '   Duree gestion de recours              : ' . $appealDuration . "\n";
    $log .= '   Taux gestion de recours               : ' . $appealRate . "\n";
    $log .= '   Montant gestion de recours            : ' . $appealAmount . "\n";
    $log .= "Transport ---------------------------------------------------------------------------------------\n";
    $log .= '   Id Remise exceptionnelle              : ' . $tabTptTariff->{'specialReductionId'} . "\n";
    $log .= '   RE transport aller auto-validée       : ' . $specialReductionAutoValidated . "\n";
    $log .= '   RE Nb machines                        : ' . $specialReductionNbOfMachine . "\n";
    $log .= '   Transport aller a facturer            : ' . $deliveryToInvoice . "\n";
    $log .= '   Montant transport aller               : ' . $deliveryBaseAmount . "\n";
    $log .= '   RE transport aller a facturer         : ' . $deliverySpecialReductionToInvoice . "\n";
    $log .= '   % RE transport aller                  : ' . $deliverySpecialReductionPercent . "\n";
    $log .= '   Montant RE transport aller            : ' . $deliverySpecialReductionAmount . "\n";
    $log .= '   Lbelle RE transport aller             : ' . $deliverySpecialReductionBillLabel . "\n";
    $log .= '   Transport retour a facturer           : ' . $recoveryToInvoice . "\n";
    $log .= '   Montant transport retour              : ' . $recoveryBaseAmount . "\n";
    $log .= '   RE transport retour a facturer        : ' . $recoverySpecialReductionToInvoice . "\n";
    $log .= '   % RE transport retour                 : ' . $recoverySpecialReductionPercent . "\n";
    $log .= '   Montant RE transport retour           : ' . $recoverySpecialReductionAmount . "\n";
    $log .= '   Lbelle RE transport retour            : ' . $recoverySpecialReductionBillLabel . "\n";
    $log .= "Autres ------------------------------------------------------------------------------------------\n";
    $log .= '   Carburant a facturer                  : ' . $fuelToInvoice . "\n";
    $log .= '   Montant carburant                     : '. $fuelAmount . "\n";
    $log .= '   Residus a facturer                    : ' . $residuesToInvoice . "\n";
    $log .= '   Montant residus                       : '. $residuesAmount . "\n";
    $log .= '   Nettoyage a facturer                  : ' . $cleaningToInvoice . "\n";
    $log .= '   Montant nettoyage                     : '. $cleaningAmount . "\n";
    $log .= "Services location et transport ------------------------------------------------------------------\n";
    $log .= '   Anciens services a facturer           : ' . $oldServicesToInvoice . "\n";
    $log .= '   Montant anciens services              : '. $oldServicesAmount . "\n";
    $log .= '   Nouveaux services a facturer          : ' . $oldServicesToInvoice . "\n";
    $log .= '   Montant nouveaux services             : '. $oldServicesAmount . "\n";
    $log .= "Services et jours remise en etat ----------------------------------------------------------------\n";
    $log .= '   Services de remise en etat a facturer : ' . $repairServicesToInvoice . "\n";
    $log .= '   Montant services de remise en etat    : '. $repairServicesAmount . "\n";
    $log .= '   Jours de remise en etat a facturer    : ' . $repairDaysToInvoice . "\n";
    $log .= '   Montant jours de remise en etat       : '. $repairDaysAmount . "\n";
    $log .= "Extras ------------------------------------------------------------------------------------------\n";
    $log .= '   Code affaire                          : ' . $affair . "\n";
    $log .= "MONTANT DE LA FACTURE ---------------------------------------------------------------------------\n";
    $log .= '   Montant total facture                 : ' . $invoiceAmount . "\n";

    my $isDocumentExportable = &LOC::Invoice::isDocumentExportable($countryId, $invoiceAmount);

    # Si on génère une facture de type 'facture', on crée un numéro de facture
    my $documentNumber = &LOC::Invoice::generateDocumentNo($countryId, LOC::Invoice::TYPE_RENT,
                                                           $mode, $todayDate, $increment, $docNoPrefix);

    # Log
    $log .= "Numéro de facture -------------------------------------------------------------------------------\n";
    if ($invoiceType eq LOC::Invoice::EXPORTDOCTYPE_INVOICE)
    {
        $log .= '   N° facture : ' . ($isDocumentExportable ? $documentNumber : '[Facture non exportable]') . "\n";
    }
    else
    {
        $log .= '   Numéro de BL : ' . ($isDocumentExportable ? $documentNumber : '[BL non exportable]') . "\n";
    }

    # Origine de la facture
    # Facturation globale ou mise en facturation d'un contrat
    my $origin = ($mode eq LOC::Invoice::Rent::INVOICETYPE_MONTH ? ORIGIN_INVOICING : ORIGIN_CONTRACT);

    # Identifiant de la facturation
    my $invoicingId = undef;
    if ($mode eq LOC::Invoice::Rent::INVOICETYPE_MONTH)
    {
        $invoicingId = &LOC::Invoice::getId($countryId, $todayDate, LOC::Invoice::TYPE_RENT);
    }

    # Date de la facturation
    my $invoicingDate = ($origin eq ORIGIN_INVOICING ? $todayDate : undef);

    # Construction du tableau d'en-tête de de facture
    my %tabInvoice = {};
    $tabInvoice{'header'} =
        {'id'                        => $invoicingId,
         'contract.id'               => $tabContractInfos->{'id'},
         'contract.invoiceState'     => $tabContractInfos->{'invoiceState'},
         'document'                  => ($isDocumentExportable ? $documentNumber : undef),
         'invoicingDate'             => $invoicingDate,
         'origin'                    => $origin,
         'date'                      => $documentDate,
         'rentType'                  => ($mode eq LOC::Invoice::Rent::INVOICETYPE_MONTH ? $mode : undef),
         'reference'                 => $tabContractInfos->{'code'},
         'contract.beginDate'        => $tabContractInfos->{'beginDate'},
         'thirdParty'                => $tabContractInfos->{'customer.code'},
         'payer'                     => $tabContractInfos->{'customer.code'},
         'header1'                   => $tabContractInfos->{'orderNo'},
         'header2'                   => '',
         'header3'                   => $tabContractInfos->{'site.tabInfos'}->{'label'},
         'header4'                   => $tabContractInfos->{'site.tabInfos'}->{'locality.label'},
         'affair'                    => $affair,
         'accountingCategory'        => $accountingCategory,
         'generalAccount'            => $tabContractInfos->{'customer.controlAccount'},
         'beginDate'                 => $beginInvoiceDate,
         'endDate'                   => $endInvoiceDate,
         'amount'                    => &LOC::Util::round($invoiceAmount, 4),
         'amount.euros'              => &LOC::Util::round($invoiceAmountEU, 4),
         'vatAmount'                 => &LOC::Util::round($invoiceAmountVAT, 4),
         'vatAmount.euros'           => &LOC::Util::round($invoiceAmountVATEU, 4),
         'taxesIncludedAmount'       => &LOC::Util::round($invoiceAmount + $invoiceAmountVAT, 4),
         'taxesIncludedAmount.euros' => &LOC::Util::round($invoiceAmountEU + $invoiceAmountVATEU, 4),
         'agency.id'                 => $tabContractInfos->{'agency.id'},
         'amountType'                => $amountType};

    # Ajout des lignes de la facture
    my $cptLine = 0;
    #hash des info pour le update
    my %tabInsertLine = ();
    #hash contenant le nom de la table et des champs pour le update
    my %tabInfoDataBase = ();

    # Location
    if ($rentToInvoice == 1)
    {
        if ($VATToInvoice == 0)
        {
            $rentVATRate = 0;
        }
        $tabInvoice{'tabLines'}{$cptLine} =
            {
             'affair'          => $affair,
             'thirdParty'      => $tabContractInfos->{'customer.code'},
             'articleRef'      => $tabParams->{'tabArticleRefs'}->{$rentArticleRef},
             'label'           => $rentLineLabel,
             'unitPrice'       => $dailyBasePrice,
             'unitPrice.euros' => &LOC::Util::round($dailyBasePrice * $euroRate, $tabSageFormats->{'price'}),
             'reduction'       => 0,
             'VATRate1'        => $rentVATRate * 100,
             'VATRateType1'    => 0,
             'VATRate2'        => 0,
             'VATRateType2'    => 0,
             'VATRate3'        => 0,
             'VATRateType3'    => 0,
             'quantity'        => $nbRentalDays,
             'packaging'       => '',
             'deposite'        => $tabParams->{'deposit'},
             'labelExtra'      => $rentLineLabelExtra,
             'freeInfo2'       => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
             'freeInfo3'       => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
             'freeInfo4'       => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
             'freeInfo5'       => $agencyId,
             'freeInfo8'       => $tabContractInfos->{'machine.tabInfos'}->{'businessFamily.label'}};
        $cptLine++;
    }

    # Remise exceptionnelle de location
    if ($specialReductionRentToInvoice == 1)
    {
        if ($VATToInvoice == 0)
        {
            $specialReductionRentVATRate = 0;
        }
        $tabInvoice{'tabLines'}{$cptLine} =
            {
             'affair'          => $affair,
             'thirdParty'      => $tabContractInfos->{'customer.code'},
             'articleRef'      => $tabParams->{'tabArticleRefs'}->{'REMEX'},
             'label'           => sprintf($tabParams->{'tabArticleLabels'}->{'REMEX'},
                                          $locale->getNumberFormat($specialReductionPercent),
                                          $specialReductionBillLabel),
             'unitPrice'       => $specialReductionRentUnitAmount,
             'unitPrice.euros' => &LOC::Util::round($specialReductionRentUnitAmount * $euroRate, $tabSageFormats->{'price'}),
             'reduction'       => 0,
             'VATRate1'        => $specialReductionRentVATRate * 100,
             'VATRateType1'    => 0,
             'VATRate2'        => 0,
             'VATRateType2'    => 0,
             'VATRate3'        => 0,
             'VATRateType3'    => 0,
             'quantity'        => $nbRentalDays,
             'packaging'       => '',
             'deposite'        => $tabParams->{'deposit'},
             'labelExtra'      => '',
             'freeInfo2'       => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
             'freeInfo3'       => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
             'freeInfo4'       => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
             'freeInfo5'       => $agencyId,
             'freeInfo8'       => $tabContractInfos->{'machine.tabInfos'}->{'businessFamily.label'}};
        $cptLine++;
    }

    # Jours additionnels
    if ($additionalDaysToInvoice == 1)
    {
        if ($VATToInvoice == 0)
        {
            $additionalDaysVATRate = 0;
        }
        $tabInvoice{'tabLines'}{$cptLine} =
            {
             'affair'          => $affair,
             'thirdParty'      => $tabContractInfos->{'customer.code'},
             'articleRef'      => $tabParams->{'tabArticleRefs'}->{'JRSUP'},
             'label'           => $tabParams->{'tabArticleLabels'}->{'JRSUP'},
             'unitPrice'       => $dailyFinalPrice,
             'unitPrice.euros' => &LOC::Util::round($dailyFinalPrice * $euroRate, $tabSageFormats->{'price'}),
             'reduction'       => 0,
             'VATRate1'        => $additionalDaysVATRate * 100,
             'VATRateType1'    => 0,
             'VATRate2'        => 0,
             'VATRateType2'    => 0,
             'VATRate3'        => 0,
             'VATRateType3'    => 0,
             'quantity'        => $additionalDays,
             'packaging'       => '',
             'deposite'        => $tabParams->{'deposit'},
             'labelExtra'      => $additionalDaysLabelExtra,
             'freeInfo2'       => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
             'freeInfo3'       => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
             'freeInfo4'       => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
             'freeInfo5'       => $agencyId,
             'freeInfo8'       => $tabContractInfos->{'machine.tabInfos'}->{'businessFamily.label'}};

        # Marquage comme facturés des jours
        $tabInfoDataBase{'additionalDays'} = {
                                            'table'         => 'f_day',
                                            'state'         => 'day_is_invoiced',
                                            'id'            => 'day_id'
                                           };
        if ($origin eq ORIGIN_INVOICING)
        {
            $tabInfoDataBase{'additionalDays'}->{'invoicingDate'} = 'day_date_invoicing';
        }

        my @tabIds = keys(%$tabAddDaysList);
        $tabInsertLine{$cptLine} = {
                                    'type'       => 'additionalDays',
                                    'date'       => $invoicingDate,
                                    'externalId' => \@tabIds,
                                    'state'      => 1
                                   };

        $cptLine++;
    }

    # Jours additionnels de remise en état
    if ($repairDaysToInvoice == 1)
    {
        $tabInfoDataBase{'repairDay'} = {
                                         'table'         => 'f_rentcontractrepairday',
                                         'state'         => 'crd_sta_id',
                                         'id'            => 'crd_id',
                                         'lineId'        => 'crd_ril_id'
                                        };
        if ($origin eq ORIGIN_INVOICING)
        {
            $tabInfoDataBase{'repairDay'}->{'invoicingDate'} = 'crd_date_invoicing';
        }
        if ($VATToInvoice == 0)
        {
            $repairDaysVATRate = 0;
        }
        foreach my $repairSheetId (keys(%$tabRepairDays))
        {
            my $tmpTab = $tabRepairDays->{$repairSheetId};
            foreach my $repairDay (@$tmpTab)
            {
                if ($repairDay->{'state.id'} eq LOC::Contract::RepairService::STATE_APPROVED)
                {
                    my $unitPrice = &LOC::Util::round($repairDay->{'unitAmount'}, $tabSageFormats->{'price'});
                    my $quantity  = &LOC::Util::round($repairDay->{'number'}, $tabSageFormats->{'quantity'});

                    $tabInvoice{'tabLines'}{$cptLine} =
                        {
                         'affair'          => $affair,
                         'thirdParty'      => $tabContractInfos->{'customer.code'},
                         'articleRef'      => $tabParams->{'tabArticleRefs'}->{'JRSRE'},
                         'label'           => $tabParams->{'tabArticleLabels'}->{'JRSRE'},
                         'unitPrice'       => $unitPrice,
                         'unitPrice.euros' => &LOC::Util::round($unitPrice * $euroRate, $tabSageFormats->{'price'}),
                         'reduction'       => 0,
                         'VATRate1'        => $repairDaysVATRate * 100,
                         'VATRateType1'    => 0,
                         'VATRate2'        => 0,
                         'VATRateType2'    => 0,
                         'VATRate3'        => 0,
                         'VATRateType3'    => 0,
                         'quantity'        => $quantity,
                         'packaging'       => '',
                         'deposite'        => $tabParams->{'deposit'},
                         'labelExtra'      => '',
                         'freeInfo2'       => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
                         'freeInfo3'       => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
                         'freeInfo4'       => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
                         'freeInfo5'       => $agencyId,
                         'freeInfo8'       => $tabContractInfos->{'machine.tabInfos'}->{'businessFamily.label'}};
                    $tabInsertLine{$cptLine} = {
                                                'type'       => 'repairDay',
                                                'date'       => $invoicingDate,
                                                'externalId' => $repairDay->{'id'},
                                                'state'      => LOC::Contract::RepairDay::STATE_INVOICED
                                               };
                    $cptLine++;
                }
            }
        }
    }
    # Jours déduits
    if ($deductedDaysToInvoice == 1)
    {
        if ($VATToInvoice == 0)
        {
            $deductedDaysVATRate = 0;
        }
        $tabInvoice{'tabLines'}{$cptLine} =
            {
             'affair'          => $affair,
             'thirdParty'      => $tabContractInfos->{'customer.code'},
             'articleRef'      => $tabParams->{'tabArticleRefs'}->{'JRDED'},
             'label'           => $tabParams->{'tabArticleLabels'}->{'JRDED'},
             'unitPrice'       => $dailyFinalPrice,
             'unitPrice.euros' => &LOC::Util::round($dailyFinalPrice * $euroRate, $tabSageFormats->{'price'}),
             'reduction'       => 0,
             'VATRate1'        => $deductedDaysVATRate * 100,
             'VATRateType1'    => 0,
             'VATRate2'        => 0,
             'VATRateType2'    => 0,
             'VATRate3'        => 0,
             'VATRateType3'    => 0,
             'quantity'        => -$deductedDays,
             'packaging'       => '',
             'deposite'        => $tabParams->{'deposit'},
             'labelExtra'      => $deductedDaysLabelExtra,
             'freeInfo2'       => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
             'freeInfo3'       => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
             'freeInfo4'       => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
             'freeInfo5'       => $agencyId,
             'freeInfo8'       => $tabContractInfos->{'machine.tabInfos'}->{'businessFamily.label'}};

        # Marquage comme facturés des jours
        $tabInfoDataBase{'deductedDays'} = {
                                            'table'         => 'f_day',
                                            'state'         => 'day_is_invoiced',
                                            'id'            => 'day_id'
                                           };
        if ($origin eq ORIGIN_INVOICING)
        {
            $tabInfoDataBase{'deductedDays'}->{'invoicingDate'} = 'day_date_invoicing';
        }

        my @tabIds = keys(%$tabDedDaysList);
        $tabInsertLine{$cptLine} = {
                                    'type'       => 'deductedDays',
                                    'date'       => $invoicingDate,
                                    'externalId' => \@tabIds,
                                    'state'      => 1
                                   };

        $cptLine++;
    }

    # Assurance
    if ($insuranceToInvoice == 1)
    {
        if ($VATToInvoice == 0)
        {
            $insuranceVATRate = 0;
        }
        $tabInvoice{'tabLines'}{$cptLine} =
            {
             'affair'          => $affair,
             'thirdParty'      => $tabContractInfos->{'customer.code'},
             'articleRef'      => $tabParams->{'tabArticleRefs'}->{$insuranceArticleRef},
             'label'           => sprintf($tabParams->{'tabArticleLabels'}->{$insuranceArticleRef},
                                          $locale->getNumberFormat($insuranceRate * 100)),
             'unitPrice'       => $insuranceUnitPrice,
             'unitPrice.euros' => &LOC::Util::round($insuranceUnitPrice * $euroRate, $tabSageFormats->{'price'}),
             'reduction'       => 0,
             'VATRate1'        => $insuranceVATRate * 100,
             'VATRateType1'    => 0,
             'VATRate2'        => 0,
             'VATRateType2'    => 0,
             'VATRate3'        => 0,
             'VATRateType3'    => 0,
             'quantity'        => 1,
             'packaging'       => '',
             'deposite'        => $tabParams->{'deposit'},
             'labelExtra'      => '',
             'freeInfo2'       => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
             'freeInfo3'       => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
             'freeInfo4'       => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
             'freeInfo5'       => $agencyId,
             'freeInfo8'       => $tabContractInfos->{'machine.tabInfos'}->{'businessFamily.label'}};
        $cptLine++;
    }

    # Gestion de recours
    if ($appealToInvoice == 1)
    {
        if ($VATToInvoice == 0)
        {
            $appealVATRate = 0;
        }
        $tabInvoice{'tabLines'}{$cptLine} =
            {
             'affair'          => $affair,
             'thirdParty'      => $tabContractInfos->{'customer.code'},
             'articleRef'      => $tabParams->{'tabArticleRefs'}->{$appealArticleRef},
             'label'           => sprintf($tabParams->{'tabArticleLabels'}->{$appealArticleRef},
                                          $locale->getNumberFormat($appealRate * 100)),
             'unitPrice'       => $appealUnitPrice,
             'unitPrice.euros' => &LOC::Util::round($appealUnitPrice * $euroRate, $tabSageFormats->{'price'}),
             'reduction'       => 0,
             'VATRate1'        => $appealVATRate * 100,
             'VATRateType1'    => 0,
             'VATRate2'        => 0,
             'VATRateType2'    => 0,
             'VATRate3'        => 0,
             'VATRateType3'    => 0,
             'quantity'        => 1,
             'packaging'       => '',
             'deposite'        => $tabParams->{'deposit'},
             'labelExtra'      => '',
             'freeInfo2'       => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
             'freeInfo3'       => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
             'freeInfo4'       => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
             'freeInfo5'       => $agencyId,
             'freeInfo8'       => $tabContractInfos->{'machine.tabInfos'}->{'businessFamily.label'}};
        $cptLine++;
    }

    # Transport aller
    if ($deliveryToInvoice == 1)
    {
        if ($VATToInvoice == 0)
        {
            $deliveryVATRate = 0;
        }
        $tabInvoice{'tabLines'}{$cptLine} =
            {
             'affair'          => $affair,
             'thirdParty'      => $tabContractInfos->{'customer.code'},
             'articleRef'      => $tabParams->{'tabArticleRefs'}->{'TRANA'},
             'label'           => $tabParams->{'tabArticleLabels'}->{'TRANA'},
             'unitPrice'       => $deliveryBaseAmount,
             'unitPrice.euros' => &LOC::Util::round($deliveryBaseAmount * $euroRate, $tabSageFormats->{'price'}),
             'reduction'       => 0,
             'VATRate1'        => $deliveryVATRate * 100,
             'VATRateType1'    => 0,
             'VATRate2'        => 0,
             'VATRateType2'    => 0,
             'VATRate3'        => 0,
             'VATRateType3'    => 0,
             'quantity'        => 1,
             'packaging'       => '',
             'deposite'        => $tabParams->{'deposit'},
             'labelExtra'      => '',
             'freeInfo2'       => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
             'freeInfo3'       => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
             'freeInfo4'       => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
             'freeInfo5'       => $agencyId,
             'freeInfo8'       => $tabContractInfos->{'machine.tabInfos'}->{'businessFamily.label'}};
        $cptLine++;
    }

    # Remise exceptionnelle transport aller
    if ($deliverySpecialReductionToInvoice == 1)
    {
        if ($VATToInvoice == 0)
        {
            $deliveryVATRate = 0;
        }
        $tabInvoice{'tabLines'}{$cptLine} =
            {
             'affair'          => $affair,
             'thirdParty'      => $tabContractInfos->{'customer.code'},
             'articleRef'      => $tabParams->{'tabArticleRefs'}->{$deliverySpecialReductionArticleType},
             'label'           => $deliverySpecialReductionLineLabel,
             'unitPrice'       => $deliverySpecialReductionAmount * -1,
             'unitPrice.euros' => &LOC::Util::round($deliverySpecialReductionAmount * $euroRate * -1, 4),
             'reduction'       => 0,
             'VATRate1'        => $deliveryVATRate * 100,
             'VATRateType1'    => 0,
             'VATRate2'        => 0,
             'VATRateType2'    => 0,
             'VATRate3'        => 0,
             'VATRateType3'    => 0,
             'quantity'        => 1,
             'packaging'       => '',
             'deposite'        => $tabParams->{'deposit'},
             'labelExtra'      => '',
             'freeInfo2'       => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
             'freeInfo3'       => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
             'freeInfo4'       => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
             'freeInfo5'       => $agencyId,
             'freeInfo8'       => $tabContractInfos->{'machine.tabInfos'}->{'businessFamily.label'}};
        $cptLine++;
    }

    # Transport retour
    if ($recoveryToInvoice == 1)
    {
        if ($VATToInvoice == 0)
        {
            $recoveryVATRate = 0;
        }
        $tabInvoice{'tabLines'}{$cptLine} =
            {
             'affair'          => $affair,
             'thirdParty'      => $tabContractInfos->{'customer.code'},
             'articleRef'      => $tabParams->{'tabArticleRefs'}->{'TRANR'},
             'label'           => $tabParams->{'tabArticleLabels'}->{'TRANR'},
             'unitPrice'       => $recoveryBaseAmount,
             'unitPrice.euros' => &LOC::Util::round($recoveryBaseAmount * $euroRate, $tabSageFormats->{'price'}),
             'reduction'       => 0,
             'VATRate1'        => $recoveryVATRate * 100,
             'VATRateType1'    => 0,
             'VATRate2'        => 0,
             'VATRateType2'    => 0,
             'VATRate3'        => 0,
             'VATRateType3'    => 0,
             'quantity'        => 1,
             'packaging'       => '',
             'deposite'        => $tabParams->{'deposit'},
             'labelExtra'      => '',
             'freeInfo2'       => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
             'freeInfo3'       => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
             'freeInfo4'       => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
             'freeInfo5'       => $agencyId,
             'freeInfo8'       => $tabContractInfos->{'machine.tabInfos'}->{'businessFamily.label'}};
        $cptLine++;
    }

    # Remise exceptionnelle transport retour
    if ($recoverySpecialReductionToInvoice == 1)
    {
        if ($VATToInvoice == 0)
        {
            $recoveryVATRate = 0;
        }
        $tabInvoice{'tabLines'}{$cptLine} =
            {
             'affair'          => $affair,
             'thirdParty'      => $tabContractInfos->{'customer.code'},
             'articleRef'      => $tabParams->{'tabArticleRefs'}->{$recoverySpecialReductionArticleType},
             'label'           => $recoverySpecialReductionLineLabel,
             'unitPrice'       => $recoverySpecialReductionAmount * -1,
             'unitPrice.euros' => &LOC::Util::round($recoverySpecialReductionAmount * $euroRate * -1, 4),
             'reduction'       => 0,
             'VATRate1'        => $recoveryVATRate * 100,
             'VATRateType1'    => 0,
             'VATRate2'        => 0,
             'VATRateType2'    => 0,
             'VATRate3'        => 0,
             'VATRateType3'    => 0,
             'quantity'        => 1,
             'packaging'       => '',
             'deposite'        => $tabParams->{'deposit'},
             'labelExtra'      => '',
             'freeInfo2'       => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
             'freeInfo3'       => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
             'freeInfo4'       => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
             'freeInfo5'       => $agencyId,
             'freeInfo8'       => $tabContractInfos->{'machine.tabInfos'}->{'businessFamily.label'}};
        $cptLine++;
    }

    # Carburant
    if ($fuelToInvoice == 1)
    {
        if ($VATToInvoice == 0)
        {
            $fuelVATRate = 0;
        }
        $tabInvoice{'tabLines'}{$cptLine} =
            {
             'affair'          => $affair,
             'thirdParty'      => $tabContractInfos->{'customer.code'},
             'articleRef'      => $tabParams->{'tabArticleRefs'}->{'CARBU'},
             'label'           => $tabParams->{'tabArticleLabels'}->{'CARBU'},
             'unitPrice'       => $fuelUnitPrice,
             'unitPrice.euros' => &LOC::Util::round($fuelUnitPrice * $euroRate, $tabSageFormats->{'price'}),
             'reduction'       => 0,
             'VATRate1'        => $fuelVATRate * 100,
             'VATRateType1'    => 0,
             'VATRate2'        => 0,
             'VATRateType2'    => 0,
             'VATRate3'        => 0,
             'VATRateType3'    => 0,
             'quantity'        => 1,
             'packaging'       => '',
             'deposite'        => $tabParams->{'deposit'},
             'labelExtra'      => '',
             'freeInfo2'       => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
             'freeInfo3'       => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
             'freeInfo4'       => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
             'freeInfo5'       => $agencyId,
             'freeInfo8'       => $tabContractInfos->{'machine.tabInfos'}->{'businessFamily.label'}};
        $cptLine++;
    }

    # Nettoyage
    if ($cleaningToInvoice == 1)
    {
        if ($VATToInvoice == 0)
        {
            $cleaningVATRate = 0;
        }
        $tabInvoice{'tabLines'}{$cptLine} =
            {
             'affair'          => $affair,
             'thirdParty'      => $tabContractInfos->{'customer.code'},
             'articleRef'      => $tabParams->{'tabArticleRefs'}->{'NETTO'},
             'label'           => sprintf($tabParams->{'tabArticleLabels'}->{'NETTO'},
                                                       $tabContractInfos->{'cleaning.type.label'}),
             'unitPrice'       => $cleaningUnitPrice,
             'unitPrice.euros' => &LOC::Util::round($cleaningUnitPrice * $euroRate, $tabSageFormats->{'price'}),
             'reduction'       => 0,
             'VATRate1'        => $cleaningVATRate * 100,
             'VATRateType1'    => 0,
             'VATRate2'        => 0,
             'VATRateType2'    => 0,
             'VATRate3'        => 0,
             'VATRateType3'    => 0,
             'quantity'        => 1,
             'packaging'       => '',
             'deposite'        => $tabParams->{'deposit'},
             'labelExtra'      => '',
             'freeInfo2'       => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
             'freeInfo3'       => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
             'freeInfo4'       => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
             'freeInfo5'       => $agencyId,
             'freeInfo8'       => $tabContractInfos->{'machine.tabInfos'}->{'businessFamily.label'}};
        $cptLine++;
    }

    # Anciens services de location

    if ($oldServicesToInvoice == 1)
    {
        $tabInfoDataBase{'oldService'} = {
                                          'table'         => 'CONTRATSERVICES',
                                          'state'         => 'ETCODE',
                                          'id'            => 'COSERAUTO'
                                         };
        if ($origin eq ORIGIN_INVOICING)
        {
            $tabInfoDataBase{'oldService'}->{'invoicingDate'} = 'COSERDATEFAC';
        }
        if ($VATToInvoice == 0)
        {
            $oldServicesVATRate = 0;
        }
        foreach my $id (keys(%$tabOldServices))
        {
            if ($tabOldServices->{$id}->{'state.id'} eq LOC::Contract::Rent::SERVICESTATE_UNBILLED)
            {
                my $unitPrice = &LOC::Util::round($tabOldServices->{$id}->{'amount'}, $tabSageFormats->{'price'});

                $tabInvoice{'tabLines'}{$cptLine} =
                    {
                     'affair'          => $affair,
                     'thirdParty'      => $tabContractInfos->{'customer.code'},
                     'articleRef'      => $tabParams->{'tabArticleRefs'}->{'ANCSC'},
                     'label'           => sprintf($tabParams->{'tabArticleLabels'}->{'ANCSC'},
                                                  $tabOldServices->{$id}->{'label'}),
                     'unitPrice'       => $unitPrice,
                     'unitPrice.euros' => &LOC::Util::round($unitPrice * $euroRate, $tabSageFormats->{'price'}),
                     'reduction'       => 0,
                     'VATRate1'        => $oldServicesVATRate * 100,
                     'VATRateType1'    => 0,
                     'VATRate2'        => 0,
                     'VATRateType2'    => 0,
                     'VATRate3'        => 0,
                     'VATRateType3'    => 0,
                     'quantity'        => 1,
                     'packaging'       => '',
                     'deposite'        => $tabParams->{'deposit'},
                     'labelExtra'      => '',
                     'freeInfo2'       => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
                     'freeInfo3'       => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
                     'freeInfo4'       => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
                     'freeInfo5'       => $agencyId,
                     'freeInfo8'       => $tabContractInfos->{'machine.tabInfos'}->{'businessFamily.label'}};
               $tabInsertLine{$cptLine} = {
                                           'type'        => 'oldService',
                                           'date'       => $invoicingDate,
                                           'externalId' => $id,
                                           'state'      => LOC::Contract::Rent::SERVICESTATE_BILLED
                                          };

              $cptLine++;
            }
        }
    }

    # Nouveaux services de location

    if ($rentServicesToInvoice == 1)
    {
        $tabInfoDataBase{'rentServices'} = {
                                            'table'         => 'f_rentcontract_rentservice',
                                            'state'         => 'rcs_sta_id',
                                            'id'            => 'rcs_id'
                                           };
        if ($origin eq ORIGIN_INVOICING)
        {
            $tabInfoDataBase{'rentServices'}->{'invoicingDate'} = 'rcs_date_invoicing';
        }
        foreach my $id (keys(%$tabRentServices))
        {
            if ($tabRentServices->{$id}->{'state.id'} eq LOC::Common::RentService::STATE_UNBILLED)
            {
                my $articleType = $tabRentServices->{$id}->{'rentService.type'};
                my $tmpVATRate  = $tabVAT->{$tabArticleVAT->{$articleType}}->{'value'};
                if ($VATToInvoice == 0)
                {
                    $tmpVATRate = 0;
                }
                my $unitPrice = &LOC::Util::round($tabRentServices->{$id}->{'amount'}, $tabSageFormats->{'price'});

                $tabInvoice{'tabLines'}{$cptLine} =
                    {
                     'affair'          => $affair,
                     'thirdParty'      => $tabContractInfos->{'customer.code'},
                     'articleRef'      => $tabParams->{'tabArticleRefs'}->{$articleType},
                     'label'           => sprintf($tabParams->{'tabArticleLabels'}->{$articleType},
                                                  $tabRentServices->{$id}->{'rentService.name'} . ' ' .
                                                  $tabRentServices->{$id}->{'comment'}),
                     'unitPrice'       => $unitPrice,
                     'unitPrice.euros' => &LOC::Util::round($unitPrice * $euroRate, $tabSageFormats->{'price'}),
                     'reduction'       => 0,
                     'VATRate1'        => $tmpVATRate * 100,
                     'VATRateType1'    => 0,
                     'VATRate2'        => 0,
                     'VATRateType2'    => 0,
                     'VATRate3'        => 0,
                     'VATRateType3'    => 0,
                     'quantity'        => 1,
                     'packaging'       => '',
                     'deposite'        => $tabParams->{'deposit'},
                     'labelExtra'      => '',
                     'freeInfo2'       => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
                     'freeInfo3'       => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
                     'freeInfo4'       => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
                     'freeInfo5'       => $agencyId,
                     'freeInfo8'       => $tabContractInfos->{'machine.tabInfos'}->{'businessFamily.label'}};
                $tabInsertLine{$cptLine} = {
                                            'type'       => 'rentServices',
                                            'date'       => $invoicingDate,
                                            'externalId' => $id,
                                            'state'      => LOC::Common::RentService::STATE_BILLED
                                           };

                $cptLine++;
            }
        }
    }

    # Services de remise en état

    if ($repairServicesToInvoice == 1)
    {
        $tabInfoDataBase{'repairService'}    = {
                                                'table'         => 'f_rentcontractrepairservice',
                                                'state'         => 'crs_sta_id',
                                                'id'            => 'crs_id',
                                                'lineId'        => 'crs_ril_id'
                                               };
        if ($origin eq ORIGIN_INVOICING)
        {
            $tabInfoDataBase{'repairService'}->{'invoicingDate'} = 'crs_date_invoicing';
        }
        # Ajout d'une ligne de commentaire
        $tabInvoice{'tabLines'}{$cptLine} =
            {
             'type'            => 'comment',
             'thirdParty'      => $tabContractInfos->{'customer.code'},
             'label'           => 'Remise en état',
             'freeInfo2'       => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
             'freeInfo3'       => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
             'freeInfo4'       => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
             'freeInfo5'       => $agencyId,
             'freeInfo8'       => $tabContractInfos->{'machine.tabInfos'}->{'businessFamily.label'}};
        $cptLine++;

        if ($VATToInvoice == 0)
        {
            $repairServicesVATRate = 0;
        }
        foreach my $repairSheetId (keys(%$tabRepairServices))
        {
            my $tmpTab = $tabRepairServices->{$repairSheetId};
            foreach my $rentService (@$tmpTab)
            {
                if ($rentService->{'state.id'} eq LOC::Contract::RepairService::STATE_APPROVED)
                {
                    my $unitPrice = &LOC::Util::round($rentService->{'unitAmount'}, $tabSageFormats->{'price'});
                    my $quantity  = &LOC::Util::round($rentService->{'quantity'}, $tabSageFormats->{'quantity'});

                    $tabInvoice{'tabLines'}{$cptLine} =
                        {
                         'affair'          => $affair,
                         'thirdParty'      => $tabContractInfos->{'customer.code'},
                         'articleRef'      => $tabParams->{'tabArticleRefs'}->{'SERRE'},
                         'label'           => sprintf('  ' . $tabParams->{'tabArticleLabels'}->{'SERRE'},
                                                      $rentService->{'label'}),
                         'unitPrice'       => $unitPrice,
                         'unitPrice.euros' => &LOC::Util::round($unitPrice * $euroRate, $tabSageFormats->{'price'}),
                         'reduction'       => 0,
                         'VATRate1'        => $repairServicesVATRate * 100,
                         'VATRateType1'    => 0,
                         'VATRate2'        => 0,
                         'VATRateType2'    => 0,
                         'VATRate3'        => 0,
                         'VATRateType3'    => 0,
                         'quantity'        => $quantity,
                         'packaging'       => '',
                         'deposite'        => $tabParams->{'deposit'},
                         'labelExtra'      => '',
                         'freeInfo2'       => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
                         'freeInfo3'       => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
                         'freeInfo4'       => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
                         'freeInfo5'       => $agencyId,
                         'freeInfo8'       => $tabContractInfos->{'machine.tabInfos'}->{'businessFamily.label'}};
                    $tabInsertLine{$cptLine} = {'type'       => 'repairService',
                                                'date'       => $invoicingDate,
                                                'externalId' => $rentService->{'id'},
                                                'state'      => LOC::Contract::RepairService::STATE_INVOICED
                                               };
                    $cptLine++;
                }
            }
        }
    }

    # Participation au recyclage
    if ($residuesToInvoice == 1)
    {
        if ($VATToInvoice == 0)
        {
            $residuesVATRate = 0;
        }
        $tabInvoice{'tabLines'}{$cptLine} =
            {
             'affair'          => $affair,
             'thirdParty'      => $tabContractInfos->{'customer.code'},
             'articleRef'      => $tabParams->{'tabArticleRefs'}->{'DECHE'},
             'label'           => $tabParams->{'tabArticleLabels'}->{'DECHE'},
             'unitPrice'       => $residuesUnitPrice,
             'unitPrice.euros' => &LOC::Util::round($residuesUnitPrice * $euroRate, $tabSageFormats->{'price'}),
             'reduction'       => 0,
             'VATRate1'        => $residuesVATRate * 100,
             'VATRateType1'    => 0,
             'VATRate2'        => 0,
             'VATRateType2'    => 0,
             'VATRate3'        => 0,
             'VATRateType3'    => 0,
             'quantity'        => 1,
             'packaging'       => '',
             'deposite'        => $tabParams->{'deposit'},
             'labelExtra'      => '',
             'freeInfo2'       => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
             'freeInfo3'       => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
             'freeInfo4'       => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
             'freeInfo5'       => $agencyId,
             'freeInfo8'       => $tabContractInfos->{'machine.tabInfos'}->{'businessFamily.label'}};
        $cptLine++;
    }

    # Enregistrement de la facture seulement si elle contient des lignes
    if ($cptLine > 0)
    {
        #hash contenant la ligne de factu
        my %tabLine = {};

        # Insertion de l'en-tête de facture
        my $headerId = &insertHeader($countryId, $tabInvoice{'header'});
        if ($headerId == 0)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;
        }

        # Insertion des lignes
        for (my $i = 0; $i < $cptLine; $i++)
        {
            my $lineId = &insertLine($countryId, $headerId, $tabInvoice{'tabLines'}{$i});
            if ($lineId == 0)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return -1;
            }

            $tabLine{$i} = $lineId;
        }

        # Si le montant de la facture atteint le minimum requis (hors avoirs), insertion de la facture
        if ($isDocumentExportable)
        {
            # Incrémentation du numéro de pièce
            $increment++;
        }

        # Update spécifique même si la facture n'est pas générée
        foreach my $line (keys(%tabInsertLine))
        {
            my $tableName = $tabInfoDataBase{$tabInsertLine{$line}->{'type'}}->{'table'};
            my $idName    = $tabInfoDataBase{$tabInsertLine{$line}->{'type'}}->{'id'};

            if (defined $tableName && defined $idName)
            {
                # Tableau des mises à jour
                my $tabUpdates = {};

                my $lineIdName        = $tabInfoDataBase{$tabInsertLine{$line}->{'type'}}->{'lineId'};
                my $stateName         = $tabInfoDataBase{$tabInsertLine{$line}->{'type'}}->{'state'};
                my $invoicingDateName = $tabInfoDataBase{$tabInsertLine{$line}->{'type'}}->{'invoicingDate'};

                if (defined $lineIdName && defined $tabLine{$line})
                {
                    $tabUpdates->{$lineIdName} = $tabLine{$line};
                }
                if (defined $stateName)
                {
                    $tabUpdates->{$stateName} = $tabInsertLine{$line}->{'state'};
                }
                if (defined $invoicingDateName)
                {
                    $tabUpdates->{$invoicingDateName} = $tabInsertLine{$line}->{'date'};
                }

                # Condition
                my $tabWhere = {$idName => $tabInsertLine{$line}->{'externalId'}};

                if ($db->update($tableName, $tabUpdates, $tabWhere) == -1)
                {
                    # Annule la transaction
                    if (!$hasTransaction)
                    {
                        $db->rollBack();
                    }
                    return -1;
                }
            }
        }

        # Log
        &LOC::Log::writeFileLog('invoice', $log);

        # Trace sur le contrat
        my $tabTraces = [{'code' => LOC::Log::EventType::GENRENTINVOICE, 'content' => ''}];
        if (&LOC::Log::writeDbFunctionalityLogs('CONTRAT', $contractId, $tabTraces, {'db' => $db}) == 0)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;
        }
    }

    # Mise à jour de l'état facture du contrat
    my $newInvoiceState = '';
    if ($tabContractInfos->{'state.id'} eq LOC::Contract::Rent::STATE_CONTRACT)
    {
        $newInvoiceState = LOC::Contract::Rent::INVOICESTATE_PARTIAL;
    }
    if ($tabContractInfos->{'state.id'} eq LOC::Contract::Rent::STATE_STOPPED)
    {
        if ($tabContractInfos->{'endDate'} gt $tabParams->{'endMonthInvoiceDate'})
        {
            $newInvoiceState = LOC::Contract::Rent::INVOICESTATE_PARTIAL;
        }
        else
        {
            $newInvoiceState = LOC::Contract::Rent::INVOICESTATE_FINALPARTIAL;
        }
    }
    if ($tabContractInfos->{'state.id'} eq LOC::Contract::Rent::STATE_BILLED)
    {
        $newInvoiceState = LOC::Contract::Rent::INVOICESTATE_FINAL;
    }

    # Calcul du dernier jour de location facturé pour le champ CONTRATDATEFACTURE
    my $lastRentDayInvoiced = $endInvoiceDate;
    if ($tabContractInfos->{'endDate'} < $endInvoiceDate)
    {
        $lastRentDayInvoiced = $tabContractInfos->{'endDate'};
    }

    my $tabContractUpdates = {
        'ETATFACTURE'           => $newInvoiceState,
        'ANCIENETATFACTURE'     => $tabContractInfos->{'invoiceState'},
        'CONTRATJOURSPLUS*'     => 'CONTRATJOURSPLUS - ' . $additionalDays,
        'CONTRATJOURSMOINS*'    => 'CONTRATJOURSMOINS - ' . $deductedDays,
        'CONTRATJOURPLUSFACT*'  => 'CONTRATJOURPLUSFACT + ' . $additionalDays,
        'CONTRATJOURMOINSFACT*' => 'CONTRATJOURMOINSFACT + ' . $deductedDays
    };
    if (defined $lastRentDayInvoiced)
    {
        $tabContractUpdates->{'CONTRATDATEFACTURE'} = $lastRentDayInvoiced;
    }
    if ($db->update('CONTRAT', $tabContractUpdates, {'CONTRATAUTO' => $contractId}) == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return -1;
    }

    # Mise à jour de l'alerte 28 'ALFIR' BL/BR à signer pour le Bon de retour
    if ($updateAlertRecoveryNote == 1 && $newInvoiceState eq LOC::Contract::Rent::INVOICESTATE_FINAL &&
        $tabContractInfos->{'customer.deliveryRecoveryNotesRequired'} == 1)
    {
        if (&LOC::Alert::setDeltaCounterAlert(28, $agencyId, +1) == 0)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;
        };
    }

    # Mise à jour des transports du contrat si l'état de facturation a changé
    if ($newInvoiceState ne $tabContractInfos->{'invoiceState'})
    {
        # Changements pouvant impacter le transport ? => on le lui communique
        &LOC::Transport::onExternalChanges($countryId, {
            'contract' => {
                'id'    => $contractId,
                'event' => 'update',
                'props' => {
                    'invoiceState' => {'old' => $tabContractInfos->{'invoiceState'}, 'new' => $newInvoiceState}
                }
            }
        }, undef, undef, undef);
    }

    # Commit la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }

    return $increment;
}

# Function: generateDocumentDate
# Génère la date de facture en fonction de la date de facturation
#
# Parameters:
# string  $rentType - Type de la facturation de location (INVOICETYPE_MONTH ou INVOICETYPE_INTERMEDIATE)
# string  $date - Date de la facturation
#
# Returns:
# string - Date de la facture (au format SQL)
sub generateDocumentDate
{
    my ($rentType, $date) = @_;

    unless (defined $rentType)
    {
        $rentType = INVOICETYPE_MONTH;
    }
    unless (defined $date)
    {
        return 0;
    }

    my $documentDate = '';

    # Facturation mensuelle
    if ($rentType eq INVOICETYPE_MONTH)
    {
        # Calcul de la date du jour du mois précédent
        my @tabDate = split(/\-/, $date);
        my ($year, $month, $day) = &Date::Calc::Add_Delta_YM(&LOC::Util::toNumber($tabDate[0]),
                                                             &LOC::Util::toNumber($tabDate[1]),
                                                             &LOC::Util::toNumber($tabDate[2]),
                                                             0,    # -0 année
                                                             -1);  # -1 mois
        # Nombre de jours du mois précédent
        $day = &Date::Calc::Days_in_Month($year, $month);

        $documentDate = sprintf('%04d-%02d-%02d', $year, $month, $day);
    }

    # Facturation intermédiaire
    if ($rentType eq INVOICETYPE_INTERMEDIATE)
    {
        # Calcul de la date du jour précédent
        my @tabDate = split(/\-/, $date);
        my ($year, $month ,$day) = &Date::Calc::Add_Delta_Days($tabDate[0], $tabDate[1], $tabDate[2], -1);

        $documentDate = sprintf('%04d-%02d-%02d', $year, $month, $day);
    }

    return $documentDate;
}

# Function: getExportFileContent
# Retourne le contenu du fichier d'export vers Sage
#
# Parameters:
# string $countryId - Identifiant du pays
# string $rentType  - Type de facturation
# string $date      - Date de facturation
#
# Returns:
# string - Contenu du fichier d'export vers Sage
sub getExportFileContent
{
    my ($countryId, $rentType, $date) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my @tabContent;   # Tableau stockant le contenu du fichier d'export

    # Variables nécessaires à la génération du contenu
    my $documentType = &LOC::Characteristic::getCountryValueByCode('TYPEDOCSAGE', $countryId);   # BL ou facture
    my $depot = &LOC::Characteristic::getCountryValueByCode('DEPOTSTOCK', $countryId);   # Dépôt de stockage

    # Récupération des formats Sage (quantité, prix et montant)
    my $charac = &LOC::Characteristic::getCountryValueByCode('FORMATSSAGE', $countryId);
    my $tabSageFormats = {'quantity' => 2, 'price' => 4, 'amount' => 2};
    if ($charac ne '')
    {
        $tabSageFormats = &LOC::Json::fromJson($charac);
    }

    # Début du fichier d'export
    my $version = '#VER ' . LOC::Invoice::EXPORTFILE_VERSION;
    push(@tabContent, $version);   # Version du fichier

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $id = &LOC::Invoice::getId($countryId, $date, LOC::Invoice::TYPE_RENT);

    # Récupération des informations d'en-tête des factures non encore exportées
    my $query = '
SELECT
    tbl_f_rentinvoice.ENTETEAUTO AS `id`,
    tbl_f_rentinvoice.ENTETENUMFAC AS `document`,
    DATE_FORMAT(tbl_f_rentinvoice.ENTETEDATEDOC, "%d%m%y") AS `date`,
    tbl_f_rentinvoice.CONTRATCODE AS `reference`,
    DATE_FORMAT(tbl_f_rentinvoice.ENTETECONTRATDD, "%d%m%y") AS `contract.beginDate`,
    tbl_f_rentinvoice.CLCODE AS `thirdParty`,
    tbl_f_rentinvoice.CLCODE AS `payer`,
    tbl_f_rentinvoice.ENTETE1REFCMMDE AS `header1`,
    tbl_f_rentinvoice.ENTETE2 AS `header2`,
    tbl_f_rentinvoice.ENTETE3ADR1 AS `header3`,
    tbl_f_rentinvoice.ENTETE4ADR2 AS `header4`,
    tbl_f_rentinvoice.ENTETEAFFAIRE AS `affair`,
    tbl_f_rentinvoice.ENTETECOLLECTIF AS `generalAccount`,
    tbl_f_customer.CLNBFACT AS `customer.invoicesNo`,
    tbl_f_customer.CLFACTGRP AS `customer.CLFACTGRP`,
    tbl_f_rentinvoice.ENTETECATEGORIE AS `accountingCategory`
FROM ENTETEFACTURE tbl_f_rentinvoice
LEFT JOIN CLIENT tbl_f_customer
ON tbl_f_rentinvoice.CLCODE = tbl_f_customer.CLCODE
WHERE tbl_f_rentinvoice.ENTETEIDFACTU = ' . $db->quote($id) . '
AND tbl_f_rentinvoice.ENTETENUMFAC IS NOT NULL;';

    my $tabHeaderResult = $db->fetchAssoc($query, {}, 'id');
    my @tabInvoicesToUpdate = ();

    foreach my $tabInvoice (values(%$tabHeaderResult))
    {
        my $groupedInvoices = ($tabInvoice->{'customer.CLFACTGRP'} eq '1' ? '1' : '0');

        push(@tabContent, '#CHEN');   # Drapeau d'en-tête de document
        push(@tabContent, '1');   # Ligne 1 : domaine
        push(@tabContent, $documentType);   # Ligne 2 : type
        push(@tabContent, '1');   # Ligne 3 : provenance
        push(@tabContent, '1');   # Ligne 4 : souche
        push(@tabContent, $tabInvoice->{'document'});   # Ligne 5 : n° de pièce
        push(@tabContent, $tabInvoice->{'date'});   # Ligne 6 : date
        push(@tabContent, $tabInvoice->{'reference'});   # Ligne 7 : référence
        push(@tabContent, $tabInvoice->{'contract.beginDate'});   # Ligne 8 : livraison
        push(@tabContent, $tabInvoice->{'thirdParty'});   # Ligne 9 : tiers
        push(@tabContent, $depot);   # Ligne 10 : dépôt de stockage
        push(@tabContent, '');   # Ligne 11 : dépôt de livraison
        push(@tabContent, '1');   # Ligne 12 : périodicité
        push(@tabContent, '0');   # Ligne 13 : devise
        push(@tabContent, '0,000000');   # Ligne 14 : cours
        push(@tabContent, $tabInvoice->{'payer'});   # Ligne 15 : payeur/encaisseur
        push(@tabContent, '1');   # Ligne 16 : expédition
        push(@tabContent, '1');   # Ligne 17 : condition livraison
        push(@tabContent, '0');   # Ligne 18 : langue
        push(@tabContent, '');   # Ligne 19 : nom représentant
        push(@tabContent, '');   # Ligne 20 : prénom représentant
        push(@tabContent, $tabInvoice->{'header1'});   # Ligne 21 : en-tête 1
        push(@tabContent, $tabInvoice->{'header2'});   # Ligne 22 : en-tête 2
        push(@tabContent, $tabInvoice->{'header3'});   # Ligne 23 : en-tête 3
        push(@tabContent, $tabInvoice->{'header4'});   # Ligne 24 : en-tête 4
        push(@tabContent, $tabInvoice->{'affair'});   # Ligne 25 : affaire
        push(@tabContent, '1');   # Ligne 26 : catégorie tarifaire
        push(@tabContent, '21');   # Ligne 27 : régime
        push(@tabContent, '11');   # Ligne 28 : transaction
        push(@tabContent, &LOC::Invoice::getSageFormat(1, $tabSageFormats->{'quantity'}));   # Ligne 29 : colisage (format quantité)
        push(@tabContent, '1');   # Ligne 30 : unité colisage
        push(@tabContent, $tabInvoice->{'customer.invoicesNo'});   # Ligne 31 : nombre exemplaires
        push(@tabContent, $groupedInvoices);   # Ligne 32 : 1 BL/Facture
        push(@tabContent, '0,0000');   # Ligne 33 : taux d'escompte
        push(@tabContent, '0,0000');   # Ligne 34 : écart valorisation
        push(@tabContent, $tabInvoice->{'accountingCategory'});   # Ligne 35 : catégorie comptable
        push(@tabContent, '0');   # Ligne 36 : frais
        push(@tabContent, '2');   # Ligne 37 : statut
        push(@tabContent, $tabInvoice->{'generalAccount'});   # Ligne 38 : compte général
        push(@tabContent, '00:00:00');   # Ligne 39 : heure
        push(@tabContent, '');   # Ligne 40 : caisse
        push(@tabContent, '');   # Ligne 41 : caissier nom
        push(@tabContent, '');   # Ligne 42 : caissier prénom
        push(@tabContent, '0');   # Ligne 43 : clôturé
        push(@tabContent, '');   # Ligne 44 : numéro de commande site marchand
        push(@tabContent, '');   # Ligne 45 : ventilation IFRS
        push(@tabContent, '0');   # Ligne 46 : type valeur calcul frais d'expédition
        push(@tabContent, '0,00');   # Ligne 47 : valeur frais d'expédition
        push(@tabContent, '0');   # Ligne 48 : type valeur frais d'expédition
        push(@tabContent, '0');   # Ligne 49 : type valeur calcul franco de port
        push(@tabContent, '0,00');   # Ligne 50 : valeur franco de port
        push(@tabContent, '0');   # Ligne 51 : type valeur franco de port
        push(@tabContent, '0,00');   # Ligne 52 : taux taxe 1
        push(@tabContent, '0');   # Ligne 53 : type taux taxe 1
        push(@tabContent, '0');   # Ligne 54 : type de taxe 1
        push(@tabContent, '0,00');   # Ligne 55 : taux taxe 2
        push(@tabContent, '0');   # Ligne 56 : type taux taxe 2
        push(@tabContent, '0');   # Ligne 57 : type de taxe 2
        push(@tabContent, '0,00');   # Ligne 58 : taux taxe 3
        push(@tabContent, '0');   # Ligne 59 : type taux taxe 3
        push(@tabContent, '0');   # Ligne 60 : type de taxe 3
        push(@tabContent, '');   # Ligne 61 : motif
        push(@tabContent, '');   # Ligne 62 : centrale d'achat
        push(@tabContent, '');   # Ligne 63 : contact

        # Récupération des informations des lignes
        my $query = '
SELECT
    tbl_f_rentinvoiceline.LIGNEAUTO AS `id`,
    tbl_f_rentinvoice.CONTRATCODE AS `header.reference`,
    tbl_f_rentinvoiceline.LIGNEREFARTICLE AS `articleReference`,
    tbl_f_rentinvoiceline.LIGNEDESIGNATION AS `label`,
    tbl_f_rentinvoiceline.LIGNECOMMENTAIRE AS `labelExtra`,
    tbl_f_rentinvoiceline.LIGNEPRIX AS `unitPrice`,
    tbl_f_rentinvoiceline.LIGNEQTITE AS `quantity`,
    tbl_f_rentinvoiceline.LIGNEREMISE AS `reduction`,
    tbl_f_rentinvoiceline.LIGNEAFFAIRE AS `affair`,
    tbl_f_rentinvoiceline.LIGNETVA1 AS `vatRate1`,
    tbl_f_rentinvoiceline.LIGNETYPETVA1 AS `vatRateType1`,
    tbl_f_rentinvoiceline.LIGNETVA2 AS `vatRate2`,
    tbl_f_rentinvoiceline.LIGNETYPETVA2 AS `vatRateType2`,
    tbl_f_rentinvoiceline.LIGNETVA3 AS `vatRate3`,
    tbl_f_rentinvoiceline.LIGNETYPETVA3 AS `vatRateType3`,
    tbl_f_rentinvoiceline.LIGNETIERS AS `thirdPartyNumber`,
    tbl_f_rentinvoiceline.LIGNEINFOLIBRE2 AS `freeInfo2`,
    tbl_f_rentinvoiceline.LIGNEINFOLIBRE3 AS `freeInfo3`,
    tbl_f_rentinvoiceline.LIGNEINFOLIBRE4 AS `freeInfo4`,
    tbl_f_rentinvoiceline.LIGNEINFOLIBRE5 AS `freeInfo5`,
    tbl_f_rentinvoiceline.LIGNEINFOLIBRE8 AS `freeInfo8`
FROM LIGNEFACTURE tbl_f_rentinvoiceline
LEFT JOIN ENTETEFACTURE tbl_f_rentinvoice
ON tbl_f_rentinvoiceline.ENTETEAUTO = tbl_f_rentinvoice.ENTETEAUTO
WHERE tbl_f_rentinvoiceline.ENTETEAUTO = ' . $db->quote($tabInvoice->{'id'}) . ';';

        my $tabLineResult = $db->fetchAssoc($query, {}, 'id');

        foreach my $tabLine (values(%$tabLineResult))
        {
            push(@tabContent, '#CHLI');   # Drapeau d'en-tête de ligne
            push(@tabContent, $tabLine->{'header.reference'});   # Ligne 1 : référence ligne
            push(@tabContent, $tabLine->{'articleReference'});   # Ligne 2 : référence article
            push(@tabContent, $tabLine->{'label'});   # Ligne 3 : désignation
            push(@tabContent, $tabLine->{'labelExtra'});   # Ligne 4 : texte complémentaire
            push(@tabContent, '');   # Ligne 5 : énuméré de gamme 1
            push(@tabContent, '');   # Ligne 6 : énuméré de gamme 2
            push(@tabContent, '');   # Ligne 7 : n° de série & lot
            push(@tabContent, '');   # Ligne 8 : complément série/lot
            push(@tabContent, '');   # Ligne 9 : date péremption
            push(@tabContent, '');   # Ligne 10 : date fabrication
            push(@tabContent, '0');   # Ligne 11 : type de prix
            push(@tabContent, &LOC::Invoice::getSageFormat($tabLine->{'unitPrice'}, $tabSageFormats->{'price'}));   # Ligne 12 : prix unitaire (format montant)
            push(@tabContent,  &LOC::Invoice::getSageFormat(0, $tabSageFormats->{'price'}));   # Ligne 13 : prix unitaire en devise (format montant)
            push(@tabContent, &LOC::Invoice::getSageFormat($tabLine->{'quantity'}, $tabSageFormats->{'quantity'}));   # Ligne 14 : quantité (format quantité)
            push(@tabContent, &LOC::Invoice::getSageFormat(1, $tabSageFormats->{'quantity'}));   # Ligne 15 : quantité colisée (format quantité)
            push(@tabContent, '');   # Ligne 16 : conditionnement
            push(@tabContent, &LOC::Invoice::getSageFormat(0, $tabSageFormats->{'quantity'}));   # Ligne 17 : poids net global (format quantité)
            push(@tabContent, &LOC::Invoice::getSageFormat(0, $tabSageFormats->{'quantity'}));   # Ligne 18 : poids brut global (format quantité)
            push(@tabContent, &LOC::Invoice::getSageFormat(sprintf('%.2f%%', $tabLine->{'reduction'}), 2));   # Ligne 19 : remise
            push(@tabContent, '0');   # Ligne 20 : type de ligne
            push(@tabContent, '0,00');   # Ligne 21 : prix de revient unitaire
            push(@tabContent, &LOC::Invoice::getSageFormat(0, $tabSageFormats->{'amount'}));   # Ligne 22 : frais (format montant)
            push(@tabContent, &LOC::Invoice::getSageFormat(0, $tabSageFormats->{'price'}));   # Ligne 23 : CMUP (format prix)
            push(@tabContent, '0');   # Ligne 24 : provenance facture
            push(@tabContent, '');   # Ligne 25 : nom représentant
            push(@tabContent, '');   # Ligne 26 : prénom représentant
            push(@tabContent, '');   # Ligne 27 : date livraison
            push(@tabContent, $depot);   # Ligne 28 : dépôt de stockage
            push(@tabContent, $tabLine->{'affair'});   # Ligne 29 : affaire
            push(@tabContent, '1');   # Ligne 30 : valorisation
            push(@tabContent, '');   # Ligne 31 : référence composé
            push(@tabContent, '0');   # Ligne 32 : article non livré
            push(@tabContent, &LOC::Invoice::getSageFormat($tabLine->{'vatRate1'}, 2));   # Ligne 33 : taux taxe 1
            push(@tabContent, $tabLine->{'vatRateType1'});   # Ligne 34 : type taux taxe 1
            push(@tabContent, '0');   # Ligne 35 : type de taxe 1
            push(@tabContent, &LOC::Invoice::getSageFormat($tabLine->{'vatRate2'}, 2));   # Ligne 36 : taux taxe 2
            push(@tabContent, $tabLine->{'vatRateType2'});   # Ligne 37 : type taux taxe 2
            push(@tabContent, '0');   # Ligne 38 : type de taxe 2
            push(@tabContent, &LOC::Invoice::getSageFormat($tabLine->{'vatRate3'}, 2));   # Ligne 39 : taux taxe 3
            push(@tabContent, $tabLine->{'vatRateType3'});   # Ligne 40 : type taux taxe 3
            push(@tabContent, '0');   # Ligne 41 : type de taxe 3
            push(@tabContent, $tabLine->{'thirdPartyNumber'});   # Ligne 42 : numéro tiers
            push(@tabContent, '');   # Ligne 43 : référence fournisseur
            push(@tabContent, '');   # Ligne 44 : référence client
            push(@tabContent, '0');   # Ligne 45 : facturation sur le poids net
            push(@tabContent, '0');   # Ligne 46 : hors escompte
            push(@tabContent, '');   # Ligne 47 : numéro de colis
            push(@tabContent, '');   # Ligne 48 : code ressource
            push(@tabContent, '0');   # Ligne 49 : quantité ressource
            push(@tabContent, '0');   # Ligne 50 : existence agenda
            push(@tabContent, '000000');   # Ligne 51 : date avancement
            push(@tabContent, '1');   # Ligne 52 : code emplacement
            push(@tabContent, &LOC::Invoice::getSageFormat(1, $tabSageFormats->{'quantity'}));   # Ligne 53 : quantité emplacement (format quantité)

            push(@tabContent, '#CIVA');   # Drapeau d'en-tête des informations libres
            push(@tabContent, '');   # Ligne 1
            push(@tabContent, $tabLine->{'freeInfo2'});   # Ligne 2 : n° de parc
            push(@tabContent, $tabLine->{'freeInfo3'});   # Ligne 3 : modèle de machines
            push(@tabContent, $tabLine->{'freeInfo4'});   # Ligne 4 : n° de série
            push(@tabContent, $tabLine->{'freeInfo5'});   # Ligne 5 : agence
            push(@tabContent, '');   # Ligne 6
            push(@tabContent, '');   # Ligne 7
            push(@tabContent, $tabLine->{'freeInfo8'});   # Ligne 8 : famille commerciale
        }

        push(@tabInvoicesToUpdate, $tabInvoice->{'id'});
    }

    # Passage des factures à l'état "exportée"
    my $update;
    my $nbInvoicesToUpdate = @tabInvoicesToUpdate;

    if ($nbInvoicesToUpdate > 0)
    {
        my $tabUpdates = {'ETCODE' => LOC::Invoice::STATE_EXPORTED, 'ENTETEDATEEXPORT*' => 'NOW()'};
        $db->update('ENTETEFACTURE', $tabUpdates, {'ENTETEAUTO' => \@tabInvoicesToUpdate}, $schemaName);
    }

    if ($update == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        @tabContent = ($version);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
        }
    }

    push(@tabContent, '#FIN');   # Drapeau de fin de fichier

    my $content = &LOC::Util::encodeUtf8(join(LOC::Invoice::EXPORTFILE_LINEEND, @tabContent));
    &Encode::from_to($content, 'utf8', LOC::Invoice::EXPORTFILE_ENCODING);

    return $content;
}

# Function: getInvoicesToCancel
# Retourne la liste des factures à supprimer dans le cas d'une annulation de facturation
#
# Parameters:
# string $countryId - Identifiant du pays
# string $rentType  - Type de la facturation de location à annuler
# string $date      - Date de la facturation à annuler
#
# Returns:
# hashref - Liste des factures à supprimer
sub getInvoicesToCancel
{
    my ($countryId, $rentType, $date) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Récupération des références articles;
    my $tabArticleRefs;
    foreach my $code ('JRSUP', 'JRDED')
    {
        $tabArticleRefs->{$code} = &LOC::Invoice::ArticleRef::getValueByCode($countryId, $code);
    }

    my $invoicingId = &LOC::Invoice::getId($countryId, $date, LOC::Invoice::TYPE_RENT);
    my $tabInvoices = &getList($countryId, LOC::Util::GETLIST_ASSOC,
                              {'rentType' => $rentType, 'invoicingId' => $invoicingId});

    foreach my $tabInvoice (values(%{$tabInvoices}))
    {
        # Récupération des informations d'en-tête des factures non encore exportées
        my $contractQuery = '
SELECT
    tbl_f_contract.CONTRATAUTO AS `id`,
    tbl_f_contract.CONTRATJOURSPLUS AS `additionalDaysUnbilled`,
    (SELECT
        SUM(LIGNEQTITE)
    FROM LIGNEFACTURE tbl_f_rentinvoiceline
    WHERE tbl_f_rentinvoiceline.ENTETEAUTO = ' . $db->quote($tabInvoice->{'id'}) . '
        AND LIGNEREFARTICLE = ' . $db->quote($tabArticleRefs->{'JRSUP'}) . ') AS `additionalDaysBilled`,
    tbl_f_contract.CONTRATJOURSMOINS AS `deductedDaysUnbilled`,
    (SELECT
        SUM(- LIGNEQTITE)
    FROM LIGNEFACTURE tbl_f_rentinvoiceline
    WHERE tbl_f_rentinvoiceline.ENTETEAUTO = ' . $db->quote($tabInvoice->{'id'}) . '
        AND LIGNEREFARTICLE = ' . $db->quote($tabArticleRefs->{'JRDED'}) . ') AS `deductedDaysBilled`,
    tbl_f_contract.ETATFACTURE AS `invoiceState`,
    IF (tbl_f_contract.ETATFACTURE = "", "", tbl_f_contract.CONTRATDATEFACTURE) AS `lastInvoiceDate`,
    (SELECT
        SUM(crd_number)
    FROM f_rentcontractrepairday
    WHERE tbl_f_contract.CONTRATAUTO = crd_rct_id AND crd_date_invoicing IS NOT NULL) AS `repairDaysNo`,
    (SELECT
        SUM(crd_amount)
    FROM f_rentcontractrepairday
    WHERE tbl_f_contract.CONTRATAUTO = crd_rct_id AND crd_date_invoicing IS NOT NULL) AS `repairDaysAmount`,
    (SELECT
        COUNT(*)
    FROM CONTRATSERVICES tbl_f_contract_services
    WHERE tbl_f_contract.CONTRATAUTO = tbl_f_contract_services.CONTRATAUTO
        AND tbl_f_contract_services.COSERDATEFAC IS NOT NULL) AS `oldServicesNo`,
    (SELECT
        SUM(tbl_f_contract_services.COSERMONTANT)
    FROM CONTRATSERVICES tbl_f_contract_services
    WHERE tbl_f_contract.CONTRATAUTO = tbl_f_contract_services.CONTRATAUTO
        AND tbl_f_contract_services.COSERDATEFAC IS NOT NULL) AS `oldServicesAmount`,
    (SELECT
        COUNT(*)
    FROM f_rentcontractrepairservice
    WHERE tbl_f_contract.CONTRATAUTO = crs_rct_id AND crs_date_invoicing IS NOT NULL) AS `repairServicesNo`,
    (SELECT
        SUM(crs_amount)
    FROM f_rentcontractrepairservice
    WHERE tbl_f_contract.CONTRATAUTO = crs_rct_id AND crs_date_invoicing IS NOT NULL) AS `repairServicesAmount`,
    (SELECT
        COUNT(*)
    FROM f_rentcontract_rentservice
    WHERE tbl_f_contract.CONTRATAUTO = rcs_rct_id AND rcs_date_invoicing IS NOT NULL) AS `rentServicesNo`,
    (SELECT
        SUM(rcs_amount)
    FROM f_rentcontract_rentservice
    WHERE tbl_f_contract.CONTRATAUTO = rcs_rct_id AND rcs_date_invoicing IS NOT NULL) AS `rentServicesAmount`
FROM CONTRAT tbl_f_contract
WHERE tbl_f_contract.CONTRATCODE = ' . $db->quote($tabInvoice->{'contract.code'}) . '
GROUP BY tbl_f_contract.CONTRATAUTO;';

        my $tabContractResult = $db->fetchRow($contractQuery, {}, LOC::Db::FETCH_ASSOC);

        $tabInvoice->{'contract.id'}                      = $tabContractResult->{'id'};
        $tabInvoice->{'contract.additionalDaysUnbilled'}  = &LOC::Util::toNumber($tabContractResult->{'additionalDaysUnbilled'});
        $tabInvoice->{'additionalDaysBilled'}             = &LOC::Util::toNumber($tabContractResult->{'additionalDaysBilled'});
        $tabInvoice->{'contract.deductedDaysUnbilled'}    = &LOC::Util::toNumber($tabContractResult->{'deductedDaysUnbilled'});
        $tabInvoice->{'deductedDaysBilled'}               = &LOC::Util::toNumber($tabContractResult->{'deductedDaysBilled'});
        $tabInvoice->{'contract.lastInvoiceDate'}         = $tabContractResult->{'lastInvoiceDate'};

        $tabInvoice->{'contract.repairDaysNo'}            = $tabContractResult->{'repairDaysNo'};
        $tabInvoice->{'contract.repairDaysAmount'}        = $tabContractResult->{'repairDaysAmount'};
        $tabInvoice->{'contract.oldServicesNo'}           = $tabContractResult->{'oldServicesNo'};
        $tabInvoice->{'contract.oldServicesAmount'}       = $tabContractResult->{'oldServicesAmount'};
        $tabInvoice->{'contract.repairServicesNo'}        = $tabContractResult->{'repairServicesNo'};
        $tabInvoice->{'contract.repairServicesAmount'}    = $tabContractResult->{'repairServicesAmount'};
        $tabInvoice->{'contract.rentServicesNo'}          = $tabContractResult->{'rentServicesNo'};
        $tabInvoice->{'contract.rentServicesAmount'}      = $tabContractResult->{'rentServicesAmount'};
        if ($tabInvoice->{'invoicingOrigin'} eq ORIGIN_CONTRACT)
        {
            $tabInvoice->{'contract.repairDaysNo'}         = 0;
            $tabInvoice->{'contract.repairDaysAmount'}     = 0;
            $tabInvoice->{'contract.oldServicesNo'}        = 0;
            $tabInvoice->{'contract.oldServicesAmount'}    = 0;
            $tabInvoice->{'contract.repairServicesNo'}     = 0;
            $tabInvoice->{'contract.repairServicesAmount'} = 0;
            $tabInvoice->{'contract.rentServicesNo'}       = 0;
            $tabInvoice->{'contract.rentServicesAmount'}   = 0;
        }

        # Détermination de l'état de facturation qui va être mis dans le champ
        # ETATFACTURE après annulation de la facturation
        $tabInvoice->{'contract.previousInvoiceState'}    = $tabInvoice->{'contract.invoiceState'};
        $tabInvoice->{'contract.invoiceState'}            = $tabContractResult->{'invoiceState'};
        if ($tabInvoice->{'invoicingOrigin'} eq ORIGIN_CONTRACT)
        {
            $tabInvoice->{'contract.previousInvoiceState'} = $tabInvoice->{'contract.invoiceState'};
        }

        # Détermination de la date jusqu'à laquelle est facturée la location qui va être mise dans le champ
        # CONTRATDATEFACTURE après annulation de la facturation
        $tabInvoice->{'contract.previousLastInvoiceDate'} = undef;
        if ($tabInvoice->{'invoicingOrigin'} eq ORIGIN_CONTRACT)
        {
            $tabInvoice->{'contract.previousLastInvoiceDate'} = $tabInvoice->{'contract.lastInvoiceDate'};
        }
        elsif ($tabContractResult->{'invoiceState'} ne '')
        {
            if (defined $tabInvoice->{'beginDate'})
            {
                my @invoiceBeginDate = split(/-/, $tabInvoice->{'beginDate'});
                my ($year, $month ,$day) = &Date::Calc::Add_Delta_Days($invoiceBeginDate[0],
                                                                       $invoiceBeginDate[1],
                                                                       $invoiceBeginDate[2],
                                                                       -1);
                $tabInvoice->{'contract.previousLastInvoiceDate'} = sprintf('%04d-%02d-%02d', $year, $month ,$day);
            }
            else
            {
                $tabInvoice->{'contract.previousLastInvoiceDate'} = $tabContractResult->{'lastInvoiceDate'};
            }
        }
    }

    return $tabInvoices;
}

# Function: cancelInvoicing
# Annule une facturation
#
# Parameters:
# string $countryId - Identifiant du pays
# string $rentType  - Type de la facturation de location à annuler
# string $date      - Date de la facturation à annuler
#
# Returns:
# hashref - Liste des factures supprimées
sub cancelInvoicing
{
    my ($countryId, $rentType, $date) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Identifiant de la facturation à annuler
    my $id = &LOC::Invoice::getId($countryId, $date, LOC::Invoice::TYPE_RENT);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }


    # Récupération des factures à supprimer

    my $query = '
(SELECT
    tbl_f_rentinvoice.ENTETEAUTO AS `id`
FROM ENTETEFACTURE tbl_f_rentinvoice
WHERE tbl_f_rentinvoice.ENTETETYPEFACT = ' . $db->quote($rentType) . '
AND tbl_f_rentinvoice.ENTETEIDFACTU = ' . $db->quote($id) . ')
UNION DISTINCT
(SELECT
    tbl_f_rentinvoice.ENTETEAUTO AS `id`
FROM ENTETEFACTURE tbl_f_rentinvoice
INNER JOIN CONTRAT tbl_f_contract ON (tbl_f_contract.CONTRATAUTO = tbl_f_rentinvoice.CONTRATAUTO)
WHERE tbl_f_rentinvoice.ENTETETYPEFACT IS NULL
AND tbl_f_rentinvoice.ENTETEDATEFAC >= ' . $db->quote($date) . '
AND tbl_f_rentinvoice.CONTRATCODE IN
  (SELECT
    EF.CONTRATCODE
  FROM ENTETEFACTURE EF
  WHERE EF.ENTETETYPEFACT = ' . $db->quote($rentType) . '
  AND EF.ENTETEIDFACTU = ' . $db->quote($id) . '
  )
)';

    my @tabInvoiceId = $db->fetchCol($query);

    my $deletedInvoicesNo = 0;
    foreach my $id (@tabInvoiceId)
    {
        my $nbInvoices = &cancelInvoice($countryId, $id);
        if ($nbInvoices == 0)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;
        }
        $deletedInvoicesNo += $nbInvoices;
    }

    # Marquage de la facturation en non effectué
    if ($db->update('p_invoicedate', {'ivd_done' => 0}, {'ivd_cty_id' => $countryId,
                                                         'ivd_date'   => $date,
                                                         'ivd_type'   => $rentType}, 'AUTH') == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return -1;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }

    return $deletedInvoicesNo;
}

# Function: cancelInvoice
# Annule une facture
#
# Parameters:
# string $countryId   - Identifiant du pays
# int    $id          - Identifiant de la facture
# bool   $forceDelete - Booléen indiquant si la suppression de la facture est forcée
#
# Returns:
# bool - 1 en cas de succès, 0 sinon
sub cancelInvoice
{
    my ($countryId, $id, $forceDelete) = @_;

    unless (defined $forceDelete)
    {
        $forceDelete = 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Récupération de la date de facturation
    my $query = '
SELECT
    ENTETEDATEFAC AS `invoicingDate`,
    tbl_f_rentinvoice.CONTRATAUTO AS `contract.id`,
    ENTETEDATED AS `beginDate`,
    ENTETETYPEFACT AS `rentType`,
    ENTETEORIGINE AS `invoicingOrigin`,
    tbl_f_rentinvoice.AGAUTO AS `agency.id`,
    tbl_f_rentinvoice.ETATFACTURE AS `contract.lastInvoiceState`,
    tbl_f_contract.ETATFACTURE AS `contract.invoiceState`,
    CONTRATDATEFACTURE AS `contract.lastInvoiceDate`,
    tbl_f_contract.ETCODE AS `contract.state`
FROM ENTETEFACTURE tbl_f_rentinvoice
LEFT JOIN CONTRAT tbl_f_contract
ON tbl_f_rentinvoice.CONTRATAUTO = tbl_f_contract.CONTRATAUTO
WHERE tbl_f_rentinvoice.ENTETEAUTO = ' . $db->quote($id);
    my $tabInvoiceData = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);

    if (!$tabInvoiceData)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    my $date             = $tabInvoiceData->{'invoicingDate'};
    my $contractId       = $tabInvoiceData->{'contract.id'};
    my $rentType         = $tabInvoiceData->{'rentType'};
    my $invoicingOrigin  = $tabInvoiceData->{'invoicingOrigin'};
    my $agencyId         = $tabInvoiceData->{'agency.id'};
    my $lastInvoiceState = $tabInvoiceData->{'contract.lastInvoiceState'};
    my $invoiceState     = $tabInvoiceData->{'contract.invoiceState'};
    my $lastInvoiceDate  = $tabInvoiceData->{'contract.lastInvoiceDate'};
    my $contractState    = $tabInvoiceData->{'contract.state'};

    # Calcul de la pécédente date de fin de facturation de la location du contrat
    # (date de début de la période de location facturée sur la facture à supprimer moins 1 jour)
    my $endInvoiceDate = undef;
    if ($lastInvoiceState ne '')
    {
        if (defined $tabInvoiceData->{'beginDate'})
        {
            my @invoiceBeginDate = split(/-/, $tabInvoiceData->{'beginDate'});
            my ($year, $month ,$day) = &Date::Calc::Add_Delta_Days($invoiceBeginDate[0],
                                                                   $invoiceBeginDate[1],
                                                                   $invoiceBeginDate[2],
                                                                   -1);
            $endInvoiceDate = sprintf('%04d-%02d-%02d', $year, $month ,$day);
        }
        else
        {
            $endInvoiceDate = $lastInvoiceDate;
        }
    }

    # Récupération des références articles;
    my $tabArticleRefs;
    foreach my $code ('JRSUP', 'JRDED')
    {
        $tabArticleRefs->{$code} = &LOC::Invoice::ArticleRef::getValueByCode($countryId, $code);
    }

    # Récupération des jours supplémentaires et déduits facturés
    $query = '
SELECT
    LIGNEREFARTICLE AS `articleReference`,
    LIGNEQTITE AS `quantity`
FROM LIGNEFACTURE tbl_f_rentinvoiceline
WHERE tbl_f_rentinvoiceline.ENTETEAUTO = ' . $db->quote($id) . '
AND LIGNEREFARTICLE IN (' . $db->quote(values(%$tabArticleRefs)) . ')';
    my $tabDaysData = $db->fetchAssoc($query, {}, 'articleReference');
    my $addDaysNo = &LOC::Util::toNumber($tabDaysData->{$tabArticleRefs->{'JRSUP'}}->{'quantity'});
    my $dedDaysNo = &LOC::Util::toNumber(- $tabDaysData->{$tabArticleRefs->{'JRDED'}}->{'quantity'});

    my $tabUpdates;
    my $tabWheres;

    # Mise à jour J+/J- facturés
    $tabUpdates = {'day_date_invoicing*' => 'NULL'};
    if ($forceDelete || $invoicingOrigin ne LOC::Invoice::Rent::ORIGIN_CONTRACT)
    {
        $tabUpdates->{'day_is_invoiced'} = 0;
    }
    $tabWheres  = {'day_rct_id'          => $contractId,
                   'day_is_invoiced'     => 1,
                   'day_date_invoicing'  => $date};
    if ($db->update('f_day', $tabUpdates, $tabWheres, $schemaName) == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Mise à jour des jours de remise en état des contrats facturés
    $tabUpdates = {'crd_date_invoicing*' => 'NULL',
                   'crd_ril_id*'         => 'NULL'};
    if ($forceDelete || $invoicingOrigin ne LOC::Invoice::Rent::ORIGIN_CONTRACT)
    {
        $tabUpdates->{'crd_sta_id'} = LOC::Contract::RepairDay::STATE_APPROVED;
    }
    $tabWheres  = {'crd_rct_id'          => $contractId,
                   'crd_sta_id'          => LOC::Contract::RepairDay::STATE_INVOICED,
                   'crd_date_invoicing'  => $date};
    if ($db->update('f_rentcontractrepairday', $tabUpdates, $tabWheres, $schemaName) == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Mise à jour des services de remise en état des contrats facturés
    $tabUpdates = {'crs_date_invoicing*' => 'NULL',
                   'crs_ril_id*'         => 'NULL'};
    if ($forceDelete || $invoicingOrigin ne LOC::Invoice::Rent::ORIGIN_CONTRACT)
    {
        $tabUpdates->{'crs_sta_id'} = LOC::Contract::RepairService::STATE_APPROVED;
    }
    $tabWheres  = {'crs_rct_id'          => $contractId,
                   'crs_sta_id'          => LOC::Contract::RepairService::STATE_INVOICED,
                   'crs_date_invoicing'  => $date};
    if ($db->update('f_rentcontractrepairservice', $tabUpdates, $tabWheres, $schemaName) == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Mise à jour des anciens services des contrats facturés
    $tabUpdates = {'COSERDATEFAC*' => 'NULL'};
    if ($forceDelete || $invoicingOrigin ne LOC::Invoice::Rent::ORIGIN_CONTRACT)
    {
        $tabUpdates->{'ETCODE'} = LOC::Common::RentService::STATE_UNBILLED;
    }
    $tabWheres  = {'CONTRATAUTO'   => $contractId,
                   'ETCODE'        => LOC::Common::RentService::STATE_BILLED,
                   'COSERDATEFAC'  => $date};
    if ($db->update('CONTRATSERVICES', $tabUpdates, $tabWheres, $schemaName) == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Mise à jour des services de location et de transport des contrats facturés
    $tabUpdates = {'rcs_date_invoicing*' => 'NULL'};
    if ($forceDelete || $invoicingOrigin ne LOC::Invoice::Rent::ORIGIN_CONTRACT)
    {
        $tabUpdates->{'rcs_sta_id'} = LOC::Common::RentService::STATE_UNBILLED;
    }
    $tabWheres  = {'rcs_rct_id'          => $contractId,
                   'rcs_sta_id'          => LOC::Common::RentService::STATE_BILLED,
                   'rcs_date_invoicing'  => $date};
    if ($db->update('f_rentcontract_rentservice', $tabUpdates, $tabWheres, $schemaName) == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # On vide la date de clôture du contrat
    my $tabContractUpdates = {
        'CONTRATDATECLOTURE' => undef
    };

    # Facture générée lors de la facturation
    if ($forceDelete || $invoicingOrigin eq LOC::Invoice::Rent::ORIGIN_INVOICING)
    {
        my $isOriginInvoicing = ($invoicingOrigin eq LOC::Invoice::Rent::ORIGIN_INVOICING ? 1 : 0);

        # Mise à jour des contrats facturés
        $tabContractUpdates->{'ETATFACTURE'}           = $lastInvoiceState;
        $tabContractUpdates->{'CONTRATJOURSPLUS*'}     = sprintf('CONTRATJOURSPLUS %+f', $addDaysNo);
        $tabContractUpdates->{'CONTRATJOURSMOINS*'}    = sprintf('CONTRATJOURSMOINS %+f', $dedDaysNo);
        $tabContractUpdates->{'CONTRATJOURPLUSFACT*'}  = sprintf('CONTRATJOURPLUSFACT %+f', - $addDaysNo);
        $tabContractUpdates->{'CONTRATJOURMOINSFACT*'} = sprintf('CONTRATJOURMOINSFACT %+f', - $dedDaysNo);
        $tabContractUpdates->{'CONTRATDATEFACTURE'}    = $endInvoiceDate;
        $tabContractUpdates->{'ETCODE*'}               = ($isOriginInvoicing && $contractState eq LOC::Contract::Rent::STATE_BILLED ? $db->quote(LOC::Contract::Rent::STATE_STOPPED) : 'ETCODE');
        $tabContractUpdates->{'LIETCONTRATAUTO*'}      = ($isOriginInvoicing && $contractState eq LOC::Contract::Rent::STATE_BILLED ? substr(LOC::Contract::Rent::STATE_STOPPED, -2) * 1 : 'LIETCONTRATAUTO');

        # Suppression des lignes de factures
        if ($db->delete('LIGNEFACTURE', {'ENTETEAUTO' => $id}, $schemaName) == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }

        # Suppression des en-têtes de factures
        if ($db->delete('ENTETEFACTURE', {'ENTETEAUTO' => $id}, $schemaName) == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }
    }

    # Mise à jour du contrat
    if ($db->update('CONTRAT', $tabContractUpdates, {'CONTRATAUTO*' => $contractId}, $schemaName) == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Facture générée à la mise en facturation du contrat
    if (!$forceDelete && $invoicingOrigin eq LOC::Invoice::Rent::ORIGIN_CONTRACT)
    {
        # si il s'agit d'une facture non numérotée générée entre la facturation et l'annulation de la facturation
        if ($rentType eq undef)
        {
            # Suppression des lignes de factures
            if ($db->delete('LIGNEFACTURE', {'ENTETEAUTO' => $id}, $schemaName) == -1)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return 0;
            }

            # Suppression des en-têtes de factures
            if ($db->delete('ENTETEFACTURE', {'ENTETEAUTO' => $id}, $schemaName) == -1)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return 0;
            }
        }
        else
        {
            # Suppression de la date de facturation, du type de facturation de location,
            # de la date d'export et passage à l'état "non exportée"
            $tabUpdates = {'ENTETEDATEFAC*'    => 'NULL',
                           'ENTETEDATEDOC*'    => 'NULL',
                           'ENTETENUMFAC*'     => 'NULL',
                           'ENTETETYPEFACT*'   => 'NULL',
                           'ENTETEDATEEXPORT*' => 'NULL',
                           'ENTETEIDFACTU*'    => 'NULL',
                           'ETCODE'            => LOC::Invoice::STATE_NOTEXPORTED};
            $tabWheres = {'ENTETEAUTO'  => $id};
            my $update = $db->update('ENTETEFACTURE', $tabUpdates, $tabWheres, $schemaName);
            if ($update == -1)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return 0;
            }
        }
    }

    # Déverrouillage des transports
    # - Types : livraison et récupération
    my $tabFilters = {
        'type'    => [LOC::Transport::TYPE_DELIVERY, LOC::Transport::TYPE_RECOVERY],
        'from-to' => {
            'type' => LOC::Transport::FROMTOTYPE_CONTRACT,
            'id'   => $contractId
        }
    };
    if (!&LOC::Transport::unlock($countryId, $tabFilters, undef, undef))
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # - Type : transfert inter-chantiers
    my $tabFilters = {
        'type' => [LOC::Transport::TYPE_SITESTRANSFER],
        'to'   => {
            'type' => LOC::Transport::FROMTOTYPE_CONTRACT,
            'id'   => $contractId
        }
    };
    if (!&LOC::Transport::unlock($countryId, $tabFilters, undef, undef))
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }


    # Mise à jour des transports du contrat si l'état de facturation a changé
    if ($invoiceState ne $lastInvoiceState)
    {
        # Changements pouvant impacter le transport ? => on le lui communique
        &LOC::Transport::onExternalChanges($countryId, {
            'contract' => {
                'id'    => $contractId,
                'event' => 'update',
                'props' => {
                    'invoiceState' => {'old' => $invoiceState, 'new' => $lastInvoiceState}
                }
            }
        }, undef, undef, undef);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    return 1;
}

# Function: readSubRentFile
# Lit le fichier d'entrée pour la sous-location
#
# Parameters:
# string $file        - chemin du fichier à lire
# int    $nopiece     - numéro de pièce à incrémenter
# string $countryId   - Identifiant du pays
#
# Returns:
# ref array - tableau des données lues par ligne
sub readSubRentFile
{
    my ($file, $nopiece, $countryId) = @_;

    my %hashDate        = &LOC::Date::getHashDate();
    # Calcul de la date du moins précédent
    my @tabMonths = (1..9, 'O', 'N', 'D');
    my $year = $hashDate{'y'};
    my $month = $hashDate{'m'} - 1;
    if ($month == 0)
    {
        $month = 12;
        $year -= 1;
    }
    my $prefixSubRentNumber = sprintf('R%02d%s', $year % 100, $tabMonths[$month - 1]);

    # Ouverture du fichier
    open(FILE, $file) or return 0;
    $/ = "\r\n";   # Caractères de fin de ligne à supprimer

    #Récupération de la liste des taux de TVA en vigueur en fonction du pays
    my $tabResult = &LOC::Country::getVATRatesList($countryId);
    my $tva = '';
    #Récupération du taux de tva en vigueur (TN)
    foreach my $tabRow (values %$tabResult)
    {
        if ($tabRow->{'id'} eq 'TN')
        {
            $tva =  $tabRow->{'value'};
        }
     }
    # Lecture
    my @tabInfos;
    my $indexTabInfos = 0;
    while (my $line = <FILE>)
    {
        my @tabLine = split(';', $line);
        # récup des informations communes à toutes les lignes d'un bloc
        $tabInfos[$indexTabInfos] = {
            'domain'   => 0,
            'type'     => 3,
            'piece'    => $prefixSubRentNumber . sprintf("%04d", $nopiece),
            'customer' => $tabLine[1],
            'refLine'  => $tabLine[0],
            'txTva'    => sprintf("%.4f", $tva * 100),
            'typeTva'  => 0
        };
        $tabInfos[$indexTabInfos]->{'txTva'} =~ s/\./,/;
        # récup des informations spécifiques à chaque ligne du bloc
        my @lineBlock;
        my $cptLineBlock = 0;
        ## - REALQ
        $lineBlock[$cptLineBlock] = {
            'codearticle' => 'REALQ',
            'article'     => $tabLine[18] . ' ' . $tabLine[19] . ' del ' . $tabLine[21] . ' al ' . $tabLine[22],
            'priceUnit'   => $tabLine[6],
            'qty'         => $tabLine[23]
        };
        $cptLineBlock++;
        ## - J+
        if ($tabLine[25] * 1 > 0 || $tabLine[25] ne '')
        {
            $lineBlock[$cptLineBlock] = {
                'codearticle' => 'J+',
                'article'     => 'Dias mas',
                'priceUnit'   => $tabLine[6],
                'qty'         => $tabLine[25]
            };
            $cptLineBlock++;
        }
        ## - J-
        if ($tabLine[26] * 1 > 0 || $tabLine[26] ne '')
        {
            $lineBlock[$cptLineBlock] = {
                'codearticle' => 'J-',
                'article'     => 'Dias menos',
                'priceUnit'   => -$tabLine[6],
                'qty'         => $tabLine[26]
            };
            $cptLineBlock++;
        }
        ## - ASS
        if ($tabLine[31] * 1 > 0)
        {
            $lineBlock[$cptLineBlock] = {
                'codearticle' => 'ASS',
                'article'     => 'Cobertura riesgo ' . $tabLine[8],
                'priceUnit'   => $tabLine[31],
                'qty'         => 1
            };
            $cptLineBlock++;
        }
        ## - TRANSA
        if ($tabLine[32] * 1 > 0)
        {
            $lineBlock[$cptLineBlock] = {
                'codearticle' => 'TRANSA',
                'article'     => 'Entrega',
                'priceUnit'   => $tabLine[32],
                'qty'         => 1
            };
            $cptLineBlock++;
        }
        ## - TRANSR
        if ($tabLine[33] * 1 > 0)
        {
            $lineBlock[$cptLineBlock] = {
                'codearticle' => 'TRANSR',
                'article'     => 'Recogida',
                'priceUnit'   => $tabLine[33],
                'qty'         => 1
            };
            $cptLineBlock++;
        }
        ## - CARB
        if ($tabLine[34] * 1 > 0)
        {
            $lineBlock[$cptLineBlock] = {
                'codearticle' => 'CARB',
                'article'     => 'Carburante',
                'priceUnit'   => $tabLine[34],
                'qty'         => 1
            };
            $cptLineBlock++;
        }
        ## - NET
        if ($tabLine[35] * 1 > 0)
        {
            $lineBlock[$cptLineBlock] = {
                'codearticle' => 'NET',
                'article'     => 'Limpiezas',
                'priceUnit'   => $tabLine[35],
                'qty'         => 1
            };
            $cptLineBlock++;
        }
        ## - RES
        if ($tabLine[36] * 1 > 0)
        {
            $lineBlock[$cptLineBlock] = {
                'codearticle' => 'RES',
                'article'     => 'Gestion de residuos',
                'priceUnit'   => $tabLine[36],
                'qty'         => 1
            };
            $cptLineBlock++;
        }
        ## - SER
        if ($tabLine[28] ne '')
        {
            $lineBlock[$cptLineBlock] = {
                'codearticle' => 'SER',
                'article'     => $tabLine[28],
                'priceUnit'   => $tabLine[27],
                'qty'         => 1
            };
            $cptLineBlock++;
        }
        $tabInfos[$indexTabInfos]->{'lineBlock'} = \@lineBlock;
        $nopiece++;
        $indexTabInfos++;
    }
    close(FILE);
    return \@tabInfos;

}

# Function: generateSageFile
# génère le fichier txt pour import dans SAGE de la sous-location
#
# Parameters:
# refarray $tabInfos   - tableau des données à insérer dans le fichier
#
# Returns:
# bool - 1 succès 0 sinon
sub generateSageFile
{
    my ($tabInfos, $countryId) = @_;

    my %hashDate        = &LOC::Date::getHashDate();
    # Calcul de la date du moins précédent
    my $year = $hashDate{'y'};
    my $month = $hashDate{'m'} - 1;
    if ($month == 0)
    {
        $month = 12;
        $year -= 1;
    }

    # Création du fichier à exporter
    my $dirToWritePath = &LOC::Util::getCommonFilesPath('subRent/' . sprintf('%04d_%02d', $year, $month) . '/');
    my $fileToWritePath = $dirToWritePath . 'exportSousLoc_' . $countryId . '_' . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE) . '.txt';
    open(FILE, ">" . $fileToWritePath);

    foreach my $line (@$tabInfos)
    {
        foreach my $lineArticle (@{$line->{'lineBlock'}})
        {
            my @tabLineToWrite = (
                    $line->{'domain'},
                    $line->{'type'},
                    $line->{'piece'},
                    sprintf('%02d', $hashDate{'d'}) . sprintf('%02d', $hashDate{'m'}) . substr($hashDate{'y'}, 2),
                    $line->{'customer'},
                    $lineArticle->{'codearticle'},
                    $lineArticle->{'article'},
                    $line->{'refLine'},
                    $lineArticle->{'priceUnit'},
                    $lineArticle->{'qty'},
                    $line->{'txTva'},
                    $line->{'typeTva'}
            );
            my $lineToWrite = join(';', @tabLineToWrite);
            print FILE $lineToWrite . ';' . LOC::Invoice::EXPORTFILE_LINEEND;
        }
    }


    close(FILE);
    return sprintf('%04d_%02d', $year, $month) . '-exportSousLoc_' . $countryId . '_' . &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE) . '.txt';
}


# Function: update
# Met à jour une facture de location
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $invoiceId  - Identifiant de la facture
# hashref $tabData    - Données à modifier
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub update
{
    my ($countryId, $invoiceId, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'update', $_[0], __FILE__, '');
        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }
        return 0;
    };

    # Récupération des informations
    my $tabInfos = &getInfos($countryId, $invoiceId);
    if (!$tabInfos)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Vérification des données
    my $oldGeneralAccount = $tabInfos->{'generalAccount'};
    my $newGeneralAccount = (exists $tabData->{'generalAccount'} ? $tabData->{'generalAccount'} : $oldGeneralAccount);

    my $tabUpdates = {};
    if ($oldGeneralAccount ne $newGeneralAccount)
    {
        $tabUpdates->{'ENTETECOLLECTIF'} = $newGeneralAccount;
    }
    if (keys %$tabUpdates > 0)
    {
        if (!$db->update('ENTETEFACTURE', $tabUpdates, {'ENTETEAUTO' => $invoiceId}))
        {
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: onExternalChanges
# Exécute toutes les mises à jour nécessaires lors de la modification d'une entitée externe
#
# Parameters:
# string  $countryId  - Pays
# hashref $tabChanges - Informations sur les mises à jour des entitées externes
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub onExternalChanges
{
    my ($countryId, $tabChanges, $userId, $tabErrors, $tabEndUpds) = @_;

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'onExternalChanges', $_[0], __FILE__, '');
        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }
        return 0;
    };

    my %tabToUpdate = ();

    # Impacts des clients sur les factures non numérotées
    # ---------------------------------------------
    if (exists $tabChanges->{'customer'})
    {
        my $id = $tabChanges->{'customer'}->{'id'};

        my $tabProps = $tabChanges->{'customer'}->{'props'};
        if (keys %$tabProps > 0)
        {
            my $tabCustomerInfos = &LOC::Customer::getInfos($countryId, $id);
            if ($tabCustomerInfos->{'code'} ne '')
            {
                # Impact sur les factures non exportées
                if ($tabProps->{'accountingCode'})
                {
                    my $tabInvoices = &LOC::Invoice::Rent::getList($countryId,
                                                                LOC::Util::GETLIST_IDS,
                                                                {'customerCode' => $tabCustomerInfos->{'code'},
                                                                 'numbered'     => 0
                                                                });
                    foreach my $invoiceId (@$tabInvoices)
                    {
                        $tabToUpdate{$invoiceId} = {
                            'generalAccount' => $tabProps->{'accountingCode'}->{'new'}
                        };
                    }
                }
            }
        }
    }

    # Mise à jour des factures
    foreach my $invoiceId (keys %tabToUpdate)
    {
        if (!&LOC::Invoice::Rent::update($countryId, $invoiceId, $tabToUpdate{$invoiceId}))
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}

# Function: _getContractInvoiceDatesInfos
# Retourne des informations sur les dates pour la facturation du contrat
#
# Parameters:
# string  $invoiceType    - Type de facturation (INVOICETYPE_MONTH ou INVOICETYPE_INTERMEDIATE)
# hashref $tabInfos       - Informations sur le contrat
# hashref $tabParams      - Tableau des paramètres nécessaires pour facturer
# hashref $tabSageFormats - Tableau des formats Sage
#
# Return:
# hashref - 
#    'beginInvoiceDate'    - Date de début de facturation du contrat
#    'endInvoiceDate'      - Date de fin de facturation du contrat
#    'nbRentalDays'        - Nombre de jours à facturer sur le contrat (hors J+/-)
#    'finishedInMonth'     - Le contrat finit dans le mois ?
    
sub _getContractInvoiceDatesInfos
{
    my ($invoiceType, $tabInfos, $tabParams, $tabSageFormats) = @_;

    my $agencyId = $tabInfos->{'agency.id'};
    my $finishedInMonth = 1;

    # Date de début de facturation
    my $beginInvoiceDate;
    my %beginInvoiceHashDate;

    # Dates de fin de facturation par défaut remplies avec les dates de fin du contrat
    my $endInvoiceDate = $tabInfos->{'endDate'};
    my %endInvoiceHashDate = &LOC::Date::getHashDate($tabInfos->{'endDate'});

    # Positionnement de la date de début de facturation
    if ($tabInfos->{'lastInvoiceDate'} && $tabInfos->{'lastInvoiceDate'} ne '0000-00-00')
    {
        # Date de dernière facturation du contrat
        my %lastInvoiceHashDate = &LOC::Date::getHashDate($tabInfos->{'lastInvoiceDate'});
        # La date de de début de facturation est la date de dernière facturation + 1 jour
        my ($bIDYear, $bIDMonth, $bIDDay) = &Date::Calc::Add_Delta_Days($lastInvoiceHashDate{'y'},
                                                                        $lastInvoiceHashDate{'m'},
                                                                        $lastInvoiceHashDate{'d'},
                                                                        1);
        $beginInvoiceHashDate{'y'} = $bIDYear;
        $beginInvoiceHashDate{'m'} = $bIDMonth;
        $beginInvoiceHashDate{'d'} = $bIDDay;
        $beginInvoiceDate = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE, %beginInvoiceHashDate);
    }
    else
    {
        $beginInvoiceDate     = $tabInfos->{'beginDate'};
    }

    # FACTURATION MENSUELLE
    # Si le contrat se termine après la fin du mois de facturation,
    # la date de fin du mois remplace la date de fin du contrat
    if ($invoiceType eq LOC::Invoice::Rent::INVOICETYPE_MONTH &&
        $tabInfos->{'endDate'} gt $tabParams->{'endMonthInvoiceDate'})
    {
        $endInvoiceDate = $tabParams->{'endMonthInvoiceDate'};
        $finishedInMonth = 0;
    }

    # Numéro du dernier jour à facturer (1 lundi -> 7 dimanche)
    my $lastDayToInvoiceNumber = &Date::Calc::Day_of_Week($endInvoiceHashDate{'y'}, $endInvoiceHashDate{'m'},
                                                          $endInvoiceHashDate{'d'});

    # Calcul des jours à facturer sur le contrat (sans jours plus ni jours moins)
    my $nbRentalDays = &LOC::Date::getRentDuration(
        $beginInvoiceDate,
        $endInvoiceDate,
        $agencyId,
        $tabInfos->{'endDate'}
    );
    $nbRentalDays = &LOC::Util::round($nbRentalDays, $tabSageFormats->{'quantity'});

    # Si contrat en PF ou date de début postérieure à la date de fin
    if ($tabInfos->{'invoiceState'} eq LOC::Contract::Rent::INVOICESTATE_FINALPARTIAL ||
        $beginInvoiceDate gt $endInvoiceDate)
    {
        $beginInvoiceDate = undef;
        $endInvoiceDate   = undef;
    }

    return {
        'beginInvoiceDate' => $beginInvoiceDate,
        'endInvoiceDate'   => $endInvoiceDate,
        'nbRentalDays'     => $nbRentalDays,
        'finishedInMonth'  => $finishedInMonth
    };
}

1;
