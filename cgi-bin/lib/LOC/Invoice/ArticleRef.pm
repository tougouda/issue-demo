use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Invoice::ArticleRef
# Module de gestion des références articles
package LOC::Invoice::ArticleRef;


# Constants: États
# STATE_ACTIVE   - Alerte active
# STATE_INACTIVE - Alerte inactive
use constant
{
    STATE_ACTIVE   => 'GEN01',
    STATE_INACTIVE => 'GEN03',
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Util;


# Function: getInfos
# Retourne un hachage contenant les infos d'une référence article
#
# Parameters:
# string $countryId - Pays
# int    $id        - Id de la prestation
#
# Returns:
# hash|hashref
sub getInfos
{
    my ($countryId, $id, $flags) = @_;

    my $tab = getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Récupérer la liste des références articles
#
# Contenu du hachage:
# - id          => id
# - name        => libellé
# - state.id    => état
# - code        => code
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id, code)
# int     $flags      - Flags
#
# Returns:
# hash|hashref
sub getList
{
    my ($countryId, $format, $tabFilters, $flags) = @_;
    unless (defined $flags)
    {
        $flags = 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '';

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    `art_id` AS `id`,
    `art_name` AS `name`,
    `art_code` AS `code`,
    `art_value` AS `value`,
    `art_label` AS `label`,
    `art_vat_id` AS `vat.id`,
    `art_sta_id` AS `state.id`
FROM p_articleref
WHERE art_sta_id <> ' . $db->quote(STATE_INACTIVE);
        # Recherche par id
        if (defined $tabFilters->{'id'})
        {
            if (ref($tabFilters->{'id'}) eq 'ARRAY')
            {
                $query .= '
    AND `art_id` IN (' . join(', ', @{$tabFilters->{'id'}}) . ')';
            }
            else
            {
                $query .= '
    AND `art_id` = ' . $tabFilters->{'id'};
            }
        }
        # Recherche par code
        if (defined $tabFilters->{'code'})
        {
            if (ref($tabFilters->{'code'}) eq 'ARRAY')
            {
                $query .= '
    AND `art_code` IN ("' . join('", "', @{$tabFilters->{'code'}}) . '")';
            }
            else
            {
                $query .= '
    AND `art_code` = "' . $tabFilters->{'code'} . '"';
            }
        }
        $query .= '
ORDER BY art_name'; 
    }
    elsif($format == LOC::Util::GETLIST_PAIRS)
    {
        $query = '
SELECT
    `art_id` AS `id`,
    `art_name` AS `name`
FROM p_articleref
WHERE art_sta_id <> ' . $db->quote(STATE_INACTIVE);
        if (defined $tabFilters->{'id'})
        {
            if (ref($tabFilters->{'id'}) eq 'ARRAY')
            {
                $query .= '
    AND `art_id` IN (' . join(', ', @{$tabFilters->{'id'}}) . ')';
            }
            else
            {
                $query .= '
    AND `art_id` = ' . $tabFilters->{'id'};
            }
        }
        $query .= '
ORDER BY art_name'; 
    }

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    return (wantarray ? %$tab : $tab);
}

# Function: getValueByCode
# Récupérer la valeur de l'article Sage à partir du code Gestion des locations
#
# Parameters:
# string  $countryId - Pays
# string  $code      - Code
#
# Returns:
# string Valeur de l'article Sage à partir du code Gestion des locations
sub getValueByCode
{
    my ($countryId, $code) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT
    `art_value` AS `value`
FROM p_articleref
WHERE art_sta_id <> ' . $db->quote(STATE_INACTIVE) . '
AND art_code = ' . $db->quote($code);

    return $db->fetchOne($query);
}

# Function: set
# Met à jour (insert/update) toutes les infos d'une référence article en BDD
#
# Contenu du hachage à passer en paramètre:
# - id          => id
# - name        => libellé
# - code        => code
# - value       => valeur
#
# Parameters:
# string    $countryId - Pays
# hashref   $values    - Valeurs de la prestation
#
# Returns:
# integer
sub set
{
    my ($countryId, $values) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Mise à jour
    if (defined $values->{'id'} && $values->{'id'} != 0)
    {
        my $result = $db->update('p_articleref', {
                                                  'art_name'   => $values->{'name'},
                                                  'art_code'   => $values->{'code'},
                                                  'art_value'  => $values->{'value'},
                                                  'art_label'  => $values->{'label'},
                                                  'art_vat_id' => $values->{'vat.id'},
                                                 },
                                                 {
                                                  'art_id'    => $values->{'id'}
                                                 });
                                                 
        return 1;
    }
    # Création
    else
    {
        my $artId = $db->insert('p_articleref', {
                                                 'art_sta_id' => STATE_ACTIVE,
                                                 'art_name'   => $values->{'name'},
                                                 'art_code'   => $values->{'code'},
                                                 'art_value'  => $values->{'value'},
                                                 'art_label'  => $values->{'label'},
                                                 'art_vat_id' => $values->{'vat.id'},
                                                });
        
        return $artId;
    }
}

# Function: delete
# Supprime une référence article
#
# Parameters:
# string    $countryId   - Pays
# integer   $id          - Id de la prestation
#
# Returns:
# integer
sub delete
{
    my ($countryId, $id) = @_;
    
    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    
    return $db->update('p_articleref', {'art_sta_id' => STATE_INACTIVE}, {'art_id' => $id});
}

1;