use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::StartCode::Static
# Module permettant de gérer les codes de démarrage statiques
package LOC::StartCode::Static;


# Constants: Portée
# SCOPE - Portée statique
use constant
{
    SCOPE => 'static'
};

# Constants: Types de codes de démarrage
# TYPE_AFTERSALES         - Code SAV
# TYPE_TECHNICIAN         - Code technicien
# TYPE_DRIVER             - Code chauffeur
# TYPE_HELP               - Code de secours
# TYPE_EXTERNALDRIVER     - Code sous-traitant transport
# TYPE_EXTERNALTECHNICIAN - Code sous-traitant technique
use constant
{
    TYPE_AFTERSALES         => 'SAV',
    TYPE_TECHNICIAN         => 'TECHNICIEN',
    TYPE_DRIVER             => 'CHAUFFEUR',
    TYPE_HELP               => 'SECOURS',
    TYPE_EXTERNALDRIVER     => 'SSTRAITTSP',
    TYPE_EXTERNALTECHNICIAN => 'SSTRAITTECH'
};


# Constants: Récupération d'informations sur les codes de démarrage
# GETINFOS_ALL         - Récupération de l'ensemble des informations
# GETINFOS_BASE        - Récupération des informations de base
# GETINFOS_RULESLIST   - Récupération des règles
# GETINFOS_MACHINESIDS - Récupération des identifiants des machines associées
use constant
{
    GETINFOS_ALL         => 0xFFFFFFFF,
    GETINFOS_BASE        => 0x00000000,
    GETINFOS_RULESLIST   => 0x00000001,
    GETINFOS_MACHINESIDS => 0x00000002
};

use strict;

# Modules internes
use LOC::StartCode;
use LOC::Machine;
use LOC::Util;
use LOC::EndUpdates;
use LOC::Db;


my $fatalErrMsg = 'Erreur fatale: module LOC::StartCode::Static, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";

# Liste des erreurs:
#
# 0x0001 => Erreur fatale
# 0x0002 => Code déjà régénéré




# Function: getInfos
# Retourne les infos d'un code de démarrage statique
#
# Contenu du hashage : voir LOC::StartCode::getInfos
#
#
# Parameters:
# string  $countryId  - Pays
# string  $id         - Identifiant du code de démarrage
# int     $flags      - Flags (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}


# Function: getList
# Retourne la liste des codes de démarrage statiques
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres (facultatif)
# int     $flags      - Flags pour le choix des colonnes (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Liste des codes de démarrage
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;

    if (ref($tabFilters) ne 'HASH')
    {
        $tabFilters = {};
    }
    $tabFilters->{'typeScope'} = SCOPE;

    my $tab = &LOC::StartCode::getList($countryId, $format, $tabFilters, $flags, $tabOptions);

    return (wantarray ? %$tab : $tab);
}


# Function: getTypeInfos
# Retourne les infos d'un type de code de démarrage statique
#
# Contenu du hashage : voir LOC::StartCode::getTypeInfos
#
#
# Parameters:
# string  $countryId  - Pays
# string  $idOrCode   - Identifiant ou code du code de démarrage
# int     $flags      - Flags (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getTypeInfos
{
    my ($countryId, $idOrCode, $flags, $tabOptions) = @_;

    my $tab;
    if (&LOC::Util::isNumeric($idOrCode))
    {
        $tab = &getTypesList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $idOrCode}, $flags, $tabOptions);
    }
    else
    {
        $tab = &getTypesList($countryId, LOC::Util::GETLIST_ASSOC, {'code' => $idOrCode}, $flags, $tabOptions);
    }

    my ($id, $tabInfos) = each(%$tab);

    return ($tabInfos ? (wantarray ? %$tabInfos : $tabInfos) : undef);
}


# Function: getTypesList
# Retourne la liste des types de codes de démarrage statiques
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres (facultatif)
# int     $flags      - Flags pour le choix des colonnes (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Liste des codes de démarrage
sub getTypesList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (ref($tabFilters) ne 'HASH')
    {
        $tabFilters = {};
    }
    $tabFilters->{'scope'} = SCOPE;

    my $tab = &LOC::StartCode::getTypesList($countryId, $format, $tabFilters, $flags, $tabOptions);

    return (wantarray ? %$tab : $tab);
}


# Function: generate
# Génère un nouveau code de démarrage statique
#
# Parameters:
# string  $countryId    - Identifiant du pays
# int     $typeIdOrCode - Identifiant ou code du type code
# int     $startCodeId  - Identifiant du digicode actuel
# int     $userId       - Identifiant de l'utilisateur responsable
# hashref $tabOptions   - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors    - Tableau d'erreurs
# hashref $tabEndUpds   - Mises à jour de fin
#
# Returns:
# int - Identifiant du code de démarrage créé
sub generate
{
    my ($countryId, $typeIdOrCode, $startCodeId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Si la télématique n'est pas activée alors on ne fait rien
    if (!&LOC::StartCode::isTelematActived($countryId))
    {
        return 1;
    }

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Vérification de la présence de l'id du digicode actuel
    if (!$startCodeId)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'generate', $_[0], __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, $_[1]);
    };

    # Récupération des informations sur le type de code
    my $tabTypeInfos = &getTypeInfos($countryId, $typeIdOrCode);
    if (!$tabTypeInfos)
    {
        # Erreur fatale !
        &$rollBackAndError(__LINE__, 0x0001);
        return 0;
    }

    # Récupération de la liste des machines équipées pour la télématique
    my $tabFilters = {
        'isTelematic'    => 1,
        'telematicScope' => SCOPE
    };
    my $tabMachinesIds = &LOC::Machine::getList($countryId, LOC::Util::GETLIST_IDS, $tabFilters);
    if (!$tabMachinesIds)
    {
        # Erreur fatale !
        &$rollBackAndError(__LINE__, 0x0001);
        return 0;
    }

    # Suppression de l'ancien code du même type (si il y en a un)
    my $tabCodesFilters = {
        'typeId'  => $tabTypeInfos->{'id'},
        'stateId' => LOC::StartCode::STATE_ACTIVE
    };
    my $tabOldCodesIds = &getList($countryId,
                               LOC::Util::GETLIST_IDS,
                               $tabCodesFilters,
                               GETINFOS_BASE);
    # Il ne peut y en avoir qu'un maximum
    if (@$tabOldCodesIds > 1)
    {
        # Erreur fatale !
        &$rollBackAndError(__LINE__, 0x0001);
        return 0;
    }
    elsif (@$tabOldCodesIds == 1)
    {
        # Si le code précédent est déjà supprimé => Erreur
        if ($tabOldCodesIds->[0] != $startCodeId)
        {
            # Erreur fatale !
            &$rollBackAndError(__LINE__, 0x0002);
            return 0;
        }

        # Si il y avait un code du même type on le supprime
        if (!&LOC::StartCode::delete(
                    $countryId,
                    $tabOldCodesIds->[0],
                    $userId, {}, {}, $tabEndUpds))
        {
            # Erreur fatale !
            &$rollBackAndError(__LINE__, 0x0001);
            return 0;
        }
    }


    # Insertion du nouveau code
    my $startCodeId = &LOC::StartCode::insert(
                $countryId,
                $tabTypeInfos->{'id'}, $tabMachinesIds,
                undef, undef,
                $userId, {}, {}, $tabEndUpds
    );
    if (!$startCodeId)
    {
        # Erreur fatale !
        &$rollBackAndError(__LINE__, 0x0001);
        return 0;
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            &$rollBackAndError(__LINE__, 0x0001);
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return $startCodeId;
}


# Function: synchronizeAll
# Envoie les codes statiques sur les machines qui ne les ont pas déjà
#
# Parameters:
# string  $countryId  - Pays
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, 0 sinon
sub synchronizeAll
{
    my ($countryId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Si la télématique n'est pas activée alors on ne fait rien
    if (!&LOC::StartCode::isTelematActived($countryId))
    {
        return 1;
    }

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'synchronizeAll', $_[0], __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, $_[1]);
    };


    # Récupération de la liste des codes statiques
    my $tabStaticStartCodesIds = &getList($countryId, LOC::Util::GETLIST_IDS,
                                          {'stateId' => LOC::StartCode::STATE_ACTIVE});
    if (!defined $tabStaticStartCodesIds)
    {
        # Erreur fatale
        &$rollBackAndError(__LINE__, 0x0001);
        return 0;
    }

    # Récupération de la liste des machines
    my $tabMachinesIds = &LOC::Machine::getList($countryId,
                                                LOC::Util::GETLIST_IDS,
                                                {'isTelematic'    => 1,
                                                 'telematicScope' => SCOPE,
                                                 'countryId'      => $countryId});
    if (!defined $tabMachinesIds)
    {
        # Erreur fatale
        &$rollBackAndError(__LINE__, 0x0001);
        return 0;
    }

    if (@$tabStaticStartCodesIds > 0 && @$tabMachinesIds > 0)
    {
        foreach my $machineId (@$tabMachinesIds)
        {
            # Récupération des codes présents sur la machine
            my $tabMachineStaticStartCodesIds = &getList(
                $countryId,
                LOC::Util::GETLIST_IDS,
                {
                    'id'   => $tabStaticStartCodesIds,
                    'rule' => {
                        'machineId' => $machineId,
                        'stateId'   => LOC::StartCode::STATE_RULE_ACTIVE
                    }
                },
                GETINFOS_BASE
            );

            # On vérifie la présence de chaque règle statique
            foreach my $staticStartCodeId (@$tabStaticStartCodesIds)
            {
                if (!&LOC::Util::in_array($staticStartCodeId, $tabMachineStaticStartCodesIds))
                {
                    my $result = &LOC::StartCode::update($countryId,
                                                         $staticStartCodeId,
                                                         $machineId,
                                                         undef, undef,
                                                         $userId, {'isAddedMachines' => 1}, {}, $tabEndUpds);
                    # Ajout de la règle pour la machine
                    if ($result != $staticStartCodeId)
                    {
                        # Erreur fatale
                        &$rollBackAndError(__LINE__, 0x0001);
                        return 0;
                    }
                }
            }
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale
            &$rollBackAndError(__LINE__, 0x0001);
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}

1;