use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::StartCode::Contract::Rent
# Module permettant de gérer les codes de démarrage liés aux contrats
package LOC::StartCode::Contract::Rent;



# Constants: Portée
# SCOPE - Portée contrat
use constant
{
    SCOPE => 'rentcontract'
};


# Constants: États du lien entre le code et le contrat
# LINK_STATE_ACTIVE  - Lien actif
# LINK_STATE_DELETED - Lien supprimé
use constant
{
    LINK_STATE_ACTIVE  => 'GEN01',
    LINK_STATE_DELETED => 'GEN03'
};


# Constants: Types de codes de démarrage
# TYPE_CUSTOMER   - Code client
# TYPE_BACKUP     - Code client backup
use constant
{
    TYPE_CUSTOMER => 'CLIENT',
    TYPE_BACKUP   => 'BACKUP'
};


# Constants: Récupération d'informations sur les codes de démarrage
# GETINFOS_ALL             - Récupération de l'ensemble des informations
# GETINFOS_BASE            - Récupération des informations de base
# GETINFOS_RULESLIST       - Récupération des règles
# GETINFOS_MACHINESIDS     - Récupération des identifiants des machines associées
# GETINFOS_LINKEDCONTRACTS - Récupération des contrats liés
use constant
{
    GETINFOS_ALL             => 0xFFFFFFFF,
    GETINFOS_BASE            => 0x00000000,
    GETINFOS_RULESLIST       => 0x00000001,
    GETINFOS_MACHINESIDS     => 0x00000002,

    GETINFOS_LINKEDCONTRACTS => 0x00010000
};


# Constants: Erreurs
# ERROR_FATAL                - Erreur fatale
# ERROR_NONMOBILEPHONENUMBER - Numéro de téléphone non mobile
use constant
{
    ERROR_FATAL                => 0x0001,

    ERROR_NONMOBILEPHONENUMBER => 0x0100
};


use strict;

# Modules internes
use LOC::Globals;
use LOC::StartCode;
use LOC::Contract::Rent;
use LOC::Machine;
use LOC::Util;
use LOC::EndUpdates;
use LOC::Db;
use LOC::Date;
use List::Util;
use Tie::IxHash;
use LOC::Sms;
use LOC::Template;


my $fatalErrMsg = 'Erreur fatale: module LOC::StartCode::Contract::Rent, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";


# Function: getInfos
# Retourne les infos d'un code de démarrage lié à un contrat
#
# Contenu du hashage : voir LOC::StartCode::getInfos
#
#
# Parameters:
# string  $countryId  - Pays
# string  $id         - Identifiant du code de démarrage
# int     $flags      - Flags (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}


# Function: getList
# Retourne la liste des codes de démarrage liés à un contrat
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres (facultatif)
# int     $flags      - Flags pour le choix des colonnes (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Liste des codes de démarrage
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;

    if (ref($tabFilters) ne 'HASH')
    {
        $tabFilters = {};
    }
    $tabFilters->{'typeScope'} = SCOPE;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    if (defined $tabFilters->{'contractId'})
    {
        my $query = '
SELECT
    src_sco_id
FROM f_startcode_rentcontract
INNER JOIN CONTRAT tbl_f_contract
ON tbl_f_contract.CONTRATAUTO = src_rct_id AND src_sta_id = ' . $db->quote(LINK_STATE_ACTIVE) . '
WHERE src_rct_id IN (' . &LOC::Util::join(', ', $tabFilters->{'contractId'}) . ');';
        my $tabIds = $db->fetchCol($query);

        if (@$tabIds > 0)
        {
            $tabFilters->{'id'} = $tabIds;
        }
        else
        {
            return (wantarray ? () : ($format == LOC::Util::GETLIST_IDS ? [] : {}));
        }

    }

    # Récupération des codes
    my $tab = &LOC::StartCode::getList($countryId, $format, $tabFilters, $flags, $tabOptions);


    # Formatage des champs
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        # Options pour la récupération des règles
        my $tabLinkedContractsFilters;
        if ($flags & GETINFOS_LINKEDCONTRACTS)
        {
            $tabLinkedContractsFilters = (defined $tabOptions->{'getLinkedContractsFilters'} ?
                                        $tabOptions->{'getLinkedContractsFilters'} : {});
        }

        foreach my $tabRow (values %$tab)
        {
            # Liste des contrats associés
            if ($flags & GETINFOS_LINKEDCONTRACTS)
            {
                $tabRow->{'linkedContracts'} = &_getLinkedContracts($db, $countryId, $tabRow->{'id'}, $tabLinkedContractsFilters);
                $tabRow->{'period'} = &_calculatePeriodFromContracts($tabRow->{'linkedContracts'});
            }
        }
    }

    return (wantarray ? %$tab : $tab);
}


# Function: getTypeInfos
# Retourne les infos d'un type de code de démarrage contrat
#
# Contenu du hashage : voir LOC::StartCode::getTypeInfos
#
#
# Parameters:
# string  $countryId  - Pays
# string  $idOrCode   - Identifiant ou code du code de démarrage
# int     $flags      - Flags (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getTypeInfos
{
    my ($countryId, $idOrCode, $flags, $tabOptions) = @_;

    my $tab;
    if (&LOC::Util::isNumeric($idOrCode))
    {
        $tab = &getTypesList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $idOrCode}, $flags, $tabOptions);
    }
    else
    {
        $tab = &getTypesList($countryId, LOC::Util::GETLIST_ASSOC, {'code' => $idOrCode}, $flags, $tabOptions);
    }

    my ($id, $tabInfos) = each(%$tab);

    return ($tabInfos ? (wantarray ? %$tabInfos : $tabInfos) : undef);
}


# Function: getTypesList
# Retourne la liste des types de codes de démarrage contrats
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres (facultatif)
# int     $flags      - Flags pour le choix des colonnes (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Liste des codes de démarrage
sub getTypesList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (ref($tabFilters) ne 'HASH')
    {
        $tabFilters = {};
    }
    $tabFilters->{'scope'} = SCOPE;

    my $tab = &LOC::StartCode::getTypesList($countryId, $format, $tabFilters, $flags, $tabOptions);

    return (wantarray ? %$tab : $tab);
}


# Function: save
# Mise à jour des codes de démarrage liés à un contrat
#
# Parameters:
# string  $countryId    - Identifiant du pays
# int     $contractId   - Identifiant du contrat lié
# int     $userId       - Identifiant de l'utilisateur responsable
# hashref $tabOptions   - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors    - Tableau d'erreurs
# hashref $tabEndUpds   - Mises à jour de fin
#
# Returns:
# int - Identifiant du code de démarrage créé
sub save
{
    my ($countryId, $contractId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Si la télématique n'est pas activée alors on ne fait rien
    if (!&LOC::StartCode::isTelematActived($countryId))
    {
        return 1;
    }

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'save', $_[0], __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, $_[1]);
        return 0;
    };


    # Recherche des codes affectés à ce contrat
    my @tabContractCodeTypes = (TYPE_CUSTOMER, TYPE_BACKUP);
    my $tabFilters = {
        'contractId' => $contractId,
        'typeCode'   => \@tabContractCodeTypes,
        'stateId'    => LOC::StartCode::STATE_ACTIVE
    };
    my $tabCodesOptions = {
        'getRulesListFilters' => {
            'stateId' => LOC::StartCode::STATE_RULE_ACTIVE
        }
    };
    my $tabContractStartCodes = &getList($countryId, LOC::Util::GETLIST_ASSOC,
                                         $tabFilters,
                                         GETINFOS_RULESLIST | GETINFOS_LINKEDCONTRACTS,
                                         $tabCodesOptions);
    if (!$tabContractStartCodes)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }


    # Mise à plat des codes
    my %tabStartCodesByType = ();
    foreach my $tabStartCode (values %$tabContractStartCodes)
    {
        my $tabFormattedStartCode = &_formatStartCode($tabStartCode);

        # Il ne peut pas y avoir plus d'un code du même type rattaché au contrat
        if (!$tabFormattedStartCode || exists $tabStartCodesByType{$tabFormattedStartCode->{'typeCode'}})
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }

        $tabStartCodesByType{$tabFormattedStartCode->{'typeCode'}} = $tabFormattedStartCode;
    }

    # Récupération des informations du contrat et de la machine attribuée
    my $tabContractInfos = &LOC::Contract::Rent::getInfos($countryId, $contractId, LOC::Contract::Rent::GETINFOS_BASE);
    if (!$tabContractInfos)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # La machine doit être équipée pour la télématique
    my $tabMachineInfos = undef;
    if ($tabContractInfos->{'machine.id'})
    {
        $tabMachineInfos = &LOC::Machine::getInfos($countryId, $tabContractInfos->{'machine.id'}, LOC::Machine::GETINFOS_TELEMATICS);
        if (!$tabMachineInfos)
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Tableau des codes à mettre à jour
    my @tabStartCodesToUpdate = ();

    # Si il y a une machine attribuée au contrat et qu'il n'est pas annulé...
    if ($tabContractInfos->{'state.id'} ne LOC::Contract::Rent::STATE_CANCELED &&
        $tabMachineInfos && $tabMachineInfos->{'telematicId'} &&
        &LOC::Util::in_array(SCOPE, $tabMachineInfos->{'telematicScope'}))
    {
        foreach my $typeCode (@tabContractCodeTypes)
        {
            # Y'a-t-il un code existant de ce type pour ce contrat ?
            my $tabExistingStartCode = (exists $tabStartCodesByType{$typeCode} ? $tabStartCodesByType{$typeCode} : undef);

            # Si il y a un code existant non dormant de ce type ou que c'est un pré-contrat
            # alors on génère un nouveau code ou on modifie celui existant
            if (($tabExistingStartCode && $tabExistingStartCode->{'hasBeenActivated'}) ||
                $tabContractInfos->{'state.id'} eq LOC::Contract::Rent::STATE_PRECONTRACT)
            {
                # Si il n'y a pas de code de ce type ou un code "dormant" ou un changement de machine
                if (!$tabExistingStartCode ||
                    !$tabExistingStartCode->{'hasBeenActivated'} ||
                    $tabExistingStartCode->{'machine.id'} != $tabContractInfos->{'machine.id'})
                {
                    # Récupération du dernier code non dormant de la machine
                    my $tabFilters = {
                        'typeCode'         => $typeCode,
                        'rule'      => {
                            'machineId' => $tabContractInfos->{'machine.id'}
                        },
                        'hasBeenActivated' => 1,
                        'stateId'          => LOC::StartCode::STATE_ACTIVE
                    };
                    my $tabOpts = {
                        'limit'               => 1,
                        'orderBy'             => ['activationDate:DESC'],
                        'getRulesListFilters' => {
                            'stateId' => LOC::StartCode::STATE_RULE_ACTIVE
                        }
                    };
                    my $tabFoundStartCodes = &getList(
                        $countryId, LOC::Util::GETLIST_ASSOC, $tabFilters,
                        GETINFOS_RULESLIST | GETINFOS_LINKEDCONTRACTS,
                        $tabOpts
                    );
                    if (!$tabFoundStartCodes)
                    {
                        # Erreur fatale !
                        return &$rollBackAndError(__LINE__, ERROR_FATAL);
                    }

                    my $tabPreservedStartCode = (values %$tabFoundStartCodes)[0];
                    my $isCodePreserved = 0;
                    if ($tabPreservedStartCode)
                    {
                        # Délai maximum pour la conservation du digicode (en jours calendaires)
                        my $nbPreservationMaxDays = &LOC::Characteristic::getCountryValueByCode('DIGIDELTACONS', $countryId);

                        # Il faut qu'au moins un contrat associé au code soit :
                        # - affecté au même client
                        # - non annulé
                        # - avec une date de fin >= à la date de début du contrat courant - délai maximum pour la conservation du digicode
                        my $minDate = &LOC::Date::addDays($tabContractInfos->{'beginDate'}, -$nbPreservationMaxDays);
                        foreach my $tabLinkedContract (values %{$tabPreservedStartCode->{'linkedContracts'}})
                        {
                            if ($tabLinkedContract->{'state.id'} ne LOC::Contract::Rent::STATE_CANCELED &&
                                $tabLinkedContract->{'customer.id'} == $tabContractInfos->{'customer.id'} &&
                                $tabLinkedContract->{'endDate'} ge $minDate)
                            {
                                $isCodePreserved = 1;
                            }
                        }
                    }

                    # En priorité, on essaie de conserver ce code
                    if ($isCodePreserved)
                    {
                        # Si un code existait
                        if ($tabExistingStartCode)
                        {
                            # on supprime le lien
                            $tabExistingStartCode->{'linkedContracts'}->{$contractId}->{'@action'} = 'remove';

                            # on le supprime définitivement si c'était un code "dormant"
                            if (values %{$tabExistingStartCode->{'linkedContracts'}} == 1)
                            {
                                $tabExistingStartCode->{'@action'}  = 'delete';
                            }

                            # Ajout du code à mettre à jour dans le tableau
                            push(@tabStartCodesToUpdate, $tabExistingStartCode);
                        }

                        my $tabStartCode = &_formatStartCode($tabPreservedStartCode);
                        if (!$tabStartCode)
                        {
                            # Erreur fatale !
                            return &$rollBackAndError(__LINE__, ERROR_FATAL);
                        }

                        # Ajout du lien entre le code et le contrat
                        $tabStartCode->{'linkedContracts'}->{$contractId} = {
                            '@action'   => 'add',
                            'id'        => $tabContractInfos->{'id'},
                            'beginDate' => $tabContractInfos->{'beginDate'},
                            'endDate'   => $tabContractInfos->{'endDate'},
                            'agency.id' => $tabContractInfos->{'agency.id'}
                        };

                        # Ajout du code à mettre à jour dans le tableau
                        push(@tabStartCodesToUpdate, $tabStartCode);
                    }
                    # En second, on modifie le code existant
                    elsif ($tabExistingStartCode)
                    {
                        # Réactivation du code avec la nouvelle machine
                        $tabExistingStartCode->{'@action'}        = 'save';

                        # Il s'agit soit d'une réactivation, soit d'un changement de machine
                        $tabExistingStartCode->{'@newMachine.id'} = $tabContractInfos->{'machine.id'};

                        # Ajout du code à mettre à jour dans le tableau
                        push(@tabStartCodesToUpdate, $tabExistingStartCode);
                    }
                    # Sinon on en créé un nouveau
                    else
                    {
                        # Création d'un nouveau code
                        my $tabStartCode = {
                            '@action'          => 'save',
                            '@newMachine.id'   => $tabContractInfos->{'machine.id'},

                            'id'               => undef,
                            'typeCode'         => $typeCode,
                            'hasBeenActivated' => 1,
                            'machine.id'       => undef,
                            'hasBeginDate'     => ($typeCode eq TYPE_CUSTOMER ? 1 : 0),
                            'beginDate'        => undef,
                            'hasEndDate'       => ($typeCode eq TYPE_CUSTOMER ? 1 : 0),
                            'endDate'          => undef,
                            'valueDate'        => &LOC::Date::getMySQLDate(),
                            'linkedContracts'  => {
                                $contractId => {
                                    '@action'   => 'add',
                                    'id'        => $tabContractInfos->{'id'},
                                    'beginDate' => $tabContractInfos->{'beginDate'},
                                    'endDate'   => $tabContractInfos->{'endDate'},
                                    'agency.id' => $tabContractInfos->{'agency.id'}
                                }
                            }
                        };

                        # Ajout du code à mettre à jour dans le tableau
                        push(@tabStartCodesToUpdate, $tabStartCode);
                    }
                }
                else
                {
                    # Ajout du code à mettre à jour dans le tableau
                    push(@tabStartCodesToUpdate, $tabExistingStartCode);
                }
            }
        }
    }
    else
    {
        # Désactivation de tous les codes associés au contrat si il n'y a pas de machine télématique attribuée
        # ou qu'il est annulé
        foreach my $tabStartCode (values %tabStartCodesByType)
        {
            if ($tabStartCode->{'hasBeenActivated'})
            {
                if (values %{$tabStartCode->{'linkedContracts'}} == 1)
                {
                    # Le code passe dans le statut "dormant" et la machine est désattribuée
                    $tabStartCode->{'@action'}        = 'clear';
                    $tabStartCode->{'@newMachine.id'} = undef;
                }
                else
                {
                    # On supprime le lien avec ce contrat
                    $tabStartCode->{'linkedContracts'}->{$contractId}->{'@action'} = 'remove';
                }

                # Ajout du code à mettre à jour dans le tableau
                push(@tabStartCodesToUpdate, $tabStartCode);
            }
        }
    }

    # Exécution de toutes les mises à jour
    if (!&_updateStartCodes($db, $countryId, \@tabStartCodesToUpdate, $userId, $tabEndUpds))
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: save
# Génération des codes de démarrage liés à un contrat
#
# Parameters:
# string  $countryId    - Identifiant du pays
# int     $contractId   - Identifiant du contrat lié
# int     $userId       - Identifiant de l'utilisateur responsable
# hashref $tabOptions   - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors    - Tableau d'erreurs
# hashref $tabEndUpds   - Mises à jour de fin
#
# Returns:
# int - Identifiant du code de démarrage créé
sub generateAll
{
    my ($countryId, $contractId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Si la télématique n'est pas activée alors on ne fait rien
    if (!&LOC::StartCode::isTelematActived($countryId))
    {
        return 1;
    }

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'generateAll', $_[0], __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, $_[1]);
        return 0;
    };

    # Récupération des informations du contrat
    my $tabContractInfos = &LOC::Contract::Rent::getInfos($countryId, $contractId, LOC::Contract::Rent::GETINFOS_BASE);
    if (!$tabContractInfos ||
        !$tabContractInfos->{'machine.id'} ||
        $tabContractInfos->{'state.id'} eq LOC::Contract::Rent::STATE_CANCELED)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # La machine doit être équipée pour la télématique
    my $tabMachineInfos = &LOC::Machine::getInfos($countryId, $tabContractInfos->{'machine.id'}, LOC::Machine::GETINFOS_TELEMATICS);
    if (!$tabMachineInfos ||
        !$tabMachineInfos->{'telematicId'} ||
        !&LOC::Util::in_array(SCOPE, $tabMachineInfos->{'telematicScope'}))
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Recherche des codes affectés à ce contrat
    my @tabContractCodeTypes = (TYPE_CUSTOMER, TYPE_BACKUP);
    my $tabFilters = {
        'contractId' => $contractId,
        'typeCode'   => \@tabContractCodeTypes,
        'stateId'    => LOC::StartCode::STATE_ACTIVE
    };
    my $tabCodesOptions = {
        'getRulesListFilters' => {
            'stateId' => LOC::StartCode::STATE_RULE_ACTIVE
        }
    };
    my $tabContractStartCodes = &getList($countryId, LOC::Util::GETLIST_ASSOC,
                                         $tabFilters,
                                         GETINFOS_RULESLIST | GETINFOS_LINKEDCONTRACTS,
                                         $tabCodesOptions);
    if (!$tabContractStartCodes)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Mise à plat des codes
    my %tabStartCodesByType = ();
    foreach my $tabStartCode (values %$tabContractStartCodes)
    {
        my $tabFormattedStartCode = &_formatStartCode($tabStartCode);

        # Il ne peut pas y avoir plus d'un code du même type rattaché au contrat
        if (!$tabFormattedStartCode || exists $tabStartCodesByType{$tabFormattedStartCode->{'typeCode'}})
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }

        $tabStartCodesByType{$tabFormattedStartCode->{'typeCode'}} = $tabFormattedStartCode;
    }


    # Tableau des codes à mettre à jour
    my @tabStartCodesToUpdate = ();

    foreach my $typeCode (@tabContractCodeTypes)
    {
        # Y'a-t-il un code existant de ce type pour ce contrat ?
        my $tabExistingStartCode = (exists $tabStartCodesByType{$typeCode} ? $tabStartCodesByType{$typeCode} : undef);

        # Création d'un nouveau code
        my $tabNewStartCode = {
            '@action'          => 'save',
            '@newMachine.id'   => $tabContractInfos->{'machine.id'},

            'id'               => undef,
            'typeCode'         => $typeCode,
            'hasBeenActivated' => 1,
            'machine.id'       => undef,
            'hasBeginDate'     => ($typeCode eq TYPE_CUSTOMER ? 1 : 0),
            'beginDate'        => undef,
            'hasEndDate'       => ($typeCode eq TYPE_CUSTOMER ? 1 : 0),
            'endDate'          => undef,
            'valueDate'        => &LOC::Date::getMySQLDate(),
            'linkedContracts'  => {}
        };

        # Code existant du même type
        if ($tabExistingStartCode)
        {
            my $hasLinksToRemove = 0;
            my $hasActiveLinks = 0;
            foreach my $tabLinkedContract (values %{$tabExistingStartCode->{'linkedContracts'}})
            {
                # Date de début du contrat lié postérieure à la date de début du contrat principal
                # (permet de ne régénérer que les codes postérieurs au contrat sur lequel on a cliqué sur "Régénérer").
                if ($tabLinkedContract->{'beginDate'} ge $tabContractInfos->{'beginDate'})
                {
                    # Ajout du lien avec le contrat sur le nouveau code
                    $tabNewStartCode->{'linkedContracts'}->{$tabLinkedContract->{'id'}} = {
                        '@action'   => 'add',
                        'id'        => $tabLinkedContract->{'id'},
                        'beginDate' => $tabLinkedContract->{'beginDate'},
                        'endDate'   => $tabLinkedContract->{'endDate'},
                        'agency.id' => $tabLinkedContract->{'agency.id'}
                    };

                    # Suppression du lien avec le contrat sur le code existant
                    $tabExistingStartCode->{'linkedContracts'}->{$tabLinkedContract->{'id'}}->{'@action'} = 'remove';

                    $hasLinksToRemove = 1;
                }
                else
                {
                    $hasActiveLinks = 1;
                }
            }

            # Supprimer le code existant s'il n'y a plus de lien actif
            if (!$hasActiveLinks)
            {
                $tabExistingStartCode->{'@action'} = 'delete';
            }

            # Ajout du code existant dans les codes à mettre à jour seulement s'il y a au moins un lien à supprimer
            if ($hasLinksToRemove)
            {
                push(@tabStartCodesToUpdate, $tabExistingStartCode);
            }
        }
        # Pas de code existant du même type
        else
        {
            # Ajout du lien avec le contrat sur le nouveau code
            $tabNewStartCode->{'linkedContracts'}->{$contractId} = {
                '@action'   => 'add',
                'id'        => $tabContractInfos->{'id'},
                'beginDate' => $tabContractInfos->{'beginDate'},
                'endDate'   => $tabContractInfos->{'endDate'},
                'agency.id' => $tabContractInfos->{'agency.id'}
            };
        }

        push(@tabStartCodesToUpdate, $tabNewStartCode);
    }


    # Exécution de toutes les mises à jour
    if (!&_updateStartCodes($db, $countryId, \@tabStartCodesToUpdate, $userId, $tabEndUpds))
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }


    return 1;
}


# Function: freeEndedCodes
# Libère les codes de démarrage de type contrat réservés,
# ayant une date de fin et dont tous les contrats associés sont facturés totalement
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# int - 1 si c'est un succès, 0 sinon
sub freeEndedCodes
{
    my ($countryId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Si la télématique n'est pas activée alors on ne fait rien
    if (!&LOC::StartCode::isTelematActived($countryId))
    {
        return 1;
    }

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'freeEndedCodes', $_[0], __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, $_[1]);
        return 0;
    };


    # Récupération des codes
    my $tabFilters = {
        'hasBeenActivated' => 1,
        'isCodeReserved'   => 1,
        'stateId'          => LOC::StartCode::STATE_ACTIVE,
        'rule' => {
            'hasEndDate' => 1,
            'stateId'    => LOC::StartCode::STATE_RULE_ACTIVE
        }
    };
    my $tabContractStartCodes = &getList($countryId, LOC::Util::GETLIST_ASSOC,
                                         $tabFilters,
                                         GETINFOS_LINKEDCONTRACTS);
    if (!$tabContractStartCodes)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }


    # Vérification des états de facturation des contrats
    foreach my $tabStartCode (values %$tabContractStartCodes)
    {
        my @tabNotClosedContracts = grep { !$_->{'isClosed'} } values %{$tabStartCode->{'linkedContracts'}};

        # Si tous les contrats associés sont clôturés...
        if (@tabNotClosedContracts == 0)
        {
            # ... alors on libère le code
            &LOC::StartCode::freeCode($countryId, $tabStartCode->{'id'}, $userId, {}, {}, $tabEndUpds);
        }
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: isSmsSendable
# Indique si le SMS est envoyable ou non
#
# Parameters:
# hashref $tabInfos         - Informations sur le code de démarrage
# hashref $tabContractInfos - Informations sur le contrat
#
# Returns:
# hashref - Liste des identifiants des SMS envoyés
sub isSmsSendable
{
    my ($tabInfos, $tabContractInfos) = @_;

    my $SmsEnabled = &LOC::Characteristic::getAgencyValueByCode('DIGISMS', $tabInfos->{'agency.id'});

    # Si la fonctionnalité d'envoi des SMS n'est pas activée
    if (!$SmsEnabled)
    {
        return undef;
    }

    # Si ce n'est pas un code client
    if (!defined $tabInfos->{'type.code'} || $tabInfos->{'type.code'} ne TYPE_CUSTOMER)
    {
        return undef;
    }

    # Si code inactif
    if ($tabInfos->{'state.id'} ne LOC::StartCode::STATE_ACTIVE)
    {
        return 0;
    }

    # Filtrage des règles actives
    my @tabRules = grep {
        $_->{'state.id'} eq LOC::StartCode::STATE_RULE_ACTIVE &&
        ($_->{'application.id'} eq LOC::StartCode::STATE_RULE_APPLICATION_PENDING ||
            $_->{'application.id'} eq LOC::StartCode::STATE_RULE_APPLICATION_DONE) &&
        !defined $_->{'unapplication.id'}
    } values(%{$tabInfos->{'rulesList'}});

    # Si plus d'une règle active
    my $nbRules = scalar @tabRules;
    if ($nbRules != 1)
    {
        return 0;
    }

    my $tabRuleInfos = $tabRules[0];

    # Si mauvaises conditions au niveau de la règle
    if ($tabRuleInfos->{'state.id'} ne LOC::StartCode::STATE_RULE_ACTIVE ||
        $tabRuleInfos->{'application.id'} ne LOC::StartCode::STATE_RULE_APPLICATION_PENDING &&
            $tabRuleInfos->{'application.id'} ne LOC::StartCode::STATE_RULE_APPLICATION_DONE)
    {
        return 0;
    }

    # Si la date de fin de validité du code est passée
    if (defined $tabRuleInfos && $tabRuleInfos->{'endDate'} ne '' &&
        &LOC::Date::compare($tabRuleInfos->{'endDate'}, &LOC::Date::getMySQLDate()) < 0)
    {
        return 0;
    }

    # Si les conditions d'envoi du SMS ne sont pas réunies sur le contrat
    # (pas de numéro de téléphone par exemple)
    if (defined $tabContractInfos && defined $tabContractInfos->{'sendSMS'} &&
        !$tabContractInfos->{'sendSMS'})
    {
        return 0;
    }

    return 1;
}


# Function: sendSms
# Envoi le digicode au client par SMS
#
# Parameters:
# string  $countryId    - Identifiant du pays
# hashref $tabReceivers - Liste des destinataires au format suivant : id contrat => numéro de téléphone
# hashref $tabInfos     - Tableau des informations sur le contenu du SMS :
#                         - machinesFamily.label : libellé complet de la famille de machines
#                         - machine.parkNumber : numéro de parc
#                         - code : digicode
#                         - beginDate : date de début de validité du digicode
#                         - endDate : date de fin de validité du digicode
# hashref $tabErrors    - Tableau d'erreurs
# hashref $tabEndUpds   - Mises à jour de fin
#
# Returns:
# hashref - Liste des identifiants des SMS envoyés
sub sendSms
{
    my ($countryId, $userId, $tabReceivers, $tabInfos, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Si la télématique ou l'envoi de SMS ne sont pas activés alors on ne fait rien
    if (!&LOC::StartCode::isTelematActived($countryId) ||
        !&LOC::Characteristic::getCountryValueByCode('DIGISMS', $countryId))
    {
        return {};
    }

    # Si la date de fin de validité du code est passée alors on ne fait rien
    if (&LOC::Date::compare($tabInfos->{'endDate'}, &LOC::Date::getMySQLDate()) < 0)
    {
        return {};
    }

    if (ref($tabInfos) ne 'HASH')
    {
        $tabInfos = {};
    }


    # Construction du SMS
    my $file = &LOC::Globals::get('cgiPath') . '/cfg/templates/' . $countryId . '/startCode/sms.txt';
    open(TPL, $file);
    my $template = join('', <TPL>);
    close(TPL);

    my $tabReplaces = {
        'machinesFamily.label' => $tabInfos->{'machinesFamily.label'},
        'machine.parkNumber'   => $tabInfos->{'machine.parkNumber'},
        'startCode.code'       => $tabInfos->{'code'},
        'beginDate'            => $tabInfos->{'beginDate'},
        'endDate'              => $tabInfos->{'endDate'},
        'site.label'           => $tabInfos->{'site.label'},
        'site.locality.name'   => $tabInfos->{'site.locality.name'}
    };

    # Formatage et remplacement des données dans le template
    $template =~ s/(\<%(([a-z]{3})\:)?(.*?)\>)/@{[&_formatTemplate($3, $4, $tabReplaces)]}/g;


    my @tabSmsReceivers = ();
    my %tabTraces = ();
    foreach my $contractId (keys(%$tabReceivers))
    {
        my $receiver = $tabReceivers->{$contractId};

        # Destinataire pris en compte s'il n'est pas vide
        if (&LOC::Util::trim($receiver) ne '')
        {
            push(@tabSmsReceivers, $receiver);
            $tabTraces{$contractId} = $receiver;
        }
    }

    # S'il n'y a pas de destinataire, on n'essaye même pas d'envoyer le SMS
    my $nbReceivers = scalar @tabSmsReceivers;
    if ($nbReceivers == 0)
    {
        return {};
    }

    # Envoi du SMS
    my $tabSmsErrors = {};
    my $result = &LOC::Sms::send($template, \@tabSmsReceivers, {}, $tabSmsErrors);
    if (!$result)
    {
        if (&LOC::Util::in_array(LOC::Sms::ERROR_NONMOBILEPHONENUMBER, $tabSmsErrors->{'fatal'}))
        {
            # Numéro de téléphone non mobile
            push(@{$tabErrors->{'fatal'}}, ERROR_NONMOBILEPHONENUMBER);
        }
        else
        {
            # Erreur fatale
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        }
        return 0;
    }

    # Trace d'envoi des SMS
    foreach my $contractId (keys(%tabTraces))
    {
        my $schemaName = &LOC::Db::getSchemaName($countryId);
        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, {
            'schema'  => $schemaName,
            'code'    => LOC::Log::EventType::SMSSTARTCODE,
            'content' => $tabTraces{$contractId}
        });
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return $result;
}


# Function: cleanLinks
# Supprime les liens avec les codes de démarrage obsolètes
#
# Parameters:
# string  $countryId  - Pays
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabOptions - Tableau des options supplémentaires (facultatif). Peut contenir id.
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# int - 1 si c'est un succès, 0 sinon
sub cleanLinks
{
    my ($countryId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Si la télématique n'est pas activée alors on ne fait rien
    if (!&LOC::StartCode::isTelematActived($countryId))
    {
        return 1;
    }

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'cleanLinks', $_[0], __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, $_[1]);
        return 0;
    };

    # Suppression des liens avec des codes obsolètes
    my $query = '
UPDATE f_startcode';
    # Jointure dans le cas où on spécifie une machine
    if (defined $tabOptions->{'machine.id'})
    {
        $query .= '
INNER JOIN f_startcoderule
ON scr_sco_id = sco_id AND scr_mac_id = ' . $tabOptions->{'machine.id'};
    }
    $query .= '
INNER JOIN f_startcode_rentcontract
ON src_sco_id = sco_id AND src_sta_id = ' . $db->quote(LINK_STATE_ACTIVE) . '
SET src_sta_id = ' . $db->quote(LINK_STATE_DELETED) . ', src_date_deletion = NOW()
WHERE sco_sta_id = ' . $db->quote(LOC::StartCode::STATE_OBSOLETE) . ';';

    if (!$db->query($query))
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: _formatTemplate
# formate une chaîne de caractères
#
# Params:
# string    $code          - quel formatage à appliquer
# string    $value         - la chaîne template à formater
# hashref   $tabReplaces   - les valeurs de la chaîne à mettre dans le template
#
# Return:
# string - la chaîne formatée
sub _formatTemplate
{
    my ($code, $value, $tabReplaces) = @_;

    if ($code eq 'dte')
    {
        return &LOC::Template::formatDate($value, $tabReplaces);
    }

    return (exists $tabReplaces->{$value} ? $tabReplaces->{$value} : $value);
}


# Function: _updateStartCodes
# Met à jour tous les codes de démarrage d'un contrat (les codes doivent d'être formatés avec la méthode _formatStartCode())
#
# Parameters:
# LOC::Db::Adapter $db            - Connexion à la base de données
# string           $countryId     - Pays
# arrayref         $tabStartCodes - Tableau des codes de démarrage
# int              $userId        - Utilisateur responsable
# hashref          $tabEndUpds    - Mises à jour de fin
#
# Returns:
# bool
sub _updateStartCodes
{
    my ($db, $countryId, $tabStartCodes, $userId, $tabEndUpds) = @_;

    # Identifiants des codes à supprimer pour les exclure des recherches
    my @tabIdsToDelete = map { $_->{'id'} } grep { $_->{'@action'} eq 'delete' } @$tabStartCodes;

    # Détection des changements de machine
    my @tabExcludedCodes = ();
    foreach my $tabStartCode (@$tabStartCodes)
    {
        # Récupération du code précédent du même type dans le cas d'une attribution ou désattribution
        if (exists $tabStartCode->{'@newMachine.id'})
        {
            # Identifiants des codes à exclure pour les recherches
            my @tabExcludedIds = @tabIdsToDelete;
            if ($tabStartCode->{'id'})
            {
                push(@tabExcludedIds, $tabStartCode->{'id'});
            }

            # Code précédent du même type de la machine attribuée si il y en a une
            if (defined $tabStartCode->{'@newMachine.id'})
            {
                my $tabFilters = {
                    'typeCode'  => $tabStartCode->{'typeCode'},
                    'rule'      => {
                        'machineId' => $tabStartCode->{'@newMachine.id'}
                    },
                    'hasBeenActivated' => 1,
                    'stateId'   => LOC::StartCode::STATE_ACTIVE
                };
                if (@tabExcludedIds > 0)
                {
                    $tabFilters->{'!id'} = \@tabExcludedIds;
                }
                my $tabOpts = {
                    'limit'   => 1,
                    'orderBy' => ['activationDate:DESC']
                };
                my $flags = GETINFOS_BASE;
                # Dans le cas d'un code BACKUP, on devoir le modifier
                if ($tabStartCode->{'typeCode'} eq TYPE_BACKUP)
                {
                    $flags = GETINFOS_RULESLIST | GETINFOS_LINKEDCONTRACTS;
                    $tabOpts->{'getRulesListFilters'} = {
                        'stateId' => LOC::StartCode::STATE_RULE_ACTIVE
                    };
                }
                my $tabOldStartCodes = &getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                $tabFilters, $flags, $tabOpts);
                if (!$tabOldStartCodes)
                {
                    return 0;
                }
                my @tabTmp = values %$tabOldStartCodes;
                if (@tabTmp > 0)
                {
                    # Référence sur la liste des codes exclus
                    $tabStartCode->{'excludedCodes'} = \@tabExcludedCodes;

                    # Ce code est exclus pour la génération d'un nouveau code
                    push(@tabExcludedCodes, $tabTmp[0]->{'code'});

                    # Ajout de la modification du code BACKUP précédent
                    if ($tabStartCode->{'typeCode'} eq TYPE_BACKUP)
                    {
                        # Recherche si ce code est déjà dans la liste des codes à modifier
                        my $tabOldStartCode = &List::Util::first(sub { $_->{'id'} == $tabTmp[0]->{'id'} }, @$tabStartCodes);
                        if (!$tabOldStartCode)
                        {
                            # Sinon on le rajoute
                            $tabOldStartCode = &_formatStartCode($tabTmp[0]);
                            if (!$tabOldStartCode)
                            {
                                return 0;
                            }
                            push(@$tabStartCodes, $tabOldStartCode);
                        }

                        # Le BACKUP doit avoir une date de fin
                        $tabOldStartCode->{'hasEndDate'} = 1;
                    }
                }
            }

            # Code précédent du même type de l'ancienne machine dans le cas d'une désattribution
            if ($tabStartCode->{'machine.id'})
            {
                if ($tabStartCode->{'typeCode'} eq TYPE_BACKUP)
                {
                    my $tabFilters = {
                        'typeCode'  => TYPE_BACKUP,
                        'rule'      => {
                            'machineId' => $tabStartCode->{'machine.id'}
                        },
                        'hasBeenActivated' => 1,
                        'stateId'   => LOC::StartCode::STATE_ACTIVE
                    };
                    if (@tabExcludedIds > 0)
                    {
                        $tabFilters->{'!id'} = \@tabExcludedIds;
                    }
                    my $tabOpts = {
                        'limit'               => 1,
                        'orderBy'             => ['activationDate:DESC'],
                        'getRulesListFilters' => {
                            'stateId' => LOC::StartCode::STATE_RULE_ACTIVE
                        }
                    };
                    my $tabOldStartCodes = &getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                    $tabFilters, GETINFOS_RULESLIST | GETINFOS_LINKEDCONTRACTS,
                                                    $tabOpts);
                    if (!$tabOldStartCodes)
                    {
                        return 0;
                    }
                    my @tabTmp = values %$tabOldStartCodes;
                    if (@tabTmp > 0)
                    {
                        # Recherche si ce code est déjà dans la liste des codes à modifier
                        my $tabOldStartCode = &List::Util::first(sub { $_->{'id'} == $tabTmp[0]->{'id'} }, @$tabStartCodes);
                        if (!$tabOldStartCode)
                        {
                            # Sinon on le rajoute
                            $tabOldStartCode = &_formatStartCode($tabTmp[0]);
                            if (!$tabOldStartCode)
                            {
                                return 0;
                            }
                            push(@$tabStartCodes, $tabOldStartCode);
                        }

                        # Le BACKUP ne doit plus avoir de date de fin
                        $tabOldStartCode->{'hasEndDate'} = 0;
                    }
                }
            }

            # Mise à jour de la machine
            $tabStartCode->{'machine.id'} = $tabStartCode->{'@newMachine.id'};
            $tabStartCode->{'valueDate'}  = &LOC::Date::getMySQLDate();
        }
    }


    # Recalcul des périodes des codes
    foreach my $tabStartCode (@$tabStartCodes)
    {
        if ($tabStartCode->{'@action'} ne 'clear' && $tabStartCode->{'@action'} ne 'delete')
        {
            # Recalcul de la période
            $tabStartCode->{'period'} = &_calculatePeriodFromContracts($tabStartCode->{'linkedContracts'});

            # Calcul des dates de validité
            my $beginDate = undef;
            my $endDate   = undef;
            if ($tabStartCode->{'hasBeginDate'} || $tabStartCode->{'hasEndDate'})
            {
                if ($tabStartCode->{'hasBeginDate'})
                {
                    $beginDate = $tabStartCode->{'period'}->{'beginDate'} . ' 00:00:00';
                }
                if ($tabStartCode->{'hasEndDate'})
                {
                    # Calcul de la date de fin
                    $endDate = &LOC::StartCode::_addGracePeriodToDate(
                        $tabStartCode->{'period'}->{'endDate'},
                        $tabStartCode->{'period'}->{'agency.id'},
                        $tabStartCode->{'typeCode'},
                        $tabStartCode->{'valueDate'}
                    );
                    $endDate .= ' 23:59:59';
                }
            }

            # Vérification des changements de validité et mise à jour
            if ($beginDate ne $tabStartCode->{'beginDate'})
            {
                $tabStartCode->{'@action'}   = 'save';
                $tabStartCode->{'beginDate'} = $beginDate;
            }
            if ($endDate ne $tabStartCode->{'endDate'})
            {
                $tabStartCode->{'@action'} = 'save';
                $tabStartCode->{'endDate'} = $endDate;
            }
        }
    }


    # Mises à jour des codes
    foreach my $tabStartCode (@$tabStartCodes)
    {
        # Mises à jour des codes
        # ----------------------------------------------------
        my $oldStartCodeId = $tabStartCode->{'id'};

        if ($tabStartCode->{'@action'} eq 'save')
        {
            my $tabOpts = {};
            if (exists $tabStartCode->{'excludedCodes'})
            {
                $tabOpts->{'excludedCodes'} = $tabStartCode->{'excludedCodes'};
            }

            if ($tabStartCode->{'id'})
            {
                # Modification du code
                $tabStartCode->{'id'} = &LOC::StartCode::update(
                    $countryId,
                    $tabStartCode->{'id'},
                    $tabStartCode->{'machine.id'},
                    $tabStartCode->{'beginDate'},
                    $tabStartCode->{'endDate'},
                    $userId, $tabOpts, {}, $tabEndUpds
                );
            }
            else
            {
                # Création du code
                $tabStartCode->{'id'} = &LOC::StartCode::insert(
                    $countryId,
                    $tabStartCode->{'typeCode'},
                    $tabStartCode->{'machine.id'},
                    $tabStartCode->{'beginDate'},
                    $tabStartCode->{'endDate'},
                    $userId, $tabOpts, {}, $tabEndUpds
                );
            }
            if (!$tabStartCode->{'id'})
            {
                return 0;
            }
        }
        elsif ($tabStartCode->{'@action'} eq 'clear')
        {
            # Suppression des règles actives et libération du code
            if (!&LOC::StartCode::clear($countryId, $tabStartCode->{'id'},
                                        $userId, {}, {}, $tabEndUpds))
            {
                return 0;
            }
        }
        elsif ($tabStartCode->{'@action'} eq 'delete')
        {
            # Suppression du code
            if (!&LOC::StartCode::delete($countryId, $tabStartCode->{'id'},
                                         $userId, {}, {}, $tabEndUpds))
            {
                return 0;
            }
        }

        # Ajout / Suppression des liens avec les contrats
        # ----------------------------------------------------

        # Dans le cas où un nouveau code a été généré en cas
        # d'indisponibilité du précédent, on associe tous les contrats
        # on supprime les liens avec l'ancien et on créé les liens avec le nouveau
        if ($oldStartCodeId && $oldStartCodeId != $tabStartCode->{'id'})
        {
            foreach my $tabLinkedContract (values %{$tabStartCode->{'linkedContracts'}})
            {
                # Suppression des liens avec l'ancien code
                if (!&_removeLink($db, $countryId, $oldStartCodeId, $tabLinkedContract->{'id'}))
                {
                    return 0;
                }
                # On recréé tous les liens actifs sur le nouveau code
                if ($tabLinkedContract->{'@action'} ne 'remove')
                {
                    if (!&_addLink($db, $countryId, $tabStartCode->{'id'}, $tabLinkedContract->{'id'}))
                    {
                        return 0;
                    }
                }
            }
        }
        else
        {
            foreach my $tabLinkedContract (values %{$tabStartCode->{'linkedContracts'}})
            {
                if ($tabLinkedContract->{'@action'} eq 'add')
                {
                    # Ajout du lien entre le code et le contrat
                    if (!&_addLink($db, $countryId, $tabStartCode->{'id'}, $tabLinkedContract->{'id'}))
                    {
                        return 0;
                    }
                }
                elsif ($tabLinkedContract->{'@action'} eq 'remove')
                {
                    # Suppression du lien entre le code et le contrat
                    if (!&_removeLink($db, $countryId, $tabStartCode->{'id'}, $tabLinkedContract->{'id'}))
                    {
                        return 0;
                    }
                }
            }
        }

        if ($tabStartCode->{'@action'} eq 'save' && $tabStartCode->{'typeCode'} eq LOC::StartCode::Contract::Rent::TYPE_CUSTOMER)
        {
            # Récupération des informations des contrats associés au code de démarrage
            my $tabLinkedContracts = &_getLinkedContracts($db, $countryId, $tabStartCode->{'id'});

            my $tabCodeInfos;
            my $tabMachineInfos;
            # Pour chacun de ces contrats
            foreach my $tabContractInfos (values(%$tabLinkedContracts))
            {
                # Si les conditions d'envoi du SMS sont réunies (téléphone du contact chantier présent)
                if ($tabContractInfos->{'sendSMS'})
                {
                    # Initialisation des infos sur le code et la machine
                    if (!$tabCodeInfos)
                    {
                        $tabCodeInfos = &getInfos($countryId, $tabStartCode->{'id'});
                    }
                    if (!$tabMachineInfos)
                    {
                        $tabMachineInfos = &LOC::Machine::getInfos(
                            $countryId,
                            $tabStartCode->{'machine.id'},
                            LOC::Machine::GETINFOS_BASE,
                            {'level' => LOC::Machine::LEVEL_FAMILY}
                        );
                    }

                    # Récupération des informations nécessaires
                    # et envoi des SMS
                    my $tabSmsInfos = {
                        'machinesFamily.label' => $tabMachineInfos->{'machinesFamily.label'},
                        'machine.parkNumber'   => $tabMachineInfos->{'parkNumber'},
                        'code'                 => $tabCodeInfos->{'code'},
                        'beginDate'            => $tabStartCode->{'period'}->{'beginDate'},
                        'endDate'              => $tabStartCode->{'period'}->{'endDate'},
                        'site.label'           => $tabContractInfos->{'site.label'},
                        'site.locality.name'   => $tabContractInfos->{'site.locality.name'}
                    };
                    my $tabReceiversInfos = {
                        $tabContractInfos->{'id'} => $tabContractInfos->{'contact.telephone'}
                    };

                    &LOC::EndUpdates::sendStartCodeSms(
                        $tabEndUpds,
                        $countryId,
                        $tabReceiversInfos,
                        $tabSmsInfos
                    );
                }
            }
        }
    }

    return 1;
}


# Function: _calculatePeriodFromContracts
# Calcule la période de validité par rapport aux contrats liés
#
# Parameters:
# hashref $tabLinkedContracts - Liste des contrats
#
# Returns:
# hashref
sub _calculatePeriodFromContracts
{
    my ($tabLinkedContracts) = @_;

    my $tabPeriod = {
        'beginDate' => undef,
        'endDate'   => undef,
        'agency.id' => undef
    };

    foreach my $tabLinkedContract (values %$tabLinkedContracts)
    {
        if ($tabLinkedContract->{'@action'} ne 'remove')
        {
            if (!defined $tabPeriod->{'beginDate'} || $tabLinkedContract->{'beginDate'} lt $tabPeriod->{'beginDate'})
            {
                $tabPeriod->{'beginDate'} = $tabLinkedContract->{'beginDate'};
            }
            if (!defined $tabPeriod->{'endDate'} || $tabLinkedContract->{'endDate'} gt $tabPeriod->{'endDate'})
            {
                $tabPeriod->{'endDate'}   = $tabLinkedContract->{'endDate'};
                $tabPeriod->{'agency.id'} = $tabLinkedContract->{'agency.id'};
            }
        }
    }

    return $tabPeriod;
}


# Function: _getLinkedContracts
# Récupère la liste des contrats liés à un code
#
# Parameters:
# LOC::Db::Adapter $db          - Connexion à la base de données
# string           $countryId   - Pays
# int              $startCodeId - Identifiant du code
# hashref          $tabFilters  - Filtres
#
# Returns:
# hashref
sub _getLinkedContracts
{
    my ($db, $countryId, $startCodeId, $tabFilters) = @_;

    my @tabWheres = ();
    if (defined $tabFilters->{'linkStateId'})
    {
        push(@tabWheres, 'src_sta_id IN (' . $db->quote($tabFilters->{'linkStateId'}) . ')');
    }
    else
    {
        push(@tabWheres, 'src_sta_id  = ' . $db->quote(LINK_STATE_ACTIVE));
    }

    my $query = '
SELECT
    src_rct_id AS `id`,
    tbl_f_contract.CLAUTO AS `customer.id`,
    tbl_f_contract.CONTRATCODE AS `code`,
    tbl_f_contract.CONTRATDD AS `beginDate`,
    tbl_f_contract.CONTRATDR AS `endDate`,
    tbl_f_contract.ETCODE AS `state.id`,
    tbl_f_contract.AGAUTO AS `agency.id`,
    tbl_f_contract.CONTRATDATECLOTURE AS `closureDate`,
    tbl_f_site.CHTELEPHONE AS `contact.telephone`,
    tbl_f_site.CHLIBELLE AS `site.label`,
    tbl_p_sitelocality.VINOM AS `site.locality.name`,
    -tbl_f_machine.MADIGIINACTIF AS `machine.isDigiKeyDeactivated`,
    src_sta_id AS `linkState.id`
FROM f_startcode_rentcontract
INNER JOIN CONTRAT tbl_f_contract
ON tbl_f_contract.CONTRATAUTO = src_rct_id
LEFT JOIN CHANTIER tbl_f_site
ON tbl_f_site.CHAUTO = tbl_f_contract.CHAUTO
LEFT JOIN VILLECHA tbl_p_sitelocality
ON tbl_f_site.VILLEAUTO = tbl_p_sitelocality.VILLEAUTO
LEFT JOIN AUTH.MACHINE tbl_f_machine
ON tbl_f_machine.MAAUTO = tbl_f_contract.MAAUTO
WHERE src_sco_id = ' . $startCodeId;
    if (@tabWheres > 0)
    {
        $query .= '
AND ' . join("\nAND ", @tabWheres);
    }
    $query .= ';';

    my $stmt = $db->query($query);

    tie(my %tabLinkedContracts, 'Tie::IxHash');
    while (my $tabRow = $stmt->fetch(LOC::Db::FETCH_ASSOC))
    {
        my $contactTelephone = &LOC::Util::trim($tabRow->{'contact.telephone'});
        my $isDigiKeyDeactivated = $tabRow->{'machine.isDigiKeyDeactivated'} * 1;
        my $tab = {
            'id'                 => $tabRow->{'id'} * 1,
            'customer.id'        => $tabRow->{'customer.id'} * 1,
            'code'               => $tabRow->{'code'},
            'beginDate'          => $tabRow->{'beginDate'},
            'endDate'            => $tabRow->{'endDate'},
            'state.id'           => $tabRow->{'state.id'},
            'agency.id'          => $tabRow->{'agency.id'},
            'isClosed'           => ($tabRow->{'closureDate'} ne ''),
            'contact.telephone'  => $contactTelephone,
            'site.label'         => $tabRow->{'site.label'},
            'site.locality.name' => $tabRow->{'site.locality.name'},
            'sendSMS'            => ($contactTelephone ne '' && !$isDigiKeyDeactivated),
            'linkState.id'       => $tabRow->{'linkState.id'}
        };
        $tabLinkedContracts{$tabRow->{'id'}} = $tab;
    }

    return \%tabLinkedContracts;
}


# Function: _addLink
# Ajout d'un lien entre un code et un contrat
#
# Parameters:
# LOC::Db::Adapter $db          - Connexion à la base de données
# string           $countryId   - Pays
# int              $startCodeId - Identifiant du code
# int              $contractId  - Identifiant du contrat de location
#
# Returns:
# bool
sub _addLink
{
    my ($db, $countryId, $startCodeId, $contractId) = @_;

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    if ($db->insert('f_startcode_rentcontract', {
            'src_sco_id'         => $startCodeId,
            'src_rct_id'         => $contractId,
            'src_date_creation*' => 'NOW()',
            'src_sta_id'         => LINK_STATE_ACTIVE
        }, $schemaName) == -1)
    {
        return 0;
    }

    return 1;
}


# Function: _removeLink
# Supprime le lien entre un code et un contrat
#
# Parameters:
# LOC::Db::Adapter $db          - Connexion à la base de données
# string           $countryId   - Pays
# int              $startCodeId - Identifiant du code
# int              $contractId  - Identifiant du contrat de location
#
# Returns:
# bool
sub _removeLink
{
    my ($db, $countryId, $startCodeId, $contractId) = @_;

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    if ($db->update('f_startcode_rentcontract', {
            'src_sta_id'         => LINK_STATE_DELETED,
            'src_date_deletion*' => 'NOW()'
        }, {
            'src_sco_id'         => $startCodeId,
            'src_rct_id'         => $contractId,
            'src_sta_id'         => LINK_STATE_ACTIVE
        }, $schemaName) == -1)
    {
        return 0;
    }

    return 1;
}


# Function: _formatStartCode
# Formate un code de démarrage renvoyé par le getList (ou getInfos) pour la méthode _updateStartCodes()
#
# Parameters:
# hashref $tabStartCode - Code de démarrage au format du getList (avec les flags GETINFOS_RULESLIST | GETINFOS_LINKEDCONTRACTS)
#
# Returns:
# hashref
sub _formatStartCode
{
    my ($tabStartCode) = @_;

    my $typeCode = $tabStartCode->{'type.code'};
    my @tabRules = values %{$tabStartCode->{'rulesList'}};
    # Le code ne doit pas comporter plus d'une règle active
    if (@tabRules > 1)
    {
        return undef;
    }

    my $tabRule = (exists $tabRules[0] ? $tabRules[0] : undef);

    my $machineId = ($tabRule ? $tabRule->{'machine.id'} : undef);
    my $beginDate = ($tabRule ? $tabRule->{'beginDate'} : undef);
    my $endDate   = ($tabRule ? $tabRule->{'endDate'} : undef);

    foreach my $tabLinkedContract (values %{$tabStartCode->{'linkedContracts'}})
    {
        $tabLinkedContract->{'@action'} = '';
    }

    return {
        'id'               => $tabStartCode->{'id'},
        'typeCode'         => $typeCode,
        'hasBeenActivated' => $tabStartCode->{'hasBeenActivated'},
        'machine.id'       => $machineId,
        'hasBeginDate'     => ($beginDate ne '' || $typeCode eq TYPE_CUSTOMER ? 1 : 0),
        'beginDate'        => $beginDate,
        'hasEndDate'       => ($endDate ne '' || $typeCode eq TYPE_CUSTOMER ? 1 : 0),
        'endDate'          => $endDate,
        'valueDate'        => ($tabStartCode->{'hasBeenActivated'} ? substr($tabStartCode->{'activationDate'}, 0, 10) : undef),
        'linkedContracts'  => $tabStartCode->{'linkedContracts'},

        '@action'          => ''
    };
}


1;