use utf8;
use open (':encoding(UTF-8)');

# Class: LOC::Kimoce::Soap
# Module gérant les webservices Kimoce
package LOC::Kimoce::Soap;

# Constantes
# HTTP_VERSION_1_1 - HTTP version 1.1.
use constant
{
    HTTP_VERSION_1_1 => '1.1',
};
# HTTP_RESPONSECODE_OK - Code de réponse 200
use constant
{
    HTTP_RESPONSECODE_OK => 200,
};
# ENCODING_UTF8 - Encodage en UTF-8
use constant
{
    ENCODING_UTF8 => 'UTF-8'
};

use strict;

# Modules externes
use LWP::UserAgent;
use HTTP::Request;
use XML::Simple;

# Modules internes
use LOC::Globals;
use LOC::Log;
use LOC::Xml;
use LOC::Util;

# Function: new
# Constructeur de la classe
#
# Returns:
# LOC::Json - Instance créée
sub new
{
    my ($class) = @_;

    my $this = {};
    bless($this, $class);

    $this->{'_userAgent'}       = undef;
    $this->{'_requestLocation'} = undef;
    $this->{'_isConnected'}     = 0;
    $this->{'_lastXml'}         = undef;
    $this->{'_lastMethod'}      = undef;
    $this->{'_errorMsg'}        = undef;

    return $this;
}

# Function: _getConnection
# Récupère la connexion au Webservice Kimoce
sub _getConnection
{
    my ($this) = @_;

    if (!defined $this->{'_userAgent'} || !$this->{'_isConnected'})
    {
        my $kimoceWs = &LOC::Globals::get('kimoceWs');

        if ($kimoceWs)
        {
            $this->{'_userAgent'} = LWP::UserAgent->new();
            $this->{'_userAgent'}->cookie_jar({});
            $this->{'_requestLocation'} = HTTP::Request->new(POST => $kimoceWs->{'location'});
            $this->{'_requestLocation'}->content_type('text/xml; charset=' . ENCODING_UTF8);
            $this->{'_requestLocation'}->protocol('HTTP/' . HTTP_VERSION_1_1);

            $this->{'_isConnected'} = $this->_call('connect', [['arg0', $kimoceWs->{'user'}], ['arg1', $kimoceWs->{'password'}]]);
        }
    }
    return $this->{'_isConnected'};
}

# Function _closeConnection
# Ferme la connexion au Webservice Kimoce
sub _closeConnection
{
    my ($this) = @_;

    if ($this->{'_isConnected'})
    {
        $this->{'_isConnected'} = !$this->_call('disconnect');
    }
}

# Function: getLastXml
# Récupère la dernière trame XML envoyée
#
# Returns:
# string | undef
sub getLastXml
{
    my ($this) = @_;
    return $this->{'_lastXml'};
}

# Function: getLastMethod
# Récupère la dernière méthode appelée
#
# Returns:
# string | undef
sub getLastMethod
{
    my ($this) = @_;
    return $this->{'_lastMethod'};
}

# Function: getErrorMsg
# Récupère le message de la dernière erreur survenue
#
# Returns:
# string | undef
sub getErrorMsg
{
    my ($this) = @_;
    return $this->{'_errorMsg'};
}

# Function: request
# Effectue une requête webservice
#
# Parameters:
# string $method  - Nom de la méthode à appeler
# mixed  $tabParams - Données à envoyer
#
# Returns:
# bool
sub request
{
    my ($this, $method, $tabParams) = @_;

    $this->_getConnection();
    if ($this->{'_isConnected'})
    {
        return $this->_call($method, $tabParams);
    }
    return 0;
}

# Function: updateFeatures
# Met à jour des caractéristiques sur une machine
#
# Parameters:
# string $parkNumber   - Numéro de parc de la machine
# hashref $tabFeatures - Caractéristiques à mettre à jour
#
# Return:
# bool
sub updateFeatures
{
    my ($this, $parkNumber, $tabFeatures) = @_;
    
    my $tabParams = $this->_getUpdateFeaturesParams($parkNumber, $tabFeatures);

    return $this->request('update', $tabParams);
}


# Function: _call
# Appelle un webservice de Kimoce
#
# Parameters:
# string $method  - Nom de la méthode à appeler
# mixed  $tabData - Données à envoyer
#
# Returns:
# bool
sub _call
{
    my ($this, $method, $tabData) = @_;

    my $tagName = 'kws:' . $method;

    my $tabEnvelopContent = [
        ['soapenv:Header', ''],
        ['soapenv:Body', [[$tagName, $tabData]]]
    ];

    my $tabEnvelopAttributes = [
        ['xmlns:soapenv', 'http://schemas.xmlsoap.org/soap/envelope/'],
        ['xmlns:kws', 'http://kws.services.kim/'],
        ['xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance']
    ];

    my $xml = &LOC::Xml::getTagOuterXML('soapenv:Envelope', $tabEnvelopContent, $tabEnvelopAttributes);
    $xml = &LOC::Util::encodeUtf8($xml);

    $this->{'_lastXml'}    = $xml;
    $this->{'_lastMethod'} = $method;

    $this->{'_requestLocation'}->header('Content-Length', length($xml));
    $this->{'_requestLocation'}->content($xml);
    my $response = $this->{'_userAgent'}->request($this->{'_requestLocation'});

    return $this->_isResponseOk($response, $method, $tabData);
}


# Function: _isResponseOk
# Vérifie s'il y a une erreur SOAP et traite l'erreur
#
# Parameters:
# $result     - Résultat de l'appel SOAP
# $callMethod - Le nom de la méthode appelée.
# $tabParams  - La liste des paramètres passés à l'appel SOAP.
#
# Return:
# bool
sub _isResponseOk
{
    my ($this, $result, $callMethod, $tabParams) = @_;

    if (!$result)
    {
        return 0;
    }

    if ($result->code == HTTP_RESPONSECODE_OK)
    {
        my $resultContent = &XML::Simple::XMLin($result->content);

        my $tabResult = $resultContent->{'soap:Body'}->{'ns2:' . $callMethod . 'Response'}->{'return'};

        if ($tabResult)
        {
            if (defined $tabResult->{'status'})
            {
                $tabResult = $tabResult->{'status'};
            }
            if ($tabResult->{'type'} != 0)
            {
                my $logMessage = sprintf(
                        '%s - %s --- %s - %s',
                        $callMethod,
                        $tabResult->{'code'},
                        $tabResult->{'message'},
                        &LOC::Json::toJson($tabParams)
                    );
                $this->{'_errorMsg'} = $logMessage;
    
                &LOC::Log::writeFileLog('kimoce', $logMessage, undef, LOC::Log::MODE_SEND_MAIL);
                return 0;
            }
        }
    }
    else
    {
        my $logMessage = sprintf(
                '%s --- %s - %s',
                $callMethod,
                $result->as_string,
                &LOC::Json::toJson($tabParams)
            );
        $this->{'_errorMsg'} = $logMessage;

        &LOC::Log::writeFileLog('kimoce', $logMessage, undef, LOC::Log::MODE_SEND_MAIL);
        return 0;
    }

    return 1;
}


# Function: _getUpdateFeaturesParams
# Récupère la trame de mise à jour à envoyer en webservice pour une(des) caractéristique(s)
#
# Parameters:
# string  $parkNumber - Numéro de parc de la machine
# hashref $tabFeatures   - Tableau des caractéristiques à mettre à jour
#
# Return:
# arrayref
sub _getUpdateFeaturesParams
{
    my ($this, $parkNumber, $tabFeatures) = @_;

    my @tabParams = (
        ['cols', 'chars'],
        ['item', [
            ['key', [
                ['extKey', $parkNumber],
                ['intKey', 0],
                ['typ', 'DOS']]]
        ], [['xsi:type', 'kws:dosItem']]]
    );

    foreach my $featureCode (keys(%$tabFeatures))
    {
        push(@{$tabParams[1]->[1]}, ['chars', [
            ['charKey', [
                ['extKey', $featureCode],
                ['intKey', 0]
            ]],
            ['charVal', &LOC::Util::removeCRLF($tabFeatures->{$featureCode}, ' ')]
        ]]);
    }

    return [['arg0', \@tabParams]];
}


# Function: DESTROY
# Destruction du module
#
sub DESTROY
{
    my ($this) = @_;
    $this->_closeConnection();
}


1;
