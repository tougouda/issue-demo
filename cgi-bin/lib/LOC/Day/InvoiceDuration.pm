use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Day::InvoiceDuration
# Module permettant d'obtenir les durées de facturation possibles pour les J+/J-
package LOC::Day::InvoiceDuration;


# Constants: Récupération d'informations sur les durées
# GETINFOS_ALL  - Récupération de l'ensemble des informations
# GETINFOS_BASE - Récupération des informations de base
use constant
{
    GETINFOS_ALL  => 0xFFFFFFFF,
    GETINFOS_BASE => 0x00000000
};


# Constants: États
# STATE_ACTIVE  - Actif
# STATE_DELETED - Supprimé
use constant
{
    STATE_ACTIVE  => 'GEN01',
    STATE_DELETED => 'GEN03'
};

# Constants: Flags
# FLAG_DEDUCTEDDAY - Valable pour les jours déduits
# FLAG_ADDITIONALDAY    - Valable pour les jours supplémentaires
use constant
{
    FLAG_DEDUCTEDDAY   => 1,
    FLAG_ADDITIONALDAY => 2
};

# Constants: Durée facturée pour le jour
# DURATION_HALFDAY   - Demi-journée
# DURATION_DAY       - Journée entière
# DURATION_DOUBLEDAY - Journée double
use constant
{
    DURATION_HALFDAY   => 0.5,
    DURATION_DAY       => 1,
    DURATION_DOUBLEDAY => 2
};

# Constants: Erreurs
# ERROR_FATAL       - Erreur fatale
# ERROR_RIGHTS      - Impossible de modifier les informations
use constant
{
    ERROR_FATAL  => 0x0001,
    ERROR_RIGHTS => 0x0003
};

use strict;

# Modules internes
use LOC::Db;
use LOC::Util;

my $fatalErrMsg = 'Erreur fatale: module LOC::InvoiceDuration, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";



# Function: getInfos
# Retourne les infos d'un jour supplémentaire ou déduit
#
# Parameters:
# string    $id        - Id du jour
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getInfos
{
    my ($countryId, $id) = @_;

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id});
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}


# Function: getList
# Retourne la liste des durées facturées possibles
#
# Contenu d'une ligne du hashage :
# - id       : Identifiant
# - label    : Libellé
# - duration : Durée
# - flags    : Flags d'utilisation du motif
# - state.id : État
#
# Parameters:
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|0 - Liste des durées
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT';

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query .= '
    did_id AS `id`,
    did_label AS `label`,
    did_duration AS `duration`,
    did_flags AS `flags`,
    did_is_default AS `isDefault`,
    did_sta_id AS `state.id`';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query .= '
    COUNT(*)';
    }
    else
    {
        $query .= '
    did_id,
    CONCAT_WS(" / ", did_label, did_duration)';
    }

    $query .= '
FROM p_dayinvoiceduration';

    my @tabWheres = ();

    # Filtre sur l'état
    my $tabStates = (defined $tabFilters->{'stateId'} ? $tabFilters->{'stateId'} : [STATE_ACTIVE]);
    push(@tabWheres, 'did_sta_id IN (' . $db->quote($tabStates) . ')');
    # Filtre sur l'id
    if (defined $tabFilters->{'id'})
    {
        push(@tabWheres, 'did_id IN (' . &LOC::Util::join(', ', $tabFilters->{'id'}) . ')');
    }
    # Filtre sur le flag
    if (defined $tabFilters->{'flags'})
    {
        push(@tabWheres, 'did_flags & ' . $tabFilters->{'flags'});
    }

    if (@tabWheres > 0)
    {
        $query .= '
WHERE ' . join("\nAND ", @tabWheres);
    }

    $query .= '
ORDER BY did_duration ASC;';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    # Formatage des champs
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}        *= 1;
            $tabRow->{'duration'}  *= 1;
            $tabRow->{'flags'}     *= 1;
            $tabRow->{'isDefault'} *= 1;
        }
    }

    return (wantarray ? %$tab : $tab);
}

1;