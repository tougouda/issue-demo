use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Day::Reason
# Module permettant d'obtenir les motifs possibles pour les J+/J-
package LOC::Day::Reason;


# Constants: Récupération d'informations sur les motifs
# GETINFOS_ALL  - Récupération de l'ensemble des informations
# GETINFOS_BASE - Récupération des informations de base
use constant
{
    GETINFOS_ALL  => 0xFFFFFFFF,
    GETINFOS_BASE => 0x00000000
};


# Constants: États
# STATE_ACTIVE  - Actif
# STATE_DELETED - Supprimé
use constant
{
    STATE_ACTIVE  => 'GEN01',
    STATE_DELETED => 'GEN03'
};

# Constants: Flags
# USEFLAG_DEDUCTEDDAY       - Valable pour les jours déduits
# USEFLAG_ADDITIONALDAY     - Valable pour les jours supplémentaires
# USEFLAG_CONTRACT          - Valable pour les contrats
# USEFLAG_ESTIMATE          - Valable pour les devis
use constant
{
    USEFLAG_DEDUCTEDDAY       => 0x0001,
    USEFLAG_ADDITIONALDAY     => 0x0002,
    USEFLAG_CONTRACT          => 0x0010,
    USEFLAG_ESTIMATE          => 0x0020
};


# Constants: Erreurs
# ERROR_FATAL       - Erreur fatale
# ERROR_RIGHTS      - Impossible de modifier les informations
use constant
{
    ERROR_FATAL  => 0x0001,
    ERROR_RIGHTS => 0x0003
};

use strict;

# Modules internes
use LOC::Db;
use LOC::Util;

my $fatalErrMsg = 'Erreur fatale: module LOC::Day::Reason, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";



# Function: getInfos
# Retourne les infos d'un jour supplémentaire ou déduit
#
# Parameters:
# string    $id        - Id du jour
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getInfos
{
    my ($countryId, $id) = @_;

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id});
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}


# Function: getList
# Retourne la liste des motifs possibles
#
# Contenu d'une ligne du hashage :
# - id       : Identifiant
# - label    : Libellé
# - flags    : Flags d'utilisation du motif
# - state.id : État
#
# Parameters:
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|0 - Liste des motifs
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT';

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query .= '
    dre_id AS `id`,
    dre_label AS `label`,
    dre_useflags AS `useFlags`,
    dre_typeflags AS `typeFlags`,
    dre_sta_id AS `state.id`';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query .= '
    COUNT(*)';
    }
    else
    {
        $query .= '
    dre_id,
    dre_label';
    }

    $query .= '
FROM p_dayreason';

    my @tabWheres = ();

    # Filtre sur l'état
    my $tabStates = (defined $tabFilters->{'stateId'} ? $tabFilters->{'stateId'} : [STATE_ACTIVE]);
    push(@tabWheres, 'dre_sta_id IN (' . $db->quote($tabStates) . ')');
    # Filtre sur l'id
    if (exists $tabFilters->{'id'})
    {
        push(@tabWheres, 'dre_id IN (' . &LOC::Util::join(', ', $tabFilters->{'id'}) . ')');
    }
    # Filtre sur le flag d'utilisation
    if (exists $tabFilters->{'useFlags'})
    {
        push(@tabWheres, 'dre_useflags & ' . $tabFilters->{'useFlags'});
    }
    # Filtre sur le flag de type
    if (exists $tabFilters->{'typeFlags'})
    {
        push(@tabWheres, 'dre_typeflags & ' . $tabFilters->{'typeFlags'});
    }

    if (@tabWheres > 0)
    {
        $query .= '
WHERE ' . join("\nAND ", @tabWheres);
    }

    $query .= '
ORDER BY dre_label ASC;';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    # Formatage des champs
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}        *= 1;
            $tabRow->{'useFlags'}  *= 1;
            $tabRow->{'typeFlags'} *= 1;
        }
    }

    return (wantarray ? %$tab : $tab);
}


1;