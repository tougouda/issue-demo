use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::AccountFamily
# Module permettant d'obtenir les informations sur les familles comptables
package LOC::AccountFamily;


# Constants: Récupération d'informations sur les familles comptables
# GETINFOS_ALL  - Récupération de l'ensemble des informations
# GETINFOS_BASE - Récupération des informations de base
use constant
{
    GETINFOS_ALL  => 0xFFFFFFFF,
    GETINFOS_BASE => 0x00000000
};

# Constants: Familles comptables définies
# ID_SUBRENT - Famille comptable pour la sous-location
use constant
{
    ID_SUBRENT  => 17
};

use strict;

# Modules internes
use LOC::Db;


# Function: getInfos
# Retourne les infos d'une famille comptable
# 
# Contenu du hashage :
# - id => identifiant de la famille comptable
# - label => libellé
#
#
# Parameters:
# string  $countryId  - Identifiant du pays
# string  $id         - Identifiant de la famille comptable
# int     $flags      - Flags de récupération
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);
    return ($tab && exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Retourne la liste des familles comptables
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres ('id' : Recherche par l'identifiant)
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|0 - Liste des familles comptables
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }

    # connexion à la base
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Filtres
    my $whereQuery = '';

    # - Recherche par l'identifiant
    if ($tabFilters->{'id'})
    {
        $whereQuery .= ' AND tbl_p_accountfamily.FAMACOMPTAAUTO = ' . $tabFilters->{'id'};
    }

    my $query;
    if ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*) AS `nb`';
    }
    else
    {
        $query = '
SELECT
    tbl_p_accountfamily.FAMACOMPTAAUTO AS `id`,
    tbl_p_accountfamily.FAMACOMPTA AS `label`';
    }

    $query .= '
FROM FAMILLECOMPTA tbl_p_accountfamily
WHERE 1
' . $whereQuery . ';';

    my $tab = $db->fetchList($format, $query, {}, 'id');

    # Formatage des données
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'} *= 1;
        }
    }

    return (wantarray ? %$tab : $tab);
}

1;
