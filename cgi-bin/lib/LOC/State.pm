use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::State
# Module permettant d'obtenir les informations sur les états
package LOC::State;
use strict;

# Modules internes
use LOC::Db;



# Function: getInfos
# Retourne les infos d'un état
#
# Contenu du hashage : voir la fonction getList.
#
# Parameters:
# string $id        - Identifiant de l'état
# string $countryId - Identifiant du pays
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getInfos
{
    my ($id, $countryId) = @_;

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id});
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Retourne la liste des états
#
# Contenu de la table de hachage :
# - id : code de la région ;
# - module : code du module associé
# - label : nom ;
# - country.id : identifiant du pays ;
# - manager.id : identifiant du responsable de région ;
# - manager.name : nom du responsable de région ;
# - manager.firstname : prénom du responsable de région ;
# - manager.fullName : nom complet du responsable de région ;
# - creationDate : date et heure de création ;
# - modificationDate : date et heure de modification ;
# - state.id : identifiant de l'état.
#
# Parameters:
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres ()
#
# Returns:
# hash|hashref|0 - Liste des états
sub getList
{
    my ($countryId, $format, $tabFilters) = @_;
    
    # Connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Filtres
    my $whereQuery = '';
    if ($tabFilters->{'id'} ne '')
    {
        $whereQuery .= '
AND tbl_p_state.ETCODE = ' . $db->quote($tabFilters->{'id'});
    }
    if ($tabFilters->{'category'} ne '')
    {
        my $categoryWhere = '';
        if (ref $tabFilters->{'category'} eq 'ARRAY')
        {
            foreach my $category (@{$tabFilters->{'category'}})
            {
                $categoryWhere .= '
OR tbl_p_state.ETCODE LIKE ' . $db->quote($category . '%');
            }
        }
        else
        {
            $categoryWhere .= '
OR tbl_p_state.ETCODE LIKE ' . $db->quote($tabFilters->{'category'} . '%');
        }
        $whereQuery .= '
AND (FALSE ' . $categoryWhere . ')';
    }
    if ($tabFilters->{'module'} ne '')
    {
        $whereQuery .= '
AND tbl_p_state.ETMODULE = ' . $db->quote($tabFilters->{'module'});
    }
    if ($tabFilters->{'exception'} ne '')
    {
        $whereQuery .= '
AND tbl_p_state.ETCODE NOT IN (' . $db->quote($tabFilters->{'exception'}) . ')';
    }

    # Récupère les données concernant le pays
    my $query = '';
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    tbl_p_state.ETCODE AS `id`,
    tbl_p_state.ETMODULE AS `module`,
    tbl_p_state.ETLIBELLE AS `label`,
    tbl_p_state.ETABR AS `code`,
    tbl_p_state.ETPROBA AS `probability`,
    tbl_p_state.ObjSitInCde AS `kimCode`
FROM ETATTABLE tbl_p_state
WHERE TRUE' .
$whereQuery . '
ORDER BY `id`;';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*) AS `label`
FROM ETATTABLE tbl_p_state
WHERE TRUE' .
$whereQuery . ';';
    }
    else
    {
        $query = '
SELECT
    tbl_p_state.ETCODE AS `id`,
    tbl_p_state.ETLIBELLE AS `label`
FROM ETATTABLE tbl_p_state
WHERE TRUE' .
$whereQuery . '
ORDER BY `id`;';
    }

    my $dataRef = $db->fetchList($format, $query, {}, 'id');

    unless ($dataRef)
    {
        return 0;
    }

    return (wantarray ? %$dataRef : $dataRef);
}

1;
