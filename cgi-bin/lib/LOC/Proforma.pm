use utf8;
use open (':encoding(UTF-8)');

# Package : LOC::Proforma
# Module de gestion des pro formas sur le devis et le contrat de location.
package LOC::Proforma;


# Constants: Récupération d'informations sur les pro formas
# GETINFOS_ALL           - Récupération de l'ensemble des informations
# GETINFOS_BASE          - Récupération des informations de base
# GETINFOS_CUSTOMER      - Récupération des informations sur le client
# GETINFOS_CREATOR       - Récupération des informations sur le l'utilisateur créateur
# GETINFOS_LINES         - Récupération des informations sur les lignes
# GETINFOS_CONTRACT      - Récupération des informations sur le contrat associé
# GETINFOS_ESTIMATELINE  - Récupération des informations sur la ligne de devis associée
# GETINFOS_INDEX         - Récupération de l'index la pro forma sur la ligne de devis ou le contrat associé
# GETINFOS_LINK          - Récupération des informations sur la pro forma remplacée
# GETINFOS_LINKEXTENSION - Récupération des informations sur la pro forma prolongée
use constant
{
    GETINFOS_ALL           => 0xFFFFFFFF,
    GETINFOS_BASE          => 0x00000000,
    GETINFOS_CUSTOMER      => 0x00000001,
    GETINFOS_CREATOR       => 0x00000002,
    GETINFOS_LINES         => 0x00000004,
    GETINFOS_CONTRACT      => 0x00000008,
    GETINFOS_ESTIMATELINE  => 0x00000010,
    GETINFOS_INDEX         => 0x00000020,
    GETINFOS_LINK          => 0x00000030,
    GETINFOS_LINKEXTENSION => 0x00000040
};

# Constants: États des pro formas
# STATE_ACTIVE   - Pro forma active
# STATE_REPLACED - Pro forma remplacée
# STATE_ABORTED  - Pro forma abandonnée
# STATE_DELETED  - Pro forma supprimée
use constant
{
    STATE_ACTIVE   => 'PFM01',
    STATE_REPLACED => 'PFM02',
    STATE_ABORTED  => 'PFM03',
    STATE_DELETED  => 'PFM04'
};

# Constants: États d'encaissement
# ENCASHMENTSTATE_WAITING     - Encaissement en attente
# ENCASHMENTSTATE_RECEIVED    - Encaissement reçu
# ENCASHMENTSTATE_NOTRECEIVED - Encaissement non reçu
# ENCASHMENTSTATE_PARTIAL     - Encaissement partiel
use constant
{
    ENCASHMENTSTATE_WAITING     => 'ECM01', # en attente
    ENCASHMENTSTATE_RECEIVED    => 'ECM02', # reçu
    ENCASHMENTSTATE_NOTRECEIVED => 'ECM03', # non reçu
    ENCASHMENTSTATE_PARTIAL     => 'ECM04'  # partiel
};

# Constants: États de clôture
# CLOSURESTATE_CREDIT     - Avoir
# CLOSURESTATE_LOSS       - Perte
# CLOSURESTATE_LITIGATION - Contentieux
# CLOSURESTATE_PAYMENT    - Règlement
# CLOSURESTATE_CANCEL     - Location annulée
use constant
{
    CLOSURESTATE_CREDIT     => 'CLO01',
    CLOSURESTATE_LOSS       => 'CLO02',
    CLOSURESTATE_LITIGATION => 'CLO03',
    CLOSURESTATE_PAYMENT    => 'CLO04',
    CLOSURESTATE_CANCEL     => 'CLO05'
};

# Constants: Flags sur les documents
# DOCFLAG_CT_ISCUSTOMERPFREQONCREATION - Est-ce que le contrat a été créé avec un client PAL ?
# DOCFLAG_EXISTACTIVED                 - Il existe des pro formas actives
# DOCFLAG_EXISTACTIVEDFINALIZED        - Il existe au moins une pro forma active finalisée
# DOCFLAG_ALLACTIVEDFINALIZED          - Les pro formas actives sont finalisées
# DOCFLAG_EXISTACTIVEDEXPIRED          - Il existe au moins une pro forma active expirée
# DOCFLAG_EXISTACTIVEDADVANCE          - Il existe au moins une facture d'acompte active
# DOCFLAG_EXISTACTIVEDEXTENSION        - Il existe au moins une pro forma de prolongation active
use constant
{
    # Les 8 premiers bits (0 à 7) sont réservés au document lui même (état)
    DOCFLAG_CT_ISCUSTOMERPFREQONCREATION => 0x00000001,

    # Les 8 bits suivants (8 à 15) sont réservés pour les compteurs
    DOCFLAG_EXISTACTIVED                 => 0x00000100,
    DOCFLAG_EXISTACTIVEDFINALIZED        => 0x00000200,
    DOCFLAG_ALLACTIVEDFINALIZED          => 0x00000400,
    DOCFLAG_EXISTACTIVEDEXPIRED          => 0x00000800,
    DOCFLAG_EXISTACTIVEDADVANCE          => 0x00002000,
    DOCFLAG_EXISTACTIVEDEXTENSION        => 0x00004000
};


use strict;

# Module internes
use Number::Format;
use POSIX;
use LOC::Db;
use LOC::Date;
use LOC::Country;
use LOC::Characteristic;
use LOC::Util;
use LOC::Request;
use LOC::Estimate::Rent;
use LOC::Contract::Rent;
use LOC::Contract::RepairDay;
use LOC::Contract::RepairService;
use LOC::Common::RentService;
use LOC::Proforma::Type;
use LOC::Proforma::ArticleRef;
use LOC::Common::Rent;
use LOC::PaymentMode;
use LOC::Mail;
use LOC::EndUpdates;
use LOC::Transport;
use LOC::Insurance::Type;
use LOC::Appeal::Type;
use LOC::Template;


# Liste des erreurs:
#
# 0x0001 => Erreur fatale
# 0x0004 => Type de montant mensuel sur le document
# 0x0005 => Action proforma non autorisée
# 0x0008 => La date de fin saisie est antérieure à la date de fin du contrat
# 0x0009 => La date de fin saisie est antérieure à la date de fin de la dernière prolongation
#
# 0x0012 => Impossible de modifier la proforma
# 0x0013 => Impossible d'abandonner la proforma
# 0x0015 => Impossible de réactiver la proforma
# 0x0017 => Le montant de la pro forma est nul
# 0x0018 => Le mode de règlement n'est pas renseigné
# 0x0019 => estimation carburant incorrecte

# 0x0020 => Le reste du ne peut etre egal à 0 dans le cas d'un encaissement partiel
# 0x0021 => On ne pas "définaliser" une pro forma
# 0x0022 => La pro forma a été modifiée par un autre utilisateur
# 0x0023 => Le commentaire est obligatoire pour la validation par agence

# 0x0100 => Erreur sur le contrat



# Function: abort
# Abandon d'une proforma
#
# Parameters:
# string   $countryId  - Pays
# int      $proformaId - Id de la proforma à abandonner
# int      $userId     - Id de l'utilisateur responsable
# hashref  $tabErrors  - Tableau des erreurs
# hashref  $tabEndUpds - Mises à jour de fin
#
# Return:
# bool
sub abort
{
    my ($countryId, $proformaId, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $tabInfos = &getInfos($countryId, $proformaId);

    # Possibilité d'abandonner la proforma
    if (!$tabInfos->{'isAbortable'})
    {
        push(@{$tabErrors->{'fatal'}}, 0x0013);

        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    if ($db->update('f_proforma', {'pfm_sta_id' => STATE_ABORTED}, {'pfm_id' => $proformaId}, $schemaName) == -1)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);

        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # mise à jour du flag proformaOK sur ligne de devis et contrat
    my $documentType = (defined $tabInfos->{'contract.id'} ? 'contract' : 'estimateLine');
    my $documentId   = (defined $tabInfos->{'contract.id'} ? $tabInfos->{'contract.id'} : $tabInfos->{'estimateLine.id'});

    # Mise à jour du document impacté
    if (!&_updateDocument($db, $countryId, $documentType, $documentId, $userId, $tabEndUpds))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);

        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Mise à jour des alertes Règlements proforma (commerce et comptabilité)
    if (!$tabInfos->{'isExpired'})
    {
        &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::PROFORMA_ALL, -1);
        &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::PROFORMA_AGENCY, -1);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: abortAll
# Abandonne toutes les proformas d'un document
#
# Parameters:
# string   $countryId     - Pays
# string   $documentType  - Type de document (contract/estimateLine/estimate)
# int      $documentId    - Id du document
# int      $userId        - Id de l'utilisateur responsable
# hashref  $tabOptions    - Options supplémentaires
# hashref  $tabErrors     - Tableau d'erreurs
# hashref  $tabEndUpds    - Mises à jour de fin
#
# Returns:
# bool
sub abortAll
{
    my ($countryId, $documentType, $documentId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (!exists $tabErrors->{'fatal'})
    {
        $tabErrors->{'fatal'} = [];
    }

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my $tabFilters = {
        'stateId' => [STATE_ACTIVE]
    };

    if ($documentType eq 'estimateLine')
    {
        $tabFilters->{'estimateLineId'} = $documentId;
    }
    elsif ($documentType eq 'contract')
    {
        $tabFilters->{'contractId'} = $documentId;
    }
    elsif ($documentType eq 'estimate')
    {
        $tabFilters->{'estimateId'} = $documentId;
    }
    else
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Récupération de la liste des pro formas à abandonner
    my $tabList = &getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $tabDocuments = {
        'estimateLines' => [],
        'contracts'     => []
    };
    foreach my $tabInfos (values %$tabList)
    {
        if (!$tabOptions->{'isForced'})
        {
            if ($tabOptions->{'doJustPossible'} && !$tabInfos->{'isAbortable'})
            {
                next;
            }
            # Possibilité d'abandonner la proforma
            if (!$tabInfos->{'isAbortable'})
            {
                push(@{$tabErrors->{'fatal'}}, 0x0013);
                return 0;
            }
        }

        if ($db->update('f_proforma', {'pfm_sta_id' => STATE_ABORTED}, {'pfm_id' => $tabInfos->{'id'}}, $schemaName) == -1)
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);

            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }

        # Recensement des lignes de devis et des contrats
        if (defined $tabInfos->{'estimateLine.id'})
        {
            push(@{$tabDocuments->{'estimateLines'}}, $tabInfos->{'estimateLine.id'});
        }
        if (defined $tabInfos->{'contract.id'})
        {
            push(@{$tabDocuments->{'contracts'}}, $tabInfos->{'contract.id'});
        }

        # Mise à jour des alertes Règlements proforma (commerce et comptabilité)
        if (!$tabInfos->{'isExpired'} && $tabInfos->{'encashment.id'} ne ENCASHMENTSTATE_RECEIVED)
        {
            &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::PROFORMA_ALL, -1);
            &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::PROFORMA_AGENCY, -1);
        }
    }

    # Mise à jour des documents impactés
    if (!&_updateDocuments($db, $countryId, $tabDocuments, $userId, $tabEndUpds))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: abortAllForCustomer
# Abandonne toutes les proformas d'un client
#
# Parameters:
# string   $countryId     - Pays
# int      $customerId    - Id du client
# int      $userId        - Id de l'utilisateur responsable
# hashref  $tabOptions    - Options supplémentaires
# hashref  $tabErrors     - Tableau d'erreurs
# hashref  $tabEndUpds    - Mises à jour de fin
#
# Returns:
# bool
sub abortAllForCustomer
{
    my ($countryId, $customerId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (!exists $tabErrors->{'fatal'})
    {
        $tabErrors->{'fatal'} = [];
    }

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Récupération des lignes de devis et des contrats du client
    my $query = '
SELECT
    "estimateLine",
    tbl_f_estimateline.DETAILSDEVISAUTO,
    tbl_f_estimate.AGAUTO
FROM DETAILSDEVIS tbl_f_estimateline
INNER JOIN DEVIS tbl_f_estimate
ON tbl_f_estimate.DEVISAUTO = tbl_f_estimateline.DEVISAUTO
WHERE tbl_f_estimate.CLAUTO = ' . $customerId . '
AND tbl_f_estimate.ETCODE <> "' . LOC::Estimate::Rent::STATE_ABORTED . '"
AND (SELECT COUNT(*)
     FROM f_proforma
     WHERE pfm_rel_id = tbl_f_estimateline.DETAILSDEVISAUTO
     AND pfm_sta_id = "' . LOC::Proforma::STATE_ACTIVE . '") > 0
UNION ALL
SELECT
    "contract",
    tbl_f_contract.CONTRATAUTO,
    tbl_f_contract.AGAUTO
FROM CONTRAT tbl_f_contract
WHERE tbl_f_contract.CLAUTO = ' . $customerId . '
AND tbl_f_contract.ETCODE <> "' . LOC::Contract::Rent::STATE_CANCELED . '"
AND (SELECT COUNT(*)
     FROM f_proforma
     WHERE pfm_rct_id = tbl_f_contract.CONTRATAUTO
     AND pfm_sta_id = "' . LOC::Proforma::STATE_ACTIVE . '") > 0;';
    my $stmt = $db->query($query);
    while (my $tabRow = $stmt->fetch(LOC::Db::FETCH_NUM))
    {
        my ($documentType, $documentId, $agencyId) = @$tabRow;
        if (!&LOC::Proforma::abortAll($countryId, $documentType, $documentId, $userId, $tabOptions, {}, $tabEndUpds))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);

            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }
    }
    $stmt->close();


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function affectToContract
# Affectation des pro formas actives d'un devis à un contrat
#
# Parameters:
# string    $country        - Pays
# int       $estimateLineId - Id de la ligne de devis
# int       $contractId     - Id du contrat
# int       $userId         - Id de l'utilisateur responsable
# hashref   $tabErrors      - Tableau des erreurs
# hashref   $tabEndUpds     - Mises à jour de fin
#
# Return:
# bool
sub affectToContract
{
    my ($countryId, $estimateLineId, $contractId, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }
    my $wheres = {
        'estimateLineId' => $estimateLineId,
        'isFinalized'    => 1,
        'stateId'        => [STATE_ACTIVE, STATE_REPLACED]
    };

    my $tabList = &getList($countryId, LOC::Util::GETLIST_ASSOC, $wheres);

    foreach my $tabInfos (values %$tabList)
    {
        my $tabUpdates = {
            'pfm_rct_id*' => $contractId
        };

        if (!$tabInfos->{'isExpired'})
        {
            my $tabCompareInfos = {};
            my $isUpToDate = &isUpToDate($countryId, $tabInfos->{'id'}, 'contract', $contractId, $tabCompareInfos);

            my $articleToUpdate = '';

            # Pour la facture d'acompte
            if ($tabInfos->{'type.id'} == LOC::Proforma::Type::ID_ADVANCE)
            {
                # Mise à jour de la ligne d'acompte
                $articleToUpdate = 'ACPTELD';
            }
            else
            {
                if ($isUpToDate)
                {
                    $articleToUpdate = 'LOCAT';
                }
                else
                {
                    # Sinon on l'expire
                    $tabUpdates->{'pfm_is_expired'} = 1;

                    # si la pro forma est active, on l'enlève du compte d'alertes
                    # on peut vouloir expirer une "abandonnée", dans ce cas, pas besoin de l'enlever du compte d'alertes
                    if ($tabInfos->{'state.id'} eq STATE_ACTIVE && $tabInfos->{'encashment.id'} ne ENCASHMENTSTATE_RECEIVED)
                    {
                        # Mise à jour des alertes Règlements proforma (commerce et comptabilité)
                        &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::PROFORMA_ALL, -1);
                        &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::PROFORMA_AGENCY, -1);
                    }
                }
            }

            # Mise à jour d'une ligne de la pro forma si demandé
            if ($articleToUpdate ne '')
            {
                # Recherche de la ligne de l'article à mettre à jour
                my $tabOldLines = $tabCompareInfos->{'old'}->{'tabLines'};
                my $tabNewLines = $tabCompareInfos->{'new'}->{'tabLines'};
                my $oldLineIndex = &_getLineIndexByArticleRef($tabOldLines, 'LOCAT');
                my $newLineIndex = &_getLineIndexByArticleRef($tabNewLines, 'LOCAT');

                # Si la pro forma est à jour on met à jour la ligne LOCAT (avec les infos du modèle de machine)
                if (!&_updateLine($db, $countryId, $tabOldLines->[$oldLineIndex]->{'id'}, $tabNewLines->[$newLineIndex], {}, $tabErrors))
                {
                    push(@{$tabErrors->{'fatal'}}, 0x0001);
                    # Annule la transaction
                    if (!$hasTransaction)
                    {
                        $db->rollBack();
                    }
                    return 0;
                }

                # Mise à jour de la date de début de location
                if ($tabCompareInfos->{'old'}->{'beginDate'} ne $tabCompareInfos->{'new'}->{'beginDate'})
                {
                    $tabUpdates->{'pfm_date_begin'} = $tabCompareInfos->{'new'}->{'beginDate'};
                }
            }
        }

        if ($db->update('f_proforma', $tabUpdates, {'pfm_id' => $tabInfos->{'id'}}, $schemaName) == -1)
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);

            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }
    }

    # Mise à jour des documents impactés
    if (!&_updateDocument($db, $countryId, 'contract', $contractId, $userId, $tabEndUpds) ||
        !&_updateDocument($db, $countryId, 'estimateLine', $estimateLineId, $userId, $tabEndUpds))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}

# Function: deleteAll
# Suppression de toutes les proformas d'un document
#
# Parameters:
# string   $countryId    - Pays
# string   $documentType - Type de document (contract/estimateLine)
# int      $documentId   - Id du document
# int      $userId       - Id de l'utilisateur responsable
# hashref  $tabErrors    - Tableau d'erreurs
# hashref  $tabEndUpds   - Mises à jour de fin
#
# Returns:
# bool
sub deleteAll
{
    my ($countryId, $documentType, $documentId, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my $tabFilters = {
        'stateId' => [STATE_ACTIVE, STATE_REPLACED, STATE_ABORTED, STATE_DELETED]
    };

    if ($documentType eq 'estimateLine')
    {
        $tabFilters->{'estimateLineId'} = $documentId;
    }
    elsif ($documentType eq 'contract')
    {
        $tabFilters->{'contractId'} = $documentId;
    }
    else
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    my $tabList = &getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters);
    my $countDeleteSucceed = 0;

    my @tabIds = keys %$tabList;
    if (@tabIds > 0)
    {
        # Démarre la transaction
        if (!$hasTransaction)
        {
            $db->beginTransaction();
        }

        # Suppression des lignes des proformas
        if ($db->delete('f_proformaline', {'pfl_pfm_id*' => \@tabIds}, $schemaName) == -1)
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);

            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }

        # Suppression de la proforma
        if ($db->delete('f_proforma', {'pfm_id*' => \@tabIds}, $schemaName) == -1)
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);

            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }

        my $tabDocuments = {
            'estimateLines' => [],
            'contracts'     => []
        };
        foreach my $tabInfos (values %$tabList)
        {
            if (!$tabInfos->{'isExpired'} && $tabInfos->{'state.id'} eq STATE_ACTIVE && $tabInfos->{'encashment.id'} ne ENCASHMENTSTATE_RECEIVED)
            {
                # Mise à jour des alertes Règlements proforma (commerce et comptabilité)
                &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::PROFORMA_ALL, -1);
                &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::PROFORMA_AGENCY, -1);
            }

            # Recensement des lignes de devis et des contrats
            if (defined $tabInfos->{'estimateLine.id'})
            {
                push(@{$tabDocuments->{'estimateLines'}}, $tabInfos->{'estimateLine.id'});
            }
            if (defined $tabInfos->{'contract.id'})
            {
                push(@{$tabDocuments->{'contracts'}}, $tabInfos->{'contract.id'});
            }
        }

        # Mise à jour du document impacté
        if (!&_updateDocuments($db, $countryId, $tabDocuments, $userId, $tabEndUpds))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);

            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }


        # Valide la transaction
        if (!$hasTransaction)
        {
            if (!$db->commit())
            {
                $db->rollBack();
                return 0;
            }
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: expireAll
# Passage en non à jour de toutes les proformas d'un document
#
# Parameters:
# string   $countryId    - Pays
# string   $documentType - Type de document (contrat/ligne de devis)
# int      $documentId   - Id du document
# int      $userId       - Id de l'utilisateur responsable
# hashref  $tabErrors    - Tableau d'erreurs
# hashref  $tabEndUpds   - Mises à jour de fin
#
# Returns:
# bool
sub expireAll
{
    my ($countryId, $documentType, $documentId, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Si le type de document est inconnu alors on ne fait rien !
    my $tabFilters = {};
    # Contrat ou devis ?
    if ($documentType eq 'contract')
    {
        $tabFilters->{'contractId'} = $documentId;
    }
    elsif ($documentType eq 'estimateLine')
    {
        $tabFilters->{'estimateLineId'} = $documentId;
    }
    else
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }
    $tabFilters->{'isExpired'} = 0;

    # Récupération de la liste des pro formas à expirer
    my $tabList = &getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters, GETINFOS_CONTRACT | GETINFOS_ESTIMATELINE);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $tabDocuments = {
        'estimateLines' => [],
        'contracts'     => []
    };
    foreach my $tabInfos (values %$tabList)
    {
        if ($tabInfos->{'isExpirable'})
        {
            if ($db->update('f_proforma', {'pfm_is_expired' => 1}, {'pfm_id' => $tabInfos->{'id'}}, $schemaName) == -1)
            {
                push(@{$tabErrors->{'fatal'}}, 0x0001);
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return 0;
            }
            # si la pro forma est active, on l'enlève du compte d'alertes
            # on peut vouloir expirer une "abandonnée", dans ce cas, pas besoin de l'enlever du compte d'alertes
            if ($tabInfos->{'state.id'} eq STATE_ACTIVE && $tabInfos->{'encashment.id'} ne ENCASHMENTSTATE_RECEIVED)
            {
                # Mise à jour des alertes Règlements proforma (commerce et comptabilité)
                &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::PROFORMA_ALL, -1);
                &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::PROFORMA_AGENCY, -1);
            }
        }

        # Recensement des lignes de devis et des contrats
        if (defined $tabInfos->{'estimateLine.id'})
        {
            push(@{$tabDocuments->{'estimateLines'}}, $tabInfos->{'estimateLine.id'});
        }
        if (defined $tabInfos->{'contract.id'})
        {
            push(@{$tabDocuments->{'contracts'}}, $tabInfos->{'contract.id'});
        }
    }

    # Mise à jour des documents impactés
    if (!&_updateDocuments($db, $countryId, $tabDocuments, $userId, $tabEndUpds))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function getAllIds
# Recense toutes les proformas actives suivant si elles sont à jour ou non pour chaque type
#
# Parameters:
# string   $countryId    - Pays
# string   $documentType - Type de document (contrat/ligne de devis)
# int      $documentId   - Id du document
# arrayref $tabStates    - Etats à récupérer
#
# Returns:
# hash
sub getAllIds
{
    my ($countryId, $documentType, $documentId, $tabStates) = @_;


    if (!defined $tabStates)
    {
        $tabStates = [STATE_ACTIVE, STATE_REPLACED, STATE_ABORTED];
    }
    my $tabTypes = [LOC::Proforma::Type::ID_ADVANCE, LOC::Proforma::Type::ID_PROFORMA, LOC::Proforma::Type::ID_EXTENSION];

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # On recense toutes les proformas actives suivant si elles sont à jour ou non pour chaque type
    my $query = '
SELECT
    pfm_sta_id,
    pfm_id,
    pfm_pft_id,
    pfm_is_expired,
    IF (pfm_sta_id_encashment = "' . ENCASHMENTSTATE_RECEIVED . '" OR pfm_is_justified != 0 OR pfm_is_validated != 0, 1, 0)
FROM f_proforma
WHERE pfm_sta_id IN (' . $db->quote($tabStates) . ')';

    if ($documentType eq 'contract')
    {
        $query .= '
AND pfm_rct_id = ' . $documentId;
    }
    elsif ($documentType eq 'estimateLine')
    {
        $query .= '
AND pfm_rel_id = ' . $documentId;
    }
    my $stmt = $db->query($query);


    my $nbTypes = @$tabTypes;
    my $nbStates = @$tabStates;

    my $tab = {};
    for (my $i = 0; $i < $nbStates; $i++)
    {
        # Tableau vide
        $tab->{'state_' . $tabStates->[$i]} = {
            '_all' => [],
            'finalized' => [],
            'notExpired' => {
                '_all' => [],
                'finalized' => []
            },
            'expired' => {
                '_all' => [],
                'finalized' => []
            }
        };
        for (my $j = 0; $j < 4; $j++)
        {
            $tab->{'state_' . $tabStates->[$i]}->{'type_' . $tabTypes->[$j]} = {
                '_all' => [],
                'notExpired' => {
                    '_all' => [],
                    'finalized' => []
                },
                'expired' => {
                    '_all' => [],
                    'finalized' => []
                }
            };
        }
    }

    while (my $tabRow = $stmt->fetch(LOC::Db::FETCH_NUM))
    {
        my $stateKey    = 'state_' . $tabRow->[0];
        my $proformaId  = $tabRow->[1] * 1;
        my $typeKey     = 'type_' . $tabRow->[2];
        my $expiredKey  = ($tabRow->[3] * 1 ? 'expired' : 'notExpired');
        my $isFinalized = $tabRow->[4] * 1;

        push(@{$tab->{$stateKey}->{'_all'}}, $proformaId);
        push(@{$tab->{$stateKey}->{$typeKey}->{'_all'}}, $proformaId);
        push(@{$tab->{$stateKey}->{$expiredKey}->{'_all'}}, $proformaId);
        push(@{$tab->{$stateKey}->{$typeKey}->{$expiredKey}->{'_all'}}, $proformaId);
        if ($isFinalized)
        {
            push(@{$tab->{$stateKey}->{'finalized'}}, $proformaId);
            push(@{$tab->{$stateKey}->{$expiredKey}->{'finalized'}}, $proformaId);
            push(@{$tab->{$stateKey}->{$typeKey}->{$expiredKey}->{'finalized'}}, $proformaId);
        }
    }

    return $tab;
}


# Function getAuthorizedTypes
# retourne les types de pro forma possible
#
# Parameters:
# string $countryId     - Pays
# string $documentType  - Type de document (contrat/ligne de devis)
# int    $documentId    - Id du document
#
# Returns:
# arrayref
sub getAuthorizedTypes
{
    my ($countryId, $documentType, $documentId) = @_;

    my $tabAllIds = undef;
    if ($documentId)
    {
        # Récupération des stats du document
        $tabAllIds = &getAllIds($countryId, $documentType, $documentId, [STATE_ACTIVE, STATE_REPLACED]);
    }

    my $tabResult = &_getAuthorizedTypes($countryId, $documentType, $documentId, $tabAllIds);

    return (wantarray ? @$tabResult : $tabResult);
}


# Function getFuelInfos
# Récupérer l'estimation carburant pour un document
#
# Parameters:
# string $countryId     - Pays
# string $documentType  - Type de document (contrat/ligne de devis)
# int    $documentId    - Id du document
#
# Returns:
# arrayref
sub getFuelEstimationInfos
{
    my ($countryId, $documentType, $documentId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);


    my $query = '
SELECT
    pfm_fuelqty,
    pfm_fuelunitprice,
    SUM(pfm_fuelqty) AS fuelQtyTotal,
    count(*)
FROM f_proforma
WHERE pfm_sta_id = "' . STATE_ACTIVE . '"
AND pfm_is_expired = 0';

    if ($documentType eq 'contract')
    {
        $query .= '
AND pfm_rct_id = ' . $documentId;
    }
    elsif ($documentType eq 'estimateLine')
    {
        $query .= '
AND pfm_rel_id = ' . $documentId;
    }
    $query .= '
ORDER BY pfm_date_create DESC
LIMIT 1;';

    my $tabRow = $db->fetchRow($query, {}, LOC::Db::FETCH_NUM);

    my $quantity  = undef;
    my $unitPrice = 0;
    my $amount    = 0;
    if ($tabRow->[3] > 0)
    {
        $quantity  = $tabRow->[2] * 1;
        $unitPrice = $tabRow->[1] * 1;
        $amount    = $quantity * $unitPrice;
    }
    else
    {
        my $tabCountry = &LOC::Country::getInfos($countryId);
        $unitPrice  = $tabCountry->{'fuelUnitPrice'};
    }

    return {
        'quantity'  => $quantity,
        'unitPrice' => $unitPrice,
        'amount'    => $amount
    };
}


# Function: getInfos
# Retourne les infos d'une pro forma
#
# Parameters:
# string  $countryId  - Pays
# int     $id         - Id de la pro forma
# int     $flags      - Flags (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, UNDEF si pas de résultat
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);
    return ($tab && exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}


# Function getInfosByDocument
# retourne les informations nécessaires à la génération d'une proforma ou à l'édition d'un bon de valorisation
#
# Parameters:
# string   $countryId     - Pays
# string   $documentType  - Type de document (contrat/ligne de devis)
# int      $documentId    - Id du document
# hashref  $tabFirstData  - Données de la proforma
# arrayref $tabArticles   - lignes de la proforma à mettre à jour
#
# Returns:
# hashref
sub getInfosByDocument
{
    my ($countryId, $documentType, $documentId, $tabFirstData, $tabArticles) = @_;


    # Connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération des informations sur le document (contrat ou ligne de devis)
    my $query = '
SELECT

    CONCAT_WS(" ", tbl_f_familymachine.FAMAMODELE, tbl_f_familymachine.FAMAENERGIE, tbl_f_familymachine.FAMAELEVATION, tbl_f_manufacturer.FOLIBELLE, tbl_f_model.MOMADESIGNATION) AS `model`,
    CONCAT_WS(" ", tbl_f_site.CHLIBELLE, tbl_p_sitelocality.VICP, tbl_p_sitelocality.VINOM) AS `site`,
    tbl_f_tarifffamily.FAMTARCARBUQTEJOUR AS `fuelQtyEstimByDay`,
    tbl_f_tarifffamily.FAMTARCARBUQTEMAX AS `fuelQtyMax`,
    tbl_f_familymachine.FAMAENERGIE as `machine.energy`,
    tbl_f_customer.CLCEE AS `isVatInvoiced`,
    tbl_f_customer.MOPAAUTO AS `paymentMode.id`,';

    if ($documentType eq 'contract')
    {
        $query .= '
    tbl_f_contract.CONTRATAUTO AS `id`,
    tbl_f_contract.CONTRATCODE AS `code`,
    tbl_f_contract.CONTRATDD AS `beginDate`,
    tbl_f_contract.CONTRATDR AS `endDate`,
    tbl_f_contract.AGAUTO AS `agency.id`,
    tbl_f_contract.CONTRATNUMCOMM AS `orderNo`,
    tbl_f_contract.CONTRATCONTACTCOMM AS `orderContact.fullName`,
    tbl_f_contract.CONTRATTELEPHONECOMM AS `orderContact.telephone`,
    tbl_f_contract.CONTRATEMAILCOMM AS `orderContact.email`,
    tbl_f_contract.CLAUTO AS `customer.id`,
    tbl_f_contract.PEAUTO_INTERLOC AS `negotiator.id`,
    tbl_f_contractamount.MOASSFR AS `insurance.amount`,
    tbl_f_contract.CONTRATTYPEASSURANCE AS `insuranceType.id`,
    tbl_f_contract.CONTRATPASSURANCE AS `insurance.rate`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISINVOICED) . ', 1, 0) AS `insurance.isInvoiced`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISINCLUDED) . ', 1, 0) AS `insurance.isIncluded`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISCUSTOMER) . ', 1, 0) AS `insurance.isCustomer`,
    tbl_f_contractamount.MORECOURS AS `appeal.amount`,
    tbl_f_contract.CONTRATTYPERECOURS AS `appealType.id`,
    p_appealtype.apl_mode AS `appeal.isInvoiced`,
    tbl_f_contractamount.MOTRANSPORTDFR AS `delivery`,
    tbl_f_contractamount.MOTRANSPORTRFR AS `recovery`,
    IFNULL(tbl_f_contractcleaning.CONETMONTANT, IFNULL(tbl_p_cleaning.NETMONTANT, 0)) AS `cleaning`,
    IFNULL(tbl_p_cleaning.NETLIBELLE, "") AS `cleaning.label`,
    -tbl_f_contract.CONTRATDECHETSFACT AS `residues.mode`,
    tbl_f_contract.CONTRATDECHETSVAL AS `residues.value`,
    tbl_f_contractamount.MODECHETS AS `residues.amount`,
    IFNULL(tbl_f_contractfuel.CAMONTANTFR, 0) AS `fuelAmount`,
    IF(tbl_f_contractamount.MOFORFAITFR = 0, IF(tbl_f_contractamount.MOTARIFMSFR = 0, "day", "month"), "fixed") AS `amountType`,
    IF(tbl_f_contractamount.MOFORFAITFR = 0, IF(tbl_f_contractamount.MOTARIFMSFR = 0, tbl_f_contractamount.MOPXJOURFR, tbl_f_contractamount.MOTARIFMSFR), tbl_f_contractamount.MOFORFAITFR) AS `rentUnitPrice`,
    tbl_f_contract.CONTRATDUREE AS `duration`,
    SUM(tbl_f_contract.CONTRATJOURSPLUS + tbl_f_contract.CONTRATJOURPLUSFACT) AS `additionnalDays`,
    SUM(tbl_f_contract.CONTRATJOURSMOINS + tbl_f_contract.CONTRATJOURMOINSFACT) AS `deductedDays`,
    tbl_f_contract.CONTRATDATECREATION AS `creationDate`

FROM CONTRAT tbl_f_contract
INNER JOIN MONTANT tbl_f_contractamount
ON tbl_f_contractamount.MOAUTO = tbl_f_contract.MOAUTO
LEFT JOIN CLIENT tbl_f_customer
ON tbl_f_customer.CLAUTO = tbl_f_contract.CLAUTO
LEFT JOIN CHANTIER tbl_f_site
ON tbl_f_site.CHAUTO = tbl_f_contract.CHAUTO
LEFT JOIN CONTRATNETTOYAGE tbl_f_contractcleaning
ON tbl_f_contractcleaning.CONTRATAUTO = tbl_f_contract.CONTRATAUTO
LEFT JOIN NETTOYAGE tbl_p_cleaning
ON tbl_p_cleaning.NETAUTO = tbl_f_contractcleaning.NETAUTO
LEFT JOIN CARBURANT tbl_f_contractfuel
ON tbl_f_contractfuel.CARBUAUTO = tbl_f_contract.CARBUAUTO
LEFT JOIN AUTH.VICL tbl_f_customerlocality
ON tbl_f_customerlocality.VICLAUTO = tbl_f_customer.VICLAUTO
LEFT JOIN AUTH.MODELEMACHINE tbl_f_model
ON tbl_f_model.MOMAAUTO = tbl_f_contract.MOMAAUTO
LEFT JOIN FAMILLEMACHINE tbl_f_familymachine
ON tbl_f_familymachine.FAMAAUTO = tbl_f_model.FAMAAUTO
LEFT JOIN FAMILLETARIFAIRE tbl_f_tarifffamily
ON tbl_f_model.FAMTARAUTO = tbl_f_tarifffamily.FAMTARAUTO
LEFT JOIN FOURNISSEUR tbl_f_manufacturer
ON tbl_f_manufacturer.FOAUTO = tbl_f_model.FOAUTO
LEFT JOIN VILLECHA tbl_p_sitelocality
ON tbl_p_sitelocality.VILLEAUTO = tbl_f_site.VILLEAUTO
LEFT JOIN p_insurancetype
ON p_insurancetype.ity_id = tbl_f_contract.CONTRATTYPEASSURANCE
LEFT JOIN p_appealtype
ON p_appealtype.apl_id = tbl_f_contract.CONTRATTYPERECOURS
WHERE tbl_f_contract.CONTRATAUTO = ' . $documentId . ';';
    }
    elsif ($documentType eq 'estimateLine')
    {
        $query .= '
    tbl_f_estimateline.DETAILSDEVISAUTO AS `id`,
    (SELECT COUNT(tbl_f_estimateline2.DETAILSDEVISAUTO) + 1
     FROM DETAILSDEVIS tbl_f_estimateline2
     WHERE tbl_f_estimateline2.DEVISAUTO = tbl_f_estimateline.DEVISAUTO
     AND tbl_f_estimateline2.DETAILSDEVISAUTO < tbl_f_estimateline.DETAILSDEVISAUTO) AS `index`,
    ' . &LOC::Estimate::Rent::generateCode('tbl_f_estimate.DEVISAUTO', 'tbl_f_estimate.AGAUTO',
                     '-tbl_f_estimate.DEVISFULLSERVICE', 1) . ' AS `code`,
    tbl_f_estimateline.DETAILSDEVISDATE AS `beginDate`,
    tbl_f_estimateline.DETAILSDEVISDATEFIN AS `endDate`,
    tbl_f_estimate.AGAUTO AS `agency.id`,
    tbl_f_estimate.DEVISNUMCOM AS `orderNo`,
    tbl_f_estimate.DEVISCONTACTCOM AS `orderContact.fullName`,
    tbl_f_estimate.DEVISTELEPHONECOM AS `orderContact.telephone`,
    tbl_f_estimate.DEVISEMAILCOM AS `orderContact.email`,
    tbl_f_estimate.CLAUTO AS `customer.id`,
    tbl_f_estimate.PEAUTO_INTERLOC AS `negotiator.id`,
    (SELECT COUNT(tbl_f_estimateline2.DETAILSDEVISAUTO) + 1
     FROM DETAILSDEVIS tbl_f_estimateline2
     WHERE tbl_f_estimateline2.DEVISAUTO = tbl_f_estimateline.DEVISAUTO
     AND tbl_f_estimateline2.DETAILSDEVISAUTO < tbl_f_estimateline.DETAILSDEVISAUTO) AS `index`,
    tbl_f_estimateline.DETAILSDEVISASSMONTANT AS `insurance.amount`,
    tbl_f_estimateline.DETAILSDEVISTYPEASSURANCE AS `insuranceType.id`,
    tbl_f_estimateline.DETAILSDEVISASSTAUX AS `insurance.rate`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISINVOICED) . ', 1, 0) AS `insurance.isInvoiced`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISINCLUDED) . ', 1, 0) AS `insurance.isIncluded`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISCUSTOMER) . ', 1, 0) AS `insurance.isCustomer`,
    tbl_f_estimateline.DETAILSDEVISRECOURSMONTANT AS `appeal.amount`,
    tbl_f_estimateline.DETAILSDEVISTYPERECOURS AS `appealType.id`,
    p_appealtype.apl_mode AS `appeal.isInvoiced`,
    tbl_f_estimateline.DETAILSDEVISTRD AS `delivery`,
    tbl_f_estimateline.DETAILSDEVISTRR AS `recovery`,
    tbl_f_estimateline.DETAILSDEVISNETMONTANT AS `cleaning`,
    IFNULL(tbl_p_cleaning.NETLIBELLE, "") AS `cleaning.label`,
    -tbl_f_estimateline.DETAILSDEVISDECHETSFACT AS `residues.mode`,
    tbl_f_estimateline.DETAILSDEVISDECHETSVAL AS `residues.value`,
    tbl_f_estimateline.DETAILSDEVISDECHETSMONTANT AS `residues.amount`,
    0 AS `fuelAmount`,
    IF (tbl_f_estimateline.DETAILSDEVISTYPEMONTANT = 1, "day", "month") AS `amountType`,
    tbl_f_estimateline.DETAILSDEVISPXJOURSFR AS `rentUnitPrice`,
    tbl_f_estimateline.DETAILSDEVISDUREE AS `duration`,
    tbl_f_estimateline.DETAILSDEVISJOURSPLUS AS `additionnalDays`,
    tbl_f_estimateline.DETAILSDEVISJOURSMOINS AS `deductedDays`,
    tbl_f_estimateline.DETAILSDEVISDATECREATION AS `creationDate`

FROM DETAILSDEVIS tbl_f_estimateline
INNER JOIN DEVIS tbl_f_estimate
ON tbl_f_estimate.DEVISAUTO = tbl_f_estimateline.DEVISAUTO
INNER JOIN AUTH.MODELEMACHINE tbl_f_model
ON tbl_f_model.MOMAAUTO = tbl_f_estimateline.MOMAAUTO
LEFT JOIN NETTOYAGE tbl_p_cleaning
ON tbl_p_cleaning.NETAUTO = tbl_f_estimateline.DETAILSDEVISNETAUTO
LEFT JOIN CLIENT tbl_f_customer
ON tbl_f_customer.CLAUTO = tbl_f_estimate.CLAUTO
LEFT JOIN CHANTIER tbl_f_site
ON tbl_f_site.CHAUTO = tbl_f_estimate.CHAUTO
LEFT JOIN AUTH.VICL tbl_f_customerlocality
ON tbl_f_customerlocality.VICLAUTO = tbl_f_customer.VICLAUTO
LEFT JOIN FAMILLEMACHINE tbl_f_familymachine
ON tbl_f_familymachine.FAMAAUTO = tbl_f_model.FAMAAUTO
LEFT JOIN FAMILLETARIFAIRE tbl_f_tarifffamily
ON tbl_f_model.FAMTARAUTO = tbl_f_tarifffamily.FAMTARAUTO
LEFT JOIN FOURNISSEUR tbl_f_manufacturer
ON tbl_f_manufacturer.FOAUTO = tbl_f_model.FOAUTO
LEFT JOIN VILLECHA tbl_p_sitelocality
ON tbl_p_sitelocality.VILLEAUTO = tbl_f_site.VILLEAUTO
LEFT JOIN p_insurancetype
ON p_insurancetype.ity_id = tbl_f_estimateline.DETAILSDEVISTYPEASSURANCE
LEFT JOIN p_appealtype
ON p_appealtype.apl_id = tbl_f_estimateline.DETAILSDEVISTYPERECOURS
WHERE tbl_f_estimateline.DETAILSDEVISAUTO = ' . $documentId . ';';
    }

    my $tabInfosDocument = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);

    # Calcul du prix par jour
    $tabInfosDocument->{'rentDailyPrice'} = &LOC::Common::Rent::getDailyPrice(
        $tabInfosDocument->{'amountType'},
        $tabInfosDocument->{'rentUnitPrice'},
        $tabInfosDocument->{'duration'}
    );

    if (!$tabInfosDocument)
    {
        return undef;
    }

    # Génération des lignes de la pro forma
    my $tabFiltersArticle = {};
    if (defined $tabArticles)
    {
        $tabFiltersArticle->{'id'} = $tabArticles;
    }
    my $tabArtRefs = &LOC::Proforma::ArticleRef::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFiltersArticle);
    my $tabVAT     = &LOC::Country::getVATRatesList($countryId);
    my $tabCountry = &LOC::Country::getInfos($countryId);


    # Mise en forme des données de proforma
    my $tabData = {};
    $tabData->{'type.id'}        = (exists $tabFirstData->{'type.id'} ? $tabFirstData->{'type.id'} * 1 : LOC::Proforma::Type::ID_PROFORMA);
    $tabData->{'paidAmount'}     = (exists $tabFirstData->{'paidAmount'} ? &LOC::Util::round($tabFirstData->{'paidAmount'}, 2) : 0);
    $tabData->{'deposit'}        = (exists $tabFirstData->{'deposit'} ? &LOC::Util::round($tabFirstData->{'deposit'}, 2) : 0);
    $tabData->{'endDate'}        = (exists $tabFirstData->{'endDate'} && &LOC::Date::checkMySQLDate($tabFirstData->{'endDate'}) && $tabFirstData->{'endDate'} ne '' ? $tabFirstData->{'endDate'} : undef);
    $tabData->{'paymentMode.id'} = (exists $tabFirstData->{'paymentMode.id'} ? $tabFirstData->{'paymentMode.id'} * 1 : $tabInfosDocument->{'paymentMode.id'} * 1);
    $tabData->{'fuelQuantity'}   = (exists $tabFirstData->{'fuelQuantity'} ? $tabFirstData->{'fuelQuantity'} * 1 : 0);
    $tabData->{'comment'}        = (exists $tabFirstData->{'comment'} ? &LOC::Util::trim($tabFirstData->{'comment'}) : undef);
    $tabData->{'isVatInvoiced'}  = (exists $tabFirstData->{'isVatInvoiced'} ? ($tabFirstData->{'isVatInvoiced'} ? 1 : 0) : $tabInfosDocument->{'isVatInvoiced'} * 1);

    my $advanceAmount = $tabFirstData->{'amount'};

    if ($tabData->{'type.id'} == LOC::Proforma::Type::ID_ADVANCE)
    {
        $tabData->{'paidAmount'}   = 0;
        $tabData->{'deposit'}      = 0;
        $tabData->{'fuelQuantity'} = 0;
        $tabData->{'endDate'}      = undef;
    }
    elsif ($tabData->{'type.id'} == LOC::Proforma::Type::ID_PROFORMA)
    {
        $tabData->{'endDate'} = undef;
    }
    if ($documentType ne 'contract')
    {
        $tabData->{'endDate'} = undef;
    }

    # Autres informations
    $tabData->{'amount'}         = 0;
    $tabData->{'amountWithVat'}  = 0;
    $tabData->{'customer.id'}    = $tabInfosDocument->{'customer.id'} * 1;
    $tabData->{'beginDate'}      = $tabInfosDocument->{'beginDate'};
    $tabData->{'realEndDate'}    = $tabInfosDocument->{'endDate'};
    $tabData->{'fuelUnitPrice'}  = $tabCountry->{'fuelUnitPrice'};
    $tabData->{'agency.id'}      = $tabInfosDocument->{'agency.id'};
    $tabData->{'duration'}       = 0;
    $tabData->{'fuelQtyEstimByDay'} = $tabInfosDocument->{'fuelQtyEstimByDay'};
    $tabData->{'fuelQtyMax'}     = $tabInfosDocument->{'fuelQtyMax'};
    $tabData->{'machine.energy'} = $tabInfosDocument->{'machine.energy'};

    if ($documentType eq 'contract')
    {
        $tabData->{'contract.code'}                   = $tabInfosDocument->{'code'};
        $tabData->{'contract.agency.id'}              = $tabInfosDocument->{'agency.id'};
        $tabData->{'contract.orderNo'}                = $tabInfosDocument->{'orderNo'};
        $tabData->{'contract.orderContact.fullName'}  = $tabInfosDocument->{'orderContact.fullName'};
        $tabData->{'contract.orderContact.telephone'} = $tabInfosDocument->{'orderContact.telephone'};
        $tabData->{'contract.orderContact.email'}     = $tabInfosDocument->{'orderContact.email'};
        $tabData->{'contract.site'}                   = $tabInfosDocument->{'site'};
        $tabData->{'contract.negotiator.id'}          = $tabInfosDocument->{'negotiator.id'};

        $tabData->{'estimateLine.index'}              = undef;
        $tabData->{'estimateLine.estimate.code'}      = undef;
        $tabData->{'estimateLine.estimate.agency.id'} = undef;

        $tabInfosDocument->{'valueDate'} = &LOC::Contract::Rent::getValueDate($countryId, $tabInfosDocument->{'id'});
    }
    elsif ($documentType eq 'estimateLine')
    {
        $tabData->{'estimateLine.index'}              = $tabInfosDocument->{'index'};
        $tabData->{'estimateLine.estimate.code'}      = $tabInfosDocument->{'code'};
        $tabData->{'estimateLine.estimate.agency.id'} = $tabInfosDocument->{'agency.id'};
        $tabData->{'estimateLine.estimate.orderNo'}   = $tabInfosDocument->{'orderNo'};
        $tabData->{'estimateLine.estimate.orderContact.fullName'}  = $tabInfosDocument->{'orderContact.fullName'};
        $tabData->{'estimateLine.estimate.orderContact.telephone'} = $tabInfosDocument->{'orderContact.telephone'};
        $tabData->{'estimateLine.estimate.orderContact.email'}     = $tabInfosDocument->{'orderContact.email'};
        $tabData->{'estimateLine.estimate.site'}                   = $tabInfosDocument->{'site'};
        $tabData->{'estimateLine.estimate.negotiator.id'}          = $tabInfosDocument->{'negotiator.id'};

        $tabData->{'contract.code'}                   = undef;
        $tabData->{'contract.agency.id'}              = undef;

        $tabInfosDocument->{'valueDate'} = &LOC::Estimate::Rent::getLineValueDate($countryId, $tabInfosDocument->{'id'});
    }
    $tabData->{'document.amountType'} = $tabInfosDocument->{'amountType'};


    my $tabRepairDays = {};
    my $tabRepairServices = {};
    my $tabRentServices = {};
    my $tabPreviousProformas = {};
    # Récupération des données spécifiques au contrat
    if ($documentType eq 'contract')
    {
        # Ajout des informations sur les jours de remise en état
		my $tabRepairDaysList = &LOC::Contract::RepairDay::getList($countryId, LOC::Util::GETLIST_ASSOC, {'contractId' => $documentId});
        # Réorganisation du tableau : regroupement des jours de remise en état par fiche de remise en état
        foreach my $tabRepairDayInfo (values %$tabRepairDaysList)
        {
            push(@{$tabRepairDays->{$tabRepairDayInfo->{'repairsheet.id'}}}, $tabRepairDayInfo);
        }

        # Ajout des informations sur les services de remise en état
        my $tabRepairServicesList = &LOC::Contract::RepairService::getList($countryId, LOC::Util::GETLIST_ASSOC, {'contractId' => $documentId});
        # Réorganisation du tableau : regroupement des services de remise en état par fiche de remise en état
        foreach my $tabRepairServiceInfo (values %$tabRepairServicesList)
        {
            push(@{$tabRepairServices->{$tabRepairServiceInfo->{'repairsheet.id'}}}, $tabRepairServiceInfo);
        }

        # Ajout des informations sur les services de location et transport
        $tabRentServices = &LOC::Contract::Rent::getRentServices($countryId, $documentId);

        # Récupération des pro formas précedentes
        if ($tabData->{'type.id'} == LOC::Proforma::Type::ID_EXTENSION)
        {
            my $tabFilters = {
                'contractId'  => $documentId,
                'isFinalized' => 1,
                'isExpired'   => 0
            };
            $tabPreviousProformas = &getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters);
        }
    }

    # Récupération des données spécifiques au devis
    if ($documentType eq 'estimateLine')
    {
        # Ajout des informations sur les services de location et transport
        $tabRentServices = &LOC::Estimate::Rent::getLineRentServices($countryId, $documentId);
    }

    # Paramètres pour les pro forma de prolongation
    my ($beginDate, $endDate, $nbDays, $nbDaysWithExtension);

    if ($tabData->{'type.id'} == LOC::Proforma::Type::ID_EXTENSION)
    {
        # Nombre de jours de location du document d'origine
        my $nbDaysOriginal   = &LOC::Date::getRentDuration($tabInfosDocument->{'beginDate'}, $tabInfosDocument->{'endDate'}, $tabInfosDocument->{'agency.id'});

        # Nombre de jours de location du document avec prolongation
        my $extensionEndDate = (defined $tabData->{'endDate'} ? $tabData->{'endDate'} : $tabInfosDocument->{'endDate'});

        $nbDaysWithExtension = &LOC::Date::getRentDuration($tabInfosDocument->{'beginDate'}, $extensionEndDate, $tabInfosDocument->{'agency.id'});

        # Début et fin de la période de location, nombre de jours ouvrés de location
        $beginDate = &LOC::Date::getNextDay($tabInfosDocument->{'endDate'});
        $endDate   = $extensionEndDate;
        $nbDays    = $nbDaysWithExtension - $nbDaysOriginal;

        # pour l'enregistrement la date de début de location = date de début de la prolongation
        $tabData->{'beginDate'} = $beginDate;
    }
    else
    {
        # Début et fin de la période de location, nombre de jours ouvrés de location
        $beginDate = $tabInfosDocument->{'beginDate'};
        $endDate   = $tabInfosDocument->{'endDate'};
        $nbDays    = &LOC::Date::getRentDuration($beginDate, $endDate, $tabInfosDocument->{'agency.id'});
    }



    # Lignes
    $tabData->{'tabLines'} = [];

    foreach my $tabArtRefInfos (values %{$tabArtRefs})
    {
        # ne pas insérer certaines lignes pour les proforma de prolongation
        if (($tabData->{'type.id'} == LOC::Proforma::Type::ID_PROFORMA &&
             ($tabArtRefInfos->{'id'} eq 'CARB' || $tabArtRefInfos->{'id'} eq 'JRSRE' || $tabArtRefInfos->{'id'} eq 'SERRE')) ||
            ($tabData->{'type.id'} == LOC::Proforma::Type::ID_EXTENSION &&
             ($tabArtRefInfos->{'id'} ne 'ASSUR' && $tabArtRefInfos->{'id'} ne 'RECO'&&
              $tabArtRefInfos->{'id'} ne 'ECARB' &&
              $tabArtRefInfos->{'id'} ne 'LOCAT' && $tabArtRefInfos->{'id'} ne 'DECHE')))
        {
            next;
        }

        my $tabTmpLines = [];
        my $vatRate   = &LOC::Util::round($tabVAT->{$tabArtRefInfos->{'vat.id'}}->{'value'} * 100, 4);
        my $lineOrder = $tabArtRefInfos->{'order'};

        # Données spécifiques

        # Article : Location
        if ($tabArtRefInfos->{'id'} eq 'LOCAT')
        {
            my $quantity = $nbDays;
            $tabData->{'duration'} += $quantity;

            push(@$tabTmpLines, {
                'articleRef' => $tabArtRefInfos->{'id'},
                'label'      => $tabArtRefInfos->{'label'},
                'labelExtra' => sprintf($tabArtRefInfos->{'labelExtra'},
                                        $tabInfosDocument->{'model'},
                                        '<%dte:' . $beginDate . '|10>',
                                        '<%dte:' . $endDate . '|10>'),
                'quantity'   => $quantity,
                'unitPrice'  => &LOC::Util::round($tabInfosDocument->{'rentUnitPrice'}, 4)
            });
        }
        # Article : Jours supplémentaires
        elsif ($tabArtRefInfos->{'id'} eq 'JRSUP')
        {
            if ($tabInfosDocument->{'additionnalDays'} > 0)
            {
                $tabData->{'duration'} += $tabInfosDocument->{'additionnalDays'};
                push(@$tabTmpLines, {
                    'articleRef' => $tabArtRefInfos->{'id'},
                    'label'      => $tabArtRefInfos->{'label'},
                    'quantity'   => $tabInfosDocument->{'additionnalDays'},
                    'unitPrice'  => &LOC::Util::round($tabInfosDocument->{'rentUnitPrice'}, 4)
                });
            }
        }
        # Article : Jours déduits
        elsif ($tabArtRefInfos->{'id'} eq 'JRDED')
        {
            if ($tabInfosDocument->{'deductedDays'} > 0)
            {
                $tabData->{'duration'} -= $tabInfosDocument->{'deductedDays'};
                push(@$tabTmpLines, {
                    'articleRef' => $tabArtRefInfos->{'id'},
                    'label'      => $tabArtRefInfos->{'label'},
                    'quantity'   => -$tabInfosDocument->{'deductedDays'},
                    'unitPrice'  => &LOC::Util::round($tabInfosDocument->{'rentUnitPrice'}, 4)
                });
            }
        }
        # Article : Assurance
        elsif ($tabArtRefInfos->{'id'} eq 'ASSUR')
        {
            if ($tabInfosDocument->{'insurance.isInvoiced'})
            {
                my $unitPrice = $tabInfosDocument->{'insurance.amount'};

                # Si on a saisi une date de fin
                if ($tabData->{'type.id'} == LOC::Proforma::Type::ID_EXTENSION)
                {
                    my $unitPriceWithExtension = &LOC::Common::Rent::calculateInsurance($countryId, $tabInfosDocument->{'agency.id'},
                                                                                        $tabInfosDocument->{'insuranceType.id'}, $tabInfosDocument->{'insurance.rate'},
                                                                                        $tabInfosDocument->{'rentDailyPrice'},
                                                                                        $tabInfosDocument->{'beginDate'}, $endDate,
                                                                                        $tabInfosDocument->{'additionnalDays'}, $tabInfosDocument->{'deductedDays'});

                    $unitPrice = $unitPriceWithExtension - $unitPrice;
                }

                push(@$tabTmpLines, {
                    'articleRef' => $tabArtRefInfos->{'id'},
                    'label'      => $tabArtRefInfos->{'label'},
                    'quantity'   => 1,
                    'unitPrice'  => &LOC::Util::round($unitPrice, 4)
                });
            }
        }
        # Article : Gestion de recours
        elsif ($tabArtRefInfos->{'id'} eq 'RECO')
        {
            if ($tabInfosDocument->{'appeal.isInvoiced'})
            {
                my $unitPrice = $tabInfosDocument->{'appeal.amount'};

                # Si on a saisi une date de fin
                if ($tabData->{'type.id'} == LOC::Proforma::Type::ID_EXTENSION)
                {
                    my $appealRate = &LOC::Characteristic::getCountryValueByCode('PCRECOURS', $countryId, $tabInfosDocument->{'valueDate'}) * 1;
                    my $unitPriceWithExtension = &LOC::Common::Rent::calculateAppeal($countryId, $tabInfosDocument->{'agency.id'},
                                                                                     $tabInfosDocument->{'appealType.id'}, $appealRate,
                                                                                     $tabInfosDocument->{'rentDailyPrice'},
                                                                                     $tabInfosDocument->{'beginDate'}, $endDate,
                                                                                     $tabInfosDocument->{'additionnalDays'}, $tabInfosDocument->{'deductedDays'});

                    $unitPrice = $unitPriceWithExtension - $unitPrice;
                }

                push(@$tabTmpLines, {
                    'articleRef' => $tabArtRefInfos->{'id'},
                    'label'      => $tabArtRefInfos->{'label'},
                    'quantity'   => 1,
                    'unitPrice'  => &LOC::Util::round($unitPrice, 4)
                });
            }
        }
        # Article : Transport allé
        elsif ($tabArtRefInfos->{'id'} eq 'TRANA')
        {
            if ($tabInfosDocument->{'delivery'} != 0)
            {
                push(@$tabTmpLines, {
                    'articleRef' => $tabArtRefInfos->{'id'},
                    'label'      => $tabArtRefInfos->{'label'},
                    'quantity'   => 1,
                    'unitPrice'  => &LOC::Util::round($tabInfosDocument->{'delivery'}, 4)
                });
            }
        }
        # Article : Transport retour
        elsif ($tabArtRefInfos->{'id'} eq 'TRANR')
        {
            if ($tabInfosDocument->{'recovery'} != 0)
            {
                push(@$tabTmpLines, {
                    'articleRef' => $tabArtRefInfos->{'id'},
                    'label'      => $tabArtRefInfos->{'label'},
                    'quantity'   => 1,
                    'unitPrice'  => &LOC::Util::round($tabInfosDocument->{'recovery'}, 4)
                });
            }
        }
        # Article : Estimation carburant
        elsif ($tabArtRefInfos->{'id'} eq 'ECARB')
        {
            my $qty = $tabData->{'fuelQuantity'};

            if (defined $qty && $qty != 0)
            {
                push(@$tabTmpLines, {
                    'articleRef' => $tabArtRefInfos->{'id'},
                    'label'      => $tabArtRefInfos->{'label'},
                    'quantity'   => 1,
                    'unitPrice'  => &LOC::Util::round($tabData->{'fuelUnitPrice'} * $qty, 4)
                });
            }
        }
        # Article : Carburant
        elsif ($tabArtRefInfos->{'id'} eq 'CARB')
        {
            if ($tabInfosDocument->{'fuelAmount'} != 0)
            {
                push(@$tabTmpLines, {
                    'articleRef' => $tabArtRefInfos->{'id'},
                    'label'      => $tabArtRefInfos->{'label'},
                    'quantity'   => 1,
                    'unitPrice'  => &LOC::Util::round($tabInfosDocument->{'fuelAmount'}, 4)
                });
            }
        }
        # Article : Nettoyage
        elsif ($tabArtRefInfos->{'id'} eq 'NETTO')
        {
            if ($tabInfosDocument->{'cleaning'} != 0)
            {
                push(@$tabTmpLines, {
                    'articleRef' => $tabArtRefInfos->{'id'},
                    'label'      => sprintf($tabArtRefInfos->{'label'}, $tabInfosDocument->{'cleaning.label'}),
                    'quantity'   => 1,
                    'unitPrice'  => &LOC::Util::round($tabInfosDocument->{'cleaning'}, 4)
                });
            }
        }
        # Article : Gestion des déchets
        elsif ($tabArtRefInfos->{'id'} eq 'DECHE')
        {
            # Participation au recyclage facturée
            if ($tabInfosDocument->{'residues.mode'} != LOC::Common::RESIDUES_MODE_NONE)
            {
                # Montant des résidus
                my $unitPrice = $tabInfosDocument->{'residues.amount'};

                # Pro forma de prolongation : calcul du montant des résidus
                if ($tabData->{'type.id'} == LOC::Proforma::Type::ID_EXTENSION)
                {
                    $unitPrice = undef;
                    # Mode pourcentage
                    if ($tabInfosDocument->{'residues.mode'} == LOC::Common::RESIDUES_MODE_PERCENT)
                    {
                        my $unitPriceWithExtension = &LOC::Common::Rent::calculateResiduesAmount($tabInfosDocument->{'agency.id'},
                                                                                 $tabInfosDocument->{'residues.mode'},
                                                                                 $tabInfosDocument->{'residues.value'},
                                                                                 $tabInfosDocument->{'rentDailyPrice'},
                                                                                 $tabInfosDocument->{'beginDate'},
                                                                                 $endDate,
                                                                                 $tabInfosDocument->{'additionnalDays'},
                                                                                 $tabInfosDocument->{'deductedDays'});
                        $unitPrice = $unitPriceWithExtension - $tabInfosDocument->{'residues.amount'};
                    }
                }

                if (defined $unitPrice)
                {
                    push(@$tabTmpLines, {
                        'articleRef' => $tabArtRefInfos->{'id'},
                        'label'      => $tabArtRefInfos->{'label'},
                        'quantity'   => 1,
                        'unitPrice'  => &LOC::Util::round($unitPrice, 4)
                    });
                }
            }
        }
        # Article : Services complémentaires
        elsif ($tabArtRefInfos->{'id'} eq 'SERCL')
        {
            foreach my $tabRentSrvInfos (values %$tabRentServices)
            {
                if ($tabRentSrvInfos->{'state.id'} ne LOC::Common::RentService::STATE_DELETED)
                {
                    push(@$tabTmpLines, {
                        'articleRef' => $tabArtRefInfos->{'id'},
                        'label'      => $tabRentSrvInfos->{'rentService.name'},
                        'labelExtra' => $tabRentSrvInfos->{'comment'},
                        'quantity'   => 1,
                        'unitPrice'  => &LOC::Util::round($tabRentSrvInfos->{'amount'}, 4)
                    });
                }
            }
        }
        # Article : Jours de remise en état
        elsif ($tabArtRefInfos->{'id'} eq 'JRSRE')
        {
            # parcours chaque FRE basculée
            foreach my $sheetRepair (values %$tabRepairDays)
            {
                # parcours chaque service de la FRE
                foreach my $tabRprDayInfos (@$sheetRepair)
                {
                    if ($tabRprDayInfos->{'state.id'} ne LOC::Contract::RepairDay::STATE_CANCELED)
                    {
                        push(@$tabTmpLines, {
                            'articleRef' => $tabArtRefInfos->{'id'},
                            'label'      => $tabArtRefInfos->{'label'},
                            'quantity'   => $tabRprDayInfos->{'number'},
                            'unitPrice'  => &LOC::Util::round($tabRprDayInfos->{'unitAmount'}, 4)
                        });
                    }
                }
            }
        }
        # Article : Services de remise en état
        elsif ($tabArtRefInfos->{'id'} eq 'SERRE')
        {
            # parcours chaque FRE basculée
            foreach my $sheetRepair (values %$tabRepairServices)
            {
                # parcours chaque service de la FRE
                foreach my $tabRprSrvInfos (@$sheetRepair)
                {
                    if ($tabRprSrvInfos->{'state.id'} ne LOC::Contract::RepairService::STATE_CANCELED)
                    {
                        push(@$tabTmpLines, {
                            'articleRef' => $tabArtRefInfos->{'id'},
                            'label'      => $tabRprSrvInfos->{'label'},
                            'quantity'   => $tabRprSrvInfos->{'quantity'},
                            'unitPrice'  => &LOC::Util::round($tabRprSrvInfos->{'unitAmount'}, 4)
                        });
                    }
                }
            }
        }

        my $linesCount = @$tabTmpLines;
        if ($linesCount > 0)
        {
            for (my $i = 0; $i < $linesCount; $i++)
            {
                $tabTmpLines->[$i]->{'vatRate'} = $vatRate;
                $tabTmpLines->[$i]->{'order'}   = $lineOrder;
                $tabTmpLines->[$i]->{'amount'}  = &LOC::Util::round($tabTmpLines->[$i]->{'quantity'} * $tabTmpLines->[$i]->{'unitPrice'}, 4);

                $tabData->{'amount'} += $tabTmpLines->[$i]->{'amount'};
                if ($tabData->{'isVatInvoiced'})
                {
                    $tabData->{'amountWithVat'} += &LOC::Util::round($tabTmpLines->[$i]->{'quantity'} * $tabTmpLines->[$i]->{'unitPrice'} * (1 + $vatRate / 100), 4);
                }
                else
                {
                    $tabData->{'amountWithVat'} = $tabData->{'amount'};
                }

                push(@{$tabData->{'tabLines'}}, $tabTmpLines->[$i]);
            }
        }
    }


    # Pour la facture d'acompte
    if ($tabData->{'type.id'} == LOC::Proforma::Type::ID_ADVANCE)
    {
        if ($advanceAmount < 1)
        {
            $advanceAmount = 1;
        }
        elsif ($advanceAmount >= $tabData->{'amount'})
        {
            $advanceAmount = $tabData->{'amount'};
        }
        $tabData->{'percent'} = &LOC::Util::round(100 * $advanceAmount / $tabData->{'amount'}, 2);

        my @tabLabelParams;
        my $tabArtRefInfos = {};
        if ($documentType eq 'contract')
        {
            $tabArtRefInfos = $tabArtRefs->{'ACPTEC'};

            @tabLabelParams = (
                '<%num:' . $tabData->{'percent'} . '|2|1>',
                $tabInfosDocument->{'code'},
                '<%dte:' . substr($tabInfosDocument->{'creationDate'}, 0, 10) . '|10>',
                '<%cur:' . $tabData->{'amount'} . '|2|' . $tabCountry->{'currency.symbol'} . '>'
            );
        }
        elsif ($documentType eq 'estimateLine')
        {
            $tabArtRefInfos = $tabArtRefs->{'ACPTELD'};

            @tabLabelParams = (
                '<%num:' . $tabData->{'percent'} . '|2|1>',
                $tabInfosDocument->{'index'},
                $tabInfosDocument->{'code'},
                '<%dte:' . substr($tabInfosDocument->{'creationDate'}, 0, 10) . '|10>',
                '<%cur:' . $tabData->{'amount'} . '|2|' . $tabCountry->{'currency.symbol'} . '>'
            );
        }

        my $vatRate   = $tabVAT->{$tabArtRefInfos->{'vat.id'}}->{'value'} * 100;
        my $lineOrder = $tabArtRefInfos->{'order'};

        my $tabTmpLine = {
            'articleRef'    => $tabArtRefInfos->{'id'},
            'label'         => sprintf($tabArtRefInfos->{'label'}, @tabLabelParams),
            'labelExtra'    => sprintf($tabArtRefs->{'LOCAT'}->{'labelExtra'},
                                       $tabInfosDocument->{'model'},
                                       '<%dte:' . $tabInfosDocument->{'beginDate'} . '|10>',
                                       '<%dte:' . $tabInfosDocument->{'endDate'} . '|10>'),
            'quantity'      => 1,
            'unitPrice'     => $advanceAmount,
            'vatRate'       => $vatRate,
            'order'         => $lineOrder,
            'amount'        => &LOC::Util::round($advanceAmount, 4)
        };

        # On remplace toutes les lignes par celle de l'acompte
        $tabData->{'tabLines'} = [$tabTmpLine];

        $tabData->{'amount'} = $tabTmpLine->{'amount'};
        if ($tabData->{'isVatInvoiced'})
        {
            $tabData->{'amountWithVat'} = &LOC::Util::round($advanceAmount * (1 + $vatRate / 100), 4);
        }
        else
        {
            $tabData->{'amountWithVat'} = $tabData->{'amount'};
        }
    }
    else
    {
        $tabData->{'percent'} = 100;
    }

    # Arrondis des montants
    $tabData->{'amount'}        = &LOC::Util::round($tabData->{'amount'}, 2);
    $tabData->{'amountWithVat'} = &LOC::Util::round($tabData->{'amountWithVat'}, 2);

    # Calcul du montant total
    $tabData->{'total'} = $tabData->{'amountWithVat'} - $tabData->{'paidAmount'};

    # - Doit-on compter le dépôt de garantie dans le montant total de la pro forma ?
    my $isDepositInTotal = &LOC::Characteristic::getCountryValueByCode('PFDEPOSITTOTAL', $countryId) * 1;
    if ($isDepositInTotal)
    {
        $tabData->{'total'} += $tabData->{'deposit'};
    }
    $tabData->{'total'} = LOC::Util::round($tabData->{'total'}, 2);

    # Non encaissé
    $tabData->{'remainDue'} = $tabData->{'total'};

    # Encaissé
    $tabData->{'cashed'} = LOC::Util::round($tabData->{'total'} - $tabData->{'remainDue'}, 2);

    return $tabData;
}


# Function: getList
# Retourne la liste des pro forma
#
# Contenu du hashage :
# - id => identifiant,
# - type.id => identifiant du type de proforma,
# - estimate.id => identifiant du devis,
# - contract.id => identifiant du contrat,
# - customer.id => identifiant du client sur le contrat ou le devis,
# - amount => montant de la pro forma,
# - isVatInvoiced => facturation de la TVA (1) ou non (0),
# - paidAmount => montant du déjà payé,
# - deposit => montant du dépot de garantie,
# - paymentMode.id => identifiant du mode de paiement,
# - paymentMode.label => libelle du mode de paiement,
# - beginDate => date de début du document associé,
# - endDate => date de fin souhaitée,
# - fuelQuantity => quantitée de carburant,
# - comment => commentaire de la pro forma,
# - isJustified  => pro forma justifiée (1) ou non (0),
# - isValidated => pro forma validée (1) ou non (0),
# - validcomment => commentaire de validation,
# - enchasment.id => identifiant de l'état d'encaissement,
# - closure.id => identifiant de l'état de cloture,
# - remainDue => reste dû,
# - user.id => identifiant de l'utilisateur,
# - creationDate => date de création,
# - modificationDate => date de modification,
# - state.id => identifiant de l'etat,
# - tabLines => information sur les lignes articles (si $flags & GETINFOS_LINES)

# Parameters:
# string  $countryId  - Identifiant du pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres (facultatif)
# int     $flags      - Flags pour le choix des colonnes (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|0 - Liste des pro formas
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;

    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }
    if (!defined $tabFilters)
    {
        $tabFilters = {};
    }

    # Filtre sur l'état (par défaut on ne prend pas les pro formas supprimées)
    if (!defined $tabFilters->{'stateId'})
    {
        $tabFilters->{'stateId'} = [STATE_ACTIVE, STATE_REPLACED, STATE_ABORTED];
    }
    if (!defined $tabOptions->{'order'})
    {
        $tabOptions->{'order'} = 'pfm.pfm_date_create ASC';
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $tariffRentSchemaName = 'GESTIONTARIF' . $countryId;
    my $tariffTrspSchemaName = 'GESTIONTRANS' . $countryId;

    my $query;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    pfm.pfm_id AS `id`,
    pfm.pfm_id_link AS `link.id`,
    pfm.pfm_id_extension AS `extension.id`,
    pfm.pfm_pft_id AS `type.id`,
    pft.pft_label AS `type.label`,
    pfm.pfm_rel_id AS `estimateLine.id`,
    pfm.pfm_rct_id AS `contract.id`,
    pfm.pfm_cus_id AS `customer.id`,
    pfm.pfm_amount AS `amount`,
    pfm.pfm_is_vatinvoiced AS `isVatInvoiced`,
    pfm.pfm_amountwithvat AS `amountWithVat`,
    pfm.pfm_paidamount AS `paidAmount`,
    pfm.pfm_deposit AS `deposit`,
    pfm.pfm_pmt_id AS `paymentMode.id`,
    tbl_f_modepaie.MOPALIBELLE AS `paymentMode.label`,
    pfm.pfm_date_end AS `endDate`,
    pfm.pfm_date_begin AS `beginDate`,
    pfm.pfm_fuelqty AS `fuelQuantity`,
    pfm.pfm_fuelunitprice AS `fuelUnitPrice`,
    pfm.pfm_comment AS `comment`,
    pfm.pfm_is_justified AS `isJustified`,
    pfm.pfm_is_validated AS `isValidated`,
    pfm.pfm_validcomment AS `validComment`,
    pfm.pfm_sta_id_encashment AS `encashment.id`,
    tbl_p_state_encashment.ETLIBELLE AS `encashment.label`,
    pfm.pfm_sta_id_closure AS `closure.id`,
    tbl_p_state_closure.ETLIBELLE AS `closure.label`,
    pfm.pfm_remaindue AS `remainDue`,
    pfm.pfm_percent AS `percent`,
    pfm.pfm_agc_id AS `agency.id`,
    pfm.pfm_usr_id AS `user.id`,
    pfm.pfm_is_expired AS `isExpired`,
    pfm.pfm_date_create AS `creationDate`,
    pfm.pfm_date_modif AS `modificationDate`,
    pfm.pfm_sta_id AS `state.id`,
    tbl_p_state.ETLIBELLE AS `state.label`';
        if ($flags & GETINFOS_CUSTOMER)
        {
            $query .= ',
    tbl_f_customer.CLCODE AS `customer.code`,
    tbl_f_customer.CLSTE AS `customer.name`,
    -tbl_f_customer.CLPROFORMA AS `customer.isProformaRequired`,
    IF (ABS(tbl_f_customer.CLVERIFIER) = 1, 0, 1) AS `customer.isVerified`,
    IF (tbl_f_customer.CLVERIFIER = 1, "estimate", IF (tbl_f_customer.CLVERIFIER = -1, "contract", "")) AS `customer.prospectSource`,
    tbl_f_customer.CLSIREN AS `customer.siret`,
    tbl_f_customer.CLEMAIL AS `customer.email`,
    tbl_f_customer.CLCOLLECTIF AS `customer.accountingCode`,
    tbl_f_customer.CLCEE AS `customer.accountingCategory`,
    -tbl_f_customer.CLBONSOBLIG AS `deliveryRecoveryNotesRequired`';
        }
        if ($flags & GETINFOS_CREATOR)
        {
            $query .= ',
    f_user_creator.usr_firstname AS `creator.firstName`,
    f_user_creator.usr_name AS `creator.name`,
    CONCAT_WS(" ", f_user_creator.usr_firstname, f_user_creator.usr_name) AS `creator.fullName`';
        }
        if ($flags & GETINFOS_CONTRACT)
        {
            $query .= ',
    tbl_f_contract.CONTRATCODE AS `contract.code`,
    tbl_f_contract.ETCODE AS `contract.state.id`,
    tbl_f_contract.AGAUTO AS `contract.agency.id`,
    tbl_f_contract.CONTRATNUMCOMM AS `contract.orderNo`,
    tbl_f_contract.CONTRATDATEMODIF AS `contract.modificationDate`,
    IF (tbl_f_contractamount.MOREMISEEX > 0,
        ELT(1-tbl_f_contractamount.MOREMISEOK, "' . LOC::Tariff::Rent::REDUCTIONSTATE_REFUSED . '", "' . LOC::Tariff::Rent::REDUCTIONSTATE_PENDING . '", "' . LOC::Tariff::Rent::REDUCTIONSTATE_VALIDATED . '"),
        IF (tbl_f_rentestim2.VALIDOK = 0, "' . LOC::Tariff::Rent::REDUCTIONSTATE_REFUSED . '", "")) AS `contract.rentTariff.specialReductionStatus`,
    IFNULL(tbl_f_trspspcreduc2.ETAT, "") AS `contract.transportTariff.specialReductionStatus`,
    tbl_f_contract.CONTRATCONTACTCOMM AS `contract.orderContact.fullName`,
    tbl_f_contract.CONTRATTELEPHONECOMM AS `contract.orderContact.telephone`,
    tbl_f_contract.CONTRATEMAILCOMM AS `contract.orderContact.email`,
    CONCAT_WS(" ", tbl_f_site1.CHLIBELLE, tbl_p_sitelocality1.VICP, tbl_p_sitelocality1.VINOM) AS `contract.site`,
    tbl_f_contract.PEAUTO_INTERLOC AS `contract.negotiator.id`';
        }
        if ($flags & GETINFOS_ESTIMATELINE)
        {
            $query .= ',
    IF (pfm.pfm_rel_id IS NULL, NULL, (SELECT COUNT(tbl_f_estimateline2.DETAILSDEVISAUTO) + 1
                                   FROM DETAILSDEVIS tbl_f_estimateline2
                                   WHERE tbl_f_estimateline2.DEVISAUTO = tbl_f_estimateline.DEVISAUTO
                                   AND tbl_f_estimateline2.DETAILSDEVISAUTO < tbl_f_estimateline.DETAILSDEVISAUTO)) AS `estimateLine.index`,
    tbl_f_estimate.DEVISAUTO AS `estimateLine.estimate.id`,
    ' . &LOC::Estimate::Rent::generateCode('tbl_f_estimate.DEVISAUTO', 'tbl_f_estimate.AGAUTO',
                     '-tbl_f_estimate.DEVISFULLSERVICE', 1) . ' AS `estimateLine.estimate.code`,
    tbl_f_estimate.AGAUTO AS `estimateLine.estimate.agency.id`,
    tbl_f_estimate.DEVISNUMCOM AS `estimateLine.estimate.orderNo`,
    tbl_f_estimate.ETCODE AS `estimateLine.estimate.state.id`,
    tbl_f_estimateline.DETAILSDEVISDATEMODIF AS `estimateLine.modificationDate`,
    IF (tbl_f_estimateline.DETAILSDEVISREMISEEX > 0,
        ELT(1-tbl_f_estimateline.DETAILSDEVISREMISEOK, "' . LOC::Tariff::Rent::REDUCTIONSTATE_REFUSED . '", "' . LOC::Tariff::Rent::REDUCTIONSTATE_PENDING . '", "' . LOC::Tariff::Rent::REDUCTIONSTATE_VALIDATED . '"),
        IF (tbl_f_rentestim.VALIDOK = 0, "' . LOC::Tariff::Rent::REDUCTIONSTATE_REFUSED . '", "")) AS `estimateline.rentTariff.specialReductionStatus`,
    IFNULL(tbl_f_trspspcreduc.ETAT, "") AS `estimateline.transportTariff.specialReductionStatus`,
    tbl_f_estimate.DEVISCONTACTCOM AS `estimateLine.estimate.orderContact.fullName`,
    tbl_f_estimate.DEVISTELEPHONECOM AS `estimateLine.estimate.orderContact.telephone`,
    tbl_f_estimate.DEVISEMAILCOM AS `estimateLine.estimate.orderContact.email`,
    CONCAT_WS(" ", tbl_f_site.CHLIBELLE, tbl_p_sitelocality.VICP, tbl_p_sitelocality.VINOM) AS `estimateLine.estimate.site`,
    tbl_f_estimate.PEAUTO_INTERLOC AS `estimateLine.estimate.negotiator.id`';
        }
        if ($flags & GETINFOS_INDEX)
        {
            $query .= ',
    IF (pfm.pfm_rct_id IS NULL, NULL, (SELECT COUNT(*) + 1
                                   FROM f_proforma f_proforma2
                                   WHERE f_proforma2.pfm_rct_id = pfm.pfm_rct_id
                                   AND f_proforma2.pfm_date_create < pfm.pfm_date_create)) AS `indexOnContract`,
    IF (pfm.pfm_rel_id IS NULL, NULL, (SELECT COUNT(*) + 1
                                   FROM f_proforma f_proforma2
                                   WHERE f_proforma2.pfm_rel_id = pfm.pfm_rel_id
                                   AND f_proforma2.pfm_date_create < pfm.pfm_date_create)) AS `indexOnEstimateLine`';
        }
        if ($flags & GETINFOS_LINK)
        {
            $query .= ',
    pftLink.pft_id AS `link.type.id`,
    pftLink.pft_label AS `link.type.label`,
    pfmLink.pfm_date_create AS `link.creationDate`';
        }
        if ($flags & GETINFOS_LINKEXTENSION)
        {
            $query .= ',
    pftExtension.pft_id AS `extension.type.id`,
    pftExtension.pft_label AS `extension.type.label`,
    pfmExtension.pfm_date_create AS `extension.creationDate`';
        }
        $query .= '
FROM f_proforma pfm
INNER JOIN p_proformatype pft
ON pft.pft_id = pfm.pfm_pft_id
LEFT JOIN MODEPAIE tbl_f_modepaie
ON MOPAAUTO = pfm.pfm_pmt_id
INNER JOIN ETATTABLE tbl_p_state
ON tbl_p_state.ETCODE = pfm.pfm_sta_id
INNER JOIN ETATTABLE tbl_p_state_encashment
ON tbl_p_state_encashment.ETCODE = pfm.pfm_sta_id_encashment
LEFT JOIN ETATTABLE tbl_p_state_closure
ON tbl_p_state_closure.ETCODE = pfm.pfm_sta_id_closure';
        if ($flags & GETINFOS_CUSTOMER)
        {
            $query .= '
LEFT JOIN CLIENT tbl_f_customer
ON tbl_f_customer.CLAUTO = pfm.pfm_cus_id';
        }
        if ($flags & GETINFOS_CREATOR)
        {
            $query .= '
LEFT JOIN frmwrk.f_user f_user_creator
ON f_user_creator.usr_id = pfm.pfm_usr_id';
        }
        if ($flags & GETINFOS_CONTRACT)
        {
            $query .= '
LEFT JOIN CONTRAT tbl_f_contract
ON tbl_f_contract.CONTRATAUTO = pfm.pfm_rct_id
LEFT JOIN MONTANT tbl_f_contractamount
ON tbl_f_contract.MOAUTO = tbl_f_contractamount.MOAUTO
LEFT JOIN ' . $tariffRentSchemaName . '.GC_ARCHIVES tbl_f_rentestim2
ON tbl_f_rentestim2.CONTRATAUTO = pfm.pfm_rct_id
LEFT JOIN ' . $tariffTrspSchemaName . '.TRANSPORT tbl_f_trspestim2
ON tbl_f_trspestim2.TYPE = "CONTRAT" AND
   tbl_f_trspestim2.VALEUR = pfm.pfm_rct_id AND
   tbl_f_trspestim2.ETAT <> "' . LOC::Tariff::Transport::REDUCTIONSTATE_CANCELED . '"
LEFT JOIN ' . $tariffTrspSchemaName . '.REMISEEX tbl_f_trspspcreduc2
ON tbl_f_trspspcreduc2.ID = tbl_f_trspestim2.REMISEEX
LEFT JOIN CHANTIER tbl_f_site1
ON tbl_f_site1.CHAUTO = tbl_f_contract.CHAUTO
LEFT JOIN VILLECHA tbl_p_sitelocality1
ON tbl_p_sitelocality1.VILLEAUTO = tbl_f_site1.VILLEAUTO';
        }
        if ($flags & GETINFOS_ESTIMATELINE)
        {
            $query .= '
LEFT JOIN DETAILSDEVIS tbl_f_estimateline
ON tbl_f_estimateline.DETAILSDEVISAUTO = pfm_rel_id
LEFT JOIN DEVIS tbl_f_estimate
ON tbl_f_estimate.DEVISAUTO = tbl_f_estimateline.DEVISAUTO
LEFT JOIN ' . $tariffRentSchemaName . '.GC_ARCHIVES tbl_f_rentestim
ON tbl_f_rentestim.DETAILSDEVISAUTO = tbl_f_estimateline.DETAILSDEVISAUTO
LEFT JOIN ' . $tariffTrspSchemaName . '.TRANSPORT tbl_f_trspestim
ON tbl_f_trspestim.TYPE = "DEVIS" AND
   tbl_f_trspestim.VALEUR = tbl_f_estimateline.DETAILSDEVISAUTO AND
   tbl_f_trspestim.ETAT <> "' . LOC::Tariff::Transport::REDUCTIONSTATE_CANCELED . '"
LEFT JOIN ' . $tariffTrspSchemaName . '.REMISEEX tbl_f_trspspcreduc
ON tbl_f_trspspcreduc.ID = tbl_f_trspestim.REMISEEX
LEFT JOIN CHANTIER tbl_f_site
ON tbl_f_site.CHAUTO = tbl_f_estimate.CHAUTO
LEFT JOIN VILLECHA tbl_p_sitelocality
ON tbl_p_sitelocality.VILLEAUTO = tbl_f_site.VILLEAUTO';
        }
        if ($flags & GETINFOS_LINK)
        {
            $query .= '
LEFT JOIN f_proforma pfmLink
ON pfmLink.pfm_id = pfm.pfm_id_link
LEFT JOIN p_proformatype pftLink
ON pftLink.pft_id = pfmLink.pfm_pft_id';
        }
        if ($flags & GETINFOS_LINKEXTENSION)
        {
            $query .= '
LEFT JOIN f_proforma pfmExtension
ON pfmExtension.pfm_id = pfm.pfm_id_extension
LEFT JOIN p_proformatype pftExtension
ON pftExtension.pft_id = pfmExtension.pfm_pft_id';
        }
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*)
FROM f_proforma pfm';
    }
    else
    {
        $query = '
SELECT
    pfm.pfm_id,
    pfm.pfm_date_create
FROM f_proforma pfm';
    }


    $query .= '
WHERE pfm.pfm_sta_id IN (' . $db->quote($tabFilters->{'stateId'}) . ')';

    # Filtre
    if (defined $tabFilters->{'isExpired'})
    {
        $query .= '
AND pfm.pfm_is_expired = ' . ($tabFilters->{'isExpired'} ? 1 : 0);
    }
    if (defined $tabFilters->{'id'})
    {
        $query .= '
AND pfm.pfm_id IN (' . &LOC::Util::join(', ', $tabFilters->{'id'}) . ')';
    }
    if (defined $tabFilters->{'typeId'})
    {
        $query .= '
AND pfm.pfm_pft_id IN (' . &LOC::Util::join(', ', $tabFilters->{'typeId'}) . ')';
    }
    if (exists $tabFilters->{'contractId'})
    {
        if (defined $tabFilters->{'contractId'})
        {
            $query .= '
AND pfm.pfm_rct_id IN (' . &LOC::Util::join(', ', $tabFilters->{'contractId'}) . ')';
        }
        else
        {
            $query .= '
AND pfm.pfm_rct_id IS NULL';
        }
    }
    if (exists $tabFilters->{'estimateLineId'})
    {
        if (defined $tabFilters->{'estimateLineId'})
        {
            $query .= '
AND pfm.pfm_rel_id IN (' . &LOC::Util::join(', ', $tabFilters->{'estimateLineId'}) . ') ';
        }
        else
        {
            $query .= '
AND pfm.pfm_rel_id IS NULL';
        }
    }
    if (defined $tabFilters->{'estimateId'})
    {
        $query .= '
AND pfm.pfm_rel_id IN (SELECT tbl_f_estimateline3.DETAILSDEVISAUTO
                   FROM DETAILSDEVIS tbl_f_estimateline3
                   WHERE tbl_f_estimateline3.DEVISAUTO IN (' . &LOC::Util::join(', ', $tabFilters->{'estimateId'}) . '))';
    }

    if (defined $tabFilters->{'documentStateId'})
    {
        $query .= '
AND (tbl_f_contract.ETCODE IN (' . $db->quote($tabFilters->{'documentStateId'}) . ')
     OR tbl_f_estimate.ETCODE IN (' . $db->quote($tabFilters->{'documentStateId'}) . '))';
    }

    if (defined $tabFilters->{'isJustified'})
    {
        $query .= '
AND pfm.pfm_is_justified IN (' . $tabFilters->{'isJustified'} . ')';
    }

    if (defined $tabFilters->{'isValidated'})
    {
        $query .= '
AND pfm.pfm_is_validated IN (' . $tabFilters->{'isValidated'} . ')';
    }
    if (defined $tabFilters->{'encashmentId'})
    {
        $query .= '
AND pfm.pfm_sta_id_encashment IN (' . $db->quote($tabFilters->{'encashmentId'}) . ')';
    }
    if (exists $tabFilters->{'closureId'})
    {
        if (defined $tabFilters->{'closureId'})
        {
            $query .= '
AND pfm.pfm_sta_id_closure IN (' . $db->quote($tabFilters->{'closureId'}) . ')';
        }
        else
        {
            $query .= '
AND pfm.pfm_sta_id_closure IS NULL';
        }
    }
    if (defined $tabFilters->{'isFinalized'})
    {
        my $op = ($tabFilters->{'isFinalized'} ? '' : '!');
        $query .= '
AND ' . $op . '(pfm.pfm_is_justified = 1 OR pfm.pfm_is_validated = 1 OR pfm.pfm_sta_id_encashment = ' . $db->quote(ENCASHMENTSTATE_RECEIVED) . ')';
    }

    if (defined $tabFilters->{'noReceived'})
    {
        $query .= '
AND pfm.pfm_sta_id_encashment != ' . $db->quote(ENCASHMENTSTATE_RECEIVED);
    }

    if (defined $tabFilters->{'customerId'})
    {
        $query .= '
AND pfm.pfm_cus_id IN (' . &LOC::Util::join(', ', $tabFilters->{'customerId'}) . ')';
    }

    if (defined $tabFilters->{'userId'})
    {
        $query .= '
AND pfm.pfm_usr_id IN (' . &LOC::Util::join(', ', $tabFilters->{'userId'}) . ')';
    }
    $query .= '
ORDER BY ' . $tabOptions->{'order'};
    if (defined $tabOptions->{'limit'})
    {
        $query .= '
LIMIT ' . $tabOptions->{'limit'};
    }
    $query .= ';';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return 0;
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        # Doit-on compter le dépôt de garantie dans le montant total de la pro forma ?
        my $isDepositInTotal = &LOC::Characteristic::getCountryValueByCode('PFDEPOSITTOTAL', $countryId) * 1;

        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'} *= 1;
            $tabRow->{'type.id'} *= 1;
            $tabRow->{'estimateLine.id'} = (defined $tabRow->{'estimateLine.id'} ? $tabRow->{'estimateLine.id'} * 1 : undef);
            $tabRow->{'contract.id'} = (defined $tabRow->{'contract.id'} ? $tabRow->{'contract.id'} * 1 : undef);
            $tabRow->{'customer.id'} *= 1;
            $tabRow->{'amount'} *= 1;
            $tabRow->{'comment'} .= '';
            $tabRow->{'isVatInvoiced'} *= 1;
            $tabRow->{'paidAmount'} *= 1;
            $tabRow->{'deposit'} *= 1;
            $tabRow->{'paymentMode.id'} *= 1;
            $tabRow->{'fuelQuantity'} *= 1;
            $tabRow->{'isJustified'} *= 1;
            $tabRow->{'isValidated'} *= 1;
            $tabRow->{'validComment'} .= '';
            $tabRow->{'isExpired'} *= 1;
            $tabRow->{'isFinalized'} = ($tabRow->{'encashment.id'} eq ENCASHMENTSTATE_RECEIVED || $tabRow->{'isJustified'} || $tabRow->{'isValidated'}) * 1;

            # Possibilité de modifier ?
            $tabRow->{'isEditable'} = 0;
            if (!$tabRow->{'isExpired'} && $tabRow->{'state.id'} eq STATE_ACTIVE &&
                !$tabRow->{'isJustified'} && !$tabRow->{'isValidated'} &&
                $tabRow->{'encashment.id'} eq ENCASHMENTSTATE_WAITING)
            {
                $tabRow->{'isEditable'} = 1;
            }

            # Possibilité de valider (justifier, valider par agence, encaisser) ?
            $tabRow->{'isValidable'} = (!$tabRow->{'isExpired'} && $tabRow->{'state.id'} eq STATE_ACTIVE ? 1 : 0);
            # Possibilité d'abandonner ?
            $tabRow->{'isAbortable'} = 0;
            if ($tabRow->{'state.id'} eq STATE_ACTIVE && !$tabRow->{'isJustified'} &&
                !$tabRow->{'isValidated'} && $tabRow->{'encashment.id'} eq ENCASHMENTSTATE_WAITING)
            {
                $tabRow->{'isAbortable'} = 1;
            }

            # Possibilité de réactiver ?
            $tabRow->{'isReactivable'} = (!$tabRow->{'isExpired'} && $tabRow->{'state.id'} eq STATE_ABORTED && $tabRow->{'encashment.id'} eq ENCASHMENTSTATE_WAITING ? 1 : 0);
            # Possibilité d'imprimer ?
            $tabRow->{'isPrintable'} = ((!$tabRow->{'isExpired'} && $tabRow->{'state.id'} eq STATE_ACTIVE ? 1 : 0) ||
                                         $tabRow->{'isFinalized'} ||
                                        ($tabRow->{'encashment.id'} eq ENCASHMENTSTATE_NOTRECEIVED && defined $tabRow->{'closure.id'}));
            # Possibilité d'expirer ?
            $tabRow->{'isExpirable'} = (!$tabRow->{'isExpired'} && !($tabRow->{'type.id'} == LOC::Proforma::Type::ID_ADVANCE && $tabRow->{'isFinalized'}));

            if ($flags & GETINFOS_LINES)
            {
                $tabRow->{'tabLines'} = &_getLines($countryId, $tabRow->{'id'});
            }

            # Calcul du montant total
            $tabRow->{'total'} = $tabRow->{'amountWithVat'} - $tabRow->{'paidAmount'};
            if ($isDepositInTotal)
            {
                $tabRow->{'total'} += $tabRow->{'deposit'};
            }
            $tabRow->{'total'} = LOC::Util::round($tabRow->{'total'}, 2);

            # Reste dû
            if ($tabRow->{'encashment.id'} eq ENCASHMENTSTATE_RECEIVED)
            {
                $tabRow->{'remainDue'} = 0;
            }
            elsif ($tabRow->{'encashment.id'} eq ENCASHMENTSTATE_PARTIAL)
            {
                $tabRow->{'remainDue'} *= 1;
            }
            else
            {
                $tabRow->{'remainDue'} = $tabRow->{'total'};
            }

            # Encaissé
            $tabRow->{'cashed'} = LOC::Util::round($tabRow->{'total'} - $tabRow->{'remainDue'}, 2);
        }
    }

    return (wantarray ? %$tab : $tab);
}


# Function: getList
# Récupérer la liste des devis
#
# Contenu d'une ligne du hashage :
# - id => Identifiant
# - label  => Libellé
#
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres ()
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hash|hashref -
sub getPaymentModesList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;

    if (!defined $tabFilters)
    {
        $tabFilters = {};
    }
    $tabFilters->{'isForProforma'} = 1;

    return &LOC::PaymentMode::getList($countryId, $format, $tabFilters, $flags, $tabOptions);
}


# Function: isUpToDate
# Est-ce que la pro forma est à jour par rapport aux données du document ?
#
# Parameters:
# string   $countryId       - Pays
# int      $proformaId      - Id de la proforma
# string   $documentType    - Type de document
# int      $documentId      - Id du document
# hashref  $tabCompareInfos - Retour: Données de comparaison (ancienne et vieille données)
#
# Return:
# bool/hashref
sub isUpToDate
{
    my ($countryId, $proformaId, $documentType, $documentId, $tabCompareInfos) = @_;

    my $tabOldInfos = &getInfos($countryId, $proformaId, GETINFOS_LINES);
    my $tabNewInfos = &getInfosByDocument($countryId, $documentType, $documentId, $tabOldInfos);

    # Renvoi les données de comparaison si elles sont demandées
    if (ref($tabCompareInfos) eq 'HASH')
    {
        $tabCompareInfos->{'old'} = $tabOldInfos;
        $tabCompareInfos->{'new'} = $tabNewInfos;
    }

    # Les montants totaux doivent être identique
    if ($tabOldInfos->{'amount'} != $tabNewInfos->{'amount'})
    {
        return 0;
    }

    # on compare chaque ligne de pro forma : avant/après une nouvelle génération
    my @tabOldLines = sort {$b->{'amount'} <=> $a->{'amount'}} @{$tabOldInfos->{'tabLines'}};
    my @tabNewLines = sort {$b->{'amount'} <=> $a->{'amount'}} @{$tabNewInfos->{'tabLines'}};

    # L'ancienne pro forma et la nouvelle doivent forcément avoir le même nombre de ligne
    my $oldLinesCount = @tabOldLines;
    my $newLinesCount = @tabNewLines;
    if ($oldLinesCount != $newLinesCount)
    {
        return 0;
    }

    # Ces lignes sont en théorie placées dans le même ordre
    # Pour chaque ligne on compare les montants
    for (my $i = 0; $i < $oldLinesCount; $i++)
    {
        if ($tabOldInfos->{'type.id'} != LOC::Proforma::Type::ID_ADVANCE && $tabOldLines[$i]->{'articleRef'} ne $tabNewLines[$i]->{'articleRef'})
        {
            return 0;
        }
        if ($tabOldLines[$i]->{'amount'} != $tabNewLines[$i]->{'amount'})
        {
            return 0;
        }
    }

    return 1;
}

# Function: reactivate
# Réactiver une proforma
#
# Parameters:
# string   $countryId  - Pays
# int      $proformaId - Id de la proforma à réactiver
# int      $userId     - Id de l'utilisateur responsable
# hashref  $tabErrors  - Tableau des erreurs
# hashref  $tabEndUpds - Mises à jour de fin
#
# Return:
# bool
sub reactivate
{
    my ($countryId, $proformaId, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $tabInfos = &getInfos($countryId, $proformaId);

    # Possibilité d'abandonner la proforma
    if (!$tabInfos->{'isReactivable'})
    {
        push(@{$tabErrors->{'fatal'}}, 0x0015);

        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    if ($db->update('f_proforma', {'pfm_sta_id' => STATE_ACTIVE}, {'pfm_id' => $proformaId}, $schemaName) == -1)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);

        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # mise à jour du flag proformaOK sur ligne de devis et contrat
    my $documentType = (defined $tabInfos->{'contract.id'} ? 'contract' : 'estimateLine');
    my $documentId   = (defined $tabInfos->{'contract.id'} ? $tabInfos->{'contract.id'} : $tabInfos->{'estimateLine.id'});

    # Mise à jour du document impacté
    if (!&_updateDocument($db, $countryId, $documentType, $documentId, $userId, $tabEndUpds))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);

        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Mise à jour des alertes Règlements proforma (commerce et comptabilité)
    if ($tabInfos->{'encashment.id'} ne ENCASHMENTSTATE_RECEIVED)
    {
        &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::PROFORMA_ALL, +1);
        &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::PROFORMA_AGENCY, +1);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: add
# Ajout d'une pro forma
#
# Parameters:
# string   $countryId    - Pays
# string   $documentType - Type de document (contrat/ligne devis)
# int      $documentId   - Id du document
# hashref  $tabData      - Données de la proforma
# int      $userId       - Utilisateur responsable de la création
# hashref  $tabErrors    - Tableau des erreurs
# hashref  $tabEndUpds   - Mises à jour de fin
#
# Returns:
# bool -
sub add
{
    my ($countryId, $documentType, $documentId, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    my $tabInfos = &_getAction($countryId, $documentType, $documentId, $tabData, $tabErrors);
    if (!$tabInfos)
    {
        return 0;
    }
    my $hasErrors = 0;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $proformaId = 0;
    if (defined $tabInfos->{'link.id'})
    {
        # Appel de l'update sur la proforma à mettre à jour
        if ($db->update('f_proforma', {'pfm_sta_id' => STATE_REPLACED}, {'pfm_id' => $tabInfos->{'link.id'}}, $schemaName) != -1)
        {
            if (ref($tabInfos->{'link.id'}) eq 'ARRAY')
            {
                my $countLinkId = @{$tabInfos->{'link.id'}};
                $tabInfos->{'link.id'} = $tabInfos->{'link.id'}->[$countLinkId - 1];
            }

            # Insertion de la nouvelle pro forma
            my $oldProforma = &getInfos($countryId, $tabInfos->{'link.id'});
            if ($oldProforma->{'isExpired'} || $oldProforma->{'closure.id'} || $oldProforma->{'encashment.id'} eq ENCASHMENTSTATE_RECEIVED)
            {
                # Mise à jour des alertes Règlements proforma (commerce et comptabilité)
                &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::PROFORMA_ALL, +1);
                &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::PROFORMA_AGENCY, +1);
            }
        }
    }
    else
    {
        # Mise à jour des alertes Règlements proforma (commerce et comptabilité)
        &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::PROFORMA_ALL, +1);
        &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::PROFORMA_AGENCY, +1);
    }

    # Avant d'insérer on met en non à jour les pro formas existantes
    my $wheres = {};

    # Contrat ou devis ?
    if ($documentType eq 'contract')
    {
        $wheres->{'pfm_rct_id'}     = $documentId;
        if ($tabInfos->{'type.id'} == LOC::Proforma::Type::ID_EXTENSION)
        {
            $wheres->{'pfm_is_justified'} = 0;
            $wheres->{'pfm_is_validated'} = 0;
            $wheres->{'pfm_sta_id_encashment'} = [ENCASHMENTSTATE_WAITING, ENCASHMENTSTATE_NOTRECEIVED, ENCASHMENTSTATE_PARTIAL];
        }
    }
    elsif ($documentType eq 'estimateLine')
    {
        $wheres->{'pfm_rel_id'}       = $documentId;
    }
    # - Passage au flag non à jour de toutes les proformas existantes pour ce document
    if ($db->update('f_proforma', {'pfm_is_expired' => 1}, $wheres, $schemaName) == -1)
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, 0x0001);
    }

    # Récupération de l'id de la dernière pro forma active non expirée pour le lien de la prolongation
    if ($tabInfos->{'type.id'} == LOC::Proforma::Type::ID_EXTENSION)
    {
        my $tabFilters = {
            'stateId'     => [STATE_ACTIVE],
            'isExpired'   => 0,
            'isFinalized' => 1,
            'contractId'  => $documentId
        };

        my $tabOptions = {
            'order' => 'pfm_id DESC',
            'limit' => 1
        };
        my $pfToExtend = &getList($countryId, LOC::Util::GETLIST_IDS, $tabFilters, undef, $tabOptions);
        $tabInfos->{'extension.id'} = $pfToExtend->[0];
    }


    # Insertion
    if ($hasErrors == 0)
    {
        $proformaId = &_insert($db, $countryId, $documentType, $documentId, $tabInfos, $userId);
    }

    if (!$proformaId)
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, 0x0001);
    }

    if ($hasErrors == 0)
    {
        # Mise à jour du document impacté
        if (!&_updateDocument($db, $countryId, $documentType, $documentId, $userId, $tabEndUpds))
        {
            $hasErrors++;
            push(@{$tabErrors->{'fatal'}}, 0x0001);
        }
    }

    if ($hasErrors > 0)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return $proformaId;
}


#Function : edit
# Modification de la proforma
#
# Parameters :
# string   $countryId   - Pays
# int      $proformaId  - Identifiant de la proforma a metre à jour
# hashref  $tabData     - Données de la proforma
# int      $userId      - Id de l'utilisateur responsable de la modification
# hashref  $tabErrors   - Tableau des erreurs
# hashref  $tabEndUpds  - Mises à jour de fin
#
# Returns:
# bool -
sub edit
{
    my ($countryId, $proformaId, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    #Récupération de la proforma existante
    my $tabInfos = &getInfos($countryId, $proformaId, GETINFOS_ALL);
    my $documentType = (defined $tabInfos->{'contract.id'} ? 'contract' : 'estimateLine');
    my $documentId   = (defined $tabInfos->{'contract.id'} ? $tabInfos->{'contract.id'} : $tabInfos->{'estimateLine.id'});
    if (!$tabInfos)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Si non modifiable
    if (!$tabInfos->{'isEditable'})
    {
        push(@{$tabErrors->{'fatal'}}, 0x0012);
        return 0;
    }

    # Paramètres
    my $oldIsVatInvoiced = $tabInfos->{'isVatInvoiced'};
    my $isVatInvoiced    = (exists $tabData->{'isVatInvoiced'} ? ($tabData->{'isVatInvoiced'} ? 1 : 0) : $oldIsVatInvoiced);
    my $oldPaidAmount    = $tabInfos->{'paidAmount'};
    my $paidAmount       = (exists $tabData->{'paidAmount'} ? &LOC::Util::round($tabData->{'paidAmount'}, 2) : $oldPaidAmount);
    my $oldDeposit       = $tabInfos->{'deposit'};
    my $deposit          = (exists $tabData->{'deposit'} ? &LOC::Util::round($tabData->{'deposit'}, 2) : $oldDeposit);
    my $oldPmtId         = $tabInfos->{'paymentMode.id'};
    my $pmtId            = (exists $tabData->{'paymentMode.id'} ? $tabData->{'paymentMode.id'} * 1 : $oldPmtId);
    my $oldFuelQty       = $tabInfos->{'fuelQuantity'};
    my $fuelQty          = (exists $tabData->{'fuelQuantity'} ? $tabData->{'fuelQuantity'} * 1 : $oldFuelQty);
    my $fuelUnitPrice    = $tabInfos->{'fuelUnitPrice'};
    my $oldComment       = $tabInfos->{'comment'};
    my $comment          = (exists $tabData->{'comment'} ? &LOC::Util::trim($tabData->{'comment'}) : $oldComment);
    my $oldAmount        = $tabInfos->{'amount'};
    my $amount           = $oldAmount;

    # Mise en forme des données de proforma
    if ($tabInfos->{'type.id'} == LOC::Proforma::Type::ID_ADVANCE)
    {
        $paidAmount = 0;
        $deposit    = 0;
        $fuelQty    = 0;
        $amount     = (exists $tabData->{'amount'} ? $tabData->{'amount'} : $oldAmount);
    }



    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $tabLineErrors = {};

    my $tabUpdates = {
        'pfm_is_vatinvoiced' => $isVatInvoiced,
        'pfm_paidamount'     => $paidAmount,
        'pfm_deposit'        => $deposit,
        'pfm_pmt_id'         => $pmtId,
        'pfm_comment'        => ($comment ne '' ? $comment : undef),
        'pfm_date_modif*'    => 'Now()'
    };

    # Si on modifie l'estimation de carburant on met à jour la ligne "Estimation carburant"
    # Si on modifie le montant de la facture d'acompte, on met à jour le pourcentage et la ligne de la facture
    if ($fuelQty != $oldFuelQty || $amount != $oldAmount || $oldPaidAmount != $paidAmount)
    {
        my $tabToUpdate = [];
        my %tabNewInfos = %$tabInfos;
        # Mise à jour de l'estimation carburant ?
        if ($fuelQty != $oldFuelQty)
        {
            $tabNewInfos{'fuelQuantity'} = $fuelQty;
            push(@$tabToUpdate, 'fuelEstimation');
        }
        # Mise à jour du montant de l'acompte ?
        if ($amount != $oldAmount)
        {
            $tabNewInfos{'amount'} = $amount;
            push(@$tabToUpdate, 'advance');
        }
        # Prise en compte de la modif de déjà versé
        if ($paidAmount != $oldPaidAmount)
        {
            push(@$tabToUpdate, 'paidAmount');
        }
        if (!&_updateGroupLines($db, $countryId, $documentType, $documentId, \%tabNewInfos, $tabToUpdate, $tabUpdates))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);

            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }

        # On vérifie que le montant n'est pas nul sauf pour la facture pro forma avec déjà versé
        my $query = '
SELECT
    ROUND(SUM(pfl_unitprice * pfl_quantity), 2)
FROM f_proformaline
WHERE pfl_pfm_id = ' . $proformaId . ';';
        my $amount = $db->fetchOne($query);
        if ($amount == 0 && $paidAmount == 0)
        {
            push(@{$tabErrors->{'fatal'}}, 0x0017);

            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }
    }


    # Mise à jour des montants totaux
    $tabUpdates->{'pfm_amount*'} = '(SELECT IFNULL(ROUND(SUM(pfl_unitprice * pfl_quantity), 2), 0) FROM f_proformaline WHERE pfl_pfm_id = ' . $proformaId . ')';
    if ($isVatInvoiced)
    {
        $tabUpdates->{'pfm_amountwithvat*'} = '(SELECT IFNULL(ROUND(SUM(pfl_unitprice * pfl_quantity * (1 + pfl_vatrate / 100)), 2), 0) FROM f_proformaline WHERE pfl_pfm_id = ' . $proformaId . ')';
    }
    else
    {
        $tabUpdates->{'pfm_amountwithvat*'} = $tabUpdates->{'pfm_amount*'};
    }

    # Enregistrement dans la table f_proforma
    if ($db->update('f_proforma', $tabUpdates, {'pfm_id' => $proformaId}, $schemaName) == -1)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);

        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Mise à jour du document impacté
    if (!&_updateDocument($db, $countryId, $documentType, $documentId, $userId, $tabEndUpds))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


#Function : validate
# Validation de la proforma (justification, validation par agence, encaissement)
#
# Parameters :
# string   $countryId   - Pays
# int      $proformaId  - Identifiant de la proforma a metre à jour
# hashref  $tabData     - Données de la proforma
# int      $userId      - Id de l'utilisateur responsable de la validation
# hashref  $tabErrors   - Tableau des erreurs
# hashref  $tabEndUpds  - Mises à jour de fin
#
# Returns:
# bool -
sub validate
{
    my ($countryId, $proformaId, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }
    $tabErrors->{'modules'} = { 'contract' => {} };

    #Récupération de la proforma existante
    my $tabInfos = &getInfos($countryId, $proformaId, GETINFOS_ALL);
    my $documentType = (defined $tabInfos->{'contract.id'} ? 'contract' : 'estimateLine');
    my $documentId   = (defined $tabInfos->{'contract.id'} ? $tabInfos->{'contract.id'} : $tabInfos->{'estimateLine.id'});

    if (!$tabInfos)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Si pas validable
    if (!$tabInfos->{'isValidable'})
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # si la pro forma a été modifiée entre l'ouverture de l'alerte et la validation
    # Vérification de la date de modification (accès concurentiel)
    if (exists $tabData->{'modificationDate'} && $tabData->{'modificationDate'} ne $tabInfos->{'modificationDate'})
    {
        push(@{$tabErrors->{'fatal'}}, 0x0022);
        return 0;
    }

    # Paramètres
    my $oldIsJustified  = $tabInfos->{'isJustified'};
    my $isJustified     = (exists $tabData->{'isJustified'} ? ($tabData->{'isJustified'} ? 1 : 0) : $oldIsJustified);
    my $oldIsValidated  = $tabInfos->{'isValidated'};
    my $isValidated     = (exists $tabData->{'isValidated'} ? ($tabData->{'isValidated'} ? 1 : 0) : $oldIsValidated);
    my $oldValidComment = $tabInfos->{'validComment'};
    my $validComment    = (exists $tabData->{'validComment'} ? &LOC::Util::trim($tabData->{'validComment'}) : $oldValidComment);
    my $oldEncashmentId = $tabInfos->{'encashment.id'};
    my $encashmentId    = (exists $tabData->{'encashment.id'} ? $tabData->{'encashment.id'} : $oldEncashmentId);
    my $oldClosureId    = $tabInfos->{'closure.id'};
    my $closureId       = (exists $tabData->{'closure.id'} ? ($tabData->{'closure.id'} eq '' ? undef : $tabData->{'closure.id'}) : $oldClosureId);
    my $oldRemainDue    = $tabInfos->{'remainDue'};
    my $remainDue       = (exists $tabData->{'remainDue'} ? &LOC::Util::round($tabData->{'remainDue'}, 2) : $oldRemainDue);

    # L'état de clôture est possible que si l'encaissement est non reçu
    if ($encashmentId ne ENCASHMENTSTATE_NOTRECEIVED)
    {
        $closureId = undef;
    }
    # Le reste dû est possible que si l'encaissement est partiel
    if ($encashmentId ne ENCASHMENTSTATE_PARTIAL)
    {
        $remainDue = 0;
    }
    # Le commentaire de validation est possible que si elle est validé par l'agence
    if (!$isValidated)
    {
        $validComment = '';
    }

    my $oldIsFinalized = $tabInfos->{'isFinalized'};
    my $newIsFinalized = ($isJustified || $isValidated || $encashmentId eq ENCASHMENTSTATE_RECEIVED);



    # Le reste dû ne peut être à zéro pour un encaissement partiel
    if ($encashmentId eq ENCASHMENTSTATE_PARTIAL && ($remainDue <= 0 || $remainDue >= $tabInfos->{'total'}))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0020);
        return 0;
    }

    # Il n'est pas possible de "définaliser" une pro forma
    if ($tabInfos->{'isFinalized'} && ($encashmentId ne ENCASHMENTSTATE_RECEIVED && !$isJustified && !$isValidated))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0021);
        return 0;
    }

    # il n'est pas possible de valider par agence sans saisir de commentaire
    if ($isValidated && $validComment eq '')
    {
        push(@{$tabErrors->{'fatal'}}, 0x0023);
        return 0;
    }


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Enregistrement dans la table f_proforma
    my $tabUpdates = {
        'pfm_is_justified'      => $isJustified,
        'pfm_is_validated'      => $isValidated,
        'pfm_validcomment'      => ($validComment ne '' ? $validComment : undef),
        'pfm_sta_id_encashment' => $encashmentId,
        'pfm_sta_id_closure'    => $closureId,
        'pfm_remaindue'         => $remainDue,

        'pfm_date_modif*'       => 'Now()'
    };
    # On abandonne la pro forma si elle est clôturée
    if ($encashmentId eq ENCASHMENTSTATE_NOTRECEIVED && $closureId ne '')
    {
        $tabUpdates->{'pfm_sta_id'} = STATE_ABORTED;
    }

    if ($db->update('f_proforma', $tabUpdates, {'pfm_id' => $proformaId}, $schemaName) == -1)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);

        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Mise à jour des documents impactés
    if ((defined $tabInfos->{'contract.id'} &&
         !&_updateDocument($db, $countryId, 'contract', $tabInfos->{'contract.id'}, $userId, $tabEndUpds)) ||
        (defined $tabInfos->{'estimateLine.id'} &&
         !&_updateDocument($db, $countryId, 'estimateLine', $tabInfos->{'estimateLine.id'}, $userId, $tabEndUpds)))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    #Mise à jour de la date de fin sur le contrat
    if ($tabInfos->{'type.id'} == LOC::Proforma::Type::ID_EXTENSION && !$oldIsFinalized && $newIsFinalized)
    {
        my $tabContractData = {
            'endDate'       => $tabInfos->{'endDate'},
            'tabRentTariff' => {
                'isKeepAmount' => 1
            }
        };
        # on indique au contrat, que la modification vient d'une validation, pour ne pas expirer les pro formas à cause du changement de durée
        $tabContractData->{'updateFrom'} = 'validatePF';

        my $tabContractErrors = $tabErrors->{'modules'}->{'contract'};
        if (!&LOC::Contract::Rent::update($countryId, $tabInfos->{'contract.id'}, $tabContractData, $userId, $tabContractErrors, $tabEndUpds))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0100);
            # Erreur fatale ?
            if (&LOC::Util::in_array(0x0001, $tabContractErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, 0x0001);
            }

            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }
    }

    if (($oldEncashmentId ne ENCASHMENTSTATE_RECEIVED && $encashmentId eq ENCASHMENTSTATE_RECEIVED) || (!defined $oldClosureId && defined $closureId))
    {
        # Mise à jour des alertes Règlements proforma (commerce et comptabilité)
        &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::PROFORMA_ALL, -1);
        &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::PROFORMA_AGENCY, -1);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    # Envoi de l'email de modification par la compta
    if (($isJustified || ($isValidated && defined $validComment && $validComment ne '') || $encashmentId eq ENCASHMENTSTATE_RECEIVED) &&
        ($oldIsJustified != $isJustified || $oldIsValidated != $isValidated || $oldValidComment ne $validComment || $oldEncashmentId ne $encashmentId))
    {
        my @tabModif;
        if ($isJustified)
        {
            push(@tabModif, 'justification');
        }
        if ($isValidated && defined $validComment && $validComment ne '')
        {
            $tabInfos->{'validComment'} = $validComment;
            push(@tabModif, 'validation');
        }
        if ($encashmentId eq ENCASHMENTSTATE_RECEIVED)
        {
            push(@tabModif, 'encashment');
        }
        &_sendEMail($countryId, $tabInfos, \@tabModif);
    }

    return 1;
}


# Function: updateGroupLines
# Met à jour des lignes spécifiques des pro forma d'une ligne de devis ou d'un contrat
#
# Parameters
# string     $countryId     - Pays
# string     $documentType  - type de document
# int        $documentId    - id du document
# hashref    $tabFilters    - Filtres de recherche pour la mise à jour
# arrayref   $tabToUpdate   - Groupe à mettre à jour ('modelOrPeriod', 'rentServices', 'fuelEstimation')
sub updateGroupLines
{
    my ($countryId, $documentType, $documentId, $tabFilters, $tabToUpdate, $tabErrors) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    if ($documentType eq 'estimateLine')
    {
        $tabFilters->{'estimateLineId'} = $documentId;
    }
    elsif ($documentType eq 'contract')
    {
        $tabFilters->{'contractId'} = $documentId;
    }
    else
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $tabList = &getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters);

    foreach my $tabInfos (values %$tabList)
    {
        my $tabUpdates = {};
        if (!&_updateGroupLines($db, $countryId, $documentType, $documentId, $tabInfos, $tabToUpdate, $tabUpdates))
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }

        # Mise à jour des montants totaux
        $tabUpdates->{'pfm_amount*'} = '(SELECT IFNULL(ROUND(SUM(pfl_unitprice * pfl_quantity), 2), 0) FROM f_proformaline WHERE pfl_pfm_id = ' . $tabInfos->{'id'} . ')';
        if ($tabInfos->{'isVatInvoiced'})
        {
            $tabUpdates->{'pfm_amountwithvat*'} = '(SELECT IFNULL(ROUND(SUM(pfl_unitprice * pfl_quantity * (1 + pfl_vatrate / 100)), 2), 0) FROM f_proformaline WHERE pfl_pfm_id = ' . $tabInfos->{'id'} . ')';
        }
        else
        {
            $tabUpdates->{'pfm_amountwithvat*'} = $tabUpdates->{'pfm_amount*'};
        }

        if ($db->update('f_proforma', $tabUpdates, {'pfm_id*' => $tabInfos->{'id'}}, $schemaName) == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }
    }
    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }
    return 1;
}


# Function: _deleteLine
# Supprime une ligne de proforma
#
# Parameters:
# LOC::Db::Adapter $db             - Connexion à la base de données
# string           $countryId      - Pays
# int              $proformaLineId - Id de la ligne de proforma
# arrayRef         $tabLineErrors  - Tableau d'erreurs
sub _deleteLine
{
    my ($db, $countryId, $proformaLineId, $tabLineErrors) = @_;

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # - Suppression d'une ligne de proforma
    if ($db->delete('f_proformaline', {'pfl_id*' => $proformaLineId}, $schemaName) == -1)
    {
        return 0;
    }
    return 1;
}

# function: _getAction
# Vérifie les règles de gestion pour la génération d'une proforma
#
# Parameters:
# string   $countryId           - Pays
# string   $documentType        - Type de document
# int      $documentId          - Id du document
# hashref  $tabData     - Données de la proforma
# hashref  $tabErrors           - Tableau des erreurs
#
# Returns:
# bool -
sub _getAction
{
    my ($countryId, $documentType, $documentId, $tabData, $tabErrors) = @_;

    my $tabProformaInfos = &getInfosByDocument($countryId, $documentType, $documentId, $tabData);

    if (!$tabProformaInfos)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    my $tabAllIds      = &getAllIds($countryId, $documentType, $documentId, [STATE_ACTIVE, STATE_REPLACED]);
    my $tabActiveIds   = $tabAllIds->{'state_' . STATE_ACTIVE};
    my $tabReplacedIds = $tabAllIds->{'state_' . STATE_REPLACED};


    # si le type de montant du document est mensuel, on ne peut pas faire de pro forma
    if ($tabProformaInfos->{'document.amountType'} ne 'day')
    {
        push(@{$tabErrors->{'fatal'}}, 0x0004);
        return 0;
    }

    # si une date de fin est saisie, on vérifie qu'elle est supérieure à la date du contrat
    if ($tabProformaInfos->{'type.id'} == LOC::Proforma::Type::ID_EXTENSION &&
        (&LOC::Date::checkMySQLDate($tabProformaInfos->{'endDate'}) == 0 ||
         $tabProformaInfos->{'endDate'} eq '' ||
         $tabProformaInfos->{'endDate'} le $tabProformaInfos->{'realEndDate'}))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0008);
        return 0;
    }

    # On vérifie que le montant total de la pro forma n'est pas nul
    if ($tabProformaInfos->{'amount'} == 0 && $tabProformaInfos->{'paidAmount'} == 0)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0017);
        return 0;
    }

    # On vérifie que le mode de règlement est bien renseigné
    if ($tabProformaInfos->{'paymentMode.id'} == 0)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0018);
        return 0;
    }

    # On vérifie que l'estimation carburant n'est pas supérieur au max possible
    if ($tabProformaInfos->{'fuelQuantity'} > $tabProformaInfos->{'fuelQtyMax'})
    {
        push(@{$tabErrors->{'fatal'}}, 0x0019);
        return 0;
    }

    # Vérification des types possibles
    my $tabAuthorized = &_getAuthorizedTypes($countryId, $documentType, $documentId, $tabAllIds);
    if (!(&LOC::Util::in_array($tabProformaInfos->{'type.id'}, $tabAuthorized)))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0005);
        return 0;
    }

    $tabProformaInfos->{'link.id'} = undef;

    # Cas différents types de documents
    # - Proforma
    if ($tabProformaInfos->{'type.id'} == LOC::Proforma::Type::ID_PROFORMA)
    {
        # est-ce qu'une proforma existe ?
        if (@{$tabActiveIds->{'type_' . LOC::Proforma::Type::ID_PROFORMA}->{'_all'}} > 0 || @{$tabActiveIds->{'expired'}->{'_all'}} > 0)
        {
            my $notExpiredPFId = $tabActiveIds->{'type_' . LOC::Proforma::Type::ID_PROFORMA}->{'notExpired'}->{'_all'}->[0];
            my $expiredPFId = $tabActiveIds->{'expired'}->{'_all'};
            $tabProformaInfos->{'link.id'} = ($notExpiredPFId ? $notExpiredPFId : $expiredPFId);
        }
    }

    # - Proforma de prolongation
    elsif ($tabProformaInfos->{'type.id'} == LOC::Proforma::Type::ID_EXTENSION)
    {
        if (&LOC::Date::checkMySQLDate($tabProformaInfos->{'endDate'}) == 0 || $tabProformaInfos->{'endDate'} eq '')
        {
            push(@{$tabErrors->{'fatal'}}, 0x0008);
            return 0;
        }

        # si des pro forma de prolongation existent déjà
        if (@{$tabActiveIds->{'type_' . LOC::Proforma::Type::ID_EXTENSION}->{'notExpired'}->{'_all'}} > 0 || @{$tabActiveIds->{'type_' . LOC::Proforma::Type::ID_EXTENSION}->{'expired'}->{'_all'}} > 0)
        {
            my $expiredPFId = $tabActiveIds->{'type_' . LOC::Proforma::Type::ID_EXTENSION}->{'expired'}->{'_all'}->[0];
            my $notExpiredPFIds = $tabActiveIds->{'type_' . LOC::Proforma::Type::ID_EXTENSION}->{'notExpired'}->{'_all'};
            my $lastNotExpiredPFId = $notExpiredPFIds->[-1];
            my $activeProformaId = ($lastNotExpiredPFId ? $lastNotExpiredPFId : $expiredPFId);

            # Etat de la pro forma
            my $proformaExist = &getInfos($countryId, $activeProformaId);

            # si la date de fin saisie est postérieure à toutes les dates de fin existantes
            if ($tabProformaInfos->{'endDate'} gt $proformaExist->{'endDate'})
            {
                if (!$proformaExist->{'isFinalized'})
                {
                    # Remplacement
                    $tabProformaInfos->{'link.id'} = $proformaExist->{'id'};
                }
            }
            else
            {
                push(@{$tabErrors->{'fatal'}}, 0x0009);
                return 0;
            }
        }
    }

    # - Facture d'acompte
    elsif ($tabProformaInfos->{'type.id'} == LOC::Proforma::Type::ID_ADVANCE)
    {
        # si des factures d'acompte existent déjà
        if (@{$tabActiveIds->{'type_' . LOC::Proforma::Type::ID_ADVANCE}->{'_all'}} > 0 || @{$tabActiveIds->{'expired'}->{'_all'}} > 0)
        {
            my $notExpiredPFId = $tabActiveIds->{'type_' . LOC::Proforma::Type::ID_ADVANCE}->{'notExpired'}->{'_all'}->[0];
            my $expiredPFId = $tabActiveIds->{'expired'}->{'_all'}->[0];
            my $activeProformaId = ($notExpiredPFId ? $notExpiredPFId : $expiredPFId);

            # Etat de la pro forma
            my $proformaExist = &getInfos($countryId, $activeProformaId);
            if ($proformaExist->{'isFinalized'})
            {
                push(@{$tabErrors->{'fatal'}}, 0x0005);
                return 0;
            }
            else
            {
                # Remplacement
                $tabProformaInfos->{'link.id'} = $proformaExist->{'id'};
            }
        }
    }

    return $tabProformaInfos;
}


# Function _getAuthorizedTypes
# retourne les types de pro forma possible à générer
#
# Parameters:
# string  $countryId    - Pays
# string  $documentType - Type de document (contrat/ligne de devis)
# int     $documentId   - Id du document
# hashref $tabAllIds    - Stats du document
#
# Returns:
# arrayref
sub _getAuthorizedTypes
{
    my ($countryId, $documentType, $documentId, $tabAllIds) = @_;

    my $tabActiveIds   = $tabAllIds->{'state_' . STATE_ACTIVE};
    my $tabReplacedIds = $tabAllIds->{'state_' . STATE_REPLACED};


    # Est-ce que le client est en pro forma obligatoire ?
    my $isProformaRequired = 0;
    if ($documentType eq 'contract')
    {
        my $tabDocInfos = &LOC::Contract::Rent::getInfos($countryId, $documentId, LOC::Contract::Rent::GETINFOS_CUSTOMER);
        if ($tabDocInfos)
        {
            $isProformaRequired = $tabDocInfos->{'customer.isProformaRequired'};
        }
    }
    elsif ($documentType eq 'estimateLine')
    {
        my $tabDocInfos = &LOC::Estimate::Rent::getLineInfos($countryId, $documentId, LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE |
                                                                                      LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE_CUSTOMER);
        if ($tabDocInfos)
        {
            $isProformaRequired = $tabDocInfos->{'estimate.customer.isProformaRequired'};
        }
    }

    # Calcul des types autorisés
    my $tabResult = [];
    if ($tabActiveIds && @{$tabActiveIds->{'type_' . LOC::Proforma::Type::ID_ADVANCE}->{'notExpired'}->{'_all'}} > 0)
    {
        # Une facture d'acompte existe
        if (@{$tabActiveIds->{'type_' . LOC::Proforma::Type::ID_ADVANCE}->{'notExpired'}->{'finalized'}} == 0)
        {
            # On peut en faire une nouvelle a condition qu'elle ne soit pas finalisée
            $tabResult = [LOC::Proforma::Type::ID_ADVANCE];
        }
    }
    elsif ($tabActiveIds && @{$tabActiveIds->{'type_' . LOC::Proforma::Type::ID_PROFORMA}->{'notExpired'}->{'_all'}} > 0)
    {
        # Une pro forma existe
        if (@{$tabActiveIds->{'type_' . LOC::Proforma::Type::ID_PROFORMA}->{'notExpired'}->{'finalized'}} == 0)
        {
            # On peut en faire une nouvelle a condition qu'elle ne soit pas finalisée
            $tabResult = [LOC::Proforma::Type::ID_PROFORMA];
        }
        else
        {
            # Sinon on peut tout de même faire une pro forma avec déjà versé (si pas de prolongation en cours)
            if (@{$tabActiveIds->{'type_' . LOC::Proforma::Type::ID_EXTENSION}->{'notExpired'}->{'_all'}} == 0)
            {
                push(@$tabResult, LOC::Proforma::Type::ID_PROFORMA);
            }
            # dans le cas du contrat, prolongation possible si PF finalisée et si la récupération n'est pas un transfert (transfert inter-chantier ou reprise sur chantier)
            my $tabRecoveryInfos = &LOC::Transport::getDocumentRecoveryInfos($countryId, $documentType, $documentId);
            if ($documentType eq 'contract' && $tabRecoveryInfos->{'type'} ne LOC::Transport::TYPE_SITESTRANSFER)
            {
                push(@$tabResult, LOC::Proforma::Type::ID_EXTENSION);
            }
        }
    }
    elsif ($tabActiveIds && @{$tabActiveIds->{'type_' . LOC::Proforma::Type::ID_EXTENSION}->{'notExpired'}->{'_all'}} > 0)
    {
        $tabResult = [LOC::Proforma::Type::ID_EXTENSION];
    }
    elsif (
        ($tabActiveIds && @{$tabActiveIds->{'notExpired'}->{'_all'}} == 0) &&
        (
         ($tabActiveIds && @{$tabActiveIds->{'type_' . LOC::Proforma::Type::ID_PROFORMA}->{'expired'}->{'finalized'}} > 0) ||
         ($tabReplacedIds && @{$tabReplacedIds->{'type_' . LOC::Proforma::Type::ID_PROFORMA}->{'expired'}->{'finalized'}} > 0) ||
         ($tabActiveIds && @{$tabActiveIds->{'type_' . LOC::Proforma::Type::ID_EXTENSION}->{'expired'}->{'finalized'}} > 0) ||
         ($tabReplacedIds && @{$tabReplacedIds->{'type_' . LOC::Proforma::Type::ID_EXTENSION}->{'expired'}->{'finalized'}} > 0)
        )
    )
    {
        $tabResult = [LOC::Proforma::Type::ID_PROFORMA];
    }
    else
    {
        if ($isProformaRequired)
        {
            $tabResult = [LOC::Proforma::Type::ID_PROFORMA];
        }
        else
        {
            $tabResult = [LOC::Proforma::Type::ID_ADVANCE, LOC::Proforma::Type::ID_PROFORMA];
        }
    }

    return $tabResult;
}


# Function : _getLines
# Retourne la liste des lignes d'une pro forma.
#
# Parameters
# string  $countryId  - Pays
# int     $id         - Id de la proforma
# hashref $tabFilters - Filtres
#
# Return
# arrayref
sub _getLines
{
    my ($countryId, $id, $tabFilters) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT
   pfl_id AS `id`,
   pfl_pfa_id AS `articleRef`,
   pfl_label AS `label`,
   pfl_labelextra AS `labelExtra`,
   pfl_unitprice AS `unitPrice`,
   pfl_quantity AS `quantity`,
   pfl_vatrate AS `vatRate`
FROM f_proformaline
INNER JOIN p_proformaartref
ON pfa_id = pfl_pfa_id
WHERE pfl_pfm_id =' . $id;

    if (defined $tabFilters && defined $tabFilters->{'articleRef'})
    {
        $query .= '
AND pfl_pfa_id IN (' . $db->quote($tabFilters->{'articleRef'}) . ')';
    }
    $query .= '
ORDER BY pfl_order, pfl_id;';

    my $tab = $db->fetchAssoc($query);

    if (!$tab)
    {
        return 0;
    }

    foreach my $tabRow (@$tab)
    {
        $tabRow->{'id'}        *= 1;
        $tabRow->{'unitPrice'} *= 1;
        $tabRow->{'quantity'}  *= 1;
        $tabRow->{'vatRate'}   *= 1;
        $tabRow->{'amount'}     = &LOC::Util::round($tabRow->{'quantity'} * $tabRow->{'unitPrice'}, 4);
    }

    return (wantarray ? %$tab : $tab);
}


# Function: _insert
# insertion d'une proforma
#
# Parameters:
# LOC::Db::Adapter $db        - Connexion à la base de données
# string           $countryId - Pays
# hashref          $tabInfos  - Données du document (contrat/ligne de devis + proforma + lignes de proforma)
# int              $userId    - Utilisateur
sub _insert
{
    my ($db, $countryId, $documentType, $documentId, $tabInfos, $userId, $tabErrors) = @_;

    # Données à insérer
    my $tabUpdates = {
        'pfm_pft_id'            => $tabInfos->{'type.id'},
        'pfm_id_link'           => $tabInfos->{'link.id'},
        'pfm_id_extension'      => $tabInfos->{'extension.id'},
        'pfm_cus_id'            => $tabInfos->{'customer.id'},
        'pfm_amount'            => $tabInfos->{'amount'},
        'pfm_is_vatinvoiced'    => ($tabInfos->{'isVatInvoiced'} ? 1 : 0),
        'pfm_amountwithvat'     => $tabInfos->{'amountWithVat'},
        'pfm_paidamount'        => $tabInfos->{'paidAmount'},
        'pfm_deposit'           => $tabInfos->{'deposit'},
        'pfm_pmt_id'            => $tabInfos->{'paymentMode.id'},
        'pfm_date_begin'        => $tabInfos->{'beginDate'},
        'pfm_date_end'          => $tabInfos->{'endDate'},
        'pfm_fuelqty'           => $tabInfos->{'fuelQuantity'},
        'pfm_fuelunitprice'     => $tabInfos->{'fuelUnitPrice'},
        'pfm_comment'           => ($tabInfos->{'comment'} ne '' ? $tabInfos->{'comment'} : undef),

        'pfm_is_justified'      => 0,
        'pfm_is_validated'      => 0,
        'pfm_validcomment'      => undef,
        'pfm_sta_id_encashment' => ENCASHMENTSTATE_WAITING,
        'pfm_sta_id_closure'    => undef,
        'pfm_is_expired'        => 0,

        'pfm_percent'           => $tabInfos->{'percent'},
        'pfm_agc_id'            => $tabInfos->{'agency.id'},
        'pfm_usr_id'            => $userId,
        'pfm_date_create*'      => 'Now()',
        'pfm_sta_id'            => STATE_ACTIVE
    };

    # Contrat ou devis ?
    if ($documentType eq 'contract')
    {
        $tabUpdates->{'pfm_rct_id'} = $documentId;
    }
    elsif ($documentType eq 'estimateLine')
    {
        $tabUpdates->{'pfm_rel_id'}   = $documentId;
    }

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction;

    # - Insertion d'une proforma
    my $proformaId = $db->insert('f_proforma', $tabUpdates, $schemaName);
    if ($proformaId == -1)
    {
        return 0;
    }
    $proformaId *= 1;

    # - Insertion des lignes proforma
    foreach my $tabLineData (@{$tabInfos->{'tabLines'}})
    {
        my $proformaLineId = &_insertLine($db, $countryId, $proformaId, $tabLineData);
        if (!$proformaLineId)
        {
            return 0;
        }
    }

    return $proformaId;
}


# Function: _insertLine
# Ajouter une ligne de proforma
#
# Parameters:
# LOC::Db::Adapter $db          - Connexion à la base de données
# string           $countryId   - Pays
# int              $proformaId  - Id de la proforma
# hashref          $tabLineData - Données de la ligne
sub _insertLine
{
    my ($db, $countryId, $proformaId, $tabLineData, $tabLineErrors) = @_;

    # Données à insérer
    my $tabUpdates = {
        'pfl_pfm_id'     => $proformaId,
        'pfl_pfa_id'     => $tabLineData->{'articleRef'},
        'pfl_label'      => $tabLineData->{'label'},
        'pfl_labelextra' => $tabLineData->{'labelExtra'},
        'pfl_unitprice'  => $tabLineData->{'unitPrice'},
        'pfl_quantity'   => $tabLineData->{'quantity'},
        'pfl_vatrate'    => $tabLineData->{'vatRate'},
        'pfl_order'      => $tabLineData->{'order'}
    };

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # - Insertion d'une ligne de proforma
    my $proformaLineId = $db->insert('f_proformaline', $tabUpdates, $schemaName);
    if ($proformaLineId == -1)
    {
        return 0;
    }

    return $proformaLineId;
}


# Function: _updateDocument
# Mise à jour du champ DETAILSDEVISPROFORMAOK/CONTRATPROFORMAOK
#
# Parameters :
# LOC::Db::Adapter $db           - Connexion à la base de données
# string           $countryId    - Pays
# string           $documentType - Type de document
# int              $documentId   - Id du document
# int              $userId       - Identifiant de l'utilisateur responsable
# hashref          $tabEndUpds   - Mises à jour de fin
#
# Returns :
# bool
sub _updateDocument
{
    my ($db, $countryId, $documentType, $documentId, $userId, $tabEndUpds) = @_;

    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my $flags = 0;
    # Récupération des stats du document
    my $tabAllIds     = &getAllIds($countryId, $documentType, $documentId, [STATE_ACTIVE, STATE_ABORTED]);
    my $tabActiveIds  = $tabAllIds->{'state_' . STATE_ACTIVE};
    my $tabAbortedIds = $tabAllIds->{'state_' . STATE_ABORTED};

    # Recherche si des proformas sont actives
    my $nbActived = @{$tabActiveIds->{'_all'}};
    my $nbActivedFinalized = @{$tabActiveIds->{'finalized'}};

    if ($nbActived > 0)
    {
        $flags |= DOCFLAG_EXISTACTIVED;
        if ($nbActived == $nbActivedFinalized)
        {
            $flags |= DOCFLAG_ALLACTIVEDFINALIZED;
        }
    }

    # Recherche de proforma actives ET ((justifiées OU validées) OU encaissées)
    if ($nbActivedFinalized > 0)
    {
        $flags |= DOCFLAG_EXISTACTIVEDFINALIZED;
    }

    # Recherche de proforma actives et expirées
    my $nbActiveExpired = @{$tabActiveIds->{'expired'}->{'_all'}};
    if ($nbActiveExpired > 0)
    {
        $flags |= DOCFLAG_EXISTACTIVEDEXPIRED;
    }

    # Recherche des factures d'acomptes actives
    my $nbActiveAdvances = @{$tabActiveIds->{'type_' . LOC::Proforma::Type::ID_ADVANCE}->{'_all'}};
    if ($nbActiveAdvances > 0)
    {
        $flags |= DOCFLAG_EXISTACTIVEDADVANCE;
    }
    # Recherche des pro forma de prolongation actives
    my $nbActiveExtensions = @{$tabActiveIds->{'type_' . LOC::Proforma::Type::ID_EXTENSION}->{'_all'}};

    if ($nbActiveExtensions > 0)
    {
        $flags |= DOCFLAG_EXISTACTIVEDEXTENSION;
    }

    # Mise à jour du champ DETAILSDEVIS ou CONTRAT
    if ($documentType eq 'contract')
    {
        # Mise à jour des flags pro forma sur le contrat
        &LOC::Contract::Rent::updateProformaFlags($countryId, $documentId, $flags);
    }
    elsif ($documentType eq 'estimateLine')
    {
        my $tabUpdates = {};

        # Si la ligne de devis n'est pas active alors on ne met pas à jour l'estimation carburant
        my $query = '
SELECT
    IF (DETAILSDEVISOK = 0, DEVISAUTO, 0),
    DETAILSDEVISPROFORMAOK
FROM ' . $schemaName . '.DETAILSDEVIS
WHERE DETAILSDEVISAUTO = ' . $documentId . ';';
        my ($estimateId, $oldFlags) = @{$db->fetchRow($query, {}, LOC::Db::FETCH_NUM)};

        $estimateId *= 1;
        $oldFlags   *= 1;

        if ($estimateId)
        {
            # Récupération des informations du carburant sur la dernière pro forma active
            my $tabFuelInfos = &getFuelEstimationInfos($countryId, 'estimateLine', $documentId);

            # Récupération du taux monétaire
            my $euroRate = &LOC::Country::getEuroRate($countryId);

            # Mise à jour des informations
            $tabUpdates = {
                # Mise à jour des infos carburant
                'DETAILSDEVISCARBUQTE'         => $tabFuelInfos->{'quantity'},
                'DETAILSDEVISCARBUMONTANT'     => &LOC::Util::round($tabFuelInfos->{'amount'}, 2),
                'DETAILSDEVISCARBUMONTANTEU'   => &LOC::Util::round($tabFuelInfos->{'amount'} * $euroRate, 2),
                'DETAILSDEVISCARBUPXLITRE'     => $tabFuelInfos->{'unitPrice'},
                'DETAILSDEVISCARBUPXLITREEU'   => &LOC::Util::round($tabFuelInfos->{'unitPrice'} * $euroRate, 2)
            };
        }

        # Calcul des flags
        my $newFlags = ($oldFlags & 255) | $flags;
        if ($newFlags != $oldFlags)
        {
            $tabUpdates->{'DETAILSDEVISPROFORMAOK'} = $newFlags;

            # Changements pouvant impacter le transport ? => on le lui communique
            &LOC::Transport::onExternalChanges($countryId, {
                'estimateLine' => {
                    'id'    => $documentId,
                    'event' => 'update',
                    'props' => {
                        'proformaFlags' => {'old' => $oldFlags, 'new' => $newFlags}
                    }
                }
            }, $userId, undef, $tabEndUpds);
        }

        if (keys %$tabUpdates > 0 && $db->update('DETAILSDEVIS', $tabUpdates,
                                                 {'DETAILSDEVISAUTO*' => $documentId},
                                                 $schemaName) == -1)
        {
            return 0;
        }

        # Mise à jour du montant total du devis dans le cas d'un changement dans l'estimation carburant
        if ($estimateId && !LOC::Estimate::Rent::updateAmounts($countryId, $estimateId, {}))
        {
            return 0;
        }
    }

    return 1;
}


# Function: _updateDocuments
# Mise à jour des documents (lignes de devis ou contrats)
#
# Parameters :
# LOC::Db::Adapter $db           - Connexion à la base de données
# string           $countryId    - Pays
# hashref          $tabDocuments - Tableau des documents (type : 'estimateLines' ou 'contracts')
# int              $userId       - Identifiant de l'utilisateur responsable
# hashref          $tabEndUpds   - Mises à jour de fin
#
# Returns :
# bool
sub _updateDocuments
{
    my ($db, $countryId, $tabDocuments, $userId, $tabEndUpds) = @_;

    # mise à jour du flag proformaOK et mise à jour du carburant sur les lignes de devis
    if (exists $tabDocuments->{'estimateLines'})
    {
        my @tabEstimateLineIds = &LOC::Util::array_unique($tabDocuments->{'estimateLines'});

        foreach my $estimateLineId (@tabEstimateLineIds)
        {
            if (!&_updateDocument($db, $countryId, 'estimateLine', $estimateLineId, $userId, $tabEndUpds))
            {
                return 0;
            }
        }
    }

    # mise à jour du flag proformaOK sur les contrats
    if (exists $tabDocuments->{'contracts'})
    {
        my @tabContractIds = &LOC::Util::array_unique($tabDocuments->{'contracts'});

        foreach my $contractId (@tabContractIds)
        {
            if (!&_updateDocument($db, $countryId, 'contract', $contractId, $userId, $tabEndUpds))
            {
                return 0;
            }
        }
    }

    return 1;
}


# Function: _updateGroupLines
# Met à jour des lignes spécifiques des pro forma d'une ligne de devis ou d'un contrat
#
# Parameters
# LOC::Db::Adapter $db           - Connexion à la base de données
# string           $countryId    - Pays
# string           $documentType - type de document
# int              $documentId   - id du document
# hashref          $tabFilters   - Filtres de recherche pour la mise à jour
# arrayref         $tabToUpdate  - Groupe à mettre à jour ('modelOrPeriod', 'rentServices', 'fuelEstimation')
sub _updateGroupLines
{
    my ($db, $countryId, $documentType, $documentId, $tabInfos, $tabToUpdate, $tabUpdates) = @_;

    my $tabArticles = [];
    # Le groupe 'modelOrPeriod' impacte l'article "Location de matériel"
    if (&LOC::Util::in_array('modelOrPeriod', $tabToUpdate))
    {
        push(@$tabArticles, 'LOCAT');
    }
    # Le groupe 'rentServices' impacte l'article "Services complémentaires"
    if (&LOC::Util::in_array('rentServices', $tabToUpdate))
    {
        push(@$tabArticles, 'SERCL');
    }
    # Le groupe 'fuelEstimation' impacte l'article "Estimation carburant"
    if (&LOC::Util::in_array('fuelEstimation', $tabToUpdate))
    {
        push(@$tabArticles, 'ECARB');
    }

    # Cas spécial pour la facture d'acompte et pour le déjà versé modifié
    if (($tabInfos->{'type.id'} == LOC::Proforma::Type::ID_ADVANCE && (&LOC::Util::in_array('advance', $tabToUpdate) || @$tabArticles > 0))
        || &LOC::Util::in_array('paidAmount', $tabToUpdate) )
    {
        $tabArticles = undef;
    }


    my $tabLineErrors = {};

    my $tabNewInfos = &getInfosByDocument($countryId, $documentType, $documentId, $tabInfos, $tabArticles);

    my $tabOldLines = &_getLines($countryId, $tabInfos->{'id'}, {'articleRef' => $tabArticles});
    my $tabNewLines = $tabNewInfos->{'tabLines'};

    # Modification des lignes
    my $oldLineIndex  = 0;
    my $countOldLines = @$tabOldLines;
    my $newLineIndex;
    my $countNewLines;
    while ($oldLineIndex < $countOldLines)
    {
        $newLineIndex = 0;
        $countNewLines = @$tabNewLines;
        while ($newLineIndex < $countNewLines && $tabNewLines->[$newLineIndex]->{'articleRef'} ne $tabOldLines->[$oldLineIndex]->{'articleRef'})
        {
            $newLineIndex++;
        }
        if ($newLineIndex < $countNewLines)
        {
            if (!&_updateLine($db, $countryId, $tabOldLines->[$oldLineIndex]->{'id'}, $tabNewLines->[$newLineIndex], $tabOldLines->[$oldLineIndex], $tabLineErrors))
            {
                return 0;
            }

            splice(@$tabOldLines, $oldLineIndex, 1);
            splice(@$tabNewLines, $newLineIndex, 1);
            $countOldLines = @$tabOldLines;
        }
        else
        {
            $oldLineIndex++;
        }
    }
    # Suppression des lignes
    $countOldLines = @$tabOldLines;
    for ($oldLineIndex = 0; $oldLineIndex < $countOldLines; $oldLineIndex++)
    {
        if (!&_deleteLine($db, $countryId, $tabOldLines->[$oldLineIndex]->{'id'}, $tabLineErrors))
        {
            return 0;
        }
    }
    # Ajout des lignes
    $countNewLines = @$tabNewLines;
    for ($newLineIndex = 0; $newLineIndex < $countNewLines; $newLineIndex++)
    {
        if (!&_insertLine($db, $countryId, $tabInfos->{'id'}, $tabNewLines->[$newLineIndex], $tabLineErrors))
        {
            return 0;
        }
    }


    # Mise à jour du pourcentage de l'acompte
    if ($tabInfos->{'type.id'} == LOC::Proforma::Type::ID_ADVANCE &&
        (&LOC::Util::in_array('advance', $tabToUpdate) || &LOC::Util::in_array('modelOrPeriod', $tabToUpdate)))
    {
        # Mise à jour de la date de début sur la pro forma
        $tabUpdates->{'pfm_percent'} = $tabNewInfos->{'percent'};
    }
    # Le groupe 'modelOrPeriod' impacte aussi la date de début de la pro forma
    if (&LOC::Util::in_array('modelOrPeriod', $tabToUpdate))
    {
        # Mise à jour de la date de début sur la pro forma
        $tabUpdates->{'pfm_date_begin'} = $tabNewInfos->{'beginDate'};
    }
    # Le groupe 'modelOrPeriod' impacte aussi la date de début de la pro forma
    if (&LOC::Util::in_array('fuelEstimation', $tabToUpdate))
    {
        # Mise à jour de la date de début sur la pro forma
        $tabUpdates->{'pfm_fuelqty'} = $tabNewInfos->{'fuelQuantity'};
    }

    return 1;
}


#Function: _updateLine
#
# Parameters
# LOC::Db::Adapter $db             - Connexion à la base de données
# string           $countryId      - pays
# int              $proformaLineId - id de la ligne de proforam
# hashref          $tabLineData    - nouvelles infos ligne
# hashref          $tabLineInfos   - anciennes infos ligne
# arrayref         $tabLineErrors  - Tableau d'erreurs
#
# Returns :
# bool
sub _updateLine
{
    my ($db, $countryId, $proformaLineId, $tabLineData, $tabLineInfos, $tabLineErrors) = @_;

    if (!$tabLineInfos)
    {
        push(@{$tabLineErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Paramètres
    my $articleRef = (exists $tabLineData->{'articleRef'} ? $tabLineData->{'articleRef'} : $tabLineInfos->{'articleRef'});
    my $label      = (exists $tabLineData->{'label'} ? $tabLineData->{'label'} : $tabLineInfos->{'label'});
    my $labelExtra = (exists $tabLineData->{'labelExtra'} ? $tabLineData->{'labelExtra'} : $tabLineInfos->{'labelExtra'});
    my $unitprice  = (exists $tabLineData->{'unitPrice'} ? $tabLineData->{'unitPrice'} : $tabLineInfos->{'unitPrice'});
    my $quantity   = (exists $tabLineData->{'quantity'} ? $tabLineData->{'quantity'} : $tabLineInfos->{'quantity'});
    my $vatRate    = (exists $tabLineData->{'vatRate'} ? $tabLineData->{'vatRate'} : $tabLineInfos->{'vatRate'});

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Enregistrement dans la table f_proforma
    my $tabUpdates = {
        'pfl_pfa_id'     => $articleRef,
        'pfl_label'      => $label,
        'pfl_labelextra' => $labelExtra,
        'pfl_unitprice'  => $unitprice,
        'pfl_quantity'   => $quantity,
        'pfl_vatrate'    => $vatRate
    };

    if ($db->update('f_proformaline', $tabUpdates, {'pfl_id' => $proformaLineId}, $schemaName) == -1)
    {
        push(@{$tabLineErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    return 1;
}


#Function: _getLineIndexByArticleRef
#
# Parameters
# arrayref $tabLines   - Tableau des lignes de pro forma
# string   $articleRef - Nom de l'article à rechercher
#
# Returns :
# int
sub _getLineIndexByArticleRef
{
    my ($tabLines, $articleRef) = @_;

    my $i = 0;
    my $linesCount = @$tabLines;
    while ($i < $linesCount && $tabLines->[$i]->{'articleRef'} ne $articleRef)
    {
        $i++;
    }

    return ($i < $linesCount ? $i : -1);
}

# Function: _sendMail
#
# Params:
# string  $countryId   - Pays
# hashref $tabProforma - Infos pro forma
# bool    $isValidated - finalisation ou non
# hashref $tabModif    - tableau des modifications par la compta
sub _sendEMail()
{
    my ($countryId, $tabProforma, $tabModif) = @_;
    my $eMailObj = LOC::Mail->new();
    my $tabUserInfos = &LOC::User::getInfos($tabProforma->{'user.id'});
    my $agencyId = $tabProforma->{'agency.id'};
    my $tabAgencyInfos = &LOC::Agency::getInfos($agencyId);
    #Encodage
    $eMailObj->setEncodingType(LOC::Mail::ENCODING_TYPE_UTF8);

    # Expéditeur
    my %tabSender = ($tabUserInfos->{'email'} => $tabUserInfos->{'firstName'} . ' ' . $tabUserInfos->{'name'});
    $eMailObj->setSender(%tabSender);

    # Corps
    my $nameFile = (defined $tabProforma->{'contract.id'} ? 'validatedProformaContract.html' : 'validatedProformaEstimate.html');
    my $file = &LOC::Globals::get('cgiPath') . '/cfg/templates/' . $countryId . '/proforma/' . $nameFile;

    open(TPL, $file);
    my $template = join('', <TPL>);
    close(TPL);

    # construction des différentes données
    # - URL du document contrat/devis
    my $documentCode = (defined $tabProforma->{'contract.id'} ? $tabProforma->{'contract.code'} : $tabProforma->{'estimateLine.estimate.code'});
    my $estimateLine = $tabProforma->{'estimateLine.index'};
    my $paramsUrl = (defined $tabProforma->{'contract.id'} ? {'contractId' => $tabProforma->{'contract.id'}} : {'estimateId' => $tabProforma->{'estimateLine.estimate.id'}, 'estimateLineId' => $tabProforma->{'estimateLine.id'}});
    my $fileUrl = (defined $tabProforma->{'contract.id'} ? 'rent:rentContract:view' : 'rent:rentEstimate:view');

    my $documentUrl = &LOC::Request::createRequestUri($fileUrl, $paramsUrl);
    my $documentInfos = {};
    my $tabMachineInfos = {};
    if (defined $tabProforma->{'contract.id'})
    {
        $documentInfos = &LOC::Contract::Rent::getInfos($countryId, $tabProforma->{'contract.id'},
                                                                LOC::Contract::Rent::GETINFOS_EXT_MODEL |
                                                                LOC::Contract::Rent::GETINFOS_EXT_MACHINE |
                                                                LOC::Contract::Rent::GETINFOS_EXT_SITE);
        if ($documentInfos->{'machine.id'})
        {
            $tabMachineInfos->{'model.label'} = $documentInfos->{'machine.tabInfos'}->{'model.fullName'};
            $tabMachineInfos->{'parkNumber'}  = $documentInfos->{'machine.tabInfos'}->{'parkNumber'};
        }
        else
        {
            $tabMachineInfos->{'model.label'} = $documentInfos->{'model.tabInfos'}->{'label'};
            $tabMachineInfos->{'parkNumber'}  = '---';
        }
    }
    else
    {
        $documentInfos = &LOC::Estimate::Rent::getLineInfos($countryId, $tabProforma->{'estimateLine.id'},
                                                                LOC::Estimate::Rent::GETLINEINFOS_MODEL |
                                                                LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE_SITE);
        $tabMachineInfos->{'model.label'} = $documentInfos->{'model.label'};
        $tabMachineInfos->{'parkNumber'}  = '';
    }
    my $tabSiteInfos = {};
    if (defined $tabProforma->{'contract.id'})
    {
        $tabSiteInfos = $documentInfos->{'site.tabInfos'};
    }
    else
    {
        $tabSiteInfos = {
            'label'          => $documentInfos->{'estimate.site.label'},
            'address'        => $documentInfos->{'estimate.site.address'},
            'locality.label' => $documentInfos->{'estimate.site.locality.label'}
        };
    }

    # Remplacement dans le template
    my @tabAddress = ();
    if ($tabSiteInfos->{'label'} ne '')
    {
        push(@tabAddress, $tabSiteInfos->{'label'});
    }
    if ($tabSiteInfos->{'address'} ne '')
    {
        push(@tabAddress, $tabSiteInfos->{'address'});
    }
    if ($tabSiteInfos->{'locality.label'} ne '')
    {
        push(@tabAddress, $tabSiteInfos->{'locality.label'});
    }

    my $tabReplaces = {
        'proforma.type'          => lc($tabProforma->{'type.label'}),
        'proforma.indexLine'     => (defined $tabProforma->{'contract.id'} ? $tabProforma->{'indexOnContract'} : $tabProforma->{'indexOnEstimateLine'}),
        'proforma.modifications' => $tabModif,
        'proforma.validComment'  => $tabProforma->{'validComment'},
        'document.url'           => $documentUrl,
        'document.code'          => $documentCode,
        'proforma.amount'        => $tabProforma->{'amount'},
        'document.estimateLine'  => $tabProforma->{'estimateLine.index'},
        'user.firstName'         => $tabUserInfos->{'firstName'},
        'user.name'              => $tabUserInfos->{'name'},
        'agency.label'           => $tabAgencyInfos->{'label'},
        'agency.telephone'       => $tabAgencyInfos->{'telephone'},
        'agency.fax'             => $tabAgencyInfos->{'fax'},
        'customer.code'          => $tabProforma->{'customer.code'},
        'customer.name'          => $tabProforma->{'customer.name'},
        'date.begin'             => $documentInfos->{'beginDate'},
        'date.end'               => $documentInfos->{'endDate'},
        'machine.model'          => $tabMachineInfos->{'model.label'},
        'machine.park'           => $tabMachineInfos->{'parkNumber'},
        'site.address'           => join(' - ', @tabAddress)
    };

    $template =~ s/(\<%([a-z]{3})\:(.*?)\>)/@{[&_formatTemplate($2, $3, $tabReplaces)]}/g;

    while ($template =~ m/\<\%(.*?)\>/)
    {
        my $replace = '';
        if (exists $tabReplaces->{$1})
        {
            $replace = $tabReplaces->{$1};
        }
        $template =~ s/<%$1>/$replace/g;
    }

    my $subject = ($template =~ m/\<title\>(.*?)\<\/title\>/ ? $1 : '');

    if ($template =~ /"cid:(.+)"/ && -e &LOC::Globals::get('imgPath') . '/print/gui/' . $1)
    {
        $eMailObj->addInlineAttachment(&LOC::Globals::get('imgUrl') . '/print/gui/' . $1, $1);
    }

    $eMailObj->setBodyType(LOC::Mail::BODY_TYPE_HTML);
    my %tabRecipient;
    my $templatePlus = '';
    # Objet
    if (&LOC::Globals::getEnv() ne 'exp')
    {
        $subject = '[TEST] ' . $subject;

        # Informations sur l'utilisateur
        my $tabCurrentUserInfos = &LOC::Session::getUserInfos();
        if (defined $tabCurrentUserInfos)
        {
            %tabRecipient = ($tabCurrentUserInfos->{'email'} => $tabCurrentUserInfos->{'firstName'} . ' ' . $tabCurrentUserInfos->{'name'});
        }
        else
        {
            # En cas !
            %tabRecipient = ('archive.dev@acces-industrie.com' => 'Développeurs');
        }

        # Destinataires d'origines
        $templatePlus = 'Destinataire d\'origine : ' . $tabUserInfos->{'firstName'} . ' ' . $tabUserInfos->{'name'} . ' &lt;' . $tabUserInfos->{'email'} . '&gt;, ' .
                        $tabAgencyInfos->{'label'} . ' &lt;' . $tabAgencyInfos->{'email'} . '&gt;<br />';
    }
    else
    {
        %tabRecipient = ($tabUserInfos->{'email'} => $tabUserInfos->{'firstName'} . ' ' . $tabUserInfos->{'name'},
                         $tabAgencyInfos->{'email'} => $tabAgencyInfos->{'label'});
    }

    $eMailObj->setBody($templatePlus . $template);

    $eMailObj->addRecipients(%tabRecipient);

    $eMailObj->setSubject($subject);

    $eMailObj->send();

    return 1;

}

# Function: _formatTemplate
# formate une chaîne de caractères
#
# Params:
# string    $code          - quel formatage à appliquer
# string    $value         - la chaîne template à formater
# hashref   $tabReplaces   - les valeurs de la chaîne à mettre dans le template
#
# Return:
# string - la chaîne formatée
sub _formatTemplate
{
    my ($code, $value, $tabReplaces) = @_;

    my @params = split(/\|/, $value);

    if ($code eq 'cur')
    {
        return &LOC::Template::formatCurrency($value, $tabReplaces);
    }
    if ($code eq 'dte')
    {
        return &LOC::Template::formatDate($value, $tabReplaces);
    }
    # Types de modification de pro forma
    if ($code eq 'mod')
    {
        my $tabModif = $tabReplaces->{'proforma.modifications'};
        if ((&LOC::Util::in_array('justification', $tabModif) && $params[1] eq 'justification') ||
            (&LOC::Util::in_array('encashment', $tabModif) && $params[1] eq 'encashment'))
        {
            return '<li>' . $params[0] . '</li>';
        }
        elsif (&LOC::Util::in_array('validation', $tabModif) && $params[1] eq 'validation')
        {
            return '<li>' . $params[0] . ' : ' . $tabReplaces->{'proforma.validComment'} . '</li>';
        }
        else
        {
            return '';
        }
    }
    return $value;
}

# Function: getAlertRights
# droit sur les alertes proforma compta/commerce
#
# Params:
# string    $countryId      - pays
# string    $userId         - id de l'utilisateur
#
# Return:
# hashref - Tableau des droits
sub getAlertRights
{
    my ($countryId, $userId) = @_;

    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    my $isUserAdmin  = $tabUserInfos->{'isAdmin'};
    my $isUserChefAg = &LOC::Util::in_array(LOC::User::GROUP_AGENCYMANAGER, $tabUserInfos->{'tabGroups'});
    my $isUserComm   = &LOC::Util::in_array(LOC::User::GROUP_COMMERCIAL, $tabUserInfos->{'tabGroups'});
    my $isUserCompta = &LOC::Util::in_array(LOC::User::GROUP_ACCOUNTING, $tabUserInfos->{'tabGroups'});

    my $tabRights = {
        'business'   => '', #alerte commerce
        'accounting' => ''  #alerte comptabilité
    };
    if (!$isUserAdmin && !$isUserChefAg && !$isUserComm && !$isUserCompta)
    {
        return $tabRights;
    }

    if ($isUserAdmin || $isUserCompta)
    {
        $tabRights->{'business'}   = 'ok';
        $tabRights->{'accounting'} = 'ok';
    }
    if ($isUserChefAg || $isUserComm)
    {
        $tabRights->{'business'}   = 'ok';
    }

    return $tabRights;
}

1;