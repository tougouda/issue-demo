use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Transport::TimeSlot
# Module de gestion des plages horaires de livraison et récupération de transport
package LOC::Transport::TimeSlot;


# Constants: États
# STATE_ACTIVE  - Actif
# STATE_DELETED - Supprimé
use constant {
    STATE_ACTIVE  => 'GEN01',
    STATE_DELETED => 'GEN03',

    GETINFOS_ALL  => 0xFFFF,
    GETINFOS_BASE => 0x0000
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Util;


# Function: delete
# Supprime une plage horaire (change l'état)
#
# Parameters:
# string    $countryId   - Pays
# integer   $id          - Id de la plage horaire
#
# Returns:
# integer
sub delete
{
    my ($countryId, $id) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    return $db->update('p_transporttimeslot', {'tts_sta_id' => STATE_DELETED}, {'tts_id' => $id});
}


# Function: getInfos
# Retourne les infos d'une plage horaire
#
# Parameters:
# string  $countryId  - Pays
# int     $id         - Id de la plage horaire
# int     $flags      - Flags (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getInfos
{
    my ($countryId, $id, $flags) = @_;

    $id *= 1;
    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id, 'stateId' => [STATE_ACTIVE, STATE_DELETED]}, $flags);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Récupérer la liste des plages horaires de transport
#
# Contenu du hachage:
# - id               => Id
# - label            => Libellé
# - beginHour        => Heure de début
# - endHour          => Heure de fin
# - creationDate     => Date de création
# - modificationDate => Date de modification
# - state.id         => Etat
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id)
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hash|hashref
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    unless (defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    unless (defined $tabOptions)
    {
        $tabOptions = {};
    }

    my $labelQuery = '';
    if ($tabOptions->{'labelFormat'} == 2)
    {
        $labelQuery = 'CONCAT(LPAD(tts_beginhour, 2, "0"), ":00 - ", LPAD(tts_endhour, 2, "0"), ":00")';
    }
    else
    {
        $labelQuery = 'CONCAT(LPAD(tts_beginhour, 2, "0"), "-", LPAD(tts_endhour, 2, "0"))';
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT';

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query .= '
    tts_id AS `id`,
    ' . $labelQuery . ' AS `label`,
    tts_beginhour AS `beginHour`,
    tts_endhour AS `endHour`,
    tts_date_create AS `creationDate`,
    tts_date_modif AS `modificationDate`,
    tts_sta_id AS `state.id`';
    }
    elsif($format == LOC::Util::GETLIST_PAIRS)
    {
        $query .= '
    tts_id,
    ' . $labelQuery;
    }
    elsif($format == LOC::Util::GETLIST_COUNT)
    {
        $query .= '
    COUNT(*)';
    }

    $query .= '
FROM p_transporttimeslot';

    my $tabWheres = [];

    my $tabStates = (defined $tabFilters->{'stateId'} ? $tabFilters->{'stateId'} : [STATE_ACTIVE]);
    push(@$tabWheres, 'tts_sta_id IN (' . $db->quote($tabStates) . ')');
    if (defined $tabFilters->{'id'})
    {
        push(@$tabWheres, 'tts_id IN (' . $db->quote($tabFilters->{'id'}) . ')');
    }

    if (@$tabWheres > 0)
    {
        $query .= '
WHERE ' . join("\nAND ", @$tabWheres);
    }

    $query .= '
ORDER BY tts_beginhour ASC, tts_endhour ASC';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    return (wantarray ? %$tab : $tab);
}


# Function: set
# Met à jour (insert/update) toutes les infos d'une plage horaire
#
# Contenu du hachage à passer en paramètre:
# - id        => id
# - beginHour => Heure de début
# - endHour   => Heure de fin
#
# Parameters:
# string    $countryId - Pays
# hashref   $values    - Valeurs de la plage horaire
#
# Returns:
# integer
sub set
{
    my ($countryId, $values) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);


    my $tabUpdates = {
        'tts_beginhour' => $values->{'beginHour'} * 1,
        'tts_endhour'   => $values->{'endHour'} * 1
    };

    # Mise à jour
    if (defined $values->{'id'} && $values->{'id'} != 0)
    {
        my $id = $values->{'id'} * 1;
        $db->update('p_transporttimeslot', $tabUpdates, {'tts_id' => $id});

        return 1;
    }
    # Création
    else
    {
        # Vérifie si cette plage horaire existe déjà en supprimée
        my $query = '
SELECT
    tts_id
FROM p_transporttimeslot
WHERE tts_beginhour = ' . $tabUpdates->{'tts_beginhour'} . '
AND tts_endhour = ' . $tabUpdates->{'tts_endhour'} . '
AND tts_sta_id = ' . $db->quote(STATE_DELETED) . '
LIMIT 1;';
        my $oldId = $db->fetchOne($query);
        if ($oldId)
        {
            # Réactive la plage horaire existante
            $db->update('p_transporttimeslot', {'tts_sta_id' => STATE_ACTIVE}, {'tts_id' => $oldId});
        }
        else
        {
            # Créé une nouvelle plage horaire
            $tabUpdates->{'tts_date_create*'} = 'Now()';
            $tabUpdates->{'tts_sta_id'}       = STATE_ACTIVE;

            return $db->insert('p_transporttimeslot', $tabUpdates);
        }
    }
}


return 1;