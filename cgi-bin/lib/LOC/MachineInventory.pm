use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::MachineInventory
# Module permettant de gérer les états des lieux
package LOC::MachineInventory;



# Constants: États
# STATE_NOTATTACHED - État des lieux détaché
# STATE_ATTACHED    - État des lieux rattaché à un contrat
# STATE_DELETED     - État des lieux supprimé
use constant
{
    STATE_NOTATTACHED => 'MIN01',
    STATE_ATTACHED    => 'MIN02',
    STATE_DELETED     => 'MIN03'
};

# Constants: Types
# TYPE_DELIVERY - Livraison
# TYPE_RECOVERY - Récupération
use constant
{
    TYPE_DELIVERY => 'D',
    TYPE_RECOVERY => 'R'
};

# Constants: Nom de fichier pour les photos des états des lieux
use constant
{
    PHOTO_FILENAME_TPL => 'CHF_MI_%06s_%02s_%s%s'
};

# Constants: Récupération d'informations sur les état des lieux
# GETINFOS_ALL  - Récupération de l'ensemble des informations
# GETINFOS_BASE - Récupération des informations de base
use constant
{
    GETINFOS_ALL           => 0xFFFFFFFF,
    GETINFOS_BASE          => 0x00000000,
    GETINFOS_CREATOR       => 0x00000001,
    GETINFOS_MACHINE       => 0x00000002,
    GETINFOS_CONTRACT      => 0x00000004,
    GETINFOS_RIGHTS        => 0x00000008,
    GETINFOS_POSSIBILITIES => 0x00000010
};

# Constants: Statut de l'état des lieux
# STATUSFLAG_UNKNOWNMACHINE  - Machine inconnue
# STATUSFLAG_UNKNOWNCONTRACT - Contrat inconnu
# STATUSFLAG_INVALIDCOUPLE   - Association contrat/machine incohérente
use constant
{
    STATUSFLAG_UNKNOWNMACHINE  => 0x01,
    STATUSFLAG_UNKNOWNCONTRACT => 0x02,
    STATUSFLAG_INVALIDCOUPLE   => 0x04
};

# Constants: Erreurs
# ERROR_FATAL                 - Erreur fatale
# ERROR_RIGHTS                - Impossible de modifier cette information de l'état des lieux
# ERROR_ALREADYEXISTING       - État des lieux déjà existant
# ERROR_UNKNOWNCOUNTRY        - Pays inconnnu
# ERROR_UNKNOWNUSER           - Utilisateur inconnnu
# ERROR_TYPEREQUIRED          - Type requis
# ERROR_PARKNUMBERREQUIRED    - Numéro de parc machine requis,
# ERROR_CONTRACTCODEREQUIRED  - Code du contrat requis
# ERROR_INVALIDTYPE           - Type non valide
# ERROR_INVALIDPARKNUMBER     - Numéro de parc non valide
# ERROR_INVALIDCONTRACTCODE   - Code contrat non valide
# ERROR_REQUIREDPHOTOS        - Photos obligatoires
# ERROR_INVALIDPHOTOEXTENSION - Extension de photo non autorisée
# ERROR_INVALIDPHOTOTYPE      - Type de photo non autorisé
# ERROR_PHOTOMAXSIZEREACHED   - Taille maximale dépassée
# ERROR_PHOTONOTFOUND         - Photo introuvable
# ERROR_INVALIDPHOTOCONTENT   - Contenu de la photo non valide
# ERROR_ALREADYATTACHED       - État des lieux déjà rattaché
# ERROR_ALREADYDETACHED       - État des lieux déjà détaché
# ERROR_ATTACHED              - État des lieux rattaché (non modifiable)
# ERROR_UNDEFINEDMACHINE      - Machine non définie
# ERROR_UNKNOWNMACHINE        - Machine inconnue
# ERROR_UNDEFINEDCONTRACT     - Contrat non défini
# ERROR_UNKNOWNCONTRACT       - Contrat inconnu
# ERROR_INVALIDCOUPLE         - Association contrat/machine incohérente
# ERROR_EMAILREQUIRED         - Adresse e-mail client requise
# ERROR_NOTATTACHED           - État des lieux non rattaché
use constant
{
    ERROR_FATAL                     => 0x0001,
    ERROR_RIGHTS                    => 0x0003,

    ERROR_ALREADYEXISTING           => 0x0010,

    ERROR_UNKNOWNCOUNTRY            => 0x0100,
    ERROR_UNKNOWNUSER               => 0x0101,
    ERROR_TYPEREQUIRED              => 0x0102,
    ERROR_PARKNUMBERREQUIRED        => 0x0103,
    ERROR_CONTRACTCODEREQUIRED      => 0x0104,
    ERROR_INVALIDTYPE               => 0x0201,
    ERROR_INVALIDPARKNUMBER         => 0x0202,
    ERROR_INVALIDCONTRACTCODE       => 0x0203,

    ERROR_REQUIREDPHOTOS            => 0x0300,
    ERROR_INVALIDPHOTOEXTENSION     => 0x0301,
    ERROR_INVALIDPHOTOTYPE          => 0x0302,
    ERROR_PHOTOMAXSIZEREACHED       => 0x0303,
    ERROR_PHOTONOTFOUND             => 0x0304,
    ERROR_INVALIDPHOTOCONTENT       => 0x0305,

    ERROR_ALREADYATTACHED           => 0x0501,
    ERROR_ALREADYDETACHED           => 0x0502,
    ERROR_ATTACHED                  => 0x0503,
    ERROR_INVALIDSTATUS             => 0x0510,

    ERROR_UNDEFINEDMACHINE          => 0x1000,
    ERROR_UNKNOWNMACHINE            => 0x1001,
    ERROR_UNDEFINEDCONTRACT         => 0x1002,
    ERROR_UNKNOWNCONTRACT           => 0x1003,
    ERROR_INVALIDCOUPLE             => 0x1004,

    ERROR_EMAILREQUIRED             => 0x1100,
    ERROR_NOTATTACHED               => 0x1101,

    ERROR_DEVICEUIDUPDATEFORBIDDEN  => 0x1200,
    ERROR_UIDUPDATEFORBIDDEN        => 0x1201
};


use strict;

use Tie::IxHash;
use File::Basename;
use POSIX qw(strftime);


# Modules internes
use LOC::Machine;
use LOC::Attachments;
use LOC::Util;
use LOC::EndUpdates;
use LOC::Session;
use LOC::Db;
use Date::Parse;
use DateTime;
use DateTime::TimeZone;
use LOC::Contract::Rent;
use LOC::Agency;
use LOC::User;
use LOC::Country;
use LOC::Sms;
use LOC::Log;
use LOC::Date;
use LOC::Locale;
use POSIX;
use LOC::Template;


my $fatalErrMsg = 'Erreur fatale: module LOC::MachineInventory, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";


# Function: getRights
# Récupérer les droits sur un état des lieux (par rapport à l'utilisateur)
#
# Parameters:
# string $countryId  - Pays
# string $agencyId   - Agence
# int    $id         - Id de l'état des lieux
# int    $userId     - Id de l'utilisateur
#
# Returns:
# hash|hashref - Liste droits
sub getRights
{
    my ($countryId, $agencyId, $id, $userId) = @_;

    my $tabInfos = {};
    my $tabUserInfos;

    # Informations sur l'utilisateur
    $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Informations sur l'état des lieux
    if ($id)
    {
        $tabInfos = &getInfos($countryId, $id);
    }

    return &_getRights($countryId, $tabInfos, $tabUserInfos);
}


# Function: getInfos
# Retourne les infos d'un état des lieux
#
# Contenu du hashage :
# - id                    => identifiant de l'état des lieux
# - type                  => type de l'état des lieux
# - date                  => date de saisie
# - parkNumber            => n° de parc saisi
# - contractCode          => n° de contrat saisi
# - timeCounter           => compteur horaire saisi
# - description           => description
# - latitude              => latitude lors de la saisie
# - longitude             => longitude lors de la saisie
# - nbPhotos              => nombre de photos
# - contract.id           => identifiant du contrat
# - contract.code         => code du contrat
# - isContractCodeOk      => n° de contrat initialement saisi correspond à un contrat existant
# - machine.id            => identifiant de la machine
# - isParkNumberOk        => n° de parc initialement saisi correspond à un parc existant
# - status                => statut de l'état des lieux (voir STATUSFLAG)
# - attachedToContract.id => identifiant du contrat rattaché
# - agency.id             => identifiant de l'agence rattachée
# - creationDate          => date de création
# - modificationDate      => date de dernière modification
# - state.id              => identifiant de l'état
# FLAG GETINFOS_CREATOR
# - creator.firstName     => prénom de l'utilisateur qui a saisi l'état des lieux
# - creator.name          => nom de l'utilisateur qui a saisi l'état des lieux
# - creator.fullName      => nom complet de l'utilisateur qui a saisi l'état des lieux
#
# Parameters:
# string  $countryId  - Pays
# string  $id         - Identifiant de l'état des lieux
# int     $flags      - Flags (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {
        'id'      => $id,
        'stateId' => [
            STATE_NOTATTACHED,
            STATE_ATTACHED,
            STATE_DELETED
        ]
    }, $flags, $tabOptions);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Verrou sur la ligne de la ligne
    if (exists $tab->{$id} && (!$tabOptions->{'lockDbRow'} || &_lockDbRow($db, $id)))
    {
        return (wantarray ? %{$tab->{$id}} : $tab->{$id});
    }
    return undef;
}


# Function: getList
# Retourne la liste des états des lieux
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres (facultatif)
# int     $flags      - Flags pour le choix des colonnes (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Liste des états des lieux
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (ref($tabFilters) ne 'HASH')
    {
        $tabFilters = {};
    }
    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my @tabWheres = ();

    my $query;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    min_id AS `id`,
    min_type AS `type`,
    min_parknumber AS `parkNumber`,
    min_contractcode AS `contractCode`,
    min_timecounter AS `timeCounter`,
    min_description AS `description`,
    min_latitude AS `latitude`,
    min_longitude AS `longitude`,
    min_nbphotos AS `nbPhotos`,
    min_agc_id AS `agency.id`,
    min_mac_id AS `machine.id`,
    min_rct_id AS `contract.id`,
    min_is_parknumberok AS `isParkNumberOk`,
    min_is_contractcodeok AS `isContractCodeOk`,
    min_status AS `status`,
    min_rct_id_attachedto AS `attachedToContract.id`,
    min_date AS `date`,
    min_usr_id_creator AS `creator.id`,';
        if ($flags & GETINFOS_CREATOR)
        {
            $query .= '
    f_user_creator.usr_firstname AS `creator.firstName`,
    f_user_creator.usr_name AS `creator.name`,
    CONCAT_WS(" ", f_user_creator.usr_firstname, f_user_creator.usr_name) AS `creator.fullName`,';
        }
        $query .= '
    HEX(min_deviceuid) AS `deviceUid`,
    CONCAT(
            SUBSTRING(HEX(min_uid), 1, 8), ' . $db->quote('-') . ',
            SUBSTRING(HEX(min_uid), 9, 4), ' . $db->quote('-') . ',
            SUBSTRING(HEX(min_uid), 13, 4), ' . $db->quote('-') . ',
            SUBSTRING(HEX(min_uid), 17, 4), ' . $db->quote('-') . ',
            SUBSTRING(HEX(min_uid), 21, 12)
        ) AS `uid`,
    min_date_creation AS `creationDate`,
    min_date_modification AS `modificationDate`,
    min_sta_id AS `state.id`
FROM f_machineinventory';

        if ($flags & GETINFOS_CREATOR)
        {
            $query .= '
LEFT JOIN frmwrk.f_user f_user_creator
ON min_usr_id_creator = f_user_creator.usr_id'
        }
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(min_id)
FROM f_machineinventory';
    }
    else
    {
        $query = '
SELECT DISTINCT
    min_id AS `id`,
    CONCAT_WS(" / ", min_type, min_parknumber, min_contractcode) AS `label`
FROM f_machineinventory';
    }


    # Filtres
    # - Id
    if (defined $tabFilters->{'id'})
    {
        push(@tabWheres, 'min_id IN (' . $db->quote($tabFilters->{'id'}) . ')');
    }
    # - Type
    if (defined $tabFilters->{'type'})
    {
        push(@tabWheres, 'min_type IN (' . $db->quote($tabFilters->{'type'}) . ')');
    }
    # - État
    if (defined $tabFilters->{'stateId'})
    {
        push(@tabWheres, 'min_sta_id IN (' . $db->quote($tabFilters->{'stateId'}) . ')');
    }
    else
    {
        push(@tabWheres, 'min_sta_id <> ' . $db->quote(STATE_DELETED));
    }
    # - Machine liée
    if (defined $tabFilters->{'machineId'})
    {
        push(@tabWheres, 'min_mac_id IN (' . $db->quote($tabFilters->{'machineId'}) . ')');
    }
    # - Contrat lié
    if (defined $tabFilters->{'contractId'})
    {
        push(@tabWheres, 'min_rct_id IN (' . $db->quote($tabFilters->{'contractId'}) . ')');
    }
    # - Contrat rattaché
    if (defined $tabFilters->{'attachedToContractId'})
    {
        push(@tabWheres, 'min_rct_id_attachedto IN (' . $db->quote($tabFilters->{'attachedToContractId'}) . ')');
    }
    # - Identifiant unique du terminal
    if (defined $tabFilters->{'deviceUid'})
    {
        my $tabDeviceUid = (ref $tabFilters->{'deviceUid'} eq 'ARRAY' ? $tabFilters->{'deviceUid'} : [$tabFilters->{'deviceUid'}]);
        foreach my $deviceUid (@$tabDeviceUid)
        {
            $deviceUid = 'UNHEX(' . sprintf('%016s', $db->quote($deviceUid)) . ')';
        }
        push(@tabWheres, 'min_deviceuid IN (' . join(', ', @$tabDeviceUid) . ')');
    }
    # - Identidiant unique de l'état des lieux
    if (defined $tabFilters->{'uid'})
    {
        my $tabUid = (ref $tabFilters->{'uid'} eq 'ARRAY' ? $tabFilters->{'uid'} : [$tabFilters->{'uid'}]);
        foreach my $uid (@$tabUid)
        {
            $uid =~ s/-//g;
            $uid = 'UNHEX(' . $db->quote(sprintf('%032s', $uid)) . ')';
        }
        push(@tabWheres, 'min_uid IN (' . join(', ', @$tabUid) . ')');
    }

    if (@tabWheres > 0)
    {
        $query .= '
WHERE ' . join("\nAND ", @tabWheres);
    }

    # Tris
    if (defined $tabOptions->{'orderBy'})
    {
        my $tabOrderFields = {
            'date'             => 'min_date',
            'creationDate'     => 'min_date_creation',
            'modificationDate' => 'min_date_modification'
        };
        foreach my $orderBy (@{$tabOptions->{'orderBy'}})
        {
            my ($field, $dir) = split(/:/, $orderBy);
            $query .= '
ORDER BY ' . $tabOrderFields->{$field} . ' ' . (defined $dir && uc($dir) eq 'DESC' ? 'DESC' : 'ASC');
        }
    }
    else
    {
        $query .= '
ORDER BY min_date ASC';
    }

    if (defined $tabOptions->{'limit'})
    {
        $query .= '
LIMIT ' . $tabOptions->{'limit'};
    }

    $query .= ';';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    # Formatage des champs
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            my $tabContractInfos = undef;
            $tabRow->{'contract.id'}  = (defined $tabRow->{'contract.id'} ? $tabRow->{'contract.id'} * 1 : undef);
            if ($tabRow->{'contract.id'} && ($flags & GETINFOS_CONTRACT || $flags & GETINFOS_POSSIBILITIES))
            {
                $tabContractInfos = &_getContractInfos($countryId, $tabRow->{'contract.id'});
            }

            $tabRow->{'id'}              *= 1;
            $tabRow->{'nbPhotos'}        *= 1;
            $tabRow->{'timeCounter'}      = (defined $tabRow->{'timeCounter'} ? $tabRow->{'timeCounter'} * 1 : undef);
            $tabRow->{'latitude'}         = (defined $tabRow->{'latitude'} ? $tabRow->{'latitude'} * 1 : undef);
            $tabRow->{'longitude'}        = (defined $tabRow->{'longitude'} ? $tabRow->{'longitude'} * 1 : undef);
            $tabRow->{'machine.id'}       = ($tabRow->{'machine.id'} ? $tabRow->{'machine.id'} * 1 : undef);
            $tabRow->{'creator.id'}       = $tabRow->{'creator.id'} * 1;
            $tabRow->{'isParkNumberOk'}   = ($tabRow->{'isParkNumberOk'} != 0 ? 1 : 0);
            $tabRow->{'isContractCodeOk'} = ($tabRow->{'isContractCodeOk'} != 0 ? 1 : 0);
            if ($flags & GETINFOS_MACHINE)
            {
                if ($tabRow->{'machine.id'})
                {
                    $tabRow->{'machine.tabInfos'} = &_getMachineInfos($countryId, $tabRow->{'machine.id'});
                }
                else
                {
                    $tabRow->{'machine.tabInfos'} = undef;
                }
            }
            if ($flags & GETINFOS_CONTRACT)
            {
                $tabRow->{'contract.tabInfos'} = $tabContractInfos;
            }

            # Possibilités
            if ($flags & GETINFOS_POSSIBILITIES)
            {
                $tabRow->{'tabPossibilities'} = &_getPossibilities($tabRow, $tabContractInfos);
            }

            # Droits
            if ($flags & GETINFOS_RIGHTS)
            {
                # Informations sur l'utilisateur
                my $tabUserInfos = &LOC::Session::getUserInfos();
                if (defined $tabOptions->{'userId'} && $tabUserInfos->{'id'} != $tabOptions->{'userId'})
                {
                    $tabUserInfos = &LOC::User::getInfos($tabOptions->{'userId'});
                }
                $tabRow->{'tabRights'} = &_getRights($countryId, $tabRow, $tabUserInfos);
            }
        }
    }

    return (wantarray ? %$tab : $tab);
}


# Function: getHistory
# Récupérer l'historique de l'état des lieux
#
# Contenu d'une ligne du hashage :
# - id : identifiant de la ligne d'historique ;
# - eventType.code : code du type d'événement ;
# - eventType.label : libellé du type d'événement ;
# - user.id : identifiant de l'utilisateur ;
# - user.name : nom de l'utilisateur ;
# - user.firstName : prénom de l'utilisateur ;
# - user.fullName : nom complet de l'utilisateur ;
# - user.state.id : identifiant de l'état de l'utilisateur ;
# - datetime : date et heure de la trace ;
# - content : contenu de la trace.
#
# Parameters:
# string  $countryId  - Pays
# int     $machineId  - Identifiant de l'état des lieux
# string  $localeId   - Identifiant de la locale pour les traductions
#
# Returns:
# array|arrayref|undef - Historique de l'état des lieux
sub getHistory
{
    my ($countryId, $machineInventoryId, $localeId) = @_;

    return &LOC::Log::getFunctionalityHistory($countryId, 'machineInventory', $machineInventoryId, $localeId);
}

# Function: getPhotosList
# Récupère les photos d'un état des lieux
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Identifiant de l'état des lieux
# hashref $tabFilters - Filtres (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
sub getPhotosList
{
    my ($countryId, $id, $tabFilters, $tabOptions) = @_;

    if (ref($tabFilters) ne 'HASH')
    {
        $tabFilters = {};
    }
    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }

    $tabFilters->{'moduleName'} = LOC::Attachments::MODULE_MACHINEINVENTORY;
    $tabFilters->{'relatedIds'} = [$countryId, $id];

	return &LOC::Attachments::getList($countryId, $tabFilters, $tabOptions);

}

# Function: getPhotoInfos
# Récupère les informations d'une photo d'un état des lieux
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $photoId    - Identifiant d'une photo
# hashref $tabOptions - Options supplémentaires (facultatif)
sub getPhotoInfos
{
    my ($countryId, $photoId, $tabOptions) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }

    return &LOC::Attachments::getInfos($countryId, $photoId, $tabOptions);
}


# Function: insert
# Insère un nouvel état des lieux
#
# Parameters:
# string  $countryId  - Identifiant du pays
# hashref $tabData    - Tableau des données
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# int - Identifiant de l'état des lieux créé
sub insert
{
    my ($countryId, $tabData, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Vérification des données
    # - Type
    my $type = (defined $tabData->{'type'} ? $tabData->{'type'} : '');
    if ($type eq '')
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_TYPEREQUIRED);
    }
    elsif ($type ne TYPE_DELIVERY && $type ne TYPE_RECOVERY)
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_INVALIDTYPE);
    }
    # - Numéro de parc machine
    my $parkNumber = (defined $tabData->{'parkNumber'} ? &LOC::Util::trim($tabData->{'parkNumber'}) : '');
    if ($parkNumber eq '')
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_PARKNUMBERREQUIRED);
    }
    elsif (!&LOC::Util::isNumeric($parkNumber) || length $parkNumber < 3)
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_INVALIDPARKNUMBER);
    }
    # - Code du contrat
    my $contractCode = (defined $tabData->{'contractCode'} ? &LOC::Util::trim($tabData->{'contractCode'}) : '');
    if ($contractCode eq '')
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_CONTRACTCODEREQUIRED);
    }
    elsif (!&LOC::Util::isNumeric($contractCode))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_INVALIDCONTRACTCODE);
    }
    # Photos
    my $tabPhotos = (defined $tabData->{'photos'} ? $tabData->{'photos'} : []);
    my $nbPhotos = @$tabPhotos;
    if ($nbPhotos == 0)
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_REQUIREDPHOTOS);
    }

    # Erreurs ?
    if (@{$tabErrors->{'fatal'}} > 0)
    {
        return 0;
    }

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        if (defined $_[0])
        {
            print STDERR sprintf($fatalErrMsg, 'insert', $_[0], __FILE__, '');
        }
        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }
        return 0;
    };

    # Vérification du pays
    if (!&LOC::Country::isActive($countryId))
    {
        return &$rollBackAndError(__LINE__, ERROR_UNKNOWNCOUNTRY);
    }

    # Vérification si l'état des lieux n'existe pas déjà
    if (defined $tabData->{'deviceUid'} && defined $tabData->{'uid'})
    {
        my $tabExistingMachineInventories = &getList($countryId, LOC::Util::GETLIST_IDS, {
                    'deviceUid' => $tabData->{'deviceUid'},
                    'uid'       => $tabData->{'uid'}
                });

        if (defined $tabExistingMachineInventories)
        {
            my $nbExistingMachineInventories = @$tabExistingMachineInventories;
            if ($nbExistingMachineInventories == 1)
            {
                &$rollBackAndError(__LINE__, ERROR_ALREADYEXISTING);
                return $tabExistingMachineInventories->[0];
            }
            elsif ($nbExistingMachineInventories > 1)
            {
                return &$rollBackAndError(__LINE__, ERROR_FATAL);
            }
        }
    }

    # Vérification de l'utilisateur
    $userId = $userId * 1;
    if (!&LOC::User::exists($userId))
    {
        return &$rollBackAndError(__LINE__, ERROR_UNKNOWNUSER);
    }

    # Recherche de la machine saisie
    my $tabMachineInfos = undef;
    my $machineId = &_searchMachineIdByParkNumber($countryId, $parkNumber);
    if ($machineId)
    {
        $tabMachineInfos = &_getMachineInfos($countryId, $machineId);
    }

    # Recherche du contrat saisi
    my $tabContractInfos = undef;
    my $contractId = &_searchContractIdByCode($db, $contractCode);
    if ($contractId)
    {
        $tabContractInfos = &_getContractInfos($countryId, $contractId);
    }

    # Calcul de l'agence responsable
    my $agencyId = &_calculateAgencyId($tabMachineInfos, $tabContractInfos);

    # Calcul du status
    my $status = &_calculateStatus($tabMachineInfos, $tabContractInfos);

    # Description
    my $description = (defined $tabData->{'description'} ? &LOC::Util::trim($tabData->{'description'}) : undef);

    # Date
    my $date = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME);
    if (defined $tabData->{'date'})
    {
        my ($dtSeconds, $dtMinutes, $dtHours, $dtDay, $dtMonth, $dtYear, $dtZone) = Date::Parse::strptime($tabData->{'date'});

        my $defaultTZ = DateTime::TimeZone->new({'name' => 'local'});
        my $dt = DateTime->new({
            'year'       => $dtYear + 1900,
            'month'      => $dtMonth + 1,
            'day'        => $dtDay,
            'hour'       => $dtHours,
            'minute'     => $dtMinutes,
            'second'     => $dtSeconds,
            'nanosecond' => 0,
            'time_zone'  => ($dtZone ne '' ? 'UTC' : $defaultTZ)
        });

        $dt->set_time_zone($defaultTZ);

        $date = $dt->strftime('%Y-%m-%d %H:%M:%S');
    }

    # Insertion de l'état des lieux en base de données
    my $tabUpdates = {
        'min_type'              => $type,
        'min_parknumber'        => $parkNumber,
        'min_contractcode'      => $contractCode,
        'min_description'       => ($description ne '' ? $description : undef),
        'min_latitude'          => undef,
        'min_longitude'         => undef,
        'min_nbphotos'          => $nbPhotos,
        'min_agc_id'            => $agencyId,
        'min_mac_id'            => $machineId,
        'min_rct_id'            => $contractId,
        'min_is_parknumberok'   => ($machineId ? 1 : 0),
        'min_is_contractcodeok' => ($contractId ? 1 : 0),
        'min_rct_id_attachedto' => undef,
        'min_status'            => $status,
        'min_date'              => $date,
        'min_usr_id_creator'    => $userId,
        'min_date_creation*'    => 'NOW()',
        'min_sta_id'            => STATE_NOTATTACHED
    };

    # Identifiant unique du terminal
    if (defined $tabData->{'deviceUid'})
    {
        $tabUpdates->{'min_deviceuid*'} = 'UNHEX(' . $db->quote(sprintf('%016s', $tabData->{'deviceUid'})) . ')';
    }

    # Identifiant unique de l'état des lieux
    if (defined $tabData->{'uid'})
    {
        my $uid = $tabData->{'uid'};
        $uid =~ s/-//g;
        $tabUpdates->{'min_uid*'} = 'UNHEX(' . $db->quote(sprintf('%032s', $uid)) . ')';
    }

    # Compteur horaire
    if (defined $tabData->{'timeCounter'})
    {
        $tabUpdates->{'min_timecounter'} = $tabData->{'timeCounter'};
    }

    # Position géo
    if (defined $tabData->{'position'} && ref($tabData->{'position'}) eq 'HASH' &&
        defined $tabData->{'position'}->{'lat'} && &LOC::Util::trim($tabData->{'position'}->{'lat'}) ne '' &&
        defined $tabData->{'position'}->{'lng'} && &LOC::Util::trim($tabData->{'position'}->{'lng'}) ne '')
    {
        $tabUpdates->{'min_latitude'}  = &LOC::Util::trim($tabData->{'position'}->{'lat'}) * 1;
        $tabUpdates->{'min_longitude'} = &LOC::Util::trim($tabData->{'position'}->{'lng'}) * 1;
    }

    my $machineInventoryId = $db->insert('f_machineinventory', $tabUpdates, $schemaName);
    if ($machineInventoryId == -1)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }
    $machineInventoryId *= 1;


    # Récupération des photos
    my $tabPhotosOptions = (defined $tabOptions->{'photos'} && ref($tabOptions->{'photos'}) eq 'HASH' ? $tabOptions->{'photos'} : {});

    # Préparation des données pour LOC::Attachments
    my $photoIndex = 1;
    my @tabAttachments = ();
    foreach my $tabPhotoInfos (@$tabPhotos)
    {
        my ($fileName, $filePath, $extension) = fileparse($tabPhotoInfos->{'name'}, '\.[^\.]*');
        my $datetime = strftime "%Y%m%d%H%M", localtime;
        $fileName = sprintf(PHOTO_FILENAME_TPL, $machineInventoryId, $photoIndex, $datetime, $extension);

        push(@tabAttachments, {
            'fileName'    => $fileName,
            'fileContent' => $tabPhotoInfos->{'content'},
            'date'        => $tabPhotoInfos->{'date'},
            'position'    => $tabPhotoInfos->{'position'}
        });

        $photoIndex++;
    }

    # Configurations des photos
    my $tabAttachOptions = &LOC::Json::fromJson(&LOC::Characteristic::getCountryValueByCode('CFGEDL', $countryId));

    $tabAttachOptions->{'isBase64Content'} = ($tabPhotosOptions->{'isBase64Content'} ? 1 : 0);

    my $tabAttachmentErrors = {};
    if (!&LOC::Attachments::insertMultiple($countryId, LOC::Attachments::MODULE_MACHINEINVENTORY,
                                           $machineInventoryId, \@tabAttachments,
                                           $userId, $tabAttachOptions,
                                           $tabAttachmentErrors, $tabEndUpds))
    {
        if (&LOC::Util::in_array(LOC::Attachments::ERROR_INVALIDFILEEXTENSION, $tabAttachmentErrors->{'fatal'}))
        {
            # Extension de fichier non autorisée
            push(@{$tabErrors->{'fatal'}}, ERROR_INVALIDPHOTOEXTENSION);
        }
        elsif (&LOC::Util::in_array(LOC::Attachments::ERROR_INVALIDFILETYPE, $tabAttachmentErrors->{'fatal'}))
        {
            # Type de fichier non autorisé
            push(@{$tabErrors->{'fatal'}}, ERROR_INVALIDPHOTOTYPE);
        }
        elsif (&LOC::Util::in_array(LOC::Attachments::ERROR_MAXSIZEREACHED, $tabAttachmentErrors->{'fatal'}))
        {
            # Taille maximale dépassée
            push(@{$tabErrors->{'fatal'}}, ERROR_PHOTOMAXSIZEREACHED);
        }
        elsif (&LOC::Util::in_array(LOC::Attachments::ERROR_FILENOTFOUND, $tabAttachmentErrors->{'fatal'}))
        {
            # Fichier introuvable
            push(@{$tabErrors->{'fatal'}}, ERROR_PHOTONOTFOUND);
        }
        elsif (&LOC::Util::in_array(LOC::Attachments::ERROR_INVALIDCONTENT, $tabAttachmentErrors->{'fatal'}))
        {
            # Contenu non valide
            push(@{$tabErrors->{'fatal'}}, ERROR_INVALIDPHOTOCONTENT);
        }
        return &$rollBackAndError(__LINE__, (@{$tabErrors->{'fatal'}} == 0 ? ERROR_FATAL : undef));
    }


    # Trace de création
    &LOC::EndUpdates::addTrace($tabEndUpds, 'machineInventory', $machineInventoryId, {
        'schema'  => $schemaName,
        'code'    => LOC::Log::EventType::MOBILECREATION,
        'old'     => undef,
        'new'     => undef,
        'content' => '',
        'date'    => $date
    });

    # Trace d'enregistrement
    &LOC::EndUpdates::addTrace($tabEndUpds, 'machineInventory', $machineInventoryId, {
        'schema'  => $schemaName,
        'code'    => LOC::Log::EventType::SAVE,
        'old'     => undef,
        'new'     => undef,
        'content' => ''
    });

    # Incrémentation de l'alerte des états des lieux à traiter
    my $alertAgencyId = $agencyId || &LOC::Country::getMainAgencyId($countryId);
    &LOC::EndUpdates::updateAlert($tabEndUpds, $alertAgencyId, LOC::Alert::MACHINEINVENTORIES, 1);

    if ($status == 0)
    {
        # Rattachement automatique
        if (!&_attach($db, $countryId, {
                'id'                    => $machineInventoryId,
                'agency.id'             => $agencyId,
                'type'                  => $type,
                'machine.id'            => $machineId,
                'machine.tabInfos'      => $tabMachineInfos,
                'contract.id'           => $contractId,
                'contract.tabInfos'     => $tabContractInfos,
                'attachedToContract.id' => undef,
                'nbPhotos'              => $nbPhotos,
                'status'                => 0,
                'date'                  => $date
            }, $userId, $tabEndUpds))
        {
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return $machineInventoryId;
}


# Function: update
# Modifie un état des lieux
#
# Parameters:
# string  $countryId          - Identifiant du pays
# int     $machineInventoryId - Identifiant de l'état des lieux
# hashref $tabData            - Données à mettre à jour
# int     $userId             - Identifiant de l'utilisateur responsable
# hashref $tabOptions         - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors          - Tableau d'erreurs
# hashref $tabEndUpds         - Mises à jour de fin
#
# Returns:
# int - 1 si c'est un succès, 0 sinon
sub update
{
    my ($countryId, $machineInventoryId, $tabData, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'update', $_[0], __FILE__, '');
        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }
        return 0;
    };

    # Récupération des informations de l'état des lieux
    my $tabInfos = &getInfos($countryId, $machineInventoryId, GETINFOS_BASE | GETINFOS_RIGHTS | GETINFOS_POSSIBILITIES,
                                  {'writeDbLock' => 1});
    if (!$tabInfos)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Vérifications Droits
    my $tabRights = $tabInfos->{'tabRights'};
    if (!$tabRights->{'actions'}->{'update'})
    {
        return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
    }

    # - Possibilités
    my $tabPossibilities = $tabInfos->{'tabPossibilities'};
    if (!$tabPossibilities->{'actions'}->{'update'})
    {
        # L'état des lieux doit être détaché
        if ($tabInfos->{'state.id'} eq STATE_ATTACHED)
        {
            # Erreur état des lieux attaché
            return &$rollBackAndError(__LINE__, ERROR_ATTACHED);
        }
        # Erreur fatale
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Erreur si tentative de changer l'UID du terminal ou de l'état des lieux
    my $oldDeviceUid = $tabInfos->{'deviceUid'};
    my $newDeviceUid = (exists $tabData->{'deviceUid'} ? $tabData->{'deviceUid'} : $oldDeviceUid);

    if ($oldDeviceUid ne $newDeviceUid)
    {
        # Mise à jour de l'identifiant unique du terminal interdite
        push(@{$tabErrors->{'fatal'}}, ERROR_DEVICEUIDUPDATEFORBIDDEN);
    }

    my $oldUid = $tabInfos->{'uid'};
    my $newUid = (exists $tabData->{'uid'} ? $tabData->{'uid'} : $oldUid);

    if ($oldUid ne $newUid)
    {
        # Mise à jour de l'identifiant unique interdite
        push(@{$tabErrors->{'fatal'}}, ERROR_UIDUPDATEFORBIDDEN);
    }

    # Paramètres

    # - Identifiant de la machine liée
    my $oldMachineId = $tabInfos->{'machine.id'};
    my $newMachineId = (exists $tabData->{'machine.id'} ? $tabData->{'machine.id'} : $oldMachineId);

    my $tabMachineInfos = undef;
    if (!$newMachineId)
    {
        # Machine non définie
        push(@{$tabErrors->{'fatal'}}, ERROR_UNDEFINEDMACHINE);
    }
    else
    {
        # Vérification de la machine liée
        $tabMachineInfos = &_getMachineInfos($countryId, $newMachineId);
        if (!$tabMachineInfos)
        {
            # Machine inconnue
            push(@{$tabErrors->{'fatal'}}, ERROR_UNKNOWNMACHINE);
        }
    }

    # - Identifiant du contrat lié
    my $oldContractId = $tabInfos->{'contract.id'};
    my $newContractId = (exists $tabData->{'contract.id'} ? $tabData->{'contract.id'} : $oldContractId);

    my $tabContractInfos = undef;
    if (!$newContractId)
    {
        # Contrat non défini
        push(@{$tabErrors->{'fatal'}}, ERROR_UNDEFINEDCONTRACT);
    }
    else
    {
        # Vérification du contrat lié
        $tabContractInfos = &_getContractInfos($countryId, $newContractId);
        if (!$tabContractInfos)
        {
            # Contrat inconnu
            push(@{$tabErrors->{'fatal'}}, ERROR_UNKNOWNCONTRACT);
        }
    }

    # Si erreur de vérification de données alors on arrête !!
    if (@{$tabErrors->{'fatal'}} > 0)
    {
        return &$rollBackAndError(__LINE__);
    }

    # Calcul du status
    my $oldStatus = $tabInfos->{'status'};
    my $newStatus = &_calculateStatus($tabMachineInfos, $tabContractInfos);

    # Calcul du status
    my $oldAgencyId = $tabInfos->{'agency.id'};
    my $newAgencyId = &_calculateAgencyId($tabMachineInfos, $tabContractInfos);


    # Tableau des mise à jour à effectuer
    my $tabUpdates = {};
    my $tabChanges = {};

    # - Machine liée
    if ($oldMachineId != $newMachineId)
    {
        $tabUpdates->{'min_mac_id'} = $newMachineId;
        $tabChanges->{'machine.id'} = {'old' => $oldMachineId, 'new' => $newMachineId};

        # Ajout d'une trace
        &LOC::EndUpdates::addTrace($tabEndUpds, 'machineInventory', $machineInventoryId, {
            'schema'  => $schemaName,
            'code'    => LOC::Log::EventType::CHANGEMACHINE,
            'old'     => $oldMachineId,
            'new'     => $newMachineId,
            'content' => $oldMachineId . ' → ' . $newMachineId
        });

        if ($tabInfos->{'isParkNumberOk'})
        {
            $tabUpdates->{'min_is_parknumberok'} = 0;
        }
        else
        {
            # Vérification de la machine initialement saisie
            my $originalMachineId = &_searchMachineIdByParkNumber($countryId, $tabInfos->{'parkNumber'});
            if ($originalMachineId && $originalMachineId == $newMachineId)
            {
                $tabUpdates->{'min_is_parknumberok'} = 1;
            }
        }
    }

    # - Contrat lié
    if ($oldContractId != $newContractId)
    {
        $tabUpdates->{'min_rct_id'} = $newContractId;
        $tabChanges->{'contract.id'} = {'old' => $oldContractId, 'new' => $newContractId};

        # Ajout d'une trace
        &LOC::EndUpdates::addTrace($tabEndUpds, 'machineInventory', $machineInventoryId, {
            'schema'  => $schemaName,
            'code'    => LOC::Log::EventType::CHANGECONTRACT,
            'old'     => $oldContractId,
            'new'     => $newContractId,
            'content' => $oldContractId . ' → ' . $newContractId
        });

        if ($tabInfos->{'isContractCodeOk'})
        {
            $tabUpdates->{'min_is_contractcodeok'} = 0;
        }
        else
        {
            # Vérification du contrat initialement saisi
            my $originalContractId = &_searchContractIdByCode($db, $tabInfos->{'contractCode'});
            if ($originalContractId && $originalContractId == $newContractId)
            {
                $tabUpdates->{'min_is_contractcodeok'} = 1;
            }
        }
    }

    # - Status
    if ($oldStatus != $newStatus)
    {
        $tabUpdates->{'min_status'} = $newStatus;
    }

    # - Agence
    if ($oldAgencyId ne $newAgencyId)
    {
        $tabUpdates->{'min_agc_id'} = $newAgencyId;
    }

    if (keys %$tabUpdates > 0)
    {
        if ($db->update('f_machineinventory', $tabUpdates,
                        {'min_id*' => $machineInventoryId}, $schemaName) == -1)
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Avec demande d'attachement
    if ($tabOptions->{'attach'})
    {
        # Vérification de la possibilité d'attacher
        if ($newStatus != 0)
        {
            # Association contrat/machine incohérente
            return &$rollBackAndError(__LINE__, ERROR_INVALIDCOUPLE);
        }
        # Rattachement
        if (!&_attach($db, $countryId, {
                'id'                    => $machineInventoryId,
                'agency.id'             => $newAgencyId,
                'type'                  => $tabInfos->{'type'},
                'machine.id'            => $newMachineId,
                'machine.tabInfos'      => $tabMachineInfos,
                'contract.id'           => $newContractId,
                'contract.tabInfos'     => $tabContractInfos,
                'attachedToContract.id' => $tabInfos->{'attachedToContract.id'},
                'nbPhotos'              => $tabInfos->{'nbPhotos'},
                'status'                => $newStatus,
                'date'                  => $tabInfos->{'date'}
            }, $userId, $tabEndUpds))
        {
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: delete
# Supprime un état des lieux
#
# Parameters:
# string  $countryId          - Identifiant du pays
# int     $machineInventoryId - Identifiant de l'état des lieux
# int     $userId             - Identifiant de l'utilisateur responsable
# hashref $tabOptions         - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors          - Tableau d'erreurs
# hashref $tabEndUpds         - Mises à jour de fin
#
# Returns:
# int - 1 si c'est un succès, 0 sinon
sub delete
{
    my ($countryId, $machineInventoryId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'delete', $_[0], __FILE__, '');
        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }
        return 0;
    };

    # Récupération des informations de l'état des lieux
    my $tabInfos = &getInfos($countryId, $machineInventoryId, GETINFOS_BASE | GETINFOS_RIGHTS | GETINFOS_POSSIBILITIES,
                                {'writeDbLock' => 1});
    if (!$tabInfos)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Vérifications Droits
    my $tabRights = $tabInfos->{'tabRights'};

    if (!$tabRights->{'actions'}->{'delete'})
    {
        return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
    }

    # - Possibilités
    my $tabPossibilities = $tabInfos->{'tabPossibilities'};
    if (!$tabPossibilities->{'actions'}->{'delete'})
    {
        if ($tabInfos->{'state.id'} eq STATE_ATTACHED)
        {
            # Erreur état des lieux rattaché
            return &$rollBackAndError(__LINE__, ERROR_ATTACHED);
        }
        # Erreur fatale
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }


    # Mise à jour
    my $tabUpdates = {
        'min_sta_id'         => STATE_DELETED,
        'min_date_deletion*' => 'NOW()'
    };

    if ($db->update('f_machineinventory', $tabUpdates,
                    {'min_id*' => $machineInventoryId}, $schemaName) == -1)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Décrémentation de l'alerte des états des lieux à traiter
    my $alertAgencyId = $tabInfos->{'agency.id'} || &LOC::Country::getMainAgencyId($countryId);
    &LOC::EndUpdates::updateAlert($tabEndUpds, $alertAgencyId, LOC::Alert::MACHINEINVENTORIES, -1);

    # Trace de suppression
    &LOC::EndUpdates::addTrace($tabEndUpds, 'machineInventory', $machineInventoryId, {
        'schema'  => $schemaName,
        'code'    => LOC::Log::EventType::DELETE,
        'old'     => undef,
        'new'     => undef,
        'content' => ''
    });

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: attach
# Rattache un état des lieux à son contrat
#
# Parameters:
# string  $countryId          - Identifiant du pays
# int     $machineInventoryId - Identifiant de l'état des lieux
# int     $userId             - Identifiant de l'utilisateur responsable
# hashref $tabOptions         - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors          - Tableau d'erreurs
# hashref $tabEndUpds         - Mises à jour de fin
#
# Returns:
# int - 1 si c'est un succès, 0 sinon
sub attach
{
    my ($countryId, $machineInventoryId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'attach', $_[0], __FILE__, '');
        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }
        return 0;
    };

    # Récupération des informations de l'état des lieux
    my $tabInfos = &getInfos($countryId, $machineInventoryId,
                             GETINFOS_BASE |
                             GETINFOS_MACHINE |
                             GETINFOS_CONTRACT |
                             GETINFOS_RIGHTS |
                             GETINFOS_POSSIBILITIES,
                             {'writeDbLock' => 1});
    if (!$tabInfos)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Vérifications Droits
    my $tabRights = $tabInfos->{'tabRights'};

    if (!$tabRights->{'actions'}->{'attach'})
    {
        return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
    }

    # - Possibilités
    my $tabPossibilities = $tabInfos->{'tabPossibilities'};
    if (!$tabPossibilities->{'actions'}->{'attach'})
    {
        if ($tabInfos->{'state.id'} eq STATE_ATTACHED)
        {
            # Erreur état des lieux déjà attaché
            push(@{$tabErrors->{'fatal'}}, ERROR_ALREADYATTACHED);
        }
        # Status non valide ?
        if ($tabInfos->{'status'} != 0)
        {
            if ($tabInfos->{'status'} & STATUSFLAG_UNKNOWNMACHINE)
            {
                # Machine inconnue
                push(@{$tabErrors->{'fatal'}}, ERROR_UNKNOWNMACHINE);
            }
            if ($tabInfos->{'status'} & STATUSFLAG_UNKNOWNCONTRACT)
            {
                # Contrat inconnu
                push(@{$tabErrors->{'fatal'}}, ERROR_UNKNOWNCONTRACT);
            }
            if ($tabInfos->{'status'} & STATUSFLAG_INVALIDCOUPLE)
            {
                # Association contrat/machine incohérente
                push(@{$tabErrors->{'fatal'}}, ERROR_INVALIDCOUPLE);
            }
        }
        return &$rollBackAndError(__LINE__, (@{$tabErrors->{'fatal'}} == 0 ? ERROR_FATAL : undef));
    }


    # Rattachement
    if (!&_attach($db, $countryId, $tabInfos, $userId, $tabEndUpds))
    {
        # Erreur fatale
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: detach
# Détache un état des lieux de son contrat
#
# Parameters:
# string  $countryId          - Identifiant du pays
# int     $machineInventoryId - Identifiant de l'état des lieux
# int     $userId             - Identifiant de l'utilisateur responsable
# hashref $tabOptions         - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors          - Tableau d'erreurs
# hashref $tabEndUpds         - Mises à jour de fin
#
# Returns:
# int - 1 si c'est un succès, 0 sinon
sub detach
{
    my ($countryId, $machineInventoryId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'detach', $_[0], __FILE__, '');
        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }
        return 0;
    };

    # Récupération des informations de l'état des lieux
    my $tabInfos = &getInfos($countryId, $machineInventoryId, GETINFOS_BASE | GETINFOS_RIGHTS | GETINFOS_POSSIBILITIES,
                                {'writeDbLock' => 1});
    if (!$tabInfos)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Vérifications Droits
    my $tabRights = $tabInfos->{'tabRights'};

    if (!$tabRights->{'actions'}->{'detach'})
    {
        return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
    }

    # - Possibilités
    my $tabPossibilities = $tabInfos->{'tabPossibilities'};
    if (!$tabPossibilities->{'actions'}->{'detach'})
    {
        # L'état des lieux doit être rattaché à un contrat
        if ($tabInfos->{'state.id'} eq STATE_NOTATTACHED)
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_ALREADYDETACHED);
        }
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Détachement
    if (!&_detach($db, $countryId, $tabInfos, $userId, $tabEndUpds))
    {
        # Erreur fatale
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: sendDeliveryMail
# Envoi du mail de notification de la livraison
#
# Parameters:
# string  $countryId          - Identifiant du pays
# int     $machineInventoryId - Identifiant de l'état des lieux
# int     $userId             - Identifiant de l'utilisateur responsable
# hashref $tabOptions         - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors          - Tableau d'erreurs
# hashref $tabEndUpds         - Mises à jour de fin
#
# Returns:
# int - 1 si c'est un succès, 0 sinon
sub sendDeliveryMail
{
    my ($countryId, $machineInventoryId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'sendDeliveryMail', $_[0], __FILE__, '');
        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }
        return 0;
    };

    # Récupération des informations de l'état des lieux
    my $tabInfos = &getInfos($countryId, $machineInventoryId,
                             GETINFOS_BASE |
                             GETINFOS_MACHINE |
                             GETINFOS_CONTRACT |
                             GETINFOS_RIGHTS |
                             GETINFOS_POSSIBILITIES,
                             {'writeDbLock' => 1});
    if (!$tabInfos)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Vérifications Droits
    my $tabRights = $tabInfos->{'tabRights'};

    if (!$tabRights->{'actions'}->{'sendNotification'})
    {
        return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
    }

    # - Possibilités
    my $tabPossibilities = $tabInfos->{'tabPossibilities'};
    if (!$tabPossibilities->{'actions'}->{'sendNotification'})
    {
        if ($tabInfos->{'state.id'} ne STATE_ATTACHED)
        {
            # Etat des lieux non rattaché
            push(@{$tabErrors->{'fatal'}}, ERROR_NOTATTACHED);
        }
        if ($tabInfos->{'contract.tabInfos'}->{'orderContact.email'} eq '')
        {
            # Adresse e-mail contat commande requise
            push(@{$tabErrors->{'fatal'}}, ERROR_EMAILREQUIRED);
        }
        return &$rollBackAndError(__LINE__, (@{$tabErrors->{'fatal'}} == 0 ? ERROR_FATAL : undef));
    }


    # Envoi du mail
    if (!&_sendDeliveryMail($countryId, $tabInfos, $userId, $tabEndUpds))
    {
        # Erreur fatale
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: onExternalChanges
# Exécute toutes les mises à jour nécessaires lors de la modification d'une entitée externe
#
# Parameters:
# string  $countryId  - Pays
# hashref $tabChanges - Informations sur les mises à jour des entitées externes
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub onExternalChanges
{
    my ($countryId, $tabChanges, $userId, $tabErrors, $tabEndUpds) = @_;

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'onExternalChanges', $_[0], __FILE__, '');
        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }
        return 0;
    };

    my %tabToRecalculate = ();

    # Impacts des contrats sur les états des lieux
    # ---------------------------------------------
    if (exists $tabChanges->{'contract'})
    {
        my $contractId = $tabChanges->{'contract'}->{'id'};

        my $tabProps = $tabChanges->{'contract'}->{'props'};
        if (keys %$tabProps > 0)
        {
            if ($tabProps->{'machine.id'})
            {
                # Récupération des états des lieux liés à ce contrat
                my $tab = &getList($countryId, LOC::Util::GETLIST_IDS, {
                    'contractId' => $contractId
                });

                foreach my $machineInventoryId (@$tab)
                {
                    $tabToRecalculate{$machineInventoryId} = 1;
                }
            }
        }
    }

    # Renouvellement des transports
    foreach my $machineInventoryId (keys %tabToRecalculate)
    {
        if (!&_recalculate($db, $countryId, $machineInventoryId, $userId, $tabEndUpds))
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }


    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: _sendDeliveryMail
# Envoi le mail notification de livraison au client
#
# Parameters:
# string  $countryId          - Identifiant du pays
# int     $machineInventoryId - Liste des destinataires au format suivant : id contrat => numéro de téléphone
# int     $userId             - Utilisateur à l'origine du sms
# hashref $tabEndUpds         - Mises à jour de fin
#
# Returns:
# bool -
sub _sendDeliveryMail
{
    my ($countryId, $tabInfos, $userId, $tabEndUpds) = @_;

    # Destinataires
    my @tabReceivers = ();
    my $orderContactEmail = $tabInfos->{'contract.tabInfos'}->{'orderContact.email'};
    if ($orderContactEmail ne '')
    {
        push(@tabReceivers, $orderContactEmail);
    }
    my $siteContactEmail = $tabInfos->{'contract.tabInfos'}->{'site.contact.email'};
    if ($siteContactEmail ne '')
    {
        push(@tabReceivers, $siteContactEmail);
    }

    if (@tabReceivers == 0)
    {
        return 1;
    }

    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Informations de l'agence du contrat
    my $tabAgencyInfos = &LOC::Agency::getInfos($tabInfos->{'contract.tabInfos'}->{'agency.id'});

    # Signature du mail
    my $signTemplate;
    my $tabSignReplaces = {};
    my %tabSender;
    if ($tabAgencyInfos->{'isTransportPlatform'})
    {
        # Récupération de la signature plateforme transport
        $signTemplate = &LOC::Util::getFileContent(&LOC::Globals::get('cgiPath') . '/cfg/templates/' . $countryId . '/signatures/transport.html');

        my $senderName = sprintf('%s - %s', $tabAgencyInfos->{'country.subsidiary'}, 'Plateforme Transport');

        %tabSender = ($tabAgencyInfos->{'email.transp'} => $senderName);
    }
    else
    {
        # Récupération de la signature agence
        $signTemplate = &LOC::Util::getFileContent(&LOC::Globals::get('cgiPath') . '/cfg/templates/' . $countryId . '/signatures/agency.html');

        $tabSignReplaces = {
            'user.firstName' => '',
            'user.name'      => '',
            'label'          => $tabAgencyInfos->{'label'},
            'address'        => $tabAgencyInfos->{'address'},
            'postalCode'     => $tabAgencyInfos->{'postalCode'},
            'city'           => $tabAgencyInfos->{'city'},
            'telephone'      => $tabAgencyInfos->{'telephone'},
            'fax'            => $tabAgencyInfos->{'fax'}
        };

        my $senderName = sprintf('%s - %s', $tabAgencyInfos->{'country.subsidiary'}, $tabAgencyInfos->{'label'});

        %tabSender = ($tabAgencyInfos->{'email.contact'} => $senderName);
    }

    # Formatage et remplacement des données dans le template
    $signTemplate =~ s/(\<%(([a-z]{2,3})\:)?(.*?)\>)/@{[&_formatTemplate($3, $4, $tabSignReplaces)]}/g;

    # Expéditeur

    # Construction du Mail
    my $template = &LOC::Util::getFileContent(&LOC::Globals::get('cgiPath') . '/cfg/templates/' . $countryId . '/machineInventory/deliveryMail.html');

    my $tabReplaces = {
        'machine.parkNumber'  => $tabInfos->{'machine.tabInfos'}->{'parkNumber'},
        'date'                => $tabInfos->{'date'},
        'contract.code'       => $tabInfos->{'contract.tabInfos'}->{'code'},
        'site.label'          => $tabInfos->{'contract.tabInfos'}->{'site.label'},
        'site.address'        => $tabInfos->{'contract.tabInfos'}->{'site.address'},
        'site.locality.label' => $tabInfos->{'contract.tabInfos'}->{'site.locality.label'},
        'signature'           => $signTemplate
    };

    # Formatage et remplacement des données dans le template
    $template =~ s/(\<%(([a-z]{2,3})\:)?(.*?)\>)/@{[&_formatTemplate($3, $4, $tabReplaces)]}/g;

    # Création d'un mail
    my $mailObj = LOC::Mail->new();

    # Encodage
    $mailObj->setEncodingType(LOC::Mail::ENCODING_TYPE_UTF8);

    # Expéditeur
    $mailObj->setSender(%tabSender);

    # Formatage et remplacement des données dans le sujet
    my $subject = ($template =~ m/\<title\>(.*?)\<\/title\>/ ? $1 : '');

    if ($template =~ /"cid:(.+)"/ && -e &LOC::Globals::get('imgPath') . '/print/gui/' . $1)
    {
        $mailObj->addLocalFileAttachment(&LOC::Globals::get('imgPath') . '/print/gui/' . $1, $1);
    }

    $mailObj->setBodyType(LOC::Mail::BODY_TYPE_HTML);

    my %tabRecipient;
    my $templatePlus = '';

    my $recipientEmail = join(', ', @tabReceivers);
    if (&LOC::Globals::getEnv() ne 'exp')
    {
        $subject = '[TEST] ' . $subject;

        # Informations sur l'utilisateur
        my $tabUserInfos = &LOC::User::getInfos($userId);

        %tabRecipient = ($tabUserInfos->{'email'} => $tabUserInfos->{'firstName'} . ' ' . $tabUserInfos->{'name'});

        # Destinataires d'origines
        $templatePlus  = 'Destinataire d\'origine : <br/>' . $recipientEmail . '<br />';
    }
    else
    {
        foreach my $recipient (@tabReceivers)
        {
            $tabRecipient{$recipient} = '';
        }
    }

    $mailObj->setBody($templatePlus . $template);

    $mailObj->addRecipients(%tabRecipient);

    $mailObj->setSubject($subject);

    # Ajout des pièces jointes
    my $tabPhotos = &getPhotosList($countryId, $tabInfos->{'id'}, {}, {
        'getContents' => {
            'file' => 1
        }
    });

    foreach my $tabPhoto (@$tabPhotos)
    {
        $mailObj->addStringAttachment($tabPhoto->{'fileContent'}, $tabPhoto->{'fileName'});
    }


    # Envoi du Mail à la fin des traitements
    &LOC::EndUpdates::addAction($tabEndUpds, sub {
            $mailObj->send();
        },
        {'category' => 'sendDeliveryMail',
         'priority' => -1000,
         'id'       => 'LOC::MachineInventory::sendDeliveryMail-id:' . $tabInfos->{'id'}}
    );

    # Ajout de la trace du Mail
    &LOC::EndUpdates::addTrace($tabEndUpds, 'machineInventory', $tabInfos->{'id'}, {
        'schema'  => $schemaName,
        'code'    => LOC::Log::EventType::SENDMAIL,
        'old'     => undef,
        'new'     => $recipientEmail,
        'content' => $recipientEmail
    });

    return 1;
}


# Function: _sendDeliverySms
# Envoi une notification de livraison au client par SMS
#
# Parameters:
# string  $countryId          - Identifiant du pays
# int     $machineInventoryId - Liste des destinataires au format suivant : id contrat => numéro de téléphone
# int     $userId             - Utilisateur à l'origine du sms
# hashref $tabEndUpds         - Mises à jour de fin
#
# Returns:
# bool -
sub _sendDeliverySms
{
    my ($countryId, $tabInfos, $userId, $tabEndUpds) = @_;

    # Destinataires
    my @tabReceivers = ();
    my $siteContactTel = $tabInfos->{'contract.tabInfos'}->{'site.contact.telephone'};
    if ($siteContactTel ne '')
    {
        push(@tabReceivers, {
            'fullName'  => $tabInfos->{'contract.tabInfos'}->{'site.contact.fullName'},
            'telephone' => $siteContactTel
        });
    }
    my $orderContactTel = $tabInfos->{'contract.tabInfos'}->{'orderContact.telephone'};
    if ($orderContactTel ne '' && $orderContactTel ne $siteContactTel)
    {
        push(@tabReceivers, {
            'fullName'  => $tabInfos->{'contract.tabInfos'}->{'orderContact.fullName'},
            'telephone' => $orderContactTel
        });
    }

    if (@tabReceivers == 0)
    {
        return 1;
    }

    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Construction du SMS
    my $template = &LOC::Util::getFileContent(&LOC::Globals::get('cgiPath') . '/cfg/templates/' . $countryId . '/machineInventory/deliverySms.txt');

    my $tabReplaces = {
        'machine.parkNumber' => $tabInfos->{'machine.tabInfos'}->{'parkNumber'},
        'date'               => $tabInfos->{'date'},
        'site.label'         => $tabInfos->{'contract.tabInfos'}->{'site.label'},
        'site.locality.name' => $tabInfos->{'contract.tabInfos'}->{'site.locality.name'}, 
        'nbPhotos'           => $tabInfos->{'nbPhotos'}
    };

    # Formatage et remplacement des données dans le template
    $template =~ s/(\<%(([a-z]{2,3})\:)?(.*?)\>)/@{[&_formatTemplate($3, $4, $tabReplaces)]}/g;


    foreach my $tabReceiver (@tabReceivers)
    {
        # Envoi du SMS à la fin des traitements
        &LOC::EndUpdates::addAction($tabEndUpds, sub {
                &LOC::Sms::send($template, [$tabReceiver->{'telephone'}]);
            },
            {'category' => 'sendDeliverySms',
             'priority' => -1000,
             'id'       => 'LOC::MachineInventory::sendDeliverySms-id:' . $tabInfos->{'id'}}
        );

        # Ajout de la trace du SMS
        &LOC::EndUpdates::addTrace($tabEndUpds, 'machineInventory', $tabInfos->{'id'}, {
            'schema'  => $schemaName,
            'code'    => LOC::Log::EventType::SENDSMS,
            'old'     => undef,
            'new'     => $tabReceiver->{'telephone'},
            'content' => $tabReceiver->{'telephone'} . ($tabReceiver->{'fullName'} ne '' ? ' (' . $tabReceiver->{'fullName'} . ')' : '')
        });
    }

    return 1;
}


# Function: _recalculate
# Recalcule un état des lieux
#
# Parameters:
# LOC::Db::Adapter $db                 - Connexion à la base de données
# string           $countryId          - Pays
# int              $machineInventoryId - Identifiant de l'état des lieux
# int              $userId             - Utilisateur à l'origine du recalcul
# hashref          $tabEndUpds         - Mises à jour de fin
#
# Returns:
# hashref | undef
sub _recalculate
{
    my ($db, $countryId, $machineInventoryId, $userId, $tabEndUpds) = @_;

    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Informations sur l'état des lieux
    my $tabInfos = &getInfos($countryId, $machineInventoryId, GETINFOS_MACHINE | GETINFOS_CONTRACT);

    # Calcul du status
    my $status = &_calculateStatus($tabInfos->{'machine.tabInfos'}, $tabInfos->{'contract.tabInfos'});

    # Détachement de l'état des lieux
    my $tabUpdates = {
        'min_status' => $status
    };

    if ($db->update('f_machineinventory', $tabUpdates,
                    {'min_id*' => $tabInfos->{'id'}}, $schemaName) == -1)
    {
        return 0;
    }

    # Si il y a une erreur détectée alors on détache l'état des lieux
    if ($status != 0 && $tabInfos->{'state.id'} eq STATE_ATTACHED)
    {
        if (!&_detach($db, $countryId, $tabInfos, $userId, $tabEndUpds))
        {
            return 0;
        }
    }

    return 1;
}


# Function: _searchMachineIdByParkNumber
# Recherche de l'identifiant de la machine saisie
#
# Parameters:
# string $countryId  - Pays
# string $parkNumber - Numéro de parc saisi
#
# Returns:
# int | undef
sub _searchMachineIdByParkNumber
{
    my ($countryId, $parkNumber) = @_;

    my $tabFilters = {
        'parkNumber' => $parkNumber
    };
    my $tabMachinesIds = &LOC::Machine::getList($countryId, LOC::Util::GETLIST_IDS, $tabFilters);
    if (!$tabMachinesIds || @$tabMachinesIds == 0)
    {
        return undef;
    }

    return $tabMachinesIds->[0];
}


# Function: _searchContractIdByCode
# Recherche de l'identifiant du contrat saisi
#
# Parameters:
# LOC::Db::Adapter $db           - Connexion à la base de données
# string           $contractCode - Numéro de contrat saisi
#
# Returns:
# int | undef
sub _searchContractIdByCode
{
    my ($db, $contractCode) = @_;

    # Recherche du contrat saisi
    my $query = '
SELECT
    tbl_f_contract.CONTRATAUTO
FROM CONTRAT tbl_f_contract
WHERE tbl_f_contract.CONTRATAUTO = ' . $db->quote($contractCode) . ';';
    my $contractId = $db->fetchOne($query);

    return ($contractId ? $contractId * 1 : undef);
}


# Function: _calculateAgencyId
# Calcul de l'agence responsable
#
# Parameters:
# hashref $tabMachineInfos  - Informations sur la machine
# hashref $tabContractInfos - Informations sur le contrat
#
# Returns:
# string | undef
sub _calculateAgencyId
{
    my ($tabMachineInfos, $tabContractInfos) = @_;

    # Rattachement automatique à un contrat
    if ($tabMachineInfos && $tabContractInfos &&
        (($tabContractInfos->{'state.id'} ne LOC::Contract::Rent::STATE_CANCELED &&
          $tabMachineInfos->{'id'} == $tabContractInfos->{'machine.id'}) ||
         $tabMachineInfos->{'agency.id'} eq $tabContractInfos->{'agency.id'}))
    {
        return $tabContractInfos->{'agency.id'};
    }

    return undef;
}


# Function: _calculateStatus
# Calcul du status
#
# Parameters:
# hashref $tabMachineInfos  - Informations sur la machine
# hashref $tabContractInfos - Informations sur le contrat
#
# Returns:
# int
sub _calculateStatus
{
    my ($tabMachineInfos, $tabContractInfos) = @_;

    my $status = 0;
    if (!$tabMachineInfos)
    {
        $status |= STATUSFLAG_UNKNOWNMACHINE;
    }
    if (!$tabContractInfos)
    {
        $status |= STATUSFLAG_UNKNOWNCONTRACT;
    }

    if ($tabMachineInfos && $tabContractInfos &&
        ($tabContractInfos->{'state.id'} eq LOC::Contract::Rent::STATE_CANCELED ||
         $tabMachineInfos->{'id'} != $tabContractInfos->{'machine.id'}))
    {
        $status |= STATUSFLAG_INVALIDCOUPLE;
    }

    return $status;
}


# Function: _detach
# Détache l'état des lieux
#
# Parameters:
# LOC::Db::Adapter $db         - Connexion à la base de données
# string           $countryId  - Pays
# hashref          $tabInfos   - Informations sur l'état des lieux
# int              $userId     - Utilisateur responsable de la modification
# hashref          $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub _detach
{
    my ($db, $countryId, $tabInfos, $userId, $tabEndUpds) = @_;

    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Détachement de l'état des lieux
    my $tabUpdates = {
        'min_sta_id' => STATE_NOTATTACHED
    };
    if ($db->update('f_machineinventory', $tabUpdates,
                    {'min_id*' => $tabInfos->{'id'}}, $schemaName) == -1)
    {
        return 0;
    }

    # Incrémentation de l'alerte des états des lieux à traiter
    my $alertAgencyId = $tabInfos->{'agency.id'} || &LOC::Country::getMainAgencyId($countryId);
    &LOC::EndUpdates::updateAlert($tabEndUpds, $alertAgencyId, LOC::Alert::MACHINEINVENTORIES, 1);

    # Ajout d'une trace sur l'état des lieux
    &LOC::EndUpdates::addTrace($tabEndUpds, 'machineInventory', $tabInfos->{'id'}, {
        'schema'  => $schemaName,
        'code'    => LOC::Log::EventType::CONTRACTDETACHMENT,
        'old'     => $tabInfos->{'attachedToContract.id'},
        'new'     => undef,
        'content' => $tabInfos->{'attachedToContract.id'}
    });

    # Ajout d'une trace sur le contrat anciennement rattaché
    &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $tabInfos->{'attachedToContract.id'}, {
        'schema'  => $schemaName,
        'code'    => ($tabInfos->{'type'} eq TYPE_DELIVERY ? LOC::Log::EventType::MACHINEINVENTORYDELDETACHMENT : LOC::Log::EventType::MACHINEINVENTORYRECDETACHMENT),
        'old'     => $tabInfos->{'id'},
        'new'     => undef,
        'content' => $tabInfos->{'id'}
    });

    return 1;
}


# Function: _attach
# Rattache l'état des lieux
#
# Parameters:
# LOC::Db::Adapter $db         - Connexion à la base de données
# string           $countryId  - Pays
# hashref          $tabInfos   - Informations sur l'état des lieux
# int              $userId     - Utilisateur responsable de la modification
# hashref          $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub _attach
{
    my ($db, $countryId, $tabInfos, $userId, $tabEndUpds) = @_;

    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Le contrat de rattachement a-t-il changé ?
    my $isContractChanged = ($tabInfos->{'contract.id'} != $tabInfos->{'attachedToContract.id'});

    # Rattachement de l'état des lieux
    my $tabUpdates = {
        'min_rct_id_attachedto' => $tabInfos->{'contract.id'},
        'min_sta_id'            => STATE_ATTACHED
    };
    if ($db->update('f_machineinventory', $tabUpdates,
                    {'min_id*' => $tabInfos->{'id'}}, $schemaName) == -1)
    {
        return 0;
    }

    # Décrémentation de l'alerte des états des lieux à traiter
    my $alertAgencyId = $tabInfos->{'agency.id'} || &LOC::Country::getMainAgencyId($countryId);
    &LOC::EndUpdates::updateAlert($tabEndUpds, $alertAgencyId, LOC::Alert::MACHINEINVENTORIES, -1);

    # Ajout d'une trace sur l'état des lieux
    &LOC::EndUpdates::addTrace($tabEndUpds, 'machineInventory', $tabInfos->{'id'}, {
        'schema'  => $schemaName,
        'code'    => LOC::Log::EventType::CONTRACTATTACHMENT,
        'old'     => undef,
        'new'     => $tabInfos->{'contract.id'},
        'content' => $tabInfos->{'contract.id'}
    });

    # Ajout d'une trace sur le contrat rattaché
    &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $tabInfos->{'contract.id'}, {
        'schema'  => $schemaName,
        'code'    => ($tabInfos->{'type'} eq TYPE_DELIVERY ? LOC::Log::EventType::MACHINEINVENTORYDELATTACHMENT : LOC::Log::EventType::MACHINEINVENTORYRECATTACHMENT),
        'old'     => undef,
        'new'     => $tabInfos->{'id'},
        'content' => $tabInfos->{'id'}
    });

    if ($isContractChanged)
    {
        if ($tabInfos->{'type'} eq TYPE_DELIVERY)
        {
            # Envoi du mail de notification de la livraison
            if (!&_sendDeliveryMail($countryId, $tabInfos, $userId, $tabEndUpds))
            {
                return 0;
            }

            # Envoi du sms de notification de la livraison

            # Délai max pour l'envoi du SMS
            my $delta = &LOC::Characteristic::getCountryValueByCode('DELTASMSEDL', $countryId);

            my $currentDate = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME);
            if (&LOC::Date::getInterval($tabInfos->{'date'}, $currentDate, LOC::Date::UNIT_MINUTES) <= $delta)
            {
                if (!&_sendDeliverySms($countryId, $tabInfos, $userId, $tabEndUpds))
                {
                    return 0;
                }
            }
        }
    }

    return 1;
}


# Function: _getMachineInfos
# Retourne les informations d'une machine
#
# Contenu du hashage :
# - id => identifiant de la machine,
# - state.id => état de la machine,
# - agency.id => agence de la machine,
# - parkNumber => numéro de parc
#
# Parameters:
# string  $countryId - Identifiant du pays
# int     $id        - Id de la machine
#
# Returns:
# hashref|undef - Informations sur la machine
sub _getMachineInfos
{
    my ($countryId, $id) = @_;

    # Récupération des informations de la machine
    my $tabInfos = &LOC::Machine::getInfos($countryId, $id, LOC::Machine::GETINFOS_BASE);
    if (!$tabInfos)
    {
        return undef;
    }

    return {
        'id'         => $tabInfos->{'id'},
        'state.id'   => $tabInfos->{'state.id'},
        'agency.id'  => $tabInfos->{'agency.id'},
        'parkNumber' => $tabInfos->{'parkNumber'}
    };
}


# Function: _getContractInfos
# Retourne les informations d'un contrat
#
# Contenu du hashage :
# - id => identifiant du contrat,
# - code => code du contrat,
# - machine.id => identifiant de la machine attribuée au contrat,
# - customer.name => raison sociale du client,
# - customer.email => adresse mail du client,
# - site.label => libellé chantier,
# - site.address => adresse chantier,
# - site.locality.label => ville chantier,
# - site.contact.fullName => nom du contact chantier,
# - site.contact.telephone => téléphone du contact chantier,
# - orderContact.fullName => nom du contact commande,
# - orderContact.telephone => téléphone du contact commande,
# - agency.id => agence du contrat,
# - state.id => état du contrat
#
# Parameters:
# string  $countryId - Identifiant du pays
# int     $id        - Identifiant du contrat
#
# Returns:
# hashref|undef - Informations sur le contrat
sub _getContractInfos
{
    my ($countryId, $id) = @_;

    # Récupération des informations du contrat
    my $tabInfos = &LOC::Contract::Rent::getInfos($countryId, $id, LOC::Contract::Rent::GETINFOS_BASE |
                                                                   LOC::Contract::Rent::GETINFOS_CUSTOMER |
                                                                   LOC::Contract::Rent::GETINFOS_EXT_SITE);
    if (!$tabInfos)
    {
        return undef;
    }

    return {
        'id'                     => $tabInfos->{'id'},
        'code'                   => $tabInfos->{'code'},
        'machine.id'             => $tabInfos->{'machine.id'},
        'customer.email'         => &LOC::Util::trim($tabInfos->{'customer.email'}),
        'customer.name'          => $tabInfos->{'customer.name'},
        'site.label'             => $tabInfos->{'site.tabInfos'}->{'label'},
        'site.address'           => $tabInfos->{'site.tabInfos'}->{'address'},
        'site.locality.name'     => $tabInfos->{'site.tabInfos'}->{'locality.name'},
        'site.locality.label'    => $tabInfos->{'site.tabInfos'}->{'locality.label'},
        'site.contact.fullName'  => $tabInfos->{'site.tabInfos'}->{'contact.fullName'},
        'site.contact.telephone' => &LOC::Util::trim($tabInfos->{'site.tabInfos'}->{'contact.telephone'}),
        'site.contact.email'     => &LOC::Util::trim($tabInfos->{'site.tabInfos'}->{'contact.email'}),
        'orderContact.fullName'  => $tabInfos->{'orderContact.fullName'},
        'orderContact.telephone' => &LOC::Util::trim($tabInfos->{'orderContact.telephone'}),
        'orderContact.email'     => &LOC::Util::trim($tabInfos->{'orderContact.email'}),
        'agency.id'              => $tabInfos->{'agency.id'},
        'state.id'               => $tabInfos->{'state.id'}
    };
}

# Function: _getPossibilities
# Récupérer les possibilités sur l'état des lieux pour un utilisateur donné
#
# Parameters:
# hashref $tabInfos         - Informations sur l'état des lieux
# hashref $tabContractInfos - Informations sur le contrat lié
#
# Returns:
# hashref - Tableau des droits
sub _getPossibilities
{
    my ($tabInfos, $tabContractInfos) = @_;

    # Possibilités
    my $tabPossibilities = {};
    my $tabComponents = {
        'actions' => ['attach', 'detach', 'update', 'delete', 'sendMail', 'sendNotification']
    };

    # Initialisation des possibilités
    &LOC::Util::setTreeValues($tabPossibilities, $tabComponents, 0);

    if ($tabInfos->{'state.id'} eq STATE_NOTATTACHED)
    {
        # Attacher à un contrat
        if (!$tabInfos->{'status'})
        {
            &LOC::Util::setTreeValues($tabPossibilities, {'actions' => ['attach']}, 1);
        }

        # Mettre à jour
        &LOC::Util::setTreeValues($tabPossibilities, {'actions' => ['update']}, 1);

        # Supprimer
        &LOC::Util::setTreeValues($tabPossibilities, {'actions' => ['delete']}, 1);
    }

    if ($tabInfos->{'state.id'} eq STATE_ATTACHED)
    {
        # Détacher d'un contrat
        &LOC::Util::setTreeValues($tabPossibilities, {'actions' => ['detach', 'sendMail']}, 1);

        # Envoyer le mail au client
        # - Livraison et le client a un e-mail
        if ($tabInfos->{'type'} eq TYPE_DELIVERY && $tabContractInfos && $tabContractInfos->{'orderContact.email'})
        {
            &LOC::Util::setTreeValues($tabPossibilities, {'actions' => ['sendNotification']}, 1);
        }
    }

    return $tabPossibilities;
}


# Function: _getRights
# Récupérer les droits sur l'état des lieux pour un utilisateur donné
#
# Parameters:
# string  $countryId     - Identifiant du pays
# hashref $tabInfos      - Informations sur l'état des lieux
# hashref $tabUserInfos  - Informations sur l'utilisateur
#
# Returns:
# hashref - Tableau des droits
sub _getRights
{
    my ($countryId, $tabInfos, $tabUserInfos) = @_;

    my $agencyId = $tabInfos->{'agency.id'};

    my $isUserAdmin        = $tabUserInfos->{'isAdmin'};
    my $isUserSuperv       = $tabUserInfos->{'isSuperv'};
    my $isUserTech         = &LOC::Util::in_array(LOC::User::GROUP_TECHNICAL, $tabUserInfos->{'tabGroups'});
    my $isUserChefAg       = &LOC::Util::in_array(LOC::User::GROUP_AGENCYMANAGER, $tabUserInfos->{'tabGroups'});
    my $isUserComm         = &LOC::Util::in_array(LOC::User::GROUP_COMMERCIAL, $tabUserInfos->{'tabGroups'});
    my $isUserCompta       = &LOC::Util::in_array(LOC::User::GROUP_ACCOUNTING, $tabUserInfos->{'tabGroups'});
    my $isUserTransp       = &LOC::Util::in_array(LOC::User::GROUP_TRANSPORT, $tabUserInfos->{'tabGroups'});
    my $isUserChefAt       = &LOC::Util::in_array(LOC::User::GROUP_FOREMAN, $tabUserInfos->{'tabGroups'});
    my $isUserRespTech     = &LOC::Util::in_array(LOC::User::GROUP_TECHNICALMANAGER, $tabUserInfos->{'tabGroups'});
    my $isUserTechPlatform = &LOC::Util::in_array(LOC::User::GROUP_TECHNICALPLATFORM, $tabUserInfos->{'tabGroups'});

    my $tabRights = {};
    my $tabComponents = {
        'actions' => ['attach', 'detach', 'update', 'delete', 'sendMail', 'sendNotification']
    };

    # Initialisation des droits
    &LOC::Util::setTreeValues($tabRights, $tabComponents, 0);

    # TRANSP
    if ($isUserTransp || $isUserAdmin)
    {
        my $hasAgency = ((defined $agencyId && exists $tabUserInfos->{'tabAuthorizedAgencies'}->{$agencyId}) ||
                         (!defined $agencyId && &LOC::Util::in_array($countryId, $tabUserInfos->{'tabAuthorizedCountries'})));

        if ($hasAgency || $isUserAdmin)
        {
            &LOC::Util::setTreeValues($tabRights, {'actions' => ['attach', 'detach', 'update', 'delete']}, 1);
        }
        &LOC::Util::setTreeValues($tabRights, {'actions' => ['sendMail', 'sendNotification']}, 1);
    }

    # TECH, CHEFAT et PSTN
    if ($isUserTech || $isUserChefAt || $isUserTechPlatform || $isUserAdmin)
    {
        &LOC::Util::setTreeValues($tabRights, {'actions' => ['sendMail', 'sendNotification',]}, 1);
    }

    # COMM et CHEFAG
    if ($isUserComm || $isUserChefAg || $isUserAdmin)
    {
        &LOC::Util::setTreeValues($tabRights, {'actions' => ['sendMail', 'sendNotification']}, 1);
    }

    return $tabRights;
}


# Function: _formatTemplate
# formate une chaîne de caractères
#
# Params:
# string    $code          - quel formatage à appliquer
# string    $value         - la chaîne template à formater
# hashref   $tabReplaces   - les valeurs de la chaîne à mettre dans le template
#
# Return:
# string - la chaîne formatée
sub _formatTemplate
{
    my ($code, $value, $tabReplaces) = @_;

    if ($code eq 'dte')
    {
        return &LOC::Template::formatDate($value, $tabReplaces);
    }
    elsif ($code eq 'nb')
    {
        return &LOC::Template::formatSingularPlural($value, $tabReplaces);
    }

    return (exists $tabReplaces->{$value} ? $tabReplaces->{$value} : $value);
}


# Function: _lockDbRow
# Verrou sur la ligne de l'état des lieux
#
# Parameters:
# LOC::Db::Adapter $db - Connexion à la base de données
# int              $id - Identifiant de l'état des lieux
#
# Returns:
# bool
sub _lockDbRow
{
    my ($db, $machineInventoryId) = @_;

    # Attente du verrou sur la ligne
    my $query = '
SELECT
    min_id
FROM f_machineinventory
WHERE min_id = ' . $machineInventoryId . '
FOR UPDATE;';

    return ($db->query($query) ? 1 : 0);
}


1;