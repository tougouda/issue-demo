use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Kimoce
# Module de fonctions d'interfaçage avec Kimoce
package LOC::Kimoce;


# Constant: AGOTYPEDSC_TRANSFERT
# Libellé du champ correspondant au transfert dans Kimoce
use constant
{
    AGOTYPEDSC_TRANSFERT => 'Transfert'
};

# Constants: Compteurs
# COUNTER_RECOVERIES     - Compteur de récupérations
# COUNTER_SITESTRANSFERS - Compteur de transferts inter-chantiers
# COUNTER_TIME          - Compteur horaire
use constant
{
    COUNTER_RECOVERIES     => 'CREC',
    COUNTER_SITESTRANSFERS => 'CTRF',
    COUNTER_TIME           => 'CH'
};

# Constants: Caractéristiques
#
use constant
{
    FEAT_SITEADDRESS           => 'ADR',
    FEAT_SITELABEL             => 'CHA',
    FEAT_CUSTOMERLABEL         => 'CLI',
    FEAT_SITECONTACT           => 'CTC',
    FEAT_SITECONTACTTEL        => 'TEL',
    FEAT_SITECITY              => 'VIL',
    FEAT_SITEPOSTALCODE        => 'CP',
    FEAT_SITECOMMENT           => 'COM',
    FEAT_SITEISASBESTOS        => 'AMI',
    FEAT_BREAKDOWNDATE         => 'DTEPA',
    FEAT_CONTRACTBEGINDATE     => 'DTEDC',
    FEAT_CONTRACTENDDATE       => 'DTEFC',
    FEAT_CONTRACTFULLSERVICE   => 'CONFS',
    FEAT_CONTROLDATE           => 'DTEVGP',
    FEAT_LASTAGENCYCHECKINDATE => 'DTEAGC'
};

# Constants: Demandes Kimoce
# REQUEST_STATUS_CLOSED      - Statut clôturée
# REQUEST_TYPE_MAINTENANCE   - Demande d'entretien
# REQUEST_TYPE_SITESTRANSFER - Demande de transfert inter-chantiers
use constant
{
    REQUEST_STATUS_CLOSED      => 5,
    REQUEST_TYPE_MAINTENANCE   => 15,
    REQUEST_TYPE_SITESTRANSFER => 52
};

# Constants: Modes de transaction de la base de données
# DB_TRANSACTION_ENABLED  - Utilise le mode transaction de la base de données
# DB_TRANSACTION_DISABLED - Désactive le mode transaction de la base de données
use constant
{
    DB_TRANSACTION_DISABLED => 0,
    DB_TRANSACTION_ENABLED  => 1
};

# Constants: États des informations de machine
# MACHINEINFO_STATE_ACTIVE  - Actif
# MACHINEINFO_STATE_DELETED - Supprimé
use constant
{
    MACHINEINFO_STATE_ACTIVE  => 'GEN01',
    MACHINEINFO_STATE_DELETED => 'GEN03'
};

# Constants: Types de caractéristiques
# CHARTYPE_BOOLEAN - Booléen
use constant
{
    CHARTYPE_BOOLEAN => 'O'
};

# Constants: Paramétrage des caractéristiques
# FEATURE_MAXLENGTH - Longueur maximale d'une caractéristique
use constant
{
    FEATURE_MAXLENGTH => 50
};

# Constants: Code interne des dossiers dans Kimoce
# DOSFOLDER_AIRENT - Code interne du dossier AI Location dans Kimoce
use constant
{
    DOSFOLDER_AIRENT => 3
};

# Constants: Mode de mise à jour
# UPDATEMODE_WS - Mise à jour par webservice
# UPDATEMODE_DB - Mise à jour par requête
use constant
{
    UPDATEMODE_WS => 'ws',
    UPDATEMODE_DB => 'db'
};


use strict;

# Modules internes
use LOC::Util;
use LOC::Db;
use LOC::Agency;
use LOC::Log;
use LOC::Machine;
use LOC::ForkProcess;
use LOC::Xml;
use LOC::Kimoce::Soap;
use Data::UUID;
use Tie::IxHash;

# Formats des compteurs
my %tabCountersFormats = (
    COUNTER_RECOVERIES()     => '%d',
    COUNTER_SITESTRANSFERS() => '%d',
    COUNTER_TIME()           => '%.1f'
);


# Function: setMachineSituation
# Met à jour la situation (état) de la machine
#
# Parameters:
# string $parkNumber - Numéro de parc de la machine
# string $etcode     - Code de l'état de la machine
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, 0 sinon
sub setMachineSituation
{
    my ($parkNumber, $etcode) = @_;

    if (!&LOC::Globals::get('connectToKimoce'))
    {
        return 1;
    }

    $parkNumber = &_formatParkNumber($parkNumber);

    # connexion à Kimoce
    my $kimoceDb = &LOC::Db::getConnection('kimoce');
    my $hasTransaction = $kimoceDb->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $kimoceDb->beginTransaction();
    }

    # récupération du numéro interne de la machine
    my $dosInCde = &getMachineInternalId($parkNumber);
    if (!$dosInCde)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $kimoceDb->rollBack();
        }
        return 0;
    }

    # récupération du code situation Kimoce correspondant au code état de la machine
    my $objSitInCde = &getMachineSituationByCode($etcode);
    if (!$objSitInCde)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $kimoceDb->rollBack();
        }
        return 0;
    }

    # mise à jour de la situation de la machine dans p_dos
    my $result3 = $kimoceDb->update('p_dos', {'ObjSitInCde' => $objSitInCde},
                                             {'DosInCde' => $dosInCde});
    if ($result3 == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $kimoceDb->rollBack();
        }
        &LOC::Log::writeFileLog('kimoce', $kimoceDb->getLastQuery() . ' - ' . $kimoceDb->getErrorMsg(), undef,
                                LOC::Log::MODE_SEND_MAIL);
        return 0;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$kimoceDb->commit())
        {
            $kimoceDb->rollBack();
            return 0;
        }
    }
    return 1;
}

# Function: syncMachineFeatures
# Met à jour toutes les caractéristiques de la machine voulue dans Kimoce
#
# Caractéristiques concernées :
# - ADR : adresse du chantier où se trouve la machine;
# - CHA : libellé du chantier où se trouve la machine;
# - CLI : raison sociale du client qui loue la machine;
# - CTC : nom du contact du chantier;
# - TEL : numéro de téléphone du contact du chantier;
# - VIL : nom de la ville du chantier;
# - CP : code postal du chantier;
# - COM : commentaire Informations transport
# - AMI : présence d'amiante sur le chantier
# - DTEPA : Date de la panne de la machine (format YYYYMMDD)
# - DTEVGP : Date de la dernière VGP
# - DTEDC : Date de début du contrat
# - DTEFC : Date de fin du contrat
#
#
# Parameters:
# string $parkNumber - Numéro de parc de la machine
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, 0 sinon
sub syncMachineFeatures
{
    my ($parkNumber) = @_;

    if (!&LOC::Globals::get('connectToKimoce'))
    {
        return 1;
    }

    $parkNumber = &_formatParkNumber($parkNumber);

    # Traitement dans le processus fils
    &LOC::ForkProcess::fork(sub
        {
            # Connexion à auth
            my $authConn = &LOC::Db::getConnection('auth');

            # Récupération des informations de la machine
            my $query = '
SELECT
    tbl_f_machine.MAAUTO AS `id`,
    agc_cty_id AS `country.id`,
    tbl_f_machine.ETCODE AS `state.id`,
    DATE_FORMAT(tbl_f_machine.MADATEPANNE, ' . $authConn->quote('%d/%m/%Y') . ') AS `crashDate`,
    tbl_f_machine.CONTRATAUTO AS `contract.id`
FROM MACHINE tbl_f_machine
LEFT JOIN frmwrk.p_agency
ON tbl_f_machine.AGAUTO = agc_id
WHERE tbl_f_machine.MANOPARC = ' . $authConn->quote($parkNumber) . ';';
            my $tabInfos = $authConn->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
            if (!$tabInfos)
            {
                return 0;
            }
            # Récupération d'informations complémentaires
            # - Date de dernier contrôle
            my $query = '
SELECT
    DATE_FORMAT(COMADAPAVE, ' . $authConn->quote('%d/%m/%Y') . ') AS `controlDate`
FROM LOCATION' . ($tabInfos->{'country.id'} ne 'FR' ? $tabInfos->{'country.id'} : '') . '.CONTROLEMACHINE
WHERE MAAUTO = ' . $tabInfos->{'id'} . ';';

            $tabInfos->{'controlDate'} = $authConn->fetchOne($query);

            # - Date de dernier passage agence
            $tabInfos->{'agencyCheckInDate'} = &LOC::Machine::getLastAgencyCheckIn($tabInfos->{'country.id'}, $tabInfos->{'id'});
            # -- Formatage de la date
            my @tabDateInfos = split(/-/, $tabInfos->{'agencyCheckInDate'});


            my $id                = $tabInfos->{'id'};
            my $countryId         = $tabInfos->{'country.id'};
            my $stateId           = $tabInfos->{'state.id'};
            my $crashDate         = $tabInfos->{'crashDate'};
            my $controlDate       = $tabInfos->{'controlDate'};
            my $agencyCheckInDate = sprintf('%02d/%02d/%04d', $tabDateInfos[2], $tabDateInfos[1], $tabDateInfos[0]);
            my $contractId        = $tabInfos->{'contract.id'};


            # Si la machine est en livraison, louée ou en récupération, on met à jour les informations du chantier
            my $tabFeatures = {};
            if ($contractId ne '' &&
                ($stateId eq LOC::Machine::STATE_DELIVERY ||
                 $stateId eq LOC::Machine::STATE_RENTED ||
                 $stateId eq LOC::Machine::STATE_RECOVERY))
            {
                # Connexion à location du pays concerné
                my $locationConn = &LOC::Db::getConnection('location', $countryId);

                $query = '
SELECT
    CHADRESSE AS `' . FEAT_SITEADDRESS . '`,
    CHLIBELLE AS `' . FEAT_SITELABEL . '`,
    CLSTE AS `' . FEAT_CUSTOMERLABEL . '`,
    CHCONTACT AS `' . FEAT_SITECONTACT . '`,
    CHTELEPHONE AS `' . FEAT_SITECONTACTTEL . '`,
    VINOM AS `' . FEAT_SITECITY . '`,
    VICP AS `' . FEAT_SITEPOSTALCODE . '`,
    CHCOMMENTAIRE AS `' . FEAT_SITECOMMENT . '`,
    IF (CHAMIANTE != 0, ' . $locationConn->quote('Oui') . ', ' . $locationConn->quote('Non') . ') AS `'. FEAT_SITEISASBESTOS . '`,
    DATE_FORMAT(CONTRATDD, ' . $locationConn->quote('%d/%m/%Y') . ') AS `' . FEAT_CONTRACTBEGINDATE . '`,
    DATE_FORMAT(CONTRATDR, ' . $locationConn->quote('%d/%m/%Y') . ') AS `' . FEAT_CONTRACTENDDATE . '`,
    IF (CONTRATFULLSERVICE != 0, ' . $locationConn->quote('Oui') . ', ' . $locationConn->quote('Non') . ') AS `' . FEAT_CONTRACTFULLSERVICE . '`
FROM CONTRAT
LEFT JOIN CHANTIER
ON CHANTIER.CHAUTO = CONTRAT.CHAUTO
LEFT JOIN CLIENT
ON CLIENT.CLAUTO = CONTRAT.CLAUTO
LEFT JOIN VILLECHA
ON VILLECHA.VILLEAUTO = CHANTIER.VILLEAUTO
WHERE CONTRAT.CONTRATAUTO = ' . $contractId . ';';
                $tabFeatures = $locationConn->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
                if (!$tabFeatures)
                {
                    return 0;
                }

                # Formatage commentaire chantier
                $tabFeatures->{FEAT_SITECOMMENT()} = &LOC::Util::substr_ellipsis($tabFeatures->{FEAT_SITECOMMENT()}, FEATURE_MAXLENGTH);
            }
            else
            {
                # On vide ses caractéristiques
                $tabFeatures = {
                    FEAT_SITEADDRESS()         => '',
                    FEAT_SITELABEL()           => '',
                    FEAT_CUSTOMERLABEL()       => '',
                    FEAT_SITECONTACT()         => '',
                    FEAT_SITECONTACTTEL()      => '',
                    FEAT_SITECITY()            => '',
                    FEAT_SITEPOSTALCODE()      => '',
                    FEAT_SITECOMMENT()         => '',
                    FEAT_SITEISASBESTOS()      => 'Non',
                    FEAT_CONTRACTBEGINDATE()   => '',
                    FEAT_CONTRACTENDDATE()     => '',
                    FEAT_CONTRACTFULLSERVICE() => ''
                };
            }

            # On met à jour systématiquement la date de dernière panne
            $tabFeatures->{FEAT_BREAKDOWNDATE()} = $crashDate;
            # On met à jour systématiquement la date de dernière VGP
            $tabFeatures->{FEAT_CONTROLDATE()} = $controlDate;
            # On met à jour systématiquement la date de dernier passage agence
            $tabFeatures->{FEAT_LASTAGENCYCHECKINDATE()} = $agencyCheckInDate;

            # Mise à jour des caractéristiques
            if (!&_setMachineFeatures($parkNumber, $tabFeatures, UPDATEMODE_DB))
            {
                return 0;
            }

            return 1;
        }
    , 1);

    return 1;
}

# Function: setMachineLocalization
# Met à jour la localisation (agence de rattachement) de la machine voulue dans Kimoce
#
# Parameters:
# string $parkNumber - Numéro de parc de la machine
# string $agency     - Agence de localisation de la machine
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, 0 sinon
sub setMachineLocalization
{
    my ($parkNumber, $agency) = @_;

    if (!&LOC::Globals::get('connectToKimoce'))
    {
        return 1;
    }

    $parkNumber = &_formatParkNumber($parkNumber);

    # connexion à Kimoce
    my $kimoceDb = &LOC::Db::getConnection('kimoce');
    my $hasTransaction = $kimoceDb->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $kimoceDb->beginTransaction();
    }

    # récupération du numéro interne de la machine
    my $dosInCde = &getMachineInternalId($parkNumber);
    if (!$dosInCde)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $kimoceDb->rollBack();
        }
        return 0;
    }

    # mise à jour de l'historique de la position géographique de la machine
    my $result2 = &updateMachineGeoHistory($parkNumber, $agency, DB_TRANSACTION_DISABLED);
    if (!$result2)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $kimoceDb->rollBack();
        }
        return 0;
    }

    # récupération des codes kimoce de l'agence
    my %hashAgency = &LOC::Agency::getInfos($agency, LOC::Agency::GETINFOS_ALL);
    if (!%hashAgency)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $kimoceDb->rollBack();
        }
        return 0;
    }
    my $cpyInCde = $hashAgency{'CpyInCde'};
    my $cpyAddrInCde = $hashAgency{'CpyAddrInCde'};

    # mise à jour de la localisation de la machine dans p_dosloc
    my $result3 = $kimoceDb->update('p_dosloc', {'CpyInCde' => $cpyInCde, 'CpyAddrInCde' => $cpyAddrInCde},
                                                {'DosInCde' => $dosInCde});
    if ($result3 == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $kimoceDb->rollBack();
        }
        &LOC::Log::writeFileLog('kimoce', $kimoceDb->getLastQuery() . ' - ' . $kimoceDb->getErrorMsg(), undef,
                                LOC::Log::MODE_SEND_MAIL);
        return 0;
    }

    # mise à jour de la localisation de la machine dans p_dos
    my $result4 = $kimoceDb->update('p_dos', {'CpyInCde' => $cpyInCde, 'CpyAddrInCde' => $cpyAddrInCde},
                                             {'DosInCde' => $dosInCde});
    if ($result4 == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $kimoceDb->rollBack();
        }
        &LOC::Log::writeFileLog('kimoce', $kimoceDb->getLastQuery() . ' - ' . $kimoceDb->getErrorMsg(), undef,
                                LOC::Log::MODE_SEND_MAIL);
        return 0;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$kimoceDb->commit())
        {
            $kimoceDb->rollBack();
            return 0;
        }
    }

    return 1;
}

# Function: updateMachineCounters
# Met à jour les compteurs demandés de la machine voulue dans Kimoce
#
# Parameters:
# string  $parkNumber  - Parc de la machine
# hashref $tabCounters - Compteurs à mettre à jour
#
# Returns :
# bool
sub updateMachineCounters
{
    my ($parkNumber, $tabCounters) = @_;

    if (!&LOC::Globals::get('connectToKimoce'))
    {
        return 1;
    }

    $parkNumber = &_formatParkNumber($parkNumber);

    &LOC::ForkProcess::fork(sub
        {
            # Vérification
            # connexion à Kimoce
            my $kimoceDb = &LOC::Db::getConnection('kimoce');
            my $hasTransaction = $kimoceDb->hasTransaction();

            # Démarre la transaction
            if (!$hasTransaction)
            {
                $kimoceDb->beginTransaction();
            }

            my @tabCountersId = keys(%$tabCounters);
            my $tabOldFeatures = &_getMachineFeatures($parkNumber, \@tabCountersId);

            my $tabFeatures = {};

            foreach my $counterId (keys(%$tabCounters))
            {
                if (&LOC::Util::in_array($counterId, [COUNTER_RECOVERIES, COUNTER_SITESTRANSFERS]))
                {
                    # Vérification de l'existence d'une demande de type "Entretien" ou "Transfert inter-chantiers"
                    my $requestType = ($counterId eq COUNTER_RECOVERIES ? REQUEST_TYPE_MAINTENANCE : REQUEST_TYPE_SITESTRANSFER);
                    my $isRequestExists = &_isMachineRequestExists($parkNumber, $requestType);
                    if (defined $isRequestExists && !$isRequestExists)
                    {
                        # Récupération des anciennes valeurs
                        $tabFeatures->{$counterId} = sprintf($tabCountersFormats{$counterId}, $tabOldFeatures->{$counterId} + $tabCounters->{$counterId});
                    }
                }
                else
                {
                    $tabFeatures->{$counterId} = sprintf($tabCountersFormats{$counterId}, $tabCounters->{$counterId});
                }
            }

            # Mise à jour des valeurs des compteurs
            my $result = &_setMachineFeatures($parkNumber, $tabFeatures, UPDATEMODE_WS);
            if (!$result)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $kimoceDb->rollBack();
                }
                return 0;
            }

            # Valide la transaction
            if (!$hasTransaction)
            {
                if (!$kimoceDb->commit())
                {
                    $kimoceDb->rollBack();
                    return 0;
                }
            }

            return 1;
        }
    , 1);

    return 1;
}


# Function: getMachineInfos
# Retourne les informations sur la localisation de la machine dans Kimoce
#
# Contenu du tableau :
# - 'DosInCde'      => DosInCde, identifiant interne de la machine;
# - 'ObjInCde'      => ObjInCde identifiant de l'objet de la machine;
# - 'CpyInCde'      => CpyInCde;
# - 'CpyAddrInCde'  => CpyAddrInCde;
# - 'CpyTrdNamDsc'  => CpyTrdNamDsc;
# - 'CpyAddrExCde'  => CpyAddrExCde;
# - 'ObjSitDsc'     => Situation;
# - 'CpyExCde'      => CpyExCde;
#
# Parameters:
# string $parkNumber - Le numéro de parc de la machine
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getMachineInfos
{
    my ($parkNumber) = @_;

    if (!&LOC::Globals::get('connectToKimoce'))
    {
        return undef;
    }

    $parkNumber = &_formatParkNumber($parkNumber);

    # connexion à Kimoce
    my $kimoceDb = &LOC::Db::getConnection('kimoce');

    my $query = '
SELECT
    p.DosInCde,
    p.ObjInCde,
    d.CpyInCde,
    d.CpyAddrInCde,
    c.CpyTrdNamDsc,
    ca.CpyAddrExCde,
    r.ObjSitDsc,
    cp.CpyExCde
FROM p_dos p
LEFT JOIN p_dosloc d
ON d.DosLocInCde = p.DosLocInCde
LEFT JOIN p_cpy c
ON c.CpyInCde = d.CpyInCde
LEFT JOIN p_cpyaddr ca
ON ca.CpyAddrInCde = d.CpyAddrInCde AND ca.CpyInCde = d.CpyInCde
LEFT JOIN p_cpy cp
ON cp.CpyInCde = ca.CpyInCde
LEFT JOIN r_objsit r
ON r.ObjSitInCde = p.ObjSitInCde
WHERE p.DosFolderInCde = ' . DOSFOLDER_AIRENT . '
AND p.ObjIdentVal = ' . $kimoceDb->quote($parkNumber) . ';';

    my $result = $kimoceDb->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if (!$result)
    {
        return 0;
    }

    return (wantarray ? %$result : $result);
}


# Function: getMachineExternalId
# Retourne l'identifiant interne de la machine dans Kimoce
#
# Parameters:
# string $parkNumber - Le numéro de parc de la machine
#
# Returns:
# int|undef - Retourne l'identifiant interne de la machine
sub getMachineInternalId
{
    my ($parkNumber) = @_;

    if (!&LOC::Globals::get('connectToKimoce'))
    {
        return undef;
    }

    $parkNumber = &_formatParkNumber($parkNumber);

    # connexion à Kimoce
    my $kimoceDb = &LOC::Db::getConnection('kimoce');

    my $query = '
SELECT
    p.DosInCde
FROM p_dos p
WHERE p.DosFolderInCde = ' . DOSFOLDER_AIRENT . '
AND p.ObjIdentVal = ' . $kimoceDb->quote($parkNumber) . ';';

    my $result = $kimoceDb->fetchOne($query);
    return ($result ne '' ? $result * 1 : undef);
}

# Function: isMachineExists
# Indique si la machine existe ou non dans Kimoce
#
# Parameters:
# string $parkNumber - Le numéro de parc de la machine
#
# Returns:
# bool - Retourne 1 si la machine existe sinon 0
sub isMachineExists
{
    my ($parkNumber) = @_;

    if (!&LOC::Globals::get('connectToKimoce'))
    {
        return undef;
    }

    $parkNumber = &_formatParkNumber($parkNumber);

    # connexion à Kimoce
    my $kimoceDb = &LOC::Db::getConnection('kimoce');

    my $query = '
SELECT
    COUNT(*)
FROM p_dos
WHERE DosFolderInCde = ' . DOSFOLDER_AIRENT . '
AND ObjIdentVal = ' . $kimoceDb->quote($parkNumber) . ';';

    return ($kimoceDb->fetchOne($query) > 0 ? 1 : 0);
}


# Function: getMachineFeatures
# Retourne les caractéristiques de la machine dans Kimoce
#
# - 'customer'             => Raison sociale du client
# - 'site'                 => Chantier
# - 'address'              => Adresse;
# - 'postalCode'           => Code postal;
# - 'city'                 => Ville;
# - 'comment'              => Commentaire chantier
# - 'isAsbestos'           => Chantier avec amiante
# - 'contact'              => Contact;
# - 'telephone'            => Telephone;
# - 'lastBreakdown'        => Dernière panne
# - 'beginDate'            => Date de début du contrat
# - 'endDate'              => Date de fin du contrat
# - 'fullService'          => Contrat Full service
# - 'lastControl'          => Dernière VGP
# - 'lastAgencyCheckIn'    => Date de dernier passage agence
#
# Parameters:
# string $parkNumber - Le numéro de parc de la machine
#
# Returns:
# hashref|0 - Retourne un hashref, 0 si pas de résultat
sub getMachineFeatures
{
    my ($parkNumber) = @_;

    if (!&LOC::Globals::get('connectToKimoce'))
    {
        return undef;
    }

    $parkNumber = &_formatParkNumber($parkNumber);

    my $tabChars = &_getMachineFeatures($parkNumber, [FEAT_CUSTOMERLABEL, FEAT_SITELABEL, FEAT_SITEADDRESS,
                                                      FEAT_SITEPOSTALCODE, FEAT_SITECITY, FEAT_SITECOMMENT,
                                                      FEAT_SITEISASBESTOS, FEAT_SITECONTACT, FEAT_SITECONTACTTEL,
                                                      FEAT_BREAKDOWNDATE, FEAT_CONTRACTBEGINDATE, FEAT_CONTRACTENDDATE,
                                                      FEAT_CONTRACTFULLSERVICE, FEAT_CONTROLDATE, FEAT_LASTAGENCYCHECKINDATE]);

    return $tabChars;

}

# Function: getMachineCounters
# Retourne les compteurs de la machine
#
# - 'recoveryCounter'      => Compteur de récupération
# - 'sitesTransfercounter' => Compteur de transfert
#
# Parameters:
# string $parkNumber - Le numéro de parc de la machine
#
# Returns:
# hashref|0 - Retourne un hashref, 0 si pas de résultat
sub getMachineCounters
{
    my ($parkNumber) = @_;

    if (!&LOC::Globals::get('connectToKimoce'))
    {
        return undef;
    }

    $parkNumber = &_formatParkNumber($parkNumber);

    my $tabChars = &_getMachineFeatures($parkNumber, [COUNTER_RECOVERIES, COUNTER_SITESTRANSFERS, COUNTER_TIME]);

    return $tabChars;
}



# Function: getAgencyInfos
# Retourne les champs de Kimoce correspondant à une agence
#
# Contenu du tableau :
# - 'CpyInCde' => CpyInCde;
# - 'CpyAddrInCde' => CpyAddrInCde;
# - 'CpyTrdNamDsc' => CpyTrdNamDsc;
# - 'CpyAddrExCde' => CpyAddrExCde;
#
# Parameters:
# string $agency - Le code de l'agence
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getAgencyInfos
{
    my ($agency) = @_;

    if (!&LOC::Globals::get('connectToKimoce'))
    {
        return undef;
    }

    my %hashAgency = &LOC::Agency::getInfos($agency, LOC::Agency::GETINFOS_KIMOCE);
    my $cpyInCde = $hashAgency{'CpyInCde'};
    my $cpyAddrInCde = $hashAgency{'CpyAddrInCde'};

    # connexion à Kimoce
    my $kimoceDb = &LOC::Db::getConnection('kimoce');

    my $query = '
SELECT
    c.CpyTrdNamDsc,
    ca.CpyAddrExCde
FROM p_cpy c
LEFT JOIN p_cpyaddr ca
ON ca.CpyInCde = c.CpyInCde
WHERE c.CpyInCde = ' . $kimoceDb->quote($cpyInCde) . '
AND ca.CpyAddrInCde = ' . $kimoceDb->quote($cpyAddrInCde) . ';';
    my $result  = $kimoceDb->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if (!$result)
    {
        &LOC::Log::writeFileLog('kimoce', $kimoceDb->getLastQuery() . ' - ' . $kimoceDb->getErrorMsg(), undef,
                                LOC::Log::MODE_SEND_MAIL);
        return 0;
    }
    $result->{'CpyInCde'}     = $cpyInCde;
    $result->{'CpyAddrInCde'} = $cpyAddrInCde;

    return (wantarray ? %$result : $result);
}

# Function: isCallable
# Est-ce que Kimoce est joignable ?
#
# Returns:
# bool - Retourne 1 si le serveur répond
sub isCallable
{
    if (!&LOC::Globals::get('connectToKimoce'))
    {
        return 0;
    }

    # connexion à Kimoce
    my $kimoceDb = &LOC::Db::getConnection('kimoce');

    my $query = 'SELECT 1;';
    if (!$kimoceDb->query($query))
    {
        return 0;
    }

    return 1;
}

# Function: updateMachineGeoHistory
# Met à jour dans Kimoce la table d'historique des localisations de machines
#
# Parameters:
# string $parkNumber - Le numéro de parc de la machine
# string $agency     - Le code de la nouvelle agence de localisation
# bool $transaction  - Utilise le mode de transction ou pas (optionnel DB_TRANSACTION_ENABLED par défaut)
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, 0 sinon
sub updateMachineGeoHistory
{
    my ($parkNumber, $agency, $transaction) = @_;

    if (!&LOC::Globals::get('connectToKimoce'))
    {
        return 1;
    }

    $parkNumber = &_formatParkNumber($parkNumber);

    # si pas de spécification au niveau de la transaction
    if (!defined($transaction))
    {
        $transaction = DB_TRANSACTION_ENABLED;
    }

    # récupération des infos de l'agence dans kimoce
    my $destAgency = &getAgencyInfos($agency);

    # récupération du code interne de la machine
    my $result = &getMachineInfos($parkNumber);
    if (!$result)
    {
        return 0;
    }
    my $dosInCde = $result->{'DosInCde'};
    my $objIncde = $result->{'ObjInCde'};

    # connexion à Kimoce
    my $kimoceDb = &LOC::Db::getConnection('kimoce');

    if ($transaction)
    {
        $kimoceDb->beginTransaction();
    }

    # récupération de l'ancienne agence de localisation dans Kimoce
    my $result = &getMachineInfos($parkNumber);
    my $oldCpyInCde = $result->{'CpyInCde'};
    my $oldCpyAddrInCde = $result->{'CpyAddrInCde'};
    my $oldCpyTrdNamDsc = $result->{'CpyTrdNamDsc'};
    my $oldCpyAddrExCde = $result->{'CpyAddrExCde'};

    # si l'agence d'origine est bien différente de l'agence de destination
    if ($oldCpyInCde ne $destAgency->{'CpyInCde'} && $oldCpyAddrInCde ne $destAgency->{'CpyAddrInCde'})
    {
        # Récupération du prochain incrément de la table H_GEO
        my $increment = &getNextTableIncrement('H_GEO');
        if (!$increment)
        {
            if ($transaction)
            {
                $kimoceDb->rollBack();
            }
            return 0;
        }

        # récupération de la date du dernier mouvement dans h_geo
        my $query = '
SELECT
    count(*) AS nb,
    convert(varchar,max(Geotodte),21) AS lastDate,
    convert(varchar,getDate(),21) AS todayDate
FROM h_geo
WHERE DosInCde = ' . $kimoceDb->quote($dosInCde) . '
AND ObjInCde = ' . $kimoceDb->quote($objIncde) . ';';
        my $result2 = $kimoceDb->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
        if (!$result2)
        {
            return 0;
        }
        my $lastDate = $result2->{'lastDate'};
        if (!$lastDate)
        {
            $lastDate = $result2->{'todayDate'};
        }

        # insertion de la ligne dans h_geo
        my $resultId = $kimoceDb->insert('h_geo', {'GeoInCde'      =>  $increment,
                                                   'DosInCde'       => $dosInCde,
                                                   'ObjInCde'       => $objIncde,
                                                   'ObjIdentVal'    => $parkNumber,
                                                   'GeoFromDte'    =>  $lastDate,
                                                   'GeoToDte*'      => 'getDate()',
                                                   'AgoTypDsc'      => AGOTYPEDSC_TRANSFERT,
                                                   'CpyTrdNamDsc'   => $oldCpyTrdNamDsc,
                                                   'CpyAddrExCde'   => $oldCpyAddrExCde
                                                  });
        if ($resultId == -1)
        {
            &LOC::Log::writeFileLog('kimoce', $kimoceDb->getLastQuery() . ' - ' . $kimoceDb->getErrorMsg(), undef,
                                    LOC::Log::MODE_SEND_MAIL);
            if ($transaction)
            {
                $kimoceDb->rollBack();
            }
            return 0;
        }
    }
    if ($transaction)
    {
        if (!$kimoceDb->commit())
        {
            $kimoceDb->rollBack();
            return 0;
        }
    }

    return 1;
}

# Function: getNextTableIncrement
# Retourne le prochain incrément de la table
#
# Parameters:
# string $tableName - Le nom de la table
#
# Returns:
# int|0 - Retourne le prochain incrément si la requête s'est correctement effectuée, 0 sinon
sub getNextTableIncrement
{
    my ($tableName) = @_;

    if (!&LOC::Globals::get('connectToKimoce'))
    {
        return undef;
    }

    # connexion à Kimoce
    my $kimoceDb = &LOC::Db::getConnection('kimoce');

    my $query = 'DECLARE @INCDE int EXEC KIM_GETNEWCOUNTER @TABLENAME = ' . $kimoceDb->quote($tableName) . ', @INCDE = @INCDE OUTPUT SELECT @INCDE';
    my $num;
    my $result = $kimoceDb->getResource()->prepare($query);
    $result->execute();

    do {
        while (my $data = $result->fetchrow_arrayref())
        {
            if ($result->{syb_result_type} == 4040)
            {
                $num = $data->[0];
                last;
            }
        }
    }
    while ($result->{syb_more_results} && !$num);

    if (!$num)
    {
        &LOC::Log::writeFileLog('kimoce', $kimoceDb->getLastQuery() . ' - ' . $kimoceDb->getErrorMsg(), undef,
                                LOC::Log::MODE_SEND_MAIL);
        return 0;
    }

    return $num;
}

# Function: getMachineSituationByCode
# Retourne le code situation de Kimoce à partir d'un état dans la location de machine
#
# *TODO* : Connexion à la base de données LOCATION en passant 'FR' en dur, voir à déplacer la correspondance
# entre les ETCODE et les ObjSitInCde?
#
# Parameters:
# string $etcode - Le code état
#
# Returns:
# int - Retourne le code situation Kimoce, 0 en cas d'erreur
sub getMachineSituationByCode
{
    my ($etcode) = @_;

    if (!&LOC::Globals::get('connectToKimoce'))
    {
        return undef;
    }

    # récupère les paramètres de connexion à la location
    my $conn = &LOC::Db::getConnection('location', 'FR');

    my $query = '
SELECT
    ObjSitInCde
FROM ETATTABLE
WHERE ETCODE = ' . $conn->quote($etcode) . ';';
    my $num  = $conn->fetchOne($query);
    if (!$num)
    {
        return 0;
    }

    return $num;
}


# Function: getMachinesAddInfos
# Retourne les valeurs des informations additionnelles des machines dans Kimoce
#
# Parameters:
# string  $parkNumber - Numéro de parc de la machine (undef pour retourner les informations additionnelles
#                       de toutes les machines)
# Returns:
# hashref|0 - Retourne un hashref, 0 si pas de résultat
sub getMachineAddInfos
{
    my ($parkNumber) = @_;

    if (!&LOC::Globals::get('connectToKimoce'))
    {
        return {};
    }

    $parkNumber = &_formatParkNumber($parkNumber);

    my $tabAddInfos = &getAddInfos(LOC::Util::GETLIST_PAIRS);

    # connexion à Kimoce
    my $kimoceDb = &LOC::Db::getConnection('kimoce');

    my $query = '
SELECT
    ObjIdentVal,
    CharExCde,
    ObjCharVal,
    EntryTypExCde
FROM p_objchar
INNER JOIN r_char
ON r_char.CharInCde = p_objchar.CharInCde
INNER JOIN p_dos
ON p_dos.DosInCde = p_objchar.DosInCde
INNER JOIN s_entrytyp
ON r_char.CharTypCde = s_entrytyp.EntryTypInCde
WHERE CharExCde IN (' . $kimoceDb->quote(values(%$tabAddInfos)) . ')
AND p_dos.DosFolderInCde = ' . DOSFOLDER_AIRENT . '
AND p_dos.ObjIdentVal ' . (defined $parkNumber ? ' = ' . $kimoceDb->quote($parkNumber) : 'IS NOT NULL') . ';';

    my $tab = $kimoceDb->fetchList(LOC::Db::FETCH_ASSOC, $query, {});
    if (!$tab)
    {
        return 0;
    }

    # Tableau des résultats;
    my $tabResult = {};
    foreach my $tabRow (@$tab)
    {
        $tabResult->{$tabRow->{'ObjIdentVal'}}->{$tabRow->{'CharExCde'}} = {
            'ObjCharVal'    => $tabRow->{'ObjCharVal'},
            'EntryTypExCde' => $tabRow->{'EntryTypExCde'}
        };
    }

    return $tabResult;
}

# Function: getAddInfos
# Retourne la liste des informations additionnelles possibles sur les machines
#
# Parameters:
# int     $format      - Format de retour de la liste
# string  $localeId    - Identifiant de la locale pour les traductions
#
# Returns:
# hash|hashref|0 - Liste des informations additionnelles possibles sur les machines
sub getAddInfos
{
    my ($format, $localeId) = @_;

    if (!defined $localeId)
    {
        $localeId = LOC::Locale::DEFAULT_LOCALE_ID;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('auth');

    my $query = '';

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    inf_id AS `id`,
    IFNULL(inl_label, inf_name) AS `name`,
    inf_externalcode AS `externalCode`,
    inf_externaltype AS `externalType`,
    inf_sta_id AS `state.id`
FROM p_info
LEFT JOIN AUTH.p_info_locale
ON inf_id = inl_inf_id AND inl_lcl_id = ' . $db->quote($localeId) . '
WHERE inf_sta_id <> ' . $db->quote(MACHINEINFO_STATE_DELETED). ';';
    }
    else
    {
        $query = '
SELECT
    inf_id AS `id`,
    inf_externalcode  AS `externalCode`
FROM p_info
WHERE inf_sta_id <> ' . $db->quote(MACHINEINFO_STATE_DELETED). ';';
    }

    my $tab = $db->fetchList($format, $query, {}, 'id');

    return (wantarray ? %$tab : $tab);
}


# Function: getFeaturesUpdatesHistoriesList
# Retourne la liste des mises à jour de caractéristiques
#
# Parameters:
# hashref $tabFilters - Filtres (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Liste des mises à jour
sub getFeaturesUpdatesHistoriesList
{
    my ($tabFilters, $tabOptions) = @_;
    if (ref($tabFilters) ne 'HASH')
    {
        $tabFilters = {};
    }
    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('auth');

    my @tabWheres = ();

    my $query = '
SELECT
    kfu_id AS `id`,
    kfu_uuid AS `UUID`,
    kfu_parknumber AS `parkNumber`,
    CONCAT(IFNULL(tbl_f_machine.MANOPARC_AUX, ""), tbl_f_machine.MANOPARC) AS `fullParkNumber`,
    kfu_updatemode AS `updateMode`,
    kfu_featurecode AS `featureCode`,
    kfu_oldvalue AS `oldValue`,
    kfu_newvalue AS `newValue`,
    kfu_errormethod AS `errorMethod`,
    kfu_errorlastrequest AS `errorLastRequest`,
    kfu_errormsg AS `errorMessage`,
    kfu_date AS `date`,
    kfu_is_treated AS `isTreated`,
    kfu_sta_id AS `state.id`
FROM h_kimocefeatureupd
LEFT JOIN MACHINE tbl_f_machine
ON tbl_f_machine.MANOPARC = kfu_parknumber';

    # Filtres
    # - Id
    if (defined $tabFilters->{'id'})
    {
        push(@tabWheres, 'kfu_id IN (' . &LOC::Util::join(', ', $tabFilters->{'id'}) . ')');
    }
    # - UUID
    if (defined $tabFilters->{'kfu_uuid'})
    {
        push(@tabWheres, 'kfu_uuid IN (' . $db->quote($tabFilters->{'UUID'}) . ')');
    }
    # - Numéro de parc
    if (defined $tabFilters->{'parkNumber'})
    {
        push(@tabWheres, 'kfu_parknumber IN (' . $db->quote($tabFilters->{'parkNumber'}) . ')');
    }
    # - A-t-elle était traitée ?
    if (defined $tabFilters->{'isTreated'})
    {
        push(@tabWheres, 'kfu_is_treated ' . ($tabFilters->{'isTreated'} ? '!= 0' : '= 0'));
    }
    # - État
    if (defined $tabFilters->{'stateId'})
    {
        push(@tabWheres, 'kfu_sta_id IN (' . &LOC::Util::join(', ', $db->quote($tabFilters->{'stateId'})) . ')');
    }

    if (@tabWheres > 0)
    {
        $query .= '
WHERE ' . join("\nAND ", @tabWheres);
    }

    # Tris
    if (defined $tabOptions->{'orderBy'})
    {
        my $tabOrderFields = {
            'id'   => 'kfu_id',
            'uuid' => 'kfu_uuid',
            'date' => 'kfu_date'
        };
        foreach my $orderBy (@{$tabOptions->{'orderBy'}})
        {
            my ($field, $dir) = split(/:/, $orderBy);
            $query .= '
ORDER BY ' . $tabOrderFields->{$field} . ' ' . (defined $dir && uc($dir) eq 'DESC' ? 'DESC' : 'ASC');
        }
    }
    else
    {
        $query .= '
ORDER BY kfu_id ASC';
    }

    if (defined $tabOptions->{'limit'})
    {
        $query .= '
LIMIT ' . $tabOptions->{'limit'};
    }

    $query .= ';';

    my $tab = $db->fetchAssoc($query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    # Formatage des champs
    foreach my $tabRow (values %$tab)
    {
        $tabRow->{'id'}         *= 1;
        $tabRow->{'parkNumber'} *= 1;
        $tabRow->{'isTreated'}   = ($tabRow->{'isTreated'} != 0 ? 1 : 0);
    }

    return (wantarray ? %$tab : $tab);
}


# Function: resendNotTreatedFeaturesUpdates
# Renvoi les mises à jour de caractéristiques non traitées
#
# Parameters:
# int $lastId - Dernier identifiant de mise à jour à renvoyer (facultatif)
#
# Returns:
# hash|hashref|undef - Résultats
sub resendNotTreatedFeaturesUpdates
{
    my ($lastId) = @_;


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('auth');

    # Récupération de la list des mises à jour à renvoyer
    my $tabToResend = &getFeaturesUpdatesHistoriesList({
        'isTreated' => 0
    }, {
        'orderBy' => [
            'id:ASC'
        ]
    });

    my $tabResults = {};
    tie(%$tabResults, 'Tie::IxHash');

    # Groupement des mises à jour
    my $tabGroupedUpdates = {};
    foreach my $row (values(%$tabToResend))
    {
        if (!$lastId || $row->{'id'} <= $lastId)
        {
            my $key = $row->{'UUID'} . '@' . $row->{'parkNumber'};
            if (!exists $tabGroupedUpdates->{$key})
            {
                $tabGroupedUpdates->{$key} = {
                    'parkNumber' => $row->{'parkNumber'},
                    'list'       => {}
                };
            }
            $tabGroupedUpdates->{$key}->{'list'}->{$row->{'featureCode'}} = $row;
        }
    }

    # Envoi par groupe
    foreach my $group (values(%$tabGroupedUpdates))
    {
        my @tabFeaturesCodes = keys(%{$group->{'list'}});
        my $tabMachineFeatures = &_getMachineFeatures($group->{'parkNumber'}, \@tabFeaturesCodes);

        my $tabFeaturesToUpdate = {};
        foreach my $row (values(%{$group->{'list'}}))
        {
            # Flag de la ligne en traitée
            $db->update('h_kimocefeatureupd', {
                'kfu_is_treated' => 1
            }, {
                'kfu_id' => $row->{'id'}
            }, 'AUTH');

            # Traitement si la valeur de la caractéristique n'a pas changé
            my $isToDo = 1;
            my $notDoneReason = '';
            my $currentValue = $tabMachineFeatures->{$row->{'featureCode'}};
            if ($row->{'oldValue'} eq $currentValue)
            {
                if (&LOC::Util::in_array($row->{'featureCode'}, [COUNTER_RECOVERIES, COUNTER_SITESTRANSFERS]))
                {
                    # Vérification de l'existence d'une demande de type "Entretien" ou "Transfert inter-chantiers"
                    my $requestType = ($row->{'featureCode'} eq COUNTER_RECOVERIES ? REQUEST_TYPE_MAINTENANCE : REQUEST_TYPE_SITESTRANSFER);
                    my $isRequestExists = &_isMachineRequestExists($group->{'parkNumber'}, $requestType);
                    if (!defined $isRequestExists || $isRequestExists)
                    {
                        $isToDo = 0;
                        $notDoneReason = 'requestExists';
                    }
                }
            }
            else
            {
                $isToDo = 0;
                $notDoneReason = 'valueHasChanged';
            }

            if ($isToDo)
            {
                $tabFeaturesToUpdate->{$row->{'featureCode'}} = $row->{'newValue'};
                $row->{'@done'} = 1;
                $row->{'@details'} = undef;
            }
            else
            {
                $row->{'@done'} = 0;
                $row->{'@details'} = {
                    'reason'       => $notDoneReason,
                    'currentValue' => $currentValue
                };
            }

            $tabResults->{$row->{'id'}} = $row;
        }

        if (keys(%$tabFeaturesToUpdate) > 0)
        {
            # Lancement des mises à jour
            &_setMachineFeatures($group->{'parkNumber'}, $tabFeaturesToUpdate, UPDATEMODE_WS);
        }
    }

    return $tabResults;
}


#TODO: utiliser les webservices pour récupérer les caractéristiques (pb de performance)
## Function: _getMachineFeatures
## Retourne les caractéristiques de la machine
##
## Parameters:
## string   $parkNumber - Le numéro de parc de la machine
## arrayref $tabFilters - Tableau des caractéristiques à récupérer
##
## Returns:
## hashref|0 - Retourne un hashref, 0 si pas de résultat
#sub _getMachineFeatures
#{
#    my ($parkNumber, $tabFilters) = @_;
#
#    $parkNumber = &_formatParkNumber($parkNumber);
#
#    if (!&LOC::Globals::get('connectToKimoce'))
#    {
#        return undef;
#    }
#
#    if (!defined $tabFilters)
#    {
#        $tabFilters = [];
#    }
#    if (!defined $tabOptions)
#    {
#        $tabOptions = {};
#    }
#
#    # connexion à Kimoce
#    &_getWebServiceConnection();
#
#    # Récupération des caractéristiques
#    my $tabParams = {
#        'key' => {
#            'extKey' => $parkNumber,
#            'intKey' => 0,
#            'typ'    => 'DOS'
#        }
#    };
#     my $result = &_callWs('get', {'arg0' => $tabParams})
#
#    if (!$result)
#    {
#        return 0;
#    }
# TODO : parcourir correctement la trame de résultat pour trouver les caractéristiques
#    my $tabChars = $result->result->{'mainItem'}->{'chars'};
#
#    # Tableau contenant des caractéristiques à formater
#    my @tabCharExCdeDate = (
#        FEAT_BREAKDOWNDATE,
#        FEAT_CONTRACTBEGINDATE,
#        FEAT_CONTRACTENDDATE,
#        FEAT_CONTROLDATE,
#        FEAT_LASTAGENCYCHECKINDATE
#    );
#    my @tabCharExCdeCounterIds = keys(%tabCountersFormats);
#
#    # Tableau des résultats;
#    my $tabResult = {};
#    foreach my $tabRow (@$tabChars)
#    {
#        my $code = $tabRow->{'charKey'}->{'extKey'};
#        my $value = $tabRow->{'charVal'};
#
#        if (@$tabFilters == 0 || &LOC::Util::in_array($code, $tabFilters))
#        {
#            # Tableau des résultats;
#            if (&LOC::Util::in_array($code, \@tabCharExCdeDate))
#            {
#                my $year  = substr($value, 0, 4);
#                my $month = substr($value, 4, 2);
#                my $day   = substr($value, 6, 2);
#                $value = $day . '/' . $month . '/' . $year;
#                if ($value == "//")
#                {
#                    $value = "";
#                }
#            }
#            if (&LOC::Util::in_array($code, \@tabCharExCdeCounterIds))
#            {
#                $value = sprintf($tabCountersFormats{$code}, $value);
#            }
#            $value =~ s/\\r\\n/ /;
#
#            $tabResult->{$code} = $value;
#        }
#    }
#
#    return $tabResult;
#}

# Function: _getMachineFeatures
# Retourne les caractéristiques de la machine
#
# Parameters:
# string   $parkNumber - Le numéro de parc de la machine
# arrayref $tabFilters - Tableau des caractéristiques à récupérer
#
# Returns:
# hashref|0 - Retourne un hashref, 0 si pas de résultat
sub _getMachineFeatures
{
    my ($parkNumber, $tabFilters) = @_;

    if (!defined $tabFilters)
    {
        $tabFilters = [];
    }

    # connexion à Kimoce
    my $kimoceDb = &LOC::Db::getConnection('kimoce');

    my $query = '
SELECT
    CharExCde,
    CharDsc,
    CONCAT(' . $kimoceDb->quote('#') . ', ObjCharVal) AS ObjCharVal
FROM p_objchar
LEFT JOIN r_char
ON r_char.CharInCde = p_objchar.CharInCde
LEFT JOIN p_dos
ON p_dos.DosInCde = p_objchar.DosInCde
WHERE p_dos.DosFolderInCde = ' . DOSFOLDER_AIRENT . '
AND p_dos.ObjIdentVal = ' . $kimoceDb->quote($parkNumber) . '
AND CharExCde IN (' . $kimoceDb->quote($tabFilters) . ');';
    my $tabChars = $kimoceDb->fetchList(LOC::Db::FETCH_ASSOC, $query, {});
    if (!$tabChars)
    {
        return 0;
    }

    # Tableau contenant des caractéristiques à formater
    my @tabCharExCdeDate = (
        FEAT_BREAKDOWNDATE,
        FEAT_CONTRACTBEGINDATE,
        FEAT_CONTRACTENDDATE,
        FEAT_CONTROLDATE,
        FEAT_LASTAGENCYCHECKINDATE
    );
    my @tabCharExCdeCounterIds = keys(%tabCountersFormats);

    # Tableau des résultats;
    my $tabResult = {};
    foreach my $tabRow (@$tabChars)
    {
        my $code = $tabRow->{'CharExCde'};
        # Suppression du caractère '#' ajouté dans la requête
        # (permet d'éviter le problème avec Sybase et les chaînes vides renvoyées par un espace simple)
        my $value = substr($tabRow->{'ObjCharVal'}, 1);

        # Tableau des résultats;
        if (&LOC::Util::in_array($code, \@tabCharExCdeDate))
        {
            my $year  = substr($value, 0, 4);
            my $month = substr($value, 4, 2);
            my $day   = substr($value, 6, 2);
            $value = $day . '/' . $month . '/' . $year;
            if ($value == '//')
            {
                $value = '';
            }
        }
        if (&LOC::Util::in_array($code, \@tabCharExCdeCounterIds))
        {
            $value = sprintf($tabCountersFormats{$code}, $value);
        }
        $value =~ s/\\r\\n/ /;

        $tabResult->{$code} = $value;
    }

    return $tabResult;
}

# Function: _isMachineRequestExists
# Indique si une demande d'entretien existe actuellement sur la machine
#
# Parameters:
# string $parkNumber  - Le numéro de parc de la machine
# string $requestType - Type de demande
#
# Returns:
# bool
sub _isMachineRequestExists
{
    my ($parkNumber, $requestType) = @_;

    $parkNumber = &_formatParkNumber($parkNumber);

    # Connexion à Kimoce
    my $kimoceDb = &LOC::Db::getConnection('kimoce');

    # Vérification de l'existence d'une demande de type "Entretien" ou "Transfert inter-chantiers"
    my $query = '
SELECT
    COUNT(*)
FROM p_rqt
LEFT JOIN p_dos
ON p_dos.DosInCde = p_rqt.DosInCde
WHERE p_dos.DosFolderInCde = ' . DOSFOLDER_AIRENT . '
AND p_dos.ObjIdentVal = ' . $kimoceDb->quote($parkNumber) . '
AND RqtStsInCde != ' . REQUEST_STATUS_CLOSED . '
AND RqtNatInCde = ' . $requestType . ';';

    my $nbRequests = $kimoceDb->fetchOne($query);
    if (!defined $nbRequests)
    {
        return undef;
    }

    return ($nbRequests == 0 ? 0 : 1);
}

# Function: _addFeaturesUpdatesHistory
# Ajoute les historiques des mises à jour de caractéristiques
#
# Parameters:
# string        $parkNumber    - Numéro de parc de la machine
# hashref       $tabFeatures   - Caractéristiques mises à jour (contenant 2 clées 'oldValue' et 'newValue')
# string        $updateMode    - Mode de mise à jour
# hashref|undef $tabErrorInfos - Dans le cas d'une, les informations détaillées de celle-ci sinon undef
sub _addFeaturesUpdatesHistory
{
    my ($parkNumber, $tabFeatures, $updateMode, $tabErrorInfos) = @_;

    # Connexion à la base de données
    my $db = &LOC::Db::getConnection('auth');

    # Génération d'un uuid
    my $ug = Data::UUID->new();
    my $uuid = $ug->create_str();

    foreach my $featureCode (keys(%$tabFeatures))
    {
        my $tabInsert = {
            'kfu_uuid'        => $uuid,
            'kfu_parknumber'  => $parkNumber,
            'kfu_updatemode'  => $updateMode,
            'kfu_featurecode' => $featureCode,
            'kfu_oldvalue'    => $tabFeatures->{$featureCode}->{'oldValue'},
            'kfu_newvalue'    => $tabFeatures->{$featureCode}->{'newValue'},
            'kfu_date*'       => 'NOW()'
        };

        if ($tabErrorInfos)
        {
            $tabInsert->{'kfu_errormethod'}      = $tabErrorInfos->{'method'};
            $tabInsert->{'kfu_errorlastrequest'} = $tabErrorInfos->{'lastRequest'};
            $tabInsert->{'kfu_errormsg'}         = $tabErrorInfos->{'message'};
            $tabInsert->{'kfu_is_treated'}       = 0;
            $tabInsert->{'kfu_sta_id'}           = 'KFU02';
        }
        else
        {
            $tabInsert->{'kfu_is_treated'} = 1;
            $tabInsert->{'kfu_sta_id'}     = 'KFU01';
        }

        $db->insert('h_kimocefeatureupd', $tabInsert, 'AUTH');
    }
}


# Function: _setMachineFeatures
# Met à jour les caractéristiques voulues de la machine voulue dans Kimoce
#
# Caractéristiques possibles :
# - ADR : adresse du chantier où se trouve la machine;
# - CHA : libellé du chantier où se trouve la machine;
# - CLI : raison sociale du client qui loue la machine;
# - CTC : nom du contact du chantier;
# - TEL : numéro de téléphone du contact du chantier;
# - VIL : nom de la ville du chantier;
# - CP : code postal du chantier;
# - DTEPA : Date de la panne de la machine (format YYYYMMDD)
# - DTEDC : Date de début du contrat
# - DTEFC : Date de fin du contrat
# - DTEVGP : Date de contrôle VGP
# - CREC : Compteur de récupération de la machine
# - CH : Compteur horaire
# - DTEAGC : Date de dernier passage agence
#
# Parameters:
# string  $parkNumber  - Numéro de parc de la machine
# hashref $tabFeatures - Caractéristiques à mettre à jour
# string  $updateMode  - Mode de mise à jour
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, 0 sinon
sub _setMachineFeatures
{
    my ($parkNumber, $tabFeatures, $updateMode) = @_;

    # Récupération des valeurs courantes des caractéristiques dans Kimoce
    my @tabFeaturesCodes = keys(%$tabFeatures);

    if (@tabFeaturesCodes == 0)
    {
        return 1;
    }

    my $tabOldFeatures = &_getMachineFeatures($parkNumber, \@tabFeaturesCodes);

    my $tabFeaturesToUpdate = {};
    my $tabFeaturesHistory  = {};
    my $nbUpdates = 0;
    foreach my $featureCode (@tabFeaturesCodes)
    {
        my $oldValue = $tabOldFeatures->{$featureCode};
        my $newValue = $tabFeatures->{$featureCode};
        if ($newValue ne $oldValue)
        {
            $nbUpdates++;
            $tabFeaturesToUpdate->{$featureCode} = $newValue;
            $tabFeaturesHistory->{$featureCode}  = {
                'oldValue' => $oldValue,
                'newValue' => $newValue
            };
        }
    }

    # Il n'y a rien a faire
    if ($nbUpdates == 0)
    {
        return 1;
    }


    # - Mise à jour par webservice
    if ($updateMode eq UPDATEMODE_WS)
    {
        my $kimoceSoap = LOC::Kimoce::Soap->new();
        my $result = $kimoceSoap->updateFeatures($parkNumber, $tabFeaturesToUpdate);

        my $tabErrorInfos = undef;
        if (!$result)
        {
            $tabErrorInfos = {
                'method'      => $kimoceSoap->getLastMethod(),
                'lastRequest' => $kimoceSoap->getLastXml(),
                'message'     => $kimoceSoap->getErrorMsg()
            };
        }
        &_addFeaturesUpdatesHistory($parkNumber, $tabFeaturesHistory, UPDATEMODE_WS, $tabErrorInfos);

        return $result;
    }
    # - Mise à jour par requête
    elsif ($updateMode eq UPDATEMODE_DB)
    {
        # connexion à Kimoce
        my $kimoceDb = &LOC::Db::getConnection('kimoce');
        my $hasTransaction = $kimoceDb->hasTransaction();

        # Démarre la transaction
        if (!$hasTransaction)
        {
            $kimoceDb->beginTransaction();
        }

        # récupération du numéro interne de la machine
        my $dosInCde = &getMachineInternalId($parkNumber);
        if (!$dosInCde)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $kimoceDb->rollBack();
            }
            return 0;
        }

        # Ajout des historiques
        $kimoceDb->addTransactionAction('commit.post', sub {
            return &_addFeaturesUpdatesHistory($parkNumber, $tabFeaturesHistory, UPDATEMODE_DB, undef);
        });
        $kimoceDb->addTransactionAction('rollback.post', sub {
            my $db = $_[0];
            return &_addFeaturesUpdatesHistory($parkNumber, $tabFeaturesHistory, UPDATEMODE_DB, {
                'method'      => undef,
                'lastRequest' => $db->getLastQuery(),
                'message'     => $db->getErrorCode() . ' - ' . $db->getErrorMsg()
            });
        });

        foreach my $featureCode (keys(%$tabFeaturesToUpdate))
        {
            # Récupération identifiant Kimoce de la caractéristique
            my $query = '
SELECT
    CharInCde
FROM r_char
WHERE CharExCde = ' . $kimoceDb->quote($featureCode) . ';';
            my $charInCde  = $kimoceDb->fetchOne($query);
            if (!$charInCde)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $kimoceDb->rollBack();
                }
                &LOC::Log::writeFileLog('kimoce', $kimoceDb->getLastQuery() . ' - ' . $kimoceDb->getErrorMsg(), undef,
                                        LOC::Log::MODE_SEND_MAIL);
                return 0;
            }

            # Mise en forme spécifique
            my $value = $tabFeaturesToUpdate->{$featureCode};
            # - Dates
            my @tabCharExCdeDate = (
                FEAT_BREAKDOWNDATE,
                FEAT_CONTRACTBEGINDATE,
                FEAT_CONTRACTENDDATE,
                FEAT_CONTROLDATE,
                FEAT_LASTAGENCYCHECKINDATE
            );
            if (&LOC::Util::in_array($featureCode, \@tabCharExCdeDate))
            {
                my @tabDateInfos = split(/\//, $value);
                $value = $tabDateInfos[2] . $tabDateInfos[1] . $tabDateInfos[0];
            }

            # Mise à jour de la caractéristique dans Kimoce
            if ($kimoceDb->update('p_objchar', {'ObjCharVal' => $value},
                                               {'CharInCde' => $charInCde, 'DosInCde' => $dosInCde}) == -1)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $kimoceDb->rollBack();
                }
                &LOC::Log::writeFileLog('kimoce', $kimoceDb->getLastQuery() . ' - ' . $kimoceDb->getErrorMsg(), undef,
                                        LOC::Log::MODE_SEND_MAIL);
                return 0;
            }
        }

        # Valide la transaction
        if (!$hasTransaction)
        {
            if (!$kimoceDb->commit())
            {
                $kimoceDb->rollBack();
                return 0;
            }
        }
        return 1;
    }
    else
    {
        return 0;
    }
}

# Function: _formatParkNumber
# Formatte le numéro de parc machine pour Kimoce
#
# Parameters:
# string  $parkNumber - Numéro de parc de la machine
#
# Returns:
# string - Numéro de parc formatté
sub _formatParkNumber
{
    my ($parkNumber) = @_;
    $parkNumber =~ s/\D//g;
    return $parkNumber;
}

1;
