use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Machine
# Module permettant de gérer les machines
package LOC::Machine;


# Constants: Récupération d'informations sur les machines
# GETINFOS_ALL              - Récupération de l'ensemble des informations
# GETINFOS_BASE             - Récupération des informations de base
# GETINFOS_POSSIBILITIES    - Récupération des possibilités sur la machine
# GETINFOS_RIGHTS           - Récupération des droits utilisateur sur la machine
# GETINFOS_AGENCIESTRANSFER - Récupération des informations sur les transferts inter-agences en cours de la machine
# GETINFOS_ADDINFOS         - Récupération des informations sur les équipements de la machine
# GETINFOS_CONTROLORDER     - Récupération des informations sur la dernière commande contrôle
use constant
{
    GETINFOS_ALL              => 0xFFFFFFFF,
    GETINFOS_BASE             => 0x00000000,
    GETINFOS_POSSIBILITIES    => 0x00000001,
    GETINFOS_RIGHTS           => 0x00000002,
    GETINFOS_AGENCIESTRANSFER => 0x00000004,
    GETINFOS_ADDINFOS         => 0x00000008,
    GETINFOS_TELEMATICS       => 0x00000010,
    GETINFOS_CONTROLORDER     => 0x00000020,
    GETINFOS_LASTAGENCYCHECKIN => 0x00000040
};

# Constants: Niveaux de récupération des données
# LEVEL_MODEL          - Niveau modéle machine
# LEVEL_MACHINESFAMILY - Niveau famille machine
# LEVEL_BUSINESSFAMILY - Niveau famille commerciale
# LEVEL_FAMILY         - Niveau famille
use constant
{
    LEVEL_MODEL          => 0,
    LEVEL_MACHINESFAMILY => 1,
    LEVEL_BUSINESSFAMILY => 2,
    LEVEL_FAMILY         => 3
};

# Constants: États des machines
# STATE_RENTED            - Louée
# STATE_AVAILABLE         - Disponible
# STATE_RECOVERY          - Récupération
# STATE_REVIEW            - Révision
# STATE_CONTROL           - Contrôle
# STATE_BROKEN            - Panne
# STATE_CHECK             - Vérification
# STATE_ATNRENOVATION     - Réno - ATN/ASM
# STATE_TRANSFER          - En transfert
# STATE_JLGRENOVATION     - Réno - JLG
# STATE_AVAILABLEFAUILLET - Dispo - Fauillet
# STATE_DELIVERY          - En livraison
# STATE_RETURNED          - Restituée
# STATE_STOLEN            - Vol
# STATE_LOST              - Perdu
# STATE_SOLD              - Vendue
# STATE_DISABLED          - Inactive
# STATE_RENOSC            - Réno - ST
# STATE_TORETURNFR        - À retourner FR
# STATE_FORSALE           - À vendre
use constant
{
    STATE_RENTED            => 'MAC01',
    STATE_AVAILABLE         => 'MAC02',
    STATE_RECOVERY          => 'MAC03',
    STATE_REVIEW            => 'MAC04',
    STATE_CONTROL           => 'MAC05',
    STATE_BROKEN            => 'MAC06',
    STATE_CHECK             => 'MAC07',
    STATE_ATNRENOVATION     => 'MAC08',
    STATE_TRANSFER          => 'MAC09',
    STATE_JLGRENOVATION     => 'MAC10',
    STATE_AVAILABLEFAUILLET => 'MAC11',
    STATE_DELIVERY          => 'MAC12',
    STATE_SHOWROOM          => 'MAC20',
    STATE_RETURNED          => 'MAC21',
    STATE_STOLEN            => 'MAC23',
    STATE_LOST              => 'MAC24',
    STATE_SOLD              => 'MAC25',
    STATE_TORECEIVE         => 'MAC28',
    STATE_DISABLED          => 'MAC31',
    STATE_RENOSC            => 'MAC35',
    STATE_TORETURNFR        => 'MAC36',
    STATE_FORSALE           => 'MAC39',
    STATE_FORSALEMC         => 'MAC40',
    STATE_FORSALEMN         => 'MAC41',
    STATE_FORSALERB         => 'MAC42',
    STATE_FORSALEEMT        => 'MAC43',
    STATE_FORSALEVTM        => 'MAC44',
    STATE_FORSALEGU         => 'MAC45',
    STATE_FORSALEUP         => 'MAC46',
    STATE_FORSALEVARIOUS    => 'MAC47',
    STATE_FORSALEIP         => 'MAC48',
    STATE_FORSALEMB         => 'MAC49',
    STATE_SALEMC            => 'MAC50',
    STATE_SALEMN            => 'MAC51',
    STATE_SALERB            => 'MAC52',
    STATE_SALEEMT           => 'MAC53',
    STATE_SALEVTM           => 'MAC54',
    STATE_SALEGU            => 'MAC55',
    STATE_SALEUP            => 'MAC56',
    STATE_SALEVARIOUS       => 'MAC57',
    STATE_SALEIP            => 'MAC58',
    STATE_SALEMB            => 'MAC59'
};

# Constants: Mode de visibilité de la machine dans une autre agence
# VISIBILITYMODE_NONE      - pas de visibilité
# VISIBILITYMODE_MACHINE   - visible par la coche sur la fiche machine
# VISIBILITYMODE_TRANSPORT - visible par la coche de la popup transport (transférer à l'agence de)
use constant
{
    VISIBILITYMODE_NONE      => 0,
    VISIBILITYMODE_MACHINE   => 1,
    VISIBILITYMODE_TRANSPORT => 2
};

# Constants: Préfixes du numéro de parc
# PREFIX_SUBRENT   - Sous-location
# PREFIX_ACCESSORY - Full service
# PREFIX_CATEGORYB - Catégorie B
use constant
{
    PREFIX_SUBRENT   => 'SL',
    PREFIX_ACCESSORY => 'A',
    PREFIX_CATEGORYB => 'B',
    PREFIX_SALE      => 'V'
};

# Constants: Informations additionnelles
# ADDINFO_SECURITYSYSTEM     - GIES
# ADDINFO_STABILIZERS        - Stabilisateurs
# ADDINFO_TELEMATICS         - Télématique
# ADDINFO_REGISTRATIONNUMBER - Numéro d'immatriculation
use constant
{
    ADDINFO_SECURITYSYSTEM     => 'GIES',
    ADDINFO_STABILIZERS        => 'STABI',
    ADDINFO_TELEMATICS         => 'TELEMAT',
    ADDINFO_REGISTRATIONNUMBER => 'NUMIMMAT'
};

# Constants: Paliers d'alertes pour les contrôles VGP en jours
# CONTROL_1WEEK    - 7 jours
# CONTROL_2WEEKS   - 15 jours
# CONTROL_2MONTHS  - 60 jours
use constant
{
    CONTROL_1WEEK   => 7,
    CONTROL_2WEEKS  => 15,
    CONTROL_2MONTHS => 60
};

# Constants: Erreurs
# ERROR_FATAL                     - Erreur fatale
# ERROR_RIGHTS                    - Impossible de modifier cette information du contrat
# ERROR_KIMOCEUNAVAILABLE         - Kimoce non disponible
# ERROR_UNKNOWNMACHINEINKIMOCE    - Machine inconnue dans Kimoce
use constant
{
    ERROR_FATAL       => 0x0001,
    ERROR_RIGHTS      => 0x0003,

    ERROR_KIMOCEUNAVAILABLE      => 0x0040,
    ERROR_UNKNOWNMACHINEINKIMOCE => 0x0041,

    ERROR_CREATIONALREADYFINISHED => 0x0050

};


use strict;

use Tie::IxHash;


my $fatalErrMsg = 'Erreur fatale: module LOC::Machine, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";


# Modules internes
use LOC::Contract::Rent;
use LOC::Db;
use LOC::Date;
use LOC::EndUpdates;
use LOC::Transport;
use LOC::Agency;
use LOC::Json;
use LOC::StartCode;
use LOC::StartCode::Contract::Rent;
use LOC::Template;

# Sauvegarde des machines existantes
my $_tabExists = {};


# Liste des erreurs:
#
# 0x0001 => Erreur fatale
# 0x0002 => Changement d'état impossible : un transfert inter-agences est en cours
# 0x0003 => Impossible de modifier cette information
# 0x0004 => Commentaire blocage sur chantier obligatoire
# 0x0005 => La machine n'est plus sur le contrat
# 0x0006 => La machine ne peut pas être désattribuée
# 0x0007 => La date de dernière VGP est postérieure à la date du jour
#
# 0x0010 => Erreur de la création/modification du transfert inter-agences
# 0x0020 => Erreur lors de la modification des informations machine
# 0x0030 => Champs obligatoires non renseignés
# 0x0031 => Le modèle de machine n'existe pas
# 0x0032 => Le numéro de parc existe déjà
# 0x0033 => Le numéro de série associé au modèle existe déjà
#
# 0x0040 => Kimoce non disponible
# 0x0041 => La machine n'existe pas dans Kimoce
#
# 0x0100 => La date de modification n'est pas valide


# Function: _isTelematActived
# Indique si la télématique est activée ou non
#
# Parameters:
# string $countryId - Pays
#
# Returns:
# bool
sub _isTelematActived
{
    my ($countryId) = @_;

    my $isActived = &LOC::Characteristic::getCountryValueByCode('TELEMAT', $countryId);

    return ($isActived != 0 ? 1 : 0);
}


# Function: _getRights
# Récupérer les droits sur la fiche machine pour un utilisateur donné
#
# Parameters:
# hashref $tabMachineInfos - Informations sur la machine
# hashref $tabUserInfos    - Informations sur l'utilisateur
#
# Returns:
# hashref - Tableau des droits
sub _getRights
{
    my ($tabMachineInfos, $tabUserInfos) = @_;

    my $agencyId     = $tabMachineInfos->{'agency.id'};

    my $isUserAdmin        = $tabUserInfos->{'isAdmin'};
    my $isUserSuperv       = $tabUserInfos->{'isSuperv'};
    my $isUserTech         = &LOC::Util::in_array(LOC::User::GROUP_TECHNICAL, $tabUserInfos->{'tabGroups'});
    my $isUserChefAg       = &LOC::Util::in_array(LOC::User::GROUP_AGENCYMANAGER, $tabUserInfos->{'tabGroups'});
    my $isUserComm         = &LOC::Util::in_array(LOC::User::GROUP_COMMERCIAL, $tabUserInfos->{'tabGroups'});
    my $isUserCompta       = &LOC::Util::in_array(LOC::User::GROUP_ACCOUNTING, $tabUserInfos->{'tabGroups'});
    my $isUserTransp       = &LOC::Util::in_array(LOC::User::GROUP_TRANSPORT, $tabUserInfos->{'tabGroups'});
    my $isUserChefAt       = &LOC::Util::in_array(LOC::User::GROUP_FOREMAN, $tabUserInfos->{'tabGroups'});
    my $isUserRespTech     = &LOC::Util::in_array(LOC::User::GROUP_TECHNICALMANAGER, $tabUserInfos->{'tabGroups'});
    my $isUserTechPlatform = &LOC::Util::in_array(LOC::User::GROUP_TECHNICALPLATFORM, $tabUserInfos->{'tabGroups'});
    my $isUserTelematics   = &LOC::Util::in_array(LOC::User::GROUP_TELEMATICS, $tabUserInfos->{'tabGroups'});

    my $tabRights = {};

    # Initialisation des droits
    &LOC::Util::setTreeValues($tabRights, {
        'telematics' => ['immobilization',
                         'customer', 'backup', 'technician', 'driver', 'aftersales', 'externalTechnician', 'externalDriver', 'help',
                         'disable', 'toggle', 'regenerateRentContractCodes', 'activateRentContractScope', 'digiKeyActivation', 'changeEquipment'],
        'actions'    => ['control', 'siteBlocked', 'outOfPark', 'state', 'createTransfer', 'cancelTransfer',
                         'doTransfer', 'deallocate', 'visibility', 'delegate', 'finishCreation'],
        'kimoce'     => ['localization', 'infos', 'counters', 'characteristics', 'addInfos']
    }, 0);

    # Caractéristiques
    my $tabUsers = &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('USRCREERKIMOCE', $agencyId));
    if (&LOC::Util::in_array($tabUserInfos->{'id'}, $tabUsers))
    {
        &LOC::Util::setTreeValues($tabRights, {
            'actions'     => ['finishCreation']
        }, 1);
    }

    # TECH, CHEFAT et PSTN
    if ($isUserTech || $isUserChefAt || $isUserTechPlatform || $isUserAdmin)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'telematics' => ['immobilization', 'technician', 'externalTechnician', 'externalDriver'],
            'actions'    => ['control', 'siteBlocked', 'deallocate']
        }, 1);
    }

    # CHEFAT
    if ($isUserChefAt || $isUserAdmin)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'actions' => ['createTransfer', 'cancelTransfer']
        }, 1);
    }

    # RESPTECH
    if ($isUserRespTech || $isUserAdmin)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'telematics' => ['immobilization'],
            'actions'    => ['siteBlocked', 'state']
        }, 1);
    }

    # TRANSP
    if ($isUserTransp || $isUserAdmin)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'telematics' => ['immobilization', 'driver', 'externalDriver'],
            'actions'    => ['siteBlocked', 'createTransfer', 'cancelTransfer',
                             'doTransfer', 'deallocate', 'visibility', 'delegate']
        }, 1);
    }

    # COMM et CHEFAG
    if ($isUserComm || $isUserChefAg || $isUserAdmin)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'telematics' => ['immobilization', 'customer', 'externalTechnician', 'externalDriver'],
            'actions'    => ['siteBlocked', 'createTransfer', 'cancelTransfer']
        }, 1);
    }

    # Télématique
    if ($isUserTelematics || $isUserAdmin)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'telematics' => ['immobilization', 'customer', 'backup', 'technician', 'driver', 'aftersales', 'externalTechnician', 'externalDriver', 'help']
        }, 1);
        if ($isUserSuperv || $isUserAdmin)
        {
            &LOC::Util::setTreeValues($tabRights, {
                'telematics' => ['disable', 'toggle', 'regenerateRentContractCodes', 'activateRentContractScope', 'digiKeyActivation', 'changeEquipment']
            }, 1);
        }
    }

    # SUPERV
    if ($isUserSuperv || $isUserAdmin)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'actions'    => ['control', 'siteBlocked', 'outOfPark', 'state', 'createTransfer', 'cancelTransfer',
                             'doTransfer', 'deallocate', 'visibility', 'delegate', 'finishCreation'],
            'kimoce'     => ['localization', 'infos', 'counters', 'characteristics', 'addInfos']
        }, 1);
    }

    return $tabRights;
}


# Function: deallocate
# Désattribuer une machine d'un contrat
#
# Parameters:
# string  $countryId  - Pays
# int     $id         - Identifiant de la machine
# int     $userId     - Utilisateur responsable de la modification
# hashref $tabOptions - Tableau des options supplémentaires (facultatif). Peut contenir contractId et modificationDate
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub deallocate
{
    my ($countryId, $id, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'deallocate', $_[0], __FILE__, '');

        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }

        return 0;
    };

    # Récupération des informations de la machine
    my $tabInfos = &getInfos($countryId, $id);
    if (!$tabInfos)
    {
        return &$rollBackAndError(__LINE__);
    }

    # Vérification de la date de modification (accès concurrentiel)
    if (!&_checkUpToDateData($tabInfos->{'modificationDate'}, $tabOptions->{'modificationDate'}))
    {
        return &$rollBackAndError(__LINE__, 0x0100);
    }

    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }
    # Récupération des droits
    my $tabRights = &_getRights($tabInfos, $tabUserInfos);

    # Vérifications
    # - Droits
    if (!&_hasRight($tabRights->{'actions'}, 'deallocate'))
    {
        return &$rollBackAndError(__LINE__, 0x0003);
    }

    # - Machine toujours sur le contrat
    if (defined $tabOptions->{'contractId'} && $tabOptions->{'contractId'} != $tabInfos->{'contract.id'})
    {
        return &$rollBackAndError(__LINE__, 0x0005);
    }

    # - Etat machine/Etat contrat
    # -- Récupération de l'état du contrat de la machine et du type de transport de livraison
    my $query = '
SELECT
    ETCODE AS `contract.state.id`,
    tsp_type AS `delivery.type`
FROM CONTRAT
LEFT JOIN f_transport ON (tsp_rct_id_to = CONTRATAUTO
                          AND tsp_sta_id = "' . LOC::Transport::STATE_ACTIVE . '")
WHERE CONTRATAUTO = ' . $tabInfos->{'contract.id'} . ';';

    my $tabInfosSup = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);

    # -- Machine doit être en livraison
    # -- Contrat doit être en pré contrat
    # -- Transport de livraison n'est pas un transfert
    if (($tabInfos->{'state.id'} ne STATE_DELIVERY && $tabInfos->{'state.id'} ne STATE_TRANSFER) ||
            $tabInfosSup->{'contract.state.id'} ne LOC::Contract::Rent::STATE_PRECONTRACT ||
            $tabInfosSup->{'delivery.type'} eq LOC::Transport::TYPE_SITESTRANSFER)
    {
        return &$rollBackAndError(__LINE__, 0x0006);
    }

    # Désattribution
    my $tabContractErrors = {};
    if(!&LOC::Contract::Rent::update($countryId, $tabInfos->{'contract.id'}, {'machine.id' => undef}, $userId,
                                     $tabContractErrors, $tabEndUpds))
    {
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabContractErrors->{'fatal'}))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);
        }
        # Modifications interdites ?
        if (&LOC::Util::in_array(0x0003, $tabContractErrors->{'fatal'}))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
        }
        if (&LOC::Util::in_array(0x0140, $tabContractErrors->{'fatal'}))
        {
            if (&LOC::Util::in_array(0x0003, $tabContractErrors->{'modules'}->{'machine'}->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, 0x0003);
            }
        }
        return &$rollBackAndError(__LINE__);
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            return &$rollBackAndError(__LINE__);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}

# Function: getInfos
# Retourne les infos d'une machine
#
# Contenu du hashage :
# - id => identifiant de la machine
# - model.id => identifiant du modèle de machine
# - model.label => désignation du modèle de machine
# - agency.id => identifiant de l'agence
# - country.id => identifiant du pays
# - parkNumber => numéro de parc
# - serialNumber => numéro de série
# - purchaseDate => date d'achat
# - crashDate => date de dernière panne
# - contract.id => identifiant du dernier contrat auquel a été attribuée la machine
# - isOutOfPark => booléen indiquant si la machine est hors parc (1) ou non (0)
# - isSiteBlocked => booléen indiquant si la machine est bloquée sur le chantier (1) ou non (0)
# - isImmobilized => booléen indiquant si la machine est immobilisée (1) ou non (0) - information traqueur
# - isCreationFinished => booléen indiquant si la création de la machine a été finalisée (1) ou non (0)
# - siteBlockedComment => Commentaire du blocage de la machine sur chantier
# - state.id => code de l'état
# - machinesFamily.id => flag ($level) = LEVEL_MACHINESFAMILY ou LEVEL_BUSINESSFAMILY ou LEVEL_FAMILY
# - model.fullName => libellé complet (famille de machines, constructeur, libellé du modèle) = LEVEL_MACHINESFAMILY ou LEVEL_BUSINESSFAMILY ou LEVEL_FAMILY
# - businessFamily.id => flag ($level) = LEVEL_BUSINESSFAMILY ou LEVEL_FAMILY
# - family.id => flag ($level) = LEVEL_FAMILY
#
#
# Parameters:
# string  $countryId  - Pays
# string  $id         - Identifiant de la machine
# int     $flags      - Flags (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: exists
# Vérifie si la machine correspondant à l'identifiant passé en paramètre existe.
#
# Parameters:
# string  $id - Identifiant de la machine
#
# Returns:
# int - 1 si la machine existe, 0 sinon.
sub exists
{
    my ($id) = @_;

    if ($id eq '')
    {
        return 0;
    }

    if (!defined $_tabExists->{$id})
    {
        $_tabExists->{$id} = (&getList(LOC::Util::GETLIST_COUNT, {'id' => $id}) > 0);
    }

    return $_tabExists->{$id};
}


# Function: _getLastContracts
# Retourne la liste des derniers contrats d'une machine
#
# Parameters:
# string  $countryId - Pays
# int     $machineId - Identifiant de la machine
# int     $number    - Nombre de contrats à récupérer
#
# Returns:
# hash|hashref|undef - Liste des contrats
sub getLastContracts
{
    my ($countryId, $machineId, $number) = @_;

    # connexion à AUTH
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Liste des pays
    my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);

    # Récupération de tous les contrats sur lesquels la machine est attribuée
    my $query = '';
    my $nbCountries = @$tabCountries;
    for (my $i = 0; $i < $nbCountries; $i++)
    {
        my $schema = &LOC::Db::getSchemaName($tabCountries->[$i]);

        $query .= '
(SELECT
    agc_cty_id AS `country.id`,
    C.CONTRATAUTO AS `id`,
    C.CONTRATCODE AS `code`,
    CL.CLAUTO AS `customer.id`,
    CL.CLCODE AS `customer.code`,
    CL.CLSTE AS `customer.name`,
    C.CONTRATDD AS `beginDate`,
    C.CONTRATDR AS `endDate`,
    C.CONTRATDUREE AS `duration`,
    C.CONTRATDATECREATION AS `creationDate`,
    1 AS `isAffected`,
    IF(C.CONTRATAUTO = M.CONTRATAUTO AND
        (M.ETCODE IN ("' . STATE_DELIVERY . '", "' . STATE_TRANSFER . '") AND C.ETCODE = "' . LOC::Contract::Rent::STATE_PRECONTRACT . '" OR
         M.ETCODE = "' . STATE_RENTED . '" AND C.ETCODE = "' . LOC::Contract::Rent::STATE_CONTRACT . '" OR
         M.ETCODE = "' . STATE_RECOVERY . '" AND C.ETCODE = "' . LOC::Contract::Rent::STATE_STOPPED . '"), 1, 0) AS `isCurrent`,
    IF (M.ETCODE IN ("' . STATE_DELIVERY . '", "' . STATE_TRANSFER . '")
        AND C.ETCODE = "' . LOC::Contract::Rent::STATE_PRECONTRACT . '"
        AND T.tsp_type != "' . LOC::Transport::TYPE_SITESTRANSFER . '", 1, 0) AS `isUnaffectPossible`
FROM ' . $schema . '.CONTRAT C
LEFT JOIN ' . $schema . '.CLIENT CL ON C.CLAUTO = CL.CLAUTO
LEFT JOIN frmwrk.p_agency ON C.AGAUTO = agc_id
LEFT JOIN AUTH.MACHINE M ON C.MAAUTO = M.MAAUTO
LEFT JOIN ' . $schema . '.f_transport T ON (T.tsp_rct_id_to = C.CONTRATAUTO AND T.tsp_sta_id = "' . LOC::Transport::STATE_ACTIVE . '")
WHERE C.MAAUTO = ' . $machineId . ')';
        if ($i < $nbCountries - 1)
        {
            $query .= '
UNION';
        }
    }

    $query .= '
ORDER BY `creationDate` DESC';

    if ($number > 0)
    {
        $query .= '
LIMIT ' . $number . ';';
    }

    my $tab = $db->fetchList(LOC::Util::GETLIST_ASSOC, $query, {}, 'id');
    if (!$tab)
    {
        return 0;
    }

    # Recherche du dernier contrat auquel la machine a été attribuée s'il ne figure pas déjà dans la liste
    my $tabMachineInfos = &getInfos($countryId, $machineId);
    my $lastContractId = $tabMachineInfos->{'contract.id'};
    if (!(grep { $_ eq $lastContractId } keys(%$tab)))
    {
        $query = '
SELECT
    agc_cty_id AS `country.id`,
    C.CONTRATAUTO AS `id`,
    C.CONTRATCODE AS `code`,
    CL.CLAUTO AS `customer.id`,
    CL.CLCODE AS `customer.code`,
    CL.CLSTE AS `customer.name`,
    C.CONTRATDD AS `beginDate`,
    C.CONTRATDR AS `endDate`,
    C.CONTRATDUREE AS `duration`,
    C.CONTRATDATECREATION AS `creationDate`,
    0 AS `isAffected`,
    IF((M.ETCODE = "' . STATE_DELIVERY . '" OR M.ETCODE = "' . STATE_TRANSFER . '" AND C.MAAUTO = M.MAAUTO)
            AND C.ETCODE = "' . LOC::Contract::Rent::STATE_PRECONTRACT . '" OR
       M.ETCODE = "' . STATE_RENTED . '" AND C.ETCODE = "' . LOC::Contract::Rent::STATE_CONTRACT . '" OR
       M.ETCODE = "' . STATE_RECOVERY . '" AND C.ETCODE = "' . LOC::Contract::Rent::STATE_STOPPED . '", 1, 0) AS `isCurrent`,
    IF ((M.ETCODE = "' . STATE_DELIVERY . '" OR M.ETCODE = "' . STATE_TRANSFER . '" AND C.MAAUTO = M.MAAUTO)
        AND C.ETCODE = "' . LOC::Contract::Rent::STATE_PRECONTRACT . '"
        AND T.tsp_type != "' . LOC::Transport::TYPE_SITESTRANSFER . '", 1, 0) AS `isUnaffectPossible`
FROM AUTH.MACHINE M
LEFT JOIN CONTRAT C ON C.CONTRATAUTO = M.CONTRATAUTO
LEFT JOIN CLIENT CL ON C.CLAUTO = CL.CLAUTO
LEFT JOIN frmwrk.p_agency ON C.AGAUTO = agc_id
LEFT JOIN f_transport T ON (T.tsp_rct_id_to = C.CONTRATAUTO AND T.tsp_sta_id = "' . LOC::Transport::STATE_ACTIVE . '")
WHERE M.MAAUTO = ' . $machineId . '';

        my $tabLastContract = $db->fetchList(LOC::Util::GETLIST_ASSOC, $query, {}, 'id');
        if (!$tabLastContract)
        {
            return 0;
        }

        # Ajout du contrat et tri par date de création
        foreach my $contractId (keys(%$tabLastContract))
        {
            $tab->{$contractId} = $tabLastContract->{$contractId};
        }
        tie(my %newTab, 'Tie::IxHash');
        foreach my $contractId (sort { -($tab->{$a}->{'creationDate'} cmp $tab->{$b}->{'creationDate'}) } keys(%$tab))
        {
            $newTab{$contractId} = $tab->{$contractId};
        }
        $tab = \%newTab;
    }

    return (wantarray ? %$tab : $tab);
}


# Function: getList
# Retourne la liste des machines
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres (facultatif)
# int     $flags      - Flags pour le choix des colonnes (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Liste des machines
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);


    my $query;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        my $fields = '';
        my $joins = '';


        if (!($flags & GETINFOS_ALL) && defined $tabFilters->{'areaId'})
        {
            $flags = $flags | GETINFOS_ALL;
        }

        # Infos suivant le niveau de détail demandé
        if ($flags & GETINFOS_ALL)
        {
            $fields .= ',
    CpyInCde,
    CpyAddrInCde,
    are_id AS `area.id`,
    are_label AS `area.label`,
    cty_label AS `country.label`,
    tbl_p_state.ETLIBELLE AS `state.nonLocaleLabel`';


            $joins .= '
LEFT JOIN AUTH.AGENCE tbl_p_agency
ON tbl_f_machine.AGAUTO = tbl_p_agency.AGAUTO
LEFT JOIN frmwrk.p_area
ON tbl_f_machineagency.agc_are_id = are_id
LEFT JOIN frmwrk.p_country
ON tbl_f_machineagency.agc_cty_id = cty_id
LEFT JOIN LOCATION.ETATTABLE tbl_p_state
ON tbl_f_machine.ETCODE = tbl_p_state.ETCODE';

        }


        $query = '
SELECT
    tbl_f_machine.MAAUTO AS `id`,
    tbl_f_machine.MOMAAUTO AS `model.id`,
    tbl_p_model.MOMADESIGNATION AS `model.label`,
    tbl_p_model.MOMALNG AS `model.length`,
    tbl_p_model.MOMALG AS `model.width`,
    tbl_p_model.MOMAHTPLIEE AS `model.height`,
    tbl_p_model.MOMAHTREPOS AS `model.restHeight`,
    tbl_p_model.MOMAPOIDS AS `model.weight`,
    tbl_f_machine.AGAUTO AS `agency.id`,
    tbl_f_machineagency.agc_label AS `agency.label`,
    tbl_f_machineagency.agc_cty_id AS `country.id`,
    CONCAT(IFNULL(tbl_f_machine.MANOPARC_AUX, ""), tbl_f_machine.MANOPARC) AS `parkNumber`,
    tbl_f_machine.MANOSERIE AS `serialNumber`,
    tbl_f_machine.MADATEACHAT AS `purchaseDate`,
    tbl_f_machine.MADATEPANNE AS `crashDate`,
    tbl_f_machine.CONTRATAUTO AS `contract.id`,
    -tbl_f_machine.MAHORSPARC AS `isOutOfPark`,
    -tbl_f_machine.MABLOQUECH AS `isSiteBlocked`,
    tbl_f_machine.MABLOQUECHCOMM AS `siteBlockedComment`,
    tbl_f_machine.MASUPPAGAUTO AS `visibleOnAgency.id`,
    tbl_f_visibilityagency.agc_label AS `visibleOnAgency.label`,
    tbl_f_machine.MASUPPAGMODE AS `visibilityMode`,
    -tbl_f_machine.MACREATIONOK AS `isCreationFinished`,
    tbl_f_machine.ETCODE AS `state.id`,
    tbl_f_machine.MADATEMODIF AS `modificationDate`';

    # Informations sur la télématique
    if ($flags & GETINFOS_TELEMATICS)
    {
        $query .= ',
    -tbl_f_machine.MAIMMOBILISATION AS `isImmobilized`,
    tbl_f_machine.IDTarget AS `telematicId`,
    -tbl_f_machine.MATELEMATSUSP AS `isTelematicSuspended`,
    tbl_f_machine.MAPORTEETELEMAT AS `telematicScope`,
    -tbl_f_machine.MADIGIINACTIF AS `isDigiKeyDeactivated`';
    }

    $query .= $fields . '
FROM AUTH.MACHINE tbl_f_machine
LEFT JOIN AUTH.MODELEMACHINE tbl_p_model
ON tbl_f_machine.MOMAAUTO = tbl_p_model.MOMAAUTO
LEFT JOIN frmwrk.p_agency tbl_f_machineagency
ON tbl_f_machine.AGAUTO = tbl_f_machineagency.agc_id
LEFT JOIN frmwrk.p_agency tbl_f_visibilityagency
ON tbl_f_machine.MASUPPAGAUTO = tbl_f_visibilityagency.agc_id' .
$joins;
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*)
FROM AUTH.MACHINE tbl_f_machine
LEFT JOIN frmwrk.p_agency tbl_f_machineagency
ON tbl_f_machine.AGAUTO = tbl_f_machineagency.agc_id';
    }
    else
    {
        $query = '
SELECT
    tbl_f_machine.MAAUTO,
    CONCAT(IFNULL(tbl_f_machine.MANOPARC_AUX, ""), tbl_f_machine.MANOPARC)
FROM AUTH.MACHINE tbl_f_machine
LEFT JOIN frmwrk.p_agency tbl_f_machineagency
ON tbl_f_machine.AGAUTO = tbl_f_machineagency.agc_id';
    }

    # Jointures pour les recherches
    if (defined $tabFilters->{'stateId'} && $tabFilters->{'isVisibleMachineInTransfer'} && $tabFilters->{'visibleOnAgencyId'})
    {
        $query .= '
LEFT JOIN CONTRAT tbl_f_contract
ON (tbl_f_machine.CONTRATAUTO = tbl_f_contract.CONTRATAUTO AND tbl_f_machine.MAAUTO = tbl_f_contract.MAAUTO)';
    }

    # Filtres
    $query .= '
WHERE 1';

    # - Id
    if (defined $tabFilters->{'id'})
    {
        $query .= '
AND tbl_f_machine.MAAUTO IN (' . &LOC::Util::join(', ', $tabFilters->{'id'}) . ')';
    }
    # - Numéro de parc
    if (defined $tabFilters->{'parkNumber'})
    {
        $query .= '
AND (tbl_f_machine.MANOPARC = ' . $db->quote($tabFilters->{'parkNumber'}) . ' OR
     CONCAT(tbl_f_machine.MANOPARC_AUX, tbl_f_machine.MANOPARC) = ' . $db->quote($tabFilters->{'parkNumber'}) . ')';
    }
    # - Modèle de machine
    if (defined $tabFilters->{'modelId'})
    {
        $query .= '
AND tbl_f_machine.MOMAAUTO IN (' . &LOC::Util::join(', ', $tabFilters->{'modelId'}) . ')';
    }
    # - Hors parc ?
    if (defined $tabFilters->{'isOutOfPark'})
    {
        $query .= '
AND tbl_f_machine.MAHORSPARC = ' . $tabFilters->{'isOutOfPark'};
    }
    # - Pays
    if ($tabFilters->{'countryId'} ne '')
    {
        $query .= '
AND tbl_f_machineagency.agc_cty_id IN (' . $db->quote($tabFilters->{'countryId'}) . ')';
    }
    # - Agence
    if (defined $tabFilters->{'agencyId'})
    {
        $query .= '
AND tbl_f_machine.AGAUTO IN (' . $db->quote($tabFilters->{'agencyId'}) . ')';
    }
    # - Agence de visibilité
    if (defined $tabFilters->{'visibleOnAgencyId'})
    {
        $query .= '
AND ((tbl_f_machine.AGAUTO IN (' . $db->quote($tabFilters->{'visibleOnAgencyId'}) . ') AND tbl_f_machine.MASUPPAGAUTO IS NULL) OR ' .
    'tbl_f_machine.MASUPPAGAUTO IN (' . $db->quote($tabFilters->{'visibleOnAgencyId'}) . '))';
    }
    # - Etats
    if (defined $tabFilters->{'stateId'})
    {
        if ($tabFilters->{'isVisibleMachineInTransfer'} && $tabFilters->{'visibleOnAgencyId'})
        {
            $query .= '
AND (tbl_f_machine.ETCODE IN (' . &LOC::Util::join(', ', $db->quote($tabFilters->{'stateId'})) . ') OR
(tbl_f_machine.ETCODE = ' . $db->quote(STATE_TRANSFER) . ' AND
 tbl_f_machine.MASUPPAGAUTO IN (' . $db->quote($tabFilters->{'visibleOnAgencyId'}) . ') AND
 (tbl_f_contract.CONTRATAUTO IS NULL OR tbl_f_contract.ETCODE NOT IN (' . $db->quote(LOC::Contract::Rent::STATE_PRECONTRACT) . '))))';

        }
        else
        {
            $query .= '
AND tbl_f_machine.ETCODE IN (' . &LOC::Util::join(', ', $db->quote($tabFilters->{'stateId'})) . ')';
        }
    }
    # - Famille de machine
    if (defined $tabFilters->{'familyId'})
    {
        $query .= '
AND tbl_p_model.FAMAAUTO IN (' . &LOC::Util::join(', ', $tabFilters->{'familyId'}) . ')';
    }
    # - Famille tarifaire de la machine
    if (defined $tabFilters->{'tariffFamilyId'})
    {
        $query .= '
AND tbl_p_model.FAMTARAUTO IN (' . &LOC::Util::join(', ', $tabFilters->{'tariffFamilyId'}) . ')';
    }
    # - Fournisseurs
    if (defined $tabFilters->{'manufacturerId'})
    {
        $query .= '
AND tbl_p_model.FOAUTO IN (' . &LOC::Util::join(', ', $tabFilters->{'manufacturerId'}) . ')';
    }

    # - N° série
    if (defined $tabFilters->{'serialNumber'})
    {
        $query .= '
AND tbl_f_machine.MANOSERIE IN (' . &LOC::Util::join(', ', $db->quote($tabFilters->{'serialNumber'})) . ')';
    }

    # - Régions
    if (defined $tabFilters->{'areaId'})
    {
        $query .= '
AND are_id IN (' . &LOC::Util::join(', ', $db->quote($tabFilters->{'areaId'})) . ')';
    }
    # - Bloqué sur chantier ?
    if (defined $tabFilters->{'isSiteBlocked'})
    {
        $query .= '
AND tbl_f_machine.MABLOQUECH = ' . $tabFilters->{'isSiteBlocked'};
    }
    # - Machine équipée de la télématique ?
    if (defined $tabFilters->{'isTelematic'})
    {
        $query .= '
AND tbl_f_machine.IDTarget ' . ($tabFilters->{'isTelematic'} ? 'IS NOT NULL' : 'IS NULL');
    }
    # - Portée télématique ?
    if (defined $tabFilters->{'telematicScope'})
    {
        $query .= '
AND tbl_f_machine.MAPORTEETELEMAT LIKE ' . $db->quote('%"' . $tabFilters->{'telematicScope'} . '"%');
    }
    # - Créée dans Kimoce ?
    if (defined $tabFilters->{'isCreationFinished'})
    {
        $query .= '
AND tbl_f_machine.MACREATIONOK = ' . $tabFilters->{'isCreationFinished'};
    }
    # - Avec un code de démarrage
    if (defined $tabFilters->{'startCode'})
    {
        $query .= '
AND (
    SELECT COUNT(*)
    FROM f_startcoderule';

    if (defined $tabFilters->{'startCode'}->{'stateId'})
    {
        $query .= '
    INNER JOIN f_startcode ON scr_sco_id = sco_id';
    }

        $query .= '
    WHERE scr_mac_id = tbl_f_machine.MAAUTO';
        if (defined $tabFilters->{'startCode'}->{'stateId'})
        {
            $query .= '
    AND sco_sta_id IN (' . &LOC::Util::join(', ', $db->quote($tabFilters->{'startCode'}->{'stateId'})) . ')';
        }
        if (defined $tabFilters->{'startCode'}->{'rule'} && defined $tabFilters->{'startCode'}->{'rule'}->{'stateId'})
        {
            $query .= '
    AND scr_sta_id IN (' . &LOC::Util::join(', ', $db->quote($tabFilters->{'startCode'}->{'rule'}->{'stateId'})) . ')';
        }
        $query .= '
) > 0';
    }

    $query .= '
ORDER BY tbl_f_machine.MANOPARC;';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        # Formatage des données
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}              *= 1;
            $tabRow->{'model.id'}        *= 1;
            $tabRow->{'model.length'}    *= 1;
            $tabRow->{'model.width'}     *= 1;
            $tabRow->{'contract.id'}     *= 1;
            $tabRow->{'isOutOfPark'}     *= 1;
            $tabRow->{'isSiteBlocked'}   *= 1;
            $tabRow->{'visibleOnAgency.id'} = ($tabRow->{'visibleOnAgency.id'} ne '' ? $tabRow->{'visibleOnAgency.id'} : undef);
            $tabRow->{'visibilityMode'}  *= 1;
            $tabRow->{'isCreationFinished'} *= 1;

            my $schemaLoc = &LOC::Db::getSchemaName($tabRow->{'country.id'});

            my @tabFields = ();
            my @tabJoins = ();

            # la machine est-elle attribuée ou associée à un précontrat ?
            my $query = '
SELECT
    COUNT(*)
FROM ' . $schemaLoc . '.CONTRAT
WHERE MAAUTO = ' . $tabRow->{'id'} . '
AND ETCODE = ' . $db->quote(LOC::Contract::Rent::STATE_PRECONTRACT);

            my $nbResult = $db->fetchOne($query) * 1;
            $tabRow->{'isAffectedOnPrecontract'} = ($nbResult == 1 ? 1 : 0);


            if ($flags & GETINFOS_ALL)
            {
                if ($tabRow->{'country.id'} ne 'FR')
                {
                    push(@tabFields, 'tbl_p_state.ETLIBELLE AS `state.label`');
                    push(@tabJoins, 'LEFT JOIN ' . $schemaLoc . '.ETATTABLE tbl_p_state
                                     ON tbl_p_state.ETCODE = tbl_f_machine.ETCODE ');
                }
            }

            if (defined $tabOptions->{'level'})
            {
                my $level = $tabOptions->{'level'};

                push(@tabFields, 'tbl_p_machinescontrol.COMAAPAVE AS `control.nextDate`');
                push(@tabFields, '0 AS `control.score`');
                push(@tabFields, 'tbl_p_machinescontrol.COMADAPAVE AS `control.lastDate`');
                push(@tabFields, 'tbl_p_model.MOMACONTROLEVGP AS `isControlVgp`');
                push(@tabJoins, 'LEFT JOIN ' . $schemaLoc . '.CONTROLEMACHINE tbl_p_machinescontrol
                                 ON tbl_p_machinescontrol.MAAUTO = tbl_f_machine.MAAUTO');
                push(@tabJoins, 'LEFT JOIN AUTH.MODELEMACHINE tbl_p_model
                                 ON tbl_p_model.MOMAAUTO = tbl_f_machine.MOMAAUTO');
                # À partir du niveau "modèle de machines"
                if ($level >= LEVEL_MODEL)
                {
                    push(@tabFields, 'IF(tbl_p_model.MOMATYPAGE LIKE "%\"' . LOC::Model::TYPE_ACCESSORY . '\"%", 1, 0) AS `isAccessory`');
                    push(@tabFields, 'tbl_p_model.MOMAPERIODICITEVGP AS `periodicityVgp`');
                    push(@tabFields, 'IF (STMORESERVABLE > 0 AND STMORESERVABLE > STMORESERVE, 1, 0) AS `model.isRentable`');

                    if ($flags & GETINFOS_ALL)
                    {
                        push(@tabFields, 'tbl_p_model.MOMACAPACITE AS `model.capacity`');
                        push(@tabFields, 'FO.FOAUTO AS `manufacturer.id`');
                        push(@tabFields, 'FO.FOLIBELLE AS `manufacturer.label`');
                    }

                    push(@tabJoins, 'LEFT JOIN AUTH.STOCKMODELE tbl_f_modelstock
                                     ON (tbl_p_model.MOMAAUTO = tbl_f_modelstock.MOMAAUTO
                                         AND tbl_f_modelstock.AGAUTO = ' . $db->quote($tabRow->{'agency.id'}) . ')');
                    push(@tabJoins, 'LEFT JOIN ' . $schemaLoc . '.FOURNISSEUR FO
                                     ON tbl_p_model.FOAUTO = FO.FOAUTO');

                    # À partir du niveau "famille de machines"
                    if ($level >= LEVEL_MACHINESFAMILY)
                    {
                        push(@tabFields, 'tbl_p_machinesfamily.FAMAAUTO AS `machinesFamily.id`');
                        push(@tabFields, 'tbl_p_machinesfamily.FAMAMODELE AS `machinesFamily.shortLabel`');
                        push(@tabFields, 'CONCAT_WS(" ", tbl_p_machinesfamily.FAMAMODELE,
                                                         tbl_p_machinesfamily.FAMAENERGIE,
                                                         tbl_p_machinesfamily.FAMAELEVATION) AS `machinesFamily.label`');
                        push(@tabFields, 'tbl_p_machinesfamily.FAMAENERGIE AS `machinesFamily.energy`');
                        push(@tabFields, 'CONCAT_WS(" ", tbl_p_machinesfamily.FAMAMODELE,
                                                         tbl_p_machinesfamily.FAMAENERGIE,
                                                         tbl_p_machinesfamily.FAMAELEVATION,
                                                         FO.FOLIBELLE,
                                                         tbl_p_model.MOMADESIGNATION) AS `model.fullName`');

                        push(@tabJoins, 'LEFT JOIN ' . $schemaLoc . '.FAMILLEMACHINE tbl_p_machinesfamily
                                         ON tbl_p_model.FAMAAUTO = tbl_p_machinesfamily.FAMAAUTO');

                        if ($flags & GETINFOS_ALL)
                        {
                            push(@tabFields, 'tbl_p_machinesfamily.FAMAELEVATION AS `machinesFamily.elevation`');
                        }

                        # À partir du niveau "famille commerciale"
                        if ($level >= LEVEL_BUSINESSFAMILY)
                        {
                            push(@tabFields, 'tbl_p_businessfamily.FACOMAUTO AS `businessFamily.id`');
                            push(@tabFields, 'tbl_p_businessfamily.FACOMLIBELLE AS `businessFamily.label`');

                            push(@tabJoins, 'LEFT JOIN ' . $schemaLoc . '.FAMILLECOMMERCIALE tbl_p_businessfamily
                                             ON tbl_p_machinesfamily.FACOMAUTO = tbl_p_businessfamily.FACOMAUTO');

                            # À partir du niveau "famille"
                            if ($level >= LEVEL_FAMILY)
                            {
                                # Récupération de la locale
                                my $tabCountryInfos = &LOC::Country::getInfos($tabRow->{'country.id'});

                                push(@tabFields, 'p_family.fly_id AS `family.id`');
                                push(@tabFields, 'fll_fullname AS `family.fullName`');
                                push(@tabFields, 'tbl_p_accountingfamily.FAMACOMPTAAUTO AS `accountingFamily.id`');
                                push(@tabFields, 'tbl_p_accountingfamily.FAMACOMPTA AS `accountingFamily.label`');
                                push(@tabFields, 'tbl_p_tarifffamily.FAMTARAUTO AS `tariffFamily.id`');
                                push(@tabFields, 'CONCAT(tbl_p_tarifffamily.FAMTARLIBELLE,
                                                         " ",
                                                         tbl_p_tarifffamily.FAMTARSOUSLIBELLE) AS `tariffFamily.label`');

                                push(@tabJoins, 'LEFT JOIN AUTH.p_family
                                                 ON tbl_p_businessfamily.fly_id = p_family.fly_id');
                                push(@tabJoins, 'LEFT JOIN AUTH.p_family_locale
                                                 ON (p_family.fly_id = fll_fly_id
                                                      AND fll_lcl_id = ' . $db->quote($tabCountryInfos->{'locale.id'}) . ')');
                                push(@tabJoins, 'LEFT JOIN ' . $schemaLoc . '.FAMILLECOMPTA tbl_p_accountingfamily
                                                 ON tbl_p_machinesfamily.FAMACOMPTAAUTO = tbl_p_accountingfamily.FAMACOMPTAAUTO');
                                push(@tabJoins, 'LEFT JOIN ' . $schemaLoc . '.FAMILLETARIFAIRE tbl_p_tarifffamily
                                                 ON tbl_p_model.FAMTARAUTO = tbl_p_tarifffamily.FAMTARAUTO');
                            }
                        }
                   }
                }
            }

            # Récupération de toutes les informations supplémentaires
            if (@tabFields > 0)
            {
                my $query = '
SELECT
' . join(",\n", @tabFields) . '
FROM AUTH.MACHINE tbl_f_machine
' . join("\n", @tabJoins) . '
WHERE tbl_f_machine.MAAUTO = ' . $tabRow->{'id'};

                my $tabSupInfos = $db->fetchList($format, $query);
                # Formatage des données
                foreach my $supInfo (@$tabSupInfos)
                {
                    if ($flags & GETINFOS_ALL)
                    {
                        if ($tabRow->{'country.id'} eq 'FR')
                        {
                             $tabRow->{'state.label'} = $tabRow->{'state.nonLocaleLabel'};
                        }
                        else
                        {
                            $tabRow->{'state.label'} = $supInfo->{'state.label'};
                        }
                    }
                    if (defined $tabOptions->{'level'})
                    {
                        my $level = $tabOptions->{'level'};
                        $tabRow->{'control.nextDate'} = $supInfo->{'control.nextDate'};
                        $tabRow->{'control.score'}    = $supInfo->{'control.score'} * 1;
                        $tabRow->{'control.lastDate'} = $supInfo->{'control.lastDate'};
                        $tabRow->{'isControlVgp'}     = $supInfo->{'isControlVgp'};

                        # Modèle faisant l'objet de VGP
                        if (!&LOC::Util::isNumeric($tabRow->{'isControlVgp'}))
                        {
                            my $tabIsControlVgp = &LOC::Json::fromJson($tabRow->{'isControlVgp'});
                            $tabRow->{'isControlVgp'} = $tabIsControlVgp->{$tabRow->{'country.id'}};
                        }

                        # Prochain contrôle
                        if ($tabRow->{'isControlVgp'} && LOC::Date::checkMySQLDate($tabRow->{'control.nextDate'}))
                        {
                            # Préparation de l'affichage de la date de prochain contrôle
                            my %tabCtrlMacDate = &LOC::Date::getHashDate($tabRow->{'control.nextDate'});
                            # préparation du score de prochaine date de contrôle
                            my $nbDaysNextCtrl = &LOC::Date::getDeltaDays(&LOC::Date::getMySQLDate(), $tabRow->{'control.nextDate'});

                            if ($nbDaysNextCtrl <= 0)
                            {
                                $tabRow->{'control.score'} = 4;
                            }
                            elsif ($nbDaysNextCtrl <= 7)
                            {
                                $tabRow->{'control.score'} = 3;
                            }
                            elsif ($nbDaysNextCtrl <= 15)
                            {
                                $tabRow->{'control.score'} = 2;
                            }
                            elsif ($nbDaysNextCtrl <= 60)
                            {
                                $tabRow->{'control.score'} = 1;
                            }
                        }

                        # À partir du niveau "modèle de machines"
                        if ($level >= LEVEL_MODEL)
                        {
                            $tabRow->{'isAccessory'}       = $supInfo->{'isAccessory'};
                            $tabRow->{'periodicityVgp'}    = $supInfo->{'periodicityVgp'};
                            # Modèle réservable
                            $tabRow->{'model.isRentable'}  = $supInfo->{'model.isRentable'} * 1;

                            if ($flags & GETINFOS_ALL)
                            {
                                $tabRow->{'model.capacity'}      = $supInfo->{'model.capacity'};
                                $tabRow->{'manufacturer.id'}     = $supInfo->{'manufacturer.id'};
                                $tabRow->{'manufacturer.label'}  = $supInfo->{'manufacturer.label'};
                            }

                            # Modèle faisant l'objet de VGP - Périodicité
                            if ($tabRow->{'isControlVgp'} && !&LOC::Util::isNumeric($tabRow->{'periodicityVgp'}))
                            {
                                my $tabPeriodicityVgp = &LOC::Json::fromJson($tabRow->{'periodicityVgp'});
                                $tabRow->{'periodicityVgp'} = $tabPeriodicityVgp->{$tabRow->{'country.id'}};
                            }

                            # À partir du niveau "famille de machines"
                            if ($level >= LEVEL_MACHINESFAMILY)
                            {
                                $tabRow->{'machinesFamily.id'}      = $supInfo->{'machinesFamily.id'};
                                $tabRow->{'machinesFamily.shortLabel'} = $supInfo->{'machinesFamily.shortLabel'};
                                $tabRow->{'machinesFamily.label'}   = $supInfo->{'machinesFamily.label'};
                                $tabRow->{'machinesFamily.energy'}  = $supInfo->{'machinesFamily.energy'};
                                $tabRow->{'model.fullName'}         = $supInfo->{'model.fullName'};

                                if ($flags & GETINFOS_ALL)
                                {
                                    $tabRow->{'machinesFamily.elevation'} = $supInfo->{'machinesFamily.elevation'};
                                }

                                # À partir du niveau "famille commerciale"
                                if ($level >= LEVEL_BUSINESSFAMILY)
                                {
                                    $tabRow->{'businessFamily.id'}    = $supInfo->{'businessFamily.id'};
                                    $tabRow->{'businessFamily.label'} = $supInfo->{'businessFamily.label'};

                                    if ($level >= LEVEL_FAMILY)
                                    {
                                        $tabRow->{'family.id'}              = $supInfo->{'family.id'};
                                        $tabRow->{'family.fullName'}        = $supInfo->{'family.fullName'};
                                        $tabRow->{'accountingFamily.id'}    = $supInfo->{'accountingFamily.id'};
                                        $tabRow->{'accountingFamily.label'} = $supInfo->{'accountingFamily.label'};
                                        $tabRow->{'tariffFamily.id'}        = $supInfo->{'tariffFamily.id'};
                                        $tabRow->{'tariffFamily.label'}     = $supInfo->{'tariffFamily.label'};
                                    }
                                }
                            }
                        }
                    }
                }
            }

            # Informations sur les équipements
            if ($flags & GETINFOS_ADDINFOS)
            {
                # Récupération de la liste des valeurs des équipements
                my $query = '
SELECT
    inf_id,
    min_value
FROM AUTH.f_machine_info
INNER JOIN AUTH.p_info
ON min_inf_id = inf_id
WHERE min_mac_id = ' . $tabRow->{'id'}. ';';

                $tabRow->{'tabAddInfos'} = $db->fetchPairs($query);
            }

            # Informations transfert inter-agences
            if ($flags & GETINFOS_AGENCIESTRANSFER)
            {
                # Liste des pays
                my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);

                foreach my $country (@$tabCountries)
                {
                    my $tabTransfers = &LOC::Transport::getAgenciesTransferInfos($country, $tabRow->{'id'},
                                                                                 LOC::Transport::GETINFOS_MACHINE |
                                                                                 LOC::Transport::GETINFOS_ORDER);

                    if ($tabTransfers)
                    {
                        $tabRow->{'agenciesTransfer.tabInfos'} = $tabTransfers;
                        last;
                    }
                }
            }

            # Informations commande VGP
            if ($flags & GETINFOS_CONTROLORDER)
            {
                my $query = '
SELECT
    tbl_f_controlorder.COMMANDEAUTO AS `id`,
    COMMDATEJOUR AS `date`,
    COMMDATEENTREE AS `controlDate`,
    COMMANDEMONTANT AS `amount`,
    ORGALIBELLE AS `organization.label`,
    ORGATELEPHONE AS `organization.telephone`,
    ORGATELECOPIE AS `organization.fax`,
    ORGACONTACT AS `organization.contact`,
    ORGAADD AS `organization.address`,
    ORGACP AS `organization.postalCode`,
    ORGAVILLE AS `organization.city`,
    AGLIBELLE AS `organization.agency.label`,
    AGADRESSE AS `organization.agency.address`,
    AGCP AS `organization.agency.postalCode`,
    AGVILLE AS `organization.agency.city`,
    AGTEL AS `organization.agency.telephone`,
    AGFAX AS `organization.agency.fax`
FROM CONTROLEMACHINE tbl_f_controlmachine
LEFT JOIN COMMANDE tbl_f_controlorder ON tbl_f_controlorder.COMMANDEAUTO = (SELECT COMMANDEAUTO FROM COMMCONT
                                                                            WHERE COMAAUTO = tbl_f_controlmachine.COMAAUTO
                                                                            ORDER BY COMMANDEAUTO DESC LIMIT 1)
LEFT JOIN ORGANISMECONTROLE tbl_f_controlorganization ON tbl_f_controlorganization.ORGAAUTO = tbl_f_controlmachine.ORGAAUTO
LEFT JOIN AUTH.AGENCE tbl_f_agency ON tbl_f_agency.AGAUTO = tbl_f_controlorganization.AGAUTO
WHERE tbl_f_controlmachine.MAAUTO = ' . $tabRow->{'id'} . ';';

                my $tabControlOrderInfos = $db->fetchRow($query, {}, $format);
                # Formatage des données
                $tabControlOrderInfos->{'id'}    *= 1;
                $tabControlOrderInfos->{'amount'} = &LOC::Util::round($tabControlOrderInfos->{'amount'} * 1, 2);

                $tabRow->{'tabControlOrderInfos'} = $tabControlOrderInfos;
            }

            # Informations sur la télématique
            if ($flags & GETINFOS_TELEMATICS)
            {
                # Est-ce que la télématique est activée ?
                my $isTelematicsActived = &_isTelematActived($tabRow->{'country.id'});

                if ($isTelematicsActived)
                {
                    $tabRow->{'isImmobilized'}        *= 1;
                    $tabRow->{'telematicId'}           = ($tabRow->{'telematicId'} ? $tabRow->{'telematicId'} * 1 : undef);
                    $tabRow->{'telematicScope'}        = ($tabRow->{'telematicScope'} ne '' ? &LOC::Json::fromJson($tabRow->{'telematicScope'}) : []);
                    $tabRow->{'isDigiKeyDeactivated'} *= 1;
                }
                else
                {
                    $tabRow->{'isImmobilized'}        = undef;
                    $tabRow->{'telematicId'}          = undef;
                    $tabRow->{'telematicScope'}       = [];
                    $tabRow->{'isDigiKeyDeactivated'} = 0;
                }
            }

            # Informations sur le dernier passage en agence
            if ($flags & GETINFOS_LASTAGENCYCHECKIN)
            {
                $tabRow->{'lastAgencyCheckIn'} = &getLastAgencyCheckIn($countryId, $tabRow->{'id'});
            }

            # Possibilités
            if ($flags & GETINFOS_POSSIBILITIES)
            {
                $tabRow->{'possibilities'} = {};

                # Informations sur l'utilisateur
                my $tabUserInfos = &LOC::Session::getUserInfos();
                my $isUserAdmin  = $tabUserInfos->{'isAdmin'};
                my $isUserSuperv = $tabUserInfos->{'isSuperv'};
                my @tabAuthorizedAgencies = keys(%{$tabUserInfos->{'tabAuthorizedAgencies'}});

                my $isAuthorizedAgency = ($isUserAdmin || &LOC::Util::in_array($tabRow->{'agency.id'},
                                                                               \@tabAuthorizedAgencies));

                # Date de dernier contrôle :
                # - L'agence de la machine doit faire partie des agences nomades de l'utilisateur
                # - Le modèle de machines doit faire l'objet de VGP
                if ($isAuthorizedAgency && $tabRow->{'isControlVgp'})
                {
                    &LOC::Util::setTreeValues($tabRow->{'possibilities'}, {'actions' => ['control']}, 1);
                }

                # Machine bloquée sur chantier
                if ($isAuthorizedAgency &&
                    ($tabRow->{'state.id'} eq STATE_RENTED || $tabRow->{'state.id'} eq STATE_RECOVERY))
                {
                    &LOC::Util::setTreeValues($tabRow->{'possibilities'}, {'actions' => ['siteBlocked']}, 1);
                }

                # Hors parc et changement d'état
                if ($tabRow->{'state.id'} ne STATE_DELIVERY && $tabRow->{'state.id'} ne STATE_RENTED &&
                    $tabRow->{'state.id'} ne STATE_RECOVERY && $tabRow->{'state.id'} ne STATE_TRANSFER)
                {
                    &LOC::Util::setTreeValues($tabRow->{'possibilities'}, {'actions' => ['outOfPark']}, 1);
                }

                # Changement d'état
                # - Machine en visibilité ET état machine [Disponible, Révision, Vérification]
                # OU
                # Pas de visibilité et état machine différent de [Livraison, Louée, Récupération, En transfert]
                if ((($tabRow->{'state.id'} eq STATE_AVAILABLE || $tabRow->{'state.id'} eq STATE_REVIEW ||
                      $tabRow->{'state.id'} eq STATE_CHECK) && $tabRow->{'visibleOnAgency.id'}) ||
                       ($tabRow->{'state.id'} ne STATE_DELIVERY && $tabRow->{'state.id'} ne STATE_RENTED &&
                        $tabRow->{'state.id'} ne STATE_RECOVERY && $tabRow->{'state.id'} ne STATE_TRANSFER &&
                        !$tabRow->{'visibleOnAgency.id'})
                    )
                {
                    &LOC::Util::setTreeValues($tabRow->{'possibilities'}, {'actions' => ['state']}, 1);
                }

                # Visibilité sur une autre agence
                # - Machine faisant partie des agences nomades de l'utilisateur
                # ET
                # - pas dans une visibilité depuis le transport (VISIBILITYMODE_TRANSPORT)
                # ET
                # - état machine [Disponible, Révision, Vérification, Récupération] ou [Transfert avec visibilité et non attribué à un préco]
                # - les stocks le permettent :
                # -- pas de visu = modèle doit être réservable sur l'agence physique
                # -- visu sur X  = modèle doit être réservable sur X
                # - OU état Transfert ET pas de visibilité sur une autre agence

                # => modèle réservable ?
                my $stockAgencyId = ($tabRow->{'visibleOnAgency.id'} ? $tabRow->{'visibleOnAgency.id'} : $tabRow->{'agency.id'});
                my $modelIsRentable = &LOC::Model::isRentable($countryId, $stockAgencyId, $tabRow->{'model.id'});

                if ($isAuthorizedAgency &&
                      $tabRow->{'visibilityMode'} != VISIBILITYMODE_TRANSPORT &&
                      ((($tabRow->{'state.id'} eq STATE_AVAILABLE || $tabRow->{'state.id'} eq STATE_REVIEW ||
                         $tabRow->{'state.id'} eq STATE_CHECK || $tabRow->{'state.id'} eq STATE_RECOVERY ||
                        ($tabRow->{'state.id'} eq STATE_TRANSFER && $tabRow->{'visibleOnAgency.id'}))
                         && $modelIsRentable) ||
                      ($tabRow->{'state.id'} eq STATE_TRANSFER && !$tabRow->{'visibleOnAgency.id'})) &&
                      !$tabRow->{'isAffectedOnPrecontract'})
                {
                    &LOC::Util::setTreeValues($tabRow->{'possibilities'}, {'actions' => ['visibility']}, 1);
                }

                # Création de transfert inter-agences :
                # États autorisés
                my $tabAuthorizedStates = {
                    # - pour tout le monde
                    'standard' => [STATE_AVAILABLE, STATE_REVIEW, STATE_BROKEN, STATE_CHECK,
                                   STATE_TORETURNFR],
                    # - pour les superviseurs et les admins
                    'superv'   => [STATE_SOLD, STATE_TORECEIVE, STATE_FORSALE, STATE_FORSALEMC, STATE_FORSALEMN,
                                   STATE_FORSALERB, STATE_FORSALEEMT, STATE_FORSALEVTM, STATE_FORSALEGU, STATE_FORSALEUP,
                                   STATE_FORSALEVARIOUS, STATE_FORSALEIP, STATE_FORSALEMB, STATE_SALEMC, STATE_SALEMN,
                                   STATE_SALERB, STATE_SALEEMT, STATE_SALEVTM, STATE_SALEGU, STATE_SALEUP,
                                   STATE_SALEVARIOUS, STATE_SALEIP, STATE_SALEMB]
                };
                # - Pas de transfert en cours ou transfert non verrouillé et réalisé
                if ((!$tabRow->{'agenciesTransfer.tabInfos'} ||
                        !$tabRow->{'agenciesTransfer.tabInfos'}->{'isLocked'} &&
                            $tabRow->{'agenciesTransfer.tabInfos'}->{'loading'} &&
                            $tabRow->{'agenciesTransfer.tabInfos'}->{'unloading'} &&
                            $tabRow->{'agenciesTransfer.tabInfos'}->{'loading'}->{'isDone'} &&
                            $tabRow->{'agenciesTransfer.tabInfos'}->{'unloading'}->{'isDone'}) &&
                    # - État de la machine qui va bien
                    (&LOC::Util::in_array($tabRow->{'state.id'}, $tabAuthorizedStates->{'standard'}) ||
                        (&LOC::Util::in_array($tabRow->{'state.id'}, $tabAuthorizedStates->{'superv'})) &&
                            ($isUserSuperv || $isUserAdmin)) &&
                    # - L'agence de la machine fait partie des agences nomades
                    $isAuthorizedAgency &&
                    # - Les stocks le permettent
                    # -- pas de visibilité et état réservable et modèle encore réservable
                    ((($tabRow->{'state.id'} eq STATE_AVAILABLE || $tabRow->{'state.id'} eq STATE_REVIEW ||
                        $tabRow->{'state.id'} eq STATE_CHECK) && !$tabRow->{'visibleOnAgency.id'} && $modelIsRentable) ||
                    # -- visibilité
                      $tabRow->{'visibleOnAgency.id'} ||
                    # -- état non réservable
                      !($tabRow->{'state.id'} eq STATE_AVAILABLE || $tabRow->{'state.id'} eq STATE_REVIEW ||
                        $tabRow->{'state.id'} eq STATE_CHECK))
                    )
                {
                    &LOC::Util::setTreeValues($tabRow->{'possibilities'}, {'actions' => ['createTransfer']}, 1);
                }

                # Réalisation du transfert inter-agences
                # - Il y a un transfert en cours et il n'est pas verrouillé
                if ($tabRow->{'agenciesTransfer.tabInfos'} && !$tabRow->{'agenciesTransfer.tabInfos'}->{'isLocked'} &&
                    # - Le chargement et le déchargement existent
                    $tabRow->{'agenciesTransfer.tabInfos'}->{'loading'} &&
                    $tabRow->{'agenciesTransfer.tabInfos'}->{'unloading'} &&
                    # - Le chargement et le déchargement sont réalisables
                    $tabRow->{'agenciesTransfer.tabInfos'}->{'loading'}->{'isDoable'} &&
                    $tabRow->{'agenciesTransfer.tabInfos'}->{'unloading'}->{'isDoable'} &&
                    # - L'agence s'occupant du transport doit faire partie des agences nomades de l'utilisateur
                    # (Agence déléguée si renseignée, sinon agence de destination)
                    ($isUserAdmin ||
                        &LOC::Util::in_array(($tabRow->{'agenciesTransfer.tabInfos'}->{'delegatedAgency.id'} ne '' ?
                                              $tabRow->{'agenciesTransfer.tabInfos'}->{'delegatedAgency.id'} :
                                              $tabRow->{'agenciesTransfer.tabInfos'}->{'to.id'}), \@tabAuthorizedAgencies)))
                {
                    &LOC::Util::setTreeValues($tabRow->{'possibilities'}, {'actions' => ['doTransfer']}, 1);
                }

                # Annulation de transfert inter-agences :
                # - Transfert en cours et transfert non verrouillé
                if ($tabRow->{'agenciesTransfer.tabInfos'} && !$tabRow->{'agenciesTransfer.tabInfos'}->{'isLocked'} &&
                    # - Le chargement et le déchargement existent
                    $tabRow->{'agenciesTransfer.tabInfos'}->{'loading'} &&
                    $tabRow->{'agenciesTransfer.tabInfos'}->{'unloading'} &&
                    # - Le chargement et le déchargement sont réalisés
                    ($tabRow->{'agenciesTransfer.tabInfos'}->{'loading'}->{'isDone'} &&
                            $tabRow->{'agenciesTransfer.tabInfos'}->{'unloading'}->{'isDone'} &&
                            # et la réalisation peut être annulée
                            $tabRow->{'possibilities'}->{'actions'}->{'doTransfer'} ||
                        # - Ou le chargement ou le déchargement n'est pas réalisé
                        # - et la machine n'est pas attribuée sur un précontrat
                        (!$tabRow->{'agenciesTransfer.tabInfos'}->{'loading'}->{'isDone'} ||
                            !$tabRow->{'agenciesTransfer.tabInfos'}->{'unloading'}->{'isDone'})
                            && !$tabRow->{'isAffectedOnPrecontract'}) &&
                    # - L'agence de provenance fait partie des agences nomades
                    ($isUserAdmin ||
                        &LOC::Util::in_array($tabRow->{'agenciesTransfer.tabInfos'}->{'from.id'}, \@tabAuthorizedAgencies)))
                {
                    &LOC::Util::setTreeValues($tabRow->{'possibilities'}, {'actions' => ['cancelTransfer']}, 1);
                }

                if ($flags & GETINFOS_TELEMATICS && defined $tabRow->{'telematicId'})
                {
                    # Déséquipement de la télématique
                    # Suspension/réactivation de la télématique
                    &LOC::Util::setTreeValues($tabRow->{'possibilities'}, {'telematics' => ['disable', 'toggle']}, 1);

                    # Activation des codes contrats
                    if (!&LOC::Util::in_array(LOC::StartCode::Contract::Rent::SCOPE, $tabRow->{'telematicScope'}))
                    {
                        &LOC::Util::setTreeValues($tabRow->{'possibilities'}, {'telematics' => ['activateRentContractScope']}, 1);
                    }

                    # Activation/désactivation du digicode
                    &LOC::Util::setTreeValues($tabRow->{'possibilities'}, {'telematics' => ['digiKeyActivation']}, 1);
                }
            }

            # Droits
            if ($flags & GETINFOS_RIGHTS)
            {
                # Informations sur l'utilisateur
                my $tabUserInfos = &LOC::Session::getUserInfos();
                if (defined $tabOptions->{'userId'} && $tabUserInfos->{'id'} != $tabOptions->{'userId'})
                {
                    $tabUserInfos = &LOC::User::getInfos($tabOptions->{'userId'});
                }
                $tabRow->{'tabRights'} = &_getRights($tabRow, $tabUserInfos);
            }

        }
    }

    return (wantarray ? %$tab : $tab);
}


# Function: getRights
# Récupérer les droits sur une machine (par rapport à l'utilisateur)
#
# Parameters:
# string $countryId  - Pays
# string $agencyId   - Agence
# int    $id         - Id de la machine
# int    $userId     - Id de l'utilisateur
#
# Returns:
# hash|hashref - Liste droits
sub getRights
{
    my ($countryId, $agencyId, $id, $userId) = @_;

    my $tabMachineInfos = {};
    my $tabUserInfos;

    # Informations sur l'utilisateur
    $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Informations sur la machine
    if ($id)
    {
        $tabMachineInfos = &getInfos($countryId, $id);
    }

    return &_getRights($tabMachineInfos, $tabUserInfos);
}


# Function: getTransferedMachinesList
# Retourne la liste des machines transférées dans une autre agence que celle en cours
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres ('agencyId' : liste d'agences, 'stateId' : liste des états)
#
# Returns:
# hash|hashref|undef - Liste des machines (mêmes clés que pour la fonction getInfos sauf model.fullName)
sub getTransferedMachinesList
{
    my ($countryId, $format, $tabFilters) = @_;


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT
    DISTINCT tbl_f_machine.MAAUTO AS `id`,
    CONCAT(IFNULL(tbl_f_machine.MANOPARC_AUX, ""), tbl_f_machine.MANOPARC) AS `parkNumber`,
    tbl_f_machine.MOMAAUTO AS `model.id`,
    tbl_p_model.MOMADESIGNATION AS `model.label`
FROM AUTH.MACHINE tbl_f_machine
INNER JOIN CONTRAT tbl_f_contract
ON tbl_f_contract.MAAUTO = tbl_f_machine.MAAUTO
INNER JOIN AUTH.MODELEMACHINE tbl_p_model
ON tbl_f_machine.MOMAAUTO = tbl_p_model.MOMAAUTO
WHERE tbl_f_machine.ETCODE IN (' . $db->quote($tabFilters->{'stateId'}) . ')
AND tbl_f_machine.AGAUTO NOT IN (' . $db->quote($tabFilters->{'agencyId'}) . ')
AND tbl_f_contract.AGAUTO IN (' . $db->quote($tabFilters->{'agencyId'}) . ')
AND tbl_f_contract.CONTRATOK = -1
AND tbl_f_contract.CONTRATDR >= DATE_SUB(CURRENT_DATE(), INTERVAL 6 MONTH)
ORDER BY tbl_f_machine.MANOPARC;';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        # Formatage des données
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}          *= 1;
            $tabRow->{'model.id'}    *= 1;
        }
    }

    return (wantarray ? %$tab : $tab);
}


# Function: getUrl
# Retourne l'URL de la fiche machine
#
# Parameters:
# int    $id       - Identifiant de la machine
# string $viewType - Type d'affichage (facultatif)
#
# Returns:
# string - URL de la fiche machine
sub getUrl
{
    my ($id, $viewType) = @_;

    my $url = &LOC::Request::createRequestUri('machine:machine:view', {'machineId' => $id, 'view' => $viewType});

    return $url;
}


# Function: getUrlByParkNumber
# Retourne l'URL de la fiche machine par numéro dep arc
#
# Parameters:
# string $parkNumber - Numéro de parc de la machine
# string $viewType   - Type d'affichage (facultatif)
#
# Returns:
# string - URL de la fiche machine
sub getUrlByParkNumber
{
    my ($parkNumber, $viewType) = @_;

    my $url = &LOC::Request::createRequestUri('machine:machine:view', {'parkNumber' => $parkNumber, 'view' => $viewType});

    return $url;
}


# Function: getDisplayInfos
# Retourne les informations nécessaire à l'affichage du numéro de parc formatté
#
# Parameters:
# string $parkNumber - Numéro de parc de la machine
# string $viewType   - Type d'affichage (facultatif)
#
# Returns:
# hashref - 'url' : URL de la fiche machine
sub getDisplayInfos
{
    my ($parkNumber, $viewType) = @_;

    if (!defined $viewType)
    {
        $viewType = 'full';
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('auth');

    my $query = '
SELECT
    tbl_f_machine.MAAUTO AS `id`,
    tbl_f_machineagency.agc_cty_id AS `country.id`,
    IFNULL((
        SELECT min_value
        FROM AUTH.f_machine_info
        INNER JOIN AUTH.p_info
        ON (min_inf_id = inf_id AND inf_externalcode = ' . $db->quote(ADDINFO_TELEMATICS) . ')
        WHERE min_mac_id = tbl_f_machine.MAAUTO
    ), 0) AS `isWithTelematic`
FROM AUTH.MACHINE tbl_f_machine
LEFT JOIN frmwrk.p_agency tbl_f_machineagency
ON tbl_f_machine.AGAUTO = tbl_f_machineagency.agc_id
WHERE (tbl_f_machine.MANOPARC = ' . $db->quote($parkNumber) . ' OR
     CONCAT(tbl_f_machine.MANOPARC_AUX, tbl_f_machine.MANOPARC) = ' . $db->quote($parkNumber) . ')';

    my $tabInfos = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);

    my $tabDisplayInfos = {
        'url'             => &getUrl($tabInfos->{'id'}, $viewType),
        'isWithTelematic' => (&_isTelematActived($tabInfos->{'country.id'}) ? $tabInfos->{'isWithTelematic'} * 1 : 0)
    };

    if ($viewType ne 'full')
    {
        $tabDisplayInfos->{'fullUrl'} = &getUrl($tabInfos->{'id'}, 'full');
    }

    return $tabDisplayInfos;
}



# Function: getHistory
# Récupérer l'historique de la machine
#
# Contenu d'une ligne du hashage :
# - id : identifiant de la ligne d'historique ;
# - eventType.code : code du type d'événement ;
# - eventType.label : libellé du type d'événement ;
# - user.id : identifiant de l'utilisateur ;
# - user.name : nom de l'utilisateur ;
# - user.firstName : prénom de l'utilisateur ;
# - user.fullName : nom complet de l'utilisateur ;
# - user.state.id : identifiant de l'état de l'utilisateur ;
# - datetime : date et heure de la trace ;
# - content : contenu de la trace.
#
# Parameters:
# string  $countryId  - Pays
# int     $machineId  - Identifiant de la machine
# string  $localeId   - Identifiant de la locale pour les traductions
#
# Returns:
# array|arrayref|undef - Historique de la machine
sub getHistory
{
    my ($countryId, $machineId, $localeId) = @_;

    return &LOC::Log::getFunctionalityHistory($countryId, 'machine', $machineId, $localeId);
}

# Function: getLastAgencyCheckIn
# Récupérer la date de dernier passage à l'agence de la machine
#
# Parameters:
# string  $countryId  - Pays
# int     $machineId  - Identifiant de la machine
#
# Returns
# string  - Date
sub getLastAgencyCheckIn
{
    my ($countryId, $machineId, $format) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT
    DATE(DATE_SUB(LEAST(tsp_date, tsp_date_unloading), INTERVAL 1 DAY))
FROM f_transport
WHERE tsp_mac_id = ' . $machineId . '
AND tsp_type = ' . $db->quote(LOC::Transport::TYPE_DELIVERY) . '
AND tsp_sta_id = ' . $db->quote(LOC::Transport::STATE_ACTIVE) . '
AND tsp_is_unloadingdone = 1
ORDER BY tsp_date_unloading DESC
LIMIT 1';

    return $db->fetchOne($query);
}


# Function: insert
# Crée une machine
#
# Parameters:
# string  $countryId  - Pays
# hashref $tabData    - Données à insérer
# int     $userId     - Utilisateur responsable de la modification
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# int
sub insert
{
    my ($countryId, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Propriétés
    my %tabProperties = (
        STATE_CHECK  => 'nbCheck',
    );

    # Formatage des données
    my $parkNumber      = (exists $tabData->{'parkNumber'} ? $tabData->{'parkNumber'} : 0);
    my $modelId         = (exists $tabData->{'model.id'} ? $tabData->{'model.id'} : 0);
    my $serialNumber    = (exists $tabData->{'serialNumber'} ? &LOC::Util::trim($tabData->{'serialNumber'}) : '');
    my $nextControlDate = (exists $tabData->{'control.nextDate'} ? $tabData->{'control.nextDate'} : undef);
    my $agencyId        = (exists $tabData->{'agency.id'} ? $tabData->{'agency.id'} : undef);

    # Erreurs
    # - Champs obligatoires non renseignés
    if ($parkNumber == 0 || $modelId == 0 || $serialNumber eq '' || !defined $nextControlDate || !defined $agencyId)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0030);
        return 0;
    }

    # - Doublon
    # -- numéro de parc existant
    if (&getList($countryId, LOC::Util::GETLIST_COUNT, {'parkNumber' => $parkNumber}) * 1 > 0)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0032);
        return 0;
    }
    # -- N° de série/modèle existant
    if (&getList($countryId, LOC::Util::GETLIST_COUNT, {'modelId' => $modelId, 'serialNumber' => $serialNumber}) * 1 > 0)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0033);
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaNameLoc = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Récupération des informations du modèle
    my $tabModelInfos = &LOC::Model::getInfos($countryId, $modelId);
    if (!$tabModelInfos)
    {
        if (!$hasTransaction)
        {
            push(@{$tabErrors->{'fatal'}}, 0x0031);
            # Annule la transaction
            $db->rollBack();
        }
        return 0;
    }

    # - Récupération du préfixe pour le numéro de parc
    tie(my %tabTypesPrefix, 'Tie::IxHash');
    %tabTypesPrefix = (
        LOC::Model::TYPE_CATEGORYB => PREFIX_CATEGORYB,
        LOC::Model::TYPE_SUBRENT   => PREFIX_SUBRENT,
        LOC::Model::TYPE_ACCESSORY => PREFIX_ACCESSORY,
        LOC::Model::TYPE_SALE      => PREFIX_SALE
    );
    my @tabPrefix = ();
    foreach my $type (keys(%tabTypesPrefix))
    {
        if (&LOC::Util::in_array($type, $tabModelInfos->{'tabTypes'}))
        {
            push(@tabPrefix, $tabTypesPrefix{$type});
        }
    }

    my $tabUpdates = {
        'MOMAAUTO'     => $modelId,
        'MANOPARC'     => $parkNumber,
        'MANOSERIE'    => $serialNumber,
        'MADATEACHAT*' => 'CURDATE()',
        'AGAUTO'       => $agencyId,
        'ETCODE'       => STATE_CHECK,
        'LIETCODE'     => substr(STATE_CHECK, -2) * 1,
        'MANOPARC_AUX' => join('-', @tabPrefix),
        'MABLOQUECH'   => 0,
        'MACREATIONOK' => 0
    };


    # Insertion de la machine
    my $machineId = $db->insert('MACHINE', $tabUpdates, 'AUTH');

    if ($machineId == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        print STDERR sprintf($fatalErrMsg, 'insert', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Mise à jour de la date apave
    if ($tabModelInfos->{'isControlVgp'})
    {
        my $tabUpdatesControl = {
            'MAAUTO'     => $machineId,
            'COMAAPAVE'  => $nextControlDate,
            'COMAOK'     => 0,
            'COMAINDICE' => 0
        };
        if ($db->insert('CONTROLEMACHINE', $tabUpdatesControl, $schemaNameLoc) == -1)
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }

        # Alertes Machine(s) à contrôler dans X
        my $currentDate = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE);
        # - Calcul de l'intervalle pour connaître l'alerte à modifier
        my $diffDays   = &LOC::Date::getDeltaDays($currentDate, $nextControlDate);

        my $alertId = undef;
        if ($diffDays <= CONTROL_1WEEK)
        {
            $alertId = LOC::Alert::MACHINESTOCONTROL1WEEK;
        }
        elsif ($diffDays > CONTROL_1WEEK && $diffDays <= CONTROL_2WEEKS)
        {
            $alertId = LOC::Alert::MACHINESTOCONTROL2WEEKS;
        }
        elsif ($diffDays > CONTROL_2WEEKS && $diffDays <= CONTROL_2MONTHS)
        {
            $alertId = LOC::Alert::MACHINESTOCONTROL2MONTHS;
        }
        if ($alertId)
        {
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, $alertId, +1);
        }

    }

    # Mise à jour des stocks
    # - Modèle sur l'agence
    &LOC::EndUpdates::updateModelStock($tabEndUpds, $agencyId, $modelId, $tabProperties{STATE_CHECK}, +1);
    # - Nombre total sur l'agence
    &LOC::EndUpdates::updateModelStock($tabEndUpds, $agencyId, $modelId, 'nbTotal', +1);
    # - Nombre de réservables sur l'agence
    &LOC::EndUpdates::updateModelStock($tabEndUpds, $agencyId, $modelId, 'nbBookable', +1);

    # Mise à jour alerte création Kimoce
    &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::MACHINESCREATIONTOFINALIZE, +1);

    # Ajout de la trace de création
    my $tabContent = {
        'schema'  => $schemaNameLoc,
        'code'    => LOC::Log::EventType::CREATION,
        'old'     => undef,
        'new'     => undef,
        'content' => ''
    };
    &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $machineId, $tabContent);

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }


    # Envoi de mail
    &sendEmail($countryId, undef, 'newMachine', {'machineId' => $machineId}, $userId);

    return $machineId;
}


# Function: sendEmail
# Envoi mail concernant une action transport
#
# Parameters:
# string       $countryId  - Pays
# string       $mainType   - Module du type de mail (par exemple : 'visibility')
# string       $type       - Type de mail ('visibleOnAgency')
# hashref      $tabData    - Données nécessaires au formatage du mail
# int          $userId     - Utilisateur responsable
#
# Returns:
# bool
sub sendEmail
{
    my ($countryId, $mainType, $type, $tabData, $userId) = @_;

    # Informations sur l'utilisateur (avec optimisation)
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Informations de l'agence de la personne connnectée
    my $tabAgencyInfos = &LOC::Agency::getInfos($tabUserInfos->{'nomadAgency.id'});

    # Création d'un mail
    my $mailObj = LOC::Mail->new();

    # Encodage
    $mailObj->setEncodingType(LOC::Mail::ENCODING_TYPE_UTF8);

    # Expéditeur
    my %tabSender = ($tabUserInfos->{'email'} => $tabUserInfos->{'firstName'} . ' ' . $tabUserInfos->{'name'});
    $mailObj->setSender(%tabSender);

    # Corps
    my $dir = (defined($mainType) ? $mainType . '/' : '');
    my $file = &LOC::Globals::get('cgiPath') . '/cfg/templates/' . $countryId . '/' . $dir . $type . '.html';
    open(TPL, $file);
    my $template = join('', <TPL>);
    close(TPL);

    # Recherche des données pour le template
    my $tabOldInfos = $tabData->{'tabOldInfos'};
    my $tabNewInfos = $tabData->{'tabNewInfos'};
    my $tabModif    = $tabData->{'tabModif'};
    my $mailData = {};

    if ($mainType eq 'visibility')
    {
        $mailData = &_getDataForMailVisibleOnAgency($countryId, $tabOldInfos, $tabNewInfos, $tabModif);
    }
    elsif ($type eq 'newMachine')
    {
        $mailData = &_getDataForMailNewMachine($countryId, $tabData, $userId);
    }

    my $tabReplaces = $mailData->{'tabReplaces'};
    $tabReplaces->{'user.firstName'}    = $tabUserInfos->{'firstName'};
    $tabReplaces->{'user.name'}         = $tabUserInfos->{'name'};
    $tabReplaces->{'agency.label'}      = $tabAgencyInfos->{'label'};
    $tabReplaces->{'agency.telephone'}  = $tabAgencyInfos->{'telephone'};
    $tabReplaces->{'agency.fax'}        = $tabAgencyInfos->{'fax'};
    $tabReplaces->{'agency.address'}    = $tabAgencyInfos->{'address'};
    $tabReplaces->{'agency.postalCode'} = $tabAgencyInfos->{'postalCode'};
    $tabReplaces->{'agency.city'}       = $tabAgencyInfos->{'city'};

    # Formatage et remplacement des données dans le template
    $template =~ s/(\<%([a-z]{3})\:(.*?)\>)/@{[&_formatTemplate($2, $3, $tabReplaces)]}/g;

    while ($template =~ m/\<\%(.*?)\>/)
    {
        my $replace = '';
        if (exists $tabReplaces->{$1})
        {
            $replace = $tabReplaces->{$1};
        }
        $template =~ s/<%$1>/$replace/g;
    }

    # Formatage et remplacement des données dans le sujet
    my $subject = ($template =~ m/\<title\>(.*?)\<\/title\>/ ? $1 : '');

    if ($template =~ /"cid:(.+)"/ && -e &LOC::Globals::get('imgPath') . '/print/gui/' . $1)
    {
        $mailObj->addInlineAttachment(&LOC::Globals::get('imgUrl') . '/print/gui/' . $1, $1);
    }

    $mailObj->setBodyType(LOC::Mail::BODY_TYPE_HTML);

    my %tabRecipients = %{$mailData->{'tabRecipients'}};
    my %tabRecipient;
    my $templatePlus = '';

    if (&LOC::Globals::getEnv() ne 'exp')
    {
        $subject = '[TEST] ' . $subject;

        # Informations sur l'utilisateur
        my $tabCurrentUserInfos = &LOC::Session::getUserInfos();

        if (defined $tabCurrentUserInfos)
        {
            %tabRecipient = ($tabCurrentUserInfos->{'email'} => $tabCurrentUserInfos->{'firstName'} . ' ' . $tabCurrentUserInfos->{'name'});
        }
        else
        {
            # En cas !
            %tabRecipient = ('archive.dev@acces-industrie.com' => 'Développeurs');
        }

        # Destinataires d'origines
        my $recipients = '';
        foreach my $email (keys %tabRecipients)
        {
            $recipients .= $tabRecipients{$email} . ' &lt;' . $email . '&gt;<br />';
        }


        $templatePlus  = 'Destinataire d\'origine : <br/>' . $tabUserInfos->{'firstName'} . ' ' . $tabUserInfos->{'name'} . ' &lt;' . $tabUserInfos->{'email'} . '&gt;<br />';
        $templatePlus .= $recipients;
    }
    else
    {
        %tabRecipient = %tabRecipients;
        $tabRecipient{$tabUserInfos->{'email'}} = $tabUserInfos->{'firstName'} . ' ' . $tabUserInfos->{'name'};
    }

    $mailObj->setBody($templatePlus . $template);

    $mailObj->addRecipients(%tabRecipient);

    $mailObj->setSubject($subject);

    $mailObj->send();

    return 1;
}



# Function: update
# Mise à jour des informations d'une machine
#
# Parameters:
# string  $countryId  - Pays
# int     $id         - Identifiant de la machine
# hashref $tabData    - Données à mettre à jour
# int     $userId     - Utilisateur responsable de la modification
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub update
{
    my ($countryId, $id, $tabData, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Propriétés
    my %tabProperties = (
        STATE_RENTED()    => 'nbRented',
        STATE_AVAILABLE() => 'nbAvailable',
        STATE_RECOVERY()  => 'nbRecovery',
        STATE_CHECK()     => 'nbCheck',
        STATE_DELIVERY()  => 'nbDelivery',
        STATE_REVIEW()    => 'nbReview',
        STATE_CONTROL()   => 'nbControl',
        STATE_BROKEN()    => 'nbBroken',
        STATE_TRANSFER()  => 'nbTransfer'
    );

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'update', $_[0], __FILE__, '');

        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }

        return 0;
    };

    # Récupération des informations de la machine
    my $tabInfos = &getInfos($countryId, $id, GETINFOS_ALL, {'level' => LEVEL_MODEL});
    if (!$tabInfos)
    {
        return &$rollBackAndError(__LINE__);
    }

    # Vérification de la date de modification (accès concurrentiel)
    if (!&_checkUpToDateData($tabInfos->{'modificationDate'}, $tabData->{'modificationDate'}))
    {
        return &$rollBackAndError(__LINE__, 0x0100);
    }

    # Schema
    my $schemaName = 'AUTH';
    my $schemaNameLoc = &LOC::Db::getSchemaName($tabInfos->{'country.id'});


    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }
    # Récupération des droits
    my $tabRights = &_getRights($tabInfos, $tabUserInfos);

    # Paramètres

    my $oldStateId            = $tabInfos->{'state.id'};
    my $newStateId            = (exists $tabData->{'state.id'} && $tabData->{'state.id'} ne '' ? $tabData->{'state.id'} : $oldStateId);
    my $oldIsOutOfPark        = $tabInfos->{'isOutOfPark'};
    my $newIsOutOfPark        = (exists $tabData->{'isOutOfPark'} ? $tabData->{'isOutOfPark'} : $oldIsOutOfPark);
    my $oldAgencyId           = $tabInfos->{'agency.id'};
    my $newAgencyId           = (exists $tabData->{'agency.id'} && $tabData->{'agency.id'} ne '' ? $tabData->{'agency.id'} : $oldAgencyId);
    my $oldContractId         = $tabInfos->{'contract.id'};
    my $newContractId         = (exists $tabData->{'contract.id'} ? $tabData->{'contract.id'} : $oldContractId);
    my $oldControlDate        = $tabInfos->{'control.lastDate'};
    my $newControlDate        = (exists $tabData->{'lastControlDate'} ? $tabData->{'lastControlDate'} : $oldControlDate);
    my $oldControlNextDate    = $tabInfos->{'control.nextDate'};
    my $newControlNextDate    = $oldControlNextDate;
    # Machine bloquée sur chantier
    my $oldIsSiteBlocked      = $tabInfos->{'isSiteBlocked'};
    my $newIsSiteBlocked      = (exists $tabData->{'isSiteBlocked'} ? $tabData->{'isSiteBlocked'} : $oldIsSiteBlocked);
    # Commentaire du blocage sur chantier
    my $oldSiteBlockedComment = $tabInfos->{'siteBlockedComment'};
    my $newSiteBlockedComment = (exists $tabData->{'siteBlockedComment'} ? &LOC::Util::trim($tabData->{'siteBlockedComment'}) : $oldSiteBlockedComment);
    # Mode de visibilité sur une autre agence
    my $oldVisibilityMode     = $tabInfos->{'visibilityMode'};
    my $newVisibilityMode     = (exists $tabData->{'visibilityMode'} ? $tabData->{'visibilityMode'} : $oldVisibilityMode);
    # Agence de visibilité
    my $oldVisibleOnAgencyId  = $tabInfos->{'visibleOnAgency.id'};
    my $newVisibleOnAgencyId  = (exists $tabData->{'visibleOnAgency.id'} ? $tabData->{'visibleOnAgency.id'} : $oldVisibleOnAgencyId);

    my $oldTransferAgencyId = $tabInfos->{'agenciesTransfer.tabInfos'}->{'to.id'};

    # action sur le transfert inter-chantiers
    my $newIsTransferDone   = (exists $tabData->{'isTransferDone'} ?
                                    $tabData->{'isTransferDone'} :
                                    ($tabInfos->{'agenciesTransfer.tabInfos'}->{'unloading'}->{'isDone'} ? 1 : 0));

    # Vérifications
    # - Commentaire blocage chantier obligatoire
    if ($newIsSiteBlocked && $newSiteBlockedComment eq '')
    {
        return &$rollBackAndError(__LINE__, 0x0004);
    }

    # Tableau des mise à jour à effectuer
    my $tabUpdates = {
        'MADATEMODIF*' => 'NOW()'    # Force la modification de la date de modif
    };
    my $tabChanges = {};

    # Etat machine
    if ($oldStateId ne $newStateId)
    {
        # Vérification des droits si on vient de la fiche machine
        if ($tabOptions->{'updateFrom'} eq 'machineForm' &&
            (!&_hasRight($tabRights->{'actions'}, 'state') ||
                !$tabInfos->{'possibilities'}->{'actions'}->{'state'}))
        {
            return &$rollBackAndError(__LINE__, 0x0003);
        }

        # On ne peut pas changer l'état si un transfert inter-agences est en cours
        # sauf si on a un état "en transfert"
        if ($newStateId ne STATE_TRANSFER && $oldTransferAgencyId
                && !$newIsTransferDone && !$tabInfos->{'agenciesTransfer.tabInfos'}->{'unloading'}->{'isDone'})
        {
            return &$rollBackAndError(__LINE__, 0x0002);
        }

        $tabUpdates->{'ETCODE'}   = $newStateId;
        $tabUpdates->{'LIETCODE'} = substr($newStateId, -2) * 1;
        $tabChanges->{'state.id'} = {'old' => $oldStateId, 'new' => $newStateId};

        # Date de panne
        if ($newStateId eq STATE_BROKEN)
        {
            $tabUpdates->{'MADATEPANNE'} = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE);
        }
    }
    # Etat parc
    if ($oldIsOutOfPark != $newIsOutOfPark)
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'actions'}, 'outOfPark') ||
            !$tabInfos->{'possibilities'}->{'actions'}->{'outOfPark'})
        {
            return &$rollBackAndError(__LINE__, 0x0003);
        }
        $tabUpdates->{'MAHORSPARC'} = $newIsOutOfPark * -1;
        $tabChanges->{'isOutOfPark'} = {'old' => $oldIsOutOfPark, 'new' => $newIsOutOfPark};
    }
    # Machine bloquée
    if ($oldIsSiteBlocked != $newIsSiteBlocked)
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'actions'}, 'siteBlocked') ||
            !$tabInfos->{'possibilities'}->{'actions'}->{'siteBlocked'})
        {
            return &$rollBackAndError(__LINE__, 0x0003);
        }
        $tabUpdates->{'MABLOQUECH'} = $newIsSiteBlocked * -1;
        $tabChanges->{'isSiteBlocked'}  = {'old' => $oldIsSiteBlocked, 'new' => $newIsSiteBlocked};
    }
    # Commentaire du blocage de la machine
    if ($oldSiteBlockedComment ne $newSiteBlockedComment)
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'actions'}, 'siteBlocked') ||
            !$tabInfos->{'possibilities'}->{'actions'}->{'siteBlocked'})
        {
            return &$rollBackAndError(__LINE__, 0x0003);
        }
        $tabUpdates->{'MABLOQUECHCOMM'} = ($newSiteBlockedComment eq '' ? undef : $newSiteBlockedComment);
    }
    # Visibilité
    if ($oldVisibleOnAgencyId ne $newVisibleOnAgencyId)
    {
        # Vérification:
        if ($tabOptions->{'updateFrom'} ne 'internal' &&
              (!&_hasRight($tabRights->{'actions'}, 'visibility') ||
              !$tabInfos->{'possibilities'}->{'actions'}->{'visibility'}))
        {
            return &$rollBackAndError(__LINE__, 0x0003);
        }

        # Recherche d'un transfert inter-agences en cours sur la machine
        # - Liste des pays
        my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);
        my $tabAgenciesTransfer = undef;
        foreach my $country (@$tabCountries)
        {
            my $tabTransfers = &LOC::Transport::getAgenciesTransferInfos($country, $id);

            if ($tabTransfers)
            {
                $tabAgenciesTransfer = $tabTransfers;
                last;
            }
        }
        # Vérification que la visibilité est dans la même agence que le transfert (s'il existe)
        if ($tabAgenciesTransfer && $newVisibleOnAgencyId && $tabAgenciesTransfer->{'agency.id'} ne $newVisibleOnAgencyId)
        {
            return &$rollBackAndError(__LINE__, 0x0003);
        }

        $tabUpdates->{'MASUPPAGAUTO'} = $newVisibleOnAgencyId;
        $tabChanges->{'visibleOnAgency.id'}  = {'old' => $oldVisibleOnAgencyId, 'new' => $newVisibleOnAgencyId};
        # - Modification du mode de visibilité
        $tabUpdates->{'MASUPPAGMODE'}   = ($newVisibleOnAgencyId ? $newVisibilityMode : VISIBILITYMODE_NONE);
        $tabChanges->{'visibilityMode'} = {'old' => $oldVisibilityMode, 'new' => $newVisibilityMode};
    }
    # Agence
    if ($oldAgencyId ne $newAgencyId)
    {
        $tabUpdates->{'AGAUTO'}       = $newAgencyId;
        $tabChanges->{'agency.id'}          = {'old' => $oldAgencyId, 'new' => $newAgencyId};

        # si la nouvelle agence est l'agence de visibilité
        # on peut supprimer la visibilité sur la machine
        if ($newAgencyId eq $newVisibleOnAgencyId)
        {
            $tabUpdates->{'MASUPPAGAUTO'} = undef;
            $tabUpdates->{'MASUPPAGMODE'} = VISIBILITYMODE_NONE;

            $tabChanges->{'visibleOnAgency.id'} = {'old' => $oldVisibleOnAgencyId, 'new' => undef};
            $tabChanges->{'visibilityMode'}     = {'old' => $oldVisibilityMode, 'new' => VISIBILITYMODE_NONE};
            # pour la trace de mise à jour de la visibilité agence
            $newVisibleOnAgencyId = undef;
        }
    }
    # Contrat
    if ($oldContractId != $newContractId)
    {
        $tabUpdates->{'CONTRATAUTO'} = $newContractId;
        $tabChanges->{'contract.id'} = {'old' => $oldContractId, 'new' => $newContractId};
    }

    if (keys %$tabUpdates > 0)
    {
        if ($db->update('MACHINE', $tabUpdates, {'MAAUTO*' => $id}, $schemaName) == -1)
        {
            return &$rollBackAndError(__LINE__, 0x0001);
        }
    }

    # Date de dernière VGP
    if ($oldControlDate ne $newControlDate)
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'actions'}, 'control') ||
            !$tabInfos->{'possibilities'}->{'actions'}->{'control'})
        {
            return &$rollBackAndError(__LINE__, 0x0003);
        }

        # Date de dernière inférieure antérieure ou égale à la date du jour
        if ($newControlDate gt &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE))
        {
            return &$rollBackAndError(__LINE__, 0x0007);
        }

        # calcul de la prochaine date de VGP
        if ($tabInfos->{'isControlVgp'})
        {
            $tabChanges->{'control'} = {'old' => $oldControlDate, 'new' => $newControlDate};
            my $periodicity = $tabInfos->{'periodicityVgp'};
            my %tabControlDate = &LOC::Date::getHashDate($newControlDate);
            my ($nextYear, $nextMonth, $nextDay) = &Date::Calc::Add_Delta_YM($tabControlDate{'y'},
                                                                    $tabControlDate{'m'},
                                                                    $tabControlDate{'d'},
                                                                    0,
                                                                    $periodicity);
            $newControlNextDate = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE,
                                                                 ('y' => $nextYear, 'm' => $nextMonth, 'd' => $nextDay));
            my $tabUpdatesControl = {
                'COMAAPAVE'   => $newControlNextDate,
                'COMADAPAVE'  => $newControlDate,
                'COMAOK'      => -1,
                'COMAINDICE*' => 'COMAINDICE + 1'
            };
            if ($db->update('CONTROLEMACHINE', $tabUpdatesControl, {'MAAUTO*' => $id}, $schemaNameLoc) == -1)
            {
                return &$rollBackAndError(__LINE__, 0x0001);
            }
            # Mise à jour des caractéristiques de la machine dans Kimoce
            &LOC::EndUpdates::updateKimoceMachine($tabEndUpds, $tabInfos->{'parkNumber'}, LOC::EndUpdates::KIMUPD_FEATURES);

            # Mise à jour des traces
            &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $id,
                               {'schema'  => $schemaNameLoc,
                                'code'    => LOC::Log::EventType::UPDMACHINECONTROLDATE,
                                'old'     => $oldControlDate,
                                'new'     => $newControlDate,
                                'content' => $oldControlDate . ' → ' . $newControlDate
                               });
        }
    }

    # Si il y a des données invalides alors on arrête ici
    if (@{$tabErrors->{'fatal'}} > 0)
    {
        return &$rollBackAndError(__LINE__);
    }


    if (keys %$tabChanges > 0)
    {
        # Changements pouvant impacter le transport ? => on le lui communique
        &LOC::Transport::onExternalChanges($countryId, {
            'machine' => {
                'id'    => $id,
                'event' => 'update',
                'props' => $tabChanges
            }
        }, $userId, undef, $tabEndUpds);

        # Changements pouvant verrouiller les transferts inter-agences terminés
        if ($tabChanges->{'state.id'} || $tabChanges->{'agency.id'} ||
            $tabChanges->{'contract.id'} || $tabChanges->{'visibleOnAgency.id'})
        {
            # Filtres de recherche pour les transports à verrouiller
            # - transfert inter-agences
            # - non verrouillé
            # - portant sur la machine
            # - réalisation effectuée
            my $tabFilters = {
                'type'            => LOC::Transport::TYPE_AGENCIESTRANSFER,
                'isLocked'        => 0,
                'machineId'       => $id,
                'isUnloadingDone' => 1
            };
            # Liste des pays
            my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);
            # Verrouillage pour chaque pays
            foreach my $country (@$tabCountries)
            {
                &LOC::Transport::lock($country, $tabFilters, $userId, $tabEndUpds);
            }
        }
    }

    # Mises à jour stock + KIMOCE
    if (defined $tabProperties{$oldStateId})
    {
        &LOC::EndUpdates::updateModelStock($tabEndUpds, $oldAgencyId, $tabInfos->{'model.id'}, $tabProperties{$oldStateId}, -1);
        # Mise à jour du stock total si la machine passe dans un état non stocké
        if (!defined $tabProperties{$newStateId})
        {
            &LOC::EndUpdates::updateModelStock($tabEndUpds, $oldAgencyId, $tabInfos->{'model.id'}, 'nbTotal', -1);
        }
    }
    if (defined $tabProperties{$newStateId})
    {
        &LOC::EndUpdates::updateModelStock($tabEndUpds, $newAgencyId, $tabInfos->{'model.id'}, $tabProperties{$newStateId}, +1);
        # Mise à jour du stock total si la machine était dans un état non stocké
        if (!defined $tabProperties{$oldStateId})
        {
            &LOC::EndUpdates::updateModelStock($tabEndUpds, $oldAgencyId, $tabInfos->{'model.id'}, 'nbTotal', +1);
        }
    }

    # Mise à jour du stock réservable
    # - état avant
    # -- états dispo, récup, vérif, révision
    # -- état transfert avec visibilité et le nouvel état n'est pas livraison (car dans ce cas, la machine est attribuée, et le réservable a déjà été mis à jour)
    if ($oldStateId eq STATE_AVAILABLE || $oldStateId eq STATE_RECOVERY ||
        $oldStateId eq STATE_CHECK || $oldStateId eq STATE_REVIEW ||
        ($oldStateId eq STATE_TRANSFER && $oldVisibleOnAgencyId && $newStateId ne STATE_DELIVERY))
    {
        my $beforeAgencyId = ($oldVisibleOnAgencyId ? $oldVisibleOnAgencyId : $oldAgencyId);
        &LOC::EndUpdates::updateModelStock($tabEndUpds, $beforeAgencyId, $tabInfos->{'model.id'}, 'nbBookable', -1);
    }
    # - état après
    if ($newStateId eq STATE_AVAILABLE || $newStateId eq STATE_RECOVERY ||
        $newStateId eq STATE_CHECK || $newStateId eq STATE_REVIEW ||
        ($newStateId eq STATE_TRANSFER && $newVisibleOnAgencyId))
    {
        my $afterAgencyId = ($newVisibleOnAgencyId ? $newVisibleOnAgencyId : $newAgencyId);
        &LOC::EndUpdates::updateModelStock($tabEndUpds, $afterAgencyId, $tabInfos->{'model.id'}, 'nbBookable', +1);
    }
    # Changement d'état
    if ($oldStateId ne $newStateId)
    {
        # Mise à jour des traces
        &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $id,
                               {'schema'  => $schemaNameLoc,
                                'code'    => LOC::Log::EventType::UPDMACHINESTATE,
                                'old'     => $oldStateId, 'new' => $newStateId,
                                'content' => &_formatTraces($db, $tabInfos->{'country.id'}, LOC::Log::EventType::UPDMACHINESTATE, $oldStateId)
                                             . ' → '
                                             . &_formatTraces($db, $tabInfos->{'country.id'}, LOC::Log::EventType::UPDMACHINESTATE, $newStateId)});

        # Mise à jour de la situation (de l'état) dans Kimoce
        &LOC::EndUpdates::updateKimoceMachine($tabEndUpds, $tabInfos->{'parkNumber'},
                                                LOC::EndUpdates::KIMUPD_SITUATION,
                                                {'state.id' => $newStateId});
    }
    # Changement d'agence
    if ($oldAgencyId ne $newAgencyId)
    {
        &LOC::EndUpdates::updateModelStock($tabEndUpds, $oldAgencyId, $tabInfos->{'model.id'}, 'nbTotal', -1);
        &LOC::EndUpdates::updateModelStock($tabEndUpds, $newAgencyId, $tabInfos->{'model.id'}, 'nbTotal', +1);

        # Mise à jour des traces
        my $tabOldAgencyInfo = &LOC::Agency::getInfos($oldAgencyId);
        my $oldAgencyLabel = $tabOldAgencyInfo->{'label'};

        my $tabNewAgencyInfo = &LOC::Agency::getInfos($newAgencyId);
        my $newAgencyLabel = $tabNewAgencyInfo->{'label'};

        &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $id,
                               {'schema'  => $schemaNameLoc,
                                'code'    => LOC::Log::EventType::CHANGEMACHINEAGENCY,
                                'old'     => $oldAgencyId, 'new' => $newAgencyId,
                                'content' => $oldAgencyLabel . ' → ' . $newAgencyLabel});

        # Mise à jour de la localisation dans Kimoce
        &LOC::EndUpdates::updateKimoceMachine($tabEndUpds, $tabInfos->{'parkNumber'},
                                                LOC::EndUpdates::KIMUPD_LOCALIZATION,
                                                {'agency.id' => $newAgencyId});
    }

    # Changement d'agence ou de date de prochaine VGP
    if ($oldAgencyId ne $newAgencyId || $oldControlNextDate ne $newControlNextDate)
    {
        my $currentDate = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE);
        # Mise à jour alertes VGP
        # - Calcul de l'intervalle pour connaître l'alerte à modifier
        my $oldDiffDays   = &LOC::Date::getDeltaDays($currentDate, $oldControlNextDate);
        my $oldAlertId = undef;
        if ($oldDiffDays <= CONTROL_1WEEK)
        {
            $oldAlertId = LOC::Alert::MACHINESTOCONTROL1WEEK;
        }
        elsif ($oldDiffDays > CONTROL_1WEEK && $oldDiffDays <= CONTROL_2WEEKS)
        {
            $oldAlertId = LOC::Alert::MACHINESTOCONTROL2WEEKS;
        }
        elsif ($oldDiffDays > CONTROL_2WEEKS && $oldDiffDays <= CONTROL_2MONTHS)
        {
            $oldAlertId = LOC::Alert::MACHINESTOCONTROL2MONTHS;
        }

        # - Calcul de l'intervalle pour connaître l'alerte à modifier
        my $newDiffDays   = &LOC::Date::getDeltaDays($currentDate, $newControlNextDate);

        my $newAlertId = undef;
        if ($newDiffDays <= CONTROL_1WEEK)
        {
            $newAlertId = LOC::Alert::MACHINESTOCONTROL1WEEK;
        }
        elsif ($newDiffDays > CONTROL_1WEEK && $newDiffDays <= CONTROL_2WEEKS)
        {
            $newAlertId = LOC::Alert::MACHINESTOCONTROL2WEEKS;
        }
        elsif ($newDiffDays > CONTROL_2WEEKS && $newDiffDays <= CONTROL_2MONTHS)
        {
            $newAlertId = LOC::Alert::MACHINESTOCONTROL2MONTHS;
        }

        if ($oldAlertId)
        {
            &LOC::EndUpdates::updateAlert($tabEndUpds, $oldAgencyId, $oldAlertId, -1);
        }
        if ($newAlertId)
        {
            &LOC::EndUpdates::updateAlert($tabEndUpds, $newAgencyId, $newAlertId, +1);
        }

    }
    # Changement d'état parc
    if ($oldIsOutOfPark != $newIsOutOfPark)
    {
        # Mise à jour des traces
        &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $id,
                               {'schema'  => $schemaNameLoc,
                                'code'    => LOC::Log::EventType::UPDMACHINEOUTOFPARK,
                                'old'     => $oldIsOutOfPark, 'new' => $newIsOutOfPark,
                                'content' => &_formatTraces($db, $tabInfos->{'country.id'}, LOC::Log::EventType::UPDMACHINEOUTOFPARK, $oldIsOutOfPark)
                                             . ' → '
                                             . &_formatTraces($db, $tabInfos->{'country.id'}, LOC::Log::EventType::UPDMACHINEOUTOFPARK, $newIsOutOfPark)});
    }
    # Changement de visibilité
    if ($oldVisibleOnAgencyId ne $newVisibleOnAgencyId)
    {
        # Mise à jour des traces
        &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $id,
                               {'schema'  => $schemaNameLoc,
                                'code'    => LOC::Log::EventType::UPDMACHINEVISIBILITY,
                                'old'     => $oldVisibleOnAgencyId, 'new' => $newVisibleOnAgencyId,
                                'content' => &_formatTraces($db, $tabInfos->{'country.id'}, LOC::Log::EventType::UPDMACHINEVISIBILITY, $oldVisibleOnAgencyId)
                                             . ' → '
                                             . &_formatTraces($db, $tabInfos->{'country.id'}, LOC::Log::EventType::UPDMACHINEVISIBILITY, $newVisibleOnAgencyId)});

        if ($oldVisibleOnAgencyId)
        {
            # Mise à jour de l'alerte des machines en visibilité - Agence de la machine
            &LOC::EndUpdates::updateAlert($tabEndUpds, $oldAgencyId, LOC::Alert::MACHINESVISIBILITY, -1);
            # Mise à jour de l'alerte des machines en visibilité - Agence de visibilité
            &LOC::EndUpdates::updateAlert($tabEndUpds, $oldVisibleOnAgencyId, LOC::Alert::MACHINESVISIBILITY, -1);
        }
        if ($newVisibleOnAgencyId)
        {
            # Mise à jour de l'alerte des machines en visibilité - Agence de la machine
            &LOC::EndUpdates::updateAlert($tabEndUpds, $newAgencyId, LOC::Alert::MACHINESVISIBILITY, +1);
            # Mise à jour de l'alerte des machines en visibilité - Agence de visibilité
            &LOC::EndUpdates::updateAlert($tabEndUpds, $newVisibleOnAgencyId, LOC::Alert::MACHINESVISIBILITY, +1);
        }
    }
    # Changement de contrat
    if ($oldContractId != $newContractId)
    {
        # Mise à jour des caractèristiques de la machine dans Kimoce
        &LOC::EndUpdates::updateKimoceMachine($tabEndUpds, $tabInfos->{'parkNumber'}, LOC::EndUpdates::KIMUPD_FEATURES);
    }
    # Blocage/déblocage sur chantier
    if ($oldIsSiteBlocked != $newIsSiteBlocked)
    {
        my $traceCode = ($oldIsSiteBlocked ? LOC::Log::EventType::UPDMACHINESITEUNBLOCKED : LOC::Log::EventType::UPDMACHINESITEBLOCKED);
        my $tabContent = {
            'schema'  => $schemaNameLoc,
            'code'    => $traceCode,
            'old'     => $oldIsSiteBlocked, 'new' => $newIsSiteBlocked,
            'content' => &_formatTraces($db, $tabInfos->{'country.id'}, $traceCode, $oldIsSiteBlocked)
                         . ' → '
                         . &_formatTraces($db, $tabInfos->{'country.id'}, $traceCode, $newIsSiteBlocked)};
        # Mise à jour des traces
        &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $id, $tabContent);
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            return &$rollBackAndError(__LINE__);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    # Envoi d'un mail si la visibilité a changé mais sans changer d'agence physique
    # (ce qui exclut la réalisation du transport des machines en visibilité)
    if ($oldVisibleOnAgencyId ne $newVisibleOnAgencyId && $oldAgencyId eq $newAgencyId)
    {
        # Type de modification de la visibilité
        my @tabModif = ();
        if (!$oldVisibleOnAgencyId)
        {
            push(@tabModif, 'create');
        }
        elsif (!$newVisibleOnAgencyId)
        {
            push(@tabModif, 'suppr');
        }
        else
        {
           push(@tabModif, 'modif');
        }

        my $tabData = {
            'tabOldInfos' => {
                'visibleOnAgency.id' => $oldVisibleOnAgencyId,
                'agency.id'          => $oldAgencyId
            },
            'tabNewInfos' => {
                'id'                 => $id,
                'visibleOnAgency.id' => $newVisibleOnAgencyId,
                'agency.id'          => $newAgencyId
            },
            'tabModif'    => \@tabModif
        };

        # Détermination du modèle de mails
        my $type = 'physicalAgency' . ($oldAgencyId eq $newAgencyId ? 'Unchanged' : 'Changed');

        # Envoi du mail
        &sendEmail($countryId, 'visibility', $type, $tabData, $userId);
    }

    return 1;
}

# Function: updateInfosAndTransport
# Met à jour la machine depuis la fiche machine
#
# Parameters:
# string   $countryId  - Identifiant du pays
# hashref  $tabAction  - Action à effectuer
# - Contenu du tableau :
# - id   : identifiant de la machine
# - type : type d'action (update ou transfer)
# - data : données pour l'action
# int      $userId     - Identifiant de l'utilisateur responsable
# hashref  $tabOptions - Tableau des options (facultatif)
# hashref  $tabErrors  - Tableau des erreurs
#
# Returns:
# bool -
sub updateInfosAndTransport
{
    my ($countryId, $tabAction, $userId, $tabOptions, $tabErrors) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];
    $tabErrors->{'modules'} = {
        'transfer'       => {
            'fatal' => []
        },
        'machine'        => {
            'fatal' => []
        }
    };

    my $machineId = $tabAction->{'id'};
    my $tabData   = $tabAction->{'data'};

    if ($tabAction->{'type'} eq 'update')
    {
        # Mise à jour de la machine
        my $tabMachineErrors = $tabErrors->{'modules'}->{'machine'};
        if(!&update($countryId, $machineId, $tabData, $userId, $tabOptions, $tabMachineErrors))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0020);
            # Erreur fatale ?
            if (&LOC::Util::in_array(0x0001, $tabMachineErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, 0x0001);
            }
            # Modifications interdites ?
            if (&LOC::Util::in_array(0x0003, $tabMachineErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, 0x0003);
            }
            return 0;
        }
    }
    elsif ($tabAction->{'type'} eq 'transfer')
    {
        # Transfert inter-agences
        my $transferAction = (exists $tabData->{'transferAction'} ? $tabData->{'transferAction'} : undef);

        # Récupération des informations de la machine
        my $tabInfos = &getInfos($countryId, $machineId, GETINFOS_ALL);

        if (!$tabInfos)
        {
            return 0;
        }

        # Vérification de la date de modification (accès concurrentiel)
        if (!&_checkUpToDateData($tabInfos->{'modificationDate'}, $tabData->{'modificationDate'}))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0020);
            push(@{$tabErrors->{'modules'}->{'machine'}->{'fatal'}}, 0x0100);
            return 0;
        }

        my $tabUserInfos = &LOC::Session::getUserInfos();
        if ($tabUserInfos->{'id'} != $userId)
        {
            $tabUserInfos = &LOC::User::getInfos($userId);
        }
        # Récupération des droits
        my $tabRights = &_getRights($tabInfos, $tabUserInfos);

        # Paramètres

        my $oldTransferAgencyId = $tabInfos->{'agenciesTransfer.tabInfos'}->{'to.id'};
        # nouvelle agence de transfert :
        # - si on passe une agence de transfert -> code de l'agence ou undef
        # - sinon ancienne agence de transfert
        my $newTransferAgencyId = (exists $tabData->{'agenciesTransfer.tabInfos'}->{'to.id'} ?
                                        ($tabData->{'agenciesTransfer.tabInfos'}->{'to.id'} ne '' ? $tabData->{'agenciesTransfer.tabInfos'}->{'to.id'} : undef) :
                                        $oldTransferAgencyId);
        my $oldDelegatedAgencyId = $tabInfos->{'agenciesTransfer.tabInfos'}->{'delegatedAgency.id'};
        # nouvelle agence déléguée du transfert :
        # - si on passe une agence déléguée -> code de l'agence ou undef
        # - sinon ancienne agence déléguée
        my $newDelegatedAgencyId = (exists $tabData->{'agenciesTransfer.tabInfos'}->{'delegatedAgency.id'} ?
                                        ($tabData->{'agenciesTransfer.tabInfos'}->{'delegatedAgency.id'} ne '' ? $tabData->{'agenciesTransfer.tabInfos'}->{'delegatedAgency.id'} : undef) :
                                        $oldDelegatedAgencyId);
        my $oldTransferDate     = $tabInfos->{'agenciesTransfer.tabInfos'}->{'date'};
        # nouvelle date de transfert :
        # - si on passe une date de transfert -> date ou undef
        # - sinon ancienne date de transfert
        my $newTransferDate     = (exists $tabData->{'agenciesTransfer.tabInfos'}->{'date'} ?
                                        ($tabData->{'agenciesTransfer.tabInfos'}->{'date'} ne '' ? $tabData->{'agenciesTransfer.tabInfos'}->{'date'} : undef) :
                                        $oldTransferDate);

        my $tabTransferErrors = $tabErrors->{'modules'}->{'transfer'};

        # - Actions
        if ($transferAction)
        {
            # Agence s'occupant du transport : Agence déléguée si renseignée sinon agence de destination
            my $referenceAgencyId = ($tabInfos->{'agenciesTransfer.tabInfos'}->{'delegatedAgency.id'} ne '' ?
                                     $tabInfos->{'agenciesTransfer.tabInfos'}->{'delegatedAgency.id'} :
                                     $tabInfos->{'agenciesTransfer.tabInfos'}->{'to.id'});
            # Informations de l'agence s'occupant du transport
            my $referenceAgencyInfos = &LOC::Agency::getInfos($referenceAgencyId);
            # Pays s'occupant du transport
            my $referenceCountryId   = $referenceAgencyInfos->{'country.id'};

            # Annulation du transfert inter-agences
            if ($transferAction eq 'cancel')
            {
                # Vérification:
                if (!&_hasRight($tabRights->{'actions'}, 'cancelTransfer') ||
                    !$tabInfos->{'possibilities'}->{'actions'}->{'cancelTransfer'})
                {
                    push(@{$tabErrors->{'fatal'}}, 0x0003);
                    return 0;
                }
                if (!&LOC::Transport::cancelAgenciesTransfer($referenceCountryId, $machineId, $userId, $tabTransferErrors))
                {
                    push(@{$tabErrors->{'fatal'}}, 0x0010);
                    # Erreur fatale ?
                    if (&LOC::Util::in_array(0x0001, $tabTransferErrors->{'fatal'}))
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x0001);
                    }
                    # Modifications interdites ?
                    if (&LOC::Util::in_array(0x0003, $tabTransferErrors->{'fatal'}))
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x0003);
                    }
                    return 0;
                }
            }
            # Réalisation du transfert inter-agences
            elsif ($transferAction eq 'do')
            {
                # Vérification:
                if (!&_hasRight($tabRights->{'actions'}, 'doTransfer') ||
                    !$tabInfos->{'possibilities'}->{'actions'}->{'doTransfer'})
                {
                    push(@{$tabErrors->{'fatal'}}, 0x0003);
                    return 0;
                }
                if (!&LOC::Transport::doAgenciesTransfer($referenceCountryId, $machineId, undef, $userId, $tabTransferErrors))
                {
                    push(@{$tabErrors->{'fatal'}}, 0x0010);
                    # Erreur fatale ?
                    if (&LOC::Util::in_array(0x0001, $tabTransferErrors->{'fatal'}))
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x0001);
                    }
                    # Modifications interdites ?
                    if (&LOC::Util::in_array(0x0003, $tabTransferErrors->{'fatal'}))
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x0003);
                    }
                    return 0;
                }
            }
            # Annulation de la réalisation du transfert inter-agences
            elsif ($transferAction eq 'undo')
            {
                # Vérification:
                if (!&_hasRight($tabRights->{'actions'}, 'doTransfer') ||
                    !$tabInfos->{'possibilities'}->{'actions'}->{'doTransfer'})
                {
                    push(@{$tabErrors->{'fatal'}}, 0x0003);
                    return 0;
                }

                if (!&LOC::Transport::undoAgenciesTransfer($referenceCountryId, $machineId, $userId, $tabTransferErrors))
                {
                    push(@{$tabErrors->{'fatal'}}, 0x0010);
                    # Erreur fatale ?
                    if (&LOC::Util::in_array(0x0001, $tabTransferErrors->{'fatal'}))
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x0001);
                    }
                    # Modifications interdites ?
                    if (&LOC::Util::in_array(0x0003, $tabTransferErrors->{'fatal'}))
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x0003);
                    }
                    return 0;
                }
            }
        }

        # - Enregistrement
        # -- si pas d'action sur le transfert ou action d'annulation
        # -- ET
        # -- agence de transfert a changé ou date de transfert a changé
        if (!$transferAction &&
            ($oldTransferAgencyId ne $newTransferAgencyId || $oldTransferDate ne $newTransferDate ||
                $oldDelegatedAgencyId ne $newDelegatedAgencyId))
        {
            # Vérification des droits
            # - pour la création d'un nouveau transfert
            # OU
            # - pour la sélection d'une agence de délégation lors de la création d'un nouveau transfert
            if (!$transferAction &&
                ((!&_hasRight($tabRights->{'actions'}, 'createTransfer') ||
                 !$tabInfos->{'possibilities'}->{'actions'}->{'createTransfer'}) ||
                 (!&_hasRight($tabRights->{'actions'}, 'delegate') && $oldDelegatedAgencyId ne $newDelegatedAgencyId)))
            {
                push(@{$tabErrors->{'fatal'}}, 0x0003);
                return 0;
            }
            my $tabDataTransfer = {
                'to.id'              => $newTransferAgencyId,
                'date'               => $newTransferDate,
                'delegatedAgency.id' => $newDelegatedAgencyId};

            # Agence s'occupant du transport : Agence déléguée si renseignée sinon agence de destination
            my $referenceAgencyId = ($tabDataTransfer->{'delegatedAgency.id'} ne '' ? $tabDataTransfer->{'delegatedAgency.id'} :
                                     $tabDataTransfer->{'to.id'});
            # Informations de l'agence s'occupant du transport
            my $referenceAgencyInfos = &LOC::Agency::getInfos($referenceAgencyId);
            # Pays s'occupant du transport
            my $referenceCountryId   = $referenceAgencyInfos->{'country.id'};

            if(!&LOC::Transport::setAgenciesTransfer($referenceCountryId, $machineId, $tabDataTransfer, $userId, {}, $tabTransferErrors))
            {
                push(@{$tabErrors->{'fatal'}}, 0x0010);
                # Erreur fatale ?
                if (&LOC::Util::in_array(0x0001, $tabTransferErrors->{'fatal'}))
                {
                    push(@{$tabErrors->{'fatal'}}, 0x0001);
                }
                # Modifications interdites ?
                if (&LOC::Util::in_array(0x0003, $tabTransferErrors->{'fatal'}))
                {
                    push(@{$tabErrors->{'fatal'}}, 0x0003);
                }
                return 0;
            }
        }
    }

    return 1;
}


# Function: _checkUpToDateData
# Vérifie que les données soumises sont à jour.
#
# Params:
# string   $oldDate - La date de modification enregistrée.
# string   $newDate - La date de modification soumise.
#
# Return:
# int - 1 si les données soumises sont à jour, 0 si elles sont obsolètes.
sub _checkUpToDateData
{
    my ($oldDate, $newDate) = @_;

    if (defined $newDate && $newDate ne $oldDate)
    {
        return 0;
    }

    return 1;
}


# Function: _formatTemplate
# formate une chaîne de caractères
#
# Params:
# string    $code          - quel formatage à appliquer
# string    $value         - la chaîne template à formater
# hashref   $tabReplaces   - les valeurs de la chaîne à mettre dans le template
#
# Return:
# string - la chaîne formatée
sub _formatTemplate
{
    my ($code, $value, $tabReplaces) = @_;

    my @params = split(/\|/, $value);

    if ($code eq 'dte')
    {
        return &LOC::Template::formatDate($value, $tabReplaces);
    }
    # Types de modification de visibilité
    if ($code eq 'mod')
    {
        my $tabModif = $tabReplaces->{'visibility.modifications'};
        if ((&LOC::Util::in_array('create', $tabModif) && $params[1] eq 'create') ||
            (&LOC::Util::in_array('modif', $tabModif) && $params[1] eq 'modif')||
            (&LOC::Util::in_array('suppr', $tabModif) && $params[1] eq 'suppr')
            )
        {
            return $params[0];
        }
        else
        {
            return '';
        }
    }

    return $value;
}

# Function: _formatTraces
# Formate les traces
#
# Parameters:
# LOC::Db::Adapter  $db        - Connexion à la base de données
# string            $countryId - Identifiant du pays
# string            $traceCode - Code de la trace
# string|int        $value     - Valeur de la trace à formater
#
# Returns:
# string - Trace formatée
sub _formatTraces
{
    my ($db, $countryId, $traceCode, $value) = @_;

    # Récupération de la locale
    my $tabCountryInfos = &LOC::Country::getInfos($countryId);
    my $locale = &LOC::Locale::getLocale($tabCountryInfos->{'locale.id'}, undef, [POSIX::LC_MESSAGES, POSIX::LC_CTYPE]);
    my $schemaNameLoc = &LOC::Db::getSchemaName($countryId);

    # Etat de la machine
    if ($traceCode eq LOC::Log::EventType::UPDMACHINESTATE)
    {
        my $query = '
SELECT
    ETLIBELLE
FROM
    ' . $schemaNameLoc . '.ETATTABLE
WHERE
    ETCODE = ' . $db->quote($value) . ';';

        my $label = $db->fetchOne($query);
        return $label;
    }

    # Etat parc
    if ($traceCode eq LOC::Log::EventType::UPDMACHINEOUTOFPARK)
    {
        my $tab = {
            0 => $locale->t('Parc normal'),
            1 => $locale->t('Hors parc')
        };
        return $tab->{$value};
    }

    # Etat blocage sur chantier
    if ($traceCode eq LOC::Log::EventType::UPDMACHINESITEBLOCKED || $traceCode eq LOC::Log::EventType::UPDMACHINESITEUNBLOCKED)
    {
        my $tab = {
            0 => $locale->t('Débloquée'),
            1 => $locale->t('Bloquée')
        };
        return $tab->{$value};
    }

    # Changement de visibilité
    if ($traceCode eq LOC::Log::EventType::UPDMACHINEVISIBILITY)
    {
        my $tabAgencyInfo = &LOC::Agency::getInfos($value);
        return $tabAgencyInfo->{'label'};
    }

    return '';

}


# Function: _getDataForMailNewMachine
# Retourne les informations nécessaires au template du mail concernant les nouvelles machines
#
# Contenu du hashage :
# - tabReplaces   => Données pour le remplacement dans le template,
# - tabRecipients => Destinataires du mail,
#
# Parameters:
# string  $countryId - Identifiant du pays
# hashref $tabData   - Informations sur la machine
#
# Returns:
# hashref - Informations pour le template
sub _getDataForMailNewMachine
{
    my ($countryId, $tabData, $userId) = @_;

    # Récupération des infos sur l'utilisateur
    my $tabUserInfos = &LOC::User::getInfos($userId, undef, {'formatMode' => 1});

    # Récupération des données de la machine
    my $tabMachineInfos = &getInfos($countryId, $tabData->{'machineId'}, GETINFOS_BASE,
                                                    {'level' => LEVEL_MODEL});

    # - URL de la machine
    $tabMachineInfos->{'urlMachine'} = &getUrlByParkNumber($tabMachineInfos->{'parkNumber'});

    # Tableau des données à remplacer
    my $tabReplaces = {
        'creator.fullName'     => $tabUserInfos->{'fullName'},
        'parkNumber'           => $tabMachineInfos->{'parkNumber'},
        'model.label'          => $tabMachineInfos->{'model.label'},
        'serialNumber'         => $tabMachineInfos->{'serialNumber'},
        'date.creation'        => $tabMachineInfos->{'purchaseDate'},
        'date.control'         => $tabMachineInfos->{'control.nextDate'},
        'creationAgency.label' => $tabMachineInfos->{'agency.label'}
    };

    # Destinataires
    my @tabMailRecipients = split(',', &LOC::Characteristic::getCountryValueByCode('STATNVMACHINE', $countryId));

    my %tabRecipients;
    foreach my $email (@tabMailRecipients)
    {
        $tabRecipients{$email} = $email;
    }

    # Données finales
    my $mailData = {
        'tabReplaces'   => $tabReplaces,
        'tabRecipients' => \%tabRecipients
    };
    return $mailData;
}


# Function: _getDataForMailAgenciesTransfer
# Retourne les informations nécessaires au template du mail concernant les transferts inter-agences
#
# Contenu du hashage :
# - tabReplaces   => Données pour le remplacement dans le template,
# - tabRecipients => Destinataires du mail,
#
# Parameters:
# string  $countryId - Identifiant du pays
# hashref $tabOldInfos - Anciennes informations de la machine
# hashref $tabNewInfos - Nouvelles informations de la machine
# arrayre $tabModif    - Tableau des modifications effectuées. (create, suppr)
#
# Returns:
# hashref - Informations pour le template
sub _getDataForMailVisibleOnAgency
{
    my ($countryId, $tabOldInfos, $tabNewInfos, $tabModif) = @_;

    # Ancienne agence de la machine
    my $tabOldAgencyInfos = &LOC::Agency::getInfos($tabOldInfos->{'agency.id'});

    # Nouvelle agence de la machine
    my $tabNewAgencyInfos = &LOC::Agency::getInfos($tabNewInfos->{'agency.id'});

    # Ancienne agence de visibilité
    my $tabOldVisibilityAgencyInfos = LOC::Agency::getInfos($tabOldInfos->{'visibleOnAgency.id'});

    # Nouvelle agence de visibilité
    my $tabNewVisibilityAgencyInfos = LOC::Agency::getInfos($tabNewInfos->{'visibleOnAgency.id'});

    # Récupération des données de la machine
    my $tabMachineInfos = &getInfos($countryId, $tabNewInfos->{'id'}, GETINFOS_BASE);
    # - URL de la machine
    $tabMachineInfos->{'urlMachine'} = &getUrlByParkNumber($tabMachineInfos->{'parkNumber'});

    # Tableau des données à remplacer
    my $tabReplaces = {
        'visibility.modifications'   => $tabModif,
        'machine.parkNumber'         => $tabMachineInfos->{'parkNumber'},
        'machine.url'                => $tabMachineInfos->{'urlMachine'},
        'model.label'                => $tabMachineInfos->{'model.label'},
        'beforePhysicalAgency.label' => ($tabOldAgencyInfos ? $tabOldAgencyInfos->{'label'} : undef),
        'afterPhysicalAgency.label'  => ($tabNewAgencyInfos ? $tabNewAgencyInfos->{'label'} : undef),
        'beforeAgency.label'         => ($tabOldVisibilityAgencyInfos ? $tabOldVisibilityAgencyInfos->{'label'} : undef),
        'afterAgency.label'          => ($tabNewVisibilityAgencyInfos ? $tabNewVisibilityAgencyInfos->{'label'} : undef),
    };

    # Destinataires
    # - Agence de visibilité (avant et après)
    my %tabRecipients = (
        $tabOldAgencyInfos->{'email.shop'} => $tabOldAgencyInfos->{'label'},
        $tabNewAgencyInfos->{'email.shop'} => $tabNewAgencyInfos->{'label'}
    );
    if ($tabOldVisibilityAgencyInfos)
    {
        $tabRecipients{$tabOldVisibilityAgencyInfos->{'email.shop'}} = $tabOldVisibilityAgencyInfos->{'label'};
    }
    if ($tabNewVisibilityAgencyInfos)
    {
        $tabRecipients{$tabNewVisibilityAgencyInfos->{'email.shop'}} = $tabNewVisibilityAgencyInfos->{'label'};
    }

    # Données finales
    my $mailData = {
        'tabReplaces'  => $tabReplaces,
        'tabRecipients' => \%tabRecipients
    };
    return $mailData;
}

# Function: _hasRight
# Vérifier le droit à un champ
#
# Parameters:
# hashref $tab  - Tableau des droits
# string  $name - Nom du droit
#
# Returns:
# bool
sub _hasRight
{
    my ($tab, $name) = @_;

    return (ref($tab) eq 'HASH' ? ($tab->{$name} ? 1 : 0) : (index('|' . $tab . '|', '|' . $name . '|') == -1 ? 0 : 1));
}

# Function: updateAdditionalInfosValues
# Met à jour les valeurs des informations additionnelles de toutes les machines
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, -1 sinon
sub updateAdditionalInfosValues
{

    if (!&LOC::Globals::get('connectToKimoce'))
    {
        return 1;
    }

    # récupère les paramètres de connexion à la location
    my $db = &LOC::Db::getConnection('auth');
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Récupération des informations de l'article dans Kimoce
    my $tabMachinesAddInfos = &LOC::Kimoce::getMachineAddInfos();

    # Récupération de la liste des machines
    # et construction des numéros de parc à la mode Kimoce (seul le préfixe SL est conservé)
    my %tabMachines = &getList(LOC::Country::DEFAULT_COUNTRY_ID, LOC::Util::GETLIST_PAIRS);
    my $subrentPrefix = PREFIX_SUBRENT;
    foreach my $parkNumber (values(%tabMachines))
    {
        ($parkNumber) = ($parkNumber =~ /(($subrentPrefix)?\d+)$/);
    }
    # Inversion des clés et des valeurs
    %tabMachines = reverse(%tabMachines);

    # Récupération de la liste des informations additionnelle
    my %tabAddInfos = &LOC::Kimoce::getAddInfos(LOC::Util::GETLIST_PAIRS);
    # Inversion des clés et des valeurs
    %tabAddInfos = reverse(%tabAddInfos);

    # Suppression de toutes les données de la table f_machine_info
    if ($db->truncate('f_machine_info', 'AUTH') == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return -1;
    }

    # Parcours du tableau des informations additionnelles issues de Kimoce
    foreach my $parkNumber (keys(%$tabMachinesAddInfos))
    {
        # Identifiant de la machine à partir du numéro de parc Kimoce
        my $machineId = $tabMachines{$parkNumber};

        # vérification de l'existence de la machine dans la Gesloc
        if (defined $machineId)
        {
            # Mise à jour de chacune des informations additionnelles
            foreach my $addInfoCode (keys(%{$tabMachinesAddInfos->{$parkNumber}}))
            {
                # Identifiant de l'information additionnelle
                my $addInfoId = $tabAddInfos{$addInfoCode};

                # Vérification de l'existence de l'information additionnelle
                if (defined $addInfoId)
                {
                    # Type et valeur de l'information additionnelle
                    my $type = $tabMachinesAddInfos->{$parkNumber}->{$addInfoCode}->{'EntryTypExCde'};
                    my $value = $tabMachinesAddInfos->{$parkNumber}->{$addInfoCode}->{'ObjCharVal'};
                    # Type booléen
                    if ($type eq LOC::Kimoce::CHARTYPE_BOOLEAN)
                    {
                        $value = ($value eq 'Oui' ? 1 : ($value eq 'Non' ? 0 : undef));
                    }

                    if ($db->insert('f_machine_info', {
                                                       'min_mac_id'  => $machineId,
                                                       'min_inf_id'  => $addInfoId,
                                                       'min_value'   => $value
                                                      }) == -1)
                    {
                        # Annule la transaction
                        if (!$hasTransaction)
                        {
                            $db->rollBack();
                        }
                        return -1;
                    }
                }
            }
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }

    return 1;
}
# Function: finishCreation
# Finalise la création d'une machine
#
# string  $countryId  - Identifiant du pays
# int     $id         - Identifiant de la machine
# int     $userId     - Utilisateur responsable de la modification
# hashref $tabOptions - Tableau des options supplémentaires (facultatif). Peut contenir modificationDate
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, -1 sinon
sub finishCreation
{
    my ($countryId, $id, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = 'AUTH';

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'finishCreation', $_[0], __FILE__, '');

        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }

        return 0;
    };

    # Informations sur la machine
    my $tabInfos = &getInfos($countryId, $id);
    if (!$tabInfos)
    {
        return &$rollBackAndError(__LINE__);
    }

    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }
    # Récupération des droits
    my $tabRights = &_getRights($tabInfos, $tabUserInfos);

    # Vérification:
    # - Droits
    if (!&_hasRight($tabRights->{'actions'}, 'finishCreation'))
    {
        return &$rollBackAndError(__LINE__, ERROR_RIGHTS);
    }
    # - Création de machine déjà finalisée
    if ($tabInfos->{'isCreationFinished'})
    {
        return &$rollBackAndError(__LINE__, ERROR_CREATIONALREADYFINISHED);
    }
    # - Kimoce disponible
    my $isKimoceCallable = &LOC::Kimoce::isCallable();
    if (!$isKimoceCallable)
    {
        return &$rollBackAndError(__LINE__, ERROR_KIMOCEUNAVAILABLE);
    }
    # - Machine non existante dans Kimoce
    if (!&LOC::Kimoce::isMachineExists($tabInfos->{'parkNumber'}))
    {
        return &$rollBackAndError(__LINE__, ERROR_UNKNOWNMACHINEINKIMOCE);
    }

    # Mises à jour
    if ($db->update('MACHINE', {'MACREATIONOK' => -1}, {'MAAUTO*' => $id}, $schemaName) == -1)
    {
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Décrémentation de l'alerte des créations de machines à finaliser
    &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'agency.id'}, LOC::Alert::MACHINESCREATIONTOFINALIZE, -1);

    # Mises à jour Kimoce
    &LOC::EndUpdates::updateKimoceMachine($tabEndUpds, $tabInfos->{'parkNumber'},
                                          LOC::EndUpdates::KIMUPD_SITUATION |
                                          LOC::EndUpdates::KIMUPD_FEATURES |
                                          LOC::EndUpdates::KIMUPD_LOCALIZATION,
                                          {'agency.id' => $tabInfos->{'agency.id'},
                                           'state.id'  => $tabInfos->{'state.id'}});

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            return &$rollBackAndError(__LINE__);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: disableTelematics
# Désactive la télématique de la machine
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Identifiant de la machine
# int     $userId     - Utilisateur responsable de la modification
# hashref $tabOptions - Tableau des options supplémentaires (facultatif). Peut contenir modificationDate
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, -1 sinon
sub disableTelematics
{
    my ($countryId, $id, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'disableTelematics', $_[0], __FILE__, '');

        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }

        return 0;
    };

    # Récupération des informations de la machine
    my $tabInfos = &getInfos($countryId, $id, GETINFOS_BASE |
                                              GETINFOS_TELEMATICS |
                                              GETINFOS_POSSIBILITIES);
    if (!$tabInfos)
    {
        return &$rollBackAndError(__LINE__);
    }

    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }
    # Récupération des droits
    my $tabRights = &_getRights($tabInfos, $tabUserInfos);

    # Schema
    my $schemaName = 'AUTH';
    my $schemaNameLoc = &LOC::Db::getSchemaName($countryId);

    # Vérification de la date de modification (accès concurrentiel)
    if (!&_checkUpToDateData($tabInfos->{'modificationDate'}, $tabOptions->{'modificationDate'}))
    {
        return &$rollBackAndError(__LINE__, 0x0100);
    }

    # Vérification:
    if (!&_hasRight($tabRights->{'telematics'}, 'disable') ||
        !$tabInfos->{'possibilities'}->{'telematics'}->{'disable'})
    {
        return &$rollBackAndError(__LINE__, 0x0002);
    }

    my $tabUpdates = {
        'IDTarget'        => undef,
        'MAPORTEETELEMAT' => undef,
        'MATELEMATSUSP'   => 0
    };

    if ($db->update('MACHINE', $tabUpdates, {'MAAUTO*' => $id}, $schemaName) == -1)
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    # Suppression des codes de démarrage
    if (!&LOC::StartCode::clean($countryId, $userId, {'machine.id' => $id}))
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    # Suppression des liens avec les contrats
    if (!&LOC::StartCode::Contract::Rent::cleanLinks($countryId, $userId, {'machine.id' => $id}))
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }


    # Ajout de trace
    &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $id,
                           {'schema'  => $schemaNameLoc,
                            'code'    => LOC::Log::EventType::DISABLETELEMATICS});


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            return &$rollBackAndError(__LINE__);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $tabUserInfos->{'id'});
    }


    return 1;
}


# Function: suspendTelematics
# Suspend la télématique de la machine
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Identifiant de la machine
# int     $userId     - Utilisateur responsable de la modification
# hashref $tabOptions - Tableau des options supplémentaires (facultatif). Peut contenir modificationDate
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, -1 sinon
sub suspendTelematics
{
    my ($countryId, $id, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    $tabOptions->{'isTelematicSuspended'} = 1;

    return &toggleTelematics($countryId, $id, $userId, $tabOptions, $tabErrors, $tabEndUpds);
}


# Function: unsuspendTelematics
# Réactive la télématique de la machine
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Identifiant de la machine
# int     $userId     - Utilisateur responsable de la modification
# hashref $tabOptions - Tableau des options supplémentaires (facultatif). Peut contenir modificationDate
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, -1 sinon
sub unsuspendTelematics
{
    my ($countryId, $id, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }
    $tabOptions->{'isTelematicSuspended'} = 0;

    return &toggleTelematics($countryId, $id, $userId, $tabOptions, $tabErrors, $tabEndUpds);
}


# Function: toggleTelematics
# Suspend ou réactive la télématique de la machine
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Identifiant de la machine
# int     $userId     - Utilisateur responsable de la modification (facultatif)
# hashref $tabOptions - Tableau des options supplémentaires (facultatif). Peut contenir modificationDate
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, 0 sinon
sub toggleTelematics
{
    my ($countryId, $id, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'toggleTelematics', $_[0], __FILE__, '');

        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }

        return 0;
    };

    # Récupération des informations de la machine
    my $tabInfos = &getInfos($countryId, $id, GETINFOS_BASE |
                                              GETINFOS_TELEMATICS |
                                              GETINFOS_POSSIBILITIES);
    if (!$tabInfos)
    {
        return &$rollBackAndError(__LINE__);
    }

    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if (defined $userId && $tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }
    # Récupération des droits
    my $tabRights = &_getRights($tabInfos, $tabUserInfos);

    # Schema
    my $schemaName = 'AUTH';
    my $schemaNameLoc = &LOC::Db::getSchemaName($countryId);

    # Vérification de la date de modification (accès concurrentiel)
    if (!&_checkUpToDateData($tabInfos->{'modificationDate'}, $tabOptions->{'modificationDate'}))
    {
        return &$rollBackAndError(__LINE__, 0x0100);
    }

    # Vérification des droits si un utilisateur a été passé en paramètre
    # et des possibilités
    if (defined $userId && !&_hasRight($tabRights->{'telematics'}, 'toggle') ||
        !$tabInfos->{'possibilities'}->{'telematics'}->{'toggle'})
    {
        return &$rollBackAndError(__LINE__, 0x0002);
    }

    # Changement de l'état d'activation de la télématique
    my $oldIsTelematicSuspended = ($tabInfos->{'isTelematicSuspended'} ? 1 : 0);
    my $newIsTelematicSuspended = ($tabOptions->{'isTelematicSuspended'} ? 1 : 0);

    if ($oldIsTelematicSuspended != $newIsTelematicSuspended)
    {
        my $tabUpdates = {
            'MATELEMATSUSP' => ($newIsTelematicSuspended ? -1 : 0)
        };

        if ($db->update('MACHINE', $tabUpdates, {'MAAUTO*' => $id}, $schemaName) == -1)
        {
            return &$rollBackAndError(__LINE__, 0x0001);
        }


        # Ajout de trace
        &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $id,
                               {'schema'  => $schemaNameLoc,
                                'code'    => ($newIsTelematicSuspended ? LOC::Log::EventType::SUSPENDTELEMATICS : LOC::Log::EventType::REACTIVATETELEMATICS)});
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            return &$rollBackAndError(__LINE__);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $tabUserInfos->{'id'});
    }


    return 1;
}


# Function: activateTelematicsRentContractScope
# Active la génération des codes de démarrage contrats
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Identifiant de la machine
# int     $userId     - Utilisateur responsable de la modification
# hashref $tabOptions - Tableau des options supplémentaires (facultatif). Peut contenir modificationDate
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, -1 sinon
sub activateTelematicsRentContractScope
{
    my ($countryId, $id, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'activateTelematicsRentContractScope', $_[0], __FILE__, '');

        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }

        return 0;
    };

    # Récupération des informations de la machine
    my $tabInfos = &getInfos($countryId, $id, GETINFOS_BASE |
                                              GETINFOS_TELEMATICS |
                                              GETINFOS_POSSIBILITIES);
    if (!$tabInfos)
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    # Vérification de la date de modification (accès concurrentiel)
    if (!&_checkUpToDateData($tabInfos->{'modificationDate'}, $tabOptions->{'modificationDate'}))
    {
        return &$rollBackAndError(__LINE__, 0x0100);
    }

    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }
    # Récupération des droits
    my $tabRights = &_getRights($tabInfos, $tabUserInfos);

    # Schema
    my $schemaName = 'AUTH';
    my $schemaNameLoc = &LOC::Db::getSchemaName($countryId);


    # Vérification:
    if (!&_hasRight($tabRights->{'telematics'}, 'activateRentContractScope') ||
        !$tabInfos->{'possibilities'}->{'telematics'}->{'activateRentContractScope'})
    {
        return &$rollBackAndError(__LINE__, 0x0002);
    }

    my $tabScope = $tabInfos->{'telematicScope'};
    push(@$tabScope, LOC::StartCode::Contract::Rent::SCOPE);

    my $tabUpdates = {
        'MAPORTEETELEMAT' => &LOC::Json::toJson($tabScope)
    };

    if ($db->update('MACHINE', $tabUpdates, {'MAAUTO*' => $id}, $schemaName) == -1)
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    # Génération des codes de démarrage sur le contrat en cours
    if ($tabInfos->{'state.id'} eq STATE_RENTED || $tabInfos->{'state.id'} eq STATE_DELIVERY)
    {
        if (!&LOC::StartCode::Contract::Rent::generateAll($countryId, $tabInfos->{'contract.id'}, $userId, {}, {}, $tabEndUpds))
        {
            return &$rollBackAndError(__LINE__, 0x0001);
        }
    }

    # Modification de la date de fin de validité des codes réservés sur la machine
    &LOC::StartCode::setReservedCodesEndDate($countryId, $tabInfos->{'id'});

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            return &$rollBackAndError(__LINE__);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $tabUserInfos->{'id'});
    }


    return 1;
}

# Function: changeTelematicsEquipment
# Initialise le boîtier télématique suite à un changement
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Identifiant de la machine
# int     $userId     - Utilisateur responsable de la modification
# hashref $tabOptions - Tableau des options supplémentaires (facultatif). Peut contenir modificationDate
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, -1 sinon
sub changeTelematicsEquipment
{
    my ($countryId, $id, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Schema
    my $schemaName = 'AUTH';
    my $schemaNameLoc = 'LOCATION' . ($countryId eq 'FR' ? '' : $countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'changeTelematicsEquipment', $_[0], __FILE__, '');

        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }

        return 0;
    };

    # Récupération des informations de la machine
    my $tabInfos = &getInfos($countryId, $id, GETINFOS_BASE |
                                              GETINFOS_TELEMATICS |
                                              GETINFOS_POSSIBILITIES);
    if (!$tabInfos)
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    # Vérification de la date de modification (accès concurrentiel)
    if (!&_checkUpToDateData($tabInfos->{'modificationDate'}, $tabOptions->{'modificationDate'}))
    {
        return &$rollBackAndError(__LINE__, 0x0100);
    }

    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }
    # Récupération des droits
    my $tabRights = &_getRights($tabInfos, $tabUserInfos);

    # Vérification:
    if (!&_hasRight($tabRights->{'telematics'}, 'changeEquipment'))
    {
        return &$rollBackAndError(__LINE__, 0x0002);
    }

    my $tabUpdates = {
        'MADATEMODIF*' => 'NOW()'
    };

    if ($db->update('MACHINE', $tabUpdates, {'MAAUTO*' => $id}, $schemaName) == -1)
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    if (!&LOC::StartCode::changeEquipment($countryId, $id, $userId, $tabErrors, $tabEndUpds))
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            return &$rollBackAndError(__LINE__);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $tabUserInfos->{'id'});
    }


    return 1;
}


# Function: setDigiKeyDeactivated
# Désactive ou réactive le digicode de la machine
#
# Parameters:
# string  $countryId     - Identifiant du pays
# int     $id            - Identifiant de la machine
# int     $isDeactivated - 1 pour désactiver les digicodes, 0 pour les activer.
# int     $userId        - Utilisateur responsable de la modification
# hashref $tabOptions    - Tableau des options supplémentaires (facultatif). Peut contenir modificationDate
# hashref $tabErrors     - Tableau des erreurs
# hashref $tabEndUpds    - Mises à jour de fin
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, -1 sinon
sub setDigiKeyDeactivated
{
    my ($countryId, $id, $isDeactivated, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'setDigiKeyDeactivated', $_[0], __FILE__, '');

        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }

        return 0;
    };

    # Récupération des informations de la machine
    my $tabInfos = &getInfos($countryId, $id, GETINFOS_BASE |
                                              GETINFOS_TELEMATICS |
                                              GETINFOS_POSSIBILITIES);
    if (!$tabInfos)
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    # Vérification de la date de modification (accès concurrentiel)
    if (!&_checkUpToDateData($tabInfos->{'modificationDate'}, $tabOptions->{'modificationDate'}))
    {
        return &$rollBackAndError(__LINE__, 0x0100);
    }

    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }
    # Récupération des droits
    my $tabRights = &_getRights($tabInfos, $tabUserInfos);

    # Schema
    my $schemaName = 'AUTH';
    my $schemaNameLoc = &LOC::Db::getSchemaName($countryId);


    # Vérification:
    if (!&_hasRight($tabRights->{'telematics'}, 'digiKeyActivation') ||
        !$tabInfos->{'possibilities'}->{'telematics'}->{'digiKeyActivation'})
    {
        return &$rollBackAndError(__LINE__, 0x0002);
    }

    # Modification de la date de fin de validité des codes réservés sur la machine
    my $servicesWs = &LOC::Globals::get('servicesWs');
    my $service = SOAP::Lite->uri($servicesWs->{'telematics'}->{'uri'})
                            ->proxy($servicesWs->{'telematics'}->{'location'})
                            ->setDigiKeyActivationState($tabInfos->{'telematicId'}, !$isDeactivated);
    if ($service->fault() || !$service->result())
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    my $tabUpdates = {
        'MADIGIINACTIF' => ($isDeactivated ? -1 : 0)
    };

    if ($db->update('MACHINE', $tabUpdates, {'MAAUTO*' => $id}, $schemaName) == -1)
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    # Trace d'activation/désactivation du digicode
    my $tabContent = {
        'schema'  => $schemaNameLoc,
        'code'    => ($isDeactivated ? LOC::Log::EventType::DEACTIVATESTARTCODE : LOC::Log::EventType::ACTIVATESTARTCODEACTIVE),
        'old'     => undef,
        'new'     => undef,
        'content' => ''
    };
    &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $id, $tabContent);


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            return &$rollBackAndError(__LINE__);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $tabUserInfos->{'id'});
    }


    return 1;
}


# Function: sendTelematicsRentContractCodesSms
# Envoi par SMS les codes de démarrage liés à un contrat.
#
# Parameters:
# string  $countryId  - L'identifiant du pays.
# int     $id         - L'identifiant de la machine.
# int     $contractId - L'identifiant du contrat de location.
# int     $userId     - L'utilisateur responsable de la régénération.
# hashref $tabOptions - Le tableau des options supplémentaires (facultatif). Peut contenir modificationDate.
# hashref $tabErrors  - Le tableau des erreurs.
# hashref $tabEndUpds - Les mises à jour de fin.
#
# Returns:
# bool - Retourne 1 si la régénération s'est correctement effectuée, -1 sinon.
sub sendSmsTelematicsRentContractCodes
{
    my ($countryId, $id, $contractId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Schema
    my $schemaName = 'AUTH';
    my $schemaNameLoc = 'LOCATION' . ($countryId eq 'FR' ? '' : $countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'sendTelematicsRentContractCodesSms', $_[0], __FILE__, '');

        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }

        return 0;
    };

    # Récupération des informations de la machine
    my $tabInfos = &getInfos(
        $countryId,
        $id,
        GETINFOS_BASE | GETINFOS_TELEMATICS | GETINFOS_POSSIBILITIES,
        {
            'level' => LEVEL_MACHINESFAMILY
        }
    );
    if (!$tabInfos)
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    # Vérification de la date de modification (accès concurrentiel)
    if (!&_checkUpToDateData($tabInfos->{'modificationDate'}, $tabOptions->{'modificationDate'}))
    {
        return &$rollBackAndError(__LINE__, 0x0100);
    }

    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }
    # Récupération des droits
    my $tabRights = &_getRights($tabInfos, $tabUserInfos);

    # Vérification:
    if (!&_hasRight($tabRights->{'telematics'}, 'customer'))
    {
        return &$rollBackAndError(__LINE__, 0x0002);
    }

    my $tabUpdates = {
        'MADATEMODIF*' => 'NOW()'
    };

    if ($db->update('MACHINE', $tabUpdates, {'MAAUTO*' => $id}, $schemaName) == -1)
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }


    # Récupération des informations pour l'envoi du SMS
    my $tabFilters = {
        'contractId' => $contractId,
        'typeCode'   => LOC::StartCode::Contract::Rent::TYPE_CUSTOMER,
        'stateId'    => LOC::StartCode::STATE_ACTIVE,
        'rule'       => {
            'stateId' => LOC::StartCode::STATE_RULE_ACTIVE
        }
    };
    my $tabStartCodesList = &LOC::StartCode::Contract::Rent::getList(
        $countryId,
        LOC::Util::GETLIST_ASSOC,
        $tabFilters,
        LOC::StartCode::GETINFOS_RULESLIST |
        LOC::StartCode::Contract::Rent::GETINFOS_LINKEDCONTRACTS
    );

    # Nombre de codes différent de 1
    my $nbStartCodes = scalar values(%$tabStartCodesList);
    if ($nbStartCodes != 1)
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    my $tabStartCodeInfos = (values(%$tabStartCodesList))[0];

    # Nombre de règles différent de 1
    my $nbRules = scalar values(%{$tabStartCodeInfos->{'rulesList'}});
    if ($nbRules != 1)
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    my $tabContractInfos = $tabStartCodeInfos->{'linkedContracts'}->{$contractId};

    # Pas de numéro de téléphone
    if (!$tabContractInfos->{'contact.telephone'})
    {
        return &$rollBackAndError(__LINE__, 0x0004);
    }

    my $tabReceiversInfos = {
        $tabContractInfos->{'id'} => $tabContractInfos->{'contact.telephone'}
    };

    my $tabSmsInfos = {
        'code'                 => $tabStartCodeInfos->{'code'},
        'machine.parkNumber'   => $tabInfos->{'parkNumber'},
        'machinesFamily.label' => $tabInfos->{'machinesFamily.label'},
        'beginDate'            => $tabStartCodeInfos->{'period'}->{'beginDate'},
        'endDate'              => $tabStartCodeInfos->{'period'}->{'endDate'},
        'site.label'           => $tabContractInfos->{'site.label'},
        'site.locality.name'   => $tabContractInfos->{'site.locality.name'}
    };

    my $sendSms = &LOC::StartCode::Contract::Rent::sendSms(
        $countryId,
        $tabUserInfos->{'id'},
        $tabReceiversInfos,
        $tabSmsInfos,
        $tabErrors
    );
    if (!$sendSms)
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            return &$rollBackAndError(__LINE__);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $tabUserInfos->{'id'});
    }

    return 1;
}


# Function: regenerateTelematicsRentContractCodes
# Régénère les codes de démarrage liés à un contrat.
#
# Parameters:
# string  $countryId  - L'identifiant du pays.
# int     $id         - L'identifiant de la machine.
# int     $contractId - L'identifiant du contrat de location.
# int     $userId     - L'utilisateur responsable de la régénération.
# hashref $tabOptions - Le tableau des options supplémentaires (facultatif). Peut contenir modificationDate.
# hashref $tabErrors  - Le tableau des erreurs.
# hashref $tabEndUpds - Les mises à jour de fin.
#
# Returns:
# bool - Retourne 1 si la régénération s'est correctement effectuée, -1 sinon.
sub regenerateTelematicsRentContractCodes
{
    my ($countryId, $id, $contractId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Schema
    my $schemaName = 'AUTH';
    my $schemaNameLoc = 'LOCATION' . ($countryId eq 'FR' ? '' : $countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }

        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'regenerateTelematicsRentContractCodes', $_[0], __FILE__, '');

        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }

        return 0;
    };

    # Récupération des informations de la machine
    my $tabInfos = &getInfos($countryId, $id, GETINFOS_BASE |
                                              GETINFOS_TELEMATICS |
                                              GETINFOS_POSSIBILITIES);
    if (!$tabInfos)
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    # Vérification de la date de modification (accès concurrentiel)
    if (!&_checkUpToDateData($tabInfos->{'modificationDate'}, $tabOptions->{'modificationDate'}))
    {
        return &$rollBackAndError(__LINE__, 0x0100);
    }

    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }
    # Récupération des droits
    my $tabRights = &_getRights($tabInfos, $tabUserInfos);

    # Vérification:
    if (!&_hasRight($tabRights->{'telematics'}, 'regenerateRentContractCodes'))
    {
        return &$rollBackAndError(__LINE__, 0x0002);
    }

    my $tabUpdates = {
        'MADATEMODIF*' => 'NOW()'
    };

    if ($db->update('MACHINE', $tabUpdates, {'MAAUTO*' => $id}, $schemaName) == -1)
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    if (!&LOC::StartCode::Contract::Rent::generateAll($countryId, $contractId, $userId, {}, $tabErrors, $tabEndUpds))
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            return &$rollBackAndError(__LINE__);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $tabUserInfos->{'id'});
    }

    return 1;
}

1;