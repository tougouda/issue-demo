use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Printer
# Module permettant d'obtenir les informations d'une imprimante
package LOC::Printer;
use strict;

# Modules internes
use LOC::Db;

# Constants: États
# STATE_ACTIVE  - Agence active
# STATE_DELETED - Agence supprimée
use constant
{
    STATE_ACTIVE  => 'GEN01',
    STATE_DELETED => 'GEN03',
};

# Function: getInfos
# Retourne les infos d'une imprimante
# 
# Contenu du hashage : voir la fonction getList.
#
# Parameters:
# string $id - Identifiantd e l'imprimante
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getInfos
{
    my ($id) = @_;

    my $tab = &getList(LOC::Util::GETLIST_ASSOC, {'id' => $id});
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Retourne la liste des imprimantes
#
# Contenu de la table de hachage :
# - id : identifiant de l'imprimante ;
#- agency.id : identifiant de l'agence ;
#- agency.label : nom de l'agence ;
#- label : libellé ;
#- location : emplacement ;
#- ip : adresse IP ;
#- creationDate : date de création ;
#- modificationDate : date de modification ;
#- state.id : identifiant de l'état.
#
# Parameters:
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres ('id' : liste des identifiants d'imprimantes,
#                                'agency' : liste des identifiants d'agences)
#
# Returns:
# hash|hashref|0 - Liste des imprimantes
sub getList
{
    my ($format, $tabFilters) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('frmwrk');

    my $query = '';

    # Filtres
    my $whereQuery = '';
    # - Imprimantes
    if ($tabFilters->{'agency'} ne '')
    {
        $whereQuery .= '
AND prt_agc_id IN (' . $db->quote($tabFilters->{'agency'}) . ')';
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    prt_id AS `id`,
    prt_agc_id AS `agency.id`,
    agc_label AS `agency.label`,
    prt_label AS `label`,
    prt_location AS `location`,
    prt_ip AS `ip`,
    prt_date_create AS `creationDate`,
    prt_date_modif AS `modificationDate`,
    prt_sta_id AS `state.id`
FROM p_printer
LEFT JOIN p_agency ON prt_agc_id = agc_id
WHERE prt_sta_id <> ' . $db->quote(STATE_DELETED) .
$whereQuery . '
ORDER BY `label`;';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*) AS `label`
FROM p_printer
WHERE prt_sta_id <> ' . $db->quote(STATE_DELETED) .
$whereQuery . ';';
    }
    else
    {
        $query = '
SELECT
    prt_id AS `id`,
    prt_label AS `label`
FROM p_printer
WHERE prt_sta_id <> ' . $db->quote(STATE_DELETED) .
$whereQuery . '
ORDER BY `label`;';
    }

    return $db->fetchList($format, $query, {}, 'id');
}

# Function: getStatus
# Récupère l'état courant de l'imprimante grâce à snmpwalk
#
# Parameters:
# string  $ip - Adresse IP de l'imprimante
#
# Returns:
# string - Code état
sub getStatus
{
    my ($ip) = @_;

    my $snmpResult = `snmpwalk -r 1 -c public -v 1 -Ov $ip 1.3.6.1.4.1.11.2.3.9.4.2.1.1.2.5.0`;
    my @tabResult = split(/:/, $snmpResult);
    
    my $result = 0;
    if ($tabResult[1] == 1)
    {
        $result = 1;
    }
    if ($tabResult[1] == 2)
    {
        $result = 2;
    }
    
    return $result;
}


# Function: addLog
# Historise les impressions de documents
#
# Parameters:
# string $document   - Type de document
# int    $documentId - Identifiant du document
#
# Returns:
# bool
sub addLog
{
    my ($document, $documentId) = @_;
    
    if ($document eq '')
    {
        $document = 'unknown';
    }
    unless (defined $documentId)
    {
        $documentId = 0;
    }
    
    my $conn = &LOC::Db::getConnection('auth');

    my $sessionInfos = &LOC::Session::getInfos();
    my $sessionId = $sessionInfos->{'id'};

    my $result = $conn->insert('SESSIONIMP', {
                                              'SESSIONAUTO' => $sessionId,
                                              'SESSIONIMPDOC' => $document,
                                              'SESSIONIMPIDENTIF' => $documentId,
                                             });

    if (!$result)
    {
        return 0;
    }
    return 1;
}

1;