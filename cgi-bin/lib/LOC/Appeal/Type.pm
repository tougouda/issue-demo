use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Appeal::Type
# Module permettant d'obtenir les types de gestion de recours
package LOC::Appeal::Type;


# Constants: Récupération d'informations sur les types de gestion de recours
# GETINFOS_ALL  - Récupération de l'ensemble des informations
# GETINFOS_BASE - Récupération des informations de base
use constant
{
    GETINFOS_ALL  => 0xFFFFFFFF,
    GETINFOS_BASE => 0x00000000
};


# Constants: Mode de calcul
# MODE_NOTINVOICED     - Non facturé
# MODE_CALENDAR        - Pourcentage du prix de location jour fois jours calendaires

use constant
{
    MODE_NOTINVOICED => 0,
    MODE_CALENDAR    => 1
};


# Constants: États
# STATE_ACTIVE  - Actif
# STATE_DELETED - Supprimé
use constant
{
    STATE_ACTIVE  => 'GEN01',
    STATE_DELETED => 'GEN03'
};

use strict;

# Modules internes
use LOC::Db;
use LOC::Util;


# Function: getInfos
# Retourne les infos d'un type de gestion de recours
# 
# Parameters:
# string  $countryId  - Pays
# int     $id         - Identifiant du type
# int     $flags      - Flags (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hashref|undef - Retourne un hashref suivant la demande, undef si pas de résultat
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);

    my ($id, $tabInfos) = each(%$tab);

    return ($tabInfos ? (wantarray ? %$tabInfos : $tabInfos) : undef);
}

# Function: getList
# Récupérer la liste des types de gestion de recours
#
# Contenu du hachage:
# - id         => id,
# - label      => libellé,
# - extraLabel => complément de libellé,
# - fullLabel  => libellé complet
# - mode       => mode de calcul,
# - state.id   => identifiant de l'état
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id, energy)
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (ref($tabFilters) ne 'HASH')
    {
        $tabFilters = {};
    }
    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query;
    my @tabWheres;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query .= '
SELECT
    apl_id AS `id`,
    apl_label AS `label`,
    apl_labelext AS `extraLabel`,
    apl_labelfull AS `fullLabelMask`,
    apl_mode AS `mode`,
    apl_is_default AS `isDefault`,
    apl_sta_id AS `state.id`';
    }
    else
    {
        $query = '
SELECT
    apl_id,
    apl_label';
    }

    $query .= '
FROM p_appealtype';

    ## Filtres
    # Id
    if (defined $tabFilters->{'id'})
    {
        push(@tabWheres, 'apl_id IN (' . $db->quote($tabFilters->{'id'}) . ')');
    }
    # - État
    if (exists $tabFilters->{'stateId'})
    {
        push(@tabWheres, 'apl_sta_id IN (' . $db->quote($tabFilters->{'stateId'}) . ')');
    }
    elsif (!exists $tabFilters->{'stateId'} && !defined $tabFilters->{'id'})
    {
        push(@tabWheres, 'apl_sta_id <> ' . $db->quote(STATE_DELETED));
    }

    if (@tabWheres > 0)
    {
        $query .= '
WHERE ' . join("\nAND ", @tabWheres);
    }

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}        *= 1;
            $tabRow->{'mode'}      *= 1;
            $tabRow->{'isDefault'} *= 1;

            if ($tabRow->{'fullLabelMask'} ne '')
            {
                my $insuranceLabel = $tabRow->{'fullLabelMask'};
                $insuranceLabel =~ s/<%label>/$tabRow->{'label'}/g;
                $insuranceLabel =~ s/<%extraLabel>/$tabRow->{'extraLabel'}/g;

                $tabRow->{'fullLabel'} = $insuranceLabel;
            }
            else
            {
                $tabRow->{'fullLabel'} = $tabRow->{'label'};
            }

        }
    }

    return (wantarray ? %$tab : $tab);
}

# Function: getDefaultId
# Retourne l'identifiant du type de gestion de recours par défaut
# 
# Parameters:
# string  $countryId  - Pays
#
# Returns:
# int
sub getDefaultId
{
    my ($countryId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT
    apl_id
FROM p_appealtype
WHERE
    apl_is_default = 1
    AND apl_sta_id = ' . $db->quote(STATE_ACTIVE) . ';';

    return $db->fetchOne($query) * 1;
}


1;