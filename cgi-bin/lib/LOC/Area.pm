use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Area
# Module permettant d'obtenir les informations d'une région
package LOC::Area;


# Constants: États
# STATE_ACTIVE  - Région active
# STATE_DELETED - Région supprimée
use constant
{
    STATE_ACTIVE  => 'GEN01',
    STATE_DELETED => 'GEN03',
};


use strict;

# Modules internes
use LOC::Db;


# Function: getInfos
# Retourne les infos d'une région
# 
# Contenu du hashage : voir la fonction getList.
#
# Parameters:
# int $id - Identifiant de la région
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getInfos
{
    my ($id) = @_;

    my $tab = &getList(LOC::Util::GETLIST_ASSOC, {'id' => $id});
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Retourne la liste des régions
#
# Contenu de la table de hachage :
# - id : identifiant de la région ;
# - label : nom ;
# - country.id : identifiant du pays ;
# - manager.id : identifiant du responsable de région ;
# - manager.name : nom du responsable de région ;
# - manager.firstname : prénom du responsable de région ;
# - manager.fullName : nom complet du responsable de région ;
# - creationDate : date et heure de création ;
# - modificationDate : date et heure de modification ;
# - state.id : identifiant de l'état.
#
# Parameters:
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres ('country' : liste des pays, 'id' : Identifiant de la région)
#
# Returns:
# hash|hashref|0 - Liste des agences
sub getList
{
    my ($format, $tabFilters) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('auth');

    my $query = '';

    # Filtres
    my $whereQuery = '';
    # - Identifiant
    if (defined $tabFilters->{'id'})
    {
        $whereQuery = '
AND are_id IN (' . $db->quote($tabFilters->{'id'}) . ')';
    }
    # - Pays
    if ($tabFilters->{'country'} ne '')
    {
        $whereQuery .= '
 AND are_cty_id IN (' . $db->quote($tabFilters->{'country'}) . ')';
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    are_id AS `id`,
    are_label AS `label`,
    are_code AS `code`,
    are_cty_id AS `country.id`,
    are_usr_id AS `manager.id`,
    tbl_f_user.usr_name AS `manager.name`,
    tbl_f_user.usr_firstname AS `manager.firstname`,
    CONCAT_WS(" ", tbl_f_user.usr_name, tbl_f_user.usr_firstname) AS `manager.fullName`,
    tbl_f_techuser.usr_name AS `techManager.name`,
    tbl_f_techuser.usr_firstname AS `techManager.firstname`,
    CONCAT_WS(" ", tbl_f_techuser.usr_name, tbl_f_techuser.usr_firstname) AS `techManager.fullName`,
    tbl_f_techuser.usr_mail AS `techManager.email`,
    are_date_create AS `creationDate`,
    are_date_modif AS `modificationDate`,
    are_sta_id AS `state.id`
FROM frmwrk.p_area
LEFT JOIN frmwrk.f_user tbl_f_user
ON are_usr_id = tbl_f_user.usr_id
LEFT JOIN frmwrk.f_user tbl_f_techuser
ON are_usr_id_techmanager = tbl_f_techuser.usr_id
WHERE are_sta_id <> ' . $db->quote(STATE_DELETED) .
$whereQuery . '
ORDER BY `label`;';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*)
FROM frmwrk.p_area
WHERE are_sta_id <> ' . $db->quote(STATE_DELETED) .
$whereQuery . ';';
    }
    else
    {
        $query = '
SELECT
    are_id AS `id`,
    are_label AS `label`
FROM frmwrk.p_area
WHERE are_sta_id <> ' . $db->quote(STATE_DELETED) .
$whereQuery . '
ORDER BY `label`;';
    }

    return $db->fetchList($format, $query, {}, 'id');
}

1;