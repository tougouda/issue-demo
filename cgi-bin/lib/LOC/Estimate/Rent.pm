use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Estimate::Rent
# Module de gestion des devis de location
package LOC::Estimate::Rent;


# Constantes: Etats
# STATE_ACTIVE   - Etat du devis : actif
# STATE_ABORTED  - Etat du devis : abandonné
# STATE_CONTRACT - Etat du devis : passé en contrat
#
# LINE_STATE_ACTIVE    - Etat d'une ligne de devis : active
# LINE_STATE_CONVERTED - Etat d'une ligne de devis : passée en contrat
# LINE_STATE_INACTIVE  - Etat d'une ligne de devis : désactivée
use constant {
    STATE_ACTIVE   => 'DEV01',
    STATE_ABORTED  => 'DEV02',
    STATE_CONTRACT => 'DEV03',

    LINE_STATE_ACTIVE    => 'DDV01',
    LINE_STATE_CONVERTED => 'DDV02',
    LINE_STATE_INACTIVE  => 'DDV03'
};

# Constants: États des services de location
# SERVICESTATE_UNBILLED - Non facturé
# SERVICESTATE_BILLED   - Facturé
# SERVICESTATE_DELETED  - Supprimé
use constant {
    SERVICESTATE_UNBILLED => 'SER01',
    SERVICESTATE_BILLED   => 'SER02',
    SERVICESTATE_DELETED  => 'SER03'
};

# Constantes: Récupération d'informations sur le devis
# GETINFOS_ALL                - Récupération de l'ensemble des informations
# GETINFOS_BASE               - Récupération des informations de base
# GETINFOS_CUSTOMER           - Récupération des informations du client
# GETINFOS_EXT_SITE           - Récupération des informations du chantier
# GETINFOS_CREATOR            - Récupération des informations du créateur
# GETINFOS_ERRONEOUSLINES     - Récupération des informations sur les lignes de devis en erreur
# GETINFOS_PRINTABLE          - Récupération des informations sur les droits d'impression du devis
# GETINFOS_RIGHTS             - Récupération des informations sur les droits
# GETINFOS_NEGOTIATOR         - Récupération des informations sur le négociateur
# GETINFOS_VALUEDATE          - Récupération d'informations sur la date de valeur
use constant {
    GETINFOS_ALL            => 0xFFFF,
    GETINFOS_BASE           => 0x0000,
    GETINFOS_CUSTOMER       => 0x0001,
    GETINFOS_EXT_SITE       => 0x0002,
    GETINFOS_CREATOR        => 0x0008,
    GETINFOS_ERRONEOUSLINES => 0x0010,
    GETINFOS_PRINTABLE      => 0x0020,
    GETINFOS_RIGHTS         => 0x0040,
    GETINFOS_NEGOTIATOR     => 0x0080,
    GETINFOS_VALUEDATE      => 0x0100
};

# Constantes: Récupération d'informations sur les lignes de devis
# GETLINEINFOS_ALL                 - Récupération de l'ensemble des informations
# GETLINEINFOS_BASE                - Récupération des informations de base
# GETLINEINFOS_EXT_MODEL           - Récupération des informations complètes sur le modèle de machine
# GETLINEINFOS_MODEL               - Récupération des informations de base du modèle de machine
# GETLINEINFOS_EXT_TARIFFTRSP      - Récupération des informations complètes de la tarification transport
# GETLINEINFOS_TARIFFTRSP          - Récupération des informations de base de la tarification transport
# GETLINEINFOS_EXT_TARIFFRENT      - Récupération des informations complètes de la tarification location
# GETLINEINFOS_TARIFFRENT          - Récupération des informations de base de la tarification location
# GETLINEINFOS_INSURANCE           - Récupération des informations sur l'assurance
# GETLINEINFOS_CONVERTIBLE         - Récupération des informations sur les possibilités du passage en contrat
# GETLINEINFOS_INDEX               - Récupération des informations sur les index
# GETLINEINFOS_LINKEDCONTRACT      - Récupération des informations sur le contrat lié
# GETLINEINFOS_RIGHTS              - Récupération des informations sur les droits
# GETLINEINFOS_DURATIONS           - Récupération des informations sur les durées
# GETLINEINFOS_CLEANING            - Récupération des informations sur le nettoyage
# GETLINEINFOS_RESIDUES            - Récupération des informations sur la participation au recyclage
# GETLINEINFOS_FUEL                - Récupération des informations sur le carburant
# GETLINEINFOS_POSSIBILITIES       - Récupération des informations sur les possibilités
# GETLINEINFOS_EXT_INSURANCETYPE   - Récupération des informations sur le type d'assurance
# GETLINEINFOS_APPEAL              - Récupération des informations sur la gestion de recours
# GETLINEINFOS_ESTIMATE            - Récupération des informations du devis
# GETLINEINFOS_ESTIMATE_CUSTOMER   - Récupération des informations du client
# GETLINEINFOS_ESTIMATE_SITE       - Récupération des informations du chantier
# GETLINEINFOS_ESTIMATE_CREATOR    - Récupération des informations du créateur
# GETLINEINFOS_ESTIMATE_VALUEDATE  - Récupération des informations de la date de valeur du devis
# GETLINEINFOS_ESTIMATE_NEGOCIATOR - Récupération des informations du négociateur
use constant {
    GETLINEINFOS_ALL               => 0xFFFFFFFF,
    GETLINEINFOS_BASE              => 0x00000000,
    GETLINEINFOS_EXT_MODEL         => 0x00000001,
    GETLINEINFOS_MODEL             => 0x00000002,
    GETLINEINFOS_EXT_TARIFFTRSP    => 0x00000004,
    GETLINEINFOS_TARIFFTRSP        => 0x00000008,
    GETLINEINFOS_EXT_TARIFFRENT    => 0x00000010,
    GETLINEINFOS_TARIFFRENT        => 0x00000020,
    GETLINEINFOS_INSURANCE         => 0x00000040,
    GETLINEINFOS_CONVERTIBLE       => 0x00000080,
    GETLINEINFOS_INDEX             => 0x00000100,
    GETLINEINFOS_LINKEDCONTRACT    => 0x00000200,
    GETLINEINFOS_RIGHTS            => 0x00000400,
    GETLINEINFOS_DURATIONS         => 0x00000800,
    GETLINEINFOS_CLEANING          => 0x00001000,
    GETLINEINFOS_RESIDUES          => 0x00002000,
    GETLINEINFOS_FUEL              => 0x00004000,
    GETLINEINFOS_EXT_RENTSERVICES  => 0x00008000,
    GETLINEINFOS_POSSIBILITIES     => 0x01000000, 
    GETLINEINFOS_EXT_INSURANCETYPE => 0x02000000,
    GETLINEINFOS_APPEAL            => 0x04000000,
    GETLINEINFOS_EXT_APPEALTYPE    => 0x08000000,

    GETLINEINFOS_ESTIMATE            => 0x00010000,
    GETLINEINFOS_ESTIMATE_CUSTOMER   => 0x00030000, # 0x010000 + 0x020000
    GETLINEINFOS_ESTIMATE_SITE       => 0x00050000, # 0x010000 + 0x040000
    GETLINEINFOS_ESTIMATE_CREATOR    => 0x00090000, # 0x010000 + 0x080000
    GETLINEINFOS_ESTIMATE_VALUEDATE  => 0x00110000, # 0x010000 + 0x100000
    GETLINEINFOS_ESTIMATE_NEGOCIATOR => 0x00210000 # 0x010000 + 0x200000
};

# Constantes: Divers
# MAXLINESCOUNT   - Nombre de lignes maximales sur le devis
use constant {
    MAXLINESCOUNT => 4
};

# Constants: Erreurs
# LINE_ERROR_FATAL             - Erreur fatale
# LINE_ERROR_WRITELOCKED       - Impossible de modifier le contrat
# LINE_ERROR_RIGHTS            - Impossible de modifier cette information du contrat
# LINE_ERROR_MODULE_TARIFFRENT - Erreur sur l'enregistrement du tarif de location
# LINE_ERROR_MODULE_TARIFFTRSP - Erreur sur l'enregistrement du tarif transport
# LINE_ERROR_MODULE_PROFORMA   - Erreur sur l'enregistrement des pro formas
# LINE_ERROR_MODULE_TRANSPORT  - Erreur sur l'enregistrement du transport
# LINE_ERROR_MODULE_DAYS       - Erreur sur l'enregistrement des jours +/-
# LINE_ERROR_MODULE_DOCUMENTS  - Erreur sur l'enregistrement des documents
use constant
{
    LINE_ERROR_FATAL             => 0x0001,
    LINE_ERROR_WRITELOCKED       => 0x0002,
    LINE_ERROR_RIGHTS            => 0x0003,

    LINE_ERROR_MODULE_TARIFFRENT => 0x1010,
    LINE_ERROR_MODULE_TARIFFTRSP => 0x1020,
    LINE_ERROR_MODULE_PROFORMA   => 0x1030,
    LINE_ERROR_MODULE_TRANSPORT  => 0x1040,
    LINE_ERROR_MODULE_DAYS       => 0x1060,
    LINE_ERROR_MODULE_DOCUMENTS  => 0x1070
};

use strict;


# Liste des erreurs:
#
# 0x0001 => Erreur fatale
# 0x0003 => Impossible de modifier cette information du devis
#
# 0x0010 => Négociateur non renseigné
# 0x0100 => Date de modification invalide
# 0x0102 => Agence invalide
# 0x0103 => Zone invalide
# 0x0104 => Client invalide
# 0x0105 => Client bloqué au devis
# 0x0106 => Commentaire d'abandon obligatoire
# 0x0107 => Impossible de modifier le devis
# 0x0108 => Impossible d'abandonner le devis
# 0x0127 => Email du contact commande non valide
# 0x0128 => Informations contact commande incomplètes
# 0x0110 => La zone ne peut pas être modifiée
# 0x0130 => Erreur sur l'enregistrement du chantier
# 0x0150 => Erreur sur l'enregistrement d'une ligne

# 0x1000 => Modèle de machine invalide
# 0x1001 => Date de modification invalide
# 0x1002 => Durée invalide
# 0x1003 => Une ligne passée en contrat ne peut être supprimée
# 0x1004 => Le nombre de jours ouvrés a changé
# 0x1005 => Les dates de début et de fin ne sont pas renseignées
# 0x1006 => Nombre de lignes max
# 0x1007 => La date de fin ne doit pas être inférieure à 1 mois
# 0x1008 => La date de fin est inférieure à la date de début
# 0x1009 => L'acceptation de la ligne nécessite un horaire de livraison
# 0x1010 => Erreur sur l'enregistrement du tarif de location
# 0x1020 => Erreur sur l'enregistrement du tarif transport
# 0x1030 => Erreur sur l'enregistrement de la proforma
# 0x1040 => Erreur sur l'enregistrement du transport
# 0x1050 => Le type d'assurance n'est pas modifiable
# 0x1051 => Le taux d'assurance n'est pas modifiable
# 0x1052 => Le mode de calcul de la participation au recyclage n'est pas disponible
# 0x1053 => Le mode de calcul de la gestion de recours n'est pas disponible
# 0x1060 => Erreur sur l'enregistrement des jours +/-
# 0x1070 => Erreur sur l'enregistrement des documents


use LOC::Db;
use LOC::Util;
use LOC::Country;
use LOC::Estimate;
use LOC::Site;
use LOC::Model;
use LOC::Tariff::Rent;
use LOC::Tariff::Transport;
use LOC::Customer;
use LOC::User;
use LOC::Transport;
use LOC::Proforma;
use LOC::Common::Rent;
use LOC::EndUpdates;
use LOC::Attachments;
use LOC::Day;
use LOC::Insurance::Type;


# Function: checkIfErrors
# Récupérer la liste des lignes en erreur
#
# Parameters:
# string  $countryId  - Pays
# string  $estimateId - Id du devis
#
# Returns:
# int|undef -
sub getErroneousLines
{
    my ($countryId, $estimateId) = @_;

    return &LOC::Tariff::Transport::checkIfErrors($countryId, 'estimate', $estimateId);
}


# Function: isPrintable
# Indique si le devis est imprimable ou non
#
# Parameters:
# string $countryId        - Pays
# int    $estimateId       - Id du devis
# int    $nbErroneousLines - Nombre de ligne en erreurs (optimisation)
#
# Returns:
# bool -
sub isPrintable
{
    my ($countryId, $estimateId, $nbErroneousLines) = @_;

    if (!defined $nbErroneousLines)
    {
        my $tabErroneousLines = &getErroneousLines($countryId, $estimateId);
        $nbErroneousLines = @$tabErroneousLines;
    }
    if ($nbErroneousLines > 0)
    {
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $tariffRentSchemaName = 'GESTIONTARIF' . $countryId;
    my $tariffTrspSchemaName = 'GESTIONTRANS' . $countryId;

    # Si au moins une des estimations est en attente, alors il est impossible d'imprimer le devis
    my $query = '
SELECT
    IF (tbl_f_estimatedetail.DETAILSDEVISREMISEEX > 0,
        ELT(1-tbl_f_estimatedetail.DETAILSDEVISREMISEOK, "' . LOC::Tariff::Rent::REDUCTIONSTATE_REFUSED . '", "' . LOC::Tariff::Rent::REDUCTIONSTATE_PENDING . '", "' . LOC::Tariff::Rent::REDUCTIONSTATE_VALIDATED . '"),
        IF (tbl_f_rentestim2.VALIDOK = 0, "' . LOC::Tariff::Rent::REDUCTIONSTATE_REFUSED . '", "")) AS `rentTariff.specialReductionStatus`,
    IFNULL(tbl_f_trspspcreduc.ETAT, "") AS `transportTariff.specialReductionStatus`,
    tbl_f_estimate.ETCODE AS `estimate.state.id`
FROM DEVIS tbl_f_estimate
INNER JOIN DETAILSDEVIS tbl_f_estimatedetail
ON tbl_f_estimatedetail.DEVISAUTO = tbl_f_estimate.DEVISAUTO
LEFT JOIN ' . $tariffRentSchemaName . '.GC_ARCHIVES tbl_f_rentestim2
ON tbl_f_rentestim2.DETAILSDEVISAUTO = tbl_f_estimatedetail.DETAILSDEVISAUTO
LEFT JOIN ' . $tariffTrspSchemaName . '.TRANSPORT tbl_f_trspestim
ON tbl_f_trspestim.TYPE = "DEVIS" AND
   tbl_f_trspestim.VALEUR = tbl_f_estimatedetail.DETAILSDEVISAUTO AND
   tbl_f_trspestim.ETAT <> "' . LOC::Tariff::Transport::REDUCTIONSTATE_CANCELED . '"
LEFT JOIN ' . $tariffTrspSchemaName . '.REMISEEX tbl_f_trspspcreduc
ON tbl_f_trspspcreduc.ID = tbl_f_trspestim.REMISEEX
WHERE tbl_f_estimatedetail.DEVISAUTO = ' . $estimateId . '
AND tbl_f_estimatedetail.DETAILSDEVISOK <> -2;';
    my $stmt = $db->query($query);
    if ($stmt->getRowsCount() == 0)
    {
        $stmt->close();
        return 0;
    }
    while (my $tabRow = $stmt->fetch(LOC::Db::FETCH_ASSOC))
    {
        if ($tabRow->{'estimate.state.id'} eq STATE_ABORTED ||
            $tabRow->{'rentTariff.specialReductionStatus'} eq LOC::Tariff::Rent::REDUCTIONSTATE_PENDING ||
            $tabRow->{'transportTariff.specialReductionStatus'} eq LOC::Tariff::Transport::REDUCTIONSTATE_PENDING)
        {
            $stmt->close();
            return 0;
        }
    }
    $stmt->close();

    return 1;
}


# Function: generateCode
# Générer le code d'un devis
#
# Parameters:
# int    $estimateId    - Id du devis
# string $agencyId      - Id de l'agence du devis
# bool   $isFullService - Full Service ?
# bool   $isSQL         - Format SQL ?
#
# Returns:
# string -
sub generateCode
{
    my ($estimateId, $agencyId, $isFullService, $isSQL) = @_;

    my $code;
    if ($isSQL)
    {
        $code = 'CONCAT("DEV-COM-", ' . $agencyId . ', ' . $estimateId . ', IF(' . $isFullService . ', "FS", ""))';
    }
    else
    {
        $code = 'DEV-COM-' . $agencyId . $estimateId . ($isFullService ? 'FS' : '');
    }
    return $code;
}


# Function: getHistory
# Récupérer l'historique du devis
#
# Contenu d'une ligne du hashage :
# - id : identifiant de la ligne d'historique ;
# - eventType.code : code du type d'événement ;
# - eventType.label : libellé du type d'événement ;
# - user.id : identifiant de l'utilisateur ;
# - user.name : nom de l'utilisateur ;
# - user.firstName : prénom de l'utilisateur ;
# - user.fullName : nom complet de l'utilisateur ;
# - user.state.id : identifiant de l'état de l'utilisateur ;
# - datetime : date et heure de la trace ;
# - content : contenu de la trace.
#
# Parameters:
# string  $countryId  - Pays
# int     $estimateId - Identifiant du devis
# string  $localeId   - Identifiant de la locale pour les traductions
#
# Returns:
# array|arrayref - Historique du devis
sub getHistory
{
    my ($countryId, $estimateId, $localeId) = @_;

    return &LOC::Log::getFunctionalityHistory($countryId, 'estimate', $estimateId, $localeId);
}


# Function: getLineHistory
# Récupérer l'historique d'une ligne de devis
#
# Contenu d'une ligne du hashage :
# - id : identifiant de la ligne d'historique ;
# - eventType.code : code du type d'événement ;
# - eventType.label : libellé du type d'événement ;
# - user.id : identifiant de l'utilisateur ;
# - user.name : nom de l'utilisateur ;
# - user.firstName : prénom de l'utilisateur ;
# - user.fullName : nom complet de l'utilisateur ;
# - user.state.id : identifiant de l'état de l'utilisateur ;
# - datetime : date et heure de la trace ;
# - content : contenu de la trace.
#
# Parameters:
# string  $countryId      - Pays
# int     $estimateLineId - Identifiant de la ligne de devis
# string  $localeId       - Identifiant de la locale pour les traductions
#
# Returns:
# array|arrayref - Historique de la ligne de devis
sub getLineHistory
{
    my ($countryId, $estimateLineId, $localeId) = @_;

    return &LOC::Log::getFunctionalityHistory($countryId, 'estimateLine', $estimateLineId, $localeId);
}

# Function: getLineRentServices
# Récupérer la liste des services de location et transport d'une ligne de devis
#
# Contenu du hachage:
# - id          => id
# - currency.id => id de la devise
# - name        => libellé
# - code        => code
# - comment     => commentaire
# - type        => type d'imputation
# - amount      => montant
# - state.id    => code de l'état

#
# Parameters:
# string  $countryId  - Pays
# int     $contractId - id de la ligne de devis
# hashref $tabFilters - Liste des filtres (state (état))
#
# Returns:
# hash|hashref
sub getLineRentServices
{
    my ($countryId, $estimateLineId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT
    res_id AS `id`,
    rsr_id AS `rentService.id`,
    res_sta_id AS `state.id`,
    ETLIBELLE AS `state.label`,
    rsr_name AS `rentService.name`,
    res_comment AS `comment`,
    rsr_code AS `rentService.code`,
    rsr_type AS `rentService.type`,
    res_amount AS `amount`,
    res_cur_id AS `currency.id`
FROM f_rentestimateline_rentservice
INNER JOIN p_rentservice
ON rsr_id = res_rsr_id
LEFT JOIN ETATTABLE p_state
ON p_state.ETCODE = res_sta_id
WHERE res_rel_id = ' . $estimateLineId . '
ORDER BY res_id;';

    my $tab = $db->fetchAssoc($query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    foreach my $tabRow (values %$tab)
    {
        $tabRow->{'id'} *= 1;
        $tabRow->{'rentService.id'} *= 1;
        $tabRow->{'amount'} *= 1;
        $tabRow->{'isRemovable'} = ($tabRow->{'state.id'} ne LOC::Common::RentService::STATE_BILLED ? 1 : 0);
    }

    return (wantarray ? %$tab : $tab);
}


# Function: getIdByLineId
# Récupérer l'identifiant d'un devis par rapport à l'id d'une ligne de devis
#
# Parameters:
# string $countryId - Pays
# int    $lineId    - Identifiant d'une ligne de devis
#
# Returns:
# int - Id du devis
sub getIdByLineId
{
    my ($countryId, $lineId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT
    tbl_f_estimatedetail.DEVISAUTO
FROM DETAILSDEVIS tbl_f_estimatedetail
WHERE tbl_f_estimatedetail.DETAILSDEVISAUTO = ' . $lineId . ';';

    return $db->fetchOne($query);
}

# Function: getLineAgencyId
# Récupérer l'agence de la ligne de devis
#
# Parameters:
# string  $countryId      - Pays
# int     $estimateLineId - Identifiant de la ligne de devis
#
# Returns:
# string - Identifiant de l'agence
sub getLineAgencyId
{
    my ($countryId, $estimateLineId) = @_;

    # Récupèration de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # La date de valeur du contrat est sa date de création par défaut
    my $query = '
SELECT
    tbl_f_estimate.AGAUTO
FROM DETAILSDEVIS tbl_f_estimateline
INNER JOIN DEVIS tbl_f_estimate ON tbl_f_estimate.DEVISAUTO = tbl_f_estimateline.DEVISAUTO
WHERE tbl_f_estimateline.DETAILSDEVISAUTO = ' . $estimateLineId . ';';

    return $db->fetchOne($query);
}


# Function: getInfos
# Retourne un hachage contenant les infos du devis
#
# Contenu du hashage :
# - id => Identifiant du devis
# - code  => Code du devis
# - tracking => Commentaires de suivi
# - comments => Commentaires
# - email => E-mail du devis
# - orderNo => Numéro de commande
# - agency.id => Identifiant de l'agence du devis
# - country.id => Pays du devis
# - customer.id => Identifiant du client associé
# - customer.code => Code du client (si $flags & GETINFOS_CUSTOMER)
# - customer.name => Raison sociale du client (si $flags & GETINFOS_CUSTOMER)
# - customer.siret => Siret du client (si $flags & GETINFOS_CUSTOMER)
# - customer.controlAccount => Compte général du client (si $flags & GETINFOS_CUSTOMER)
# - site.id => Identifiant du chantier associé
# - zone => Zone
# - isFullService => Indique si c'est un devis FullService
# - abortReason.id => Identifiant du motif d'abandon
# - abortComments => Commentaires d'abandon
# - tabLines => Identifiant des lignes du devis
# - nbConvertedLines => Nombre de lignes converties
# - state.id => Identifiant de l'état du devis
# - state.label => Libellé de l'état du devis
# - creator.id => Id du créateur du devis
# - creator.firstName => Prénom du créateur (si $flags & GETINFOS_CREATOR)
# - creator.name => Nom du créateur (si $flags & GETINFOS_CREATOR)
# - creator.fullName => Nom complet du créateur (si $flags & GETINFOS_CREATOR)
# - amount => Montant total du devis
# - modificationDate => Date de modification
# - creationDate => Date de création
#
#
# Parameters:
# string  $countryId  - Pays
# int     $id         - Id du devis
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hash|hashref|undef
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);
    return ($tab && exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}


# Function: getLineInfos
# Retourne un hachage contenant les infos d'une ligne d'un devis
#
# Contenu d'une ligne du hashage : voir la méthode getLinesList
#
# Parameters:
# string  $countryId  - Pays
# int     $id         - Id de la ligne du devis
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hash|hashref|undef
sub getLineInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETLINEINFOS_BASE;
    }

    my $tab = &getLinesList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}


# Function: getLinesList
# Récupérer la liste des lignes de devis
#
# Contenu du hashage :
# - id                                       => Identifiant de la ligne du devis
# - estimate.id                              => Identifiant du devis
# - estimate.state.id                        => Identifiant de l'état du devis
# - estimate.agency.id                       => Identifiant de l'agence du devis
# - estimate.modificationDate                => Date de dernière modification du devis
# - model.id                                 => Identifiant du modèle de machine
# - isImperativeModel                        => modèle impératif (1) ou non (0) ;
# - quantity                                 => Quantité de la ligne de devis
# - beginDate                                => Date de début de location
# - endDate                                  => Date de fin  de location
# - additionalDays                           => Nombre de jours complémentaires supplémentaires
# - deductedDays                             => Nombre de jours complémentaires en moins
# - duration                                 => Durée de la location
# - amountType                               => Indique le type de montant (day: jours, month: mois)
# - rentAmount                               => Prix de la location par jour en monnaie locale
# - rentTotal                                => Montant total de la location de la ligne de devis
# - total                                    => Montant total de la ligne de devis
# - state.id                                 => Etat de la ligne de devis
# - proformaFlags                            => Drapeaux pro forma  (Bit 0: Y a-t-il des proformas actives ?,
#                                                                    Bit 1: Y a-t-il au moins une proforma active finalisée ?,
#                                                                    Bit 2; Toutes les proformas actives sont-elles finalisées ?,
#                                                                    Bit 3: Il existe au moins une proforma active expirée)
# - isAccepted                               => Indique si la ligne est acceptée (1: oui, 0: non)
# - isFollowed                               => Indique si la ligne est à suivre (1: oui, 0: non)
# - isDatesToConfirm                         => Indique si les dates sont à confirmer (1: oui, 0: non)
# - creationDate                             => Date de création de la ligne
# - modificationDate                         => Date de dernière modification de la ligne
# - estimate.code                            => Code du devis associé
# - estimate.country.id                      => Identifiant du pays du devis
# - estimate.orderNo                         => N° de commande du devis
# - estimate.zone                            => Zone de transport du devis
# - estimate.customer.id                     => Identifiant du client du devis
# - estimate.site.id                         => Identifiant du chantier du devis
# - estimate.creator.id                      => Identifiant du créateur du devis
# - estimate.creator.firstName               => Prénom du créateur du devis
# - estimate.creator.name                    => Nom du créateur du devis
# - estimate.creator.fullName                => Nom complet du créateur du devis
# - estimate.negotiator.firstName            => Prénom du négociateur du devis
# - estimate.negotiator.name                 => Nom du négociateur du devis
# - estimate.negotiator.fullName             => Nom complet du négociateur du devis
# - estimate.customer.code                   => Code du client du devis
# - estimate.customer.name                   => Raison sociale du client du devis
# - estimate.customer.telephone              => Téléphone du client du devis
# - estimate.customer.locality.id            => Identifiant de la ville du client
# - estimate.customer.locality.name          => Libellé de la ville du client
# - estimate.customer.locality.postalCode    => Code postal de la ville du client
# - estimate.customer.locality.label         => Concaténation du code postal et du libellé de la ville du client
# - estimate.customer.email                  => Email du client
# - estimate.customer.isProformaRequired     => Indique si la pro forma est obligatoire (1: oui, 0: non)
# - estimate.customer.accountingCode         => Code collectif du client
# - estimate.site.locality.id                => Identifiant de la ville du chantier
# - estimate.site.locality.name              => Libellé de la ville du chantier
# - estimate.site.locality.postalCode        => Code postal de la ville du chantier
# - estimate.site.locality.label             => Concaténation du code postal et du libellé de la ville du chantier
# - estimate.site.label                      => Libellé du chantier
# - estimate.site.address                    => Adresse du chantier
# - estimate.site.isAnticipated              => Indique si la livraison est anticipée (1: oui, 0: non)
# - estimate.site.deliveryHour               => Heure de livraison
# - estimate.site.deliveryTimeSlot.id        => Identifiant de la plage horaire de livraison
# - estimate.site.recoveryHour               => Heure de récupération
# - estimate.site.recoveryTimeSlot.id        => Identifiant de la plage horaire de récupération
# - estimate.site.comments                   => Commentaires du chantier
# - index                                    => Index
# - linkedContract.infos                     => Informations du contrat lié (identifiant et code du contrat)
# - model.label                              => Désignation du modèle
# - insuranceType.id                           => Type d'assurance
# - insurance.rate                           => Taux d'assurance
# - insurance.amount                         => Montant de l'assurance
# - cleaning.type.id                         => Identifiant du type de nettoyage
# - cleaning.amount                          => Montant du nettoyage en monnaie locale
# - residues.mode                            => Indique le mode de calcul de la participation au recyclage (0: non facturée, 1: forfait, 2: pourcentage)
# - residues.value                           => Valeur de la participation au recyclage pour le calcul (montant forfaitaire, pourcentage)
# - residues.amount                          => Montant de la participation au recyclage en monnaie locale
# - fuel.quantity                            => Estimation quantité de carburant
# - fuel.amount                              => Montant du carburant en monnaie locale
# - fuel.unitPrice                           => Prix au litre du carburant en monnaie locale
# - rentTariff.amountType                    => Type de montant (day, month)
# - rentTariff.baseAmount                    => Tarif de base à l'enregistrement en monnaie locale
# - rentTariff.standardReductionPercent      => Pourcentage de remise standard
# - rentTariff.maxiReductionPercent          => Pourcentage max pour la remise standard
# - rentTariff.specialReductionPercent       => Pourcentage de la remise exceptionnelle
# - rentTariff.specialReductionStatus        => Identifiant de l'état de la remise exceptionnelle
# - transportTariff.baseAmount               => Tarif de base du transport
# - transportTariff.standardReductionPercent => Pourcentage de la remise standard du transport
# - transportTariff.zone                     => Zone du transport
# - transportTariff.customer.id              => Client lié au transport
# - transportTariff.specialReductionPercent  => Pourcentage de la remise exceptionnelle du transport
# - transportTariff.specialReductionStatus   => Identifiant de l'état de la remise exceptionnelle du transport
# - transportTariff.deliveryAmount           => Montant de la livraison
# - transportTariff.isNoDelivery             => Indique si la livraison est un enlèvement (1: oui, 0: non)
# - transportTariff.isAddMachineDelivery     => Indique s'il s'agit d'une machine supplémentaire pour la livraison (1: oui, 0: non)
# - transportTariff.recoveryAmount           => Montant de la récupération
# - transportTariff.isNoRecovery             => Indique si la récupération est un enlèvement (1: oui, 0: non)
# - transportTariff.isAddMachineRecovery     => Indique s'il s'agit d'une machine supplémentaire pour la récupération (1: oui, 0: non)
# - transportTariff.isNoSaved                => Indique si la remise exceptionnelle est en attente (1: oui, 0: non)
#
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hash|hashref|undef
sub getLinesList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETLINEINFOS_BASE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $tariffRentSchemaName = 'GESTIONTARIF' . $countryId;
    my $tariffTrspSchemaName = 'GESTIONTRANS' . $countryId;

    my $stateQuery = 'ELT(ABS(tbl_f_estimatedetail.DETAILSDEVISOK) + 1, "' . LINE_STATE_ACTIVE . '", "' . LINE_STATE_CONVERTED . '", "' . LINE_STATE_INACTIVE . '")';

    my $query;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        # Pour savoir si il est possible de passer la ligne en contrat, il faut les informations sur les
        # estimations de location et transport
        if ($flags & GETLINEINFOS_CONVERTIBLE)
        {
            $flags = $flags | GETLINEINFOS_ESTIMATE;
            $flags = $flags | GETLINEINFOS_ESTIMATE_CUSTOMER;
            if (!($flags & GETLINEINFOS_EXT_TARIFFRENT))
            {
                $flags = $flags | GETLINEINFOS_TARIFFRENT;
            }
            if (!($flags & GETLINEINFOS_EXT_TARIFFTRSP))
            {
                $flags = $flags | GETLINEINFOS_EXT_TARIFFTRSP;
                $tabOptions->{'transportTariff'} = {'flags' => LOC::Tariff::Transport::GETINFOS_BASE};
            }
        }
        if ($flags & GETLINEINFOS_RIGHTS)
        {
            $flags = $flags | GETLINEINFOS_ESTIMATE;
        }
        if (($flags & GETLINEINFOS_EXT_INSURANCETYPE) && !($flags & GETLINEINFOS_INSURANCE))
        {
            $flags = $flags | GETLINEINFOS_INSURANCE;
        }
        if (($flags & GETLINEINFOS_EXT_APPEALTYPE) && !($flags & GETLINEINFOS_APPEAL))
        {
            $flags = $flags | GETLINEINFOS_APPEAL;
        }

        $query = '
SELECT
    tbl_f_estimatedetail.DETAILSDEVISAUTO AS `id`,
    tbl_f_estimatedetail.DEVISAUTO AS `estimate.id`,
    tbl_f_estimate.ETCODE AS `estimate.state.id`,
    tbl_f_estimate.AGAUTO AS `estimate.agency.id`,
    tbl_f_estimate.DEVISDATE AS `estimate.modificationDate`,';
        if ($flags & GETLINEINFOS_ESTIMATE)
        {
            $query .= '
    ' . generateCode('tbl_f_estimate.DEVISAUTO', 'tbl_f_estimate.AGAUTO',
                     '-tbl_f_estimate.DEVISFULLSERVICE', 1) . ' AS `estimate.code`,
    "' . $countryId . '" AS `estimate.country.id`,
    tbl_p_estimatestate.ETLIBELLE AS `estimate.state.label`,
    tbl_f_estimate.DEVISNUMCOM AS `estimate.orderNo`,
    tbl_f_estimate.DEVISCONTACTCOM AS `estimate.orderContact.fullName`,
    tbl_f_estimate.DEVISTELEPHONECOM AS `estimate.orderContact.telephone`,
    tbl_f_estimate.DEVISEMAILCOM AS `estimate.orderContact.email`,
    tbl_f_estimate.ZONE AS `estimate.zone`,
    tbl_f_estimate.CLAUTO AS `estimate.customer.id`,';

            if (($flags & GETLINEINFOS_ESTIMATE_CREATOR) == GETLINEINFOS_ESTIMATE_CREATOR)
            {
                $query .= '
    f_user_creator.usr_id AS `estimate.creator.id`,
    f_user_creator.usr_firstname AS `estimate.creator.firstName`,
    f_user_creator.usr_name AS `estimate.creator.name`,
    CONCAT_WS(" ", f_user_creator.usr_firstname, f_user_creator.usr_name) AS `estimate.creator.fullName`,';
            }

            if (($flags & GETLINEINFOS_ESTIMATE_NEGOCIATOR) == GETLINEINFOS_ESTIMATE_NEGOCIATOR)
            {
                $query .= '
    f_user_negotiator.usr_firstname AS `estimate.negotiator.firstName`,
    f_user_negotiator.usr_name AS `estimate.negotiator.name`,
    CONCAT_WS(" ", f_user_negotiator.usr_firstname, f_user_negotiator.usr_name) AS `estimate.negotiator.fullName`,';
            }

            if (($flags & GETLINEINFOS_ESTIMATE_CUSTOMER) == GETLINEINFOS_ESTIMATE_CUSTOMER)
            {
                $query .= '
    tbl_f_customer.CLCODE AS `estimate.customer.code`,
    tbl_f_customer.CLSTE AS `estimate.customer.name`,
    tbl_f_customer.CLTEL AS `estimate.customer.telephone`,
    tbl_f_customer.VICLAUTO AS `estimate.customer.locality.id`,
    tbl_p_locality.VICLNOM AS `estimate.customer.locality.name`,
    tbl_p_locality.VICLCP AS `estimate.customer.locality.postalCode`,
    CONCAT (tbl_p_locality.VICLCP, " ", tbl_p_locality.VICLNOM) AS `estimate.customer.locality.label`,
    tbl_f_customer.CLEMAIL AS `estimate.customer.email`,
    tbl_f_customer.CLPROFORMA AS `estimate.customer.isProformaRequired`,
    tbl_f_customer.CLCOLLECTIF AS `estimate.customer.accountingCode`,';
            }

            $query .= '
    tbl_f_estimate.CHAUTO AS `estimate.site.id`,';

            if (($flags & GETLINEINFOS_ESTIMATE_SITE) == GETLINEINFOS_ESTIMATE_SITE)
            {
                $query .= '
    tbl_f_site.VILLEAUTO AS `estimate.site.locality.id`,
    tbl_p_sitelocality.VINOM AS `estimate.site.locality.name`,
    tbl_p_sitelocality.VICP AS `estimate.site.locality.postalCode`,
    CONCAT (tbl_p_sitelocality.VICP, " ", tbl_p_sitelocality.VINOM) AS `estimate.site.locality.label`,
    tbl_f_site.CHLIBELLE AS `estimate.site.label`,
    tbl_f_site.CHADRESSE AS `estimate.site.address`,
    tbl_f_site.CHANTICIPATION AS `estimate.site.isAnticipated`,
    tbl_f_site.CHHEUREDP AS `estimate.site.deliveryHour`,
    tbl_f_site.CHPLAGEDP AS `estimate.site.deliveryTimeSlot.id`,
    tbl_f_site.CHHEUREAR AS `estimate.site.recoveryHour`,
    tbl_f_site.CHPLAGEAR AS `estimate.site.recoveryTimeSlot.id`,
    tbl_f_site.CHCOMMENTAIRE AS `estimate.site.comments`,';
            }
        }
        if ($flags & GETLINEINFOS_INDEX)
        {
            $query .= '
    (SELECT COUNT(tbl_f_estimatedetail2.DETAILSDEVISAUTO) + 1
     FROM DETAILSDEVIS tbl_f_estimatedetail2
     WHERE tbl_f_estimatedetail2.DEVISAUTO = tbl_f_estimatedetail.DEVISAUTO
     AND tbl_f_estimatedetail2.DETAILSDEVISAUTO < tbl_f_estimatedetail.DETAILSDEVISAUTO) AS `index`,';
        }
        if ($flags & GETLINEINFOS_LINKEDCONTRACT)
        {
            $query .= '
    IF (tbl_f_estimatedetail.DETAILSDEVISOK <> -1, "0;",
        (SELECT IF(tbl_f_contract.CONTRATAUTO IS NULL, "0;", CONCAT(tbl_f_contract.CONTRATAUTO, ";", tbl_f_contract.CONTRATCODE))
         FROM CONTRAT tbl_f_contract
         WHERE tbl_f_contract.DETAILSDEVISAUTO = tbl_f_estimatedetail.DETAILSDEVISAUTO
         LIMIT 1)) AS `linkedContract.infos`,';
        }
        $query .= '
    tbl_f_estimatedetail.MOMAAUTO AS `model.id`,';
        if ($flags & GETLINEINFOS_MODEL)
        {
            $query .= '
    tbl_f_model.MOMADESIGNATION AS `model.label`,';
        }
        $query .= '
    -tbl_f_estimatedetail.DETAILSDEVISMODIMP AS `isImperativeModel`,
    tbl_f_estimatedetail.DETAILSDEVISQTE AS `quantity`,
    tbl_f_estimatedetail.DETAILSDEVISDATE AS `beginDate`,
    tbl_f_estimatedetail.DETAILSDEVISDATEFIN AS `endDate`,
    tbl_f_estimatedetail.DETAILSDEVISJOURSPLUS AS `additionalDays`,
    tbl_f_estimatedetail.DETAILSDEVISJOURSMOINS AS `deductedDays`,
    tbl_f_estimatedetail.DETAILSDEVISDUREE AS `duration`,
    IF (tbl_f_estimatedetail.DETAILSDEVISTYPEMONTANT = 1, "day", "month") AS `amountType`,
    tbl_f_estimatedetail.DETAILSDEVISPXJOURSFR AS `rentAmount`,
    tbl_f_estimatedetail.DETAILSDEVISMONTANTFR AS `rentTotal`,';
        if ($flags & GETLINEINFOS_INSURANCE)
        {
            $query .= '
    tbl_f_estimatedetail.DETAILSDEVISTYPEASSURANCE AS `insuranceType.id`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISINVOICED) . ', tbl_f_estimatedetail.DETAILSDEVISASSTAUX, 0) AS `insurance.rate`,
    tbl_f_estimatedetail.DETAILSDEVISASSMONTANT AS `insurance.amount`,
    p_insurancetype.ity_flags AS `insurance.flags`,
    p_insurancetype.ity_mode AS `insurance.mode`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISINVOICED) . ', 1, 0) AS `insurance.isInvoiced`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISINCLUDED) . ', 1, 0) AS `insurance.isIncluded`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISCUSTOMER) . ', 1, 0) AS `insurance.isCustomer`,';
        }
        if ($flags & GETLINEINFOS_APPEAL)
        {
            $query .= '
    tbl_f_estimatedetail.DETAILSDEVISTYPERECOURS AS `appealType.id`,
    tbl_f_estimatedetail.DETAILSDEVISRECOURSVAL AS `appeal.value`,
    tbl_f_estimatedetail.DETAILSDEVISRECOURSMONTANT AS `appeal.amount`,';
        }
        if ($flags & GETLINEINFOS_CLEANING)
        {
            $query .= '
    IFNULL(tbl_f_estimatedetail.DETAILSDEVISNETAUTO, 0) AS `cleaning.type.id`,
    tbl_f_estimatedetail.DETAILSDEVISNETMONTANT AS `cleaning.amount`,';
        }
        if ($flags & GETLINEINFOS_RESIDUES)
        {
            $query .= '
    -tbl_f_estimatedetail.DETAILSDEVISDECHETSFACT AS `residues.mode`,
    tbl_f_estimatedetail.DETAILSDEVISDECHETSVAL AS `residues.value`,
    tbl_f_estimatedetail.DETAILSDEVISDECHETSMONTANT AS `residues.amount`,';
        }
        if ($flags & GETLINEINFOS_FUEL)
        {
            $query .= '
    tbl_f_estimatedetail.DETAILSDEVISCARBUQTE AS `fuel.quantity`,
    tbl_f_estimatedetail.DETAILSDEVISCARBUMONTANT AS `fuel.amount`,
    tbl_f_estimatedetail.DETAILSDEVISCARBUPXLITRE AS `fuel.unitPrice`,';
        }

        if ($flags & GETLINEINFOS_TARIFFRENT)
        {
            $query .= '
    IF (tbl_f_estimatedetail.DETAILSDEVISTYPEMONTANT = 1, "day", "month") AS `rentTariff.amountType`,
    tbl_f_estimatedetail.DETAILSDEVISTARIFBASE AS `rentTariff.baseAmount`,
    tbl_f_estimatedetail.DETAILSDEVISREMISE AS `rentTariff.standardReductionPercent`,
    tbl_f_estimatedetail.DETAILSDEVISREMISEMAX AS `rentTariff.maxiReductionPercent`,
    tbl_f_estimatedetail.DETAILSDEVISREMISEEX AS `rentTariff.specialReductionPercent`,
    IF (tbl_f_estimatedetail.DETAILSDEVISREMISEEX > 0,
        ELT(1-tbl_f_estimatedetail.DETAILSDEVISREMISEOK, "' . LOC::Tariff::Rent::REDUCTIONSTATE_REFUSED . '", "' . LOC::Tariff::Rent::REDUCTIONSTATE_PENDING . '", "' . LOC::Tariff::Rent::REDUCTIONSTATE_VALIDATED . '"),
        IF (tbl_f_rentestim2.VALIDOK = 0, "' . LOC::Tariff::Rent::REDUCTIONSTATE_REFUSED . '", "")) AS `rentTariff.specialReductionStatus`,';
        }
        if ($flags & GETLINEINFOS_TARIFFTRSP)
        {
            $query .= '
    tbl_f_trspestim.TARIFBASE AS `transportTariff.baseAmount`,
    tbl_f_trspestim.REMISEAPP AS `transportTariff.standardReductionPercent`,
    tbl_f_trspestim.ZONE AS `transportTariff.zone`,
    tbl_f_trspestim.CLIENT AS `transportTariff.customer.id`,
    IF (tbl_f_trspspcreduc.ID IS NULL, 0, IF (tbl_f_trspspcreduc.ETAT <> "' . LOC::Tariff::Transport::REDUCTIONSTATE_PENDING . '", tbl_f_trspspcreduc.REMEXACC, tbl_f_trspspcreduc.REMEXDEM)) AS `transportTariff.specialReductionPercent`,
    IFNULL(tbl_f_trspspcreduc.ETAT, "") AS `transportTariff.specialReductionStatus`,
    tbl_f_estimatedetail.DETAILSDEVISTRD AS `transportTariff.deliveryAmount`,
    tbl_f_estimatedetail.DETAILSDEVISENLALLER AS `transportTariff.isNoDelivery`,
    -tbl_f_estimatedetail.DETAILSDEVISMACHSUPALLER AS `transportTariff.isAddMachineDelivery`,
    tbl_f_estimatedetail.DETAILSDEVISTRR AS `transportTariff.recoveryAmount`,
    tbl_f_estimatedetail.DETAILSDEVISENLRETOUR AS `transportTariff.isNoRecovery`,
    -tbl_f_estimatedetail.DETAILSDEVISMACHSUPRETOUR AS `transportTariff.isAddMachineRecovery`,
    IF (tbl_f_trspestim.ETAT = "' . LOC::Tariff::Transport::REDUCTIONSTATE_PENDING . '", 1, 0) AS `transportTariff.isNoSaved`,';
        }
        $query .= '
    (tbl_f_estimatedetail.DETAILSDEVISMONTANTFR +
     tbl_f_estimatedetail.DETAILSDEVISTRD +
     tbl_f_estimatedetail.DETAILSDEVISTRR +
     tbl_f_estimatedetail.DETAILSDEVISASSMONTANT +
     tbl_f_estimatedetail.DETAILSDEVISRECOURSMONTANT +
     tbl_f_estimatedetail.DETAILSDEVISNETMONTANT +
     tbl_f_estimatedetail.DETAILSDEVISDECHETSMONTANT +
     tbl_f_estimatedetail.DETAILSDEVISCARBUMONTANT +
     (SELECT IFNULL(SUM(res_amount), 0)
        FROM f_rentestimateline_rentservice
        WHERE res_rel_id = tbl_f_estimatedetail.DETAILSDEVISAUTO
        AND res_sta_id <> "' . LOC::Common::RentService::STATE_DELETED . '")) AS `total`,
    ' . $stateQuery . ' AS `state.id`,
    tbl_f_estimatedetail.DETAILSDEVISPROFORMAOK AS `proformaFlags`,
    -tbl_f_estimatedetail.DETAILSDEVISACCEPTE AS `isAccepted`,
    -tbl_f_estimatedetail.DETAILSDEVISSUIVI AS `isFollowed`,
    -tbl_f_estimatedetail.DETAILSDEVISDATESACONFIRMER AS `isDatesToConfirm`,
    tbl_f_estimatedetail.DETAILSDEVISDATECREATION AS `creationDate`,
    tbl_f_estimatedetail.DETAILSDEVISDATEMODIF AS `modificationDate`
FROM DETAILSDEVIS tbl_f_estimatedetail
INNER JOIN DEVIS tbl_f_estimate
ON tbl_f_estimate.DEVISAUTO = tbl_f_estimatedetail.DEVISAUTO';
        if ($flags & GETLINEINFOS_MODEL)
        {
            $query .= '
INNER JOIN AUTH.MODELEMACHINE tbl_f_model
ON tbl_f_model.MOMAAUTO = tbl_f_estimatedetail.MOMAAUTO';
        }
        if ($flags & GETLINEINFOS_TARIFFRENT)
        {
            $query .= '
LEFT JOIN ' . $tariffRentSchemaName . '.GC_ARCHIVES tbl_f_rentestim2
ON tbl_f_rentestim2.DETAILSDEVISAUTO = tbl_f_estimatedetail.DETAILSDEVISAUTO';
        }
        if ($flags & GETLINEINFOS_TARIFFTRSP)
        {
            $query .= '
LEFT JOIN ' . $tariffTrspSchemaName . '.TRANSPORT tbl_f_trspestim
ON tbl_f_trspestim.TYPE = "DEVIS" AND
   tbl_f_trspestim.VALEUR = tbl_f_estimatedetail.DETAILSDEVISAUTO AND
   tbl_f_trspestim.ETAT <> "' . LOC::Tariff::Transport::REDUCTIONSTATE_CANCELED . '"
LEFT JOIN ' . $tariffTrspSchemaName . '.REMISEEX tbl_f_trspspcreduc
ON tbl_f_trspspcreduc.ID = tbl_f_trspestim.REMISEEX';
        }
        if ($flags & GETLINEINFOS_INSURANCE)
        {
            $query .= '
LEFT JOIN p_insurancetype
ON p_insurancetype.ity_id = tbl_f_estimatedetail.DETAILSDEVISTYPEASSURANCE';
        }
        if ($flags & GETLINEINFOS_ESTIMATE)
        {
            $query .= '
INNER JOIN ETATTABLE tbl_p_estimatestate
ON tbl_f_estimate.ETCODE = tbl_p_estimatestate.ETCODE';
            if (($flags & GETLINEINFOS_ESTIMATE_CREATOR) == GETLINEINFOS_ESTIMATE_CREATOR)
            {
                $query .= '
LEFT JOIN frmwrk.f_user f_user_creator
ON tbl_f_estimate.PELOGIN = f_user_creator.usr_login';
            }
            if (($flags & GETLINEINFOS_ESTIMATE_NEGOCIATOR) == GETLINEINFOS_ESTIMATE_NEGOCIATOR)
            {
                $query .= '
LEFT JOIN frmwrk.f_user f_user_negotiator
ON tbl_f_estimate.PEAUTO_INTERLOC = f_user_negotiator.usr_id';
            }
            if (($flags & GETLINEINFOS_ESTIMATE_CUSTOMER) == GETLINEINFOS_ESTIMATE_CUSTOMER)
            {
                $query .= '
INNER JOIN CLIENT tbl_f_customer
ON tbl_f_customer.CLAUTO = tbl_f_estimate.CLAUTO
LEFT JOIN AUTH.VICL tbl_p_locality
ON tbl_p_locality.VICLAUTO = tbl_f_customer.VICLAUTO';
            }
            if (($flags & GETLINEINFOS_ESTIMATE_SITE) == GETLINEINFOS_ESTIMATE_SITE)
            {
                $query .= '
INNER JOIN CHANTIER tbl_f_site
ON tbl_f_site.CHAUTO = tbl_f_estimate.CHAUTO
LEFT JOIN VILLECHA tbl_p_sitelocality
ON tbl_f_site.VILLEAUTO = tbl_p_sitelocality.VILLEAUTO';
            }
        }
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query .= '
SELECT
    COUNT(*)
FROM DETAILSDEVIS tbl_f_estimatedetail
INNER JOIN DEVIS tbl_f_estimate
ON tbl_f_estimate.DEVISAUTO = tbl_f_estimatedetail.DEVISAUTO';
    }
    else
    {
        $query .= '
SELECT
    tbl_f_estimatedetail.DETAILSDEVISAUTO,
    tbl_f_estimatedetail.DEVISAUTO
FROM DETAILSDEVIS tbl_f_estimatedetail
INNER JOIN DEVIS tbl_f_estimate
ON tbl_f_estimate.DEVISAUTO = tbl_f_estimatedetail.DEVISAUTO';
    }

    # Jointures pour les recherches
    if (defined $tabFilters->{'userId'} &&
        (($flags & GETLINEINFOS_ESTIMATE_CREATOR) != GETLINEINFOS_ESTIMATE_CREATOR))
    {
        $query .= '
LEFT JOIN frmwrk.f_user f_user_creator
ON tbl_f_estimate.PELOGIN = f_user_creator.usr_login';
    }

    if (defined $tabFilters->{'siteLabel'} &&
        (($flags & GETLINEINFOS_ESTIMATE_SITE) != GETLINEINFOS_ESTIMATE_SITE))
    {
        $query .= '
INNER JOIN CHANTIER tbl_f_site
ON tbl_f_site.CHAUTO = tbl_f_estimate.CHAUTO
LEFT JOIN VILLECHA tbl_p_sitelocality
ON tbl_f_site.VILLEAUTO = tbl_p_sitelocality.VILLEAUTO';
    }

    $query .= '
WHERE 1';

    if (defined $tabFilters->{'id'})
    {
        $query .= '
AND tbl_f_estimatedetail.DETAILSDEVISAUTO IN (' . &LOC::Util::join(', ', $tabFilters->{'id'}) . ')';
    }
    if (defined $tabFilters->{'stateId'})
    {
        $query .= '
AND ' . $stateQuery . ' IN (' . $db->quote($tabFilters->{'stateId'}) . ')';
    }

    # Critères sur le devis
    if (defined $tabFilters->{'estimateId'})
    {
        $query .= '
AND tbl_f_estimatedetail.DEVISAUTO IN (' . &LOC::Util::join(', ', $tabFilters->{'estimateId'}) . ')';
    }
    if (defined $tabFilters->{'estimateAgencyId'})
    {
        $query .= '
AND tbl_f_estimate.AGAUTO IN (' . $db->quote($tabFilters->{'estimateAgencyId'}) . ')';
    }
    if (defined $tabFilters->{'estimateStateId'})
    {
        $query .= '
AND tbl_f_estimate.ETCODE IN (' . $db->quote($tabFilters->{'estimateStateId'}) . ')';
    }
    if (defined $tabFilters->{'isAccepted'})
    {
        $query .= '
AND tbl_f_estimatedetail.DETAILSDEVISACCEPTE = ' . ($tabFilters->{'isAccepted'} ? -1 : 0);
    }
    if (defined $tabFilters->{'isFollowed'})
    {
        $query .= '
AND tbl_f_estimatedetail.DETAILSDEVISSUIVI = ' . ($tabFilters->{'isFollowed'} ? -1 : 0);
    }
    if (defined $tabFilters->{'isDatesToConfirm'})
    {
        $query .= '
AND tbl_f_estimatedetail.DETAILSDEVISDATESACONFIRMER = ' . ($tabFilters->{'isDatesToConfirm'} ? -1 : 0);
    }
    # - identifiant du client
    if (defined $tabFilters->{'customerId'})
    {
        $query .= '
AND tbl_f_estimate.CLAUTO IN (' . &LOC::Util::join(', ', $tabFilters->{'customerId'}) . ')';
    }
    # - date de début de location
    if (defined $tabFilters->{'beginDate'})
    {
        if (ref($tabFilters->{'beginDate'}) eq 'ARRAY')
        {
            $query .= '
AND DATE(tbl_f_estimatedetail.DETAILSDEVISDATE) BETWEEN ' . $db->quote($tabFilters->{'beginDate'}->[0]) . ' AND ' . $db->quote($tabFilters->{'beginDate'}->[1]);
        }
        else
        {
            $query .= '
AND (DATE(tbl_f_estimatedetail.DETAILSDEVISDATE) >= ' . $db->quote($tabFilters->{'beginDate'}) . ')';
        }
    }
    #- date de fin de location
    if (defined $tabFilters->{'endDate'})
    {
        if (ref($tabFilters->{'endDate'}) eq 'ARRAY')
        {
            $query .= '
AND DATE(tbl_f_estimatedetail.DETAILSDEVISDATEFIN) BETWEEN ' . $db->quote($tabFilters->{'endDate'}->[0]) . ' AND ' . $db->quote($tabFilters->{'endDate'}->[1]);
        }
        else
        {
            $query .= '
AND DATE(tbl_f_estimatedetail.DETAILSDEVISDATEFIN) <= ' . $db->quote($tabFilters->{'endDate'});
        }
    }

    # Identifiant du créateur
    if (defined $tabFilters->{'userId'})
    {
        $query .= '
AND f_user_creator.usr_id IN (' . &LOC::Util::join(', ', $tabFilters->{'userId'}) . ')';

    }

    # Libellé du chantier
    if (defined $tabFilters->{'siteLabel'})
    {
        if ($tabFilters->{'siteLabel'} =~ /%/)
        {
            $query .= '
AND tbl_f_site.CHLIBELLE LIKE ' . $db->quote($tabFilters->{'siteLabel'});
        }
        else
        {
            $query .= '
AND tbl_f_site.CHLIBELLE IN (' . $db->quote($tabFilters->{'siteLabel'}) . ')';
        }
    }

    # Département du chantier
    if (defined $tabFilters->{'siteDepartment'})
    {
        $query .= '
AND tbl_p_sitelocality.DEPVILLE IN (' . $db->quote($tabFilters->{'siteDepartment'}) . ')';
    }


    # - identifiant de l'interlocuteur
    if (defined $tabFilters->{'negotiatorId'})
    {
        $query .= '
AND tbl_f_estimate.PEAUTO_INTERLOC IN (' . &LOC::Util::join(', ', $tabFilters->{'negotiatorId'}) . ')';
    }

    # - identifiant de l'agence
    if (defined $tabFilters->{'agencyId'})
    {
        $query .= '
AND tbl_f_estimate.AGAUTO IN (' . $db->quote($tabFilters->{'agencyId'}) . ')';
    }

    if (defined $tabOptions->{'sorter'})
    {
        if ($tabOptions->{'sorter'} eq 'id' && defined $tabFilters->{'id'})
        {
            $query .= '
ORDER BY FIELD(tbl_f_estimatedetail.DETAILSDEVISAUTO, ' . &LOC::Util::join(', ', $tabFilters->{'id'}) . ');';
        }
    }
    else
    {
        $query .= '
ORDER BY tbl_f_estimatedetail.DETAILSDEVISAUTO;';
    }


    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        # Formatage des données
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}          *= 1;
            $tabRow->{'estimate.id'} *= 1;
            $tabRow->{'quantity'}    *= 1;
            $tabRow->{'model.id'}    *= 1;
            $tabRow->{'isImperativeModel'} *= 1;
            $tabRow->{'duration'}    *= 1;
            $tabRow->{'rentAmount'}  *= 1;
            $tabRow->{'rentTotal'}   *= 1;
            $tabRow->{'total'}       *= 1;
            $tabRow->{'proformaFlags'} *= 1;
            $tabRow->{'beginDate'}   .= ''; # pas de undef

            $tabRow->{'isAccepted'}        *= 1;
            $tabRow->{'isFollowed'}        *= 1;
            $tabRow->{'isDatesToConfirm'} *= 1;
            $tabRow->{'isAcceptable'} = ($tabRow->{'estimate.state.id'} ne STATE_ABORTED && $tabRow->{'state.id'} eq LINE_STATE_ACTIVE ? 1 : 0);

            $tabRow->{'additionalDays'} *= 1;
            $tabRow->{'deductedDays'}   *= 1;

            # Index de la ligne dans le devis
            if ($flags & GETLINEINFOS_INDEX)
            {
                $tabRow->{'index'} *= 1;
            }
            # Id du contrat lié
            if ($flags & GETLINEINFOS_LINKEDCONTRACT)
            {
                my @tab = split(';', $tabRow->{'linkedContract.infos'});
                delete($tabRow->{'linkedContract.infos'});
                $tabRow->{'linkedContract.id'}   = $tab[0] * 1;
                $tabRow->{'linkedContract.code'} = $tab[1];
            }

            # Assurance
            if ($flags & GETLINEINFOS_INSURANCE)
            {
                $tabRow->{'insuranceType.id'}     *= 1;
                $tabRow->{'insurance.rate'}       *= 1;
                $tabRow->{'insurance.amount'}     *= 1;
                $tabRow->{'insurance.flags'}      *= 1;
                $tabRow->{'insurance.mode'}       *= 1;
                $tabRow->{'insurance.isInvoiced'} *= 1;
                $tabRow->{'insurance.isIncluded'} *= 1;
                $tabRow->{'insurance.isCustomer'} *= 1;
            }
            if ($flags & GETLINEINFOS_EXT_INSURANCETYPE)
            {
                $tabRow->{'insuranceType.tabInfos'} = &LOC::Insurance::Type::getInfos($countryId, $tabRow->{'insuranceType.id'});
            }

            # Gestion de recours
            if ($flags & GETLINEINFOS_APPEAL)
            {
                $tabRow->{'appealType.id'}  = ($tabRow->{'appealType.id'} ne '' ? $tabRow->{'appealType.id'} * 1 : undef);
                $tabRow->{'appeal.value'}   = ($tabRow->{'appeal.value'} ne '' ? $tabRow->{'appeal.value'} * 1 : undef);
                $tabRow->{'appeal.amount'} *= 1;
            }
            if ($flags & GETLINEINFOS_EXT_APPEALTYPE)
            {
                $tabRow->{'appealType.tabInfos'} = ($tabRow->{'appealType.id'} ? &LOC::Appeal::Type::getInfos($countryId, $tabRow->{'appealType.id'}) : undef);
            }

            # Nettoyage
            if ($flags & GETLINEINFOS_CLEANING)
            {
                $tabRow->{'cleaning.type.id'}   *= 1;
                $tabRow->{'cleaning.amount'} *= 1;
            }

            # Participation au recyclage
            if ($flags & GETLINEINFOS_RESIDUES)
            {
                $tabRow->{'residues.mode'}   *= 1;
                $tabRow->{'residues.value'}   = ($tabRow->{'residues.value'} ne '' ? $tabRow->{'residues.value'} * 1 : undef);
                $tabRow->{'residues.amount'} *= 1;
            }

            # Carburant
            if ($flags & GETLINEINFOS_FUEL)
            {
                $tabRow->{'fuel.quantity'}   = ($tabRow->{'fuel.quantity'} ne '' ? $tabRow->{'fuel.quantity'} * 1 : undef);
                $tabRow->{'fuel.amount'}    *= 1;
                $tabRow->{'fuel.unitPrice'} *= 1;
            }

            # Ajout des propriétés du devis
            if ($flags & GETLINEINFOS_ESTIMATE)
            {
                $tabRow->{'estimate.customer.id'} *= 1;
                $tabRow->{'estimate.zone'} *= 1;

                if (($flags & GETLINEINFOS_ESTIMATE_CUSTOMER) == GETLINEINFOS_ESTIMATE_CUSTOMER)
                {
                    $tabRow->{'estimate.customer.isNewAccount'} = &LOC::Customer::isNewAccount($countryId, $tabRow->{'estimate.customer.code'},
                                                                                                           $tabRow->{'estimate.customer.accountingCode'});
                }

                $tabRow->{'estimate.site.id'} *= 1;

                if (($flags & GETLINEINFOS_ESTIMATE_SITE) == GETLINEINFOS_ESTIMATE_SITE)
                {
                    $tabRow->{'estimate.site.locality.id'} *= 1;
                    $tabRow->{'estimate.site.isAnticipated'} *= 1;
                    $tabRow->{'estimate.site.deliveryHour'}        = &LOC::Site::_toHour($tabRow->{'estimate.site.deliveryHour'});
                    $tabRow->{'estimate.site.deliveryTimeSlot.id'} = ($tabRow->{'estimate.site.deliveryTimeSlot.id'} ? $tabRow->{'estimate.site.deliveryTimeSlot.id'} * 1 : undef);
                    $tabRow->{'estimate.site.recoveryHour'}        = &LOC::Site::_toHour($tabRow->{'estimate.site.recoveryHour'});
                    $tabRow->{'estimate.site.recoveryTimeSlot.id'} = ($tabRow->{'estimate.site.recoveryTimeSlot.id'} ? $tabRow->{'estimate.site.recoveryTimeSlot.id'} * 1 : undef);
                }

                # Date de valeur du devis
                if (($flags & GETLINEINFOS_ESTIMATE_VALUEDATE) == GETLINEINFOS_ESTIMATE_VALUEDATE)
                {
                    $tabRow->{'estimate.valueDate'} = &getValueDate($countryId, $tabRow->{'estimate.id'});
                }
            }

            # Ajout des informations du modèle de machine
            if ($flags & GETLINEINFOS_EXT_MODEL)
            {
                $tabRow->{'model.tabInfos'} = &LOC::Model::getInfos($countryId, $tabRow->{'model.id'});
            }

            # Duration
            if ($flags & GETLINEINFOS_DURATIONS)
            {
                # Calcul de la durée de location
                $tabRow->{'realDuration'} = $tabRow->{'duration'} - $tabRow->{'additionalDays'} + $tabRow->{'deductedDays'};

                if ($tabRow->{'beginDate'} ne '' && $tabRow->{'endDate'} ne '')
                {
                    # Est-ce que la durée de location a changé ?
                    # Raison: Les jours fériés peuvent avoir changé
                    my $realDuration = &LOC::Date::getRentDuration(
                                                                        $tabRow->{'beginDate'},
                                                                        $tabRow->{'endDate'},
                                                                        $tabRow->{'estimate.agency.id'});

                    $tabRow->{'isRealDurationChanged'} = ($tabRow->{'realDuration'} == $realDuration ? 0 : 1);

                    # Calcul de la durée calendaire
                    $tabRow->{'calendarDuration'} = &LOC::Date::getDeltaDays(
                                                                $tabRow->{'beginDate'},
                                                                $tabRow->{'endDate'});
                }
                else
                {
                    $tabRow->{'isRealDurationChanged'} = 0;
                    $tabRow->{'calendarDuration'} = 0;
                }
            }

            # Ajout des informations sur les services de complémentaires loc et transport
            if ($flags & GETLINEINFOS_EXT_RENTSERVICES)
            {
                # Récupération des informations
                $tabRow->{'tabRentServices'} = &getLineRentServices($countryId, $tabRow->{'id'});
            }

            # Ajout des informations tarification location
            if ($flags & GETLINEINFOS_TARIFFRENT)
            {
                $tabRow->{'rentTariff.baseAmount'}               *= 1;
                $tabRow->{'rentTariff.standardReductionPercent'} *= 1;
                $tabRow->{'rentTariff.maxiReductionPercent'}     *= 1;
                $tabRow->{'rentTariff.specialReductionPercent'}  *= 1;
            }
            if ($flags & GETLINEINFOS_EXT_TARIFFRENT)
            {
                $tabRow->{'rentTariff.tabInfos'} = &LOC::Tariff::Rent::getInfos(
                                                       $countryId,
                                                       'estimateLine',
                                                       $tabRow->{'id'},
                                                       LOC::Tariff::Rent::GETINFOS_ALL);
            }

            # Ajout des informations tarification transport
            if ($flags & GETLINEINFOS_TARIFFTRSP)
            {
                $tabRow->{'transportTariff.baseAmount'}               *= 1;
                $tabRow->{'transportTariff.standardReductionPercent'} *= 1;
                $tabRow->{'transportTariff.specialReductionPercent'}  *= 1;
                $tabRow->{'transportTariff.deliveryAmount'}           *= 1;
                $tabRow->{'transportTariff.isNoDelivery'}             *= 1;
                $tabRow->{'transportTariff.isAddMachineDelivery'}     *= 1;
                $tabRow->{'transportTariff.recoveryAmount'}           *= 1;
                $tabRow->{'transportTariff.isNoRecovery'}             *= 1;
                $tabRow->{'transportTariff.isAddMachineRecovery'}     *= 1;
                $tabRow->{'transportTariff.zone'}                     *= 1;
            }
            if ($flags & GETLINEINFOS_EXT_TARIFFTRSP)
            {
                # Récupération des informations sur l'estimation transport
                $tabRow->{'transportTariff.tabInfos'} = &LOC::Tariff::Transport::getInfos(
                                                            $countryId,
                                                            'estimateLine',
                                                            $tabRow->{'id'},
                                                            (exists $tabOptions->{'transportTariff'}->{'flags'} ?
                                                             $tabOptions->{'transportTariff'}->{'flags'} :
                                                             LOC::Tariff::Transport::GETINFOS_ALL));
            }
            if ($flags & GETLINEINFOS_CONVERTIBLE)
            {
                $tabRow->{'isConvertible'} = &_isConvertibleLine($countryId, $tabRow->{'id'}, $tabRow);
            }

            # Possibilités
            if ($flags & GETLINEINFOS_POSSIBILITIES)
            {
                $tabRow->{'possibilities'} = {};

                &LOC::Util::setTreeValues($tabRow->{'possibilities'}, {
                    'duration' => ['setInvoiceableDay', 'setNonInvoiceableDay']
                }, 0);
            }

            # Droits
            if ($flags & GETLINEINFOS_RIGHTS)
            {
                # Informations sur l'utilisateur
                my $tabUserInfos = &LOC::Session::getUserInfos();
                if (defined $tabOptions->{'userId'} && $tabUserInfos->{'id'} != $tabOptions->{'userId'})
                {
                    $tabUserInfos = &LOC::User::getInfos($tabOptions->{'userId'});
                }
                $tabRow->{'tabRights'} = &_getLineRights($tabRow, $tabUserInfos, $tabRow->{'proformaFlags'});
            }
        }
    }

    return (wantarray ? %$tab : $tab);
}


# Function: getList
# Récupérer la liste des devis
#
# Contenu d'une ligne du hashage :
# - id => Identifiant du devis
# - code  => Code du devis
# - tracking => Commentaires de suivi
# - comments => Commentaires
# - email => E-mail du devis
# - orderNo => Numéro de commande
# - agency.id => Identifiant de l'agence du devis
# - country.id => Pays du devis
# - customer.id => Identifiant du client associé
# - customer.code => Code du client (si $flags & GETINFOS_CUSTOMER)
# - customer.name => Raison sociale du client (si $flags & GETINFOS_CUSTOMER)
# - customer.siret => Siret du client (si $flags & GETINFOS_CUSTOMER)
# - customer.controlAccount => Compte général du client (si $flags & GETINFOS_CUSTOMER)
# - site.id => Identifiant du chantier associé
# - site.tabInfos => Informations du chantier (si $flags & GETINFOS_EXT_SITE)
# - zone => Zone
# - isFullService => Indique si c'est un devis FullService
# - abortReason.id => Identifiant du motif d'abandon
# - abortComments => Commentaires d'abandon
# - tabLines => Identifiant des lignes du devis
# - nbConvertedLines => Nombre de lignes converties
# - state.id => Identifiant de l'état du devis
# - state.label => Libellé de l'état du devis
# - creator.id => Id du créateur du devis
# - creator.firstName => Prénom du créateur (si $flags & GETINFOS_CREATOR)
# - creator.name => Nom du créateur (si $flags & GETINFOS_CREATOR)
# - creator.fullName => Nom complet du créateur (si $flags & GETINFOS_CREATOR)
# - amount => Montant total du devis
# - modificationDate => Date de modification
# - creationDate => Date de création
#
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres ('id' : identifiant du devis,
#                                          'stadeId' : identifiant de l'état,
#                                          'userId' : identifiant du créateur du devis,
#                                          'customerId' : identifiant du client,
#                                          'agencyId' : identifiant de l'agence,
#                                          'tarifFamilyId' : identifiant de la famille tarifaire)
# hashref $tabOptions - Options supplémentaires
# int     $flags      - Flags
#
# Returns:
# hash|hashref|0
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT ' . (defined $tabOptions->{'limit'} && exists ($tabOptions->{'limit'}->{'&total'}) ? 'SQL_CALC_FOUND_ROWS' : '');

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query .= '
    tbl_f_estimate.DEVISAUTO AS `id`,
    ' . generateCode('tbl_f_estimate.DEVISAUTO', 'tbl_f_estimate.AGAUTO', '-tbl_f_estimate.DEVISFULLSERVICE', 1) . ' AS `code`,
    tbl_f_estimate.DEVISNOTE AS `tracking`,
    tbl_f_estimate.DEVISCOMMENTAIRE AS `comments`,
    tbl_f_estimate.DEVISMAIL AS `email`,
    tbl_f_estimate.DEVISNUMCOM AS `orderNo`,
    tbl_f_estimate.DEVISCONTACTCOM AS `orderContact.fullName`,
    tbl_f_estimate.DEVISTELEPHONECOM AS `orderContact.telephone`,
    tbl_f_estimate.DEVISEMAILCOM AS `orderContact.email`,
    tbl_f_estimate.AGAUTO AS `agency.id`,
    "' . $countryId . '" AS `country.id`,
    tbl_f_estimate.CLAUTO AS `customer.id`,';
        if ($flags & GETINFOS_CUSTOMER)
        {
            $query .= '
    tbl_f_customer.CLCODE AS `customer.code`,
    tbl_f_customer.CLSTE AS `customer.name`,
    tbl_f_customer.CLSIREN AS `customer.siret`,
    tbl_f_customer.CLCOLLECTIF AS `customer.controlAccount`,';
        }
        $query .= '
    tbl_f_estimate.CHAUTO AS `site.id`,
    tbl_f_estimate.ZONE AS `zone`,
    -tbl_f_estimate.DEVISFULLSERVICE AS `isFullService`,
    tbl_f_estimate.MOTIFAUTO AS `abortReason.id`,
    tbl_f_estimate.DEVISMOTIFAUTRE AS `abortComments`,
    (SELECT
         CONCAT(GROUP_CONCAT(tbl_f_estimatedetail.DETAILSDEVISAUTO ORDER BY tbl_f_estimatedetail.DETAILSDEVISAUTO ASC SEPARATOR ";"), "|",
         GROUP_CONCAT(tbl_f_estimatedetail.DETAILSDEVISOK ORDER BY tbl_f_estimatedetail.DETAILSDEVISAUTO ASC SEPARATOR ";"))
     FROM DETAILSDEVIS tbl_f_estimatedetail
     WHERE tbl_f_estimatedetail.DEVISAUTO = tbl_f_estimate.DEVISAUTO
     ORDER BY tbl_f_estimatedetail.DETAILSDEVISAUTO) AS `tabLines`,
    tbl_f_estimate.ETCODE AS `state.id`,
    tbl_p_state.ETLIBELLE AS `state.label`,
    f_user_creator.usr_id AS `creator.id`,';
        if ($flags & GETINFOS_CREATOR)
        {
            $query .= '
    f_user_creator.usr_firstname AS `creator.firstName`,
    f_user_creator.usr_name AS `creator.name`,
    CONCAT_WS(" ", f_user_creator.usr_firstname, f_user_creator.usr_name) AS `creator.fullName`,';
        }
        $query .= '
    tbl_f_estimate.PEAUTO_INTERLOC AS `negotiator.id`,';
        if ($flags & GETINFOS_NEGOTIATOR)
        {
            $query .= '
    f_user_negotiator.usr_firstname AS `negotiator.firstName`,
    f_user_negotiator.usr_name AS `negotiator.name`,
    CONCAT_WS(" ", f_user_negotiator.usr_firstname, f_user_negotiator.usr_name) AS `negotiator.fullName`,
    f_user_negotiator.usr_mobile AS `negotiator.mobile`,';
        }
        $query .= '
    tbl_f_estimate.DEVISMONTANTFR AS `total`,
    tbl_f_estimate.DEVISDATE AS `modificationDate`,
    tbl_f_estimate.DEVISDATECREATION AS `creationDate`
FROM DEVIS tbl_f_estimate
INNER JOIN ETATTABLE tbl_p_state
ON tbl_f_estimate.ETCODE = tbl_p_state.ETCODE';
        if ($flags & GETINFOS_CUSTOMER)
        {
            $query .= '
INNER JOIN CLIENT tbl_f_customer
ON tbl_f_customer.CLAUTO = tbl_f_estimate.CLAUTO';
        }
        if ($flags & GETINFOS_NEGOTIATOR)
        {
            $query .= '
LEFT JOIN frmwrk.f_user f_user_negotiator
ON tbl_f_estimate.PEAUTO_INTERLOC = f_user_negotiator.usr_id';
        }
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query .= '
    COUNT(*)
FROM DEVIS tbl_f_estimate';
    }
    else
    {
        $query .= '
    tbl_f_estimate.DEVISAUTO AS `id`,
    ' . generateCode('tbl_f_estimate.DEVISAUTO', 'tbl_f_estimate.AGAUTO', 0, 1) . ' AS `code`
FROM DEVIS tbl_f_estimate';
    }

    # Jointures pour les recherches
    $query .= '
LEFT JOIN frmwrk.f_user f_user_creator
ON tbl_f_estimate.PELOGIN = f_user_creator.usr_login';

    $query .= '
WHERE TRUE';
    # Filtres
    # - identifiant du devis
    if (defined $tabFilters->{'id'})
    {
        if ($tabFilters->{'id'} =~ /%/)
        {
            $query .= '
AND tbl_f_estimate.DEVISAUTO LIKE ' . $db->quote($tabFilters->{'id'});
        }
        else
        {
            $query .= '
AND tbl_f_estimate.DEVISAUTO IN (' . &LOC::Util::join(', ', $tabFilters->{'id'}) . ')';
        }
    }
    # - identifiant de l'état
    if (defined $tabFilters->{'stateId'})
    {
        $query .= '
AND tbl_f_estimate.ETCODE IN (' . $db->quote($tabFilters->{'stateId'}) . ')';
    }
    # - identifiant de l'utilisateur
    if (defined $tabFilters->{'userId'})
    {
        $query .= '
AND f_user_creator.usr_id IN (' . &LOC::Util::join(', ', $tabFilters->{'userId'}) . ')';
    }
    # - identifiant de l'interlocuteur
    if (defined $tabFilters->{'negotiatorId'})
    {
        $query .= '
AND tbl_f_estimate.PEAUTO_INTERLOC IN (' . &LOC::Util::join(', ', $tabFilters->{'negotiatorId'}) . ')';
    }
    # - identifiant du client
    if (defined $tabFilters->{'customerId'})
    {
        $query .= '
AND tbl_f_estimate.CLAUTO IN (' . &LOC::Util::join(', ', $tabFilters->{'customerId'}) . ')';
    }
    # - identifiant de l'agence
    if (defined $tabFilters->{'agencyId'})
    {
        $query .= '
AND tbl_f_estimate.AGAUTO IN (' . $db->quote($tabFilters->{'agencyId'}) . ')';
    }
    # - identifiant de la famille tarifaire
    if (defined $tabFilters->{'tariffFamilyId'})
    {
        $query .= '
AND (SELECT COUNT(*)
     FROM DETAILSDEVIS  
     LEFT JOIN AUTH.MODELEMACHINE tbl_f_model
     ON DETAILSDEVIS.MOMAAUTO = tbl_f_model.MOMAAUTO
     WHERE tbl_f_model.FAMTARAUTO IN (' . $db->quote($tabFilters->{'tariffFamilyId'}) . ')
     AND DETAILSDEVIS.DEVISAUTO = tbl_f_estimate.DEVISAUTO) > 0';
    }

    $query .= '
ORDER BY tbl_f_estimate.DEVISDATECREATION DESC';

    if (defined $tabOptions->{'limit'})
    {
        my $offset = 0;
        my $rowCount = 0;

        if (ref $tabOptions->{'limit'} eq 'HASH')
        {
            $offset = (defined $tabOptions->{'limit'}->{'offset'} ? $tabOptions->{'limit'}->{'offset'} : 0);
            $rowCount = (defined $tabOptions->{'limit'}->{'count'} ? $tabOptions->{'limit'}->{'count'} : 0);
        }
        else
        {
            $rowCount = $tabOptions->{'limit'};
        }

        $query .= '
LIMIT ' . $offset . ',' . $rowCount;
    }

    $query .= ';';
    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return 0;
    }

    # Récupération du total complet
    if (defined $tabOptions->{'limit'} && exists ($tabOptions->{'limit'}->{'&total'}))
    {
        $tabOptions->{'limit'}->{'&total'} = $db->fetchOne('SELECT FOUND_ROWS();');
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        # Formatage des données
        foreach my $tabRow (values %$tab)
        {
            my @tabParts  = split(/\|/, $tabRow->{'tabLines'});
            my @tabLines  = split(/\;/, $tabParts[0]);
            my @tabStates = split(/\;/, $tabParts[1]);
            my @tabLinesStates = ();

            # Comptage des lignes par état
            $tabRow->{'nbValidLines'} = 0;
            my $nbLines = @tabStates;
            for (my $cpt = 0; $cpt < $nbLines; $cpt++)
            {
                if ($tabStates[$cpt] == 0)
                {
                    push(@tabLinesStates, LINE_STATE_ACTIVE);
                    $tabRow->{'nbValidLines'}++;
                }
                elsif ($tabStates[$cpt] == -1)
                {
                    push(@tabLinesStates, LINE_STATE_CONVERTED);
                    $tabRow->{'nbValidLines'}++;
                }
                elsif ($tabStates[$cpt] == -2)
                {
                    push(@tabLinesStates, LINE_STATE_INACTIVE);
                }
            }

            $tabRow->{'id'}             *= 1;
            $tabRow->{'customer.id'}    *= 1;
            $tabRow->{'site.id'}        *= 1;
            $tabRow->{'zone'}           *= 1;
            $tabRow->{'isFullService'}  *= 1;
            $tabRow->{'abortReason.id'} *= 1;
            $tabRow->{'creator.id'}     *= 1;
            $tabRow->{'negotiator.id'}   = ($tabRow->{'negotiator.id'} ? $tabRow->{'negotiator.id'} * 1 : undef);
            $tabRow->{'total'}          *= 1;
            $tabRow->{'tabLines'}       = \@tabLines;
            $tabRow->{'tabLinesStates'} = \@tabLinesStates;
            $tabRow->{'state.id'}       = &_getCorrectStateId($tabRow->{'state.id'}, \@tabLinesStates);

            # Lignes en erreurs
            my $nbErroneousLines = undef;
            if ($flags & GETINFOS_ERRONEOUSLINES)
            {
                $tabRow->{'tabErroneousLines'} = &getErroneousLines($countryId, $tabRow->{'id'});
                $nbErroneousLines = @{$tabRow->{'tabErroneousLines'}};
            }
            # Est-il imprimable ?
            if ($flags & GETINFOS_PRINTABLE)
            {
                $tabRow->{'isPrintable'} = &isPrintable($countryId, $tabRow->{'id'}, $nbErroneousLines);
            }

            # Ajout des informations du chantier
            if ($flags & GETINFOS_EXT_SITE)
            {
                # Récupération des informations du chantier
                $tabRow->{'site.tabInfos'} = &LOC::Site::getInfos($countryId, $tabRow->{'site.id'});
            }

            # Droits
            if ($flags & GETINFOS_RIGHTS)
            {
                # Informations sur l'utilisateur
                my $tabUserInfos = &LOC::Session::getUserInfos();
                if (defined $tabOptions->{'userId'} && $tabUserInfos->{'id'} != $tabOptions->{'userId'})
                {
                    $tabUserInfos = &LOC::User::getInfos($tabOptions->{'userId'});
                }
                $tabRow->{'tabRights'} = &_getRights($tabRow, $tabUserInfos);
            }

            # Date de valeur du devis
            if ($flags & GETINFOS_VALUEDATE)
            {
                $tabRow->{'valueDate'} = &getValueDate($countryId, $tabRow->{'id'});
            }
        }
    }
    return (wantarray ? %$tab : $tab);
}


# Function: getRights
# Récupérer les droits sur le devis (par rapport à l'utilisateur)
#
# Parameters:
# string $countryId  - Pays
# string $agencyId   - Agence
# int    $id         - Id du devis
# int    $userId     - Id de l'utilisateur
#
# Returns:
# hash|hashref - Tableau des droits
sub getRights
{
    my ($countryId, $agencyId, $id, $userId) = @_;

    my $tabEstimateInfos;
    my $tabUserInfos;

    # Informations sur l'utilisateur
    $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Informations sur le devis
    if ($id)
    {
        $tabEstimateInfos = &getInfos($countryId, $id);
    }
    else
    {
        $tabEstimateInfos = {
            'state.id'  => STATE_ACTIVE,
            'agency.id' => $agencyId
        };
    }

    return &_getRights($tabEstimateInfos, $tabUserInfos);
}


# Function: getLineRights
# Récupérer les droits sur une ligne de devis pour un utilisateur donné
#
# Parameters:
# string $countryId  - Pays
# string $agencyId   - Agence
# int    $id         - Id de la ligne de devis
# int    $userId     - Id de l'utilisateur
#
# Returns:
# hash|hashref - Tableau des droits
sub getLineRights
{
    my ($countryId, $agencyId, $id, $userId) = @_;

    my $tabEstimateLineInfos;
    my $tabUserInfos;

    # Informations sur l'utilisateur
    $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Informations sur le devis
    if ($id)
    {
        $tabEstimateLineInfos = &getLineInfos($countryId, $id);
    }
    else
    {
        $tabEstimateLineInfos = {
            'state.id'           => LINE_STATE_ACTIVE,
            'estimate.state.id'  => STATE_ACTIVE,
            'estimate.agency.id' => $agencyId
        };
    }

    return &_getLineRights($tabEstimateLineInfos, $tabUserInfos, 0);
}


# Function: getStates
# Récupérer la liste des états de devis
#
# Parameters:
# string  $countryId  - Pays
#
# Returns:
# hash|hashref -
sub getStates
{
    my ($countryId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT
    tbl_p_state.ETCODE,
    tbl_p_state.ETLIBELLE
FROM ETATTABLE tbl_p_state
WHERE tbl_p_state.ETCODE IN ("' . STATE_ACTIVE . '", "' . STATE_ABORTED. '", "' . STATE_CONTRACT . '")
ORDER BY tbl_p_state.ETLIBELLE';
    my $tab = $db->fetchPairs($query);
    if (!$tab)
    {
        return undef;
    }
    return (wantarray ? %$tab : $tab);
}


# Function: getValueDate
# Récupérer la date de valeur du devis de location
#
# Parameters:
# string $countryId  - Pays
# int    $estimateId - Id du devis de location
#
# Returns:
# string - Date de valeur
sub getValueDate
{
    my ($countryId, $estimateId) = @_;

    # Récupèration de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # La date de valeur du devis est sa date de création
    my $query = '
SELECT
    DATE(tbl_f_estimate.DEVISDATECREATION)
FROM DEVIS tbl_f_estimate
WHERE tbl_f_estimate.DEVISAUTO = ' . $estimateId . ';';
    my $valueDate = $db->fetchOne($query);

    return $valueDate;
}

# Function: getLineValueDate
# Récupérer la date de valeur de la ligne de devis de location
#
# Parameters:
# string $countryId  - Pays
# int    $estimateId - Id du devis de location
#
# Returns:
# string - Date de valeur
sub getLineValueDate
{
    my ($countryId, $lineId) = @_;

    # Récupèration de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # La date de valeur du devis est sa date de création
    my $query = '
SELECT
    DATE(tbl_f_estimate.DEVISDATECREATION)
FROM DETAILSDEVIS tbl_f_estimateline
INNER JOIN DEVIS tbl_f_estimate ON tbl_f_estimate.DEVISAUTO = tbl_f_estimateline.DEVISAUTO
WHERE tbl_f_estimateline.DETAILSDEVISAUTO = ' . $lineId . ';';
    my $valueDate = $db->fetchOne($query);

    return $valueDate;
}


# Function: insert
# Création d'un devis
#
# Parameters:
# string   $countryId  - Pays
# int      $customerId - Id du client
# hashref  $tabData    - Données à insérées
# int      $userId     - Utilisateur responsable de la création
# hashref  $tabErrors  - Tableau des erreurs
# hashref  $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool -
sub insert
{
    my ($countryId, $customerId, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    # Tableau des erreurs
    $tabErrors->{'fatal'} = [];
    $tabErrors->{'modules'} = {
        'site' => {},
        'lines' => []
    };
    my $hasErrors = 0;

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération des droits
    my $tabRights = &getRights($countryId, $tabData->{'agency.id'}, undef, $userId);

    # Vérifie que l'utilisateur ait bien le droit de créer un contrat
    if (!&_hasRight($tabRights->{'actions'}, 'valid'))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0003);
        return 0;
    }


    # Compte les différentes actions à effectuer
    my $nbInserts = 0;
    foreach my $tabLineData (@{$tabData->{'tabLines'}})
    {
        if ($tabLineData->{'@action'} eq 'insert')
        {
            $nbInserts++;
        }
    }

    # Il faut au moins une ligne à insérer
    if ($nbInserts == 0)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }


    # Paramètres
    my $agencyId = (defined $tabData->{'agency.id'} ? $tabData->{'agency.id'} : '');
    my $negotiatorId = (defined $tabData->{'negotiator.id'} ? $tabData->{'negotiator.id'} : undef);
    my $comments = (defined $tabData->{'comments'} ? &LOC::Util::trim($tabData->{'comments'}) : '');
    my $tracking = (defined $tabData->{'tracking'} ? &LOC::Util::trim($tabData->{'tracking'}) : '');
    my $isFullService = (defined $tabData->{'isFullService'} ? ($tabData->{'isFullService'} ? 1 : 0) : 0);
    my $zone = (defined $tabData->{'tabSite'}->{'zone'} ? $tabData->{'tabSite'}->{'zone'} : 0);
    my $orderNo = (defined $tabData->{'tabCustomer'}->{'orderNo'} ? $tabData->{'tabCustomer'}->{'orderNo'} : '');
    my $orderContactFullName  = (defined $tabData->{'tabCustomer'}->{'orderContact.fullName'} ? &LOC::Util::trim($tabData->{'tabCustomer'}->{'orderContact.fullName'}) : undef);
    my $orderContactTelephone = (defined $tabData->{'tabCustomer'}->{'orderContact.telephone'} ? $tabData->{'tabCustomer'}->{'orderContact.telephone'} : undef);
    my $orderContactEmail  = (defined $tabData->{'tabCustomer'}->{'orderContact.email'} ? &LOC::Util::trim($tabData->{'tabCustomer'}->{'orderContact.email'}) : undef);

    # Le devis est actif
    my $stateId = STATE_ACTIVE;


    # Vérification des données
    if (!$negotiatorId)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0010);
        return 0;
    }
    if ($customerId == 0)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0104);
        return 0;
    }

    # Verifie que l'agence est valide
    if ($agencyId eq '')
    {
        push(@{$tabErrors->{'fatal'}}, 0x0102);
        return 0;
    }

    # - Vérifie que le mail et le tél du contact commande sont renseignés
    if (!$orderContactEmail || !$orderContactTelephone)
    {
         push(@{$tabErrors->{'warning'}}, 0x0128);
    }

    # Vérifie que le mail du contact commande est valide
    if ($orderContactEmail && !&LOC::Util::isValidEmail($orderContactEmail))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0127);
        return 0
    }


    # Informations sur le chantier
    my $tabSiteData = $tabData->{'tabSite'};

    # Récupération des informations du client
    my $tabCustomerInfos = &LOC::Customer::getInfos($countryId, $customerId);

    # Vérifie que le client n'est pas bloqué au devis
    my $isCustomerLocked = (!$tabCustomerInfos->{'isTempUnlock'} &&
                            ($tabCustomerInfos->{'lockLevel'} == 3 || $tabCustomerInfos->{'lockLevel'} == 4));
    if ($isCustomerLocked)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0105);
        return 0;
    }

    # Indique si la tarification transport est activé ou non ?
    my $isTrspTarification  = &LOC::Characteristic::getAgencyValueByCode('TRSPTARIF', $agencyId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }


    # Mise à jour du chantier
    $tabSiteData->{'documentType'} = 'estimate';
    $tabSiteData->{'documentId'}   = undef;

    my $tabSiteErrors = $tabErrors->{'modules'}->{'site'};
    my $siteId = &LOC::Site::insert($countryId, $tabSiteData, $userId, $tabSiteErrors, $tabEndUpds);
    if (!$siteId)
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, 0x0130);
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabSiteErrors->{'fatal'}))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);
        }
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }
    if (@{$tabSiteErrors->{'warning'}} > 0)
    {
        push(@{$tabErrors->{'warning'}}, 0x0130);
    }
    # Zone
    if ($isTrspTarification && ($zone < 1 || $zone > 6))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0103);
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }


    # Enregistrement dans la table DEVIS
    my $tabUpdates = {
        'CLAUTO'             => $customerId,
        'AGAUTO'             => $agencyId,
        'PELOGIN*'           => '(SELECT usr_login FROM frmwrk.f_user WHERE usr_id = ' . $userId . ')',
        'CHAUTO'             => $siteId,

        'PEAUTO_INTERLOC'    => $negotiatorId,

        'DEVISMONTANTFR'     => 0,
        'DEVISMONTANTEU'     => 0,
        'DEVISCOMMENTAIRE'   => ($comments eq '' ? undef : $comments),
        'DEVISNOTE'          => ($tracking eq '' ? undef : $tracking),
        'DEVISFULLSERVICE'   => ($isFullService ? -1 : 0),
        'ZONE'               => $zone,
        'DEVISNUMCOM'        => substr($orderNo, 0, 25),
        'DEVISCONTACTCOM'    => $orderContactFullName,
        'DEVISTELEPHONECOM'  => $orderContactTelephone,
        'DEVISEMAILCOM'      => $orderContactEmail,
        'DEVISDATECREATION*' => 'Now()',
        'ETCODE'             => $stateId,

        'DEVISMAIL'          => undef
    };
    my $estimateId = $db->insert('DEVIS', $tabUpdates, $schemaName);
    if ($estimateId == -1)
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }
    $estimateId *= 1;

    # Données pour l'insertion des lignes
    my $tabInsertInfos = {
        'estimate.id'                       => $estimateId,
        'estimate.customer.id'              => $customerId,
        'estimate.agency.id'                => $agencyId,
        'estimate.zone'                     => $zone,
        'estimate.site.deliveryHour'        => $tabSiteData->{'deliveryHour'},
        'estimate.site.deliveryTimeSlot.id' => $tabSiteData->{'deliveryTimeSlot.id'},
        'estimate.state.id'                 => $stateId,
        'estimate.nbValidLines'             => 0
    };

    # Insertion des lignes
    my $hasLinesErrors = 0;
    foreach my $tabLineData (@{$tabData->{'tabLines'}})
    {
        my $tabLineErrors = {
            'fatal'   => [],
            'warning' => [],
            'modules' => {
                'rentTariff'      => {},
                'transportTariff' => {},
                'transport'       => {
                    'fatal' => [],
                    'modules' => {
                        'delivery' => {},
                        'recovery' => {}
                    }
                },
                'proforma'  => {},
                'days'      => {},
                'documents' => []
            }
        };
        my $isError = 0;
        if ($tabLineData->{'@action'} eq 'insert')
        {
            my $estimateLineId = &_insertLine($db, $countryId, $estimateId, $tabLineData, $tabInsertInfos, $userId,
                                              {}, $tabLineErrors, $tabEndUpds);
            if (!$estimateLineId)
            {
                $hasLinesErrors++;
                $isError = 1;
            }
            $tabInsertInfos->{'estimate.nbValidLines'}++;
        }
        push(@{$tabErrors->{'modules'}->{'lines'}}, $tabLineErrors);

        # Erreur fatale ?
        if ($isError)
        {
            if (&LOC::Util::in_array(0x0001, $tabLineErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, 0x0001);
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return 0;
            }
        }
    }

    if ($hasLinesErrors > 0)
    {
        $hasErrors += $hasLinesErrors;
        push(@{$tabErrors->{'fatal'}}, 0x0150);
    }

    # Mise à jour du montant total du devis
    if ($hasErrors == 0 && !&_updateAmounts($db, $countryId, $estimateId, 1))
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, 0x0001);
    }

    # Reblocage du client s'il était débloqué temporairement
    if ($hasErrors == 0 && !&_lockCustomerAfterUpdate($countryId, $tabCustomerInfos, $userId, $tabEndUpds, 'insert'))
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, 0x0001);
    }

    # Si il y a des données invalides alors on arrête ici
    if ($hasErrors > 0)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Trace de création du devis
    &LOC::EndUpdates::deleteTraces($tabEndUpds, 'estimate', $estimateId);

    my $tabTrace = {
        'schema'  => $schemaName,
        'code'    => LOC::Log::EventType::CREATION,
        'old'     => undef,
        'new'     => undef,
        'content' => ''
    };
    &LOC::EndUpdates::addTrace($tabEndUpds, 'estimate', $estimateId, $tabTrace);

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    # Sauvegarde de l'identifiant du devis
    if (exists $tabData->{'id'})
    {
        $tabData->{'@oldId'} = $tabData->{'id'};
    }
    $tabData->{'id'} = $estimateId;

    return $estimateId;
}


# Function: update
# Mise à jour du devis
#
# Parameters:
# string   $countryId  - Pays
# int      $estimateId - Id du devis
# hashref  $tabData    - Données à mettre à jour
# int      $userId     - Utilisateur responsable de la modification
# arrayref $tabErrors  - Tableau des erreurs
# hashref  $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool -
sub update
{
    my ($countryId, $estimateId, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    # Tableau des erreurs
    $tabErrors->{'fatal'} = [];
    $tabErrors->{'modules'} = {
        'site' => {},
        'lines' => []
    };
    my $hasErrors = 0;

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }


    # Compte les différentes actions à effectuer
    my $tabActionsCount = {'insert' => 0, 'update' => 0, 'delete' => 0};
    my $nbActions = 0;
    foreach my $tabLineData (@{$tabData->{'tabLines'}})
    {
        $tabActionsCount->{$tabLineData->{'@action'}}++;
        $nbActions++;
    }

    # Récupération des informations du devis
    my $tabInfos = &getInfos($countryId, $estimateId, GETINFOS_BASE);
    if (!$tabInfos)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Vérification de la date de modification (accès concurentiel)
    if (defined $tabData->{'modificationDate'} && $tabData->{'modificationDate'} ne $tabInfos->{'modificationDate'})
    {
        push(@{$tabErrors->{'fatal'}}, 0x0100);
        return 0;
    }

    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Récupération des droits
    my $tabRights = &_getRights($tabInfos, $tabUserInfos);


    # Paramètres
    my $customerId = $tabInfos->{'customer.id'};
    my $agencyId   = $tabInfos->{'agency.id'};
    my $newNegotiatorId = (exists $tabData->{'negotiator.id'} ? $tabData->{'negotiator.id'} : $tabInfos->{'negotiator.id'});
    my $oldNegotiatorId = $tabInfos->{'negotiator.id'};
    my $siteId     = $tabInfos->{'site.id'};
    my $comments   = (exists $tabData->{'comments'} ? &LOC::Util::trim($tabData->{'comments'}) : $tabInfos->{'comments'});
    my $tracking   = (exists $tabData->{'tracking'} ? &LOC::Util::trim($tabData->{'tracking'}) : $tabInfos->{'tracking'});
    my $abortReasonId = (exists $tabData->{'abortReason.id'} ? $tabData->{'abortReason.id'} : $tabInfos->{'abortReason.id'});
    my $abortComments = (exists $tabData->{'abortComments'} ? &LOC::Util::trim($tabData->{'abortComments'}) : $tabInfos->{'abortComments'});
    my $oldZone = $tabInfos->{'zone'};
    my $newZone = (exists $tabData->{'tabSite'}->{'zone'} ? $tabData->{'tabSite'}->{'zone'} : $oldZone);
    my $oldTotalAmount = $tabInfos->{'total'};
    my $orderNo = (exists $tabData->{'tabCustomer'}->{'orderNo'} ? $tabData->{'tabCustomer'}->{'orderNo'} : $tabInfos->{'orderNo'});
    my $orderContactFullName = (exists $tabData->{'tabCustomer'}->{'orderContact.fullName'} ? &LOC::Util::trim($tabData->{'tabCustomer'}->{'orderContact.fullName'}) : $tabInfos->{'orderContact.fullName'});
    my $orderContactTelephone = (exists $tabData->{'tabCustomer'}->{'orderContact.telephone'} ? &LOC::Util::trim($tabData->{'tabCustomer'}->{'orderContact.telephone'}) : $tabInfos->{'orderContact.telephone'});
    my $orderContactEmail = (exists $tabData->{'tabCustomer'}->{'orderContact.email'} ? &LOC::Util::trim($tabData->{'tabCustomer'}->{'orderContact.email'}) : $tabInfos->{'orderContact.email'});

    # Calcul de l'état
    my $oldStateId = $tabInfos->{'state.id'};
    my $newStateId = $oldStateId;
    if ($oldStateId ne STATE_CONTRACT)
    {
        if ($abortReasonId != $tabInfos->{'abortReason.id'})
        {
            if (!&_hasRight($tabRights->{'abort'}, 'reason'))
            {
                push(@{$tabErrors->{'fatal'}}, 0x0003);
                return 0;
            }
            if ($abortReasonId == 0)
            {
                $newStateId = STATE_ACTIVE;
                $abortComments = '';
            }
            else
            {
                if ($abortComments eq '')
                {
                    my $tabAbortInfos = &LOC::Estimate::getAbortInfos($countryId, $abortReasonId);
                    # Erreur commentaire obligatoire
                    if ($tabAbortInfos->{'requiredComments'})
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x0106);
                        return 0;
                    }
                }
                $newStateId = STATE_ABORTED;
            }
        }
    }


    # Impossible de modifier un devis abandonné
    if ($oldStateId eq STATE_ABORTED && $newStateId eq STATE_ABORTED)
    {
        if ($nbActions > 0)
        {
            push(@{$tabErrors->{'fatal'}}, 0x0107);
            return 0;
        }
        if ($oldZone != $newZone)
        {
            push(@{$tabErrors->{'fatal'}}, 0x0110);
            return 0;
        }
    }
    # Impossible de modifier la zone sur un devis dont l'état est contrat
    if ($oldStateId eq STATE_CONTRACT && $oldZone != $newZone)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0110);
        return 0;
    }


    # Vérification des données
    if (!$newNegotiatorId)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0010);
        return 0;
    }
    if ($customerId == 0)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0104);
        return 0;
    }

    # - Vérifie que le mail et le tél du contact commande sont renseignés
    if (!$orderContactEmail || !$orderContactTelephone)
    {
         push(@{$tabErrors->{'warning'}}, 0x0128);
    }

    # - Vérifie que le mail du contact commande est valide
    if ($orderContactEmail && !&LOC::Util::isValidEmail($orderContactEmail))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0127);
        return 0
    }

    # Vérification et historisation des modifications
    # -------------------------------
    my $tabChanges = {};

    # - Modification du commentaire
    if ($comments ne $tabInfos->{'comments'})
    {
        if (!&_hasRight($tabRights->{'infoscpl'}, 'comments'))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }
        $tabChanges->{'comments'} = {'old' => $comments, 'new' => $tabInfos->{'comments'}};
    }
    # - Modification du suivi
    if ($tracking ne $tabInfos->{'tracking'})
    {
        if (!&_hasRight($tabRights->{'infoscpl'}, 'tracking'))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }
        $tabChanges->{'tracking'} = {'old' => $tracking, 'new' => $tabInfos->{'tracking'}};
    }
    # - Numéro de commande
    if ($orderNo ne $tabInfos->{'orderNo'})
    {
        if (!&_hasRight($tabRights->{'customer'}, 'orderNo'))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }
        $tabChanges->{'orderNo'} = {'old' => $orderNo, 'new' => $tabInfos->{'orderNo'}};
    }
    if ($orderContactFullName ne $tabInfos->{'orderContact.fullName'})
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'customer'}, 'orderContact'))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }
        $tabChanges->{'orderContact.fullName'} = {'old' => $orderContactFullName, 'new' => $tabInfos->{'orderContact.fullName'}};
    }
    if ($orderContactTelephone ne $tabInfos->{'orderContact.telephone'})
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'customer'}, 'orderContact'))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }
        $tabChanges->{'orderContact.telephone'} = {'old' => $orderContactTelephone, 'new' => $tabInfos->{'orderContact.telephone'}};
    }
    if ($orderContactEmail ne $tabInfos->{'orderContact.email'})
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'customer'}, 'orderContact'))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }
        $tabChanges->{'orderContact.email'} = {'old' => $orderContactEmail, 'new' => $tabInfos->{'orderContact.email'}};
    }
    
    # - Modification interlocuteur
    if ($newNegotiatorId != $oldNegotiatorId)
    {
        if (!&_hasRight($tabRights->{'infos'}, 'negotiator'))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }
        $tabChanges->{'negotiator'} = {'old' => $newNegotiatorId, 'new' => $oldNegotiatorId};
    }

    # Récupération des informations du client
    my $tabCustomerInfos = &LOC::Customer::getInfos($countryId, $customerId);

    # Si il y a des insertions alors erreur !, la suppression est autorisée même si le client est bloqué au devis ($tabActionsCount->{'delete'})
    my $hasLockableUpdates = $tabActionsCount->{'insert'};
    my $isCustomerLocked = (!$tabCustomerInfos->{'isTempUnlock'} &&
                            ($tabCustomerInfos->{'lockLevel'} == 3 || $tabCustomerInfos->{'lockLevel'} == 4));
    # Vérifie que le client n'est pas bloqué au devis
    if ($hasLockableUpdates > 0 && $isCustomerLocked)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0105);
        return 0;
    }

    # Indique si la tarification transport est activé ou non ?
    my $isTrspTarification  = &LOC::Characteristic::getAgencyValueByCode('TRSPTARIF', $agencyId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }


    # Mise à jour du chantier
    my $tabSiteData = (exists $tabData->{'tabSite'} ? $tabData->{'tabSite'} : {});

    # Vérifie l'existence des horaires de livraison si le devis comporte des lignes acceptées
    my $tabAcceptedLines = &getLinesList($countryId, LOC::Util::GETLIST_PAIRS, {'estimateId' => $estimateId, 'isAccepted' => 1});
    my $countAcceptedLines = keys(%$tabAcceptedLines);

    if ($countAcceptedLines > 0 && !$tabSiteData->{'deliveryHour'} && !$tabSiteData->{'deliveryTimeSlot.id'})
    {
        # On ne peut pas accepter une ligne si l'heure de livraison n'est pas renseignée
        push(@{$tabErrors->{'fatal'}}, 0x1009);
        return 0;
    }



    $tabSiteData->{'documentType'} = 'estimate';
    $tabSiteData->{'documentId'}   = $estimateId;

    $tabSiteData->{'rights'}                 = $tabRights->{'site'};
    $tabSiteData->{'oldZone'}                = $oldZone;
    $tabSiteData->{'newZone'}                = $newZone;
    my $tabSiteErrors = $tabErrors->{'modules'}->{'site'};
    if (!&LOC::Site::update($countryId, $siteId, $tabSiteData, $userId, $tabSiteErrors, $tabEndUpds))
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, 0x0130);
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabSiteErrors->{'fatal'}))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }
        # Modifications interdites ?
        if (&LOC::Util::in_array(0x0003, $tabSiteErrors->{'fatal'}))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }
    }
    if (@{$tabSiteErrors->{'warning'}} > 0)
    {
        push(@{$tabErrors->{'warning'}}, 0x0130);
    }
    # Zone
    if ($isTrspTarification && ($newZone < 1 || $newZone > 6))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0103);
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Enregistrement dans la table DEVIS
    my $tabUpdates = {
        'PEAUTO_INTERLOC'  => $newNegotiatorId,

        'DEVISCOMMENTAIRE'  => ($comments eq '' ? undef : $comments),
        'DEVISNOTE'         => ($tracking eq '' ? undef : $tracking),
        'ETCODE'            => $newStateId,
        'MOTIFAUTO'         => ($abortReasonId == 0 ? undef : $abortReasonId),
        'DEVISMOTIFAUTRE'   => $abortComments,
        'ZONE'              => $newZone,
        'DEVISNUMCOM'       => substr($orderNo, 0, 25),
        'DEVISCONTACTCOM'   => $orderContactFullName,
        'DEVISTELEPHONECOM' => $orderContactTelephone,
        'DEVISEMAILCOM'     => $orderContactEmail
    };
    if ($db->update('DEVIS', $tabUpdates, {'DEVISAUTO*' => $estimateId}, $schemaName) == -1)
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, 0x0001);
    }

    # Abandon/Désabandon du devis
    my $tabLineOptions = {};
    if ($oldStateId ne $newStateId && ($newStateId eq STATE_ABORTED || $oldStateId eq STATE_ABORTED))
    {
        # Toutes les lignes du devis qui sont actives doivent être mises à jour (pour le transport, et déacceptation)
        my $tabLinesIdsToAbort = &getLinesList($countryId, LOC::Util::GETLIST_IDS, {'estimateId' => $estimateId,
                                                                                    'stateId'    => LINE_STATE_ACTIVE});

        if ($newStateId eq STATE_ABORTED)
        {
            $tabLineOptions->{'updateFrom'} = 'abortEstimate';
        }

        foreach my $abortLineId (@$tabLinesIdsToAbort)
        {
            # On cherche si la ligne de devis fait partie de la liste des mises à jour (update ou delete)
            my $i = 0;
            my $nb = @{$tabData->{'tabLines'}};
            my $isFound = 0;
            while ($i < $nb && !$isFound)
            {
                my $tabLineData = $tabData->{'tabLines'}->[$i];

                if ($tabLineData->{'@action'} ne 'insert' && $tabLineData->{'id'} == $abortLineId)
                {
                    # Si une mise à jour était déjà prévue, dans le cas d'un abandon, on en profite pour désaccepter la ligne
                    if ($tabLineData->{'@action'} eq 'update' && $newStateId eq STATE_ABORTED)
                    {
                        $tabLineData->{'@subAction'} = 'unaccept';
                    }
                    $isFound = 1;
                }

                $i++;
            }

            # On ajoute la ligne de modification dans le cas où elle n'était pas dans la liste des mises à jour
            if (!$isFound)
            {
                my $tabLineUpdate = {
                    '@action' => 'update',
                    'id'      => $abortLineId
                };
                if ($newStateId eq STATE_ABORTED)
                {
                    $tabLineUpdate->{'@subAction'} = 'unaccept';
                }

                push(@{$tabData->{'tabLines'}}, $tabLineUpdate);
            }

        }

    }


    # Récupération des informations sur les lignes (seules les lignes à modifier et à supprimer sont chargées)
    my @tabLinesId = ();
    foreach my $tabLineData (@{$tabData->{'tabLines'}})
    {
        if ($tabLineData->{'@action'} ne 'insert')
        {
            push(@tabLinesId, $tabLineData->{'id'});
        }
    }


    # Récupération des informations des lignes de devis
    my $tabLinesInfos = {};
    if (@tabLinesId > 0)
    {
        $tabLinesInfos = &getLinesList($countryId, LOC::Util::GETLIST_ASSOC, {'estimateId' => $estimateId, 'id' => \@tabLinesId}, GETLINEINFOS_ESTIMATE_SITE);
    }

    # Données pour l'insertion des lignes
    my $tabInsertInfos = {
        'estimate.id'                       => $estimateId,
        'estimate.customer.id'              => $customerId,
        'estimate.agency.id'                => $agencyId,
        'estimate.zone'                     => $newZone,
        'estimate.site.deliveryHour'        => $tabSiteData->{'deliveryHour'},
        'estimate.site.deliveryTimeSlot.id' => $tabSiteData->{'deliveryTimeSlot.id'},
        'estimate.state.id'                 => $oldStateId,
        'estimate.nbValidLines'             => $tabInfos->{'nbValidLines'}
    };

    # Insertion et mise à jour des lignes
    my $hasLinesErrors = 0;
    foreach my $tabLineData (@{$tabData->{'tabLines'}})
    {
        my $estimateLineId;
        my $tabLineErrors = {
            'fatal'   => [],
            'warning' => [],
            'modules' => {
                'rentTariff'      => {},
                'transportTariff' => {},
                'transport'       => {
                    'fatal' => [],
                    'modules' => {
                        'delivery' => {},
                        'recovery' => {}
                    }
                },
                'proforma'  => {},
                'days'      => {},
                'documents' => []
            }
        };

        my $isError = 0;
        if ($tabLineData->{'@action'} eq 'insert')
        {
            $estimateLineId = &_insertLine($db, $countryId, $estimateId, $tabLineData, $tabInsertInfos, $userId,
                                           $tabLineOptions, $tabLineErrors, $tabEndUpds);
            if (!$estimateLineId)
            {
                $hasLinesErrors++;
                $isError = 1;
            }
            $tabInsertInfos->{'estimate.nbValidLines'}++;
        }
        else
        {
            $estimateLineId = $tabLineData->{'id'};
            my $tabLineInfos = $tabLinesInfos->{$estimateLineId};

            # Vérification de la date de modification (accès concurentiel)
            if (defined $tabLineData->{'modificationDate'} && $tabLineData->{'modificationDate'} ne $tabLineInfos->{'modificationDate'})
            {
                push(@{$tabLineErrors->{'fatal'}}, 0x1001);
                $hasLinesErrors++;
                $isError = 1;
            }

            if (!$isError)
            {
                $tabLineInfos->{'estimate.id'}          = $estimateId;
                $tabLineInfos->{'estimate.customer.id'} = $customerId;
                $tabLineInfos->{'estimate.agency.id'}   = $agencyId;
                $tabLineInfos->{'estimate.zone'}        = $newZone;
                $tabLineInfos->{'estimate.state.id'}    = ($oldStateId ne $newStateId && $newStateId eq STATE_ABORTED ? $oldStateId : $newStateId);

                if (($tabLineData->{'@action'} eq 'update' && !&_updateLine($db, $countryId, $estimateLineId, $tabLineData,
                                                                            $tabLineInfos, $userId, $tabLineOptions,
                                                                            $tabLineErrors, $tabEndUpds)) ||
                    ($tabLineData->{'@action'} eq 'delete' && !&_deleteLine($db, $countryId, $estimateLineId, $tabLineInfos, $userId,
                                                                            $tabLineOptions, $tabLineErrors, $tabEndUpds)))
                {
                    $hasLinesErrors++;
                    $isError = 1;
                }
            }
        }
        push(@{$tabErrors->{'modules'}->{'lines'}}, $tabLineErrors);

        # Erreur fatale ?
        if ($isError)
        {
            if (&LOC::Util::in_array(0x0001, $tabLineErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, 0x0001);
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return 0;
            }
            if (&LOC::Util::in_array(0x0003, $tabLineErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, 0x0003);
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return 0;
            }
        }
    }

    if ($hasLinesErrors > 0)
    {
        $hasErrors += $hasLinesErrors;
        push(@{$tabErrors->{'fatal'}}, 0x0150);
    }

    # Mise à jour du montant total du devis
    my $tabNewInfos = &_updateAmounts($db, $countryId, $estimateId, 1);
    if ($hasErrors == 0 && !$tabNewInfos)
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, 0x0001);
    }
    my $newStateId = $tabNewInfos->{'state.id'};


    # Abandon/Désabandon du devis
    if ($oldStateId ne $newStateId && ($newStateId eq STATE_ABORTED || $oldStateId eq STATE_ABORTED))
    {
        # compte le nombre de lignes avec un suivi
        my $query = '
SELECT
    COUNT(*)
FROM DETAILSDEVIS
WHERE DETAILSDEVISOK != -1
AND DETAILSDEVISSUIVI = -1
AND DEVISAUTO = ' . $estimateId . ';';

        my $linesToCallCount = $db->fetchOne($query);

        if ($newStateId eq STATE_ABORTED)
        {
            # On abandonne toutes les pro formas de toutes les lignes
            &LOC::Proforma::abortAll($countryId, 'estimate', $estimateId, $userId, {'isForced' => 1}, {}, $tabEndUpds);

            # - Décrémentation des alertes de lignes à rappeler
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ESTIMATELINESTOCALL, -$linesToCallCount);
        }
        elsif ($oldStateId eq STATE_ABORTED)
        {
            # - Incrémentation des alertes de lignes à rappeler
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ESTIMATELINESTOCALL, $linesToCallCount);
        }
    }

    # Vérifie si il n'y a pas une maj bloquante (le montant du devis a augmenté de + du delta)
    my $newTotalAmount = $tabNewInfos->{'total'};
    if ($newTotalAmount > $oldTotalAmount)
    {
        my $updateDelta = &LOC::Characteristic::getAgencyValueByCode('UPDDELTA', $agencyId);
        if ($newTotalAmount - $oldTotalAmount > $updateDelta)
        {
            $hasLockableUpdates++; # Ceci fait parti des mises à jour bloquantes
        }
    }
    # Impossible de dépasser le delta si le client est bloqué
    if ($hasLockableUpdates > 0 && $isCustomerLocked)
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, 0x0105);
    }

    # Reblocage du client s'il était débloqué temporairement et si il y a eu des maj bloquantes
    if ($hasErrors == 0 && $hasLockableUpdates > 0 && !&_lockCustomerAfterUpdate($countryId, $tabCustomerInfos, $userId, $tabEndUpds, 'update'))
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, 0x0001);
    }


    # Si il y a des données invalides alors on arrête ici
    if ($hasErrors > 0)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    if (keys %$tabChanges > 0)
    {
        # Changements pouvant impacter le transport ? => on le lui communique
        &LOC::Transport::onExternalChanges($countryId, {
            'estimate' => {
                'id'    => $estimateId,
                'event' => 'update',
                'props' => $tabChanges
            }
        }, $userId, undef, $tabEndUpds);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }


    # Exécution des mises à jour de fin
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: updateLine
# Mise à jour d'une ligne de devis
#
# Parameters:
# string  $countryId      - Pays
# int     $estimateLineId - Id de la ligne de devis
# hashref $tabData        - Données à mettre à jour
# int     $userId         - Utilisateur responsable de la modification
# hashref $tabOptions     - Options supplémentaires (facultatif)
# hashref $tabErrors      - Tableau des erreurs
# hashref $tabEndUpds     - Mises à jour de fin
#
# Returns:
# bool -
sub updateLine
{
    my ($countryId, $estimateLineId, $tabData, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Tableau des erreurs
    $tabErrors->{'fatal'} = [];
    $tabErrors->{'modules'} = {
        'rentTariff' => {},
        'transportTariff' => {},
        'transport'       => {
            'fatal' => [],
            'modules' => {
                'delivery' => {},
                'recovery' => {}
            }
        },
        'proforma'  => {},
        'days'      => {},
        'documents' => []
    };

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }


    # Récupération des informations de la ligne
    my $tabInfos = &getLineInfos($countryId, $estimateLineId, GETLINEINFOS_ESTIMATE_SITE);
    if (!$tabInfos)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Vérification des dates de modification du devis et de la ligne (accès concurentiel)
    if (defined $tabData->{'estimate.modificationDate'} && $tabData->{'estimate.modificationDate'} ne $tabInfos->{'estimate.modificationDate'})
    {
        push(@{$tabErrors->{'fatal'}}, 0x0100);
        return 0;
    }
    if (defined $tabData->{'modificationDate'} && $tabData->{'modificationDate'} ne $tabInfos->{'modificationDate'})
    {
        push(@{$tabErrors->{'fatal'}}, 0x1001);
        return 0;
    }


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }


    # Mise à jour de la ligne
    if (!&_updateLine($db, $countryId, $estimateLineId, $tabData, $tabInfos, $userId, $tabOptions, $tabErrors, $tabEndUpds))
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Mise à jour du montant du devis
    if (!&_updateAmounts($db, $countryId, $tabInfos->{'estimate.id'}, 1))
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }


    # Exécution des mises à jour de fin
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: updateAmounts
# Mise à jour du montant total du devis
#
# Parameters:
# string           $countryId  - Pays
# int              $estimateId - Id du devis
# hashref          $tabErrors  - Tableau des erreurs
#
# Returns:
# bool -
sub updateAmounts
{
    my ($countryId, $estimateId, $tabErrors) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();


    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Mise à jour des montants du contrat
    if (!&_updateAmounts($db, $countryId, $estimateId, 0))
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    return 1;
}


# Function: updateLineAmounts
# Mise à jour du montant total sur une ligne de devis
#
# Parameters:
# string           $countryId  - Pays
# int              $contractId - Id du contrat
# hashref          $tabErrors  - Tableau des erreurs
#
# Returns:
# bool -
sub updateLineAmounts
{
    my ($countryId, $estimateLineId, $tabErrors) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();


    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Mise à jour des montants du contrat
    if (!&_updateLineAmounts($db, $countryId, $estimateLineId))
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Mise à jour des montants du contrat
    my $estimateId = &getIdByLineId($countryId, $estimateLineId);
    if (!&_updateAmounts($db, $countryId, $estimateId, 0))
    {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    return 1;
}



# Function: _deleteLine
# Suppression d'une ligne de devis
#
# Parameters:
# LOC::Db::Adapter $db             - Connexion à la base de données
# string           $countryId      - Pays
# int              $estimateLineId - Id de la ligne du devis
# hashref          $tabInfos       - Tableau des données de la ligne
# int              $userId         - Utilisateur responsable de la suppression
# hashref          $tabOptions     - Options supplémentaires (facultatif)
# hashref          $tabErrors      - Tableau des erreurs
# hashref          $tabEndUpds     - Mises à jour de fin
#
# Returns:
# bool -
sub _deleteLine
{
    my ($db, $countryId, $estimateLineId, $tabInfos, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    my $hasErrors = 0;


    # Une ligne de devis passée en contrat ne peut pas être supprimée
    if ($tabInfos->{'state.id'} eq LINE_STATE_CONVERTED)
    {
        push(@{$tabErrors->{'fatal'}}, 0x1003);
        return 0;
    }

    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Récupération des droits
    my $tabRights = &_getLineRights($tabInfos, $tabUserInfos, $tabInfos->{'proformaFlags'});

    # Impossible de modifier une ligne de devis passée en contrat ou désactivée
    if (!&_hasRight($tabRights->{'actions'}, 'remove'))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0003);
        return 0;
    }

    my $customerId = $tabInfos->{'estimate.customer.id'};
    my $estimateId = $tabInfos->{'estimate.id'};

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Suppression du tarif de location
    my $tabRentTariffErrors = $tabErrors->{'modules'}->{'rentTariff'};
    if (!&LOC::Tariff::Rent::delete($countryId, 'estimateLine', $estimateLineId, $userId, $tabRentTariffErrors))
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, LINE_ERROR_MODULE_TARIFFRENT);
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabRentTariffErrors->{'fatal'}))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Suppression des jours
    my $tabDaysErrors = $tabErrors->{'modules'}->{'days'};
    if ($hasErrors == 0 &!&LOC::Day::deleteAll($countryId, 'estimateLine', $estimateLineId, $userId, {}, $tabDaysErrors, $tabEndUpds))
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, LINE_ERROR_MODULE_DAYS);

        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabDaysErrors->{'fatal'}))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Suppression du tarif transport
    my $tabTrspTariffErrors = $tabErrors->{'modules'}->{'transportTariff'};
    if ($hasErrors == 0 && !&LOC::Tariff::Transport::delete($countryId, 'estimateLine', $estimateLineId, $userId, $tabTrspTariffErrors))
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, LINE_ERROR_MODULE_TARIFFTRSP);
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabTrspTariffErrors->{'fatal'}))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Suppression des pro formas
    my $tabProformaErrors = $tabErrors->{'modules'}->{'proforma'};
    if ($hasErrors == 0 && !&LOC::Proforma::deleteAll($countryId, 'estimateLine', $estimateLineId, $userId, $tabProformaErrors, $tabEndUpds))
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, LINE_ERROR_MODULE_PROFORMA);
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabProformaErrors->{'fatal'}))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Suppression des documents
    my $tabDocumentsErrors = {};
    if ($hasErrors == 0 && !&LOC::Attachments::deleteAll($countryId, LOC::Attachments::MODULE_RENTESTIMATELINEDOCUMENT, $estimateLineId, $userId, {}, $tabDocumentsErrors, $tabEndUpds))
    {
        $hasErrors++;
        push(@{$tabErrors->{'warning'}}, LINE_ERROR_MODULE_DOCUMENTS);
    }

    # Suppression des transports
    my $tabTransportErrors = $tabErrors->{'modules'}->{'transport'};
    if ($hasErrors == 0 && !&LOC::Transport::deleteAllForEstimateLine($countryId, $estimateLineId, $userId, $tabTransportErrors, $tabEndUpds))
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, LINE_ERROR_MODULE_TRANSPORT);
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabTransportErrors->{'fatal'}))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Suppression des services
    if ($hasErrors == 0 && !&LOC::Estimate::Rent::_deleteLineRentServices($countryId, $estimateLineId, $userId, {}, {}))
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Suppression de la ligne
    if ($hasErrors == 0 && $db->delete('DETAILSDEVIS', {'DETAILSDEVISAUTO*' => $estimateLineId}, $schemaName) == -1)
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    if ($hasErrors > 0)
    {
        return 0;
    }

    # Mise à jour des alertes
    # - Décrémentation de l'alerte des lignes de devis acceptées
    if ($tabInfos->{'isAccepted'})
    {
        &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'estimate.agency.id'}, LOC::Alert::ACCESPTEDESTIMATELINES, -1);
    }
    # - Décrémentation de l'alerte des lignes de devis à rappeler
    elsif ($tabInfos->{'isFollowed'})
    {
        &LOC::EndUpdates::updateAlert($tabEndUpds, $tabInfos->{'estimate.agency.id'}, LOC::Alert::ESTIMATELINESTOCALL, -1);
    }

    return 1;
}

# Function: _deleteLineRentServices
# Suppression de tous les services de location/transport d'une ligne de devis
#
# Parameters:
# string   $countryId      - Pays
# int      $estimateLineId - Id de la ligne de devis
# int      $userId         - Id de l'utilisateur responsable
# hashref  $tabErrors      - Tableau d'erreurs
# hashref  $tabEndUpds     - Mises à jour de fin
#
# Returns:
# bool
sub _deleteLineRentServices
{
    my ($countryId, $estimateLineId, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Suppression des services de location
    if ($db->delete('f_rentestimateline_rentservice', {'res_rel_id*' => $estimateLineId}, $schemaName) == -1)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);

        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}

# Function: _deleteLineRentServices
# Suppression de tous les services de location/transport d'une ligne de devis
#
# Parameters:
# string   $countryId      - Pays
# int      $estimateLineId - Id de la ligne de devis
# int      $userId         - Id de l'utilisateur responsable
# hashref  $tabErrors      - Tableau d'erreurs
# hashref  $tabEndUpds     - Mises à jour de fin
#
# Returns:
# bool
sub _deleteLineRentServices
{
    my ($countryId, $estimateLineId, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Suppression des services de location
    if ($db->delete('f_rentestimateline_rentservice', {'res_rel_id*' => $estimateLineId}, $schemaName) == -1)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);

        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}

# Function: _execDocumentsActions
# Exécuter les actions sur les documents du contrat
#
# Parameters:
# string  $countryId      - Pays
# int     $contractId     - Identifiant du contrat
# arrayref $tabActions    - Paramètre des actions
# int     $userId         - Id de l'utilisateur
# hashref $tabErrors      - Tableau des erreurs
# hashref $tabEndUpds     - Mises à jour de fin
#
# Returns:
# bool -
sub _execDocumentsActions
{
    my ($countryId, $estimateLineId, $tabActions, $userId, $tabActionsErrors, $tabEndUpds) = @_;

    my $result = 1;

    foreach my $tabAction (@$tabActions)
    {
        my ($docType, $docId) = split(':', $tabAction->{'id'});
        my $tabErrors = {};
        my @tabFatalErrors = ();

        # Actions sur les "attachments"
        if ($tabAction->{'type'} eq 'remove')
        {
            if ($docType eq 'att')
            {
                if (!&LOC::Attachments::delete($countryId, $docId,
                                               $userId, {}, $tabErrors, $tabEndUpds))
                {
                    $result = 0;
                }
            }
            else
            {
                $result = 0;
                push(@tabFatalErrors, 0x0001);
            }
        }
        elsif ($tabAction->{'type'} eq 'edit')
        {
            if ($docType eq 'att')
            {
                my $tabData = {
                    'description' => $tabAction->{'data'}->{'description'},
                    'title'       => $tabAction->{'data'}->{'title'}
                };
                if (!&LOC::Attachments::update($countryId, $docId,
                                               $tabData,
                                               $userId, {}, $tabErrors, $tabEndUpds))
                {
                    $result = 0;
                }
            }
            else
            {
                $result = 0;
                push(@tabFatalErrors, 0x0001);
            }
        }
        elsif ($tabAction->{'type'} eq 'add')
        {
            my $tabData = {
                'fileName'    => $tabAction->{'data'}->{'fileName'},
                'description' => $tabAction->{'data'}->{'description'},
                'title'       => $tabAction->{'data'}->{'title'},
                'filePath'    => &LOC::Util::getCommonFilesPath('temp/') . $tabAction->{'data'}->{'tempName'}
            };
            if (!&LOC::Attachments::insert($countryId, LOC::Attachments::MODULE_RENTESTIMATELINEDOCUMENT,
                                           $estimateLineId, $tabData,
                                           $userId, {}, $tabErrors, $tabEndUpds))
            {
                $result = 0;
            }
        }
        elsif ($tabAction->{'type'} eq 'duplicate')
        {
            if (!&LOC::Attachments::duplicate($countryId, $docId,
                                               LOC::Attachments::MODULE_RENTESTIMATELINEDOCUMENT,
                                               $estimateLineId,
                                               $userId, {
                                                   'title'       => $tabAction->{'data'}->{'title'},
                                                   'description' => $tabAction->{'data'}->{'description'}
                                               }, $tabErrors, $tabEndUpds))
            {
                $result = 0;
            }
        }

        # Toutes les erreurs des documents sont gérées comme des warning pour le contrat
        push(@$tabActionsErrors, {
            'fatal'   => \@tabFatalErrors,
            'warning' => $tabErrors->{'fatal'}
        });
    }

    return $result;
}


# Function: _execDaysActions
# Exécuter l'action sur les jours+/- de la ligne de devis
#
# Parameters:
# string  $countryId      - Pays
# int     $estimateLineId - Identifiant de la ligne de devis
# arrayref $tabActions    - Paramètre des actions
# int     $userId         - Id de l'utilisateur
# hashref $tabErrors      - Tableau des erreurs
# hashref $tabEndUpds     - Mises à jour de fin
#
# Returns:
# bool -
sub _execDaysActions
{
    my ($countryId, $estimateLineId, $tabActions, $userId, $tabOptions, $tabDaysErrors, $tabEndUpds) = @_;

    foreach my $tabAction (@$tabActions)
    {
        if ($tabAction->{'type'} eq 'add')
        {
            if (!&LOC::Day::insert($countryId, 'estimateLine', $estimateLineId, $tabAction->{'data'}, $userId, $tabOptions, $tabDaysErrors, $tabEndUpds))
            {
                return 0;
            }
        }
        elsif ($tabAction->{'type'} eq 'edit')
        {
            if (!&LOC::Day::update($countryId, $tabAction->{'id'}, $tabAction->{'data'}, $userId, $tabOptions, $tabDaysErrors, $tabEndUpds))
            {
                return 0;
            }
        }
        elsif ($tabAction->{'type'} eq 'remove')
        {
            if (!&LOC::Day::delete($countryId, $tabAction->{'id'}, $userId, $tabOptions, $tabDaysErrors, $tabEndUpds))
            {
                return 0;
            }
        }
        elsif ($tabAction->{'type'} eq 'setNonInvoiceable')
        {
            if (!&LOC::Day::setInvoiceable($countryId, $tabAction->{'id'}, 0, $userId, $tabOptions, $tabDaysErrors, $tabEndUpds))
            {
                return 0;
            }
        }
        elsif ($tabAction->{'type'} eq 'setInvoiceable')
        {
            if (!&LOC::Day::setInvoiceable($countryId, $tabAction->{'id'}, 1, $userId, $tabOptions, $tabDaysErrors, $tabEndUpds))
            {
                return 0;
            }
        }
    }

    return 1;
}


# Function: _execProformaAction
# Exécuter l'action sur les pro formas d'une ligne de devis
#
# Parameters:
# string  $countryId      - Pays
# int     $estimateLineId - Identifiant de la ligne de devis
# hashref $tabAction      - Paramètre de l'action
# int     $userId         - Id de l'utilisateur
# hashref $tabErrors      - Tableau des erreurs
# hashref $tabEndUpds     - Mises à jour de fin
#
# Returns:
# string - Etat correct du contrat
sub _execProformaAction
{
    my ($countryId, $estimateLineId, $tabAction, $userId, $tabErrors, $tabEndUpds) = @_;

    my $result = 0;
    if ($tabAction->{'type'} eq 'add')
    {
        $result = &LOC::Proforma::add($countryId, 'estimateLine', $estimateLineId, $tabAction->{'tab'}, $userId, $tabErrors, $tabEndUpds);
    }
    elsif ($tabAction->{'type'} eq 'edit')
    {
        $result = &LOC::Proforma::edit($countryId, $tabAction->{'id'}, $tabAction->{'tab'}, $userId, $tabErrors, $tabEndUpds);
    }
    elsif ($tabAction->{'type'} eq 'reactivate')
    {
        $result = &LOC::Proforma::reactivate($countryId, $tabAction->{'id'}, $userId, $tabErrors, $tabEndUpds);
    }
    elsif ($tabAction->{'type'} eq 'abort')
    {
        $result = &LOC::Proforma::abort($countryId, $tabAction->{'id'}, $userId, $tabErrors, $tabEndUpds);
    }

    return $result;
}


# Function: _expireProformas
# Vérifie s'il faut passer des pro formas en non à jour si des changements impactant sont intervenus sur la ligne de devis
#
# Parameters:
#
# LOC::Db::Adapter $db             - Connexion à la base de données
# string           $countryId      - Pays
# int              $estimateLineId - Id de la ligne de devis
# int              $userId         - Id de l'utilisateur
# hashref          $tabEndUpds     - Mises à jour de fin
#
sub _expireProformas
{
    my ($countryId, $estimateLineId, $userId, $tabEndUpds) = @_;

    # Liste des traces
    my $tabTraces = &LOC::EndUpdates::getTraces($tabEndUpds, 'estimateLine', $estimateLineId);

    my $tabImpactAmount = [
        # Jours supplémentaires
        LOC::Log::EventType::ADDADDEDDAYS,
        LOC::Log::EventType::UPDADDEDDAY,
        LOC::Log::EventType::DELADDEDDAY,
        LOC::Log::EventType::INVOICEADDEDDAY,
        LOC::Log::EventType::UNINVOICEADDEDDAY,
        # Jours déduits
        LOC::Log::EventType::ADDDEDUCTEDDAYS,
        LOC::Log::EventType::UPDDEDUCTEDDAY,
        LOC::Log::EventType::DELDEDUCTEDDAY,
        LOC::Log::EventType::INVOICEDEDUCTEDDAY,
        LOC::Log::EventType::UNINVOICEDEDUCTEDDAY,
        # Durée
        LOC::Log::EventType::UPDDURATION,
        LOC::Log::EventType::UPDCALENDARDURATION,
        # Prix de location
        LOC::Log::EventType::UPDRENTAMOUNT,
        # Transport
        LOC::Log::EventType::UPDDELIVERYAMOUNT,
        LOC::Log::EventType::UPDRECOVERYAMOUNT,
        # Autres services
        LOC::Log::EventType::UPDINSURANCE,
        LOC::Log::EventType::UPDCLEANING,
        LOC::Log::EventType::UPDRESIDUESMODE,
        LOC::Log::EventType::UPDAPPEAL
    ];
    my $tabImpactRentServices = [
        LOC::Log::EventType::ADDRENTSERVICE,
        LOC::Log::EventType::DELRENTSERVICE,
        LOC::Log::EventType::UPDRENTSERVICE
    ];
    my $tabImpactRentServices = ['AJSLT', 'SUSLT', 'CHSLT'];

    # Liste des groupes modifiables sans expiration
    my $tabGroups = [
        {
            'name' => 'modelOrPeriod',
            'traces' => [
                LOC::Log::EventType::CHANGEMODEL,
                LOC::Log::EventType::UPDBEGINDATE,
                LOC::Log::EventType::UPDENDDATE
            ],
            'count' => 0},
        {
            'name' => 'rentServices',
            'traces' => $tabImpactRentServices,
            'count' => 0
        }
    ];


    my $countImpactAmount = 0;
    my $diffRentServices  = 0;
    foreach my $lineUpd (@{$tabTraces})
    {
        if (&LOC::Util::in_array($lineUpd->{'code'}, $tabImpactAmount))
        {
            if ($lineUpd->{'code'} eq LOC::Log::EventType::UPDRENTAMOUNT && $lineUpd->{'old'}->{'amount'} == $lineUpd->{'new'}->{'amount'})
            {
                next;
            }
            if (&LOC::Util::in_array($lineUpd->{'code'}, [LOC::Log::EventType::UPDADDEDDAY, LOC::Log::EventType::UPDDEDUCTEDDAY]))
            {
                my $oldInvoiceDuration = &LOC::Day::InvoiceDuration::getInfos($countryId, $lineUpd->{'old'}->{'invoiceDurationId'});
                my $newInvoiceDuration = &LOC::Day::InvoiceDuration::getInfos($countryId, $lineUpd->{'new'}->{'invoiceDurationId'});

                if ($oldInvoiceDuration == $newInvoiceDuration)
                {
                    next;
                }
            }
            $countImpactAmount++;
        }
        # Impacts sur le montant des services complémentaires
        if (&LOC::Util::in_array($lineUpd->{'code'}, $tabImpactRentServices))
        {
            $diffRentServices += (defined $lineUpd->{'new'} ? $lineUpd->{'new'}->{'amount'} : 0) -
                                 (defined $lineUpd->{'old'} ? $lineUpd->{'old'}->{'amount'} : 0);
        }

        # Impacts sur les différents groupes
        foreach my $tabGroup (@$tabGroups)
        {
            if (&LOC::Util::in_array($lineUpd->{'code'}, $tabGroup->{'traces'}))
            {
                $tabGroup->{'count'}++;
            }
        }
    }

    # changement de modèle/durée sans changement de tarif et sans aucun autre changement
    if ($countImpactAmount == 0 && $diffRentServices == 0)
    {
        # Mise à jour des groupes détectés
        my $tabGroupsToUpdate = [];
        foreach my $tabGroup (@$tabGroups)
        {
            if ($tabGroup->{'count'} > 0)
            {
                push(@$tabGroupsToUpdate, $tabGroup->{'name'});
            }
        }

        my $tabFilters = {
            'stateId'   => [LOC::Proforma::STATE_ACTIVE, LOC::Proforma::STATE_ABORTED],
            'isExpired' => 0
        };
        if (@$tabGroupsToUpdate > 0 && !&LOC::Proforma::updateGroupLines($countryId, 'estimateLine', $estimateLineId, $tabFilters, $tabGroupsToUpdate, {}))
        {
            return 0;
        }
    }
    else
    {
        if (!&LOC::Proforma::expireAll($countryId, 'estimateLine', $estimateLineId, $userId, {}, $tabEndUpds))
        {
            return 0;
        }
    }

    return 1;
}


# Function: _getCorrectStateId
# Récupérer l'état correct du devis
#
# Parameters:
# string   $currentStateId - Etat courant du devis
# arrayref $tabLinesStates - Liste des états des lignes du devis
#
# Returns:
# string - Etat correct du contrat
sub _getCorrectStateId
{
    my ($currentStateId, $tabLinesStates) = @_;

    # Si l'état du devis est "Contrat" alors on ne fait rien
    if ($currentStateId eq STATE_CONTRACT)
    {
        return STATE_CONTRACT;
    }

    my $tabLinesCountByState = &_linesCountByState($tabLinesStates);

    # On s'assure que l'état du devis est correct
    if ($tabLinesCountByState->{&LINE_STATE_CONVERTED} > 0)
    {
        # Force l'état à "Contrat" si il y a au moins une ligne passée en contrat
        return STATE_CONTRACT;
    }
    elsif ($currentStateId ne STATE_ABORTED && $tabLinesCountByState->{&LINE_STATE_INACTIVE} == @$tabLinesStates)
    {
        # Le devis ne peut pas être accepté si il n'y a que des lignes désactivées
        return STATE_ACTIVE;
    }
    return $currentStateId;
}


# Function: _linesCountByState
# Récupérer l'état correct du devis
#
# Parameters:
# arrayref $tabLinesStates - Liste des états des lignes du devis
#
# Returns:
# string - Etat correct du contrat
sub _linesCountByState
{
    my ($tabLinesStates) = @_;

    my @tabStates;
    if (ref($tabLinesStates) eq 'HASH')
    {
        @tabStates = values %$tabLinesStates;
    }
    else
    {
        @tabStates = @$tabLinesStates;
    }

    my $tabLinesCountByStates = {
        LINE_STATE_ACTIVE()    => 0,
        LINE_STATE_CONVERTED() => 0,
        LINE_STATE_INACTIVE()  => 0
    };
    my $nbLines = @tabStates;
    for (my $i = 0; $i < $nbLines; $i++)
    {
        $tabLinesCountByStates->{@tabStates[$i]}++;
    }
    return $tabLinesCountByStates;
}


# Function: _getLineRights
# Récupérer les droits sur une ligne de devis pour un utilisateur donné
#
# Parameters:
# hashref $tabEstimateInfos - Informations sur le contrat
# hashref $tabUserInfos     - Informations sur l'utilisateur
# int     $proformaFlags    - Flags de la pro forma
#
# Returns:
# hashref - Tableau des droits
sub _getLineRights
{
    my ($tabEstimateLineInfos, $tabUserInfos, $proformaFlags) = @_;

    my $stateId         = $tabEstimateLineInfos->{'state.id'};
    my $estimateStateId = $tabEstimateLineInfos->{'estimate.state.id'};
    my $agencyId        = $tabEstimateLineInfos->{'estimate.agency.id'};

    my $isUserAdmin  = $tabUserInfos->{'isAdmin'};
    my $isUserSuperv = $tabUserInfos->{'isSuperv'};
    my $isUserChefAg = &LOC::Util::in_array(LOC::User::GROUP_AGENCYMANAGER, $tabUserInfos->{'tabGroups'});
    my $isUserComm   = &LOC::Util::in_array(LOC::User::GROUP_COMMERCIAL, $tabUserInfos->{'tabGroups'});
    my $isUserCompta = &LOC::Util::in_array(LOC::User::GROUP_ACCOUNTING, $tabUserInfos->{'tabGroups'});

    my $tabRights = {
        'machine'    => '',
        'duration'   => '',
        'rentTariff' => '',
        'transport'  => '',
        'services'   => '',
        'proforma'   => '',
        'documents'  => '',
        'actions'    => ''
    };

    # Vérifie si l'agence de l'utilisateur est identique
    if (!$isUserAdmin)
    {
        if (!exists $tabUserInfos->{'tabAuthorizedAgencies'}->{$agencyId})
        {
            return $tabRights;
        }
        if (!$isUserSuperv && !$isUserChefAg && !$isUserComm)
        {
            if ($isUserCompta)
            {
                $tabRights->{'proforma'} = 'print|forcePrint';
                $tabRights->{'actions'}  = 'valorizationPrint';
            }
            return $tabRights;
        }
    }


    if ($estimateStateId eq STATE_ABORTED)
    {
        if ($stateId eq LINE_STATE_CONVERTED)
        {
            # Droits sur les boutons d'actions
            $tabRights->{'actions'} = 'viewContract|valorizationPrint';
        }
    }
    else
    {
        if ($stateId eq LINE_STATE_ACTIVE)
        {
            # Droits sur la partie onglet Pro forma
            $tabRights->{'proforma'} = 'add|abort|reactivate|edit|print';

            # Droits sur la partie onglet Documents
            $tabRights->{'documents'} = 'add|edit|remove';

            # Droits sur la partie onglet Transport
            $tabRights->{'transport'} = 'estimation|amount|addMachine|self|delegate|transferToAgency';

            # Impossible de modifier une ligne devis ayant une pro forma active et finalisée
            if ($proformaFlags & LOC::Proforma::DOCFLAG_EXISTACTIVEDFINALIZED)
            {
                # Droits sur les boutons d'actions
                $tabRights->{'actions'} = 'valid|duplicate|reload|accept|convert';
            }
            else
            {
                # Droits sur la partie onglet Matériel
                $tabRights->{'machine'} = 'model|isImperativeModel';

                # Droits sur la partie onglet Durée
                $tabRights->{'duration'} = 'beginDate|endDate|additionalDays|deductedDays|datesToConfirm';

                # Droits sur la partie onglet Location
                $tabRights->{'rentTariff'} = 'amountType|reduc';

                # Droits sur la partie onglet Autres services
                $tabRights->{'services'} = 'insurance|appeal|cleaning|residues|addRentServices|editRentServices';

                # Droits sur les boutons d'actions
                $tabRights->{'actions'} = 'valid|remove|duplicate|reload|convert|valorizationPrint|accept';
            }
        }
        elsif ($stateId eq LINE_STATE_CONVERTED)
        {
            # Droits sur les boutons d'actions
            $tabRights->{'actions'} = 'duplicate|reload|viewContract|valorizationPrint';
        }
        else
        {
            # Droits sur les boutons d'actions
            $tabRights->{'actions'} = 'duplicate|reload';
        }
    }

    # Administrateurs
    if ($isUserAdmin)
    {
        $tabRights->{'proforma'} .= '|forcePrint';
    }

    return $tabRights;
}


# Function: _getRights
# Récupérer les droits sur un devis pour un utilisateur donné
#
# Parameters:
# hashref $tabEstimateInfos - Informations sur le contrat
# hashref $tabUserInfos     - Informations sur l'utilisateur
#
# Returns:
# hashref - Tableau des droits
sub _getRights
{
    my ($tabEstimateInfos, $tabUserInfos) = @_;

    my $stateId      = $tabEstimateInfos->{'state.id'};
    my $agencyId     = $tabEstimateInfos->{'agency.id'};

    my $isUserAdmin  = $tabUserInfos->{'isAdmin'};
    my $isUserSuperv = $tabUserInfos->{'isSuperv'};
    my $isUserChefAg = &LOC::Util::in_array(LOC::User::GROUP_AGENCYMANAGER, $tabUserInfos->{'tabGroups'});
    my $isUserComm   = &LOC::Util::in_array(LOC::User::GROUP_COMMERCIAL, $tabUserInfos->{'tabGroups'});
    my $isUserCompta = &LOC::Util::in_array(LOC::User::GROUP_ACCOUNTING, $tabUserInfos->{'tabGroups'});
    my $isUserTransp = &LOC::Util::in_array(LOC::User::GROUP_TRANSPORT, $tabUserInfos->{'tabGroups'});

    my $tabRights = {
        'infos'           => '',
        'customer'        => '',
        'site'            => '',
        'infoscpl'        => '',
        'abort'           => '',
        'actions'         => ''
    };

    # Vérifie si l'agence de l'utilisateur est identique
    if (!$isUserAdmin)
    {
        if (!exists $tabUserInfos->{'tabAuthorizedAgencies'}->{$agencyId})
        {
            return $tabRights;
        }
        if (!$isUserSuperv && !$isUserChefAg && !$isUserComm)
        {
            if ($isUserTransp)
            {
                $tabRights->{'site'} = 'label|address|locality|contact|activateAsbestos|hours|wharf|anticipated|comments';
                $tabRights->{'actions'} = 'valid|cancel';
            }
            return $tabRights;
        }
    }

    # Droits sur la partie Information
    $tabRights->{'infos'} = 'negotiator';
    if ($stateId eq STATE_ACTIVE)
    {
        $tabRights->{'infos'} .= '|isFullService';
    }

    # Droits sur la partie Client
    $tabRights->{'customer'} = 'new|history|name|orderNo|orderContact';

    # Droits sur la partie Chantier
    $tabRights->{'site'} = 'label|address|locality|contact|activateAsbestos|hours|wharf|anticipated|comments';
    if ($isUserAdmin)
    {
        $tabRights->{'site'} .= '|deactivateAsbestos';
    }

    # Droits sur la partie Informations complémentaires
    $tabRights->{'infoscpl'} = 'comments|tracking';

    # Droits sur la partie Abandon de devis
    if ($stateId eq STATE_ACTIVE || $stateId eq STATE_ABORTED)
    {
        $tabRights->{'abort'} = 'reason|comments';
    }

    # Droits sur les boutons d'actions
    $tabRights->{'actions'} = 'valid|cancel|accept';
    if ($stateId ne STATE_ABORTED)
    {
        $tabRights->{'actions'} .= '|print|addLine|acceptAll';
    }

    return $tabRights;
}

# Function: _getValueForLineStateId
# Récupérer la valeur correspondant à l'état d'une ligne de devis
#
# Parameters:
# string $stateId  - État de la ligne de devis
#
# Returns: int
sub _getValueForLineStateId
{
    my ($stateId) = @_;

    my $value = 0;

    if ($stateId eq LINE_STATE_ACTIVE)
    {
        $value = 0;
    }
    elsif ($stateId eq LINE_STATE_CONVERTED)
    {
        $value = -1;
    }
    elsif ($stateId eq LINE_STATE_INACTIVE)
    {
        $value = -2;
    }

    return $value;
}


# Function: _hasRight
# Vérifier le droit à un champ
#
# Parameters:
# hashref $tab  - Tableau des droits
# string  $name - Nom du droit
#
# Returns:
# bool
sub _hasRight
{
    my ($tab, $name) = @_;

    return (ref($tab) eq 'HASH' ? ($tab->{$name} ? 1 : 0) : (index('|' . $tab . '|', '|' . $name . '|') == -1 ? 0 : 1));
}


# Function: _insertLine
# Ajout d'une ligne de devis
#
# Parameters:
# LOC::Db::Adapter $db            - Connexion à la base de données
# string           $countryId     - Pays
# int              $estimateId    - Id de la ligne du devis
# hashref          $tabData       - Données à mettre à jour
# hashref          $tabInfos      - Tableau des données pour l'insertion
# int              $userId        - Utilisateur responsable de l'ajout
# hashref          $tabOptions    - Options supplémentaires (facultatif)
# hashref          $tabErrors     - Tableau des erreurs
# hashref          $tabEndUpds    - Mises à jour de fin
#
# Returns:
# bool -
sub _insertLine
{
    my ($db, $countryId, $estimateId, $tabData, $tabInfos, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    my $hasErrors = 0;


    # Vérifier le nombre de lignes du devis
    if ($tabInfos->{'estimate.nbValidLines'} >= MAXLINESCOUNT)
    {
        push(@{$tabErrors->{'fatal'}}, 0x1006);
        return 0;
    }

    # Récupération des droits
    my $tabRights = &getRights($countryId, $tabInfos->{'estimate.agency.id'}, undef, $userId);

    # Vérifie que l'utilisateur ait bien le droit de créer une ligne
    if (!&_hasRight($tabRights->{'actions'}, 'addLine'))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0003);
        return 0;
    }


    my $customerId = $tabInfos->{'estimate.customer.id'};
    my $agencyId   = $tabInfos->{'estimate.agency.id'};
    my $zone       = $tabInfos->{'estimate.zone'};

    my $modelId    = (defined $tabData->{'model.id'} ? $tabData->{'model.id'} : 0);
    my $quantity   = 1;

    my $isImperativeModel = (exists $tabData->{'isImperativeModel'} ? $tabData->{'isImperativeModel'} : 0);

    my $isAccepted = (defined $tabData->{'isAccepted'} ? $tabData->{'isAccepted'} : 0);
    my $isFollowed = 1;
    my $isDatesToConfirm = (defined $tabData->{'isDatesToConfirm'} ? $tabData->{'isDatesToConfirm'} : 0);

    my $beginDate  = (defined $tabData->{'beginDate'} ? $tabData->{'beginDate'} : '');
    my $endDate     = (exists $tabData->{'endDate'} ? $tabData->{'endDate'} : '');
    if ($beginDate eq '' || !&LOC::Date::checkMySQLDate($beginDate))
    {
        $beginDate = '';
    }
    if ($endDate eq '' || !&LOC::Date::checkMySQLDate($endDate))
    {
        $endDate = '';
    }
    # - Jours plus et jours moins
    my $tabDaysActions  = (exists $tabData->{'tabDaysActions'} ? $tabData->{'tabDaysActions'} : []);
    my $tabDiffDaysDuration = &LOC::Common::Rent::calculateDiffDaysByActions($countryId, $tabDaysActions);
    my $additionalDays = $tabDiffDaysDuration->{&LOC::Day::TYPE_ADDITIONAL};
    my $deductedDays   = $tabDiffDaysDuration->{&LOC::Day::TYPE_DEDUCTED};

    # - Calcul des jours ouvrés de location
    my $realDuration = &LOC::Date::getRentDuration($beginDate, $endDate, $agencyId);
    # - Calcul de la durée de location
    my $duration = $realDuration + ($additionalDays - $deductedDays);


    # Services de location et de transport
    # - Récupération du montant minimum des services location et transport pour l'utilisateur connecté
    my $rentServiceMinAmount = &LOC::Characteristic::getAgencyValueByCode('MTMINSRV', $agencyId);
    my $rentServiceNoLimitUserIdList = &LOC::Characteristic::getAgencyValueByCode('USRNOMAXREMSRV', $agencyId);
    my @rentServiceNoLimitUserId = split(',', $rentServiceNoLimitUserIdList);
    # - Si l'utilisateur courant n'a pas de limite de services location et transport
    if (grep {$_ eq $userId} @rentServiceNoLimitUserId)
    {
        $rentServiceMinAmount = undef;
    }
    my $tabRentServices = (exists $tabData->{'tabRentServices'} ? $tabData->{'tabRentServices'} : []);


    # Sous actions
    my $subAction = $tabData->{'@subAction'};
    if ($subAction ne '')
    {
        if ($subAction eq 'accept') # Accepter
        {
            $isAccepted = 1;
        }
        elsif ($subAction eq 'unaccept') # Ne plus accepter
        {
            $isAccepted = 0;
        }
        elsif ($subAction eq 'follow') # Suivre
        {
            $isFollowed = 1;
        }
        elsif ($subAction eq 'unfollow') # Ne plus suivre
        {
            $isAccepted = 0;
        }
    }


    # Vérification des données

    # - Vérifie que le modèle de machine est renseigné
    if ($modelId == 0)
    {
        push(@{$tabErrors->{'fatal'}}, 0x1000);
    }
    else
    {
        # Les dates de début et de fin sont obligatoires
        if ($beginDate eq '' || $endDate eq '')
        {
            push(@{$tabErrors->{'fatal'}}, 0x1005);
        }
        else
        {
            if ($duration eq '' || $duration <= 0)
            {
                push(@{$tabErrors->{'fatal'}}, 0x1002);
            }
            if ($endDate lt $beginDate)
            {
                # La date de fin est inférieure à la date de début
                push(@{$tabErrors->{'fatal'}}, 0x1008);
            }

            my %tabCreationDate = &LOC::Date::getHashDate();
            my ($tYear, $tMonth, $tDay) = &Date::Calc::Add_Delta_YM($tabCreationDate{'y'},
                                                                    $tabCreationDate{'m'},
                                                                    $tabCreationDate{'d'},
                                                                    0,
                                                                    -1);
            my $creationDateLess1Month = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE,
                                                                 ('y' => $tYear, 'm' => $tMonth, 'd' => $tDay));
            if ($beginDate le $creationDateLess1Month)
            {
                # La date de début ne doit pas être inférieure à 1 mois
                push(@{$tabErrors->{'fatal'}}, 0x1007);
            }

        }
    }

    # - Vérifie que l'utilisateur peut accepter une ligne
    if (!&_hasRight($tabRights->{'actions'}, 'accept'))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0003);
        return 0;
    }

    # - Vérifie que l'horaire est renseigné si on accepte la ligne
    if ($isAccepted)
    {
        if (!$tabInfos->{'estimate.site.deliveryHour'} && !$tabInfos->{'estimate.site.deliveryTimeSlot.id'})
        {
            # On ne peut pas accepter une ligne si l'heure de livraison n'est pas renseignée
            push(@{$tabErrors->{'fatal'}}, 0x1009);
        }
    }

    # Si erreur de vérification de données alors on arrête !!
    if (@{$tabErrors->{'fatal'}} > 0)
    {
        return 0;
    }

    # Paramètres pour la mise à jour du tarif de location
    my $tabRentTariffDatas = $tabData->{'tabRentTariff'};
    # Optimisations location
    $tabRentTariffDatas->{'estimateLine.estimate.id'}          = $estimateId;
    $tabRentTariffDatas->{'estimateLine.estimate.customer.id'} = $customerId;
    $tabRentTariffDatas->{'estimateLine.estimate.agency.id'}   = $agencyId;
    $tabRentTariffDatas->{'estimateLine.model.id'}             = $modelId;
    $tabRentTariffDatas->{'estimateLine.quantity'}             = $quantity;
    $tabRentTariffDatas->{'editMode'}                          = 'create';
    # Vérification du nombre de jours (en cas)
    if ($tabRentTariffDatas->{'days'} != $duration)
    {
        push(@{$tabErrors->{'fatal'}}, 0x01004);
        return 0;
    }

    # Paramètres pour la mise à jour du tarif transport
    my $tabTrspTariffDatas = $tabData->{'tabTransport'}->{'tabTariff'};
    # Optimisations transport
    $tabTrspTariffDatas->{'estimateLine.estimate.id'}          = $estimateId;
    $tabTrspTariffDatas->{'estimateLine.estimate.customer.id'} = $customerId;
    $tabTrspTariffDatas->{'estimateLine.estimate.agency.id'}   = $agencyId;
    $tabTrspTariffDatas->{'estimateLine.estimate.zone'}        = $zone;
    $tabTrspTariffDatas->{'editMode'}                          = 'create';

    # Paramètres pour l'enregistrement de la proforma
    my $tabProformaAction = $tabData->{'tabProformaAction'};

    # Paramètres pour l'enregistrement des documents
    my $tabDocumentsActions = $tabData->{'tabDocumentsActions'};

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);


    my $tabUpdates = {
        'DEVISAUTO'          => $estimateId,
        'MOMAAUTO'           => $modelId,
        'DETAILSDEVISQTE'    => $quantity,
        'DETAILSDEVISMODIMP' => ($isImperativeModel ? -1 : 0),

        'DETAILSDEVISDATE'        => $beginDate,
        'DETAILSDEVISDATEFIN'     => $endDate,
        'DETAILSDEVISDUREE'       => $duration,
        'DETAILSDEVISJOURSPLUS'   => $additionalDays,
        'DETAILSDEVISJOURSMOINS'  => $deductedDays,

        'DETAILSDEVISOK'   => 0,

        'DETAILSDEVISDATECREATION*' => 'Now()',

        'DETAILSDEVISPXJOURSFR' => 0,
        'DETAILSDEVISPXJOUREU'  => 0,
        'DETAILSDEVISMONTANTFR' => 0,
        'DETAILSDEVISMONTANTEU' => 0,
        'DETAILSDEVISHORSPARC'  => 0,
        'DETAILSDEVISREMISE'    => 0,
        'DETAILSDEVISREMISEEX'  => 0,
        'DETAILSDEVISREMISEOK'  => 0,
        'DETAILSDEVISMONTANTEU' => 0,

        'DETAILSDEVISPROFORMAOK' => 0,

        'DETAILSDEVISACCEPTE'   => ($isAccepted ? -1 : 0),
        'DETAILSDEVISSUIVI'     => ($isFollowed ? -1 : 0),
        'DETAILSDEVISDATESACONFIRMER' => ($isDatesToConfirm ? -1 : 0)
    };

    # Insertion dans la table DETAILSDEVIS
    my $estimateLineId = $db->insert('DETAILSDEVIS', $tabUpdates, $schemaName);
    if ($estimateLineId == -1)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Enregistrement du tarif de location
    my $tabRentTariffErrors = $tabErrors->{'modules'}->{'rentTariff'};
    if (!&LOC::Tariff::Rent::save($countryId, 'estimateLine', $estimateLineId, $tabRentTariffDatas, $userId, $tabRentTariffErrors, undef))
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, LINE_ERROR_MODULE_TARIFFRENT);
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabRentTariffErrors->{'fatal'}))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Enregistrement de l'estimation transport
    my $tabTrspTariffErrors = $tabErrors->{'modules'}->{'transportTariff'};
    if (!&LOC::Tariff::Transport::save($countryId, 'estimateLine', $estimateLineId, $tabTrspTariffDatas, $userId, $tabTrspTariffErrors, undef))
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, LINE_ERROR_MODULE_TARIFFTRSP);
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabTrspTariffErrors->{'fatal'}))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Enregistrement des jours +/-
    if ($hasErrors == 0)
    {
        my $tabDaysErrors = $tabErrors->{'modules'}->{'days'};
        if ($tabDaysActions && !&_execDaysActions($countryId, $estimateLineId, $tabDaysActions, $userId, {}, $tabDaysErrors, $tabEndUpds))
        {
            push(@{$tabErrors->{'fatal'}}, LINE_ERROR_MODULE_DAYS);

            # Erreur fatale ?
            if (&LOC::Util::in_array(0x0001, $tabDaysErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, 0x0001);
            }
        }
    }

    # Mise à jour des informations de l'onglet autres services
    if ($hasErrors == 0 && !&_updateLineOtherServices($db, $countryId, $estimateId, $estimateLineId, $customerId,
                                                                   $tabData, $tabRights->{'services'}, $tabErrors, undef))
    {
        $hasErrors++;
    }

    # Mise à jour des services de location et de transport
    if ($hasErrors == 0 && !&_updateLineRentServices($db, $countryId, $estimateLineId, $tabRentServices, $rentServiceMinAmount,
                                                     $tabRights->{'services'}, $userId, $tabErrors, $tabEndUpds))
    {
        $hasErrors++;
    }

    # Mise à jour des montants de la ligne
    if ($hasErrors == 0 && !&_updateLineAmounts($db, $countryId, $estimateLineId))
    {
        $hasErrors++;
    }

    # Enregistrement des infos proforma
    if ($hasErrors == 0 && $tabProformaAction)
    {
        my $tabProformaErrors = $tabErrors->{'modules'}->{'proforma'};
        if (!&_execProformaAction($countryId, $estimateLineId, $tabProformaAction, $userId, $tabProformaErrors, $tabEndUpds))
        {
            $hasErrors++;
            push(@{$tabErrors->{'fatal'}}, LINE_ERROR_MODULE_PROFORMA);

            # Erreur fatale ?
            if (&LOC::Util::in_array(0x0001, $tabProformaErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, 0x0001);
            }
        }
    }

    # Enregistrement des documents
    if ($hasErrors == 0 && $tabDocumentsActions)
    {
        my $tabDocumentsErrors = $tabErrors->{'modules'}->{'documents'};
        if (!&_execDocumentsActions($countryId, $estimateLineId, $tabDocumentsActions, $userId, $tabDocumentsErrors, $tabEndUpds))
        {
            push(@{$tabErrors->{'warning'}}, LINE_ERROR_MODULE_DOCUMENTS);
        }
    }


    # Enregistrement du transport
    my $tabTransportErrors = $tabErrors->{'modules'}->{'transport'};
    my $tabDeliveryErrors = $tabTransportErrors->{'modules'}->{'delivery'};
    my $tabTransportOptions = {};
    if ($hasErrors == 0 && !&LOC::Transport::setDocumentDelivery($countryId, 'estimateLine', $estimateLineId, $tabData->{'tabTransport'}->{'tabDelivery'}->{'data'}, $userId, $tabTransportOptions, $tabDeliveryErrors, $tabEndUpds))
    {
        $hasErrors++;
        push(@{$tabTransportErrors->{'fatal'}}, 0x0100);
        push(@{$tabErrors->{'fatal'}}, LINE_ERROR_MODULE_TRANSPORT);

        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabDeliveryErrors->{'fatal'}))
        {
            push(@{$tabTransportErrors->{'fatal'}}, 0x0001);
            push(@{$tabErrors->{'fatal'}}, 0x0001);
        }
        # Modifications interdites ?
        if (&LOC::Util::in_array(0x0003, $tabDeliveryErrors->{'fatal'}))
        {
            push(@{$tabTransportErrors->{'fatal'}}, 0x0003);
            push(@{$tabErrors->{'fatal'}}, 0x0003);
        }
    }

    my $tabRecoveryErrors = $tabTransportErrors->{'modules'}->{'recovery'};
    my $tabTransportOptions = {};
    if ($hasErrors == 0 && !&LOC::Transport::setDocumentRecovery($countryId, 'estimateLine', $estimateLineId, $tabData->{'tabTransport'}->{'tabRecovery'}->{'data'}, $userId, $tabTransportOptions, $tabRecoveryErrors, $tabEndUpds))
    {
        $hasErrors++;
        push(@{$tabTransportErrors->{'fatal'}}, 0x0200);
        push(@{$tabErrors->{'fatal'}}, LINE_ERROR_MODULE_TRANSPORT);

        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabRecoveryErrors->{'fatal'}))
        {
            push(@{$tabTransportErrors->{'fatal'}}, 0x0001);
            push(@{$tabErrors->{'fatal'}}, 0x0001);
        }
        # Modifications interdites ?
        if (&LOC::Util::in_array(0x0003, $tabRecoveryErrors->{'fatal'}))
        {
            push(@{$tabTransportErrors->{'fatal'}}, 0x0003);
            push(@{$tabErrors->{'fatal'}}, 0x0003);
        }
    }


    # Si il y a des données invalides alors on arrête ici
    if ($hasErrors > 0 || @{$tabErrors->{'fatal'}} > 0)
    {
        return 0;
    }

    # Mise à jour des alertes
    # - Incrémentation de l'alerte des lignes de devis acceptées
    if ($isAccepted)
    {
        &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ACCESPTEDESTIMATELINES, +1);
    }
    # - Incrémentation de l'alerte des lignes de devis à rappeler
    elsif ($isFollowed)
    {
        &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ESTIMATELINESTOCALL, +1);
    }

    # Trace de création de la ligne de devis
    &LOC::EndUpdates::deleteTraces($tabEndUpds, 'estimateLine', $estimateLineId);

    my $tabTrace = {
        'schema'  => $schemaName,
        'code'    => LOC::Log::EventType::CREATION,
        'old'     => undef,
        'new'     => undef,
        'content' => ''
    };
    &LOC::EndUpdates::addTrace($tabEndUpds, 'estimateLine', $estimateLineId, $tabTrace);


    # Sauvegarde de l'identifiant de la ligne
    if (exists $tabData->{'id'})
    {
        $tabData->{'@oldId'} = $tabData->{'id'};
    }
    $tabData->{'id'} = $estimateLineId;

    return $estimateLineId;
}


# Function: _isConvertibleLine
# Indique si il est possible de passer la ligne de devis en contrat
#
# Parameters:
# string   $countryId      - Pays
# int      $estimateLineId - Id de la ligne du devis
# hashref  $tabInfos       - Tableau des données pour optimisations
#
# Returns:
# bool -
sub _isConvertibleLine
{
    my ($countryId, $estimateLineId, $tabInfos) = @_;

    # Calcul si il est possible ou non de passer la ligne en contrat
    my $rentReducStatus = (exists $tabInfos->{'rentTariff.tabInfos'} ? $tabInfos->{'rentTariff.tabInfos'}->{'specialReductionStatus'} : $tabInfos->{'rentTariff.specialReductionStatus'});
    my $trspReducStatus = $tabInfos->{'transportTariff.tabInfos'}->{'specialReductionStatus'};
    my $hasTrspErrors   = $tabInfos->{'transportTariff.tabInfos'}->{'hasErrors'};

    if (($tabInfos->{'state.id'} ne LINE_STATE_ACTIVE) ||
        ($tabInfos->{'estimate.state.id'} eq STATE_ABORTED) ||
        ($rentReducStatus eq LOC::Tariff::Rent::REDUCTIONSTATE_PENDING) ||
        ($trspReducStatus eq LOC::Tariff::Transport::REDUCTIONSTATE_PENDING) ||
        $hasTrspErrors)
    {
        return 0;
    }

    # Il faut vérifier l'état des proformas pour pouvoir basculer la ligne
    my $proformaFlags = $tabInfos->{'proformaFlags'};

    # Blocage si des proformas existent et ne sont pas finalisées
    # Conditions par rapport aux pro formas
    if ($tabInfos->{'estimate.customer.isProformaRequired'} && $tabInfos->{'total'} > 0)
    {
        # Si le client est en paiement avant livraison alors :
        # On peut passer la ligne de devis en contrat si toutes les pro formas actives sont encaissées, justifiées ou validées (cad finalisées).
        if (!($proformaFlags & LOC::Proforma::DOCFLAG_ALLACTIVEDFINALIZED))
        {
            return 0;
        }
    }
    else
    {
        # Si le client n'est pas en paiement avant livraison alors :
        # On peut passer la ligne de devis en contrat si toutes les pro formas actives sont encaissées, justifiées ou validées (cad finalisées)
        # ou qu'il n'y a aucune pro forma active et aucune non à jour
        if (!(($proformaFlags & LOC::Proforma::DOCFLAG_ALLACTIVEDFINALIZED) || !($proformaFlags & LOC::Proforma::DOCFLAG_EXISTACTIVED)))
        {
            return 0;
        }
    }

    return 1;
}


# Function: _lockCustomerAfterUpdate
# # Reblocage du client s'il était débloqué temporairement
#
# Parameters:
# string   $countryId        - Pays
# hashref  $tabCustomerInfos - Informations sur le client
# int      $userId           - Utilisateur responsable de la modification
# hashref  $tabEndUpds       - Mises à jour de fin
# string   $callerRoutine    - Routine appellante
#
# Returns:
# bool -
sub _lockCustomerAfterUpdate
{
    my ($countryId, $tabCustomerInfos, $userId, $tabEndUpds, $callerRoutine) = @_;

    # Reblocage du client s'il était débloqué temporairement
    if ($tabCustomerInfos->{'isTempUnlock'} == 1)
    {
        # Récupération des codes collectifs à ne pas rebloquer
        my $regEx = &LOC::Characteristic::getCountryValueByCode('COLTMPUNL', $countryId);

        if ($regEx eq '' || $tabCustomerInfos->{'accountingCode'} !~ /^$regEx$/)
        {
            my $tabData = {
                'isLocked'     => 1,
                'isTempUnlock' => 0
            };
            if (!&LOC::Customer::update($countryId, $tabCustomerInfos->{'id'}, $tabData, $userId,
                                        {'caller' => 'LOC::Estimate::Rent::' . $callerRoutine . '()'},
                                        undef, $tabEndUpds))
            {
                return 0;
            }
        }
    }
    return 1;
}


# Function: _updateAmounts
# Mise à jour du montant total du devis
#
# Parameters:
# LOC::Db::Adapter $db                        - Connexion à la base de données
# string           $countryId                 - Pays
# int              $estimateId                - Id du devis
# bool             $isModificationDateUpdated - Mettre à jour la date de modification ?
#
# Returns:
# bool -
sub _updateAmounts
{
    my ($db, $countryId, $estimateId, $isModificationDateUpdated) = @_;

    # Nom du schéma
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Montant des services de location et transport
    my $queryRentServicesAmount =
        'SELECT IFNULL(SUM(res_amount), 0) ' .
        'FROM ' . $schemaName . '.f_rentestimateline_rentservice ' .
        'WHERE res_rel_id = tbl_f_estimatedetail.DETAILSDEVISAUTO ' .
        'AND res_sta_id <> "' . LOC::Common::RentService::STATE_DELETED . '"';

    my $query = '
SELECT
    SUM(IF (tbl_f_estimatedetail.DETAILSDEVISOK <> -2,
            (tbl_f_estimatedetail.DETAILSDEVISMONTANTFR +
             tbl_f_estimatedetail.DETAILSDEVISASSMONTANT +
             tbl_f_estimatedetail.DETAILSDEVISRECOURSMONTANT +
             tbl_f_estimatedetail.DETAILSDEVISTRD +
             tbl_f_estimatedetail.DETAILSDEVISTRR +
             tbl_f_estimatedetail.DETAILSDEVISNETMONTANT +
             tbl_f_estimatedetail.DETAILSDEVISCARBUMONTANT +
             tbl_f_estimatedetail.DETAILSDEVISDECHETSMONTANT +
             (' . $queryRentServicesAmount . ')), 0)),
    SUM(IF (tbl_f_estimatedetail.DETAILSDEVISOK <> -2,
            (tbl_f_estimatedetail.DETAILSDEVISMONTANTEU +
             tbl_f_estimatedetail.DETAILSDEVISASSMONTANTEU +
             tbl_f_estimatedetail.DETAILSDEVISRECOURSMONTANTEU +
             tbl_f_estimatedetail.DETAILSDEVISTRDEU +
             tbl_f_estimatedetail.DETAILSDEVISTRREU +
             tbl_f_estimatedetail.DETAILSDEVISNETMONTANTEU +
             tbl_f_estimatedetail.DETAILSDEVISCARBUMONTANTEU +
             tbl_f_estimatedetail.DETAILSDEVISDECHETSMONTANTEU +
             (' . $queryRentServicesAmount . ')), 0)),
    GROUP_CONCAT(tbl_f_estimatedetail.DETAILSDEVISOK ORDER BY tbl_f_estimatedetail.DETAILSDEVISAUTO ASC SEPARATOR ";"),
    tbl_f_estimate.ETCODE
FROM ' . $schemaName . '.DEVIS tbl_f_estimate
INNER JOIN ' . $schemaName . '.DETAILSDEVIS tbl_f_estimatedetail
ON tbl_f_estimate.DEVISAUTO = tbl_f_estimatedetail.DEVISAUTO
WHERE tbl_f_estimate.DEVISAUTO = ' . $estimateId . ';';
    my $tabRow = $db->fetchRow($query, {}, LOC::Db::FETCH_NUM);

    my $amount     = $tabRow->[0] * 1;
    my $euroAmount = $tabRow->[1] * 1;
    my @tabStates  = split(/\;/, $tabRow->[2]);
    my $stateId    = $tabRow->[3];
    my @tabLinesStates = ();

    # Comptage des lignes par état
    my $nbLines = @tabStates;
    for (my $cpt = 0; $cpt < $nbLines; $cpt++)
    {
        if ($tabStates[$cpt] == 0)
        {
            push(@tabLinesStates, LINE_STATE_ACTIVE);
        }
        elsif ($tabStates[$cpt] == -1)
        {
            push(@tabLinesStates, LINE_STATE_CONVERTED);
        }
        elsif ($tabStates[$cpt] == -2)
        {
            push(@tabLinesStates, LINE_STATE_INACTIVE);
        }
    }

    # On s'assure que l'état du devis est correct
    my $stateId = &_getCorrectStateId($stateId, \@tabLinesStates);

    # Mise à jour du montant total du devis
    my $tabUpdates = {
        'DEVISMONTANTFR' => $amount,
        'DEVISMONTANTEU' => $euroAmount,
        'ETCODE'         => $stateId,
        'DEVISDATE*'     => ($isModificationDateUpdated ? 'NOW()' : 'DEVISDATE')
    };
    if ($db->update('DEVIS', $tabUpdates, {'DEVISAUTO*' => $estimateId}, $schemaName) == -1)
    {
        return 0;
    }
    return {'total' => $amount, 'state.id' => $stateId};
}


# Function: _updateLine
# Mise à jour d'une ligne de devis
#
# Parameters:
# LOC::Db::Adapter $db             - Connexion à la base de données
# string           $countryId      - Pays
# int              $estimateLineId - Id de la ligne du devis
# hashref          $tabData        - Données à mettre à jour
# hashref          $tabInfos       - Tableau des données de la ligne
# int              $userId         - Utilisateur responsable de la modification
# hashref          $tabOptions     - Options supplémentaires (facultatif)
# hashref          $tabErrors      - Tableau des erreurs
# hashref          $tabEndUpds     - Mises à jour de fin
#
# Returns:
# bool -
sub _updateLine
{
    my ($db, $countryId, $estimateLineId, $tabData, $tabInfos, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    my $hasErrors = 0;

    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Récupération des droits
    my $tabRights = &_getLineRights($tabInfos, $tabUserInfos, $tabInfos->{'proformaFlags'});


    # Nom du schéma de connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);


    # Impossible de modifier une ligne de devis passée en contrat ou désactivée
    if ((!defined $tabData->{'@subAction'} || $tabData->{'@subAction'} eq '') && !&_hasRight($tabRights->{'actions'}, 'valid'))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0003);
        return 0;
    }

    my $customerId = $tabInfos->{'estimate.customer.id'};
    my $agencyId   = $tabInfos->{'estimate.agency.id'};
    my $zone       = $tabInfos->{'estimate.zone'};
    my $estimateId = $tabInfos->{'estimate.id'};

    my $newStateId =  $tabInfos->{'state.id'};

    my $oldModelId = $tabInfos->{'model.id'};
    my $newModelId = (defined $tabData->{'model.id'} ? $tabData->{'model.id'} : $oldModelId);
    my $quantity   = $tabInfos->{'quantity'}; # non modifiable
    my $oldIsImperativeModel = $tabInfos->{'isImperativeModel'};
    my $newIsImperativeModel = (exists $tabData->{'isImperativeModel'} ? $tabData->{'isImperativeModel'} : $oldIsImperativeModel);

    my $oldIsAccepted = $tabInfos->{'isAccepted'};
    my $newIsAccepted = (exists $tabData->{'isAccepted'} ? $tabData->{'isAccepted'} : $oldIsAccepted);
    my $oldIsFollowed = $tabInfos->{'isFollowed'};
    my $newIsFollowed = (exists $tabData->{'isFollowed'} ? $tabData->{'isFollowed'} : $oldIsFollowed);
    my $newIsDatesToConfirm = (exists $tabData->{'isDatesToConfirm'} ? $tabData->{'isDatesToConfirm'} : $tabInfos->{'isDatesToConfirm'});

    # Onglet "Durée"
    my $oldBeginDate = $tabInfos->{'beginDate'};
    my $newBeginDate = (exists $tabData->{'beginDate'} ? $tabData->{'beginDate'} : $oldBeginDate);
    my $oldEndDate   = $tabInfos->{'endDate'};
    my $newEndDate   = (exists $tabData->{'endDate'} ? $tabData->{'endDate'} : $oldEndDate);
    my $oldDuration  = $tabInfos->{'duration'};
    my $oldCalDuration = &LOC::Date::getDeltaDays($oldBeginDate, $oldEndDate);
    if ($newBeginDate eq '' || !&LOC::Date::checkMySQLDate($newBeginDate))
    {
        $newBeginDate = undef;
    }
    if ($newEndDate eq '' || !&LOC::Date::checkMySQLDate($newEndDate))
    {
        $newEndDate = undef;
    }
    # - Jours plus et jours moins
    my $tabDaysActions  = $tabData->{'tabDaysActions'};

    my $oldAdditionalDays = $tabInfos->{'additionalDays'};
    my $oldDeductedDays   = $tabInfos->{'deductedDays'};

    my $newTabDiffDaysDuration = &LOC::Common::Rent::calculateDiffDaysByActions($countryId, $tabDaysActions);
    my $newAdditionalDays = $oldAdditionalDays + $newTabDiffDaysDuration->{&LOC::Day::TYPE_ADDITIONAL};
    my $newDeductedDays   = $oldDeductedDays + $newTabDiffDaysDuration->{&LOC::Day::TYPE_DEDUCTED};

    # - Calcul des jours ouvrés de location
    my $realDuration = &LOC::Date::getRentDuration($newBeginDate, $newEndDate, $agencyId);
    # - Calcul de la durée de location
    my $newDuration;
    if (&_hasRight($tabRights->{'duration'}, 'endDate'))
    {
        $newDuration = $realDuration + $newAdditionalDays - $newDeductedDays;
    }
    else
    {
        $newDuration = $oldDuration;
    }
    # - Calcul de la durée calendaire
    my $newCalDuration = &LOC::Date::getDeltaDays($newBeginDate, $newEndDate);

    # Services de location et de transport
    # - Récupération du montant minimum des services location et transport pour l'utilisateur connecté
    my $rentServiceMinAmount = &LOC::Characteristic::getAgencyValueByCode('MTMINSRV', $agencyId);
    my $rentServiceNoLimitUserIdList = &LOC::Characteristic::getAgencyValueByCode('USRNOMAXREMSRV', $agencyId);
    my @rentServiceNoLimitUserId = split(',', $rentServiceNoLimitUserIdList);
    # - Si l'utilisateur courant n'a pas de limite de services location et transport
    if (grep {$_ eq $userId} @rentServiceNoLimitUserId)
    {
        $rentServiceMinAmount = undef;
    }
    my $tabRentServices = (exists $tabData->{'tabRentServices'} ? $tabData->{'tabRentServices'} : []);

    # Sous actions
    my $subAction = $tabData->{'@subAction'};
    if ($subAction ne '')
    {
        if ($subAction eq 'accept') # Accepter
        {
            $newIsAccepted = 1;
        }
        elsif ($subAction eq 'unaccept') # Ne plus accepter
        {
            $newIsAccepted = 0;
        }
        elsif ($subAction eq 'follow') # Suivre
        {
            $newIsFollowed = 1;
        }
        elsif ($subAction eq 'unfollow') # Ne plus suivre
        {
            $newIsFollowed = 0;
        }
        elsif ($subAction eq 'deactivate') # Désactiver la ligne
        {
            $newStateId = LINE_STATE_INACTIVE;
        }
    }


    # Vérification des données
    if ($newModelId == 0)
    {
        push(@{$tabErrors->{'fatal'}}, 0x1000);
    }
    else
    {
        # Les dates de début et de fin sont obligatoires
        if ($newBeginDate eq '' || $newEndDate eq '')
        {
            push(@{$tabErrors->{'fatal'}}, 0x1005);
        }
        else
        {
            if ($newDuration eq '' || $newDuration <= 0)
            {
                # Durée invalide
                push(@{$tabErrors->{'fatal'}}, 0x1002);
            }

            if ($newEndDate lt $newBeginDate)
            {
                # La date de fin est inférieure à la date de début
                push(@{$tabErrors->{'fatal'}}, 0x1008);
            }

            # Si la date de début a été modifiée
            if ($newBeginDate ne $oldBeginDate)
            {
                # Date de création - 1 mois
                my %tabCreationDate = &LOC::Date::getHashDate($tabInfos->{'creationDate'});
                my ($tYear, $tMonth, $tDay) = &Date::Calc::Add_Delta_YM($tabCreationDate{'y'},
                                                                        $tabCreationDate{'m'},
                                                                        $tabCreationDate{'d'},
                                                                        0,
                                                                        -1);
                my $creationDateLess1Month = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE, ('y' => $tYear, 'm' => $tMonth, 'd' => $tDay));
                if ($newBeginDate le $creationDateLess1Month)
                {
                    # La date de début ne doit pas être inférieure à 1 mois
                    push(@{$tabErrors->{'fatal'}}, 0x1007);
                }
            }
        }
    }

    # Si erreur de vérification de données alors on arrête !!
    if (@{$tabErrors->{'fatal'}} > 0)
    {
        return 0;
    }


    # Vérification et historisation des modifications
    # -------------------------------
    my $tabChanges = {};
    # - Changement de modèle de machines
    if ($newModelId != $oldModelId)
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'machine'}, 'model'))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }

        # Historisation:
        my $subQuery = '(SELECT CAST(CONCAT(MOMADESIGNATION, " {", MOMAAUTO,"}") AS CHAR CHARACTER SET utf8) FROM AUTH.MODELEMACHINE WHERE MOMAAUTO = %d)';
        my $query = 'CONCAT(' . sprintf($subQuery, $oldModelId) . ', " → ", ' . sprintf($subQuery, $newModelId) . ')';
        my $tabTrace = {
            'schema'   => $schemaName,
            'code'     => LOC::Log::EventType::CHANGEMODEL,
            'old'      => $oldModelId,
            'new'      => $newModelId,
            'content'  => '',
            'sqlQuery' => $query
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'estimateLine', $estimateLineId, $tabTrace);
    }
    # - Modification de la date de début
    if ($oldBeginDate ne $newBeginDate)
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'duration'}, 'beginDate'))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }

        # Historisation:
        my $tabTrace = {
            'schema'  => $schemaName,
            'code'    => LOC::Log::EventType::UPDBEGINDATE,
            'old'     => $oldBeginDate,
            'new'     => $newBeginDate,
            'content' => $oldBeginDate . ' → ' . $newBeginDate
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'estimateLine', $estimateLineId, $tabTrace);
    }
    # - Modification de la date de fin
    if ($oldEndDate ne $newEndDate)
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'duration'}, 'endDate'))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }

        # Historisation:
        my $tabTrace = {
            'schema'  => $schemaName,
            'code'    => LOC::Log::EventType::UPDENDDATE,
            'old'     => $oldEndDate,
            'new'     => $newEndDate,
            'content' => $oldEndDate . ' → ' . $newEndDate
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'estimateLine', $estimateLineId, $tabTrace);
    }
    # - Modification des jours supplémentaires
    if ($newAdditionalDays != $oldAdditionalDays)
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'duration'}, 'additionalDays'))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }
    }
    # - Modification des jours déduits
    if ($newDeductedDays != $oldDeductedDays)
    {
        # Vérification:
        if (!&_hasRight($tabRights->{'duration'}, 'deductedDays'))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }
    }
    # - Modification de la durée
    if ($newDuration != $oldDuration)
    {
        # Historisation:
        my $tabTrace = {
            'schema'  => $schemaName,
            'code'    => LOC::Log::EventType::UPDDURATION,
            'old'     => $oldDuration,
            'new'     => $newDuration,
            'content' => sprintf('%.1f → %.1f', $oldDuration, $newDuration)
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'estimateLine', $estimateLineId, $tabTrace);
    }

    #- Modification de la durée calendaire
    if ($newCalDuration != $oldCalDuration)
    {
        # Historisation:
        my $tabTrace = {
            'schema'  => $schemaName,
            'code'    => LOC::Log::EventType::UPDCALENDARDURATION,
            'old'     => $oldCalDuration,
            'new'     => $newCalDuration,
            'content' => sprintf('%.1f → %.1f', $oldCalDuration, $newCalDuration)
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'estimateLine', $estimateLineId, $tabTrace);
    }

    # - Modification de l'état d'acceptation
    if ($oldIsAccepted != $newIsAccepted)
    {
        # - Vérifie que l'utilisateur peut accepter une ligne
        if ($tabOptions->{'updateFrom'} ne 'abortEstimate' &&
            (($newIsAccepted && !$tabInfos->{'isAcceptable'}) ||
            !&_hasRight($tabRights->{'actions'}, 'accept')))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }

        # - Vérifie que l'horaire est renseigné si on accepte la ligne
        if ($newIsAccepted)
        {
            if (!$tabInfos->{'estimate.site.deliveryHour'} && !$tabInfos->{'estimate.site.deliveryTimeSlot.id'})
            {
                # On ne peut pas accepter une ligne si l'heure de livraison n'est pas renseignée
                push(@{$tabErrors->{'fatal'}}, 0x1009);
            }

            # Historisation:
            my $tabTrace = {
                'schema' => $schemaName,
                'code'   => LOC::Log::EventType::ACCEPTESTIMATELINE
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, 'estimateLine', $estimateLineId, $tabTrace);
        }
        else
        {
            # Historisation:
            my $tabTrace = {
                'schema' => $schemaName,
                'code'   => LOC::Log::EventType::UNACCEPTESTIMATELINE
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, 'estimateLine', $estimateLineId, $tabTrace);
        }
        $tabChanges->{'isAccepted'} = {'old' => $oldIsAccepted, 'new' => $newIsAccepted};

        # Si l'état d'acceptation change alors on suit à nouveau la ligne
        $newIsFollowed = 1;
    }

    # - Vérification des droits pour le modèle impératif
    if ($oldIsImperativeModel != $newIsImperativeModel
        && !&_hasRight($tabRights->{'machine'}, 'isImperativeModel'))
    {
        push(@{$tabErrors->{'fatal'}}, 0x0003);
        return 0;
    }

    # Paramètres pour la mise à jour du tarif de location
    my $tabRentTariffDatas = $tabData->{'tabRentTariff'};
    # Optimisations location
    $tabRentTariffDatas->{'estimateLine.estimate.id'}          = $estimateId;
    $tabRentTariffDatas->{'estimateLine.estimate.customer.id'} = $customerId;
    $tabRentTariffDatas->{'estimateLine.estimate.agency.id'}   = $agencyId;
    $tabRentTariffDatas->{'estimateLine.model.id'}             = $newModelId;
    $tabRentTariffDatas->{'estimateLine.quantity'}             = $quantity;
    $tabRentTariffDatas->{'rights'}                            = $tabRights->{'rentTariff'};
    $tabRentTariffDatas->{'editMode'}                          = 'update';


    # Vérification du nombre de jours (en cas)
    if (exists $tabRentTariffDatas->{'days'} && $tabRentTariffDatas->{'days'} != $newDuration)
    {
        push(@{$tabErrors->{'fatal'}}, 0x1004);
        return 0;
    }
    $tabRentTariffDatas->{'days'} = $newDuration;


    # Paramètres pour la mise à jour du tarif transport
    my $tabTrspTariffDatas = $tabData->{'tabTransport'}->{'tabTariff'};
    # Optimisations transport
    $tabTrspTariffDatas->{'estimateLine.estimate.id'}          = $estimateId;
    $tabTrspTariffDatas->{'estimateLine.estimate.customer.id'} = $customerId;
    $tabTrspTariffDatas->{'estimateLine.estimate.agency.id'}   = $agencyId;
    $tabTrspTariffDatas->{'estimateLine.estimate.zone'}        = $zone;
    $tabTrspTariffDatas->{'rights'}                            = $tabRights->{'transport'};
    $tabTrspTariffDatas->{'editMode'}                          = 'update';

    # Paramètres pour l'enregistrement des proformas
    my $tabProformaAction = $tabData->{'tabProformaAction'};
    if ($tabProformaAction)
    {
        my $type = $tabProformaAction->{'type'};
        # - Vérification des droits pour la proforma
        if (!&_hasRight($tabRights->{'proforma'}, $type))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }
    }

    # Paramètres pour l'enregistrement des documents
    my $tabDocumentsActions = $tabData->{'tabDocumentsActions'};


    my $tabUpdates = {
        'MOMAAUTO'               => $newModelId,
        'DETAILSDEVISMODIMP'     => ($newIsImperativeModel ? -1 : 0),

        'DETAILSDEVISDATE'       => $newBeginDate,
        'DETAILSDEVISDATEFIN'    => $newEndDate,
        'DETAILSDEVISDUREE'      => $newDuration,
        'DETAILSDEVISJOURSPLUS'  => $newAdditionalDays,
        'DETAILSDEVISJOURSMOINS' => $newDeductedDays,

        'DETAILSDEVISACCEPTE'    => ($newIsAccepted ? -1 : 0),
        'DETAILSDEVISSUIVI'      => ($newIsFollowed ? -1 : 0),

        'DETAILSDEVISDATESACONFIRMER' => ($newIsDatesToConfirm ? -1 : 0),

        'DETAILSDEVISOK'         => &_getValueForLineStateId($newStateId),

        'DETAILSDEVISDATEMODIF*' => 'Now()' # Force la modification de la date de modif
    };

    if ($db->update('DETAILSDEVIS', $tabUpdates, {'DETAILSDEVISAUTO*' => $estimateLineId}, $schemaName) == -1)
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, 0x0001);
    }


    # Enregistrement du tarif de location
    my $tabRentTariffErrors = $tabErrors->{'modules'}->{'rentTariff'};
    if (!&LOC::Tariff::Rent::save($countryId, 'estimateLine', $estimateLineId, $tabRentTariffDatas, $userId, $tabRentTariffErrors, $tabEndUpds))
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, LINE_ERROR_MODULE_TARIFFRENT);
        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabRentTariffErrors->{'fatal'}))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
        # Modifications interdites ?
        if (&LOC::Util::in_array(0x0003, $tabRentTariffErrors->{'fatal'}))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }
    }

    # Enregistrement de l'estimation transport
    my $tabTrspTariffErrors = $tabErrors->{'modules'}->{'transportTariff'};
    if (!&LOC::Tariff::Transport::save($countryId, 'estimateLine', $estimateLineId, $tabTrspTariffDatas, $userId, $tabTrspTariffErrors, $tabEndUpds))
    {
        # Option permettant de ne pas prendre en compte les erreurs de la tarification transport (erreurs non fatales)
        if (!$tabOptions->{'disableTransportTariffErrors'} || &LOC::Util::in_array(0x0001, $tabTrspTariffErrors->{'fatal'}))
        {
            $hasErrors++;
            push(@{$tabErrors->{'fatal'}}, LINE_ERROR_MODULE_TARIFFTRSP);
            # Erreur fatale ?
            if (&LOC::Util::in_array(0x0001, $tabTrspTariffErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, 0x0001);
                return 0;
            }
            # Modifications interdites ?
            if (&LOC::Util::in_array(0x0003, $tabTrspTariffErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, 0x0003);
                return 0;
            }
        }
    }

    # Enregistrement des jours +/-
    if ($hasErrors == 0 && $tabDaysActions)
    {
        my $tabDaysErrors = $tabErrors->{'modules'}->{'days'};
        if (!&_execDaysActions($countryId, $estimateLineId, $tabDaysActions, $userId, {}, $tabDaysErrors, $tabEndUpds))
        {
            push(@{$tabErrors->{'fatal'}}, LINE_ERROR_MODULE_DAYS);

            # Erreur fatale ?
            if (&LOC::Util::in_array(0x0001, $tabDaysErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, 0x0001);
            }
        }
    }

    # Mise à jour des informations de l'onglet autres services
    if ($hasErrors == 0 && !&_updateLineOtherServices($db, $countryId, $estimateId, $estimateLineId, $customerId,
                                                                   $tabData, $tabRights->{'services'}, $tabErrors, $tabEndUpds))
    {
        $hasErrors++;
    }

    # Mise à jour des services de location et de transport
    if ($hasErrors == 0 && !&_updateLineRentServices($db, $countryId, $estimateLineId, $tabRentServices, $rentServiceMinAmount,
                                                     $tabRights->{'services'}, $userId, $tabErrors, $tabEndUpds))
    {
        $hasErrors++;
    }

    # Mise à jour des montants de la ligne
    if ($hasErrors == 0 && !&_updateLineAmounts($db, $countryId, $estimateLineId))
    {
        $hasErrors++;
    }


    # - Modifications impactant les proforma existantes
    if ($hasErrors == 0 && &_expireProformas($countryId, $estimateLineId, $userId, $tabEndUpds) == 0)
    {
        $hasErrors++;
        push(@{$tabErrors->{'fatal'}}, 0x0001);
    }

    # Enregistrement des infos proforma
    if ($hasErrors == 0 && $tabProformaAction)
    {
        my $tabProformaErrors = $tabErrors->{'modules'}->{'proforma'};
        if (!&_execProformaAction($countryId, $estimateLineId, $tabProformaAction, $userId, $tabProformaErrors, $tabEndUpds))
        {
            $hasErrors++;
            push(@{$tabErrors->{'fatal'}}, LINE_ERROR_MODULE_PROFORMA);

            # Erreur fatale ?
            if (&LOC::Util::in_array(0x0001, $tabProformaErrors->{'fatal'}))
            {
                push(@{$tabErrors->{'fatal'}}, 0x0001);
            }
        }
    }

    # Enregistrement des documents
    if ($hasErrors == 0 && $tabDocumentsActions)
    {
        my $tabDocumentsErrors = $tabErrors->{'modules'}->{'documents'};
        if (!&_execDocumentsActions($countryId, $estimateLineId, $tabDocumentsActions, $userId, $tabDocumentsErrors, $tabEndUpds))
        {
            push(@{$tabErrors->{'warning'}}, LINE_ERROR_MODULE_DOCUMENTS);
        }
    }


    # Enregistrement du transport
    my $tabTransportErrors = $tabErrors->{'modules'}->{'transport'};
    my $tabDeliveryErrors = $tabTransportErrors->{'modules'}->{'delivery'};
    $tabData->{'tabTransport'}->{'tabDelivery'}->{'rights'} = $tabRights->{'transport'};
    my $tabTransportOptions = {
        'tabDocumentChanges' => $tabChanges
    };
    if ($hasErrors == 0 && !&LOC::Transport::setDocumentDelivery($countryId, 'estimateLine', $estimateLineId, $tabData->{'tabTransport'}->{'tabDelivery'}->{'data'}, $userId, $tabTransportOptions, $tabDeliveryErrors, $tabEndUpds))
    {
        $hasErrors++;
        push(@{$tabTransportErrors->{'fatal'}}, 0x0100);
        push(@{$tabErrors->{'fatal'}}, LINE_ERROR_MODULE_TRANSPORT);

        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabDeliveryErrors->{'fatal'}))
        {
            push(@{$tabTransportErrors->{'fatal'}}, 0x0001);
            push(@{$tabErrors->{'fatal'}}, 0x0001);
        }
        # Modifications interdites ?
        if (&LOC::Util::in_array(0x0003, $tabDeliveryErrors->{'fatal'}))
        {
            push(@{$tabTransportErrors->{'fatal'}}, 0x0003);
            push(@{$tabErrors->{'fatal'}}, 0x0003);
        }
    }

    my $tabRecoveryErrors = $tabTransportErrors->{'modules'}->{'recovery'};
    $tabData->{'tabTransport'}->{'tabRecovery'}->{'rights'} = $tabRights->{'transport'};
    my $tabTransportOptions = {
        'tabDocumentChanges' => $tabChanges
    };
    if ($hasErrors == 0 && !&LOC::Transport::setDocumentRecovery($countryId, 'estimateLine', $estimateLineId, $tabData->{'tabTransport'}->{'tabRecovery'}->{'data'}, $userId, $tabTransportOptions, $tabRecoveryErrors, $tabEndUpds))
    {
        $hasErrors++;
        push(@{$tabTransportErrors->{'fatal'}}, 0x0200);
        push(@{$tabErrors->{'fatal'}}, LINE_ERROR_MODULE_TRANSPORT);

        # Erreur fatale ?
        if (&LOC::Util::in_array(0x0001, $tabRecoveryErrors->{'fatal'}))
        {
            push(@{$tabTransportErrors->{'fatal'}}, 0x0001);
            push(@{$tabErrors->{'fatal'}}, 0x0001);
        }
        # Modifications interdites ?
        if (&LOC::Util::in_array(0x0003, $tabRecoveryErrors->{'fatal'}))
        {
            push(@{$tabTransportErrors->{'fatal'}}, 0x0003);
            push(@{$tabErrors->{'fatal'}}, 0x0003);
        }
    }


    # Si il y a des données invalides alors on arrête ici
    if ($hasErrors > 0 || @{$tabErrors->{'fatal'}} > 0)
    {
        return 0;
    }


    # Mise à jour des alertes
    # - L'état de l'acceptation de la ligne a changé
    if ($oldIsAccepted != $newIsAccepted)
    {
        # - non accepté à accepté
        if ($newIsAccepted)
        {
            # Incrémentation de l'alerte des lignes de devis acceptées
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ACCESPTEDESTIMATELINES, +1);
            if ($oldIsFollowed)
            {
                # Décrémentation de l'alerte des lignes de devis à rappeler
                &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ESTIMATELINESTOCALL, -1);
            }
        }
        # - accepté à non accepté
        else
        {
            # Décrémentation de l'alerte des lignes de devis acceptées
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ACCESPTEDESTIMATELINES, -1);
            if ($newIsFollowed)
            {
                # Incrémentation de l'alerte des lignes de devis à rappeler
                &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ESTIMATELINESTOCALL, +1);
            }
        }
    }
    elsif ($oldIsFollowed != $newIsFollowed)
    {
        # - non suivi à suivi
        if ($newIsFollowed)
        {
            # Incrémentation de l'alerte des lignes de devis à rappeler
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ESTIMATELINESTOCALL, +1);
        }
        # - suivi à non suivi
        else
        {
             # Décrémentation de l'alerte des lignes de devis à rappeler
            &LOC::EndUpdates::updateAlert($tabEndUpds, $agencyId, LOC::Alert::ESTIMATELINESTOCALL, -1);
        }
    }

    return 1;
}


# Function: _updateLineAmounts
# Mise à jour du montant total d'une ligne de devis
#
# Parameters:
# LOC::Db::Adapter $db                        - Connexion à la base de données
# string           $countryId                 - Pays
# int              $estimateLineId                - Id du devis
#
# Returns:
# bool -
sub _updateLineAmounts
{
    my ($db, $countryId, $estimateLineId) = @_;

    # Nom du schéma
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my $query = '
SELECT
    tbl_f_estimate.AGAUTO AS `estimate.agency.id`,
    tbl_f_estimatedetail.DEVISAUTO AS `estimate.id`,
    tbl_f_estimatedetail.DETAILSDEVISDATE AS `beginDate`,
    tbl_f_estimatedetail.DETAILSDEVISDATEFIN AS `endDate`,
    tbl_f_estimatedetail.DETAILSDEVISDUREE AS `duration`,
    tbl_f_estimatedetail.DETAILSDEVISJOURSPLUS AS `additionalDays`,
    tbl_f_estimatedetail.DETAILSDEVISJOURSMOINS AS `deductedDays`,

    IF (tbl_f_estimatedetail.DETAILSDEVISTYPEMONTANT = 1, "day", "month") AS `amountType`,
    tbl_f_estimatedetail.DETAILSDEVISPXJOURSFR AS `unitPrice`,

    tbl_f_estimatedetail.DETAILSDEVISTYPEASSURANCE AS `insuranceType.id`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISINVOICED) . ', tbl_f_estimatedetail.DETAILSDEVISASSTAUX, 0) AS `insurance.rate`,

    tbl_f_estimatedetail.DETAILSDEVISTYPERECOURS AS `appealType.id`,
    tbl_f_estimatedetail.DETAILSDEVISRECOURSVAL AS `appeal.value`,

    -tbl_f_estimatedetail.DETAILSDEVISDECHETSFACT AS `residues.mode`,
    tbl_f_estimatedetail.DETAILSDEVISDECHETSVAL AS `residues.value`
FROM DETAILSDEVIS tbl_f_estimatedetail
INNER JOIN DEVIS tbl_f_estimate
ON tbl_f_estimate.DEVISAUTO = tbl_f_estimatedetail.DEVISAUTO
LEFT JOIN p_insurancetype
ON p_insurancetype.ity_id = tbl_f_estimatedetail.DETAILSDEVISTYPEASSURANCE
WHERE tbl_f_estimatedetail.DETAILSDEVISAUTO = ' . $estimateLineId . ';';
    my $tabRow = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if (!$tabRow)
    {
        return 0;
    }

    # Calcul du prix par jour
    $tabRow->{'dailyPrice'} = &LOC::Common::Rent::getDailyPrice(
        $tabRow->{'amountType'},
        $tabRow->{'unitPrice'},
        $tabRow->{'duration'}
    );

    # Récupération des informations diverses
    my $agencyId        = $tabRow->{'estimate.agency.id'};
    my $beginDate       = $tabRow->{'beginDate'};
    my $endDate         = $tabRow->{'endDate'};
    my $duration        = $tabRow->{'duration'} * 1;
    my $insuranceTypeId = $tabRow->{'insuranceType.id'} * 1;
    my $insuranceRate   = $tabRow->{'insurance.rate'} * 1;
    my $appealTypeId    = $tabRow->{'appealType.id'} * 1;
    my $appealValue     = $tabRow->{'appeal.value'} * 1;
    my $residuesMode    = $tabRow->{'residues.mode'} * 1;
    my $residuesValue   = $tabRow->{'residues.value'} * 1;
    my $additionalDays  = $tabRow->{'additionalDays'} * 1;
    my $deductedDays    = $tabRow->{'deductedDays'} * 1;
    my $amountType      = $tabRow->{'amountType'};
    my $unitPrice       = $tabRow->{'unitPrice'} * 1;
    my $dailyPrice      = $tabRow->{'dailyPrice'} * 1;


    # Calcul du montant de l'assurance
    my $insuranceAmount = &LOC::Common::Rent::calculateInsurance($countryId, $agencyId, $insuranceTypeId, $insuranceRate,
                                                                 $dailyPrice, $beginDate, $endDate,
                                                                 $additionalDays, $deductedDays);

    # Calcul de la gestion de recours
    my $appealAmount = 0;
    if ($appealTypeId)
    {
        $appealAmount = &LOC::Common::Rent::calculateAppeal($countryId, $agencyId, $appealTypeId, $appealValue,
                                                            $dailyPrice, $beginDate, $endDate,
                                                            $additionalDays, $deductedDays);
    }

    # Calcul du montant de la participation au recyclage
    my $residuesAmount = &LOC::Common::Rent::calculateResiduesAmount($agencyId, $residuesMode, $residuesValue,
                                                                     $dailyPrice, $beginDate, $endDate,
                                                                     $additionalDays, $deductedDays);

    # Récupération du taux monétaire
    my $euroRate = &LOC::Country::getEuroRate($countryId);

    # Mise à jour des informations
    my $tabUpdates = {
        # Mise à jour de l'assurance
        'DETAILSDEVISASSMONTANT*'      => $insuranceAmount,
        'DETAILSDEVISASSMONTANTEU*'    => &LOC::Util::round($insuranceAmount * $euroRate, 2),

        # Mise à jour de la gestion de recours
        'DETAILSDEVISRECOURSMONTANT*'   => $appealAmount,
        'DETAILSDEVISRECOURSMONTANTEU*' => &LOC::Util::round($appealAmount * $euroRate, 2),

        # Mise à jour des infos participation au recyclage
        'DETAILSDEVISDECHETSMONTANT'   => $residuesAmount,
        'DETAILSDEVISDECHETSMONTANTEU' => &LOC::Util::round($residuesAmount * $euroRate, 2),

        # Pas de mise à jour de la date de modification
        'DETAILSDEVISDATEMODIF*'       => 'DETAILSDEVISDATEMODIF'
    };

    if ($db->update('DETAILSDEVIS', $tabUpdates, {'DETAILSDEVISAUTO*' => $estimateLineId}, $schemaName) == -1)
    {
        return 0;
    }
    return 1;


}



# Function: _updateLineOtherServices
# Mise à jour de l'assurance, du nettoyage, de la gestion des déchets sur une ligne de devis et des services complémentaires
#
# Parameters:
# LOC::Db::Adapter $db             - Connexion à la base de données
# string           $countryId      - Pays
# int              $estimateLineId - Id de la ligne du devis
# int              $customerId     - Id du client
# hashref          $tabData        - Données à mettre à jour
# string           $rights         - Droits sur l'onglet Autres services
# hashref          $tabErrors      - Tableau des erreurs
# hashref          $tabEndUpds     - Mises à jour de fin
#
# Returns:
# bool - 1 si succès 0 sinon
sub _updateLineOtherServices
{
    my ($db, $countryId, $estimateId, $estimateLineId, $customerId, $tabData, $rights, $tabErrors, $tabEndUpds) = @_;

    # Nom du schéma
    my $schemaName = &LOC::Db::getSchemaName($countryId);


    my $query = '
SELECT
    tbl_f_estimate.AGAUTO AS `estimate.agency.id`,
    -tbl_f_estimate.DEVISFULLSERVICE AS `isFullService`,

    IFNULL(tbl_f_estimatedetail.DETAILSDEVISNETAUTO, 0) AS `cleaning.type.id`,
    tbl_f_estimatedetail.DETAILSDEVISNETMONTANT AS `cleaning.amount`,

    tbl_f_estimatedetail.DETAILSDEVISTYPEASSURANCE AS `insuranceType.id`,
    IF (p_insurancetype.ity_flags & ' . $db->quote(LOC::Insurance::Type::FLAG_ISINVOICED) . ', tbl_f_estimatedetail.DETAILSDEVISASSTAUX, 0) AS `insurance.rate`,

    tbl_f_estimatedetail.DETAILSDEVISTYPERECOURS AS `appealType.id`,
    tbl_f_estimatedetail.DETAILSDEVISRECOURSVAL AS `appeal.value`,

    -tbl_f_estimatedetail.DETAILSDEVISDECHETSFACT AS `residues.mode`,
    tbl_f_estimatedetail.DETAILSDEVISDECHETSVAL AS `residues.value`
FROM DETAILSDEVIS tbl_f_estimatedetail
INNER JOIN DEVIS tbl_f_estimate
ON tbl_f_estimate.DEVISAUTO = tbl_f_estimatedetail.DEVISAUTO
LEFT JOIN p_insurancetype
ON p_insurancetype.ity_id = tbl_f_estimatedetail.DETAILSDEVISTYPEASSURANCE
WHERE tbl_f_estimatedetail.DETAILSDEVISAUTO = ' . $estimateLineId . ';';
    my $tabRow = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if (!$tabRow)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Récupération des informations diverses
    my $agencyId       = $tabRow->{'estimate.agency.id'};
    my $isFullService  = $tabRow->{'isFullService'} * 1;
    my $valueDate      = &getValueDate($countryId, $estimateId);

    my $tabUpdates = {};
    # Récupération du taux monétaire
    my $euroRate = &LOC::Country::getEuroRate($countryId);

    # ---------------------------------
    # ASSURANCE

    my $oldInsuranceTypeId  = $tabRow->{'insuranceType.id'} * 1;
    my $oldInsuranceRate  = $tabRow->{'insurance.rate'} * 1;
    my $insuranceTypeId = (exists $tabData->{'tabInsurance'}->{'typeId'} ? $tabData->{'tabInsurance'}->{'typeId'} : $oldInsuranceTypeId);
    my $insuranceRate = (exists $tabData->{'tabInsurance'}->{'rate'} ? $tabData->{'tabInsurance'}->{'rate'} : $oldInsuranceRate);

    my $tabInsuranceTypeInfos    = &LOC::Insurance::Type::getInfos($countryId, $insuranceTypeId);
    my $tabOldInsuranceTypeInfos = &LOC::Insurance::Type::getInfos($countryId, $oldInsuranceTypeId);

    # - Modification de l'assurance
    if ($insuranceTypeId != $oldInsuranceTypeId || ($tabInsuranceTypeInfos->{'isInvoiced'} && $insuranceRate != $oldInsuranceRate))
    {
        # Vérification:
        if (defined $rights && !&_hasRight($rights, 'insurance'))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }
        # Vérification de l'assurance des devis en FullService
        # - l'assurance doit être assurance incluse
        # - le type et le taux d'assurance ne sont pas modifiables
        # Vérification de l'assurance des contrats non FullService
        # -- le type et le taux négocié ne sont pas modifiables pour les clients en assurance facturée négociée (taux <> taux par défaut)
        # -- le taux par défaut est le seul taux possible
        if ($isFullService)
        {
            # Type non modifiable
            if (!$tabInsuranceTypeInfos->{'isIncluded'})
            {
                push(@{$tabErrors->{'fatal'}}, 0x1050);
                return 0;
            }
            # Taux non modifiable
            if ($insuranceRate != 0)
            {
                push(@{$tabErrors->{'fatal'}}, 0x1051);
                return 0;
            }
        }
        else
        {

            # Récupération des informations du pays
            my $tabCountryInfos = &LOC::Country::getInfos($countryId);

            # En création de lignes de devis
            if ($tabData->{'@action'} eq 'insert')
            {
                # Récupération des informations du client
                my $tabCustomerInfos = &LOC::Customer::getInfos($countryId, $customerId);

                # Vérification : le type et le taux ne doivent pas changer pour les clients en assurance facturée négociée
                if ($tabCustomerInfos->{'insurance.isInvoiced'} && $tabCustomerInfos->{'insurance.rate'} != $tabCountryInfos->{'insurance.defaultRate'})
                {
                    # Type non modifiable
                    if ($insuranceTypeId != $tabCustomerInfos->{'insuranceType.id'})
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x1050);
                        return 0;
                    }
                    # Taux non modifiable
                    if ($insuranceRate != $tabCustomerInfos->{'insurance.rate'})
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x1051);
                        return 0;
                    }
                }
                # Vérification : le taux par défaut est le seul taux possible
                elsif ($tabInsuranceTypeInfos->{'isInvoiced'} &&
                       $insuranceRate != $tabCountryInfos->{'insurance.defaultRate'})
                {
                    # Taux non modifiable
                    push(@{$tabErrors->{'fatal'}}, 0x1051);
                    return 0;
                }
            }
            # En modification de lignes de devis
            else
            {
                # Vérification : le type et le taux ne doivent pas changer pour les clients en assurance facturée négociée
                if ($tabOldInsuranceTypeInfos->{'isInvoiced'} &&
                    $oldInsuranceRate != $tabCountryInfos->{'insurance.defaultRate'})
                {
                    # Type non modifiable
                    if ($insuranceTypeId != $oldInsuranceTypeId)
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x1050);
                        return 0;
                    }
                    # Taux non modifiable
                    if ($insuranceRate != $oldInsuranceRate)
                    {
                        push(@{$tabErrors->{'fatal'}}, 0x1051);
                        return 0;
                    }
                }
                # Vérification : le taux par défaut est le seul taux possible
                if ($tabOldInsuranceTypeInfos->{'isInvoiced'} &&
                    $tabInsuranceTypeInfos->{'isInvoiced'} &&
                    $insuranceRate != $tabCountryInfos->{'insurance.defaultRate'})
                {
                    push(@{$tabErrors->{'fatal'}}, 0x1051);
                    return 0;
                }
            }
        }

        if (defined $tabEndUpds)
        {
            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDINSURANCE,
                'old'     => {
                    'type' => $oldInsuranceTypeId,
                    'rate' => $oldInsuranceRate
                },
                'new'     => {
                    'type' => $insuranceTypeId,
                    'rate' => $insuranceRate
                },
                'content' => 'type ' . $oldInsuranceTypeId . ', taux ' . ($oldInsuranceRate * 100) . ' %' .
                             ' → ' .
                             'type ' . $insuranceTypeId . ', taux ' . ($insuranceRate * 100) . ' %'
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, 'estimateLine', $estimateLineId, $tabTrace);
        }

        # Mise à jour de l'assurance
        $tabUpdates->{'DETAILSDEVISTYPEASSURANCE'} = $insuranceTypeId;
        $tabUpdates->{'DETAILSDEVISASSTAUX'} = ($tabInsuranceTypeInfos->{'isInvoiced'} ? $insuranceRate : 0);
    }

    # ---------------------------------
    # GESTION DE RECOURS

    my $oldAppealTypeId  = $tabRow->{'appealType.id'};
    my $appealTypeId = (exists $tabData->{'tabAppeal'}->{'typeId'} ? $tabData->{'tabAppeal'}->{'typeId'} : $oldAppealTypeId);

    my $tabAppealTypeInfos    = ($appealTypeId ? &LOC::Appeal::Type::getInfos($countryId, $appealTypeId) : undef);
    my $tabOldAppealTypeInfos = ($oldAppealTypeId ? &LOC::Appeal::Type::getInfos($countryId, $oldAppealTypeId) : undef);

    # - Modification de la gestion de recours
    if ($oldAppealTypeId != $appealTypeId)
    {
        # Vérification:
        if (defined $rights && !&_hasRight($rights, 'appeal'))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }

        # Vérification de la gestion de recours
        # - Gestion de recours sur un type d'assurance non éligible
        if ($tabInsuranceTypeInfos)
        {
            # - Gestion de recours sur un type d'assurance non éligible
            if (!$tabInsuranceTypeInfos->{'isAppealActivated'} && defined $appealTypeId)
            {
                push(@{$tabErrors->{'fatal'}}, 0x1053);
                return 0;
            }

            # - Pas de gestion de recours définie sur un type d'assurance éligible
            if ($tabInsuranceTypeInfos->{'isAppealActivated'} && !defined $appealTypeId)
            {
                push(@{$tabErrors->{'fatal'}}, 0x1053);
                return 0;
            }
        }

        my $oldAppealValue = $tabRow->{'appeal.value'} * 1;

        my $defaultAppealMode = &LOC::Characteristic::getAgencyValueByCode('RECOURSMODE', $agencyId, $valueDate) * 1;
        # Vérification du nouveau mode par rapport aux possibilités sur le contrat
        if ($tabAppealTypeInfos && $tabAppealTypeInfos->{'mode'} != LOC::Appeal::Type::MODE_NOTINVOICED && $tabAppealTypeInfos->{'mode'} != $defaultAppealMode)
        {
            # Pas les droits !
            push(@{$tabErrors->{'fatal'}}, 0x1053);
            return 0;
        }

        my $newAppealValue = undef;
        if ($defaultAppealMode == LOC::Common::APPEAL_MODE_PERCENT)
        {
            $newAppealValue = &LOC::Characteristic::getAgencyValueByCode('PCRECOURS', $agencyId, $valueDate) * 1;
        }

        # Historisation:
        if (defined $tabEndUpds)
        {
            my $tabCountryInfos = &LOC::Country::getInfos($countryId);
            my $locale = &LOC::Locale::getLocale($tabCountryInfos->{'locale.id'}, undef, [POSIX::LC_MESSAGES, POSIX::LC_CTYPE]);

            my $oldLabel = $tabOldAppealTypeInfos ? $tabOldAppealTypeInfos->{'fullLabel'} : $locale->t('Non facturable');
            $oldLabel =~ s/<%percent>/%s %%/;
            my $newLabel = $tabAppealTypeInfos ? $tabAppealTypeInfos->{'fullLabel'} : $locale->t('Non facturable');
            $newLabel =~ s/<%percent>/%s %%/;

            # Modification de la gestion de recours
            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDAPPEAL,
                'old'     => {
                    'type'  => $oldAppealTypeId,
                    'value' => $oldAppealValue
                },
                'new'     => {
                    'type'  => $appealTypeId,
                    'value' => $newAppealValue
                },
                'content' => (sprintf($oldLabel, $oldAppealValue * 100) .
                              ' → ' .
                              sprintf($newLabel, $newAppealValue * 100))
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, 'estimateLine', $estimateLineId, $tabTrace);
        }

        # Mise à jour des infos participation au recyclage
        $tabUpdates->{'DETAILSDEVISTYPERECOURS'} = $appealTypeId;
        $tabUpdates->{'DETAILSDEVISRECOURSVAL'}  = $newAppealValue;
    }

    # ---------------------------------
    # NETTOYAGE

    my $oldCleaningTypeId = $tabRow->{'cleaning.type.id'} * 1;
    my $oldCleaningAmount = $tabRow->{'cleaning.amount'} * 1;
    my $cleaningTypeId = (exists $tabData->{'tabCleaning'}->{'typeId'} ? $tabData->{'tabCleaning'}->{'typeId'} : $oldCleaningTypeId);
    my $cleaningAmount = (exists $tabData->{'tabCleaning'}->{'amount'} ? $tabData->{'tabCleaning'}->{'amount'} : $oldCleaningAmount);

    # Modification du nettoyage
    if ($cleaningTypeId != $oldCleaningTypeId || $cleaningAmount != $oldCleaningAmount)
    {
        # Vérification:
        if (defined $rights && !&_hasRight($rights, 'cleaning'))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }

        # Historisation:
        if (defined $tabEndUpds)
        {
            my $tabCountryInfos = &LOC::Country::getInfos($countryId);
            my $locale = &LOC::Locale::getLocale($tabCountryInfos->{'locale.id'}, undef, [POSIX::LC_MESSAGES, POSIX::LC_CTYPE]);

            my $subQuery = '(SELECT CAST(CONCAT("%s (", NETLIBELLE,")") AS CHAR CHARACTER SET utf8) FROM ' . $schemaName . '.NETTOYAGE WHERE NETAUTO = %d)';
            my $subQuery0 = '0 (' . $locale->t('Non facturé') . ')';
            my $query = '';
            if ($cleaningTypeId == 0)
            {
                $query = 'CONCAT(' . sprintf($subQuery, $oldCleaningAmount, $oldCleaningTypeId) . ', " → ", "' . $subQuery0 . '")';
            }
            elsif ($oldCleaningTypeId == 0)
            {
                $query = 'CONCAT("' . $subQuery0 . '", " → ", ' . sprintf($subQuery, $cleaningAmount, $cleaningTypeId) . ')';
            }
            else
            {
                $query = 'CONCAT(' . sprintf($subQuery, $oldCleaningAmount, $oldCleaningTypeId) . ', " → ", ' .
                                     sprintf($subQuery, $cleaningAmount, $cleaningTypeId) . ')';
            }

            my $tabTrace = {
                'schema'   => $schemaName,
                'code'     => LOC::Log::EventType::UPDCLEANING,
                'old'      => {
                    'amount' => $oldCleaningAmount,
                    'typeId' => $oldCleaningTypeId
                },
                'new'      => {
                    'amount' => $cleaningAmount,
                    'typeId' => $cleaningTypeId
                },
                'content'  => '',
                'sqlQuery' => $query
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, 'estimateLine', $estimateLineId, $tabTrace);
        }

        # Mise à jour des infos nettoyage
        $tabUpdates->{'DETAILSDEVISNETAUTO'}      = ($cleaningTypeId == 0 ? undef : $cleaningTypeId);
        $tabUpdates->{'DETAILSDEVISNETMONTANT'}   = $cleaningAmount;
        $tabUpdates->{'DETAILSDEVISNETMONTANTEU'} = &LOC::Util::round($cleaningAmount * $euroRate, 2);
    }

    # ---------------------------------
    # PARTICIPATION AU RECYCLAGE

    my $oldResiduesMode = $tabRow->{'residues.mode'} * 1;
    my $newResiduesMode = (exists $tabData->{'residues'}->{'mode'} ? $tabData->{'residues'}->{'mode'} : $oldResiduesMode);

    # - Modification de la participation au recyclage
    if ($oldResiduesMode != $newResiduesMode)
    {
        # Vérification:
        if (defined $rights && !&_hasRight($rights, 'residues'))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }

        my $oldResiduesValue = $tabRow->{'residues.value'} * 1;

        my $defaultResiduesMode = &LOC::Characteristic::getAgencyValueByCode('DECHETSMODE', $agencyId, $valueDate) * 1;
        # Vérification du nouveau mode par rapport aux possibilités sur le contrat
        if ($newResiduesMode != LOC::Common::RESIDUES_MODE_NONE && $newResiduesMode != $defaultResiduesMode)
        {
            # Pas les droits !
            push(@{$tabErrors->{'fatal'}}, 0x1052);
            return 0;
        }

        my $newResiduesValue = undef;
        if ($newResiduesMode == LOC::Common::RESIDUES_MODE_PRICE)
        {
            $newResiduesValue = &LOC::Characteristic::getAgencyValueByCode('MTDECHETS', $agencyId, $valueDate) * 1;
        }
        elsif ($newResiduesMode == LOC::Common::RESIDUES_MODE_PERCENT)
        {
            $newResiduesValue = &LOC::Characteristic::getAgencyValueByCode('PCDECHETS', $agencyId, $valueDate) * 1;
        }

        # Historisation:
        if (defined $tabEndUpds)
        {
            my $tabCountryInfos = &LOC::Country::getInfos($countryId);
            my $locale = &LOC::Locale::getLocale($tabCountryInfos->{'locale.id'}, undef, [POSIX::LC_MESSAGES, POSIX::LC_CTYPE]);

            my $tabOldResiduesInvoiceInfos = {};
            if ($oldResiduesMode == LOC::Common::RESIDUES_MODE_PRICE)
            {
                $tabOldResiduesInvoiceInfos = {
                    'type' => $locale->t('Montant forfaitaire'),
                    'format'  => $oldResiduesValue
                };
            }
            elsif ($oldResiduesMode == LOC::Common::RESIDUES_MODE_PERCENT)
            {
                $tabOldResiduesInvoiceInfos = {
                    'type' => $locale->t('Pourcentage'),
                    'format'  => $oldResiduesValue * 100 . '%'
                };
            }

            my $tabNewResiduesInvoiceInfos = {};
            if ($newResiduesMode == LOC::Common::RESIDUES_MODE_PRICE)
            {
                $tabNewResiduesInvoiceInfos = {
                    'type' => $locale->t('Montant forfaitaire'),
                    'format'  => $newResiduesValue
                };
            }
            elsif ($newResiduesMode == LOC::Common::RESIDUES_MODE_PERCENT)
            {
                $tabNewResiduesInvoiceInfos = {
                    'type' => $locale->t('Pourcentage'),
                    'format'  => $newResiduesValue * 100 . '%'
                };
            }

            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDRESIDUESMODE,
                'old'     => {
                    'mode' => $oldResiduesMode,
                },
                'new'     => {
                    'mode' => $newResiduesMode
                },
                'content' => ($oldResiduesMode ? sprintf('%s : %s', $tabOldResiduesInvoiceInfos->{'type'}, $tabOldResiduesInvoiceInfos->{'format'}) :
                                                      $locale->t('Non facturé'))  .
                             ' → ' .
                             ($newResiduesMode ?  sprintf('%s : %s', $tabNewResiduesInvoiceInfos->{'type'}, $tabNewResiduesInvoiceInfos->{'format'}) :
                                                      $locale->t('Non facturé'))
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, 'estimateLine', $estimateLineId, $tabTrace);
        }

        # Mise à jour des infos participation au recyclage
        $tabUpdates->{'DETAILSDEVISDECHETSFACT'} = -$newResiduesMode;
        $tabUpdates->{'DETAILSDEVISDECHETSVAL'}  = $newResiduesValue;
    }



    if (keys %$tabUpdates > 0)
    {
        if ($db->update('DETAILSDEVIS', $tabUpdates, {'DETAILSDEVISAUTO*' => $estimateLineId}, $schemaName) == -1)
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }
    return 1;
}


# Function: _updateLineRentServices
# Mise à jour des services de location et de transport
#
# Parameters:
# LOC::Db::Adapter $db                   - Connexion à la base de données
# string           $countryId            - Pays
# int              $estimateLineId       - Id de la ligne de devis
# arrayref         $tabRentServices      - Tableau des services de location et transport
# float            $rentServiceMinAmount - Montant minimum d'un service pour l'utilisateur en cours
# bool             $hasAddRight          - Indique si on a droit ou pas à l'ajout de services
# bool             $hasEditRight         - Indique si on a droit ou pas à la modification de services
# int              $userId               - Utilisateur responsable de la modification
# hashref          $tabErrors            - Tableau des erreurs
# hashref          $tabEndUpds           - Mises à jour de fin
#
# Returns:
# bool - 1 si tout est ok, sinon 0
sub _updateLineRentServices
{
    my ($db, $countryId, $estimateLineId, $tabRentServices, $rentServiceMinAmount, $tabRights, $userId, $tabErrors, $tabEndUpds) = @_;

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my $query = '
SELECT
    `res_id` AS `id`,
    `rsr_id` AS `rentService.id`,
    `res_sta_id` AS `state.id`,
    `rsr_name` AS `rentService.name`,
    `res_comment` AS `comment`,
    `res_amount` AS `amount`
FROM f_rentestimateline_rentservice
LEFT JOIN ' . $schemaName . '.p_rentservice
ON rsr_id = res_rsr_id
WHERE res_rel_id = ' . $estimateLineId . ';';
    my $tabOldRentServices = $db->fetchAssoc($query, {}, 'id');
    if (!$tabOldRentServices)
    {
        # Erreur fatale !
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    my $nbRentServices = @$tabRentServices;
    for (my $i = 0; $i < $nbRentServices; $i++)
    {
        my $id = $tabRentServices->[$i]->{'id'};
        if ($id && !exists $tabOldRentServices->{$id})
        {
            # Erreur fatale !
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }

        # Formatage des données de comparaison
        $tabOldRentServices->{$id}->{'amount'} *= 1;
        $tabRentServices->[$i]->{'amount'} *= 1;
        $tabOldRentServices->{$id}->{'rentService.id'} *= 1;
        $tabRentServices->[$i]->{'rentService.id'} *= 1;
        $tabRentServices->[$i]->{'comment'} = &LOC::Util::trim($tabRentServices->[$i]->{'comment'});

        # Si c'est une mise à jour et que le montant est changé
        if ($id && $tabOldRentServices->{$id}->{'amount'} != $tabRentServices->[$i]->{'amount'})
        {
            # Vérification que le nouveau montant respecte le seuil minimum autorisé pour la personne
            if (defined($rentServiceMinAmount) && $tabRentServices->[$i]->{'amount'} < $rentServiceMinAmount)
            {
                # Erreur fatale !
                push(@{$tabErrors->{'fatal'}}, 0x0001);
                return 0;
            }
        }

        my $hasModifications;
        if ($id)
        {
            $hasModifications = ($tabOldRentServices->{$id}->{'state.id'} ne $tabRentServices->[$i]->{'state.id'} ||
                                 $tabOldRentServices->{$id}->{'rentService.id'} != $tabRentServices->[$i]->{'rentService.id'} ||
                                 $tabOldRentServices->{$id}->{'comment'} ne $tabRentServices->[$i]->{'comment'} ||
                                 $tabOldRentServices->{$id}->{'amount'} != $tabRentServices->[$i]->{'amount'});
            if ($hasModifications)
            {
                # Vérification des droits
                if (defined $tabRights && !&_hasRight($tabRights, 'editRentServices'))
                {
                    # Pas les droits !
                    push(@{$tabErrors->{'fatal'}}, 0x0003);
                    return 0;
                }
                my $tabUpdates = {'res_sta_id'  => $tabRentServices->[$i]->{'state.id'},
                                  'res_rsr_id'  => $tabRentServices->[$i]->{'rentService.id'},
                                  'res_comment' => $tabRentServices->[$i]->{'comment'},
                                  'res_amount'  => $tabRentServices->[$i]->{'amount'}};
                if ($db->update('f_rentestimateline_rentservice', $tabUpdates, {'res_id' => $id}, $schemaName) == -1)
                {
                    # Erreur fatale !
                    push(@{$tabErrors->{'fatal'}}, 0x0001);
                    return 0;
                }

                # Traces
                if (defined($tabEndUpds))
                {
                    if ($tabOldRentServices->{$id}->{'state.id'} ne $tabRentServices->[$i]->{'state.id'} &&
                        $tabRentServices->[$i]->{'state.id'} eq LOC::Common::RentService::STATE_DELETED)
                    {
                        my $tabTrace = {
                            'schema'  => $schemaName,
                            'code'    => LOC::Log::EventType::DELRENTSERVICE,
                            'old'     => {
                                'name'   => $tabOldRentServices->{$id}->{'rentService.name'},
                                'amount' => $tabOldRentServices->{$id}->{'amount'}
                            },
                            'new'     => undef,
                            'content' => sprintf('%s (%s)', $tabOldRentServices->{$id}->{'rentService.name'},
                                                            $tabOldRentServices->{$id}->{'amount'})
                        };
                        &LOC::EndUpdates::addTrace($tabEndUpds, 'estimateLine', $estimateLineId, $tabTrace);
                    }
                    else
                    {
                        my $tabTrace = {
                            'schema'  => $schemaName,
                            'code'    => LOC::Log::EventType::UPDRENTSERVICE,
                            'old'     => {
                                'name'    => $tabOldRentServices->{$id}->{'rentService.name'},
                                'comment' => $tabOldRentServices->{$id}->{'comment'},
                                'amount'  => $tabOldRentServices->{$id}->{'amount'}
                            },
                            'new'     => {
                                'name'    => $tabRentServices->[$i]->{'rentService.name'},
                                'comment' => $tabRentServices->[$i]->{'comment'},
                                'amount'  => $tabRentServices->[$i]->{'amount'}
                            },
                            'content' => sprintf('%s (%s) → %s (%s)', $tabOldRentServices->{$id}->{'rentService.name'},
                                                                       $tabOldRentServices->{$id}->{'amount'},
                                                                       $tabRentServices->[$i]->{'rentService.name'},
                                                                       $tabRentServices->[$i]->{'amount'})
                        };
                        &LOC::EndUpdates::addTrace($tabEndUpds, 'estimateLine', $estimateLineId, $tabTrace);
                    }
                }
            }
        }
        else
        {
            $hasModifications = 1;

            # Vérification des droits
            if (defined $tabRights && !&_hasRight($tabRights, 'addRentServices'))
            {
                # Pas les droits !
                push(@{$tabErrors->{'fatal'}}, 0x0003);
                return 0;
            }
            my $tabUpdates = {'res_sta_id'  => $tabRentServices->[$i]->{'state.id'},
                              'res_rsr_id'  => $tabRentServices->[$i]->{'rentService.id'},
                              'res_rel_id'  => $estimateLineId,
                              'res_comment' => $tabRentServices->[$i]->{'comment'},
                              'res_amount'  => $tabRentServices->[$i]->{'amount'},
                              'res_cur_id*' => '(SELECT cty_cur_id FROM frmwrk.p_country WHERE cty_id = "' . $countryId . '")'};
            if ($db->insert('f_rentestimateline_rentservice', $tabUpdates, $schemaName) == -1)
            {
                # Erreur fatale !
                push(@{$tabErrors->{'fatal'}}, 0x0001);
                return 0;
            }

            # Traces
            if (defined($tabEndUpds))
            {
                if ($tabRentServices->[$i]->{'state.id'} eq LOC::Common::RentService::STATE_UNBILLED)
                {
                    my $tabTrace = {
                        'schema'  => $schemaName,
                        'code'    => LOC::Log::EventType::ADDRENTSERVICE,
                        'old'     => undef,
                        'new'     => {
                            'name'   => $tabRentServices->[$i]->{'rentService.name'},
                            'amount' => $tabRentServices->[$i]->{'amount'}
                        },
                        'content' => sprintf('%s (%s)', $tabRentServices->[$i]->{'rentService.name'},
                                                        $tabRentServices->[$i]->{'amount'})
                    };
                    &LOC::EndUpdates::addTrace($tabEndUpds, 'estimateLine', $estimateLineId, $tabTrace);
                }
            }
        }
    }

    return 1;
}

1;