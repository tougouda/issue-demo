use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::TariffFamily
# Module permettant d'obtenir les informations sur les familles tarifaires
package LOC::TariffFamily;

# Constants: Récupération d'information sur la famille tarifaire
# GETINFOS_ALL     : Récupération de l'ensemble des informations
# GETINFOS_BASE    : Récupération des informations de base
# GETINFOS_TARIFFS : Récupération des informations tarifaires
use constant {
    GETINFOS_ALL     => 0xFFFF,
    GETINFOS_BASE    => 0x0000,
    GETINFOS_TARIFFS => 0x0001
};

# Constants: Familles tarifaires définies
# ID_SUBRENT - Famille tarifaire pour la sous-location
use constant
{
    ID_SUBRENT  => 75
};

use strict;

# Liste des erreurs:
#
# 0x0001 => Erreur fatale
# 0x0002 => Quantité de carburant non renseignée


# Modules internes
use LOC::Db;
use LOC::Tariff::Rent;


# Function: getInfos
# Retourne les infos d'une famille tarifaire
# 
# Contenu du hashage :
# - id              => identifiant de la famille de machines
# - code            => code interne au contrôle de gestion
# - label           => libellé
# - sublabel        => sous-libellé
# - fulllabel       => libellé complet
# - order           => ordre d'affichage
# - cashCoefficient => coefficient utilisé dans le calcul du coût cash
# - isYieldable     => famille tarifaire yieldable ou non
#
#
# Parameters:
# string $id        - Identifiant de la famille de machines
# string $countryId - Identifiant du pays
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);
    return ($tab && exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Retourne la liste des familles tarifaires
#
# Contenu d'une ligne du hashage :
# - id          => identifiant ;
# - code        => code interne au contrôle de gestion
# - label       => libellé ;
# - sublabel    => sous-libellé ;
# - fulllabel   => libellé complet ;
# - ordre       => ordre d'affichage ;
# - cashRatio   => ratio cash ;
# - isYieldable => famille tarifaire yieldable ou non ;
# - nbElements  => nombre d'éléments dans l'ensemble de familles tarifaires ;
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $format     - Format de retour de la liste
# string  $tabFilters - Liste des filtres (id : identifiant de la famille tarifaire)
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires
#             sort       Tri
#                 field     Colonne de tri
#                 dir       Sens du tri
#
# Returns:
# hash|hashref|0 - Liste des familles tarifaires
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;

    # Connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Filtres 
    my $whereQuery = '';
    my $whereQueryAddId = '';
    
    # Ajout d'une famille tarifaire
    if ($tabFilters->{'addId'})
    {
        $whereQueryAddId = ' OR tbl_p_tarifffamily.FAMTARAUTO = ' . $tabFilters->{'addId'};
    }
    if ($tabFilters->{'id'} ne '')
    {
        $whereQuery .= '
AND tbl_p_tarifffamily.FAMTARAUTO IN (' . $db->quote($tabFilters->{'id'}) . ')';
    }

    # Tri
    my $orderQuery = '';
    if (defined $tabOptions->{'sort'})
    {
        my @tabOrders;
        foreach my $orderer (@{$tabOptions->{'sort'}}) {
          push (@tabOrders, $orderer->{'field'} . ' ' . $orderer->{'dir'}) ;
        }
        if(@tabOrders > 0)
        {
            $orderQuery = ' ORDER BY ' . join(', ', @tabOrders) . ';';
        }
    }
    else
    { # Tri par default
        $orderQuery = ' ORDER BY tbl_p_tarifffamily.FAMTARORDRE;';
    }

    # Récupère les données concernant le pays
    my $query = '';
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    tbl_p_tarifffamily.FAMTARAUTO AS `id`,
    tbl_p_tarifffamily.FAMTARCODE AS `code`,
    tbl_p_tarifffamily.FAMTARLIBELLE AS `label`,
    tbl_p_tarifffamily.FAMTARSOUSLIBELLE AS `sublabel`,
    CONCAT_WS(" ", tbl_p_tarifffamily.FAMTARLIBELLE, tbl_p_tarifffamily.FAMTARSOUSLIBELLE) AS `fulllabel`,
    tbl_p_tarifffamily.FAMTARORDRE AS `order`,
    tbl_p_tarifffamily.FAMTARCOEFCASH AS `cashRatio`,
    tbl_p_tarifffamily.FAMTARCARBUQTEJOUR AS `family.estimateFuel`,
    tbl_p_tarifffamily.FAMTARCARBUQTEMAX AS `family.fuelMax`,
    IFNULL(GROUP_CONCAT(DISTINCT FM.FAMAENERGIE), "n/a") AS `family.energy`,
    tbl_p_tarifffamily.FAMTARYIELDABLE AS `isYieldable`
FROM FAMILLETARIFAIRE tbl_p_tarifffamily
LEFT JOIN AUTH.MODELEMACHINE MM ON MM.FAMTARAUTO = tbl_p_tarifffamily.FAMTARAUTO
LEFT JOIN FAMILLEMACHINE FM ON FM.FAMAAUTO = MM.FAMAAUTO
LEFT JOIN TARIF tbl_f_tariff ON tbl_p_tarifffamily.FAMTARAUTO = tbl_f_tariff.FAMTARAUTO

WHERE (tbl_f_tariff.ETCODE = "GEN01"' .
$whereQuery . ')' . $whereQueryAddId;
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*) AS `label`
FROM FAMILLETARIFAIRE tbl_p_tarifffamily
LEFT JOIN TARIF tbl_f_tariff ON tbl_p_tarifffamily.FAMTARAUTO = tbl_f_tariff.FAMTARAUTO
WHERE (tbl_f_tariff.ETCODE = "GEN01"' .
$whereQuery . ')' . $whereQueryAddId;
    }
    else
    {
        $query = '
SELECT
    tbl_p_tarifffamily.FAMTARAUTO AS `id`,
    CONCAT_WS(" ", tbl_p_tarifffamily.FAMTARLIBELLE, tbl_p_tarifffamily.FAMTARSOUSLIBELLE) AS `fulllabel`
FROM FAMILLETARIFAIRE tbl_p_tarifffamily
LEFT JOIN TARIF tbl_f_tariff ON tbl_p_tarifffamily.FAMTARAUTO = tbl_f_tariff.FAMTARAUTO
WHERE (tbl_f_tariff.ETCODE = "GEN01"' .
$whereQuery . ')' . $whereQueryAddId ;
    }

    $query .= '
GROUP BY tbl_p_tarifffamily.FAMTARAUTO ' .
$orderQuery; 

    my $dataRef = $db->fetchList($format, $query, {}, 'id');
    unless ($dataRef)
    {
        return 0;
    }

    # Récupération du nombre d'éléments dans chaque groupe de familles tarifaires
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT FLOOR(tbl_p_tarifffamily.FAMTARORDRE / 100)
FROM FAMILLETARIFAIRE tbl_p_tarifffamily
LEFT JOIN TARIF tbl_f_tariff ON tbl_p_tarifffamily.FAMTARAUTO = tbl_f_tariff.FAMTARAUTO
WHERE tbl_f_tariff.ETCODE = "GEN01"
GROUP BY tbl_p_tarifffamily.FAMTARAUTO';

        my $tabElements = $db->fetchCol($query);
        my $tabOrders = {};

        foreach my $order (@$tabElements)
        {
            $tabOrders->{$order}++;
        }

        foreach my $line (values(%$dataRef))
        {
            $line->{'nbElements'} = $tabOrders->{($line->{'order'} - $line->{'order'} % 100) / 100};

            # - Récupération tarifs de base
            if ($flags & GETINFOS_TARIFFS)
            {
                $line->{'tabTariffs'} = &LOC::Tariff::Rent::getBaseTariffs($countryId, {'tariffFamily.id' => $line->{'id'}});
            }
        }
    }

    return (wantarray ? %$dataRef : $dataRef);
}

# Function: getUseRate
# Retourne le taux d'utilisation de la famille de machines dans l'agence et dans le pays
# 
# Parameters:
# string $id       - Identifiant de la famille de machines
# string $agencyId - Identifiant de l'agence
#
# Returns:
# hash|float|0 - Retourne le taux d'utilisation ('useRate'), le nombre de machines louées ('nbRented') et
#                le nombre total de machines ('nbTotal') dans l'agence ('agency'),la région ('area')
#                et dans le pays ('country') ou 0 si pas de résultat
sub getUseRate
{
    my ($id, $agencyId) = @_;

    # connexion à AUTH
    my $db = &LOC::Db::getConnection('auth');

    # Récupère les données concernant la machine
    my $query = '
SELECT
    IFNULL(SUM(SM.STMOLOU) + SUM(SM.STMOLIV), 0) AS `nbRented`,
    IFNULL(SUM(SM.STMOTOTAL), 0) AS `nbTotal`,
    agc_cty_id AS `agency.country.id`,
    agc_are_id AS `agency.area.id`
FROM STOCKMODELE SM
INNER JOIN frmwrk.p_agency
ON agc_id = SM.AGAUTO
LEFT JOIN MODELEMACHINE MM
ON SM.MOMAAUTO = MM.MOMAAUTO
WHERE MM.FAMTARAUTO = ' . $db->quote($id) . ' AND SM.AGAUTO = ' . $db->quote($agencyId) . ';';
    my $dataRef = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    unless ($dataRef)
    {
        return 0;
    }

    # Retour sous forme de tableau ?
    my $useRate = &LOC::Util::round(($dataRef->{'nbTotal'} != 0 ? $dataRef->{'nbRented'} / $dataRef->{'nbTotal'} : 0), 4);
    if (wantarray)
    {
        my %tabResult = (
            'nbRented' => {
                'agency' => $dataRef->{'nbRented'} * 1,
                'area'   => undef,
                'country' => undef
            },
            'nbTotal'  => {
                'agency' => $dataRef->{'nbTotal'} * 1,
                'area'   => undef,
                'country' => undef
            },
            'useRate'  => {
                'agency' => $useRate,
                'area'   => undef,
                'country' => undef
            }
        );
        my $countryId = $dataRef->{'agency.country.id'};
        my $areaId    = $dataRef->{'agency.area.id'} * 1;

        # Calcul du taux d'utilisation instantané dans la région et le pays
        my $quotedAreaId    = $db->quote($areaId);
        my $quotedCountryId = $db->quote($countryId);
        $query = '
SELECT
    IFNULL(SUM(IF(agc_are_id = ' . $quotedAreaId . ', SM.STMOLOU, 0))
            + SUM(IF(agc_are_id = ' . $quotedAreaId . ', SM.STMOLIV, 0)), 0) AS `area.nbRented`,
    IFNULL(SUM(IF(agc_are_id = ' . $quotedAreaId . ', SM.STMOTOTAL, 0)), 0) AS `area.nbTotal`,
    IFNULL(SUM(IF(agc_cty_id = ' . $quotedCountryId . ', SM.STMOLOU, 0))
            + SUM(IF(agc_cty_id = ' . $quotedCountryId . ', SM.STMOLIV, 0)), 0) AS `country.nbRented`,
    IFNULL(SUM(IF(agc_cty_id = ' . $quotedCountryId . ', SM.STMOTOTAL, 0)), 0) AS `country.nbTotal`
FROM STOCKMODELE SM
INNER JOIN frmwrk.p_agency
ON agc_id = SM.AGAUTO AND
   (agc_are_id = ' . $quotedAreaId . ' OR agc_cty_id = ' . $quotedCountryId . ') AND
   agc_type = ' . $db->quote(LOC::Agency::TYPE_AGENCY) . ' AND
   agc_sta_id <> ' . $db->quote(LOC::Agency::STATE_DELETED) . '
LEFT JOIN MODELEMACHINE MM
ON SM.MOMAAUTO = MM.MOMAAUTO
WHERE MM.FAMTARAUTO = ' . $db->quote($id) . ';';
        $dataRef = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
        if ($areaId)
        {
            $tabResult{'nbRented'}->{'area'} = $dataRef->{'area.nbRented'} * 1;
            $tabResult{'nbTotal'}->{'area'}  = $dataRef->{'area.nbTotal'} * 1;
            $tabResult{'useRate'}->{'area'}  = &LOC::Util::round(($dataRef->{'area.nbTotal'} != 0 ?
                                                    $dataRef->{'area.nbRented'} / $dataRef->{'area.nbTotal'} : 0), 4);
        }
        $tabResult{'nbRented'}->{'country'} = $dataRef->{'country.nbRented'} * 1;
        $tabResult{'nbTotal'}->{'country'}  = $dataRef->{'country.nbTotal'} * 1;
        $tabResult{'useRate'}->{'country'}  = &LOC::Util::round(($dataRef->{'country.nbTotal'} != 0 ?
                                                  $dataRef->{'country.nbRented'} / $dataRef->{'country.nbTotal'} : 0), 4);

        return %tabResult;
    }
    else
    {
        return $useRate;
    }
}

# Function: getUseRateData
# Retourne les informations nécessaires à la visualisation des taux d'utilisation par région
# 
# Parameters:
# string $countryId - Identifiant du pays
# int    $areaId    - Identifiant de la région
#
# Returns:
# hash|float|0 - Retourne le taux d'utilisation ('useRate'), le nombre de machines louées ('nbRented') et
#                le nombre total de machines ('nbTotal') dans l'agence ('agency'),la région ('area')
#                et dans le pays ('country') ou 0 si pas de résultat
sub getUseRateData
{
    my ($countryId, $areaId, $tabAgency, $tabTariffFamily, $tabTariffFamilyToExclude) = @_;

    unless (defined $countryId)
    {
        return 0;
    }

    # Connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Récupération des taux d'utilisation par famille tarifaire
    my $query = '
SELECT
    tbl_f_stock.STMOAUTO AS `id`,
    tbl_f_stock.AGAUTO AS `agency.id`,
    agc_are_id AS `area.id`,
    tbl_p_model.FAMTARAUTO AS `tariffFamily.id`,
    SUM(tbl_f_stock.STMOTOTAL) AS `nbTotal`,
    SUM(tbl_f_stock.STMOLOU) AS `nbRented`,
    SUM(tbl_f_stock.STMODIS) AS `nbAvailable`,
    SUM(tbl_f_stock.STMOREC) AS `nbRecovery`,
    SUM(tbl_f_stock.STMOREV) AS `nbReview`,
    SUM(tbl_f_stock.STMOCON) AS `nbControl`,
    SUM(tbl_f_stock.STMOPAN) AS `nbBroken`,
    SUM(tbl_f_stock.STMOVER) AS `nbCheck`,
    SUM(tbl_f_stock.STMOLIV) AS `nbDelivery`
FROM AUTH.STOCKMODELE tbl_f_stock
INNER JOIN AUTH.MODELEMACHINE tbl_p_model ON tbl_f_stock.MOMAAUTO = tbl_p_model.MOMAAUTO
LEFT JOIN frmwrk.p_agency ON CONVERT(tbl_f_stock.AGAUTO USING utf8) = agc_id
WHERE agc_cty_id = ' . $db->quote($countryId) . '
    AND agc_type = ' . $db->quote(LOC::Agency::TYPE_AGENCY) . '
    AND tbl_p_model.FAMTARAUTO NOT IN ("' . join('", "', @$tabTariffFamilyToExclude) . '") 
GROUP BY tbl_f_stock.AGAUTO, tbl_p_model.FAMTARAUTO';
    my $tabStock = $db->fetchAssoc($query, {}, 'id');

    # Stockage des nombres de machines
    my @tabAgencyId = keys(%$tabAgency);
    my $tabData = {};
    foreach my $tabStockLine (values(%$tabStock))
    {
        # Par état
        $tabData->{'nbTotal'}->{'total'}->{'nb'}     += $tabStockLine->{'nbTotal'};
        $tabData->{'nbRented'}->{'total'}->{'nb'}    += $tabStockLine->{'nbRented'};
        $tabData->{'nbAvailable'}->{'total'}->{'nb'} += $tabStockLine->{'nbAvailable'};
        $tabData->{'nbRecovery'}->{'total'}->{'nb'}  += $tabStockLine->{'nbRecovery'};
        $tabData->{'nbReview'}->{'total'}->{'nb'}    += $tabStockLine->{'nbReview'};
        $tabData->{'nbControl'}->{'total'}->{'nb'}   += $tabStockLine->{'nbControl'};
        $tabData->{'nbBroken'}->{'total'}->{'nb'}    += $tabStockLine->{'nbBroken'};
        $tabData->{'nbCheck'}->{'total'}->{'nb'}     += $tabStockLine->{'nbCheck'};
        $tabData->{'nbDelivery'}->{'total'}->{'nb'}  += $tabStockLine->{'nbDelivery'};

        if ($areaId && $tabStockLine->{'area.id'} == $areaId)
        {
            $tabData->{'nbTotal'}->{'area'}->{'nb'}     += $tabStockLine->{'nbTotal'};
            $tabData->{'nbRented'}->{'area'}->{'nb'}    += $tabStockLine->{'nbRented'};
            $tabData->{'nbAvailable'}->{'area'}->{'nb'} += $tabStockLine->{'nbAvailable'};
            $tabData->{'nbRecovery'}->{'area'}->{'nb'}  += $tabStockLine->{'nbRecovery'};
            $tabData->{'nbReview'}->{'area'}->{'nb'}    += $tabStockLine->{'nbReview'};
            $tabData->{'nbControl'}->{'area'}->{'nb'}   += $tabStockLine->{'nbControl'};
            $tabData->{'nbBroken'}->{'area'}->{'nb'}    += $tabStockLine->{'nbBroken'};
            $tabData->{'nbCheck'}->{'area'}->{'nb'}     += $tabStockLine->{'nbCheck'};
            $tabData->{'nbDelivery'}->{'area'}->{'nb'}  += $tabStockLine->{'nbDelivery'};
        }

        if (&LOC::Util::in_array($tabStockLine->{'agency.id'}, \@tabAgencyId))
        {
            $tabData->{'nbTotal'}->{$tabStockLine->{'agency.id'}}->{'nb'}     += $tabStockLine->{'nbTotal'};
            $tabData->{'nbRented'}->{$tabStockLine->{'agency.id'}}->{'nb'}    += $tabStockLine->{'nbRented'};
            $tabData->{'nbAvailable'}->{$tabStockLine->{'agency.id'}}->{'nb'} += $tabStockLine->{'nbAvailable'};
            $tabData->{'nbRecovery'}->{$tabStockLine->{'agency.id'}}->{'nb'}  += $tabStockLine->{'nbRecovery'};
            $tabData->{'nbReview'}->{$tabStockLine->{'agency.id'}}->{'nb'}    += $tabStockLine->{'nbReview'};
            $tabData->{'nbControl'}->{$tabStockLine->{'agency.id'}}->{'nb'}   += $tabStockLine->{'nbControl'};
            $tabData->{'nbBroken'}->{$tabStockLine->{'agency.id'}}->{'nb'}    += $tabStockLine->{'nbBroken'};
            $tabData->{'nbCheck'}->{$tabStockLine->{'agency.id'}}->{'nb'}     += $tabStockLine->{'nbCheck'};
            $tabData->{'nbDelivery'}->{$tabStockLine->{'agency.id'}}->{'nb'}  += $tabStockLine->{'nbDelivery'};
        }

        # Par famille tarifaire
        $tabData->{$tabStockLine->{'tariffFamily.id'}}->{'total'}->{'nbTotal'}    += $tabStockLine->{'nbTotal'};
        $tabData->{$tabStockLine->{'tariffFamily.id'}}->{'total'}->{'nbRented'}   += $tabStockLine->{'nbRented'};
        $tabData->{$tabStockLine->{'tariffFamily.id'}}->{'total'}->{'nbDelivery'} += $tabStockLine->{'nbDelivery'};
        $tabData->{'total'}->{'total'}->{'nbTotal'}    += $tabStockLine->{'nbTotal'};
        $tabData->{'total'}->{'total'}->{'nbRented'}   += $tabStockLine->{'nbRented'};
        $tabData->{'total'}->{'total'}->{'nbDelivery'} += $tabStockLine->{'nbDelivery'};

        if ($areaId && $tabStockLine->{'area.id'} == $areaId)
        {
            $tabData->{$tabStockLine->{'tariffFamily.id'}}->{'area'}->{'nbTotal'}    += $tabStockLine->{'nbTotal'};
            $tabData->{$tabStockLine->{'tariffFamily.id'}}->{'area'}->{'nbRented'}   += $tabStockLine->{'nbRented'};
            $tabData->{$tabStockLine->{'tariffFamily.id'}}->{'area'}->{'nbDelivery'} += $tabStockLine->{'nbDelivery'};
            $tabData->{'total'}->{'area'}->{'nbTotal'}    += $tabStockLine->{'nbTotal'};
            $tabData->{'total'}->{'area'}->{'nbRented'}   += $tabStockLine->{'nbRented'};
            $tabData->{'total'}->{'area'}->{'nbDelivery'} += $tabStockLine->{'nbDelivery'};
        }

        if (&LOC::Util::in_array($tabStockLine->{'agency.id'}, \@tabAgencyId))
        {
            $tabData->{$tabStockLine->{'tariffFamily.id'}}->{$tabStockLine->{'agency.id'}}->{'nbTotal'}
                    += $tabStockLine->{'nbTotal'};
            $tabData->{$tabStockLine->{'tariffFamily.id'}}->{$tabStockLine->{'agency.id'}}->{'nbRented'}
                    += $tabStockLine->{'nbRented'};
            $tabData->{$tabStockLine->{'tariffFamily.id'}}->{$tabStockLine->{'agency.id'}}->{'nbDelivery'}
                    += $tabStockLine->{'nbDelivery'};
            $tabData->{'total'}->{$tabStockLine->{'agency.id'}}->{'nbTotal'}    += $tabStockLine->{'nbTotal'};
            $tabData->{'total'}->{$tabStockLine->{'agency.id'}}->{'nbRented'}   += $tabStockLine->{'nbRented'};
            $tabData->{'total'}->{$tabStockLine->{'agency.id'}}->{'nbDelivery'} += $tabStockLine->{'nbDelivery'};
        }
    }

    # Calcul des pourcentages
    # Par état
    my $nbTotal = $tabData->{'nbTotal'}->{'total'}->{'nb'};
    $tabData->{'nbRented'}->{'total'}->{'pc'}
            = ($nbTotal ? 100 * $tabData->{'nbRented'}->{'total'}->{'nb'} / $nbTotal : 0);
    $tabData->{'nbAvailable'}->{'total'}->{'pc'}
            = ($nbTotal ? 100 * $tabData->{'nbAvailable'}->{'total'}->{'nb'} / $nbTotal : 0);
    $tabData->{'nbRecovery'}->{'total'}->{'pc'}
            = ($nbTotal ? 100 * $tabData->{'nbRecovery'}->{'total'}->{'nb'} / $nbTotal : 0);
    $tabData->{'nbReview'}->{'total'}->{'pc'}
            = ($nbTotal ? 100 * $tabData->{'nbReview'}->{'total'}->{'nb'} / $nbTotal : 0);
    $tabData->{'nbControl'}->{'total'}->{'pc'}
            = ($nbTotal ? 100 * $tabData->{'nbControl'}->{'total'}->{'nb'} / $nbTotal : 0);
    $tabData->{'nbBroken'}->{'total'}->{'pc'}
            = ($nbTotal ? 100 * $tabData->{'nbBroken'}->{'total'}->{'nb'} / $nbTotal : 0);
    $tabData->{'nbCheck'}->{'total'}->{'pc'}
            = ($nbTotal ? 100 * $tabData->{'nbCheck'}->{'total'}->{'nb'} / $nbTotal : 0);
    $tabData->{'nbDelivery'}->{'total'}->{'pc'}
            = ($nbTotal ? 100 * $tabData->{'nbDelivery'}->{'total'}->{'nb'} / $nbTotal : 0);

    my $nbTotalArea = $tabData->{'nbTotal'}->{'area'}->{'nb'};
    $tabData->{'nbRented'}->{'area'}->{'pc'}
            = ($nbTotalArea ? 100 * $tabData->{'nbRented'}->{'area'}->{'nb'} / $nbTotalArea : 0);
    $tabData->{'nbAvailable'}->{'area'}->{'pc'}
            = ($nbTotalArea ? 100 * $tabData->{'nbAvailable'}->{'area'}->{'nb'} / $nbTotalArea : 0);
    $tabData->{'nbRecovery'}->{'area'}->{'pc'}
            = ($nbTotalArea ? 100 * $tabData->{'nbRecovery'}->{'area'}->{'nb'} / $nbTotalArea : 0);
    $tabData->{'nbReview'}->{'area'}->{'pc'}
            = ($nbTotalArea ? 100 * $tabData->{'nbReview'}->{'area'}->{'nb'} / $nbTotalArea : 0);
    $tabData->{'nbControl'}->{'area'}->{'pc'}
            = ($nbTotalArea ? 100 * $tabData->{'nbControl'}->{'area'}->{'nb'} / $nbTotalArea : 0);
    $tabData->{'nbBroken'}->{'area'}->{'pc'}
            = ($nbTotalArea ? 100 * $tabData->{'nbBroken'}->{'area'}->{'nb'} / $nbTotalArea : 0);
    $tabData->{'nbCheck'}->{'area'}->{'pc'}
            = ($nbTotalArea ? 100 * $tabData->{'nbCheck'}->{'area'}->{'nb'} / $nbTotalArea : 0);
    $tabData->{'nbDelivery'}->{'area'}->{'pc'}
            = ($nbTotalArea ? 100 * $tabData->{'nbDelivery'}->{'area'}->{'nb'} / $nbTotalArea : 0);

    foreach my $agencyId (@tabAgencyId)
    {
        my $nbTotalAgency = $tabData->{'nbTotal'}->{$agencyId}->{'nb'};
        $tabData->{'nbRented'}->{$agencyId}->{'pc'}
                = ($nbTotalAgency ? 100 * $tabData->{'nbRented'}->{$agencyId}->{'nb'} / $nbTotalAgency : 0);
        $tabData->{'nbAvailable'}->{$agencyId}->{'pc'}
                = ($nbTotalAgency ? 100 * $tabData->{'nbAvailable'}->{$agencyId}->{'nb'} / $nbTotalAgency : 0);
        $tabData->{'nbRecovery'}->{$agencyId}->{'pc'}
                = ($nbTotalAgency ? 100 * $tabData->{'nbRecovery'}->{$agencyId}->{'nb'} / $nbTotalAgency : 0);
        $tabData->{'nbReview'}->{$agencyId}->{'pc'}
                = ($nbTotalAgency ? 100 * $tabData->{'nbReview'}->{$agencyId}->{'nb'} / $nbTotalAgency : 0);
        $tabData->{'nbControl'}->{$agencyId}->{'pc'}
                = ($nbTotalAgency ? 100 * $tabData->{'nbControl'}->{$agencyId}->{'nb'} / $nbTotalAgency : 0);
        $tabData->{'nbBroken'}->{$agencyId}->{'pc'}
                = ($nbTotalAgency ? 100 * $tabData->{'nbBroken'}->{$agencyId}->{'nb'} / $nbTotalAgency : 0);
        $tabData->{'nbCheck'}->{$agencyId}->{'pc'}
                = ($nbTotalAgency ? 100 * $tabData->{'nbCheck'}->{$agencyId}->{'nb'} / $nbTotalAgency : 0);
        $tabData->{'nbDelivery'}->{$agencyId}->{'pc'}
                = ($nbTotalAgency ? 100 * $tabData->{'nbDelivery'}->{$agencyId}->{'nb'} / $nbTotalAgency : 0);
    }

    # Par famille tarifaire
    foreach my $tariffFamilyId (keys(%$tabTariffFamily))
    {
        $tabData->{$tariffFamilyId}->{'total'}->{'pc'} = 0;
        if ($tabData->{$tariffFamilyId}->{'total'}->{'nbTotal'})
        {
             $tabData->{$tariffFamilyId}->{'total'}->{'pc'} =
                    100 * ($tabData->{$tariffFamilyId}->{'total'}->{'nbRented'}
                        + $tabData->{$tariffFamilyId}->{'total'}->{'nbDelivery'})
                    / $tabData->{$tariffFamilyId}->{'total'}->{'nbTotal'};
         }
        $tabData->{$tariffFamilyId}->{'area'}->{'pc'} = 0;
        if ($tabData->{$tariffFamilyId}->{'area'}->{'nbTotal'})
        {
            $tabData->{$tariffFamilyId}->{'area'}->{'pc'} =
                    100 * ($tabData->{$tariffFamilyId}->{'area'}->{'nbRented'}
                        + $tabData->{$tariffFamilyId}->{'area'}->{'nbDelivery'})
                    / $tabData->{$tariffFamilyId}->{'area'}->{'nbTotal'};
        }
        foreach my $agencyId (@tabAgencyId)
        {
            $tabData->{$tariffFamilyId}->{$agencyId}->{'pc'} = 0;
            if ($tabData->{$tariffFamilyId}->{$agencyId}->{'nbTotal'})
            {
                $tabData->{$tariffFamilyId}->{$agencyId}->{'pc'} =
                        100 * ($tabData->{$tariffFamilyId}->{$agencyId}->{'nbRented'}
                            + $tabData->{$tariffFamilyId}->{$agencyId}->{'nbDelivery'})
                        / $tabData->{$tariffFamilyId}->{$agencyId}->{'nbTotal'};
            }
        }
    }
    $tabData->{'total'}->{'total'}->{'pc'} = 0;
    if ($tabData->{'total'}->{'total'}->{'nbTotal'})
    {
        $tabData->{'total'}->{'total'}->{'pc'} =
                100 * ($tabData->{'total'}->{'total'}->{'nbRented'} + $tabData->{'total'}->{'total'}->{'nbDelivery'})
                / $tabData->{'total'}->{'total'}->{'nbTotal'};
    }
    $tabData->{'total'}->{'area'}->{'pc'} = 0;
    if ($tabData->{'total'}->{'area'}->{'nbTotal'})
    {
        $tabData->{'total'}->{'area'}->{'pc'} =
                100 * ($tabData->{'total'}->{'area'}->{'nbRented'} + $tabData->{'total'}->{'area'}->{'nbDelivery'})
                / $tabData->{'total'}->{'area'}->{'nbTotal'};
    }
    foreach my $agencyId (@tabAgencyId)
    {
        $tabData->{'total'}->{$agencyId}->{'pc'} = 0;
        if ($tabData->{'total'}->{$agencyId}->{'nbTotal'})
        {
            $tabData->{'total'}->{$agencyId}->{'pc'} =
                    100 * ($tabData->{'total'}->{$agencyId}->{'nbRented'}
                        + $tabData->{'total'}->{$agencyId}->{'nbDelivery'})
                    / $tabData->{'total'}->{$agencyId}->{'nbTotal'};
        }
    }

    return $tabData;
}

# Function: updateFuel
# Met à jour l'estimation de carburant par jour pour la famille tarifaire
# 
# Parameters:
# string $countryId   - Identifiant du pays
# int    $familyId    - Identifiant de la famille tarifaire
# hash   $tabData     - tableau de carburant
#   keys = qty - quantité par jour
#          max - quantité max
#
# Returns:
sub updateFuel
{
    my ($countryId, $familyId, $tabData, $tabErrors) = @_;
    
    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Récupération des informations de la famille tarifaire
    my $tabInfos = &getInfos($countryId, $familyId);

    if (!$tabInfos)
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }
    
    # vérification de la quantité non vide
    if ($tabData->{'qty'} eq '' || $tabData->{'max'} eq '')
    {
        push(@{$tabErrors->{'fatal'}}, 0x0002);
        return 0;
    }
    
    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Mise à jour du carburant
    my $tabUpdates = {
        'FAMTARCARBUQTEJOUR'      => $tabData->{'qty'},
        'FAMTARCARBUQTEMAX'       => $tabData->{'max'}
    };
    if ($familyId)
    {
        if ($db->update('FAMILLETARIFAIRE', $tabUpdates, {'FAMTARAUTO' => $familyId}) == -1)
        {
            if (!$hasTransaction)
            {
                # Annule la transaction
                $db->rollBack();
            }
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    return 1;
}

1;
