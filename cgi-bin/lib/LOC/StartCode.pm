use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::StartCode
# Module permettant de gérer les codes de démarrage
package LOC::StartCode;


# Constants: Nombre de chiffres pour un code de démarrage
# CODE_LENGTH - Taille d'un code
use constant
{
    CODE_LENGTH => 4
};


# Constants: États
# STATE_ACTIVE   - Code de démarrage actif
# STATE_OBSOLETE - Code de démarrage obsolète
use constant
{
    STATE_ACTIVE   => 'SCO01',
    STATE_OBSOLETE => 'SCO02'
};


# Constants: États des règles
# STATE_RULE_ACTIVE   - Règle active
# STATE_RULE_OBSOLETE - Règle périmée
use constant
{
    STATE_RULE_ACTIVE   => 'SCR01',
    STATE_RULE_OBSOLETE => 'SCR02'
};


# Constants: États d'application des règles
# STATE_RULE_APPLICATION_PENDING - Règle en attente d'application
# STATE_RULE_APPLICATION_DONE    - Règle appliquée
# STATE_RULE_APPLICATION_NOTDONE - Règle non appliquée
use constant
{
    STATE_RULE_APPLICATION_PENDING => 'SCR11',
    STATE_RULE_APPLICATION_DONE    => 'SCR12',
    STATE_RULE_APPLICATION_NOTDONE => 'SCR13'
};


# Constants: États de désapplication des règles
# STATE_RULE_UNAPPLICATION_PENDING - Règle en attente de désapplication
# STATE_RULE_UNAPPLICATION_DONE    - Règle désappliquée
# STATE_RULE_UNAPPLICATION_NOTDONE - Règle non désappliquée
use constant
{
    STATE_RULE_UNAPPLICATION_PENDING => 'SCR21',
    STATE_RULE_UNAPPLICATION_DONE    => 'SCR22',
    STATE_RULE_UNAPPLICATION_NOTDONE => 'SCR23'
};


# Constants: Types d'action pour la file d'attente des règles des codes de démarrage envoyées à Traqueur
# RULEQUEUE_ACTION_ADD    - Ajout
# RULEQUEUE_ACTION_DELETE - Suppression
use constant
{
    RULEQUEUE_ACTION_ADD    => 'ADD',
    RULEQUEUE_ACTION_DELETE => 'DELETE'
};


# Constants: États des lignes de la file d'attente des règles des codes de démarrage envoyées à Traqueur
# STATE_RULEQUEUE_PENDING  - En attente
# STATE_RULEQUEUE_DONE     - Traitée
# STATE_RULEQUEUE_OBSOLETE - Obsolète
use constant
{
    STATE_RULEQUEUE_PENDING  => 'QSC01',
    STATE_RULEQUEUE_DONE     => 'QSC02',
    STATE_RULEQUEUE_OBSOLETE => 'QSC03'
};


# Constants: Récupération d'informations sur les codes de démarrage
# GETINFOS_ALL         - Récupération de l'ensemble des informations
# GETINFOS_BASE        - Récupération des informations de base
# GETINFOS_RULESLIST   - Récupération des règles
# GETINFOS_MACHINESIDS - Récupération des identifiants des machines associées
use constant
{
    GETINFOS_ALL         => 0xFFFFFFFF,
    GETINFOS_BASE        => 0x00000000,
    GETINFOS_RULESLIST   => 0x00000001,
    GETINFOS_MACHINESIDS => 0x00000002
};


use strict;

use Tie::IxHash;


# Modules internes
use LOC::Machine;
use LOC::Util;
use LOC::EndUpdates;
use LOC::Date;
use LOC::Session;
use LOC::Db;


my $fatalErrMsg = 'Erreur fatale: module LOC::StartCode, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";

# Liste des erreurs:
#
# 0x0001 => Erreur fatale




# Function: getRights
# Récupérer les droits pour un utilisateur donné
#
# Returns:
# hashref - Tableau des droits
sub getRights
{
    my $tabUserInfos = &LOC::Session::getUserInfos();

    my $isUserAdmin      = $tabUserInfos->{'isAdmin'};
    my $isUserTelematics = &LOC::Util::in_array(LOC::User::GROUP_TELEMATICS, $tabUserInfos->{'tabGroups'});

    my $tabRights = {};

    # Initialisation des droits
    &LOC::Util::setTreeValues($tabRights, {
        'actions' => ['renew']
    }, 0);

    # Télématique
    if ($isUserTelematics || $isUserAdmin)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'actions' => ['renew']
        }, 1);
    }

    return $tabRights;
}


# Function: getInfos
# Retourne les infos d'un code de démarrage
#
# Contenu du hashage :
# - id                 => identifiant du code de démarrage
# - type.id            => identifiant du type de code de démarrage
# - type.name          => nom du type de code de démarrage
# - type.code          => code du type de code de démarrage
# - type.scope         => portée du type de code de démarrage (statique (static) ou contrat (contract))
# - type.externId      => identifiant externe (chez Traqueur) du type de code
# - type.order         => ordre d'affichage du type de code
# - code               => code de démarrage
# - creationDate       => date de création
# - modificationDate   => date de dernière modification
# - state.id           => identifiant de l'état
# -- Un sous tableau "rulesList" pour les règles (si GETINFOS_RULESLIST) :
# - id                 => identifiant de la règle
# - externId           => identifiant externe (chez Traqueur) de la règle
# - machine.id         => identifiant de la machine associée à la règle
# - machine.parkNumber => numéro de parc de la machine associée à la règle
# - beginDate          => date de début de validité de la règle
# - endDate            => date de fin de validité de la règle
# - creationDate       => date de création de la règle
# - state.id           => identifiant de l'état de la règle
# - application.id     => identifiant de l'état d'application de la règle
# - unapplication.id   => identifiant de l'état de déapplication de la règle
#
#
# Parameters:
# string  $countryId  - Pays
# string  $id         - Identifiant du code de démarrage
# int     $flags      - Flags (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Verrou sur la ligne du code
    if (exists $tab->{$id} && (!$tabOptions->{'lockDbRow'} || &_lockDbRow($db, $id)))
    {
        return (wantarray ? %{$tab->{$id}} : $tab->{$id});
    }
    return undef;
}


# Function: getList
# Retourne la liste des codes de démarrage
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres (facultatif)
# int     $flags      - Flags pour le choix des colonnes (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Liste des codes de démarrage
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (ref($tabFilters) ne 'HASH')
    {
        $tabFilters = {};
    }
    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }

    # Si la télématique n'est pas activée alors on envoie un tableau vide
    if (!&isTelematActived($countryId))
    {
        return (wantarray ? () : ($format == LOC::Util::GETLIST_IDS ? [] : {}));
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my @tabWheres = ();

    my $query;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    sco_id AS `id`,
    sco_sct_id AS `type.id`,
    sct_name AS `type.name`,
    sct_code AS `type.code`,
    sct_scope AS `type.scope`,
    sct_extid AS `type.externId`,
    sct_order AS `type.order`,
    sco_code AS `code`,
    sco_is_codereserved AS `isCodeReserved`,
    sco_date_activation AS `activationDate`,
    sco_date_creation AS `creationDate`,
    sco_date_modification AS `modificationDate`,
    sco_sta_id AS `state.id`';
        if ($flags & GETINFOS_MACHINESIDS)
        {
            $query .= ',
    (SELECT GROUP_CONCAT(DISTINCT scr_mac_id ORDER BY scr_mac_id SEPARATOR ";") FROM f_startcoderule WHERE scr_sco_id = sco_id) AS `machinesIds`';
        }
        $query .= '
FROM f_startcode
INNER JOIN p_startcodetype
ON sco_sct_id = sct_id';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(sco_id)
FROM f_startcode
INNER JOIN p_startcodetype
ON sco_sct_id = sct_id';
    }
    else
    {
        $query = '
SELECT DISTINCT
    sco_id,
    sco_code
FROM f_startcode
INNER JOIN p_startcodetype
ON sco_sct_id = sct_id';
    }


    # Filtres
    # - Id
    if (defined $tabFilters->{'id'})
    {
        push(@tabWheres, 'sco_id IN (' . &LOC::Util::join(', ', $tabFilters->{'id'}) . ')');
    }
    if (defined $tabFilters->{'!id'})
    {
        push(@tabWheres, 'sco_id NOT IN (' . &LOC::Util::join(', ', $tabFilters->{'!id'}) . ')');
    }
    # - Id du type de code
    if (defined $tabFilters->{'typeId'})
    {
        push(@tabWheres, 'sco_sct_id IN (' . &LOC::Util::join(', ', $tabFilters->{'typeId'}) . ')');
    }
    # - Code du type de code
    if (defined $tabFilters->{'typeCode'})
    {
        push(@tabWheres, 'sct_code IN (' . $db->quote($tabFilters->{'typeCode'}) . ')');
    }
    # - Portée du code (static/contract/etc)
    if (defined $tabFilters->{'typeScope'})
    {
        push(@tabWheres, 'sct_scope IN (' . $db->quote($tabFilters->{'typeScope'}) . ')');
    }
    # - A-t-il une date d'activation ?
    if (defined $tabFilters->{'hasBeenActivated'})
    {
        push(@tabWheres, 'sco_date_activation IS ' . ($tabFilters->{'hasBeenActivated'} ? 'NOT ' : '')  . 'NULL');
    }
    # - État
    if (defined $tabFilters->{'stateId'})
    {
        push(@tabWheres, 'sco_sta_id IN (' . &LOC::Util::join(', ', $db->quote($tabFilters->{'stateId'})) . ')');
    }

    # - Filtres sur les règles associées au code
    if (defined $tabFilters->{'rule'})
    {
        # Ajout des filtres commun entre getList() et _getRulesList()
        my @tabRuleWheres = &_getRulesWheresFromFilters($db, $tabFilters->{'rule'});

        if (@tabRuleWheres > 0)
        {
            push(@tabWheres, '(SELECT COUNT(*) FROM f_startcoderule WHERE scr_sco_id = sco_id AND ' . join(' AND ', @tabRuleWheres) . ') > 0');
        }
    }

    if (@tabWheres > 0)
    {
        $query .= '
WHERE ' . join("\nAND ", @tabWheres);
    }

    # Tris
    if (defined $tabOptions->{'orderBy'})
    {
        my $tabOrderFields = {
            'activationDate' => 'sco_date_activation'
        };
        foreach my $orderBy (@{$tabOptions->{'orderBy'}})
        {
            my ($field, $dir) = split(/:/, $orderBy);
            $query .= '
ORDER BY ' . $tabOrderFields->{$field} . ' ' . (defined $dir && uc($dir) eq 'DESC' ? 'DESC' : 'ASC');
        }
    }
    else
    {
        $query .= '
ORDER BY sco_date_creation ASC, sco_id ASC';
    }

    if (defined $tabOptions->{'limit'})
    {
        $query .= '
LIMIT ' . $tabOptions->{'limit'};
    }

    $query .= ';';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    # Formatage des champs
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        # Options pour la récupération des règles
        my $tabRulesFilters;
        if ($flags & GETINFOS_RULESLIST)
        {
            $tabRulesFilters = (defined $tabOptions->{'getRulesListFilters'} ?
                                    $tabOptions->{'getRulesListFilters'} :
                                    (defined $tabFilters->{'rule'} ? $tabFilters->{'rule'} : {}));
        }

        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}               *= 1;
            $tabRow->{'type.id'}          *= 1;
            $tabRow->{'type.order'}       *= 1;
            $tabRow->{'isCodeReserved'}   *= 1;
            $tabRow->{'hasBeenActivated'}  = ($tabRow->{'activationDate'} ne '' ? 1 : 0);

            # Liste des règles
            if ($flags & GETINFOS_RULESLIST)
            {
                $tabRow->{'rulesList'} = &_getRulesList($db, $countryId, LOC::Util::GETLIST_ASSOC, $tabRow->{'id'}, $tabRulesFilters);
            }

            # Liste des identifiants de machines associées
            if ($flags & GETINFOS_MACHINESIDS)
            {
                my @tabMachinesIds = map { $_ * 1 } split(';', $tabRow->{'machinesIds'});
                $tabRow->{'machinesIds'} = \@tabMachinesIds;
            }
        }
    }

    return (wantarray ? %$tab : $tab);
}


# Function: getTypeInfos
# Retourne les infos d'un type de code de démarrage
#
# Contenu du hashage :
# - id                => identifiant du type de code de démarrage
# - name              => nom du type de code de démarrage
# - code              => code du type de code de démarrage
# - scope             => portée du type de code de démarrage (statique (static) ou contrat (contract))
# - externId          => identifiant externe (chez Traqueur) du type de code
# - easyRestartPeriod => période de redémarrage rapide
# - order             => ordre d'affichage du type de code
#
#
# Parameters:
# string  $countryId  - Pays
# string  $idOrCode   - Identifiant ou code du code de démarrage
# int     $flags      - Flags (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getTypeInfos
{
    my ($countryId, $idOrCode, $flags, $tabOptions) = @_;

    my $tab;
    if (&LOC::Util::isNumeric($idOrCode))
    {
        $tab = &getTypesList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $idOrCode}, $flags, $tabOptions);
    }
    else
    {
        $tab = &getTypesList($countryId, LOC::Util::GETLIST_ASSOC, {'code' => $idOrCode}, $flags, $tabOptions);
    }

    my ($id, $tabInfos) = each(%$tab);

    return ($tabInfos ? (wantarray ? %$tabInfos : $tabInfos) : undef);
}


# Function: getTypesList
# Retourne la liste des types de codes de démarrage
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres (facultatif)
# int     $flags      - Flags pour le choix des colonnes (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Liste des codes de démarrage
sub getTypesList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (ref($tabFilters) ne 'HASH')
    {
        $tabFilters = {};
    }
    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }

    # Si la télématique n'est pas activée alors on envoie un tableau vide
    if (!&isTelematActived($countryId))
    {
        return (wantarray ? () : ($format == LOC::Util::GETLIST_IDS ? [] : {}));
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my @tabWheres = ();

    my $query;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    sct_id AS `id`,
    sct_name AS `name`,
    sct_code AS `code`,
    sct_scope AS `scope`,
    sct_extid AS `externId`,
    sct_easyrestartperiod AS `easyRestartPeriod`,
    sct_order AS `order`
FROM p_startcodetype';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(sct_id)
FROM p_startcodetype';
    }
    else
    {
        $query = '
SELECT DISTINCT
    sct_id,
    sct_name
FROM p_startcodetype';
    }


    # Filtres
    # - Id
    if (defined $tabFilters->{'id'})
    {
        push(@tabWheres, 'sct_id IN (' . &LOC::Util::join(', ', $tabFilters->{'id'}) . ')');
    }
    # - Code
    if (defined $tabFilters->{'code'})
    {
        push(@tabWheres, 'sct_code IN (' . $db->quote($tabFilters->{'code'}) . ')');
    }
    # - Portée (static/contract/etc)
    if (defined $tabFilters->{'scope'})
    {
        push(@tabWheres, 'sct_scope IN (' . $db->quote($tabFilters->{'scope'}) . ')');
    }

    if (@tabWheres > 0)
    {
        $query .= '
WHERE ' . join("\nAND ", @tabWheres);
    }

    # Tris
    $query .= '
ORDER BY sct_order, sct_id;';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    # Formatage des champs
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}    *= 1;
            $tabRow->{'order'} *= 1;
        }
    }

    return (wantarray ? %$tab : $tab);
}


# Function: delete
# Supprime un code de démarrage
#
# Parameters:
# string  $countryId   - Identifiant du pays
# int     $startCodeId - Identifiant du code
# int     $userId      - Identifiant de l'utilisateur responsable
# hashref $tabOptions  - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors   - Tableau d'erreurs
# hashref $tabEndUpds  - Mises à jour de fin
#
# Returns:
# int - Identifiant du code de démarrage créé
sub delete
{
    my ($countryId, $startCodeId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Si la télématique n'est pas activée alors on ne fait rien
    if (!&isTelematActived($countryId))
    {
        return 1;
    }

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'delete', $_[0], __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, $_[1]);
        return 0;
    };

    # Récupération des informations du code
    my $tabInfos = &getInfos($countryId, $startCodeId, undef, {'writeDbLock' => 1});
    if (!$tabInfos || $tabInfos->{'state.id'} ne STATE_ACTIVE)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    # Désactivation de toutes les règles actives de ce code
    my $tabRulesIds = &_getRulesList($db, $countryId, LOC::Util::GETLIST_IDS, $startCodeId, {
        'stateId' => STATE_RULE_ACTIVE
    });
    foreach my $ruleId (@$tabRulesIds)
    {
        if (!&_deleteRule($db, $countryId, $ruleId, $userId, $tabEndUpds))
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, 0x0001);
        }
    }

    # Mise à jour de l'état du code de démarrage
    my $tabUpdates = {
        'sco_sta_id'          => STATE_OBSOLETE,
        'sco_is_codereserved' => 0
    };
    if ($db->update('f_startcode', $tabUpdates,
                    {'sco_id' => $startCodeId * 1}, $schemaName) == -1)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, 0x0001);
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, 0x0001);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: insert
# Insère un nouveau code de démarrage
#
# Parameters:
# string       $countryId      - Identifiant du pays
# int          $typeIdOrCode   - Identifiant ou code du type de code
# arrayref|int $tabMachinesIds - Identifiants des machines affectés au code
# string       $beginDate      - Date de début de validité du code (ou undef)
# string       $endDate        - Date de début de validité du code (ou undef)
# int          $userId         - Identifiant de l'utilisateur responsable
# hashref      $tabOptions     - Tableau des options supplémentaires (facultatif)
# hashref      $tabErrors      - Tableau d'erreurs
# hashref      $tabEndUpds     - Mises à jour de fin
#
# Returns:
# int - Identifiant du code de démarrage créé
sub insert
{
    my ($countryId, $typeIdOrCode, $tabMachinesIds, $beginDate, $endDate, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Si la télématique n'est pas activée alors on ne fait rien
    if (!&isTelematActived($countryId))
    {
        return 1;
    }

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Au moins une machine doit être renseignée
    if (ref($tabMachinesIds) eq '')
    {
        $tabMachinesIds = [$tabMachinesIds];
    }
    elsif (ref($tabMachinesIds) ne 'ARRAY')
    {
        $tabMachinesIds = [];
    }
    else
    {
        $tabMachinesIds = &LOC::Util::array_unique($tabMachinesIds);
    }

    # Au moins une machine doit être renseignée
    if (@$tabMachinesIds == 0)
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'insert', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Vérification des dates
    $beginDate = &LOC::Util::trim($beginDate);
    $endDate   = &LOC::Util::trim($endDate);
    if (($beginDate ne '' && !&LOC::Date::checkMySQLDate($beginDate)) ||
        ($endDate   ne '' && !&LOC::Date::checkMySQLDate($endDate)) ||
        ($beginDate ne '' && $endDate ne '' && $beginDate gt $endDate))
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'insert', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'insert', $_[0], __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, $_[1]);
        return 0;
    };

    # Récupération des informations sur le type de code
    my $tabTypeInfos = &getTypeInfos($countryId, $typeIdOrCode);
    if (!$tabTypeInfos)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    # Liste des codes non disponibles pour la génération d'un nouveau code
    my $tabUnavailableCodes = &getUnavailableCodes($countryId, $tabMachinesIds);
    if ($tabOptions->{'excludedCodes'})
    {
        push(@$tabUnavailableCodes, @{$tabOptions->{'excludedCodes'}});
    }

    # Génération d'un code
    my $code = &_generateCode($countryId, $tabUnavailableCodes);
    if (!$code)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    # Insertion du code de démarrage en base de données
    my $tabUpdates = {
        'sco_sct_id'            => $tabTypeInfos->{'id'},
        'sco_code'              => $code,
        'sco_is_codereserved'   => 1,
        'sco_date_activation*'  => 'NOW()',
        'sco_date_creation*'    => 'NOW()',
        'sco_sta_id'            => STATE_ACTIVE
    };
    my $startCodeId = $db->insert('f_startcode', $tabUpdates, $schemaName);
    if ($startCodeId == -1)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, 0x0001);
    }
    $startCodeId *= 1;


    # Création de la première règle associée au code de démarrage
    # pour toutes les machines impactées
    foreach my $machineId (@$tabMachinesIds)
    {
        # Ajout de la règle pour la machine du contrat
        if (!&_addRule($db, $countryId,
                       $startCodeId,
                       $machineId,
                       $beginDate, $endDate,
                       $userId, $tabEndUpds))
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, 0x0001);
        };
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, 0x0001);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return $startCodeId;
}


# Function: update
# Met à jour un code de démarrage
#
# Parameters:
# string       $countryId      - Identifiant du pays
# int          $startCodeId    - Identifiant du code
# arrayref|int $tabMachinesIds - Identifiants des machines affectés au code
# string       $beginDate      - Date de début de validité du code (ou undef)
# string       $endDate        - Date de début de validité du code (ou undef)
# int          $userId         - Identifiant de l'utilisateur responsable
# hashref      $tabOptions     - Tableau des options supplémentaires (facultatif)
# hashref      $tabErrors      - Tableau d'erreurs
# hashref      $tabEndUpds     - Mises à jour de fin
#
# Returns:
# int - 1 si c'est un succès, 0 sinon
sub update
{
    my ($countryId, $startCodeId, $tabMachinesIds, $beginDate, $endDate, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Si la télématique n'est pas activée alors on ne fait rien
    if (!&isTelematActived($countryId))
    {
        return 1;
    }

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Au moins une machine doit être renseignée
    if (ref($tabMachinesIds) eq '')
    {
        $tabMachinesIds = [$tabMachinesIds];
    }
    elsif (ref($tabMachinesIds) ne 'ARRAY')
    {
        $tabMachinesIds = [];
    }
    else
    {
        $tabMachinesIds = &LOC::Util::array_unique($tabMachinesIds);
    }

    # Au moins une machine doit être renseignée
    if (@$tabMachinesIds == 0)
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'update', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Vérification des dates
    $beginDate = &LOC::Util::trim($beginDate);
    $endDate   = &LOC::Util::trim($endDate);
    if (($beginDate ne '' && !&LOC::Date::checkMySQLDate($beginDate)) ||
        ($endDate   ne '' && !&LOC::Date::checkMySQLDate($endDate)) ||
        ($beginDate ne '' && $endDate ne '' && $beginDate gt $endDate))
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'update', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'update', $_[0], __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, $_[1]);
        return 0;
    };

    my $isAddMachine = defined $tabOptions->{'isAddedMachines'} && $tabOptions->{'isAddedMachines'};

    # Récupération de l'état du code
    my $tabInfos = &getInfos($countryId, $startCodeId, GETINFOS_MACHINESIDS, {'writeDbLock' => 1});
    if (!$tabInfos || $tabInfos->{'state.id'} ne STATE_ACTIVE)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    # Suppression des règles actives sur ces machines
    my $tabFilters = {
        'stateId'   => STATE_RULE_ACTIVE
    };
    if ($isAddMachine)
    {
        $tabFilters->{'machineId'} = $tabMachinesIds;
    }
    my $tabRulesIds = &_getRulesList($db, $countryId, LOC::Util::GETLIST_IDS,
                                     $startCodeId, $tabFilters);
    foreach my $ruleId (@$tabRulesIds)
    {
        if (!&_deleteRule($db, $countryId, $ruleId, $userId, $tabEndUpds))
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, 0x0001);
        }
    }

    # Calcul des machines ajoutées
    my @tabAddedMachines = &LOC::Util::array_minus($tabMachinesIds, $tabInfos->{'machinesIds'});

    # Vérification de la disponibilité du code si il n'est plus réservé ou
    # si l'ensemble des machines affectés a changé
    if (!$tabInfos->{'isCodeReserved'} || @tabAddedMachines > 0)
    {
        # Vérification si le code est encore dispo
        my $tabUnavailableCodes = &getUnavailableCodes($countryId, $tabMachinesIds, $startCodeId);
        if ($tabOptions->{'excludedCodes'})
        {
            push(@$tabUnavailableCodes, @{$tabOptions->{'excludedCodes'}});
        }

        my $codeToInsert = '';
        if (&LOC::Util::in_array($tabInfos->{'code'}, $tabUnavailableCodes))
        {
            # Le code n'est plus disponible

            # Dans le cas d'un ajout de machines au code on génère une erreur
            if ($isAddMachine)
            {
                # Erreur fatale !
                return &$rollBackAndError(__LINE__, 0x0001);
            }

            # On genère un nouveau code
            $codeToInsert = &_generateCode($countryId, $tabUnavailableCodes);
            if (!$codeToInsert)
            {
                # Erreur fatale !
                return &$rollBackAndError(__LINE__, 0x0001);
            }
        }
        elsif (!$isAddMachine && @tabAddedMachines > 0)
        {
            # Si l'ensemble des machines affectés a changé, on créé un nouveau code avec le même code à 4 chiffres
            $codeToInsert = $tabInfos->{'code'};
        }

        if ($codeToInsert ne '')
        {
            # Suppression du code
            my $tabUpdates = {
                'sco_is_codereserved' => 0,
                'sco_sta_id'          => STATE_OBSOLETE
            };
            if ($db->update('f_startcode', $tabUpdates,
                            {'sco_id' => $startCodeId * 1}, $schemaName) == -1)
            {
                # Erreur fatale !
                return &$rollBackAndError(__LINE__, 0x0001);
            }

            # Génération d'un nouveau code du même type
            # Insertion du code de démarrage en base de données
            my $tabUpdates = {
                'sco_sct_id'           => $tabInfos->{'type.id'},
                'sco_code'             => $codeToInsert,
                'sco_is_codereserved'  => 1,
                'sco_date_activation*' => 'NOW()',
                'sco_date_creation*'   => 'NOW()',
                'sco_sta_id'           => STATE_ACTIVE
            };
            $startCodeId = $db->insert('f_startcode', $tabUpdates, $schemaName);
            if ($startCodeId == -1)
            {
                # Erreur fatale !
                return &$rollBackAndError(__LINE__, 0x0001);
            }
            $startCodeId *= 1;
        }
        else
        {
            # Réservation du code
            my $tabUpdates = {
                'sco_is_codereserved' => 1
            };
            if (!$tabInfos->{'hasBeenActivated'})
            {
                $tabUpdates->{'sco_date_activation*'} = 'NOW()';
            }
            if ($db->update('f_startcode', $tabUpdates,
                            {'sco_id' => $startCodeId}, $schemaName) == -1)
            {
                # Erreur fatale !
                return &$rollBackAndError(__LINE__, 0x0001);
            }
        }
    }
    elsif (!$tabInfos->{'hasBeenActivated'})
    {
        # Réservation du code
        my $tabUpdates = {
            'sco_date_activation*' => 'NOW()'
        };
        if ($db->update('f_startcode', $tabUpdates,
                        {'sco_id' => $startCodeId}, $schemaName) == -1)
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, 0x0001);
        }
    }

    # Création des nouvelles règles pour chacune des machines
    foreach my $machineId (@$tabMachinesIds)
    {
        if (!&_addRule($db, $countryId,
                       $startCodeId,
                       $machineId,
                       $beginDate, $endDate,
                       $userId, $tabEndUpds))
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, 0x0001);
        };
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, 0x0001);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return $startCodeId;
}


# Function: freeCode
# Libère un code de démarrage
#
# Parameters:
# string  $countryId   - Identifiant du pays
# int     $startCodeId - Identifiant du code
# int     $userId      - Identifiant de l'utilisateur responsable
# hashref $tabOptions  - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors   - Tableau d'erreurs
# hashref $tabEndUpds  - Mises à jour de fin
#
# Returns:
# int - 1 si c'est un succès, 0 sinon
sub freeCode
{
    my ($countryId, $startCodeId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Si la télématique n'est pas activée alors on ne fait rien
    if (!&isTelematActived($countryId))
    {
        return 1;
    }

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'freeCode', $_[0], __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, $_[1]);
        return 0;
    };

    # Libération du code
    my $tabUpdates = {
        'sco_is_codereserved' => 0
    };
    if ($db->update('f_startcode', $tabUpdates,
                    {'sco_id' => $startCodeId}, $schemaName) == -1)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, 0x0001);
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, 0x0001);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: clear
# Supprime toutes les règles actives d'un code et libère le code de démarrage
#
# Parameters:
# string  $countryId   - Identifiant du pays
# int     $startCodeId - Identifiant du code
# int     $userId      - Identifiant de l'utilisateur responsable
# hashref $tabOptions  - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors   - Tableau d'erreurs
# hashref $tabEndUpds  - Mises à jour de fin
#
# Returns:
# int - 1 si c'est un succès, 0 sinon
sub clear
{
    my ($countryId, $startCodeId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Si la télématique n'est pas activée alors on ne fait rien
    if (!&isTelematActived($countryId))
    {
        return 1;
    }

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'clear', $_[0], __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, $_[1]);
        return 0;
    };

    # Récupération des informations du code
    my $tabInfos = &getInfos($countryId, $startCodeId, undef, {'writeDbLock' => 1});
    if (!$tabInfos)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    # Désactivation de toutes les règles actives de ce code
    my $tabRulesIds = &_getRulesList($db, $countryId, LOC::Util::GETLIST_IDS, $startCodeId, {
        'stateId' => STATE_RULE_ACTIVE
    });
    foreach my $ruleId (@$tabRulesIds)
    {
        if (!&_deleteRule($db, $countryId, $ruleId, $userId, $tabEndUpds))
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, 0x0001);
        }
    }

    # Libération du code
    my $tabUpdates = {
        'sco_is_codereserved' => 0,
        'sco_date_activation' => undef
    };
    if ($db->update('f_startcode', $tabUpdates,
                    {'sco_id' => $startCodeId}, $schemaName) == -1)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, 0x0001);
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, 0x0001);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: clean
# Supprime les codes de démarrage sur les machines qui ne sont pas équipées télématique
#
# Parameters:
# string  $countryId  - Pays
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabOptions - Tableau des options supplémentaires (facultatif). Peut contenir machine.id.
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# int - 1 si c'est un succès, 0 sinon
sub clean
{
    my ($countryId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Si la télématique n'est pas activée alors on ne fait rien
    if (!&isTelematActived($countryId))
    {
        return 1;
    }

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'clean', $_[0], __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, $_[1]);
        return 0;
    };

    # Récupération de la liste des machines
    my $tabMachinesFilters = {
        'isTelematic' => 0,
        'countryId'   => $countryId,
        'startCode'   => {
            'stateId' => STATE_ACTIVE,
            'rule'    => {
                'stateId' => STATE_RULE_ACTIVE
            }
        }
    };
    if (defined $tabOptions->{'machine.id'})
    {
        $tabMachinesFilters->{'id'} = $tabOptions->{'machine.id'};
    }
    my $tabMachinesIds = &LOC::Machine::getList($countryId, LOC::Util::GETLIST_IDS, $tabMachinesFilters);
    if (@$tabMachinesIds > 0)
    {
        foreach my $machineId (@$tabMachinesIds)
        {
            # Récupération des codes présents sur la machine
            my $tabMachineStartCodeIds = &getList(
                $countryId,
                LOC::Util::GETLIST_IDS,
                {
                    'stateId' => STATE_ACTIVE,
                    'rule' => {
                        'machineId' => $machineId,
                        'stateId'   => STATE_RULE_ACTIVE
                    }
                },
                GETINFOS_BASE
            );

            foreach my $startCodeId (@$tabMachineStartCodeIds)
            {
                my $tabRules = &_getRulesList($db, $countryId, LOC::Util::GETLIST_ASSOC, $startCodeId, {
                    'stateId' => STATE_RULE_ACTIVE
                });
                my $hasActiveRule = 0;
                while (my ($ruleId, $tabRule) = each(%$tabRules))
                {
                    if ($tabRule->{'machine.id'} == $machineId)
                    {
                        # Mise à jour de l'état des règles
                        my $tabUpdates = {
                            'scr_date_deletion*' => 'IFNULL(scr_date_deletion, NOW())',
                            'scr_sta_id'         => STATE_RULE_OBSOLETE
                        };
                        if ($db->update('f_startcoderule', $tabUpdates,
                                        {'scr_id' => $ruleId}, $schemaName) == -1)
                        {
                            # Erreur fatale !
                            return &$rollBackAndError(__LINE__, 0x0001);
                        }
                    }
                    else
                    {
                        $hasActiveRule = 1;
                    }
                }

                # Mise à jour de l'état du code de démarrage si il n'y a plus de règle active
                if (!$hasActiveRule)
                {
                    if ($db->update('f_startcode',
                                    {'sco_is_codereserved' => 0,
                                     'sco_sta_id'          => STATE_OBSOLETE},
                                    {'sco_id' => $startCodeId}, $schemaName) == -1)
                    {
                        # Erreur fatale !
                        return &$rollBackAndError(__LINE__, 0x0001);
                    }
                }
            }


            # Recherche de toutes les règles en cours d'application ou de déapplication concernant cette machine
            # pour les mettre en Non fait et supprimer la ligne dans la file d'attente si elle est active
            my $query = '
SELECT
    scr_id AS `id`,
    scr_sta_id_application AS `application.id`,
    scr_sta_id_unapplication AS `unapplication.id`
FROM f_startcoderule
WHERE scr_mac_id = ' . $machineId . '
AND (scr_sta_id_application   = ' . $db->quote(STATE_RULE_APPLICATION_PENDING) . ' OR
     scr_sta_id_unapplication = ' . $db->quote(STATE_RULE_UNAPPLICATION_PENDING) . ')
FOR UPDATE;';
            my $stmt = $db->query($query);
            while (my $tabRuleInfos = $stmt->fetch(LOC::Db::FETCH_ASSOC))
            {
                my $ruleId = $tabRuleInfos->{'id'} * 1;

                # Mise à jour de l'état des règles
                my $tabUpdates = {
                    'scr_date_deletion*' => 'NOW()'
                };
                if ($tabRuleInfos->{'application.id'} eq STATE_RULE_APPLICATION_PENDING)
                {
                    $tabUpdates->{'scr_sta_id_application'} = STATE_RULE_APPLICATION_NOTDONE;
                }
                if ($tabRuleInfos->{'unapplication.id'} eq STATE_RULE_UNAPPLICATION_PENDING)
                {
                    $tabUpdates->{'scr_sta_id_unapplication'} = STATE_RULE_UNAPPLICATION_NOTDONE;
                }

                if ($db->update('f_startcoderule', $tabUpdates,
                                {'scr_id' => $ruleId}, $schemaName) == -1)
                {
                    # Erreur fatale !
                    return &$rollBackAndError(__LINE__, 0x0001);
                }

                # Mise à jour de l'état des demandes dans la file d'attente
                if ($db->update('q_startcoderule', {
                        'qsc_sta_id' => STATE_RULEQUEUE_OBSOLETE
                    }, {
                        'qsc_scr_id' => $ruleId,
                        'qsc_sta_id' => STATE_RULEQUEUE_PENDING
                    }) == -1)
                {
                    # Erreur fatale !
                    return &$rollBackAndError(__LINE__, 0x0001);
                }
            }
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, 0x0001);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: queueExpiredRules
# Envoie, dans la file d'attente, de la suppression des règles échues sur les boîtiers
#
# Parameters:
# string  $countryId  - Pays
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# int - 1 si c'est un succès, 0 sinon
sub queueExpiredRules
{
    my ($countryId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Si la télématique n'est pas activée alors on ne fait rien
    if (!&isTelematActived($countryId))
    {
        return 1;
    }

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'queueExpiredRules', $_[0], __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, $_[1]);
        return 0;
    };

    my $query = '
SELECT
    scr_id AS `id`,
    scr_extruleid AS `externRuleId`
FROM f_startcoderule
WHERE scr_sta_id = ' . $db->quote(STATE_RULE_ACTIVE) . '
AND scr_sta_id_application <> ' . $db->quote(STATE_RULE_APPLICATION_NOTDONE) . '
AND scr_sta_id_unapplication IS NULL
AND scr_date_end IS NOT NULL
AND scr_date_end < NOW()
FOR UPDATE;';

    my $stmt = $db->query($query);
    while (my $tabRow = $stmt->fetch(LOC::Db::FETCH_ASSOC))
    {
        my $ruleId = $tabRow->{'id'};
        my $externRuleId = ($tabRow->{'externRuleId'} ? $tabRow->{'externRuleId'} * 1 : undef);

        # Mise à jour de la règle
        my $tabUpdates = {
            'scr_date_deletion*' => 'NOW()'
        };

        if (!$externRuleId)
        {
            # État de application de la règle
            $tabUpdates->{'scr_sta_id_application'} = STATE_RULE_APPLICATION_NOTDONE;

            # Dans le cas où la demande d'ajout n'a pas été envoyée à Traqueur
            # on lui met l'état "obsolète" et on ne créé pas la nouvelle ligne
            if (!&_expireQueueAddAction($db, $countryId, $ruleId, $userId, $tabEndUpds))
            {
                # Erreur fatale !
                return &$rollBackAndError(__LINE__, 0x0001);
            }
        }
        else
        {
            # État de désapplication de la règle
            $tabUpdates->{'scr_sta_id_unapplication'} = STATE_RULE_UNAPPLICATION_PENDING;

            # Insertion de la demande de suppression de la règle dans la file d'attente
            if (!&_addQueueDeleteAction($db, $countryId, $ruleId, $externRuleId, $userId, $tabEndUpds))
            {
                # Erreur fatale !
                return &$rollBackAndError(__LINE__, 0x0001);
            }
        }

        if ($db->update('f_startcoderule', $tabUpdates, {'scr_id' => $ruleId * 1}, $schemaName) == -1)
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, 0x0001);
        }

    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, 0x0001);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: getUnavailableCodes
# Récupère la liste des codes non disponibles pour une liste de machines
#
# Parameters:
# LOC::Db::Adapter $db                  - Connexion à la base de données
# string           $countryId           - Pays
# arrayref|undef   $tabMachinesIds      - Identifiants des machines
# int|undef        $excludedStartCodeId - Identifiant du code exclu (facultatif)
#
# Returns:
# string
sub getUnavailableCodes
{
    my ($countryId, $tabMachinesIds, $excludedStartCodeId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Récupération de la liste des codes réservés
    my @tabReservedCodes = split(/,/, &LOC::Characteristic::getCountryValueByCode('DIGIRESERV', $countryId));

    # Récupération de la liste des codes déjà utilisés
    my $query = '
SELECT
    DISTINCT sco_code
FROM f_startcode
INNER JOIN f_startcoderule
ON scr_sco_id = sco_id
WHERE (
          sco_is_codereserved = 1 OR
          (
              scr_sta_id_unapplication IS NULL AND
              (
                  scr_sta_id_application = ' . $db->quote(STATE_RULE_APPLICATION_PENDING) . ' OR
                  scr_sta_id_application = ' . $db->quote(STATE_RULE_APPLICATION_DONE) . ' AND
                  scr_sta_id = ' . $db->quote(STATE_RULE_ACTIVE) . '
              ) OR
              scr_sta_id_unapplication = ' . $db->quote(STATE_RULE_UNAPPLICATION_PENDING) . '
          )
      )';
    if (defined $tabMachinesIds)
    {
        $query .= '
AND scr_mac_id IN (' . $db->quote($tabMachinesIds) . ')';
    }
    if (defined $excludedStartCodeId)
    {
        $query .= '
AND sco_id != ' . $excludedStartCodeId;
    }
    $query .= ';';

    my $tabUsedCodes = $db->fetchCol($query);

    push(@$tabUsedCodes, @tabReservedCodes);

    return $tabUsedCodes;
}


# Function: setReservedCodesEndDate
# Mise à jour de la date de fin de validité (à J+N) des codes réservés sur la machine choisie
#
# Parameters:
# string $countryId - Pays
# int    $machineId - Identifiant de la machine
#
# Returns:
# int - 1 si c'est un succès, 0 sinon
sub setReservedCodesEndDate
{
    my ($countryId, $machineId) = @_;

    # Si la télématique n'est pas activée alors on ne fait rien
    if (!&isTelematActived($countryId))
    {
        return 1;
    }

    # Récupération des informations de la machine
    my $tabMachineInfos = &LOC::Machine::getInfos($countryId, $machineId, LOC::Machine::GETINFOS_TELEMATICS);
    if (!$tabMachineInfos || !$tabMachineInfos->{'telematicId'})
    {
        return 0;
    }

    # Récupération de la liste des codes réservés
    my @tabReservedCodes = split(/,/, &LOC::Characteristic::getCountryValueByCode('DIGIRESERV', $countryId));

    # Calcul de la date de fin
    my $endDate = &_addGracePeriodToDate(
        &LOC::Date::getMySQLDate(),
        $tabMachineInfos->{'agency.id'},
        undef,
        undef
    );
    $endDate .= ' 23:59:59';

    # Modification de la date de fin de validité des codes réservés sur la machine
    my $servicesWs = &LOC::Globals::get('servicesWs');
    my $service = SOAP::Lite->uri($servicesWs->{'telematics'}->{'uri'})
                            ->proxy($servicesWs->{'telematics'}->{'location'})
                            ->setStartCodesEndDate($tabMachineInfos->{'telematicId'}, \@tabReservedCodes, $endDate);
    if ($service->fault())
    {
        return 0;
    }

    return 1;
}


# Function: changeEquipment
# Initialise le boîtier suite à un changement
#
# Parameters:
# string  $countryId  - Pays
# int     $machineId  - Identifiant de la machine
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# int - 1 si c'est un succès, 0 sinon
sub changeEquipment
{
    my ($countryId, $machineId, $userId, $tabErrors, $tabEndUpds) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'changeEquipment', $_[0], __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, $_[1]);
        return 0;
    };


    # Récupération des informations de la machine
    my $tabMachineInfos = &LOC::Machine::getInfos($countryId, $machineId, LOC::Machine::GETINFOS_TELEMATICS);
    if (!$tabMachineInfos || !$tabMachineInfos->{'telematicId'})
    {
        return 0;
    }

    # Envoi d'un commit au boîtier
    my $servicesWs = &LOC::Globals::get('servicesWs');
    my $service = SOAP::Lite->uri($servicesWs->{'telematics'}->{'uri'})
                            ->proxy($servicesWs->{'telematics'}->{'location'})
                            ->resendStartCodeRules($tabMachineInfos->{'telematicId'});
    if (my $fault = $service->fault())
    {
        return 0;
    }

    # Ménage des règles qui ne seront jamais envoyées au boîtier
    # - Règles en cours de désapplication
    my $tabFilters = {
        'rule' => {
            'machineId' => $machineId,
            'unapplicationId' => STATE_RULE_UNAPPLICATION_PENDING
        }
    };
    my %tabCodes = &getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters, GETINFOS_RULESLIST);
    my @tabRulesIds;
    foreach my $codeInfos (values(%tabCodes))
    {
        push(@tabRulesIds, keys(%{$codeInfos->{'rulesList'}}));
    }
    if (@tabRulesIds > 0)
    {
        # -- Passage en "non désappliquée"
        my $tabUpdates = {
            'scr_sta_id_unapplication' => STATE_RULE_UNAPPLICATION_NOTDONE
        };
        if ($db->update('f_startcoderule', $tabUpdates,
                        {'scr_id' => \@tabRulesIds}, $schemaName) == -1)
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, 0x0001);
        }
    }

    # - Règles en cours d'application
    my $tabFilters = {
        'rule' => {
            'machineId'     => $machineId,
            'applicationId' => STATE_RULE_APPLICATION_PENDING,
            'hasEndDate'    => 1,
            'maxEndDate'    => 'NOW'
        }
    };
    my %tabCodes = &getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters, GETINFOS_RULESLIST);

    foreach my $codeInfos (values(%tabCodes))
    {
        my @tabRulesIds = keys(%{$codeInfos->{'rulesList'}});
        if (@tabRulesIds > 0)
        {
            # -- Passage en "obsolète" + "non appliquée"
            my $tabUpdates = {
                'scr_sta_id'             => STATE_RULE_OBSOLETE,
                'scr_sta_id_application' => STATE_RULE_APPLICATION_NOTDONE
            };
            if ($db->update('f_startcoderule', $tabUpdates,
                            {'scr_id' => \@tabRulesIds}, $schemaName) == -1)
            {
                # Erreur fatale !
                return &$rollBackAndError(__LINE__, 0x0001);
            }
        }

        # -- Vérification passage code en obsolète
        my $nbActiveRules = &_getRulesList($db, $countryId, LOC::Util::GETLIST_COUNT, $codeInfos->{'id'}, {
            'stateId' => STATE_RULE_ACTIVE
        });
        if ($nbActiveRules == 0)
        {
            if ($db->update('f_startcode',
                            {'sco_is_codereserved' => 0,
                             'sco_sta_id'          => STATE_OBSOLETE},
                            {'sco_id' => $codeInfos->{'id'}}, $schemaName) == -1)
            {
                # Erreur fatale !
                return &$rollBackAndError(__LINE__, 0x0001);
            }
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    return 1;
}


# Function: _addGracePeriodToDate
# Ajoute la période de grâce à une date
#
# Parameters:
# string $date      - Date
# string $agencyId  - Identifiant de l'agence
# string $typeCode  - Type de code
# string $valueDate - Date de valeur
#
# Returns:
# string
sub _addGracePeriodToDate
{
    my ($date, $agencyId, $typeCode, $valueDate) = @_;

    # Récupération du nombre de jours supplémentaires et du mode de calcul dans la carac
    my $tabCfgGracePeriod = &LOC::Json::fromJson(
        &LOC::Characteristic::getAgencyValueByCode(
            'DIGIPERGRACE',
            $agencyId,
            $valueDate
        )
    );

    if (!$typeCode || !exists $tabCfgGracePeriod->{$typeCode})
    {
        $typeCode = 'default';
    }

    my $newDate;
    my $graceMode = $tabCfgGracePeriod->{$typeCode}->{'mode'};
    if ($graceMode eq 'working')
    {
        $newDate = &LOC::Date::addWorkingDays(
            $date,
            $tabCfgGracePeriod->{$typeCode}->{'days'},
            $agencyId
        );
    }
    elsif ($graceMode eq 'calendar')
    {
        $newDate = &LOC::Date::addDays(
            $date,
            $tabCfgGracePeriod->{$typeCode}->{'days'},
            $agencyId
        );
    }
    else
    {
        $newDate = $date;
    }

    return $newDate;
}


# Function: _generateCode
# Génère un code
#
# Parameters:
# string         $countryId           - Pays
# arrayref|undef $tabUnavailableCodes - Liste des codes non disponibles (facultatif)
#
# Returns:
# string
sub _generateCode
{
    my ($countryId, $tabUnavailableCodes) = @_;

    # Calcul de la liste des codes
    my @tabAllCodes = ();
    my $nbCodes = 10 ** CODE_LENGTH;
    my $format  = '%0' . CODE_LENGTH . 'd';
    for (my $i = 0; $i < $nbCodes; $i++)
    {
        push(@tabAllCodes, sprintf($format, $i));
    }

    # Liste des codes disponibles
    my @tabAvailableCodes = &LOC::Util::array_minus(\@tabAllCodes, $tabUnavailableCodes);

    # Si il n'y a plus de codes disponibles alors on retourne vide
    my $nbAvailableCodes = @tabAvailableCodes;
    if ($nbAvailableCodes == 0)
    {
        return undef;
    }

    # Choix aléatoire d'un des codes disponibles
    return $tabAvailableCodes[rand($nbAvailableCodes)];
}


# Function: _getRulesWheresFromFilters
# Retourne la liste des conditions sur les règles pour le getList() et le _getRulesList()
#
# Parameters:
# LOC::Db::Adapter $db         - Connexion à la base de données
# hashref          $tabFilters - Filtres
#
# Returns:
# array - Conditions
sub _getRulesWheresFromFilters
{
    my ($db, $tabFilters) = @_;

    my @tabWheres = ();

    # - Machine
    if (defined $tabFilters->{'machineId'})
    {
        push(@tabWheres, 'scr_mac_id IN (' . &LOC::Util::join(', ', $tabFilters->{'machineId'}) . ')');
    }
    # - État de règles
    if (defined $tabFilters->{'stateId'})
    {
        push(@tabWheres, 'scr_sta_id IN (' . &LOC::Util::join(', ', $db->quote($tabFilters->{'stateId'})) . ')');
    }
    # - Date de début de validité
    if (defined $tabFilters->{'hasBeginDate'})
    {
        push(@tabWheres, 'scr_date_begin IS ' . ($tabFilters->{'hasBeginDate'} ? 'NOT ' : '')  . 'NULL');
    }
    # - Date de fin de validité
    if (defined $tabFilters->{'hasEndDate'})
    {
        push(@tabWheres, 'scr_date_end IS ' . ($tabFilters->{'hasEndDate'} ? 'NOT ' : '')  . 'NULL');
    }
    # - Comparaison de la date de fin
    if (defined $tabFilters->{'maxEndDate'})
    {
        my $maxEndDate = $tabFilters->{'maxEndDate'} eq 'NOW' ? 'NOW()' : $db->quote($tabFilters->{'maxEndDate'});
        push(@tabWheres, 'scr_date_end < ' . $maxEndDate);
    }
    # - État d'application des règles
    if (defined $tabFilters->{'applicationId'})
    {
        push(@tabWheres, 'scr_sta_id_application IN (' . &LOC::Util::join(', ', $db->quote($tabFilters->{'applicationId'})) . ')');
    }
    # - État de déapplication des règles
    if (defined $tabFilters->{'unapplicationId'})
    {
        push(@tabWheres, 'scr_sta_id_unapplication IN (' . &LOC::Util::join(', ', $db->quote($tabFilters->{'unapplicationId'})) . ')');
    }
    # - États "courants"
    if (defined $tabFilters->{'isCurrent'})
    {
        my $condition = '(
                             scr_sta_id_unapplication IS NULL AND
                             (
                                 scr_sta_id_application = ' . $db->quote(STATE_RULE_APPLICATION_PENDING) . ' OR
                                 scr_sta_id_application = ' . $db->quote(STATE_RULE_APPLICATION_DONE) . ' AND
                                 scr_sta_id = ' . $db->quote(STATE_RULE_ACTIVE) . '
                             ) OR
                             scr_sta_id_unapplication = ' . $db->quote(STATE_RULE_UNAPPLICATION_PENDING) . '
                         )';

        push(@tabWheres, ($tabFilters->{'isCurrent'} ? $condition : 'NOT ' . $condition));
    }

    return @tabWheres;
}


# Function: _getRulesList
# Retourne la liste des règles des codes de démarrage
#
# Parameters:
# string  $countryId  - Pays
# hashref $tabFilters - Filtres (facultatif)
#
# Returns:
# hashref|undef - Liste des règles des codes de démarrage
sub _getRulesList
{
    my ($db, $countryId, $format, $startCodeId, $tabFilters) = @_;

    my @tabWheres = ();

    my $query;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    scr_id AS `id`,
    scr_extruleid AS `externId`,
    scr_mac_id AS `machine.id`,
    CONCAT(IFNULL(tbl_f_machine.MANOPARC_AUX, ""), tbl_f_machine.MANOPARC) AS `machine.parkNumber`,
    scr_date_begin AS `beginDate`,
    scr_date_end AS `endDate`,
    scr_date_creation AS `creationDate`,
    scr_date_deletion AS `deletionDate`,
    scr_sta_id AS `state.id`,
    scr_sta_id_application AS `application.id`,
    scr_sta_id_unapplication AS `unapplication.id`
FROM f_startcoderule
INNER JOIN AUTH.MACHINE tbl_f_machine
ON tbl_f_machine.MAAUTO = scr_mac_id';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(scr_id)
FROM f_startcoderule
INNER JOIN AUTH.MACHINE tbl_f_machine
ON tbl_f_machine.MAAUTO = scr_mac_id';
    }
    else
    {
        $query = '
SELECT
    scr_id,
    scr_mac_id
FROM f_startcoderule
INNER JOIN AUTH.MACHINE tbl_f_machine
ON tbl_f_machine.MAAUTO = scr_mac_id';
    }

    # Filtres
    # - Identifiant du code
    push(@tabWheres, 'scr_sco_id = ' . $startCodeId);

    # Ajout des filtres commun entre getList() et _getRulesList()
    push(@tabWheres, &_getRulesWheresFromFilters($db, $tabFilters));

    if (@tabWheres > 0)
    {
        $query .= '
WHERE ' . join("\nAND ", @tabWheres);
    }

    # Tri
    $query .= '
ORDER BY scr_date_creation, scr_id;';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    # Formatage des champs
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'} *= 1;
            $tabRow->{'machine.id'} *= 1;
        }
    }

    return $tab;
}


# Function: isTelematActived
# Indique si la télématique est activée ou non
#
# Parameters:
# string $countryId - Pays
#
# Returns:
# bool
sub isTelematActived
{
    my ($countryId) = @_;

    my $isActived = &LOC::Characteristic::getCountryValueByCode('TELEMAT', $countryId);

    return ($isActived != 0 ? 1 : 0);
}


# Function: _isQueueActived
# Indique si l'envoi des règles est activée ou non
#
# Parameters:
# string $countryId - Pays
#
# Returns:
# bool
sub _isQueueActived
{
    my ($countryId) = @_;

    my $isActived = &LOC::Characteristic::getCountryValueByCode('SYNCTRAQUEUR', $countryId);

    return ($isActived != 0 ? 1 : 0);
}


# Function: _addRule
# Ajoute une règle à un code existant
#
# Parameters:
# LOC::Db::Adapter $db          - Connexion à la base de données
# string           $countryId   - Pays
# hashref          $startCodeId - Identifiant du code de démarrage
# string           $machineId   - Identifiant de la machine
# string           $beginDate   - Date de début de validité (yyyy-mm-dd hh:mm:ss)
# string           $endDate     - Date de fin de validité (yyyy-mm-dd hh:mm:ss)
# int              $userId      - Utilisateur responsable
# hashref          $tabEndUpds  - Mises à jour de fin
#
# Returns:
# bool
sub _addRule
{
    my ($db, $countryId, $startCodeId, $machineId, $beginDate, $endDate, $userId, $tabEndUpds) = @_;

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my $currentDateTime = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATETIME);
    my $isExpired = ($endDate ne '' && $endDate le $currentDateTime);

    # Ajout des heures au dates de début et fin
    if ($beginDate ne '' && length($beginDate) == 10)
    {
        $beginDate = $beginDate . ' 00:00:00';
    }
    if ($endDate ne '' && length($endDate) == 10)
    {
        $endDate = $endDate . ' 23:59:59';
    }

    # Insertion de la règle
    my $tabUpdates = {
        'scr_sco_id'               => $startCodeId,
        'scr_mac_id'               => $machineId,
        'scr_date_begin'           => ($beginDate ne '' ? $beginDate : undef),
        'scr_date_end'             => ($endDate   ne '' ? $endDate   : undef),
        'scr_date_creation*'       => 'NOW()',
        'scr_sta_id'               => STATE_RULE_ACTIVE,
        'scr_sta_id_application'   => ($isExpired ? STATE_RULE_APPLICATION_NOTDONE : STATE_RULE_APPLICATION_PENDING),
        'scr_sta_id_unapplication' => undef
    };
    my $ruleId = $db->insert('f_startcoderule', $tabUpdates, $schemaName);
    if ($ruleId == -1)
    {
        return 0;
    }

    # Insertion de la demande d'ajout de la règle dans la file d'attente
    if (!$isExpired)
    {
        if (!&_addQueueAddAction($db, $countryId, $ruleId, $userId, $tabEndUpds))
        {
            return 0;
        }
    }

    return 1;
}


# Function: _deleteRule
# Supprime une règle active
#
# Parameters:
# LOC::Db::Adapter $db         - Connexion à la base de données
# string           $countryId  - Pays
# hashref          $ruleId     - Identifiant de la règle
# int              $userId     - Utilisateur responsable
# hashref          $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub _deleteRule
{
    my ($db, $countryId, $ruleId, $userId, $tabEndUpds) = @_;

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Attente du verrou sur la règle
    my $query = '
SELECT
    scr_extruleid AS `externRuleId`,
    scr_sta_id_application AS `application.id`,
    scr_sta_id_unapplication AS `unapplication.id`
FROM f_startcoderule
WHERE scr_id = ' . $ruleId . '
FOR UPDATE;';

    my $tabInfos = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if (!$tabInfos)
    {
        return 0;
    }

    # Mise à jour de la règle
    my $tabUpdates = {
        'scr_sta_id' => STATE_RULE_OBSOLETE
    };

    # Si il n'y a pas déjà eu de demande de suppression...
    if ($tabInfos->{'unapplication.id'} eq '')
    {
        my $externRuleId = ($tabInfos->{'externRuleId'} ? $tabInfos->{'externRuleId'} * 1 : undef);

        $tabUpdates->{'scr_date_deletion*'} = 'NOW()';

        if (!$externRuleId)
        {
            if ($tabInfos->{'application.id'} eq STATE_RULE_APPLICATION_PENDING)
            {
                # État d'application de la règle
                $tabUpdates->{'scr_sta_id_application'} = STATE_RULE_APPLICATION_NOTDONE;

                # Dans le cas où la demande d'ajout n'a pas été envoyée à Traqueur
                # on lui met l'état "obsolète" et on ne créé pas la nouvelle ligne
                if (!&_expireQueueAddAction($db, $countryId, $ruleId, $userId, $tabEndUpds))
                {
                    return 0;
                }
            }
        }
        else
        {
            # État de désapplication de la règle
            $tabUpdates->{'scr_sta_id_unapplication'} = STATE_RULE_UNAPPLICATION_PENDING;

            # Insertion de la demande de suppression de la règle dans la file d'attente
            if (!&_addQueueDeleteAction($db, $countryId, $ruleId, $externRuleId, $userId, $tabEndUpds))
            {
                return 0;
            }
        }
    }

    if ($db->update('f_startcoderule', $tabUpdates, {'scr_id' => $ruleId * 1}, $schemaName) == -1)
    {
        return 0;
    }

    return 1;
}


# Function: _addQueueAddAction
# Ajout d'une action d'ajout dans la file d'attente
#
# Parameters:
# LOC::Db::Adapter $db         - Connexion à la base de données
# string           $countryId  - Pays
# int              $ruleId     - Identifiant de la règle
# int              $userId     - Utilisateur responsable
# hashref          $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub _addQueueAddAction
{
    my ($db, $countryId, $ruleId, $userId, $tabEndUpds) = @_;

    # Si l'envoi des règles n'est pas activé alors on ne fait rien
    if (!&_isQueueActived($countryId))
    {
        return 1;
    }

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);


    # Récupération des informations sur la règle
    my $query = '
SELECT
    tbl_f_machine.IDTarget AS `targetId`,
    sct_extid AS `externTypeId`,
    sco_code AS `code`,
    scr_date_begin AS `beginDate`,
    scr_date_end AS `endDate`,
    sct_easyrestartperiod AS `easyRestartPeriod`
FROM f_startcoderule
INNER JOIN AUTH.MACHINE tbl_f_machine
ON tbl_f_machine.MAAUTO = scr_mac_id
INNER JOIN f_startcode
ON sco_id = scr_sco_id
INNER JOIN p_startcodetype
ON sct_id = sco_sct_id
WHERE scr_id = ' . $ruleId . ';';

    my $tabInfos = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);

    my $targetId = ($tabInfos->{'targetId'} ? $tabInfos->{'targetId'} * 1 : undef);
    if (!$targetId)
    {
        return 0;
    }

    my $tabUpdates = {
        'qsc_scr_id'            => $ruleId,
        'qsc_action'            => RULEQUEUE_ACTION_ADD,
        'qsc_exttargetid'       => $targetId,
        'qsc_extruleid'         => undef,
        'qsc_exttypeid'         => $tabInfos->{'externTypeId'},
        'qsc_code'              => $tabInfos->{'code'},
        'qsc_date_begin'        => $tabInfos->{'beginDate'},
        'qsc_date_end'          => $tabInfos->{'endDate'},
        'qsc_easyrestartperiod' => $tabInfos->{'easyRestartPeriod'},
        'qsc_extplanningid'     => undef,
        'qsc_date_creation*'    => 'NOW()',
        'qsc_date_sent'         => undef,
        'qsc_is_committed'      => 0,
        'qsc_sta_id'            => STATE_RULEQUEUE_PENDING
    };

    # Insertion de la ligne
    if ($db->insert('q_startcoderule', $tabUpdates, $schemaName) == -1)
    {
        return 0;
    }

    return 1;
}


# Function: _addQueueDeleteAction
# Ajout d'une action dans la file d'attente
#
# Parameters:
# LOC::Db::Adapter $db           - Connexion à la base de données
# string           $countryId    - Pays
# int              $ruleId       - Identifiant de la règle
# int              $externRuleId - Identifiant externe de la règle
# int              $userId       - Utilisateur responsable
# hashref          $tabEndUpds   - Mises à jour de fin
#
# Returns:
# bool
sub _addQueueDeleteAction
{
    my ($db, $countryId, $ruleId, $externRuleId, $userId, $tabEndUpds) = @_;

    # Si l'envoi des règles n'est pas activé alors on ne fait rien
    if (!&_isQueueActived($countryId))
    {
        return 1;
    }

    if (!$externRuleId)
    {
        return 0;
    }

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);


    # Récupération des informations sur la règle
    my $query = '
SELECT
    tbl_f_machine.IDTarget AS `targetId`
FROM f_startcoderule
INNER JOIN AUTH.MACHINE tbl_f_machine
ON tbl_f_machine.MAAUTO = scr_mac_id
WHERE scr_id = ' . $ruleId . ';';

    my $tabInfos = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);

    my $targetId = ($tabInfos->{'targetId'} ? $tabInfos->{'targetId'} * 1 : undef);
    if (!$targetId)
    {
        return 0;
    }

    my $tabUpdates = {
        'qsc_scr_id'            => $ruleId,
        'qsc_action'            => RULEQUEUE_ACTION_DELETE,
        'qsc_exttargetid'       => $targetId,
        'qsc_extruleid'         => $externRuleId,
        'qsc_exttypeid'         => undef,
        'qsc_code'              => undef,
        'qsc_date_begin'        => undef,
        'qsc_date_end'          => undef,
        'qsc_easyrestartperiod' => undef,
        'qsc_extplanningid'     => undef,
        'qsc_date_creation*'    => 'NOW()',
        'qsc_date_sent'         => undef,
        'qsc_is_committed'      => 0,
        'qsc_sta_id'            => STATE_RULEQUEUE_PENDING
    };

    # Insertion de la ligne
    if ($db->insert('q_startcoderule', $tabUpdates, $schemaName) == -1)
    {
        return 0;
    }

    return 1;
}


# Function: _expireQueueAddAction
# Péremption d'une action d'ajout dans la file d'attente
#
# Parameters:
# LOC::Db::Adapter $db         - Connexion à la base de données
# string           $countryId  - Pays
# int              $ruleId     - Identifiant de la règle
# int              $userId     - Utilisateur responsable
# hashref          $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub _expireQueueAddAction
{
    my ($db, $countryId, $ruleId, $userId, $tabEndUpds) = @_;

    # Si l'envoi des règles n'est pas activé alors on ne fait rien
    if (!&_isQueueActived($countryId))
    {
        return 1;
    }

    # Récupération de la connexion à la base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);


    if ($db->update('q_startcoderule', {
            'qsc_sta_id' => STATE_RULEQUEUE_OBSOLETE
        }, {
            'qsc_scr_id' => $ruleId,
            'qsc_action' => RULEQUEUE_ACTION_ADD,
            'qsc_sta_id' => STATE_RULEQUEUE_PENDING
        }) == -1)
    {
        return 0;
    }

    return 1;
}


# Function: _lockDbRow
# Verrou sur la ligne du code de démarrage
#
# Parameters:
# LOC::Db::Adapter $db - Connexion à la base de données
# int              $id - Identifiant du code de démarrage
#
# Returns:
# bool
sub _lockDbRow
{
    my ($db, $startCodeId) = @_;

    # Attente du verrou sur le code
    my $query = '
SELECT
    sco_id
FROM f_startcode
WHERE sco_id = ' . $startCodeId . '
FOR UPDATE;';

    return ($db->query($query) ? 1 : 0);
}


1;