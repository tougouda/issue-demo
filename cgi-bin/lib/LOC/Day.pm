use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Day
# Module permettant d'obtenir les informations sur les J+/J-
package LOC::Day;


# Constants: Récupération d'informations sur les pays
# GETINFOS_ALL  - Récupération de l'ensemble des informations
# GETINFOS_BASE - Récupération des informations de base
use constant
{
    GETINFOS_ALL               => 0xFFFFFFFF,
    GETINFOS_BASE              => 0x00000000,
    GETINFOS_DURATION          => 0x00000001,
    GETINFOS_APPLIEDTOCONTRACT => 0x00000002,
    GETINFOS_CONTRACT          => 0x00000004
};

# Constants: Récupération d'informations sur les contrats liés
# GETAPPLIEDTOCONTRACTINFOS_ALL     - Récupération de l'ensemble des informations
# GETAPPLIEDTOCONTRACTINFOS_BASE    - Récupération des informations de base
# GETAPPLIEDTOCONTRACTINFOS_DETAILS - Récupération des informations détaillées
use constant
{
    GETAPPLIEDTOCONTRACTINFOS_ALL     => 0xFFFFFFFF,
    GETAPPLIEDTOCONTRACTINFOS_BASE    => 0x00000000,
    GETAPPLIEDTOCONTRACTINFOS_DETAILS => 0x00000001
};


# Constants: États
# STATE_ACTIVE  - Actif
# STATE_DELETED - Supprimé
use constant
{
    STATE_ACTIVE  => 'GEN01',
    STATE_DELETED => 'GEN03'
};

# Constants: Types
# TYPE_ADDITIONAL  - Jour +
# TYPE_DEDUCTED    - Jour -
use constant
{
    TYPE_ADDITIONAL => 'A',
    TYPE_DEDUCTED   => 'D'
};

# Constants: Flags
# FLAG_DEDUCTEDDAY   - Valable pour les jours déduits
# FLAG_ADDITIONALDAY - Valable pour les jours supplémentaires
use constant
{
    FLAG_DEDUCTEDDAY   => 1,
    FLAG_ADDITIONALDAY => 2
};

# Constants: Valeurs par défaut
# DEFAULT_CONTRACT_TABINDEX     - Onglet Durée à activer lors de l'ouverture du contrat de location
# DEFAULT_ESTIMATELINE_TABINDEX - Onglet Durée à activer lors de l'ouverture du devis de location
use constant
{
    DEFAULT_CONTRACT_TABINDEX      => 1,
    DEFAULT_ESTIMATELINE_TABINDEX  => 1
};

# Constants: Erreurs
# ERROR_FATAL       - Erreur fatale
# ERROR_RIGHTS      - Impossible de modifier les informations
# ERROR_TYPEREQUIRED - Type obligatoire
# ERROR_DATEREQUIRED - Date obligatoire
use constant
{
    ERROR_FATAL                    => 0x0001,
    ERROR_RIGHTS                   => 0x0003,

    ERROR_TYPEREQUIRED             => 0x0010,
    ERROR_DATEREQUIRED             => 0x0011,
    ERROR_REASONREQUIRED           => 0x0012,
    ERROR_EXISTINGDAY              => 0x0013,
    ERROR_OUTOFPERIOD              => 0x0014,
    ERROR_ALREADYINVOICEABLE       => 0x0015,
    ERROR_ALREADYNOTINVOICEABLE    => 0x0016,
    ERROR_INVALIDAPPLIEDTOCONTRACT => 0x0017
};

use strict;

# Modules internes
use LOC::Db;
use LOC::Util;
use LOC::Json;
use LOC::Date;
use LOC::EndUpdates;
use LOC::Locale;
use LOC::Day::InvoiceDuration;
use LOC::Contract::Rent;
use LOC::Estimate::Rent;
use LOC::Characteristic;
use LOC::Country;

my $fatalErrMsg = 'Erreur fatale: module LOC::Day, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";



# Function: getInfos
# Retourne les infos d'un jour supplémentaire ou déduit
#
# Parameters:
# string    $id        - Id du jour
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getInfos
{
    my ($countryId, $id) = @_;

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id});
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Retourne la liste des pays
#
# Contenu d'une ligne du hashage :
#
# Parameters:
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|0 - Liste des pays
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (ref($tabFilters) ne 'HASH')
    {
        $tabFilters = {};
    }
    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my @tabWheres = ();

    my $query;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    day_id AS `id`,
    day_type AS `type`,
    day_date AS `date`,
    day_agc_id AS `agency.id`,
    day_rel_id AS `estimateLine.id`,
    day_rct_id AS `contract.id`,
    day_rct_id_appliedto AS `appliedToContract.id`,
    day_did_id AS `invoiceDuration.id`,
    did_label AS `invoiceDuration.label`,
    did_duration AS `invoiceDuration.duration`,
    day_dre_id AS `reason.id`,
    dre_label AS `reason.label`,
    day_comment AS `comment`,
    day_is_invoiceable AS `isInvoiceable`,
    day_is_invoiced AS `isInvoiced`,
    day_is_migrated AS `isMigrated`,
    day_usr_id_creator AS `creator.id`,
    day_date_creation AS `creationDate`,
    day_date_modification AS `modificationDate`,
    day_sta_id AS `state.id`
FROM f_day
INNER JOIN p_dayinvoiceduration ON did_id = day_did_id
LEFT JOIN p_dayreason ON dre_id = day_dre_id';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(day_id)
FROM f_day';
    }
    else
    {
        $query = '
SELECT DISTINCT
    day_id,
    CONCAT_WS(" / ", day_type, day_date)
FROM f_day';
    }

    # Filtres
    # - Id
    if (exists $tabFilters->{'id'})
    {
        push(@tabWheres, 'day_id IN (' . $db->quote($tabFilters->{'id'}) . ')');
    }
    if (exists $tabFilters->{'!id'})
    {
        push(@tabWheres, 'day_id NOT IN (' . $db->quote($tabFilters->{'!id'}) . ')');
    }
    # - Type
    if (exists $tabFilters->{'type'})
    {
        push(@tabWheres, 'day_type IN (' . $db->quote($tabFilters->{'type'}) . ')');
    }
    # - État
    if (exists $tabFilters->{'stateId'})
    {
        push(@tabWheres, 'day_sta_id IN (' . $db->quote($tabFilters->{'stateId'}) . ')');
    }
    else
    {
        push(@tabWheres, 'day_sta_id <> ' . $db->quote(STATE_DELETED));
    }
    # - Ligne de devis
    if (exists $tabFilters->{'estimateLineId'})
    {
        if (ref $tabFilters->{'estimateLineId'} ne 'HASH')
        {
            push(@tabWheres, 'day_rel_id IN (' . $db->quote($tabFilters->{'estimateLineId'}) . ')');
        }
        else
        {
            my $values = $tabFilters->{'estimateLineId'}->{'values'};
            # - Extension du filtre ligne de devis (tous les jours concernant directement la ligne de devis)
            if ($tabFilters->{'estimateLineId'}->{'isChargedTo'})
            {
                push(@tabWheres, '(day_rel_id IN (' . $db->quote($values) . ') AND day_rct_id_appliedto IS NULL)');
            }
            else
            {
                push(@tabWheres, 'day_rel_id IN (' . $db->quote($values) . ')');
            }
        }
    }
    # - Contrat
    if (exists $tabFilters->{'contractId'})
    {
        if (ref $tabFilters->{'contractId'} ne 'HASH')
        {
            push(@tabWheres, 'day_rct_id IN (' . $db->quote($tabFilters->{'contractId'}) . ')');
        }
        else
        {
            my $values = $tabFilters->{'contractId'}->{'values'};
            # - Extension du filtre contrat :
            #   - tous les jours concernant directement le contrat
            if ($tabFilters->{'contractId'}->{'isChargedTo'})
            {
                push(@tabWheres, '((day_rct_id IN (' . $db->quote($values) . ') AND day_rct_id_appliedto IS NULL)
                                    OR (day_rct_id_appliedto IN (' . $db->quote($values) . ') AND
                                        day_rct_id IS NOT NULL AND
                                        (SELECT ETCODE FROM CONTRAT WHERE CONTRATAUTO = day_rct_id) != ' . $db->quote(LOC::Contract::Rent::STATE_CANCELED) . '))');
            }
            #   - tous les jours saisis sur un autre contrat
            elsif ($tabFilters->{'contractId'}->{'isExternal'})
            {
                push(@tabWheres, '(day_rct_id_appliedto IN (' . $db->quote($values) . ') AND
                                    day_rct_id IS NOT NULL AND
                                    (SELECT ETCODE FROM CONTRAT WHERE CONTRATAUTO = day_rct_id) != ' . $db->quote(LOC::Contract::Rent::STATE_CANCELED) . ')');
            }
            else
            {
                push(@tabWheres, 'day_rct_id IN (' . $db->quote($values) . ')');
            }
        }
    }
    # - Facturable
    if (exists $tabFilters->{'isInvoiceable'})
    {
        push(@tabWheres, 'day_is_invoiceable = ' . ($tabFilters->{'isInvoiceable'} ? 1 : 0));
    }
    # - Facturé
    if (exists $tabFilters->{'isInvoiced'})
    {
        push(@tabWheres, 'day_is_invoiced = ' . ($tabFilters->{'isInvoiced'} ? 1 : 0));
    }
    # - Date
    if (exists $tabFilters->{'date'})
    {
        my $includeEmptyDates = 0;
        my $tabValues = $tabFilters->{'date'};

        if (ref($tabFilters->{'date'}) eq 'HASH')
        {
            if (defined $tabFilters->{'date'}->{'includeEmpty'})
            {
                $includeEmptyDates = 1;
            }
            if (defined $tabFilters->{'date'}->{'values'})
            {
                $tabValues = $tabFilters->{'date'}->{'values'};
            }
        }

        # Condition si la date est définie
        my @tabDateWheres = ();
        if (ref($tabValues) eq 'ARRAY')
        {
            if (defined $tabValues->[0])
            {
                push(@tabDateWheres, 'day_date >= ' . $db->quote($tabValues->[0]));
            }
            if (defined $tabValues->[1])
            {
                push(@tabDateWheres, 'day_date <= ' . $db->quote($tabValues->[1]));
            }
        }
        else
        {
            push(@tabDateWheres, 'day_date = ' . $db->quote($tabValues));
        }

        # Inclusion des dates vides
        if ($includeEmptyDates)
        {
            my $dateWhere = 'day_date IS NULL';
            if (@tabDateWheres > 0)
            {
                $dateWhere .= ' OR (' . join(" AND ", @tabDateWheres) . ')';
            }
            push(@tabWheres, '(' . $dateWhere . ')');
        }
        else
        {
            push(@tabWheres, @tabDateWheres);
        }
    }

    if (@tabWheres > 0)
    {
        $query .= '
WHERE ' . join("\nAND ", @tabWheres);
    }

    # Tris
    if (defined $tabOptions->{'orderBy'})
    {
        my $tabOrderFields = {
            'date'             => 'day_date',
            'toComplete'       => '(day_is_invoiced = 0 AND day_date IS NULL)',
            'stateId'          => 'day_sta_id',
            'creationDate'     => 'day_date_creation',
            'modificationDate' => 'day_date_modification',
            'contractId'       => 'day_rct_id'
        };
        my @tabOrderBy;
        foreach my $orderBy (@{$tabOptions->{'orderBy'}})
        {
            my ($field, $dir) = split(/:/, $orderBy);
            push(@tabOrderBy, $tabOrderFields->{$field} . ' ' . (defined $dir && uc($dir) eq 'DESC' ? 'DESC' : 'ASC'));
        }
        if (@tabOrderBy > 0)
        {
            $query .= '
ORDER BY ' . join(', ', @tabOrderBy);
        }
    }
    else
    {
        $query .= '
ORDER BY day_date DESC';
    }

    if (defined $tabOptions->{'limit'})
    {
        $query .= '
LIMIT ' . $tabOptions->{'limit'};
    }

    $query .= ';';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return 0;
    }
    # Formatage des champs
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            my $agencyId = $tabRow->{'agency.id'};
            $tabRow->{'id'}                 *= 1;
            $tabRow->{'estimateLine.id'}     = ($tabRow->{'estimateLine.id'} ? $tabRow->{'estimateLine.id'} * 1 : undef);
            $tabRow->{'contract.id'}         = ($tabRow->{'contract.id'} ? $tabRow->{'contract.id'} * 1 : undef);
            $tabRow->{'appliedToContract.id'} = ($tabRow->{'appliedToContract.id'} ? $tabRow->{'appliedToContract.id'} * 1 : undef);
            $tabRow->{'invoiceDuration.id'} *= 1;
            $tabRow->{'reason.id'}          *= 1;
            $tabRow->{'isInvoiceable'}      *= 1;
            $tabRow->{'isInvoiced'}         *= 1;
            $tabRow->{'isMigrated'}         *= 1;
            $tabRow->{'creator.id'}          = $tabRow->{'creator.id'} * 1;

            if ($flags & GETINFOS_APPLIEDTOCONTRACT && $tabRow->{'appliedToContract.id'})
            {
                $tabRow->{'appliedToContract.tabInfos'} = &getAppliedToContractInfos($countryId, $tabRow->{'appliedToContract.id'}, $tabOptions->{'getAppliedToContractFlags'});
            }

            if ($flags & GETINFOS_CONTRACT)
            {
                $tabRow->{'contract.tabInfos'} = &LOC::Contract::Rent::getInfos($countryId, $tabRow->{'contract.id'});
            }

            if ($flags & GETINFOS_DURATION)
            {
                $tabRow->{'invoiceDuration.tabInfos'} = &LOC::Day::InvoiceDuration::getInfos($countryId, $tabRow->{'invoiceDuration.id'});
            }

            # Récupération des date de début max et date de fin min autorisées sur le document pour ce jour
            if ($tabRow->{'date'})
            {
                my $valueDate;
                my $documentType;
                if ($tabRow->{'appliedToContract.id'})
                {
                    $valueDate = &LOC::Contract::Rent::getValueDate($countryId, $tabRow->{'appliedToContract.id'});
                    $documentType = 'contract';
                }
                elsif ($tabRow->{'contract.id'})
                {
                    $valueDate = &LOC::Contract::Rent::getValueDate($countryId, $tabRow->{'contract.id'});
                    $documentType = 'contract';
                }
                else
                {
                    $valueDate = &LOC::Estimate::Rent::getLineValueDate($countryId, $tabRow->{'estimateLine.id'});
                    $documentType = 'estimateLine';
                }

                my $periodLimitDates = &getPeriodLimitDates($agencyId, $tabRow->{'type'}, $documentType, $tabRow->{'date'}, $valueDate);
                $tabRow->{'beginDateMax'} = $periodLimitDates->{'beginDateMax'};
                $tabRow->{'endDateMin'}   = $periodLimitDates->{'endDateMin'};
            }
        }
    }

    return (wantarray ? %$tab : $tab);
}

# Function: getAppliedToContractInfos
# Récupère les informations nécessaires pour le contrat lié
#
# Parameters:
# string $countryId            - Identifiant du pays
# int    $appliedToContractId  - Identifiant du contrat lié
sub getAppliedToContractInfos
{
    my ($countryId, $appliedToContractId, $flags) = @_;

    if (!defined $flags)
    {
        $flags = GETAPPLIEDTOCONTRACTINFOS_BASE;
    }

    my $tabContractInfos = &LOC::Contract::Rent::getInfos($countryId, $appliedToContractId,
                                                          LOC::Contract::Rent::GETINFOS_BASE |
                                                          LOC::Contract::Rent::GETINFOS_VALUEDATE);
    if ($tabContractInfos)
    {
        my $tabDays = undef;
        if ($flags & GETAPPLIEDTOCONTRACTINFOS_DETAILS)
        {
            my $tabDaysList = &getList($countryId, LOC::Util::GETLIST_ASSOC,
                                       {'isInvoiceable' => 1,
                                        'contractId'    => {
                                            'values'      => [$tabContractInfos->{'id'}],
                                            'isChargedTo' => 1
                                        }
                                       });

            $tabDays = {
                TYPE_ADDITIONAL() => [],
                TYPE_DEDUCTED()   => []
            };
            foreach my $dayInfos (values(%$tabDaysList))
            {
                push(@{$tabDays->{$dayInfos->{'type'}}}, {
                    'id'   => $dayInfos->{'id'},
                    'date' => $dayInfos->{'date'},
                });
            }
        }

        return {
            'id'                      => $tabContractInfos->{'id'},
            'code'                    => $tabContractInfos->{'code'},
            'authorizedPeriodAddDays' => &getAuthorizedPeriod($tabContractInfos->{'agency.id'}, TYPE_ADDITIONAL, 'contract', [$tabContractInfos->{'beginDate'}, $tabContractInfos->{'endDate'}], $tabContractInfos->{'valueDate'}),
            'authorizedPeriodDedDays' => &getAuthorizedPeriod($tabContractInfos->{'agency.id'}, TYPE_DEDUCTED, 'contract', [$tabContractInfos->{'beginDate'}, $tabContractInfos->{'endDate'}], $tabContractInfos->{'valueDate'}),
            'tabDeltasPeriodAddDays'  => &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('INTJRSPCT', $tabContractInfos->{'agency.id'}, $tabContractInfos->{'valueDate'})),
            'tabDeltasPeriodDedDays'  => &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('INTJRSMCT', $tabContractInfos->{'agency.id'}, $tabContractInfos->{'valueDate'})),
            'tabDays'                 => $tabDays
        };

    }
    return 0;
}


# Function: insert
# Insère un nouveau jour+/-
#
# Parameters:
# string  $countryId  - Identifiant du pays
# string  $entityType - Entité sur laquelle insérer le jour
# int     $entityId   - Identifiant de l'entité
# hashref $tabData    - Tableau des données
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# int - Identifiant du jour+/- créé
sub insert
{
    my ($countryId, $entityType, $entityId, $tabData, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'insert', $_[0], __FILE__, '');
        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }
        return 0;
    };

    # Vérification des données
    my $appliedToContractId = (exists $tabData->{'appliedToContractId'} && $tabData->{'appliedToContractId'} ? $tabData->{'appliedToContractId'} : undef);
    # - Type de jour
    my $type = (defined $tabData->{'type'} ? $tabData->{'type'} : undef);
    if (!defined $type)
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_TYPEREQUIRED);
    }
    # - Date si contrat ou contrat lié
    my $date = (exists $tabData->{'date'} && $tabData->{'date'} ? $tabData->{'date'} : undef);
    if (!defined $date && ($entityType eq 'contract' || $appliedToContractId))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_DATEREQUIRED);
    }
    # - Motif
    my $reasonId = (defined $tabData->{'reasonId'} ? $tabData->{'reasonId'} : undef);
    if (!defined $reasonId)
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_REASONREQUIRED);
    }
    # - Contrat lié
    if ($appliedToContractId)
    {
        my $agencyId;
        my $valueDate;
        if ($entityType eq 'contract')
        {
            $agencyId  = &LOC::Contract::Rent::getAgencyId($countryId, $entityId);
            $valueDate = &LOC::Contract::Rent::getValueDate($countryId, $entityId);
        }
        elsif ($entityType eq 'estimateLine')
        {
            $agencyId  = &LOC::Estimate::Rent::getLineAgencyId($countryId, $entityId);
            $valueDate = &LOC::Estimate::Rent::getLineValueDate($countryId, $entityId);
        }
        else
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
        my $delayMinDateEnd = &LOC::Characteristic::getAgencyValueByCode('DELTAMAXCTTLIE', $agencyId, $valueDate) * 1;

        my $tabFilters = {
            'id'       => $appliedToContractId,
            'agencyId' => $agencyId,
            'isClosed' => 1,
            'endDate'  => [&LOC::Date::subDays($valueDate, $delayMinDateEnd), undef]
        };

        my $result = &LOC::Contract::Rent::getList($countryId, LOC::Util::GETLIST_COUNT, $tabFilters);
        if (!$result)
        {
            push(@{$tabErrors->{'fatal'}}, ERROR_INVALIDAPPLIEDTOCONTRACT);
        }
    }
    # - Cohérence de date
    &_isDatePossible($countryId, $type, $date, $entityType, $entityId, $appliedToContractId, {}, $tabErrors);

    # Erreurs ?
    if (@{$tabErrors->{'fatal'}} > 0)
    {
        $tabErrors->{'details'} = {
            'type'                => $type,
            'date'                => $date,
            'appliedToContractId' => $appliedToContractId
        };
        return 0;
    }

    # Insertion du jour en base de données
    my $tabUpdates = {
        'day_type'             => $type,
        'day_date'             => $date,
        'day_rct_id_appliedto' => $appliedToContractId,
        'day_did_id'           => $tabData->{'invoiceDurationId'},
        'day_dre_id'           => $reasonId,
        'day_comment'          => $tabData->{'comment'},
        'day_is_invoiceable'   => $tabData->{'isInvoiceable'},
        'day_usr_id_creator'   => $userId,
        'day_date_creation*'   => 'NOW()',
        'day_sta_id'           => STATE_ACTIVE
    };

    # - Calcul de l'agence
    $tabUpdates->{'day_agc_id'} = ($appliedToContractId ? &LOC::Contract::Rent::getAgencyId($countryId, $appliedToContractId) :
                                        ($entityType eq 'contract' ? &LOC::Contract::Rent::getAgencyId($countryId, $entityId) :
                                            ($entityType eq 'estimateLine' ? &LOC::Estimate::Rent::getLineAgencyId($countryId, $entityId) : undef)));

    if ($entityType eq 'contract')
    {
        $tabUpdates->{'day_rct_id'} = $entityId;
    }
    elsif ($entityType eq 'estimateLine')
    {
        $tabUpdates->{'day_rel_id'} = $entityId;
    }
    else
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    my $dayId = $db->insert('f_day', $tabUpdates, $schemaName);
    if ($dayId == -1)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }
    $dayId *= 1;

    # Historisation
    # - Infos nécessaires
    my $tabInvoiceDurationInfos = &LOC::Day::InvoiceDuration::getInfos($countryId, $tabData->{'invoiceDurationId'});
    my $tabReasonInfos = &LOC::Day::Reason::getInfos($countryId, $reasonId);
    my $tabCountryInfos = &LOC::Country::getInfos($countryId);
    my $locale = &LOC::Locale::getLocale($tabCountryInfos->{'locale.id'}, undef, [POSIX::LC_MESSAGES, POSIX::LC_CTYPE]);
    if ($date)
    {
        $date = $locale->getDateFormat($date, LOC::Locale::FORMAT_DATE_NUMERIC);
    }

    my $schemaName = 'LOCATION' . ($countryId eq 'FR' ? '' : $countryId);
    my $content = $date;
    if ($appliedToContractId)
    {
        $content .= ' (contrat ' . $appliedToContractId . ')';
    }

    my $tabContentExtra = [
        {
            'prop'  => 'date',
            'value' => $date
        },
        {
            'prop'  => 'duration',
            'value' => $tabInvoiceDurationInfos->{'label'}
        },
        {
            'prop'  => 'reason',
            'value' => $tabReasonInfos->{'label'}
        },
        {
            'prop'  => 'appliedToContract',
            'value' => $appliedToContractId
        },
        {
            'prop'  => 'comment',
            'value' => $tabData->{'comment'}
        },
        {
            'prop'  => 'isInvoiceable',
            'value' => $tabData->{'isInvoiceable'} ? $locale->t('Oui') : $locale->t('Non')
        }
    ];
    
    my $newValues = {
        'type'                => $type,
        'date'                => $date,
        'appliedToContractId' => $appliedToContractId,
        'invoiceDurationId'   => $tabData->{'invoiceDurationId'},
        'reasonId'            => $reasonId,
        'comment'             => $tabData->{'comment'},
        'isInvoiceable'       => $tabData->{'isInvoiceable'}
    };

    my $tabTrace = {
        'schema'       => $schemaName,
        'code'         => ($type eq TYPE_ADDITIONAL ? LOC::Log::EventType::ADDADDEDDAYS : LOC::Log::EventType::ADDDEDUCTEDDAYS),
        'content'      => $content,
        'contentExtra' => $tabContentExtra,
        'new'          => $newValues
    };
    &LOC::EndUpdates::addTrace($tabEndUpds, $entityType, $entityId, $tabTrace);

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return $dayId;
}

# Function: update
# Met à jour un jour
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Identifiant du jour
# hashref $tabData   - Données à mettre à jour
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Return: bool
sub update
{
    my ($countryId, $id, $tabData, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'update', $_[0], __FILE__, '');
        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }
        return 0;
    };

    # Récupération des informations du document
    my $tabInfos = &getInfos($countryId, $id);
    if (!$tabInfos)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Vérification des données
    my $oldDate = $tabInfos->{'date'};
    my $newDate = (exists $tabData->{'date'} && $tabData->{'date'} ? $tabData->{'date'} : undef);

    my $oldInvoiceDurationId = $tabInfos->{'invoiceDuration.id'};
    my $newInvoiceDurationId = (exists $tabData->{'invoiceDurationId'} ? $tabData->{'invoiceDurationId'} : undef);

    my $oldReasonId = $tabInfos->{'reason.id'};
    my $newReasonId = (exists $tabData->{'reasonId'} ? $tabData->{'reasonId'} : 0);

    my $oldAppliedToContractId = $tabInfos->{'appliedToContract.id'};
    my $newAppliedToContractId = (exists $tabData->{'appliedToContractId'} && $tabData->{'appliedToContractId'} ? $tabData->{'appliedToContractId'} : undef);

    my $oldComment = $tabInfos->{'comment'};
    my $newComment = (exists $tabData->{'comment'} ? &LOC::Util::trim($tabData->{'comment'}) : undef);

    # - Date si contrat
    if (!defined $newDate && ($tabInfos->{'contract.id'} || $newAppliedToContractId))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_DATEREQUIRED);
    }
    # - Motif
    if (!$newReasonId)
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_REASONREQUIRED);
    }
    # - Contrat lié
    if ($newAppliedToContractId)
    {
        my $agencyId  = ($tabInfos->{'contract.id'} ? &LOC::Contract::Rent::getAgencyId($countryId, $tabInfos->{'contract.id'}) :
                                                     &LOC::Estimate::Rent::getLineAgencyId($countryId, $tabInfos->{'estimateLine.id'}));
        my $valueDate = ($tabInfos->{'contract.id'} ? &LOC::Contract::Rent::getValueDate($countryId, $tabInfos->{'contract.id'}) :
                                                     &LOC::Estimate::Rent::getLineValueDate($countryId, $tabInfos->{'estimateLine.id'}));
        my $delayMinDateEnd = &LOC::Characteristic::getAgencyValueByCode('DELTAMAXCTTLIE', $agencyId, $valueDate) * 1;

        my $tabFilters = {
            'id'       => $newAppliedToContractId,
            'agencyId' => $agencyId,
            'isClosed' => 1,
            'endDate'  => [&LOC::Date::subDays($valueDate, $delayMinDateEnd), undef]
        };

        my $result = &LOC::Contract::Rent::getList($countryId, LOC::Util::GETLIST_COUNT, $tabFilters);
        if (!$result)
        {
            push(@{$tabErrors->{'fatal'}}, ERROR_INVALIDAPPLIEDTOCONTRACT);
        }
    }

    # - Cohérence de date
    if ($tabInfos->{'isInvoiceable'} && $newDate && $oldDate ne $newDate)
    {
        my $entityType = ($tabInfos->{'contract.id'} ? 'contract' : 'estimateLine');
        my $entityId   = ($tabInfos->{'contract.id'} ? $tabInfos->{'contract.id'} : $tabInfos->{'estimateLine.id'});
        &_isDatePossible($countryId, $tabInfos->{'type'}, $newDate, $entityType, $entityId, $newAppliedToContractId, {'excludedIds' => [$id]}, $tabErrors);
    }

    # Erreurs ?
    if (@{$tabErrors->{'fatal'}} > 0)
    {
        $tabErrors->{'details'} = {
            'type' => $tabInfos->{'type'},
            'date' => $newDate,
            'appliedToContractId' => $newAppliedToContractId
        };
        return 0;
    }

    # Mises à jour
    my $tabUpdates = {};
    my $tabTraces  = {'old' => {}, 'new' => {}};
    my @tabChangeLabels = ();

    if ($oldDate ne $newDate)
    {
        $tabUpdates->{'day_date'} = $newDate;
        $tabTraces->{'old'}->{'date'} = $oldDate;
        $tabTraces->{'new'}->{'date'} = $newDate;
        push(@tabChangeLabels, 'date');
    }
    if ($oldAppliedToContractId != $newAppliedToContractId)
    {
        $tabUpdates->{'day_rct_id_appliedto'} = $newAppliedToContractId;
        # - Calcul de l'agence
        my $oldAgencyId = $tabInfos->{'agency.id'};
        my $newAgencyId = ($newAppliedToContractId ? &LOC::Contract::Rent::getAgencyId($countryId, $newAppliedToContractId) :
                                        (defined $tabInfos->{'contract.id'} ? &LOC::Contract::Rent::getAgencyId($countryId, $tabInfos->{'contract.id'}) :
                                            (defined $tabInfos->{'estimateLine.id'} ? &LOC::Estimate::Rent::getLineAgencyId($countryId, $tabInfos->{'estimateLine.id'}) : undef)));

        if ($oldAgencyId ne $newAgencyId)
        {
            $tabUpdates->{'day_agc_id'} = $newAgencyId;
        }
        $tabTraces->{'old'}->{'appliedToContractId'} = $oldAppliedToContractId;
        $tabTraces->{'new'}->{'appliedToContractId'} = $newAppliedToContractId;
        push(@tabChangeLabels, 'contrat lié');
    }
    if ($oldInvoiceDurationId != $newInvoiceDurationId)
    {
        $tabUpdates->{'day_did_id'} = $newInvoiceDurationId;
        $tabTraces->{'old'}->{'invoiceDurationId'} = $oldInvoiceDurationId;
        $tabTraces->{'new'}->{'invoiceDurationId'} = $newInvoiceDurationId;
        push(@tabChangeLabels, 'durée');
    }
    if ($oldReasonId != $newReasonId)
    {
        $tabUpdates->{'day_dre_id'} = $newReasonId;
        $tabTraces->{'old'}->{'reasonId'} = $oldReasonId;
        $tabTraces->{'new'}->{'reasonId'} = $newReasonId;
        push(@tabChangeLabels, 'motif');
    }
    if ($oldComment ne $newComment)
    {
        $tabUpdates->{'day_comment'} = $newComment;
        $tabTraces->{'old'}->{'comment'} = $oldComment;
        $tabTraces->{'new'}->{'comment'} = $newComment;
        push(@tabChangeLabels, 'commentaire');
    }

    if (keys %$tabUpdates > 0)
    {
        if (!$db->update('f_day', $tabUpdates, {'day_id' => $id}, $schemaName))
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }

        # Historisation
        # - Infos nécessaires
        my $tabInvoiceDurationInfos = &LOC::Day::InvoiceDuration::getInfos($countryId, $newInvoiceDurationId);
        my $tabReasonInfos = &LOC::Day::Reason::getInfos($countryId, $newReasonId);
        my $tabCountryInfos = &LOC::Country::getInfos($countryId);
        my $locale = &LOC::Locale::getLocale($tabCountryInfos->{'locale.id'}, undef, [POSIX::LC_MESSAGES, POSIX::LC_CTYPE]);
        if ($newDate)
        {
            $newDate = $locale->getDateFormat($newDate, LOC::Locale::FORMAT_DATE_NUMERIC);
        }

        my $schemaName = 'LOCATION' . ($countryId eq 'FR' ? '' : $countryId);
        my $content = $newDate;
        if ($newAppliedToContractId)
        {
            $content .= ' (contrat ' . $newAppliedToContractId . ')';
        }
        my $entityType = ($tabInfos->{'contract.id'} ? 'contract' : 'estimateLine');
        my $entityId   = ($tabInfos->{'contract.id'} ? $tabInfos->{'contract.id'} : $tabInfos->{'estimateLine.id'});


        my $tabContentExtra = [
            {
                'prop'  => 'date',
                'value' => $newDate
            },
            {
                'prop'  => 'duration',
                'value' => $tabInvoiceDurationInfos->{'label'}
            },
            {
                'prop'  => 'reason',
                'value' => $tabReasonInfos->{'label'}
            },
            {
                'prop'  => 'appliedToContract',
                'value' => $newAppliedToContractId
            },
            {
                'prop'  => 'comment',
                'value' => $newComment
            }
        ];

        my $tabTrace = {
            'schema'  => $schemaName,
            'code'    => ($tabInfos->{'type'} eq TYPE_ADDITIONAL ? LOC::Log::EventType::UPDADDEDDAY : LOC::Log::EventType::UPDDEDUCTEDDAY),
            'old'     => $tabTraces->{'old'},
            'new'     => $tabTraces->{'new'},
            'content' => $content . ' (' . join(', ', @tabChangeLabels) . ')',
            'contentExtra' => $tabContentExtra
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, $entityType, $entityId, $tabTrace);
    }
    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: delete
# Supprime un jour
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Identifiant du jour
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Return: bool
sub delete
{
    my ($countryId, $id, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'delete', $_[0], __FILE__, '');
        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }
        return 0;
    };

    # Récupération des informations du document
    my $tabInfos = &getInfos($countryId, $id);
    if (!$tabInfos)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Mise à jour
    my $tabUpdates = {
        'day_sta_id' => STATE_DELETED
    };

    if (!$db->update('f_day', $tabUpdates, {'day_id' => $id}, $schemaName))
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Historisation
    my $schemaName = 'LOCATION' . ($countryId eq 'FR' ? '' : $countryId);
    my $content = $tabInfos->{'date'};
    if ($tabInfos->{'appliedToContract.id'})
    {
        $content .= ' (contrat ' . $tabInfos->{'appliedToContract.id'} . ')';
    }
    my $entityType = ($tabInfos->{'contract.id'} ? 'contract' : 'estimateLine');
    my $entityId   = ($tabInfos->{'contract.id'} ? $tabInfos->{'contract.id'} : $tabInfos->{'estimateLine.id'});

    my $tabTrace = {
        'schema' => $schemaName,
        'code'   => ($tabInfos->{'type'} eq TYPE_ADDITIONAL ? LOC::Log::EventType::DELADDEDDAY : LOC::Log::EventType::DELDEDUCTEDDAY),
        'content' => $content
    };
    &LOC::EndUpdates::addTrace($tabEndUpds, $entityType, $entityId, $tabTrace);


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}

# Function: deleteAll
# Supprime tous les jours d'une entité définitivement
#
# Parameters:
# string  $countryId  - Identifiant du pays
# string  $entityType - Entité sur laquelle supprimer les jour
# int     $entityId   - Identifiant de l'entité
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors  - Tableau d'erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool
sub deleteAll
{
    my ($countryId, $entityType, $entityId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

        if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'deleteAll', $_[0], __FILE__, '');
        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }
        return 0;
    };

    my $tabFilters = {
        'stateId' => [STATE_ACTIVE, STATE_DELETED]
    };

    if ($entityType eq 'estimateLine')
    {
        $tabFilters->{'estimateLineId'} = $entityId;
    }
    elsif ($entityType eq 'contract')
    {
        $tabFilters->{'contractId'} = $entityId;
    }
    else
    {
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    my $tabIds = &getList($countryId, LOC::Util::GETLIST_IDS, $tabFilters);
    if (@$tabIds > 0)
    {
        # Suppression du jour
        if ($db->delete('f_day', {'day_id' => $tabIds}, $schemaName) == -1)
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}



# Function: setInvoiceable
# Définir l'état de facturabilité d'un jour
#
# Parameters:
# string  $countryId      - Identifiant du pays
# int     $id             - Identifiant du jour
# hashref $isInvoiceable  - État facturable du jour
# int     $userId         - Identifiant de l'utilisateur responsable
# hashref $tabOptions     - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors      - Tableau d'erreurs
# hashref $tabEndUpds     - Mises à jour de fin
#
# Return: bool
sub setInvoiceable
{
    my ($countryId, $id, $isInvoiceable, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'setInvoiceable', $_[0], __FILE__, '');
        if (defined $_[1])
        {
            push(@{$tabErrors->{'fatal'}}, $_[1]);
        }
        return 0;
    };

    # Récupération des informations du document
    my $tabInfos = &getInfos($countryId, $id);
    if (!$tabInfos)
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Vérifications
    if ($tabInfos->{'isInvoiceable'} == $isInvoiceable)
    {
        if ($isInvoiceable)
        {
            push(@{$tabErrors->{'fatal'}}, ERROR_ALREADYINVOICEABLE);
        }
        else
        {
            push(@{$tabErrors->{'fatal'}}, ERROR_ALREADYNOTINVOICEABLE);
        }
    }
    # - Cohérence de la date dans le cas d'une "facturabilité"
    if ($isInvoiceable && $tabInfos->{'date'})
    {
        my $entityType = ($tabInfos->{'contract.id'} ? 'contract' : 'estimateLine');
        my $entityId   = ($tabInfos->{'contract.id'} ? $tabInfos->{'contract.id'} : $tabInfos->{'estimateLine.id'});
        &_isDatePossible($countryId, $tabInfos->{'type'}, $tabInfos->{'date'}, $entityType, $entityId, $tabInfos->{'appliedToContract.id'}, {'excludedIds' => [$id]}, $tabErrors);
    }

    # Erreurs ?
    if (@{$tabErrors->{'fatal'}} > 0)
    {
        $tabErrors->{'details'} = {
            'type' => $tabInfos->{'type'},
            'date' => $tabInfos->{'date'},
            'appliedToContractId' => $tabInfos->{'appliedToContract.id'}
        };
        return 0;
    }

    # Mise à jour
    my $tabUpdates = {
        'day_is_invoiceable' => $isInvoiceable
    };

    if (!$db->update('f_day', $tabUpdates, {'day_id' => $id}, $schemaName))
    {
        # Erreur fatale !
        return &$rollBackAndError(__LINE__, ERROR_FATAL);
    }

    # Historisation
    my $schemaName = 'LOCATION' . ($countryId eq 'FR' ? '' : $countryId);
    my $content = $tabInfos->{'date'};
    if ($tabInfos->{'appliedToContract.id'})
    {
        $content .= ' (contrat ' . $tabInfos->{'appliedToContract.id'} . ')';
    }
    my $entityType = ($tabInfos->{'contract.id'} ? 'contract' : 'estimateLine');
    my $entityId   = ($tabInfos->{'contract.id'} ? $tabInfos->{'contract.id'} : $tabInfos->{'estimateLine.id'});

    my $code = '';
    if ($tabInfos->{'type'} eq TYPE_ADDITIONAL)
    {
        $code = ($isInvoiceable ? LOC::Log::EventType::INVOICEADDEDDAY : LOC::Log::EventType::UNINVOICEADDEDDAY);
    }
    else
    {
        $code = ($isInvoiceable ? LOC::Log::EventType::INVOICEDEDUCTEDDAY : LOC::Log::EventType::UNINVOICEDEDUCTEDDAY);
    }

    my $tabTrace = {
        'schema'  => $schemaName,
        'code'    => $code,
        'content' => $content
    };
    &LOC::EndUpdates::addTrace($tabEndUpds, $entityType, $entityId, $tabTrace);

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            # Erreur fatale !
            return &$rollBackAndError(__LINE__, ERROR_FATAL);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}


# Function: getPeriodLimitDates
# Récupère les dates de début max et date de fin min autorisées sur un document en fonction du jour
#
# Parameters:
# string   $agencyId     - Identifiant de l'agence
# string   $type         - Type de jour
# string   $documentType - Type de document (contrat/devis)
# arrayref $tabPeriod    - Période du contrat/devis
# string   $valueDate    - Date de valeur du document
sub getPeriodLimitDates
{
    my ($agencyId, $type, $documentType, $date, $valueDate) = @_;

    my $code = ($type eq  TYPE_ADDITIONAL ? 'INTJRSP' : 'INTJRSM');
    $code   .= ($documentType eq 'contract' ? 'CT' : 'DV');

    my $tabDeltaDays = &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode($code, $agencyId, $valueDate));

    return {
        'beginDateMax' => &LOC::Date::addWorkingDays($date, -$tabDeltaDays->[0], $agencyId),
        'endDateMin'   => &LOC::Date::addWorkingDays($date, -$tabDeltaDays->[1], $agencyId)
    };
}

# Function: getAuthorizedPeriod
# Récupère la période autorisée pour la saisie de J+/- sur le contrat ou devis
#
# Parameters:
# string   $agencyId     - Identifiant de l'agence
# string   $type         - Type de jour
# string   $documentType - Type de document (contrat/devis)
# arrayref $tabPeriod    - Période du contrat/devis
# string   $valueDate    - Date de valeur du document
sub getAuthorizedPeriod
{
    my ($agencyId, $type, $documentType, $tabPeriod, $valueDate) = @_;

    my $code = ($type eq  TYPE_ADDITIONAL ? 'INTJRSP' : 'INTJRSM');
    $code   .= ($documentType eq 'contract' ? 'CT' : 'DV');

    my $deltaDays = &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode($code, $agencyId, $valueDate));

    return [
        &LOC::Date::addWorkingDays($tabPeriod->[0], $deltaDays->[0], $agencyId),
        &LOC::Date::addWorkingDays($tabPeriod->[1], $deltaDays->[1], $agencyId)
    ];
}


# Function: _isDatePossible
# Indique si la date saisie est possible (par rapport à la période de l'entité et à ses jours déjà présents)
#
# Parameters:
# string  $countryId           - Identifiant du pays
# string  $type                - Type de jour
# string  $date                - Date à vérifier
# string  $entityType          - Entité sur laquelle vérifier
# int     $entityId            - Identifiant de l'entité
# int     $appliedToContractId - Identifiant du contrat lié
# hashref $tabOptions          - Tableau d'options
# hashref $tabErrors           - Tableau d'erreurs
sub _isDatePossible
{
    my ($countryId, $type, $date, $entityType, $entityId, $appliedToContractId, $tabOptions, $tabErrors) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {
            'fatal' => []
        };
    }
    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }

    # Pas de vérification si pas de date
    if (!$date)
    {
        return 1;
    }

    # -- Jours dans la période autorisée
    my $documentType = $entityType;
    my $tabEntityInfos;
    my $agencyId;
    my $valueDate;
    if (!$appliedToContractId)
    {
        if ($entityType eq 'contract')
        {
            $tabEntityInfos = &LOC::Contract::Rent::getInfos($countryId, $entityId,
                                                             LOC::Contract::Rent::GETINFOS_BASE |
                                                             LOC::Contract::Rent::GETINFOS_VALUEDATE);
            $agencyId = $tabEntityInfos->{'agency.id'};
            $valueDate = $tabEntityInfos->{'valueDate'};
        }
        elsif ($entityType eq 'estimateLine')
        {
            $tabEntityInfos = &LOC::Estimate::Rent::getLineInfos($countryId, $entityId,
                                                                 LOC::Estimate::Rent::GETLINEINFOS_BASE |
                                                                 LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE_VALUEDATE);
            $agencyId = $tabEntityInfos->{'estimate.agency.id'};
            $valueDate = $tabEntityInfos->{'estimate.valueDate'};
        }
    }
    else
    {
        $documentType = 'contract';
        $tabEntityInfos = &LOC::Contract::Rent::getInfos($countryId, $appliedToContractId,
                                                         LOC::Contract::Rent::GETINFOS_BASE |
                                                         LOC::Contract::Rent::GETINFOS_VALUEDATE);
        $agencyId = $tabEntityInfos->{'agency.id'};
        $valueDate = $tabEntityInfos->{'valueDate'};
    }
    my $authorizedPeriod = &getAuthorizedPeriod($agencyId, $type, $documentType, [$tabEntityInfos->{'beginDate'}, $tabEntityInfos->{'endDate'}], $valueDate);

    if ($date lt $authorizedPeriod->[0] || $date gt $authorizedPeriod->[1])
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_OUTOFPERIOD);
        return 0;
    }

    # -- Jours déjà existants
    my $tabFilters = {
        'type'                => $type,
        'isInvoiceable'       => 1,
        'date'                => $date
    };
    if (exists $tabOptions->{'excludeIds'})
    {
        $tabFilters->{'!id'} = $tabOptions->{'excludeIds'};
    }
    # -- Récupération des dates facturables existantes sur le document
    if (!$appliedToContractId)
    {
        if ($entityType eq 'contract')
        {
            $tabFilters->{'contractId'} = {
                'values'      => $entityId,
                'isChargedTo' => 1
            };
        }
        elsif ($entityType eq 'estimateLine')
        {
            $tabFilters->{'estimateLineId'} = {
                'values'      => $entityId,
                'isChargedTo' => 1
            };
        }
    }
    else
    {
        $tabFilters->{'contractId'} = {
            'values'      => $appliedToContractId,
            'isChargedTo' => 1
        };
    }
    my $nbExistingDays = &getList($countryId, LOC::Util::GETLIST_COUNT, $tabFilters);
    if ($nbExistingDays > 0)
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_EXISTINGDAY);
        return 0;
    }

    return 1;
}


1;