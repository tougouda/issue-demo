use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::TransportRequest
# Module permettant d'obtenir les informations sur les requêtes de mise à jour de transport
package LOC::TransportRequest;


# Constants: États
# STATE_CREATED   - Créée
# STATE_PROGRESS  - En cours
# STATE_SUCCESS   - Réussie
# STATE_FAILED    - Echouée
use constant
{
    STATE_CREATED  => 'REQ01',
    STATE_PROGRESS => 'REQ02',
    STATE_SUCCESS  => 'REQ03',
    STATE_FAILED   => 'REQ04',
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Json;
use LOC::Util;


# Function: getInfos
# Retourne les infos d'une requête de mise à jour de transport
# 
# Contenu du hashage :
# - id : identifiant de la requête de mise à jour de transport ;
# - pid : numéro du processus Unix associé à la requête ;
# - user.id : identifiant de l'utilisateur qui a lancé la requête ;
# - parameters : paramètres de la requête ;
# - startDate : date de début ;
# - endDate : date de fin ;
# - treatedTransportsNo : nombre de transports traités
# - totalTransportsNo : nombre total de transport à traiter
# - progression : pourcentage de progression ;
# - updatedTransportNo : nombre de transport total mis à jour;
# - creationDate : date de création ;
# - errors : erreurs ;
# - state.id : identifiant de l'état.
#
#
# Parameters:
# string $id        - Identifiant de la requête de mise à jour de transport
# string $countryId - Identifiant du pays
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getInfos
{
    my ($countryId, $id) = @_;

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id});
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Retourne la liste des requêtes de mise à jour de transport
#
# Parameters:
# int     $format     - Format de retour de la liste
# string  $countryId  - Identifiant du pays
# hashref $tabFilters - Filtres
#
# Returns:
# hash|hashref|undef - Liste des requêtes de mise à jour de transport
sub getList
{
    my ($countryId, $format, $tabFilters) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query;
    if ($format == LOC::Util::GETLIST_PAIRS)
    {
        $query = '
SELECT
    trq_id AS `id`,
    trq_pid AS `pid`
FROM f_transportrequest';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*) AS `id`
FROM f_transportrequest';
    }
    else
    {
        $query = '
SELECT
    trq_id AS `id`,
    trq_pid AS `pid`,
    trq_usr_id AS `user.id`,
    trq_parameters AS `parameters`,
    trq_date_start AS `startDate`,
    trq_date_end AS `endDate`,
    trq_treatedtransports AS `treatedTransportsNo`,
    trq_totaltransports AS `totalTransportsNo`,
    trq_progression AS `progression`,
    trq_updatedtransports AS `updatedTransportsNo`,
    trq_date_create AS `creationDate`,
    trq_stderr AS `errors`,
    trq_result AS `tabResult`,
    trq_sta_id AS `state.id`
FROM f_transportrequest';
    }
    $query .= '
WHERE TRUE';

    # Filtres
    # - suivant un identifiant de requête
    if ($tabFilters->{'id'} ne '')
    {
        $query .= '
AND trq_id = ' . $db->quote($tabFilters->{'id'});
    }
    if ($tabFilters->{'stateId'} ne '')
    {
        $query .= '
AND trq_sta_id IN (' . $db->quote($tabFilters->{'stateId'}) . ')';
    }
    # - suivant les paramètres
    if ($tabFilters->{'parameters'} ne '')
    {
        if (ref($tabFilters->{'parameters'}) eq 'HASH')
        {
            foreach my $key (keys(%{$tabFilters->{'parameters'}}))
            {
                my $search = &LOC::Json::toJson({$key => $tabFilters->{'parameters'}->{$key}});
                $search = substr(substr($search, 0, -1), 1);
                $query .= '
AND trq_parameters LIKE ' . $db->quote('%' . $search . '%');
            }
        }
        else
        {
            $query .= '
AND trq_parameters = ' . $db->quote($tabFilters->{'parameters'});
        }
    }
    $query .= '
ORDER BY `id`;';

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    # Transformation du JSON en hashref
    foreach my $tabLine (values(%$tab))
    {
        if ($tabLine->{'tabResult'})
        {
            $tabLine->{'tabResult'} = &LOC::Json::fromJson($tabLine->{'tabResult'});
        }
    }

    return (wantarray ? %$tab : $tab);
}

# Function: insert
# Crée en base de données la requête de mise à jour de transport
#
# Contenu du de la table de hashage à passer en paramètre :
# - pid : numéro du processus Unix ;
# - userId : identifiant de l'utilisateur qui a lancé la requête ;
# - parameters : paramètres de la requête ;
# - progression : pourcentage de progression ;
# - treatedTransportsNo : nombre de transports mis à jour ;
# - errors : erreurs ;
# - stateId : état ;
#
#
# Parameters:
# string   $countryId - Identifiant du pays
# hashref  $tabParams - Paramètres pour la requête
#
# Returns:
# bool - Retourne 1 si l'insertion est correcte, 0 sinon
sub insert
{
    # Récupération des paramètres
    my ($countryId, $tabParams) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    unless (defined $tabParams->{'stateId'})
    {
        $tabParams->{'stateId'} = STATE_CREATED;
    }

    if (ref($tabParams->{'parameters'}) eq 'HASH')
    {
        $tabParams->{'parameters'} = &LOC::Json::toJson($tabParams->{'parameters'});
    }

    # Ligne dans la table f_transportrequest
    my $tabUpdates = {
        'trq_pid'             => $tabParams->{'pid'},
        'trq_usr_id'          => $tabParams->{'userId'},
        'trq_parameters'      => $tabParams->{'parameters'},
        'trq_treatedtransports' => $tabParams->{'treatedTransportsNo'},
        'trq_totaltransports'   => $tabParams->{'totalTransportsNo'},
        'trq_progression'     => &LOC::Util::toNumber($tabParams->{'progression'}),
        'trq_date_create*'    => 'NOW()',
        'trq_stderr'          => $tabParams->{'errors'},
        'trq_sta_id'          => $tabParams->{'stateId'},
    };

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # insertion de la ligne dans la table f_transportrequest
    my $requestId = $db->insert('f_transportrequest', $tabUpdates, $schemaName);
    if ($requestId == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return $requestId * 1;
}

# Function: setStartDate
# Met à jour la date de début
#
# Parameters:
# string $countryId   - Identifiant du pays
# int    $id          - Id de la requête
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setStartDate
{
    # Récupération des paramètres
    my ($countryId, $id) = @_;

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_transportrequest
    if ($db->update('f_transportrequest', {'trq_date_start*' => 'NOW()'}, {'trq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

# Function: setEndDate
# Met à jour la date de fin
#
# Parameters:
# string $countryId   - Identifiant du pays
# int    $id          - Id de la requête
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setEndDate
{
    # Récupération des paramètres
    my ($countryId, $id) = @_;

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_transportrequest
    if ($db->update('f_transportrequest', {'trq_date_end*' => 'NOW()'}, {'trq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

# Function: setPid
# Met à jour le PID
#
# Parameters:
# string $countryId - Identifiant du pays
# int    $id        - Id de la requête
# int    $pid       - PID
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setPid
{
    # Récupération des paramètres
    my ($countryId, $id, $pid) = @_;

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_transportrequest
    if ($db->update('f_transportrequest', {'trq_pid' => $pid}, {'trq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

# Function: setProgression
# Met à jour le pourcentage de progression
#
# Parameters:
# string $countryId   - Identifiant du pays
# int    $id          - Id de la requête
# int    $progression - Pourcentage de progression (compris entre 0 et 100)
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setProgression
{
    # Récupération des paramètres
    my ($countryId, $id, $progression) = @_;

    # Progression non valide
    if ($progression < 0 || $progression > 100)
    {
        return 0;
    }
    $progression = &LOC::Util::round($progression, 0);

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_transportrequest
    if ($db->update('f_transportrequest', {'trq_progression' => $progression}, {'trq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

# Function: setTreatedTransportsNo
# Met à jour le nombre de transports mis à jour
#
# Parameters:
# string $countryId         - Identifiant du pays
# int    $id                - Id de la requête
# int    $treatedTransportsNo - Nombre de transports mis à jour
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setTreatedTransportsNo
{
    # Récupération des paramètres
    my ($countryId, $id, $treatedTransportsNo) = @_;

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_transportrequest
    my $tabUpdates = {'trq_treatedtransports' => $treatedTransportsNo,
                      'trq_progression*'    => 'IFNULL(ROUND(trq_treatedtransports / trq_totaltransports * 100), 0)'
                     };
    if ($db->update('f_transportrequest', $tabUpdates, {'trq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

# Function: setUpdatedTransportsNo
# Met à jour le nombre de total de transports traités
#
# Parameters:
# string $countryId           - Identifiant du pays
# int    $id                  - Id de la requête
# int    $updatedTransportsNo - Nombre de transports mis à jour
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setUpdatedTransportsNo
{
    # Récupération des paramètres
    my ($countryId, $id, $updatedTransportsNo) = @_;

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_transportrequest
    my $tabUpdates = {'trq_updatedtransports' => $updatedTransportsNo};
    if ($db->update('f_transportrequest', $tabUpdates, {'trq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

# Function: setTotalTransportsNo
# Met à jour le nombre total de transports à traiter
#
# Parameters:
# string $countryId       - Identifiant du pays
# int    $id              - Id de la requête
# int    $totalTransportsNo - Nombre total de transports à traiter
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setTotalTransportsNo
{
    # Récupération des paramètres
    my ($countryId, $id, $totalTransportsNo) = @_;

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_transportrequest
    my $tabUpdates = {'trq_totaltransports' => $totalTransportsNo,
                      'trq_progression*'  => 'IFNULL(ROUND(trq_treatedtransports / trq_totaltransports * 100), 0)'
                     };
    if ($db->update('f_transportrequest', $tabUpdates, {'trq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

# Function: setState
# Met à jour l'état
#
# Parameters:
# string $countryId - Identifiant du pays
# int    $id        - Id de la requête
# string $stateId   - Identifiant de l'état
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setState
{
    # Récupération des paramètres
    my ($countryId, $id, $stateId) = @_;

    # État non valide
    if (!grep(/^$stateId$/, STATE_CREATED, STATE_PROGRESS, STATE_SUCCESS, STATE_FAILED))
    {
        return 0;
    }

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_transportrequest
    if ($db->update('f_transportrequest', {'trq_sta_id' => $stateId}, {'trq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

# Function: setErrors
# Met à jour les erreurs
#
# Parameters:
# string $countryId - Identifiant du pays
# int    $id        - Id de la requête
# string $errors    - Erreurs
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setErrors
{
    # Récupération des paramètres
    my ($countryId, $id, $errors) = @_;

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_transportrequest
    if ($db->update('f_transportrequest', {'trq_stderr' => $errors}, {'trq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

# Function: setResult
# Met à jour le tableau de résultat
#
# Parameters:
# string  $countryId - Identifiant du pays
# int     $id        - Id de la requête
# hashref $tabResult - Tableau de résultat
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub setResult
{
    # Récupération des paramètres
    my ($countryId, $id, $tabResult) = @_;

    unless (ref($tabResult) eq 'HASH')
    {
        return 0;
    }

    # Nom du schéma utilisé
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Transaction séparée forcée puisque les mises à jour sont faites en général en même temps qu'une autre transaction
    if ($db->do('START TRANSACTION') == -1)
    {
        return 0;
    }

    # Modification de la ligne dans la table f_transportrequest
    my $jsonResult = &LOC::Json::toJson($tabResult);
    if ($db->update('f_transportrequest', {'trq_result' => $jsonResult}, {'trq_id' => $id}, $schemaName) == -1)
    {
        $db->do('ROLLBACK');
        return 0;
    }

    if ($db->do('COMMIT') == -1)
    {
        return 0;
    }

    return 1;
}

1;
