use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Tariff::Rent
# Module de gestion des tarifs de location
package LOC::Tariff::Rent;


use constant {
    GETINFOS_ALL  => 0xFFFF,
    GETINFOS_BASE => 0x0000
};

# Constants: États des remises exceptionnelles
# REDUCTIONSTATE_VALIDATED - validée
# REDUCTIONSTATE_PENDING   - en attente
# REDUCTIONSTATE_REFUSED   - refusée
#
# Constants: Types des remises exceptionnelles
# REDUCTIONTYPE_NONE      - Inexistante
# REDUCTIONTYPE_PENDING   - En attente
# REDUCTIONTYPE_PROCESSED - Traitée
use constant {
    REDUCTIONSTATE_VALIDATED => 'REM01',
    REDUCTIONSTATE_PENDING   => 'REM02',
    REDUCTIONSTATE_REFUSED   => 'REM03',

    REDUCTIONTYPE_NONE      => 0,
    REDUCTIONTYPE_PENDING   => 1,
    REDUCTIONTYPE_PROCESSED => 2,

    DURATIONRANGESTATE_ACTIVE  => 'GEN01',
    DURATIONRANGESTATE_DELETED => 'GEN03'
};

# COnstants: Erreurs
# ERROR_FATAL                    - Erreur fatale
# ERROR_RIGHTS                   - Certaines informations ne sont pas modifiables
# ERROR_NOSPECIALREDUCTIONJUSTIF - La justification de la RE n'est pas renseignée
use constant {
    ERROR_FATAL   => 0x0001,
    ERROR_RIGHTS  => 0x0003,

    ERROR_NOSPECIALREDUCTIONJUSTIF => 0x0101
};


use strict;

use LOC::Date;
use LOC::Db;
use LOC::Util;
use LOC::Country;
use LOC::EndUpdates;

my $fatalErrMsg = 'Erreur fatale: module LOC::Tariff::Rent, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";


# Function: getBaseTariffs
# Récupérer la liste des tarifs de bases
#
# Parameters:
# string  $countryId  - Pays
# hashref $tabFilters - Liste des filtres
# int     $flags      - Flags
#
# Returns:
# hash|hashref -
sub getBaseTariffs
{
    my ($countryId, $tabFilters, $flags) = @_;
    if (!defined $flags)
    {
        $flags = 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query;
    $query .= '
SELECT
    tbl_p_basegrid.TARIFAUTO AS `id`,
    tbl_p_basegrid.FAMTARAUTO AS `tariffFamily.id`,
    tbl_p_basegrid.TRANCHEDUREEAUTO AS `durationRange.id`,
    tbl_p_basegrid.TARIFPRIX AS `amount`,
    tbl_p_basegrid.TARIFPRICING AS `pricingPrice`,
    tbl_p_basegrid.TARIFPRIXREVIENT AS `costPrice`,
    tdr_lower AS `minDays`,
    tdr_upper AS `maxDays`
FROM TARIF tbl_p_basegrid
INNER JOIN p_tariffdurationrange ON tdr_id = tbl_p_basegrid.TRANCHEDUREEAUTO
WHERE TRUE';
    if (defined $tabFilters->{'tariffFamily.id'})
    {
        $query .= '
AND tbl_p_basegrid.FAMTARAUTO = ' . $tabFilters->{'tariffFamily.id'};
    }
    $query .= '
ORDER BY `minDays`;';
    my $tab = $db->fetchAssoc($query);
    if (!$tab)
    {
        return undef;
    }

    # Formatage des données
    foreach my $tabRow (@$tab)
    {
        $tabRow->{'id'}               *= 1;
        $tabRow->{'durationRange.id'} *= 1;
        $tabRow->{'tariffFamily.id'}  *= 1;
        $tabRow->{'amount'}           *= 1;
        $tabRow->{'pricingPrice'}     *= 1;
        $tabRow->{'costPrice'}        *= 1;
        $tabRow->{'minDays'}          *= 1;

        # Les durées inférieures à 1 jour ont le même tarif que la tranche à 1 jour
        if ($tabRow->{'minDays'} <= 1)
        {
            $tabRow->{'minDays'} = 0;
        }
    }

    return (wantarray ? @$tab : $tab);
}


# Function: getCustomerTariffs
# Récupérer la liste des tarifs d'un client
#
# Parameters:
# string  $countryId  - Pays
# int     $customerId - Id du client
# hashref $tabFilters - Liste des filtres
# int     $flags      - Flags
#
# Returns:
# hash|hashref -
sub getCustomerTariffs
{
    my ($countryId, $customerId, $tabFilters, $flags) = @_;
    if (!defined $flags)
    {
        $flags = 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = 'GESTIONTARIF' . $countryId;

    my $query = '
SELECT
  tbl_p_basegrid.FAMTARAUTO AS `tariffFamily.id`,
  IFNULL(IFNULL(tbl_p_grid.REMISERECO,   tbl_p_groupgrid.REMISERECO),   tbl_p_basegrid.REMISERECO)   AS `recoReduction`,
  IFNULL(IFNULL(tbl_p_grid.REMISEMAX,    tbl_p_groupgrid.REMISEMAX),    tbl_p_basegrid.REMISEMAX)    AS `maxiReduction`,
  IFNULL(IFNULL(tbl_p_grid.GRNBJOURSMIN, tbl_p_groupgrid.GRNBJOURSMIN), tbl_p_basegrid.GRNBJOURSMIN) AS `minDays`,
  tbl_p_tariff.TARIFPRIX AS `baseAmount`,
  IFNULL(IFNULL(tbl_p_grid.NEGOCIE,   tbl_p_groupgrid.NEGOCIE),   0)   AS `isNegotiate`
FROM ' . $schemaName . '.GC_PRIXREF tbl_p_basegrid
LEFT JOIN CLIENT tbl_f_customer
ON tbl_p_basegrid.CLIENTTARIFAUTO = tbl_f_customer.CLIENTTARIFAUTO
LEFT JOIN ' . $schemaName . '.GC_GRILLE tbl_p_grid
ON tbl_p_grid.CLAUTO            = tbl_f_customer.CLAUTO   AND
   tbl_p_basegrid.FAMTARAUTO    = tbl_p_grid.FAMTARAUTO   AND
   tbl_p_basegrid.GRNBJOURSMIN  = tbl_p_grid.GRNBJOURSMIN
LEFT JOIN ' . $schemaName . '.GC_PRIXGROUPE tbl_p_groupgrid
ON tbl_p_groupgrid.CLGROUPEAUTO = tbl_f_customer.CLGROUPEAUTO  AND
   tbl_p_basegrid.FAMTARAUTO    = tbl_p_groupgrid.FAMTARAUTO   AND
   tbl_p_basegrid.GRNBJOURSMIN  = tbl_p_groupgrid.GRNBJOURSMIN
LEFT JOIN TARIF tbl_p_tariff
ON tbl_p_basegrid.FAMTARAUTO    = tbl_p_tariff.FAMTARAUTO   AND
   tbl_p_basegrid.GRNBJOURSMIN  = tbl_p_tariff.TARIFNBJOURSMIN
WHERE tbl_f_customer.CLAUTO = ' . $customerId;
    if (defined $tabFilters->{'tariffFamily.id'})
    {
        $query .= '
AND tbl_p_basegrid.FAMTARAUTO = ' . $tabFilters->{'tariffFamily.id'};
    }
    if (defined $tabFilters->{'model.id'})
    {
        $query .= '
AND tbl_p_basegrid.FAMTARAUTO =
    (SELECT tbl_p_model.FAMTARAUTO
     FROM AUTH.MODELEMACHINE tbl_p_model
     WHERE tbl_p_model.MOMAAUTO = ' . $tabFilters->{'model.id'} . ')';
    }
    $query .= '
ORDER BY `tariffFamily.id`, `minDays`;';
    my $tab = $db->fetchAssoc($query);
    if (!$tab)
    {
        return undef;
    }

    # Formatage des données
    my $oldTabRow;
    my $oldTariffFamilyId = -1;
    foreach my $tabRow (@$tab)
    {
        $tabRow->{'tariffFamily.id'} *= 1;
        $tabRow->{'minDays'}         *= 1;
        $tabRow->{'isNegotiate'}     *= 1;
        $tabRow->{'maxDays'}         = undef;
        $tabRow->{'baseAmount'}      = &LOC::Util::round($tabRow->{'baseAmount'} * 1, 0);
        $tabRow->{'recoReduction'}   = &LOC::Util::round($tabRow->{'recoReduction'} * 1, 3);
        $tabRow->{'maxiReduction'}   = &LOC::Util::round($tabRow->{'maxiReduction'} * 1, 3);
        $tabRow->{'recoAmount'}      = &LOC::Util::round($tabRow->{'recoReduction'} * $tabRow->{'baseAmount'} / 100, 0);
        $tabRow->{'maxiAmount'}      = &LOC::Util::round($tabRow->{'maxiReduction'} * $tabRow->{'baseAmount'} / 100, 0);


        if ($oldTariffFamilyId != $tabRow->{'tariffFamily.id'})
        {
            $oldTabRow = {};
        }
        elsif (exists $oldTabRow->{'maxDays'})
        {
            $oldTabRow->{'maxDays'} = $tabRow->{'minDays'};
        }

        # Les durées inférieures à 1 jour ont le même tarif que la tranche à 1 jour
        if ($tabRow->{'minDays'} <= 1)
        {
            $tabRow->{'minDays'} = undef;
        }

        $oldTariffFamilyId = $tabRow->{'tariffFamily.id'};
        $oldTabRow = $tabRow;
    }

    return (wantarray ? @$tab : $tab);
}


# Function: getCustomerTariffsByPeriod
# Récupérer la liste des tarifs d'un client sur une tranche de durée
#
# Parameters:
# string  $countryId  - Pays
# int     $customerId - Id du client
# int     $days       - Nombre de jours
# hashref $tabFilters - Liste des filtres
# int     $flags      - Flags
#
# Returns:
# hash|hashref -
sub getCustomerTariffsByPeriod
{
    my ($countryId, $customerId, $days, $tabFilters, $flags) = @_;
    if (!defined $flags)
    {
        $flags = 0;
    }

    my $tabTmp = &getCustomerTariffs($countryId, $customerId, $tabFilters, $flags);
    my @tab = ();
    foreach my $tabRow (@$tabTmp)
    {
        if ((!defined $tabRow->{'minDays'} || $days >= $tabRow->{'minDays'}) &&
            (!defined $tabRow->{'maxDays'} || $days < $tabRow->{'maxDays'}))
        {
            push(@tab, $tabRow);
        }
    }
    return (wantarray ? @tab : \@tab);
}


# Function: getInfos
# Récupérer les informations du tarif de location
#
# Contenu du hashage de retour :
# - estimateLine.id => identifiant de la ligne de devis associée ;
# - contract.id => identifiant du contrat associé ;
# - customer.id => identifiant du client associé ;
# - amountType => type de montant ('day' ou 'month') ;
# - baseAmount => tarif de base ;
# - standardReductionPercent => pourcentage de remise standard ;
# - maxiReductionPercent => pourcentage de remise max ;
# - specialReductionType => type de remise exceptionnelle (REDUCTIONTYPE_NONE, REDUCTIONTYPE_PENDING ou REDUCTIONTYPE_PROCESSED) ;
# - specialReductionId => identifiant de la remise exceptionnelle ;
# - specialReductionDate => date de la remise exceptionnelle ;
# - specialReductionPercent => pourcentage de remise exceptionnelle ;
# - specialReductionJustification => justificatif de la remise exceptionnelle ;
# - specialReductionBillLabel => libellé facture de la remise exceptionnelle ;
# - specialReductionStatus => état de la remise exceptionnelle (REDUCTIONSTATE_VALIDATED, REDUCTIONSTATE_PENDING, REDUCTIONSTATE_REFUSED);
# - amount => prix de location;
#
# Parameters:
# string $countryId    - Pays
# string $documentType - Type de document ('estimateLine' => ligne de devis, 'contract' => Contrat)
# string $documentId   - Id du document (ligne de devis ou contrat)
# int     $flags       - Flags
#
# Returns:
# hash|hashref -
sub getInfos
{
    my ($countryId, $documentType, $documentId, $flags) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = 'GESTIONTARIF' . $countryId;

    my $query;
    if ($documentType eq 'estimateLine')
    {
        $query = '
SELECT
    tbl_f_estimatedetail.DETAILSDEVISAUTO AS `estimateLine.id`,
    IFNULL(tbl_f_rentestim1.CONTRATAUTO, tbl_f_rentestim2.CONTRATAUTO) AS `contract.id`,
    IFNULL(tbl_f_rentestim1.CLAUTO, tbl_f_rentestim2.CLAUTO) AS `customer.id`,
    IF (tbl_f_estimatedetail.DETAILSDEVISTYPEMONTANT = 1, "day", "month") AS `amountType`,
    tbl_f_estimatedetail.DETAILSDEVISTARIFBASE AS `baseAmount`,
    tbl_f_estimatedetail.DETAILSDEVISREMISE AS `standardReductionPercent`,
    tbl_f_estimatedetail.DETAILSDEVISREMISEMAX AS `maxiReductionPercent`,
    IF(tbl_f_rentestim1.ALAUTO IS NULL, IF(tbl_f_rentestim2.ARCHIVESAUTO IS NULL, ' . REDUCTIONTYPE_NONE . ', ' . REDUCTIONTYPE_PROCESSED . '), ' . REDUCTIONTYPE_PENDING . ') AS `specialReductionType`,
    IFNULL(tbl_f_rentestim1.ALAUTO, tbl_f_rentestim2.ARCHIVESAUTO) AS `specialReductionId`,
    IFNULL(tbl_f_rentestim1.ALDATE, tbl_f_rentestim2.ALERTEDATE) AS `specialReductionDate`,
    tbl_f_estimatedetail.DETAILSDEVISREMISEEX AS `specialReductionPercent`,
    IFNULL(tbl_f_rentestim1.ALMOTIF, tbl_f_rentestim2.ALMOTIF) AS `specialReductionJustification`,
    IFNULL(tbl_f_rentestim1.ALLIBELLE, tbl_f_rentestim2.ALLIBELLE) AS `specialReductionBillLabel`,
    IF (tbl_f_estimatedetail.DETAILSDEVISREMISEEX > 0,
        ELT(1-tbl_f_estimatedetail.DETAILSDEVISREMISEOK, "' . REDUCTIONSTATE_REFUSED . '", "' . REDUCTIONSTATE_PENDING . '", "' . REDUCTIONSTATE_VALIDATED . '"),
        IF (tbl_f_rentestim2.VALIDOK = 0, "' . REDUCTIONSTATE_REFUSED . '", "")) AS `specialReductionStatus`,
    tbl_f_estimatedetail.DETAILSDEVISPXJOURSFR AS `amount`
FROM DETAILSDEVIS tbl_f_estimatedetail
LEFT JOIN ' . $schemaName . '.GC_ALERTES tbl_f_rentestim1
ON tbl_f_rentestim1.DETAILSDEVISAUTO = tbl_f_estimatedetail.DETAILSDEVISAUTO
LEFT JOIN ' . $schemaName . '.GC_ARCHIVES tbl_f_rentestim2
ON tbl_f_rentestim2.DETAILSDEVISAUTO = tbl_f_estimatedetail.DETAILSDEVISAUTO
WHERE tbl_f_estimatedetail.DETAILSDEVISAUTO = ' . $documentId . ';';
    }
    elsif ($documentType eq 'contract')
    {
        $query = '
SELECT
    IFNULL(tbl_f_rentestim1.DETAILSDEVISAUTO, tbl_f_rentestim2.DETAILSDEVISAUTO) AS `estimateLine.id`,
    tbl_f_contract.CONTRATAUTO AS `contract.id`,
    IFNULL(tbl_f_rentestim1.CLAUTO, tbl_f_rentestim2.CLAUTO) AS `customer.id`,
    IF(tbl_f_contractamount.MOFORFAITFR = 0, IF(tbl_f_contractamount.MOTARIFMSFR = 0, "day", "month"), "fixed") AS `amountType`,
    tbl_f_contractamount.MOTARIFBASE AS `baseAmount`,
    tbl_f_contractamount.MOREMISE AS `standardReductionPercent`,
    tbl_f_contractamount.MOREMISEMAX AS `maxiReductionPercent`,
    IF(tbl_f_rentestim1.ALAUTO IS NULL, IF(tbl_f_rentestim2.ARCHIVESAUTO IS NULL, ' . REDUCTIONTYPE_NONE . ', ' . REDUCTIONTYPE_PROCESSED . '), ' . REDUCTIONTYPE_PENDING . ') AS `specialReductionType`,
    IFNULL(tbl_f_rentestim1.ALAUTO, tbl_f_rentestim2.ARCHIVESAUTO) AS `specialReductionId`,
    IFNULL(tbl_f_rentestim1.ALDATE, tbl_f_rentestim2.ALERTEDATE) AS `specialReductionDate`,
    tbl_f_contractamount.MOREMISEEX AS `specialReductionPercent`,
    IFNULL(tbl_f_rentestim1.ALMOTIF, tbl_f_rentestim2.ALMOTIF) AS `specialReductionJustification`,
    IFNULL(tbl_f_rentestim1.ALLIBELLE, tbl_f_rentestim2.ALLIBELLE) AS `specialReductionBillLabel`,
    IF (tbl_f_contractamount.MOREMISEEX > 0,
        ELT(1-tbl_f_contractamount.MOREMISEOK, "' . REDUCTIONSTATE_REFUSED . '", "' . REDUCTIONSTATE_PENDING . '", "' . REDUCTIONSTATE_VALIDATED . '"),
        IF (tbl_f_rentestim2.VALIDOK = 0, "' . REDUCTIONSTATE_REFUSED . '", "")) AS `specialReductionStatus`,
    IF(tbl_f_contractamount.MOFORFAITFR = 0, IF(tbl_f_contractamount.MOTARIFMSFR = 0, tbl_f_contractamount.MOPXJOURFR, tbl_f_contractamount.MOTARIFMSFR), tbl_f_contractamount.MOFORFAITFR) AS `amount`
FROM CONTRAT tbl_f_contract
INNER JOIN MONTANT tbl_f_contractamount
ON tbl_f_contractamount.MOAUTO = tbl_f_contract.MOAUTO
LEFT JOIN ' . $schemaName . '.GC_ALERTES tbl_f_rentestim1
ON tbl_f_rentestim1.CONTRATAUTO = tbl_f_contract.CONTRATAUTO
LEFT JOIN ' . $schemaName . '.GC_ARCHIVES tbl_f_rentestim2
ON tbl_f_rentestim2.CONTRATAUTO = tbl_f_contract.CONTRATAUTO
WHERE tbl_f_contract.CONTRATAUTO = ' . $documentId . ';';
    }
    my $tab = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if (!$tab)
    {
        return undef;
    }

    # Formatage des données
    $tab->{'estimateLine.id'}          *= 1;
    $tab->{'contract.id'}              *= 1;
    $tab->{'baseAmount'}               = &LOC::Util::round($tab->{'baseAmount'} * 1, 0);
    $tab->{'customer.id'}              *= 1;
    $tab->{'standardReductionPercent'} = &LOC::Util::round($tab->{'standardReductionPercent'} * 1, 3);
    $tab->{'specialReductionJustification'} .= '';
    $tab->{'specialReductionBillLabel'} .= '';
    $tab->{'specialReductionType'}     *= 1;
    $tab->{'specialReductionId'}       *= 1;
    $tab->{'specialReductionPercent'}  = &LOC::Util::round($tab->{'specialReductionPercent'} * 1, 3);
    $tab->{'maxiReductionPercent'}     = &LOC::Util::round($tab->{'maxiReductionPercent'} * 1, 3);
    $tab->{'amount'}                   *= 1;
    if ($documentType eq 'estimateLine')
    {
        # Pas de maintien de tarif (au changement de durée et de modèle)
        $tab->{'isKeepAmount'} = 0;
    }
    elsif ($documentType eq 'contract')
    {
        # Maintien du tarif (au changement de durée et de modèle) toujours activé pour un contrat en modification
        $tab->{'isKeepAmount'} = ($tab->{'baseAmount'} != 0 ? 1 : 0);
    }

    return (wantarray ? @$tab : $tab);
}


# Function: save
# Sauvegarder le tarif de location
#
# Parameters:
# string   $countryId     - Pays
# string   $documentType  - Type de document ('estimateLine' => ligne de devis, 'contract' => Contrat)
# string   $documentId    - Id du document (ligne de devis ou contrat)
# hashref  $tabData       - Données à mettre à jour
# int      $userId        - Utilisateur responsable de la modification
# arrayref $tabErrors     - Tableau des erreurs
# hashref  $tabEndUpds    - Mises à jour de fin
#
# Returns:
# bool -
sub save
{
    my ($countryId, $documentType, $documentId, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    $tabErrors->{'fatal'} = [];

    # Récupération des informations de la ligne de devis (optimisations)
    my $editMode = $tabData->{'editMode'};
    my $rights   = $tabData->{'rights'};

    my $customerId;
    my $agencyId;
    my $modelId;
    my $quantity;
    my $isKeepAmount = ($editMode eq 'create' ? 0 : $tabData->{'isKeepAmount'} * 1);
    my $linkedEstimateLineId = 0;
    my $isLineConvert = 0;
    if ($documentType eq 'estimateLine')
    {
        $customerId    = $tabData->{'estimateLine.estimate.customer.id'} * 1;
        $agencyId      = $tabData->{'estimateLine.estimate.agency.id'};
        $modelId       = $tabData->{'estimateLine.model.id'} * 1;
        $quantity      = $tabData->{'estimateLine.quantity'} * 1;
    }
    elsif ($documentType eq 'contract')
    {
        $customerId    = $tabData->{'contract.customer.id'} * 1;
        $agencyId      = $tabData->{'contract.agency.id'};
        $modelId       = $tabData->{'contract.model.id'} * 1;
        $quantity      = 1;
        if ($editMode eq 'create')
        {
            $linkedEstimateLineId = $tabData->{'contract.linkedEstimateLine.id'} * 1;
            $isLineConvert        = ($linkedEstimateLineId != 0);
        }
    }


    # Récupération des informations de la remise
    my $tabInfos;
    if ($isLineConvert)
    {
        $tabInfos = &getInfos($countryId, 'estimateLine', $linkedEstimateLineId, GETINFOS_BASE);
        # Pour un passage de la ligne de devis en contrat, ca ne peut pas être une alerte en attente
        if ($tabInfos->{'specialReductionType'} == REDUCTIONTYPE_PENDING)
        {
            print STDERR sprintf($fatalErrMsg, 'save', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            return 0;
        }
    }
    else
    {
        $tabInfos = &getInfos($countryId, $documentType, $documentId, GETINFOS_BASE);
    }
    if (!$tabInfos)
    {
        print STDERR sprintf($fatalErrMsg, 'save', __LINE__,  __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    # Récupérer le taux de conversion de la monnaie locale en Euros
    my $euroRate = &LOC::Country::getEuroRate($countryId);


    # Récupération des informations
    my $oldAmountType                 = $tabInfos->{'amountType'};
    my $amountType                    = (exists $tabData->{'amountType'} ? $tabData->{'amountType'} : $oldAmountType);
    my $oldBaseAmount                 = $tabInfos->{'baseAmount'};
    my $baseAmount                    = (exists $tabData->{'baseAmount'} ? $tabData->{'baseAmount'} : $oldBaseAmount);
    my $oldMaxiReducPercent           = $tabInfos->{'maxiReductionPercent'};
    my $maxiReducPercent              = (exists $tabData->{'maxiReductionPercent'} ? $tabData->{'maxiReductionPercent'} : $oldMaxiReducPercent);
    my $specialReductionBillLabel     = (exists $tabData->{'specialReductionBillLabel'} ? &LOC::Util::trim($tabData->{'specialReductionBillLabel'}) : $tabInfos->{'specialReductionBillLabel'});
    my $specialReductionJustification = (exists $tabData->{'specialReductionJustification'} ? &LOC::Util::trim($tabData->{'specialReductionJustification'}) : $tabInfos->{'specialReductionJustification'});
    my $oldStandardReductionPercent   = $tabInfos->{'standardReductionPercent'};
    my $standardReductionPercent      = (exists $tabData->{'standardReductionPercent'} ? $tabData->{'standardReductionPercent'} : $oldStandardReductionPercent);
    my $oldSpecialReductionPercent    = $tabInfos->{'specialReductionPercent'};
    my $specialReductionPercent       = (exists $tabData->{'specialReductionPercent'} ? $tabData->{'specialReductionPercent'} : $oldSpecialReductionPercent);
    my $oldAmount                     = $tabInfos->{'amount'};
    my $amount                        = (exists $tabData->{'amount'} ? $tabData->{'amount'} : $oldAmount);

    my $multiplier                    = ($amountType eq 'day' ? 1 : LOC::Date::AVERAGE_MONTH_WORKING_DAYS);
    my $days                          = $tabData->{'days'} * 1;
    my $duration                      = $days / $multiplier;

    my $reducType                     = $tabInfos->{'specialReductionType'};
    my $reducId                       = $tabInfos->{'specialReductionId'};

    # Si le tarif de base est 0 alors on force les remises et le prix final à 0
    if ($baseAmount == 0)
    {
        $standardReductionPercent = 0;
        $specialReductionPercent  = 0;
        $amount                   = 0;
        $isKeepAmount             = 0;
    }

    # Vérifie si le montant et le type de montant ont le droit d'être modifié
    if (defined $rights && (
        # Type de montant
        (($amountType ne $oldAmountType) && !&_hasRight($rights, 'amountType')) ||
        # Montant
        (($amount != $oldAmount) && !&_hasRight($rights, 'reduc')) ))
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
        return 0;
    }


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $result;
    my $tabUpdates;
    my $schemaName = 'GESTIONTARIF' . $countryId;
    my $schemaNameLoc = &LOC::Db::getSchemaName($countryId);

    if (!$hasTransaction)
    {
        # Démarre la transaction
        $db->beginTransaction();
    }

    my $tabDocumentUpdates = {};
    if (!$isKeepAmount)
    {
        # *********************************************************************************************
        # PAS DE MAINTIEN DU TARIF DE LOCATION
        # *********************************************************************************************

        # Vérification des pourcentages de remises
        # Si le tarif de base ou le pourcentage de remise max ont changé alors on prend les valeurs théoriques
        my $hasModifications = 0;
        if ($amountType       ne $oldAmountType ||
            $baseAmount       != $oldBaseAmount ||
            $maxiReducPercent != $oldMaxiReducPercent ||
            $amount           != $oldAmount)
        {
            # Récupération des tarifs du client pour le modèle de machine
            my $tabTariffs = &getCustomerTariffsByPeriod($countryId, $customerId, $days, {'model.id' => $modelId});
            my $theoricBaseAmount       = $tabTariffs->[0]->{'baseAmount'} * $multiplier;
            my $theoricMaxiReducPercent = $tabTariffs->[0]->{'maxiReduction'};

            # Vérification du tarif de base et de la remise max
            if ($baseAmount != $theoricBaseAmount || 
                &LOC::Util::round($maxiReducPercent, 3) != &LOC::Util::round($theoricMaxiReducPercent, 3))
            {
                print STDERR sprintf($fatalErrMsg, 'save', __LINE__, __FILE__, '');
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                return 0;
            }
            $hasModifications = 1;
        }

        if ($baseAmount != 0)
        {
            # Calcul des pourcentages de réductions
            my $reductionAmount  = $baseAmount - $amount;
            my $reductionPercent = &LOC::Util::round(100 * $reductionAmount / $baseAmount, 3);

            # Recalcul des pourcentages
            $standardReductionPercent   = ($reductionPercent > $maxiReducPercent ? $maxiReducPercent : $reductionPercent);
            my $standardReductionAmount = &LOC::Util::round($standardReductionPercent * $baseAmount / 100, 0);
            my $specialReductionAmount  = 0;
            $specialReductionPercent    = 0;
            if ($standardReductionAmount < $baseAmount)
            {
                $specialReductionAmount  = $reductionAmount - $standardReductionAmount;
                $specialReductionPercent = &LOC::Util::round(100 * $specialReductionAmount / ($baseAmount - $standardReductionAmount), 3);
            }

            # Calcul du prix de location par rapport aux pourcentages donnés
            my $testAmount = $baseAmount;
            # - On enlève la remise standard au prix de base
            my $percent = (exists $tabData->{'standardReductionPercent'} ? $tabData->{'standardReductionPercent'} : $tabInfos->{'standardReductionPercent'});
            $testAmount -= &LOC::Util::round($percent * $testAmount / 100, 0);
            # - On y enlève la remise exceptionnelle
            my $percent = (exists $tabData->{'specialReductionPercent'} ? $tabData->{'specialReductionPercent'} : $tabInfos->{'specialReductionPercent'});
            $testAmount -= &LOC::Util::round($percent * $testAmount / 100, 0);

            # Vérification du montant final
            if ($testAmount != $amount)
            {
                print STDERR sprintf($fatalErrMsg, 'save', __LINE__, __FILE__, ' {' . $testAmount . ' != ' . $amount . '}');
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                return 0;
            }
        }

        # Calcul du prix total de location
        my $total = &LOC::Util::round($amount * $quantity * $duration, 2);

        # Justificatif de la demande de remise exceptionnelle obligatoire
        if (($specialReductionPercent > 0 && $specialReductionJustification eq '') &&
            ($hasModifications || $reducType == REDUCTIONTYPE_PENDING))
        {
            push(@{$tabErrors->{'fatal'}}, ERROR_NOSPECIALREDUCTIONJUSTIF);
            return 0;
        }


        # Trace
        if (defined $tabEndUpds && $editMode eq 'update' && $hasModifications)
        {
            # Modification du prix de location
            my $tabTrace = {
                'schema'  => $schemaNameLoc,
                'code'    => LOC::Log::EventType::UPDRENTAMOUNT,
                'old'     => {
                    'amount'                   => $oldAmount,
                    'amountType'               => $oldAmountType,
                    'baseAmount'               => $oldBaseAmount,
                    'standardReductionPercent' => $oldStandardReductionPercent,
                    'specialReductionPercent'  => $oldSpecialReductionPercent
                },
                'new'     => {
                    'amount'                   => $amount,
                    'amountType'               => $amountType,
                    'baseAmount'               => $baseAmount,
                    'standardReductionPercent' => $standardReductionPercent,
                    'specialReductionPercent'  => $specialReductionPercent
                },
                'content' => $oldAmount . ' | ' . $oldAmountType .
                             ' (base ' . $oldBaseAmount . ', ' .
                               'RS ' . $oldStandardReductionPercent . ', ' .
                               'RE ' . $oldSpecialReductionPercent . ' %)' .
                             ' → ' .
                             $amount . ' | ' . $amountType .
                             ' (base ' . $baseAmount . ', ' .
                               'RS ' . $standardReductionPercent . ' %, ' .
                               'RE ' . $specialReductionPercent . ' %)'
            };

            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }

        # Mise à jour de la base de données
        if ($documentType eq 'estimateLine')
        {
            # Ligne de devis
            $tabDocumentUpdates = {
                'DETAILSDEVISTYPEMONTANT' => ($amountType eq 'day' ? 1 : 2),
                'DETAILSDEVISTARIFBASE'   => $baseAmount,
                'DETAILSDEVISTARIFBASEEU' => $baseAmount * $euroRate,
                'DETAILSDEVISREMISE'      => $standardReductionPercent,
                'DETAILSDEVISREMISEEX'    => $specialReductionPercent,
                'DETAILSDEVISREMISEMAX'   => $maxiReducPercent,
                'DETAILSDEVISPXJOURSFR'   => $amount,
                'DETAILSDEVISPXJOUREU'    => $amount * $euroRate,
                'DETAILSDEVISMONTANTFR'   => $total,
                'DETAILSDEVISMONTANTEU'   => $total * $euroRate
            };
            # Si il y a eu une modification alors on force l'état à en attente et
            # on met à jour la date mise à jour du tarif de location
            if ($hasModifications)
            {
                $tabDocumentUpdates->{'DETAILSDEVISDATEMODIFTARIFLOC*'} = 'Now()';
                $tabDocumentUpdates->{'DETAILSDEVISREMISEOK'} = -1;
            }
        }
        elsif ($documentType eq 'contract')
        {
            # contrat
            $tabDocumentUpdates = {
                'MOTARIFBASE'   => $baseAmount,
                'MOTARIFBASEEU' => $baseAmount * $euroRate,
                'MOREMISE'      => $standardReductionPercent,
                'MOREMISEEX'    => $specialReductionPercent,
                'MOREMISEMAX'   => $maxiReducPercent,
                'MOPXJOURFR'    => 0,
                'MOPXJOUREU'    => 0,
                'MOTARIFMSFR'   => 0,
                'MOTARIFMSEU'   => 0,
                'MOFORFAITFR'   => 0,
                'MOFORFAITEU'   => 0
            };
            # Si il y a eu une modification alors on force l'état à en attente
            # Si c'est un passage de ligne en contrat alors on laisse l'état validé
            if ($hasModifications)
            {
                $tabDocumentUpdates->{'MOREMISEOK'} = -1;
            }
            elsif ($isLineConvert)
            {
                $tabDocumentUpdates->{'MOREMISEOK'} = -2;
            }
            # Stockage par type de montant:
            if ($amountType eq 'day')
            {
                $tabDocumentUpdates->{'MOPXJOURFR'} = $amount;
                $tabDocumentUpdates->{'MOPXJOUREU'} = $amount * $euroRate;
            }
            elsif ($amountType eq 'month')
            {
                $tabDocumentUpdates->{'MOTARIFMSFR'} = $amount;
                $tabDocumentUpdates->{'MOTARIFMSEU'} = $amount * $euroRate;
            }
            elsif ($amountType eq 'fixed')
            {
                $tabDocumentUpdates->{'MOFORFAITFR'} = $amount;
                $tabDocumentUpdates->{'MOFORFAITEU'} = $amount * $euroRate;
            }
        }

        # Detection des changements impactant l'état de la remise
        if ($hasModifications)
        {
            # Si il y a eu une modification alors on supprime l'estimation validée
            if (!$isLineConvert && $reducType == REDUCTIONTYPE_PROCESSED)
            {
                # On désaffecte l'estimation validée de la ligne de devis ou du contrat suivant le cas
                $tabUpdates = {};
                if ($documentType eq 'estimateLine')
                {
                    $tabUpdates->{'DETAILSDEVISAUTO'} = 0;
                }
                elsif ($documentType eq 'contract')
                {
                    $tabUpdates->{'CONTRATAUTO'} = 0;
                }
                if ($db->update('GC_ARCHIVES', $tabUpdates, {'ARCHIVESAUTO*' => $reducId}, $schemaName) == -1)
                {
                    print STDERR sprintf($fatalErrMsg, 'save', __LINE__, __FILE__, '');
                    push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                    # Annule la transaction
                    if (!$hasTransaction)
                    {
                        $db->rollBack();
                    }
                    return 0;
                }

                # Et on supprime les lignes qui ne sont ni affectées à une ligne de devis, ni à un contrat
                if ($db->delete('GC_ARCHIVES', {'DETAILSDEVISAUTO*' => 0, 'CONTRATAUTO*' => 0}, $schemaName) == -1)
                {
                    print STDERR sprintf($fatalErrMsg, 'save', __LINE__, __FILE__, '');
                    push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                    # Annule la transaction
                    if (!$hasTransaction)
                    {
                        $db->rollBack();
                    }
                    return 0;
                }
            }


            # Si il y a une remise exceptionnelle...
            if ($specialReductionPercent > 0)
            {
                $tabUpdates = {
                    'CLAUTO'           => $customerId,
                    'PEAUTO'           => $userId,
                    'DETAILSDEVISAUTO' => ($documentType eq 'estimateLine' ? $documentId : 0),
                    'CONTRATAUTO'      => ($documentType eq 'contract' ? $documentId : 0),
                    'REMISEREQ'        => $specialReductionPercent,
                    'REMISEACC'        => 0,
                    'ALMOTIF'          => $specialReductionJustification,
                    'ALLIBELLE'        => $specialReductionBillLabel,
                    'ALETAT'           => 0,
                    'PEAUTOTRAITEMENT' => undef
                };

                if (!$isLineConvert && $reducType == REDUCTIONTYPE_PENDING)
                {
                    $result = $db->update('GC_ALERTES', $tabUpdates, {'ALAUTO*' => $reducId}, $schemaName);
                }
                else
                {
                    $tabUpdates->{'ALDATE*'}   = 'Now()';
                    $tabUpdates->{'AGAUTO'}    = $agencyId;
                    $tabUpdates->{'LIBALAUTO'} = 0;
                    $result = $db->insert('GC_ALERTES', $tabUpdates, $schemaName);
                }

                # Erreur ?
                if ($result == -1)
                {
                    print STDERR sprintf($fatalErrMsg, 'save', __LINE__, __FILE__, '');
                    push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                    # Annule la transaction
                    if (!$hasTransaction)
                    {
                        $db->rollBack();
                    }
                    return 0;
                }
            }
            else
            {
                # Suppression des demandes de remises si elles existent
                if (!$isLineConvert && $reducType == REDUCTIONTYPE_PENDING)
                {
                    if ($db->delete('GC_ALERTES', {'ALAUTO*' => $reducId}, $schemaName) == -1)
                    {
                        print STDERR sprintf($fatalErrMsg, 'save', __LINE__, __FILE__, '');
                        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                        # Annule la transaction
                        if (!$hasTransaction)
                        {
                            $db->rollBack();
                        }
                        return 0;
                    }
                }

                # État de la RE remis en "acceptée" sur la ligne de devis ou le contrat
                if ($documentType eq 'estimateLine')
                {
                    $tabDocumentUpdates->{'DETAILSDEVISREMISEOK'} = -2;
                }
                elsif ($documentType eq 'contract')
                {
                    $tabDocumentUpdates->{'MOREMISEOK'} = -2;
                }
            }
        }
        else
        {
            if ($isLineConvert)
            {
                # Affectation de l'estimation de la ligne de devis au contrat
                if ($reducType == REDUCTIONTYPE_PROCESSED)
                {
                    if ($db->update('GC_ARCHIVES', {'CONTRATAUTO' => $documentId}, {'ARCHIVESAUTO*' => $reducId}, $schemaName) == -1)
                    {
                        print STDERR sprintf($fatalErrMsg, 'save', __LINE__, __FILE__, '');
                        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                        # Annule la transaction
                        if (!$hasTransaction)
                        {
                            $db->rollBack();
                        }
                        return 0;
                    }
                }
            }
            else
            {
                # Modifications n'impactant pas l'état de la demande
                if ($reducType == REDUCTIONTYPE_PENDING && $specialReductionPercent > 0)
                {
                    $tabUpdates = {
                        'ALMOTIF'   => $specialReductionJustification,
                        'ALLIBELLE' => $specialReductionBillLabel
                    };
                    if ($db->update('GC_ALERTES', $tabUpdates, {'ALAUTO*' => $reducId}, $schemaName) == -1)
                    {
                        print STDERR sprintf($fatalErrMsg, 'save', __LINE__, __FILE__, '');
                        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                        # Annule la transaction
                        if (!$hasTransaction)
                        {
                            $db->rollBack();
                        }
                        return 0;
                    }
                }
            }
        }
    }
    else
    {
        # *********************************************************************************************
        # MAINTIEN DU TARIF DE LOCATION
        # *********************************************************************************************

        # Vérification du prix de loc et de la remise excep (doit être identique à l'ancienne)
        if ($amountType              ne $oldAmountType ||
            #($baseAmount             == $oldBaseAmount && $maxiReducPercent == $oldMaxiReducPercent) ||
            $amount                  != $oldAmount ||
            $specialReductionPercent != $oldSpecialReductionPercent)
        {
            print STDERR sprintf($fatalErrMsg, 'save', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            return 0;
        }

        # Récupération des tarifs du client pour le modèle de machine
        my $tabTariffs = &getCustomerTariffsByPeriod($countryId, $customerId, $days, {'model.id' => $modelId});
        my $theoricBaseAmount       = $tabTariffs->[0]->{'baseAmount'} * $multiplier;
        my $theoricMaxiReducPercent = $tabTariffs->[0]->{'maxiReduction'};

        # Vérification du tarif de base et de la remise max
        if (($baseAmount != $oldBaseAmount &&
             $baseAmount != $theoricBaseAmount) || 
            (&LOC::Util::round($maxiReducPercent, 3) != &LOC::Util::round($oldMaxiReducPercent, 3) &&
             &LOC::Util::round($maxiReducPercent, 3) != &LOC::Util::round($theoricMaxiReducPercent, 3)))
        {
            print STDERR sprintf($fatalErrMsg, 'save', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            return 0;
        }

        # Calcul des pourcentages de réductions
        my $reductionAmount  = $baseAmount - $amount;
        my $reductionPercent = &LOC::Util::round(100 * $reductionAmount / $baseAmount, 3);

        my $standardReductionAmount = &LOC::Util::round($standardReductionPercent * $baseAmount / 100, 0);
        my $specialReductionAmount  = 0;
        $specialReductionPercent    = 0;
        if ($standardReductionAmount < $baseAmount)
        {
            $specialReductionAmount  = $reductionAmount - $standardReductionAmount;
            $specialReductionPercent = &LOC::Util::round(100 * $specialReductionAmount / ($baseAmount - $standardReductionAmount), 3);
        }


        # Calcul du prix de location par rapport aux pourcentages donnés
        my $testAmount = $baseAmount;
        # - On enlève la remise standard au prix de base
        my $percent = (exists $tabData->{'standardReductionPercent'} ? $tabData->{'standardReductionPercent'} : $tabInfos->{'standardReductionPercent'});
        $testAmount -= &LOC::Util::round($percent * $baseAmount / 100, 0);
        # - On y enlève la remise exceptionnelle
        my $percent = (exists $tabData->{'specialReductionPercent'} ? $tabData->{'specialReductionPercent'} : $tabInfos->{'specialReductionPercent'});
        $testAmount -= &LOC::Util::round($percent * $testAmount / 100, 0);

        # Vérification du montant final
        if ($testAmount != $amount)
        {
            print STDERR sprintf($fatalErrMsg, 'save', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            return 0;
        }

        # Calcul du prix total de location
        my $total = &LOC::Util::round($amount * $quantity * $duration, 2);

        # Justificatif de la demande de remise exceptionnelle obligatoire
        if (($specialReductionPercent > 0 && $specialReductionJustification eq '') && $reducType == REDUCTIONTYPE_PENDING)
        {
            push(@{$tabErrors->{'fatal'}}, ERROR_NOSPECIALREDUCTIONJUSTIF);
            return 0;
        }

        # Trace : Modification du prix de location
        if (defined $tabEndUpds && $editMode eq 'update')
        {
            # Vérifi si il y a eu des modifications
            if ($baseAmount               != $oldBaseAmount ||
                $maxiReducPercent         != $oldMaxiReducPercent ||
                $standardReductionPercent != $oldStandardReductionPercent)
            {
                my $tabTrace = {
                    'schema'  => $schemaNameLoc,
                    'code'    => LOC::Log::EventType::UPDRENTAMOUNT,
                    'old'     => {
                        'amount'                   => $oldAmount,
                        'amountType'               => $oldAmountType,
                        'baseAmount'               => $oldBaseAmount,
                        'standardReductionPercent' => $oldStandardReductionPercent,
                        'specialReductionPercent'  => $oldSpecialReductionPercent
                    },
                    'new'     => {
                        'amount'                   => $amount,
                        'amountType'               => $amountType,
                        'baseAmount'               => $baseAmount,
                        'standardReductionPercent' => $standardReductionPercent,
                        'specialReductionPercent'  => $specialReductionPercent
                    },
                    'content' => $oldAmount . ' | ' . $oldAmountType .
                                 ' (base ' . $oldBaseAmount . ', ' .
                                   'RS ' . $oldStandardReductionPercent . ', ' .
                                   'RE ' . $oldSpecialReductionPercent . ' %)' .
                                 ' → ' .
                                 $amount . ' | ' . $amountType .
                                 ' (base ' . $baseAmount . ', ' .
                                   'RS ' . $standardReductionPercent . ' %, ' .
                                   'RE ' . $specialReductionPercent . ' %)'
                };

                &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
            }
        }

        if ($reducType == REDUCTIONTYPE_PENDING)
        {
            my $tabUpdates = {
                'PEAUTO'           => $userId,
                'REMISEREQ'        => $specialReductionPercent,
                'REMISEACC'        => 0,
                'ALMOTIF'          => $specialReductionJustification,
                'ALLIBELLE'        => $specialReductionBillLabel
            };
            if ($db->update('GC_ALERTES', $tabUpdates, {'ALAUTO*' => $reducId}, $schemaName) == -1)
            {
                print STDERR sprintf($fatalErrMsg, 'save', __LINE__, __FILE__, '');
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return 0;
            }
        }
        elsif ($reducType == REDUCTIONTYPE_PROCESSED)
        {
            my $tabUpdates = {
                'PEAUTO'           => $userId,
                'REMISEREQ'        => $specialReductionPercent,
                'REMISEACC'        => 0
            };
            if ($db->update('GC_ARCHIVES', $tabUpdates, {'ARCHIVESAUTO*' => $reducId}, $schemaName) == -1)
            {
                print STDERR sprintf($fatalErrMsg, 'save', __LINE__, __FILE__, '');
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return 0;
            }
        }

        # Mise à jour de la base de données
        if ($documentType eq 'estimateLine')
        {
            $tabDocumentUpdates = {
                'DETAILSDEVISTARIFBASE'   => $baseAmount,
                'DETAILSDEVISTARIFBASEEU' => $baseAmount * $euroRate,
                'DETAILSDEVISREMISE'      => $standardReductionPercent,
                'DETAILSDEVISREMISEEX'    => $specialReductionPercent,
                'DETAILSDEVISREMISEMAX'   => $maxiReducPercent
            };
        }
        elsif ($documentType eq 'contract')
        {
            $tabDocumentUpdates = {
                'MOTARIFBASE'   => $baseAmount,
                'MOTARIFBASEEU' => $baseAmount * $euroRate,
                'MOREMISE'      => $standardReductionPercent,
                'MOREMISEEX'    => $specialReductionPercent,
                'MOREMISEMAX'   => $maxiReducPercent
            };
        }
    }


    # Mettre à jour les informations dans la ligne de devis ou sur le contrat
    if (keys %$tabDocumentUpdates > 0)
    {
        if ($documentType eq 'estimateLine')
        {
            # Mise à jour des données dans la table DETAILSDEVIS
            if ($db->update('DETAILSDEVIS', $tabDocumentUpdates, {'DETAILSDEVISAUTO*' => $documentId}) == -1)
            {
                print STDERR sprintf($fatalErrMsg, 'save', __LINE__, __FILE__, '');
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return 0;
            }
        }
        elsif ($documentType eq 'contract')
        {
            # Mise à jour des données dans la table MONTANT concernant la tarification location
            # Les totaux du contrat ne sont pas mis à jour ici
            my $column;
            my $value;
            my @tabSetters = ();
            while (($column, $value) = each(%$tabDocumentUpdates))
            {
                if (substr($column, -1, 1) eq '*')
                {
                    push(@tabSetters, 'tbl_f_contractamount.' . $db->quoteIdentifier(substr($column, 0, -1)) . ' = ' . $value);
                }
                else
                {
                    push(@tabSetters, 'tbl_f_contractamount.' . $db->quoteIdentifier($column) . ' = ' . $db->quote($value));
                }
            }
            my $query = '
UPDATE MONTANT tbl_f_contractamount
INNER JOIN CONTRAT tbl_f_contract
ON tbl_f_contractamount.MOAUTO = tbl_f_contract.MOAUTO
SET ' . join(', ', @tabSetters) . '
WHERE tbl_f_contract.CONTRATAUTO = ' . $documentId . ';';
            # Exécution de la requête
            my $resultObj = $db->query($query);
            if (!$resultObj)
            {
                print STDERR sprintf($fatalErrMsg, 'save', __LINE__, __FILE__, '');
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return 0;
            }
            $resultObj->close();
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    return 1;
}


# Function: getDurationRangeInfos
# Récupérer la tranche de durée pour un nombre de jours
#
# Parameters:
# string  $countryId - Pays
# int     $days      - Nombre de jours
#
# Returns:
# hashref|undef -
sub getDurationRangeInfos
{
    my ($countryId, $days) = @_;

    my $tab = &getDurationRangesList($countryId, {'duration' => $days});
    if (@$tab == 0)
    {
        return undef;
    }

    return pop(@$tab);
}


# Function: getDurationRangesList
# Récupérer la liste des tranches de durée
#
# Parameters:
# string  $countryId  - Pays
# hashref $tabFilters - Liste des filtres
# int     $flags      - Flags
#
# Returns:
# arrayref -
sub getDurationRangesList
{
    my ($countryId, $tabFilters, $flags) = @_;
    if (!defined $flags)
    {
        $flags = 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my $query = '
SELECT
    tdr_id AS `id`,
    tdr_label AS `label`,
    tdr_shortlabel AS `shortLabel`,
    tdr_lower AS `lower`,
    tdr_upper AS `upper`,
    tdr_order AS `order`
FROM ' . $schemaName . '.p_tariffdurationrange
WHERE tdr_sta_id <> "' . DURATIONRANGESTATE_DELETED . '"';
    if (defined $tabFilters->{'duration'})
    {
        $query .= '
AND (tdr_lower IS NULL OR tdr_lower <= ' . $tabFilters->{'duration'} . ')
AND (tdr_upper IS NULL OR tdr_upper >  ' . $tabFilters->{'duration'} . ')';
    }
    $query .= '
ORDER BY tdr_order ASC;';
    my $tab = $db->fetchAssoc($query);
    if (!$tab)
    {
        return undef;
    }

    # Formatage des données
    foreach my $tabRow (@$tab)
    {
        $tabRow->{'id'}    *= 1;
        $tabRow->{'lower'}  = ($tabRow->{'lower'} ? $tabRow->{'lower'} * 1 : undef);
        $tabRow->{'upper'}  = ($tabRow->{'upper'} ? $tabRow->{'upper'} * 1 : undef);
        $tabRow->{'order'} *= 1;
    }

    return (wantarray ? @$tab : $tab);
}


# Function: delete
# Supprimer le tarif de location
#
# Parameters:
# string   $countryId     - Pays
# string   $documentType  - Type de document ('estimateLine' => ligne de devis, 'contract' => Contrat)
# string   $documentId    - Id du document (ligne de devis ou contrat)
# int      $userId        - Utilisateur responsable de la modification
# arrayref $tabErrors     - Tableau des erreurs
#
# Returns:
# bool -
sub delete
{
    my ($countryId, $documentType, $documentId, $userId, $tabErrors) = @_;

    $tabErrors->{'fatal'} = [];

    # Récupération des informations sur la remise
    my $tabInfos = &getInfos($countryId, $documentType, $documentId, GETINFOS_BASE);
    if (!$tabInfos)
    {
        print STDERR sprintf($fatalErrMsg, 'delete', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    # Suppression de la remise si non affectée à une autre entité
    my $reducType  = $tabInfos->{'specialReductionType'};
    my $reducId    = $tabInfos->{'specialReductionId'};

    my $estimateLineId = $tabInfos->{'estimateLine.id'};
    my $contractId     = $tabInfos->{'contract.id'};

    if ($reducType != REDUCTIONTYPE_NONE && (($documentType eq 'estimateLine' && $contractId     == 0) ||
                                             ($documentType eq 'contract'     && $estimateLineId == 0)))
    {
        # Récupération de la connexion à la base de données
        my $db = &LOC::Db::getConnection('location', $countryId);
        my $hasTransaction = $db->hasTransaction();
        my $result;
        my $schemaName = 'GESTIONTARIF' . $countryId;

        if (!$hasTransaction)
        {
            # Démarre la transaction
            $db->beginTransaction();
        }

        if ($reducType == REDUCTIONTYPE_PROCESSED)
        {
            $result = $db->delete('GC_ARCHIVES', {'ARCHIVESAUTO*' => $reducId}, $schemaName);
        }
        elsif ($reducType == REDUCTIONTYPE_PENDING)
        {
            $result = $db->delete('GC_ALERTES', {'ALAUTO*' => $reducId}, $schemaName);
        }

        # Erreur ?
        if ($result == -1)
        {
            print STDERR sprintf($fatalErrMsg, 'delete', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }

        # Valide la transaction
        if (!$hasTransaction)
        {
            if (!$db->commit())
            {
                $db->rollBack();
                return 0;
            }
        }
    }
    return 1;
}

# Function: updateTariffs
# Mettre à jour la tarification location pour une famille tarifaire
#
# Parameters:
# string  $countryId      - Identifiant du pays
# int     $tariffFamilyId - Identifiant de la famille tarifaire
# hashref $tabData        - Données sur la tarification location
# int     $userId         - Identifiant de l'utilisateur responsable
# hashref $tabErrors      - Tableau des erreurs
# hashref $tabEndUpds     - Tableau des mises à jour de fin (facultatif)
#
# Returns
# bool
sub updateTariffs
{
    my ($countryId, $tariffFamilyId, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Récupération des informations de la famille tarifaire
    my $tabInfos = &LOC::TariffFamily::getInfos($countryId, $tariffFamilyId);
    if (!$tabInfos)
    {
        print STDERR sprintf($fatalErrMsg, 'updateTariffs', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }
    # Récupération des droits
    my $tabRights = &getRights($tabInfos, $tabUserInfos);

    # Récupération du taux monétaire
    my $euroRate = &LOC::Country::getEuroRate($countryId);

    # Mise à jour des tarifs
    foreach my $durationRangeId (keys(%$tabData))
    {
        my $tabUpdates = {};
        if (exists $tabData->{$durationRangeId}->{'amount'})
        {
            # Vérification - Droits
            if (!&_hasRight($tabRights->{'actions'}, 'baseTariff'))
            {
                print STDERR sprintf($fatalErrMsg, 'updateTariffs', __LINE__, __FILE__, '');
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            $tabUpdates->{'TARIFPRIX'}   = $tabData->{$durationRangeId}->{'amount'};
            $tabUpdates->{'TARIFPRIXEU'} = $tabData->{$durationRangeId}->{'amount'} * $euroRate;
        }
        if (exists $tabData->{$durationRangeId}->{'pricingPrice'})
        {
            # Vérification - Droits
            if (!&_hasRight($tabRights->{'actions'}, 'pricingPrice'))
            {
                print STDERR sprintf($fatalErrMsg, 'updateTariffs', __LINE__, __FILE__, '');
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            $tabUpdates->{'TARIFPRICING'}   = $tabData->{$durationRangeId}->{'pricingPrice'};
            $tabUpdates->{'TARIFPRICINGEU'} = $tabData->{$durationRangeId}->{'pricingPrice'} * $euroRate;
        }
        if (exists $tabData->{$durationRangeId}->{'costPrice'})
        {
            # Vérification - Droits
            if (!&_hasRight($tabRights->{'actions'}, 'costPrice'))
            {
                print STDERR sprintf($fatalErrMsg, 'updateTariffs', __LINE__, __FILE__, '');
                push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS);
                return 0;
            }
            $tabUpdates->{'TARIFPRIXREVIENT'}   = $tabData->{$durationRangeId}->{'costPrice'};
            $tabUpdates->{'TARIFPRIXREVIENTEU'} = $tabData->{$durationRangeId}->{'costPrice'} * $euroRate;
        }

        # Mise à jour des informations TARIF
        if (keys %$tabUpdates > 0)
        {
            if ($db->update('TARIF', $tabUpdates, {'FAMTARAUTO*'       => $tariffFamilyId,
                                                   'TRANCHEDUREEAUTO*' => $durationRangeId}) == -1)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                print STDERR sprintf($fatalErrMsg, 'updateTariffs', __LINE__, __FILE__, '');
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
                return 0;
            }
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            print STDERR sprintf($fatalErrMsg, 'updateTariffs', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            return 0;
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}



# Function: _hasRight
# Vérifier le droit à un champ
#
# Parameters:
# hashref $tab  - Tableau des droits
# string  $name - Nom du droit
#
# Returns:
# bool
sub _hasRight
{
    my ($tab, $name) = @_;

    return (ref($tab) eq 'HASH' ? ($tab->{$name} ? 1 : 0) : (index('|' . $tab . '|', '|' . $name . '|') == -1 ? 0 : 1));
}


# Function: getRights
# Récupérer les droits sur la tarification location (par rapport à l'utilisateur)
#
#
# Returns:
# hashref - Liste droits
sub getRights
{
    my $tabUserInfos = &LOC::Session::getUserInfos();

    my $isUserAdmin  = $tabUserInfos->{'isAdmin'};
    my $isUserSuperv = $tabUserInfos->{'isSuperv'};

    my $tabRights = {};

    # Initialisation des droits
    &LOC::Util::setTreeValues($tabRights, {
        'actions' => ['baseTariff', 'pricingPrice', 'costPrice']
    }, 0);

    # Tarifs
    if ($isUserSuperv)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'actions' => ['pricingPrice', 'costPrice']
        }, 1);
    }

    if ($isUserAdmin)
    {
        &LOC::Util::setTreeValues($tabRights, {
            'actions' => ['baseTariff', 'pricingPrice', 'costPrice']
        }, 1);
    }

    return $tabRights;
}

1;