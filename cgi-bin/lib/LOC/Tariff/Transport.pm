use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Tariff::Transport
# Module de gestion des tarifs de transport
package LOC::Tariff::Transport;


# Constants: États des remises exceptionnelles
# REDUCTIONSTATE_VALIDATED - validée
# REDUCTIONSTATE_PENDING   - en attente
# REDUCTIONSTATE_REFUSED   - refusée
# REDUCTIONSTATE_CANCELED  - annulée
use constant {
    REDUCTIONSTATE_VALIDATED => 'REM01',
    REDUCTIONSTATE_PENDING   => 'REM02',
    REDUCTIONSTATE_REFUSED   => 'REM03',
    REDUCTIONSTATE_CANCELED  => 'REM04',
};

use constant {
    GETINFOS_ALL  => 0xFFFF,
    GETINFOS_BASE => 0x0000,
    GETINFOS_SOAP => 0x0001
};


use strict;

use LOC::Db;
use LOC::Util;
use SOAP::Lite;
use LOC::Country;
use LOC::EndUpdates;
use LOC::Transport;



# Liste des erreurs:
#
# 0x0001 => Erreur fatale
#
# 0x0100 => Identifiant de l'estimation incorrect
# 0x0101 => Le client à changé
# 0x0102 => La zone de transport a changé
# 0x0103 => Le montant du transport aller est inférieur à l'estimation
# 0x0104 => Le montant du transport retour est inférieur à l'estimation
# 0x0105 => Le contrat lié au transport aller est invalide
# 0x0106 => Le contrat lié au transport retour est invalide
# 0x0107 => Le forfait machine supplémentaire aller est inférieur à la valeur minimale
# 0x0108 => Le forfait machine supplémentaire retour est inférieur à la valeur minimale


# Function: checkIfErrors
# Indique si le document (devis ou contrat) contient des erreurs sur les estimations transports
# (pour un devis: les lignes en erreurs sont retournées, pour un contrat: 1, sinon 0)
#
# Parameters:
# string  $countryId    - Pays
# string  $documentType - Type de document ('estimate' => Devis, 'estimateLine' => Ligne de devis, 'contract' => Contrat)
# string  $documentId   - Id du document (ligne de devis ou contrat)
#
# Returns:
# bool|arrayref - Retourne un tableau pour le type 'estimate' et un booléen pour les autres types
sub checkIfErrors
{
    my ($countryId, $documentType, $documentId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = 'GESTIONTRANS' . $countryId;

    my $query;
    if ($documentType =~ m/^estimate/)
    {
        $query = '
SELECT
    tbl_f_estimate.DEVISAUTO AS `estimate.id`,
    tbl_f_estimatedetail.DETAILSDEVISAUTO AS `estimateLine.id`,' . &_getHasErrorsQuery($countryId, 'estimate', $documentId) . '
FROM DEVIS tbl_f_estimate
INNER JOIN DETAILSDEVIS tbl_f_estimatedetail
ON tbl_f_estimatedetail.DEVISAUTO = tbl_f_estimate.DEVISAUTO
LEFT JOIN ' . $schemaName . '.TRANSPORT tbl_f_trspestim
ON tbl_f_trspestim.TYPE = "DEVIS" AND
   tbl_f_trspestim.VALEUR = tbl_f_estimatedetail.DETAILSDEVISAUTO AND
   tbl_f_trspestim.ETAT <> "' . REDUCTIONSTATE_CANCELED . '"
LEFT JOIN ' . $schemaName . '.REMISEEX tbl_f_trspspcreduc
ON tbl_f_trspspcreduc.ID = tbl_f_trspestim.REMISEEX';
        if ($documentType eq 'estimateLine')
        {
            $query .= '
WHERE tbl_f_estimatedetail.DETAILSDEVISAUTO = ' . $documentId . '
AND tbl_f_estimatedetail.DETAILSDEVISOK = 0;';
        }
        else
        {
            $query .= '
WHERE tbl_f_estimate.DEVISAUTO = ' . $documentId . ';';
        }

        my $tabErroneousLines = [];
        my $nbErroneousLines = 0;
        my $stmt = $db->query($query);
        while (my $tabRow = $stmt->fetch(LOC::Db::FETCH_ASSOC))
        {
            my $lineId = $tabRow->{'estimateLine.id'} * 1;
            if ($tabRow->{'hasErrors'} == 1)
            {
                $nbErroneousLines++;
                push(@$tabErroneousLines, $lineId);
            }
        }
        $stmt->close();

        return ($documentType eq 'estimate' ? $tabErroneousLines : $nbErroneousLines);
    }
    else
    {
        $query = '
SELECT
    tbl_f_contract.CONTRATAUTO AS `estimate.id`,' . &_getHasErrorsQuery($countryId, 'contract', $documentId) . '
FROM CONTRAT tbl_f_contract
INNER JOIN MONTANT tbl_f_contractamount
ON tbl_f_contractamount.MOAUTO = tbl_f_contract.MOAUTO
LEFT JOIN ' . $schemaName . '.TRANSPORT tbl_f_trspestim
ON tbl_f_trspestim.TYPE = "CONTRAT" AND
   tbl_f_trspestim.VALEUR = tbl_f_contract.CONTRATAUTO AND
   tbl_f_trspestim.ETAT <> "' . REDUCTIONSTATE_CANCELED . '"
LEFT JOIN ' . $schemaName . '.REMISEEX tbl_f_trspspcreduc
ON tbl_f_trspspcreduc.ID = tbl_f_trspestim.REMISEEX
WHERE tbl_f_contract.CONTRATAUTO = ' . $documentId . ';';

        my $tabRow = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);

        if ($tabRow->{'hasErrors'} == 1)
        {
            return 1;
        }
        return 0;
    }

    return 0;
}


# Function: delete
# Supprimer le tarif transport
#
# Parameters:
# string   $countryId     - Pays
# string   $documentType  - Type de document ('estimateLine' => ligne de devis, 'contract' => Contrat)
# string   $documentId    - Id du document (ligne de devis ou contrat)
# int      $userId        - Utilisateur responsable de la modification
# arrayref $tabErrors     - Tableau des erreurs
#
# Returns:
# bool -
sub delete
{
    my ($countryId, $documentType, $documentId, $userId, $tabErrors) = @_;

    $tabErrors->{'fatal'} = [];
    my $fatalErrMsg = 'Erreur fatale: module LOC::Tariff::Transport, fonction delete(), ligne %d, fichier "%s"%s.' . "\n";

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = 'GESTIONTRANS' . $countryId;

    if (!$hasTransaction)
    {
        # Démarre la transaction
        $db->beginTransaction();
    }

    my $query = '
SELECT
    DISTINCT tbl_f_trspestim.REMISEEX
FROM ' . $schemaName . '.TRANSPORT tbl_f_trspestim
WHERE tbl_f_trspestim.VALEUR =  ' . $db->quote($documentId) . '
AND tbl_f_trspestim.TYPE = "' . ($documentType eq 'estimateLine' ? 'DEVIS' : 'CONTRAT') . '"
AND tbl_f_trspestim.REMISEEX <> 0;';
    my $tabReducs = $db->fetchCol($query);

    my $nbReducs = @$tabReducs;
    if ($nbReducs > 0)
    {
        if ($db->delete('REMISEEX', {'ID*' => $tabReducs}, $schemaName) == -1)
        {
            print STDERR sprintf($fatalErrMsg, __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
        }
    }
    if ($db->delete('TRANSPORT', {'TYPE'    => ($documentType eq 'estimateLine' ? 'DEVIS' : 'CONTRAT'),
                                  'VALEUR*' => $documentId}, $schemaName) == -1)
    {
        print STDERR sprintf($fatalErrMsg, __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    return 1;
}

# Function: duplicateEstimation
# Duplique l'estimation transport et la remise exceptionnelle associée
#
# Parameters:
# string   $countryId     - Pays
# string   $documentType  - Type de document ('estimateLine' => ligne de devis, 'contract' => Contrat)
# string   $documentId    - Id du document (ligne de devis ou contrat)
# string   $generalTempId
# string   $tempId
# string   $mode
#
# Returns:
# bool -
sub duplicateEstimation
{
    my ($countryId, $estimationId, $generalTempId, $tempId, $mode) = @_;

    # Service SOAP pour récupérer les informations de l'estimation transport
    sub SOAP::Transport::HTTP::Client::get_basic_credentials
    {
        my $tabUserInfos = &LOC::Session::getUserInfos();
        return ($tabUserInfos->{'login'} => $tabUserInfos->{'password'});
    };
    my $service = SOAP::Lite->uri('ws_get_trans')
                            ->proxy(&LOC::Globals::get('tariffTrspServicesUrl'))
                            ->ws_duplicate_estimation($estimationId, $generalTempId, $tempId, $mode);
    if (!$service->fault)
    {
        # Récupération des resultats
        return $service->result;
    }
    return 0;
}


# Function: getInfos
# Récupérer les informations d'estimation de transport
#
# Parameters:
# string  $countryId    - Pays
# string  $documentType - Type de document ('estimateLine' => ligne de devis, 'contract' => Contrat)
# string  $documentId   - Id du document (ligne de devis ou contrat)
# int     $flags        - Flags
#
# Returns:
# hash|hashref -
sub getInfos
{
    my ($countryId, $documentType, $documentId, $flags) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = 'GESTIONTRANS' . $countryId;

    # Informations transport sur la ligne de devis ou le contrat
    my $query;
    if ($documentType eq 'estimateLine')
    {
        $query = '
SELECT
    IFNULL(tbl_f_trspestim.ID, 0) AS `id`,
    tbl_f_estimatedetail.DETAILSDEVISENLALLER AS `isNoDelivery`,
    -tbl_f_estimatedetail.DETAILSDEVISMACHSUPALLER AS `isAddMachineDelivery`,
    tbl_f_estimatedetail.DETAILSDEVISTRD AS `deliveryAmount`,
    IF (tbl_f_estimatedetail.DETAILSDEVISOK = 0, 1, 0) AS `isDeliveryAmountPossible`,
    tbl_f_estimatedetail.DETAILSDEVISENLRETOUR AS `isNoRecovery`,
    -tbl_f_estimatedetail.DETAILSDEVISMACHSUPRETOUR AS `isAddMachineRecovery`,
    tbl_f_estimatedetail.DETAILSDEVISTRR AS `recoveryAmount`,
    IF (tbl_f_estimatedetail.DETAILSDEVISOK = 0, 1, 0) AS `isRecoveryAmountPossible`,
    tbl_f_trspestim.TARIFBASE AS `baseAmount`,
    tbl_f_trspestim.REMISEAPP AS `standardReductionPercent`,
    IF (tbl_f_trspspcreduc.ID IS NULL, 0, IF (tbl_f_trspspcreduc.ETAT <> "' . REDUCTIONSTATE_PENDING . '", tbl_f_trspspcreduc.REMEXACC, tbl_f_trspspcreduc.REMEXDEM)) AS `specialReductionPercent`,
    IFNULL(tbl_f_trspspcreduc.ETAT, "") AS `specialReductionStatus`,
    IF (tbl_f_trspestim.ETAT = "' . REDUCTIONSTATE_PENDING . '", 1, 0) AS `isNoSaved`,' . &_getHasErrorsQuery($countryId, 'estimate', $documentId) . '
FROM DEVIS tbl_f_estimate
INNER JOIN DETAILSDEVIS tbl_f_estimatedetail
ON tbl_f_estimatedetail.DEVISAUTO = tbl_f_estimate.DEVISAUTO
LEFT JOIN ' . $schemaName . '.TRANSPORT tbl_f_trspestim
ON tbl_f_trspestim.TYPE = "DEVIS" AND
   tbl_f_trspestim.VALEUR = tbl_f_estimatedetail.DETAILSDEVISAUTO AND
   tbl_f_trspestim.ETAT <> "' . REDUCTIONSTATE_CANCELED . '"
LEFT JOIN ' . $schemaName . '.REMISEEX tbl_f_trspspcreduc
ON tbl_f_trspspcreduc.ID = tbl_f_trspestim.REMISEEX
WHERE tbl_f_estimatedetail.DETAILSDEVISAUTO = ' . $documentId . ';';
    }
    elsif ($documentType eq 'contract')
    {
        $query = '
SELECT
    IFNULL(tbl_f_trspestim.ID, 0) AS `id`,
    -tbl_f_contract.CONTRATENLVTDP AS `isNoDelivery`,
    -tbl_f_contract.CONTRATMACHSUPALLER AS `isAddMachineDelivery`,
    tbl_f_contract.CONTRATAUTO_MACHSUPALLER AS `addMachineDeliveryLnkCtt.id`,
    tbl_f_contract_machsupal.CONTRATCODE AS `addMachineDeliveryLnkCtt.code`,
    tbl_f_contractamount.MOTRANSPORTDFR AS `deliveryAmount`,
    IF (tbl_f_contract.ETCODE <> "' . LOC::Contract::Rent::STATE_CANCELED . '" && tbl_f_contract.ETATFACTURE = "' . LOC::Contract::Rent::INVOICESTATE_NONE . '", 1, 0) AS `isDeliveryAmountPossible`,
    -tbl_f_contract.CONTRATENLVTRT AS `isNoRecovery`,
    -tbl_f_contract.CONTRATMACHSUPRETOUR AS `isAddMachineRecovery`,
    tbl_f_contract.CONTRATAUTO_MACHSUPRETOUR AS `addMachineRecoveryLnkCtt.id`,
    tbl_f_contract_machsupre.CONTRATCODE AS `addMachineRecoveryLnkCtt.code`,
    tbl_f_contractamount.MOTRANSPORTRFR AS `recoveryAmount`,
    IF (tbl_f_contract.ETCODE <> "' . LOC::Contract::Rent::STATE_CANCELED . '" && tbl_f_contract.ETATFACTURE IN ("' . LOC::Contract::Rent::INVOICESTATE_NONE . '", "' . LOC::Contract::Rent::INVOICESTATE_PARTIAL . '"), 1, 0) AS `isRecoveryAmountPossible`,
    tbl_f_trspestim.TARIFBASE AS `baseAmount`,
    tbl_f_trspestim.REMISEAPP AS `standardReductionPercent`,
    IFNULL(tbl_f_trspspcreduc.ID, 0) AS `specialReductionId`,
    IF (tbl_f_trspspcreduc.ID IS NULL, 0, IF (tbl_f_trspspcreduc.ETAT <> "' . REDUCTIONSTATE_PENDING . '", tbl_f_trspspcreduc.REMEXACC, tbl_f_trspspcreduc.REMEXDEM)) AS `specialReductionPercent`,
    tbl_f_trspspcreduc.LIBELLEFACTURE AS `specialReductionBillLabel`,
    IFNULL(tbl_f_trspspcreduc.ETAT, "") AS `specialReductionStatus`,
    IF (tbl_f_trspestim.ETAT = "' . REDUCTIONSTATE_PENDING . '", 1, 0) AS `isNoSaved`,' . &_getHasErrorsQuery($countryId, 'contract', $documentId) . '
FROM CONTRAT tbl_f_contract
INNER JOIN MONTANT tbl_f_contractamount
ON tbl_f_contract.MOAUTO = tbl_f_contractamount.MOAUTO
LEFT JOIN ' . $schemaName . '.TRANSPORT tbl_f_trspestim
ON tbl_f_trspestim.TYPE = "CONTRAT" AND
   tbl_f_trspestim.VALEUR =  tbl_f_contract.CONTRATAUTO AND
   tbl_f_trspestim.ETAT <> "' . REDUCTIONSTATE_CANCELED . '"
LEFT JOIN ' . $schemaName . '.REMISEEX tbl_f_trspspcreduc
ON tbl_f_trspspcreduc.ID = tbl_f_trspestim.REMISEEX
LEFT JOIN CONTRAT tbl_f_contract_machsupal
ON tbl_f_contract_machsupal.CONTRATAUTO = tbl_f_contract.CONTRATAUTO_MACHSUPALLER
LEFT JOIN CONTRAT tbl_f_contract_machsupre
ON tbl_f_contract_machsupre.CONTRATAUTO = tbl_f_contract.CONTRATAUTO_MACHSUPRETOUR
WHERE tbl_f_contract.CONTRATAUTO = ' . $documentId . ';';
    }

    my $tab = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);

    # Formatage des données
    $tab->{'id'}                      *= 1;

    $tab->{'isNoDelivery'}            *= 1;
    $tab->{'isAddMachineDelivery'}    *= 1;
    $tab->{'deliveryAmount'}          *= 1;
    $tab->{'isTransferDelivery'}       = 0;

    $tab->{'isNoRecovery'}            *= 1;
    $tab->{'isAddMachineRecovery'}    *= 1;
    $tab->{'recoveryAmount'}          *= 1;
    $tab->{'isTransferRecovery'}       = 0;

    $tab->{'hasErrors'}               *= 1;
    $tab->{'isNoSaved'}               *= 1;

    $tab->{'addMachineDeliveryLnkCtt.id'} = ($tab->{'addMachineDeliveryLnkCtt.id'} ? $tab->{'addMachineDeliveryLnkCtt.id'} * 1 : undef);
    $tab->{'addMachineDeliveryLnkCtt.url'} = ($tab->{'addMachineDeliveryLnkCtt.id'} ? &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $tab->{'addMachineDeliveryLnkCtt.id'}}) : '');
    $tab->{'addMachineRecoveryLnkCtt.id'} = ($tab->{'addMachineRecoveryLnkCtt.id'} ? $tab->{'addMachineRecoveryLnkCtt.id'} * 1 : undef);
    $tab->{'addMachineRecoveryLnkCtt.url'} = ($tab->{'addMachineRecoveryLnkCtt.id'} ? &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $tab->{'addMachineRecoveryLnkCtt.id'}}) : '');

    $tab->{'possibilities'} = {
        'estimation' => 0,
        'delivery'   => {
            'amount'     => 0,
            'self'       => 0,
            'addMachine' => 0
        },
        'recovery'   => {
            'amount'     => 0,
            'self'       => 0,
            'addMachine' => 0
        }
    };

    my $isDeliveryAmountPossible = ($tab->{'isDeliveryAmountPossible'} * 1);
    delete $tab->{'isDeliveryAmountPossible'};
    my $isRecoveryAmountPossible = ($tab->{'isRecoveryAmountPossible'} * 1);
    delete $tab->{'isRecoveryAmountPossible'};

    my $tabDeliveryInfos = &LOC::Transport::getDocumentDeliveryInfos($countryId, $documentType, $documentId);
    if ($tabDeliveryInfos)
    {
        # Y-a-t-il un transfert sur la livraison ?
        if ($tabDeliveryInfos->{'type'} eq LOC::Transport::TYPE_SITESTRANSFER)
        {
            $tab->{'isTransferDelivery'} = 1;
        }

        # Possibilités de modification des montants de la livraison
        if ($isDeliveryAmountPossible && !$tabDeliveryInfos->{'isLocked'})
        {
            $tab->{'possibilities'}->{'delivery'}->{'amount'} = 1;
        }

        # Possibilité de l'enlèvement et de la machine supp seulement si la livraison n'est pas réalisée
        if (!$tabDeliveryInfos->{'unloading'}->{'isDone'})
        {
            $tab->{'possibilities'}->{'delivery'}->{'self'}       = 1;
            $tab->{'possibilities'}->{'delivery'}->{'addMachine'} = 1;
        }
    }
    else
    {
        # En création de ligne de devis ou contrat
        $tab->{'possibilities'}->{'delivery'}->{'amount'} = 1;
        $tab->{'possibilities'}->{'delivery'}->{'self'} = 1;
        $tab->{'possibilities'}->{'delivery'}->{'addMachine'} = 1;
    }

    my $tabRecoveryInfos = &LOC::Transport::getDocumentRecoveryInfos($countryId, $documentType, $documentId);
    if ($tabRecoveryInfos)
    {
        # Y-a-t-il un transfert sur la récupération ?
        if ($tabRecoveryInfos->{'type'} eq LOC::Transport::TYPE_SITESTRANSFER)
        {
            $tab->{'isTransferRecovery'} = 1;
        }

        # Possibilités de modification des montants de la récupération
        if ($isRecoveryAmountPossible && !$tabRecoveryInfos->{'isLocked'})
        {
            $tab->{'possibilities'}->{'recovery'}->{'amount'} = 1;
        }

        # Possibilité de l'enlèvement et de la machine supp seulement si la récupération n'est pas réalisée
        if (!$tabRecoveryInfos->{'loading'}->{'isDone'})
        {
            $tab->{'possibilities'}->{'recovery'}->{'self'}       = 1;
            $tab->{'possibilities'}->{'recovery'}->{'addMachine'} = 1;
        }
    }
    else
    {
        # En création de ligne de devis ou contrat
        $tab->{'possibilities'}->{'recovery'}->{'amount'} = 1;
        $tab->{'possibilities'}->{'recovery'}->{'self'} = 1;
        $tab->{'possibilities'}->{'recovery'}->{'addMachine'} = 1;
    }

    if ($isDeliveryAmountPossible || $isRecoveryAmountPossible)
    {
        $tab->{'possibilities'}->{'estimation'} = 1;
    }


    # Récupération des informations sur l'estimation (en GESTIONTRANS) avec le service SOAP
    if ($tab->{'id'})
    {
        if ($flags & GETINFOS_SOAP)
        {
            # Service SOAP pour récupérer les informations de l'estimation transport
            sub SOAP::Transport::HTTP::Client::get_basic_credentials
            {
                my $tabUserInfos = &LOC::Session::getUserInfos();
                return ($tabUserInfos->{'login'} => $tabUserInfos->{'password'});
            };
            my $service = SOAP::Lite->uri('ws_get_trans')
                                    ->proxy(&LOC::Globals::get('tariffTrspServicesUrl'))
                                    ->ws_get_trans('', '', '', '', $tab->{'id'});
            if (!$service->fault)
            {
                # Récupération des resultats
                my $tabResult = $service->result;

                $tab->{'baseAmount'}          = $tabResult->{'pxbase'} * 1;
                $tab->{'size'}                = $tabResult->{'encombrement'} * 1;
                $tab->{'durationSlice'}       = $tabResult->{'duree'} * 1;
                $tab->{'customer.id'}         = $tabResult->{'client'} * 1;
                $tab->{'customer.class'}      = $tabResult->{'classe'};
                $tab->{'zone'}                = $tabResult->{'zone'} * 1;
                $tab->{'recoStandardPercent'} = $tabResult->{'pcremreco'} * 1;
                $tab->{'maxiStandardPercent'} = $tabResult->{'pcremmaxi'} * 1;
                if ($tabResult->{'pxbase'} > 0)
                {
                    $tab->{'standardReductionPercent'} = $tabResult->{'pcrem'} * 1;
                    $tab->{'standardReductionAmount'}  = ($tabResult->{'pxbase'} - $tabResult->{'pxrem'}) * 1;
                    $tab->{'specialReductionPercent'}  = $tabResult->{'pcremex'} * 1;
                    $tab->{'specialReductionAmount'}   = &LOC::Util::round($tabResult->{'pxremex'}, 0);
                    $tab->{'maxiStandardAmount'}       = &LOC::Util::round($tabResult->{'pxbase'} * ($tabResult->{'pcremmaxi'} / 100), 0);
                    $tab->{'reductionAmount'}          = ($tabResult->{'pxbase'} - $tabResult->{'pxfinal'}) * 1;
                    $tab->{'reductionPercent'}         = &LOC::Util::round(($tab->{'reductionAmount'} / $tabResult->{'pxbase'}) * 100, 0);
                }
                else
                {
                    $tab->{'standardReductionPercent'} = 0;
                    $tab->{'standardReductionAmount'}  = 0;
                    $tab->{'specialReductionPercent'}  = 0;
                    $tab->{'specialReductionAmount'}   = 0;
                    $tab->{'maxiStandardAmount'}       = 0;
                    $tab->{'reductionAmount'}          = 0;
                    $tab->{'reductionPercent'}         = 0;
                }
                $tab->{'specialReductionStatus'}    = (($tabResult->{'etatremex'} eq REDUCTIONSTATE_VALIDATED && $tab->{'specialReductionPercent'} == 0) ? '' : $tabResult->{'etatremex'});
                $tab->{'specialReductionId'}        = $tabResult->{'idremex'} * 1;
                $tab->{'amount'}                    = $tabResult->{'pxfinal'} * 1;
                $tab->{'specialReductionBillLabel'} = $tabResult->{'libfac'};
                $tab->{'autoValidated'}             = $tabResult->{'autovalidee'} * 1;
                $tab->{'numberOfMachines'}          = $tabResult->{'nbmac'} * 1;
            }
        }
        else
        {
            $tab->{'baseAmount'}                = &LOC::Util::round($tab->{'baseAmount'} * 1, 0);
            $tab->{'standardReductionPercent'}  = &LOC::Util::round($tab->{'standardReductionPercent'} * 1, 3);
            $tab->{'specialReductionId'}       *= 1;
            $tab->{'specialReductionPercent'}   = &LOC::Util::round($tab->{'specialReductionPercent'} * 1, 3);

            # Calcul du prix final
            $tab->{'amount'} = $tab->{'baseAmount'};
            $tab->{'amount'} = &LOC::Util::round($tab->{'amount'} - ($tab->{'amount'} * $tab->{'standardReductionPercent'} / 100), 0);
            $tab->{'amount'} = &LOC::Util::round($tab->{'amount'} - ($tab->{'amount'} * $tab->{'specialReductionPercent'} / 100), 0);
        }
    }

    return (wantarray ? %$tab : $tab);
}

# Function: resetSelfRecoveryAmountOnContractRestop
# Remise à zéro du montant de la récupération si il est supérieur à zéro et qu'il s'agit d'un enlèvement agence
# dans le cas d'une remise en arrêt d'un contrat (voir FA-13121)
#
# Parameters:
# string   $countryId  - Pays
# string   $contractId - Identifiant du contrat
# int      $userId     - Utilisateur responsable de la modification
# arrayref $tabErrors  - Tableau des erreurs
# hashref  $tabEndUpds - Mises à jour de fin
#
# Returns:
# int - Retourne 1 si une modification est effectuée, -1 sinon et 0 dans le cas où une erreur s'est produit
sub resetSelfRecoveryAmountOnContractRestop
{
    my ($countryId, $contractId, $userId, $tabErrors, $tabEndUpds) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    # Nom du schéma de base de données
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my $query = '
SELECT
    tbl_f_contract.MOAUTO AS `amount.id`,
    tbl_f_contractamount.MOTRANSPORTRFR AS `recoveryAmount`
FROM CONTRAT tbl_f_contract
INNER JOIN MONTANT tbl_f_contractamount
ON tbl_f_contract.MOAUTO = tbl_f_contractamount.MOAUTO
WHERE tbl_f_contract.CONTRATAUTO = ' . $contractId . ';';

    my $tab = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);

    my $amountId       = $tab->{'amount.id'} * 1;
    my $recoveryAmount = $tab->{'recoveryAmount'} * 1;

    # Si le montant de la récupération est > 0 ...
    if ($recoveryAmount > 0)
    {
        my $tabRecoveryInfos = &LOC::Transport::getDocumentRecoveryInfos($countryId, 'contract', $contractId);
        if (!$tabRecoveryInfos)
        {
            return 0;
        }

        # ... et que le transport de récupération est de type "enlevement agence"
        if ($tabRecoveryInfos->{'isSelf'} && $tabRecoveryInfos->{'to.type'} eq LOC::Transport::FROMTOTYPE_AGENCY)
        {
            # Alors on remet à zéro le montant de la récupération
            my $tabUpdates = {
                'MOTRANSPORTRFR' => 0,
                'MOTRANSPORTREU' => 0
            };
            if ($db->update('MONTANT', $tabUpdates, {'MOAUTO*' => $amountId}, $schemaName) == -1)
            {
                return 0;
            }

            # Ajout des traces de la modification du montant de la récupération
            my $tabTrace = {
                'schema'  => $schemaName,
                'code'    => LOC::Log::EventType::UPDRECOVERYAMOUNT,
                'old'     => {
                    'amount' => $recoveryAmount,
                },
                'new'     => {
                    'amount' => 0,
                },
                'content' => $recoveryAmount .
                             ' → ' .
                             0
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabTrace);

            return 1;
        }
    }

    return -1;
}


# Function: save
# Confirme l'estimation transport et la remise exceptionnelle et l'alerte
#
# Parameters:
# string   $countryId     - Pays
# string   $documentType  - Type de document ('estimateLine' => ligne de devis, 'contract' => Contrat)
# string   $documentId    - Id du document (ligne de devis ou contrat)
# hashref  $tabData       - Données à mettre à jour
# int      $userId        - Utilisateur responsable de la modification
# arrayref $tabErrors     - Tableau des erreurs
# hashref  $tabEndUpds    - Mises à jour de fin
#
# Returns:
# bool -
sub save
{
    my ($countryId, $documentType, $documentId, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    $tabErrors->{'fatal'} = [];
    my $fatalErrMsg = 'Erreur fatale: module LOC::Tariff::Transport, fonction save(), ligne %d, fichier "%s"%s.' . "\n";

    # Paramètres
    my $editMode = $tabData->{'editMode'};
    my $rights   = $tabData->{'rights'};


    # Récupération de l'id du client et de la zone de la ligne de devis ou du contrat
    my $customerId;
    my $agencyId;
    my $zone;
    my $documentValueDate;
    if ($documentType eq 'estimateLine')
    {
        # Ligne de devis
        $customerId = $tabData->{'estimateLine.estimate.customer.id'};
        $agencyId   = $tabData->{'estimateLine.estimate.agency.id'};
        $zone       = $tabData->{'estimateLine.estimate.zone'};

        # Date de valeur du devis
        $documentValueDate = &LOC::Estimate::Rent::getValueDate($countryId, $tabData->{'estimateLine.estimate.id'});
    }
    else
    {
        # Contrat
        $customerId = $tabData->{'contract.customer.id'};
        $agencyId   = $tabData->{'contract.agency.id'};
        $zone       = $tabData->{'contract.zone'};

        # Date de valeur du devis
        $documentValueDate = &LOC::Contract::Rent::getValueDate($countryId, $documentId);
    }

    # Récupération des informations de l'estimation
    my $tabInfos = &getInfos($countryId, $documentType, $documentId, GETINFOS_BASE);
    if (!$tabInfos)
    {
        print STDERR sprintf($fatalErrMsg, __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, 0x0001);
        return 0;
    }

    my $tabDeliveryPsbls = $tabInfos->{'possibilities'}->{'delivery'};
    my $tabRecoveryPsbls = $tabInfos->{'possibilities'}->{'recovery'};

    # Gère-t-on les estimations transport ?
    my $isTrspTarification = &LOC::Characteristic::getAgencyValueByCode('TRSPTARIF', $agencyId);
    # Montant par défaut pour la machine supplémentaire
    my $addMachinePrice    = &LOC::Characteristic::getAgencyValueByCode('ADDMACH', $agencyId, $documentValueDate);
    # Montant minimum pour la machine supplémentaire
    my $addMachineMinPrice = &LOC::Characteristic::getAgencyValueByCode('ADDMACHMIN', $agencyId, $documentValueDate);


    my $id     = 0;
    my $amount = 0;
    my $specialReductionId = 0;
    if ($isTrspTarification)
    {
        $id     = (exists $tabData->{'id'} ? $tabData->{'id'} : $tabInfos->{'id'});
        $amount = (exists $tabData->{'amount'} ? $tabData->{'amount'} : $tabInfos->{'amount'});
    }

    # - Livraison
    my $oldIsNoDelivery         = $tabInfos->{'isNoDelivery'};
    my $isNoDelivery            = (exists $tabData->{'isNoDelivery'} ? ($tabData->{'isNoDelivery'} ? 1 : 0) : $oldIsNoDelivery);
    my $oldIsTransferDelivery   = $tabInfos->{'isTransferDelivery'};
    my $isTransferDelivery      = (exists $tabData->{'isTransferDelivery'} ? ($tabData->{'isTransferDelivery'} ? 1 : 0) : $oldIsTransferDelivery);
    my $oldIsAddMachineDelivery = $tabInfos->{'isAddMachineDelivery'};
    my $isAddMachineDelivery    = (exists $tabData->{'isAddMachineDelivery'} ? ($tabData->{'isAddMachineDelivery'} ? 1 : 0) : $oldIsAddMachineDelivery);
    my $oldDeliveryAmount       = $tabInfos->{'deliveryAmount'};
    my $deliveryAmount          = (exists $tabData->{'deliveryAmount'} ? $tabData->{'deliveryAmount'} * 1 : $oldDeliveryAmount);
    my $isLinkedContract  = (exists $tabData->{'isLinkedContract'} ? ($tabData->{'isLinkedContract'} ? 1 : 0) : 0);

    if ($tabDeliveryPsbls->{'amount'} && !exists $tabData->{'deliveryAmount'})
    {
        # Changement de type std ou enlvt
        # Changement de machine sup
        if ($oldIsNoDelivery != $isNoDelivery || $oldIsAddMachineDelivery != $isAddMachineDelivery)
        {
            $deliveryAmount = ($isNoDelivery && !$isTransferDelivery ? 0 : ($isAddMachineDelivery ? $addMachinePrice : $amount));
        }

        # dans le cas d'un enlèvement sans transfert (ou du contrat lié d'une reprise sur chantier)
        if (exists $tabData->{'isTransferDelivery'} && $isNoDelivery &&
                (!$tabData->{'isTransferDelivery'} || $isLinkedContract))
        {
            $deliveryAmount = 0;
        }
    }
    # - Récupération
    my $oldIsNoRecovery         = $tabInfos->{'isNoRecovery'};
    my $isNoRecovery            = (exists $tabData->{'isNoRecovery'} ? ($tabData->{'isNoRecovery'} ? 1 : 0) : $oldIsNoRecovery);
    my $oldIsTransferRecovery   = $tabInfos->{'isTransferRecovery'};
    my $isTransferRecovery      = (exists $tabData->{'isTransferRecovery'} ? ($tabData->{'isTransferRecovery'} ? 1 : 0) : $oldIsTransferRecovery);
    my $oldIsAddMachineRecovery = $tabInfos->{'isAddMachineRecovery'};
    my $isAddMachineRecovery    = (exists $tabData->{'isAddMachineRecovery'} ? ($tabData->{'isAddMachineRecovery'} ? 1 : 0) : $oldIsAddMachineRecovery);
    my $oldRecoveryAmount = $tabInfos->{'recoveryAmount'};
    my $recoveryAmount    = (exists $tabData->{'recoveryAmount'} ? $tabData->{'recoveryAmount'} * 1 : $oldRecoveryAmount);
    my $isLinkedContract  = (exists $tabData->{'isLinkedContract'} ? ($tabData->{'isLinkedContract'} ? 1 : 0) : 0);

    if ($tabRecoveryPsbls->{'amount'} && !exists $tabData->{'recoveryAmount'})
    {
        # Changement de type std ou enlvt
        # Changement de machine sup
        if ($oldIsNoRecovery != $isNoRecovery || $oldIsAddMachineRecovery != $isAddMachineRecovery)
        {
            $recoveryAmount = ($isNoRecovery && !$isTransferRecovery ? 0 : ($isAddMachineRecovery ? $addMachinePrice : $amount));
        }

        # dans le cas d'un enlèvement sans transfert (ou du contrat lié d'une reprise sur chantier)
        if (exists $tabData->{'isTransferRecovery'} && $isNoRecovery &&
               (!$tabData->{'isTransferRecovery'} || $isLinkedContract))
        {
            $recoveryAmount = 0;
        }
    }


    my $oldAddMachineDeliveryLnkCtt = {};
    my $addMachineDeliveryLnkCtt    = {};
    my $oldAddMachineRecoveryLnkCtt = {};
    my $addMachineRecoveryLnkCtt    = {};
    if ($documentType eq 'contract')
    {
        $oldAddMachineDeliveryLnkCtt = {
            'id'   => $tabInfos->{'addMachineDeliveryLnkCtt.id'},
            'code' => $tabInfos->{'addMachineDeliveryLnkCtt.code'}
        };
        $addMachineDeliveryLnkCtt = {
            'id'   => ($isAddMachineDelivery ? (exists $tabData->{'addMachineDeliveryLnkCtt.id'} ? ($tabData->{'addMachineDeliveryLnkCtt.id'} ? $tabData->{'addMachineDeliveryLnkCtt.id'} * 1 : undef) : $oldAddMachineDeliveryLnkCtt->{'id'}) : undef),
            'code' => ''
        };

        $oldAddMachineRecoveryLnkCtt = {
            'id'   => $tabInfos->{'addMachineRecoveryLnkCtt.id'},
            'code' => $tabInfos->{'addMachineRecoveryLnkCtt.code'}
        };
        $addMachineRecoveryLnkCtt = {
            'id'   => ($isAddMachineRecovery ? (exists $tabData->{'addMachineRecoveryLnkCtt.id'} ? ($tabData->{'addMachineRecoveryLnkCtt.id'} ? $tabData->{'addMachineRecoveryLnkCtt.id'} * 1 : undef) : $oldAddMachineRecoveryLnkCtt->{'id'}) : undef),
            'code' => ''
        };
    }


    # ---------------------------------------------
    # VERIFICATIONS DES POSSIBILITEES ET DROITS
    # ---------------------------------------------

    if ($editMode eq 'update')
    {
        # Vérification des possibilitées
        if ((!$tabDeliveryPsbls->{'amount'} && $oldDeliveryAmount != $deliveryAmount) ||
            (!$tabDeliveryPsbls->{'self'} && $oldIsNoDelivery != $isNoDelivery) ||
            (!$tabDeliveryPsbls->{'addMachine'} && $oldIsAddMachineDelivery != $isAddMachineDelivery) ||
            (!$tabRecoveryPsbls->{'amount'} && $oldRecoveryAmount != $recoveryAmount) ||
            (!$tabRecoveryPsbls->{'self'} && $oldIsNoRecovery != $isNoRecovery) ||
            (!$tabRecoveryPsbls->{'addMachine'} && $oldIsAddMachineRecovery != $isAddMachineRecovery))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }

        # Vérification des droits
        if (defined $rights &&
            (($oldDeliveryAmount != $deliveryAmount || $oldRecoveryAmount != $recoveryAmount) && !&_hasRight($rights, 'amount') ||
             ($oldIsNoDelivery != $isNoDelivery || $oldIsNoRecovery != $isNoRecovery) && !&_hasRight($rights, 'self') ||
             ($oldIsAddMachineDelivery != $isAddMachineDelivery || $oldIsAddMachineRecovery != $isAddMachineRecovery) && !&_hasRight($rights, 'addMachine'))
           )
        {
            push(@{$tabErrors->{'fatal'}}, 0x0003);
            return 0;
        }
    }



    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = 'GESTIONTRANS' . $countryId;
    my $schemaNameLoc = &LOC::Db::getSchemaName($countryId);


    # -------------------------------
    # VERIFICATIONS DE COHERENCE
    # -------------------------------

    if ($isTrspTarification)
    {
        if (!$id)
        {
            push(@{$tabErrors->{'fatal'}}, 0x0100);
            return 0;
        }

        # Récupération de l'id de remise exceptionnelle si il y en a une
        my $query = '
SELECT
    tbl_f_trspestim.REMISEEX AS `specialReduction.id`,
    tbl_f_trspestim.CLIENT  AS `customer.id`,
    tbl_f_trspestim.ZONE  AS `zone`
FROM ' . $schemaName . '.TRANSPORT tbl_f_trspestim
WHERE tbl_f_trspestim.ID = ' . $id . ';';
        my $tabResult = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
        if (!$tabResult)
        {
            # Erreur fatale !
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }

        $specialReductionId      = $tabResult->{'specialReduction.id'};
        my $estimationCustomerId = $tabResult->{'customer.id'} * 1;
        my $estimationZone       = $tabResult->{'zone'} * 1;

        if ($estimationCustomerId != $customerId)
        {
            push(@{$tabErrors->{'fatal'}}, 0x0101);
        }
        if ($estimationZone != $zone)
        {
            push(@{$tabErrors->{'fatal'}}, 0x0102);
        }
    }

    # - Vérifications sur la livraison
    if ($isNoDelivery)
    {
        # Machine supplémentaire activée sur un enlèvement sur place
        # - OU -
        # Montant positif pour un enlèvement sur place
        if (($tabDeliveryPsbls->{'addMachine'} && $isAddMachineDelivery) ||
            ($tabDeliveryPsbls->{'amount'} && !$isTransferDelivery && $deliveryAmount > 0))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }
    else
    {
        if ($isAddMachineDelivery)
        {
            # Forfait machine supplémentaire aller inférieur à la valeur minimale
            if ($tabDeliveryPsbls->{'amount'} && $deliveryAmount < $addMachineMinPrice)
            {
                push(@{$tabErrors->{'fatal'}}, 0x0107);
            }
        }
        else
        {
            # Montant aller inférieur à l'estimation transport
            if ($isTrspTarification && $tabDeliveryPsbls->{'amount'} && $deliveryAmount < $amount)
            {
                push(@{$tabErrors->{'fatal'}}, 0x0103);
            }
        }
    }

    # - Vérifications sur la récupération
    if ($isNoRecovery)
    {
        # Machine supplémentaitre activée sur un enlèvement sur place
        # - OU -
        # Montant positif pour un enlèvement sur place
        if (($tabRecoveryPsbls->{'addMachine'} && $isAddMachineRecovery) ||
            ($tabRecoveryPsbls->{'amount'} && !$isTransferRecovery && $recoveryAmount > 0))
        {
            push(@{$tabErrors->{'fatal'}}, 0x0001);
            return 0;
        }
    }
    else
    {
        if ($isAddMachineRecovery)
        {
            # Forfait machine supplémentaire retour inférieur à la valeur minimale
            if ($tabRecoveryPsbls->{'amount'} && $recoveryAmount < $addMachineMinPrice)
            {
                push(@{$tabErrors->{'fatal'}}, 0x0108);
            }
        }
        else
        {
            # Montant retour inférieur à l'estimation transport
            if ($isTrspTarification && $tabRecoveryPsbls->{'amount'} && $recoveryAmount < $amount)
            {
                push(@{$tabErrors->{'fatal'}}, 0x0104);
            }
        }
    }


    # - Vérification des contrats liés pour le contrat
    if ($documentType eq 'contract')
    {
        my @tabIds = ();
        if ($addMachineDeliveryLnkCtt->{'id'})
        {
            push(@tabIds, $addMachineDeliveryLnkCtt->{'id'});
        }
        if ($addMachineRecoveryLnkCtt->{'id'})
        {
            push(@tabIds, $addMachineRecoveryLnkCtt->{'id'});
        }

        if (@tabIds > 0)
        {
            # Les codes contrats doivent être valides et différents du code contrat courant
            my $query = '
SELECT
    CONTRATAUTO,
    CONTRATCODE
FROM CONTRAT
WHERE CONTRATAUTO IN (' . &LOC::Util::join(', ', \@tabIds) . ')
AND CONTRATAUTO <> ' . $documentId . '
AND CONTRATOK = -1;';
            my $tabContracts = $db->fetchPairs($query);

            $addMachineDeliveryLnkCtt->{'code'} = $tabContracts->{$addMachineDeliveryLnkCtt->{'id'}};
            if ($addMachineDeliveryLnkCtt->{'id'} && $addMachineDeliveryLnkCtt->{'code'} eq '')
            {
                push(@{$tabErrors->{'fatal'}}, 0x0105);
            }

            $addMachineRecoveryLnkCtt->{'code'} = $tabContracts->{$addMachineRecoveryLnkCtt->{'id'}};
            if ($addMachineRecoveryLnkCtt->{'id'} && $addMachineRecoveryLnkCtt->{'code'} eq '')
            {
                push(@{$tabErrors->{'fatal'}}, 0x0106);
            }
        }
    }


    # Sauvegarde en base de données
    if (@{$tabErrors->{'fatal'}} > 0)
    {
        return 0;
    }





    # ----------------------
    # HISTORISATION
    # ----------------------

    if (defined $tabEndUpds && $editMode eq 'update')
    {
        # - Pour la livraison
        # -- Montant
        if ($deliveryAmount != $oldDeliveryAmount)
        {
            my $tabTrace = {
                'schema'  => $schemaNameLoc,
                'code'    => LOC::Log::EventType::UPDDELIVERYAMOUNT,
                'old'     => {
                    'amount' => $oldDeliveryAmount,
                },
                'new'     => {
                    'amount' => $deliveryAmount,
                },
                'content' => $oldDeliveryAmount .
                             ' → ' .
                             $deliveryAmount
            };
    
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }
        # -- Machine supplémentaire
        if ($oldIsAddMachineDelivery != $isAddMachineDelivery || $oldAddMachineDeliveryLnkCtt->{'id'} != $addMachineDeliveryLnkCtt->{'id'})
        {
            # Formatage du contenu
            my $oldContent = '';
            if ($oldIsAddMachineDelivery)
            {
                $oldContent .= 'Mach. supp.';
                if ($oldAddMachineDeliveryLnkCtt->{'id'})
                {
                    $oldContent .= ' de ' . $oldAddMachineDeliveryLnkCtt->{'code'};
                }
            }
            else
            {
                $oldContent .= '';
            }

            my $newContent = '';
            if ($isAddMachineDelivery)
            {
                $newContent .= 'Mach. supp.';
                if ($addMachineDeliveryLnkCtt->{'id'})
                {
                    $newContent .= ' de ' . $addMachineDeliveryLnkCtt->{'code'};
                }
            }
            else
            {
                $newContent .= '';
            }

            my $tabTrace = {
                'schema'  => $schemaNameLoc,
                'code'    => LOC::Log::EventType::UPDADDMACHINEDELIVERY,
                'old'     => {
                    'isAddMachine'     => $oldIsAddMachineDelivery,
                    'addMachineLnkCtt' => $oldAddMachineDeliveryLnkCtt->{'code'}
                },
                'new'     => {
                    'isAddMachine'     => $isAddMachineDelivery,
                    'addMachineLnkCtt' => $addMachineDeliveryLnkCtt->{'code'}
                },
                'content' => $oldContent . ' → ' . $newContent
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }

        # - Pour la récupération
        # -- Montant
        if ($recoveryAmount != $oldRecoveryAmount)
        {
            my $tabTrace = {
                'schema'  => $schemaNameLoc,
                'code'    => LOC::Log::EventType::UPDRECOVERYAMOUNT,
                'old'     => {
                    'amount' => $oldRecoveryAmount,
                },
                'new'     => {
                    'amount' => $recoveryAmount,
                },
                'content' => $oldRecoveryAmount .
                             ' → ' .
                             $recoveryAmount
            };
    
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }
        # -- Machine supplémentaire
        if ($oldIsAddMachineRecovery != $isAddMachineRecovery || $oldAddMachineRecoveryLnkCtt->{'id'} != $addMachineRecoveryLnkCtt->{'id'})
        {
            # Formatage du contenu
            my $oldContent = '';
            if ($oldIsAddMachineRecovery)
            {
                $oldContent .= 'Mach. supp.';
                if ($oldAddMachineRecoveryLnkCtt->{'id'})
                {
                    $oldContent .= ' de ' . $oldAddMachineRecoveryLnkCtt->{'code'};
                }
            }
            else
            {
                $oldContent .= '';
            }

            my $newContent = '';
            if ($isAddMachineRecovery)
            {
                $newContent .= 'Mach. supp.';
                if ($addMachineRecoveryLnkCtt->{'id'})
                {
                    $newContent .= ' de ' . $addMachineRecoveryLnkCtt->{'code'};
                }
            }
            else
            {
                $newContent .= '';
            }

            my $tabTrace = {
                'schema'  => $schemaNameLoc,
                'code'    => LOC::Log::EventType::UPDADDMACHINERECOVERY,
                'old'     => {
                    'isAddMachine'     => $oldIsAddMachineRecovery,
                    'addMachineLnkCtt' => $oldAddMachineRecoveryLnkCtt->{'code'}
                },
                'new'     => {
                    'isAddMachine'     => $isAddMachineRecovery,
                    'addMachineLnkCtt' => $addMachineRecoveryLnkCtt->{'code'}
                },
                'content' => $oldContent . ' → ' . $newContent
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $documentType, $documentId, $tabTrace);
        }
    }


    # ------------------
    # ENREGISTREMENT
    # ------------------

    my $isDeliveryEditPossible = $tabDeliveryPsbls->{'amount'} || $tabDeliveryPsbls->{'self'} || $tabDeliveryPsbls->{'addMachine'};
    my $isRecoveryEditPossible = $tabRecoveryPsbls->{'amount'} || $tabRecoveryPsbls->{'self'} || $tabRecoveryPsbls->{'addMachine'};


    if ($isDeliveryEditPossible || $isRecoveryEditPossible || $tabInfos->{'possibilities'}->{'estimation'})
    {
        # Récupérer le taux de conversion de la monnaie locale en Euros
        my $euroRate = &LOC::Country::getEuroRate($countryId);

        if (!$hasTransaction)
        {
            # Démarre la transaction
            $db->beginTransaction();
        }

        # Mise à jour de la ligne de devis ou du contrat
        if ($documentType eq 'estimateLine')
        {
            # Mise à jour de la ligne de devis
            my $tabUpdates = {};
            if ($tabDeliveryPsbls->{'amount'})
            {
                $tabUpdates->{'DETAILSDEVISTRD'}          = $deliveryAmount;
                $tabUpdates->{'DETAILSDEVISTRDEU'}        = $deliveryAmount * $euroRate;
            }
            if ($tabDeliveryPsbls->{'self'})
            {
                $tabUpdates->{'DETAILSDEVISENLALLER'}     = $isNoDelivery;
            }
            if ($tabDeliveryPsbls->{'addMachine'})
            {
                $tabUpdates->{'DETAILSDEVISMACHSUPALLER'} = -$isAddMachineDelivery;
            }

            if ($tabRecoveryPsbls->{'amount'})
            {
                $tabUpdates->{'DETAILSDEVISTRR'}           = $recoveryAmount;
                $tabUpdates->{'DETAILSDEVISTRREU'}         = $recoveryAmount * $euroRate;
            }
            if ($tabRecoveryPsbls->{'self'})
            {
                $tabUpdates->{'DETAILSDEVISENLRETOUR'}     = $isNoRecovery;
            }
            if ($tabRecoveryPsbls->{'addMachine'})
            {
                $tabUpdates->{'DETAILSDEVISMACHSUPRETOUR'} = -$isAddMachineRecovery;
            }

            if (keys %$tabUpdates > 0 && $db->update('DETAILSDEVIS', $tabUpdates, {'DETAILSDEVISAUTO*' => $documentId}) == -1)
            {
                print STDERR sprintf($fatalErrMsg, __LINE__, __FILE__, '');
                push(@{$tabErrors->{'fatal'}}, 0x0001);
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return 0;
            }
        }
        else
        {
            # Mise à jour du contrat
            my @tabSets = ();
            if ($tabDeliveryPsbls->{'amount'})
            {
                push(@tabSets, 'tbl_f_contractamount.MOTRANSPORTDFR = ' . $db->quote($deliveryAmount));
                push(@tabSets, 'tbl_f_contractamount.MOTRANSPORTDEU = ' . $db->quote($deliveryAmount * $euroRate));
            }
            if ($tabDeliveryPsbls->{'self'})
            {
                push(@tabSets, 'tbl_f_contract.CONTRATENLVTDP = ' . -$isNoDelivery);
            }
            if ($tabDeliveryPsbls->{'addMachine'})
            {
                push(@tabSets, 'tbl_f_contract.CONTRATMACHSUPALLER = ' . -$isAddMachineDelivery);
                push(@tabSets, 'tbl_f_contract.CONTRATAUTO_MACHSUPALLER = ' . $db->quote($addMachineDeliveryLnkCtt->{'id'}));
            }

            if ($tabRecoveryPsbls->{'amount'})
            {
                push(@tabSets, 'tbl_f_contractamount.MOTRANSPORTRFR = ' . $db->quote($recoveryAmount));
                push(@tabSets, 'tbl_f_contractamount.MOTRANSPORTREU = ' . $db->quote($recoveryAmount * $euroRate));
            }
            if ($tabRecoveryPsbls->{'self'})
            {
                push(@tabSets, 'tbl_f_contract.CONTRATENLVTRT = ' . -$isNoRecovery);
            }
            if ($tabRecoveryPsbls->{'addMachine'})
            {
                push(@tabSets, 'tbl_f_contract.CONTRATMACHSUPRETOUR = ' . -$isAddMachineRecovery);
                push(@tabSets, 'tbl_f_contract.CONTRATAUTO_MACHSUPRETOUR = ' . $db->quote($addMachineRecoveryLnkCtt->{'id'}));
            }

            if (@tabSets > 0)
            {
                my $query = '
UPDATE MONTANT tbl_f_contractamount
INNER JOIN CONTRAT tbl_f_contract
ON tbl_f_contractamount.MOAUTO = tbl_f_contract.MOAUTO
SET ' . join(', ', @tabSets) . '
WHERE tbl_f_contract.CONTRATAUTO = ' . $documentId . ';';

                # Exécution de la requête
                my $resultObj = $db->query($query);
                if (!$resultObj)
                {
                    print STDERR sprintf($fatalErrMsg, __LINE__, __FILE__, '');
                    push(@{$tabErrors->{'fatal'}}, 0x0001);
                    # Annule la transaction
                    if (!$hasTransaction)
                    {
                        $db->rollBack();
                    }
                    return 0;
                }
                $resultObj->close();
            }
        }

        # Mise à jour de l'estimation transport
        if ($isTrspTarification)
        {
            # update de la ligne de transport
            my $result = $db->update('TRANSPORT', {'VALEUR' => $documentId, 'ETAT' => REDUCTIONSTATE_VALIDATED}, {'ID*' => $id}, $schemaName);
            if (!$result)
            {
                print STDERR sprintf($fatalErrMsg, __LINE__, __FILE__, '');
                push(@{$tabErrors->{'fatal'}}, 0x0001);
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return 0;
            }

            # Si il y a une remise exceptionnelle...
            if ($specialReductionId)
            {
                my $query = '
UPDATE ' . $schemaName . '.ALERTE A
INNER JOIN ' . $schemaName . '.REMISEEX R
ON A.ID = R.ALERTE AND R.ID = ' . $specialReductionId . '
SET A.DATE = NOW(), A.ETAT = "ALE01"
WHERE A.ETAT = "ALE03";';
                my $resultObj = $db->query($query);
                if (!$resultObj)
                {
                    print STDERR sprintf($fatalErrMsg, __LINE__, __FILE__, '');
                    push(@{$tabErrors->{'fatal'}}, 0x0001);
                    # Annule la transaction
                    if (!$hasTransaction)
                    {
                        $db->rollBack();
                    }
                    return 0;
                }
            }
        }

        # Valide la transaction
        if (!$hasTransaction)
        {
            if (!$db->commit())
            {
                $db->rollBack();
                return 0;
            }
        }
    }

    return 1;
}


sub _getHasErrorsQuery
{
    my ($countryId, $documentType, $documentId) = @_;

    # Tarification transport activée ou non ?
    my $isTrspTarification = &LOC::Characteristic::getCountryValueByCode('TRSPTARIF', $countryId);
    if (!$isTrspTarification)
    {
        return '
    0 AS `hasErrors`';
    }

    my $baseAmountQuery = 'tbl_f_trspestim.TARIFBASE';
    my $stdReducAmountQuery = 'ROUND(' . $baseAmountQuery . ' * tbl_f_trspestim.REMISEAPP / 100, 0)';
    my $spcReducPercentQuery = 'IF (tbl_f_trspspcreduc.ID IS NULL, 0, IF (tbl_f_trspspcreduc.ETAT <> "' . REDUCTIONSTATE_PENDING . '", tbl_f_trspspcreduc.REMEXACC, tbl_f_trspspcreduc.REMEXDEM))';
    my $spcReducAmountQuery = 'ROUND((' . $baseAmountQuery . ' - ' . $stdReducAmountQuery . ') * ' . $spcReducPercentQuery . ' / 100, 0)';
    my $amountQuery = '(' . $baseAmountQuery . ' - ' . $stdReducAmountQuery . ' - ' . $spcReducAmountQuery . ')';

    if ($documentType eq 'estimate')
    {
        # Devis et ligne de devis
        return '
    IF (tbl_f_estimatedetail.DETAILSDEVISOK = 0 AND (
        tbl_f_trspestim.ID IS NULL OR
        tbl_f_estimate.CLAUTO <> tbl_f_trspestim.CLIENT OR
        tbl_f_estimate.ZONE   <> tbl_f_trspestim.ZONE OR
        (tbl_f_estimatedetail.DETAILSDEVISENLALLER = 0 AND
         tbl_f_estimatedetail.DETAILSDEVISMACHSUPALLER = 0 AND
         tbl_f_estimatedetail.DETAILSDEVISTRD < ' . $amountQuery . ') OR
        (tbl_f_estimatedetail.DETAILSDEVISENLRETOUR = 0 AND
         tbl_f_estimatedetail.DETAILSDEVISMACHSUPRETOUR = 0 AND
         tbl_f_estimatedetail.DETAILSDEVISTRR < ' . $amountQuery . ')), 1, 0) AS `hasErrors`';
    }
    else
    {
        # Contrat
        return '
    IF (tbl_f_trspestim.ID IS NULL OR
        tbl_f_contract.CLAUTO <> tbl_f_trspestim.CLIENT OR
        tbl_f_contract.ZONE   <> tbl_f_trspestim.ZONE OR
        (tbl_f_contract.ETATFACTURE = "' . LOC::Contract::Rent::INVOICESTATE_NONE . '" AND
         tbl_f_contract.CONTRATENLVTDP = 0 AND
         tbl_f_contract.CONTRATMACHSUPALLER = 0 AND
         tbl_f_contractamount.MOTRANSPORTDFR < ' . $amountQuery . ') OR
        (tbl_f_contract.ETATFACTURE IN ("' . LOC::Contract::Rent::INVOICESTATE_NONE . '", "' . LOC::Contract::Rent::INVOICESTATE_PARTIAL . '") AND
         tbl_f_contract.CONTRATENLVTRT = 0 AND
         tbl_f_contract.CONTRATMACHSUPRETOUR = 0 AND
         tbl_f_contractamount.MOTRANSPORTRFR < ' . $amountQuery . '), 1, 0) AS `hasErrors`';
    }
}


# Function: _hasRight
# Vérifier le droit à un champ
#
# Parameters:
# hashref $tab  - Tableau des droits
# string  $name - Nom du droit
#
# Returns:
# bool
sub _hasRight
{
    my ($tab, $name) = @_;

    return (ref($tab) eq 'HASH' ? ($tab->{$name} ? 1 : 0) : (index('|' . $tab . '|', '|' . $name . '|') == -1 ? 0 : 1));
}

1;