use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::MachinesFamily
# Module permettant d'obtenir les informations sur les familles de machines
package LOC::MachinesFamily;


# Constants: Récupération d'informations sur les modèles
# GETINFOS_ALL  - Récupération de l'ensemble des informations
# GETINFOS_BASE - Récupération des informations de base
use constant
{
    GETINFOS_ALL            => 0xFFFFFFFF,
    GETINFOS_BASE           => 0x00000000,
    GETINFOS_BUSINESSFAMILY => 0x00000001,
    GETINFOS_ACCOUNTFAMILY  => 0x00000002,
    GETINFOS_COUNTRYLABELS  => 0x00000004
};


# Constants: Energies de fonctionnement des familles de machines
# ENERGY_DIESEL - Diesel
# ENERGY_GAS - Gaz
# ENERGY_DUAL - Bi-énergie
# ENERGY_HYBRID - Hybride
# ENERGY_ELECTRIC - Electrique
# ENERGY_MANUAL - Manuelle
# ENERGY_UNDEFINED - Indéfini

use constant
{
    ENERGY_DIESEL    => 'DIESEL',
    ENERGY_GAS       => 'GAZ',
    ENERGY_DUAL      => 'BI-EN',
    ENERGY_HYBRID    => 'HYB',
    ENERGY_ELECTRIC  => 'ELECT',
    ENERGY_MANUAL    => 'MANIV',
    ENERGY_UNDEFINED => 'n/a'
};

# Constants: Suffixes utilisés dans les libellés de famille de machines
use constant
{
    SUFFIX_SUBRENT => ' - SL'
};


use strict;

# Modules internes
use LOC::Db;
use LOC::AccountFamily;
use LOC::BusinessFamily;
use LOC::EndUpdates;
use LOC::Country;


my $fatalErrMsg = 'Erreur fatale: module LOC::MachinesFamily, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";

# Liste des erreurs:
#
# 0x0001 => Erreur fatale
# 0x0002 => Champs obligatoires non renseignés


# Function: getFuelEnergiesList
# Retourne la liste des énergies nécéssitant du carburant
#
# Returns:
# hash|hashref|0 - Liste des énergies à carburant
sub getFuelEnergiesList
{
    return [ENERGY_DIESEL, ENERGY_DUAL, ENERGY_HYBRID];

}


# Function: getEnergiesList
# Retourne la liste des énergies
#
# Returns:
# hash|hashref|0 - Liste des énergies à carburant
sub getEnergiesList
{
    return [ENERGY_UNDEFINED, ENERGY_DIESEL, ENERGY_ELECTRIC, ENERGY_DUAL, ENERGY_HYBRID, ENERGY_GAS, ENERGY_MANUAL];

}


# Function: getInfos
# Retourne les infos d'une famille de machines
# 
# Contenu du hashage :
# - id => identifiant de la famille de machines
# - label => libellé
# - energy => énergie
# - elevation => élévation
# - height => hauteur
# - family => famille
# - businessFamily.id => identifiant de la famille commerciale
# - accountFamily.id => identifiant de la famille comptable
# - targetPrice => prix cible (obsolète)
# - packageZ1 => forfait zone 1 (obsolète)
# - packageZ2 => forfait zone 2 (obsolète)
# - pricePerDay => prix par jour (obsolète)
# - pricePerWeek => prix par semaine (obsolète)
# - indMonth => pas d'information sur ce champ (obsolète)
# - minMonth => pas d'information sur ce champ (obsolète)
# - load => charge
#
#
# Parameters:
# string  $countryId  - Identifiant du pays
# string  $id         - Identifiant de la famille de machines
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getInfos
{
    my ($countryId, $id, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    my $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);
    return ($tab && exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Retourne la liste des familles de machines
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres ('businessFamily' : liste des familles commerciales,
#                       'accountFamily' : liste des familles comptables)
# int     $flags      - Flags
#
# Returns:
# hash|hashref|0 - Liste des familles de machines
sub getList
{
    my ($countryId, $format, $tabFilters, $flags) = @_;
    unless (defined $flags)
    {
        $flags = GETINFOS_BASE;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Filtres
    my $whereQuery = '';

    if ($tabFilters->{'id'} ne '')
    {
        $whereQuery .= ' AND tbl_p_machinesfamily.FAMAAUTO IN (' . $db->quote($tabFilters->{'id'}) . ')';
    }

    if ($whereQuery ne '')
    {
        $whereQuery = ' AND (' . substr($whereQuery, 4) . ')';
    }

    # Récupération des libellés des pays
    my $labels = '';
    if ($flags & GETINFOS_COUNTRYLABELS)
    {
        $labels = '
    CONCAT_WS(",", ';
        my %tabCountry = &LOC::Country::getList(LOC::Util::GETLIST_PAIRS);
        foreach my $country (keys %tabCountry)
        {
            $labels .= '
            (SELECT CONCAT("' . $country . ':", FAMAMODELE) FROM ' . &LOC::Db::getSchemaName($country) . '.FAMILLEMACHINE WHERE FAMAAUTO = tbl_p_machinesfamily.FAMAAUTO),';
        }
        $labels = substr($labels, 0, -1) . '
        ) AS `labels`,';
    }

    my $query;
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    tbl_p_machinesfamily.FAMAAUTO AS `id`,
    tbl_p_machinesfamily.FAMAMODELE AS `label`,';
        if ($labels)
        {
            $query .= $labels;
        }
        $query .= '
    CONCAT_WS(" ", tbl_p_machinesfamily.FAMAMODELE, tbl_p_machinesfamily.FAMAENERGIE, tbl_p_machinesfamily.FAMAELEVATION) AS `fullName`,
    tbl_p_machinesfamily.FAMAENERGIE AS `energy`,
    tbl_p_machinesfamily.FAMAELEVATION AS `elevation`,
    tbl_p_machinesfamily.FAMAHAUTEUR AS `height`,
    tbl_p_machinesfamily.FAMILLE AS `family`,
    tbl_p_machinesfamily.FACOMAUTO AS `businessFamily.id`,';

        # Famille commerciale
        if ($flags & GETINFOS_BUSINESSFAMILY)
        {
            $query .= '
    tbl_p_businessfamily.FACOMLIBELLE AS `businessFamily.label`,';
        }

        $query .= '
    tbl_p_machinesfamily.FAMACOMPTAAUTO AS `accountFamily.id`,';

        # Famille comptable
        if ($flags & GETINFOS_ACCOUNTFAMILY)
        {
            $query .= '
    tbl_p_accountfamily.FAMACOMPTA AS `accountFamily.label`,';
        }

        $query .= '
    tbl_p_machinesfamily.FAMAPRIXINDICATIF AS `targetPrice`,
    tbl_p_machinesfamily.FAMFORFZ1 AS `packageZ1`,
    tbl_p_machinesfamily.FAMFORFZ2 AS `packageZ2`,
    tbl_p_machinesfamily.FAMPXJOUR AS `pricePerDay`,
    tbl_p_machinesfamily.FAMPXSEM AS `pricePerWeek`,
    tbl_p_machinesfamily.FAMMOISIND AS `indMonth`,
    tbl_p_machinesfamily.FAMMOISMIN AS `minMonth`,
    tbl_p_machinesfamily.CHARGE AS `load`
FROM FAMILLEMACHINE tbl_p_machinesfamily';

        # Jointure pour la famille commerciale
        if ($flags & GETINFOS_BUSINESSFAMILY)
        {
            $query .= '
LEFT JOIN FAMILLECOMMERCIALE tbl_p_businessfamily
ON tbl_p_machinesfamily.FACOMAUTO = tbl_p_businessfamily.FACOMAUTO';
        }

        # Jointure pour la famille comptable
        if ($flags & GETINFOS_ACCOUNTFAMILY)
        {
            $query .= '
LEFT JOIN FAMILLECOMPTA tbl_p_accountfamily
ON tbl_p_machinesfamily.FAMACOMPTAAUTO = tbl_p_accountfamily.FAMACOMPTAAUTO';
        }

        $query .= '
WHERE 1 ' .
$whereQuery . '
ORDER BY tbl_p_machinesfamily.FAMAMODELE;';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*)
FROM FAMILLEMACHINE tbl_p_machinesfamily';
        $query .= '
WHERE 1 ' .
$whereQuery . ';';
    }
    else
    {
        $query = '
SELECT
    tbl_p_machinesfamily.FAMAAUTO,
    CONCAT_WS(" ", tbl_p_machinesfamily.FAMAMODELE, tbl_p_machinesfamily.FAMAELEVATION, tbl_p_machinesfamily.FAMAENERGIE)
FROM FAMILLEMACHINE tbl_p_machinesfamily';
        $query .= '
WHERE 1 ' .
$whereQuery . '
ORDER BY tbl_p_machinesfamily.FAMAMODELE;';
    }

    my $tab = $db->fetchList($format, $query, {}, 'id');

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        # Formatage des données
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}                *= 1;
            $tabRow->{'businessFamily.id'} *= 1;
            $tabRow->{'accountFamily.id'}  *= 1;
            $tabRow->{'height'}            *= 1;
            $tabRow->{'packageZ1'}         *= 1;
            $tabRow->{'packageZ2'}         *= 1;
            $tabRow->{'pricePerDay'}       *= 1;
            $tabRow->{'pricePerWeek'}      *= 1;
            $tabRow->{'indMonth'}          *= 1;
            $tabRow->{'minMonth'}          *= 1;
            $tabRow->{'load'}              *= 1;

            # Formatage des libellés par pays
            if (exists $tabRow->{'labels'})
            {
                my @tabLabels = split(/,/, $tabRow->{'labels'});

                $tabRow->{'labels'} = {};
                foreach (@tabLabels)
                {
                    my ($country, $label) = split(/:/, $_);
                    $tabRow->{'labels'}->{$country} = $label;
                }
            }
        }
    }

    return (wantarray ? %$tab : $tab);
}


# Function: getUseRate
# Retourne le taux d'utilisation de la famille de machines dans l'agence et dans le pays
# 
# Parameters:
# string $id       - Identifiant de la famille de machines
# string $agencyId - Identifiant de l'agence
#
# Returns:
# hash|float|0 - Retourne le taux d'utilisation ('useRate'), le nombre de machines louées ('nbRented') et
#                le nombre total de machines ('nbTotal') dans l'agence ('agency'),la région ('area')
#                et dans le pays ('country') ou 0 si pas de résultat
sub getUseRate
{
    my ($id, $agencyId) = @_;

    # connexion à AUTH
    my $db = &LOC::Db::getConnection('auth');

    # Récupère les données concernant la machine
    my $query = '
SELECT
    IFNULL(SUM(SM.STMOLOU) + SUM(SM.STMOLIV), 0) AS `nbRented`,
    IFNULL(SUM(SM.STMOTOTAL), 0) AS `nbTotal`,
    agc_cty_id AS `agency.country.id`,
    agc_are_id AS `agency.area.id`
FROM STOCKMODELE SM
INNER JOIN frmwrk.p_agency
ON agc_id = SM.AGAUTO
LEFT JOIN MODELEMACHINE MM
ON SM.MOMAAUTO = MM.MOMAAUTO
WHERE MM.FAMAAUTO = ' . $db->quote($id) . ' AND SM.AGAUTO = ' . $db->quote($agencyId) . ';';
    my $dataRef = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    unless ($dataRef)
    {
        return 0;
    }

    # Retour sous forme de tableau ?
    my $useRate = &LOC::Util::round(($dataRef->{'nbTotal'} != 0 ? $dataRef->{'nbRented'} / $dataRef->{'nbTotal'} : 0), 4);
    if (wantarray)
    {
        my %tabResult = (
            'nbRented' => {
                'agency' => $dataRef->{'nbRented'} * 1,
                'area'   => undef,
                'country' => undef
            },
            'nbTotal'  => {
                'agency' => $dataRef->{'nbTotal'} * 1,
                'area'   => undef,
                'country' => undef
            },
            'useRate'  => {
                'agency' => $useRate,
                'area'   => undef,
                'country' => undef
            }
        );
        my $countryId = $dataRef->{'agency.country.id'};
        my $areaId    = $dataRef->{'agency.area.id'} * 1;

        # Calcul du taux d'utilisation instantané dans la région et le pays
        my $quotedAreaId    = $db->quote($areaId);
        my $quotedCountryId = $db->quote($countryId);
        $query = '
SELECT
    IFNULL(SUM(IF(agc_are_id = ' . $quotedAreaId . ', SM.STMOLOU, 0))
            + SUM(IF(agc_are_id = ' . $quotedAreaId . ', SM.STMOLIV, 0)), 0) AS `area.nbRented`,
    IFNULL(SUM(IF(agc_are_id = ' . $quotedAreaId . ', SM.STMOTOTAL, 0)), 0) AS `area.nbTotal`,
    IFNULL(SUM(IF(agc_cty_id = ' . $quotedCountryId . ', SM.STMOLOU, 0))
            + SUM(IF(agc_cty_id = ' . $quotedCountryId . ', SM.STMOLIV, 0)), 0) AS `country.nbRented`,
    IFNULL(SUM(IF(agc_cty_id = ' . $quotedCountryId . ', SM.STMOTOTAL, 0)), 0) AS `country.nbTotal`
FROM STOCKMODELE SM
INNER JOIN frmwrk.p_agency
ON agc_id = SM.AGAUTO AND
   (agc_are_id = ' . $quotedAreaId . ' OR agc_cty_id = ' . $quotedCountryId . ') AND
   agc_type = ' . $db->quote(LOC::Agency::TYPE_AGENCY) . ' AND
   agc_sta_id <> ' . $db->quote(LOC::Agency::STATE_DELETED) . '
LEFT JOIN MODELEMACHINE MM
ON SM.MOMAAUTO = MM.MOMAAUTO
WHERE MM.FAMAAUTO = ' . $db->quote($id) . ';';
        $dataRef = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
        if ($areaId)
        {
            $tabResult{'nbRented'}->{'area'} = $dataRef->{'area.nbRented'} * 1;
            $tabResult{'nbTotal'}->{'area'}  = $dataRef->{'area.nbTotal'} * 1;
            $tabResult{'useRate'}->{'area'}  = &LOC::Util::round(($dataRef->{'area.nbTotal'} != 0 ?
                                                    $dataRef->{'area.nbRented'} / $dataRef->{'area.nbTotal'} : 0), 4);
        }
        $tabResult{'nbRented'}->{'country'} = $dataRef->{'country.nbRented'} * 1;
        $tabResult{'nbTotal'}->{'country'}  = $dataRef->{'country.nbTotal'} * 1;
        $tabResult{'useRate'}->{'country'}  = &LOC::Util::round(($dataRef->{'country.nbTotal'} != 0 ?
                                                  $dataRef->{'country.nbRented'} / $dataRef->{'country.nbTotal'} : 0), 4);

        return %tabResult;
    }
    else
    {
        return $useRate;
    }
}


# Function: insert
# Ajoute une famille de machines.
#
# Parameters:
# string  $countryId  - Identifiant du pays
# hashref $tabData    - Données pour l'insertion de la famille de machines.
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Tableau des mises à jour de fin (facultatif)
#
# Returns:
# int|0 - Identifiant de la famille de machines créée, 0 en cas d'erreur
sub insert
{
    my ($countryId, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Formatage des données
    my $businessFamilyId  = (exists $tabData->{'businessFamily.id'} ? $tabData->{'businessFamily.id'} : 0);
    my $accountFamilyId   = (exists $tabData->{'accountFamily.id'} ? $tabData->{'accountFamily.id'} : 0);
    my $label             = (exists $tabData->{'label'} ? &LOC::Util::trim($tabData->{'label'}) : '');
    my $labels            = (exists $tabData->{'labels'} ? $tabData->{'labels'} : {});
    my $energy            = (exists $tabData->{'energy'} ? &LOC::Util::trim($tabData->{'energy'}) : ENERGY_UNDEFINED);
    my $elevation         = (exists $tabData->{'elevation'} ? &LOC::Util::trim($tabData->{'elevation'}) : '');
    my $height            = (exists $tabData->{'height'} ? $tabData->{'height'} : 0);

    # Erreurs
    # - Champs obligatoires non renseignés
    if ($businessFamilyId == 0 || $accountFamilyId == 0  ||
        ($label eq '' && $labels->{LOC::Country::DEFAULT_COUNTRY_ID} eq '') || $energy eq '' || $elevation eq '')
    {
        push(@{$tabErrors->{'fatal'}}, 0x0002);
        return 0;
    }

    # Formatage de l'élévation et de la hauteur
    &_formatElevationAndHeight(\$elevation, \$height);

    # Liste des pays
    my %tabCountry = &LOC::Country::getList(LOC::Util::GETLIST_PAIRS, undef, undef, {'defaultFirst' => 1});

    # Tableau des mise à jour à effectuer
    my $tabUpdates = {};

    foreach (keys %tabCountry)
    {
        my $countryLabel = $label;
        if ($labels->{$_})
        {
            $countryLabel = $labels->{$_};
        }
        elsif ($labels->{LOC::Country::DEFAULT_COUNTRY_ID})
        {
            $countryLabel = $labels->{LOC::Country::DEFAULT_COUNTRY_ID};
        }
        $tabUpdates->{$_} = {
            'FAMAMODELE'     => $countryLabel,
            'FAMAELEVATION'  => $elevation,
            'FAMAENERGIE'    => $energy,
            'FAMACOMPTAAUTO' => $accountFamilyId,
            'FAMAHAUTEUR'    => $height,
            'FACOMAUTO'      => $businessFamilyId
        };
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'insert', $_[0], __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, $_[1]);
        return 0;
    };

    # Insertion de la famille de machines dans tous les pays
    my $id;
    foreach my $updateCountry (keys(%$tabUpdates))
    {
        if (keys %{$tabUpdates->{$updateCountry}} > 0)
        {
            if ($id)
            {
                $tabUpdates->{$updateCountry}->{'FAMAAUTO'} = $id;
            }
            my $countrySchemaName = &LOC::Db::getSchemaName($updateCountry);
            $id = $db->insert('FAMILLEMACHINE', $tabUpdates->{$updateCountry}, $countrySchemaName);
            if ($id == -1)
            {
                return &$rollBackAndError(__LINE__, 0x0001);
            }
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            return &$rollBackAndError(__LINE__, 0x0001);
        }
    }

    # Ajout des traces de création
    my $tabTrace = {
        'schema'  => $schemaName,
        'code'    => LOC::Log::EventType::CREATION,
        'old'     => undef,
        'new'     => undef,
        'content' => ''
    };
    &LOC::EndUpdates::addTrace($tabEndUpds, 'machinesFamily', $id, $tabTrace);

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return $id;
}


# Function: insertSubRent
# Ajoute une famille de machines de sous-location
#
# Parameters:
# string  $countryId  - Identifiant du pays
# hashref $tabData    - Données pour l'insertion de la famille de machines
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Tableau des mises à jour de fin (facultatif)
#
# Returns:
# int|0 - Identifiant de la famille de machines créée, 0 en cas d'erreur
sub insertSubRent
{
    my ($countryId, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    $tabData->{'businessFamily.id'} = LOC::BusinessFamily::ID_SUBRENT;
    $tabData->{'accountFamily.id'}  = LOC::AccountFamily::ID_SUBRENT;
    $tabData->{'label'}  = $tabData->{'label'} . SUFFIX_SUBRENT;

    return &insert($countryId, $tabData, $userId, $tabErrors, $tabEndUpds);
}

# Function: update
# Met à jour une famille de machines
#
# Parameters
# string  $countryId  - Identifiant du pays
# int     $id         - Identifiant de la famille de machines
# hashref $tabData    - Données pour l'insertion de la famille de machines
# int     $userId     - Identifiant de l'utilisateur responsable
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Tableau des mises à jour de fin (facultatif)
sub update
{
    my ($countryId, $id, $tabData, $userId, $tabErrors, $tabEndUpds) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $rollBackAndError = sub {
        if (!$hasTransaction)
        {
            # Annule la transaction
            $db->rollBack();
        }
        # Affichage de l'erreur !
        print STDERR sprintf($fatalErrMsg, 'update', $_[0], __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, $_[1]);
        return 0;
    };

    # Récupération des informations de la famille de machines
    my $tabInfos = &getInfos($countryId, $id, GETINFOS_COUNTRYLABELS);
    if (!$tabInfos)
    {
        return &$rollBackAndError(__LINE__, 0x0001);
    }

    # Paramètres
    # - Famille de machines
    my $oldBusinessFamilyId = $tabInfos->{'businessFamily.id'};
    my $newBusinessFamilyId = (exists $tabData->{'businessFamily.id'} ? $tabData->{'businessFamily.id'} : $oldBusinessFamilyId);

    # - Famille comptable
    my $oldAccountFamilyId = $tabInfos->{'accountFamily.id'};
    my $newAccountFamilyId = (exists $tabData->{'accountFamily.id'} ? $tabData->{'accountFamily.id'} : $oldAccountFamilyId);

    # - Libellé
    my $oldLabel = $tabInfos->{'label'};
    my $newLabel = (exists $tabData->{'label'} ? $tabData->{'label'} : $oldLabel);

    # - Libellés par pays
    my $oldLabels = &LOC::Util::sortHashByKeys($tabInfos->{'labels'});
    my $newLabels = (exists $tabData->{'labels'} ? &LOC::Util::sortHashByKeys($tabData->{'labels'}) : $oldLabels);

    # - Énergie
    my $oldEnergy = $tabInfos->{'energy'};
    my $newEnergy = (exists $tabData->{'energy'} ? $tabData->{'energy'} : $oldEnergy);

    # - Élévation
    my $oldElevation = $tabInfos->{'elevation'};
    my $newElevation = (exists $tabData->{'elevation'} ? $tabData->{'elevation'} : $oldElevation);

    # - Hauteur
    my $oldHeight = $tabInfos->{'height'};
    my $newHeight = (exists $tabData->{'height'} ? $tabData->{'height'} : $oldHeight);

    # Formatage de l'élévation et de la hauteur
    &_formatElevationAndHeight(\$newElevation, \$newHeight);

    # Liste des pays
    my %tabCountry = &LOC::Country::getList(LOC::Util::GETLIST_PAIRS);

    # Tableau des mise à jour à effectuer
    my $tabUpdates = {};

    # Méthode d'ajout aux mises à jour et aux traces
    my $addUpdateAndTrace = sub
    {
        my ($old, $new, $field, $code, $country) = @_;

        foreach (keys %tabCountry)
        {
            # Seulement pour le pays passé en paramètre
            if (defined $country && $country ne $_)
            {
                next;
            }

            # Mise à jour
            $tabUpdates->{$_}->{$field} = $new;

            # Historisation
            my $tabTrace = {
                'schema'  => &LOC::Db::getSchemaName($_),
                'code'    => $code,
                'old'     => $old,
                'new'     => $new,
                'content' => $old . ' → ' . $new
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, 'machinesFamily', $id, $tabTrace);
        }
    };

    # Tableau des traces
    my $tabTraces  = {'old' => {}, 'new' => {}};

    if ($oldBusinessFamilyId != $newBusinessFamilyId)
    {
        &$addUpdateAndTrace($oldBusinessFamilyId, $newBusinessFamilyId, 'FACOMAUTO', LOC::Log::EventType::UPDBUSINESSFAMILY);
    }
    if ($oldAccountFamilyId != $newAccountFamilyId)
    {
        &$addUpdateAndTrace($oldAccountFamilyId, $newAccountFamilyId, 'FAMACOMPTAAUTO', LOC::Log::EventType::UPDACCOUNTFAMILY);
    }
    if ($oldLabel ne $newLabel)
    {
        &$addUpdateAndTrace($oldLabel, $newLabel, 'FAMAMODELE', LOC::Log::EventType::UPDLABEL);
    }
    if (!&LOC::Util::areArraysEquals($oldLabels, $newLabels))
    {
        # Historisation
        foreach my $labelCountry (keys(%$oldLabels))
        {
            if ($oldLabels->{$labelCountry} ne $newLabels->{$labelCountry})
            {
                &$addUpdateAndTrace(
                        $oldLabels->{$labelCountry},
                        $newLabels->{$labelCountry},
                        'FAMAMODELE',
                        LOC::Log::EventType::UPDLABEL,
                        $labelCountry
                    );
            }
        }
    }
    if ($oldEnergy ne $newEnergy)
    {
        &$addUpdateAndTrace($oldEnergy, $newEnergy, 'FAMAENERGIE', LOC::Log::EventType::UPDENERGY);
    }
    if ($oldElevation ne $newElevation)
    {
        &$addUpdateAndTrace($oldElevation, $newElevation, 'FAMAELEVATION', LOC::Log::EventType::UPDELEVATION);
    }
    if ($oldHeight != $newHeight)
    {
        &$addUpdateAndTrace($oldHeight, $newHeight, 'FAMAHAUTEUR', LOC::Log::EventType::UPDHEIGHT);
    }

    # Mise à jour des informations
    foreach my $updateCountry (keys(%$tabUpdates))
    {
        if (keys %{$tabUpdates->{$updateCountry}} > 0)
        {
            my $countrySchemaName = &LOC::Db::getSchemaName($updateCountry);
            if ($db->update('FAMILLEMACHINE', $tabUpdates->{$updateCountry}, {'FAMAAUTO*' => $id}, $countrySchemaName) == -1)
            {
                return &$rollBackAndError(__LINE__, 0x0001);
            }
        }
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            return &$rollBackAndError(__LINE__, 0x0001);
        }
    }

    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}

sub _formatElevationAndHeight
{
    my ($elevation, $height) = @_;

    if ($$elevation =~ /^([0-9]+[\.,]?[0-9]*) *([A-Za-z]?)$/)
    {
        my $number = $1;
        my $unit = $2;

        $number =~ s/\./,/;
        $$elevation = sprintf('%s %s', $number, $unit);

        $number =~ s/,/\./;
        $$height = &LOC::Util::round($number);
    }
}

1;
