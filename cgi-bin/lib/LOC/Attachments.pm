use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Attachments
# Module permettant de gérer les documents joints (au contrat ou devis)
package LOC::Attachments;


# Constants: Récupération d'informations sur les documents joints
# GETINFOS_ALL       - Récupération de l'ensemble des informations
# GETINFOS_BASE      - Récupération des informations de base
use constant {
    GETINFOS_ALL       => 0xFFFFFFFF,
    GETINFOS_BASE      => 0x00000000
};

# Constants: États
# STATE_ACTIVE  - Document actif
# STATE_DELETED - Document supprimé
use constant
{
    STATE_ACTIVE  => 'GEN01',
    STATE_DELETED => 'GEN03'
};

# Constants: Module des documents attachés
# MODULE_RENTCONTRACTDOCUMENT     - Document sur un contrat
# MODULE_RENTESTIMATELINEDOCUMENT - Document sur une ligne de devis
# MODULE_MACHINEINVENTORY         - État des lieux
use constant {
    MODULE_RENTCONTRACTDOCUMENT     => 'rentContractDocument',
    MODULE_RENTESTIMATELINEDOCUMENT => 'rentEstimateLineDocument',
    MODULE_MACHINEINVENTORY         => 'machineInventory'
};

# Constants: Erreurs
# ERROR_FATAL       - Erreur fatale
use constant
{
    ERROR_FATAL => 0x0001,

    ERROR_INVALIDFILEEXTENSION => 0x0100,
    ERROR_INVALIDFILETYPE      => 0x0101,
    ERROR_MAXSIZEREACHED       => 0x0102,
    ERROR_FILENOTFOUND         => 0x0103,
    ERROR_INVALIDCONTENT       => 0x0104,

    ERROR_UNKNOWNATTACHMENT    => 0x0200
};


use strict;

# Modules internes
use SOAP::Lite;
use HTTP::Cookies;
use LOC::Util;
use LOC::EndUpdates;

my $fatalErrMsg = 'Erreur fatale: module LOC::Attachments, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";

my $soapClient = undef;


# Function: getInfos
# Retourne les infos d'un document
#
# Contenu du hashage :
# - id => identifiant du document;
#
# Parameters:
# string $countryId   - Identifiant du pays
# string $id          - Identifiant du chantier
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|undef - Retourne un hash ou un hashref suivant la demande, undef si pas de résultat
sub getInfos
{
    my ($countryId, $id, $tabOptions) = @_;

    # Initialisation du tableau d'options
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    my $tab = &getList($countryId, {'id' => $id}, $tabOptions);
    return ($tab > 0  ? (wantarray ? %{$tab->[0]} : $tab->[0]) : undef);
}


# Function: getList
# Retourne la liste des documents
#
# Parameters:
# string  $countryId  - Identifiant du pays
# hashref $tabFilters - Filtres ()
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# hash|hashref|0 - Liste des documents (mêmes clés que pour la méthode getInfos)
sub getList
{
    my ($countryId, $tabFilters, $tabOptions) = @_;

    # Initialisation du tableau d'options
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    $tabOptions->{'filters'} = $tabFilters;

    my $result = &_soapCallMethod('getAttachmentsList', $tabOptions);
    if (!$result || $result->fault)
    {
        return 0;
    }

    # Formatage UTF-8
    my $tabList = $result->result;
    foreach my $tabInfos (@$tabList)
    {
        # Décodage base 64 ?
        if (!$tabOptions->{'getBase64Content'})
        {
            if (defined $tabInfos->{'fileContent'})
            {
                $tabInfos->{'fileContent'} = &LOC::Util::base64Decode($tabInfos->{'fileContent'});
            }
            if (defined $tabInfos->{'thumbnailsContents'} && ref $tabInfos->{'thumbnailsContents'} eq 'HASH')
            {
                foreach my $thumbnailContent (values(%{$tabInfos->{'thumbnailsContents'}}))
                {
                    $thumbnailContent = &LOC::Util::base64Decode($thumbnailContent);
                }
            }
        }
    }

    return $tabList;
}


# Function: insert
# Insertion d'un document
#
# Contenu du de la table de hashage à passer en paramètre:
# -
#
# Parameters:
# string  $countryId  - Identifiant du pays
# string  $moduleName - Nom du module
# mixed   $entityId   - Identifiant lié
# hashref $tabData    - Données
# int     $userId     - Id de l'utilisateur créateur
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool - Retourne 1 si l'insertion est correcte, 0 sinon
sub insert
{
    # récupération des paramètres
    my ($countryId, $moduleName, $entityId, $tabData, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Initialisation du tableau d'options
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];
    $tabErrors->{'warning'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération du fichier
    my $fileContent;
    if (defined $tabData->{'fileContent'})
    {
        $fileContent = $tabData->{'fileContent'};
        if (!$tabOptions->{'isBase64Content'})
        {
            $fileContent = &LOC::Util::base64Encode($fileContent);
        }
    }
    elsif (defined $tabData->{'filePath'})
    {
        open(FILE, $tabData->{'filePath'});
        binmode FILE, ":raw";
        my @tabFile = <FILE>;
        close(FILE);
        $fileContent = join('', @tabFile);
        $fileContent = &LOC::Util::base64Encode($fileContent);
    }
    else
    {
        # Erreur fatale
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    my $result = &_soapCallMethod('addAttachment',
                                  $moduleName,
                                  $tabData->{'fileName'},
                                  $fileContent,
                                  [$countryId, $entityId],
                                  $tabData->{'title'},
                                  $tabData->{'description'},
                                  undef,
                                  $userId,
                                  $tabOptions);
    if (!$result)
    {
        # Erreur fatale
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    # Erreur
    if ($result->fault)
    {
        my $faultCode = $result->fault->{'faultcode'};
        if ($faultCode eq 'InvalidFileExtension')
        {
            # Extension de fichier non autorisée
            push(@{$tabErrors->{'fatal'}}, ERROR_INVALIDFILEEXTENSION);
        }
        elsif ($faultCode eq 'InvalidFileType')
        {
            # Type de fichier non autorisé
            push(@{$tabErrors->{'fatal'}}, ERROR_INVALIDFILETYPE);
        }
        elsif ($faultCode eq 'MaxSizeReached')
        {
            # Taille maximale dépassée
            push(@{$tabErrors->{'fatal'}}, ERROR_MAXSIZEREACHED);
        }
        elsif ($faultCode eq 'FileNotFound')
        {
            # Fichier introuvable
            push(@{$tabErrors->{'fatal'}}, ERROR_FILENOTFOUND);
        }
        elsif ($faultCode eq 'InvalidContent')
        {
            # Contenu non valide
            push(@{$tabErrors->{'fatal'}}, ERROR_INVALIDCONTENT);
        }
        else
        {
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'insert', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        }
        return 0;
    }

    my $id = $result->result * 1;

    # Historisation
    if (&LOC::Util::in_array($moduleName, [MODULE_RENTCONTRACTDOCUMENT, MODULE_RENTESTIMATELINEDOCUMENT]))
    {
        my $schemaName = 'LOCATION' . ($countryId eq 'FR' ? '' : $countryId);
        my $content = $tabData->{'fileName'};
        my $tabTrace = {
            'schema' => $schemaName,
            'code'   => LOC::Log::EventType::ADDDOCUMENT,
            'content' => $content
        };
        my $functionality;
        if ($moduleName eq MODULE_RENTCONTRACTDOCUMENT)
        {
            $functionality = 'contract';
        }
        if ($moduleName eq MODULE_RENTESTIMATELINEDOCUMENT)
        {
            $functionality = 'estimateLine';
        }
        &LOC::EndUpdates::addTrace($tabEndUpds, $functionality, $entityId, $tabTrace);
    }


    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }
    return $id;
}




# Function: insertMultiple
# Insertion de plusieurs documents
#
# Contenu de la table de hashage à passer en paramètre:
# -
#
# Parameters:
# string   $countryId  - Identifiant du pays
# string   $moduleName - Nom du module
# mixed    $entityId   - Identifiant lié
# arrayref $tabFiles   - Tableau des fichiers
# int      $userId     - Id de l'utilisateur créateur
# hashref  $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref  $tabErrors  - Tableau des erreurs
# hashref  $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool - Retourne 1 si l'insertion est correcte, 0 sinon
sub insertMultiple
{
    # récupération des paramètres
    my ($countryId, $moduleName, $entityId, $tabFiles, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Initialisation du tableau d'options
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];
    $tabErrors->{'warning'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    my @tabAttachments = ();
    foreach my $file (@$tabFiles)
    {
        # Récupération du fichier
        my $fileContent;
        if (defined $file->{'fileContent'})
        {
            $fileContent = $file->{'fileContent'};
            if (!$tabOptions->{'isBase64Content'})
            {
                $fileContent = &LOC::Util::base64Encode($fileContent);
            }
        }
        elsif (defined $file->{'filePath'})
        {
            open(FILE, $file->{'filePath'});
            binmode FILE, ":raw";
            my @tabFile = <FILE>;
            close(FILE);
            $fileContent = join('', @tabFile);
            $fileContent = &LOC::Util::base64Encode($fileContent);
        }
        else
        {
            # Erreur fatale
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            return 0;
        }

        push(@tabAttachments, {
            'name'       => $file->{'fileName'},
            'content'    => $fileContent,
            'date'       => $file->{'date'},
            'position'   => $file->{'position'},
            'relatedIds' => [$countryId, $entityId]
        });
    }

    my $result = &_soapCallMethod('addAttachments',
                                  $moduleName,
                                  \@tabAttachments,
                                  $userId,
                                  $tabOptions);
    if (!$result)
    {
        # Erreur fatale
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    # Erreur
    if ($result->fault)
    {
        my $faultCode = $result->fault->{'faultcode'};
        if ($faultCode eq 'InvalidFileExtension')
        {
            # Extension de fichier non autorisée
            push(@{$tabErrors->{'fatal'}}, ERROR_INVALIDFILEEXTENSION);
        }
        elsif ($faultCode eq 'InvalidFileType')
        {
            # Type de fichier non autorisé
            push(@{$tabErrors->{'fatal'}}, ERROR_INVALIDFILETYPE);
        }
        elsif ($faultCode eq 'MaxSizeReached')
        {
            # Taille maximale dépassée
            push(@{$tabErrors->{'fatal'}}, ERROR_MAXSIZEREACHED);
        }
        elsif ($faultCode eq 'FileNotFound')
        {
            # Fichier introuvable
            push(@{$tabErrors->{'fatal'}}, ERROR_FILENOTFOUND);
        }
        elsif ($faultCode eq 'InvalidContent')
        {
            # Contenu non valide
            push(@{$tabErrors->{'fatal'}}, ERROR_INVALIDCONTENT);
        }
        else
        {
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'insert', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        }
        return 0;
    }

    # Historisation
    if (&LOC::Util::in_array($moduleName, [MODULE_RENTCONTRACTDOCUMENT]))
    {
        my $schemaName = 'LOCATION' . ($countryId eq 'FR' ? '' : $countryId);
        my $functionality;
        if ($moduleName eq MODULE_RENTCONTRACTDOCUMENT)
        {
            $functionality = 'contract';
        }
        foreach my $attachment (@tabAttachments)
        {
            my $content = $attachment->{'name'};
            my $tabTrace = {
                'schema' => $schemaName,
                'code'   => LOC::Log::EventType::ADDDOCUMENT,
                'content' => $content
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, $functionality, $entityId, $tabTrace);
        }

    }


    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }
    return $result->result;
}

# Function: duplicate
# Duplique un document
#
# Parameters:
# string  $countryId  - Identifiant du pays
# hashref $id         - Identifiant du doc attaché à dupliquer
# string  $moduleName - Nom du module
# mixed   $entityId   - Identifiant lié
# int     $userId     - Id de l'utilisateur créateur
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool - Retourne 1 si l'insertion est correcte, 0 sinon
sub duplicate
{
    # récupération des paramètres
    my ($countryId, $id, $moduleName, $entityId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Initialisation du tableau d'options
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];
    $tabErrors->{'warning'} = [];


    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération des informations du document
    my $tabInfos = &getInfos($countryId, $id);
    if (!$tabInfos)
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'duplicate', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    my $result = &_soapCallMethod('duplicateAttachment',
                                  $id,
                                  $moduleName,
                                  [$countryId, $entityId],
                                  $userId,
                                  $tabOptions);
    if (!$result)
    {
        # Erreur fatale
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    # Erreur
    if ($result->fault)
    {
        my $faultCode = $result->fault->{'faultcode'};
        if ($faultCode eq 'UnknownAttachment')
        {
            # Document inconnu
            push(@{$tabErrors->{'fatal'}}, ERROR_UNKNOWNATTACHMENT);
        }
        elsif ($faultCode eq 'InvalidFileExtension')
        {
            # Extension de fichier non autorisée
            push(@{$tabErrors->{'fatal'}}, ERROR_INVALIDFILEEXTENSION);
        }
        elsif ($faultCode eq 'InvalidFileType')
        {
            # Type de fichier non autorisé
            push(@{$tabErrors->{'fatal'}}, ERROR_INVALIDFILETYPE);
        }
        elsif ($faultCode eq 'MaxSizeReached')
        {
            # Taille maximale dépassée
            push(@{$tabErrors->{'fatal'}}, ERROR_MAXSIZEREACHED);
        }
        elsif ($faultCode eq 'FileNotFound')
        {
            # Fichier introuvable
            push(@{$tabErrors->{'fatal'}}, ERROR_FILENOTFOUND);
        }
        elsif ($faultCode eq 'InvalidContent')
        {
            # Contenu non valide
            push(@{$tabErrors->{'fatal'}}, ERROR_INVALIDCONTENT);
        }
        else
        {
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'duplicate', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        }
        return 0;
    }

    # Historisation
    if (&LOC::Util::in_array($moduleName, [MODULE_RENTCONTRACTDOCUMENT, MODULE_RENTESTIMATELINEDOCUMENT]))
    {
        my $schemaName = 'LOCATION' . ($countryId eq 'FR' ? '' : $countryId);
        my $content = $tabInfos->{'fileName'};
        my $tabTrace = {
            'schema' => $schemaName,
            'code'   => LOC::Log::EventType::ADDDOCUMENT,
            'content' => $content
        };
        my $functionality;
        if ($moduleName eq MODULE_RENTCONTRACTDOCUMENT)
        {
            $functionality = 'contract';
        }
        if ($moduleName eq MODULE_RENTESTIMATELINEDOCUMENT)
        {
            $functionality = 'estimateLine';
        }
        &LOC::EndUpdates::addTrace($tabEndUpds, $functionality, $entityId, $tabTrace);
    }


    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }
    return 1;
}


# Function: update
# Mise à jour des informations d'un document
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Id du document à modifier
# hashref $tabData    - Données
# int     $userId     - Id de l'utilisateur modifieur
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub update
{
    # récupération des paramètres
    my ($countryId, $id, $tabData, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Initialisation du tableau d'options
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }

    $tabErrors->{'fatal'} = [];
    $tabErrors->{'warning'} = [];

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération des informations du document
    my $tabInfos = &getInfos($countryId, $id);
    if (!$tabInfos)
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'update', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    my $result = &_soapCallMethod('modifyAttachment', $id, $tabData, $userId, $tabOptions);
    if (!$result)
    {
        # Erreur fatale
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    # Erreur
    if ($result->fault)
    {
        my $faultCode = $result->fault->{'faultcode'};
        if ($faultCode eq 'UnknownAttachment')
        {
            # Document inconnu
            push(@{$tabErrors->{'fatal'}}, ERROR_UNKNOWNATTACHMENT);
        }
        else
        {
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'update', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        }
        return 0;
    }

    # Historisation
    my $tabTraces  = {'old' => {}, 'new' => {}};
    my $hasChanged = 0;
    my @tabChangeLabels = ();
    if (defined $tabData->{'title'} && $tabInfos->{'title'} ne $tabData->{'title'})
    {
        $tabTraces->{'old'}->{'title'} = $tabInfos->{'title'};
        $tabTraces->{'new'}->{'title'} = $tabData->{'title'};
        $hasChanged = 1;
        push(@tabChangeLabels, 'titre');
    }
    if (defined $tabData->{'description'} && $tabInfos->{'description'} ne $tabData->{'description'})
    {
        $tabTraces->{'old'}->{'description'} = $tabInfos->{'description'};
        $tabTraces->{'new'}->{'description'} = $tabData->{'description'};
        $hasChanged = 1;
        push(@tabChangeLabels, 'description');
    }

    if ($hasChanged)
    {
        my $schemaName = &LOC::Db::getSchemaName($countryId);
        my $tabTrace = {
            'schema' => $schemaName,
            'code'   => LOC::Log::EventType::UPDDOCUMENT,
            'old' => $tabTraces->{'old'},
            'new' => $tabTraces->{'new'},
            'content' => $tabInfos->{'fileName'} . ' (' . join(', ', @tabChangeLabels) . ')',
        };
        my $functionality;
        my $functionalityId;
        if ($tabInfos->{'moduleName'} eq MODULE_RENTCONTRACTDOCUMENT)
        {
            $functionality = 'contract';
            $functionalityId = $tabInfos->{'secondId'} * 1;
        }
        if ($tabInfos->{'moduleName'} eq MODULE_RENTESTIMATELINEDOCUMENT)
        {
            $functionality = 'estimateLine';
            $functionalityId = $tabInfos->{'secondId'} * 1;
        }
        &LOC::EndUpdates::addTrace($tabEndUpds, $functionality, $functionalityId, $tabTrace);
    }


    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }

    return 1;
}

# Function: delete
# Supprime un document
#
# Parameters:
# string  $countryId  - Identifiant du pays
# int     $id         - Id du document à supprimer
# int     $userId     - Id de l'utilisateur modifieur
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub delete
{
    # récupération des paramètres
    my ($countryId, $id, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Initialisation du tableau d'options
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }

    $tabErrors->{'fatal'} = [];
    $tabErrors->{'warning'} = [];

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    # Récupération des informations du document
    my $tabInfos = &getInfos($countryId, $id);
    if (!$tabInfos)
    {
        # Erreur fatale !
        print STDERR sprintf($fatalErrMsg, 'delete', __LINE__, __FILE__, '');
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    my $result = &_soapCallMethod('removeAttachment', $id, $userId, $tabOptions);
    if (!$result)
    {
        # Erreur fatale
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    # Erreur
    if ($result->fault)
    {
        my $faultCode = $result->fault->{'faultcode'};
        if ($faultCode eq 'UnknownAttachment')
        {
            # Document inconnu
            push(@{$tabErrors->{'fatal'}}, ERROR_UNKNOWNATTACHMENT);
        }
        else
        {
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'delete', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        }
        return 0;
    }

    # Historisation
    my $schemaName = &LOC::Db::getSchemaName($countryId);
    my $tabTrace = {
        'schema'  => $schemaName,
        'code'    => LOC::Log::EventType::DELDOCUMENT,
        'content' => $tabInfos->{'fileName'}
    };
    my $functionality;
    my $functionalityId;
    if ($tabInfos->{'moduleName'} eq MODULE_RENTCONTRACTDOCUMENT)
    {
        $functionality = 'contract';
        $functionalityId = $tabInfos->{'secondId'} * 1;
    }
    if ($tabInfos->{'moduleName'} eq MODULE_RENTESTIMATELINEDOCUMENT)
    {
        $functionality = 'estimateLine';
        $functionalityId = $tabInfos->{'secondId'} * 1;
    }
    &LOC::EndUpdates::addTrace($tabEndUpds, $functionality, $functionalityId, $tabTrace);


    # Exécution des mises à jour de fin si nécessaire
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
    }
    return 1;
}

# Function: deleteAll
# Supprime les documents d'une entité
#
# Parameters:
# string  $countryId  - Identifiant du pays
# string  $moduleName - Nom du module
# int     $entityId   - Id de l'entité sur laquelle supprimer les documents
# int     $userId     - Id de l'utilisateur modifieur
# hashref $tabOptions - Tableau des options supplémentaires (facultatif)
# hashref $tabErrors  - Tableau des erreurs
# hashref $tabEndUpds - Mises à jour de fin
#
# Returns:
# bool - Retourne 1 si la mise à jour est correcte, 0 sinon
sub deleteAll
{
    # récupération des paramètres
    my ($countryId, $moduleName, $entityId, $userId, $tabOptions, $tabErrors, $tabEndUpds) = @_;

    # Initialisation du tableau d'options
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }
    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }

    $tabErrors->{'fatal'} = [];
    $tabErrors->{'warning'} = [];

    # Initialisation des mises à jour de fin si nécessaire
    my $execEndUpdsAtEnd = 0;
    if (!defined $tabEndUpds)
    {
        $tabEndUpds = {};
        $execEndUpdsAtEnd = 1;
    }

    my $result = &_soapCallMethod('removeAttachments', $moduleName, [$countryId, $entityId], $userId, $tabOptions);
    if (!$result)
    {
        # Erreur fatale
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        return 0;
    }

    # Erreur
    if ($result->fault)
    {
        my $faultCode = $result->fault->{'faultcode'};
        if ($faultCode eq 'UnknownAttachment')
        {
            # Document inconnu
            push(@{$tabErrors->{'fatal'}}, ERROR_UNKNOWNATTACHMENT);
        }
        else
        {
            # Erreur fatale !
            print STDERR sprintf($fatalErrMsg, 'deleteAll', __LINE__, __FILE__, '');
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        }
        return 0;
    }
    return 1;
}

# Function: _convertToSoapData
# Transforme une variable en SoapData
#
# Parameters:
# string  $dataName - Nom
# hashref $data     - Hash
#
# Returns:
# SOAP::Data
sub _convertToSoapData
{
    my ($dataName, $data) = @_;

    my $result;
    if (ref($data) eq 'HASH')
    {
        my @items = ();
        foreach my $key (keys %$data)
        {
            push(@items, \SOAP::Data->value(
                SOAP::Data->name('key' => $key),
                &_convertToSoapData('value', $data->{$key})
            ));
        }
        if (@items > 0)
        {
            $result = SOAP::Data->name($dataName => \SOAP::Data->name('item' => @items))->type('ns2:Map');
        }
        else
        {
            $result = SOAP::Data->name($dataName => []);
        }
    }
    elsif (ref($data) eq 'ARRAY')
    {
        my @items = ();
        foreach my $item (@$data)
        {
            push(@items, &_convertToSoapData('item', $item));
        }
        $result = SOAP::Data->name($dataName => \@items);
    }
    else
    {
        $result = SOAP::Data->name($dataName => $data);
    }

    return $result;
}


# Function: _soapCallMethod
# Appelle une méthode Soap
#
# Parameters:
# string $methodName - Nom de la méthode
# array  @tabParams  - Paramètres
#
# Returns:
# mixed
sub _soapCallMethod
{
    my ($methodName, @tabParams) = @_;

    if (!defined $soapClient)
    {
        my $guiWs = &LOC::Globals::get('guiWs');
        $soapClient  = SOAP::Lite->uri($guiWs->{'uri'})
                                 ->proxy($guiWs->{'location'},
                                         'cookie_jar' => HTTP::Cookies->new())
                                 ->ns($guiWs->{'uri'}, 'ns1')
                                 ->ns('http://xml.apache.org/xml-soap', 'ns2');

        $soapClient->call(SOAP::Data->name('ns1:setApplicationId'), &LOC::Globals::get('appId'));
    }

    my @tabSoapParams = ();

    my $cpt = 0;
    foreach my $param (@tabParams)
    {
        push(@tabSoapParams, &_convertToSoapData('param' . $cpt++, $param));
    }

    return $soapClient->call(SOAP::Data->name('ns1:' . $methodName), @tabSoapParams);
}


1;