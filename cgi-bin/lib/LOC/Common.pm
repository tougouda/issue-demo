use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Common
# Module contenant des fonctions et constantes métiers 'généralistes'
package LOC::Common;
use strict;


# Constants: Modes d'application de la participation au recyclage
# RESIDUES_MODE_NONE    - Pas de participation au recyclage
# RESIDUES_MODE_PRICE   - Montant forfaitaire
# RESIDUES_MODE_PERCENT - Pourcentage de la location et de la remise en état
use constant
{
    RESIDUES_MODE_NONE    => 0,
    RESIDUES_MODE_PRICE   => 1,
    RESIDUES_MODE_PERCENT => 2
};

# Constants: Modes d'application de la gestion de recours
# APPEAL_MODE_NONE    - Pas de gestion de recours
# APPEAL_MODE_PERCENT - Pourcentage de la location fois jours calendaires
use constant
{
    APPEAL_MODE_NONE    => 0,
    APPEAL_MODE_PERCENT => 1
};

1;