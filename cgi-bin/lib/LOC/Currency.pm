use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Currency
# Module permettant d'obtenir les informations d'une devise
package LOC::Currency;


# Constants: Récupération d'informations sur les nettoyages
# GETINFOS_BASE - Récupération des informations de base
use constant
{
    GETINFOS_BASE => 0x00000000
};

# Modules internes
use LOC::Db;
use LOC::Util;

# Constants: États
# STATE_ACTIVE   - Alerte active
# STATE_INACTIVE - Alerte inactive
use constant
{
    STATE_ACTIVE   => 'GEN01',
    STATE_INACTIVE => 'GEN03',
};

# Constants: Valeurs par défaut
# DEFAULT_CURRENCY_ID - Identifiant de la devise par défaut
use constant
{
    DEFAULT_CURRENCY_ID => 'EUR'
};


use strict;


# Sauvegarde des taux de conversion en euro pour les différentes devises
my $_tabEuroRates = {};


# Function: getInfos
# Retourne les infos d'une devise
# 
# Contenu du hashage :
# - id       => identifiant de la devise ;
# - symbol   => symbole ;
# - name     => nom ;
# - euroRate => taux de conversion par rapport l'euro ;
# - state.id => code état.
#
# Parameters:
# string $id - Code de la devise
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getInfos
{
    my ($id) = @_;

    # connexion à AUTH
    my $conn = &LOC::Db::getConnection('frmwrk');

    # Récupère les données concernant l'agence
    my $query = '
SELECT
    cur_id AS `id`,
    cur_symbol AS `symbol`,
    cur_name AS `name`,
    cur_eurorate AS `euroRate`,
    cur_sta_id AS `state.id`
FROM p_currency
WHERE cur_id = "' . $id . '";';
    my $tab = $conn->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);;
    if (!$tab)
    {
        return 0;
    }

    $tab->{'euroRate'} *= 1;

    return (wantarray ? %$tab : $tab);
}

# Function: getBySymbol
# Retourne l'identifiant d'une devise grâce à son symbole
# 
# Parameters:
# string $symbol - Symbole de la devise recherchée
#
# Returns:
# string|0
sub getBySymbol
{
    my ($symbol) = @_;

    # connexion à AUTH
    my $db = &LOC::Db::getConnection('frmwrk');

    # Récupère les données concernant l'agence
    my $query = '
SELECT
    cur_id AS `id`
FROM p_currency
WHERE cur_symbol = ' . $db->quote($symbol) . ';';
    my $tab = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if (!$tab)
    {
        return 0;
    }
    return $tab->{'id'};
}

# Function: getList
# Récupérer la liste des devises
#
# Contenu du hachage:
# - id          => id
# - symbol      => symbole local
# - name        => nom
# - euroRate    => taux de conversion par rapport à l'euro
# - state.id    => état
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id)
# int     $flags      - Flags
#
# Returns:
# hash|hashref|undef
sub getList
{
    my ($countryId, $format, $tabFilters, $flags) = @_;
    if (!defined $flags)
    {
        $flags = GETINFOS_BASE;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('frmwrk');

    my $query = '';

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    cur_id AS `id`,
    cur_symbol AS `symbol`,
    cur_name AS `name`,
    cur_eurorate AS `euroRate`,
    cur_sta_id AS `state.id`
FROM p_currency
WHERE cur_sta_id <> ' . $db->quote(STATE_INACTIVE);
        if (defined $tabFilters->{'id'})
        {
            if (ref($tabFilters->{'id'}) eq 'ARRAY')
            {
                $query .= '
    AND `cur_id` IN (' . join(', ', @{$tabFilters->{'id'}}) . ')';
            }
            else
            {
                $query .= '
    AND `cur_id` = ' . $tabFilters->{'id'};
            }
        }
        $query .= '
ORDER BY cur_name'; 
    }
    elsif ($format == LOC::Util::GETLIST_PAIRS)
    {
        $query = '
SELECT
    cur_id AS `id`,
    cur_symbol AS `symbol`
FROM p_currency
WHERE cur_sta_id <> ' . $db->quote(STATE_INACTIVE);
        if (defined $tabFilters->{'id'})
        {
            if (ref($tabFilters->{'id'}) eq 'ARRAY')
            {
                $query .= '
    AND `cur_id` IN (' . join(', ', @{$tabFilters->{'id'}}) . ')';
            }
            else
            {
                $query .= '
    AND `cur_id` = ' . $tabFilters->{'id'};
            }
        }
        $query .= '
ORDER BY cur_name'; 
    }

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    return (wantarray ? %$tab : $tab);
}

# Function: getEuroRate
# Retourne le taux de conversion vers l'euro d'une devise à une date demandée
# 
# Parameters:
# string $id   - Identifiant de la devise
# string $date - Date
#
# Returns:
# float - Taux de conversion vers l'euro
sub getEuroRate
{
    my ($id, $date) = @_;

    my $rateStrId = $id . '_' . $date;
    if (!defined $_tabEuroRates->{$rateStrId})
    {
        # connexion à AUTH
        my $db = &LOC::Db::getConnection('frmwrk');

        # Récupère les données concernant l'historique du taux de conversion
        my $query = '
SELECT
    crr_value
FROM h_currencyrate
WHERE crr_cur_id = ' . $db->quote($id);
        if ($date ne '')
        {
            $query .= '
AND crr_date_begin <= ' . $db->quote($date);
        }
        $query .= '
ORDER BY crr_date_begin DESC
LIMIT 1;';
        $_tabEuroRates->{$rateStrId} = $db->fetchOne($query);
        unless (defined $_tabEuroRates->{$rateStrId})
        {
            my $tabInfos = &getInfos($id);
            if ($tabInfos)
            {
                $_tabEuroRates->{$rateStrId} = $tabInfos->{'euroRate'};
            }
        }
        $_tabEuroRates->{$rateStrId} = $_tabEuroRates->{$rateStrId} * 1;
    }

    return $_tabEuroRates->{$rateStrId};
}

1;