use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Agency
# Module permettant d'obtenir les informations d'une agence
package LOC::Agency;


# Constants: États
# STATE_ACTIVE  - Agence active
# STATE_DELETED - Agence supprimée
use constant
{
    STATE_ACTIVE  => 'GEN01',
    STATE_DELETED => 'GEN03',
};

# Constants: Types
# TYPE_AGENCY     - Agence
# TYPE_AFTERSALES - SAV
# TYPE_HEADOFFICE - Siège
use constant
{
    TYPE_AGENCY     => 'AGC',
    TYPE_AFTERSALES => 'SAV',
    TYPE_HEADOFFICE => 'SIE',
};

# Constants: Identifiants d'agences
# ID_HEADOFFICE - Siège - Tonneins
use constant
{
    ID_HEADOFFICE   => 'SIE'
};

# Constants: Flags pour la récupération des informations
# GETINFOS_ALL    - Toutes les informations
# GETINFOS_BASE   - Informations de base
# GETINFOS_KIMOCE - Informations sur Kimoce
use constant
{
    GETINFOS_ALL    => 0xFFFF,
    GETINFOS_BASE   => 0x0000,
    GETINFOS_KIMOCE => 0x0001
};


use strict;

# Modules internes
use LOC::Db;

# Function: getInfos
# Retourne les infos d'une agence
#
# Contenu du hashage : voir la fonction getList.
#
# Parameters:
# string $id          - Code de l'agence
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getInfos
{
    my ($id, $flags, $tabOptions) = @_;

    my $tab = &getList(LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags, $tabOptions);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Retourne la liste des agences
#
# Contenu de la table de hachage :
# - id : code de l'agence ;
# - label : nom ;
# - country.id : identifiant du pays ;
# - area.id : identifiant de la région ;
# - area.label : nom de la région ;
# - area.code : code de la région ;
# - area.manager.id : identifiant du responsable de région ;
# - area.techManager.id : identifiant du responsable technique de région
# - manager.id : identifiant du chef d'agence ;
# - manager.name : nom du chef d'agence ;
# - manager.firstname : prénom du chef d'agence ;
# - manager.fullName : nom complet du chef d'agence ;
# - address : adresse ;
# - postalCode : code postal ;
# - city : ville ;
# - telephone : numéro de téléphone ;
# - fax : numéro de fax ;
# - email : adresse e-mail ;
# - email.contact : adresse e-mail de contact ;
# - email.shop : adresse e-mail de l'atelier ;
# - email.site : adresse e-mail de l'agence ;
# - email.tech : adresse e-mail des techniciens ;
# - email.transp : adresse e-mail des exploitants transport ;
# - ip : tranche IP ;
# - timeZone : nom du fuseau horaire (liste diponible par la fonction DateTime::TimeZone->all_names) ;
# - type : type d'agence ;
# - analyticalAccount : portion du code analytique relatif à l'agence ;
# - CpyInCde : valeur du CpyInCde (information Kimoce) ;
# - CpyAddrInCde : valeur du CpyAddrInCde (information Kimoce) ;
# - creationDate : date et heure de création ;
# - modificationDate : date et heure de modification ;
# - state.id : identifiant de l'état.
#
# Parameters:
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres ('id' : liste des identifiants d'agences,
#                                'country' : liste des identifiants de pays,
#                                'area' : liste des identifiants de région,
#                                'type' : liste des types d'agences)
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hash|hashref|0 - Liste des agences
sub getList
{
    my ($format, $tabFilters, $flags, $tabOptions) = @_;

    if (ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('frmwrk');

    # Filtres
    my $whereQuery = '';

    # - Identifiant
    if (defined $tabFilters->{'id'})
    {
        $whereQuery = '
WHERE agc_id IN (' . $db->quote($tabFilters->{'id'}) . ')';
    }
    else
    {
        $whereQuery = '
WHERE agc_sta_id <> ' . $db->quote(STATE_DELETED);
    }

    # - Pays
    if ($tabFilters->{'country'} ne '')
    {
        $whereQuery .= '
AND agc_cty_id IN (' . $db->quote($tabFilters->{'country'}) . ')';
    }
    # - Régions
    if ($tabFilters->{'area'} ne '')
    {
        $whereQuery .= '
AND agc_are_id IN (' . $db->quote($tabFilters->{'area'}) . ')';
    }
    # - Types
    if ($tabFilters->{'type'} ne '')
    {
        $whereQuery .= '
AND agc_type IN (' . $db->quote($tabFilters->{'type'}) . ')';
    }
    # - Exceptions
    if ($tabFilters->{'exceptions'} ne '')
    {
        $whereQuery .= '
AND agc_id NOT IN (' . $db->quote($tabFilters->{'exceptions'}) . ')';
    }

    my $query = '';
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    agc_id AS `id`,
    agc_cty_id AS `country.id`,
    agc_label AS `label`,
    agc_address AS `address`,
    agc_postalcode AS `postalCode`,
    agc_city AS `city`,
    agc_telephone AS `telephone`,
    agc_fax AS `fax`,
    agc_mail AS `email`,
    GROUP_CONCAT(CONCAT_WS(":", agm_type, agm_value)) AS `email.list`,
    agc_ip AS `ip`,
    agc_timezone AS `timeZone`,
    agc_type AS `type`,
    agc_analyticalaccount AS `analyticalAccount`,
    agc_are_id AS `area.id`,
    are_label AS `area.label`,
    are_code AS `area.code`,
    are_usr_id AS `area.manager.id`,
    are_usr_id_techmanager AS `area.techManager.id`,
    agc_usr_id AS `manager.id`,
    usr_name AS `manager.name`,
    usr_firstname AS `manager.firstname`,
    CONCAT_WS(" ", usr_name, usr_firstname) AS `manager.fullName`';
        if ($flags & GETINFOS_KIMOCE)
        {
            $query .= ',
    CpyInCde,
    CpyAddrInCde';
        }
        $query .= ',
    cty_subsidiary AS `country.subsidiary`,
    AGPTRN AS `isTransportPlatform`,
    agc_date_create AS `creationDate`,
    agc_date_modif AS `modificationDate`,
    agc_sta_id AS `state.id`
FROM p_agency
INNER JOIN p_country
ON cty_id = agc_cty_id
LEFT JOIN AUTH.AGENCE
ON agc_id = AGAUTO
LEFT JOIN p_agencymail
ON agm_agc_id = agc_id
LEFT JOIN p_area
ON agc_are_id = are_id
LEFT JOIN f_user
ON agc_usr_id = usr_id' .
$whereQuery . '
GROUP BY agc_id
ORDER BY agc_label';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*)
FROM p_agency' .
$whereQuery;
    }
    else
    {
        $query = '
SELECT
    agc_id,
    agc_label
FROM p_agency' .
$whereQuery . '
ORDER BY agc_label';
    }

    if (defined $tabOptions->{'limit'})
    {
        $query .= '
LIMIT ' . $tabOptions->{'limit'};
    }
    $query .= ';';

    my $tab = $db->fetchList($format, $query, {}, 'id');

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        # Formatage des données
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'isTransportPlatform'} = ($tabRow->{'isTransportPlatform'} != 0 ? 1 : 0);

            my @tabEmails = split(/,/, $tabRow->{'email.list'});
            my $nbEmails = @tabEmails;
            for (my $i = 0; $i < $nbEmails; $i++)
            {
                my ($type, $value) = split(/:/, $tabEmails[$i]);
                $tabRow->{'email.' . $type} = $value;
            }
            delete $tabRow->{'email.list'};
        }
    }
    return (wantarray ? ($format == LOC::Util::GETLIST_ASSOC ? %$tab : @$tab) : $tab);
}


# Function: getByIP
# Retourne l'agence correspondant à l'adresse IP passée en paramètre
#
# Parameters:
# string  $ip - Adresse IP
#
# Returns:
# string - Identifiant de l'agence
sub getByIP
{
    my ($ip) = @_;

    $ip =~ /\d+\.\d+\.(\d+)\.\d+/;
    my $ipSlice = $1;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('frmwrk');

    my $query = '
SELECT agc_id
FROM p_agency
WHERE agc_sta_id <> ' . $db->quote(STATE_DELETED) . '
AND agc_ip REGEXP "(^|;)' . $ipSlice . '(;|$)";';

    return $db->fetchOne($query);
}


# Function: getAnalyticalCodesList
# Retourne la liste des codes analytiques par agence
#
# Returns:
# hash|hashref|0 - Liste des codes analytiques par agence
sub getAnalyticalCodesList
{
    # connexion à la base
    my $db = &LOC::Db::getConnection('frmwrk');

    my $query = '
SELECT
    agc_id,
    agc_analyticalaccount
FROM p_agency';

    my $tab = $db->fetchPairs($query);

    return (wantarray ? %$tab : $tab);
}


# Function: getLabelById
# Retourne le libellé d'une agence
#
# Parameters:
# string  $id - Identifiant de l'agence
#
# Returns:
# string - Libellé de l'agence
sub getLabelById
{
    my ($id) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('frmwrk');

    my $query = '
SELECT
    agc_label
FROM p_agency
WHERE agc_id = ' . $db->quote($id) . ';';

    return $db->fetchOne($query);
}


1;