use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Repair::OtherService
# Module de gestion des autres prestations de remise en état
package LOC::Repair::OtherService;


# Constants: États
# STATE_ACTIVE   - Alerte active
# STATE_INACTIVE - Alerte inactive
use constant
{
    STATE_ACTIVE   => 'GEN01',
    STATE_INACTIVE => 'GEN03',
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Util;


# Function: getInfos
# Retourne un hachage contenant les infos d'une autre prestation
#
# Parameters:
# string $countryId - Pays
# int    $id        - Id de la prestation
#
# Returns:
# hash|hashref
sub getInfos
{
    my ($countryId, $id, $flags) = @_;

    my $tab = getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Récupérer la liste des autres prestations
#
# Contenu du hachage:
# - id          => id
# - name        => libellé
# - state.id    => état
# - code        => code
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id)
# int     $flags      - Flags
#
# Returns:
# hash|hashref
sub getList
{
    my ($countryId, $format, $tabFilters, $flags) = @_;
    unless (defined $flags)
    {
        $flags = 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '';

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    `osv_id` AS `id`,
    `osv_name` AS `name`,
    `osv_code` AS `code`,
    `osv_sta_id` AS `state.id`
FROM p_otherservice
WHERE osv_sta_id <> ' . $db->quote(STATE_INACTIVE);
        if (defined $tabFilters->{'id'})
        {
            if (ref($tabFilters->{'id'}) eq 'ARRAY')
            {
                $query .= '
    AND `osv_id` IN (' . join(', ', @{$tabFilters->{'id'}}) . ')';
            }
            else
            {
                $query .= '
    AND `osv_id` = ' . $tabFilters->{'id'};
            }
        }
        $query .= '
ORDER BY osv_name'; 
    }
    elsif($format == LOC::Util::GETLIST_PAIRS)
    {
        $query = '
SELECT
    `osv_id` AS `id`,
    `osv_name` AS `name`
FROM p_otherservice
WHERE osv_sta_id <> ' . $db->quote(STATE_INACTIVE);
        if (defined $tabFilters->{'id'})
        {
            if (ref($tabFilters->{'id'}) eq 'ARRAY')
            {
                $query .= '
    AND `osv_id` IN (' . join(', ', @{$tabFilters->{'id'}}) . ')';
            }
            else
            {
                $query .= '
    AND `osv_id` = ' . $tabFilters->{'id'};
            }
        }
        $query .= '
ORDER BY osv_name'; 
    }

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    return (wantarray ? %$tab : $tab);
}

# Function: set
# Met à jour (insert/update) toutes les infos d'une prestation en BDD
#
# Contenu du hachage à passer en paramètre:
# - id          => id
# - name        => libellé
# - code        => code
#
# Parameters:
# string    $countryId - Pays
# hashref   $values    - Valeurs de la prestation
#
# Returns:
# integer
sub set
{
    my ($countryId, $values) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Mise à jour
    if (defined $values->{'id'} && $values->{'id'} != 0)
    {
        my $result = $db->update('p_otherservice', {
                                                 'osv_name'     => $values->{'name'},
                                                 'osv_code'     => $values->{'code'},
                                                 },
                                                 {
                                                 'osv_id'                => $values->{'id'}
                                                 });
                                                 
        return 1;
    }
    # Création
    else
    {
        my $osvId = $db->insert('p_otherservice', {
                                                 'osv_sta_id'   => 'GEN01',
                                                 'osv_name'     => $values->{'name'},
                                                 'osv_code'     => $values->{'code'}
                                                 });
        
        return $osvId;
    }
}

# Function: delete
# Supprime une prestation (change l'état à GEN03)
#
# Parameters:
# string    $countryId   - Pays
# integer   $id          - Id de la prestation
#
# Returns:
# integer
sub delete
{
    my ($countryId, $id) = @_;
    
    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    
    return $db->update('p_otherservice', {'osv_sta_id' => 'GEN03'}, {'osv_id' => $id});
}

1;