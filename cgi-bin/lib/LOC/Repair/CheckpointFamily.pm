use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Repair::CheckpointFamily
# Module de gestion des modèles de fiche de casse
package LOC::Repair::CheckpointFamily;


use constant
{
    FAMILIES_BY_CHECKPOINTID  => 0x0000,
    CHECKPOINTS_BY_FAMILYID   => 0x0001,
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Util;

# Function: getOrders
# Récupérer la liste des couples (famille machine | point de vérification, ordre d'affichage)
# liés à une famille ou à un point
#
# Contenu du hachage :
# - id      => id de la famille ou du point
# - order   => ordre d'affichage
#
# Parameters:
# string  $countryId    - Pays
# int     $id           - Id du point de vérification ou de la liste
# int     $flag         - Flag (FAMILIES_BY_CHECKPOINTID : ordre des familles, CHECKPOINTS_BY_FAMILYID : ordre des points)
#
# Returns:
# hash|hashref
sub getOrders
{
    my ($countryId, $id, $flag) = @_;
    unless (defined $flag)
    {
        $flag = 0;
    }
    
    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    
    my $query = '
SELECT';
    if ($flag == FAMILIES_BY_CHECKPOINTID)
    {
        $query .= '
    `chf_fly_id` AS `id`,';
    }
    else
    {
        $query .= '
    `chf_chp_id` AS `id`,';
    }
    $query .= '`chf_order` AS `order`
FROM p_checkpoint_family
WHERE 1';
    if ($flag == FAMILIES_BY_CHECKPOINTID)
    {
        $query .= '
    AND `chf_chp_id` = ' . $id;
    }
    else
    {
        $query .= '
    AND `chf_fly_id` = ' . $id;
    }
    
    my $tab = $db->fetchAssoc($query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    return (wantarray ? %$tab : $tab);    
}

# Function: set
# Met à jour toutes les associations famille machine/point de vérification en BDD
#
# Contenu du hachage :
# - id      => id de la famille ou du point
# - order   => ordre d'affichage
#
# Parameters:
# string  $countryId    - Pays
# hashref $values       - Valeurs de l'association
# int     $id           - Id du point de vérification ou de la liste
# int     $flag         - Flag (FAMILIES_BY_CHECKPOINTID : ordre des familles, FAMILIES_BY_CHECKPOINTID : ordre des points)
#
# Returns:
# integer
sub setOrders
{
    my ($countryId, $values, $id, $flag) = @_;
    unless (defined $flag)
    {
        $flag = 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
     
    my $filter;
    if ($flag == FAMILIES_BY_CHECKPOINTID)
    {
       $filter = {'chf_chp_id' => $id};
    }
    else
    {
       $filter = {'chf_fly_id' => $id}; 
    }

    # suppression des associations
    $db->delete('p_checkpoint_family', $filter);
        
    # ajout des associations
    while (my ($entityId, $order) = each(%$values))
    {
        my $hash = {'chf_order' => $order};
        if ($flag == FAMILIES_BY_CHECKPOINTID)
        {
            $hash->{'chf_chp_id'} = $id;
            $hash->{'chf_fly_id'} = $entityId;
        }
        else
        {
            $hash->{'chf_fly_id'} = $id;
            $hash->{'chf_chp_id'} = $entityId;
        }

        $db->insert('p_checkpoint_family', $hash);
    }

    return 1;
}
1;