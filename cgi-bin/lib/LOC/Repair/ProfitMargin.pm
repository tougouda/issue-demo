use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Repair::ProfitMargin
# Module de gestion des marges sur les points de contrôle
package LOC::Repair::ProfitMargin;


# Constants: États
# STATE_ACTIVE   - Alerte active
# STATE_INACTIVE - Alerte inactive
use constant
{
    STATE_ACTIVE   => 'GEN01',
    STATE_INACTIVE => 'GEN03',
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Util;

#Tableau contenant les valeurs des marges par tranche de prix
my %prmList;

# Function: getAmountByCost
# Retourne le montant de la marge à appliquer
#
# Parameters:
# string $countryId - Pays
# float  $cost      - Prix du changement de piéce
#
# Returns:
# float
sub getAmountByCost
{
    my ($countryId, $cost) = @_;

    if (scalar(keys(%prmList)) == 0)
    {
        %prmList = getList($countryId, LOC::Util::GETLIST_ASSOC);
    }

    foreach my $prmId (keys %prmList)
    {
        if ($cost == 0)
        {
            return 0
        }
        elsif ($cost >= $prmList{$prmId}->{'lowerCost'} && $cost < $prmList{$prmId}->{'upperCost'})
        {
            return $prmList{$prmId}->{'amount'};
        }
    }

    return 0;
}

# Function: getInfos
# Retourne un hachage contenant les infos d'une marge
#
# Parameters:
# string $countryId - Pays
# int    $id        - Id de la marge
#
# Returns:
# hash|hashref
sub getInfos
{
    my ($countryId, $id, $flags) = @_;

    my $tab = getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Récupérer la liste des marges
#
# Contenu du hachage:
# - id          => id
# - name        => libellé
# - code        => code
# - state.id    => id état
# - currency.id => id devise
# - lowerCost   => borne inférieure
# - upperCost   => borne supérieure
# - amount      => montant de la marge
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id, cost)
# int     $flags      - Flags
#
# Returns:
# hash|hashref
sub getList
{
    my ($countryId, $format, $tabFilters, $flags) = @_;
    unless (defined $flags)
    {
        $flags = 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '';

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    prm_id AS `id`,
    prm_name AS `name`,
    prm_code AS `code`,
    prm_sta_id AS `state.id`,
    prm_cur_id AS `currency.id`,
    prm_lowercost AS `lowerCost`,
    prm_uppercost AS `upperCost`,
    prm_amount AS `amount`
FROM p_profitmargin
WHERE prm_sta_id <> ' . $db->quote(STATE_INACTIVE);
        if (defined $tabFilters->{'id'})
        {
            if (ref($tabFilters->{'id'}) eq 'ARRAY')
            {
                $query .= '
    AND `prm_id` IN (' . join(', ', @{$tabFilters->{'id'}}) . ')';
            }
            else
            {
                $query .= '
    AND `prm_id` = ' . $tabFilters->{'id'};
            }
        }
        if (defined $tabFilters->{'cost'})
        {
            $query .= '
    AND (' . $tabFilters->{'cost'} . ' >= `prm_lowercost` AND ' . $tabFilters->{'cost'} . ' < `prm_uppercost`)';
        }
        $query .= '
ORDER BY prm_lowercost, prm_uppercost;';
    }

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    return (wantarray ? %$tab : $tab);
}

# Function: set
# Met à jour (insert/update) toutes les infos d'une marge en BDD
#
# Contenu du hachage à passer en paramètre:
# - id          => id
# - name        => libellé
# - code        => code
# - currency.id => id devise
# - lowerCost   => borne inférieure
# - upperCost   => borne supérieure
# - amount      => montant de la marge
#
# Parameters:
# string    $countryId - Pays
# hashref   $values    - Valeurs de la marge
#
# Returns:
# integer
sub set
{
    my ($countryId, $values) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Mise à jour
    if (defined $values->{'id'} && $values->{'id'} != 0)
    {
        my $result = $db->update('p_profitmargin', {
                                                 'prm_cur_id'       => $values->{'currency.id'},
                                                 'prm_name'         => $values->{'name'},
                                                 'prm_code'         => $values->{'code'},
                                                 'prm_lowercost'    => $values->{'lowerCost'},
                                                 'prm_uppercost'    => $values->{'upperCost'},
                                                 'prm_amount'       => $values->{'amount'},
                                                 },
                                                 {
                                                 'prm_id'                => $values->{'id'}
                                                 });
                                                 
        return 1;
    }
    # Création
    else
    {
        my $srvId = $db->insert('p_profitmargin', {
                                                 'prm_sta_id'       => 'GEN01',
                                                 'prm_cur_id'       => $values->{'currency.id'},
                                                 'prm_name'         => $values->{'name'},
                                                 'prm_code'         => $values->{'code'},
                                                 'prm_lowercost'    => $values->{'lowerCost'},
                                                 'prm_uppercost'    => $values->{'upperCost'},
                                                 'prm_amount'       => $values->{'amount'},
                                                 });
        
        return $srvId;
    }
}

# Function: delete
# Supprime une prestation (change l'état à GEN03)
#
# Parameters:
# string    $countryId   - Pays
# integer   $id          - Id de la marge
#
# Returns:
# hash|hashref
sub delete
{
    my ($countryId, $id) = @_;
    
    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    
    return $db->update('p_profitmargin', {'prm_sta_id' => 'GEN03'}, {'prm_id' => $id});
}

1;