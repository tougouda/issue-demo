use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Repair::Checkpoint
# Module de gestion des points de contrôle
package LOC::Repair::Checkpoint;


# Constants: États
# STATE_ACTIVE   - Alerte active
# STATE_INACTIVE - Alerte inactive
use constant
{
    STATE_ACTIVE   => 'GEN01',
    STATE_INACTIVE => 'GEN03',
};

# Constants: Flags
# GETINFOS_MARGIN  - Récupère les informations sur la marge
# GETINFOS_DELETED - Récupère les informations des points de vérification supprimés
use constant
{
    GETINFOS_MARGIN  => 0x0001,
    GETINFOS_DELETED => 0x0002,
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Util;
use LOC::Repair::Article;
use LOC::Repair::CheckpointFamily;

# Function: getInfos
# Retourne un hachage contenant les infos du point de contrôle
#
# Contenu du hachage:
# - currency.id          => id de la devise
# - state.id             => id de l'état
# - label                => libellé du point de contrôle
# - changePartAllowed    => indique si le changement de pièce est autorisé
# - repairAllowed        => indique si la réparation est autorisée
# - fixAllowed           => indique si les petites réparations sont autorisées
# - articleRequired      => indique si l'on doit saisir un code article
# - articles.averageCost => prix moyen des articles liés au point de contrôle
# - repairCost           => coût forfaitaire de réparation
# - fixCost              => coût forfaitaire des petites réparations
# - dateCreation         => date de création
# - dateModification     => date de modification
# - family.order         => ordre d'affichage par rapport à la famille machine
#
# Parameters:
# string $countryId - Pays
# int    $id        - Id du point de contrôle
#
# Returns:
# hash|hashref
sub getInfos
{
    my ($countryId, $id, $flags) = @_;

    my $tab = getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Récupérer la liste des points de contrôle
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id, checkpoint, family)
# int     $flags      - Flags
#
# Returns:
# hash|hashref
sub getList
{
    my ($countryId, $format, $tabFilters, $flags) = @_;
    unless (defined $tabFilters)
    {
        $tabFilters = {};
    }
    unless (defined $flags)
    {
        $flags = 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '';

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    `chp_id` AS `id`,
    `chp_cur_id` AS `currency.id`,
    `chp_sta_id` AS `state.id`,
    `chp_label` AS `label`,
    `chp_is_chgpartallowed` AS `changePartAllowed`,
    `chp_is_repairallowed` AS `repairAllowed`,
    `chp_is_fixallowed` AS `fixAllowed`,
    `chp_is_articlerequired` AS `articleRequired`,
    `chp_is_marginallowed` AS `marginAllowed`,
    (
        SELECT IFNULL(AVG(IFNULL(art_purchasecost, 0)), 0)
        FROM p_article
        WHERE art_chp_id = chp_id
    ) AS `articles.averageCost`,
    `chp_repaircost` AS `repairCost`,
    `chp_fixcost` AS `fixCost`,
    `chp_date_creation` AS `dateCreation`,
    `chp_date_modification` AS `dateModification`,
    `chf_order` AS `family.order`';

        $query .= '
FROM `p_checkpoint`
INNER JOIN `p_checkpoint_family` ON chf_chp_id = chp_id';

        $query .= '
WHERE TRUE';
        unless ($flags & GETINFOS_DELETED)
        {
            $query .= '
    AND `chp_sta_id` != ' . $db->quote(STATE_INACTIVE);
        }
        if (defined $tabFilters->{'id'})
        {
            if (ref($tabFilters->{'id'}) eq 'ARRAY')
            {
                $query .= '
    AND `chp_id` IN (' . join(', ', @{$tabFilters->{'id'}}) . ')';
            }
            else
            {
                $query .= '
    AND `chp_id` = ' . $tabFilters->{'id'};
            }
        }
        if (defined $tabFilters->{'family'})
        {
            if (ref($tabFilters->{'family'}) eq 'ARRAY')
            {
                $query .= '
    AND `chf_fly_id` IN (' . join(', ', @{$tabFilters->{'family'}}) . ')';
            }
            else
            {
                $query .= '
    AND `chf_fly_id` = ' . $tabFilters->{'family'};
            }
        }
        if (defined $tabFilters->{'family'})
        {
            $query .= '
ORDER BY `chf_fly_id`, `chf_order`, `chp_label`';
        }
        else
        {
            $query .= '
ORDER BY `chp_label`;';
        }
    }


    my $tab = $db->fetchList($format, $query, {}, 'id');
    
    if ($flags & GETINFOS_MARGIN)
    {
         # rajout de la marge
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'margin'} = &LOC::Repair::ProfitMargin::getAmountByCost($countryId, $tabRow->{'articles.averageCost'});
        }
    }
    if (!$tab)
    {
        return undef;
    }

    return (wantarray ? %$tab : $tab);
}

# Function: set
# Met à jour (insert/update) toutes les infos d'un point de vérification en BDD
#
# Contenu du hachage à passer en paramètre:
# - currency.id          => id de la devise
# - label                => libellé du point de contrôle
# - changePartAllowed    => indique si le changement de pièce est autorisé
# - repairAllowed        => indique si la réparation est autorisée
# - fixAllowed           => indique si les petites réparations sont autorisées
# - articleRequired      => indique si l'on doit saisir un code article
# - repairCost           => coût forfaitaire de réparation
# - fixCost              => coût forfaitaire des petites réparations
# - articles             => liste des articles à rattacher au point de vérification :
#                           - code          => code article
#                           - externalId    => id Kimoce de l'article
#                           - purchaseCost  => prix du fournisseur privilégié
# - families             => liste d'ordre pour les familles indexées sur l'id famille
#
# Parameters:
# string    $countryId - Pays
# hashref   $values    - Valeurs du point de vérification
#
# Returns:
# integer
sub set
{
    my ($countryId, $values) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Mise à jour
    if (defined $values->{'id'} && $values->{'id'} != 0)
    {
        my $result = $db->update('p_checkpoint', {
                                                 'chp_cur_id'            => $values->{'currency.id'},
                                                 'chp_label'             => $values->{'label'},
                                                 'chp_is_chgpartallowed' => $values->{'changePartAllowed'},
                                                 'chp_is_repairallowed'  => $values->{'repairAllowed'},
                                                 'chp_is_fixallowed'     => $values->{'fixAllowed'},
                                                 'chp_is_articlerequired'=> $values->{'articleRequired'},
                                                 'chp_is_marginallowed'  => $values->{'marginAllowed'},
                                                 'chp_repaircost'        => $values->{'repairCost'},
                                                 'chp_fixcost'           => $values->{'fixCost'},
                                                 },
                                                 {
                                                 'chp_id'                => $values->{'id'}
                                                 });
        # ajout et mise à jour des articles
        my @articlesId; 
        my $articles = $values->{'articles'};
        while (my ($i, $article) = each(%$articles))
        {
            if (defined $article->{'id'} && $article->{'id'} ne '')
            {
                # pas de modification des articles (mise à jour par cron)
                push(@articlesId, $article->{'id'});
            }
            else
            {
                # enregistrement des nouveaux articles
                if ($article->{'code'} ne '' && $article->{'purchaseCost'} ne '' && $article->{'externalId'} ne '')
                {
                    my $articleId = &LOC::Repair::Article::set($countryId, $article,
                                                               $values->{'id'}, $values->{'currency.id'});
                    
                    if ($articleId != -1)
                    {
                        push(@articlesId, $articleId);
                    }
                }
            }
        }
        # suppression en base des articles supprimés
        &LOC::Repair::Article::delete($countryId, $values->{'id'}, \@articlesId);
        
        # mise à jour des associations famille machine / point de vérification
        &LOC::Repair::CheckpointFamily::setOrders($countryId, $values->{'families'}, $values->{'id'},
                                                  LOC::Repair::CheckpointFamily::FAMILIES_BY_CHECKPOINTID);

        return 1;
    }
    # Création
    else
    {
        my $chpId = $db->insert('p_checkpoint', {
                                                 'chp_cur_id'            => $values->{'currency.id'},
                                                 'chp_sta_id'            => 'GEN01',
                                                 'chp_label'             => $values->{'label'},
                                                 'chp_is_chgpartallowed' => $values->{'changePartAllowed'},
                                                 'chp_is_repairallowed'  => $values->{'repairAllowed'},
                                                 'chp_is_fixallowed'     => $values->{'fixAllowed'},
                                                 'chp_is_articlerequired'=> $values->{'articleRequired'},
                                                 'chp_is_marginallowed'  => $values->{'marginAllowed'},
                                                 'chp_repaircost'        => $values->{'repairCost'},
                                                 'chp_fixcost'           => $values->{'fixCost'},
                                                 'chp_date_creation*'    => 'NOW()',
                                                 });
        if ($chpId != -1)
        {
            my $articles = $values->{'articles'};
            while (my ($i, $article) = each(%$articles))
            {
                if ($article->{'code'} ne '' && $article->{'purchaseCost'} ne '' && $article->{'externalId'} ne '')
                {
                    &LOC::Repair::Article::set($countryId, $article, $chpId, $values->{'currency.id'});
                }
            }

            # mise à jour des associations famille machine / point de vérification
            &LOC::Repair::CheckpointFamily::setOrders($countryId, $values->{'families'}, $chpId,
                                                      LOC::Repair::CheckpointFamily::FAMILIES_BY_CHECKPOINTID);

        }
        
        return $chpId;
    }
}

# Function: delete
# Supprime un point de vérification (change l'état à GEN03)
#
# Parameters:
# string    $countryId   - Pays
# integer   $id          - Id du point de vérification
#
# Returns:
# integer
sub delete
{
    my ($countryId, $id) = @_;
    
    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    
    return $db->update('p_checkpoint', {'chp_sta_id' => STATE_INACTIVE}, {'chp_id' => $id});
}

1;