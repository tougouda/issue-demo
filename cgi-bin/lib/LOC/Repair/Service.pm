use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Repair::Service
# Module de gestion des prestations de remise en état
package LOC::Repair::Service;


# Constants: États
# STATE_ACTIVE   - Alerte active
# STATE_INACTIVE - Alerte inactive
use constant
{
    STATE_ACTIVE   => 'GEN01',
    STATE_INACTIVE => 'GEN03',
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Util;
use LOC::Locale;


# Function: getInfos
# Retourne un hachage contenant les infos d'une prestation
#
# Parameters:
# string $countryId - Pays
# int    $id        - Id de la prestation
#
# Returns:
# hash|hashref
sub getInfos
{
    my ($countryId, $id, $flags) = @_;

    my $tab = getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Récupérer la liste prestations
#
# Contenu du hachage:
# - id          => id
# - name        => libellé
# - state.id    => état
# - currency.id => id devise
# - type        => type 
# - cost        => coût de la prestation
# - amount      => montant de la prestation
# - infos       => Informations sur le service
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id)
# int     $flags      - Flags
#
# Returns:
# hash|hashref
sub getList
{
    my ($countryId, $format, $tabFilters, $flags) = @_;
    unless (defined $flags)
    {
        $flags = 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '';

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    srv_id AS `id`,
    srv_name AS `name`,
    srv_code AS `code`,
    srv_sta_id AS `state.id`,
    srv_cur_id AS `currency.id`,
    srv_type AS `type`,
    srv_cost AS `cost`,
    srv_amount AS `amount`,
    srv_is_marginallowed AS `isMarginAllowed`,
    srv_is_quantityallowed AS `isQuantityAllowed`,
    srv_comment AS `infos`
FROM p_service
WHERE srv_sta_id <> ' . $db->quote(STATE_INACTIVE);
        if (defined $tabFilters->{'id'})
        {
            if (ref($tabFilters->{'id'}) eq 'ARRAY')
            {
                $query .= '
    AND `srv_id` IN (' . join(', ', @{$tabFilters->{'id'}}) . ')';
            }
            else
            {
                $query .= '
    AND `srv_id` = ' . $tabFilters->{'id'};
            }
        }
        $query .= '
ORDER BY srv_name'; 
    }

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    return (wantarray ? %$tab : $tab);
}

# Function: getTypesList
# Récupérer la liste des types de prestation
#
# Contenu du hachage:
# libellé indexé sur l'id
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# string  $locale     - Locale
#
# Returns:
# hash|hashref
sub getTypesList
{
    my ($countryId, $format, $localeId) = @_;

    my $locale = &LOC::Locale::getLocale($localeId);

    my $tab = {
                'EXT' => $locale->t('Externe'),
                'I/E' => $locale->t('Interne / Externe'),
                'INT' => $locale->t('Interne'),
                'INM' => $locale->t('Interne modifiable')
                };

    return (wantarray ? %$tab : $tab);
}

# Function: set
# Met à jour (insert/update) toutes les infos d'une prestation en BDD
#
# Contenu du hachage à passer en paramètre:
# - id          => id
# - name        => libellé
# - state.id    => état
# - currency.id => id devise
# - type        => type 
# - cost        => coût de la prestation
# - amount      => montant de la prestation
#
# Parameters:
# string    $countryId - Pays
# hashref   $values    - Valeurs de la prestation
#
# Returns:
# integer
sub set
{
    my ($countryId, $values) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Mise à jour
    if (defined $values->{'id'} && $values->{'id'} != 0)
    {
        my $result = $db->update('p_service', {
                                                 'srv_cur_id'             => $values->{'currency.id'},
                                                 'srv_name'               => $values->{'name'},
                                                 'srv_code'               => $values->{'code'},
                                                 'srv_type'               => $values->{'type'},
                                                 'srv_cost'               => $values->{'cost'},
                                                 'srv_amount'             => ($values->{'type'} eq 'INM' ? $values->{'cost'} : $values->{'amount'}),
                                                 'srv_is_marginallowed'   => $values->{'isMarginAllowed'},
                                                 'srv_is_quantityallowed' => $values->{'isQuantityAllowed'},
                                                 'srv_comment'            => $values->{'infos'}
                                                 },
                                                 {
                                                 'srv_id'                => $values->{'id'}
                                                 });
                                                 
        return 1;
    }
    # Création
    else
    {
        if (!defined $values->{'isMarginAllowed'})
        {
            $values->{'isMarginAllowed'} = 0;
        }
        if (!defined $values->{'isQuantityAllowed'})
        {
            $values->{'isQuantityAllowed'} = 0;
        }
        
        my $srvId = $db->insert('p_service', {
                                                 'srv_sta_id'             => STATE_ACTIVE,
                                                 'srv_cur_id'             => $values->{'currency.id'},
                                                 'srv_name'               => $values->{'name'},
                                                 'srv_code'               => $values->{'code'},
                                                 'srv_type'               => $values->{'type'},
                                                 'srv_cost'               => $values->{'cost'},
                                                 'srv_amount'             => $values->{'amount'},
                                                 'srv_is_marginallowed'   => $values->{'isMarginAllowed'},
                                                 'srv_is_quantityallowed' => $values->{'isQuantityAllowed'},
                                                 'srv_comment'            => $values->{'infos'}
                                                 });
        
        return $srvId;
    }
}

# Function: delete
# Supprime une prestation (change l'état à GEN03)
#
# Parameters:
# string    $countryId   - Pays
# integer   $id          - Id de la prestation
#
# Returns:
# integer
sub delete
{
    my ($countryId, $id) = @_;
    
    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    
    return $db->update('p_service', {'srv_sta_id' => STATE_INACTIVE}, {'srv_id' => $id});
}

1;