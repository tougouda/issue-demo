use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Repair::Article
# Module de gestion des articles liés aux points de contrôle
package LOC::Repair::Article;
use strict;

# Modules internes
use LOC::Db;
use LOC::Util;
use LOC::Country;
use LOC::Currency;
use LOC::Characteristic;


# Function: getInfos
# Retourne un hachage contenant les infos de l'article
#
# Parameters:
# string $countryId - Pays
# int    $id        - Id de l'article
#
# Returns:
# hash|hashref
sub getInfos
{
    my ($countryId, $id, $flags) = @_;

    my $tab = getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Récupérer la liste des articles
#
# Contenu du hachage:
# - id               => id
# - checkpoint.id    => id du point de contrôle
# - currency.id      => id devise
# - code             => code article
# - externalId       => id de l'article dans Kimoce
# - purchaseCost     => prix pratiqué standard de l'article
# - dateCreation     => date de création
# - dateModification => date de dernière modification
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id, checkpoint)
# int     $flags      - Flags
#
# Returns:
# hash|hashref
sub getList
{
    my ($countryId, $format, $tabFilters, $flags) = @_;
    unless (defined $flags)
    {
        $flags = 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '';
    
    # Filtres
    my $whereQuery = '';
    # - Article
    if (defined $tabFilters->{'id'})
    {
        if (ref($tabFilters->{'id'}) eq 'ARRAY')
        {
            $whereQuery .= '
AND art_id IN (' . join(', ', @{$tabFilters->{'id'}}) . ')';
        }
        else
        {
            $whereQuery .= '
AND art_id = ' . $tabFilters->{'id'};
        }
    }
    if (defined $tabFilters->{'checkpoint'})
    {
        if (ref($tabFilters->{'checkpoint'}) eq 'ARRAY')
        {
            $whereQuery .= '
AND art_chp_id IN (' . join(', ', @{$tabFilters->{'checkpoint'}}) . ')';
        }
        else
        {
            $whereQuery .= '
AND art_chp_id = ' . $tabFilters->{'checkpoint'};
        }
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    `art_id` AS `id`,
    `art_chp_id` AS `checkpoint.id`,
    `art_cur_id` AS `currency.id`,
    `art_code` AS `code`,
    `art_externalid` AS `externalId`,
    `art_purchasecost` AS `purchaseCost`,
    `art_date_creation` AS `dateCreation`,
    `art_date_modification` AS `dateModification`
FROM `p_article`
WHERE 1' . 
$whereQuery . '
ORDER BY `art_code`';
    }

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    return (wantarray ? %$tab : $tab);
}

# Function: getListFromKimoce
# Récupérer une liste d'article Kimoce
#
# Contenu du hachage:
# - id          => id de l'article
# - code        => code article
# - cost        => prix pratiqué par le fournisseur privilégié
# - currency.id => identifiant de la devise
#
# Parameters:
# string  $countryId  - Pays
# hashref $tabFilters - Liste des filtres (id, code)
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hash|hashref
sub getListFromKimoce
{
    my ($countryId, $tabFilters, $tabOptions) = @_;

    # Connexion à Kimoce non disponible
    if (!&LOC::Globals::get('connectToKimoce'))
    {
        return undef;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('kimoce');

    my $tariffId = &LOC::Characteristic::getCountryValueByCode('IDTARIFKIM', $countryId);

    my $query = '
SELECT';

    if (defined $tabOptions->{'limit'})
    {
        $query .= '
TOP ' . $tabOptions->{'limit'};
    }

    $query .= '
    p_obj.ObjInCde AS id,
    p_obj.ObjExCde AS code,
    p_objsppl.ObjSpplObjCde AS supplierCode,
    p_obj.ObjDsc AS label,
    p_objprc.objPrc1Val AS cost,
    p_objprc.objPrc1Val AS localCost,
    UnitExCde AS "currency.id"
FROM p_objprc
    INNER JOIN p_obj ON p_obj.ObjInCde = p_objprc.ObjInCde
    INNER JOIN r_unit on r_unit.UnitInCde = p_objprc.UnitInCde
    INNER JOIN p_objsppl ON p_obj.ObjInCde = p_objsppl.ObjInCde
    INNER JOIN p_objspplprf ON p_objspplprf.ObjSpplInCde = p_objsppl.ObjSpplInCde
WHERE p_objprc.ObjRteInCde = ' . $db->quote($tariffId) . '
AND p_objspplprf.OwnerCpyInCde = 1000';

    if (defined $tabFilters->{'id'})
    {
        $query .= '
AND p_obj.ObjInCde = ' . $tabFilters->{'id'};
    }
    if (defined $tabFilters->{'code'})
    {
        $query .= '
AND p_obj.ObjExCde = ' . $db->quote($tabFilters->{'code'});
    }
    if (defined $tabFilters->{'code%'})
    {
        $query .= '
AND (p_obj.ObjExCde LIKE ' . $db->quote($tabFilters->{'code%'} . '%') . '
     OR p_objsppl.ObjSpplObjCde LIKE ' . $db->quote($tabFilters->{'code%'} . '%') . ')';
    }

    $query .= ';';
    my $tab = $db->fetchList(LOC::Db::FETCH_ASSOC, $query);
    if (!$tab)
    {
        return undef;
    }

    foreach my $tabRow (@$tab)
    {
        if ($tabRow->{'cost'})
        {
            $tabRow->{'cost'} =~ s/,/./;
        }
    
        # Taux de conversion à la date courante
        my $euroRate = &LOC::Country::getEuroRate($countryId);
    
        # Conversion de l'euro vers la devise locale
        if ($tabRow->{'cost'} && $tabRow->{'currency.id'} eq LOC::Currency::DEFAULT_CURRENCY_ID && $euroRate)
        {
            $tabRow->{'localCost'} = &LOC::Util::round($tabRow->{'cost'} / $euroRate, 2);
        }

        #Renvoi le montant de la marge de l'article
        if ($tabOptions->{'withMargin'})
        {
            $tabRow->{'margin'} = &LOC::Repair::ProfitMargin::getAmountByCost($countryId,  $tabRow->{'localCost'});
        }
        else
        {
            $tabRow->{'margin'} = 0;
        }
    }

    return (wantarray ? @$tab : $tab);
}


# Function: getInfosFromKimoce
# Récupérer les informations de Kimoce sur un code article
#
# Contenu du hachage:
# - id          => id de l'article
# - code        => code article
# - cost        => prix pratiqué par le fournisseur privilégié
# - currency.id => identifiant de la devise
#
# Parameters:
# string  $countryId  - Pays
# hashref $tabFilters - Liste des filtres (id, code)
#
# Returns:
# hash|hashref
sub getInfosFromKimoce
{
    my ($countryId, $tabFilters, $tabOptions) = @_;

    my $tab = &getListFromKimoce($countryId, $tabFilters, $tabOptions);
    return (defined $tab ? (wantarray ? %{$tab->[0]} : $tab->[0]) : undef);

}

# Function: set
# Met à jour (insert/update) toutes les infos d'un article en BDD
#
# Contenu du hachage à passer en paramètre:
# - code
# - externalId
# - purchaseCost
#
# Parameters:
# string    $countryId    - Pays
# hashref   $values       - Valeurs de l'article
# integer   $checkpointId - Id du point de vérification
# integer   $currencyId   - Id de la devise
#
# Returns:
# integer
sub set
{
    my ($countryId, $values, $checkpointId, $currencyId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    
    my $articleId = $db->insert('p_article', {
                                              'art_chp_id'          => $checkpointId,
                                              'art_cur_id'          => $currencyId,
                                              'art_code'            => $values->{'code'},
                                              'art_externalid'      => $values->{'externalId'},
                                              'art_purchasecost'    => $values->{'purchaseCost'},
                                              'art_date_creation*'  => 'NOW()'
                                              });
                                              
    return $articleId;
}

# Function: delete
# Supprime les articles d'un point de vérification en BDD
#
# Contenu du hachage à passer en paramètre:
#
# Parameters:
# string    $countryId    - Pays
# hashref   $values       - Valeurs de l'article
# integer   $checkpointId - Id du point de vérification
# arrayref  $exceptIds    - Ids article à ne pas supprimer
#
# Returns:
# integer
sub delete
{
    my ($countryId, $checkpointId, $exceptIds) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    
    my $query = '
DELETE
FROM p_article
WHERE art_chp_id = ' . $checkpointId;
        if (@$exceptIds > 0)
        {
            $query .= '
    AND art_id NOT IN (' . join(', ', @$exceptIds) . ')';
        }
        $db->query($query);

    return 1;
}

# Function: updateAllCosts
# Met à jour le coût de tous les articles avec le prix du frs privilégié récupéré dans Kimoce
#
# Parameters:
# string  $countryId  - Pays
#
# Returns:
# integer
sub updateAllCosts
{
    my ($countryId) = @_;
    
    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    
    my $tabArticles = &getList($countryId, LOC::Util::GETLIST_ASSOC);
    while (my ($id, $article) = each(%$tabArticles))
    {
        # Récupération des informations de l'article dans Kimoce
        my $tabKimoceInfos = &getInfosFromKimoce($countryId, {'id' => $article->{'externalId'}});

        # Calcul du coût dans la devise locale
        my $cost = $tabKimoceInfos->{'localCost'};

        # Mise à jour
        $db->update('p_article', {'art_purchasecost' => $cost}, {'art_id' => $id});
    }
    
    return 1;
}

1;