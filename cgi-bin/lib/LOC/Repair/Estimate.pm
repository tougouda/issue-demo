use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Repair::Estimate
# Module de gestion des devis de remise en état
package LOC::Repair::Estimate;


use constant
{
    STATE_WAITING  => 'CAS01',
    STATE_APPROVED => 'CAS02',
    STATE_TURNEDIN => 'CAS03',
    STATE_ABORTED  => 'CAS04',
    STATE_INVOICED => 'CAS05',
    STATE_NOCHANGE => 'CAS07', #Devis abandoné avec reactivation puis modification de la fiche

    GETINFOS_CONTRACT      => 0x0002,
    GETINFOS_MACHINE       => 0x0004,
    GETINFOS_CUSTOMER      => 0x0008,
    GETINFOS_USER          => 0x0010,
    GETINFOS_REPAIRINVOICE => 0x0016,

     #Flag de bascule
    FLAG_TURNEDINSERVICES => 1, # Bascule en services
    FLAG_TURNEDINDAYS     => 2, # Bascule en jours
    FLAG_SHEETUPDATED     => 3  # Modification sur la fiche
};

# Modules internes
use LOC::Db;
use LOC::Util;
use LOC::Characteristic;
use LOC::Invoice;
use LOC::Invoice::ArticleRef;
use LOC::Json;
use LOC::Repair::OtherService;
use LOC::Repair::LineType;
use LOC::Proforma;


use strict;

# Function: getStatesList
# Retourne la liste des etats
#
# Parameters:
# string    $countryId      - Pays
# arrayref  $tabFilters     - Filtres
#
# Returns:
# hash
sub getStatesList
{
    my ($countryId, $tabFilters) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    if (!defined $tabFilters)
    {
        $tabFilters = [STATE_WAITING, STATE_APPROVED, STATE_ABORTED, STATE_INVOICED];
    }

    my $query = '
SELECT
    ETCODE AS `id`,
    ETLIBELLE AS `label`
FROM ETATTABLE
WHERE ETCODE IN (' . $db->quote($tabFilters) . ') ORDER BY ETLIBELLE';


    return $db->fetchPairs($query);
}


# Function: getInfos
# Retourne un hachage contenant les infos du devis de remise en état
#
# Contenu du hachage:
# - sheet.id             => id de la fiche de remise en état
# - agency.id            => id de l'agence du devis
# - user.id              => id de l'utilisateur ayant saisi le devis
# - currency.id          => id de la devise
# - state.id             => id de l'état
# - code                 => code du devis
# - orderCode            => n° de commande
# - object               => objet du devis
# - comments             => commentaires client
# - notes                => commentaires internes
# - totalCost            => coût total
# - totalAmount          => montant total
# - reductionPercent     => % de remise
# - reduction            => montant remisé
# - total                => total à facturer HT
# - vatPercent           => % de TVA
# - vat                  => montant de la TVA
# - taxesIncluded        => total à facturer TTC
# - dateInvoice          => date du passage en facturation
# - dateCreation         => date de création
# - dateModification     => date de modification
#
# Parameters:
# string $countryId - Pays
# int    $id        - Id du devis de remise en état
#
# Returns:
# hash|hashref
sub getInfos
{
    my ($countryId, $id, $flags) = @_;

    my $tab = getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Récupérer la liste des devis de remise en état
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id)
# int     $flags      - Flags
#
# Returns:
# hash|hashref
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    unless (defined $tabFilters)
    {
        $tabFilters = {};
    }
    unless (defined $flags)
    {
        $flags = 0;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT ' . (defined $tabOptions->{'limit'} && exists ($tabOptions->{'limit'}->{'&total'}) ? 'SQL_CALC_FOUND_ROWS' : '');

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query .= '
    rpe_id AS `id`,
    rpe_rps_id AS `sheet.id`,
    rpe_agc_id AS `agency.id`,
    rpe_sta_id AS `state.id`,
    rpe_usr_id AS `user.id`,
    rpe_cur_id AS `currency.id`,
    rpe_code AS `code`,
    rpe_ordercode AS `orderCode`,
    rpe_object AS `object`,
    rpe_comments AS `comments`,
    rpe_notes AS `notes`,
    rpe_residuesmode AS `residuesMode`,
    rpe_residuesvalue AS `residuesValue`,
    rpe_totalcost AS `totalCost`,
    rpe_totalamount AS `totalAmount`,
    rpe_reductionpercent AS `reductionPercent`,
    rpe_reduction AS `reduction`,
    rpe_total AS `total`,
    rpe_vatpercent AS `vatPercent`,
    rpe_vat AS `vat`,
    rpe_taxesincluded AS `taxesIncluded`,
    rpe_date_invoice AS `dateInvoice`,
    rpe_date_creation AS `dateCreation`,
    rpe_date_modification AS `dateModification`,
    rpe_flag AS `flag`,
    rpe_interlocutor AS `interlocutor`,
    tbl_p_state.ETLIBELLE AS `state.label`';
        if ($flags & GETINFOS_MACHINE)
        {
            $query .= ',
    tbl_f_machine.MANOPARC AS `machine.parkNumber`,
    tbl_f_machine.MANOSERIE AS `machine.serialNumber`';
        }
        if ($flags & GETINFOS_CONTRACT)
        {
            $query .= ',
    tbl_f_contract.CONTRATCODE AS `contract.code`,
    tbl_f_contract.ETATFACTURE AS `contract.invoiceState`,
    IF(tbl_f_contractamount.MOFORFAITFR = 0, IF(tbl_f_contractamount.MOTARIFMSFR = 0, tbl_f_contractamount.MOPXJOURFR, tbl_f_contractamount.MOTARIFMSFR), tbl_f_contractamount.MOFORFAITFR) AS `contract.unitPrice`,
    IF(tbl_f_contractamount.MOFORFAITFR = 0, IF(tbl_f_contractamount.MOTARIFMSFR = 0, "day", "month"), "fixed") AS `contract.amountType`,
    tbl_f_contract.CONTRATPROFORMAOK AS `proformaFlags`';
        }
        if ($flags & GETINFOS_CUSTOMER)
        {
            $query .= ',
    tbl_f_customer.CLCODE AS `customer.code`,
    tbl_f_customer.CLSTE AS `customer.name`,
    -tbl_f_customer.CLPROFORMA AS `customer.isProformaRequired`,
    -tbl_f_customer.CLCEE AS `customer.isVatInvoiced`';
        }
        if ($flags & GETINFOS_REPAIRINVOICE)
        {
            $query .= ',
    IF(rpi_date, 0, IF(rpi_invoicingid, 0 , IF(rpi_date_invoicing, 0, 1))) AS `isUnInvoicing`';
        }
        if ($flags & GETINFOS_USER)
        {
            $query .= ',
    CONCAT_WS(" ", f_user.usr_firstname, f_user.usr_name) AS `user.fullName`,
    f_user.usr_sta_id AS `user.state.id`';
        }
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query .= '
    COUNT(*)';
    }
    $query .= '
FROM f_repairestimate
INNER JOIN f_repairsheet
ON rps_id = rpe_rps_id
INNER JOIN ETATTABLE tbl_p_state
ON tbl_p_state.ETCODE = rpe_sta_id';
    if (defined $tabFilters->{'parkNumber'} || ($flags & GETINFOS_MACHINE))
    {
        $query .= '
INNER JOIN AUTH.MACHINE AS tbl_f_machine
ON tbl_f_machine.MAAUTO = rps_mac_id';
    }
    if (defined $tabFilters->{'contractCode'} || ($flags & GETINFOS_CONTRACT) ||
        defined $tabFilters->{'customerId'} || ($flags & GETINFOS_CUSTOMER))
    {
        $query .= '
INNER JOIN CONTRAT AS tbl_f_contract
ON (tbl_f_contract.CONTRATAUTO = rps_rct_id AND tbl_f_contract.CONTRATOK = -1)';
    }
    if (defined $tabFilters->{'contractCode'} || ($flags & GETINFOS_CONTRACT))
    {
        $query .= '
LEFT JOIN MONTANT tbl_f_contractamount
ON tbl_f_contract.MOAUTO = tbl_f_contractamount.MOAUTO';
    }
    if (defined $tabFilters->{'customerId'} || ($flags & GETINFOS_CUSTOMER))
    {
        $query .= '
INNER JOIN CLIENT AS tbl_f_customer
ON tbl_f_customer.CLAUTO = tbl_f_contract.CLAUTO';
    }
    if ($flags & GETINFOS_REPAIRINVOICE)
    {
        $query .= '
LEFT JOIN f_repairinvoice ON rpe_id = rpi_rpe_id';
    }
    if ($flags & GETINFOS_USER)
    {
        $query .= '
LEFT JOIN frmwrk.f_user f_user
ON rpe_usr_id = f_user.usr_id';
    }

    $query .= '
WHERE TRUE ';
    if (defined $tabFilters->{'id'})
    {
        if (ref($tabFilters->{'id'}) eq 'ARRAY')
        {
            $query .= '
AND rpe_id IN (' . $db->quote($tabFilters->{'id'}) . ')';
        }
        else
        {
            if ($tabFilters->{'id'} =~ /%/)
            {
                $query .= '
AND rpe_id LIKE ' . $db->quote($tabFilters->{'id'});
            }
            else
            {
                $query .= '
AND rpe_id = ' . $db->quote($tabFilters->{'id'});
            }
        }
    }
    if (defined $tabFilters->{'code'})
    {
        if (&LOC::Util::isNumeric($tabFilters->{'code'}))
        {
            $query .= '
AND rpe_id = ' . $tabFilters->{'code'};
        }
        else
        {
            if ($tabFilters->{'code'} =~ /%/)
            {
                $query .= '
AND rpe_code like ' . $db->quote($tabFilters->{'code'});
            }
            else
            {
                $query .= '
AND rpe_code = ' . $db->quote($tabFilters->{'code'});
            }
        }
    }
    #Si on précise l'Etat
    if (defined $tabFilters->{'stateId'})
    {
        $query .= '
AND rpe_sta_id = ' . $db->quote($tabFilters->{'stateId'});
    }
    if (defined $tabFilters->{'agencyId'})
    {
        if (ref($tabFilters->{'agencyId'}) eq 'ARRAY')
        {
            $query .= '
AND rpe_agc_id IN (' . $db->quote($tabFilters->{'agencyId'}) . ')';
        }
        else
        {
            $query .= '
AND rpe_agc_id = ' . $db->quote($tabFilters->{'agencyId'});
        }
    }
    if (defined $tabFilters->{'parkNumber'})
    {
         if ($tabFilters->{'parkNumber'} =~ /%/)
        {
            $query .= '
AND MANOPARC LIKE ' . $db->quote($tabFilters->{'parkNumber'});
        }
        else
        {
            $query .= '
AND MANOPARC = ' . $db->quote($tabFilters->{'parkNumber'});
        }
    }
    if (defined $tabFilters->{'contractCode'})
    {
        if (&LOC::Util::isNumeric($tabFilters->{'contractCode'}))
        {
            $query .= '
AND SUBSTR(tbl_f_contract.CONTRATCODE, -7) = ' . $db->quote($tabFilters->{'contractCode'});
        }
        else
        {
            if ($tabFilters->{'contractCode'} =~ /%/)
            {
                $query .= '
AND tbl_f_contract.CONTRATCODE like ' . $db->quote($tabFilters->{'contractCode'});
            }
            else
            {
                $query .= '
AND tbl_f_contract.CONTRATCODE = ' . $db->quote($tabFilters->{'contractCode'});
            }
        }
    }
    if (defined $tabFilters->{'customerId'})
    {
        $query .= '
AND tbl_f_customer.CLAUTO = ' . $tabFilters->{'customerId'};
    }
    if (defined $tabFilters->{'interlocutorId'})
    {
        $query .= '
AND rpe_interlocutor = ' . $tabFilters->{'interlocutorId'};
    }
    if (defined $tabFilters->{'dateInterval'})
    {
        my $dateInterval = $tabFilters->{'dateInterval'};
        if (defined $dateInterval->{'start'} && defined $dateInterval->{'end'})
        {
            $query .= '
AND (DATE(rpe_date_creation) BETWEEN ' . $db->quote($dateInterval->{'start'}) . ' AND ' . $db->quote($dateInterval->{'end'}) . ')';
        }
        elsif (defined $dateInterval->{'start'})
        {
            $query .= '
AND DATE(rpe_date_creation) >= ' . $db->quote($dateInterval->{'start'});
        }
        elsif (defined $dateInterval->{'end'})
        {
            $query .= '
AND DATE(rpe_date_creation) <= ' . $db->quote($dateInterval->{'end'});
        }
    }
    $query .= '
ORDER BY rpe_id';

    if (defined $tabOptions->{'limit'})
    {
        my $offset = 0;
        my $rowCount = 0;

        if (ref $tabOptions->{'limit'} eq 'HASH')
        {
            $offset = (defined $tabOptions->{'limit'}->{'offset'} ? $tabOptions->{'limit'}->{'offset'} : 0);
            $rowCount = (defined $tabOptions->{'limit'}->{'count'} ? $tabOptions->{'limit'}->{'count'} : 0);
        }
        else
        {
            $rowCount = $tabOptions->{'limit'};
        }

        $query .= '
LIMIT ' . $offset . ',' . $rowCount;
    }

    $query .= ';';

    my $tab = $db->fetchList($format, $query, {}, 'id');


    if (!$tab)
    {
        return undef;
    }

    # Récupération du total complet
    if (defined $tabOptions->{'limit'} && exists ($tabOptions->{'limit'}->{'&total'}))
    {
        $tabOptions->{'limit'}->{'&total'} = $db->fetchOne('SELECT FOUND_ROWS();');
    }

    # Formatage des données
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}               *= 1;
            $tabRow->{'sheet.id'}         *= 1;
            $tabRow->{'user.id'}          *= 1;
            $tabRow->{'totalCost'}        *= 1;
            $tabRow->{'totalAmount'}      *= 1;
            $tabRow->{'reductionPercent'} *= 1;
            $tabRow->{'reduction'}        *= 1;
            $tabRow->{'total'}            *= 1;
            $tabRow->{'vatPercent'}       *= 1;
            $tabRow->{'vat'}              *= 1;
            $tabRow->{'taxesIncluded'}    *= 1;

            if ($flags & GETINFOS_CONTRACT)
            {
                $tabRow->{'contract.unitPrice'} *= 1;
            }
            if ($flags & GETINFOS_CUSTOMER)
            {
                $tabRow->{'customer.isProformaRequired'} *= 1;
                $tabRow->{'customer.isVatInvoiced'}      *= 1;
            }
        }
    }

    return (wantarray ? %$tab : $tab);
}

# Function: getLines
# Retourne un hachage contenant les infos des lignes d'un devis
#
# Contenu du hachage:
# - estimate.id          => id du devis
# - currency.id          => id de la devise
# - state.id             => id de l'état
# - type                 => type de ligne
# - label                => description
# - quantity             => quantité
# - unitAmount           => prix unitaire
# - amount               => prix de la ligne
# - reductionPercent     => % de remise
# - finalAmount          => total à facturer
# - cost                 => coût de la ligne
# - isToInvoice          => à 1 indique que la ligne devra être passée en facturation
#
# Parameters:
# string $countryId - Pays
# int    $estimateId- Id du devis de remise en état
#
# Returns:
# hash|hashref
sub getLines
{
    my ($countryId, $estimateId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT
    rel_id AS `id`,
    rel_rpe_id AS `estimate.id`,
    rel_sta_id AS `state.id`,
    rel_cur_id AS `currency.id`,
    rel_rch_id AS `sheet.checkpoint.id`,
    rel_rsv_id AS `sheet.service.id`,
    rel_type AS `type`,
    rel_label AS `label`,
    rel_quantity AS `quantity`,
    rel_unitamount AS `unitAmount`,
    rel_amount AS `amount`,
    rel_reductionpercent AS `reductionPercent`,
    rel_total AS `finalAmount`,
    rel_cost AS `cost`,
    rel_is_toinvoice AS `isToInvoice`
FROM f_repairestimateline
WHERE rel_rpe_id = ' . $estimateId;

    my $tab = $db->fetchAssoc($query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    # Formatage des données
    foreach my $tabRow (values %$tab)
    {
        $tabRow->{'id'}                  *= 1;
        $tabRow->{'estimate.id'}         *= 1;
        $tabRow->{'sheet.checkpoint.id'} *= 1;
        $tabRow->{'sheet.service.id'}    *= 1;
        $tabRow->{'quantity'}            *= 1;
        $tabRow->{'unitAmount'}          *= 1;
        $tabRow->{'amount'}              *= 1;
        $tabRow->{'reductionPercent'}    *= 1;
        $tabRow->{'finalAmount'}         *= 1;
        $tabRow->{'cost'}                *= 1;
        $tabRow->{'isToInvoice'}         *= 1;
    }

    return (wantarray ? %$tab : $tab);
}

# Function: getOtherServices
# Retourne un hachage contenant les infos des autres prestations d'un devis
#
# Contenu du hachage:
# - id                   => id
# - estimate.id          => id du devis
# - service.id           => id de la prestation
# - currency.id          => id de la devise
# - type                 => type
# - quantity             => quantité
# - unitAmount           => prix unitaire
# - amount               => prix
# - reductionPercent     => % de remise
# - finalAmount          => total à facturer
# - cost                 => coût
#
# Parameters:
# string $countryId - Pays
# int    $estimateId- Id du devis de remise en état
#
# Returns:
# hash|hashref
sub getOtherServices
{
    my ($countryId, $estimateId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT
    reo_id AS `id`,
    reo_rpe_id AS `estimate.id`,
    reo_osv_id AS `service.id`,
    reo_cur_id AS `currency.id`,
    reo_type AS `type`,
    reo_quantity AS `quantity`,
    reo_unitamount AS `unitAmount`,
    reo_cost AS `cost`,
    reo_amount AS `amount`,
    reo_reductionpercent AS `reductionPercent`,
    reo_total AS `finalAmount`,
    reo_is_toinvoice AS `isToInvoice`
FROM f_repairestimate_otherservice
WHERE reo_rpe_id = ' . $estimateId;

    my $tab = $db->fetchAssoc($query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    # Formatage des données
    foreach my $tabRow (values %$tab)
    {
        $tabRow->{'id'}               *= 1;
        $tabRow->{'estimate.id'}      *= 1;
        $tabRow->{'service.id'}       *= 1;
        $tabRow->{'quantity'}         *= 1;
        $tabRow->{'unitAmount'}       *= 1;
        $tabRow->{'cost'}             *= 1;
        $tabRow->{'amount'}           *= 1;
        $tabRow->{'reductionPercent'} *= 1;
        $tabRow->{'finalAmount'}      *= 1;
        $tabRow->{'isToInvoice'}      *= 1;
    }

    return (wantarray ? %$tab : $tab);
}

# Function: set
# Met à jour (insert/update) toutes les infos d'un devis de remise en état en BDD
#
# Contenu du hachage à passer en paramètre:
# - sheetId              => id de la fiche de remise en état
# - agencyId             => id de l'agence du devis
# - userId               => id de l'utilisateur ayant saisi le devis
# - currencyId           => id de la devise
# - code                 => code du devis
# - orderCode            => n° de commande
# - object               => objet du devis
# - comments             => commentaires client
# - notes                => commentaires internes
# - totalCost            => coût total
# - totalAmount          => montant total
# - reductionPercent     => % de remise
# - reduction            => montant remisé
# - total                => total à facturer HT
# - vatPercent           => % de TVA
# - vat                  => montant de la TVA
# - taxesIncluded        => total à facturer TTC
# - dateInvoice          => date du passage en facturation
# - lines                => liste des lignes de devis
# - services             => liste des autres prestations
#
# Parameters:
# string    $countryId - Pays
# hashref   $values    - Valeurs du devis de remise en état
# int       $userId    - Identifiant de l'utilisateur
# bool      $fromSheet - 1 : mise à jour depuis la fiche de remise en état, 0 sinon
#
# Returns:
# integer
sub set
{
    my ($countryId, $values, $userId, $fromSheet) = @_;

    unless (defined $fromSheet)
    {
        $fromSheet = 0;
    }

    # Récupération des droits
    my $tabRights = &getRights($countryId, $values->{'agencyId'}, $values->{'id'}, $userId);

    # Utilisateur n'ayant pas les droits
    if (!$fromSheet && $tabRights->{'actions'} !~ /^(|.*\|)valid(|\|.*)$/)
    {
        return -1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    my $estId;

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Mise à jour
    if (defined $values->{'id'} && $values->{'id'} != 0)
    {
        my $result = $db->update('f_repairestimate', {
                                    'rpe_ordercode'         => $values->{'orderCode'},
                                    'rpe_interlocutor'      => $values->{'estimateUserId'},
                                    'rpe_object'            => $values->{'object'},
                                    'rpe_comments'          => $values->{'comments'},
                                    'rpe_notes'             => $values->{'notes'},
                                    'rpe_residuesmode'      => $values->{'residuesMode'},
                                    'rpe_residuesvalue'     => $values->{'residuesValue'},
                                    'rpe_totalcost'         => $values->{'totalCost'},
                                    'rpe_totalamount'       => $values->{'totalAmount'},
                                    'rpe_reductionpercent'  => $values->{'reductionPercent'},
                                    'rpe_reduction'         => $values->{'reductionAmount'},
                                    'rpe_total'             => $values->{'finalAmount'},
                                    'rpe_vatpercent'        => $values->{'vatPercent'},
                                    'rpe_vat'               => $values->{'vat'},
                                    'rpe_taxesincluded'     => $values->{'taxesIncluded'},
                                 },
                                 {
                                    'rpe_id'               => $values->{'id'}
                                 });

        if ($result == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1
        }


        $estId = $values->{'id'};

    }
    # Création
    else
    {

        $estId = $db->insert('f_repairestimate', {
                                'rpe_rps_id'            => $values->{'sheetId'},
                                'rpe_agc_id'            => $values->{'agencyId'},
                                'rpe_sta_id'            => STATE_WAITING,
                                'rpe_usr_id'            => $values->{'userId'},
                                'rpe_cur_id'            => $values->{'currencyId'},
                                'rpe_code'              => $values->{'code'},
                                'rpe_ordercode'         => $values->{'orderCode'},
                                'rpe_interlocutor'      => $values->{'estimateUserId'},
                                'rpe_object'            => $values->{'object'},
                                'rpe_comments'          => $values->{'comments'},
                                'rpe_notes'             => $values->{'notes'},
                                'rpe_residuesmode'      => $values->{'residuesMode'},
                                'rpe_residuesvalue'     => $values->{'residuesValue'},
                                'rpe_totalcost'         => $values->{'totalCost'},
                                'rpe_totalamount'       => $values->{'totalAmount'},
                                'rpe_reductionpercent'  => $values->{'reductionPercent'},
                                'rpe_reduction'         => $values->{'reductionAmount'},
                                'rpe_total'             => $values->{'finalAmount'},
                                'rpe_vatpercent'        => $values->{'vatPercent'},
                                'rpe_vat'               => $values->{'vat'},
                                'rpe_taxesincluded'     => $values->{'taxesIncluded'},
                                'rpe_date_invoice'      => $values->{'dateInvoice'},
                                'rpe_date_creation*'    => 'NOW()',
                             });



        if ($estId == -1)
        {
           # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1
        }


    }#fin du else


    if ($estId != -1)
    {
        my $resultLine = &setLines($countryId, $estId, $values->{'lines'});
        if ($resultLine == -1)
        {
             # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;
        }

        my $resultOtherService = &setOtherServices($countryId, $estId, $values->{'services'});
        if ($resultOtherService == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;
        }
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }


    return $estId;
}

# Function: setLines
# Met à jour (insert/update) toutes les lignes d'un devis de remise en état en BDD
#
# Contenu du hachage à passer en paramètre:
# - currencyId           => id de la devise
# - type                 => type de ligne
# - label                => description
# - quantity             => quantité
# - unitAmount           => prix unitaire
# - amount               => prix de la ligne
# - reductionPercent     => % de remise
# - finalAmount          => total à facturer
# - cost                 => coût de la ligne
# - isToInvoice          => à 1 indique que la ligne devra être passée en facturation
#
# Parameters:
# string    $countryId  - Pays
# integer   $estimateId - Pays
# hashref   $values     - Valeurs des lignes du devis de remise en état
#
# Returns:
# integer
sub setLines
{
    my ($countryId, $estimateId, $values) = @_;

    # Récupération des formats Sage (quantité, prix et montant)
    my $charac = &LOC::Characteristic::getCountryValueByCode('FORMATSSAGE', $countryId);
    my $tabSageFormats = {'quantity' => 2, 'price' => 4, 'amount' => 2};
    if ($charac ne '')
    {
        $tabSageFormats = &LOC::Json::fromJson($charac);
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }


    # Ajout et mise à jour
    my @ids;
    while (my ($i, $line) = each(%$values))
    {
        my $quantity          = &LOC::Util::round($line->{'quantity'}, $tabSageFormats->{'quantity'});
        my $unitAmount        = &LOC::Util::round($line->{'unitAmount'}, $tabSageFormats->{'price'});
        my $reducedUnitAmount = &LOC::Util::round($unitAmount * (1 - $line->{'reductionPercent'} / 100), $tabSageFormats->{'price'});
        my $amount            = &LOC::Util::round($quantity * $unitAmount, $tabSageFormats->{'price'});
        my $total             = &LOC::Util::round($quantity * $reducedUnitAmount, $tabSageFormats->{'amount'});
        my $cost              = &LOC::Util::round($line->{'cost'}, $tabSageFormats->{'price'});

        if (defined $line->{'id'} && $line->{'id'} ne '')
        {
            my $v = {
                'rel_type'              => $line->{'type'},
                'rel_label'             => $line->{'label'},
                'rel_quantity'          => $quantity,
                'rel_unitamount'        => $unitAmount,
                'rel_amount'            => $amount,
                'rel_reductionpercent'  => $line->{'reductionPercent'},
                'rel_total'             => $total,
                'rel_cost'              => $cost,
                'rel_is_toinvoice'      => $line->{'isToInvoice'}
            };

            if (defined $line->{'sheetCheckpointId'} && $line->{'sheetCheckpointId'} ne '')
            {
                $v->{'rel_rch_id'} = $line->{'sheetCheckpointId'};
            }
            if (defined $line->{'sheetServiceId'} && $line->{'sheetServiceId'} ne '')
            {

                $v->{'rel_rsv_id'} = $line->{'sheetServiceId'};
            }

            my $update = $db->update('f_repairestimateline', $v, {'rel_id' => $line->{'id'}});

            if ($update == -1)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return -1;
            }
            if ($update!= -1)
            {
                push(@ids, $line->{'id'});
            }

        }
        else
        {
            my $v = {
                'rel_rpe_id'            => $estimateId,
                'rel_cur_id'            => $line->{'currencyId'},
                'rel_type'              => $line->{'type'},
                'rel_label'             => $line->{'label'},
                'rel_quantity'          => $quantity,
                'rel_unitamount'        => $unitAmount,
                'rel_amount'            => $amount,
                'rel_reductionpercent'  => $line->{'reductionPercent'},
                'rel_total'             => $total,
                'rel_cost'              => $cost,
                'rel_is_toinvoice'      => $line->{'isToInvoice'},
                'rel_sta_id'            => STATE_WAITING
            };
            if (defined $line->{'sheetCheckpointId'} && $line->{'sheetCheckpointId'} ne '')
            {
                $v->{'rel_rch_id'} = $line->{'sheetCheckpointId'};
            }
            if (defined $line->{'sheetServiceId'} && $line->{'sheetServiceId'} ne '')
            {
                $v->{'rel_rsv_id'} = $line->{'sheetServiceId'};
            }

            my $linId = $db->insert('f_repairestimateline', $v);

            if ($linId == -1)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return -1;
            }

            if ($linId != -1)
            {
                push(@ids, $linId);
            }
        }
    }


    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }

    return 1;

}

# Function: setOtherServices
# Met à jour (insert/update/delete) toutes les prestations d'un devis de remise en état en BDD
#
# Contenu du hachage à passer en paramètre:
# - id                   => id
# - serviceId            => id de la prestation
# - currencyId           => id de la devise
# - type                 => type
# - quantity             => quantité
# - unitAmount           => prix unitaire
# - amount               => prix
# - reductionPercent     => % de remise
# - finalAmount          => total à facturer
# - cost                 => coût
#
# Parameters:
# string    $countryId  - Pays
# integer   $estimateId - Pays
# arrayref  $values     - Valeurs des lignes du devis de remise en état
#
# Returns:
# integer
sub setOtherServices
{
    my ($countryId, $estimateId, $values) = @_;

    # Récupération des formats Sage (quantité, prix et montant)
    my $charac = &LOC::Characteristic::getCountryValueByCode('FORMATSSAGE', $countryId);
    my $tabSageFormats = {'quantity' => 2, 'price' => 4, 'amount' => 2};
    if ($charac ne '')
    {
        $tabSageFormats = &LOC::Json::fromJson($charac);
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Id à supprimer
    my @ids;

    foreach my $service (@$values)
    {
        unless (defined $service->{'isToInvoice'})
        {
            $service->{'isToInvoice'} = 1;
        }

        my $vals;

        # Le service n'est pas supprimé
        if ($service->{'isDeleted'} == 0)
        {
            my $quantity          = &LOC::Util::round($service->{'quantity'}, $tabSageFormats->{'quantity'});
            my $unitAmount        = &LOC::Util::round($service->{'unitAmount'}, $tabSageFormats->{'price'});
            my $reducedUnitAmount = &LOC::Util::round($unitAmount * (1 - $service->{'reductionPercent'} / 100), $tabSageFormats->{'price'});
            my $amount            = &LOC::Util::round($quantity * $unitAmount, $tabSageFormats->{'price'});
            my $total             = &LOC::Util::round($quantity * $reducedUnitAmount, $tabSageFormats->{'amount'});
            my $cost              = &LOC::Util::round($service->{'cost'}, $tabSageFormats->{'price'});

            if (defined $service->{'id'} && $service->{'id'} ne '')
            {
                $vals = {
                    'reo_type'              => $service->{'type'},
                    'reo_quantity'          => $quantity,
                    'reo_unitamount'        => $unitAmount,
                    'reo_cost'              => $cost,
                    'reo_amount'            => $amount,
                    'reo_reductionpercent'  => $service->{'reductionPercent'},
                    'reo_total'             => $total,
                    'reo_is_toinvoice'      => $service->{'isToInvoice'}
                };
                if ($service->{'serviceId'} != 0)
                {
                    $vals->{'reo_osv_id'} = $service->{'serviceId'};
                }

                my $update = $db->update('f_repairestimate_otherservice', $vals, { 'reo_id' => $service->{'id'}});
                if ($update == -1)
                {
                    # Annule la transaction
                    if (!$hasTransaction)
                    {
                        $db->rollBack();
                    }
                    return -1;

                }
            }
            else
            {

                $vals = {
                    'reo_rpe_id'            => $estimateId,
                    'reo_cur_id'            => $service->{'currencyId'},
                    'reo_type'              => $service->{'type'},
                    'reo_quantity'          => $quantity,
                    'reo_unitamount'        => $unitAmount,
                    'reo_cost'              => $cost,
                    'reo_amount'            => $amount,
                    'reo_reductionpercent'  => $service->{'reductionPercent'},
                    'reo_total'             => $total,
                    'reo_is_toinvoice'      => $service->{'isToInvoice'}
                };
                if ($service->{'serviceId'} != 0)
                {
                    $vals->{'reo_osv_id'} = $service->{'serviceId'};
                }
                my $srvId = $db->insert('f_repairestimate_otherservice', $vals);

                if ($srvId == -1)
                {
                    # Annule la transaction
                    if (!$hasTransaction)
                    {
                        $db->rollBack();
                    }
                    return -1;
                }

            }#fin du else
        }#fin du if
        else # cas de la suppression
        {
            #Ajout dans le tableau des Id à supprimer
            if (defined $service->{'id'} && $service->{'id'} ne '')
            {
                push(@ids, $service->{'id'});
            }
        }
    }

    # Suppresion des prestations
    if (@ids > 0)
    {
        my $query = '
DELETE FROM f_repairestimate_otherservice
WHERE reo_rpe_id = ' . $estimateId . '
AND reo_id IN ('. join(', ', @ids) . ');';

        my $deleteResult = $db->query($query);
        if ($deleteResult == 0)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }

            return -1;
        }
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }

    return 1
}

# Function: setCode
# Génère le code du devis et met à jour en base
#
# Parameters:
# string    $countryId   - Pays
# string    $agencyId    - Agence
# string    $id          - Id du devis
#
# Returns:
# integer
sub setCode
{
    my ($countryId, $agencyId, $id) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Création du code du devis
    my $pattern = 'DEV-DRE-%s%d';
    my $code = sprintf($pattern, $agencyId, $id);

    # Mise à jour
    my $update = $db->update('f_repairestimate', {'rpe_code' => $code}, {'rpe_id' => $id});

    if ($update == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return -1;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }
    return $update;

}

# Function: setCode
# Génère le code du devis et met à jour en base
#
# Parameters:
# string       $countryId - Identifiant du pays
# int|arrayref $id        - Identifiant ou liste des identifants des devis de remise en état
# string       $stateId   - Identifiant de l'état (parmi STATE_WAITING, STATE_APPROVED, STATE_TURNEDIN, STATE_ABORTED et
#                           STATE_INVOICED)
#
# Returns:
# integer
sub setState
{
    my ($countryId, $id, $stateId) = @_;

    # Vérification de la validité de l'état
    my $tabStates = [
        STATE_WAITING,
        STATE_APPROVED,
        STATE_TURNEDIN,
        STATE_ABORTED,
        STATE_INVOICED,
    ];
    if (!&LOC::Util::in_array($stateId, $tabStates))
    {
        return -1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Mise à jour
    my $update = $db->update('f_repairestimate', {'rpe_sta_id' => $stateId}, {'rpe_id' => $id});

    if ($update == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return -1;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }
    return $update;
}

# Function: abort
# Abandonne un devis de remise en état
#
# Parameters:
# string    $countryId  - Pays
# string    $agencyId   - Pays
# integer   $id         - Id du devis de remise en état
# integer   $userId     - Id de l'utilisateur
#
# Returns:
# integer
sub abort
{
    my ($countryId, $agencyId, $id, $userId) = @_;

    # Récupération des droits
    my $tabRights = &getRights($countryId, $agencyId, $id, $userId);

    # Utilisateur n'ayant pas les droits
    if ($tabRights->{'actions'} !~ /^(|.*\|)abort(|\|.*)$/)
    {
        return -1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }


    my $updateResult = $db->update('f_repairestimate',
                                   {'rpe_sta_id' => STATE_ABORTED}, {'rpe_id' => $id});


    if ($updateResult == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return -1;
    }
    if ($updateResult != -1)
    {
        &LOC::Alert::setDeltaCounterAlert(30, $agencyId, -1);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }
    return $updateResult;

}

# Function: invoice
# Approuve un devis de remise en état pour le passage en factu
#
# Parameters:
# string  $countryId              - Pays
# string  $agencyId               - Identifiant de l'agence
# integer $id                     - Id du devis de remise en état
# integer $userId                 - Id de l'utilisateur
# bool    $displayDisplLabDetails - Afficher le détail de la main d'oeuvre et du déplacement
#
# Returns:
# integer
sub invoice
{
    my ($countryId, $agencyId, $id, $userId, $displayDisplLabDetails) = @_;

    # Récupération des droits
    my $tabRights = &getRights($countryId, $agencyId, $id, $userId);

    # Utilisateur n'ayant pas les droits
    if ($tabRights->{'actions'} !~ /^(|.*\|)invoice(|\|.*)$/)
    {
        return -1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $estimate    = &getInfos($countryId, $id);   # Infos du devis
    my $tabLines    = &getLines($countryId, $id);   # Lignes du devis
    my $tabServices = &getOtherServices($countryId, $id);   # Autres prestations

    # Récupération des formats Sage (quantité, prix et montant)
    my $charac = &LOC::Characteristic::getCountryValueByCode('FORMATSSAGE', $countryId);
    my $tabSageFormats = {'quantity' => 2, 'price' => 4, 'amount' => 2};
    if ($charac ne '')
    {
        $tabSageFormats = &LOC::Json::fromJson($charac);
    }

    # Récupération des informations sur le client, le contrat, le chantier et la machine
    my $query = '
SELECT
    tbl_f_customer.CLAUTO AS `customer.id`,
    tbl_f_customer.CLCODE AS `customer.code`,
    tbl_f_customer.CLCOLLECTIF AS `customer.accountingCode`,
    tbl_f_customer.CLCEE AS `customer.clcee`,
    tbl_f_machine.MANOPARC AS `machine.parkNumber`,
    tbl_f_machine.MANOSERIE AS `machine.serialNumber`,
    tbl_f_model.MOMADESIGNATION AS `model.label`,
    tbl_f_contract.CONTRATCODE AS `contract.code`,
    agc_id AS `agency.id`,
    agc_analyticalaccount AS `agency.analyticalAccount`,
    tbl_f_businessfamily.FACOMANALYTIQUE as `businessFamily.analyticalCode`,
    tbl_f_businessfamily.FACOMLIBELLE as `businessFamily.label`,
    tbl_f_site.CHLIBELLE AS `site.label`,
    tbl_f_site.CHADRESSE AS `site.address`,
    tbl_p_sitelocality.VICP AS `site.locality.postalCode`,
    tbl_p_sitelocality.VINOM AS `site.locality.name`
FROM f_repairsheet
INNER JOIN frmwrk.p_agency ON agc_id = rps_agc_id
INNER JOIN AUTH.MACHINE AS tbl_f_machine ON tbl_f_machine.MAAUTO = rps_mac_id
INNER JOIN AUTH.MODELEMACHINE AS tbl_f_model ON tbl_f_machine.MOMAAUTO = tbl_f_model.MOMAAUTO
INNER JOIN FAMILLEMACHINE AS tbl_f_machinesfamily ON tbl_f_machinesfamily.FAMAAUTO = tbl_f_model.FAMAAUTO
INNER JOIN FAMILLECOMMERCIALE AS tbl_f_businessfamily ON tbl_f_businessfamily.FACOMAUTO = tbl_f_machinesfamily.FACOMAUTO
INNER JOIN CONTRAT AS tbl_f_contract ON (tbl_f_contract.CONTRATAUTO = rps_rct_id AND tbl_f_contract.CONTRATOK = -1)
INNER JOIN CLIENT AS tbl_f_customer ON tbl_f_customer.CLAUTO = tbl_f_contract.CLAUTO
LEFT JOIN CHANTIER AS tbl_f_site ON tbl_f_site.CHAUTO = tbl_f_contract.CHAUTO
LEFT JOIN VILLECHA tbl_p_sitelocality ON tbl_f_site.VILLEAUTO = tbl_p_sitelocality.VILLEAUTO
WHERE rps_id = ' . $db->quote($estimate->{'sheet.id'});
    my $infos = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);

    # Centre de coût
    my $costCenter = &LOC::Characteristic::getValueByCode('CENTRCOUT');

    # En-tête 4 (adresse chantier)
    my $header4 = $infos->{'site.adress'};
    $header4 .= ($header4 eq '' ? '' : ' ') . $infos->{'site.locality.postalCode'};
    $header4 .= ($header4 eq '' ? '' : ' ') . $infos->{'site.locality.name'};

    # Code affaire
    my $affair = $infos->{'agency.analyticalAccount'} . $infos->{'businessFamily.analyticalCode'} . $costCenter;

    # CLCEE
    my $accCategory = $infos->{'customer.clcee'};

    # TVA
    my $vatPct      = $estimate->{'vatPercent'};
    my $vat         = $estimate->{'vat'};
    my $taxesInc    = $estimate->{'taxesIncluded'};
    if ($infos->{'customer.clcee'} == 0)
    {
        $accCategory = 2;
        $vatPct      = 0;
        $vat         = 0;
        $taxesInc    = $estimate->{'total'};
    }

    # Insertion de la ligne d'en-tête de la facture
    my $vals = {
        'rpi_rpe_id'             => $estimate->{'id'},
        'rpi_cus_id'             => $infos->{'customer.id'},
        'rpi_reference'          => $infos->{'contract.code'},
        'rpi_thirdparty'         => $infos->{'customer.code'},
        'rpi_payer'              => $infos->{'customer.code'},
        'rpi_header1'            => $estimate->{'orderCode'},
        'rpi_header2'            => $estimate->{'code'},
        'rpi_header3'            => $infos->{'site.label'},
        'rpi_header4'            => $header4,
        'rpi_affair'             => $affair,
        'rpi_accountingcategory' => $accCategory,
        'rpi_generalaccount'     => $infos->{'customer.accountingCode'},
        'rpi_sta_id'             => LOC::Invoice::STATE_NOTEXPORTED,
        'rpi_total'              => $estimate->{'total'},
        'rpi_vat'                => $vat,
        'rpi_taxesincluded'      => $taxesInc,
        'rpi_date_creation*'     => 'NOW()',
        'rpi_agc_id'             => $estimate->{'agency.id'}
    };
    my $invcId = $db->insert('f_repairinvoice', $vals);
    if ($invcId == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return -1;
    }

    # Références articles
    my @tabCodes = ('PTVRE', 'PIIRE', 'PIERE', 'APIRE', 'MOERE', 'TPSDEPRE', 'DEPRE', 'DECHE');
    my $tabArticleRefInfos = &LOC::Invoice::ArticleRef::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                                {'code' => \@tabCodes});
    my $tabArticleRef = {};
    foreach my $tabInfos (values(%$tabArticleRefInfos))
    {
        $tabArticleRef->{$tabInfos->{'code'}} = $tabInfos->{'value'};
    }

    # Mode réduction globale
    my $rdcMode = 0;
    if ($estimate->{'reductionPercent'} > 0)
    {
        $rdcMode = 1;
    }

    # Insertion des lignes de la facture correspondant aux lignes de devis
    while (my ($i, $line) = each(%$tabLines))
    {
        # Seulement si la ligne est à facturer et que la quantité n'est pas nulle
        if ($line->{'isToInvoice'} == 1 && $line->{'quantity'} != 0)
        {
            # Référence article
            my $articleRef;
            # Point de vérification
            if ($line->{'type'} eq LOC::Repair::LineType::TYPE_CHANGEMENT ||
                $line->{'type'} eq LOC::Repair::LineType::TYPE_REPAIR ||
                $line->{'type'} eq LOC::Repair::LineType::TYPE_FIX)
            {
                $articleRef = $tabArticleRef->{'PTVRE'};
            }
            # Point de vérification
            elsif ($line->{'type'} eq LOC::Repair::LineType::TYPE_SERVICE)
            {
                $articleRef = $tabArticleRef->{'PIIRE'};
            }

            # Réduction
            my $rdcPct = $line->{'reductionPercent'};
            if ($rdcMode == 1)
            {
                $rdcPct = $estimate->{'reductionPercent'};
            }

            $vals = {
                'ril_rpi_id'           => $invcId,
                'ril_articleref'       => $articleRef,
                'ril_label'            => $line->{'label'},
                'ril_labelextra'       => $line->{'type'},
                'ril_unitprice'        => &LOC::Util::round($line->{'unitAmount'}, $tabSageFormats->{'price'}),
                'ril_quantity'         => &LOC::Util::round($line->{'quantity'}, $tabSageFormats->{'quantity'}),
                'ril_reduction'        => $rdcPct,
                'ril_affair'           => $affair,
                'ril_vatrate1'         => $vatPct,
                'ril_vatratetype1'     => 0,
                'ril_thirdpartynumber' => $infos->{'customer.code'},
                'ril_freeinfo2'        => $infos->{'machine.parkNumber'},
                'ril_freeinfo3'        => $infos->{'model.label'},
                'ril_freeinfo4'        => $infos->{'machine.serialNumber'},
                'ril_freeinfo5'        => $infos->{'agency.id'},
                'ril_freeinfo8'        => $infos->{'businessFamily.label'}
            };
            my $lnId = $db->insert('f_repairinvoiceline', $vals);
            if ($lnId == -1)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return -1;
            }
        }
    }

    # Insertion des lignes de la facture correspondant aux autres prestations
    while (my ($i, $service) = each(%$tabServices))
    {
        if ($service->{'isToInvoice'} == 1 && $service->{'quantity'} != 0)
        {
            my $tabLineTypeInfos = &LOC::Repair::LineType::getInfos($countryId, $service->{'type'});

            # Désignation et référence article
            my $label;
            my $articleRef;
            my $quantity = $service->{'quantity'};
            my $unitAmount = $service->{'unitAmount'};
            # Autre service
            if ($service->{'type'} eq LOC::Repair::LineType::TYPE_SERVICE)
            {
                my $otherService = &LOC::Repair::OtherService::getInfos($countryId, $service->{'service.id'});
                $label = $otherService->{'name'};
                $articleRef = $tabArticleRef->{'APIRE'};
            }
            # Main d'oeuvre
            elsif ($service->{'type'} eq LOC::Repair::LineType::TYPE_LABOUR)
            {
                my $hours   = int($service->{'quantity'});
                my $minutes = ($service->{'quantity'} - int($service->{'quantity'})) * 30 / 0.5;
                $label = $tabLineTypeInfos->{'label'};
                if ($displayDisplLabDetails)
                {
                    $label .= ' ' . sprintf($tabLineTypeInfos->{'extraLabel'}, $hours, $minutes);
                }
                else
                {
                    $unitAmount *= $quantity;
                    $quantity    = 1;
                }
                $articleRef = $tabArticleRef->{'MOERE'};
            }
            # Temps de trajet
            elsif ($service->{'type'} eq LOC::Repair::LineType::TYPE_TRAVELTIME)
            {
                my $hours   = int($service->{'quantity'});
                my $minutes = ($service->{'quantity'} - int($service->{'quantity'})) * 30 / 0.5;
                $label = $tabLineTypeInfos->{'label'};
                if ($displayDisplLabDetails)
                {
                    $label .= ' ' . sprintf($tabLineTypeInfos->{'extraLabel'}, $hours, $minutes);
                }
                else
                {
                    $unitAmount *= $quantity;
                    $quantity    = 1;
                }
                $articleRef = $tabArticleRef->{'TPSDEPRE'};
            }
            # Déplacement
            elsif ($service->{'type'} eq LOC::Repair::LineType::TYPE_DISPLACEMENT)
            {
                $label = $tabLineTypeInfos->{'label'};
                if ($displayDisplLabDetails)
                {
                    $label .= ' ' . sprintf($tabLineTypeInfos->{'extraLabel'}, $service->{'quantity'});
                }
                else
                {
                    $unitAmount *= $quantity;
                    $quantity    = 1;
                }
                $articleRef = $tabArticleRef->{'DEPRE'};
            }
            # Participation au recyclage
            elsif ($service->{'type'} eq LOC::Repair::LineType::TYPE_RESIDUES)
            {
                $label = $tabLineTypeInfos->{'label'};
                $articleRef = $tabArticleRef->{'DECHE'};
            }

            # Réduction
            my $rdcPct = $service->{'reductionPercent'};
            if ($rdcMode == 1)
            {
                $rdcPct = $estimate->{'reductionPercent'};
            }

            $vals = {
                'ril_rpi_id'           => $invcId,
                'ril_articleref'       => $articleRef,
                'ril_label'            => $label,
                'ril_unitprice'        => &LOC::Util::round($unitAmount, $tabSageFormats->{'price'}),
                'ril_quantity'         => &LOC::Util::round($quantity, $tabSageFormats->{'quantity'}),
                'ril_reduction'        => $rdcPct,
                'ril_affair'           => $affair,
                'ril_vatrate1'         => $vatPct,
                'ril_vatratetype1'     => 0,
                'ril_thirdpartynumber' => $infos->{'customer.code'},
                'ril_freeinfo2'        => $infos->{'machine.parkNumber'},
                'ril_freeinfo3'        => $infos->{'model.label'},
                'ril_freeinfo4'        => $infos->{'machine.serialNumber'},
                'ril_freeinfo5'        => $infos->{'agency.id'},
                'ril_freeinfo8'        => $infos->{'businessFamily.label'}
            };
            my $lnId = $db->insert('f_repairinvoiceline', $vals);
            if ($lnId == -1)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                return -1;
            }
        }
    }

    my $updateResult = $db->update('f_repairestimate',
                                   {'rpe_sta_id' => STATE_APPROVED, 'rpe_date_invoice*' => 'NOW()'}, {'rpe_id' => $id});

    if ($updateResult == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return -1;
    }

    if ($updateResult != -1)
    {
        # Mise à jour des alertes
        &LOC::Alert::setDeltaCounterAlert(30, $infos->{'agency.id'}, -1);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }

    return $updateResult;
}

# Function: reactivate
# Remise en cours d'un devis de remise en état
#
# Parameters:
# string  $countryId              - Pays
# string  $agencyId               - Identifiant de l'agence
# integer $id                     - Id du devis de remise en état
# integer $userId                 - Id de l'utilisateur
#
# Returns:
# integer
sub reactivate
{
    my ($countryId, $agencyId, $id, $userId) = @_;

    # Récupération des droits
    my $tabRights = &getRights($countryId, $agencyId, $id, $userId);

    # Utilisateur n'ayant pas les droits
    if ($tabRights->{'actions'} !~ /^(|.*\|)reactivate(|\|.*)$/)
    {
        return -1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Infos du devis
    my $estimate    = &getInfos($countryId, $id);   

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Suppression de la facture et des lignes associées
    my $deleteRepairInvoiceResult = $db->delete('f_repairinvoice', {'rpi_rpe_id*' => $estimate->{'id'}});
    if ($deleteRepairInvoiceResult == -1)
    {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
    }

    # Mise à jour de l'état du devis de remise en état
    my $updateEstimate = $db->update('f_repairestimate', {'rpe_sta_id' => LOC::Repair::Estimate::STATE_WAITING}, {'rpe_id*' => $estimate->{'id'}});
    if ($updateEstimate == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Mise à jour des alertes
    if (($deleteRepairInvoiceResult != -1) && ($updateEstimate != -1))
    {
        &LOC::Alert::setDeltaCounterAlert(30, $agencyId, +1);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }
    
    return 1;
}

# Function: turnInto
# Bascule un devis de remise en état
#
# Parameters:
# string    $countryId   - Pays
# string    $agencyId    - Agence
# integer   $id          - Id du devis de remise en état
# integer   $userId      - Id de l'utilisateur
# integer   $sheetId     - Id de la fiche de remise en état
# integer   $flag        - Transformer en jours de remise en état (FLAG_TURNEDINDAYS)
#                          ou en services (FLAG_TURNEDINSERVICES)
#
# Returns:
# integer
sub turnInto
{
    my ($countryId, $agencyId, $id, $userId, $sheetId, $flag) = @_;

    # Récupération des droits
    my $tabRights = &getRights($countryId, $agencyId, $id, $userId);

    # Utilisateur n'ayant pas les droits
    if (($flag ne FLAG_TURNEDINDAYS || $tabRights->{'actions'} !~ /^(|.*\|)days(|\|.*)$/) &&
        ($flag ne FLAG_TURNEDINSERVICES || $tabRights->{'actions'} !~ /^(|.*\|)services(|\|.*)$/))
    {
        return -1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }


    my $updateResult = $db->update('f_repairestimate',
                                   {'rpe_sta_id' => STATE_ABORTED,
                                    'rpe_flag'   => $flag,
                                   }, {'rpe_id'  => $id});

    if ($updateResult == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return -1;
    }

    #Mise à NULL des Id des prestations techniques sur le DRE
    my $updateServiceResult = $db->update('f_repairestimateline',
                                         {'rel_rsv_id' => undef},
                                         {'rel_rpe_id'  => $id});

    if ($updateServiceResult == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return -1;
    }



    my $updateSheetResult = $db->update('f_repairsheet',
                                        {'rps_sta_id' => STATE_WAITING,
                                         'rps_flag'   => undef},
                                        {'rps_id' => $sheetId});

    if ($updateSheetResult == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return -1;
    }

    if ($updateResult != -1 && $updateSheetResult != -1 && $updateServiceResult!= -1)
    {
        &LOC::Alert::setDeltaCounterAlert(30, $agencyId, -1);
        &LOC::Alert::setDeltaCounterAlert(29, $agencyId, +1);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }

    return $updateResult;
}


# Function: _getRights
# Récupérer les droits sur un devis de remise en état pour un utilisateur donné
#
# Parameters:
# hashref $tabEstimateInfos - Informations sur le devis de remise en état
# hashref $tabUserInfos     - Informations sur l'utilisateur
#
# Returns:
# hashref - Tableau des droits
sub _getRights
{
    my ($tabEstimateInfos, $tabUserInfos) = @_;

    my $stateId       = $tabEstimateInfos->{'state.id'};
    my $agencyId      = $tabEstimateInfos->{'agency.id'};
    my $proformaFlags = $tabEstimateInfos->{'proformaFlags'} * 1;

    my $isAdmin          = $tabUserInfos->{'isAdmin'};
    my $isCommercial     = $isAdmin || &LOC::Util::in_array(LOC::User::GROUP_COMMERCIAL, $tabUserInfos->{'tabGroups'});
    my $isAgencyOfficial = $isAdmin || &LOC::Util::in_array(LOC::User::GROUP_AGENCYMANAGER, $tabUserInfos->{'tabGroups'});

    my $tabRights = {
        'infos'       => '',
        'lines'       => '',
        'services'    => '',
        'supplements' => '',
        'total'       => '',
        'actions'     => 'print'
    };

    # Vérifie si l'agence de l'utilisateur est identique
    if (!$isAdmin && ($agencyId ne $tabUserInfos->{'nomadAgency.id'}))
    {
        return $tabRights;
    }

    # Droits seulement pour le chef d'agence
    if ($isAgencyOfficial || $isCommercial)
    {
        # Devis en attente ou accepté
        if ($stateId eq STATE_WAITING || $stateId eq STATE_APPROVED)
        {
            $tabRights->{'infos'} .= '|estimateUser|object';
            $tabRights->{'supplements'} .= '|comments|notes';
            $tabRights->{'actions'} .= '|valid|reinit';
        }

        # Devis en attente
        if ($stateId eq STATE_WAITING)
        {
            $tabRights->{'infos'} .= '|orderNo';
            $tabRights->{'lines'} .= '|reduction';
            if ($isAgencyOfficial)
            {
                $tabRights->{'lines'} .= '|invoice';
            }
            $tabRights->{'services'} .= '|label|quantity|unitPrice|reduction|cost';
            if ($isAgencyOfficial)
            {
                $tabRights->{'services'} .= '|add|delete';
            }
            $tabRights->{'total'} .= '|reduction';
            $tabRights->{'actions'} .= '|invoice|abort';
            if ($tabEstimateInfos->{'contract.invoiceState'} ne LOC::Contract::Rent::INVOICESTATE_FINAL && 
                !($proformaFlags & LOC::Proforma::DOCFLAG_EXISTACTIVED)
                && !($tabEstimateInfos->{'customer.isProformaRequired'} * 1))
            {
                $tabRights->{'actions'} .= '|services';
                if ($tabEstimateInfos->{'contract.amountType'} eq 'day' &&
                    $tabEstimateInfos->{'contract.unitPrice'} != 0)
                {
                    $tabRights->{'actions'} .= '|days';
                }
            }
        }

        # Devis accepté pour facturation
        if ($stateId eq STATE_APPROVED)
        {
            if ($tabEstimateInfos->{'isUnInvoicing'} == 1)
            {
                $tabRights->{'actions'} .= '|reactivate';
            }
        }
    }

    return $tabRights;
}


# Function: getRights
# Récupérer les droits sur le devis de remise en état (par rapport à l'utilisateur)
#
# Parameters:
# string $countryId  - Pays
# string $agencyId   - Agence
# int    $id         - Id du devis de remise en état
# int    $userId     - Id de l'utilisateur
#
# Returns:
# hash|hashref - Liste des factures de remise en état
sub getRights
{
    my ($countryId, $agencyId, $id, $userId) = @_;

    my $tabEstimateInfos;
    my $tabUserInfos;

    # Informations sur l'utilisateur
    $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Informations sur le devis de remise en état
    if ($id)
    {
        $tabEstimateInfos = &getInfos($countryId, $id, GETINFOS_CONTRACT | GETINFOS_CUSTOMER | GETINFOS_REPAIRINVOICE);
    }
    else
    {
        $tabEstimateInfos = {
            'state.id'  => STATE_WAITING,
            'agency.id' => $agencyId
        };
    }

    return &_getRights($tabEstimateInfos, $tabUserInfos);
}

1;