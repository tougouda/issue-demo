use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Repair::Sheet
# Module de gestion des Fiche de remise en état
package LOC::Repair::Sheet;


# Modules internes
use LOC::Util;
use LOC::Globals;


# Constants: États
# STATE_WAITING   - En attente
# STATE_TURNEDIN  - Basculé
# STATE_ABORTED   - Abandonné
# STATE_CANCELED  - Annulé
use constant
{
    STATE_WAITING    => 'CAS01',
    STATE_TURNEDIN   => 'CAS03',
    STATE_ABORTED    => 'CAS04',
    STATE_CANCELED   => 'CAS06'
};
# Constants: Récupération d'informations sur les fiches
# GETINFOS_CONTRACT    - Informations contrat
# GETINFOS_MACHINE     - Informations machine
# GETINFOS_CUSTOMER    - Informations client
# GETINFOS_SITE        - Informations chantier
# GETINFOS_TECHNICIAN  - Informations sur le technicien
# GETINFOS_STATE       - Informations état
# GETINFOS_ALL         - Toutes les informations
use constant
{
    GETINFOS_CONTRACT   => 0x0002,
    GETINFOS_MACHINE    => 0x0004,
    GETINFOS_CUSTOMER   => 0x0008,
    GETINFOS_TECHNICIAN => 0x0010,
    GETINFOS_SITE       => 0x0016,
    GETINFOS_STATE      => 0x0020,
    GETINFOS_ALL        => 0xFFFF
};

# Constants: Emplacements de photos
# PHOTOS_PATH - Chemin du dossier photo
# PHOTOS_URL  - URL du docussier photo
# PHOTOS_TEMP - Chemin du dossier temporaire
use constant
{
    PHOTOS_PATH => &LOC::Util::getCommonFilesPath('repairSheetPhotos/'),
    PHOTOS_URL  => &LOC::Globals::get('commonFilesUrl') . '/repairSheetPhotos/',
    PHOTOS_TEMP => &LOC::Util::getCommonFilesPath('temp/')
};

# Constants: Format de récupération de liste
# SHEET_FORMAT     - récupération pour affichage sur la fiche de remise en état
# ESTIMATE_FORMAT  - récupération pour affichage sur le devis de remise en état
use constant
{
    SHEET_FORMAT    => 0x0001,
    ESTIMATE_FORMAT => 0x0002
};

# Constants: Récupération d'informations sur les checkpoints
# GETCHECKPOINTINFOS_ALL - Informations sur les points de vérification
use constant
{
    GETCHECKPOINTINFOS_ALL => 0x0010
};

# Constants: Type de bascule
# TURNIN_DAYS       - Jours
# TURNIN_SERVICES   - Services
use constant
{
    TURNIN_DAYS     => 'days',
    TURNIN_SERVICES => 'services'
};

# Constants: Flags de bascule
# FLAG_TURNEDINESTIMATE  - Bascule en devis
# FLAG_TURNEDINSERVICES  - Bascule en services
# FLAG_TURNEDINDAYS      - Bascule en jours
use constant
{
    FLAG_TURNEDINESTIMATE => 0,
    FLAG_TURNEDINSERVICES => 1,
    FLAG_TURNEDINDAYS     => 2
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Repair::ProfitMargin;
use LOC::Alert;
use LOC::Machine;
use LOC::Repair::Checkpoint;
use LOC::Repair::Estimate;
use LOC::Repair::LineType;
use LOC::Common;
use LOC::Contract::RepairDay;
use LOC::Contract::RepairService;
use LOC::Proforma;
use LOC::Log;

my $machineStates = [
    LOC::Machine::STATE_RENTED,
    LOC::Machine::STATE_AVAILABLE,
    LOC::Machine::STATE_RECOVERY,
    LOC::Machine::STATE_REVIEW,
    LOC::Machine::STATE_CONTROL,
    LOC::Machine::STATE_BROKEN,
    LOC::Machine::STATE_CHECK,
    LOC::Machine::STATE_ATNRENOVATION,
    LOC::Machine::STATE_TRANSFER,
    LOC::Machine::STATE_AVAILABLEFAUILLET,
    LOC::Machine::STATE_RETURNED,
    LOC::Machine::STATE_DELIVERY
];

# Function: getInfos
# Retourne un hachage contenant les infos de la fiche de remise en état
#
# Parameters:
# string $countryId - Pays
# int    $id        - Id de la fiche de remise en état
# int    $flags     - flags
#
# Returns:
# hash|hashref
sub getInfos
{
    my ($countryId, $id, $flags) = @_;

    my $tab = getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Récupérer la liste des fiches de remise en état
#
# Contenu du hachage:
# - id                   => id
# - contract.id          => id du contrat associé
# - machine.id           => id de la machine associée
# - user.id              => Id du technicien créateur de la fiche
# - currency.id          => Id devise
# - state.id             => Id de l'etat de la fiche
# - date                 => Date de création de la fiche par le technicien
# - labTime              => Nombre d'heure de main d'oeuvre
# - labUnitCost          => Cout de l'heure de main d'oeuvre
# - labCost              => Cout de la main d'oeuvre
# - labUnitAmount        => Montant facturé de l'heure de la main d'oeuvre
# - labAmount            => Montant facturé de main d'oeuvre
# - travelTime           => Nombre d'heure de main d'oeuvre
# - travelTimeUnitCost   => Cout de l'heure de main d'oeuvre
# - travelTimeCost       => Cout de la main d'oeuvre
# - travelTimeUnitAmount => Montant facturé de l'heure de la main d'oeuvre
# - travelTimeAmount     => Montant facturé de main d'oeuvre
# - dspKm                => Nombre de km de déplacement
# - dspUnitCost          => Cout du km de déplacement
# - dspCost              => Cout total deplacement
# - dspUnitAmount        => Montant facturé du km de déplacement
# - dspAmount            => Montant facturé du déplacement
# - sheetCost            => Cout total fiche de casse
# - sheetAmount          => Montant total facturé de la fiche de casse
# - dateCreation         => date de création de la fiche en base
# - dateModification     => date de dernière modification
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id)
# int     $flags      - Flags
#
# Returns:
# hash|hashref
sub getList
{
    my ($countryId, $format, $tabFilters, $flags, $tabOptions) = @_;
    unless (defined $flags)
    {
        $flags = 0;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '
SELECT ' . (defined $tabOptions->{'limit'} && exists ($tabOptions->{'limit'}->{'&total'}) ? 'SQL_CALC_FOUND_ROWS' : '');

    # Filtres
    my $whereQuery = '';
    # - Article
    if (defined $tabFilters->{'id'})
    {
        if (ref($tabFilters->{'id'}) eq 'ARRAY')
        {
            $whereQuery .= '
AND rps_id IN (' . $db->quote($tabFilters->{'id'}) . ')';
        }
        else
        {
            if ($tabFilters->{'id'} =~ /%/)
            {
                $whereQuery .= '
AND rps_id LIKE ' . $db->quote($tabFilters->{'id'});
            }
            else
            {
                $whereQuery .= '
AND rps_id = ' . $db->quote($tabFilters->{'id'});
            }
        }
    }
    if (defined $tabFilters->{'stateId'})
    {
        $whereQuery .= '
AND rps_sta_id IN (' . $db->quote($tabFilters->{'stateId'}) . ')';
    }

    if (defined $tabFilters->{'agencyId'})
    {
        if (ref($tabFilters->{'agencyId'}) eq 'ARRAY')
        {
            $whereQuery .= '
AND rps_agc_id IN (' . $db->quote($tabFilters->{'agencyId'}) . ')';
        }
        else
        {
            $whereQuery .= '
AND rps_agc_id = ' . $db->quote($tabFilters->{'agencyId'});
        }
    }

    if (defined $tabFilters->{'parkNumber'})
    {
        if ($tabFilters->{'parkNumber'} =~ /%/)
        {
            $whereQuery .= '
AND MANOPARC LIKE ' . $db->quote($tabFilters->{'parkNumber'});
        }
        else
        {
            $whereQuery .= '
AND MANOPARC = ' . $db->quote($tabFilters->{'parkNumber'});
        }
    }
    if (defined $tabFilters->{'contractId'})
    {
        $whereQuery .= '
AND rps_rct_id IN (' . $db->quote($tabFilters->{'contractId'}) . ')';
    }

    if (defined $tabFilters->{'contractCode'})
    {
        if (&LOC::Util::isNumeric($tabFilters->{'contractCode'}))
        {
            $whereQuery .= '
AND SUBSTR(tbl_f_contract.CONTRATCODE, -7) = ' . $db->quote($tabFilters->{'contractCode'});
        }
        else
        {
            if ($tabFilters->{'contractCode'} =~ /%/)
            {
                $whereQuery .= '
AND tbl_f_contract.CONTRATCODE like ' . $db->quote($tabFilters->{'contractCode'});
            }
            else
            {
                $whereQuery .= '
AND tbl_f_contract.CONTRATCODE = ' . $db->quote($tabFilters->{'contractCode'});
            }
        }
    }
    if (defined $tabFilters->{'customerId'})
    {
        $whereQuery .= '
AND tbl_f_customer.CLAUTO = ' . $db->quote($tabFilters->{'customerId'});
    }
    if (defined $tabFilters->{'interlocutorId'})
    {
        $whereQuery .= '
AND rps_interlocutor = ' . $db->quote($tabFilters->{'interlocutorId'});
    }
    if (defined $tabFilters->{'dateInterval'})
    {
        my $dateInterval = $tabFilters->{'dateInterval'};
        if (defined $dateInterval->{'start'} && defined $dateInterval->{'end'})
        {
            $whereQuery .= '
AND (DATE(rps_date_creation) BETWEEN ' . $db->quote($dateInterval->{'start'}) . ' AND ' . $db->quote($dateInterval->{'end'}) . ')';
        }
        elsif (defined $dateInterval->{'start'})
        {
            $whereQuery .= '
AND DATE(rps_date_creation) >= ' . $db->quote($dateInterval->{'start'});
        }
        elsif (defined $dateInterval->{'end'})
        {
            $whereQuery .= '
AND DATE(rps_date_creation) <= ' . $db->quote($dateInterval->{'end'});
        }
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query .= '
    rps_id AS `id`,
    rps_rct_id AS `contract.id`,
    rps_agc_id AS `agency.id`,
    agc_cty_id AS `country.id`,
    rps_mac_id AS `machine.id`,
    rps_usr_id AS `user.id`,
    rps_interlocutor AS `interlocutor.id`,
    rps_cur_id AS `currency.id`,
    rps_date AS `date`,
    rps_labourtime AS `labourTime`,
    rps_labourunitcost AS `labourUnitCost`,
    rps_labourcost AS `labourCost`,
    rps_labourunitamount AS `labourUnitAmount`,
    rps_labouramount AS `labourAmount`,
    rps_displtime AS `travelTime`,
    rps_displtimeunitcost AS `travelTimeUnitCost`,
    rps_displtimecost AS `travelTimeCost`,
    rps_displtimeunitamount AS `travelTimeUnitAmount`,
    rps_displtimeamount AS `travelTimeAmount`,
    rps_displkm AS `displKm`,
    rps_displunitcost AS `displUnitCost`,
    rps_displcost AS `displCost`,
    rps_displunitamount AS `displUnitAmount`,
    rps_displamount AS `displAmount`,
    rps_totalcost AS `sheetCost`,
    rps_totalamount AS `sheetAmount`,
    rps_comment AS `sheetComment`,
    rps_date_creation AS `dateCreation`,
    rps_date_modification AS `dateModification`,
    rps_sta_id AS `state.id`,
    IF(rps_sta_id = "' . STATE_WAITING . '", 1, 0) AS `isMachineModifiable`,
    rps_flag AS `flag`,
    tbl_f_contract.CONTRATPROFORMAOK AS `proformaFlags`,
    tbl_f_contract.CONTRATEMAILCOMM AS `orderContact.email`,
    rpe_id AS `repairEstimate.id`,
    rpe_code AS `codeRepairEstimate`,
    rps_abortcomment AS `abortComment`';
        if ($flags & GETINFOS_MACHINE)
        {
            $query .= ',
    tbl_f_machine.MANOPARC AS `machine.parkNumber`,
    tbl_f_machine.MANOSERIE AS `machine.serialNumber`,
    tbl_f_model.MOMADESIGNATION AS `model.label`,
    tbl_p_businessfamily.fly_id AS `family.id`,
    tbl_p_tariffamily.FAMTARLIBELLE AS `family.label`';
        }
        if ($flags & GETINFOS_CONTRACT)
        {
            $query .= ',
    tbl_f_contract.CONTRATCODE AS `contract.code`';
        }
        if ($flags & GETINFOS_CUSTOMER)
        {
            $query .= ',
    tbl_f_customer.CLCODE AS `customer.code`,
    tbl_f_customer.CLAUTO AS `customer.id`,
    tbl_f_customer.CLEMAIL AS `customer.email`,
    tbl_f_customer.CLSTE AS `customer.name`,
    -tbl_f_customer.CLCEE AS `customer.isVatInvoiced`,
    -tbl_f_customer.CLDECHETSFACT AS `customer.isResiduesInvoiced`';
        }
        $query .= ',
    tbl_f_contract.CHAUTO AS `site.id`';
        if ($flags & GETINFOS_SITE)
        {
            $query .= ',
    tbl_f_site.CHLIBELLE AS `site.label`,
    tbl_p_sitelocality.VICP AS `site.locality.postalCode`,
    tbl_p_sitelocality.VINOM AS `site.locality.name`,
    CONCAT (tbl_p_sitelocality.VICP, " ", tbl_p_sitelocality.VINOM) AS `site.locality.label`';
        }
        if ($flags & GETINFOS_TECHNICIAN)
        {
            $query .= ',
    CONCAT_WS(" ", f_user_tech.usr_firstname, f_user_tech.usr_name) AS `user.fullName`,
    f_user_tech.usr_sta_id AS `user.state.id`';
        }
        if ($flags & GETINFOS_STATE)
        {
            $query .= ',
    tbl_p_state.ETLIBELLE AS `state.label`';
        }
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query .= '
    COUNT(*)';
    }

    $query .= '
FROM f_repairsheet
LEFT JOIN frmwrk.p_agency ON rps_agc_id = agc_id
LEFT JOIN f_repairestimate ON 

    rpe_id = 
    ( SELECT rpe_id from f_repairestimate WHERE
        rpe_rps_id = rps_id
        AND 
        (
            (rpe_sta_id != ' . $db->quote(LOC::Repair::Estimate::STATE_ABORTED) . ')
            OR 
            (
                (rpe_sta_id = ' . $db->quote(LOC::Repair::Estimate::STATE_ABORTED) . ')
                AND (rpe_flag IS NULL)
            )
        )
        ORDER BY rpe_id DESC
        LIMIT 1
    )

INNER JOIN CONTRAT AS tbl_f_contract ON (tbl_f_contract.CONTRATAUTO = rps_rct_id AND tbl_f_contract.CONTRATOK = -1)';

    if (defined $tabFilters->{'parkNumber'} || ($flags & GETINFOS_MACHINE))
    {
        $query .= '
INNER JOIN AUTH.MACHINE AS tbl_f_machine ON tbl_f_machine.MAAUTO = rps_mac_id
INNER JOIN AUTH.MODELEMACHINE AS tbl_f_model ON tbl_f_machine.MOMAAUTO = tbl_f_model.MOMAAUTO
INNER JOIN FAMILLETARIFAIRE as tbl_p_tariffamily ON tbl_f_model.FAMTARAUTO = tbl_p_tariffamily.FAMTARAUTO
INNER JOIN FAMILLEMACHINE tbl_p_machinesfamily ON tbl_f_model.FAMAAUTO = tbl_p_machinesfamily.FAMAAUTO
INNER JOIN FAMILLECOMMERCIALE tbl_p_businessfamily ON tbl_p_machinesfamily.FACOMAUTO = tbl_p_businessfamily.FACOMAUTO';
    }
#    if (defined $tabFilters->{'contractCode'} || ($flags & GETINFOS_CONTRACT) ||
#        defined $tabFilters->{'customerId'} || ($flags & GETINFOS_CUSTOMER))
#    {
#        $query .= '
#INNER JOIN CONTRAT AS tbl_f_contract ON (tbl_f_contract.CONTRATAUTO = rps_rct_id AND tbl_f_contract.CONTRATOK = -1)';
#    }
    if (defined $tabFilters->{'customerId'} || ($flags & GETINFOS_CUSTOMER))
    {
        $query .= '
INNER JOIN CLIENT AS tbl_f_customer ON tbl_f_customer.CLAUTO = tbl_f_contract.CLAUTO';
    }
    if ($flags & GETINFOS_SITE)
    {
        $query .= '
LEFT JOIN CHANTIER AS tbl_f_site
ON tbl_f_site.CHAUTO = tbl_f_contract.CHAUTO
LEFT JOIN VILLECHA tbl_p_sitelocality
ON tbl_f_site.VILLEAUTO = tbl_p_sitelocality.VILLEAUTO';
    }
    if ($flags & GETINFOS_TECHNICIAN)
    {
        $query .= '
LEFT JOIN frmwrk.f_user f_user_tech
ON rps_usr_id = f_user_tech.usr_id';
    }
    if ($flags & GETINFOS_STATE)
    {
        $query .= '
LEFT JOIN ETATTABLE tbl_p_state
ON rps_sta_id = tbl_p_state.ETCODE';
    }

    $query .= '
WHERE 1' .
    $whereQuery;

    # Options
    if (defined $tabOptions->{'orders'})
    {
        my @orders;
        foreach my $order (@{$tabOptions->{'orders'}})
        {
            my $dir = ($order->{'dir'} ? $order->{'dir'} : '');
            if ($order->{'field'} eq 'id')
            {
                push(@orders, 'rps_id' . ' ' . $dir);
            }
        }
        $query .= '
ORDER BY ' . join(', ', @orders);
    }
    else
    {
        $query .= '
ORDER BY rps_id';
    }

    if (defined $tabOptions->{'limit'})
    {
        my $offset = 0;
        my $rowCount = 0;

        if (ref $tabOptions->{'limit'} eq 'HASH')
        {
            $offset = (defined $tabOptions->{'limit'}->{'offset'} ? $tabOptions->{'limit'}->{'offset'} : 0);
            $rowCount = (defined $tabOptions->{'limit'}->{'count'} ? $tabOptions->{'limit'}->{'count'} : 0);
        }
        else
        {
            $rowCount = $tabOptions->{'limit'};
        }

        $query .= '
LIMIT ' . $offset . ',' . $rowCount;
    }

    $query .= ';';

    my $tab = $db->fetchList($format, $query, {}, 'id');

    if (!$tab)
    {
        return undef;
    }

    # Récupération du total complet
    if (defined $tabOptions->{'limit'} && exists ($tabOptions->{'limit'}->{'&total'}))
    {
        $tabOptions->{'limit'}->{'&total'} = $db->fetchOne('SELECT FOUND_ROWS();');
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'labourUnitCost'} *= 1;
            $tabRow->{'labourCost'} *= 1;
            $tabRow->{'labourUnitAmount'} *= 1;
            $tabRow->{'labourAmount'} *= 1;

            $tabRow->{'travelTimeUnitCost'} *= 1;
            $tabRow->{'travelTimeCost'} *= 1;
            $tabRow->{'travelTimeUnitAmount'} *= 1;
            $tabRow->{'travelTimeAmount'} *= 1;

            $tabRow->{'displKm'} *= 1;
            $tabRow->{'displUnitCost'} *= 1;
            $tabRow->{'displCost'} *= 1;
            $tabRow->{'displUnitAmount'} *= 1;
            $tabRow->{'displAmount'} *= 1;

            $tabRow->{'sheetCost'} *= 1;
            $tabRow->{'sheetAmount'} *= 1;
            $tabRow->{'site.id'} *= 1;

            $tabRow->{'flag'} = (defined $tabRow->{'flag'} ? $tabRow->{'flag'} * 1 : undef);
        }
    }

    return (wantarray ? %$tab : $tab);
}

# Function: getServices
# Récupérer la liste des services d'une fiche de remise en état
##
# Parameters:
# string  $countryId    - Pays
# int     $sheetId      - Fiche de remise en état
# int     $format       - Format de retour
# int     $flags        - flag
# hashref $tabFilters   - Tableau des filtes
#
# Returns:
# hash|hashref
sub getCheckPoints
{
    my ($countryId, $sheetId, $format, $flags, $tabFilters) = @_;

    unless (defined $format)
    {
        $format = LOC::Util::GETLIST_ASSOC;
    }

    unless (defined $flags)
    {
        $flags = 0;
    }


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = 'SELECT ';

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query .= '
    rch_id AS `id`,
    rch_rps_id AS `sheet.id`,
    rch_chp_id AS `checkpoint.id`,
    IF (rch_chp_id IS NULL, 1, 0) AS `isReference`,
    rch_label as `label`,
    rch_cur_id AS `currency.id`,
    rch_status AS `status`,
    rch_repairtype AS `repairType`,
    rch_articlecode AS `artCode`,
    rch_articleexternalid AS `artExtId`,
    rch_quantity AS `quantity`,
    rch_unitcost AS `unitCost`,
    rch_cost AS `cost`,
    rch_unitamount AS `unitAmount`,
    rch_amount AS `amount`';

        if (defined $tabFilters->{'family.id'})
        {
            $query .= ',
    chf_order AS `chp.order`';
        }
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query .= '
    COUNT(*)';
    }
    if (defined $tabFilters->{'family.id'})
    {
        $query .= '
FROM f_repairsheet_checkpoint
INNER JOIN p_checkpoint_family
ON rch_chp_id = chf_chp_id AND chf_fly_id = ' . $tabFilters->{'family.id'} . '
WHERE rch_rps_id = ' . $db->quote($sheetId) . '
ORDER BY chf_order;';
    }
    elsif (defined $tabFilters->{'isReference'} && $tabFilters->{'isReference'} == 0)
    {
        $query .= '
FROM f_repairsheet_checkpoint
WHERE rch_chp_id IS NOT NULL
AND  rch_rps_id = ' . $db->quote($sheetId) . ';';
    }
    elsif (defined $tabFilters->{'isReference'} && $tabFilters->{'isReference'} == 1)
    {
        $query .= '
FROM f_repairsheet_checkpoint
WHERE rch_chp_id IS NULL
AND  rch_rps_id = ' . $db->quote($sheetId) . ';';
    }
    else
    {
        $query .= '
FROM f_repairsheet_checkpoint
WHERE rch_rps_id = ' . $db->quote($sheetId) . ';';
    }
    my $tab;
    #On ordonne le tableau sur les Id des points de vérifs
    if ($flags & SHEET_FORMAT)
    {
        $tab = $db->fetchList($format, $query, {}, 'checkpoint.id');
    }
    elsif ($flags & ESTIMATE_FORMAT)
    {
        $tab = $db->fetchList($format, $query, {}, 'checkpoint.id');
    }
    else
    {
        $tab = $db->fetchList($format, $query, {}, 'id');
    }

    if (!$tab)
    {
        return undef;
    }
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        # Formatage des données
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}             = $tabRow->{'id'} * 1;
            $tabRow->{'sheet.id'}       = $tabRow->{'sheet.id'} * 1;
            $tabRow->{'checkpoint.id'}  = $tabRow->{'checkpoint.id'} * 1;
            $tabRow->{'status'}         = $tabRow->{'status'} * 1;
            defined ($tabRow->{'artExtId'}) ? $tabRow->{'artExtId'} * 1 : undef;
            $tabRow->{'quantity'}       = $tabRow->{'quantity'} * 1;

            if ($tabRow->{'artCode'})
            {
                my $article = &LOC::Repair::Article::getInfosFromKimoce($countryId, {'code' => $tabRow->{'artCode'}});

                $tabRow->{'unitCost'}       = &LOC::Util::round($article->{'localCost'} * 1, 2);
                $tabRow->{'cost'}           = $tabRow->{'unitCost'} * $tabRow->{'quantity'};
                $tabRow->{'unitMargin'}     = $article->{'margin'};
                $tabRow->{'unitAmount'}     = $tabRow->{'unitCost'} + $tabRow->{'unitMargin'};
                $tabRow->{'amount'}         = $tabRow->{'unitAmount'} * $tabRow->{'quantity'};

            }
            else
            {
                $tabRow->{'unitCost'}       = $tabRow->{'unitCost'} * 1;
                $tabRow->{'cost'}           = $tabRow->{'cost'} * 1;
                $tabRow->{'unitAmount'}     = $tabRow->{'unitAmount'} * 1;
                $tabRow->{'amount'}         = $tabRow->{'amount'} * 1;

                # Calcul des marges
                $tabRow->{'unitMargin'} = $tabRow->{'unitAmount'} - $tabRow->{'unitCost'};
            }
            $tabRow->{'margin'}     = $tabRow->{'amount'} - $tabRow->{'cost'};
            $tabRow->{'isReference'}    = $tabRow->{'isReference'} * 1;


            $tabRow->{'checkpoint.tabInfos'} = undef;
            if ($flags & GETCHECKPOINTINFOS_ALL && $tabRow->{'checkpoint.id'})
            {
                $tabRow->{'checkpoint.tabInfos'} = &LOC::Repair::Checkpoint::getInfos($countryId, $tabRow->{'checkpoint.id'});
            }
        }
    }

    return (wantarray ? %$tab : $tab);
}

# Function: getServices
# Récupérer la liste des services d'une fiche de remise en état
##
# Parameters:
# string  $countryId  - Pays
# int     $sheetId   - Fiche de remise en état
# int     $format     - Format de retour
# int     $flags     - flag
#
# Returns:
# hash|hashref
sub getServices
{
    my ($countryId, $sheetId, $format, $flags) = @_;

    unless (defined $format)
    {
        $format = LOC::Util::GETLIST_ASSOC;
    }

    unless (defined $flags)
    {
        $flags = 0;
    }


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = 'SELECT ';

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query .= '
    rsv_id AS `id`,
    rsv_rps_id AS `sheet.id`,
    rsv_srv_id AS `service.id`,
    rsv_cur_id AS `currency.id`,
    rsv_type AS `type`,
    rsv_quantity AS `quantity`,
    rsv_unitcost AS `unitCost`,
    rsv_cost AS `cost`,
    rsv_unitamount AS `unitAmount`,
    rsv_amount AS `amount`,
    srv_name AS `service.name`,
    srv_comment AS `service.infos`';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query .= '
    COUNT(*)';
    }

    $query .= '
FROM f_repairsheet_service
LEFT JOIN p_service
ON rsv_srv_id = srv_id
WHERE rsv_rps_id = ' . $db->quote($sheetId) . ';';

    my $tab;
    if ($flags == SHEET_FORMAT)
    {
        $tab = $db->fetchList($format, $query, {}, 'service.id');
    }
    else
    {
        $tab = $db->fetchList($format, $query, {}, 'id');
    }
    if (!$tab)
    {
        return undef;
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        # Formatage des données
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}             = $tabRow->{'id'} * 1;
            $tabRow->{'sheet.id'}       = $tabRow->{'sheet.id'} * 1;
            $tabRow->{'service.id'}     = $tabRow->{'service.id'} * 1;
            $tabRow->{'quantity'}       = $tabRow->{'quantity'} * 1;
            $tabRow->{'unitCost'}       = $tabRow->{'unitCost'} * 1;
            $tabRow->{'cost'}           = $tabRow->{'cost'} * 1;
            $tabRow->{'unitAmount'}     = $tabRow->{'unitAmount'} * 1;
            $tabRow->{'amount'}         = $tabRow->{'amount'} * 1;
        }
    }

    return (wantarray ? %$tab : $tab);
}

# Function: getPhotos
# Récupérer la liste des Photos de la fiche de remise en état
##
# Parameters:
# string  $countryId   - Id du pays
# int     $sheetId     - Id de la fiche
#
# Returns:
# hash|hashref
sub getPhotos
{
    my ($countryId, $sheetId, $tabOptions) = @_;

    my $path = PHOTOS_PATH . $countryId . '/' . $sheetId;
    my $url  = PHOTOS_URL . $countryId . '/' . $sheetId;
    my @tabPhotos = ();

    if (-e $path)
    {
        opendir (my $myDirectory, $path);
        my @tabFilesList = grep { !/^\.\.?$/ } readdir($myDirectory);
        closedir ($myDirectory);

        foreach my $fileName (@tabFilesList)
        {
            $fileName = &LOC::Util::decodeUtf8($fileName);
            my $prefix = substr($fileName, 0, 5);
            if (-f $path . '/' . $fileName && $prefix ne 'thumb')
            {
                my $thumbFileName = 'thumb_' . $fileName;
                $thumbFileName =~ s/\.[^.]*$/\.png/;

                push(@tabPhotos, {
                    'path'      => $path . '/' . $fileName,
                    'url'       => $url . '/' . $fileName,
                    'name'      => $fileName,
                    'thumbPath' => $path . '/' . $thumbFileName,
                    'thumbUrl'  => $url . '/' . $thumbFileName,
                    'thumbName' => $thumbFileName
                });
            }
        }
    }

    return \@tabPhotos;
}

# Function: getList
# Récupérer la liste des Machines de remise en état
#
# Parameters:
# int     $format     - Format de retour
# string  $countryId  - Pays
# int     $agencyId   - Agence
#
# Returns:
# hash|hashref
sub getMachinesList
{
    my ($format, $countryId, $agencyId) = @_;

    return &LOC::Machine::getList($countryId, $format, {'countryId' => $countryId, 'agencyId' => $agencyId, 'stateId' => $machineStates}, LOC::Machine::GETINFOS_BASE);
}


# Function: getTransferedMachinesList
# Récupérer la liste des Machines transférées dans une autre agence que l'agence en cours et pour lesquelles il y a eu des contrats il y a moins de 6 mois
#
# Parameters:
# int     $format     - Format de retour
# string  $countryId  - Pays
# int     $agencyId   - Agence
#
# Returns:
# hash|hashref
sub getTransferedMachinesList
{
    my ($format, $countryId, $agencyId) = @_;

    return &LOC::Machine::getTransferedMachinesList($countryId, $format, {'countryId' => $countryId, 'agencyId' => $agencyId, 'stateId' => $machineStates});
}


# Function save
# Met à jour (insert/update) toutes les infos d'une fiche de remise en état
# Met à jour (insert/update) toutes les infos des services de remise en état pour la fiche
# Met à jour (insert/update) toutes les infos des points de vérification  pour la fiche
#
# Parameters :
# string    $countryId      - Pays
# int       $userId         - Identifiant de l'utilisateur
# hashref   $sheetDatas     - Infos sur la fiche de remise en état
# hashref   $srvAdded       - Infos sur les services de remise en état
# float     $srvMargin      - % de marche appliquée au service externe
# hashref   $chpAdded       - Infos sur les points de vérification
# bool      $exitSave       - 1 : quitter l'enregistrement, 0 sinon
#                             (actuellement utilisé pour ne pas enregistrer la fiche si on n'a pas les droits de modif
#                               et que la fiche doit être abandonnée ou basculée)
# arrayref  $errorsTab      - Tableau des erreurs
#
# Contenu des hachages à passer en paramètre
### $sheetDatas :
# - id           => id
# - machine.id   => Id de la machine associée
# - contract.id  => id du contrat associé
# - currency.id  => id devise
# - creator.id   => Id du technicien createur de la fiche
# - date         => Date de creation de la fiche papier
# - labTime      => Nombre d'heure de main d'oeuvre
# - labUntCst    => Cout de l'heure de main d'oeuvre
# - labCst       => Cout de la main d'oeuvre
# - labUntAmt    => Montant facturé de l'heure de la main d'oeuvre
# - labAmt       => Montant facturé de main d'oeuvre
# - tvTime       => Nombre d'heure de trajet
# - tvtUntCst    => Cout de l'heure de trajet
# - tvtCst       => Cout du temps de trajet
# - tvtUntAmt    => Montant facturé de l'heure de trajet
# - tvtAmt       => Montant facturé du temps de trajet
# - dspKm        => Nombre de km de déplacement
# - dspUntCst    => Cout du km de déplacement
# - dspCst       => Cout total deplacement
# - dspUntAmt    => Montant facturé du km de déplacement
# - dspAmt       => Montant facturé du déplacement
# - sheetCst     => Cout total fiche de casse
# - sheetAmt     => Montant total facturé de la fiche de casse
#
### $srvAdded :
# - sheet.id    => id de la fiche de remise en etat associée
# - service.id  => Id du service
# - currency.id => id devise
# - type        => type du service
# - cost        => cout du service
# - amount      => Montant facturé du service
#
### $tabCheckpoints / $tabReferences
# - sheet.id          => id de la fiche de remise en etat associée
# - checkpoint.id     => Id du service
# - currency.id       => id devise
# - status            => statut de point de verification
# - repairType        => type de reparation
# - articleCode       => code de l'article
# - articleExternalId => id Kimoce de l'article
# - quantity          => quantité
# - unitCost          => cout unitaire
# - cost              => cout total
# - unitAmount        => montant facturé unitaire
# - amount            => Montant facturé total
#
# Returns:
#integer $sheetId ou 0
sub save()
{
    my ($countryId, $userId, $sheetDatas, $srvAdded, $srvMargin, $tabCheckpoints, $tabReferences, $exitSave, $errorsTab) = @_;

    # Récupération des droits
    my $tabRights = &getRights($countryId, $sheetDatas->{'agency.id'}, $sheetDatas->{'id'}, $userId);

    # Utilisateur n'ayant pas les droits
    if ($tabRights->{'actions'} !~ /^(|.*\|)valid(|\|.*)$/)
    {
        if ($exitSave)
        {
            return $sheetDatas->{'id'};
        }
        push(@$errorsTab, 'rightsError');
        return 0;
    }

    # récupération du contract.id dans le cas où la fiche a déjà été basculée ou abandonnée
    if ($sheetDatas->{'state.id'} eq LOC::Repair::Sheet::STATE_TURNEDIN || $sheetDatas->{'state.id'} eq LOC::Repair::Sheet::STATE_ABORTED || $sheetDatas->{'state.id'} eq LOC::Repair::Sheet::STATE_CANCELED)
    {
        $sheetDatas->{'contract.id'} = &_getContractId($sheetDatas->{'id'}, $countryId);
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    my $sheetId;

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    ### Mise à jour de la fiche de remise en état ###
    if (defined $sheetDatas->{'id'} && $sheetDatas->{'id'} != 0)
    {
        my $result = $db->update('f_repairsheet', {
                                                   'rps_agc_id'              => $sheetDatas->{'agency.id'},
                                                   'rps_rct_id'              => $sheetDatas->{'contract.id'},
                                                   'rps_mac_id'              => $sheetDatas->{'machine.id'},
                                                   'rps_usr_id'              => $sheetDatas->{'creator.id'},
                                                   'rps_interlocutor'        => $sheetDatas->{'interlocutor.id'},
                                                   'rps_cur_id'              => $sheetDatas->{'currency.id'},
                                                   'rps_date'                => $sheetDatas->{'date'},
                                                   'rps_labourtime'          => $sheetDatas->{'labTime'},
                                                   'rps_labourunitcost'      => $sheetDatas->{'labUntCst'},
                                                   'rps_labourcost'          => $sheetDatas->{'labCst'},
                                                   'rps_labourunitamount'    => $sheetDatas->{'labUntAmt'},
                                                   'rps_labouramount'        => $sheetDatas->{'labAmt'},
                                                   'rps_displtime'           => $sheetDatas->{'tvTime'},
                                                   'rps_displtimeunitcost'   => $sheetDatas->{'tvtUntCst'},
                                                   'rps_displtimecost'       => $sheetDatas->{'tvtCst'},
                                                   'rps_displtimeunitamount' => $sheetDatas->{'tvtUntAmt'},
                                                   'rps_displtimeamount'     => $sheetDatas->{'tvtAmt'},
                                                   'rps_displkm'             => $sheetDatas->{'dspKm'},
                                                   'rps_displunitcost'       => $sheetDatas->{'dspUntCst'},
                                                   'rps_displcost'           => $sheetDatas->{'dspCst'},
                                                   'rps_displunitamount'     => $sheetDatas->{'dspUntAmt'},
                                                   'rps_displamount'         => $sheetDatas->{'dspAmt'},
                                                   'rps_totalcost'           => $sheetDatas->{'sheetCst'},
                                                   'rps_totalamount'         => $sheetDatas->{'sheetAmt'},
                                                   'rps_comment'             => $sheetDatas->{'sheetComment'},
                                                   'rps_date_modification*'  => 'NOW()',
                                                  },
                                                  {
                                                   'rps_id'                 => $sheetDatas->{'id'}
                                                  });

        if ($result == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            push(@$errorsTab, 'fatalError');
            return 0;
        }

        $sheetId = $sheetDatas->{'id'};
    }

    # Création de la fiche de remise en état
    else
    {
        my $result = $db->insert('f_repairsheet', {
                                                 'rps_agc_id'              => $sheetDatas->{'agency.id'},
                                                 'rps_rct_id'              => $sheetDatas->{'contract.id'},
                                                 'rps_mac_id'              => $sheetDatas->{'machine.id'},
                                                 'rps_usr_id'              => $sheetDatas->{'creator.id'},
                                                 'rps_interlocutor'        => $sheetDatas->{'interlocutor.id'},
                                                 'rps_cur_id'              => $sheetDatas->{'currency.id'},
                                                 'rps_date'                => $sheetDatas->{'date'},
                                                 'rps_labourtime'          => $sheetDatas->{'labTime'},
                                                 'rps_labourunitcost'      => $sheetDatas->{'labUntCst'},
                                                 'rps_labourcost'          => $sheetDatas->{'labCst'},
                                                 'rps_labourunitamount'    => $sheetDatas->{'labUntAmt'},
                                                 'rps_labouramount'        => $sheetDatas->{'labAmt'},
                                                 'rps_displtime'           => $sheetDatas->{'tvTime'},
                                                 'rps_displtimeunitcost'   => $sheetDatas->{'tvtUntCst'},
                                                 'rps_displtimecost'       => $sheetDatas->{'tvtCst'},
                                                 'rps_displtimeunitamount' => $sheetDatas->{'tvtUntAmt'},
                                                 'rps_displtimeamount'     => $sheetDatas->{'tvtAmt'},
                                                 'rps_displkm'             => $sheetDatas->{'dspKm'},
                                                 'rps_displunitcost'       => $sheetDatas->{'dspUntCst'},
                                                 'rps_displcost'           => $sheetDatas->{'dspCst'},
                                                 'rps_displunitamount'     => $sheetDatas->{'dspUntAmt'},
                                                 'rps_displamount'         => $sheetDatas->{'dspAmt'},
                                                 'rps_totalcost'           => $sheetDatas->{'sheetCst'},
                                                 'rps_totalamount'         => $sheetDatas->{'sheetAmt'},
                                                 'rps_comment'             => $sheetDatas->{'sheetComment'},
                                                 'rps_date_creation*'      => 'NOW()',
                                                 'rps_sta_id'              => LOC::Repair::Sheet::STATE_WAITING,
                                             });

        if ($result == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            push(@$errorsTab, 'fatalError');
            return 0;
        }

        if ($result != -1)
        {
            &LOC::Alert::setDeltaCounterAlert(29, $sheetDatas->{'agency.id'}, +1);
        }

        $sheetId = $result;
    }

    #### Si la fiche n'est pas en état basculé ou abandonné
    if ($sheetDatas->{'state.id'} ne LOC::Repair::Sheet::STATE_TURNEDIN && $sheetDatas->{'state.id'} ne LOC::Repair::Sheet::STATE_ABORTED && $sheetDatas->{'state.id'} ne LOC::Repair::Sheet::STATE_CANCELED)
    {
        ### Mise à jour des services de remise en état ###

        tie(my %srvList, 'Tie::IxHash');
        my %srvSavedList;

        #Parcourt du tableau des prestas techniques (enregistrées et temp)
        foreach (keys %{$srvAdded})
        {
            my $srvObj = $srvAdded->{$_};

            # Si pas de prestas == "Aucune"
            if ($srvObj->{'id'} != 0)
            {
                my $isInternalSrv = (($srvObj->{'type'} eq LOC::Repair::LineType::TYPE_INTERNALSERVICE) || ($srvObj->{'type'} eq 'INM'));
                my $unitCost = $isInternalSrv ? $srvObj->{'unitCst'} : $srvObj->{'cost'};
                my $unitAmount = $isInternalSrv ? $srvObj->{'unitAmt'} : $srvObj->{'cost'};
                my $qty = 1;
                my $cost = $unitCost;
                my $amount = $isInternalSrv ? $srvObj->{'unitAmt'} : $srvObj->{'cost'};


                #Service interne ou interne modifiable avec quantité
                if ($isInternalSrv && $srvObj->{'hasQty'})
                {
                    $qty = $srvObj->{'qty'};
                    $cost = $qty * $unitCost;
                    $amount = $qty * $unitAmount;
                }

                #Service externe avec marge autorisée
                if ($srvObj->{'type'} eq LOC::Repair::LineType::TYPE_EXTERNALSERVICE)
                {
                    if ($srvObj->{'isMarginAllowed'} == 1)
                    {
                        $unitAmount = $amount = $cost + $cost * $srvMargin;
                    }
                    elsif (($srvObj->{'isActivated'} == 0) && ($srvObj->{'isMarginAllowed'} eq 'undefined'))
                    {
                        $unitAmount = $srvObj->{'unitAmt'};
                        $amount     = $qty * $unitAmount;
                    }
                }

                my $srvDatas =
                {
                    'id'                => $srvObj->{'lineId'},
                    'sheet.id'          => $sheetId,
                    'service.id'        => $srvObj->{'id'},
                    'currency.id'       => $srvObj->{'currency.id'},
                    'cost'              => $cost,
                    'unitAmount'        => $unitAmount,
                    'amount'            => $amount,
                    'type'              => $srvObj->{'type'},
                    'unitCost'          => $unitCost,
                    'quantity'          => $qty
                };

                $srvList{$srvObj->{'id'}}      = $srvDatas;#Services en cours d'enregistrement(ADD, DELETE, UPDATE)
                $srvSavedList{$srvObj->{'id'}} = $srvObj->{'isSaved'};
            }
        }

        # Récupération des services (déjà enregistrés) dans la base de données
        my $savedSrv = getServices($countryId, $sheetId, LOC::Util::GETLIST_ASSOC, SHEET_FORMAT);

        tie(my %insertList, 'Tie::IxHash');
        tie(my %updateList, 'Tie::IxHash');

        # Equivalence entre les champs de la table et les alias de la requête
        my $dbNames = {
                        'sheet.id'      => 'rsv_rps_id',
                        'service.id'    => 'rsv_srv_id',
                        'currency.id'   => 'rsv_cur_id',
                        'type'          => 'rsv_type',
                        'quantity'      => 'rsv_quantity',
                        'unitCost'      => 'rsv_unitcost',
                        'cost'          => 'rsv_cost',
                        'unitAmount'    => 'rsv_unitamount',
                        'amount'        => 'rsv_amount'
                     };
        #Tableau qui va contenir tous les ids des services de la fiche
        my @ids;

        # On parcourt les services en cours sur la fiche
        foreach my $sheetSrvId (keys %srvList)
        {
            # Id des services déjà enregistrés en base
            my @dbSrvIds = keys (%$savedSrv);

            # Si service déjà enregistré, on regarde s'il y a eu des modifs
            if ((&LOC::Util::in_array($sheetSrvId, \@dbSrvIds)) && ($srvSavedList{$sheetSrvId} == 1))
            {
                my $sheetSrv = $srvList{$sheetSrvId};
                tie(my %updatesTmp, 'Tie::IxHash');# Rangement du tableau de hachage dans l'ordre croissant
                # Parcourt de toutes les colonnes du service en cours
                foreach my $sheetSrvValue (keys %$sheetSrv)
                {
                    if (&LOC::Util::isInt($sheetSrv->{$sheetSrvValue}))
                    {
                        if ($savedSrv->{$sheetSrvId}->{$sheetSrvValue} != $sheetSrv->{$sheetSrvValue})
                        {
                            $updatesTmp{$dbNames->{$sheetSrvValue}} = $sheetSrv->{$sheetSrvValue};
                        }
                    }
                    else
                    {
                        if ($savedSrv->{$sheetSrvId}->{$sheetSrvValue} ne $sheetSrv->{$sheetSrvValue})
                        {
                            $updatesTmp{$dbNames->{$sheetSrvValue}} = $sheetSrv->{$sheetSrvValue};
                        }
                    }
                }
                # Nb d'elements mis à jour dans le tableau de hachage <> de 0
                if (keys (%updatesTmp) != 0)
                {
                    $updateList{$sheetSrvId} = \%updatesTmp;
                }

                # Rajout des Id dans le tableau
                push(@ids, $srvList{$sheetSrvId}->{'id'});
            }
            else
            {
                $insertList{$sheetSrvId} = $srvList{$sheetSrvId};
            }
        }

        # Vérification des droits
        if (keys %insertList > 0 && $tabRights->{'services'} !~ /^(|.*\|)add(|\|.*)$/)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            push(@$errorsTab, 'rightsError');
            return 0;
        }
        #Insertion des prestations
        foreach my $insertId (keys %insertList)
        {
            my $srvLine = $insertList{$insertId};
            my $rpsSrvId = $db->insert('f_repairsheet_service', {
                                                     'rsv_rps_id'       => $srvLine->{'sheet.id'},
                                                     'rsv_srv_id'       => $srvLine->{'service.id'},
                                                     'rsv_cur_id'       => $srvLine->{'currency.id'},
                                                     'rsv_cost'         => $srvLine->{'cost'},
                                                     'rsv_amount'       => $srvLine->{'amount'},
                                                     'rsv_unitamount'   => $srvLine->{'unitAmount'},
                                                     'rsv_type'         => $srvLine->{'type'},
                                                     'rsv_quantity'     => $srvLine->{'quantity'},
                                                     'rsv_unitcost'     => $srvLine->{'unitCost'},
                                                 });

            if ($rpsSrvId == -1)
            {
                #Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                push(@$errorsTab, 'servicesError');
                return 0;

            }
            push(@ids, $rpsSrvId);
        }

        #Mise à jour des prestations
        foreach my $updateId (keys %updateList)
        {
            my $chpLine = $updateList{$updateId};
            # Vérification des droits
            # - Quantité
            # - Montant
            if ($chpLine->{'rsv_quantity'} && $tabRights->{'services'} !~ /^(|.*\|)quantity(|\|.*)$/ ||
                $chpLine->{'rsv_type'} && $tabRights->{'services'} !~ /^(|.*\|)internal(|\|.*)$/ ||
                $chpLine->{'rsv_type'} && $tabRights->{'services'} !~ /^(|.*\|)external(|\|.*)$/ ||
                $chpLine->{'rsv_amount'} && $tabRights->{'services'} !~ /^(|.*\|)totalAmount(|\|.*)$/)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                push(@$errorsTab, 'rightsError');
                return 0;
            }
            my $updateResult = $db->update('f_repairsheet_service', $updateList{$updateId}, {'rsv_id' => $srvList{$updateId}->{'id'}});
            if ($updateResult == -1)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                push(@$errorsTab, 'servicesError');
                return 0;
            }
        }

        # Suppression des prestations
        #recuperation des service sur la fiche
        my $sheetService = &getServices($countryId, $sheetId);
        my $isServiceDeletable = $tabRights->{'services'} =~ /^(|.*\|)delete(|\|.*)$/;
        #Pour chaque service
        foreach my $tabLineService (values %{$sheetService})
        {
            #Si le service n'est pas dans @ids, donc si on le supprime
            if (!&LOC::Util::in_array($tabLineService->{'id'}, \@ids))
            {
                # Vérification des droits
                if(!$isServiceDeletable)
                {
                    # Annule la transaction
                    if (!$hasTransaction)
                    {
                        $db->rollBack();
                    }
                    push(@$errorsTab, 'rightsError');
                    return 0;
                }

                #recuperation des lignes sur le devis où rel_srv_id n'est pas a null
                my $queryEstimateLineService ='
SELECT
    rel_id AS `id`,
    rel_rsv_id AS `sheet.service.id`,
    rpe_id AS `estimate.id`
FROM f_repairestimateline
INNER JOIN f_repairestimate ON rpe_id =rel_rpe_id
INNER JOIN f_repairsheet ON rps_id = rpe_rps_id
WHERE rel_rsv_id IS NOT NULL
AND rps_id = ' . $sheetId;
                my $tabEstiLineSrv = $db->fetchList(LOC::Util::GETLIST_ASSOC, $queryEstimateLineService, {}, 'id');
                #pour chaque ligne
                foreach my $tabRowEstiLineSrv (values %{$tabEstiLineSrv})
                {
                    #Si rel_srv_id n'est pas dans @ids
                    if (!&LOC::Util::in_array($tabRowEstiLineSrv->{'rel_rsv_id'}, \@ids))
                    {
                        #mise a jour du devis en non modifiable CAS07
                        my $result = $db->update('f_repairestimate', {'rpe_sta_id' => LOC::Repair::Estimate::STATE_NOCHANGE, 'rpe_flag' => LOC::Repair::Estimate::FLAG_SHEETUPDATED}, {'rpe_id*' => $tabRowEstiLineSrv->{'estimate.id'}});
                        if ($result == -1)
                        {
                            # Annule la transaction
                            if (!$hasTransaction)
                            {
                                $db->rollBack();
                            }

                            push(@$errorsTab, ['deleteServicesEstimateError', {'id' => $tabLineService->{'service.name'}}]);
                            return 0;
                        }
                        #mise a null du rel_rsv_id
                        $result = $db->update('f_repairestimateline', {'rel_rsv_id' => undef}, {'rel_id*' => $tabRowEstiLineSrv->{'id'}});
                        if ($result == -1)
                        {
                            # Annule la transaction
                            if (!$hasTransaction)
                            {
                                $db->rollBack();
                            }
                            push(@$errorsTab, ['deleteServicesEstimateError', {'id' => $tabLineService->{'service.name'}}]);
                            return 0;
                        }
                    }
                }
                #suppression du service dans la fiche
                my $query = '
DELETE
FROM f_repairsheet_service
WHERE rsv_id = ' . $tabLineService->{'id'};
                my $deleteResult = $db->query($query);

                if ($deleteResult == 0)
                {
                    # Annule la transaction
                    if (!$hasTransaction)
                    {
                        $db->rollBack();
                    }
                    push(@$errorsTab, ['deleteServicesError', {'id' => $tabLineService->{'service.name'}}]);
                    return 0;
                }
            }
        }

        ### Mise à jour des points de vérification ###

        tie(my %chpList, 'Tie::IxHash');

        push(@$tabCheckpoints, @$tabReferences);

        # Récupération de la liste des points de vérification déjà enregistrés en BDD
        my $savedCheckpoints = &getCheckPoints($countryId, $sheetId, LOC::Util::GETLIST_ASSOC, 0);

        tie(my %insertList, 'Tie::IxHash');
        tie(my %updateList, 'Tie::IxHash');

        # Liste des Id à supprimer
        my @deleteIds;

        # Liste des Ids récupérés de l'ancien système (points de vérif passés en référence)
        my @updateTransitionIds;
        my @deleteTransitionIds;

        #Parcourt la liste de tous les points de vérification
        foreach my $checkpointInfos (@$tabCheckpoints)
        {
            my $chpDatas = {
                'sheet.id'      => $sheetId,
                'checkpoint.id' => $checkpointInfos->{'checkpoint.id'},
                'label'         => $checkpointInfos->{'label'},
                'currency.id'   => $checkpointInfos->{'currency.id'},
                'artCode'       => undef,
                'artExtId'      => undef,
                'repairType'    => $checkpointInfos->{'repairType'},
                'quantity'      => $checkpointInfos->{'quantity'}
            };
            # Dans le cas où la colonne n'est pas à OK
            if ($checkpointInfos->{'state'} ne 'removed')
            {
                $chpDatas->{'status'} = 0;

                $chpDatas->{'artCode'}  = $checkpointInfos->{'artCode'};
                $chpDatas->{'artExtId'} = $checkpointInfos->{'artExtId'};

                $chpDatas->{'unitCost'} = $checkpointInfos->{'unitCost'};
                $chpDatas->{'cost'}       = $checkpointInfos->{'cost'};
                $chpDatas->{'amount'}     =  $checkpointInfos->{'cost'} + $checkpointInfos->{'margin'};
                $chpDatas->{'unitAmount'} = $chpDatas->{'unitCost'} + $checkpointInfos->{'unitMargin'};
            }
            else
            {
                push(@deleteIds, $checkpointInfos->{'id'});
            }

            $chpList{$checkpointInfos->{'id'}} = $chpDatas;
        }

        my $dbNames = {
                        'sheet.id'      => 'rch_rps_id',
                        'checkpoint.id' => 'rch_chp_id',
                        'label'         => 'rch_label',
                        'currency.id'   => 'rch_cur_id',
                        'status'        => 'rch_status',
                        'repairType'    => 'rch_repairtype',
                        'artCode'       => 'rch_articlecode',
                        'artExtId'      => 'rch_articleexternalid',
                        'quantity'      => 'rch_quantity',
                        'unitCost'      => 'rch_unitcost',
                        'cost'          => 'rch_cost',
                        'unitAmount'    => 'rch_unitamount',
                        'amount'        => 'rch_amount'
                     };

        foreach my $chpId (keys %chpList)
        {
            my @tabSavedIds = keys (%$savedCheckpoints);
            if (!$savedCheckpoints->{$chpId}->{'checkpoint.id'})
            {
                $savedCheckpoints->{$chpId}->{'checkpoint.id'} = undef;
            }

            if (&LOC::Util::in_array($chpId, \@tabSavedIds))
            {
                my $checkpointData = $chpList{$chpId};
                tie(my %updatesTmp, 'Tie::IxHash');

                foreach my $property (keys %$checkpointData)
                {
                    if (!defined $checkpointData->{$property})
                    {
                        if (defined $savedCheckpoints->{$chpId}->{$property})
                        {
                            $updatesTmp{$dbNames->{$property}} = $checkpointData->{$property};
                        }
                    }
                    elsif (&LOC::Util::isInt($checkpointData->{$property}))
                    {
                        if ($savedCheckpoints->{$chpId}->{$property} != $checkpointData->{$property} )
                        {
                            $updatesTmp{$dbNames->{$property}} = $checkpointData->{$property};
                        }
                    }
                    else
                    {
                        if ($savedCheckpoints->{$chpId}->{$property} ne $checkpointData->{$property})
                        {
                            # Migration de l'ancien système :
                            # - Lorsqu'on récupère les anciens points de vérification avec une référence, il n'y a pas de libellé
                            # - Celui-ci est maintenant enregistré.
                            # - Si un profil qui n'a pas le droit de modifier les références enregistre la FRE nouvellement migrée, il reçoit une erreur de modif dû à ces libellés
                            if ($property eq 'label' && !defined $savedCheckpoints->{$chpId}->{$property})
                            {
                                push(@updateTransitionIds, $chpId);
                            }
                            $updatesTmp{$dbNames->{$property}} = $checkpointData->{$property};
                        }
                    }
                }
                if (keys (%updatesTmp) != 0)
                {
                    $updateList{$chpId} = \%updatesTmp;
                }
            }
            else
            {
                $insertList{$chpId} = $chpList{$chpId};
            }
        }

        foreach my $savedChpId (keys %$savedCheckpoints)
        {
            my @sheetChpIds = keys (%chpList);

            if (!&LOC::Util::in_array($savedChpId, \@sheetChpIds))
            {
                push (@deleteIds, $savedChpId);

                # Migration de l'ancien système :
                # - On récupère tous les points de vérif : même ceux non utilisés
                # - Maintenant on les supprime.
                # - Si un profil qui n'a pas le droit de supprimer des points de vérif et enregistre la FRE nouvellement migrée, il reçoit une erreur de modif dû à ces suppressions
                if ($savedCheckpoints->{$savedChpId}->{'status'})
                {
                    push(@deleteTransitionIds, $savedChpId);
                }
            }
        }

        # Vérification des droits
        if (keys %insertList > 0 && $tabRights->{'references'} !~ /^(|.*\|)add(|\|.*)$/)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            push(@$errorsTab, 'rightsError');
            return 0;
        }
        foreach my $insertId (keys %insertList)
        {
            my $chpLine = $insertList{$insertId};
            if (!$chpLine->{'checkpoint.id'})
            {
                $chpLine->{'checkpoint.id'} = undef;
            }

            my $rpsChpId = $db->insert('f_repairsheet_checkpoint', {
                                                     'rch_rps_id'           => $chpLine->{'sheet.id'},
                                                     'rch_chp_id'           => $chpLine->{'checkpoint.id'},
                                                     'rch_label'            => $chpLine->{'label'},
                                                     'rch_cur_id'           => $chpLine->{'currency.id'},
                                                     'rch_status'           => $chpLine->{'status'},
                                                     'rch_repairtype'       => $chpLine->{'repairType'},
                                                     'rch_articlecode'      => $chpLine->{'artCode'},
                                                     'rch_articleexternalid'=> $chpLine->{'artExtId'},
                                                     'rch_quantity'         => $chpLine->{'quantity'},
                                                     'rch_unitcost'         => &LOC::Util::round($chpLine->{'unitCost'}, 2),
                                                     'rch_cost'             => &LOC::Util::round($chpLine->{'cost'}, 2),
                                                     'rch_unitamount'       => &LOC::Util::round($chpLine->{'unitAmount'}, 2),
                                                     'rch_amount'           => &LOC::Util::round($chpLine->{'amount'}, 2),
                                                 });
            if ($rpsChpId == -1)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                push(@$errorsTab, 'referencesError');
                return 0;
            }
        }
        # Mise à jour des points de vérification paramétrés
        foreach my $updateId (keys %updateList)
        {
            my $chpLine = $updateList{$updateId};
            # Vérification des droits
            if (($chpLine->{'rch_articlecode'} || ($chpLine->{'rch_label'} && !&LOC::Util::in_array($updateId, \@updateTransitionIds)) || $chpLine->{'rch_quantity'}) &&
                $tabRights->{'references'} !~ /^(|.*\|)edit(|\|.*)$/)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                push(@$errorsTab, 'rightsError');
                return 0;
            }
            my $update = $db->update('f_repairsheet_checkpoint', $updateList{$updateId},
                                     {'rch_rps_id' => $sheetId, 'rch_id' => $updateId});

            if ($update == -1)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                push(@$errorsTab, 'referencesError');
                return 0;
            }
        }

        my @tabCondition = ();
        # Points de vérification à supprimer
        # Vérification des droits
        my @tabDiffDeleteIds = &LOC::Util::array_minus(\@deleteIds, \@deleteTransitionIds);
        if (@tabDiffDeleteIds > 0 && $tabRights->{'references'} !~ /^(|.*\|)delete(|\|.*)$/)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            push(@$errorsTab, 'rightsError');
            return 0;
        }

        if (@deleteIds > 0)
        {
            push(@tabCondition, 'rch_id IN (' . $db->quote(@deleteIds) . ')');
        }

        if (@tabCondition > 0)
        {
            # Suppression des liens entre les DRE et les points de vérification à supprimer
            # et passage en CAS07
            my $query = '
    UPDATE f_repairestimateline
    LEFT JOIN f_repairestimate ON rel_rpe_id = rpe_id
    LEFT JOIN f_repairsheet_checkpoint ON rpe_rps_id = rch_rps_id
    SET rpe_sta_id = ' . $db->quote(LOC::Repair::Estimate::STATE_NOCHANGE) . ',
        rpe_flag = 3,
        rel_rch_id = NULL
    WHERE rpe_rps_id = ' . $db->quote($sheetId) . '
        AND (' . join(' OR ', @tabCondition) . ')';
    
            if ($db->query($query) == 0)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                push(@$errorsTab, 'deleteReferencesEstimateError');
                return 0;
            }

            # Suppression des points de vérification sur la FRE
            my $query = '
    DELETE FROM f_repairsheet_checkpoint
    WHERE rch_rps_id = ' . $db->quote($sheetId) . '
        AND (' . join(' OR ', @tabCondition) . ')';
    
            if ($db->query($query) == 0)
            {
                # Annule la transaction
                if (!$hasTransaction)
                {
                    $db->rollBack();
                }
                push(@$errorsTab, ['deleteReferencesError']);
                return 0;
            }
        }
    }

    ### Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    return $sheetId;


}


# Function: abort
# Abandonner une fiche de remise en état
#
# Parameters:
# string    $countryId   - Pays
# string    $agencyId    - Agence
#hashref    $values      - Valeurs
#
# Returns:
# bool 0 si l'abandon a échoué, 1 s'il a réussi
sub abort
{
    my ($countryId, $agencyId, $values, $userId) = @_;

    # Récupération des droits
    my $tabRights = &getRights($countryId, $agencyId, $values->{'id'}, $userId);

    # Utilisateur n'ayant pas les droits
    if ($tabRights->{'actions'} !~ /^(|.*\|)abort(|\|.*)$/)
    {
        return -1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Mise à jour
    my $updateResult = $db->update('f_repairsheet',
                                  {'rps_sta_id' => STATE_ABORTED, 'rps_abortcomment' => $values->{'abortComment'}},
                                  {'rps_id' => $values->{'id'}});

    if ($updateResult == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    if ($updateResult)
    {
        &LOC::Alert::setDeltaCounterAlert(29, $agencyId, -1);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    return $updateResult;
}

# Function: cancel
# Annulée une fiche de remise en état
#
# Parameters:
# string    $countryId   - Pays
# string    $agencyId    - Agence
#hashref    $values      - Valeurs
#
# Returns:
# bool 0 si l'annulation a échoué, 1 s'elle a réussi
sub cancel
{
    my ($countryId, $agencyId, $values, $userId) = @_;

    # Récupération des droits
    my $tabRights = &getRights($countryId, $agencyId, $values->{'id'}, $userId);

    # Utilisateur n'ayant pas les droits
    if ($tabRights->{'actions'} !~ /^(|.*\|)cancel(|\|.*)$/)
    {
        return -1;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    # Mise à jour
    my $updateResult = $db->update('f_repairsheet',
                                  {'rps_sta_id' => STATE_CANCELED},
                                  {'rps_id' => $values->{'id'}});

    if ($updateResult == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    if ($updateResult)
    {
        &LOC::Alert::setDeltaCounterAlert(29, $agencyId, -1);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    return $updateResult;
}

# Function: reopen
# Rouvre une fiche de remise en état
#
# Parameters:
# string    $countryId   - Pays
# string    $agencyId    - Agence
# integer   $id          - Id de la fiche de remise en état
#
# Returns:
# bool 0 si l'abandon a échoué, 1 s'il a réussi
sub reopen
{
    my ($countryId, $agencyId, $id) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $updateResult = $db->update('f_repairsheet',
                                   {'rps_sta_id' => STATE_WAITING, 'rps_flag*' => 'NULL'},
                                   {'rps_id' => $id});

    if ($updateResult == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return -1;
    }

    if ($updateResult)
    {
        &LOC::Alert::setDeltaCounterAlert('29', $agencyId, +1);
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return -1;
        }
    }

    return $updateResult;
}


# Function: getStatesList
# Retourne la liste des etats
#
# Parameters:
# string    $countryId      - Pays
#
# Returns:
# hash
sub getStatesList
{
    my ($countryId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = 'SELECT ETCODE AS id, ETLIBELLE AS label FROM ETATTABLE
    WHERE ETCODE IN (' . $db->quote((STATE_WAITING, STATE_ABORTED, STATE_CANCELED, STATE_TURNEDIN)) . ') ORDER BY ETLIBELLE';

    return $db->fetchPairs($query);
}

# Function: turnInto
# Passe la fiche de remise en état dans le statut "basculé"
#
# Parameters:
# string    $countryId   - Pays
# string    $agencyId    - Agence
# integer   $id          - Id de la fiche de remise en état
# hashref   $values      - Liste de valeurs
# integer   $userId      - Identifiant de l'utilisateur
#
# Returns:
# integer
sub turnInto
{
    my ($countryId, $agencyId, $id, $turnIn, $values, $userId) = @_;

    # Récupération des droits
    my $tabRights = &getRights($countryId, $agencyId, $id, $userId);
    my $sheetInfos = getInfos($countryId, $id);

    # Utilisateur n'ayant pas les droits
    if ($turnIn eq TURNIN_DAYS && $tabRights->{'actions'} !~ /^(|.*\|)days(|\|.*)$/ || $sheetInfos->{'proformaFlags'} & LOC::Proforma::DOCFLAG_EXISTACTIVED)
    {
        return -1;
    }
    if ($turnIn eq TURNIN_SERVICES && $tabRights->{'actions'} !~ /^(|.*\|)services(|\|.*)$/ || $sheetInfos->{'proformaFlags'} & LOC::Proforma::DOCFLAG_EXISTACTIVED)
    {
        return -1;
    }

    my $result = 0;
    # Type de bascule
    my $flag  = '';
    # Bascule en Jours
    if ($turnIn eq TURNIN_DAYS)
    {
        $flag = FLAG_TURNEDINDAYS;
        $result  = &LOC::Contract::RepairDay::set($countryId, $values);

        # Ecriture dans la table d'historique
        if ($result != -1)
        {
            my $tabTraces = [{'code' => LOC::Log::EventType::TURNTOREPAIRDAYS, 'content' => $values->{'repairsheet.id'}}];
            &LOC::Log::writeDbFunctionalityLogs('CONTRAT', $values->{'contract.id'}, $tabTraces);
        }
    }
    # Bascule en Services
    else
    {
        $flag = FLAG_TURNEDINSERVICES;
        my $sheetInfos = getInfos($countryId, $id);

        # Préparation paramétres services
        my $i = 0;
        my $serviceValues;
        my @labourTime = split(/:/, $sheetInfos->{'labourTime'});
        my @travelTime = split(/:/, $sheetInfos->{'travelTime'});
        # si déplacement
        if ($sheetInfos->{'displKm'} != 0)
        {
            my $lineTypeInfos = &LOC::Repair::LineType::getInfos($countryId, LOC::Repair::LineType::TYPE_DISPLACEMENT);
            my $label = $lineTypeInfos->{'label'} . ' ' . sprintf($lineTypeInfos->{'extraLabel'}, $sheetInfos->{'displKm'});
            $serviceValues->{$i} = {
                'contract.id'       => $sheetInfos->{'contract.id'},
                'repairsheet.id'    => $sheetInfos->{'id'},
                'currency.id'       => $sheetInfos->{'currency.id'},
                'type'              => LOC::Repair::LineType::TYPE_DISPLACEMENT,
                'label'             => $label,
                'unitAmount'        => $sheetInfos->{'displUnitAmount'},
                'quantity'          => $sheetInfos->{'displKm'},
                'amount'            => $sheetInfos->{'displAmount'},
           };
           $i++;
        }

        # Si main d'oeuvre
        if ($labourTime[0] != 0 || $labourTime[1] != 0)
        {
            my $lineTypeInfos = &LOC::Repair::LineType::getInfos($countryId, LOC::Repair::LineType::TYPE_LABOUR);
            my $label = $lineTypeInfos->{'label'} . ' ' . sprintf($lineTypeInfos->{'extraLabel'}, $labourTime[0], $labourTime[1]);
            $serviceValues->{$i} = {
                'contract.id'       => $sheetInfos->{'contract.id'},
                'repairsheet.id'    => $sheetInfos->{'id'},
                'currency.id'       => $sheetInfos->{'currency.id'},
                'type'              => LOC::Repair::LineType::TYPE_LABOUR,
                'label'             => $label,
                'unitAmount'        => $sheetInfos->{'labourUnitAmount'},
                'quantity'          => ($labourTime[0] + ($labourTime[1] / 60)),
                'amount'            => $sheetInfos->{'labourAmount'},
           };

            $i++;
        }

        # Si temps de trajet
        if ($travelTime[0] != 0 || $travelTime[1] != 0)
        {
            my $lineTypeInfos = &LOC::Repair::LineType::getInfos($countryId, LOC::Repair::LineType::TYPE_TRAVELTIME);
            my $label = $lineTypeInfos->{'label'} . ' ' . sprintf($lineTypeInfos->{'extraLabel'}, $travelTime[0], $travelTime[1]);
            $serviceValues->{$i} = {
                'contract.id'       => $sheetInfos->{'contract.id'},
                'repairsheet.id'    => $sheetInfos->{'id'},
                'currency.id'       => $sheetInfos->{'currency.id'},
                'type'              => LOC::Repair::LineType::TYPE_TRAVELTIME,
                'label'             => $label,
                'unitAmount'        => $sheetInfos->{'travelTimeUnitAmount'},
                'quantity'          => ($travelTime[0] + ($travelTime[1] / 60)),
                'amount'            => $sheetInfos->{'travelTimeAmount'},
           };

            $i++;
        }
        # Points de vérification
        my $checkpoints = &getCheckPoints($countryId, $sheetInfos->{'id'});
        foreach my $chpId (keys %{$checkpoints})
        {
            if ($checkpoints->{$chpId}->{'status'} == 0)
            {
                my $checkpointInfos = &LOC::Repair::Checkpoint::getInfos($countryId,
                                                                         $checkpoints->{$chpId}->{'checkpoint.id'},
                                                                         LOC::Repair::Checkpoint::GETINFOS_DELETED);
                my $label = $checkpointInfos->{'label'};
                if ($label eq undef)
                {
                    $label = $checkpoints->{$chpId}->{'label'};
                }


                $serviceValues->{$i} = {
                                        'contract.id'       => $sheetInfos->{'contract.id'},
                                        'repairsheet.id'    => $sheetInfos->{'id'},
                                        'currency.id'       => $checkpoints->{$chpId}->{'currency.id'},
                                        'type'              => LOC::Repair::LineType::TYPE_SERVICE,
                                        'label'             => $label,
                                        'unitAmount'        => $checkpoints->{$chpId}->{'unitAmount'},
                                        'quantity'          => $checkpoints->{$chpId}->{'quantity'},
                                        'amount'            => $checkpoints->{$chpId}->{'amount'},
                                       };
                $i++;
            }
        }

        # Prestations techniques
        my $services = &getServices($countryId, $sheetInfos->{'id'});

        foreach my $srvId (keys %{$services})
        {
            my $serviceInfos = &LOC::Repair::Service::getInfos($countryId, $services->{$srvId}->{'service.id'});

            $serviceValues->{$i} = {
                                    'contract.id'       => $sheetInfos->{'contract.id'},
                                    'repairsheet.id'    => $sheetInfos->{'id'},
                                    'currency.id'       => $services->{$srvId}->{'currency.id'},
                                    'type'              => LOC::Repair::LineType::TYPE_SERVICE,
                                    'label'             => (defined $serviceInfos->{'name'} ? $serviceInfos->{'name'} : ''),
                                    'unitAmount'        => $services->{$srvId}->{'unitAmount'},
                                    'quantity'          => $services->{$srvId}->{'quantity'},
                                    'amount'            => $services->{$srvId}->{'amount'},
                                   };
            $i++;
        }

        foreach my $contractSrvId (keys %{$serviceValues})
        {
            # Insertion de toutes les données à basculer en Services
            if ($result != -1)
            {
                $result = &LOC::Contract::RepairService::set($countryId, $serviceValues->{$contractSrvId});
            }
        }

        # Ecriture dans la table d'historique
        if ($result != -1)
        {
            my $tabTraces = [{'code' => LOC::Log::EventType::TURNTOREPAIRSERVICES, 'content' => $sheetInfos->{'id'}}];
            &LOC::Log::writeDbFunctionalityLogs('CONTRAT', $sheetInfos->{'contract.id'}, $tabTraces);
        }
    }

    if ($result != -1)
    {
        # Récupération de la connexion à la base de données
        my $db = &LOC::Db::getConnection('location', $countryId);
        my $hasTransaction = $db->hasTransaction();

        # Démarre la transaction
        if (!$hasTransaction)
        {
            $db->beginTransaction();
        }


        my $updateResult = $db->update('f_repairsheet',
                                       {'rps_sta_id' => STATE_TURNEDIN, 'rps_flag' => $flag},
                                       {'rps_id' => $id});


        if ($updateResult == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;
        }


        if ($updateResult != -1)
        {
            &LOC::Alert::setDeltaCounterAlert(29, $agencyId, -1);
        }

        # Valide la transaction
        if (!$hasTransaction)
        {
            if (!$db->commit())
            {
                $db->rollBack();
                return -1;
            }
        }

        return $updateResult;

    }
    else
    {
        return $result;
    }


}

# Function: turnIntoEstimate
# Passe la fiche de remise en devis de remise en état
#
# Parameters:
# string    $countryId   - Pays
# hash      $values      - Liste des valeurs necessaires à la balance
# integer   $id          - Identifiant du devis de remise en état
# integer   $userId      - Identifiant de l'utilisateur

# Contenu du hachage à passer en paramètre:
# - sheetId     => id de la fiche de remise en etat associée
# - agencyId    => Id de l'agence
# - userId      => id de l'utilisateur qui saisit la fiche
# - currencyId  => Id de la monnaie utilisé
# - totalCost   => Cout Total
# - totalAmount => Montant Total

# Returns:
# integer
sub turnIntoEstimate
{
    my ($countryId, $sheetId, $userId) = @_;

    # Récupération des infos de la fiche
    my $sheetInfos = &getInfos($countryId, $sheetId, GETINFOS_CUSTOMER);

    # Récupération des droits
    my $tabRights = &getRights($countryId, $sheetInfos->{'agency.id'}, $sheetInfos->{'id'}, $userId);

    # Utilisateur n'ayant pas les droits
    if ($tabRights->{'actions'} !~ /^(|.*\|)repairEstimate(|\|.*)$/)
    {
        return -1;
    }

    # Hachage ordonné dans l'ordre balancé (ici inséré dans l'ordre croissant)
    tie(my %tabLines, 'Tie::IxHash');

     # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }


    # Récupération des formats Sage (quantité, prix et montant)
    my $charac = &LOC::Characteristic::getCountryValueByCode('FORMATSSAGE', $countryId);
    my $tabSageFormats = {'quantity' => 2, 'price' => 4, 'amount' => 2};
    if ($charac ne '')
    {
        $tabSageFormats = &LOC::Json::fromJson($charac);
    }


    # Montant de base pour le calcul de la participationau recyclage;
    my $residuesBaseAmount = 0;

    # Infos machine
    my $machineInfos = &LOC::Machine::getInfos($countryId, $sheetInfos->{'machine.id'}, LOC::Machine::GETINFOS_BASE, {'level' => LOC::Machine::LEVEL_FAMILY});
    # bascule des points de vérif paramétrés
    my $tab = &getCheckPoints($countryId,
                              $sheetId,
                              LOC::Util::GETLIST_ASSOC,
                              LOC::Repair::Sheet::ESTIMATE_FORMAT,
                              {'family.id' => $machineInfos->{'family.id'}});
    tie(my %lines, 'Tie::IxHash');
    my $i = 0;
    while (my ($id, $line) = each(%$tab))
    {
        # Dans le cas où le point de vérif doit être basculé
        if ($line->{'status'} == 0)
        {
            my $checkpoint = &LOC::Repair::Checkpoint::getInfos($countryId, $line->{'checkpoint.id'},
                                                                LOC::Repair::Checkpoint::GETINFOS_DELETED);

            my $newLine = {
                'currencyId'        => $sheetInfos->{'currency.id'},
                'sheetCheckpointId' => $line->{'id'},
                'type'              => $line->{'repairType'},
                'label'             => $checkpoint->{'label'},
                'quantity'          => $line->{'quantity'},
                'cost'              => &LOC::Util::round($line->{'cost'}, $tabSageFormats->{'amount'}),
                'unitAmount'        => &LOC::Util::round($line->{'unitAmount'}, $tabSageFormats->{'price'}),
                'amount'            => &LOC::Util::round($line->{'amount'}, $tabSageFormats->{'amount'}),
                'reductionPercent'  => 0,
                'finalAmount'       => &LOC::Util::round($line->{'amount'}, $tabSageFormats->{'amount'}),
                'isToInvoice'       => 1
            };

            $residuesBaseAmount += $newLine->{'finalAmount'};

            $lines{$i} = $newLine;
            $i++;
        }
    }


    # bascule des points de vérification libres
    my $tab = &getCheckPoints($countryId,
                              $sheetId,
                              LOC::Util::GETLIST_ASSOC,
                              0,
                              {'isReference' => 1});

    while (my ($id, $line) = each(%$tab))
    {
        # Dans le cas où le point de vérif doit être basculé
        if ($line->{'status'} == 0)
        {
            my $newLine = {
                'currencyId'        => $sheetInfos->{'currency.id'},
                'sheetCheckpointId' => $line->{'id'},
                'type'              => $line->{'repairType'},
                'label'             => $line->{'label'},
                'quantity'          => $line->{'quantity'},
                'cost'              => &LOC::Util::round($line->{'cost'}, $tabSageFormats->{'amount'}),
                'unitAmount'        => &LOC::Util::round($line->{'unitAmount'}, $tabSageFormats->{'price'}),
                'amount'            => &LOC::Util::round($line->{'amount'}, $tabSageFormats->{'amount'}),
                'reductionPercent'  => 0,
                'finalAmount'       => &LOC::Util::round($line->{'amount'}, $tabSageFormats->{'amount'}),
                'isToInvoice'       => 1
            };

            $residuesBaseAmount += $newLine->{'finalAmount'};

            $lines{$i} = $newLine;
            $i++;
        }
    }


    # bascule des presta
    $tab = &getServices($countryId, $sheetId);

    while (my ($id, $line) = each(%$tab))
    {
        my $service = &LOC::Repair::Service::getInfos($countryId, $line->{'service.id'});

        my $newLine = {
            'currencyId'       => $sheetInfos->{'currency.id'},
            'sheetServiceId'   => $line->{'id'},
            'type'             => LOC::Repair::LineType::TYPE_SERVICE,
            'label'            => (defined $service->{'name'} ? $service->{'name'} : ''),
            'quantity'         => $line->{'quantity'},
            'cost'             => &LOC::Util::round($line->{'cost'}, $tabSageFormats->{'amount'}),
            'unitAmount'       => &LOC::Util::round($line->{'unitAmount'}, $tabSageFormats->{'price'}),
            'amount'           => &LOC::Util::round($line->{'amount'}, $tabSageFormats->{'amount'}),
            'reductionPercent' => 0,
            'finalAmount'      => &LOC::Util::round($line->{'amount'}, $tabSageFormats->{'amount'}),
            'isToInvoice'      => 1
        };

        $residuesBaseAmount += $newLine->{'finalAmount'};

        $lines{$i} = $newLine;
        $i++;
    }


    my $tabServices = [];

    # Ajout de la participation au recyclage
    my $residuesMode              = &LOC::Characteristic::getAgencyValueByCode('DECHETSMODE', $sheetInfos->{'agency.id'});
    my $useResiduesForAccessories = &LOC::Characteristic::getAgencyValueByCode('ACCDECHETS', $sheetInfos->{'agency.id'});
    my $residuesValue  = undef;
    my $residuesAmount = 0;

    # Facturé par défaut ou non suivant l'option du client
    my $isCustomerResiduesInvoiced = $sheetInfos->{'customer.isResiduesInvoiced'};

    # - si le mode "pourcentage" est activé
    # - si la FRE porte sur un accessoire et qu'on applique le recyclage aux accessoires
    if ($residuesMode == LOC::Common::RESIDUES_MODE_PERCENT &&
        (!$machineInfos->{'isAccessory'} || $machineInfos->{'isAccessory'} && $useResiduesForAccessories))
    {
        $residuesValue = &LOC::Characteristic::getAgencyValueByCode('PCDECHETS', $sheetInfos->{'agency.id'});

        # Calcul du montant de la participation au recyclage
        $residuesAmount = &LOC::Util::round($residuesBaseAmount * $residuesValue, $tabSageFormats->{'amount'});

        # Ajout de la ligne
        push(@$tabServices, {
                'type'              => LOC::Repair::LineType::TYPE_RESIDUES,
                'serviceId'         => 0,
                'quantity'          => 1,
                'unitAmount'        => &LOC::Util::round($residuesAmount, $tabSageFormats->{'price'}),
                'amount'            => &LOC::Util::round($residuesAmount, $tabSageFormats->{'amount'}),
                'reductionPercent'  => 0,
                'finalAmount'       => &LOC::Util::round($residuesAmount, $tabSageFormats->{'amount'}),
                'cost'              => 0,
                'currencyId'        => $sheetInfos->{'currency.id'},
                'isToInvoice'       => $isCustomerResiduesInvoiced
            }
        );
    }
    # Sinon pas de participation au recyclage facturée
    else
    {
        $residuesMode = LOC::Common::RESIDUES_MODE_NONE;
    }

    # Bascule de la main d'oeuvre, du temps de trajet et du déplacement
    my @labourTime = split(/:/, $sheetInfos->{'labourTime'});
    my @travelTime = split(/:/, $sheetInfos->{'travelTime'});
    push(@$tabServices, {
            'type'              => LOC::Repair::LineType::TYPE_LABOUR,
            'serviceId'         => 0,
            'quantity'          => $labourTime[0] + $labourTime[1] / 60,
            'unitAmount'        => &LOC::Util::round($sheetInfos->{'labourUnitAmount'}, $tabSageFormats->{'price'}),
            'amount'            => &LOC::Util::round($sheetInfos->{'labourAmount'}, $tabSageFormats->{'amount'}),
            'reductionPercent'  => 0,
            'finalAmount'       => &LOC::Util::round($sheetInfos->{'labourAmount'}, $tabSageFormats->{'amount'}),
            'cost'              => &LOC::Util::round($sheetInfos->{'labourCost'}, $tabSageFormats->{'amount'}),
            'currencyId'        => $sheetInfos->{'currency.id'},
            'isToInvoice'       => 1
        },
        {
            'type'              => LOC::Repair::LineType::TYPE_TRAVELTIME,
            'serviceId'         => 0,
            'quantity'          => $travelTime[0] + $travelTime[1] / 60,
            'unitAmount'        => &LOC::Util::round($sheetInfos->{'travelTimeUnitAmount'}, $tabSageFormats->{'price'}),
            'amount'            => &LOC::Util::round($sheetInfos->{'travelTimeAmount'}, $tabSageFormats->{'amount'}),
            'reductionPercent'  => 0,
            'finalAmount'       => &LOC::Util::round($sheetInfos->{'travelTimeAmount'}, $tabSageFormats->{'amount'}),
            'cost'              => &LOC::Util::round($sheetInfos->{'travelTimeCost'}, $tabSageFormats->{'amount'}),
            'currencyId'        => $sheetInfos->{'currency.id'},
            'isToInvoice'       => 1
        },
        {
            'type'              => LOC::Repair::LineType::TYPE_DISPLACEMENT,
            'serviceId'         => 0,
            'quantity'          => $sheetInfos->{'displKm'},
            'unitAmount'        => &LOC::Util::round($sheetInfos->{'displUnitAmount'}, $tabSageFormats->{'price'}),
            'amount'            => &LOC::Util::round($sheetInfos->{'displAmount'}, $tabSageFormats->{'amount'}),
            'reductionPercent'  => 0,
            'finalAmount'       => &LOC::Util::round($sheetInfos->{'displAmount'}, $tabSageFormats->{'amount'}),
            'cost'              => &LOC::Util::round($sheetInfos->{'displCost'}, $tabSageFormats->{'amount'}),
            'currencyId'        => $sheetInfos->{'currency.id'},
            'isToInvoice'       => 1
        }
    );


    # Calcul des taxes
    my $countryInfos = &LOC::Country::getInfos($countryId);
    # Taxe en vigueur
    my $vatPct = ($sheetInfos->{'customer.isVatInvoiced'} ? $countryInfos->{'defaultRate.value'} : 0);

    # Coût total
    my $totalCost = &LOC::Util::round($sheetInfos->{'sheetCost'}, $tabSageFormats->{'amount'});

    # Montant total HT
    my $totalAmount = $sheetInfos->{'sheetAmount'} + ($isCustomerResiduesInvoiced ? $residuesAmount : 0);
    $totalAmount = &LOC::Util::round($totalAmount, $tabSageFormats->{'amount'});

    # Montant de la taxe
    my $vat           = &LOC::Util::round($totalAmount * $vatPct / 100, $tabSageFormats->{'amount'});
    my $taxesIncluded = &LOC::Util::round($totalAmount + $vat, $tabSageFormats->{'amount'});

    # Récupération de ttes les infos de la fiche devant être basculées sur le devis
    my $values = {
        'sheetId'           => $sheetInfos->{'id'},
        'agencyId'          => $sheetInfos->{'agency.id'},
        'userId'            => $sheetInfos->{'user.id'},
        'estimateUserId'    => $sheetInfos->{'interlocutor.id'},
        'currencyId'        => $sheetInfos->{'currency.id'},
        'code'              => '',
        'orderCode'         => '',
        'object'            => '',
        'lines'             => \%lines,
        'services'          => $tabServices,
        'residuesMode'      => $residuesMode,
        'residuesValue'     => $residuesValue,
        'totalCost'         => $totalCost,
        'totalAmount'       => $totalAmount,
        'reductionPercent'  => 0,
        'reductionAmount'   => 0,
        'finalAmount'       => $totalAmount,
        'vatPercent'        => $vatPct,
        'vat'               => $vat,
        'taxesIncluded'     => $taxesIncluded,
        'dateInvoice'       => undef,
        'comments'          => '',
        'notes'             => ''
    };

    # Id du devis
    my $estimateId = &LOC::Repair::Estimate::set($countryId, $values, $userId, 1);

    # Création du code du devis
    if ($estimateId != -1)
    {

        my $code = &LOC::Repair::Estimate::setCode($countryId, $sheetInfos->{'agency.id'}, $estimateId);
        if ($code == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;
        }

        # Bascule de la fiche
        my $updateResult = $db->update('f_repairsheet', {'rps_sta_id' => STATE_TURNEDIN, 'rps_flag'   => FLAG_TURNEDINESTIMATE}, {'rps_id' => $sheetId});

        if ($updateResult == -1)
        {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return -1;

        }

        # Maj du  compteur des alertes FRE
        if ($updateResult != -1)
        {
             &LOC::Alert::setDeltaCounterAlert(29, $sheetInfos->{'agency.id'}, -1);
        }


        # Maj du compteur des alertes DRE
        if (($code != -1) && ($updateResult != -1))
        {
            &LOC::Alert::setDeltaCounterAlert(30, $values->{'agencyId'}, +1);
        }

        # Valide la transaction
        if (!$hasTransaction)
        {
            if (!$db->commit())
            {
                $db->rollBack();
                return -1;
            }
        }

        return $updateResult;
    }
    else
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }

        return -1;
    }


}

# Function: _updateState
# Met à jour l'état d'une fiche de remise en état
#
# Parameters:
# string  $countryId - Pays
# integer $id        - Identifiant de la fiche de remise en état
# string  $stateId   - Identifiant de l'état
#
# Returns:
# bool 0 si la mise à jour a échoué, 1 si elle a réussi
sub _updateState
{
    my ($countryId, $id, $stateId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $result = $db->update('f_repairsheet',  {'rps_sta_id' => $stateId}, {'rps_id' => $id});

    if ($result == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }


    return ($result != -1);

}


# Function: _getContractId
# Récupérer le CONTRATAUTO en cours d'une fiche de remise en état
#
# Parameters:
# int $sheetId      - id de la fiche
# string $countryId - pays
#
# Returns:
# int - CONTRATAUTO
sub _getContractId
{
    my ($sheetId, $countryId) = @_;
    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $query = '
SELECT
    rps_rct_id
FROM f_repairsheet
WHERE rps_id = ' . $sheetId . ';';
    my $contractId = $db->fetchOne($query);
    $contractId = &LOC::Contract::Rent::getActiveLineId($countryId, $contractId);

    return $contractId;
}


# Function: _getRights
# Récupérer les droits sur une fiche de remise en état pour un utilisateur donné
#
# Parameters:
# hashref $tabSheetInfos - Informations sur la fiche de remise en état
# hashref $tabUserInfos  - Informations sur l'utilisateur
#
# Returns:
# hashref - Tableau des droits
sub _getRights
{
    my ($tabSheetInfos, $tabUserInfos) = @_;

    my $stateId  = $tabSheetInfos->{'state.id'};
    my $agencyId = $tabSheetInfos->{'agency.id'};

    my $isAdmin          = $tabUserInfos->{'isAdmin'};
    my $isSuperv         = $tabUserInfos->{'isSuperv'};
    my $isCommercial     = $isAdmin || &LOC::Util::in_array(LOC::User::GROUP_COMMERCIAL, $tabUserInfos->{'tabGroups'});
    my $isTechnician     = $isAdmin || &LOC::Util::in_array(LOC::User::GROUP_TECHNICAL, $tabUserInfos->{'tabGroups'});
    my $isForeman        = $isAdmin || &LOC::Util::in_array(LOC::User::GROUP_FOREMAN, $tabUserInfos->{'tabGroups'});
    my $isTechPlatform   = $isAdmin || &LOC::Util::in_array(LOC::User::GROUP_TECHNICALPLATFORM, $tabUserInfos->{'tabGroups'});
    my $isAgencyOfficial = $isAdmin || &LOC::Util::in_array(LOC::User::GROUP_AGENCYMANAGER, $tabUserInfos->{'tabGroups'});
    my $isRespTech       = $isAdmin || &LOC::Util::in_array(LOC::User::GROUP_TECHNICALMANAGER, $tabUserInfos->{'tabGroups'});

    my $tabRights = {
        'equipment'     => '',
        'infos'         => '',
        'references'    => '',
        'services'      => '',
        'otherServices' => '',
        'photos'        => '',
        'actions'       => ''
    };

    # Vérifie si l'agence de l'utilisateur est identique
    if (!$isAdmin && ($agencyId ne $tabUserInfos->{'nomadAgency.id'}))
    {
        return $tabRights;
    }

    # Fiche en attente
    if ($stateId eq '' || $stateId eq STATE_WAITING)
    {
        if ($isTechnician || $isForeman || $isTechPlatform || $isRespTech)
        {
            $tabRights->{'equipment'} .= '|machine|contract';
            $tabRights->{'infos'} .= '|user|date|comments|interlocutor';
            $tabRights->{'references'} .= '|add|edit|delete';
            $tabRights->{'services'} .= '|add|delete|label|internal|external|unitAmount|quantity|totalAmount';
            $tabRights->{'otherServices'} .= '|labour|travelTime|displacement';
            $tabRights->{'photos'} .= '|add|delete';
            $tabRights->{'actions'} .= '|valid|reinit';
        }
    }
    if ($stateId eq STATE_WAITING)
    {
        if ($isSuperv)
        {
            $tabRights->{'actions'} .= '|print|fullprint';
        }
        if ($isCommercial || $isAgencyOfficial)
        {
            $tabRights->{'services'} .= '|add|delete|label|internal|external|unitAmount|quantity|totalAmount';
            $tabRights->{'otherServices'} .= '|labour|travelTime|displacement';
            $tabRights->{'actions'} .= '|services|days|repairEstimate|abort|print|valid|reinit';
        }
        if ($isTechnician || $isForeman || $isTechPlatform)
        {
            $tabRights->{'actions'} .= '|repairEstimate|cancel|print';
        }
    }

    my $stateEstimate = '';
    # Fiche basculée
    if ($stateId eq STATE_TURNEDIN)
    {
        if ($isSuperv)
        {
            $tabRights->{'actions'} .= '|print';
        }
        if ($isTechnician || $isForeman || $isTechPlatform)
        {
            $tabRights->{'infos'} .= '|comments';
            $tabRights->{'actions'} .= '|valid|reinit';
        }
        # si la fiche est basculée en devis
        if ($tabSheetInfos->{'repairEstimate.id'} ne '')
        {
            my $estimate   = &LOC::Repair::Estimate::getInfos($tabSheetInfos->{'country.id'}, $tabSheetInfos->{'repairEstimate.id'});
            $stateEstimate = $estimate->{'state.id'};
        }
    }

    #Fiche abandonnée ou fiche basculée en devis abandonné
    if ($stateId eq STATE_ABORTED || $stateEstimate eq LOC::Repair::Estimate::STATE_ABORTED)
    {

        if ($isCommercial || $isAgencyOfficial)
        {
            $tabRights->{'actions'} .= '|reactivate';
        }
    }

    return $tabRights;
}


# Function: getRights
# Récupérer les droits sur la fiche de remise en état (par rapport à l'utilisateur)
#
# Parameters:
# string $countryId  - Pays
# string $agencyId   - Agence
# int    $id         - Id de la fiche de remise en état
# int    $userId     - Id de l'utilisateur
#
# Returns:
# hash|hashref - Liste des factures de remise en état
sub getRights
{
    my ($countryId, $agencyId, $id, $userId) = @_;

    my $tabSheetInfos;
    my $tabUserInfos;

    # Informations sur l'utilisateur
    $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Informations sur la fiche de remise en état
    if ($id)
    {
        $tabSheetInfos = &getInfos($countryId, $id);
    }
    else
    {
        $tabSheetInfos = {
            'state.id'     => '',
            'agency.id'    => $agencyId
        };
    }

    return &_getRights($tabSheetInfos, $tabUserInfos);
}


1;