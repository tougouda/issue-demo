use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Repair::LineType
# Module de gestion des types de prestations de remise en état
package LOC::Repair::LineType;


# Constants: États
# STATE_ACTIVE   - Alerte active
# STATE_INACTIVE - Alerte inactive
use constant
{
    STATE_ACTIVE   => 'GEN01',
    STATE_INACTIVE => 'GEN03',
};

# Constants: Types des lignes
# TYPE_CHANGEMENT      - Changement de pièce
# TYPE_REPAIR          - Grosse réparation
# TYPE_FIX             - Petite rénovation
# TYPE_SERVICE         - Changement de pièce
# TYPE_LABOUR          - Main d'oeuvre
# TYPE_TRAVELTIME      - Temps de trajet
# TYPE_DISPLACEMENT    - Déplacement
# TYPE_RESIDUES        - Participation au recyclage
# TYPE_INTERNALSERVICE - Prestation interne
# TYPE_EXTERNALSERVICE - Prestation externe
use constant
{
    TYPE_CHANGEMENT      => 'CHG',
    TYPE_REPAIR          => 'RPR',
    TYPE_FIX             => 'FIX',
    TYPE_SERVICE         => 'SRV',
    TYPE_LABOUR          => 'LAB',
    TYPE_TRAVELTIME      => 'TVT',
    TYPE_DISPLACEMENT    => 'DSP',
    TYPE_RESIDUES        => 'RES',
    TYPE_INTERNALSERVICE => 'INT',
    TYPE_EXTERNALSERVICE => 'EXT',
};

use strict;

# Modules internes
use LOC::Db;
use LOC::Util;


# Function: getInfos
# Retourne un hachage contenant les infos d'une prestation
#
# Parameters:
# string $countryId - Pays
# int    $id        - Id de la prestation
#
# Returns:
# hash|hashref
sub getInfos
{
    my ($countryId, $idOrCode, $flags) = @_;

    my $tab;
    if (&LOC::Util::isNumeric($idOrCode))
    {
        $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $idOrCode}, $flags);
    }
    else
    {
        $tab = &getList($countryId, LOC::Util::GETLIST_ASSOC, {'code' => $idOrCode}, $flags);
    }

    my ($id, $tabInfos) = each(%$tab);

    return ($tabInfos ? (wantarray ? %$tabInfos : $tabInfos) : undef);
}

# Function: getList
# Récupérer la liste des prestations
#
# Contenu du hachage:
# - id          => id
# - label       => libellé
# - extraLabel  => libellé complémentaire
# - state.id    => état
# - code        => code
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id)
# int     $flags      - Flags
#
# Returns:
# hash|hashref
sub getList
{
    my ($countryId, $format, $tabFilters, $flags) = @_;
    unless (defined $flags)
    {
        $flags = 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '';

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    `rlt_id` AS `id`,
    `rlt_label` AS `label`,
    `rlt_extralabel` AS `extraLabel`,
    `rlt_code` AS `code`,
    `rlt_sta_id` AS `state.id`
FROM p_repairlinetype';
    }
    elsif($format == LOC::Util::GETLIST_PAIRS)
    {
        $query = '
SELECT
    `rlt_id` AS `id`,
    `rlt_label` AS `label`
FROM p_repairlinetype';
    }

    # Filtres
    $query .= '
WHERE rlt_sta_id <> ' . $db->quote(STATE_INACTIVE);
    if (defined $tabFilters->{'id'})
    {
        $query .= '
AND `rlt_id` IN (' . $db->quote($tabFilters->{'id'}) . ')';
    }
    if (defined $tabFilters->{'code'})
    {
        $query .= '
AND `rlt_code` IN (' . $db->quote($tabFilters->{'code'}) . ')';
    }

    # Tri
        $query .= '
ORDER BY rlt_label';


    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    return (wantarray ? %$tab : $tab);
}


1;