#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
package Test::LOC::Date;

use Test::More;

# Modules internes
use LOC::Date;


# Appel du calcul de la durée de location
my $getRentDuration = sub
{

    my ($tabInfos) = @_;

    return &LOC::Date::_getRentDuration($tabInfos->{'beginDate'}, $tabInfos->{'endDate'},
                                        $tabInfos->{'tabLegalHolidays'}, $tabInfos->{'contractEndDate'});
};

# Appel du calcul des jours ouvrés
my $getDeltaWorkingDays = sub
{
    my ($tabInfos) = @_;

    return &LOC::Date::_getDeltaWorkingDays($tabInfos->{'beginDate'}, $tabInfos->{'endDate'}, $tabInfos->{'tabLegalHolidays'});
};



# Test du calcul du nombre de jours ouvrés sur une période
sub getDeltaWorkingDaysTest
{
    my %tabParameters = (
        'beginDate' => undef,
        'endDate'   => undef,
        'tabLegalHolidays' => []
    );

    my @tabCases = (
        {
            'label'    => 'Période en semaine - pas de WE - pas de jour férié',
            'expected' => 4,
            'props'    => {
                'beginDate' => '2017-10-10',
                'endDate' => '2017-10-13'
            }
        },
        {
            'label'    => 'Période en semaine - pas de WE - 1 jour férié',
            'expected' => 3,
            'props'    => {
                'beginDate' => '2017-10-10',
                'endDate' => '2017-10-13',
                'tabLegalHolidays' => ['2017-10-12']
            }
        },
        {
            'label'    => 'Période entre 2 semaines - 1 WE - pas de jour férié',
            'expected' => 8,
            'props'    => {
                'beginDate' => '2017-10-10',
                'endDate' => '2017-10-19'
            }
        },
        {
            'label'    => 'Période entre 2 semaines - 1 WE - 1 jour férié en semaine',
            'expected' => 7,
            'props'    => {
                'beginDate' => '2017-10-10',
                'endDate' => '2017-10-19',
                'tabLegalHolidays' => ['2017-10-12']
            }
        },
        {
            'label'    => 'Période entre 2 semaines - 1 WE - 1 jour férié en WE',
            'expected' => 8,
            'props'    => {
                'beginDate' => '2017-10-10',
                'endDate' => '2017-10-19',
                'tabLegalHolidays' => ['2017-10-15']
            }
        },
        {
            'label'    => 'Période entre 2 semaines - 1 WE - 1 jour férié en WE, 1 en semaine',
            'expected' => 7,
            'props'    => {
                'beginDate' => '2017-10-10',
                'endDate' => '2017-10-19',
                'tabLegalHolidays' => ['2017-10-12', '2017-10-15']
            }
        }
    );


    &LOC::Test::test('Calcul du nombre de jours ouvrés',
                     \%tabParameters, \@tabCases,
                     $getDeltaWorkingDays,
                     LOC::Test::TYPE_IS);
}

# Test du calcul de la durée de location
sub getRentDurationTest
{
    my %tabParameters = (
        'beginDate'        => undef,
        'endDate'          => undef,
        'tabLegalHolidays' => [],
        'contractEndDate'  => undef
    );

    my @tabCases = (
        {
            'label'    => 'Date de fin de contrat postérieure à la date de fin de la période',
            'expected' => 5,
            'props'    => {
                'beginDate' => '2017-09-11',
                'endDate'   => '2017-09-17', # Termine un dimanche
                'contractEndDate' => '2017-09-20' # mercredi
            }
        },
        {
            'label'    => 'Date de fin de contrat antérieure à la date de fin de la période',
            'expected' => 5,
            'props'    => {
                'beginDate' => '2017-09-11',
                'endDate'   => '2017-09-20',
                'contractEndDate' => '2017-09-15'
            }
        },
        {
            'label'    => 'Aucun jour férié dans la période - Termine un jour de semaine',
            'expected' => 8,
            'props'    => {
                'beginDate' => '2017-09-11',
                'endDate'   => '2017-09-20'
            }
        },
        {
            'label'    => 'Aucun jour férié dans la période - Termine un samedi',
            'expected' => 11,
            'props'    => {
                'beginDate' => '2017-09-11',
                'endDate'   => '2017-09-23'
            }
        },
        {
            'label'    => 'Aucun jour férié dans la période - Termine un dimanche',
            'expected' => 12,
            'props'    => {
                'beginDate' => '2017-09-11',
                'endDate'   => '2017-09-24'
            }
        },
        {
            'label'    => 'Aucun jour férié dans la période - Termine un jour férié',
            'expected' => 8,
            'props'    => {
                'beginDate' => '2017-09-11',
                'endDate'   => '2017-09-20',
                'tabLegalHolidays' => ['2017-09-20']
            }
        },
        {
            'label'    => 'Aucun jour férié dans la période - Commence un jour de semaine',
            'expected' => 8,
            'props'    => {
                'beginDate' => '2017-09-11',
                'endDate'   => '2017-09-20'
            }
        },
        {
            'label'    => 'Aucun jour férié dans la période - Commence un samedi',
            'expected' => 8,
            'props'    => {
                'beginDate' => '2017-09-09',
                'endDate'   => '2017-09-20'
            }
        },
        {
            'label'    => 'Aucun jour férié dans la période - Commence un dimanche',
            'expected' => 8,
            'props'    => {
                'beginDate' => '2017-09-10',
                'endDate'   => '2017-09-20'
            }
        },
        {
            'label'    => 'Aucun jour férié dans la période - Commence un jour férié',
            'expected' => 7,
            'props'    => {
                'beginDate' => '2017-09-11',
                'endDate'   => '2017-09-20',
                'tabLegalHolidays' => ['2017-09-11']
            }
        },
        {
            'label'    => 'Jour férié dans la période - Termine un jour de semaine',
            'expected' => 7,
            'props'    => {
                'beginDate' => '2017-09-11',
                'endDate'   => '2017-09-20',
                'tabLegalHolidays' => ['2017-09-13']
            }
        },
        {
            'label'    => 'Jour férié dans la période - Termine un samedi',
            'expected' => 10,
            'props'    => {
                'beginDate' => '2017-09-11',
                'endDate'   => '2017-09-23',
                'tabLegalHolidays' => ['2017-09-13']
            }
        },
        {
            'label'    => 'Jour férié dans la période - Termine un dimanche',
            'expected' => 11,
            'props'    => {
                'beginDate' => '2017-09-11',
                'endDate'   => '2017-09-24',
                'tabLegalHolidays' => ['2017-09-13']
            }
        },
        {
            'label'    => 'Jour férié dans la période - Termine un jour férié',
            'expected' => 7,
            'props'    => {
                'beginDate' => '2017-09-11',
                'endDate'   => '2017-09-20',
                'tabLegalHolidays' => ['2017-09-13', '2017-09-20']
            }
        },
        {
            'label'    => 'Que des jours de week-end et fériés dans la période',
            'expected' => 4,
            'props'    => {
                'beginDate' => '2017-09-09',
                'endDate'   => '2017-09-12',
                'tabLegalHolidays' => ['2017-09-11', '2017-09-12']
            }
        },
        {
            'label'    => 'Termine un samedi - Date de fin de contrat le dimanche qui suit ',
            'expected' => 4,
            'props'    => {
                'beginDate'        => '2016-12-28',
                'endDate'          => '2016-12-31',
                'contractEndDate'  => '2017-01-01'
            }
        },
        {
            'label'    => 'Termine un jour de week-end - Date de fin de contrat un week-end suivant',
            'expected' => 4,
            'props'    => {
                'beginDate'        => '2017-04-25',
                'endDate'          => '2017-04-30',
                'contractEndDate'  => '2017-05-06'
            }
        },
        {
            'label'    => 'Termine un week-end - Date de fin de contrat un jour férié accolé ',
            'expected' => 6,
            'props'    => {
                'beginDate'        => '2017-04-25',
                'endDate'          => '2017-04-30',
                'contractEndDate'  => '2017-05-01',
                'tabLegalHolidays' => ['2017-05-01']
            }
        },
        {
            'label'    => 'Termine un week-end - Date de fin de contrat un jour férié non accolé ',
            'expected' => 4,
            'props'    => {
                'beginDate'        => '2017-04-25',
                'endDate'          => '2017-04-30',
                'contractEndDate'  => '2017-05-04',
                'tabLegalHolidays' => ['2017-05-04']
            }
        },
    );

    &LOC::Test::test('Calcul de la durée de location',
                     \%tabParameters, \@tabCases,
                     $getRentDuration,
                     LOC::Test::TYPE_IS);

}


1;
