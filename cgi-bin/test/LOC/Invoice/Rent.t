#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
package Test::LOC::Invoice::Rent;

# Modules internes
use LOC::Invoice::Rent;

# Génère la date de facture en fonction de la date de facturation
my $generateDocumentDate = sub {

    my ($tabInfos) = @_;
    return &LOC::Invoice::Rent::generateDocumentDate($tabInfos->{'rentInvoiceType'}, $tabInfos->{'invoiceDate'});
};

# Retourne la date de début de la période de facturation de la location correspondant à une facturation
my $getRentalPeriodBeginDate = sub {

    my ($tabInfos) = @_;
    return &LOC::Invoice::Rent::getRentalPeriodBeginDate($tabInfos->{'invoiceDate'}, $tabInfos->{'rentInvoiceType'});
};

# Retourne la date de fin de la période de facturation de la location correspondant à une facturation
my $getRentalPeriodEndDate = sub {

    my ($tabInfos) = @_;
    return &LOC::Invoice::Rent::getRentalPeriodEndDate($tabInfos->{'invoiceDate'}, $tabInfos->{'rentInvoiceType'});
};

# Retourne des informations sur les dates pour la facturation d'un contrat
my $getContractInvoiceDatesInfos = sub {

    my ($tabInfos) = @_;

    my $tabSageFormats = {
        'quantity' => 2,
        'price'    => 4,
        'amount'   => 2
    };

    return &LOC::Invoice::Rent::_getContractInvoiceDatesInfos($tabInfos->{'invoiceType'}, $tabInfos->{'tabContractInfos'},
                                                              $tabInfos->{'tabParams'}, $tabSageFormats);
};


# Test de génération de la date de facture
sub generateDocumentDateTest
{
    my %tabParameters = (
        'rentInvoiceType' => undef,
        'invoiceDate'     => undef
    );

    my @tabCases = (
        {
            'label'    => 'Type non défini - Date de facturation non définie',
            'expected' => 0,
        },
        {
            'label'    => 'Type non pris en charge - Date de facturation',
            'expected' => '',
            'props'    => {
                'invoiceDate'     => '2017-12-04',
                'rentInvoiceType' => 'typeDeTest'
            }
        },
        {
            'label'    => 'Type "mensuelle" - Date de facturation non définie',
            'expected' => 0,
            'props'    => {
                'rentInvoiceType' => LOC::Invoice::Rent::INVOICETYPE_MONTH
            }
        },
        {
            'label'    => 'Type "intermédiaire" - Date de facturation non définie',
            'expected' => 0,
            'props'    => {
                'rentInvoiceType' => LOC::Invoice::Rent::INVOICETYPE_INTERMEDIATE
            }
        },
        {
            'label'    => 'Type non défini - Date de facturation',
            'expected' => '2017-11-30',
            'props'    => {
                'invoiceDate' => '2017-12-04'
            }
        },
        {
            'label'    => 'Type "mensuelle" - Date de facturation',
            'expected' => '2017-11-30',
            'props'    => {
                'rentInvoiceType' => LOC::Invoice::Rent::INVOICETYPE_MONTH,
                'invoiceDate'     => '2017-12-04'
            }
        },
        {
            'label'    => 'Type "intermédiaire" - Date de facturation',
            'expected' => '2017-12-15',
            'props'    => {
                'rentInvoiceType' => LOC::Invoice::Rent::INVOICETYPE_INTERMEDIATE,
                'invoiceDate'     => '2017-12-16'
            }
        }
    );

    &LOC::Test::test('Génération de la date de facture',
                     \%tabParameters, \@tabCases,
                     $generateDocumentDate,
                     LOC::Test::TYPE_IS);
}

# Test de calcul de la date de début de la période de facturation
sub getRentalPeriodBeginDateTest
{
    my %tabParameters = (
        'invoiceDate'     => undef,
        'rentInvoiceType' => undef,
    );

    my @tabCases = (
        {
            'label'    => 'Type non défini - Date de facturation non définie',
            'expected' => undef,
        },
        {
            'label'    => 'Type non pris en charge - Date de facturation',
            'expected' => undef,
            'props'    => {
                'invoiceDate'     => '2017-12-04',
                'rentInvoiceType' => 'typeDeTest'
            }
        },
        {
            'label'    => 'Type "mensuelle" - Date de facturation non définie',
            'expected' => undef,
            'props'    => {
                'rentInvoiceType' => LOC::Invoice::Rent::INVOICETYPE_MONTH
            }
        },
        {
            'label'    => 'Type "intermédiaire" - Date de facturation non définie',
            'expected' => undef,
            'props'    => {
                'rentInvoiceType' => LOC::Invoice::Rent::INVOICETYPE_INTERMEDIATE
            }
        },
        {
            'label'    => 'Type non défini - Date de facturation',
            'expected' => undef,
            'props'    => {
                'invoiceDate' => '2017-12-04'
            }
        },
        {
            'label'    => 'Type "mensuelle" - Date de facturation',
            'expected' => '2017-11-01',
            'props'    => {
                'invoiceDate'     => '2017-12-04',
                'rentInvoiceType' => LOC::Invoice::Rent::INVOICETYPE_MONTH
            }
        },
        {
            'label'    => 'Type "intermédiaire" - Date de facturation',
            'expected' => undef,
            'props'    => {
                'invoiceDate'     => '2017-12-16',
                'rentInvoiceType' => LOC::Invoice::Rent::INVOICETYPE_INTERMEDIATE
            }
        }
    );

    &LOC::Test::test('Calcul de la date de début de période de facturation',
                     \%tabParameters, \@tabCases,
                     $getRentalPeriodBeginDate,
                     LOC::Test::TYPE_IS);
}

# Test de calcul de la date de fin de la période de facturation
sub getRentalPeriodEndDateTest
{
    my %tabParameters = (
        'invoiceDate'     => undef,
        'rentInvoiceType' => undef,
    );

    my @tabCases = (
        {
            'label'    => 'Type non défini - Date de facturation non définie',
            'expected' => undef,
        },
        {
            'label'    => 'Type non pris en charge - Date de facturation',
            'expected' => undef,
            'props'    => {
                'rentInvoiceType' => 'typeDeTest',
                'invoiceDate'     => '2017-12-04'
            }
        },
        {
            'label'    => 'Type "mensuelle" - Date de facturation non définie',
            'expected' => undef,
            'props'    => {
                'rentInvoiceType' => LOC::Invoice::Rent::INVOICETYPE_MONTH
            }
        },
        {
            'label'    => 'Type "intermédiaire" - Date de facturation non définie',
            'expected' => undef,
            'props'    => {
                'rentInvoiceType' => LOC::Invoice::Rent::INVOICETYPE_INTERMEDIATE
            }
        },
        {
            'label'    => 'Type non défini - Date de facturation',
            'expected' => undef,
            'props'    => {
                'invoiceDate' => '2017-12-04'
            }
        },
        {
            'label'    => 'Type "mensuelle" - Date de facturation',
            'expected' => '2017-11-30',
            'props'    => {
                'invoiceDate'     => '2017-12-04',
                'rentInvoiceType' => LOC::Invoice::Rent::INVOICETYPE_MONTH
            }
        },
        {
            'label'    => 'Type "intermédiaire" - Date de facturation',
            'expected' => '2017-12-15',
            'props'    => {
                'invoiceDate'     => '2017-12-16',
                'rentInvoiceType' => LOC::Invoice::Rent::INVOICETYPE_INTERMEDIATE
            }
        }
    );

    &LOC::Test::test('Calcul de la date de fin de période de facturation',
                     \%tabParameters, \@tabCases,
                     $getRentalPeriodEndDate,
                     LOC::Test::TYPE_IS);
}

# Test de récupération d'informations sur les dates de facturation pour un contrat
sub getContractInvoiceDatesInfosTest
{
    my %tabParameters = (
        'invoiceType'      => LOC::Invoice::Rent::INVOICETYPE_MONTH,
        'tabContractInfos' => {
            'agency.id'       => 'ROU',
            'beginDate'       => undef,
            'endDate'         => undef,
            'lastInvoiceDate' => undef,
            'invoiceState'    => undef
        },
        'tabParams'        => {
            'endMonthInvoiceDate' => '2017-11-30'
        }
    );

    my @tabCases = (
        {
            'label'    => 'Type "mensuelle" - Contrat sur le mois (n) - jamais facturé (1)',
            'expected' => {
                'beginInvoiceDate' => '2017-11-10',
                'endInvoiceDate'   => '2017-11-20',
                'finishedInMonth'  => 1
            },
            'props'    => {
                'tabContractInfos' => {
                    'beginDate' => '2017-11-10',
                    'endDate'   => '2017-11-20'
                }
            }
        },
        {
            'label'    => 'Type "mensuelle" - Contrat sur le mois (n) - jamais facturé (2)',
            'expected' => {
                'beginInvoiceDate' => '2017-11-10',
                'endInvoiceDate'   => '2017-11-20',
                'finishedInMonth'  => 1
            },
            'props'    => {
                'tabContractInfos' => {
                    'beginDate' => '2017-11-10',
                    'endDate'   => '2017-11-20',
                    'lastInvoiceDate' => '0000-00-00'
                }
            }
        },
        {
            'label'    => 'Type non pris en charge - Contrat sur le mois (n) - jamais facturé',
            'expected' => {
                'beginInvoiceDate' => '2017-11-10',
                'endInvoiceDate'   => '2017-11-20',
                'finishedInMonth'  => 1
            },
            'props'    => {
                'tabContractInfos' => {
                    'beginDate' => '2017-11-10',
                    'endDate'   => '2017-11-20',
                },
                'invoiceType'      => 'typeDeTest',
            }
        },
        {
            'label'    => 'Type "mensuelle" - Contrat sur 2 mois (n-1 / n) - P',
            'expected' => {
                'beginInvoiceDate' => '2017-11-01',
                'endInvoiceDate'   => '2017-11-20',
                'finishedInMonth'  => 1
            },
            'props'    => {
                'tabContractInfos' => {
                    'beginDate' => '2017-10-10',
                    'endDate'   => '2017-11-20',
                    'lastInvoiceDate' => '2017-10-31',
                    'invoiceState'    => LOC::Contract::Rent::INVOICESTATE_PARTIAL
                }
            }
        },
        {
            'label'    => 'Type "mensuelle" - Contrat sur mois dernier (n-1) - PF',
            'expected' => {
                'beginInvoiceDate' => undef,
                'endInvoiceDate'   => undef,
                'finishedInMonth'  => 1
            },
            'props'    => {
                'tabContractInfos' => {
                    'beginDate' => '2017-10-10',
                    'endDate'   => '2017-10-30',
                    'lastInvoiceDate' => '2017-10-31',
                    'invoiceState'    => LOC::Contract::Rent::INVOICESTATE_FINALPARTIAL
                }
            }
        },
        {
            'label'    => 'Type "mensuelle" - Contrat sur mois dernier (n-1) - état facturation non pris en charge',
            'expected' => {
                'beginInvoiceDate' => undef,
                'endInvoiceDate'   => undef,
                'finishedInMonth'  => 1
            },
            'props'    => {
                'tabContractInfos' => {
                    'beginDate' => '2017-10-10',
                    'endDate'   => '2017-10-30',
                    'lastInvoiceDate' => '2017-10-31',
                    'invoiceState'    => 'etatDeTest'
                }
            }
        },
        {
            'label'    => 'Type "mensuelle" - Contrat sur mois dernier (n-1) - F',
            'expected' => {
                'beginInvoiceDate' => undef,
                'endInvoiceDate'   => undef,
                'finishedInMonth'  => 1
            },
            'props'    => {
                'tabContractInfos' => {
                    'beginDate' => '2017-10-10',
                    'endDate'   => '2017-10-30',
                    'lastInvoiceDate' => '2017-10-31',
                    'invoiceState'    => LOC::Contract::Rent::INVOICESTATE_FINAL
                }
            }
        },
        {
            'label'    => 'Type "mensuelle" - Contrat sur 2 mois (n / n+1) - jamais facturé (1)',
            'expected' => {
                'beginInvoiceDate' => '2017-11-10',
                'endInvoiceDate'   => '2017-11-30',
                'finishedInMonth'  => 0
            },
            'props'    => {
                'tabContractInfos' => {
                    'beginDate' => '2017-11-10',
                    'endDate'   => '2017-12-10'
                }
            }
        },
        {
            'label'    => 'Type "mensuelle" - Contrat sur 2 mois (n / n+1) - jamais facturé (2)',
            'expected' => {
                'beginInvoiceDate' => '2017-11-10',
                'endInvoiceDate'   => '2017-11-30',
                'finishedInMonth'  => 0
            },
            'props'    => {
                'tabContractInfos' => {
                    'beginDate'       => '2017-11-10',
                    'endDate'         => '2017-12-10',
                    'lastInvoiceDate' => '0000-00-00'
                }
            }
        },
        {
            'label'    => 'Type "mensuelle" - Contrat sur 2 mois (n / n+1) - P',
            'expected' => {
                'beginInvoiceDate' => '2017-11-01',
                'endInvoiceDate'   => '2017-11-30',
                'finishedInMonth'  => 0
            },
            'props'    => {
                'tabContractInfos' => {
                    'beginDate'       => '2017-10-10',
                    'endDate'         => '2017-12-10',
                    'lastInvoiceDate' => '2017-10-31',
                    'invoiceState'    => LOC::Contract::Rent::INVOICESTATE_PARTIAL
                }
            }
        },
        {
            'label'    => 'Type "intermédiaire" - Contrat sur le mois (n) - jamais facturé (1)',
            'expected' => {
                'beginInvoiceDate' => '2017-11-10',
                'endInvoiceDate'   => '2017-11-20',
                'finishedInMonth'  => 1
            },
            'props'    => {
                'invoiceType'      => LOC::Invoice::Rent::INVOICETYPE_INTERMEDIATE,
                'tabContractInfos' => {
                    'beginDate' => '2017-11-10',
                    'endDate'   => '2017-11-20'
                }
            }
        },
        {
            'label'    => 'Type "intermédiaire" - Contrat sur le mois (n) - jamais facturé (2)',
            'expected' => {
                'beginInvoiceDate' => '2017-11-10',
                'endInvoiceDate'   => '2017-11-20',
                'finishedInMonth'  => 1
            },
            'props'    => {
                'invoiceType'      => LOC::Invoice::Rent::INVOICETYPE_INTERMEDIATE,
                'tabContractInfos' => {
                    'beginDate' => '2017-11-10',
                    'endDate'   => '2017-11-20',
                    'lastInvoiceDate' => '0000-00-00'
                }
            }
        },
        {
            'label'    => 'Type "intermédiaire" - Contrat sur 2 mois (n-1 / n) - P',
            'expected' => {
                'beginInvoiceDate' => '2017-11-01',
                'endInvoiceDate'   => '2017-11-20',
                'finishedInMonth'  => 1
            },
            'props'    => {
                'invoiceType'      => LOC::Invoice::Rent::INVOICETYPE_INTERMEDIATE,
                'tabContractInfos' => {
                    'beginDate' => '2017-10-10',
                    'endDate'   => '2017-11-20',
                    'lastInvoiceDate' => '2017-10-31',
                    'invoiceState'    => LOC::Contract::Rent::INVOICESTATE_PARTIAL
                }
            }
        },
        {
            'label'    => 'Type "intermédiaire" - Contrat sur mois précédent (n-1) - PF',
            'expected' => {
                'beginInvoiceDate' => undef,
                'endInvoiceDate'   => undef,
                'finishedInMonth'  => 1
            },
            'props'    => {
                'invoiceType'      => LOC::Invoice::Rent::INVOICETYPE_INTERMEDIATE,
                'tabContractInfos' => {
                    'beginDate' => '2017-10-10',
                    'endDate'   => '2017-10-30',
                    'lastInvoiceDate' => '2017-10-31',
                    'invoiceState'    => LOC::Contract::Rent::INVOICESTATE_FINALPARTIAL
                }
            }
        }
    );

    &LOC::Test::test('Récupération d\'informations sur les dates de facturation pour un contrat',
                     \%tabParameters, \@tabCases,
                     $getContractInvoiceDatesInfos,
                     LOC::Test::TYPE_DEEP);
}

1;
