#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

use strict;
package Test::LOC::Common::Rent;

# Modules internes
use LOC::Common::Rent;
use LOC::Insurance::Type;
use LOC::Appeal::Type;


# Appel du calcul de l'assurance
my $calculateInsuranceInfos = sub{

    my ($tabInfos) = @_;
    my $tabOptions = {
        'round' => 2
    };

    my %tabInsuranceInfos = &LOC::Common::Rent::calculateInsuranceInfos($tabInfos->{'insuranceTypeFlags'}, $tabInfos->{'insuranceTypeMode'},
                                                                        $tabInfos->{'insuranceRate'}, $tabInfos->{'dailyPrice'},
                                                                        $tabInfos->{'duration'},
                                                                        $tabInfos->{'beginDate'}, $tabInfos->{'endDate'},
                                                                        $tabInfos->{'additionalDays'}, $tabInfos->{'deductedDays'},
                                                                        $tabOptions);
    return $tabInsuranceInfos{'amount'};
};

# Appel du calcul de la gestion de recours
my $calculateAppealInfos = sub{

    my ($tabInfos) = @_;
    my $tabOptions = {
        'round' => 2
    };

    my %tabAppealInfos = &LOC::Common::Rent::calculateAppealInfos($tabInfos->{'appealTypeMode'}, $tabInfos->{'appealRate'},
                                                                  $tabInfos->{'dailyPrice'},
                                                                  $tabInfos->{'duration'}, $tabInfos->{'beginDate'}, $tabInfos->{'endDate'},
                                                                  $tabInfos->{'additionalDays'}, $tabInfos->{'deductedDays'},
                                                                  $tabOptions);
    return $tabAppealInfos{'amount'};
};


# Test du calcul de l'assurance
sub calculateInsuranceTest
{
    my %tabParameters = (
        'insuranceTypeFlags' => 0,
        'insuranceTypeMode'  => LOC::Insurance::Type::MODE_NOTINVOICED,
        'insuranceRate'      => 0.10,
        'dailyPrice'         => 26,
        'duration'           => 0,
        'beginDate'          => '2017-11-06',
        'endDate'            => '2017-11-17',
        'additionalDays'     => 0,
        'deductedDays'       => 0,
    );

    my @tabCases = (
        {
            'label'    => 'Assurance - mode 0',
            'expected' => 0,
        },
        {
            'label'    => 'Assurance facturée - mode 2',
            'expected' => 28.6,
            'props'    => {
                'insuranceTypeFlags' => LOC::Insurance::Type::FLAG_ISINVOICED,
                'insuranceTypeMode'  => LOC::Insurance::Type::MODE_WORKING,
                'duration'           => 11
            }
        },
        {
            'label'    => 'Assurance facturée - mode 1 - J+ : 0 ; J- : 0',
            'expected' => 31.2,
            'props'    => {
                'insuranceTypeFlags' => LOC::Insurance::Type::FLAG_ISINVOICED,
                'insuranceTypeMode'  => LOC::Insurance::Type::MODE_CALENDARADDAYS,
                'additionalDays'     => 0,
                'deductedDays'       => 0
            }
        },
        {
            'label'    => 'Assurance facturée - mode 1 - J+ : 0 ; J- : 0.5 ; rate : 6%',
            'expected' => 17.94,
            'props'    => {
                'insuranceTypeFlags' => LOC::Insurance::Type::FLAG_ISINVOICED,
                'insuranceTypeMode'  => LOC::Insurance::Type::MODE_CALENDARADDAYS,
                'insuranceRate'      => 0.06,
                'additionalDays'     => 0,
                'deductedDays'       => 0.5
            }
        },
        {
            'label'    => 'Assurance facturée - mode 1 - J+ : 0 ; J- : 1',
            'expected' => 28.6,
            'props'    => {
                'insuranceTypeFlags' => LOC::Insurance::Type::FLAG_ISINVOICED,
                'insuranceTypeMode'  => LOC::Insurance::Type::MODE_CALENDARADDAYS,
                'additionalDays'     => 0,
                'deductedDays'       => 1
            }
        },
        {
            'label'    => 'Assurance facturée - mode 1 - J+ : 0.5 ; J- : 0',
            'expected' => 32.5,
            'props'    => {
                'insuranceTypeFlags' => LOC::Insurance::Type::FLAG_ISINVOICED,
                'insuranceTypeMode'  => LOC::Insurance::Type::MODE_CALENDARADDAYS,
                'additionalDays'     => 0.5,
                'deductedDays'       => 0
            }
        },
        {
            'label'    => 'Assurance facturée - mode 1 - J+ : 0.5 ; J- : 1.5',
            'expected' => 28.6,
            'props'    => {
                'insuranceTypeFlags' => LOC::Insurance::Type::FLAG_ISINVOICED,
                'insuranceTypeMode'  => LOC::Insurance::Type::MODE_CALENDARADDAYS,
                'additionalDays'     => 0.5,
                'deductedDays'       => 1.5
            }
        },
        {
            'label'    => 'Assurance facturée - mode 1 - J+ : 0.5 ; J- : 1',
            'expected' => 29.9,
            'props'    => {
                'insuranceTypeFlags' => LOC::Insurance::Type::FLAG_ISINVOICED,
                'insuranceTypeMode'  => LOC::Insurance::Type::MODE_CALENDARADDAYS,
                'additionalDays'     => 0.5,
                'deductedDays'       => 1
            }
        },
        {
            'label'    => 'Assurance facturée - mode 1 - J+ : 1 ; J- : 0',
            'expected' => 33.8,
            'props'    => {
                'insuranceTypeFlags' => LOC::Insurance::Type::FLAG_ISINVOICED,
                'insuranceTypeMode'  => LOC::Insurance::Type::MODE_CALENDARADDAYS,
                'additionalDays'     => 1,
                'deductedDays'       => 0
            }
        },
        {
            'label'    => 'Assurance facturée - mode 1 - J+ : 1 ; J- : 0.5',
            'expected' => 32.5,
            'props'    => {
                'insuranceTypeFlags' => LOC::Insurance::Type::FLAG_ISINVOICED,
                'insuranceTypeMode'  => LOC::Insurance::Type::MODE_CALENDARADDAYS,
                'additionalDays'     => 1,
                'deductedDays'       => 0.5
            }
        },
        {
            'label'    => 'Assurance facturée - mode 1 - J+ : 1 ; J- : 2',
            'expected' => 28.6,
            'props'    => {
                'insuranceTypeFlags' => LOC::Insurance::Type::FLAG_ISINVOICED,
                'insuranceTypeMode'  => LOC::Insurance::Type::MODE_CALENDARADDAYS,
                'additionalDays'     => 1,
                'deductedDays'       => 2
            }
        },
        {
            'label'    => 'Assurance facturée - mode 1 - J+ : 1 ; J- : 0.5 - Dates indéfinies',
            'expected' => 1.3,
            'props'    => {
                'insuranceTypeFlags' => LOC::Insurance::Type::FLAG_ISINVOICED,
                'insuranceTypeMode'  => LOC::Insurance::Type::MODE_CALENDARADDAYS,
                'additionalDays'     => 1,
                'deductedDays'       => 0.5,
                'beginDate'          => undef,
                'endDate'            => undef
            }
        },
        {
            'label'    => 'Assurance facturée - mode 3 - J+ : 0 ; J- : 0',
            'expected' => 31.2,
            'props'    => {
                'insuranceTypeFlags' => LOC::Insurance::Type::FLAG_ISINVOICED,
                'insuranceTypeMode'  => LOC::Insurance::Type::MODE_CALENDAR,
                'additionalDays'     => 0,
                'deductedDays'       => 0
            }
        },
        {
            'label'    => 'Assurance facturée - mode 3 - J+ : 0.5 ; J- : 1',
            'expected' => 31.2,
            'props'    => {
                'insuranceTypeFlags' => LOC::Insurance::Type::FLAG_ISINVOICED,
                'insuranceTypeMode'  => LOC::Insurance::Type::MODE_CALENDAR,
                'additionalDays'     => 0.5,
                'deductedDays'       => 1
            }
        },
        {
            'label'    => 'Assurance facturée - mode 3 - J+ : 1 ; J- : 0.5 - Dates indéfinies',
            'expected' => 0,
            'props'    => {
                'insuranceTypeFlags' => LOC::Insurance::Type::FLAG_ISINVOICED,
                'insuranceTypeMode'  => LOC::Insurance::Type::MODE_CALENDAR,
                'additionalDays'     => 1,
                'deductedDays'       => 2,
                'beginDate'          => undef,
                'endDate'            => undef
            }
        }
    );

    &LOC::Test::test('Calcul de l\'assurance',
                     \%tabParameters, \@tabCases,
                     $calculateInsuranceInfos,
                     LOC::Test::TYPE_IS);
}


# Test du calcul de la gestion de recours
sub calculateAppealTest
{
    my %tabParameters = (
        'appealTypeMode' => LOC::Appeal::Type::MODE_NOTINVOICED,
        'appealRate'     => 0.03,
        'dailyPrice'     => 26,
        'duration'       => 0,
        'beginDate'      => '2017-11-06',
        'endDate'        => '2017-11-17',
        'additionalDays' => 0,
        'deductedDays'   => 0,
    );

    my @tabCases = (
        {
            'label'    => 'Gestion de recours - mode 0',
            'expected' => 0,
        },
        {
            'label'    => 'Gestion de recours facturée - mode 1 - J+ : 0 ; J- : 0',
            'expected' => 9.36,
            'props'    => {
                'appealTypeMode'  => LOC::Appeal::Type::MODE_CALENDAR,
                'additionalDays'  => 0,
                'deductedDays'    => 0
            }
        },
        {
            'label'    => 'Gestion de recours facturée - mode 1 - J+ : 0.5 ; J- : 1',
            'expected' => 9.36,
            'props'    => {
                'appealTypeMode'  => LOC::Appeal::Type::MODE_CALENDAR,
                'additionalDays'  => 0.5,
                'deductedDays'    => 1
            }
        },
        {
            'label'    => 'Gestion de recours facturée - mode 1 - J+ : 0 ; J- : 0 - dates indéfinies',
            'expected' => 0,
            'props'    => {
                'appealTypeMode'  => LOC::Appeal::Type::MODE_CALENDAR,
                'additionalDays'  => 0,
                'deductedDays'    => 0,
                'beginDate'       => undef,
                'endDate'         => undef
            }
        }
    );

    &LOC::Test::test('Calcul de la gestion de recours',
                     \%tabParameters, \@tabCases,
                     $calculateAppealInfos,
                     LOC::Test::TYPE_IS);
}


1;
