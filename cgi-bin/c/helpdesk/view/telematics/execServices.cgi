use utf8;
use open (':encoding(UTF-8)');

use strict;
use lib 'inc', 'lib';

use LOC::Html::Standard;
use LOC::Locale;
use LOC::Util;

our %tabViewData;


my $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
my $view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

$view->setPageTitle('Support technique');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle('Support technique > Télématique > Accès aux services');

# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc('helpdesk/telematics/execServices.js');
# CSS
$view->addCSSSrc('helpdesk/base.css');
$view->addCSSSrc('location.css');

# Bouton "Retour"
my $viewUrl = $view->createURL('helpdesk:index:view');
my $btn     = $view->displayTextButton('Retour', 'revert(|Over).png', $viewUrl,
                                   'Retourner au support',
                                   'l',
                                   {'tabindex' => -1,
                                    'onfocus' => 'this.blur();'
                                   });

$view->addControlsContent($btn, 'left', 1);

my $actualizeBtn = $view->createURL('helpdesk:telematics:execServices');
my $btn     = $view->displayTextButton('Actualiser', 'refresh(|Over).png', $actualizeBtn,
                                   'Actualiser',
                                   'l',
                                   {'tabindex' => -1,
                                    'onfocus' => 'this.blur();'
                                   });
$view->addControlsContent($btn, 'right', 1);

# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();

# Formulaire des entrées
my @listCountry;
foreach my $countryId (keys(%{$tabViewData{'countryList'}}))
{
    push(@listCountry, {'value' => $countryId, 'innerHTML' => $tabViewData{'countryList'}->{$countryId}});
}
my $countrySelect = $view->displayFormSelect('countryId', \@listCountry, $tabViewData{'countryId'}, 0, {'onchange' => 'submitForm("country")'});

my $tabServices = [
    {
        'id'    => 'machinesExternalIds',
        'label' => 'Mise à jour des IDTarget des machines'
    },
    {
        'id'    => 'machinesImmobilizations',
        'label' => 'Mise à jour de l\'immobilisation des machines'
    },
    {
        'id'    => 'startCodeRulesCheckTransmissions',
        'label' => 'Récupération des états de transmission'
    },
    {
        'id'    => 'startCodeRulesProcessQueue',
        'label' => 'Traitement de la file d\'attente'
    }
];

my @radiosGroup = ();
my $defaultServiceId = ($tabViewData{'serviceId'} ? $tabViewData{'serviceId'} : $tabServices->[0]->{'id'});
foreach my $service (@$tabServices)
{
    push(@radiosGroup, $view->displayFormInputRadio('serviceId', $service->{'id'},
                                                                 $defaultServiceId eq $service->{'id'},
                                                                 {},
                                                                 $service->{'label'}));
}

print '
<fieldset class="form">
    <legend>Données</legend>
    <table class="formTable">
        <tr>
            <td class="label">Pays</td>
            <td>' . $countrySelect . '</td>
        </tr>
        <tr>
            <td class="label align-top">Service</td>
            <td>' . join('<br />', @radiosGroup) . '</td>
        </tr>
    </table>
</fieldset>
';

# Boutons de vérification/validation
my $validButton = $view->displayTextButton(
                                            'Exécuter',
                                            'valid(|Over).png',
                                            'submitForm()',
                                            'Exécuter le service',
                                            'l',
                                            {'id'    => 'supportValidBtn',
                                             'class' => 'locCtrlButton',
                                             'tabindex' => -1,
                                             'onfocus' => 'this.blur();'
                                            }
                                           );

print $view->displayControlPanel({'left' => $validButton, 'substyle' => 'bottom'});

if ($tabViewData{'serviceId'})
{
    my $urlIframe = &LOC::Globals::get('servicesUrl') . '/exec/cron/telematics/' . $tabViewData{'serviceId'} . '.php?output=1&country=' . $tabViewData{'countryId'};
    my $jsBlock = '$(window).load(function() {$("#execService").attr("src", "' . $urlIframe . '");});';
    print $view->displayJSBlock($jsBlock);
    print '
    <fieldset class="scripts">
        <legend>Appel au service</legend>';
    print '<iframe id="execService" src="" width="100%" height="200px" frameborder="0"></iframe>';
    print '
    </fieldset>';
}


# Affichage du pied de page
print $view->endForm();
print $view->displayFooter();