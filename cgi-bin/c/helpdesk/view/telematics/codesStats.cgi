use utf8;
use open (':encoding(UTF-8)');

use strict;
use lib 'inc', 'lib';

use LOC::Html::Standard;
use LOC::Locale;
use LOC::Util;

our %tabViewData;


my $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
my $view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

$view->setPageTitle('Support technique');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle('Support technique > Télématique > Statistiques sur les codes');

# jQuery
$view->addJSSrc('jQuery/jquery.js');
# CSS
$view->addCSSSrc('helpdesk/base.css');
$view->addCSSSrc('location.css');

# Bouton "Retour"
my $viewUrl = $view->createURL('helpdesk:index:view');
my $btn     = $view->displayTextButton('Retour', 'revert(|Over).png', $viewUrl,
                                   'Retourner au support',
                                   'l',
                                   {'tabindex' => -1,
                                    'onfocus' => 'this.blur();'
                                   });

$view->addControlsContent($btn, 'left', 1);

my $actualizeBtn = $view->createURL('helpdesk:telematics:codesStats');
my $btn     = $view->displayTextButton('Actualiser', 'refresh(|Over).png', $actualizeBtn,
                                   'Actualiser',
                                   'l',
                                   {'tabindex' => -1,
                                    'onfocus' => 'this.blur();'
                                   });
$view->addControlsContent($btn, 'right', 1);

# Affichage de l'entête
print $view->displayHeader();



# Codes statiques pouvant être générés
my $tabStatic = $tabViewData{'static'};

my $nbDisplayed = 10;
if ($nbDisplayed > @{$tabStatic->{'unavailableList'}})
{
    $nbDisplayed = @{$tabStatic->{'unavailableList'}};
}
my @tabTopCodes = @{$tabStatic->{'unavailableList'}}[0 .. $nbDisplayed - 1];
my @tabLastCodes = @{$tabStatic->{'unavailableList'}}[$nbDisplayed .. @{$tabStatic->{'unavailableList'}} - 1];

print '
<fieldset class="form">
    <legend>Codes statiques pouvant être générés</legend>
    <p>' . $locale->tn('Il reste %s code statique pouvant être généré',
                       'Il reste %s codes statiques pouvant être générés',
                       $tabStatic->{'nbAvailable'},
                       '<strong>' . $tabStatic->{'nbAvailable'} . '</strong>') . '.</p>
    <p>'. $locale->tn('Il y a %s code non disponible',
                      'Il y a %s codes non disponibles',
                      $tabStatic->{'nbUnavailable'},
                      '<strong>' . $tabStatic->{'nbUnavailable'} . '</strong>') . ':
        <div>
            ' . join(' ', map { '<span class="startcode">' . $_ . '</span>' } @tabTopCodes);
if (@tabLastCodes > 0)
{
    my $nbLastCodes = @tabLastCodes;
    print '<a href="#" onclick="$(\'#lastCodes\').show(); $(this).hide();">' . $locale->tn('Voir le dernier', 'Voir les %s autres', $nbLastCodes, $nbLastCodes) . '</a>
            <span id="lastCodes" style="display: none;">' . join(' ', map { '<span class="startcode">' . $_ . '</span>' } @tabLastCodes) . '</span>';
}
print '
        </div>
    </p>
</fieldset>';


# Machines ayant le plus de codes non disponibles
my $tabWorstMachines = $tabViewData{'worstMachines'};

my $nbDisplayed = 10;
if ($nbDisplayed > @{$tabWorstMachines->{'list'}})
{
    $nbDisplayed = @{$tabWorstMachines->{'list'}};
}
my @tabTopMachines = @{$tabWorstMachines->{'list'}}[0 .. $nbDisplayed - 1];
my @tabLastMachines = @{$tabWorstMachines->{'list'}}[$nbDisplayed .. @{$tabWorstMachines->{'list'}} - 1];

print '
<fieldset class="form">
    <legend>Machines ayant le plus de codes non disponibles</legend>
    <p>' . $locale->tn('%s codes non disponibles sur les machines suivantes',
                       '%s codes non disponibles sur les machines suivantes',
                       $tabWorstMachines->{'count'},
                       '<strong>' . $tabWorstMachines->{'count'} . '</strong>') . ':
         <div>
             ' . join(' ', map { $view->displayParkNumber($_) } @tabTopMachines);
 if (@tabLastMachines > 0)
 {
     my $nbLastMachines = @tabLastMachines;
     print '<a href="#" onclick="$(\'#lastMachines\').show(); $(this).hide();">' . $locale->tn('Voir la dernière', 'Voir les %s autres', $nbLastMachines, $nbLastMachines) . '</a>
             <span id="lastMachines" style="display: none;">' . join(' ', map { $view->displayParkNumber($_) } @tabLastMachines) . '</span>';
 }
 print '
         </div>
    </p>
</fieldset>';


# Affichage du pied de page
print $view->displayFooter();