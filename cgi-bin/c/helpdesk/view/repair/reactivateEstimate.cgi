use utf8;
use open (':encoding(UTF-8)');

use strict;
use lib 'inc', 'lib';

use LOC::Html::Standard;
use LOC::Locale;

our %tabViewData;


my $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
my $view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

$view->setPageTitle('Support technique');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle('Support technique > Remise en état > Réactivation d\'un devis');

# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc('helpdesk/repair/reactivateEstimate.js');
# CSS
$view->addCSSSrc('helpdesk/base.css');
$view->addCSSSrc('location.css');
$view->addCSSSrc('helpdesk/repair/reactivateEstimate.css');


# Bouton "Retour"
my $viewUrl = $view->createURL('helpdesk:index:view');
my $btn     = $view->displayTextButton('Retour', 'revert(|Over).png', $viewUrl,
                                   'Retourner au support',
                                   'l',
                                   {'tabindex' => -1,
                                    'onfocus' => 'this.blur();'
                                   });

$view->addControlsContent($btn, 'left', 1);

my $actualizeBtn = $view->createURL('helpdesk:repair:reactivateEstimate');
my $btn     = $view->displayTextButton('Actualiser', 'refresh(|Over).png', $actualizeBtn,
                                   'Actualiser',
                                   'l',
                                   {'tabindex' => -1,
                                    'onfocus' => 'this.blur();'
                                   });
$view->addControlsContent($btn, 'right', 1);

# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();


# Erreurs possibles
my $tabTranslations = {
    'errorNumEstimate'      => 'Il n\'y a pas de devis à ce numéro',
    'estimateToActive'      => 'Modifications effectuées (état en attente)',
    'errorNoChange'         => 'Il y a eu une erreur lors des modifications sur le devis',
    'errorIdEstimateChange' => 'Le numéro de devis n\'est plus le même',
    'errorCountryChange'    => 'Le pays n\'est plus le même',
    'noPossible'            => 'Il n\'est pas possible de modifier ce devis'
};

#Formulaire des entrées
my $countrySelect = $view->displayFormSelect('countryId', $tabViewData{'countryList'}, $tabViewData{'country'});

my $repairEstimateInput = $view->displayFormInputText('repairEstimateId',
                                               ($tabViewData{'id'} == 0 ? '' : $tabViewData{'id'}),
                                               '',
                                               {
                                                    'size' => '20', 
                                                    'maxlength' => '20', 
                                                    'autocomplete' => 'off'
                                               }
                                               );


print '
<fieldset class="form">
    <legend>Données</legend>
    <table class="formTable">
        <tr>
            <td class="label">Pays</td>
            <td>' . $countrySelect . '</td>
        </tr>
        <tr>
            <td class="label">Numéro du devis de remise en état</td>
            <td>' . $repairEstimateInput . '</td>
        </tr>
    </table>
</fieldset>';

if (defined $tabViewData{'repairEstimateInfo'})
{
    print '
    <div id="checkRepairEstimateInvocingBlock">
        <fieldset class="info">
            <legend>Vérification des données</legend>
            <table class="formTable">
                <tr>
                    <td class="label">Id du devis</td>
                    <td>' . $tabViewData{'repairEstimateInfo'}->{'id'} . '</td>
                </tr>
                <tr>
                    <td class="label">Code du devis</td>
                    <td><a target="_blank" href="' . $view->createURL('repair:estimate:edit', {'id' => $tabViewData{'repairEstimateInfo'}->{'id'}}) . '">' . $tabViewData{'repairEstimateInfo'}->{'code'} . '</a></td>
                </tr>
                <tr>
                    <td class="label">Etat</td>
                    <td>' . $tabViewData{'repairEstimateInfo'}->{'state.id'} . '</td>
                    <td>' . $tabViewData{'repairEstimateInfo'}->{'state.label'} . '</td>
                </tr>
                <tr>
                    <td class="label">Bascule</td>
                    <td>' . $tabViewData{'repairEstimateInfo'}->{'turnInto'} . '</td>
                </tr>
                <tr>
                    <td class="label">Possiblilité de changer l\'état du devis</td>';
    if($tabViewData{'repairEstimateInfo'}->{'isReactivable'})
    {
        print '<td>Oui</td>';
    }
    else
    {
        print '<td>Non</td>';
    }
    print'
                </tr>
                
                <tr>
                    <td class="label">Id de la fiche</td>
                    <td><a target="_blank" href="' . $view->createURL('repair:sheet:edit', {'sheetId' => $tabViewData{'repairSheetInfo'}->{'id'}}) . '">' . $tabViewData{'repairSheetInfo'}->{'id'} . '</a></td>
                </tr>
                <tr>
                    <td class="label">Etat</td>
                    <td>' . $tabViewData{'repairSheetInfo'}->{'state.id'} . '</td>
                    <td>' . $tabViewData{'repairSheetInfo'}->{'state.label'} . '</td>
                </tr>
                <tr>
                    <td class="label">Bascule</td>
                    <td>' . $tabViewData{'repairSheetInfo'}->{'turnInto'} . '</td>
                </tr>
            </table>
        </fieldset>
    </div>
    ';
}

if ($tabViewData{'repairEstimateInfo'}->{'isReactivable'})
{
    print '<fieldset class="info">
            <legend>Les modifications suivantes vont être effectuées sur le devis ' . $tabViewData{'repairEstimateInfo'}->{'code'} . '</legend>
            <table class="formTable">
                <tr>
                    <td class="label">Modification de l\'état de </td>
                    <td>CAS04 => CAS01</td>
                </tr>
                <tr>
                    <td class="label">Modification de l\'état de la fiche</td>
                    <td>' . $tabViewData{'repairSheetInfo'}->{'state.id'} . ' => CAS03</td>
                </tr>
                <tr>
                    <td class="label">Modification du flag de la fiche</td>
                    <td>' . $tabViewData{'repairSheetInfo'}->{'flag'} . ' => 0</td>
                </tr>
            </table>
            </fieldset>';
}

if (defined $tabViewData{'repairEstimateNewInfo'})
{
    print '
    <div id="checkRepairEstimateInvocingBlock">
        <fieldset class="info">
            <legend>Modification effectuées sur le devis ' . $tabViewData{'repairEstimateNewInfo'}->{'code'} . '</legend>
            <table class="formTable">
                <tr>
                    <td class="label">Id du devis</td>
                    <td>' . $tabViewData{'repairEstimateNewInfo'}->{'id'} . '</td>
                </tr>
                <tr>
                    <td class="label">Code du devis</td>
                    <td><a target="_blank" href="' . $view->createURL('repair:estimate:edit', {'sheetId' => $tabViewData{'repairEstimateNewInfo'}->{'id'}}) . '">' . $tabViewData{'repairEstimateNewInfo'}->{'id'} . '</a></td>
                </tr>
                <tr>
                    <td class="label">Etat</td>
                    <td>' . $tabViewData{'repairEstimateNewInfo'}->{'state.id'} . '</td>
                    <td>' . $tabViewData{'repairEstimateNewInfo'}->{'state.label'} . '</td>
                </tr>
                <tr>
                    <td class="label">Bascule</td>
                    <td>' . $tabViewData{'repairEstimateNewInfo'}->{'turnInto'} . '</td>
                </tr>
                <tr>
                    <td class="label">Possiblilité de changer l\'état du devis</td>';
    if($tabViewData{'repairEstimateNewInfo'}->{'isReactivable'})
    {
        print '     <td>Oui</td>';
    }
    else
    {
        print '     <td>Non</td>';
    }
    print'
                </tr>
                <tr>
                    <td class="label">Id de la fiche</td>
                    <td><a target="_blank" href="' . $view->createURL('repair:sheet:edit', {'id' => $tabViewData{'repairSheetNewInfo'}->{'id'}}) . '">' . $tabViewData{'repairSheetNewInfo'}->{'code'} . '</a></td>
                </tr>
                <tr>
                    <td class="label">Etat</td>
                    <td>' . $tabViewData{'repairSheetNewInfo'}->{'state.id'} . '</td>
                    <td>' . $tabViewData{'repairSheetNewInfo'}->{'state.label'} . '</td>
                </tr>
                <tr>
                    <td class="label">Bascule</td>
                    <td>' . $tabViewData{'repairSheetNewInfo'}->{'turnInto'} . '</td>
                </tr>
            </table>
        </fieldset>
    </div>
    ';
    
    my $urlIframe =&LOC::Globals::get('locationUrl') . '/cgi-bin/old/location/majAlertesStocks.cgi?countryId=' . $tabViewData{'country'} . '&agencyId=' . $tabViewData{'repairEstimateNewInfo'}->{'agency'};
    my $jsBlock = '$(window).load(function() {$("#majStocksAlerts").attr("src", "' . $urlIframe . '");});';
    print $view->displayJSBlock($jsBlock);
    print '
    <fieldset class="scripts">
        <legend>Mise à jour des alertes et des stocks</legend>';
    print '<iframe id="majStocksAlerts" src="" width="100%" height="120px" frameborder="0"></iframe>';
    print '
    </fieldset>';
}

# Boutons de vérification/validation
my $verifButton = $view->displayTextButton(
                                            'Vérifier',
                                            'valid(|Over).png',
                                            'submitForm()',
                                            'Vérifier les données',
                                            'l',
                                            {'id'    => 'supportVerifBtn',
                                             'class' => 'locCtrlButton',
                                             'tabindex' => -1,
                                             'onfocus' => 'this.blur();'
                                            }
                                           );
                                           
                                           # Boutons de vérification/validation
my $validButton = $view->displayTextButton(
                                            'Modifier le devis',
                                            'valid(|Over).png',
                                            'submitForm()',
                                            'Modifier le devis',
                                            'l',
                                            {'id'    => 'supportValidBtn',
                                             'class' => 'locCtrlButton',
                                             'tabindex' => -1,
                                             'onfocus' => 'this.blur();'
                                            }
                                           );

if ($tabViewData{'check'} eq 'noError')
{
    print $view->displayControlPanel({'left' => $validButton, 'substyle' => 'bottom'});
}
elsif ((!$tabViewData{'repairEstimateInfo'}->{'isReactivable'} || !$tabViewData{'repairEstimateNewInfo'}->{'isReactivable'}) && $tabViewData{'check'} ne '')
{
    print $view->displayControlPanel({'left' => $btn, 'substyle' => 'bottom'});
}
elsif($tabViewData{'repairEstimateInfo'}->{'isReactivable'} || $tabViewData{'check'} eq '')
{
    print $view->displayControlPanel({'left' => $verifButton, 'substyle' => 'bottom'});
}

my @tabCode =  @{$tabViewData{'codeReturn'}};
if (&LOC::Util::in_array('errorNumEstimate', \@{$tabViewData{'codeReturn'}}))
{
    print $view->displayMessages('error', [$tabTranslations->{'errorNumEstimate'}], 0);
}
elsif (&LOC::Util::in_array('estimateToActive', \@{$tabViewData{'codeReturn'}}))
{
    print $view->displayMessages('valid', [$tabTranslations->{'estimateToActive'}], 0);
}
elsif (&LOC::Util::in_array('errorNoChange', \@{$tabViewData{'codeReturn'}}))
{
    print $view->displayMessages('error', [$tabTranslations->{'errorNoChange'}], 0);
}
elsif (&LOC::Util::in_array('errorIdEstimateChange', \@{$tabViewData{'codeReturn'}}))
{
    print $view->displayMessages('error', [$tabTranslations->{'errorIdEstimateChange'}], 0);
}
elsif (&LOC::Util::in_array('noPossible', \@{$tabViewData{'codeReturn'}}))
{
    print $view->displayMessages('error', [$tabTranslations->{'noPossible'}], 0);
}


print $view->displayFormInputHidden('check', $tabViewData{'check'});
print $view->displayFormInputHidden('oldId', ($tabViewData{'id'} == 0 ? '' : $tabViewData{'id'}));
print $view->displayFormInputHidden('oldCountry', $tabViewData{'country'});


# Affichage du pied de page
print $view->endForm();
print $view->displayFooter();


