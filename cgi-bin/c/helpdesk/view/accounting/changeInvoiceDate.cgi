use utf8;
use open (':encoding(UTF-8)');

use strict;
use lib 'inc', 'lib';

use LOC::Html::Standard;
use LOC::Locale;
use LOC::Util;

our %tabViewData;


my $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
my $view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

$view->setPageTitle('Support technique');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle('Support technique > Comptabilité > Suppression du code client');

# jQuery
#$view->addJSSrc('jQuery/jquery.js');
#$view->addJSSrc('helpdesk/accounting/changeInvoiceDate.js');
# CSS
$view->addCSSSrc('helpdesk/base.css');
$view->addCSSSrc('location.css');
$view->addCSSSrc('helpdesk/accounting/changeInvoiceDate.css');

# Bouton "Retour"
my $viewUrl = $view->createURL('helpdesk:index:view');
my $btn     = $view->displayTextButton('Retour', 'revert(|Over).png', $viewUrl,
                                   'Retourner au support',
                                   'l',
                                   {'tabindex' => -1,
                                    'onfocus' => 'this.blur();'
                                   });

$view->addControlsContent($btn, 'left', 1);

my $actualizeBtn = $view->createURL('helpdesk:accounting:changeInvoiceDate');
my $btn     = $view->displayTextButton('Actualiser', 'refresh(|Over).png', $actualizeBtn,
                                   'Actualiser',
                                   'l',
                                   {'tabindex' => -1,
                                    'onfocus' => 'this.blur();'
                                   });
$view->addControlsContent($btn, 'right', 1);

# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();

# Erreurs possibles
my $tabTranslations = {
    'errorNumEstimate'   => 'Il n\'y a pas de client correspondant à ce code',
    'removed'            => 'Modifications effectuées (CLCODE = null, CLVERIFIER = -1, CLNONVALIDE = null)',
    'errorNoChange'      => 'Il y a eu une erreur lors des modifications du client',
    'errorCodeChange'    => 'Le code client n\'est plus le même',
    'errorCountryChange' => 'Le pays n\'est plus le même',
    'noPossible'         => 'Il n\'est pas possible de modifier ce client'
};

#Formulaire des entrées
my @listCountry = ({'value' => 0, 'innerHTML' => '-- ' . $locale->t('Sélectionner') . ' --'});
foreach my $countryId (keys(%{$tabViewData{'countryList'}}))
{
    push(@listCountry, {'value' => $countryId, 'innerHTML' => $tabViewData{'countryList'}->{$countryId}});
}
my $countrySelect = $view->displayFormSelect('countryId', \@listCountry, $tabViewData{'countryId'}, 0, {'onchange' => 'submitForm("country")'});

print '
<fieldset class="form">
    <legend>Données</legend>
    <table class="formTable">
        <tr>
            <td class="label">Pays</td>
            <td>' . $countrySelect . '</td>
        </tr>
    </table>
';

if ($tabViewData{'check'} eq 'displayDate')
{
    print '
    <table class="standard">
        <thead>
            <tr>
                <th class="label">Date de facturation</th>
                <th class="label">Type de facturation</th>
                <th>Modifier cette date</th>
            </tr>
        </thead>
        <tbody>';
    foreach my $tabInfo (@{$tabViewData{'tabInvoiceDate'}})
    {
        my $changeDateInput = $view->displayFormInputRadio('toDateChange', $tabInfo->{'id'});
        my $newInvoiceDateInput = $view->displayFormInputDate('newInvoiceDate_' . $tabInfo->{'id'} .'',
                                                '',
                                                '',
                                                '',
                                                {
                                                    'size' => '20', 
                                                    'maxlength' => '20', 
                                                    'autocomplete' => 'off'
                                                }
                                                );
        print '
            <tr>
                <td>' . $tabInfo->{'date'} . '</td>
                <td>' . $tabInfo->{'type'} . '</td>
                <td class="action">' . $changeDateInput . '<span id="divDateChange[' . $tabInfo->{'id'} .']" class="">'. $newInvoiceDateInput . '</span></td>
            </tr>';
    }
    print '
        </tbody>
    </table>
</fieldset>';
}

print $view->displayFormInputHidden('check', $tabViewData{'check'});
# Affichage du pied de page
print $view->endForm();
print $view->displayFooter();