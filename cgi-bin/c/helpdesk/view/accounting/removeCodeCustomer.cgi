use utf8;
use open (':encoding(UTF-8)');

use strict;
use lib 'inc', 'lib';

use LOC::Html::Standard;
use LOC::Locale;
use LOC::Util;

our %tabViewData;


my $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
my $view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

$view->setPageTitle('Support technique');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle('Support technique > Comptabilité > Suppression de la vérification client');

# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc('helpdesk/accounting/removeCodeCustomer.js');
# CSS
$view->addCSSSrc('helpdesk/base.css');
$view->addCSSSrc('location.css');
$view->addCSSSrc('helpdesk/accounting/removeCodeCustomer.css');


# Bouton "Retour"
my $viewUrl = $view->createURL('helpdesk:index:view');
my $btn     = $view->displayTextButton('Retour', 'revert(|Over).png', $viewUrl,
                                   'Retourner au support',
                                   'l',
                                   {'tabindex' => -1,
                                    'onfocus' => 'this.blur();'
                                   });

$view->addControlsContent($btn, 'left', 1);

my $actualizeBtn = $view->createURL('helpdesk:accounting:removeCodeCustomer');
my $btn     = $view->displayTextButton('Actualiser', 'refresh(|Over).png', $actualizeBtn,
                                   'Actualiser',
                                   'l',
                                   {'tabindex' => -1,
                                    'onfocus' => 'this.blur();'
                                   });
$view->addControlsContent($btn, 'right', 1);

# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();

# Erreurs possibles
my $tabTranslations = {
    'errorNumEstimate'   => 'Il n\'y a pas de client correspondant à ce code',
    'removed'            => 'Modifications effectuées (CLCODE = null, CLVERIFIER = -1, CLNONVALIDE = null)',
    'errorNoChange'      => 'Il y a eu une erreur lors des modifications du client',
    'errorCodeChange'    => 'Le code client n\'est plus le même',
    'errorCountryChange' => 'Le pays n\'est plus le même',
    'noPossible'         => 'Il n\'est pas possible de modifier ce client'
};

#Formulaire des entrées
my $countrySelect = $view->displayFormSelect('countryId', $tabViewData{'countryList'}, ['FR']);

my $customerCodeInput = $view->displayFormInputText('customerCode',
                                                $tabViewData{'customerCode'},
                                                '',
                                                {
                                                    'size' => '20', 
                                                    'maxlength' => '20', 
                                                    'autocomplete' => 'off'
                                                }
                                                );


print '
<fieldset class="form">
    <legend>Données</legend>
    <table class="formTable">
        <tr>
            <td class="label">Pays</td>
            <td>' . $countrySelect . '</td>
        </tr>
        <tr>
            <td class="label">Code du client</td>
            <td>' . $customerCodeInput . '</td>
        </tr>
    </table>
</fieldset>';

if (defined $tabViewData{'customerInfo'})
{
    print '
    <fieldset class="info">
        <legend>Vérification des données</legend>
        <table class="formTable">
            <tr>
                <td class="label">Id du client</td>
                <td>' . $tabViewData{'customerInfo'}->{'id'} . '</td>
            </tr>
            <tr>
                <td class="label">Code du client</td>
                <td>' . $tabViewData{'customerInfo'}->{'code'} . '</td>
            </tr>
            <tr>
                <td class="label">Raison sociale</td>
                <td>' . $tabViewData{'customerInfo'}->{'name'} . '</td>
            </tr>
            <tr>
                <td class="label">Vérifier</td>
                <td>' . $tabViewData{'customerInfo'}->{'verificationStatus'} . '</td>
            </tr>
            <tr>
                <td class="label">Non valide</td>
                <td>' . $tabViewData{'customerInfo'}->{'validityStatus'} . '</td>
            </tr>
            <tr>
                <td class="label">Collectif</td>
                <td>' . $tabViewData{'customerInfo'}->{'accountingCode'} . '</td>
            </tr>
            <tr>
                <td class="label">Nombre de contrats</td>
                <td>' . $tabViewData{'customerInfo'}->{'countContract'} . '</td>
            </tr>
            <tr>
                <td class="label">Possiblilité de modifier client</td>';
    if($tabViewData{'customerInfo'}->{'isRemoveCode'})
    {
        print '<td>Oui</td>';
    }
    else
    {
        print '<td>Non</td>';
    }
    print'
            </tr>
        </table>
    </fieldset>
    ';
}

if ($tabViewData{'customerInfo'}->{'isRemoveCode'})
{
    print '<fieldset class="info">
            <legend>Les modifications suivantes vont être effectuées sur le client </legend>
            <table class="formTable">
                <tr>
                    <td class="label">Suppression du code client</td>
                    <td>' . $tabViewData{'customerInfo'}->{'code'} . ' => null</td>
                </tr>
                <tr>
                    <td class="label">Mise à jour de CLVERIFIER</td>
                    <td>' . $tabViewData{'customerInfo'}->{'verificationStatus'} . ' => -1</td>
                </tr>
                <tr>
                    <td class="label">Mise à jour de CLNONVALIDE</td>
                    <td>' . $tabViewData{'customerInfo'}->{'validityStatus'} . ' => null</td>
                </tr>
                <tr>
                    <td class="label">Mise à jour de CLCOLLECTIF</td>
                    <td>' . $tabViewData{'customerInfo'}->{'accountingCode'} . ' => \'\'</td>
                </tr>
            </table>
            </fieldset>';
}

if (defined $tabViewData{'customerNewInfo'})
{
    print '
    <fieldset class="info">
        <legend>Vérification des données</legend>
        <table class="formTable">
            <tr>
                <td class="label">Id du client</td>
                <td>' . $tabViewData{'customerNewInfo'}->{'id'} . '</td>
            </tr>
            <tr>
                <td class="label">Code du client</td>
                <td>' . $tabViewData{'customerNewInfo'}->{'code'} . '</td>
            </tr>
            <tr>
                <td class="label">Raison sociale</td>
                <td>' . $tabViewData{'customerNewInfo'}->{'name'} . '</td>
            </tr>
            <tr>
                <td class="label">Vérifier</td>
                <td>' . $tabViewData{'customerNewInfo'}->{'verificationStatus'} . '</td>
            </tr>
            <tr>
                <td class="label">Non valide</td>
                <td>' . $tabViewData{'customerNewInfo'}->{'validityStatus'} . '</td>
            </tr>
            <tr>
                <td class="label">Collectif</td>
                <td>' . $tabViewData{'customerNewInfo'}->{'accountingCode'} . '</td>
            </tr>
            <tr>
                <td class="label">Nombre de contrats</td>
                <td>' . $tabViewData{'customerNewInfo'}->{'countContract'} . '</td>
            </tr>
            <tr>
                <td class="label">Possiblilité de supprimer le code client</td>';
    if($tabViewData{'customerNewInfo'}->{'isRemoveCode'})
    {
        print '<td>Oui</td>';
    }
    else
    {
        print '<td>Non</td>';
    }
    print'
            </tr>
        </table>
    </fieldset>
    ';
    
    my $urlIframe =&LOC::Globals::get('locationUrl') . '/cgi-bin/old/location/majAlertesStocks.cgi?countryId=' . $tabViewData{'country'} . '&agencyId=ALL';
    my $jsBlock = '$(window).load(function() {$("#majStocksAlerts").attr("src", "' . $urlIframe . '");});';
    print $view->displayJSBlock($jsBlock);
    print '
    <fieldset class="scripts">
        <legend>Mise à jour des alertes et des stocks</legend>';
    print '<iframe id="majStocksAlerts" src="" width="100%" height="120px" frameborder="0"></iframe>';
    print '
    </fieldset>';
    
}


# Boutons de vérification/validation
my $verifButton = $view->displayTextButton(
                                            'Vérifier',
                                            'valid(|Over).png',
                                            'submitForm()',
                                            'Vérifier les données',
                                            'l',
                                            {'id'    => 'supportVerifBtn',
                                             'class' => 'locCtrlButton',
                                             'tabindex' => -1,
                                             'onfocus' => 'this.blur();'
                                            }
                                           );
                                           
                                           # Boutons de vérification/validation
my $validButton = $view->displayTextButton(
                                            'Valider',
                                            'valid(|Over).png',
                                            'submitForm()',
                                            'Modifier le devis',
                                            'l',
                                            {'id'    => 'supportValidBtn',
                                             'class' => 'locCtrlButton',
                                             'tabindex' => -1,
                                             'onfocus' => 'this.blur();'
                                            }
                                           );

if ($tabViewData{'check'} eq 'noError')
{
    print $view->displayControlPanel({'left' => $validButton, 'substyle' => 'bottom'});
}
elsif ((!$tabViewData{'customerInfo'}->{'isRemoveCode'} || !$tabViewData{'customerNewInfo'}->{'isRemoveCode'}) && $tabViewData{'check'} ne '')
{
    print $view->displayControlPanel({'left' => $btn, 'substyle' => 'bottom'});
}
elsif($tabViewData{'customerInfo'}->{'isRemoveCode'} || $tabViewData{'check'} eq '')
{
    print $view->displayControlPanel({'left' => $verifButton, 'substyle' => 'bottom'});
}

my @tabCode =  @{$tabViewData{'codeReturn'}};
if (&LOC::Util::in_array('errorNumEstimate', \@{$tabViewData{'codeReturn'}}))
{
    print $view->displayMessages('error', [$tabTranslations->{'errorNumEstimate'}], 0);
}
elsif (&LOC::Util::in_array('removed', \@{$tabViewData{'codeReturn'}}))
{
    print $view->displayMessages('valid', [$tabTranslations->{'removed'}], 0);
}
elsif (&LOC::Util::in_array('errorNoChange', \@{$tabViewData{'codeReturn'}}))
{
    print $view->displayMessages('error', [$tabTranslations->{'errorNoChange'}], 0);
}
elsif (&LOC::Util::in_array('errorCodeChange', \@{$tabViewData{'codeReturn'}}))
{
    print $view->displayMessages('error', [$tabTranslations->{'errorCodeChange'}], 0);
}
elsif (&LOC::Util::in_array('noPossible', \@{$tabViewData{'codeReturn'}}))
{
    print $view->displayMessages('error', [$tabTranslations->{'noPossible'}], 0);
}

print $view->displayFormInputHidden('check', $tabViewData{'check'});
print $view->displayFormInputHidden('oldCode', $tabViewData{'customerCode'});
print $view->displayFormInputHidden('oldCountry', $tabViewData{'country'});
# Affichage du pied de page
print $view->endForm();
print $view->displayFooter();

