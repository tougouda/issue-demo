use utf8;
use open (':encoding(UTF-8)');

use strict;
use lib 'inc', 'lib';

use LOC::Html::Standard;
use LOC::Locale;
use LOC::Util;

our %tabViewData;


my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
my $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


$view->setPageTitle($locale->t('Support technique'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle($locale->t('Support technique') . ' > ' . $locale->t('Kimoce') . ' > ' . $locale->t('Synchro des caractéristiques d\'une machine'));

# jQuery
$view->addJSSrc('jQuery/jquery.js');
# CSS
$view->addCSSSrc('helpdesk/base.css');
$view->addCSSSrc('location.css');


# Bouton "Retour"
my $viewUrl = $view->createURL('helpdesk:index:view');
my $btn     = $view->displayTextButton('Retour', 'revert(|Over).png', $viewUrl,
                                   $locale->t('Retourner au support'),
                                   'l',
                                   {'tabindex' => -1,
                                    'onfocus' => 'this.blur();'
                                   });

$view->addControlsContent($btn, 'left', 1);

my $actualizeBtn = $view->createURL('helpdesk:kimoce:syncMachineFeatures');
my $btn     = $view->displayTextButton('Actualiser', 'refresh(|Over).png', $actualizeBtn,
                                   $locale->t('Actualiser'),
                                   'l',
                                   {'tabindex' => -1,
                                    'onfocus' => 'this.blur();'
                                   });
$view->addControlsContent($btn, 'right', 1);

# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();

#Formulaire des entrées
my $listTextArea = $view->displayFormInputText('parkNumber', $tabViewData{'parkNumber'});


print '
<fieldset class="form">
    <legend>' . htmllocale('Données') . '</legend>
    <table class="formTable">
        <tr>
            <td class="label">' . htmllocale('N° parc') . '</td>
            <td>' . $listTextArea . '</td>
        </tr>
    </table>
</fieldset>';

if ($tabViewData{'result'} eq 'ok')
{
    print $view->displayMessages('valid', [$locale->t('Caractéristiques de la machine %s mises à jour dans Kimoce', $tabViewData{'parkNumber'})], 0);
}
elsif ($tabViewData{'result'} eq 'ko')
{
    my $tabErrorMessages = {
        'unknownMachine' => $locale->t('Machine %s inconnue dans Kimoce', $tabViewData{'parkNumber'})
    };

    print $view->displayMessages('error', [$tabErrorMessages->{$tabViewData{'errorCode'}}], 0);
}


# Boutons de vérification/validation
my $validButton = $view->displayTextButton(
                                            $locale->t('Synchroniser'),
                                            'valid(|Over).png',
                                            'window.document.forms[0].submit()',
                                            $locale->t('Synchroniser les caractéristiques'),
                                            'l',
                                            {'class' => 'locCtrlButton',
                                             'tabindex' => -1,
                                             'onfocus' => 'this.blur();'
                                            }
                                           );

print $view->displayControlPanel({'left' => $validButton, 'substyle' => 'bottom'});

# Affichage du pied de page
print $view->endForm();
print $view->displayFooter();

