use utf8;
use open (':encoding(UTF-8)');

use strict;
use lib 'inc', 'lib';

use LOC::Html::Standard;
use LOC::Locale;
use LOC::Util;

our %tabViewData;


my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
my $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


$view->setPageTitle($locale->t('Support technique'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle($locale->t('Support technique') . ' > ' . $locale->t('Kimoce') . ' > ' . $locale->t('Renvoi des mises à jour de caractéristiques non traitées'));

# jQuery
$view->addJSSrc('jQuery/jquery.js');
# CSS
$view->addCSSSrc('helpdesk/base.css');
$view->addCSSSrc('location.css');


# Bouton "Retour"
my $viewUrl = $view->createURL('helpdesk:index:view');
my $btn     = $view->displayTextButton('Retour', 'revert(|Over).png', $viewUrl,
                                   $locale->t('Retourner au support'),
                                   'l',
                                   {'tabindex' => -1,
                                    'onfocus' => 'this.blur();'
                                   });

$view->addControlsContent($btn, 'left', 1);

my $actualizeBtn = $view->createURL('helpdesk:kimoce:resendNotTreatedFeaturesUpds');
my $btn     = $view->displayTextButton('Actualiser', 'refresh(|Over).png', $actualizeBtn,
                                   $locale->t('Actualiser'),
                                   'l',
                                   {'tabindex' => -1,
                                    'onfocus' => 'this.blur();'
                                   });
$view->addControlsContent($btn, 'right', 1);

# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();


my $tabButtons = [];


if (defined $tabViewData{'results'})
{
    print '
<fieldset class="form">
    <legend>' . htmllocale('Résultats') . '</legend>
<table class="basic" style="width: 100%;">
    <thead>
        <tr>
            <th>' . htmllocale('Id') . '</th>
            <th>' . htmllocale('N° de parc') . '</th>
            <th>' . htmllocale('Caractéristique') . '</th>
            <th>' . htmllocale('Ancienne valeur') . '</th>
            <th>' . htmllocale('Nouvelle valeur') . '</th>
            <th>' . htmllocale('Date') . '</th>
            <th>' . htmllocale('Renvoyée ?') . '</th>
        </tr>
    </thead>
    <tbody>';

    my $tabReasons = {
        'requestExists'   => $locale->t('Une demande existe'),
        'valueHasChanged' => $locale->t('La valeur a changé')
    };

    foreach my $row (values(%{$tabViewData{'results'}}))
    {
        print '
        <tr>
            <td style="font-size: 10px;">' . $view->toHTMLEntities($row->{'id'}) . '</td>
            <td>' . $view->displayParkNumber($row->{'fullParkNumber'}) . '</td>
            <td>' . $view->toHTMLEntities($row->{'featureCode'}) . '</td>
            <td>' . $view->toHTMLEntities($row->{'oldValue'}) . '</td>
            <td>' . $view->toHTMLEntities($row->{'newValue'}) . '</td>
            <td>' . $locale->getDateFormat($row->{'date'}, LOC::Locale::FORMAT_DATETIME_NUMERIC) . '</td>
            <td>' . $view->displayBoolean($row->{'@done'}, '') .
                    ($row->{'@details'} ? $view->displayTooltipIcon('info', '<b>' . $locale->t('Motif') . ':</b><br />' .
                                                                            $view->toHTMLEntities($tabReasons->{$row->{'@details'}->{'reason'}}) . '<br />' .
                                                                            '<b>' . $locale->t('Valeur actuelle') . ':</b><br />' .
                                                                            $view->toHTMLEntities($row->{'@details'}->{'currentValue'})) : '') . '</td>
        </tr>';
    }

    print '
    </tbody>
</table>

</fieldset>';
}
elsif (values(%{$tabViewData{'list'}}) > 0)
{
    print '
<table class="basic" style="width: 100%;">
    <thead>
        <tr>
            <th>' . htmllocale('Id') . '</th>
            <th>' . htmllocale('N° de parc') . '</th>
            <th>' . htmllocale('Caractéristique') . '</th>
            <th>' . htmllocale('Ancienne valeur') . '</th>
            <th>' . htmllocale('Nouvelle valeur') . '</th>
            <th>' . htmllocale('Date') . '</th>
            <th>' . htmllocale('Erreur') . '</th>
        </tr>
    </thead>
    <tbody>';

    my $tabModes = {
        LOC::Kimoce::UPDATEMODE_WS() => $locale->t('Webservice'),
        LOC::Kimoce::UPDATEMODE_DB() => $locale->t('Base de données')
    };

    foreach my $row (values(%{$tabViewData{'list'}}))
    {
        print '
        <tr>
            <td style="font-size: 10px;">' . $view->toHTMLEntities($row->{'id'}) . '</td>
            <td>' . $view->displayParkNumber($row->{'fullParkNumber'}) . '</td>
            <td>' . $view->toHTMLEntities($row->{'featureCode'}) . '</td>
            <td>' . $view->toHTMLEntities($row->{'oldValue'}) . '</td>
            <td>' . $view->toHTMLEntities($row->{'newValue'}) . '</td>
            <td>' . $locale->getDateFormat($row->{'date'}, LOC::Locale::FORMAT_DATETIME_NUMERIC) . '</td>
            <td>' . $view->displayTooltipIcon('info', '<b>' . $locale->t('Mode') . ':</b><br />' . $view->toHTMLEntities($tabModes->{$row->{'updateMode'}}) . '<br />' .
                                                      '<b>' . $locale->t('Méthode') . ':</b><br />' . $view->toHTMLEntities($row->{'errorMethod'}) . '<br />' .
                                                      '<b>' . $locale->t('Dernière requête') . ':</b><br />' . $view->toHTMLEntities($row->{'errorLastRequest'}) . '<br />' .
                                                      '<b>' . $locale->t('Message') . ':</b><br />' . $view->toHTMLEntities($row->{'errorMessage'})) . '</td>
        </tr>';
    }

    print '
    </tbody>
</table>';


    # Boutons de vérification/validation
    push(@$tabButtons, $view->displayTextButton($locale->t('Renvoyer les mises à jour'),
                                                'valid(|Over).png',
                                                'window.document.forms[0].submit()',
                                                $locale->t('Renvoyer les mises à jour à Kimoce'),
                                                'l',
                                                {'class' => 'locCtrlButton',
                                                'tabindex' => -1,
                                                'onfocus' => 'this.blur();'
                                                }
                                            ));

    print $view->displayFormInputHidden('lastId', $tabViewData{'lastId'});
}
else
{
    # Message d'information
    print $view->displayMessages('info', [$locale->t('Aucune mise à jour de caractéristique non traitée')], 0);
}

push(@$tabButtons, $view->displayTextButton('Vérifier de nouveau', 'reload(|Over).png', $actualizeBtn,
                                            $locale->t('Vérifier de nouveau'),
                                            'l',
                                            {'tabindex' => -1,
                                            'onfocus' => 'this.blur();'
                                            }));

print $view->displayControlPanel({'left' => $tabButtons, 'substyle' => 'bottom'});


# Affichage du pied de page
print $view->endForm();
print $view->displayFooter();

