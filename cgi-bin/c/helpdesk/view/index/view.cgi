use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;
use LOC::Locale;

our %tabViewData;

# TODO: Le jour où on veut traduire cette interface, renommer tous les "&locale_t" par "$locale->t"
sub locale_t
{
    return $_[0];
}


my $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
my $view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});


$view->setPageTitle(&locale_t('Support technique'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle(&locale_t('Support technique'));

# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addCSSSrc('helpdesk/base.css');
$view->addCSSSrc('helpdesk/index/view.css');

# Bouton "actualiser"
my $btnRefresh = $view->displayTextButton($locale->t('Actualiser'),
                                          'refresh(|Over).png',
                                          $view->createURL('helpdesk:index:view'),
                                          $locale->t('Actualiser le devis de location'),
                                          'l',
                                          {'tabindex' => -1,
                                           'onfocus' => 'this.blur();'
                                          });

$view->addControlsContent($btnRefresh, 'right', 1);



# Bouton "Gestion des utilisateurs"
my $btnUsers = $view->displayTextButton(&locale_t('Gestion des utilisateurs'),
                                        'common/contact/list(|Over).png',
                                        $view->createURL('admin:users:view'),
                                        &locale_t('Gestion des utilisateurs'),
                                        'l',
                                        {'tabindex' => -1,
                                         'onfocus' => 'this.blur();',
                                         'target' => '_blank'
                                        });

# GLPI
my $btnGlpi = $view->displayTextButton(&locale_t('GLPI'),
                                       'helpdesk/index/glpi(|Over).png',
                                       &LOC::Globals::get('glpiUrl'),
                                       &locale_t('GLPI'),
                                       'l',
                                       {'tabindex' => -1,
                                        'onfocus' => 'this.blur();',
                                        'target' => '_blank'
                                       });

# Wiki
my $btnWiki = $view->displayTextButton(&locale_t('Wiki'),
                                       'helpdesk/index/wiki(|Over).png',
                                       &LOC::Globals::get('helpDeskWikiUrl'),
                                       &locale_t('Wiki'),
                                       'l',
                                       {'tabindex' => -1,
                                        'onfocus' => 'this.blur();',
                                        'target' => '_blank'
                                       });

# Annuaire
my $btnBook = $view->displayTextButton(&locale_t('Annuaire interne'),
                                       'helpdesk/index/addressBook(|Over).png',
                                       &LOC::Globals::get('addressBookUrl'),
                                       &locale_t('Annuaire interne'),
                                       'l',
                                       {'tabindex' => -1,
                                        'onfocus' => 'this.blur();',
                                        'target' => '_blank'
                                       });

# Annuaire
my $btnSpvC = $view->displayTextButton(&locale_t('Console de supervision'),
                                       'helpdesk/index/supervConsole(|Over).png',
                                       &LOC::Globals::get('supervConsoleUrl'),
                                       &locale_t('Console de supervision'),
                                       'l',
                                       {'tabindex' => -1,
                                        'onfocus' => 'this.blur();',
                                        'target' => '_blank'
                                       });



my $tabLinks = [
    $btnUsers,
    '<hr />',
    $btnGlpi,
    $btnWiki,
    $btnBook,
    $btnSpvC
];

my $tabModules = [

    # Transport
    {
        'name' => &locale_t('Transport'),
        'tabPages' => [
            {
                'name' => &locale_t('Mise à jour des commandes OTD'),
                'url'  => $view->createURL('helpdesk:transport:updateOrder')
            }
        ]
    },

    # Télématique
    {
        'name' => &locale_t('Télématique'),
        'tabPages' => [
            {
                'name' => &locale_t('Procédures'),
                'url'  => $view->createURL('helpdesk:telematics:execProcedures')
            },
            {
                'name' => &locale_t('Accès aux services'),
                'url'  => $view->createURL('helpdesk:telematics:execServices')
            },
            {
                'name' => &locale_t('Statistiques sur les codes'),
                'url'  => $view->createURL('helpdesk:telematics:codesStats')
            }
        ]
    },

#    # Technique
#    {
#        'name' => &locale_t('Technique'),
#        'tabPages' => [
#            {
#                'name' => &locale_t('Modification de date de visite périodique impossible'),
#                'url'  => ''
#            }
#        ]
#    },

    # Remise en etat
    {
        'name' => &locale_t('Remise en etat'),
        'tabPages' => [
            {
                'name' => &locale_t('Annulation de la facturation d\'un devis'),
                'url'  => $view->createURL('helpdesk:repair:cancelEstimateInvoicing')
            },
            {
                'name' => &locale_t('Remise en cours d\'un devis abandonné'),
                'url'  => $view->createURL('helpdesk:repair:reactivateEstimate')
            }
        ]
    },

    # Comptabilité
    {
        'name' => &locale_t('Comptabilité'),
        'tabPages' => [
            {
                'name' => &locale_t('Supprimer le code d\'un client'),
                'url'  => $view->createURL('helpdesk:accounting:removeCodeCustomer')
            }
        ]
    },

    # Kimoce
    {
        'name' => &locale_t('Kimoce'),
        'tabPages' => [
            {
                'name' => &locale_t('Synchro des caractéristiques d\'une machine'),
                'url'  => $view->createURL('helpdesk:kimoce:syncMachineFeatures')
            },
            {
                'name' => &locale_t('Renvoi des mises à jour de caractéristiques non traitées'),
                'url'  => $view->createURL('helpdesk:kimoce:resendNotTreatedFeaturesUpds')
            }
        ]
    }
];



# Affichage de l'entête
print $view->displayHeader();

print '
<fieldset class="scripts">
    <legend>' . $view->toHTMLEntities(&locale_t('Scripts')) . '</legend>';

# Affichage des modules et des pages
foreach my $tabMod (@$tabModules)
{
    print '
<div class="module">
    <span class="title">' . $view->toHTMLEntities($tabMod->{'name'}) . '</span>

    <ul>';

    foreach my $tabPage (@{$tabMod->{'tabPages'}})
    {
        if ($tabPage->{'url'} ne '')
        {
            print '
            <li class="active"><a href="' . $tabPage->{'url'} . '">' . $view->toHTMLEntities($tabPage->{'name'}) . '</a></li>';
        }
        else
        {
            print '
            <li class="inactive"><a onclick="return false;" href="#">' . $view->toHTMLEntities($tabPage->{'name'}) . '</a></li>';
        }
    }
    print '
    </ul>

</div>';
}

print '
</fieldset>';

print '
<fieldset class="links">
    <legend>' . $view->toHTMLEntities(&locale_t('Liens utiles')) . '</legend>

    <ul>';

    foreach my $link (@$tabLinks)
    {
        print '
            <li class="inactive">' . $link . '</li>';
    }
    print '
    </ul>

</fieldset>';


print $view->displayFooter();




