use utf8;
use open (':encoding(UTF-8)');

# Formulaire des entrées
my $prefix = 'agenciesTransfer.';
my $inputsTabIndex = 1;


# N° de parc
my $parkNoInput = $view->displayFormInputText($prefix . 'search.parkNo', $tabViewData{'tabData'}->{$prefix . 'search.parkNo'}, 
                                              '', 
                                              {
                                                  'size'         => 6, 
                                                  'maxlength'    => 6,
                                                  'autocomplete' => 'off',
                                                  'onkeyup'      => 'onKeyUpField(event);',
                                                  'tabindex'     => $inputsTabIndex++
                                              });
# Date prévue du transfert
my $transferDateInput = $view->displayFormInputDate($prefix . 'search.transferDate', 
                                                ($tabViewData{'tabData'}->{$prefix . 'search.transferDate'} eq '' ? undef : $tabViewData{'tabData'}->{$prefix . 'search.transferDate'}),
                                                 undef, undef, {'tabindex' => $inputsTabIndex++});

# Agences
tie(my %tabAgencies, 'Tie::IxHash');
$tabAgencies{''} = '-- ' . $locale->t('Sélectionner') . ' --';
foreach my $agencyId (keys(%{$tabViewData{'agencyList'}}))
{
    $tabAgencies{$agencyId} = $tabViewData{'agencyList'}->{$agencyId};
}

# Agence de provenance
my $agencyFromSelect = $view->displayFormSelect($prefix . 'search.agencyFrom.id', \%tabAgencies, 
                                                $tabViewData{'tabData'}->{$prefix . 'search.agencyFrom.id'}, 0, 
                                              {'tabindex' => $inputsTabIndex++
                                              });
# Agence de destination
my $agencyToSelect = $view->displayFormSelect($prefix . 'search.agencyTo.id', \%tabAgencies, 
                                              $tabViewData{'tabData'}->{$prefix . 'search.agencyTo.id'},0, 
                                              {'tabindex' => $inputsTabIndex++
                                              });

# Bouton pour effacer la recherche
my $clearSearchButton = $view->displayTextButton('', 'clear.gif', "clearSearch('" . $prefix . "', 2);", $locale->t('Effacer la recherche'));

# Onglet Transfert inter-agences
print "
<div title='" . &locale_t('Transfert inter-agences') . '  ' . $clearSearchButton . "'>";
print '
    <table class="formTable">
        <tr>
            <td class="label">' . &locale_t('Machine') . '</td>
            <td>' . $parkNoInput . '</td>
        </tr>
        <tr>
            <td class="label">' . &locale_t('Date prévue du transfert') . '</td>
            <td>' . $transferDateInput . '</td>
        </tr>
        <tr>
            <td class="label">' . &locale_t('Agence(s) de provenance') . '</td>
            <td>' . $agencyFromSelect . '</td>
            <td class="label">' . &locale_t('Agence(s) de destination') . '</td>
            <td>' . $agencyToSelect . '</td>
        </tr>

    </table>
</div>';