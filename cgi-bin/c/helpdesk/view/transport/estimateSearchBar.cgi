use utf8;
use open (':encoding(UTF-8)');

my $prefix = 'estimateLine.';
my $inputsTabIndex = 1;

# N°
my $noInput = $view->displayFormInputText($prefix . 'search.no', 
                                          $tabViewData{'tabData'}->{$prefix . 'search.no'}, 
                                          '', 
                                          {
                                              'size'         => 16, 
                                              'maxlength'    => 20, 
                                              'autocomplete' => 'off',
                                              'onkeyup'      => 'onKeyUpField(event);',
                                              'tabindex'     => $inputsTabIndex++
                                          });

# États sur le contrat
tie(my %tabStates, 'Tie::IxHash');
$tabStates{''} = '-- ' . $locale->t('Tous') . ' --';
foreach my $stateId (sort keys(%{$tabViewData{'estimateStateList'}}))
{
    $tabStates{$stateId} = $tabViewData{'estimateStateList'}->{$stateId};
}
my $statesInput = $view->displayFormSelect($prefix . 'search.state.id', \%tabStates, 
                                           $tabViewData{'tabData'}->{$prefix . 'search.state.id'}, 0,
                                           {'tabindex' => $inputsTabIndex++});

# Client
my $custListUrl = $view->createURL('accounting:customer:jsonList', {'countryId' => $tabViewData{'tabData'}->{'country.id'}, 'format' => 0});
my $custInput = $view->displayFormSelect($prefix . 'search.customer.id', $tabViewData{'tabData'}->{$prefix . 'customers'}, 
                                         $tabViewData{'tabData'}->{$prefix . 'search.customer.id'}, 0,
                                         {'tabindex' => $inputsTabIndex}) .
                $view->displayJSBlock('var searchEstimateLineCustomerSearchBox = Location.searchBoxesManager.createSearchBox("' . $prefix . 'search.customer.id", {isRequired: false, url: "' . $custListUrl . '"});
                searchEstimateLineCustomerSearchBox.createElement = function(key, value)
{
    return new Option((value["name"] || "") + ", " + (value["locality.postalCode"] || ""), value["id"]);
}');

$inputsTabIndex += 2;

# Libellé du chantier
my $siteLabelInput = $view->displayFormInputText($prefix . 'search.site.label', $tabViewData{'tabData'}->{$prefix . 'search.site.label'}, 
                                                 '', 
                                                 {
                                                     'size'         => 25, 
                                                     'autocomplete' => 'off',
                                                     'onkeyup'      => 'onKeyUpField(event);',
                                                     'tabindex'     => $inputsTabIndex++
                                                 });
    
# Département chantier
my $siteDepartmentInput = $view->displayFormInputText($prefix . 'search.site.department', $tabViewData{'tabData'}->{$prefix . 'search.site.department'}, 
                                                      '', 
                                                      {
                                                          'size'         => '3', 
                                                          'autocomplete' => 'off',
                                                          'onkeyup'      => 'onKeyUpField(event);',
                                                          'tabindex'     => $inputsTabIndex++
                                                      });
# Date début Location
my $beginDateInput = $view->displayFormInputDate($prefix . 'search.beginDate', 
                                                 ($tabViewData{'tabData'}->{$prefix . 'search.beginDate'} eq '' ? undef : $tabViewData{'tabData'}->{$prefix . 'search.beginDate'}),
                                                 undef, undef, {'tabindex' => $inputsTabIndex++});
# Aide sur la date de début
my $beginDateHelp = $view->displayHelp($locale->t('La recherche prend en compte les contrats dont la date de début commence après la date saisie'));

# Date fin location
my $endDateInput = $view->displayFormInputDate($prefix . 'search.endDate', 
                                               ($tabViewData{'tabData'}->{$prefix . 'search.endDate'} eq '' ? undef : $tabViewData{'tabData'}->{$prefix . 'search.endDate'}),
                                               undef, undef, {'tabindex' => $inputsTabIndex++});
# Aide sur la date de fin
my $endDateHelp = $view->displayHelp($locale->t('La recherche prend en compte les contrats dont la date de fin se termine avant la date saisie'));

# Utilisateurs
my $userListUrl = $view->createURL('default:user:jsonList', {'format' => 1});
my $usersInput = $view->displayFormSelect($prefix . 'search.user.id', $tabViewData{'tabData'}->{$prefix . 'users'}, 
                                          $tabViewData{'tabData'}->{$prefix . 'search.user.id'}, 0, 
                                          {'tabindex' => $inputsTabIndex}) .
                 $view->displayJSBlock('var searchEstimateLineUserSearchBox = Location.searchBoxesManager.createSearchBox("' . $prefix . 'search.user.id", {isRequired: false, url: "' . $userListUrl . '"});');

$inputsTabIndex += 2;

# Négociateur
my $userListUrl = $view->createURL('default:user:jsonList', {'format' => 1});
my $interlocInput = $view->displayFormSelect($prefix . 'search.negotiator.id',$tabViewData{'tabData'}->{$prefix . 'negotiators'}, 
                                             $tabViewData{'tabData'}->{$prefix . 'search.negotiator.id'}, 0, 
                                             {'tabindex' => $inputsTabIndex}) .
                    $view->displayJSBlock('var searchEstimateLineInterlocSearchBox = Location.searchBoxesManager.createSearchBox("' . $prefix . 'search.negotiator.id", {isRequired: false, url: "' . $userListUrl . '"});');

$inputsTabIndex += 2;

my $custHelp = $view->displayHelp($locale->t('La recherche de client s\'effectue parmi la raison sociale, le code client, la localité, le code postal et le département'));
my $beginDateHelp = $view->displayHelp($locale->t('La recherche prend en compte les lignes de devis dont la date de début commence après la date saisie'));
my $endDateHelp = $view->displayHelp($locale->t('La recherche prend en compte les les lignes de devis dont la date de fin se termine avant la date saisie'));

# Bouton pour effacer la recherche
my $clearSearchButton = $view->displayTextButton('', 'clear.gif', "clearSearch('" . $prefix . "', 1);", $locale->t('Effacer la recherche'));

# Onglet Lignes de devis
print "
<div title='" . &locale_t('Ligne de devis') . '  ' . $clearSearchButton . "'>";
print '
    <table class="formTable">
        <tr>
            <td class="label">' . &locale_t('N°') . '</td>
            <td>' . $noInput . '</td>
            <td class="label">' . &locale_t('Etat Devis') . '</td>
            <td>' . $statesInput . '</td>
        </tr>
        <tr>
            <td class="label">' . &locale_t('Client') . $custHelp . '</td>
            <td>' . $custInput . ' ' .  $view->displayTextButton('', 'clear.gif', 'searchEstimateLineCustomerSearchBox.clear();', $locale->t('Effacer la recherche')) . '</td>
        </tr>
        <tr>
            <td class="label">' . &locale_t('Lib. chantier') . '</td>
            <td>' . $siteLabelInput . '</td>
            <td class="label">' . &locale_t('Dép. chantier') . '</td>
            <td>' . $siteDepartmentInput . '</td>
        </tr>
        <tr>
            <td class="label">' . &locale_t('Date de début') . $beginDateHelp . '</td>
            <td>' . $beginDateInput . '</td>
            <td class="label">' . &locale_t('Date de fin') . $endDateHelp . '</td>
            <td>' . $endDateInput . '</td>
        </tr>
        <tr>
            <td class="label">' . &locale_t('Créé par') . '</td>
            <td>' . $usersInput . ' ' .  $view->displayTextButton('', 'clear.gif', 'searchEstimateLineUserSearchBox.clear();', $locale->t('Effacer la recherche')) . '</td>
            <td class="label">' . &locale_t('Négociateur') . '</td>
            <td>' . $interlocInput . ' ' .  $view->displayTextButton('', 'clear.gif', 'searchEstimateLineInterlocSearchBox.clear();', $locale->t('Effacer la recherche')) . '</td>
        </tr>
    </table>
</div>';