use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;

our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
our $view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});


# Répertoire CSS/JS/Images
our $dir = 'helpdesk/transport/';

sub locale_t
{
    return $_[0];
}

$view->setPageTitle('Support technique');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle('Support technique > Transport > Mise à jour des commandes OTD');

# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc($dir . 'updateOrder.js');
$view->addJSSrc($dir . 'launchRenew.js');

# CSS
$view->addCSSSrc('location.css');
$view->addCSSSrc('helpdesk/base.css');
$view->addCSSSrc($dir . 'general.css');
$view->addCSSSrc($dir . 'updateOrder.css');



$view->addPackage('searchboxes')
     ->addPackage('tabBoxes');

our $progressionUrl = $view->createURL('helpdesk:transport:checkProgression',
                                            {'countryId'   => $tabViewData{'searchCountry'}});

$view->addBodyEvent('load', 'setDocumentActiveTabIndex("' . $tabViewData{'activeTabIndex'} . '");
                             displayChoiceAgenciesLabel();');
if (exists $tabViewData{'requestId'})
{
    $view->addBodyEvent('load', 'checkProgression(' . &LOC::Json::toJson($tabViewData{'requestId'}) . ', ' .
                                                 '"' . $view->toJSEntities($progressionUrl) . '");');
}

# Bouton "Retour"
my $viewUrl = $view->createURL('helpdesk:index:view');
my $btn     = $view->displayTextButton('Retour', 'revert(|Over).png', $viewUrl,
                                   'Retourner au support',
                                   'l',
                                   {'tabindex' => -1,
                                    'onfocus' => 'this.blur();'
                                   });

$view->addControlsContent($btn, 'left', 1);

# Affichage de l'entête
print $view->displayHeader();
print $view->startForm(undef, undef, 'formVerif');


# Affichage retour exécution déverrouillage
if (exists($tabViewData{'unlockError'}) && $tabViewData{'unlockError'})
{
    print $view->displayMessages('error', [$locale->t('Une erreur est survenue lors du déverrouillage des transports')], 0);
}
elsif (exists($tabViewData{'unlockSuccess'}) && $tabViewData{'unlockSuccess'})
{
    print $view->displayMessages('valid', [$locale->t('Les transports demandés ont été déverrouillés')], 0);
}


my %inputOptions;
%inputOptions = ('onchange' => ' window.document.forms[0].submit();');

tie(my %tabCountries, 'Tie::IxHash');

foreach my $countryId (sort keys(%{$tabViewData{'countryList'}}))
{
    $tabCountries{$countryId} = $tabViewData{'countryList'}->{$countryId};
}
$tabCountries{'TS'} = $locale->t('Tous les pays');

my $countrySelect = $view->displayFormSelect('country.id', \%tabCountries, 
                                             $tabViewData{'tabData'}->{'country.id'}, 0, \%inputOptions);

my $validAgenciesBtn = $view->displayTextButton($locale->t('Valider les agences'), 
                                               'valid(|Over).png',
                                               'displayChoiceAgenciesLabel();',
                                               $locale->t('Valider les agences'),
                                               'l',
                                               {'id' => 'validAgBtn',
                                                'tabindex' => -1,
                                                'onfocus' => 'this.blur();'
                                               });
                                               
my $agenciesSelectHelp = $view->displayHelp($locale->t('La sélection des agences n\'est pas pris en compte pour l\'onglet Transfert inter-agences'));

my $agenciesSelect = '
    <div id="agenciesSelect">
        <a class="agenciesLabelAll" href="#" onclick="return false;">' . $locale->t('Toutes les agences') . ' ' . $agenciesSelectHelp . '</a>
        <a class="agenciesLabel" href="#" onclick="return false;">' . $locale->t('Agence(s) de ') . '<span></span> ' . $agenciesSelectHelp . '</a>
        <div id="choiceAgencies">
            <table class="searchAgencies">
                <tbody>';

my @tabAgenciesList;
my $tabCountriesInfos = $tabViewData{'tabCountriesInfos'};
my $nbCountries = @$tabCountriesInfos; 
my $selectedAgencies = $tabViewData{'tabData'}->{'agencies.id'};
for (my $i = 0; $i < $nbCountries; $i++)
{
    my $tabAreas = $tabCountriesInfos->[$i]->{'tabAreas'};
    my $nbAreas  = @$tabAreas;
    for (my $j = 0; $j < $nbAreas; $j++)
    {
        $agenciesSelect.= '
                    <tr class="area">
                        <td class="name" onclick="">' . ($tabAreas->[$j]->{'id'} ? $tabAreas->[$j]->{'label'} : $locale->t('Pas de région')) . '</td>
                        <td class="agencies">';
        my $tabAgencies = $tabAreas->[$j]->{'tabAgencies'};
        my $nbAgencies = @$tabAgencies;
        for (my $k = 0; $k < $nbAgencies; $k++)
        {
            my $activeClass = 'off';
            if (LOC::Util::in_array($tabAgencies->[$k]->{'id'}, $selectedAgencies))
            {
                $activeClass = 'on';
            }
            
            $agenciesSelect.= '
                            <span class="agency ' . $activeClass . '" title="' . $tabAgencies->[$k]->{'label'} . '">' . $tabAgencies->[$k]->{'id'} . '</span>';

            push(@tabAgenciesList, {
                'value'     => $tabAgencies->[$k]->{'id'},
                'innerHTML' => $tabAgencies->[$k]->{'label'}
            });
        }
        $agenciesSelect.= '
                        </td>
                    </tr>';
    }
}
# Tri des régions par ordre alpha
@tabAgenciesList = sort {$a->{'value'} cmp $b->{'value'}} @tabAgenciesList;

    $agenciesSelect.= '
                </tbody>
            </table>
            ' . $view->displayControlPanel({'left' => $validAgenciesBtn, 'substyle' => 'bottom'}) . '
        </div>
    </div>
';

print '
<fieldset id="Fieldset">
    <legend>' . $view->displayImage('helpdesk/common/form.png') . '&nbsp;' . $locale->t('Recherche') . '</legend>
        <table class="formTable">
        <tbody>
        <tr>
            <td class="label">' . $locale->t('Pays') . '</td>
            <td>' . $countrySelect . '</td>
            <td>' . $agenciesSelect . '</td>
        </tr>
        </tbody>
        </table>
        <div id="TabBox">';

# Onglet Contrat
require($directory . '/transport/contractSearchBar.cgi');
# Onglet Lignes de devis
require($directory . '/transport/estimateSearchBar.cgi');
# Onglet Transfert inter-agences
require($directory . '/transport/agenciesTransferSearch.cgi');

print '
        </div>';

my @tabLeftBtns = ();
# Boutons de vérification/validation
my $verifButton = $view->displayTextButton(
                                        'Rechercher',
                                        'valid(|Over).png',
                                        'submitForm("formVerif", "validVerif");',
                                        'Vérifier les données',
                                        'l',
                                        {'id'    => 'supportVerifBtn',
                                         'class' => 'locCtrlButton',
                                         'tabindex' => -1,
                                         'onfocus' => 'this.blur();'
                                        }
                                        );


print $view->displayControlPanel({'left' => $verifButton, 'substyle' => 'bottom'});
print '
</fieldset>';


# Affichage des résultats correspondant aux critères de recherche
my $isUpdatePossible = 0;

# Affichage pour les résultats par pays
my $isOnlySelectedCountry = 0;
# Si aucun critère sélectionné, la maj concerne le pays sélectionné
if (exists $tabViewData{'nbFilters'} &&  $tabViewData{'nbFilters'}== 0)
{
    print '<p class="noResult">' . $locale->tn('%d transport concerné', '%d transports concernés', $tabViewData{'nbResult'}, $tabViewData{'nbResult'}) . '</p>';
    $isUpdatePossible = 1;
    $isOnlySelectedCountry = 1;
}
elsif (exists $tabViewData{'result'})
{

    print '<h1 class="searchResult">' . $locale->t('Résultat de la recherche') . '</h1>';

    if ($tabViewData{'nbResult'} > 100)
    {
        print '<p class="noResult">' . $locale->t('Le résultat de la recherche contient plus de 100 lignes. Veuillez affiner votre recherche.') . '
               </p>';
    }
    elsif ($tabViewData{'nbResult'} > 0)
    {

        print '<p class="noResult">' . $locale->tn('%d résultat correspond à la recherche', '%d résultats correspondent à la recherche', $tabViewData{'nbResult'}, $tabViewData{'nbResult'}) . '</p>';

        $isUpdatePossible = 1;

        # CheckBox pour la sélection
        my $selectionAllCheck = $view->displayFormInputCheckBox('cbCheckAll', 0, 
                                                                {'onclick' => 'checkAll();'}, '', '', 0);
       
       print '
<table class="list basic">
<thead>
    <tr>
        <th>' . $locale->t('Type de document') . '</th>
        <th>' . $locale->t('Agence') . '</th>
        <th class="documentCode">' . $locale->t('Code document') . '</th>
        <th class="documentCode">' . $locale->t('Machine') . '</th>
        <th>' . $locale->t('Provenance') . '</th>
        <th>' . $locale->t('Destination') . '</th>
        <th>' . $locale->t('Date de début') . '</th>
        <th>' . $locale->t('Date de fin') . '</th>
        <th class="orderDelivery">' . $locale->t('Cmde Livraison') . '</th>
        <th class="isSynchroDisabled" title="' . $locale->t('Synchronisation avec OTD') . '">Synchro.</th>
        <th class="isLocked" title="' . $locale->t('Transport verrouillé') . '">Verr.</th>
        <th class="orderRecovery">' . $locale->t('Cmde Récupération') . '</th>
        <th class="isSynchroDisabled" title="' . $locale->t('Synchronisation avec OTD') . '">Synchro.</th>
        <th class="isLocked" title="' . $locale->t('Transport verrouillé') . '">Verr.</th>
        <th class="selection" title="' . $locale->t('Cmde Livraison et Récupération') . '">' . $selectionAllCheck . '</th>
    </tr>
</thead>
<tbody>';

        # Compteur du nombre de lignes
        my $countLine = 0;

        my %tabResult = %{$tabViewData{'result'}};
        foreach my $tabInfos (reverse sort {$tabResult{$a}->{'type'} cmp $tabResult{$b}->{'type'}} keys(%tabResult))
        {
            my $selectionCheck = '';
            my $documentUrl = '';
            my $documentType = '';
            my $documentCode = '';
            my $machineUrl = '';

            # CheckBox pour la sélection
            $selectionCheck = $view->displayFormInputCheckBox('tsp', 0, 
                                                              {},
                                                              '', $tabResult{$tabInfos}->{'tspDelivery.id'} .',' . $tabResult{$tabInfos}->{'tspRecovery.id'}, 0);

            if ($tabResult{$tabInfos}->{'type'} eq 'contract')
            {
                $documentUrl = $view->createURL('rent:rentContract:view', {'contractId' => $tabResult{$tabInfos}->{'document.id'},
                                                                           'tabIndex'   => LOC::Transport::DEFAULT_CONTRACT_TABINDEX});
                $documentCode = $tabResult{$tabInfos}->{'document.code'};
                $documentType = $locale->t('Contrat');
                $machineUrl = &LOC::Machine::getUrl($tabResult{$tabInfos}->{'document.id'});
            }
            elsif ($tabResult{$tabInfos}->{'type'} eq 'estimateLine')
            {
                $documentUrl = $view->createURL('rent:rentEstimate:view', {'estimateId'     => $tabResult{$tabInfos}->{'document.id'}->[0],
                                                                           'estimateLineId' => $tabResult{$tabInfos}->{'document.id'}->[1],
                                                                           'tabIndex'       => LOC::Transport::DEFAULT_ESTIMATELINE_TABINDEX});
                $documentCode = $tabResult{$tabInfos}->{'document.code'}->[0] . '<br /><i>(' . $locale->t('ligne %s', $tabResult{$tabInfos}->{'document.code'}->[1]) . ')</i>';
                $documentType = $locale->t('Lignes de devis');
            }
            elsif ($tabResult{$tabInfos}->{'type'} eq 'agenciesTransfer')
            {
                $documentType = $locale->t('Transfert inter-agences');
            }

            # Mise en forme booléen
            my $isDeliverySynchroDisabled = '';
            my $isRecoverySynchroDisabled = '';
            my $isDelivSynchroTitle = '';
            my $isRecovSynchroTitle = '';
            my $isDeliveryLockedClassName = '';
            my $isRecoveryLockedClassName = '';
            my $className = '';

            if ($tabResult{$tabInfos}->{'isDeliverySynchroDisabled'})
            {
                $isDeliverySynchroDisabled = $view->displayBoolean(0, '');
                $isDelivSynchroTitle = $locale->t('Pas de synchronisation avec OTD');
            }
            if ($tabResult{$tabInfos}->{'isRecoverySynchroDisabled'})
            {
                $isRecoverySynchroDisabled = $view->displayBoolean(0, '');
                $isRecovSynchroTitle = $locale->t('Pas de synchronisation avec OTD');
            }
            if ($tabResult{$tabInfos}->{'isDeliveryLocked'} && $tabResult{$tabInfos}->{'isRecoveryLocked'})
            {
                $className     = 'locked';
            }
            if ($tabResult{$tabInfos}->{'isDeliveryLocked'})
            {
                $isDeliveryLockedClassName = 'on';
            }
            if ($tabResult{$tabInfos}->{'isRecoveryLocked'})
            {
                $isRecoveryLockedClassName = 'on';
            }

            print '
    <tr id="line_' . $countLine . '" class="' . $className . '">
        <td>' . $documentType . '</td>
        <td class="">' . $tabResult{$tabInfos}->{'agency.id'} . '</td>
        <td class="documentCode"><a href="' . $documentUrl . '" target="_blank" title="">' . $documentCode . '</a></td>
        <td>' . ($tabResult{$tabInfos}->{'machine.parkNumber'} ? $view->displayParkNumber($tabResult{$tabInfos}->{'machine.parkNumber'}, 'parkNumber_' . $countLine, 1) : '') . '
        </td>
        <td class="">' . $tabResult{$tabInfos}->{'agencyFrom'} . '</td>
        <td class="">' . $tabResult{$tabInfos}->{'agencyTo'} . '</td>
        <td class="beginDate">' . $locale->getDateFormat($tabResult{$tabInfos}->{'beginDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC) . '
        </td>
        <td class="endDate">' . $locale->getDateFormat($tabResult{$tabInfos}->{'endDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC) . '
        </td>
        <td class="orderDelivery">' . $tabResult{$tabInfos}->{'orderDelivery'} . '</td>
        <td class="isSynchroDisabled" title="' . $isDelivSynchroTitle . '">' . $isDeliverySynchroDisabled . '</td>
        <td class="isLocked ' . $isDeliveryLockedClassName . '"><span id="tspLocked_' . $tabResult{$tabInfos}->{'tspDelivery.id'} . '"></span></td>
        <td class="orderRecovery">' . $tabResult{$tabInfos}->{'orderRecovery'} . '</td>
        <td class="isSynchroDisabled" title="' . $isRecovSynchroTitle . '">' . $isRecoverySynchroDisabled . '</td>
        <td class="isLocked ' . $isRecoveryLockedClassName . '"><span id="tspLocked_' . $tabResult{$tabInfos}->{'tspRecovery.id'} . '"></span></td>
        <td class="selection">' . $selectionCheck . '</td>
    </tr>';
    
        $countLine++;
    }
print '
</tbody>
</table>';
    }
    elsif ($tabViewData{'nbResult'} eq 'noSearch')
    {
        print '<p class="noResult">' . $locale->t('Veuillez sélectionnez un pays pour votre recherche.') . '</p>';
    }
    else
    {
        print '<p class="noResult">' . $locale->t('Aucun résultat ne correspond à votre recherche.') . '</p>';
    }
}


print $view->displayJSBlock('
Location.tabBoxesManager.createTabBox("TabBox", null, "200px");
function setDocumentActiveTabIndex(index)
{
    return Location.tabBoxesManager.getTabBox("TabBox").setActiveTabIndex(index);
}');

# Affichage des résultats correspondant aux critères de recherche
if ($isUpdatePossible || exists $tabViewData{'requestId'})
{
    # Bouton de déverrouillage
    my $unlockButton = $view->displayTextButton(
                                 &locale_t('Déverrouiller les transports sélectionnés'),
                                 'standard/opened.png',
                                 'submitForm("formVerif", "validUnlock")',
                                 &locale_t('Déverrouiller les transports sélectionnés'),
                                 'l',
                                 {'id' => 'unlockBtn', 
                                 'class' => 'locCtrlButton disabled'
                                 }
                                 );
    # Bouton lancement du traitement
    my $renewUrl = $view->createURL('helpdesk:transport:launchRenew',
                                            {'countryId'   => $tabViewData{'searchCountry'}});
    my $renewAction = 'renew("' . $view->toJSEntities($renewUrl) . '", ' .
                                    '"' . $view->toJSEntities($progressionUrl) . '");';

    my $launchButton = $view->displayTextButton(
                                 &locale_t('Lancer la mise à jour'),
                                 $dir . 'launch(|Over).png',
                                 $renewAction,
                                 &locale_t('Lancer la mise à jour'),
                                 'l',
                                 {'id' => 'launchRenewBtn', 
                                 'class' => 'locCtrlButton ' . ($isOnlySelectedCountry ? '' : 'disabled')
                                 }
                                 );

    my $forceUpdateBtn = $view->displayFormInputCheckBox('forceUpdate', 0, 
                                                         {'id'       => 'forceUpdate', 
                                                          'disabled' => ($isOnlySelectedCountry ? 0 : 1)}, 
                                                         'Forcer l\'envoi des commandes synchronisables', 1, 0);

    print $view->displayFormInputHidden('isOnlySelectedCountry', ($isOnlySelectedCountry ? 1 : 0));

    print $view->displayControlPanel({'left' => [$launchButton, $forceUpdateBtn, $unlockButton], 'substyle' => 'bottom'});
    print '
<fieldset id="fld-updating">
    <legend>' . $view->displayImage($dir . 'launchOver.png') . '&nbsp;' . $locale->t('Mise à jour en cours') . '</legend>
    <span id="renew-progression"' . (exists $tabViewData{'requestId'} ? '' : ' class="hidden"') . '>' .
       '<div class="progressbar"><div id="renew-progression-bar" class="progression"></div></div>' .
        $view->displayImage($dir . 'updating.gif', {'id' => 'renewUpdating'}) . 
        $view->displayImage($dir . 'error.png', {'id' => 'renewError', 'title' => $locale->t('Erreur lors de la mise à jour')}) . '
        <span class="finished-msg">' . $locale->t('Mise à jour effectuée') . '</span>
    </span>
</fieldset>';
}



print $view->displayFormInputHidden('validVerif', '');
print $view->displayFormInputHidden('validUnlock', '');
print $view->displayFormInputHidden('tspToUnlock', []);
print $view->displayFormInputHidden('activeTabIndex', '');
print $view->displayFormInputHidden('selectedAgencies', []);

print $view->endForm();
print $view->displayFooter();