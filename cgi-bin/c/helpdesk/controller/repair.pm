use utf8;
use open (':encoding(UTF-8)');

# Package: repair
# Contrôleur de gestion du support technique de la GesLoc
package repair;

use strict;
use File::Basename;
use LOC::Country;
use LOC::Repair::Estimate;
use LOC::Db;


sub cancelEstimateInvoicingAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my @tabCode = ();

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );
    
    $tabViewData{'countryList'} = &LOC::Country::getList(LOC::Util::GETLIST_PAIRS);
    $tabViewData{'check'}       = &LOC::Request::getString('check', '');
    
    my $countryId      = &LOC::Request::getString('countryId', 'FR');
    my $id             = &LOC::Request::getInteger('repairEstimateId', 0);
    my $oldId          = &LOC::Request::getInteger('oldId', 0);
    my $oldCountry     = &LOC::Request::getString('oldCountry');

    if ($tabViewData{'check'} eq 'toCheck' && $id ne '')
    {
        #appel methode de verification
        my $tab = &_getEstimateInfo($countryId, $id);

        if ($tab->{'id'} != 0)
        {
            $tabViewData{'repairEstimateInfo'} = $tab;
            if ($tab->{'isUnInvoicingPossible'})
            {
                $tabViewData{'check'} = 'noError';
            }
            else
            {
                push(@tabCode, 'noPossible');
            }
        }
        else
        {
            push(@tabCode, 'errorNumEstimate');
        }
    }

    if ($tabViewData{'check'} eq 'toChange')
    {
        if ($id == $oldId)
        {
            if ($countryId == $oldCountry)
            {
                #appel methode de verification
                my $tab = &_getEstimateInfo($countryId, $id);
                $tabViewData{'repairEstimateInfo'} = $tab;
                if ($tab->{'isUnInvoicingPossible'})
                {
                    #appel methode de traitement
                    if (&_cancelInvoicing($countryId, $id))
                    {
                        #recuperation des nouvelles info
                        $tabViewData{'repairEstimateNewInfo'} = &_getEstimateInfo($countryId, $id);
                        
                        push(@tabCode, 'unInvoicing');
                    }
                    else
                    {
                        push(@tabCode, 'errorNoChange');
                    }
                }
                else
                {
                    push(@tabCode, 'noPossible');
                }
            }
            else
            {
                push(@tabCode, 'errorCountryChange');
            }
        }
        else
        {
            push(@tabCode, 'errorIdEstimateChange');
        }
    }

    $tabViewData{'id'} = $id;
    $tabViewData{'country'} = $countryId;
    $tabViewData{'codeReturn'} = \@tabCode;
    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/repair/cancelEstimateInvoicing.cgi';
}

sub reactivateEstimateAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my @tabCode = ();
    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );
    
    $tabViewData{'countryList'} = &LOC::Country::getList(LOC::Util::GETLIST_PAIRS);
    $tabViewData{'check'}       = &LOC::Request::getString('check', '');
    
    my $countryId      = &LOC::Request::getString('countryId', 'FR');
    my $id             = &LOC::Request::getInteger('repairEstimateId', 0);
    my $oldId          = &LOC::Request::getInteger('oldId', 0);
    my $oldCountry     = &LOC::Request::getString('oldCountry');

    if ($tabViewData{'check'} eq 'toCheck' && $id ne '')
    {
        #appel methode de verification
        my $tab = &_getEstimateInfo($countryId, $id);
        if ($tab->{'id'} != 0)
        {
            $tabViewData{'repairEstimateInfo'} = $tab;
            $tabViewData{'repairSheetInfo'} = &_getSheetInfo($countryId, $tab->{'sheet.id'});
            if ($tab->{'isReactivable'})
            {
                $tabViewData{'check'} = 'noError';
            }
            else
            {
                push(@tabCode, 'noPossible');
            }
        }
        else
        {
            push(@tabCode, 'errorNumEstimate');
        }
    }

    if ($tabViewData{'check'} eq 'toChange')
    {
        if ($id == $oldId)
        {
            if ($countryId == $oldCountry)
            {
                #appel methode de verification
                my $tab = &_getEstimateInfo($countryId, $id);
                $tabViewData{'repairEstimateInfo'} = $tab;
                $tabViewData{'repairSheetInfo'} = &_getSheetInfo($countryId, $tab->{'sheet.id'});
                if ($tab->{'isReactivable'})
                {
                    #appel methode de traitement
                    if (&_reactivate($countryId, $id, $tab->{'sheet.id'}))
                    {
                        #recuperation des nouvelles info
                        $tabViewData{'repairEstimateNewInfo'} = &_getEstimateInfo($countryId, $id);
                        
                        $tabViewData{'repairSheetNewInfo'} = &_getSheetInfo($countryId, $tabViewData{'repairEstimateNewInfo'}->{'sheet.id'});
                        push(@tabCode, 'estimateToActive');
                    }
                    else
                    {
                        push(@tabCode, 'errorNoChange');
                    }
                }
                else
                {
                    push(@tabCode, 'noPossible');
                }
            }
            else
            {
                push(@tabCode, 'errorCountryChange');
            }
        }
        else
        {
            push(@tabCode, 'errorIdEstimateChange');
        }
    }

    $tabViewData{'id'} = $id;
    $tabViewData{'country'} = $countryId;
    $tabViewData{'codeReturn'} = \@tabCode;

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/repair/reactivateEstimate.cgi';
}

sub _getEstimateInfo
{
    my ($countryId, $id) = @_;
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $query = '
SELECT
    rpe_id AS `id`,
    rps_id AS `sheet.id`,
    rpe_code AS `code`,
    rpe_sta_id AS `state.id`,
    tbl_f_state.ETLIBELLE AS  `state.label`,
    rpe_flag AS `flag`,
    IF(rpi_date, 0, IF(rpi_invoicingid, 0 , IF(rpi_date_invoicing, 0, 1))) AS `isUnInvoicing`,
    rpi_id AS  `rpi.id`,
    rpe_agc_id AS `agency`
FROM f_repairestimate 
LEFT JOIN f_repairinvoice ON rpe_id = rpi_rpe_id
LEFT JOIN ETATTABLE tbl_f_state ON rpe_sta_id = tbl_f_state.ETCODE
INNER JOIN f_repairsheet on rpe_rps_id = rps_id
WHERE rpe_id = ' . $id . ';';

    my $tab = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    # Formatage des données
    $tab->{'id'}                   *= 1;
    $tab->{'sheet.id'}             *= 1;
    $tab->{'rpi.id'}               *= 1;
    $tab->{'isUnInvoicing'}        *= 1;
    $tab->{'turnInto'}              = undef;
    $tab->{'isUnInvoicingPossible'} = 0;
    $tab->{'isReactivable'} = 0;
    
    if (defined $tab->{'flag'} && $tab->{'flag'} == 1)
    {
        $tab->{'turnInto'} = 'Bascule en services';
    }
    elsif (defined $tab->{'flag'} && $tab->{'flag'} == 2)
    {
        $tab->{'turnInto'} = 'Bascule en jours';
    }
    
    if ($tab->{'isUnInvoicing'} == 1 && $tab->{'state.id'} eq LOC::Repair::Estimate::STATE_APPROVED)
    {
        $tab->{'isUnInvoicingPossible'} = 1;
    }
    
    if (!defined $tab->{'flag'} && $tab->{'state.id'} eq LOC::Repair::Estimate::STATE_ABORTED)
    {
        $tab->{'isReactivable'} = 1;
    }

    return (wantarray ? %$tab : $tab);
}

sub _getSheetInfo
{
    my ($countryId, $id) = @_;
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $query = '
SELECT
    rps_id AS `id`,
    rps_sta_id AS `state.id`,
    rps_flag AS `flag`,
    tbl_f_state.ETLIBELLE AS `state.label`
FROM f_repairsheet
LEFT JOIN ETATTABLE tbl_f_state ON rps_sta_id = tbl_f_state.ETCODE
WHERE rps_id = ' . $id . ';';

    my $tab = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    # Formatage des données
    $tab->{'id'}                   *= 1;

    if (defined $tab->{'flag'} && $tab->{'flag'} == 0)
    {
        $tab->{'turnInto'} = 'Bascule en devis';
    }
    elsif (defined $tab->{'flag'} && $tab->{'flag'} == 1)
    {
        $tab->{'turnInto'} = 'Bascule en services';
    }
    elsif (defined $tab->{'flag'} && $tab->{'flag'} == 2)
    {
        $tab->{'turnInto'} = 'Bascule en jours';
    }
    return (wantarray ? %$tab : $tab);
}

sub _cancelInvoicing
{
    my ($countryId, $id) = @_;
    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }
    
    if ($db->delete('f_repairinvoice', {'rpi_rpe_id*' => $id}, $schemaName) == -1)
    {
            # Annule la transaction
            if (!$hasTransaction)
            {
                $db->rollBack();
            }
            return 0;
    }
    
    if ($db->update('f_repairestimate', {'rpe_sta_id' => LOC::Repair::Estimate::STATE_WAITING}, {'rpe_id*' => $id}, $schemaName) == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }
    
    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }
    return 1;
}

sub _reactivate
{
    my ($countryId, $estimateId, $sheetId) = @_;
    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }
    
    if ($db->update('f_repairestimate', {'rpe_sta_id' => LOC::Repair::Estimate::STATE_WAITING}, {'rpe_id*' => $estimateId}, $schemaName) == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }
    
    if ($db->update('f_repairsheet', {'rps_sta_id' => LOC::Repair::Sheet::STATE_TURNEDIN, 'rps_flag' => LOC::Repair::Sheet::FLAG_TURNEDINESTIMATE}, {'rps_id*' => $sheetId}, $schemaName) == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }
    
    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }
    return 1;
}

1;