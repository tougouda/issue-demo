use utf8;
use open (':encoding(UTF-8)');

# Contrôleur de gestion du support technique de la GesLoc
package telematics;

use strict;
use File::Basename;
use LOC::Country;
use LOC::Db;
use LOC::StartCode;


sub execProceduresAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    my $countryId  = &LOC::Request::getString('countryId', $tabUserInfos->{'country.id'});

    $tabViewData{'procedureId'} = &LOC::Request::getString('procedureId', undef);

    $tabViewData{'countryList'} = &LOC::Country::getList(LOC::Util::GETLIST_PAIRS);
    $tabViewData{'countryId'} = $countryId;

    if ($tabViewData{'procedureId'} eq 'boxDeclaration' && $tabViewData{'countryId'})
    {
        my $command = 'php ' . &LOC::Globals::get('servicesPath') . '/exec/cron/telematics/machinesExternalIds.php output=1 country=' . $tabViewData{'countryId'} .
            ' && cd ' . &LOC::Globals::get('cgiPath') . '/cron/telematics && ./updateStartCodeRules.pl synchronizeAll';
        $tabViewData{'output'} = `$command`;
    }


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/telematics/execProcedures.cgi';
}


sub execServicesAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    my $countryId  = &LOC::Request::getString('countryId', $tabUserInfos->{'country.id'});

    $tabViewData{'serviceId'} = &LOC::Request::getString('serviceId', undef);

    $tabViewData{'countryList'} = &LOC::Country::getList(LOC::Util::GETLIST_PAIRS);
    $tabViewData{'countryId'} = $countryId;


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/telematics/execServices.cgi';
}


sub codesStatsAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    my $countryId  = &LOC::Request::getString('countryId', $tabUserInfos->{'country.id'});

    # Récupération des stats sur les codes réservés
    # ------------------------------------------------

    # Liste des codes non disponibles globalement
    my $tabUnavailableCodes = &LOC::StartCode::getUnavailableCodes($countryId);

    my $nbTotal = 10 ** LOC::StartCode::CODE_LENGTH;
    my $nbUnavailable = @$tabUnavailableCodes;
    $tabViewData{'static'} = {
        'nbUnavailable'   => $nbUnavailable,
        'nbAvailable'     => $nbTotal - $nbUnavailable,
        'unavailableList' => $tabUnavailableCodes
    };

    $tabViewData{'worstMachines'} = {
        'count' => 0,
        'list'  => []
    };

    # Récupération de la liste des machines équipées pour la télématique
    my $tabMachines = &LOC::Machine::getList($countryId, LOC::Util::GETLIST_ASSOC, {
        'isTelematic' => 1
    });
    while (my ($machineId, $tabMachine) = each(%$tabMachines))
    {
        # Liste des codes non disponibles sur cette machine
        my $tabUnavailableCodes = &LOC::StartCode::getUnavailableCodes($countryId, $machineId);

        my $nbUnavailable = @$tabUnavailableCodes;
        if ($nbUnavailable > $tabViewData{'worstMachines'}->{'count'})
        {
            $tabViewData{'worstMachines'}->{'count'} = $nbUnavailable;
            $tabViewData{'worstMachines'}->{'list'} = [];
        }

        if ($nbUnavailable == $tabViewData{'worstMachines'}->{'count'})
        {
            push(@{$tabViewData{'worstMachines'}->{'list'}}, $tabMachine->{'parkNumber'});
        }
    }



    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/telematics/codesStats.cgi';
}

1;