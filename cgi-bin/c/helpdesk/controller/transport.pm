use utf8;
use open (':encoding(UTF-8)');

# Package: transport
# Contrôleur de gestion du support technique de la GesLoc
package transport;

use strict;
use File::Basename;
use LOC::Country;
use LOC::Area;
use LOC::Db;
use LOC::Json;
use LOC::Util;
use LOC::Kimoce;
use LOC::Transport;
use LOC::TransportRequest;


# Function launchRenew
# Lance la mise à jour des transports
sub launchRenewAction
{
    my $tabParameters = $_[0];

    my $tabSession             = &LOC::Session::getInfos();
    my $tabUserInfos           = $tabParameters->{'tabUserInfos'};
    my $countryId              = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $ignoreHash             = &LOC::Request::getInteger('ignoreHash', 0);
    my $isOnlySelectedCountry  = &LOC::Request::getInteger('isOnlySelectedCountry', 0);

    my $tabTransports;

    # Variables pour la vue
    our %tabViewData;
    # Lancement de la mise à jour
    if ($isOnlySelectedCountry == 1)
    {
        # Mise à jour de tous les pays
        if ($countryId eq 'TS')
        {
            my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);
            foreach my $countryId (@$tabCountries)
            {
                $tabTransports = &LOC::Transport::getList($countryId,
                                                          LOC::Util::GETLIST_IDS,
                                                          {
                                                              'stateId' => LOC::Transport::STATE_ACTIVE
                                                          });
                $tabViewData{'requestId'}->{$countryId} = &LOC::Transport::launchRenew($countryId, $tabTransports, $ignoreHash, $tabUserInfos);
            }
        }
        else
        {
            # Mise à jour du pays sélectionné
            $tabTransports = &LOC::Transport::getList($countryId,
                                                      LOC::Util::GETLIST_IDS,
                                                      {
                                                          'stateId' => LOC::Transport::STATE_ACTIVE
                                                      });
            $tabViewData{'requestId'}->{$countryId} = &LOC::Transport::launchRenew($countryId, $tabTransports, $ignoreHash, $tabUserInfos);
        }
    }
    else
    {
        # Mise à jour des transports sélectionnés
        $tabTransports = &LOC::Request::getArray('tabTransports', []);
        $tabViewData{'requestId'}->{$countryId} = &LOC::Transport::launchRenew($countryId, $tabTransports, $ignoreHash, $tabUserInfos);
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/transport/launchRenew.cgi';
}

# Function: checkProgressionAction
# Vérification de la progression de la mise à jour des transports
sub checkProgressionAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $requestId    =  &LOC::Request::getString('requestId');
    my $countryId    = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});

    # Variables pour la vue
    our %tabViewData;
    # Récupération des informations de la requête
    my $tabRequestInfos;

    # Cas de tous les pays
    if ($countryId eq 'TS')
    {
        foreach my $countryId (keys(%$requestId))
        {
            $tabRequestInfos = &LOC::TransportRequest::getInfos($countryId, $requestId->{$countryId});
            $tabViewData{'progression'}->{$requestId->{$countryId}}         = $tabRequestInfos->{'progression'};
            $tabViewData{'stateId'}->{$requestId->{$countryId}}             = $tabRequestInfos->{'state.id'};
            $tabViewData{'treatedTransportsNo'}->{$requestId->{$countryId}} = $tabRequestInfos->{'treatedTransportsNo'};
            $tabViewData{'tabResult'}->{$requestId->{$countryId}}           = $tabRequestInfos->{'tabResult'};
        }
    }
    else
    {
        foreach my $countryId (keys(%$requestId))
        {
        
            $tabRequestInfos = &LOC::TransportRequest::getInfos($countryId, $requestId->{$countryId});
            $tabViewData{'progression'}->{$requestId->{$countryId}}         = $tabRequestInfos->{'progression'};
            $tabViewData{'stateId'}->{$requestId->{$countryId}}             = $tabRequestInfos->{'state.id'};
            $tabViewData{'treatedTransportsNo'}->{$requestId->{$countryId}} = $tabRequestInfos->{'treatedTransportsNo'};
            $tabViewData{'tabResult'}->{$requestId->{$countryId}}           = $tabRequestInfos->{'tabResult'};
        }
    }
    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/transport/checkProgression.cgi';
}


sub updateOrderAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
        'country.id'     => $tabUserInfos->{'nomadCountry.id'}
    );

    # Récupération des informations
    my $validVerif = &LOC::Request::getString('validVerif', '');
    my $activeTabIndex = &LOC::Request::getInteger('activeTabIndex', 0);
    my $validUnlock = &LOC::Request::getString('validUnlock', '');
    my $tspToUnlock = &LOC::Request::getString('tspToUnlock', '');
    
    # Récupération des informations
    my $tabData = {
        'country.id'                       => &LOC::Request::getString('country.id', 'FR'),
        'agencies.id'                      => &LOC::Request::getString('selectedAgencies', ''),

        'contract.search.no'               => &LOC::Request::getString('contract.search.no'),
        'contract.search.state.id'         => &LOC::Request::getString('contract.search.state.id'),
        'contract.search.beginDate'        => &LOC::Request::getString('contract.search.beginDate'),
        'contract.search.endDate'          => &LOC::Request::getString('contract.search.endDate'),
        'contract.search.parkNo'           => &LOC::Request::getString('contract.search.parkNo'),
        'contract.search.site.label'       => &LOC::Request::getString('contract.search.site.label'),
        'contract.search.site.department'  => &LOC::Request::getString('contract.search.site.department'),
        'contract.search.user.id'          => &LOC::Request::getInteger('contract.search.user.id'),
        'contract.search.negotiator.id'    => &LOC::Request::getInteger('contract.search.negotiator.id'),
        'contract.search.customer.id'      => &LOC::Request::getInteger('contract.search.customer.id'),

        'estimateLine.search.no'               => &LOC::Request::getString('estimateLine.search.no'),
        'estimateLine.search.state.id'         => &LOC::Request::getString('estimateLine.search.state.id'),
        'estimateLine.search.beginDate'        => &LOC::Request::getString('estimateLine.search.beginDate'),
        'estimateLine.search.endDate'          => &LOC::Request::getString('estimateLine.search.endDate'),
        'estimateLine.search.site.label'       => &LOC::Request::getString('estimateLine.search.site.label'),
        'estimateLine.search.site.department'  => &LOC::Request::getString('estimateLine.search.site.department'),
        'estimateLine.search.user.id'          => &LOC::Request::getInteger('estimateLine.search.user.id'),
        'estimateLine.search.negotiator.id'    => &LOC::Request::getInteger('estimateLine.search.negotiator.id'),
        'estimateLine.search.customer.id'      => &LOC::Request::getInteger('estimateLine.search.customer.id'),

        'agenciesTransfer.search.parkNo'        => &LOC::Request::getString('agenciesTransfer.search.parkNo'),
        'agenciesTransfer.search.transferDate'  => &LOC::Request::getString('agenciesTransfer.search.transferDate'),
        'agenciesTransfer.search.agencyFrom.id' => &LOC::Request::getString('agenciesTransfer.search.agencyFrom.id'),
        'agenciesTransfer.search.agencyTo.id'   => &LOC::Request::getString('agenciesTransfer.search.agencyTo.id')
    };

    # Mise en forme des agences sélectionnées
    my @tabSelectedAgencies;
    if ($tabData->{'agencies.id'} ne '')
    {
        @tabSelectedAgencies = split(',', $tabData->{'agencies.id'});
        $tabData->{'agencies.id'} = \@tabSelectedAgencies;
    }
    else
    {
        $tabData->{'agencies.id'} = undef;
    }

    # Mise en forme des transports à déverrouiller
    my @tabTspToUnlock;
    if ($tspToUnlock ne '')
    {
        @tabTspToUnlock = split(',', $tspToUnlock);
    }

    # Lancement du déverrouillage des transports sélectionnés
    if ($validUnlock eq 'ok')
    {
        foreach my $tspId (@tabTspToUnlock)
        {
            if (!&LOC::Transport::unlock($tabData->{'country.id'}, {'id' => $tspId}, $tabUserInfos->{'id'}, undef))
            {
                $tabViewData{'unlockError'} = 1;
            }
        }
        if (!$tabViewData{'unlockError'})
        {
            $tabViewData{'unlockSuccess'} = 1;
        }
    }

    # Lancement de la recherche
    if ($validVerif eq 'ok')
    {

        # Informations concernant le contrat
        $tabData->{'contract.users'} = ();
        if ($tabData->{'contract.search.user.id'} != 0)
        {
            $tabData->{'contract.users'} = &LOC::User::getList(LOC::Util::GETLIST_PAIRS,
                                               {'id' => $tabData->{'contract.search.user.id'}},
                                               LOC::User::GET_ACTIVE,
                                               {'formatMode' => 1});
        }

        $tabData->{'contract.negotiators'} = ();
        if ($tabData->{'contract.search.negotiator.id'} != 0)
        {
            $tabData->{'contract.negotiators'} = &LOC::User::getList(LOC::Util::GETLIST_PAIRS,
                                                      {'id' => $tabData->{'contract.search.negotiator.id'}},
                                                      LOC::User::GET_ACTIVE,
                                                      {'formatMode' => 1});
        }

        $tabData->{'contract.customers'} = ();
        if ($tabData->{'contract.search.customer.id'} != 0)
        {
            $tabData->{'contract.customers'} = &LOC::Customer::getList($tabData->{'country.id'}, LOC::Util::GETLIST_PAIRS,
                                                   {'id' => $tabData->{'contract.search.customer.id'}});
        }

        # Informations concernant le devis
        $tabData->{'estimateLine.users'} = ();
        if ($tabData->{'estimateLine.search.user.id'} != 0)
        {
            $tabData->{'estimateLine.users'} = &LOC::User::getList(LOC::Util::GETLIST_PAIRS,
                                               {'id' => $tabData->{'estimateLine.search.user.id'}},
                                               LOC::User::GET_ACTIVE,
                                               {'formatMode' => 1});
        }
        $tabData->{'estimateLine.negotiators'} = ();
        if ($tabData->{'estimateLine.search.negotiator.id'} != 0)
        {
            $tabData->{'estimateLine.negotiators'} = &LOC::User::getList(LOC::Util::GETLIST_PAIRS,
                                                     {'id' => $tabData->{'estimateLine.search.negotiator.id'}},
                                                     LOC::User::GET_ACTIVE,
                                                     {'formatMode' => 1});
        }
        $tabData->{'estimateLine.customers'} = ();
        if ($tabData->{'estimateLine.search.customer.id'} != 0)
        {
            $tabData->{'estimateLine.customers'} = &LOC::Customer::getList($tabData->{'country.id'}, LOC::Util::GETLIST_PAIRS,
                                                   {'id' => $tabData->{'estimateLine.search.customer.id'}});
        }

        # Tableau des résultats de la recherche pour l'onglet Contrat
        my $tabSearchContract = &_searchContract($tabData);
        # Tableau des résultats de la recherche pour l'onglet Lignes de devis
        my $tabSearchEstimateLine = &_searchEstimateLine($tabData, $tabSearchContract->[1]);
        # Tableau des résultats de la recherche pour l'onglet Transferts inter-agences
        my $tabSearchAgenciesTransfer = &_searchAgenciesTransfer($tabData, $tabSearchContract->[1] + $tabSearchEstimateLine->[1]);

        # Nombre de critères totaux sélectionnés
        $tabViewData{'nbFilters'} = $tabSearchContract->[0] + $tabSearchEstimateLine->[0] + $tabSearchAgenciesTransfer->[0];

        # Si aucun critère de recherche sélectionné : lancement de la recherche sur le pays sélectionné
        if ($tabViewData{'nbFilters'} == 0)
        {
            # Tableau des résultats de la recherche pour l'onglet Contrat
            my $nbResultTotal = &_searchCountries($tabData->{'country.id'});
            $tabViewData{'nbResult'} = $nbResultTotal;
        }
        else
        {
            # Si tous les pays sont sélectionnés avec des critères de recherche : la recherche ne se lance pas
            if (($tabSearchContract->[3] == 0) || ($tabSearchEstimateLine->[3] == 0) || ($tabSearchAgenciesTransfer->[3] == 0))
            {
                $tabViewData{'nbResult'} = 'noSearch';
            }
            else
            {
                $tabViewData{'nbResult'} = $tabSearchContract->[1] + $tabSearchEstimateLine->[1] + $tabSearchAgenciesTransfer->[1];
            }
            # Tableau final correspondant à l'ensemble des critères de recherche sur l'ensemble des onglets
            my %tabResult = (%{$tabSearchContract->[2]}, %{$tabSearchEstimateLine->[2]}, %{$tabSearchAgenciesTransfer->[2]}); 
            $tabViewData{'result'} = \%tabResult;
        }
    }

    # Données pour affichage
    # Liste des pays
    $tabViewData{'countryList'} = &LOC::Country::getList(LOC::Util::GETLIST_PAIRS);
    # Liste des régions
    $tabViewData{'areaList'} = &LOC::Area::getList(LOC::Util::GETLIST_PAIRS);
    # Liste des agences
    $tabViewData{'agencyList'} = &LOC::Agency::getList(LOC::Util::GETLIST_PAIRS);
    # Liste des états du contrat
    $tabViewData{'contractStateList'} = &LOC::Contract::Rent::getStates($tabData->{'country.id'});
    # Liste des états du devis
    $tabViewData{'estimateStateList'} = &LOC::Estimate::Rent::getStates($tabData->{'country.id'});
    # Liste des localités
    $tabViewData{'siteLocalitiesSearchUrl'} = &LOC::Request::createRequestUri('site:site:siteLocalitiesList',
                                                                              {'country.id'      => $tabData->{'country.id'},
                                                                               'format'          => 0});


    # Récupération des données des agences et des pays (pour le choix des agences)
    my $tabCountryInfos = &LOC::Country::getInfos($tabData->{'country.id'});
    my $tabAgencies     = &LOC::Agency::getList(LOC::Util::GETLIST_ASSOC, {'country' => $tabData->{'country.id'}});
    my $tabAreas        = &LOC::Area::getList(LOC::Util::GETLIST_ASSOC, {'country' => $tabData->{'country.id'}});

    my $tabCountriesInfos = [];
    my $tabAr = [];
    while (my ($areaId, $tabAreaInfos) = each(%$tabAreas))
    {
        my $tabAg = [];
        while (my ($agencyId, $tabAgencyInfos) = each(%$tabAgencies))
        {
            if ($tabAgencyInfos->{'area.id'} eq $areaId)
            {
                push(@$tabAg, {'id' => $agencyId, 'label' => $tabAgencyInfos->{'label'}});
            } 
        }

        push(@$tabAr, {'id' => $areaId, 'label' => $tabAreaInfos->{'label'}, 'tabAgencies' => $tabAg});
    }

    my $tabAg = [];
    while (my ($agencyId, $tabAgencyInfos) = each(%$tabAgencies))
    {
        if ($tabAgencyInfos->{'area.id'} eq '')
        {
            push(@$tabAg, {'id' => $agencyId, 'label' => $tabAgencyInfos->{'label'}});
        } 
    }
    my $nbAg = @$tabAg;
    if ($nbAg > 0)
    {
        push(@$tabAr, {'id' => 0, 'label' => '', 'tabAgencies' => $tabAg});
    }

    push(@$tabCountriesInfos, {'id' => $tabData->{'country.id'}, 'label' => $tabCountryInfos->{'label'}, 'tabAreas' => $tabAr});
    $tabViewData{'tabCountriesInfos'} = $tabCountriesInfos;

    $tabViewData{'searchCountry'} = $tabData->{'country.id'};

    # Onglet Actif
    $tabViewData{'activeTabIndex'} = $activeTabIndex;

    $tabViewData{'tabData'} = $tabData;

    # Recherche d'une mise à jour en cours d'exécution
    my $tabRequestParams = {'stateId'    => [LOC::InvoicingRequest::STATE_CREATED,
                                             LOC::InvoicingRequest::STATE_PROGRESS
                                            ]
                               };
    my $tabRequest;
    if ($tabViewData{'searchCountry'} eq 'TS')
    {
        my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);
        foreach my $countryId (@$tabCountries)
        {
            $tabRequest = &LOC::TransportRequest::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabRequestParams);
            if (keys(%$tabRequest) > 0)
            {
                $tabViewData{'requestId'}->{$countryId} = (keys(%$tabRequest))[0];
            }
        }
    }
    else
    {
        $tabRequest= &LOC::TransportRequest::getList($tabViewData{'searchCountry'}, LOC::Util::GETLIST_ASSOC, $tabRequestParams);
        if (keys(%$tabRequest) > 0)
        {
            $tabViewData{'requestId'}->{$tabViewData{'searchCountry'}} = (keys(%$tabRequest))[0];
        }
    }
    

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;

    require $directory . '/view/transport/updateOrder.cgi';
}


sub _searchContract
{
    my ($tabData) = @_;

    my $tabRows = {};

    # Etats contrat
    my @tabState = ([LOC::Contract::Rent::STATE_PRECONTRACT, LOC::Contract::Rent::STATE_CONTRACT], 
                    [LOC::Contract::Rent::STATE_STOPPED, LOC::Contract::Rent::STATE_CANCELED, LOC::Contract::Rent::STATE_BILLED]);


    # Critères de recherche pour l'onglet Contrat
    my $tabContractFilters = {};
    if ($tabData->{'contract.search.no'} ne '')
    {
        my $codeSearch = $tabData->{'contract.search.no'};
        if ($codeSearch !~ /[A-Za-z]{3}\d{10,}/)
        {
            $codeSearch = '%' . $codeSearch;
        }
        $tabContractFilters->{'code'} = $codeSearch;
    }
    if ($tabData->{'contract.search.state.id'} ne '')
    {
        $tabContractFilters->{'stateId'} = $tabData->{'contract.search.state.id'};
    }
    if ($tabData->{'contract.search.user.id'} != 0)
    {
        $tabContractFilters->{'userId'} = $tabData->{'contract.search.user.id'};
    }
    if ($tabData->{'contract.search.customer.id'} != 0)
    {
        $tabContractFilters->{'customerId'} = $tabData->{'contract.search.customer.id'};
    }
    if ($tabData->{'contract.search.parkNo'} ne '')
    {
        $tabContractFilters->{'parkNo'} = $tabData->{'contract.search.parkNo'};
    }
    if ($tabData->{'contract.search.site.label'} ne '')
    {
        my $search = $tabData->{'contract.search.site.label'};
        if ($search !~ /%/)
        {
            $search = '%' . $search . '%';
        }
        $tabContractFilters->{'siteLabel'} = $search;
    }
    if ($tabData->{'contract.search.site.department'} ne '')
    {
        $tabContractFilters->{'siteDepartment'} = $tabData->{'contract.search.site.department'};
    }
    if ($tabData->{'contract.search.beginDate'} ne '')
    {
        $tabContractFilters->{'beginDate'} = $tabData->{'contract.search.beginDate'};
    }
    if ($tabData->{'contract.search.endDate'} ne '')
    {
        $tabContractFilters->{'endDate'} = $tabData->{'contract.search.endDate'};
    }
    if ($tabData->{'contract.search.negotiator.id'} != 0)
    {
        $tabContractFilters->{'negotiatorId'} = $tabData->{'contract.search.negotiator.id'};
    }
    
    # Nombre de critères sélectionnés
    my $nbContractFilters = keys %$tabContractFilters;

    # Nombre de résultats
    my $nbResult = 0;

    # Si tous les pays sont sélectionnés : la recherche ne se lance pas
    my $isSelectedCountry = 0;
    if ($tabData->{'country.id'} ne 'TS')
    {
        $isSelectedCountry = 1;
    }

    # s'il y a assez de critères sélectionnés
    if ($nbContractFilters > 0 && $isSelectedCountry == 1)
    {

        # Le filtre agence ne compte pas dans le nombre de critères sélectionnés
        if ($tabData->{'agencies.id'})
        {
            $tabContractFilters->{'agencyId'} = $tabData->{'agencies.id'};
        }
        $nbResult = &LOC::Contract::Rent::getList($tabData->{'country.id'}, LOC::Util::GETLIST_COUNT, $tabContractFilters);

        if ($nbResult > 0 && $nbResult <= 100)
        {
            my $tabContracts = &LOC::Contract::Rent::getList($tabData->{'country.id'}, LOC::Util::GETLIST_ASSOC,
                                                             $tabContractFilters,
                                                             &LOC::Contract::Rent::GETINFOS_BASE |
                                                             &LOC::Contract::Rent::GETINFOS_CUSTOMER |
                                                             &LOC::Contract::Rent::GETINFOS_EXT_SITE |
                                                             &LOC::Contract::Rent::GETINFOS_CREATOR | 
                                                             &LOC::Contract::Rent::GETINFOS_MACHINE);

            # Récupération des transports associés à chacun des contrats
            foreach my $tabContractInfos (values %$tabContracts)
            {
                my $tabTransportInfos = &LOC::Transport::getList($tabData->{'country.id'}, LOC::Util::GETLIST_ASSOC,
                               {'from-to' => {'type' => LOC::Transport::FROMTOTYPE_CONTRACT, 'id' => $tabContractInfos->{'id'}}}, LOC::Transport::GETINFOS_ALL);

               # Remplir le tableau des résultats
               $tabRows->{$tabContractInfos->{'id'}} =  {
                   'document.id'         => $tabContractInfos->{'id'},
                   'type'                => 'contract',
                   'document.code'       => $tabContractInfos->{'code'},
                   'machine.parkNumber'  => $tabContractInfos->{'machine.parkNumber'},
                   'machine.url'         => &LOC::Machine::getUrl($tabContractInfos->{'machine.id'}),
                   'agency.id'           => $tabContractInfos->{'agency.id'},
                   'beginDate'           => $tabContractInfos->{'beginDate'},
                   'orderDelivery'       => undef,
                   'endDate'             => $tabContractInfos->{'endDate'},
                   'orderRecovery'       => undef,
                   'agencyFrom'          => undef,
                   'agencyTo'            => undef,
                   'isDeliveryLocked'    => 0,
                   'isRecoveryLocked'    => 0
               };

               foreach my $tabTransportInfos(values %$tabTransportInfos)
               {
                   $tabRows->{$tabContractInfos->{'id'}}->{'isLocked'}  = $tabTransportInfos->{'isLocked'};
                   # Type Livraison
                   if ($tabTransportInfos->{'type'} eq LOC::Transport::TYPE_DELIVERY)
                   {
                       $tabRows->{$tabContractInfos->{'id'}}->{'orderDelivery'}  = $tabTransportInfos->{'order.tabInfos'}->{'code'};
                       $tabRows->{$tabContractInfos->{'id'}}->{'tspDelivery.id'} = $tabTransportInfos->{'id'};
                       $tabRows->{$tabContractInfos->{'id'}}->{'isDeliverySynchroDisabled'} = $tabTransportInfos->{'isSynchroDisabled'};
                       $tabRows->{$tabContractInfos->{'id'}}->{'isDeliveryLocked'} = $tabTransportInfos->{'isLocked'};
                   }
                   # Type Récupération
                   elsif ($tabTransportInfos->{'type'} eq LOC::Transport::TYPE_RECOVERY)
                   {
                       $tabRows->{$tabContractInfos->{'id'}}->{'orderRecovery'}  = $tabTransportInfos->{'order.tabInfos'}->{'code'};
                       $tabRows->{$tabContractInfos->{'id'}}->{'tspRecovery.id'} = $tabTransportInfos->{'id'};
                       $tabRows->{$tabContractInfos->{'id'}}->{'isRecoverySynchroDisabled'} = $tabTransportInfos->{'isSynchroDisabled'};
                       $tabRows->{$tabContractInfos->{'id'}}->{'isRecoveryLocked'} = $tabTransportInfos->{'isLocked'};
                   }
                   # Type transfert
                   elsif ($tabTransportInfos->{'type'} eq LOC::Transport::TYPE_SITESTRANSFER)
                   {
                       $tabRows->{$tabContractInfos->{'id'}}->{'isDeliveryLocked'} = $tabTransportInfos->{'isLocked'};
                       $tabRows->{$tabContractInfos->{'id'}}->{'isRecoveryLocked'} = $tabTransportInfos->{'isLocked'};
                       # Livraison
                       if (LOC::Util::in_array($tabContractInfos->{'state.id'}, @tabState[0]))
                       {
                           $tabRows->{$tabContractInfos->{'id'}}->{'orderDelivery'}  = $tabTransportInfos->{'order.tabInfos'}->{'code'};
                           $tabRows->{$tabContractInfos->{'id'}}->{'tspDelivery.id'} = $tabTransportInfos->{'id'};
                           $tabRows->{$tabContractInfos->{'id'}}->{'isDeliverySynchroDisabled'} = $tabTransportInfos->{'isSynchroDisabled'};
                       }
                       # Récupération
                       elsif (LOC::Util::in_array($tabContractInfos->{'state.id'}, @tabState[1]))
                       {
                           $tabRows->{$tabContractInfos->{'id'}}->{'orderRecovery'}  = $tabTransportInfos->{'order.tabInfos'}->{'code'};
                           $tabRows->{$tabContractInfos->{'id'}}->{'tspRecovery.id'} = $tabTransportInfos->{'id'};
                           $tabRows->{$tabContractInfos->{'id'}}->{'isRecoverySynchroDisabled'} = $tabTransportInfos->{'isSynchroDisabled'};
                       }
                       
                   }
                }
            }  
        }
    }

    return [$nbContractFilters, $nbResult, $tabRows, $isSelectedCountry];

}

sub _searchEstimateLine
{

    my ($tabData, $nbOtherTabsResult) = @_;

    my $tabRows = {};

    # Critères de recherche pour l'onglet Lignes de devis
    my $tabEstimateFilters = {};
    $tabData->{'estimateLine.search.no'} =~ s/([^0-9%]*)//g;
    if ($tabData->{'estimateLine.search.no'} ne '')
    {
        $tabEstimateFilters->{'id'} = $tabData->{'estimateLine.search.no'};
    }
    if ($tabData->{'estimateLine.search.state.id'} ne '')
    {
        $tabEstimateFilters->{'estimateStateId'} = $tabData->{'estimateLine.search.state.id'};
    }
    if ($tabData->{'estimateLine.search.customer.id'} != 0)
    {
        $tabEstimateFilters->{'customerId'} = $tabData->{'estimateLine.search.customer.id'};
    }
    if ($tabData->{'estimateLine.search.user.id'} != 0)
    {
        $tabEstimateFilters->{'userId'} = $tabData->{'estimateLine.search.user.id'};
    }
    if ($tabData->{'estimateLine.search.site.label'} ne '')
    {
        my $search = $tabData->{'estimateLine.search.site.label'};
        if ($search !~ /%/)
        {
            $search = '%' . $search . '%';
        }
        $tabEstimateFilters->{'siteLabel'} = $search;
    }
    if ($tabData->{'estimateLine.search.site.department'} ne '')
    {
        $tabEstimateFilters->{'siteDepartment'} = $tabData->{'estimateLine.search.site.department'};
    }
    if ($tabData->{'estimateLine.search.beginDate'} ne '')
    {
        $tabEstimateFilters->{'beginDate'} = $tabData->{'estimateLine.search.beginDate'};
    }
    if ($tabData->{'estimateLine.search.endDate'} ne '')
    {
        $tabEstimateFilters->{'endDate'} = $tabData->{'estimateLine.search.endDate'};
    }
    if ($tabData->{'estimateLine.search.negotiator.id'} != 0)
    {
        $tabEstimateFilters->{'negotiatorId'} = $tabData->{'estimateLine.search.negotiator.id'};
    }

    # Nombre de critères sélectionnés
    my $nbEstimateFilters = keys %$tabEstimateFilters;

    # Nombre de résultats
    my $nbResult = 0;

    # Si tous les pays sont sélectionnés : la recherche ne se lance pas
    my $isSelectedCountry = 0;
    if ($tabData->{'country.id'} ne 'TS')
    {
        $isSelectedCountry = 1;
    }

    # s'il y a assez de critères sélectionnés
    if ($nbEstimateFilters > 0 && $isSelectedCountry == 1)
    {
        # Le filtre "agence" ne compte pas dans le nombre de critères sélectionnés
        if ($tabData->{'agencies.id'})
        {
            $tabEstimateFilters->{'agencyId'} = $tabData->{'agencies.id'};
        }

        $nbResult = &LOC::Estimate::Rent::getLinesList($tabData->{'country.id'}, LOC::Util::GETLIST_COUNT,
                                                       $tabEstimateFilters);

        my $nbTotalResult = $nbOtherTabsResult + $nbResult;

        if ($nbResult > 0 && $nbTotalResult <= 100)
        {
            my $tabEstimateLines = &LOC::Estimate::Rent::getLinesList($tabData->{'country.id'}, LOC::Util::GETLIST_ASSOC, 
                                                                      $tabEstimateFilters,
                                                                      LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE |
                                                                      LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE_SITE |
                                                                      LOC::Estimate::Rent::GETLINEINFOS_INDEX |
                                                                      LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE_CREATOR |
                                                                      LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE_CUSTOMER);

            # Récupération des transports associées à chacune des lignes de devis
            foreach my $tabEstimateLinesInfos (values %$tabEstimateLines)
            {
                my $tabTransportInfos = &LOC::Transport::getList($tabData->{'country.id'}, LOC::Util::GETLIST_ASSOC,
                               {'from-to' => {'type' => LOC::Transport::FROMTOTYPE_ESTIMATELINE, 'id' => $tabEstimateLinesInfos->{'id'}}}, LOC::Transport::GETINFOS_ALL);

               # Remplir le tableau des résultats
               $tabRows->{$tabEstimateLinesInfos->{'id'}} =  {
                   'document.id'         => [$tabEstimateLinesInfos->{'estimate.id'}, $tabEstimateLinesInfos->{'id'}],
                   'type'                => 'estimateLine',
                   'document.code'       => [$tabEstimateLinesInfos->{'estimate.code'}, $tabEstimateLinesInfos->{'index'}],
                   'machine.parkNumber'  => undef,
                   'machine.url'         => undef,
                   'agency.id'           => $tabEstimateLinesInfos->{'estimate.agency.id'},
                   'beginDate'           => $tabEstimateLinesInfos->{'beginDate'},
                   'orderDelivery'       => undef,
                   'endDate'             => $tabEstimateLinesInfos->{'endDate'},
                   'orderRecovery'       => undef,
                   'agencyFrom'          => undef,
                   'agencyTo'            => undef,
                   'isDeliveryLocked'    => 0,
                   'isRecoveryLocked'    => 0
               };

               foreach my $tabTransportInfos(values %$tabTransportInfos)
               {
                   $tabRows->{$tabEstimateLinesInfos->{'id'}}->{'isLocked'}  = $tabTransportInfos->{'isLocked'};
                   # Type Livraison
                   if ($tabTransportInfos->{'type'} eq LOC::Transport::TYPE_DELIVERY)
                   {
                       $tabRows->{$tabEstimateLinesInfos->{'id'}}->{'orderDelivery'}  = $tabTransportInfos->{'order.tabInfos'}->{'code'};
                       $tabRows->{$tabEstimateLinesInfos->{'id'}}->{'tspDelivery.id'} = $tabTransportInfos->{'id'};
                       $tabRows->{$tabEstimateLinesInfos->{'id'}}->{'isDeliverySynchroDisabled'} = $tabTransportInfos->{'isSynchroDisabled'};
                       $tabRows->{$tabEstimateLinesInfos->{'id'}}->{'isDeliveryLocked'}  = $tabTransportInfos->{'isLocked'};
                   }
                   elsif ($tabTransportInfos->{'type'} eq LOC::Transport::TYPE_RECOVERY)
                   {
                       $tabRows->{$tabEstimateLinesInfos->{'id'}}->{'orderRecovery'}  = $tabTransportInfos->{'order.tabInfos'}->{'code'};
                       $tabRows->{$tabEstimateLinesInfos->{'id'}}->{'tspRecovery.id'} = $tabTransportInfos->{'id'};
                       $tabRows->{$tabEstimateLinesInfos->{'id'}}->{'isRecoverySynchroDisabled'} = $tabTransportInfos->{'isSynchroDisabled'};
                       $tabRows->{$tabEstimateLinesInfos->{'id'}}->{'isRecoveryLocked'} = $tabTransportInfos->{'isLocked'};
                   }
               }
            }
        }
    }

    return [$nbEstimateFilters, $nbResult, $tabRows, $isSelectedCountry];

}


sub _searchAgenciesTransfer
{
    my ($tabData, $nbOtherTabsResult) = @_;

    my $tabRows = {};

    # Critères de recherche pour l'onglet Transfert inter-agences
    my $tabAgenciesTransferFilters = {};

    # Critères de recherche sur le n°  de parc
    if ($tabData->{'agenciesTransfer.search.parkNo'} ne '')
    {
        my $tabMachineInfos = &LOC::Machine::getList($tabData->{'country.id'}, LOC::Util::GETLIST_PAIRS, 
                                                     {'parkNumber' => $tabData->{'agenciesTransfer.search.parkNo'}}, 
                                                     LOC::Machine::GETINFOS_BASE);

        # Initialisé afin de prendre en compte le critère n° parc même si la saisie est incorrecte
        $tabAgenciesTransferFilters->{'machineId'} = 0;
        # Vérification que la saisie du n° de parc correspond à une machine existante
        if (keys(%$tabMachineInfos) > 0)
        {
            $tabAgenciesTransferFilters->{'machineId'} = (keys(%$tabMachineInfos))[0];
        }
     }

    # Critères de recherche sur la date de transfert
    if ($tabData->{'agenciesTransfer.search.transferDate'} ne '')
    {
        $tabAgenciesTransferFilters->{'transferDate'} = $tabData->{'agenciesTransfer.search.transferDate'};
    }

    # Critères de recherche sur l'agence de provenance
    if ($tabData->{'agenciesTransfer.search.agencyFrom.id'} ne '')
    {
        $tabAgenciesTransferFilters->{'agencyIdFrom'}= $tabData->{'agenciesTransfer.search.agencyFrom.id'};
    }

    # Critères de recherche sur l'agence de destination
    if ($tabData->{'agenciesTransfer.search.agencyTo.id'} ne '')
    {
        $tabAgenciesTransferFilters->{'agencyIdTo'}= $tabData->{'agenciesTransfer.search.agencyTo.id'};
    }

    # Nombre de filtre sélectionné
    my $nbAgenciesTransferFilters = keys %$tabAgenciesTransferFilters;

    # Lancement de la recherche
    my $launchSearch = 1;
    # Si le n° de parc saisi est incorrect, on ne lance pas la recherche
    if (exists $tabAgenciesTransferFilters->{'machineId'} && ($tabAgenciesTransferFilters->{'machineId'} == 0))
    {
        $launchSearch = 0;
    }
    # Nombre de résultats
    my $nbResult = 0;

    # Si tous les pays sont sélectionnés : la recherche ne se lance pas
    my $isSelectedCountry = 0;
    if ($tabData->{'country.id'} ne 'TS')
    {
        $isSelectedCountry = 1;
    }

    if ($nbAgenciesTransferFilters > 0 && $launchSearch == 1 && $isSelectedCountry == 1)
    {
        $tabAgenciesTransferFilters->{'type'} = LOC::Transport::TYPE_AGENCIESTRANSFER;
        $tabAgenciesTransferFilters->{'isLocked'} = 0;

        $nbResult = &LOC::Transport::getList($tabData->{'country.id'}, LOC::Util::GETLIST_COUNT,
                                             $tabAgenciesTransferFilters);

        my $nbTotalResult = $nbOtherTabsResult + $nbResult;

        if ($nbResult > 0 && $nbTotalResult <= 100)
        {
            my $tabTransportInfos = &LOC::Transport::getList($tabData->{'country.id'}, LOC::Util::GETLIST_ASSOC,
                                                             $tabAgenciesTransferFilters, LOC::Transport::GETINFOS_ALL);

           # Remplir le tableau des résultats
           foreach my $tabTransportInfos (values %$tabTransportInfos)
           {
               # Récupération des informations de la machine
               my $tabMachineInfos = &LOC::Machine::getInfos($tabData->{'country.id'}, $tabTransportInfos->{'machine.id'}, 
                                                             LOC::Machine::GETINFOS_BASE);

               # Remplir le tableau des résultats
               $tabRows->{$tabTransportInfos->{'id'}} =  {
                   'document.id'               => undef,
                   'type'                      => 'agenciesTransfer',
                   'document.code'             => undef,
                   'machine.parkNumber'        => $tabMachineInfos->{'parkNumber'},
                   'machine.url'               => &LOC::Machine::getUrl($tabMachineInfos->{'id'}),
                   'agency.id'                 => $tabMachineInfos->{'agency.id'},
                   'beginDate'                 => $tabTransportInfos->{'date'},
                   'orderDelivery'             => $tabTransportInfos->{'order.tabInfos'}->{'code'},
                   'endDate'                   => $tabTransportInfos->{'date'},
                   'orderRecovery'             => $tabTransportInfos->{'order.tabInfos'}->{'code'},
                   'agencyFrom'                => $tabTransportInfos->{'from.id'},
                   'agencyTo'                  => $tabTransportInfos->{'to.id'},
                   'tspDelivery.id'            => $tabTransportInfos->{'id'},
                   'isDeliverySynchroDisabled' => $tabTransportInfos->{'isSynchroDisabled'},
                   'isRecoverySynchroDisabled' => $tabTransportInfos->{'isSynchroDisabled'},
                   'isDeliveryLocked'          => $tabTransportInfos->{'isLocked'},
                   'isRecoveryLocked'          => $tabTransportInfos->{'isLocked'}
               };
            }
        }
    }

    return [$nbAgenciesTransferFilters, $nbResult, $tabRows, $isSelectedCountry];
}

sub _searchCountries
{
    my ($searchCountry) = @_;

    my $tabRows = {};

    my $nbResult = 0;

    if ($searchCountry eq 'TS')
    {
        my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);
        foreach my $countryId (@$tabCountries)
        {
            $nbResult += &LOC::Transport::getList($countryId,
                                                  LOC::Util::GETLIST_COUNT,
                                                  {
                                                      'stateId' => LOC::Transport::STATE_ACTIVE
                                                  });
        }
    }
    else
    {
        $nbResult = &LOC::Transport::getList($searchCountry, LOC::Util::GETLIST_COUNT,
                                             {
                                                 'stateId'  => LOC::Transport::STATE_ACTIVE
                                             });
    }
   return $nbResult;
}

1;