use utf8;
use open (':encoding(UTF-8)');

# Contrôleur de gestion du support technique de la GesLoc
package kimoce;

use strict;
use File::Basename;
use LOC::Country;
use LOC::Db;
use LOC::Kimoce;
use List::Util;


sub syncMachineFeaturesAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my @tabCode = ();

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    $tabViewData{'parkNumber'} = &LOC::Request::getString('parkNumber', '');
    $tabViewData{'parkNumber'} =~ s/\D//g;

    $tabViewData{'result'} = '';
    $tabViewData{'errorCode'} = '';

    if ($tabViewData{'parkNumber'} ne '')
    {
        # Vérifie si la machine existe dans Kimoce
        if (&LOC::Kimoce::isMachineExists($tabViewData{'parkNumber'}))
        {
            # Mise à jour des caractéristiques dans Kimoce
            &LOC::Kimoce::syncMachineFeatures($tabViewData{'parkNumber'});

            $tabViewData{'result'} = 'ok';
        }
        else
        {
            $tabViewData{'result'} = 'ko';
            $tabViewData{'errorCode'} = 'unknownMachine';
        }
    }


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/kimoce/syncMachineFeatures.cgi';
}


sub resendNotTreatedFeaturesUpdsAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my @tabCode = ();

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    $tabViewData{'results'} = undef;


    # Renvoi des mises à jour
    my $lastId = &LOC::Request::getInteger('lastId', '');
    if ($lastId != 0)
    {
        $tabViewData{'results'} = &LOC::Kimoce::resendNotTreatedFeaturesUpdates($lastId);
    }
    else
    {
        # Récupération de la liste des mises à jour non traitées
        $tabViewData{'list'} = &LOC::Kimoce::getFeaturesUpdatesHistoriesList({
            'isTreated' => 0
        }, {
            'orderBy' => [
                'id:ASC'
            ],
            'limit' => 10
        });

        # Dernier id de mise à jour
        $tabViewData{'lastId'} = &List::Util::max(keys(%{$tabViewData{'list'}}));
    }


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/kimoce/resendNotTreatedFeaturesUpds.cgi';
}


# Function _checkDeliveryList
# Vérifie l'existence des bons de livraison Kimoce fournis
#
# Parameters
# string   $countryId - Identifiant du pays
# arrayRef $list      - Liste des numéros de BL
sub _checkDeliveryList
{
    my ($countryId, $list) = @_;

    # Récupération de la connexion à la base de données Kimoce
    my $db = &LOC::Db::getConnection('kimoce', $countryId);

    my $query = '
SELECT
    HdSpplOrdEXCde,
    SpplOrdStsInCde
FROM p_hdspplord
WHERE HdSpplOrdEXCde IN (' . $db->quote($list) . ');';

    my $tabDeliveries = $db->fetchPairs($query);

    my $tabResult = {};

    # Pour chaque id fourni, existe-t-il dans Kimoce ?
    foreach my $id (@$list)
    {
        my @tabKeys = keys(%$tabDeliveries);
        $tabResult->{$id} = {
            'isExists' => &LOC::Util::in_array($id, \@tabKeys),
            'value'    => $tabDeliveries->{$id}
        };
    }
    return $tabResult;
}

# Function _updateDeliveryList
# Fait réapparaître les BL dans Kimoce
#
# Parameters
# string   $countryId - Identifiant du pays
# arrayRef $list      - Liste des numéros de BL
sub _updateDeliveryList
{
    my ($countryId, $list) = @_;

    # Récupération de la connexion à la base de données Kimoce
    my $db = &LOC::Db::getConnection('kimoce', $countryId);
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $tabUpdates = {
        'SpplOrdStsInCde' => 6
    };

    if ($db->update('p_hdspplord', $tabUpdates, {'HdSpplOrdEXCde' => $list}) == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }
    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }
    return 1;
}

1;