use utf8;
use open (':encoding(UTF-8)');

# Contrôleur de gestion du support technique de la GesLoc
package accounting;

use strict;
use File::Basename;
use LOC::Country;
use LOC::Db;
use LOC::Customer;


sub changeInvoiceDateAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my @tabCode = ();

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );
    
    $tabViewData{'countryList'} = &LOC::Country::getList(LOC::Util::GETLIST_PAIRS);
    $tabViewData{'check'}       = &LOC::Request::getString('check', '');
    my $countryId       = &LOC::Request::getString('countryId', '');
    if ($tabViewData{'check'} eq 'changeCountry' && $countryId ne '')
    {
        #Recuperation des dates de facturation du pays
        $tabViewData{'tabInvoiceDate'} = &_getInvoiceDate($countryId);
        $tabViewData{'check'} = 'displayDate';
    }
    elsif($tabViewData{'check'} eq 'toCheck')
    {
        
    }
    elsif ($tabViewData{'check'} eq 'toChange')
    {
        
    }
    
    $tabViewData{'countryId'} = $countryId;
    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/accounting/changeInvoiceDate.cgi';
}

sub removeCodeCustomerAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my @tabCode = ();

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );
    
    $tabViewData{'countryList'} = &LOC::Country::getList(LOC::Util::GETLIST_PAIRS);
    $tabViewData{'check'}       = &LOC::Request::getString('check', '');
    
    my $countryId       = &LOC::Request::getString('countryId', '');
    my $customerCode    = &LOC::Request::getString('customerCode', '');
    my $oldCustomerCode = &LOC::Request::getString('oldCode', '');
    my $oldCountry      = &LOC::Request::getString('oldCountry');
    
    if ($tabViewData{'check'} eq 'toCheck' &&  $customerCode ne '')
    {
        #appel methode de verification
        my $tab = &_getCustomerInfos($countryId, $customerCode);

        if ($tab->{'id'} != 0)
        {
            $tabViewData{'customerInfo'} = $tab;
            if ($tab->{'isRemoveCode'})
            {
                $tabViewData{'check'} = 'noError';
            }
            else
            {
                push(@tabCode, 'noPossible');
            }
        }
        else
        {
            push(@tabCode, 'errorNumEstimate');
        }
    }

    if ($tabViewData{'check'} eq 'toChange')
    {
        if ($customerCode == $oldCustomerCode)
        {
            if ($countryId == $oldCountry)
            {
                #appel methode de verification
                my $tab = &_getCustomerInfos($countryId, $customerCode);
                $tabViewData{'customerInfo'} = $tab;
                if ($tab->{'isRemoveCode'})
                {
                    #appel methode de traitement
                    if (&_removeCode($countryId, $tab->{'id'}))
                    {
                        #recuperation des nouvelles info
                        $tabViewData{'customerNewInfo'} = &_getCustomerInfos($countryId, $tab->{'id'});
                        
                        push(@tabCode, 'removed');
                    }
                    else
                    {
                        push(@tabCode, 'errorNoChange');
                    }
                }
                else
                {
                    push(@tabCode, 'noPossible');
                }
            }
            else
            {
                push(@tabCode, 'errorCountryChange');
            }
        }
        else
        {
            push(@tabCode, 'errorCodeChange');
        }
    }
    
    $tabViewData{'customerCode'} = $customerCode;
    $tabViewData{'country'} = $countryId;
    $tabViewData{'codeReturn'} = \@tabCode;
    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/accounting/removeCodeCustomer.cgi';
}

# Function _getCustomerInfos
# Récupère les informations nécessaires du client
#
# Parameters
# string       $countryId    - Identifiant du pays
# int|string   $customerCode - Identifiant ou code du client
#
# Return
# hash|hashref
sub _getCustomerInfos
{
    my ($countryId, $customerCode) = @_;
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $tabFilters = {};
    if (&LOC::Util::isNumeric($customerCode))
    {
        $tabFilters->{'id'} = $customerCode * 1;
    }
    else
    {
        $tabFilters->{'code'} = $customerCode;
    }
    my $tabCustomerInfos = &LOC::Customer::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters);
    my @tabResult = keys(%$tabCustomerInfos);
    my $countResult = @tabResult;

    my $tab = {};

    if ($countResult == 0 || $countResult > 1)
    {
        return $tab;
    }

    foreach my $tab (values(%$tabCustomerInfos))
    {
        my $countContracts = &LOC::Contract::Rent::getList($countryId, LOC::Util::GETLIST_COUNT, {'customerId' => $tab->{'id'}});
        $tab->{'countContract'} = $countContracts * 1;

        $tab->{'isRemoveCode'} = 0;
        if ($tab->{'countContract'} == 0)
        {
            $tab->{'isRemoveCode'} = 1;
        }
        return (wantarray ? %$tab : $tab);
    }
}

sub _getInvoiceDate
{
    my ($countryId) = @_;
    my $db = &LOC::Db::getConnection('auth');
    my $query = '
SELECT 
    ivd_id AS `id`,
    ivd_date AS `date`,
    IF(ivd_type = "I", "Intermédiaire", "Mensuelle") AS `type`
FROM p_invoicedate
WHERE 
    ivd_done = 0
    AND ivd_cty_id = "' . $countryId . '"
ORDER BY ivd_date';
    my $tab = $db->fetchAssoc($query);
    return (wantarray ? %$tab : $tab);
}

# Function _removeCode
# Supprime la vérification du client
#
# Parameters
# string   $countryId    - Identifiant du pays
# int      $customerId   - Identifiant du client
#
# Return
# bool
sub _removeCode
{
    my ($countryId, $customerId) = @_;
    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $tabUpdates = {
        'code' => undef,
        'verificationStatus' => -1,
        'validityStatus'     => undef,
        'accountingCode'     => ''
    };
    
    if (!&LOC::Customer::update($countryId, $customerId, $tabUpdates))
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }
        return 0;
    }
    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }
    return 1;
}

1;