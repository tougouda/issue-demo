use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;
use LOC::Locale;

our %tabViewData;
my $view;
my $locale;


$locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
$view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


$view->setPageTitle($locale->t('Liste des modèles de fiche de remise en état'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle($locale->t('Liste des modèles de fiche de remise en état'));

# CSS
$view->addCSSSrc('location.css');
$view->addCSSSrc('tr.rowBg0, tr.rowBg1 {cursor: pointer;}');

# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();

print '
<br />
<table id="tableArticle" class="standard ex" style="width: 100%;">
    <tr>
        <th>' . htmllocale('Id') . '</th>
        <th>' . htmllocale('Libellé') . '</th>
    </tr>';
my $i = 1;
my $tabfamilies = $tabViewData{'tabFamilies'};
while (my ($id, $family) = each(%$tabfamilies))
{
    my $url = $view->createURL('repair:checkpointFamily:edit', {'familyId' => $id});
    
    print '
    <tr class="rowBg'. ($i % 2) .'" onclick="LOC_View.openWindow(\'checkpoint\', \'' . $url . '\')">
        <td>' .
            $family->{'id'} . '
        </td>
        <td>' .
            $family->{'fullName'} . '
        </td>
    </tr>';
            
    $i++;
}
print '
</table>';

print $view->displayFooter();
