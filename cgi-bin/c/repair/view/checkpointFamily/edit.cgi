use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;
use LOC::Locale;

our %tabViewData;
our $view;
our $locale;


$locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
$view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


$view->setPageTitle($locale->t('Modèle fiche remise en état'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle($locale->t('Modèle de fiche de remise en état'));

# JS
my $jsCode = '
function submitForm()
{
    var form = document.getElementsByTagName(\'form\')[0];
    
    $(\'valid\').value = \'ok\';
    form.submit();
}

function resetForm()
{
    var form = document.getElementsByTagName(\'form\')[0];

    form.reset();
}';
$view->addJSSrc($jsCode);

# CSS
$view->addCSSSrc('location.css');


# Éléments du formulaire

# champs cachés
my $htmlHidden = '';
$htmlHidden .= $view->displayFormInputHidden('valid', $tabViewData{'valid'});
$htmlHidden .= $view->displayFormInputHidden('familyId', $tabViewData{'familyId'});

# Bouton de validation
my $btnValid = $view->displayTextButton(htmllocale('Valider'), 'valid(|Over).png', 'submitForm()');

my $btnReinit = $view->displayTextButton(htmllocale('Réinitialiser'), 'undo(|Over).png',
    'resetForm()');


# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();


# "Définition"
print '
<fieldset>
    <legend>' . htmllocale('Famille de machine') . '</legend>
    <table class="formTable">
        <tr>
            <td class="label">' . htmllocale('Libellé') . '</td>
            <td colspan="2">' . $tabViewData{'family'}->{'fullName'} . '</td>
        </tr>
    </table>
</fieldset>';

    
# "Points de vérifications"
print '
<fieldset>
<legend>' . htmllocale('Points de vérifications') . '</legend>
    <div style="position: relative;" title="' . htmllocale('Points de vérifications') . '">
        <table class="standard">
            <tr>
                <th>' . htmllocale('Point de vérification') . '</th>
                <th>' . htmllocale('Ordre') . '</th>
                <th>' . htmllocale('Point de vérification') . '</th>
                <th>' . htmllocale('Ordre') . '</th>
            </tr>';
my $checkpoints = $tabViewData{'checkpoints'};
my $checkpointValues = $tabViewData{'tabCheckpointValues'};
my $count = scalar keys(%$checkpoints);
if ($count > 0)
{
    my $i = 0;
    while (my ($id, $checkpoint) = each(%$checkpoints))
    {
        if (($i % 2) == 0)
        {
            print '<tr>';
        }
        
        my $checked = 0;
        my $order = '';
        if (defined $checkpointValues->{$id})
        {
            $checked = 1;
            $order = $checkpointValues->{$id}->{'order'};
        }

        print $view->displayFormInputHidden('tabCheckpoint['. $i .'][id]', $checkpoint->{'id'});
        print '
        <td>' .
            $view->displayFormInputCheckBox('tabCheckpoint['. $i .'][cb]',
                        $checked, {'id' => 'tabCheckpoint['. $i .'][cb]'}, $checkpoint->{'label'}, 1) . '
        </td>
        <td>' .
            $view->displayFormInputText('tabCheckpoint['. $i .'][order]', $order, '',
                        {'size' => 4, 'maxlength' => 3, 'autocomplete' => 'off'}) . '
        </td>';

        if ((($i % 2) == 1) || $i == ($count - 1))
        {
            print '</tr>';
        }
        
        $i++;
    }
}
print '
        </table>
    </div>
</fieldset>';

# Affichage des champs cachés
print '
    <div id="chpHiddens" style="display: none;">' .
        $htmlHidden . '
    </div>';

# Affichage du panel
print $view->displayControlPanel({'left' => [$btnValid],
                                  'right' => [$btnReinit],
                                  'substyle' => 'bottom'});

my $messages = $tabViewData{'messages'};
my $count = scalar keys(%$messages);
if ($count > 0)
{
    print '<p>';
    while (my ($type, $msgs) = each(%$messages))
    {
        print $view->displayMessages($type, $msgs, 1);
    }
    print '</p>';
}

print $view->displayFooter();
