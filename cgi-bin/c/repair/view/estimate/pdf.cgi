use utf8;
use open (':encoding(UTF-8)');
binmode STDOUT, ":raw";    # Pour envoyer le flux en l'état

use strict;
use locale;

use LOC::Pdf;
use LOC::Locale;
use LOC::Browser;



our %tabViewData;
our $var;

my $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});

my $margin = 10/mm;
my $pageWidth = 210/mm;
my $pageHeight = 297/mm;
my $pageLimit = $margin;
my $headerPageLimitCheck = 70/mm;
my $gap = 2/pt;

my $pdf = LOC::Pdf->new($locale->t('Devis de remise en état'), $var->{agencyId}, \$locale);
my $pdfObj = $pdf->getPdfObject();

my $regularFont    = $pdf->{font}->{regular};
my $italicFont     = $pdf->{font}->{italic};
my $boldFont       = $pdf->{font}->{bold};
my $boldItalicFont = $pdf->{font}->{bolditalic};



# Définition du sujet et des mots-clés
$pdf->setSubject($locale->t('Devis de remise en état n° %s, client %s, machine %s n° %s',
                            $var->{code}, $var->{customer}->{label}, $var->{model}, $var->{number}));
$pdf->setKeywords($locale->t('Devis'), $locale->t('Remise en état'));


# Documents PDF supplémentaires à insérer
my $tabAdditionalDocuments = $tabViewData{'tabAdditionalDocuments'};

# - Documents "avant"
&_displayAdditionalDocuments($pdf, $tabAdditionalDocuments, 'before');

# Nouvelle page
my $page = $pdf->addPage();
my $gfx = $page->gfx();
my $text = $page->text();



my $x;
my $y;
my $width;
my $height;

# Numéro et date du document
my $number = $locale->t('N° %s du %s', $var->{code},
                                       $locale->getDateFormat($var->{date}, LOC::Locale::FORMAT_DATE_NUMERIC));
$gfx->textlabel(10/mm + 95/mm, 297/mm - 35/mm - 16/pt * 1.2, $boldFont, 10/pt, $number);

# Contacts
$pdf->addContactsBlock(\$page, $var->{interlocutor});

# Références
tie(my %tabReferences, 'Tie::IxHash');
%tabReferences = (
                     $locale->t('N° commande :')    => $var->{order},
                     $locale->t('N° contrat :')     => $var->{contract},
                     $locale->t('Contact :')        => $var->{contact},
                     $locale->t('Tél. contact :')   => $var->{telephone},
                     $locale->t('Chantier :')       => $var->{site},
                     $locale->t('Location :')       => $locale->t('Du %s au %s', $locale->getDateFormat($var->{beginDate}, LOC::Locale::FORMAT_DATE_NUMERIC), $locale->getDateFormat($var->{endDate}, LOC::Locale::FORMAT_DATE_NUMERIC))
                    );
my $tabReferences = \%tabReferences;
$pdf->addReferencesBlock(\$page, $tabReferences);

# Coordonnées client
my $tabCustomer = [$var->{customer}->{label}, $var->{customer}->{address1}, $var->{customer}->{address2},
                   $var->{customer}->{locality}, $locale->t('Tél. ') . $var->{customer}->{telephone}, $locale->t('Fax ') . $var->{customer}->{fax}];
$pdf->addCustomerBlock(\$page, $tabCustomer);



# Informations spécifiques du devis de remise en état
$y = 297/mm - 95/mm;
$text->translate(10/mm, $y);
$text->font($boldFont, 8/pt);
$text->text($locale->t('Famille :'));
$text->font($regularFont, 10/pt);
$text->text(' ');
$text->text($var->{family});

$text->translate(90/mm, $y);
$text->font($boldFont, 8/pt);
$text->text($locale->t('Modèle :'));
$text->font($regularFont, 10/pt);
$text->text(' ');
$text->text($var->{model});

$text->translate(160/mm, $y);
$text->font($boldFont, 8/pt);
$text->text($locale->t('N° de parc :'));
$text->font($regularFont, 10/pt);
$text->text(' ');
$text->text($var->{number});

$y -= 10/pt * 1.2;
$text->translate(10/mm, $y);
$text->font($boldFont, 8/pt);
$text->text($locale->t('Objet :'));
$text->font($regularFont, 10/pt);
$text->text(' ');
$text->section($var->{description}, $pageWidth - 3 * $margin, $text->lead(), -align => 'justified');



# En-tête du tableau des lignes de devis
my $xPrixHT;       # Stockage de l'abscisse de la colonne "Prix HT"
my $xMontantHT;    # Stockage de l'abscisse de la colonne "Montant HT"


my $gap = 2/pt;
$x = 10/mm;
$y = 297/mm - 112/mm;
$text->font($boldFont, 8/pt);
$gfx->linewidth(1/pt);

my %tabX = ();

$text->translate($x, $y);
$tabX{'label'} = $x;
$text->text($locale->t('Désignation'), -maxWidth => 80/mm - $gap);
$gfx->move($x, $y - 1/mm);
$x = 210/mm - 10/mm - 30/mm - 25/mm - 15/mm - 25/mm;
$gfx->line($x - 11.8/mm - $gap, $y - 1/mm);

$text->translate($x, $y);
$tabX{'quantity'} = $x;
$text->text_right($locale->t('Qté'), -maxWidth => 10/mm - $gap);
$gfx->move($x, $y - 1/mm);
$gfx->line($x - 11.8/mm, $y - 1/mm);

$x += 25/mm;
$text->translate($x, $y);
$tabX{'unitPrice'} = $x;
$text->text_right($locale->t('Prix HT'), -maxWidth => 20/mm - $gap);
$gfx->move($x, $y - 1/mm);
$gfx->line($x - 25/mm + $gap, $y - 1/mm);
$xPrixHT = $x - 25/mm + $gap;

my $amountShift = 0;
if ($var->{'hasLineReduction'})
{
    $x += 15/mm;
    $text->translate($x, $y);
    $tabX{'reduction'} = $x;
    $text->text_right($locale->t('% rem.'), -maxWidth => 12/mm - $gap);
    $gfx->move($x, $y - 1/mm);
    $gfx->line($x - 15/mm + $gap, $y - 1/mm);
    
    $x += 25/mm;
    $text->translate($x, $y);
    $tabX{'reducedUnitPrice'} = $x;
    $text->text_right($locale->t('PU net HT'), -maxWidth => 20/mm - $gap);
    $gfx->move($x, $y - 1/mm);
    $gfx->line($x - 25/mm + $gap, $y - 1/mm);
}
else
{
    $amountShift = 40/mm;
}

$x += $amountShift + 30/mm;
$text->translate($x, $y);
$tabX{'amount'} = $x;
$text->text_right($locale->t('Montant HT'), -maxWidth => 20/mm - $gap);
$gfx->move($x, $y - 1/mm);
$gfx->line($x - ($amountShift + 30/mm) + $gap, $y - 1/mm);
$xMontantHT = $x - 30/mm + $gap;

$gfx->stroke();

# Lignes de devis
my @tabDesignation = @{$var->{lines}};
$y -= 8/pt * 1.2 + 1/pt + 1/mm;
$text->font($regularFont, 10/pt);
my $nbLines = @tabDesignation;
for (my $i = 0; $i < $nbLines; $i++)
{
    # Saut de page si nécessaire
    if (&checkRemainingSpace($y - $pageLimit - 8/pt - (7/mm + 10/pt * 1.2 * 3 + 12/pt * 1.2 * 2)))
    {
        $x = 10/mm;
        $text->font($boldFont, 8/pt);
        $gfx->linewidth(1/pt);
        
        $text->translate($x, $y);
        $text->text($locale->t('Désignation'), -maxWidth => 80/mm - $gap);
        $gfx->move($x, $y - 1/mm);
        $x = 210/mm - 10/mm - 30/mm - 25/mm - 15/mm - 25/mm;
        $gfx->line($x - 11.8/mm - $gap, $y - 1/mm);
        
        $text->translate($x, $y);
        $text->text_right($locale->t('Qté'), -maxWidth => 10/mm - $gap);
        $gfx->move($x, $y - 1/mm);
        $gfx->line($x - 11.8/mm, $y - 1/mm);
        
        $x += 25/mm;
        $text->translate($x, $y);
        $text->text_right($locale->t('Prix HT'), -maxWidth => 20/mm - $gap);
        $gfx->move($x, $y - 1/mm);
        $gfx->line($x - 25/mm + $gap, $y - 1/mm);
        $xPrixHT = $x - 25/mm + $gap;
        
        $x += 15/mm;
        $text->translate($x, $y);
        $text->text_right($locale->t('% rem.'), -maxWidth => 12/mm - $gap);
        $gfx->move($x, $y - 1/mm);
        $gfx->line($x - 15/mm + $gap, $y - 1/mm);
        
        $x += 25/mm;
        $text->translate($x, $y);
        $text->text_right($locale->t('PU net HT'), -maxWidth => 20/mm - $gap);
        $gfx->move($x, $y - 1/mm);
        $gfx->line($x - 25/mm + $gap, $y - 1/mm);

        $x += 30/mm;
        $text->translate($x, $y);
        $text->text_right($locale->t('Montant HT'), -maxWidth => 20/mm - $gap);
        $gfx->move($x, $y - 1/mm);
        $gfx->line($x - 30/mm + $gap, $y - 1/mm);
        
        $gfx->stroke();

        $x = 10/mm;
        $y -= 8/pt * 1.2 + 1/pt + 1/mm;
        
        $text->font($regularFont, 10/pt);
    }

    # Désignation

    # Type de ligne
    my $label = '';
    my $quantity = $tabDesignation[$i]->{quantity} * 1;
    my $unitPrice = &LOC::Util::round($tabDesignation[$i]->{unitPrice}, 2);
    my $reducedPrice = &LOC::Util::round($tabDesignation[$i]->{reducedUnitPrice}, 2);
    if ($tabDesignation[$i]->{type} eq LOC::Repair::LineType::TYPE_LABOUR)
    {
        if ($tabViewData{'displLabDetails'})
        {
            $label = $locale->t('Main d\'oeuvre (en heures)');
        }
        else
        {
            $label         = $locale->t('Main d\'oeuvre');
            $unitPrice    *= $quantity;
            $reducedPrice *= $quantity;
            $quantity      = 1;
        }
    }
    elsif ($tabDesignation[$i]->{type} eq LOC::Repair::LineType::TYPE_TRAVELTIME)
    {
        if ($tabViewData{'displLabDetails'})
        {
            $label = $locale->t('Temps de trajet (en heures)');
        }
        else
        {
            $label         = $locale->t('Temps de trajet');
            $unitPrice    *= $quantity;
            $reducedPrice *= $quantity;
            $quantity      = 1;
        }
    }
    elsif ($tabDesignation[$i]->{type} eq LOC::Repair::LineType::TYPE_DISPLACEMENT)
    {
        if ($tabViewData{'displLabDetails'})
        {
            $label = $locale->t('Déplacement (en kilomètres)');
        }
        else
        {
            $label         = $locale->t('Déplacement');
            $unitPrice    *= $quantity;
            $reducedPrice *= $quantity;
            $quantity      = 1;
        }
    }
    elsif ($tabDesignation[$i]->{type} eq LOC::Repair::LineType::TYPE_RESIDUES)
    {
        $label = $locale->t('Participation au recyclage');
    }
    else
    {
        my $typeLabel = '';
        if (($tabDesignation[$i]->{type} eq LOC::Repair::LineType::TYPE_REPAIR) || ($tabDesignation[$i]->{type} eq LOC::Repair::LineType::TYPE_FIX))
        {
            $typeLabel = ' - ' . $locale->t('Rénovation');
        }
        $label = $tabDesignation[$i]->{label} . $typeLabel;
    }
    

    $text->translate($tabX{'label'}, $y);
    if (length($tabDesignation[$i]->{label}) > 35)
    {
        $text->text_justified($label, 83/mm);
    }
    else
    {
        $text->text($label);
    }
    

    # Quantité
    $text->translate($tabX{'quantity'}, $y);
    $text->text_right($quantity);
    
    # Prix HT
    $text->translate($tabX{'unitPrice'}, $y);
    $text->text_right($locale->getCurrencyFormat($unitPrice, undef, $tabViewData{'currencySymbol'}));
    
    unless ($tabDesignation[$i]->{reduction} == 0)
    {
        # % remise
        $text->translate($tabX{'reduction'}, $y);
        $text->text_right($locale->t('%s %%', $locale->getNumberFormat($tabDesignation[$i]->{reduction} * 100, 2, 1)));
        
        # PU net HT
        $text->translate($tabX{'reducedUnitPrice'}, $y);
        $text->text_right($locale->getCurrencyFormat($reducedPrice, undef, $tabViewData{'currencySymbol'}));
    }
    
    # Montant HT
    $text->translate($tabX{'amount'}, $y);
    $text->text_right($locale->getCurrencyFormat($tabDesignation[$i]->{amount}, undef, $tabViewData{'currencySymbol'}));
    
    $y -= 10/pt * 1.2;
    
}


# Bloc total
if ($var->{globalReduc} != 0)
{
    $x = $xMontantHT;
    $y -= 5/mm;
    $gfx->linewidth(.5/pt);
    $gfx->move($x, $y);
    $gfx->line(210/mm - 10/mm, $y);
    $gfx->stroke();
    
    $x = $xPrixHT;
    $y -= 10/pt * 1.2;
    $text->translate($x, $y);
    $text->font($regularFont, 10/pt);
    $text->text($locale->t('Total HT'));
    
    $x = 210/mm - 10/mm;
    $text->translate($x, $y);
    $text->text_right($locale->getCurrencyFormat($var->{total}, undef, $tabViewData{'currencySymbol'}));
    
    $x = $xPrixHT;
    $y -= 10/pt * 1.2;
    $text->translate($x, $y);
    $text->font($regularFont, 10/pt);
    $text->text($locale->t('Remise %s %%', $var->{globalReduc} * 100));
    
    $x = 210/mm - 10/mm;
    $text->translate($x, $y);
    $text->text_right($locale->getCurrencyFormat(-$var->{globalReducAmount}, undef, $tabViewData{'currencySymbol'}));
}

$x = $xMontantHT;
$y -= 2/mm;
$gfx->linewidth(.5/pt);
$gfx->move($x, $y);
$gfx->line(210/mm - 10/mm, $y);
$gfx->stroke();

$x = $xPrixHT;
$y -= 12/pt * 1.2;
$text->translate($x, $y);
#$text->font($boldFont, 12/pt);
$text->font($regularFont, 10/pt);
$text->text($locale->t('Montant total HT'));

$x = 210/mm - 10/mm;
$text->translate($x, $y);
$text->text_right($locale->getCurrencyFormat($var->{reducedTotal}, undef, $tabViewData{'currencySymbol'}));


$x = $xPrixHT;
$y -= 10/pt * 1.2;
$text->translate($x, $y);
$text->font($regularFont, 10/pt);
$text->text($locale->t('TVA %s %%', $var->{tva} * 100));

$x = 210/mm - 10/mm;
$text->translate($x, $y);
#$text->text_right($locale->getCurrencyFormat(-$var->{globalReducAmount}, undef, $tabViewData{'currencySymbol'}));
$text->text_right($locale->getCurrencyFormat(+ $var->{amountWithTVA}, undef, $tabViewData{'currencySymbol'}));

#Affichage du trait de séparation 
$x = $xMontantHT;
$y -= 2/mm;
$gfx->linewidth(1/pt);
$gfx->move($x, $y);
$gfx->line(210/mm - 10/mm, $y);
$gfx->stroke();

#Affichage du libelle Montant total TTC
$x = $xPrixHT;
$y -= 12/pt * 1.2;
$text->translate($x, $y);
$text->font($boldFont, 12/pt);
$text->text($locale->t('Montant total TTC'));

#Affichage du montant total TTC
$x = 210/mm - 10/mm;
$text->translate($x, $y);
$text->text_right($locale->getCurrencyFormat($var->{totalAmount}, undef, $tabViewData{'currencySymbol'}));





# Commentaires
if (length($var->{'comment'}) > 0)
{
    $x = $margin;
    $y -= 10/mm;
    
    # Vérification de l'espace disponible
    &checkRemainingSpace($y - $pageLimit - 8/pt);
    
    $text->textlabel($x, $y, $boldFont, 8/pt, $locale->t('Commentaires :'));
    
    $y -= 10/pt * 1.2;
    $text->font($regularFont, 10/pt);
    $text->translate($x, $y);
    $text->lead(10/pt);
    
    my $overflowText = $var->{'comment'};
    while ($overflowText = $text->section($overflowText, $pageWidth - 2 * $margin, $y - $pageLimit - 8/pt,
                                          -align => 'justified'))
    {
        $page = $pdf->addPage(LOC::Pdf::DISPLAY_NONE);
        $gfx  = $page->gfx();
        $text = $page->text();
        
        $y = $pageHeight - $margin;
    
        $text->textlabel($x, $y, $boldFont, 8/pt, $locale->t('Commentaires (suite) :'));
        $y -= 10/pt * 1.2;
    
        $text->font($regularFont, 10/pt);
        $text->translate($x, $y);
        $text->lead(10/pt);
    }
}


# Blocs signatures
# Client
$x = 10/mm;

# Vérification de l'espace disponible
&checkRemainingSpace($y - $headerPageLimitCheck - $margin);
$y = $margin + $headerPageLimitCheck;

$text->translate($x, $y);
$text->font($boldFont, 10/pt);
$text->text($locale->t('Visa du client'));

$y -= 10/pt * 1.2 + 2/mm;
$text->translate($x, $y);
$text->font($regularFont, 10/pt);
my $textWidth = $text->text($locale->t('Nom :')) + 3/mm;
$x += $textWidth;
$y -= 1/mm;
$width = 210/mm / 2 - 5/mm - $x;
$height = 10/pt;
$gfx->fillcolor('#ECF5FB');
$gfx->rect($x, $y, $width, $height + 1/mm);
$gfx->fill();
$y -= .75/pt;
$gfx->linewidth(1.5/pt);
$gfx->strokecolor('#D9EBF8');
$gfx->move($x, $y);
$gfx->line($x + $width, $y);
$gfx->stroke();
$gfx->fillcolor('black');
$gfx->strokecolor('black');

$x = 10/mm;
$y -= 10/pt * 1.2 + 2/mm;
$text->translate($x, $y);
$text->font($regularFont, 10/pt);
$text->text($locale->t('Bon pour accord'));
$y -= 10/pt * 1.2;
$text->translate($x, $y);
$text->font($regularFont, 10/pt);
$text->text($locale->t('Cachet et signature :'));

# Atelier
$x = 210/mm / 2 + 5/mm;
$y = $margin + $headerPageLimitCheck;
$text->translate($x, $y);
$text->font($boldFont, 10/pt);
$text->text($locale->t('Visa de l\'atelier'));

$y -= 10/pt * 1.2 + 2/mm;
$text->translate($x, $y);
$text->font($regularFont, 10/pt);
$textWidth = $text->text($locale->t('Nom :')) + 3/mm;
$x += $textWidth;
$y -= 1/mm;
$width = 210/mm - 10/mm - $x;
$height = 10/pt;
$gfx->fillcolor('#ECF5FB');
$gfx->rect($x, $y, $width, $height + 1/mm);
$gfx->fill();
$y -= .75/pt;
$gfx->linewidth(1.5/pt);
$gfx->strokecolor('#D9EBF8');
$gfx->move($x, $y);
$gfx->line($x + $width, $y);
$gfx->stroke();
$gfx->fillcolor('black');
$gfx->strokecolor('black');

$x = 210/mm / 2 + 5/mm;
$y -= 10/pt * 1.2 + 2/mm;
$text->translate($x, $y);
$text->font($regularFont, 10/pt);
$text->text($locale->t('Signature :'));

$x = 10/mm;
$y = 25/mm;
$text->font($regularFont, 10/pt);
$text->translate($x, $y);
$text->text($locale->t('Sans réponse de votre part sous quinzaine, le devis sera considéré comme accepté.'));
#$y -= 10/pt * 1.2;
#$text->translate($x, $y);
#$text->text($locale->t('L\'écotaxe est incluse dans le prix des pièces.'));

# - Documents "après"
&_displayAdditionalDocuments($pdf, $tabAdditionalDocuments, 'after');


# Numéros de page
&displayPageNumbers();



# Sortie
my $pdfString = $pdf->getString();
$pdf->end();

&LOC::Browser::sendHeaders(
        "Content-type: application/pdf;\n"
        . "Content-Disposition: inline; filename=\"devisRE.pdf\";\n\n"
    );

print $pdfString;



# Function: displayPageNumbers
# Affichage du numéro de page
sub displayPageNumbers
{
    my $nbPages = $pdfObj->pages();
    
    for (my $i = 0; $i < $nbPages; $i++)
    {
        my $page = $pdfObj->openpage($i + 1);
        my $text = $page->text();
        
        $text->font($italicFont, 8/pt);
        $text->translate($pageWidth - $margin, 1.5 * $margin);
        $text->text_right(sprintf('%d/%d', $i + 1, $nbPages));
    }
}


# Function: checkRemainingSpace
# Vérification de l'espace restant dans la page
#
# Parameters:
# string $value - Ordonnée ($y par défaut)
#
# Returns:
# bool - 1 si l'espace restant est suffisant, 0 sinon
sub checkRemainingSpace
{
    my ($value) = @_;
    
    unless (defined $value)
    {
        $value = $y;
    }
    
    if ($value < $pageLimit)
    {
        $page = $pdf->addPage(LOC::Pdf::DISPLAY_NONE);
        $gfx  = $page->gfx();
        $text = $page->text();
        
        $y = $pageHeight - $margin;
        
        return 1;
    }
    return 0;
}

# Function: _displayAdditionalDocuments
# Affiche des documents supplémentaires
#
# Parameters:
# LOC::Pdf  $pdf          - Instance du document principal
# hashref   $tabDocuments - Tableau des documents
# string    $zone         - Zone d'affichage
sub _displayAdditionalDocuments
{
    my ($pdf, $tabDocuments, $zone) = @_;
    
    if (!defined $zone)
    {
        $zone = 'before';
    }

    if (defined $tabAdditionalDocuments->{$zone})
    {
        foreach my $documentInfos (@{$tabAdditionalDocuments->{$zone}})
        {
            if ($documentInfos->{'active'} && -e $documentInfos->{'path'})
            {
                $pdf->addFile($documentInfos->{'path'});
            }
        }
    }
}
