use utf8;
use open (':encoding(UTF-8)');

#our $view;
#our %tabViewData;

my $tabFlags = {
    LOC::Repair::Estimate::FLAG_TURNEDINSERVICES() => $locale->t('Fiche basculée en services'),
    LOC::Repair::Estimate::FLAG_TURNEDINDAYS()     => $locale->t('Fiche basculée en jours'),
    LOC::Repair::Estimate::FLAG_SHEETUPDATED()     => $locale->t('Modification sur la fiche')
};

# Valeurs cachées (participation au recyclage)
my $residuesModeHidden  = $view->displayFormInputHidden('residuesMode',  $tabViewData{'residuesMode'});
my $residuesValueHidden = $view->displayFormInputHidden('residuesValue', $tabViewData{'residuesValue'});

print $residuesModeHidden . $residuesValueHidden;


my %inputOptions;

# Objet du devis
%inputOptions = ('cols' => 50, 'rows' => 4);
if ($tabViewData{'tabRights'}->{'infos'} !~ /^(|.*\|)object(|\|.*)$/)
{
    $inputOptions{'disabled'} = 'readonly';
}
$inputOptions{'tabindex'} = $tabIndex++;
my $objectInput = $view->displayFormTextArea('object', $tabViewData{'object'}, \%inputOptions);

print '
<fieldset id="header">
    <legend>' . $view->displayImage('repair/estimate/infos.png') . '&nbsp;' . $locale->t('Devis de remise en état') . '</legend>
    <table class="formTable" style="float: right; margin-left: 10px;">
        <tr>
            <td class="label">' . $locale->t('Objet') . '</td>
        </tr>
        <tr>
            <td>' . $objectInput . '</td>
        </tr>
    </table>';

# URL fiche de remise en état
my $urlSheet = $view->createURL('repair:sheet:edit', {'sheetId' => $tabViewData{'sheetId'}});

# URL contrat de location
my $urlContract = $view->createURL('rent:rentContract:view', {'contractId' => $tabViewData{'contract'}->{'id'}});

my $proformaGenInContract    = '';
if ($tabViewData{'contract'}->{'proformaFlags'} & LOC::Proforma::DOCFLAG_EXISTACTIVED || $tabViewData{'contract'}->{'proformaFlags'} & LOC::Proforma::DOCFLAG_CT_ISCUSTOMERPFREQONCREATION)
{
    $proformaGenInContract = '<span id="proformaGeneratedEstimate">' . htmllocale('Ce contrat fait l\'objet d\'une pro forma') . '</span>';
}

if ($isNew == 0)
{
    %inputOptions = ('size' => 24, 'maxlength' => 24, 'readonly' => 'readonly', 'style' => 'font-weight: bold');
    $inputOptions{'tabindex'} = $tabIndex++;
    my $codeInput = $view->displayFormInputText('code', $tabViewData{'code'}, '', \%inputOptions);

    print '
    <table class="formTable">
        <tr>
            <td class="label">' . $locale->t('Code') . '</td>
            <td style="font-weight: bold;">' . $codeInput . '</td>
            <td colspan="2" style="padding-left: 20px">' .
                $view->displayState($tabViewData{'stateId'}, $tabViewData{'stateLabel'}) .
                (defined ($tabViewData{'flag'}) ?  ' (' . $tabFlags->{$tabViewData{'flag'}} . ')' : "") . '
            </td>
        </tr>
    </table>
    <hr>
    <table class="formTable">
        <tr>
            <td class="label">' . $locale->t('Créé le') . '</td>
            <td>' . $locale->getDateFormat($tabViewData{'dateCreation'}, &LOC::Locale::FORMAT_DATETIME_NUMERIC,
                                           $tabViewData{'timeZone'}) . '</td>
            <td class="label" style="padding-left: 20px">' . $locale->t('modifié le') . '</td>
            <td>' . $locale->getDateFormat($tabViewData{'dateModification'},
                                                       &LOC::Locale::FORMAT_DATETIME_NUMERIC,
                                                       $tabViewData{'timeZone'}) . '</td>';
}
else
{
    print '
    <table class="formTable">
        <tr>';
}


my $sheetComment = '';
if ($tabViewData{'sheet'}->{'sheetComment'} ne '')
{
    $sheetComment = $view->toHTMLEntities('<span class="sheetCommentLabel">') . $locale->t('Commentaire issu de la fiche') . '&nbsp;:</span>' . $view->toJSEntities($tabViewData{'sheet'}->{'sheetComment'});
}
else
{
    $sheetComment = $view->toHTMLEntities('<span class="sheetCommentLabel">') . $locale->t('Pas de commentaire issu de la fiche') . '</span>';
}

my $sheetLink = '<span ' .
                     'onmouseover="Location.toolTipsObj.show(\'' . $sheetComment . '\', {corner:\'tl\'});" ' .
                     'onmouseout="Location.toolTipsObj.hide();">';
$sheetLink .= '<a target="_blank" href="' . $urlSheet . '">' .
                     $tabViewData{'sheetId'} .
                '</a>';
if ($tabViewData{'sheet'}->{'sheetComment'} ne '')
{
    $sheetLink .= ' ' . $view->displayImage('repair/estimate/info.gif');
}
$sheetLink .= '</span>';

print '
            <td style="padding-left: 20px">' . $locale->t('Depuis la fiche de remise en état %s', $sheetLink) . '</td>';
print '
        </tr>
    </table>
    <table class="formTable" style="margin-top: 5px">
        <tr>
            <td class="label">' . $locale->t('Contrat') . '</td>
            <td colspan="3">
                <a class="action-btn openRentContract" target="_blank" href="' . $urlContract . '">' . $tabViewData{'contract'}->{'code'} . '</a>' .
                $proformaGenInContract . '
                ' . $locale->t('(location du %s au %s)', $locale->getDateFormat($tabViewData{'contract'}->{'beginDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC), $locale->getDateFormat($tabViewData{'contract'}->{'endDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC)) . '
            </td>
            <td class="label" style="padding-left: 20px">' . $locale->t('Machine') . '</td>
            <td>' . $view->displayParkNumber($tabViewData{'machine'}->{'parkNumber'}) . ' ' . $tabViewData{'machine'}->{'model.label'} . '</td>
        </tr>
    </table>';

print '
    <hr />';

#Interlocuteur
# Zone de recherche utilisateur
my $estimateUserId = $view->displayFormSelect('estimateUserId',  $tabViewData{'creatorsList'}, $tabViewData{'estimateUserId'});
#{'onchange' => '$ge("estimateUserId").value = this.value;'}
my $userListUrl = $view->createURL('default:user:jsonList', {'format' => 1});

$estimateUserId .= $view->displayJSBlock('
var searchUserSearchBox = Location.searchBoxesManager.createSearchBox(
                                                                      "estimateUserId",
                                                                      {
                                                                       isRequired:false,
                                                                       url:"' . $userListUrl . '"
                                                                      }
                                                                     );');


if ($tabViewData{'tabRights'}->{'infos'} !~ /^(|.*\|)estimateUser(|\|.*)$/)
{
    $estimateUserId .= $view->displayJSBlock('Location.searchBoxesManager.createSearchBox("estimateUserId").disable();');
}

# N° commande
%inputOptions = ('size' => 20, 'maxlength' => 20);
if ($tabViewData{'tabRights'}->{'infos'} !~ /^(|.*\|)orderNo(|\|.*)$/)
{
    $inputOptions{'disabled'} = 'disabled';
}
$inputOptions{'tabindex'} = $tabIndex++;
my $orderCodeHidden = $view->displayFormInputHidden('orderCode', $tabViewData{'orderCode'});
my $ordCodeInput = $view->displayFormInputText('orderCode', $tabViewData{'orderCode'}, '', \%inputOptions);

print '
    <table class="formTable">
        <tr>
            <td class="label">' . $locale->t('Interlocuteur') . '</td>
            <td><span>' . $estimateUserId . '</span>
            ' . (($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)valid(|\|.*)$/) ?
            $view->displayTextButton('', 'clear.gif', 'searchUserSearchBox.clear();', $locale->t('Effacer la recherche'))
              : '&nbsp;') .  '</td>
            <td class="label" style="padding-left: 20px">' . $locale->t('N° de commande') . '</td>
            <td>' . $orderCodeHidden . $ordCodeInput . '</td>
            <td class="label" style="text-align: left;">&nbsp;</td>
        </tr>
    </table>';

print '
</fieldset>';

# Infos du client
my @tabAddress = ();
foreach my $addressLine ($tabViewData{'customer'}->{'address1'}, $tabViewData{'customer'}->{'address2'})
{
    if (&LOC::Util::trim($addressLine) ne '')
    {
        push(@tabAddress, $addressLine);
    }
}

print '
<div style="float: left; width: 60%;">
    <fieldset id="customer" style="margin-right: 5px;">
        <legend>' . $view->displayImage('repair/estimate/customer.png') . '&nbsp;' . $locale->t('Client') . '</legend>

        <table class="formTable">
            <tr>
                <td class="label">' . $locale->t('Code') . '</td>
                <td><a href="' . $tabViewData{'UrlCustomerInfo'} . '" target="_blank">' . $tabViewData{'customer'}->{'code'} . '</a></td>
                <td class="label">' . $locale->t('Raison sociale') . '</td>
                <td><a href="' . $tabViewData{'UrlCustomerInfo'} . '" target="_blank">' . $tabViewData{'customer'}->{'name'} . '</a></td>
            </tr>
            <tr>
                <td class="label">' . $locale->t('Adresse') . '</td>
                <td>' .
                    join('<br>', @tabAddress) . '
                </td>
            </tr>
            <tr>
                <td class="label">' . $locale->t('Ville') . '</td>
                <td>' . $tabViewData{'customer'}->{'locality.postalCode'}  . ' ' . $tabViewData{'customer'}->{'locality.name'} . '</td>
            </tr>
            <tr>
                <td class="label">' . $locale->t('Tél.') . '</td>
                <td>' . $tabViewData{'customer'}->{'telephone'} . '</td>
                <td class="label">' . $locale->t('E-mail') . '</td>
                <td><a href="mailto:' . $tabViewData{'customer'}->{'email'} . '">' . $tabViewData{'customer'}->{'email'} . '</a></td>
            </tr>
            <tr>
                <td class="label">' . $locale->t('Fax') . '</td>
                <td>' . $tabViewData{'customer'}->{'fax'} . '</td>
            </tr>
        </table>

    </fieldset>
</div>';

print '
<div style="float: right; width: 40%;">
    <fieldset id="site" style="margin-left: 5px;">
        <legend>' . $view->displayImage('repair/estimate/site.png') . '&nbsp;' . $locale->t('Chantier') . '</legend>

        <span class="siteLabel">' . $tabViewData{'site'}->{'label'} . '</span><br />' .
        $tabViewData{'site'}->{'address'} . '<br />' .
        $tabViewData{'site'}->{'locality.label'} . '<br />

        <div style="margin: 5px 0px; border-top: 1px solid #66AFDA"></div>

        <table class="formTable">
            <tr style="vertical-align: baseline;">
                <td class="label">' . $locale->t('Contact chantier') . '</td>
                <td>' .
                    $tabViewData{'site'}->{'contact.fullName'} . '<br />' .
                    $tabViewData{'site'}->{'contact.telephone'} . '
                </td>
            </tr>
        </table>

    </fieldset>
</div>';
