use utf8;
use open (':encoding(UTF-8)');


print '
<fieldset id="photos">
<legend>' . $view->displayImage('repair/sheet/photo.png') . '&nbsp;' . $locale->t('Photos') . '</legend>

    <div id="photosContainer">';

foreach my $photo (@{$tabViewData{'sheet'}->{'images'}})
{
    print '<a class="photo" target="_blank" href="' . $photo->{'url'} . '">' .
               '<img src="' . $photo->{'thumbUrl'} . '" />' .
               '<div class="cb">' .
                    '<input type="checkbox" name="photos[]" value="' . $photo->{'name'} . '" onclick="onCheckPhoto();"/>' .
               '</div>' .
          '</a>';
}

print '
        <div class="actions">' .
            $view->displayTextButton($locale->t('Zipper'),
                                     'zip(|Over).gif',
                                     $view->createURL('repair:sheet:zipPhotos', {'sheetId' => $tabViewData{'sheetId'}}) . '&photos=',
                                     $locale->t('Télécharger l\'archive ZIP'),
                                     'left',
                                     {
                                         'id'    => 'zipBtn',
                                         'style' => 'visibility:hidden;'
                                     }) . '
        </div>
    </div>
</fieldset>';