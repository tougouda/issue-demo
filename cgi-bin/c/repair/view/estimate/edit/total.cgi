use utf8;
use open (':encoding(UTF-8)');

my $align = ($locale->doesCurrencySymbolPrecedes() ? 'left' : 'right');

my $totCstInput = $view->displayFormInputHidden('totalCost', $tabViewData{'totalCost'});

my $totAmtInput = $view->displayFormInputCurrency('totalAmount', $tabViewData{'totalAmount'}, LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);

my $rdcPctInput = $view->displayFormInputText('reductionPercent',
                                              sprintf('%.2f', &LOC::Util::round($tabViewData{'reductionPercent'}, 2)),
                                              '',
                                              {'class'      => 'discount',
                                               'maxlength'  => 6,
                                               'onchange'   => 'changeGlobalReduction();',
                                               'tabindex'   => 1002
                                              });
if ($tabViewData{'tabRights'}->{'total'} =~ /^(|.*\|)reduction(|\|.*)$/)
{
    $rdcPctInput .= $view->displayJSBlock('Location.spinControlsManager.createSpinControl("reductionPercent", 0, 100, 1, 10, 2);');
}
else
{
    $rdcPctInput .= $view->displayJSBlock('Location.spinControlsManager.createSpinControl("reductionPercent", 0, 100, 1, 10, 2).setReadOnly();');
}

my $rdcAmtInput = $view->displayFormInputCurrency('reductionAmount', $tabViewData{'reductionAmount'}, LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);

my $fnlAmtInput = $view->displayFormInputCurrency('finalAmount', $tabViewData{'finalAmount'},
                                                  LOC::Html::CURRENCYMODE_TEXT, $currencySymbol, undef,
                                                  {'size' => '10', 'style' => 'font-weight: bold;'});

my $tipBreakEvenPoint = $locale->t("Attention, la remise est supérieure à %s%.<br />Vous risquez de descendre au-dessous du seuil de rentabilité.", $tabViewData{'maxReductionPercentForChangement'});
my $breakEvenPointElt = '<span class="warning" onmouseover="Location.toolTipsObj.show(\'' . $tipBreakEvenPoint . '\', {corner:\'tr\'});" onmouseout="Location.toolTipsObj.hide();"></span>';


print '
<fieldset id="total">
<legend>' . $view->displayImage('repair/estimate/total.png') . '&nbsp;'  . $locale->t('Total') . '</legend>

<table class="formTable" id="total">
<tbody>
    <tr style="vertical-align: baseline">
        <td class="label" style="padding-top: 5px">' . $locale->t('Total du devis') . '</td>
        <td colspan="2" style="padding-top: 5px"></td>
        <td style="text-align: ' . $align . '; padding-left: 20px; padding-top: 5px">' . $totAmtInput . $totCstInput . '</td>
    </tr>
    <tr style="vertical-align: baseline">
        <td class="label">' . $locale->t('Remise') . '</td>
        <td style="text-align: right; padding-left: 20px">' . $locale->t('%s %%', $rdcPctInput) . '</td>
        <td class="warning">' . $breakEvenPointElt . '</td>
        <td style="text-align: ' . $align . '; padding-left: 20px">' . $rdcAmtInput . '</td>
    </tr>
    <tr style="vertical-align: baseline" class="total">
        <td class="label">' . $locale->t('Total') . '</td>
        <td colspan="2" ></td>
        <td class="amount" style="text-align: ' . $align . '; padding-left: 20px; font-weight: bold;"><div>' . $fnlAmtInput . '</div></td>
    </tr>
</tbody>
</table>

</fieldset>';
