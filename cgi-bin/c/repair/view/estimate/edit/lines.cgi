use utf8;
use open (':encoding(UTF-8)');

#our $view;
#our %tabViewData;

print '
<div style="clear: both"></div>
<fieldset style="margin-top: 10px">
    <legend>' . $view->displayImage('repair/estimate/line.png') . '&nbsp;' . $locale->t('Lignes de devis') . '</legend>

    <table id="tableLine" class="standard" style="width: 100%">
        <tr>
            <th>' . $locale->t('Type') . '</th>
            <th>' . $locale->t('Désignation') . '</th>
            <th style="text-align: right">' . $locale->t('Quantité') . '</th>
            <th style="text-align: right">' . $locale->t('Prix unitaire') . '</th>
            <th style="text-align: right">' . $locale->t('Remise') . '</th>
            <th></th>
            <th style="text-align: right">' . $locale->t('PU Net') . '</th>
            <th style="text-align: right">' . $locale->t('Prix facturé') . '</th>
            <th>' . $locale->t('État') . '</th>
            <th style="width: 20px"></th>
        </tr>';

my $i = 0;
#Tableau contenant toutes les lignes de devis
my $tabLines = $tabViewData{'tabLines'};
while (my ($id, $line) = each(%$tabLines))
{
    # Id
    my $idInput = $view->displayFormInputHidden('line[' . $i . '][id]', $line->{'id'});
    
    # Type de ligne
    my $typeLabel = '';
    if ($line->{'type'} eq LOC::Repair::LineType::TYPE_CHANGEMENT)
    {
        $typeLabel = $locale->t('Changement de pièce');
    }
    elsif ($line->{'type'} eq LOC::Repair::LineType::TYPE_REPAIR)
    {
        $typeLabel = $locale->t('Grosse réparation');
    }
    elsif ($line->{'type'} eq LOC::Repair::LineType::TYPE_FIX)
    {
        $typeLabel = $locale->t('Petite rénovation');
    }
    elsif ($line->{'type'} eq LOC::Repair::LineType::TYPE_SERVICE)
    {
        $typeLabel = $locale->t('Prestation technique');
    }
    
    my $typeInput = $view->displayFormInputHidden('line[' . $i . '][type]', $line->{'type'});
    $typeInput .= $view->toHTMLEntities($typeLabel);
    
    # Description
    my $labelInput = $view->displayFormInputHidden('line[' . $i . '][label]', $line->{'label'});
    $labelInput .= $line->{'label'};
    
    # Quantité
    my $qtyInput = $view->displayFormInputHidden('line[' . $i . '][quantity]', $line->{'quantity'});
    $qtyInput .= $line->{'quantity'};

    # Prix unitaire
    my $untAmtInput = $view->displayFormInputCurrency('line[' . $i . '][unitAmount]',
                                                      &LOC::Util::round($line->{'unitAmount'}, $locale->getFracDigits()),
                                                      LOC::Html::CURRENCYMODE_TEXT,
                                                      $currencySymbol);

    # Montant brut
    my $amtInput = $view->displayFormInputHidden('line[' . $i . '][amount]',
                                                 &LOC::Util::round($line->{'amount'}, $locale->getFracDigits()));

    # Pourcentage de remise
    my $rdcPctInput = $view->displayFormInputText('line[' . $i . '][reductionPercent]',
                                                  sprintf('%.2f', &LOC::Util::round($line->{'reductionPercent'}, 2)),
                                                  '',
                                                  {
                                                    'tabindex'  => $tabIndex++,
                                                    'class'     => 'discount',
                                                    'maxlength' => 6,
                                                    'onchange'  => 'changeLineReduction(' . $i . ');'
                                                  });
                                                  
    #Si la ligne est à facturer et le devis est modifiable
    if ($line->{'isToInvoice'} && $tabViewData{'tabRights'}->{'lines'} =~ /^(|.*\|)reduction(|\|.*)$/)
    {
        $rdcPctInput .= $view->displayJSBlock('Location.spinControlsManager.createSpinControl("line[' . $i . '][reductionPercent]", 0, 100, 1, 10, 2);');
    }
    else
    {
        $rdcPctInput .= $view->displayJSBlock('Location.spinControlsManager.createSpinControl("line[' . $i . '][reductionPercent]", 0, 100, 1, 10, 2).disable();');
    }

    # Prix unitaire net (remisé)
    my $fnlUntAmt = &LOC::Util::round($line->{'unitAmount'} * (1 - $line->{'reductionPercent'} / 100), $locale->getFracDigits());#$locale->getFracDigits renvoie le nb de chiffre aps la virgule par pays
    my $fnlUntAmtInput = $view->displayFormInputCurrency('line[' . $i . '][finalUnitAmount]',
                                                         $fnlUntAmt,
                                                         LOC::Html::CURRENCYMODE_TEXT,
                                                         $currencySymbol);

    # Montant total remisé
    my $fnlAmtInput = $view->displayFormInputCurrency('line[' . $i . '][finalAmount]',
                                                      &LOC::Util::round($line->{'finalAmount'}, $locale->getFracDigits()),
                                                      LOC::Html::CURRENCYMODE_TEXT,
                                                      $currencySymbol);

    # Coût
    my $cstInput = $view->displayFormInputHidden('line[' . $i . '][cost]', $line->{'cost'});

    # Etat
    my $toInvInput = $view->displayFormInputHidden('line[' . $i . '][isToInvoice]', $line->{'isToInvoice'});
    
    my $invFlags = '<span id="spLineFlagToInvc_' . $i . '" style="display: ' . ($line->{'isToInvoice'} ? 'block;' : 'none;') . '">' .
                        $view->displayImage('flagGreen.png', $locale->t('Facturé')) . '
                    </span>';
    $invFlags   .= '<span id="spLineFlagNoInvc_' . $i . '" style="display: ' . ($line->{'isToInvoice'} ? 'none;' : 'block;') . '">' .
                        $view->displayImage('flagRed.png', $locale->t('Non facturé')) . '
                    </span>';
    
    # Boutons
    my $invBtns = '<span id="spLineBtnToInvc_' . $i . '" style="display: ' . ($line->{'isToInvoice'} ? 'none;' : 'block;') . '">' .
                        $view->displayTextButton('', 'validGreen(|Over).png', 'changeLineInvoicing(' . $i . ');', $locale->t('Facturer la ligne')) . '
                    </span>';
    $invBtns   .= '<span id="spLineBtnNoInvc_' . $i . '" style="display: ' . ($line->{'isToInvoice'} ? 'block;' : 'none;') . '">' .
                        $view->displayTextButton('', 'delete(|Over).png', 'changeLineInvoicing(' . $i . ');', $locale->t('Ne pas facturer la ligne')) . '
                    </span>';


    # Style de la ligne : barrée si la ligne ne doit pas être facturée
    my $isInvoiceable = ($tabViewData{'tabRights'}->{'lines'} =~ /^(|.*\|)invoice(|\|.*)$/);

    my $class = (!$line->{'isToInvoice'} ? 'uninvoiced' : '');
    my $tipBreakEvenPoint = $locale->t("Attention, la remise est supérieure à %s%.<br />Vous risquez de descendre au-dessous du seuil de rentabilité.", $tabViewData{'maxReductionPercentForChangement'});
    my $breakEvenPointElt = '<span class="warning" onmouseover="Location.toolTipsObj.show(\'' . $tipBreakEvenPoint . '\', {corner:\'tl\'});" onmouseout="Location.toolTipsObj.hide();"></span>';

    print '
        <tr id="trLine_' . $i . '" class="' . $class . '">
            <td id="tdLineTyp_' . $i . '">' . $idInput . $typeInput . '</td>
            <td id="tdLineLbl_' . $i . '">' . $labelInput . '</td>
            <td id="tdLineQty_' . $i . '" style="text-align: right;">' . $qtyInput . '</td>
            <td id="tdLineUntAmt_' . $i . '" style="text-align: right;">' . $untAmtInput . '</td>
            <td id="tdLineRdcPct_' . $i . '" style="text-align: right;">' . $locale->t('%s %%', $rdcPctInput) . '</td>
            <td id="tdLineWarning_' . $i . '">' . $breakEvenPointElt . '</td>
            <td id="tdLineFnlUntAmt_' . $i . '" style="text-align: right;">' . $fnlUntAmtInput . '</td>
            <td id="tdLineFnlAmt_' . $i . '" style="text-align: right;">' . $fnlAmtInput . $amtInput . $cstInput . '</td>
            <td id="tdLineSta_' . $i . '">' . $toInvInput . $invFlags . '</td>
            <td id="tdLineBtn_' . $i . '" style="text-align: right">' . ($isInvoiceable ? $invBtns : '&nbsp;') . '</td>
        </tr>';
        
    $i++;
}

print '
    </table>

</fieldset>';