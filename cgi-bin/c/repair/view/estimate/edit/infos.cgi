use utf8;
use open (':encoding(UTF-8)');


# Commentaires client
my %inputOptions = ('cols' => 35, 'rows' => 4);
if ($tabViewData{'tabRights'}->{'supplements'} !~ /^(|.*\|)comments(|\|.*)$/)
{
    $inputOptions{'disabled'} = 'readonly';
}
$inputOptions{'tabindex'} = 1000;

my $commentsInput = $view->displayFormTextArea('comments', $tabViewData{'comments'}, \%inputOptions);

my $commentBtn = '';
if ($tabViewData{'sheet'}->{'sheetComment'} ne '' && !$inputOptions{'disabled'})
{
    my $onclick = 'var el = $ge(\'comments\'); ' .
                  'el.value += (el.value != \'\' ? \'\x0A\' : \'\') + \'' . $view->toJSEntities($tabViewData{'sheet'}->{'sheetComment'}) . '\'; ';
    $commentBtn = '<a style="margin-left: 8px;" href="#" onclick="' . $onclick . '">' .
                         $view->displayImage('copy(|Over).png', $locale->t('Coller les commentaires de la fiche de remise en état')) .
                  '</a>';
}


# Commentaires internes
%inputOptions = ('cols' => 35, 'rows' => 4);
if ($tabViewData{'tabRights'}->{'supplements'} !~ /^(|.*\|)notes(|\|.*)$/)
{
    $inputOptions{'disabled'} = 'readonly';
}
$inputOptions{'tabindex'} = 1001;

my $notesInput = $view->displayFormTextArea('notes', $tabViewData{'notes'}, \%inputOptions);



print '
<fieldset id="infos">
<legend>' . $view->displayImage('repair/estimate/comments.png') . '&nbsp;' . $locale->t('Informations complémentaires') . '</legend>

<table class="formTable">
<tbody>
    <tr>
        <td class="label">'  . $locale->t('Commentaires client') . $commentBtn . '</td>
        <td class="label">'  . $locale->t('Commentaires internes') . '</td>
    </tr>
    <tr>
        <td>'  . $commentsInput . '</td>
        <td>'  . $notesInput . '</td>
    </tr>
</tbody>
</table>

</fieldset>';