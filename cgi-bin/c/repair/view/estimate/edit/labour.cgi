use utf8;
use open (':encoding(UTF-8)');

print '<fieldset style="margin-top: 5px">
<legend>' . $view->displayImage('repair/estimate/service.png') . '&nbsp;' . $locale->t('Autres prestations') . '</legend>

<table id="tableService" class="standard" style="width: 100%">

<tr style="vertical-align: baseline">
<th>' . $locale->t('Désignation'). '</th>
<th style="text-align: right;">' . $locale->t('Quantité') . '</th>
<th style="text-align: right;">' . $locale->t('Prix unitaire') . '</th>
<th style="text-align: right;">' . $locale->t('Remise') . '</th>
<th style="text-align: right;">' . $locale->t('PU Net') . '</th>
<th style="text-align: right;">' . $locale->t('Prix facturé') . '</th>
<th>' . $locale->t('État') . '</th>
<th style="text-align: right;">&nbsp;</th>';

my %inputOptions;

my $i = 0;
my $services = $tabViewData{'tabServices'};
while (my ($id, $srv) = each(%$services))
{
    # Id
    my $srvIdInput = $view->displayFormInputHidden('service[' . $i . '][id]', $srv->{'id'});
    
    # Type
    my $srvTypeInput = $view->displayFormInputHidden('service[' . $i . '][type]', $srv->{'type'});

    # IsDeleted : le service est-il supprimé (1:Oui, 0: Non)
    my $srvIsDeletedInput = $view->displayFormInputHidden('service[' . $i . '][isDeleted]', 0);

    
    # Service / Libellé
    my $srvLabelInput;
    # Cas des Autres prestas qui seront rajoutés (par défaut Frais de port)
    if ($srv->{'type'} eq LOC::Repair::LineType::TYPE_SERVICE)
    {
        %inputOptions = ();
        if ($tabViewData{'tabRights'}->{'services'} !~ /^(|.*\|)add(|\|.*)$/)
        {
            $inputOptions{'disabled'} = 'disabled';
            $srvLabelInput = $view->displayFormInputHidden('service[' . $i . '][serviceId]', $srv->{'service.id'});
        }
        else
        {
            $srvLabelInput = '';
        }
        $inputOptions{'tabindex'} = $tabIndex++;
        $srvLabelInput .= $view->displayFormSelect('service[' . $i . '][serviceId]',
                                                  $tabViewData{'otherServices'}, $srv->{'service.id'}, 0, \%inputOptions);
        
    }
    else
    {
        my $label = '';

        if ($srv->{'type'} eq LOC::Repair::LineType::TYPE_LABOUR)
        {
            $label = $locale->t('Main-d\'œuvre');
        }
        elsif ($srv->{'type'} eq LOC::Repair::LineType::TYPE_TRAVELTIME)
        {
            $label = $locale->t('Temps de trajet');
        }
        elsif ($srv->{'type'} eq LOC::Repair::LineType::TYPE_DISPLACEMENT)
        {
            $label = $locale->t('Déplacement');
        }
        elsif ($srv->{'type'} eq LOC::Repair::LineType::TYPE_RESIDUES)
        {
            $label = $locale->t('Participation au recyclage');
        }

        $srvLabelInput  = $view->displayFormInputHidden('service[' . $i . '][serviceId]', 0);
        $srvLabelInput .= $label;
    }
    
    # Quantité
    my $srvQtyInput;
    if ($srv->{'type'} eq LOC::Repair::LineType::TYPE_SERVICE)
    {
        %inputOptions = ('style' => 'text-align: right;width: 40px;', 'onchange' => 'changeService(' . $i . ')');
        $inputOptions{'tabindex'} = $tabIndex++;
        $srvQtyInput = $view->displayFormInputText('service[' . $i . '][quantity]', $srv->{'quantity'}, 
                                                   {'class' => 'spinInput'}, \%inputOptions);
                                                   
        if ($tabViewData{'tabRights'}->{'services'} =~ /^(|.*\|)quantity(|\|.*)$/)
        {
            $srvQtyInput .= $view->displayJSBlock('Location.spinControlsManager.createSpinControl("service[' . $i . '][quantity]", 1, null, 1, 10, 0);');
        }
        else
        {
            $srvQtyInput .= $view->displayJSBlock('Location.spinControlsManager.createSpinControl("service[' . $i . '][quantity]", 1, null, 1, 10, 0).setReadOnly();');
        }
    }
    else
    {
        $srvQtyInput = $view->displayFormInputHidden('service[' . $i . '][quantity]', $srv->{'quantity'});
        if (&LOC::Util::in_array($srv->{'type'}, [LOC::Repair::LineType::TYPE_LABOUR, LOC::Repair::LineType::TYPE_TRAVELTIME]))
        {
            $srvQtyInput .= int($srv->{'quantity'}) . ' h ' . 
            &LOC::Util::round(($srv->{'quantity'} - int($srv->{'quantity'})) * 30 / 0.5, 0) . ' min';
        }
        elsif ($srv->{'type'} eq LOC::Repair::LineType::TYPE_DISPLACEMENT)
        {
            $srvQtyInput .= $srv->{'quantity'} . ' km';
        }
        else
        {
            $srvQtyInput .= $srv->{'quantity'};
        }
    }
    
    
    my $srvCurMode;
    if ($srv->{'type'} eq LOC::Repair::LineType::TYPE_SERVICE)
    {
        $srvCurMode = LOC::Html::CURRENCYMODE_POSITIVEINPUT;
    }
    else
    {
        $srvCurMode = LOC::Html::CURRENCYMODE_TEXT;
    }

    # Prix unitaire
    %inputOptions = ('style' => 'width: 70px;', 'onchange' => 'changeService(' . $i . ');');
    if ($tabViewData{'tabRights'}->{'services'} !~ /^(|.*\|)unitPrice(|\|.*)$/)
    {
        $inputOptions{'readonly'} = 'readonly';
    }
    $inputOptions{'tabindex'} = $tabIndex++;
    my $srvUntAmtInput = $view->displayFormInputCurrency('service[' . $i . '][unitAmount]',
                                                         &LOC::Util::round($srv->{'unitAmount'}, $locale->getFracDigits()),
                                                         $srvCurMode, $currencySymbol, undef, \%inputOptions);
    
    # Prix brut non remisé
    my $srvAmtInput = $view->displayFormInputHidden('service[' . $i . '][amount]',
                                                    &LOC::Util::round($srv->{'amount'}, $locale->getFracDigits()));
    
    # Pourcentage de remise
    my $srvRdcPctInput = '0';
    my $isServiceModifiable = ($tabViewData{'tabRights'}->{'services'} =~ /^(|.*\|)reduction(|\|.*)$/);
    if ($srv->{'type'} ne LOC::Repair::LineType::TYPE_RESIDUES)
    {
        $srvRdcPctInput = $view->displayFormInputText('service[' . $i . '][reductionPercent]',
                                                         sprintf('%.2f', &LOC::Util::round($srv->{'reductionPercent'}, 2)),
                                                         '',
                                                         {
                                                          'class'       => 'discount',
                                                          'maxlength'   => 6,
                                                          'tabindex'    => $tabIndex++,
                                                          'onchange'    => 'changeService(' . $i . ')'
                                                         });

        my $discountJs = 'Location.spinControlsManager.createSpinControl("service[' . $i . '][reductionPercent]", 0, 100, 1, 10, 2)' . ($isServiceModifiable ? '' : '.disable()') . ';';
        $srvRdcPctInput .= $view->displayJSBlock($discountJs);
    }

    # PU net
    my $srvFnlUntAmt = &LOC::Util::round($srv->{'unitAmount'} * (1 - $srv->{'reductionPercent'} / 100), $locale->getFracDigits());
    my $srvFnlUntAmtInput = $view->displayFormInputCurrency('service[' . $i . '][finalUnitAmount]',
                                                            $srvFnlUntAmt,
                                                            LOC::Html::CURRENCYMODE_TEXT,
                                                            $currencySymbol);

    # Prix facturé
    my $srvFnlAmtInput = $view->displayFormInputCurrency('service[' . $i . '][finalAmount]',
                                                         &LOC::Util::round($srv->{'finalAmount'}, $locale->getFracDigits()),
                                                         LOC::Html::CURRENCYMODE_TEXT,
                                                         $currencySymbol);

    # Coût
    %inputOptions = ('style' => 'width: 70px;', 'onchange' => 'changeService(' . $i . ');');
    if ($tabViewData{'tabRights'}->{'services'} !~ /^(|.*\|)cost(|\|.*)$/)
    {
        $inputOptions{'readonly'} = 'readonly';
    }
    $inputOptions{'tabindex'} = $tabIndex++;
    my $srvCstInput = $view->displayFormInputHidden('service[' . $i . '][cost]', $srv->{'cost'});

    # État
    my $toInvInput = $view->displayFormInputHidden('service[' . $i . '][isToInvoice]', $srv->{'isToInvoice'});

    my $invFlags = '<span id="spServiceFlagToInvc_' . $i . '" style="display: ' . ($srv->{'isToInvoice'} ? 'block;' : 'none;') . '">' .
                        $view->displayImage('flagGreen.png', $locale->t('Facturé')) . '
                    </span>';
    $invFlags   .= '<span id="spServiceFlagNoInvc_' . $i . '" style="display: ' . ($srv->{'isToInvoice'} ? 'none;' : 'block;') . '">' .
                        $view->displayImage('flagRed.png', $locale->t('Non facturé')) . '
                    </span>';

    # Bouton de suppression
    my $srvRemBtn = '';
    if ($tabViewData{'tabRights'}->{'services'} =~ /^(|.*\|)delete(|\|.*)$/ && $srv->{'type'} eq LOC::Repair::LineType::TYPE_SERVICE)
    {
        $srvRemBtn = $view->displayTextButton('', 'remove(|Over).png', 'removeRowService(' . $i . ')',
                                              htmllocale('Supprimer cette prestation'));
    }

    # Boutons de facturation/non facturation
    my $invBtns = '';
    if ($isServiceModifiable && $srv->{'type'} eq LOC::Repair::LineType::TYPE_RESIDUES)
    {
        $invBtns  = '<span id="spServiceBtnToInvc_' . $i . '" style="display: ' . ($srv->{'isToInvoice'} ? 'none;' : 'block;') . '">' .
                         $view->displayTextButton('', 'validGreen(|Over).png', 'changeLineInvoicing(' . $i . ', "service");', $locale->t('Facturer la ligne')) . '
                     </span>';
        $invBtns .= '<span id="spServiceBtnNoInvc_' . $i . '" style="display: ' . ($srv->{'isToInvoice'} ? 'block;' : 'none;') . '">' .
                         $view->displayTextButton('', 'delete(|Over).png', 'changeLineInvoicing(' . $i . ', "service");', $locale->t('Ne pas facturer la ligne')) . '
                     </span>';
    }

    my $class = ($srv->{'type'} eq LOC::Repair::LineType::TYPE_RESIDUES && !$srv->{'isToInvoice'} ? 'uninvoiced' : '');

    print '
<tr id="trService_' . $i . '"' . ($srv->{'type'} eq LOC::Repair::LineType::TYPE_RESIDUES ? ' residues="1"' : '') . ' style="vertical-align: baseline" class="' . $class . '">
    <td id="tdServiceLbl_' . $i .'" class="label">' . $srvIdInput . $srvTypeInput . $srvIsDeletedInput . $srvLabelInput . '</td>
    <td id="tdServiceQty_' . $i .'" style="text-align: right;">' . $srvQtyInput . '</td>
    <td id="tdServiceUntAmt_' . $i .'" style="text-align: right;">' . $srvUntAmtInput . '</td>
    <td id="tdServiceRdcPct_' . $i .'" style="text-align: right;">' . $locale->t('%s %%', $srvRdcPctInput) . '</td>
    <td id="tdServiceFnlUntAmt_' . $i .'" style="text-align: right;">' . $srvFnlUntAmtInput . '</td>
    <td id="tdServiceFnlAmt_' . $i .'" style="text-align: right;">' . $srvFnlAmtInput . $srvAmtInput . $srvCstInput . '</td>
    <td id="tdServiceSta_' . $i . '">' . $toInvInput . $invFlags . '</td>
    <td id="tdServiceRemBtn_' . $i .'" style="text-align: right;">' . $srvRemBtn . $invBtns . '</td>
</tr>';

    $i++;
}#Fin du While

my $isAddable = ($tabViewData{'tabRights'}->{'services'} =~ /^(|.*\|)add(|\|.*)$/);
print '
<tr>
    <td colspan="7">&nbsp;</td>
    <td  style="text-align: right;">' . ($isAddable ?
        $view->displayTextButton('', 'add(|Over).png', 'addRowService();', htmllocale('Ajouter')) :
        '&nbsp;') . '
    </td>
</tr>

</table>

</fieldset>';

print '
<div style="display: none;">' .
    $view->displayFormInputHidden('serviceType_blank', LOC::Repair::LineType::TYPE_SERVICE) .
    $view->displayFormInputHidden('serviceIsDeleted_blank', 0) .
    $view->displayFormSelect('serviceServiceId_blank', $tabViewData{'otherServices'}, '', 0, {'tabindex' => $tabIndex++}) .
    $view->displayFormInputText('serviceQuantity_blank', 1, '',
                                {'size' => 8, 'style' => 'text-align: right;', 'tabindex' => $tabIndex++}) .
    $view->displayFormInputCurrency('serviceUnitAmount_blank', 0, LOC::Html::CURRENCYMODE_INPUT, $currencySymbol, undef,
                                    {'style' => 'width: 70px;', 'tabindex' => $tabIndex++}) .
    $view->displayFormInputHidden('serviceAmount_blank', 0) .
    $view->displayFormInputText('serviceReductionPercent_blank', 0, '',
                                {'class' => 'discount', 'maxlength' => 6, 'tabindex' => $tabIndex++}) .
    $view->displayFormInputCurrency('serviceFinalUnitAmount_blank', 0, LOC::Html::CURRENCYMODE_TEXT, $currencySymbol) .
    $view->displayFormInputCurrency('serviceFinalAmount_blank', 0, LOC::Html::CURRENCYMODE_TEXT,$currencySymbol) .
    $view->displayFormInputCurrency('serviceCost_blank', 0, LOC::Html::CURRENCYMODE_INPUT, $currencySymbol, undef,
                                    {'style' => 'width: 70px;', 'tabindex' => $tabIndex++}) .
    $view->displayFormInputHidden('serviceToInvc_blank', 1) . '
    <span id="spanServiceFlagToInvc">' .
        $view->displayImage('flagGreen.png', $locale->t('Facturé')) . '
    </span>
    <span id="spanServiceRembtn">' .
        $view->displayTextButton('', 'remove(|Over).png', '', htmllocale('Supprimer cette prestation')) . '
    </span>
</div>';