use utf8;
use open (':encoding(UTF-8)');

# CSS
$view->addCSSSrc('repair/estimate.css');

# JS
$view->addJSSrc('repair/estimate.js');


# n° du devis
my $noInput = $view->displayFormInputText('repairEstimate', $tabViewData{'repairEstimate'}, '',
                                          {'size' => 10, 'maxlength' => '45',
                                           'tabindex' => $inputsTabIndex++});

# Etat
tie(my %tabStates, 'Tie::IxHash');
$tabStates{''} = '-- ' . htmllocale('Tous') . ' --';
foreach my $stateId (keys(%{$tabViewData{'tabStates'}}))
{
    $tabStates{$stateId} = $tabViewData{'tabStates'}->{$stateId};
}
my $statesInput = $view->displayFormSelect('stateId', \%tabStates,  $tabViewData{'state.id'}, 0, {'tabindex' => $inputsTabIndex++});

# Client
my $custListUrl = $view->createURL('accounting:customer:jsonList', {'countryId' => $tabViewData{'countryId'}, 'format' => 0});
my $custInput = $view->displayFormSelect('customerId', $tabViewData{'customers'}, $tabViewData{'customerId'}, 0, {'tabindex' => $inputsTabIndex}) .
                $view->displayJSBlock('
Location.searchBoxesManager.createSearchBox("customerId", {isRequired: false, url: "' . $custListUrl . '"}).createElement = function(key, value)
{
    return new Option((value["name"] || "") + ", " + (value["locality.postalCode"] || ""), value["id"]);
}');
$inputsTabIndex += 2;

# n° de parc
my $parkInput = $view->displayFormInputText('parkNumber', $tabViewData{'parkNumber'}, '',
                                            {'size' => 5, 'maxlength' => '45', 'tabindex' => $inputsTabIndex++});

# National
my $isNationalInput = $view->displayFormInputCheckBox(
        'isNational',
        $tabViewData{'isNational'},
        {
            'tabindex' => $inputsTabIndex
        },
        '',
        1
    );
$inputsTabIndex++;

# n° de contrat
my $rentContractInput = $view->displayFormInputText('rentContract', $tabViewData{'rentContract'}, '',
                                                    {'size' => 16, 'maxlength' => '45', 'tabindex' => $inputsTabIndex++});

# date début
my $startDate = $view->displayFormInputDate('startDate', ($tabViewData{'startDate'} eq '' ? undef : $tabViewData{'startDate'}), undef, undef, {'tabindex' => $inputsTabIndex++});

# date fin
my $endDate = $view->displayFormInputDate('endDate', ($tabViewData{'endDate'} eq '' ? undef : $tabViewData{'endDate'}), undef, undef, {'tabindex' => $inputsTabIndex++});

# Utilisateurs
my $userListUrl = $view->createURL('default:user:jsonList', {'format' => 1});
my $interlocInput = $view->displayFormSelect('search.user.id', $tabViewData{'users'}, $tabViewData{'search.user.id'}, 0, {'tabindex' => $inputsTabIndex}) .
                    $view->displayJSBlock('Location.searchBoxesManager.createSearchBox("search.user.id", {isRequired: false, url: "' . $userListUrl . '"});');
$inputsTabIndex += 2;



my $tabLines = [
    [
        {'name' => $locale->t('N°'), 'input' => $noInput},
        {'name' => $locale->t('État'), 'input' => $statesInput},
        {'name' => $locale->t('Client'), 'help' => $locale->t('La recherche de client s\'effectue parmi la raison sociale, le code client, la localité, le code postal et le département'), 'input' => $custInput},
        {'name' => $locale->t('Machine'), 'input' => $parkInput},
        {'name' => $locale->t('Rech. nat.'), 'help' => $locale->t('Effectue une recherche au niveau national'), 'input' => $isNationalInput}
    ],
    [
        {'name' => $locale->t('N° contrat'), 'input' => $rentContractInput},
        {'name' => $locale->t('Début'), 'input' => $startDate},
        {'name' => $locale->t('Fin'), 'input' => $endDate}
    ],
    [
        {'name' => $locale->t('Interlocuteur'), 'input' => $interlocInput}
    ]
];

# Fonction de vidage des champs de recherche
$view->addJSSrc('
function goSearch()
{
    if (LOC_Common.addEltClass(\'searchBar\', \'search\'))
    {
        window.document.getElementsByName("searchBar.form")[0].submit();
        return true;
    }
    return false;
}
function clearSearch()
{
    $ge("rentContract").value = "";
    $ge("parkNumber").value = "";
    $ge("repairEstimate").value = "";
    $ge("stateId").value = "";
    Location.searchBoxesManager.getSearchBox("search.user.id").clear();
    Location.searchBoxesManager.getSearchBox("customerId").clear();
    clearMaskedInputValue("startDate");
    clearMaskedInputValue("endDate");
    $ge("isNational").checked = false;
}');


# Début du formulaire de recherche
my $searchUrl = $view->createURL('repair:estimate:view',{'fromSearchBar' => '1'});
my $searchBar = '
<form method="post" action="' . $searchUrl . '" onsubmit="return goSearch();" autocomplete="off" name="searchBar.form">' .
    $view->displaySearchBar('searchBar', $tabLines,
                            {'onsearch' => 'goSearch()',
                             'onclear'  => 'clearSearch()'}) . '
    <div class="hidden-submit"><input type="submit" tabindex="-1"/></div>
</form>';

$view->addControlsContent($searchBar, 'left', 1);

