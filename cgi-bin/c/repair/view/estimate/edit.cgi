use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;
use LOC::Locale;
use LOC::Common;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


our $inputsTabIndex = 1;
our %tabViewData;
our $view;
our $locale;
our $currencySymbol = $tabViewData{'currencySymbol'};

$locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
$view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


our $isNew = $tabViewData{'id'} ? 0 : 1; # Ajout ou modification
# Devis modifiable :
# utilisateur autorisé et devis en attente ou nouveau devis
our $tabIndex = 1;

# Affichage et prise en compte des alertes sur les remises des lignes
$view->addBodyEvent('load', 'checkLinesReducPercent();');

if ($isNew == 0)
{
    my $reducMode = $tabViewData{'reductionPercent'} > 0 ? 0 : 1;
    $view->addBodyEvent('load', 'setOnLoadReducMode(' . $reducMode . ');');
}

if ($isNew == 0)
{
    $view->setPageTitle($tabViewData{'code'});
}
else
{
    $view->setPageTitle($locale->t('Devis de remise en état'));
}

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle($locale->t('Devis de remise en état'));

# Packages
$view->addPackage('spincontrols')
     ->addPackage('popups')
     ->addPackage('searchboxes')
     ->addPackage('modalwindow');;

# CSS
$view->addCSSSrc('location.css');
$view->addCSSSrc('repair/estimate.css');

#Blocage de la touche F5 sur le DRE
$view->addBodyEvent('onkeydown', 'LOC_View.lockKeys(event, [116], LOC_View.getTranslation(\'lockKey\'));');

#Blocage de la touche "BACKSPACE" lorsque devis non éditable
if ($tabViewData{'tabRights'}->{'actions'} !~ /^(|.*\|)valid(|\|.*)$/)
{
    $view->addBodyEvent('onkeydown', 'if (event.keyCode == 8) return false;');
}

# JS
$view->addJSSrc('repair/estimate.js');

# Traductions
$view->addTranslations({
    'confirmResetGlobalReduc'   => $locale->t('La remise globale va être annulée, voulez-vous continuer ?'),
    'confirmResetLinesReduc'    => $locale->t('Les remises à la ligne vont être annulées, voulez-vous continuer ?'),
    'confirmRemoveService'      => $locale->t('Souhaitez-vous supprimer cette prestation ?'),
    'confirmTurnIntoServices'   => $locale->t('Voulez-vous vraiment basculer le devis en services sur le contrat ?'),
    'confirmTurnIntoDays'       => $locale->t('Voulez-vous vraiment basculer le devis en jours sur le contrat ?'),
    'confirmAbortEstimate'      => $locale->t('Voulez-vous vraiment abandonner le devis ?'),
    'confirmInvoice'            => $locale->t('Voulez-vous vraiment envoyer le devis en facturation ?'),
    'confirmReactivate'         => $locale->t('Voulez-vous vraiment remettre en cours le devis ?'),
    'confirmIgnoreReducWarning' => $locale->t("Attention, la remise est supérieure à %s%.\nVous risquez de descendre au-dessous du seuil de rentabilité.", $tabViewData{'maxReductionPercentForChangement'}),
    'lockKey'                   => $locale->t('Merci de ne pas appuyer sur cette touche !'),
});

$view->addJSSrc('
var TYPE_CHANGEMENT      = "' . LOC::Repair::LineType::TYPE_CHANGEMENT . '";
var TYPE_REPAIR          = "' . LOC::Repair::LineType::TYPE_REPAIR . '";
var TYPE_FIX             = "' . LOC::Repair::LineType::TYPE_FIX . '";
var TYPE_SERVICE         = "' . LOC::Repair::LineType::TYPE_SERVICE . '";
var TYPE_LABOUR          = "' . LOC::Repair::LineType::TYPE_LABOUR . '";
var TYPE_DISPLACEMENT    = "' . LOC::Repair::LineType::TYPE_DISPLACEMENT . '";
var TYPE_RESIDUES        = "' . LOC::Repair::LineType::TYPE_RESIDUES . '";
var TYPE_INTERNALSERVICE = "' . LOC::Repair::LineType::TYPE_INTERNALSERVICE . '";
var TYPE_EXTERNALSERVICE = "' . LOC::Repair::LineType::TYPE_EXTERNALSERVICE . '";

/* Caractéristiques */
var MAXREDUCTIONPERCENT_CHANGEMENT = "' . $tabViewData{'maxReductionPercentForChangement'} . '";
');

# Éléments du formulaire

# champs cachés
my $htmlHidden = '';
$htmlHidden .= $view->displayFormInputHidden('id', $tabViewData{'id'});
$htmlHidden .= $view->displayFormInputHidden('sheetId', $tabViewData{'sheetId'});
$htmlHidden .= $view->displayFormInputHidden('agencyId', $tabViewData{'agencyId'});
$htmlHidden .= $view->displayFormInputHidden('isVatInvoiced', $tabViewData{'customer'}->{'accountingCategory'});
$htmlHidden .= $view->displayFormInputHidden('valid', $tabViewData{'valid'});
$htmlHidden .= $view->displayFormInputHidden('reactivateSheet', '0');

# Barre de recherche du haut
require($directory . '/estimate/searchBar.cgi');

# Message de confirmation
if ($tabViewData{'messages'}->{'valid'})
{
    my $validBlock = '
    <div id="validBlock" class="messageBlock valid" >
        <div class="popup" style="display: block;">
            <div class="content">
                <table>
                <tr>
                    <td style="font-weight: bold;">' . $locale->t($tabViewData{'messages'}->{'valid'}) . '.</td>
                </tr>
                <tr class="buttons">
                    <td>' .
                    $view->displayTextButton($locale->t('Ok'), '',
                                             '$ge("validBlock").style.display = "none";',
                                             '', 'l', {'id' => 'validBlockBtn'}) .
                    '</td>
                </tr>
                </table>
            </div>
        </div>
    </div>';
    
    $view->addBodyContent($validBlock, 0, 1);
    $view->addBodyEvent('load', 'window.document.getElementById("validBlockBtn").focus();');
}


#Message d'erreur
if ($tabViewData{'messages'}->{'error'})
{
    my $errorBlock = '
    <div id="errorBlock" class="messageBlock error" >
        <div class="popup" style="display: block;">
            <div class="content">
                <table>
                <tr>
                    <td style="font-weight: bold;">' . $locale->t($tabViewData{'messages'}->{'error'}) . '.</td>
                </tr>
                <tr class="buttons">
                    <td>' .
                    $view->displayTextButton($locale->t('Ok'), '',
                                             '$ge("errorBlock").style.display = "none";',
                                             '', 'l', {'id' => 'errorBlockBtn'}) .
                    '</td>
                </tr>
                </table>
            </div>
        </div>
    </div>';
    
    $view->addBodyContent($errorBlock, 0, 1);
}

# Popup de réouverture de fiche de remise en état
my $abortEstimatePopup = '
    <div style="display: none; padding: 10px;" id="abortEstimate" title="' . htmllocale('Abandonner le devis de remise en état') . '">
        <p>' . $locale->t('Vous êtes sur le point d\'abandonner le devis de remise en état n° %s.', '<span id="estimateNo"></span>') . '</p>
        <p>' . $locale->t('Souhaitez-vous continuer ?') . '</p>
        <p style="padding-top: 10px; text-align: center;">
            ' . $view->displayFormInputButton('estimateAbortNoBtn', $locale->t('Non')) . '<br /><br />
            ' . $view->displayFormInputButton('estimateAbortYesReopenSheetBtn', $locale->t('Oui et réactiver la fiche de remise en état correspondante')) . '<br /><br />
            ' . $view->displayFormInputButton('estimateAbortYesBtn', $locale->t('Oui mais sans réactiver la fiche de remise en état correspondante')) . '
        </p>
    </div>';

$view->addBodyContent($abortEstimatePopup, 0, 1);

# Affichage de l'entête
#Optimisation
$view->{'_networkOptimization'} = 'repair-estimate-edit';
print $view->displayHeader();

print $view->startForm();

# Inclusions
require $tabViewData{'scriptPath'} . 'edit/header.cgi';#En-tête
require $tabViewData{'scriptPath'} . 'edit/lines.cgi';#Lignes de devis
require $tabViewData{'scriptPath'} . 'edit/labour.cgi';#Autres prestations

my $nbPhotos = @{$tabViewData{'sheet'}->{'images'}};

print '
<div id="totalCtn">';

require $tabViewData{'scriptPath'} . 'edit/total.cgi';

print '
</div>';

print '
<div id="infosCtn">';

require $tabViewData{'scriptPath'} . 'edit/infos.cgi';

print '
</div>';

if ($nbPhotos > 0)
{
    print '
<div id="photosCtn">';

    require $tabViewData{'scriptPath'} . 'edit/photos.cgi';

    print '
</div>';
}


# Affichage des champs cachés
print '
    <div id="chpHiddens" style="display: none;">' .
        $htmlHidden . '
    </div>';

my @leftPart;
my @rightPart;

# Bouton de validation
if ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)valid(|\|.*)$/)
{
    my $btnValid = $view->displayTextButton(htmllocale('Valider'),
        'valid(|Over).png', 'submitForm()', $locale->t('Valider le devis de remise en état'));

    push(@leftPart, $btnValid);
}

#Bouton Rétablir
if ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)reinit(|\|.*)$/)
{

    my $btnReinit = $view->displayTextButton(htmllocale('Rétablir'), 'revert(|Over).png',
        'refresh();', $locale->t('Annuler les modifications effectuées sur le devis de remise en état'));

    push(@leftPart, $btnReinit);
}

# Boutons services et jours
if ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)services(|\|.*)$/)
{
    #Passage du devis en Services
    my $btnServices = $view->displayTextButton(htmllocale('Services'), 
                                               'repair/estimate/services(|Over).png',
                                               'turnIntoServices()', 
                                               $locale->t('Basculer le devis en services sur le contrat'));

    push(@rightPart, $btnServices);
}
if ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)days(|\|.*)$/)
{
    my $btnDays = $view->displayTextButton(htmllocale('Jours'), 
                                           'repair/estimate/days(|Over).png',
                                           'turnIntoDays()', 
                                           $locale->t('Basculer le devis en jours sur le contrat'));

    push(@rightPart, $btnDays);
}

# Bouton Remttre en cours le devis
if ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)reactivate(|\|.*)$/)
{
    my $btnReactivate = $view->displayTextButton(htmllocale('Remettre en cours'), 
                                                 'revert(|Over).png',
                                                 'reactivateEstimate()', 
                                                  $locale->t('Remettre en cours le devis'));

    push(@rightPart, $btnReactivate);
}

# Bouton de facturation
if ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)invoice(|\|.*)$/)
{
    my $btnInvoice = $view->displayTextButton(htmllocale('Facturer'), 
                                              'coins(|Over).png',
                                              'invoiceEstimate()', 
                                              $locale->t('Facturer le devis'));

    push(@rightPart, $btnInvoice);
}
# Bouton d'abandon
if ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)abort(|\|.*)$/)
{
    my $btnAbort = $view->displayTextButton(htmllocale('Abandonner'), 
                                             'cancel(|Over).png',
                                             'showAbortEstimatePopup("' . $tabViewData{'id'} . '", "' . $tabViewData{'sheetId'} . '")', 
                                             $locale->t('Abandonner le devis de remise en état'));

    push(@rightPart, $btnAbort);
}

# Boutons d'impression, édition en PDF et envoi par e-mail
if ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)print(|\|.*)$/)
{
    push(@rightPart, '<span></span>');

    my $pdfUrl = {'url' => 'repair:estimate:pdf', 'options' => {'estimateId' => $tabViewData{'id'}}};
    my $pdfName = $locale->t('AI_Devis_%s.pdf', $tabViewData{'code'});
    my $btnPrint = $view->displayPrintButtons(
                                               'printBtns',
                                               $pdfUrl,
                                               $pdfName,
                                               $tabViewData{'contract'}->{'orderContact.email'},
                                               $locale->t('Votre devis de remise en état'),
                                               $tabViewData{'emailContent'},
                                               $tabViewData{'agencyId'},
                                               {
                                                   'tabEndActions' => [
                                                       {
                                                           'type'      => 'logPrint',
                                                           'entity'    => 'DRE',
                                                           'entityIds' => [$tabViewData{'id'}]
                                                       }
                                                   ]
                                               }
                                              );

    push(@rightPart, $btnPrint);
}


# Affichage du panel
print $view->displayControlPanel({'left' => \@leftPart, 'right' => \@rightPart, 'substyle' => 'bottom'});


# Passage des constantes au JavaScript
print $view->displayJSBlock('
var MODE_NONE    = ' . LOC::Common::RESIDUES_MODE_NONE . ';
var MODE_PRICE   = ' . LOC::Common::RESIDUES_MODE_PRICE . ';
var MODE_PERCENT = ' . LOC::Common::RESIDUES_MODE_PERCENT . ';');


print $view->displayFooter();
