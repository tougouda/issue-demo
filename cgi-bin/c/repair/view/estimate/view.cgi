use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'repair/estimate/';


our $inputsTabIndex = 1;
our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
our $view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}

our $currencySymbol = $tabViewData{'currencySymbol'};

# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'search.css');
# Js
$view->addJSSrc($dir . 'search.js');

$view->setPageTitle($locale->t('Liste des devis de remise en état'))
     ->addPackage('searchboxes');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT);

$view->setTitle($locale->t('Devis de remise en état'));

$view->addTranslations({
    'label-result-s' => $locale->t('1 résultat'),
    'label-result-p' => $locale->t('%s / %s résultats', '<%nb>', '<%total>'),
    'label-see-more-s' => $locale->t('Voir %s résultat supplémentaire', '<%step>'),
    'label-see-more-p' => $locale->t('Voir %s résultats supplémentaires', '<%step>'),
    'flag_' . LOC::Repair::Estimate::FLAG_TURNEDINSERVICES => $locale->t('Fiche basculée en services'),
    'flag_' . LOC::Repair::Estimate::FLAG_TURNEDINDAYS     => $locale->t('Fiche basculée en jours'),
    'flag_' . LOC::Repair::Estimate::FLAG_SHEETUPDATED     => $locale->t('Modification sur la fiche')
});

foreach my $stateId (keys(%{$tabViewData{'tabStates'}}))
{
    $view->addTranslations({
        'state_' . $stateId => $view->displayState($stateId, $tabViewData{'tabStates'}->{$stateId})
    });
}

my $searchUrl = $view->createURL('repair:estimate:jsonSearch');
my $tabData = {
    'result' => $tabViewData{'result'},
    'total'  => $tabViewData{'total'}
};

$view->addBodyEvent('load', 'search.init("' . $searchUrl . '", ' .
                                &LOC::Json::toJson($tabViewData{'tabFilters'}) . ', ' .
                                &LOC::Json::toJson($tabData) . ',' .
                                $tabViewData{'offset'} . ', ' .
                                $tabViewData{'step'} . ');');



# Barre de recherche
require($directory . '/estimate/searchBar.cgi');

# Affichage de l'entête
print $view->displayHeader();


print '<h1 class="searchResult">' . $locale->t('Résultat de la recherche') . '</h1>';

# S'il n'y a pas assez de critères sélectionnés
print '<p id="msg-ok" class="result-msg"></p>';

print '
<table id="estimatesList" class="basic list">
    <thead>
        <tr>
            <th class="code">' . $locale->t('Devis') . '</th>
            <th class="customer">' . $locale->t('Client') . '</th>
            <th class="contractCode">' . $locale->t('Contrat') . '</th>
            <th class="machine">' . $locale->t('Machine') . '</th>
            <th class="object"><span>' . $locale->t('Objet') . '</span></th>
            <th class="total">' . $locale->t('Total HT') . '</th>
            <th class="state">' . $locale->t('Etat') . '</th>
            <th class="flag"></th>
        </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
        <tr class="pending-search">
            <td colspan="8">
                ' . $locale->t('Recherche en cours') . $view->displayImage('loading.gif') . '
            </td>
        </tr>
        <tr class="no-filters">
            <td colspan="8">' . $locale->t('Aucun critère de recherche sélectionné. Veuillez affiner votre recherche.') . '</td>
        </tr>
        <tr class="no-result">
            <td colspan="8">' . $locale->t('Aucun devis de remise en état ne correspond à votre recherche') . '</td>
        </tr>
        <tr class="more" id="search-more">
            <td colspan="8"></td>
        </tr>
        <tr class="error">
            <td colspan="8">' . $locale->t('Une erreur est survenue lors de la recherche.') . '</td>
        </tr>
    </tfoot>
</table>';

# Affichage du pied de page
print $view->displayFooter();

