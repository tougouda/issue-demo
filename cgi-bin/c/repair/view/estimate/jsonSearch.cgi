use strict;


use LOC::Json;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


our %tabViewData;


# Locale
my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});


my $view = LOC::Json->new();
print $view->displayHeader();
print $view->encode(\%tabViewData);
