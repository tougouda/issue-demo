use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;

our %tabViewData;
our $view;

$view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

my $sheetUrl = $view->createURL('repair:sheet:edit', {'sheetId' => $tabViewData{'sheetId'},
                                                        'toTurnInto' => $tabViewData{'valid'}
});

print "Location: $sheetUrl\n\n";