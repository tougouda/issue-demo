use utf8;
use open (':encoding(UTF-8)');
binmode STDOUT, ":raw";    # Pour envoyer le flux en l'état

use strict;
use locale;

use LOC::Pdf;
use LOC::Locale;
use LOC::Browser;



our %tabViewData;
our $var;

my $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});

my $margin = 10/mm;
my $pageWidth = 210/mm;
my $pageHeight = 297/mm;
my $pageLimit = 2 * $margin;
my $headerPageLimitCheck = 8/pt * 1.5 + 1/mm + 10/pt * 1.2;
my $gap = 2/pt;

my $pdf = LOC::Pdf->new($locale->t('Avis de facturation'), $var->{'agencyId'}, \$locale);
my $pdfObj = $pdf->getPdfObject();

my $regularFont    = $pdf->{'font'}->{'regular'};
my $italicFont     = $pdf->{'font'}->{'italic'};
my $boldFont       = $pdf->{'font'}->{'bold'};
my $boldItalicFont = $pdf->{'font'}->{'bolditalic'};
my $dingbatsFont   = $pdfObj->corefont('ZapfDingbats');



# Définition du sujet et des mots-clés
$pdf->setSubject($locale->t('Avis de remise en état'));
$pdf->setKeywords($locale->t('Avis'), $locale->t('Remise en état'));


# Documents PDF supplémentaires à insérer
my $tabAdditionalDocuments = $tabViewData{'tabAdditionalDocuments'};

# - Documents "avant"
&_displayAdditionalDocuments($pdf, $tabAdditionalDocuments, 'before');

# Nouvelle page
my $page = $pdf->addPage();
my $gfx = $page->gfx();
my $text = $page->text();



my $x;
my $y;
my $width;
my $height;



# Références
tie(my %tabReferences, 'Tie::IxHash');

# - Contact commande
my @tabOrderCttRef = ();
if ($var->{'orderContact.fullName'} ne '')
{
    push(@tabOrderCttRef, $var->{'orderContact.fullName'});
}
if ($var->{'orderContact.telephone'} ne '')
{
    my $tel = $locale->getPhoneNumberFormat($var->{'orderContact.telephone'});
    push(@tabOrderCttRef, $tel);
}

# - Contact chantier
my @tabSiteCttRef = ();
if ($var->{'site.contact.fullName'} ne '')
{
    push(@tabSiteCttRef, $var->{'site.contact.fullName'});
}
if ($var->{'site.contact.telephone'} ne '')
{
    my $tel = $locale->getPhoneNumberFormat($var->{'site.contact.telephone'});
    push(@tabSiteCttRef, $tel);
}


%tabReferences = (
    $locale->t('Contact commande') . ' :' => join(' ', @tabOrderCttRef),
    $locale->t('Contact chantier') . ' :' => join(' ', @tabSiteCttRef),
    $locale->t('Chantier') . ' :'         => $var->{'site.label'} . ' ' . $var->{'site.locality.label'}
);

$pdf->addReferencesBlock(\$page, \%tabReferences);



# Coordonnées client
my $tabCustomer = [$var->{'customer.name'}, $var->{'customer.address1'}, $var->{'customer.address2'},
                   $var->{'customer.locality'}, $locale->t('Tél. ') . $var->{'customer.tel'}, $locale->t('Fax ') . $var->{'customer.fax'}];
$pdf->addCustomerBlock(\$page, $tabCustomer);

#$text->textlabel($margin, $y-15/mm, $regularFont, 7/pt, $var->{'comment'});
$x = $margin;
$y = $pageHeight - 95/mm;
$text->translate($x, $y);
$text->font($regularFont, 10/pt);

#my $opt{-maxWidth} = $pageWidth - ($margin * 2);
my $amount = $locale->getCurrencyFormat($var->{'sheetAmount'}, undef, $var->{'currencySymbol'});
$var->{'comment'} =~ s/<%modelMachine>/$var->{'model.label'}/;
$var->{'comment'} =~ s/<%familyMachine>/$var->{'family.label'}/;
$var->{'comment'} =~ s/<%parkNumber>/$var->{'machine.parkNumber'}/;
$var->{'comment'} =~ s/<%contractCode>/$var->{'contract.code'}/;
$var->{'comment'} =~ s/<%sheetAmount>/$amount/;

my @tabLigne = split(/\n/, $var->{'comment'});
foreach my  $ligne (@tabLigne)
{
    $text->paragraph($ligne, $pageWidth - ($margin * 4));
    $text->paragraph(' ', $pageWidth - ($margin * 2));
}


# - Documents "après"
&_displayAdditionalDocuments($pdf, $tabAdditionalDocuments, 'after');


# Sortie
my $pdfString = $pdf->getString();
$pdf->end();

&LOC::Browser::sendHeaders(
        "Content-type: application/pdf;\n"
        . "Content-Disposition: inline; filename=\"avisRE.pdf\";\n\n"
    );

print $pdfString;


# Function: _displayAdditionalDocuments
# Affiche des documents supplémentaires
#
# Parameters:
# LOC::Pdf  $pdf          - Instance du document principal
# hashref   $tabDocuments - Tableau des documents
# string    $zone         - Zone d'affichage
sub _displayAdditionalDocuments
{
    my ($pdf, $tabDocuments, $zone) = @_;
    
    if (!defined $zone)
    {
        $zone = 'before';
    }

    if (defined $tabAdditionalDocuments->{$zone})
    {
        foreach my $documentInfos (@{$tabAdditionalDocuments->{$zone}})
        {
            if ($documentInfos->{'active'} && -e $documentInfos->{'path'})
            {
                $pdf->addFile($documentInfos->{'path'});
            }
        }
    }
}
