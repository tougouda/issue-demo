use utf8;
use open (':encoding(UTF-8)');
binmode STDOUT, ":raw";    # Pour envoyer le flux en l'état

use strict;
use locale;

use LOC::Pdf;
use LOC::Locale;
use LOC::Browser;



our %tabViewData;
our $var;

my $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});

my $margin = 10/mm;
my $pageWidth = 210/mm;
my $pageHeight = 297/mm;
my $pageLimit = 2 * $margin;
my $headerPageLimitCheck = 8/pt * 1.5 + 1/mm + 10/pt * 1.2;
my $gap = 2/pt;

my $pdf = LOC::Pdf->new($locale->t('Fiche de remise en état'), $var->{'agencyId'}, \$locale);
my $pdfObj = $pdf->getPdfObject();

my $regularFont    = $pdf->{'font'}->{'regular'};
my $italicFont     = $pdf->{'font'}->{'italic'};
my $boldFont       = $pdf->{'font'}->{'bold'};
my $boldItalicFont = $pdf->{'font'}->{'bolditalic'};
my $dingbatsFont   = $pdfObj->corefont('ZapfDingbats');



# Définition du sujet et des mots-clés
$pdf->setSubject($locale->t('Fiche de remise en état n° %s, client %s, machine %s n° %s',
                            $var->{'code'}, $var->{'customer.name'}, $var->{'model.label'},
                            $var->{'machine.parkNumber'}));
$pdf->setKeywords($locale->t('Fiche'), $locale->t('Remise en état'));



# Nouvelle page
my $page = $pdf->addPage();
my $gfx = $page->gfx();
my $text = $page->text();



my $x;
my $y;
my $width;
my $height;

# Numéro et date du document
my $number = $locale->t('N° %s du %s', $var->{'code'},
                                       $locale->getDateFormat($var->{'dateCreation'},
                                                              LOC::Locale::FORMAT_DATE_NUMERIC));
$text->textlabel($margin + 95/mm, $pageHeight - 35/mm - 16/pt * 1.2, $boldFont, 10/pt, $number);

# Contacts
$pdf->addContactsBlock(\$page, $var->{interlocutor});

# Références
tie(my %tabReferences, 'Tie::IxHash');
%tabReferences = (
                     $locale->t('Machine :')     => $locale->t('%s n° %s', $var->{'model.label'},
                                                                           $var->{'machine.parkNumber'}),
                     $locale->t('N° contrat :')  => $var->{'contract.code'},
                     $locale->t('Client :')      => $var->{'customer.name'},
                     $locale->t('Chantier :')    => $var->{'site.label'},
                    );

$x = $margin;
$y = $pageHeight - 70/mm;

$text->textlabel($margin, $y, $regularFont, 9/pt, uc($locale->t('Références')),
                 -color => LOC::Pdf::COLOR_EMPHASIS);
$y -= 9/pt * 1.2;

# Calcul de la largeur de la colonne des libellés
my $columnWidth = 0;
foreach my $label (keys(%tabReferences))
{
    my $currentWidth = $text->advancewidth($label, 'font' => $regularFont, 'fontsize' => 7/pt);
    
    if ($columnWidth < $currentWidth)
    {
        $columnWidth = $currentWidth;
    }
}
$columnWidth += 1.5/mm;

foreach my $label (keys(%tabReferences))
{
    $text->textlabel($margin, $y, $regularFont, 7/pt, $label);
    $text->textlabel($margin + $columnWidth, $y, $regularFont, 8/pt, , $tabReferences{$label});
    $y -= 8/pt * 1.2;
}
my $yReferences = $y;


# Contrôle / Intervention
my $tabInfos = {
                     $locale->t('Effectué par :') => $var->{'creator'},
                     $locale->t('Le :')           => $locale->getDateFormat($var->{'dateControl'},
                                                                            LOC::Locale::FORMAT_DATE_NUMERIC),
                    };

$x = $margin + $pageWidth / 2;
$y = $pageHeight - 70/mm;

$text->textlabel($margin + $pageWidth / 2, $y, $regularFont, 9/pt,
                 uc($locale->t('Contrôle / Intervention')), -color => LOC::Pdf::COLOR_EMPHASIS);
$y -= 9/pt * 1.2;

# Calcul de la largeur de la colonne des libellés
my $columnWidth = 0;
foreach my $label (keys(%$tabInfos))
{
    my $currentWidth = $text->advancewidth($label, 'font' => $regularFont, 'fontsize' => 7/pt);
    
    if ($columnWidth < $currentWidth)
    {
        $columnWidth = $currentWidth;
    }
}
$columnWidth += 1.5/mm;

foreach my $label (keys(%$tabInfos))
{
    $text->textlabel($margin + $pageWidth / 2, $y, $regularFont, 7/pt, $label);
    $text->textlabel($margin + $pageWidth / 2 + $columnWidth, $y, $regularFont, 8/pt, , $tabInfos->{$label});
    $y -= 8/pt * 1.2;
}
$y = ($y > $yReferences ? $yReferences : $y);


# Points de vérification
my @tabSheetCheckpoints = values(%{$var->{'tabSheetCheckPoints'}});

if (@tabSheetCheckpoints > 0)
{
    $x = $margin;
    $y -= 10/mm;

    # Vérification de l'espace disponible
    &checkRemainingSpace($y - $headerPageLimitCheck);
    $text->textlabel($x, $y, $regularFont, 9/pt, uc($locale->t('Points de vérification')),
                     -color => LOC::Pdf::COLOR_EMPHASIS);
    
    # Liste des colonnes
    my $tabColumns = [
                      {
                       'label'  => $locale->t('Désignation'),
                       'width'  => 80/mm,
                       'align'  => 'left',
                      },
                      {
                       'label'  => $locale->t('OK'),
                       'width'  => 10/mm,
                       'align'  => 'left',
                       'symbol' => 1,
                      },
                      {
                       'label'  => $locale->t('Chgt de pièce'),
                       'width'  => 20/mm,
                       'align'  => 'left',
                       'symbol' => 1,
                      },
                      {
                       'label'  => $locale->t('Petite réno.'),
                       'width'  => 20/mm,
                       'align'  => 'left',
                       'symbol' => 1,
                      },
                      {
                       'label'  => $locale->t('Grosse rép.'),
                       'width'  => 20/mm,
                       'align'  => 'left',
                       'symbol' => 1,
                      },
                      {
                       'label'  => $locale->t('Qté'),
                       'width'  => 15/mm,
                       'align'  => 'right',
                      },
                      {
                       'label'  => $locale->t('Montant'),
                       'width'  => 25/mm,
                       'align'  => 'right',
                      },
                     ];
    
    $x = $margin;
    $y -= 5/mm;
    
    
    # Ligne d'en-tête
    &displayTableHeader($tabColumns);
    
    $y -= 1/mm;
    $text->font($regularFont, 10/pt);

    # Lignes de données
    my $nbColumns = @$tabColumns;
    foreach my $checkpointInfos (sort { &checkpointsCmp($a, $b); } @tabSheetCheckpoints)
    {
        my $repairType = $checkpointInfos->{'repairType'};
        my $quantity   = $checkpointInfos->{'quantity'};
        my $amount     = $checkpointInfos->{'amount'};
        my $isDisplay  = $checkpointInfos->{'status'};

        #On affiche uniquement les points de vérif nécessitant une intervention
        if ($isDisplay == 0)
        {
            my $tabLabel = [
                ($checkpointInfos->{'label'} ? $checkpointInfos->{'label'} : $checkpointInfos->{'checkpoint.tabInfos'}->{'label'}),
                ($repairType eq '' ? '3' : ''),
                ($repairType eq LOC::Repair::LineType::TYPE_CHANGEMENT ? '3' : ''), # Changement de pièce
                ($repairType eq LOC::Repair::LineType::TYPE_FIX ? '3' : ''), # Petite réparation
                ($repairType eq LOC::Repair::LineType::TYPE_REPAIR ? '3' : ''), # Grosse réparation
                ($repairType ne '' ? $quantity : ''),
                ($repairType ne '' ? $locale->getCurrencyFormat($amount, undef, $tabViewData{'currencySymbol'}) : ''),
           ];

            my $nbColumns = @$tabLabel;

            # Saut de page si nécessaire
            if (&checkRemainingSpace())
            {
                $text->textlabel($x, $y, $regularFont, 9/pt,
                                 uc($locale->t('Points de vérification (suite)')),
                                 -color => LOC::Pdf::COLOR_EMPHASIS);
                $y -= 5/mm;

                &displayTableHeader($tabColumns);
                $y -= 1/mm;
            }
            
            # Affichage de la ligne
            for (my $i = 0; $i < $nbColumns; $i++)
            {

                if ($tabColumns->[$i]->{'symbol'})
                {
                    $text->font($dingbatsFont, 10/pt);
                }
                else
                {
                    $text->font($regularFont, 10/pt);
                }

                my $label = $tabLabel->[$i];
                
                # Affichage du libellé
                if ($tabColumns->[$i]->{'align'} eq 'right')
                {
                    $text->translate($x + $tabColumns->[$i]->{'width'} - $gap, $y);
                    $text->text_right($label, -maxWidth => $tabColumns->[$i]->{'width'} - $gap);
                }
                elsif ($tabColumns->[$i]->{'align'} eq 'center')
                {
                    $text->translate($x + ($tabColumns->[$i]->{'width'} - $gap) / 2, $y);
                    $text->text_center($label, -maxWidth => $tabColumns->[$i]->{'width'} - $gap);
                }
                else
                {
                    $text->translate($x, $y);
                    $text->text($label, -maxWidth => $tabColumns->[$i]->{'width'} - $gap);
                }
                $x += $tabColumns->[$i]->{'width'};
            }#fin du for
            
            $x = $margin;
            $y -= 10/pt * 1.2;
        }#fin du if isDisplay
    }#fin du foreach
}


# Prestations technique
my $nbServices = keys(%{$var->{'tabSheetServices'}});

if ($nbServices > 0)
{
    # Vérification de l'espace disponible
    &checkRemainingSpace($y - $headerPageLimitCheck);
    
    $x = $margin;
    $y -= 10/mm;
    $text->textlabel($x, $y, $regularFont, 9/pt, uc($locale->t('Prestations techniques')),
                     -color => LOC::Pdf::COLOR_EMPHASIS);
    
    # Liste des colonnes
    my $tabColumns = [
                      {
                       'label'  => $locale->t('Désignation'),
                       'width'  => 95/mm,
                       'align'  => 'left',
                      },
                      {
                       'label'  => $locale->t('Interne'),
                       'width'  => 15/mm,
                       'align'  => 'left',
                       'symbol' => 1,
                      },
                      {
                       'label'  => $locale->t('Externe'),
                       'width'  => 15/mm,
                       'align'  => 'left',
                       'symbol' => 1,
                      },
                      {
                       'label'  => $locale->t('Qté'),
                       'width'  => 15/mm,
                       'align'  => 'right',
                      },
                      {
                       'label'  => $locale->t('Montant unit.'),
                       'width'  => 25/mm,
                       'align'  => 'right',
                      },
                      {
                       'label'  => $locale->t('Montant'),
                       'width'  => 25/mm,
                       'align'  => 'right',
                      },
                     ];
    
    $x = $margin;
    $y -= 5/mm;
    $text->font($boldFont, 8/pt);
    $gfx->linewidth(1/pt);
    
    
    # Ligne d'en-tête
    &displayTableHeader($tabColumns);
    
    $x = $margin;
    $y -= 1/mm;
    $text->font($regularFont, 10/pt);
    
    # Lignes de données
    my $nbColumns = @$tabColumns;
    foreach my $serviceId (keys(%{$var->{'tabSheetServices'}}))
    {
        my $service = $var->{'tabSheetServices'}->{$serviceId};
        my @tabLabel = (
                        $var->{'tabServices'}->{$serviceId}->{'name'},
                        ($service->{'type'} =~ /IN[MT]/ ? '3' : ''), # Service interne ou interne modifiable
                        ($service->{'type'} eq LOC::Repair::LineType::TYPE_EXTERNALSERVICE ? '3' : ''),    # Service externe
                        $service->{'quantity'},
                        $locale->getCurrencyFormat($service->{'unitAmount'}, undef, $tabViewData{'currencySymbol'}),
                        $locale->getCurrencyFormat($service->{'amount'}, undef, $tabViewData{'currencySymbol'}),
                       );

        # Saut de page si nécessaire
        if (&checkRemainingSpace())
        {
            $text->textlabel($x, $y, $regularFont, 9/pt,
                             uc($locale->t('Prestations techniques (suite)')),
                             -color => LOC::Pdf::COLOR_EMPHASIS);
            $y -= 5/mm;
    
            &displayTableHeader($tabColumns);
            $y -= 1/mm;
        }
    
        # Affichage de la ligne
        for (my $i = 0; $i < $nbColumns; $i++)
        {
            if ($tabColumns->[$i]->{'symbol'})
            {
                $text->font($dingbatsFont, 10/pt);
            }
            else
            {
                $text->font($regularFont, 10/pt);
            }
    
            my $label = $tabLabel[$i];
            
            if ($tabColumns->[$i]->{'align'} eq 'right')
            {
                $text->translate($x + $tabColumns->[$i]->{'width'} - $gap, $y);
                $text->text_right($label, -maxWidth => $tabColumns->[$i]->{'width'} - $gap);
            }
            elsif ($tabColumns->[$i]->{'align'} eq 'center')
            {
                $text->translate($x + ($tabColumns->[$i]->{'width'} - $gap) / 2, $y);
                $text->text_center($label, -maxWidth => $tabColumns->[$i]->{'width'} - $gap);
            }
            else
            {
                $text->translate($x, $y);
                $text->text($label, -maxWidth => $tabColumns->[$i]->{'width'} - $gap);
            }
            $x += $tabColumns->[$i]->{'width'};
        }
        
        $x = $margin;
        $y -= 10/pt * 1.2;
    }
}


# Autres prestations
$x = $margin;
$y -= 10/mm;

# Vérification de l'espace disponible
&checkRemainingSpace($y - $headerPageLimitCheck);

# Liste des colonnes
my $tabColumns = [
                  {
                   'label'  => $locale->t('Désignation'),
                   'width'  => 125/mm,
                   'align'  => 'left',
                  },
                  {
                   'label'  => $locale->t('Qté'),
                   'width'  => 15/mm,
                   'align'  => 'right',
                  },
                  {
                   'label'  => $locale->t('Montant unit.'),
                   'width'  => 25/mm,
                   'align'  => 'right',
                  },
                  {
                   'label'  => $locale->t('Montant'),
                   'width'  => 25/mm,
                   'align'  => 'right',
                  },
                 ];


# Lignes de données
my %tabLabourTime = (
    'hours' => substr($var->{'labour.time'}, 0, 2) * 1,
    'minutes' => substr($var->{'labour.time'}, 3, 2) * 1
);
my $labourTime = ($tabLabourTime{'minutes'} > 0 ?
                      $locale->t('%d h %02d', $tabLabourTime{'hours'}, $tabLabourTime{'minutes'}) :
                      $locale->t('%d h', $tabLabourTime{'hours'}));
my %tabTravelTime = (
    'hours' => substr($var->{'travelTime'}, 0, 2) * 1,
    'minutes' => substr($var->{'travelTime'}, 3, 2) * 1
);
my $travelTime = ($tabTravelTime{'minutes'} > 0 ?
                      $locale->t('%d h %02d', $tabTravelTime{'hours'}, $tabTravelTime{'minutes'}) :
                      $locale->t('%d h', $tabTravelTime{'hours'}));
my @tabOtherServices;
if ($tabLabourTime{'hours'} > 0 || $tabLabourTime{'minutes'})
{
    push(@tabOtherServices, {
        'label'      => $locale->t('Temps de travaux'),
        'quantity'   => $labourTime,
        'unitAmount' => $var->{'labour.unitAmount'},
        'amount'     => $var->{'labour.amount'},
    });
}
if ($tabTravelTime{'hours'} > 0 || $tabTravelTime{'minutes'} > 0)
{
    push(@tabOtherServices, {
        'label'      => $locale->t('Temps de trajet'),
        'quantity'   => $travelTime,
        'unitAmount' => $var->{'travelTime.unitAmount'},
        'amount'     => $var->{'travelTime.amount'},
    });
}
if ($var->{'displacement.km'} > 0)
{
    push(@tabOtherServices, {
        'label'      => $locale->t('Déplacement'),
        'quantity'   => $locale->t('%s km', $var->{'displacement.km'}),
        'unitAmount' => $var->{'displacement.unitAmount'},
        'amount'     => $var->{'displacement.amount'},
    });
}

my $nbServices = @tabOtherServices;

if ($nbServices > 0)
{
    $text->textlabel($x, $y, $regularFont, 9/pt, uc($locale->t('Autres prestations')),
                     -color => LOC::Pdf::COLOR_EMPHASIS);
    $x = $margin;
    $y -= 5/mm;
    $text->font($boldFont, 8/pt);
    $gfx->linewidth(1/pt);
    
    
    # Ligne d'en-tête
    &displayTableHeader($tabColumns);
    
    $x = $margin;
    $y -= 1/mm;
    $text->font($regularFont, 10/pt);
    
    
my $nbColumns = @$tabColumns;
for (my $i = 0; $i < $nbServices; $i++)
{
    # Saut de page si nécessaire
    if (&checkRemainingSpace())
    {
        $text->textlabel($x, $y, $regularFont, 9/pt, uc($locale->t('Autres prestations (suite)')),
                         -color => LOC::Pdf::COLOR_EMPHASIS);
        $y -= 5/mm;

        &displayTableHeader($tabColumns);
        $y -= 1/mm;
    }

    my $service = $tabOtherServices[$i];
    my @tabLabel = (
                    $service->{'label'},
                    $service->{'quantity'},
                    $locale->getCurrencyFormat($service->{'unitAmount'}, undef, $tabViewData{'currencySymbol'}),
                    $locale->getCurrencyFormat($service->{'amount'}, undef, $tabViewData{'currencySymbol'}),
                   );

    # Affichage de la ligne
    for (my $j = 0; $j < $nbColumns; $j++)
    {
        if ($tabColumns->[$j]->{'symbol'})
        {
            $text->font($dingbatsFont, 10/pt);
        }
        else
        {
            $text->font($regularFont, 10/pt);
        }

        my $label = $tabLabel[$j];
        
        if ($tabColumns->[$j]->{'align'} eq 'right')
        {
            $text->translate($x + $tabColumns->[$j]->{'width'} - $gap, $y);
            $text->text_right($label);
        }
        elsif ($tabColumns->[$j]->{'align'} eq 'center')
        {
            $text->translate($x + ($tabColumns->[$j]->{'width'} - $gap) / 2, $y);
            $text->text_center($label);
        }
        else
        {
            $text->translate($x, $y);
            $text->text($label);
        }
        $x += $tabColumns->[$j]->{'width'};
    }
    
    $x = $margin;
    $y -= 10/pt * 1.2;
}
}


# Vérification de l'espace disponible
&checkRemainingSpace($y - $headerPageLimitCheck);

# Montant total
$x = $margin + 165/mm;
$y -= 10/mm;

$gfx->linewidth(1/pt);
$gfx->move($x, $y);
$gfx->line($pageWidth - $margin, $y);
$gfx->stroke();

$x = $margin + 135/mm;
$y -= 12/pt * 1.2;
$text->font($boldFont, 12/pt);
$text->translate($x, $y);
$text->text($locale->t('Montant total'));

$x = $margin + 190/mm;
$text->translate($x, $y);
$text->text_right($locale->getCurrencyFormat($var->{'amount'}, undef, $tabViewData{'currencySymbol'}));


# Commentaires
if (length($var->{'comment'}) > 0)
{
    # Vérification de l'espace disponible
    &checkRemainingSpace($y - $headerPageLimitCheck);
    
    $x = $margin;
    $y -= 10/mm;
    
    $text->textlabel($x, $y, $regularFont, 9/pt, uc($locale->t('Commentaires')),
                     -color => LOC::Pdf::COLOR_EMPHASIS);
    
    $y -= 5/mm;
    $text->font($regularFont, 10/pt);
    $text->translate($x, $y);
    $text->lead(10/pt);
    
    my $overflowText = $var->{'comment'};
    while ($overflowText = $text->section($overflowText, $pageWidth - 2 * $margin, $y - 1.5 * $margin,
                                            -align => 'justified'))
    {
        &checkRemainingSpace(1.5 * $margin);
    
        $text->textlabel($x, $y, $regularFont, 9/pt, uc($locale->t('Commentaires (suite)')),
                         -color => LOC::Pdf::COLOR_EMPHASIS);
        $y -= 5/mm;
    
        $text->font($regularFont, 10/pt);
        $text->translate($x, $y);
        $text->lead(10/pt);
    }
}



# Numéros de page
&displayPageNumbers();



# Sortie
my $pdfString = $pdf->getString();
$pdf->end();

&LOC::Browser::sendHeaders(
        "Content-type: application/pdf;\n"
        . "Content-Disposition: inline; filename=\"ficheRE.pdf\";\n\n"
    );

print $pdfString;




# Function: displayPageNumbers
# Affichage du numéro de page
sub displayPageNumbers
{
    my $nbPages = $pdfObj->pages();
    
    for (my $i = 0; $i < $nbPages; $i++)
    {
        my $page = $pdfObj->openpage($i + 1);
        my $text = $page->text();
        
        $text->font($italicFont, 8/pt);
        $text->translate($pageWidth - $margin, 1.5 * $margin);
        $text->text_right(sprintf('%d/%d', $i + 1, $nbPages));
    }
}


# Function: displayTableHeader
# Affichage de l'en-tête d'une table
#
# Parameters:
# string $tabColumns - Liste des colonnes du tableau
sub displayTableHeader
{
    my ($tabColumns) = @_;
    
    # Police et épaisseur du trait
    $text->font($boldFont, 8/pt);
    $gfx->linewidth(1/pt);

    # Sauvegarde de la position courante
    my ($x0, $y0) = $text->textpos();
    
    my $nbColumns = @$tabColumns;
    
    for (my $i = 0; $i < $nbColumns; $i++)
    {
        my $label = $tabColumns->[$i]->{'label'};
        
        if ($tabColumns->[$i]->{'align'} eq 'right')
        {
            $text->translate($x + $tabColumns->[$i]->{'width'} - $gap, $y);
            $text->text_right($label, -maxWidth => $tabColumns->[$i]->{'width'} - $gap);
        }
        elsif ($tabColumns->[$i]->{'align'} eq 'center')
        {
            $text->translate($x + ($tabColumns->[$i]->{'width'} - $gap) / 2, $y);
            $text->text_center($label, -maxWidth => $tabColumns->[$i]->{'width'} - $gap);
        }
        else
        {
            $text->translate($x, $y);
            $text->text($label, -maxWidth => $tabColumns->[$i]->{'width'} - $gap);
        }
        $gfx->move($x, $y - 1/mm);
        $x += $tabColumns->[$i]->{'width'};
        $gfx->line($x - $gap, $y - 1/mm);
    }
    
    $gfx->stroke();
    
    # Retour à la ligne
    $x = $x0 + $margin;
    $y -= 8/pt * 1.5;
    $text->translate($x, $y);
}


# Function: checkRemainingSpace
# Vérification de l'espace restant dans la page
#
# Parameters:
# string $value - Ordonnée ($y par défaut)
#
# Returns:
# bool - 1 si l'espace restant est suffisant, 0 sinon
sub checkRemainingSpace
{
    my ($value) = @_;
    
    unless (defined $value)
    {
        $value = $y;
    }
    
    if ($value <= $pageLimit)
    {
        $page = $pdf->addPage(LOC::Pdf::DISPLAY_NONE);
        $gfx = $page->gfx();
        $text = $page->text();
        
        $y = $pageHeight - $margin;
        
        return 1;
    }
    return 0;
}


# Function: checkpointsCmp
# Fonction de tri des points de vérification (suivant l'ordre pré-établi en paramétrage)
#
# Parameters:
# string $a - Premier point de vérification
# string $b - Second point de vérification
#
# Returns:
# int
sub checkpointsCmp
{
    my ($a, $b) = @_;

    return $b->{'checkpoint.tabInfos'}->{'family.order'} <=> $a->{'checkpoint.tabInfos'}->{'family.order'};
}