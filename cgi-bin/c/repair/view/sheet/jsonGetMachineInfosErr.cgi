use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Json;
use LOC::Locale;

our %tabViewData;

my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});


# Libellé de l'erreur ou avertissement
if ($tabViewData{'errorCode'} eq 'nocontracts')
{
    $tabViewData{'values'}->{'errorMsg'} = $locale->t('Pas de contrat pour cette machine. Sélection de la machine précédemment sélectionnée.');
}
elsif ($tabViewData{'errorCode'} eq 'nomachine')
{
    $tabViewData{'values'}->{'errorMsg'} = $locale->t('Pas d\'information sur la machine.');
}

my $view = LOC::Json->new();
print $view->displayHeader();
print $view->encode($tabViewData{'values'});