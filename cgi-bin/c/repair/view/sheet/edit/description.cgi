use utf8;
use open (':encoding(UTF-8)');

#Bloc autres prestation
#our $view;
#our %tabViewData;
my $searchMachineInfosJSONUrl   = $view->createURL('repair:sheet:jsonGetMachineInfos');

my %inputOptions;
# Zone de recherche machine
my $repairSheetMachineId = $view->displayFormSelect('searchMachineId',
                                                    $tabViewData{'tabMachinesList'},
                                                    $tabViewData{'machineId'}, 0);

my $isMachineModifiable = ($tabViewData{'isMachineModifiable'} && $tabViewData{'tabRights'}->{'equipment'} =~ /^(|.*\|)machine(|\|.*)$/);
$repairSheetMachineId .= $view->displayJSBlock('
var searchBoxObj = Location.searchBoxesManager.createSearchBox("searchMachineId");
searchBoxObj.setDisabled(' . (!$isMachineModifiable ? 'true' : 'false') . ');
searchBoxObj.displayNullItem(' . ($tabViewData{'machineId'} == 0 ? 'true' : 'false') . ', 0, "---");
searchBoxObj.setValue(' . $tabViewData{'machineId'} . ');
');

# Select du contrat de loc
%inputOptions = ('onchange' => 'displayContractInfo(this.selectedIndex);');
if ($tabViewData{'tabRights'}->{'equipment'} !~ /^(|.*\|)contract(|\|.*)$/)
{
    $inputOptions{'disabled'} = 'disabled';
}
my $sheetContractId = $view->displayFormSelect('contractId',
                                              [],
                                              $tabViewData{'contractId'},
                                              0,
                                              \%inputOptions);
my $contractIdInput = '';
if ($inputOptions{'disabled'})
{
    $contractIdInput = $view->displayFormInputHidden('contractId', $tabViewData{'contractId'});
}

# Aide pour la recherche de machine
my $machineHelp = $view->displayHelp($locale->t('La recherche de machine s\'effectue parmi le numéro de parc et le modèle de machines'));

# Aide pour le choix du contrat
my $contractHelp = $view->displayHelp($locale->t('%d derniers contrats portant sur la machine sélectionnée', DISPLAY_NBCONTRACTS));

# Si une proforma est genere sur le contrat
my $proformaGenInContractImg = $view->displayImage('repair/sheet/generate(|Over).png', {'title' => $locale->t('Une pro forma est générée sur le contrat.')});

print '
<fieldset>
    <legend>' .
        $view->displayImage('repair/sheet/description.png') . ' ' .
        htmllocale('Description du matériel') .
    '</legend>
    <table class="formTable" height = "135px">
        <tr>
            <td class="label">' . htmllocale('Machine') . $requiredIcon . $machineHelp . '</td>
            <td><span>' . $repairSheetMachineId . '</span> </td>
            <td><div id="alertsLoading" style="text-align: center; visibility:hidden;">' . $view->displayImage('loading2.gif') .'</div></td>
        </tr>
        <tr>
            <td class="label">' . htmllocale('Famille') . '</td>
            <td><span>' .  $tabViewData{'familyLabel'} . '</span></td>
        </tr>
        <tr>
            <td class="label">' . htmllocale('Contrat') . $requiredIcon . $contractHelp . '</td>
            <td>' . $sheetContractId . ' ' .
                    $view->displayTextButton('',
                                            '',
                                            'return false;',
                                             $locale->t('Voir le contrat'),
                                            'left',
                                            {'target' => '_blank', 'id' => 'contractLink',
                                             'class' => 'locCtrlButton action-btn openRentContract'
                                            }) .
                    $contractIdInput .
            '</td>
        </tr>
        <tr id="proformaGenerated" style="visibility:hidden;">
            <td class="label"></td>
            <td>
                <span>' . htmllocale('Ce contrat fait l\'objet d\'une pro forma') . '</span>
            </td>
        </tr>
        <tr>
            <td class="label">' . htmllocale('Client') . '</td>
            <td>' .
            $view->displayFormInputText('customerName',
                                        '',
                                        0,
                                        {'class' => 'displayText', 'size' => 55, 'readonly' => 'readonly'}) .
            '</td>
        </tr>
        <tr>
            <td class="label">' . htmllocale('Chantier') . '</td>
            <td>' .
            $view->displayFormInputText('siteLabel',
                                        '',
                                        0,
                                        {'class' => 'displayText', 'size' => 50, 'readonly' => 'readonly'}) .
            '</td>
        </tr>
        <tr>
            <td class="label"></td>
            <td>' .
            $view->displayFormInputText('siteAddress',
                                        '',
                                        0,
                                        {'class' => 'displayText', 'size' => 50, 'readonly' => 'readonly'}) .
            '</td>
        </tr>
        <tr>
            <td class="label"></td>
            <td>' .
            $view->displayFormInputText('sitePostalLabel',
                                        '',
                                        0,
                                        {'class' => 'displayText', 'size' => 50, 'readonly' => 'readonly'}) .
            '</td>
        </tr>
    </table>
</fieldset>';
