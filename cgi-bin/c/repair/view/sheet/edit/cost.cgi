use utf8;
use open (':encoding(UTF-8)');

#Bloc Cout total
#our $view;
#our %tabViewData;

#Affichage du cout et du montant
print '
<fieldset style="padding: 10px; margin-top: 20px; text-align: center;">
    <span style="color: #007AC2; font-weight: bold;">' . htmllocale('Montant total de la remise en état') . ' : </span>
    <span style="color: black; font-weight:bold;">' .
        $view->displayFormInputCurrency('sheetAmt', 
                                        0,
                                        LOC::Html::CURRENCYMODE_TEXT, 
                                        $tabViewData{'currencySymbol'}, 
                                        undef, 
                                        {'disabled' => '', 'size' => 5}) . '</span>' .
        $view->displayFormInputHidden('sheetCst', 0) . '
</fieldset>';
