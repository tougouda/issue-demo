use utf8;
use open (':encoding(UTF-8)');

#Bloc photo
#our $view;
#our %tabViewData;

print '
<fieldset>
    <legend>' . $view->displayImage('repair/sheet/photo.png') . ' ' . 
                htmllocale('Photos') . '</legend>

    <div style="position: relative;">
        <div id="photosDisabled" class="disabledDiv" style="visibility: hidden;"></div>
        <div id="error" style="display: none;"></div>

        <iframe id="photosIframe" src="" frameborder="0" height="45px" width="500px"></iframe>

        <div id="imagesContainer"></div>

        <div class="buttonZip">' .
            $view->displayTextButton($locale->t('Zipper'), 
                                     'zip(|Over).gif',
                                     $view->createURL('repair:sheet:zipPhotos', {'sheetId' => $tabViewData{'sheetId'}}) . '&photos=',
                                     $locale->t('Zipper'),
                                     'left',
                                     {
                                         'id'    => 'zipBtn',
                                         'style' => 'visibility:hidden;'
                                     }) . '
        </div>
    </div>
</fieldset>';
