use utf8;
use open (':encoding(UTF-8)');

#Bloc hidden contenant les elements du formulaire
#our $view;
#our %tabViewData;

# Infobulle
my $srvTooltip = $view->displayHelp('', {'id' => 'srvTooltip'});

print '
<fieldset>
    <legend>' . $view->displayImage('repair/sheet/intervention.png') . ' ' . 
                htmllocale('Prestations techniques') . '</legend>

    <div style="display: none;">' .
        $view->displayFormInputCurrency('serviceCst_blank', 
                                        0, 
                                        LOC::Html::CURRENCYMODE_POSITIVEINPUT, 
                                        $tabViewData{'currencySymbol'}, 
                                        2, 
                                        {'disabled'     => ''}) . 
        $srvTooltip . '
    </div>';

#Affichage du tableau des services
print '
    <div style="position: relative;">
        <div id="servicesDisabled" class="disabledDiv" style="visibility: hidden;";></div>

        <table id="tableService" class="standard ex">
        <thead>
            <tr>
                <th>' . htmllocale('Désignation') . '</th>
                <th style="text-align: center;">' . htmllocale('Int.') . '</th>
                <th style="text-align: center;">' . htmllocale('Ext.') . '</th>
                <th></th>
                <th style="text-align: right;">' . htmllocale('Montant') . '</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        </tbody>';

if ($tabViewData{'tabRights'}->{'services'} =~ /^(|.*\|)add(|\|.*)$/)
{
    print '
        <tfoot>
            <tr>
                <td colspan="5"></td>
                <td style="text-align: right;">' .

                    $view->displayImage('add(|Over).png', 
                                        {
                                            'id'    => 'addServiceBtn',
                                            'style' => 'cursor: pointer;'
                                        }) . '
                </td>
            </tr>
        </tfoot>';
}

print '
        </table>
    </div>

</fieldset>';