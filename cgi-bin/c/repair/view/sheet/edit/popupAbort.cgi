use utf8;
use open (':encoding(UTF-8)');

# Commentaire d'abandon
my $abortCommentInput = $view->displayFormTextArea('abortComment', '');
my $btnAbortValid = $view->displayTextButton($locale->t('Valider'),
                                             'valid(|Over).png',
                                             'return false;',
                                             $locale->t('Valider l\'abandon de la fiche de remise en état'),
                                             '',
                                             {'id' => 'validAbortBtn'});

print '
<div style="display: none;" id="abortSheet" title="' . htmllocale('Abandon de la fiche de remise en état') . '">
  <center>
    <table>
    <tbody>
        <tr>
            <td>
                <fieldset class="sub">
                    <legend>' . htmllocale('Abandon de la fiche de remise en état') . '</legend>
                        <table class="formTable">
                            <tr>
                                <td class="label">' . htmllocale('Commentaires d\'abandon') . $requiredIcon . '</label>
                                <td class="cstCol">' . $abortCommentInput . '</label>
                            </tr>
                        </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset class="sub">
                    <table>
                        <tr>
                            <td>' . $btnAbortValid . '</td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </tbody>
    </table>
  </center>
</div>';