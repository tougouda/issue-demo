use utf8;
use open (':encoding(UTF-8)');


# Tableau des références
print '
<fieldset style="margin-bottom: 10px;">
    <legend>' . $view->displayImage('repair/sheet/freeControl.png') . ' ' . $locale->t('Références') . '</legend>
';

print '
    <div class="clonedCurrencies">
        ' . $view->displayFormInputCurrency('amount_clone', 0,
                                               LOC::Html::CURRENCYMODE_TEXT,
                                               $tabViewData{'currencySymbol'}) .'
        ' . $view->displayFormInputText('quantity_clone', 1) . '
        ' . $view->displayJSBlock('Location.spinControlsManager.createSpinControl("quantity_clone", 1, null, 1, 10, 0);') . '
        <div id="searchResult_clone" class="dynamicSearch searchResult">
            <div class="container">
                <div class="loading">' . $locale->t('Recherche en cours') . '</div>
                <div class="error">' . $locale->t('Une erreur est survenue pendant la recherche') . '</div>
                <ul>
                </ul>
            </div>
        </div>
    </div>';

# - Anciens points de vérification
print '
    <table class="tableCheckPoints standard" id="oldCheckpoints">
        <thead>
            <tr>
                <th class="type"></th>
                <th class="label">' . $locale->t('Désignation') . '</th>
                <th class="amount">' . $locale->t('Montant') . '</th>
                <th class="ctrls"></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
';


# - Références
print '
    <table id="tableReferences" class="tableCheckPoints standard">
    <thead>
        <tr>
            <th class="chg">' . htmllocale('Changement de pièce') . '</th>
            <th class="label">' . htmllocale('Désignation') . '</th>
            <th class="amount">' . htmllocale('Montant') . '</th>
            <th class="ctrls"></th>
        </tr>
    </thead>
    <tbody>
    </tbody>';

if ($tabViewData{'tabRights'}->{'references'} =~ /^(|.*\|)add(|\|.*)$/)
{
    print '
    <tfoot>
        <tr>
            <td colspan="3"></td>
            <td class="ctrls">
                <a href="#" class="add" id="refAddBtn" title="' . $locale->t('Ajouter une référence') . '"></a>
            </td>
        </tr>
    </tfoot>';
}

print '
    </table>
</fieldset>';

