use utf8;
use open (':encoding(UTF-8)');

#Bloc Controle/Intervention
#our $view;
#our %tabViewData;

my %inputOptions;

#Tableau des flags
my $tabFlags = {
    LOC::Repair::Sheet::FLAG_TURNEDINESTIMATE() => $locale->t('Fiche basculée en devis'),
    LOC::Repair::Sheet::FLAG_TURNEDINSERVICES() => $locale->t('Fiche basculée en services'),
    LOC::Repair::Sheet::FLAG_TURNEDINDAYS()     => $locale->t('Fiche basculée en jours')
};


# Liste des utilisateurs
my @tabCreatorsList = ();
while (my($key, $tab) = each(%{$tabViewData{'creatorsList'}}))
{
    my $attributes = {
                      'value'     => $key,
                      'innerHTML' => $tab,
                      'style'     => (($tabViewData{'stateCreator'}->{$key} eq 'GEN03') ? 'color:#cccccc;text-decoration:line-through;font-style:italic;' : ''),
                      'disabled'  => (($tabViewData{'stateCreator'}->{$key} eq 'GEN03') ? 'disabled' : '')
                     };
    push(@tabCreatorsList, $attributes);
}

# Zone de recherche utilisateur
my $sheetUserId = $view->displayFormSelect('sheetUserId',  \@tabCreatorsList, $tabViewData{'sheetUsrId'}, 0,
                                           {'onchange' => '$ge("sheetUsrId").value = this.value;'
                                           });
my $userListUrl = $view->createURL('default:user:jsonList', {'format' => 1});

$sheetUserId .= $view->displayJSBlock('
var searchUserSearchBox = Location.searchBoxesManager.createSearchBox(
                                                                      "sheetUserId",
                                                                      {
                                                                       isRequired:false,
                                                                       url:"' . $userListUrl . '"
                                                                      }
                                                                     );');

if ($tabViewData{'tabRights'}->{'infos'} !~ /^(|.*\|)user(|\|.*)$/)
{
    $sheetUserId .= $view->displayJSBlock('Location.searchBoxesManager.createSearchBox("sheetUserId").disable();');
}


# Aide pour le choix du contrôleur
my $userHelp = $view->displayHelp($locale->t('Nom de la personne ayant effectué le contrôle ou l\'intervention'));

print '
<fieldset>
    <legend>' . $view->displayImage('repair/sheet/infos.png') . ' ' .  
                htmllocale('Contrôle / Intervention') . '</legend>

    <table class="formTable" height = "135px">
    <tbody>
        <tr>
            <td class="label">' . htmllocale('Numéro') . '</td>
            <td>' . 
            ($tabViewData{'sheetId'} == 0 ? 
                                htmllocale('Fiche non enregistrée') : 
                                $tabViewData{'sheetId'} . ' ' .
                                '<span onmouseover="Location.toolTipsObj.show(\'' . $view->toJSEntities($tabViewData{'abortComment'}) . '\', {corner:\'tl\'});"
                                       onmouseout="Location.toolTipsObj.hide();">' . $view->displayState($tabViewData{'stateId'}, 
                                                     $tabViewData{'tabStates'}->{$tabViewData{'stateId'}}) . '</span>') . 
                                 (defined ($tabViewData{'flag'}) ?  ' (' . $tabFlags->{$tabViewData{'flag'}} . ')' : "") .
                                 ((defined ($tabViewData{'flag'}) && ($tabViewData{'flag'} == 0)) ? '&nbsp;&gt;&nbsp;<a target="_blank" href="' . $view->createURL('repair:estimate:edit', {'id' => $tabViewData{'repairEstimateId'}}) . '">' . $tabViewData{'codeRepairEstimate'} . '</a>' : '') . '
                                 </td>
        </tr>
        <tr>
            <td class="label">' . htmllocale('Effectué par') . $requiredIcon . $userHelp . '</td>
            <td><span>' . $sheetUserId . '</span></td>
        </tr>';

# Date de la fiche
my $sheetDate;
my $tabDateAttributes = {};
if ($tabViewData{'tabRights'}->{'infos'} !~ /^(|.*\|)date(|\|.*)$/)
{
    $tabDateAttributes = {'disabled' => 'disabled'};
}
$sheetDate = $view->displayFormInputDate('sheetDate',  $tabViewData{'sheetDate'}, undef, undef, $tabDateAttributes);

#Interlocuteur
# Zone de recherche utilisateur
my $sheetInterlocutorId = $view->displayFormSelect('sheetInterlocutorId',  $tabViewData{'interlocutorsList'}, $tabViewData{'sheetInterlocutorId'});
my $interlocutorListUrl = $view->createURL('default:user:jsonList', {'format' => 1});

$sheetInterlocutorId .= $view->displayJSBlock('
var searchInterlocutorSearchBox = Location.searchBoxesManager.createSearchBox(
                                                                      "sheetInterlocutorId",
                                                                      {
                                                                       isRequired:false,
                                                                       url:"' . $interlocutorListUrl . '"
                                                                      }
                                                                     );');

if ($tabViewData{'tabRights'}->{'infos'} !~ /^(|.*\|)interlocutor(|\|.*)$/)
{
    $sheetInterlocutorId .= $view->displayJSBlock('Location.searchBoxesManager.createSearchBox("sheetInterlocutorId").disable();');
}
# Vider le champ interlocuteur
my $clearInterlocutor = '';
if ($tabViewData{'tabRights'}->{'infos'} =~ /^(|.*\|)interlocutor(|\|.*)$/)
{
    $clearInterlocutor = $view->displayTextButton('', 'clear.gif', 'searchInterlocutorSearchBox.clear();', $locale->t('Effacer la recherche'));
}

# Commentaire
%inputOptions = ('rows' => 4, 'cols' => 35);
if ($tabViewData{'tabRights'}->{'infos'} !~ /^(|.*\|)comments(|\|.*)$/)
{
    $inputOptions{'readonly'} = 'readonly';
    $inputOptions{'class'}    = 'readonlyComments';
}
my $sheetComment = $view->displayFormTextArea('sheetComment', $tabViewData{'comment'}, \%inputOptions);

# Aide pour la date de contrôle
my $dateHelp = $view->displayHelp($locale->t('Date du contrôle ou de l\'intervention'));

print '
        <tr>
            <td class="label">' . htmllocale('Le') . $requiredIcon . $dateHelp . '</td>
            <td>' . $sheetDate . '</td>
        </tr>
        <tr>
            <td class="label">' . htmllocale('Interlocuteur') . '</td>
            <td><span>' . $sheetInterlocutorId . ' ' . $clearInterlocutor . '</span></td>
        </tr>
        <tr>
            <td class="label">' . htmllocale('Commentaires') . '</td>
            <td>' . $sheetComment . '</td>
        </tr>
    </tbody>
    </table>
</fieldset>';
