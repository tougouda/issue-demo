use utf8;
use open (':encoding(UTF-8)');

#Bloc autres prestation
#our $view;
#our %tabViewData;

my $isLabourModifiable = ($tabViewData{'tabRights'}->{'otherServices'} =~ /^(|.*\|)labour(|\|.*)$/);
my $isTravelTimeModifiable = ($tabViewData{'tabRights'}->{'otherServices'} =~ /^(|.*\|)travelTime(|\|.*)$/);
my $isDisplModifiable  = ($tabViewData{'tabRights'}->{'otherServices'} =~ /^(|.*\|)displacement(|\|.*)$/);

#Bloc autres services, affichage du déplacement et de lamain d'oeuvre
print '
<fieldset>
    <legend>' . $view->displayImage('repair/sheet/prestation.png') . ' ' . 
                htmllocale('Autres prestations') . '</legend>

    <div style="position: relative;">
    <div id="otherServicesDisabled" class="disabledDiv" style="visibility: hidden;";></div>
    <table class="formTable">
    <tbody>
        <tr>
            <td colspan="3"></td>
            <td style="font-style: italic; padding-left: 20px;">' . htmllocale('Montant') . '</td>
        </tr>
        <tr>
            <td class="label">' . htmllocale('Temps de travaux') . '</td>
            <td>'. 
                $view->displayFormInputText('labHour', 
                                            $tabViewData{'labHour'}, 
                                            LOC::Html::TEXTMODE_DIGIT, 
                                            {
                                             'size'     => 2,
                                             'disabled' => ($isLabourModifiable ? '' : 'disabled')
                                            }) . ' ' . 
                htmllocale('h') . ' ' . 
                $view->displayFormInputText('labMinutes', 
                                            $tabViewData{'labMinutes'}, 
                                            LOC::Html::TEXTMODE_DIGIT, 
                                            {
                                             'size'     => 2,
                                             'disabled' => ($isLabourModifiable ? '' : 'disabled')
                                            }) . ' ' . 
                htmllocale('min') . 
                $view->displayFormInputHidden('labTimeCst', $tabViewData{'labHourCst'}) . 
                $view->displayFormInputHidden('labTimeAmount', $tabViewData{'labHourAmount'}) .
            '</td>
            <td>
                x
                <span>' . 
                    $locale->getCurrencyFormat($tabViewData{'labHourAmount'}, undef, $tabViewData{'currencySymbol'}) . 
                '</span>
            </td>
            <td align="right">
                <span style="font-weight: bold;">' .
                    $view->displayFormInputHidden('labCst', $tabViewData{'labCst'}) . 
                    $view->displayFormInputCurrency('labAmount', 
                                                    $tabViewData{'labAmount'}, 
                                                    LOC::Html::CURRENCYMODE_TEXT, 
                                                    $tabViewData{'currencySymbol'}, 
                                                    undef, 
                                                    {
                                                     'disabled' => ($isLabourModifiable ? '' : 'disabled'),
                                                     'size'     => 5
                                                    }) . '
                </span>
            </td>
        </tr>
        <tr>
            <td class="label">' . htmllocale('Temps de trajet') . '</td>
            <td>'. 
                $view->displayFormInputText('tvtHour', 
                                            $tabViewData{'tvtHour'}, 
                                            LOC::Html::TEXTMODE_DIGIT, 
                                            {
                                             'size'     => 2,
                                             'disabled' => ($isTravelTimeModifiable ? '' : 'disabled')
                                            }) . ' ' . 
                htmllocale('h') . ' ' . 
                $view->displayFormInputText('tvtMinutes', 
                                            $tabViewData{'tvtMinutes'}, 
                                            LOC::Html::TEXTMODE_DIGIT, 
                                            {
                                             'size'     => 2,
                                             'disabled' => ($isTravelTimeModifiable ? '' : 'disabled')
                                            }) . ' ' . 
                htmllocale('min') . 
                $view->displayFormInputHidden('tvtHourCst', $tabViewData{'tvtHourCst'}) . 
                $view->displayFormInputHidden('tvtHourAmount', $tabViewData{'tvtHourAmount'}) .
            '</td>
            <td>
                x
                <span>' . 
                    $locale->getCurrencyFormat($tabViewData{'tvtHourAmount'}, undef, $tabViewData{'currencySymbol'}) . 
                '</span>
            </td>
            <td align="right">
                <span style="font-weight: bold;">' .
                    $view->displayFormInputHidden('tvtCst', $tabViewData{'tvtCst'}) . 
                    $view->displayFormInputCurrency('tvtAmount', 
                                                    $tabViewData{'tvtAmount'}, 
                                                    LOC::Html::CURRENCYMODE_TEXT, 
                                                    $tabViewData{'currencySymbol'}, 
                                                    undef, 
                                                    {
                                                     'disabled' => ($isTravelTimeModifiable ? '' : 'disabled'),
                                                     'size'     => 5
                                                    }) . '
                </span>
            </td>
        </tr>
        <tr>
            <td class="label">' . htmllocale('Déplacement') . '</td>
            <td>' . 
                $view->displayFormInputText('dspKm',
                                            $tabViewData{'dspKm'},
                                            LOC::Html::TEXTMODE_DIGIT,
                                            {
                                             'size'         => 3,
                                             'disabled'     => ($isDisplModifiable ? '' : 'disabled')
                                            }) . ' ' .
                htmllocale('km') . 
                $view->displayFormInputHidden('dspKmCst', $tabViewData{'dspUntCst'}) . 
                $view->displayFormInputHidden('dspKmAmount', $tabViewData{'dspUntAmount'}) . '
            </td>
            <td>
                x
                <span>' . 
                    $locale->getCurrencyFormat($tabViewData{'dspUntAmount'}, undef, $tabViewData{'currencySymbol'}) . '
                </span>
            </td>
            <td align="right">
                <span style="font-weight: bold;">' . 
                $view->displayFormInputHidden('dspCst', $tabViewData{'dspCst'}) . 
                    $view->displayFormInputCurrency('dspAmount', 
                                                    $tabViewData{'dspAmount'}, 
                                                    LOC::Html::CURRENCYMODE_TEXT, 
                                                    $tabViewData{'currencySymbol'}, 
                                                    undef, 
                                                    {
                                                     'disabled' => ($isDisplModifiable ? '' : 'disabled'),
                                                     'size'     => 5
                                                    }) . 
                '</span>
            </td>
        </tr>
    </tbody>
    </table>
    </div>
</fieldset>';
