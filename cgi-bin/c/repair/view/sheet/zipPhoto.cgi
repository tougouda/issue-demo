use utf8;
use open (':encoding(UTF-8)');

use strict;

use Archive::Zip qw(:ERROR_CODES :CONSTANTS);
use IO::File;
use LOC::Browser;

our %tabViewData;

my $dirPhotos = LOC::Repair::Sheet::PHOTOS_PATH . $tabViewData{'countryId'} . '/' . $tabViewData{'sheetId'} . '/' ;

my $zipName = 'photos.zip';
my $zip = Archive::Zip->new();

foreach my $photo(@{$tabViewData{'tabPhotos'}})
{
    $photo = &LOC::Util::encodeUtf8($photo);
    $zip->addFile($dirPhotos.$photo, $photo);
}

&LOC::Browser::sendHeaders(
        "Content-Type: application/x-zip\n"
        . "Content-Type: application/force-download\n"
        . "Content-Type: application/octet-stream\n"
        . "Content-Type: application/download\n"
        . "Content-Disposition: attachment; filename=FRE_" . $tabViewData{'sheetId'} . '/' . $zipName . "\n"
        . "Content-Transfer-Encoding: binary\n"
        . "Pragma: no-cache\n"
        . "Cache-Control: must-revalidate, post-check=0, pre-check=0, public,max-age=0\n"
        . "Expires: 0\n"
        . "\n"
    );
open(my $fh, ">&STDOUT");
binmode $fh, ":raw";
my $zipstatus = $zip->writeToFileHandle($fh);
if ($zipstatus != AZ_OK)
{
    print STDERR 'Erreur lors de la création du fichier ' . $zipName . ".\n";
}

1;
