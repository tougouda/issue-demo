use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
my $dir = 'repair/sheet/';


our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
our $view = LOC::Html->new($tabViewData{'localeId'});

#Fonction de traduction
sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


$view->addCSSSrc('location.css');
$view->addCSSSrc('
body
{
    background-color: #F5FBFF;
    
}
');
#Java scipt appelé après l'ajout d'une photo
$view->addJSSrc($tabViewData{'jsSrc'});

print $view->displayHeader();

#Bouton d'upload 
my $addBtn = $view->displayFormInputButton('uploadPhoto',
                                           $locale->t('Ajouter'),
                                           {
                                            'onclick' => 'window.parent.upload(this.form);',
                                            'id' => 'addBtn',
                                            'style' => 'visibility:hidden;',
                                            'disabled' => ($tabViewData{'isModifiable'} ? '' : 'disabled'),
                                           });

my %tabOptions = ();
if ($tabViewData{'isModifiable'})
{
    $tabOptions{'onchange'} = 'parent.manageBtn(this, $ge("addBtn"));';
}
else
{
    $tabOptions{'disabled'} = 'disabled';
}

#Affichage du formulaire
print '<form id="photoForm" action="" method="post" enctype="multipart/form-data">
        <table class="formTable">
            <tr>
                <td class="label">' . htmllocale('Fichier') . '</td>
                <td>' . $view->displayFormInputFile('photo', 0, \%tabOptions) . '</td>
                <td>' . $addBtn . '</td>
            </tr>
        </table>
    </form>';

print $view->displayFooter();
