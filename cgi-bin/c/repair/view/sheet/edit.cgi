use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
my $dir = 'repair/';


our $inputsTabIndex = 1;
our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
our $view   = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}



our $isNew         = $tabViewData{'sheetId'} == 0 ? 1 : 0; # Ajout ou modification
my $isModifiable = ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)valid(|\|.*)$/);

# Symbole champ requis
our $requiredIcon = ($isModifiable ? $view->displayRequiredSymbol() : '');

# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'sheet.css');

$view->addJSSrc($dir . 'sheet.js');
$view->addJSSrc($dir . 'sheet/uploadPhoto.js');
$view->addJSSrc($dir . 'sheet/oldCheckpoints.js');
$view->addJSSrc($dir . 'sheet/references.js');
$view->addJSSrc($dir . 'sheet/services.js');


$view->setPageTitle($locale->t('Fiche de remise en état'))
     ->addPackage('searchboxes')
     ->addPackage('spincontrols')
     ->addPackage('popups')
     ->addPackage('modalwindow');
     
# Traductions qui seront utilisées à partir de l'"Id" dans le JAVASCRIPT
$view->addTranslations({
    'noneSelected'           => $locale->t('Aucune'),
    'confirmRemoveService'   => $locale->t('Souhaitez-vous supprimer cette prestation ?'),
    'errorRemoveService'     => $locale->t('Erreur lors de la suppression du service'),
    'errorQuery'             => $locale->t('Erreur requête'),
    'confirmRemovePhoto'     => $locale->t('Souhaitez-vous supprimer cette photo ?'),
    'errorAddPhoto0'         => $locale->t('Le poids de la photo dépasse la taille limite.'),
    'errorAddPhoto1'         => $locale->t('Erreur lors de l\'ajout de la photo'),
    'errorAddPhoto3'         => $locale->t('Pas de photo à ajouter'),
    'errorAddPhoto2'         => $locale->t('Le type de fichier n\'est pas pris en compte.'),
    'errorKimoce'            => $locale->t('Veuillez saisir tous les codes KIMOCE'),
    'errorLabel'             => $locale->t('Veuillez saisir toutes les désignations pour les références'),
    'errorUser'              => $locale->t('Veuillez sélectionner un utilisateur'),
    'errorDate'              => $locale->t('Veuillez sélectionner une date valide'),
    'errorDateSup'           => $locale->t('La date de création doit être inférieure ou égale à la date du jour'),
    'errorAbortSheet'        => $locale->t('Veuillez saisir un commentaire d\'abandon'),
    'confirmTurnInEstimate'  => $locale->t('Voulez-vous vraiment basculer la fiche en devis de remise en état ?'),
    'confirmTurnInServices'  => $locale->t('Voulez-vous vraiment basculer la fiche en services sur le contrat ?'),
    'confirmTurnInDays'      => $locale->t('Voulez-vous vraiment basculer la fiche en jours sur le contrat ?'),
    'confirmAbortSheet'      => $locale->t('Voulez-vous vraiment abandonner la fiche ?'),
    'confirmCancelSheet'     => $locale->t('Voulez-vous vraiment annuler la fiche ?'),
    'confirmReactivateSheet' => $locale->t('Voulez-vous vraiment réactiver la fiche ?'),
    'lockKey'                => $locale->t('Merci de ne pas appuyer sur cette touche !'),
    'confirmRemoveReference' => $locale->t('Souhaitez-vous supprimer cette référence ?'),

    'typeFix'                => $locale->t('Petite rénovation'),
    'typeRepair'             => $locale->t('Grosse réparation'),
    'typeChangement'         => $locale->t('Changement de pièce'),
    'removeReferenceBtn'     => $locale->t('Supprimer cette référence'),
    
    'placeholderCode'        => $locale->t('Code Kimoce/constructeur'),
    'labelSupplierCode'      => $locale->t('(réf. constructeur %s)', '<%code>'),

    'searchArticle_noResult'  => $locale->t('Aucun article ne correspond à ce code dans la base de données Kimoce'),
    'searchArticle_error'     => $locale->t('Une erreur est survenue pendant la recherche'),
});

my $tabTranslations = {
    'fatalError'              => $locale->t('Erreur fatale lors de l\'enregistrement.'),
    'rightsError'             => $locale->t('Certaines informations ne sont pas modifiables.'),
    'requiredCreator'         => $locale->t('Veuillez sélectionner le créateur de la fiche.'),
    'requiredDate'            => $locale->t('Veuillez saisir une date valide.'),
    'requiredKimoceCode'      => $locale->t('Vous devez saisir tous les codes Kimoce.'),
    'unknownKimoceCode'       => $locale->t('Code Kimoce inconnu : %s.', '<%code>'),
    'invalidCostAmount'       => $locale->t('Erreur lors de l\'enregistrement, incohérence au niveau des coûts et des montants, le paramétrage a pu être modifié, veuillez contacter le service informatique pour plus d\'informations.'),
    'invalidDate'             => $locale->t('La date de création doit être inférieure ou égale à la date du jour.'),
    'requiredAbortComment'    => $locale->t('Veuillez saisir un commentaire d\'abandon.'),
    'abortError'              => $locale->t('Une erreur s\'est produite lors de l\'abandon.'),
    'cancelError'             => $locale->t('Une erreur s\'est produite lors de l\'annulation.'),
    'reactivateError'         => $locale->t('Une erreur s\'est produite lors de la réactivation.'),
    'turnInEstimateError'     => $locale->t('Une erreur s\'est produite lors de la bascule de la fiche en devis de remise en état.'),
    'turnInServicesError'     => $locale->t('Une erreur s\'est produite lors de la bascule de la fiche en services de remise en état.'),
    'turnInDaysError'         => $locale->t('Une erreur s\'est produite lors de la bascule de la fiche en jours de remise en état.'),
    'turnInDaysContractError' => $locale->t('Une erreur s\'est produite lors de la bascule de la fiche en jours de remise en état : incohérence au niveau du montant sur le contrat.'),
    'servicesError'           => $locale->t('Une erreur s\'est produite lors de l\'enregistrement des prestations techniques.'),
    'deleteServicesError'     => $locale->t('Une erreur s\'est produite lors de la suppression d\'une prestation technique : %s.', '<%id>'),
    'deleteServicesEstimateError' => $locale->t('Une erreur s\'est produite lors de la suppression d\'une prestation technique : %s : <br />Mise à jour des infos des devis existants.', '<%id>'),
    'referencesError'         => $locale->t('Une erreur s\'est produite lors de l\'enregistrement des références.'),
    'deleteReferencesError'   => $locale->t('Une erreur s\'est produite lors de la suppression de références.'),
    'deleteReferencesEstimateError' => $locale->t('Une erreur s\'est produite lors de la suppression de références : <br />Mise à jour des infos des devis existants.', '<%id>'),

};


# url
my $sheetUrl = $view->createURL('repair:sheet:edit', {'sheetId' => $tabViewData{'sheetId'}});


$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

#Liste des URL utilisé par le JS
our $searchArticleJSONUrl        = $view->createURL('repair:checkpoint:jsonSearchArticle',
                                                    {'countryId' => $tabViewData{'countryId'}});
my $searchMachineInfosJSONUrl    = $view->createURL('repair:sheet:jsonGetMachineInfos');

my $isPhotosModifiable           = ($tabViewData{'tabRights'}->{'photos'} =~ /^(|.*\|)add(|\|.*)$/);
my $uploadPhotoUrl               = $view->createURL('repair:sheet:uploadPhoto', {'isModifiable' => $isPhotosModifiable});

$view->setTitle($locale->t('Fiche de remise en état'));


#Variables pour la bascule en jours
#SpinControl du nombre de jour
my $daysInput = $view->displayFormInputText('daysNumber', 0, '',{'class' => 'spinInput'});
$daysInput .= $view->displayJSBlock('Location.spinControlsManager.createSpinControl("daysNumber", 1, null, 1, 10, 0);');


#Au chargement de la page
our $sheetState = '';
if ($tabViewData{'toTurnInto'} eq LOC::Repair::Sheet::TURNIN_DAYS || $tabViewData{'stateId'} eq LOC::Repair::Sheet::STATE_TURNEDIN || $tabViewData{'stateId'} eq LOC::Repair::Sheet::STATE_ABORTED || $tabViewData{'stateId'} eq LOC::Repair::Sheet::STATE_CANCELED)
{
    $sheetState = 'fixed';
}

# Tableau contenant les références existantes
my $js = '
/* Type de points de vérification */
var TYPE_CHANGEMENT = "' . LOC::Repair::LineType::TYPE_CHANGEMENT . '";
var TYPE_FIX = "' . LOC::Repair::LineType::TYPE_FIX . '";
var TYPE_REPAIR = "' . LOC::Repair::LineType::TYPE_REPAIR . '";

var DOCFLAG_CT_ISCUSTOMERPFREQONCREATION  = ' . LOC::Proforma::DOCFLAG_CT_ISCUSTOMERPFREQONCREATION . ';
var DOCFLAG_EXISTACTIVED                  = ' . LOC::Proforma::DOCFLAG_EXISTACTIVED . ';
';

$view->addJSSrc($js);

my @tabRefServicesList = values(%{$tabViewData{'srvList'}});

my $tabSheetCfgs = {
    'tabInfos' => {
        'id'                  => $tabViewData{'sheetId'},
        'agencyId'            => $tabViewData{'agencyId'},
        'contractId'          => $tabViewData{'contractId'},
        'tabMachineContracts' => $tabViewData{'tabMachineContracts'},
        'sheetState'          => $sheetState,
        'finalSheetCst'       => $tabViewData{'finalSheetCst'},
        'finalSheetAmt'       => $tabViewData{'finalSheetAmt'},
        'toTurnInto'          => $tabViewData{'toTurnInto'}
    },
    'currencyId'        => $tabViewData{'currencyId'},
    'isMarginAllowed'   => 0,
    'isMarginApplied'   => 0,
    'searchArticleUrl'  => $searchArticleJSONUrl,
    'uploadPhotoUrl'    => $uploadPhotoUrl,
    'searchMachineUrl'  => $searchMachineInfosJSONUrl,
    'tabOldCheckpoints' => $tabViewData{'tabOldCheckpoints'},
    'tabReferences'     => $tabViewData{'tabReferences'},
    'tabTempServices'   => $tabViewData{'srvAdded'},
    'tabRefServices'    => \@tabRefServicesList,
    'tabServices'       => $tabViewData{'services'},
    'tabImages'         => $tabViewData{'images'},

};
$view->addBodyEndCode($view->displayJSBlock('
Location.repairSheetObj = new Location.RepairSheet("' . $tabViewData{'countryId'} . '", ' .
                                                          &LOC::Json::toJson($tabSheetCfgs) . ', ' .
                                                          &LOC::Json::toJson($tabViewData{'tabRights'}) . ');'));




#Blocage de la touche F5 sur la FRE
$view->addBodyEvent('onkeydown', 'LOC_View.lockKeys(event, [116], LOC_View.getTranslation(\'lockKey\'));');

#Blocage de la touche "BACKSPACE" lorsque fiche non éditable
if ($tabViewData{'tabRights'}->{'actions'} !~ /^(|.*\|)valid(|\|.*)$/)
{
    $view->addBodyEvent('onkeydown', 'if (event.keyCode == 8) return false;');
}

# CSS
$view->addCSSSrc($dir . 'edit.css');

# Barre de recherche du haut
require($directory . '/sheet/searchBar.cgi');

# Message de confirmation
if ($tabViewData{'messages'}->{'valid'})
{
    my $validBlock = '
    <div id="validBlock" class="messageBlock valid" >
        <div class="popup" style="display: block;">
            <div class="content">
                <table>
                <tr>
                    <td style="font-weight: bold;">' . $locale->t($tabViewData{'messages'}->{'valid'}) . '.</td>
                </tr>
                <tr class="buttons">
                    <td>' .
                    $view->displayTextButton($locale->t('Ok'), '',
                                             '$ge("validBlock").style.display = "none";',
                                             '', 'l', {'id' => 'validBlockBtn'}) .
                    '</td>
                </tr>
                </table>
            </div>
        </div>
    </div>';
    
    $view->addBodyContent($validBlock, 0, 1);
    $view->addBodyEvent('load', 'window.document.getElementById("validBlockBtn").focus();');
}

#Message d'erreur
my $errorBlock = '
<div id="errorBlock" class="messageBlock error"'; 
(@{$tabViewData{'messages'}->{'error'}} > 0) ?  $errorBlock .= 'style="display: visible;"' : $errorBlock .= 'style="display: none;"';

    $errorBlock .= '>
        <div class="popup" style="display: block;">
            <div class="content">
                <table>
                 <tbody id="errorsTable">
                 <tr>
                    <td>
                        <ul style="font-weight : bold;">';

    #Erreurs retournées depuis PERL
    foreach my $error (@{$tabViewData{'messages'}->{'error'}})
    {
        my $errorText;
        if (ref $error eq 'ARRAY' && defined $tabTranslations->{$error->[0]})
        {
            $errorText = $tabTranslations->{$error->[0]};
            foreach my $replacementCode (keys(%{$error->[1]}))
            {
                $errorText =~ s/\<%$replacementCode\>/$error->[1]->{$replacementCode}/;
            }
        }
        elsif (defined $tabTranslations->{$error})
        {
            $errorText = $tabTranslations->{$error};
        }
        else
        {
            $errorText = $error;
        }
        $errorBlock .= '<br/><li>' . $errorText . '</li>';
    }

    $errorBlock .= '</ul></td></tr>
                    <tr class="buttons" id="buttonRow">
                        <td>' .
                            $view->displayTextButton($locale->t('Fermer'), '', '$ge("errorBlock").style.display = "none";') .
                        '</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>';
    
    $view->addBodyContent($errorBlock, 0, 1);

# Affichage de l'entête
$view->{'_networkOptimization'} = 'repair-sheet-edit';
print $view->displayHeader();


print $view->startForm();


#Affichage final

print '<table style="width: 100%; margin-bottom: 10px;">
           <tr style="vertical-align: top;">';
print '<td style="padding-right: 5px;">';


require $tabViewData{'scriptPath'} . 'edit/description.cgi'; 

print '</td>
        <td style="padding-left: 5px;">';
        
require $tabViewData{'scriptPath'} . 'edit/intervention.cgi';

print '</td>
    </tr>
</table>';

require $tabViewData{'scriptPath'} . 'edit/checkpoints.cgi';

print '<table style="width: 100%;">
            <tr style="vertical-align: top;">
                <td style="padding-right: 10px;" width = "50%">'; 
            
require $tabViewData{'scriptPath'} . 'edit/services.cgi';

print '</td><td>';
 
require $tabViewData{'scriptPath'} . 'edit/otherServices.cgi';
require $tabViewData{'scriptPath'} . 'edit/photos.cgi';

print '</td></tr></table>';

require $tabViewData{'scriptPath'} . 'edit/cost.cgi';


# Boutons à gauche
my @leftPart;
# Boutons à droite
my @rightPart;

# Affichage des boutons
if ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)valid(|\|.*)$/)
{
    my $btnValid = $view->displayTextButton($locale->t('Valider'),
                                            'valid(|Over).png',
                                            'return false;',
                                            $locale->t('Valider la fiche de remise en état'),
                                            '',
                                            {'id' => 'validBtn'});

    push(@leftPart, $btnValid);
}


if ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)reinit(|\|.*)$/)
{

    my $btnReinit = $view->displayTextButton($locale->t('Rétablir'), 
                                             'revert(|Over).png',
                                             $sheetUrl, 
                                             $locale->t('Annuler les modifications effectuées sur la fiche de remise en état'));

    push(@leftPart, $btnReinit);
}
if ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)services(|\|.*)$/)
{
    my $servicesUrl = $view->createURL('rent:rentContract:view',
                                       {
                                        'createFromSheet' => 'services',
                                        'sheetId'         => $tabViewData{'sheetId'}
                                       });

    my $btnServices = $view->displayTextButton($locale->t('Services'), 
                                                'repair/estimate/services(|Over).png',
                                                'return false;',
                                                $locale->t('Basculer en services sur le contrat'),
                                                'left',
                                                {'id' => 'turnInServicesBtn'}
                                                );

    push(@rightPart, $btnServices);
}

if ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)days(|\|.*)$/)
{
    # Div contenant la fenetre de bascule en jours sur le contrat (popup)
    if ($tabViewData{'machineId'} != 0)
    {
        # Calcul des montants nécessaires
        my $sheetAmtInput = $view->displayFormInputCurrency('turnInDaysSheetAmt', 1,
                                                          LOC::Html::CURRENCYMODE_TEXT, $tabViewData{'currencySymbol'}, undef,
                                                          {'size' => '10', 'style' => 'font-weight: bold;'});

        my $unitDayAmtInput = $view->displayFormInputCurrency('unitDayAmt', 1,
                                                          LOC::Html::CURRENCYMODE_TEXT, $tabViewData{'currencySymbol'}, undef,
                                                          {'size' => '10', 'style' => 'font-weight: bold;'});

        my $contractAmtInput = $view->displayFormInputCurrency('contractAmt', 1,
                                                          LOC::Html::CURRENCYMODE_TEXT, $tabViewData{'currencySymbol'}, undef,
                                                          {'size' => '10', 'style' => 'font-weight: bold;'});

        my $btnTurnInDaysValid = $view->displayTextButton($locale->t('Valider'),
                                                          'valid(|Over).png',
                                                          'return false;',
                                                          $locale->t('Valider la bascule en jours sur le contrat'),
                                                          '',
                                                          {'id' => 'validTurnInDaysBtn'});

        print '
<div style="display: none;" id="turnIntoDays" title="' . htmllocale('Bascule en jours sur le contrat') . '">
  <center>
    <table>
        <tr>
            <td>
                <fieldset class = "sub">
                    <legend>' . htmllocale('Bascule en jours') . '</legend>
                        <table class = "formTable">
                            <tr>
                                <td class = "label">' . htmllocale('Montant de la fiche') . '</label>
                                <td class = "cstCol">' . $sheetAmtInput . '</td>
                            </tr>
                            <tr>
                                <td class = "label">' . htmllocale('Prix de location') . '</label>
                                <td class = "cstCol">' . $unitDayAmtInput .'</td>
                            </tr>
                            <tr>
                                <td class = "label">' . htmllocale('Nombre de jour(s)') . '</label>
                                <td class = "cstCol">' . $daysInput .'</td>
                            </tr>
                            <tr>
                                <td class = "label">' . htmllocale('Montant sur le contrat') . '</label>
                                <td class = "cstCol">' . $contractAmtInput . '</td>
                            </tr>
                        </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset class = "sub">
                    <table>
                        <tr>
                            <td>' . $btnTurnInDaysValid . '</td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
  </center>
</div>';
    }

    my $btnDays = $view->displayTextButton($locale->t('Jours'), 
                                            'repair/estimate/days(|Over).png',
                                            'return false;',
                                            $locale->t('Basculer en jours sur le contrat'),
                                            'left',
                                            {'id' => 'turnInDaysBtn'}
                                            );
    push(@rightPart, $btnDays);
}


if ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)repairEstimate(|\|.*)$/)
{
    my $btnEstimate = $view->displayTextButton ($locale->t('Devis de remise en état'), 
                                                'repair/sheet/filenew(|Over).png',
                                                'return false;', 
                                                 $locale->t('Basculer en devis de remise en état'),
                                                'left',
                                                {'id' => 'turnInEstimateBtn'});

    push(@rightPart, $btnEstimate);
}


#bouton abandonner
if ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)abort(|\|.*)$/)
{
    require $tabViewData{'scriptPath'} . 'edit/popupAbort.cgi'; 

    my $btnAbort = $view->displayTextButton($locale->t('Abandonner'), 
                                           'repair/sheet/abort(|Over).png',
                                           'return false;',
                                           $locale->t('Abandonner la fiche de remise en état'),
                                           'left',
                                           {'id' => 'abortBtn'}
                                           );

    push(@rightPart, $btnAbort);
}

#bouton réactiver
if ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)reactivate(|\|.*)$/)
{
    my $btnReactivate = $view->displayTextButton ($locale->t('Réactiver'), 
                                                'revert(|Over).png',
                                                'return false;', 
                                                 $locale->t('Réactiver la fiche de remise en état'),
                                                'left',
                                                {'id' => 'reactivateBtn'});

    push(@rightPart, $btnReactivate);
}

#bouton annuler
if ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)cancel(|\|.*)$/)
{
    my $btnCancel = $view->displayTextButton($locale->t('Annuler'), 
                                           'cancel(|Over).png',
                                           'return false;',
                                           $locale->t('Annuler la fiche de remise en état'),
                                           'left',
                                           {'id' => 'cancelBtn'}
                                           );

    push(@rightPart, $btnCancel);
}
#Bouton d'avis Client de Remis en etat
if ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)print(|\|.*)$/)
{
    my $pdfUrl = {'url' => 'repair:sheet:pdfNotice', 'options' => {'id' => $tabViewData{'sheetId'}}};
    my $pdfName = $locale->t('AI_Avis_%s.pdf', $tabViewData{'sheetId'});
    my $btnEMail = $view->displayPrintButtons(
                                               'emailBtns',
                                               $pdfUrl,
                                               $pdfName,
                                               $tabViewData{'orderContact.email'},
                                               $locale->t('Votre avis de remise en état'),
                                               &LOC::Characteristic::getCountryValueByCode('MAILFRE', $tabViewData{'countryId'}),
                                               $tabViewData{'agencyId'},
                                               {'label' => $locale->t('Avis au client'),
                                                'tabEndActions' => [
                                                    {
                                                        'type'      => 'logPrint',
                                                        'entity'    => 'FRE',
                                                        'entityIds' => [$tabViewData{'sheetId'}]
                                                    }
                                                ]}
                                              );
    push(@rightPart, $btnEMail);
}

#Bouton Print/PDF
if ($tabViewData{'tabRights'}->{'actions'} =~ /^(|.*\|)fullprint(|\|.*)$/)
{

    push(@rightPart, '<span></span>');

    my $pdfUrl = {'url' => 'repair:sheet:pdf', 'options' => {'id' => $tabViewData{'sheetId'}}};
    my $pdfName = $locale->t('AI_Fiche_%s.pdf', $tabViewData{'sheetId'});
    my $btnPrint = $view->displayPrintButtons(
                                               'printBtns',
                                               $pdfUrl,
                                               $pdfName,
                                               '',
                                               $locale->t('Votre fiche de remise en état'),
                                               '',
                                               $tabViewData{'agencyId'},
                                               {
                                                   'buttons' => ['print', 'pdf'],
                                                   'label' => $locale->t('Fiche'),
                                                    'tabEndActions' => [
                                                        {
                                                            'type'      => 'logPrint',
                                                            'entity'    => 'FRE',
                                                            'entityIds' => [$tabViewData{'sheetId'}]
                                                        }
                                                    ]
                                               }
                                              );
    push(@rightPart, $btnPrint);
}



# Affichage du Panel du bas
print $view->displayControlPanel({'left' => \@leftPart, 'right' => \@rightPart, 'substyle' => 'bottom'});

# Hiddens
print $view->displayFormInputHidden('valid', '');
print $view->displayFormInputHidden('sheetId', $tabViewData{'sheetId'});
print $view->displayFormInputHidden('machineId', $tabViewData{'machineId'});
print $view->displayFormInputHidden('proformaFlags', $tabViewData{'proformaFlags'});
print $view->displayFormInputHidden('agencyId', $tabViewData{'agencyId'});
print $view->displayFormInputHidden('familyId', $tabViewData{'familyId'});
print $view->displayFormInputHidden('sheetUsrId', $tabViewData{'sheetUsrId'});
print $view->displayFormInputHidden('countryId', $tabViewData{'countryId'});
print $view->displayFormInputHidden('stateId', $tabViewData{'stateId'});
print $view->displayFormInputHidden('extSrvMargin', $tabViewData{'extSrvMargin'});
print $view->displayFormInputHidden('sheetMaxAmount', $tabViewData{'sheetMaxAmount'});
print $view->displayFormInputHidden('turnIn', $tabViewData{'turnIn'});
print $view->displayFormInputHidden('invoiceState');
print $view->displayFormInputHidden('unitDayAmtHidden');#Prix de loc unitaire
print $view->displayFormInputHidden('daysNumberHidden');#Nb de jours à basculer
print $view->displayFormInputHidden('contractAmtHidden');#Montant sur le contrat à basculer
print $view->displayFormInputHidden('abortCommentHidden');#Commentaire abandon
print $view->displayFormInputHidden('tabOldCheckpoints', '[]');# Anciens points de vérification
print $view->displayFormInputHidden('tabReferences', '[]');# Références

# Affichage du pied de page
print $view->endForm();

print $view->displayFooter();
