use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;
use LOC::Locale;

our %tabViewData;
our $view;
our $locale;


$locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
$view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}

my $isNew = $tabViewData{'id'} ? 0 : 1; # Ajout ou modification


$view->setPageTitle($locale->t('Point de vérification'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle($locale->t('Point de vérification'));

# CSS
$view->addCSSSrc('location.css');
$view->addCSSSrc('repair/checkpoint.css');

# JS
$view->addJSSrc('repair/checkpoint.js');

if ($isNew)
{
    $view->addBodyEvent('onload', 'setFirstLoad(1);');
}
if ($tabViewData{'isArticleRequired'} == 1 || $tabViewData{'isChgPartAllowed'} == 0)
{
    $view->addBodyEvent('onload', "setArticlesActivation(-1)");
}
else
{
    $view->addBodyEvent('onload', "setArticlesActivation(1)");
}

# Traductions
$view->addTranslations({
    'confirmRemoveArticle'   => $locale->t('Souhaitez-vous supprimer cet article ?'),
    'searchArticle_noResult' => $locale->t('Aucun article ne correspond à ce code dans la base de données Kimoce'),
});


# Éléments du formulaire

# intitulé
my $labelInput = $view->displayFormInputText('label', $tabViewData{'label'}, '',
    {'size' => '40', 'maxlength' => '45', 'autocomplete' => 'off'});

# petites réparations autorisées
my $isFixAllowedInput = $view->displayFormInputCheckBox('isFixAllowed',
    $tabViewData{'isFixAllowed'}, {'onclick' => 'changeFixAllowed(this)'},
    htmllocale('autorisées'), 1);

# coût des petites réparations
my $fixCost = $view->displayFormInputCurrency('fixCost',
    &LOC::Util::round($tabViewData{'fixCost'}, $locale->getFracDigits()),
                     LOC::Html::CURRENCYMODE_INPUT, $tabViewData{'currencySymbol'});

# grosses réparations autorisées
my $isRepairAllowedInput = $view->displayFormInputCheckBox('isRepairAllowed',
    $tabViewData{'isRepairAllowed'}, {'onclick' =>'changeRepairAllowed(this)'},
    htmllocale('autorisées'), 1);

# coût des grosses réparations
my $repairCost = $view->displayFormInputCurrency('repairCost',
    &LOC::Util::round($tabViewData{'repairCost'}, $locale->getFracDigits()), 
                    LOC::Html::CURRENCYMODE_INPUT, $tabViewData{'currencySymbol'});

# changement de pièce autorisé
my $isChgPartAllowedInput = $view->displayFormInputCheckBox('isChgPartAllowed',
    $tabViewData{'isChgPartAllowed'}, {'onclick' =>'changeIsChgPartAllowed(this)'},
    htmllocale('autorisé'), 1);

# code Kimoce obligatoire
my $isArticleRequiredInput = $view->displayFormInputCheckBox('isArticleRequired',
    $tabViewData{'isArticleRequired'}, {'onclick' => 'changeIsArticleRequired(this)', 'id' => 'cbIsArticleRequired'},
    htmllocale('Saisie d\'un code Kimoce obligatoire'), 1);
    
# marge autorisée
my $isMarginAllowedInput = $view->displayFormInputCheckBox('isMarginAllowed',
    $tabViewData{'isMarginAllowed'}, {'id' => 'cbIsMarginAllowed'},
    htmllocale('Appliquer la marge'), 1);

# champs cachés
my $htmlHidden = '';
$htmlHidden .= $view->displayFormInputHidden('id', $tabViewData{'id'});
$htmlHidden .= $view->displayFormInputHidden('valid', $tabViewData{'valid'});
$htmlHidden .= $view->displayFormInputHidden('countryId', $tabViewData{'countryId'});
$htmlHidden .= $view->displayFormInputHidden('urlSearchArticle',
                                             $view->createURL('repair:checkpoint:jsonSearchArticle'));

# Bouton de validation
my $btnValid = $view->displayTextButton(($isNew ? htmllocale('Ajouter') : htmllocale('Valider')),
    'valid(|Over).png', 'submitForm()');

my $btnReinit = $view->displayTextButton(htmllocale('Réinitialiser'), 'undo(|Over).png',
    'resetForm()');

# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();


# "Définition"
print '
<fieldset>
    <legend>' . htmllocale('Point de vérification') . '</legend>
    <table class="formTable">
        <tr>
            <td class="label">' . htmllocale('Libellé') . '</td>
            <td colspan="2">' . $labelInput . '</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="label">' . htmllocale('Petites rénovations') . '</td>
            <td width="120px;">' . $isFixAllowedInput . '</td>
            <td class="label">
                <span id="spanFixCost" style="visibility: ' . ($tabViewData{'isFixAllowed'} ? 'visible' : 'hidden') . ';">
                    ' . htmllocale('Coût') . ' : ' . $fixCost . '
                </span>
            </td>
        </tr>
        <tr>
            <td class="label">' . htmllocale('Grosses réparations') . '</td>
            <td>' . $isRepairAllowedInput . '</td>
            <td class="label">
                <span id="spanRepairCost" style="visibility: ' . ($tabViewData{'isRepairAllowed'} ? 'visible' : 'hidden') . ';">
                    ' . htmllocale('Coût') . ' : ' . $repairCost . '
                </span>
            </td>
        </tr>
        <tr>
            <td class="label">' . htmllocale('Changement de pièce') . '</td>
            <td>' . $isChgPartAllowedInput . '</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2">
                <span id="spanIsArticleRequired" style="visibility: ' . ($tabViewData{'isChgPartAllowed'} ? 'visible' : 'hidden') . ';">
                    ' . $isArticleRequiredInput . '
                </span>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2">
                <span id="spanIsMarginAllowed" style="visibility: ' . ($tabViewData{'isChgPartAllowed'} ? 'visible' : 'hidden') . ';">
                    ' . $isMarginAllowedInput . '
                </span>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
</fieldset>';

print '
<table>
<tr style="vertical-align: top;">';
    
# "Familles"
print '
<td style="padding-right: 10px;">
<fieldset>
<legend>' . htmllocale('Familles') . '</legend>
    <div style="position: relative;" title="' . htmllocale('Familles machine') . '">
        <table class="standard">
            <tr>
                <th>' . htmllocale('Famille') . '</th>
                <th>' . htmllocale('Ordre') . '</th>
                <th>' . htmllocale('Famille') . '</th>
                <th>' . htmllocale('Ordre') . '</th>
            </tr>';
my $families = $tabViewData{'families'};
my $familyValues = $tabViewData{'tabFamilyValues'};
my $count = scalar keys(%$families);
if ($count > 0)
{
    my $i = 0;
    while (my ($id, $family) = each(%$families))
    {
        if (($i % 2) == 0)
        {
            print '<tr>';
        }
        
        my $checked = 0;
        my $order = '';
        if (defined $familyValues->{$id})
        {
            $checked = 1;
            $order = $familyValues->{$id}->{'order'};
        }

        print $view->displayFormInputHidden('tabFamily['. $i .'][id]', $family->{'id'});
        print '
        <td>' .
            $view->displayFormInputCheckBox('tabFamily['. $i .'][cb]',
                        $checked, {'id' => 'tabFamily['. $i .'][cb]'}, $family->{'fullName'}, 1) . '
        </td>
        <td>' .
            $view->displayFormInputText('tabFamily['. $i .'][order]', $order, '',
                        {'size' => 4, 'maxlength' => 3, 'autocomplete' => 'off'}) . '
        </td>';

        if ((($i % 2) == 1) || $i == ($count - 1))
        {
            print '</tr>';
        }
        
        $i++;
    }
}
print '
        </table>
    </div>
</fieldset>
</td>';

# "Articles"
print '
<td>
<fieldset>
<legend>' . htmllocale('Codes articles') . '</legend>
<div style="position: relative;">
    <div id="articlesDisabled" class="checkpointDisabled" style="width: 400px;"></div>
    <div title="' . htmllocale('Codes articles') . '">
        <table id="tableArticle" class="standard" style="width: 400px;">
            <tr>
                <th>' . htmllocale('Code article') . '</th>
                <th style="width: 100px;">' . htmllocale('Prix d\'achat') . '</th>
                <th style="width: 25px;">&nbsp;</th>
            </tr>';
my $i = 0;
my $tabArticles = $tabViewData{'tabArticles'};
while (my ($id, $article) = each(%$tabArticles))
{
    print '
            <tr id="trArticle_' . $i . '">
                <td id="tdArticleCode_' . $i. '">' .
                       $view->displayFormInputHidden('tabArticle[' . $i . '][id]', $article->{'id'}) .
                       $view->displayFormInputHidden('tabArticle[' . $i . '][externalId]', $article->{'externalId'}) .
                       $article->{'code'} . '
                </td>
                <td id="tdArticleCost_' . $i . '" style="text-align: right">' .
                    $locale->getCurrencyFormat($article->{'purchaseCost'}, undef, $tabViewData{'currencySymbol'}) . '
                </td>
                <td id="tdArticleRbtn_' . $i . '" style="text-align: right">' .
                    $view->displayTextButton('', 'remove(|Over).png',
                                             'removeRowArticle(' . $i . ')',
                                             htmllocale('Supprimer cet article')) . '
                </td>
            </tr>';
    $i++;
}
print '
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td style="text-align: right;">' .
                    $view->displayTextButton('', 'add(|Over).png', 'addRowArticle();', htmllocale('Ajouter')) . '
                </td>
            </tr>
            <tr>
                <td style="background-color: #E5F1F9;">' . htmllocale('Coût moyen') . '</td>
                <td style="background-color: #E5F1F9; border-top: 1px solid #007AC2; text-align: right;">' .
                    $locale->getCurrencyFormat($tabViewData{'avgCost'}, undef, $tabViewData{'currencySymbol'}) . '
                </td>
                <td style="background-color: #E5F1F9;">&nbsp;</td>
            </tr>
        </table>';

print '
        <div style="display: none;">' .
            $view->displayFormInputHidden('tabArticleExternalId_blank') .
            $view->displayFormInputText('tabArticleCode_blank', '', '',
                                        {'id' => 'tabArticleCode_blank', 'size' => 10, 'maxlength' => 10,
                                         'onchange' => 'searchArticle("%d")'}) .
            $view->displayFormInputCurrency('tabArticlePurchaseCost_blank', '', 
                                        LOC::Html::CURRENCYMODE_TEXT, $tabViewData{'currencySymbol'}) . '
            <span id="spanArticleRbtn">' .
                $view->displayTextButton('', 'remove(|Over).png', '', htmllocale('Supprimer cet article')) . '
            </span>
        </div>
    </div>
</div>
</fieldset>
</td>';

print '
</tr>
</table>';

# Affichage des champs cachés
print '
    <div id="chpHiddens" style="display: none;">' .
        $htmlHidden . '
    </div>';

# Affichage du panel
print $view->displayControlPanel({'left' => [$btnValid],
                                  'right' => [$btnReinit],
                                  'substyle' => 'bottom'});

my $messages = $tabViewData{'messages'};
my $count = scalar keys(%$messages);
if ($count > 0)
{
    print '<p>';
    while (my ($type, $msgs) = each(%$messages))
    {
        print $view->displayMessages($type, $msgs, 1);
    }
    print '</p>';
}

print $view->displayFooter();
