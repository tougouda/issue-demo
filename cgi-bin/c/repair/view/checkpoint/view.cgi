use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;
use LOC::Locale;

our %tabViewData;
my $view;
my $locale;


$locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
$view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


$view->setPageTitle($locale->t('Liste des points de vérification'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle($locale->t('Liste des points de vérification'));

# CSS
$view->addCSSSrc('location.css');
$view->addCSSSrc('tr.rowBg0, tr.rowBg1 {cursor: pointer;}');

# JS
$view->addJSSrc('repair/checkpoint.js');


# Bouton d'ajout
my $btnAdd = $view->displayTextButton((htmllocale('Ajouter')),
                                       'add(|Over).png',
                                       'LOC_View.openWindow(\'checkpoint\', \'' . $view->createURL('repair:checkpoint:edit') . '\')');

# Bouton de suppression
my $action = 'if (confirm("' .
    $locale->t('Souhaitez-vous supprimer les points de vérifications sélectionnés ?') .
    '")) submitForm();';
my $btnDel = $view->displayTextButton(htmllocale('Supprimer'),
                                                 'remove(|Over).png',
                                                 $action, '', 'left',
                                                 {'id' => 'deleteBtn', 'style' => 'visibility: hidden;'});
                                                 
# bouton de raffraîchissement
my $btnRefresh = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png', 'refresh()');

$view->addControlsContent($btnAdd . '&nbsp;' . $btnDel);
$view->addControlsContent($btnRefresh, 'right');

# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();

print '
<br />
<table id="tableArticle" class="standard ex" style="width: 100%;">
    <tr>
        <th>' . htmllocale('Id') . '</th>
        <th>' . htmllocale('Libellé') . '</th>
        <th width="15px;">&nbsp;</th>
    </tr>';
my $i = 1;
my $tabCheckpoints = $tabViewData{'tabCheckpoints'};
while (my ($id, $checkpoint) = each(%$tabCheckpoints))
{
    my $url = $view->createURL('repair:checkpoint:edit', {'id' => $id});
    
    print '
    <tr class="rowBg'. ($i % 2) .'" onclick="LOC_View.openWindow(\'checkpoint\', \'' . $url . '\')">
        <td>' .
               $checkpoint->{'id'} . '
        </td>
        <td>' .
            $checkpoint->{'label'} . '
        </td>
        <td onclick="LOC_Common.stopEventBubble(event)">' .
            $view->displayFormInputCheckBox('checkpointsToDelete['. $id .']', 0, 
                                            {'id' => 'checkpointsToDelete['. $id .']',
                                            'onclick' => 'showHideDeleteButton(\'checkpointsToDelete\')'},
                                            '', 1) . '
        </td>
    </tr>';
            
    $i++;
}
print '
</table>';

# champs cachés
print $view->displayFormInputHidden('valid', '');

# affichage des messages
my $messages = $tabViewData{'messages'};
my $count = scalar keys(%$messages);
if ($count > 0)
{
    print '<p>';
    while (my ($type, $msgs) = each(%$messages))
    {
        print $view->displayMessages($type, $msgs, 1);
    }
    print '</p>';
}

print $view->displayFooter();
