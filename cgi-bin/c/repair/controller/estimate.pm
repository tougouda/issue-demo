use utf8;
use open (':encoding(UTF-8)');

# Package: estimate
# Contrôleur devis de remise en état
package estimate;

# Constants: Paramètres d'affichage
# NBSEARCHSTEP - Nombre de FRE à afficher dans une recherche et lors du clic sur "Voir plus"
use constant
{
    NBSEARCHSTEP        => 100
};

use strict;
use File::Basename;

use LOC::Family;
use LOC::Repair::Checkpoint;
use LOC::Repair::Service;
use LOC::Repair::Estimate;
use LOC::Repair::OtherService;
use LOC::Repair::Sheet;
use LOC::Agency;
use LOC::Contract;
use LOC::Contract::Rent;
use LOC::Customer;
use LOC::Site;
use LOC::Machine;
use LOC::MachinesFamily;
use LOC::Date;

use LOC::User;
use LOC::Util;
use LOC::Locale;


# Function: editAction
# Prépare les données pour l'affichage de l'interface d'édition d'un devis de remise en état
sub editAction
{
    my $tabParameters = $_[0];

    #Récupération des infos user
    my $user = $tabParameters->{'tabUserInfos'};


    my $session = &LOC::Session::getInfos();

    # Variables pour la vue
    our %tabViewData = (
        'user.agency.id' => $user->{'nomadAgency.id'},
        'timeZone'       => $session->{'timeZone'},
    );

    # hachage de l'utilisateur
    $tabViewData{'user'}        = $user;
    $tabViewData{'localeId'}    = $user->{'locale.id'};
    $tabViewData{'countryId'}   = $user->{'nomadCountry.id'};
    $tabViewData{'currencyId'}  = $user->{'currency.id'};
    $tabViewData{'currencySymbol'}  = $user->{'currency.symbol'};
    # Détails de la main d'oeuvre et du déplacement
    $tabViewData{'displLabDetails'} = &LOC::Characteristic::getAgencyValueByCode('FACDEPLMOE', $tabViewData{'agencyId'});

    # Récupération des variables du formulaire
    $tabViewData{'id'}                 = &LOC::Request::getInteger('id', 0);
    $tabViewData{'sheetId'}            = &LOC::Request::getInteger('sheetId', 0);
    $tabViewData{'agencyId'}           = &LOC::Request::getString('agencyId');
    $tabViewData{'isVatInvoiced'}      = &LOC::Request::getInteger('isVatInvoiced', 0);
    $tabViewData{'orderCode'}          = &LOC::Request::getString('orderCode');
    $tabViewData{'object'}             = &LOC::Request::getString('object');
    $tabViewData{'comments'}           = &LOC::Request::getString('comments');
    $tabViewData{'notes'}              = &LOC::Request::getString('notes');
    $tabViewData{'residuesMode'}       = &LOC::Request::getInteger('residuesMode');
    $tabViewData{'residuesValue'}      = &LOC::Request::getInteger('residuesValue');
    $tabViewData{'tabLines'}           = &LOC::Request::getHash('line');
    $tabViewData{'tabServices'}        = &LOC::Request::getHash('service');
    $tabViewData{'totalCost'}          = &LOC::Request::getInteger('totalCost');
    $tabViewData{'totalAmount'}        = &LOC::Request::getInteger('totalAmount');
    $tabViewData{'reductionPercent'}   = &LOC::Request::getInteger('reductionPercent', 0);
    $tabViewData{'reductionAmount'}    = &LOC::Request::getInteger('reductionAmount');
    $tabViewData{'finalAmount'}        = &LOC::Request::getInteger('finalAmount');
    $tabViewData{'valid'}              = &LOC::Request::getString('valid', '');
    $tabViewData{'reactivateSheet'}    = &LOC::Request::getInteger('reactivateSheet', 0);
    # Id de l'interlocuteur
    $tabViewData{'estimateUserId'}      = &LOC::Request::getInteger('estimateUserId', 0);

    # Suppression des \r
    $tabViewData{'object'} =~ s/\r//g;
    $tabViewData{'comments'} =~ s/\r//g;
    $tabViewData{'notes'} =~ s/\r//g;

    # Listes
    $tabViewData{'otherServices'} = &LOC::Repair::OtherService::getList($tabViewData{'countryId'}, LOC::Util::GETLIST_PAIRS);

    # Ajout, modification
    if ($tabViewData{'valid'} ne '')
    {
        my $country = &LOC::Country::getInfos($tabViewData{'countryId'});
        my $vatPct = ($tabViewData{'isVatInvoiced'} ? $country->{'defaultRate.value'} : 0);

        # Récupération des formats Sage (quantité, prix et montant)
        my $charac = &LOC::Characteristic::getCountryValueByCode('FORMATSSAGE', $tabViewData{'countryId'});
        my $tabSageFormats = {'quantity' => 2, 'price' => 4, 'amount' => 2};
        if ($charac ne '')
        {
            $tabSageFormats = &LOC::Json::fromJson($charac);
        }

        # Préparation des données
        my $values = {
            'currencyId'       => $tabViewData{'currencyId'},
            'sheetId'          => $tabViewData{'sheetId'},
            'agencyId'         => $tabViewData{'agencyId'},
            'userId'           => $user->{'id'},
            'orderCode'        => $tabViewData{'orderCode'},
            'object'           => $tabViewData{'object'},
            'comments'         => $tabViewData{'comments'},
            'notes'            => $tabViewData{'notes'},
            'residuesMode'     => $tabViewData{'residuesMode'},
            'residuesValue'    => $tabViewData{'residuesValue'},
            'totalCost'        => $tabViewData{'totalCost'},
            'totalAmount'      => $tabViewData{'totalAmount'},
            'reductionPercent' => $tabViewData{'reductionPercent'},
            'reductionAmount'  => $tabViewData{'reductionAmount'},
            'finalAmount'      => $tabViewData{'finalAmount'},
            'estimateUserId'   => $tabViewData{'estimateUserId'},
            'vatPercent'       => $vatPct
        };
        if ($tabViewData{'id'} != 0)
        {
            $values->{'id'} = $tabViewData{'id'};
        }

        # Préparation pour l'enregistrement des lignes de devis et de service et calcul des totaux
        my $tab;
        my $j;

        # totaux
        my $totalCost = 0;
        my $totalAmount = 0;
        my $finalAmount = 0;
        my $reductionAmount = 0;

        # Les lignes de devis
        $tab = $tabViewData{'tabLines'};
        $j = 0;
        tie(my %lines, 'Tie::IxHash');
        while (my ($i, $line) = each(%$tab))
        {
            if ($line->{'type'} ne '')
            {
                $lines{$j} = $line;
                $j++;

                my $cost              = &LOC::Util::round($line->{'cost'}, $tabSageFormats->{'price'});
                $totalCost += $cost;
                if ($line->{'isToInvoice'} == 1)
                {
                    my $quantity          = &LOC::Util::round($line->{'quantity'}, $tabSageFormats->{'quantity'});
                    my $unitAmount        = &LOC::Util::round($line->{'unitAmount'}, $tabSageFormats->{'price'});
                    my $reductionPc       = ($tabViewData{'reductionPercent'} > 0 ? $tabViewData{'reductionPercent'}
                                                                                  : $line->{'reductionPercent'});
                    my $total             = &LOC::Util::round($unitAmount * $quantity * (1 - $reductionPc / 100),
                                                              $tabSageFormats->{'amount'});
                    my $reducedUnitAmount = 0;
                    if ($quantity != 0)
                    {
                        $reducedUnitAmount = &LOC::Util::round($total / $quantity, $tabSageFormats->{'price'});
                    }
                    my $reduction         = &LOC::Util::round($unitAmount - $reducedUnitAmount,
                                                              $tabSageFormats->{'price'});
                    my $amount            = &LOC::Util::round($quantity * $unitAmount, $tabSageFormats->{'price'});

                    $totalAmount += ($tabViewData{'reductionPercent'} > 0 ? $amount : $total);
                    $reductionAmount += ($tabViewData{'reductionPercent'} > 0 ? $reduction*$quantity : 0);
                    $finalAmount += $total;
                }
            }
        }

        $values->{'lines'} = \%lines;

        # Les services
        $tab = $tabViewData{'tabServices'};
        my $services = [];
        while (my ($i, $service) = each(%$tab))
        {
            if ($service->{'type'} ne LOC::Repair::LineType::TYPE_SERVICE || $service->{'serviceId'} != 0)
            {
                if ($service->{'isToInvoice'})
                {
                    my $quantity          = &LOC::Util::round($service->{'quantity'}, $tabSageFormats->{'quantity'});
                    my $unitAmount        = &LOC::Util::round($service->{'unitAmount'}, $tabSageFormats->{'price'});
                    my $reductionPc       = ($tabViewData{'reductionPercent'} > 0 ? $tabViewData{'reductionPercent'}
                                                                                  : $service->{'reductionPercent'});
                    my $total             = &LOC::Util::round($unitAmount * $quantity * (1 - $reductionPc / 100),
                                                              $tabSageFormats->{'amount'});
                    my $reducedUnitAmount = 0;
                    if ($quantity != 0)
                    {
                        $reducedUnitAmount = &LOC::Util::round($total / $quantity, $tabSageFormats->{'price'});
                    }
                    my $reduction         = &LOC::Util::round($unitAmount - $reducedUnitAmount,
                                                              $tabSageFormats->{'price'});
                    my $amount            = &LOC::Util::round($quantity * $unitAmount, $tabSageFormats->{'price'});
                    my $cost              = &LOC::Util::round($service->{'cost'}, $tabSageFormats->{'price'});

                    $totalCost   += $cost;
                    $totalAmount += ($tabViewData{'reductionPercent'} > 0 ? $amount : $total);
                    $reductionAmount += ($tabViewData{'reductionPercent'} > 0 ? $reduction*$quantity : 0);
                    $finalAmount += $total;
                }

                $service->{'currencyId'} = $tabViewData{'currencyId'};

                push(@$services, $service);
            }
        }
        $values->{'services'} = $services;

        # Mise à jour des totaux
        $values->{'totalCost'}       = &LOC::Util::round($totalCost, $tabSageFormats->{'amount'});
        $values->{'totalAmount'}     = &LOC::Util::round($totalAmount, $tabSageFormats->{'amount'});
        $values->{'reductionAmount'} = &LOC::Util::round(-$reductionAmount, $tabSageFormats->{'amount'});
        $values->{'finalAmount'}     = &LOC::Util::round($finalAmount, $tabSageFormats->{'amount'});
        $values->{'vat'}             = $finalAmount * $vatPct / 100;
        $values->{'taxesIncluded'}   = $finalAmount + $values->{'vat'};

        # Enregistrement
        my $result = &LOC::Repair::Estimate::set($tabViewData{'countryId'}, $values, $user->{'id'});
        if ($result > 0)
        {
            if ($tabViewData{'valid'} eq 'invoice' && $tabViewData{'id'} != 0)
            {
                my $result = &LOC::Repair::Estimate::invoice($tabViewData{'countryId'},
                                                             $tabViewData{'agencyId'},
                                                             $tabViewData{'id'},
                                                             $user->{'id'},
                                                             $tabViewData{'displLabDetails'});
                if ($result > 0)
                {
                    $tabViewData{'valid'} = 'invoiced';
                }
                else
                {
                    $tabViewData{'valid'} = '';
                    $tabViewData{'messages'} = {'error' => 'Une erreur s\'est produite lors du passage en facturation'};
                }
            }
            elsif (($tabViewData{'valid'} eq 'services' || $tabViewData{'valid'} eq 'days') && $tabViewData{'id'} != 0)
            {
                #Flag de Bascule correspondant
                my $flag;
                #Bascule en services
                if ($tabViewData{'valid'} eq 'services')
                {
                    $flag = LOC::Repair::Estimate::FLAG_TURNEDINSERVICES;
                }
                #Bascule en jours
                elsif ($tabViewData{'valid'} eq 'days')
                {
                    $flag = LOC::Repair::Estimate::FLAG_TURNEDINDAYS;
                }
                my $result = &LOC::Repair::Estimate::turnInto($tabViewData{'countryId'},
                                                              $tabViewData{'agencyId'},
                                                              $tabViewData{'id'},
                                                              $user->{'id'},
                                                              $tabViewData{'sheetId'},
                                                              $flag);
                if ($result <= 0)
                {
                    $tabViewData{'valid'} = '';
                    $tabViewData{'messages'} = {'error' => 'Une erreur s\'est produite lors de la bascule sur le contrat'};
                }
            }
            elsif ($tabViewData{'valid'} eq 'reactivated' && $tabViewData{'id'} != 0)
            {
                my $result = &LOC::Repair::Estimate::reactivate($tabViewData{'countryId'},
                                                               $tabViewData{'agencyId'},
                                                               $tabViewData{'id'},
                                                               $user->{'id'});
                if ($result > 0)
                {
                    $tabViewData{'valid'} = 'reactivated';
                }
                else
                {
                    $tabViewData{'valid'} = '';
                    $tabViewData{'messages'} = {'error' => 'Une erreur s\'est produite lors de la réactivation'};
                }
            }
            elsif ($tabViewData{'valid'} eq 'abort' && $tabViewData{'id'} != 0)
            {
                # abandon du devis
                my $result = &LOC::Repair::Estimate::abort($tabViewData{'countryId'}, $tabViewData{'agencyId'},
                                                            $tabViewData{'id'}, $user->{'id'});
                if ($result > 0)
                {
                    $tabViewData{'valid'} = 'aborted';
                }
                else
                {
                    $tabViewData{'valid'} = '';
                    $tabViewData{'messages'} = {'error' => 'Une erreur s\'est produite lors de l\'abandon'};
                }
                # réactivation de la fiche de remise en état correspondante
                if ($tabViewData{'reactivateSheet'} == 1)
                {
                    $tabViewData{'valid'} = 'reactivate';
                }
            }
            else
            {
                $tabViewData{'valid'} = 'saved';
            }
        }
        else
        {
            $tabViewData{'valid'} = '';
            $tabViewData{'messages'} = {'error' => 'Une erreur s\'est produite lors de l\'enregistrement'};
        }
    }

    # (Re)chargement des données
    if ($tabViewData{'id'} != 0 || $tabViewData{'valid'} ne '')
    {
        # Initialise le devis de remise en état
        my $obj = &LOC::Repair::Estimate::getInfos($tabViewData{'countryId'}, $tabViewData{'id'});

        $tabViewData{'code'}               = $obj->{'code'};
        $tabViewData{'sheetId'}            = $obj->{'sheet.id'};
        $tabViewData{'agencyId'}           = $obj->{'agency.id'};
        $tabViewData{'stateId'}            = $obj->{'state.id'};
        $tabViewData{'stateLabel'}         = $obj->{'state.label'};
        $tabViewData{'orderCode'}          = $obj->{'orderCode'};
        $tabViewData{'object'}             = $obj->{'object'};
        $tabViewData{'comments'}           = $obj->{'comments'};
        $tabViewData{'notes'}              = $obj->{'notes'};
        $tabViewData{'residuesMode'}       = $obj->{'residuesMode'};
        $tabViewData{'residuesValue'}      = $obj->{'residuesValue'};
        $tabViewData{'totalCost'}          = $obj->{'totalCost'};
        $tabViewData{'totalAmount'}        = $obj->{'totalAmount'};
        $tabViewData{'reductionPercent'}   = $obj->{'reductionPercent'};
        $tabViewData{'reductionAmount'}    = $obj->{'reduction'};
        $tabViewData{'finalAmount'}        = $obj->{'total'};
        $tabViewData{'dateCreation'}       = $obj->{'dateCreation'};
        $tabViewData{'dateModification'}   = $obj->{'dateModification'};
        $tabViewData{'flag'}               = $obj->{'flag'};
        $tabViewData{'estimateUserId'}     = $obj->{'interlocutor'};

        $tabViewData{'tabLines'}           = &LOC::Repair::Estimate::getLines($tabViewData{'countryId'}, $tabViewData{'id'});
        $tabViewData{'tabServices'}        = &LOC::Repair::Estimate::getOtherServices($tabViewData{'countryId'}, $tabViewData{'id'});

        # Corps du mail par défaut
        $tabViewData{'emailContent'}       = &LOC::Characteristic::getAgencyValueByCode('MAILDRE', $tabViewData{'agencyId'});
    }

    # Récupération des infos statiques des différents blocs

    # Infos fiche de remise en état
    if ($tabViewData{'sheetId'} != 0)
    {
        $tabViewData{'sheet'} = &LOC::Repair::Sheet::getInfos($tabViewData{'countryId'}, $tabViewData{'sheetId'});

        # Récupération de la liste des photos de la fiche de remise en état
        $tabViewData{'sheet'}->{'images'} = &LOC::Repair::Sheet::getPhotos($tabViewData{'countryId'}, $tabViewData{'sheetId'});
    }

    # Infos contrat
    if ($tabViewData{'sheet'}->{'contract.id'})
    {
        $tabViewData{'contract'} = &LOC::Contract::Rent::getInfos($tabViewData{'countryId'},
                                                                  $tabViewData{'sheet'}->{'contract.id'});

        #Récupération du prix de location
        if ($tabViewData{'contract'}->{'amountType'} eq 'month')
        {
            $tabViewData{'contract'}->{'dayAmount'} = &LOC::Util::round($tabViewData{'contract'}->{'unitPrice'} / LOC::Date::AVERAGE_MONTH_WORKING_DAYS, 0);
        }
        elsif ($tabViewData{'contract'}->{'amountType'} eq 'fixed')
        {
            $tabViewData{'contract'}->{'dayAmount'} = &LOC::Util::round($tabViewData{'contract'}->{'unitPrice'} / $tabViewData{'contract'}->{'duration'},0);
        }
        else
        {
            $tabViewData{'contract'}->{'dayAmount'} = $tabViewData{'contract'}->{'unitPrice'};
        }

    }

    # Infos client
    $tabViewData{'customer'} = &LOC::Customer::getInfos($tabViewData{'countryId'},
                                                        $tabViewData{'contract'}->{'customer.id'});
    $tabViewData{'UrlCustomerInfo'} = &LOC::Customer::getUrl($tabViewData{'contract'}->{'customer.id'});

    # Infos machine
    $tabViewData{'machine'} = &LOC::Machine::getInfos($tabViewData{'countryId'}, $tabViewData{'sheet'}->{'machine.id'}, LOC::Machine::GETINFOS_BASE, {'level' => LOC::Machine::LEVEL_FAMILY});

    # Infos chantier
    $tabViewData{'site'} = &LOC::Site::getInfos($tabViewData{'countryId'}, $tabViewData{'contract'}->{'site.id'});

    # Liste des users
    $tabViewData{'creatorsList'} = ();
    if ($tabViewData{'estimateUserId'} != 0)
    {
        $tabViewData{'creatorsList'} = &LOC::User::getList(LOC::Util::GETLIST_PAIRS,
                                                      {'id' => $tabViewData{'estimateUserId'}},
                                                      LOC::User::GET_ACTIVE,
                                                      {'formatMode' => 1});
    }

    # Récupère la liste des états de casse
    $tabViewData{'tabStates'} = &LOC::Repair::Estimate::getStatesList($tabViewData{'countryId'});

    # Droits de modification
    $tabViewData{'tabRights'} = &LOC::Repair::Estimate::getRights($tabViewData{'countryId'}, $tabViewData{'agencyId'}, $tabViewData{'id'}, $user->{'id'});

    # Caractéristiques
    # - Pourcentage max de remise sur les pièces
    $tabViewData{'maxReductionPercentForChangement'} = &LOC::Characteristic::getCountryValueByCode('PCMAXPIECEDRE', $tabViewData{'countryId'});

    # Message de confirmation
    if ($tabViewData{'valid'} eq 'added')
    {
        $tabViewData{'messages'} = {'valid' => 'Le devis de remise en état a été ajouté'};
    }
    elsif ($tabViewData{'valid'} eq 'saved')
    {
        $tabViewData{'messages'} = {'valid' => 'Les modifications ont été enregistrées'};
    }
    elsif ($tabViewData{'valid'} eq 'aborted')
    {
        $tabViewData{'messages'} = {'valid' => 'Le devis a été abandonné'};
    }
    elsif ($tabViewData{'valid'} eq 'invoiced')
    {
        $tabViewData{'messages'} = {'valid' => 'Le devis a été envoyé en facturation'};
    }
    elsif ($tabViewData{'valid'} eq 'reactivated')
    {
        $tabViewData{'messages'} = {'valid' => 'Le devis a été réactivé'};
    }

    # Chemin d'accès aux scripts de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    $directory .= '/view/estimate/';
    $tabViewData{'scriptPath'} = $directory;

    # Affichage de la vue, redirection vers la FRE si bascule, affichage du devis sinon
    if ($tabViewData{'valid'} eq 'services' || $tabViewData{'valid'} eq 'days' || $tabViewData{'valid'} eq 'reactivate')
    {
        require $tabViewData{'scriptPath'} . 'redirect.cgi';
    }
    else
    {
        require $tabViewData{'scriptPath'} . 'edit.cgi';
    }
}

#Affichage de la liste des devis
sub viewAction
{
    my $tabParameters = $_[0];

    # Si on provient de la barre de recherche
    my $fromSearchBar      = &LOC::Request::getInteger('fromSearchBar');

    #Dans la barre de recherche, il doit y avoir au moins un critère de sélectionné (compteur décalé, il y a toujours
    # au moins un filtre par défaut, l'agence dans laquelle on est.
    my $nbFiltersMin = 1;
    if ($fromSearchBar == 1)
    {
        $nbFiltersMin = 2;
    }

    my $user = &LOC::Session::getUserInfos();

    # Variables pour la vue
    our %tabViewData = (
        'user.agency.id' => $user->{'nomadAgency.id'},
        'offset'         => 0,
        'step'           => NBSEARCHSTEP
    );

    # Hachage de l'utilisateur
    $tabViewData{'localeId'}        = $user->{'locale.id'};
    $tabViewData{'agencyId'}        = $user->{'nomadAgency.id'};
    $tabViewData{'countryId'}       = $user->{'nomadCountry.id'};
    $tabViewData{'currencySymbol'}  = $user->{'currency.symbol'};

    # locale
    my $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});

    my $flags = LOC::Repair::Estimate::GETINFOS_MACHINE |
                LOC::Repair::Estimate::GETINFOS_CONTRACT |
                LOC::Repair::Estimate::GETINFOS_CUSTOMER;

    # Récupère la liste des états de casse
    $tabViewData{'tabStates'} = &LOC::Repair::Estimate::getStatesList($tabViewData{'countryId'}, [LOC::Repair::Estimate::STATE_WAITING,
                                                                                                  LOC::Repair::Estimate::STATE_APPROVED,
                                                                                                  LOC::Repair::Estimate::STATE_ABORTED,
                                                                                                  LOC::Repair::Estimate::STATE_INVOICED,
                                                                                                  LOC::Repair::Estimate::STATE_TURNEDIN,
                                                                                                  LOC::Repair::Estimate::STATE_NOCHANGE]);

    $tabViewData{'rentContract'}    = &LOC::Request::getString('rentContract');
    $tabViewData{'parkNumber'}      = &LOC::Request::getString('parkNumber');
    $tabViewData{'startDate'}       = &LOC::Request::getString('startDate');
    $tabViewData{'endDate'}         = &LOC::Request::getString('endDate');
    $tabViewData{'repairEstimate'}  = &LOC::Request::getString('repairEstimate');
    $tabViewData{'customerId'}      = &LOC::Request::getInteger('customerId');
    $tabViewData{'state.id'}        = &LOC::Request::getString('stateId');
    $tabViewData{'search.user.id'}  = &LOC::Request::getInteger('search.user.id');
    $tabViewData{'isNational'}      = &LOC::Request::getBoolean('isNational');

    $tabViewData{'customers'} = ();
    if ($tabViewData{'customerId'} != 0)
    {
        $tabViewData{'customers'} = &LOC::Customer::getList($tabViewData{'countryId'}, LOC::Util::GETLIST_PAIRS,
                                                              {'id' => $tabViewData{'customerId'}});
    }
    
    $tabViewData{'users'} = ();
    if ($tabViewData{'search.user.id'} != 0)
    {
        $tabViewData{'users'} = &LOC::User::getList(LOC::Util::GETLIST_PAIRS,
                                                      {'id' => $tabViewData{'search.user.id'}},
                                                      LOC::User::GET_ACTIVE,
                                                      {'formatMode' => 1});
    }


    # Filtres
    my $filters = {};
    if (!$tabViewData{'isNational'})
    {
        my @tabAuthorizedAgenciesId = keys %{$user->{'tabAuthorizedAgencies'}};
        $filters->{'agencyId'} = \@tabAuthorizedAgenciesId;
    }
    if ($tabViewData{'rentContract'} ne '')
    {
        $filters->{'contractCode'} = $tabViewData{'rentContract'};
    }
    if ($tabViewData{'parkNumber'} ne '')
    {
        $filters->{'parkNumber'} = $tabViewData{'parkNumber'};
    }
    if ($tabViewData{'repairEstimate'} ne '')
    {
        $filters->{'code'} = $tabViewData{'repairEstimate'};
    }
    if ($tabViewData{'customerId'} != 0)
    {
        $filters->{'customerId'} = $tabViewData{'customerId'};
    }
    if ($tabViewData{'search.user.id'} != 0)
    {
        $filters->{'interlocutorId'} = $tabViewData{'search.user.id'};
    }
    if ($tabViewData{'state.id'} ne '')
    {
        $filters->{'stateId'} = $tabViewData{'state.id'};
    }
    if ($tabViewData{'startDate'} ne '' || $tabViewData{'endDate'} ne '')
    {
        my $dateInterval = {};
        if ($tabViewData{'startDate'} ne '')
        {
            $dateInterval->{'start'} = $tabViewData{'startDate'};
        }
        if ($tabViewData{'endDate'} ne '')
        {
            $dateInterval->{'end'} = $tabViewData{'endDate'};
        }
        $filters->{'dateInterval'} = $dateInterval;
    }

    # nombre de critères sélectionnés
    $tabViewData{'nbFilters'} = keys %$filters;
    $tabViewData{'nbFiltersMin'} = $nbFiltersMin;
    $tabViewData{'tabFilters'} = $filters;

    # Si on ne vient pas la barre de recherche et qu'il n'y a aucun filtre, on n'affiche que les devis dans l'état en attente.
    if ($fromSearchBar != 1 && $tabViewData{'nbFilters'} == $nbFiltersMin)
    {
        $filters->{'stateId'} = $tabViewData{'state.id'} = LOC::Repair::Estimate::STATE_WAITING;
    }
    # recherche
    my $tabOptions = {
        'limit' => {
            'offset' => $tabViewData{'offset'},
            'count'  => $tabViewData{'step'},
            '&total' => 0
        }
    };

    my $tabEstimates = &LOC::Repair::Estimate::getList($tabViewData{'countryId'}, LOC::Util::GETLIST_ASSOC,
                                                    $filters,
                                                    LOC::Repair::Estimate::GETINFOS_MACHINE |
                                                    LOC::Repair::Estimate::GETINFOS_CONTRACT |
                                                    LOC::Repair::Estimate::GETINFOS_CUSTOMER,
                                                    $tabOptions);

    my @tabList;

    foreach my $tabEstimateInfos (values(%$tabEstimates))
    {
        push(@tabList, {
            'id'               => $tabEstimateInfos->{'id'},
            'code'             => $tabEstimateInfos->{'code'},
            'customerName'     => $tabEstimateInfos->{'customer.name'},
            'contractCode'     => $tabEstimateInfos->{'contract.code'},
            'machineParkNumber'=> $tabEstimateInfos->{'machine.parkNumber'},
            'object'           => $tabEstimateInfos->{'object'},
            'total'            => $tabEstimateInfos->{'total'},
            'stateId'          => $tabEstimateInfos->{'state.id'},
            'flag'             => $tabEstimateInfos->{'flag'},
            'url'              => &LOC::Request::createRequestUri('repair:estimate:edit', {'id' => $tabEstimateInfos->{'id'}})
        });
    }

    $tabViewData{'result'} = \@tabList;
    $tabViewData{'total'}  = $tabOptions->{'limit'}->{'&total'};


    if ($tabViewData{'total'} == 1)
    {
        my $url = &LOC::Request::createRequestUri('repair:estimate:edit', {'id' => $tabList[0]->{'id'}});
        # Redirection
        &LOC::Browser::sendHeaders("Status: 302 Found\nLocation: " . $url . "\n\n");
    }

    $tabViewData{'fromSearchBar'} = $fromSearchBar;

    # chemin d'accès aux scripts de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    $directory .= '/view/estimate/';

    # affichage de la vue
    require $directory . 'view.cgi';

}

# Function: jsonSearchAction
# Recherche de devis de remise en état
sub jsonSearchAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
    );

    my $countryId  = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $tabFilters = &LOC::Json::fromJson(&LOC::Request::getString('tabFilters', ''));
    $tabViewData{'offset'} = &LOC::Request::getInteger('offset', 0);
    $tabViewData{'step'}   = &LOC::Request::getInteger('step', 100);


    # nombre de critères sélectionnés
    $tabViewData{'nbFilters'} = keys %$tabFilters;
    $tabViewData{'tabFilters'}   = $tabFilters;

    # s'il y a assez de critères sélectionnés
    if ($tabViewData{'nbFilters'} > 0)
    {
        my $tabOptions = {
            'limit' => {
                'offset' => $tabViewData{'offset'},
                'count'  => $tabViewData{'step'},
                '&total' => 0
            }
        };
        
        my $tabEstimates = &LOC::Repair::Estimate::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                        $tabFilters,
                                                        LOC::Repair::Estimate::GETINFOS_MACHINE |
                                                        LOC::Repair::Estimate::GETINFOS_CONTRACT |
                                                        LOC::Repair::Estimate::GETINFOS_CUSTOMER,
                                                        $tabOptions);

        my @tabList;

        foreach my $tabEstimateInfos (values(%$tabEstimates))
        {
            push(@tabList, {
                'id'               => $tabEstimateInfos->{'id'},
                'code'             => $tabEstimateInfos->{'code'},
                'customerName'     => $tabEstimateInfos->{'customer.name'},
                'contractCode'     => $tabEstimateInfos->{'contract.code'},
                'machineParkNumber'=> $tabEstimateInfos->{'machine.parkNumber'},
                'object'           => $tabEstimateInfos->{'object'},
                'total'            => $tabEstimateInfos->{'total'},
                'stateId'          => $tabEstimateInfos->{'state.id'},
                'flag'             => $tabEstimateInfos->{'flag'},
                'url'              => &LOC::Request::createRequestUri('repair:estimate:edit', {'id' => $tabEstimateInfos->{'id'}})
            });
        }

        $tabViewData{'result'} = \@tabList;
        $tabViewData{'total'}  = $tabOptions->{'limit'}->{'&total'};
    }


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/estimate/jsonSearch.cgi';
}


# Function: pdfAction
# Exemple de génération de PDF
sub pdfAction
{
    my $tabParameters = $_[0];


    #Récupération des infos user
    my $user    = $tabParameters->{'tabUserInfos'};

    my $session = &LOC::Session::getInfos();

    # Variables pour la vue
    our %tabViewData;

    $tabViewData{'timeZone'}  = $session->{'timeZone'};

    # hachage de l'utilisateur
    $tabViewData{'localeId'}        = $user->{'locale.id'};
    $tabViewData{'agencyId'}        = $user->{'nomadAgency.id'};
    $tabViewData{'countryId'}       = $user->{'nomadCountry.id'};
    $tabViewData{'currencySymbol'}  = $user->{'currency.symbol'};
    # Détails de la main d'oeuvre et du déplacement
    $tabViewData{'displLabDetails'} = &LOC::Characteristic::getAgencyValueByCode('FACDEPLMOE', $tabViewData{'agencyId'});

    $tabViewData{'id'} = &LOC::Request::getInteger('estimateId', 3);

    # PDF complémentaires à insérer
    my $tabAdditionalDocuments = &LOC::Json::fromJson(&LOC::Characteristic::getCountryValueByCode('PDFCPLDRE', $tabViewData{'countryId'}));
    foreach my $tabZone (values(%$tabAdditionalDocuments))
    {
        foreach my $documentInfos (@$tabZone)
        {
            $documentInfos->{'path'} =~ s/<%commonFilesPath>/@{[&LOC::Globals::get('commonFilesPath')]}/;
        }
    }
    $tabViewData{'tabAdditionalDocuments'} = $tabAdditionalDocuments;


    # Initialise le devis de remise en état
    my $estimObj = &LOC::Repair::Estimate::getInfos($tabViewData{'countryId'}, $tabViewData{'id'});
    my $sheetObj = &LOC::Repair::Sheet::getInfos($tabViewData{'countryId'}, $estimObj->{'sheet.id'});
    my $contrObj = &LOC::Contract::Rent::getInfos($tabViewData{'countryId'}, $sheetObj->{'contract.id'});
    my $custObj  = &LOC::Customer::getInfos($tabViewData{'countryId'}, $contrObj->{'customer.id'});
    my $machObj  = &LOC::Machine::getInfos($tabViewData{'countryId'}, $sheetObj->{'machine.id'}, LOC::Machine::GETINFOS_BASE, {'level' => LOC::Machine::LEVEL_MACHINESFAMILY});
    my $familObj = &LOC::MachinesFamily::getInfos($tabViewData{'countryId'}, $machObj->{'machinesFamily.id'});
    my $siteObj  = &LOC::Site::getInfos($tabViewData{'countryId'}, $contrObj->{'site.id'});
    my %intObj   = &LOC::User::getInfos($estimObj->{'interlocutor'});

    our $var;
    $var = {
        'agencyId'          => $estimObj->{'agency.id'},
        'code'              => $estimObj->{'code'},
        'date'              => $estimObj->{'dateCreation'},
        'contract'          => $contrObj->{'code'},
        'beginDate'         => $contrObj->{'beginDate'},
        'endDate'           => $contrObj->{'endDate'},
        'order'             => $estimObj->{'orderCode'},
        'site'              => $siteObj->{'label'} . ' ' . $siteObj->{'locality.label'},
        'contact'           => $siteObj->{'contact.fullName'},
        'telephone'         => $siteObj->{'contact.telephone'},
        'family'            => $familObj->{'label'} . ' ' . $familObj->{'energy'} . ' ' .$familObj->{'elevation'},
        'model'             => $machObj->{'model.label'},
        'number'            => $machObj->{'parkNumber'},
        'customer'          => {
            'label'     => $custObj->{'name'},
            'address1'  => $custObj->{'address1'},
            'address2'  => $custObj->{'address2'},
            'telephone' => $custObj->{'telephone'},
            'fax'       => $custObj->{'fax'},
            'locality'  => $custObj->{'locality.postalCode'} . ' ' . $custObj->{'locality.name'},
        },
        'description'       => $estimObj->{'object'},
        'comment'           => $estimObj->{'comments'},
        'globalReduc'       => ($estimObj->{'reductionPercent'} / 100),
        'tva'               => $estimObj->{'vatPercent'} / 100,
        'total'             => $estimObj->{'totalAmount'},
        'globalReducAmount' => $estimObj->{'reduction'},
        'reducedTotal'      => $estimObj->{'total'},
        'hasLineReduction'  => 0
    };


    # Suppression des retours à la ligne dans la description
    $var->{'description'} =~ s/\n/ /g;

    #Affichage de l'interlocuteur si celui ci est renseigné
    $var->{'interlocutor'} = (defined $estimObj->{'interlocutor'} && $estimObj->{'interlocutor'} != 0 ? $intObj{'firstName'} . ' ' . $intObj{'name'} : '');



    #Montant de la tva
    $var->{'amountWithTVA'} = $var->{'tva'} * $estimObj->{'total'};
    #Montant TTC
    $var->{'totalAmount'} =  (1+$var->{'tva'}) * $estimObj->{'total'};

    my @lines;

    my $tabLines = &LOC::Repair::Estimate::getLines($tabViewData{'countryId'}, $tabViewData{'id'});
    while (my ($id, $line) = each(%$tabLines))
    {
        if ($line->{'isToInvoice'})
        {
            my $invcLine = {};

            $invcLine->{'type'}             = $line->{'type'};
            $invcLine->{'label'}            = $line->{'label'};
            $invcLine->{'quantity'}         = $line->{'quantity'};
            $invcLine->{'unitPrice'}        = $line->{'unitAmount'};
            $invcLine->{'reduction'}        = ($line->{'reductionPercent'} / 100);
            $invcLine->{'reducedUnitPrice'} = $line->{'unitAmount'} * (1 - $line->{'reductionPercent'} / 100);
            $invcLine->{'amount'}           = $line->{'finalAmount'};

            push(@lines, $invcLine);

            if ($invcLine->{'reduction'} > 0)
            {
                $var->{'hasLineReduction'} = 1;
            }
        }
    }

    my $tabServices = &LOC::Repair::Estimate::getOtherServices($tabViewData{'countryId'}, $tabViewData{'id'});
    while (my ($id, $srv) = each(%$tabServices))
    {
        my $invcLine = {};

        if ($srv->{'service.id'})
        {
            my $otherService = &LOC::Repair::OtherService::getInfos($tabViewData{'countryId'}, $srv->{'service.id'});
            $invcLine->{'label'} = $otherService->{'name'};

            $invcLine->{'quantity'}         = $srv->{'quantity'};
            $invcLine->{'unitPrice'}        = $srv->{'unitAmount'};
            $invcLine->{'reduction'}        = ($srv->{'reductionPercent'} / 100);
            $invcLine->{'reducedUnitPrice'} = $srv->{'unitAmount'} * (1 - $srv->{'reductionPercent'} / 100);
            $invcLine->{'amount'}           = $srv->{'finalAmount'};

            if ($invcLine->{'reduction'} > 0)
            {
                $var->{'hasLineReduction'} = 1;
            }

            push(@lines, $invcLine);

        }
        else
        {
            #On affiche uniquement si Qté <> 0 et qu'il faut le facturer
            if (($srv->{'type'} eq LOC::Repair::LineType::TYPE_LABOUR ||
                 $srv->{'type'} eq LOC::Repair::LineType::TYPE_TRAVELTIME ||
                 $srv->{'type'} eq LOC::Repair::LineType::TYPE_DISPLACEMENT ||
                 $srv->{'type'} eq LOC::Repair::LineType::TYPE_RESIDUES) &&
                $srv->{'quantity'} != 0 && $srv->{'isToInvoice'})
            {

                $invcLine->{'type'}             = $srv->{'type'};
                $invcLine->{'quantity'}         = $srv->{'quantity'};
                $invcLine->{'unitPrice'}        = $srv->{'unitAmount'};
                $invcLine->{'reduction'}        = ($srv->{'reductionPercent'} / 100);
                $invcLine->{'reducedUnitPrice'} = $srv->{'unitAmount'} * (1 - $srv->{'reductionPercent'} / 100);
                $invcLine->{'amount'}           = $srv->{'finalAmount'};

                if ($invcLine->{'reduction'} > 0)
                {
                    $var->{'hasLineReduction'} = 1;
                }

                push(@lines, $invcLine);

           }

        }#fin du else

    }

    $var->{'lines'} = \@lines;

    my $directory = &dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;

    require $directory . '/view/estimate/pdf.cgi';
}

1;