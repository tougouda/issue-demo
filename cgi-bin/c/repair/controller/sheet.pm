# Package: sheet
# Contrôleur Remise en état
package sheet;


# Constants: Paramètres d'affichage
# DISPLAY_NBCONTRACTS - Nombre de contrats à afficher dans la recherche sur une fiche de remise en état
# NBSEARCHSTEP - Nombre de FRE à afficher dans une recherche et lors du clic sur "Voir plus"
use constant
{
    DISPLAY_NBCONTRACTS => 10,
    NBSEARCHSTEP        => 100
};


use strict;
use File::Basename;
use File::Path;

use LOC::Globals;
use LOC::Agency;
use LOC::Model;
use LOC::User;
use LOC::Machine;
use LOC::Family;
use LOC::Characteristic;
use LOC::Repair::Service;
use LOC::Repair::Checkpoint;
use LOC::Site;
use LOC::Contract;
use LOC::Contract::Rent;
use LOC::Repair::Sheet;
use LOC::Repair::Estimate;
use LOC::Repair::Article;
use LOC::Request;
use LOC::Html::Standard;
use LOC::Util;
use LOC::Json;
use LOC::Date;
use POSIX qw(strftime);
use Image::Resize;
use File::PathInfo;
use File::Copy;

#Variables globale montant paramétrage
our $srvMrgCode = 'PRESTEXTMRG';
our $sheetMaxAmountCode = 'MTMAXFRM';

# Function: editAction
# Affichage de la fiche de remise en état
sub editAction
{
    my $tabParameters = $_[0];

    #Récupération des infos user
    my $tabUserInfos    = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
                        'localeId'       => $tabUserInfos->{'locale.id'},
                        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
                        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
                        'currencyId'     => $tabUserInfos->{'currency.id'},
                       );

    # Tableau des états des contrats à prendre en compte dans la recherche des 10 derniers contrats
    my $tabStates = [
                     LOC::Contract::Rent::STATE_PRECONTRACT,
                     LOC::Contract::Rent::STATE_CONTRACT,
                     LOC::Contract::Rent::STATE_STOPPED,
                     LOC::Contract::Rent::STATE_BILLED
                    ];

    # Caractéristiques
    #Coût de la main d'oeuvre
    my $labHourCst = &LOC::Characteristic::getAgencyValueByCode('MOECT', $tabViewData{'agencyId'});
    # Coût du temps de trajet
    my $tvtHourCst = &LOC::Characteristic::getAgencyValueByCode('TPSDEPLCT', $tabViewData{'agencyId'});
    #Coût unitaire du Déplacement (km)
    my $dspUntCst  = &LOC::Characteristic::getAgencyValueByCode('DEPLCT', $tabViewData{'agencyId'});
    #Montant de la main d'oeuvre
    my $labHourAmount = &LOC::Characteristic::getAgencyValueByCode('MOE', $tabViewData{'agencyId'});
    #Montant du temps de trajet
    my $tvtHourAmount = &LOC::Characteristic::getAgencyValueByCode('TSPDEPLMT', $tabViewData{'agencyId'});
    #Montant du Déplacement (km)
    my $dspUntAmount  = &LOC::Characteristic::getAgencyValueByCode('DEPL', $tabViewData{'agencyId'});

    #Récuperation de la marge pour les services externes (caractéristique)
    $tabViewData{'extSrvMargin'} = &LOC::Characteristic::getAgencyValueByCode($srvMrgCode, $tabViewData{'agencyId'});
    $tabViewData{'sheetMaxAmount'} = &LOC::Characteristic::getAgencyValueByCode($sheetMaxAmountCode, $tabViewData{'agencyId'});

    $tabViewData{'images'}      = {};
    $tabViewData{'services'}    = {};

    # Récupération des variables de formulaire
    #Id de la fiche courante
    $tabViewData{'sheetId'}         = &LOC::Request::getInteger('sheetId', 0);
    # Agence
    $tabViewData{'agencyId'}         = &LOC::Request::getString('agencyId', $tabUserInfos->{'nomadAgency.id'});
    # Pays
    $tabViewData{'countryId'}         = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    #Id de la famille de la machine sélectionnée
    $tabViewData{'familyId'}        = &LOC::Request::getInteger('familyId', 0);
    #Id de la machine sélectionnée
    $tabViewData{'machineId'}       = &LOC::Request::getInteger('machineId', 0);
    #Flag proforma du contrat associé
    $tabViewData{'proformaFlags'}   = &LOC::Request::getInteger('proformaFlags', 0);
    # Id du créateur de la FRE (Effectué par)
    $tabViewData{'sheetUsrId'}      = &LOC::Request::getInteger('sheetUsrId', 0);
    # Id de l'interlocuteur pour la FRE (et le devis)
    $tabViewData{'sheetInterlocutorId'} = &LOC::Request::getInteger('sheetInterlocutorId', 0);
    #Date affichée dans le champ "Le"
    $tabViewData{'sheetDate'}       = &LOC::Request::getDate('sheetDate', strftime("%Y-%m-%d", localtime));
    #Hachage contenant toutes les prestations techniques
    $tabViewData{'srvAdded'}        = &LOC::Request::getHash('service', {});
    # Heures et minutes saisies pour la main d'oeuvre sur la FRE
    $tabViewData{'labHour'}         = &LOC::Request::getInteger('labHour', 0);
    $tabViewData{'labMinutes'}      = &LOC::Request::getInteger('labMinutes', 0);
    # Heures et minutes saisies pour le temps de trajet sur la FRE
    $tabViewData{'tvtHour'}         = &LOC::Request::getInteger('tvtHour', 0);
    $tabViewData{'tvtMinutes'}      = &LOC::Request::getInteger('tvtMinutes', 0);
    #Déplacment saisi
    $tabViewData{'dspKm'}           = &LOC::Request::getInteger('dspKm', 0);
    $tabViewData{'imgTab'}          = &LOC::Request::getHash('image', {});
    $tabViewData{'contractId'}      = &LOC::Request::getInteger('contractId', 0);
    $tabViewData{'comment'}         = &LOC::Request::getString('sheetComment');
    #Coût de la MO affiché sur la FRE
    $tabViewData{'labCst'}          = &LOC::Request::getInteger('labCst', 0);
    #Coût du temps de trajet affiché sur la FRE
    $tabViewData{'tvtCst'}          = &LOC::Request::getInteger('tvtCst', 0);
    #Coût du déplacement affiché sur la FRE
    $tabViewData{'dspCst'}          = &LOC::Request::getInteger('dspCst', 0);
    #Montant de la MO affiché sur la FRE (coût + marge)
    $tabViewData{'labAmount'}       = &LOC::Request::getInteger('labAmount', 0);
    #Montant du temps de trajet affiché sur la FRE (coût + marge)
    $tabViewData{'tvtAmount'}       = &LOC::Request::getInteger('tvtAmount', 0);
    #Montant du déplacement affiché sur la FRE (coût + marge)
    $tabViewData{'dspAmount'}       = &LOC::Request::getInteger('dspAmount', 0);
    #Coût total récupéré de la FRE
    $tabViewData{'sheetCst'}        = &LOC::Request::getInteger('sheetCst');
    #Montant total récupéré de la FRE
    $tabViewData{'sheetAmt'}        = &LOC::Request::getInteger('sheetAmt');

    # Anciens points de vérification
    $tabViewData{'tabOldCheckpoints'} = &LOC::Json::fromJson(&LOC::Request::getString('tabOldCheckpoints', '[]'));
    # Références
    $tabViewData{'tabReferences'} = &LOC::Json::fromJson(&LOC::Request::getString('tabReferences', '[]'));

    #Coût unitaire de la MO (récupéré sur la FRE)
    $tabViewData{'labHourCst'}      = &LOC::Request::getInteger('labTimeCst', $labHourCst);
    #Coût unitaire du temps de trajet (récupéré sur la FRE)
    $tabViewData{'tvtHourCst'}      = &LOC::Request::getInteger('tvtHourCst', $tvtHourCst);
    #Coût unitaire du déplacement (récupéré sur la FRE)
    $tabViewData{'dspUntCst'}       = &LOC::Request::getInteger('dspKmCst', $dspUntCst);
    #Montant de la MO (récupéré sur la FRE)
    $tabViewData{'labHourAmount'}   = &LOC::Request::getInteger('labTimeAmount', $labHourAmount);
    #Montant du temps de trajet (récupéré sur la FRE)
    $tabViewData{'tvtHourAmount'}   = &LOC::Request::getInteger('tvtHourAmount', $tvtHourAmount);
    #Montant du déplacement (récupéré sur la FRE)
    $tabViewData{'dspUntAmount'}    = &LOC::Request::getInteger('dspKmAmount', $dspUntAmount);
    $tabViewData{'valid'}           = &LOC::Request::getString('valid', '');
    $tabViewData{'stateId'}         = &LOC::Request::getString('stateId', '');
    #Variable gérant la bascule ("ESTIMATE", "DAYS", "SERVICE")
    $tabViewData{'turnIn'}          = &LOC::Request::getString('turnIn', '');
    #Par défault la fiche et modifiable
    $tabViewData{'isMachineModifiable'} = 1;

    # Vérification que l'enregistrement se fait sur le bon CONTRATAUTO
    if ($tabViewData{'contractId'} != 0)
    {
        my $activeLine = &LOC::Contract::Rent::getActiveLineId($tabViewData{'countryId'}, $tabViewData{'contractId'});
        if ($tabViewData{'contractId'} != $activeLine)
        {
            $tabViewData{'contractId'} = $activeLine;
        }
    }

    #Valeur envoyée depuis le DRE si bascule sur contrat (on revient sur la FRE si on bascule sur contrat depuis le DRE)
    if($tabViewData{'valid'} eq '')
    {
        $tabViewData{'toTurnInto'}      = &LOC::Request::getString('toTurnInto', '');
    }

    # Bascule en Services depuis le DRE
    if ($tabViewData{'toTurnInto'} eq LOC::Repair::Sheet::TURNIN_SERVICES)
    {
        $tabViewData{'turnIn'} = $tabViewData{'toTurnInto'};
        $tabViewData{'valid'} = 'saved';
    }
    # Réactivation de la FRE depuis l'abandon d'un DRE
    if ($tabViewData{'toTurnInto'} eq 'reactivate')
    {
        $tabViewData{'valid'} = 'reactivate';
    }

    # Suppression des \r
    $tabViewData{'comment'} =~ s/\r//g;
    #tableau d'erreur actions
    my @errorsTab;
    # Gestion de l'enregistrement
    if ($tabViewData{'valid'} ne '' && $tabViewData{'valid'} ne 'changeFamily' && $tabViewData{'valid'} ne 'reactivate' && $tabViewData{'valid'} ne 'saved')
    {
        # Liste de tous les services existants
        $tabViewData{'srvList'} = &LOC::Repair::Service::getList($tabViewData{'countryId'},
                                                                 LOC::Util::GETLIST_ASSOC);

        # Liste des Checkpoints
        $tabViewData{'chpList'} = &LOC::Repair::Checkpoint::getList($tabViewData{'countryId'},
                                                            LOC::Util::GETLIST_ASSOC,
                                                            {'family' =>  $tabViewData{'familyId'}},
                                                            LOC::Repair::Checkpoint::GETINFOS_MARGIN);

        #Blocage erreur
        #créateur fiche incorrect
        if ($tabViewData{'sheetUsrId'} == 0)
        {
            $tabViewData{'valid'} = '';
            push(@errorsTab, 'requiredCreator');
        }

        #Date invalide
        if ($tabViewData{'sheetDate'} eq '')
        {
            $tabViewData{'valid'} = '';
            push(@errorsTab, 'requiredDate');
            $tabViewData{'sheetDate'} = strftime("%Y-%m-%d", localtime);
        }

        ## Vérification cout et montant si la FRE n'est pas déjà basculée ou abandonnée
        if ($tabViewData{'turnIn'} ne LOC::Repair::Sheet::TURNIN_DAYS && $tabViewData{'stateId'} ne LOC::Repair::Sheet::STATE_TURNEDIN && $tabViewData{'stateId'} ne LOC::Repair::Sheet::STATE_ABORTED && $tabViewData{'stateId'} ne LOC::Repair::Sheet::STATE_CANCELED)
        {
            # Verification cout total et du montant avec PERL
            my $totalCost = $tabViewData{'dspCst'} + $tabViewData{'labCst'} + $tabViewData{'tvtCst'};
            my $totalAmount = $tabViewData{'dspAmount'} + $tabViewData{'labAmount'} + $tabViewData{'tvtAmount'};
            $totalAmount = &LOC::Util::round($totalAmount, 2);
            $totalCost = &LOC::Util::round($totalCost, 2);

            # Boucle sur les anciens points de vérification
            foreach my $oldCheckpointInfos (@{$tabViewData{'tabOldCheckpoints'}})
            {
                my $chpId = $oldCheckpointInfos->{'checkpoint.id'};
                if ($oldCheckpointInfos->{'state'} ne 'removed')
                {
                    if ($oldCheckpointInfos->{'repairType'} eq LOC::Repair::LineType::TYPE_FIX)
                    {
                        # Cas où le paramétrage a été modifié
                        if ($tabViewData{'chpList'}->{$chpId}->{'fixAllowed'} == 0)
                        {
                            $totalCost   += $oldCheckpointInfos->{'cost'};
                            $totalAmount += $oldCheckpointInfos->{'cost'};
                        }
                        else
                        {
                            $totalCost += $tabViewData{'chpList'}->{$chpId}->{'fixCost'};
                            $totalAmount += $tabViewData{'chpList'}->{$chpId}->{'fixCost'};
                        }
                    }
                    elsif ($oldCheckpointInfos->{'repairType'} eq LOC::Repair::LineType::TYPE_REPAIR)
                    {
                        # Cas où le paramétrage a été modifié
                        if ($tabViewData{'chpList'}->{$chpId}->{'repairAllowed'} == 0)
                        {
                            $totalCost   += $oldCheckpointInfos->{'cost'};
                            $totalAmount += $oldCheckpointInfos->{'cost'};
                        }
                        else
                        {
                            $totalCost += $tabViewData{'chpList'}->{$chpId}->{'repairCost'};
                            $totalAmount += $tabViewData{'chpList'}->{$chpId}->{'repairCost'};
                        }
                    }
                    $totalAmount = &LOC::Util::round($totalAmount, 2);
                    $totalCost = &LOC::Util::round($totalCost, 2);
                }
            }
            
            # Boucle sur les références de la fiche en cours
            foreach my $referenceInfos (@{$tabViewData{'tabReferences'}})
            {
                my $articleCost;
                my $articleMargin;
                my $isMarginApplied = 0;

                # Cas saisie kimoce obligatoire
                if ($referenceInfos->{'artCode'} eq '')
                {
                    $tabViewData{'valid'} = '';
                    push(@errorsTab, 'requiredKimoceCode');
                }
                elsif ($referenceInfos->{'state'} ne 'removed')
                {
                    #vérification Kimoce
                    my $validCode = &LOC::Repair::Article::getInfosFromKimoce($tabViewData{'countryId'},
                                                                              {'code' => $referenceInfos->{'artCode'}});

                    if (!defined $validCode)
                    {
                        $tabViewData{'valid'} = '';
                        push(@errorsTab, ['unknownKimoceCode', {'code' => $referenceInfos->{'artCode'}}]);
                    }
                    else
                    {
                        $articleCost = $validCode->{'localCost'};
                        $articleMargin = &LOC::Repair::ProfitMargin::getAmountByCost($tabViewData{'countryId'},
                                                                                     $articleCost);
                    }
                }

                $totalCost += (&LOC::Util::round($articleCost, 2) * $referenceInfos->{'quantity'});
                $totalAmount += (&LOC::Util::round($articleCost, 2) * $referenceInfos->{'quantity'});

                # Si changement de pièce est margé
                if ($isMarginApplied)
                {
                    $totalAmount += (&LOC::Util::round($articleMargin, 2) * $referenceInfos->{'quantity'});
                }

            }
            $totalAmount = &LOC::Util::round($totalAmount, 2);
            $totalCost = &LOC::Util::round($totalCost, 2);


            # Tableau contenant les clés du tableau de la liste des services
            my @tabKeys = keys %{$tabViewData{'srvList'}};
            # Boucle sur les services
            foreach my $lineId (keys %{$tabViewData{'srvAdded'}})
            {
                my $srvId = $tabViewData{'srvAdded'}->{$lineId}->{'id'};
                $tabViewData{'srvAdded'}->{$lineId}->{'currency.id'} = $tabViewData{'currencyId'};
                my $cost;
                my $amount;

                #service interne
                if ($tabViewData{'srvAdded'}->{$lineId}->{'type'} eq LOC::Repair::LineType::TYPE_INTERNALSERVICE)
                {
                    # Si le service a été supprimé du paramétrage
                    if (!&LOC::Util::in_array($tabViewData{'srvAdded'}->{$lineId}->{'id'}, \@tabKeys))
                    {
                        if ($tabViewData{'srvAdded'}->{$lineId}->{'qty'} ne '')
                        {
                            $cost   = $tabViewData{'srvAdded'}->{$lineId}->{'qty'} * &LOC::Util::round($tabViewData{'srvAdded'}->{$lineId}->{'unitCst'}, 2);
                            $amount = &LOC::Util::round($tabViewData{'srvAdded'}->{$lineId}->{'amount'}, 2);
                        }
                        else
                        {
                            $cost   = &LOC::Util::round($tabViewData{'srvAdded'}->{$lineId}->{'unitCst'}, 2);
                            $amount = &LOC::Util::round($tabViewData{'srvAdded'}->{$lineId}->{'amount'}, 2);
                        }
                    }
                    else
                    {
                        #Si le service a une quantité
                        if ($tabViewData{'srvList'}->{$srvId}->{'isQuantityAllowed'} == 1)
                        {
                            $cost = $tabViewData{'srvAdded'}->{$lineId}->{'qty'} * &LOC::Util::round($tabViewData{'srvList'}->{$srvId}->{'cost'}, 2);
                            $amount = $tabViewData{'srvAdded'}->{$lineId}->{'qty'} * &LOC::Util::round($tabViewData{'srvList'}->{$srvId}->{'amount'}, 2);
                        }
                        else
                        {
                            $cost   = $tabViewData{'srvList'}->{$srvId}->{'cost'};
                            $amount = $tabViewData{'srvList'}->{$srvId}->{'amount'};
                        }
                    }
                }
                # Interne modifiable
                elsif ($tabViewData{'srvAdded'}->{$lineId}->{'type'} eq 'INM')
                {
                    if ($tabViewData{'srvList'}->{$srvId}->{'isQuantityAllowed'} == 1)
                    {
                        $cost   = $tabViewData{'srvAdded'}->{$lineId}->{'qty'} * &LOC::Util::round($tabViewData{'srvAdded'}->{$lineId}->{'unitCst'}, 2);
                        $amount = $tabViewData{'srvAdded'}->{$lineId}->{'qty'} * &LOC::Util::round($tabViewData{'srvAdded'}->{$lineId}->{'unitCst'}, 2);
                    }
                    else
                    {
                        $cost   = $tabViewData{'srvAdded'}->{$lineId}->{'cost'};
                        $amount = $tabViewData{'srvAdded'}->{$lineId}->{'cost'};
                    }
                }
                # Externe
                else
                {
                    # Si le service a été supprimé du paramétrage
                    if (!&LOC::Util::in_array($tabViewData{'srvAdded'}->{$lineId}->{'id'}, \@tabKeys))
                    {
                        $cost   = &LOC::Util::round($tabViewData{'srvAdded'}->{$lineId}->{'unitCst'}, 2);
                        if (defined $tabViewData{'srvAdded'}->{$lineId}->{'isSaved'})
                        {
                            $amount = &LOC::Util::round($tabViewData{'srvAdded'}->{$lineId}->{'unitAmt'}, 2);
                        }
                        # Cas où on supprime la prestation technique
                        else
                        {
                            $amount = 0;
                        }
                    }
                    else
                    {
                        $cost = $tabViewData{'srvAdded'}->{$lineId}->{'cost'};
                        $amount = $tabViewData{'srvAdded'}->{$lineId}->{'cost'};
                        #Si marge doit être appliquée
                        if ($tabViewData{'srvList'}->{$srvId}->{'isMarginAllowed'})
                        {
                            $amount += $tabViewData{'srvAdded'}->{$lineId}->{'cost'} *  $tabViewData{'extSrvMargin'};
                        }
                    }
                }

                $amount = &LOC::Util::round($amount, 2);
                $cost   = &LOC::Util::round($cost, 2);

                $totalCost += $cost;
                $totalAmount += $amount;

            }

            $totalAmount = &LOC::Util::toNumber($totalAmount);
            $totalCost = &LOC::Util::toNumber($totalCost);
            $tabViewData{'sheetAmt'} = &LOC::Util::toNumber($tabViewData{'sheetAmt'});
            $tabViewData{'sheetCst'} = &LOC::Util::toNumber($tabViewData{'sheetCst'});

            # Si erreurs cohérence
            # Vérifications des valeurs (coût total|Montant) JS/PERL
            if (&LOC::Util::round($totalCost, 2) != &LOC::Util::round($tabViewData{'sheetCst'}, 2) ||
                &LOC::Util::round($totalAmount, 2) != &LOC::Util::round($tabViewData{'sheetAmt'}, 2))
            {
                $tabViewData{'valid'} = '';
                push(@errorsTab, 'invalidCostAmount');
            }
        }
        elsif ($tabViewData{'turnIn'} eq LOC::Repair::Sheet::TURNIN_DAYS)
        {
            # Boucle sur les services
            foreach my $lineId (keys %{$tabViewData{'srvAdded'}})
            {
                $tabViewData{'srvAdded'}->{$lineId}->{'currency.id'} = $tabViewData{'currencyId'};
            }
        }

        #Vérification date
        if ($tabViewData{'sheetDate'} gt strftime("%Y-%m-%d", localtime))
        {
             $tabViewData{'valid'} = '';
             push(@errorsTab, 'invalidDate');
        }

        #Enregistrement : préparation des données
        # Hachage contenant ttes les données correctes à maj
        my $sheetDatas = {
            'id'                => $tabViewData{'sheetId'},
            'agency.id'         => $tabViewData{'agencyId'},
            'machine.id'        => $tabViewData{'machineId'},
            'contract.id'       => $tabViewData{'contractId'},
            'currency.id'       => $tabViewData{'currencyId'},
            'creator.id'        => $tabViewData{'sheetUsrId'},
            'interlocutor.id'   => $tabViewData{'sheetInterlocutorId'},
            'date'              => $tabViewData{'sheetDate'},
            'labTime'           => $tabViewData{'labHour'} . ':' . $tabViewData{'labMinutes'},
            'labUntCst'         => $tabViewData{'labHourCst'},
            'labCst'            => $tabViewData{'labCst'},
            'labUntAmt'         => $tabViewData{'labHourAmount'},
            'labAmt'            => $tabViewData{'labAmount'},
            'tvTime'            => $tabViewData{'tvtHour'} . ':' . $tabViewData{'tvtMinutes'},
            'tvtUntCst'         => $tabViewData{'tvtHourCst'},
            'tvtCst'            => $tabViewData{'tvtCst'},
            'tvtUntAmt'         => $tabViewData{'tvtHourAmount'},
            'tvtAmt'            => $tabViewData{'tvtAmount'},
            'dspKm'             => $tabViewData{'dspKm'},
            'dspUntCst'         => $tabViewData{'dspUntCst'},
            'dspCst'            => $tabViewData{'dspCst'},
            'dspUntAmt'         => $tabViewData{'dspUntAmount'},
            'dspAmt'            => $tabViewData{'dspAmount'},
            'sheetCst'          => $tabViewData{'sheetCst'},
            'sheetAmt'          => $tabViewData{'sheetAmt'},
            'state.id'          => $tabViewData{'stateId'},
            'sheetComment'      => $tabViewData{'comment'}
        };

        # Enregistrement
        my $saveResult = &LOC::Repair::Sheet::save($tabViewData{'countryId'},
                                                $tabUserInfos->{'id'},
                                                $sheetDatas,
                                                $tabViewData{'srvAdded'},
                                                $tabViewData{'extSrvMargin'},
                                                $tabViewData{'tabOldCheckpoints'},
                                                $tabViewData{'tabReferences'},
                                                ($tabViewData{'turnIn'} ne '' || $tabViewData{'valid'} eq 'abort'),
                                                \@errorsTab);

        # Création du repertoire pour stocker les photos
        my $count = scalar keys %{$tabViewData{'imgTab'}};
        if ($count >= 1 && $saveResult != 0)
        {
            my $dirPhotos = LOC::Repair::Sheet::PHOTOS_PATH . $tabViewData{'countryId'} . '/' . $saveResult;
            &File::Path::make_path($dirPhotos);

            my $dirTmp = LOC::Repair::Sheet::PHOTOS_TEMP;

            # Enregistrement des photos
            # valeur de saved :
            # 0 : Non sauvegardée
            # 1 : sauvegardée
            # -1 : à supprimer
            foreach my $key (keys %{$tabViewData{'imgTab'}})
            {
                #Ajout de l'image
                if ($tabViewData{'imgTab'}->{$key}->{'saved'} == 0)
                {
                    my $fi = new File::PathInfo($dirTmp . $tabViewData{'imgTab'}->{$key}->{'name'});
                    my $fileNameThumb = 'thumb_' . $fi->filename_only() . '.png';
                    &File::Copy::move($dirTmp . $fileNameThumb,
                           $dirPhotos . '/' . $fileNameThumb);

                    &File::Copy::move($dirTmp . $tabViewData{'imgTab'}->{$key}->{'name'},
                           $dirPhotos . '/' . $tabViewData{'imgTab'}->{$key}->{'name'});
                }
                #Suppression de l'image
                if ($tabViewData{'imgTab'}->{$key}->{'saved'} == -1)
                {
                    unlink ($dirPhotos . '/' . $tabViewData{'imgTab'}->{$key}->{'name'});
                }
            }
        }
        #Remise à zéro du hachage des services courants de la FRE
        $tabViewData{'srvAdded'} = {};


        ### ACTIONS ###

        # Abandon de la fiche de remise en état
        if ($tabViewData{'valid'} eq 'abort' && $saveResult != 0)
        {

            # Récupération du commentaire d'abandon saisi
            $tabViewData{'abortComment'}  = &LOC::Request::getString('abortCommentHidden');
            # Suppression des \r
            $tabViewData{'abortComment'} =~ s/\r//g;

            #Commentaire d'abandon non renseigné
            if ($tabViewData{'abortComment'} eq '')
            {
                $tabViewData{'valid'} = '';
                push(@errorsTab, 'requiredAbortComment');
            }
            else
            {
                my $result = &LOC::Repair::Sheet::abort($tabViewData{'countryId'},
                                                        $tabViewData{'agencyId'},
                                                        {'id' => $tabViewData{'sheetId'}, 'abortComment' => $tabViewData{'abortComment'}},
                                                        $tabUserInfos->{'id'});
                if ($result > 0)
                {
                    $tabViewData{'valid'} = 'aborted';
                }
                else
                {
                    $tabViewData{'valid'} = '';
                    push(@errorsTab, 'abortError');
                }
            }
        }
        #Annulation de la fiche
        elsif ($tabViewData{'valid'} eq 'cancel' && $saveResult != 0)
        {
            my $result = &LOC::Repair::Sheet::cancel($tabViewData{'countryId'},
                                                    $tabViewData{'agencyId'},
                                                    {'id' => $tabViewData{'sheetId'}},
                                                    $tabUserInfos->{'id'});
            if ($result > 0)
            {
                $tabViewData{'valid'} = 'canceled';
            }
            else
            {
                $tabViewData{'valid'} = '';
                push(@errorsTab, 'cancelError');
            }

        }

        # Enregistrement en base de données (Click sur Valider)
        elsif ($tabViewData{'valid'} eq 'ok')
        {
            # Faire une verif avec affichage des messages
            if ($tabViewData{'sheetId'} == 0 && $saveResult != 0)
            {
                $tabViewData{'valid'} = 'added';
                $tabViewData{'sheetId'} = $saveResult;
            }
            elsif ($saveResult != 0)
            {
                $tabViewData{'valid'} = 'saved';
            }
            elsif ($saveResult == 0)
            {
                $tabViewData{'valid'} = '';
            }
        }#fin du elseif

    }#fin du if

    # Réactivation de la fiche
    if ($tabViewData{'valid'} eq 'reactivate' && $tabViewData{'sheetId'} != 0)
    {
        my $result = &LOC::Repair::Sheet::reopen($tabViewData{'countryId'},
                                                    $tabViewData{'agencyId'},
                                                    {'id' => $tabViewData{'sheetId'}});
        if ($result > 0)
        {
            $tabViewData{'valid'} = 'reactivated';
        }
        else
        {
            $tabViewData{'valid'} = '';
            push(@errorsTab, 'reactivateError');
        }
    }

    #Si bien sauvegardé et si on applique une action
    if ($tabViewData{'valid'} eq 'saved')
    {

        #Bascule en devis
        if ($tabViewData{'turnIn'} eq 'estimate')
        {
            my $resultTurnIntoEstimate = &LOC::Repair::Sheet::turnIntoEstimate($tabViewData{'countryId'},
                                                                               $tabViewData{'sheetId'},
                                                                               $tabUserInfos->{'id'});
            if ($resultTurnIntoEstimate > 0)
            {

                $tabViewData{'valid'} = 'turnInEstimate';
            }
            else
            {
                $tabViewData{'valid'} = '';
                push(@errorsTab, 'turnInEstimateError');
            }
        }
        #Bascule en services (FRE et DRE)
        elsif ($tabViewData{'turnIn'} eq LOC::Repair::Sheet::TURNIN_SERVICES)
        {
            #bascule en services
            my $result = &LOC::Repair::Sheet::turnInto($tabViewData{'countryId'},
                                                       $tabViewData{'agencyId'},
                                                       $tabViewData{'sheetId'},
                                                       LOC::Repair::Sheet::TURNIN_SERVICES,
                                                       {},
                                                       $tabUserInfos->{'id'}
                                                      );

            if ($result != -1)
            {
                $tabViewData{'valid'} = 'turnInServices';
                $tabViewData{'toTurnInto'} = '';
            }
            else
            {
                $tabViewData{'valid'} = '';
                push(@errorsTab, 'turnInServicesError');
            }
        }
        elsif ($tabViewData{'turnIn'} eq LOC::Repair::Sheet::TURNIN_DAYS)
        {
            #Récupération des données formulaire concernant la bascule en jours
            #Prix unitaire de loc récupéré de la FRE
            $tabViewData{'unitDayAmt'}  = &LOC::Request::getInteger('unitDayAmtHidden');
            #Mtt de jours récupéré de la FRE
            $tabViewData{'daysNumber'}  = &LOC::Request::getInteger('daysNumberHidden');
            #Montant sur le contrat récupéré de la FRE
            $tabViewData{'contractAmt'} = &LOC::Request::getInteger('contractAmtHidden');
            $tabViewData{'contractAmt'} = &LOC::Util::toNumber($tabViewData{'contractAmt'});

            #Récupération du prix de location du contrat
            my $contractDatas = &LOC::Contract::Rent::getInfos($tabViewData{'countryId'}, $tabViewData{'contractId'});

            my $unitAmount;
            if ($contractDatas->{'amountType'} eq 'month')
            {
                $unitAmount = &LOC::Util::round($contractDatas->{'unitPrice'} / LOC::Date::AVERAGE_MONTH_WORKING_DAYS, 0);
            }
            elsif ($contractDatas->{'amountType'} eq 'fixed')
            {
                $unitAmount = &LOC::Util::round($contractDatas->{'unitPrice'} / $contractDatas->{'duration'},0);
            }
            else
            {
                $unitAmount = $contractDatas->{'unitPrice'};
            }

            #Montant qui sera basculé sur le contrat
            my $turnInDaysAmount = $tabViewData{'daysNumber'} * $unitAmount;
            $turnInDaysAmount = &LOC::Util::toNumber($turnInDaysAmount);

            if ($turnInDaysAmount == $tabViewData{'contractAmt'})
            {
                #Préparation du hash de bascule en jours
                my $daysValues = {
                    'contract.id'       => $tabViewData{'contractId'},
                    'repairsheet.id'    => $tabViewData{'sheetId'},
                    'currency.id'       => $tabViewData{'currencyId'},
                    'number'            => $tabViewData{'daysNumber'},
                    'unitAmount'        => $unitAmount,
                    'amount'            => $turnInDaysAmount,
                };
                my $result = &LOC::Repair::Sheet::turnInto($tabViewData{'countryId'},
                                                           $tabViewData{'agencyId'},
                                                           $tabViewData{'sheetId'},
                                                           LOC::Repair::Sheet::TURNIN_DAYS,
                                                           $daysValues,
                                                           $tabUserInfos->{'id'}
                                                          );

                if ($result != -1)
                {
                    $tabViewData{'valid'} = 'turnInDays';
                    $tabViewData{'toTurnInto'} = '';
                }
                else
                {
                    $tabViewData{'valid'} = '';
                    push(@errorsTab, 'turnInDaysError');
                }
            }
            else
            {
                $tabViewData{'valid'} = '';
                push(@errorsTab, 'turnInDaysContractError');
            }
        }
    }#fin du if si on a bien sauvegardé


    #Réinitialisation de la variable turnIn
    $tabViewData{'turnIn'} = '';





    # Chargement des données d'entête de la fiche de remise en état (depuis un rechargement)
    if ($tabViewData{'sheetId'} != 0 && $tabViewData{'valid'} ne 'changeFamily')
    {
        my $sheetInfos = &LOC::Repair::Sheet::getInfos($tabViewData{'countryId'}, $tabViewData{'sheetId'}, LOC::Repair::Sheet::GETINFOS_CUSTOMER);

        #Formatage du temps de travaux
        my @labourTime = split(':', $sheetInfos->{'labourTime'});
        my @travelTime = split(':', $sheetInfos->{'travelTime'});

        $tabViewData{'agencyId'}            = $sheetInfos->{'agency.id'};
        $tabViewData{'countryId'}           = $sheetInfos->{'country.id'};
        $tabViewData{'currency.id'}         = $sheetInfos->{'currency.id'};
        $tabViewData{'machineId'}           = $sheetInfos->{'machine.id'};
        $tabViewData{'sheetUsrId'}          = $sheetInfos->{'user.id'};
        $tabViewData{'sheetInterlocutorId'} = $sheetInfos->{'interlocutor.id'};
        $tabViewData{'sheetDate'}           = $sheetInfos->{'date'};
        $tabViewData{'labHour'}             = $labourTime[0];
        $tabViewData{'labMinutes'}          = $labourTime[1];
        $tabViewData{'tvtHour'}             = $travelTime[0];
        $tabViewData{'tvtMinutes'}          = $travelTime[1];
        $tabViewData{'dspKm'}               = $sheetInfos->{'displKm'};
        $tabViewData{'contractId'}          = $sheetInfos->{'contract.id'};
        $tabViewData{'comment'}             = $sheetInfos->{'sheetComment'};
        $tabViewData{'labCst'}              = $sheetInfos->{'labourCost'};
        $tabViewData{'tvtCst'}              = $sheetInfos->{'travelTimeCost'};
        $tabViewData{'dspCst'}              = $sheetInfos->{'displCost'};
        $tabViewData{'stateId'}             = $sheetInfos->{'state.id'};
        $tabViewData{'sheetCst'}            = $sheetInfos->{'sheetCost'};
        $tabViewData{'sheetAmt'}            = $sheetInfos->{'sheetAmount'};
        $tabViewData{'flag'}                = $sheetInfos->{'flag'};
        $tabViewData{'repairEstimateId'}    = $sheetInfos->{'repairEstimate.id'};
        $tabViewData{'codeRepairEstimate'}  = $sheetInfos->{'codeRepairEstimate'};
        $tabViewData{'abortComment'}        = $sheetInfos->{'abortComment'};
        $tabViewData{'isMachineModifiable'} = $sheetInfos->{'isMachineModifiable'};
        $tabViewData{'orderContact.email'}  = $sheetInfos->{'orderContact.email'};

        # récupération spéciale du coût et du montant pour le cas où la fiche est basculée ou abandonnée
        $tabViewData{'finalSheetCst'}       = "$tabViewData{'sheetCst'}";
        $tabViewData{'finalSheetAmt'}       = "$tabViewData{'sheetAmt'}";

        # Récupération des couts unitaires
        $tabViewData{'labHourCst'}      = $sheetInfos->{'labourUnitCost'};
        $tabViewData{'dspUntCst'}       = $sheetInfos->{'displUnitCost'};
        # dans le cas où les paramètres ont changé et que la fiche est toujours en attente
        if ($tabViewData{'stateId'} eq LOC::Repair::Sheet::STATE_WAITING && $tabViewData{'toTurnInto'} ne LOC::Repair::Sheet::TURNIN_DAYS)
        {
            $tabViewData{'labHourCst'} = $labHourCst;
            $tabViewData{'dspUntCst'} = $dspUntCst;
            $tabViewData{'labHourAmount'} = $labHourAmount;
            $tabViewData{'dspUntAmount'} = $dspUntAmount;
        }

        #Récuperation de la marge pour les services externes (caractéristique)
        $tabViewData{'extSrvMargin'} = &LOC::Characteristic::getAgencyValueByCode($srvMrgCode,
                                                                              $tabViewData{'agencyId'},
                                                                              $tabViewData{'sheetDate'});
        #Récuperation du montant de bascule max pour la fiche de remise
        $tabViewData{'sheetMaxAmount'} = &LOC::Characteristic::getAgencyValueByCode($sheetMaxAmountCode,
                                                                              $tabViewData{'agencyId'},
                                                                              $tabViewData{'sheetDate'});
    }

    # Chargement ou rechargement des lignes (dans le cas d'un chargement normal ou d'un changement de famille machine)
    if ($tabViewData{'sheetId'} != 0)
    {
        $tabViewData{'services'}        = &LOC::Repair::Sheet::getServices($tabViewData{'countryId'},
                                                                           $tabViewData{'sheetId'},
                                                                           LOC::Util::GETLIST_ASSOC,
                                                                           LOC::Repair::Sheet::SHEET_FORMAT);

        my $tabOldCheckpoints = &LOC::Repair::Sheet::getCheckPoints($tabViewData{'countryId'},
                                                                              $tabViewData{'sheetId'},
                                                                              LOC::Util::GETLIST_ASSOC,
                                                                              LOC::Repair::Sheet::SHEET_FORMAT |
                                                                              LOC::Repair::Sheet::GETCHECKPOINTINFOS_ALL,
                                                                              {'isReference' => 0});
        my @tabOldCheckpoints = values(%$tabOldCheckpoints);
        $tabViewData{'tabOldCheckpoints'} = \@tabOldCheckpoints;

        my $tabReferences = &LOC::Repair::Sheet::getCheckPoints($tabViewData{'countryId'},
                                                                              $tabViewData{'sheetId'},
                                                                              LOC::Util::GETLIST_ASSOC,
                                                                              0,
                                                                              {'isReference' => 1});
        my @tabReferences = values(%$tabReferences);
        $tabViewData{'tabReferences'} = \@tabReferences;

        # Récupération de la liste des photos attachées à la FRE
        my $tabPhotos = &LOC::Repair::Sheet::getPhotos($tabViewData{'countryId'}, $tabViewData{'sheetId'});
        $tabViewData{'images'} = [];
        foreach my $photo (@$tabPhotos)
        {
            push(@{$tabViewData{'images'}}, $photo->{'url'});
        }
    }

    #Todo recherche des info contrat
    $tabViewData{'tabMachineContracts'} = [];
    $tabViewData{'tabMachineContractList'} = [];
    $tabViewData{'tabMachineContract'} = [];

    if ($tabViewData{'machineId'}!=0)
    {
        # 10 derniers contrats de la machine
        $tabViewData{'tabMachineContractList'} = &_getMachineContracts($tabViewData{'countryId'}, $tabViewData{'agencyId'}, $tabViewData{'machineId'}, $tabStates, ($tabViewData{'isMachineModifiable'} ? undef : $tabViewData{'contractId'}));
        my $isContractExists = 0;
        # recherche si le contrat associé à la FRE est présent dans la liste des contrats
        foreach my $contractLine (@{$tabViewData{'tabMachineContractList'}})
        {
            if ($contractLine->{'id'} == $tabViewData{'contractId'})
            {
                $isContractExists = 1;
                last;
            }
        }
        # s'il n'est pas présent, on l'ajoute à la liste
        if ($isContractExists == 0)
        {
            $tabViewData{'tabMachineContract'} = &_getMachineContracts($tabViewData{'countryId'}, $tabViewData{'agencyId'}, undef, undef, $tabViewData{'contractId'});
            @{$tabViewData{'tabMachineContracts'}} = (@{$tabViewData{'tabMachineContract'}}, @{$tabViewData{'tabMachineContractList'}});
        }
        else
        {
            $tabViewData{'tabMachineContracts'} = $tabViewData{'tabMachineContractList'};
        }


        if (!$tabViewData{'tabMachineContracts'})
        {

            push(\@errorsTab, 'nocontract');
        }
    }

    # Liste des modèles de machines avec toutes les données
    my $tabMachinesList = &LOC::Repair::Sheet::getMachinesList(LOC::Util::GETLIST_ASSOC, $tabViewData{'countryId'}, $tabViewData{'agencyId'});


    # Liste  des Machines transférées dans une autre agence que celle en cours et sur lesquelles il y a eu des contrats sur les 6 derniers mois
    my $tabMachinesTransferedList = &LOC::Repair::Sheet::getTransferedMachinesList(LOC::Util::GETLIST_ASSOC, $tabViewData{'countryId'}, $tabViewData{'agencyId'});




    # Liste des machines
    tie(my %hashMachinesList, 'Tie::IxHash');

    #Formatage de la liste déroulante des Machines
    while (my($key, $tab) = each(%{$tabMachinesList}))
    {
       $hashMachinesList{$tab->{'id'}} = $tab->{'parkNumber'} . ' - ' . $tab->{'model.label'};
    }

    #Formatage de la liste déroulante des Machines transférées
    while (my($key, $tab) = each(%{$tabMachinesTransferedList}))
    {
       $hashMachinesList{$tab->{'id'}} = $tab->{'parkNumber'} . ' - ' . $tab->{'model.label'};
    }

    #Tri du tableau
    $tabViewData{'tabMachinesList'} = &LOC::Util::sortHashByValues(\%hashMachinesList);


    $tabViewData{'tabMachinesList'} = \%hashMachinesList;

    $tabViewData{'familyLabel'}     = '';

    # Si une machine est sélectionnée
    if ($tabViewData{'machineId'} != 0)
    {
        my $machineInfos = &LOC::Machine::getInfos($tabViewData{'countryId'}, $tabViewData{'machineId'}, LOC::Machine::GETINFOS_BASE, {'level' => LOC::Machine::LEVEL_FAMILY});

        $tabViewData{'familyId'} = $machineInfos->{'family.id'};
        my $familyInfos = &LOC::Family::getInfos($tabViewData{'familyId'}, $tabViewData{'localeId'});
        $tabViewData{'familyLabel'} = $familyInfos->{'fullName'};
    }


    # Liste des services
    $tabViewData{'srvList'} = &LOC::Repair::Service::getList($tabViewData{'countryId'}, LOC::Util::GETLIST_ASSOC);

    # Liste des users
    $tabViewData{'creatorsList'} = ();
    $tabViewData{'interlocutorsList'} = ();
    $tabViewData{'stateCreator'} = ();
    $tabViewData{'stateInterlocutor'} = ();
    if ($tabViewData{'sheetUsrId'} != 0)
    {
        my %tabCreatorInfos = &LOC::User::getInfos($tabViewData{'sheetUsrId'}, &LOC::User::GET_ALL, {'formatMode' => 1});
        # Récupération du nom complet du créateur de la FRE
        $tabViewData{'creatorsList'}->{$tabViewData{'sheetUsrId'}} = $tabCreatorInfos{'fullName'};
        # Récupération de l'état du créateur de la FRE (Actif ou supprimé)
        $tabViewData{'stateCreator'}->{$tabViewData{'sheetUsrId'}} = $tabCreatorInfos{'state.id'};
    }
    if ($tabViewData{'sheetInterlocutorId'} != 0)
    {
        my %tabInterlocutorInfos = &LOC::User::getInfos($tabViewData{'sheetInterlocutorId'}, &LOC::User::GET_ALL, {'formatMode' => 1});
        # Récupération du nom complet de l'interlocuteur de la FRE
        $tabViewData{'interlocutorsList'}->{$tabViewData{'sheetInterlocutorId'}} = $tabInterlocutorInfos{'fullName'};
        # Récupération de l'état de l'état de l'interlocuter de la FRE (Actif ou supprimé)
        $tabViewData{'stateInterlocutor'}->{$tabViewData{'sheetInterlocutorId'}} = $tabInterlocutorInfos{'state.id'};
    }

    #On récupere la liste des libellés
    $tabViewData{'tabStates'} = &LOC::Repair::Sheet::getStatesList($tabViewData{'countryId'});

    # Droits de modification
    $tabViewData{'tabRights'} = &LOC::Repair::Sheet::getRights($tabViewData{'countryId'}, $tabViewData{'agencyId'},
                                                               $tabViewData{'sheetId'}, $tabUserInfos->{'id'});


    # Message de confirmation
    if ($tabViewData{'valid'} eq 'added')
    {
        $tabViewData{'messages'} = {'valid' => 'La fiche de remise en état a été enregistrée'};
    }
    elsif ($tabViewData{'valid'} eq 'saved')
    {
        $tabViewData{'messages'} = {'valid' => 'Les modifications ont été enregistrées'};
    }
    elsif ($tabViewData{'valid'} eq 'aborted')
    {
        $tabViewData{'messages'} = {'valid' => 'La fiche a été abandonnée'};
    }
    elsif ($tabViewData{'valid'} eq 'canceled')
    {
        $tabViewData{'messages'} = {'valid' => 'La fiche a été annulée'};
    }
    elsif ($tabViewData{'valid'} eq 'reactivated')
    {
        $tabViewData{'messages'} = {'valid' => 'La fiche a été réactivée'};
    }
     elsif ($tabViewData{'valid'} eq 'turnInEstimate')
    {
        $tabViewData{'messages'} = {'valid' => 'La fiche a été basculée en devis de remise en état'};
    }
    elsif ($tabViewData{'valid'} eq 'turnInDays')
    {
        $tabViewData{'messages'} = {'valid' => 'La fiche a été basculée en jours sur le contrat'};
    }
    elsif ($tabViewData{'valid'} eq 'turnInServices')
    {
        $tabViewData{'messages'} = {'valid' => 'La fiche a été basculée en services sur le contrat'};
    }

    # Messages d'erreur
    $tabViewData{'messages'}->{'error'} = \@errorsTab;

    # Corps du mail par défaut
        $tabViewData{'emailContent'}       = &LOC::Characteristic::getAgencyValueByCode('MAILFRE', $tabViewData{'agencyId'});


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    $directory .= '/view/sheet/';
    $tabViewData{'scriptPath'} = $directory;

    # Affichage de la vue
    require $tabViewData{'scriptPath'} . 'edit.cgi';
}


# Function:jsonGetMachineInfosAction
# Récupération des information de la machine
sub jsonGetMachineInfosAction
{
    my $tabParameters = $_[0];

   # utilisateur
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    #récupération des infos passés en paramétre JSON
    my $machineId = &LOC::Request::getInteger('machineId');
    my $countryId = &LOC::Request::getString('countryId');
    my $agencyId = &LOC::Request::getString('agencyId');
    my $familyId = &LOC::Request::getInteger('familyId');

    # Tableau des états des contrats à prendre en compte dans la recherche des 10 derniers contrats
    my $tabStates = [
                     LOC::Contract::Rent::STATE_PRECONTRACT,
                     LOC::Contract::Rent::STATE_CONTRACT,
                     LOC::Contract::Rent::STATE_STOPPED,
                     LOC::Contract::Rent::STATE_BILLED
                    ];


    # Variables pour la vue
    our %tabViewData = (
                       'values' => {
                                    'machineId' => $machineId,
                                    'machineFamily' => '',
                                    'tabMachineContracts' => [],
                                    'errorMsg' => ''
                                    },
                       'errorCode' => ''
                        );

    #On récupére les informations sur la machine (famille)
    my $machineInfos = &LOC::Machine::getInfos($countryId, $machineId, LOC::Machine::GETINFOS_BASE, {'level' => LOC::Machine::LEVEL_FAMILY});

    if ($machineInfos)
    {
        my $tabContracts = &_getMachineContracts($countryId, $agencyId, $machineId, $tabStates);
        if (@$tabContracts > 0)
        {
            #On forme le hash de retour des données vers la fiche
            $tabViewData{'values'}->{'isFamilyChanged'}     = ($familyId != $machineInfos->{'family.id'} ? 1 : 0);
            $tabViewData{'values'}->{'tabMachineContracts'} = $tabContracts;
        }
        else
        {
            $tabViewData{'errorCode'} = 'nocontracts';
        }
    }
    else
    {
        $tabViewData{'errorCode'} = 'nomachine';
    }

    # chemin d'accès aux scripts de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;


    # affichage de la vue
    require $directory . '/view/sheet/jsonGetMachineInfos' . ($tabViewData{'errorCode'} ne '' ? 'Err' : '') . '.cgi';
}


#



# Function:uploadPhotoAction
# Iframe formulaire et photos
sub uploadPhotoAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
                        'localeId'       => $tabUserInfos->{'locale.id'}
                       );

    $tabViewData{'jsSrc'} = '';
    $tabViewData{'isModifiable'} = &LOC::Request::getInteger('isModifiable');

    #recuperation de la photo
    my $photoFile = &LOC::Request::getFile('photo');
    #Quand on charge une photo
    if ($photoFile->{'name'} ne '')
    {
        if ($photoFile->{'name'} ne './')
        {
            my $originalFileName = $photoFile->{'name'};

            # Liste des formats autorisés
            my @allowedTypes = [LOC::Request::FILE_IMG_JPG,
                                LOC::Request::FILE_IMG_PNG,
                                LOC::Request::FILE_IMG_GIF]; #LOC::Request::FILE_IMG_BMP

            #Taille de fichier maximale autorisée (2 MO)
            my $maxFileSize = 2 * 1048576;
            $maxFileSize = &LOC::Util::toNumber($maxFileSize);

            #Taille du fichier > à la taille Max autorisée
            if ($photoFile->{'size'} > $maxFileSize)
            {
                $tabViewData{'jsSrc'} = 'window.parent.uploadError(0, "' . $originalFileName . '");';
            }
            else
            {
                #Si le type de photo est accepté
                if (&LOC::Util::in_array($photoFile->{'type'}, @allowedTypes))
                {
                    #On prend la date pour générer un nom
                    my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime(time);

                    #On se positionne dans les repertoires
                    my $tmpFilesRep = LOC::Repair::Sheet::PHOTOS_TEMP;
                    $photoFile->{'name'} =~ s/\s//g;
                    my $tmpFileName = $sec . $min . $hour . $mday . $mon . $year . '_' . $photoFile->{'name'};

                    # Chemin relatif de ce répertoire
                    $tmpFilesRep =~ /\/htdocs(\/.+)/;

                    my $srvFileName = &LOC::Globals::get('commonFilesUrl') . '/temp/';

                    #On deplace la photo dans le dossier temporaire
                    if (&File::Copy::move($photoFile->{'tmp_name'}, $tmpFilesRep . $tmpFileName))
                    {
                        #Création de la miniature d'affichage
                        my $gdImage = new GD::Image($tmpFilesRep . $tmpFileName);
                        my $image = new Image::Resize($tmpFilesRep . $tmpFileName);
                        my $resizePhotoFile = $image->resize((48 * $gdImage->width)/$gdImage->height, 48);
                        
                        my $fi = new File::PathInfo($tmpFilesRep . $tmpFileName);
                        my $fileNameThumb = 'thumb_' . $fi->filename_only();
                        
                        open(PICT, '>' . $tmpFilesRep . $fileNameThumb . '.png');
                        binmode PICT, ":raw";
                        print PICT $resizePhotoFile->jpeg;
                        close(PICT);
                        $tabViewData{'jsSrc'} = 'window.parent.setUploadedImage("' . $srvFileName . '","' . $tmpFileName . '");';

                    }
                    else
                    {
                        $tabViewData{'jsSrc'} = 'window.parent.uploadError(1, "' . $originalFileName . '");';
                    }
                }
                else
                {
                    $tabViewData{'jsSrc'} = 'window.parent.uploadError(2, "' . $originalFileName . '");';
                }
            }
        }
        else
        {
            $tabViewData{'jsSrc'} = 'window.parent.uploadError(3);';
        }
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/sheet/uploadPhoto.cgi';
}

# Function: searchAction
# Recherche des Fiches de remise en état
sub searchAction
{

    my $user = &LOC::Session::getUserInfos();

    # Variables pour la vue
    our %tabViewData = (
        'user.agency.id' => $user->{'nomadAgency.id'},
        'offset'         => 0,
        'step'           => NBSEARCHSTEP
    );

    # hachage de l'utilisateur
    $tabViewData{'localeId'}  = $user->{'locale.id'};
    $tabViewData{'agencyId'}  = $user->{'nomadAgency.id'};
    $tabViewData{'countryId'} = $user->{'nomadCountry.id'};

    my $flags = LOC::Repair::Sheet::GETINFOS_MACHINE |
                LOC::Repair::Sheet::GETINFOS_CONTRACT |
                LOC::Repair::Sheet::GETINFOS_CUSTOMER;

    # Récupère la liste des états de casse
    $tabViewData{'tabStates'} = &LOC::Repair::Sheet::getStatesList($tabViewData{'countryId'});

    $tabViewData{'sheetId'}         = &LOC::Request::getString('repairSheet');
    $tabViewData{'rentContract'}    = &LOC::Request::getString('rentContract');
    $tabViewData{'parkNumber'}      = &LOC::Request::getString('parkNumber');
    $tabViewData{'startDate'}       = &LOC::Request::getString('startDate');
    $tabViewData{'endDate'}         = &LOC::Request::getString('endDate');
    $tabViewData{'repairSheet'}     = &LOC::Request::getString('repairSheet');
    $tabViewData{'customerId'}      = &LOC::Request::getInteger('customerId');
    $tabViewData{'state.id'}        = &LOC::Request::getString('state.id');
    $tabViewData{'search.user.id'}  = &LOC::Request::getInteger('search.user.id');
    $tabViewData{'isNational'}      = &LOC::Request::getBoolean('isNational');

    $tabViewData{'customers'} = ();
    if ($tabViewData{'customerId'} != 0)
    {
        $tabViewData{'customers'} = &LOC::Customer::getList($tabViewData{'countryId'}, LOC::Util::GETLIST_PAIRS,
                                                              {'id' => $tabViewData{'customerId'}});
    }
    $tabViewData{'users'} = ();
    if ($tabViewData{'search.user.id'} != 0)
    {
        $tabViewData{'users'} = &LOC::User::getList(LOC::Util::GETLIST_PAIRS,
                                                      {'id' => $tabViewData{'search.user.id'}},
                                                      LOC::User::GET_ACTIVE,
                                                      {'formatMode' => 1});
    }

    # Filtres
    my $filters = {};
    if (!$tabViewData{'isNational'})
    {
        my @tabAuthorizedAgenciesId = keys %{$user->{'tabAuthorizedAgencies'}};
        $filters->{'agencyId'} = \@tabAuthorizedAgenciesId;
    }
    if ($tabViewData{'sheetId'} ne '')
    {
        $filters->{'id'} = $tabViewData{'sheetId'};
    }
    if ($tabViewData{'rentContract'} ne '')
    {
        $filters->{'contractCode'} = $tabViewData{'rentContract'};
    }
    if ($tabViewData{'parkNumber'} ne '')
    {
        $filters->{'parkNumber'} = $tabViewData{'parkNumber'};
    }
    if ($tabViewData{'repairSheet'} ne '')
    {
        $filters->{'code'} = $tabViewData{'repairSheet'};
    }
    if ($tabViewData{'customerId'} != 0)
    {
        $filters->{'customerId'} = $tabViewData{'customerId'};
    }
    if ($tabViewData{'search.user.id'} != 0)
    {
        $filters->{'interlocutorId'} = $tabViewData{'search.user.id'};
    }
    if ($tabViewData{'state.id'} ne '')
    {
        $filters->{'stateId'} = $tabViewData{'state.id'};
    }
    if ($tabViewData{'startDate'} ne '' || $tabViewData{'endDate'} ne '')
    {
        my $dateInterval = {};
        if ($tabViewData{'startDate'} ne '')
        {
            $dateInterval->{'start'} = $tabViewData{'startDate'};
        }
        if ($tabViewData{'endDate'} ne '')
        {
            $dateInterval->{'end'} = $tabViewData{'endDate'};
        }
        $filters->{'dateInterval'} = $dateInterval;
    }

    # nombre de critères sélectionnés
    $tabViewData{'nbFilters'}  = keys %$filters;
    $tabViewData{'tabFilters'} = $filters;
    

    my $tabOptions = {
        'limit' => {
            'offset' => $tabViewData{'offset'},
            'count'  => $tabViewData{'step'},
            '&total' => 0
        }
    };
    
    my $tabSheets = &LOC::Repair::Sheet::getList($tabViewData{'countryId'}, LOC::Util::GETLIST_ASSOC,
                                                    $filters,
                                                    LOC::Repair::Sheet::GETINFOS_MACHINE |
                                                    LOC::Repair::Sheet::GETINFOS_CONTRACT |
                                                    LOC::Repair::Sheet::GETINFOS_CUSTOMER,
                                                    $tabOptions);

    my @tabList;

    foreach my $tabSheetInfos (values(%$tabSheets))
    {
        push(@tabList, {
            'id'               => $tabSheetInfos->{'id'},
            'customerName'     => $tabSheetInfos->{'customer.name'},
            'contractCode'     => $tabSheetInfos->{'contract.code'},
            'machineParkNumber'=> $tabSheetInfos->{'machine.parkNumber'},
            'stateId'          => $tabSheetInfos->{'state.id'},
            'flag'             => $tabSheetInfos->{'flag'},
            'url'              => &LOC::Request::createRequestUri('repair:sheet:edit', {'sheetId' => $tabSheetInfos->{'id'}})
        });
    }

    $tabViewData{'result'} = \@tabList;
    $tabViewData{'total'}  = $tabOptions->{'limit'}->{'&total'};


    if ($tabViewData{'total'} == 1)
    {
        my $url = &LOC::Request::createRequestUri('repair:sheet:edit', {'sheetId' => $tabList[0]->{'id'}});
        # Redirection
        &LOC::Browser::sendHeaders("Status: 302 Found\nLocation: " . $url . "\n\n");
    }



    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/sheet/search.cgi';
}



# Function: jsonSearchAction
# Recherche de fiche de remise en état
sub jsonSearchAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
    );

    my $countryId  = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $tabFilters = &LOC::Json::fromJson(&LOC::Request::getString('tabFilters', ''));
    $tabViewData{'offset'} = &LOC::Request::getInteger('offset', 0);
    $tabViewData{'step'}   = &LOC::Request::getInteger('step', 100);


    # nombre de critères sélectionnés
    $tabViewData{'nbFilters'} = keys %$tabFilters;
    $tabViewData{'tabFilters'}   = $tabFilters;

    # s'il y a assez de critères sélectionnés
    if ($tabViewData{'nbFilters'} > 0)
    {
        my $tabOptions = {
            'limit' => {
                'offset' => $tabViewData{'offset'},
                'count'  => $tabViewData{'step'},
                '&total' => 0
            }
        };
        
        my $tabSheets = &LOC::Repair::Sheet::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                        $tabFilters,
                                                        LOC::Repair::Sheet::GETINFOS_MACHINE |
                                                        LOC::Repair::Sheet::GETINFOS_CONTRACT |
                                                        LOC::Repair::Sheet::GETINFOS_CUSTOMER,
                                                        $tabOptions);

        my @tabList;

        foreach my $tabSheetInfos (values(%$tabSheets))
        {
            push(@tabList, {
                'id'               => $tabSheetInfos->{'id'},
                'customerName'     => $tabSheetInfos->{'customer.name'},
                'contractCode'     => $tabSheetInfos->{'contract.code'},
                'machineParkNumber'=> $tabSheetInfos->{'machine.parkNumber'},
                'stateId'          => $tabSheetInfos->{'state.id'},
                'flag'             => $tabSheetInfos->{'flag'},
                'url'              => &LOC::Request::createRequestUri('repair:sheet:edit', {'sheetId' => $tabSheetInfos->{'id'}})
            });
        }

        $tabViewData{'result'} = \@tabList;
        $tabViewData{'total'}  = $tabOptions->{'limit'}->{'&total'};
    }


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/sheet/jsonSearch.cgi';
}



# Function: pdfAction
# Génération du PDF de la fiche de remise en état
sub pdfAction
{
    my $tabParameters = $_[0];

    #Récupération des infos user
    my $user     = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData;

    # hachage de l'utilisateur
    $tabViewData{'localeId'}  = $user->{'locale.id'};
    $tabViewData{'agencyId'}  = $user->{'nomadAgency.id'};
    $tabViewData{'countryId'} = $user->{'nomadCountry.id'};
    $tabViewData{'currencySymbol'} = $user->{'currency.symbol'};

    $tabViewData{'id'} = &LOC::Request::getInteger('id');

    my $tabSheetInfos = &LOC::Repair::Sheet::getInfos($tabViewData{'countryId'}, $tabViewData{'id'},
                                                      LOC::Repair::Sheet::GETINFOS_ALL);

    our $var = {
                'code'                    => $tabSheetInfos->{'id'},
                'agencyId'                => $tabSheetInfos->{'agency.id'},
                'dateCreation'            => $tabSheetInfos->{'dateCreation'},
                'dateControl'             => $tabSheetInfos->{'date'},
                'dateCreation'            => $tabSheetInfos->{'dateCreation'},
                'customer.name'           => $tabSheetInfos->{'customer.name'},
                'contract.code'           => $tabSheetInfos->{'contract.code'},
                'site.label'              => $tabSheetInfos->{'site.label'},
                'model.label'             => $tabSheetInfos->{'model.label'},
                'machine.parkNumber'      => $tabSheetInfos->{'machine.parkNumber'},
                'labour.time'             => $tabSheetInfos->{'labourTime'},
                'labour.unitAmount'       => $tabSheetInfos->{'labourUnitAmount'},
                'labour.amount'           => $tabSheetInfos->{'labourAmount'},
                'travelTime'              => $tabSheetInfos->{'travelTime'},
                'travelTime.unitAmount'   => $tabSheetInfos->{'travelTimeUnitAmount'},
                'travelTime.amount'       => $tabSheetInfos->{'travelTimeAmount'},
                'displacement.km'         => $tabSheetInfos->{'displKm'},
                'displacement.unitAmount' => $tabSheetInfos->{'displUnitAmount'},
                'displacement.amount'     => $tabSheetInfos->{'displAmount'},
                'amount'                  => $tabSheetInfos->{'sheetAmount'},
                'comment'                 => $tabSheetInfos->{'sheetComment'},
               };
    my %tabCreatorInfos = &LOC::User::getInfos($tabSheetInfos->{'user.id'});
    $var->{'creator'} = $tabCreatorInfos{'firstName'} . ' ' . $tabCreatorInfos{'name'};

    if (defined $tabSheetInfos->{'interlocutor.id'} && $tabSheetInfos->{'interlocutor.id'} != 0)
    {
        my %tabInterlocutorInfos = &LOC::User::getInfos($tabSheetInfos->{'interlocutor.id'});
        $var->{'interlocutor'} = $tabInterlocutorInfos{'firstName'} . ' ' . $tabInterlocutorInfos{'name'};
    }
    $var->{'tabSheetCheckPoints'} = &LOC::Repair::Sheet::getCheckPoints($tabViewData{'countryId'},
                                                                        $tabViewData{'id'},
                                                                        LOC::Util::GETLIST_ASSOC,
                                                                        LOC::Repair::Sheet::GETCHECKPOINTINFOS_ALL);

    $var->{'tabSheetServices'} = &LOC::Repair::Sheet::getServices($tabViewData{'countryId'},
                                                                  $tabViewData{'id'},
                                                                  LOC::Util::GETLIST_ASSOC,
                                                                  LOC::Repair::Sheet::SHEET_FORMAT);

    $var->{'tabServices'} = &LOC::Repair::Service::getList($tabViewData{'countryId'}, LOC::Util::GETLIST_ASSOC);

    my $directory = &dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;

    require $directory . '/view/sheet/pdf.cgi';
}
#Function pdfNoticeAction
#Génération du pdf avis au client
sub pdfNoticeAction
{
    my $tabParameters = $_[0];

    #Récupération des infos user
    my $user     = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData;

    # hachage de l'utilisateur
    $tabViewData{'localeId'}  = $user->{'locale.id'};
    $tabViewData{'agencyId'}  = $user->{'nomadAgency.id'};
    $tabViewData{'countryId'} = $user->{'nomadCountry.id'};
    $tabViewData{'currencySymbol'} = $user->{'currency.symbol'};

    $tabViewData{'id'} = &LOC::Request::getInteger('id');

    my $tabSheetInfos = &LOC::Repair::Sheet::getInfos($tabViewData{'countryId'}, $tabViewData{'id'},
                                                      LOC::Repair::Sheet::GETINFOS_ALL);

    my $tabCustomerInfos = &LOC::Customer::getInfos($tabViewData{'countryId'}, $tabSheetInfos->{'customer.id'});

    # Récupération des informations sur le chantier
    my $tabSiteInfos = &LOC::Site::getInfos($tabViewData{'countryId'}, $tabSheetInfos->{'site.id'});

    # Récupération des informations contrat
    my $tabContractInfos = &LOC::Contract::Rent::getInfos($tabViewData{'countryId'}, $tabSheetInfos->{'contract.id'});

    # PDF complémentaires à insérer
    my $tabAdditionalDocuments = &LOC::Json::fromJson(&LOC::Characteristic::getCountryValueByCode('PDFCPLFRE', $tabViewData{'countryId'}));
    foreach my $tabZone (values(%$tabAdditionalDocuments))
    {
        foreach my $documentInfos (@$tabZone)
        {
            $documentInfos->{'path'} =~ s/<%commonFilesPath>/@{[&LOC::Globals::get('commonFilesPath')]}/;
        }
    }
    $tabViewData{'tabAdditionalDocuments'} = $tabAdditionalDocuments;

    our $var = {
        'code'                        => $tabSheetInfos->{'id'},
        'agencyId'                    => $tabSheetInfos->{'agency.id'},
        'dateCreation'                => $tabSheetInfos->{'dateCreation'},
        'dateControl'                 => $tabSheetInfos->{'date'},
        'dateCreation'                => $tabSheetInfos->{'dateCreation'},
        'customer.name'               => $tabSheetInfos->{'customer.name'},
        'customer.address1'           => $tabCustomerInfos->{'address1'},
        'customer.address2'           => $tabCustomerInfos->{'address2'},
        'customer.locality'           => $tabCustomerInfos->{'locality.postalCode'} . ' ' . $tabCustomerInfos->{'locality.name'},
        'customer.tel'                => $tabCustomerInfos->{'telephone'},
        'customer.fax'                => $tabCustomerInfos->{'fax'},
        'contract.code'               => $tabSheetInfos->{'contract.code'},
        'site.label'                  => $tabSiteInfos->{'label'},
        'site.locality.label'         => $tabSiteInfos->{'locality.label'},
        'site.contact.fullName'       => $tabSiteInfos->{'contact.fullName'},
        'site.contact.telephone'      => $tabSiteInfos->{'contact.telephone'},
        'orderContact.fullName'       => $tabContractInfos->{'orderContact.fullName'},
        'orderContact.telephone'      => $tabContractInfos->{'orderContact.telephone'},
        'model.label'                 => $tabSheetInfos->{'model.label'},
        'machine.parkNumber'          => $tabSheetInfos->{'machine.parkNumber'},
        'sheetAmount'                 => $tabSheetInfos->{'sheetAmount'},
        'family.label'                => $tabSheetInfos->{'family.label'},
        'comment'                     => &LOC::Characteristic::getCountryValueByCode('PDFFRE', $tabViewData{'countryId'}),
        'currencySymbol'              => $tabViewData{'currencySymbol'}
    };
    my %tabCreatorInfos = &LOC::User::getInfos($tabSheetInfos->{'user.id'});
    $var->{'creator'} = $tabCreatorInfos{'firstName'} . ' ' . $tabCreatorInfos{'name'};

    my $directory = &dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;

    require $directory . '/view/sheet/pdfNotice.cgi';
}


sub zipPhotosAction
{

    my $tabParameters = $_[0];
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $countryId    = $tabUserInfos->{'nomadCountry.id'};
    my $agencyId     = $tabUserInfos->{'nomadAgency.id'};

    # Variables pour la vue
    our %tabViewData = (
                        'localeId'       => $tabUserInfos->{'locale.id'},
                        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
                        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
                        'currencyId'     => $tabUserInfos->{'currency.id'},
                        'countryId'      => $tabUserInfos->{'nomadCountry.id'}
                       );

    # Id de la FRE
    $tabViewData{'sheetId'}        = &LOC::Request::getInteger('sheetId', 0);

    # Liste des photos à zipper
    my $photos                     = &LOC::Request::getString('photos', '', &LOC::Request::SRC_REQUEST);
    $tabViewData{'tabPhotos'}      = &LOC::Json::fromJson($photos);

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/sheet/zipPhoto.cgi';

}

sub _getMachineContracts
{
    my ($countryId, $agencyId, $machineId, $tabStates, $contractId) = @_;
    #Récupération des 10 derniers contrats de la machine
    my $tabFilters = {
                      'id'        => $contractId,
                      'machineId' => $machineId * 1,
                      'agencyId'  => $agencyId,
                      'stateId'   => $tabStates
                     };
    my $machineContractsInfos = &LOC::Contract::Rent::getList($countryId,
                                                              LOC::Util::GETLIST_ASSOC,
                                                              $tabFilters,
                                                              LOC::Contract::Rent::GETINFOS_EXT_SITE |
                                                              LOC::Contract::Rent::GETINFOS_CUSTOMER,
                                                              {'getLastContracts' => DISPLAY_NBCONTRACTS});

    #Si des résultats sont retournés
    if (scalar(keys%{$machineContractsInfos}) != 0)
    {
        my $varKey;
        #Hachage ordonné dans l'ordre balancé
        my @machineContracts = ();
        my $dayAmount;
        #On récupére les informations nécessaires pour chaque contrat
        foreach $varKey (keys %{$machineContractsInfos})
        {
            if ($machineContractsInfos->{$varKey}->{'amountType'} eq 'month')
            {
                $dayAmount = &LOC::Util::round($machineContractsInfos->{$varKey}->{'unitPrice'} / LOC::Date::AVERAGE_MONTH_WORKING_DAYS, 0);
            }
            elsif ($machineContractsInfos->{$varKey}->{'amountType'} eq 'fixed')
            {
                $dayAmount = &LOC::Util::round($machineContractsInfos->{$varKey}->{'unitPrice'} / $machineContractsInfos->{$varKey}->{'duration'},0);
            }
            else
            {
                $dayAmount = $machineContractsInfos->{$varKey}->{'unitPrice'};
            }

            push(@machineContracts, {'id'              => $varKey,
                                     'code'            => $machineContractsInfos->{$varKey}->{'code'},
                                     'customerName'    => $machineContractsInfos->{$varKey}->{'customer.name'},
                                     'customerEmail'   => $machineContractsInfos->{$varKey}->{'customer.email'},
                                     'customerIsProformaRequired' => $machineContractsInfos->{$varKey}->{'customer.isProformaRequired'},
                                     'siteLabel'       => $machineContractsInfos->{$varKey}->{'site.tabInfos'}->{'label'},
                                     'siteAddress'     => $machineContractsInfos->{$varKey}->{'site.tabInfos'}->{'address'},
                                     'sitePostalLabel' => $machineContractsInfos->{$varKey}->{'site.tabInfos'}->{'locality.label'},
                                     'url'             => &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $varKey}),
                                     'invoiceState'    => $machineContractsInfos->{$varKey}->{'invoiceState'},
                                     'proformaFlags'   => $machineContractsInfos->{$varKey}->{'proformaFlags'},
                                     'dayAmount'       => $dayAmount,
                                     'amountType'      => $machineContractsInfos->{$varKey}->{'amountType'}
                                    });
        }

        #On forme le hash de retour des données vers la fiche
        if (@machineContracts)
        {
            return  \@machineContracts;
        }

    }

    return [];
}



1;