use utf8;
use open (':encoding(UTF-8)');

# Package: checkpoint
# Contrôleur point de vérification
package checkpoint;

use strict;
use File::Basename;

use LOC::Family;
use LOC::Repair::Checkpoint;
use LOC::Repair::Article;
use LOC::Repair::CheckpointFamily;
use LOC::Repair::ProfitMargin;
use LOC::Agency;
use LOC::User;
use LOC::Util;
use LOC::Locale;


# Function: editAction
# Prépare les données pour l'affichage de l'interface d'édition d'un point de vérification
sub editAction
{
    my $tabParameters = $_[0];

    #Récupération des infos user
    my $user    = $tabParameters->{'tabUserInfos'};
    
    # Variables pour la vue
    our %tabViewData = (
        'user.agency.id' => $user->{'nomadAgency.id'},
    );
    
    # hachage de l'utilisateur
    $tabViewData{'user'} = $user;
    $tabViewData{'localeId'} = $user->{'locale.id'};
    $tabViewData{'countryId'} = $user->{'nomadCountry.id'};
    $tabViewData{'currencySymbol'} = $user->{'currency.symbol'};

    # Récupération des variables du formulaire
    $tabViewData{'id'}                 = &LOC::Request::getInteger('id', 0);
    $tabViewData{'label'}              = &LOC::Request::getString('label');
    $tabViewData{'isChgPartAllowed'}   = &LOC::Request::getInteger('isChgPartAllowed', 0);
    $tabViewData{'isFixAllowed'}       = &LOC::Request::getInteger('isFixAllowed', 0);
    $tabViewData{'isRepairAllowed'}    = &LOC::Request::getInteger('isRepairAllowed', 0);
    $tabViewData{'isArticleRequired'}  = &LOC::Request::getInteger('isArticleRequired', 0);
    $tabViewData{'isMarginAllowed'}    = &LOC::Request::getInteger('isMarginAllowed', 0);
    $tabViewData{'fixCost'}            = &LOC::Request::getInteger('fixCost');
    $tabViewData{'repairCost'}         = &LOC::Request::getInteger('repairCost');
    $tabViewData{'tabArticles'}        = &LOC::Request::getHash('tabArticle');
    $tabViewData{'tabFamilies'}        = &LOC::Request::getHash('tabFamily');
    $tabViewData{'valid'}              = &LOC::Request::getString('valid');
    $tabViewData{'avgCost'}            = 0;

    # Listes
    $tabViewData{'families'}           = &LOC::Family::getList(LOC::Util::GETLIST_ASSOC, {}, $tabViewData{'localeId'});
    $tabViewData{'tabFamilyValues'}    = {};

    # Ajout, modification
    if ($tabViewData{'valid'} ne '')
    {
        # Préparation des données
        my $values = {
                        'currency.id'       => $user->{'currency.id'},
                        'label'             => $tabViewData{'label'},
                        'changePartAllowed' => $tabViewData{'isChgPartAllowed'},
                        'repairAllowed'     => $tabViewData{'isRepairAllowed'},
                        'fixAllowed'        => $tabViewData{'isFixAllowed'},
                        'articleRequired'   => $tabViewData{'isArticleRequired'},
                        'marginAllowed'     => $tabViewData{'isMarginAllowed'},
                        'repairCost'        => $tabViewData{'repairCost'},
                        'fixCost'           => $tabViewData{'fixCost'}, 
                     };
                     
        if ($tabViewData{'id'} != 0)
        {
            $values->{'id'} = $tabViewData{'id'};
        }
        
        $values->{'articles'} = $tabViewData{'tabArticles'};
        
        my %families;
        my $tab = $tabViewData{'tabFamilies'};
        while (my ($i, $family) = each(%$tab))
        {
            if ($family->{'cb'} == 1 && $family->{'order'} ne '')
            {
                $families{$family->{'id'}} = $family->{'order'};
            }
        }
        $values->{'families'} = \%families;
        
        # Enregistrement
        my $result = &LOC::Repair::Checkpoint::set($user->{'nomadCountry.id'}, $values);
        if ($result > 0)
        {
            if ($tabViewData{'id'} == 0)
            {
                $tabViewData{'id'} = $result;
                $tabViewData{'valid'} = 'added';
            }
            else
            {
                $tabViewData{'valid'} = 'saved';
            }
        }
        else
        {
            $tabViewData{'valid'} = '';
            $tabViewData{'messages'} = {'error' => ['Une erreur s\'est produite lors de l\'enregistrement']};
        }
    }

    # (Re)chargement des données
    if ($tabViewData{'id'} != 0 || $tabViewData{'valid'} ne '')
    {
        # Initialise le point de vérification
        my $chp = &LOC::Repair::Checkpoint::getInfos($tabViewData{'countryId'}, $tabViewData{'id'});

        $tabViewData{'label'}              = $chp->{'label'};
        $tabViewData{'isChgPartAllowed'}   = $chp->{'changePartAllowed'};
        $tabViewData{'isFixAllowed'}       = $chp->{'fixAllowed'};
        $tabViewData{'isRepairAllowed'}    = $chp->{'repairAllowed'};
        $tabViewData{'isArticleRequired'}  = $chp->{'articleRequired'};
        $tabViewData{'isMarginAllowed'}    = $chp->{'marginAllowed'};
        $tabViewData{'fixCost'}            = $chp->{'fixCost'};
        $tabViewData{'repairCost'}         = $chp->{'repairCost'};
        $tabViewData{'avgCost'}            = $chp->{'articles.averageCost'};
        
        # Récupération des infos sur les articles liés au point
        $tabViewData{'tabArticles'}        = &LOC::Repair::Article::getList($tabViewData{'countryId'},
                                                                           LOC::Util::GETLIST_ASSOC,
                                                                           {'checkpoint' => $tabViewData{'id'}});

        # Récupération des familles liées au point avec l'ordre d'affichage
        $tabViewData{'tabFamilyValues'} = &LOC::Repair::CheckpointFamily::getOrders($tabViewData{'countryId'},
                                                                                    $tabViewData{'id'},
                                                                                    LOC::Repair::CheckpointFamily::FAMILIES_BY_CHECKPOINTID);
    }

    # Message de confirmation
    if ($tabViewData{'valid'} eq 'added')
    {
        $tabViewData{'messages'} = {'valid' => ['Le point de vérification a été ajouté']};
    }
    elsif ($tabViewData{'valid'} eq 'saved')
    {
        $tabViewData{'messages'} = {'valid' => ['Les modifications ont été enregistrées']};
    }

    # Chemin d'accès aux scripts de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    $directory .= '/view/checkpoint/';
    $tabViewData{'scriptPath'} = $directory;

    # Affichage de la vue
    require $tabViewData{'scriptPath'} . 'edit.cgi';
}

sub viewAction
{
    my $user = &LOC::Session::getUserInfos();
    
    # Variables pour la vue
    our %tabViewData = (
        'user.agency.id' => $user->{'nomadAgency.id'},
    );
    
    # hachage de l'utilisateur
    $tabViewData{'localeId'} = $user->{'locale.id'};
    
    # locale
    my $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});

    # Récupération des variables du formulaire
    my $tabToDelete = &LOC::Request::getHash('checkpointsToDelete');
    my $valid = &LOC::Request::getString('valid');

    if ($valid ne '')
    {
        # Itère sur la liste des éléments à supprimer
        my $deleted = 0;
        while (my ($id, $checked) = each(%$tabToDelete))
        {
            # Suppression
            if ($checked == 1)
            {
                &LOC::Repair::Checkpoint::delete($user->{'nomadCountry.id'}, $id);
                $deleted++;
            }
        }
        
        # message de suppression
        if ($deleted > 0)
        {
            if ($deleted > 1)
            {
                $tabViewData{'messages'} = {'info' => [$deleted . ' ' . $locale->t('points de vérifications supprimés')]};
            }
            else
            {
                $tabViewData{'messages'} = {'info' => ['1 ' . $locale->t('point de vérification supprimé')]};
            }
        }
    }

    # Récupère la liste des points
    $tabViewData{'tabCheckpoints'} = &LOC::Repair::Checkpoint::getList($user->{'nomadCountry.id'},
                                                                       LOC::Util::GETLIST_ASSOC);

    # chemin d'accès aux scripts de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    $directory .= '/view/checkpoint/';

    # affichage de la vue
    require $directory . 'view.cgi';
}


# Function: jsonSearchArticlesAction
# Effectue une recherche d'articles Kimoce
sub jsonSearchArticleAction
{
    # Variables pour la vue
    our %tabViewData = (
        'values' => {
            'articles' => [],
            'errorMsgs' => ''
        }
    );

    # hachage de l'utilisateur
    my $user = &LOC::Session::getUserInfos();

    # Pays
    my $countryId = &LOC::Request::getString('countryId', $user->{'nomadCountry.id'});

    # locale
    my $locale = &LOC::Locale::getLocale($user->{'locale.id'});

    my $artCode    = &LOC::Request::getString('articleCode');
    my $maxResultsCount = &LOC::Request::getInteger('maxResultsCount', 10);
    my $withMargin = &LOC::Request::getString('withMargin', 0);
    my $type       = &LOC::Request::getString('type', 'exact');

    my $tabResult;
    if ($type eq 'exact')
    {
        if ($artCode)
        {
            $tabResult = &LOC::Repair::Article::getInfosFromKimoce($countryId, {'code' => $artCode}, {'withMargin' => $withMargin});
        }
    }
    elsif ($type eq 'multiple')
    {
        $tabResult = &LOC::Repair::Article::getListFromKimoce($countryId, {'code%' => $artCode}, {'withMargin' => $withMargin,
                                                                                                  'limit'      => $maxResultsCount});
    }

    if ($tabResult)
    {
        $tabViewData{'values'}->{'articles'} = $tabResult;
    }
    else
    {
        $tabViewData{'values'}->{'errorMsgs'} = 'searchArticle_noResult';
    }

    # chemin d'accès aux scripts de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    $directory .= '/view/checkpoint/';

    # affichage de la vue
    require $directory . 'jsonSearchArticle.cgi';
}

1;