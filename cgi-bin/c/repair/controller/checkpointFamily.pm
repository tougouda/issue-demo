use utf8;
use open (':encoding(UTF-8)');

# Package: checkpointFamily
# Contrôleur modèle de fiche de remise en état
package checkpointFamily;

use strict;
use File::Basename;

use LOC::Family;
use LOC::Repair::Checkpoint;
use LOC::Repair::CheckpointFamily;
use LOC::User;
use LOC::Util;


# Function: editAction
# Prépare les données pour l'affichage de l'interface d'édition d'un point de vérification
sub editAction
{
    my $user = &LOC::Session::getUserInfos();
    
    # Variables pour la vue
    our %tabViewData = (
        'user.agency.id' => $user->{'nomadAgency.id'},
    );
    
    
    # hachage de l'utilisateur
    $tabViewData{'user'} = $user;
    $tabViewData{'localeId'} = $user->{'locale.id'};
    $tabViewData{'countryId'} = $user->{'nomadCountry.id'};

    # Récupération des variables du formulaire
    $tabViewData{'familyId'}           = &LOC::Request::getInteger('familyId', 0);
    $tabViewData{'tabCheckpoints'}     = &LOC::Request::getHash('tabCheckpoint');
    $tabViewData{'valid'}              = &LOC::Request::getString('valid');

    # Listes
    $tabViewData{'checkpoints'}         = &LOC::Repair::Checkpoint::getList($tabViewData{'countryId'}, LOC::Util::GETLIST_ASSOC);
    $tabViewData{'tabCheckpointValues'} = {};
    
    # Infos sur la famille de machine
    if ($tabViewData{'familyId'})
    {
        $tabViewData{'family'} = &LOC::Family::getInfos($tabViewData{'familyId'}, $tabViewData{'localeId'});
    }

    # Ajout, modification
    if ($tabViewData{'valid'} ne '')
    {
        # Préparation des données
        my %checkpoints;
        my $tab = $tabViewData{'tabCheckpoints'};
        while (my ($i, $checkpoint) = each(%$tab))
        {
            if ($checkpoint->{'cb'} == 1 && $checkpoint->{'order'} ne '')
            {
                $checkpoints{$checkpoint->{'id'}} = $checkpoint->{'order'};
            }
        }
        
        # Enregistrement
        my $result = &LOC::Repair::CheckpointFamily::setOrders($tabViewData{'countryId'}, \%checkpoints, $tabViewData{'familyId'},
                                                               LOC::Repair::CheckpointFamily::CHECKPOINTS_BY_FAMILYID);
        if ($result > 0)
        {
            $tabViewData{'valid'} = 'saved';
        }
        else
        {
            $tabViewData{'valid'} = '';
            $tabViewData{'messages'} = {'error' => ['Une erreur s\'est produite lors de l\'enregistrement']};
        }
    }

    # (Re)chargement des données
    if ($tabViewData{'familyId'} != 0 || $tabViewData{'valid'} ne '')
    {
        # Récupération des points de vérifications liés à la famille avec l'ordre d'affichage
        $tabViewData{'tabCheckpointValues'} = &LOC::Repair::CheckpointFamily::getOrders($tabViewData{'countryId'},
                                                                                        $tabViewData{'familyId'},
                                                                                        LOC::Repair::CheckpointFamily::CHECKPOINTS_BY_FAMILYID);
    }

    # Message de confirmation
    if ($tabViewData{'valid'} eq 'saved')
    {
        $tabViewData{'messages'} = {'valid' => ['Enregistrement effectué']};
    }

    # Chemin d'accès aux scripts de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    $directory .= '/view/checkpointFamily/';
    $tabViewData{'scriptPath'} = $directory;

    # Affichage de la vue
    require $tabViewData{'scriptPath'} . 'edit.cgi';
}

sub viewAction
{
    my $user = &LOC::Session::getUserInfos();
    
    # Variables pour la vue
    our %tabViewData = (
        'user.agency.id' => $user->{'nomadAgency.id'},
    );
    
    # hachage de l'utilisateur
    $tabViewData{'localeId'} = $user->{'locale.id'};

    # Récupère la liste des associations famille / point de vérification
    $tabViewData{'tabFamilies'} = &LOC::Family::getList(LOC::Util::GETLIST_ASSOC, {}, $tabViewData{'localeId'});

    # chemin d'accès aux scripts de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    $directory .= '/view/checkpointFamily/';

    # affichage de la vue
    require $directory . 'view.cgi';
}

1;