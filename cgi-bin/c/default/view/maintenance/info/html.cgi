use utf8;
use open (':encoding(UTF-8)');

use strict;
use lib 'inc', 'lib';

use LOC::Html;

our %tabViewData;


my $view = LOC::Html->new($tabViewData{'locale.id'});


$view->setPageTitle($tabViewData{'title'});


# CSS
$view->addCSSSrc('location.css');
$view->addCSSSrc('default/maintenance/info.css');

# JS
$view->addJSSrc('default/maintenance/info.js');


# Initialisation du JavaScript
$view->addBodyEvent('load', 'info.init("' . $tabViewData{'checkUrl'} . '", ' . $tabViewData{'period'} . ');');


print $view->displayHeader();

print '
<div class="logo">' . $view->displayImage('default/maintenance/icon.svg') . '</div>
<div id="info" class="info">
    <div class="wrapper">
        <div id="pendingMessage" class="message">
            <h1>' . $tabViewData{'pending'}->{'heading'} . '</h1>
            ' . $tabViewData{'pending'}->{'message'} . '
        </div>
        <div id="completeMessage" class="message">
            <h1>' . $tabViewData{'complete'}->{'heading'} . '</h1>
            ' . $tabViewData{'complete'}->{'message'} . '
        </div>
    </div>
</div>';

print $view->displayFooter();
