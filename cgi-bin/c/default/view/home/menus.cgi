use utf8;
use open (':encoding(UTF-8)');

use strict;

our %myViewData;
our $view;
our $locale;

my $user = $myViewData{'user'};

my $htmlMenu = '';
my $curParentId = '';
my $menus = $myViewData{'menus'};
for (my $i = 0; $i < @$menus; $i++)
{
    my $menu = $menus->[$i];

    if ($curParentId ne $menu->{'parent.id'})
    {
        if ($curParentId ne '')
        {
            $htmlMenu .= '</div></div>';
        }
        $htmlMenu .= '<div class="menuItem arrow" onclick="menuClick(event, this, 1);">' .
                        '<a href="javascript:;">' .
                            '<div class="text">' .
                                 $menu->{'parent.label'} .
                            '</div>' .
                        '</a>' .
                        '<div class="subMenu">';
    }

    my $url;
    my $expr = LOC::Request::REGEXP_MCA_URL;
    if ($menu->{'url'} =~ m/$expr/)
    {
        $url = $view->createURL($menu->{'url'});
    }
    else
    {
        my $directory = $menu->{'parent.dir'};
        $directory =~ s/^\///;
        $url = &LOC::Globals::get('locationUrl') . '/' . $directory . $menu->{'url'};
    }

    $htmlMenu .=            '<div class="menuItem">' .
                                '<a href="' . $url . '" onclick="LOC_Common.stopEventBubble(event); return true;" target="_blank">' .
                                    '<div class="text">' .
                                        $menu->{'label'} .
                                    '</div>' .
                                '</a>' . '
                            </div>';

    $curParentId = $menu->{'parent.id'};
}
$htmlMenu .= '</div></div>';

my $html = '';
$html .= '<div class="top">' . 
            '<div class="topContent">' .
                $htmlMenu .
            '</div>' .
        '</div>' .
        '<div class="bottom">' .
        '</div>';

print $html;