use utf8;
use open (':encoding(UTF-8)');

use strict;

our %myViewData;
our $view;
our $locale;

my $nextI = $myViewData{'nextInvcDates'}{'inter'};
my $nextM = $myViewData{'nextInvcDates'}{'month'};
my $accessInvoicingControls = $myViewData{'accessInvoicingControls'};

my $htmlDateI = '';
if ($nextI)
{
    if ($accessInvoicingControls)
    {
        my $url = $view->createURL('accounting:controls:view', {'date' => $nextI});

        $htmlDateI .= '
<span class="invoicing">' .
        $view->displayImage('default/home/invoicing/intermediate.png') . '&nbsp;' .
        '<a target="_blank" href="' . $url . '" title="' . $locale->t('Voir l\'état des contrôles pré-facturation') . '">' .
        $locale->t('Intermédiaire le %s',
                   '<span>' . $locale->getDateFormat($nextI, LOC::Locale::FORMAT_DATEYEAR_ABBREV) . '</span>') .
        '</a>
</span>';
    }
    else
    {
        $htmlDateI .= '
<span class="invoicing">' .
        $view->displayImage('default/home/invoicing/intermediate.png') . '&nbsp;' .
        $locale->t('Intermédiaire le %s',
                   '<span>' . $locale->getDateFormat($nextI, LOC::Locale::FORMAT_DATEYEAR_ABBREV) . '</span>') .
        '
</span>';
    }
}


my $htmlDateM = '';
if ($nextM)
{
    if ($accessInvoicingControls)
    {
        my $url = $view->createURL('accounting:controls:view', {'date' => $nextM});

        $htmlDateM .= '
<span class="invoicing">' .
        $view->displayImage('default/home/invoicing/monthly.png') . '&nbsp;' .
        '<a target="_blank" href="' . $url . '" title="' . $locale->t('Voir l\'état des contrôles pré-facturation') . '">' .
        $locale->t('Mensuelle le %s',
                   '<span>' . $locale->getDateFormat($nextM, LOC::Locale::FORMAT_DATEYEAR_ABBREV) . '</span>') .
        '</a>
</span>';
    }
    else
    {
        $htmlDateM .= '
<span class="invoicing">' .
        $view->displayImage('default/home/invoicing/monthly.png') . '&nbsp;' .
        $locale->t('Mensuelle le %s',
                   '<span>' . $locale->getDateFormat($nextM, LOC::Locale::FORMAT_DATEYEAR_ABBREV) . '</span>') .
        '
</span>';
    }
}

my $html = '';
if ($htmlDateI && $htmlDateM && (&LOC::Date::compare($nextI, $nextM) > 0))
{
    $html = '
<fieldset class="nextinvoicing">
    <legend>' . $locale->t('Prochaines facturations') . '</legend>' .
    $htmlDateM .
    $htmlDateI . '
</fieldset>';
}
else
{
    $html = '
<fieldset class="nextinvoicing">
    <legend>' . $locale->t('Prochaines facturations') . '</legend>' .
    $htmlDateI .
    $htmlDateM . '
</fieldset>';
}

print $html;