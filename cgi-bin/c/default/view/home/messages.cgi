use utf8;
use open (':encoding(UTF-8)');

use strict;

our %myViewData;
our $view;
our $locale;

my $messages = $myViewData{'messages'};

my $display;
if (%$messages > 0)
{
    $display = 'block';
}
else
{
    $display = 'none';
}

my $htmlMessages = '
<div id="newsContainer" class="newscontainer" style="display: ' . $display . ';">
    <div class="top">
        <div class="title">' . $locale->t('Infos') . '</div>
        <div class="controls">' . $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png', 'refreshMessages();') . '</div>
    </div>
    <div style="position: relative;">
        <div id="newsLoading" class="homeLoading"></div>
        <div id="newsContent" class="news">';
if (%$messages > 0)
{
    while (my ($id, $message) = each(%$messages))
    {
        my $className = '';
        if ($message->{'priority'} == 1)
        {
            $className = ' breaking';
        }
        $htmlMessages .= '
        <div class="title' . $className . '">' . $message->{'subject'} . '</div>
        <div class="date">' . $locale->getDateFormat($message->{'dateCreation'}, LOC::Locale::FORMAT_DATETIME_NUMERIC,
                                                                $myViewData{'timeZone'}) . '</div>
        <div class="message' . $className . '">' . $message->{'content'} . '</div>';
    }
}
$htmlMessages .= '</div></div></div>';

print $htmlMessages;