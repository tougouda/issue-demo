use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Json;

our %myViewData;

my $view = LOC::Json->new();
print $view->displayHeader();

print $view->encode($myViewData{'values'});
