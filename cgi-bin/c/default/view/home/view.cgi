use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html;
use LOC::Locale;
use LOC::Date;


our %myViewData;
our $view = LOC::Html->new();
our $locale = &LOC::Locale::getLocale($myViewData{'user'}->{'locale.id'});

my $title = $locale->t('Gestion des locations');
my $user = $myViewData{'user'};

$view->setPageTitle($title . ': ' . $locale->t('Accueil'))
     ->addCSSSrc('default/home/view.css')
     ->addCSSSrc('default/home/alert.css')
     ->addJSSrc('default/home/view.js');

$view->addBodyEvent('load', 'pageInit({refreshAlertsDelay: 60000, refreshMessagesDelay: 1800000});');

$view->{'_networkOptimization'} = 'default-home-view';
my $html = $view->displayHeader();

my $welcomeExtras = '';

my $img = &LOC::Globals::get('imgUrl') . '/base/country/' . lc($myViewData{'localeCountryId'}) . '.gif';
my $lang = $view->displayImage($img, {'title' => $myViewData{'localeLabel'}, 'class' => 'active'});

my $searchContent = '';
# Raison sociale du client
my $customerName = $view->displayFormInputText('customerName', '', '', {'onkeypress' => 'return enterKey(event);'});
# Ville Chantier
my $city = $view->displayFormInputText('city', '', '', {'onkeypress' => 'return enterKey(event);'});
# Numéro parc
my $parcNumber = $view->displayFormInputText('parcNumber', '', '', {'size' => 5, 'onkeypress' => 'return enterKey(event);'});

$searchContent .= '<div id="mySearchBar">' .
            '<span class="label" id="labelCustomerName">' . $locale->t('Raison sociale') . '</span>:&nbsp;' . $customerName .
            '<span class="label" id="labelCity">' . $locale->t('Ville chantier') . '</span>:&nbsp;' . $city .
            '<span class="label" id="labelParcNumber">' . $locale->t('Machine') . '</span>:&nbsp;' . $parcNumber . '&nbsp;&nbsp;' .
            $view->displayTextButton('', 'search(|Over).png', 'doSearch();', $locale->t('Rechercher')) .
            $view->displayTextButton('', 'clear.gif', 'clearSearch();', $locale->t('Effacer la recherche')) .  
        '</div>';

# Barre de titre
my $env = ($myViewData{'envLabel'} eq '' ? '' : '[' . $locale->t($myViewData{'envLabel'}) .
                                           ($myViewData{'dev'} eq '' ? '' : ' DB' . $myViewData{'dev'}) .
                                           ($myViewData{'envStr'} eq '' ? '' : ' ' . $myViewData{'envStr'}) . ']');

# Boutons de changement de base de données
my @btnDB = ();
if ($myViewData{'env'} =~ /^dev/)
{
    for (my $i = 1; $i <= 2; $i++)
    {
        my $className = 'switchdb';
        if ($myViewData{'dev'} == $i || $myViewData{'dev'} eq '' && $i == 1)
        {
            $className .= ' selected';
        }
        my $onclick = 'window.document.location = "' . $myViewData{'locationUrl'} . '?dev=' . $i . '"';
        push(@btnDB, $view->displayFormInputSubmit('dbBtn', 'DB' . $i, {'class' => $className, 'onclick' => $onclick}));
    }
}

my $img = &LOC::Globals::get('imgUrl') . '/icons/loc-light-bg.svg';

$html .= "<!--TITRE:DEBUT-->\n".
        '<div id="homeTitle">' .
            '<div class="env">' .
            $env .
            join('', @btnDB) .
            '</div>' .
            '<div class="right"></div>' .
            '<div class="titleGrid">' .
                '<div class="logo">' . $view->displayImage($img, {'title' => $title, 'alt' => $title}) . '</div>' .
                '<div class="welcome">' . $locale->t('Bonjour') . ', ' .
                    '<span class="name">' . $user->{'firstName'} . ' ' . $user->{'name'} . '</span>' .
                    '<span id="homeWelcomeExtras" class="extras">' . $welcomeExtras . '</span>' .
                '</div>' .
                '<div class="langs"><div class="separator"></div>' . $lang . '</div>' .
                '<div class="search">' . $searchContent . '</div>' .
            '</div>'.
        "</div>\n" .
        "<!--TITRE:FIN-->\n";

# Menu et corps de page
$html .= "<!--CORPS:DEBUT-->\n";
$html .= '<div id="homeMiddle">' . "\n";
$html .= '<div id="homeMenu">';

print $html;


# Affichage des menus
require $myViewData{'scriptPath'} . 'menus.cgi';


$html  = '</div>';
$html .= '<div id="homeContent">';

# Creation formulaire
$html .= $view->startForm();

print $html;


# Affichage de la barre d'infos agence
require $myViewData{'scriptPath'} . 'agencies.cgi';


# Affichage des prochaines facturations
require $myViewData{'scriptPath'} . 'invoicing.cgi';


# Affichage des alertes
require $myViewData{'scriptPath'} . 'alerts.cgi';


# Affichage des messages d'information
require $myViewData{'scriptPath'} . 'messages.cgi';


# Champs cachés
$html  = $view->displayFormInputHidden('urlAlerts', $view->createURL('default:home:jsonAlert'));
$html .= $view->displayFormInputHidden('urlSrchMachine', $myViewData{'urlSrchMachine'});
$html .= $view->displayFormInputHidden('urlSrchCustomer', $myViewData{'urlSrchCustomer'});
$html .= $view->displayFormInputHidden('urlMessages', $view->createURL('default:home:jsonMessage'));

print $html;


# Footer
$html = $view->displayFormInputHidden('urlAlert', $view->createURL('default:home:jsonAlert'));
$html .= "</div>\n</div>\n<!--CORPS:FIN-->\n";
$html .= "<!--PIED:DEBUT-->\n";
$html .= "<div id=\"homeFooter\">\n";

my $currentYear = POSIX::strftime('%Y', localtime());
$html .= "\t<div class=\"copyright\" id=\"homeFooterText\">" .
                '<b>C</b>opyright <b>&copy;</b> 2005-' . $currentYear . ' <b>A</b>ccès Industrie. ' .
                '<b>T</b>ous droits réservés.';
$html .= '</div>';

if ($user->{'isAdmin'})
{
    $html .= '   <a href="#" onclick="displayChangeProfile(true); return false;" id="homeChangeProfileButton">' .
                    $view->toHTMLEntities($locale->t('Changer de profil')) .
                '</a>';
}

# Date et heure en base à droite
my $date = $locale->getDateFormat(undef, LOC::Locale::FORMAT_DATEYEAR_TEXT);
my $timeObj = '<a href="javascript:;" class="time" id="locStandardTime" title="' .
                    $view->toHTMLEntities($date) . '">---</a>';

# Maintenance
my $maintenanceObj = '';
if ($view->isMaintenanceActivated())
{
    $html .= "\t<div class=\"maintenance\">" . $locale->t('Maintenance en cours') . '</div>';
}

$html .= "\t<div class=\"version\">" . &LOC::Globals::get('version') . ' r' . &LOC::Globals::get('release') . "</div>\n";
$html .= "\t<div class=\"right\">" . $timeObj . "</div>\n";
$html .= '</div>';

# Changement de profil
if ($user->{'isAdmin'})
{
    my $selectChangeProfile = $view->displayFormSelect('profileId', $myViewData{'profiles'}, $user->{'profile.id'});
    
    my $btnChangeProfile = $view->displayFormInputSubmit('changeProfileBtn', 'OK',
                                                        {'class' => 'button', 'id' => 'changeProfileBtn'});
    
    $html .= '
<div id="homeChangeProfileWindow">
    <div class="title">' . $view->toHTMLEntities($locale->t('Changer de profil')) . '
    <a href="#" onclick="displayChangeProfile(false); return false;" class="close"></a>
    </div>
    <div class="content">' .
        $selectChangeProfile . 
        $btnChangeProfile .
   '</div>
</div>';
}

$html .= "\n<!--PIED:FIN-->\n";

$html .= $view->endForm();

# Pied de page
$html .= $view->displayFooter();

print $html;