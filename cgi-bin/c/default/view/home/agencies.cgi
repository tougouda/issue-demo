use utf8;
use open (':encoding(UTF-8)');

use strict;

our %myViewData;
our $view;
our $locale;

my $user = $myViewData{'user'}; 

# gère l'affichage du nomade
my $htmlChangeAgency = '';
if ($user->{'isNomad'})
{
    tie(my %values, 'Tie::IxHash');
    my $tabAgencies = $myViewData{'tabNomadAgencies'};
    foreach my $agencyId (keys %$tabAgencies)
    {
        my $tabAgencyInfos = $tabAgencies->{$agencyId};

        if (!exists($values{$tabAgencyInfos->{'country.name'}}))
        {
            tie(my %countryValues, 'Tie::IxHash');
            %countryValues = ();
            $values{$tabAgencyInfos->{'country.name'}} = \%countryValues;
        }
        $values{$tabAgencyInfos->{'country.name'}}{$tabAgencyInfos->{'id'}} = $tabAgencyInfos->{'name'};
    }

    my $selectChangeAgency = $view->displayFormSelect('agencyId', \%values, $user->{'nomadAgency.id'});

    my $btnChangeAgency = $view->displayFormInputSubmit('changeAgencyBtn', 'OK',
                                                        {'class' => 'button', 'id' => 'changeAgencyBtn'});

    $htmlChangeAgency .= '<span id="changeUserAgency">' . $locale->t('Aller sur l\'agence de') . '&nbsp;' . "\n";
    $htmlChangeAgency .= $selectChangeAgency . '&nbsp;' . "\n";
    $htmlChangeAgency .= $btnChangeAgency . "\n";
    $htmlChangeAgency .= '</span>' . "\n";
}

# affichage de l'agence de l'utilisateur
my $htmlAgency = '<b>' . $locale->t('Agence de') . '</b>';
$htmlAgency .= '&nbsp;<span id="nomadAgency">' . $user->{'nomadAgency.label'} . ' [' . $user->{'nomadAgency.id'} . ']</span>&nbsp;';
$htmlAgency .= $view->displayCountryImage($user->{'nomadCountry.id'});
$htmlAgency .= '&nbsp;&nbsp;&nbsp;&nbsp';

# bouton de raffraîchissement
my $btnRefresh = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png', 'refreshAlerts();');

# affichage du panel
print $view->displayControlPanel({'left' => [$htmlAgency, $htmlChangeAgency],
                                  'right' => [$btnRefresh],
                                  'substyle' => 'top'});
