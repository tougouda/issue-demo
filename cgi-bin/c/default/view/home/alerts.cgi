use utf8;
use open (':encoding(UTF-8)');

use strict;

our %myViewData;
our $view;
our $locale;

my $htmlAlerts = '';

my $alerts = $myViewData{'alerts'};
if (%$alerts > 0)
{
    $htmlAlerts .= '
<fieldset>
    <legend>' . $locale->t('Alertes') . '</legend>
    <div style="position: relative;">
        <div id="alertLoading" class="homeLoading"></div>
        <div id="alertContainer">';

    my $curGroup = '';
    while (my ($id, $alert) = each(%$alerts))
    {
        # changement de catégorie d'alerte
        if ($curGroup ne $alert->{'group.name'})
        {
            if ($curGroup ne '')
            {
                $htmlAlerts .=
                   '</div>
                </div>
            </div>';
            }
            $htmlAlerts .=
          '<div class="alertBlock">
                <div class="top">
                    <div class="title">' . $alert->{'group.name'} . '</div>
                </div>
                <div class="content">
                    <div class="alerts">';
        }

        my $className = '';
        if ($alert->{'count'} > 0)
        {
            $className = 'alert hot';
        }
        else
        {
            $className = 'alert';
        }

        $htmlAlerts .= '<a class="' . $className . '" href="'. $alert->{'url'} .'" target="_blank">' .
                            '<span class="count">' . $alert->{'count'} . '</span>' .
                            '<span class="title">' . $alert->{'label'} . '</span>' .
                       '</a>';

        $curGroup = $alert->{'group.name'};
    }
    $htmlAlerts .= '</div>
                </div>
            </div>
        </div>
    </div>
</fieldset>';
}

print $htmlAlerts;