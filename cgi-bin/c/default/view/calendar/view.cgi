use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
my $dir = 'default/calendar/';


our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'});


$view->setPageTitle($locale->t('Calendrier'));

# Feuilles de style
$view->addCSSSrc($dir . 'view.css');
# JS
$view->addJSSrc($dir . 'view.js');

$view->addBodyEvent('load',
                    'LOC_View.initCalendar("' . $tabViewData{'name'} . '", ' .
                                           '"' . $tabViewData{'currentDate'} . '", ' .
                                           $tabViewData{'yearMin'} . ', ' .
                                           $tabViewData{'yearMax'} . ',
                                           "' . $tabViewData{'pattern'} . '");');

# Langue
my $lang = lc(substr($tabViewData{'locale.id'}, 0, 2));
my @tabLangs = ('cn', 'cz', 'de', 'en', 'es', 'fr', 'it', 'jp', 'pl', 'pt', 'ro', 'ru');
if (!grep(/^$lang$/, @tabLangs))
{
    $lang = 'fr';
}

$view->addJSSrc($dir . 'jscal2/jscal2.js');
$view->addJSSrc($dir . 'jscal2/lang/' . $lang . '.js');
$view->addCSSSrc($dir . 'jscal2/jscal2.css');
$view->addCSSSrc($dir . 'jscal2/acces/acces.css');
$view->addCSSSrc($dir . 'jscal2/border-radius.css');


# Affichage de l'entête
print $view->displayHeader();


print '<div id="calendarContent"></div>';


# Affichage du pied de page
print $view->displayFooter();
