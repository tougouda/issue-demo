use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Json;

our %tabViewData;

my $view = LOC::Json->new();
print $view->displayHeader();

print $view->encode($tabViewData{'tabMachinesList'});
