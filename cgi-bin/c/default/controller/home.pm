use utf8;
use open (':encoding(UTF-8)');

package home;

use strict;

use File::Basename;
use LOC::Agency;
use LOC::Alert;
use LOC::Message;
use LOC::Util;
use LOC::Locale;
use LOC::Machine;

# Function: viewAction
# Prépare les données pour l'affichage de la page d'accueil
sub viewAction
{
    my $connAuth = &LOC::Db::getConnection('auth');

    # Variables pour la vue
    our %myViewData = ();

    # Récupération des variables du formulaire
    $myViewData{'isChangeAgency'}   = &LOC::Request::getString('changeAgencyBtn');
    $myViewData{'agencyId'}         = &LOC::Request::getString('agencyId');
    $myViewData{'isChangeProfile'}  = &LOC::Request::getString('changeProfileBtn');
    $myViewData{'profileId'}        = &LOC::Request::getString('profileId');

    # hachage de l'utilisateur
    $myViewData{'user'} = &LOC::Session::getUserInfos();

    # Session
    my $tabSession = &LOC::Session::getInfos();
    $myViewData{'timeZone'} = $tabSession->{'timeZone'};

    # Changement d'agence
    if ($myViewData{'isChangeAgency'} ne '')
    {
        # Mise à jour de l'agence nomade si elle fait partie des agences autorisées
        if ($myViewData{'user'}->{'tabAuthorizedAgencies'}->{$myViewData{'agencyId'}} ne undef)
        {
            &LOC::User::setNomadAgency($myViewData{'user'}->{'id'}, $myViewData{'agencyId'});
        }
        else
        {
            &LOC::User::setNomadAgency($myViewData{'user'}->{'id'}, $myViewData{'user'}->{'agency.id'});
        }

        # Rafraîchit les infos utilisateur
        $myViewData{'user'} = &LOC::Session::getUserInfos(1);
    }

    # Gestion du changement de profil
    if ($myViewData{'user'}->{'isAdmin'})
    {
        my $query = '
SELECT
    PROFILAUTO,
    CONCAT(LPAD(PROFILAUTO, 3, "000"), " - ", CONVERT(PROFILLIBELLE USING utf8))
FROM PROFIL
ORDER BY PROFILAUTO';
        $myViewData{'profiles'} = $connAuth->fetchPairs($query);

        # Changement de profil
        if ($myViewData{'isChangeProfile'} ne '')
        {
            &LOC::User::setProfile($myViewData{'user'}->{'id'}, $myViewData{'profileId'});

            # Rafraîchit les infos utilisateur
            $myViewData{'user'} = &LOC::Session::getUserInfos(1);
        }
    }

    # Récupération de la langue
    my $connFrm = &LOC::Db::getConnection('frmwrk');
    my $query = '
SELECT
    lcl_label AS `label`,
    lcl_cty_id AS `country.id`
FROM frmwrk.p_locale
WHERE lcl_id = "' . $myViewData{'user'}->{'locale.id'} . '";';
    my $result = $connFrm->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    $myViewData{'localeLabel'} = $result->{'label'};
    $myViewData{'localeCountryId'} = $result->{'country.id'};

    if ($myViewData{'user'}->{'isNomad'})
    {
        # Liste des agences autorisées
        $myViewData{'tabNomadAgencies'} = $myViewData{'user'}->{'tabAuthorizedAgencies'};
    }
    $myViewData{'menus'} = [&LOC::Session::getMenuList()];


    # Prochaines facturations
    $myViewData{'nextInvcDates'} = {'inter' => '', 'month' => ''};

    # Prochaine intermédiaire
    my $query = '
SELECT
    ivd_date
FROM p_invoicedate
WHERE ivd_date >= Now()
AND ivd_type = "I"
AND ivd_done = 0
AND ivd_cty_id = "' . $myViewData{'user'}->{'nomadCountry.id'} .'"
ORDER BY ivd_date
LIMIT 1;';
    $myViewData{'nextInvcDates'}{'inter'} = $connAuth->fetchOne($query);

    # Prochaine mensuelle
    my $query = '
SELECT
    ivd_date
FROM p_invoicedate
WHERE ivd_date >= Now()
AND ivd_type = "M"
AND ivd_done = 0
AND ivd_cty_id = "' . $myViewData{'user'}->{'nomadCountry.id'} . '"
ORDER BY ivd_date
LIMIT 1;';
    $myViewData{'nextInvcDates'}{'month'} = $connAuth->fetchOne($query);

    # Autorisation d'accès aux contrôles de pré-facturation
    $myViewData{'accessInvoicingControls'} = &LOC::Session::isAccessAuthorized('accounting', 'controls');

    # Alertes

    # Aletes Gestion des Locations
    my $locAlerts = &LOC::Alert::getList(LOC::Util::GETLIST_ASSOC,
                                         $myViewData{'user'}->{'nomadCountry.id'},
                                         $myViewData{'user'}->{'locale.id'},
                                         {'user' => $myViewData{'user'}->{'id'},
                                         'agency' => $myViewData{'user'}->{'nomadAgency.id'}});

    # Alertes CRM
    my $crmAlerts = &LOC::Alert::getCrmList(LOC::Util::GETLIST_ASSOC,
                                            $myViewData{'user'}->{'nomadCountry.id'},
                                            $myViewData{'user'}->{'locale.id'},
                                            {'user' => $myViewData{'user'}->{'id'}});

    # Merge les alertes
    tie(my %alerts, 'Tie::IxHash');
    %alerts = (%$locAlerts, %$crmAlerts);
    $myViewData{'alerts'} = \%alerts;


    # Url pour la recherche d'une machine
    $myViewData{'urlSrchMachine'} = &LOC::Request::createRequestUri('machine:machine:view');
    # Url pour la recherche dans l'historique client
    $myViewData{'urlSrchCustomer'} = &LOC::Globals::get('locationUrl') . '/cgi-bin/old/location/rech_rap.cgi';

    # Messages d'information
    $myViewData{'messages'} = &LOC::Message::getList(LOC::Util::GETLIST_ASSOC,
                                                     $myViewData{'user'}->{'locale.id'},
                                                     {'country' => $myViewData{'user'}->{'nomadCountry.id'}});

    # Environnement
    my $env = &LOC::Globals::get('env');
    if ($env  =~ m/^dev(.*)/)
    {
        $myViewData{'envLabel'} = 'Développement';
        $myViewData{'envStr'} = $1;
    }
    elsif ($env  =~ m/^int(.*)/)
    {
        $myViewData{'envLabel'} = 'Intégration';
        $myViewData{'envStr'} = $1;
    }
    elsif ($env  =~ m/^for(.*)/)
    {
        $myViewData{'envLabel'} = 'Formation';
        $myViewData{'envStr'} = $1;
    }
    else
    {
        $myViewData{'envLabel'} = '';
        $myViewData{'envStr'} = '';
    }
    $myViewData{'env'} = $env;
    $myViewData{'dev'} = &LOC::Globals::get('dev');

    # URL de l'application
    $myViewData{'locationUrl'} = &LOC::Globals::get('locationUrl') . '/cgi-bin/index.cgi';

    # Chemin d'accès aux scripts de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    $directory .= '/view/home/';
    $myViewData{'scriptPath'} = $directory;

    # Affichage de la vue
    require $myViewData{'scriptPath'} . 'view.cgi';
}

# Function: jsonAlertAction
# Prépare les alertes utilisateur pour l'encodage en JSON
sub jsonAlertAction
{
    # Variables pour la vue
    our %myViewData = ();

    # hachage de l'utilisateur
    my $user = &LOC::Session::getUserInfos();

    # locale
    my $locale = &LOC::Locale::getLocale($user->{'locale.id'});

    # récupère les alertes Gestion des Locations de l'utilisateur
    my $locAlerts = &LOC::Alert::getList(LOC::Util::GETLIST_ASSOC,
                                         $user->{'nomadCountry.id'},
                                         $user->{'locale.id'},
                                         {'user' => $user->{'id'}, 'agency' => $user->{'nomadAgency.id'}});

    # récupère les alertes CRM
    my $crmAlerts = &LOC::Alert::getCrmList(LOC::Util::GETLIST_ASSOC,
                                            $user->{'nomadCountry.id'},
                                            $user->{'locale.id'},
                                            {'user' => $user->{'id'}});

    my @alerts = (values %$locAlerts, values %$crmAlerts);

    # données à passer en JSON
    $myViewData{'values'} = {
                            'alerts' => \@alerts,
                            'user' => {
                                'nomadCountry.id'    => $user->{'nomadCountry.id'},
                                'nomadCountry.label' => $user->{'nomadCountry.label'},
                                'nomadAgency.id'     => $user->{'nomadAgency.id'},
                                'nomadAgency.label'  => $user->{'nomadAgency.label'}
                            }
    };

    # chemin d'accès aux scripts de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    $directory .= '/view/home/';

    # affichage de la vue
    require $directory . 'jsonAlert.cgi';
}

# Function: jsonMessageAction
# Prépare les messages d'informations pour l'encodage en JSON
sub jsonMessageAction
{
    # Variables pour la vue
    our %myViewData = ();

    # hachage de l'utilisateur
    my $user = &LOC::Session::getUserInfos();

    # locale
    my $locale = &LOC::Locale::getLocale($user->{'locale.id'});

    # Session
    my $tabSession = &LOC::Session::getInfos();

    # Messages d'information
    my $messages = &LOC::Message::getList(LOC::Util::GETLIST_ASSOC,
                                                     $user->{'locale.id'},
                                                     {'country' => $user->{'nomadCountry.id'}});

    # Formatage de la date des messages dans la locale de l'utilisateur
    if (%$messages > 0)
    {
        while (my ($id, $message) = each(%$messages))
        {
            $message->{'dateCreation'} = $locale->getDateFormat($message->{'dateCreation'},
                                                                LOC::Locale::FORMAT_DATETIME_NUMERIC,
                                                                $tabSession->{'timeZone'});
        }
    }

    # Passage des données à la vue
    $myViewData{'messages'} = $messages;

    # chemin d'accès aux scripts de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    $directory .= '/view/home/';

    # affichage de la vue
    require $directory . 'jsonMessage.cgi';
}

1;
