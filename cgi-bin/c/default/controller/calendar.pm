use utf8;
use open (':encoding(UTF-8)');

# Package: rentEstimate
# Exemple de contrôleur
package calendar;

use strict;
use File::Basename;

use LOC::Agency;
use LOC::Country;
use LOC::Customer;
use LOC::Estimate;
use LOC::Estimate::Rent;
use LOC::Tariff::Rent;
use LOC::Model;
use LOC::TariffFamily;
use LOC::User;
use LOC::Site;
use LOC::Characteristic;

# Function: viewAction
# Affichage du devis de location
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my @tabTime = localtime();

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'   => $tabUserInfos->{'locale.id'},
        'name'        =>  &LOC::Request::getString('name'),
        'yearMin'     =>  &LOC::Request::getInteger('yearMin', 2000),
        'yearMax'     =>  &LOC::Request::getInteger('yearMax', $tabTime[5] + 1900 + 3),
        'month'       =>  &LOC::Request::getInteger('month'),
        'year'        =>  &LOC::Request::getInteger('year'),
        'pattern'     =>  &LOC::Request::getString('pattern')
    );

    if ($tabViewData{'month'} eq '' || $tabViewData{'year'} eq '')
    {
        $tabViewData{'year'}  = $tabTime[5] + 1900;
        $tabViewData{'month'} = $tabTime[4] + 1;
    }

    $tabViewData{'currentDate'} = sprintf('%04s%02s%02s', $tabViewData{'year'}, $tabViewData{'month'}, 1);


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/calendar/view.cgi';
}


1;
