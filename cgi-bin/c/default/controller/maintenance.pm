use utf8;
use open (':encoding(UTF-8)');

# Contrôleur d'affichage des informations de maintenance.
package maintenance;


use strict;
use File::Basename;

use LOC::Globals;
use LOC::Maintenance;


# Function: infoAction
# Gère l'affichage de la page de maintenance.
#
# Parameters:
# hashref $tabParameters - Les paramètres.
sub infoAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Droits
    my $tabRights = &LOC::Maintenance::getRights($tabUserInfos);

    # Paramètres de la maintenance
    my $tabInfo = &LOC::Maintenance::getSettings();

    # Page autorisée pour la prévisualisation ou si le mode maintenance est activé
    if ($tabRights->{'preview'} || $tabInfo->{'isActivated'})
    {
        # Variables pour la vue
        our %tabViewData = (
            'locale.id'   => $tabUserInfos->{'locale.id'},
            'redirectUrl' => &LOC::Globals::get('locationUrl')
        );

        $tabViewData{'title'}                 = $tabInfo->{'title'};
        $tabViewData{'period'}                = $tabInfo->{'period'};
        $tabViewData{'pending'}->{'heading'}  = $tabInfo->{'pending'}->{'heading'};
        $tabViewData{'pending'}->{'message'}  = &LOC::Globals::replaceVariables($tabInfo->{'pending'}->{'message'});
        $tabViewData{'complete'}->{'heading'} = $tabInfo->{'complete'}->{'heading'};
        $tabViewData{'complete'}->{'message'} = &LOC::Globals::replaceVariables($tabInfo->{'complete'}->{'message'});
        $tabViewData{'checkUrl'}              = &LOC::Request::createRequestUri('default:maintenance:check');


        # Vue JSON ou HTML
        my $view = (&LOC::Request::isJsonRequested() ? 'json' : 'html');

        # Affichage de la vue
        my $directory = dirname(__FILE__);
        $directory =~ s/(.*)\/.+/$1/;
        require $directory . '/view/maintenance/info/' . $view . '.cgi';
    }
    else
    {
        &LOC::Browser::sendHeaders("Status: 403 Forbidden\n\n");
    }
}


# Function: infoAction
# Gère l'affichage de la page de maintenance.
#
# Parameters:
# hashref $tabParameters - Les paramètres.
sub checkAction
{
    my $tabParameters = $_[0];

    # Variables pour la vue
    our %tabViewData = (
        'isActivated' => &LOC::Maintenance::isActivated()
    );

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/maintenance/check.cgi';
}

1;