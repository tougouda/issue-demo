use utf8;
use open (':encoding(UTF-8)');

# Package: machine
# Contrôleur
package machine;

use strict;
use File::Basename;
use Tie::IxHash;

use LOC::Machine;


# Function: jsonListAction
# Récupérer la liste des machines (avec recherche possible)
#
sub jsonListAction
{
    # Variables pour la vue
    our %tabViewData = ();
    my $tabParameters = $_[0];
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $locale = &LOC::Locale::getLocale($tabUserInfos->{'locale.id'});

    # Récupération des paramètres
    my $searchValue      = &LOC::Util::trim(&LOC::Request::getString('searchValue', ''));
    my $format           = &LOC::Request::getInteger('format', 0);
    my $currentMachineId = &LOC::Request::getInteger('currentMachine', 0);

    if ($searchValue eq '')
    {
        $tabViewData{'tabMachinesList'} = {
            'returnCode' => 'error',
            'title' => $locale->t('Votre recherche est vide'),
        };
    }
    else
    {
        my $nbResult = &LOC::Machine::getList($tabUserInfos->{'nomadCountry.id'}, LOC::Util::GETLIST_COUNT, {'parkNumber' => $searchValue});

        if ($nbResult < 100)
        {
            if ($nbResult != 0)
            {
                my $result = &LOC::Machine::getList($tabUserInfos->{'nomadCountry.id'}, LOC::Util::GETLIST_ASSOC, {'parkNumber' => $searchValue});

                my $tab = [];
                if ($format == 0)
                {
                    foreach (keys %$result)
                    {
                        if ($result->{$_}->{'id'} != $currentMachineId)
                        {
                            push(@$tab, {'id'         => $result->{$_}->{'id'},
                                         'parkNumber' => $result->{$_}->{'parkNumber'}
                                        });
                        }
                    }
                }
                elsif ($format == 1)
                {
                    foreach (keys %$result)
                    {
                        if ($result->{$_}->{'id'} != $currentMachineId)
                        {
                            push(@$tab, [$result->{$_}->{'id'}, $result->{$_}->{'parkNumber'}]);
                        }
                    }
                }
                $tabViewData{'tabMachinesList'} = {'returnCode' => 'ok', 'format' => $format, 'tabData' => $tab};
            }
            else
            {
                $tabViewData{'tabMachinesList'} = {
                    'returnCode' => 'error',
                    'title' => $locale->t('Il n\'y a pas de résultat'),
                };
            }
        }
        else
        {
            $tabViewData{'tabMachinesList'} = {
                'returnCode' => 'warning' , 
                'title' => $locale->t('Vous avez plus de %d résultats', 100),
            };
        }
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/machine/jsonList.cgi';
}

1;
