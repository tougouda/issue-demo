use utf8;
use open (':encoding(UTF-8)');

# Package: user
# Contrôleur
package contract;

use strict;
use File::Basename;
use Tie::IxHash;

use LOC::Contract::Rent;
use LOC::Day;

use constant
{
    FORMAT_URL       => 0,
    FORMAT_PAIRS     => 1,
    FORMAT_DAYSINFOS => 2
};


# Function: jsonListAction
# Récupérer la liste des contrats (avec recherche possible)
#
sub jsonListAction
{
    # Variables pour la vue
    our %tabViewData = ();
    my $tabParameters = $_[0];
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $locale = &LOC::Locale::getLocale($tabUserInfos->{'locale.id'});

    # Récupération des paramètres
    my $searchValue     = &LOC::Util::trim(&LOC::Request::getString('searchValue', ''));
    my $format          = &LOC::Request::getInteger('format', 0);
    my $currentContract = &LOC::Request::getInteger('currentContract', 0);
    my $tabFilters      = &LOC::Request::getHash('filters', {});

    if ($searchValue eq '')
    {
        $tabViewData{'tabContractsList'} = {
                                            'returnCode' => 'error',
                                            'title' => $locale->t('Votre recherche est vide'),
                                           };
    }
    else
    {
        $tabFilters->{'code'} = '%' . $searchValue . '%';
        my $nbResult = &LOC::Contract::Rent::getList($tabUserInfos->{'nomadCountry.id'}, LOC::Util::GETLIST_COUNT, $tabFilters);

        if ($nbResult < 100)
        {
            if ($nbResult != 0)
            {
                my $result = &LOC::Contract::Rent::getList($tabUserInfos->{'nomadCountry.id'}, LOC::Util::GETLIST_ASSOC, $tabFilters);

                my $tab = [];
                if ($format == FORMAT_URL)
                {
                    foreach (keys %$result)
                    {
                        if ($result->{$_}->{'id'} != $currentContract)
                        {
                            push(@$tab, {'id'       => $result->{$_}->{'id'},
                                         'code'     => $result->{$_}->{'code'},
                                         'url'      => &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $result->{$_}->{'id'}})
                                  });
                        }
                    }
                }
                elsif ($format == FORMAT_PAIRS)
                {
                    foreach (keys %$result)
                    {
                        if ($result->{$_}->{'id'} != $currentContract)
                        {
                            push(@$tab, [$result->{$_}->{'id'}, $result->{$_}->{'code'}]);
                        }
                    }
                }
                elsif ($format == FORMAT_DAYSINFOS)
                {
                    foreach (keys %$result)
                    {
                        if ($result->{$_}->{'id'} != $currentContract)
                        {
                            push(@$tab, &LOC::Day::getAppliedToContractInfos($result->{$_}->{'country.id'}, $result->{$_}->{'id'}));
                        }
                    }
                }
                $tabViewData{'tabContractsList'} = {'returnCode' => 'ok', 'format' => $format, 'tabData' => $tab};
            }
            else
            {
                $tabViewData{'tabContractsList'} = {
                                                'returnCode' => 'error',
                                                'title' => $locale->t('Il n\'y a pas de résultat'),
                                               };
            }
        }
        else
        {
            $tabViewData{'tabContractsList'} = {
                                            'returnCode' => 'warning' ,
                                            'title' => $locale->t('Vous avez plus de %d résultats', 100),
                                           };
        }
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/contract/jsonList.cgi';
}

1;
