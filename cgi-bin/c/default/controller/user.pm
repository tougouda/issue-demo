use utf8;
use open (':encoding(UTF-8)');

# Package: user
# Contrôleur
package user;

use strict;
use File::Basename;
use Tie::IxHash;

use LOC::User;


# Function: jsonListAction
# Récupérer la liste des utilisateurs (avec recherche possible)
#
sub jsonListAction
{
    # Variables pour la vue
    our %tabViewData = ();
    my $tabParameters = $_[0];
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $locale = &LOC::Locale::getLocale($tabUserInfos->{'locale.id'});

    # Récupération des paramètres
    my $searchValue  = &LOC::Util::trim(&LOC::Request::getString('searchValue', ''));
    my $format       = &LOC::Request::getInteger('format', 0);

    if ($searchValue eq '')
    {
        $tabViewData{'tabUsersList'} = {
                                            'returnCode' => 'error',
                                            'title' => $locale->t('Votre recherche est vide'),
                                           };
    }
    else
    {
        # Récupération de la liste des utilisateurs
        my $nbResult = &LOC::User::getList(LOC::Util::GETLIST_COUNT,
                                           {'fullName' => $searchValue},
                                           LOC::User::GET_ACTIVE,
                                           {'formatMode' => 1});

        if ($nbResult < 100)
        {
            if ($nbResult != 0)
            {
                my $result = &LOC::User::getList(LOC::Util::GETLIST_ASSOC,
                                                 {'fullName' => $searchValue},
                                                 LOC::User::GET_ACTIVE,
                                                 {'formatMode' => 1});

                my $tab = [];
                if ($format == 0)
                {
                    foreach (keys %$result)
                    {
                        push(@$tab, {'id'       => $result->{$_}->{'id'},
                                     'fullName' => $result->{$_}->{'fullName'}});
                    }
                }
                elsif ($format == 1)
                {
                    foreach (keys %$result)
                    {
                        push(@$tab, [$result->{$_}->{'id'}, $result->{$_}->{'fullName'}]);
                    }
                }
                $tabViewData{'tabUsersList'} = {'returnCode' => 'ok', 'format' => $format, 'tabData' => $tab};
            }
            else
            {
                $tabViewData{'tabUsersList'} = {
                                                'returnCode' => 'error',
                                                'title' => $locale->t('Il n\'y a pas de résultat'),
                                               };
            }
        }
        else
        {
            $tabViewData{'tabUsersList'} = {
                                            'returnCode' => 'warning' , 
                                            'title' => $locale->t('Vous avez plus de %d résultats', 100),
                                           };
        }
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/user/jsonList.cgi';
}

1;
