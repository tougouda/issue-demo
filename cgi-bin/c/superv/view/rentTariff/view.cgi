use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'superv/rentTariff/';


our %tabViewData;
our $inputsTabIndex = 1;


our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}

# URL courante
my $currentUrl = $view->createURL('superv:rentTariff:view');


# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'view.css');

# jQuery
$view->addJSSrc('jQuery/jquery.js');
# js
$view->addJSSrc($dir . 'view.js');

$view->addPackage('popups');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);


my $title = $locale->t('Tarification location');
$view->setPageTitle($title);
$view->setTitle($title);

my $fatalErrorMsg = htmllocale('Erreur fatale !') .
    '<p class="details">' .
    htmllocale('Merci de contacter le support informatique.') .
    '</p>';
# Messages de retour
my $tabTranslations = {
    'fatalError'             => $fatalErrorMsg,
    'rightsError'            => $locale->t('Certaines informations ne sont pas modifiables'),
};


my $currencySymbol = $tabViewData{'currencySymbol'};

# Bouton "Actualiser"
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png',
                                          $currentUrl, $locale->t('Actualiser la page'));
$view->addControlsContent([$refreshBtn], 'right', 1);

my $errorBlockRefreshBtn = '<span onclick="$ge(\'errorBlock\').style.display = \'none\';">' . $refreshBtn . '</span>';
my $fatalErrorMsg = htmllocale('Erreur fatale !') .
    '<p class="details">' .
    $locale->t('Veuillez %s la page et renouveler l\'opération.', $errorBlockRefreshBtn) . '<br>' .
    htmllocale('Si l\'erreur se reproduit, merci de contacter le support informatique.') .
    '</p>';


# Bouton d'extraction excel
my $excelUrl = $view->createURL('superv:rentTariff:view', {'displayType' => 'xls'});
my $excelBtn = $view->displayTextButton($locale->t('Extraction Excel'), 'excel(|Over).gif',
                                        $excelUrl,
                                        $locale->t('Extraction au format Excel'));

$view->addControlsContent($excelBtn, 'right', 0);

# Messages de retour
my $tabTranslations = {
    'fatalError'  => $fatalErrorMsg,
    'rightsError' => $locale->t('Certaines informations ne sont pas modifiables.')
};

# Message de confirmation de validation avec succès
if ($tabViewData{'return'} && @{$tabViewData{'return'}->{'success'}} > 0 && @{$tabViewData{'return'}->{'errors'}} == 0)
{
    my $validBlock = '
<div id="validBlock" class="messageBlock valid" >
    <div class="popup">
        <div class="content">
            <table>
                <tbody>
                    <tr>
                        <td style="font-weight: bold; vertical-align: top;">' . $locale->t('Les tarifs ont été mis à jour') . '.</td>
                    </tr>
                    <tr class="buttons">
                        <td>' .
                        $view->displayTextButton($locale->t('Ok'), '',
                                                 '$("#validBlock").hide();',
                                                 '', 'l', {'id' => 'validBlockBtn'}) .
                        '</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>';

    $view->addBodyContent($validBlock, 0, 1);
    $view->addBodyEvent('load', '$("#validBlockBtn").focus();');
}

# Messages d'erreurs
if ($tabViewData{'return'} && @{$tabViewData{'return'}->{'errors'}} > 0)
{
    my $errorBlock = '
<div id="errorBlock" class="messageBlock error">
    <div class="popup">
        <div class="content">
            <table>
                <tbody>
                    <tr>
                        <td style="vertical-align: top;">
                            <p style="font-weight : bold;">'
                            . $locale->t('Des erreurs ont été détectées :')
                            . '</p>
                            <ul>';
    #Erreurs retournées depuis PERL
    foreach my $errorInfos (@{$tabViewData{'return'}->{'errors'}})
    {
       $errorBlock .= '<li>' . $locale->t($tabTranslations->{$errorInfos->{'error'}}) . '</li>';
    }

    $errorBlock .= '
                            </ul>
                        </td>
                    </tr>
                    <tr class="buttons">
                        <td>' .
                        $view->displayTextButton($locale->t('Fermer'), '',
                                                 '$("#errorBlock").hide();',
                                                 '', 'l', {'id' => 'errorBlockBtn'}) .
                        '</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>';

    $view->addBodyContent($errorBlock, 0, 1);
    $view->addBodyEvent('load', '$("#errorBlockBtn").focus();');
}


# Affichage de l'entête
print $view->displayHeader();

print $view->startForm();

# Filtres
# -- sur les familles tarifaires
my $tariffFamilyFilter = $view->displayFormInputText('tariffFamily-filter', $tabViewData{'filter'}, undef,
                                                    {'placeholder' => $locale->t('Famille tarifaire')});

my $tabCfgsFilters = [
    # Tarif de base
    {
        'name'      => 'baseTariff',
        'label'     => $locale->t('le tarif de base')
    },
    # Prix pricing
    {
        'name'      => 'pricingPrice',
        'label'     => $locale->t('le prix pricing')
    },
    # Prix de revient
    {
        'name'      => 'costPrice',
        'label'     => $locale->t('le prix de revient')
    }
];
my @tabFilters;
foreach my $filter (@$tabCfgsFilters)
{
    push(@tabFilters, $view->displayFormInputCheckBox($filter->{'name'} . '-filter', $tabViewData{$filter->{'name'} . '-filter'},
                                                       {'id' => $filter->{'name'} . '-filter'},
                                                       $filter->{'label'}, 1));
}

print '
<div id="filters">'
    . $tariffFamilyFilter . '
    <span class="chb-filter">' . $locale->t('Afficher : ') . join('', @tabFilters) . '
</div>';


print '
<table id="rentTariff" class="basic">
    <thead>
        <tr>
            <th>Libellé</th>
            <th>Sous-libellé</th>';

foreach my $durationRange (@{$tabViewData{'durationRangesList'}})
{
    print '
            <th class="duration-range label" colspan="3">' . $durationRange->{'shortLabel'} . '</th>';
}

print '
        </tr>
        <tr>
            <th></th>
            <th></th>';

foreach my $durationRange (@{$tabViewData{'durationRangesList'}})
{
    print '
            <th class="duration-range-sub tb" title="' . $locale->t('Tarif de base') . '">' . $locale->t('TB') . '</th>
            <th class="duration-range-sub pp" title="' . $locale->t('Prix pricing') . '">' . $locale->t('PP') . '</th>
            <th class="duration-range-sub pr" title="' . $locale->t('Prix de revient') . '">' . $locale->t('PR') . '</th>';
}
print '
        </tr>
    </thead>
    <tbody>';

my $line = 0;
my $labelTemp = '';

my $isBaseTariffDisabled    = !$tabViewData{'tabRights'}->{'actions'}->{'baseTariff'};
my $isPricingPriceDisabled  = !$tabViewData{'tabRights'}->{'actions'}->{'pricingPrice'};
my $isCostPriceDisabled  = !$tabViewData{'tabRights'}->{'actions'}->{'costPrice'};

foreach my $tariffFamilyInfos (@{$tabViewData{'tabTariffFamily'}})
{
    my @class;
    push (@class, ($line % 2 ? 'even' : 'odd'));

    # Erreur ou succès d'enregistrement
    if ($tabViewData{'return'})
    {
        if (&LOC::Util::in_array($tariffFamilyInfos->{'id'}, $tabViewData{'return'}->{'success'}))
        {
            push(@class, 'isSuccess');
        }

        if (grep {$_->{'id'} == $tariffFamilyInfos->{'id'}} @{$tabViewData{'return'}->{'errors'}})
        {
            push(@class, 'isError');
        }
    }

    if ($labelTemp ne $tariffFamilyInfos->{'label'})
    {
        $labelTemp = $tariffFamilyInfos->{'label'};
        push (@class, 'sep');
    }
    print '
        <tr id="line_' . $tariffFamilyInfos->{'id'} . '" data-content="' . $tariffFamilyInfos->{'label'} . '" class="' . join(' ', @class) . '">
            <td>' . $tariffFamilyInfos->{'label'} . '</td>
            <td class="sublabel">' . $tariffFamilyInfos->{'sublabel'} . '</td>';

    foreach my $durationRange (@{$tabViewData{'durationRangesList'}})
    {
        # Recherche du tarif correspondant
        my @tariffInfos = grep {$_->{'durationRange.id'} == $durationRange->{'id'}} @{$tariffFamilyInfos->{'tabTariffs'}};

        my $baseTariffInput = $view->displayFormInputCurrency('amount_' . $tariffFamilyInfos->{'id'} . '_' . $tariffInfos[0]->{'durationRange.id'},
                                                         $tariffInfos[0]->{'amount'},
                                                         LOC::Html::CURRENCYMODE_INPUT,
                                                         $currencySymbol, undef,
                                                         {'style' => 'width: 35px;', 'disabled' => $isBaseTariffDisabled});

        my $pricingPriceInput = $view->displayFormInputCurrency('pricingPrice_' . $tariffFamilyInfos->{'id'} . '_' . $tariffInfos[0]->{'durationRange.id'},
                                                         $tariffInfos[0]->{'pricingPrice'},
                                                         LOC::Html::CURRENCYMODE_INPUT,
                                                         $currencySymbol, undef,
                                                         {'style' => 'width: 35px;', 'disabled' => $isPricingPriceDisabled});

        my $costPriceInput = $view->displayFormInputCurrency('costPrice_' . $tariffFamilyInfos->{'id'} . '_' . $tariffInfos[0]->{'durationRange.id'},
                                                         $tariffInfos[0]->{'costPrice'},
                                                         LOC::Html::CURRENCYMODE_INPUT,
                                                         $currencySymbol, undef,
                                                         {'style' => 'width: 35px;', 'disabled' => $isCostPriceDisabled});
        print '
            <td class="duration-range tb">' . $baseTariffInput . '</td>
            <td class="duration-range pp">' . $pricingPriceInput . '</td>
            <td class="duration-range pr">' . $costPriceInput . '</td>';
    }

    print '
        </tr>';

    $line++;
}

print '
    </tbody>

</table>';

# Hidden
print $view->displayFormInputHidden('valid', '');

# Bouton de validation
my $validBtn = $view->displayTextButton($locale->t('Valider'), 'valid(|Over).png', 'tariffs.valid()', $locale->t('Valider'));

#Affichage du Panel du bas
print $view->displayControlPanel({'left' => [$validBtn], 'substyle' => 'bottom'});


print $view->endForm();

# Variables JS et initialisation
print $view->displayJSBlock('
tariffs.init(' . &LOC::Json::toJson($tabViewData{'tabTariffFamily'}) . ', "pricingPrice");');






# Affichage du pied de page
print $view->displayFooter();
