use utf8;
use open(':encoding(UTF-8)');

use strict;

use Spreadsheet::WriteExcel;

use LOC::Locale;
use LOC::Browser;

# Répertoire
my $dir = 'superv/rentTariff.';

our %tabViewData;

my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});


# Titre de l'extraction
my $title = $locale->t('Tarification Location');


# Téléchargement du fichier
my $fileName = $title . '_' . POSIX::strftime('%Y%m%d_%H%M', localtime()) . '.xls';
$fileName =~ tr/"<>&\/\\?#\*;/'{}+____\-\-/;

&LOC::Browser::sendHeaders(
    "Content-Type: application/force-download\n" .
    "Content-Disposition: attachment; filename=\"" . $fileName . "\"\n" .
    "\n"
);

# Nouveau classeur Excel envoyé sur la sortie standard
my $workbook = Spreadsheet::WriteExcel->new('-');
my $author = $locale->t('Gestion des locations') . ' ' . &LOC::Globals::get('version');
my $company = 'Accès industrie';
$workbook->set_properties('title' => $title, 'author' => $author, 'company' => $company);

# Légende
my $legendFmt = $workbook->add_format();
$legendFmt->set_italic();
$legendFmt->set_size(8);
$legendFmt->set_text_wrap();


# Format de la ligne d'en-tête
my $headerFmt = $workbook->add_format();
$headerFmt->set_bold();
$headerFmt->set_bg_color($workbook->set_custom_color(12, 83, 141, 213));
$headerFmt->set_color('white');
$headerFmt->set_align('center');
$headerFmt->set_right();
$headerFmt->set_left();

# Format monétaire
my $currencyFmt = $workbook->add_format();
$currencyFmt->set_align('right');
$currencyFmt->set_num_format('#,##0.00 €');

# Couleur par tranches
my $currencyColorFmt = $workbook->add_format();
$currencyColorFmt->copy($currencyFmt);
$currencyColorFmt->set_bg_color($workbook->set_custom_color(13, 197, 217, 241));

# Création des feuilles et de leurs contenus
my $worksheet = $workbook->add_worksheet($locale->t('Tarification location'));
$worksheet->activate();

# Entête et pied de page pour l'impression
$worksheet->set_header('&C&12&",Bold"' . $title);
$worksheet->set_footer('&C&8' . $locale->t('Page') . ' &P / &N');

# Figer les volets
$worksheet->freeze_panes(3, 2);

my $c = 0;
my $l = 0;

# Légende
$worksheet->write_string($l, $c, $locale->t('TB : Tarif de base') . "\n" . 
                                 $locale->t('PP : Prix pricing') .  "\n" . 
                                 $locale->t('PR : Prix de revient'), $legendFmt);

$l++;

# Ligne d'en-tête
$worksheet->set_column($c, $c, 20);
$worksheet->write_string($l, $c++, $locale->t('Libellé'), $headerFmt);
$worksheet->set_column($c, $c, 20);
$worksheet->write_string($l, $c++, $locale->t('Sous-libellé'), $headerFmt);


foreach my $durationRange (@{$tabViewData{'durationRangesList'}})
{
    $worksheet->write_string($l, $c, $durationRange->{'shortLabel'}, $headerFmt);
    $worksheet->set_column($c, $c, 10);
    $worksheet->merge_cells($l, $c, $l, $c + 2);
    $worksheet->write_blank($l, $c + 1, $headerFmt);
    $worksheet->write_blank($l, $c + 2, $headerFmt);
    $c += 3;
}
$l++;

$c = 0;
$worksheet->write_blank($l, $c++, $headerFmt);
$worksheet->write_blank($l, $c++, $headerFmt);
# - Types de tarif
foreach my $durationRange (@{$tabViewData{'durationRangesList'}})
{
    $worksheet->write_string($l, $c++, $locale->t('TB'), $headerFmt);
    $worksheet->write_string($l, $c++, $locale->t('PP'), $headerFmt);
    $worksheet->write_string($l, $c++, $locale->t('PR'), $headerFmt);
}

$l++;

# Affichage des données
foreach my $tariffFamilyInfos (@{$tabViewData{'tabTariffFamily'}})
{
    $c = 0;
    $worksheet->write_string($l, $c++, $tariffFamilyInfos->{'label'});
    $worksheet->write_string($l, $c++, $tariffFamilyInfos->{'sublabel'});

    my $cpt = 0;
    foreach my $durationRange (@{$tabViewData{'durationRangesList'}})
    {
        my $format = ($cpt ? $currencyFmt : $currencyColorFmt);
        my $formatBorder = $workbook->add_format();
        $formatBorder->copy($format);
        $formatBorder->set_right();

        # Recherche du tarif correspondant
        my @tariffInfos = grep {$_->{'durationRange.id'} == $durationRange->{'id'}} @{$tariffFamilyInfos->{'tabTariffs'}};

        $worksheet->write_number($l, $c++, $tariffInfos[0]->{'amount'}, $format);
        $worksheet->write_number($l, $c++, $tariffInfos[0]->{'pricingPrice'}, $format);
        $worksheet->write_number($l, $c++, $tariffInfos[0]->{'costPrice'}, $formatBorder);
        $cpt = !$cpt;
    }

    $l++;
}


# Fermeture du classeur
$workbook->close();
