use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'superv/telematics/';


our %tabViewData;
our $inputsTabIndex = 1;


our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}

# URL courante
my $currentUrl = $view->createURL('superv:telematics:view');


# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'view.css');

# jQuery
$view->addJSSrc('jQuery/jquery.js');
# js
$view->addJSSrc($dir . 'view.js');

$view->addPackage('popups');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);


my $title = $locale->t('Gestion des codes statiques');
$view->setPageTitle($title);
$view->setTitle($title);



# Bouton "Actualiser"
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png',
                                          $currentUrl, $locale->t('Actualiser la page'));
$view->addControlsContent([$refreshBtn], 'right', 1);

my $errorBlockRefreshBtn = '<span onclick="$ge(\'errorBlock\').style.display = \'none\';">' . $refreshBtn . '</span>';
my $fatalErrorMsg = htmllocale('Erreur fatale !') .
    '<p class="details">' .
    $locale->t('Veuillez %s la page et renouveler l\'opération.', $errorBlockRefreshBtn) . '<br>' .
    htmllocale('Si l\'erreur se reproduit, merci de contacter le support informatique.') .
    '</p>';

# Messages de retour
my $tabTranslations = {
  'fatalError'              => $fatalErrorMsg,
  'rightsError'             => $locale->t('Vous ne disposez pas des droits nécessaires pour régénérer ce code'),
  'unknownError'            => $locale->t('Erreur inconnue'),
  'alreadyRegeneratedError' => $locale->t('Vous avez déjà régénéré ce code.<br>Veuillez cliquer sur le bouton %s.', $refreshBtn),
  'success'                 => $locale->t('Le code a été régénéré')
};

# Message de confirmation de validation avec succès
if ($tabViewData{'tabData'}->{'@valid'})
{
    my $validBlock = '
<div id="validBlock" class="messageBlock valid" >
    <div class="popup">
        <div class="content">
            <table>
                <tbody>
                    <tr>
                        <td style="font-weight: bold; vertical-align: top;">' . $tabTranslations->{$tabViewData{'tabData'}->{'@valid'}} . '.</td>
                    </tr>
                    <tr class="buttons">
                        <td>' .
                        $view->displayTextButton($locale->t('Ok'), '',
                                                 '$("#validBlock").hide();',
                                                 '', 'l', {'id' => 'validBlockBtn'}) .
                        '</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>';

    $view->addBodyContent($validBlock, 0, 1);
    $view->addBodyEvent('load', '$("#validBlockBtn").focus();');
}

# Messages d'erreurs
if ($tabViewData{'tabData'}->{'@errors'})
{
    my $errorBlock = '
<div id="errorBlock" class="messageBlock error">
    <div class="popup">
        <div class="content">
            <table>
                <tbody>
                    <tr>
                        <td style="vertical-align: top;">
                            <p style="font-weight : bold;">'
                            . $locale->t('La régénération du code n\'a pas été effectuée car les erreurs suivantes ont été détectées')
                            . '</p>
                            <ul>';
    #Erreurs retournées depuis PERL
    while (my ($index, $errorCode) = each(@{$tabViewData{'tabData'}->{'@errors'}}))
    {
       $errorBlock .= '<li>' . $locale->t($tabTranslations->{$errorCode}) . '</li>';
    }

    $errorBlock .= '
                            </ul>
                        </td>
                    </tr>
                    <tr class="buttons">
                        <td>' .
                        $view->displayTextButton($locale->t('Fermer'), '',
                                                 '$("#errorBlock").hide();',
                                                 '', 'l', {'id' => 'errorBlockBtn'}) .
                        '</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>';

    $view->addBodyContent($errorBlock, 0, 1);
    $view->addBodyEvent('load', '$("#errorBlockBtn").focus();');
}


# Affichage de l'entête
print $view->displayHeader();

print $view->displayJSBlock('
$(function()
{
    $(".pendingBtn").click(function() {
        $(this).parents("fieldset")
               .find(".pending-machines, .more, .less").toggleClass("hide");
    });
});');

print $view->startForm();


print '
<div class="codes-container">';


foreach my $tabCodeTypeInfos (values %{$tabViewData{'tabTypesList'}})
{
    my $tabCodeInfos = $tabViewData{'tabList'}->{$tabCodeTypeInfos->{'id'}};


    my $regenerateBtn = '';
    if ($tabViewData{'tabRights'}->{'actions'}->{'renew'})
    {
        my $label = ($tabCodeInfos ? htmllocale('Régénérer') : htmllocale('Générer'));
        my $title = ($tabCodeInfos ? htmllocale('Régénérer le code') : htmllocale('Générer le code'));

        $regenerateBtn = $view->displayTextButton($label, 'refresh(|Over).png',
                                                  'submitForm(' . $tabCodeTypeInfos->{'id'} . ', ' . $tabCodeInfos->{'startCodeId'} . ')',
                                                  $title);
    }

    my $pendingBtn = $view->displayTextButton('', 'arrow-left.png',
                                              '',
                                              $locale->t('Voir les machines concernées'), 'left', {'class' => 'pendingBtn'});

    my $pendingBtnLess = $view->displayTextButton('', 'arrow-down.png',
                                                  '',
                                                  $locale->t('Replier les machines concernées'), 'left', {'class' => 'pendingBtn'});



    print '
    <fieldset>
        <legend>' . htmllocale($tabCodeTypeInfos->{'name'}) .'</legend>

        <div class="align-center">';

    if ($tabCodeInfos)
    {
        print '
            <span class="startcode">' . $tabCodeInfos->{'code'} . '</span>';
    }

    if ($regenerateBtn ne '')
    {
        print '
            <div class="btn-regenerate">' . $regenerateBtn . '</div>';
    }

    print '
        </div>';


    if ($tabCodeInfos)
    {
        my $total = $tabCodeInfos->{'count'}->{'active'} + $tabCodeInfos->{'count'}->{'pending'};
        my $tabPercent = {
            'active'  => ($total > 0 ? LOC::Util::round($tabCodeInfos->{'count'}->{'active'} * 100 / $total, 0) : 0),
            'pending' => ($total > 0 ? LOC::Util::round($tabCodeInfos->{'count'}->{'pending'} * 100 / $total, 0) : 0),
        };

        print '

        <div class="resume">

            <div class="stat-bar">
                <span class="active" style="width: ' . $tabPercent->{'active'} . '%;"></span>
                <span class="pending" style="width: ' . $tabPercent->{'pending'} . '%;"></span>
            </div>

            <div class="legend">
                <p class="active">' . htmllocale('Codes actifs') . ' : ' . $tabCodeInfos->{'count'}->{'active'} . ' / ' . $total . '</p>
                <p class="pending">' . htmllocale('Codes en cours d\'envoi') . ' : ' . $tabCodeInfos->{'count'}->{'pending'} . ' / ' . $total;
        if ($tabCodeInfos->{'count'}->{'pending'} > 0)
        {
            print '
                    <span class="more">' . $pendingBtn . '</span>
                    <span class="less hide">' . $pendingBtnLess . '</span>';
        }

        print '
                </p>
            </div>
        </div>

       <div class="pending-machines hide">
            <table class="standard">
                <thead>
                    <tr>
                        <th>' . htmllocale('N° parc') . '</td>
                        <th>' . htmllocale('Code actif') . '</td>
                    </tr>
                </thead
                <tbody>';

        # Affichage des machines dont le cours est en cours de transmission
        foreach my $parkNumber (@{$tabCodeInfos->{'pendingParkNumbers'}})
        {
            my $currentCode = (defined $tabCodeInfos->{'pending'}->{$parkNumber} ? $tabCodeInfos->{'pending'}->{$parkNumber} : undef);
            print '
                    <tr>
                        <td>' . $view->displayParkNumber($parkNumber) .'</td>
                        <td>';
            if ($currentCode)
            {
                print '
                            <span class="startcode">' . $currentCode . '</span>';
            }
            print '
                        </td>
                    </tr>';
        }

        print '
                </tbody>
            </table>
        </div>';
    }

    print '
    </fieldset>';
}

print '
</div>';

print $view->displayFormInputHidden('idToRegenerate', undef);
print $view->displayFormInputHidden('startCodeIdToRegenerate', undef);


print $view->endForm();
# Affichage du pied de page
print $view->displayFooter();
