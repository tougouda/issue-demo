use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'superv/machinesFamily/';


our %tabViewData;
our $inputsTabIndex = 1;


our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});


# URL courante 
my $currentUrl = $view->createURL('superv:machinesFamily:view');


# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'view.css');

# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc('jQuery/plugins/tablesorter.js');

$view->addPackage('popups');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);


my $title = $locale->t('Familles de machines');
$view->setPageTitle($title);
$view->setTitle($title);


# Bouton "Ajouter"
my $addUrl = $view->createURL('superv:machinesFamily:manage');
my $addBtn= $view->displayTextButton($locale->t('Ajouter'), 'add(|Over).png',
                                     'Location.popupsManager.getPopup(\'machinesFamily\').open(\'' . $addUrl . '\');
                                     Location.popupsManager.getPopup(\'machinesFamily\').setPosition(\'center\', \'center\');',
                                     $locale->t('Ajouter une famille de machines'));

$view->addControlsContent($addBtn, 'left', 1);

# Bouton "Actualiser"
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png',
                                          $currentUrl, $locale->t('Actualiser la page'));
$view->addControlsContent([$refreshBtn], 'right', 1);


# Affichage de l'entête
print $view->displayHeader();
print $view->displayJSBlock('Location.popupsManager.createPopup("machinesFamily", "", "center", "center", 500, 320);');


# On n'affiche pas le tableau si il est vide
my @tabMachinesFamily = values %{$tabViewData{'tabMachinesFamily'}};
if (@tabMachinesFamily == 0)
{
    # Pas de résultat
    print '<div class="noResult">' . $view->displayMessages('info', [$locale->t('Aucune famille de machines existante')], 0) . '</div>';
}
else
{
    # Options pour tablesorter
    my $headersOption = '
6 : {sorter: false}'
;
    my $sortListOption = '[[1,0]]'; # Tri par défaut: "Libellé de la famille de machines" (ASC)

    print '
<table class="basic machinesFamily">
<thead>
    <tr>
        <th class="id">' . $locale->t('Id.') . '</th>
        <th class="label">' . $locale->t('Libellé') . '</th>
        <th class="energy">' . $locale->t('Énergie') . '</th>
        <th class="elevation">' . $locale->t('Élévation') . '</th>
        <th class="businessFamily">' . $locale->t('Famille commerciale') . '</th>
        <th class="accountFamily">' . $locale->t('Famille comptable') . '</th>
        <th class="edit"></th>
    </tr>
</thead>
<tbody>';

    # Affichage des lignes
    my $lineNo = 0;
    foreach my $tabMachinesFamilyInfos (@tabMachinesFamily)
    {
        my $editUrl = $view->createURL('superv:machinesFamily:manage', {'id' => $tabMachinesFamilyInfos->{'id'}});
        my $editBtn = $view->displayTextButton('', 'edit(|Over).png',
                                               'Location.popupsManager.getPopup(\'machinesFamily\').open(\'' . $editUrl . '\');
                                               Location.popupsManager.getPopup(\'machinesFamily\').setPosition(\'center\', \'center\');',
                                               $locale->t('Modifier la famille de machines'));

        my $lineClass = ($lineNo % 2 ? 'even' : 'odd');

        print '
    <tr class="' . $lineClass . '">
        <td class="id small">' . $tabMachinesFamilyInfos->{'id'} . '</td>
        <td class="label">' . $locale->t($tabMachinesFamilyInfos->{'label'}) . '</td>
        <td class="energy">' . $locale->t($tabMachinesFamilyInfos->{'energy'}) . '</td>
        <td class="elevation"><div>' . $locale->t($tabMachinesFamilyInfos->{'elevation'}) . '</div></td>
        <td class="businessFamily">' . $tabMachinesFamilyInfos->{'businessFamily.label'} . '</a></td>
        <td class="accountFamily">' . $tabMachinesFamilyInfos->{'accountFamily.label'} . '</td>
        <td class="edit">' . $editBtn . '</td>
    </tr>';

        $lineNo++;
    }

    print '
</tbody>
</table>';

    # Tri du tableau
    print $view->displayJSBlock('
$(function() 
{ 
    $("table.basic.machinesFamily").tablesorter({
        headers: {' . $headersOption . '},
        sortList: ' . $sortListOption . ',
        widgets: ["zebra"]
    });
}
);');
}


# Affichage du pied de page
print $view->displayFooter();
