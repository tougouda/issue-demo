use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'superv/machinesFamily/';


our %tabViewData;
our $inputsTabIndex = 1;


our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});


# URL courante 
my $currentUrl = $view->createURL('superv:machinesFamily:manage');


# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'manage.css');

# Scripts JS
# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc($dir . 'manage.js');

$view->addPackage('spincontrols');

# On a un identifiant de famille de machines
my $isNew = ($tabViewData{'id'} ? 0 : 1); # Ajout ou modification

my $title = ($isNew ? $locale->t('Ajouter une famille de machines') : $locale->t('Modifier une famille de machines'));
$view->setPageTitle($title);
$view->setTitle($title);

# Affichage de l'entête
print $view->displayHeader();


# Début du formulaire
print $view->startForm(undef, undef, 'manageForm');


# Tableau des retours
my $tabReturn = {
    'valid' => {
        0 => $locale->t('Famille de machines modifiée avec succès'),
        1 => $locale->t('Famille de machines ajoutée avec succès')
    },
    'error' => {
        0  => $locale->t('Erreur inconnue'),
        1  => $locale->t('Erreur fatale ! Veuillez contacter le service informatique s.v.p.'),
        2  => $locale->t('Tous les champs obligatoires ne sont pas renseignés')
    }
};

# Affichage des messages
if ($tabViewData{'tabData'})
{
    my @msg;
    foreach my $code (@{$tabViewData{'tabData'}->{'@return'}})
    {
        push(@msg, $tabReturn->{$tabViewData{'tabData'}->{'@state'}}->{$code});
    }
    print $view->displayMessages($tabViewData{'tabData'}->{'@state'}, \@msg, 0);
}

# - Éléments du formulaire
my $requiredSymbol = $view->displayRequiredSymbol();

my @businessFamilyList = ({'value' => 0, 'innerHTML' => '-- ' . $locale->t('Sélectionner') . ' --'});
foreach my $businessFamilyId (keys(%{$tabViewData{'businessFamilyList'}}))
{
    push(@businessFamilyList, {
            'value' => $businessFamilyId,
            'innerHTML' => $tabViewData{'businessFamilyList'}->{$businessFamilyId}
        });
}
my @accountFamilyList = ({'value' => 0, 'innerHTML' => '-- ' . $locale->t('Sélectionner') . ' --'});
foreach my $accountFamilyId (keys(%{$tabViewData{'accountFamilyList'}}))
{
    push(@accountFamilyList, {
            'value' => $accountFamilyId,
            'innerHTML' => $tabViewData{'accountFamilyList'}->{$accountFamilyId}
        });
}
my @energyList = ();
foreach my $energy (@{$tabViewData{'energyList'}})
{
    push(@energyList, {
            'value' => $energy,
            'innerHTML' => $energy
        });
}


# Info-bulles
my $elevationHelp = $view->displayHelp($locale->t('Format : dimension m<br />Ex : 12 m<br />Saisir n/a s’il n’y a pas d’élévation.'));
my $businessFamilyHelp = $view->displayHelp($locale->t('Veuillez contacter le support informatique pour créer une nouvelle famille commerciale'));
my $accountFamilyHelp = $view->displayHelp($locale->t('Veuillez contacter le support informatique pour créer une nouvelle famille comptable'));

# Champs
my $businessFamilySelect = $view->displayFormSelect(
        'businessFamilyId',
        \@businessFamilyList,
        $tabViewData{'machinesFamily.infos'}->{'businessFamily.id'}
    );
my $accountFamilySelect = $view->displayFormSelect(
        'accountFamilyId',
        \@accountFamilyList,
        $tabViewData{'machinesFamily.infos'}->{'accountFamily.id'}
    );
my $energySelect = $view->displayFormSelect(
        'energy',
        \@energyList,
        $tabViewData{'machinesFamily.infos'}->{'energy'}
    );
my $elevation = $view->displayFormInputText(
        'elevation',
        $tabViewData{'machinesFamily.infos'}->{'elevation'},
        '',
        {'maxlength' => '10'}
    );


print '
<fieldset>
    <legend>' . $locale->t('Identification') . '</legend>
    <table class="formTable">
        <tbody>';

if ($tabViewData{'machinesFamily.infos'}->{'id'})
{
    print '
            <tr>
                <td class="label">' . $locale->t('Identifiant interne') . '</td>
                <td>' . $tabViewData{'machinesFamily.infos'}->{'id'} . '</td>
            </tr>';
}

print '
            <tr>
                <td class="label">' . $locale->t('Famille commerciale') . $requiredSymbol . '</td>
                <td>' . $businessFamilySelect . ' ' . $businessFamilyHelp . '</td>
            </tr>
            <tr>
                <td class="label">' . $locale->t('Famille comptable') . $requiredSymbol . '</td>
                <td>' . $accountFamilySelect . ' ' . $accountFamilyHelp . '</td>
            </tr>
            <tr>
                <td class="label" colspan="2">' . $locale->t('Libellé') . '</td>
            </tr>';

my @tabCountry = keys(%{$tabViewData{'countryList'}});
my $nbCountries = scalar @tabCountry;
for (my $i = 0; $i < $nbCountries; $i++)
{
    my $isRequired = ($tabCountry[$i] eq LOC::Country::DEFAULT_COUNTRY_ID);

    print '
            <tr>';

    my $label = $view->displayFormInputText(
            'labels[' . $tabCountry[$i] . ']',
            $tabViewData{'machinesFamily.infos'}->{'labels'}->{$tabCountry[$i]},
            LOC::Html::TEXTMODE_UPPER,
            {'maxlength' => '30'}
        );

    print '
                <td class="label country">
                    ' . ($isRequired ? $requiredSymbol : '') . '
                    ' . $view->displayCountryImage($tabCountry[$i]) . '
                </td>
                <td>' . $label . '</td>';
    if ($isRequired)
    {
        $view->addJSSrc('tabInputRequired.push("labels[' . LOC::Country::DEFAULT_COUNTRY_ID . ']");');
    }

    print '
            </tr>';
}
print '
        </tbody>
    </table>
</fieldset>

<fieldset>
    <legend>' . $locale->t('Caractéristiques') . '</legend>
    <table class="formTable">
        <tbody>
            <tr>
                <td class="label">' . $locale->t('Énergie') . $requiredSymbol . '</td>
                <td>' . $energySelect . '</td>
            </tr>
            <tr>
                <td class="label">' . $locale->t('Élévation') . $requiredSymbol . '</td>
                <td>' . $elevation . ' ' . $elevationHelp . '</td>
            </tr>
        </tbody>
    </table>
</fieldset>
';


# champs cachés
print $view->displayFormInputHidden('id', $tabViewData{'id'});
print $view->displayFormInputHidden('valid', $tabViewData{'valid'});

# Bouton de validation
my $btnValid = $view->displayTextButton($locale->t(($isNew ? 'Ajouter' : 'Modifier')), 'valid(|Over).png', 'submitForm()');

# Affichage du panel
print $view->displayControlPanel({'left' => [$btnValid], 'substyle' => 'bottom'});


# Fin du formulaire
print $view->endForm();

# Affichage du pied de page
print $view->displayFooter();
