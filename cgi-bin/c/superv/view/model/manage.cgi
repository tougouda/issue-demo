use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'superv/model/';


our %tabViewData;
our $inputsTabIndex = 1;


our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});


# URL courante 
my $currentUrl = $view->createURL('superv:model:manage');


# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'manage.css');

# Scripts JS
# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc($dir . 'manage.js');

$view->addPackage('spincontrols');

# On a un identifiant de machine
my $isNew = ($tabViewData{'id'} ? 0 : 1); # Ajout ou modification

my $title = ($isNew ? $locale->t('Ajouter un modèle de machines') : $locale->t('Modifier un modèle de machines'));
$view->setPageTitle($title);
$view->setTitle($title);

# Affichage de l'entête
print $view->displayHeader();

# Début du formulaire
print $view->startForm(undef, undef, 'manageForm');


# Tableau des retours
my $tabReturn = {
    'valid' => {
        0 => $locale->t('Modèle de machines modifié avec succès'),
        1 => $locale->t('Modèle de machines ajouté avec succès')
    },
    'error' => {
        0  => $locale->t('Erreur inconnue'),
        1  => $locale->t('Erreur fatale ! Veuillez contacter le service informatique s.v.p.'),
        2  => $locale->t('Tous les champs obligatoires ne sont pas renseignés'),
        3  => $locale->t('Le modèle de machines ne peut pas être à la fois de type "Sous-location" et "Catégorie B"'),
        4  => $locale->t('Le modèle de machines ne peut pas être à la fois de type "Accessoire" et "Catégorie B"'),
        5  => $locale->t('Le modèle de machines ne peut pas être à la fois de type "Sous-location" et "Vente"'),
        6  => $locale->t('Le modèle de machines ne peut pas être à la fois de type "Accessoire" et "Vente"'),
        7  => $locale->t('Le modèle de machines ne peut pas être à la fois de type "Catégorie B" et "Vente"')
    }
};

# Affichage des messages
if ($tabViewData{'tabData'})
{
    my @msg;
    foreach my $code (@{$tabViewData{'tabData'}->{'@return'}})
    {
        push(@msg, $tabReturn->{$tabViewData{'tabData'}->{'@state'}}->{$code});
    }
    print $view->displayMessages($tabViewData{'tabData'}->{'@state'}, \@msg, 0);
}

# - Éléments du formulaire
my $requiredSymbol = $view->displayRequiredSymbol();

my @machinesFamilyList = ({'value' => 0, 'innerHTML' => '-- ' . $locale->t('Sélectionner') . ' --'});
foreach my $machinesFamilyId (keys(%{$tabViewData{'machinesFamilyList'}}))
{
    push(@machinesFamilyList, {
            'value' => $machinesFamilyId,
            'innerHTML' => $tabViewData{'machinesFamilyList'}->{$machinesFamilyId}
        });
}
my @tariffFamilyList = ({'value' => 0, 'innerHTML' => '-- ' . $locale->t('Sélectionner') . ' --'});
foreach my $tariffFamilyId (keys(%{$tabViewData{'tariffFamilyList'}}))
{
    push(@tariffFamilyList, {
            'value' => $tariffFamilyId,
            'innerHTML' => $tabViewData{'tariffFamilyList'}->{$tariffFamilyId}
        });
}
my @manufacturersList = ({'value' => 0, 'innerHTML' => '-- ' . $locale->t('Sélectionner') . ' --'});
foreach my $manufacturerId (keys(%{$tabViewData{'manufacturersList'}}))
{
    push(@manufacturersList, {
            'value' => $manufacturerId,
            'innerHTML' => $tabViewData{'manufacturersList'}->{$manufacturerId}
        });
}


# Constantes
$view->addJSSrc('
/* Typage */
var TYPE_ACCESSORY = "' . LOC::Model::TYPE_ACCESSORY . '";
var TYPE_SUBRENT   = "' . LOC::Model::TYPE_SUBRENT . '";
var TYPE_CATEGORYB = "' . LOC::Model::TYPE_CATEGORYB . '";
var TYPE_SALE      = "' . LOC::Model::TYPE_SALE . '";
');


my $inputOptions = {
    'style' => 'width: 85px',
    'maxlength' => '10',
    'autocomplete' => 'off'
};

my $platformHelp = $view->displayHelp($locale->t('Format : dimension x dimension (x dimension)<br />
                                                  Ex : 0,76 x 1,63 ; 0,76 x 1,56 x 2,50'));
my $vgpHelp      = $view->displayHelp($locale->t('Laisser le champ vide, si le modèle ne nécessite pas de contrôle VGP'));

my $machinesFamilyHelp = $view->displayHelp($locale->t('Veuillez contacter le support informatique pour créer une nouvelle famille de machines'));
my $tariffFamilyHelp = $view->displayHelp($locale->t('Veuillez contacter le support informatique pour créer une nouvelle famille tarifaire'));
my $manufacturerHelp = $view->displayHelp($locale->t('Veuillez contacter le support informatique pour créer un nouveau fabricant'));


my $familySelect       = $view->displayFormSelect('familyId', \@machinesFamilyList,
                                                    $tabViewData{'model.infos'}->{'machinesFamily.id'});
my $tariffFamilySelect = $view->displayFormSelect('tariffFamilyId', \@tariffFamilyList,
                                                    $tabViewData{'model.infos'}->{'tariffFamily.id'});
my $manufacturerSelect = $view->displayFormSelect('manufacturerId', \@manufacturersList,
                                                    $tabViewData{'model.infos'}->{'manufacturer.id'});
my $label              = $view->displayFormInputText('label', $tabViewData{'model.infos'}->{'label'}, '',
                                                        {'style' => 'width: 295px;', 'autocomplete' => 'off'});

my $workingHeight      = $view->displayFormInputText('workingHeight', $tabViewData{'model.infos'}->{'workingHeight'},
                                                        LOC::Html::TEXTMODE_NUMERIC, $inputOptions);
my $floorHeight        = $view->displayFormInputText('floorHeight', $tabViewData{'model.infos'}->{'floorHeight'},
                                                        LOC::Html::TEXTMODE_NUMERIC, $inputOptions);
my $restHeight         = $view->displayFormInputText('restHeight', $tabViewData{'model.infos'}->{'restHeight'},
                                                        LOC::Html::TEXTMODE_NUMERIC, $inputOptions);
my $stowedHeight       = $view->displayFormInputText('stowedHeight', $tabViewData{'model.infos'}->{'stowedHeight'},
                                                        LOC::Html::TEXTMODE_NUMERIC, $inputOptions);
my $backwardation      = $view->displayFormInputText('backwardation', $tabViewData{'model.infos'}->{'backwardation'},
                                                        LOC::Html::TEXTMODE_NUMERIC, $inputOptions);
my $capacity           = $view->displayFormInputText('capacity', $tabViewData{'model.infos'}->{'capacity'},
                                                        LOC::Html::TEXTMODE_NUMERIC, $inputOptions);
my $maxCapacity        = $view->displayFormInputText('maxCapacity', $tabViewData{'model.infos'}->{'maxCapacity'},
                                                        LOC::Html::TEXTMODE_NUMERIC, $inputOptions);
my $maxRange           = $view->displayFormInputText('maxRange', $tabViewData{'model.infos'}->{'maxRange'},
                                                        LOC::Html::TEXTMODE_NUMERIC, $inputOptions);
my $length             = $view->displayFormInputText('length', $tabViewData{'model.infos'}->{'length'},
                                                        LOC::Html::TEXTMODE_NUMERIC, $inputOptions);
my $width              = $view->displayFormInputText('width', $tabViewData{'model.infos'}->{'width'},
                                                        LOC::Html::TEXTMODE_NUMERIC, $inputOptions);
my $weight             = $view->displayFormInputText('weight', $tabViewData{'model.infos'}->{'weight'},
                                                        LOC::Html::TEXTMODE_NUMERIC, $inputOptions);
my $platformLength     = $view->displayFormInputText('platformLength', $tabViewData{'model.infos'}->{'platformLength'},
                                                        '',
                                                        {'style' => 'width: 155px;', 'maxlength' => '40',
                                                            'autocomplete' => 'off'});

my $tabStabilizersValues = [
    {'value' => 'NON', 'label' => $locale->t('Non')},
    {'value' => 'OUI', 'label' =>  $locale->t('Oui')},
    {'value' => 'AUTO', 'label' => $locale->t('Auto')}
];
my @stabilizersRadioGroup;
foreach my $elt (@$tabStabilizersValues)
{
    my $isChecked = ($tabViewData{'model.infos'}->{'stabilizers'} eq $elt->{'value'} ? 1 : 0);
    my $radio = $view->displayFormInputRadio('stabilizers', $elt->{'value'}, $isChecked, {}, $elt->{'label'});
    push(@stabilizersRadioGroup, $radio);
}
my $stabilizers = join(' ', @stabilizersRadioGroup);


my $rotation           = $view->displayFormInputText('rotation', $tabViewData{'model.infos'}->{'rotation'},
                                                        LOC::Html::TEXTMODE_NUMERIC, $inputOptions);
my $motor              = $view->displayFormInputText('motor', $tabViewData{'model.infos'}->{'motor'}, '',
                                                        {'class' => 'spinInput', 'style' => 'width:20px'});
$motor .= $view->displayJSBlock('Location.spinControlsManager.createSpinControl("motor", 0, 6, 1);');
my $localCurrencyCost  = $view->displayFormInputCurrency('localCurrencyCost',
                                                         $tabViewData{'model.infos'}->{'localCurrencyCost'},
                                                         LOC::Html::CURRENCYMODE_POSITIVEINPUT,
                                                         undef,
                                                         undef,
                                                         {'style' => 'width: 85px;', 'maxlength' => '10',
                                                             'autocomplete' => 'off'});
# Typage
my $tabTypes = [
                {'id' => LOC::Model::TYPE_ACCESSORY, 'label' => $locale->t('Accessoire')},
                {'id' => LOC::Model::TYPE_SUBRENT,   'label' => $locale->t('Sous-location')},
                {'id' => LOC::Model::TYPE_CATEGORYB, 'label' => $locale->t('Catégorie B')},
                {'id' => LOC::Model::TYPE_SALE,      'label' => $locale->t('Vente')}
               ];
my $types = '<ul id="types">' . "\n";
foreach my $type (@$tabTypes)
{
    my $isChecked = (&LOC::Util::in_array($type->{'id'}, $tabViewData{'model.infos'}->{'tabTypes'}));

    $types .= '<li>' ;
    $types .= $view->displayFormInputCheckBox('type[' . $type->{'id'} . ']', $isChecked, {}, $type->{'label'});
    $types .= '</li>' . "\n";
}
$types .= '</ul>' . "\n";


print '
<fieldset>
    <legend>' . $locale->t('Identification') . '</legend>
    <table class="formTable">
        <tbody>';

if ($tabViewData{'model.infos'}->{'id'})
{
    print '
            <tr>
                <td class="label">' . $locale->t('Identifiant interne') . '</td>
                <td>' . $tabViewData{'model.infos'}->{'id'} . '</td>
            </tr>';
}

print '
            <tr>
                <td class="label">' . $locale->t('Famille de machines') . $requiredSymbol . '</td>
                <td>' . $familySelect . ' ' . $machinesFamilyHelp . '</td>
            </tr>
            <tr>
                <td class="label">' . $locale->t('Famille tarifaire') . $requiredSymbol . '</td>
                <td>' . $tariffFamilySelect . ' ' . $tariffFamilyHelp . '</td>
            </tr>
            <tr>
                <td class="label">' . $locale->t('Fabricant') . $requiredSymbol . '</td>
                <td>' . $manufacturerSelect . ' ' . $manufacturerHelp . '</td>
            </tr>
            <tr>
                <td class="label">' . $locale->t('Référence') . $requiredSymbol . '</td>
                <td>' . $label . '</td>
            </tr>
            <tr>
                <td class="label">' . $locale->t('Typage') . '</td>
                <td>' . $types . '</td>
            </tr>
        </tbody>
    </table>
</fieldset>

<fieldset>
    <legend>' . $locale->t('Caractéristiques') . '</legend>
    <table class="formTable">
        <tbody>
            <tr>
                <td class="label">' . $locale->t('Longueur') . '</td>
                <td>' . $length . ' m</td>
                <td class="label">'.
                    $locale->t('Largeur') . ' / ' . $locale->t('Largeur essieux') .
               '</td>
                <td>' . $width . ' m</td>
            </tr>
            <tr>
                <td class="label">' . $locale->t('Hauteur de travail') . '</td>
                <td>' . $workingHeight . ' m</td>
                <td class="label">' . $locale->t('Hauteur plancher') . '</td>
                <td>' . $floorHeight . ' m</td>
            </tr>
            <tr>
                <td class="label">' .
                    $locale->t('Hauteur rambarde relevée') . '</td>
                <td>' . $restHeight . ' m</td>
                <td class="label">' .
                    $locale->t('Hauteur rambarde repliée') . '</td>
                <td>' . $stowedHeight . ' m</td>
            </tr>
            <tr>
                <td class="label">' . $locale->t('Poids') . '</td>
                <td>' . $weight . ' kg</td>
                <td class="label">' . $locale->t('Charge nominale') . '</td>
                <td>' . $capacity . ' kg</td>
            </tr>
            <tr>
                <td class="label">' .
                    $locale->t('Capacité max.') . ' / ' . $locale->t('Capacité à hauteur max.') .
               '</td>
                <td>' . $maxCapacity . ' kg</td>
                <td class="label">' . $locale->t('Capacité max. à portée max.') . '</td>
                <td>' . $maxRange . ' kg</td>
            </tr>
            <tr class="platformLength">
                <td class="label">' . $locale->t('Dimension de la plateforme') . '</td>
                <td>' . $platformLength . ' m ' . $platformHelp . '</td>
                <td colspan="2"><span></span></td>
            </tr>
            <tr>
                <td class="label">' .
                    $locale->t('Déport') . ' / ' . $locale->t('Portée maximum') .
               '</td>
                <td>' . $backwardation . ' m</td>
                <td class="label">' . $locale->t('Rotation tourelle') . '</td>
                <td>' . $rotation . ' °</td>
            </tr>
            <tr>
                <td class="label">' . $locale->t('Stabilisateurs') . '</td>
                <td>' . $stabilizers . '</td>
                <td class="label">' . $locale->t('Motricité') . '</td>
                <td>' . $motor . '</td>
            </tr>
        </tbody>
    </table>
</fieldset>

<fieldset>
    <legend>' . $locale->t('Informations diverses') . '</legend>
    <table class="formTable">
        <tbody>
            <tr>
                <td class="label">' . $locale->t('Coût du modèle') . '</td>
                <td colspan="5">' . $localCurrencyCost . '</td>
            </tr><!--
            <tr>
                <td colspan="6">&nbsp;</td>
            </tr>-->';

# Préparation des colonnes par pays
my $countriesList = '';
my $countriesLoad = '';
my $countriesVgp = '';
foreach my $countryId (keys(%{$tabViewData{'countriesList'}}))
{
    $countriesList .= '<td class="small">' . $tabViewData{'countriesList'}->{$countryId} . '</td>';

    # Encombrement
    my $loadSelect = $view->displayImage('no.png', {'title' => 'Inactif'});
    if ($tabViewData{'loads.infos'}->{$countryId}->{'isActive'})
    {
        my @loadsList;

        # Option "Non défini" seulement dans le cas où l'encombrement n'est pas défini
        if (!defined $tabViewData{'model.infos'}->{'tabLoads'}->{$countryId} || $tabViewData{'model.infos'}->{'tabLoads'}->{$countryId} eq '')
        {
            push(@loadsList, {'value'     => undef,
                              'innerHTML' => '-- ' . $locale->t('Non défini') . ' --'});
        }

        foreach my $loadId (keys(%{$tabViewData{'loads.infos'}->{$countryId}->{'list'}}))
        {
            push(@loadsList, {'value'     => $loadId,
                              'innerHTML' => $tabViewData{'loads.infos'}->{$countryId}->{'list'}->{$loadId}});
        }
    
        $loadSelect = $view->displayFormSelect('load[' . $countryId . ']', 
                                                    \@loadsList,
                                                    $tabViewData{'model.infos'}->{'tabLoads'}->{$countryId});
    }
    $countriesLoad .= '<td>' . $loadSelect . '</td>';

    # VGP
    my $periodicityVgpInput = $view->displayFormInputText('periodicity[' . $countryId . ']',
                                                          $tabViewData{'model.infos'}->{'tabPeriodicityVgp'}->{$countryId},
                                                          LOC::Html::TEXTMODE_NUMERIC,
                                                          {'style' => 'width:35px;', 'maxlength' => '2', 'autocomplete' => 'off'});
    $countriesVgp .= '<td>' . $periodicityVgpInput . ' ' . $locale->t('mois') . '</td>';
}
print '
            <tr>
                <td></td>' . $countriesList .
           '</tr>
            <tr>
                <td class="label">' . $locale->t('Encombrement') . $requiredSymbol . '</td>' .
                $countriesLoad .
           '</tr>
            <tr>
                <td class="label">' . $locale->t('Périodicité VGP') . '</td>' .
                $countriesVgp .
               '<td>' . $vgpHelp . '</td>
           </tr>
       </tbody>
    </table>
</fieldset>
';


# champs cachés
print $view->displayFormInputHidden('id', $tabViewData{'id'});
print $view->displayFormInputHidden('valid', $tabViewData{'valid'});

# Bouton de validation
my $btnValid = $view->displayTextButton($locale->t(($isNew ? 'Ajouter' : 'Modifier')), 'valid(|Over).png', 'submitForm()');

# Affichage du panel
print $view->displayControlPanel({'left' => [$btnValid], 'substyle' => 'bottom'});


# Fin du formulaire
print $view->endForm();

# Affichage du pied de page
print $view->displayFooter();
