use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'superv/model/';


our %tabViewData;
our $inputsTabIndex = 1;


our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});


# URL courante 
my $currentUrl = $view->createURL('superv:model:view');


# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'view.css');

# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc('jQuery/plugins/tablesorter.js');

$view->addPackage('popups');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);


my $title = $locale->t('Modèles de machines');
$view->setPageTitle($title);
$view->setTitle($title);


# Bouton "Ajouter"
my $addModelUrl = $view->createURL('superv:model:manage');
my $addModelBtn = $view->displayTextButton($locale->t('Ajouter'), 'add(|Over).png',
                                             'Location.popupsManager.getPopup(\'model\').open(\'' . $addModelUrl . '\');
                                              Location.popupsManager.getPopup(\'model\').setPosition(\'center\', \'center\');',
                                             $locale->t('Ajouter un modèle de machines'));
                                             
my $addSubRentModelUrl = $view->createURL('superv:model:manageSubRent');
my $addSubRentModelBtn = $view->displayTextButton($locale->t('Ajouter un modèle de sous-location'), 'add(|Over).png',
                                             'Location.popupsManager.getPopup(\'modelSubrent\').open(\'' . $addSubRentModelUrl . '\');
                                              Location.popupsManager.getPopup(\'modelSubrent\').setPosition(\'center\', \'center\');',
                                             $locale->t('Ajouter un modèle de machines'));
$view->addControlsContent([$addModelBtn, $addSubRentModelBtn], 'left', 1);

# Bouton "Actualiser"
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png',
                                          $currentUrl, $locale->t('Actualiser la page'));
$view->addControlsContent([$refreshBtn], 'right', 1);


# Affichage de l'entête
print $view->displayHeader();
print $view->displayJSBlock('Location.popupsManager.createPopup("model", "", "center", "center", 760, 525);');
print $view->displayJSBlock('Location.popupsManager.createPopup("modelSubrent", "", "center", "center", 760, 270);');


# On n'affiche pas le tableau si il est vide
my @tabModels = values %{$tabViewData{'tabModels'}};
if (@tabModels == 0)
{
    # Pas de résultat
    print '<div class="noResult">' . $view->displayMessages('info', [$locale->t('Aucun modèle existant')], 0) . '</div>';
}
else
{
    # Options pour tablesorter
    my $headersOption = '
8 : {sorter: false}'
;
    my $sortListOption = '[[1,0]]'; # Tri par défaut: "Référence du modèle" (ASC)

    print '
<table class="basic model">
<thead>
    <tr>
        <th class="id">' . $locale->t('Id.') . '</th>
        <th class="label">' . $locale->t('Référence') . '</th>
        <th class="machinesFamily">' . $locale->t('Famille de machines') . '</th>
        <th class="tariffFamily">' . $locale->t('Famille tarifaire') . '</th>
        <th class="manufacturer">' . $locale->t('Fabricant') . '</th>
        <th class="workingHeight">' . $locale->t('Hauteur de travail') . ' (m)</th>
        <th class="length">' . $locale->t('Longueur') . ' (m)</th>
        <th class="width">' . $locale->t('Largeur') . ' (m)</th>
        <th class="edit"></th>
    </tr>
</thead>
<tbody>';

    # Affichage des lignes
    my $lineNo = 0;
    foreach my $tabModelInfos (@tabModels)
    {
        my $editModelUrl = $view->createURL('superv:model:manage', {'id' => $tabModelInfos->{'id'}});
        my $editModelBtn = $view->displayTextButton('', 'edit(|Over).png',
                                             'Location.popupsManager.getPopup(\'model\').open(\'' . $editModelUrl . '\');
                                              Location.popupsManager.getPopup(\'model\').setPosition(\'center\', \'center\');',
                                             $locale->t('Modifier le modèle de machines'));

        my $lineClass = ($lineNo % 2 ? 'even' : 'odd');

        print '
    <tr class="' . $lineClass . '">
        <td class="id small">' . $tabModelInfos->{'id'} . '</td>
        <td class="label">' . $locale->t($tabModelInfos->{'label'}) . '</td>
        <td class="machinesFamily">' . $tabModelInfos->{'machinesFamily.fullName'} . '</a></td>
        <td class="tariffFamily">' . $tabModelInfos->{'tariffFamily.fullName'} . '</td>
        <td class="manufacturer">' . $tabModelInfos->{'manufacturer.label'} . '</td>
        <td class="workingHeight">' . $tabModelInfos->{'workingHeight'} . '</td>
        <td class="length">' . $tabModelInfos->{'length'} . '</td>
        <td class="width">' . $tabModelInfos->{'width'} . '</td>
        <td class="edit">' . $editModelBtn . '</td>
    </tr>';

        $lineNo++;
    }

    print '
</tbody>
</table>';

    # Tri du tableau
    print $view->displayJSBlock('
$(function() 
{ 
    $("table.basic.model").tablesorter({
        headers: {' . $headersOption . '},
        sortList: ' . $sortListOption . ',
        widgets: ["zebra"]
    });
}
);');
}


# Affichage du pied de page
print $view->displayFooter();
