use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'superv/model/';


our %tabViewData;
our $inputsTabIndex = 1;


our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});


# URL courante 
my $currentUrl = $view->createURL('superv:model:manage');


# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'manage.css');

# Scripts JS
# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc($dir . 'manageSubRent.js');

$view->addPackage('spincontrols');

my $title = $locale->t('Ajouter un modèle de machines de sous-location');
$view->setPageTitle($title);
$view->setTitle($title);

# Affichage de l'entête
print $view->displayHeader();

print $view->startForm(undef, undef, 'manageForm');


# Tableau des retours
my $tabReturn = {
    'valid' => {
        0 => $locale->t('Modèle de machines modifié avec succès'),
        1 => $locale->t('Modèle de machines ajouté avec succès')
    },
    'error' => {
        0  => $locale->t('Erreur inconnue'),
        1  => $locale->t('Erreur fatale ! Veuillez contacter le service informatique s.v.p.'),
        2  => $locale->t('Tous les champs obligatoires ne sont pas renseignés')
    }
};

# Affichage des messages
if ($tabViewData{'tabData'})
{
    my @msg;
    foreach my $code (@{$tabViewData{'tabData'}->{'@return'}})
    {
        push(@msg, $tabReturn->{$tabViewData{'tabData'}->{'@state'}}->{$code});
    }
    print $view->displayMessages($tabViewData{'tabData'}->{'@state'}, \@msg, 0);
}

# - Éléments du formulaire
my $requiredSymbol = $view->displayRequiredSymbol();

my @machinesFamilyList = ({'value' => 0, 'innerHTML' => '-- ' . $locale->t('Sélectionner') . ' --'});
foreach my $machinesFamilyId (keys(%{$tabViewData{'machinesFamilyList'}}))
{
    push(@machinesFamilyList, {'value' => $machinesFamilyId,
                               'innerHTML' => $tabViewData{'machinesFamilyList'}->{$machinesFamilyId}});
}


my $inputOptions = {
    'style' => 'width: 85px',
    'maxlength' => '10',
    'autocomplete' => 'off'
};

my @tabEnergiesList;
foreach my $energyLabel (@{$tabViewData{'tabEnergiesList'}})
{
    push(@tabEnergiesList, {
        'value' => $energyLabel,
        'innerHTML' => $energyLabel
    });
}

my $generalHelpLabel = $locale->t('Veuillez contacter le support informatique');
my $machinesFamilyHelp = $view->displayHelp($locale->t('Ex : ARAIGNEE, CISEAU...'));


my $familyLabel  = $view->displayFormInputText('familyLabel', $tabViewData{'model.infos'}->{'machinesFamily.label'}, LOC::Html::TEXTMODE_UPPER,
                                                {'style' => 'width: 175px;', 'autocomplete' => 'off'});
my $familyEnergy = $view->displayFormSelect('energyLabel', \@tabEnergiesList, $tabViewData{'model.infos'}->{'machinesFamily.energy'});
my $familyElevation    = $view->displayFormInputText('elevation', $tabViewData{'model.infos'}->{'machinesFamily.elevation'},
                                                LOC::Html::TEXTMODE_NUMERIC, $inputOptions);

my $label              = $view->displayFormInputText('label', $tabViewData{'model.infos'}->{'label'}, '',
                                                        {'style' => 'width: 295px;', 'autocomplete' => 'off'});

print '
<fieldset>
    <legend>' . $locale->t('Famille de machines') . '</legend>
    <table class="formTable">

        <tr>
            <td class="label">' . $locale->t('Libellé') . $requiredSymbol . '</td>
            <td>' . $familyLabel . ' - ' . $locale->t('SL') . ' ' . $machinesFamilyHelp . '</td>
        </tr>
        <tr>
            <td class="label">' . $locale->t('Énergie') . '</td>
            <td>' . $familyEnergy . '</td>
        </tr>
        <tr>
            <td class="label">' . $locale->t('Élévation') . '</td>
            <td>' . $familyElevation . ' ' . $locale->t('m') . '</td>
        </tr>
    </table>
</fieldset>';


print '
<fieldset>
    <legend>' . $locale->t('Identification') . '</legend>
    <table class="formTable">
        <tr>
            <td class="label">' . $locale->t('Référence') . $requiredSymbol . '</td>
            <td>' . $label . ' - ' . $locale->t('SL') . '</td>
        </tr>
    </table>
</fieldset>

<fieldset>
    <legend>' . $locale->t('Informations diverses') . '</legend>
    <table class="formTable">';

# Préparation des colonnes par pays
my $countriesList = '';
my $countriesLoad = '';
my $countriesVgp = '';
foreach my $countryId (keys(%{$tabViewData{'countriesList'}}))
{
    $countriesList .= '<td class="small">' . $tabViewData{'countriesList'}->{$countryId} . '</td>';

    # Encombrement
    my $loadSelect = $view->displayImage('no.png', {'title' => 'Inactif'});
    if ($tabViewData{'loads.infos'}->{$countryId}->{'isActive'})
    {
        my @loadsList;

        # Option "Non défini" seulement dans le cas où l'encombrement n'est pas défini
        if (!defined $tabViewData{'model.infos'}->{'tabLoads'}->{$countryId} || $tabViewData{'model.infos'}->{'tabLoads'}->{$countryId} eq '')
        {
            push(@loadsList, {'value'     => undef,
                              'innerHTML' => '-- ' . $locale->t('Non défini') . ' --'});
        }

        foreach my $loadId (keys(%{$tabViewData{'loads.infos'}->{$countryId}->{'list'}}))
        {
            push(@loadsList, {'value'     => $loadId,
                              'innerHTML' => $tabViewData{'loads.infos'}->{$countryId}->{'list'}->{$loadId}});
        }
    
        $loadSelect = $view->displayFormSelect('load[' . $countryId . ']', 
                                                    \@loadsList,
                                                    $tabViewData{'model.infos'}->{'tabLoads'}->{$countryId});
    }
    $countriesLoad .= '<td>' . $loadSelect . '</td>';

    # VGP
    my $periodicityVgpInput = $view->displayFormInputText('periodicity[' . $countryId . ']',
                                                          $tabViewData{'model.infos'}->{'tabPeriodicityVgp'}->{$countryId},
                                                          LOC::Html::TEXTMODE_NUMERIC,
                                                          {'style' => 'width:35px;', 'maxlength' => '2', 'autocomplete' => 'off'});
    $countriesVgp .= '<td>' . $periodicityVgpInput . ' ' . $locale->t('mois') . '</td>';
}
print '
        <tr>
            <td></td>' . $countriesList .
       '</tr>
        <tr>
            <td class="label">' . $locale->t('Encombrement') . $requiredSymbol . '</td>' .
            $countriesLoad .
       '</tr>
    </table>
</fieldset>
';


# champs cachés
my $htmlHidden = '';
$htmlHidden .= $view->displayFormInputHidden('id', $tabViewData{'id'});
$htmlHidden .= $view->displayFormInputHidden('valid', $tabViewData{'valid'});

# Affichage des champs cachés
print '
    <div id="chpHiddens" style="display: none;">' .
        $htmlHidden . '
    </div>';

# Bouton de validation
my $btnValid = $view->displayTextButton($locale->t('Ajouter'), 'valid(|Over).png', 'submitForm()');

# Affichage du panel
print $view->displayControlPanel({'left' => [$btnValid], 'substyle' => 'bottom'});


# Affichage du pied de page
print $view->displayFooter();
