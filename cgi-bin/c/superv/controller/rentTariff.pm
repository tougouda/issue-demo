use utf8;
use open (':encoding(UTF-8)');

# Package: rentTariff
# Contrôleur pour la tarification location
package rentTariff;

use strict;
use File::Basename;

use LOC::Country;
use LOC::Session;
use LOC::TariffFamily;
use LOC::Util;
use LOC::Json;


# Function: viewAction
# Liste des familles tarifaires
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId      = $tabUserInfos->{'nomadCountry.id'};
    my $agencyId       = $tabUserInfos->{'nomadAgency.id'};
    my $displayType    = &LOC::Request::getString('displayType', '');

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    # Filtres actifs
    $tabViewData{'filter'}              = &LOC::Request::getString('tariffFamily-filter', '');
    $tabViewData{'baseTariff-filter'}   = &LOC::Request::getInteger('baseTariff-filter');
    $tabViewData{'pricingPrice-filter'} = &LOC::Request::getInteger('pricingPrice-filter');
    $tabViewData{'costPrice-filter'}    = &LOC::Request::getInteger('costPrice-filter');

    # Validation
    my $validInfos = &LOC::Request::getString('valid', '');
    my $tabErrors = {};
    if ($validInfos ne '')
    {
        my $tabValidInfos = &LOC::Json::fromJson($validInfos);
        if ($tabValidInfos->{'valid'} eq 'ok')
        {
            # Tableau de retour
            $tabViewData{'return'} = {
                'success' => [],
                'errors'  => []
            };
        }
        # Mise à jour de chaque tarif
        foreach my $tariffFamilyId (keys(%{$tabValidInfos->{'tabTariffs'}}))
        {
            if (&LOC::Tariff::Rent::updateTariffs($countryId, $tariffFamilyId, $tabValidInfos->{'tabTariffs'}->{$tariffFamilyId}, $tabUserInfos->{'id'}, $tabErrors))
            {
                push(@{$tabViewData{'return'}->{'success'}}, $tariffFamilyId);
            }
            else
            {
                my $errorCode = ();

                my $nbErrors = @{$tabErrors->{'fatal'}};
                if ($nbErrors > 0)
                {
                    # Erreur fatale
                    if (&LOC::Util::in_array(LOC::Tariff::Rent::ERROR_FATAL, $tabErrors->{'fatal'}))
                    {
                        $errorCode = 'fatalError';
                    }
                    # Erreur de droits
                    elsif (&LOC::Util::in_array(LOC::Tariff::Rent::ERROR_RIGHTS, $tabErrors->{'fatal'}))
                    {
                        $errorCode = 'rightsError';
                    }
                }

                push(@{$tabViewData{'return'}->{'errors'}}, {
                    'id'         => $tariffFamilyId,
                    'error'      => $errorCode
                });
            }
        }
    }


    # Récupération des informations pour la vue
    my $tabTariffFamily;
    tie(%$tabTariffFamily, 'Tie::IxHash');
    $tabTariffFamily = &LOC::TariffFamily::getList(
        $countryId,
        LOC::Util::GETLIST_ASSOC,
        {},
        LOC::TariffFamily::GETINFOS_TARIFFS
    );
    my @tabTariffFamily = values(%$tabTariffFamily);

    $tabViewData{'tabTariffFamily'} = \@tabTariffFamily;
    $tabViewData{'durationRangesList'} = &LOC::Tariff::Rent::getDurationRangesList($countryId);

    # Droits
    $tabViewData{'tabRights'} = &LOC::Tariff::Rent::getRights();

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    if ($displayType eq 'xls')
    {
        require $directory . '/view/rentTariff/viewExcel.cgi';
    }
    else
    {
        require $directory . '/view/rentTariff/view.cgi';
    }
}


1;