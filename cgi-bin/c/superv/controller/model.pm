use utf8;
use open (':encoding(UTF-8)');

# Package: model
# Contrôleur pour les modèles de machines
package model;

use strict;
use File::Basename;

use LOC::Country;
use LOC::Session;
use LOC::Model;
use LOC::MachinesFamily;
use LOC::TariffFamily;
use LOC::Characteristic;
use LOC::Date;


# Function: viewAction
# Liste des modèles de machines
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId      = $tabUserInfos->{'nomadCountry.id'};
    my $agencyId       = $tabUserInfos->{'nomadAgency.id'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    # Récupération des informations pour la vue
    $tabViewData{'tabModels'} = &LOC::Model::getList($countryId, LOC::Util::GETLIST_ASSOC);

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/model/view.cgi';
}

# Function: manageAction
# Ajouter ou modifier un modèle
sub manageAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    
    my $countryId = $tabUserInfos->{'nomadCountry.id'};
    my $agencyId  = $tabUserInfos->{'nomadAgency.id'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    # Récupération de valeurs par défaut
    $tabViewData{'countriesList'}      = &LOC::Country::getList(LOC::Util::GETLIST_PAIRS);
    my $tabDefaultType           = {};
    my $tabDefaultPeriodicityVgp = {};
    my $tabDefaultLoad           = {};
    my $dateNow = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE);
    foreach my $countryId (keys(%{$tabViewData{'countriesList'}}))
    {
        # VGP
        $tabDefaultPeriodicityVgp->{$countryId} = &LOC::Characteristic::getCountryValueByCode('PERIODICITEVGP', $countryId, $dateNow) * 1;
        # Encombrement
        $tabDefaultLoad->{$countryId} = undef;
    }

    # Récupération des informations du formulaire
    $tabViewData{'valid'} = &LOC::Request::getString('valid', '');
    $tabViewData{'id'}    = &LOC::Request::getInteger('id', 0);
    my %tabData = (
        'machinesFamily.id' => &LOC::Request::getInteger('familyId', 0),
        'tariffFamily.id'   => &LOC::Request::getInteger('tariffFamilyId', 0),
        'manufacturer.id'   => &LOC::Request::getInteger('manufacturerId', 0),
        'label'             => &LOC::Request::getString('label', ''),
        'length'            => &LOC::Request::getInteger('length'),
        'width'             => &LOC::Request::getInteger('width'),
        'weight'            => &LOC::Request::getInteger('weight'),
        'workingHeight'     => &LOC::Request::getInteger('workingHeight'),
        'floorHeight'       => &LOC::Request::getInteger('floorHeight'),
        'restHeight'        => &LOC::Request::getInteger('restHeight'),
        'stowedHeight'      => &LOC::Request::getInteger('stowedHeight'),
        'backwardation'     => &LOC::Request::getInteger('backwardation'),
        'capacity'          => &LOC::Request::getInteger('capacity'),
        'maxCapacity'       => &LOC::Request::getInteger('maxCapacity'),
        'maxRange'          => &LOC::Request::getInteger('maxRange'),
        'platformLength'    => &LOC::Request::getString('platformLength', undef),
        'rotation'          => &LOC::Request::getInteger('rotation'),
        'stabilizers'       => &LOC::Request::getString('stabilizers', undef),
        'motor'             => &LOC::Request::getInteger('motor'),
        'localCurrencyCost' => &LOC::Request::getInteger('localCurrencyCost', 0),
        'load'              => &LOC::Request::getInteger('load', 4)
    );
    my $tabTypes = &LOC::Request::getHash('type', $tabDefaultType);
    $tabData{'tabTypes'}          = [keys(%$tabTypes)];
    $tabData{'tabPeriodicityVgp'} = &LOC::Request::getHash('periodicity', $tabDefaultPeriodicityVgp);
    $tabData{'tabLoads'}          = &LOC::Request::getHash('load', $tabDefaultLoad);

    # Enregistrement
    if ($tabViewData{'valid'} eq 'ok')
    {
        my $tabErrors = {};
        my $modelId = $tabViewData{'id'};
        if ($tabViewData{'id'} > 0)
        {
            &LOC::Model::update($countryId, $tabViewData{'id'}, \%tabData, $tabUserInfos->{'id'}, $tabErrors);
        }
        else
        {
            $modelId = &LOC::Model::insert($countryId, \%tabData, $tabUserInfos->{'id'}, $tabErrors);
        }

        my $nbErrors = @{$tabErrors->{'fatal'}};
        if ($nbErrors > 0)
        {
            $tabViewData{'tabData'} = {
                '@state'  => 'error',
            };
    
            my @tabJSErrors = (); # 0 pour être sûr d'avoir un numéro d'erreur dans le tableau !!
    
            # Erreur sur le contrat
            if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
            {
                push(@tabJSErrors, 1); # Erreur fatale
            }
            if (&LOC::Util::in_array(0x0002, $tabErrors->{'fatal'}))
            {
                push(@tabJSErrors, 2); # Tous les champs obligatoires ne sont pas renseignés
            }
            if (&LOC::Util::in_array(0x0003, $tabErrors->{'fatal'}))
            {
                push(@tabJSErrors, 3); # Typage "Sous-location" et "Catégorie B"
            }
            if (&LOC::Util::in_array(0x0004, $tabErrors->{'fatal'}))
            {
                push(@tabJSErrors, 4); # Typage "Accessoire" et "Catégorie B"
            }
            if (&LOC::Util::in_array(0x0005, $tabErrors->{'fatal'}))
            {
                push(@tabJSErrors, 5); # Typage "Sous-location" et "Vente"
            }
            if (&LOC::Util::in_array(0x0006, $tabErrors->{'fatal'}))
            {
                push(@tabJSErrors, 6); # Typage "Accessoire" et "Vente"
            }
            if (&LOC::Util::in_array(0x0007, $tabErrors->{'fatal'}))
            {
                push(@tabJSErrors, 7); # Typage "Catégorie B" et "Vente"
            }
            # Si pas d'erreur alors erreur inconnue !!
            if (@tabJSErrors == 0)
            {
                push(@tabJSErrors, 0); # Erreur inconnue !
            }
    
            $tabViewData{'tabData'}->{'@return'} = \@tabJSErrors;
        }
        else
        {
            $tabViewData{'tabData'} = {
                '@state'  => 'valid',
                '@return' => [($tabViewData{'id'} > 0 ? 0 : 1)],
            };
            $tabViewData{'id'} = $modelId;
        }
    }

    # Récupération des informations pour la vue
    if ($tabViewData{'id'})
    {
        $tabViewData{'model.infos'} = &LOC::Model::getInfos($countryId, $tabViewData{'id'});
    }
    else
    {
        $tabViewData{'model.infos'} = \%tabData;
    }

    my $tariffFamilyId = $tabViewData{'model.infos'}->{'tariffFamily.id'};
    $tabViewData{'machinesFamilyList'} = &LOC::MachinesFamily::getList($countryId, LOC::Util::GETLIST_PAIRS);
    $tabViewData{'tariffFamilyList'}   = &LOC::TariffFamily::getList($countryId, LOC::Util::GETLIST_PAIRS,
                                                                        {'addId' => $tariffFamilyId});
    $tabViewData{'manufacturersList'}  = &LOC::Model::getManufacturersList($countryId, LOC::Util::GETLIST_PAIRS);
    foreach my $country (keys(%{$tabViewData{'countriesList'}}))
    {
        my $isLoadActive = &LOC::Characteristic::getCountryValueByCode('TRSPTARIF', $country, $dateNow) * 1;
        my $load = $tabViewData{'model.infos'}->{'tabLoads'}->{$country};
        my $loadsList = ($isLoadActive ? &LOC::Model::getLoadsList($country, {'addId' => $load}) : {});

        $tabViewData{'loads.infos'}->{$country} = {
            'isActive' => $isLoadActive,
            'list'     => $loadsList
        };
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/model/manage.cgi';
}

# Function: manageSubRentAction
# Ajouter ou modifier un modèle de sous-location
sub manageSubRentAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    
    my $countryId = $tabUserInfos->{'nomadCountry.id'};
    my $agencyId  = $tabUserInfos->{'nomadAgency.id'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    # Récupération de valeurs par défaut
    $tabViewData{'countriesList'}      = &LOC::Country::getList(LOC::Util::GETLIST_PAIRS);
    my $tabDefaultType           = {};
    my $tabDefaultPeriodicityVgp = {};
    my $tabDefaultLoad           = {};
    my $dateNow = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE);
    foreach my $countryId (keys(%{$tabViewData{'countriesList'}}))
    {
        # VGP
        $tabDefaultPeriodicityVgp->{$countryId} = &LOC::Characteristic::getCountryValueByCode('PERIODICITEVGP', $countryId, $dateNow) * 1;
        # Encombrement
        $tabDefaultLoad->{$countryId} = undef;
    }

    # Récupération des informations du formulaire
    $tabViewData{'valid'} = &LOC::Request::getString('valid', '');
    $tabViewData{'id'}    = &LOC::Request::getInteger('id', 0);
    
    my %tabData = (
        'machinesFamily.label'     => &LOC::Request::getString('familyLabel', undef),
        'machinesFamily.energy'    => &LOC::Request::getString('energyLabel', undef),
        'machinesFamily.elevation' => &LOC::Request::getInteger('elevation'),
        'label'                    => &LOC::Request::getString('label', undef),
        'tabPeriodicityVgp'        => $tabDefaultPeriodicityVgp
    );
    $tabData{'tabLoads'} = &LOC::Request::getHash('load', $tabDefaultLoad);

    # Enregistrement
    if ($tabViewData{'valid'} eq 'ok')
    {
        my $tabErrors = {};
        my $modelId = &LOC::Model::insertSubRent($countryId, \%tabData, $tabUserInfos->{'id'}, $tabErrors);

        my $nbErrors = @{$tabErrors->{'fatal'}};
        if ($nbErrors > 0)
        {
            $tabViewData{'tabData'} = {
                '@state'  => 'error',
            };
    
            my @tabJSErrors = (); # 0 pour être sûr d'avoir un numéro d'erreur dans le tableau !!
    
            # Erreur sur le contrat
            if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
            {
                push(@tabJSErrors, 1); # Erreur fatale
            }
            if (&LOC::Util::in_array(0x0002, $tabErrors->{'fatal'}))
            {
                push(@tabJSErrors, 2); # Tous les champs obligatoires ne sont pas renseignés
            }
            # Si pas d'erreur alors erreur inconnue !!
            if (@tabJSErrors == 0)
            {
                push(@tabJSErrors, 0); # Erreur inconnue !
            }
    
            $tabViewData{'tabData'}->{'@return'} = \@tabJSErrors;
        }
        else
        {
            $tabViewData{'tabData'} = {
                '@state'  => 'valid',
                '@return' => [($tabViewData{'id'} > 0 ? 0 : 1)],
            };
            $tabViewData{'id'} = $modelId;
        }
    }

    # Récupération des informations pour la vue
    $tabViewData{'model.infos'} = \%tabData;

    foreach my $country (keys(%{$tabViewData{'countriesList'}}))
    {
        my $isLoadActive = &LOC::Characteristic::getCountryValueByCode('TRSPTARIF', $country, $dateNow) * 1;
        my $load = $tabViewData{'model.infos'}->{'tabLoads'}->{$country};
        my $loadsList = ($isLoadActive ? &LOC::Model::getLoadsList($country, {'addId' => $load}) : {});

        $tabViewData{'loads.infos'}->{$country} = {
            'isActive' => $isLoadActive,
            'list'     => $loadsList
        };
    }

    # Liste des énergies
    $tabViewData{'tabEnergiesList'} = &LOC::MachinesFamily::getEnergiesList(); 

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/model/manageSubRent.cgi';
}

1;