use utf8;
use open (':encoding(UTF-8)');

# Package: machinesFamily
# Contrôleur pour les familles de machines
package machinesFamily;

use strict;
use File::Basename;

use LOC::Country;
use LOC::Session;
use LOC::MachinesFamily;
use LOC::BusinessFamily;
use LOC::AccountFamily;
use LOC::Util;


# Function: viewAction
# Liste des familles de machines
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId      = $tabUserInfos->{'nomadCountry.id'};
    my $agencyId       = $tabUserInfos->{'nomadAgency.id'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    # Récupération des informations pour la vue
    $tabViewData{'tabMachinesFamily'} = &LOC::MachinesFamily::getList(
            $countryId,
            LOC::Util::GETLIST_ASSOC,
            {},
            LOC::MachinesFamily::GETINFOS_BUSINESSFAMILY | LOC::MachinesFamily::GETINFOS_ACCOUNTFAMILY
        );

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/machinesFamily/view.cgi';
}

# Function: manageAction
# Ajouter ou modifier une famille de machines
sub manageAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    
    my $countryId = $tabUserInfos->{'nomadCountry.id'};
    my $agencyId  = $tabUserInfos->{'nomadAgency.id'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    # Récupération des informations du formulaire
    $tabViewData{'valid'} = &LOC::Request::getString('valid', '');
    $tabViewData{'id'}    = &LOC::Request::getInteger('id', 0);
    my $tabLabel = &LOC::Request::getHash('labels', {});
    my %tabData = (
        'businessFamily.id' => &LOC::Request::getInteger('businessFamilyId', 0),
        'accountFamily.id'  => &LOC::Request::getInteger('accountFamilyId', 0),
        'labels'            => $tabLabel,
        'energy'            => &LOC::Request::getString('energy', ''),
        'elevation'         => &LOC::Request::getString('elevation', '')
    );

    # Enregistrement
    if ($tabViewData{'valid'} eq 'ok')
    {
        my $tabErrors = {};
        my $machinesFamilyId = $tabViewData{'id'};
        if ($tabViewData{'id'} > 0)
        {
            &LOC::MachinesFamily::update($countryId, $tabViewData{'id'}, \%tabData, $tabUserInfos->{'id'}, $tabErrors);
        }
        else
        {
            $machinesFamilyId = &LOC::MachinesFamily::insert($countryId, \%tabData, $tabUserInfos->{'id'}, $tabErrors);
        }

        my $nbErrors = @{$tabErrors->{'fatal'}};
        if ($nbErrors > 0)
        {
            $tabViewData{'tabData'} = {
                '@state'  => 'error',
            };

            my @tabJSErrors = (); # 0 pour être sûr d'avoir un numéro d'erreur dans le tableau !!

            # Erreur sur le contrat
            if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
            {
                push(@tabJSErrors, 1); # Erreur fatale
            }
            if (&LOC::Util::in_array(0x0002, $tabErrors->{'fatal'}))
            {
                push(@tabJSErrors, 2); # Tous les champs obligatoires ne sont pas renseignés
            }
            # Si pas d'erreur alors erreur inconnue !!
            if (@tabJSErrors == 0)
            {
                push(@tabJSErrors, 0); # Erreur inconnue !
            }

            $tabViewData{'tabData'}->{'@return'} = \@tabJSErrors;
        }
        else
        {
            $tabViewData{'tabData'} = {
                '@state'  => 'valid',
                '@return' => [($tabViewData{'id'} > 0 ? 0 : 1)],
            };
            $tabViewData{'id'} = $machinesFamilyId;
        }
    }

    # Récupération des informations pour la vue
    if ($tabViewData{'id'})
    {
        $tabViewData{'machinesFamily.infos'} = &LOC::MachinesFamily::getInfos(
                $countryId,
                $tabViewData{'id'},
                LOC::MachinesFamily::GETINFOS_COUNTRYLABELS
            );
    }
    else
    {
        $tabViewData{'machinesFamily.infos'} = \%tabData;
    }

    $tabViewData{'countryList'}        = &LOC::Country::getList(LOC::Util::GETLIST_PAIRS, undef, undef, {'defaultFirst' => 1});
    $tabViewData{'businessFamilyList'} = &LOC::BusinessFamily::getList($countryId, LOC::Util::GETLIST_PAIRS);
    $tabViewData{'businessFamilyList'} = &LOC::Util::sortHashByValues($tabViewData{'businessFamilyList'});
    $tabViewData{'accountFamilyList'}  = &LOC::AccountFamily::getList($countryId, LOC::Util::GETLIST_PAIRS);
    $tabViewData{'accountFamilyList'}  = &LOC::Util::sortHashByValues($tabViewData{'accountFamilyList'});
    $tabViewData{'energyList'}         = &LOC::MachinesFamily::getEnergiesList();

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/machinesFamily/manage.cgi';
}

1;