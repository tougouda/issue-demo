use utf8;
use open (':encoding(UTF-8)');

# Package: telematics
# Contrôleur pour les modèles de machines
package telematics;

use strict;
use File::Basename;

use LOC::StartCode::Static;


# Function: viewAction
# Liste des modèles de machines
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId = $tabUserInfos->{'nomadCountry.id'};
    my $agencyId  = $tabUserInfos->{'nomadAgency.id'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    # Droits
    $tabViewData{'tabRights'} = &LOC::StartCode::getRights();

    my $idToRegenerate = &LOC::Request::getInteger('idToRegenerate', undef),
    my $startCodeIdToRegenerate = &LOC::Request::getInteger('startCodeIdToRegenerate', undef),
    my $tabErrors = {};

    # Regénération du code
    if ($idToRegenerate)
    {
        if ($tabViewData{'tabRights'}->{'actions'}->{'renew'})
        {
            # Génération du code statique
            if (&LOC::StartCode::Static::generate($countryId, $idToRegenerate, $startCodeIdToRegenerate, $tabUserInfos->{'id'}, undef, $tabErrors))
            {
                $tabViewData{'tabData'} = {
                    '@state'  => 'ok',
                    '@valid'  => 'success'
                };
            }
            else
            {
                # Un erreur s'est produite
                $tabViewData{'tabData'} = {
                    '@state'  => 'ko',
                    '@perlErrors' => $tabErrors
                };

                my @tabCodeErrors = (); # 0 pour être sûr d'avoir un numéro d'erreur dans le tableau !!

                # Erreur sur la gestion des codes statiques
                if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
                {
                    push(@tabCodeErrors, 'fatalError'); # Erreur fatale
                }
                if (&LOC::Util::in_array(0x0002, $tabErrors->{'errors'}))
                {
                    push(@tabCodeErrors, 'alreadyRegeneratedError'); # Code déjà régénéré
                }

                # Si pas d'erreur alors erreur inconnue !
                if (@tabCodeErrors == 0)
                {
                    push(@tabCodeErrors, 'unknownError'); # Erreur inconnue !
                }

                $tabViewData{'tabData'}->{'@errors'} = \@tabCodeErrors;
            }
        }
        else
        {
            # Pas les droits nécessaires
            $tabViewData{'tabData'}->{'@errors'} = ['rightsError'];
        }
    }

    # Liste des types de codes
    $tabViewData{'tabTypesList'} = &LOC::StartCode::Static::getTypesList($countryId, LOC::Util::GETLIST_ASSOC);

    my %tabData;

    # Récupération des codes statiques actifs
    my $tabFilters = {
        'stateId' => LOC::StartCode::STATE_ACTIVE
    };
    my $tabActivesList = &LOC::StartCode::Static::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                          $tabFilters,
                                                          LOC::StartCode::GETINFOS_RULESLIST);

   # Récupération des codes statiques obsolètes avec au moins une règle courante
    my $tabFilters = {
        'rule' => {
            'isCurrent' => 1
        },
        'stateId' => LOC::StartCode::STATE_OBSOLETE
    };
    my $tabObsoletesList = &LOC::StartCode::Static::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                            $tabFilters,
                                                            LOC::StartCode::GETINFOS_RULESLIST);

    # Récupération des informations par type de code
    foreach my $tabInfos (values %$tabActivesList)
    {
        if (!defined $tabData{$tabInfos->{'type.id'}})
        {
            $tabData{$tabInfos->{'type.id'}} = {
                'id'                 => $tabInfos->{'type.id'},
                'label'              => $tabInfos->{'type.name'},
                'order'              => $tabInfos->{'type.order'},
                'startCodeId'        => $tabInfos->{'id'},
                'count'              => {},
                'pendingParkNumbers' => []
            };
        }

        # Récupération du code actif et informations associées
        $tabData{$tabInfos->{'type.id'}}->{'code'} = $tabInfos->{'code'};

        my $nbActiveRules  = 0;
        my $nbPendingRules = 0;

        foreach my $tabRuleInfos (values %{$tabInfos->{'rulesList'}})
        {
            if ($tabRuleInfos->{'state.id'} eq LOC::StartCode::STATE_RULE_ACTIVE)
            {
                if ($tabRuleInfos->{'application.id'} eq LOC::StartCode::STATE_RULE_APPLICATION_DONE)
                {
                    $nbActiveRules++;
                }
                if ($tabRuleInfos->{'application.id'} eq LOC::StartCode::STATE_RULE_APPLICATION_PENDING)
                {
                    $nbPendingRules++;
                    # permet de récupérer également les machines qui n'ont pas de code précédent (nouvelles machines)
                    push(@{$tabData{$tabInfos->{'type.id'}}->{'pendingParkNumbers'}}, $tabRuleInfos->{'machine.parkNumber'});
                }
            }
        }
        $tabData{$tabInfos->{'type.id'}}->{'count'} = {
            'active'  => $nbActiveRules,
            'pending' => $nbPendingRules
        };
    }

    foreach my $tabInfos (values %$tabObsoletesList)
    {
        # Récupération des machines en attente
        if (defined $tabData{$tabInfos->{'type.id'}})
        {
            foreach my $tabRuleInfos (values %{$tabInfos->{'rulesList'}})
            {
                # Ajoute la machine dans la liste des codes en cours d'envoi si
                # - l'ancienne règle est appliquée
                if ($tabRuleInfos->{'application.id'} eq LOC::StartCode::STATE_RULE_APPLICATION_DONE)
                {
                    $tabData{$tabInfos->{'type.id'}}->{'pending'}->{$tabRuleInfos->{'machine.parkNumber'}} = $tabInfos->{'code'};
                }
            }
        }
    }

    $tabViewData{'tabList'} = \%tabData;


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/telematics/view.cgi';
}

1;