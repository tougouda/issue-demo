#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;

my $view = LOC::Html::Standard->new();

$view->setPageTitle('Double connection');

print $view->displayHeader();

my $errors = ['<p>Vous êtes déjà connecté avec une autre adresse IP</p>
<p>You are already connected with another address IP</p>
<p>Ya se les conecta con otra dirección IP</p>
<p>Já é conectada com um outro endereço IP</p>
<p>Siete già collegati con un\'altro indirizzo IP</p>'];

print $view->displayMessages('warning', $errors, 0);

print $view->displayFooter();