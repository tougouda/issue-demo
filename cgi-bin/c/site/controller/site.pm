use utf8;
use open (':encoding(UTF-8)');

# Package: site
# Contrôleur pour les chantiers
package site;

use strict;
use File::Basename;
use LOC::SiteLocality;

# Function: siteLocalitiesListAction
# Récupérer la liste des villes chantier (avec recherche possible sur id ou le nom de ville)
# en json
sub siteLocalitiesListAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id' => $tabUserInfos->{'locale.id'},
        'errorCode' => ''
    );

    # Récupération des paramètres
    my $searchValue  = &LOC::Util::trim(&LOC::Request::getString('searchValue', ''));
    my $countryId    = &LOC::Request::getString('country.id');
    my $format       = &LOC::Request::getInteger('format', 0);
    my $transportAgc = &LOC::Request::getString('transportAgency', '');

    if ($searchValue eq '')
    {
        $tabViewData{'errorCode'} = 'emptysearch';
        $tabViewData{'result'} = {
            'returnCode' => 'error'
        };
    }
    else
    {
        # Liste des modèles de machines
        my $tabFilters = {'search' => $searchValue};
        my $tabOptions = {};
        if ($transportAgc ne '')
        {
            $tabOptions->{'transportAgency'} = $transportAgc;
        }

        # Récupération de la liste des villes
        my $nbResult = &LOC::SiteLocality::getList($countryId, LOC::Util::GETLIST_COUNT, $tabFilters, $tabOptions);
        if ($nbResult == 0)
        {
            $tabViewData{'errorCode'} = 'noresult';
            $tabViewData{'result'} = {
                'returnCode' => 'error'
            };
        }
        elsif ($nbResult < 100)
        {
            my $result = &LOC::SiteLocality::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters, $tabOptions);

            my $tab = [];
            if ($format == 0)
            {
                foreach (keys %$result)
                {
                    push(@$tab, {'id'         => $result->{$_}->{'id'} * 1,
                                 'name'       => $result->{$_}->{'name'},
                                 'postalCode' => $result->{$_}->{'postalCode'},
                                 'fullName'   => $result->{$_}->{'fullName'},
                                 'zone'       => ($result->{$_}->{'zone'} * 1)});
                }
            }
            elsif ($format == 1)
            {
                foreach (keys %$result)
                {
                    push(@$tab, [$result->{$_}->{'id'} * 1, $result->{$_}->{'fullName'}]);
                }
            }
            $tabViewData{'result'} = {
                'returnCode' => 'ok',
                'format'     => $format,
                'tabData'    => $tab
            };
        }
        else
        {
            $tabViewData{'errorCode'} = 'exceeded';
            $tabViewData{'result'} = {
                'returnCode' => 'warning'
            };
        }
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/site/siteLocalitiesList' . ($tabViewData{'errorCode'} ne '' ? 'Err' : '') . '.cgi';
}

1;
