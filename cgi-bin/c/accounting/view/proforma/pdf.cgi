use utf8;
use open (':encoding(UTF-8)');
binmode STDOUT, ":raw";    # Pour envoyer le flux en l'état

use strict;
use locale;

use LOC::Date;
use LOC::Locale;
use LOC::Pdf;
use LOC::Util;
use LOC::Browser;




our $tabViewData;

# Locale
my $locale = &LOC::Locale::getLocale($tabViewData->{'locale.id'});

# Dimensions de la page
my $margin = 10/mm;
my $pageWidth = 210/mm;
my $textWidth = $pageWidth - 2 * $margin;

my $pageHeight = 297/mm;
my $textHeight = $pageHeight - 2 * $margin;

my $pageLimit = $margin;

my $gap = 2/pt;

# Création de l'objet PDF
my $title = '';
my $type  = $tabViewData->{'type'};
if ($type == LOC::Proforma::Type::ID_PROFORMA || $type == LOC::Proforma::Type::ID_EXTENSION)
{
    $title = $locale->t('Facture pro forma');
}
elsif ($type == LOC::Proforma::Type::ID_ADVANCE)
{
    $title = $locale->t('Facture d\'acompte');
}
elsif ($type == 0)
{
    $title = $locale->t('Bon de valorisation');
}

my $pdf = LOC::Pdf->new($title, $tabViewData->{'agency.id'}, \$locale);
my $pdfObj = $pdf->getPdfObject();

# Polices
my $regularFont    = $pdf->{'font'}->{'regular'};
my $italicFont     = $pdf->{'font'}->{'italic'};
my $boldFont       = $pdf->{'font'}->{'bold'};
my $boldItalicFont = $pdf->{'font'}->{'bolditalic'};
my $dingbatsFont   = $pdfObj->corefont('ZapfDingbats');


# Définition du sujet et des mots-clés
$pdf->setSubject($title . ', ' . $locale->t('Client') . ' ' . $tabViewData->{'tabCustomer'}->{'name'});
$pdf->setKeywords($title);


# Nouvelle page
my $page = $pdf->addPage();
my $text = $page->text();
my $gfx  = $page->gfx();



# Numéro et date du document
my $number = $locale->t('du %s', $locale->getDateFormat($tabViewData->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC));
# Si pas bon de valorisation, on ajoute le n° de pro forma
if ($tabViewData->{'type'} != 0)
{
    $number = $locale->t('N° %s du %s', $tabViewData->{'id'}, $locale->getDateFormat($tabViewData->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC));
}
$gfx->textlabel(10/mm + 95/mm, 297/mm - 35/mm - 16/pt * 1.2, $boldFont, 10/pt, $number);


my $x = 10/mm + 95/mm;
my $y = 297/mm - 42/mm - 16/pt * 1.2;

# Annule et remplace une ancienne pro forma
if ($tabViewData->{'tabLink'}->{'id'} && 
    (($tabViewData->{'type'} eq LOC::Proforma::Type::ID_ADVANCE && $tabViewData->{'tabLink'}->{'type'} == $tabViewData->{'type'}) ||
     ($tabViewData->{'type'} ne LOC::Proforma::Type::ID_ADVANCE && $tabViewData->{'tabLink'}->{'type'} ne LOC::Proforma::Type::ID_ADVANCE))
   )
{
    $text->translate($x, $y);
    my $textLink = $locale->t('Annule et remplace la %s n°%s du %s', 
                                lc($tabViewData->{'tabLink'}->{'label'}), 
                                $tabViewData->{'tabLink'}->{'id'}, 
                                $locale->getDateFormat($tabViewData->{'tabLink'}->{'creationDate'}, LOC::Locale::FORMAT_DATE_NUMERIC));
    $gfx->textlabel($x, $y, $regularFont, 8/pt, $textLink);
    $y -= 4/mm;
}

# Prolonge une pro forma
if ($tabViewData->{'tabLinkExtension'}->{'id'})
{
    $text->translate($x, $y);
    my $textLinkExtension = $locale->t('Prolongation de la location de la pro forma n°%s du %s', 
                                            $tabViewData->{'tabLinkExtension'}->{'id'}, 
                                            $locale->getDateFormat($tabViewData->{'tabLinkExtension'}->{'creationDate'}, LOC::Locale::FORMAT_DATE_NUMERIC));
    $gfx->textlabel($x, $y, $regularFont, 8/pt, $textLinkExtension);
}

# Contacts
$pdf->addContactsBlock(\$page, ($tabViewData->{'negotiatorFullName'} ne '' ? [{'label' => $locale->t('Négociateur'), 'value' => $tabViewData->{'negotiatorFullName'}}] : undef));

# Origine (devis ou contrat)
my $tabOriginLabel = {'estimateLine' => $locale->t('Devis') . ' :', 'contract' => $locale->t('Contrat') . ' :'};

# Références
tie(my %tabReferences, 'Tie::IxHash');
my $phoneNumber  = $locale->getPhoneNumberFormat($tabViewData->{'tabCustomer'}->{'telephone'});
my $faxNumber    = $locale->getPhoneNumberFormat($tabViewData->{'tabCustomer'}->{'fax'});
my $documentLine = ($tabViewData->{'document.line'} > 0 ? ' (' . $locale->t('ligne %s', $tabViewData->{'document.line'}) . ')' : ''); 
%tabReferences = (
                  $locale->t('Code client') . ' :'          => $tabViewData->{'tabCustomer'}->{'code'},
                  $locale->t('Téléphone') . ' :'            => $phoneNumber,
                  $locale->t('Fax') . ' :'                  => $faxNumber,
                  $tabOriginLabel->{$tabViewData->{'document.type'}} => $tabViewData->{'document.code'} . $documentLine,
                 );
if ($tabViewData->{'document.orderNo'})
{
    $tabReferences{$locale->t('N° de commande') . ' :'} = $tabViewData->{'document.orderNo'};
}
$tabReferences{$locale->t('Chantier') . ' :'} = $tabViewData->{'document.site'};

$pdf->addReferencesBlock(\$page, \%tabReferences);

# Coordonnées client
my $tabCustomer = [
                   $tabViewData->{'tabCustomer'}->{'name'},
                   $tabViewData->{'tabCustomer'}->{'address1'},
                   $tabViewData->{'tabCustomer'}->{'address2'},
                   $tabViewData->{'tabCustomer'}->{'locality.postalCode'} . ' ' . $tabViewData->{'tabCustomer'}->{'locality.name'},
                   ' ',
                   $tabViewData->{'tabCustomer'}->{'orderContact.fullName'} . ' ' . $tabViewData->{'tabCustomer'}->{'orderContact.telephone'}
                  ];
$pdf->addCustomerBlock(\$page, $tabCustomer);


# Marque obsolète
# - pro forma (pas les bons de valorisation) ET
# - non imprimable OU non active
if ($type && (!$tabViewData->{'isPrintable'} || $tabViewData->{'state.id'} ne LOC::Proforma::STATE_ACTIVE))
{
    $pdf->addStamp($page, uc($locale->t('Annulé')));
}


my $x = $margin;
my $y = $pageHeight - 95/mm;

# En-tête du tableau
my $qtyColWidth = 0/mm;
my $amountColWidth = 25/mm;
my $unitPriceColWidth = 0/mm;

if (LOC::Util::in_array($type, [0, LOC::Proforma::Type::ID_PROFORMA, LOC::Proforma::Type::ID_EXTENSION]))
{
    $unitPriceColWidth = 25/mm;
    $qtyColWidth = 15/mm;
}
my $designationColWidth = $pageWidth - 2 * $margin - $qtyColWidth - $unitPriceColWidth - $amountColWidth + $gap;
my $adjustmentColDeposit = $designationColWidth - 25/mm - 15/mm;

&printHeaderTabContent();

my $isLineCarbExist = 0;
# Location
foreach my $line (@{$tabViewData->{'tabLines'}})
{
    my $label = $locale->tpl($line->{'label'});
    my $complement = $locale->tpl($line->{'labelExtra'});
    my $quantity = &LOC::Util::toNumber($line->{'quantity'});
    my $unitPrice = $line->{'unitPrice'};
    my $amount = $line->{'quantity'} * $line->{'unitPrice'};

    if ($type == LOC::Proforma::Type::ID_ADVANCE)
    {
        $y = &displayTableLineTwoCells($y, $label, $unitPrice, $complement);
    }
    else
    {
        $y = &displayTableLineFourCells($y, $label, $quantity, $unitPrice, $amount, $complement);
    }
    # est-ce que la ligne carburant existe ?
    if ($line->{'articleRef'} eq 'CARB')
    {
        $isLineCarbExist = 1;
    }
    &checkRemainingSpace($y - $pageLimit - 20/mm, 'tabContent');
}

if ($type == LOC::Proforma::Type::ID_ADVANCE)
{
    $designationColWidth = $adjustmentColDeposit;
}

# Vérification de l'espace disponible
&checkRemainingSpace($y - $pageLimit - 20/mm);


$x = $pageWidth - $margin - $amountColWidth;
$y -= 2/mm;
$gfx->linewidth(0.5/pt);
$gfx->move($x + $gap, $y);
$gfx->line($pageWidth - $margin, $y);
$gfx->stroke();

# Montant total HT
$text->font($regularFont, 10/pt);
$text->lead(10/pt);
$x = $margin + $designationColWidth;

$y -= $text->lead() + 1/mm;
$text->translate($x, $y);
$text->text($locale->t('Montant total HT'));

$x = $pageWidth - $margin;
$text->translate($x, $y);
$text->text_right($locale->getCurrencyFormat($tabViewData->{'total'}, undef, $tabViewData->{'currencySymbol'}), -maxWidth => $amountColWidth - $gap);

if (defined $tabViewData->{'tabVAT'})
{
    foreach my $vat (@{$tabViewData->{'tabVAT'}})
    {
        # Montant TVA
        $x = $margin + $designationColWidth;
        $y -= $text->lead() + 1/mm;
        $text->translate($x, $y);
        $text->text($locale->t('TVA %s %%', $vat->{'vatRate'} * 100));
        
        $x = $pageWidth - $margin;
        $text->translate($x, $y);
        $text->text_right($locale->getCurrencyFormat($vat->{'amount'}, undef, $tabViewData->{'currencySymbol'}), -maxWidth => $amountColWidth - $gap);
    }
}

my $isAlreadyPaid = ($tabViewData->{'paidAmount'} != 0);

my $lineWidth = ($isAlreadyPaid || ($tabViewData->{'depositTotal'} && $tabViewData->{'deposit'} > 0) ? 0.5/pt : 1/pt);

$x = $pageWidth - $margin - $amountColWidth;
$y -= 2/mm;
$gfx->linewidth($lineWidth);
$gfx->move($x + $gap, $y);
$gfx->line($pageWidth - $margin, $y);
$gfx->stroke();

if (!$isAlreadyPaid && !($tabViewData->{'depositTotal'} && $tabViewData->{'deposit'} > 0))
{
    $text->font($boldFont, 12/pt);
    $text->lead(12/pt);
}

# Montant total TTC
$x = $margin + $designationColWidth;
$y -= $text->lead() + 1/mm;
$text->translate($x, $y);
$text->text($locale->t('Montant total TTC'));

$x = $pageWidth - $margin;
$text->translate($x, $y);
$text->text_right($locale->getCurrencyFormat($tabViewData->{'totalTaxesIncluded'}, undef, $tabViewData->{'currencySymbol'}),
                  -maxWidth => $amountColWidth - $gap);

my $finalTotal = 0;

if ($tabViewData->{'depositTotal'} && $tabViewData->{'deposit'} > 0)
{
    # dépôt de garantie dans les montants
    $x = $margin + $designationColWidth;
    $y -= $text->lead() + 1/mm;
    $text->translate($x, $y);
    $text->text($locale->t('Dépôt de garantie'));

    $x = $pageWidth - $margin;
    $text->translate($x, $y);
    $text->text_right($locale->getCurrencyFormat($tabViewData->{'deposit'}, undef, $tabViewData->{'currencySymbol'}),
                      -maxWidth => $amountColWidth - $gap);
    $finalTotal = 1;
}

if ($isAlreadyPaid)
{
    # Déjà versé
    $x = $margin + $designationColWidth;
    $y -= $text->lead() + 1/mm;
    $text->translate($x, $y);
    $text->text($locale->t('Déjà versé') . ' *');
    
    $x = $pageWidth - $margin;
    $text->translate($x, $y);
    my $paidAmount = $tabViewData->{'paidAmount'};
    if ($tabViewData->{'paidAmountNegative'})
    {
        $paidAmount *= -1;
    }
    $text->text_right($locale->getCurrencyFormat($paidAmount, undef, $tabViewData->{'currencySymbol'}),
                      -maxWidth => $amountColWidth - $gap);

    $finalTotal = 1;
}

if ($finalTotal)
{
    $lineWidth = 1/pt;
    $x = $pageWidth - $margin - $amountColWidth;
    $y -= 2/mm;
    $gfx->linewidth($lineWidth);
    $gfx->move($x + $gap, $y);
    $gfx->line($pageWidth - $margin, $y);
    $gfx->stroke();

    $text->font($boldFont, 12/pt);
    $text->lead(12/pt);

    # Montant total final
    $x = $margin + $designationColWidth;
    $y -= $text->lead() + 1/mm;
    $text->translate($x, $y);
    $text->text($locale->t('Solde à payer'));

    $x = $pageWidth - $margin;
    $text->translate($x, $y);
    $text->text_right($locale->getCurrencyFormat($tabViewData->{'finalTotal'}, undef, $tabViewData->{'currencySymbol'}),
                      -maxWidth => $amountColWidth - $gap);
}


# Dépôt de garantie
if ($tabViewData->{'deposit'} > 0)
{
    $x = $margin;
    $y -= 10/mm;

    # Vérification de l'espace disponible
    &checkRemainingSpace($y - $pageLimit - 8/pt);

    $gfx->textlabel($x, $y, $regularFont, 9/pt, uc($locale->t('Dépôt de garantie')),
                    -color => LOC::Pdf::COLOR_EMPHASIS);
    $y -= 9/pt * 1.2;

    $text->font($regularFont, 10/pt);
    $text->translate($x, $y);
    $text->lead(10/pt);

    my $remainingText = $locale->t('Un dépôt de garantie de %s a été versé en couverture d\'éventuels soldes dus sur votre location ou de dégradations survenues sur le matériel.',
                                   $locale->getCurrencyFormat($tabViewData->{'deposit'}, undef, $tabViewData->{'currencySymbol'}));
    if ($tabViewData->{'depositTotal'})
    {
        $remainingText = $locale->t('Un dépôt de garantie a été versé en couverture d\'éventuels soldes dus sur votre location ou de dégradations survenues sur le matériel.');
    }
    
    while ($remainingText = $text->section($remainingText, $pageWidth - 2 * $margin, $text->lead(), -align => 'justified'))
    {
        $y -= $text->lead();
    }
}



# Emplacement du mode de règlement
my $paymentY = $pageHeight - 220/mm;

## Commentaires
if ($tabViewData->{'comment'} ne '')
{
    $x = $margin;
    $y -= 10/mm;

    # Vérification de l'espace disponible
    &checkRemainingSpace($y - $pageLimit - 8/pt);

    $gfx->textlabel($x, $y, $regularFont, 9/pt, uc($locale->t('Commentaires')),
                    -color => LOC::Pdf::COLOR_EMPHASIS);
    $y -= 9/pt * 1.2;

    $text->font($regularFont, 10/pt);
    $text->translate($x, $y);
    $text->lead(10/pt);

    my $availableHeight = $paymentY - $pageLimit - 10/mm;
    my $overflowText = $tabViewData->{'comment'};
    my $height;
    ($overflowText, $height) = $text->section($overflowText, $pageWidth - 2 * $margin, $availableHeight,
                                              -align => 'justified');

    while ($overflowText)
    {
        $page = $pdf->addPage(LOC::Pdf::DISPLAY_NONE);
        $gfx  = $page->gfx();
        $text = $page->text();
        if (!$tabViewData->{'isPrintable'})
        {
            $pdf->addStamp($page, uc($locale->t('Obsolète')));
        }
        $y = $pageHeight - $margin;

        $gfx->textlabel($x, $y, $regularFont, 9/pt, uc($locale->t('Commentaires')),
                        -color => LOC::Pdf::COLOR_EMPHASIS);
        $y -= 9/pt * 1.2;

        $text->font($regularFont, 10/pt);
        $text->translate($x, $y);
        $text->lead(10/pt);

        $availableHeight = $paymentY - $pageLimit - 10/mm;

        ($overflowText, $height) = $text->section($overflowText, $pageWidth - 2 * $margin, $availableHeight,
                                                  -align => 'justified');
    }

    $y -= $availableHeight - $height;
}



# Nouvelle page s'il n'y a pas assez de place
if ($y < $paymentY)
{
    $page = $pdf->addPage(LOC::Pdf::DISPLAY_NONE);
    $gfx  = $page->gfx();
    $text = $page->text();
    if (!$tabViewData->{'isPrintable'})
    {
        $pdf->addStamp($page, uc($locale->t('Obsolète')));
    }
}


# Règlement
$x = $margin;

if ($tabViewData->{'tabBank'})
{
    $y = $pageHeight - 223/mm;
}
else
{
    $y = $pageHeight - 243/mm;
}

$gfx->textlabel($x, $y, $regularFont, 9/pt, uc($locale->t('Règlement')), -color => LOC::Pdf::COLOR_EMPHASIS);
$y -= 9/pt * 1.2;

# Mode de règlement
$text->font($regularFont, 10/pt);
$text->lead(10/pt);
$text->translate($x, $y);
$text->text(sprintf('%s.', $tabViewData->{'paymentMode.label'}), -maxWidth => $pageWidth - 2 * $margin);
$y -= $text->lead();
$text->translate($x, $y);
$text->text($locale->t('Sans escompte en cas de paiement anticipé.'), -maxWidth => $pageWidth - 2 * $margin);



if ($tabViewData->{'tabBank'})
{
    $gfx->linewidth(0.5/pt);
    $x = $margin;
    $y -= $text->lead();
    $gfx->move($x, $y);
    $gfx->line($pageWidth - $margin, $y);
    $gfx->stroke();
    
    $y -= 8/mm;
    # RIB
    $text->font($regularFont, 10/pt);
    $text->translate($x, $y);
    my $l0 = $text->text($locale->t('RIB') . ' :', -maxWidth => $pageWidth - 2 * $margin);
    
    $x += $l0 + 5/mm;
    my $cptCol = 1;
    foreach my $ribElement (@{$tabViewData->{'tabBank'}->{'bban'}})
    {
        my @tabElement = split(':', $ribElement);
        $text->font($regularFont, 10/pt);
        $text->translate($x, $y);
        my $l1 = $text->text($tabElement[1], -maxWidth => $pageWidth - 2 * $margin);
        $text->font($italicFont, 8/pt);
        $text->translate($x, $y + 4/mm);
        my $l2 = $text->text($tabElement[0], -maxWidth => $pageWidth - 2 * $margin);
        
        my $maxCol = ($l1 > $l2 ? $l1 : $l2);
        $x += $maxCol + 7/mm;

        $cptCol++;
    }

    $text->font($regularFont, 10/pt);
    $text->translate($x, $y);
    $text->text($tabViewData->{'tabBank'}->{'bankName'}, -maxWidth => $pageWidth - 2 * $margin);
    $text->font($italicFont, 8/pt);
    $text->translate($x, $y + 4/mm);
    $text->text($locale->t('Domiciliation'), -maxWidth => $pageWidth - 2 * $margin);
    
    # IBAN
    $x = $margin;
    $y -= 6/mm;
    $text->font($regularFont, 10/pt);
    $text->translate($x, $y);
    my $l3 = $text->text($locale->t('IBAN') . ' :', -maxWidth => $pageWidth - 2 * $margin);
    
    $x += $l3 + 5/mm;
    $text->font($regularFont, 10/pt);
    $text->translate($x, $y);
    my $l4 = $text->text($tabViewData->{'tabBank'}->{'iban'}, -maxWidth => $pageWidth - 2 * $margin);
    
    $x += $l4 + 10/mm;
    $text->font($regularFont, 10/pt);
    $text->translate($x, $y);
    my $l5 = $text->text($locale->t('BIC') . ' :', -maxWidth => $pageWidth - 2 * $margin);
    
    $x += $l5 + 5/mm;
    $text->font($regularFont, 10/pt);
    $text->translate($x, $y);
    $text->text($tabViewData->{'tabBank'}->{'bic'}, -maxWidth => $pageWidth - 2 * $margin);
    
    $gfx->linewidth(0.5/pt);
    $x = $margin;
    $y -= 2/mm;
    $gfx->move($x, $y);
    $gfx->line($pageWidth - $margin, $y);
    $gfx->stroke();
}


# Mentions légales
$x = $margin;
$y = $pageHeight - 258/mm;
$text->translate($x, $y);
$text->font($regularFont, 10/pt);
$text->lead(10/pt);
$text->section($tabViewData->{'legal'}, $pageWidth - 2 * $margin, $y - $pageLimit - 8/pt, -align => 'justified');

# Mention Bon de valorisation
if ($type == 0 && !$isLineCarbExist && $tabViewData->{'estimateFuelAmount'} > 0 && $tabViewData->{'isFuelNeeded'})
{
    $x = $margin;
    $y -= 12/mm;
    $text->translate($x, $y);
    $text->section($locale->t('Le montant de ce bon de valorisation ne tient pas compte du carburant (ce montant est estimé à %s).', 
                               $locale->getCurrencyFormat($tabViewData->{'estimateFuelAmount'}, undef, $tabViewData->{'currencySymbol'})), 
                   $pageWidth - 2 * $margin, $y - $pageLimit - 8/pt, -align => 'justified');
}

# Notes de bas de page
if ($isAlreadyPaid)
{
    $x = $margin;
    $y = $pageHeight - 280/mm;
    $gfx->linewidth(0.5/pt);
    $gfx->move($x, $y + 3/mm);
    $gfx->line($x + 50/mm, $y + 3/mm);
    $gfx->stroke();
    $text->translate($x, $y);
    $text->font($regularFont, 8/pt);
    $text->lead(8/pt);
    $text->section('* ' . $locale->t('Sous réserve d\'encaissement'), $pageWidth - 2 * $margin, $y - $pageLimit - 8/pt,
                   -align => 'justified');
}


# Numéros de page
&displayPageNumbers();

# Sortie
my $pdfString = $pdf->getString();
$pdf->end();

my $filename = sprintf('Proforma_%s_%s_%s.pdf', $tabViewData->{'tabCustomer'}->{'code'},
                                                $tabViewData->{'document.code'},
                                                $tabViewData->{'date'});
&LOC::Browser::sendHeaders(
        "Content-type: application/pdf;\n"
        . "Content-Disposition: inline; filename=\"" . $filename . "\";\n\n"
    );

print $pdfString;



# Function: checkRemainingSpace
# Vérification de l'espace restant dans la page
#
# Parameters:
# string $value - Ordonnée ($y par défaut)
# string $origin - si tabContent, on répète l'entete du tableau des désignations
#
# Returns:
# bool - 1 si l'espace restant est suffisant, 0 sinon
sub checkRemainingSpace
{
    my ($value, $origin) = @_;

    unless (defined $value)
    {
        $value = $y;
    }

    if ($value < $pageLimit)
    {
        $page = $pdf->addPage(LOC::Pdf::DISPLAY_FULLHEADER);
        $gfx  = $page->gfx();
        $text = $page->text();
        if (!$tabViewData->{'isPrintable'})
        {
            $pdf->addStamp($page, uc($locale->t('Obsolète')));
        }
        $y = $pageHeight - 55/mm;

        if (defined($origin) && $origin eq 'tabContent')
        {
            &printHeaderTabContent();
        }
        return 1;
    }
    return 0;
}

# Function: printHeaderTabContent
# Affichage de la ligne d'entete du tableau des désignations
#
sub printHeaderTabContent
{
    $x  = $margin;
    if ($tabViewData->{'document.orderNo'})
    {
        $text->lead(10/pt);
        $y -= $text->lead() + 1/mm;
    }
    $text->font($boldFont, 8/pt);
    $gfx->linewidth(1/pt);
    
    $text->translate($x, $y);
    $text->text($locale->t('Désignation'), -maxWidth => $designationColWidth - $gap);
    $gfx->move($x, $y - 1/mm);
    $x += $designationColWidth;
    $gfx->line($x - $gap, $y - 1/mm);
    
    if (LOC::Util::in_array($type, [0, LOC::Proforma::Type::ID_PROFORMA, LOC::Proforma::Type::ID_EXTENSION]))
    {
        $x += $qtyColWidth;
        $text->translate($x - $gap, $y);
        $text->text_right($locale->t('Quantité'), -maxWidth => $qtyColWidth - $gap);
        $gfx->move($x - $gap, $y - 1/mm);
        $gfx->line($x - $qtyColWidth, $y - 1/mm);
    
        $x += $unitPriceColWidth;
        $text->translate($x - $gap, $y);
        $text->text_right($locale->t('Prix unitaire'), -maxWidth => $unitPriceColWidth - $gap);
        $gfx->move($x - $gap, $y - 1/mm);
        $gfx->line($x - $unitPriceColWidth, $y - 1/mm);
    }
    $x += $amountColWidth;
    $text->translate($x - $gap, $y);
    $text->text_right($locale->t('Montant'), -maxWidth => $amountColWidth - $gap);
    $gfx->move($x - $gap, $y - 1/mm);
    $gfx->line($x - $amountColWidth, $y - 1/mm);
    
    $gfx->stroke();
    
    # Lignes du tableau
    $text->font($regularFont, 10/pt);
    $text->lead(10/pt);
    $y -= $text->lead() + 1/mm;

    return 1;
}

# Function: displayPageNumbers
# Affichage du numéro de page
sub displayPageNumbers
{
    my $nbPages = $pdfObj->pages();
    
    for (my $i = 0; $i < $nbPages; $i++)
    {
        my $page = $pdfObj->openpage($i + 1);
        my $text = $page->text();
        
        $text->font($italicFont, 8/pt);
        $text->translate($pageWidth - $margin, 1.5 * $margin);
        $text->text_right(sprintf('%d/%d', $i + 1, $nbPages));
    }
}

# Function: displayTableLine
# Affichage d'une ligne du tableau
#
# Parameters:
# string $value - Ordonnée ($y par défaut)
#
# Returns:
# bool - 1 si l'espace restant est suffisant, 0 sinon
sub displayTableLineTwoCells
{
    my ($y, $label, $amount, $complement) = @_;

    $text->font($regularFont, 10/pt);
    $text->lead(10/pt);

    my $x = $margin;
    $text->translate($x, $y);
    $text->text($label, -maxWidth => $designationColWidth - $gap);

    $x += $designationColWidth + $amountColWidth;
    $text->translate($x - $gap, $y);
    $text->text_right($locale->getCurrencyFormat($amount, undef, $tabViewData->{'currencySymbol'}), -maxWidth => $amountColWidth - $gap);

    # Complément
    if ($complement ne '')
    {
        $text->font($regularFont, 8/pt);
        $text->lead(8/pt);
        $x = $margin;
        $y -= $text->lead();

        $text->translate($x, $y);
        my $availableWidht  = $pageWidth - 2 * $margin;
        my $availableHeight = $y - $pageLimit - 8/pt;
        my ($remainingText, $height) = $text->section($complement, $availableWidht, $availableHeight, -align => 'justified');
        $y -= $availableHeight - $height;
    }
    else
    {
        $y -= $text->lead();
    }
    $y -= 1/mm;

    return $y;
}

# Function: displayTableLine
# Affichage d'une ligne du tableau
#
# Parameters:
# string $value - Ordonnée ($y par défaut)
#
# Returns:
# bool - 1 si l'espace restant est suffisant, 0 sinon
sub displayTableLineFourCells
{
    my ($y, $label, $quantity, $unitPrice, $amount, $complement) = @_;

    $text->font($regularFont, 10/pt);
    $text->lead(10/pt);

    my $x = $margin;
    $text->translate($x, $y);
    $text->text($label, -maxWidth => $designationColWidth - $gap);

    $x += $designationColWidth + $qtyColWidth;
    $text->translate($x - $gap, $y);
    $text->text_right($quantity, -maxWidth => $amountColWidth - $gap);

    $x += $unitPriceColWidth;
    $text->translate($x - $gap, $y);
    $text->text_right($locale->getCurrencyFormat($unitPrice, undef, $tabViewData->{'currencySymbol'}),
                      -maxWidth => $unitPriceColWidth - $gap);

    $x += $amountColWidth;
    $text->translate($x - $gap, $y);
    $text->text_right($locale->getCurrencyFormat($amount, undef, $tabViewData->{'currencySymbol'}),
                      -maxWidth => $amountColWidth - $gap);

    # Complément
    if ($complement ne '')
    {
        $text->font($regularFont, 8/pt);
        $text->lead(8/pt);
        $x = $margin;
        $y -= $text->lead();

        $text->translate($x, $y);
        my $availableWidth  = $pageWidth - 2 * $margin;
        my $availableHeight = $y - $pageLimit - $text->lead();
        my ($remainingText, $height) = $text->section($complement, $availableWidth, $availableHeight,
                                                      -align => 'justified');
        $y -= $availableHeight - $height;
    }
    else
    {
        $y -= $text->lead();
    }
    $y -= 1/mm;

    return $y;
}
