use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;
use LOC::Json;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'accounting/proforma/';


our %tabViewData;
our $inputsTabIndex = 1;


our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});



sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}

our $currencySymbol = $tabViewData{'currencySymbol'};

# URL courante
my $currentUrl = $view->createURL('accounting:proforma:viewAlerts');
if ($tabViewData{'readOnly'})
{
    $currentUrl = $view->createURL('accounting:proforma:viewAlertsReadOnly');
}


# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'viewAlerts.css');

# JS
$view->addJSSrc($dir . 'viewAlerts.js');
# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc('jQuery/plugins/tablesorter.js');


$view->addPackage('popups');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);


my $title = $locale->t('Règlements pro forma');
$view->setPageTitle($title);
my $subTitle = '';
if ($tabViewData{'readOnly'})
{
    $subTitle = '(' . $locale->t('Commerce') . ')';
}
else
{
    $subTitle = '(' . $locale->t('Comptabilité') . ')';
}
$view->setTitle($locale->t('Règlements pro forma') . ' <i>' . $subTitle . '</i>');

# Réactualisation automatique
if (!$tabViewData{'readOnly'} && $tabViewData{'refreshTime'} >= 5)
{
    my $tabRefreshTimeCfgs = {
        'delay' => $tabViewData{'refreshTime'},
        'title' => $locale->t('Réactualisation automatique')
    };
    $view->addBodyEvent('load', 'refreshTimeObj.init(' . &LOC::Json::toJson($tabRefreshTimeCfgs) . '); refreshTimeObj.start();');
}


# Bouton "Actualiser"
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png',
                                          $currentUrl, $locale->t('Actualiser la page'));
$view->addControlsContent([$refreshBtn], 'right', 1);

my $errorBlockRefreshBtn = '<span onclick="$ge(\'errorBlock\').style.display = \'none\';">' . $refreshBtn . '</span>';
my $fatalErrorMsg = htmllocale('Erreur fatale !') .
    '<p class="details">' .
    $locale->t('Veuillez %s la liste et renouveler l\'opération.', $errorBlockRefreshBtn) . '<br>' .
    htmllocale('Si l\'erreur se reproduit, merci de contacter le support informatique.') .
    '</p>';


# Messages de retour
my $tabTranslations = {
  'fatalError'      => $fatalErrorMsg,
  'updated'         => $locale->t('Les modifications ont été enregistrées'),
  'remainDueError'  => $locale->t('Le reste dû ne peut être égal à 0 ou supérieur au solde à payer dans le cas d\'un encaissement partiel'),
  'unFinalizeError' => $locale->t('La pro forma a été finalisée précédemment. La ligne de devis ou le contrat a été débloqué. Il est impossible de remettre la pro forma en attente.'),
  'modifError'      => $locale->t('La pro forma a été modifiée par un autre utilisateur'),
  'prolongationError' => $locale->t('La prolongation ne peut être effectuée sur ce contrat'),
  'validAgencyError'  => $locale->t('Le commentaire est obligatoire pour la validation par agence')
};

# Types d'erreurs
my $tabErrorTypes = [
    {
        'code' => 'rentTariff',
        'title' => $locale->t('Remise exceptionnelle de location en attente')
    },
    {
        'code' => 'transportTariff',
        'title' => $locale->t('Remise exceptionnelle de transport en attente')
    },
    {
        'code' => 'customerNotVerified',
        'title' => $locale->t('Client avec ouverture de compte en attente')
    }
];

# Message de confirmation de validation avec succès
if ($tabViewData{'messages'}->{'valid'})
{
    my $validBlock = '
    <div id="validBlock" class="messageBlock valid" >
        <div class="popup" style="display: block;">
            <div class="content">
                <table>
                <tr>
                    <td style="font-weight: bold;">' . $tabTranslations->{$tabViewData{'messages'}->{'valid'}} . '.</td>
                </tr>
                <tr class="buttons">
                    <td>' .
                    $view->displayTextButton($locale->t('Ok'), '',
                                             '$ge("validBlock").style.display = "none";',
                                             '', 'l', {'id' => 'validBlockBtn'}) .
                    '</td>
                </tr>
                </table>
            </div>
        </div>
    </div>';
    
    $view->addBodyContent($validBlock, 0, 1);
    $view->addBodyEvent('load', '$("#validBlockBtn").focus();');
}


#Message d'erreur sur une ou plusieurs lignes
my $errorBlock = '
<div id="errorBlock" class="messageBlock error"'; 
($tabViewData{'messages'}->{'error'}) ?  $errorBlock .= 'style="display: visible;"' : $errorBlock .= 'style="display: none;"';
$errorBlock .= '>
    <div class="popup" style="display: block;">
        <div class="content">
            <table>
                <tbody id="errorsTable">
                    <tr>
                        <td>
                            <p style="font-weight : bold;">' . $locale->t('Des erreurs ont été détectées') . '</p>
                            <ul>';
                            #Erreurs retournées depuis PERL
                            while (my ($line, $type) = each(%{$tabViewData{'messagesLine'}}))
                            {
                               $errorBlock .= '<li>' . $locale->t('Erreur ligne n° ') . $line . ' : ' . $tabTranslations->{$type->{'error'}} . '</li>';

                            }
                            $errorBlock .= '</ul>';
                            
                            $errorBlock .= '
                        </td>
                    </tr>
                    <tr class="buttons" id="buttonRow">
                        <td>' .
                        $view->displayTextButton($locale->t('Fermer'), '', '$ge("errorBlock").style.display = "none";') .
                        '</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>';

$view->addBodyContent($errorBlock, 0, 1);

# Légende
my $legendHtml = '<div class="legend">' . 
                    '<span class="title">' . htmllocale('Légende pour les cas erronés') . '&nbsp;:</span>';
foreach my $tabErrorTypeInfos (@$tabErrorTypes)
{
    $legendHtml .= '<div class="button ' . $tabErrorTypeInfos->{'code'} . '">' .
                        '<div class="errorImg ' . $tabErrorTypeInfos->{'code'} . '"></div>' .
                        '<div class="text"><div class="pick"></div>' . $view->toHTMLEntities($tabErrorTypeInfos->{'title'}) . '</div>' .
                   '</div>';
}
$legendHtml .= '</div>';

$view->addControlsContent([$legendHtml], 'left', 1);


# Bouton d'extraction excel
my $excelUrl = $view->createURL('accounting:proforma:viewAlerts', {'displayType' => 'xls'});
if ($tabViewData{'readOnly'})
{
    $excelUrl = $view->createURL('accounting:proforma:viewAlertsReadOnly', {'displayType' => 'xls'});
}
my $excelBtn = $view->displayTextButton($locale->t('Extraction Excel'), 'excel(|Over).gif',
                                        $excelUrl,
                                        $locale->t('Extraction au format Excel'));

$view->addControlsContent($excelBtn, 'right', 0);



# Affichage de l'entête
print $view->displayHeader();


# On n'affiche pas le tableau si il est vide
my @tabProformas = @{$tabViewData{'tabProformas'}};
if (@tabProformas == 0)
{
    # Pas de résultat
    print '<div class="noResult">' . $view->displayMessages('info', [$locale->t('Aucun règlement pro forma en attente')], 0) . '</div>';
}
else
{
    print $view->startForm();


    # Options pour tablesorter
    my $headersOption = '
1  : {sorter: "textAttr"},
2  : {sorter: "textAttr"},
5  : {sorter: "integerAttr"},
6  : {sorter: "integerAttr"},
7  : {sorter: "integerAttr"},
11 : {sorter: "textAttr"}';
    if ($tabViewData{'readOnly'})
    {
        $headersOption .= ',
12 : {sorter: "integerAttr"},
13 : {sorter: "integerAttr"}';
    }
    else
    {
        $headersOption .= ',
12 : {sorter: false},
13 : {sorter: false},
14 : {sorter: false}';
    }
    $headersOption .= ',
15 : {sorter: false}';
    my $sortListOption = '[[4,0],[0,0]]'; # Tri par défaut: "Code client" (ASC) puis Id (ASC)

    print '
<br />
<table class="basic list' . ($tabViewData{'readOnly'} ? ' readOnly' : '') . '">
<thead>
    <tr>
        <th class="id"></th>
        <th class="type"></th>
        <th class="date">' . htmllocale('Date') . '</th>
        <th class="paymentLabel">' . htmllocale('Mode de règlement') . '</th>
        <th class="customerCode">' . htmllocale('Client') . '</th>
        <th class="customerPAL"><span title="' . htmllocale('Paiement avant livraison') . '">' . htmllocale('p.a.l') . '</span></th>
        <th class="amountWithVat">' . htmllocale('Montant TTC') . '</th>
        <th class="total">' . htmllocale('Solde à payer') . '</th>
        <th class="agency"><span title="' . htmllocale('Agence') . '">' . htmllocale('Ag.') . '</span></th>
        <th class="typeRef"><span title="' . htmllocale('Type document') . '">' . htmllocale('Type doc.') . '</span></th>
        <th class="documentCode"><span title="' . htmllocale('Code document') . '">' . htmllocale('Code doc.') . '</span></th>
        <th class="documentBeginDate"><span title="' . htmllocale('Début de location') . '">' . htmllocale('Début de loc.') . '</span></th>
        <th class="justification"><span title="' . htmllocale('Justificatif') . '">' . htmllocale('Justif.') . '</span></th>
        <th class="validation"><span title="' . htmllocale('Validé par l\'agence') . '">' . htmllocale('Val. par ag.') . '</span></th>
        <th class="encashment">' . htmllocale('Encaissement') . '</th>
        <th class="editCtrls"></th>
    </tr>
</thead>
<tbody>';


    my $compteurLine = 0;
    foreach my $tabProformaInfos (@tabProformas)
    {
        $compteurLine++;
        my $proformaId = $tabProformaInfos->{'id'};

        # URL contrat/devis de location
        my $documentUrl = '';
        my $documentCode = '';
        my $documentType = '';
        if ($tabProformaInfos->{'document.type'} eq 'contract')
        {
            $documentUrl = $view->createURL('rent:rentContract:view', {'contractId' => $tabProformaInfos->{'document.id'}});
            $documentCode = $tabProformaInfos->{'document.code'};
            $documentType = $locale->t('Contrat');
        }
        else
        {
            $documentUrl = $view->createURL('rent:rentEstimate:view', {'estimateId'     => $tabProformaInfos->{'document.id'}->[0],
                                                                       'estimateLineId' => $tabProformaInfos->{'document.id'}->[1]});
            $documentCode = $tabProformaInfos->{'document.code'}->[0] . '<br /><i>(' . $locale->t('ligne %s', $tabProformaInfos->{'document.code'}->[1]) . ')</i>';
            $documentType = $locale->t('Devis');
        }

        my $class = 'type_' . $tabProformaInfos->{'type.id'} . ' ' .
                    'encashment_' . $tabProformaInfos->{'encashment.type'};
        my $startState = '';
        my $toUpdate = 0;
        if ($tabProformaInfos->{'isValidated'})
        {
            $class .= ' validate';
            $startState = 'validate';
        }

        if ($tabViewData{'infoLine'}->{$proformaId} eq 'isUpdated')
        {
            $class .= ' isUpdated';
            $startState = 'isUpdated';
        }
        elsif ($tabViewData{'infoLine'}->{$proformaId} eq 'isError')
        {
            $class .= ' isError';
            $startState = 'isError';
            # si on ne modifie pas la ligne, forcer l'enregistrement à réessayer avec les valeurs en erreur. 
            # Evite de pouvoir enregistrer en reprenant pour cette ligne les valeurs de départ.
            $toUpdate = 2; 
        }

        #URL client
        my $customerUrl = $tabViewData{'customerUrl'};

        my $customerId = $tabProformaInfos->{'customer.id'};
        $customerUrl =~ s/\<\%id\>/$customerId/;



        # CheckBox pour le justificatif
        my $justificationCheck ='';
        my $validationInputs ='';
        my $encashmentTypesList ='';
        my $encashmentParams ='';


        if ($tabViewData{'readOnly'})
        {
            # CheckBox pour le justificatif
            $justificationCheck = $view->displayBoolean($tabProformaInfos->{'isJustified'}, '');
            # CheckBox validé par l'agence
            $validationInputs = '<div>' . $view->displayBoolean($tabProformaInfos->{'isValidated'}, '');

            # Champ commentaire validé par l'agence
            if ($tabProformaInfos->{'validation.comment'} ne '')
            {
                my $label = $tabProformaInfos->{'validation.comment'};
                $validationInputs .= '&nbsp;' . $view->displayTooltipIcon('comment', '<b>' . $locale->t('Commentaire de l\'agence') . ':</b><br />' . $label);
            }
            $validationInputs .= '</div>';
            # Encaissement
            $encashmentTypesList = $tabViewData{'tabEncashmentTypes'}->{$tabProformaInfos->{'encashment.type'}};

            $encashmentParams = '<div class="param debt"><span class="title">' . $locale->t('Reste dû') . ':</span>' . $locale->getCurrencyFormat($tabProformaInfos->{'encashment.debtAmount'}, undef, $currencySymbol) . '</div>';
        }
        else
        {
            my $tabLineData = {
                'isJustified'  => $tabProformaInfos->{'isJustified'},
                'isValidated'  => $tabProformaInfos->{'isValidated'},
                'validComment' => (defined $tabProformaInfos->{'validation.comment'} ? $tabProformaInfos->{'validation.comment'} : ''),
                'encashmentId' => $tabProformaInfos->{'encashment.type'},
                'closureId'    => (defined $tabProformaInfos->{'closure.id'} ? $tabProformaInfos->{'closure.id'} : ''),
                'remainDue'    => $tabProformaInfos->{'encashment.debtAmount'},
                'total'        => $tabProformaInfos->{'total'},
                'state'        => $startState
            };
            my $lineData = &LOC::Json::toJson($tabLineData);


            # CheckBox pour le justificatif
            $justificationCheck = $view->displayFormInputCheckBox('pf[' . $proformaId .'][isJustified]', $tabProformaInfos->{'isJustified'}, 
                                                                    {'onclick' => 'setUpdatesRow(' . $proformaId .', ' . $lineData . ');'},
                                                                    '', 1, 1);

            # CheckBox validé par l'agence
            $validationInputs = '<div>';
            $validationInputs .= $view->displayFormInputCheckBox('pf[' . $proformaId .'][isValidated]', $tabProformaInfos->{'isValidated'}, 
                                                                  {'onclick' => 'setUpdatesRow(' . $proformaId .', ' . $lineData . '); if (this.checked) { this.nextSibling.focus(); };'},
                                                                  '', 1, 1);

            # Champ commentaire validé par l'agence
            $validationInputs .= $view->displayFormTextArea('pf[' . $proformaId .'][validComment]', $tabProformaInfos->{'validation.comment'},
                                                            {'onchange' => 'setUpdatesRow(' . $proformaId .', ' . $lineData . ');',
                                                             'onfocus' => 'refreshTimeObj.stop();',
                                                             'onblur' => 'if (tabUpdates.length == 0) { refreshTimeObj.start(); }'
                                                            });
            $validationInputs .= '</div>';

            # Encaissement
            $encashmentTypesList = $view->displayFormSelect('pf[' . $proformaId . '][encashment.id]', $tabViewData{'tabEncashmentTypes'},
                                                               $tabProformaInfos->{'encashment.type'}, 0, {
                                                                    'onchange' => 'setUpdatesRow(' . $proformaId .', ' . $lineData . ');',
                                                                    'tabindex' => $inputsTabIndex++,
                                                                    'onfocus' => 'refreshTimeObj.stop();',
                                                                    'onblur' => 'if (tabUpdates.length == 0) { refreshTimeObj.start(); }'
                                                               });
            my $classRemainDue = 'reductionAmount';
            if ($tabProformaInfos->{'encashment.debtAmount'} == 0)
            {
                $classRemainDue = 'reductionAmount error';
            }
            my $debtInput = $view->displayFormInputCurrency('pf[' . $proformaId . '][remainDue]',
                                                            $tabProformaInfos->{'encashment.debtAmount'},
                                                            LOC::Html::CURRENCYMODE_POSITIVEINPUT,
                                                            $currencySymbol, undef,
                                                            {'class' => $classRemainDue,
                                                             'onchange' => 'setUpdatesRow(' . $proformaId .', ' . $lineData . ');',
                                                             'onfocus' => 'refreshTimeObj.stop();',
                                                             'onblur' => 'if (tabUpdates.length == 0) { refreshTimeObj.start(); }'});

            # Génération de la listes des états de clôture
            my $enclosedSelect = $view->displayFormSelect('pf[' . $proformaId . '][closure.id]', $tabViewData{'tabClosureTypes'},
                                                          '', 0, {
                                                                  'onchange' => 'setUpdatesRow(' . $proformaId .', ' . $lineData . ');',
                                                                  'tabindex' => $inputsTabIndex++,
                                                                  'onfocus' => 'refreshTimeObj.stop();',
                                                                  'onblur' => 'if (tabUpdates.length == 0) { refreshTimeObj.start(); }'
                                                          });

            $encashmentParams = '<div class="param debt"><span class="title">' . $locale->t('Reste dû') . ':</span>' . $debtInput . '</div>' .
                                '<div class="param enclosed"><span class="title">' . $locale->t('Clôturé') . ':</span>' . $enclosedSelect . '</div>';
        }



        # Génération des icônes pour les lignes
        my $classNameReduction = 'ok';
        my $countErrors = @{$tabProformaInfos->{'specialReductionList'}};
        if ($countErrors > 0)
        {
            $classNameReduction = 'ko ' . join(' ', @{$tabProformaInfos->{'specialReductionList'}});
        }
        my $errorIcons = '<div id="specialReductionStatus" class="' . $classNameReduction . '">';
        foreach my $tabErrorTypeInfos (@$tabErrorTypes)
        {
            $errorIcons .= '<div title="' . $view->toHTMLEntities($tabErrorTypeInfos->{'title'}) . '" class="errorImg ' . $tabErrorTypeInfos->{'code'} . '"></div>';
        }
        $errorIcons .= '</div>';

        # Boutons d'impression
        my $printBtns = '';
        if ($tabProformaInfos->{'isPrintable'})
        {
            $printBtns = $view->displayPrintButtons(
                                           'printBtns_' . $proformaId,
                                           $tabProformaInfos->{'pdfUrl'},
                                           $tabProformaInfos->{'pdfName'},
                                           $tabProformaInfos->{'orderContact.email'},
                                           $tabProformaInfos->{'type.label'},
                                           $tabProformaInfos->{'mailContent'},
                                           $tabProformaInfos->{'agency.id'},
                                           {'buttons' => ['mail', 'pdf', 'print'],
                                            'tabEndActions' => [
                                                {
                                                    'type'      => 'logPrint',
                                                    'entity'    => 'PROFORMA',
                                                    'entityIds' => [$proformaId]
                                                }
                                            ]
                                           }
                                          );
        }

        print '
    <tr id="proforma_' . $proformaId . '" class="' . $class . '">
        <td class="id">' . $proformaId .
                           $view->displayFormInputHidden('pf[' . $proformaId . '][modificationDate]', $tabProformaInfos->{'modificationDate'}) .
                           $view->displayFormInputHidden('pf[' . $proformaId . '][toUpdate]', $toUpdate) . '</td>
        <td class="type" data-val="' . $view->toHTMLEntities($tabProformaInfos->{'type.label'}) . '"><div title="' . $view->toHTMLEntities($tabProformaInfos->{'type.label'}) . '"></div></td>
        <td class="date" data-val="' . $tabProformaInfos->{'date'} . '">' . $locale->getDateFormat($tabProformaInfos->{'date'}, &LOC::Locale::FORMAT_DATE_NUMERIC) . '</td>
        <td class="paymentLabel"><div>' . $view->toHTMLEntities($tabProformaInfos->{'paymentMode.label'}) . '</div></td>
        <td class="customerCode"><a href="' . $customerUrl . '" target="_blank" title="' . $view->toHTMLEntities($tabProformaInfos->{'customer.name'}) . '">' . ($tabProformaInfos->{'customer.code'} eq '' ? '[' . $locale->t('Aucun') . ']' : $tabProformaInfos->{'customer.code'}) . '</a><div title="' . $locale->t('Client avec ouverture de compte en attente') . '" class ="' . ($tabProformaInfos->{'customer.notVerified'} ? 'errorImg customerNotVerified' : '') . '"></div></td>
        <td class="isPFRequired" data-val="' . ($tabProformaInfos->{'customer.isPFRequired'} * 1) . '">' . $view->displayBoolean($tabProformaInfos->{'customer.isPFRequired'}, '') . '</td>
        <td class="amountWithVat" data-val="' . ($tabProformaInfos->{'amountWithVat'} * 100) . '">' . $view->displayFormInputCurrency('pf[' . $proformaId .'][amountWithVat]', $tabProformaInfos->{'amountWithVat'}, &LOC::Html::CURRENCYMODE_TEXT, $currencySymbol) . '</td>
        <td class="total" data-val="' . ($tabProformaInfos->{'total'} * 100) . '">' . $view->displayFormInputCurrency('pf[' . $proformaId .'][total]', $tabProformaInfos->{'total'}, &LOC::Html::CURRENCYMODE_TEXT, $currencySymbol) . '</td>
        <td class="agency">' . $tabProformaInfos->{'agency.id'} . '</td>
        <td class="documentType">' . $view->toHTMLEntities($documentType) . $errorIcons . '</td>
        <td class="documentCode"><a href="' . $documentUrl . '" target="_blank" title="' . $tabProformaInfos->{'document.state'} . '">' . $documentCode . '</a></td>
        <td class="documentBeginDate" data-val="' . $tabProformaInfos->{'document.beginDate'} . '">' . $locale->getDateFormat($tabProformaInfos->{'document.beginDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC) . '</td>
        <td class="justification" data-val="' . ($tabProformaInfos->{'isJustified'} * 1) . '">' . $justificationCheck . '</td>
        <td class="validation" data-val="' . ($tabProformaInfos->{'isValidated'} * 1) . '">' . $validationInputs . '</td>
        <td class="encashment">' . $encashmentTypesList . $encashmentParams . '</td>
        <td class="editCtrls">' . $printBtns . '</td>
    </tr>';
    }

    print '
</tbody>
</table>';

    # Tri du tableau
    print $view->displayJSBlock('
$.tablesorter.addParser({
    id: "textAttr",
    is: function(s) { return false; },
    format: function(s, table, cell, cellIndex) { return $(cell).attr("data-val"); },
    type: "text"
});
$.tablesorter.addParser({
    id: "integerAttr",
    is: function(s) { return false; },
    format: function(s, table, cell, cellIndex) { return $(cell).attr("data-val"); },
    type: "numeric"
});

$(function() 
{ 
    $("table.basic.list").tablesorter({
        headers: {' . $headersOption . '},
        sortList: ' . $sortListOption . ',
        widgets: ["zebra"]
    });
}
);');

    if (!$tabViewData{'readOnly'} && $compteurLine)
    {
        # Bouton Valider
        my $validBtn = $view->displayTextButton($locale->t('Valider'), 'valid(|Over).png', 'submitForm()', $locale->t('Valider'));

        #Affichage du Panel du bas
        print $view->displayControlPanel({'left' => [$validBtn], 'substyle' => 'bottom'});
    }

    print $view->displayFormInputHidden('valid', '');

    # Affichage du pied de page
    print $view->endForm();
}

print $view->displayFooter();
