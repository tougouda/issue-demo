use utf8;
use open (':encoding(UTF-8)');

use strict;


use Spreadsheet::WriteExcel;

use LOC::Locale;
use LOC::Browser;


# Répertoire CSS/JS/Images
my $dir = 'accounting/proforma/';


our %tabViewData;


my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});


# Fonctions utiles
sub displayBoolean
{
    my ($value) = @_;
    return ($value ? 'X' : '');
}

# Titre de l'extraction
my $title = $locale->t('Règlements pro forma') .
            ' (' . ($tabViewData{'readOnly'} ? $locale->t('Commerce') : $locale->t('Comptabilité')) . ')';


# Téléchargement du fichier
my $fileName = $title . '_' . POSIX::strftime('%Y%m%d_%H%M', localtime()) . '.xls';
$fileName =~ tr/"<>&\/\\?#\*;/'{}+____\-\-/;

&LOC::Browser::sendHeaders(
        "Content-Type: application/force-download\n" .
        "Content-Disposition: attachment; filename=\"" . $fileName . "\"\n" .
        "\n"
    );


# Nouveau classeur Excel envoyé sur la sortie standard
my $workbook = Spreadsheet::WriteExcel->new('-');
my $author = $locale->t('Gestion des locations') . ' ' . &LOC::Globals::get('version');
my $company = 'Accès industrie';
$workbook->set_properties('title' => $title, 'author' => $author, 'company' => $company);

# Format de la ligne d'en-tête
my $headerFmt = $workbook->add_format();
$headerFmt->set_italic();
$headerFmt->set_bold();
$headerFmt->set_bg_color($workbook->set_custom_color(12, 232, 243, 249));
$headerFmt->set_bottom();

# Format de date
my $dateFmt = $workbook->add_format();
$dateFmt->set_num_format(14);

# Format des booléens
my $boolFmt = $workbook->add_format();
$boolFmt->set_align('center');

# Format monétaire
my $currencyFmt = $workbook->add_format();
$currencyFmt->set_align('right');
$currencyFmt->set_num_format('#,##0.00 €');


# Format des récapitulatifs
my $recapFmt = $workbook->add_format();
$recapFmt->set_bold();
$recapFmt->set_bg_color($workbook->set_custom_color(13, 200, 200, 249));
$recapFmt->set_top();


# Tri du tableau
my @tabProformas = @{$tabViewData{'tabProformas'}};

@tabProformas = sort {$a->{'date'} cmp $b->{'date'}} @tabProformas;


# Création des feuilles et de leurs contenus
my $worksheet = $workbook->add_worksheet($locale->t('Règlements pro forma'));
$worksheet->activate();

# Entête et pied de page pour l'impression
$worksheet->set_header('&C&12&",Bold"' . $title);
$worksheet->set_footer('&C&8' . $locale->t('Page') . ' &P / &N');


# Figer les volets
$worksheet->freeze_panes(1, 0);


# Ligne d'en-tête
my $c = 0;
my $l = 0;
$worksheet->write_string($l, $c, $locale->t('Numéro'), $headerFmt);
$worksheet->set_column($c, $c++, 8);
$worksheet->write_string($l, $c, $locale->t('Type'), $headerFmt);
$worksheet->set_column($c, $c++, 22);
$worksheet->write_string($l, $c, $locale->t('Date'), $headerFmt);
$worksheet->set_column($c, $c++, 10);
$worksheet->write_string($l, $c, $locale->t('Mode de règlement'), $headerFmt);
$worksheet->set_column($c, $c++, 22);
$worksheet->write_string($l, $c, $locale->t('Code client'), $headerFmt);
$worksheet->set_column($c, $c++, 14);
$worksheet->write_string($l, $c, $locale->t('Raison sociale'), $headerFmt);
$worksheet->set_column($c, $c++, 35);
$worksheet->write_string($l, $c, $locale->t('p.a.l'), $headerFmt);
$worksheet->set_column($c, $c++, 6);
$worksheet->write_string($l, $c, $locale->t('Montant TTC'), $headerFmt);
$worksheet->set_column($c, $c++, 14);
$worksheet->write_string($l, $c, $locale->t('Solde à payer'), $headerFmt);
$worksheet->set_column($c, $c++, 14);
$worksheet->write_string($l, $c, $locale->t('Agence'), $headerFmt);
$worksheet->set_column($c, $c++, 8);
$worksheet->write_string($l, $c, $locale->t('Type doc.'), $headerFmt);
$worksheet->set_column($c, $c++, 10);
$worksheet->write_string($l, $c, $locale->t('Code doc.'), $headerFmt);
$worksheet->set_column($c, $c++, 20);
$worksheet->write_string($l, $c, $locale->t('Début de loc.'), $headerFmt);
$worksheet->set_column($c, $c++, 18);
$worksheet->write_string($l, $c, $locale->t('Justif.'), $headerFmt);
$worksheet->set_column($c, $c++, 6);
$worksheet->write_string($l, $c, $locale->t('Val. par ag.'), $headerFmt);
$worksheet->set_column($c, $c++, 10);
$worksheet->write_string($l, $c, $locale->t('Commentaire de validation agence'), $headerFmt);
$worksheet->set_column($c, $c++, 35);
$worksheet->write_string($l, $c, $locale->t('Encaissement'), $headerFmt);
$worksheet->set_column($c, $c++, 15);
$worksheet->write_string($l, $c, $locale->t('Reste dû'), $headerFmt);
$worksheet->set_column($c, $c++, 14);

$l++;


# Pro formas
foreach my $tabProformaInfos (@tabProformas)
{
    my $documentCode = '';
    my $documentType = '';
    if ($tabProformaInfos->{'document.type'} eq 'contract')
    {
        $documentCode = $tabProformaInfos->{'document.code'};
        $documentType = $locale->t('Contrat');
    }
    else
    {
        $documentCode = $tabProformaInfos->{'document.code'}->[0] .
                        ' (' .$locale->t('ligne %s', $tabProformaInfos->{'document.code'}->[1]) . ')';
        $documentType = $locale->t('Devis');
    }

    # Libellé du type d'encaissement
    my $encashmentTypeLabel = $tabViewData{'tabEncashmentTypes'}->{$tabProformaInfos->{'encashment.type'}};


    $c = 0;
    $worksheet->write_number($l, $c++, $tabProformaInfos->{'id'});
    $worksheet->write_string($l, $c++, $tabProformaInfos->{'type.label'});
    $worksheet->write_date_time($l, $c++, substr($tabProformaInfos->{'date'}, 0, 10) . 'T', $dateFmt);
    $worksheet->write_string($l, $c++, $tabProformaInfos->{'paymentMode.label'});
    $worksheet->write_string($l, $c++, $tabProformaInfos->{'customer.code'});
    $worksheet->write_string($l, $c++, $tabProformaInfos->{'customer.name'});
    $worksheet->write_string($l, $c++, &displayBoolean($tabProformaInfos->{'customer.isPFRequired'}), $boolFmt);
    $worksheet->write_number($l, $c++, $tabProformaInfos->{'amountWithVat'}, $currencyFmt);
    $worksheet->write_number($l, $c++, $tabProformaInfos->{'total'}, $currencyFmt);
    $worksheet->write_string($l, $c++, $tabProformaInfos->{'agency.id'});
    $worksheet->write_string($l, $c++, $documentType);
    $worksheet->write_string($l, $c++, $documentCode);
    $worksheet->write_date_time($l, $c++, substr($tabProformaInfos->{'document.beginDate'}, 0, 10) . 'T', $dateFmt);
    $worksheet->write_string($l, $c++, &displayBoolean($tabProformaInfos->{'isJustified'}), $boolFmt);
    $worksheet->write_string($l, $c++, &displayBoolean($tabProformaInfos->{'isValidated'}), $boolFmt);
    $worksheet->write_string($l, $c++, $tabProformaInfos->{'validation.comment'});
    $worksheet->write_string($l, $c++, $encashmentTypeLabel);
    if ($tabProformaInfos->{'encashment.type'} eq LOC::Proforma::ENCASHMENTSTATE_PARTIAL)
    {
        $worksheet->write_number($l, $c++, $tabProformaInfos->{'encashment.debtAmount'}, $currencyFmt);
    }

    $l++;
}

# Fermeture du classeur
$workbook->close();
