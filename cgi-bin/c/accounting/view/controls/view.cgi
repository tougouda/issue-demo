use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;

# Répertoire CSS/JS/Images
my $dir = 'accounting/controls/';


our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});
our $currencySymbol = $tabViewData{'currencySymbol'};

my $tabInvoiceInfos = $tabViewData{'tabInvoiceInfos'};


# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc('default/home/alert.css');
$view->addCSSSrc($dir . 'view.css');

# JavaScripts
$view->addJSSrc($dir . 'view.js');

$view->setPageTitle($locale->t('Contrôles pré-facturation'));
$view->setTitle($locale->t('Contrôles pré-facturation'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT);

# Chargement en AJAX
my $controlsUrl = $view->createURL('accounting:controls:json', {'countryId' => $tabViewData{'country.id'},
                                                                'date'      => $tabViewData{'date'},
                                                                'type'      => $tabViewData{'type'}});
$view->addBodyEvent('load', 'init("' . $controlsUrl . '")');


# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();



print '<fieldset>';
print '<span class="date">';
print $locale->getDateFormat($tabViewData{'date'}, LOC::Locale::FORMAT_DATEYEAR_ABBREV);
print '</span>';
print '<span class="type">';
if ($tabViewData{'type'} eq LOC::Invoice::Rent::INVOICETYPE_MONTH)
{
    print $view->displayImage($dir . 'monthly.png') . ' ' . $locale->t('Mensuelle');
}
elsif ($tabViewData{'type'} eq LOC::Invoice::Rent::INVOICETYPE_INTERMEDIATE)
{
    print $view->displayImage($dir . 'intermediate.png') . ' ' . $locale->t('Intermédiaire');
}
print '</span>';
print '</fieldset>';


# Message d'information
print $view->displayMessages('info', [$locale->t('Les compteurs se mettent automatiquement à jour toutes les 5 minutes')], 0);


my $nbControls = @{$tabViewData{'tabControlsKey'}};

if ($tabViewData{'type'} ne '' && $nbControls > 0)
{
    # Libellés
    my %tabLabel = ('contractsToStop'    => $locale->t('Contrats à arrêter'),
                    'fuelsToType'        => $locale->t('Carburants à saisir'),
                    'contractsToInvoice' => $locale->t('Contrats à mettre en facturation'));

    # Libellés
    my $localeFormattedDate = $locale->getDateFormat($tabViewData{'rentalPeriodEndDate'},
                                                     LOC::Locale::FORMAT_DATEYEAR_TEXT);
    my %tabHelp = ('contractsToStop'    => $locale->t('Nombre de contrats en cours dont la date de fin est actuellement antérieure ou égale au %s',
                                                      $localeFormattedDate),
                   'fuelsToType'        => $locale->t('Nombre de carburants à saisir sur des contrats en arrêt se terminant au plus tard le %s',
                                                      $localeFormattedDate),
                   'contractsToInvoice' => $locale->t('Nombre de contrats en arrêt dont la date de fin est actuellement antérieure ou égale au %s',
                                                      $localeFormattedDate));

    print '<table class="structure">' . "\n";
    print '<tr>' . "\n";

    my $maxCellsNo = 2;
    my $cellWidth = int(100 / $maxCellsNo);

    my $i = 0;
    foreach my $control (@{$tabViewData{'tabControlsKey'}})
    {
        if ($i > 0 && $i % $maxCellsNo == 0)
        {
            print '</tr>' . "\n";
            print '<tr>' . "\n";
        }

        my $tabControl = $tabViewData{'tabControl'}->{$control};
        my $label = (grep(/^$control$/, keys(%tabLabel)) ? $tabLabel{$control} : $control);

        print '<td style="width: ' . $cellWidth . '%;">' . "\n";


        print '<div class="alertBlock">' . "\n";
        print '<div name="loading" class="loading hidden"></div>' . "\n";
        print '<div class="top"><div class="title">' . $label . '</div></div>' . "\n";
        print '<div class="content">' . "\n";
        print '<div class="alerts">' . "\n";

        foreach my $agencyId (@{$tabViewData{'tabAgency'}})
        {
            print '<a name="alert" id="' . $control . '_alert_' . $agencyId . '" class="alert">';
            print '<table><tr><td class="agency">' . $agencyId . '</td><td name="number" id="' . $control . '_number_' . $agencyId . '" class="number"></td></tr></table>' . "\n";
            print '</a>' . "\n";
        }

        print '</div>' . "\n";
        print '</div>' . "\n";
        print '</div>' . "\n";

        # Message d'aide
        print $view->displayMessages('help', [$tabHelp{$control}], 0);

        print '</td>' . "\n";

        $i++;
    }

    print '</tr>' . "\n";

    print '</table>' . "\n";
}


# Affichage du pied de page
print $view->endForm();
print $view->displayFooter();
