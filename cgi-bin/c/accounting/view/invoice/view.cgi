use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;

# Répertoire CSS/JS/Images
my $dir = 'accounting/invoice/';


our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});
our $currencySymbol = $tabViewData{'currencySymbol'};

my $tabInvoiceInfos = $tabViewData{'tabInvoiceInfos'};


# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'view.css');

# JavaScript
if (!$tabViewData{'allowCopy'})
{
    $view->addBodyEvent('oncopy', 'return false');
}

$view->setPageTitle($locale->t('Facture'));
$view->setTitle($locale->t('Facture'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_NONE);


# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();


print '
<table class="structure">
<tr>
<td style="padding-right: 5px">';

# Infos de la facture
my $date = '';
if ($tabInvoiceInfos->{'date'} ne '')
{
    $date = $locale->getDateFormat($tabInvoiceInfos->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC);
}
print '
<fieldset id="invoiceInfos">
<legend>' . $locale->t('Informations') . '</legend>
<div class="title">' . $locale->t('Facture') . '</div>
' . $locale->t('N° <b>%s</b>', $tabInvoiceInfos->{'number'}) . '<br />
' . $locale->t('Date') . '&nbsp;: ' . $date . '<br />
</fieldset>';

print '
</td>
<td rowspan="2" style="padding-left: 5px">';

# Infos du client
print '
<fieldset id="address">
<legend>' . $locale->t('Adressée à') . '</legend>
<b>' . $tabInvoiceInfos->{'customer.name'} . '</b><br />';

if ($tabInvoiceInfos->{'customer.address1'} ne '')
{
    print '
' . $tabInvoiceInfos->{'customer.address1'} . '<br />';
}
if ($tabInvoiceInfos->{'customer.address2'} ne '')
{
    print '
' . $tabInvoiceInfos->{'customer.address2'} . '<br />';
}

my $customerLink = $tabInvoiceInfos->{'customer.code'};
if ($tabInvoiceInfos->{'customer.id'} != 0)
{
    my $customerUrl = &LOC::Globals::get('locationUrl') . '/cgi-bin/old/location/infoclient.cgi?value=' .
                      $tabInvoiceInfos->{'customer.id'};
    $customerLink = '<a href="' . $customerUrl . '" target="_blank">' . $customerLink . '</a>';
}
print '
' . $tabInvoiceInfos->{'customer.address3'} . '<br />
<br />
' . $locale->t('Code client') . '&nbsp;: ' . $customerLink . '<br />
</fieldset>';

print '
</td>
</tr>
<tr>
<td style="padding-right: 5px">';

# Adresse de livraison
print '
<fieldset>
<legend>' . $locale->t('Adresse de livraison') . '</legend>';

if ($tabInvoiceInfos->{'header.line3'} ne '')
{
    print '
' . $tabInvoiceInfos->{'header.line3'} . '<br />';
}
if ($tabInvoiceInfos->{'header.line4'} ne '')
{
    print '
' . $tabInvoiceInfos->{'header.line4'} . '<br />';
}

print '
</fieldset>';

print '
</td>
</tr>
</table>';


# Bloc références
my $contractUrl = $view->createURL('rent:rentContract:view', {'contractId' => $tabInvoiceInfos->{'contract.id'}});
print '
<table class="standard" id="references">
    <tr>
        <th>' . $locale->t('Réf. contrat') . '</th>
        <th>' . $locale->t('Réf. commande') . '</th>
        <th>' . $locale->t('Date de livraison') . '</th>
    </tr>
    <tr>
        <td><a href="' . $contractUrl . '" target="_blank">' . $tabInvoiceInfos->{'contract.code'} . '</a></td>
        <td>' . $tabInvoiceInfos->{'header.line1'} . '</td>
        <td>' . $locale->getDateFormat($tabInvoiceInfos->{'contract.beginDate'}, LOC::Locale::FORMAT_DATE_NUMERIC) . '</td>
    </tr>
</table>';


# Lignes de la facture
print '
<table class="standard" id="lines">
    <tr>
        <th>' . $locale->t('Désignation') . '</th>
        <th style="text-align: right;">' . $locale->t('Qté') . '</th>
        <th style="text-align: right;">' . $locale->t('Prix HT') . '</th>
        <th style="text-align: right;">' . $locale->t('% rem.') . '</th>
        <th style="text-align: right;">' . $locale->t('PU net HT') . '</th>
        <th style="text-align: right;">' . $locale->t('Montant HT') . '</th>
        <th>' . $locale->t('Code TVA') . '</th>
    </tr>';

my $i = 0;
foreach my $tabLine (@{$tabInvoiceInfos->{'lines'}})
{
    my $designation = join("\\n", $tabLine->{'designation'}, $tabLine->{'comments'});
    $designation =~ s/\\n/\n/g;
    $designation =~ s/^( *)(.+?)( *)$/'&nbsp;' x length($1) . $2 . '&nbsp;' x length($3)/mge;
    $designation =~ s/\n/<br>/g;

    my $quantity = '';
    if ($tabLine->{'quantity'} ne '')
    {
        $quantity = $locale->getNumberFormat($tabLine->{'quantity'});
    }
    my $priceInput = '';
    if (defined $tabLine->{'price'})
    {
        $priceInput = $view->displayFormInputCurrency('price[' . $i . ']',
                                                      $tabLine->{'price'}, LOC::Html::CURRENCYMODE_TEXT,
                                                      $currencySymbol);
    }
    my $reduction = '';
    if ($tabLine->{'reduction'} != 0)
    {
        $reduction = $locale->getNumberFormat($tabLine->{'reduction'} * 100, 2);
    }
    my $netPriceInput = '';
    if (defined $tabLine->{'netPrice'})
    {
        $netPriceInput = $view->displayFormInputCurrency('netPrice[' . $i . ']',
                                                         $tabLine->{'netPrice'}, LOC::Html::CURRENCYMODE_TEXT,
                                                         $currencySymbol);
    }
    my $amountInput = '';
    if (defined $tabLine->{'amount'})
    {
        $amountInput = $view->displayFormInputCurrency('amount[' . $i . ']',
                                                       $tabLine->{'amount'},
                                                       LOC::Html::CURRENCYMODE_TEXT,
                                                       $currencySymbol);
    }

    print '
    <tr style="vertical-align: baseline">
        <td>' . $designation . '</td>
        <td style="text-align: right;">' . $quantity . '</td>
        <td style="text-align: right;">' . $priceInput . '</td>
        <td style="text-align: right;">' . $reduction . '</td>
        <td style="text-align: right;">' . $netPriceInput . '</td>
        <td style="text-align: right;">' . $amountInput . '</td>
        <td>' . $tabLine->{'vatCode'} . '</td>
    </tr>';

    $i++;
}

print '
</table>';


# Bloc TVA
print '
<table class="standard" id="vat">
    <tr>
        <th>' . $locale->t('Code TVA') . '</th>
        <th style="text-align: right;">' . $locale->t('Taux') . '</th>
        <th style="text-align: right;">' . $locale->t('Base') . '</th>
        <th style="text-align: right;">' . $locale->t('Montant') . '</th>
    </tr>';
foreach my $vatCode (sort keys(%{$tabInvoiceInfos->{'vat'}}))
{
    my $tabVat = $tabInvoiceInfos->{'vat'}->{$vatCode};

    my $vatBaseInput = $view->displayFormInputCurrency('vatBase[' . $vatCode . ']',
                                                      $tabVat->{'base'},
                                                      LOC::Html::CURRENCYMODE_TEXT,
                                                      $currencySymbol);;
    my $vatAmountInput = $view->displayFormInputCurrency('vatAmount[' . $vatCode . ']',
                                                         $tabVat->{'amount'},
                                                         LOC::Html::CURRENCYMODE_TEXT,
                                                         $currencySymbol);;

    print '
    <tr>
        <td>' . $vatCode . '</td>
        <td style="text-align: right;">' . $locale->t('%s %%', $tabVat->{'rate'} * 100) . '</td>
        <td style="text-align: right;">' . $vatBaseInput . '</td>
        <td style="text-align: right;">' . $vatAmountInput . '</td>
    </tr>';
}

print '
</table>';


# Bloc totaux
my $totalInput = $view->displayFormInputCurrency('total', $tabInvoiceInfos->{'amount'}, LOC::Html::CURRENCYMODE_TEXT,
                                                 $currencySymbol);
my $vatTotalInput = $view->displayFormInputCurrency('vatTotal', $tabInvoiceInfos->{'vatAmount'},
                                                    LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);
my $taxesIncludedTotalInput = $view->displayFormInputCurrency('taxesIncludedTotal',
                                                              $tabInvoiceInfos->{'taxesIncludedAmount'},
                                                              LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);

print '
<table class="standard" id="totals">
    <tr>
        <th>' . $locale->t('Montant total HT') . '</th>
        <td style="text-align: right;">' . $totalInput . '</td>
    </tr>
    <tr>
        <th>' . $locale->t('Montant TVA') . '</th>
        <td style="text-align: right;">' . $vatTotalInput . '</td>
    </tr>
    <tr>
        <th>' . $locale->t('Montant TTC') . '</th>
        <td class="total" style="text-align: right;">' . $taxesIncludedTotalInput . '</td>
    </tr>
</table>';


# Affichage du pied de page
print $view->endForm();
print $view->displayFooter();
