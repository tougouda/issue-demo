use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;

# Répertoire CSS/JS/Images
our $dir = 'accounting/invoice/';


our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});
our $currencySymbol = $tabViewData{'currencySymbol'};

# Ajout des traductions JavaScript
$view->addTranslations({
    'fileSend'             => $locale->t('Envoi du fichier'),
    'fileRead'             => $locale->t('Lecture du fichier'),
    'fileReadError'        => $locale->t('Erreur lors de la lecture du fichier'),
    'customersUpdate'      => $locale->t('Mise à jour des clients'),
    'customersUpdateError' => $locale->t('Erreur lors de la mise à jour des clients'),
});


# Modules additionnels
$view->addPackage('popups')
     ->addPackage('modalwindow')
     ->addPackage('tabBoxes');

# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'manage.css');

# JavaScripts
$view->addJSSrc($dir . 'manage.js');

# Titre
$view->setPageTitle($locale->t('Facturation'));
$view->setTitle($locale->t('Facturation'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);


# Bouton "actualiser"
my $url = $view->createURL('accounting:invoice:manage', {'countryId' => $tabViewData{'country.id'}});
my $reload = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png', $url,
                                      $locale->t('Actualiser la page'));

$view->addControlsContent([$reload], 'right', 1);


# Chargement de l'historique en AJAX
my $historyUrl = $view->createURL('accounting:invoice:history', {'countryId'      => $tabViewData{'country.id'},
                                                                 'currencySymbol' => $currencySymbol});
$view->addBodyEvent('load', 'init(); getHistory("' . $view->toJSEntities($historyUrl) . '");');


# Vérification de la progression si une requête de facturation de location est en cours d'exécution
our $progressionUrl = $view->createURL('accounting:invoice:checkProgression',
                                       {'countryId'   => $tabViewData{'country.id'}});
if ($tabViewData{'rentRequestId'} > 0)
{
    $view->addBodyEvent('load', 'checkProgression("' . LOC::Invoice::TYPE_RENT . '", ' .
                                                 '"' . $tabViewData{'rentRequestId'} . '", ' .
                                                 '"' . $view->toJSEntities($progressionUrl) . '");');
}


# Liste des pays
my $countries = $view->startForm($view->createURL('accounting:invoice:manage'));
if ($tabViewData{'isAdmin'})
{
    $countries .= '<span class="label">';
    $countries .= $locale->t('Pays');
    $countries .= '</span>';
    $countries .= $view->displayFormSelect('countryId',
                                           $tabViewData{'tabCountries'},
                                           $tabViewData{'country.id'});
    $countries .= ' ';
    $countries .= $view->displayCountryImage($tabViewData{'country.id'});
    $countries .= ' ';
    $countries .= $view->displayFormInputSubmit('valid', $locale->t('OK'));
}
$countries .= $view->endForm();

$view->addControlsContent([$countries], 'left', 1);


# Message de confirmation de facturation
my $validBlock = '
<div id="invoice.validBlock" class="messageBlock valid" style="display: none">
    <div class="popup" style="display: block;">
        <div class="content">
            <table>
            <tr>
                <td style="font-weight: bold;">
                    ' . $locale->t('La facturation s\'est déroulée avec succès.') . '<br />
                    <span id="invoice.validBlock.message"></span>
                    <span id="invoice.validBlock.none" style="display: none">' . $locale->t('Aucune facture n\'est concernée') . '</span>
                    <span id="invoice.validBlock.singular" style="display: none">' . $locale->tn('%s facture est concernée', '%s factures sont concernées', 1, '<%nb>') . '</span>
                    <span id="invoice.validBlock.plural" style="display: none">' . $locale->tn('%s facture est concernée', '%s factures sont concernées', 2, '<%nb>') . '</span>
                </td>
            </tr>
            <tr class="buttons">
                <td>' .
                $view->displayTextButton($locale->t('Ok'), '', $url) .
                '</td>
            </tr>
            </table>
        </div>
    </div>
</div>';

$view->addBodyContent($validBlock, 0, 1);


# Message d'erreur de facturation
my $errorBlock = '
<div id="invoice.errorBlock" class="messageBlock error" style="display: none">
    <div class="popup" style="display: block;">
        <div class="content">
            <table>
            <tr>
                <td style="font-weight: bold;">
                    <span id="invoice.errorBlock.standardError" style="display: none">
                        ' . $locale->t('Une erreur s\'est produite pendant la facturation.') . '<br />
                        ' . $locale->t('Veuillez contacter le 5000') . '
                    </span>
                    <span id="invoice.errorBlock.contractsControlError" style="display: none">
                        ' . $locale->t('Le contrôle des contrats n\'est pas correct.') . '<br />
                        ' . $locale->t('Veuillez le relancer avant de lancer de nouveau la facturation') . '
                    </span>
                </td>
            </tr>
            <tr class="buttons">
                <td>' .
                $view->displayTextButton($locale->t('Fermer'), '',
                                         '$("invoice.errorBlock").style.display = "none";') .
                '</td>
            </tr>
            </table>
        </div>
    </div>
</div>';

$view->addBodyContent($errorBlock, 0, 1);


# Message de confirmation d'annulation de facturation
my $validBlock = '
<div id="cancel.validBlock" class="messageBlock valid" style="display: none">
    <div class="popup" style="display: block;">
        <div class="content">
            <table>
            <tr>
                <td style="font-weight: bold;">
                    ' . $locale->t('La facturation a été annulée.') . '<br />
                    <span id="cancel.validBlock.message"></span>
                    <span id="cancel.validBlock.none" style="display: none">' . $locale->t('Aucune facture n\'a été supprimée') . '</span>
                    <span id="cancel.validBlock.singular" style="display: none">' . $locale->tn('%s facture a été supprimée', '%s factures ont été supprimées', 1, '<%nb>') . '</span>
                    <span id="cancel.validBlock.plural" style="display: none">' . $locale->tn('%s facture a été supprimée', '%s factures ont été supprimées', 2, '<%nb>') . '</span>
                </td>
            </tr>
            <tr class="buttons">
                <td>' .
                $view->displayTextButton($locale->t('Ok'), '', $url) .
                '</td>
            </tr>
            </table>
        </div>
    </div>
</div>';

$view->addBodyContent($validBlock, 0, 1);


# Message d'erreur d'annulation de facturation
my $errorBlock = '
<div id="cancel.errorBlock" class="messageBlock error" style="display: none">
    <div class="popup" style="display: block;">
        <div class="content">
            <table>
            <tr>
                <td style="font-weight: bold;">
                    ' . $locale->t('Vous ne pouvez pas annuler cette facturation') . '
                </td>
            </tr>
            <tr class="buttons">
                <td>' .
                $view->displayTextButton($locale->t('Fermer'), '',
                                         '$("cancel.errorBlock").style.display = "none";') .
                '</td>
            </tr>
            </table>
        </div>
    </div>
</div>';

$view->addBodyContent($errorBlock, 0, 1);

# Message d'erreur envoi fichier sous-location
my $errorBlock = '
<div id="subRent.errorBlock" class="messageBlock error" style="display: none">
    <div class="popup" style="display: block;">
        <div class="content">
            <table>
            <tr>
                <td style="font-weight: bold;">
                    ' . $locale->t('Une erreur s\'est produite lors de la lecture du fichier de sous-location') . '
                </td>
            </tr>
            <tr class="buttons">
                <td>' .
                $view->displayTextButton($locale->t('Fermer'), '',
                                         '$("subRent.errorBlock").style.display = "none";') .
                '</td>
            </tr>
            </table>
        </div>
    </div>
</div>';

$view->addBodyContent($errorBlock, 0, 1);


# Affichage de l'entête
print $view->displayHeader();
print $view->startForm($view->createURL('accounting:invoice:manage'));


# Info-bulles partagées
our $customersCtrlHelp = $locale->t('Le contrôle des clients n\'empêche pas le lancement de la facturation, mais est vivement conseillé afin d\'éviter les erreurs d\'import du fichier dans Sage');
our $invoiceHelp = $locale->t('Saisissez le prochain numéro de pièce');


print '
<table class="structure subrent_' . $tabViewData{'isSubRent'} . '">
<tr>
<td style="padding-right: 5px;" rowspan="2" id="bloc_rent">';

# Prochaine facturation de location
require($directory . '/invoice/manage/rent.cgi');

print '
</td>
<td style="padding-left: 5px;" id="bloc_repair">';

# Prochaine facturation de remise en état
require($directory . '/invoice/manage/repair.cgi');

print '
</td>
</tr>';

# Fichier Sage sous-location Espagne
if ($tabViewData{'isSubRent'})
{
    print '
<tr>
<td style="padding-left: 5px;" id="bloc_subrent">';
    require($directory . '/invoice/manage/subrent.cgi');
    print '
</td>
</tr>'
}
print '
</table>';

# Champs cachés
print $view->displayFormInputHidden('countryId', $tabViewData{'country.id'});

# Fin du formulaire
print $view->endForm();


# Historique des facturations
print '
<fieldset>
<legend>' . $locale->t('Historique des facturations') . '</legend>
<div id="history.content" class="hidden"></div>
<div id="history.loading"></div>
</fieldset>';


# Rapport du contrôle des contrats
my $excelUrl = $view->createURL('accounting:invoice:checkContracts',
                                {'id'          => $tabViewData{'tabNextRentInvoice'}->{'id'},
                                 'date'        => $tabViewData{'tabNextRentInvoice'}->{'date'},
                                 'rentType'    => $tabViewData{'tabNextRentInvoice'}->{'rentType'},
                                 'countryId'   => $tabViewData{'country.id'},
                                 'displayType' => 'xls'});
my $excelBtn = $view->displayTextButton('', 'accounting/invoice/excelExport(|Over).png', $excelUrl,
                                        $locale->t('Exporter au format Excel'));
print '
<div id="contractsCtrl.report" class="contractsCtrlReport" style="padding: 10px;">
<div style="position: absolute; top: 2px; right: 30px; z-index: 10;">
' . $excelBtn . '
</div>
<div id="contractsCtrl.report.tabs">

<div id="contractsCtrl.fuelsToType" title="' . $locale->t('Carburants à saisir') . '"></div>
<div id="contractsCtrl.contractsToStop" title="' . $locale->t('Contrats à arrêter') . '"></div>

</div>
</div>' .
$view->displayJSBlock('Location.tabBoxesManager.createTabBox("contractsCtrl.report.tabs", null, "400px");');


# Rapport du contrôle des clients
print '
<div id="customersCtrl.report" class="customersCtrlReport">

<fieldset class="sub">
<legend>' . $locale->t('Clients à créer dans Sage') . '</legend>
<div id="customersCtrl.customersToCreate"></div>
</fieldset>

<fieldset class="sub">
<legend>' . $locale->t('Clients mis à jour dans la Gestion des locations') . '</legend>
<div id="customersCtrl.customersToUpdate"></div>
</fieldset>

</div>';


# Prévisualisation des factures à annuler
if ($tabViewData{'isAdmin'})
{
    print '
<div id="cancel.report" class="cancelReport" style="padding: 10px;">
</div>';
}


# Affichage du pied de page
print $view->displayFooter();
