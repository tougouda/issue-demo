use utf8;
use open (':encoding(UTF-8)');
binmode STDOUT, ":raw";    # Pour envoyer le flux en l'état

use strict;
use CGI ('-utf8');

our %tabViewData;

my $cgi = CGI->new();

# Taille du fichier en octets
my $contentLength;
{
    use bytes;
    $contentLength = length($tabViewData{'content'});
}

# Envoi de la réponse au navigateur
my $fileName = sprintf('export%s.txt', $tabViewData{'id'});
print $cgi->header(
                   -type           => 'application/force-download',
                   -attachment     => $fileName,
                   -content_length => $contentLength
                  );

print $tabViewData{'content'};
