use utf8;
use open (':encoding(UTF-8)');
binmode STDOUT, ":raw";    # Pour envoyer le flux en l'état 

use strict;
use CGI ('-utf8');

our %tabViewData;

my $cgi = CGI->new();

# Envoi de la réponse au navigateur
my $fileName = 'exportSousLoc.txt';
print $cgi->header(
                   -type       => 'application/force-download',
                   -attachment => $fileName
                  );

print $tabViewData{'content'};
