use utf8;
use open (':encoding(UTF-8)');

use strict;

use List::Util;

use LOC::Invoice;
use LOC::Invoice::Rent;
use LOC::Json;
use LOC::Locale;
use LOC::Html::Standard;

our $dir = 'accounting/invoice/';

our %tabViewData;
my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
my $currencySymbol = $tabViewData{'currencySymbol'};

# Vue HTML
my $htmlView = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

# Vue JSON
my $view = LOC::Json->new();
print $view->displayHeader();


my $html;
my $modals;

# Nombre de facturations précédentes
my $nbLatestInvoices = values(%{$tabViewData{'tabLatestInvoices'}});

if ($nbLatestInvoices == 0)
{
    $html .= '<span class="noresult">' . $locale->t('Aucun historique n\'est disponible') . '</span>';
}
else
{
    $html .= '
<table class="structure">
<tr>
<td>';

    $html .= '
<table id="latestInvoicing" class="standard">
<tr>
<th></th>
<th>' . $locale->t('Date') . '</th>
<th>' . $locale->t('Type') . '</th>
<th style="text-align: right">' . $locale->t('Factures') . '</th>
<th style="text-align: right">' . $locale->t('Montant HT') . '</th>
<th></th>';

    if ($tabViewData{'isAdmin'})
    {
        $html .= '
<th></th>';
    }

    $html .= '
<th style="width: 100%;"></th>
</tr>';

    my $i = 0;
    my $tabLatestsInvoices = &LOC::Util::sortHashByKeys($tabViewData{'tabLatestInvoices'}, 1);

    foreach (values(%$tabLatestsInvoices))
    {
        my $nb = @$_;
        for (my $j = 0; $j < $nb; $j++)
        {
            my $tabInvoice = $_->[$j];

            my $invoicingType = '';
            if ($tabInvoice->{'type'} eq LOC::Invoice::TYPE_REPAIR)
            {
                $invoicingType = $htmlView->displayImage($dir . 'repair.png') . ' ' . $locale->t('Remise en état');
            }
            if ($tabInvoice->{'rentType'} eq LOC::Invoice::Rent::INVOICETYPE_MONTH)
            {
                $invoicingType = $htmlView->displayImage($dir . 'monthly.png') . ' ' . $locale->t('Mensuelle');
            }
            elsif ($tabInvoice->{'rentType'} eq LOC::Invoice::Rent::INVOICETYPE_INTERMEDIATE)
            {
                $invoicingType = $htmlView->displayImage($dir . 'intermediate.png') . ' ' . $locale->t('Intermédiaire');
            }

            my @tabKeys = keys(%$tabInvoice);
            my $displayAssets = $tabViewData{'displayAssets'};
            my $modalToolTip = ($displayAssets ? $locale->t('Afficher le résumé et les avoirs') :
                                                 $locale->t('Afficher le résumé'));
            my $modalWidth = ($displayAssets ? 600 : 300);
            my $modalHeight = &List::Util::min(&List::Util::max(51 + keys(%{$tabInvoice->{'details'}}) * 15 + 60,
                                                                51 + keys(%{$tabInvoice->{'assets'}}) * 15 + 60),
                                               600);
            my $modalLink = '
<a title="' . $modalToolTip . '" href="#"
   onclick="'
        . 'Location.modalWindowManager.show('
            . '\'invoice[%d]\', '
            . '{'
                . 'contentType: 3, '
                . 'width: ' . $modalWidth . ', '
                . 'height: ' . $modalHeight . ', '
                . 'onclose: function() { '
                    . 'var assetPopup = Location.popupsManager.getPopup(\'assetPopup\'); '
                    . 'if (assetPopup) { assetPopup.close(); } '
                . '}'
            . '}'
        . '); '
        . 'return false;'
    . '">
    %s
</a>';

            $html .= '
<tr>
<td></td>
<td>' . $locale->getDateFormat($tabInvoice->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC) . '</td>
<td>' . $invoicingType . '</td>
<td style="text-align: right">
' . sprintf($modalLink, $i, $locale->getNumberFormat($tabInvoice->{'number'}, 0)) . '
</td>
<td style="text-align: right">
' . sprintf($modalLink, $i, $locale->getCurrencyFormat($tabInvoice->{'amount'}, undef, $currencySymbol)) . '
</td>';

            # Bouton d'export du fichier Sage
            if ($tabInvoice->{'isExportable'})
            {
                my $exportUrl = $htmlView->createURL('accounting:invoice:export',
                                                 {'countryId' => $tabViewData{'country.id'},
                                                  'type'      => $tabInvoice->{'type'},
                                                  'rentType'  => $tabInvoice->{'rentType'},
                                                  'date'      => $tabInvoice->{'date'},
                                                  'id'        => $tabInvoice->{'id'}});

                $html .= '
<td>' . $htmlView->displayTextButton('', $dir . 'export(|Over).png', $exportUrl, $locale->t('Télécharger le fichier Sage')) . '</td>';
            }
            else
            {
                $html .= '
<td></td>';
            }

            # Bouton d'annulation de facturation (mode admin seulement)
            if ($tabViewData{'isAdmin'})
            {
                if ($tabInvoice->{'isCancellable'})
                {
                    my $cancelUrl = $htmlView->createURL('accounting:invoice:checkCancel',
                                                     {'countryId' => $tabViewData{'country.id'},
                                                      'type'      => $tabInvoice->{'type'},
                                                      'rentType'  => $tabInvoice->{'rentType'},
                                                      'date'      => $tabInvoice->{'date'},
                                                      'id'        => $tabInvoice->{'id'}});

                    $html .= '
<td>
<div style="width: 16px">
' . $htmlView->displayTextButton('', $dir . 'cancel(|Over).png', 'checkCancel(' . $i . ', "' . $htmlView->toJSEntities($cancelUrl) . '")',
                             $locale->t('Annuler la facturation'), undef, {'id' => 'cancel[' . $i . '].btn'}) . '
<span id="cancel[' . $i . '].waiting" class="waiting hidden">&nbsp;</span>
</div>
</td>';
                }
                else
                {
                    $html .= '
<td></td>';
                }
            }

            $html .= '
<td style="width: 100%;"></td>
</tr>';

            # Traces
            my $nbHistory = keys(%{$tabInvoice->{'history'}});
            if ($nbHistory > 0)
            {
                $html .= '
<tr>
<td></td>
<td colspan="7">
<div class="invoiceLogsCointainer">';

                $html .= '
<table class="invoiceLogs">';

                foreach my $tabHistory (values(%{$tabInvoice->{'history'}}))
                {
                    my $className = '';
                    if ($tabHistory->{'user.state.id'} eq LOC::User::STATE_INACTIVE)
                    {
                        $className = ' class="user inactive"';
                    }

                    $html .= '
<tr>
<td>' . $locale->getDateFormat($tabHistory->{'datetime'}, LOC::Locale::FORMAT_DATETIME_NUMERIC) . '</td>
<td' . $className . '>' . $tabHistory->{'user.fullName'} . '</td>
<td>' . $tabHistory->{'eventType.label'} . '</td>
</tr>';
                }

                $html .= '
</table>';

                $html .= '
</div>
</td>
</tr>';
            }

            # Contenu de la fenêtre modale
            # Résumé
            $modals .= '
<div id="invoice[' . $i . ']" class="invoiceSummary">

<table class="structure">
<tr>
<td>

<fieldset class="sub">
<legend>' . $locale->t('Résumé') . '</legend>
<table class="standard" style="width: 100%;">
<tr>
<th>' . $locale->t('Agence') . '</th>
<th style="text-align: right;">' . $locale->t('Nb factures') . '</th>
<th style="text-align: right;">' . $locale->t('Montant') . '</th>
</tr>';

            foreach my $agencyId (sort keys(%{$tabInvoice->{'details'}}))
            {
                $modals .= '
<tr>
<td>' . $agencyId . '</td>
<td style="text-align: right;">' . $locale->getNumberFormat($tabInvoice->{'details'}->{$agencyId}->{'number'}, 0) . '</td>
<td style="text-align: right;">' . $locale->getCurrencyFormat($tabInvoice->{'details'}->{$agencyId}->{'amount'}, undef, $currencySymbol) . '</td>
</tr>';
            }

            $modals .= '
<tr class="total">
<td>' . $locale->t('Total') . '</td>
<td style="text-align: right;">' . $locale->getNumberFormat($tabInvoice->{'number'}, 0) . '</td>
<td style="text-align: right;">' . $locale->getCurrencyFormat($tabInvoice->{'amount'}, undef, $currencySymbol) . '</td>
</tr>
</table>
</fieldset>

</td>';

            # Avoirs
            if ($displayAssets)
            {
                $modals .= '
<td>

<fieldset class="sub">
<legend>' . $locale->t('Avoirs') . '</legend>';

                if (keys(%{$tabInvoice->{'assets'}}) > 0)
                {
                    $modals .= '
<table class="standard" style="width: 100%;">
<tr>
<th>' . $locale->t('Contrat') . '</th>
<th>' . $locale->t('Code client') . '</th>
<th style="text-align: right;">' . $locale->t('Montant') . '</th>
<th style="text-align: right;"></th>
</tr>';

                    my $j = 0;
                    foreach my $tabAsset (values(%{$tabInvoice->{'assets'}}))
                    {
                        # Lien vers le contrat
                        my $contractUrl = $htmlView->createURL('rent:rentContract:view',
                                                           {'contractId' => $tabAsset->{'contract.id'}});
                        my $contract = sprintf('<a href="%s" target="_blank">%s</a>', $contractUrl, $tabAsset->{'contract.code'});

                        # Lien vers la fiche du client
                        my $customer = $tabAsset->{'customer.code'};
                        if ($tabAsset->{'customer.id'} != 0)
                        {
                            my $customerUrl = &LOC::Globals::get('locationUrl') . '/cgi-bin/old/location/infoclient.cgi?value=' .
                                              $tabAsset->{'customer.id'};
                            $customer = sprintf('<a href="%s" target="_blank">%s</a>', $customerUrl, $customer);
                        }

                        my $assetUrl = &LOC::Request::createRequestUri(
                                'accounting:invoice:view',
                                {
                                    'type' => LOC::Invoice::TYPE_RENT,
                                    'invoiceId' => $tabAsset->{'id'}
                                }
                            );
                        my $assetLink = '<a href="javascript:;" '
                            . 'onclick="Location.popupsManager.setContainer(Location.modalWindowManager.getEl()); '
                                . 'var assetPopup = Location.popupsManager.createPopup('
                                    . '\'assetPopup\', \'\', \'center\', \'center\', 800, 600, {\'contentType\': 1}'
                                . '); '
                                . 'assetPopup.open(\'' . $assetUrl . '\');" '
                            . 'title="' . $locale->t('Visualiser l\'avoir') . '">#</a>';

                        $modals .= '
<tr>
<td>' . $contract .'</td>
<td>' . $customer . '</td>
<td style="text-align: right;">' . $locale->getCurrencyFormat($tabAsset->{'amount'}, undef, $currencySymbol) . '</td>
<td style="text-align: right;">' . $assetLink . '</td>
</tr>';

                        $j++;
                    }
    
                    $modals .= '
</table>';
                }
                else
                {
                    $modals .= '<span class="noresult">' . $locale->t('Aucun avoir pour cette facturation') . '</span>';
                }
            }

            $modals .= '
</fieldset>

</td>
</tr>
</table>
';

if ($tabInvoice->{'unexportable'} > 0)
{
    $modals .= '<hr class="dotted" />
' . $locale->tn(
        '<b>%s</b> facture n\'est pas exportable car elle a un montant inférieur à <em>%s</em>.',
        '<b>%s</b> factures ne sont pas exportables car elles ont un montant inférieur à <em>%s</em>.',
        $tabInvoice->{'unexportable'},
        $locale->getNumberFormat($tabInvoice->{'unexportable'}, 0),
        $locale->getCurrencyFormat($tabInvoice->{'exportableMinAmount'})
    );
}

$modals .= '
</div>';

            $i++;
        }
    }

    $html .= '
</table>

</td>
<td style="width: 100%;">
</td>
</tr>
</table>';
}


# Envoi du code HTML
$tabViewData{'tabResult'}->{'html'} = $html . $modals;

print $view->encode($tabViewData{'tabResult'});
