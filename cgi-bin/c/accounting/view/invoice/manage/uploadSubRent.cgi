use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;

# Répertoire CSS/JS/Images
my $dir = 'accounting/invoice/';


our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});


# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'manage/upload.css');

# JavaScripts
$view->addJSSrc($dir . 'manage/upload.js');

my $readFileUrl = $view->createURL('accounting:invoice:readFileSubRent',
                                   {'filePath'  => $tabViewData{'filePath'},
                                    'countryId' => $tabViewData{'countryId'},
                                    'nopiece'   => $tabViewData{'nopiece'}});
;

$view->addBodyEvent('onload',
                    'pageInitSubRent(' . $tabViewData{'isFileImported'} . ', "' .
                              $view->toJSEntities($readFileUrl) . '");');

# Affichage de l'entête
print $view->displayHeader();

# Formulaire d'upload du fichier
print $view->startForm(undef, 'multipart/form-data', 'uploadForm');

print $view->displayFormInputFile('file', 20 * 1024 * 1024);
print $view->displayFormInputHidden('nopiece', $tabViewData{'nopiece'});

# Affichage du pied de page
print $view->endForm();
print $view->displayFooter();
