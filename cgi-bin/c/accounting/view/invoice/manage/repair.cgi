use utf8;
use open (':encoding(UTF-8)');

print '
<fieldset id="nextRepairInvoicing">
<legend>' . $locale->t('Prochaine facturation de remise en état') . '</legend>';

print '<div class="nextInvoicingHeader">';
print '<span class="date">';
print $locale->getDateFormat($tabViewData{'nextRepairInvoice'}->{'date'}, LOC::Locale::FORMAT_DATEYEAR_ABBREV);
print '</span>';
print '</div>';

my $lastNo = $tabViewData{'repairLastDocumentNo'};
if ($lastNo eq '')
{
    $lastNo = '<i>' . $locale->t('aucun') . '</i>';
}
print '<div class="nextInvoicingHeader">';
print $locale->t('Dernier numéro de pièce enregistré dans la Gestion des locations : <b>%s</b>', $lastNo);
print '</div>';

my $uploadUrl = $view->createURL('accounting:invoice:upload',
                                 {'invoicingId' => $tabViewData{'nextRepairInvoice'}->{'id'},
                                  'date'        => $tabViewData{'nextRepairInvoice'}->{'date'},
                                  'rentType'    => $tabViewData{'nextRepairInvoice'}->{'rentType'},
                                  'type'        => LOC::Invoice::TYPE_REPAIR});
my $repairCustomersFileInput = '<iframe src="' . $uploadUrl . '" id="repairUploadFrame" name="repairUploadFrame" frameborder="0"></iframe>';

print '<table class="formTable">';

# Ligne "Contrôle des clients"
print '<tr>';

print '<td class="label">' .
    $locale->t('Contrôle des clients') . ' ' .
    $view->displayHelp($customersCtrlHelp) .
    '</td>';

print '<td>';
print '<span id="repairCustomersCtrl.yes" class="result hidden">' . $view->displayBoolean(1, '') . '</span>';
print '<span id="repairCustomersCtrl.no" class="result">' . $view->displayBoolean(0, '') . '</span>';
print '<span id="repairCustomersCtrl.waiting" class="waiting hidden">&nbsp;</span>';
print '</td>';

print '<td class="separator"></td>';

print '<td>' . $repairCustomersFileInput . ' ' .
    $view->displayTextButton($locale->t('Lancer'), $dir . 'customer(|Over).png',
                             'upload(\'' . LOC::Invoice::TYPE_REPAIR . '\');',
                             $locale->t('Lancer le contrôle des clients'), undef,
                             {'id' => 'repairCustomersCtrl.btn'}) .
    '<span class="separator">&nbsp;</span>' .
    $view->displayTextButton($locale->t('Rapport'), $dir . 'report(|Over).png', 'showCustomersReport("repair")',
                             $locale->t('Afficher le rapport du contrôle des clients'), undef,
                             {'id' => 'repairCustomersCtrl.reportBtn', 'class' => 'locCtrlButton hidden'}) .
    '</td>';

print '</tr>';

print '<tr>';
print '<td class="label"></td>';
print '<td colspan="2"></td>';
print '<td><div id="repairCustomersCtrl.messages" class="messages">&nbsp;</div></td>';
print '</tr>';

# Ligne "Numéro de pièce"
my $tabNumberAttributes = {'maxlength' => 4};
if (!$tabViewData{'nextRepairInvoice'}->{'readyToInvoice'})
{
    $tabNumberAttributes->{'readonly'} = 1;
}
my $repairNumberInput = $view->displayFormInputText('repairNumber', '', LOC::Html::TEXTMODE_NORMAL, $tabNumberAttributes);

# URL de la facturation de remise en état
my $invoiceUrl = $view->createURL('accounting:invoice:invoice',
                                  {'countryId'   => $tabViewData{'country.id'},
                                   'date'        => $tabViewData{'nextRepairInvoice'}->{'date'},
                                   'type'        => LOC::Invoice::TYPE_REPAIR});
my $invoiceAction = 'invoice("' . LOC::Invoice::TYPE_REPAIR . '", "' . $view->toJSEntities($invoiceUrl) . '", ' .
                            '"' . $view->toJSEntities($progressionUrl) . '")';

my $disabled = ($tabViewData{'nextRepairInvoice'}->{'date'} gt $tabViewData{'today'});
my $tabBtnAttributes = {'id' => 'repairInvoiceBtn', 'class' => 'locCtrlButton' . ($disabled ? ' disabled' : '')};

print '<tr>';
print '<td class="label">' .
    $locale->t('Numéro de pièce') . ' ' .
    $view->displayHelp($invoiceHelp) .
    '</td>';
print '<td></td>';
print '<td class="separator"></td>';
print '<td>' .
    $repairNumberInput . ' ' .
    '<span id="repair.invoiceBtn"' . ($tabViewData{'repairRequestId'} > 0 ? ' class="hidden"' : '') . '>' . 
    $view->displayTextButton($locale->t('Facturer'), 'coins(|Over).png', $invoiceAction,
                             $locale->t('Lancer la facturation'), undef,
                             $tabBtnAttributes) .
    '</span>' .
    '<span id="repair.progression"' . ($tabViewData{'repairRequestId'} > 0 ? '' : ' class="hidden"') . '>' .
    '<div class="progressbar"><div id="repair.progression.bar" class="progression"></div></div>' .
    $view->displayImage($dir . 'invoicing.gif') .
    '</span>' .
    '</td>';
print '</tr>';

print '</table>';

print '
</fieldset>';
