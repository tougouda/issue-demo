use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Json;
use LOC::Locale;

our %tabViewData;
my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});

my $view = LOC::Json->new();
print $view->displayHeader();


# Code HTML à afficher dans le rapport
my $html ='<div class="noresult">' . $locale->t('Aucun') . '</div>';

if (ref($tabViewData{'tabResult'}->{'customersToCreate'}) eq 'ARRAY' &&
    @{$tabViewData{'tabResult'}->{'customersToCreate'}} > 0)
{
    $html = '
<table class="standard" style="width: 100%;">
<tr>
<th>' . $locale->t('Contrat') . '</th>
<th>' . $locale->t('Code client') . '</th>
<th>' . $locale->t('Raison sociale') . '</th>
<th>' . $locale->t('Code collectif') . '</th>
<th>' . $locale->t('Nombre de factures') . '</th>
<th>' . $locale->t('Agence') . '</th>
</tr>';

    foreach my $tabCustomer (sort {$a->{'name'} cmp $b->{'name'}} @{$tabViewData{'tabResult'}->{'customersToCreate'}})
    {
        my $customerName = $tabCustomer->{'name'};
        if (length($customerName) > 20)
        {
            $customerName = substr($customerName, 0, 20) . '...';
        }

        my $contractUrl = &LOC::Request::createRequestUri('rent:rentContract:view',
                                                          {'contractId' => $tabCustomer->{'contract.id'}});
        my $customerUrl = &LOC::Globals::get('locationUrl') . '/cgi-bin/old/location/infoclient.cgi?value=' .
                                                              $tabCustomer->{'id'};

        $html .= '
<tr>
<td><a href="' . $contractUrl . '" target="_blank">' . $tabCustomer->{'contract.code'} . '</a></td>
<td><a href="' . $customerUrl . '" target="_blank">' . $tabCustomer->{'code'} . '</a></td>
<td><a href="' . $customerUrl . '" target="_blank">' . $customerName . '</a></td>
<td>' . $tabCustomer->{'accountingCode'} . '</td>
<td>' . $tabCustomer->{'invoicesNo'} . '</td>
<td>' . $tabCustomer->{'contract.agency.id'} . '</td>
</tr>';
    }

    $html .= '
</table>';
}

$tabViewData{'tabResult'}->{'html'} = $html;

print $view->encode($tabViewData{'tabResult'});
