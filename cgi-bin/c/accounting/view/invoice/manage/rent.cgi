use utf8;
use open (':encoding(UTF-8)');

print '
<fieldset id="nextRentInvoicing">
<legend>' . $locale->t('Prochaine facturation de location') . '</legend>';

if (defined $tabViewData{'tabNextRentInvoice'}->{'date'})
{
    print '<div class="nextInvoicingHeader">';
    print '<span class="date">';
    print $locale->getDateFormat($tabViewData{'tabNextRentInvoice'}->{'date'}, LOC::Locale::FORMAT_DATEYEAR_ABBREV);
    print '</span>';
    print '<span class="type">';
    if ($tabViewData{'tabNextRentInvoice'}->{'rentType'} eq LOC::Invoice::Rent::INVOICETYPE_MONTH)
    {
        print $view->displayImage($dir . 'monthly.png') . ' ' . $locale->t('Mensuelle');
    }
    elsif ($tabViewData{'tabNextRentInvoice'}->{'rentType'} eq LOC::Invoice::Rent::INVOICETYPE_INTERMEDIATE)
    {
        print $view->displayImage($dir . 'intermediate.png') . ' ' . $locale->t('Intermédiaire');
    }
    print '</span>';
    print '</div>';

    my $lastNo = $tabViewData{'rentLastDocumentNo'};
    if ($lastNo eq '')
    {
        $lastNo = '<i>' . $locale->t('aucun') . '</i>';
    }
    print '<div class="nextInvoicingHeader subrent_' . $tabViewData{'isSubRent'} . '">';
    print $locale->t('La location sera facturée jusqu\'au <b>%s</b>',
                     $locale->getDateFormat($tabViewData{'nextRentalPeriodEndDate'}, LOC::Locale::FORMAT_DATE_NUMERIC));
    print '<br />';
    print $locale->t('Dernier numéro de pièce enregistré dans la Gestion des locations : <b>%s</b>', $lastNo);
    print '</div>';

    print '<table class="formTable">';

    # Ligne "Contrôle des contrats"
    my $contractsCtrlHelp = $view->displayHelp($locale->t('La facturation ne peut être lancée que si le contrôle des contrats est OK'));
    my $contractsCtrlUrl = $view->createURL('accounting:invoice:checkContracts',
                                            {'id'        => $tabViewData{'tabNextRentInvoice'}->{'id'},
                                             'date'      => $tabViewData{'tabNextRentInvoice'}->{'date'},
                                             'rentType'  => $tabViewData{'tabNextRentInvoice'}->{'rentType'},
                                             'countryId' => $tabViewData{'country.id'}});

    print '<tr>';

    print '<td class="label">' . $locale->t('Contrôle des contrats') . ' ' . $contractsCtrlHelp . '</td>';

    print '<td>';
    print '<span id="rentContractsCtrl.yes" class="result hidden">' . $view->displayBoolean(1, '') . '</span>';
    print '<span id="rentContractsCtrl.no" class="result">' . $view->displayBoolean(0, '') . '</span>';
    print '<span id="rentContractsCtrl.waiting" class="waiting hidden">&nbsp;</span>';
    print '</td>';

    print '<td class="separator"></td>';

    print '<td>' .
        $view->displayTextButton($locale->t('Lancer'), $dir . 'contract(|Over).png',
                                 'checkContracts("' . $view->toJSEntities($contractsCtrlUrl) . '");',
                                 $locale->t('Lancer le contrôle des contrats'), undef,
                                 {'id' => 'rentContractsCtrl.btn'}) .
        '<span class="separator">&nbsp;</span>' .
        $view->displayTextButton($locale->t('Rapport'), $dir . 'report(|Over).png', 'showContractsReport()',
                                 $locale->t('Afficher le rapport du contrôle des contrats'), undef,
                                 {'id' => 'rentContractsCtrl.reportBtn', 'class' => 'locCtrlButton hidden'}) .
        '</td>';

    print '</tr>';

    # Ligne des messages du contrôle des contrats
    my $uploadUrl = $view->createURL('accounting:invoice:upload',
                                     {'invoicingId' => $tabViewData{'tabNextRentInvoice'}->{'id'},
                                      'date'        => $tabViewData{'tabNextRentInvoice'}->{'date'},
                                      'rentType'    => $tabViewData{'tabNextRentInvoice'}->{'rentType'},
                                      'type'        => LOC::Invoice::TYPE_RENT,
                                      'countryId'  => $tabViewData{'country.id'}});
    my $rentCustomersFileInput = '<iframe src="' . $uploadUrl . '" id="rentUploadFrame" name="rentUploadFrame" frameborder="0"></iframe>';

    # Ligne "Contrôle des clients"
    print '<tr>';
    
    print '<td class="label">' .
        $locale->t('Contrôle des clients') . ' ' .
        $view->displayHelp($customersCtrlHelp) .
        '</td>';
    
    print '<td>';
    print '<span id="rentCustomersCtrl.yes" class="result hidden">' . $view->displayBoolean(1, '') . '</span>';
    print '<span id="rentCustomersCtrl.no" class="result">' . $view->displayBoolean(0, '') . '</span>';
    print '<span id="rentCustomersCtrl.waiting" class="waiting hidden">&nbsp;</span>';
    print '</td>';
    
    print '<td class="separator"></td>';
    
    print '<td>' . $rentCustomersFileInput . ' ' .
        $view->displayTextButton($locale->t('Lancer'), $dir . 'customer(|Over).png',
                                 'upload(\'' . LOC::Invoice::TYPE_RENT . '\');',
                                 $locale->t('Lancer le contrôle des clients'), undef,
                                 {'id' => 'rentCustomersCtrl.btn'}) .
        '<span class="separator">&nbsp;</span>' .
        $view->displayTextButton($locale->t('Rapport'), $dir . 'report(|Over).png', 'showCustomersReport("rent")',
                                 $locale->t('Afficher le rapport du contrôle des clients'), undef,
                                 {'id' => 'rentCustomersCtrl.reportBtn', 'class' => 'locCtrlButton hidden'}) .
        '</td>';

    print '</tr>';

    # Ligne des messages du contrôle des clients
    print '<tr>';
    print '<td class="label"></td>';
    print '<td colspan="2"></td>';
    print '<td><div id="rentCustomersCtrl.messages" class="messages">&nbsp;</div></td>';
    print '</tr>';

    # Ligne "Numéro de pièce"
    my $rentNumberInput  = $view->displayFormInputText('rentNumber', $tabViewData{'rentNumber'},
                                                       LOC::Html::TEXTMODE_NORMAL,
                                                       {'maxlength' => 4, 'readonly' => 'readonly'});
    my $invoiceUrl = $view->createURL('accounting:invoice:invoice',
                                      {'countryId'   => $tabViewData{'country.id'},
                                       'date'        => $tabViewData{'tabNextRentInvoice'}->{'date'},
                                       'rentType'    => $tabViewData{'tabNextRentInvoice'}->{'rentType'},
                                       'type'        => LOC::Invoice::TYPE_RENT});
    my $invoiceAction = 'invoice("' . LOC::Invoice::TYPE_RENT . '", "' . $view->toJSEntities($invoiceUrl) . '", ' .
                                '"' . $view->toJSEntities($progressionUrl) . '")';

    print '<tr>';
    print '<td class="label">' .
        $locale->t('Numéro de pièce') . ' ' .
        $view->displayHelp($invoiceHelp) .
        '</td>';
    print '<td></td>';
    print '<td class="separator"></td>';
    print '<td>' .
        $rentNumberInput . ' ' .
        '<span id="rent.invoiceBtn"' . ($tabViewData{'rentRequestId'} > 0 ? ' class="hidden"' : '') . '>' . 
        $view->displayTextButton($locale->t('Facturer'), 'coins(|Over).png', $invoiceAction,
                                 $locale->t('Lancer la facturation'), undef,
                                 {'id' => 'rentInvoiceBtn', 'class' => 'locCtrlButton disabled'}) .
        '</span>' .
        '<span id="rent.progression"' . ($tabViewData{'rentRequestId'} > 0 ? '' : ' class="hidden"') . '>' .
        '<div class="progressbar"><div id="rent.progression.bar" class="progression"></div></div>' .
        $view->displayImage($dir . 'invoicing.gif') .
        '</span>' .
        '</td>';
    print '</tr>';

    print '</table>';
}
else
{
    print '<span class="noresult">' . $locale->t('Aucune facturation de location n\'est prévue') . '</span>';
}

# Champs caché pour le pays
print $view->displayFormInputHidden('countryId', $tabViewData{'country.id'});


print '
</fieldset>';
