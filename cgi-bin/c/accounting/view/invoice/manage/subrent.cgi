use utf8;
use open (':encoding(UTF-8)');

print '
<fieldset id="subRent">
<legend>' . $locale->t('Sous-location') . '</legend>';

print '<table class="formTable">';

# Ligne "fichier de sousloc"
my $uploadUrl = $view->createURL('accounting:invoice:uploadSubRent');
my $subRentFileInput = '<iframe src="' . $uploadUrl . '" id="subRentUploadFrame" name="subRentUploadFrame" frameborder="0"></iframe>';
print '<tr>';

print '<td class="label">' .
    $locale->t('Fichier de sous-location') . ' ' .
    $view->displayHelp($locale->t('Fichier au format CSV contenant les informations de la sous-location')) .
    '</td>';

print '<td class="separator"></td>';
print '<td>' . $subRentFileInput . '</td>';
my $test = $view->createURL('accounting:invoice:exportSubRent',{'id' => 12}, 0);

print '<td><span id="fileToImport">' . $view->displayTextButton($locale->t('Télécharger le fichier Sage'), 
                                        'accounting/invoice/export(|Over).png',
                                        $test,
                                        $locale->t('Télécharger le fichier Sage'),
                                        'left',
                                        {'style' => 'display: none'}
                                        ) . '</td>';
print '</tr>';

print '<tr>';
print '<td class="label"></td>';
print '<td colspan="2"></td>';
print '<td><div id="repairCustomersCtrl.messages" class="messages">&nbsp;</div></td>';
print '</tr>';

## Ligne "Numéro de pièce"
my $tabNumberAttributes = {'maxlength' => 4};

my $subRentNumberInput = $view->displayFormInputText('subRentNumber', '', LOC::Html::TEXTMODE_NUMERIC, $tabNumberAttributes);


print '<tr>';
print '<td class="label">' .
    $locale->t('Numéro de pièce') . ' ' .
    $view->displayHelp($invoiceHelp) .
    '</td>';
print '<td class="separator"></td>';
print '<td colspan="2">' . $tabViewData{'prefixSubRentNumber'} . ' ' .
    $subRentNumberInput . ' ' .
    '<span id="subrent.invoiceBtn">' . 
    $view->displayTextButton($locale->t('Générer'), 'coins(|Over).png', 'uploadSubRent();',
                             $locale->t('Générer le fichier SAGE')) .
    '</span>
    <span id="subrent.waiting" class="waiting hidden">&nbsp;</span>
     </td>';
print '</tr>';

print '</table>';
print $view->displayFormInputHidden('type', '');
print '
</fieldset>';
