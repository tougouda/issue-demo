use utf8;
use open (':encoding(UTF-8)');

# Constants: Encodage
# ENCODING_SOURCEFILE - Encodage du fichier source
# ENCODING_EXCELFILE  - Encodage du fichier Excel
use constant
{
    ENCODING_SOURCEFILE => 'UTF-8',
    ENCODING_EXCELFILE  => 'CP1252'
};

use strict;

use CGI ('-utf8');
use Spreadsheet::WriteExcel;

use LOC::Locale;

our %tabViewData;

my $cgi = CGI->new();
my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});

# Téléchargement forcé du fichier
my $fileName = sprintf('checkContracts%s.xls', $tabViewData{'id'});
print $cgi->header(
                   -type       => 'application/force-download',
                   -attachment => $fileName,
                   -charset    => $tabViewData{'charset'},
                  );

# Nouveau classeur Excel envoyé sur la sortie standard
my $workbook = Spreadsheet::WriteExcel->new('-');
my $title = $locale->t('Contrôle des contrats avant la facturation');
my $author = $locale->t('Gestion des locations') . ' ' . &LOC::Globals::get('version');
my $company = 'Acces industrie';
$workbook->set_properties('title' => $title, 'author'=> $author, 'company' => $company);

# Format de la ligne d'en-tête
my $headerFmt = $workbook->add_format();
$headerFmt->set_italic();
my $headerBlue = $workbook->set_custom_color(12, 232, 243, 249);
$headerFmt->set_bg_color($headerBlue);

# Format de date
my $dateFmt = $workbook->add_format();
$dateFmt->set_num_format(14);

# Format barré
my $strikeoutFmt = $workbook->add_format();
$strikeoutFmt->set_font_strikeout();



# Contrats à arrêter
my %tabContractsToStop = %{$tabViewData{'checkContracts'}->{'contractsToStop'}};

if (keys(%tabContractsToStop) > 0)
{
    my $worsheetLabel = $locale->t('Contrats à arrêter');
    my $worksheet = $workbook->add_worksheet($worsheetLabel);

    # Ligne d'en-tête
    $worksheet->write(0, 0, $locale->t('Agence'), $headerFmt);
    $worksheet->write(0, 1, $locale->t('Contrat'), $headerFmt);
    $worksheet->write(0, 2, $locale->t('Date de fin'), $headerFmt);
    $worksheet->write(0, 3, $locale->t('Contact'), $headerFmt);
    $worksheet->freeze_panes(1, 0);
    $worksheet->set_column(0, 0, 8);
    $worksheet->set_column(1, 1, 16);
    $worksheet->set_column(2, 2, 10);
    $worksheet->set_column(3, 3, 30);

    my $row = 1;
    foreach my $contractId (sort {$tabContractsToStop{$a}->{'agency.id'} cmp $tabContractsToStop{$b}->{'agency.id'}} keys(%tabContractsToStop))
    {
        my $userFmt = undef;
        if ($tabContractsToStop{$contractId}->{'user.state.id'} eq LOC::User::STATE_INACTIVE)
        {
            $userFmt = $strikeoutFmt;
        }

        my $agencyId = $tabContractsToStop{$contractId}->{'agency.id'};
        my $contractCode = $tabContractsToStop{$contractId}->{'code'};
        my $userName = $tabContractsToStop{$contractId}->{'user.fullName'};

        $worksheet->write($row, 0, $agencyId);
        $worksheet->write($row, 1, $contractCode);
        $worksheet->write_date_time($row, 2, $tabContractsToStop{$contractId}->{'endDate'} . 'T', $dateFmt);
        $worksheet->write($row, 3, $userName, $userFmt);

        $row++;
    }
}



# Carburants à saisir
my %tabFuelsToType = %{$tabViewData{'checkContracts'}->{'fuelsToType'}};

if (keys(%tabFuelsToType) > 0)
{
    my $worsheetLabel = $locale->t('Carburants à saisir');
    my $worksheet = $workbook->add_worksheet($worsheetLabel);

    # Ligne d'en-tête
    $worksheet->write(0, 0, $locale->t('Agence'), $headerFmt);
    $worksheet->write(0, 1, $locale->t('Contrat'), $headerFmt);
    $worksheet->write(0, 2, $locale->t('Machine'), $headerFmt);
    $worksheet->write(0, 3, $locale->t('Contact'), $headerFmt);
    $worksheet->freeze_panes(1, 0);
    $worksheet->set_column(0, 0, 8);
    $worksheet->set_column(1, 1, 16);
    $worksheet->set_column(2, 2, 10);
    $worksheet->set_column(3, 3, 30);

    my $row = 1;
    foreach my $contractId (sort {$tabFuelsToType{$a}->{'agency.id'} cmp $tabFuelsToType{$b}->{'agency.id'}} keys(%tabFuelsToType))
    {
        my $userFmt = undef;
        if ($tabFuelsToType{$contractId}->{'user.state.id'} eq LOC::User::STATE_INACTIVE)
        {
            $userFmt = $strikeoutFmt;
        }

        my $agencyId = $tabFuelsToType{$contractId}->{'agency.id'};
        my $contractCode = $tabFuelsToType{$contractId}->{'code'};
        my $userName = $tabFuelsToType{$contractId}->{'user.fullName'};

        $worksheet->write($row, 0, $agencyId);
        $worksheet->write($row, 1, $contractCode);
        $worksheet->write($row, 2, $tabFuelsToType{$contractId}->{'machine.parkNumber'});
        $worksheet->write($row, 3, $userName, $userFmt);

        $row++;
    }
}

1;
