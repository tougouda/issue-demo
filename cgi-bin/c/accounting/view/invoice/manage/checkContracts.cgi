use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Json;
use LOC::Locale;

our %tabViewData;
my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});

my $view = LOC::Json->new();
print $view->displayHeader();


# Code HTML à afficher dans le rapport
my $tabResult = {
                 'readyToInvoice' => $tabViewData{'checkContracts'}->{'readyToInvoice'},
                 'control'        => $tabViewData{'checkContracts'}->{'control'},
                 'html'           => {
                                      'contractsToStop'  => '<div class="noresult">' . $locale->t('Aucun') . '</div>',
                                      'fuelsToType'      => '<div class="noresult">' . $locale->t('Aucun') . '</div>',
                                     }
                };

# Contrats à arrêter
my %tabContractsToStop = %{$tabViewData{'checkContracts'}->{'contractsToStop'}};
if (keys(%tabContractsToStop) > 0)
{
    my $html = '
<table class="standard" style="width: 100%;">
<tr>
<th>' . $locale->t('Agence') . '</th>
<th>' . $locale->t('Contrat') . '</th>
<th>' . $locale->t('Date de fin') . '</th>
<th>' . $locale->t('Contact') . '</th>
</tr>';

    foreach my $contractId (sort {$tabContractsToStop{$a}->{'agency.id'} cmp $tabContractsToStop{$b}->{'agency.id'}} keys(%tabContractsToStop))
    {
        my $contractUrl = &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $contractId});

        my $userClass = '';
        if ($tabContractsToStop{$contractId}->{'user.state.id'} eq LOC::User::STATE_INACTIVE)
        {
            $userClass = ' class="user inactive"';
        }

        $html .= '
<tr>
<td>' . $tabContractsToStop{$contractId}->{'agency.id'} . '</td>
<td><a href="' . $contractUrl . '" target="_blank">' . $tabContractsToStop{$contractId}->{'code'} . '</a></td>
<td>' . $locale->getDateFormat($tabContractsToStop{$contractId}->{'endDate'}, LOC::Locale::FORMAT_DATE_NUMERIC) . '</td>
<td' . $userClass . '>' . $tabContractsToStop{$contractId}->{'user.fullName'} . '</td>
</tr>';
    }

    $html .= '
</table>';

    $tabResult->{'html'}->{'contractsToStop'} = $html;
}

# Carburants à saisir
my %tabFuelsToType = %{$tabViewData{'checkContracts'}->{'fuelsToType'}};
if (keys(%tabFuelsToType) > 0)
{
    my $html = '
<table class="standard" style="width: 100%;">
<tr>
<th>' . $locale->t('Agence') . '</th>
<th>' . $locale->t('Contrat') . '</th>
<th>' . $locale->t('Machine') . '</th>
<th>' . $locale->t('Contact') . '</th>
</tr>';

    foreach my $contractId (sort {$tabFuelsToType{$a}->{'agency.id'} cmp $tabFuelsToType{$b}->{'agency.id'}} keys(%tabFuelsToType))
    {
        my $contractUrl = &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $contractId});

        my $machineUrl = &LOC::Machine::getUrl($tabFuelsToType{$contractId}->{'machine.id'});

        my $userClass = '';
        if ($tabFuelsToType{$contractId}->{'user.state.id'} eq LOC::User::STATE_INACTIVE)
        {
            $userClass = ' class="user inactive"';
        }

        $html .= '
<tr>
<td>' . $tabFuelsToType{$contractId}->{'agency.id'} . '</td>
<td><a href="' . $contractUrl . '" target="_blank">' . $tabFuelsToType{$contractId}->{'code'} . '</a></td>
<td><a href="' . $machineUrl . '" target="_blank">' . $tabFuelsToType{$contractId}->{'machine.parkNumber'} . '</a></td>
<td' . $userClass . '>' . $tabFuelsToType{$contractId}->{'user.fullName'} . '</td>
</tr>';
    }

    $html .= '
</table>';

    $tabResult->{'html'}->{'fuelsToType'} = $html;
}

print $view->encode($tabResult);
