use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Json;
use LOC::Locale;

our %tabViewData;
my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});

my $view = LOC::Json->new();
print $view->displayHeader();

# Code HTML à afficher dans le rapport
my $html ='<div class="noresult">' . $locale->t('Aucun') . '</div>';

if (ref($tabViewData{'tabResult'}) eq 'ARRAY' && @{$tabViewData{'tabResult'}} > 0)
{
    $html = '
<table class="standard" style="width: 100%;">
<tr>
<th>' . $locale->t('Contrat') . '</th>
<th>' . $locale->t('Code client') . '</th>
<th>' . $locale->t('Raison sociale') . '</th>
<th colspan="3">' . $locale->t('Code collectif') . '</th>
<th colspan="3">' . $locale->t('Nombre de factures') . '</th>
<th>' . $locale->t('Agence') . '</th>
</tr>';

    foreach my $tabCustomer (sort {$a->{'name'} cmp $b->{'name'}} @{$tabViewData{'tabResult'}})
    {
        my $customerName = $tabCustomer->{'name'};
        if (length($customerName) > 20)
        {
            $customerName = substr($customerName, 0, 20) . '...';
        }

        my $contractUrl = &LOC::Request::createRequestUri('rent:rentContract:view',
                                                          {'contractId' => $tabCustomer->{'contract.id'}});
        my $customerUrl = &LOC::Globals::get('locationUrl') . '/cgi-bin/old/location/infoclient.cgi?value=' .
                                                              $tabCustomer->{'id'};

        $html .= '
<tr>
<td><a href="' . $contractUrl . '" target="_blank">' . $tabCustomer->{'contract.code'} . '</a></td>
<td><a href="' . $customerUrl . '" target="_blank">' . $tabCustomer->{'code'} . '</a></td>
<td><a href="' . $customerUrl . '" target="_blank">' . $customerName . '</a></td>';

        if ($tabCustomer->{'accountingCode'} eq $tabCustomer->{'accountingCode.new'})
        {
            $html .= '
<td colspan="3">' . $tabCustomer->{'accountingCode.new'} . '</td>';
        }
        else
        {
            $html .= '
<td style="text-align: right;">' . $tabCustomer->{'accountingCode'} . '</td>
<td style="text-align: center;">&rarr;</td>
<td>' . $tabCustomer->{'accountingCode.new'} . '</td>';
        }

        if ($tabCustomer->{'invoicesNo'} eq $tabCustomer->{'invoicesNo.new'})
        {
            $html .= '
<td colspan="3">' . $tabCustomer->{'invoicesNo.new'} . '</td>';
        }
        else
        {
            $html .= '
<td style="text-align: right;">' . $tabCustomer->{'invoicesNo'} . '</td>
<td style="text-align: center;">&rarr;</td>
<td>' . $tabCustomer->{'invoicesNo.new'} . '</td>';
        }

        $html .= '
<td>' . $tabCustomer->{'contract.agency.id'} . '</td>
</tr>';
    }

    $html .= '
</table>';
}

print $view->encode({'html' => $html});
