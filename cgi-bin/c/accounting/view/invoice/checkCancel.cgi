use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Json;
use LOC::Locale;
use LOC::Html::Standard;

our %tabViewData;
my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
my $currencySymbol = $tabViewData{'currencySymbol'};

# Vue HTML
my $htmlView = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

# Vue JSON
my $view = LOC::Json->new();
print $view->displayHeader();


# Code HTML à afficher dans la prévisualisation
my $label = '%s';
if ($tabViewData{'type'} eq LOC::Invoice::TYPE_RENT)
{
    if ($tabViewData{'rentType'} eq LOC::Invoice::Rent::INVOICETYPE_MONTH)
    {
        $label = 'Vous avez demandé l\'annulation de la facturation mensuelle de location du %s.';
    }
    if ($tabViewData{'rentType'} eq LOC::Invoice::Rent::INVOICETYPE_INTERMEDIATE)
    {
        $label = 'Vous avez demandé l\'annulation de la facturation intermédiaire de location du %s.';
    }
}
if ($tabViewData{'type'} eq LOC::Invoice::TYPE_REPAIR)
{
    $label = 'Vous avez demandé l\'annulation de la facturation de remise en état du %s.';
}

my $html = '';
$html .= '
<p>' . $locale->t($label, $locale->getDateFormat($tabViewData{'date'}, LOC::Locale::FORMAT_DATE_TEXT)) . '</p>';

my $nbInvoices = values(%{$tabViewData{'tabInvoices'}});
if ($nbInvoices == 0)
{
    $html .= '
<p>' . $locale->t('Il n\'y a aucune facture à annuler.') . '</p>';
}
else
{
    $html .= '
<p>' . $locale->t('Les factures suivantes vont être supprimées') . '&nbsp;:</p>';

    $html .= '
<div id="cancel.report.invoices">';

    $html .= '
<table class="standard" style="width: 100%">
<tr>
<th>' . $locale->t('N°') . '</th>
<th>' . $locale->t('Client') . '</th>
<th style="text-align: right">' . $locale->t('Montant') . '</th>
<th>' . $locale->t('Contrat') . '</th>';
    if ($tabViewData{'type'} eq LOC::Invoice::TYPE_RENT)
    {
        $html .= '
<th colspan="3" title="' . $locale->t('Jours supplémentaires') . '" style="text-align: center">' . $locale->t('Jours supp.') . '</th>
<th colspan="3" title="' . $locale->t('Jours déduits') . '" style="text-align: center">' . $locale->t('Jours déd.') . '</th>
<th colspan="3" title="' . $locale->t('État de facturation du contrat') . '" style="text-align: center">' . $locale->t('État factu.') . '</th>
<th colspan="3" title="' . $locale->t('Date de facturation') . '" style="text-align: center">' . $locale->t('Date fact.') . '</th>
<th title="' . $locale->t('Jours de remise en état et services de remise en état, de location et de transport') . '">' . $locale->t('Jrs/Serv.') . '</th>';
    }
    $html .= '
</tr>';

    my $i = 0;
    foreach my $tabInvoice (values(%{$tabViewData{'tabInvoices'}}))
    {
        # Nom du client
        my $customerName = $tabInvoice->{'customer.name'};
        if (length($customerName) > 20)
        {
            $customerName = substr($customerName, 0, 20) . '...';
        }

        # Liens vers la fiche client et le contrat
        my $customerUrl = &LOC::Globals::get('locationUrl') . '/cgi-bin/old/location/infoclient.cgi?value=' .
                                                              $tabInvoice->{'customer.id'};
        my $contractUrl = &LOC::Request::createRequestUri('rent:rentContract:view',
                                                          {'contractId' => $tabInvoice->{'contract.id'}});

        # Date jusqu'à laquelle la location a été facturée
        my $date = '';
        if ($tabInvoice->{'contract.lastInvoiceDate'} ne '')
        {
            $date = $locale->getDateFormat($tabInvoice->{'contract.lastInvoiceDate'},
                                           LOC::Locale::FORMAT_DATE_NUMERIC);
        }
        my $previousDate = '';
        if ($tabInvoice->{'contract.previousLastInvoiceDate'} ne '')
        {
            $previousDate = $locale->getDateFormat($tabInvoice->{'contract.previousLastInvoiceDate'},
                                                   LOC::Locale::FORMAT_DATE_NUMERIC);
        }

        # Services
        my @tabServices = ();
        if ($tabInvoice->{'contract.repairDaysNo'} != 0)
        {
            my $amount = $locale->getCurrencyFormat($tabInvoice->{'contract.repairDaysAmount'}, undef, $currencySymbol);
            push(@tabServices, $locale->t('%d jrs rem. état (%s)', $tabInvoice->{'contract.repairDaysNo'}, $amount));
        }
        if ($tabInvoice->{'contract.oldServicesNo'} != 0)
        {
            my $amount = $locale->getCurrencyFormat($tabInvoice->{'contract.oldServicesAmount'}, undef, $currencySymbol);
            push(@tabServices, $locale->t('%d serv. cpl. (%s)', $tabInvoice->{'contract.oldServicesNo'}, $amount));
        }
        if ($tabInvoice->{'contract.rentServicesNo'} != 0)
        {
            my $amount = $locale->getCurrencyFormat($tabInvoice->{'contract.rentServicesAmount'}, undef, $currencySymbol);
            push(@tabServices, $locale->t('%d serv. loc./tsp. (%s)', $tabInvoice->{'contract.rentServicesNo'}, $amount));
        }
        if ($tabInvoice->{'contract.repairServicesNo'} != 0)
        {
            my $amount = $locale->getCurrencyFormat($tabInvoice->{'contract.repairServicesAmount'}, undef, $currencySymbol);
            push(@tabServices, $locale->t('%d serv. rem. état (%s)', $tabInvoice->{'contract.repairServicesNo'}, $amount));
        }

        $html .= '
<tr>
<td>' . $tabInvoice->{'number'} . '</td>
<td><a href="' . $customerUrl . '" target="_blank">' . $customerName . '</a></td>
<td style="text-align: right">' . $locale->getCurrencyFormat($tabInvoice->{'amount'}, undef, $currencySymbol) . '</td>
<td><a href="' . $contractUrl . '" target="_blank">' . $tabInvoice->{'contract.code'} . '</a></td>';
        if ($tabViewData{'type'} eq LOC::Invoice::TYPE_RENT)
        {
            if ($tabInvoice->{'contract.additionalDaysUnbilled'} eq $tabInvoice->{'contract.additionalDaysUnbilled'} + $tabInvoice->{'additionalDaysBilled'})
            {
                $html .= '
<td colspan="3"></td>';
            }
            else
            {
                $html .= '
<td style="text-align: right">' . $locale->getNumberFormat($tabInvoice->{'contract.additionalDaysUnbilled'}) . '</td>
<td style="text-align: center">&rarr;</td>
<td>' . $locale->getNumberFormat($tabInvoice->{'contract.additionalDaysUnbilled'} + $tabInvoice->{'additionalDaysBilled'}) . '</td>';
            }
            if ($tabInvoice->{'contract.deductedDaysUnbilled'} eq $tabInvoice->{'contract.deductedDaysUnbilled'} + $tabInvoice->{'deductedDaysBilled'})
            {
                $html .= '
<td colspan="3"></td>';
            }
            else
            {
                $html .= '
<td style="text-align: right">' . $locale->getNumberFormat($tabInvoice->{'contract.deductedDaysUnbilled'}) . '</td>
<td style="text-align: center">&rarr;</td>
<td>' . $locale->getNumberFormat($tabInvoice->{'contract.deductedDaysUnbilled'} + $tabInvoice->{'deductedDaysBilled'}) . '</td>';
            }
            if ($tabInvoice->{'contract.invoiceState'} eq $tabInvoice->{'contract.previousInvoiceState'})
            {
                $html .= '
<td colspan="3"></td>';
            }
            else
            {
                $html .= '
<td style="text-align: right">' . $tabInvoice->{'contract.invoiceState'} . '</td>
<td style="text-align: center">&rarr;</td>
<td>' . $tabInvoice->{'contract.previousInvoiceState'} . '</td>';
            }
            if ($tabInvoice->{'contract.lastInvoiceDate'} eq $tabInvoice->{'contract.previousLastInvoiceDate'})
            {
                $html .= '
<td colspan="3"></td>';
            }
            else
            {
                $html .= '
<td style="text-align: right">' . $date . '</td>
<td style="text-align: center">&rarr;</td>
<td>' . $previousDate . '</td>';
            }
            $html .= '
<td>' . join('<br />', @tabServices) . '</td>';
        }
        $html .= '
</tr>';

        $i++;
    }

    $html .= '
</table>';

    $html .= '
</div>';
}

# Boutons
my @tabLeftBtns  = ();
if ($nbInvoices > 0)
{
    my $url = $htmlView->createURL('accounting:invoice:cancel',
                               {'countryId'   => $tabViewData{'country.id'},
                                'date'        => $tabViewData{'date'},
                                'rentType'    => $tabViewData{'rentType'},
                                'type'        => $tabViewData{'type'}});
    my $action = 'cancel("' . $htmlView->toJSEntities($url) . '")';
    push(@tabLeftBtns, '<span id="cancel.invoiceBtn">' .
                       $htmlView->displayTextButton($locale->t('Poursuivre'), 'valid(|Over).png', $action,
                                                    $locale->t('Poursuivre la procédure d\'annulation de la facturation'),
                                                    undef, {'id' => 'cancelBtn'}) .
                       '</span>' .
                       '<span id="cancel.loading" class="waiting hidden">&nbsp;</span>');
}
my @tabRightBtns = ('<span id="cancel.cancelBtn">' . $htmlView->displayTextButton($locale->t('Annuler') . '</span>',
                                                 'cancel(|Over).png',
                                                 'Location.modalWindowManager.hide()',
                                                 $locale->t('Annuler la procédure d\'annulation de la facturation')));
$html .= $htmlView->displayControlPanel({'left' => \@tabLeftBtns, 'right' => \@tabRightBtns, 'substyle' => 'bottom'});

# Envoi du code HTML
$tabViewData{'tabResult'}->{'html'} = $html;

print $view->encode($tabViewData{'tabResult'});
