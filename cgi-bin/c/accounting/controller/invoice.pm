use utf8;
use open (':encoding(UTF-8)');

# package: accounting
# Contrôleur d'affichage des factures
package invoice;

use strict;
use File::Basename;

use LOC::Agency;
use LOC::Country;
use LOC::Date;
use LOC::Invoice::Rent;
use LOC::Invoice::Repair;
use LOC::Session;
use LOC::Characteristic;
use LOC::Globals;


my $viewDirectory = &dirname(__FILE__);
$viewDirectory =~ s/(.*)\/.+/$1/;
$viewDirectory .= '/view/invoice/';



# Function: viewAction
# Interface de visualisation d'une facture
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId  = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $type       = &LOC::Request::getString('type', LOC::Invoice::TYPE_RENT);
    my $invoiceId  = &LOC::Request::getInteger('invoiceId');
    
    # Variables pour la vue
    our %tabViewData = (
                        'locale.id'      => $tabUserInfos->{'locale.id'},
                        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
                        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
                        'allowCopy'      => (&LOC::Globals::getEnv() =~ /dev/),
                       );
                       
    $tabViewData{'tabVATRates'}     = &LOC::Country::getVATRatesList($countryId, $tabViewData{'tabInvoiceInfos'}->{'date'});
    
    if ($type eq LOC::Invoice::TYPE_RENT)
    {
        $tabViewData{'tabInvoiceInfos'} = &LOC::Invoice::Rent::getInfos($countryId, $invoiceId);
    }
    elsif ($type eq LOC::Invoice::TYPE_OLDREPAIR)
    {
        $tabViewData{'tabInvoiceInfos'} = &LOC::Invoice::Repair::getInfos($countryId, $invoiceId,
                                                                          LOC::Invoice::TYPE_OLDREPAIR);
    }
    elsif ($type eq LOC::Invoice::TYPE_REPAIR)
    {
        $tabViewData{'tabInvoiceInfos'} = &LOC::Invoice::Repair::getInfos($countryId, $invoiceId,
                                                                          LOC::Invoice::TYPE_REPAIR);
    }


    # Affichage de la vue
    require $viewDirectory . 'view.cgi';
}


# Function: manageAction
# Interface de gestion de la facturation
sub manageAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId       = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $tabCountryInfos = &LOC::Country::getInfos($countryId);
    my %hashDate        = &LOC::Date::getHashDate();
    # Calcul de la date du moins précédent
    my @tabMonths = (1..9, 'O', 'N', 'D');
    my $year = $hashDate{'y'};
    my $month = $hashDate{'m'} - 1;
    if ($month == 0)
    {
        $month = 12;
        $year -= 1;
    }
    my $prefixSubRentNumber = sprintf('R%02d%s', $year % 100, $tabMonths[$month - 1]);


    # Variables pour la vue
    our %tabViewData = (
                        'country.id'          => $countryId,
                        'locale.id'           => $tabUserInfos->{'locale.id'},
                        'user.agency.id'      => $tabUserInfos->{'nomadAgency.id'},
                        'currencySymbol'      => $tabCountryInfos->{'currency.symbol'},
                        'isAdmin'             => $tabUserInfos->{'isAdmin'},
                        'today'               => &LOC::Date::getMySQLDate(),
                        'isSubRent'           => &LOC::Characteristic::getCountryValueByCode('GENSOUSLOC', $countryId),
                        'prefixSubRentNumber' => $prefixSubRentNumber
                       );

    # Liste des pays
    $tabViewData{'tabCountries'} = &LOC::Country::getList(LOC::Util::GETLIST_PAIRS);

    # Location
    $tabViewData{'tabNextRentInvoice'} = &LOC::Invoice::Rent::getNextInvoicingDate($countryId);

    $tabViewData{'nextRentalPeriodEndDate'} =
        &LOC::Invoice::Rent::getRentalPeriodEndDate($tabViewData{'tabNextRentInvoice'}->{'date'},
                                                    $tabViewData{'tabNextRentInvoice'}->{'rentType'});

    # Remise en état
    $tabViewData{'nextRepairInvoice'} = &LOC::Invoice::Repair::getNextInvoicingDate($countryId);

    # Dernier numéro de pièce
    # Location
    $tabViewData{'rentLastDocumentNo'} =
        &LOC::Invoice::getLastDocumentNo($countryId, LOC::Invoice::TYPE_RENT,
                                         $tabViewData{'tabNextRentInvoice'}->{'rentType'},
                                         $tabViewData{'tabNextRentInvoice'}->{'date'});
    # Remise en état
    $tabViewData{'repairLastDocumentNo'} =
        &LOC::Invoice::getLastDocumentNo($countryId, LOC::Invoice::TYPE_REPAIR, undef,
                                         $tabViewData{'nextRepairInvoice'}->{'date'});

    # Recherche d'une facturation de location en cours d'exécution
    my $tabRentRequestParams = {'parameters' => {'countryId' => $countryId,
                                                 'type'      => LOC::Invoice::TYPE_RENT,
                                                 'rentType'  => $tabViewData{'tabNextRentInvoice'}->{'rentType'},
                                                 'date'      => $tabViewData{'tabNextRentInvoice'}->{'date'}
                                                },
                                'stateId'    => [LOC::InvoicingRequest::STATE_CREATED,
                                                 LOC::InvoicingRequest::STATE_PROGRESS
                                                ]
                               };
    my $tabRentRequest = &LOC::InvoicingRequest::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabRentRequestParams);
    if (keys(%$tabRentRequest) > 0)
    {
        $tabViewData{'rentRequestId'} = (keys(%$tabRentRequest))[0];
        my $tabParams = &LOC::Json::fromJson($tabRentRequest->{$tabViewData{'rentRequestId'}}->{'parameters'});
        $tabViewData{'rentNumber'}    = $tabParams->{'no'};
    }

    # Recherche d'une facturation de remise en état en cours d'exécution
    my $tabRepairRequestParams = {'parameters' => {'countryId' => $countryId,
                                                   'type'      => LOC::Invoice::TYPE_REPAIR,
                                                   'date'      => $tabViewData{'tabNextRentInvoice'}->{'date'}
                                                  },
                                  'stateId'    => [LOC::InvoicingRequest::STATE_CREATED,
                                                   LOC::InvoicingRequest::STATE_PROGRESS
                                                  ]
                                 };
    my $tabRepairRequest = &LOC::InvoicingRequest::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                           $tabRepairRequestParams);
    if (keys(%$tabRepairRequest) > 0)
    {
        $tabViewData{'repairRequestId'} = (keys(%$tabRepairRequest))[0];
        my $tabParams = &LOC::Json::fromJson($tabRepairRequest->{$tabViewData{'repairRequestId'}}->{'parameters'});
        $tabViewData{'repairNumber'}    = $tabParams->{'no'};
    }

    # Affichage de la vue
    require $viewDirectory . 'manage.cgi';
}


# Function: checkContractsAction
# Contrôle des contrats
sub checkContractsAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId   = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $date        = &LOC::Request::getString('date');
    my $rentType    = &LOC::Request::getString('rentType');
    my $id          = &LOC::Request::getString('id');
    my $displayType = lc(&LOC::Request::getString('displayType', 'html'));

    my $tabCheckContracts = &LOC::Invoice::Rent::checkContracts($countryId, $date, LOC::Invoice::Rent::GETINFOS_ALL,
                                                                $rentType);

    # Trace du lancement du contrôle clients
    &LOC::Log::writeDBLog('FACTURATION', $id, LOC::Log::EventType::CHECKCONTRACTS, {'content' => ''});

    # Variables pour la vue
    our %tabViewData = (
                        'country.id'     => $countryId,
                        'locale.id'      => $tabUserInfos->{'locale.id'},
                        'checkContracts' => $tabCheckContracts,
                        'date'           => $date,
                        'type'           => $rentType,
                       );

    # Affichage de la vue
    if ($displayType eq 'xls')
    {
        require $viewDirectory . 'manage/checkContractsExcel.cgi';
    }
    else
    {
        require $viewDirectory . 'manage/checkContracts.cgi';
    }
}


# Function: uploadAction
# Envoi du fichier clients sur le serveur
sub uploadAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId     = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $file          = &LOC::Request::getFile('file');
    my $id            = &LOC::Request::getString('id');
    my $date          = &LOC::Request::getString('date');
    my $type          = &LOC::Request::getString('type');
    my $rentType      = &LOC::Request::getString('rentType');
    
    my $isFileImported = 0;
    my $filePath = '';
    if (defined $file->{'tmp_name'} && $file->{'name'})
    {
        my $maxFileSize = &LOC::Characteristic::getGlobalValueByCode(&LOC::Characteristic::CUST_MAX_FILE_SIZE);
        if ($file->{'name'} =~ /\.txt$/ && $file->{'size'} < $maxFileSize * 1024 * 1024)
        {
            $filePath = &LOC::Util::getCommonFilesPath('temp/') . $file->{'name'};
            my $mvCommand = sprintf('mv "%s" "%s"', $file->{'tmp_name'}, $filePath);
            if (!system($mvCommand))
            {
                $isFileImported = 1;
            }
        }
    }

    # Variables pour la vue
    our %tabViewData = (
                        'countryId'     => $countryId,
                        'locale.id'      => $tabUserInfos->{'locale.id'},
                        'isFileImported' => $isFileImported,
                        'filePath'       => $filePath,
                        'date'           => $date,
                        'type'           => $type,
                        'rentType'       => $rentType,
                        'id'             => $id,
                       );

    # Trace du lancement du contrôle clients
    if ($isFileImported)
    {
        &LOC::Log::writeDBLog('FACTURATION', $id, LOC::Log::EventType::CHECKCUSTOMERS, {'content' => ''});
    }

    # Affichage de la vue
    require $viewDirectory . 'manage/upload.cgi';
}

# Function: uploadAction
# Envoi du fichier clients sur le serveur
sub uploadSubRentAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId     = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $file          = &LOC::Request::getFile('file');
    my $nopiece       = &LOC::Request::getString('nopiece');

    my $isFileImported = 0;
    my $filePath = '';
    if (defined $file->{'tmp_name'} && $file->{'name'})
    {
        if ($file->{'name'} =~ /\.csv/ && $file->{'size'} < 20 * 1024 * 1024)
        {
            $filePath = &LOC::Util::getCommonFilesPath('temp/') . $file->{'name'};
            my $mvCommand = sprintf('mv "%s" "%s"', $file->{'tmp_name'}, $filePath);
            if (!system($mvCommand))
            {
                $isFileImported = 1;
            }
        }
    }

    # Variables pour la vue
    our %tabViewData = (
                        'countryId'      => $countryId,
                        'locale.id'      => $tabUserInfos->{'locale.id'},
                        'isFileImported' => $isFileImported,
                        'filePath'       => $filePath,
                        'nopiece'        => $nopiece
                       );

    # Affichage de la vue
    require $viewDirectory . 'manage/uploadSubRent.cgi';
}


# Function: readFileAction
# Lecture du fichier clients
#
sub readFileAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = ('locale.id' => $tabUserInfos->{'locale.id'});

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Récupération des paramètres
    my $countryId     = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $date          = &LOC::Request::getString('date');
    my $filePath      = &LOC::Request::getString('filePath');
    my $type          = &LOC::Request::getString('type');
    my $rentType      = &LOC::Request::getString('rentType');

    # URL de la mise à jour des clients
    my $updateCustomersUrl = &LOC::Request::createRequestUri('accounting:invoice:updateCustomers');
    
    # Récupération de la liste des modèles de machine
    my $tabResult = &LOC::Invoice::Rent::checkCustomers($countryId, $date, $type, $rentType, $filePath);
    if ($tabResult == 0)
    {
        $tabViewData{'tabResult'} = 0;
    }
    else
    {
        $tabViewData{'tabResult'} = {'updateCustomersUrl' => $updateCustomersUrl,
                                     'customersToCreate'  => $tabResult->{'customersToCreate'},
                                     'customersToUpdate'  => $tabResult->{'customersToUpdate'}};
    }

    my $rmCommand = sprintf('rm "%s"', $filePath);
    system($rmCommand);

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/invoice/manage/readFile.cgi';
}

sub readFileSubRentAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = ('locale.id' => $tabUserInfos->{'locale.id'});

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Récupération des paramètres
    my $countryId     = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $filePath      = &LOC::Request::getString('filePath');
    my $nopiece       = &LOC::Request::getString('nopiece') * 1;
    
    my $tabReadFile = &LOC::Invoice::Rent::readSubRentFile($filePath, $nopiece, $countryId);
    if ($tabReadFile == 0)
    {
        $tabViewData{'returnSubRent'} = 0;
    }
    else
    {
        $tabViewData{'returnSubRent'} = &LOC::Invoice::Rent::generateSageFile($tabReadFile, $countryId);
    }

    my $rmCommand = sprintf('rm "%s"', $filePath);
    system($rmCommand);

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/invoice/manage/readFileSubRent.cgi';
}


# Function: updateCustomersAction
# Mise à jour des clients
#
sub updateCustomersAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = ('locale.id' => $tabUserInfos->{'locale.id'});

    # Récupération des paramètres
    my $countryId            = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $tabCustomersToUpdate = &LOC::Request::getString('customersToUpdate');

    # Récupération de la liste des modèles de machine
    $tabViewData{'tabResult'} = &LOC::Invoice::Rent::updateCustomers($countryId, $tabCustomersToUpdate);

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/invoice/manage/updateCustomers.cgi';
}


# Function: exportAction
# Export du fichier des factures
#
sub exportAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Récupération des paramètres
    my $countryId = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $type      = &LOC::Request::getString('type');
    my $rentType  = &LOC::Request::getString('rentType');
    my $date      = &LOC::Request::getDate('date');
    my $id        = &LOC::Request::getString('id');

    # Variables pour la vue
    our %tabViewData = (
                        'id'      => $id,
                        'charset' => LOC::Invoice::EXPORTFILE_ENCODING,
                       );

    # Récupération de la liste des modèles de machine
    $tabViewData{'content'} = &LOC::Invoice::getExportFileContent($countryId, $type, $rentType, $date);

    # Trace de l'export
    if ($tabViewData{'content'} != -1)
    {
        &LOC::Log::writeDBLog('FACTURATION', $id, LOC::Log::EventType::SAGEFILEEXPORT, {'content' => ''});
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/invoice/export.cgi';
}

# Function: exportSageAction
# Export du fichier sage pour la sous-location
#
sub exportSubRentAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Récupération des paramètres
    my $file     = &LOC::Request::getString('file', '', '_GET');
    $file        =~ s/-/\//;

    open(FILE, &LOC::Util::getCommonFilesPath('subRent/') . $file);
    my @tabFile = <FILE>;
    close(FILE);
    my $content = join('', @tabFile);

    # Variables pour la vue
    our %tabViewData = (
                        'charset' => LOC::Invoice::EXPORTFILE_ENCODING,
                        'content' => $content
                       );

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/invoice/exportSubRent.cgi';
}


# Function: cancelAction
# Export du fichier des factures
#
sub checkCancelAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
                        'locale.id'      => $tabUserInfos->{'locale.id'},
                        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
                        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
                        'country.id'     => &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'}),
                        'type'           => &LOC::Request::getString('type'),
                        'rentType'       => &LOC::Request::getString('rentType'),
                        'date'           => &LOC::Request::getDate('date'),
                        'id'             => &LOC::Request::getString('id'),
                       );

    # Récupération de la liste des factures à supprimer
    $tabViewData{'tabInvoices'} = &LOC::Invoice::getInvoicesToCancel($tabViewData{'country.id'}, $tabViewData{'type'},
                                                                     $tabViewData{'rentType'}, $tabViewData{'date'});

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/invoice/checkCancel.cgi';
}


# Function: historyAction
# Historique des facturations
sub historyAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId      = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $currencySymbol = &LOC::Request::getString('currencySymbol');
    # Variables pour la vue
    our %tabViewData = (
                        'country.id'     => $countryId,
                        'currencySymbol' => $currencySymbol,
                        'locale.id'      => $tabUserInfos->{'locale.id'},
                        'isAdmin'        => $tabUserInfos->{'isAdmin'},
                       );

    # Historique
    my $flags = LOC::Invoice::GETLASTEST_BASE;
    # Logs pour les administrateurs
    if ($tabViewData{'isAdmin'})
    {
        $flags = LOC::Invoice::GETLASTEST_ALL;
    }
    $tabViewData{'tabLatestInvoices'} = &LOC::Invoice::getLastest($countryId, $tabUserInfos->{'locale.id'},
                                                                  undef, $flags);

    # Afficher les avoirs
    $tabViewData{'displayAssets'} = &LOC::Characteristic::getCountryValueByCode('AVOIRS', $countryId);

    # Affichage de la vue
    require $viewDirectory . 'history.cgi';
}


# Function: invoiceAction
# Lancement de la facturation
sub invoiceAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $date      = &LOC::Request::getDate('date');
    my $rentType  = &LOC::Request::getString('rentType');
    my $type      = &LOC::Request::getString('type');
    my $no        = &LOC::Request::getInteger('no');

    my $invoicingId = &LOC::Invoice::getId($countryId, $date, $type);

    # Variables pour la vue
    our %tabViewData;

    # Lancement de la facturation
    $tabViewData{'requestId'} = &LOC::Invoice::launch($countryId, $tabUserInfos, $type, $rentType, $date, $no);

    # Affichage de la vue
    require $viewDirectory . 'invoice.cgi';
}


# Function: checkProgressionAction
# Vérification de la progression d'une facturation
sub checkProgressionAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $requestId = &LOC::Request::getInteger('requestId');

    # Variables pour la vue
    our %tabViewData;

    # Récupération des informations de la requête
    my $tabRequestInfos = &LOC::InvoicingRequest::getInfos($requestId, $countryId);
    $tabViewData{'progression'}         = $tabRequestInfos->{'progression'};
    $tabViewData{'stateId'}             = $tabRequestInfos->{'state.id'};
    $tabViewData{'generatedInvoicesNo'} = $tabRequestInfos->{'generatedInvoicesNo'};
    $tabViewData{'tabResult'}           = $tabRequestInfos->{'tabResult'};

    # Affichage de la vue
    require $viewDirectory . 'checkProgression.cgi';
}


# Function: cancelAction
# Annulation de la facturation
sub cancelAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $date      = &LOC::Request::getDate('date');
    my $rentType  = &LOC::Request::getString('rentType');
    my $type      = &LOC::Request::getString('type');
    my $no        = &LOC::Request::getInteger('no');

    my $invoicingId = &LOC::Invoice::getId($countryId, $date, $type);

    # Variables pour la vue
    our %tabViewData;

    if ($tabUserInfos->{'isAdmin'})
    {
        # Annulation
        $tabViewData{'canceledInvoicesNo'} = &LOC::Invoice::cancelInvoicing($countryId, $type, $rentType, $date);
        &LOC::Log::writeDBLog('FACTURATION', $invoicingId, LOC::Log::EventType::CANCELINVOICING, {'content' => ''});
    }
    else
    {
        $tabViewData{'canceledInvoicesNo'} = -1;
    }

    # Affichage de la vue
    require $viewDirectory . 'invoice.cgi';
}

1;
