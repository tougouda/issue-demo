use utf8;
use open (':encoding(UTF-8)');

# package: accounting
# Contrôleur d'affichage des contrôles pré-facturation
package controls;

use strict;
use Date::Calc;
use File::Basename;

use LOC::Agency;
use LOC::Contract::Rent;
use LOC::Country;
use LOC::Invoice;
use LOC::Invoice::Rent;
use LOC::Session;


my $viewDirectory = &dirname(__FILE__);
$viewDirectory =~ s/(.*)\/.+/$1/;
$viewDirectory .= '/view/controls/';



# Function: viewAction
# Interface de visualisation d'une facture
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId = $tabUserInfos->{'nomadCountry.id'};
    my $date  = &LOC::Request::getString('date');

    # Récupération du type de facturation
    my $db = &LOC::Db::getConnection('auth');

    my $query = '
SELECT ivd_type AS `type`
FROM p_invoicedate
WHERE ivd_cty_id = ' . $db->quote($countryId) . '
    AND ivd_date = ' . $db->quote($date);

    my $type = $db->fetchOne($query);

    # Liste des agences
    my $tabAgency = &LOC::Agency::getList(LOC::Util::GETLIST_PAIRS,
                                          {'country' => $countryId, 'type' => LOC::Agency::TYPE_AGENCY});
    my $tabUserAuthorizedAgency = $tabUserInfos->{'tabAuthorizedAgencies'};
    my $tabAuthorizedAgency = [];

    foreach my $agency (keys(%$tabUserAuthorizedAgency))
    {
        if (grep(/^$agency$/, keys(%$tabAgency)))
        {
            push(@$tabAuthorizedAgency, $agency);
        }
    }

    # Liste des contrôles
    my @tabControlsKey = ();
    if ($type eq LOC::Invoice::Rent::INVOICETYPE_MONTH)
    {
        @tabControlsKey = ('contractsToStop', 'fuelsToType');
    }
    elsif ($type eq LOC::Invoice::Rent::INVOICETYPE_INTERMEDIATE)
    {
        @tabControlsKey = ('contractsToInvoice');
    }

    # Date de fin de la période de facturation
    my $rentalPeriodEndDate = &LOC::Invoice::Rent::getRentalPeriodEndDate($date, $type);

    # Variables pour la vue
    our %tabViewData = (
                        'country.id'          => $countryId,
                        'locale.id'           => $tabUserInfos->{'locale.id'},
                        'user.agency.id'      => $tabUserInfos->{'nomadAgency.id'},
                        'currencySymbol'      => $tabUserInfos->{'currency.symbol'},
                        'date'                => $date,
                        'rentalPeriodEndDate' => $rentalPeriodEndDate,
                        'type'                => $type,
                        'tabAgency'           => $tabAuthorizedAgency,
                        'tabControlsKey'      => \@tabControlsKey,
                       );
                       


    # Affichage de la vue
    require $viewDirectory . 'view.cgi';
}

# Function: jsonControlsAction
# Récupérer les contrôles pré-facturation
#
sub jsonAction
{
    # Variables pour la vue
    our %tabViewData = ();

    # Récupération des paramètres
    my $countryId = &LOC::Request::getString('countryId');
    my $date      = &LOC::Request::getString('date');
    my $type      = &LOC::Request::getString('type');

    my %tabToday = &LOC::Date::getHashDate($date);
    my ($year, $month, $day) = &Date::Calc::Add_Delta_Days($tabToday{'y'}, $tabToday{'m'}, $tabToday{'d'}, -1);
    my $yesterday = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE, ('y' => $year, 'm' => $month, 'd' => $day));

    my $endDate = &LOC::Invoice::Rent::getRentalPeriodEndDate($date, $type);

    # Facturation mensuelle
    if ($type eq LOC::Invoice::Rent::INVOICETYPE_MONTH)
    {
        # Contrats à mettre en arrêt
        $tabViewData{'contractsToStop'} = &LOC::Invoice::Rent::getNoOfContractsToStopByAgency($countryId, $yesterday);

        # Carburants à saisir
        $tabViewData{'fuelsToType'} = &LOC::Invoice::Rent::getNoOfFuelsToTypeByAgency($countryId, $yesterday);
    }
    elsif ($type eq LOC::Invoice::Rent::INVOICETYPE_INTERMEDIATE)
    {
        # Contrats à mettre en facturation
        $tabViewData{'contractsToInvoice'} = &LOC::Invoice::Rent::getNoOfContractsToInvoiceByAgency($countryId, $yesterday);
    }

    # Affichage de la vue
    require $viewDirectory . 'json.cgi';
}

1;
