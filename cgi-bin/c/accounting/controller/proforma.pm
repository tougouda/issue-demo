use utf8;
use open (':encoding(UTF-8)');

# Package: proforma
# Contrôleur pour les alertes et les pdf de la proforma
package proforma;

use strict;
use File::Basename;

use LOC::Country;
use LOC::Customer;
use LOC::State;
use LOC::Contract;
use LOC::Estimate;
use LOC::Proforma;
use LOC::Estimate::Rent;
use LOC::Contract::Rent;
use LOC::MachinesFamily;
use LOC::Characteristic;
use LOC::Json;
use LOC::PaymentMode;
use LOC::Util;
use LOC::Browser;


# Function : _getAlertsList
#
# Parameters:
# string  $countryId - Pays
# string  $agencyId  - Id de l'agence
# int     $readOnly  - Visu commerce (sans modification) 1, visu comptabilité (avec modification) 0
# hashref $tabLineError - Tableau des lignes en erreur
#
# Returns:
# array - Information des pro formas à afficher
sub _getAlertsList
{
    my ($countryId, $agencyId, $readOnly, $tabLineError) = @_;

    my $tabFilters = {'stateId'     => [LOC::Proforma::STATE_ACTIVE],
                      'noReceived'  => 1,
                      'isExpired'   => 0,
                      'closureId'   => undef,
                      'documentStateId' => [LOC::Estimate::Rent::STATE_ACTIVE,
                                            LOC::Estimate::Rent::STATE_CONTRACT,
                                            LOC::Contract::Rent::STATE_PRECONTRACT,
                                            LOC::Contract::Rent::STATE_CONTRACT,
                                            LOC::Contract::Rent::STATE_STOPPED,
                                            LOC::Contract::Rent::STATE_BILLED]
    };

    my @tabLineErrorId = keys(%$tabLineError);

    my $flags = LOC::Proforma::GETINFOS_CUSTOMER | LOC::Proforma::GETINFOS_CONTRACT | LOC::Proforma::GETINFOS_ESTIMATELINE | LOC::Proforma::GETINFOS_INDEX;

    my $tabProformasList = &LOC::Proforma::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters, $flags);
    my $tabProformas = [];
    #pour chaque PF
    foreach my $tabRow (values %$tabProformasList)
    {
        # Si visu commerce ET qu'on ce trouve sur l'agence de l'utilisateur
        # OU visu compta
        if (($readOnly && $agencyId eq $tabRow->{'agency.id'}) || !$readOnly)
        {
            # récupération des valeurs saisies si la ligne est en erreur
            my $isJustified = (&LOC::Util::in_array($tabRow->{'id'}, \@tabLineErrorId) ? $tabLineError->{$tabRow->{'id'}}->{'isJustified'} : $tabRow->{'isJustified'});
            my $isValidated = (&LOC::Util::in_array($tabRow->{'id'}, \@tabLineErrorId) ? $tabLineError->{$tabRow->{'id'}}->{'isValidated'} : $tabRow->{'isValidated'});
            my $validationComment = (&LOC::Util::in_array($tabRow->{'id'}, \@tabLineErrorId) ? $tabLineError->{$tabRow->{'id'}}->{'validComment'} : (defined $tabRow->{'validComment'} ? $tabRow->{'validComment'} : ''));
            my $encashmentType = (&LOC::Util::in_array($tabRow->{'id'}, \@tabLineErrorId) ? $tabLineError->{$tabRow->{'id'}}->{'encashment.id'} : $tabRow->{'encashment.id'});
            my $encashmentDebtAmount = (&LOC::Util::in_array($tabRow->{'id'}, \@tabLineErrorId) ? $tabLineError->{$tabRow->{'id'}}->{'remainDue'} : $tabRow->{'remainDue'});

            # Etats des remises exceptionnelles de location et transport si elles existent
            my @tabReducsList = ();
            my $rentReducStatus = (defined $tabRow->{'contract.id'} ? $tabRow->{'contract.rentTariff.specialReductionStatus'} : $tabRow->{'estimateline.rentTariff.specialReductionStatus'});
            if ($rentReducStatus eq LOC::Tariff::Rent::REDUCTIONSTATE_PENDING)
            {
                push (@tabReducsList, 'rentTariff');
            }
            my $trspReducStatus = (defined $tabRow->{'contract.id'} ? $tabRow->{'contract.transportTariff.specialReductionStatus'} : $tabRow->{'estimateline.transportTariff.specialReductionStatus'});
            if ($trspReducStatus eq LOC::Tariff::Transport::REDUCTIONSTATE_PENDING)
            {
                push (@tabReducsList, 'transportTariff');
            }

            # Informations sur le client
            my $isNewAccount = &LOC::Customer::isNewAccount($countryId,
                                                            $tabRow->{'customer.code'},
                                                            $tabRow->{'customer.accountingCode'});
            my $isCustomerLocked = ($tabRow->{'customer.prospectSource'} eq 'contract' || #créé depuis contrat
                                    $tabRow->{'customer.prospectSource'} eq 'estimate' || #crée depuis devis
                                    $isNewAccount ? 1 : 0); #client avec ouverture de compte


            my $documentState = (defined $tabRow->{'contract.id'} ? $tabRow->{'contract.state.id'} : $tabRow->{'estimateLine.estimate.state.id'});
            my $state = &_getStateInfo($countryId, $documentState);
            my $tabData = {
                'id'                    => $tabRow->{'id'},
                'date'                  => $tabRow->{'creationDate'},
                'type.id'               => $tabRow->{'type.id'},
                'type.label'            => $tabRow->{'type.label'},
                'document.type'         => (defined $tabRow->{'contract.id'} ? 'contract' : 'estimateLine'),
                'document.id'           => (defined $tabRow->{'contract.id'} ? $tabRow->{'contract.id'} : [$tabRow->{'estimateLine.estimate.id'}, $tabRow->{'estimateLine.id'}]),
                'document.code'         => (defined $tabRow->{'contract.id'} ? $tabRow->{'contract.code'} : [$tabRow->{'estimateLine.estimate.code'}, $tabRow->{'estimateLine.index'}]),
                'agency.id'             => $tabRow->{'agency.id'},
                'document.state'        => $state,
                'document.beginDate'    => $tabRow->{'beginDate'},
                'customer.id'           => $tabRow->{'customer.id'},
                'customer.isPFRequired' => $tabRow->{'customer.isProformaRequired'},
                'customer.notVerified'  => $isCustomerLocked,
                'customer.code'         => $tabRow->{'customer.code'},
                'customer.name'         => $tabRow->{'customer.name'},
                'customer.email'        => $tabRow->{'customer.email'},
                'orderContact.email'    => (defined $tabRow->{'contract.id'} ? $tabRow->{'contract.orderContact.email'} : $tabRow->{'estimateLine.estimate.orderContact.email'}),
                'amount'                => $tabRow->{'amount'},
                'amountWithVat'         => $tabRow->{'amountWithVat'},
                'total'                 => $tabRow->{'total'},
                'paymentMode.id'        => $tabRow->{'paymentMode.id'},
                'paymentMode.label'     => $tabRow->{'paymentMode.label'},
                'isJustified'           => $isJustified,
                'isValidated'           => $isValidated,
                'validation.comment'    => $validationComment,
                'encashment.type'       => $encashmentType,
                'encashment.debtAmount' => $encashmentDebtAmount,
                'specialReductionList'  => \@tabReducsList,
                'isPrintable'           => ($tabRow->{'isPrintable'} && @tabReducsList == 0 ? 1 : 0),
                'modificationDate'      => $tabRow->{'modificationDate'}
            };

            if ($tabRow->{'isPrintable'})
            {
                $tabData->{'pdfUrl'}  = &LOC::Request::createRequestUri('accounting:proforma:pdf', {'proformaId' => $tabRow->{'id'}}, 1);
                if (defined $tabRow->{'contract.id'})
                {
                    $tabData->{'pdfName'} = 'AI_' . $tabRow->{'type.label'} . '_' . $tabRow->{'contract.code'} . '_' . $tabRow->{'indexOnContract'} . '.pdf';
                }
                else
                {
                    $tabData->{'pdfName'} = 'AI_' . $tabRow->{'type.label'} . '_' . $tabRow->{'estimateLine.estimate.code'} . '_' . $tabRow->{'estimateLine.index'} . '_' . $tabRow->{'indexOnEstimateLine'} . '.pdf';
                }
                $tabData->{'mailContent'} = &LOC::Characteristic::getAgencyValueByCode('MAILPROFORMA', $tabRow->{'agency.id'});
            }

            push(@$tabProformas, $tabData);
        }
    }
    return $tabProformas;
}


# Function : _getStateInfo
#
# Parameters:
# string  $countryId - Pays
# string  $id        - Id de l'etat
#
# Returns:
# string - Label de l'etat
sub _getStateInfo
{
    my ($countryId, $id) = @_;
    my $stateInfo = &LOC::State::getInfos($id, $countryId);
    return $stateInfo->{'label'};
}

# Function : _getClosuresList
#
# Parameters:
# string $countryId - Pays
#
# Returns:
# hashref - Tableau des états de clôture
sub _getClosuresList
{
    my ($countryId) = @_;
    my %closureList = &LOC::State::getList($countryId, LOC::Util::GETLIST_PAIRS, {'module' => 'CLOSURE'});

    my %tabClosure = ('0' => '----', %closureList);
    my @tabKeyClosure = sort (keys %tabClosure);
    my @tabClosureFinal;
    # Réordonne la liste selon leur identifiant
    foreach my $keyClosure (@tabKeyClosure)
    {
        my $valueClosure = '';
        if ($keyClosure eq '0')
        {
            $keyClosure = '';
            $valueClosure = '----';
        }
        else
        {
            $valueClosure = $closureList{$keyClosure};
        }
        push (@tabClosureFinal, {'innerHTML' => $valueClosure, 'value' => $keyClosure});
    }
    return \@tabClosureFinal;
}

# Function : _getEncashmentsList
#
# Parameters:
# string $countryId - Pays
#
# Returns:
# hashref - Tableau des états de clôture
sub _getEncashmentsList
{
    my ($countryId) = @_;
    my $encashmentList = &LOC::State::getList($countryId, LOC::Util::GETLIST_PAIRS, {'module' => 'ENCASHMENT'});
    return $encashmentList;
}


# Function : viewAlertsAction
# Alerte des proforma pour la compta et le commerce
sub viewAlertsAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId      = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $agencyId       = &LOC::Request::getString('agencyId', $tabUserInfos->{'nomadAgency.id'});
    my $userId         = &LOC::Request::getInteger('userId', $tabUserInfos->{'id'});
    my $displayType    = &LOC::Request::getString('displayType', '');
    my $readOnly       = 0;

    my $tabRights = &LOC::Proforma::getAlertRights($countryId, $tabUserInfos->{'id'});

    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    my $tabLineError = {};

    if ($tabRights->{'accounting'} =~ /^(|.*\|)ok(|\|.*)$/)
    {
        my $tabProformas = &LOC::Request::getHash('pf', {});

        #Récupération de la valdiation
        $tabViewData{'valid'}         = &LOC::Request::getString('valid', '');
            #Mise à jour
        if ($tabViewData{'valid'} eq 'ok')
        {
            foreach my $proformaId (keys %$tabProformas)
            {
                #Update de la proforma
                my $tabErrors = {};
                if ($tabProformas->{$proformaId}->{'toUpdate'})
                {
                    if (!&LOC::Proforma::validate($countryId, $proformaId, $tabProformas->{$proformaId}, $userId, $tabErrors, undef))
                    {
                        # S'il y avait des mises à jour à faire
                        $tabViewData{'messages'}->{'error'} = 1;
                        if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
                        {
                             $tabViewData{'messagesLine'}->{$proformaId} = {'error' => 'fatalError'};
                        }
                        elsif (&LOC::Util::in_array(0x0020, $tabErrors->{'fatal'}))
                        {
                             $tabViewData{'messagesLine'}->{$proformaId} = {'error' => 'remainDueError'};
                        }
                        elsif (&LOC::Util::in_array(0x0021, $tabErrors->{'fatal'}))
                        {
                             $tabViewData{'messagesLine'}->{$proformaId} = {'error' => 'unFinalizeError'};
                        }
                        elsif (&LOC::Util::in_array(0x0022, $tabErrors->{'fatal'}))
                        {
                             $tabViewData{'messagesLine'}->{$proformaId} = {'error' => 'modifError'};
                        }
                        elsif (&LOC::Util::in_array(0x0023, $tabErrors->{'fatal'}))
                        {
                             $tabViewData{'messagesLine'}->{$proformaId} = {'error' => 'validAgencyError'};
                        }
                        # si on a des erreurs lors de la mise à jour du contrat dans le cas d'une prolongation
                        elsif (&LOC::Util::in_array(0x0100, $tabErrors->{'fatal'}))
                        {
                            my $tabContractErrors = $tabErrors->{'modules'}->{'contract'};
                            if (&LOC::Util::in_array(0x0106, $tabContractErrors->{'fatal'}) || &LOC::Util::in_array(0x0112, $tabContractErrors->{'fatal'}) ||
                                &LOC::Util::in_array(0x0118, $tabContractErrors->{'fatal'}) || &LOC::Util::in_array(0x0120, $tabContractErrors->{'fatal'}) ||
                                &LOC::Util::in_array(0x0122, $tabContractErrors->{'fatal'})
                                )
                            {
                                $tabViewData{'messagesLine'}->{$proformaId} = {'error' => 'prolongationError'};
                            }
                        }

                        $tabViewData{'infoLine'}->{$proformaId} = 'isError';
                        $tabLineError->{$proformaId} = $tabProformas->{$proformaId};
                    }
                    else
                    {
                        $tabViewData{'infoLine'}->{$proformaId} = 'isUpdated';
                    }
                }

            }
            if (!defined $tabViewData{'messages'}->{'error'})
            {
                $tabViewData{'messages'}->{'valid'} = 'updated';
            }

        }

        #Recuperation des proforma active non finalisée
        $tabViewData{'tabProformas'} = &_getAlertsList($countryId, $agencyId, $readOnly, $tabLineError);
    }
    elsif ($tabRights->{'business'} =~ /^(|.*\|)ok(|\|.*)$/)
    {
        $readOnly = 1;
        #Recuperation des proforma active non finalisée
        $tabViewData{'tabProformas'} = &_getAlertsList($countryId, $agencyId, $readOnly, $tabLineError);
    }
    else
    {
        my $directory = dirname(__FILE__);
        $directory =~ s/(.*)\/.+/$1/;
        require $directory . '/../../c/unauthorizedArea.cgi';
        exit;
    }

    $tabViewData{'readOnly'} = $readOnly;
     #Recuperation des infos du document (ligne de devis | contrat)
    # URL's sur les vieux scripts
    $tabViewData{'customerUrl'} = &LOC::Customer::getUrl('<%id>');

    # Liste des états de clôture
    $tabViewData{'tabClosureTypes'} = &_getClosuresList($countryId);
    # Liste des états d'encaissement
    $tabViewData{'tabEncashmentTypes'} = &_getEncashmentsList($countryId);

    # Temps de réactualisation des pro formas
    $tabViewData{'refreshTime'} = &LOC::Characteristic::getCountryValueByCode('DELAIPROFORMA', $countryId) * 1;



    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    if ($displayType eq 'xls')
    {
        require $directory . '/view/proforma/viewAlertsExcel.cgi';
    }
    else
    {
        require $directory . '/view/proforma/viewAlerts.cgi';
    }
}

sub viewAlertsReadOnlyAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId      = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $agencyId       = &LOC::Request::getString('agencyId', $tabUserInfos->{'nomadAgency.id'});
    my $userId         = &LOC::Request::getInteger('userId', $tabUserInfos->{'id'});
    my $displayType    = &LOC::Request::getString('displayType', '');
    my $readOnly       = 1;


    my $tabRights = &LOC::Proforma::getAlertRights($countryId, $tabUserInfos->{'id'});

    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    my $tabLineError = {};

    if ($tabRights->{'business'} =~ /^(|.*\|)ok(|\|.*)$/)
    {
        #Recupereation des proforma active non finalisée
        $tabViewData{'tabProformas'} = &_getAlertsList($countryId, $agencyId, $readOnly, $tabLineError);
    }
    else
    {
        my $directory = dirname(__FILE__);
        $directory =~ s/(.*)\/.+/$1/;
        require $directory . '/../../c/unauthorizedArea.cgi';
        exit;
    }

    $tabViewData{'readOnly'} = $readOnly;
     #Recuperation des infos du document (ligne de devis | contrat)
    # URL's sur les vieux scripts
    $tabViewData{'customerUrl'} = &LOC::Customer::getUrl('<%id>');

    # Liste des états de clôture
    $tabViewData{'tabClosureTypes'} = &_getClosuresList($countryId);
    # Liste des états d'encaissement
    $tabViewData{'tabEncashmentTypes'} = &_getEncashmentsList($countryId);

    # Temps de réactualisation des pro formas
    $tabViewData{'refreshTime'} = &LOC::Characteristic::getCountryValueByCode('DELAIPROFORMA', $countryId) * 1;


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    if ($displayType eq 'xls')
    {
        require $directory . '/view/proforma/viewAlertsExcel.cgi';
    }
    else
    {
        require $directory . '/view/proforma/viewAlerts.cgi';
    }
}

# Function: pdfAction
# Génération du PDF d'un document pro forma ou facture d'acompte
sub pdfAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $countryId    = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $proformaId   = &LOC::Request::getInteger('proformaId', 0);
    our $tabViewData = &_getPdfInfos($countryId, $proformaId);
    if (!$tabViewData)
    {
        # Erreur 400: Bad Request
        &LOC::Browser::sendHeaders("Status: 400\nContent-Type: text/plain; charset=UTF-8\n\n");
        return;
    }

    $tabViewData->{'locale.id'}      = $tabUserInfos->{'locale.id'};
    $tabViewData->{'currencySymbol'} = $tabUserInfos->{'currency.symbol'};

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/proforma/pdf.cgi';
}

# Function: valorizationPdfAction
# Génération du PDF d'un bon de valorisation
sub valorizationPdfAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $countryId    = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $documentType = &LOC::Request::getString('documentType', '');
    my $documentId   = &LOC::Request::getInteger('documentId', 0);
    our $tabViewData = &_getValorizationPdfInfos($countryId, $documentType, $documentId);
    if (!$tabViewData)
    {
        # Erreur 400: Bad Request
        &LOC::Browser::sendHeaders("Status: 400\nContent-Type: text/plain; charset=UTF-8\n\n");
        return;
    }

    $tabViewData->{'locale.id'}      = $tabUserInfos->{'locale.id'};
    $tabViewData->{'currencySymbol'} = $tabUserInfos->{'currency.symbol'};

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/proforma/pdf.cgi';
}


# Function : _appendLegal
# Ajoute du texte aux mentions légales. Unretour à la ligne est ajouté avant le nouveau texte.
#
# Parameters:
# string $legalRef - La référence à la chaîne de caractères contenant les mentions légales.
# string $text     - Le texte à ajouter.
sub _appendLegal
{
    my ($legalRef, $text) = @_;

    if ($text ne '')
    {
        $$legalRef .= ($$legalRef eq '' ? '' : "\n") . $text;
    }
}


sub _getPdfInfos
{
    my ($countryId, $id) = @_;

    my $tabProformaInfos = &LOC::Proforma::getInfos($countryId, $id, LOC::Proforma::GETINFOS_LINES |
                                                                     LOC::Proforma::GETINFOS_CONTRACT |
                                                                     LOC::Proforma::GETINFOS_ESTIMATELINE|
                                                                     LOC::Proforma::GETINFOS_LINK |
                                                                     LOC::Proforma::GETINFOS_LINKEXTENSION);
    if (!$tabProformaInfos)
    {
        return undef;
    }

    # A-t-on des remises exceptionnelles de location ou transport en attentes ?
    my $rentReducStatus = (defined $tabProformaInfos->{'contract.id'} ? $tabProformaInfos->{'contract.rentTariff.specialReductionStatus'} : $tabProformaInfos->{'estimateline.rentTariff.specialReductionStatus'});
    my $trspReducStatus = (defined $tabProformaInfos->{'contract.id'} ? $tabProformaInfos->{'contract.transportTariff.specialReductionStatus'} : $tabProformaInfos->{'estimateline.transportTariff.specialReductionStatus'});
    if ($rentReducStatus eq LOC::Tariff::Rent::REDUCTIONSTATE_PENDING || $trspReducStatus eq LOC::Tariff::Transport::REDUCTIONSTATE_PENDING)
    {
        # Si c'est le cas alors on ne doit pas pouvoir imprimer le pdf
        return undef;
    }


    my $documentType = (defined($tabProformaInfos->{'estimateLine.id'}) ? 'estimateLine' : 'contract');

    my $documentCode = ($documentType eq 'estimateLine' ? $tabProformaInfos->{'estimateLine.estimate.code'} : $tabProformaInfos->{'contract.code'});
    my $documentLine = ($documentType eq 'estimateLine' ? $tabProformaInfos->{'estimateLine.index'} : 0);
    my $orderNo      = ($documentType eq 'contract' ? $tabProformaInfos->{'contract.orderNo'} : $tabProformaInfos->{'estimateLine.estimate.orderNo'});
    my $orderContactName      = ($documentType eq 'contract' ? $tabProformaInfos->{'contract.orderContact.fullName'} : $tabProformaInfos->{'estimateLine.estimate.orderContact.fullName'});
    my $orderContactTelephone = ($documentType eq 'contract' ? $tabProformaInfos->{'contract.orderContact.telephone'} : $tabProformaInfos->{'estimateLine.estimate.orderContact.telephone'});
    my $negotiatorId = ($documentType eq 'contract' ? $tabProformaInfos->{'contract.negotiator.id'} : $tabProformaInfos->{'estimateLine.estimate.negotiator.id'});
    my $site = ($documentType eq 'contract' ? $tabProformaInfos->{'contract.site'} : $tabProformaInfos->{'estimateLine.estimate.site'});
    my $isVatInvoiced = $tabProformaInfos->{'isVatInvoiced'};

    my $tabLink = {
        'id'           => (defined($tabProformaInfos->{'link.id'}) ? $tabProformaInfos->{'link.id'} : undef),
        'type'         => (defined($tabProformaInfos->{'link.type.id'}) ? $tabProformaInfos->{'link.type.id'} : undef),
        'label'        => (defined($tabProformaInfos->{'link.type.label'}) ?$tabProformaInfos->{'link.type.label'} : undef),
        'creationDate' => (defined($tabProformaInfos->{'link.creationDate'}) ?$tabProformaInfos->{'link.creationDate'} : undef)
    };

    my $tabLinkExtension = {
        'id'           => (defined($tabProformaInfos->{'extension.id'}) ? $tabProformaInfos->{'extension.id'} : undef),
        'type'         => (defined($tabProformaInfos->{'extension.type.id'}) ? $tabProformaInfos->{'extension.type.id'} : undef),
        'label'        => (defined($tabProformaInfos->{'extension.type.label'}) ?$tabProformaInfos->{'extension.type.label'} : undef),
        'creationDate' => (defined($tabProformaInfos->{'extension.creationDate'}) ?$tabProformaInfos->{'extension.creationDate'} : undef)
    };


    # Récupération des informations du client
    my $tabCustomer = &LOC::Customer::getInfos($countryId, $tabProformaInfos->{'customer.id'});

    # Récupération du nom du négociateur
    my $tabNegotiator = &LOC::User::getInfos($negotiatorId, {}, {'formatMode' => 1});

    #config du pdf
    my $tabConfigPDF = &LOC::Json::fromJson(&LOC::Characteristic::getCountryValueByCode('PDFPROFORMA', $countryId,
                                                                                        $tabProformaInfos->{'creationDate'}));
    my $displayDeposit = &LOC::Characteristic::getCountryValueByCode('PFDEPOSITTOTAL', $countryId,
                                                                     $tabProformaInfos->{'creationDate'});

    # type de paiement/banque
    my $type = $tabProformaInfos->{'type.id'};
    my $typeRib = eval(&LOC::Characteristic::getCountryValueByCode('TYPERIB', $countryId,
                                                                   $tabProformaInfos->{'creationDate'}));
    my $legal = '';
    my $tabBank = undef;
    my $tabInfosPaymentMode = &LOC::PaymentMode::getInfos($countryId, $tabProformaInfos->{'paymentMode.id'});
    if ($tabInfosPaymentMode->{'flags'} & LOC::PaymentMode::FLAG_TRANSFER)
    {
        my $tabBankTemp = &LOC::Characteristic::getCountryValueByCode('RIB' . $typeRib, $countryId,
                                                                      $tabProformaInfos->{'creationDate'});
        if ($tabBankTemp eq '')
        {
            $tabBankTemp = &LOC::Characteristic::getCountryValueByCode('RIB1', $countryId,
                                                                       $tabProformaInfos->{'creationDate'});
        }
        $tabBank = &LOC::Json::fromJson($tabBankTemp);
        &_appendLegal(\$legal, $tabConfigPDF->{'legal'}->{$type}->{'transfer'});
    }
    if ($tabInfosPaymentMode->{'flags'} & LOC::PaymentMode::FLAG_CHECK)
    {
        &_appendLegal(\$legal, $tabConfigPDF->{'legal'}->{$type}->{'check'});
    }
    &_appendLegal(\$legal, $tabConfigPDF->{'legal'}->{$type}->{'all'});


    # calcul des différentes TVA
    my @tabVAT;
    my $totalTaxesIncluded;
    my $tabVATRate = &LOC::Country::getVATRatesList($countryId);

    if ($isVatInvoiced)
    {
        foreach my $vatKey (keys(%$tabVATRate))
        {
            my $total = 0;
            foreach my $line (@{$tabProformaInfos->{'tabLines'}})
            {
                if ($line->{'vatRate'} == $tabVATRate->{$vatKey}->{'value'} * 100)
                {
                    $total += $line->{'unitPrice'} * $line->{'quantity'};
                }
            }
            if ($total > 0)
            {
                my $tabVatTemp = {
                    'vatRate' => $tabVATRate->{$vatKey}->{'value'},
                    'amount'  => $total * $tabVATRate->{$vatKey}->{'value'} * 1
                };
                push(@tabVAT, $tabVatTemp);
            }
        }
    }
    else
    {
        my $tabVatTemp = {
            'vatRate' => 0,
            'amount'  => 0
        };
        push(@tabVAT, $tabVatTemp);
    }

    $totalTaxesIncluded = $tabProformaInfos->{'amountWithVat'};

    # Total final comptant TVA - déjà versé (+ garantie)
    my $finalTotal = $totalTaxesIncluded - $tabProformaInfos->{'paidAmount'};
    # Décompte du dépôt de garantie dans le total ?
    if ($displayDeposit)
    {
        $finalTotal += $tabProformaInfos->{'deposit'};
    }

    return {
        'type'               => $type,
        'id'                 => $tabProformaInfos->{'id'},
        'tabLink'            => $tabLink,
        'tabLinkExtension'   => $tabLinkExtension,
        'isPrintable'        => $tabProformaInfos->{'isPrintable'},
        'state.id'           => $tabProformaInfos->{'state.id'},
        'date'               => $tabProformaInfos->{'creationDate'},
        'agency.id'          => $tabProformaInfos->{'agency.id'},
        'negotiatorFullName' => ($tabNegotiator ? $tabNegotiator->{'fullName'} : ''),
        'tabCustomer'        => {
            'code'                   => $tabCustomer->{'code'},
            'name'                   => $tabCustomer->{'name'},
            'address1'               => $tabCustomer->{'address1'},
            'address2'               => $tabCustomer->{'address2'},
            'telephone'              => $tabCustomer->{'telephone'},
            'fax'                    => $tabCustomer->{'fax'},
            'locality.postalCode'    => $tabCustomer->{'locality.postalCode'},
            'locality.name'          => $tabCustomer->{'locality.name'},
            'orderContact.fullName'  => $orderContactName,
            'orderContact.telephone' => $orderContactTelephone
        },
        'document.site'      => $site,
        'document.type'      => $documentType,
        'document.code'      => $documentCode,
        'document.line'      => $documentLine,
        'document.orderNo'   => $orderNo,
        'isVatInvoiced'      => $isVatInvoiced,
        'tabVAT'             => \@tabVAT,
        'totalTaxesIncluded' => $totalTaxesIncluded,
        'paidAmount'         => $tabProformaInfos->{'paidAmount'},
        'finalTotal'         => $finalTotal,
        'deposit'            => $tabProformaInfos->{'deposit'},
        'depositTotal'       => $displayDeposit,
        'paidAmountNegative' => $tabConfigPDF->{'paidAmountNegative'},
        'total'              => $tabProformaInfos->{'amount'},
        'comment'            => $tabProformaInfos->{'comment'},
        'tabLines'           => $tabProformaInfos->{'tabLines'},
        'paymentMode.label'  => $tabInfosPaymentMode->{'label'},
        'tabBank'            => $tabBank,
        'legal'              => $legal
    };
}

sub _getValorizationPdfInfos
{
    my ($countryId, $documentType, $documentId) = @_;

    my $tabProformaInfos = {
        'type.id' => 0
    };
    $tabProformaInfos = &LOC::Proforma::getInfosByDocument($countryId, $documentType, $documentId, $tabProformaInfos);
    if (!$tabProformaInfos || $tabProformaInfos->{'document.amountType'} ne 'day')
    {
        return undef;
    }

    # A-t-on des remises exceptionnelles de location ou transport en attentes ?
    my $tabRentTariffInfos = &LOC::Tariff::Rent::getInfos($countryId, $documentType, $documentId, LOC::Tariff::Rent::GETINFOS_BASE);
    if ($tabRentTariffInfos->{'specialReductionStatus'} eq LOC::Tariff::Rent::REDUCTIONSTATE_PENDING)
    {
        return undef;
    }
    my $tabRentTransportInfos = &LOC::Tariff::Transport::getInfos($countryId, $documentType, $documentId, LOC::Tariff::Transport::GETINFOS_BASE);
    if ($tabRentTransportInfos->{'specialReductionStatus'} eq LOC::Tariff::Transport::REDUCTIONSTATE_PENDING)
    {
        return undef;
    }


    my $documentCode = ($documentType eq 'estimateLine' ? $tabProformaInfos->{'estimateLine.estimate.code'} : $tabProformaInfos->{'contract.code'});
    my $documentLine = ($documentType eq 'estimateLine' ? $tabProformaInfos->{'estimateLine.index'} : 0);
    my $orderNo      = ($documentType eq 'contract' ? $tabProformaInfos->{'contract.orderNo'} : $tabProformaInfos->{'estimateLine.estimate.orderNo'});
    my $orderContactName      = ($documentType eq 'contract' ? $tabProformaInfos->{'contract.orderContact.fullName'} : $tabProformaInfos->{'estimateLine.estimate.orderContact.fullName'});
    my $orderContactTelephone = ($documentType eq 'contract' ? $tabProformaInfos->{'contract.orderContact.telephone'} : $tabProformaInfos->{'estimateLine.estimate.orderContact.telephone'});
    my $negotiatorId = ($documentType eq 'contract' ? $tabProformaInfos->{'contract.negotiator.id'} : $tabProformaInfos->{'estimateLine.estimate.negotiator.id'});
    my $site         = ($documentType eq 'contract' ? $tabProformaInfos->{'contract.site'} : $tabProformaInfos->{'estimateLine.estimate.site'});
    my $isVatInvoiced = $tabProformaInfos->{'isVatInvoiced'};

    # Récupération des informations du client
    my $tabCustomer = &LOC::Customer::getInfos($countryId, $tabProformaInfos->{'customer.id'});

    # Récupération du nom du négociateur
    my $tabNegotiator = &LOC::User::getInfos($negotiatorId, {}, {'formatMode' => 1});

    # Besoin de carburant ?
    my $isFuelNeeded = (&LOC::Util::in_array($tabProformaInfos->{'machine.energy'}, &LOC::MachinesFamily::getFuelEnergiesList()) ? 1 : 0);
    my $nbLitres = 0;
    my $realNbLitres = 0;
    my $estimateFuelAmount = 0;
    if ($isFuelNeeded)
    {
        # calcul de l'estimation carburant théorique pour un bon de valorisation
        $nbLitres     = $tabProformaInfos->{'duration'} * $tabProformaInfos->{'fuelQtyEstimByDay'};
        $realNbLitres = ($nbLitres > $tabProformaInfos->{'fuelQtyMax'} ? $tabProformaInfos->{'fuelQtyMax'} : $nbLitres);
        $estimateFuelAmount = $realNbLitres * $tabProformaInfos->{'fuelUnitPrice'};
    }

    #config du pdf
    my $tabConfigPDF = &LOC::Json::fromJson(&LOC::Characteristic::getCountryValueByCode('PDFPROFORMA', $countryId,
                                                                                        $tabProformaInfos->{'creationDate'}));
    my $displayDeposit = &LOC::Characteristic::getCountryValueByCode('PFDEPOSITTOTAL', $countryId,
                                                                     $tabProformaInfos->{'creationDate'});

    # type de paiement/banque
    my $type = $tabProformaInfos->{'type.id'};
    my $legal = '';
    my $tabBank = undef;
    my $tabInfosPaymentMode = &LOC::PaymentMode::getInfos($countryId, $tabProformaInfos->{'paymentMode.id'});
    if ($tabInfosPaymentMode->{'flags'} & LOC::PaymentMode::FLAG_TRANSFER)
    {
        my $tabBankTemp = &LOC::Characteristic::getCountryValueByCode('RIB2', $countryId,
                                                                      $tabProformaInfos->{'creationDate'});
        if ($tabBankTemp eq '')
        {
            $tabBankTemp = &LOC::Characteristic::getCountryValueByCode('RIB1', $countryId,
                                                                       $tabProformaInfos->{'creationDate'});
        }
        $tabBank = &LOC::Json::fromJson($tabBankTemp);
        &_appendLegal(\$legal, $tabConfigPDF->{'legal'}->{$type}->{'transfer'});
    }
    if ($tabInfosPaymentMode->{'flags'} & LOC::PaymentMode::FLAG_CHECK)
    {
        &_appendLegal(\$legal, $tabConfigPDF->{'legal'}->{$type}->{'check'});
    }
    &_appendLegal(\$legal, $tabConfigPDF->{'legal'}->{$type}->{'all'});


    # calcul des différentes TVA
    my @tabVAT;
    my $totalTaxesIncluded;
    my $tabVATRate = &LOC::Country::getVATRatesList($countryId);

    foreach my $vatKey (keys(%$tabVATRate))
    {
        my $total = 0;
        foreach my $line (@{$tabProformaInfos->{'tabLines'}})
        {
            if ($line->{'vatRate'} == $tabVATRate->{$vatKey}->{'value'} * 100)
            {
                $total += $line->{'unitPrice'} * $line->{'quantity'};
            }
        }
        if ($total > 0)
        {
            $total = ($isVatInvoiced ? $total : 0);
            my $tabVatTemp = {
                'vatRate' => $tabVATRate->{$vatKey}->{'value'},
                'amount'  => $total * $tabVATRate->{$vatKey}->{'value'} * 1
            };
            push(@tabVAT, $tabVatTemp);
        }
    }

    $totalTaxesIncluded = $tabProformaInfos->{'amountWithVat'};



    # Décompte du dépôt de garantie dans le total ?
    if ($displayDeposit)
    {
        $totalTaxesIncluded += $tabProformaInfos->{'deposit'};
    }


    return {
        'type'               => 0, #Bon de valorisation pour le PDF
        'isPrintable'        => 1,
        'date'               => $tabProformaInfos->{'creationDate'},
        'agency.id'          => $tabProformaInfos->{'agency.id'},
        'negotiatorFullName' => ($tabNegotiator ? $tabNegotiator->{'fullName'} : ''),
        'tabCustomer'        => {
            'code'                   => $tabCustomer->{'code'},
            'name'                   => $tabCustomer->{'name'},
            'address1'               => $tabCustomer->{'address1'},
            'address2'               => $tabCustomer->{'address2'},
            'telephone'              => $tabCustomer->{'telephone'},
            'fax'                    => $tabCustomer->{'fax'},
            'locality.postalCode'    => $tabCustomer->{'locality.postalCode'},
            'locality.name'          => $tabCustomer->{'locality.name'},
            'orderContact.fullName'  => $orderContactName,
            'orderContact.telephone' => $orderContactTelephone
        },
        'document.site'      => $site,
        'document.type'      => $documentType,
        'document.code'      => $documentCode,
        'document.line'      => $documentLine,
        'document.orderNo'   => $orderNo,
        'isVatInvoiced'      => $tabProformaInfos->{'isVatInvoiced'},
        'tabVAT'             => \@tabVAT,
        'totalTaxesIncluded' => $totalTaxesIncluded,
        'paidAmount'         => $tabProformaInfos->{'paidAmount'},
        'finalTotal'         => $totalTaxesIncluded - $tabProformaInfos->{'paidAmount'},
        'deposit'            => $tabProformaInfos->{'deposit'},
        'depositTotal'       => $displayDeposit,
        'paidAmountNegative' => $tabConfigPDF->{'paidAmountNegative'},
        'total'              => $tabProformaInfos->{'amount'},
        'comment'            => $tabProformaInfos->{'comment'},
        'tabLines'           => $tabProformaInfos->{'tabLines'},
        'isFuelNeeded'       => $isFuelNeeded,
        'estimateFuelAmount' => $estimateFuelAmount,
        'paymentMode.label'  => $tabInfosPaymentMode->{'label'},
        'tabBank'            => $tabBank,
        'legal'              => $legal
    };
}

1;