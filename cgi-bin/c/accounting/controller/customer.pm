use utf8;
use open (':encoding(UTF-8)');

# Package: customer
# Contrôleur
package customer;

use strict;
use File::Basename;
use Tie::IxHash;

use LOC::Customer;


# Function: jsonListAction
# Récupérer la liste des clients (avec recherche possible)
#
sub jsonListAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id' => $tabUserInfos->{'locale.id'},
        'errorCode' => ''
    );

    # Récupération des paramètres
    my $countryId    = &LOC::Request::getString('countryId');
    my $searchValue  = &LOC::Util::trim(&LOC::Request::getString('searchValue', ''));
    my $isRestricted = &LOC::Request::getBoolean('isRestricted', 0);
    my $format       = &LOC::Request::getInteger('format', 0);

    if ($searchValue eq '')
    {
        $tabViewData{'errorCode'} = 'emptysearch';
        $tabViewData{'result'} = {
            'returnCode' => 'error'
        };
    }
    else
    {
        # Liste des clients
        my $tabFilters = {'search' => $searchValue};
        if ($isRestricted)
        {
            my $accountingCodes = &LOC::Characteristic::getCountryValueByCode('LISTECL', $countryId);
            my @tabAccountingCodes = split(/;/, $accountingCodes);
            $tabFilters->{'accountingCode'} = \@tabAccountingCodes;
        }

        # Récupération de la liste des modèles de machine
        my $nbResult = &LOC::Customer::getList($countryId, LOC::Util::GETLIST_COUNT, $tabFilters);
        if ($nbResult == 0)
        {
            $tabViewData{'errorCode'} = 'noresult';
            $tabViewData{'result'} = {
                'returnCode' => 'error'
            };
        }
        elsif ($nbResult < 100)
        {
            my $result = &LOC::Customer::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters);

            my $tab = [];
            if ($format == 0)
            {
                foreach (keys %$result)
                {
                    push(@$tab, {'id'                    => $result->{$_}->{'id'},
                                 'name'                  => $result->{$_}->{'name'},
                                 'locality.postalCode'   => $result->{$_}->{'locality.postalCode'},
                                 'locality.name'         => $result->{$_}->{'locality.name'},
                                 'lockLevel'             => $result->{$_}->{'lockLevel'},
                                 'locality.country.name' => $result->{$_}->{'locality.country.name'}});
                }
            }
            elsif ($format == 1)
            {
                foreach (keys %$result)
                {
                    push(@$tab, [$result->{$_}->{'id'}, $result->{$_}->{'name'}]);
                }
            }
            $tabViewData{'result'} = {
                'returnCode' => 'ok',
                'format'     => $format,
                'tabData'    => $tab
            };
        }
        else
        {
            $tabViewData{'errorCode'} = 'exceeded';
            $tabViewData{'result'} = {
                'returnCode' => 'warning'
            };
        }
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/customer/jsonList' . ($tabViewData{'errorCode'} ne '' ? 'Err' : '') . '.cgi';
}


# Function: jsonInfosAction
# Récupérer les informations d'un client à partir de son identifiant
#
sub jsonInfosAction
{
    # Variables pour la vue
    our %tabViewData = ();

    # Récupération des paramètres
    my $countryId  = &LOC::Request::getString('countryId');
    my $customerId = &LOC::Request::getString('customerId', '');
    my $flags      = &LOC::Request::getInteger('flags', LOC::Customer::GETINFOS_ALL);

    # Récupération des informations client
    $tabViewData{'tabCustomerInfos'} = &LOC::Customer::getInfos($countryId, $customerId, $flags);

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/customer/jsonInfos.cgi';
}


# Function: sendUnlockRequestAction
# Envoie une demande de déblocage temporaire
#
sub sendUnlockRequestAction
{
    # Variables pour la vue
    our %tabViewData = ();

    my $tabParameters = $_[0];
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $userId   = $tabUserInfos->{'id'};

    # Récupération des paramètres
    my $countryId     = &LOC::Request::getString('countryId');
    my $customerId    = &LOC::Request::getInteger('id', 0);
    my $type          = &LOC::Request::getString('type', 'create');
    my $tabDocumentInfos = &LOC::Request::getString('documentInfos', undef);
    my $comment       = &LOC::Request::getString('comment', undef);
    
    my $tabData = {
        'comment' => $comment
    };
    # Envoi de la demande de déblocage
    $tabViewData{'result'} = &LOC::Customer::sendUnlockRequest($countryId, $customerId, $type, $tabDocumentInfos, $tabData, $userId);

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/customer/sendUnlockRequest.cgi';
}

1;
