use utf8;
use open (':encoding(UTF-8)');

# Package: machineInventory
# Gestion des états des lieux
package machineInventory;


use strict;
use File::Basename;

use LOC::MachineInventory;
use LOC::AjaxTable;
use LOC::Area;
use LOC::Util;
use LOC::Json;
use LOC::Template;

# Function: listAction
# Affichage de la liste des états des lieux à gérer
sub listAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $countryId    = $tabUserInfos->{'nomadCountry.id'};

    my $isTelematActived = (&LOC::Characteristic::getCountryValueByCode('TELEMAT', $countryId) != 0 ? 1 : 0);


    # Variables pour la vue
    our %tabViewData = (
        'locale.id'        => $tabUserInfos->{'locale.id'},
        'user.id'          => $tabUserInfos->{'id'},
        'user.agency.id'   => $tabUserInfos->{'nomadAgency.id'},
        'user.country.id'  => $tabUserInfos->{'nomadCountry.id'},
        'currencySymbol'   => $tabUserInfos->{'currency.symbol'},
        'timeZone'         => $tabSession->{'timeZone'},
        'isTelematActived' => $isTelematActived
    );

    # Type d'affichage
    my $displayType = &LOC::Request::getString('displayType', '');

    # Récupération du nombre d'éléments par page
    my $pageSize   = &LOC::Request::getInteger('pageSize', 20);
    my $startIndex = &LOC::Request::getInteger('startIndex', 0);
    # Récupération des tris demandés
    my $tabSorts   = &LOC::Request::getArray('sorts', [{'col' => 'date', 'dir' => 'asc'}]);

    my $tabOptions = {};
    my $tabDefaultActivesFilters = {};

    # Récupération des filtres demandés
    my $tabActivesFilters = &LOC::Request::getHash('filters', $tabDefaultActivesFilters);

    # Récupération de la liste des transports et des filtres
    $tabViewData{'data'} = &_getData($countryId, $tabActivesFilters, $tabSorts,
                                     $startIndex, $pageSize, $tabUserInfos, $tabOptions);

    foreach my $row (@{$tabViewData{'data'}->{'rows'}})
    {
        $row->{'tabRights'} = &LOC::MachineInventory::getRights($countryId, $tabViewData{'user.agency.id'},
                                                            $row->{'id'}, $tabViewData{'user.id'});
    }

    if ($tabViewData{'data'}->{'result'} eq 'ko')
    {
        exit;
    }


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;

    $tabViewData{'controllerAction'} = 'transport:machineInventory:list';
    if ($displayType eq '')
    {
        # Paramètres de tris et pagination
        $tabViewData{'pageSize'}              = $pageSize;
        $tabViewData{'startIndex'}            = $startIndex;
        $tabViewData{'sorts'}                 = $tabSorts;
        $tabViewData{'activesFilters'}        = $tabActivesFilters;
        $tabViewData{'defaultActivesFilters'} = $tabDefaultActivesFilters;

        # Page HTML
        require $directory . '/view/machineInventory/list.cgi';
    }
    elsif ($displayType eq 'json')
    {
        # Trame JSON
        require $directory . '/view/machineInventory/json.cgi';
    }
}



# Function: _getData
# Récupérer les données du tableau et les filtres
#
# Parameters:
# string   $countryId         - Pays
# hashref  $tabActivesFilters - Filtres actifs
# arrayref $tabSorts          - Tris
# int      $startIndex        - Index de la ligne de départ à récupérer
# int      $pageSize          - Nombre de lignes à récupérer
# hashref  $tabUserInfos      - Informations sur l'utilisateur
# hashref  $tabOptions        - Options supplémentaires
#
# Returns:
# hashref - Données du tableau et des filtres
sub _getData
{
    my ($countryId, $tabActivesFilters, $tabSorts, $startIndex, $pageSize, $tabUserInfos, $tabOptions) = @_;


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);


    # Filtres disponibles
    my %tabFiltersCfgs = (

        'agency.id' => {
            'type'    => 'list',
            'select'  => {
                'value' => 'IFNULL(min_agc_id, "")'
            },
            'where'   => 'IFNULL(min_agc_id, "") IN (<%values>)',
            'values' => {}
        },

        'area.id' => {
            'type'   => 'list',
            'select' => {
                'value' => 'IFNULL(tbl_p_agency.agc_are_id, "")',
                'label' => 'IFNULL(tbl_p_area.are_label, "")'
            },
            'where'  => 'IFNULL(tbl_p_agency.agc_are_id, "") IN (<%values>)'
        },

        'type'    => {
            'type'    => 'list',
            'select'  => {
                'value' => 'min_type'
            },
            'where'   => 'min_type IN (<%values>)'
        },

        'user.id'    => {
            'type'    => 'list',
            'select'  => {
                'value' => 'min_usr_id_creator',
                'label' => 'CONCAT_WS(" ", tbl_f_user.usr_firstname, tbl_f_user.usr_name)'
            },
            'where'   => 'min_usr_id_creator IN (<%values>)'
        },

        'status'    => {
            'type'    => 'list',
            'select'  => {
                'value' => 'min_status'
            },
            'where'   => sub {
                my ($tabErrors) = @_;

                my @tabWheres = ();
                foreach my $error (@$tabErrors)
                {
                    if ($error * 1 > 0)
                    {
                        push(@tabWheres, '(min_status & ' . $error . ')');
                    }
                    else
                    {
                        push(@tabWheres, '(min_status = 0)');
                    }
                }

                return join(' OR ', @tabWheres);
            },
            'values' => {}
        }
    );


    # ***********************************************************************************************************
    # Récupération de la liste
    # ***********************************************************************************************************

    # Colonnes disponibles
    my %tabColumnsCfgs = (
        'id' => {
            'select' => 'min_id'
        },
        'type' => {
            'select' => 'min_type',
        },
        'agency.id' => {
            'select' => 'min_agc_id'
        },
        'date' => {
            'select' => 'min_date'
        },
        'user.fullName' => {
            'select' => 'CONCAT_WS(" ", tbl_f_user.usr_firstname, tbl_f_user.usr_name)',
            'orderBy' => '`user.fullName` <%dir>'
        },
        'status' => {
            'select' => 'min_status'
        },
        'machine.id' => {
            'select' => 'min_mac_id'
        },
        'machine.parkNumber' => {
            'select' => 'CONCAT(IFNULL(tbl_f_machine.MANOPARC_AUX, ""), tbl_f_machine.MANOPARC)',
        },
        'machine.enteredParkNumber' => {
            'select' => 'IF (min_is_parknumberok = 0, min_parknumber, NULL)'
        },
        'contract.id' => {
            'select' => 'min_rct_id'
        },
        'contract.code' => {
            'select' => 'tbl_f_contract.CONTRATCODE',
        },
        'contract.enteredCode' => {
            'select' => 'IF (min_is_contractcodeok = 0, min_contractcode, NULL)'
        },
    );

    # Jointures
    my @tabJoinsCfgs = (
        {
            'type'  => 'INNER',
            'table' => 'frmwrk.f_user',
            'alias' => 'tbl_f_user',
            'on'    => 'min_usr_id_creator = tbl_f_user.usr_id'
        },
        {
            'type'  => 'LEFT',
            'table' => 'AUTH.MACHINE',
            'alias' => 'tbl_f_machine',
            'on'    => 'tbl_f_machine.MAAUTO = min_mac_id'
        },
        {
            'type'  => 'LEFT',
            'table' => 'CONTRAT',
            'alias' => 'tbl_f_contract',
            'on'    => 'tbl_f_contract.CONTRATAUTO = min_rct_id'
        },
        {
            'type'  => 'LEFT',
            'table' => 'frmwrk.p_agency',
            'alias' => 'tbl_p_agency',
            'on'    => 'tbl_p_agency.agc_id = min_agc_id'
        },
        {
            'type'  => 'LEFT',
            'table' => 'frmwrk.p_area',
            'alias' => 'tbl_p_area',
            'on'    => 'tbl_p_area.are_id = tbl_p_agency.agc_are_id'
        }
    );

    # Conditions permanentes
    my @tabQueryFirstWheres = ();

    # - status
    push(@tabQueryFirstWheres, 'min_sta_id IN (' . $db->quote([LOC::MachineInventory::STATE_NOTATTACHED]) . ')');

    # Filtres actifs par défaut
    my @tabAutorAgencies = keys %{$tabUserInfos->{'tabAuthorizedAgencies'}};
    # - Agences
    my @agencyFilter = ('min_agc_id IN (' . $db->quote(\@tabAutorAgencies) . ')');
    # - Agence vide
    push(@agencyFilter, 'min_agc_id IS NULL');
    push(@tabQueryFirstWheres, '(' . join(' OR ', @agencyFilter) . ')');

    return &LOC::AjaxTable::getData($db, 'f_machineinventory', \@tabJoinsCfgs,
                                    \%tabColumnsCfgs, \%tabFiltersCfgs,
                                    \@tabQueryFirstWheres, $tabActivesFilters,
                                    $tabSorts, $startIndex, $pageSize, $tabOptions);
}

# Function: viewAction
# Affichage d'un état des lieux
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $countryId    = $tabUserInfos->{'nomadCountry.id'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'        => $tabUserInfos->{'locale.id'},
        'user.id'          => $tabUserInfos->{'id'},
        'user.agency.id'   => $tabUserInfos->{'nomadAgency.id'},
        'user.country.id'  => $tabUserInfos->{'nomadCountry.id'},
        'currencySymbol'   => $tabUserInfos->{'currency.symbol'},
        'timeZone'         => $tabSession->{'timeZone'},
        'errors'           => [],
        'valid'            => []
    );

    my $id   = &LOC::Request::getInteger('id', undef);
    my $from = &LOC::Request::getString('from', undef);
    my $action = &LOC::Request::getString('action', undef);

    if ($action eq 'detach')
    {
        my $tabErrors = {};
        # Détacher l'état des lieux du contrat
        if (!&LOC::MachineInventory::detach($countryId, $id, $tabUserInfos->{'id'}, {}, $tabErrors, undef))
        {
            if (&LOC::Util::in_array(LOC::MachineInventory::ERROR_RIGHTS, $tabErrors->{'fatal'}))
            {
                push(@{$tabViewData{'errors'}}, 'rightsError');
            }
            elsif (&LOC::Util::in_array(LOC::MachineInventory::ERROR_ALREADYDETACHED, $tabErrors->{'fatal'}))
            {
                push(@{$tabViewData{'errors'}}, 'alreadyDetached');
            }
            else
            {
                push(@{$tabViewData{'errors'}}, 'fatalError');
            }
        }
        else
        {
            push(@{$tabViewData{'valid'}}, 'detach');
        }
    }
    elsif ($action eq 'attach')
    {
        my $tabErrors = {};
        # Mettre à jour et rattacher l'état des lieux
        my $tabData = {
            'machine.id'  => &LOC::Request::getInteger('machineId', undef),
            'contract.id' => &LOC::Request::getInteger('contractId', undef)
        };

        if (!&LOC::MachineInventory::update($countryId, $id, $tabData, $tabUserInfos->{'id'}, {'attach' => 1}, $tabErrors, undef))
        {
            if (&LOC::Util::in_array(LOC::MachineInventory::ERROR_RIGHTS, $tabErrors->{'fatal'}))
            {
                push(@{$tabViewData{'errors'}}, 'rightsError');
            }
            elsif (&LOC::Util::in_array(LOC::MachineInventory::ERROR_ATTACHED, $tabErrors->{'fatal'}))
            {
                push(@{$tabViewData{'errors'}}, 'attached');
            }
            elsif (&LOC::Util::in_array(LOC::MachineInventory::ERROR_UNDEFINEDMACHINE, $tabErrors->{'fatal'}))
            {
                push(@{$tabViewData{'errors'}}, 'undefinedMachine');
            }
            elsif (&LOC::Util::in_array(LOC::MachineInventory::ERROR_UNKNOWNMACHINE, $tabErrors->{'fatal'}))
            {
                push(@{$tabViewData{'errors'}}, 'unknownMachine');
            }
            elsif (&LOC::Util::in_array(LOC::MachineInventory::ERROR_UNDEFINEDCONTRACT, $tabErrors->{'fatal'}))
            {
                push(@{$tabViewData{'errors'}}, 'undefinedContract');
            }
            elsif (&LOC::Util::in_array(LOC::MachineInventory::ERROR_UNKNOWNCONTRACT, $tabErrors->{'fatal'}))
            {
                push(@{$tabViewData{'errors'}}, 'unknownContract');
            }
            elsif (&LOC::Util::in_array(LOC::MachineInventory::ERROR_INVALIDCOUPLE, $tabErrors->{'fatal'}))
            {
                push(@{$tabViewData{'errors'}}, 'invalidCouple');
            }
            else
            {
                push(@{$tabViewData{'errors'}}, 'fatalError');
            }
        }
        else
        {
            push(@{$tabViewData{'valid'}}, 'attach');
        }
    }
    elsif ($action eq 'delete')
    {
        my $tabErrors = {};
        if (!&LOC::MachineInventory::delete($countryId, $id, $tabUserInfos->{'id'}, {}, $tabErrors, undef))
        {
            if (&LOC::Util::in_array(LOC::MachineInventory::ERROR_RIGHTS, $tabErrors->{'fatal'}))
            {
                push(@{$tabViewData{'errors'}}, 'rightsError');
            }
            elsif (&LOC::Util::in_array(LOC::MachineInventory::ERROR_ATTACHED, $tabErrors->{'fatal'}))
            {
                push(@{$tabViewData{'errors'}}, 'attached');
            }
            else
            {
                push(@{$tabViewData{'errors'}}, 'fatalError');
            }
        }
        else
        {
            push(@{$tabViewData{'valid'}}, 'delete');
        }
    }

    my $tabInfos = &LOC::MachineInventory::getInfos($countryId, $id,
                                                    LOC::MachineInventory::GETINFOS_BASE |
                                                    LOC::MachineInventory::GETINFOS_CREATOR |
                                                    LOC::MachineInventory::GETINFOS_MACHINE |
                                                    LOC::MachineInventory::GETINFOS_CONTRACT |
                                                    LOC::MachineInventory::GETINFOS_POSSIBILITIES |
                                                    LOC::MachineInventory::GETINFOS_RIGHTS);

    if ($tabInfos)
    {
        my $tabStates = {
            LOC::MachineInventory::STATE_ATTACHED    => 'attached',
            LOC::MachineInventory::STATE_NOTATTACHED => 'notAttached',
            LOC::MachineInventory::STATE_DELETED     => 'deleted'
        };
        $tabViewData{'infos'} = {
            'id'                    => $tabInfos->{'id'},
            'type'                  => $tabInfos->{'type'},
            'date'                  => $tabInfos->{'date'},
            'machineId'             => $tabInfos->{'machine.id'},
            'parkNumber'            => ($tabInfos->{'machine.tabInfos'} ? $tabInfos->{'machine.tabInfos'}->{'parkNumber'} : ''),
            'enteredParkNumber'     => $tabInfos->{'parkNumber'},
            'isParkNumberOk'        => $tabInfos->{'isParkNumberOk'},
            'contractId'            => $tabInfos->{'contract.id'},
            'contractCode'          => ($tabInfos->{'contract.tabInfos'} ? $tabInfos->{'contract.tabInfos'}->{'code'} : ''),
            'enteredContractCode'   => $tabInfos->{'contractCode'},
            'siteLabel'             => ($tabInfos->{'contract.tabInfos'} ? $tabInfos->{'contract.tabInfos'}->{'site.label'} : ''),
            'siteAddress'           => ($tabInfos->{'contract.tabInfos'} ? $tabInfos->{'contract.tabInfos'}->{'site.address'} : ''),
            'siteLocalityLabel'     => ($tabInfos->{'contract.tabInfos'} ? $tabInfos->{'contract.tabInfos'}->{'site.locality.label'} : ''),
            'isContractCodeOk'      => $tabInfos->{'isContractCodeOk'},
            'latitude'              => $tabInfos->{'latitude'},
            'longitude'             => $tabInfos->{'longitude'},
            'creator'               => $tabInfos->{'creator.fullName'},
            'description'           => $tabInfos->{'description'},
            'stateId'               => $tabStates->{$tabInfos->{'state.id'}},
            'tabRights'             => $tabInfos->{'tabRights'},
            'tabPossibilities'      => $tabInfos->{'tabPossibilities'}
        };

        # URL d'envoi de l'état des lieux par mail
        my $schemaName = &LOC::Db::getSchemaName($countryId);
        # - Récupération des informations à mettre dans tabAdditionalAttachments
        my @tabAdditionalAttachments;
        my $tabPhotosList = &LOC::MachineInventory::getPhotosList($countryId, $tabInfos->{'id'});
        foreach my $tabPhotoInfos (@$tabPhotosList)
        {
            push(@tabAdditionalAttachments, {
                'attachmentId' => $tabPhotoInfos->{'id'},
                'filename'     => $tabPhotoInfos->{'fileName'},
                'extension'    => $tabPhotoInfos->{'extension'}
            });
        }
        # - Récupération des configurations pour l'envoi par mail
        my $code = ($tabInfos->{'type'} eq LOC::MachineInventory::TYPE_DELIVERY ? 'MAILEDLLIV' : 'MAILEDLREC');
        my $tabMailCfgs = &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode($code, $tabUserInfos->{'agency.id'}));

        # Remplacement des variables
        my $tabTemplateReplaces = {
            'date' => $tabViewData{'infos'}->{'date'}
        };

        $tabMailCfgs->{'subject'}  =~ s/<%parkNumber>/$tabViewData{'infos'}->{'parkNumber'}/;
        $tabMailCfgs->{'content'}  =~ s/<%parkNumber>/$tabViewData{'infos'}->{'parkNumber'}/;
        $tabMailCfgs->{'content'}  =~ s/\<%dte\:(.*?)\>/@{[&LOC::Template::formatDate($1, $tabTemplateReplaces)]}/;
        $tabMailCfgs->{'content'}  =~ s/<%site.label>/$tabViewData{'infos'}->{'siteLabel'}/;
        $tabMailCfgs->{'content'}  =~ s/<%site.address>/$tabViewData{'infos'}->{'siteAddress'}/;
        $tabMailCfgs->{'content'}  =~ s/<%site.locality.label>/$tabViewData{'infos'}->{'siteLocalityLabel'}/;
        $tabMailCfgs->{'content'}  =~ s/<%contract.code>/$tabViewData{'infos'}->{'contractCode'}/;

        $tabViewData{'infos'}->{'sendMail'} = {
            'defaultRecipient'         => ($tabInfos->{'contract.tabInfos'} ? $tabInfos->{'contract.tabInfos'}->{'orderContact.email'} : undef),
            'defaultSubject'           => $tabMailCfgs->{'subject'},
            'defaultContent'           => $tabMailCfgs->{'content'},
            'documentAgency.id'        => $tabUserInfos->{'agency.id'},
            'tabAdditionalAttachments' => \@tabAdditionalAttachments,
            'tabEndActions' => [
                {
                    'type'      => 'addTrace',
                    'functionality'   => &LOC::Log::Functionality::MACHINEINVENTORY,
                    'functionalityId' => $tabInfos->{'id'},
                    'tabContent'      => {
                        'schema'  => $schemaName,
                        'code'    => LOC::Log::EventType::SENDMAIL,
                        'old'     => undef,
                        'new'     => undef,
                        'content' => '<%recipient>'
                    }
                }
            ]
        };
    }

    $tabViewData{'from'} = $from;

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;

    require $directory . '/view/machineInventory/view.cgi';
}

# Function: jsonPhotos
# Récupération de la liste des photos d'un état des lieux
sub jsonPhotosAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $countryId = $tabUserInfos->{'country.id'};

    our %tabViewData = ();

    my $machineInventoryId = &LOC::Request::getInteger('id', 0);

    my $tabOptions = {
        'getBase64Content' => 1,
        'getContents'      => { # Récupération du contenu de la miniature sm
            'thumbnails' => ['sm']
        }
    };

    my @tabPhotos;
    my $tabPhotosList = &LOC::MachineInventory::getPhotosList($countryId, $machineInventoryId, undef, $tabOptions);
    foreach my $tabPhotoInfos (@$tabPhotosList)
    {
        my $photo = {
            'id'           => $tabPhotoInfos->{'id'},
            'fileName'     => $tabPhotoInfos->{'fileName'},
            'date'         => $tabPhotoInfos->{'takeDate'},
            'mimeType'     => $tabPhotoInfos->{'mimeType'},
            'content'      => (ref $tabPhotoInfos->{'thumbnailsContents'} eq 'HASH' ? $tabPhotoInfos->{'thumbnailsContents'}->{'sm'} : ''),
            'latitude'     => $tabPhotoInfos->{'latitude'},
            'longitude'    => $tabPhotoInfos->{'longitude'},
            'url'          => &LOC::Request::createRequestUri('transport:machineInventory:displayPhoto',
                                                                {'photoId' => $tabPhotoInfos->{'id'}})
        };
        push(@tabPhotos, $photo);
    }
    $tabViewData{'tabPhotos'} = \@tabPhotos;

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/machineInventory/jsonPhotos.cgi';
}

# Function: jsonHistory
# Récupération de l'historique d'un état des lieux
sub jsonHistoryAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $countryId = $tabUserInfos->{'country.id'};

    our %tabViewData = ();

    my $machineInventoryId = &LOC::Request::getInteger('id', 0);

    my $tabHistory = &LOC::MachineInventory::getHistory($countryId, $machineInventoryId, $tabUserInfos->{'locale.id'});

    # On n'envoie que les données qui nous intéressent
    my @tabResult = ();
    foreach my $history (@$tabHistory)
    {
        my $tabElement = {
            'id'              => $history->{'id'},
            'datetime'        => $history->{'datetime'},
            'user.fullName'   => $history->{'user.fullName'},
            'user.state.id'   => $history->{'user.state.id'},
            'eventType.label' => $history->{'eventType.label'},
            'content'         => $history->{'content'} . ''
        };
        push(@tabResult, $tabElement);
    }
    # Tri par date décroissante puis par id décroissant
    @tabResult = sort { -($a->{'datetime'} cmp $b->{'datetime'}) ||
                        -($a->{'id'} cmp $b->{'id'}) } @tabResult;
    $tabViewData{'tabHistory'} = \@tabResult;

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/machineInventory/jsonHistory.cgi';
}



# Function: displayPhotoAction
# Téléchargement d'une photo
sub displayPhotoAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'localeId' => $tabUserInfos->{'locale.id'},
        'errors'   => undef
    );

    # Récupération du fichier
    my $id = &LOC::Request::getInteger('photoId', 0);

    my $tabOptions = {
        'getContents' => 1,
    };

    my $tabInfos = &LOC::MachineInventory::getPhotoInfos($tabUserInfos->{'country.id'}, $id, $tabOptions);
    if ($tabInfos && $tabInfos->{'fileContent'} ne '')
    {
        $tabViewData{'fileInfos'} = $tabInfos;
    }
    else
    {
        $tabViewData{'errors'} = 'unknownFile';
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/machineInventory/displayPhoto.cgi';
}


1;
