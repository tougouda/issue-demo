use utf8;
use open (':encoding(UTF-8)');

# Package: selfForm
# Contrôleur pour les bordereaux d'enlèvement ou de restitution
package selfForm;

use strict;
use File::Basename;

use LOC::Country;
use LOC::Customer;
use LOC::State;
use LOC::Contract::Rent;
use LOC::Characteristic;
use LOC::Json;
use LOC::Util;
use LOC::Transport;
use LOC::Browser;


# Function: pdfAction
# Génération du PDF d'un bordereau d'enlèvement ou de restitution
#
sub pdfAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $countryId    = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $transportId  = &LOC::Request::getInteger('transportId', 0);
    my $side         = &LOC::Request::getString('side', undef); 

    my $selfFormId = &LOC::Transport::generateSelfForm($countryId, $transportId, $side, 1, $tabUserInfos->{'id'});

    our $tabViewData = &_getPdfInfos($countryId, $transportId, $side);
    if (!$tabViewData)
    {
        # Erreur 400: Bad Request
        &LOC::Browser::sendHeaders("Status: 400\nContent-Type: text/plain; charset=UTF-8\n\n");
        return;
    }

    $tabViewData->{'locale.id'}      = $tabUserInfos->{'locale.id'};
    $tabViewData->{'currencySymbol'} = $tabUserInfos->{'currency.symbol'};

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/selfForm/pdf.cgi';
}

# Function _getPdfInfos
# Récupération des informations nécessaires à l'impression du PDF
#
# Parameters
# string   $countryId  - Identifiant du pays
# int      $id         - Identifiant du transport
# string   $side       - Côté du transport
#
# Return
# hashref
#
sub _getPdfInfos
{
    my ($countryId, $id, $side) = @_;

    my $flagSelfForm = ($side eq LOC::Transport::SIDE_DELIVERY ? LOC::Transport::GETINFOS_DELIVERYSELFFORM :
                                                                 LOC::Transport::GETINFOS_RECOVERYSELFFORM);

    my $tabTransportInfos = &LOC::Transport::getInfos($countryId, $id, LOC::Transport::GETINFOS_FROM |
                                                                       LOC::Transport::GETINFOS_TO |
                                                                       $flagSelfForm,
                                                                       {'getFromInfosFlags' => LOC::Transport::GETFROMTOINFOS_DETAILS,
                                                                        'getToInfosFlags'   => LOC::Transport::GETFROMTOINFOS_DETAILS});
    # si le bordereau n'est pas éditable
    if (!$tabTransportInfos ||
        ($side eq LOC::Transport::SIDE_DELIVERY && !$tabTransportInfos->{'deliverySelfForm.id'}) ||
        ($side eq LOC::Transport::SIDE_RECOVERY && !$tabTransportInfos->{'recoverySelfForm.id'}))
    {
        return undef;
    }

    my %tabSideInfos;
    my %tabOtherSideInfos;
    my %tabAgencyInfos;

    my $id;
    my $date;

    # Bordereau d'enlèvement
    if ($side eq LOC::Transport::SIDE_DELIVERY)
    {
        %tabSideInfos = %{$tabTransportInfos->{'to.tabInfos'}};
        # Livraison
        if ($tabTransportInfos->{'type'} eq LOC::Transport::TYPE_DELIVERY)
        {
            %tabAgencyInfos  = %{$tabTransportInfos->{'from.tabInfos'}}; 
        }
        # Reprise sur chantier
        elsif ($tabTransportInfos->{'type'} eq LOC::Transport::TYPE_SITESTRANSFER)
        {
            %tabOtherSideInfos = %{$tabTransportInfos->{'from.tabInfos'}};
        }
        $id   = $tabTransportInfos->{'deliverySelfForm.tabInfos'}->{'id'};
        $date = $tabTransportInfos->{'deliverySelfForm.tabInfos'}->{'modificationDate'};
    }

    # Bordereau de restitution
    if ($side eq LOC::Transport::SIDE_RECOVERY)
    {
        %tabSideInfos = %{$tabTransportInfos->{'from.tabInfos'}};
        # Livraison
        if ($tabTransportInfos->{'type'} eq LOC::Transport::TYPE_RECOVERY)
        {
            %tabAgencyInfos  = %{$tabTransportInfos->{'to.tabInfos'}}; 
        }
        # Reprise sur chantier
        elsif ($tabTransportInfos->{'type'} eq LOC::Transport::TYPE_SITESTRANSFER)
        {
            %tabOtherSideInfos = %{$tabTransportInfos->{'to.tabInfos'}};
        }
        $id   = $tabTransportInfos->{'recoverySelfForm.tabInfos'}->{'id'};
        $date = $tabTransportInfos->{'recoverySelfForm.tabInfos'}->{'modificationDate'};
    }

    my $tabCustomer      = &LOC::Customer::getInfos($countryId, $tabSideInfos{'tabDetails'}->{'customer.id'});
    my $tabMachineInfos  = &LOC::Machine::getInfos($countryId, $tabSideInfos{'machine.id'}, LOC::Machine::GETINFOS_BASE, {'level' => LOC::Machine::LEVEL_BUSINESSFAMILY});


    return {
        'id'                => $id,
        'date'              => $date,
        'side'              => $side,
        'type'              => $tabTransportInfos->{'type'},
        'agency.id'         => $tabTransportInfos->{'agency.id'},
        'tabCustomer'       => {
            'code'                => $tabCustomer->{'code'},
            'name'                => $tabCustomer->{'name'},
            'address1'            => $tabCustomer->{'address1'},
            'address2'            => $tabCustomer->{'address2'},
            'locality.postalCode' => $tabCustomer->{'locality.postalCode'},
            'locality.name'       => $tabCustomer->{'locality.name'},
            'orderContact.fullName'  => $tabSideInfos{'orderContact.fullName'},
            'orderContact.telephone' => $tabSideInfos{'orderContact.telephone'}
        },
        'document.code'     => $tabSideInfos{'code'},
        'document.orderNo'  => $tabSideInfos{'orderNo'},
        'tabSite'           => {
            'label'      => $tabSideInfos{'site.label'},
            'address'    => $tabSideInfos{'site.address'},
            'locality'   => $tabSideInfos{'site.postalCode'} . ' ' . $tabSideInfos{'site.city'},
            'contact.fullName' => $tabSideInfos{'site.contact.fullName'},
            'contact.telephone'=> $tabSideInfos{'site.contact.telephone'},
            'isAsbestos' => $tabSideInfos{'site.isAsbestos'},
            'comments'   => $tabSideInfos{'site.comments'}
        },
        'tabOtherSite'      => {
            'label'      => $tabOtherSideInfos{'site.label'},
            'address'    => $tabOtherSideInfos{'site.address'},
            'locality'   => $tabOtherSideInfos{'site.postalCode'} . ' ' . $tabOtherSideInfos{'site.city'}
        },
        'tabAgency'         => \%tabAgencyInfos,
        'window'            => {
            'date'      => substr($tabSideInfos{'window'}->{'from'}, 0, 10),
            'beginHour' => substr($tabSideInfos{'window'}->{'from'}, 11, 5),
            'endHour'   => substr($tabSideInfos{'window'}->{'till'}, 11, 5)
        },
        'tabMachine'        => $tabMachineInfos
    };
}

1;