use utf8;
use open (':encoding(UTF-8)');

# Package: pending
# Affichage de la liste des transports en cours
package pending;


# Constants: Filtre sur l'état de réalisation
# FILTER_DOABLESTATE_DOABLE     - Réalisable
# FILTER_DOABLESTATE_NOTDOABLE  - Non réalisable
use constant
{
    FILTER_DOABLESTATE_DOABLE     => '1-0',
    FILTER_DOABLESTATE_NOTDOABLE  => '0-0'
};

# Constants: Source des données
# DATASRC_VIEW  - Vue (v_pendingtransport)
# DATASRC_TABLE - Table (f_pendingtransport)
use constant
{
    DATASRC_VIEW  => 1,
    DATASRC_TABLE => 2
};


use strict;
use File::Basename;

use LOC::Transport;
use LOC::AjaxTable;
use LOC::Area;
use LOC::Estimate::Rent;

# Function: viewAction
# Affichage de la liste des transports en cours
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $countryId    = $tabUserInfos->{'nomadCountry.id'};

    my $isTelematActived = (&LOC::Characteristic::getCountryValueByCode('TELEMAT', $countryId) != 0 ? 1 : 0);
    my $dataSource = &LOC::Characteristic::getCountryValueByCode('SRCTSPALERT', $countryId);

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'        => $tabUserInfos->{'locale.id'},
        'user.id'          => $tabUserInfos->{'id'},
        'user.agency.id'   => $tabUserInfos->{'nomadAgency.id'},
        'user.country.id'  => $tabUserInfos->{'nomadCountry.id'},
        'currencySymbol'   => $tabUserInfos->{'currency.symbol'},
        'timeZone'         => $tabSession->{'timeZone'},
        'isTelematActived' => $isTelematActived,
        'dataSource'       => $dataSource
    );

    # Type d'affichage
    my $displayType = &LOC::Request::getString('displayType', '');
    my $isSelf = ($tabParameters->{'_isSelf'} ? 1 : 0);
    my $hasMachineInventory = ($tabParameters->{'_hasMachineInventory'} ? 1 : 0);
    my $excelRowsLimit = &LOC::Characteristic::getAgencyValueByCode('LSTMACHXLSMAX',
                                                                    $tabUserInfos->{'nomadAgency.id'},
                                                                    undef, 0) * 1;


    # Récupération du nombre d'éléments par page
    my $pageSize   = &LOC::Request::getInteger('pageSize', 20);
    my $startIndex = &LOC::Request::getInteger('startIndex', 0);
    # Récupération des tris demandés
    my $tabSorts   = &LOC::Request::getArray('sorts', [{'col' => 'date', 'dir' => 'asc'}]);

    my $tabOptions = {};
    my $tabDefaultActivesFilters = {
        'type' => [LOC::Transport::TYPE_DELIVERY]
    };

    if ($displayType eq '')
    {
        $tabViewData{'isAgenciesFilterActived'} = 0;
        $tabViewData{'isAreasFilterActived'} = 0;

        my @tabNomadAgencies = values %{$tabUserInfos->{'tabAuthorizedAgencies'}};

        # Si l'utilisateur a accès à plusieurs agences alors on active le filtre sur les agences
        if (@tabNomadAgencies > 1)
        {
            my $nomadAgencyId = $tabUserInfos->{'nomadAgency.id'};
            my $nomadAreaId = $tabUserInfos->{'tabAuthorizedAgencies'}->{$tabUserInfos->{'nomadAgency.id'}}->{'area.id'};

            $tabViewData{'isAgenciesFilterActived'} = 1;
            $tabDefaultActivesFilters->{'agency.id'} = [$nomadAgencyId];

            # De même avec le filtre des régions si il a accès à plusieurs régions
            my @tabAgenciesFromOthersAreas = grep { $_->{'area.id'} != $nomadAreaId } @tabNomadAgencies;
            if (@tabAgenciesFromOthersAreas > 0 && &LOC::Area::getList(LOC::Util::GETLIST_COUNT, {'country' => $countryId}) > 0)
            {
                $tabViewData{'isAreasFilterActived'} = 1;
            }
        }

        # Filtre "état de réalisation" par défaut
        if (!$isSelf)
        {
            # Pour le groupe COMM
            $tabDefaultActivesFilters->{'doableState'} = [];
            if (&LOC::Util::in_array(LOC::User::GROUP_COMMERCIAL, $tabUserInfos->{'tabGroups'}) ||
                &LOC::Util::in_array(LOC::User::GROUP_AGENCYMANAGER, $tabUserInfos->{'tabGroups'}))
            {
                # non réalisable
                push(@{$tabDefaultActivesFilters->{'doableState'}}, FILTER_DOABLESTATE_NOTDOABLE);
            }
            # Pour le groupe TRANSP
            if (&LOC::Util::in_array(LOC::User::GROUP_TRANSPORT, $tabUserInfos->{'tabGroups'}))
            {
                # réalisable
                push(@{$tabDefaultActivesFilters->{'doableState'}}, FILTER_DOABLESTATE_DOABLE);
            }
        }
    }
    elsif ($displayType eq 'excel')
    {
        # Pour le fichier excel, on ne récupère pas les données sur les filtres
        $tabOptions->{'getFiltersData'} = 0;
        $tabOptions->{'rowsLimit'} = $excelRowsLimit;
    }


    # Récupération des filtres demandés
    my $tabActivesFilters = &LOC::Request::getHash('filters', $tabDefaultActivesFilters);

    # Récupération de la liste des transports et des filtres
    $tabViewData{'data'} = &_getData($countryId, $dataSource, $tabActivesFilters, $tabSorts,
                                     $startIndex, $pageSize, $isSelf, $hasMachineInventory, $tabUserInfos, $tabOptions);

    # Récupération des droits sur chaque ligne de transport
    if ($displayType ne 'excel')
    {
        foreach my $row (@{$tabViewData{'data'}->{'rows'}})
        {
            $row->{'tabRights'} = &LOC::Transport::getRights($countryId, $tabViewData{'user.agency.id'},
                                                                $row->{'id'}, $tabViewData{'user.id'});
        }
    }

    if ($tabViewData{'data'}->{'result'} eq 'ko')
    {
        exit;
    }


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;

    my $action = $hasMachineInventory ? 'viewMachineInventory' : ($isSelf ? 'viewSelf' : 'view');
    $tabViewData{'controlerAction'} = 'transport:pending:' . $action;
    $tabViewData{'isSelf'} = $isSelf;
    $tabViewData{'hasMachineInventory'} = $hasMachineInventory;
    if ($displayType eq '')
    {
        # Paramètres de tris et pagination
        $tabViewData{'pageSize'}              = $pageSize;
        $tabViewData{'startIndex'}            = $startIndex;
        $tabViewData{'sorts'}                 = $tabSorts;
        $tabViewData{'activesFilters'}        = $tabActivesFilters;
        $tabViewData{'defaultActivesFilters'} = $tabDefaultActivesFilters;

        $tabViewData{'excelRowsLimit'}        = $excelRowsLimit;


        # URL pour le fichier Excel
        $tabViewData{'data'}->{'excelUrl'} = &LOC::Request::createRequestUri($tabViewData{'controlerAction'}, {
                                                 'displayType' => 'excel',
                                                 'sorts'       => [{'col' => 'parkNumber', 'dir' => 'asc'}],
                                                 'filters'     => $tabActivesFilters,
                                                 'startIndex'  => 0,
                                                 'pageSize'    => 0
                                             });

        # Page HTML
        require $directory . '/view/pending/view.cgi';
    }
    elsif ($displayType eq 'json')
    {
        # URL pour le fichier Excel
        $tabViewData{'data'}->{'excelUrl'} = &LOC::Request::createRequestUri($tabViewData{'controlerAction'}, {
                                                 'displayType' => 'excel',
                                                 'sorts'       => [{'col' => 'parkNumber', 'dir' => 'asc'}],
                                                 'filters'     => $tabActivesFilters,
                                                 'startIndex'  => 0,
                                                 'pageSize'    => 0
                                             });

        # Trame JSON
        require $directory . '/view/pending/json.cgi';
    }
    elsif ($displayType eq 'excel')
    {
        # Fichier Excel
        require $directory . '/view/pending/excel.cgi';
    }
}

# Function: viewSelfAction
# Affichage de la liste des enlèvements sur place
sub viewSelfAction
{
    my $tabParameters = $_[0];
    $tabParameters->{'_isSelf'} = 1;

    # Appel de la liste des transports en cours
    &viewAction(@_);
}

# Function: viewMachineInventoryAction
# Affichage de la liste des transports avec un état des lieux
sub viewMachineInventoryAction
{
    my $tabParameters = $_[0];
    $tabParameters->{'_hasMachineInventory'} = 1;

    # Appel de la liste des transports en cours
    &viewAction(@_);
}

# Function: doAction
# Réalisation
sub doAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $countryId    = $tabUserInfos->{'nomadCountry.id'};


    # Variables pour la vue
    our %tabViewData = (
        'locale.id'       => $tabUserInfos->{'locale.id'},
        'user.id'         => $tabUserInfos->{'id'},
        'user.agency.id'  => $tabUserInfos->{'nomadAgency.id'},
        'user.country.id' => $tabUserInfos->{'nomadCountry.id'},
        'currencySymbol'  => $tabUserInfos->{'currency.symbol'},
        'timeZone'        => $tabSession->{'timeZone'}
    );

    # Type d'action
    my $action = &LOC::Request::getString('action', 'do');

    # Récupération des transports concernés
    my $tabTransports = &LOC::Request::getArray('transports', []);

    my $tabDone = [];
    my $tabUndoable = [];
    my $tabErrors = [];

    my $nbTransports = @$tabTransports;
    for (my $i = 0; $i < $nbTransports; $i++)
    {
        my $do;
        # Récupération des droits sur le transport
        my $tabRights = &LOC::Transport::getRights($countryId, $tabViewData{'user.agency.id'},
                                                   $tabTransports->[$i]->{'id'}, $tabViewData{'user.id'});

        # Transfert inter-agences
        if ($tabTransports->[$i]->{'type'} eq LOC::Transport::TYPE_AGENCIESTRANSFER)
        {
            # Réalisation
            if ($action eq 'do' && $tabRights->{'actions'}->{'do-agtsf'})
            {
                $do = &LOC::Transport::doAgenciesTransfer($tabTransports->[$i]->{'country.id'},
                                                          $tabTransports->[$i]->{'machine.id'}, undef,
                                                          $tabViewData{'user.id'});
            }
            # Annulation de réalisation
            elsif ($action eq 'undo' && $tabRights->{'actions'}->{'undo-agtsf'})
            {
                $do = &LOC::Transport::undoAgenciesTransfer($tabTransports->[$i]->{'country.id'},
                                                            $tabTransports->[$i]->{'machine.id'},
                                                            $tabViewData{'user.id'});
            }
        }
        # Transfert inter-chantiers
        elsif ($tabTransports->[$i]->{'type'} eq LOC::Transport::TYPE_SITESTRANSFER)
        {
            # Transfert inter-chantier complet
            if (!defined $tabTransports->[$i]->{'subtype'})
            {
                # Réalisation
                if ($action eq 'do' &&
                        (($tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'do-self'}) ||
                         (!$tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'do-std'})))
                {
                    $do = &LOC::Transport::doDocumentRecovery($tabTransports->[$i]->{'country.id'}, 'contract',
                                                              $tabTransports->[$i]->{'contract.id'}, 1, undef,
                                                              $tabViewData{'user.id'});
                }
                # Annulation de réalisation
                elsif ($action eq 'undo' &&
                        (($tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'undo-self'}) ||
                         (!$tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'undo-std'})))
                {
                    $do = &LOC::Transport::undoDocumentRecovery($tabTransports->[$i]->{'country.id'}, 'contract',
                                                                $tabTransports->[$i]->{'contract.id'}, 1,
                                                                $tabViewData{'user.id'});
                }
            }
            # Livraison du transfert inter-chantiers
            elsif ($tabTransports->[$i]->{'subtype'} eq LOC::Transport::TYPE_DELIVERY)
            {
                # Réalisation
                if ($action eq 'do' &&
                        (($tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'do-self'}) ||
                         (!$tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'do-std'})))
                {
                    $do = &LOC::Transport::doDocumentDelivery($tabTransports->[$i]->{'country.id'}, 'contract',
                                                              $tabTransports->[$i]->{'contract.id'}, 1, undef,
                                                              $tabViewData{'user.id'});
                }
                # Annulation de réalisation
                elsif ($action eq 'undo' &&
                        (($tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'undo-self'}) ||
                         (!$tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'undo-std'})))
                {
                    $do = &LOC::Transport::undoDocumentDelivery($tabTransports->[$i]->{'country.id'}, 'contract',
                                                                $tabTransports->[$i]->{'contract.id'}, 0,
                                                                $tabViewData{'user.id'});
                }
            }
            # Récupération du transfert inter-chantiers
            elsif ($tabTransports->[$i]->{'subtype'} eq LOC::Transport::TYPE_RECOVERY)
            {
                # Réalisation
                if ($action eq 'do' &&
                        (($tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'do-self'}) ||
                         (!$tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'do-std'})))
                {
                    $do = &LOC::Transport::doDocumentRecovery($tabTransports->[$i]->{'country.id'}, 'contract',
                                                              $tabTransports->[$i]->{'contract.id'}, 0, undef,
                                                              $tabViewData{'user.id'});
                }
                # Annulation de réalisation
                elsif ($action eq 'undo' &&
                        (($tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'undo-self'}) ||
                         (!$tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'undo-std'})))
                {
                    $do = &LOC::Transport::undoDocumentRecovery($tabTransports->[$i]->{'country.id'}, 'contract',
                                                                $tabTransports->[$i]->{'contract.id'}, 0,
                                                                $tabViewData{'user.id'});
                }
            }
        }
        # Livraison
        elsif ($tabTransports->[$i]->{'type'} eq LOC::Transport::TYPE_DELIVERY)
        {
            # Réalisation
            if ($action eq 'do' &&
                    (($tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'do-self'}) ||
                     (!$tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'do-std'})))
            {
                $do = &LOC::Transport::doDocumentDelivery($tabTransports->[$i]->{'country.id'}, 'contract',
                                                          $tabTransports->[$i]->{'contract.id'}, 0, undef,
                                                          $tabViewData{'user.id'});
            }
            # Annulation de réalisation
            elsif ($action eq 'undo' &&
                        (($tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'undo-self'}) ||
                         (!$tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'undo-std'})))
            {
                $do = &LOC::Transport::undoDocumentDelivery($tabTransports->[$i]->{'country.id'}, 'contract',
                                                            $tabTransports->[$i]->{'contract.id'}, 0,
                                                            $tabViewData{'user.id'});
            }
        }
        # Récupération
        elsif ($tabTransports->[$i]->{'type'} eq LOC::Transport::TYPE_RECOVERY)
        {
            # Réalisation
            if ($action eq 'do' &&
                    (($tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'do-self'}) ||
                     (!$tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'do-std'})))
            {
                $do = &LOC::Transport::doDocumentRecovery($tabTransports->[$i]->{'country.id'}, 'contract',
                                                          $tabTransports->[$i]->{'contract.id'}, 0, undef,
                                                          $tabViewData{'user.id'});
            }
            # Annulation de réalisation
            elsif ($action eq 'undo' &&
                        (($tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'undo-self'}) ||
                         (!$tabTransports->[$i]->{'self'} && $tabRights->{'actions'}->{'undo-std'})))
            {
                $do = &LOC::Transport::undoDocumentRecovery($tabTransports->[$i]->{'country.id'}, 'contract',
                                                            $tabTransports->[$i]->{'contract.id'}, 0,
                                                            $tabViewData{'user.id'});
            }
        }

        # Réalisation sans erreur
        if ($do)
        {
            push(@$tabDone, $tabTransports->[$i]);
            my $tabTransport = &LOC::Transport::getInfos($countryId, $tabTransports->[$i]->{'id'});
            if (!$tabTransport->{'isLocked'})
            {
                push(@$tabUndoable, $tabTransports->[$i]);
            }
        }
        # Erreur lors de la réalisation
        else
        {
            push(@$tabErrors, $tabTransports->[$i]);
        }
    }

    $tabViewData{'data'} = {'done' => $tabDone, 'undoable' => $tabUndoable, 'errors' => $tabErrors};

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;

    require $directory . '/view/pending/do.cgi';
}


# Function: _getData
# Récupérer les données du tableau et les filtres
#
# Parameters:
# string   $countryId         - Pays
# int      $dataSource        - Source des données (DATASRC_VIEW ou DATASRC_TABLE)
# hashref  $tabActivesFilters - Filtres actifs
# arrayref $tabSorts          - Tris
# int      $startIndex        - Index de la ligne de départ à récupérer
# int      $pageSize          - Nombre de lignes à récupérer
# int      $isSelf            - Indique si on est dans l'alerte des transports en cours ou des enlèvements en attentes
# int      $hasMachineInventory - Indique si on est dans l'alerte des transports avec état des lieux
# hashref  $tabUserInfos      - Informations sur l'utilisateur
# hashref  $tabOptions        - Options supplémentaires
#
# Returns:
# hashref - Données du tableau et des filtres
sub _getData
{
    my ($countryId, $dataSource, $tabActivesFilters, $tabSorts, $startIndex, $pageSize, $isSelf, $hasMachineInventory, $tabUserInfos, $tabOptions) = @_;


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Filtres disponibles
    my %tabFiltersCfgs = ();

    if (!$hasMachineInventory)
    {
        %tabFiltersCfgs = (
            'id' => {
                'type'    => 'list',
                'hidden'  => 1,
                'select'  => {
                    'value' => 'pdt_id'
                },
                'where'   => 'pdt_id IN (<%values>)'
            },

            'area.id'    => {
                'type'    => 'list',
                'select'  => {
                    'value' => 'IFNULL(`pdt_are_id`, "")',
                    'label' => ($dataSource == DATASRC_VIEW ? 'IFNULL(`pdt_arealabel`, "")' : 'IFNULL(`tbl_p_delegatedarea`.`are_label`, "")')
                },
                'where'   => 'IFNULL(`pdt_are_id`, "") IN (<%values>)',
                'values' => {
                    'select' => {
                        'value' => ($dataSource == DATASRC_VIEW ? 'IFNULL(are_id, "")' : 'IFNULL(frmwrk.p_area.are_id, "")'),
                        'label' => ($dataSource == DATASRC_VIEW ? 'IFNULL(are_label, "")' : 'IFNULL(frmwrk.p_area.are_label, "")')
                    },
                    'table' => 'frmwrk.p_area',
                    'where' => ($dataSource == DATASRC_VIEW ? 'IFNULL(are_id, "") IN (<%values>)' : 'IFNULL(frmwrk.p_area.are_id, "") IN (<%values>)')
                }
            },
    
            'agency.id'    => {
                'type'    => 'list',
                'select'  => {
                    'value' => 'pdt_agc_id'
                },
                'where'   => 'pdt_agc_id IN (<%values>)',
                'values' => {}
            },
    
            'type'    => {
                'type'    => 'list',
                'select'  => {
                    'value' => 'pdt_type'
                },
                'where'   => 'pdt_type IN (<%values>)'
            },
    
            'doableState' => {
                'type'    => 'list',
                'select'  => {
                    'value' => 'CONCAT(pdt_is_doable, "-", pdt_errors)'
                },
                'where'   => sub {
                    my ($tabValues) = @_;
    
                    my @tabWheres = ();
                    foreach my $value (@$tabValues)
                    {
                        my ($isDoable, $errors) = split('-', $value);
    
                        my $where = 'pdt_is_doable = ' . $isDoable;
                        if ($errors * 1 > 0)
                        {
                            $where .= ' AND pdt_errors & ' . $errors;
                        }
    
                        push(@tabWheres, '(' . $where . ')');
                    }
    
                    return join(' OR ', @tabWheres);
                },
                'values' => {}
            },
    
            'order.code'    => {
                'type'    => 'list',
                'select'  => {
                    'value' => ($dataSource == DATASRC_VIEW ? 'pdt_ordercode' : 'f_transportorder.tor_code')
                },
                'where'   => ($dataSource == DATASRC_VIEW ? 'pdt_ordercode IN (<%values>)' : 'f_transportorder.tor_code IN (<%values>)'),
                'func'    => sub {
                    my ($tabRows, $tabCfgs, $values) = @_;
    
                    # Suppression de la valeur vide
                    my $i = 0;
                    while ($i < @$tabRows)
                    {
                        if ($tabRows->[$i]->{'value'} eq '')
                        {
                            splice(@$tabRows, $i, 1);
                        }
                        else
                        {
                            $i++;
                        }
                    }
    
                    return $tabRows;
                }
            },
    
            'dateDiff'    => {
                'type'    => 'list',
                'options' => {
                    'operator' => 1
                },
                'select'  => {
                    'value' => 'DATEDIFF(pdt_date, Now())'
                },
                'where'   => 'DATEDIFF(pdt_date, Now()) <%op> <%values>',
                'values'  => {}
            },
    
            'from-address'    => {
                'type'    => 'list',
                'select'  => {
                    'value' => 'CONCAT(pdt_fromlabel, " - ", pdt_fromaddress)'
                },
                'where'   => 'CONCAT(pdt_fromlabel, " - ", pdt_fromaddress) IN (<%values>)'
            },
    
            'to-address'    => {
                'type'    => 'list',
                'select'  => {
                    'value' => 'CONCAT(pdt_tolabel, " - ", pdt_toaddress)'
                },
                'where'   => 'CONCAT(pdt_tolabel, " - ", pdt_toaddress) IN (<%values>)'
            },
    
            'customer.id'    => {
                'type'    => 'list',
                'select'  => {
                    'value' => 'pdt_cus_id',
                    'label' => 'pdt_customerlabel',
                    'extra' => 'pdt_cus_id_link AS `link.value`, pdt_linkcustomerlabel AS `link.label`'
                },
                'where'   => 'pdt_cus_id IN (<%values>) OR pdt_cus_id_link IN (<%values>)',
                'func'    => sub {
                    my ($tabRows, $tabCfgs, $values) = @_;
    
                    # Fusion des éléments des clients issus des transferts avec les autres
                    my %tabHash = ();
                    foreach my $row (@$tabRows)
                    {
                        if ($row->{'link.value'} ne '' && !exists $tabHash{$row->{'link.value'}})
                        {
                            if (!defined $values || &LOC::Util::in_array($row->{'link.value'}, $values))
                            {
                                $tabHash{$row->{'link.value'}} = {
                                    'value' => $row->{'link.value'},
                                    'label' => $row->{'link.label'}
                                };
                            }
                        }
    
                        if ($row->{'value'} ne '' && (!defined $values || &LOC::Util::in_array($row->{'value'}, $values)))
                        {
                            $tabHash{$row->{'value'}} = {
                                'value' => $row->{'value'},
                                'label' => $row->{'label'}
                            };
                        }
                    }
    
                    my @tabRows2 = sort {$a->{'label'} cmp $b->{'label'}} values %tabHash;
    
                    return \@tabRows2;
                }
            },
    
            'machine.parkNumber' => {
                'type'    => 'search',
                'where'   => ($dataSource == DATASRC_VIEW ? 'CAST(pdt_machineparknumber AS CHAR) = <%values> OR ' . 
                                                            'CONCAT(CONVERT(pdt_machineparknumberprefix USING "utf8"), pdt_machineparknumber) = <%values>'
                                                          : 'CAST(tbl_f_machine.MANOPARC AS CHAR) = <%values> OR ' .
                                                            'CONCAT(CONVERT(tbl_f_machine.MANOPARC_AUX USING "utf8"), tbl_f_machine.MANOPARC) = <%values>')
            }
    
        );
    }


    # ***********************************************************************************************************
    # Récupération de la liste
    # ***********************************************************************************************************

    # Colonnes disponibles
    my %tabColumnsCfgs = (
        'id' => {
            'select' => 'pdt_id'
        },
        'agency.id' => {
            'select' => 'pdt_agc_id'
        },
        'type' => {
            'select' => 'pdt_type'
        },
        'order.code' => ($dataSource == DATASRC_VIEW ? {
            'select' => 'pdt_ordercode'
        } : {
            'select' => 'tor_code',
            'table'  => 'f_transportorder'
        }),
        'date' => {
            'select' => 'pdt_date'
        },
        'linkDate' => {
            'select' => 'pdt_date_link'
        },
        'hour' => {
            'select' => 'pdt_hour'
        },
        'linkHour' => {
            'select' => 'pdt_linkhour'
        },
        'contract.id' => {
            'select' => 'pdt_rct_id'
        },
        'estimateLine.id' => {
            'select' => 'pdt_rel_id'
        },
        'document.type' => {
            'select' => 'pdt_documenttype'
        },
        'document.code' => {
            'select' => 'pdt_documentcode'
        },
        'document.index' => {
            'select' => 'pdt_documentindex'
        },
        'document.state.id' => {
            'select' => 'pdt_rct_sta_id'
        },
        'document.isAsbestos' => {
            'select' => 'pdt_is_asbestosdocument'
        },
        'linkContract.id' => {
            'select' => 'pdt_rct_id_link'
        },
        'linkContract.code' => {
            'select' => 'pdt_linkcontractcode'
        },
        'linkContract.state.id' => {
            'select' => 'pdt_rct_sta_id_link'
        },
        'linkContract.isAsbestos' => {
            'select' => 'pdt_is_asbestoslink'
        },
        'from.label' => {
            'select' => 'pdt_fromlabel',
            'orderBy' => 'pdt_fromaddress <%dir>'
        },
        'from.address' => {
            'select' => 'pdt_fromaddress'
        },
        'to.label' => {
            'select' => 'pdt_tolabel',
            'orderBy' => 'pdt_toaddress <%dir>'
        },
        'to.address' => {
            'select' => 'pdt_toaddress'
        },
        'machine.id' => {
            'select' => 'pdt_mac_id'
        },
        'machine.parkNumberPrefix' => {
            'select' => ($dataSource == DATASRC_VIEW ? 'pdt_machineparknumberprefix' : 'IFNULL(tbl_f_machine.MANOPARC_AUX, "")')
        },
        'machine.parkNumber' => ($dataSource == DATASRC_VIEW ? {
            'select' => 'pdt_machineparknumber'
        } : {
            'select' => 'MANOPARC',
            'table'  => 'tbl_f_machine'
        }),
        'model.label' => ($dataSource == DATASRC_VIEW ? {
            'select' => 'pdt_modellabel'
        } : {
            'select' => 'MOMADESIGNATION',
            'table'  => 'tbl_f_model'
        }),
        'customer.id' => {
            'select' => 'pdt_cus_id'
        },
        'customer.label' => {
            'select' => 'pdt_customerlabel',
            'orderBy' => 'pdt_customerlabel <%dir>, pdt_linkcustomerlabel <%dir>'
        },
        'linkCustomer.id' => {
            'select' => 'pdt_cus_id_link'
        },
        'linkCustomer.label' => {
            'select' => 'pdt_linkcustomerlabel'
        },
        'isDone' => {
            'select' => 'pdt_is_done'
        },
        'isDoable' => {
            'select' => 'pdt_is_doable'
        },
        'isLinkDone' => {
            'select' => 'pdt_is_linkdone'
        },
        'isSelf' => {
            'select' => 'pdt_is_self'
        },
        'errors' => {
            'select' => 'pdt_errors'
        },
        'isWithTelematic' => {
            'select' => 'pdt_is_machinewithtelematic'
        }
    );

    # Jointures
    my @tabJoinsCfgs = ();
    if ($dataSource != DATASRC_VIEW)
    {
        @tabJoinsCfgs = (
            {
                'type'  => 'LEFT',
                'table' => 'frmwrk.p_area',
                'alias' => 'tbl_p_delegatedarea',
                'on'    => '`tbl_p_delegatedarea`.`are_id` = `pdt_are_id`'
            },
            {
                'type'  => 'LEFT',
                'table' => 'AUTH.MODELEMACHINE',
                'alias' => 'tbl_f_model',
                'on'    => '`tbl_f_model`.`MOMAAUTO` = `pdt_mod_id`'
            },
            {
                'type'  => 'LEFT',
                'table' => 'AUTH.MACHINE',
                'alias' => 'tbl_f_machine',
                'on'    => '`tbl_f_machine`.`MAAUTO` = `pdt_mac_id`'
            },
            {
                'type'  => 'LEFT',
                'table' => 'f_transportorder',
                'on'    => '`f_transportorder`.`tor_id` = `pdt_tor_id`'
            }
        );
    }

    # Conditions permanentes
    my @tabQueryFirstWheres = ();
    push(@tabQueryFirstWheres, 'pdt_is_self = ' . $isSelf);

    # Filtre "État des lieux"
    if ($hasMachineInventory)
    {
        push(@tabQueryFirstWheres, '(SELECT COUNT(min_id)
                FROM f_machineinventory
                WHERE ((min_type = pdt_type AND min_rct_id_attachedto = pdt_rct_id) OR
                         (pdt_type = "' . LOC::Transport::TYPE_SITESTRANSFER . '" AND
                            ((min_type = "' . LOC::Transport::TYPE_RECOVERY . '" and min_rct_id_attachedto = pdt_rct_id) OR 
                             (min_type = "' . LOC::Transport::TYPE_DELIVERY . '" and min_rct_id_attachedto = pdt_rct_id_link))
                        ))) > 0');
    }


    # Filtres actifs par défaut
    my @tabAutorAgencies = keys %{$tabUserInfos->{'tabAuthorizedAgencies'}};
    push(@tabQueryFirstWheres, 'pdt_agc_id IN (' . $db->quote(\@tabAutorAgencies) . ')');

    my $table = ($dataSource == DATASRC_VIEW ? 'v_pendingtransport' : 'f_pendingtransport');

    return &LOC::AjaxTable::getData($db, $table, \@tabJoinsCfgs,
                                    \%tabColumnsCfgs, \%tabFiltersCfgs,
                                    \@tabQueryFirstWheres, $tabActivesFilters,
                                    $tabSorts, $startIndex, $pageSize, $tabOptions);
}


1;
