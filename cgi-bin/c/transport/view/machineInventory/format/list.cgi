use utf8;
use open (':encoding(UTF-8)');

use strict;


sub formatList
{
    my ($countryId, $tabData, $locale, $tabOptions) = @_;

    # Liste des choix pour le filtre des types d'état des lieux
    my %tabTypesLabels = (
        LOC::MachineInventory::TYPE_DELIVERY         => $locale->t('Livraison'),
        LOC::MachineInventory::TYPE_RECOVERY         => $locale->t('Récupération')
    );

    # Liste des choix pour le filtre des statuts
    my @tabStatusLabels = (
        {
            'value' => 0,
            'label' => $locale->t('Détaché')
        },
        {
            'value' => LOC::MachineInventory::STATUSFLAG_UNKNOWNMACHINE,
            'label' => $locale->t('Machine inconnue')
        },
        {
            'value' => LOC::MachineInventory::STATUSFLAG_UNKNOWNCONTRACT,
            'label' => $locale->t('Contrat inconnu')
        },
        {
            'value' => LOC::MachineInventory::STATUSFLAG_INVALIDCOUPLE,
            'label' => $locale->t('Association contrat/machine incohérente')
        }
    );

    # Fonction d'ajout des libellés dans les listes sans libellés
    my $addLabels = sub {
        my ($tab, $tabLabels) = @_;

        foreach my $row (@$tab)
        {
            $row->{'label'} = $tabLabels->{$row->{'value'}};
        }
        my @tab2 = sort {$a->{'label'} cmp $b->{'label'}} @$tab;

        return \@tab2;
    };



    # Formatage des lignes
    foreach my $row (@{$tabData->{'rows'}})
    {
        # Libellé du type
        $row->{'type.label'} = $tabTypesLabels{$row->{'type'}};

        # Date de saisie
        if ($row->{'date'} ne '')
        {
            $row->{'hour'} = $locale->getDateFormat($row->{'date'}, LOC::Locale::FORMAT_TIMESHORT_NUMERIC);
            $row->{'date'} = $locale->getDateFormat($row->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC);

        }

        # Statuts
        $row->{'status.list'} = [];
        foreach my $status (@tabStatusLabels)
        {
            my $error = $status->{'value'};
            if (($row->{'status'} * 1 & $error * 1) == $error * 1)
            {
                push(@{$row->{'status.list'}}, $error);
            }
        }

        # Lien vers la machine
        if ($row->{'machine.id'})
        {
            my $tabDisplayMachineInfos = &LOC::Machine::getDisplayInfos($row->{'machine.parkNumber'});
            $row->{'machine.url'}             = $tabDisplayMachineInfos->{'url'};
            $row->{'machine.isWithTelematic'} = $tabDisplayMachineInfos->{'isWithTelematic'};
        }

        # Lien vers le contrat
        if ($row->{'contract.id'})
        {
            $row->{'contract.url'} = &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $row->{'contract.id'}});

        }

        # Lien vers l'état des lieux
        $row->{'url'} = &LOC::Request::createRequestUri('transport:machineInventory:view', {'id'   => $row->{'id'},
                                                                                            'from' => 'alert'});
    }

    # Formatage des filtres
    if ($tabData->{'filters'})
    {
        my $tabFilters = $tabData->{'filters'};

        # Filtres sur les types de transport
        if (exists $tabFilters->{'type'})
        {
            $tabFilters->{'type'}->{'list'} = &$addLabels($tabFilters->{'type'}->{'list'}, \%tabTypesLabels);
            if (exists $tabFilters->{'type'}->{'values'})
            {
                $tabFilters->{'type'}->{'values'} = &$addLabels($tabFilters->{'type'}->{'values'}, \%tabTypesLabels);
            }
        }

        # Filtre sur le statut
        if (exists $tabFilters->{'status'})
        {
            my @tabNewList = ();

            my %tabLabels = ();
            foreach my $row (@tabStatusLabels)
            {
                my $error = $row->{'value'};

                my $expr = sub {
                    my $err = $_[0]->{'value'};
                    my $result = (($err * 1 & $error * 1) == $error * 1 ? 1 : 0);
                    return $result;
                };

                my @tab = grep &$expr($_), @{$tabFilters->{'status'}->{'list'}};
                if (@tab > 0)
                {
                    push(@tabNewList, $row);
                }

                $tabLabels{$error} = $row->{'label'};
            }

            if (exists $tabFilters->{'status'}->{'values'})
            {
                foreach my $row (@{$tabFilters->{'status'}->{'values'}})
                {
                    $row->{'label'} = $tabLabels{$row->{'value'}};
                }
            }

            $tabFilters->{'status'}->{'list'} = \@tabNewList;
        }
    }
}


1;
