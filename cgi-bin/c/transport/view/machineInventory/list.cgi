use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'transport/machineInventory/';


our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});


sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}



# CSS
$view->addCSSSrc($dir . 'list.css');
$view->addCSSSrc('location.css');

# JS
$view->addJSSrc($dir . 'list.js');
# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc('jQuery/plugins/ajaxTable.js');
$view->addCSSSrc('jQuery/plugins/ajaxTable.css');

# Ajout des packages
$view->addPackage('modalwindow');

# Affichage de l'entête
$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_NOTIFS);

my $title = $locale->t('États des lieux à traiter');
$view->setPageTitle($title);
$view->setTitle($title);


# Traductions
$view->addTranslations({
    'edit'       => $locale->t('Modifier l\'état des lieux'),
    'parkNumber' => $locale->t('N° de parc'),
    'contract'   => $locale->t('Contrat'),
    'noValue'    => $locale->t('Aucun'),
    'differentValue' => $locale->t('(saisi : %s)', '<%value>')
});


# Constantes en JavaScript
my $tabErrorTypes = [
    {
        'code'         => 'unknown-machine',
        'value'        => LOC::MachineInventory::STATUSFLAG_UNKNOWNMACHINE,
        'title'        => $locale->t('Machine inconnue')
    },
    {
        'code'         => 'unknown-contract',
        'value'        => LOC::MachineInventory::STATUSFLAG_UNKNOWNCONTRACT,
        'title'        => $locale->t('Contrat inconnu')
    },
    {
        'code'         => 'invalid-couple',
        'value'        => LOC::MachineInventory::STATUSFLAG_INVALIDCOUPLE,
        'title'        => $locale->t('Association contrat/machine incohérente')
    }
];

$view->addJSSrc('
/* Erreurs */
var tabErrorTypes = ' . &LOC::Json::toJson($tabErrorTypes) . ';

// Récupérer l\'instance
function getListObj()
{
    return machineInventories;
}
');

# Légende des cas d'erreurs
my $legendHtml = '<div class="legend">' .
                    '<span class="title">' . htmllocale('Légende pour les cas erronés') . '&nbsp;:</span>';
foreach my $tabError (@$tabErrorTypes)
{
    $legendHtml .= '<div class="button ' . $tabError->{'code'} . '">' .
                        '<div class="errorImg ' . $tabError->{'code'} . '"></div>' .
                        '<div class="text"><div class="pick"></div>' . $view->toHTMLEntities($tabError->{'title'}) . '</div>' .
                   '</div>';
}
$legendHtml .= '</div>';

$view->addControlsContent([$legendHtml], 'left', 1);

# Bouton "Actualiser"
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png',
                                          '',
                                          $locale->t('Actualiser la page'),
                                          'l',
                                          {'id' => 'refresh-btn'});
$view->addControlsContent([$refreshBtn], 'right', 1);



$view->{'_networkOptimization'} = 'transport-machineInventory-list';
print $view->displayHeader();


# Bouton de réinitialisation des filtres
my $resetFiltersBtn = $view->displayTextButton($locale->t('Réinitialiser'), 'common/ajaxTable/reset.png',
                                               'return true;',
                                               '',
                                               'l',
                                               {'class' => 'locCtrlButton', 'id' => 'reset-filters-btn'});


print '
<fieldset id="table-filters-container" class="filters">
    <legend>' . htmllocale('Filtres') . '</legend>
    
    <ul class="content">
        <li class="part">
            <ul>
                <li id="filter-area"></li>
                <li id="filter-agency"></li>
            </ul>
        </li>
        <li class="part">
            <ul>
                <li id="filter-type"></li>
                <li id="filter-user"></li>
                <li id="filter-status"></li>
            </ul>
        </li>
    </ul>
    <div class="ctrls">
        ' . $resetFiltersBtn . '
    </div>
</fieldset>

<div class="list">
    <div id="table-toolbar-container" class="toolbar header">
        <div class="col content">
            <div id="header-pagination-container"></div>
            <div class="fixed-content">
                <div id="header-filters-recap"></div>
            </div>
        </div>
        <div class="col controls">
            <div class="fixed-content">
                <a href="#table-filters-container" id="back-top-btn" title="' . htmllocale('Retourner en haut de la page') . '">' .
                    $view->displayImage('common/ajaxTable/up.png') .
               '</a>
            </div>
        </div>
    </div>
    <div class="content" id="table-container"></div>
</div>';


# Formatage de la liste
my $tabData = $tabViewData{'data'};
require($directory . '/machineInventory/format/list.cgi');

# Formatage des données de l'onglet Durée
&formatList($tabViewData{'user.country.id'}, $tabData, $locale, {});


my @tabColumnsOptions = ();

# Colonne avec l'identifiant de l'état des lieux
push(@tabColumnsOptions, {
    'id'         => 'id',
    'title'      => $locale->t('N°'),
    'cssClasses' => 'id',
    'sort'       => 1
});

# Colonne avec l'agence responsable
push(@tabColumnsOptions, {
    'id'         => 'agency.id',
    'title'      => $locale->t('Ag.'),
    'tip'        => $locale->t('Agence'),
    'cssClasses' => 'agency',
    'sort'       => 1
});

# Colonne avec le type d'état des lieux
push(@tabColumnsOptions, {
    'id'         => 'type',
    'title'      => $locale->t('Type'),
    'cssClasses' => 'type',
    'sort'       => 1
});

# Colonne avec le nom du chauffeur
push(@tabColumnsOptions, {
    'id'         => 'user.fullName',
    'title'      => $locale->t('Chauffeur'),
    'cssClasses' => 'user',
    'sort'       => 1
});

# Colonne avec la date de saisie de l'état des lieux
push(@tabColumnsOptions, {
    'id'         => 'date',
    'title'      => $locale->t('Date'),
    'cssClasses' => 'date',
    'sort'       => 1
});

# Colonne avec les informations à traiter
push(@tabColumnsOptions, {
    'id'         => 'data',
    'title'      => $locale->t('Informations'),
    'cssClasses' => 'data',
    'sort'       => 0
});

# Colonne avec l'état
push(@tabColumnsOptions, {
    'id'         => 'status',
    'title'      => $locale->t('Statut'),
    'cssClasses' => 'status',
    'sort'       => 1
});

# Colonne avec le bouton pour ouvrir la popup d'état des lieux
push(@tabColumnsOptions, {
    'id'         => 'edit',
    'title'      => '',
    'cssClasses' => 'edit',
    'sort'       => 0
});

# Paramètrage des filtres de recherche
my $updateFilterDelay = 600;
my @tabFiltersOptions = ();

# Filtre sur les régions
push(@tabFiltersOptions, {
    'id' => 'area.id',
    'container' => '#filter-area',
    'type' => 'list',
    'options' => {
        'delay' => $updateFilterDelay
    },
    'labels' => {
        'title'       => $locale->t('Région(s)'),
        'inactive'    => $locale->t('Toutes les régions'),
        'noSelection' => $locale->t('Choisir les régions'),
        'selections'  => $locale->t('Les régions de %s', '<%values>'),
        'selection'   => $locale->t('La région de %s', '<%value>'),
        'emptyValue'  => $locale->t('Sans région'),
        'noItemAvlb'  => $locale->t('Aucune région disponible')
    }
});

# Filtre sur les agences
push(@tabFiltersOptions, {
    'id' => 'agency.id',
    'container' => '#filter-agency',
    'type' => 'list',
    'options' => {
        'delay' => $updateFilterDelay
    },
    'labels' => {
        'title'       => $locale->t('Agence(s)'),
        'inactive'    => $locale->t('Toutes les agences'),
        'noSelection' => $locale->t('Choisir les agences'),
        'selections'  => $locale->t('Les agences de %s', '<%values>'),
        'selection'   => $locale->t('L\'agence de %s', '<%value>'),
        'emptyValue'  => $locale->t('Sans agence'),
        'noItemAvlb'  => $locale->t('Aucune agence disponible')
    }
});

# Filtre sur les types d'état des lieux
push(@tabFiltersOptions, {
    'id' => 'type',
    'container' => '#filter-type',
    'type' => 'list',
    'options' => {
        'delay' => $updateFilterDelay
    },
    'labels' => {
        'title'       => $locale->t('Type(s) d\'état des lieux'),
        'inactive'    => $locale->t('Tous les types d\'état des lieux'),
        'noSelection' => $locale->t('Choisir les types d\'état des lieux'),
        'selections'  => $locale->t('Les types d\'état des lieux %s', '<%values>'),
        'selection'   => $locale->t('Le type d\'état des lieux %s', '<%value>'),
        'noItemAvlb'  => $locale->t('Aucun type d\'état des lieux disponible')
    }
});
# Filtre sur les chauffeurs
push(@tabFiltersOptions, {
    'id' => 'user.id',
    'container' => '#filter-user',
    'type' => 'list',
    'options' => {
        'delay'  => $updateFilterDelay,
        'search' => 1
    },
    'labels' => {
        'title'        => $locale->t('Chauffeur(s)'),
        'inactive'     => $locale->t('Tous les chauffeurs'),
        'noSelection'  => $locale->t('Choisir les chauffeurs'),
        'selections'   => $locale->t('Les chauffeurs %s', '<%values>'),
        'selection'    => $locale->t('Le chauffeur %s', '<%value>'),
        'noItemAvlb'   => $locale->t('Aucun chauffeur disponible'),
        'search'       => $locale->t('Rechercher un chauffeur'),
        'clearSearch'  => $locale->t('Vider la saisie'),
        'noSearchRslt' => $locale->t('Pas de correspondance')
    }
});

# Filtre sur les adresses de destination
push(@tabFiltersOptions, {
    'id' => 'status',
    'container' => '#filter-status',
    'type' => 'list',
    'options' => {
        'delay' => $updateFilterDelay,
    },
    'labels' => {
        'title'        => $locale->t('Statut(s)'),
        'inactive'     => $locale->t('Tous les statuts'),
        'noSelection'  => $locale->t('Choisir les statuts'),
        'selections'   => $locale->t('Les statuts %s', '<%values>'),
        'selection'    => $locale->t('Le statut %s', '<%value>'),
        'noItemAvlb'   => $locale->t('Aucun statut disponible')
    }
});


# Paramètrage du tableau
my %tabAjaxTableOptions = (
    'url'            => $view->createURL($tabViewData{'controllerAction'}, {'displayType' => 'json'}),
    'cssClasses'     => 'basic list',
    'startIndex'     => $tabViewData{'startIndex'},
    'pageSize'       => $tabViewData{'pageSize'},
    'sorts'          => $tabViewData{'sorts'},
    'activesFilters' => $tabViewData{'activesFilters'},
    'defaultActivesFilters' => $tabViewData{'defaultActivesFilters'},
    'data'           => $tabData,
    'paginations'    => '#header-pagination-container',
    'filtersRecaps'  => '#header-filters-recap',
    'columns'        => \@tabColumnsOptions,
    'filters'        => \@tabFiltersOptions,
    'labels'         => {
        'pagination' => {
            'noResult' => $locale->t('Pas de résultat'),
            'result'   => $locale->t('%s résultat', '<b>1</b>'),
            'results'  => $locale->t('%s-%s sur %s résultats', '<b><%start></b>', '<b><%end></b>', '<b><%total></b>')
        },
        'clearSel' => $locale->t('Vider la sélection'),
        'errorMsg' => $locale->t('Une erreur s\'est produite.Veuillez contacter le service informatique s.v.p.'),
        'noResult' => $locale->t('Aucun état des lieux trouvé')
    }
);


# Variables à passer au JS
print $view->displayJSBlock('
var isTelematActived = ' . $tabViewData{'isTelematActived'} . ';');

# Données à passer au JavaScript
print $view->displayJSBlock('
$(function()
{
    machineInventories.init(' . &LOC::Json::toJson({
        'tableOptions'   => \%tabAjaxTableOptions,
        'countryId'      => $tabViewData{'user.country.id'}
    }) . ');
});');


# Affichage du pied de page
print $view->displayFooter();

