use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Json;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


our %tabViewData;
our $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});

# Dates et heures au format local
foreach my $data (@{$tabViewData{'tabPhotos'}})
{
    $data->{'date'} = (defined $data->{'date'} ? $locale->getDateFormat($data->{'date'}, LOC::Locale::FORMAT_DATETIME_NUMERIC) : undef);
}


my $view = LOC::Json->new();
print $view->displayHeader();
print $view->encode(\%tabViewData);
