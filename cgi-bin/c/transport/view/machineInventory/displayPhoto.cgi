use open (':encoding(UTF-8)');
binmode STDOUT, ":raw";    # Pour envoyer le flux en l'état
use bytes;                 # Pour avoir la vraie longueur en octets

use strict;

use LOC::Locale;
use LOC::Browser;

our %tabViewData;

# Locale
our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});

if (defined $tabViewData{'errors'})
{
    &LOC::Browser::sendHeaders("Status: 404\n\n");
    print $locale->t('Le fichier n\'existe pas !');
}
else
{
    my $fileInfos = $tabViewData{'fileInfos'};

    &LOC::Browser::sendHeaders(
            "Content-Type: " . $fileInfos->{'mimeType'} . "\n" .
            "Content-Disposition: inline; filename=\"" . $fileInfos->{'fileName'} . "\"\n" .
            "Content-Length: " . length($fileInfos->{'fileContent'}) . "\n" .
            "\n" .
            $fileInfos->{'fileContent'}
        );
}
