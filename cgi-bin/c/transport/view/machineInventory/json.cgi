use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Json;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


our %tabViewData;

my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
my $view = LOC::Json->new();




## Formatage de la liste
my $tabData = $tabViewData{'data'};
require($directory . '/machineInventory/format/list.cgi');

# Formatage des données de l'onglet Durée
&formatList($tabViewData{'user.country.id'}, $tabData, $locale, {});


print $view->displayHeader();
print $view->encode($tabData);
