use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Json;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


our %tabViewData;
our $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});

# Dates et heures au format local
foreach my $data (@{$tabViewData{'tabHistory'}})
{
    $data->{'date'} = $locale->getDateFormat($data->{'datetime'}, LOC::Locale::FORMAT_DATE_NUMERIC);
    $data->{'time'} = $locale->getDateFormat($data->{'datetime'}, LOC::Locale::FORMAT_TIME_NUMERIC);
    delete($data->{'datetime'});
}


my $view = LOC::Json->new();
print $view->displayHeader();
print $view->encode(\%tabViewData);
