use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;

# Répertoire CSS/JS/Images
my $dir = 'transport/machineInventory/';


our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}

# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'view.css');
# JavaScript
$view->addJSSrc($dir . 'view.js');

$view->addPackage('popups')
     ->addPackage('searchboxes');

$view->setPageTitle($locale->t('État des lieux'));
$view->setTitle($locale->t('État des lieux'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_NONE);

# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();

# Traductions
$view->addTranslations({
    'button_view_photo'       => $locale->t('Afficher la photo'),
    'button_view_photo_infos' => $locale->t('Voir les informations sur la photo'),

    'label_photo_date'      => $locale->t('Date : %s', '<%date>'),
    'label_photo_position'  => $locale->t('Position : '),

    'label_open_map'  => $locale->t('Voir la carte'),
    'format_position' => $locale->t('%s, %s', '<%lat>', '<%lng>'),

    'answerAreYouSureDelete'   => $locale->t('Êtes-vous sûr de vouloir supprimer cet état des lieux ?'),
    'answerAreYouSureDetach'   => $locale->t('Êtes-vous sûr de vouloir détacher cet état des lieux du contrat ?')
});

my $tabErrorTypes = {
    'fatalError'        => $locale->t('Erreur fatale ! Veuillez contacter le service informatique s.v.p.'),
    'rightsError'       => $locale->t('Vous n\'avez pas le droit de modifier l\'état des lieux'),
    'alreadyDetached'   => $locale->t('L\'état des lieux est déjà détaché du contrat.'),
    'alreadyAttached'   => $locale->t('L\'état des lieux est déjà rattaché à un contrat.'),
    'attached'          => $locale->t('L\'état des lieux est rattaché à un contrat.'),
    'notAttached'       => $locale->t('L\'état des lieux n\'est pas rattaché à un contrat.'),
    'undefinedMachine'  => $locale->t('Machine non définie'),
    'unknownMachine'    => $locale->t('Machine inconnue'),
    'undefinedContract' => $locale->t('Contrat non défini'),
    'unknownContract'   => $locale->t('Contrat inconnu'),
    'invalidCouple'     => $locale->t('Association contrat/machine incohérente'),
    'emailRequired'     => $locale->t('L\'adresse e-mail du client est nécessaire')
};

my $tabValidTypes = {
    'detach'   => $locale->t('L\'état des lieux a été détaché du contrat.'),
    'delete'   => $locale->t('L\'état des lieux a été supprimé.'),
    'attach'   => $locale->t('L\'état des lieux a été rattaché au contrat.'),
    'sendMail' => $locale->t('L\'e-mail de notification de la livraison a été envoyé au client.')
};

my $mapsUrl = 'http://maps.google.fr/maps?f=q&hl=fr&q=%s,%s';

my $tabInfos = $tabViewData{'infos'};

my $isEditable = ($tabViewData{'from'} eq 'alert' &&
                  $tabInfos->{'tabRights'}->{'actions'}->{'update'} &&
                  $tabInfos->{'tabPossibilities'}->{'actions'}->{'update'});
my $isDeletable = ($tabViewData{'from'} eq 'alert' &&
                   $tabInfos->{'tabRights'}->{'actions'}->{'delete'} &&
                   $tabInfos->{'tabPossibilities'}->{'actions'}->{'delete'});

# Popup de visualisation des états des lieux
# - Bouton d'historique
my $historyBtn = $view->displayTextButton('',
                                      'history.png',
                                      'return false;',
                                      $locale->t('Historique')
                                      ,'l',
                                      {'id' => 'machineInventoryHistoryBtn'});

# - Bouton de retour d'historique
my $backBtn = $view->displayTextButton('',
                                      'revertOver.png',
                                      'return false;',
                                      $locale->t('Retour')
                                      ,'l',
                                      {'id' => 'machineInventoryBackBtn'});


print '
<div class="slides">
    <div class="slide main">
        <fieldset class="sub content">
            <legend>
                <span class="type_d">' . $locale->t('État des lieux de livraison n°%s', $tabInfos->{'id'}) . '</span>
                <span class="type_r">' . $locale->t('État des lieux de récupération n°%s', $tabInfos->{'id'}) . '</span>
                <span class="status-notAttached">(' . $locale->t('non rattaché') . ')</span>
                <span class="status-deleted">(' . $locale->t('supprimé') . ')</span>
            </legend>
            <div class="legendBtn">';

# - Bouton de détachement
if ($tabInfos->{'tabRights'}->{'actions'}->{'detach'} &&
    $tabInfos->{'tabPossibilities'}->{'actions'}->{'detach'} &&
    $tabViewData{'from'} eq 'rentContract')
{
    my %tabAttributes = (
        'id'    => 'machineInventoryDetachBtn',
        'class' => 'locCtrlButton'
    );
    my $detachBtn = $view->displayTextButton('',
                                          $dir . 'view/detach.png',
                                          'return false;',
                                          $locale->t('Détacher du contrat')
                                          , 'l',
                                          \%tabAttributes);
    print '
                <div>' . $detachBtn . '</div>';
}

# - Bouton d'envoi générique par mail
if ($tabInfos->{'tabRights'}->{'actions'}->{'sendMail'})
{
    my $mailBtn = $view->displayPrintButtons('machineInventorySendMailBtn',
                                              undef,
                                              undef,
                                              $tabInfos->{'sendMail'}->{'defaultRecipient'},
                                              $tabInfos->{'sendMail'}->{'defaultSubject'},
                                              $tabInfos->{'sendMail'}->{'defaultContent'},
                                              $tabInfos->{'sendMail'}->{'documentAgency.id'},
                                              {
                                                  'buttons'                  => ['mail'],
                                                  'tabAdditionalAttachments' => $tabInfos->{'sendMail'}->{'tabAdditionalAttachments'},
                                                  'tabEndActions'            => $tabInfos->{'sendMail'}->{'tabEndActions'}
                                              });

    print '

                <div>' . $mailBtn . '</div>';
}

print '
                <div>' . $historyBtn . '</div>
            </div>
            <table class="formTable" id="machineInventoryInfos">
                <tbody>';
if ($tabInfos->{'date'})
{
    my $date = $locale->getDateFormat($tabInfos->{'date'},
                                                LOC::Locale::FORMAT_DATETIME_NUMERIC);
    print '
                    <tr>
                        <td class="label">' . $locale->t('Date') . '</td>
                        <td class="content">' . $date . '</td>
                    </tr>';
}
if ($tabInfos->{'creator'})
{
    print '
                    <tr>
                        <td class="label">' . $locale->t('Par') . '</td>
                        <td class="content">' . $tabInfos->{'creator'} . '</td>
                    </tr>';
}

my $defaultParkNumberValue = '';
if (!$tabInfos->{'isParkNumberOk'})
{
    $defaultParkNumberValue = $locale->t(' (saisi : %s)', $tabInfos->{'enteredParkNumber'});
}

my $defaultContractValue = '';
if (!$tabInfos->{'isContractCodeOk'})
{
    $defaultContractValue = $locale->t(' (saisi : %s)', $tabInfos->{'enteredContractCode'});
}

if ($isEditable)
{
    # Modification
    my $tabMachinesList = {};
    if ($tabInfos->{'machineId'} != 0)
    {
        $tabMachinesList = {$tabInfos->{'machineId'} => $tabInfos->{'parkNumber'}};
    }
    my $parkNumberSelect   = $view->displayFormSelect('machineId', $tabMachinesList, $tabInfos->{'machineId'});
    my $parkNumbersListUrl = $view->createURL('default:machine:jsonList', {'format' => 0});
    $parkNumberSelect .= $view->displayJSBlock('
        var parkNumberSearchBoxObj = Location.searchBoxesManager.createSearchBox("machineId", {isRequired: true, url: "' . $parkNumbersListUrl . '"});
        parkNumberSearchBoxObj.createElement = function(key, value) {
            var opt = new Option(value["parkNumber"], value["id"]);
            return opt;
        };
    ');

    my $tabContractsList = {};
    if ($tabInfos->{'contractId'} != 0)
    {
        $tabContractsList = {$tabInfos->{'contractId'} => $tabInfos->{'contractCode'}};
    }
    my $contractCodeSelect   = $view->displayFormSelect('contractId', $tabContractsList, 0);
    my $contractCodesListUrl = $view->createURL('default:contract:jsonList', {'format' => 0});
    $contractCodeSelect .= $view->displayJSBlock('
        var contractCodeSearchBoxObj = Location.searchBoxesManager.createSearchBox("contractId", {isRequired: true, url: "' . $contractCodesListUrl . '"});
        contractCodeSearchBoxObj.createElement = function(key, value) {
            var opt = new Option(value["code"], value["id"]);
            return opt;
        };
    ');

    print '
                    <tr>
                        <td class="label">' . $locale->t('N° de parc') . '</td>
                        <td class="content">' . $parkNumberSelect . $defaultParkNumberValue . '</td>
                    </tr>';

    print '
                    <tr>
                        <td class="label">' . $locale->t('Contrat') . '</td>
                        <td class="content">' . $contractCodeSelect . $defaultContractValue . '</td>
                    </tr>';
}
else
{
    # Affichage
    if ($tabInfos->{'parkNumber'})
    {
        print '
                    <tr>
                        <td class="label">' . $locale->t('N° de parc') . '</td>
                        <td class="content">' . $view->displayParkNumber($tabInfos->{'parkNumber'}) . $defaultParkNumberValue . '</td>
                    </tr>';
    }
    if ($tabInfos->{'contractCode'})
    {
        my $contractUrl = $view->createURL('rent:rentContract:view', {'contractId' => $tabInfos->{'contractId'}});
        print '
                    <tr>
                        <td class="label">' . $locale->t('Contrat') . '</td>
                        <td class="content">
                            <a href="' . $contractUrl . '" target="_blank">' . $tabInfos->{'contractCode'} . '</a>' . $defaultContractValue . '
                        </td>
                    </tr>';
    }
}

if ($tabInfos->{'latitude'} && $tabInfos->{'longitude'})
{
    my $latitude  = &LOC::Util::getNonLocaleFloat($tabInfos->{'latitude'});
    my $longitude = &LOC::Util::getNonLocaleFloat($tabInfos->{'longitude'});

    my $positionUrl = sprintf($mapsUrl, $latitude, $longitude);

    print '
                    <tr>
                        <td class="label">' . $locale->t('Position') . '</td>
                        <td class="content">
                            <a href="' . $positionUrl . '" target="_blank">' . $locale->t('%s, %s', $latitude, $longitude) . '</a>
                        </td>
                    </tr>';
}
if ($tabInfos->{'description'})
{
    print '
                    <tr>
                        <td class="label">' . $locale->t('Description') . '</td>
                        <td class="content">' . $tabInfos->{'description'} . '</td>
                    </tr>';
}
print '
                </tbody>
            </table>

            <div class="gallery">
                <div id="layer-downloading" class="photo-loading"></div>
                <div class="photo-error hidden" title="' . htmllocale('Erreur lors de la récupération des photos') . '"></div>
            </div>
        </fieldset>';

if ($isEditable || $isDeletable)
{
    my $validAttachBtn = '';
    my $deleteBtn      = '';

    # - Bouton de validation
    if ($isEditable)
    {
        my %tabAttributes = (
            'id' => 'machineInventoryAttachBtn',
            'class' => 'locCtrlButton',
            'tabindex' => -1,
            'onfocus' => 'this.blur();'
        );
        $validAttachBtn = $view->displayTextButton($locale->t('Valider et rattacher'),
                                      'validOver.png',
                                      'return false;',
                                      $locale->t('Valider et rattacher')
                                      ,'l',
                                      \%tabAttributes);
    }
    # - Bouton de suppression
    if ($isDeletable)
    {
        my %tabAttributes = (
            'id' => 'machineInventoryDeleteBtn',
            'class' => 'locCtrlButton',
            'tabindex' => -1,
            'onfocus' => 'this.blur();'
        );
        $deleteBtn = $view->displayTextButton($locale->t('Supprimer'),
                                      'deleteOver.png',
                                      'return false;',
                                      $locale->t('Supprimer')
                                      ,'l',
                                      \%tabAttributes);
    }

    print '
        <fieldset class="sub ctrls">
            ' . $validAttachBtn . $deleteBtn . '
        </fieldset>';
}

# Gestion des erreurs
if (@{$tabViewData{'errors'}} > 0)
{
    my @tabMessages = map ($tabErrorTypes->{$_}, @{$tabViewData{'errors'}});

    print $view->displayMessages('error', \@tabMessages);
}
if (@{$tabViewData{'valid'}} > 0)
{
    my @tabMessages = map ($tabValidTypes->{$_}, @{$tabViewData{'valid'}});

    print $view->displayMessages('valid', \@tabMessages);
}

print '
    </div>

    <div class="slide history">
        <fieldset class="sub content">
            <legend>' . $locale->t('Historique de l\'état des lieux n°%s', $tabInfos->{'id'}) . '
                <span class="status-notAttached">(' . $locale->t('non rattaché') . ')</span>
                <span class="status-deleted">(' . $locale->t('supprimé') . ')</span>
            </legend>
            <div class="legendBtn">
                <div>' . $backBtn . '</div>
            </div>
            <div id="machineInventory-historyWaiting"></div>
            <div id="machineInventory-historyResult" class="hidden">
                <table class="standard">
                    <thead>
                        <tr>
                            <th colspan="2">' . $locale->t('Date et heure') . '</th>
                            <th>' . $locale->t('Utilisateur') . '</th>
                            <th>' . $locale->t('Événement') . '</th>
                            <th>' . $locale->t('Description') . '</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>';

# Message s'il n'y a pas d'historique
print '
            <div id="machineInventory-historyNoResult" class="hidden">
                ' . $view->displayMessages('info', [$locale->t('Il n\'y a pas de données disponibles.')], 0) . '
            </div>';

# Message d'erreur
print '
            <div id="machineInventory-historyError" class="hidden">
                ' . $view->displayMessages('error', [$locale->t('Une erreur est survenue lors de la récupération des dernières modifications.')], 0) . '
            </div>
        </fieldset>
    </div>
</div>';

# Données à passer au JavaScript
print $view->displayJSBlock('
    machineInventory.init(' . &LOC::Json::toJson({
        'from'       => $tabViewData{'from'},
        'tabInfos'   => $tabInfos,
        'photosUrl'  => $view->createURL('transport:machineInventory:jsonPhotos'),
        'historyUrl' => $view->createURL('transport:machineInventory:jsonHistory'),
        'mapsUrl'    => sprintf($mapsUrl, '<%lat>', '<%lng>'),
        'tabValid'   => $tabViewData{'valid'}
    }) . ');
');

print $view->displayFormInputHidden('action', undef);
print $view->displayFormInputHidden('from', $tabViewData{'from'});

print $view->endForm();
# Affichage du pied de page
print $view->displayFooter();
