use utf8;
use open (':encoding(UTF-8)');

use strict;


sub formatList
{
    my ($countryId, $tabData, $locale, $tabOptions) = @_;

    # Liste des choix pour le filtre des types de transport
    my %tabTypesLabels = (
        &LOC::Transport::TYPE_DELIVERY         => $locale->t('Livraison'),
        &LOC::Transport::TYPE_RECOVERY         => $locale->t('Récupération'),
        &LOC::Transport::TYPE_SITESTRANSFER    => ($tabOptions->{'isSelf'} ? $locale->t('Reprise sur chantier') :
                                                                             $locale->t('Transfert inter-chantiers')),
        &LOC::Transport::TYPE_AGENCIESTRANSFER => $locale->t('Transfert inter-agences')
    );

    # Liste des choix pour le filtre des dates
    my @tabDateDiffLabels = (
        {
            'value' => '=:-5',
            'label' => $locale->t('Cdes transport à J%s', '-5')
        },
        {
            'value' => '=:-4',
            'label' => $locale->t('Cdes transport à J%s', '-4')
        },
        {
            'value' => '=:-3',
            'label' => $locale->t('Cdes transport à J%s', '-3')
        },
        {
            'value' => '=:-2',
            'label' => $locale->t('Cdes transport à J%s', '-2')
        },
        {
            'value' => '=:-1',
            'label' => $locale->t('Cdes transport à J%s', '-1')
        },
        {
            'value' => '<:0',
            'label' => $locale->t('Cdes transport en retard')
        },
        {
            'value' => '=:0',
            'label' => $locale->t('Cdes transport du jour')
        },
        {
            'value' => '>:0',
            'label' => $locale->t('Cdes transport à venir')
        },
        {
            'value' => '=:1',
            'label' => $locale->t('Cdes transport à J%s', '+1')
        },
        {
            'value' => '=:2',
            'label' => $locale->t('Cdes transport à J%s', '+2')
        },
        {
            'value' => '=:3',
            'label' => $locale->t('Cdes transport à J%s', '+3')
        },
        {
            'value' => '=:4',
            'label' => $locale->t('Cdes transport à J%s', '+4')
        },
        {
            'value' => '=:5',
            'label' => $locale->t('Cdes transport à J%s', '+5')
        }
    );

    # Liste des choix pour le filtre de réalisation
    my @tabDoableLabels = (
        {
            'value' => '1-0',
            'label' => $locale->t('Réalisable')
        },
        {
            'value' => '0-0',
            'label' => $locale->t('Non réalisable')
        },
        {
            'value' => '0-' . LOC::Transport::ERROR_NOMACHINE,
            'label' => $locale->t('Non réalisable car la machine n\'est pas attribuée')
        },
        {
            'value' => '0-' . LOC::Transport::ERROR_CUSTNOTVERIF,
            'label' => $locale->t('Non réalisable car l\'ouverture de compte du client est en attente')
        },
        {
            'value' => '0-' . LOC::Transport::ERROR_PFNOTGEN,
            'label' => $locale->t('Non réalisable car la pro forma n\'est pas générée')
        },
        {
            'value' => '0-' . LOC::Transport::ERROR_PFNOTUPTODATE,
            'label' => $locale->t('Non réalisable car la pro forma n\'est pas à jour')
        },
        {
            'value' => '0-' . LOC::Transport::ERROR_PFNOTPAYED,
            'label' => $locale->t('Non réalisable car la pro forma n\'est pas finalisée')
        },
        {
            'value' => '0-' . LOC::Transport::ERROR_RENTTARIFF,
            'label' => $locale->t('Non réalisable car la remise exceptionnelle de location est en attente')
        },
        {
            'value' => '0-' . LOC::Transport::ERROR_TRSPTARIFF,
            'label' => $locale->t('Non réalisable car la remise exceptionnelle de transport en attente')
        },
        {
            'value' => '0-' . LOC::Transport::ERROR_DELIVERYNOTDONE,
            'label' => $locale->t('Non réalisable car la livraison n\'est pas effectuée')
        },
        {
            'value' => '0-' . LOC::Transport::ERROR_DELIVERY,
            'label' => $locale->t('Non réalisable car il y a une erreur sur la livraison du transfert')
        },
        {
            'value' => '0-' . LOC::Transport::ERROR_RECOVERY,
            'label' => $locale->t('Non réalisable car il y a une erreur sur la récupération du transfert')
        },
        {
            'value' => '0-' . LOC::Transport::ERROR_NOMAINCONTRACT,
            'label' => $locale->t('Non réalisable car il n\'y a pas de contrat principal sélectionné')
        },
        {
            'value' => '0-' . LOC::Transport::ERROR_MACHINESITEBLOCKED,
            'label' => $locale->t('Non réalisable car la machine est bloquée sur chantier')
        },
        {
            'value' => '0-' . LOC::Transport::ERROR_MACHINENOTTRANSFERED,
            'label' => $locale->t('Non réalisable car la machine n\'est pas encore transférée')
        }
    );

    # Fonction d'ajout des libellés dans les listes sans libellés
    my $addLabels = sub {
        my ($tab, $tabLabels) = @_;

        foreach my $row (@$tab)
        {
            $row->{'label'} = $tabLabels->{$row->{'value'}};
        }
        my @tab2 = sort {$a->{'label'} cmp $b->{'label'}} @$tab;

        return \@tab2;
    };



    # Formatage des lignes
    foreach my $row (@{$tabData->{'rows'}})
    {
        # Libellé du type
        $row->{'type.label'} = $tabTypesLabels{$row->{'type'}};

        # N° de commande
        $row->{'order.code'} = $row->{'order.code'} . ''; # Pour éviter les "null"

        # Date du transport
        if ($row->{'date'} ne '')
        {
            $row->{'date'} = $locale->getDateFormat($row->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC);
        }
        if ($row->{'linkDate'} ne '')
        {
            $row->{'linkDate'} = $locale->getDateFormat($row->{'linkDate'}, LOC::Locale::FORMAT_DATE_NUMERIC);
        }

        # Type de document
        if ($row->{'document.type'} eq 'estimateLine')
        {
            $row->{'document.type'}  = $locale->t('Ligne de devis');
            $row->{'document.index'} = $locale->t('ligne') . ' ' . $row->{'document.index'};
        }
        elsif ($row->{'document.type'} eq 'contract')
        {
            $row->{'document.type'} = $locale->t('Contrat');
        }
        else
        {
            $row->{'document.type'} = '';
        }

        # URL du dernier contrat associé
        if ($row->{'document.code'} ne '')
        {
            if ($row->{'contract.id'})
            {
                $row->{'contract.url'} = &LOC::Request::createRequestUri('rent:rentContract:view',
                                                                         {'contractId' => $row->{'contract.id'},
                                                                          'tabIndex'   => LOC::Transport::DEFAULT_CONTRACT_TABINDEX});
            }
            if ($row->{'estimateLine.id'})
            {
                my $estimateId = &LOC::Estimate::Rent::getIdByLineId($countryId, $row->{'estimateLine.id'});
                $row->{'estimateLine.url'} = &LOC::Request::createRequestUri('rent:rentEstimate:view',
                                                                             {'estimateId'     => $estimateId,
                                                                              'estimateLineId' => $row->{'estimateLine.id'},
                                                                              'tabIndex'       => LOC::Transport::DEFAULT_ESTIMATELINE_TABINDEX});
            }
        }

        # URL du dernier contrat associé
        if ($row->{'linkContract.code'} ne '')
        {
            $row->{'linkContract.url'} = &LOC::Request::createRequestUri('rent:rentContract:view',
                                                                         {'contractId' => $row->{'linkContract.id'},
                                                                          'tabIndex'   => LOC::Transport::DEFAULT_CONTRACT_TABINDEX});
        }

        # URL de la fiche machine
        $row->{'machine.url'} = &LOC::Request::createRequestUri('machine:machine:view', {'machineId' => $row->{'machine.id'}});

        # URL de la fiche client
        if ($row->{'customer.id'})
        {
            $row->{'customer.url'} = &LOC::Customer::getUrl($row->{'customer.id'});
        }
        if ($row->{'linkCustomer.id'})
        {
            $row->{'linkCustomer.url'} = &LOC::Customer::getUrl($row->{'linkCustomer.id'});
        }
    }

    # Formatage des filtres
    if ($tabData->{'filters'})
    {
        my $tabFilters = $tabData->{'filters'};

        # Filtres sur les types de transport
        if (exists $tabFilters->{'type'})
        {
            $tabFilters->{'type'}->{'list'} = &$addLabels($tabFilters->{'type'}->{'list'}, \%tabTypesLabels);
            if (exists $tabFilters->{'type'}->{'values'})
            {
                $tabFilters->{'type'}->{'values'} = &$addLabels($tabFilters->{'type'}->{'values'}, \%tabTypesLabels);
            }
        }

        # Filtre sur les dates
        if (exists $tabFilters->{'dateDiff'})
        {
            my @tabNewList = ();

            my %tabLabels = ();
            foreach my $row (@tabDateDiffLabels)
            {
                my $value = $row->{'value'};
                $value =~ /(.*?):(.*)/;
                my $cond = ($1 eq '=' ? '==' : $1) . $2;

                my @tab = grep { eval($_->{'value'} . $cond); } @{$tabFilters->{'dateDiff'}->{'list'}};
                if (@tab > 0)
                {
                    push(@tabNewList, $row);
                }

                $tabLabels{$value} = $row->{'label'};
            }

            if (exists $tabFilters->{'dateDiff'}->{'values'})
            {
                foreach my $row (@{$tabFilters->{'dateDiff'}->{'values'}})
                {
                    $row->{'label'} = $tabLabels{$row->{'value'}};
                }
            }

            $tabFilters->{'dateDiff'}->{'list'} = \@tabNewList;

        }

        # Filtre sur les états de réalisation
        if (exists $tabFilters->{'doableState'})
        {
            my @tabNewList = ();

            my %tabLabels = ();
            foreach my $row (@tabDoableLabels)
            {
                my $value = $row->{'value'};
                my ($isDoable, $error) = split('-', $value);

                my $expr = sub {
                    my ($doable, $err) = split('-', $_[0]->{'value'});
                    my $result = ($doable * 1 == $isDoable * 1 && ($err * 1 & $error * 1) == $error * 1 ? 1 : 0);
                    return $result;
                };

                my @tab = grep &$expr($_), @{$tabFilters->{'doableState'}->{'list'}};
                if (@tab > 0)
                {
                    push(@tabNewList, $row);
                }

                $tabLabels{$value} = $row->{'label'};
            }

            if (exists $tabFilters->{'doableState'}->{'values'})
            {
                foreach my $row (@{$tabFilters->{'doableState'}->{'values'}})
                {
                    $row->{'label'} = $tabLabels{$row->{'value'}};
                }
            }

            $tabFilters->{'doableState'}->{'list'} = \@tabNewList;
        }

    }
}


1;
