use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'transport/pending/';


our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});


sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}



# Constantes en JavaScript
my $tabErrorTypes = [
    {
        'code'         => 'noMachine',
        'value'        => LOC::Transport::ERROR_NOMACHINE,
        'title'        => $locale->t('Machine non attribuée')
    },
    {
        'code'         => 'customerNotVerified',
        'value'        => LOC::Transport::ERROR_CUSTNOTVERIF,
        'title'        => $locale->t('Client avec ouverture de compte en attente')
    },
    {
        'code'         => 'proformaNotGen',
        'value'        => LOC::Transport::ERROR_PFNOTGEN,
        'title'        => $locale->t('Pro forma non générée')
    },
    {
        'code'         => 'proformaNotUpToDate',
        'value'        => LOC::Transport::ERROR_PFNOTUPTODATE,
        'title'        => $locale->t('Pro forma non à jour')
    },
    {
        'code'         => 'proformaNotPayed',
        'value'        => LOC::Transport::ERROR_PFNOTPAYED,
        'title'        => $locale->t('Pro forma non finalisée')
    },
    {
        'code'         => 'rentTariff',
        'value'        => LOC::Transport::ERROR_RENTTARIFF,
        'title'        => $locale->t('Remise exceptionnelle de location en attente')
    },
    {
        'code'         => 'transportTariff',
        'value'        => LOC::Transport::ERROR_TRSPTARIFF,
        'title'        => $locale->t('Remise exceptionnelle de transport en attente')
    },
    {
        'code'         => 'deliveryNotDone',
        'value'        => LOC::Transport::ERROR_DELIVERYNOTDONE,
        'title'        => $locale->t('Livraison non effectuée')
    },
    {
        'code'         => 'onDelivery',
        'value'        => LOC::Transport::ERROR_DELIVERY,
        'title'        => $locale->t('Erreur(s) sur la livraison du transfert')
    },
    {
        'code'         => 'onRecovery',
        'value'        => LOC::Transport::ERROR_RECOVERY,
        'title'        => $locale->t('Erreur(s) sur la récupération du transfert')
    },
    {
        'code'         => 'noMainContract',
        'value'        => LOC::Transport::ERROR_NOMAINCONTRACT,
        'title'        => $locale->t('Pas de contrat principal sélectionné')
    },
    {
        'code'         => 'machineSiteBlocked',
        'value'        => LOC::Transport::ERROR_MACHINESITEBLOCKED,
        'title'        => $locale->t('Machine bloquée sur chantier')
    },
    {
        'code'         => 'machineNotTransfered',
        'value'        => LOC::Transport::ERROR_MACHINENOTTRANSFERED,
        'title'        => $locale->t('Machine non transférée')
    }
];

$view->addJSSrc('
/* Erreurs */
var tabErrorTypes = ' . &LOC::Json::toJson($tabErrorTypes) . ';
');

$view->addJSSrc('
/* Types de transport */
var TYPE_DELIVERY         = "' . LOC::Transport::TYPE_DELIVERY . '";
var TYPE_RECOVERY         = "' . LOC::Transport::TYPE_RECOVERY . '";
var TYPE_SITESTRANSFER    = "' . LOC::Transport::TYPE_SITESTRANSFER . '";
var TYPE_AGENCIESTRANSFER = "' . LOC::Transport::TYPE_AGENCIESTRANSFER . '";
');

$view->addJSSrc('
/* États du contrat */
var STATE_PRECONTRACT = "' . LOC::Contract::Rent::STATE_PRECONTRACT . '";
var STATE_CONTRACT    = "' . LOC::Contract::Rent::STATE_CONTRACT . '";
var STATE_STOPPED     = "' . LOC::Contract::Rent::STATE_STOPPED . '";
var STATE_ARCHIVE     = "' . LOC::Contract::Rent::STATE_ARCHIVE . '";
var STATE_CANCELED    = "' . LOC::Contract::Rent::STATE_CANCELED . '";
var STATE_BILLED      = "' . LOC::Contract::Rent::STATE_BILLED . '";
');

$view->addJSSrc('
/* États de la ligne de devis */
var STATE_ESTIMATELINE = "' . ($tabViewData{'dataSource'} == DATASRC_VIEW ? 0 : LOC::Estimate::Rent::LINE_STATE_ACTIVE) . '";
');

my $tabNonDoabilityTypes = {
    'recovery-contract' => $locale->t('Récupération sur un contrat en cours'),
    'estimateLine'      => $locale->t('Ligne de devis non passée en contrat')
};

$view->addJSSrc('
/* Causes de non-faisabilité (hors erreurs) */
var tabNonDoabilityTypes = ' . &LOC::Json::toJson($tabNonDoabilityTypes) . ';
');

# CSS
$view->addCSSSrc($dir . 'view.css');
$view->addCSSSrc('location.css');

# JS
$view->addJSSrc($dir . 'view.js');
# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc('jQuery/plugins/ajaxTable.js');
$view->addCSSSrc('jQuery/plugins/ajaxTable.css');

# Ajout des packages
$view->addPackage('modalwindow');

# Affichage de l'entête
$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_NOTIFS);

my $title = '';
my $helpMessage;
if ($tabViewData{'hasMachineInventory'})
{
    $title = $locale->t('Transports effectués');
    $helpMessage = $locale->t('Liste des transports non réalisés qui ont fait l\'objet d\'un état des lieux.');
}
else
{
    $title = ($tabViewData{'isSelf'} ? $locale->t('Enlèvements agence / Reprises sur chantier') : $locale->t('Transports en cours'));
}

$view->setPageTitle($title);
$view->setTitle($title);


# Traductions
$view->addTranslations({
    'isMachineWithTelematic' => $locale->t('Cette machine est équipée de télématique'),
    'viewMachineInventory'   => $locale->t('Voir l\'état des lieux associé'),
    'isAsbestos'             => $locale->t('Présence d\'amiante sur le chantier')
});



# Notifications possibles
$view->addNotifications([
    {
        'id'      => 'cancel-single-noundo',
        'type'    => 'info',
        'options' => {
                      'isClosable' => 0
                     },
        'message' => $locale->t('Le transport a été marqué comme réalisé.')
    },
    {
        'id'      => 'cancel-single',
        'type'    => 'info',
        'options' => {
                      'isClosable' => 0
                     },
        'message' => $locale->t('Le transport a été marqué comme réalisé.'),
        'buttons' => [{
                       'title'   => $locale->t('Annuler cette réalisation'),
                       'onclick' => 'transports.undo();'
                      }]
    },
    {
        'id'      => 'cancel-plural-noundo',
        'type'    => 'info',
        'options' => {
                      'isClosable' => 0
                     },
        'message' => $locale->t('Les transports ont été marqués comme réalisés.')
    },
    {
        'id'      => 'cancel-plural',
        'type'    => 'info',
        'options' => {
                      'isClosable' => 0
                     },
        'message' => $locale->t('Les transports ont été marqués comme réalisés.'),
        'buttons' => [{
                       'title'   => $locale->t('Annuler ces réalisations'),
                       'onclick' => 'transports.undo();'
                      }]
    },
    {
        'id'      => 'error-single',
        'type'    => 'error',
        'options' => {
                      'isClosable' => 0
                     },
        'message' => $locale->t('Une erreur est survenue lors de la réalisation du transport.')
    },
    {
        'id'      => 'error-plural',
        'type'    => 'error',
        'options' => {
                      'isClosable' => 0
                     },
        'message' => $locale->t('Une erreur est survenue lors de la réalisation des transports.')
    }
]);

# Légende des cas d'erreurs
my $legendHtml = '<div class="legend">' .
                    '<span class="title">' . htmllocale('Légende pour les cas erronés') . '&nbsp;:</span>';
foreach my $tabError (@$tabErrorTypes)
{
    $legendHtml .= '<div class="button ' . $tabError->{'code'} . '">' .
                        '<div class="errorImg ' . $tabError->{'code'} . '"></div>' .
                        '<div class="text"><div class="pick"></div>' . $view->toHTMLEntities($tabError->{'title'}) . '</div>' .
                   '</div>';
}
$legendHtml .= '</div>';

$view->addControlsContent([$legendHtml], 'left', 1);

# Bouton "Actualiser"
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png',
                                          '',
                                          $locale->t('Actualiser la page'),
                                          'l',
                                          {'id' => 'refresh-btn'});
$view->addControlsContent([$refreshBtn], 'right', 1);



$view->{'_networkOptimization'} = 'transport-pending-view';
print $view->displayHeader();

if ($helpMessage)
{
    print $view->displayMessages('help', [$helpMessage], 0);
}


# Bouton d'export Excel
my $excelBtn = $view->displayTextButton($locale->t('Export Excel'), 'common/ajaxTable/excel(|Over).png',
                                        'return true;',
                                        '',
                                        'l',
                                        {'class' => 'locCtrlButton', 'id' => 'excel-btn'});

# Bouton de réalisation
my $checkedNo = ' <span id="do-checked"></span>';
my $doBtn = $view->displayTextButton($locale->t('Réaliser') . $checkedNo, 'valid(|Over).png',
                                     'return true;',
                                     '',
                                     'l',
                                     {'class' => 'locCtrlButton disabled', 'id' => 'do-btn'});

# Bouton d'affichage de tous les transports
my $viewModeFullLabel = $locale->t('Affichage : %s', '<span id="viewMode-full">' . $locale->t('toutes les lignes') . '</span>');
my $viewModeFullBtn = $view->displayTextButton($viewModeFullLabel,
                                               'common/ajaxTable/viewall.png',
                                               'return true;',
                                               '',
                                               'l',
                                               {'id'    => 'change-viewMode-full',
                                                'class' => 'locCtrlButton viewMode disabled'});

# Bouton d'affichage des transports sélectionnés seulement
my $viewModeCheckedLabel = $locale->t('Affichage : %s', '<span id="viewMode-checked">' . $locale->t('lignes sélectionnées') . '</span>');
my $viewModeCheckedBtn = $view->displayTextButton($viewModeCheckedLabel,
                                                  'common/ajaxTable/viewchecked.png',
                                                  'return true;',
                                                  '',
                                                  'l',
                                                  {'id'    => 'change-viewMode-checked',
                                                   'class' => 'locCtrlButton viewMode',
                                                   'style' => 'display: none;'});

# Bouton de réinitialisation des filtres
my $resetFiltersBtn = $view->displayTextButton($locale->t('Réinitialiser'), 'common/ajaxTable/reset.png',
                                               'return true;',
                                               '',
                                               'l',
                                               {'class' => 'locCtrlButton', 'id' => 'reset-filters-btn'});

if (!$tabViewData{'hasMachineInventory'})
{
    print '
<div class="action-mask"></div>

<fieldset id="table-filters-container" class="filters">
    <legend>' . htmllocale('Filtres') . '</legend>

    <ul class="content">
        <li class="part">
            <ul>';
    # Filtre sur les régions
    if ($tabViewData{'isAreasFilterActived'})
    {
        print '
                <li id="filter-area"></li>';
    }
    # Filtre sur les agences
    if ($tabViewData{'isAgenciesFilterActived'})
    {
        print '
                <li id="filter-agency"></li>';
    }
    print '
                <li id="filter-type"></li>
                <li id="filter-doable"></li>
            </ul>
        </li>
        <li class="part">
            <ul>';
    if (!$tabViewData{'isSelf'})
    {
        print '
                <li id="filter-order"></li>';
    }
    print '
                <li id="filter-date-diff"></li>
                <li id="filter-from-address"></li>
                <li id="filter-to-address"></li>
                <li id="filter-customer"></li>
                <li id="filter-park-number"></li>
            </ul>
        </li>
    </ul>
    <div class="ctrls">
        ' . $resetFiltersBtn . '
    </div>
</fieldset>';
}

print '
<div class="list">
    <div id="table-toolbar-container" class="toolbar header">
        <div class="col content">
            <div id="header-pagination-container"></div>
            <div class="fixed-content">
                <div id="header-filters-recap"></div>
            </div>
        </div>
        <div class="col controls">
            <ul>
                <li>' . $viewModeFullBtn . $viewModeCheckedBtn . '</li>
            </ul>
            <ul>
                <li>' . $excelBtn . '</li>
                <li> ' . $doBtn . '</li>
            </ul>
            <div id="header-checked-recap"></div>
            <div class="fixed-content">
                <a href="#table-filters-container" id="back-top-btn" title="' . htmllocale('Retourner en haut de la page') . '">' .
                    $view->displayImage('common/ajaxTable/up.png') .
               '</a>
            </div>
        </div>
    </div>
    <div class="content" id="table-container"></div>
</div>';


# Formatage de la liste
my $tabData = $tabViewData{'data'};
require($directory . '/pending/format/list.cgi');

# Formatage des données de l'onglet Durée
&formatList($tabViewData{'user.country.id'}, $tabData, $locale, {'isSelf' => $tabViewData{'isSelf'}});

# Paramètrage des colonnes
my $uncheckallBtn  = $view->displayImage('common/ajaxTable/uncheckall.png',
                                         {'title' => $locale->t('Désélectionner toutes les lignes'),
                                          'id'    => 'uncheck-btn'});

my @tabColumnsOptions = ();

# Colonne avec l'identifiant de l'agence responsable du transport
push(@tabColumnsOptions, {
    'id'         => 'agency.id',
    'title'      => $locale->t('Ag.'),
    'tip'        => $locale->t('Agence'),
    'cssClasses' => 'agency',
    'sort'       => 1
});

# Colonne avec le type de transport
push(@tabColumnsOptions, {
    'id'         => 'type',
    'title'      => $locale->t('Type'),
    'cssClasses' => 'type',
    'sort'       => 1
});

# Colonne avec le numéro de la commande associée
if (!$tabViewData{'isSelf'})
{
    push(@tabColumnsOptions, {
        'id'         => 'order.code',
        'title'      => $locale->t('N° cde tsp.'),
        'tip'        => $locale->t('N° de commande transport'),
        'cssClasses' => 'order',
        'sort'       => 1
    });
}

# Colonne avec la date du transport
push(@tabColumnsOptions, {
    'id'         => 'date',
    'title'      => $locale->t('Date'),
    'cssClasses' => 'date',
    'sort'       => 1
});

# Colonne avec la date du transport
push(@tabColumnsOptions, {
    'id'         => 'document.type',
    'title'      => $locale->t('Type document'),
    'cssClasses' => 'documenttype',
    'sort'       => 1
});

# Colonne avec le numéro de contrat associé
push(@tabColumnsOptions, {
    'id'         => 'document.code',
    'title'      => $locale->t('N° document'),
    'cssClasses' => 'documentcode',
    'sort'       => 1
});

# Colonne avec l'état amiante du contrat associé
push(@tabColumnsOptions, {
        'id'         => 'document.isAsbestos',
        'title'      => '',
        'tip'        => '',
        'cssClasses' => 'asbestos',
        'sort'       => 0
    }
);

# Colonne avec l'adresse de provenance
push(@tabColumnsOptions, {
    'id'         => 'from.label',
    'title'      => $locale->t('Provenance'),
    'cssClasses' => 'from',
    'sort'       => 1
});

# Colonne avec la flèche
push(@tabColumnsOptions, {
    'id'         => 'arrow',
    'title'      => '',
    'cssClasses' => 'arrow',
    'sort'       => 0
});

# Colonne avec l'adresse de destination
push(@tabColumnsOptions, {
    'id'         => 'to.label',
    'title'      => $locale->t('Destination'),
    'cssClasses' => 'to',
    'sort'       => 1
});

# Colonne avec le numéro de parc de la machine à transporter
push(@tabColumnsOptions, {
    'id'         => 'machine.parkNumber',
    'title'      => $locale->t('N° parc'),
    'cssClasses' => 'park',
    'sort'       => 1
});

# Colonne avec le modèle de la machine à transporter
push(@tabColumnsOptions, {
    'id'         => 'model.label',
    'title'      => $locale->t('Modèle de machines'),
    'cssClasses' => 'model',
    'sort'       => 1
});

# Colonne avec le nom du client associé
push(@tabColumnsOptions, {
    'id'         => 'customer.label',
    'title'      => $locale->t('Client'),
    'cssClasses' => 'customer',
    'sort'       => 1
});

# Colonne avec la faisabilité
push(@tabColumnsOptions, {
    'id'         => 'isDoable',
    'title'      => $uncheckallBtn,
    'cssClasses' => 'doable',
    'sort'       => 0
});


# Paramètrage des filtres de recherche
my $updateFilterDelay = 600;
my @tabFiltersOptions = ();

if (!$tabViewData{'hasMachineInventory'})
{

    # Filtre sur l'identifiant du transport
    push(@tabFiltersOptions, {
        'id' => 'id',
    });
    
    # Filtre sur les régions
    if ($tabViewData{'isAreasFilterActived'})
    {
        push(@tabFiltersOptions, {
            'id' => 'area.id',
            'container' => '#filter-area',
            'type' => 'list',
            'options' => {
                'delay' => $updateFilterDelay
            },
            'labels' => {
                'title'       => $locale->t('Région(s)'),
                'inactive'    => $locale->t('Toutes les régions'),
                'noSelection' => $locale->t('Choisir les régions'),
                'selections'  => $locale->t('Les régions de %s', '<%values>'),
                'selection'   => $locale->t('La région de %s', '<%value>'),
                'emptyValue'  => $locale->t('Sans région'),
                'noItemAvlb'  => $locale->t('Aucune région disponible')
            }
        });
    }
    
    # Filtre sur les agences
    if ($tabViewData{'isAgenciesFilterActived'})
    {
        push(@tabFiltersOptions, {
            'id' => 'agency.id',
            'container' => '#filter-agency',
            'type' => 'list',
            'options' => {
                'delay' => $updateFilterDelay
            },
            'labels' => {
                'title'       => $locale->t('Agence(s)'),
                'inactive'    => $locale->t('Toutes les agences'),
                'noSelection' => $locale->t('Choisir les agences'),
                'selections'  => $locale->t('Les agences de %s', '<%values>'),
                'selection'   => $locale->t('L\'agence de %s', '<%value>'),
                'noItemAvlb'  => $locale->t('Aucune agence disponible')
            }
        });
    }
    
    # Filtre sur les types de transport
    push(@tabFiltersOptions, {
        'id' => 'type',
        'container' => '#filter-type',
        'type' => 'list',
        'options' => {
            'delay' => $updateFilterDelay
        },
        'labels' => {
            'title'       => $locale->t('Type(s) de transport'),
            'inactive'    => $locale->t('Tous les types de transport'),
            'noSelection' => $locale->t('Choisir les types de transport'),
            'selections'  => $locale->t('Les types de transport %s', '<%values>'),
            'selection'   => $locale->t('Le type de transport %s', '<%value>'),
            'noItemAvlb'  => $locale->t('Aucun type de transport disponible')
        }
    });
    
    # Filtre sur la faisabilité
    push(@tabFiltersOptions, {
        'id' => 'doableState',
        'container' => '#filter-doable',
        'type' => 'list',
        'options' => {
            'delay'  => $updateFilterDelay,
            'width' => 450
        },
        'labels' => {
            'title'    => $locale->t('Réalisation'),
            'inactive' => $locale->t('Tous les états de réalisation'),
            'noSelection' => $locale->t('Choisir un état de réalisation'),
            'selections'  => $locale->t('Les états %s', '<%values>'),
            'selection'   => $locale->t('L\'état %s', '<%value>'),
            'noItemAvlb'  => $locale->t('Aucun état disponible')
        }
    });
    
    # Filtre sur les dates des transports
    push(@tabFiltersOptions, {
        'id' => 'dateDiff',
        'container' => '#filter-date-diff',
        'type' => 'list',
        'options' => {
            'delay'  => $updateFilterDelay,
            'isSingle' => 1
        },
        'labels' => {
            'title'       => $locale->t('Date des commandes de transport'),
            'inactive'    => $locale->t('N\'importe quelle date'),
            'noSelection' => $locale->t('Choisir une date'),
            'selections'  => '<%values>',
            'selection'   => '<%value>',
            'noItemAvlb'  => $locale->t('Aucune date de cdes transport')
        }
    });
    
    # Filtre sur les numéros de commande
    if (!$tabViewData{'isSelf'})
    {
        push(@tabFiltersOptions, {
            'id' => 'order.code',
            'container' => '#filter-order',
            'type' => 'list',
            'options' => {
                'delay'  => $updateFilterDelay,
                'search' => 1
            },
            'labels' => {
                'title'        => $locale->t('Cde(s) de transport'),
                'inactive'     => $locale->t('Toutes les cdes de transport'),
                'noSelection'  => $locale->t('Choisir les cdes de transport'),
                'selections'   => $locale->t('Les cdes de transport %s', '<%values>'),
                'selection'    => $locale->t('La cde de transport %s', '<%value>'),
                'noItemAvlb'   => $locale->t('Aucune cde de transport disponible'),
                'search'       => $locale->t('Rechercher une cde de transport'),
                'clearSearch'  => $locale->t('Vider la saisie'),
                'noSearchRslt' => $locale->t('Pas de correspondance')
            }
        });
    }
    
    # Filtre sur les adresses de provenance
    push(@tabFiltersOptions, {
        'id' => 'from-address',
        'container' => '#filter-from-address',
        'type' => 'list',
        'options' => {
            'delay'  => $updateFilterDelay,
            'width' => 350,
            'search' => 1
        },
        'labels' => {
            'title'        => $locale->t('Provenance(s)'),
            'inactive'     => $locale->t('Toutes les provenances'),
            'noSelection'  => $locale->t('Choisir les provenances'),
            'selections'   => $locale->t('Les provenances %s', '<%values>'),
            'selection'    => $locale->t('La provenance %s', '<%value>'),
            'noItemAvlb'   => $locale->t('Aucune provenance disponible'),
            'search'       => $locale->t('Rechercher une provenance'),
            'clearSearch'  => $locale->t('Vider la saisie'),
            'noSearchRslt' => $locale->t('Pas de correspondance')
        }
    });
    
    # Filtre sur les adresses de destination
    push(@tabFiltersOptions, {
        'id' => 'to-address',
        'container' => '#filter-to-address',
        'type' => 'list',
        'options' => {
            'delay' => $updateFilterDelay,
            'width' => 350,
            'search' => 1
        },
        'labels' => {
            'title'        => $locale->t('Destination(s)'),
            'inactive'     => $locale->t('Toutes les destinations'),
            'noSelection'  => $locale->t('Choisir les destinations'),
            'selections'   => $locale->t('Les destinations %s', '<%values>'),
            'selection'    => $locale->t('La destination %s', '<%value>'),
            'noItemAvlb'   => $locale->t('Aucune destination disponible'),
            'search'       => $locale->t('Rechercher une destination'),
            'clearSearch'  => $locale->t('Vider la saisie'),
            'noSearchRslt' => $locale->t('Pas de correspondance')
        }
    });
    
    # Filtre sur les clients
    push(@tabFiltersOptions, {
        'id' => 'customer.id',
        'container' => '#filter-customer',
        'type' => 'list',
        'options' => {
            'delay' => $updateFilterDelay,
            'search' => 1
        },
        'labels' => {
            'title'        => $locale->t('Client(s)'),
            'inactive'     => $locale->t('Tous les clients'),
            'noSelection'  => $locale->t('Choisir les clients'),
            'selections'   => $locale->t('Les clients %s', '<%values>'),
            'selection'    => $locale->t('Le client %s', '<%value>'),
            'noItemAvlb'   => $locale->t('Aucun client disponible'),
            'search'       => $locale->t('Rechercher un client'),
            'clearSearch'  => $locale->t('Vider la saisie'),
            'noSearchRslt' => $locale->t('Pas de correspondance')
        }
    });
    
    # Filtre sur les numéros de parc
    push(@tabFiltersOptions, {
        'id' => 'machine.parkNumber',
        'container' => '#filter-park-number',
        'type' => 'search',
        'options' => {
            'delay' => 500
        },
        'labels' => {
            'title'       => $locale->t('N° de parc'),
            'inactive'    => $locale->t('Tous les n° de parc'),
            'noSelection' => $locale->t('Choisir le n° de parc'),
            'selection'   => $locale->t('Le n° de parc %s', '<%search>'),
            'search'      => $locale->t('Saisir un n° de parc'),
            'clearSearch' => $locale->t('Vider la saisie')
        }
    });
}




# Paramètrage du tableau
my %tabAjaxTableOptions = (
    'url'            => $view->createURL($tabViewData{'controlerAction'}, {'displayType' => 'json'}),
    'cssClasses'     => 'basic list',
    'startIndex'     => $tabViewData{'startIndex'},
    'pageSize'       => $tabViewData{'pageSize'},
    'sorts'          => $tabViewData{'sorts'},
    'activesFilters' => $tabViewData{'activesFilters'},
    'defaultActivesFilters' => $tabViewData{'defaultActivesFilters'},
    'data'           => $tabData,
    'paginations'    => '#header-pagination-container',
    'filtersRecaps'  => '#header-filters-recap',
    'columns'        => \@tabColumnsOptions,
    'filters'        => \@tabFiltersOptions,
    'labels'         => {
        'pagination' => {
            'noResult' => $locale->t('Pas de résultat'),
            'result'   => $locale->t('%s résultat', '<b>1</b>'),
            'results'  => $locale->t('%s-%s sur %s résultats', '<b><%start></b>', '<b><%end></b>', '<b><%total></b>')
        },
        'clearSel' => $locale->t('Vider la sélection'),
        'errorMsg' => $locale->t('Une erreur s\'est produite.Veuillez contacter le service informatique s.v.p.')
    }
);

if ($tabViewData{'isSelf'})
{
    $tabAjaxTableOptions{'labels'}->{'noResult'} = $locale->t('Aucun(e) enlèvement / restitution agence trouvé(e)');
}
else
{
    $tabAjaxTableOptions{'labels'}->{'noResult'} = $locale->t('Aucun transport en cours trouvé');
}


# Variables à passer au JS
print $view->displayJSBlock('
var isTelematActived = ' . $tabViewData{'isTelematActived'} . ';');

# Données à passer au JavaScript
my $doUrl = $view->createURL('transport:pending:do');
print $view->displayJSBlock('
$(function()
{
    transports.init(' . &LOC::Json::toJson({
        'tableOptions'   => \%tabAjaxTableOptions,
        'excelRowsLimit' => $tabViewData{'excelRowsLimit'},
        'doUrl'          => $doUrl,
        'countryId'      => $tabViewData{'user.country.id'}
    }) . ');
});');


# Affichage du pied de page
print $view->displayFooter();

