use utf8;
use open (':encoding(UTF-8)');

use strict;


use Spreadsheet::WriteExcel;

use LOC::Locale;
use LOC::Browser;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;



our %tabViewData;

my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});


# Fonctions utiles
sub displayBoolean
{
    my ($value) = @_;
    return ($value ? $locale->t('Oui') : $locale->t('Non'));
}


# Liste des types de transport
my %tabTypesLabels = (
    &LOC::Transport::TYPE_DELIVERY               => $locale->t('Livraison'),
    &LOC::Transport::TYPE_RECOVERY               => $locale->t('Récupération'),
    &LOC::Transport::TYPE_SITESTRANSFER          => $locale->t('Transfert inter-chantiers') . 
                                                    ' (' . $locale->t('Chargement') . ')',
    &LOC::Transport::TYPE_SITESTRANSFER . '_lnk' => $locale->t('Transfert inter-chantiers') .
                                                    ' (' . $locale->t('Déchargement') . ')',
    &LOC::Transport::TYPE_AGENCIESTRANSFER       => $locale->t('Transfert inter-agences')
);

my %tabTypesDocuments = (
    'estimateLine' => $locale->t('Ligne de devis'),
    'contract'     => $locale->t('Contrat')
);

# Téléchargement du fichier
my $fileName = $locale->t('Transports en cours') . '_' . POSIX::strftime('%Y%m%d_%H%M', localtime()) . '.xls';
$fileName =~ tr/"<>&\/\\?#\*;/'{}+____\-\-/;

&LOC::Browser::sendHeaders(
        "Content-Type: application/force-download\n" .
        "Content-Disposition: attachment; filename=\"" . $fileName . "\"\n" .
        "\n"
    );


# Nouveau classeur Excel envoyé sur la sortie standard
my $workbook = Spreadsheet::WriteExcel->new('-');
my $title = $locale->t('Transports en cours');
my $author = $locale->t('Gestion des locations') . ' ' . &LOC::Globals::get('version');
my $company = 'Accès industrie';
$workbook->set_properties('title' => $title, 'author' => $author, 'company' => $company);

# Format de la ligne d'en-tête
my $headerFmt = $workbook->add_format();
$headerFmt->set_italic();
$headerFmt->set_bold();
$headerFmt->set_bg_color($workbook->set_custom_color(12, 232, 243, 249));
$headerFmt->set_bottom();

# Format de date
my $dateFmt = $workbook->add_format();
$dateFmt->set_num_format(14);

# Format des booléens
my $boolFmt = $workbook->add_format();
$boolFmt->set_align('center');


# Formatage de la liste
my $tabData = $tabViewData{'data'};


my $worsheetLabel = $locale->t('Transports en cours');
my $worksheet = $workbook->add_worksheet($worsheetLabel);
$worksheet->activate();

# Entête et pied de page pour l'impression
$worksheet->set_header('&C&12&",Bold"' . $title . ' - ' . $worsheetLabel);
$worksheet->set_footer('&C&8' . $locale->t('Page') . ' &P / &N');


# Figer les volets
$worksheet->freeze_panes(1, 0);


# Ligne d'en-tête
my $c = 0;
my $l = 0;
$worksheet->write_string($l, $c, $locale->t('Agence'), $headerFmt);
$worksheet->set_column($c, $c++, 12);
$worksheet->write_string($l, $c, $locale->t('Type'), $headerFmt);
$worksheet->set_column($c, $c++, 24);
if (!$tabViewData{'isSelf'})
{
    $worksheet->write_string($l, $c, $locale->t('N° de commande'), $headerFmt);
    $worksheet->set_column($c, $c++, 18);
}
$worksheet->write_string($l, $c, $locale->t('Date'), $headerFmt);
$worksheet->set_column($c, $c++, 12);
$worksheet->write_string($l, $c, $locale->t('Horaire'), $headerFmt);
$worksheet->set_column($c, $c++, 12);
$worksheet->write_string($l, $c, $locale->t('Type de document'), $headerFmt);
$worksheet->set_column($c, $c++, 22);
$worksheet->write_string($l, $c, $locale->t('N° de document'), $headerFmt);
$worksheet->set_column($c, $c++, 22);
$worksheet->write_string($l, $c, $locale->t('Amiante'), $headerFmt);
$worksheet->set_column($c, $c++, 12);
$worksheet->write_string($l, $c, $locale->t('Provenance'), $headerFmt);
$worksheet->set_column($c, $c++, 32);
$worksheet->write_string($l, $c, $locale->t('Destination'), $headerFmt);
$worksheet->set_column($c, $c++, 32);
$worksheet->write_string($l, $c, $locale->t('N° parc'), $headerFmt);
$worksheet->set_column($c, $c++, 12);
$worksheet->write_string($l, $c, $locale->t('Modèle de machines'), $headerFmt);
$worksheet->set_column($c, $c++, 24);
$worksheet->write_string($l, $c, $locale->t('Client'), $headerFmt);
$worksheet->set_column($c, $c++, 27);
$worksheet->write_string($l, $c, $locale->t('Réalisable'), $headerFmt);
$worksheet->set_column($c, $c++, 14);

# Activation des filtres
$worksheet->autofilter(0, 0, 0, $c - 1);

$l++;



# Lignes de devis
foreach my $tabRow (@{$tabData->{'rows'}})
{
    if ($tabRow->{'type'} ne LOC::Transport::TYPE_SITESTRANSFER || !$tabRow->{'isDone'})
    {
        $c = 0;

        $worksheet->write_string($l, $c++, $tabRow->{'agency.id'});
        $worksheet->write_string($l, $c++, $tabTypesLabels{$tabRow->{'type'}});
        if (!$tabViewData{'isSelf'})
        {
            $worksheet->write_string($l, $c++, $tabRow->{'order.code'});
        }
        my $documentCode = $tabRow->{'document.code'};
        if ($tabRow->{'document.index'})
        {
            $documentCode .= ' (' . $locale->t('ligne') . ' ' . $tabRow->{'document.index'} . ')';
        }

        $worksheet->write_date_time($l, $c++, $tabRow->{'date'} . 'T', $dateFmt);
        $worksheet->write_string($l, $c++, $tabRow->{'hour'});
        $worksheet->write_string($l, $c++, $tabTypesDocuments{$tabRow->{'document.type'}});
        $worksheet->write_string($l, $c++, $documentCode);
        $worksheet->write_string($l, $c++, &displayBoolean($tabRow->{'document.isAsbestos'}), $boolFmt);
        $worksheet->write_string($l, $c++, $tabRow->{'from.label'} . ' - ' . $tabRow->{'from.address'});
        $worksheet->write_string($l, $c++, $tabRow->{'to.label'} . ' - ' . $tabRow->{'to.address'});
        $worksheet->write_string($l, $c++, $tabRow->{'machine.parkNumber'});
        $worksheet->write_string($l, $c++, $tabRow->{'model.label'});
        $worksheet->write_string($l, $c++, $tabRow->{'customer.label'});
        $worksheet->write_string($l, $c++, &displayBoolean($tabRow->{'isDoable'}), $boolFmt);

        $l++;
    }

    # Pour les transferts inter-chantiers on duplique la ligne pour le déchargement
    if ($tabRow->{'type'} eq LOC::Transport::TYPE_SITESTRANSFER)
    {
        $c = 0;

        $worksheet->write_string($l, $c++, $tabRow->{'agency.id'});
        $worksheet->write_string($l, $c++, $tabTypesLabels{$tabRow->{'type'} . '_lnk'});
        if (!$tabViewData{'isSelf'})
        {
            $worksheet->write_string($l, $c++, $tabRow->{'order.code'});
        }
        $worksheet->write_date_time($l, $c++, $tabRow->{'date'} . 'T', $dateFmt);
        $worksheet->write_string($l, $c++, $tabRow->{'linkHour'});
        $worksheet->write_string($l, $c++, $tabTypesDocuments{$tabRow->{'document.type'}});
        $worksheet->write_string($l, $c++, $tabRow->{'linkContract.code'});
        $worksheet->write_string($l, $c++, &displayBoolean($tabRow->{'linkContract.isAsbestos'}), $boolFmt);
        $worksheet->write_string($l, $c++, $tabRow->{'from.label'} . ' - ' . $tabRow->{'from.address'});
        $worksheet->write_string($l, $c++, $tabRow->{'to.label'} . ' - ' . $tabRow->{'to.address'});
        $worksheet->write_string($l, $c++, $tabRow->{'machine.parkNumber'});
        $worksheet->write_string($l, $c++, $tabRow->{'model.label'});
        $worksheet->write_string($l, $c++, $tabRow->{'linkCustomer.label'});
        $worksheet->write_string($l, $c++, &displayBoolean($tabRow->{'isDoable'}), $boolFmt);

        $l++;
    }

}

# Fermeture du classeur
$workbook->close();
