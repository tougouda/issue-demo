use utf8;
use open (':encoding(UTF-8)');
binmode STDOUT, ":raw";    # Pour envoyer le flux en l'état

use strict;
use locale;

use LOC::Date;
use LOC::Locale;
use LOC::Pdf;
use LOC::Util;
use LOC::Browser;


our $tabViewData;

# Locale
my $locale = &LOC::Locale::getLocale($tabViewData->{'locale.id'});

# Dimensions de la page
my $margin = 10/mm;
my $pageWidth = 210/mm;
my $textWidth = $pageWidth - 2 * $margin;

my $pageHeight = 297/mm;
my $textHeight = $pageHeight - 2 * $margin;

my $pageLimit = $margin;

my $gap = 2/pt;

# Création de l'objet PDF
my $tabTranslations = {};
my $side  = $tabViewData->{'side'};
if ($side eq LOC::Transport::SIDE_DELIVERY)
{
    $tabTranslations = {
        'generic'    => $locale->t('Enlèvement'),
        'title'      => $locale->t('Bordereau d\'enlèvement'),
        'terms'      => $locale->t('Modalités d\'enlèvement'),
        'machine'    => $locale->t('Matériel enlevé'),
        'date'       => $locale->t('Date d\'enlèvement'),
        'hour'       => $locale->t('Heure d\'enlèvement')
    };
}
elsif ($side eq LOC::Transport::SIDE_RECOVERY)
{
    $tabTranslations = {
        'generic'    => $locale->t('Restitution'),
        'title'      => $locale->t('Bordereau de restitution'),
        'terms'      => $locale->t('Modalités de restitution'),
        'machine'    => $locale->t('Matériel restitué'),
        'date'       => $locale->t('Date de restitution'),
        'hour'       => $locale->t('Heure de restitution')
    };
}

my $pdf = LOC::Pdf->new($tabTranslations->{'title'}, $tabViewData->{'agency.id'}, \$locale);
my $pdfObj = $pdf->getPdfObject();

# Polices
my $regularFont    = $pdf->{'font'}->{'regular'};
my $italicFont     = $pdf->{'font'}->{'italic'};
my $boldFont       = $pdf->{'font'}->{'bold'};
my $boldItalicFont = $pdf->{'font'}->{'bolditalic'};
my $dingbatsFont   = $pdfObj->corefont('ZapfDingbats');


## Définition du sujet et des mots-clés
$pdf->setSubject($tabTranslations->{'title'} . ', ' . $locale->t('Client') . ' ' . $tabViewData->{'tabCustomer'}->{'name'});
$pdf->setKeywords($tabTranslations->{'title'});

# Nouvelle page
my $page = $pdf->addPage();
my $text = $page->text();
my $gfx  = $page->gfx(1);

# Numéro du document
my $number = $locale->t('N° %s du %s', $tabViewData->{'id'}, $locale->getDateFormat($tabViewData->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC));
$gfx->textlabel(10/mm + 95/mm, 297/mm - 35/mm - 16/pt * 1.2, $boldFont, 10/pt, $number);

# Infos Commande
my $tabOrderInfos = [
    {
        'label'  => $locale->t('N° commande'),
        'value'  => $tabViewData->{'document.orderNo'}
    },
    {
        'label'  => $locale->t('Contact'),
        'value'  => $tabViewData->{'tabCustomer'}->{'orderContact.fullName'}
    },
    {
        'label'  => '',
        'value'  => $tabViewData->{'tabCustomer'}->{'orderContact.telephone'}
    }
];
$pdf->addCustomBlock(\$page, $locale->t('Commande'), $tabOrderInfos, {'x' => $margin, 'y' => 55/mm});

# Infos Contrat
my $tabContractInfos = [
    {
        'label'  => $locale->t('Numéro'),
        'value'  => $tabViewData->{'document.code'}
    },
    {
        'label'  => $locale->t('Chantier'),
        'value'  => $tabViewData->{'tabSite'}->{'label'}
    },
    {
        'label'  => '',
        'value'  => $tabViewData->{'tabSite'}->{'locality'}
    },
    {
        'label'  => $locale->t('Contact'),
        'value'  => $tabViewData->{'tabSite'}->{'contact.fullName'}
    },
    {
        'label'  => '',
        'value'  => $tabViewData->{'tabSite'}->{'contact.telephone'}
    }
];
$pdf->addCustomBlock(\$page, $locale->t('Contrat de location'), $tabContractInfos, {'x' => $margin, 'y' => 80/mm});


# Coordonnées client
my $tabCustomer = [
   $tabViewData->{'tabCustomer'}->{'name'},
   $tabViewData->{'tabCustomer'}->{'address1'},
   $tabViewData->{'tabCustomer'}->{'address2'},
   $tabViewData->{'tabCustomer'}->{'locality.postalCode'} . ' ' . $tabViewData->{'tabCustomer'}->{'locality.name'}
];
$pdf->addCustomerBlock(\$page, $tabCustomer);



my $x = $margin;
my $xSection = $x + 10/mm;
my $y = $pageHeight - 110/mm;
$text->lead(12/pt);

# ----- Contenu -------

# -- Modalités
&printSectionTitle($tabTranslations->{'terms'}, $x, $y);

# --- Adresse d'enlèvement ou de restitution
$y -= $text->lead();
&printSubSectionTitle($locale->t('Adresse'), $x, $y);

$y -= ($text->lead() + 2/mm);

$text->translate($xSection, $y);

if ($tabViewData->{'type'} ne LOC::Transport::TYPE_SITESTRANSFER)
{
    # Libellé
    my $agencyLabel = 'ACCES INDUSTRIE - ' . $locale->t('Agence de %s', $tabViewData->{'tabAgency'}->{'label'});
    $text->text($agencyLabel);
    # Adresse
    foreach my $line (split(/\n/, $tabViewData->{'tabAgency'}->{'address'}))
    {
        $y -= $text->lead();
        $text->translate($xSection, $y);
        $text->text($line);
    }
    # Ville
    $y -= $text->lead();
    $text->translate($xSection, $y);
    $text->text($tabViewData->{'tabAgency'}->{'postalCode'} . ' ' . $tabViewData->{'tabAgency'}->{'city'});
}
else
{
    $text->text($tabViewData->{'tabOtherSite'}->{'label'});
    foreach my $line (split(/\n/, $tabViewData->{'tabOtherSite'}->{'address'}))
    {
        $y -= $text->lead();
        $text->translate($xSection, $y);
        $text->text($line);
    }
    $y -= $text->lead();
    $text->translate($xSection, $y);
    $text->text($tabViewData->{'tabOtherSite'}->{'locality'});
}

# --- Date prévue
$y -= ($text->lead() * 2);
&printSubSectionTitle($locale->t('Date prévue'), $x, $y);

$y -= ($text->lead() + 2/mm);
$text->translate($xSection, $y);
if ($tabViewData->{'window'}->{'beginHour'} ne $tabViewData->{'window'}->{'endHour'})
{
    $text->text($locale->t('Le %s entre %s et %s', $locale->getDateFormat($tabViewData->{'window'}->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC), 
                                               $tabViewData->{'window'}->{'beginHour'}, 
                                               $tabViewData->{'window'}->{'endHour'}));
}
else
{
    $text->text($locale->t('Le %s à %s', $locale->getDateFormat($tabViewData->{'window'}->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC), 
                                               $tabViewData->{'window'}->{'beginHour'}));
}


# -- Matériel
$y -= ($text->lead() * 2);
&printSectionTitle($tabTranslations->{'machine'}, $x, $y);
# --- description
$y -= $text->lead();
&printSubSectionTitle($tabViewData->{'tabMachine'}->{'model.fullName'}, $x, $y);


$y -= ($text->lead() + 2/mm);

$text->translate($xSection, $y);
$text->text($tabViewData->{'tabMachine'}->{'model.label'});
my $lineWidth = $gfx->advancewidth($tabViewData->{'tabMachine'}->{'model.label'}, 'font' => $regularFont, 'fontsize' => 9/pt);

$text->font($boldFont, 9/pt);
$text->translate($xSection + $lineWidth, $y);
$text->text(' n°' . $tabViewData->{'tabMachine'}->{'parkNumber'});

$text->font($regularFont, 9/pt);

$y -= $text->lead();
$text->translate($xSection, $y);
$text->text($locale->t('N° de série : %s', $tabViewData->{'tabMachine'}->{'serialNumber'}));

$y -= $text->lead();
$text->font($regularFont, 7/pt);
$text->translate($xSection, $y);
$text->text($locale->t('Longueur : %01.2f m', $tabViewData->{'tabMachine'}->{'model.length'}));

my $colWidth = 30/mm;

$text->translate($xSection + $colWidth, $y);
$text->text($locale->t('Largeur : %01.2f m', $tabViewData->{'tabMachine'}->{'model.width'}));

$text->translate($xSection + $colWidth * 2, $y);
$text->text($locale->t('Hauteur : %01.2f m', $tabViewData->{'tabMachine'}->{'model.restHeight'}));

$text->translate($xSection + $colWidth * 3, $y);
$text->text($locale->t('Poids : %01.0f kg', $tabViewData->{'tabMachine'}->{'model.weight'}));

$text->font($regularFont, 9/pt);

$y -= $text->lead();
$text->translate($xSection, $y);
$text->text($locale->t('Documents joints'));

$text->font($regularFont, 7/pt);
$y -= $text->lead();
$text->translate($xSection + 5/mm, $y);

$gfx->linewidth(0.5/pt);
$gfx->move($xSection, $y + 1.5/pt);
$gfx->line($xSection + 2/mm, $y + 1.5/pt);
$gfx->stroke();
$text->text($locale->t('Manuel d\'utilisation'));
$y -= $text->lead();

$gfx->move($xSection, $y + 1.5/pt);
$gfx->line($xSection + 2/mm, $y + 1.5/pt);
$gfx->stroke();
$text->translate($xSection + 5/mm, $y);
$text->text($locale->t('Rapport de VGP'));
$y -= $text->lead();

$gfx->move($xSection, $y + 1.5/pt);
$gfx->line($xSection + 2/mm, $y + 1.5/pt);
$gfx->stroke();
$text->translate($xSection + 5/mm, $y);
$text->text($locale->t('CE'));

$text->font($regularFont, 9/pt);

$y -= $text->lead();
$text->translate($xSection, $y);
$text->text($tabViewData->{'tabSite'}->{'comments'});


# -- Bloc général
$y -= ($text->lead() * 2);

&printSectionTitle($tabTranslations->{'generic'}, $x, $y);
#
# --- Observations
my $colWidth = ($pageWidth - ($margin * 3)) / 2;

$y -= $text->lead();
my $topY = $y;

$text->translate($x, $y);
$text->text($locale->t('Observations') . ' : ');
$y -= ($text->lead() + 2/mm);

my $i = 0;
$gfx->strokecolor('grey');
$gfx->fillcolor('#ECF5FB');
while ($i < 7)
{
    $gfx->rect($x, $y - $text->lead() * $i, $colWidth, $text->lead());
    $gfx->fill();

    $gfx->move($x, $y - $text->lead() * $i);
    $gfx->line($x + $colWidth, $y - $text->lead() * $i);
    
    $gfx->stroke();
    $i++;
}

# Affichage de l'info amiante
# - si restitution
if ($side eq LOC::Transport::SIDE_RECOVERY &&
    $tabViewData->{'tabSite'}->{'isAsbestos'})
{
    $text->translate($x, $y + 1/mm);
    $text->font($boldFont, 9/pt);
    $text->text($locale->t('CHANTIER AMIANTE'));
}


$text->font($regularFont, 9/pt);
# --- Date / Heure / Signature
$x = $x + $colWidth + $margin;
$y = $topY;

# plus grand libellé pour alignement
my $dateWidth =  $gfx->advancewidth($tabTranslations->{'date'} . ' : ');
my $hourWidth =  $gfx->advancewidth($tabTranslations->{'hour'} . ' : ');
my $offset = ($dateWidth > $hourWidth ? $dateWidth : $hourWidth);
my $width  = $colWidth - $offset - 5/mm;

# date
$text->translate($x, $y);
$text->text($tabTranslations->{'date'} . ' : ');
$gfx->rect($x + $offset + 5/mm, $y, $width, $text->lead());
$gfx->fill();
$gfx->move($x + $offset + 5/mm, $y);
$gfx->line($x + $offset + 5/mm + $width, $y);
$gfx->stroke();

# heure
$y -= $text->lead();
$text->translate($x, $y);
$text->text($tabTranslations->{'hour'} . ' : ');
$gfx->rect($x + $offset + 5/mm, $y, $width, $text->lead());
$gfx->fill();
$gfx->move($x + $offset + 5/mm, $y);
$gfx->line($x + $offset + 5/mm + $width, $y);
$gfx->stroke();

# signature
$y -= ($text->lead() *2);
$text->translate($x, $y);
$text->text($locale->t('Signature') . ' : ');

$gfx->fillcolor('black');
$gfx->strokecolor('black');

# Sortie
my $pdfString = $pdf->getString();
$pdf->end();

my $filename = sprintf('Enlevement_%s_%s_%s.pdf', $tabViewData->{'tabCustomer'}->{'code'},
                                                  $tabViewData->{'id'},
                                                  $tabViewData->{'window'}->{'date'});
&LOC::Browser::sendHeaders(
        "Content-type: application/pdf;\n"
        . "Content-Disposition: inline; filename=\"" . $filename . "\";\n\n"
    );

print $pdfString;


# Function: printSectionTitle
# Affiche le titre d'une section
#
# Parameters
# string     $title   - Titre de la section
# string     $x       - Coordonnées x dans la page
# string     $y       - Coordonnées y dans la page
sub printSectionTitle
{
    my ($title, $x, $y) = @_;
    $gfx->textlabel($x, $y, $regularFont, 9/pt, uc($title), -color => LOC::Pdf::COLOR_EMPHASIS);
}

# Function: printSubSectionTitle
# Affiche le sous-titre d'une section
#
# Parameters
# string     $subTitle - Sous-titre
# string     $x        - Coordonnées x dans la page
# string     $y        - Coordonnées y dans la page
#
# Return
# string $y - Nouvelle coordonnée y
sub printSubSectionTitle
{
    my ($subTitle, $x, $y) = @_;

    # Affichage sous-titre
    $text->font($boldItalicFont, 9/pt);
    $text->translate($x, $y);
    $text->text($subTitle);

    # ligne
    $y -= 2/mm;
    $gfx->linewidth(0.5/pt);
    $gfx->move($x, $y);
    $gfx->line($pageWidth - $margin, $y);
    $gfx->stroke();

    $text->font($regularFont, 9/pt);
}

