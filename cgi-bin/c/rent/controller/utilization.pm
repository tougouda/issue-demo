use utf8;
use open (':encoding(UTF-8)');

# Package: utilization
# Contrôleur pour les taux d'utilisation
package utilization;

use strict;
use File::Basename;

use LOC::Agency;
use LOC::Area;
use LOC::Characteristic;
use LOC::Db;
use LOC::Json;
use LOC::TariffFamily;
use LOC::Util;

# Function: viewAction
# Affichage des taux d'utilisation
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Récupération des paramètres
    my $countryId   = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $displayType = &LOC::Request::getString('displayType', '');

    my $areaId    = $tabUserInfos->{'tabAuthorizedAgencies'}->{$tabUserInfos->{'nomadAgency.id'}}->{'area.id'};

    # Informations sur la région
    my $tabAreaData = &LOC::Area::getInfos($areaId);

    # Liste des agences de la région
    my $tabAgencyFilter = {'type' => LOC::Agency::TYPE_AGENCY};
    if ($areaId == 0)
    {
        $tabAgencyFilter->{'country'} = $countryId;
    }
    else
    {
        $tabAgencyFilter->{'area'} = $areaId;
    }
    my $tabAgency = &LOC::Agency::getList(LOC::Util::GETLIST_PAIRS, $tabAgencyFilter);
    $tabAgency = &LOC::Util::sortHashByKeys($tabAgency);

    # Liste des familles tarifaires
    my $tabAgencyFilter = {'type' => LOC::Agency::TYPE_AGENCY};
    if ($areaId == 0)
    {
        $tabAgencyFilter->{'country'} = $countryId;
    }
    else
    {
        $tabAgencyFilter->{'area'} = $areaId;
    }
    my $tabTariffFamily = &LOC::TariffFamily::getList($countryId, LOC::Util::GETLIST_ASSOC);

    # Récupération de la caractéristique
    my $json = &LOC::Characteristic::getValueByCode('VISUTU');

    # Familles tarifaires à exclure
    my $tabTariffFamilyToExclude = [];

    # Liste des états
    my $tabState = [];

    if ($json)
    {
        my $decodedJson = &LOC::Json::fromJson($json);
        $tabTariffFamilyToExclude = $decodedJson->{'tariffFamiliesToExclude'};
        $tabState                 = $decodedJson->{'states'};
    }
    foreach my $tariffFamilyId (@$tabTariffFamilyToExclude)
    {
        if (defined $tabTariffFamily->{$tariffFamilyId})
        {
            delete($tabTariffFamily->{$tariffFamilyId});
        }
    }

    my $tabData = &LOC::TariffFamily::getUseRateData($countryId, $areaId, $tabAgency, $tabTariffFamily,
                                                     $tabTariffFamilyToExclude);

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'       => $tabUserInfos->{'locale.id'},
        'user.agency.id'  => $tabUserInfos->{'nomadAgency.id'},
        'area.id'         => $tabAreaData->{'id'},
        'area.label'      => $tabAreaData->{'label'},
        'tabAgency'       => $tabAgency,
        'tabState'        => $tabState,
        'tabTariffFamily' => $tabTariffFamily,
        'tabData'         => $tabData,
    );

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;

    if ($displayType eq 'pdf')
    {
        require $directory . '/view/utilization/pdf.cgi';
    }
    else
    {
        require $directory . '/view/utilization/view.cgi';
    }
}

1;