use utf8;
use open (':encoding(UTF-8)');

# Package: rentContract
# Exemple de contrôleur
package rentContract;

# Constants: Paramètres d'affichage
# NBSEARCHSTEP - Nombre de contrats à afficher dans une recherche et lors du clic sur "Voir plus"
use constant
{
    NBSEARCHSTEP        => 100
};

use strict;
use File::Basename;
use File::Copy;

use LOC::Agency;
use LOC::Area;
use LOC::Contract::Rent;
use LOC::Estimate::Rent;
use LOC::Country;
use LOC::Customer;
use LOC::Model;
use LOC::TariffFamily;
use LOC::Date;
use LOC::Characteristic;
use LOC::Common::RentService;
use LOC::Transport::TimeSlot;
use LOC::Proforma;
use LOC::Transport;
use LOC::Json;
use LOC::Attachments;
use LOC::MachineInventory;
use LOC::Day;
use LOC::Day::InvoiceDuration;
use LOC::Day::Reason;
use LOC::Insurance::Type;
use LOC::Appeal::Type;
use LOC::Util;
use LOC::Repair::Sheet;
use LOC::Repair::Estimate;


# Function: viewAction
# Affichage du contrat de location
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.id'        => $tabUserInfos->{'id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'timeZone'       => $tabSession->{'timeZone'}
    );
    our %tabSearchData = ();


    my $userId         = $tabUserInfos->{'id'};
    my $countryId      = $tabUserInfos->{'nomadCountry.id'};
    my $agencyId       = $tabUserInfos->{'nomadAgency.id'};

    my $contractId     = &LOC::Request::getInteger('contractId', undef);
    my $estimateLineId = &LOC::Request::getInteger('estimateLineId', undef);

    my $editMode = (!defined $contractId || $estimateLineId != 0 ? 'create' : 'update');

    # Données du formulaire de la barre de recherche
    $tabSearchData{'country.id'} = $countryId;

    # Données de la barre de recherche
    $tabSearchData{'states'} = &LOC::Contract::Rent::getStates($countryId);

    my $tabTariffFamilies = {};
    tie(%$tabTariffFamilies, 'Tie::IxHash');
    $tabTariffFamilies = &LOC::TariffFamily::getList($countryId, 
                                               LOC::Util::GETLIST_PAIRS,
                                               {},
                                               {}, 
                                               {'sort' => [ {'field' => 'fulllabel', 'dir' => 'ASC'} ]}
                                               );
    $tabSearchData{'tariffFamilies'} = $tabTariffFamilies;


    $tabViewData{'documentType'} = 'contract';
    $tabViewData{'editMode'}     = $editMode;
    $tabViewData{'tempId'}       = 'TMP' . time() . '_' . $agencyId . '_' . $userId;


    my $tabError = {};
    my $tabContractInfos;

    if ($estimateLineId ne '')
    {
        ############################
        # Passage d'une ligne de devis en contrat
        ############################

        # Récupération des données du contrat par rapport à la ligne de devis
        $tabContractInfos = &_getContractInfosFromEstimateLine($countryId, $tabUserInfos, $estimateLineId,
                                                               $tabViewData{'tempId'}, $tabError);
    }
    elsif ($editMode eq 'update')
    {
        ############################
        # Mise à jour d'un contrat
        ############################

        # Récupération des données du contrat
        $tabContractInfos = &_getContractInfos($countryId, $tabUserInfos, $contractId, $tabError);
    }
    elsif ($editMode eq 'create')
    {
        ############################
        # Création d'un contrat
        ############################

        # Récupération des informations du devis
        $tabContractInfos = &_getNewContractInfos($countryId, $tabUserInfos, $agencyId, $tabError);
    }


    # Redirection à la page d'erreur
    if (!$tabContractInfos)
    {
        # Impossible de passer la ligne en contrat
        $tabViewData{'tabError'} = $tabError;

        my $directory = dirname(__FILE__);
        $directory =~ s/(.*)\/.+/$1/;
        require $directory . '/view/rentContract/error.cgi';
        return 1;
    }

    my $contractCountryId = $tabContractInfos->{'country.id'};
    my $contractAgencyId  = $tabContractInfos->{'agency.id'};
    my $contractCreatorId = $tabContractInfos->{'creator.id'};
    my $contractValueDate = $tabContractInfos->{'valueDate'};

    # Informations sur le pays
    my $tabCountryInfos = &LOC::Country::getInfos($contractCountryId);


    # Données pour le bloc d'informations générales
    my $tabAgencies = &LOC::Agency::getList(LOC::Util::GETLIST_PAIRS, {'country' => $contractCountryId,
                                                                       'type' => LOC::Agency::TYPE_AGENCY});
    my $tabUsers    = &LOC::User::getList(LOC::Util::GETLIST_PAIRS, {'id' => $contractCreatorId},
                                          LOC::User::GET_ALL, {'formatMode' => 1});

    # Informations pour l'entête d'affichage
    $tabViewData{'tabDocumentInfos'} = {
        'agency.id'          => $tabContractInfos->{'agency.id'},
        'isLocked'           => $tabContractInfos->{'isLocked'},
        'creator.id'         => $tabContractInfos->{'creator.id'},
        'creationDate'       => $tabContractInfos->{'creationDate'},
        'linkedEstimateLine' => $tabContractInfos->{'linkedEstimateLine'},
        'invoicingStatus'    => $tabContractInfos->{'invoiceState'},
        'lastInvoicingDate'  => $tabContractInfos->{'lastInvoiceDate'},
        'closureDate'        => $tabContractInfos->{'closureDate'},
        'tabAgencies'        => $tabAgencies,
        'tabUsers'           => $tabUsers
    };

    # Informations du corps du contrat
    $tabViewData{'tabContractInfos'} = $tabContractInfos;

    # Liste des modèles de machines
    my $modelId = $tabContractInfos->{'tabModelInfos'}->{'id'};
    $tabViewData{'tabModelsList'} = &LOC::Model::getList($contractCountryId, LOC::Util::GETLIST_ASSOC,
                                                         {'rentable' => $contractAgencyId,
                                                          'addId'    => $modelId});
    if ($editMode eq 'update')
    {
        $tabViewData{'tabModelsList'}->{$modelId}->{'isRentable'} = 1;
    }


    # La tarification transport est-elle activée ?
    $tabViewData{'isTrspTarification'} = &LOC::Characteristic::getAgencyValueByCode('TRSPTARIF', $contractAgencyId) * 1;

    # Liste des plages horaires de transport
    $tabViewData{'tabTransportTimeSlots'} = &LOC::Transport::TimeSlot::getList($contractCountryId, LOC::Util::GETLIST_ASSOC,
                                                                               {'stateId' => [LOC::Transport::TimeSlot::STATE_ACTIVE,
                                                                                              LOC::Transport::TimeSlot::STATE_DELETED]});

    # Liste des types d'assurance
    my $tabInsuranceTypesList = &LOC::Insurance::Type::getList($contractCountryId, LOC::Util::GETLIST_ASSOC,
                                                                    {'stateId' => [LOC::Insurance::Type::STATE_ACTIVE, LOC::Insurance::Type::STATE_DELETED]});
    my @tabInsuranceTypes = values(%$tabInsuranceTypesList);
    $tabViewData{'insurance.types'} = \@tabInsuranceTypes;
    # Liste des taux d'assurance
    $tabViewData{'insurance.rates'} = $tabCountryInfos->{'insurance.rates'};

    # Liste des types de gestion de recours
    my $tabAppealTypesList = &LOC::Appeal::Type::getList($contractCountryId, LOC::Util::GETLIST_ASSOC,
                                                                    {'stateId' => [LOC::Appeal::Type::STATE_ACTIVE, LOC::Appeal::Type::STATE_DELETED]});
    my @tabAppealTypes = values(%$tabAppealTypesList);
    $tabViewData{'appeal.types'} = \@tabAppealTypes;

    # liste des services location et transport
    $tabViewData{'tabRentServiceTypes'} = &LOC::Common::RentService::getList($contractCountryId, LOC::Util::GETLIST_ASSOC);
    my $rentServiceMinAmount = &LOC::Characteristic::getAgencyValueByCode('MTMINSRV', $contractAgencyId);
    my $rentServiceNoLimitUserIdList = &LOC::Characteristic::getAgencyValueByCode('USRNOMAXREMSRV', $contractAgencyId);
    my @rentServiceNoLimitUserId = split(',', $rentServiceNoLimitUserIdList);
    # si l'utilisateur courant n'a pas de limite de services location et transport
    if (grep {$_ eq $userId} @rentServiceNoLimitUserId)
    {
        $rentServiceMinAmount = undef;
    }
    # Autres paramètres
    $tabViewData{'daysToStop'}              = &LOC::Characteristic::getAgencyValueByCode('DAYSTOSTOP', $contractAgencyId) * 1;
    $tabViewData{'tariffTrspEstimationUrl'} = &LOC::Globals::get('tariffTrspEstimationUrl');
    $tabViewData{'addMachinePrice'}         = &LOC::Characteristic::getAgencyValueByCode('ADDMACH', $contractAgencyId) * 1;
    $tabViewData{'addMachineMinPrice'}      = &LOC::Characteristic::getAgencyValueByCode(
                                                  'ADDMACHMIN', $contractAgencyId, $contractValueDate) * 1;
    $tabViewData{'defaultStdReducMode'}     = &LOC::Characteristic::getAgencyValueByCode('LOCREMSTDMODE', $contractAgencyId) * 1;
    $tabViewData{'useResiduesForAccessories'} = &LOC::Characteristic::getAgencyValueByCode(
                                                  'ACCDECHETS', $contractAgencyId, $contractValueDate) * 1;
    $tabViewData{'defaultAppealMode'}       = &LOC::Characteristic::getAgencyValueByCode(
                                                  'RECOURSMODE',$contractAgencyId, $contractValueDate) * 1;
    $tabViewData{'defaultAppealRate'}       = &LOC::Characteristic::getAgencyValueByCode(
                                                  'PCRECOURS', $contractAgencyId, $contractValueDate) * 1;
    $tabViewData{'defaultAppealTypeId'}     = &LOC::Appeal::Type::getDefaultId($contractCountryId);
    $tabViewData{'defaultResiduesMode'}     = &LOC::Characteristic::getAgencyValueByCode(
                                                  'DECHETSMODE',$contractAgencyId, $contractValueDate) * 1;
    $tabViewData{'defaultResiduesAmount'}   = &LOC::Characteristic::getAgencyValueByCode(
                                                  'MTDECHETS', $contractAgencyId, $contractValueDate) * 1;
    $tabViewData{'defaultResiduesRate'}     = &LOC::Characteristic::getAgencyValueByCode(
                                                  'PCDECHETS', $contractAgencyId, $contractValueDate) * 1;
    $tabViewData{'defaultInsuranceRate'}    = $tabCountryInfos->{'insurance.defaultRate'};
    $tabViewData{'defaultInsuranceTypeId'}  = &LOC::Insurance::Type::getDefaultId($contractCountryId);

    $tabViewData{'nbDaysMaxExtensionStopCtt'} = &LOC::Characteristic::getAgencyValueByCode('CONPROLONGARRET', $contractAgencyId) * 1;
    $tabViewData{'rentServiceMinAmount'}      = $rentServiceMinAmount;

    $tabViewData{'maxSitesTsfIntervals'}    = &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('DELTAMAXTSF', $contractAgencyId));

    $tabViewData{'customersSearchUrl'}      = &LOC::Request::createRequestUri('accounting:customer:jsonList',
                                                                              {'countryId'    => $contractCountryId,
                                                                               'isRestricted' => 1,
                                                                               'format'       => 0});
    $tabViewData{'siteLocalitiesSearchUrl'} = &LOC::Request::createRequestUri('site:site:siteLocalitiesList',
                                                                              {'country.id'      => $contractCountryId,
                                                                               'transportAgency' => $contractAgencyId,
                                                                               'format'          => 0});

    $tabViewData{'tabProformaTypes'} = &LOC::Proforma::Type::getList($contractCountryId, LOC::Util::GETLIST_PAIRS);
    $tabViewData{'tabProformaPaymentModes'} = &LOC::Proforma::getPaymentModesList($contractCountryId, LOC::Util::GETLIST_PAIRS);
    $tabViewData{'proformaAdvancePercent'} = &LOC::Characteristic::getCountryValueByCode('PERCACPTE', $contractCountryId);
    $tabViewData{'fuelUnitPrice'} = $tabCountryInfos->{'fuelUnitPrice'};

    # - Liste des motifs pours les jours +/-
    my $tabDayReasonsList = &LOC::Day::Reason::getList($contractCountryId, LOC::Util::GETLIST_ASSOC);
    $tabViewData{'tabDayReasonsList'} = &LOC::Util::getHashValues($tabDayReasonsList);
    # - Liste des durées possibles poru les jours +/-
    my $tabDayInvoiceDurationsList = &LOC::Day::InvoiceDuration::getList($contractCountryId, LOC::Util::GETLIST_ASSOC);
    $tabViewData{'tabDayInvoiceDurationsList'} = &LOC::Util::getHashValues($tabDayInvoiceDurationsList);
    # - Intervalles de jours pour les périodes possibles

    # - URL de recherche des contrats liés
    my $delayMinDateEnd = &LOC::Characteristic::getAgencyValueByCode(
                            'DELTAMAXCTTLIE', $contractAgencyId, $contractValueDate) * 1;
    $tabViewData{'appliedToContractSearchUrl'} = &LOC::Request::createRequestUri('default:contract:jsonList', {
        'format' => 2,
        'filters' => {
            'agencyId' => $contractAgencyId,
            'isClosed' => 1,
            'endDate'  => [&LOC::Date::subDays($contractValueDate, $delayMinDateEnd), undef]
        }
    });

    $tabViewData{'maxFileSize'} = &LOC::Characteristic::getAgencyValueByCode('MAXTAILLEDOC', $contractAgencyId) * 1;
    $tabViewData{'maxMailAttachmentsSize'} = &LOC::Characteristic::getAgencyValueByCode('MAXTAILLEDOCMAIL', $contractAgencyId) * 1;

    $tabViewData{'additionalAttachments'} = [];

    # Corps du mail par défaut
    if ($editMode eq 'update')
    {
        $tabViewData{'emailContent'} = &LOC::Characteristic::getAgencyValueByCode('MAILCONTRAT', $contractAgencyId);
        my $tabAdditionalAttachments = &LOC::Json::fromJson(&LOC::Characteristic::getCountryValueByCode('DOCAUTOCONTRAT', $contractCountryId));

        # Pièces jointes supplémentaires
        # - Télématique
        if ($tabViewData{'tabContractInfos'}->{'tabMachineInfos'}->{'isTelematicActivated'} && $tabAdditionalAttachments->{'telematics'})
        {
            foreach my $documentInfos (@{$tabAdditionalAttachments->{'telematics'}})
            {
                $documentInfos->{'url'} =~ s/<%commonFilesUrl>/@{[&LOC::Globals::get('commonFilesUrl')]}/;
                push(@{$tabViewData{'additionalAttachments'}}, {
                    'url'      => $documentInfos->{'url'},
                    'filename' => $documentInfos->{'label'}
                });
            }
        }
        # - Amiante
        if ($tabViewData{'tabContractInfos'}->{'tabSite'}->{'isAsbestos'} && $tabAdditionalAttachments->{'asbestos'})
        {
            foreach my $documentInfos (@{$tabAdditionalAttachments->{'asbestos'}})
            {
                $documentInfos->{'url'} =~ s/<%commonFilesUrl>/@{[&LOC::Globals::get('commonFilesUrl')]}/;
                push(@{$tabViewData{'additionalAttachments'}}, {
                    'url'      => $documentInfos->{'url'},
                    'filename' => $documentInfos->{'label'}
                });
            }
        }
    }

    # Onglet actif
    $tabViewData{'tabIndex'} = &LOC::Request::getInteger('tabIndex', undef);


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentContract/view.cgi';
}

# Function: modelInfosAction
# Récupérer les informations d'un modèle de machine
#
sub modelInfosAction
{
    # Variables pour la vue
    our %tabViewData = ();

    # Récupération des paramètres
    my $customerId = &LOC::Request::getInteger('customerId');
    my $contractId = &LOC::Request::getInteger('contractId');
    my $modelId    = &LOC::Request::getInteger('modelId');
    my $agencyId   = &LOC::Request::getString('agencyId');
    my $countryId  = &LOC::Request::getString('countryId');
    my $tabOptions = &LOC::Request::getHash('tabOptions');

    # Récupération des informations sur le modèle de machine
    $tabViewData{'tabModelInfos'} = &_getModelInfos($countryId, $modelId, $customerId, $agencyId, $tabOptions);

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentContract/modelInfos.cgi';
}


# Function: searchAction
# Recherche de contrat de location
sub searchAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'offset'         => 0,
        'step'           => NBSEARCHSTEP
    );
    our %tabSearchData = ();

    my $countryId = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});


    # Données du formulaire
    $tabSearchData{'country.id'}             = $countryId;
    $tabSearchData{'search.no'}              = &LOC::Request::getString('search.no');
    $tabSearchData{'search.state.id'}        = &LOC::Request::getString('search.state.id');
    $tabSearchData{'search.user.id'}         = &LOC::Request::getInteger('search.user.id');
    $tabSearchData{'search.customer.id'}     = &LOC::Request::getInteger('search.customer.id');
    $tabSearchData{'search.parkNo'}          = &LOC::Request::getString('search.parkNo');
    $tabSearchData{'search.site.label'}      = &LOC::Request::getString('search.site.label');
    $tabSearchData{'search.site.department'} = &LOC::Request::getString('search.site.department');
    $tabSearchData{'search.beginDate'}       = &LOC::Request::getString('search.beginDate');
    $tabSearchData{'search.endDate'}         = &LOC::Request::getString('search.endDate');
    $tabSearchData{'search.negotiator.id'}   = &LOC::Request::getString('search.negotiator.id');
    $tabSearchData{'search.isNational'}      = &LOC::Request::getBoolean('search.isNational');
    $tabSearchData{'search.tariffFamily.id'} = &LOC::Request::getInteger('search.tariffFamily.id');

    # Données de la barre de recherche
    $tabSearchData{'states'} = &LOC::Contract::Rent::getStates($countryId);
    $tabSearchData{'users'} = ();
    if ($tabSearchData{'search.user.id'} != 0)
    {
        $tabSearchData{'users'} = &LOC::User::getList(LOC::Util::GETLIST_PAIRS,
                                                      {'id' => $tabSearchData{'search.user.id'}},
                                                      LOC::User::GET_ACTIVE,
                                                      {'formatMode' => 1});
    }
    $tabSearchData{'customers'} = ();
    if ($tabSearchData{'search.customer.id'} != 0)
    {
        $tabSearchData{'customers'} = &LOC::Customer::getList($countryId, LOC::Util::GETLIST_PAIRS,
                                                              {'id' => $tabSearchData{'search.customer.id'}});
    }

    $tabSearchData{'negotiators'} = ();
    if ($tabSearchData{'search.negotiator.id'} != 0)
    {
        $tabSearchData{'negotiators'} = &LOC::User::getList(LOC::Util::GETLIST_PAIRS,
                                                      {'id' => $tabSearchData{'search.negotiator.id'}},
                                                      LOC::User::GET_ACTIVE,
                                                      {'formatMode' => 1});
    }

    my $tabTariffFamilies = {};
    tie(%$tabTariffFamilies, 'Tie::IxHash');
    $tabTariffFamilies = &LOC::TariffFamily::getList($countryId, 
                                               LOC::Util::GETLIST_PAIRS,
                                               {},
                                               {}, 
                                               {'sort' => [ {'field' => 'fulllabel', 'dir' => 'ASC'} ]}
                                               );
    $tabSearchData{'tariffFamilies'} = $tabTariffFamilies;

    # Résultats de la recherche
    my $tabFilters = {};
    if (!$tabSearchData{'search.isNational'})
    {
        my @tabAuthorizedAgenciesId = keys %{$tabUserInfos->{'tabAuthorizedAgencies'}};
        $tabFilters->{'agencyId'} = \@tabAuthorizedAgenciesId;
    }
    if ($tabSearchData{'search.no'} ne '')
    {
        my $codeSearch = $tabSearchData{'search.no'};
        if ($codeSearch !~ /[A-Za-z]{3}\d{10,}/)
        {
            $codeSearch = '%' . $codeSearch;
        }
        $tabFilters->{'code'} = $codeSearch;
    }
    if ($tabSearchData{'search.state.id'} ne '')
    {
        $tabFilters->{'stateId'} = $tabSearchData{'search.state.id'};
    }
    if ($tabSearchData{'search.user.id'} != 0)
    {
        $tabFilters->{'userId'} = $tabSearchData{'search.user.id'};
    }
    if ($tabSearchData{'search.customer.id'} != 0)
    {
        $tabFilters->{'customerId'} = $tabSearchData{'search.customer.id'};
    }
    if ($tabSearchData{'search.parkNo'} ne '')
    {
        $tabFilters->{'parkNo'} = $tabSearchData{'search.parkNo'};
    }
    if ($tabSearchData{'search.site.label'} ne '')
    {
        my $search = $tabSearchData{'search.site.label'};
        if ($search !~ /%/)
        {
            $search = '%' . $search . '%';
        }
        $tabFilters->{'siteLabel'} = $search;
    }
    if ($tabSearchData{'search.site.department'} ne '')
    {
        $tabFilters->{'siteDepartment'} = $tabSearchData{'search.site.department'};
    }
    if ($tabSearchData{'search.beginDate'} ne '')
    {
        $tabFilters->{'beginDate'} = $tabSearchData{'search.beginDate'};
    }
    if ($tabSearchData{'search.endDate'} ne '')
    {
        $tabFilters->{'endDate'} = $tabSearchData{'search.endDate'};
    }

    if ($tabSearchData{'search.negotiator.id'} != 0)
    {
        $tabFilters->{'negotiatorId'} = $tabSearchData{'search.negotiator.id'};
    }
    
    if ($tabSearchData{'search.tariffFamily.id'} != 0)
    {
        $tabFilters->{'tariffFamilyId'} = $tabSearchData{'search.tariffFamily.id'};
    }

    # nombre de critères sélectionnés
    $tabViewData{'nbFilters'} = keys %$tabFilters;
    $tabViewData{'tabFilters'} = $tabFilters;

    my $tabOptions = {
        'limit' => {
            'offset' => $tabViewData{'offset'},
            'count'  => $tabViewData{'step'},
            '&total' => 0
        }
    };
    my $tabContracts = &LOC::Contract::Rent::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                           $tabFilters,
                                                           &LOC::Contract::Rent::GETINFOS_BASE |
                                                           &LOC::Contract::Rent::GETINFOS_CUSTOMER |
                                                           &LOC::Contract::Rent::GETINFOS_EXT_SITE |
                                                           &LOC::Contract::Rent::GETINFOS_NEGOTIATOR,
                                                           $tabOptions);

    my @tabList;

    foreach my $tabContractInfos (values(%$tabContracts))
    {
        push(@tabList, {
            'id'              => $tabContractInfos->{'id'},
            'code'            => $tabContractInfos->{'code'},
            'customerName'    => $tabContractInfos->{'customer.name'},
            'siteLabel'       => $tabContractInfos->{'site.tabInfos'}->{'label'},
            'siteAddress'     => $tabContractInfos->{'site.tabInfos'}->{'locality.label'},
            'orderNo'         => $tabContractInfos->{'orderNo'},
            'stateLabel'      => $tabContractInfos->{'state.label'},
            'beginDate'       => $tabContractInfos->{'beginDate'},
            'endDate'         => $tabContractInfos->{'endDate'},
            'negotiatorFullName' => $tabContractInfos->{'negotiator.fullName'},
            'url'             => &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $tabContractInfos->{'id'}})
        });
    }

    $tabViewData{'result'} = \@tabList;
    $tabViewData{'total'}  = $tabOptions->{'limit'}->{'&total'};


    if ($tabViewData{'total'} == 1)
    {
        my $url = &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $tabList[0]->{'id'}});
        # Redirection
        &LOC::Browser::sendHeaders("Status: 302 Found\nLocation: " . $url . "\n\n");
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentContract/search.cgi';
}

# Function: jsonSearchAction
# Recherche de contrat de location
sub jsonSearchAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
    );

    my $countryId  = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $tabFilters = &LOC::Json::fromJson(&LOC::Request::getString('tabFilters', ''));
    $tabViewData{'offset'} = &LOC::Request::getInteger('offset', 0);
    $tabViewData{'step'}   = &LOC::Request::getInteger('step', 100);


    # nombre de critères sélectionnés
    $tabViewData{'nbFilters'} = keys %$tabFilters;
    $tabViewData{'tabFilters'}   = $tabFilters;

    # s'il y a assez de critères sélectionnés
    if ($tabViewData{'nbFilters'} > 0)
    {
        my $tabOptions = {
            'limit' => {
                'offset' => $tabViewData{'offset'},
                'count'  => $tabViewData{'step'},
                '&total' => 0
            }
        };
        my $tabContracts = &LOC::Contract::Rent::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                               $tabFilters,
                                                               &LOC::Contract::Rent::GETINFOS_BASE |
                                                               &LOC::Contract::Rent::GETINFOS_CUSTOMER |
                                                               &LOC::Contract::Rent::GETINFOS_EXT_SITE |
                                                               &LOC::Contract::Rent::GETINFOS_CREATOR,
                                                               $tabOptions);

        my @tabList;

        foreach my $tabContractInfos (values(%$tabContracts))
        {
            push(@tabList, {
                'id'              => $tabContractInfos->{'id'},
                'code'            => $tabContractInfos->{'code'},
                'customerName'    => $tabContractInfos->{'customer.name'},
                'siteLabel'       => $tabContractInfos->{'site.tabInfos'}->{'label'},
                'siteAddress'     => $tabContractInfos->{'site.tabInfos'}->{'locality.label'},
                'orderNo'         => $tabContractInfos->{'orderNo'},
                'stateLabel'      => $tabContractInfos->{'state.label'},
                'beginDate'       => $tabContractInfos->{'beginDate'},
                'endDate'         => $tabContractInfos->{'endDate'},
                'creatorFullName' => $tabContractInfos->{'creator.fullName'},
                'url'             => &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $tabContractInfos->{'id'}})
            });
        }

        $tabViewData{'result'} = \@tabList;
        $tabViewData{'total'}  = $tabOptions->{'limit'}->{'&total'};
    }


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentContract/jsonSearch.cgi';
}


# Function: rentalPeriodInfosAction
# Récupérer la durée de location entre deux dates pour une agence donnée
#
# Parameters:
# Parameters:
# string $dateBegin - Date de début au format MySQL
# string $dateEnd   - Date de fin au format MySQL
# string $agency    - Identifiant de l'agence
#
# Returns:
# hashref
sub rentalPeriodInfosAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Récupération des informations
    my $agencyId             = &LOC::Request::getString('agencyId');
    my $countryId            = &LOC::Request::getString('countryId');
    my $contractId           = &LOC::Request::getInteger('entityId');
    my $dateBegin            = &LOC::Request::getString('beginDate');
    my $dateEnd              = &LOC::Request::getString('endDate');
    my $adjustDays           = &LOC::Request::getInteger('adjustDays');
    my $requiredBeginDate    = &LOC::Request::getString('requiredBeginDate');
    my $requiredEndDate      = &LOC::Request::getString('requiredEndDate');
    my $requiredRealDuration = &LOC::Request::getInteger('requiredRealDuration');
    my $beginDateWindow      = &LOC::Request::getArray('beginDateWindow');
    my $endDateWindow        = &LOC::Request::getArray('endDateWindow');

    # Variables pour la vue
    our %tabViewData = (
        'duration'                     => 0,
        'durationStr'                  => '',
        'realDuration'                 => 0,
        'calendarDuration'             => 0,
        'rentTariffDurRangeLabel'      => '',

        'requiredBeginDate'               => $requiredBeginDate,
        'requiredEndDate'                 => $requiredEndDate,
        'requiredDuration'                => 0,
        'requiredDurationStr'             => '',
        'requiredRealDuration'            => 0,
        'requiredCalendarDuration'        => 0,
        'requiredRentTariffDurRangeLabel' => '',
        'authorizedPeriodAddDays'         => [],
        'authorizedPeriodDedDays'         => []
    );

    # Récupération des informations
    if ($dateBegin ne '' && $dateEnd ne '')
    {
        my $realDuration     = &LOC::Date::getRentDuration($dateBegin, $dateEnd, $agencyId);
        my $duration         = $realDuration + $adjustDays;
        my $tabDurationRange = &LOC::Tariff::Rent::getDurationRangeInfos($countryId, $duration);

        $tabViewData{'duration'}                = $duration;
        $tabViewData{'realDuration'}            = $realDuration;
        $tabViewData{'calendarDuration'}        = &LOC::Date::getDeltaDays($dateBegin, $dateEnd);
        $tabViewData{'rentTariffDurRangeLabel'} = $tabDurationRange->{'label'};
    }

    if ($requiredBeginDate ne '' && ($requiredEndDate ne '' || $requiredRealDuration > 0))
    {
        # Vérification de la date de début par rapport aux plages disponibles
        if ($beginDateWindow->[0] && $requiredBeginDate lt $beginDateWindow->[0])
        {
            $requiredBeginDate = $beginDateWindow->[0];
        }
        elsif ($beginDateWindow->[1] && $requiredBeginDate gt $beginDateWindow->[1])
        {
            $requiredBeginDate = $beginDateWindow->[1];
        }

        # calcul de la date de fin ou durée
        if ($requiredRealDuration > 0)
        {
            $requiredEndDate = &LOC::Date::getRentEndDate($requiredBeginDate, $requiredRealDuration, $agencyId);
        }
        else
        {
            if ($requiredEndDate lt $requiredBeginDate)
            {
                $requiredEndDate = $requiredBeginDate;
            }
        }

        # Vérification de la date de fin par rapport aux plages disponibles
        if ($endDateWindow->[0] && $requiredEndDate lt $endDateWindow->[0])
        {
            $requiredEndDate = $endDateWindow->[0];
        }
        elsif ($endDateWindow->[1] && $requiredEndDate gt $endDateWindow->[1])
        {
            $requiredEndDate = $endDateWindow->[1];
        }
        # recalcul de la durée
        $requiredRealDuration = &LOC::Date::getRentDuration($requiredBeginDate, $requiredEndDate, $agencyId);

        my $requiredDuration         = $requiredRealDuration + $adjustDays;
        my $tabRequiredDurationRange = &LOC::Tariff::Rent::getDurationRangeInfos($countryId, $requiredDuration);

        $tabViewData{'requiredBeginDate'}            = $requiredBeginDate;
        $tabViewData{'requiredEndDate'}              = $requiredEndDate;
        $tabViewData{'requiredDuration'}             = $requiredDuration;
        $tabViewData{'requiredRealDuration'}         = $requiredRealDuration;
        $tabViewData{'requiredCalendarDuration'}     = &LOC::Date::getDeltaDays($requiredBeginDate, $requiredEndDate);
        $tabViewData{'requiredRentTariffDurRangeLabel'} = $tabRequiredDurationRange->{'label'};

        my $valueDate = &LOC::Request::getString('valueDate');
        # Calcul des périodes possibles pour les jours +/-
        $tabViewData{'authorizedPeriodAddDays'} = &LOC::Day::getAuthorizedPeriod($agencyId, LOC::Day::TYPE_ADDITIONAL, 'contract', [$requiredBeginDate, $requiredEndDate], $valueDate);
        $tabViewData{'authorizedPeriodDedDays'} = &LOC::Day::getAuthorizedPeriod($agencyId, LOC::Day::TYPE_DEDUCTED, 'contract', [$requiredBeginDate, $requiredEndDate], $valueDate);
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentContract/rentalPeriodInfos.cgi';
}


# Function: saveAction
# Sauvegarde des informations du contrat
#
sub saveAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $tabSession = &LOC::Session::getInfos();

    # Variables pour la vue
    our %tabViewData = (
        'locale.id' => $tabUserInfos->{'locale.id'},
        'timeZone'  => $tabSession->{'timeZone'},
        'tabData' => {}
    );

    # Récupération des paramètres
    my $tabContractData = &LOC::Request::getHash('tabData');
    my $countryId       = $tabContractData->{'country.id'};
    my $contractId      = $tabContractData->{'id'};

    my $currentUserId   = $tabUserInfos->{'id'};

    my $action = $tabContractData->{'@action'};
    my $subAction = '';
    my $tabErrors = {};

    # Enregistrement du contrat
    my $tabSiteMessages = {};
    if ($action eq 'insert')
    {
        $contractId = &LOC::Contract::Rent::insert($countryId, $tabContractData->{'tabCustomer'}->{'id'},
                                                   $tabContractData, $currentUserId, $tabErrors);
    }
    elsif ($action eq 'update')
    {
        $subAction = $tabContractData->{'subAction'};
        &LOC::Contract::Rent::update($countryId, $contractId, $tabContractData, $currentUserId, $tabErrors);
    }



    # Construction de la trame de retour
    my $nbErrors = @{$tabErrors->{'fatal'}};
    if ($nbErrors > 0)
    {
        $tabViewData{'tabData'} = {
            '@action' => $action,
            '@state'  => 'ko',
            '@perlErrors' => $tabErrors
        };

        my @tabJSErrors = (); # 0 pour être sûr d'avoir un numéro d'erreur dans le tableau !!
        my @tabJSDetails = ();

        # Erreur sur le contrat
        if (&LOC::Util::in_array(LOC::Contract::Rent::ERROR_FATAL, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, -1); # Erreur fatale
        }
        if (&LOC::Util::in_array(0x0100, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 1); # Contrat déjà modifié
        }
        if (&LOC::Util::in_array(LOC::Contract::Rent::ERROR_WRITELOCKED, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 2); # Bloqué en écriture
        }
        if (&LOC::Util::in_array(LOC::Contract::Rent::ERROR_RIGHTS, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 3); # Certaines informations ne sont pas modifiables
        }
        if (&LOC::Util::in_array(0x0101, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 10); # Impossible de passer la ligne de devis en contrat
        }
        if (&LOC::Util::in_array(0x0010, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 15); # Négociateur non renseigné
        }
        if (&LOC::Util::in_array(0x0105, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 20); # Client bloqué au contrat
        }
        if (&LOC::Util::in_array(0x0106, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 21); # Impossible de prolonger le contrat
        }
        if (&LOC::Util::in_array(0x0108, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 23); # Impossible de prolonger le contrat sur un client PAL ou pro forma active
        }
        if (&LOC::Util::in_array(0x0112, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 22); # Impossible de prolonger le contrat en arrêt de plus de x jours
        }
        if (&LOC::Util::in_array(0x0104, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 101); # Client non renseigné
        }
        if (&LOC::Util::in_array(0x0127, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 31); # Le mail du contact commande n'est pas valide
        }
        if (&LOC::Util::in_array(0x0107, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 5); # Le contrat n'est pas modifiable
        }
        if (&LOC::Util::in_array(0x0110, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 120); # La zone n'est pas modifiable
        }
        if (&LOC::Util::in_array(0x0103, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 104); # La zone n'est pas valide
        }
        if (&LOC::Util::in_array(0x0113, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 24); # L'horaire de livraison n'est pas renseigné
        }
        if (&LOC::Util::in_array(0x0114, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 25); # Le modèle de la machine souhaitée ne correspond pas au modèle sélectionné
        }
        if (&LOC::Util::in_array(0x0115, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 105); # Le modèle de machines n'est pas renseigné
        }
        if (&LOC::Util::in_array(0x0111, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 400); # Le modèle de machines n'est pas disponible
        }
        # Dates et durée
        if (&LOC::Util::in_array(0x0109, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 108); # Les dates de début et de fin ne sont pas renseignées
        }
        if (&LOC::Util::in_array(0x0116, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 107); # La durée est incorrecte
        }
        if (&LOC::Util::in_array(0x0117, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 130); # La date de fin est inférieure à la date de début
        }
        if (&LOC::Util::in_array(0x0118, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 131); # La date de fin inférieure à la date de dernière facturation
        }
        if (&LOC::Util::in_array(0x0119, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 132); # La date de début n\'est pas modifiable
        }
        if (&LOC::Util::in_array(0x0120, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 133); # La date de fin n\'est pas modifiable
        }
        if (&LOC::Util::in_array(0x0121, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 134); # La date de début ne doit pas être inférieure à 1 mois
        }
        if (&LOC::Util::in_array(0x0123, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 136); # Le nombre jours ouvrés a changé
        }
        if (&LOC::Util::in_array(0x0124, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 137); # Le type d'assurance a changé
        }
        if (&LOC::Util::in_array(0x0125, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 138); # Le taux d'assurance a changé
        }
        if (&LOC::Util::in_array(0x0126, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 139); # Mode de calcul de participation au recyclage non valide
        }
        if (&LOC::Util::in_array(0x0129, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 140); # Mode de calcul de gestion de recours non valide
        }

        if (&LOC::Util::in_array(0x0200, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 150); # Impossible d'arrêter le contrat
        }
        if (&LOC::Util::in_array(0x0201, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 151); # Impossible de remettre en cours le contrat
        }
        if (&LOC::Util::in_array(0x0202, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 152); # Impossible de mettre en facturation le contrat
        }
        if (&LOC::Util::in_array(0x0203, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 153); # Impossible de remettre en arrêt le contrat
        }
        if (&LOC::Util::in_array(0x0204, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 154); # Impossible d'annuler le contrat
        }

        # Erreur sur le chantier
        if (&LOC::Util::in_array(LOC::Contract::Rent::ERROR_MODULE_SITE, $tabErrors->{'fatal'}))
        {
            my $tabSiteErrors = $tabErrors->{'modules'}->{'site'};
            if (&LOC::Util::in_array(LOC::Site::ERROR_LABELREQUIRED, $tabSiteErrors->{'fatal'}))
            {
                push(@tabJSErrors, 102); # Le libellé du chantier n'est pas renseigné
            }
            if (&LOC::Util::in_array(LOC::Site::ERROR_CITYNOTVALID, $tabSiteErrors->{'fatal'}))
            {
                push(@tabJSErrors, 103); # La ville du chantier n'est pas renseignée
            }
            if (&LOC::Util::in_array(LOC::Site::ERROR_CONTACTMAILNOTVALID, $tabSiteErrors->{'fatal'}))
            {
                push(@tabJSErrors, 121); # Le mail du contact n'est pas valide
            }
        }

        # Erreurs sur les j+/j-
        if (&LOC::Util::in_array(LOC::Contract::Rent::ERROR_MODULE_DAYS, $tabErrors->{'fatal'}))
        {
            my $tabDaysErrors = $tabErrors->{'modules'}->{'days'};
            if (&LOC::Util::in_array(LOC::Day::ERROR_TYPEREQUIRED, $tabDaysErrors->{'fatal'}))
            {
                push(@tabJSErrors, 801); # Le type d'un jour n'est pas renseigné
            }
            if (&LOC::Util::in_array(LOC::Day::ERROR_DATEREQUIRED, $tabDaysErrors->{'fatal'}))
            {
                push(@tabJSErrors, 802); # La date n'est pas renseignée
            }
            if (&LOC::Util::in_array(LOC::Day::ERROR_REASONREQUIRED, $tabDaysErrors->{'fatal'}))
            {
                push(@tabJSErrors, 803); # Le motif n'est pas renseigné
            }
            if (&LOC::Util::in_array(LOC::Day::ERROR_EXISTINGDAY, $tabDaysErrors->{'fatal'}))
            {
                push(@tabJSErrors, 804); # Le jour existe déjà
            }
            if (&LOC::Util::in_array(LOC::Day::ERROR_OUTOFPERIOD, $tabDaysErrors->{'fatal'}))
            {
                push(@tabJSErrors, 805); # Le jour n'est pas dans la période autorisée
            }
            if (&LOC::Util::in_array(LOC::Day::ERROR_ALREADYINVOICEABLE, $tabDaysErrors->{'fatal'}))
            {
                push(@tabJSErrors, 806); # Le jour est déjà facturable
            }
            if (&LOC::Util::in_array(LOC::Day::ERROR_ALREADYNOTINVOICEABLE, $tabDaysErrors->{'fatal'}))
            {
                push(@tabJSErrors, 807); # Le jour est déjà non facturable
            }
            if (&LOC::Util::in_array(LOC::Day::ERROR_INVALIDAPPLIEDTOCONTRACT, $tabDaysErrors->{'fatal'}))
            {
                push(@tabJSErrors, 808); # Le contrat lié n'est pas valide
            }
            if ($tabDaysErrors->{'details'})
            {
                push(@tabJSDetails, {
                    'module'   => 'days',
                    'tabInfos' => $tabDaysErrors->{'details'}
                });
            }
        }

        # Erreurs tarification location
        if (&LOC::Util::in_array(LOC::Contract::Rent::ERROR_MODULE_TARIFFRENT, $tabErrors->{'fatal'}))
        {
            my $tabRentTariffErrors = $tabErrors->{'modules'}->{'rentTariff'};
            if (&LOC::Util::in_array(0x0101, $tabRentTariffErrors->{'fatal'}))
            {
                push(@tabJSErrors, 202); # La justification de la RE n'est pas renseignée
            }
        }
        # Erreurs tarification transport
        if (&LOC::Util::in_array(LOC::Contract::Rent::ERROR_MODULE_TARIFFTRSP, $tabErrors->{'fatal'}))
        {
            my $tabTrspTariffErrors = $tabErrors->{'modules'}->{'transportTariff'};
            if (&LOC::Util::in_array(0x0100, $tabTrspTariffErrors->{'fatal'}))
            {
                push(@tabJSErrors, 301); # Pas d'estimation de transport
            }
            if (&LOC::Util::in_array(0x0101, $tabTrspTariffErrors->{'fatal'}))
            {
                push(@tabJSErrors, 303); # Le client a changé
            }
            if (&LOC::Util::in_array(0x0102, $tabTrspTariffErrors->{'fatal'}))
            {
                push(@tabJSErrors, 302); # La zone de transport a changé
            }
            if (&LOC::Util::in_array(0x0103, $tabTrspTariffErrors->{'fatal'}))
            {
                push(@tabJSErrors, 304); # Le montant du transport aller est inférieur à l'estimation
            }
            if (&LOC::Util::in_array(0x0104, $tabTrspTariffErrors->{'fatal'}))
            {
                push(@tabJSErrors, 305); # Le montant du transport retour est inférieur à l'estimation
            }
            if (&LOC::Util::in_array(0x0105, $tabTrspTariffErrors->{'fatal'}))
            {
                push(@tabJSErrors, 306); # Le contrat lié au transport aller est invalide
            }
            if (&LOC::Util::in_array(0x0106, $tabTrspTariffErrors->{'fatal'}))
            {
                push(@tabJSErrors, 307); # Le contrat lié au transport retour est invalide
            }
            if (&LOC::Util::in_array(0x0107, $tabTrspTariffErrors->{'fatal'}))
            {
                push(@tabJSErrors, 308); # Le forfait machine supplémentaire aller est inférieur à la valeur minimale
            }
            if (&LOC::Util::in_array(0x0108, $tabTrspTariffErrors->{'fatal'}))
            {
                push(@tabJSErrors, 309); # Le forfait machine supplémentaire retour est inférieur à la valeur minimale
            }
        }

        # Erreurs proforma
        if (&LOC::Util::in_array(LOC::Contract::Rent::ERROR_MODULE_PROFORMA, $tabErrors->{'fatal'}))
        {
            my $tabProformaErrors = $tabErrors->{'modules'}->{'proforma'};
            if (&LOC::Util::in_array(0x0004, $tabProformaErrors->{'fatal'}))
            {
                push(@tabJSErrors, 504); # Type de montant mensuel
            }
            if (&LOC::Util::in_array(0x0005, $tabProformaErrors->{'fatal'}))
            {
                push(@tabJSErrors, 505); # Action pro forma non autorisée
            }
            if (&LOC::Util::in_array(0x0008, $tabProformaErrors->{'fatal'}))
            {
                push(@tabJSErrors, 508); # La date de fin saisie est antérieure à la date de fin du contrat
            }
            if (&LOC::Util::in_array(0x0009, $tabProformaErrors->{'fatal'}))
            {
                push(@tabJSErrors, 509); # La date de fin saisie est antérieure à la date de fin de la dernière prolongation
            }
            if (&LOC::Util::in_array(0x0012, $tabProformaErrors->{'fatal'}))
            {
                push(@tabJSErrors, 512); # Impossible de modifier la proforma
            }
            if (&LOC::Util::in_array(0x0013, $tabProformaErrors->{'fatal'}))
            {
                push(@tabJSErrors, 513); # Impossible d'abandonner la proforma
            }
            if (&LOC::Util::in_array(0x0015, $tabProformaErrors->{'fatal'}))
            {
                push(@tabJSErrors, 515); # Impossible de réactiver la proforma
            }
            if (&LOC::Util::in_array(0x0017, $tabProformaErrors->{'fatal'}))
            {
                push(@tabJSErrors, 517); # Le montant de la pro forma est nul
            }
            if (&LOC::Util::in_array(0x0018, $tabProformaErrors->{'fatal'}))
            {
                push(@tabJSErrors, 518); # Le mode de règlement n'est pas renseigné
            }
            if (&LOC::Util::in_array(0x0019, $tabProformaErrors->{'fatal'}))
            {
                push(@tabJSErrors, 519); # estimation carburant incorrecte
            }
        }
        # Erreur documents
        if (&LOC::Util::in_array(LOC::Contract::Rent::ERROR_MODULE_DOCUMENTS, $tabErrors->{'fatal'}))
        {
            my $tabDocumentsErrors = $tabErrors->{'modules'}->{'documents'};
            if (&LOC::Util::in_array(0x0010, $tabDocumentsErrors->{'fatal'}))
            {
                push(@tabJSErrors, 550); # Impossible de supprimer le document
            }
            if (&LOC::Util::in_array(0x0020, $tabDocumentsErrors->{'fatal'}))
            {
                push(@tabJSErrors, 551); # Impossible d'ajouter le document
            }
            if (&LOC::Util::in_array(0x0030, $tabDocumentsErrors->{'fatal'}))
            {
                push(@tabJSErrors, 552); # Impossible d'ajouter le document
            }
        }

        # Erreur transport
        if (&LOC::Util::in_array(LOC::Contract::Rent::ERROR_MODULE_TRANSPORT, $tabErrors->{'fatal'}))
        {
            my $tabTransportErrors = $tabErrors->{'modules'}->{'transport'};

            # S'il y a une erreur sur la livraison
            if (&LOC::Util::in_array(LOC::Contract::Rent::ERROR_MODULE_TRANSPORT_DELIVERY, $tabTransportErrors->{'fatal'}))
            {
                my $tabDeliveryErrors = $tabTransportErrors->{'modules'}->{'delivery'};
                if (&LOC::Util::in_array(0x0100, $tabDeliveryErrors->{'fatal'}))
                {
                    push(@tabJSErrors, 600); # Erreur sur le contrat lié du transfert inter-chantiers ou de la reprise sur place
                }
                if (&LOC::Util::in_array(0x0102, $tabDeliveryErrors->{'fatal'}))
                {
                    push(@tabJSErrors, 602); # Incohérence date/horaire (récupération)
                }
                if (&LOC::Util::in_array(0x0106, $tabDeliveryErrors->{'fatal'}))
                {
                    push(@tabJSErrors, 603); # Réalisation impossible
                }
                if (&LOC::Util::in_array(0x0107, $tabDeliveryErrors->{'fatal'}))
                {
                    push(@tabJSErrors, 604); # Annulation impossible
                }

            }

            # S'il y a une erreur sur la récupération
            if (&LOC::Util::in_array(LOC::Contract::Rent::ERROR_MODULE_TRANSPORT_RECOVERY, $tabTransportErrors->{'fatal'}))
            {
                my $tabRecoveryErrors = $tabTransportErrors->{'modules'}->{'recovery'};
                if (&LOC::Util::in_array(0x0100, $tabRecoveryErrors->{'fatal'}))
                {
                    push(@tabJSErrors, 650); # Erreur sur le contrat lié du transfert inter-chantiers ou de la reprise sur place
                }
                if (&LOC::Util::in_array(0x0102, $tabRecoveryErrors->{'fatal'}))
                {
                    push(@tabJSErrors, 652); # Incohérence date/horaire (récupération)
                }
                if (&LOC::Util::in_array(0x0103, $tabRecoveryErrors->{'fatal'}))
                {
                    push(@tabJSErrors, 653); # Pro forma de prolongation non finalisée (récupération)
                }
                if (&LOC::Util::in_array(0x0106, $tabRecoveryErrors->{'fatal'}))
                {
                    push(@tabJSErrors, 656); # Réalisation impossible
                }
                if (&LOC::Util::in_array(0x0107, $tabRecoveryErrors->{'fatal'}))
                {
                    push(@tabJSErrors, 657); # Annulation impossible
                }
                if (&LOC::Util::in_array(0x0108, $tabRecoveryErrors->{'fatal'}))
                {
                    push(@tabJSErrors, 658); # Transfert de la machine vers une autre agence est impossible car la machine est déjà mise en visibilité
                }
                if (&LOC::Util::in_array(0x0109, $tabRecoveryErrors->{'fatal'}))
                {
                    push(@tabJSErrors, 659); # Impossible de modifier "La machine est transférée à" car toutes les machines du modèle sont réservées
                }
            }

        }

        # Si pas d'erreur alors erreur inconnue !!
        if (@tabJSErrors == 0)
        {
            push(@tabJSErrors, 0); # Erreur inconnue !
        }

        $tabViewData{'tabData'}->{'@errors'} = \@tabJSErrors;
        $tabViewData{'tabData'}->{'@details'} = \@tabJSDetails;
    }
    else
    {
        # TODO: Renvoyer la trame du contrat
        $tabViewData{'tabData'} = {
            '@action'     => $action,
            '@subAction'  => $subAction,
            '@state'      => 'ok',
            '@perlErrors' => $tabErrors
        };

        # Vérification des warnings
        my @tabJSWarnings = ();

        # S'il y a une erreur sur les documents
        if (&LOC::Util::in_array(LOC::Contract::Rent::ERROR_MODULE_DOCUMENTS, $tabErrors->{'warning'}))
        {
            my $tabIndexesByType = {
                'remove' => [],
                'add'    => [],
                'edit'   => []
            };

            my $index = 0;
            foreach my $tabAction (@{$tabContractData->{'tabDocumentsActions'}})
            {
                push(@{$tabIndexesByType->{$tabAction->{'type'}}}, $index);
                $index++;
            }

            foreach my $type (keys %$tabIndexesByType)
            {
                if (@{$tabIndexesByType->{$type}} > 0)
                {
                    push(@tabJSWarnings, {
                        'module'  => 'documents',
                        'code'    => $type,
                        'indexes' => $tabIndexesByType->{$type}
                    });
                }
            }
        }

        # S'il y a une erreur sur le chantier
        if (&LOC::Util::in_array(LOC::Contract::Rent::ERROR_MODULE_SITE, $tabErrors->{'warning'}))
        {
            my $tabSiteErrors = $tabErrors->{'modules'}->{'site'};
            # - Informations contact incomplètes
            if (&LOC::Util::in_array(LOC::Site::ERROR_CONTACTREQUIRED, $tabSiteErrors->{'warning'}))
            {
                push(@tabJSWarnings, {
                    'module' => 'site',
                    'code'   => 'contact'
                });
            }
        }

        # S'il y a une erreur générale
        # - Informations contact commande incomplètes
        if (&LOC::Util::in_array(0x0128, $tabErrors->{'warning'}))
        {
                push(@tabJSWarnings, {
                    'module' => 'general',
                    'code'   => 'orderContact'
                });
        }

        $tabViewData{'tabData'}->{'@warnings'} = \@tabJSWarnings;


        if ($tabContractData->{'subAction'} eq 'reopenRepairSheetsAndSwitch')
        {
            $tabViewData{'tabData'}->{'@url'} = &LOC::Request::createRequestUri('repair:sheet:edit',
                                                                                {'sheetId' => $tabContractData->{'tabRepairSheetsToReopen'}->[0]});
        }
        else
        {
            my $tabIndex = &LOC::Request::getInteger('tabIndex', undef);
            my $tabParams = {'contractId' => $contractId};
            if (defined $tabIndex)
            {
                $tabParams->{'tabIndex'} = $tabIndex;
            }
            $tabViewData{'tabData'}->{'@url'} = &LOC::Request::createRequestUri('rent:rentContract:view', $tabParams);
        }
    }


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentContract/save.cgi';
}


# Function: getUrlAction
# Récupère l'URL du contrat
#
sub getUrlAction
{
    # Variables pour la vue
    our %tabViewData = ();

    # Récupération des paramètres
    my $contractId = &LOC::Request::getInteger('contractId', undef);
    my $tabIndex   = &LOC::Request::getInteger('tabIndex', undef);

    my $tabParams = {'contractId' => $contractId};
    if (defined $tabIndex)
    {
        $tabParams->{'tabIndex'} = $tabIndex;
    }
    $tabViewData{'url'} = &LOC::Request::createRequestUri('rent:rentContract:view', $tabParams);


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentContract/getUrl.cgi';
}



# Function: jsonListForSitesTransferAction
# Récupérer la liste des contrats (avec recherche possible)
#
sub jsonListForSitesTransferAction
{
    # Variables pour la vue
    my $tabParameters = $_[0];
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id' => $tabUserInfos->{'locale.id'}
    );

    my $countryId         = &LOC::Request::getString('countryId');
    my $agencyId          = &LOC::Request::getString('agencyId');
    my $modelId           = &LOC::Request::getInteger('modelId');
    my $visibleOnAgencyId = &LOC::Request::getString('visibleOnAgencyId', undef);
    my $side              = &LOC::Request::getString('side');
    my $isSelf            = &LOC::Request::getInteger('isSelf', 0);
    my $idToInclude       = &LOC::Request::getInteger('idToInclude', undef);
    my $idToExclude       = &LOC::Request::getInteger('idToExclude', 0);
    my $isAnticipatable   = &LOC::Request::getBoolean('isAnticipatable', 0);
    my $referenceDate     = &LOC::Request::getString('referenceDate');
    my $referenceHour     = &LOC::Request::getString('referenceHour');

    # Informations concernant les dates et horaires de référence à prendre en compte dans la recherche des contrats disponibles
    my $tabInfosRef = {
        'isAnticipatable' => $isAnticipatable,
        'referenceDate'   => $referenceDate,
        'referenceHour'   => $referenceHour
    };

    my $tabContracts = &LOC::Transport::getContractsForSitesTransfer($countryId, $agencyId, $modelId, $side, $tabInfosRef,
                                                                     LOC::Util::GETLIST_ASSOC, {},
                                                                     LOC::Transport::GETSTCTTINFOS_BASE,
                                                                     {'isSelf'      => $isSelf,
                                                                      'idToInclude' => $idToInclude,
                                                                      'idToExclude' => $idToExclude,
                                                                      'visibleOnAgencyId' => $visibleOnAgencyId});

    $tabViewData{'tabContracts'} = [];
    foreach my $tabContractInfos (values %$tabContracts)
    {
        # informations complémentaires
        my @tabInfosCpl;
        if ($tabContractInfos->{'isSelf'})
        {
            push(@tabInfosCpl, 'self');
        }
        if ($tabContractInfos->{'isPlanned'})
        {
            push(@tabInfosCpl, 'planned');
        }
        if ($tabContractInfos->{'isAnticipatable'})
        {
            push(@tabInfosCpl, 'anticipatable');
        }
        # récap de toutes les informations nécessaires
        push(@{$tabViewData{'tabContracts'}}, {
            'id'              => $tabContractInfos->{'id'},
            'agency'          => {
                'id'    => $tabContractInfos->{'agency.id'},
                'label' => $tabContractInfos->{'agency.label'}
            },
            'code'            => $tabContractInfos->{'code'},
            'siteInfos'       => {
                'label'      => $tabContractInfos->{'site.label'},
                'postalCode' => $tabContractInfos->{'site.postalCode'},
                'city'       => $tabContractInfos->{'site.city'}
            },
            'url'             => &LOC::Request::createRequestUri('rent:rentContract:view',
                                                                 {'contractId' => $tabContractInfos->{'id'},
                                                                  'tabIndex'   => LOC::Transport::DEFAULT_CONTRACT_TABINDEX}),
            'limitDate'       => $tabContractInfos->{'limitDate'},
            'limitHour'       => substr($tabContractInfos->{'limitHour'}, 0, -3),
            'isAnticipatable' => $tabContractInfos->{'isAnticipatable'},
            'amount'          => $tabContractInfos->{'amount'},

            'isSelf'          => $tabContractInfos->{'isSelf'},
            'site'            => $tabContractInfos->{'site.postalCode'} . ' ' . $tabContractInfos->{'site.city'},
            'parkNumber'      => $tabContractInfos->{'machine.parkNumber'},
            'tabErrors'       => $tabContractInfos->{'tabErrors'},
            'isFatalError'    => $tabContractInfos->{'isFatalError'},
            'tabInfosCpl'     => \@tabInfosCpl
        });
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentContract/jsonListForSitesTransfer.cgi';
}

# Function: pdfAction
# Affiche le PDF d'un contrat
#
# Parameters:
# int  contractId   - Identifiant du contrat
sub pdfAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Récupération des paramètres
    my $countryId      = $tabUserInfos->{'nomadCountry.id'};
    my $currencySymbol = $tabUserInfos->{'currency.symbol'};

    my $tabIds     = &LOC::Request::getArray('contractsIds', []);


    # Récupération des informations nécessaires à l'impression
    my $tabContractsList = &LOC::Contract::Rent::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                          {'id' => $tabIds},
                                                          LOC::Contract::Rent::GETINFOS_EXT_MACHINE |
                                                          LOC::Contract::Rent::GETINFOS_EXT_SITE |
                                                          LOC::Contract::Rent::GETINFOS_EXT_TARIFFRENT |
                                                          LOC::Contract::Rent::GETINFOS_EXT_TARIFFTRSP |
                                                          LOC::Contract::Rent::GETINFOS_INSURANCE |
                                                          LOC::Contract::Rent::GETINFOS_EXT_INSURANCETYPE |
                                                          LOC::Contract::Rent::GETINFOS_APPEAL |
                                                          LOC::Contract::Rent::GETINFOS_EXT_APPEALTYPE |
                                                          LOC::Contract::Rent::GETINFOS_CREATOR |
                                                          LOC::Contract::Rent::GETINFOS_CLEANING |
                                                          LOC::Contract::Rent::GETINFOS_RESIDUES);

    my $tabContracts = [];

    foreach my $tabContractInfos (values(%$tabContractsList))
    {
        my $tabAgencyInfos = &LOC::Agency::getInfos($tabContractInfos->{'agency.id'});
        my $tabCustomerInfos = &LOC::Customer::getInfos($countryId, $tabContractInfos->{'customer.id'});
        my $isTrspTarification = &LOC::Characteristic::getAgencyValueByCode('TRSPTARIF', $tabContractInfos->{'agency.id'}) * 1;


        # Calcul des montants transport
        my $deliverySpecialReductionPercent = 0;
        my $deliverySpecialReductionBillLabel = '';
        my $recoverySpecialReductionPercent = 0;
        my $recoverySpecialReductionBillLabel = '';
        my $deliveryBaseAmount = $tabContractInfos->{'transportTariff.tabInfos'}->{'deliveryAmount'};
        my $recoveryBaseAmount = $tabContractInfos->{'transportTariff.tabInfos'}->{'recoveryAmount'};

        my $baseAmount = $tabContractInfos->{'transportTariff.tabInfos'}->{'baseAmount'};
        if ($isTrspTarification && $tabContractInfos->{'transportTariff.tabInfos'}->{'id'} != 0 && $baseAmount > 0)
        {
            $deliveryBaseAmount = &LOC::Util::round($baseAmount - (($baseAmount * $tabContractInfos->{'transportTariff.tabInfos'}->{'standardReductionPercent'}) / 100));
            $deliverySpecialReductionPercent = 0;
            $deliverySpecialReductionBillLabel = '';
            if ($deliveryBaseAmount > $tabContractInfos->{'transportTariff.tabInfos'}->{'deliveryAmount'})
            {
                $deliverySpecialReductionPercent = &LOC::Util::round(100 - ($tabContractInfos->{'transportTariff.tabInfos'}->{'deliveryAmount'} * 100 / $deliveryBaseAmount), 0);
                if ($deliverySpecialReductionPercent == $tabContractInfos->{'transportTariff.tabInfos'}->{'specialReductionPercent'} &&
                    $tabContractInfos->{'transportTariff.tabInfos'}->{'deliveryAmount'} == $tabContractInfos->{'transportTariff.tabInfos'}->{'amount'})
                {
                    $deliverySpecialReductionBillLabel = $tabContractInfos->{'transportTariff.tabInfos'}->{'specialReductionBillLabel'};
                }
            }
            else
            {
                $deliveryBaseAmount = $tabContractInfos->{'transportTariff.tabInfos'}->{'deliveryAmount'};
            }

            $recoveryBaseAmount = &LOC::Util::round($baseAmount - (($baseAmount * $tabContractInfos->{'transportTariff.tabInfos'}->{'standardReductionPercent'}) / 100));
            $recoverySpecialReductionPercent = 0;
            $recoverySpecialReductionBillLabel = '';
            if ($recoveryBaseAmount > $tabContractInfos->{'transportTariff.tabInfos'}->{'recoveryAmount'})
            {
                $recoverySpecialReductionPercent = &LOC::Util::round(100 - ($tabContractInfos->{'transportTariff.tabInfos'}->{'recoveryAmount'} * 100 / $recoveryBaseAmount), 0);
                if ($recoverySpecialReductionPercent == $tabContractInfos->{'transportTariff.tabInfos'}->{'specialReductionPercent'} &&
                    $tabContractInfos->{'transportTariff.tabInfos'}->{'recoveryAmount'} == $tabContractInfos->{'transportTariff.tabInfos'}->{'amount'})
                {
                    $recoverySpecialReductionBillLabel = $tabContractInfos->{'transportTariff.tabInfos'}->{'specialReductionBillLabel'};
                }
            }
            else
            {
                $recoveryBaseAmount = $tabContractInfos->{'transportTariff.tabInfos'}->{'recoveryAmount'};
            }
        }

        my $tabInfos = {
            'countryId'              => $countryId,
            'id'                     => $tabContractInfos->{'id'},
            'code'                   => $tabContractInfos->{'code'},
            'orderNo'                => $tabContractInfos->{'orderNo'},
            'orderContact.fullName'  => $tabContractInfos->{'orderContact.fullName'},
            'orderContact.telephone' => $tabContractInfos->{'orderContact.telephone'},
            'agency.tabInfos' => {
                'label'            => $tabAgencyInfos->{'label'},
                'manager.fullName' => $tabAgencyInfos->{'manager.firstname'} . ' ' . $tabAgencyInfos->{'manager.name'},
                'address'          => $tabAgencyInfos->{'address'},
                'telephone'        => $tabAgencyInfos->{'telephone'},
                'fax'              => $tabAgencyInfos->{'fax'},
                'postalCode'       => $tabAgencyInfos->{'postalCode'},
                'city'             => $tabAgencyInfos->{'city'}
            },
            'machine.tabInfos' => {
                'parkNumber'   => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
                'serialNumber' => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
                'manufacturer.label' => $tabContractInfos->{'machine.tabInfos'}->{'manufacturer.label'},
                'machinesFamily.shortLabel' => $tabContractInfos->{'machine.tabInfos'}->{'machinesFamily.shortLabel'},
                'machinesFamily.elevation'  => $tabContractInfos->{'machine.tabInfos'}->{'machinesFamily.elevation'},
                'machinesFamily.energy'  => $tabContractInfos->{'machine.tabInfos'}->{'machinesFamily.energy'},
                'model.label'  => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
                'model.weight' => $tabContractInfos->{'machine.tabInfos'}->{'model.weight'},
                'family.id'    => $tabContractInfos->{'machine.tabInfos'}->{'family.id'}
            },
            'customer.tabInfos' => {
                'label'      => $tabCustomerInfos->{'name'},
                'address'    => $tabCustomerInfos->{'address1'},
                'address2'   => $tabCustomerInfos->{'address2'},
                'postalCode' => $tabCustomerInfos->{'locality.postalCode'},
                'city'       => $tabCustomerInfos->{'locality.name'},
                'telephone'  => $tabCustomerInfos->{'telephone'},
                'fax'        => $tabCustomerInfos->{'fax'},
                'siren'      => $tabCustomerInfos->{'statCode'},
                'paymentMode.label' => $tabCustomerInfos->{'paymentMode.label'},
                'potential'  => $tabCustomerInfos->{'potential'}
            },
            'site.tabInfos' => {
                'label'       => $tabContractInfos->{'site.tabInfos'}->{'label'},
                'address'     => $tabContractInfos->{'site.tabInfos'}->{'address'},
                'postalCode'  => $tabContractInfos->{'site.tabInfos'}->{'locality.postalCode'},
                'city'        => $tabContractInfos->{'site.tabInfos'}->{'locality.name'},
                'contact.fullName' => $tabContractInfos->{'site.tabInfos'}->{'contact.fullName'},
                'telephone'        => $tabContractInfos->{'site.tabInfos'}->{'contact.telephone'}
            },
            'rentTariff.tabInfos' => {
                'type'                     => $tabContractInfos->{'rentTariff.tabInfos'}->{'amountType'},
                'baseAmount'               => $tabContractInfos->{'rentTariff.tabInfos'}->{'baseAmount'},
                'standardReductionPercent' => $tabContractInfos->{'rentTariff.tabInfos'}->{'standardReductionPercent'},
                'specialReductionPercent'  => $tabContractInfos->{'rentTariff.tabInfos'}->{'specialReductionPercent'},
                'specialReductionBillLabel' => $tabContractInfos->{'rentTariff.tabInfos'}->{'specialReductionBillLabel'},
                'unitPrice'                => $tabContractInfos->{'unitPrice'},
            },
            'transportTariff.tabInfos' => {
                'deliveryBaseAmount'                => $deliveryBaseAmount,
                'deliveryAmount'                    => $tabContractInfos->{'transportTariff.tabInfos'}->{'deliveryAmount'},
                'deliverySpecialReductionPercent'   => $deliverySpecialReductionPercent,
                'deliverySpecialReductionBillLabel' => $deliverySpecialReductionBillLabel,
                'recoveryBaseAmount'                => $recoveryBaseAmount,
                'recoveryAmount'                    => $tabContractInfos->{'transportTariff.tabInfos'}->{'recoveryAmount'},
                'recoverySpecialReductionPercent'   => $recoverySpecialReductionPercent,
                'recoverySpecialReductionBillLabel' => $recoverySpecialReductionBillLabel
            },
            'insurance.tabInfos' => {
                'rate'       => $tabContractInfos->{'insurance.rate'},
                'fullLabel'  => $tabContractInfos->{'insuranceType.tabInfos'}->{'fullLabel'},
                'isInvoiced' => $tabContractInfos->{'insurance.isInvoiced'}
            },
            'appeal.tabInfos' => undef,
            'creator.tabInfos' => {
                'name' => $tabContractInfos->{'creator.name'},
                'firstName' => $tabContractInfos->{'creator.firstName'}
            },

            'beginDate' => $tabContractInfos->{'beginDate'},
            'endDate'   => $tabContractInfos->{'endDate'},
            'additionalDays' => $tabContractInfos->{'additionalDaysUnbilled'},
            'deductedDays'   => $tabContractInfos->{'deductedDaysUnbilled'},
            'duration'       => $tabContractInfos->{'duration'},
            'comments'        => $tabContractInfos->{'comments'},
            'cleaning.typeId' => $tabContractInfos->{'cleaning.type.id'},
            'residues.mode'  => $tabContractInfos->{'residues.mode'},
            'residues.value' => $tabContractInfos->{'residues.value'}
        };

        if (defined $tabContractInfos->{'appealType.tabInfos'})
        {
            $tabInfos->{'appeal.tabInfos'} = {
                'isInvoiced' => ($tabContractInfos->{'appealType.tabInfos'}->{'mode'} ? 1 : 0),
                'label'      => $tabContractInfos->{'appealType.tabInfos'}->{'extraLabel'},
                'rate'       => $tabContractInfos->{'appeal.value'}
            },
        }

        # Récupération du digicode
        my $tabFilters = {
            'contractId' => $tabContractInfos->{'id'},
            'typeCode'   => LOC::StartCode::Contract::Rent::TYPE_CUSTOMER,
            'stateId'    => LOC::StartCode::STATE_ACTIVE,
            'rule'       => {
                'stateId'  => LOC::StartCode::STATE_RULE_ACTIVE
            }
        };
        my $tabCustomerStartCodes = &LOC::StartCode::Contract::Rent::getList(
            $countryId,
            LOC::Util::GETLIST_PAIRS,
            $tabFilters
        );
        my $nbStartCodes = scalar keys(%$tabCustomerStartCodes);

        if ($nbStartCodes == 1)
        {
            $tabInfos->{'startCode.code'} = (values(%$tabCustomerStartCodes))[0];
        }

        push(@$tabContracts, $tabInfos);
    }

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'       => $tabUserInfos->{'locale.id'},
        'tabContracts'    => $tabContracts,
        'countryId'       => $countryId,
        'currencySymbol'  => $currencySymbol
    );

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;

    $tabViewData{'pdfUrl'} = $directory . '/view/rentContract/pdf.php';

    require $directory . '/view/rentContract/pdf.cgi';
}


# Function: pdfListAction
# Affiche la liste des contrats au format PDF
#
# Parameters:
# string   countryId     - Pays
# string   agencyId      - Agence
# int      customerId    - Identifiant du client
# arrayref contractsId   - Tableau des identifiants des contrats à imprimer
# int      nbTotal       - Total des lignes de résultats (affichage)
# int      offset        - Début de la recherche (affichage)
# int      limit         - Nombre à afficher (affichage)
sub pdfListAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Récupération des paramètres
    my $countryId   = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $agencyId    = &LOC::Request::getString('agencyId', $tabUserInfos->{'nomadAgency.id'});
    my $customerId  = &LOC::Request::getInteger('customerId', 0);
    my $tabContractsIds = &LOC::Request::getArray('contractsIds', []);
    my $nbTotal     = &LOC::Request::getInteger('nbTotal', 0);
    my $offset      = &LOC::Request::getInteger('offset', 0);
    my $limit       = &LOC::Request::getInteger('limit', 0);

    my $tabCustomerInfos = &LOC::Customer::getInfos($countryId, $customerId);
    my $tabInsuranceTypesList = &LOC::Insurance::Type::getList($countryId, LOC::Util::GETLIST_ASSOC);

    my $tabContractsList = &LOC::Contract::Rent::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                              {'id' => $tabContractsIds},
                                                              LOC::Contract::Rent::GETINFOS_EXT_SITE |
                                                              LOC::Contract::Rent::GETINFOS_EXT_MODEL |
                                                              LOC::Contract::Rent::GETINFOS_MACHINE |
                                                              LOC::Contract::Rent::GETINFOS_INSURANCE |
                                                              LOC::Contract::Rent::GETINFOS_APPEAL |
                                                              LOC::Contract::Rent::GETINFOS_EXT_APPEALTYPE |
                                                              LOC::Contract::Rent::GETINFOS_TARIFFTRSP,
                                                              {'sorter' => 'id'});

    my @tabContracts;
    foreach my $tabContractInfos (values(%$tabContractsList))
    {
        my $tabInfos = {
            'code'               => $tabContractInfos->{'code'},
            'state.label'        => $tabContractInfos->{'state.label'},
            'machine.parkNumber' => $tabContractInfos->{'machine.parkNumber'},
            'model.label'        => $tabContractInfos->{'model.tabInfos'}->{'label'},
            'site' => {
                'label'          => $tabContractInfos->{'site.tabInfos'}->{'label'},
                'locality.label' => $tabContractInfos->{'site.tabInfos'}->{'locality.label'},
            },
            'beginDate' => $tabContractInfos->{'beginDate'},
            'endDate'   => $tabContractInfos->{'endDate'},
            'tariff'    => {
                'unitPrice'      => $tabContractInfos->{'unitPrice'} * 1,
                'amountType'     => $tabContractInfos->{'amountType'}
            },
            'duration'    => $tabContractInfos->{'duration'} * 1,
            'insurance'   => {
                'typeId' => $tabContractInfos->{'insuranceType.id'},
                'rate'   => $tabContractInfos->{'insurance.rate'},
                'flags'  => $tabContractInfos->{'insurance.flags'},
                'appeal' => {
                    'typeId' => $tabContractInfos->{'appealType.id'},
                    'value'  => $tabContractInfos->{'appeal.value'},
                    'mode'   => $tabContractInfos->{'appealType.tabInfos'}->{'mode'}
                },
            },
            'deliveryAmount' => $tabContractInfos->{'transportTariff.deliveryAmount'} * 1,
            'recoveryAmount' => $tabContractInfos->{'transportTariff.recoveryAmount'} * 1
        };
        push(@tabContracts, $tabInfos);
    }

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'        => $tabUserInfos->{'locale.id'},
        'user.agency.id'   => $agencyId,
        'currencySymbol'   => $tabUserInfos->{'currency.symbol'},
        'tabResultInfos'   => {
            'from'  => $offset + 1,
            'to'    => ($offset + $limit > $nbTotal ? $nbTotal : $offset + $limit),
            'total' => $nbTotal
        },
        'tabCustomerInfos'      => $tabCustomerInfos,
        'tabContractsList'      => \@tabContracts,
        'tabInsuranceTypesList' => $tabInsuranceTypesList
    );

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;

    require $directory . '/view/rentContract/pdfList.cgi';
}


# Function: uploadDocumentAction
# Dépôt temporaire des documents
sub uploadDocumentAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'localeId' => $tabUserInfos->{'locale.id'},
        'errors'   => undef
    );

    # Récuperation du fichier
    my $file = &LOC::Request::getFile('file');
    # Quand on charge une photo
    if ($file->{'name'} ne '' && $file->{'name'} ne './')
    {
        my $originalFileName = $file->{'name'};

        # Taille de fichier maximale autorisée
        my $maxFileSize = &LOC::Characteristic::getAgencyValueByCode('MAXTAILLEDOC', $tabUserInfos->{'agency.id'}) * 1;

        # Taille du fichier > à la taille Max autorisée
        if ($file->{'size'} > $maxFileSize)
        {
            unlink($file->{'tmp_name'});
            $tabViewData{'errors'} = 'maxSize';
        }
        else
        {
            # On se positionne dans les repertoires
            my $tmpFilesDir = &LOC::Util::getCommonFilesPath('temp/');
            my $tmpFileName = $file->{'tmp_name'};
            $tmpFileName =~ s/\/tmp\///g;

            # On deplace le fichier dans le dossier temporaire
            if (!&File::Copy::move($file->{'tmp_name'}, $tmpFilesDir . $tmpFileName))
            {
                print STDERR "Erreur sur le déplacement de ".$file->{'tmp_name'} . " dans " . $tmpFilesDir . $tmpFileName . " :\n";
                print STDERR $!;
                unlink($file->{'tmp_name'});
                $tabViewData{'errors'} = 'fatalError';
            }

            $tabViewData{'tempName'} = $tmpFileName;
        }
    }
    else
    {
        $tabViewData{'errors'} = 'unknownFile';
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentContract/uploadDocument.cgi';
}

# Function: downloadDocumentAction
# Téléchargement d'un document
sub downloadDocumentAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'localeId' => $tabUserInfos->{'locale.id'},
        'errors'   => undef
    );

    # Récupération du fichier
    my $id = &LOC::Request::getInteger('documentId', 0);
    $tabViewData{'mode'} = &LOC::Request::getString('mode', 'download');

    my $tabOptions = {
        'getContents' => 1,
    };

    my $tabInfos = &LOC::Attachments::getInfos($tabUserInfos->{'country.id'}, $id, $tabOptions);
    if ($tabInfos && $tabInfos->{'fileContent'} ne '')
    {
        $tabViewData{'fileInfos'} = $tabInfos;
    }
    else
    {
        $tabViewData{'errors'} = 'unknownFile';
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentContract/downloadDocument.cgi';
}


# Function: jsonDocumentsMailUrl
# Génération de l'URL pour la popup d'envoi des mails
sub jsonDocumentsMailUrlAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $countryId = $tabUserInfos->{'country.id'};

    our %tabViewData = ();

    my $tabDocuments  = &LOC::Request::getArray('documents');
    my $recipientMail = &LOC::Request::getString('recipientMail', undef);
    my $contractId    = &LOC::Request::getInteger('entityId', 0);
    my $nb = scalar @$tabDocuments;

    if ($nb > 0)
    {
        # Récupération des informations à mettre dans tabAdditionalAttachments
        my @tabAdditionalAttachments;
        my @tabEndActionsContent;
        foreach my $documentInfos (@$tabDocuments)
        {
            my ($docType, $docId) = split(':', $documentInfos->{'id'});
            if ($docType eq 'att')
            {
                push(@tabAdditionalAttachments, {
                    'attachmentId' => $docId,
                    'filename'     => $documentInfos->{'fileName'},
                    'extension'    => $documentInfos->{'extension'}
                });

                push(@tabEndActionsContent, $documentInfos->{'fileName'});
            }
        }

        my $schemaName = &LOC::Db::getSchemaName($countryId);

        $tabViewData{'url'} = &LOC::Request::createRequestUri('print:gui:email', {
            'defaultRecipient'         => $recipientMail,
            'documentAgency.id'        => $tabUserInfos->{'agency.id'},
            'tabAdditionalAttachments' => \@tabAdditionalAttachments,
            'tabEndActions' => [
                {
                    'type'      => 'addTrace',
                    'functionality'   => 'CONTRAT',
                    'functionalityId' => $contractId,
                    'content'         => join(', ', @tabEndActionsContent),
                    'tabContent'      => {
                        'schema'  => $schemaName,
                        'code'    => LOC::Log::EventType::SENDMAILDOCUMENTS,
                        'old'     => undef,
                        'new'     => undef,
                        'content' => '<%recipient> - <%content>'
                    }
                }
            ]
        });
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentContract/jsonDocumentsMailUrl.cgi';
}

# Function: jsonDayExtraInfosAction
# Récupère les dates de début max et date de fin min autorisées sur un document en fonction du jour
sub jsonDayExtraInfosAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $countryId = $tabUserInfos->{'country.id'};

    our %tabViewData = ();

    my $agencyId = &LOC::Request::getString('agencyId');
    my $type     = &LOC::Request::getString('type');
    my $date     = &LOC::Request::getString('date');
    my $appliedToContractId = &LOC::Request::getInteger('appliedToContractId');

    my $valueDate = &LOC::Request::getString('valueDate');

    if ($date)
    {
        $tabViewData{'periodLimitDates'} = &LOC::Day::getPeriodLimitDates($agencyId, $type, 'contract', $date, $valueDate);
    }
    if ($appliedToContractId)
    {
        $tabViewData{'appliedToContractInfos'} = &LOC::Day::getAppliedToContractInfos($countryId, $appliedToContractId, LOC::Day::GETAPPLIEDTOCONTRACTINFOS_DETAILS);
        $tabViewData{'appliedToContractInfos'}->{'url'} = &LOC::Request::createRequestUri('rent:rentContract:view',
                                                                {'contractId' => $appliedToContractId,
                                                                'tabIndex'   => LOC::Day::DEFAULT_CONTRACT_TABINDEX});
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentContract/jsonDayExtraInfos.cgi';
}

# Function: _getModelInfos
# Récupérer les informations d'un modèle de machine
#
# Parameters:
# string  $countryId  - Pays
# int     $modelId    - Id du modèle de machine
# int     $customerId - Id du client
# string  $agencyId   - Id de l'agence pour les taux d'utilisation
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hashref - Informations du modèle de machine
sub _getModelInfos
{
    my ($countryId, $modelId, $customerId, $agencyId, $tabOptions) = @_;

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Récupération des informations du modèle
    my $tabModelInfos = &LOC::Model::getInfos($countryId, $modelId);

    # Y'a-t-il des machines réservables de ce modèle ?
    my $isModelRentable = &LOC::Model::isRentable($countryId, $agencyId, $modelId);

    # Taux d'utilisation
    my %tabModelUseRate = &LOC::Model::getUseRate($modelId, $agencyId);
    my %tabTariffFamilyUseRate = &LOC::TariffFamily::getUseRate($tabModelInfos->{'tariffFamily.id'}, $agencyId);


    # Préparation des données à récupérer
    my $tabData = {
        'id'                  => $tabModelInfos->{'id'},
        'label'               => $tabModelInfos->{'label'},
        'fullName'            => $tabModelInfos->{'fullName'},
        'machinesFamilyLabel' => $tabModelInfos->{'machinesFamily.fullName'},
        'tariffFamilyLabel'   => $tabModelInfos->{'tariffFamily.fullName'},
        'isFuelNeeded'        => $tabModelInfos->{'machinesFamily.isFuelNeeded'},
        'fuelQtyEstimByDay'   => $tabModelInfos->{'tariffFamily.estimateFuel'},
        'fuelQtyMax'          => $tabModelInfos->{'tariffFamily.fuelMax'},
        'load'                => $tabModelInfos->{'load'},
        'isCleaningPossible'  => $tabModelInfos->{'isCleaningPossible'},
        'tabTypes'            => $tabModelInfos->{'tabTypes'},
        'isRentable'          => $isModelRentable
    };
    if (!$tabModelInfos->{'machinesFamily.isFuelNeeded'})
    {
        $tabData->{'fuelQtyEstimByDay'} = 0;
        $tabData->{'fuelQtyMax'} = 0;
    }

    # Taux d'utilisation
    $tabData->{'tabUseRates'} = [
        [[$tabModelUseRate{'useRate'}->{'agency'}, $tabModelUseRate{'nbTotal'}->{'agency'}],
         [$tabModelUseRate{'useRate'}->{'area'}, $tabModelUseRate{'nbTotal'}->{'area'}],
         [$tabModelUseRate{'useRate'}->{'country'}, $tabModelUseRate{'nbTotal'}->{'country'}]],
        [[$tabTariffFamilyUseRate{'useRate'}->{'agency'}, $tabTariffFamilyUseRate{'nbTotal'}->{'agency'}],
         [$tabTariffFamilyUseRate{'useRate'}->{'area'}, $tabTariffFamilyUseRate{'nbTotal'}->{'area'}],
         [$tabTariffFamilyUseRate{'useRate'}->{'country'}, $tabTariffFamilyUseRate{'nbTotal'}->{'country'}]]
    ];

    # Tarifs de base
    $tabData->{'tabTariffs'} = &LOC::Tariff::Rent::getCustomerTariffs(
                                            $countryId,
                                            $customerId,
                                            {'tariffFamily.id' => $tabModelInfos->{'tariffFamily.id'}});

    # Liste des types de nettoyages associés
    my $tabCleaningTypes = &LOC::Cleaning::getTypesList($countryId, LOC::Util::GETLIST_ASSOC,
                                                        {'energy' => $tabModelInfos->{'machinesFamily.energy'}});
    $tabData->{'tabCleaningTypes'} = [];
    if ($tabModelInfos->{'isCleaningPossible'})
    {
        foreach my $tabClnInfos (values %$tabCleaningTypes)
        {
            push(@{$tabData->{'tabCleaningTypes'}}, {
                'id'         => $tabClnInfos->{'id'} * 1,
                'label'      => $tabClnInfos->{'label'},
                'amount'     => $tabClnInfos->{'amount'} * 1,
                'isStandard' => $tabClnInfos->{'isStandard'} * 1
            });
        }
    }

    # Modèle de type accessoire ?
    $tabData->{'isAccessory'} = $tabModelInfos->{'isAccessory'};

    # Machine souhaitée disponible
    if ($tabOptions->{'getWishedMachines'})
    {
        my $tabSubOptions = {};
        if (ref($tabOptions->{'getWishedMachines'}) eq 'HASH')
        {
            $tabSubOptions = $tabOptions->{'getWishedMachines'};
        }

        my $tabFilters = {
            'modelId'  => $modelId,
            'agencyId' => $agencyId,
            'stateId'  => [
                LOC::Machine::STATE_RENTED,
                LOC::Machine::STATE_AVAILABLE,
                LOC::Machine::STATE_RECOVERY,
                LOC::Machine::STATE_REVIEW,
                LOC::Machine::STATE_CONTROL,
                LOC::Machine::STATE_BROKEN,
                LOC::Machine::STATE_CHECK,
                LOC::Machine::STATE_TRANSFER,
                LOC::Machine::STATE_DELIVERY
            ]
        };
        my $tabMachines = &LOC::Machine::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters, LOC::Machine::GETINFOS_BASE);

        $tabData->{'tabWishedMachines'} = [];
        if (exists $tabSubOptions->{'add'})
        {
            push(@{$tabData->{'tabWishedMachines'}}, [$tabSubOptions->{'add'}->{'id'}, $tabSubOptions->{'add'}->{'parkNumber'}]);
        }
        foreach my $tabMachineInfos (values %$tabMachines)
        {
            if (!exists $tabSubOptions->{'add'} || $tabSubOptions->{'add'}->{'id'} != $tabMachineInfos->{'id'})
            {
                push(@{$tabData->{'tabWishedMachines'}}, [$tabMachineInfos->{'id'}, $tabMachineInfos->{'parkNumber'}]);
            }
        }
    }

    return $tabData;
}


# Function: _getMachineInfos
# Récupérer les informations de la machine
#
# Parameters:
# string  $countryId  - Pays
# int     $machineId  - Id de de machine
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# hashref - Informations sur la machine
sub _getMachineInfos
{
    my ($countryId, $machineId, $tabOptions) = @_;

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    if (!$machineId)
    {
        return {
            'id' => 0
        };
    }

    my $tabMachineInfos = &LOC::Machine::getInfos($countryId, $machineId, LOC::Machine::GETINFOS_BASE | LOC::Machine::GETINFOS_TELEMATICS,
                                                  {'level' => LOC::Machine::LEVEL_MACHINESFAMILY});

    my $tabData = {
        'id'                   => $tabMachineInfos->{'id'},
        'stateId'              => $tabMachineInfos->{'state.id'},
        'modelId'              => $tabMachineInfos->{'model.id'},
        'modelLabel'           => $tabMachineInfos->{'model.label'},
        'modelFullName'        => $tabMachineInfos->{'model.fullName'},
        'parkNumber'           => $tabMachineInfos->{'parkNumber'},
        'agencyId'             => $tabMachineInfos->{'agency.id'},
        'visibleOnAgencyId'    => $tabMachineInfos->{'visibleOnAgency.id'},
        'visibleOnAgencyLabel' => $tabMachineInfos->{'visibleOnAgency.label'},
        'visibilityMode'       => $tabMachineInfos->{'visibilityMode'},
        'isRentableModel'      => undef,
        'contractId'           => $tabMachineInfos->{'contract.id'},
        'isSiteBlocked'        => $tabMachineInfos->{'isSiteBlocked'},
        'isControlVgp'         => $tabMachineInfos->{'isControlVgp'},
        'control.nextDate'     => $tabMachineInfos->{'control.nextDate'},
        'control.score'        => $tabMachineInfos->{'control.score'},
        'isTelematicActivated' => ($tabMachineInfos->{'telematicId'} &&
                                   &LOC::Util::in_array(LOC::StartCode::Contract::Rent::SCOPE, $tabMachineInfos->{'telematicScope'}) ? 1 : 0)
    };

    # Récupération des informations sur l'agence de visibilité de la machine
    if ($tabMachineInfos->{'visibleOnAgency.id'} ne '')
    {
        $tabData->{'isRentableModel'} = &LOC::Model::isRentable(
                                                            $countryId,
                                                            $tabMachineInfos->{'visibleOnAgency.id'},
                                                            $tabMachineInfos->{'model.id'});
    }
    else
    {
        # Le modèle est-il louable sur l'agence physique
        $tabData->{'isRentableModel'} = &LOC::Model::isRentable($countryId, $tabMachineInfos->{'agency.id'},
                                                            $tabMachineInfos->{'model.id'});
    }

    return $tabData;
}



# Function: _getDaysList
# Récupérer la liste des jours supplémentaires ou déduits sur le contrat
#
# Parameters:
# string  $countryId        - Pays
# int     $contractId       - Id du contrat
# string  $type             - Type de jour
# bool    $isDocumentClosed - Indique si le contrat est clôturé ou non.
#
# Returns:
# arrayref - Tableau des jours
sub _getDaysList
{
    my ($countryId, $contractId, $type, $isDocumentClosed) = @_;

    my $tabDays = [];
    my $tabFilters = {
        'type'       => $type,
        'contractId' => $contractId
    };
    my $tabOptions = {
        'orderBy' => [
            'toComplete:DESC',
            'date:DESC'
        ],
        'getAppliedToContractFlags' => LOC::Day::GETAPPLIEDTOCONTRACTINFOS_DETAILS
    };
    my $tabList= &LOC::Day::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                    $tabFilters,
                                    LOC::Day::GETINFOS_DURATION | LOC::Day::GETINFOS_APPLIEDTOCONTRACT,
                                    $tabOptions);
    if ($tabList)
    {
        foreach my $tabDayInfos (values(%$tabList))
        {
            my $tabAppliedToContractInfos = $tabDayInfos->{'appliedToContract.tabInfos'};
            if ($tabAppliedToContractInfos)
            {
                $tabAppliedToContractInfos->{'url'} = &LOC::Request::createRequestUri('rent:rentContract:view',
                                                                {'contractId' => $tabAppliedToContractInfos->{'id'},
                                                                 'tabIndex'   => LOC::Day::DEFAULT_CONTRACT_TABINDEX});
            }

            my $tabData = {
                'id'                    => $tabDayInfos->{'id'},
                'type'                  => $tabDayInfos->{'type'},
                'appliedToContract'     => $tabAppliedToContractInfos,
                'date'                  => $tabDayInfos->{'date'},
                'invoiceDuration'     => {
                    'id'       => $tabDayInfos->{'invoiceDuration.id'},
                    'label'    => $tabDayInfos->{'invoiceDuration.label'},
                    'duration' => $tabDayInfos->{'invoiceDuration.tabInfos'}->{'duration'}
                },
                'reason'                => ($tabDayInfos->{'reason.id'} ? {
                    'id'    => $tabDayInfos->{'reason.id'},
                    'label' => $tabDayInfos->{'reason.label'}
                } : undef),
                'comment'               => $tabDayInfos->{'comment'},
                'beginDateMax'          => $tabDayInfos->{'beginDateMax'},
                'endDateMin'            => $tabDayInfos->{'endDateMin'},
                'isInvoiceable'         => $tabDayInfos->{'isInvoiceable'},
                'isInvoiced'            => $tabDayInfos->{'isInvoiced'},
                'isMigrated'            => $tabDayInfos->{'isMigrated'},
                'isToComplete'          => (!$tabDayInfos->{'date'} && !$tabDayInfos->{'isInvoiced'} && !$isDocumentClosed ? 1 : 0),
                'isDocumentClosed'      => $isDocumentClosed
            };
            push(@$tabDays, $tabData);
        }
    }

    return $tabDays;
}


# Function: _getExternalDaysList
# Récupérer la liste des jours supplémentaires ou déduits saisis sur un autre contrat
#
# Parameters:
# string  $countryId  - Pays
# int     $contractId - Id du contrat
# string  $type       - Type de jour
#
# Returns:
# arrayref - Tableau des jours
sub _getExternalDaysList
{
    my ($countryId, $contractId, $type) = @_;

    my $tabResult = {
        'count'    => 0,
        'duration' => 0,
        'list'     => []
    };

    my $tabFilters = {
        'type'       => $type,
        'contractId' => {
            'values'     => $contractId,
            'isExternal' => 1
        }
    };
    my $tabOptions = {
        'orderBy' => [
            'contractId',
            'date:DESC'
        ]
    };
    my $tabList= &LOC::Day::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                    $tabFilters,
                                    LOC::Day::GETINFOS_DURATION | LOC::Day::GETINFOS_CONTRACT,
                                    $tabOptions);
    if ($tabList)
    {
        foreach my $tabDayInfos (values(%$tabList))
        {
            my $tabData = {
                'id'                    => $tabDayInfos->{'id'},
                'contract'              => {
                    'id'   => $tabDayInfos->{'contract.id'},
                    'code' => $tabDayInfos->{'contract.tabInfos'}->{'code'}
                },
                'type'                  => $tabDayInfos->{'type'},
                'date'                  => $tabDayInfos->{'date'},
                'invoiceDuration'     => {
                    'id'       => $tabDayInfos->{'invoiceDuration.id'},
                    'label'    => $tabDayInfos->{'invoiceDuration.label'},
                    'duration' => $tabDayInfos->{'invoiceDuration.tabInfos'}->{'duration'}
                },
                'reason'                => ($tabDayInfos->{'reason.id'} ? {
                    'id'    => $tabDayInfos->{'reason.id'},
                    'label' => $tabDayInfos->{'reason.label'}
                } : undef),
                'comment'               => $tabDayInfos->{'comment'},
                'isInvoiceable'         => $tabDayInfos->{'isInvoiceable'},
                'isInvoiced'            => $tabDayInfos->{'isInvoiced'}
            };
            push(@{$tabResult->{'list'}}, $tabData);

            $tabResult->{'count'}++;
            if ($tabDayInfos->{'isInvoiceable'})
            {
                $tabResult->{'duration'} += $tabDayInfos->{'invoiceDuration.tabInfos'}->{'duration'};
            }
        }
    }

    return $tabResult;
}


# Function: _getDaysListFromEstimateLine
# Récupérer la liste des jours supplémentaires ou déduits sur le contrat
#
# Parameters:
# string  $countryId      - Pays
# int     $estimateLineId - Id du document
# string  $type           - Type de jour
#
# Returns:
# arrayref - Tableau des jours +
sub _getDaysListFromEstimateLine
{
    my ($countryId, $estimateLineId, $type) = @_;

    my $tabDays = [];
    my $tabFilters = {
        'type'           => $type,
        'estimateLineId' => $estimateLineId
    };
    my $tabOptions = {
        'orderBy' => [
            'toComplete:DESC',
            'date:DESC'
        ]
    };
    my $tabList= &LOC::Day::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                    $tabFilters,
                                    LOC::Day::GETINFOS_DURATION | LOC::Day::GETINFOS_APPLIEDTOCONTRACT,
                                    $tabOptions);
    if ($tabList)
    {
        foreach my $tabDayInfos (values(%$tabList))
        {
            my $tabAppliedToContractInfos = $tabDayInfos->{'appliedToContract.tabInfos'};
            if ($tabAppliedToContractInfos)
            {
                $tabAppliedToContractInfos->{'url'} = &LOC::Request::createRequestUri('rent:rentContract:view',
                                                                {'contractId' => $tabAppliedToContractInfos->{'id'},
                                                                 'tabIndex'   => LOC::Day::DEFAULT_CONTRACT_TABINDEX});
            }

            my $tabData = {
                'id'                    => undef,
                'type'                  => $tabDayInfos->{'type'},
                'appliedToContract'     => $tabAppliedToContractInfos,
                'date'                  => $tabDayInfos->{'date'},
                'invoiceDuration'     => {
                    'id'       => $tabDayInfos->{'invoiceDuration.id'},
                    'label'    => $tabDayInfos->{'invoiceDuration.label'},
                    'duration' => $tabDayInfos->{'invoiceDuration.tabInfos'}->{'duration'}
                },
                'reason'                => ($tabDayInfos->{'reason.id'} ? {
                    'id'    => $tabDayInfos->{'reason.id'},
                    'label' => $tabDayInfos->{'reason.label'}
                } : undef),
                'comment'               => $tabDayInfos->{'comment'},
                'beginDateMax'          => $tabDayInfos->{'beginDateMax'},
                'endDateMin'            => $tabDayInfos->{'endDateMin'},
                'isInvoiceable'         => $tabDayInfos->{'isInvoiceable'},
                'isInvoiced'            => 0,
                'isMigrated'            => $tabDayInfos->{'isMigrated'},
                'isToComplete'          => (!$tabDayInfos->{'date'} && !$tabDayInfos->{'isInvoiced'} ? 1 : 0),
                'isDocumentClosed'      => 0
            };
            push(@$tabDays, $tabData);
        }
    }

    return $tabDays;
}

# Function: _getContractInfosFromEstimateLine
# Récupérer les informations d'un nouveau contrat pré-rempli grâce aux informations d'une ligne de devis
#
# Parameters:
#
# Returns:
# hashref - Informations sur le contrat
sub _getContractInfosFromEstimateLine
{
    my ($countryId, $tabUserInfos, $estimateLineId, $tempId, $tabError) = @_;


    # Récupération des informations sur les lignes du devis
    my $tabEstimateLineInfos = &LOC::Estimate::Rent::getLineInfos(
                                                                $countryId,
                                                                $estimateLineId,
                                                                LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE |
                                                                LOC::Estimate::Rent::GETLINEINFOS_INDEX |
                                                                LOC::Estimate::Rent::GETLINEINFOS_DURATIONS |
                                                                LOC::Estimate::Rent::GETLINEINFOS_INSURANCE |
                                                                LOC::Estimate::Rent::GETLINEINFOS_APPEAL |
                                                                LOC::Estimate::Rent::GETLINEINFOS_CLEANING |
                                                                LOC::Estimate::Rent::GETLINEINFOS_FUEL |
                                                                LOC::Estimate::Rent::GETLINEINFOS_RESIDUES |
                                                                LOC::Estimate::Rent::GETLINEINFOS_EXT_RENTSERVICES |
                                                                LOC::Estimate::Rent::GETLINEINFOS_EXT_TARIFFRENT |
                                                                LOC::Estimate::Rent::GETLINEINFOS_EXT_TARIFFTRSP |
                                                                LOC::Estimate::Rent::GETLINEINFOS_CONVERTIBLE
                                                               );
    if (!$tabEstimateLineInfos)
    {
        $tabError->{'type'} = 'unknown';
        $tabError->{'data'} = {
            'country.label' => $tabUserInfos->{'nomadCountry.label'}
        };
        return undef;
    }
    if (!$tabEstimateLineInfos->{'isConvertible'})
    {
        $tabError->{'type'} = 'convert';
        $tabError->{'data'} = {
            'index'         => $tabEstimateLineInfos->{'index'},
            'estimate.code' => $tabEstimateLineInfos->{'code'}
        };
        return undef;
    }

    # Récupération des informations du devis
    my $tabEstimateInfos = &LOC::Estimate::Rent::getInfos(
                                                       $countryId,
                                                       $tabEstimateLineInfos->{'estimate.id'},
                                                       LOC::Estimate::Rent::GETINFOS_BASE |
                                                       LOC::Estimate::Rent::GETINFOS_NEGOTIATOR |
                                                       LOC::Estimate::Rent::GETINFOS_ERRONEOUSLINES |
                                                       LOC::Estimate::Rent::GETINFOS_PRINTABLE |
                                                       LOC::Estimate::Rent::GETINFOS_VALUEDATE |
                                                       LOC::Estimate::Rent::GETINFOS_EXT_SITE
                                                      );
    if (!$tabEstimateInfos)
    {
        $tabError->{'type'} = 'unknown';
        $tabError->{'data'} = {
            'country.label' => $tabUserInfos->{'nomadCountry.label'}
        };
        return undef;
    }

	my $estimateValueDate = $tabEstimateInfos->{'valueDate'};
    my $estimateLineId    = $tabEstimateLineInfos->{'id'};
    my $countryId         = $tabEstimateInfos->{'country.id'};
    my $agencyId          = $tabEstimateInfos->{'agency.id'};

    # Informations sur le pays
    my $tabCountryInfos = &LOC::Country::getInfos($countryId);

    # Période de location
    my $realDuration          = $tabEstimateLineInfos->{'realDuration'};
    my $isRealDurationChanged = $tabEstimateLineInfos->{'isRealDurationChanged'};
    my $calendarDuration      = $tabEstimateLineInfos->{'calendarDuration'};
    my $beginDate             = $tabEstimateLineInfos->{'beginDate'};
    my $endDate               = $tabEstimateLineInfos->{'endDate'};
    if ($beginDate eq '' || !&LOC::Date::checkMySQLDate($beginDate))
    {
        $beginDate = '';
    }
    if ($endDate eq '' || !&LOC::Date::checkMySQLDate($endDate))
    {
        $endDate = '';
    }
    if ($beginDate ne '')
    {
        if ($endDate eq '')
        {
            $endDate = &LOC::Date::getRentEndDate($beginDate, $realDuration, $agencyId);
        }
        $calendarDuration = &LOC::Date::getDeltaDays($beginDate, $endDate);
    }

    # Informations sur le client
    my $tabCustomerInfos = &LOC::Customer::getInfos($countryId, $tabEstimateInfos->{'customer.id'},
                                                    LOC::Customer::GETINFOS_TARIFF |
                                                    LOC::Customer::GETINFOS_CRC |
                                                    LOC::Customer::GETINFOS_FLAGS);
    $tabEstimateInfos->{'customer.tabInfos'} = $tabCustomerInfos;

    # Informations sur le chantier
    my $tabSiteInfos = $tabEstimateInfos->{'site.tabInfos'};


    # Vérification de l'assurance de la ligne de devis par rapport à l'assurance actuelle du client
    my $insuranceTypeId = $tabEstimateLineInfos->{'insuranceType.id'};
    my $insuranceRate = $tabEstimateLineInfos->{'insurance.rate'};
    if (($tabCustomerInfos->{'insurance.isInvoiced'} &&
         $tabCustomerInfos->{'insurance.rate'} != $tabCountryInfos->{'insurance.defaultRate'}) ||
        ($tabEstimateLineInfos->{'insurance.isInvoiced'} && $insuranceRate != $tabCountryInfos->{'insurance.defaultRate'}))
    {
        # Si l'assurance n'est plus possible par rapport à l'assurance actuelle du client, on remet l'assurance du client
        $insuranceTypeId = $tabCustomerInfos->{'insuranceType.id'};
        $insuranceRate = $tabCustomerInfos->{'insurance.rate'};
    }

    # Vérification de la participation au recyclage
    my $residuesMode  = $tabEstimateLineInfos->{'residues.mode'};
    my $residuesValue = $tabEstimateLineInfos->{'residues.value'};
    if ($residuesMode != LOC::Common::RESIDUES_MODE_NONE)
    {
        $residuesMode = &LOC::Characteristic::getAgencyValueByCode('DECHETSMODE', $agencyId, $estimateValueDate) * 1;
        if ($residuesMode == LOC::Common::RESIDUES_MODE_PRICE)
        {
            $residuesValue = &LOC::Characteristic::getAgencyValueByCode('MTDECHETS', $agencyId, $estimateValueDate) * 1;
        }
        elsif ($residuesMode == LOC::Common::RESIDUES_MODE_PERCENT)
        {
            $residuesValue = &LOC::Characteristic::getAgencyValueByCode('PCDECHETS', $agencyId, $estimateValueDate) * 1;
        }
    }


    my $tabData = {
        'id'                       => undef,
        'linkedEstimateLine'       => {
            'id'           => $tabEstimateLineInfos->{'id'},
            'index'        => $tabEstimateLineInfos->{'index'},
            'estimateId'   => $tabEstimateInfos->{'id'},
            'estimateCode' => $tabEstimateInfos->{'code'}
        },
        'index'                    => 0,
        'code'                     => &LOC::Contract::Rent::generateCode('******', $agencyId, 0, 0),
        'country.id'               => $countryId,
        'agency.id'                => $agencyId,
        'creator.id'               => $tabUserInfos->{'id'},
        'isFullService'            => $tabEstimateInfos->{'isFullService'},
        'zone'                     => $tabEstimateInfos->{'zone'},
        'isEndDateNear'            => 0,

        'tabCustomer'              => $tabCustomerInfos,

        'tabSite'                  => {
            'id'                 => $tabSiteInfos->{'id'},
            'label'              => $tabSiteInfos->{'label'},
            'address'            => $tabSiteInfos->{'address'},
            'localityId'         => $tabSiteInfos->{'locality.id'},
            'localityLabel'      => $tabSiteInfos->{'locality.label'},
            'zone'               => $tabEstimateInfos->{'zone'},
            'contactFullName'    => $tabSiteInfos->{'contact.fullName'},
            'contactTelephone'   => $tabSiteInfos->{'contact.telephone'},
            'contactEmail'       => $tabSiteInfos->{'contact.email'},
            'deliveryHour'       => $tabSiteInfos->{'deliveryHour'},
            'deliveryTimeSlotId' => $tabSiteInfos->{'deliveryTimeSlot.id'} || 0,
            'recoveryHour'       => $tabSiteInfos->{'recoveryHour'},
            'recoveryTimeSlotId' => $tabSiteInfos->{'recoveryTimeSlot.id'} || 0,
            'isHasWharf'         => $tabSiteInfos->{'isHasWharf'},
            'isAnticipated'      => $tabSiteInfos->{'isAnticipated'},
            'isAsbestos'         => $tabSiteInfos->{'isAsbestos'},
            'comments'           => $tabSiteInfos->{'comments'}
        },

        'tabMachineInfos'          => {
            'id' => 0
        },
        'isFinalAllocation'        => 0,

        'tabWishedMachineInfos'    => undef,

        'tabModelInfos'            => &_getModelInfos($countryId,
                                                      $tabEstimateLineInfos->{'model.id'},
                                                      $tabEstimateInfos->{'customer.id'},
                                                      $agencyId,
                                                      {'getWishedMachines' => 1}),

        'isImperativeModel'        => $tabEstimateLineInfos->{'isImperativeModel'},

        'tabDuration' => {
            'beginDate'               => $beginDate,
            'endDate'                 => $endDate,
            'calendarDuration'        => $calendarDuration,

            'days'                    => {
                LOC::Day::TYPE_ADDITIONAL() => {
                    'list'                => &_getDaysListFromEstimateLine($countryId, $tabEstimateLineInfos->{'id'}, LOC::Day::TYPE_ADDITIONAL),
                    'nbUnbilled'          => $tabEstimateLineInfos->{'additionalDays'},
                    'nbBilled'            => 0,
                    'comment'             => undef,
                    'authorizedPeriod'    => &LOC::Day::getAuthorizedPeriod($agencyId, LOC::Day::TYPE_ADDITIONAL, 'contract', [$beginDate, $endDate], $estimateValueDate),
                    'tabDeltasPeriodDays' => &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('INTJRSPCT', $agencyId, $estimateValueDate))
                },
                LOC::Day::TYPE_DEDUCTED()   => {
                    'list'                => &_getDaysListFromEstimateLine($countryId, $tabEstimateLineInfos->{'id'}, LOC::Day::TYPE_DEDUCTED),
                    'nbUnbilled'          => $tabEstimateLineInfos->{'deductedDays'},
                    'nbBilled'            => 0,
                    'comment'             => undef,
                    'authorizedPeriod'    => &LOC::Day::getAuthorizedPeriod($agencyId, LOC::Day::TYPE_DEDUCTED, 'contract', [$beginDate, $endDate], $estimateValueDate),
                    'tabDeltasPeriodDays' => &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('INTJRSMCT', $agencyId, $estimateValueDate))
                }
            },

            'realDuration'            => $realDuration,
            'isRealDurationChanged'   => $isRealDurationChanged,
            'isDatesToConfirm'        => 0
        },

        'tabRentTariff' => $tabEstimateLineInfos->{'rentTariff.tabInfos'},

        'tabTransport'  => &_getTransportInfos($countryId, $agencyId, 'estimateLine', $tabEstimateLineInfos,
                                               {'isDuplication' => 1, 'tempId' => $tempId}),

        'tabRepair' => {
            'days'     => {},
            'services' => {},
            'sheets'   => []
        },

        'tabOtherServices' => {
            'insuranceTypeId'      => $insuranceTypeId,
            'insuranceRate'        => $insuranceRate,
            'appealTypeId'         => $tabEstimateLineInfos->{'appealType.id'},
            'appealValue'          => $tabEstimateLineInfos->{'appeal.value'},
            'cleaningTypeId'       => $tabEstimateLineInfos->{'cleaning.type.id'},
            'cleaningAmount'       => $tabEstimateLineInfos->{'cleaning.amount'},
            'residuesMode'         => $residuesMode,
            'residuesValue'        => $residuesValue,
            'fuelQuantity'         => undef,
            'fuelUnitPrice'        => $tabCountryInfos->{'fuelUnitPrice'},
            'fuelCurrentUnitPrice' => $tabCountryInfos->{'fuelUnitPrice'},
            'tabFuelEstimation'    => {
                'quantity'  => undef,
                'unitPrice' => 0,
                'amount'    => 0
            }
        },

        'tabProforma'              => &_getProformasInfos($countryId, 'estimateLine', $estimateLineId, 0),
        'proformaFlags'            => $tabEstimateLineInfos->{'proformaFlags'},

        'tabDocuments' => [],

        'tabHistory'               => [],

        'tabLinkedContracts'       => {},
        'tabOldServices'           => [],
        'oldServicesAmount'        => 0,
        'tabRentServices'          => [],
        'comments'                 => $tabEstimateInfos->{'comments'},
        'tracking'                 => '',
        'tabRentInvoice'           => {},
        'tabRepairInvoice'         => {},
        'creationDate'             => undef,
        'modificationDate'         => undef,
        'closureDate'              => undef,
        'valueDate'                => $estimateValueDate,
        'isLocked'                 => 0,
        'state.id'                 => LOC::Contract::Rent::STATE_PRECONTRACT,
        'negotiator.id'            => $tabEstimateInfos->{'negotiator.id'},
        'negotiator.fullName'      => $tabEstimateInfos->{'negotiator.fullName'},
        'isJoinDoc'                => $tabCustomerInfos->{'isJoinDoc'},
        'isPrintable'              => 0,

        'invoiceState'             => '',
        'lastInvoiceDate'          => '',

        'tabValorizationInfos'     => undef,

        'actionsDates'             => {},

        'possibilities'            => {
            'equipment'  => {
                'model'      => 1
            },
            'actions'    => {
                'valid'      => 1,
                'stop'       => 0,
                'reactivate' => 0,
                'invoice'    => 0,
                'restop'     => 0,
                'cancel'     => 0
            },
            'duration' => {
                'setInvoiceableDay'    => 1,
                'setNonInvoiceableDay' => 1
            }
        },
        'tabRights'                => &LOC::Contract::Rent::getRights($countryId, $agencyId, undef, $tabUserInfos->{'id'})
    };


    # Récupérer l'estimation carburant
    my $fuelEstimationInfos = &LOC::Proforma::getFuelEstimationInfos($countryId, 'estimateLine', $estimateLineId);
    if ($fuelEstimationInfos)
    {
        $tabData->{'tabOtherServices'}->{'tabFuelEstimation'} = $fuelEstimationInfos;
    }

    # Liste des services de location et transport
    foreach my $tabRentSrvInfos (values %{$tabEstimateLineInfos->{'tabRentServices'}})
    {
        if ($tabRentSrvInfos->{'state.id'} ne LOC::Estimate::Rent::SERVICESTATE_DELETED)
        {
            push(@{$tabData->{'tabRentServices'}}, {
                'amount'           => $tabRentSrvInfos->{'amount'},
                'comment'          => $tabRentSrvInfos->{'comment'},
                'rentService.id'   => $tabRentSrvInfos->{'rentService.id'},
                'rentService.name' => $tabRentSrvInfos->{'rentService.name'},
                'state.id'         => $tabRentSrvInfos->{'state.id'},
                'state.label'      => $tabRentSrvInfos->{'state.label'},
                'isRemovable'      => $tabRentSrvInfos->{'isRemovable'}
            });
        }
    }

    # TODO: Ne prendre que les informations nécessaires pour le client

    $tabData->{'tabCustomer'}->{'country.id'}             = $countryId;
    $tabData->{'tabCustomer'}->{'orderNo'}                = $tabEstimateInfos->{'orderNo'};
    $tabData->{'tabCustomer'}->{'orderContact.fullName'}  = $tabEstimateInfos->{'orderContact.fullName'};
    $tabData->{'tabCustomer'}->{'orderContact.telephone'} = $tabEstimateInfos->{'orderContact.telephone'};
    $tabData->{'tabCustomer'}->{'orderContact.email'}     = $tabEstimateInfos->{'orderContact.email'};


    return $tabData;
}


# Function: _getTransportInfos
# Récupérer les informations du transport
#
# Parameters:
# string  $countryId        - Pays
# string  $agencyId         - Identifiant de l'agence du document
# string  $documentType     - Type de document (contrat ou ligne de devis)
# hashref $tabDocumentInfos - Informations sur le contrat ou la ligne de devis
# hashref $tabOptions       - Options supplémentaires (pour la duplication de l'estimation transport)
#
# Returns:
# hashref - Informations sur le transport
sub _getTransportInfos
{
    my ($countryId, $agencyId, $documentType, $tabDocumentInfos, $tabOptions) = @_;

    my $tabData = {};

    # Estimation transport
    #---------------------
    my $tabTrspTariffInfos = $tabDocumentInfos->{'transportTariff.tabInfos'};
    if ($tabTrspTariffInfos->{'id'})
    {
        $tabData->{'tabTariff'} = {
            'id'                       => $tabTrspTariffInfos->{'id'},
            'baseAmount'               => $tabTrspTariffInfos->{'baseAmount'},
            'recoStandardPercent'      => $tabTrspTariffInfos->{'recoStandardPercent'},
            'maxiStandardPercent'      => $tabTrspTariffInfos->{'maxiStandardPercent'},
            'maxiStandardAmount'       => $tabTrspTariffInfos->{'maxiStandardAmount'},
            'specialReductionId'       => $tabTrspTariffInfos->{'specialReductionId'},
            'specialReductionPercent'  => $tabTrspTariffInfos->{'specialReductionPercent'},
            'specialReductionAmount'   => $tabTrspTariffInfos->{'specialReductionAmount'},
            'specialReductionStatus'   => $tabTrspTariffInfos->{'specialReductionStatus'},
            'standardReductionAmount'  => $tabTrspTariffInfos->{'standardReductionAmount'},
            'standardReductionPercent' => $tabTrspTariffInfos->{'standardReductionPercent'},
            'reductionAmount'          => $tabTrspTariffInfos->{'reductionAmount'},
            'reductionPercent'         => $tabTrspTariffInfos->{'reductionPercent'},
            'amount'                   => $tabTrspTariffInfos->{'amount'},
            'customerId'               => $tabTrspTariffInfos->{'customer.id'},
            'zone'                     => $tabTrspTariffInfos->{'zone'},
            'isNoSaved'                => $tabTrspTariffInfos->{'isSaved'},
            'possibilities'            => $tabTrspTariffInfos->{'possibilities'}
        };
    }
    else
    {
        $tabData->{'tabTariff'} = {
            'id'            => 0,
            'possibilities' => $tabTrspTariffInfos->{'possibilities'} || {}
        };
    }
    # Duplication de l'estimation
    if ($tabOptions->{'isDuplication'})
    {
        # Récupération et duplication de l'estimation transport
        $tabData->{'tabTariff'}->{'id'} = &LOC::Tariff::Transport::duplicateEstimation($countryId,
                                                                                       $tabData->{'tabTariff'}->{'id'},
                                                                                       $tabOptions->{'tempId'}, '', 2);
        $tabData->{'tabTariff'}->{'addMachineDeliveryLnkCtt.id'} = undef;
        $tabData->{'tabTariff'}->{'addMachineRecoveryLnkCtt.id'} = undef;
    }



    # Récupération des informations du transport
    #-------------------------------------------

    # LIVRAISON
    #----------
    my $tabTspInfosD = &LOC::Transport::getDocumentDeliveryInfos($countryId, $documentType, $tabDocumentInfos->{'id'},
                                                                 LOC::Transport::GETINFOS_FROM |
                                                                 LOC::Transport::GETINFOS_ORDER |
                                                                 LOC::Transport::GETINFOS_POSSIBILITIES,
                                                                 {'getToInfosFlags' => LOC::Transport::GETFROMTOINFOS_DETAILS});

    $tabData->{'tabDelivery'} = {
        'amount'             => $tabTrspTariffInfos->{'deliveryAmount'},
        'isAddMachine'       => $tabTrspTariffInfos->{'isAddMachineDelivery'},
        'addMachineLnkCtt'   => undef,
        'isSelf'             => ($tabTspInfosD ? $tabTspInfosD->{'isSelf'} : $tabTrspTariffInfos->{'isNoDelivery'}),
        'agency'             => {
            'id'    => $tabTspInfosD->{'agency.id'} || $agencyId,
            'label' => $tabTspInfosD->{'agency.label'} || &LOC::Agency::getLabelById($agencyId)
        },
        'delegatedAgencyId'  => $tabTspInfosD->{'delegatedAgency.id'},
        'transferFromAgency' => undef,
        'contract'           => undef,
        'isLoadingDone'      => $tabTspInfosD->{'loading'}->{'isDone'} || 0,
        'isUnloadingDone'    => $tabTspInfosD->{'unloading'}->{'isDone'} || 0,
        'extraInfos'         => {},
        'tabErrors'          => $tabTspInfosD->{'unloading'}->{'tabErrors'} || [],
        'action'             => undef,
        'selfFormUrls'       => undef,
        'isSynchroDisabled'  => $tabTspInfosD->{'isSynchroDisabled'} || 0,
        'possibilities'      => {
            'action' => ($tabTspInfosD->{'unloading'}->{'isDoable'} ?
                            ($tabTspInfosD->{'unloading'}->{'isDone'} ? 'undo' : 'do') : undef),
            'edit'   => $tabTspInfosD->{'possibilities'}->{'edit'} || 0
        }
    };

    # Infos supplémentaires
    if ($tabTspInfosD->{'order.tabInfos'})
    {
        $tabData->{'tabDelivery'}->{'extraInfos'}->{'order'} = {
            'code' => $tabTspInfosD->{'order.tabInfos'}->{'code'}
        };
    }
    if ($tabTspInfosD->{'unloading'}->{'date'})
    {
        $tabData->{'tabDelivery'}->{'extraInfos'}->{'dateDone'} = $tabTspInfosD->{'unloading'}->{'date'};
    }

    # Machine supp
    if ($tabTrspTariffInfos->{'addMachineDeliveryLnkCtt.id'})
    {
        $tabData->{'tabDelivery'}->{'addMachineLnkCtt'} = {
            'id'   => $tabTrspTariffInfos->{'addMachineDeliveryLnkCtt.id'},
            'code' => $tabTrspTariffInfos->{'addMachineDeliveryLnkCtt.code'},
            'url'  => $tabTrspTariffInfos->{'addMachineDeliveryLnkCtt.url'}
        };
    }

    # La provenance est un contrat
    if ($tabTspInfosD->{'from.type'} eq LOC::Transport::FROMTOTYPE_CONTRACT)
    {
        $tabData->{'tabDelivery'}->{'contract'} = {
            'id'              => $tabTspInfosD->{'from.id'},
            'agency'          => {
                'id'    => $tabTspInfosD->{'from.tabInfos'}->{'agency.id'},
                'label' => &LOC::Agency::getLabelById($tabTspInfosD->{'from.tabInfos'}->{'agency.id'})
            },
            'code'            => $tabTspInfosD->{'from.tabInfos'}->{'code'},
            'siteInfos'       => {
                'label'      => $tabTspInfosD->{'from.tabInfos'}->{'site.label'},
                'postalCode' => $tabTspInfosD->{'from.tabInfos'}->{'site.postalCode'},
                'city'       => $tabTspInfosD->{'from.tabInfos'}->{'site.city'}
            },
            'url'             => &LOC::Request::createRequestUri('rent:rentContract:view',
                                                                 {'contractId' => $tabTspInfosD->{'from.id'},
                                                                  'tabIndex'   => LOC::Transport::DEFAULT_CONTRACT_TABINDEX}),
            'limitDate'       => $tabTspInfosD->{'from.tabInfos'}->{'endDate'},
            'limitHour'       => substr($tabTspInfosD->{'from.tabInfos'}->{'window'}->{'from'}, 11, 5),
            'isAnticipatable' => 0,
            'amount'          => $tabTspInfosD->{'from.tabInfos'}->{'amount'} * 1
        };
    }

    # La provenance est une agence et ce n'est pas la même agence
    if ($tabTspInfosD->{'from.type'} eq LOC::Transport::FROMTOTYPE_AGENCY &&
        $tabTspInfosD->{'from.id'} ne $tabTspInfosD->{'agency.id'})
    {
        $tabData->{'tabDelivery'}->{'transferFromAgency'} = {
            'id'    => $tabTspInfosD->{'from.id'},
            'label' => $tabTspInfosD->{'from.tabInfos'}->{'label'}
        };
    }

    # Impression du bordereau
    if ($documentType eq 'contract' && $tabTspInfosD->{'possibilities'}->{'selfForm'})
    {
        # URL du PDF
        my $url = &LOC::Request::createRequestUri('transport:selfForm:pdf', {'transportId' => $tabTspInfosD->{'id'},
                                                                             'side' => LOC::Transport::SIDE_DELIVERY});

        # Récupération des configurations pour l'envoi par mail
        my $tabMailCfgs = &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('MAILBORDEREAULIV', $agencyId));

        # Remplacement des variables
        $tabMailCfgs->{'filename'} =~ s/<%contract.code>/$tabDocumentInfos->{'code'}/;
        $tabMailCfgs->{'subject'}  =~ s/<%contract.code>/$tabDocumentInfos->{'code'}/;
        $tabMailCfgs->{'content'}  =~ s/<%contract.code>/$tabDocumentInfos->{'code'}/;

        $tabData->{'tabDelivery'}->{'selfFormUrls'} = {
            'pdf'   => $url,
            'print' => &LOC::Request::createRequestUri('print:gui:print', {
                                                           'url'         => $url,
                                                           'tabEndActions' => [
                                                               {
                                                                   'type'      => 'logPrint',
                                                                   'entity'    => 'SELFFORM',
                                                                   'entityIds' => [$tabTspInfosD->{'id'}]
                                                               }
                                                           ]
                                                       }),
            'mail'  => &LOC::Request::createRequestUri('print:gui:email',
                                                       {
                                                           'url'               => $url,
                                                           'filename'          => $tabMailCfgs->{'filename'},
                                                           'documentAgency.id' => $tabDocumentInfos->{'agency.id'},
                                                           'defaultRecipient'  => $tabDocumentInfos->{'orderContact.email'},
                                                           'defaultSubject'    => $tabMailCfgs->{'subject'},
                                                           'defaultContent'    => $tabMailCfgs->{'content'}
                                                       })
        };
    }

    # État des lieux
    $tabData->{'tabDelivery'}->{'tabMachineInventoryInfos'} = &_getTransportMachineInventoryInfos($countryId, $tabDocumentInfos->{'id'}, LOC::MachineInventory::TYPE_DELIVERY);

    # Action de réalisation ou d'annulation (et messages de confirmation)
    #---------------------------------------------------------------------

    if ($tabTspInfosD->{'unloading'}->{'isDoable'})
    {
        if ($tabTspInfosD->{'unloading'}->{'isDone'})
        {
            $tabData->{'tabDelivery'}->{'action'} = {
                'type' => 'undo',
                'msgs' => {}
            };

            # type transfert
            if ($tabTspInfosD->{'type'} eq LOC::Transport::TYPE_SITESTRANSFER)
            {
                # reprise sur chantier
                if ($tabTspInfosD->{'isSelf'})
                {
                    # Si Etat du contrat de provenance est non faisable, pas possible d'annuler
                    # la réalisation de la récupération
                    if (!$tabTspInfosD->{'loading'}->{'isDoable'})
                    {
                        $tabData->{'tabDelivery'}->{'action'}->{'msgs'} = {
                            'current' => {'action' => {'unloading' => 0}}
                        };
                    }
                    else
                    {
                       $tabData->{'tabDelivery'}->{'action'}->{'msgs'} = {
                           'linked' => {'action' => {'loading' => 0, 'unloading' => 0}}
                       };
                    }
                }
                # avec récupération réalisée définitivement
                elsif ($tabTspInfosD->{'loading'}->{'isDone'} == LOC::Transport::ACTION_DONE)
                {
                    $tabData->{'tabDelivery'}->{'action'}->{'msgs'} = {
                        'current' => {'action' => {'unloading' => 0}}
                    };
                }
                # avec récupération réalisée manuellement
                elsif ($tabTspInfosD->{'loading'}->{'isDone'} == LOC::Transport::ACTION_USERDONE)
                {
                    # Si Etat du contrat de provenance est non faisable, pas possible d'annuler
                    # la réalisation de la récupération
                    if (!$tabTspInfosD->{'loading'}->{'isDoable'})
                    {
                        $tabData->{'tabDelivery'}->{'action'}->{'msgs'} = {
                            'current' => {'action' => {'unloading' => 0}}
                        };
                    }
                    else
                    {
                        $tabData->{'tabDelivery'}->{'action'}->{'msgs'} = {
                            'linked'  => {'action' => {'loading' => 0, 'unloading' => 0}},
                            'current' => {'action' => {'unloading' => 0}}
                        };
                    }
                }
            }
            # type livraison standard
            else
            {
                $tabData->{'tabDelivery'}->{'action'}->{'msgs'} = {
                    'current' => {'action' => {'unloading' => 0, 'loading' => 0}}
                };
            }
        }
        else
        {
            $tabData->{'tabDelivery'}->{'action'} = {
                'type' => 'do',
                'msgs' => []
            };
            # type transfert
            if ($tabTspInfosD->{'type'} eq LOC::Transport::TYPE_SITESTRANSFER)
            {
                if (!$tabTspInfosD->{'loading'}->{'isDone'})
                {
                    $tabData->{'tabDelivery'}->{'action'}->{'msgs'} = {
                        'linked' => {'action' => {'loading' => 1, 'unloading' => 1}}
                    };
                }
                else
                {
                    $tabData->{'tabDelivery'}->{'action'}->{'msgs'} = {
                        'current' => {'action' => {'unloading' => 1}}
                    };
                }
            }
            # livraison normale
            else
            {
                $tabData->{'tabDelivery'}->{'action'}->{'msgs'} = {
                    'current' => {'action' => {'unloading' => 1, 'loading' => 1}}
                };
            }
        }
    }


    # RECUPERATION
    #-------------
    my $tabTspInfosR = &LOC::Transport::getDocumentRecoveryInfos($countryId, $documentType, $tabDocumentInfos->{'id'},
                                                                 LOC::Transport::GETINFOS_TO |
                                                                 LOC::Transport::GETINFOS_ORDER |
                                                                 LOC::Transport::GETINFOS_POSSIBILITIES,
                                                                 {'getFromInfosFlags' => LOC::Transport::GETFROMTOINFOS_DETAILS});

    $tabData->{'tabRecovery'} = {
        'amount'             => $tabTrspTariffInfos->{'recoveryAmount'},
        'isAddMachine'       => $tabTrspTariffInfos->{'isAddMachineRecovery'},
        'addMachineLnkCtt'   => undef,
        'isSelf'             => ($tabTspInfosR ? $tabTspInfosR->{'isSelf'} : $tabTrspTariffInfos->{'isNoRecovery'}),
        'agency'             => {
            'id'    => $tabTspInfosR->{'agency.id'} || $agencyId,
            'label' => $tabTspInfosR->{'agency.label'} || &LOC::Agency::getLabelById($agencyId)
        },
        'delegatedAgencyId'  => $tabTspInfosR->{'delegatedAgency.id'},
        'transferToAgency'   => ($tabTspInfosR->{'transferToAgency.id'} ? {
            'id'    => $tabTspInfosR->{'transferToAgency.id'},
            'label' => $tabTspInfosR->{'transferToAgency.label'}
        } : undef),
        'contract'           => undef,
        'isLoadingDone'      => $tabTspInfosR->{'loading'}->{'isDone'} || 0,
        'isUnloadingDone'    => $tabTspInfosR->{'unloading'}->{'isDone'} || 0,
        'extraInfos'         => {},
        'tabErrors'          => $tabTspInfosR->{'loading'}->{'tabErrors'} || [],
        'action'             => undef,
        'selfFormUrls'       => undef,
        'isSynchroDisabled'  => $tabTspInfosR->{'isSynchroDisabled'} || 0,
        'possibilities'      => {
            'action' => ($tabTspInfosR->{'loading'}->{'isDoable'} ?
                             ($tabTspInfosR->{'loading'}->{'isDone'} ? 'undo' : 'do') : undef),
            'edit'   => $tabTspInfosR->{'possibilities'}->{'edit'} || 0
        }
    };

    # Infos supplémentaires
    if ($tabTspInfosR->{'order.tabInfos'})
    {
        $tabData->{'tabRecovery'}->{'extraInfos'}->{'order'} = {
            'code' => $tabTspInfosR->{'order.tabInfos'}->{'code'}
        };
    }
    if ($tabTspInfosR->{'loading'}->{'date'})
    {
        $tabData->{'tabRecovery'}->{'extraInfos'}->{'dateDone'} = $tabTspInfosR->{'loading'}->{'date'};
    }

    # Machine supp
    if ($tabTrspTariffInfos->{'addMachineRecoveryLnkCtt.id'})
    {
        $tabData->{'tabRecovery'}->{'addMachineLnkCtt'} = {
            'id'   => $tabTrspTariffInfos->{'addMachineRecoveryLnkCtt.id'},
            'code' => $tabTrspTariffInfos->{'addMachineRecoveryLnkCtt.code'},
            'url'  => $tabTrspTariffInfos->{'addMachineRecoveryLnkCtt.url'}
        };
    }
    # La destination est un contrat
    if ($tabTspInfosR->{'to.type'} eq LOC::Transport::FROMTOTYPE_CONTRACT)
    {
        $tabData->{'tabRecovery'}->{'contract'} = {
            'id'              => $tabTspInfosR->{'to.id'},
            'code'            => $tabTspInfosR->{'to.tabInfos'}->{'code'},
            'agency'          => {
                'id'    => $tabTspInfosR->{'to.tabInfos'}->{'agency.id'},
                'label' => &LOC::Agency::getLabelById($tabTspInfosR->{'to.tabInfos'}->{'agency.id'})
            },
            'siteInfos'       => {
                'label'      => $tabTspInfosR->{'to.tabInfos'}->{'site.label'},
                'postalCode' => $tabTspInfosR->{'to.tabInfos'}->{'site.postalCode'},
                'city'       => $tabTspInfosR->{'to.tabInfos'}->{'site.city'}
            },
            'url'             => &LOC::Request::createRequestUri('rent:rentContract:view',
                                                                 {'contractId' => $tabTspInfosR->{'to.id'},
                                                                  'tabIndex'   => LOC::Transport::DEFAULT_CONTRACT_TABINDEX}),
            'limitDate'       => $tabTspInfosR->{'to.tabInfos'}->{'beginDate'},
            'limitHour'       => substr($tabTspInfosR->{'to.tabInfos'}->{'window'}->{'till'}, 11, 5),
            'isAnticipatable' => $tabTspInfosR->{'to.tabInfos'}->{'isAnticipatable'},
            'amount'          => $tabTspInfosR->{'to.tabInfos'}->{'amount'} * 1
        };
    }

    # Impression du bordereau
    if ($documentType eq 'contract' && $tabTspInfosR->{'possibilities'}->{'selfForm'})
    {
        # URL du PDF
        my $url = &LOC::Request::createRequestUri('transport:selfForm:pdf', {'transportId' => $tabTspInfosR->{'id'},
                                                                             'side' => LOC::Transport::SIDE_RECOVERY});

        # Récupération des configurations pour l'envoi par mail
        my $tabMailCfgs = &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('MAILBORDEREAUREC', $agencyId));

        # Remplacement des variables
        $tabMailCfgs->{'filename'} =~ s/<%contract.code>/$tabDocumentInfos->{'code'}/;
        $tabMailCfgs->{'subject'}  =~ s/<%contract.code>/$tabDocumentInfos->{'code'}/;
        $tabMailCfgs->{'content'}  =~ s/<%contract.code>/$tabDocumentInfos->{'code'}/;

        $tabData->{'tabRecovery'}->{'selfFormUrls'} = {
            'pdf'   => $url,
            'print' => &LOC::Request::createRequestUri('print:gui:print', {
                                                           'url'         => $url,
                                                           'tabEndActions' => [
                                                               {
                                                                   'type'      => 'logPrint',
                                                                   'entity'    => 'SELFFORM',
                                                                   'entityIds' => [$tabTspInfosR->{'id'}]
                                                               }
                                                           ]
                                                       }),
            'mail'  => &LOC::Request::createRequestUri('print:gui:email',
                                                       {
                                                           'url'              => $url,
                                                           'filename'          => $tabMailCfgs->{'filename'},
                                                           'documentAgency.id' => $tabDocumentInfos->{'agency.id'},
                                                           'defaultRecipient'  => $tabDocumentInfos->{'orderContact.email'},
                                                           'defaultSubject'    => $tabMailCfgs->{'subject'},
                                                           'defaultContent'    => $tabMailCfgs->{'content'}
                                                       })
        };
    }

    # État des lieux
    $tabData->{'tabRecovery'}->{'tabMachineInventoryInfos'} = &_getTransportMachineInventoryInfos($countryId, $tabDocumentInfos->{'id'}, LOC::MachineInventory::TYPE_RECOVERY);


    # Action de réalisation ou d'annulation (et messages de confirmation)
    #---------------------------------------------------------------------

    if ($tabTspInfosR->{'loading'}->{'isDoable'})
    {
        if ($tabTspInfosR->{'loading'}->{'isDone'})
        {
            $tabData->{'tabRecovery'}->{'action'} = {
                'type' => 'undo',
                'msgs' => {}
            };

            # type transfert
            if ($tabTspInfosR->{'type'} eq LOC::Transport::TYPE_SITESTRANSFER)
            {
                # livraison associée non réalisée
                if($tabTspInfosR->{'unloading'}->{'isDone'} == LOC::Transport::ACTION_NOTDONE)
                {
                    $tabData->{'tabRecovery'}->{'action'}->{'msgs'} = {
                        'current' => {'action' => {'loading' => 0}}
                    };
                }
                # livraison associée réalisée
                else
                {
                    $tabData->{'tabRecovery'}->{'action'}->{'msgs'} = {
                        'linked' => {'action' => {'loading' => 0, 'unloading' => 0}}
                    };
                }
            }
            # type récupération standard
            else
            {
                $tabData->{'tabRecovery'}->{'action'}->{'msgs'} = {
                    'current' => {'action' => {'loading' => 0, 'unloading' => 0}}
                };
            }
        }
        else
        {
            $tabData->{'tabRecovery'}->{'action'} = {
                'type' => 'do',
                'msgs' => []
            };

            # type transfert
            if ($tabTspInfosR->{'type'} eq LOC::Transport::TYPE_SITESTRANSFER)
            {
                # enlèvement sur place
                if ($tabTspInfosR->{'isSelf'})
                {
                    $tabData->{'tabRecovery'}->{'action'}->{'msgs'} = {
                        'linked' => {'action' => {'loading' => 1, 'unloading' => 1}}
                    };
                }
                else
                {
                    $tabData->{'tabRecovery'}->{'action'}->{'msgs'} = {
                        'current' => {'action' => {'loading' => 1}},
                        'linked'  => {'action' => {'loading' => 1, 'unloading' => 1}}
                    };
                }
            }
            # type récup standard
            else
            {
                $tabData->{'tabRecovery'}->{'action'}->{'msgs'} = {
                    'current' => {'action' => {'loading' => 1, 'unloading' => 1}}
                };
            }
        }
    }


    return $tabData;
}


# Function: _getHistoryInfos
# Récupérer les historiques d'un contrat
#
# Parameters:
# string  $countryId  - Pays
# int     $contractId - Identifiant du contrat
# string  $localeId   - Identifiant de la locale pour les traductions
#
# Returns:
# hashref - Liste des historiques
sub _getHistoryInfos
{
    my ($countryId, $contractId, $localeId) = @_;

    # Récupération des historiques
    my @tabHistory = &LOC::Contract::Rent::getHistory($countryId, $contractId, $localeId);

    # Construction du tableau adapté à la vue
    my @tab = ();
    foreach my $tabLine (@tabHistory)
    {
        push(@tab, {
            'date'         => $tabLine->{'datetime'},
            'entity'       => $tabLine->{'functionality.name'},
            'userName'     => $tabLine->{'user.fullName'},
            'isUserActive' => ($tabLine->{'user.state.id'} ne LOC::User::STATE_INACTIVE ? 1 : 0),
            'event'        => $tabLine->{'eventType.label'},
            'description'  => $tabLine->{'content'},
            'extra'        => $tabLine->{'contentExtra'},

            'eventCode'      => $tabLine->{'eventType.code'}
        });
    }

    return \@tab;
}

# Function: _getActionsDates
# Récupérer les dernières dates d'actions définies d'un contrat
#
# Parameters:
# arrayref  $tabHistory  - Historique d'un contrat
#
# Returns:
# hashref - Liste des dernières dates d'actions
sub _getActionsDates
{
    my ($tabHistory) = @_;

    my $tabEventTypes = {
        LOC::Log::EventType::SENDMAIL()        => 'lastPrint', # envoi par mail
        LOC::Log::EventType::UPLOADPDF()       => 'lastPrint', # sortie PDF
        LOC::Log::EventType::PRINT()           => 'lastPrint', # impression
        LOC::Log::EventType::UPDSITEASBESTOS() => 'asbestos'   # modification amiante
    };
    my @tabEvents = keys(%$tabEventTypes);

    my @tabFilteredHistory = grep {&LOC::Util::in_array($_->{'eventCode'}, \@tabEvents); } @$tabHistory;
    my $tabActionsDates = {};

    foreach my $result (@tabFilteredHistory)
    {
        my $code = $result->{'eventCode'};
        if (!defined $tabActionsDates->{$tabEventTypes->{$code}})
        {
            $tabActionsDates->{$tabEventTypes->{$code}} = $result->{'date'};
        }
    }
    return $tabActionsDates;
}


# Function: _getProformasInfos
# Récupérer les informations des pro formas
#
# Parameters:
# string  $countryId                - Pays
# string  $documentType             - Type de document (contrat ou ligne de devis)
# int     $documentId               - Id du document
# bool    $isPendingSpcReducsExists - Y'a-t-il des remises exceptionnelles en attentes ?
#
# Returns:
# hashref - Informations sur les pro formas
sub _getProformasInfos
{
    my ($countryId, $documentType, $documentId, $isPendingSpcReducsExists) = @_;

    my $tabData = {
        'tabTypes'   => [],
        'tabList'    => [],
        'paidAmount' => 0,
        'isActived'  => 0
    };

    # Liste des pro formas associées
    my $tabFilters;
    if ($documentType eq 'contract')
    {
        $tabFilters = {
            'contractId' => $documentId
        };
    }
    elsif ($documentType eq 'estimateLine')
    {
        $tabFilters = {
            'estimateLineId' => $documentId,
            'isFinalized'    => 1,
            'stateId'        => [LOC::Proforma::STATE_ACTIVE, LOC::Proforma::STATE_REPLACED]
        };
    }

    my $tabProformas = &LOC::Proforma::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters,
                                               LOC::Proforma::GETINFOS_CUSTOMER |
                                               LOC::Proforma::GETINFOS_CONTRACT |
                                               LOC::Proforma::GETINFOS_INDEX);

    my $tabStates = {
        LOC::Proforma::STATE_ACTIVE   => 'active',
        LOC::Proforma::STATE_REPLACED => 'replaced',
        LOC::Proforma::STATE_ABORTED  => 'aborted'
    };

    # Somme du déja versé et de l'encaissé de la dernière Pro forma
    my $proformaLastPaidAmount  = 0;
    # Total de la somme du déja versé et de l'encaissé des pro forma de prolongations
    my $extensionPaidAmount = 0;

    foreach my $tabPfmInfos (values %$tabProformas)
    {
        my $tabPfmData = {
            'id'               => $tabPfmInfos->{'id'},
            'date'             => $tabPfmInfos->{'creationDate'},
            'typeId'           => $tabPfmInfos->{'type.id'},
            'typeLabel'        => $tabPfmInfos->{'type.label'},
            'paidAmount'       => $tabPfmInfos->{'paidAmount'},
            'deposit'          => $tabPfmInfos->{'deposit'},
            'paymentModeId'    => $tabPfmInfos->{'paymentMode.id'},
            'paymentModeLabel' => $tabPfmInfos->{'paymentMode.label'},
            'amount'           => $tabPfmInfos->{'amount'},
            'amountWithVat'    => $tabPfmInfos->{'amountWithVat'},
            'total'            => $tabPfmInfos->{'total'},
            'cashed'           => $tabPfmInfos->{'cashed'},
            'remainDue'        => $tabPfmInfos->{'remainDue'},
            'beginDate'        => $tabPfmInfos->{'beginDate'},
            'endDate'          => $tabPfmInfos->{'endDate'},
            'isJustified'      => $tabPfmInfos->{'isJustified'},
            'isValidated'      => $tabPfmInfos->{'isValidated'},
            'validComment'     => $tabPfmInfos->{'validComment'},
            'encashmentId'     => $tabPfmInfos->{'encashment.id'},
            'encashment'       => [$tabPfmInfos->{'encashment.label'}, $tabPfmInfos->{'closure.label'}],
            'isFinalized'      => $tabPfmInfos->{'isFinalized'},
            'fuelQuantity'     => $tabPfmInfos->{'fuelQuantity'},
            'comment'          => $tabPfmInfos->{'comment'},
            'isVatInvoiced'    => $tabPfmInfos->{'isVatInvoiced'},
            'state'            => $tabStates->{$tabPfmInfos->{'state.id'}},
            'isExpired'        => $tabPfmInfos->{'isExpired'},
            'isAbortable'      => $tabPfmInfos->{'isAbortable'},
            'isReactivable'    => $tabPfmInfos->{'isReactivable'},
            'isEditable'       => $tabPfmInfos->{'isEditable'},
            'isPrintable'      => $tabPfmInfos->{'isPrintable'},
            'isPendingSpcReducsExists' => $isPendingSpcReducsExists,
            'modificationDate' => $tabPfmInfos->{'modificationDate'},

            'tabUrls'          => undef
        };

        # Impressions
        my $url = &LOC::Request::createRequestUri('accounting:proforma:pdf', {'proformaId' => $tabPfmInfos->{'id'}});
        $tabPfmData->{'tabUrls'} = {
            'pdf'   => $url,
            'print' => &LOC::Request::createRequestUri('print:gui:print', {
                                                                        'url'         => $url,
                                                                        'tabEndActions' => [
                                                                            {
                                                                                'type'      => 'logPrint',
                                                                                'entity'    => 'PROFORMA',
                                                                                'entityIds' => [$tabPfmInfos->{'id'}]
                                                                            }
                                                                        ]
                                                                    }),
            'mail'  => &LOC::Request::createRequestUri('print:gui:email',
                                                       {
                                                           'url'               => $url,
                                                           'filename'          => 'AI_' . $tabPfmInfos->{'type.label'} .
                                                                                 '_' . $tabPfmInfos->{'contract.code'} .
                                                                                 '_' . $tabPfmInfos->{'indexOnContract'} . '.pdf',
                                                           'documentAgency.id' => $tabPfmInfos->{'agency.id'},
                                                           'defaultRecipient'  => $tabPfmInfos->{'contract.orderContact.email'},
                                                           'defaultSubject'    => $tabPfmInfos->{'type.label'},
                                                           'defaultContent'    => &LOC::Characteristic::getAgencyValueByCode('MAILPROFORMA', $tabPfmInfos->{'contract.agency.id'})
                                                       })
        };

        if ($tabPfmData->{'encashmentId'} eq LOC::Proforma::ENCASHMENTSTATE_RECEIVED ||
            $tabPfmData->{'encashmentId'} eq LOC::Proforma::ENCASHMENTSTATE_PARTIAL)
        {
            # Somme du déja versé et de l'encaissé reçu de la dernière pro forma
            if ($tabPfmData->{'typeId'} == LOC::Proforma::Type::ID_PROFORMA)
            {
                $proformaLastPaidAmount = $tabPfmInfos->{'cashed'} + $tabPfmInfos->{'paidAmount'};
                $extensionPaidAmount = 0;
            }
            # Total de la somme des montants déjà versés et encaissés reçus des pro forma de prolongation
            elsif ($tabPfmData->{'typeId'} == LOC::Proforma::Type::ID_EXTENSION)
            {
                $extensionPaidAmount += $tabPfmInfos->{'cashed'} + $tabPfmInfos->{'paidAmount'};
            }
        }

        # Montant déjà versé final à remonter
        $tabData->{'paidAmount'} = $proformaLastPaidAmount + $extensionPaidAmount;

        # Dépôt de garantie
        $tabData->{'deposit'} = $tabPfmData->{'deposit'};

        # Onglet activé ?
        if ($tabPfmInfos->{'state.id'} eq LOC::Proforma::STATE_ACTIVE)
        {
            $tabData->{'isActived'} = 1;
        }

        push(@{$tabData->{'tabList'}}, $tabPfmData);
    }

    # Types de pro formas autorisés
    $tabData->{'tabTypes'} = &LOC::Proforma::getAuthorizedTypes($countryId, $documentType, $documentId);

    return $tabData;
}

# Function: _getDocumentsInfos
# Récupérer les informations des documents joints
#
# Parameters:
# string  $countryId     - Pays
# string  $contractId    - Identifiant du contrat
#
# Returns:
# hashref - Informations sur les documents
sub _getDocumentsInfos
{
    my ($countryId, $contractId, $estimateId, $estimateLineId) = @_;

    my $tabDocuments = [];

    # - Documents attachés au contrat
    my $tabFilters = {
        'moduleName' => LOC::Attachments::MODULE_RENTCONTRACTDOCUMENT,
        'relatedIds' => [$countryId, $contractId]
    };

    my $tabOptions = {
        'sorters' => {
            'creationDate' => 'desc'
        }
    };

    my $tabDocs = &LOC::Attachments::getList($countryId, $tabFilters, $tabOptions);

    # - Documents attachés à la ligne de devis
    if (defined $estimateId && defined $estimateLineId)
    {
        $tabFilters = {
            'moduleName' => LOC::Attachments::MODULE_RENTESTIMATELINEDOCUMENT,
            'relatedIds' => [$countryId, $estimateLineId]
        };

        my $tabEstimateLineDocs = &LOC::Attachments::getList($countryId, $tabFilters, $tabOptions);
        push(@$tabDocs, @$tabEstimateLineDocs);
    }

    if ($tabDocs)
    {
        my $estimateLineUrl = LOC::Request::createRequestUri(
                'rent:rentEstimate:view',
                {
                    'estimateId' => $estimateId,
                    'estimateLineId' => $estimateLineId,
                    'tabIndex' => 6
                }
            );
        my $tabStates = {
            LOC::Attachments::STATE_ACTIVE  => 'active',
            LOC::Attachments::STATE_DELETED => 'deleted'
        };
        foreach my $tabDocInfos (@$tabDocs)
        {
            my $isExtern = ($tabDocInfos->{'moduleName'} eq LOC::Attachments::MODULE_RENTESTIMATELINEDOCUMENT);

            my $tabDocData = {
                'id'          => 'att:' . $tabDocInfos->{'id'},
                'date'        => $tabDocInfos->{'creationDate'},
                'title'       => $tabDocInfos->{'title'},
                'fileName'    => $tabDocInfos->{'fileName'},
                'size'        => $tabDocInfos->{'size'} * 1,
                'extension'   => lc($tabDocInfos->{'fileExtension'}),
                'description' => $tabDocInfos->{'description'},
                'url'         => &LOC::Request::createRequestUri('rent:rentContract:downloadDocument', {'documentId' => $tabDocInfos->{'id'}}),
                'isEditable'  => !$isExtern,
                'isDeletable' => !$isExtern,
                'isExtern'    => $isExtern,
                'origin'      => ($isExtern ? $estimateLineUrl : undef),
                'moduleName'  => $tabDocInfos->{'moduleName'},
                'stateId'     => $tabStates->{$tabDocInfos->{'stateId'}}
            };

            push(@$tabDocuments, $tabDocData);
        }
    }

    # - États des lieux
    my $tabFilters = {
        'attachedToContractId' => $contractId,
        'stateId'              => [
            LOC::MachineInventory::STATE_ATTACHED,
            LOC::MachineInventory::STATE_NOTATTACHED
        ]
    };
    my $tabMachineInventories = &LOC::MachineInventory::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters,
                                                                LOC::MachineInventory::GETINFOS_BASE |
                                                                LOC::MachineInventory::GETINFOS_CREATOR);
    if ($tabMachineInventories)
    {
        my $tabStates = {
            LOC::MachineInventory::STATE_ATTACHED    => 'attached',
            LOC::MachineInventory::STATE_NOTATTACHED => 'notAttached'
        };
        foreach my $tabMachineInventoryInfos (values %{$tabMachineInventories})
        {
            my $tabMachineInventoryData = {
                'id'           => 'min:' . $tabMachineInventoryInfos->{'id'},
                'type'         => $tabMachineInventoryInfos->{'type'},
                'date'         => $tabMachineInventoryInfos->{'date'},
                'stateId'      => $tabStates->{$tabMachineInventoryInfos->{'state.id'}},
                'url'          => &LOC::Request::createRequestUri('transport:machineInventory:view', {'id' => $tabMachineInventoryInfos->{'id'},
                                                                                                      'from' => 'rentContract'}),
            };
            push(@$tabDocuments, $tabMachineInventoryData);
        }
    }

    # Tri par date
    @$tabDocuments = sort {-($a->{'date'} cmp $b->{'date'})} @$tabDocuments;

    return $tabDocuments;
}

# Function: _getTransportMachineInventoryInfos
# Récupérer le dernier état des lieux d'un transport
#
# Parameters:
#
# Returns:
# hashref - Informations sur l'état des lieux
sub _getTransportMachineInventoryInfos
{
    my ($countryId, $contractId, $transportType) = @_;

    my $tabFilters = {
        'attachedToContractId' => $contractId,
        'stateId'              => [LOC::MachineInventory::STATE_ATTACHED],
        'type'                 => $transportType
    };
    my $tabOptions = {
        'orderBy' => ['date:DESC'],
        'limit'   => 1
    };

    my $tabMachineInventories = &LOC::MachineInventory::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters,
                                                                  LOC::MachineInventory::GETINFOS_BASE |
                                                                  LOC::MachineInventory::GETINFOS_CREATOR,
                                                                  $tabOptions);

    if ($tabMachineInventories && keys(%$tabMachineInventories) > 0)
    {
        my $tabInfos = (values %$tabMachineInventories)[0];
        my $tabMachineInventoryData = {
            'id'      => $tabInfos->{'id'},
            'url'     => &LOC::Request::createRequestUri('transport:machineInventory:view', {'id' => $tabInfos->{'id'},
                                                                                             'from' => 'rentContract'})
        };

        return $tabMachineInventoryData;
    }

    return undef;
}

# Function: _getRepairSheetsList
# Récupérer les informations des fiches de remise en état
#
# Parameters:
# string  $countryId         - Identifiant du pays
# int     $contractId        - Identifiant du contrat
# hashref $tabRepairDays     - Tableau des jours de remise en état
# hashref $tabRepairServices - Tableau des services de remise en état
sub _getRepairSheetsList
{
    my ($countryId, $contractId, $tabRepairDays, $tabRepairServices) = @_;

    my $tabSheetsList = &LOC::Repair::Sheet::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                     {'contractId' => $contractId,
                                                      'stateId'    => [LOC::Repair::Sheet::STATE_WAITING, LOC::Repair::Sheet::STATE_TURNEDIN]
                                                     },
                                                     LOC::Repair::Sheet::GETINFOS_TECHNICIAN |
                                                     LOC::Repair::Sheet::GETINFOS_STATE,
                                                     {'orders' => [
                                                         {'field' => 'id',
                                                          'dir'   => 'DESC'}
                                                     ]});

    my $tabSheets = [];

    foreach my $sheetInfos (values %$tabSheetsList)
    {
        my $tabSheetInfos = {
            'id'            => $sheetInfos->{'id'},
            'date'          => $sheetInfos->{'date'},
            'user.fullName' => $sheetInfos->{'user.fullName'},
            'user.state.id' => $sheetInfos->{'user.state.id'},
            'amount'        => $sheetInfos->{'sheetAmount'},
            'state.id'      => $sheetInfos->{'state.id'},
            'state.label'   => $sheetInfos->{'state.label'},
            'flag'          => $sheetInfos->{'flag'}
        };

        if (defined $sheetInfos->{'flag'})
        {
            if ($sheetInfos->{'flag'} == LOC::Repair::Sheet::FLAG_TURNEDINESTIMATE)
            {
                my $tabEstimateInfos = &LOC::Repair::Estimate::getInfos($countryId,
                                                                        $sheetInfos->{'repairEstimate.id'},
                                                                        LOC::Repair::Estimate::GETINFOS_USER);
                if ($tabEstimateInfos)
                {
                    $tabSheetInfos->{'repairEstimate'} = {
                        'id'            => $tabEstimateInfos->{'id'},
                        'code'          => $tabEstimateInfos->{'code'},
                        'user.fullName' => $tabEstimateInfos->{'user.fullName'},
                        'user.state.id' => $tabEstimateInfos->{'user.state.id'},
                        'amount'        => $tabEstimateInfos->{'total'},
                        'state.id'      => $tabEstimateInfos->{'state.id'},
                        'state.label'   => $tabEstimateInfos->{'state.label'}
                    }
                }
            }
            elsif ($sheetInfos->{'flag'} == LOC::Repair::Sheet::FLAG_TURNEDINSERVICES)
            {
                my @tabServices = grep {$_->{'state.id'} ne LOC::Contract::RepairService::STATE_CANCELED} @{$tabRepairServices->{$sheetInfos->{'id'}}};
                my $nb = @tabServices;

                # Montant total
                my $amount = 0;
                grep { $amount += $_->{'amount'}} @tabServices;

                $tabSheetInfos->{'repairServices'} = {
                    'count'  => $nb,
                    'amount' => $amount
                };
            }
            elsif ($sheetInfos->{'flag'} == LOC::Repair::Sheet::FLAG_TURNEDINDAYS)
            {
                my @tabDays = grep {$_->{'state.id'} ne LOC::Contract::RepairDay::STATE_CANCELED} @{$tabRepairDays->{$sheetInfos->{'id'}}};

                # Montant total
                my $amount = 0;
                grep { $amount += $_->{'amount'}} @tabDays;
                # Nombre de jours
                my $nb = 0;
                grep { $nb += $_->{'number'}} @tabDays;

                $tabSheetInfos->{'repairDays'} = {
                    'count'  => $nb,
                    'amount' => $amount
                };
            }
        }
        push(@$tabSheets, $tabSheetInfos);
    }

    return $tabSheets;
}


# Function: _getContractInfos
# Récupérer les informations d'un contrat
#
# Parameters:
#
# Returns:
# hashref - Informations sur le contrat
sub _getContractInfos
{
    my ($countryId, $tabUserInfos, $contractId, $tabError) = @_;


    # Récupération des informations du contrat
    my $tabContractInfos = &LOC::Contract::Rent::getInfos(
                                                       $countryId,
                                                       $contractId,
                                                       LOC::Contract::Rent::GETINFOS_FUEL |
                                                       LOC::Contract::Rent::GETINFOS_CLEANING |
                                                       LOC::Contract::Rent::GETINFOS_DURATIONS |
                                                       LOC::Contract::Rent::GETINFOS_EXT_WISHEDMACHINE |
                                                       LOC::Contract::Rent::GETINFOS_INSURANCE |
                                                       LOC::Contract::Rent::GETINFOS_APPEAL |
                                                       LOC::Contract::Rent::GETINFOS_EXT_SITE |
                                                       LOC::Contract::Rent::GETINFOS_EXT_TARIFFRENT |
                                                       LOC::Contract::Rent::GETINFOS_EXT_TARIFFTRSP |
                                                       LOC::Contract::Rent::GETINFOS_EXT_REPAIRDAYS |
                                                       LOC::Contract::Rent::GETINFOS_EXT_REPAIRSERVICES |
                                                       LOC::Contract::Rent::GETINFOS_EXT_RENTSERVICES |
                                                       LOC::Contract::Rent::GETINFOS_POSSIBILITIES |
                                                       LOC::Contract::Rent::GETINFOS_PRINTABLE |
                                                       LOC::Contract::Rent::GETINFOS_RESIDUES |
                                                       LOC::Contract::Rent::GETINFOS_NEGOTIATOR |
                                                       LOC::Contract::Rent::GETINFOS_RIGHTS |
                                                       LOC::Contract::Rent::GETINFOS_VALUEDATE,
                                                       {'userId' => $tabUserInfos->{'id'}}
                                                      );
    if (!$tabContractInfos)
    {
        $tabError->{'type'} = 'unknown';
        $tabError->{'data'} = {
            'country.label' => $tabUserInfos->{'nomadCountry.label'}
        };
        return undef;
    }

    my $contractValueDate = $tabContractInfos->{'valueDate'};
    my $contractId        = $tabContractInfos->{'id'};
    my $countryId         = $tabContractInfos->{'country.id'};
    my $agencyId          = $tabContractInfos->{'agency.id'};

    # Informations sur le pays
    my $tabCountryInfos = &LOC::Country::getInfos($countryId);

    # Données pour les factures
    my $tabLinkedContracts = &LOC::Contract::Rent::getLinkedContractsInfos($countryId, $contractId);
    my $tabRentInvoices    = &LOC::Contract::Rent::getRentInvoicesList($countryId, $tabContractInfos->{'code'}, !$tabUserInfos->{'isAdmin'});
    my $tabRepairInvoices  = &LOC::Contract::Rent::getRepairInvoicesList($countryId, $tabContractInfos->{'code'}, !$tabUserInfos->{'isAdmin'});

    # A-t-on des remises exceptionnelles de location ou transport en attentes ?
    my $isPendingSpcReducsExists = ($tabContractInfos->{'rentTariff.tabInfos'}->{'specialReductionStatus'} eq LOC::Tariff::Rent::REDUCTIONSTATE_PENDING ||
                                    $tabContractInfos->{'transportTariff.tabInfos'}->{'specialReductionStatus'} eq LOC::Tariff::Transport::REDUCTIONSTATE_PENDING ? 1 : 0);


    # Informations sur le client
    my $tabCustomerInfos = &LOC::Customer::getInfos($countryId, $tabContractInfos->{'customer.id'},
                                                    LOC::Customer::GETINFOS_TARIFF |
                                                    LOC::Customer::GETINFOS_CRC |
                                                    LOC::Customer::GETINFOS_FLAGS);
    $tabContractInfos->{'customer.tabInfos'} = $tabCustomerInfos;

    # Informations sur le chantier
    my $tabSiteInfos = $tabContractInfos->{'site.tabInfos'};

    # Historique
    my $tabHistory = &_getHistoryInfos($countryId, $contractId, $tabUserInfos->{'locale.id'});

    # Dates d'actions
    my $tabActionsDates = &_getActionsDates($tabHistory);

    my $tabData = {
        'id'                       => $contractId,
        'linkedEstimateLine'       => undef,
        'index'                    => $tabContractInfos->{'index'},
        'code'                     => $tabContractInfos->{'code'},
        'country.id'               => $countryId,
        'agency.id'                => $agencyId,
        'creator.id'               => $tabContractInfos->{'creator.id'},
        'isFullService'            => $tabContractInfos->{'isFullService'},
        'zone'                     => $tabContractInfos->{'zone'},
        'isEndDateNear'            => $tabContractInfos->{'isEndDateNear'},

        'tabCustomer'              => $tabCustomerInfos,

        'tabSite'                  => {
            'id'                 => $tabSiteInfos->{'id'},
            'label'              => $tabSiteInfos->{'label'},
            'address'            => $tabSiteInfos->{'address'},
            'localityId'         => $tabSiteInfos->{'locality.id'},
            'localityLabel'      => $tabSiteInfos->{'locality.label'},
            'zone'               => $tabContractInfos->{'zone'},
            'contactFullName'    => $tabSiteInfos->{'contact.fullName'},
            'contactTelephone'   => $tabSiteInfos->{'contact.telephone'},
            'contactEmail'       => $tabSiteInfos->{'contact.email'},
            'deliveryHour'       => $tabSiteInfos->{'deliveryHour'},
            'deliveryTimeSlotId' => $tabSiteInfos->{'deliveryTimeSlot.id'} || 0,
            'recoveryHour'       => $tabSiteInfos->{'recoveryHour'},
            'recoveryTimeSlotId' => $tabSiteInfos->{'recoveryTimeSlot.id'} || 0,
            'isHasWharf'         => $tabSiteInfos->{'isHasWharf'},
            'isAnticipated'      => $tabSiteInfos->{'isAnticipated'},
            'isAsbestos'         => $tabSiteInfos->{'isAsbestos'},
            'comments'           => $tabSiteInfos->{'comments'}
        },

        'tabMachineInfos'          => &_getMachineInfos($countryId, $tabContractInfos->{'machine.id'}, {}),
        'isFinalAllocation'        => $tabContractInfos->{'isFinalAllocation'},

        'tabWishedMachineInfos'    => undef,

        'tabDuration' => {
            'beginDate'               => $tabContractInfos->{'beginDate'},
            'endDate'                 => $tabContractInfos->{'endDate'},
            'calendarDuration'        => $tabContractInfos->{'calendarDuration'},

            'days'                    => {
                LOC::Day::TYPE_ADDITIONAL() => {
                    'list'                => &_getDaysList($countryId, $tabContractInfos->{'id'}, LOC::Day::TYPE_ADDITIONAL, ($tabContractInfos->{'closureDate'} ? 1 : 0)),
                    'nbUnbilled'          => $tabContractInfos->{'additionalDaysUnbilled'},
                    'nbBilled'            => $tabContractInfos->{'additionalDaysBilled'},
                    'comment'             => $tabContractInfos->{'additionalDaysComment'},
                    'authorizedPeriod'    => &LOC::Day::getAuthorizedPeriod($agencyId, LOC::Day::TYPE_ADDITIONAL, 'contract', [$tabContractInfos->{'beginDate'}, $tabContractInfos->{'endDate'}], $contractValueDate),
                    'external'            => &_getExternalDaysList($countryId, $tabContractInfos->{'id'}, LOC::Day::TYPE_ADDITIONAL),
                    'tabDeltasPeriodDays' =>&LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('INTJRSPCT', $agencyId, $contractValueDate))
                },
                LOC::Day::TYPE_DEDUCTED() => {
                    'list'                => &_getDaysList($countryId, $tabContractInfos->{'id'}, LOC::Day::TYPE_DEDUCTED, ($tabContractInfos->{'closureDate'} ? 1 : 0)),
                    'nbUnbilled'          => $tabContractInfos->{'deductedDaysUnbilled'},
                    'nbBilled'            => $tabContractInfos->{'deductedDaysBilled'},
                    'comment'             => $tabContractInfos->{'deductedDaysComment'},
                    'authorizedPeriod'    => &LOC::Day::getAuthorizedPeriod($agencyId, LOC::Day::TYPE_DEDUCTED, 'contract', [$tabContractInfos->{'beginDate'}, $tabContractInfos->{'endDate'}], $contractValueDate),
                    'external'            => &_getExternalDaysList($countryId, $tabContractInfos->{'id'}, LOC::Day::TYPE_DEDUCTED),
                    'tabDeltasPeriodDays' => &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('INTJRSMCT', $agencyId, $contractValueDate))
                }
            },

            'realDuration'            => $tabContractInfos->{'realDuration'},
            'isRealDurationChanged'   => $tabContractInfos->{'isRealDurationChanged'},
            'isDatesToConfirm'        => 0
        },

        'tabRentTariff' => $tabContractInfos->{'rentTariff.tabInfos'},

        'tabTransport'  => &_getTransportInfos($countryId, $agencyId, 'contract', $tabContractInfos, {}),

        'tabRepair' => {
            'days'     => $tabContractInfos->{'tabRepairDays'},
            'services' => $tabContractInfos->{'tabRepairServices'},
            'sheets'   => &_getRepairSheetsList($countryId, $contractId,
                                                $tabContractInfos->{'tabRepairDays'},
                                                $tabContractInfos->{'tabRepairServices'})
        },

        'tabOtherServices' => {
            'insuranceTypeId'      => $tabContractInfos->{'insuranceType.id'},
            'insuranceRate'        => $tabContractInfos->{'insurance.rate'},
            'appealTypeId'         => $tabContractInfos->{'appealType.id'},
            'appealValue'          => $tabContractInfos->{'appeal.value'},
            'cleaningTypeId'       => $tabContractInfos->{'cleaning.type.id'},
            'cleaningAmount'       => $tabContractInfos->{'cleaning.amount'},
            'residuesMode'         => $tabContractInfos->{'residues.mode'},
            'residuesValue'        => $tabContractInfos->{'residues.value'},
            'fuelQuantity'         => $tabContractInfos->{'fuel.quantity'},
            'fuelUnitPrice'        => ($tabContractInfos->{'fuel.unitPrice'} == 0 ?
                                      $tabCountryInfos->{'fuelUnitPrice'} : $tabContractInfos->{'fuel.unitPrice'}),
            'fuelCurrentUnitPrice' => $tabCountryInfos->{'fuelUnitPrice'},
            'tabFuelEstimation'    => {
                'quantity'  => undef,
                'unitPrice' => 0,
                'amount'    => 0
            }
        },

        'tabProforma'              => &_getProformasInfos($countryId, 'contract', $contractId, $isPendingSpcReducsExists),
        'proformaFlags'            => $tabContractInfos->{'proformaFlags'},

        'tabDocuments'             => undef,

        'tabHistory'               => $tabHistory,

        'isImperativeModel'        => $tabContractInfos->{'isImperativeModel'},
        'tabLinkedContracts'       => $tabLinkedContracts,
        'tabOldServices'           => [],
        'oldServicesAmount'        => 0,
        'tabRentServices'          => [],
        'comments'                 => $tabContractInfos->{'comments'},
        'tracking'                 => $tabContractInfos->{'tracking'},
        'tabRentInvoice'           => $tabRentInvoices,
        'tabRepairInvoice'         => $tabRepairInvoices,
        'creationDate'             => $tabContractInfos->{'creationDate'},
        'modificationDate'         => $tabContractInfos->{'modificationDate'},
        'closureDate'              => $tabContractInfos->{'closureDate'},
        'valueDate'                => $contractValueDate,
        'isLocked'                 => (($tabCustomerInfos->{'lockLevel'} >= 1 && !$tabCustomerInfos->{'isTempUnlock'}) ||
                                       $tabContractInfos->{'isLocked'} ? 1 : 0),
        'state.id'                 => $tabContractInfos->{'state.id'},
        'negotiator.id'            => $tabContractInfos->{'negotiator.id'},
        'negotiator.fullName'      => $tabContractInfos->{'negotiator.fullName'},
        'isJoinDoc'                => $tabContractInfos->{'isJoinDoc'},
        'isPrintable'              => $tabContractInfos->{'isPrintable'},

        'invoiceState'             => $tabContractInfos->{'invoiceState'},
        'lastInvoiceDate'          => $tabContractInfos->{'lastInvoiceDate'},

        'tabValorizationInfos'     => undef,

        'actionsDates'             => $tabActionsDates,

        'possibilities'            => $tabContractInfos->{'possibilities'},
        'tabRights'                => $tabContractInfos->{'tabRights'}
    };

    # Récupération des informations sur la machine souhaitée
    my $tabModelOptions = {
        'getWishedMachines' => 1
    };
    if ($tabContractInfos->{'wishedMachine.id'})
    {
        $tabData->{'tabWishedMachineInfos'} = {
            'id'         => $tabContractInfos->{'wishedMachine.id'},
            'parkNumber' => $tabContractInfos->{'wishedMachine.tabInfos'}->{'parkNumber'}
        };

        $tabModelOptions->{'getWishedMachines'} = {
            'add' => $tabContractInfos->{'wishedMachine.tabInfos'}
        };
    }

    # Récupération des informations sur le modèle de machine
    $tabData->{'tabModelInfos'} = &_getModelInfos($countryId,
                                                  $tabContractInfos->{'model.id'},
                                                  $tabContractInfos->{'customer.id'},
                                                  $agencyId,
                                                  $tabModelOptions);

    # Déclaration de panne
    if ($tabData->{'tabMachineInfos'}->{'parkNumber'})
    {
        # Récupération des configurations pour l'envoi par mail
        # - Corps du mail
        my $tabMailCfgs = &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('MAILDECLPANNE', $agencyId));

        # Remplacement des variables
        $tabMailCfgs->{'subject'}  =~ s/<%agency.code>/$agencyId/;
        $tabMailCfgs->{'subject'}  =~ s/<%machine.parkNumber>/$tabData->{'tabMachineInfos'}->{'parkNumber'}/;

        if ($tabData->{'tabSite'}->{'isAsbestos'})
        {
            $tabMailCfgs->{'subject'}  =~ s/<%site\.isAsbestos>(.*)<\/%site\.isAsbestos>/$1/s;
            $tabMailCfgs->{'content'}  =~ s/<%site\.isAsbestos>(.*)<\/%site\.isAsbestos>/$1/s;
        }
        else
        {
            $tabMailCfgs->{'subject'}  =~ s/<%site\.isAsbestos>(.*)<\/%site\.isAsbestos>//s;
            $tabMailCfgs->{'content'}  =~ s/<%site\.isAsbestos>(.*)<\/%site\.isAsbestos>//s;
        }

        $tabMailCfgs->{'content'}  =~ s/<%machine.parkNumber>/$tabData->{'tabMachineInfos'}->{'parkNumber'}/;
        $tabMailCfgs->{'content'}  =~ s/<%site.contactName>/$tabData->{'tabSite'}->{'contactFullName'}/;
        $tabMailCfgs->{'content'}  =~ s/<%site.contactTel>/$tabData->{'tabSite'}->{'contactTelephone'}/;
        $tabMailCfgs->{'content'}  =~ s/<%customer.name>/$tabData->{'tabCustomer'}->{'name'}/;
        $tabMailCfgs->{'content'}  =~ s/<%customer.code>/$tabData->{'tabCustomer'}->{'code'}/;
        $tabMailCfgs->{'content'}  =~ s/<%model.name>/$tabData->{'tabModelInfos'}->{'label'}/;
        $tabMailCfgs->{'content'}  =~ s/<%site.label>/$tabData->{'tabSite'}->{'label'}/;
        $tabMailCfgs->{'content'}  =~ s/<%site.address>/$tabData->{'tabSite'}->{'address'}/;
        $tabMailCfgs->{'content'}  =~ s/<%site.locality.label>/$tabData->{'tabSite'}->{'localityLabel'}/;

        # - Destinataires du mail
        my $tabMailDests = &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('DESTDECLPANNE', $agencyId));
        # Remplacement des variables
        my $tabAgencyInfos = &LOC::Agency::getInfos($agencyId);
        $tabMailDests->{'cc'}  =~ s/<%mail.shop>/$tabAgencyInfos->{'email.shop'}/;
        my $areaTechManagerMail = '';
        if ($tabAgencyInfos->{'area.id'} && $tabAgencyInfos->{'area.techManager.id'})
        {
            my $tabAreaInfos   = &LOC::Area::getInfos($tabAgencyInfos->{'area.id'});
            $areaTechManagerMail = ($tabAreaInfos->{'techManager.email'} ? $tabAreaInfos->{'techManager.email'} : '');
        }

        $tabMailDests->{'cc'}  =~ s/<%areatechmanager.email>/$areaTechManagerMail/;

        my $schemaName = &LOC::Db::getSchemaName($countryId);

        my $content = '';
        if ($tabData->{'tabSite'}->{'contactFullName'} ne '' || $tabData->{'tabSite'}->{'contactTelephone'} ne '')
        {
            $content = $tabData->{'tabSite'}->{'contactFullName'} . ' - ' . $tabData->{'tabSite'}->{'contactTelephone'};
        }
        my $tabContentExtra = [
            {
                'prop'  => 'to',
                'value' => '<%to>'
            },
            {
                'prop'  => 'cc',
                'value' => '<%cc>'
            },
            {
                'prop'  => 'subject',
                'value' => '<%subject>'
            },
            {
                'prop'  => 'message',
                'value' => '<%message>'
            }
        ];
        $tabData->{'tabMachineInfos'}->{'breakdownDeclarationInfos'} = {
            'defaultRecipient'     => $tabMailDests->{'to'},
            'defaultCopyRecipient' => $tabMailDests->{'cc'},
            'defaultSubject'       => $tabMailCfgs->{'subject'},
            'defaultContent'       => $tabMailCfgs->{'content'},
            'documentAgency.id'    => $agencyId,
            'tabReplacesTpl'       => {
                'machine.url' => {
                    'type'  => 'link',
                    'url'   => &LOC::Request::createRequestUri('machine:machine:view', {'machineId' => $tabData->{'tabMachineInfos'}->{'id'}}),
                    'value' => $tabData->{'tabMachineInfos'}->{'parkNumber'}
                }
            },
            'tabEndActions'     => [
                {
                    'type' => 'addTrace',
                    'functionality'   => 'CONTRAT',
                    'functionalityId' => $tabContractInfos->{'id'},
                    'tabContent'      => {
                        'schema'       => $schemaName,
                        'code'         => LOC::Log::EventType::SENDMAILBREAKDOWNDECLARATION,
                        'old'          => undef,
                        'new'          => undef,
                        'content'      => $content,
                        'contentExtra' => $tabContentExtra
                    }
                }
            ]
       };
    }

    # Ligne de devis associée
    if ($tabContractInfos->{'linkedEstimateLine.id'})
    {
        # Récupération des informations du devis associé
        my $tabEstimateLineInfos = &LOC::Estimate::Rent::getLineInfos($countryId,
                                                                      $tabContractInfos->{'linkedEstimateLine.id'},
                                                                      LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE |
                                                                      LOC::Estimate::Rent::GETLINEINFOS_INDEX);

        $tabData->{'linkedEstimateLine'} = {
            'id'           => $tabEstimateLineInfos->{'id'},
            'index'        => $tabEstimateLineInfos->{'index'},
            'estimateId'   => $tabEstimateLineInfos->{'estimate.id'},
            'estimateCode' => $tabEstimateLineInfos->{'estimate.code'}
        };
    }

    # Liste des services de location et transport
    foreach my $tabRentSrvInfos (values %{$tabContractInfos->{'tabRentServices'}})
    {
        push(@{$tabData->{'tabRentServices'}}, {
            'id'               => $tabRentSrvInfos->{'id'},
            'amount'           => $tabRentSrvInfos->{'amount'},
            'comment'          => $tabRentSrvInfos->{'comment'},
            'rentService.id'   => $tabRentSrvInfos->{'rentService.id'},
            'rentService.name' => $tabRentSrvInfos->{'rentService.name'},
            'state.id'         => $tabRentSrvInfos->{'state.id'},
            'state.label'      => $tabRentSrvInfos->{'state.label'},
            'isRemovable'      => $tabRentSrvInfos->{'isRemovable'}
        });
    }
    # Liste des anciens services de location et transport
    my $tabOldServices = &LOC::Contract::Rent::getOldServices($countryId, $contractId);
    foreach my $tabRentSrvInfos (values %$tabOldServices)
    {
        push(@{$tabData->{'tabOldServices'}}, {
            'label'      => $tabRentSrvInfos->{'label'},
            'amount'     => $tabRentSrvInfos->{'amount'},
            'stateLabel' => $tabRentSrvInfos->{'state.label'}
        });
        if ($tabRentSrvInfos->{'state.id'} ne LOC::Contract::Rent::SERVICESTATE_DELETED)
        {
            $tabData->{'oldServicesAmount'} += $tabRentSrvInfos->{'amount'};
        }
    }

    # Récupérer l'estimation carburant
    my $fuelEstimationInfos = &LOC::Proforma::getFuelEstimationInfos($countryId, 'contract', $contractId);
    if ($fuelEstimationInfos)
    {
        $tabData->{'tabOtherServices'}->{'tabFuelEstimation'} = $fuelEstimationInfos;
    }

    # Impression du bon de valorisation
    if (!$isPendingSpcReducsExists &&
        $tabContractInfos->{'rentTariff.tabInfos'}->{'amountType'} eq 'day') # Le bon de livraison n'est pas disponible si c'est un tarif mensuel (DE-11132)
    {
        my $tabMailInfos = &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('MAILCONVALORIZAT', $agencyId));
        my $url = &LOC::Request::createRequestUri('accounting:proforma:valorizationPdf',
                                                  {'documentType' => 'contract',
                                                   'documentId' => $tabContractInfos->{'id'}});
        my $subject = $tabMailInfos->{'subject'};
        $subject =~ s/<%code>/$tabContractInfos->{'code'}/;

        $tabData->{'tabValorizationInfos'} = {
            'url'         => $url,
            'mailSubject' => $subject,
            'mailContent' => $tabMailInfos->{'content'}
        };
    }

    # Liste des documents
    $tabData->{'tabDocuments'} = &_getDocumentsInfos(
            $countryId,
            $contractId,
            ($tabData->{'linkedEstimateLine'} ? $tabData->{'linkedEstimateLine'}->{'estimateId'} : undef),
            ($tabData->{'linkedEstimateLine'} ? $tabData->{'linkedEstimateLine'}->{'id'}: undef)
        );

    # TODO: Ne prendre que les informations nécessaires pour le client

    $tabData->{'tabCustomer'}->{'country.id'}             = $countryId;
    $tabData->{'tabCustomer'}->{'orderNo'}                = $tabContractInfos->{'orderNo'};
    $tabData->{'tabCustomer'}->{'orderContact.fullName'}  = $tabContractInfos->{'orderContact.fullName'};
    $tabData->{'tabCustomer'}->{'orderContact.telephone'} = $tabContractInfos->{'orderContact.telephone'};
    $tabData->{'tabCustomer'}->{'orderContact.email'}     = $tabContractInfos->{'orderContact.email'};


    return $tabData;
}


# Function: _getNewContractInfos
# Récupérer les informations d'un nouveau contrat
#
# Parameters:
#
# Returns:
# hashref - Informations sur le contrat
sub _getNewContractInfos
{
    my ($countryId, $tabUserInfos, $agencyId, $tabError) = @_;

    # Informations sur le pays
    my $tabCountryInfos = &LOC::Country::getInfos($countryId);

    # Libellé de l'agence du contrat
    my $agencyLabel = &LOC::Agency::getLabelById($agencyId);


    my $tabData = {
        'id'                       => undef,
        'linkedEstimateLine'       => undef,
        'index'                    => 0,
        'code'                     => &LOC::Contract::Rent::generateCode('******', $agencyId, 0, 0),
        'country.id'               => $countryId,
        'agency.id'                => $agencyId,
        'creator.id'               => $tabUserInfos->{'id'},
        'isFullService'            => 0,
        'zone'                     => 0,
        'isEndDateNear'            => 0,

        'tabCustomer'              => {
            'id'         => 0,
            'name'       => '',
            'country.id' => $countryId,
            'orderNo'    => undef,
            'orderContact.fullName'  => undef,
            'orderContact.telephone' => undef,
            'orderContact.email'     => undef
        },

        'tabSite'                  => {
            'id'                 => undef,
            'label'              => '',
            'address'            => '',
            'localityId'         => undef,
            'localityLabel'      => '',
            'zone'               => undef,
            'contactFullName'    => '',
            'contactTelephone'   => '',
            'contactEmail'       => '',
            'deliveryHour'       => '',
            'deliveryTimeSlotId' => 0,
            'recoveryHour'       => '',
            'recoveryTimeSlotId' => 0,
            'isHasWharf'         => 0,
            'isAnticipated'      => 0,
            'isAsbestos'         => 0,
            'comments'           => ''
        },

        'tabMachineInfos'          => {
            'id' => 0
        },
        'isFinalAllocation'        => 0,

        'tabWishedMachineInfos'    => undef,

        'tabModelInfos'            => {
            'id'               => 0,
            'tabTariffs'       => {},
            'tabCleaningTypes' => [{'id' => -1, 'label' => 'std', 'amount' => 0, 'isStandard' => 1}]
        },

        'tabDuration' => {
            'beginDate'               => '',
            'endDate'                 => '',
            'calendarDuration'        => 0,

            'days'                    => {
                LOC::Day::TYPE_ADDITIONAL() => {
                    'list'                => [],
                    'nbUnbilled'          => 0,
                    'nbBilled'            => 0,
                    'comment'             => undef,
                    'authorizedPeriod'    => [],
                    'tabDeltasPeriodDays' => &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('INTJRSPCT', $agencyId))
                },
                LOC::Day::TYPE_DEDUCTED() => {
                    'list'                => [],
                    'nbUnbilled'          => 0,
                    'nbBilled'            => 0,
                    'comment'             => undef,
                    'authorizedPeriod'    => [],
                    'tabDeltasPeriodDays' => &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('INTJRSMCT', $agencyId))
                }
            },

            'realDuration'            => 0,
            'isRealDurationChanged'   => 0,
            'isDatesToConfirm'        => 0
        },

        'tabRentTariff'            => {
            'amountType'                    => 'day',
            'baseAmount'                    => 0,
            'maxiReductionPercent'          => 0,
            'standardReductionPercent'      => 0,
            'specialReductionPercent'       => 0,
            'specialReductionJustification' => '',
            'specialReductionBillLabel'     => '',
            'specialReductionStatus'        => '',
            'isKeepAmount'                  => 0
        },

        'tabTransport' => {
            'tabTariff' => {
                'id'            => 0,
                'possibilities' => {
                    'estimation' => 1,
                    'delivery' => {
                        'amount' => 1,
                        'self' => 1,
                        'addMachine' => 1
                    },
                    'recovery' => {
                        'amount' => 1,
                        'self' => 1,
                        'addMachine' => 1
                    }
                }
            },
            'tabDelivery' => {
                'amount'             => 0,
                'isAddMachine'       => 0,
                'addMachineLnkCtt'   => undef,
                'isSelf'             => 0,
                'agency'             => {
                    'id'    => $agencyId,
                    'label' => $agencyLabel
                },
                'delegatedAgencyId'  => undef,
                'transferFromAgency' => undef,
                'contract'           => undef,
                'isLoadingDone'      => 0,
                'isUnloadingDone'    => 0,
                'extraInfos'         => {},
                'tabErrors'          => [],
                'action'             => undef,
                'selfFormUrls'       => undef,
                'isSynchroDisabled'  => 0,
                'possibilities'      => {
                    'action' => undef,
                    'edit'   => 1
                }
            },
            'tabRecovery' => {
                'amount'            => 0,
                'isAddMachine'      => 0,
                'addMachineLnkCtt'  => undef,
                'isSelf'            => 0,
                'agency'            => {
                    'id'    => $agencyId,
                    'label' => $agencyLabel
                },
                'delegatedAgencyId' => undef,
                'transferToAgency'  => undef,
                'contract'          => undef,
                'isLoadingDone'     => 0,
                'isUnloadingDone'   => 0,
                'extraInfos'        => {},
                'tabErrors'         => [],
                'action'            => undef,
                'selfFormUrls'      => undef,
                'isSynchroDisabled' => 0,
                'possibilities'     => {
                    'action'           => undef,
                    'edit'             => 1
                }
            }
        },

        'tabRepair' => {
            'days'     => {},
            'services' => {},
            'sheets'   => []
        },

        'tabOtherServices' => {
            'insuranceTypeId'      => undef,
            'insuranceRate'        => undef,
            'appealTypeId'         => undef,
            'appealValue'          => undef,
            'cleaningTypeId'       => 0,
            'cleaningAmount'       => 0,
            'residuesMode'         => undef,
            'residuesValue'        => undef,
            'fuelQuantity'         => undef,
            'fuelUnitPrice'        => $tabCountryInfos->{'fuelUnitPrice'},
            'fuelCurrentUnitPrice' => $tabCountryInfos->{'fuelUnitPrice'},
            'tabFuelEstimation'    => {
                'quantity'  => undef,
                'unitPrice' => 0,
                'amount'    => 0
            }
        },

        'tabProforma' => {
            'tabTypes'   => [],
            'tabList'    => [],
            'paidAmount' => 0,
            'isActived'  => 0
        },
        'proformaFlags' => 0,

        'tabDocuments' => [],

        'tabHistory'               => [],

        'isImperativeModel'        => 0,
        'tabLinkedContracts'       => {},
        'tabOldServices'           => [],
        'oldServicesAmount'        => 0,
        'tabRentServices'          => [],
        'comments'                 => '',
        'tracking'                 => '',
        'tabRentInvoice'           => {},
        'tabRepairInvoice'         => {},
        'creationDate'             => undef,
        'modificationDate'         => undef,
        'closureDate'              => undef,
        'valueDate'                => &LOC::Date::getMySQLDate(),
        'isLocked'                 => 0,
        'state.id'                 => LOC::Contract::Rent::STATE_PRECONTRACT,
        'negotiator.id'            => undef,
        'negotiator.fullName'      => '',
        'isJoinDoc'                => 0,
        'isPrintable'              => 0,

        'invoiceState'             => '',
        'lastInvoiceDate'          => '',

        'tabValorizationInfos'     => undef,

        'actionsDates'             => {},

        'possibilities'            => {
            'equipment'  => {
                'model'      => 1
            },
            'actions'    => {
                'valid'      => 1,
                'stop'       => 0,
                'reactivate' => 0,
                'invoice'    => 0,
                'restop'     => 0,
                'cancel'     => 0
            },
            'duration' => {
                'setInvoiceableDay'    => 1,
                'setNonInvoiceableDay' => 1
            }
        },
        'tabRights'                => &LOC::Contract::Rent::getRights($countryId, $agencyId, undef, $tabUserInfos->{'id'})
    };

    # Types de pro formas autorisés
    $tabData->{'tabProforma'}->{'tabTypes'} = &LOC::Proforma::getAuthorizedTypes($countryId, 'contract', 0);

    return $tabData;
}


1;
