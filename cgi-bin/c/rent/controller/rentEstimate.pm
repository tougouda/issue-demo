use utf8;
use open (':encoding(UTF-8)');

# Package: rentEstimate
# Exemple de contrôleur
package rentEstimate;

# Constants: Paramètres d'affichage
# NBSEARCHSTEP - Nombre de devis à afficher dans une recherche et lors du clic sur "Voir plus"
use constant
{
    NBSEARCHSTEP        => 100
};

use strict;
use File::Basename;
use File::Copy;

use LOC::Agency;
use LOC::Country;
use LOC::Customer;
use LOC::Estimate;
use LOC::Estimate::Rent;
use LOC::Tariff::Rent;
use LOC::Model;
use LOC::Machine;
use LOC::TariffFamily;
use LOC::User;
use LOC::Site;
use LOC::Characteristic;
use LOC::Transport::TimeSlot;
use LOC::Cleaning;
use LOC::Proforma;
use LOC::Date;
use LOC::Day;
use LOC::Day::InvoiceDuration;
use LOC::Day::Reason;
use LOC::Json;
use LOC::Attachments;
use LOC::Common::RentService;
use LOC::Insurance::Type;
use LOC::Appeal::Type;


# Function: viewAction
# Affichage du devis de location
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.id'        => $tabUserInfos->{'id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'timeZone'       => $tabSession->{'timeZone'}
    );
    our %tabSearchData = ();


    my $countryId      = $tabUserInfos->{'nomadCountry.id'};
    my $agencyId       = $tabUserInfos->{'nomadAgency.id'};
    my $userId         = $tabUserInfos->{'id'};

    my $estimateId     = &LOC::Request::getInteger('estimateId', undef);
    my $estimateLineId = &LOC::Request::getInteger('estimateLineId', undef);

    my $editMode = (defined $estimateId && $estimateId != 0 ? 'update' : 'create');


    # Données du formulaire de la barre de recherche
    $tabSearchData{'country.id'} = $countryId;

    # Données de la barre de recherche
    $tabSearchData{'states'} = &LOC::Estimate::Rent::getStates($countryId);
    my $tabTariffFamilies = {};
    tie(%$tabTariffFamilies, 'Tie::IxHash');
    $tabTariffFamilies = &LOC::TariffFamily::getList($countryId, 
                                               LOC::Util::GETLIST_PAIRS,
                                               {},
                                               {}, 
                                               {'sort' => [ {'field' => 'fulllabel', 'dir' => 'ASC'} ]}
                                               );
    $tabSearchData{'tariffFamilies'} = $tabTariffFamilies;

    $tabViewData{'documentType'} = 'estimate';
    $tabViewData{'editMode'}     = $editMode;
    $tabViewData{'tempId'}       = 'TMP' . time() . '_' . $agencyId . '_' . $userId;
    # tarification transport dans le pays?

    my $estimateCountryId;
    my $estimateAgencyId;
    my $estimateCreatorId;
    my $estimateValueDate;

    if ($editMode eq 'update')
    {
        ############################
        # Mise à jour d'un devis
        ############################

        # Récupération des informations du devis
        my $tabEstimateInfos = &_getEstimateInfos($countryId, $tabUserInfos, $estimateId, {});

        $estimateCountryId = $tabEstimateInfos->{'country.id'};
        $estimateAgencyId  = $tabEstimateInfos->{'agency.id'};
        $estimateCreatorId = $tabEstimateInfos->{'creator.id'};
        $estimateValueDate = $tabEstimateInfos->{'valueDate'};

        # Récupération des informations "légères" sur toutes les lignes du devis
        my $tabEstimateLinesInfos = &LOC::Estimate::Rent::getLinesList(
                                                                    $countryId,
                                                                    LOC::Util::GETLIST_ASSOC,
                                                                    {'estimateId' => $estimateId},
                                                                    LOC::Estimate::Rent::GETLINEINFOS_MODEL |
                                                                    LOC::Estimate::Rent::GETLINEINFOS_DURATIONS |
                                                                    LOC::Estimate::Rent::GETLINEINFOS_INSURANCE |
                                                                    LOC::Estimate::Rent::GETLINEINFOS_APPEAL |
                                                                    LOC::Estimate::Rent::GETLINEINFOS_CLEANING |
                                                                    LOC::Estimate::Rent::GETLINEINFOS_FUEL |
                                                                    LOC::Estimate::Rent::GETLINEINFOS_RESIDUES |
                                                                    LOC::Estimate::Rent::GETLINEINFOS_TARIFFRENT |
                                                                    LOC::Estimate::Rent::GETLINEINFOS_TARIFFTRSP |
                                                                    LOC::Estimate::Rent::GETLINEINFOS_RIGHTS |
                                                                    LOC::Estimate::Rent::GETLINEINFOS_LINKEDCONTRACT
                                                                   );

        # Si la ligne sélectionnée n'existe pas alors on sélectionne la première de la liste
        if (!$estimateLineId || !exists $tabEstimateLinesInfos->{$estimateLineId})
        {
            # Sélection de la première ligne non désactivée
            my $nbLines = @{$tabEstimateInfos->{'tabLines'}};
            my $i = 0;
            while ($i < $nbLines && ($tabEstimateLinesInfos->{$tabEstimateInfos->{'tabLines'}->[$i]}->{'state.id'} eq LOC::Estimate::Rent::LINE_STATE_INACTIVE))
            {
                $i++;
            }
            $estimateLineId = $tabEstimateInfos->{'tabLines'}->[($i < $nbLines ? $i : 0)];
        }


        # Données pour le bloc d'informations générales
        $tabViewData{'tabDocumentInfos'} = {
            'agency.id'        => $tabEstimateInfos->{'agency.id'},
            'isLocked'         => $tabEstimateInfos->{'isLocked'},
            'creator.id'       => $tabEstimateInfos->{'creator.id'},
            'creationDate'     => $tabEstimateInfos->{'creationDate'}
        };

        # Devis
        $tabViewData{'tabEstimateInfos'}      = $tabEstimateInfos;
        $tabViewData{'tabEstimateLinesInfos'} = $tabEstimateLinesInfos;
        $tabViewData{'estimateLineId'}        = $estimateLineId;

        # Motifs d'abandons
        $tabViewData{'tabAbortReasons'} = &LOC::Estimate::getAbortList($estimateCountryId, LOC::Util::GETLIST_ASSOC);
    }
    elsif ($editMode eq 'create')
    {
        ############################
        # Création d'un devis
        ############################

        # Récupération des informations du devis
        my $tabEstimateInfos = &_getNewEstimateInfos($countryId, $tabUserInfos, $agencyId, {});

        $estimateCountryId = $tabEstimateInfos->{'country.id'};
        $estimateAgencyId  = $tabEstimateInfos->{'agency.id'};
        $estimateCreatorId = $tabEstimateInfos->{'creator.id'};
        $estimateValueDate = $tabEstimateInfos->{'valueDate'};


        # Données pour le bloc d'informations générales
        $tabViewData{'tabDocumentInfos'} = {
            'agency.id'        => $tabEstimateInfos->{'agency.id'},
            'isLocked'         => $tabEstimateInfos->{'isLocked'},
            'creator.id'       => $tabEstimateInfos->{'creator.id'},
            'creationDate'     => $tabEstimateInfos->{'creationDate'}
        };

        # Devis (structure identique à &_getEstimateInfos())
        $tabViewData{'tabEstimateInfos'} = $tabEstimateInfos;
        $tabViewData{'tabEstimateLinesInfos'} = {};
        $tabViewData{'estimateLineId'} = undef;

        $estimateLineId = undef;
    }


    # Informations sur le pays
    my $tabCountryInfos = &LOC::Country::getInfos($estimateCountryId);

    # Données par défaut d'une nouvelle ligne de devis
    $tabViewData{'tabNewLineInfos'} = &_getNewEstimateLineInfos($estimateCountryId, $tabUserInfos, $estimateAgencyId, {});

    # La tarification transport est-elle activée ?
    $tabViewData{'isTrspTarification'} = &LOC::Characteristic::getCountryValueByCode('TRSPTARIF', $estimateCountryId) * 1;

    # Liste des plages horaires de transport
    $tabViewData{'tabTransportTimeSlots'} = &LOC::Transport::TimeSlot::getList($estimateCountryId, LOC::Util::GETLIST_ASSOC,
                                                                               {'stateId' => [LOC::Transport::TimeSlot::STATE_ACTIVE,
                                                                                              LOC::Transport::TimeSlot::STATE_DELETED]});

    # Liste des modèles de machines
    $tabViewData{'tabModelsList'} = &LOC::Model::getList($estimateCountryId, LOC::Util::GETLIST_ASSOC);
    # Liste des agences
    $tabViewData{'tabDocumentInfos'}->{'tabAgencies'} = &LOC::Agency::getList(
                                                                LOC::Util::GETLIST_PAIRS,
                                                                {'country' => $estimateCountryId,
                                                                 'type' => LOC::Agency::TYPE_AGENCY});
    # Liste des utilisateurs
    $tabViewData{'tabDocumentInfos'}->{'tabUsers'} = &LOC::User::getList(
                                                                LOC::Util::GETLIST_PAIRS,
                                                                {'id' => $estimateCreatorId},
                                                                LOC::User::GET_ALL,
                                                                {'formatMode' => 1});

    # Liste des types d'assurance
    my $tabInsuranceTypesList = &LOC::Insurance::Type::getList($estimateCountryId, LOC::Util::GETLIST_ASSOC,
                                                                    {'stateId' => [LOC::Insurance::Type::STATE_ACTIVE, LOC::Insurance::Type::STATE_DELETED]});
    my @tabInsuranceTypes = values(%$tabInsuranceTypesList);
    $tabViewData{'insurance.types'} = \@tabInsuranceTypes;

    # Liste des taux d'assurance
    $tabViewData{'insurance.rates'} = $tabCountryInfos->{'insurance.rates'};

    # Liste des types de gestion de recours
    my $tabAppealTypesList = &LOC::Appeal::Type::getList($estimateCountryId, LOC::Util::GETLIST_ASSOC,
                                                                    {'stateId' => [LOC::Appeal::Type::STATE_ACTIVE, LOC::Appeal::Type::STATE_DELETED]});
    my @tabAppealTypes = values(%$tabAppealTypesList);
    $tabViewData{'appeal.types'} = \@tabAppealTypes;

    # liste des services location et transport
    $tabViewData{'tabRentServiceTypes'} = &LOC::Common::RentService::getList($estimateCountryId, LOC::Util::GETLIST_ASSOC);
    my $rentServiceMinAmount = &LOC::Characteristic::getAgencyValueByCode('MTMINSRV', $estimateAgencyId);
    my $rentServiceNoLimitUserIdList = &LOC::Characteristic::getAgencyValueByCode('USRNOMAXREMSRV', $estimateAgencyId);
    my @rentServiceNoLimitUserId = split(',', $rentServiceNoLimitUserIdList);
    # si l'utilisateur courant n'a pas de limite de services location et transport
    if (grep {$_ eq $userId} @rentServiceNoLimitUserId)
    {
        $rentServiceMinAmount = undef;
    }

    # Autres paramètres
    $tabViewData{'tariffTrspEstimationUrl'} = &LOC::Globals::get('tariffTrspEstimationUrl');
    $tabViewData{'addMachinePrice'}         = &LOC::Characteristic::getAgencyValueByCode('ADDMACH', $estimateAgencyId) * 1;
    $tabViewData{'addMachineMinPrice'}      = &LOC::Characteristic::getAgencyValueByCode(
                                                  'ADDMACHMIN', $estimateAgencyId, $estimateValueDate) * 1;
    $tabViewData{'defaultStdReducMode'}     = &LOC::Characteristic::getAgencyValueByCode('LOCREMSTDMODE', $estimateAgencyId) * 1;
    $tabViewData{'useResiduesForAccessories'} = &LOC::Characteristic::getAgencyValueByCode(
                                                  'ACCDECHETS', $estimateAgencyId, $estimateValueDate) * 1;
    $tabViewData{'defaultAppealMode'}       = &LOC::Characteristic::getAgencyValueByCode(
                                                  'RECOURSMODE',$estimateAgencyId, $estimateValueDate) * 1;
    $tabViewData{'defaultAppealRate'}       = &LOC::Characteristic::getAgencyValueByCode(
                                                  'PCRECOURS', $estimateAgencyId, $estimateValueDate) * 1;
    $tabViewData{'defaultAppealTypeId'}     = &LOC::Appeal::Type::getDefaultId($estimateCountryId);
    $tabViewData{'defaultResiduesMode'}     = &LOC::Characteristic::getAgencyValueByCode(
                                                  'DECHETSMODE',$estimateAgencyId, $estimateValueDate) * 1;
    $tabViewData{'defaultResiduesAmount'}   = &LOC::Characteristic::getAgencyValueByCode(
                                                  'MTDECHETS', $estimateAgencyId, $estimateValueDate) * 1;
    $tabViewData{'defaultResiduesRate'}     = &LOC::Characteristic::getAgencyValueByCode(
                                                  'PCDECHETS', $estimateAgencyId, $estimateValueDate) * 1;
    $tabViewData{'defaultInsuranceRate'}    = $tabCountryInfos->{'insurance.defaultRate'};
    $tabViewData{'defaultInsuranceTypeId'}  = &LOC::Insurance::Type::getDefaultId($estimateCountryId);
    $tabViewData{'fuelUnitPrice'}           = $tabCountryInfos->{'fuelUnitPrice'};

    $tabViewData{'nbLinesMax'}              = LOC::Estimate::Rent::MAXLINESCOUNT;
    $tabViewData{'rentServiceMinAmount'}    = $rentServiceMinAmount;


    $tabViewData{'customersSearchUrl'} = &LOC::Request::createRequestUri('accounting:customer:jsonList',
                                                                         {'countryId'    => $estimateCountryId,
                                                                          'isRestricted' => 1,
                                                                          'format'       => 0});
    $tabViewData{'siteLocalitiesSearchUrl'} = &LOC::Request::createRequestUri('site:site:siteLocalitiesList',
                                                                              {'country.id'      => $estimateCountryId,
                                                                               'transportAgency' => $estimateAgencyId,
                                                                               'format'          => 0});

    $tabViewData{'tabProformaTypes'} = &LOC::Proforma::Type::getList($estimateCountryId, LOC::Util::GETLIST_PAIRS);
    $tabViewData{'tabProformaPaymentModes'} = &LOC::Proforma::getPaymentModesList($estimateCountryId, LOC::Util::GETLIST_PAIRS);
    $tabViewData{'proformaAdvancePercent'} = &LOC::Characteristic::getCountryValueByCode('PERCACPTE', $estimateCountryId);

    # - Liste des motifs pours les jours +/-
    my $tabDayReasonsList = &LOC::Day::Reason::getList($estimateCountryId, LOC::Util::GETLIST_ASSOC);
    $tabViewData{'tabDayReasonsList'} = &LOC::Util::getHashValues($tabDayReasonsList);
    # - Liste des durées possibles poru les jours +/-
    my $tabDayInvoiceDurationsList = &LOC::Day::InvoiceDuration::getList($estimateCountryId, LOC::Util::GETLIST_ASSOC);
    $tabViewData{'tabDayInvoiceDurationsList'} = &LOC::Util::getHashValues($tabDayInvoiceDurationsList);

    # URL de recherche des contrats liés
    my $delayMinDateEnd = &LOC::Characteristic::getAgencyValueByCode(
                            'DELTAMAXCTTLIE', $estimateAgencyId, $estimateValueDate) * 1;
    $tabViewData{'appliedToContractSearchUrl'} = &LOC::Request::createRequestUri('default:contract:jsonList', {
        'format' => 2,
        'filters' => {
            'agencyId' => $estimateAgencyId,
            'isClosed' => 1,
            'endDate'  => [&LOC::Date::subDays($estimateValueDate, $delayMinDateEnd), undef]
        }
    });


    $tabViewData{'maxFileSize'} = &LOC::Characteristic::getAgencyValueByCode('MAXTAILLEDOC', $estimateAgencyId) * 1;
    $tabViewData{'maxMailAttachmentsSize'} = &LOC::Characteristic::getAgencyValueByCode('MAXTAILLEDOCMAIL', $estimateAgencyId) * 1;

    $tabViewData{'additionalAttachments'} = [];

    # Corps du mail par défaut
    if ($editMode eq 'update')
    {
        $tabViewData{'emailContent'} = &LOC::Characteristic::getAgencyValueByCode('MAILDEVIS', $estimateAgencyId);
        my $tabAdditionalAttachments = &LOC::Json::fromJson(&LOC::Characteristic::getCountryValueByCode('DOCAUTODEVIS', $estimateCountryId));

        # Pièces jointes supplémentaires
        # - Amiante
        if ($tabViewData{'tabEstimateInfos'}->{'tabSite'}->{'isAsbestos'} && $tabAdditionalAttachments->{'asbestos'})
        {
            foreach my $documentInfos (@{$tabAdditionalAttachments->{'asbestos'}})
            {
                $documentInfos->{'url'} =~ s/<%commonFilesUrl>/@{[&LOC::Globals::get('commonFilesUrl')]}/;
                push(@{$tabViewData{'additionalAttachments'}}, {
                    'url'      => $documentInfos->{'url'},
                    'filename' => $documentInfos->{'label'}
                });
            }
        }
    }

    # Onglet actif
    $tabViewData{'tabIndex'} = &LOC::Request::getInteger('tabIndex', undef);


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentEstimate/view.cgi';
}


# Function: saveAction
# Sauvegarde des informations du devis
#
sub saveAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $tabSession = &LOC::Session::getInfos();

    # Variables pour la vue
    our %tabViewData = (
        'locale.id' => $tabUserInfos->{'locale.id'},
        'timeZone'  => $tabSession->{'timeZone'},
        'tabData' => {}
    );

    # Récupération des paramètres
    my $tabEstimateData = &LOC::Request::getHash('tabData');
    my $countryId       = $tabEstimateData->{'country.id'};
    my $estimateId      = $tabEstimateData->{'id'};
    my $activeLineId    = $tabEstimateData->{'activeLineId'};

    my $currentUserId   = $tabUserInfos->{'id'};

    my $action = $tabEstimateData->{'@action'};
    my $tabErrors = {};

    # Enregistrement du devis
    my $tabSiteMessages = {};
    if ($action eq 'insert')
    {
        $estimateId = &LOC::Estimate::Rent::insert($countryId, $tabEstimateData->{'tabCustomer'}->{'id'},
                                                   $tabEstimateData, $currentUserId, $tabErrors);
    }
    elsif ($action eq 'update')
    {
        &LOC::Estimate::Rent::update($countryId, $estimateId, $tabEstimateData, $currentUserId, $tabErrors);
    }



    # Construction de la trame de retour
    my $nbErrors = @{$tabErrors->{'fatal'}};
    if ($nbErrors > 0)
    {
        $tabViewData{'tabData'} = {
            '@action' => $action,
            '@state'  => 'ko',
            '@perlErrors' => $tabErrors
        };

        my @tabJSErrors = (); # 0 pour être sûr d'avoir un numéro d'erreur dans le tableau !!

        # Erreur sur le devis
        if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, -1); # Erreur fatale
        }
        if (&LOC::Util::in_array(0x0010, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 15); # Négociateur non renseigné
        }
        if (&LOC::Util::in_array(0x0100, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 1); # Devis déjà modifié
        }
        if (&LOC::Util::in_array(0x0003, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 3); # Certaines informations ne sont pas modifiables
        }
        if (&LOC::Util::in_array(0x0104, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 101); # Client non renseigné
        }
        if (&LOC::Util::in_array(0x0127, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 31); # Le mail du contact commande n'est pas valide
        }
        if (&LOC::Util::in_array(0x0105, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 20); # Client bloqué au devis
        }
        if (&LOC::Util::in_array(0x0106, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 109); # Commentaire d'abandon obligatoire
        }
        if (&LOC::Util::in_array(0x0107, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 5); # Le devis n'est pas modifiable
        }
        if (&LOC::Util::in_array(0x0110, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 120); # La zone n'est pas modifiable
        }
        if (&LOC::Util::in_array(0x0103, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 104); # La zone n'est pas valide
        }
        if (&LOC::Util::in_array(0x1009, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 125); # L'horaire de livraison n'est pas renseigné
        }
        # Erreur sur le chantier
        if (&LOC::Util::in_array(0x0130, $tabErrors->{'fatal'}))
        {
            my $tabSiteErrors = $tabErrors->{'modules'}->{'site'};
            if (&LOC::Util::in_array(LOC::Site::ERROR_LABELREQUIRED, $tabSiteErrors->{'fatal'}))
            {
                push(@tabJSErrors, 102); # Le libellé du chantier n'est pas renseigné
            }
            if (&LOC::Util::in_array(LOC::Site::ERROR_CITYNOTVALID, $tabSiteErrors->{'fatal'}))
            {
                push(@tabJSErrors, 103); # La ville du chantier n'est pas renseignée
            }
            if (&LOC::Util::in_array(LOC::Site::ERROR_CONTACTMAILNOTVALID, $tabSiteErrors->{'fatal'}))
            {
                push(@tabJSErrors, 121); # Le mail du contact n'est pas valide
            }
        }
        # Erreur sur une ligne
        if (&LOC::Util::in_array(0x0150, $tabErrors->{'fatal'}))
        {
            push(@tabJSErrors, 100); # Erreur sur les lignes de devis
        }

        my @tabLinesData = ();
        my $index = 0;
        foreach my $tabLineData (@{$tabEstimateData->{'tabLines'}})
        {
            my @tabJSLineErrors = ();
            my @tabJSLineDetails = ();

            my $tabLineErrors = $tabErrors->{'modules'}->{'lines'}->[$index];
            if (&LOC::Util::in_array(0x1000, $tabLineErrors->{'fatal'}))
            {
                push(@tabJSLineErrors, 105); # Le modèle de machines n'est pas renseigné
            }
            if (&LOC::Util::in_array(0x1001, $tabLineErrors->{'fatal'}))
            {
                push(@tabJSLineErrors, 113); # Ligne de devis déjà modifiée
            }
            if (&LOC::Util::in_array(0x1002, $tabLineErrors->{'fatal'}))
            {
                push(@tabJSLineErrors, 107); # La durée est incorrecte
            }
            if (&LOC::Util::in_array(0x1004, $tabLineErrors->{'fatal'}))
            {
                push(@tabJSLineErrors, 110); # le nombre de jours ouvrés a changé
            }
            if (&LOC::Util::in_array(0x1005, $tabLineErrors->{'fatal'}))
            {
                push(@tabJSLineErrors, 108); # Les dates de début et de fin ne sont pas renseignées
            }
            if (&LOC::Util::in_array(0x1007, $tabLineErrors->{'fatal'}))
            {
                push(@tabJSLineErrors, 111); # La date de début ne doit pas être inférieure à 1 mois
            }
            if (&LOC::Util::in_array(0x1008, $tabLineErrors->{'fatal'}))
            {
                push(@tabJSLineErrors, 130); # La date de fin est inférieure à la date de début
            }
            if (&LOC::Util::in_array(0x1009, $tabLineErrors->{'fatal'}))
            {
                push(@tabJSLineErrors, 135); # L'horaire de livraison n'est pas renseigné
            }
            if (&LOC::Util::in_array(0x1006, $tabLineErrors->{'fatal'}))
            {
                push(@tabJSLineErrors, 112); # Nombre de ligne maximum dépassé
            }
            if (&LOC::Util::in_array(0x1003, $tabLineErrors->{'fatal'}))
            {
                push(@tabJSLineErrors, 403); # Impossible de supprimer une ligne de devis passée en contrat
            }
            if (&LOC::Util::in_array(0x1050, $tabLineErrors->{'fatal'}))
            {
                push(@tabJSLineErrors, 114); # Impossible de modifier le type d'assurance
            }
            if (&LOC::Util::in_array(0x1051, $tabLineErrors->{'fatal'}))
            {
                push(@tabJSLineErrors, 115); # Impossible de modifier le taux d'assurance
            }
            if (&LOC::Util::in_array(0x1052, $tabLineErrors->{'fatal'}))
            {
                push(@tabJSLineErrors, 116); # Mode calcul participation au recyclage non disponible
            }
            if (&LOC::Util::in_array(0x1053, $tabLineErrors->{'fatal'}))
            {
                push(@tabJSLineErrors, 117); # Mode calcul gestion de recours non disponible
            }

            # Erreurs sur les j+/j-
            if (&LOC::Util::in_array(LOC::Estimate::Rent::LINE_ERROR_MODULE_DAYS, $tabLineErrors->{'fatal'}))
            {
                my $tabDaysErrors = $tabLineErrors->{'modules'}->{'days'};
                if (&LOC::Util::in_array(LOC::Day::ERROR_TYPEREQUIRED, $tabDaysErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 801); # Le type d'un jour n'est pas renseigné
                }
                if (&LOC::Util::in_array(LOC::Day::ERROR_DATEREQUIRED, $tabDaysErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 802); # La date n'est pas renseignée
                }
                if (&LOC::Util::in_array(LOC::Day::ERROR_REASONREQUIRED, $tabDaysErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 803); # Le motif n'est pas renseigné
                }
                if (&LOC::Util::in_array(LOC::Day::ERROR_EXISTINGDAY, $tabDaysErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 804); # Le jour existe déjà
                }
                if (&LOC::Util::in_array(LOC::Day::ERROR_OUTOFPERIOD, $tabDaysErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 805); # Le jour n'est pas dans la période autorisée
                }
                if (&LOC::Util::in_array(LOC::Day::ERROR_ALREADYINVOICEABLE, $tabDaysErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 806); # Le jour est déjà facturable
                }
                if (&LOC::Util::in_array(LOC::Day::ERROR_ALREADYNOTINVOICEABLE, $tabDaysErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 807); # Le jour est déjà non facturable
                }
                if (&LOC::Util::in_array(LOC::Day::ERROR_INVALIDAPPLIEDTOCONTRACT, $tabDaysErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 808); # Le contrat lié n'est pas valide
                }
                if ($tabDaysErrors->{'details'})
                {
                    push(@tabJSLineDetails, {
                        'module'   => 'days',
                        'tabInfos' => $tabDaysErrors->{'details'}
                    });
                }
            }

            # Erreurs tarification location
            if (&LOC::Util::in_array(LOC::Estimate::Rent::LINE_ERROR_MODULE_TARIFFRENT, $tabLineErrors->{'fatal'}))
            {
                my $tabRentTariffErrors = $tabLineErrors->{'modules'}->{'rentTariff'};
                if (&LOC::Util::in_array(0x0101, $tabRentTariffErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 202); # La justification de la RE n'est pas renseignée
                }
            }
            # Erreurs tarification transport
            if (&LOC::Util::in_array(LOC::Estimate::Rent::LINE_ERROR_MODULE_TARIFFTRSP, $tabLineErrors->{'fatal'}))
            {
                my $tabTrspTariffErrors = $tabLineErrors->{'modules'}->{'transportTariff'};
                if (&LOC::Util::in_array(0x0100, $tabTrspTariffErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 301); # Pas d'estimation de transport
                }
                if (&LOC::Util::in_array(0x0101, $tabTrspTariffErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 303); # Le client a changé
                }
                if (&LOC::Util::in_array(0x0102, $tabTrspTariffErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 302); # La zone de transport a changé
                }
                if (&LOC::Util::in_array(0x0103, $tabTrspTariffErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 304); # Le montant du transport aller est inférieur à l'estimation
                }
                if (&LOC::Util::in_array(0x0104, $tabTrspTariffErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 305); # Le montant du transport retour est inférieur à l'estimation
                }
                if (&LOC::Util::in_array(0x0107, $tabTrspTariffErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 308); # Le forfait machine supplémentaire aller est inférieur à la valeur minimale
                }
                if (&LOC::Util::in_array(0x0108, $tabTrspTariffErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 309); # Le forfait machine supplémentaire retour est inférieur à la valeur minimale
                }
            }
            # Erreurs proforma
            if (&LOC::Util::in_array(LOC::Estimate::Rent::LINE_ERROR_MODULE_PROFORMA, $tabLineErrors->{'fatal'}))
            {
                my $tabProformaErrors = $tabLineErrors->{'modules'}->{'proforma'};
                if (&LOC::Util::in_array(0x0004, $tabProformaErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 504); # Type de montant mensuel
                }
                if (&LOC::Util::in_array(0x0005, $tabProformaErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 505); # Action pro forma non autorisée
                }
                if (&LOC::Util::in_array(0x0008, $tabProformaErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 508); # La date de fin saisie est antérieure à la date de fin du contrat
                }
                if (&LOC::Util::in_array(0x0009, $tabProformaErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 509); # La date de fin saisie est antérieure à la date de fin de la dernière prolongation
                }
                if (&LOC::Util::in_array(0x0012, $tabProformaErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 512); # Impossible de modifier la proforma
                }
                if (&LOC::Util::in_array(0x0013, $tabProformaErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 513); # Impossible d'abandonner la proforma
                }
                if (&LOC::Util::in_array(0x0015, $tabProformaErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 515); # Impossible de réactiver la proforma
                }
                if (&LOC::Util::in_array(0x0017, $tabProformaErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 517); # Le montant de la pro forma est nul
                }
                if (&LOC::Util::in_array(0x0018, $tabProformaErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 518); # Le mode de règlement n'est pas renseigné
                }
                if (&LOC::Util::in_array(0x0019, $tabProformaErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 519); # estimation carburant incorrecte
                }
            }
            # Erreur documents
            if (&LOC::Util::in_array(LOC::Estimate::Rent::LINE_ERROR_MODULE_DOCUMENTS, $tabLineErrors->{'fatal'}))
            {
                my $tabDocumentsErrors = $tabLineErrors->{'modules'}->{'documents'};
                if (&LOC::Util::in_array(0x0010, $tabDocumentsErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 550); # Impossible de supprimer le document
                }
                if (&LOC::Util::in_array(0x0020, $tabDocumentsErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 551); # Impossible d'ajouter le document
                }
                if (&LOC::Util::in_array(0x0030, $tabDocumentsErrors->{'fatal'}))
                {
                    push(@tabJSLineErrors, 552); # Impossible d'ajouter le document
                }
            }

            push(@tabLinesData, {
                '@state'   => 'ko',
                '@errors'  => \@tabJSLineErrors,
                '@details' => \@tabJSLineDetails
            });
            $index++;
        }
        # Si pas d'erreur alors erreur inconnue !!
        if (@tabJSErrors == 0)
        {
            push(@tabJSErrors, 0); # Erreur inconnue !
        }

        $tabViewData{'tabData'}->{'@errors'} = \@tabJSErrors;
        $tabViewData{'tabData'}->{'tabLines'} = \@tabLinesData;
    }
    else
    {
        # Récupération des données du devis et des lignes
        my $tabEstimateInfos = &_getEstimateInfos($countryId, $tabUserInfos, $estimateId);

        my @tabLinesId = ();
        foreach my $tabLineData (@{$tabEstimateData->{'tabLines'}})
        {
            if ($tabLineData->{'@action'} ne 'delete')
            {
                push(@tabLinesId, $tabLineData->{'id'});
            }
        }
        my $tabLinesInfos = &_getEstimateLinesInfos($countryId, $tabUserInfos, \@tabLinesId, {});


        my @tabLinesData = ();
        my $lineIndex = 0;
        foreach my $tabLineData (@{$tabEstimateData->{'tabLines'}})
        {
            my $lineId     = $tabLineData->{'id'};
            my $lineAction = $tabLineData->{'@action'};

            my $tabLineInfos;
            if ($lineAction eq 'update' || $lineAction eq 'insert')
            {
                $tabLineInfos = $tabLinesInfos->{$lineId};
                if ($tabLineInfos)
                {
                    $tabLineInfos->{'@action'} = $lineAction;
                    $tabLineInfos->{'@state'}  = 'ok';
                }
                else
                {
                    $tabLineInfos = {'id' => $lineId, '@state' => 'ko'};
                }
            }
            elsif ($lineAction eq 'delete')
            {
                $tabLineInfos = {'id' => $lineId, '@action' => 'delete', '@state' => 'ok'};
            }

            # Vérification des warnings
            my @tabJSWarnings = ();

            # S'il y a une erreur sur les documents
            my $tabLineErrors = $tabErrors->{'modules'}->{'lines'}->[$lineIndex];
            if (&LOC::Util::in_array(LOC::Estimate::Rent::LINE_ERROR_MODULE_DOCUMENTS, $tabLineErrors->{'warning'}))
            {
                my $tabIndexesByType = {
                    'remove'    => [],
                    'add'       => [],
                    'edit'      => [],
                    'duplicate' => []
                };

                my $index = 0;
                foreach my $tabAction (@{$tabLineData->{'tabDocumentsActions'}})
                {
                    push(@{$tabIndexesByType->{$tabAction->{'type'}}}, $index);
                    $index++;
                }

                foreach my $type (keys %$tabIndexesByType)
                {
                    if (@{$tabIndexesByType->{$type}} > 0)
                    {
                        push(@tabJSWarnings, {
                            'module'  => 'documents',
                            'code'    => $type,
                            'indexes' => $tabIndexesByType->{$type}
                        });
                    }
                }

                $tabLineInfos->{'@warnings'} = \@tabJSWarnings;
                $tabLineInfos->{'@documentsActions'} = $tabLineData->{'tabDocumentsActions'};
            }

            push(@tabLinesData, $tabLineInfos);

            $lineIndex++;
        }

        my @tabJSWarnings = ();
        # S'il y a une erreur sur le chantier
        if (&LOC::Util::in_array(0x0130, $tabErrors->{'warning'}))
        {
            my $tabSiteErrors = $tabErrors->{'modules'}->{'site'};
            # - Informations contact incomplètes
            if (&LOC::Util::in_array(LOC::Site::ERROR_CONTACTREQUIRED, $tabSiteErrors->{'warning'}))
            {
                push(@tabJSWarnings, {
                    'module' => 'site',
                    'code'   => 'contact'
                });
            }
        }

        # S'il y a une erreur générale
        # - Informations contact commande incomplètes
        if (&LOC::Util::in_array(0x0128, $tabErrors->{'warning'}))
        {
                push(@tabJSWarnings, {
                    'module' => 'general',
                    'code'   => 'orderContact'
                });
        }

        # Préparation des données
        $tabViewData{'tabData'} = $tabEstimateInfos;
        $tabViewData{'tabData'}->{'@action'} = $action;
        $tabViewData{'tabData'}->{'@state'}  = 'ok';
        $tabViewData{'tabData'}->{'@url'} = '';
        $tabViewData{'tabData'}->{'@warnings'} = \@tabJSWarnings;
        if ($tabEstimateData->{'isAllLines'} == 1)
        {
            my $activeLineId = &LOC::Request::getInteger('activeLineId', undef);
            my $tabIndex     = &LOC::Request::getInteger('tabIndex', undef);

            my $tabParams = {'estimateId' => $estimateId};
            if (defined $tabIndex)
            {
                $tabParams->{'tabIndex'} = $tabIndex;
            }
            if (defined $activeLineId)
            {
                $tabParams->{'estimateLineId'} = $activeLineId;
            }
            $tabViewData{'tabData'}->{'@url'} = &LOC::Request::createRequestUri('rent:rentEstimate:view', $tabParams);
        }

        $tabViewData{'tabData'}->{'tabLines'} = \@tabLinesData;
    }


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentEstimate/save.cgi';
}


# Function: getUrlAction
# Récupère l'URL du contrat
#
sub getUrlAction
{
    # Variables pour la vue
    our %tabViewData = ();

    # Récupération des paramètres
    my $estimateId     = &LOC::Request::getInteger('estimateId', undef);
    my $estimateLineId = &LOC::Request::getInteger('estimateLineId', undef);
    my $tabIndex       = &LOC::Request::getInteger('tabIndex', undef);

    my $tabParams = {'estimateId' => $estimateId};
    if (defined $tabIndex)
    {
        $tabParams->{'tabIndex'} = $tabIndex;
    }
    if (defined $estimateLineId)
    {
        $tabParams->{'estimateLineId'} = $estimateLineId;
    }
    $tabViewData{'url'} = &LOC::Request::createRequestUri('rent:rentEstimate:view', $tabParams);


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentEstimate/getUrl.cgi';
}


# Function: loadLinesAction
# Récupérer les informations d'une ligne de devis
#
sub loadLinesAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = ('locale.id' => $tabUserInfos->{'locale.id'});


    # Récupération des paramètres
    my $countryId      = &LOC::Request::getString('countryId');
    my $tabLines       = &LOC::Request::getArray('tabLines');
    my $estimateId     = &LOC::Request::getInteger('estimateId');


    $tabViewData{'tabData'} = {
        '@action'  => 'load',
        '@state'   => 'ok'
    };

    # Récupération des données des lignes
    my $tabLinesInfos = &_getEstimateLinesInfos($countryId, $tabUserInfos, $tabLines, {});

    my @tabLinesData = ();
    foreach my $lineId (@$tabLines)
    {
        my $tabLineInfos = $tabLinesInfos->{$lineId};
        if ($tabLineInfos)
        {
            $tabLineInfos->{'@action'} = 'load';
            $tabLineInfos->{'@state'}  = 'ok';
        }
        else
        {
            $tabLineInfos = {'id' => $lineId, '@state' => 'ko'};
        }

        push(@tabLinesData, $tabLineInfos);
    }
    $tabViewData{'tabData'}->{'tabLines'} = \@tabLinesData;

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentEstimate/loadLines.cgi';
}


# Function: modelInfosAction
# Récupérer les informations d'un modèle de machine
#
sub modelInfosAction
{
    # Variables pour la vue
    our %tabViewData = ();

    # Récupération des paramètres
    my $countryId  = &LOC::Request::getString('countryId');
    my $customerId = &LOC::Request::getInteger('customerId');
    my $modelId    = &LOC::Request::getInteger('modelId');
    my $agencyId   = &LOC::Request::getString('agencyId');

    # Récupération des informations sur le modèle de machine
    $tabViewData{'tabModelInfos'} = &_getModelInfos($countryId, $modelId, $customerId, $agencyId);

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentEstimate/modelInfos.cgi';
}


# Function: rentalPeriodInfosAction
# Récupérer la durée de location entre deux dates pour une agence donnée
#
# Parameters:
# Parameters:
# string $dateBegin - Date de début au format MySQL
# string $dateEnd   - Date de fin au format MySQL
# string $agency    - Identifiant de l'agence
#
# Returns:
# hashref
sub rentalPeriodInfosAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Récupération des informations
    my $agencyId             = &LOC::Request::getString('agencyId');
    my $countryId            = &LOC::Request::getString('countryId');
    my $estimateId           = &LOC::Request::getInteger('entityId');
    my $dateBegin            = &LOC::Request::getString('beginDate');
    my $dateEnd              = &LOC::Request::getString('endDate');
    my $adjustDays           = &LOC::Request::getInteger('adjustDays');
    my $requiredBeginDate    = &LOC::Request::getString('requiredBeginDate');
    my $requiredEndDate      = &LOC::Request::getString('requiredEndDate');
    my $requiredRealDuration = &LOC::Request::getInteger('requiredRealDuration');
    my $beginDateWindow      = &LOC::Request::getArray('beginDateWindow');
    my $endDateWindow        = &LOC::Request::getArray('endDateWindow');

    # Variables pour la vue
    our %tabViewData = (
        'duration'                        => 0,
        'durationStr'                     => '',
        'realDuration'                    => 0,
        'calendarDuration'                => 0,
        'rentTariffDurRangeLabel'         => '',

        'requiredBeginDate'               => $requiredBeginDate,
        'requiredEndDate'                 => $requiredEndDate,
        'requiredDuration'                => 0,
        'requiredDurationStr'             => '',
        'requiredRealDuration'            => 0,
        'requiredCalendarDuration'        => 0,
        'requiredRentTariffDurRangeLabel' => '',
        'authorizedPeriodAddDays'         => [],
        'authorizedPeriodDedDays'         => []
    );

    # Récupération des informations
    if ($dateBegin ne '' && $dateEnd ne '')
    {
        my $realDuration     = &LOC::Date::getRentDuration($dateBegin, $dateEnd, $agencyId);
        my $duration         = $realDuration + $adjustDays;
        my $tabDurationRange = &LOC::Tariff::Rent::getDurationRangeInfos($countryId, $duration);

        $tabViewData{'duration'}                = $duration;
        $tabViewData{'realDuration'}            = $realDuration;
        $tabViewData{'calendarDuration'}        = &LOC::Date::getDeltaDays($dateBegin, $dateEnd);
        $tabViewData{'rentTariffDurRangeLabel'} = $tabDurationRange->{'label'};
    }
    if ($requiredBeginDate ne '' && ($requiredEndDate ne '' || $requiredRealDuration > 0))
    {
        # Vérification de la date de début par rapport aux plages disponibles
        if ($beginDateWindow->[0] && $requiredBeginDate lt $beginDateWindow->[0])
        {
            $requiredBeginDate = $beginDateWindow->[0];
        }
        elsif ($beginDateWindow->[1] && $requiredBeginDate gt $beginDateWindow->[1])
        {
            $requiredBeginDate = $beginDateWindow->[1];
        }

        if ($requiredRealDuration > 0)
        {
            $requiredEndDate = &LOC::Date::getRentEndDate($requiredBeginDate, $requiredRealDuration, $agencyId);
        }
        else
        {
            if ($requiredEndDate lt $requiredBeginDate)
            {
                $requiredEndDate = $requiredBeginDate;
            }
            $requiredRealDuration = &LOC::Date::getRentDuration($requiredBeginDate, $requiredEndDate, $agencyId);
        }

        # Vérification de la date de fin par rapport aux plages disponibles
        if ($endDateWindow->[0] && $requiredEndDate lt $endDateWindow->[0])
        {
            $requiredEndDate = $endDateWindow->[0];
        }
        elsif ($endDateWindow->[1] && $requiredEndDate gt $endDateWindow->[1])
        {
            $requiredEndDate = $endDateWindow->[1];
        }
        # recalcul de la durée
        $requiredRealDuration = &LOC::Date::getRentDuration($requiredBeginDate, $requiredEndDate, $agencyId);



        my $requiredDuration         = $requiredRealDuration + $adjustDays;
        my $tabRequiredDurationRange = &LOC::Tariff::Rent::getDurationRangeInfos($countryId, $requiredDuration);

        $tabViewData{'requiredBeginDate'}            = $requiredBeginDate;
        $tabViewData{'requiredEndDate'}              = $requiredEndDate;
        $tabViewData{'requiredDuration'}             = $requiredDuration;
        $tabViewData{'requiredRealDuration'}         = $requiredRealDuration;
        $tabViewData{'requiredCalendarDuration'}     = &LOC::Date::getDeltaDays($requiredBeginDate, $requiredEndDate);
        $tabViewData{'requiredRentTariffDurRangeLabel'} = $tabRequiredDurationRange->{'label'};

        my $valueDate = &LOC::Request::getString('valueDate');
        # Calcul des périodes possibles pour les jours +/-
        $tabViewData{'authorizedPeriodAddDays'} = &LOC::Day::getAuthorizedPeriod($agencyId, LOC::Day::TYPE_ADDITIONAL, 'estimateLine', [$requiredBeginDate, $requiredEndDate], $valueDate);
        $tabViewData{'authorizedPeriodDedDays'} = &LOC::Day::getAuthorizedPeriod($agencyId, LOC::Day::TYPE_DEDUCTED, 'estimateLine', [$requiredBeginDate, $requiredEndDate], $valueDate);

    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentContract/rentalPeriodInfos.cgi';
}


# Function: searchAction
# Recherche de devis de location
sub searchAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'offset'         => 0,
        'step'           => NBSEARCHSTEP
    );
    our %tabSearchData = ();

    my $countryId = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});


    # Données du formulaire
    $tabSearchData{'country.id'}            = $countryId;
    $tabSearchData{'search.no'}             = &LOC::Request::getString('search.no');
    $tabSearchData{'search.state.id'}       = &LOC::Request::getString('search.state.id');
    $tabSearchData{'search.user.id'}        = &LOC::Request::getInteger('search.user.id');
    $tabSearchData{'search.customer.id'}    = &LOC::Request::getInteger('search.customer.id');
    $tabSearchData{'search.negotiator.id'}  = &LOC::Request::getString('search.negotiator.id');
    $tabSearchData{'search.isNational'}     = &LOC::Request::getBoolean('search.isNational');
    $tabSearchData{'search.tariffFamily.id'} = &LOC::Request::getInteger('search.tariffFamily.id');

    # Données de la barre de recherche
    $tabSearchData{'states'} = &LOC::Estimate::Rent::getStates($countryId);
    $tabSearchData{'users'} = ();
    if ($tabSearchData{'search.user.id'} != 0)
    {
        $tabSearchData{'users'} = &LOC::User::getList(LOC::Util::GETLIST_PAIRS,
                                                      {'id' => $tabSearchData{'search.user.id'}},
                                                      LOC::User::GET_ACTIVE,
                                                      {'formatMode' => 1});
    }
    $tabSearchData{'customers'} = ();
    if ($tabSearchData{'search.customer.id'} != 0)
    {
        $tabSearchData{'customers'} = &LOC::Customer::getList($countryId, LOC::Util::GETLIST_PAIRS,
                                                              {'id' => $tabSearchData{'search.customer.id'}});
    }

    $tabSearchData{'negotiators'} = ();
    if ($tabSearchData{'search.negotiator.id'} != 0)
    {
        $tabSearchData{'negotiators'} = &LOC::User::getList(LOC::Util::GETLIST_PAIRS,
                                                      {'id' => $tabSearchData{'search.negotiator.id'}},
                                                      LOC::User::GET_ACTIVE,
                                                      {'formatMode' => 1});
    }

    my $tabTariffFamilies = {};
    tie(%$tabTariffFamilies, 'Tie::IxHash');
    $tabTariffFamilies = &LOC::TariffFamily::getList($countryId, 
                                               LOC::Util::GETLIST_PAIRS,
                                               {},
                                               {}, 
                                               {'sort' => [ {'field' => 'fulllabel', 'dir' => 'ASC'} ]}
                                               );
    $tabSearchData{'tariffFamilies'} = $tabTariffFamilies;


    # Résultats de la recherche
    my $tabFilters = {};
    if (!$tabSearchData{'search.isNational'})
    {
        my @tabAuthorizedAgenciesId = keys %{$tabUserInfos->{'tabAuthorizedAgencies'}};
        $tabFilters->{'agencyId'} = \@tabAuthorizedAgenciesId;
    }

    # on enlève les caractères non numériques
    $tabSearchData{'search.no'} =~ s/([^0-9%]*)//g;
    if ($tabSearchData{'search.no'} ne '')
    {
        $tabFilters->{'id'} = $tabSearchData{'search.no'};
    }
    if ($tabSearchData{'search.state.id'} ne '')
    {
        $tabFilters->{'stateId'} = $tabSearchData{'search.state.id'};
    }
    if ($tabSearchData{'search.user.id'} != 0)
    {
        $tabFilters->{'userId'} = $tabSearchData{'search.user.id'};
    }
    if ($tabSearchData{'search.customer.id'} != 0)
    {
        $tabFilters->{'customerId'} = $tabSearchData{'search.customer.id'};
    }
    if ($tabSearchData{'search.negotiator.id'} != 0)
    {
        $tabFilters->{'negotiatorId'} = $tabSearchData{'search.negotiator.id'};
    }
    if ($tabSearchData{'search.tariffFamily.id'} != 0)
    {
        $tabFilters->{'tariffFamilyId'} = $tabSearchData{'search.tariffFamily.id'};
    }

    # nombre de critères sélectionnés
    $tabViewData{'nbFilters'} = keys %$tabFilters;
    $tabViewData{'tabFilters'} = $tabFilters;

    my $tabOptions = {
        'limit' => {
            'offset' => $tabViewData{'offset'},
            'count'  => $tabViewData{'step'},
            '&total' => 0
        }
    };
    my $tabEstimates = &LOC::Estimate::Rent::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                       $tabFilters,
                                                       LOC::Estimate::Rent::GETINFOS_CUSTOMER |
                                                       LOC::Estimate::Rent::GETINFOS_EXT_SITE |
                                                       LOC::Estimate::Rent::GETINFOS_NEGOTIATOR,
                                                       $tabOptions);

    my @tabList;

    foreach my $tabEstimateInfos (values(%$tabEstimates))
    {
        push(@tabList, {
            'id'                 => $tabEstimateInfos->{'id'},
            'code'               => $tabEstimateInfos->{'code'},
            'customerName'       => $tabEstimateInfos->{'customer.name'},
            'siteLabel'          => $tabEstimateInfos->{'site.tabInfos'}->{'label'},
            'siteAddress'        => $tabEstimateInfos->{'site.tabInfos'}->{'locality.label'},
            'stateLabel'         => $tabEstimateInfos->{'state.label'},
            'negotiatorFullName' => $tabEstimateInfos->{'negotiator.fullName'},
            'url'                => &LOC::Request::createRequestUri('rent:rentEstimate:view', {'estimateId' => $tabEstimateInfos->{'id'}})
        });
    }
    $tabViewData{'result'} = \@tabList;
    $tabViewData{'total'}  = $tabOptions->{'limit'}->{'&total'};

    if ($tabViewData{'total'} == 1)
    {
        my $url = &LOC::Request::createRequestUri('rent:rentEstimate:view', {'estimateId' => $tabList[0]->{'id'}});
        # Redirection
        &LOC::Browser::sendHeaders("Status: 302 Found\nLocation: " . $url . "\n\n");
    }


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentEstimate/search.cgi';
}

# Function: jsonSearchAction
# Recherche de contrat de location
sub jsonSearchAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
    );

    my $countryId  = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $tabFilters = &LOC::Json::fromJson(&LOC::Request::getString('tabFilters', ''));
    $tabViewData{'offset'} = &LOC::Request::getInteger('offset', 0);
    $tabViewData{'step'}   = &LOC::Request::getInteger('step', 100);


    # nombre de critères sélectionnés
    $tabViewData{'nbFilters'} = keys %$tabFilters;
    $tabViewData{'tabFilters'}   = $tabFilters;

    # s'il y a assez de critères sélectionnés
    if ($tabViewData{'nbFilters'} > 0)
    {
        my $tabOptions = {
            'limit' => {
                'offset' => $tabViewData{'offset'},
                'count'  => $tabViewData{'step'},
                '&total' => 0
            }
        };
        my $tabEstimates = &LOC::Estimate::Rent::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                           $tabFilters,
                                                           LOC::Estimate::Rent::GETINFOS_CUSTOMER |
                                                           LOC::Estimate::Rent::GETINFOS_EXT_SITE |
                                                           LOC::Estimate::Rent::GETINFOS_CREATOR,
                                                           $tabOptions);

        my @tabList;

        foreach my $tabEstimateInfos (values(%$tabEstimates))
        {
            push(@tabList, {
                'id'              => $tabEstimateInfos->{'id'},
                'code'            => $tabEstimateInfos->{'code'},
                'customerName'    => $tabEstimateInfos->{'customer.name'},
                'siteLabel'       => $tabEstimateInfos->{'site.tabInfos'}->{'label'},
                'siteAddress'     => $tabEstimateInfos->{'site.tabInfos'}->{'locality.label'},
                'stateLabel'      => $tabEstimateInfos->{'state.label'},
                'creatorFullName' => $tabEstimateInfos->{'creator.fullName'},
                'url'             => &LOC::Request::createRequestUri('rent:rentEstimate:view', {'estimateId' => $tabEstimateInfos->{'id'}})
            });
        }

        $tabViewData{'result'} = \@tabList;
        $tabViewData{'total'}  = $tabOptions->{'limit'}->{'&total'};
    }


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentEstimate/jsonSearch.cgi';
}




# Function: viewAcceptedActivesLinesAction
# Affichage des lignes actives de devis acceptés
sub viewAcceptedActivesLinesAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $displayType = &LOC::Request::getString('displayType', '');
    my $countryId   = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $agencyId   = &LOC::Request::getString('agencyId', $tabUserInfos->{'nomadAgency.id'});


    # Récupération de liste des régions et des agences disponibles
    my $tabAreas = {};
    tie(%$tabAreas, 'Tie::IxHash');
    my $firstAreaId = 0;
    foreach my $tabAgencyInfos (values %{$tabUserInfos->{'tabAuthorizedAgencies'}})
    {
        if ($tabAgencyInfos->{'country.id'} eq $countryId &&
            $tabAgencyInfos->{'type'} eq LOC::Agency::TYPE_AGENCY)
        {
            if (!$firstAreaId)
            {
                $firstAreaId = $tabAgencyInfos->{'area.id'};
            }

            my $id     = $tabAgencyInfos->{'id'};
            my $areaId = $tabAgencyInfos->{'area.id'};

            if (!exists $tabAreas->{$areaId})
            {
                $tabAreas->{$areaId} = {'id'          => $areaId,
                                        'name'        => $tabAgencyInfos->{'area.name'},
                                        'tabAgencies' => {}};
                tie(%{$tabAreas->{$areaId}->{'tabAgencies'}}, 'Tie::IxHash');
            }
            $tabAreas->{$areaId}->{'tabAgencies'}->{$id} = {'id'   => $id,
                                                            'name' => $tabAgencyInfos->{'name'}};
        }
    }

    # Région courante
    my $currentAreaId = &LOC::Request::getInteger('areaId', $tabUserInfos->{'tabAuthorizedAgencies'}->{$agencyId}->{'area.id'});
    if (!exists $tabAreas->{$currentAreaId})
    {
        $currentAreaId = $firstAreaId;
    }

    # Récupération de la liste des agences pour la recherche
    my $tabAgenciesId = [];
    foreach my $tabAgencyInfos (values %{$tabUserInfos->{'tabAuthorizedAgencies'}})
    {
        if ($tabAgencyInfos->{'country.id'} eq $countryId &&
            ($displayType eq 'xls' || $tabAgencyInfos->{'area.id'} == $currentAreaId))
        {
            push(@$tabAgenciesId, $tabAgencyInfos->{'id'});
        }
    }


    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );


    # Données pour la vue
    $tabViewData{'tabAreas'} = $tabAreas;
    $tabViewData{'currentAreaId'} = $currentAreaId;

    # Récupération des lignes devis
    $tabViewData{'tabEstimateLines'} = &LOC::Estimate::Rent::getLinesList(
                                                         $countryId,
                                                         LOC::Util::GETLIST_ASSOC,
                                                         {'estimateAgencyId' => $tabAgenciesId,
                                                          'stateId'          => LOC::Estimate::Rent::LINE_STATE_ACTIVE,
                                                          'isAccepted'       => 1,
                                                          'estimateStateId'  => [LOC::Estimate::Rent::STATE_ACTIVE,
                                                                                 LOC::Estimate::Rent::STATE_CONTRACT]},
                                                         LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE |
                                                         LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE_CUSTOMER |
                                                         LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE_SITE |
                                                         LOC::Estimate::Rent::GETLINEINFOS_EXT_MODEL |
                                                         LOC::Estimate::Rent::GETLINEINFOS_TARIFFTRSP |
                                                         LOC::Estimate::Rent::GETLINEINFOS_INDEX
                                                     );

    # Liste des plages horaires de transport
    $tabViewData{'tabTransportTimeSlots'} = &LOC::Transport::TimeSlot::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                                               {'stateId' => [LOC::Transport::TimeSlot::STATE_ACTIVE,
                                                                                              LOC::Transport::TimeSlot::STATE_DELETED]},
                                                                               LOC::Transport::TimeSlot::GETINFOS_BASE,
                                                                               {'labelFormat' => 2});

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;

    if ($displayType eq 'xls')
    {
        require $directory . '/view/rentEstimate/viewAcceptedActivesLinesExcel.cgi';
    }
    else
    {
        # URL's sur les vieux scripts
        $tabViewData{'customerUrl'} = &LOC::Customer::getUrl('<%id>');
        $tabViewData{'machineUrl'}  = &LOC::Machine::getUrlByParkNumber('<%parkNumber>');

        require $directory . '/view/rentEstimate/viewAcceptedActivesLines.cgi';
    }
}


# Function: viewAcceptedLinesAction
# Affichage de l'alerte des lignes de devis acceptées
sub viewAcceptedLinesAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId      = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $agencyId       = &LOC::Request::getString('agencyId', $tabUserInfos->{'nomadAgency.id'});

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'country.id'     => $countryId,
        'agency.id'      => $agencyId,
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );


    # Données pour la vue

    # Récupération des lignes devis
    $tabViewData{'tabEstimateLines'} = &LOC::Estimate::Rent::getLinesList(
                                                     $countryId,
                                                     LOC::Util::GETLIST_ASSOC,
                                                     {'estimateAgencyId' => $agencyId,
                                                      'stateId'          => [LOC::Estimate::Rent::LINE_STATE_ACTIVE],
                                                      'isAccepted'       => 1,
                                                      'estimateStateId'  => [LOC::Estimate::Rent::STATE_ACTIVE,
                                                                             LOC::Estimate::Rent::STATE_CONTRACT]},
                                                     LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE |
                                                     LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE_CUSTOMER |
                                                     LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE_SITE |
                                                     LOC::Estimate::Rent::GETLINEINFOS_EXT_MODEL |
                                                     LOC::Estimate::Rent::GETLINEINFOS_TARIFFTRSP |
                                                     LOC::Estimate::Rent::GETLINEINFOS_INDEX
                                                 );

    my $tabEstimateLinesLite = [];
    $tabViewData{'tabEstimateLinesLite'} = [];
    foreach my $estimateLine (values(%{$tabViewData{'tabEstimateLines'}}))
    {
        push(@{$tabEstimateLinesLite},
                {
                    'id'                        => $estimateLine->{'id'},
                    'modificationDate'          => $estimateLine->{'modificationDate'},
                    'estimate.modificationDate' => $estimateLine->{'estimate.modificationDate'},
                    'action'                    => 'unaccept'
                });
    }
    $tabViewData{'tabEstimateLinesLite'} = &LOC::Json::toJson($tabEstimateLinesLite);


    # URL's sur les vieux scripts
    $tabViewData{'customerUrl'} = &LOC::Customer::getUrl('<%id>');

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentEstimate/viewAcceptedLines.cgi';

}


# Function: viewToCallLinesAction
# Affichage de l'alerte des lignes de devis à rappeler
sub viewToCallLinesAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId      = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $agencyId       = &LOC::Request::getString('agencyId', $tabUserInfos->{'nomadAgency.id'});

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'country.id'     => $countryId,
        'agency.id'      => $agencyId,
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    # Données pour la vue

    # Récupération des lignes devis
    $tabViewData{'tabEstimateLines'} = &LOC::Estimate::Rent::getLinesList(
                                                     $countryId,
                                                     LOC::Util::GETLIST_ASSOC,
                                                     {'estimateAgencyId' => $agencyId,
                                                      'stateId'          => [LOC::Estimate::Rent::LINE_STATE_ACTIVE,
                                                                             LOC::Estimate::Rent::LINE_STATE_INACTIVE],
                                                      'isFollowed'       => 1,
                                                      'isAccepted'       => 0,
                                                      'estimateStateId'  => [LOC::Estimate::Rent::STATE_ACTIVE,
                                                                             LOC::Estimate::Rent::STATE_CONTRACT]},
                                                     LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE |
                                                     LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE_CUSTOMER |
                                                     LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE_SITE |
                                                     LOC::Estimate::Rent::GETLINEINFOS_EXT_MODEL |
                                                     LOC::Estimate::Rent::GETLINEINFOS_INDEX |
                                                     LOC::Estimate::Rent::GETLINEINFOS_TARIFFRENT |
                                                     LOC::Estimate::Rent::GETLINEINFOS_TARIFFTRSP |
                                                     LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE_CREATOR |
                                                     LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE_NEGOCIATOR |
                                                     LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE_CUSTOMER
                                                 );

    my $tabEstimateLinesLite = [];
    $tabViewData{'tabEstimateLinesLite'} = [];
    foreach my $estimateLine (values(%{$tabViewData{'tabEstimateLines'}}))
    {
        push(@{$tabEstimateLinesLite},
                {
                    'id'                        => $estimateLine->{'id'},
                    'modificationDate'          => $estimateLine->{'modificationDate'},
                    'estimate.modificationDate' => $estimateLine->{'estimate.modificationDate'},
                    'action'                    => 'unfollow'
                });
    }
    $tabViewData{'tabEstimateLinesLite'} = &LOC::Json::toJson($tabEstimateLinesLite);


    # URL's sur les vieux scripts
    $tabViewData{'customerUrl'} = &LOC::Customer::getUrl('<%id>');

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentEstimate/viewToCallLines.cgi';

}


# Function: doLinesAction
# Modification de l'état d'acceptation et de suivi des lignes de devis
sub doLinesAction
{
     # Variables pour la vue
    our %tabViewData = ();

    # hachage de l'utilisateur
    my $user = &LOC::Session::getUserInfos();


    # Récupération des paramètres
    my $countryId        = &LOC::Request::getString('countryId');
    my $tabEstimateLines = &LOC::Request::getArray('tabEstimateLines', []);


    # Traitement
    $tabViewData{'result'} = [];
    $tabViewData{'tabLineErrors'} = [];
    foreach my $line (@$tabEstimateLines)
    {
        my $tabErrors = {};
        my $tabUpdates = {
            'estimate.modificationDate' => $line->{'estimate.modificationDate'},
            'modificationDate'          => $line->{'modificationDate'}
        };

        my $modifDate = $line->{'modificationDate'};
        my $modifEstimateDate = $line->{'estimate.modificationDate'};

        $tabUpdates->{'@subAction'} = $line->{'action'};
        my $result = &LOC::Estimate::Rent::updateLine($countryId, $line->{'id'}, $tabUpdates, $user->{'id'},
                                                      {'disableTransportTariffErrors' => 1}, $tabErrors);
        if ($result)
        {
            my $newLine = &LOC::Estimate::Rent::getLineInfos($countryId, $line->{'id'});
            $modifDate = $newLine->{'modificationDate'};
            $modifEstimateDate = $newLine->{'estimate.modificationDate'};
        }

        # resultat
        push(@{$tabViewData{'result'}}, {
            'id'                        => $line->{'id'},
            'action'                    => $line->{'action'},
            'result'                    => $result,
            'modificationDate'          => $modifDate,
            'estimate.modificationDate' => $modifEstimateDate,
            'tabErrors'                 => $tabErrors
        });

    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentEstimate/doLines.cgi';
}

# Function: pdfAction
# Affiche le PDF d'un contrat
#
# Parameters:
# int  contractId   - Identifiant du contrat
sub pdfAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Récupération des paramètres
    my $countryId      = $tabUserInfos->{'nomadCountry.id'};
    my $currencySymbol = $tabUserInfos->{'currency.symbol'};

    my $estimateId     = &LOC::Request::getInteger('estimateId', undef);

    # Variables pour la vue
    our %tabViewData = (
        'locale.id' => $tabUserInfos->{'locale.id'}
    );
    # Récupération des informations nécessaires à l'impression
    my $tabEstimateInfos = &LOC::Estimate::Rent::getInfos($countryId, $estimateId,
                                                          LOC::Estimate::Rent::GETINFOS_EXT_SITE |
                                                          LOC::Estimate::Rent::GETINFOS_NEGOTIATOR);

    my $tabAgencyInfos = &LOC::Agency::getInfos($tabEstimateInfos->{'agency.id'});
    my $tabCountryInfos = &LOC::Country::getInfos($countryId);
    my $tabCustomerInfos = &LOC::Customer::getInfos($countryId, $tabEstimateInfos->{'customer.id'});
    my $tabEstimateLines = &LOC::Estimate::Rent::getLinesList($countryId, LOC::Util::GETLIST_ASSOC,
                                                              {'estimateId' => $estimateId,
                                                               'stateId'    => [LOC::Estimate::Rent::LINE_STATE_ACTIVE,LOC::Estimate::Rent::LINE_STATE_CONVERTED]
                                                              },
                                                              LOC::Estimate::Rent::GETLINEINFOS_EXT_MODEL |
                                                              LOC::Estimate::Rent::GETLINEINFOS_TARIFFRENT |
                                                              LOC::Estimate::Rent::GETLINEINFOS_TARIFFTRSP |
                                                              LOC::Estimate::Rent::GETLINEINFOS_INSURANCE |
                                                              LOC::Estimate::Rent::GETLINEINFOS_EXT_INSURANCETYPE |
                                                              LOC::Estimate::Rent::GETLINEINFOS_APPEAL |
                                                              LOC::Estimate::Rent::GETLINEINFOS_EXT_APPEALTYPE |
                                                              LOC::Estimate::Rent::GETLINEINFOS_CLEANING |
                                                              LOC::Estimate::Rent::GETLINEINFOS_RESIDUES);

    $tabViewData{'tabData'} = {
        'countryId'       => $countryId,
        'id'              => $tabEstimateInfos->{'id'},
        'code'            => $tabEstimateInfos->{'code'},
        'isFullService'   => $tabEstimateInfos->{'isFullService'},
        'orderNo'         => $tabEstimateInfos->{'orderNo'},
        'orderContact.fullName'  => $tabEstimateInfos->{'orderContact.fullName'},
        'orderContact.telephone' => $tabEstimateInfos->{'orderContact.telephone'},
        'comments'        => $tabEstimateInfos->{'comments'},
        'agency.tabInfos' => {
            'id'               => $tabAgencyInfos->{'id'},
            'label'            => $tabAgencyInfos->{'label'},
            'manager.fullName' => $tabAgencyInfos->{'manager.firstname'} . ' ' . $tabAgencyInfos->{'manager.name'},
            'address'          => $tabAgencyInfos->{'address'},
            'telephone'        => $tabAgencyInfos->{'telephone'},
            'fax'              => $tabAgencyInfos->{'fax'},
            'postalCode'       => $tabAgencyInfos->{'postalCode'},
            'city'             => $tabAgencyInfos->{'city'}
        },
        'customer.tabInfos' => {
            'label'      => $tabCustomerInfos->{'name'},
            'address'    => $tabCustomerInfos->{'address1'},
            'address2'   => $tabCustomerInfos->{'address2'},
            'postalCode' => $tabCustomerInfos->{'locality.postalCode'},
            'city'       => $tabCustomerInfos->{'locality.name'},
            'telephone'  => $tabCustomerInfos->{'telephone'},
            'fax'        => $tabCustomerInfos->{'fax'},
            'paymentMode.label' => $tabCustomerInfos->{'paymentMode.label'},
            'potential'  => $tabCustomerInfos->{'potential'},
            'insurance.rate' => $tabCustomerInfos->{'insurance.rate'},
            'insurance.beginDate' => $tabCustomerInfos->{'insurance.beginDate'},
            'areGeneralConditionsApproved' => $tabCustomerInfos->{'areGeneralConditionsApproved'}
        },
        'site.tabInfos' => {
            'label'       => $tabEstimateInfos->{'site.tabInfos'}->{'label'},
            'address'     => $tabEstimateInfos->{'site.tabInfos'}->{'address'},
            'postalCode'  => $tabEstimateInfos->{'site.tabInfos'}->{'locality.postalCode'},
            'city'        => $tabEstimateInfos->{'site.tabInfos'}->{'locality.name'},
            'contact.fullName' => $tabEstimateInfos->{'site.tabInfos'}->{'contact.fullName'},
            'telephone'   => $tabEstimateInfos->{'site.tabInfos'}->{'contact.telephone'},
        },
        'negotiator.tabInfos' => {
            'name'      => $tabEstimateInfos->{'negotiator.name'},
            'firstName' => $tabEstimateInfos->{'negotiator.firstName'},
            'mobile'    => $tabEstimateInfos->{'negotiator.mobile'}
        },
        'tabLines' => [],
        'currencySymbol' => $currencySymbol,
        'country.insurance.rates' => $tabCountryInfos->{'insurance.rates'}
    };

    foreach my $tabEstimateLineInfos (values(%$tabEstimateLines))
    {
        my $line = {
            'id'        => $tabEstimateLineInfos->{'id'},
            'quantity'  => $tabEstimateLineInfos->{'quantity'},
            'amountType' => $tabEstimateLineInfos->{'amountType'},
            'unitPrice' => $tabEstimateLineInfos->{'rentAmount'},
            'rentTariff.specialReductionPercent' => $tabEstimateLineInfos->{'rentTariff.specialReductionPercent'},
            'duration'   => $tabEstimateLineInfos->{'duration'},
            'beginDate'  => $tabEstimateLineInfos->{'beginDate'},
            'transportTariff.deliveryAmount' => $tabEstimateLineInfos->{'transportTariff.deliveryAmount'},
            'transportTariff.recoveryAmount' => $tabEstimateLineInfos->{'transportTariff.recoveryAmount'},
            'insurance.tabInfos' => {
                'rate'       => $tabEstimateLineInfos->{'insurance.rate'},
                'fullLabel'  => $tabEstimateLineInfos->{'insuranceType.tabInfos'}->{'fullLabel'},
                'flags'      => $tabEstimateLineInfos->{'insurance.flags'},
                'isInvoiced' => $tabEstimateLineInfos->{'insurance.isInvoiced'},
                'isAppealActivated' => $tabEstimateLineInfos->{'insuranceType.tabInfos'}->{'isAppealActivated'},
            },
            'appeal.tabInfos' => undef,
            'cleaning.type.id' => $tabEstimateLineInfos->{'cleaning.type.id'},
            'residues.mode'    => $tabEstimateLineInfos->{'residues.mode'},
            'residues.value'   => $tabEstimateLineInfos->{'residues.value'},
            'model.tabInfos' => {
                'machinesFamily.name' => $tabEstimateLineInfos->{'model.tabInfos'}->{'machinesFamily.name'},
                'machinesFamily.energy' => $tabEstimateLineInfos->{'model.tabInfos'}->{'machinesFamily.energy'},
                'machinesFamily.elevation' => $tabEstimateLineInfos->{'model.tabInfos'}->{'machinesFamily.elevation'},
                'family.id'                => $tabEstimateLineInfos->{'model.tabInfos'}->{'family.id'}
            }
        };

        if (defined $tabEstimateLineInfos->{'appealType.tabInfos'})
        {
            $line->{'appeal.tabInfos'} = {
                'isInvoiced' => ($tabEstimateLineInfos->{'appealType.tabInfos'}->{'mode'} ? 1 : 0),
                'label'      => $tabEstimateLineInfos->{'appealType.tabInfos'}->{'extraLabel'},
                'rate'       => $tabEstimateLineInfos->{'appeal.value'}
            },
        }

        push(@{$tabViewData{'tabData'}->{'tabLines'}}, $line);
    }

    # Récupération de la liste des tarifs de base
    $tabViewData{'tabBaseTariffs'} = &LOC::Tariff::Rent::getBaseTariffs($countryId);


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;

    $tabViewData{'pdfUrl'} = $directory . '/view/rentEstimate/pdf.php';

    require $directory . '/view/rentEstimate/pdf.cgi';
}


# Function: pdfListAction
# Affiche la liste des lignes de devis au format PDF
#
# Parameters:
# string   countryId         - Pays
# string   agencyId          - Agence
# int      customerId        - Identifiant du client
# arrayref estimateLinesIds  - Tableau des identifiants des lignes de devis à imprimer
# int      nbTotal           - Total des lignes de résultats (affichage)
# int      offset            - Début de la recherche (affichage)
# int      limit             - Nombre à afficher (affichage)
sub pdfListAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Récupération des paramètres
    my $countryId   = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $agencyId    = &LOC::Request::getString('agencyId', $tabUserInfos->{'nomadAgency.id'});
    my $customerId  = &LOC::Request::getInteger('customerId', 0);
    my $tabEstimateLinesIds = &LOC::Request::getArray('estimateLinesIds', []);
    my $nbTotal     = &LOC::Request::getInteger('nbTotal', 0);
    my $offset      = &LOC::Request::getInteger('offset', 0);
    my $limit       = &LOC::Request::getInteger('limit', 0);

    my $tabCustomerInfos = &LOC::Customer::getInfos($countryId, $customerId);
    my $tabInsuranceTypesList = &LOC::Insurance::Type::getList($countryId, LOC::Util::GETLIST_ASSOC); 

    my $tabEstimateLinesList = &LOC::Estimate::Rent::getLinesList($countryId, LOC::Util::GETLIST_ASSOC,
                                                              {'id' => $tabEstimateLinesIds},
                                                              LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE_SITE |
                                                              LOC::Estimate::Rent::GETLINEINFOS_MODEL |
                                                              LOC::Estimate::Rent::GETLINEINFOS_INSURANCE |
                                                              LOC::Estimate::Rent::GETLINEINFOS_APPEAL |
                                                              LOC::Estimate::Rent::GETLINEINFOS_EXT_APPEALTYPE |
                                                              LOC::Estimate::Rent::GETLINEINFOS_TARIFFTRSP,
                                                              {'sorter' => 'id'});

    my @tabEstimateLines;
    foreach my $tabEstimateLineInfos (values(%$tabEstimateLinesList))
    {
        my $tabInfos = {
            'estimate.id'   => $tabEstimateLineInfos->{'estimate.id'} * 1,
            'estimate.stateLabel' => $tabEstimateLineInfos->{'estimate.state.label'},
            'model.label'   => $tabEstimateLineInfos->{'model.label'},
            'site'          => {
                'label'          => $tabEstimateLineInfos->{'estimate.site.label'},
                'locality.label' => $tabEstimateLineInfos->{'estimate.site.locality.label'},
            },
            'beginDate'      => $tabEstimateLineInfos->{'beginDate'},
            'endDate'        => $tabEstimateLineInfos->{'endDate'},
            'tariff'         => {
                'unitPrice'      => $tabEstimateLineInfos->{'rentAmount'} * 1,
                'amountType'     => $tabEstimateLineInfos->{'amountType'}
            },
            'duration'       => $tabEstimateLineInfos->{'duration'} * 1,
            'insurance'      => {
                'typeId' => $tabEstimateLineInfos->{'insuranceType.id'},
                'rate'   => $tabEstimateLineInfos->{'insurance.rate'},
                'flags'  => $tabEstimateLineInfos->{'insurance.flags'},
                'appeal' => {
                    'typeId' => $tabEstimateLineInfos->{'appealType.id'},
                    'value'  => $tabEstimateLineInfos->{'appeal.value'},
                    'mode'   => $tabEstimateLineInfos->{'appealType.tabInfos'}->{'mode'}
                },
            },
            'deliveryAmount' => $tabEstimateLineInfos->{'transportTariff.deliveryAmount'} * 1,
            'recoveryAmount' => $tabEstimateLineInfos->{'transportTariff.recoveryAmount'} * 1
        };
        push(@tabEstimateLines, $tabInfos);
    }

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'        => $tabUserInfos->{'locale.id'},
        'user.agency.id'   => $agencyId,
        'currencySymbol'   => $tabUserInfos->{'currency.symbol'},
        'tabResultInfos'   => {
            'from'  => $offset + 1,
            'to'    => ($offset + $limit > $nbTotal ? $nbTotal : $offset + $limit),
            'total' => $nbTotal
        },
        'tabCustomerInfos'      => $tabCustomerInfos,
        'tabEstimateLinesList'  => \@tabEstimateLines,
        'tabInsuranceTypesList' => $tabInsuranceTypesList
    );

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;

    require $directory . '/view/rentEstimate/pdfList.cgi';
}


# Function: uploadDocumentAction
# Dépôt temporaire des documents
sub uploadDocumentAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'localeId' => $tabUserInfos->{'locale.id'},
        'errors'   => undef
    );

    # Récuperation du fichier
    my $file = &LOC::Request::getFile('file');
    # Quand on charge une photo
    if ($file->{'name'} ne '' && $file->{'name'} ne './')
    {
        my $originalFileName = $file->{'name'};

        # Taille de fichier maximale autorisée
        my $maxFileSize = &LOC::Characteristic::getAgencyValueByCode('MAXTAILLEDOC', $tabUserInfos->{'agency.id'}) * 1;

        # Taille du fichier > à la taille Max autorisée
        if ($file->{'size'} > $maxFileSize)
        {
            unlink($file->{'tmp_name'});
            $tabViewData{'errors'} = 'maxSize';
        }
        else
        {
            # On se positionne dans les repertoires
            my $tmpFilesDir = &LOC::Util::getCommonFilesPath('temp/');
            my $tmpFileName = $file->{'tmp_name'};
            $tmpFileName =~ s/\/tmp\///g;

            # On deplace le fichier dans le dossier temporaire
            if (!&File::Copy::move($file->{'tmp_name'}, $tmpFilesDir . $tmpFileName))
            {
                print STDERR "Erreur sur le déplacement de ".$file->{'tmp_name'} . " dans " . $tmpFilesDir . $tmpFileName . " :\n";
                print STDERR $!;
                unlink($file->{'tmp_name'});
                $tabViewData{'errors'} = 'fatalError';
            }

            $tabViewData{'tempName'} = $tmpFileName;
        }
    }
    else
    {
        $tabViewData{'errors'} = 'unknownFile';
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentEstimate/uploadDocument.cgi';
}

# Function: downloadDocumentAction
# Téléchargement d'un document
sub downloadDocumentAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'localeId' => $tabUserInfos->{'locale.id'},
        'errors'   => undef
    );

    # Récupération du fichier
    my $id = &LOC::Request::getInteger('documentId', 0);
    $tabViewData{'mode'} = &LOC::Request::getString('mode', 'download');

    my $tabOptions = {
        'getContents' => 1,
    };

    my $tabInfos = &LOC::Attachments::getInfos($tabUserInfos->{'country.id'}, $id, $tabOptions);
    if ($tabInfos && $tabInfos->{'fileContent'} ne '')
    {
        $tabViewData{'fileInfos'} = $tabInfos;
    }
    else
    {
        $tabViewData{'errors'} = 'unknownFile';
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentEstimate/downloadDocument.cgi';
}


# Function: jsonDocumentsMailUrl
# Génération de l'URL pour la popup d'envoi des mails
sub jsonDocumentsMailUrlAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $countryId = $tabUserInfos->{'country.id'};

    our %tabViewData = ();

    my $tabDocuments   = &LOC::Request::getArray('documents');
    my $recipientMail = &LOC::Request::getString('recipientMail', undef);
    my $estimateLineId = &LOC::Request::getInteger('entityId', 0);
    my $nb = scalar @$tabDocuments;

    if ($nb > 0)
    {
        # Récupération des informations à mettre dans tabAdditionalAttachments
        my @tabAdditionalAttachments;
        my @tabEndActionsContent;
        foreach my $documentInfos (@$tabDocuments)
        {
            my ($docType, $docId) = split(':', $documentInfos->{'id'});
            if ($docType eq 'att')
            {
                push(@tabAdditionalAttachments, {
                    'attachmentId' => $docId,
                    'filename'     => $documentInfos->{'fileName'},
                    'extension'    => $documentInfos->{'extension'}
                });

                push(@tabEndActionsContent, $documentInfos->{'fileName'});
            }
        }

        my $schemaName = &LOC::Db::getSchemaName($countryId);

        $tabViewData{'url'} = &LOC::Request::createRequestUri('print:gui:email', {
            'defaultRecipient'         => $recipientMail,
            'documentAgency.id'        => $tabUserInfos->{'agency.id'},
            'tabAdditionalAttachments' => \@tabAdditionalAttachments,
            'tabEndActions' => [
                {
                    'type'      => 'addTrace',
                    'functionality'   => 'LIGNEDEVIS',
                    'functionalityId' => $estimateLineId,
                    'content'         => join(', ', @tabEndActionsContent),
                    'tabContent'      => {
                        'schema'  => $schemaName,
                        'code'    => LOC::Log::EventType::SENDMAILDOCUMENTS,
                        'old'     => undef,
                        'new'     => undef,
                        'content' => '<%recipient> - <%content>'
                    }
                }
            ]
        });
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentEstimate/jsonDocumentsMailUrl.cgi';
}


# Function: jsonDayExtraInfosAction
# Récupère les dates de début max et date de fin min autorisées sur un document en fonction du jour
sub jsonDayExtraInfosAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $countryId = $tabUserInfos->{'country.id'};

    our %tabViewData = ();

    my $agencyId = &LOC::Request::getString('agencyId');
    my $type     = &LOC::Request::getString('type');
    my $date     = &LOC::Request::getString('date');
    my $appliedToContractId = &LOC::Request::getInteger('appliedToContractId');
    my $valueDate = &LOC::Request::getString('valueDate');

    if ($date)
    {
        my $documentType = ($appliedToContractId ? 'contract' : 'estimateLine');
        $tabViewData{'periodLimitDates'} = &LOC::Day::getPeriodLimitDates($agencyId, $type, $documentType, $date, $valueDate);
    }
    if ($appliedToContractId)
    {
        $tabViewData{'appliedToContractInfos'} = &LOC::Day::getAppliedToContractInfos($countryId, $appliedToContractId, LOC::Day::GETAPPLIEDTOCONTRACTINFOS_DETAILS);
        $tabViewData{'appliedToContractInfos'}->{'url'} = &LOC::Request::createRequestUri('rent:rentContract:view',
                                                                {'contractId' => $appliedToContractId,
                                                                'tabIndex'   => LOC::Day::DEFAULT_CONTRACT_TABINDEX});
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/rentEstimate/jsonDayExtraInfos.cgi';
}


# Function: _getModelInfos
# Récupérer les informations d'un modèle de machine
#
# Parameters:
# string  $countryId  - Pays
# int     $modelId    - Id du modèle de machine
# int     $customerId - Id du client
# string  $agencyId   - Id de l'agence pour les taux d'utilisation
#
# Returns:
# hashref - Informations du modèle de machine
sub _getModelInfos
{
    my ($countryId, $modelId, $customerId, $agencyId) = @_;


    # Récupération des informations du modèle
    my $tabModelInfos = &LOC::Model::getInfos($countryId, $modelId);

    # Taux d'utilisation
    my %tabModelUseRate = &LOC::Model::getUseRate($modelId, $agencyId);
    my %tabTariffFamilyUseRate = &LOC::TariffFamily::getUseRate($tabModelInfos->{'tariffFamily.id'}, $agencyId);


    # Préparation des données à récupérer
    my $tabData = {
        'id'                  => $tabModelInfos->{'id'},
        'label'               => $tabModelInfos->{'label'},
        'fullName'            => $tabModelInfos->{'fullName'},
        'machinesFamilyLabel' => $tabModelInfos->{'machinesFamily.fullName'},
        'tariffFamilyLabel'   => $tabModelInfos->{'tariffFamily.fullName'},
        'isFuelNeeded'        => $tabModelInfos->{'machinesFamily.isFuelNeeded'},
        'fuelQtyEstimByDay'   => $tabModelInfos->{'tariffFamily.estimateFuel'},
        'fuelQtyMax'          => $tabModelInfos->{'tariffFamily.fuelMax'},
        'load'                => $tabModelInfos->{'load'},
        'isCleaningPossible'  => $tabModelInfos->{'isCleaningPossible'}
    };

    if (!$tabModelInfos->{'machinesFamily.isFuelNeeded'})
    {
        $tabData->{'fuelQtyEstimByDay'} = 0;
        $tabData->{'fuelQtyMax'} = 0;
    }

    # Taux d'utilisation
    $tabData->{'tabUseRates'} = [
        [[$tabModelUseRate{'useRate'}->{'agency'}, $tabModelUseRate{'nbTotal'}->{'agency'}],
         [$tabModelUseRate{'useRate'}->{'area'}, $tabModelUseRate{'nbTotal'}->{'area'}],
         [$tabModelUseRate{'useRate'}->{'country'}, $tabModelUseRate{'nbTotal'}->{'country'}]],
        [[$tabTariffFamilyUseRate{'useRate'}->{'agency'}, $tabTariffFamilyUseRate{'nbTotal'}->{'agency'}],
         [$tabTariffFamilyUseRate{'useRate'}->{'area'}, $tabTariffFamilyUseRate{'nbTotal'}->{'area'}],
         [$tabTariffFamilyUseRate{'useRate'}->{'country'}, $tabTariffFamilyUseRate{'nbTotal'}->{'country'}]]
    ];

    # Tarifs de base
    $tabData->{'tabTariffs'} = &LOC::Tariff::Rent::getCustomerTariffs(
                                            $countryId,
                                            $customerId,
                                            {'tariffFamily.id' => $tabModelInfos->{'tariffFamily.id'}});

    # Liste des types de nettoyages associés
    my $tabCleaningTypes = &LOC::Cleaning::getTypesList($countryId, LOC::Util::GETLIST_ASSOC,
                                                        {'energy' => $tabModelInfos->{'machinesFamily.energy'}});
    $tabData->{'tabCleaningTypes'} = [];
    if ($tabModelInfos->{'isCleaningPossible'})
    {
        foreach my $tabClnInfos (values %$tabCleaningTypes)
        {
            push(@{$tabData->{'tabCleaningTypes'}}, {
                'id'         => $tabClnInfos->{'id'} * 1,
                'label'      => $tabClnInfos->{'label'},
                'amount'     => $tabClnInfos->{'amount'} * 1,
                'isStandard' => $tabClnInfos->{'isStandard'} * 1
            });
        }
    }

    # Modèle de type accessoire ?
    $tabData->{'isAccessory'} = $tabModelInfos->{'isAccessory'};

    return $tabData;
}


# Function: _getNewEstimateLineInfos
# Récupérer les informations d'une nouvelle ligne de devis
#
# Parameters:
# string  $countryId    - Pays
# hashref $tabUserInfos - Informations sur l'utilisateur
# string  $agencyId     - Identifiant de l'agence
# hashref $tabError     - Retour d'erreur
#
# Returns:
# hashref - Informations sur la ligne
sub _getNewEstimateLineInfos
{
    my ($countryId, $tabUserInfos, $agencyId, $tabError) = @_;


    # Informations sur le pays
    my $tabCountryInfos = &LOC::Country::getInfos($countryId);

    # Libellé de l'agence du devis
    my $agencyLabel = &LOC::Agency::getLabelById($agencyId);


    my $tabData = {

        'id'                      => undef,

        'isAcceptable'            => 1,
        'isAccepted'              => 0,
        'quantity'                => 1,
        'duration'                => 0,
        'rentAmount'              => 0,
        'rentTotal'               => 0,
        'total'                   => 0,
        'modificationDate'        => '',
        'stateId'                 => LOC::Estimate::Rent::LINE_STATE_ACTIVE,

        'tabModelInfos' => {
            'id'               => 0,
            'tabTariffs'       => {},
            'tabCleaningTypes' => [{'id' => -1, 'label' => 'std', 'amount' => 0, 'isStandard' => 1}]
        },

        'isImperativeModel'       => 0,

        'tabDuration' => {
            'beginDate'               => '',
            'endDate'                 => '',
            'theorEndDate'            => '',
            'calendarDuration'        => 0,

            'days'                    => {
                LOC::Day::TYPE_ADDITIONAL() => {
                    'list'                => [],
                    'nbUnbilled'          => 0,
                    'nbBilled'            => 0,
                    'comment'             => undef,
                    'authorizedPeriod'    => [],
                    'tabDeltasPeriodDays' => &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('INTJRSPDV', $agencyId))
                },
                LOC::Day::TYPE_DEDUCTED() => {
                    'list'                => [],
                    'nbUnbilled'          => 0,
                    'nbBilled'            => 0,
                    'comment'             => undef,
                    'authorizedPeriod'    => [],
                    'tabDeltasPeriodDays' => &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('INTJRSMDV', $agencyId))
                }
            },

            'realDuration'            => 0,
            'isRealDurationChanged'   => 0,
            'isDatesToConfirm'        => 0
         },

        'tabRentTariff' => {
            'amountType'                    => 'day',
            'baseAmount'                    => 0,
            'maxiReductionPercent'          => 0,
            'standardReductionPercent'      => 0,
            'specialReductionPercent'       => 0,
            'specialReductionJustification' => '',
            'specialReductionBillLabel'     => '',
            'specialReductionStatus'        => '',
            'isKeepAmount'                  => 0
        },

        'tabTransport' => {
            'tabTariff' => {
                'id'            => 0,
                'possibilities' => {
                    'estimation' => 1,
                    'delivery' => {
                        'amount' => 1,
                        'self' => 1,
                        'addMachine' => 1
                    },
                    'recovery' => {
                        'amount' => 1,
                        'self' => 1,
                        'addMachine' => 1
                    }
                }
            },
            'tabDelivery' => {
                'amount'             => 0,
                'isAddMachine'       => 0,
                'addMachineLnkCtt'   => undef,
                'isSelf'             => 0,
                'agency'             => {
                    'id'    => $agencyId,
                    'label' => $agencyLabel
                },
                'delegatedAgencyId'  => undef,
                'transferFromAgency' => undef,
                'contract'           => undef,
                'isLoadingDone'      => 0,
                'isUnloadingDone'    => 0,
                'extraInfos'         => {},
                'tabErrors'          => [],
                'action'             => undef,
                'selfFormUrls'       => undef,
                'isSynchroDisabled'  => 0,
                'possibilities'      => {
                    'action' => undef,
                    'edit'   => 1
                }
            },
            'tabRecovery' => {
                'amount'            => 0,
                'isAddMachine'      => 0,
                'addMachineLnkCtt'  => undef,
                'isSelf'            => 0,
                'agency'            => {
                    'id'    => $agencyId,
                    'label' => $agencyLabel
                },
                'delegatedAgencyId' => undef,
                'transferToAgency'  => undef,
                'contract'          => undef,
                'isLoadingDone'     => 0,
                'isUnloadingDone'   => 0,
                'extraInfos'        => {},
                'tabErrors'         => [],
                'action'            => undef,
                'selfFormUrls'      => undef,
                'isSynchroDisabled' => 0,
                'possibilities'     => {
                    'action' => undef,
                    'edit'   => 1
                }
            }
        },

        'tabOtherServices' => {
            'insuranceTypeId'      => undef,
            'insuranceRate'        => undef,
            'appealTypeId'         => undef,
            'appealValue'          => undef,
            'cleaningTypeId'       => 0,
            'cleaningAmount'       => 0,
            'residuesMode'         => undef,
            'residuesValue'        => undef,
            'fuelQuantity'         => undef,
            'fuelUnitPrice'        => $tabCountryInfos->{'fuelUnitPrice'},
            'fuelCurrentUnitPrice' => $tabCountryInfos->{'fuelUnitPrice'}
        },

        'tabRentServices'          => [],

        'tabProforma' => {
            'tabTypes'   => [],
            'tabList'    => [],
            'paidAmount' => 0,
            'isActived'  => 0
        },
        'proformaFlags' => 0,

        'tabDocuments' => [],

        'tabHistory'    => [],

        'convertUrl' => '#',

        'linkedContract' => {
            'code' => '',
            'url'  => '#'
        },

        'tabValorizationUrls' => undef,

        'possibilities' => {
            'duration' => {
                'setInvoiceableDay'    => 1,
                'setNonInvoiceableDay' => 1
            }
        },

        'tabRights' => &LOC::Estimate::Rent::getLineRights($countryId, $agencyId, undef, $tabUserInfos->{'id'})
    };

    # Types de pro formas autorisés
    $tabData->{'tabProforma'}->{'tabTypes'} = &LOC::Proforma::getAuthorizedTypes($countryId, 'estimateLine', 0);

    return $tabData;
}


# Function: _getHistoryInfos
# Récupérer les historiques d'une ligne de devis
#
# Parameters:
# string  $countryId      - Pays
# int     $estimateId     - Identifiant du devis
# int     $estimateLineId - Identifiant de la ligne du devis
# string  $localeId       - Identifiant de la locale pour les traductions
#
# Returns:
# hashref - Liste des historiques
sub _getHistoryInfos
{
    my ($countryId, $estimateId, $estimateLineId, $localeId) = @_;

    # Récupération des historiques
    my @tabHistory     = &LOC::Estimate::Rent::getHistory($countryId, $estimateId, $localeId);
    my @tabLineHistory = &LOC::Estimate::Rent::getLineHistory($countryId, $estimateLineId, $localeId);

    # Fusion des 2 tableaux
    my @tabAllHistory = (@tabHistory, @tabLineHistory);

    # Tri du tableau global
    @tabAllHistory = sort {$b->{'datetime'} cmp $a->{'datetime'}} @tabAllHistory;

    # Construction du tableau adapté à la vue
    my @tab = ();
    foreach my $tabLine (@tabAllHistory)
    {
        push(@tab, {
            'date'         => $tabLine->{'datetime'},
            'entity'       => $tabLine->{'functionality.name'},
            'userName'     => $tabLine->{'user.fullName'},
            'isUserActive' => ($tabLine->{'user.state.id'} ne LOC::User::STATE_INACTIVE ? 1 : 0),
            'event'        => $tabLine->{'eventType.label'},
            'description'  => $tabLine->{'content'}
        });
    }

    return \@tab;
}

# Function: _getDocumentsInfos
# Récupérer les informations des documents joints
#
# Parameters:
# string  $countryId     - Pays
# string  $estimateLineId    - Identifiant de la ligne de devis
#
# Returns:
# hashref - Informations sur les documents
sub _getDocumentsInfos
{
    my ($countryId, $estimateLineId) = @_;

    my $tabDocuments = [];

    # - Documents attachés
    my $tabFilters = {
        'moduleName' => LOC::Attachments::MODULE_RENTESTIMATELINEDOCUMENT,
        'relatedIds' => [$countryId, $estimateLineId]
    };

    my $tabOptions = {
        'sorters' => {
            'creationDate' => 'desc'
        }
    };

    my $tabDocs = &LOC::Attachments::getList($countryId, $tabFilters, $tabOptions);
    if ($tabDocs)
    {
        my $tabStates = {
            LOC::Attachments::STATE_ACTIVE  => 'active',
            LOC::Attachments::STATE_DELETED => 'deleted'
        };
        foreach my $tabDocInfos (@$tabDocs)
        {
            my $tabDocData = {
                'id'          => 'att:' . $tabDocInfos->{'id'},
                'date'        => $tabDocInfos->{'creationDate'},
                'title'       => $tabDocInfos->{'title'},
                'fileName'    => $tabDocInfos->{'fileName'},
                'size'        => $tabDocInfos->{'size'} * 1,
                'extension'   => lc($tabDocInfos->{'fileExtension'}),
                'description' => $tabDocInfos->{'description'},
                'url'         => &LOC::Request::createRequestUri('rent:rentEstimate:downloadDocument', {'documentId' => $tabDocInfos->{'id'}}),
                'isEditable'  => 1,
                'isDeletable' => 1,
                'isExtern'    => 0,
                'origin'      => undef,
                'moduleName'  => $tabDocInfos->{'moduleName'},
                'stateId'     => $tabStates->{$tabDocInfos->{'stateId'}}
            };

            push(@$tabDocuments, $tabDocData);
        }
    }

    # Tri par date
    @$tabDocuments = sort {-($a->{'date'} cmp $b->{'date'})} @$tabDocuments;

    return $tabDocuments;
}



# Function: _getDaysList
# Récupérer la liste des jours supplémentaires ou déduits sur la ligne de devis
#
# Parameters:
# string  $countryId      - Pays
# int     $estimateLineId - Id de la ligne de devis
# string  $type           - Type de jour
#
# Returns:
# arrayref - Tableau des jours +
sub _getDaysList
{
    my ($countryId, $estimateLineId, $type) = @_;

    my $tabDays = [];
    my $tabFilters = {
        'type'           => $type,
        'estimateLineId' => $estimateLineId
    };
    my $tabOptions = {
        'orderBy' => [
            'toComplete:DESC',
            'date:DESC'
        ],
        'getAppliedToContractFlags' => LOC::Day::GETAPPLIEDTOCONTRACTINFOS_DETAILS
    };
    my $tabList= &LOC::Day::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                    $tabFilters,
                                    LOC::Day::GETINFOS_DURATION | LOC::Day::GETINFOS_APPLIEDTOCONTRACT,
                                    $tabOptions);
    if ($tabList)
    {
        foreach my $tabDayInfos (values(%$tabList))
        {
            my $tabAppliedToContractInfos = $tabDayInfos->{'appliedToContract.tabInfos'};
            if ($tabAppliedToContractInfos)
            {
                $tabAppliedToContractInfos->{'url'} = &LOC::Request::createRequestUri('rent:rentContract:view',
                                                                {'contractId' => $tabAppliedToContractInfos->{'id'},
                                                                 'tabIndex'   => LOC::Day::DEFAULT_CONTRACT_TABINDEX});
            }

            my $tabData = {
                'id'                    => $tabDayInfos->{'id'},
                'type'                  => $tabDayInfos->{'type'},
                'appliedToContract'     => $tabAppliedToContractInfos,
                'date'                  => $tabDayInfos->{'date'},
                'invoiceDuration'     => {
                    'id'       => $tabDayInfos->{'invoiceDuration.id'},
                    'label'    => $tabDayInfos->{'invoiceDuration.label'},
                    'duration' => $tabDayInfos->{'invoiceDuration.tabInfos'}->{'duration'}
                },
                'reason'                => ($tabDayInfos->{'reason.id'} ? {
                    'id'    => $tabDayInfos->{'reason.id'},
                    'label' => $tabDayInfos->{'reason.label'}
                } : undef),
                'comment'               => $tabDayInfos->{'comment'},
                'beginDateMax'          => $tabDayInfos->{'beginDateMax'},
                'endDateMin'            => $tabDayInfos->{'endDateMin'},
                'isInvoiceable'         => $tabDayInfos->{'isInvoiceable'},
                'isInvoiced'            => $tabDayInfos->{'isInvoiced'},
                'isMigrated'            => $tabDayInfos->{'isMigrated'},
                'isToComplete'          => (!$tabDayInfos->{'date'} ? 1 : 0),
                'isDocumentClosed'      => 0
            };
            push(@$tabDays, $tabData);
        }
    }

    return $tabDays;
}

# Function: _getProformasInfos
# Récupérer les informations des pro formas
#
# Parameters:
# string  $countryId                - Pays
# int     $estimateLineId           - Id de la ligne de devis
# bool    $isPendingSpcReducsExists - Y'a-t-il des remises exceptionnelles en attentes ?
#
# Returns:
# hashref - Informations sur les pro formas
sub _getProformasInfos
{
    my ($countryId, $estimateLineId, $isPendingSpcReducsExists) = @_;


    my $tabData = {
        'tabTypes'   => [],
        'tabList'    => [],
        'paidAmount' => 0,
        'isActived'  => 0
    };

    # Liste des pro formas associées
    my $tabProformas = &LOC::Proforma::getList($countryId, LOC::Util::GETLIST_ASSOC, {'estimateLineId' => $estimateLineId},
                                               LOC::Proforma::GETINFOS_CUSTOMER |
                                               LOC::Proforma::GETINFOS_ESTIMATELINE |
                                               LOC::Proforma::GETINFOS_INDEX);

    my $tabStates = {
        LOC::Proforma::STATE_ACTIVE   => 'active',
        LOC::Proforma::STATE_REPLACED => 'replaced',
        LOC::Proforma::STATE_ABORTED  => 'aborted'
    };

    foreach my $tabPfmInfos (values %$tabProformas)
    {
        my $tabPfmData = {
            'id'               => $tabPfmInfos->{'id'},
            'date'             => $tabPfmInfos->{'creationDate'},
            'typeId'           => $tabPfmInfos->{'type.id'},
            'typeLabel'        => $tabPfmInfos->{'type.label'},
            'paidAmount'       => $tabPfmInfos->{'paidAmount'},
            'deposit'          => $tabPfmInfos->{'deposit'},
            'paymentModeId'    => $tabPfmInfos->{'paymentMode.id'},
            'paymentModeLabel' => $tabPfmInfos->{'paymentMode.label'},
            'amount'           => $tabPfmInfos->{'amount'},
            'amountWithVat'    => $tabPfmInfos->{'amountWithVat'},
            'total'            => $tabPfmInfos->{'total'},
            'cashed'           => $tabPfmInfos->{'cashed'},
            'remainDue'        => $tabPfmInfos->{'remainDue'},
            'beginDate'        => $tabPfmInfos->{'beginDate'},
            'endDate'          => $tabPfmInfos->{'endDate'},
            'isJustified'      => $tabPfmInfos->{'isJustified'},
            'isValidated'      => $tabPfmInfos->{'isValidated'},
            'validComment'     => $tabPfmInfos->{'validComment'},
            'encashmentId'     => $tabPfmInfos->{'encashment.id'},
            'encashment'       => [$tabPfmInfos->{'encashment.label'}, $tabPfmInfos->{'closure.label'}],
            'fuelQuantity'     => $tabPfmInfos->{'fuelQuantity'},
            'comment'          => $tabPfmInfos->{'comment'},
            'isVatInvoiced'    => $tabPfmInfos->{'isVatInvoiced'},
            'state'            => $tabStates->{$tabPfmInfos->{'state.id'}},
            'isExpired'        => $tabPfmInfos->{'isExpired'},
            'isAbortable'      => $tabPfmInfos->{'isAbortable'},
            'isReactivable'    => $tabPfmInfos->{'isReactivable'},
            'isEditable'       => $tabPfmInfos->{'isEditable'},
            'isPrintable'      => $tabPfmInfos->{'isPrintable'},
            'isPendingSpcReducsExists' => $isPendingSpcReducsExists,
            'modificationDate' => $tabPfmInfos->{'modificationDate'},

            'tabUrls'          => undef
        };

        # Impressions
        my $url = &LOC::Request::createRequestUri('accounting:proforma:pdf', {'proformaId' => $tabPfmInfos->{'id'}});
        $tabPfmData->{'tabUrls'} = {
            'pdf'   => $url,
            'print' => &LOC::Request::createRequestUri('print:gui:print', {
                                                                   'url'           => $url,
                                                                   'tabEndActions' => [
                                                                       {
                                                                           'type'      => 'logPrint',
                                                                           'entity'    => 'PROFORMA',
                                                                           'entityIds' => [$tabPfmInfos->{'id'}]
                                                                       }
                                                                   ]
                                                                }),
            'mail'  => &LOC::Request::createRequestUri('print:gui:email',
                                                       {
                                                           'url'               => $url,
                                                           'filename'          => 'AI_' . $tabPfmInfos->{'type.label'} .
                                                                                  '_' . $tabPfmInfos->{'estimateLine.estimate.code'} .
                                                                                  '_' . $tabPfmInfos->{'estimateLine.index'} .
                                                                                  '_' . $tabPfmInfos->{'indexOnEstimateLine'} . '.pdf',
                                                           'documentAgency.id' => $tabPfmInfos->{'agency.id'},
                                                           'defaultRecipient'  => $tabPfmInfos->{'estimateLine.estimate.orderContact.email'},
                                                           'defaultSubject'    => $tabPfmInfos->{'type.label'},
                                                           'defaultContent'    => &LOC::Characteristic::getAgencyValueByCode('MAILPROFORMA', $tabPfmInfos->{'agency.id'})
                                                       })
        };

        # Déjà versé et reste dû
        $tabData->{'paidAmount'} += $tabPfmInfos->{'cashed'};

        # Dépôt de garantie
        $tabData->{'deposit'} = $tabPfmData->{'deposit'};

        # Onglet activé ?
        if ($tabPfmInfos->{'state.id'} eq LOC::Proforma::STATE_ACTIVE)
        {
            $tabData->{'isActived'} = 1;
        }

        push(@{$tabData->{'tabList'}}, $tabPfmData);
    }
    # Types de pro formas autorisés
    $tabData->{'tabTypes'} = &LOC::Proforma::getAuthorizedTypes($countryId, 'estimateLine', $estimateLineId);

    return $tabData;
}


# Function: _getTransportInfos
# Récupérer les informations du transport
#
# Parameters:
# string  $countryId    - Pays
# string  $agencyId     - Identifiant de l'agence du devis
# hashref $tabLineInfos - Informations sur la ligne de devis
# hashref $tabOptions   - Options supplémentaires (pour la duplication de l'estimation transport)
#
# Returns:
# hashref - Informations sur le transport
sub _getTransportInfos
{
    my ($countryId, $agencyId, $tabLineInfos, $tabOptions) = @_;

    my $tabData = {};

    # Estimation transport
    #---------------------
    my $tabTrspTariffInfos = $tabLineInfos->{'transportTariff.tabInfos'};
    if ($tabTrspTariffInfos->{'id'})
    {
        $tabData->{'tabTariff'} = {
            'id'                       => $tabTrspTariffInfos->{'id'},
            'baseAmount'               => $tabTrspTariffInfos->{'baseAmount'},
            'recoStandardPercent'      => $tabTrspTariffInfos->{'recoStandardPercent'},
            'maxiStandardPercent'      => $tabTrspTariffInfos->{'maxiStandardPercent'},
            'maxiStandardAmount'       => $tabTrspTariffInfos->{'maxiStandardAmount'},
            'specialReductionId'       => $tabTrspTariffInfos->{'specialReductionId'},
            'specialReductionPercent'  => $tabTrspTariffInfos->{'specialReductionPercent'},
            'specialReductionAmount'   => $tabTrspTariffInfos->{'specialReductionAmount'},
            'specialReductionStatus'   => $tabTrspTariffInfos->{'specialReductionStatus'},
            'standardReductionAmount'  => $tabTrspTariffInfos->{'standardReductionAmount'},
            'standardReductionPercent' => $tabTrspTariffInfos->{'standardReductionPercent'},
            'reductionAmount'          => $tabTrspTariffInfos->{'reductionAmount'},
            'reductionPercent'         => $tabTrspTariffInfos->{'reductionPercent'},
            'amount'                   => $tabTrspTariffInfos->{'amount'},
            'customerId'               => $tabTrspTariffInfos->{'customer.id'},
            'zone'                     => $tabTrspTariffInfos->{'zone'},
            'isNoSaved'                => $tabTrspTariffInfos->{'isSaved'},
            'possibilities'            => $tabTrspTariffInfos->{'possibilities'}
        };
    }
    else
    {
        $tabData->{'tabTariff'} = {
            'id'            => 0,
            'possibilities' => $tabTrspTariffInfos->{'possibilities'} || {}
        };
    }



    # Récupération des informations du transport
    #-------------------------------------------

    # LIVRAISON
    #----------
    my $tabTspInfosD = &LOC::Transport::getDocumentDeliveryInfos($countryId, 'estimateLine', $tabLineInfos->{'id'},
                                                                 LOC::Transport::GETINFOS_FROM |
                                                                 LOC::Transport::GETINFOS_ORDER |
                                                                 LOC::Transport::GETINFOS_POSSIBILITIES);

    $tabData->{'tabDelivery'} = {
        'amount'             => $tabTrspTariffInfos->{'deliveryAmount'},
        'isAddMachine'       => $tabTrspTariffInfos->{'isAddMachineDelivery'},
        'addMachineLnkCtt'   => undef,
        'isSelf'             => ($tabTspInfosD ? $tabTspInfosD->{'isSelf'} : $tabTrspTariffInfos->{'isNoDelivery'}),
        'agency'             => {
            'id'    => $tabTspInfosD->{'agency.id'} || $agencyId,
            'label' => $tabTspInfosD->{'agency.label'} || &LOC::Agency::getLabelById($agencyId)
        },
        'delegatedAgencyId'  => $tabTspInfosD->{'delegatedAgency.id'},
        'transferFromAgency' => undef,
        'contract'           => undef,
        'isLoadingDone'      => $tabTspInfosD->{'loading'}->{'isDone'} || 0,
        'isUnloadingDone'    => $tabTspInfosD->{'unloading'}->{'isDone'} || 0,
        'extraInfos'         => {},
        'tabErrors'          => $tabTspInfosD->{'unloading'}->{'tabErrors'} || [],
        'action'             => undef,
        'selfFormUrls'       => undef,
        'isSynchroDisabled'  => $tabTspInfosD->{'isSynchroDisabled'} || 0,
        'possibilities'      => {
            'action' => ($tabTspInfosD->{'unloading'}->{'isDoable'} ?
                            ($tabTspInfosD->{'unloading'}->{'isDone'} ? 'undo' : 'do') : undef),
            'edit'   => $tabTspInfosD->{'possibilities'}->{'edit'} || 0
        }
    };

    # Infos supplémentaires
    if ($tabTspInfosD->{'order.tabInfos'})
    {
        $tabData->{'tabDelivery'}->{'extraInfos'}->{'order'} = {
            'code' => $tabTspInfosD->{'order.tabInfos'}->{'code'}
        };
    }
    if ($tabTspInfosD->{'unloading'}->{'date'})
    {
        $tabData->{'tabDelivery'}->{'extraInfos'}->{'dateDone'} = $tabTspInfosD->{'unloading'}->{'date'};
    }

    # Machine supp
    if ($tabTrspTariffInfos->{'addMachineDeliveryLnkCtt.id'})
    {
        $tabData->{'tabDelivery'}->{'addMachineLnkCtt'} = {
            'id'   => $tabTrspTariffInfos->{'addMachineDeliveryLnkCtt.id'},
            'code' => $tabTrspTariffInfos->{'addMachineDeliveryLnkCtt.code'},
            'url'  => $tabTrspTariffInfos->{'addMachineDeliveryLnkCtt.url'}
        };
    }



    # RECUPERATION
    #-------------
    my $tabTspInfosR = &LOC::Transport::getDocumentRecoveryInfos($countryId, 'estimateLine', $tabLineInfos->{'id'},
                                                                 LOC::Transport::GETINFOS_TO |
                                                                 LOC::Transport::GETINFOS_ORDER |
                                                                 LOC::Transport::GETINFOS_POSSIBILITIES);

    $tabData->{'tabRecovery'} = {
        'amount'            => $tabTrspTariffInfos->{'recoveryAmount'},
        'isAddMachine'      => $tabTrspTariffInfos->{'isAddMachineRecovery'},
        'addMachineLnkCtt'  => undef,
        'isSelf'            => ($tabTspInfosR ? $tabTspInfosR->{'isSelf'} : $tabTrspTariffInfos->{'isNoRecovery'}),
        'agency'            => {
            'id'    => $tabTspInfosR->{'agency.id'} || $agencyId,
            'label' => $tabTspInfosR->{'agency.label'} || &LOC::Agency::getLabelById($agencyId)
        },
        'delegatedAgencyId' => $tabTspInfosR->{'delegatedAgency.id'},
        'transferToAgency'  => undef,
        'contract'          => undef,
        'isLoadingDone'     => $tabTspInfosR->{'loading'}->{'isDone'} || 0,
        'isUnloadingDone'   => $tabTspInfosR->{'unloading'}->{'isDone'} || 0,
        'extraInfos'        => {},
        'tabErrors'         => $tabTspInfosR->{'loading'}->{'tabErrors'} || [],
        'action'            => undef,
        'selfFormUrls'      => undef,
        'isSynchroDisabled' => $tabTspInfosR->{'isSynchroDisabled'} || 0,
        'possibilities'     => {
            'action' => ($tabTspInfosR->{'loading'}->{'isDoable'} ?
                            ($tabTspInfosR->{'loading'}->{'isDone'} ? 'undo' : 'do') : undef),
            'edit'   => $tabTspInfosR->{'possibilities'}->{'edit'} || 0
        }
    };

    # Infos supplémentaires
    if ($tabTspInfosR->{'order.tabInfos'})
    {
        $tabData->{'tabRecovery'}->{'extraInfos'}->{'order'} = {
            'code' => $tabTspInfosR->{'order.tabInfos'}->{'code'}
        };
    }
    if ($tabTspInfosR->{'loading'}->{'date'})
    {
        $tabData->{'tabRecovery'}->{'extraInfos'}->{'dateDone'} = $tabTspInfosR->{'loading'}->{'date'};
    }

    # Machine supp
    if ($tabTrspTariffInfos->{'addMachineRecoveryLnkCtt.id'})
    {
        $tabData->{'tabRecovery'}->{'addMachineLnkCtt'} = {
            'id'   => $tabTrspTariffInfos->{'addMachineRecoveryLnkCtt.id'},
            'code' => $tabTrspTariffInfos->{'addMachineRecoveryLnkCtt.code'},
            'url'  => $tabTrspTariffInfos->{'addMachineRecoveryLnkCtt.url'}
        };
    }
    # La destination est une agence et ce n'est pas la même agence
    if ($tabTspInfosR->{'to.type'} eq LOC::Transport::FROMTOTYPE_AGENCY &&
        $tabTspInfosR->{'to.id'} ne $tabTspInfosR->{'agency.id'})
    {
        $tabData->{'tabRecovery'}->{'transferToAgency'} = {
            'id'    => $tabTspInfosR->{'to.id'},
            'label' => $tabTspInfosR->{'to.tabInfos'}->{'label'}
        };
    }


    return $tabData;
}


# Function: _getEstimateLinesInfos
# Récupérer les informations d'une ou plusieurs lignes de devis
#
# Parameters:
# string   $countryId    - Pays
# hashref  $tabUserInfos - Informations sur l'utilisateur
# arrayref $tabLinesId   - Id de la ligne
# hashref  $tabError     - Retour d'erreur
#
# Returns:
# hashref - Informations sur la ligne
sub _getEstimateLinesInfos
{
    my ($countryId, $tabUserInfos, $tabLinesId, $tabError) = @_;

    if (@$tabLinesId == 0)
    {
        return {};
    }

    my $tabLinesInfos = &LOC::Estimate::Rent::getLinesList($countryId,
                                                          LOC::Util::GETLIST_ASSOC,
                                                          {'id' => $tabLinesId},
                                                          LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE |
                                                          LOC::Estimate::Rent::GETLINEINFOS_ESTIMATE_CUSTOMER |
                                                          LOC::Estimate::Rent::GETLINEINFOS_INDEX |

                                                          LOC::Estimate::Rent::GETLINEINFOS_DURATIONS |
                                                          LOC::Estimate::Rent::GETLINEINFOS_LINKEDCONTRACT |
                                                          LOC::Estimate::Rent::GETLINEINFOS_INSURANCE |
                                                          LOC::Estimate::Rent::GETLINEINFOS_APPEAL |
                                                          LOC::Estimate::Rent::GETLINEINFOS_CLEANING |
                                                          LOC::Estimate::Rent::GETLINEINFOS_FUEL |
                                                          LOC::Estimate::Rent::GETLINEINFOS_RESIDUES |
                                                          LOC::Estimate::Rent::GETLINEINFOS_EXT_RENTSERVICES |
                                                          LOC::Estimate::Rent::GETLINEINFOS_EXT_TARIFFRENT |
                                                          LOC::Estimate::Rent::GETLINEINFOS_EXT_TARIFFTRSP |
                                                          LOC::Estimate::Rent::GETLINEINFOS_CONVERTIBLE |
                                                          LOC::Estimate::Rent::GETLINEINFOS_POSSIBILITIES |
                                                          LOC::Estimate::Rent::GETLINEINFOS_RIGHTS
                                                       );
    if (!$tabLinesInfos)
    {
        $tabError->{'type'} = 'unknown';
        $tabError->{'data'} = {};
        return undef;
    }

    my $tabData = {};
    foreach my $lineId (keys %$tabLinesInfos)
    {
        my $tabLineInfos = $tabLinesInfos->{$lineId};

        my $agencyId = $tabLineInfos->{'estimate.agency.id'};
        my $estimateValueDate = &LOC::Estimate::Rent::getValueDate($countryId, $tabLineInfos->{'estimate.id'});

        # Calcul de la date de fin théorique si elle n'est pas saisie (transition)
        my $theorEndDate = '';
        if ($tabLineInfos->{'beginDate'} ne '' && $tabLineInfos->{'endDate'} eq '')
        {
            $theorEndDate = &LOC::Date::getRentEndDate($tabLineInfos->{'beginDate'},
                                                            $tabLineInfos->{'realDuration'},
                                                            $agencyId);
        }

        # A-t-on des remises exceptionnelles de location ou transport en attentes ?
        my $isPendingSpcReducsExists = ($tabLineInfos->{'rentTariff.tabInfos'}->{'specialReductionStatus'} eq LOC::Tariff::Rent::REDUCTIONSTATE_PENDING ||
                                        $tabLineInfos->{'transportTariff.tabInfos'}->{'specialReductionStatus'} eq LOC::Tariff::Transport::REDUCTIONSTATE_PENDING ? 1 : 0);

        # On ne garde que les informations nécessaires
        my $tabLineData = {

            'id'                      => $tabLineInfos->{'id'},

            'isAcceptable'            => $tabLineInfos->{'isAcceptable'},
            'isAccepted'              => $tabLineInfos->{'isAccepted'},
            'quantity'                => $tabLineInfos->{'quantity'},
            'duration'                => $tabLineInfos->{'duration'},
            'rentAmount'              => $tabLineInfos->{'rentAmount'},
            'rentTotal'               => $tabLineInfos->{'rentTotal'},
            'total'                   => $tabLineInfos->{'total'},
            'modificationDate'        => $tabLineInfos->{'modificationDate'},
            'stateId'                 => $tabLineInfos->{'state.id'},

            'tabModelInfos' => &_getModelInfos($countryId, $tabLineInfos->{'model.id'},
                                                           $tabLineInfos->{'estimate.customer.id'},
                                                           $agencyId),

            'isImperativeModel'       => $tabLineInfos->{'isImperativeModel'},

            'tabDuration' => {
                'beginDate'               => $tabLineInfos->{'beginDate'},
                'endDate'                 => $tabLineInfos->{'endDate'},
                'theorEndDate'            => $theorEndDate,
                'calendarDuration'        => $tabLineInfos->{'calendarDuration'},

                'days'                    => {
                    LOC::Day::TYPE_ADDITIONAL() => {
                        'list'                => &_getDaysList($countryId, $tabLineInfos->{'id'}, LOC::Day::TYPE_ADDITIONAL),
                        'nbUnbilled'          => $tabLineInfos->{'additionalDays'},
                        'nbBilled'            => 0,
                        'comment'             => undef,
                        'authorizedPeriod'    => &LOC::Day::getAuthorizedPeriod($agencyId, LOC::Day::TYPE_ADDITIONAL, 'estimateLine', [$tabLineInfos->{'beginDate'}, $tabLineInfos->{'endDate'}], $estimateValueDate),
                        'tabDeltasPeriodDays' => &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('INTJRSPDV', $agencyId, $estimateValueDate))
                    },
                    LOC::Day::TYPE_DEDUCTED() => {
                        'list'                => &_getDaysList($countryId, $tabLineInfos->{'id'}, LOC::Day::TYPE_DEDUCTED),
                        'nbUnbilled'          => $tabLineInfos->{'deductedDays'},
                        'nbBilled'            => 0,
                        'comment'             => undef,
                        'authorizedPeriod'    => &LOC::Day::getAuthorizedPeriod($agencyId, LOC::Day::TYPE_DEDUCTED, 'estimateLine', [$tabLineInfos->{'beginDate'}, $tabLineInfos->{'endDate'}], $estimateValueDate),
                        'tabDeltasPeriodDays' => &LOC::Json::fromJson(&LOC::Characteristic::getAgencyValueByCode('INTJRSMDV', $agencyId, $estimateValueDate))
                    }
                },

                'realDuration'            => $tabLineInfos->{'realDuration'},
                'isRealDurationChanged'   => $tabLineInfos->{'isRealDurationChanged'},
                'isDatesToConfirm'        => $tabLineInfos->{'isDatesToConfirm'}
            },

            'tabRentTariff' => $tabLineInfos->{'rentTariff.tabInfos'},

            'tabTransport' => &_getTransportInfos($countryId, $agencyId, $tabLineInfos, {}),

            'tabOtherServices' => {
                'insuranceTypeId'      => $tabLineInfos->{'insuranceType.id'},
                'insuranceRate'        => $tabLineInfos->{'insurance.rate'},
                'appealTypeId'         => $tabLineInfos->{'appealType.id'},
                'appealValue'          => $tabLineInfos->{'appeal.value'},
                'cleaningTypeId'       => $tabLineInfos->{'cleaning.type.id'},
                'cleaningAmount'       => $tabLineInfos->{'cleaning.amount'},
                'residuesMode'         => $tabLineInfos->{'residues.mode'},
                'residuesValue'        => $tabLineInfos->{'residues.value'},
                'fuelQuantity'         => $tabLineInfos->{'fuel.quantity'},
                'fuelUnitPrice'        => $tabLineInfos->{'fuel.unitPrice'},
                'fuelCurrentUnitPrice' => $tabLineInfos->{'fuel.currentUnitPrice'}
            },

            'tabRentServices'          => [],

            'tabProforma'   => &_getProformasInfos($countryId, $lineId, $isPendingSpcReducsExists),
            'proformaFlags' => $tabLineInfos->{'proformaFlags'},

            'tabDocuments'  => &_getDocumentsInfos($countryId, $tabLineInfos->{'id'}),

            'tabHistory' => &_getHistoryInfos($countryId, $tabLineInfos->{'estimate.id'}, $lineId, $tabUserInfos->{'locale.id'}),

            'convertUrl' => '#',

            'linkedContract' => undef,

            'tabValorizationUrls' => undef,

            'possibilities' => $tabLineInfos->{'possibilities'},
            'tabRights'     => $tabLineInfos->{'tabRights'}
        };

        # URL de convertion de la ligne en contrat
        if ($tabLineInfos->{'isConvertible'})
        {
            $tabLineData->{'convertUrl'} = &LOC::Request::createRequestUri('rent:rentContract:view',
                                                                           {'estimateLineId' => $tabLineInfos->{'id'}});
        }

        # Contrat lié
        if ($tabLineInfos->{'linkedContract.id'})
        {
            $tabLineData->{'linkedContract'} = {
                'code' => $tabLineInfos->{'linkedContract.code'},
                'url'  => &LOC::Request::createRequestUri('rent:rentContract:view',
                                                          {'contractId' => $tabLineInfos->{'linkedContract.id'}})
            };
        }

        # Liste des services de location et transport
        foreach my $tabRentSrvInfos (values %{$tabLineInfos->{'tabRentServices'}})
        {
            push(@{$tabLineData->{'tabRentServices'}}, {
                'id'               => $tabRentSrvInfos->{'id'},
                'amount'           => $tabRentSrvInfos->{'amount'},
                'comment'          => $tabRentSrvInfos->{'comment'},
                'rentService.id'   => $tabRentSrvInfos->{'rentService.id'},
                'rentService.name' => $tabRentSrvInfos->{'rentService.name'},
                'state.id'         => $tabRentSrvInfos->{'state.id'},
                'state.label'      => $tabRentSrvInfos->{'state.label'},
                'isRemovable'      => $tabRentSrvInfos->{'isRemovable'}
            });
        }
        # Impression du bon de valorisation
        if (!$isPendingSpcReducsExists &&
            $tabLineInfos->{'state.id'} ne LOC::Estimate::Rent::LINE_STATE_INACTIVE &&
            $tabLineInfos->{'beginDate'} ne '' && $tabLineInfos->{'endDate'} ne '' &&
            $tabLineInfos->{'rentTariff.tabInfos'}->{'amountType'} eq 'day') # Le bon de livraison n'est pas disponible si c'est un tarif mensuel (DE-11132)
        {
            my $tabMailInfos = &LOC::Json::fromJson(&LOC::Characteristic::getCountryValueByCode('MAILDEVVALORIZAT', $countryId));
            my $url = &LOC::Request::createRequestUri('accounting:proforma:valorizationPdf', {'documentType' => 'estimateLine', 'documentId' => $lineId});
            my $subject = $tabMailInfos->{'subject'};
            $subject =~ s/<%estimate.code>/$tabLineInfos->{'estimate.code'}/;
            $subject =~ s/<%index>/$tabLineInfos->{'index'}/;

            $tabLineData->{'tabValorizationUrls'} = {
                'pdf'   => $url,
                'print' => &LOC::Request::createRequestUri('print:gui:print', {
                                                                           'url'         => $url,
                                                                           'tabEndActions' => [
                                                                               {
                                                                                   'type'      => 'logPrint',
                                                                                   'entity'    => 'VALORIZATION',
                                                                                   'entityIds' => [$lineId]
                                                                               }
                                                                           ]
                                                                        }),
                'mail'  => &LOC::Request::createRequestUri('print:gui:email',
                                                           {
                                                               'url'               => $url,
                                                               'filename'          => 'AI_BonValorisation_' .
                                                                                      $tabLineInfos->{'estimate.code'} . '_' .
                                                                                      $tabLineInfos->{'index'} . '.pdf',
                                                               'documentAgency.id' => $agencyId,
                                                               'defaultRecipient'  => $tabLineInfos->{'estimate.orderContact.email'},
                                                               'defaultSubject'    => $subject,
                                                               'defaultContent'    => $tabMailInfos->{'content'}
                                                           })
            };
        }

        $tabData->{$lineId} = $tabLineData;
    }

    return $tabData;
}


# Function: _getEstimateInfos
# Récupérer les informations d'un devis
#
# Parameters:
# string  $countryId    - Pays
# hashref $tabUserInfos - Informations sur l'utilisateur
# int     $lineId       - Id du devis
# hashref $tabError     - Retour d'erreur
#
# Returns:
# hashref - Informations sur le devis
sub _getEstimateInfos
{
    my ($countryId, $tabUserInfos, $estimateId, $tabError) = @_;

    # Récupération des informations du devis
    my $tabEstimateInfos = &LOC::Estimate::Rent::getInfos($countryId,
                                                          $estimateId,
                                                          LOC::Estimate::Rent::GETINFOS_BASE |
                                                          LOC::Estimate::Rent::GETINFOS_ERRONEOUSLINES |
                                                          LOC::Estimate::Rent::GETINFOS_PRINTABLE |
                                                          LOC::Estimate::Rent::GETINFOS_NEGOTIATOR |
                                                          LOC::Estimate::Rent::GETINFOS_RIGHTS |
                                                          LOC::Estimate::Rent::GETINFOS_VALUEDATE
                                                 );
    if (!$tabEstimateInfos)
    {
        $tabError->{'type'} = 'unknown';
        $tabError->{'data'} = {
            'country.label' => $tabUserInfos->{'nomadCountry.label'}
        };
        return undef;
    }

    my $countryId = $tabEstimateInfos->{'country.id'};
    my $agencyId  = $tabEstimateInfos->{'agency.id'};


    # Informations client
    my $tabCustomerInfos = &LOC::Customer::getInfos($countryId,
                                                    $tabEstimateInfos->{'customer.id'},
                                                    LOC::Customer::GETINFOS_TARIFF |
                                                    LOC::Customer::GETINFOS_CRC |
                                                    LOC::Customer::GETINFOS_FLAGS);

    # Informations chantier
    my $tabSiteInfos = &LOC::Site::getInfos($countryId, $tabEstimateInfos->{'site.id'});


    # On ne garde que les informations nécessaires
    my $tabData = {
        'id'                    => $tabEstimateInfos->{'id'},
        'country.id'            => $countryId,
        'agency.id'             => $agencyId,
        'customer.id'           => $tabEstimateInfos->{'customer.id'},
        'site.id'               => $tabEstimateInfos->{'site.id'},
        'creator.id'            => $tabEstimateInfos->{'creator.id'},
        'negotiator.id'         => $tabEstimateInfos->{'negotiator.id'},
        'negotiator.fullName'   => $tabEstimateInfos->{'negotiator.fullName'},
        'code'                  => $tabEstimateInfos->{'code'},
        'isFullService'         => $tabEstimateInfos->{'isFullService'},
        'zone'                  => $tabEstimateInfos->{'zone'},

        'tabSite'               => {
            'id'                 => $tabSiteInfos->{'id'},
            'label'              => $tabSiteInfos->{'label'},
            'address'            => $tabSiteInfos->{'address'},
            'localityId'         => $tabSiteInfos->{'locality.id'},
            'localityName'       => $tabSiteInfos->{'locality.name'},
            'localityDepartment' => $tabSiteInfos->{'locality.department'},
            'localityLabel'      => $tabSiteInfos->{'locality.label'},
            'zone'               => $tabEstimateInfos->{'zone'},
            'contactFullName'    => $tabSiteInfos->{'contact.fullName'},
            'contactTelephone'   => $tabSiteInfos->{'contact.telephone'},
            'contactEmail'       => $tabSiteInfos->{'contact.email'},
            'deliveryHour'       => $tabSiteInfos->{'deliveryHour'},
            'deliveryTimeSlotId' => $tabSiteInfos->{'deliveryTimeSlot.id'} || 0,
            'recoveryHour'       => $tabSiteInfos->{'recoveryHour'},
            'recoveryTimeSlotId' => $tabSiteInfos->{'recoveryTimeSlot.id'} || 0,
            'isHasWharf'         => $tabSiteInfos->{'isHasWharf'},
            'isAnticipated'      => $tabSiteInfos->{'isAnticipated'},
            'isAsbestos'         => $tabSiteInfos->{'isAsbestos'},
            'comments'           => $tabSiteInfos->{'comments'}
        },

        'comments'              => $tabEstimateInfos->{'comments'},
        'tracking'              => $tabEstimateInfos->{'tracking'},
        'abortComments'         => $tabEstimateInfos->{'abortComments'},
        'abortReason.id'        => $tabEstimateInfos->{'abortReason.id'},
        'isPrintable'           => $tabEstimateInfos->{'isPrintable'},
        'tabLines'              => $tabEstimateInfos->{'tabLines'},
        'tabErroneousLines'     => $tabEstimateInfos->{'tabErroneousLines'},
        'state.id'              => $tabEstimateInfos->{'state.id'},
        'creationDate'          => $tabEstimateInfos->{'creationDate'},
        'modificationDate'      => $tabEstimateInfos->{'modificationDate'},
        'valueDate'             => $tabEstimateInfos->{'valueDate'},
        'isLocked'              => ($tabCustomerInfos->{'lockLevel'} >= 3 && !$tabCustomerInfos->{'isTempUnlock'} ? 1 : 0),

        'tabRights'             => $tabEstimateInfos->{'tabRights'}
    };

    # TODO: Ne prendre que les informations nécessaires pour le client

    $tabData->{'tabCustomer'} = $tabCustomerInfos;
    $tabData->{'tabCustomer'}->{'orderNo'}                = $tabEstimateInfos->{'orderNo'};
    $tabData->{'tabCustomer'}->{'orderContact.fullName'}  = $tabEstimateInfos->{'orderContact.fullName'};
    $tabData->{'tabCustomer'}->{'orderContact.telephone'} = $tabEstimateInfos->{'orderContact.telephone'};
    $tabData->{'tabCustomer'}->{'orderContact.email'}     = $tabEstimateInfos->{'orderContact.email'};

    return $tabData;
}


# Function: _getNewEstimateInfos
# Récupérer les informations d'un nouveau devis
#
# Parameters:
# string  $countryId    - Pays
# hashref $tabUserInfos - Informations sur l'utilisateur
# string  $agencyId     - Identifiant de l'agence
# hashref $tabError     - Retour d'erreur
#
# Returns:
# hashref - Informations sur le devis
sub _getNewEstimateInfos
{
    my ($countryId, $tabUserInfos, $agencyId, $tabError) = @_;


    my $tabData = {
        'id'                    => undef,
        'country.id'            => $countryId,
        'agency.id'             => $agencyId,
        'customer.id'           => undef,
        'site.id'               => undef,
        'creator.id'            => $tabUserInfos->{'id'},
        'negotiator.id'         => undef,
        'negotiator.fullName'   => '',
        'code'                  => &LOC::Estimate::Rent::generateCode('******', $agencyId, 0, 0),
        'isFullService'         => 0,
        'zone'                  => 0,

        'tabSite'               => {
            'id'                 => undef,
            'label'              => '',
            'address'            => '',
            'localityId'         => undef,
            'localityName'       => '',
            'localityDepartment' => '',
            'localityLabel'      => '',
            'zone'               => undef,
            'contactFullName'    => '',
            'contactTelephone'   => '',
            'contactEmail'       => '',
            'deliveryHour'       => '',
            'deliveryTimeSlotId' => 0,
            'recoveryHour'       => '',
            'recoveryTimeSlotId' => 0,
            'isHasWharf'         => 0,
            'isAnticipated'      => 0,
            'isAsbestos'         => 0,
            'comments'           => ''
        },

        'comments'              => '',
        'tracking'              => '',
        'abortComments'         => '',
        'abortReason.id'        => undef,
        'isPrintable'           => 0,
        'tabLines'              => [],
        'tabErroneousLines'     => [],
        'state.id'              => LOC::Estimate::Rent::STATE_ACTIVE,
        'creationDate'          => undef,
        'modificationDate'      => undef,
        'valueDate'             => &LOC::Date::getMySQLDate(),
        'isLocked'              => 0,

        'tabRights'             => &LOC::Estimate::Rent::getRights($countryId, $agencyId, undef, $tabUserInfos->{'id'})
    };

    # Données pour le bloc client
    $tabData->{'tabCustomer'} = {
        'id'         => 0,
        'name'       => '',
        'country.id' => $countryId,
        'orderNo'    => undef,
        'orderContact.fullName'  => undef,
        'orderContact.telephone' => undef,
        'orderContact.email'     => undef
    };

    return $tabData;
}

1;
