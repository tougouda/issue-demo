use utf8;
use open (':encoding(UTF-8)');

# Package: print
# Contrôleur pour l'impression des devis et des contrats de location
package print;


# Constants: Type de liste
# TYPE_FULL      - Liste de tous les contrats éditables
# TYPE_FIRSTEDIT - Liste des contrats pour première impression
use constant
{
    TYPE_FULL      => 'full',
    TYPE_FIRSTEDIT => 'firstedit'
};


use strict;
use File::Basename;

use LOC::Contract::Rent;


# Function: jsonPrintUrlsAction
# Génère les URL d'impression des contrats sélectionnés
sub jsonPrintUrlsAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    our %tabViewData = (
        'tabUrls' => {
            'print' => '',
            'pdf'   => ''
        }
    );

    my $tabIds = &LOC::Request::getArray('ids');
    my $nbIds = scalar @$tabIds;

    if ($nbIds > 0)
    {
        my $url = &LOC::Request::createRequestUri('rent:rentContract:pdf', {
            'contractsIds' => $tabIds
        });

        $tabViewData{'tabUrls'}->{'print'} = &LOC::Request::createRequestUri('print:gui:print', {
            'url'           => $url,
            'tabEndActions' => [
                {
                    'type' => 'logPrint',
                    'entity'    => 'CONTRAT',
                    'entityIds' => $tabIds
                },
                {
                    'type' => 'printContract',
                    'contractId' => $tabIds
                }
            ]
        });

        $tabViewData{'tabUrls'}->{'pdf'} = &LOC::Request::createRequestUri('print:gui:pdf', {
            'url'         => $url,
            'tabEndActions' => [
                {
                    'type' => 'logPrint',
                    'entity'    => 'CONTRAT',
                    'entityIds' => $tabIds
                },
                {
                    'type' => 'printContract',
                    'contractId' => $tabIds
                }
            ]
        });
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/print/jsonPrintUrls.cgi';
}


# Function: viewFirstPrintRentContractsAction
# Liste des contrats pour première impression
sub viewFirstPrintRentContractsAction
{
    my $tabParameters = $_[0];
    $tabParameters->{'type'} = TYPE_FIRSTEDIT;

    # Appel de la liste des contrats en cours pouvant être imprimés
    &viewPrintableRentContractsAction(@_);
}


# Function : viewPrintableRentContractsAction
# Liste des contrats en cours pouvant être imprimés
sub viewPrintableRentContractsAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $countryId = $tabUserInfos->{'nomadCountry.id'};

    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'user.id'        => $tabUserInfos->{'id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
        'user.email'     => $tabUserInfos->{'email'},
        'type'           => &LOC::Request::getString('type', (defined $tabParameters->{'type'} ?
                                                             $tabParameters->{'type'} : TYPE_FULL)),
        'errors'         => []
    );


    # Récupération de la liste des contrats potentiellement éditables
    my $tabFilters = {
        'agencyId'          => $tabViewData{'user.agency.id'},
        'stateId'           => [LOC::Contract::Rent::STATE_PRECONTRACT, LOC::Contract::Rent::STATE_CONTRACT]
    };
    if ($tabViewData{'type'} eq TYPE_FIRSTEDIT)
    {
        $tabFilters->{'isPrinted'} = 0;
    }
    my %tabContractsList = &LOC::Contract::Rent::getList(
        $countryId,
        LOC::Util::GETLIST_ASSOC,
        $tabFilters,
        LOC::Contract::Rent::GETINFOS_BASE |
            LOC::Contract::Rent::GETINFOS_CUSTOMER |
            LOC::Contract::Rent::GETINFOS_EXT_SITE |
            LOC::Contract::Rent::GETINFOS_EXT_MACHINE |
            LOC::Contract::Rent::GETINFOS_RIGHTS |
            LOC::Contract::Rent::GETINFOS_PRINTABLE
    );

    # Filtrage sur les contrats pour lesquels l'utilisateur a le droit d'éditer le PDF
    my @tabContracts = grep {$_->{'tabRights'}->{'actions'}->{'print'} && $_->{'isPrintable'}} values(%tabContractsList);
    $tabViewData{'tabContracts'} = \@tabContracts;


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/print/viewPrintableRentContracts.cgi';
}

1;