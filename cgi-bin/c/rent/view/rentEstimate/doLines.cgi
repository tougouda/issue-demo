use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Locale;
use LOC::Json;

our %tabViewData;

my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});

foreach my $result (@{$tabViewData{'result'}})
{
    my $tabErrors = $result->{'tabErrors'};
    my @textError;

    if (&LOC::Util::in_array(0x0100, $tabErrors->{'fatal'}))
    {
        push(@textError, $locale->t('Le devis a été modifié par un autre utilisateur. Veuillez recharger la page pour obtenir les dernières modifications')); # Devis déjà modifié
    }
    if (&LOC::Util::in_array(0x1001, $tabErrors->{'fatal'}))
    {
        push(@textError, $locale->t('La ligne de devis a été modifiée par un autre utilisateur. Veuillez recharger la page pour obtenir les dernières modifications')); # Ligne de devis déjà modifiée
    }
    if (&LOC::Util::in_array(0x1004, $tabErrors->{'fatal'}))
    {
        push(@textError, $locale->t('Le nombre de jours ouvrés a changé : Veuillez recalculer la période')); # Le nombre de jours ouvrés a changé
    }
    if (&LOC::Util::in_array(0x1005, $tabErrors->{'fatal'}))
    {
        push(@textError, $locale->t('Les dates de début et de fin ne sont pas renseignées')); 
    }
    if (&LOC::Util::in_array(0x1009, $tabErrors->{'fatal'}))
    {
        push(@textError, $locale->t('Impossible d\'accepter la ligne de devis : l\'horaire de livraison n\'est pas renseigné')); 
    }

    my $countErrors    = @{$tabErrors->{'fatal'}};
    my $countTextError = @textError;

    if ($countErrors > 0 && $countTextError == 0)
    {
        push(@textError, $locale->t('Erreur fatale ! Veuillez contacter le service informatique s.v.p.')); # Erreur fatale
    }

    $result->{'textError'} = join ("\n", @textError);
}



my $view = LOC::Json->new();
print $view->displayHeader();

print $view->encode($tabViewData{'result'});
