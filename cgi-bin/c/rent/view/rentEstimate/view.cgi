use utf8;
use open (':encoding(UTF-8)');

# Id des onglets
use constant {
    # Onglets principaux
    TABINDEX_EQUIPMENT => 0,
    TABINDEX_DURATION  => 1,
    TABINDEX_RENT      => 2,
    TABINDEX_TRANSPORT => 3,
    TABINDEX_SERVICES  => 4,
    TABINDEX_PROFORMA  => 5,
    TABINDEX_DOCUMENTS => 6,
    TABINDEX_HISTORY   => 7,

    # Onglets du chantier
    SITETABINDEX_GENERAL => 0,
    SITETABINDEX_INFOS   => 1,

    # Onglet Durée
    DAY_MAXDISPLAY       => 5,

    # Onglet Documents
    DOC_MAXDISPLAY        => 5,
    DOC_PREFIX_ATTACHMENT => 'att'
};


use strict;


use LOC::Html::Standard;
use LOC::Locale;
use LOC::Json;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'rent/rentEstimate/';


our %tabViewData;
our %tabSearchData;
our $inputsTabIndex = 1;
our $tabIndex;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


my $tabEstimateInfos = $tabViewData{'tabEstimateInfos'};
my $tabEstimateLinesInfos = $tabViewData{'tabEstimateLinesInfos'};
our $currencySymbol = $tabViewData{'currencySymbol'};

# Feuilles de style
$view->addCSSSrc('location.css');

$view->addPackage('popups')
     ->addPackage('tabBoxes')
     ->addPackage('searchboxes')
     ->addPackage('spincontrols')
     ->addPackage('modalwindow')
     ->addPackage('dropdownbuttons');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS|
                  LOC::Html::Standard::DISPFLG_NOTIFS);


my $title = $locale->t('Devis de location');
if ($tabEstimateInfos->{'code'} ne '')
{
    $title = $tabEstimateInfos->{'code'};
}
$view->setPageTitle($title);
$view->setTitle($locale->t('Devis de location'));

$view->addTranslations({
    'answerAreYouSureRemoveLine' => $locale->t('Êtes-vous sûr de vouloir supprimer cette ligne ?'),
    'answerAreYouSureResetLine' => $locale->t('Êtes-vous sûr de vouloir annuler toutes les modifications sur cette ligne de devis'),
    'answerAreYouSureReset' => $locale->t('Êtes-vous sûr de vouloir annuler toutes les modifications sur ce devis ?'),

    'spcReducTypeLabel_RENT' => $locale->t('Location'),
    'spcReducTypeLabel_TRSP' => $locale->t('Transport'),

    'spcReducStateLabel_REM01' => $locale->t('Remise acceptée'),
    'spcReducStateLabel_REM02' => $locale->t('Remise en attente'),
    'spcReducStateLabel_REM03' => $locale->t('Remise annulée'),

    'ignorePendingDocsAndValid'    => $locale->t('Valider le devis en ignorant ces documents'),
    'waitPendingDocsAndValid'      => $locale->t('Attendre et valider le devis'),
    'ignorePendingDocsAndAccept'   => $locale->t('Accepter toutes les lignes du devis en ignorant ces documents'),
    'waitPendingDocsAndAccept'     => $locale->t('Attendre et accepter toutes les lignes du devis'),
    'ignorePendingDocsAndUnaccept' => $locale->t('Annuler l\'acceptation de toutes les lignes du devis en ignorant ces documents'),
    'waitPendingDocsAndUnaccept'   => $locale->t('Attendre et annuler l\'acceptation de toutes les lignes du devis'),

    'ignorePendingDocsAndValidLine'    => $locale->t('Valider la ligne de devis en ignorant ces documents'),
    'waitPendingDocsAndValidLine'      => $locale->t('Attendre et accepter la ligne de devis'),
    'ignorePendingDocsAndAcceptLine'   => $locale->t('Accepter la ligne de devis en ignorant ces documents'),
    'waitPendingDocsAndAcceptLine'     => $locale->t('Attendre et accepter la ligne de devis'),
    'ignorePendingDocsAndUnacceptLine' => $locale->t('Annuler l\'acceptation de la ligne de devis en ignorant ces documents'),
    'waitPendingDocsAndUnacceptLine'   => $locale->t('Attendre et annuler l\'acceptation de la ligne de devis'),
    'ignorePendingDocsAndConvertLine'  => $locale->t('Passer la ligne de devis en contrat en ignorant ces documents'),
    'waitPendingDocsAndConvertLine'    => $locale->t('Attendre et passer la ligne de devis en contrat'),

    'goBackToEstimate' => $locale->t('Revenir sur le devis'),

    'days.details'           => $locale->t('Jour %s : %s %s', '<%type>', '<%date>', '<%appliedToContract>'),
    'days.noDate'            => $locale->t('date non saisie'),
    'days.appliedToContract' => $locale->t('(contrat %s)', '<%appliedToContractId>'),
    'days.type_A'            => $locale->t('supplémentaire'),
    'days.type_D'            => $locale->t('déduit'),

    'transportTariff.personal' => $locale->t('Personnalisée')
});

# Notifications possibles
$view->addNotifications([
    # Demande de déblocage client envoyée
    {
        'id'      => 'unlockCustomerRequest-send',
        'type'    => 'info',
        'message' => $locale->t('La demande de déblocage pour le client a été envoyée.')
    },

    # Erreur demande de déblocage client
    {
        'id'      => 'unlockCustomerRequest-error',
        'type'    => 'error',
        'message' => $locale->t('Une erreur s\'est produite lors de l\'envoi de la demande de déblocage pour le client.')
    }
]);

$view->addJSSrc('
/* Etats du devis */
var RENTESTIMATE_STATE_ACTIVE    = "' . LOC::Estimate::Rent::STATE_ACTIVE . '";
var RENTESTIMATE_STATE_ABORTED   = "' . LOC::Estimate::Rent::STATE_ABORTED . '";
var RENTESTIMATE_STATE_CONTRACT  = "' . LOC::Estimate::Rent::STATE_CONTRACT . '";
/* Etats d\'une ligne de devis */
var RENTESTIMATELINE_STATE_ACTIVE    = "' . LOC::Estimate::Rent::LINE_STATE_ACTIVE . '";
var RENTESTIMATELINE_STATE_CONVERTED = "' . LOC::Estimate::Rent::LINE_STATE_CONVERTED . '";
var RENTESTIMATELINE_STATE_INACTIVE  = "' . LOC::Estimate::Rent::LINE_STATE_INACTIVE . '";

/* Etats des jours +/- */
var DAY_MAXDISPLAY                         = ' . DAY_MAXDISPLAY . ';
var DAY_TYPE_ADDITIONAL                    = "' . LOC::Day::TYPE_ADDITIONAL . '";
var DAY_TYPE_DEDUCTED                      = "' . LOC::Day::TYPE_DEDUCTED . '";
var DAY_FLAG_ADDITIONALDAY                 = "' . LOC::Day::FLAG_ADDITIONALDAY . '";
var DAY_FLAG_DEDUCTEDDAY                   = "' . LOC::Day::FLAG_DEDUCTEDDAY . '";
var DAY_INVOICEDURATION_FLAG_DEDUCTEDDAY   = "' . LOC::Day::InvoiceDuration::FLAG_DEDUCTEDDAY . '";
var DAY_INVOICEDURATION_FLAG_ADDITIONALDAY = "' . LOC::Day::InvoiceDuration::FLAG_ADDITIONALDAY . '";
var DAY_REASON_USEFLAG_DEDUCTEDDAY         = "' . LOC::Day::Reason::USEFLAG_DEDUCTEDDAY . '";
var DAY_REASON_USEFLAG_ADDITIONALDAY       = "' . LOC::Day::Reason::USEFLAG_ADDITIONALDAY . '";
var DAY_REASON_USEFLAG_CONTRACT            = "' . LOC::Day::Reason::USEFLAG_CONTRACT . '";
var DAY_REASON_USEFLAG_ESTIMATE            = "' . LOC::Day::Reason::USEFLAG_ESTIMATE . '";

/* Etats des remises exceptionnelles de location */
var RENTTARIFF_REDUCTIONSTATE_VALIDATED = "' . LOC::Tariff::Rent::REDUCTIONSTATE_VALIDATED . '";
var RENTTARIFF_REDUCTIONSTATE_PENDING   = "' . LOC::Tariff::Rent::REDUCTIONSTATE_PENDING . '";
var RENTTARIFF_REDUCTIONSTATE_REFUSED   = "' . LOC::Tariff::Rent::REDUCTIONSTATE_REFUSED . '";

/* Etats des remises exceptionnelles de transport */
var TRANSPORTTARIFF_REDUCTIONSTATE_VALIDATED = "' . LOC::Tariff::Transport::REDUCTIONSTATE_VALIDATED . '";
var TRANSPORTTARIFF_REDUCTIONSTATE_PENDING   = "' . LOC::Tariff::Transport::REDUCTIONSTATE_PENDING . '";
var TRANSPORTTARIFF_REDUCTIONSTATE_REFUSED   = "' . LOC::Tariff::Transport::REDUCTIONSTATE_REFUSED . '";


var INSURANCE_STATE_ACTIVE  = "' . LOC::Insurance::Type::STATE_ACTIVE . '";
var INSURANCE_STATE_DELETED = "' . LOC::Insurance::Type::STATE_DELETED . '";

var INSURANCEFLAG_ISINVOICED    = "' . LOC::Insurance::Type::FLAG_ISINVOICED . '";
var INSURANCEFLAG_ISINCLUDED    = "' . LOC::Insurance::Type::FLAG_ISINCLUDED . '";
var INSURANCEFLAG_ISCUSTOMER    = "' . LOC::Insurance::Type::FLAG_ISCUSTOMER . '";

var INSURANCEMODE_NOTINVOICED    = "' . LOC::Insurance::Type::MODE_NOTINVOICED . '";
var INSURANCEMODE_CALENDARADDAYS = "' . LOC::Insurance::Type::MODE_CALENDARADDAYS . '";
var INSURANCEMODE_WORKING        = "' . LOC::Insurance::Type::MODE_WORKING . '";
var INSURANCEMODE_CALENDAR       = "' . LOC::Insurance::Type::MODE_CALENDAR . '";

/* Etats des services de location et transport */
var RENTSRV_STATE_UNBILLED = "' . LOC::Estimate::Rent::SERVICESTATE_UNBILLED . '";
var RENTSRV_STATE_BILLED   = "' . LOC::Estimate::Rent::SERVICESTATE_BILLED . '";
var RENTSRV_STATE_DELETED  = "' . LOC::Estimate::Rent::SERVICESTATE_DELETED . '";

/* Mode de calcul de la gestion de recours */
var APPEALMODE_NOTINVOICED    = "' . LOC::Appeal::Type::MODE_NOTINVOICED . '";
var APPEALMODE_CALENDAR       = "' . LOC::Appeal::Type::MODE_CALENDAR . '";

var APPEAL_MODE_NONE    = ' . LOC::Common::APPEAL_MODE_NONE . ';
var APPEAL_MODE_PERCENT = ' . LOC::Common::APPEAL_MODE_PERCENT . ';

/* Mode de calcul de la participation au recyclage */
var RESIDUES_MODE_NONE    = ' . LOC::Common::RESIDUES_MODE_NONE . ';
var RESIDUES_MODE_PRICE   = ' . LOC::Common::RESIDUES_MODE_PRICE . ';
var RESIDUES_MODE_PERCENT = ' . LOC::Common::RESIDUES_MODE_PERCENT . ';

/* Types de pro forma */
var PFMTYPE_ID_ADVANCE    = ' . LOC::Proforma::Type::ID_ADVANCE . ';
var PFMTYPE_ID_PROFORMA   = ' . LOC::Proforma::Type::ID_PROFORMA . ';
var PFMTYPE_ID_EXTENSION  = ' . LOC::Proforma::Type::ID_EXTENSION . ';

/* Flags pro forma */
var PFMDOCFLAG_EXISTACTIVED          = ' . LOC::Proforma::DOCFLAG_EXISTACTIVED . ';
var PFMDOCFLAG_EXISTACTIVEDFINALIZED = ' . LOC::Proforma::DOCFLAG_EXISTACTIVEDFINALIZED . ';
var PFMDOCFLAG_EXISTACTIVEDEXPIRED   = ' . LOC::Proforma::DOCFLAG_EXISTACTIVEDEXPIRED . ';

/* Documents */
var DOC_MAXDISPLAY             = ' . DOC_MAXDISPLAY . ';
var DOC_PREFIX_ATTACHMENT        = "' . DOC_PREFIX_ATTACHMENT . '";

/* Index des onglets */
var TABINDEX_EQUIPMENT = ' . TABINDEX_EQUIPMENT . ';
var TABINDEX_DURATION  = ' . TABINDEX_DURATION . ';
var TABINDEX_RENT      = ' . TABINDEX_RENT . ';
var TABINDEX_TRANSPORT = ' . TABINDEX_TRANSPORT . ';
var TABINDEX_SERVICES  = ' . TABINDEX_SERVICES . ';
var TABINDEX_PROFORMA  = ' . TABINDEX_PROFORMA . ';
var TABINDEX_DOCUMENTS = ' . TABINDEX_DOCUMENTS . ';
var TABINDEX_HISTORY   = ' . TABINDEX_HISTORY . ';


/* Index des onglets du chantier */
var SITETABINDEX_GENERAL = ' . SITETABINDEX_GENERAL . ';
var SITETABINDEX_INFOS   = ' . SITETABINDEX_INFOS . ';

function getDocumentObject()
{
    return Location.rentEstimateObj;
}
function getDocumentTransportObject()
{
    return Location.rentEstimateObj.getActiveLineObject().getTransportObject();
}


function isProformaToGen(tab)
{
    return (tab.lineStateId == RENTESTIMATELINE_STATE_ACTIVE &&
            (
              (tab.isCustomerRequired && tab.totalAmount > 0 && !(tab.flags & PFMDOCFLAG_EXISTACTIVED)) ||
              (tab.flags & PFMDOCFLAG_EXISTACTIVEDEXPIRED)));
}

');


# Inclusion des fichiers JavaScript et CSS des helpers
$view->addCSSSrc('rent/helper/infos.css');

$view->addCSSSrc('rent/helper/customer.css');
$view->addJSSrc('rent/helper/customer.js');

$view->addCSSSrc('rent/helper/site.css');
$view->addJSSrc('rent/helper/site.js');

$view->addCSSSrc('rent/helper/duration.css');
$view->addJSSrc('rent/helper/duration.js');

$view->addCSSSrc('rent/helper/tariff.css');
$view->addCSSSrc('rent/helper/transport.css');
$view->addJSSrc('rent/helper/tariffRent.js');
$view->addJSSrc('rent/helper/transport.js');

$view->addCSSSrc('rent/helper/services.css');
$view->addJSSrc('rent/helper/services.js');

$view->addCSSSrc('rent/helper/proforma.css');
$view->addJSSrc('rent/helper/proforma.js');

$view->addCSSSrc('rent/helper/documents.css');
$view->addJSSrc('rent/helper/documents.js');

$view->addCSSSrc('rent/helper/history.css');
$view->addJSSrc('rent/helper/history.js');

$view->addCSSSrc($dir . 'view.css');
$view->addJSSrc($dir . 'view.js');



# Barre de recherche
require($directory . '/rentEstimate/searchBar.cgi');

# Message de confirmation
my $validBlock = '
<div id="validBlock" class="messageBlock valid" style="display: none;">
    <div class="popup" style="display: block;">
        <div class="content">
            <table>
            <tr>
                <td style="font-weight: bold;" id="success">
                    <span class="estimate">' . $locale->t('Le devis a été enregistré avec succès') . '.</span>
                    <span class="estimateLine">' . $locale->t('La ligne de devis a été enregistrée avec succès') . '.</span>
                </td>
            </tr>
            <tr>
                <td>
                    <ul class="warnings" id="warnings">
                        <li id="warning.general.orderContact">' . htmllocale('Les informations du contact commande sont incomplètes') . '</li>
                        <li id="warning.site.contact">' . htmllocale('Les informations du contact chantier sont incomplètes') . '</li>
                        <li id="warning.documents.add">' . htmllocale('Les documents suivants n\'ont pas été ajoutés') . ':
                            <div class="details"></div>
                        </li>
                        <li id="warning.documents.edit">' . htmllocale('Les documents suivants n\'ont pas été modifiés') . ':
                            <div class="details"></div>
                        </li>
                        <li id="warning.documents.remove">' . htmllocale('Les documents suivants n\'ont pas été supprimés') . ':
                            <div class="details"></div>
                        </li>
                        <li id="warning.documents.duplicate">' . htmllocale('Les documents suivants n\'ont pas été ajoutés') . ':
                            <div class="details"></div>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr class="buttons">
                <td>' .
                $view->displayTextButton($locale->t('Ok'), '',
                                         'return false;',
                                         '', 'l', {'id' => 'validBlockBtn'}) .
                '</td>
            </tr>
            </table>
        </div>
    </div>
</div>';


my $fatalErrorMsg = htmllocale('Erreur fatale !') .
    '<p class="details">' .
    htmllocale('Merci de contacter le support informatique.') .
    '</p>';

# Bouton de réactualisation
if ($tabViewData{'editMode'} ne 'create')
{
    # Bouton "actualiser"
    my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png', 'return false;',
                                              $locale->t('Actualiser le devis de location'),
                                              'l',
                                              {'id' => 'rentEstimateRefreshBtn',
                                               'tabindex' => -1,
                                               'onfocus' => 'this.blur();'
                                              });
    $view->addControlsContent($refreshBtn, 'right', 1);

    my $errorBlockRefreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png', 'return false;',
                                              $locale->t('Actualiser le devis de location'),
                                              'l',
                                              {'id' => 'rentEstimateErrorBlockRefreshBtn'});
    $fatalErrorMsg = htmllocale('Erreur fatale !') .
        '<p class="details">' .
        $locale->t('Veuillez %s le devis et renouveler l\'opération.', $errorBlockRefreshBtn) . '<br>' .
        htmllocale('Si l\'erreur se reproduit, merci de contacter le support informatique.') .
        '</p>';
}

my $addMachMin = $locale->getCurrencyFormat($tabViewData{'addMachineMinPrice'}, 0);
my $errorBlock = '
<div id="errorBlock" class="messageBlock error" style="display: none;">
    <div class="popup" style="display: block;">
        <div class="content">
            <table>
            <tr>
                <td style="font-weight: bold;">' . $locale->t('Le devis n\'a pas été enregistré car les erreurs suivantes ont été détectées') . '&nbsp;:</td>
            </tr>
            <tr>
                <td>
                    <ul class="errors" id="errors">
                    <li id="error.-1"><h3>' . $fatalErrorMsg . '</h3></li>

                    <li id="error.0">' . $locale->t('Une erreur est survenue. Veuillez actualiser le devis et recommencer') . '</li>
                    <li id="error.1">' . $locale->t('Le devis a été modifié par un autre utilisateur. Veuillez recharger le devis pour obtenir les dernières modifications') . '</li>
                    <li id="error.3">' . htmllocale('Certaines informations ne sont pas modifiables') . '</li>
                    <li id="error.5">' . $locale->t('Le devis n\'est pas modifiable') . '</li>
                    <li id="error.15">' . $locale->t('Le négociateur n\'est pas renseigné') . '</li>
                    <li id="error.20">' . $locale->t('Le client est bloqué au devis') . '</li>
                    <li id="error.101">' . $locale->t('Le client n\'est pas renseigné') . '</li>
                    <li id="error.31">' . htmllocale('L\'adresse e-mail du contact commande n\'est pas valide') . '</li>
                    <li id="error.102">' . $locale->t('Le libellé du chantier n\'est pas renseigné') . '</li>
                    <li id="error.103">' . $locale->t('La ville du chantier n\'est pas renseignée') . '</li>
                    <li id="error.104">' . $locale->t('La zone de transport n\'est pas renseignée') . '</li>
                    <li id="error.121">' . htmllocale('L\'adresse e-mail du contact chantier n\'est pas valide') . '</li>
                    <li id="error.109">' . $locale->t('Le commentaire d\'abandon n\'est pas renseigné') .'</li>
                    <li id="error.120">' . $locale->t('La zone n\'est pas modifiable') .'</li>
                    <li id="error.125">' . $locale->t('L\'horaire de livraison est obligatoire lorsque des lignes sont acceptées') .'</li>

                    <li id="error.100"><b>' . $locale->t('Erreur sur les lignes de devis') . ':</b>
                        <ul>
                            <li id="error.105">' . $locale->t('Le modèle de machines n\'est pas renseigné') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.106">' . $locale->t('L\'encombrement n\'est pas renseigné') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.107">' . $locale->t('La durée est incorrecte') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.108">' . $locale->t('Les dates de début et de fin ne sont pas renseignées') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.110">' . $locale->t('Le nombre de jours ouvrés a changé : Veuillez recalculer la période') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.111">' . $locale->t('La date de début ne doit pas être antérieure de plus d\'un mois à la date de saisie') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.112">' . $locale->t('Nombre de ligne maximum dépassé') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.113">' . $locale->t('La ligne de devis a été modifiée par un autre utilisateur. Veuillez recharger la ligne pour obtenir les dernières modifications') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.114">' . $locale->t('Impossible de modifier le type d\'assurance') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.115">' . $locale->t('Impossible de modifier le taux d\'assurance') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.116">' . $locale->t('Le mode de calcul de la participation au recyclage n\'est pas disponible') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.117">' . $locale->t('Le mode de calcul de la gestion de recours n\'est pas disponible') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.130">' . $locale->t('La date de fin est inférieure à la date de début') . '</li>
                            <li id="error.135">' . $locale->t('Impossible d\'accepter la ligne de devis : l\'horaire de livraison n\'est pas renseigné') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.202">' . $locale->t('La justification de la RE n\'est pas renseignée') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.301">' . $locale->t('Pas d\'estimation de transport') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.302">' . $locale->t('La zone a changé, veuillez réestimer le transport') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.303">' . $locale->t('Le client a changé, veuillez réestimer le transport') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.304">' . $locale->t('Le montant de la livraison transport est inférieur à l\'estimation') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.305">' . $locale->t('Le montant de la récupération transport est inférieur à l\'estimation') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.308">' . $locale->t('Le forfait machine supplémentaire aller est inférieur à %s', $addMachMin) . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.309">' . $locale->t('Le forfait machine supplémentaire retour est inférieur à %s', $addMachMin) . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.403">' . $locale->t('Impossible de supprimer une ligne de devis passée en contrat') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.504">' . $locale->t('Impossible de créer une pro forma : Le type de montant de la ligne de devis est mensuel') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.505">' . $locale->t('Action pro forma non autorisée') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.508">' . $locale->t('Impossible de créer une pro forma : La date de fin saisie est antérieure à la date de fin du contrat') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.509">' . $locale->t('Impossible de créer une pro forma : La date de fin saisie est antérieure à la date de fin de la dernière prolongation') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.512">' . $locale->t('Impossible de modifier la pro forma') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.513">' . $locale->t('Impossible d\'abandonner la pro forma') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.515">' . $locale->t('Impossible de réactiver la pro forma') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.517">' . $locale->t('Le montant de la pro forma est nul') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.518">' . $locale->t('Impossible de créer la pro forma : le mode de règlement n\'est pas renseigné') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.519">' . $locale->t('Impossible de créer la pro forma : estimation carburant incorrecte') . '&nbsp;:&nbsp;<span class="lines"></span></li>

                            <li id="error.801">' . $locale->t('Le type du jour n\'est pas renseigné') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.802">' . $locale->t('La date du jour n\'est pas renseignée') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.803">' . $locale->t('Le motif du jour n\'est pas renseigné') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.804">' . $locale->t('Un jour existe déjà sur la date') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.805">' . $locale->t('Le jour n\'est pas dans la période autorisée') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.806">' . $locale->t('La mise à l\'état facturable du jour n\'est pas possible car celui-ci est déjà facturable') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.807">' . $locale->t('La mise à l\'état non facturable du jour n\'est pas possible car celui-ci est déjà non facturable') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.808">' . htmllocale('Le contrat lié n\'est pas valide') . '&nbsp;:&nbsp;<span class="lines"></span></li>
                            <li id="error.days.details">
                                <div></div>
                            </li>
                        </ul>
                    </li>
                    </ul>
                </td>
            </tr>
            <tr class="buttons">
                <td>' .
                $view->displayTextButton($locale->t('Fermer'), '',
                                         '$ge("errorBlock").style.display = "none";',
                                         '', 'l', {'id' => 'errorBlockBtn'}) .
                '</td>
            </tr>
            </table>
        </div>
    </div>
</div>';

$view->addBodyContent($validBlock, 0, 1);
$view->addBodyContent($errorBlock, 0, 1);


# Affichage de l'entête
$view->{'_networkOptimization'} = 'default-rentEstimate-view';
print $view->displayHeader();

print '<div id="rentEstimateBlock" class="loading">';
print '<div class="loadingMask"></div>';
print '<div class="content">';


# Symbole champ requis
my $requiredIcon = $view->displayRequiredSymbol();

# Informations du devis
require($directory . '/helper/infos.cgi');

# Informations du client et du chantier
print '
<table class="alignFieldsetBlocks">
<tr>
    <td style="width: 50%; padding-right: 10px;">
        <table style="width: 100%;">
            <tr>
                <td>';
# Informations du client
require($directory . '/helper/customer.cgi');
print '
                </td>
            </tr>
            <tr>
                <td>';
# Informations de la commande
require($directory . '/helper/order.cgi');
print '
                </td>
            </tr>
        </table>';
print '
    </td>
    <td>';
# Informations du chantier
our $prefix = 'rentSite';
require($directory . '/helper/site.cgi');
print '
    </td>
</tr>
</table>';

# Affichage des lignes du devis
print '
<fieldset>
    <legend>' . $view->displayImage('rent/rentEstimate/icon.png') . '&nbsp;' .
                htmllocale('Lignes de devis') . '</legend>';


my $rentAmountClone = $view->displayFormInputCurrency('rentAmount_clone', 0,
                                                      LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);
my $rentTotalClone = $view->displayFormInputCurrency('rentTotal_clone', 0,
                                                     LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);
my $transportDeliveryAmountClone = $view->displayFormInputCurrency('transportDeliveryAmount_clone', 0,
                                                          LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);
my $transportRecoveryAmountClone = $view->displayFormInputCurrency('transportRecoveryAmount_clone', 0,
                                                          LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);
my $otherServicesAmountClone = $view->displayFormInputCurrency('otherServicesAmount_clone', 0,
                                                     LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);
my $totalClone = $view->displayFormInputCurrency('total_clone', 0,
                                                 LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);


print '
<div class="clonedCurrencies">
' . $rentAmountClone . $rentTotalClone . $transportDeliveryAmountClone . $transportRecoveryAmountClone . $otherServicesAmountClone . $totalClone . '
</div>

<div id="rentEstimateLinesBlock">
<table id="rentEstimateLinesTable" class="basic" oncontextmenu="return false;">
<thead>
    <tr>
        <td class="index"><span></span></td>
        <td class="flag"></td>
        <td class="sep"></td>
        <td class="accept"><span></span></td>
        <td class="sep"></td>
        <td class="modelLabel"><span>' . htmllocale('Modèle') . '</span></td>
        <td class="sep"></td>
        <td class="datesToConfirm"><span title="' . htmllocale('Dates à confirmer') . '"></span></td>
        <td class="beginDate"><span>' . htmllocale('Date de début') . '</span></td>
        <td class="endDate"><span>' . htmllocale('Date de fin') . '</span></td>
        <td class="duration"><span title="' . htmllocale('en jours ouvrés') . '">' . htmllocale('Durée') . '</span></td>
        <td class="sep"></td>
        <td class="rentAmount"><span>' . htmllocale('Prix Loc.') . '</span></td>
        <td class="rentTotal"><span>' . htmllocale('Total Loc.') . '</span></td>
        <td class="sep"></td>
        <td class="transportDeliveryAmount"><span>' . htmllocale('Livraison') . '</span></td>
        <td class="transportRecoveryAmount"><span>' . htmllocale('Récupération') . '</span></td>
        <td class="sep"></td>
        <td class="otherServicesAmount"><span>' . htmllocale('Autres services') . '</span></td>
        <td class="sep"></td>
        <td class="total"><span>' . htmllocale('Total') . '</span></td>
        <td class="sep"></td>
        <td class="proforma"><span>' . htmllocale('Pro forma') . '</span></td>
        <td class="sep"></td>
        <td class="ctrls"></td>
    </tr>
</thead>
<tbody>
    <tr class="clone" id="rentEstimateLineClone">
        <td class="index"><div></div></td>
        <td class="flag">
            <div class="icon">
                <div class="main errors" title="' . htmllocale('Ligne en erreur') . '"></div>
                <div class="main editmode_create" title="' . htmllocale('Ligne en cours de création') . '"></div>
                <div class="main loadstatus_error" title="' . htmllocale('Erreur de chargement') . '"></div>
                <div class="main edited" title="' . htmllocale('Ligne en cours de modification') . '"></div>
                <div class="main removed" title="' . htmllocale('Ligne en cours de suppression') . '"></div>
                <div class="main state_' . LOC::Estimate::Rent::LINE_STATE_CONVERTED . '" title="' . htmllocale('Ligne passée en contrat') . '"></div>
                <div class="main state_' . LOC::Estimate::Rent::LINE_STATE_INACTIVE . '"></div>
                <div class="sub"></div>
            </div>
        </td>
        <td class="sep"></td>
        <td class="accept"><div title="' . htmllocale('Ligne acceptée') . '"></div></td>
        <td class="sep"></td>
        <td class="modelLabel"><div><span></span></div></td>
        <td class="sep"></td>
        <td class="datesToConfirm"><div class="off" title="' . htmllocale('Dates à confirmer') . '"></div></td>
        <td class="beginDate"></td>
        <td class="endDate"></td>
        <td class="duration"><span></span><div class="changed off"></div></td>
        <td class="sep"></td>
        <td class="rentAmount"></td>
        <td class="rentTotal"></td>
        <td class="sep"></td>
        <td class="transportDeliveryAmount"></td>
        <td class="transportRecoveryAmount"></td>
        <td class="sep"></td>
        <td class="otherServicesAmount"></td>
        <td class="sep"></td>
        <td class="total"></td>
        <td class="sep"></td>
        <td class="proforma"><div class="icon">' .
                                '<div class="actived" title="' . htmllocale('Pro forma en attente') . '"></div>' .
                                '<div class="finalized" title="' . htmllocale('Pro forma finalisée') . '"></div>' .
                                '<div class="expired" title="' . htmllocale('Pro forma non à jour') . '"></div>' .
                            '</div>' .
                            '<div class="proformaToGen" title="' . htmllocale('Pro forma à générer') . '"></div>' .
       '</td>
        <td class="sep"></td>
        <td class="ctrls"><a href="#" onclick="LOC_Common.stopEventBubble(event);" target="_blank" class="view-contract action-btn openRentContract"></a></td>
    </tr>';
my $cpt = 1;
my $rowIndex = 2;
$tabEstimateInfos->{'tabLinesPreloadInfos'} = [];
foreach my $lineId (@{$tabEstimateInfos->{'tabLines'}})
{
    my $tabLineInfos = $tabEstimateLinesInfos->{$lineId};

    # Informations préchargées
    my %tabPreloadInfos = (
        'stateId'        => $tabLineInfos->{'state.id'},
        'proformaFlags'  => $tabLineInfos->{'proformaFlags'},
        'total'          => $tabLineInfos->{'total'},
        'isAccepted'     => $tabLineInfos->{'isAccepted'},
        'linkedContract' => undef
    );

    if ($tabLineInfos->{'state.id'} eq LOC::Estimate::Rent::LINE_STATE_CONVERTED)
    {
        $tabPreloadInfos{'linkedContract'} = {
            'code'  => $tabLineInfos->{'linkedContract.code'},
            'url'   => $view->createURL('rent:rentContract:view',
                                       {'contractId' => $tabLineInfos->{'linkedContract.id'}}),
            'title' => $locale->t('Voir le contrat %s', $tabLineInfos->{'linkedContract.code'})
        };
    }

    push(@{$tabEstimateInfos->{'tabLinesPreloadInfos'}}, \%tabPreloadInfos);


    # Remises en attentes
    my $rentReducSubClass = '';
    my $rentReducTitle = '';
    if ($tabLineInfos->{'rentTariff.specialReductionStatus'} ne '')
    {
        $rentReducSubClass .= ' ' . $tabLineInfos->{'rentTariff.specialReductionStatus'};
        $rentReducTitle = $view->getTranslation('spcReducTypeLabel_RENT') . ' : ' .
                          $view->getTranslation('spcReducStateLabel_' . $tabLineInfos->{'rentTariff.specialReductionStatus'});
    }

    my $trspReducSubClass = '';
    my $trspReducTitle = '';
    if ($tabLineInfos->{'transportTariff.specialReductionStatus'} ne '')
    {
        $trspReducSubClass .= ' ' . $tabLineInfos->{'transportTariff.specialReductionStatus'};
        $trspReducTitle = $view->getTranslation('spcReducTypeLabel_TRSP') . ' : ' .
                          $view->getTranslation('spcReducStateLabel_' . $tabLineInfos->{'transportTariff.specialReductionStatus'});
    }

    my $rentAmountInput = $view->displayFormInputCurrency('rentAmount[' . $lineId . ']',
                                                          $tabLineInfos->{'rentAmount'},
                                                          LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);
    my $rentTotalInput = $view->displayFormInputCurrency('rentTotal[' . $lineId . ']',
                                                          $tabLineInfos->{'rentTotal'},
                                                          LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);
    my $delAmountInput = $view->displayFormInputCurrency('transportDeliveryAmount[' . $lineId . ']',
                                                         $tabLineInfos->{'transportTariff.deliveryAmount'},
                                                         LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);
    my $recAmountInput = $view->displayFormInputCurrency('transportRecoveryAmount[' . $lineId . ']',
                                                         $tabLineInfos->{'transportTariff.recoveryAmount'},
                                                         LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);
    my $otherServicesInput = $view->displayFormInputCurrency('otherServicesAmount[' . $lineId . ']',
                                                         $tabLineInfos->{'insurance.amount'} +
                                                         $tabLineInfos->{'appeal.amount'} +
                                                         $tabLineInfos->{'cleaning.amount'} +
                                                         $tabLineInfos->{'residues.amount'} +
                                                         $tabLineInfos->{'fuel.amount'},
                                                         LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);
    my $totalInput = $view->displayFormInputCurrency('total[' . $lineId . ']', $tabLineInfos->{'total'},
                                                     LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);

    # Informations sur les lignes désactivées
    my $toolTipEvts = '';
    if ($tabLineInfos->{'state.id'} eq LOC::Estimate::Rent::LINE_STATE_INACTIVE)
    {
        my $content = '
<table>
<tbody>
    <tr>
        <td colspan="2">' . $view->displayImage('rent/rentEstimate/info.png') . '&nbsp;<b><u>' . htmllocale('Ligne désactivée') . '</u></b>:</td>
    </tr>
    <tr>
        <td>' . htmllocale('Date de création') . ':&nbsp;</td>
        <td><b>' . $locale->getDateFormat($tabLineInfos->{'creationDate'}, LOC::Locale::FORMAT_DATETIME_NUMERIC2) . '</b></td>
    </tr>
    <tr>
        <td>' . htmllocale('Date de désactivation') . ':&nbsp;</td>
        <td><b>' . $locale->getDateFormat($tabLineInfos->{'modificationDate'}, LOC::Locale::FORMAT_DATETIME_NUMERIC2) . '</b></td>
    </tr>
</tbody>
</table>';
        $toolTipEvts = ' onmousemove="' . $view->toHTMLEntities('Location.toolTipsObj.show(\'' . $view->toJSEntities($content) . '\', {corner:\'tl\'});') . '"' .
                       ' onmouseout="Location.toolTipsObj.hide();"';
    }

    print '
    <tr id="rentEstimateLine_' . $lineId . '">
        <td class="index"><div></div></td>
        <td class="flag" ' . $toolTipEvts . '>
            <div class="icon">
                <div class="main errors" title="' . htmllocale('Ligne en erreur') . '"></div>
                <div class="main editmode_create" title="' . htmllocale('Ligne en cours de création') . '"></div>
                <div class="main loadstatus_error" title="' . htmllocale('Erreur de chargement') . '"></div>
                <div class="main edited" title="' . htmllocale('Ligne en cours de modification') . '"></div>
                <div class="main removed" title="' . htmllocale('Ligne en cours de suppression') . '"></div>
                <div class="main state_' . LOC::Estimate::Rent::LINE_STATE_CONVERTED . '" title="' . htmllocale('Ligne passée en contrat') . '"></div>
                <div class="main state_' . LOC::Estimate::Rent::LINE_STATE_INACTIVE . '"></div>
                <div class="sub"></div>
            </div>
        </td>
        <td class="sep"></td>
        <td class="accept"><div title="' . htmllocale('Ligne acceptée') . '"></div></td>
        <td class="sep"></td>
        <td class="modelLabel"><div><span title="' . $view->toHTMLEntities($tabLineInfos->{'model.label'}) . '">' . $tabLineInfos->{'model.label'} . '</span></div></td>
        <td class="sep"></td>
        <td class="datesToConfirm"><div class="' . ($tabLineInfos->{'isDatesToConfirm'} ? 'on' : 'off') . '" title="' . htmllocale('Dates à confirmer') . '"></div></td>
        <td class="beginDate">' . ($tabLineInfos->{'beginDate'} ne '' ? $locale->getDateFormat($tabLineInfos->{'beginDate'}, LOC::Locale::FORMAT_DATE_NUMERIC) : '') . '</td>
        <td class="endDate">' . ($tabLineInfos->{'endDate'} ne '' ? $locale->getDateFormat($tabLineInfos->{'endDate'}, LOC::Locale::FORMAT_DATE_NUMERIC) : '') . '</td>
        <td class="duration"><span>' . $tabLineInfos->{'duration'} . '</span><div class="changed ' . ($tabLineInfos->{'isRealDurationChanged'} ? 'on' :'off') . '" title="' . htmllocale('Le nombre de jours ouvrés a changé') . '"></div></td>
        <td class="sep"></td>
        <td class="rentAmount' . $rentReducSubClass . '" title="' . $rentReducTitle . '">' . $rentAmountInput . '</td>
        <td class="rentTotal">' . $rentTotalInput . '</td>
        <td class="sep"></td>
        <td class="transportDeliveryAmount' . $trspReducSubClass . '" title="' . $trspReducTitle . '">' . $delAmountInput . '</td>
        <td class="transportRecoveryAmount' . $trspReducSubClass . '" title="' . $trspReducTitle . '">' . $recAmountInput . '</td>
        <td class="sep"></td>
        <td class="otherServicesAmount">' . $otherServicesInput . '</td>
        <td class="sep"></td>
        <td class="total">' . $totalInput . '</td>
        <td class="sep"></td>
        <td class="proforma"><div class="icon">' .
                                '<div class="actived" title="' . htmllocale('Pro forma en attente') . '"></div>' .
                                '<div class="finalized" title="' . htmllocale('Pro forma finalisée') . '"></div>' .
                                '<div class="expired" title="' . htmllocale('Pro forma non à jour') . '"></div>' .
                            '</div>' .
                            '<div class="proformaToGen" title="' . htmllocale('Pro forma à générer') . '"></div>' .
       '</td>
        <td class="sep"></td>
        <td class="ctrls"><a href="#" onclick="LOC_Common.stopEventBubble(event);" target="_blank" class="view-contract action-btn openRentContract"></a></td>
    </tr>';
    $cpt++;
    $rowIndex++;
}

my @tabModelsList = ();
while (my($key, $tab) = each(%{$tabViewData{'tabModelsList'}}))
{
    my $label = sprintf('%s (%s, %s, %s)', $tab->{'label'}, $tab->{'manufacturer.label'}, $tab->{'machinesFamily.energy'}, $tab->{'machinesFamily.elevation'});
    push(@tabModelsList, {'value' => $key, 'innerHTML' => $label, '_search' => $tab->{'fullName'}});
}


print '
</tbody>
</table>
<div id="rentEstimateAddLineBtnDiv">
<a href="#" onclick="return false;" id="rentEstimateAddLineBtn">' .
    $view->displayImage('add(|Over).png', $locale->t('Ajouter une ligne sur ce devis')) . '</a>
</div>
</div>
<div id="rentEstimateLineBlock" class="loading">
<div class="loadingMask"></div>
<div class="content">
<div id="rentEstimateLineTabBox">';

# Onglet Matériel
$tabIndex = TABINDEX_EQUIPMENT; # Id de l'onglet
my $tabModelsListAttr = ($tabEstimateInfos->{'customer.id'} ? {} : {'disabled' => 'disabled'});

# Création de l'URL de consultation des stocks
my $tabFilters = {'countryId' => $tabEstimateInfos->{'country.id'}, 'agencyId' => $tabEstimateInfos->{'agency.id'}};
my $stockUrl = &LOC::Globals::get('locationUrl') . '/cgi-bin/index.cgi?';
$stockUrl .= 'locModule=machine&locController=stock&locAction=view';
$stockUrl .= '&countryId=' . $tabEstimateInfos->{'country.id'} . '&agencyId=' . $tabEstimateInfos->{'agency.id'};

# Javascript d'ouverture de la popup
my $openStockUrl = 'var url = "' . $stockUrl . '"; ' .
                   'if ($ge("rentEstimateLineModelId.search").value == "") { ' .
                   'url += "&modelId=" + Location.searchBoxesManager.getSearchBox("rentEstimateLineModelId").getValue(); } ' .
                   'else { url += "&search=" + $ge("rentEstimateLineModelId.search").value; } ' .
                   'Location.popupsManager.getPopup("viewStock").open(url);';

# Bouton stock
my $stockBtnTitle = $locale->t('Consulter le stock de machines pour le modèle de machines sélectionné');
my $stockBtn = $view->displayTextButton('', 'common/stock/view(|Over).png',
                                        $openStockUrl, $stockBtnTitle);
$stockBtn = '<div id="stockBtn">' . $stockBtn . '</div>';


# Aide sur la recherche de modèle de machines
my $modelHelp = $view->displayHelp($locale->t('La recherche de modèle de machines s\'effectue parmi la famille de machines, l\'énergie, l\'élévation et le modèle de machines'));

my $modelInput = $view->displayFormSelect('rentEstimateLineModelId',
                                          \@tabModelsList, {}, 0,
                                          $tabModelsListAttr);
$view->addPageInput({
    'sel'  => '#rentEstimateLineModelId\.search, #rentEstimateLineModelId',
    'get'  => 'Location.searchBoxesManager.getSearchBox("rentEstimateLineModelId")',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});

# Modèle impératif
my $modelImperativeInput = $view->displayFormInputCheckBox('rentEstimateLineIsImperativeModel', 0,
                                                           {'id' => 'rentEstimateLineIsImperativeModel'},
                                                           $locale->t('Modèle impératif'), 1, 0);
$view->addPageInput({
    'sel'  => '#rentEstimateLineIsImperativeModel',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});


print '
    <div title="' . htmllocale('Matériel') . '">
        <table>
        <tr style="vertical-align: top">
            <td style="padding-right: 10px">

                <fieldset class="sub">
                    <legend>' . htmllocale('Matériel demandé') . '</legend>

                    <table class="formTable">
                    <tr>
                        <td class="label">' . $stockBtn . htmllocale('Modèle de machines') . $requiredIcon . $modelHelp . '</td>
                        <td style="white-space: nowrap">' . $modelInput . '</td>
                    </tr>
                    <tr>
                        <td class="label">' . htmllocale('Famille de machines') . '</td>
                        <td id="rentEstimateLineMachinesFamily"></td>
                    </tr>
                    <tr>
                        <td class="label">' . htmllocale('Famille tarifaire') . '</td>
                        <td id="rentEstimateLineTariffFamily"></td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">' . $modelImperativeInput . '</td>
                    </tr>
                    </table>
                </fieldset>
            </td>
            <td id="rentEstimateLineModelUseRateBlock" class="loading">
                <fieldset class="sub">
                    <legend>' . htmllocale('Taux d\'utilisation') . '</legend>

                    <table class="formTable" id="rentEstimateLineModelUseRateTable">
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th colspan="2">' . htmllocale('Modèle') . '</th>
                            <th colspan="2">' . htmllocale('Famille tarifaire') . '</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="label">
                                ' . $view->displayImage('agency.png') . '
                                ' . htmllocale('Agence') . '
                            </td>
                            <td class="useRate">-</td>
                            <td class="nbTotal">-</td>
                            <td class="useRate">-</td>
                            <td class="nbTotal">-</td>
                        </tr>
                        <tr>
                            <td class="label">
                                ' . $view->displayImage('area.png') . '
                                ' . htmllocale('Région') . '
                            </td>
                            <td class="useRate">-</td>
                            <td class="nbTotal">-</td>
                            <td class="useRate">-</td>
                            <td class="nbTotal">-</td>
                        </tr>
                        <tr>
                            <td class="label">
                                ' . $view->displayCountryImage($tabEstimateInfos->{'country.id'}) . '
                                ' . htmllocale('National') . '
                            </td>
                            <td class="useRate">-</td>
                            <td class="nbTotal">-</td>
                            <td class="useRate">-</td>
                            <td class="nbTotal">-</td>
                        </tr>
                    </tbody>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td colspan="2" id="rentEstimateLineQuantityBlock">
                <fieldset class="sub">
                    <legend>' . htmllocale('Quantité') . '</legend>

                    <table class="formTable">
                    <tr>
                        <td class="label">' . htmllocale('Quantité') . '</td>
                        <td>' . $view->displayFormInputText('rentEstimateLineQuantity', 0) .
                        $view->displayJSBlock('Location.spinControlsManager.createSpinControl("rentEstimateLineQuantity", 0, null, 1, 10, 0);') . '
                        </td>
                    </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        </table>
    </div>';

# Création de la searchBox du modèle de machine et déclaration de la popup de consultation du stock
print $view->displayJSBlock('
Location.searchBoxesManager.createSearchBox("rentEstimateLineModelId");
Location.popupsManager.createPopup("viewStock", "", "center", "center", 900, 550);');

# Onglet Durée
$tabIndex = TABINDEX_DURATION; # Id de l'onglet
our $prefix = 'durationHlp_';
require($directory . '/helper/duration.cgi');
require($directory . '/helper/format/duration.cgi');


# Onglet Location
$tabIndex = TABINDEX_RENT; # Id de l'onglet
our $prefix = 'rentTariffHlp_';
require($directory . '/helper/tariffRent.cgi');

# Onglet Transport
$tabIndex = TABINDEX_TRANSPORT; # Id de l'onglet
our $prefix = 'transportHlp_';
require($directory . '/helper/transport.cgi');
require($directory . '/helper/format/transport.cgi');


# Onglet Autres services
$tabIndex = TABINDEX_SERVICES; # Id de l'onglet
our $prefix = 'servicesHlp_';
require($directory . '/helper/services.cgi');


# Onglet Pro forma
$tabIndex = TABINDEX_PROFORMA; # Id de l'onglet
our $prefix = 'proformaHlp_';
require($directory . '/helper/proforma.cgi');
require($directory . '/helper/format/proforma.cgi');

# Onglet Document
$tabIndex = TABINDEX_DOCUMENTS; # Id de l'onglet
our $prefix = 'documentsHlp_';
require($directory . '/helper/documents.cgi');
require($directory . '/helper/format/documents.cgi');

# Onglet Historique
$tabIndex = TABINDEX_HISTORY; # Id de l'onglet
our $prefix = 'historyHlp_';
require($directory . '/helper/history.cgi');
require($directory . '/helper/format/history.cgi');


print '
</div>
</div>
</div>';


# Boutons pour la gestion de la ligne du devis
my @tabLeftBtns = ();
my @tabMiddleBtns = ();
my @tabRightBtns = ();
push(@tabLeftBtns, $view->displayTextButton(
                                    $locale->t('Valider'),
                                    'valid(|Over).png',
                                    'return false;',
                                    $locale->t('Valider les modifications effectuées sur la ligne du devis'),
                                    'l',
                                    {'id' => 'rentEstimateLineValidBtn',
                                     'class' => 'locCtrlButton disabled',
                                     'tabindex' => -1,
                                     'onfocus' => 'this.blur();'
                                    }));
push(@tabLeftBtns, $view->displayTextButton(
                                    $locale->t('Rétablir'),
                                    'revert(|Over).png',
                                    'return false;',
                                    $locale->t('Annuler les modifications effectuées sur la ligne du devis'),
                                    'l',
                                    {'id' => 'rentEstimateLineResetBtn',
                                     'class' => 'locCtrlButton disabled',
                                     'tabindex' => -1,
                                     'onfocus' => 'this.blur();'
                                    }));
push(@tabLeftBtns, $view->displayTextButton(
                                    $locale->t('Supprimer'),
                                    'delete(|Over).png',
                                    'return false;',
                                    $locale->t('Supprimer cette ligne du devis'),
                                    'l',
                                    {'id' => 'rentEstimateLineRemoveBtn',
                                     'class' => 'locCtrlButton disabled',
                                     'tabindex' => -1,
                                     'onfocus' => 'this.blur();'
                                    }));
push(@tabLeftBtns, $view->displayTextButton(
                                    $locale->t('Dupliquer'),
                                    'copy(|Over).png',
                                    'return false;',
                                    $locale->t('Dupliquer cette ligne du devis'),
                                    'l',
                                    {'id' => 'rentEstimateLineDuplicateBtn',
                                     'class' => 'locCtrlButton disabled',
                                     'tabindex' => -1,
                                     'onfocus' => 'this.blur();'
                                    }));

push(@tabRightBtns,
    '<div id="rentEstimateLineActionBtns" class="actionButtons cpanel_grp">' .
        '<label>' . &htmllocale('Actions') . '&nbsp;:</label>' .
        $view->displayTextButton(
                                '',
                                $dir . 'acceptBtn(|Over).png',
                                'return false;',
                                $locale->t('Accepter la ligne du devis'),
                                'l',
                                {'id'     => 'rentEstimateLineAccept',
                                 'class'  => 'locCtrlButton',
                                 'tabindex' => -1,
                                 'onfocus' => 'this.blur();'
                                }) .
        $view->displayTextButton(
                                '',
                                $dir . 'cancelAcceptBtn(|Over).png',
                                'return false;',
                                $locale->t('Annuler l\'acceptation de la ligne du devis'),
                                'l',
                                {'id'     => 'rentEstimateLineCancelAccept',
                                 'class'  => 'locCtrlButton',
                                 'tabindex' => -1,
                                 'onfocus' => 'this.blur();'
                                }) .
        $view->displayTextButton(
                                '',
                                '',
                                'javascript:void(0)',
                                $locale->t('Passer la ligne du devis en contrat'),
                                'l',
                                {'id'     => 'rentEstimateLineToContractBtn',
                                 'class'  => 'locCtrlButton action-btn convertIntoRentContract',
                                 'tabindex' => -1,
                                 'onfocus' => 'this.blur();'
                                }) .
    '</div>');

# Impression du bon de valorisation
push(@tabRightBtns,
    '<div id="rentEstimateLineVlzPrintBtns" class="cpanel_grp cpanel_sep_l">' .
        '<label>' . &htmllocale('Bon de valorisation') . '&nbsp;:</label>' .
        $view->displayPrintButtons(
                                   'valorizationPrintBtns',
                                   '',
                                   '',
                                   '',
                                   '',
                                   '',
                                   '',
                                   {'buttons' => ['mail', 'pdf', 'print']}
                                  ) .
    '</div>');

print $view->displayControlPanel({'left' => \@tabLeftBtns, 'middle' => \@tabMiddleBtns, 'right' => \@tabRightBtns, 'substyle' => 'middle', 'separator' => ''});

print '
</fieldset>';


# Création de la boîte à onglet
print $view->displayJSBlock('
Location.tabBoxesManager.createTabBox("rentEstimateLineTabBox", null, "255px");
function setDocumentActiveTabIndex(index)
{
    return Location.tabBoxesManager.getTabBox("rentEstimateLineTabBox").setActiveTabIndex(index);
}');


# Informations complémentaires et abandon de devis
print '
<table class="alignFieldsetBlocks">
<tr>
    <td style="width: 50%;">';
# Informations complémentaires
require($directory . '/helper/comments.cgi');
print '
    </td>
    <td>';


# Abandon de devis
my $isActive = ($tabViewData{'editMode'} eq 'update');

# Création du tableau pour le select du motif d'abandon
my @tabAbortReasons = ();
push(@tabAbortReasons, {'value' => 0, 'innerHTML' => $locale->t('-- Aucun --'), '_requiredcom' => 0});

while (my($key, $tab) = each(%{$tabViewData{'tabAbortReasons'}}))
{
    push(@tabAbortReasons, {'value' => $key, 'innerHTML' => $tab->{'label'},
                            '_requiredcom' => ($tab->{'requiredComments'} ? 1 : 0)});
}

my $reasonInput = $view->displayFormSelect('rentEstimateAbortReasonId', \@tabAbortReasons, 0, 0, {'style' => 'max-width: 350px;'});
$view->addPageInput({'sel'  => '#rentEstimateAbortReasonId'});

my $commentsInput = $view->displayFormTextArea('rentEstimateAbortComments',
                               '',
                               {'style' => 'width: 350px !important; height: 78px !important;',
                                'maxlength' => 255});
$view->addPageInput({'sel'  => '#rentEstimateAbortComments'});

print '
<fieldset id="rentAbortFieldset" class="' . ($isActive ? '' : 'disabled') . '">
    <legend>' . $view->displayImage('rent/helper/abort/icon' . ($isActive ? '' : 'Disabled') . '.png') . '&nbsp;' .
                $locale->t('Abandon de devis') . '</legend>
    <table class="formTable">
    <tr>
        <td class="label">' . $locale->t('Motif') . '</td>
        <td>' . $reasonInput . '</td>
    </tr>
    <tr>
        <td class="label">' . $locale->t('Commentaires d\'abandon') . '</td>
        <td>' . $commentsInput . '</td>
    </tr>
    </table>
</fieldset>';

print '
    </td>
</tr>
</table>';




# Boutons de gestion du devis
my @tabLeftBtns = ();
my @tabMiddleBtns = ();
my @tabRightBtns = ();
push(@tabLeftBtns, $view->displayTextButton(
                                    $locale->t('Valider'),
                                    'valid(|Over).png',
                                    'return false;',
                                    $locale->t('Valider les modifications effectuées sur le devis'),
                                    'l',
                                    {'id' => 'rentEstimateValidBtn',
                                     'class' => 'locCtrlButton disabled',
                                     'tabindex' => -1,
                                     'onfocus' => 'this.blur();'
                                    }
                            ));

push(@tabLeftBtns, $view->displayTextButton(
                                    $locale->t('Rétablir'),
                                    'revert(|Over).png',
                                    'return false;',
                                    $locale->t('Annuler les modifications effectuées sur le devis'),
                                    'l',
                                    {'id' => 'rentEstimateResetBtn',
                                     'class' => 'locCtrlButton',
                                     'tabindex' => -1,
                                     'onfocus' => 'this.blur();'
                                    }
                                    ));

push(@tabRightBtns,
    '<div id="rentEstimateActionBtns" class="actionButtons cpanel_grp">' .
        '<label>' . &htmllocale('Actions') . '&nbsp;:</label>' .
        $view->displayTextButton(
                                '',
                                $dir . 'acceptAllBtn(|Over).png',
                                'return false;',
                                $locale->t('Accepter toutes les lignes du devis'),
                                'l',
                                {'id'     => 'rentEstimateAcceptAll',
                                 'class'  => 'locCtrlButton',
                                 'tabindex' => -1,
                                 'onfocus' => 'this.blur();'
                                }) .
        $view->displayTextButton(
                                '',
                                $dir . 'cancelAcceptAllBtn(|Over).png',
                                'return false;',
                                $locale->t('Annuler l\'acceptation de toutes les lignes du devis'),
                                'l',
                                {'id'     => 'rentEstimateCancelAcceptAll',
                                 'class'  => 'locCtrlButton',
                                 'tabindex' => -1,
                                 'onfocus' => 'this.blur();'
                                }) .
    '</div>');

if ($tabViewData{'editMode'} eq 'update')
{
    my $pdfUrl = {'url' => 'rent:rentEstimate:pdf', 'options' => {'estimateId' => $tabEstimateInfos->{'id'}}};
    my $pdfName = $locale->t('AI_Devis_%s.pdf', $tabEstimateInfos->{'code'});

    my $tabTracesParams = {
                           'print' => {},
                           'pdf'   => {},
                           'mail'  => {}
                          };
    push(@tabRightBtns,
    '<div id="rentEstimatePrintBtns" class="cpanel_grp cpanel_sep_l">' .
        '<label>' . &htmllocale('Devis') . '&nbsp;:</label>' .
        $view->displayPrintButtons(
                                   'printBtns',
                                   $pdfUrl,
                                   $pdfName,
                                   $tabEstimateInfos->{'tabCustomer'}->{'orderContact.email'},
                                   $locale->t('Devis') . ' : ' . join (' / ', $tabEstimateInfos->{'tabCustomer'}->{'name'},
                                                                              $tabEstimateInfos->{'tabSite'}->{'label'},
                                                                              $tabEstimateInfos->{'tabSite'}->{'localityDepartment'} . ' ' .
                                                                              $tabEstimateInfos->{'tabSite'}->{'localityName'}),
                                   $tabViewData{'emailContent'},
                                   $tabEstimateInfos->{'agency.id'},
                                   {
                                       'traces' => $tabTracesParams,
                                       'buttons' => ['mail', 'pdf', 'print'],
                                       'tabEndActions' => [
                                           {
                                               'type'      => 'logPrint',
                                               'entity'    => 'DEVIS',
                                               'entityIds' => [$tabEstimateInfos->{'id'}]
                                           },
                                           {
                                               'type'     => 'printEstimate',
                                               'entity'   => 'estimate',
                                               'entityId' => $tabEstimateInfos->{'id'}
                                           }
                                       ],
                                       'tabAdditionalAttachments' => $tabViewData{'additionalAttachments'}
                                   }
                                  ) .
    '</div>');
}

print $view->displayControlPanel({'left' => \@tabLeftBtns, 'middle' => \@tabMiddleBtns, 'right' => \@tabRightBtns, 'substyle' => 'bottom', 'separator' => ''});
print '<br />';


# Formatage des données pour les nouvelles lignes de devis
# - Formatage des données de l'onglet Durée
&hlpFormatDurationData($tabViewData{'tabNewLineInfos'}->{'tabDuration'}, $locale, {});

# - Formatage des données de l'onglet Transport
&hlpFormatTransportData($tabViewData{'tabNewLineInfos'}->{'tabTransport'}, $locale, {});

# - Formatage des données de l'onglet Pro forma
&hlpFormatProformaData($tabViewData{'tabNewLineInfos'}->{'tabProforma'}, $locale, {});

# - Formatage des données de l'onglet Historique
&hlpFormatHistoryData($tabViewData{'tabNewLineInfos'}->{'tabHistory'}, $locale, {'timeZone' => $tabViewData{'timeZone'}});



# Liste des plages horaires de transport
my $tabTransportTimeSlots = [];
while (my ($id, $tabInfos) = each(%{$tabViewData{'tabTransportTimeSlots'}}))
{
    push(@$tabTransportTimeSlots, {
        'id'       => $id,
        'isActive' => (($tabInfos->{'state.id'} eq LOC::Transport::TimeSlot::STATE_ACTIVE) ? 1 : 0),
        'label'    => $tabInfos->{'label'}
    });
}

# Définition du tableau des types de montant
my $tabAmountTypes = [
    {'label' => $locale->t('par jour'), 'id' => 'day'},
    {'label' => $locale->t('par mois'), 'id' => 'month'}
];


# Initialisation du devis
my $tabEstimateCfgs = {
    'tabInfos'                  => $tabEstimateInfos,

    'tabNewLineInfos'           => $tabViewData{'tabNewLineInfos'},
    'nbLinesMax'                => $tabViewData{'nbLinesMax'},
    'activeLineId'              => $tabViewData{'estimateLineId'},
    'modelInfosUrl'             => $view->createURL('rent:rentEstimate:modelInfos'),
    'rentalPeriodInfosUrl'      => $view->createURL('rent:rentEstimate:rentalPeriodInfos'),
    'rentalPeriodEndDateMode'   => 'days',
    'loadLinesUrl'              => $view->createURL('rent:rentEstimate:loadLines'),
    'saveUrl'                   => $view->createURL('rent:rentEstimate:save'),
    'getUrlUrl'                 => $view->createURL('rent:rentEstimate:getUrl'),
    'transportWebServicesUrl'   => &LOC::Globals::get('tariffTrspWebServicesUrl'),
    'transportEstimationUrl'    => $tabViewData{'tariffTrspEstimationUrl'},
    'addMachinePrice'           => $tabViewData{'addMachinePrice'},
    'addMachineMinPrice'        => $tabViewData{'addMachineMinPrice'},
    'tempId'                    => $tabViewData{'tempId'},
    'isTrspTarification'        => $tabViewData{'isTrspTarification'},
    'defaultInsuranceRate'      => $tabViewData{'defaultInsuranceRate'},
    'defaultInsuranceTypeId'    => $tabViewData{'defaultInsuranceTypeId'},
    'tabInsuranceTypesList'     => $tabViewData{'insurance.types'},
    'defaultAppealTypeId'       => $tabViewData{'defaultAppealTypeId'},
    'defaultAppealMode'         => $tabViewData{'defaultAppealMode'},
    'defaultAppealRate'         => $tabViewData{'defaultAppealRate'},
    'tabAppealTypesList'        => $tabViewData{'appeal.types'},
    'defaultResiduesMode'       => $tabViewData{'defaultResiduesMode'},
    'useResiduesForAccessories' => $tabViewData{'useResiduesForAccessories'},
    'defaultResiduesAmount'     => $tabViewData{'defaultResiduesAmount'},
    'defaultResiduesRate'       => $tabViewData{'defaultResiduesRate'},
    'defaultStdReducMode'       => $tabViewData{'defaultStdReducMode'},
    'proformaAdvancePercent'    => $tabViewData{'proformaAdvancePercent'},
    'fuelUnitPrice'             => $tabViewData{'fuelUnitPrice'},
    'tabAmountTypes'            => $tabAmountTypes,
    'tabTransportTimeSlots'     => $tabTransportTimeSlots,
    'tabIndex'                  => $tabViewData{'tabIndex'},
    'customerInfosUrl'          => $view->createURL('accounting:customer:jsonInfos', {'flags' => LOC::Customer::GETINFOS_TARIFF |
                                                                                                 LOC::Customer::GETINFOS_CRC |
                                                                                                 LOC::Customer::GETINFOS_FLAGS}),
    'tabDayReasonsList'         => $tabViewData{'tabDayReasonsList'},
    'tabDayInvoiceDurationsList'=> $tabViewData{'tabDayInvoiceDurationsList'},
    'dayExtraInfosUrl'          => $view->createURL('rent:rentEstimate:jsonDayExtraInfos'),
    'maxFileSize'               => $tabViewData{'maxFileSize'},
    'maxMailAttachmentsSize'    => $tabViewData{'maxMailAttachmentsSize'},
    'uploadDocumentUrl'         => $view->createURL('rent:rentEstimate:uploadDocument'),
    'generateDocumentsMailUrl'  => $view->createURL('rent:rentEstimate:jsonDocumentsMailUrl')
};

$view->addBodyEndCode($view->displayJSBlock('
Location.rentEstimateObj = new Location.RentEstimate("' . $tabEstimateInfos->{'country.id'} . '", ' .
                                                          ($tabEstimateInfos->{'id'} ? $tabEstimateInfos->{'id'} : 'null') . ', ' .
                                                          &LOC::Json::toJson($tabEstimateCfgs) . ');'));


# Affichage du pied de page
print '</div></div>';

print $view->displayFooter();

