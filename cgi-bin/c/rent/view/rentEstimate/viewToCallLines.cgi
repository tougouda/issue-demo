use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;

# Répertoire CSS/JS/Images
my $dir = 'rent/rentEstimate/';


our %tabViewData;


my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
my $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


# URL courante 
my $currentUrl = $view->createURL('rent:rentEstimate:viewToCallLines');


# Feuilles de style
$view->addCSSSrc('location.css');

$view->addCSSSrc($dir . 'viewToCallLines.css');

# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc('jQuery/plugins/tablesorter.js');
$view->addJSSrc($dir . 'viewToCallLines.js');


$view->addBodyEvent('onload',
                    'init("' . $tabViewData{'country.id'} . '", 
                          "' . $view->createURL('rent:rentEstimate:doLines') . '", 
                          ' . $tabViewData{'tabEstimateLinesLite'} . ');');
                              
$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);


my $title = $locale->t('Alerte des lignes de devis à rappeler');
$view->setPageTitle($title);
$view->setTitle($title);


# Bouton "Actualiser"
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png', $currentUrl, $locale->t('Actualiser la page'));
$view->addControlsContent($refreshBtn, 'right', 1);


# Affichage de l'entête
print $view->displayHeader();


# On n'affiche pas le tableau si il est vide
my @tabEstimateLines = values %{$tabViewData{'tabEstimateLines'}};
if (@tabEstimateLines == 0)
{
    # Pas de résultat
    print '<div class="noResult">' . $view->displayMessages('info', [$locale->t('Aucune ligne de devis n\'est à rappeler')], 0) . '</div>';
}
else
{
    # Liste des états de remises exceptionnelles
    my $tabRentReducStates = {
        LOC::Tariff::Rent::REDUCTIONSTATE_VALIDATED => htmllocale('Remise acceptée'),
        LOC::Tariff::Rent::REDUCTIONSTATE_PENDING   => htmllocale('Remise en attente'),
        LOC::Tariff::Rent::REDUCTIONSTATE_REFUSED   => htmllocale('Remise annulée')
    };
    my $tabTrspReducStates = {
        LOC::Tariff::Transport::REDUCTIONSTATE_VALIDATED => htmllocale('Remise acceptée'),
        LOC::Tariff::Transport::REDUCTIONSTATE_PENDING   => htmllocale('Remise en attente'),
        LOC::Tariff::Transport::REDUCTIONSTATE_REFUSED   => htmllocale('Remise annulée')
    };

    # Options pour tablesorter
    my $headersOption = '
0  : {sorter: "textAttr"},
9  : {sorter: "integerAttr"},
11 : {sorter: "textAttr"},
13 : {sorter: "textAttr"},
14 : {sorter: "textAttr"},
15 : {sorter: false},
16 : {sorter: false}';
    my $sortListOption = '[[2,0],[3,0]]'; # Tri par défaut: "Agent" (ASC) et "Client" (ASC)

    print '
<table class="basic list">
<thead>
    <tr>
        <th class="state"></th>
        <th class="code">' . htmllocale('Devis') . '</th>
        <th class="creator"><span title="' . htmllocale('Créateur du devis') . '">' . htmllocale('Agent') . '</span></th>
        <th class="customer">' . htmllocale('Client') . '</th>
        <th class="tel">' . htmllocale('Téléphone') . '</th>
        <th class="city">' . htmllocale('Ville') . '</th>
        <th class="model">' . htmllocale('Modèle') . '</th>
        <th class="siteLabel">' . htmllocale('Libellé chantier') . '</th>
        <th class="site">' . htmllocale('Ville Chantier') . '</th>
        <th class="datesToConfirm" title="' . htmllocale('Dates à confirmer') . '"></th>
        <th class="negotiator">' . htmllocale('Négociateur') . '</th>
        <th class="beginDate">' . htmllocale('Date départ') . '</th>
        <th class="duration">' . htmllocale('Durée (j.)') . '</th>
        <th class="rentSpcReduc"><span title="' . htmllocale('Remise exceptionnelle') . ': ' . htmllocale('Location') . '">' . htmllocale('RE loc.') . '</span></th>
        <th class="trspSpcReduc"><span title="' . htmllocale('Remise exceptionnelle') . ': ' . htmllocale('Transport') . '">' . htmllocale('RE tsp.') . '</span></th>
        <th class="returnError"></th>
        <th class="unfollow"></th>
    </tr>
</thead>
<tbody>';

    # Affichage des lignes
    my $cpt = 0;
    foreach my $tabEstimateInfos (@tabEstimateLines)
    {
        # URL's
        my $estimateUrl = $view->createURL('rent:rentEstimate:view', {'estimateId' => $tabEstimateInfos->{'estimate.id'},
                                                                      'estimateLineId' => $tabEstimateInfos->{'id'}});
        my $customerUrl = $tabViewData{'customerUrl'};

        my $customerId = $tabEstimateInfos->{'estimate.customer.id'};
        $customerUrl =~ s/\<\%id\>/$customerId/;


        # Icône des dates à confirmer
        my $datesToConfirm = ($tabEstimateInfos->{'isDatesToConfirm'} ? '<div title="' . htmllocale('Dates à confirmer') . '"></div>' : '');

        # Bouton d'action
        my $unFollowBtn = $view->displayTextButton('', 'rent/rentEstimate/unFollowBtn(|Over).png',
                                                 'doAction(' . $tabEstimateInfos->{'id'} . ');',
                                                 $locale->t('Ne plus suivre'), 'l', {'id' => 'unfollowBtn_' . $tabEstimateInfos->{'id'}, 'class' => 'unfollowBtn'});
        my $unFollowUndoBtn = $view->displayTextButton('', 'revert(|Over).png',
                                                 'doAction(' . $tabEstimateInfos->{'id'} . ');',
                                                 $locale->t('Annuler'), 'l', {'id' => 'unfollowUndoBtn_' . $tabEstimateInfos->{'id'}, 'class' => 'unfollowUndoBtn'});

        # Etat de la ligne
        my $stateIcon = '';
        my $class = 'unfollowLine';
        if ($tabEstimateInfos->{'state.id'} eq LOC::Estimate::Rent::LINE_STATE_INACTIVE)
        {
            $stateIcon = '<div title="' . htmllocale('Ligne désactivée') . '"></div>';
            $class .= ' inactive';
        }

        # Remises exceptionnelles de Location et Transport
        my $rentSpcReducState = $tabEstimateInfos->{'rentTariff.specialReductionStatus'};
        my $trspSpcReducState = $tabEstimateInfos->{'transportTariff.specialReductionStatus'};

        print '
    <tr id="line_' . $tabEstimateInfos->{'id'} . '" class="' . $class . '">
        <td class="state" data-val="' . $tabEstimateInfos->{'state.id'} . '">' . $stateIcon . '</td>
        <td class="code"><a href="' . $estimateUrl . '" target="_blank" title="' . $view->toHTMLEntities($tabEstimateInfos->{'estimate.code'}) . '"><b>' .
                                    $tabEstimateInfos->{'estimate.id'} . '</b><br />' .
                                    '<small><i>(' . htmllocale('ligne %s', $tabEstimateInfos->{'index'}) . ')</i></small></a></td>
        <td class="creator">' . $tabEstimateInfos->{'estimate.creator.fullName'} . '</td>
        <td class="customer"><a href="' . $customerUrl . '" target="_blank">' . $view->toHTMLEntities($tabEstimateInfos->{'estimate.customer.name'}) . '</a></td>
        <td class="tel">' . $tabEstimateInfos->{'estimate.customer.telephone'} . '</td>
        <td class="city">' . $view->toHTMLEntities($tabEstimateInfos->{'estimate.customer.locality.label'}) . '</td>
        <td class="model" title="' . $view->toHTMLEntities($tabEstimateInfos->{'model.tabInfos'}->{'machinesFamily.fullName'}) . '">' . $view->toHTMLEntities($tabEstimateInfos->{'model.tabInfos'}->{'label'}) . '</td>
        <td class="siteLabel">' . $view->toHTMLEntities($tabEstimateInfos->{'estimate.site.label'}) . '</td>
        <td class="site">' . $view->toHTMLEntities($tabEstimateInfos->{'estimate.site.locality.label'}) . '</td>
        <td class="datesToConfirm" data-val="' . $tabEstimateInfos->{'isDatesToConfirm'} . '">' . $datesToConfirm . '</td>
        <td class="negotiator">' . $view->toHTMLEntities($tabEstimateInfos->{'estimate.negotiator.fullName'}) . '</td>
        <td class="beginDate" data-val="' . $tabEstimateInfos->{'beginDate'} . '">' . $locale->getDateFormat($tabEstimateInfos->{'beginDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC) . '</td>
        <td class="duration">' . $tabEstimateInfos->{'duration'} . '</td>
        <td class="spcReduc rent" data-val="' . $rentSpcReducState . '">' . ($rentSpcReducState ne '' ? '<div class="' . $rentSpcReducState . '" title="' . $tabRentReducStates->{$rentSpcReducState} . '"></div>' : '') . '</td>
        <td class="spcReduc trsp" data-val="' . $trspSpcReducState . '">' . ($trspSpcReducState ne '' ? '<div class="' . $trspSpcReducState . '" title="' . $tabTrspReducStates->{$trspSpcReducState} . '"></div>' : '') . '</td>
        <td class="returnError"></td>
        <td class="unfollow">' . $unFollowUndoBtn . $unFollowBtn . '</td>
    </tr>';
    $cpt++;
    }

    print '
</tbody>
</table>';


    # Tri du tableau
    print $view->displayJSBlock('
$.tablesorter.addParser({
    id: "textAttr",
    is: function(s) { return false; },
    format: function(s, table, cell, cellIndex) { return $(cell).attr("data-val"); },
    type: "text"
});
$.tablesorter.addParser({
    id: "integerAttr",
    is: function(s) { return false; },
    format: function(s, table, cell, cellIndex) { return $(cell).attr("data-val"); },
    type: "numeric"
});

$(function() 
{ 
    $("table.basic.list").tablesorter({
        headers: {' . $headersOption . '},
        sortList: ' . $sortListOption . ',
        widgets: ["zebra"]
    });
}
);');
}


# Affichage du pied de page
print $view->displayFooter();
