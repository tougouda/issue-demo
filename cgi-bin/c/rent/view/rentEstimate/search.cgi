use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'rent/rentEstimate/';


our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}

# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'search.css');

# Js
$view->addJSSrc($dir . 'search.js');

$view->setPageTitle($locale->t('Devis de location'))
     ->addPackage('popups')
     ->addPackage('tabBoxes')
     ->addPackage('tooltips')
     ->addPackage('searchboxes');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);


$view->setTitle($locale->t('Devis de location'));


$view->addTranslations({
    'label-result-s' => $locale->t('1 résultat'),
    'label-result-p' => $locale->t('%s / %s résultats', '<%nb>', '<%total>'),
    'label-see-more-s' => $locale->t('Voir %s résultat supplémentaire', '<%step>'),
    'label-see-more-p' => $locale->t('Voir %s résultats supplémentaires', '<%step>')
});

my $searchUrl = $view->createURL('rent:rentEstimate:jsonSearch');
my $tabData = {
    'result' => $tabViewData{'result'},
    'total'  => $tabViewData{'total'}
};
$view->addBodyEvent('load', 'search.init("' . $searchUrl . '", ' .
                                &LOC::Json::toJson($tabViewData{'tabFilters'}) . ', ' .
                                &LOC::Json::toJson($tabData) . ',' .
                                $tabViewData{'offset'} . ', ' .
                                $tabViewData{'step'} . ');');

# Barre de recherche
require($directory . '/rentEstimate/searchBar.cgi');

# Affichage de l'entête
print $view->displayHeader();

print '<h1 class="searchResult">' . $locale->t('Résultat de la recherche') . '</h1>';

# S'il n'y a pas de critères sélectionnés
print '<p id="msg-ok" class="result-msg"></p>';

print '
<table id="estimatesList" class="basic list">
    <thead>
        <tr>
            <th class="code">' . $locale->t('N°') . '</th>
            <th class="customer">' . $locale->t('Client') . '</th>
            <th colspan="2" class="site">' . $locale->t('Chantier') . '</th>
            <th class="state">' . $locale->t('État') . '</th>
            <th class="negotiator">' . $locale->t('Négociateur') . '</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
        <tr class="pending-search">
            <td colspan="6">
                ' . $locale->t('Recherche en cours') . $view->displayImage('loading.gif') . '
            </td>
        </tr>
        <tr class="no-filters">
            <td colspan="6">' . $locale->t('Aucun critère de recherche sélectionné. Veuillez affiner votre recherche.') . '</td>
        </tr>
        <tr class="no-result">
            <td colspan="6">' . $locale->t('Aucun devis ne correspond à votre recherche') . '</td>
        </tr>
        <tr class="more" id="search-more">
            <td colspan="6"></td>
        </tr>
        <tr class="error">
            <td colspan="6">' . $locale->t('Une erreur est survenue lors de la recherche.') . '</td>
        </tr>
    </tfoot>
    </tbody>
</table>';

print $view->displayFooter();
