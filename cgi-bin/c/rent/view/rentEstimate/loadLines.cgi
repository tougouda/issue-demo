use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Json;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Fonctions de formatage des données
require($directory . '/helper/format/duration.cgi');
require($directory . '/helper/format/transport.cgi');
require($directory . '/helper/format/proforma.cgi');
require($directory . '/helper/format/documents.cgi');
require($directory . '/helper/format/history.cgi');


our %tabViewData;

# Locale
our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});



foreach my $tabLineInfos (@{$tabViewData{'tabData'}->{'tabLines'}})
{
    if ($tabLineInfos->{'@state'} eq 'ok')
    {
        # Bouton de visualisation du contrat
        if ($tabLineInfos->{'linkedContract'})
        {
            $tabLineInfos->{'linkedContract'}->{'title'} = $locale->t('Voir le contrat %s',
                                                                      $tabLineInfos->{'linkedContract'}->{'code'})
        }


        # Formatage des données de l'onglet Durée
        &hlpFormatDurationData($tabLineInfos->{'tabDuration'}, $locale, {});

        # Formatage des données de l'onglet Transport
        &hlpFormatTransportData($tabLineInfos->{'tabTransport'}, $locale, {});

        # Formatage des données de l'onglet Pro forma
        &hlpFormatProformaData($tabLineInfos->{'tabProforma'}, $locale, {});

        # Formatage des données de l'onglet Documents
        &hlpFormatDocumentsData($tabLineInfos->{'tabDocuments'}, $locale, {});

        # Formatage des données de l'onglet Historique
        &hlpFormatHistoryData($tabLineInfos->{'tabHistory'}, $locale, {'timeZone' => $tabViewData{'timeZone'}});
    }
}


my $view = LOC::Json->new();
print $view->displayHeader();
print $view->encode($tabViewData{'tabData'});
