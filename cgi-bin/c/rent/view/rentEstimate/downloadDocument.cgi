use open (':encoding(UTF-8)');
binmode STDOUT, ":raw";    # Pour envoyer le flux en l'état
use bytes;                 # Pour avoir la vraie longueur en octets

use strict;

use LOC::Locale;
use LOC::Browser;

our %tabViewData;

# Locale
our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});

my $contentType = 'application/force-download';
my $contentDisposition = 'attachment';

if (defined $tabViewData{'errors'})
{
    &LOC::Browser::sendHeaders("Status: 404\n\n");
    print $locale->t('Le fichier n\'existe pas !');
}
else
{
    my $fileInfos = $tabViewData{'fileInfos'};
    if ($tabViewData{'mode'} eq 'display')
    {
        $contentType = $fileInfos->{'mimeType'};
        $contentDisposition = 'inline';
    }

    &LOC::Browser::sendHeaders(
            "Content-Type: " . $contentType . "\n" .
            "Content-Disposition: " . $contentDisposition . "; filename=\"" . $fileInfos->{'fileName'} . "\"\n" .
            "Content-Length: " . length($fileInfos->{'fileContent'}) . "\n" .
            "\n" .
            $fileInfos->{'fileContent'}
        );
}
