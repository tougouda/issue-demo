use utf8;
binmode STDOUT, ":raw";    # Pour envoyer le flux en l'état

use LOC::Locale;
use LOC::Browser;

our %tabViewData;

my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});

my $tabParams = {
    'CLSTE'             => $tabViewData{'tabData'}->{'customer.tabInfos'}->{'label'},
    'CLADRESSE'         => $tabViewData{'tabData'}->{'customer.tabInfos'}->{'address'},
    'CLADRESSE2'        => $tabViewData{'tabData'}->{'customer.tabInfos'}->{'address2'},
    'CLTEL'             => $tabViewData{'tabData'}->{'customer.tabInfos'}->{'telephone'},
    'CLFAX'             => $tabViewData{'tabData'}->{'customer.tabInfos'}->{'fax'},
    'CLCGVENTE'         => $tabViewData{'tabData'}->{'customer.tabInfos'}->{'areGeneralConditionsApproved'},
    'CLPASSURANCE'      => $tabViewData{'tabData'}->{'customer.tabInfos'}->{'insurance.rate'},
    'CLDATEDEBASS'      => $tabViewData{'tabData'}->{'customer.tabInfos'}->{'insurance.beginDate'},
    'CLPOTENTIEL'       => $tabViewData{'tabData'}->{'customer.tabInfos'}->{'potential'},
    'VICLCP'            => $tabViewData{'tabData'}->{'customer.tabInfos'}->{'postalCode'},
    'VICLNOM'           => $tabViewData{'tabData'}->{'customer.tabInfos'}->{'city'},
    'CHLIBELLE'         => $tabViewData{'tabData'}->{'site.tabInfos'}->{'label'},
    'CHADRESSE'         => $tabViewData{'tabData'}->{'site.tabInfos'}->{'address'},
    'VICP'              => $tabViewData{'tabData'}->{'site.tabInfos'}->{'postalCode'},
    'VINOM'             => $tabViewData{'tabData'}->{'site.tabInfos'}->{'city'},
    'CHCONTACT'         => $tabViewData{'tabData'}->{'site.tabInfos'}->{'contact.fullName'},
    'CHTELEPHONE'       => $tabViewData{'tabData'}->{'site.tabInfos'}->{'telephone'},
    'DEVISCONTACTCOM'   => $tabViewData{'tabData'}->{'orderContact.fullName'},
    'DEVISTELEPHONECOM' => $tabViewData{'tabData'}->{'orderContact.telephone'},
    'MOPALIBELLE'       => $tabViewData{'tabData'}->{'customer.tabInfos'}->{'paymentMode.label'},
    'DEVISNUMCOM'       => $tabViewData{'tabData'}->{'orderNo'},
    'DEVISAUTO'         => $tabViewData{'tabData'}->{'id'},
    'AGAUTO'            => $tabViewData{'tabData'}->{'agency.tabInfos'}->{'id'},
    'AGLIBELLE'         => $tabViewData{'tabData'}->{'agency.tabInfos'}->{'label'},
    'AGADRESSE'         => $tabViewData{'tabData'}->{'agency.tabInfos'}->{'address'},
    'AGCP'              => $tabViewData{'tabData'}->{'agency.tabInfos'}->{'postalCode'},
    'AGVILLE'           => $tabViewData{'tabData'}->{'agency.tabInfos'}->{'city'},
    'AGTEL'             => $tabViewData{'tabData'}->{'agency.tabInfos'}->{'telephone'},
    'AGFAX'             => $tabViewData{'tabData'}->{'agency.tabInfos'}->{'fax'},
    'INTERLOCPRENOM'    => $tabViewData{'tabData'}->{'negotiator.tabInfos'}->{'firstName'},
    'INTERLOCNOM'       => $tabViewData{'tabData'}->{'negotiator.tabInfos'}->{'name'},
    'INTERLOCPORTABLE'  => $tabViewData{'tabData'}->{'negotiator.tabInfos'}->{'mobile'},
    'DEVISCOMMENTAIRE'  => $tabViewData{'tabData'}->{'comments'},
    'DEVISASSURANCE'    => 0,
    'DEVISPASSURANCE'   => [],
    'DEVISFULLSERVICE'  => -$tabViewData{'tabData'}->{'isFullService'},
    'PAMONNAIE'         => $tabViewData{'tabData'}->{'currencySymbol'},
    'PAASSURANCE'       => $tabViewData{'tabData'}->{'country.insurance.rates'},
    'tabLines' => []
};

my @tabInsuranceFlags = (
    LOC::Insurance::Type::FLAG_ISINVOICED(),
    LOC::Insurance::Type::FLAG_ISINCLUDED(),
    LOC::Insurance::Type::FLAG_ISCUSTOMER()
);
my $tabEstimateInsuranceFlags = {};
my $tabEstimateAppealInfos = undef;

foreach my $lineInfos (@{$tabViewData{'tabData'}->{'tabLines'}})
{
    # Assurance
    my $isInsuranceNotInvoiced = 1;
    foreach my $insuranceFlag (@tabInsuranceFlags)
    {
        if ($lineInfos->{'insurance.tabInfos'}->{'flags'} & $insuranceFlag)
        {
            $tabEstimateInsuranceFlags->{$insuranceFlag} = $lineInfos->{'insurance.tabInfos'};
            $isInsuranceNotInvoiced = 0;
        }
    }
    if ($isInsuranceNotInvoiced)
    {
        $tabEstimateInsuranceFlags->{'notInvoiced'} = $lineInfos->{'insurance.tabInfos'};
    }

    # Taux d'assurance si facturé
    if ($lineInfos->{'insurance.tabInfos'}->{'flags'} & LOC::Insurance::Type::FLAG_ISINVOICED &&
        !&LOC::Util::in_array($lineInfos->{'insurance.tabInfos'}->{'rate'}, $tabParams->{'DEVISPASSURANCE'}))
    {
        push(@{$tabParams->{'DEVISPASSURANCE'}}, $lineInfos->{'insurance.tabInfos'}->{'rate'});
    }

    # Gestion de recours
    if ($lineInfos->{'appeal.tabInfos'} && $lineInfos->{'appeal.tabInfos'}->{'isInvoiced'})
    {
        $tabEstimateAppealInfos = $lineInfos->{'appeal.tabInfos'};
    }

    my $line = {
        'FAMAMODELE'              => $lineInfos->{'model.tabInfos'}->{'machinesFamily.name'},
        'FAMAENERGIE'             => $lineInfos->{'model.tabInfos'}->{'machinesFamily.energy'},
        'FAMAELEVATION'           => $lineInfos->{'model.tabInfos'}->{'machinesFamily.elevation'},
        'DETAILSDEVISQTE'         => $lineInfos->{'quantity'},
        'PRIXJOUREU'              => $lineInfos->{'unitPrice'},
        'DETAILSDEVISPXJOURSFR'   => ($lineInfos->{'rentTariff.specialReductionPercent'} != 100 ? 
                                        $lineInfos->{'unitPrice'} / (1 - $lineInfos->{'rentTariff.specialReductionPercent'} / 100) : 0),
        'DETAILSDEVISTYPEMONTANT' => $lineInfos->{'amountType'},
        'DETAILSDEVISDUREE'       => $lineInfos->{'duration'},
        'DATEDETAILSDEVISDATE'    => $lineInfos->{'beginDate'},
        'DETAILSDEVISTRD'         => $lineInfos->{'transportTariff.deliveryAmount'},
        'DETAILSDEVISTRR'         => $lineInfos->{'transportTariff.recoveryAmount'},
        'DETAILSDEVISREMISEEX'    => $lineInfos->{'rentTariff.specialReductionPercent'},
        'fly_id'                  => $lineInfos->{'model.tabInfos'}->{'family.id'},
        'cleaning.type.id'        => $lineInfos->{'cleaning.type.id'},
        'residues.mode'           => $lineInfos->{'residues.mode'},
        'residues.value'          => $lineInfos->{'residues.value'}
    };

    push(@{$tabParams->{'tabLines'}}, $line);
}

# Récupération de l'assurance globale du devis
my $globalInsuranceInfos;

if (defined $tabEstimateInsuranceFlags->{LOC::Insurance::Type::FLAG_ISINVOICED()})
{
    $globalInsuranceInfos = $tabEstimateInsuranceFlags->{LOC::Insurance::Type::FLAG_ISINVOICED()};
}
elsif (defined $tabEstimateInsuranceFlags->{LOC::Insurance::Type::FLAG_ISCUSTOMER()})
{
    $globalInsuranceInfos = $tabEstimateInsuranceFlags->{LOC::Insurance::Type::FLAG_ISCUSTOMER()};
}
elsif (defined $tabEstimateInsuranceFlags->{'notInvoiced'})
{
    $globalInsuranceInfos = $tabEstimateInsuranceFlags->{'notInvoiced'};
}
elsif (defined $tabEstimateInsuranceFlags->{LOC::Insurance::Type::FLAG_ISINCLUDED()})
{
    $globalInsuranceInfos = $tabEstimateInsuranceFlags->{LOC::Insurance::Type::FLAG_ISINCLUDED()};
}

# Assurance - libellé
$tabParams->{'insurance.isInvoiced'} = ($globalInsuranceInfos ? $globalInsuranceInfos->{'isInvoiced'} : 0);
$tabParams->{'insurance.label'}      = ($globalInsuranceInfos ? $globalInsuranceInfos->{'fullLabel'} : '');

# Gestion de recours - libellé
my $isAppealActivated = ($globalInsuranceInfos ? $globalInsuranceInfos->{'isAppealActivated'} : 0);
my $appealLabel = '';
$tabParams->{'appeal.isInvoiced'} = 0;
if ($isAppealActivated && $tabEstimateAppealInfos)
{
    $tabParams->{'appeal.isInvoiced'} = 1;
    $appealLabel = $tabEstimateAppealInfos->{'label'};
    my $percent = $locale->t('%s %%', $tabEstimateAppealInfos->{'rate'} * 100);
    $appealLabel =~ s/<%percent>/$percent/g;
}
$tabParams->{'appeal.label'} = $appealLabel;


my $tabMinDays = [];
foreach my $baseTariff (@{$tabViewData{'tabBaseTariffs'}})
{
    if (!&LOC::Util::in_array($baseTariff->{'minDays'}, $tabMinDays))
    {
        push(@$tabMinDays, $baseTariff->{'minDays'});
    }
}

my $tabAdditionalDocuments = &LOC::Json::fromJson(&LOC::Characteristic::getCountryValueByCode('PDFCPLDEVIS', $tabViewData{'tabData'}->{'countryId'}));
foreach my $tabZone (values(%$tabAdditionalDocuments))
{
    foreach my $documentInfos (@$tabZone)
    {
        $documentInfos->{'path'} =~ s/<%commonFilesPath>/@{[&LOC::Globals::get('commonFilesPath')]}/;
    }
}


my $tabData = {
    'tabParams' => $tabParams,
    'oldFrameworkPath'       => &LOC::Globals::get('oldFrameworkPath'),
    'countryId'              => $tabViewData{'tabData'}->{'countryId'},
    'tabAdditionalDocuments' => $tabAdditionalDocuments,
    'logoImg'                => &LOC::Globals::getLogo($tabViewData{'tabData'}->{'countryId'}, LOC::Globals::LOGO_TYPE_RASTER),
    'env'                    => &LOC::Globals::getEnv(),
    'footer'                 => &LOC::Characteristic::getCountryValueByCode('PIEDIMPR', $tabViewData{'tabData'}->{'countryId'}),
    'capital'                => $locale->getCurrencyFormat(&LOC::Country::getCapital($tabViewData{'tabData'}->{'countryId'}, undef, $tabViewData{'tabData'}->{'currencySymbol'})),
    'tabMinDays'             => $tabMinDays,
    'nbValidDays'            => &LOC::Characteristic::getCountryValueByCode('JRSVALIDDEVIS', $tabViewData{'tabData'}->{'countryId'})
};

my $params = &LOC::Json::toJson($tabData);

&LOC::Browser::sendHeaders("Content-Type: application/pdf;\n\n");

my $command = 'php ' . $tabViewData{'pdfUrl'} . ' ' . &LOC::Util::utf8Base64Encode($params);
print qx($command);