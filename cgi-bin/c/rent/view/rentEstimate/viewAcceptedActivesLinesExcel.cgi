use utf8;
use open (':encoding(UTF-8)');

use strict;


use Spreadsheet::WriteExcel;

use LOC::Locale;
use LOC::Browser;


# Répertoire CSS/JS/Images
my $dir = 'rent/rentEstimate/';


our %tabViewData;

my $currentAreaId = $tabViewData{'currentAreaId'};


my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});


# Fonctions utiles
sub displayBoolean
{
    my ($value) = @_;
    return ($value ? 'X' : '');
}


# Téléchargement du fichier
my $fileName = $locale->t('Lignes de devis acceptées') . '_' . POSIX::strftime('%Y%m%d_%H%M', localtime()) . '.xls';
$fileName =~ tr/"<>&\/\\?#\*;/'{}+____\-\-/;

&LOC::Browser::sendHeaders(
        "Content-Type: application/force-download\n"
        . "Content-Disposition: attachment; filename=\"" . $fileName . "\"\n"
        . "\n"
    );


# Nouveau classeur Excel envoyé sur la sortie standard
my $workbook = Spreadsheet::WriteExcel->new('-');
my $title = $locale->t('Liste des lignes de devis acceptées');
my $author = $locale->t('Gestion des locations') . ' ' . &LOC::Globals::get('version');
my $company = 'Accès industrie';
$workbook->set_properties('title' => $title, 'author' => $author, 'company' => $company);

# Format de la ligne d'en-tête
my $headerFmt = $workbook->add_format();
$headerFmt->set_italic();
$headerFmt->set_bold();
$headerFmt->set_bg_color($workbook->set_custom_color(12, 232, 243, 249));
$headerFmt->set_bottom();

# Format de date
my $dateFmt = $workbook->add_format();
$dateFmt->set_num_format(14);

# Format des booléens
my $boolFmt = $workbook->add_format();
$boolFmt->set_align('center');

# Format des récapitulatifs
my $recapFmt = $workbook->add_format();
$recapFmt->set_bold();
$recapFmt->set_bg_color($workbook->set_custom_color(13, 200, 200, 249));
$recapFmt->set_top();


# Tri du tableau
my @tabEstimatesLines = values %{$tabViewData{'tabEstimateLines'}};

@tabEstimatesLines = sort {$a->{'estimate.agency.id'} cmp $b->{'estimate.agency.id'} ||
                           $a->{'beginDate'} cmp $b->{'beginDate'}} @tabEstimatesLines;


# Tri des régions
my @tabAreas = values %{$tabViewData{'tabAreas'}};

@tabAreas = sort {$a->{'name'} cmp $b->{'name'}} @tabAreas;

# Création des feuilles et de leurs contenus
foreach my $tabAreaInfos (@tabAreas)
{
    my $worsheetLabel = $tabAreaInfos->{'name'};
    if ($worsheetLabel eq '')
    {
        $worsheetLabel = '-' . $locale->t('Autres') . '-';
    }
    my $worksheet = $workbook->add_worksheet($worsheetLabel);
    if ($tabAreaInfos->{'id'} == $currentAreaId)
    {
        $worksheet->activate();
    }

    # Entête et pied de page pour l'impression
    $worksheet->set_header('&C&12&",Bold"' . $title . ' - ' . $worsheetLabel);
    $worksheet->set_footer('&C&8' . $locale->t('Page') . ' &P / &N');


    # Figer les volets
    $worksheet->freeze_panes(1, 0);


    # Ligne d'en-tête
    my $c = 0;
    my $l = 0;
    $worksheet->write_string($l, $c, $locale->t('Agence'), $headerFmt);
    $worksheet->set_column($c, $c++, 8);
    $worksheet->write_string($l, $c, $locale->t('Devis'), $headerFmt);
    $worksheet->set_column($c, $c++, 22);
    $worksheet->write_string($l, $c, $locale->t('Ligne'), $headerFmt);
    $worksheet->set_column($c, $c++, 7);
    $worksheet->write_string($l, $c, $locale->t('Dates à confirmer'), $headerFmt);
    $worksheet->set_column($c, $c++, 18);
    $worksheet->write_string($l, $c, $locale->t('Date départ'), $headerFmt);
    $worksheet->set_column($c, $c++, 11);
    $worksheet->write_string($l, $c, $locale->t('Livraison'), $headerFmt);
    $worksheet->set_column($c, $c++, 12);
    $worksheet->write_string($l, $c, $locale->t('Livraison anticipée'), $headerFmt);
    $worksheet->set_column($c, $c++, 20);
    $worksheet->write_string($l, $c, $locale->t('Désignation'), $headerFmt);
    $worksheet->set_column($c, $c++, 14);
    $worksheet->write_string($l, $c, $locale->t('Ville chantier'), $headerFmt);
    $worksheet->set_column($c, $c++, 35);
    $worksheet->write_string($l, $c, $locale->t('Zone'), $headerFmt);
    $worksheet->set_column($c, $c++, 8);
    $worksheet->write_string($l, $c, $locale->t('Client'), $headerFmt);
    $worksheet->set_column($c, $c++, 40);
    $worksheet->write_string($l, $c, $locale->t('Montant livraison'), $headerFmt);
    $worksheet->set_column($c, $c++, 18);
    $worksheet->write_string($l, $c, $locale->t('Machine supplémentaire'), $headerFmt);
    $worksheet->set_column($c, $c++, 22);
    $worksheet->write_string($l, $c, $locale->t('Enlèvement sur place' . ' (' . $locale->t('Livraison') . ')'), $headerFmt);
    $worksheet->set_column($c, $c++, 30);
    $worksheet->write_string($l, $c, $locale->t('Durée' . ' (' . $locale->t('en jours ouvrés') . ')'), $headerFmt);
    $worksheet->set_column($c, $c++, 22);
    $worksheet->write_string($l, $c, $locale->t('Ouverture de compte à faire'), $headerFmt);
    $worksheet->set_column($c, $c++, 25);
    my $columnsCount = $c;

    $l++;




    # Lignes de devis
    my $currentAgencyId = '';
    my $linesCount = 0;
    foreach my $tabEstimateInfos (@tabEstimatesLines)
    {
        my $agencyId = $tabEstimateInfos->{'estimate.agency.id'};
        if (exists $tabAreaInfos->{'tabAgencies'}->{$agencyId})
        {
            if ($currentAgencyId ne '' && $currentAgencyId ne $agencyId)
            {
                # Ligne du récapitulatif pour l'agence
                $c = 0;
                $worksheet->write_string($l, $c++, $currentAgencyId, $recapFmt);
                $worksheet->write_number($l, $c++, $linesCount, $recapFmt);
                for (;$c < $columnsCount; $c++)
                {
                    $worksheet->write_string($l, $c, '', $recapFmt);
                }

                $linesCount = 0;
                $l++;
                $l++;
            }
            $currentAgencyId = $agencyId;


            $c = 0;
            $worksheet->write_string($l, $c++, $agencyId);
            $worksheet->write_string($l, $c++, $tabEstimateInfos->{'estimate.code'});
            $worksheet->write_number($l, $c++, $tabEstimateInfos->{'index'});
            $worksheet->write_string($l, $c++, &displayBoolean($tabEstimateInfos->{'isDatesToConfirm'}), $boolFmt);
            $worksheet->write_date_time($l, $c++, $tabEstimateInfos->{'beginDate'} . 'T', $dateFmt);
            $worksheet->write_string($l, $c, $tabEstimateInfos->{'estimate.site.deliveryHour'});
            if ($tabEstimateInfos->{'estimate.site.deliveryTimeSlot.id'})
            {
                # Plage horaire de livraison
                my $label = $tabViewData{'tabTransportTimeSlots'}->{$tabEstimateInfos->{'estimate.site.deliveryTimeSlot.id'}}->{'label'};
                $worksheet->write_comment($l, $c, $locale->t('Plage horaire' . ":\n" . $label));
            }
            $c++;
            $worksheet->write_string($l, $c++, &displayBoolean($tabEstimateInfos->{'estimate.site.isAnticipated'}), $boolFmt);
            $worksheet->write_string($l, $c++, $tabEstimateInfos->{'model.tabInfos'}->{'label'});
            $worksheet->write_string($l, $c, $tabEstimateInfos->{'estimate.site.locality.label'});
            if ($tabEstimateInfos->{'estimate.site.comments'} ne '')
            {
                $worksheet->write_comment($l, $c, $tabEstimateInfos->{'estimate.site.comments'});
            }
            $c++;
            $worksheet->write_number($l, $c++, $tabEstimateInfos->{'estimate.zone'});
            $worksheet->write_string($l, $c++, $tabEstimateInfos->{'estimate.customer.name'});
            $worksheet->write_number($l, $c++, $tabEstimateInfos->{'transportTariff.deliveryAmount'});
            $worksheet->write_string($l, $c++, &displayBoolean($tabEstimateInfos->{'transportTariff.isAddMachineDelivery'}), $boolFmt);
            $worksheet->write_string($l, $c++, &displayBoolean($tabEstimateInfos->{'transportTariff.isNoDelivery'}), $boolFmt);
            $worksheet->write_number($l, $c++, $tabEstimateInfos->{'duration'});
            $worksheet->write_string($l, $c++, &displayBoolean($tabEstimateInfos->{'estimate.customer.isNewAccount'}), $boolFmt);

            $linesCount++;
            $l++;
        }
    }

    # Dernière ligne de récapitulatif
    if ($linesCount > 0)
    {
        # Ligne du récapitulatif pour l'agence
        $c = 0;
        $worksheet->write_string($l, $c++, $currentAgencyId, $recapFmt);
        $worksheet->write_number($l, $c++, $linesCount, $recapFmt);
        for (;$c < $columnsCount; $c++)
        {
            $worksheet->write_string($l, $c, '', $recapFmt);
        }
    }
}

# Fermeture du classeur
$workbook->close();
