use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;

# Répertoire CSS/JS/Images
my $dir = 'rent/rentEstimate/';


our %tabViewData;


my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
my $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


# URL courante 
my $currentUrl = $view->createURL('rent:rentEstimate:viewAcceptedLines');


# Feuilles de style
$view->addCSSSrc('location.css');

$view->addCSSSrc($dir . 'viewAcceptedLines.css');

# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc('jQuery/plugins/tablesorter.js');
$view->addJSSrc($dir . 'viewAcceptedLines.js');


$view->addBodyEvent('onload',
                    'init("' . $tabViewData{'country.id'} . '", 
                          "' . $view->createURL('rent:rentEstimate:doLines') . '", 
                          ' . $tabViewData{'tabEstimateLinesLite'} . ');');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);


my $title = $locale->t('Alerte des lignes de devis acceptées');
$view->setPageTitle($title);
$view->setTitle($title);


# Bouton "Actualiser"
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png', $currentUrl, $locale->t('Actualiser la page'));
$view->addControlsContent($refreshBtn, 'right', 1);


# Affichage de l'entête
print $view->displayHeader();


# On n'affiche pas le tableau si il est vide
my @tabEstimateLines = values %{$tabViewData{'tabEstimateLines'}};
if (@tabEstimateLines == 0)
{
    # Pas de résultat
    print '<div class="noResult">' . $view->displayMessages('info', [$locale->t('Aucune ligne de devis n\'est acceptée')], 0) . '</div>';
}
else
{
    # Options pour tablesorter
    my $headersOption = '
5  : {sorter: "integerAttr"},
6  : {sorter: "textAttr"},
7  : {sorter: "integerAttr"},
8  : {sorter: "integerAttr"},
10  : {sorter: false},
11  : {sorter: false}';

    my $sortListOption = '[[6,0],[1,0]]'; # Tri par défaut: "Date départ" (ASC) et "Client" (ASC)

    print '
<table class="basic list">
<thead>
    <tr>
        <th class="code">' . htmllocale('Devis') . '</th>
        <th class="customer">' . htmllocale('Client') . '</th>
        <th class="model">' . htmllocale('Modèle') . '</th>
        <th class="siteLabel">' . htmllocale('Libellé chantier') . '</th>
        <th class="site">' . htmllocale('Ville Chantier') . '</th>
        <th class="datesToConfirm" title="' . htmllocale('Dates à confirmer') . '"></th>
        <th class="beginDate">' . htmllocale('Date départ') . '</th>
        <th class="isAnticipatedDlv"><span title="' . htmllocale('Livraison anticipée possible') . '">' . htmllocale('Liv. anticipée poss.') . '</span></th>
        <th class="isNoDelivery"><span title="' . htmllocale('Enlèvement sur place') . ' (' . htmllocale('Livraison') . ')">' . htmllocale('Enlvt / place') . '</span></th>
        <th class="duration">' . htmllocale('Durée (j.)') . '</th>
        <th class="returnError"></th>
        <th class="unaccept"></th>
    </tr>
</thead>
<tbody>';

    # Affichage des lignes
    my $cpt = 0;
    foreach my $tabEstimateInfos (@tabEstimateLines)
    {
        # URL's
        my $estimateUrl = $view->createURL('rent:rentEstimate:view', {'estimateId' => $tabEstimateInfos->{'estimate.id'},
                                                                      'estimateLineId' => $tabEstimateInfos->{'id'}});
        my $customerUrl = $tabViewData{'customerUrl'};

        my $customerId = $tabEstimateInfos->{'estimate.customer.id'};
        $customerUrl =~ s/\<\%id\>/$customerId/;


        # Icône des dates à confirmer
        my $datesToConfirm = ($tabEstimateInfos->{'isDatesToConfirm'} ? '<div title="' . htmllocale('Dates à confirmer') . '"></div>' : '');

        # Bouton d'action
        my $unAcceptBtn = $view->displayTextButton('', 'rent/rentEstimate/cancelAcceptBtn(|Over).png',
                                                 'doAction(' . $tabEstimateInfos->{'id'} . ');',
                                                 $locale->t('Ne plus accepter'), 'l', {'id' => 'unacceptBtn_' . $tabEstimateInfos->{'id'}, 'class' => 'unacceptBtn'});
        my $unAcceptUndoBtn = $view->displayTextButton('', 'revert(|Over).png',
                                                 'doAction(' . $tabEstimateInfos->{'id'} . ');',
                                                 $locale->t('Annuler'), 'l', {'id' => 'unacceptUndoBtn_' . $tabEstimateInfos->{'id'}, 'class' => 'unacceptUndoBtn'});

        print '
    <tr id="line_' . $tabEstimateInfos->{'id'} . '" class="unacceptLine">
        <td class="code"><a href="' . $estimateUrl . '" target="_blank" title="' . $view->toHTMLEntities($tabEstimateInfos->{'estimate.code'}) . '"><b>' .
                                    $tabEstimateInfos->{'estimate.id'} . '</b><br />' .
                                    '<small><i>(' . htmllocale('ligne %s', $tabEstimateInfos->{'index'}) . ')</i></small></a></td>
        <td class="customer"><a href="' . $customerUrl . '" target="_blank">' . $tabEstimateInfos->{'estimate.customer.name'} . '</a></td>
        <td class="model" title="' . $view->toHTMLEntities($tabEstimateInfos->{'model.tabInfos'}->{'machinesFamily.fullName'}) . '">' . $view->toHTMLEntities($tabEstimateInfos->{'model.tabInfos'}->{'label'}) . '</td>
        <td class="siteLabel">' . $view->toHTMLEntities($tabEstimateInfos->{'estimate.site.label'}) . '</td>
        <td class="site">' . $view->toHTMLEntities($tabEstimateInfos->{'estimate.site.locality.label'}) . '</td>
        <td class="datesToConfirm" data-val="' . $tabEstimateInfos->{'isDatesToConfirm'} . '">' . $datesToConfirm . '</td>
        <td class="beginDate" data-val="' . $tabEstimateInfos->{'beginDate'} . '">' . $locale->getDateFormat($tabEstimateInfos->{'beginDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC) . '</td>
        <td class="isAnticipatedDlv" data-val="' . $tabEstimateInfos->{'estimate.site.isAnticipated'} . '">' . $view->displayBoolean($tabEstimateInfos->{'estimate.site.isAnticipated'}, '') . '</td>
        <td class="isNoDelivery" data-val="' . $tabEstimateInfos->{'transportTariff.isNoDelivery'} . '">' . $view->displayBoolean($tabEstimateInfos->{'transportTariff.isNoDelivery'}, '') . '</td>
        <td class="duration">' . $tabEstimateInfos->{'duration'} . '</td>
        <td class="returnError"></td>
        <td class="unaccept">' . $unAcceptUndoBtn . $unAcceptBtn . '</td>
    </tr>';
    $cpt++;
    }

    print '
</tbody>
</table>';


    # Tri du tableau
    print $view->displayJSBlock('
$.tablesorter.addParser({
    id: "textAttr",
    is: function(s) { return false; },
    format: function(s, table, cell, cellIndex) { return $(cell).attr("data-val"); },
    type: "text"
});
$.tablesorter.addParser({
    id: "integerAttr",
    is: function(s) { return false; },
    format: function(s, table, cell, cellIndex) { return $(cell).attr("data-val"); },
    type: "numeric"
});

$(function() 
{ 
    $("table.basic.list").tablesorter({
        headers: {' . $headersOption . '},
        sortList: ' . $sortListOption . ',
        widgets: ["zebra"]
    });
}
);');
}


# Affichage du pied de page
print $view->displayFooter();
