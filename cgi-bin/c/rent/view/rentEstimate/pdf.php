<?php

// GENERATION DU PDF
$tabParams = (array)json_decode(base64_decode($argv[1]), false);
mb_internal_encoding('cp1252');

$rcs = mb_convert_encoding(str_replace('<%capital>', $tabParams['capital'], $tabParams['footer']), 'cp1252', 'utf8');

$traduction = array(
    // Specimen
    "Specimen" => "sp�cimen",

    // Ecxept. Num�ro ESPAGNE
    "NumPlateformeTel" => "",
    "NumPlateformeFax" => "",

    /* Devis */
    "DEtitre" => "LOCATION DE NACELLES INDUSTRIELLES ET DE\nCHARIOTS TELESCOPIQUES",
    "DEtitre2" => "Devis de location",
    "DEagence" => "Agence ",
    "DEdevisn" => "Devis n� :",
    "DEcommande" => "N� commande :",
    "DEtel" => "T�l.",
    "DEfax" => "Fax.",
    "DEle" => "Le ",
    "DEclient" => "Client :",
    "DEfullservice" => "FS",

    "DEjour"  => "jour",
    "DEjours" => "jours",
    "DEtjour" => "Jour",
    "DEtmois" => "Mois",
    "DElibrem" => "Apr�s remises exceptionnelles de :",
    "DEinfoch" => "Chantier :",
    "DEinfoch2" => "Contact chantier :",
    "DEinfoch3" => "Contact commande :",

    "DEfamille" => "Famille\n  ",
    "DEqte" => "Qt�\n  ",
    "DEprixht" => "Prix HT\nunitaire",
    "DEtype" => "Type\nmontant",
    "DEduree" => "Dur�e\n(jours)\n",
    "DEtrancheduree" => "Tranche\ntarifaire\n",
    "DEdatedd" => "Date d�but\npr�vue",
    "DEtranspht" => "Transport HT",
    "DEaller" => "Aller",
    "DEretour" => "Retour",

    "DEprixnet" => "Prix nets      ",
    "DEinfo01" => "Sous r�serve de disponibilit� - Devis valable %nbjours_val_devis% jours",
    "DEinfo17" => "La dur�e minimum de location est fix�e � 3 jours",
    "DEinfocl1" => "A d�faut, en cas de perte ou de d�t�rioration du mat�riel lou�, celui-ci sera factur� au prix du mat�riel neuf + TVA.",

    // par d�faut
    "DEinfo02" => "Le carburant, le nettoyage de la machine et la participation au recyclage seront factur�s en fin de contrat.\nNettoyage standard : 12 � pour une �lectrique, 16 � pour une diesel. ",
    // suivant le mode de calcul de la participation au recyclage
    "DEinfo02-P" => "Le carburant et le nettoyage de la machine seront factur�s en fin de contrat.\nNettoyage standard : 12 � pour une �lectrique, 16 � pour une diesel. ",
    "DEinfo02-F" => "Le carburant, le nettoyage de la machine et la participation au recyclage seront factur�s en fin de contrat.\nNettoyage standard : 12 � pour une �lectrique, 16 � pour une diesel. ",
    "DEinfo02-0" => "Le carburant sera factur� en fin de contrat.\n",
    "DEinfo02-1" => "Le carburant et le nettoyage de la machine seront factur�s en fin de contrat.\nNettoyage standard : 12 � pour une �lectrique, 16 � pour une diesel. ",
    "DEinfo02-2-P" => "Le carburant sera factur� en fin de contrat.\n",
    "DEinfo02-2-F" => "Le carburant et la participation au recyclage seront factur�s en fin de contrat.\n",
    "DEinfo02-3" => "Participation au recyclage : %residuesLabel%.",
    "DEappealLabel" => "Gestion de recours : %appealLabel%.",

    "DEinfo03" => "Conditions de r�glement (cf art. 6.2 des Conditions G�n�rales de Location) :",
    "DEinfo04" => " ",
    "DEinfo05" => "Attention :",
    "DEinfo06" => "les prix indiqu�s ci-dessus ne sont valables que si la dur�e de location est maintenue.",
    "DEinfo18" => "",
    "DEinfo07" => "Commentaire :",
    "DEinfo08" => "Conditions g�n�rales :",
    "DEinfo09" => "l'acceptation du devis, dont les termes seront r�it�r�s aux conditions particuli�res emporte acceptation sans r�serve des Conditions G�n�rales de location figurant en page 2 ou dont le client reconna�t avoir ant�rieurement eu connaissance, notamment lors de l�ouverture de compte.",
    "DEinfo10" => "Tribunal comp�tent :",
    "DEinfo11" => "pour tout litige, les parties attribuent comp�tence exclusive au Tribunal de commerce d�Agen (47), nonobstant appel en garantie, ou pluralit� de d�fendeurs et m�me pour les proc�dures en r�f�r� ou par requ�te.",
    "DEinfo12" => "Le service commercial :",
    "DEinfo13" => "Bon pour accord\nCachet et signature :",
    "DEinfo14" => "Le ____ /____ /_____",
    "DEinfo15" => str_replace("\n", ' - ', $rcs),
    "DEinfo16" => "1/2",
    "DEpriseElec" => "Les chargeurs des chariots �lectriques sont livr�s sans prises. La mise en ad�quation de la prise au compteur de chantier est � la charge du locataire.",
);

require_once($tabParams['oldFrameworkPath'] . '/inc/fpdf_multicelltagfit.class.php');
$pdf = new FPDF_MULTICELLTAGFIT( 'P','mm','A4' );
$pdf->AliasNbPages();
$pdf->Open();

$tabInfos = (array)$tabParams['tabParams'];
$tabInfos['tabLines'] = (array)$tabInfos['tabLines'];

foreach ($tabInfos as &$info)
{
    $info = (!is_array($info) ? mb_convert_encoding($info, 'cp1252', 'utf8') : $info);
}
foreach ($tabInfos['tabLines'] as &$line)
{
    $line = (array)$line;
}

// Documents PDF suppl�mentaires � ins�rer
$tabAdditionalDocuments = (array)$tabParams['tabAdditionalDocuments'];
foreach ($tabAdditionalDocuments as &$line)
{
    $line = (array)$line;
}

// - Documents "avant"
displayAdditionalDocuments($pdf, $tabAdditionalDocuments, 'before');


// NOUVELLE PAGE
$pdf->AddPage();

$pdf->SetMargins( 19, 8 );
$pdf->SetAutoPageBreak( false );
setWatermark($pdf, $tabParams, $traduction);

/***********************
G�n�ration du pdf DEVIS
************************/

// D�finition des balises de style
$pdf->SetStyle("a8","arial","",8,"0,0,0");
$pdf->SetStyle("a9","arial","",9,"0,0,0");
$pdf->SetStyle("a","arial","",10,"0,0,0");

$pdf->SetStyle("ab","arial","B",10,"0,0,0");
$pdf->SetStyle("ab8","arial","B",8,"0,0,0");
$pdf->SetStyle("ab9","arial","B",9,"0,0,0");

$pdf->SetStyle("ai8","arial","I",8,"0,0,0");
$pdf->SetStyle("ai7","arial","I",7,"0,0,0");

//Block 1 (Logo + Titre)
$bloc_x = 19;
$bloc_y = 8;

/***********************
Configuration sp�cifiques
************************/
$fontSize1 = 9;
$fontSize2 = 10;

$pdf->Image($tabParams['logoImg'], $bloc_x, $bloc_y, 60);

$pdf->SetXY( $bloc_x, $bloc_y + 23 );
$pdf->MultiCellTag( 70, 4, "<ab>".$traduction['DEdevisn']."</ab> <a>DEV-COM-".$tabInfos['AGAUTO'].$tabInfos['DEVISAUTO'].($tabInfos['DEVISFULLSERVICE'] == -1 ? $traduction['DEfullservice'] : '')."</a>", 0, 0, "L" );


$pdf->SetXY( $bloc_x, $bloc_y + 27 );
$pdf->MultiCellTag( 72, 4, "<ab>".$traduction['DEcommande']."</ab> <a>".$tabInfos['DEVISNUMCOM']."</a>", 0, 0, "L" );


$pdf->SetFont('Arial','B',10);
$pdf->SetXY( $bloc_x + 70, $bloc_y );
$pdf->MultiCell( 100, 4, $traduction['DEtitre'], 0, 1, "L" );

$pdf->SetXY( $bloc_x + 70, $bloc_y +12 );
$pdf->SetFont('Arial','B',10);
$pdf->MultiCellTag( 100, 4, $traduction['DEagence'].$tabInfos['AGLIBELLE'], 0, 0, "L" );

$pdf->SetXY( $bloc_x + 70, $bloc_y + 18 );
$pdf->SetFont('Arial','',10);
//DE10366: PDF devis de location Portugal : suppression du portable du responsable d'agence
$pdf->MultiCellTag( 200, 4, $tabInfos['INTERLOCPRENOM']."  ".$tabInfos['INTERLOCNOM']."  ".$tabInfos['INTERLOCPORTABLE']."\n".$tabInfos['AGADRESSE']."\n".$tabInfos['AGCP']." ".$tabInfos['AGVILLE']."\n".$traduction['DEtel']." ".( $traduction['NumPlateformeTel'] != "" ? $traduction['NumPlateformeTel'] : $tabInfos['AGTEL'] )."  <ab>".$traduction['DEfax']." ".( $traduction['NumPlateformeFax'] != "" ? $traduction['NumPlateformeFax'] : $tabInfos['AGFAX'] )."</ab>  ".$traduction['DEle']." ".date('d/m/Y'), 0, "L" );

//Block 2 (Info client + Chantier)
$bloc_x = 19;
$bloc_y = 44;
$pdf->Rect( $bloc_x, $bloc_y, 172, 20 );
$pdf->SetXY( $bloc_x, $bloc_y + 1.5 );
$pdf->MultiCellTag( 172, 3.5, "<ab9>".$traduction['DEclient']."</ab9> <a9>".$tabInfos['CLSTE']."</a9>", 0, 0, "L" );


$pdf->SetXY( $bloc_x, $bloc_y + 4.7 );
$pdf->CellFitScale(200, 3.5, $tabInfos['CLADRESSE']);
$pdf->SetXY( $bloc_x, $bloc_y + 8.4 );
$pdf->CellFitScale(200, 3.5, $tabInfos['CLADRESSE2']);
$pdf->SetXY( $bloc_x, $bloc_y + 12 );
$pdf->CellFitScale(200, 3.5, $tabInfos['VICLCP']." ".$tabInfos['VICLNOM']);
$pdf->SetXY( $bloc_x, $bloc_y + 16 );
$pdf->SetFont('Arial','B',9);
$pdf->MultiCellTag( 160, 3.5, $traduction['DEtel']."<a9>".$tabInfos['CLTEL']."</a9> ".$traduction['DEfax']."<a9>".$tabInfos['CLFAX']."</a9>", 0, "L" );


$pdf->SetXY( $bloc_x, $bloc_y + 21.5 );
$pdf->SetFont('Arial','B',9);
$pdf->Rect( $bloc_x, $bloc_y +20, 172, 16 );
$pdf->MultiCellTag( 172, 3.5, $traduction['DEinfoch']." <a9>".$tabInfos['CHLIBELLE']."</a9>", 0, "L" );

$pdf->SetXY( $bloc_x, $bloc_y + 25 );
$pdf->SetFont('Arial','',9);
$pdf->MultiCell( 178, 3.5, str_replace( "\n", " ", substr($tabInfos['CHADRESSE'], 0, 88) ), 0, "L" );
$pdf->MultiCell( 172, 3.5, $tabInfos['VICP']." ".$tabInfos['VINOM'], 0, "L" );

$pdf->SetXY( $bloc_x , $bloc_y + 32 );
$pdf->SetFont('Arial','B',9);
$pdf->CellFitScale( 30, 3.5, $traduction['DEinfoch2'], 0, "L" );
$pdf->SetXY( $bloc_x + 30 , $bloc_y + 32 );
$pdf->SetFont('Arial','',9);
$pdf->CellFitScale( 56, 3.5, $tabInfos['CHCONTACT']." / ". $tabInfos['CHTELEPHONE'], 0, 0, "L" );
$pdf->SetXY( $bloc_x + 30 + 56 , $bloc_y + 32 );
$pdf->SetFont('Arial','B',9);
$pdf->CellFitScale( 30, 3.5, $traduction['DEinfoch3'], 0, "L" );
$pdf->SetXY( $bloc_x + 30 + 56 + 30 , $bloc_y + 32 );
$pdf->SetFont('Arial','',9);
$pdf->CellFitScale( 56, 3.5, $tabInfos['DEVISCONTACTCOM']." / ". $tabInfos['DEVISTELEPHONECOM'], 0, 0, "L" );

$pdf->Rect( $bloc_x +133.50, $bloc_y +47, 38.50, 15 );

//Block 3 (Info client + Chantier)
$bloc_x = 19;
$bloc_y = 83;

$pdf->SetXY( $bloc_x, $bloc_y );
$pdf->SetFont('Arial','B',14);
$pdf->MultiCell( 172, 3.5, $traduction['DEtitre2'], 0, "C" );

$pdf->SetXY( $bloc_x, $bloc_y + 8 );
$pdf->SetFont('Arial','B',8);
$pdf->MultiCell( 52.5, 7.5, $traduction['DEfamille'], 1, "C" );

// Recherche d'une quantit� diff�rente de 1 sur au moins une lignes de devis
$tabFilter = array_filter($tabInfos['tabLines'], function($var) {return $var['DETAILSDEVISQTE'] != 1;});
$displayColumnSliceOfDuration = (count($tabFilter) == 0);

// Tableau contenant les tranches de dur�e tarifaires
$tabSlice = getSlices($tabParams['tabMinDays'], $traduction);

/* Si toute les quantit�s de chaque ligne de devis est � 1 :
** Ne pas afficher la colonne quantit�, afficher la colonne "tranche tarifaire" apres la colonne "dur�e"
*/
if (!$displayColumnSliceOfDuration)
{
    $pdf->SetXY( $bloc_x + 52.5, $bloc_y + 8 );
    $pdf->MultiCell( 15.5, 7.5, $traduction['DEqte'], 1, "C" );
    $pdf->SetXY( $bloc_x + 68, $bloc_y + 8 );
    $pdf->MultiCell( 17.5, 7.5, $traduction['DEprixht'], 1, "C" );
    $pdf->SetXY( $bloc_x + 85.5, $bloc_y + 8 );
    $pdf->MultiCell( 15, 7.5, $traduction['DEtype'], 1, "C" );
    $pdf->SetXY( $bloc_x + 100.5, $bloc_y + 8 );
    $pdf->MultiCell( 12.5, 7.5, $traduction['DEduree'], 1, "C" );
    $pdf->SetXY( $bloc_x + 113, $bloc_y + 8 );
    $pdf->MultiCell( 20, 7.5, $traduction['DEdatedd'], 1, "C" );
}
else
{
    $pdf->SetXY( $bloc_x + 52.5, $bloc_y + 8 );
    $pdf->MultiCell( 15.5, 7.5, $traduction['DEprixht'], 1, "C" );
    $pdf->SetXY( $bloc_x + 68, $bloc_y + 8 );
    $pdf->MultiCell( 17.5, 7.5, $traduction['DEtype'], 1, "C" );
    $pdf->SetXY( $bloc_x + 85.5, $bloc_y + 8 );
    $pdf->MultiCell( 13, 7.5, $traduction['DEduree'], 1, "C" );
    $pdf->SetXY( $bloc_x + 98.5, $bloc_y + 8 );
    $pdf->MultiCell( 16, 7.5, $traduction['DEtrancheduree'], 1, "C" );
    $pdf->SetXY( $bloc_x + 114.5, $bloc_y + 8 );
    $pdf->MultiCell( 18.5, 7.5, $traduction['DEdatedd'], 1, "C" );
}
$pdf->SetXY( $bloc_x + 133, $bloc_y + 8 );
$pdf->Cell( 39, 15, $traduction['DEtranspht'], 1, 1, "C" );

$lc1 = 6; //largeur cellule 1
$lc2 = 7; //largeur cellule 2
$gripElect = false;

// Affichage des lignes
foreach ($tabInfos['tabLines'] as $key => $value)
{
    if ($value['DETAILSDEVISREMISEEX'] == 0)
    {
        $librem = " ";
    }
    else
    {
        $librem = $traduction['DElibrem'];
    }

    if ($value['DETAILSDEVISREMISEEX'] == 0)
    {
        $remiseex = " ";
    }
    else
    {
        $remiseex = round($value['DETAILSDEVISREMISEEX'], 3)." %";
    }

    $pdf->SetFont('Arial','',9);
    $pdf->CellFitScale( 52.5, $lc1, $value['FAMAMODELE']." ".$value['FAMAENERGIE']." ".$value['FAMAELEVATION'], 1, 0, "L" );

    // Type de montant
    if ($value['DETAILSDEVISTYPEMONTANT'] == 'day')
    {
        $typemontant = $traduction['DEtjour'];
    }
    else
    {
        $typemontant = $traduction['DEtmois'];
    }

    if (!$displayColumnSliceOfDuration)
    {
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell( 15.5, $lc1, $value['DETAILSDEVISQTE'], 1, 0, "L" );
    }

    $pdf->SetFont('Arial','',9);
    $pdf->Cell((!$displayColumnSliceOfDuration ? 17.5 : 15.5), $lc1, round($value['DETAILSDEVISPXJOURSFR'], 0)." ".$tabInfos['PAMONNAIE'], 1, 0, "R" );

    $pdf->SetFont('Arial','',$fontSize1);
    $pdf->Cell((!$displayColumnSliceOfDuration ? 15 : 17.5), $lc1, $typemontant, 1, 0, "C" );
    $pdf->SetFont('Arial','B',$fontSize1);
    $pdf->Cell((!$displayColumnSliceOfDuration ? 12.5 : 13), $lc1, $value['DETAILSDEVISDUREE'] * 1, 1, 0, "C" );

    if ($displayColumnSliceOfDuration)
    {
        $pdf->SetFont('Arial','', 8);
        $pdf->Cell( 14+2, $lc1, getSliceLabelFromDuration($tabSlice, $value['DETAILSDEVISDUREE']), 1, 0, "C" );
    }

    $pdf->SetFont('Arial','', $fontSize1);
    $pdf->Cell((!$displayColumnSliceOfDuration ? 20 : 18.5), $lc1, date('d/m/Y', strtotime($value['DATEDETAILSDEVISDATE'])), 1, 0, "C" );
    $pdf->SetFont('Arial','',$fontSize1);
    $pdf->Cell( 0.50, $lc1, "", 1, 0, "R" );
    $pdf->Cell( 18.50, $lc1, $traduction['DEaller'], 1, 0, "C" );
    $pdf->Cell( 20, $lc1, $traduction['DEretour'], 1, 1, "C" );


    $pdf->SetFont('Arial','',8);
    $pdf->Cell( 52.5, $lc2, $librem, 1, 0, "L" );
    $pdf->Cell( 80.5, $lc2, $remiseex, 1, 0, "C" );
    $pdf->Cell( 0.45, $lc2, "", 1, 0, "R" );
    $pdf->Cell( 38.55, $lc2, "", 1, 1, "R" );
    $pdf->SetFont('Arial','B',$fontSize2);
    $pdf->Cell( 52.5, $lc2, $traduction['DEprixnet'], 1, 0, "L" );
    $pdf->Cell( 80.5, $lc2, round($value['PRIXJOUREU'], 0)." ".$tabInfos['PAMONNAIE'] . ' / ' . $typemontant, 1, 0, "C" );
    $pdf->Cell( 0.50, $lc2, "", 1, 0, "R" );
    $pdf->Cell( 18.50, $lc2 , round($value['DETAILSDEVISTRD'], 0)." ".$tabInfos['PAMONNAIE'], 1, 0, "R" );
    $pdf->Cell( 20, $lc2, round($value['DETAILSDEVISTRR'], 0)." ".$tabInfos['PAMONNAIE'], 1, 1, "R" );
    $pdf->Cell( 171, 0.7, "", 0, 1, "C" );

    if ($value['FAMAENERGIE'] == "ELECT" & $value['fly_id'] == 12)
    {
    	$gripElect = true;
    }
}

// Bloc 4 Conditions g�n�rale de location
$bloc_x = 19;
$bloc_y = 192;

$pdf->SetXY( $bloc_x, $bloc_y - 1 );
$pdf->SetFont('Arial','IB',11);
$nbValidDays = $tabParams['nbValidDays'];
$libelle = str_replace('%nbjours_val_devis%', $nbValidDays, $traduction['DEinfo01']);
if ($tabInfos['CLPOTENTIEL'] == 'B' || $tabInfos['CLPOTENTIEL'] == 'C' || $tabInfos['CLPOTENTIEL'] == '')
{
    $libelle .= ' - ' . $traduction['DEinfo17'];
}
$pdf->CellFitScale( 172, 3.5, $libelle, 0, 0, "L" );

$pdf->Rect( $bloc_x, $bloc_y + 4, 172, 35 );
$pdf->SetXY( $bloc_x, $bloc_y + 4.5 );
$pdf->SetFont('Arial','B',9);
// Condition pour detecter le pourcentage d'assurance en fonction de la date de d�but
if (str_replace("-", "", $tabInfos['CLDATEDEBASS']) < date("Ymd"))
{
    $passurance  = $tabInfos['CLPASSURANCE'];
}
else
{
    $pourcentage = explode(":", $tabInfos['PAASSURANCE']);
    $passurance  = $pourcentage[0];
}
// Assurance, gazole et nettoyage
if ($tabInfos['insurance.isInvoiced'])
{
    $tabPAss = $tabInfos['DEVISPASSURANCE'];
    $t_temp = array_unique(array_values($tabPAss));
    $passurance = (count($t_temp) == 1 ? $t_temp[0] : $tabInfos['CLPASSURANCE']);
}

$libAssurance = str_replace('<%percent>', $passurance * 100 . ' %' , $tabInfos['insurance.label']);

if ($tabInfos['insurance.isInvoiced'])
{
    $libAssurance .= '. ' . $traduction['DEinfocl1'];
}

// Gestion de recours
if ($tabInfos['appeal.isInvoiced'])
{
    $appealLabel = $traduction['DEappealLabel'];
    $libAssurance .= "\n" . str_replace("%appealLabel%", $tabInfos['appeal.label'], $appealLabel);
}

// France uniquement
$cptCleaningInvoiced = 0;
$cptResiduesInvoiced = 0;
$residuesMode        = 0;
$residuesValue       = null;
$label = '';

foreach ($tabInfos['tabLines'] as $key => $value)
{
    if ($value['cleaning.type.id'] != 0)
    {
        $cptCleaningInvoiced++;
    }
    if ($value['residues.mode'] != 0)
    {
        $cptResiduesInvoiced++;
        $residuesMode  = $value['residues.mode'];
        $residuesValue = $value['residues.value'];

        // si on est dans le mode de calcul au pourcentage,
        // alors c'est ce mode qui fait r�f�rence
        if ($residuesMode == 2)
        {
            break;
        }
    }
}

// Le nettoyage est-il factur�?
$isCleaningInvoiced = ($cptCleaningInvoiced > 0 ? 1 : 0);
// La participation au recyclage est-elle factur�e?
$isResiduesInvoiced = ($cptResiduesInvoiced > 0 ? 1 : 0);

// Mode de participation au recyclage
$suffix = '';
if ($residuesMode == 1)
{
    $residuesLabel = number_format($residuesValue, 2) . ' �';
    $suffix = '-F';
}
elseif ($residuesMode == 2)
{
    $residuesLabel = $residuesValue * 100 . '% de la location';
    $suffix = '-P';
}

// Nettoyage et participation non factur�s
if (!$isCleaningInvoiced && !$isResiduesInvoiced)
{
    $label = $traduction['DEinfo02-0'];
}
// Uniquement le nettoyage factur�
elseif (!$isResiduesInvoiced)
{
    $label = $traduction['DEinfo02-1'];
}
// Uniquement la participation au recyclage factur�
elseif (!$isCleaningInvoiced)
{
    $label = $traduction['DEinfo02-2' . $suffix] . $traduction['DEinfo02-3'];
    $label = str_replace("%residuesLabel%", $residuesLabel, $label);
}
// Nettoyage et participation au recyclage factur�s
else
{
    $label = $traduction['DEinfo02' . $suffix] . $traduction['DEinfo02-3'];
    $label = str_replace("%residuesLabel%", $residuesLabel, $label);
}

$pdf->MultiCell( 172, 3, $libAssurance . "\n" . $label, 0, "L" );

if ($gripElect)//condition FR, CHARIOT IND/FRONT et elect
{
	$pdf->MultiCellTag( 172, 3.5, "<pb3>".$traduction['DEpriseElec']."</pb3>", 0, "L" );
}

// Conditions de r�glement
$pdf->SetXY( $bloc_x, $bloc_y + 27 );
$pdf->MultiCellTag( 172, 3.5, "<a8>".$traduction['DEinfo03']."</a8> <ab>".$tabInfos['MOPALIBELLE']."</ab>", 0, 0, "L" );

// Conditions g�n�rales
if ($tabInfos['CLCGVENTE'] == -1)
{
    $pdf->SetXY( $bloc_x, $bloc_y + 30 );
    $pdf->MultiCellTag( 172, 3.5, "<a8>".$traduction['DEinfo04']."</a8>", 0, 0, "L" );
}

// Validit� des prix
$add_y = 36;
$pdf->SetXY( $bloc_x, $bloc_y + $add_y );
$pdf->SetFont('Arial', 'B', 8);
$pdf->Write( 3, $traduction['DEinfo05'] . ' ' . $traduction['DEinfo06'] );

// Commentaires
$pdf->SetXY( $bloc_x, $bloc_y + 40 );
$pdf->SetFont('Arial','B',9);
$pdf->Cell( 15, 3.5, $traduction['DEinfo07'], 0, 1, "L" );
$pdf->SetFont('Arial','',9);
$pdf->MultiCell( 170, 3.5, $tabInfos['DEVISCOMMENTAIRE'], 0, "L" );

// Conditions g�n�rales
$pdf->SetXY( $bloc_x, $bloc_y + 55 );
$pdf->SetFont('Arial','UB',9);

$pdf->Write( 3, mb_strtoupper($traduction['DEinfo08']) );
$pdf->SetFont('Arial','',7);
if ($traduction['DEinfo08'] != '')
{
    $pdf->Write( 3, ' ' );
}
$pdf->Write( 3, $traduction['DEinfo09'] );

// Tribunal comp�tent
$pdf->SetXY( $bloc_x, $bloc_y + 65 );
$pdf->SetFont('Arial','UB',9);
$pdf->Write( 3, mb_strtoupper($traduction['DEinfo10']) );
$pdf->SetFont('Arial','B',9);
if ($traduction['DEinfo10'] != '')
{
    $pdf->Write( 3, ' ' );
}
$pdf->Write( 3, $traduction['DEinfo11']);

// Bloc signatures
$pdf->SetXY( $bloc_x, $bloc_y + 75 );
$pdf->SetFont('Arial','B',10);
$pdf->Cell( 85, 3.5, $traduction['DEinfo12'], 0, 0, "L" );
$pdf->MultiCell( 80, 3.5, $traduction['DEinfo13'], 0, "L" );

$pdf->SetXY( $bloc_x + 85, $bloc_y + 86 );
$pdf->SetFont('Arial','',10);
$pdf->Cell( 85, 3.5, $traduction['DEinfo14'], 0, 0, "L" );

// Ligne juridique Acces industrie
$pdf->SetXY( $bloc_x, $bloc_y + 94 );
$pdf->SetFont('Arial','',7);
$pdf->CellFitScale( 172, 3, str_replace( "%capital_acces%", mb_convert_encoding($tabParams['capital'], 'cp1252', 'utf8'), $traduction['DEinfo15'] ), 0, 0, "C" );


// - Documents "apr�s"
displayAdditionalDocuments($pdf, $tabAdditionalDocuments, 'after');


$pdf->Output( "document.pdf", "I" );

/**
 * Affiche des documents suppl�mentaires
 * @param $pdf
 * @param array $tabAdditionalDocuments - Tableau des documents
 * @param string $zone                  - Zone d'affichage
 */
function displayAdditionalDocuments(&$pdf, $tabAdditionalDocuments, $zone = 'before')
{
    if (array_key_exists($zone, $tabAdditionalDocuments))
    {
        foreach ($tabAdditionalDocuments[$zone] as $documentInfos)
        {
            $documentInfos = (array)$documentInfos;
            if ($documentInfos['active'] && file_exists($documentInfos['path']))
            {
                $pageCount = $pdf->setSourceFile($documentInfos['path']);
                for ($i = 1; $i <= $pageCount; $i++)
                {
                    $pdf->addPage();
                    // importe une page
                    $templateId = $pdf->importPage($i);
                    // utilise la page
                    $pdf->useTemplate($templateId);
                }
            }
        }
    }
}

function setWatermark(&$pdf, $tabParams, $traduction)
{
    if ($tabParams['env'] != 'exp')
    {
        $pdf->SetFont('Arial', 'BI', 72);

        $textWatermark = mb_strtoupper ($traduction['Specimen']);
        $xWatermark = $pdf->w / 2;
        $yWatermark = $pdf->h / 2;
        $rWatermark = 30;

        $rWatermark *= M_PI / 180;
        $cosWatermark = cos($rWatermark);
        $sinWatermark = sin($rWatermark);
        $cx = $xWatermark * $pdf->k;
        $cy = ($pdf->h - $yWatermark) * $pdf->k;
        $cx = 0;
        $cy = 0;

        $pdf->_out(
        sprintf('q %.3f g %.5f %.5f %.5f %.5f %.2f %.2f cm',
        203 / 255,
        $cosWatermark,
        $sinWatermark,
        -$sinWatermark,
        $cosWatermark,
        ($pdf->w / 2 - $pdf->GetStringWidth($textWatermark) / 2 * cos($rWatermark)) * $pdf->k,
        $pdf->h / 2 * $pdf->k - $pdf->GetStringWidth($textWatermark) / 2 * sin($rWatermark) * $pdf->k
        )
        );
        $pdf->_out(
        sprintf('BT (%s) Tj ET',
        $pdf->_escape($textWatermark)
        )
        );

        $pdf->_out('Q');
    }
}


/**
 * R�cup�ration des tranches de dur�e
 * @return array - Liste des tranches de dur�es :
 */
function getSlices($tabMinDays, $traduction)
{
    $tabSlice = array();

    $nbSlices = count($tabMinDays);
    for ($i = 0; $i < $nbSlices; $i++)
    {
        $minDays = $tabMinDays[$i];
        $tabSlice[$minDays] = array();

        // Borne inf�rieure et sup�rieure de la tranche
        $tabSlice[$minDays]['minDays'] = $minDays;
        $tabSlice[$minDays]['maxDays'] = (isset($tabMinDays[$i + 1]) ? $tabMinDays[$i + 1] : null);

        // Libell� de la tranche tarifaire
        if ($tabSlice[$minDays]['minDays'] == 1)
        {
            $tabSlice[$minDays]['label']   = $tabSlice[$minDays]['minDays'] . ' ' . $traduction['DEjour'];
            $tabSlice[$minDays]['minDays'] = null;
        }
        elseif ($tabSlice[$minDays]['minDays'] >= 2 && $tabSlice[$minDays]['minDays'] < 20)
        {
            $tabSlice[$minDays]['label']   = $tabSlice[$minDays]['minDays'] . '-' .  ($tabSlice[$minDays]['maxDays'] - 1) . ' ' . $traduction['DEjours'];
        }
        else
        {
            $tabSlice[$minDays]['label']   = '+' . $tabSlice[$minDays]['minDays'] . ' ' . $traduction['DEjours'];
        }
    }

    return $tabSlice;
}


/**
 * R�cup�ration du libell� de la tranche de dur�e en fonction de la dur�e
 * @param  $tabSlice    - Tableau des tranches
 * @param  $duration    - Dur�e
 * @return string       - Libell� de la tranche de dur�e
 */
function getSliceLabelFromDuration($tabSlice, $duration)
{
    foreach ($tabSlice as  $key => $value)
    {
        if (($value['minDays'] == '' || $duration >= $value['minDays']) &&
        ($value['maxDays'] == '' || $duration < $value['maxDays']))
        {
            return $value['label'];
        }
    }

    return '';
}

?>