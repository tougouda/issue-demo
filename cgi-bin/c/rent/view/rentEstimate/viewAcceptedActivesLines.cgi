use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;


# Répertoire CSS/JS/Images
my $dir = 'rent/rentEstimate/';


our %tabViewData;


my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
my $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


my $currentAreaId = $tabViewData{'currentAreaId'};

# URL courante 
my $currentUrl = $view->createURL('rent:rentEstimate:viewAcceptedActivesLines', {'areaId' => $currentAreaId});



# Feuilles de style
$view->addCSSSrc('location.css');

$view->addCSSSrc($dir . 'viewAcceptedActivesLines.css');

# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc('jQuery/plugins/tablesorter.js');


$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);


my $title = $locale->t('Lignes de devis acceptées');
$view->setPageTitle($title);
$view->setTitle($title);


# Bouton "Actualiser"
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png', $currentUrl, $locale->t('Actualiser la page'));
$view->addControlsContent($refreshBtn, 'right', 1);


# Tableau des lignes de devis acceptées
my @tabEstimateLines = values %{$tabViewData{'tabEstimateLines'}};


# Liste des couleurs
my $tabColors = [
    ['#ccffcf', '#ddffdf'],
    ['#ccffff', '#ddffff'],
    ['#fae0ec', '#fbebf2'],
    ['#ffffcc', '#ffffdd'],
    ['#f6f6fb', '#ffffff'],
    ['#d9d5f7', '#e6e3fa'],

    ['#ccffcf', '#ddffdf'], # Répétition des couleurs (en cas)
    ['#ccffff', '#ddffff'],
    ['#fae0ec', '#fbebf2'],
    ['#ffffcc', '#ffffdd'],
    ['#f6f6fb', '#ffffff'],
    ['#d9d5f7', '#e6e3fa']
];


# Comptage des lignes par agence
my $tabLinesCountByAgency = {};
foreach my $tabEstimateInfos (@tabEstimateLines)
{
    my $agencyId = $tabEstimateInfos->{'estimate.agency.id'};
    if (!exists $tabLinesCountByAgency->{$agencyId})
    {
        $tabLinesCountByAgency->{$agencyId} = 0;
    }
    $tabLinesCountByAgency->{$agencyId}++;
}



my $filterHtml = '
<form action="' . $view->createURL('rent:rentEstimate:viewAcceptedActivesLines') . '" method="post">
<div class="filters">';

my $nbAreas = keys %{$tabViewData{'tabAreas'}};
if ($nbAreas > 1)
{
    my @tabAreasList = ();
    foreach my $tabAreaInfos (values %{$tabViewData{'tabAreas'}})
    {
        push(@tabAreasList, {
            'value'     => $tabAreaInfos->{'id'},
            'innerHTML' => ($tabAreaInfos->{'name'} eq '' ? '-' . $locale->t('Autres') . '-' : $tabAreaInfos->{'name'})
        });
    }

    # Tri des régions par nom
    @tabAreasList = sort {$a->{'value'} cmp $b->{'value'}} @tabAreasList;

    $filterHtml .= '
    <span class="label">' . htmllocale('Région') . ': </span>' .
            $view->displayFormSelect('areaId', \@tabAreasList, $currentAreaId, 0,
                                     {'onchange' => 'window.document.forms[0].submit(); this.disabled = true;'});
}

$filterHtml .= '
    <span class="label">' . htmllocale('Agences') . ': </span>
    <div class="agencies">';

my $colorStyles = '';
my $colorIndex  = 0;

# Liste des agences de la région
my @tabAgencies = values %{$tabViewData{'tabAreas'}->{$currentAreaId}->{'tabAgencies'}};
# Tri des agences par code
@tabAgencies = sort {$a->{'id'} cmp $b->{'id'}} @tabAgencies;

foreach my $tabAgencyInfos (@tabAgencies)
{
    my $agencyId   = $tabAgencyInfos->{'id'};
    my $agencyName = $tabAgencyInfos->{'name'};
    my $count = ($tabLinesCountByAgency->{$agencyId} || 0);

    $filterHtml .= '
        <div class="agency agc_' . $agencyId . ($count > 0 ? ' on' : '') . '" title="' . $view->toHTMLEntities($agencyName) . '"' .
                ($count > 0 ? ' onclick="toggleAgency(\'' . $agencyId . '\');"' : '') . '>' .
            '<span class="name">' . $agencyId . '</span>' .
            '<span class="count">' . $count . '</span>' .
       '</div>';

    # Affactation d'un couleur
    $colorStyles .= '
table.list > tbody > tr.agc_' . $agencyId . '.even
{
    background-color: ' . $tabColors->[$colorIndex]->[0] . ';
}
div.filters > div.agencies > div.agency.on.agc_' . $agencyId . ',
table.list > tbody > tr.agc_' . $agencyId . '.odd
{
    background-color: ' . $tabColors->[$colorIndex]->[1] . ';
}';
    $colorIndex++;
}
$filterHtml .= '
    </div>
</div>
</form>';

$view->addControlsContent($filterHtml, 'left', 1);


# Bouton d'extraction excel
my $excelBtn = $view->displayTextButton($locale->t('Extraction Excel'), 'excel(|Over).gif',
                                        $view->createURL('rent:rentEstimate:viewAcceptedActivesLines', {'areaId' => $currentAreaId, 'displayType' => 'xls'}),
                                        $locale->t('Extraction au format Excel'));

$view->addControlsContent($excelBtn, 'right', 0);



$view->addJSSrc('
function toggleAgency(id)
{
    var el = $("div.filters > div.agencies > div.agency.agc_" + id);
    if (el.hasClass("on"))
    {
        el.removeClass("on");
        $("table.list > tbody > tr.agc_" + id).css("display", "none");
    }
    else
    {
        el.addClass("on");
        $("table.list > tbody > tr.agc_" + id).css("display", "");
    }

    // Met à jour le tableau (widget zebra)
    $("table.basic.list").trigger("update");
    $("table.basic.list").trigger("appendCache");
}');


# Couleurs
$view->addCSSSrc($colorStyles);


# Affichage de l'entête
print $view->displayHeader();


# On n'affiche pas le tableau si il est vide
if (@tabEstimateLines == 0)
{
    # Pas de résultat
    print '<div class="noResult">' . $view->displayMessages('info', [$locale->t('Aucune ligne de devis n\'est acceptée')], 0) . '</div>';
}
else
{
    # Options pour tablesorter
    my $headersOption = '
2  : {sorter: "integerAttr"},
3  : {sorter: "textAttr"},
5  : {sorter: "integerAttr"},
10 : {sorter: "integerAttr"},
11 : {sorter: "integerAttr"},
12 : {sorter: "integerAttr"},
14 : {sorter: "integerAttr"}';
    my $sortListOption = '[[0,0],[3,0]]'; # Tri par défaut: "Agence" (ASC) et "Date départ" (ASC)

    print '
<table class="basic list">
<thead>
    <tr>
        <th class="agency">' . htmllocale('Agence') . '</th>
        <th class="code">' . htmllocale('Devis') . '</th>
        <th class="datesToConfirm" title="' . htmllocale('Dates à confirmer') . '"></th>
        <th class="beginDate">' . htmllocale('Date départ') . '</th>
        <th class="deliveryHour">' . htmllocale('Livraison') . '</th>
        <th class="isAnticipatedDlv"><span title="' . htmllocale('Livraison anticipée possible') . '">' . htmllocale('Liv. anticipée poss.') . '</span></th>
        <th class="model">' . htmllocale('Désignation') . '</th>
        <th class="site">' . htmllocale('Ville chantier') . '</th>
        <th class="zone">' . htmllocale('Zone') . '</th>
        <th class="customer">' . htmllocale('Client') . '</th>
        <th class="transportAmount">' . htmllocale('Montant livraison') . '</th>
        <th class="transportAddMach"><span title="' . htmllocale('Machine supplémentaire') . '">' . htmllocale('Machine supp.') . '</span></th>
        <th class="isNoDelivery"><span title="' . htmllocale('Enlèvement sur place') . ' (' . htmllocale('Livraison') . ')">' . htmllocale('Enlvt / place') . '</span></th>
        <th class="duration"><span title="' . htmllocale('Durée') . ' (' . htmllocale('en jours ouvrés') . ')' . '">' . htmllocale('Durée (j.)') . '</span></th>
        <th class="newAccount"><span title="' . htmllocale('Ouverture de compte à faire') . '">' . htmllocale('Ouv. cpte') . '</span></th>
    </tr>
</thead>
<tbody>';

    # Affichage des lignes
    foreach my $tabEstimateInfos (@tabEstimateLines)
    {
        my $agencyId = $tabEstimateInfos->{'estimate.agency.id'};

        # URL's
        my $contractUrl = $view->createURL('rent:rentEstimate:view', {'estimateId' => $tabEstimateInfos->{'estimate.id'},
                                                                      'estimateLineId' => $tabEstimateInfos->{'id'}});
        my $customerUrl = $tabViewData{'customerUrl'};

        my $customerId = $tabEstimateInfos->{'estimate.customer.id'};
        $customerUrl =~ s/\<\%id\>/$customerId/;

        # Commentaire chantier
        my $siteComment = '';
        if ($tabEstimateInfos->{'estimate.site.comments'} ne '')
        {
            my $label = $tabEstimateInfos->{'estimate.site.comments'};
            $siteComment = $view->displayTooltipIcon('comment', '<b>' . $locale->t('Commentaire') . ':</b><br />' . $label);
        }

        # Plage horaire de livraison
        my $timeSlot = '';
        if ($tabEstimateInfos->{'estimate.site.deliveryTimeSlot.id'})
        {
            my $label = $tabViewData{'tabTransportTimeSlots'}->{$tabEstimateInfos->{'estimate.site.deliveryTimeSlot.id'}}->{'label'};
            $timeSlot = $view->displayTooltipIcon('clock', '<b>' . $locale->t('Plage horaire') . ':</b><br />' . $label);
        }

        # Icône des dates à confirmer
        my $datesToConfirm = ($tabEstimateInfos->{'isDatesToConfirm'} ? '<div title="' . htmllocale('Dates à confirmer') . '"></div>' : '');

        print '
    <tr class="agc_' . $agencyId . '">
        <td class="agency">' . $agencyId . '</td>
        <td class="code"><a href="' . $contractUrl . '" target="_blank" title="' . $view->toHTMLEntities($tabEstimateInfos->{'estimate.code'}) . '"><b>' .
                                    $tabEstimateInfos->{'estimate.id'} . '</b><br />' .
                                    '<small><i>(' . htmllocale('ligne %s', $tabEstimateInfos->{'index'}) . ')</i></small></a></td>
        <td class="datesToConfirm" data-val="' . $tabEstimateInfos->{'isDatesToConfirm'} . '">' . $datesToConfirm . '</td>
        <td class="beginDate" data-val="' . $tabEstimateInfos->{'beginDate'} . '">' . $locale->getDateFormat($tabEstimateInfos->{'beginDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC) . '</td>
        <td class="deliveryHour">' . $tabEstimateInfos->{'estimate.site.deliveryHour'} . $timeSlot . '</td>
        <td class="isAnticipatedDlv" data-val="' . $tabEstimateInfos->{'estimate.site.isAnticipated'} . '">' . $view->displayBoolean($tabEstimateInfos->{'estimate.site.isAnticipated'}, '') . '</td>
        <td class="model" title="' . $view->toHTMLEntities($tabEstimateInfos->{'model.tabInfos'}->{'machinesFamily.fullName'}) . '">' . $view->toHTMLEntities($tabEstimateInfos->{'model.tabInfos'}->{'label'}) . '</td>
        <td class="site">' . $view->toHTMLEntities($tabEstimateInfos->{'estimate.site.locality.label'}) . $siteComment . '</td>
        <td class="zone">' . $view->displayZone($tabEstimateInfos->{'estimate.zone'}) . '</td>
        <td class="customer"><a href="' . $customerUrl . '" target="_blank">' . $view->toHTMLEntities($tabEstimateInfos->{'estimate.customer.name'}) . '</a></td>
        <td class="transportAmount" data-val="' . $tabEstimateInfos->{'transportTariff.deliveryAmount'} . '">' .
                                    $view->displayFormInputCurrency('transportAmount_' . $tabEstimateInfos->{'id'}, $tabEstimateInfos->{'transportTariff.deliveryAmount'}, LOC::Html::CURRENCYMODE_TEXT, $tabViewData{'currencySymbol'}) . '</td>
        <td class="transportAddMach" data-val="' . $tabEstimateInfos->{'transportTariff.isAddMachineDelivery'} . '">' . $view->displayBoolean($tabEstimateInfos->{'transportTariff.isAddMachineDelivery'}, '') . '</td>
        <td class="isNoDelivery" data-val="' . $tabEstimateInfos->{'transportTariff.isNoDelivery'} . '">' . $view->displayBoolean($tabEstimateInfos->{'transportTariff.isNoDelivery'}, '') . '</td>
        <td class="duration">' . $tabEstimateInfos->{'duration'} . '</td>
        <td class="newAccount" data-val="' . $tabEstimateInfos->{'estimate.customer.isNewAccount'} . '">' . $view->displayBoolean($tabEstimateInfos->{'estimate.customer.isNewAccount'}, '') . '</td>
    </tr>';
    }

    print '
</tbody>
</table>';

    # Tri du tableau
    print $view->displayJSBlock('
$.tablesorter.addParser({
    id: "textAttr",
    is: function(s) { return false; },
    format: function(s, table, cell, cellIndex) { return $(cell).attr("data-val"); },
    type: "text"
});
$.tablesorter.addParser({
    id: "integerAttr",
    is: function(s) { return false; },
    format: function(s, table, cell, cellIndex) { return $(cell).attr("data-val"); },
    type: "numeric"
});

$(function() 
{ 
    $("table.basic.list").tablesorter({
        headers: {' . $headersOption . '},
        sortList: ' . $sortListOption . ',
        widgets: ["zebra"]
    });
}
);');
}


# Affichage du pied de page
print $view->displayFooter();
