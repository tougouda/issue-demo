use utf8;
use open (':encoding(UTF-8)');
binmode STDOUT, ":raw";    # Pour envoyer le flux en l'état

use strict;
use locale;

use LOC::Date;
use LOC::Locale;
use LOC::Pdf;
use LOC::Browser;



our %tabViewData;
our $var;

# Locale
my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});

# Dimensions de la page
my $margin = 10/mm;
my $pageWidth = 210/mm;
my $textWidth = $pageWidth - 2 * $margin;

my $pageHeight = 297/mm;
my $textHeight = $pageHeight - 2 * $margin;

my $gap = 2/pt;

# Titre de la page
my @tabLocaltime = localtime();
my $title = $locale->t('Taux d\'utilisation');

# Création de l'objet PDF
my $pdf = LOC::Pdf->new($title, $tabViewData{'user.agency.id'}, \$locale);
my $pdfObj = $pdf->getPdfObject();

# Polices
my $regularFont    = $pdf->{'font'}->{'regular'};
my $italicFont     = $pdf->{'font'}->{'italic'};
my $boldFont       = $pdf->{'font'}->{'bold'};
my $boldItalicFont = $pdf->{'font'}->{'bolditalic'};
my $dingbatsFont   = $pdfObj->corefont('ZapfDingbats');

# Couleurs
my $titleColor = LOC::Pdf::COLOR_EMPHASIS;
my $zeroColor  = '#CCCCCC';
my $fgColor    = 'black';

# Définition du sujet et des mots-clés
$pdf->setSubject($locale->t('Taux d\'utilisation'));
$pdf->setKeywords($locale->t('Taux d\'utilisation'));



# Nouvelle page
my $page = $pdf->addPage(LOC::Pdf::DISPLAY_TITLE | LOC::Pdf::DISPLAY_FULLFOOTER);
my $text = $page->text();
my $gfx  = $page->gfx();


# Région
my $areaLabel = $locale->t('Région %s', $tabViewData{'area.label'});
$text->textlabel($margin + 95/mm, $pageHeight - 35/mm - 16/pt * 1.2, $boldFont, 10/pt, $areaLabel);

# Date et heure d'édition
my $startY = 297/mm - 35/mm;
my $x = 10/mm;
my $y = 0;

$text->font($regularFont, 8/pt);
$text->translate($x, $startY - $y);
my $editionTime = $locale->getDateFormat(undef, LOC::Locale::FORMAT_DATETIME_NUMERIC2);
$text->text($locale->t('Édité le %s', $editionTime));


my $x = $margin;
my $y = $pageHeight - 50/mm;


# Taille des colonnes des agences
my $agencyColWidth = 20/mm;

# Taille de la colonne des états
my $nbAgencies = keys(%{$tabViewData{'tabAgency'}});
# + $gap compense le $gap après la dernière colonne
my $firstColWidth = $textWidth - ($nbAgencies + 1 + ($tabViewData{'area.id'} != 0)) * $agencyColWidth + $gap;
$firstColWidth = ($firstColWidth < 0 ? 0/mm : $firstColWidth);


### Par état ###
my $stateLabel = $locale->t('Répartition par état');
$text->textlabel($x, $y, $regularFont, 9/pt, uc($stateLabel), -color => $titleColor);
$y -= 9/pt * 1.2;

# Ligne d'en-tête
&displayHeader($locale->t('État'));

# Données
$text->font($regularFont, 9/pt);

my $tabStateLabel = {'total'     => $locale->t('Total'),
                     'rented'    => $locale->t('Loué'),
                     'available' => $locale->t('Disponible'),
                     'recovery'  => $locale->t('Récupération'),
                     'review'    => $locale->t('Révision'),
                     'control'   => $locale->t('Contrôle'),
                     'broken'    => $locale->t('Panne'),
                     'check'     => $locale->t('Vérification'),
                    };
foreach my $state (@{$tabViewData{'tabState'}})
{
    my $stateNbCode = 'nb' . uc(substr($state, 0, 1)) . substr($state, 1);
    my $stateLabel  = $tabStateLabel->{$state};

    # Libellé
    $text->text($stateLabel, -maxWidth => $firstColWidth - $gap);
    $x += $firstColWidth;
    $text->translate($x, $y);

    # Agences
    foreach my $agencyCode (keys(%{$tabViewData{'tabAgency'}}))
    {
        my $pc = $locale->getNumberFormat($tabViewData{'tabData'}->{$stateNbCode}->{$agencyCode}->{'pc'}, 0) . ' %';
        my $nb = $locale->getNumberFormat($tabViewData{'tabData'}->{$stateNbCode}->{$agencyCode}->{'nb'}, 0);

        $text->fillcolor($nb ? $fgColor : $zeroColor);

        # Pourcentage
        $x += $agencyColWidth / 2;
        $text->translate($x, $y);
        $text->text_right($pc, -maxWidth => $agencyColWidth / 2 - $gap);

        # Nombre
        $x += $agencyColWidth / 2 - $gap;
        $text->translate($x, $y);
        $text->text_right('(' . $nb . ')', -maxWidth => $agencyColWidth / 2 - $gap);
        $x += $gap;
        $text->translate($x, $y);

        $text->fillcolor($fgColor);
    }

    # Régional
    if ($tabViewData{'area.id'})
    {
        my $pc = $locale->getNumberFormat($tabViewData{'tabData'}->{$stateNbCode}->{'area'}->{'pc'}, 0) . ' %';
        my $nb = $locale->getNumberFormat($tabViewData{'tabData'}->{$stateNbCode}->{'area'}->{'nb'}, 0);

        $text->fillcolor($nb ? $fgColor : $zeroColor);

        # Pourcentage
        $x += $agencyColWidth / 2;
        $text->translate($x, $y);
        $text->text_right($pc, -maxWidth => $agencyColWidth / 2 - $gap);

        # Nombre
        $x += $agencyColWidth / 2 - $gap;
        $text->translate($x, $y);
        $text->text_right('(' . $nb . ')', -maxWidth => $agencyColWidth / 2 - $gap);
        $x += $gap;
        $text->translate($x, $y);

        $text->fillcolor($fgColor);
    }

    # National
    my $pc = $locale->getNumberFormat($tabViewData{'tabData'}->{$stateNbCode}->{'total'}->{'pc'}, 0) . ' %';
    my $nb = $locale->getNumberFormat($tabViewData{'tabData'}->{$stateNbCode}->{'total'}->{'nb'}, 0);

    $text->fillcolor($nb ? $fgColor : $zeroColor);

    # Pourcentage
    $x += $agencyColWidth / 2;
    $text->translate($x, $y);
    $text->text_right($pc, -maxWidth => $agencyColWidth / 2 - $gap);

    # Nombre
    $x += $agencyColWidth / 2 - $gap;
    $text->translate($x, $y);
    $text->text_right('(' . $nb . ')', -maxWidth => $agencyColWidth / 2 - $gap);

    $text->fillcolor($fgColor);

    # Retour à la ligne
    $x = $margin;
    $y -= 7/pt * 1.2;
    $text->translate($x, $y);
}


$y -= 5/mm;

### Par famille tarifaire ###
my $familyLabel = $locale->t('Taux d\'utilisation par famille tarifaire');
$text->textlabel($x, $y, $regularFont, 9/pt, uc($familyLabel), -color => $titleColor);
$y -= 9/pt * 1.2;

# Ligne d'en-tête
&displayHeader($locale->t('Famille tarifaire'));

# Données
$text->font($regularFont, 9/pt);
$gfx->strokecolor($fgColor);
$gfx->linewidth(0.5/pt);

my $isFirstLine = 1;
my $previousOrder;
foreach my $tariffFamilyId (keys(%{$tabViewData{'tabTariffFamily'}}))
{
    my $tabTariffFamilyData = $tabViewData{'tabData'}->{$tariffFamilyId};

    # Ne pas afficher s'il n'y a aucune machine de cette famille tarifaire dans le parc national
    if ($tabTariffFamilyData->{'total'}->{'nbTotal'} == 0)
    {
        next;
    }

    my $tariffFamilyFulllabel = $tabViewData{'tabTariffFamily'}->{$tariffFamilyId}->{'fulllabel'};

    my $order = $tabViewData{'tabTariffFamily'}->{$tariffFamilyId}->{'order'};
    my $isSeparator = (!$isFirstLine && $previousOrder - $previousOrder % 100 != $order - $order % 100);

    # Changement de groupe de famille tarifaire
    # (exception pour la 1re ligne à afficher)
    if ($isSeparator)
    {
        $y -= 1/mm;
        $text->translate($x, $y);

        $gfx->move($x, $y + 7/pt * 1.2);
        $gfx->line($x + $firstColWidth - $gap, $y + 7/pt * 1.2);
    }

    $text->text($tariffFamilyFulllabel, -maxWidth => $firstColWidth - $gap);
    $x += $firstColWidth;

    # Agences
    foreach my $agencyCode (keys(%{$tabViewData{'tabAgency'}}))
    {
        # Changement de groupe de famille tarifaire
        if ($isSeparator)
        {
            $gfx->move($x, $y + 7/pt * 1.2);
            $gfx->line($x + $agencyColWidth - $gap, $y + 7/pt * 1.2);
        }

        my $pc = $locale->getNumberFormat($tabTariffFamilyData->{$agencyCode}->{'pc'}, 0) . ' %';
        my $nb = $locale->getNumberFormat($tabTariffFamilyData->{$agencyCode}->{'nbTotal'}, 0);

        $text->fillcolor($nb ? $fgColor : $zeroColor);

        # Pourcentage
        $x += $agencyColWidth / 2;
        $text->translate($x, $y);
        $text->text_right($pc, -maxWidth => $agencyColWidth / 2 - $gap);

        # Nombre
        $x += $agencyColWidth / 2 - $gap;
        $text->translate($x, $y);
        $text->text_right('(' . $nb . ')', -maxWidth => $agencyColWidth / 2 - $gap);
        $x += $gap;
        $text->translate($x, $y);

        $text->fillcolor($fgColor);
    }

    # Régional
    if ($tabViewData{'area.id'})
    {
        # Changement de groupe de famille tarifaire
        if ($isSeparator)
        {
            $gfx->move($x, $y + 7/pt * 1.2);
            $gfx->line($x + $agencyColWidth - $gap, $y + 7/pt * 1.2);
        }

        my $pc = $locale->getNumberFormat($tabTariffFamilyData->{'area'}->{'pc'}, 0) . ' %';
        my $nb = $locale->getNumberFormat($tabTariffFamilyData->{'area'}->{'nbTotal'}, 0);

        $text->fillcolor($nb ? $fgColor : $zeroColor);

        # Pourcentage
        $x += $agencyColWidth / 2;
        $text->translate($x, $y);
        $text->text_right($pc, -maxWidth => $agencyColWidth / 2 - $gap);

        # Nombre
        $x += $agencyColWidth / 2 - $gap;
        $text->translate($x, $y);
        $text->text_right('(' . $nb . ')', -maxWidth => $agencyColWidth / 2 - $gap);
        $x += $gap;
        $text->translate($x, $y);

        $text->fillcolor($fgColor);
    }

    # National
    # Changement de groupe de famille tarifaire
    if ($isSeparator)
    {
        $gfx->move($x, $y + 7/pt * 1.2);
        $gfx->line($x + $agencyColWidth - $gap, $y + 7/pt * 1.2);
    }

    my $pc = $locale->getNumberFormat($tabTariffFamilyData->{'total'}->{'pc'}, 0) . ' %';
    my $nb = $locale->getNumberFormat($tabTariffFamilyData->{'total'}->{'nbTotal'}, 0);

    $text->fillcolor($nb ? $fgColor : $zeroColor);

    # Pourcentage
    $x += $agencyColWidth / 2;
    $text->translate($x, $y);
    $text->text_right($pc, -maxWidth => $agencyColWidth / 2 - $gap);

    # Nombre
    $x += $agencyColWidth / 2 - $gap;
    $text->translate($x, $y);
    $text->text_right('(' . $nb . ')', -maxWidth => $agencyColWidth / 2 - $gap);

    $text->fillcolor($fgColor);

    if ($isSeparator)
    {
        $gfx->stroke();
    }

    # Retour à la ligne
    $x = $margin;
    $y -= 7/pt * 1.2;
    $text->translate($x, $y);

    $isFirstLine = 0;
    $previousOrder = $tabViewData{'tabTariffFamily'}->{$tariffFamilyId}->{'order'};
}

# Ligne de total
$text->font($boldFont, 8/pt);
$gfx->linewidth(1/pt);
$gfx->strokecolor($fgColor);

$y -= 1/mm;
$text->translate($x, $y);

$gfx->move($x, $y + 7/pt * 1.2);
$gfx->line($x + $firstColWidth - $gap, $y + 7/pt * 1.2);

$text->text(uc($locale->t('Total')), -maxWidth => $firstColWidth - $gap);
$x += $firstColWidth;

# Agences
foreach my $agencyCode (keys(%{$tabViewData{'tabAgency'}}))
{
    $gfx->move($x, $y + 7/pt * 1.2);
    $gfx->line($x + $agencyColWidth - $gap, $y + 7/pt * 1.2);

    my $pc = $locale->getNumberFormat($tabViewData{'tabData'}->{'total'}->{$agencyCode}->{'pc'}, 0) . ' %';
    my $nb = $locale->getNumberFormat($tabViewData{'tabData'}->{'total'}->{$agencyCode}->{'nbTotal'}, 0);

    $text->fillcolor($nb ? $fgColor : $zeroColor);

    # Pourcentage
    $x += $agencyColWidth / 2;
    $text->translate($x, $y);
    $text->text_right($pc, -maxWidth => $agencyColWidth / 2 - $gap);

    # Nombre
    $x += $agencyColWidth / 2 - $gap;
    $text->translate($x, $y);
    $text->text_right('(' . $nb . ')', -maxWidth => $agencyColWidth / 2 - $gap);
    $x += $gap;
    $text->translate($x, $y);

    $text->fillcolor($fgColor);
}

# Régional
if ($tabViewData{'area.id'})
{
    $gfx->move($x, $y + 7/pt * 1.2);
    $gfx->line($x + $agencyColWidth - $gap, $y + 7/pt * 1.2);

    my $pc = $locale->getNumberFormat($tabViewData{'tabData'}->{'total'}->{'area'}->{'pc'}, 0) . ' %';
    my $nb = $locale->getNumberFormat($tabViewData{'tabData'}->{'total'}->{'area'}->{'nbTotal'}, 0);

    $text->fillcolor($nb ? $fgColor : $zeroColor);

    # Pourcentage
    $x += $agencyColWidth / 2;
    $text->translate($x, $y);
    $text->text_right($pc, -maxWidth => $agencyColWidth / 2 - $gap);

    # Nombre
    $x += $agencyColWidth / 2 - $gap;
    $text->translate($x, $y);
    $text->text_right('(' . $nb . ')', -maxWidth => $agencyColWidth / 2 - $gap);
    $x += $gap;
    $text->translate($x, $y);

    $text->fillcolor($fgColor);
}

# National
$gfx->move($x, $y + 7/pt * 1.2);
$gfx->line($x + $agencyColWidth - $gap, $y + 7/pt * 1.2);

my $pc = $locale->getNumberFormat($tabViewData{'tabData'}->{'total'}->{'total'}->{'pc'}, 0) . ' %';
my $nb = $locale->getNumberFormat($tabViewData{'tabData'}->{'total'}->{'total'}->{'nbTotal'}, 0);

$text->fillcolor($nb ? $fgColor : $zeroColor);

# Pourcentage
$x += $agencyColWidth / 2;
$text->translate($x, $y);
$text->text_right($pc, -maxWidth => $agencyColWidth / 2 - $gap);

# Nombre
$x += $agencyColWidth / 2 - $gap;
$text->translate($x, $y);
$text->text_right('(' . $nb . ')', -maxWidth => $agencyColWidth / 2 - $gap);

$text->fillcolor($fgColor);

$gfx->stroke();


# Sortie
my $pdfString = $pdf->getString();
$pdf->end();

my $filename = sprintf('TauxUtilisation_%04d-%02d-%02d.pdf', $tabLocaltime[5] + 1900,
                                                             $tabLocaltime[4] + 1,
                                                             $tabLocaltime[3]);
&LOC::Browser::sendHeaders(
        "Content-type: application/force-download;\n"
        . "Content-Disposition: attachment; filename=\"" . $filename . "\";\n\n"
    );

print $pdfString;



sub displayHeader
{
    my ($firstColLabel) = @_;

    ## Ligne d'en-tête
    # Police et épaisseur du trait
    $text->font($boldFont, 8/pt);
    $gfx->linewidth(1/pt);
    $gfx->strokecolor($fgColor);

    $x = $margin;
    $text->translate($x, $y);

    # Sauvegarde de la position courante
    my ($x0, $y0) = $text->textpos();

    # État
    $text->text($firstColLabel, -maxWidth => $firstColWidth - $gap);
    $gfx->move($x, $y - 1/mm);
    $x += $firstColWidth;
    $gfx->line($x - $gap, $y - 1/mm);

    # Agences
    foreach my $agencyCode (keys(%{$tabViewData{'tabAgency'}}))
    {
        $text->translate($x, $y);
        $text->text($agencyCode, -maxWidth => $agencyColWidth - $gap);
        $gfx->move($x, $y - 1/mm);
        $x += $agencyColWidth;
        $gfx->line($x - $gap, $y - 1/mm);
    }

    # Régional
    if ($tabViewData{'area.id'})
    {
        $text->translate($x, $y);
        $text->text($locale->t('Régional'), -maxWidth => $agencyColWidth - $gap);
        $gfx->move($x, $y - 1/mm);
        $x += $agencyColWidth;
        $gfx->line($x - $gap, $y - 1/mm);
    }

    # National
    $text->translate($x, $y);
    $text->text($locale->t('National'), -maxWidth => $agencyColWidth - $gap);
    $gfx->move($x, $y - 1/mm);
    $x += $agencyColWidth;
    $gfx->line($x - $gap, $y - 1/mm);

    $gfx->stroke();

    # Retour à la ligne
    $x = $x0;
    $y = $y0 - 8/pt * 1.5;
    $text->translate($x, $y);
}