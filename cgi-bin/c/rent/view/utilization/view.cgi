use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;
use LOC::Locale;
use LOC::Json;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'rent/utilization/';


our %tabViewData;
our $inputsTabIndex = 1;


our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}

# URL courante
my $currentUrl = $view->createURL('rent:utilization:view');

# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'view.css');

# jQuery
$view->addJSSrc('jQuery/jquery.js');


#$view->addPackage('popups');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);


my $title = $locale->t('Taux d\'utilisation');
$view->setPageTitle($title);
$view->setTitle($title . ' <i>' . $tabViewData{'area.label'} . '</i>');


# Bouton "Actualiser"
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png',
                                          $currentUrl, $locale->t('Actualiser la page'));
$view->addControlsContent([$refreshBtn], 'right', 1);


# Bouton d'extraction excel
my $pdfUrl = $view->createURL('rent:utilization:view', {'displayType' => 'pdf'});
my $pdfBtn = $view->displayTextButton($locale->t('Extraction PDF'), 'standard/pdf.png',
                                        $pdfUrl,
                                        $locale->t('Extraction au format PDF'));

$view->addControlsContent($pdfBtn, 'right', 0);


# Affichage de l'entête
print $view->displayHeader();


my $tabStateLabel = {
    'total'     => $locale->t('Total'),
    'rented'    => $locale->t('Loué'),
    'available' => $locale->t('Disponible'),
    'recovery'  => $locale->t('Récupération'),
    'review'    => $locale->t('Révision'),
    'control'   => $locale->t('Contrôle'),
    'broken'    => $locale->t('Panne'),
    'check'     => $locale->t('Vérification'),
};

print '
<table class="basic list">';

# Affichage "par état"
&_displayHeader(htmllocale('Répartition par état'), htmllocale('État'));
    
print '
    <tbody>
';

foreach my $state (@{$tabViewData{'tabState'}})
{
    print '
        <tr>';
    my $stateNbCode = 'nb' . uc(substr($state, 0, 1)) . substr($state, 1);
    my $stateLabel  = $tabStateLabel->{$state};

    # Libellé
    print '
            <td>' . $stateLabel . '</td>';

    # Agences
    foreach my $agencyCode (keys(%{$tabViewData{'tabAgency'}}))
    {
        &_displayData($tabViewData{'tabData'}->{$stateNbCode}->{$agencyCode}, 'nb');
    }

    # Régional
    if ($tabViewData{'area.id'})
    {
        &_displayData($tabViewData{'tabData'}->{$stateNbCode}->{'area'}, 'nb');
    }

    # National
    &_displayData($tabViewData{'tabData'}->{$stateNbCode}->{'total'}, 'nb');

    print '
        </tr>';
}

print '
    </tbody>';

# Affichage "par famille tarifaire"
&_displayHeader(htmllocale('Taux d\'utilisation par famille tarifaire'), htmllocale('Famille tarifaire'));

print '
    <tbody>
';

# Total
print '
        <tr class="total">
            <td>' . htmllocale('TOTAL') . '</td>';


# Agences
foreach my $agencyCode (keys(%{$tabViewData{'tabAgency'}}))
{
    &_displayData($tabViewData{'tabData'}->{'total'}->{$agencyCode}, 'nbTotal');
}

# Régional
if ($tabViewData{'area.id'})
{
    &_displayData($tabViewData{'tabData'}->{'total'}->{'area'}, 'nbTotal');
}

# National
&_displayData($tabViewData{'tabData'}->{'total'}->{'total'}, 'nbTotal');

print '
        </tr>';

my $isFirstLine = 1;
my $previousOrder;
foreach my $tariffFamilyId (keys(%{$tabViewData{'tabTariffFamily'}}))
{
    my $tabTariffFamilyData = $tabViewData{'tabData'}->{$tariffFamilyId};

    # Ne pas afficher s'il n'y a aucune machine de cette famille tarifaire dans le parc national
    if ($tabTariffFamilyData->{'total'}->{'nbTotal'} == 0)
    {
        next;
    }

    my $tariffFamilyFulllabel = $tabViewData{'tabTariffFamily'}->{$tariffFamilyId}->{'fulllabel'};

    my $order = $tabViewData{'tabTariffFamily'}->{$tariffFamilyId}->{'order'};
    my $isSeparator = ($isFirstLine || $previousOrder - $previousOrder % 100 != $order - $order % 100);

    print '
        <tr class="' . ($isSeparator ? 'new' : '') . '">
            <td>' . $tariffFamilyFulllabel . '</td>';

    # Agences
    foreach my $agencyCode (keys(%{$tabViewData{'tabAgency'}}))
    {
        &_displayData($tabTariffFamilyData->{$agencyCode}, 'nbTotal');
    }

    # Régional
    if ($tabViewData{'area.id'})
    {
        &_displayData($tabTariffFamilyData->{'area'}, 'nbTotal');
    }

    # National
    &_displayData($tabTariffFamilyData->{'total'}, 'nbTotal');

    print '
        </tr>
    ';

    $isFirstLine = 0;
    $previousOrder = $tabViewData{'tabTariffFamily'}->{$tariffFamilyId}->{'order'};
}

print '
    </tbody>
</table>';



print $view->displayFooter();



# _displayHeader
# Affiche les groupes d'en-tête
#
# Parameters:
# string $headerTitle   Titre du groupe
# string $columnTitle   Titre de la colonne principale
#
sub _displayHeader
{
    my ($headerTitle, $columnTitle) = @_;
    my $nbAgencies = keys(%{$tabViewData{'tabAgency'}});
    my $titleColspan = 1 + $nbAgencies * 2 + ($tabViewData{'area.id'} ? 2 : 0) + 2;

    print '
        <thead>
            <tr>
                <th colspan="' . $titleColspan . '" class="divider">' . $headerTitle . '</th>
            </tr>
        </thead>
        <thead>
            <tr>
                <th>' . $columnTitle . '</th>';
        
    # Agences
    foreach my $agencyCode (keys(%{$tabViewData{'tabAgency'}}))
    {
        print '
                <th colspan="2" class="agency">' . $agencyCode . '</th>
        ';
    }
    
    # Régional
    if ($tabViewData{'area.id'})
    {
        print '
                <th colspan="2" class="agency">' . $locale->t('Régional') . '</th>
        ';
    }
    
    # National
    print '
                <th colspan="2" class="agency">' . $locale->t('National') . '</th>
    ';
    print '
            </tr>
        </thead>';
}

# _displayData
# Affiche les données pour chaque agence/région/pays
#
# Parameters
# hashref  $tabData     Tableau de données
# string   $totalLabel  Nom du champ contenant le nombre de machines concernées (nb ou nbTotal)
#
sub _displayData
{
    my ($tabData, $totalLabel) = @_;

    my $pc = $locale->getNumberFormat($tabData->{'pc'}, 0) . ' %';
    my $nb = $locale->getNumberFormat($tabData->{$totalLabel}, 0);

    my $class = ($nb ? '' : 'fade');

    # Pourcentage
    print '
        <td class="' . $class . '">' . $pc . '</td>';
    # Nombre
    print '
        <td class="' . $class . '">(' . $nb . ')</td>';
}