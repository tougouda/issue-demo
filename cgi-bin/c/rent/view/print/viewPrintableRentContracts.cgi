use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;
use LOC::Json;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'rent/print/';


our %tabViewData;


our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});



sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'viewPrintableRentContracts.css');

# Packages
$view->addPackage('popups');

# JS
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc('jQuery/plugins/tablesorter.js');
$view->addJSSrc($dir . 'viewPrintableRentContracts.js');


$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);


my $title = ($tabViewData{'type'} eq TYPE_FIRSTEDIT ? $locale->t('Première édition des contrats') : $locale->t('Édition des contrats'));
$view->setPageTitle($title);
$view->setTitle($title);


# Message d'erreur sur une ou plusieurs lignes
my $nbErrors = scalar @{$tabViewData{'errors'}};
if ($nbErrors > 0)
{
    my $errorBlock = '
<div id="errorBlock" class="messageBlock error">
    <div class="popup" style="display: block;">
        <div class="content">
            <table>
                <tbody id="errorsTable">
                    <tr>
                        <td style="font-weight : bold;">
                            ' . $locale->t('Les contrats marqués en rouge n\'ont pas pu être édités.') . '
                        </td>
                    </tr>
                    <tr class="buttons" id="buttonRow">
                        <td>' .
                        $view->displayTextButton($locale->t('Fermer'), '', '$ge("errorBlock").style.display = "none";') .
                        '</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>';

    $view->addBodyContent($errorBlock, 0, 1);
}


# Bouton "Actualiser"
my $currentUrl = $view->createURL('rent:print:viewPrintableRentContracts', {'type' => $tabViewData{'type'}});
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png',
                                          $currentUrl, $locale->t('Actualiser la page'), 'left', {'id' => 'reload'});
$view->addControlsContent([$refreshBtn], 'right', 1);

# Nombre de contrats
my $nbContracts = scalar @{$tabViewData{'tabContracts'}};
if ($nbContracts > 0)
{
    my $nbContractsLabel = '<div class="nbContracts">' . $locale->tn('%d contrat', '%d contrats', $nbContracts, $nbContracts) . '</div>';
    $view->addControlsContent([$nbContractsLabel], 'left', 1);
}



# Affichage de l'entête
print $view->displayHeader();


# On n'affiche pas le tableau si il est vide
if ($nbContracts == 0)
{
    # Pas de résultat
    print '<div class="noResult">' . $view->displayMessages('info', [$locale->t('Il n\'y a aucun contrat à éditer.')], 0) . '</div>';
}
else
{
    # Options pour tablesorter
    my $headersOption = '
0 : {sorter: "textAttr"},
1 : {sorter: "textAttr"},
2 : {sorter: "textAttr"},
5 : {sorter: "textAttr"},
6 : {sorter: false},
7 : {sorter: "textAttr"},
9 : {sorter: "integerAttr"},
10 : {sorter: false}'
;
    # Tri par défaut : état de contrat, puis code de contrat
    my $sortListOption = '[[1,0],[2,0],[0,0]]';

    print '
<table class="basic list">
    <thead>
        <tr>
            <th class="contract"><div>' . htmllocale('Contrat') . '</div></th>
            <th class="state"><div></div></th>
            <th class="customer"><div>' . htmllocale('Client') . '</div></th>
            <th class="site"><div>' . htmllocale('Libellé chantier') . '</div></th>
            <th class="locality"><div>' . htmllocale('Ville chantier') . '</div></th>
            <th class="begin"><div>' . htmllocale('Date de début') . '</div></th>
            <th class="arrow"><div></div></th>
            <th class="end"><div>' . htmllocale('Date de fin') . '</div></th>
            <th class="model"><div>' . htmllocale('Modèle de machines') . '</div></th>
            <th class="park"><div>' . htmllocale('N° de parc') . '</div></th>
            <th class="checkbox"><div></div></th>
        </tr>
    </thead>
    <tbody>';

    foreach my $tabContractInfos (@{$tabViewData{'tabContracts'}})
    {
        my $contractUrl = $view->createURL('rent:rentContract:view', {'contractId' => $tabContractInfos->{'id'}});
        my $customerUrl = &LOC::Globals::get('locationUrl') . '/cgi-bin/old/location/infoclient.cgi?value=' . $tabContractInfos->{'customer.id'};
        my $numericParkNumber = $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'};
        $numericParkNumber =~ s/[^\d]//;

        my $isError = &LOC::Util::in_array($tabContractInfos->{'id'}, $tabViewData{'errors'});

        print '
        <tr' . ($isError ? ' class="isError"' : '') . '>
            <td class="contract" data-val="' . $tabContractInfos->{'code'} . '"><a href="' . $contractUrl . '" target="_blank">' . $tabContractInfos->{'code'} . '</a></td>
            <td class="state" data-val="' . $tabContractInfos->{'state.id'} . '" title="' . $tabContractInfos->{'state.label'} . '">' . $view->displayState($tabContractInfos->{'state.id'}) . '</td>
            <td class="customer" data-val="' . $tabContractInfos->{'customer.name'} . '"><div><a href="' . $customerUrl . '" target="_blank">' . $tabContractInfos->{'customer.name'} . '</a></div></td>
            <td class="site"><div>' . $tabContractInfos->{'site.tabInfos'}->{'label'} . '</div></td>
            <td class="locality"><div>' . $tabContractInfos->{'site.tabInfos'}->{'locality.label'} . '</div></td>
            <td class="begin" data-val="' . $tabContractInfos->{'beginDate'} . '">' . $locale->getDateFormat($tabContractInfos->{'beginDate'}, LOC::Locale::FORMAT_DATE_NUMERIC) . '</td>
            <td class="arrow">→</td>
            <td class="end" data-val="' . $tabContractInfos->{'endDate'} . '">' . $locale->getDateFormat($tabContractInfos->{'endDate'}, LOC::Locale::FORMAT_DATE_NUMERIC) . '</td>
            <td class="model">' . $tabContractInfos->{'machine.tabInfos'}->{'model.label'} . '</td>
            <td class="park" data-val="' . $numericParkNumber . '">' . $view->displayParkNumber($tabContractInfos->{'machine.tabInfos'}->{'parkNumber'}) . '</td>
            <td class="checkbox">' . $view->displayFormInputCheckBox('contractIds[' . $tabContractInfos->{'id'} . ']', 0, undef, undef, $tabContractInfos->{'id'}) . '</td>
        </tr>';
    }

    print '
    </tbody>
</table>';


    # Tri du tableau
    print $view->displayJSBlock('
$.tablesorter.addParser({
    id: "textAttr",
    is: function(s) { return false; },
    format: function(s, table, cell, cellIndex) { return $(cell).attr("data-val"); },
    type: "text"
});
$.tablesorter.addParser({
    id: "integerAttr",
    is: function(s) { return false; },
    format: function(s, table, cell, cellIndex) { return $(cell).attr("data-val"); },
    type: "numeric"
});

$(function()
{
    $("table.basic.list").tablesorter({
        headers: {' . $headersOption . '},
        sortList: ' . $sortListOption . ',
        widgets: ["zebra"]
    });
}
);');


    # Boutons d'édition
    my $printBtns = '<label>' . $locale->t('Éditer') . '&nbsp;:</label>';
    $printBtns .= $view->displayPrintButtons('printBtns', '', '', '', '', '', '',
                                             {'buttons' => ['pdf', 'print']});
    print $view->displayControlPanel({'right' => [$printBtns], 'substyle' => 'middle'});


    # Variables JS et initialisation
    print $view->displayJSBlock('
var TYPE_FULL       = "' . TYPE_FULL . '";
var TYPE_FIRSTEDIT  = "' . TYPE_FIRSTEDIT . '";
var PRINTTYPE_PRINT = "' . LOC::Contract::Rent::PRINTTYPE_PRINT . '";
var PRINTTYPE_PDF   = "' . LOC::Contract::Rent::PRINTTYPE_PDF . '";
var PRINTTYPE_EMAIL = "' . LOC::Contract::Rent::PRINTTYPE_EMAIL . '";
var URL_PRINTURLS   = "' . $view->createURL('rent:print:jsonPrintUrls') . '";
contracts.init("' . $tabViewData{'type'} . '", ' . $tabViewData{'user.id'} . ');');
}


print $view->displayFooter();
