use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Json;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


our %tabViewData;


my $view = LOC::Json->new();
print $view->displayHeader();
print $view->encode($tabViewData{'tabUrls'});
