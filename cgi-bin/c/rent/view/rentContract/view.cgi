use utf8;
use open (':encoding(UTF-8)');

# Id des onglets
use constant {
    # Onglets principaux
    TABINDEX_EQUIPMENT => 0,
    TABINDEX_DURATION  => 1,
    TABINDEX_RENT      => 2,
    TABINDEX_TRANSPORT => 3,
    TABINDEX_REPAIR    => 4,
    TABINDEX_SERVICES  => 5,
    TABINDEX_PROFORMA  => 6,
    TABINDEX_DOCUMENTS => 7,
    TABINDEX_INVOICES  => 8,
    TABINDEX_HISTORY   => 9,

    # Onglets du chantier
    SITETABINDEX_GENERAL => 0,
    SITETABINDEX_INFOS   => 1,

    # Onglet Documents
    DOC_MAXDISPLAY              => 5,
    DOC_PREFIX_ATTACHMENT       => 'att',
    DOC_PREFIX_MACHINEINVENTORY => 'min',

    # Onglet Durée
    DAY_MAXDISPLAY   => 5
};


use strict;


use LOC::Html::Standard;
use LOC::Locale;
use LOC::Json;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'rent/rentContract/';


our %tabViewData;

our $inputsTabIndex = 1;
our $tabIndex;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


our $tabContractInfos = $tabViewData{'tabContractInfos'};
our $currencySymbol = $tabViewData{'currencySymbol'};

# Feuilles de style
$view->addCSSSrc('location.css');

$view->addPackage('popups')
     ->addPackage('tabBoxes')
     ->addPackage('tooltips')
     ->addPackage('searchboxes')
     ->addPackage('spincontrols')
     ->addPackage('modalwindow')
     ->addPackage('dropdownbuttons');


$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS|
                  LOC::Html::Standard::DISPFLG_NOTIFS);


my $title = $locale->t('Contrat de location');
if ($tabContractInfos->{'code'} ne '')
{
    $title = $tabContractInfos->{'code'};
}
$view->setPageTitle($title);
$view->setTitle($locale->t('Contrat de location'));

$view->addTranslations({
    'answerAreYouSureReset' => $locale->t('Êtes-vous sûr de vouloir annuler toutes les modifications sur ce contrat ?'),

    'answerAreYouSureStop' => $locale->t('Êtes-vous sûr de vouloir arrêter ce contrat ?'),
    'answerAreYouSureReactivate' => $locale->t('Êtes-vous sûr de vouloir remettre en cours ce contrat ?'),
    'answerAreYouSureInvoice' => $locale->t('Êtes-vous sûr de vouloir mettre en facturation ce contrat ?'),
    'answerAreYouSureRestop' => $locale->t('Êtes-vous sûr de vouloir remettre en arrêt ce contrat ?'),
    'answerAreYouSureCancel' => $locale->t('Êtes-vous sûr de vouloir annuler ce contrat ?'),

    'pendingRepairSheets.single'     => $locale->t('%s fiche de remise en état', '<%nbPendingRepairSheets>'),
    'pendingRepairSheets.plural'     => $locale->t('%s fiches de remise en état', '<%nbPendingRepairSheets>'),
    'pendingRepairEstimates'         => $locale->t('%s devis de remise en état', '<%nbPendingRepairEstimates>'),

    'ignorePendingDocsAndValid'      => $locale->t('Valider le contrat en ignorant ces documents'),
    'waitPendingDocsAndValid'        => $locale->t('Attendre et valider le contrat'),
    'ignorePendingDocsAndStop'       => $locale->t('Arrêter le contrat en ignorant ces documents'),
    'waitPendingDocsAndStop'         => $locale->t('Attendre et arrêter le contrat'),
    'ignorePendingDocsAndReactivate' => $locale->t('Remettre en cours le contrat en ignorant ces documents'),
    'waitPendingDocsAndReactivate'   => $locale->t('Attendre et remettre en cours le contrat'),
    'ignorePendingDocsAndInvoice'    => $locale->t('Passer en facturation le contrat en ignorant ces documents'),
    'waitPendingDocsAndInvoice'      => $locale->t('Attendre et passer en facturation le contrat'),
    'ignorePendingDocsAndRestop'     => $locale->t('Remettre en arrêt le contrat en ignorant ces documents'),
    'waitPendingDocsAndRestop'       => $locale->t('Attendre et remettre en arrêt le contrat'),
    'ignorePendingDocsAndCancel'     => $locale->t('Annuler le contrat en ignorant ces documents'),
    'waitPendingDocsAndCancel'       => $locale->t('Attendre et annuler le contrat'),

    'goBackToContract'          => $locale->t('Revenir sur le contrat'),

    'days.details'           => $locale->t('Jour %s : %s %s', '<%type>', '<%date>', '<%appliedToContract>'),
    'days.noDate'            => $locale->t('date non saisie'),
    'days.appliedToContract' => $locale->t('(contrat %s)', '<%appliedToContractId>'),
    'days.type_A'            => $locale->t('supplémentaire'),
    'days.type_D'            => $locale->t('déduit'),

    'transportTariff.personal' => $locale->t('Personnalisée')
});


# Notifications possibles
$view->addNotifications([
    # Modifications en attente
    {
        'id'      => 'edit',
        'type'    => 'warning',
        'message' => $locale->t('Il y a des modifications en attente. Elles seront enregistrées lorsque vous validerez le contrat.'),
        'buttons' => [{
                       'id'      => 'validate',
                       'title'   => $locale->t('Valider le contrat'),
                       'onclick' => 'Location.rentContractObj.validate();'
                      }]
    },

    # Demande de déblocage client envoyée
    {
        'id'      => 'unlockCustomerRequest-send',
        'type'    => 'info',
        'message' => $locale->t('La demande de déblocage pour le client a été envoyée.')
    },

    # Erreur demande de déblocage client
    {
        'id'      => 'unlockCustomerRequest-error',
        'type'    => 'error',
        'message' => $locale->t('Une erreur s\'est produite lors de l\'envoi de la demande de déblocage pour le client.')
    },

    # Champs bloqués sur le contrat
    {
        'id'      => 'uneditableFields-unfinalizedProforma',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas modifier le modèle de machines, la durée, le prix de location, le prix du transport et les autres services car il y a une pro forma active.')
    },

    # Pro forma de prolongation et transfert inter-chantiers/reprise sur chantier
    {
        'id'      => 'recoveryTransfer-noExtensionProforma',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas générer de facture pro forma de prolongation car la récupération est de type "Reprise sur chantier" ou "Transfert inter-chantiers".')
    },
    {
        'id'      => 'extensionProforma-noRecoveryTransfer',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas choisir les types de récupération "Reprise sur chantier" et "Transfert inter-chantiers" car il y a une facture pro forma de prolongation non finalisée.')
    },
    {
        'id'      => 'amountImpactOnDeliveryLinkedContract',
        'type'    => 'warning',
        'message' => $locale->t('Le montant du contrat <%contractCode> peut être impacté par la modification des informations de livraison.')
    },
    {
        'id'      => 'amountImpactOnRecoveryLinkedContract',
        'type'    => 'warning',
        'message' => $locale->t('Le montant du contrat <%contractCode> peut être impacté par la modification des informations de récupération.')
    },

    # Réalisation et modification du transport
    {
        'id'      => 'deliveryNotDoable',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas réaliser la livraison pour les raisons suivantes :'),
        'subMessages' => [
                          {
                           'id' => 'editionPending',
                           'single' => $locale->t('Vous ne pouvez pas réaliser la livraison car une modification l\'impactant est en attente de validation.'),
                           'multiple' => $locale->t('une modification impactant la livraison est en attente de validation')
                          },
                          {
                           'id' => 'rentExcepReduction',
                           'single' => $locale->t('Vous ne pouvez pas réaliser la livraison car une remise exceptionnelle de location est en attente de validation.'),
                           'multiple' => $locale->t('une remise exceptionnelle de location est en attente de validation')
                          },
                          {
                           'id' => 'transportExcepReduction',
                           'single' => $locale->t('Vous ne pouvez pas réaliser la livraison car une remise exceptionnelle de transport est en attente de validation.'),
                           'multiple' => $locale->t('une remise exceptionnelle de transport est en attente de validation')
                          },
                          {
                           'id' => 'proformaEdition',
                           'single' => $locale->t('Vous ne pouvez pas réaliser la livraison car une modification sur les factures pro forma est en attente de validation.'),
                           'multiple' => $locale->t('une modification sur les factures pro forma est en attente de validation')
                          },
                          {
                           'id' => 'modelEdition',
                           'single' => $locale->t('Vous ne pouvez pas réaliser la livraison car le modèle de machines a été modifié.'),
                           'multiple' => $locale->t('le modèle de machines a été modifié')
                          },
                          {
                           'id' => 'expiredProforma',
                           'single' => $locale->t('Vous ne pouvez pas réaliser la livraison car il y a une facture pro forma active et non à jour.'),
                           'multiple' => $locale->t('il y a une facture pro forma active et non à jour')
                          }
                         ]
    },
    {
        'id'      => 'deliveryNotUndoable-invoicedDelivery',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas annuler la réalisation de la livraison car le transport aller a été facturé.'),
    },
    {
        'id'      => 'deliveryNotUndoable-recoveryTransfer',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas annuler la réalisation de la livraison car la machine <%parkNumber> doit être transférée sur le contrat <%contractCode>.'),
    },
    {
        'id'      => 'recoveryNotDoable',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas réaliser la récupération car une modification l\'impactant est en attente de validation.'),
        'subMessages' => [
                          {
                           'id' => 'editionPending',
                           'single' => $locale->t('Vous ne pouvez pas réaliser la récupération car une modification l\'impactant est en attente de validation.'),
                           'multiple' => $locale->t('une modification impactant la récupération est en attente de validation')
                          },
                          {
                           'id' => 'rentExcepReduction',
                           'single' => $locale->t('Vous ne pouvez pas réaliser la récupération car une remise exceptionnelle de location est en attente de validation.'),
                           'multiple' => $locale->t('une remise exceptionnelle de location est en attente de validation')
                          },
                          {
                           'id' => 'transportExcepReduction',
                           'single' => $locale->t('Vous ne pouvez pas réaliser la récupération car une remise exceptionnelle de transport est en attente de validation.'),
                           'multiple' => $locale->t('une remise exceptionnelle de transport est en attente de validation')
                          },
                          {
                           'id' => 'proformaEdition',
                           'single' => $locale->t('Vous ne pouvez pas réaliser la récupération car une modification sur les factures pro forma est en attente de validation.'),
                           'multiple' => $locale->t('une modification sur les factures pro forma est en attente de validation')
                          },
                          {
                           'id' => 'expiredProforma',
                           'single' => $locale->t('Vous ne pouvez pas réaliser la récupération car il y a une facture pro forma active et non à jour.'),
                           'multiple' => $locale->t('il y a une facture pro forma active et non à jour')
                          }
                         ]
    },
    {
        'id'      => 'recoveryNotUndoable-transferredMachine',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas annuler la réalisation de la récupération car la machine a été transférée dans une autre agence.'),
    },
    {
        'id'      => 'recoveryNotUndoable-visibleMachine',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas annuler la réalisation de la récupération car la machine <%parkNumber> est mise en visibilité sur <%agencyId>.'),
    },
    {
        'id'      => 'deliveryNotEditable-doPending',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas modifier les informations de la livraison car la réalisation est en attente de validation.')
    },
    {
        'id'      => 'recoveryNotEditable-doPending',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas modifier les informations de la récupération car la réalisation est en attente de validation.')
    },
    {
        'id'      => 'deliveryNotEditable-undoPending',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas modifier les informations de la livraison car l\'annulation de la réalisation est en attente de validation.')
    },
    {
        'id'      => 'recoveryNotEditable-undoPending',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas modifier les informations de la récupération car l\'annulation de la réalisation est en attente de validation.')
    },
    {
        'id'      => 'transportNotEditable-doDeliveryPending',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas modifier les informations du transport car la réalisation de la livraison est en attente de validation.'),
    },
    {
        'id'      => 'transportNotEditable-doRecoveryPending',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas modifier les informations du transport car la réalisation de la récupération est en attente de validation.')
    },
    {
        'id'      => 'deliveryTransferUnavailable',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas choisir les types de livraison "Transfert inter-chantiers" et "Reprise sur chantier" pour les raisons suivantes :'),
        'subMessages' => [
                          {
                           'id' => 'assignedMachine',
                           'single' => $locale->t('Vous ne pouvez pas choisir les types de livraison "Transfert inter-chantiers" et "Reprise sur chantier" car une machine est déjà attribuée.'),
                           'multiple' => $locale->t('une machine est déjà attribuée')
                          },
                          {
                           'id' => 'customerAccountPending',
                           'single' => $locale->t('Vous ne pouvez pas choisir les types de livraison "Transfert inter-chantiers" et "Reprise sur chantier" car l\'ouverture de compte du client est en attente.'),
                           'multiple' => $locale->t('l\'ouverture de compte du client est en attente')
                          }
                         ]
    },
    {
        'id'      => 'recoveryTransferUnavailable',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas choisir les types de récupération "Transfert inter-chantiers" et "Reprise sur chantier" pour les raisons suivantes :'),
        'subMessages' => [
                          {
                           'id' => 'notFinalAllocation',
                           'single' => $locale->t('Vous ne pouvez pas choisir les types de récupération "Transfert inter-chantiers" et "Reprise sur chantier" car aucune machine n\'est attribuée.'),
                           'multiple' => $locale->t('aucune machine n\'est attribuée')
                          }
                         ]
    },
    {
        'id'      => 'transferRecoveryYetRealized',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas modifier le transfert inter-chantiers paramétré sur la livraison car la récupération est déjà réalisée.'),
    },
    {
        'id'      => 'deliverySelfSiteTransferNotEditable',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas enlever le type de livraison "Reprise sur chantier" car le montant de la récupération du contrat <%contractCode> n\'est pas à 0.'),
    },
    {
        'id'      => 'recoverySelfSiteTransferNotEditable',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas enlever le type de récupération "Reprise sur chantier" car le montant de la livraison du contrat <%contractCode> n\'est pas à 0.'),
    },
    {
        'id'      => 'synchroDisabled',
        'type'    => 'info',
        'message' => $locale->t('Les modifications effectuées ne seront pas envoyées à OTD.'),
    },

    # "Transférée à" désactivée
    {
        'id'      => 'transferToAgency-yetVisible',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas choisir l\'option "La machine est transférée à" car la machine est déjà visible sur l\'agence de <%visibleOnAgencyLabel>.'),
    },
    {
        'id'      => 'transferToAgency-unrentable',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas choisir l\'option "La machine est transférée à" car toutes les <%modelLabel> de l\'agence sont réservées.'),
    },
    {
        'id'      => 'transferToAgency-unrentableInVisibilityAgency',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas supprimer l\'option "La machine est transférée à" car toutes les <%modelLabel> de l\'agence de <%visibleOnAgencyLabel> sont réservées.'),
    },

    # Changements d'état du contrat
    # Arrêt
    {
        'id'      => 'cantStop',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas arrêter le contrat pour les raisons suivantes :'),
        'subMessages' => [
                          {
                           'id' => 'endDate',
                           'single' => $locale->tn('Vous ne pouvez pas arrêter le contrat car la date de fin est dans plus de %d jour.',
                                                   'Vous ne pouvez pas arrêter le contrat car la date de fin est dans plus de %d jours.',
                                                   $tabViewData{'daysToStop'},
                                                   $tabViewData{'daysToStop'}),
                           'multiple' => $locale->tn('la date de fin est dans plus de %d jour',
                                                     'la date de fin est dans plus de %d jours',
                                                     $tabViewData{'daysToStop'},
                                                     $tabViewData{'daysToStop'})
                          },
                          {
                           'id' => 'rentExcepReduction',
                           'single' => $locale->t('Vous ne pouvez pas arrêter le contrat car il y a une remise exceptionnelle de location en attente.'),
                           'multiple' => $locale->t('il y a une remise exceptionnelle de location en attente')
                          },
                          {
                           'id' => 'transportExcepReduction',
                           'single' => $locale->t('Vous ne pouvez pas arrêter le contrat car il y a une remise exceptionnelle de transport en attente.'),
                           'multiple' => $locale->t('il y a une remise exceptionnelle de transport en attente')
                          },
                          {
                           'id' => 'unfinalizedProforma',
                           'single' => $locale->t('Vous ne pouvez pas arrêter le contrat car il y a une facture pro forma non finalisée.'),
                           'multiple' => $locale->t('il y a une facture pro forma non finalisée')
                          },
                          {
                           'id' => 'expiredProforma',
                           'single' => $locale->t('Vous ne pouvez pas arrêter le contrat car il y a une facture pro forma active et non à jour.'),
                           'multiple' => $locale->t('il y a une facture pro forma active et non à jour')
                          }
                         ]
    },
    # Remise en cours
    {
        'id'      => 'cantReactivate',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas remettre en cours le contrat pour les raisons suivantes :'),
        'subMessages' => [
                          {
                           'id' => 'doneRecovery',
                           'single' => $locale->t('Vous ne pouvez pas remettre en cours le contrat car la récupération est réalisée.'),
                           'multiple' => $locale->t('la récupération est réalisée')
                          },
                          {
                           'id' => 'allRentInvoiced',
                           'single' => $locale->t('Vous ne pouvez pas remettre en cours le contrat car toute la location a été facturée.'),
                           'multiple' => $locale->t('toute la location a été facturée')
                          },
                          {
                           'id' => 'visibleMachine',
                           'single' => $locale->t('Vous ne pouvez pas remettre en cours le contrat car la machine est visible sur une autre agence.'),
                           'multiple' => $locale->t('la machine est visible sur une autre agence')
                          },
                          {
                           'id' => 'machineToTransfer',
                           'single' => $locale->t('Vous ne pouvez pas remettre en cours le contrat car un transfert de la machine sur une autre agence
                                                   est prévu sur le transport de récupération'),
                           'multiple' => $locale->t('un transfert de la machine sur une autre agence est prévu sur le transport de récupération')
                          },
                          {
                           'id' => 'modelUnrentable',
                           'single' => $locale->t('Vous ne pouvez pas remettre en cours le contrat car toutes les <%modelLabel> de l\'agence sont réservées.'),
                           'multiple' => $locale->t('toutes les <%modelLabel> de l\'agence sont réservées')
                          },
                          {
                           'id' => 'incoherentModels',
                           'single' => $locale->t('Vous ne pouvez pas remettre en cours le contrat car le modèle de machines demandé ne correspond pas au modèle de la machine attribuée.'),
                           'multiple' => $locale->t('le modèle de machines demandé ne correspond pas au modèle de la machine attribuée')
                          }
                         ],
        'buttons' => [{
                       'id'      => 'undoRecovery',
                       'title'   => $locale->t('Annuler la réalisation de la récupération'),
                       'onclick' => 'setDocumentActiveTabIndex(TABINDEX_TRANSPORT); LOC_Common.fireEvent($ge("transportHlp_recoveryUndoBtn"), "click");'
                      }]
    },
    # Mise en facturation
    {
        'id'      => 'cantInvoice',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas mettre en facturation le contrat pour les raisons suivantes :'),
        'subMessages' => [
                          {
                           'id' => 'undoneRecovery',
                           'single' => $locale->t('Vous ne pouvez pas mettre en facturation le contrat car la récupération n\'est pas réalisée.'),
                           'multiple' => $locale->t('la récupération n\'est pas réalisée')
                          },
                          {
                           'id' => 'rentExcepReduction',
                           'single' => $locale->t('Vous ne pouvez pas mettre en facturation le contrat car il y a une remise exceptionnelle de location en attente.'),
                           'multiple' => $locale->t('il y a une remise exceptionnelle de location en attente')
                          },
                          {
                           'id' => 'transportExcepReduction',
                           'single' => $locale->t('Vous ne pouvez pas mettre en facturation le contrat car il y a une remise exceptionnelle de transport en attente.'),
                           'multiple' => $locale->t('il y a une remise exceptionnelle de transport en attente')
                          },
                          {
                           'id' => 'unfinalizedProforma',
                           'single' => $locale->t('Vous ne pouvez pas mettre en facturation le contrat car il y a une facture pro forma non finalisée.'),
                           'multiple' => $locale->t('il y a une facture pro forma non finalisée')
                          },
                          {
                           'id' => 'expiredProforma',
                           'single' => $locale->t('Vous ne pouvez pas mettre en facturation le contrat car il y a une facture pro forma active et non à jour.'),
                           'multiple' => $locale->t('il y a une facture pro forma active et non à jour')
                          },
                          {
                           'id' => 'fuel',
                           'single' => $locale->t('Vous ne pouvez pas mettre en facturation le contrat car le carburant n\'est pas saisi.'),
                           'multiple' => $locale->t('le carburant n\'est pas saisi')
                          }
                         ],
        'buttons' => [{
                       'id'      => 'enterFuel',
                       'title'   => $locale->t('Saisir le carburant'),
                       'onclick' => 'setDocumentActiveTabIndex(TABINDEX_SERVICES); $ge("servicesHlp_fuelQty").focus();'
                      },
                      {
                       'id'      => 'doRecovery',
                       'title'   => $locale->t('Réaliser la récupération'),
                       'onclick' => 'setDocumentActiveTabIndex(TABINDEX_TRANSPORT); LOC_Common.fireEvent($ge("transportHlp_recoveryDoBtn"), "click");'
                      }]
    },
    # Remise en arrêt
    {
        'id'      => 'cantRestop',
        'type'    => 'info',
        'message' => $locale->t('Vous ne pouvez pas remettre en arrêt le contrat pour les raisons suivantes :'),
        'subMessages' => [
                          {
                           'id' => 'invoiced',
                           'single' => $locale->t('Vous ne pouvez pas remettre en arrêt le contrat car la dernière facture de location a été générée.'),
                           'multiple' => $locale->t('la dernière facture de location a été générée')
                          }
                         ]
    },
    {
        'id'      => 'restop-selfRecoveryWithAmount',
        'type'    => 'warning',
        'message' => $locale->t('Le montant de la récupération va être mis à 0 si le contrat est remis en arrêt car il s\'agit d\'un enlèvement agence.')
    },

    # Amiante
    {
        'id'      => 'isAsbestos-sendContract',
        'type'    => 'warning',
        'message' => $locale->t('Le chantier a été marqué comme contenant de l\'amiante. N\'oubliez pas d\'ajouter le forfait amiante dans les services complémentaires et de renvoyer le contrat au client.')
    },
    # Remise en état en attente
    {
        'id'      => 'pendingRepair',
        'type'    => 'warning',
        'message' => $locale->t('Vous avez de la remise en état en attente :'),
        'subMessages' => [
            {
                'id'       => 'sheet',
                'single'   => $locale->t('Vous avez %s en attente', '<%pendingRepairSheets>'),
                'multiple' => $locale->t('<%pendingRepairSheets>')
            },
            {
                'id'       => 'estimate',
                'single'   => $locale->t('Vous avez %s en attente', '<%pendingRepairEstimates>'),
                'multiple' => $locale->t('<%pendingRepairEstimates>')
            }
        ]
    },
]);

$view->addJSSrc('
/* Etats du contrat */
var RENTCONTRACT_STATE_PRECONTRACT = "' . LOC::Contract::Rent::STATE_PRECONTRACT . '";
var RENTCONTRACT_STATE_CONTRACT = "' . LOC::Contract::Rent::STATE_CONTRACT . '";
var RENTCONTRACT_STATE_STOPPED = "' . LOC::Contract::Rent::STATE_STOPPED . '";
var RENTCONTRACT_STATE_ARCHIVE = "' . LOC::Contract::Rent::STATE_ARCHIVE . '";
var RENTCONTRACT_STATE_CANCELED = "' . LOC::Contract::Rent::STATE_CANCELED . '";
var RENTCONTRACT_STATE_BILLED = "' . LOC::Contract::Rent::STATE_BILLED . '";

/* Etats de facturation du contrat */
var RENTCONTRACT_INVOICESTATE_NONE = "' . LOC::Contract::Rent::INVOICESTATE_NONE . '";
var RENTCONTRACT_INVOICESTATE_PARTIAL = "' . LOC::Contract::Rent::INVOICESTATE_PARTIAL . '";
var RENTCONTRACT_INVOICESTATE_FINALPARTIAL = "' . LOC::Contract::Rent::INVOICESTATE_FINALPARTIAL . '";
var RENTCONTRACT_INVOICESTATE_FINAL = "' . LOC::Contract::Rent::INVOICESTATE_FINAL . '";

/* Etats des jours +/- */
var DAY_MAXDISPLAY                         = ' . DAY_MAXDISPLAY . ';
var DAY_TYPE_ADDITIONAL                    = "' . LOC::Day::TYPE_ADDITIONAL . '";
var DAY_TYPE_DEDUCTED                      = "' . LOC::Day::TYPE_DEDUCTED . '";
var DAY_FLAG_ADDITIONALDAY                 = "' . LOC::Day::FLAG_ADDITIONALDAY . '";
var DAY_FLAG_DEDUCTEDDAY                   = "' . LOC::Day::FLAG_DEDUCTEDDAY . '";
var DAY_INVOICEDURATION_FLAG_DEDUCTEDDAY   = "' . LOC::Day::InvoiceDuration::FLAG_DEDUCTEDDAY . '";
var DAY_INVOICEDURATION_FLAG_ADDITIONALDAY = "' . LOC::Day::InvoiceDuration::FLAG_ADDITIONALDAY . '";
var DAY_REASON_USEFLAG_DEDUCTEDDAY         = "' . LOC::Day::Reason::USEFLAG_DEDUCTEDDAY . '";
var DAY_REASON_USEFLAG_ADDITIONALDAY       = "' . LOC::Day::Reason::USEFLAG_ADDITIONALDAY . '";
var DAY_REASON_USEFLAG_CONTRACT            = "' . LOC::Day::Reason::USEFLAG_CONTRACT . '";
var DAY_REASON_USEFLAG_ESTIMATE            = "' . LOC::Day::Reason::USEFLAG_ESTIMATE . '";

/* Etats des fiches de remise en état */
var REPAIRSHEET_STATE_WAITING  = "' . LOC::Repair::Sheet::STATE_WAITING . '";

/* Etats des devis de remise en état */
var REPAIRESTIMATE_STATE_WAITING = "' . LOC::Repair::Estimate::STATE_WAITING . '";

/* Etats des jours et des services de remise en état */
var REPAIR_STATE_APPROVED = "' . LOC::Contract::RepairService::STATE_APPROVED . '";
var REPAIR_STATE_CANCELED = "' . LOC::Contract::RepairService::STATE_CANCELED . '";
var REPAIR_STATE_INVOICED = "' . LOC::Contract::RepairService::STATE_INVOICED . '";

/* Etats des services de location et transport */
var RENTSRV_STATE_UNBILLED = "' . LOC::Contract::Rent::SERVICESTATE_UNBILLED . '";
var RENTSRV_STATE_BILLED   = "' . LOC::Contract::Rent::SERVICESTATE_BILLED . '";
var RENTSRV_STATE_DELETED  = "' . LOC::Contract::Rent::SERVICESTATE_DELETED . '";

/* Etats des remises exceptionnelles de location */
var RENTTARIFF_REDUCTIONSTATE_VALIDATED = "' . LOC::Tariff::Rent::REDUCTIONSTATE_VALIDATED . '";
var RENTTARIFF_REDUCTIONSTATE_PENDING   = "' . LOC::Tariff::Rent::REDUCTIONSTATE_PENDING . '";
var RENTTARIFF_REDUCTIONSTATE_REFUSED   = "' . LOC::Tariff::Rent::REDUCTIONSTATE_REFUSED . '";

/* Etats des remises exceptionnelles de transport */
var TRSPTARIFF_REDUCTIONSTATE_VALIDATED = "' . LOC::Tariff::Transport::REDUCTIONSTATE_VALIDATED . '";
var TRSPTARIFF_REDUCTIONSTATE_PENDING   = "' . LOC::Tariff::Transport::REDUCTIONSTATE_PENDING . '";
var TRSPTARIFF_REDUCTIONSTATE_REFUSED   = "' . LOC::Tariff::Transport::REDUCTIONSTATE_REFUSED . '";

/* États des machines */
var MACHINE_STATE_RENTED    = "' . LOC::Machine::STATE_RENTED . '";
var MACHINE_STATE_AVAILABLE = "' . LOC::Machine::STATE_AVAILABLE . '";
var MACHINE_STATE_RECOVERY  = "' . LOC::Machine::STATE_RECOVERY . '";
var MACHINE_STATE_REVIEW    = "' . LOC::Machine::STATE_REVIEW . '";
var MACHINE_STATE_CONTROL   = "' . LOC::Machine::STATE_CONTROL . '";
var MACHINE_STATE_BROKEN    = "' . LOC::Machine::STATE_BROKEN . '";
var MACHINE_STATE_CHECK     = "' . LOC::Machine::STATE_CHECK . '";
var MACHINE_STATE_TRANSFER  = "' . LOC::Machine::STATE_TRANSFER . '";
var MACHINE_STATE_DELIVERY  = "' . LOC::Machine::STATE_DELIVERY . '";
var MACHINE_STATE_LOST      = "' . LOC::Machine::STATE_LOST . '";
var MACHINE_STATE_DISABLED  = "' . LOC::Machine::STATE_DISABLED . '";

/* Visibilité machine */
var MACHINE_VISIBILITYMODE_NONE      = "' . LOC::Machine::VISIBILITYMODE_NONE . '";
var MACHINE_VISIBILITYMODE_MACHINE   = "' . LOC::Machine::VISIBILITYMODE_MACHINE . '";
var MACHINE_VISIBILITYMODE_TRANSPORT = "' . LOC::Machine::VISIBILITYMODE_TRANSPORT . '";

var INSURANCE_STATE_ACTIVE  = "' . LOC::Insurance::Type::STATE_ACTIVE . '";
var INSURANCE_STATE_DELETED = "' . LOC::Insurance::Type::STATE_DELETED . '";

var INSURANCEFLAG_ISINVOICED    = "' . LOC::Insurance::Type::FLAG_ISINVOICED . '";
var INSURANCEFLAG_ISINCLUDED    = "' . LOC::Insurance::Type::FLAG_ISINCLUDED . '";
var INSURANCEFLAG_ISCUSTOMER    = "' . LOC::Insurance::Type::FLAG_ISCUSTOMER . '";

var INSURANCEMODE_NOTINVOICED    = "' . LOC::Insurance::Type::MODE_NOTINVOICED . '";
var INSURANCEMODE_CALENDARADDAYS = "' . LOC::Insurance::Type::MODE_CALENDARADDAYS . '";
var INSURANCEMODE_WORKING        = "' . LOC::Insurance::Type::MODE_WORKING . '";
var INSURANCEMODE_CALENDAR       = "' . LOC::Insurance::Type::MODE_CALENDAR . '";

/* Mode de calcul de la gestion de recours */
var APPEALMODE_NOTINVOICED    = "' . LOC::Appeal::Type::MODE_NOTINVOICED . '";
var APPEALMODE_CALENDAR       = "' . LOC::Appeal::Type::MODE_CALENDAR . '";

var APPEAL_MODE_NONE    = ' . LOC::Common::APPEAL_MODE_NONE . ';
var APPEAL_MODE_PERCENT = ' . LOC::Common::APPEAL_MODE_PERCENT . ';

/* Mode de calcul de la participation au recyclage */
var RESIDUES_MODE_NONE    = ' . LOC::Common::RESIDUES_MODE_NONE . ';
var RESIDUES_MODE_PRICE   = ' . LOC::Common::RESIDUES_MODE_PRICE . ';
var RESIDUES_MODE_PERCENT = ' . LOC::Common::RESIDUES_MODE_PERCENT . ';

/* Types de pro forma */
var PFMTYPE_ID_ADVANCE    = ' . LOC::Proforma::Type::ID_ADVANCE . ';
var PFMTYPE_ID_PROFORMA   = ' . LOC::Proforma::Type::ID_PROFORMA . ';
var PFMTYPE_ID_EXTENSION  = ' . LOC::Proforma::Type::ID_EXTENSION . ';

/* Flags pro forma */
var PFMDOCFLAG_EXISTACTIVED          = ' . LOC::Proforma::DOCFLAG_EXISTACTIVED . ';
var PFMDOCFLAG_EXISTACTIVEDFINALIZED = ' . LOC::Proforma::DOCFLAG_EXISTACTIVEDFINALIZED . ';
var PFMDOCFLAG_EXISTACTIVEDEXPIRED   = ' . LOC::Proforma::DOCFLAG_EXISTACTIVEDEXPIRED . ';
var PFMDOCFLAG_CT_ISCUSTOMERPFREQONCREATION  = ' . LOC::Proforma::DOCFLAG_CT_ISCUSTOMERPFREQONCREATION . ';

/* Actions transports */
var TSPACTION_NOTDONE  = ' . LOC::Transport::ACTION_NOTDONE . ';
var TSPACTION_USERDONE = ' . LOC::Transport::ACTION_USERDONE . ';
var TSPACTION_DONE     = ' . LOC::Transport::ACTION_DONE . ';

/* Documents */
var DOC_MAXDISPLAY             = ' . DOC_MAXDISPLAY . ';
var DOC_PREFIX_ATTACHMENT        = "' . DOC_PREFIX_ATTACHMENT . '";
var DOC_PREFIX_MACHINEINVENTORY  = "' . DOC_PREFIX_MACHINEINVENTORY . '";

/* Index des onglets */
var TABINDEX_EQUIPMENT = ' . TABINDEX_EQUIPMENT . ';
var TABINDEX_DURATION  = ' . TABINDEX_DURATION . ';
var TABINDEX_RENT      = ' . TABINDEX_RENT . ';
var TABINDEX_TRANSPORT = ' . TABINDEX_TRANSPORT . ';
var TABINDEX_REPAIR    = ' . TABINDEX_REPAIR . ';
var TABINDEX_SERVICES  = ' . TABINDEX_SERVICES . ';
var TABINDEX_PROFORMA  = ' . TABINDEX_PROFORMA . ';
var TABINDEX_DOCUMENTS = ' . TABINDEX_DOCUMENTS . ';
var TABINDEX_INVOICES  = ' . TABINDEX_INVOICES . ';
var TABINDEX_HISTORY   = ' . TABINDEX_HISTORY . ';

/* Index des onglets du chantier */
var SITETABINDEX_GENERAL = ' . SITETABINDEX_GENERAL . ';
var SITETABINDEX_INFOS   = ' . SITETABINDEX_INFOS . ';

/* Typages des modèles de machines */
var TYPE_ACCESSORY = "' . LOC::Model::TYPE_ACCESSORY . '";
var TYPE_SUBRENT   = "' . LOC::Model::TYPE_SUBRENT . '";
var TYPE_CATEGORYB = "' . LOC::Model::TYPE_CATEGORYB . '";
var TYPE_SALE      = "' . LOC::Model::TYPE_SALE . '";

/* Suffixes du code du contrat */
var SUFFIX_SUBRENT     = "' . LOC::Contract::SUFFIX_SUBRENT . '";
var SUFFIX_FULLSERVICE = "' . LOC::Contract::SUFFIX_FULLSERVICE . '";
var SUFFIX_CATEGORYB   = "' . LOC::Contract::SUFFIX_CATEGORYB . '";

function getDocumentObject()
{
    return Location.rentContractObj;
}
function getDocumentTransportObject()
{
    return Location.rentContractObj.getTransportObject();
}
function getDocumentDocumentsObject()
{
    return Location.rentContractObj.getDocumentsObject();
}


function isProformaToGen(tab)
{
    return (tab.stateId != RENTCONTRACT_STATE_CANCELED &&
            (
                tab.flags & PFMDOCFLAG_EXISTACTIVEDEXPIRED ||
                (tab.isCustomerRequired && (tab.docEditMode == "create" || tab.isRequiredOnCreation) && tab.totalAmount > 0 && (!(tab.flags & PFMDOCFLAG_EXISTACTIVED)))
            ));
}

');


# Inclusion des fichiers JavaScript et CSS des helpers
$view->addCSSSrc('rent/helper/infos.css');

$view->addCSSSrc('rent/helper/customer.css');
$view->addJSSrc('rent/helper/customer.js');

$view->addCSSSrc('rent/helper/site.css');
$view->addJSSrc('rent/helper/site.js');

$view->addCSSSrc('rent/helper/duration.css');
$view->addJSSrc('rent/helper/duration.js');

$view->addCSSSrc('rent/helper/tariff.css');
$view->addCSSSrc('rent/helper/transport.css');
$view->addJSSrc('rent/helper/tariffRent.js');
$view->addJSSrc('rent/helper/transport.js');

$view->addCSSSrc('rent/helper/repair.css');
$view->addJSSrc('rent/helper/repair.js');

$view->addCSSSrc('rent/helper/services.css');
$view->addJSSrc('rent/helper/services.js');

$view->addCSSSrc('rent/helper/proforma.css');
$view->addJSSrc('rent/helper/proforma.js');

$view->addCSSSrc('rent/helper/documents.css');
$view->addJSSrc('rent/helper/documents.js');

$view->addCSSSrc('rent/helper/history.css');
$view->addJSSrc('rent/helper/history.js');

$view->addCSSSrc($dir . 'view.css');
$view->addJSSrc($dir . 'view.js');



# Barre de recherche
require($directory . '/rentContract/searchBar.cgi');

# Message de confirmation
my $validBlock = '
<div id="validBlock" class="messageBlock valid" style="display: none;">
    <div class="popup">
        <div class="content">
            <table>
            <tr>
                <td style="font-weight: bold;" id="success">
                    <span id="success.valid">' . htmllocale('Le contrat a été enregistré avec succès') . '.</span>
                    <span id="success.stop">' . htmllocale('Le contrat a été arrêté avec succès') . '.</span>
                    <span id="success.reactivate">' . htmllocale('Le contrat a été remis en cours avec succès') . '.</span>
                    <span id="success.invoice">' . htmllocale('Le contrat a été mis en facturation avec succès') . '.</span>
                    <span id="success.restop">' . htmllocale('Le contrat a été remis en arrêt avec succès') . '.</span>
                    <span id="success.cancel">' . htmllocale('Le contrat a été annulé avec succès') . '.</span>
                    <span id="success.reopenRepairSheets">' . htmllocale('La fiche de remise en état a été remise en cours') . '.</span>
                    <span id="success.reopenRepairSheetsAndSwitch">' . htmllocale('La fiche de remise en état a été remise en cours') . '.<br />' . htmllocale('Vous allez être redirigé sur la fiche') . '.</span>
                </td>
            </tr>
            <tr>
                <td>
                    <ul class="warnings" id="warnings">
                        <li id="warning.general.orderContact">' . htmllocale('Les informations du contact commande sont incomplètes') . '</li>
                        <li id="warning.site.contact">' . htmllocale('Les informations du contact chantier sont incomplètes') . '</li>
                        <li id="warning.documents.add">' . htmllocale('Les documents suivants n\'ont pas été ajoutés') . ':
                            <div class="details"></div>
                        </li>
                        <li id="warning.documents.edit">' . htmllocale('Les documents suivants n\'ont pas été modifiés') . ':
                            <div class="details"></div>
                        </li>
                        <li id="warning.documents.remove">' . htmllocale('Les documents suivants n\'ont pas été supprimés') . ':
                            <div class="details"></div>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr class="buttons">
                <td>' .
                $view->displayTextButton($locale->t('Ok'), '',
                                         'return false;',
                                         '', 'l', {'id' => 'validBlockBtn'}) .
                '</td>
            </tr>
            </table>
        </div>
    </div>
</div>';


my $fatalErrorMsg = htmllocale('Erreur fatale !') .
    '<p class="details">' .
    htmllocale('Merci de contacter le support informatique.') .
    '</p>';

# Bouton de réactualisation
if ($tabViewData{'editMode'} ne 'create')
{
    # Bouton "actualiser"
    my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png', 'return false;',
                                              $locale->t('Actualiser le contrat de location'),
                                              'l',
                                              {'id' => 'rentContractRefreshBtn',
                                               'tabindex' => -1,
                                               'onfocus' => 'this.blur();'
                                              });
    $view->addControlsContent($refreshBtn, 'right', 1);

    my $errorBlockRefreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png', 'return false;',
                                              $locale->t('Actualiser le contrat de location'),
                                              'l',
                                              {'id' => 'rentContractErrorBlockRefreshBtn'});
    $fatalErrorMsg = htmllocale('Erreur fatale !') .
        '<p class="details">' .
        $locale->t('Veuillez %s le contrat et renouveler l\'opération.', $errorBlockRefreshBtn) . '<br>' .
        htmllocale('Si l\'erreur se reproduit, merci de contacter le support informatique.') .
        '</p>';
}

my $addMachMin = $locale->getCurrencyFormat($tabViewData{'addMachineMinPrice'}, 0);

my $errorBlock = '
<div id="errorBlock" class="messageBlock error" style="display: none;">
    <div class="popup">
        <div class="content">
            <table>
            <tr>
                <td style="font-weight: bold;">' . htmllocale('Le contrat n\'a pas été enregistré car les erreurs suivantes ont été détectées') . '&nbsp;:</td>
            </tr>
            <tr>
                <td>
                    <ul class="errors" id="errors">
                    <li id="error.-1"><h3>' . $fatalErrorMsg . '</h3></li>

                    <li id="error.0">' . htmllocale('Une erreur est survenue. Veuillez actualiser le contrat et recommencer') . '</li>
                    <li id="error.1">' . htmllocale('Le contrat a été modifié par un autre utilisateur') . '</li>
                    <li id="error.2">' . htmllocale('Le contrat est bloqué en écriture pour maintenance, veuillez recommencer ultérieurement...') . '</li>
                    <li id="error.3">' . htmllocale('Certaines informations ne sont pas modifiables') . '</li>
                    <li id="error.5">' . htmllocale('Le contrat n\'est pas modifiable') . '</li>
                    <li id="error.10">' . htmllocale('Impossible de passer la ligne de devis en contrat') . '</li>
                    <li id="error.15">' . htmllocale('Le négociateur n\'est pas renseigné') . '</li>
                    <li id="error.20">' . htmllocale('Le client est bloqué au contrat') . '</li>
                    <li id="error.31">' . htmllocale('L\'adresse e-mail du contact commande n\'est pas valide') . '</li>
                    <li id="error.21">' . htmllocale('Impossible de prolonger le contrat sur un client bloqué') . '</li>
                    <li id="error.22">' . htmllocale('Impossible de prolonger le contrat en arrêt de plus de %s jours', $tabViewData{'nbDaysMaxExtensionStopCtt'}) . '</li>
                    <li id="error.23">' . htmllocale('Veuillez générer une pro forma de prolongation afin de prolonger le contrat') . '</li>
                    <li id="error.24">' . htmllocale('L\'horaire de livraison n\'est pas renseigné') . '</li>
                    <li id="error.25">' . htmllocale('Le modèle de la machine souhaitée ne correspond pas au modèle sélectionné') . '</li>
                    <li id="error.101">' . htmllocale('Le client n\'est pas renseigné') . '</li>
                    <li id="error.102">' . htmllocale('Le libellé du chantier n\'est pas renseigné') . '</li>
                    <li id="error.103">' . htmllocale('La ville du chantier n\'est pas renseignée') . '</li>
                    <li id="error.121">' . htmllocale('L\'adresse e-mail du contact chantier n\'est pas valide') . '</li>
                    <li id="error.104">' . htmllocale('La zone de transport n\'est pas renseignée') . '</li>
                    <li id="error.109">' . htmllocale('Le commentaire d\'abandon n\'est pas renseigné') .'</li>
                    <li id="error.120">' . htmllocale('La zone n\'est pas modifiable') .'</li>

                    <li id="error.105">' . htmllocale('Le modèle de machines n\'est pas renseigné') . '</li>
                    <li id="error.400">' . htmllocale('Le stock de ce modèle de machines ne permet pas de faire de contrat') . '</li>
                    <li id="error.106">' . htmllocale('L\'encombrement n\'est pas renseigné') . '</li>
                    <li id="error.107">' . htmllocale('La durée est incorrecte') . '</li>
                    <li id="error.108">' . htmllocale('Les dates de début et de fin ne sont pas renseignées') . '</li>
                    <li id="error.130">' . htmllocale('La date de fin est inférieure à la date de début') . '</li>
                    <li id="error.131">' . htmllocale('La date de fin est inférieure à la date de dernière facturation') . '</li>
                    <li id="error.132">' . htmllocale('La date de début n\'est pas modifiable') . '</li>
                    <li id="error.133">' . htmllocale('La date de fin n\'est pas modifiable') . '</li>
                    <li id="error.134">' . htmllocale('La date de début ne doit pas être antérieure de plus d\'un mois à la date de saisie') . '</li>
                    <li id="error.136">' . htmllocale('Le nombre de jours ouvrés a changé : Veuillez recalculer la période') . '</li>
                    <li id="error.137">' . htmllocale('Le type d\'assurance n\'est pas modifiable') . '</li>
                    <li id="error.138">' . htmllocale('Le taux d\'assurance n\'est pas modifiable') . '</li>
                    <li id="error.139">' . htmllocale('Le mode de calcul de la participation au recyclage n\'est pas disponible') . '</li>
                    <li id="error.140">' . htmllocale('Le mode de calcul de la gestion de recours n\'est pas disponible') . '</li>

                    <li id="error.801">' . htmllocale('Le type du jour n\'est pas renseigné') . '</li>
                    <li id="error.802">' . htmllocale('La date du jour n\'est pas renseignée') . '</li>
                    <li id="error.803">' . htmllocale('Le motif du jour n\'est pas renseigné') . '</li>
                    <li id="error.804">' . htmllocale('Un jour existe déjà sur la date') . '</li>
                    <li id="error.805">' . htmllocale('Le jour n\'est pas dans la période autorisée') . '</li>
                    <li id="error.806">' . htmllocale('La mise à l\'état facturable du jour n\'est pas possible car celui-ci est déjà facturable') . '</li>
                    <li id="error.807">' . htmllocale('La mise à l\'état non facturable du jour n\'est pas possible car celui-ci est déjà non facturable') . '</li>
                    <li id="error.808">' . htmllocale('Le contrat lié n\'est pas valide') . '</li>
                    <li id="error.days.details">
                        <div></div>
                    </li>
                    <li id="error.150">' . htmllocale('Impossible d\'arrêter le contrat') . '</li>
                    <li id="error.151">' . htmllocale('Impossible de remettre en cours le contrat') . '</li>
                    <li id="error.152">' . htmllocale('Impossible de mettre le contrat en facturation') . '</li>
                    <li id="error.153">' . htmllocale('Impossible de remettre en arrêt le contrat') . '</li>
                    <li id="error.154">' . htmllocale('Impossible d\'annuler le contrat') . '</li>

                    <li id="error.202">' . htmllocale('La justification de la RE n\'est pas renseignée') . '</li>
                    <li id="error.301">' . htmllocale('Pas d\'estimation de transport') . '</li>
                    <li id="error.302">' . htmllocale('La zone a changé, veuillez réestimer le transport') . '</li>
                    <li id="error.303">' . htmllocale('Le client a changé, veuillez réestimer le transport') . '</li>
                    <li id="error.304">' . htmllocale('Le montant de la livraison transport est inférieur à l\'estimation') . '</li>
                    <li id="error.305">' . htmllocale('Le montant de la récupération transport est inférieur à l\'estimation') . '</li>
                    <li id="error.306">' . htmllocale('Le contrat lié à la livraison transport est invalide') . '</li>
                    <li id="error.307">' . htmllocale('Le contrat lié à la récupération transport est invalide') . '</li>
                    <li id="error.308">' . htmllocale('Le forfait machine supplémentaire aller est inférieur à %s', $addMachMin) . '</li>
                    <li id="error.309">' . htmllocale('Le forfait machine supplémentaire retour est inférieur à %s', $addMachMin) . '</li>

                    <li id="error.504">' . $locale->t('Impossible de créer une pro forma : Le type de montant du contrat est mensuel') . '</li>
                    <li id="error.505">' . $locale->t('Action pro forma non autorisée') . '</li>
                    <li id="error.508">' . $locale->t('Impossible de créer une pro forma : La date de fin saisie est antérieure à la date de fin du contrat') . '</li>
                    <li id="error.509">' . $locale->t('Impossible de créer une pro forma : La date de fin saisie est antérieure à la date de fin de la dernière prolongation') . '</li>
                    <li id="error.512">' . $locale->t('Impossible de modifier la pro forma') . '</li>
                    <li id="error.513">' . $locale->t('Impossible d\'abandonner la pro forma') . '</li>
                    <li id="error.515">' . $locale->t('Impossible de réactiver la pro forma') . '</li>
                    <li id="error.517">' . $locale->t('Le montant de la pro forma est nul') . '</li>
                    <li id="error.518">' . $locale->t('Impossible de créer la pro forma : le mode de règlement n\'est pas renseigné') . '</li>
                    <li id="error.519">' . $locale->t('Impossible de créer la pro forma : estimation carburant incorrecte') . '</li>

                    <li id="error.600"><u>' . $locale->t('Transport côté livraison') . '</u> : ' . $locale->t('Le contrat lié du "transfert inter-chantiers" ou de la "reprise sur chantier" n\'est plus correct') . '</li>
                    <li id="error.602"><u>' . $locale->t('Transport côté livraison') . '</u> : ' . $locale->t('Incohérence de dates et d\'horaires sur le "transfert inter-chantiers" ou sur la "reprise sur chantier"') . '</li>
                    <li id="error.603"><u>' . $locale->t('Transport côté livraison') . '</u> : ' . $locale->t('La réalisation du transport est impossible.') . '</li>
                    <li id="error.604"><u>' . $locale->t('Transport côté livraison') . '</u> : ' . $locale->t('L\'annnulation du transport est impossible.') . '</li>

                    <li id="error.650"><u>' . $locale->t('Transport côté récupération') . '</u> : ' . $locale->t('Le contrat lié du "transfert inter-chantiers" ou de la "reprise sur chantier" n\'est plus correct') . '</li>
                    <li id="error.652"><u>' . $locale->t('Transport côté récupération') . '</u> : ' . $locale->t('Incohérence de dates et d\'horaires sur le "transfert inter-chantiers" ou sur la "reprise sur chantier"') . '</li>
                    <li id="error.653"><u>' . $locale->t('Transport côté récupération') . '</u> : ' . $locale->t('Il est impossible de faire un "transfert inter-chantiers" ou une "reprise sur chantier" s\'il y a une pro forma de prolongation non finalisée') . '</li>
                    <li id="error.656"><u>' . $locale->t('Transport côté récupération') . '</u> : ' . $locale->t('La réalisation du transport est impossible.') . '</li>
                    <li id="error.657"><u>' . $locale->t('Transport côté récupération') . '</u> : ' . $locale->t('L\'annnulation du transport est impossible.') . '</li>
                    <li id="error.658"><u>' . $locale->t('Transport côté récupération') . '</u> : ' . $locale->t('Le transfert de la machine vers une autre agence est impossible car la machine est déjà mise en visibilité.') . '</li>
                    <li id="error.659"><u>' . $locale->t('Transport côté récupération') . '</u> : ' . $locale->t('Il est impossible de modifier l\'option "La machine est transférée à" car toutes les machines du modèle sont réservées.') . '</li>

                    <li id="error.700">' . $locale->t('Un document ajouté est en erreur') . '</li>

                    <li id="error.550">' . $locale->t('Impossible de supprimer le document') . '</li>
                    <li id="error.551">' . $locale->t('Impossible d\'ajouter le document') . '</li>
                    <li id="error.552">' . $locale->t('Impossible de modifier le document') . '</li>

                    </ul>
                </td>
            </tr>
            <tr class="buttons">
                <td>' .
                $view->displayTextButton($locale->t('Fermer'), '',
                                         '$ge("errorBlock").style.display = "none";',
                                         '', 'l', {'id' =>'errorBlockBtn'}) .
                '</td>
            </tr>
            </table>
        </div>
    </div>
</div>';

# Popup de réouverture de fiche de remise en état
my $reopenSheetPopup = '
<div style="display: none; padding: 10px;" id="rentContractReopenSheet" ' .
    'title="' . htmllocale('Réouverture de la fiche de remise en état') . '">
<p>' . $locale->t('Vous êtes sur le point de rouvrir la fiche de remise en état n° %s.',
                  '<span id="rentContractReopenSheetNo"></span>') . '</p>
<p>' . $locale->t('Souhaitez-vous continuer ?') . '</p>
<p style="padding-top: 20px; text-align: center;">
' . $view->displayFormInputButton('rentContractReopenSheetYesSwitchBtn', $locale->t('Oui et basculer sur la fiche')) . '
' . $view->displayFormInputButton('rentContractReopenSheetYesBtn', $locale->t('Oui et rester sur le contrat'), {'style' => 'float: left'}) . '
' . $view->displayFormInputButton('rentContractReopenSheetNoBtn', $locale->t('Non'), {'style' => 'float: right'}) . '
</p>
</div>';

# erreurs lors de la réalisation du transport
my $tspErrorBlock = '
<div id="tspErrorBlock" class="messageBlock error" style="display: none;">
    <div class="popup">
        <div class="content">
            <table>
            <tr>
                <td style="font-weight: bold;">' . htmllocale('Le transport n\'a pas pu être réalisé car les erreurs suivantes ont été détectées') . '&nbsp;:</td>
            </tr>
            <tr>
                <td>
                    <ul class="errors" id="tspErrors">
                    </ul>
                </td>
            </tr>
            <tr class="buttons">
                <td>' .
                $view->displayTextButton($locale->t('Fermer'), '',
                                         '$ge("tspErrorBlock").style.display = "none";',
                                         '', 'l', {'id' => 'tspErrorBlockBtn'}) .
                '</td>
            </tr>
            </table>
        </div>
    </div>
</div>';


$view->addBodyContent($validBlock, 0, 1);
$view->addBodyContent($errorBlock, 0, 1);
$view->addBodyContent($reopenSheetPopup, 0, 1);
$view->addBodyContent($tspErrorBlock, 0, 1);




# Affichage de l'entête
$view->{'_networkOptimization'} = 'default-rentContract-view';
print $view->displayHeader();


print '
<div id="rentContractBlock" class="loading">
    <div class="loadingMask"></div>
    <div class="content">';

# Informations du contrat
require($directory . '/helper/infos.cgi');

# Informations du client et du chantier
print '
<table class="alignFieldsetBlocks">
<tr>
    <td style="width: 50%; padding-right: 10px;">
        <table style="width: 100%;">
            <tr>
                <td>';
# Informations du client
require($directory . '/helper/customer.cgi');
print '
                </td>
            </tr>
            <tr>
                <td>';
# Informations de la commande
require($directory . '/helper/order.cgi');
print '
                </td>
            </tr>
        </table>';
print '
    </td>
    <td>';
# Informations du chantier
our $prefix = 'rentSite';
require($directory . '/helper/site.cgi');
print '
    </td>
</tr>
</table>';

print '
<div id="rentContractTabBox">';

# Onglet Matériel
$tabIndex = TABINDEX_EQUIPMENT; # Id de l'onglet
require($directory . '/rentContract/viewTabs/equipment.cgi');

# Onglet Durée
$tabIndex = TABINDEX_DURATION; # Id de l'onglet
our $prefix = 'durationHlp_';
require($directory . '/helper/duration.cgi');
require($directory . '/helper/format/duration.cgi');

# Onglet Location
$tabIndex = TABINDEX_RENT; # Id de l'onglet
our $prefix = 'rentTariffHlp_';
require($directory . '/helper/tariffRent.cgi');

# Onglet Transport
$tabIndex = TABINDEX_TRANSPORT; # Id de l'onglet
our $prefix = 'transportHlp_';
require($directory . '/helper/transport.cgi');
require($directory . '/helper/format/transport.cgi');

# Onglet Remise en état
$tabIndex = TABINDEX_REPAIR; # Id de l'onglet
our $prefix = 'repairHlp_';
require($directory . '/helper/repair.cgi');

# Onglet Autres services
$tabIndex = TABINDEX_SERVICES; # Id de l'onglet
our $prefix = 'servicesHlp_';
require($directory . '/helper/services.cgi');

# Onglet Pro forma
$tabIndex = TABINDEX_PROFORMA; # Id de l'onglet
our $prefix = 'proformaHlp_';
require($directory . '/helper/proforma.cgi');
require($directory . '/helper/format/proforma.cgi');

# Onglet Document
$tabIndex = TABINDEX_DOCUMENTS; # Id de l'onglet
our $prefix = 'documentsHlp_';
require($directory . '/helper/documents.cgi');
require($directory . '/helper/format/documents.cgi');

# Onglet Facturation
$tabIndex = TABINDEX_INVOICES; # Id de l'onglet
require($directory . '/rentContract/viewTabs/invoices.cgi');

# Onglet Historique
$tabIndex = TABINDEX_HISTORY; # Id de l'onglet
our $prefix = 'historyHlp_';
require($directory . '/helper/history.cgi');
require($directory . '/helper/format/history.cgi');

print '
</div>';

# Création de la boîte à onglet
print $view->displayJSBlock('
Location.tabBoxesManager.createTabBox("rentContractTabBox", null, "280px");
function setDocumentActiveTabIndex(index)
{
    return Location.tabBoxesManager.getTabBox("rentContractTabBox").setActiveTabIndex(index);
}');






# Informations complémentaires et résumé
print '
<table class="alignFieldsetBlocks">
<tr>
    <td style="width: 50%;">';
# Informations complémentaires
require($directory . '/helper/comments.cgi');
print '
    </td>
    <td>';
# Total
require($directory . '/rentContract/viewTabs/total.cgi');
print '
    </td>
</tr>
</table>';

my @tabLeftBtns = ();
my @tabRightBtns = ();
push(@tabLeftBtns, $view->displayTextButton(
                                    $locale->t('Valider'),
                                    'valid(|Over).png',
                                    'return false;',
                                    $locale->t('Valider les modifications effectuées sur le contrat'),
                                    'l',
                                    {'id' => 'rentContractValidBtn',
                                     'class' => 'locCtrlButton disabled',
                                     'tabindex' => -1,
                                     'onfocus' => 'this.blur();'}));

push(@tabLeftBtns, $view->displayTextButton(
                                    $locale->t('Rétablir'),
                                    'revert(|Over).png',
                                    'return false;',
                                    $locale->t('Annuler les modifications effectuées sur le contrat'),
                                    'l',
                                    {'id' => 'rentContractResetBtn',
                                     'class' => 'locCtrlButton disabled',
                                     'tabindex' => -1,
                                     'onfocus' => 'this.blur();'}));

# En mise à jour seulement on affiche les boutons (Arrêt, Remise en cours, Mise en facturation, Annulation)
if ($tabViewData{'editMode'} eq 'update')
{
    push(@tabRightBtns,
    '<div id="rentContractActionBtns" class="actionButtons cpanel_grp">' .
        '<label>' . &htmllocale('Actions') . '&nbsp;:</label>' .
        $view->displayTextButton(
                            '',
                            'stop(|Over).png',
                            'return false;',
                            $locale->t('Arrêter le contrat'),
                            'l',
                            {'id'    => 'rentContractStopBtn',
                             'class' => 'locCtrlButton disabled',
                             'tabindex' => -1,
                             'onfocus' => 'this.blur();'
                            }) .
        $view->displayTextButton(
                            '',
                            'revert(|Over).png',
                            'return false;',
                            $locale->t('Remettre en cours le contrat'),
                            'l',
                            {'id'    => 'rentContractReactivateBtn',
                             'class' => 'locCtrlButton disabled',
                             'tabindex' => -1,
                             'onfocus' => 'this.blur();'
                            }) .
        $view->displayTextButton(
                            '',
                            'coins(|Over).png',
                            'return false;',
                            $locale->t('Mettre en facturation le contrat'),
                            'l',
                            {'id'    => 'rentContractInvoiceBtn',
                             'class' => 'locCtrlButton disabled',
                             'tabindex' => -1,
                             'onfocus' => 'this.blur();'
                            }) .
        $view->displayTextButton(
                            '',
                            'restop(|Over).png',
                            'return false;',
                            $locale->t('Remettre en arrêt le contrat'),
                            'l',
                            {'id'    => 'rentContractRestopBtn',
                             'class' => 'locCtrlButton disabled',
                             'tabindex' => -1,
                             'onfocus' => 'this.blur();'
                            }) .
        $view->displayTextButton(
                            '',
                            'cancel(|Over).png',
                            'return false;',
                            $locale->t('Annuler le contrat'),
                            'l',
                            {'id'    => 'rentContractCancelBtn',
                             'class' => 'locCtrlButton disabled',
                             'tabindex' => -1,
                             'onfocus' => 'this.blur();'
                            }) .
    '</div>');

    # Boutons d'impression du bon de valorisation
    if (defined $tabContractInfos->{'tabValorizationInfos'})
    {
        push(@tabRightBtns,
        '<div id="rentContractVlzPrintBtns" class="cpanel_grp cpanel_sep_l">' .
            '<label>' . &htmllocale('Bon de valorisation') . '&nbsp;:</label>' .
            $view->displayPrintButtons('vlzPrintBtns',
                                       $tabContractInfos->{'tabValorizationInfos'}->{'url'},
                                       $locale->t('AI_BonValorisation_%s.pdf', $tabContractInfos->{'code'}),
                                       $tabContractInfos->{'tabCustomer'}->{'orderContact.email'},
                                       $tabContractInfos->{'tabValorizationInfos'}->{'mailSubject'},
                                       $tabContractInfos->{'tabValorizationInfos'}->{'mailContent'},
                                       $tabContractInfos->{'agency.id'},
                                       {
                                           'buttons'       => ['mail', 'pdf', 'print'],
                                           'tabEndActions' => [
                                               {
                                                   'type'      => 'logPrint',
                                                   'entity'    => 'VALO CONTRAT',
                                                   'entityIds' => [$tabContractInfos->{'id'}]
                                               }
                                           ]
                                       }) .
        '</div>');
    }

    # Boutons d'impression du contrat
    my $pdfUrl = {'url' => 'rent:rentContract:pdf', 'options' => {'contractsIds' => [$tabContractInfos->{'id'}]}};
    my $pdfName = $locale->t('AI_Contrat_%s.pdf', $tabContractInfos->{'code'});

    push(@tabRightBtns,
    '<div id="rentContractPrintBtns" class="cpanel_grp cpanel_sep_l">' .
        '<label>' . &htmllocale('Contrat') . '&nbsp;:</label>' .
            $view->displayPrintButtons('printBtns',
                                       $pdfUrl,
                                       $pdfName,
                                       $tabContractInfos->{'tabCustomer'}->{'orderContact.email'},
                                       $locale->t('Votre contrat de location'),
                                       $tabViewData{'emailContent'},
                                       $tabContractInfos->{'agency.id'},
                                       {
                                           'buttons'       => ['mail', 'pdf', 'print'],
                                           'tabEndActions' => [
                                               {
                                                   'type'      => 'logPrint',
                                                   'entity'    => 'CONTRAT',
                                                   'entityIds' => [$tabContractInfos->{'id'}]
                                               },
                                               {
                                                   'type'      => 'printContract',
                                                   'contractId' => [$tabContractInfos->{'id'}]
                                               }
                                           ],
                                           'tabAdditionalAttachments' => $tabViewData{'additionalAttachments'}
                                       }) .
    '</div>');
}
print $view->displayControlPanel({'left' => \@tabLeftBtns, 'right' => \@tabRightBtns, 'substyle' => 'middle', 'separator' => ''});



# Formatage des données de l'onglet Durée
&hlpFormatDurationData($tabContractInfos->{'tabDuration'}, $locale, {});

# Formatage des données de l'onglet Transport
&hlpFormatTransportData($tabContractInfos->{'tabTransport'}, $locale, {});

# Formatage des données de l'onglet Pro forma
&hlpFormatProformaData($tabContractInfos->{'tabProforma'}, $locale, {});

# Formatage des données de l'onglet Documents
&hlpFormatDocumentsData($tabContractInfos->{'tabDocuments'}, $locale, {});

# Formatage des données de l'onglet Historique
&hlpFormatHistoryData($tabContractInfos->{'tabHistory'}, $locale, {'timeZone' => $tabViewData{'timeZone'}});


# Liste des plages horaires de transport
my $tabTransportTimeSlots = [];
while (my ($id, $tabInfos) = each(%{$tabViewData{'tabTransportTimeSlots'}}))
{
    push(@$tabTransportTimeSlots, {
        'id'        => $id,
        'isActive'  => (($tabInfos->{'state.id'} eq LOC::Transport::TimeSlot::STATE_ACTIVE) ? 1 : 0),
        'label'     => $tabInfos->{'label'},
        'beginHour' => $tabInfos->{'beginHour'},
        'endHour'   => $tabInfos->{'endHour'}
    });
}

# Définition du tableau des types de montant
my $tabAmountTypes = [
    {'label' => $locale->t('par jour'), 'id' => 'day'},
    {'label' => $locale->t('par mois'), 'id' => 'month'}
];
if ($tabContractInfos->{'tabRentTariff'}->{'amountType'} eq 'fixed')
{
    push(@$tabAmountTypes, {'label' => $locale->t('forfait'), 'id' => 'fixed'});
}

my $tabContractCfgs = {
    'tabInfos'                  => $tabContractInfos,
    'isTrspTarification'        => $tabViewData{'isTrspTarification'},
    'modelInfosUrl'             => $view->createURL('rent:rentContract:modelInfos'),
    'rentalPeriodInfosUrl'      => $view->createURL('rent:rentContract:rentalPeriodInfos'),
    'rentalPeriodEndDateMode'   => 'date',
    'transportWebServicesUrl'   => &LOC::Globals::get('tariffTrspWebServicesUrl'),
    'transportEstimationUrl'    => $tabViewData{'tariffTrspEstimationUrl'},
    'transportContractsTsfUrl'  => $view->createURL('rent:rentContract:jsonListForSitesTransfer'),
    'doTransportUrl'            => $view->createURL('rent:rentContract:doTransport'),
    'addMachinePrice'           => $tabViewData{'addMachinePrice'},
    'addMachineMinPrice'        => $tabViewData{'addMachineMinPrice'},
    'isTrspTarification'        => $tabViewData{'isTrspTarification'},
    'tempId'                    => $tabViewData{'tempId'},
    'saveUrl'                   => $view->createURL('rent:rentContract:save'),
    'getUrlUrl'                 => $view->createURL('rent:rentContract:getUrl'),
    'defaultInsuranceRate'      => $tabViewData{'defaultInsuranceRate'},
    'defaultInsuranceTypeId'    => $tabViewData{'defaultInsuranceTypeId'},
    'tabInsuranceTypesList'     => $tabViewData{'insurance.types'},
    'defaultAppealTypeId'       => $tabViewData{'defaultAppealTypeId'},
    'defaultAppealMode'         => $tabViewData{'defaultAppealMode'},
    'defaultAppealRate'         => $tabViewData{'defaultAppealRate'},
    'tabAppealTypesList'        => $tabViewData{'appeal.types'},
    'defaultResiduesMode'       => $tabViewData{'defaultResiduesMode'},
    'useResiduesForAccessories' => $tabViewData{'useResiduesForAccessories'},
    'defaultResiduesAmount'     => $tabViewData{'defaultResiduesAmount'},
    'defaultResiduesRate'       => $tabViewData{'defaultResiduesRate'},
    'rentServiceMinAmount'      => $tabViewData{'rentServiceMinAmount'},
    'defaultStdReducMode'       => $tabViewData{'defaultStdReducMode'},
    'proformaAdvancePercent'    => $tabViewData{'proformaAdvancePercent'},
    'fuelUnitPrice'             => $tabViewData{'fuelUnitPrice'},
    'tabAmountTypes'            => $tabAmountTypes,
    'tabTransportTimeSlots'     => $tabTransportTimeSlots,
    'maxSitesTsfIntervals'      => $tabViewData{'maxSitesTsfIntervals'},
    'nbDaysMaxExtensionStopCtt' => $tabViewData{'nbDaysMaxExtensionStopCtt'},
    'tabIndex'                  => $tabViewData{'tabIndex'},
    'customerInfosUrl'          => $view->createURL('accounting:customer:jsonInfos', {'flags' => LOC::Customer::GETINFOS_TARIFF |
                                                                                                 LOC::Customer::GETINFOS_CRC |
                                                                                                 LOC::Customer::GETINFOS_FLAGS}),
    'maxFileSize'               => $tabViewData{'maxFileSize'},
    'maxMailAttachmentsSize'    => $tabViewData{'maxMailAttachmentsSize'},
    'uploadDocumentUrl'         => $view->createURL('rent:rentContract:uploadDocument'),
    'generateDocumentsMailUrl'  => $view->createURL('rent:rentContract:jsonDocumentsMailUrl'),
    'tabDayReasonsList'         => $tabViewData{'tabDayReasonsList'},
    'tabDayInvoiceDurationsList' => $tabViewData{'tabDayInvoiceDurationsList'},
    'dayExtraInfosUrl'          => $view->createURL('rent:rentContract:jsonDayExtraInfos')
};

$view->addBodyEndCode($view->displayJSBlock('
Location.rentContractObj = new Location.RentContract("' . $tabContractInfos->{'country.id'} . '", ' .
                                                          ($tabContractInfos->{'id'} ? $tabContractInfos->{'id'} : 'null') . ', ' .
                                                          &LOC::Json::toJson($tabContractCfgs) . ');'));



# Affichage du pied de page
print '
    </div>
</div>';

print $view->displayFooter();
