use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Json;
use LOC::Locale;

our %tabViewData;

# Locale
my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});

if ($tabViewData{'duration'} != 0)
{
    $tabViewData{'durationStr'} = $locale->tn('%d jour ouvré', '%d jours ouvrés',
                                              $tabViewData{'duration'},
                                              $tabViewData{'duration'});
}
$tabViewData{'requiredDurationStr'} = $locale->tn('%d jour ouvré', '%d jours ouvrés',
                                                  $tabViewData{'requiredDuration'},
                                                  $tabViewData{'requiredDuration'});


my $view = LOC::Json->new();
print $view->displayHeader();
print $view->encode(\%tabViewData);
