use utf8;
binmode STDOUT, ":raw";    # Pour envoyer le flux en l'état

use LOC::Locale;
use LOC::Browser;

our %tabViewData;

my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});

my $tabParams = [];

foreach my $tabContractInfos (@{$tabViewData{'tabContracts'}})
{
    my $tabInfos = {
        'CONTRATCODE'       => $tabContractInfos->{'code'},
        'PEPRENOM'          => $tabContractInfos->{'creator.tabInfos'}->{'firstName'},
        'PENOM'             => $tabContractInfos->{'creator.tabInfos'}->{'name'},
        'CONTRATNUMCOMM'    => $tabContractInfos->{'orderNo'},
        'AGLIBELLE'         => $tabContractInfos->{'agency.tabInfos'}->{'label'},
        'AGRESPONSABLE'     => $tabContractInfos->{'agency.tabInfos'}->{'manager.fullName'},
        'AGADRESSE'         => $tabContractInfos->{'agency.tabInfos'}->{'address'},
        'AGCP'              => $tabContractInfos->{'agency.tabInfos'}->{'postalCode'},
        'AGVILLE'           => $tabContractInfos->{'agency.tabInfos'}->{'city'},
        'AGTEL'             => $tabContractInfos->{'agency.tabInfos'}->{'telephone'},
        'AGFAX'             => $tabContractInfos->{'agency.tabInfos'}->{'fax'},
        'CLSTE'             => $tabContractInfos->{'customer.tabInfos'}->{'label'},
        'CLTEL'             => $tabContractInfos->{'customer.tabInfos'}->{'telephone'},
        'CLFAX'             => $tabContractInfos->{'customer.tabInfos'}->{'fax'},
        'CLSIREN'           => $tabContractInfos->{'customer.tabInfos'}->{'siren'},
        'CLADRESSE'         => $tabContractInfos->{'customer.tabInfos'}->{'address'},
        'CLADRESSE2'        => $tabContractInfos->{'customer.tabInfos'}->{'address2'},
        'VICLCP'            => $tabContractInfos->{'customer.tabInfos'}->{'postalCode'},
        'VICLNOM'           => $tabContractInfos->{'customer.tabInfos'}->{'city'},
        'CHLIBELLE'         => $tabContractInfos->{'site.tabInfos'}->{'label'},
        'CHADRESSE'         => $tabContractInfos->{'site.tabInfos'}->{'address'},
        'VICP'              => $tabContractInfos->{'site.tabInfos'}->{'postalCode'},
        'VINOM'             => $tabContractInfos->{'site.tabInfos'}->{'city'},
        'CONTRATCONTACTCOMM'   => $tabContractInfos->{'orderContact.fullName'},
        'CONTRATTELEPHONECOMM' => $tabContractInfos->{'orderContact.telephone'},
        'CHCONTACT'         => $tabContractInfos->{'site.tabInfos'}->{'contact.fullName'},
        'CHTELEPHONE'       => $tabContractInfos->{'site.tabInfos'}->{'telephone'},
        'MANOPARC'          => $tabContractInfos->{'machine.tabInfos'}->{'parkNumber'},
        'MANOSERIE'         => $tabContractInfos->{'machine.tabInfos'}->{'serialNumber'},
        'MOMADESIGNATION'   => $tabContractInfos->{'machine.tabInfos'}->{'model.label'},
        'FAMAMODELE'        => $tabContractInfos->{'machine.tabInfos'}->{'machinesFamily.shortLabel'},
        'FOLIBELLE'         => $tabContractInfos->{'machine.tabInfos'}->{'manufacturer.label'},
        'FAMAENERGIE'       => $tabContractInfos->{'machine.tabInfos'}->{'machinesFamily.energy'},
        'FAMAELEVATION'     => $tabContractInfos->{'machine.tabInfos'}->{'machinesFamily.elevation'},
        'MOMAPOIDS'         => $tabContractInfos->{'machine.tabInfos'}->{'model.weight'},
        'MOTARIFMSFR'       => ($tabContractInfos->{'rentTariff.tabInfos'}->{'type'} eq 'month' && $tabContractInfos->{'rentTariff.tabInfos'}->{'specialReductionPercent'} != 100 ?
                                    $tabContractInfos->{'rentTariff.tabInfos'}->{'unitPrice'} / (1 - $tabContractInfos->{'rentTariff.tabInfos'}->{'specialReductionPercent'} / 100) : 0),
        'MOTARIFMSFR2'      => $tabContractInfos->{'rentTariff.tabInfos'}->{'unitPrice'},
        'MOPXJOURFR'        => ($tabContractInfos->{'rentTariff.tabInfos'}->{'type'} eq 'day' && $tabContractInfos->{'rentTariff.tabInfos'}->{'specialReductionPercent'} != 100 ?
                                    $tabContractInfos->{'rentTariff.tabInfos'}->{'unitPrice'} / (1 - $tabContractInfos->{'rentTariff.tabInfos'}->{'specialReductionPercent'} / 100) : 0),
        'MOPXJOURFR2'       => $tabContractInfos->{'rentTariff.tabInfos'}->{'unitPrice'},
        'PAMONNAIE'         => $tabViewData{'currencySymbol'},
        'MOREMISEEX'        => $tabContractInfos->{'rentTariff.tabInfos'}->{'specialReductionPercent'},
        'ALLIBELLE'         => $tabContractInfos->{'rentTariff.tabInfos'}->{'specialReductionBillLabel'},
        'delivery.baseAmount' => $tabContractInfos->{'transportTariff.tabInfos'}->{'deliveryBaseAmount'},
        'delivery.amount'    => $tabContractInfos->{'transportTariff.tabInfos'}->{'deliveryAmount'},
        'delivery.specialReductionPercent' => $tabContractInfos->{'transportTariff.tabInfos'}->{'deliverySpecialReductionPercent'},
        'delivery.specialReductionBillLabel' => $tabContractInfos->{'transportTariff.tabInfos'}->{'deliverySpecialReductionBillLabel'},
        'recovery.baseAmount' => $tabContractInfos->{'transportTariff.tabInfos'}->{'recoveryBaseAmount'},
        'recovery.amount'    => $tabContractInfos->{'transportTariff.tabInfos'}->{'recoveryAmount'},
        'recovery.specialReductionPercent' => $tabContractInfos->{'transportTariff.tabInfos'}->{'recoverySpecialReductionPercent'},
        'recovery.specialReductionBillLabel' => $tabContractInfos->{'transportTariff.tabInfos'}->{'recoverySpecialReductionBillLabel'},
        'CONTRATPASSURANCE'       => $tabContractInfos->{'insurance.tabInfos'}->{'rate'},
        'isInsuranceTypeInvoiced' => $tabContractInfos->{'insurance.tabInfos'}->{'isInvoiced'},
        'isAppealTypeInvoiced'    => ($tabContractInfos->{'appeal.tabInfos'} ? $tabContractInfos->{'appeal.tabInfos'}->{'isInvoiced'} : 0),
        'cleaning.type.id'  => $tabContractInfos->{'cleaning.typeId'},
        'residues.mode'     => $tabContractInfos->{'residues.mode'},
        'residues.value'    => $tabContractInfos->{'residues.value'},
        'fly_id'            => $tabContractInfos->{'machine.tabInfos'}->{'family.id'},
        'MOPALIBELLE'       => $tabContractInfos->{'customer.tabInfos'}->{'paymentMode.label'},
        'CLPOTENTIEL'       => $tabContractInfos->{'customer.tabInfos'}->{'potential'},
        'CONTRATDD'         => $tabContractInfos->{'beginDate'},
        'CONTRATDR'         => $tabContractInfos->{'endDate'},
        'CONTRATJOURSPLUS'  => $tabContractInfos->{'additionalDays'},
        'CONTRATJOURSMOINS' => $tabContractInfos->{'deductedDays'},
        'CONTRATDUREE'      => $tabContractInfos->{'duration'},
        'CONTRATCOMMENTAIRE' => $tabContractInfos->{'comments'}
    };

    if (defined $tabContractInfos->{'startCode.code'})
    {
        $tabInfos->{'startCode.code'} = $tabContractInfos->{'startCode.code'};
    }

    # Assurance
    my $insuranceLabel = $tabContractInfos->{'insurance.tabInfos'}->{'fullLabel'};
    my $percent = $locale->t('%s %%', $tabContractInfos->{'insurance.tabInfos'}->{'rate'} * 100);
    $insuranceLabel =~ s/<%percent>/$percent/g;

    $tabInfos->{'insurance.label'} = $insuranceLabel;

    # Gestion des recours
    my $appealLabel = '';
    if ($tabContractInfos->{'appeal.tabInfos'})
    {
        $appealLabel = $tabContractInfos->{'appeal.tabInfos'}->{'label'};
        my $percent = $locale->t('%s %%', $tabContractInfos->{'appeal.tabInfos'}->{'rate'} * 100);
        $appealLabel =~ s/<%percent>/$percent/g;
    }
    $tabInfos->{'appeal.label'} = $appealLabel;

    push(@$tabParams, $tabInfos);
}

my $tabAdditionalDocuments = &LOC::Json::fromJson(&LOC::Characteristic::getCountryValueByCode('PDFCPLCONTRAT', $tabViewData{'tabData'}->{'countryId'}));
foreach my $tabZone (values(%$tabAdditionalDocuments))
{
    foreach my $documentInfos (@$tabZone)
    {
        $documentInfos->{'path'} =~ s/<%commonFilesPath>/@{[&LOC::Globals::get('commonFilesPath')]}/;
    }
}

my $tabData = {
    'tabParams'              => $tabParams,
    'tabAdditionalDocuments' => $tabAdditionalDocuments,
    'oldFrameworkPath'       => &LOC::Globals::get('oldFrameworkPath'),
    'logoImg'                => &LOC::Globals::getLogo($tabViewData{'countryId'}, LOC::Globals::LOGO_TYPE_RASTER),
    'env'                    => &LOC::Globals::getEnv(),
    'footer'                 => &LOC::Characteristic::getCountryValueByCode('PIEDIMPR', $tabViewData{'countryId'}),
    'capital'                => $locale->getCurrencyFormat(&LOC::Country::getCapital($tabViewData{'countryId'}, undef, $tabViewData{'currencySymbol'}))
};

my $params = &LOC::Json::toJson($tabData);

&LOC::Browser::sendHeaders("Content-Type: application/pdf;\n\n");

my $command = 'php ' . $tabViewData{'pdfUrl'} . ' ' . &LOC::Util::utf8Base64Encode($params);
print qx($command);
