use utf8;
use open (':encoding(UTF-8)');


$view->addCSSSrc($dir . 'search.css');


# N°
my $noInput = $view->displayFormInputText('search.no', $tabSearchData{'search.no'}, '',
                                          {'size' => 16, 'tabindex' => $inputsTabIndex++});

# États
tie(my %tabStates, 'Tie::IxHash');
$tabStates{''} = '-- ' . $locale->t('Tous') . ' --';
foreach my $stateId (sort keys(%{$tabSearchData{'states'}}))
{
    $tabStates{$stateId} = $tabSearchData{'states'}->{$stateId};
}
my $statesInput = $view->displayFormSelect('search.state.id', \%tabStates, $tabSearchData{'search.state.id'}, 0,
                                           {'tabindex' => $inputsTabIndex++});

# Client
my $custListUrl = $view->createURL('accounting:customer:jsonList', {'countryId' => $tabSearchData{'country.id'}, 'format' => 0});
my $custInput = $view->displayFormSelect('search.customer.id', $tabSearchData{'customers'}, $tabSearchData{'search.customer.id'}, 0,
                                         {'tabindex' => $inputsTabIndex}) .
                $view->displayJSBlock('
Location.searchBoxesManager.createSearchBox("search.customer.id", {isRequired: false, url: "' . $custListUrl . '"}).createElement = function(key, value)
{
    return new Option((value["name"] || "") + ", " + (value["locality.postalCode"] || ""), value["id"]);
}');
$inputsTabIndex += 2;

# National
my $isNationalInput = $view->displayFormInputCheckBox(
        'search.isNational',
        $tabSearchData{'search.isNational'},
        {
            'tabindex' => $inputsTabIndex
        },
        '',
        1
    );
$inputsTabIndex++;

# N° de parc
my $parkNoInput = $view->displayFormInputText('search.parkNo', $tabSearchData{'search.parkNo'}, '',
                                              {'size' => 5, 'tabindex' => $inputsTabIndex++});

# Libellé chantier
my $siteLabelInput = $view->displayFormInputText('search.site.label', $tabSearchData{'search.site.label'}, '',
                                              {'size' => 25, 'tabindex' => $inputsTabIndex++});

# Département chantier
my $siteDepartmentInput = $view->displayFormInputText('search.site.department', $tabSearchData{'search.site.department'}, '',
                                              {'size' => 3, 'tabindex' => $inputsTabIndex++});

# Date début Location
my $beginDateInput = $view->displayFormInputDate('search.beginDate', ($tabSearchData{'search.beginDate'} eq '' ? undef : $tabSearchData{'search.beginDate'}),
                                                 undef, undef, {'tabindex' => $inputsTabIndex++});

# Date fin location
my $endDateInput = $view->displayFormInputDate('search.endDate', ($tabSearchData{'search.endDate'} eq '' ? undef : $tabSearchData{'search.endDate'}),
                                               undef, undef, {'tabindex' => $inputsTabIndex++});

# Utilisateurs
my $userListUrl = $view->createURL('default:user:jsonList', {'format' => 1});
my $usersInput = $view->displayFormSelect('search.user.id', $tabSearchData{'users'}, $tabSearchData{'search.user.id'}, 0,
                                          {'tabindex' => $inputsTabIndex}) .
                 $view->displayJSBlock('Location.searchBoxesManager.createSearchBox("search.user.id", {isRequired: false, url: "' . $userListUrl . '"});');
$inputsTabIndex += 2;

# Négociateur
my $userListUrl = $view->createURL('default:user:jsonList', {'format' => 1});
my $interlocInput = $view->displayFormSelect('search.negotiator.id',$tabSearchData{'negotiators'}, $tabSearchData{'search.negotiator.id'}, 0, {'tabindex' => $inputsTabIndex}) .
                    $view->displayJSBlock('Location.searchBoxesManager.createSearchBox("search.negotiator.id", {isRequired: false, url: "' . $userListUrl . '"});');

# Famille tarifaire
tie(my %tabTariffFamilies, 'Tie::IxHash');
$tabTariffFamilies{''} = '-- ' . $locale->t('Toutes') . ' --';
foreach my $tariffFamilyId (keys(%{$tabSearchData{'tariffFamilies'}}))
{
    $tabTariffFamilies{$tariffFamilyId} = $tabSearchData{'tariffFamilies'}->{$tariffFamilyId};
}
my $tariffFamiliesInput =  $view->displayFormSelect('search.tariffFamily.id',\%tabTariffFamilies, $tabSearchData{'search.tariffFamily.id'}, 0, {'tabindex' => $inputsTabIndex++});

$inputsTabIndex += 2;


my $tabLines = [
    [
        {'name' => $locale->t('N°'), 'input' => $noInput},
        {'name' => $locale->t('État'), 'input' => $statesInput},
        {'name' => $locale->t('Client'), 'help' => $locale->t('La recherche de client s\'effectue parmi la raison sociale, le code client, la localité, le code postal et le département'), 'input' => $custInput},
        {'name' => $locale->t('Machine'), 'input' => $parkNoInput},
        {'name' => $locale->t('Rech. nat.'), 'help' => $locale->t('Effectue une recherche au niveau national'), 'input' => $isNationalInput}
    ],
    [
        {'name' => $locale->t('Lib. chantier'), 'input' => $siteLabelInput},
        {'name' => $locale->t('Dép. chantier'), 'input' => $siteDepartmentInput}
    ],
    [
        {'name' => $locale->t('Début'), 'help' => $locale->t('La recherche prend en compte les contrats dont la date de début commence après la date saisie'), 'input' => $beginDateInput},
        {'name' => $locale->t('Fin'), 'help' => $locale->t('La recherche prend en compte les contrats dont la date de fin se termine avant la date saisie'), 'input' => $endDateInput}
    ],
    [
        {'name' => $locale->t('Créé par'), 'input' => $usersInput},
        {'name' => $locale->t('Négociateur'), 'input' => $interlocInput},
    ],
    [
        {'name' => $locale->t('Famille Tarifaire'), 'input' => $tariffFamiliesInput}
    ]
];


# Fonction de vidage des champs de recherche
$view->addJSSrc('
function goSearch()
{
    if (LOC_Common.addEltClass(\'searchBar\', \'search\'))
    {
        window.document.getElementsByName("searchBar.form")[0].submit();
        return true;
    }
    return false;
}
function clearSearch()
{
    $ge("search.no").value = "";
    $ge("search.state.id").selectedIndex = 0;
    $ge("search.tariffFamily.id").selectedIndex = 0;
    Location.searchBoxesManager.getSearchBox("search.user.id").clear();
    Location.searchBoxesManager.getSearchBox("search.customer.id").clear();
    Location.searchBoxesManager.getSearchBox("search.negotiator.id").clear();
    $ge("search.parkNo").value = "";
    $ge("search.site.label").value = "";
    $ge("search.site.department").value = "";
    clearMaskedInputValue("search.beginDate");
    clearMaskedInputValue("search.endDate");
    $ge("search.isNational").checked = false;
}');


# Formulaire de recherche
my $searchUrl = $view->createURL('rent:rentContract:search');
my $searchBar = '
<form method="post" action="' . $searchUrl . '" onsubmit="return goSearch();" autocomplete="off" name="searchBar.form">' .
    $view->displaySearchBar('searchBar', $tabLines,
                            {'onsearch' => 'goSearch()',
                             'onclear'  => 'clearSearch()'}) . '
    <div class="hidden-submit"><input type="submit" tabindex="-1"/></div>
</form>';

$view->addControlsContent($searchBar, 'left', 1);


# Bouton "nouveau contrat"
my $btn = $view->displayTextButton($locale->t('Nouveau'), 'newDocument(|Over).png', 'rent:rentContract:view',
                                   $locale->t('Saisir un nouveau contrat de location'),
                                   'l',
                                   {'tabindex' => -1,
                                    'onfocus' => 'this.blur();'
                                   });

$view->addControlsContent($btn, 'right', 1);


1;
