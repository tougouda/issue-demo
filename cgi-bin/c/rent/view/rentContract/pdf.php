<?php

// GENERATION DU PDF CONTRAT
$tabParams = (array)json_decode(base64_decode($argv[1]), false);
mb_internal_encoding('cp1252');

$rcs = mb_convert_encoding(str_replace('<%capital>', $tabParams['capital'], $tabParams['footer']), 'cp1252', 'utf8');
$traduction = array(
    // Specimen
    "Specimen" => "sp�cimen",

    // Ecxept. Num�ro ESPAGNE
    "NumPlateformeTel" => "",
    "NumPlateformeFax" => "",

    /* Contrat - CO *
     * Page1        */
    "COtitre1" => "CONTRAT DE LOCATION",
    "COtitre2" => "NACELLES INDUSTRIELLES ET CHARIOTS TELESCOPIQUES",
    "COtitre3" => "LOCATION DE NACELLES INDUSTRIELLES ET DE CHARIOTS TELESCOPIQUES",
    "COtitre4" => "CONDITIONS PARTICULI�RES",
    "COtitre5" => "Notification d'arr�t ou de prorogation",
    "COnum" => "N� contrat : ",
    "COagent" => "Agent loueur : ",
    "COncom" => "N� commande : ",
    "COagence" => "Agence ",
    "COtel" => "T�l. ",
    "COfax" => "Fax. ",
    "COle" => "Le ",
    "COchefag" => "Chef d'agence : ",
    "COclient" => "Le ",
    "COinfocli" => "Client : ",
    "COcltel" => "T�l.",
    "COclfax" => "Fax. ",
    "COclsiren" => "SIRET : ",
    "COchantier" => "Chantier : ",
    "COrespcom" => "Responsable commande : ",
    "COrespchantier" => "Responsable chantier : ",
    "COdefmatlou�" => "D�finition mat�riel lou�",
    "COnparc" => "N� parc : ",
    "COnserie" => "N� s�rie : ",
    "COdesign" => "D�signation",
    "COfamille" => "Famille",
    "COmarque" => "Marque",

    "COenergie" => "�nergie",
    "COhaut" => "Hauteur",
    "COpoids" => "Poids",
    "COlocjrht" => "Location jour HT",
    "COlocmoht" => "Location mois HT",
    "COtranspallerht" => "Transport aller HT",
    "COtranspretourht" => "Transport retour HT",
    "COprixnet" => "Prix net : ",
    "COremex" => "Apr�s remise exceptionnelle de ",
    "COtariftra" => "Tarif transport aller net : ",
    "COtariftrr" => "Tarif transport retour net : ",
    "COinfo1" => "",
    "COinfocl1" => "A d�faut, en cas de perte ou de d�t�rioration du mat�riel lou�, celui-ci sera factur� au prix du mat�riel neuf + TVA.",


    // par d�faut
    "COinfo2"     => "Le carburant, le nettoyage de la machine et la participation au recyclage seront factur�s en fin de contrat.\nNettoyage standard : 12 � pour une �lectrique, 16 � pour une diesel. ",
    // suivant le mode de calcul de la participation au recyclage
    "COinfo2-P"   => "Le carburant et le nettoyage de la machine seront factur�s en fin de contrat.\nNettoyage standard : 12 � pour une �lectrique, 16 � pour une diesel. ",
    "COinfo2-F"   => "Le carburant, le nettoyage de la machine et la participation au recyclage seront factur�s en fin de contrat.\nNettoyage standard : 12 � pour une �lectrique, 16 � pour une diesel. ",
    "COinfo2-0"   => "Le carburant sera factur� en fin de contrat.\n",
    "COinfo2-1"   => "Le carburant et le nettoyage de la machine seront factur�s en fin de contrat.\nNettoyage standard : 12 � pour une �lectrique, 16 � pour une diesel. ",
    "COinfo2-2-P" => "Le carburant sera factur� en fin de contrat.\n",
    "COinfo2-2-F" => "Le carburant et la participation au recyclage seront factur�s en fin de contrat.\n",
    "COinfo2-3"   => "Participation au recyclage : %residuesLabel%.",
    "COappealLabel" => "Gestion de recours : %appealLabel%.",

    "COinfo3" => "Conditions de r�glement (cf art. 6.2 des Conditions G�n�rales de Location) : ",
    "COinfo4" => "",
    "COinfo5" => "Attention :",
    "COinfo6" => "les prix indiqu�s ci-dessus ne sont valables que si la dur�e de location est maintenue.",
    "COinfo6bis" => "La dur�e minimum de location est fix�e � 3 jours.",
    "COinfo7" => "Le client dispose de 24 heures maximum pour informer Acces Industrie de d�fauts ou d�g�ts constat�s � la livraison de la\n"
               . "machine. Pass� ce d�lai, toute contestation concernant la responsabilit� d'Acces Industrie ne sera pas reconnue, et les d�g�ts\n"
               . "constat�s sur la machine seront factur�s au client.\n"
               . "La machine reste sous la responsabilit� du client jusqu'� sa restitution en agence ou l'enl�vement par les soins d'Acces Industrie.\n"
               . "Aucune modification  du mat�riel ou de son Installation ne sera apport�e sans l'accord d'Acces Industrie.",
    "COdatedep" => "Date de d�part : ",
    "COdateret" => "Date pr�visionnelle de retour : ",
    "COjourplus" => "Jour en plus : ",
    "COjourmoins" => "Jour en moins : ",
    "COduree" => "Dur�e : ",
    "COdigicode" => "Code de d�marrage : ",
    "COinfo30" => "La machine est �quip�e d'un dispositif permettant sa g�olocalisation par le loueur.",
    "COmmentaire" => "Commentaire : ",
    "COinfo11" => "Nous vous remercions de bien vouloir retourner ce contrat d�ment sign� et rev�tu du cachet de votre entreprise.",
    "COinfo12" => "Signature du locataire pr�c�d�e de la mention \"lu et approuv�\"",
    "COinfo13" => "Cachet de l'entreprise",
    "COLe" => "Le ____/____/_______",
    "COinfo14" => "L'arr�t de la location devra �tre confirm� par notification �crite pour �tre pris en compte (document ci-joint).",
    "COinfo16" => str_replace("\n", ' - ', $rcs),
    "COinfo17" => "Fait en 2 exemplaires.",
    "COinfo26" => "Tribunal comp�tent :",
    "COinfo27" => "pour tout litige, les parties attribuent comp�tence exclusive au Tribunal de commerce d'Agen (47), nonobstant appel en garantie ou pluralit� de d�fendeurs et m�me pour les proc�dures en r�f�r� ou par requ�te.",
    "COinfo28" => "Conditions g�n�rales :",
    "COinfo29" => "l'acceptation du contrat emporte acceptation sans r�serve des Conditions G�n�rales de location figurant en page 2 ou dont le client reconna�t avoir ant�rieurement eu connaissance, notamment lors de l'ouverture de compte.",
    "COinfo29p" => "la prolongation du contrat emporte acceptation sans r�serve des Conditions G�n�rales de location figurant en page 2 ou dont le client reconna�t avoir ant�rieurement eu connaissance, notamment lors de l'ouverture de compte.",

    "COpriseElec" => "Les chargeurs des chariots �lectriques sont livr�s sans prises. La mise en ad�quation de la prise au compteur de chantier est � la charge du locataire.",

    /* Page2 */
    "COinfo18" => "La prolongation ou l'arr�t de location devra �tre confirm� par notification �crite pour �tre pris en compte (art. 10 � Dur�e � des CGL).\n"
                . "L'arr�t ne peut �tre demand� avant la date d'expiration de la dur�e fix�e aux conditions particuli�res.\n"
                . "La demande de prorogation est toujours sous r�serve de son acceptation par Acces Industrie.",
    "COinfo19" => "PROLONGATION DU CONTRAT",
    "COinfo20" => "Veuillez prolonger le contrat ci-dessus jusqu'au\n__ / __ / ____ � __ h __.",
    "COinfo20bis" => "N� de contrat (si modifi�):____________________.",
    "COinfo21" => "FIN DE LOCATION",
    "COinfo22" => "Je confirme l'arr�t du contrat ci-dessus pour le\n__ / __ / ____ � __ h __.",
    "COinfo23" => "OBSERVATIONS : ",
    "COinfo24" => "Cachet de l'entreprise et signature",
    "COinfo25" => "Le __ / __ / ____"
);

require_once($tabParams['oldFrameworkPath'] . '/inc/fpdf_multicelltagfit.class.php');
$pdf = new FPDF_MULTICELLTAGFIT( 'P','mm','A4' );
$pdf->AliasNbPages();
$pdf->Open();

$tabInfos = (array)$tabParams['tabParams'];

foreach ($tabInfos as &$info)
{
    $info = (array)$info;
    foreach ($info as &$data)
    {
        $data = mb_convert_encoding($data, 'cp1252', 'utf8');
    }
}

// Documents PDF suppl�mentaires � ins�rer
$tabAdditionalDocuments = (array)$tabParams['tabAdditionalDocuments'];
foreach ($tabAdditionalDocuments as &$line)
{
    $line = (array)$line;
}

$pdf->SetMargins( 19, 9 );
$pdf->SetAutoPageBreak( false );

$nbContracts = count ($tabInfos);

for( $k = 0; $k < $nbContracts; $k++)
{
    // - Documents "avant"
    displayAdditionalDocuments($pdf, $tabAdditionalDocuments, 'before');

    // NOUVELLE PAGE
    $pdf->AddPage();
    setWatermark($pdf, $tabParams, $traduction);

    /***********************
    G�n�ration du pdf CONTRAT
    ************************/
    //D�finition des balises de style
    $pdf->SetStyle("ub7","arial","UB",7,"0,0,0");

    $pdf->SetStyle("pb","arial","B",10,"0,0,0");
    $pdf->SetStyle("pb2","arial","B",8,"0,0,0");
    $pdf->SetStyle("pb3","arial","B",9,"0,0,0");
    $pdf->SetStyle("pb4","arial","B",7,"0,0,0");
    $pdf->SetStyle("pb5","arial","B",11,"0,0,0");
    $pdf->SetStyle("pub8", "arial", "UB", 8, "0,0,0");

    $pdf->SetStyle("a1","arial","",10,"0,0,0");
    $pdf->SetStyle("a11","arial","B",14,"0,0,0");
    $pdf->SetStyle("a2","arial","",9,"0,0,0");
    $pdf->SetStyle("a3","arial","",7,"0,0,0");
    $pdf->SetStyle("a4","arial","",6,"0,0,0");
    $pdf->SetStyle("a5","arial","",8,"0,0,0");

    $pdf->SetStyle("Pi","arial","I",8,"0,0,0");



    //Block 1 (Logo + Titre)
    $bloc_x = 19;
    $bloc_y = 9;
    $pdf->Image($tabParams['logoImg'], $bloc_x, $bloc_y, 60);

    $pdf->SetXY( $bloc_x, $bloc_y + 22 );
    $pdf->MultiCellTag(0, 10, '<pb>' . $traduction['COnum']." </pb><a11>".$tabInfos[$k]['CONTRATCODE']."</a11>", 0, "L", 0);

    $pdf->SetXY( $bloc_x, $bloc_y + 29 );
    $pdf->SetFont('Arial','B',10);
    $pdf->Write(5, $traduction['COagent']);
    $pdf->SetFont('Arial','',10);
    $pdf->CellFitScale($bloc_x + 70 - $pdf->GetX(), 5, $tabInfos[$k]['PEPRENOM']." ".$tabInfos[$k]['PENOM'], 0, 0, "L");

    $pdf->SetXY( $bloc_x, $bloc_y + 33.5 );
    $pdf->MultiCellTag(69, 5, "<pb>".$traduction['COncom']."</pb> <a1>".$tabInfos[$k]['CONTRATNUMCOMM']."</a1>", 0, "L", 0);

    // cadre surle titre du document
    $pdf->Rect( $bloc_x +70, $bloc_y, 101, 12 );
    $pdf->SetXY( $bloc_x + 98, $bloc_y );
    $pdf->MultiCellTag( 100, 4, "<pb>".$traduction['COtitre1']."</pb>", 0, "L", 0 );

    $pdf->SetXY( $bloc_x + 79, $bloc_y + 3.5 );
    $pdf->MultiCellTag( 100, 4, "<pb2>".$traduction['COtitre2']."</pb2>", 0, "L", 0 );
    $pdf->SetXY( $bloc_x + 98.5, $bloc_y + 7 );
    $pdf->MultiCellTag( 100, 4, "<pub8>".$traduction['COtitre4']."</pub8>", 0, "L", 0 );


    $pdf->SetXY( $bloc_x + 70, $bloc_y + 12 );
    $pdf->MultiCellTag( 65, 4, "<pb>".$traduction['COagence']."</pb> <a1>".$tabInfos[$k]['AGLIBELLE']."</a1>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x + 70, $bloc_y + 20 );
    $chefag = ($tabInfos[$k]['AGRESPONSABLE'] == '') ? '' : $traduction['COchefag'].$tabInfos[$k]['AGRESPONSABLE'];
    $pdf->MultiCellTag(175, 4, "<a1>".$chefag."\n".$tabInfos[$k]['AGADRESSE']."\n".$tabInfos[$k]['AGCP']." ".$tabInfos[$k]['AGVILLE']."\n".$traduction['COtel'].( $traduction["NumPlateformeTel"] != "" ? $traduction["NumPlateformeTel"] : $tabInfos[$k]['AGTEL'] )."\n\n".$traduction['COle'].date( "d/m/Y" )."</a1>", 0, "L", 0);

    $pdf->SetXY( $bloc_x + 120, $bloc_y + 32 );
    $pdf->MultiCellTag( 45, 4, "<pb>".$traduction['COfax'].( $traduction['NumPlateformeFax'] != "" ? $traduction['NumPlateformeFax'] : $tabInfos[$k]['AGFAX'] )."</pb>", 0, 0, "L" );

    //Block 2 (Info client + Chantier)
    $bloc_x = 19;
    $bloc_y = 55;
    $pdf->SetXY( $bloc_x, $bloc_y + 1.5 );
    $pdf->Rect( $bloc_x, $bloc_y, 171, 20 );
    $pdf->MultiCellTag( 172, 3.5, "<pb3>".$traduction['COinfocli']."</pb3><a2>".$tabInfos[$k]['CLSTE']."</a2>\n\n\n\n<pb3>".$traduction['COcltel']."</pb3><a2> ".$tabInfos[$k]['CLTEL']."</a2> <pb3>".$traduction['COclfax']."</pb3><a2>".$tabInfos[$k]['CLFAX']."</a2>", 0, 1, "L" );
    if ($traduction['COclsiren'] != '')
    {
    	$pdf->SetXY( $bloc_x + 70, $bloc_y + 15.5 );
    	$pdf->MultiCellTag( 147, 3.5, "<pb4>".$traduction['COclsiren']."</pb4><a4>".$tabInfos[$k]['CLSIREN']."</a4>", 0, 1, "L" );
    }

    $pdf->SetXY( $bloc_x, $bloc_y + 4.7 );
    $pdf->CellFitScale(200, 3.5, $tabInfos[$k]['CLADRESSE']);
    $pdf->SetXY( $bloc_x, $bloc_y + 8.4 );
    $pdf->CellFitScale(200, 3.5, $tabInfos[$k]['CLADRESSE2']);
    $pdf->SetXY( $bloc_x, $bloc_y + 12 );
    $pdf->CellFitScale(200, 3.5, $tabInfos[$k]['VICLCP']." ".$tabInfos[$k]['VICLNOM']);

    $pdf->SetXY( $bloc_x, $bloc_y + 21.5 );
    $pdf->Rect( $bloc_x, $bloc_y + 20, 171, 23.5 );
    $pdf->SetXY( $bloc_x, $bloc_y + 21 );
    $pdf->MultiCellTag( 200, 3.5, "<pb3>".$traduction['COchantier']."</pb3> <a2>".$tabInfos[$k]['CHLIBELLE']."\n".$tabInfos[$k]['CHADRESSE']."</a2>", 0, 0, "L" );
    $pdf->MultiCellTag( 172, 3.5, "<a2>".$tabInfos[$k]['VICP']." ".$tabInfos[$k]['VINOM']."</a2>", 0, 1, "L" );

    $pdf->SetXY( $bloc_x, $bloc_y + 36 );
    $pdf->MultiCellTag( 150, 3.5, "<pb3>".$traduction['COrespcom']."</pb3> <a2>".$tabInfos[$k]['CONTRATCONTACTCOMM']." ".$tabInfos[$k]['CONTRATTELEPHONECOMM']."</a2>", 0, 0, "L" );
    $pdf->MultiCellTag( 150, 3.5, "<pb3>".$traduction['COrespchantier']."</pb3> <a2>".$tabInfos[$k]['CHCONTACT']." ".$tabInfos[$k]['CHTELEPHONE']."</a2>", 0, 0, "L" );


    //Block 3 (Information Machine / prix)
    $bloc_x = 19;
    $bloc_y = 102;

    $pdf->Rect( $bloc_x, $bloc_y, 171, 4.5 );
    $pdf->Rect( $bloc_x, $bloc_y + 4.5, 56.5, 6 );
    $pdf->Rect( $bloc_x + 56.5, $bloc_y + 4.5, 56.5, 6 );
    $pdf->Rect( $bloc_x + 113, $bloc_y + 4.5, 58, 6 );

    $pdf->Rect( $bloc_x, $bloc_y + 10.5, 56.5, 5 );
    $pdf->Rect( $bloc_x + 56.5, $bloc_y + 10.5, 56.5, 5 );
    $pdf->Rect( $bloc_x + 113, $bloc_y + 10.5, 58, 5 );

    $pdf->Rect( $bloc_x, $bloc_y + 15.5, 56.5, 7 );
    $pdf->Rect( $bloc_x + 56.5, $bloc_y + 15.5, 56.5, 7 );
    $pdf->Rect( $bloc_x + 113, $bloc_y + 15.5, 58, 7 );

    $pdf->Rect( $bloc_x, $bloc_y + 22.5, 56.5, 12 );
    $pdf->Rect( $bloc_x + 56.5, $bloc_y + 22.5, 56.5, 12 );
    $pdf->Rect( $bloc_x + 113, $bloc_y + 22.5, 58, 12 );


    $pdf->SetXY( $bloc_x, $bloc_y );
    $pdf->MultiCellTag( 60, 5, "<pb3>".$traduction['COdefmatlou�']."</pb3>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x + 63, $bloc_y );
    $pdf->MultiCellTag( 64, 5, "<pb3>".$traduction['COnparc'].$tabInfos[$k]['MANOPARC']."</pb3>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x + 118, $bloc_y );
    $pdf->MultiCellTag( 64, 5, "<pb3>".$traduction['COnserie'].$tabInfos[$k]['MANOSERIE']."</pb3>", 0, 1, "L" );

    $pdf->SetXY( $bloc_x , $bloc_y + 4.5 );
    $pdf->MultiCellTag( 56.5, 6, "<a3>".$traduction['COdesign'].": </a3>", 0, 0, "L" );
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->SetXY( $bloc_x + 15 , $bloc_y + 4.5 );
    $pdf->CellFitScale( 41.5, 6, $tabInfos[$k]['MOMADESIGNATION']);

    $pdf->SetXY( $bloc_x + 56.5, $bloc_y + 4.5 );
    $pdf->MultiCellTag( 56.5, 6, "<a3>".$traduction['COfamille'].": </a3>"."<pb3>".$tabInfos[$k]['FAMAMODELE']."</pb3>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x + 113 , $bloc_y + 4.5 );
    $pdf->MultiCellTag( 58, 6, "<a3>".$traduction['COmarque'].": </a3>"."<pb3>".$tabInfos[$k]['FOLIBELLE']."</pb3>", 0, 1, "L" );


    $pdf->SetXY( $bloc_x , $bloc_y + 11.5 );
    $pdf->MultiCellTag( 56.5, 3, "<a3>".$traduction['COenergie'].": </a3>"."<pb3>".$tabInfos[$k]['FAMAENERGIE']."</pb3>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x + 56.5, $bloc_y + 11.5 );
    $pdf->MultiCellTag( 56.5, 3, "<a3>".$traduction['COhaut'].": </a3>"."<pb3>".$tabInfos[$k]['FAMAELEVATION']."</pb3>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x + 113 , $bloc_y + 11.5 );
    $pdf->MultiCellTag( 58, 3, "<a3>".$traduction['COpoids'].": </a3>"."<pb3>".$tabInfos[$k]['MOMAPOIDS']."</pb3>", 0, 1, "L" );

    // Location par jour ou par mois
    if( $tabInfos[$k]['MOTARIFMSFR'] != 0 )
    {
    	$montanttype = $traduction['COlocmoht'];
    	$montant = round($tabInfos[$k]['MOTARIFMSFR']);
    	$prixnet = round($tabInfos[$k]['MOTARIFMSFR2']);
    }
    else
    {
    	$montanttype = $traduction['COlocjrht'];
    	$montant = round($tabInfos[$k]['MOPXJOURFR']);
    	$prixnet = round($tabInfos[$k]['MOPXJOURFR2']);
    }
    $pdf->SetXY( $bloc_x , $bloc_y + 16 );
    $pdf->MultiCellTag( 56.5, 3, "<a3>$montanttype</a3>\n<pb3>".$montant." ".$tabInfos[$k]['PAMONNAIE']."</pb3>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x + 56.5, $bloc_y + 16 );
    $pdf->MultiCellTag( 56.5, 3, "<a3>".$traduction['COtranspallerht']."</a3> \n<pb3>".$tabInfos[$k]['delivery.baseAmount']." ".$tabInfos[$k]['PAMONNAIE']."</pb3>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x + 113 , $bloc_y + 16 );
    $pdf->MultiCellTag( 58, 3, "<a3>".$traduction['COtranspretourht']."</a3> \n<pb3>".$tabInfos[$k]['recovery.baseAmount']." ".$tabInfos[$k]['PAMONNAIE']."</pb3>", 0, 1, "L" );


    $pdf->SetXY( $bloc_x, $bloc_y + 23.5 );
    $pdf->MultiCellTag( 56.5, 3, "<pb3>".$traduction['COprixnet']."</pb3><pb>".round($prixnet, 0)." ".$tabInfos[$k]['PAMONNAIE']."</pb>\n", 0, 1, "L" );
    if( $tabInfos[$k]['MOREMISEEX'] > 0 )
    	$pdf->MultiCellTag( 56.5, 3.5, "<a3>".$traduction['COremex']."</a3>"."<pb3>".round($tabInfos[$k]['MOREMISEEX'], 2)."%</pb3>\n<a2>".$tabInfos[$k]['ALLIBELLE']."</a2>\n", 0, 1, "L" );

    $pdf->SetXY( $bloc_x + 56.5, $bloc_y + 23.5 );
    $pdf->MultiCellTag( 56.5, 3, "<a5><pb3>" . $traduction['COtariftra'] . round($tabInfos[$k]['delivery.amount']) ." ".$tabInfos[$k]['PAMONNAIE'] . "</pb3></a5>\n", 0, 1, "L" );
    if( $tabInfos[$k]['delivery.specialReductionPercent'] > 0 )
    {
        $pdf->SetX( $bloc_x + 56.5 );
        $pdf->MultiCellTag( 56.5, 3.5, "<a5>" . $traduction['COremex'] . '<pb3>' . round($tabInfos[$k]['delivery.specialReductionPercent']) . "%</pb3>\n"
                                 . $tabInfos[$k]['delivery.specialReductionBillLabel'] . "</a5>", 0, 1, "L" );
    }

    $pdf->SetXY( $bloc_x + 113, $bloc_y + 23.5 );
    $pdf->MultiCellTag( 56.5, 3, "<a5><pb3>" . $traduction['COtariftrr'] . round($tabInfos[$k]['recovery.amount']) ." ".$tabInfos[$k]['PAMONNAIE'] . "</pb3></a5>\n", 0, 1, "L" );
    if( $tabInfos[$k]['recovery.specialReductionPercent'] > 0 )
    {
        $pdf->SetX( $bloc_x + 56.5 + 56.5);
        $pdf->MultiCellTag( 56.5, 3.5, "<a5>" . $traduction['COremex'] . '<pb3>' . round($tabInfos[$k]['recovery.specialReductionPercent']) . "%</pb3>\n"
        . $tabInfos[$k]['recovery.specialReductionBillLabel'] . "</a5>", 0, 1, "L" );
    }

    ////////////////////////////////////////////////////
    // Assurance

    $pdf->Rect( $bloc_x, $bloc_y + 34.5, 171, 25);
    $pdf->SetXY( $bloc_x, $bloc_y + 35.5 );
    $pdf->MultiCellTag( 172, 3, "<pb2>" . $tabInfos[$k]['insurance.label'] . ($tabInfos[$k]['isInsuranceTypeInvoiced'] ? '. ' . $traduction['COinfocl1'] : '') . "</pb2>", 0, "L" );

    // Gestion de recours
    if ($tabInfos[$k]['isAppealTypeInvoiced'])
    {
        $appealLabel = $traduction['COappealLabel'];
        $appealLabel = str_replace("%appealLabel%", $tabInfos[$k]['appeal.label'], $appealLabel);
        $pdf->MultiCellTag( 172, 3.5, "<pb2>" . $appealLabel . "</pb2>", 0, "L" );
    }

    // Nettoyage non factur�
    $isCleaningInvoiced = ($tabInfos[$k]['cleaning.type.id'] != 0);
    // Participation au recyclage non factur�
    $isResiduesInvoiced = ($tabInfos[$k]['residues.mode'] != 0);
    $residuesMode  = 0;
    $residuesValue = null;
    if ($isResiduesInvoiced)
    {
        $residuesMode  = (int)$tabInfos[$k]['residues.mode'];
        $residuesValue = $tabInfos[$k]['residues.value'];
    }
    // Mode de participation au recyclage
    $suffix = '';
    if ($residuesMode == 1)
    {
        $residuesLabel = number_format($residuesValue, 2) . ' �';
        $suffix = '-F';
    }
    elseif ($residuesMode == 2)
    {
        $residuesLabel = $residuesValue * 100 . '% de la location';
        $suffix = '-P';
    }

    $label = '';
    // Nettoyage et participation non factur�s
    if (!$isCleaningInvoiced && !$isResiduesInvoiced)
    {
        $label = $traduction['COinfo2-0'];
    }
    // Uniquement le nettoyage factur�
    elseif (!$isResiduesInvoiced)
    {
        $label = $traduction['COinfo2-1'];
    }
    // Uniquement la participation au recyclage factur�
    elseif (!$isCleaningInvoiced)
    {
        $label = $traduction['COinfo2-2' . $suffix] . $traduction['COinfo2-3'];
        $label = str_replace("%residuesLabel%", $residuesLabel, $label);
    }
    // Nettoyage et participation au recyclage factur�s
    else
    {
        $label = $traduction['COinfo2' . $suffix] . $traduction['COinfo2-3'];
        $label = str_replace("%residuesLabel%", $residuesLabel, $label);
    }

    $pdf->MultiCellTag( 172, 3.5, "<pb3>" . $label . "</pb3>", 0, "L" );

    if ($tabInfos[$k]['FAMAENERGIE'] == "ELECT" & $tabInfos[$k]['fly_id'] == 12)//condition FR, CHARIOT IND/FRONT et elect
    {
    	$pdf->MultiCellTag( 172, 3.5, "<pb3>".$traduction['COpriseElec']."</pb3>", 0, "L" );
    }

    // Bloc 4 Condition g�n�rale de location
    $bloc_x = 19;
    $bloc_y = 165;

    $pdf->Rect( $bloc_x, $bloc_y, 171, 50.5 );
    $pdf->SetXY( $bloc_x, $bloc_y + 1 );
    $pdf->MultiCellTag( 150, 3.5, "<pb2>".$traduction['COinfo3']."</pb2> <pb>".$tabInfos[$k]['MOPALIBELLE']."</pb>", 0, 0, "L" );

    // Conditions g�n�rales
    $pdf->SetXY( $bloc_x, $bloc_y + 9 );
    $text = '<ub7>' . mb_strtoupper($traduction['COinfo28']) . '</ub7><a5>';
    if ($traduction['COinfo28'] != '')
    {
        $text .= ' ';
    }
    $text .= $traduction['COinfo29'] . '</a5>';
    $pdf->MultiCellTag( 171, 3, $text, 0, "L" );

    $pdf->SetXY( $bloc_x, $bloc_y + 17 );
    $text = "<pb2>".$traduction['COinfo5']."</pb2> <pi>".$traduction['COinfo6']."</pi>\n";
    if ($tabInfos[$k]['CLPOTENTIEL'] == 'B' || $tabInfos[$k]['CLPOTENTIEL'] == 'C')
    {
        $text .= "<pi>".$traduction['COinfo6bis']."</pi>\n";
    }

    $text .= "<pi>".$traduction['COinfo7']."</pi>";
    $pdf->MultiCellTag( 170, 3.5, $text, 0, 1, "L" );

    // Tribunal comp�tent
    $pdf->SetXY( $bloc_x, $bloc_y + 43 );
    $text = '<ub7>' . mb_strtoupper($traduction['COinfo26']) . '</ub7><pb2>';
    if ($traduction['COinfo26'] != '')
    {
        $text .= ' ';
    }
    $text .= $traduction['COinfo27'] . '</pb2>';
    $pdf->MultiCellTag( 171, 2.5, $text, 0, "L" );


    //bloc date
    $pdf->Rect( $bloc_x, $bloc_y + 51.5, 85, 5.5 );
    $pdf->Rect( $bloc_x + 85, $bloc_y + 51.5, 86, 5.5 );
    $pdf->Rect( $bloc_x, $bloc_y + 57, 85, 6 );
    $pdf->Rect( $bloc_x + 85, $bloc_y + 57, 86, 6 );
    if (isset($tabInfos[$k]['startCode.code']))
    {
        $pdf->Rect( $bloc_x, $bloc_y + 63, 85, 6 );
        $pdf->Rect( $bloc_x, $bloc_y + 69, 85, 12 );
    }
    else
    {
        $pdf->Rect( $bloc_x, $bloc_y + 63, 85, 18 );
    }
    $pdf->Rect( $bloc_x + 85, $bloc_y + 63, 86, 18 );

    $pdf->SetXY( $bloc_x, $bloc_y + 52 );
    $pdf->MultiCellTag( 100, 3.5, "<a2>".$traduction['COdatedep']."</a2> <pb3>".date('d-m-Y', strtotime($tabInfos[$k]['CONTRATDD']))."</pb3>" , 0, 0, "L" );

    $pdf->SetXY( $bloc_x + 85, $bloc_y + 52 );
    $pdf->MultiCellTag( 100, 3.5, "<a2>".$traduction['COdateret']."</a2> <pb3>".date('d-m-Y', strtotime($tabInfos[$k]['CONTRATDR']))."</pb3>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x, $bloc_y + 58 );
    $pdf->MultiCellTag( 85, 3.5, "<a2>".$traduction['COjourplus'].($tabInfos[$k]['CONTRATJOURSPLUS'] * 1)."</a2>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x + 85, $bloc_y + 58 );
    $pdf->MultiCellTag( 85, 3.5, "<a2>".$traduction['COjourmoins'].($tabInfos[$k]['CONTRATJOURSMOINS'] * 1)."</a2>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x, $bloc_y + 64 );
    $pdf->MultiCellTag( 85, 3.5, "<a2>".$traduction['COduree'].($tabInfos[$k]['CONTRATDUREE'] * 1)."</a2>", 0, 0, "L" );

    if (isset($tabInfos[$k]['startCode.code']))
    {
        $pdf->SetXY( $bloc_x, $bloc_y + 70 );
        $pdf->MultiCellTag( 85, 3.5, "<a2>".$traduction['COdigicode'].$tabInfos[$k]['startCode.code']."</a2>", 0, 0, "L" );
        $pdf->SetXY( $bloc_x, $bloc_y + 74.5 );
        $text = "<pb2>".$traduction['COinfo5']."</pb2> <pi>".$traduction['COinfo30']."</pi>\n";
        $pdf->MultiCellTag( 85, 3, $text, 0, 1, "L" );
    }

    $pdf->SetXY( $bloc_x + 85, $bloc_y + 63.5 );
    $pdf->MultiCellTag( 85, 3.5, "<a2>".$traduction['COmmentaire']."</a2>", 0, 1, "L" );

    $pdf->SetXY( $bloc_x + 85, $bloc_y + 67 );
    $pdf->MultiCellTag( 85, 3, "<a3>".$tabInfos[$k]['CONTRATCOMMENTAIRE']."</a3>", 0, 0, "L" );


    //bloc signature
    $pdf->SetXY( $bloc_x, $bloc_y + 81.5 );
    $pdf->MultiCellTag( 170, 3.5, "<pb4>".$traduction['COinfo11']."</pb4><a3> ".$traduction['COinfo17']."</a3>", 0, 0, "L" );


    $pdf->SetXY( $bloc_x , $bloc_y + 85 );
    $pdf->MultiCellTag( 85, 5.5, "<pb4>".$traduction['COinfo12']."</pb4>", 1, 0, "C" );

    $pdf->SetXY( $bloc_x + 85, $bloc_y + 85 );
    $pdf->MultiCellTag( 86, 5.5, "<pb4>".$traduction['COinfo13']."</pb4>", 1, 0, "C" );


    $pdf->Rect( $bloc_x, $bloc_y + 90.5, 85, 25 );
    $pdf->Rect( $bloc_x + 85, $bloc_y + 90.5, 86, 25 );

    $pdf->SetXY( $bloc_x, $bloc_y + 93 );
    $pdf->MultiCellTag( 60, 3.5, "<a2>".$traduction['COLe']."</a2>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x, $bloc_y + 118 );
    $pdf->MultiCellTag( 175, 3.5, "<pb3>".$traduction['COinfo14']."</pb3>", 0, 0, "L" );

    // Phrase juridique Acces industrie
    $pdf->SetXY( $bloc_x, $bloc_y + 122 );
    $pdf->SetFont('Arial','',7);
    $pdf->CellFitScale( 171, 3, str_replace( "%capital_acces%", mb_convert_encoding($tabParams['capital'], 'cp1252', 'utf8'), $traduction['COinfo16'] ), 0, 0, "C" );



    // NOUVELLE PAGE //////////////////////////////////////////////////////////////////
    $pdf->AddPage();
    setWatermark($pdf, $tabParams, $traduction);


    //Block 1 (Logo + Titre)
    $bloc_x = 19;
    $bloc_y = 9;

    $pdf->Image($tabParams['logoImg'], $bloc_x, $bloc_y, 60);

    $pdf->SetXY( $bloc_x, $bloc_y + 22 );
    $pdf->MultiCellTag(0, 10, "<pb>".$traduction['COnum']." </pb><a11>".$tabInfos[$k]['CONTRATCODE']."</a11>", 0, "L", 0);

    $pdf->SetXY( $bloc_x, $bloc_y + 29 );
    $pdf->SetFont('Arial','B',10);
    $pdf->Write(5, $traduction['COagent']);
    $pdf->SetFont('Arial','',10);
    $pdf->CellFitScale($bloc_x + 70 - $pdf->GetX(), 5, $tabInfos[$k]['PEPRENOM']." ".$tabInfos[$k]['PENOM'], 0, 0, "L");

    $pdf->SetXY( $bloc_x, $bloc_y + 33.5 );
    $pdf->MultiCellTag(45, 5, "<pb>".$traduction['COncom']."</pb> <a1>".$tabInfos[$k]['CONTRATNUMCOMM']."</a1>", 0, "L", 0);

    $pdf->SetXY( $bloc_x + 98, $bloc_y );
    $pdf->MultiCellTag( 100, 4, "<pb>".$traduction['COtitre1']."</pb>", 0, "L", 0 );
    $pdf->SetXY( $bloc_x + 79, $bloc_y + 3.5 );
    $pdf->MultiCellTag( 100, 4, "<pb2>".$traduction['COtitre2']."</pb2>", 0, "L", 0 );
    $pdf->SetXY( $bloc_x + 98.5, $bloc_y + 7 );
    $pdf->MultiCellTag( 100, 4, "<pb2>".$traduction['COtitre5']."</pb2>", 0, "L", 0 );

    $pdf->SetXY( $bloc_x + 70, $bloc_y +12 );
    $pdf->MultiCellTag( 100, 4, "<pb>".$traduction['COagence']."</pb> <a1>".$tabInfos[$k]['AGLIBELLE']."</a1>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x + 70, $bloc_y + 20 );
    $pdf->MultiCellTag(0, 4, "<a1>".$chefag."\n".$tabInfos[$k]['AGADRESSE']."\n".$tabInfos[$k]['AGCP']." ".$tabInfos[$k]['AGVILLE']."\n".$traduction['COtel'].( $traduction['NumPlateformeTel'] != "" ? $traduction['NumPlateformeTel'] : $tabInfos[$k]['AGTEL'] )."\n\n".$traduction['COle'].date( "d/m/Y" )."</a1>", 0, "L", 0);

    $pdf->SetXY( $bloc_x + 120, $bloc_y + 32 );
    $pdf->MultiCellTag( 45, 4, "<pb>".$traduction['COfax'].( $traduction['NumPlateformeFax'] != "" ? $traduction['NumPlateformeFax'] : $tabInfos[$k]['AGFAX'] )."</pb>", 0, 0, "L" );

    //Block 2 (Info client + Chantier)
    $bloc_x = 19;
    $bloc_y = 55;
    $pdf->SetXY( $bloc_x, $bloc_y + 1.5 );
    $pdf->Rect( $bloc_x, $bloc_y, 171, 20 );
    $pdf->MultiCellTag( 172, 3.5, "<pb3>".$traduction['COinfocli']."</pb3><a2>".$tabInfos[$k]['CLSTE']."</a2>\n\n\n\n<pb3>".$traduction['COcltel']."</pb3><a2> ".$tabInfos[$k]['CLTEL']."</a2> <pb3>".$traduction['COclfax']."</pb3><a2>".$tabInfos[$k]['CLFAX']."</a2>", 0, 1, "L" );

    $pdf->SetXY( $bloc_x, $bloc_y + 4.7 );
    $pdf->CellFitScale(200, 3.5, $tabInfos[$k]['CLADRESSE']);
    $pdf->SetXY( $bloc_x, $bloc_y + 8.4 );
    $pdf->CellFitScale(200, 3.5, $tabInfos[$k]['CLADRESSE2']);
    $pdf->SetXY( $bloc_x, $bloc_y + 12 );
    $pdf->CellFitScale(200, 3.5, $tabInfos[$k]['VICLCP']." ".$tabInfos[$k]['VICLNOM']);

    $pdf->SetXY( $bloc_x, $bloc_y + 21.5 );
    $pdf->Rect( $bloc_x, $bloc_y + 20, 171, 23.5 );
    $pdf->SetXY( $bloc_x, $bloc_y + 21 );
    $pdf->MultiCellTag( 172, 3.5, "<pb3>".$traduction['COchantier']."</pb3> <a2>".$tabInfos[$k]['CHLIBELLE']."\n".$tabInfos[$k]['CHADRESSE']."</a2>", 0, 0, "L" );
    $pdf->MultiCellTag( 172, 3.5, "<a2>".$tabInfos[$k]['VICP']." ".$tabInfos[$k]['VINOM']."</a2>", 0, 1, "L" );

    $pdf->SetXY( $bloc_x, $bloc_y + 36 );
    $pdf->MultiCellTag( 150, 3.5, "<pb3>".$traduction['COrespcom']."</pb3> <a2>".$tabInfos[$k]['CONTRATCONTACTCOMM']." ".$tabInfos[$k]['CONTRATTELEPHONECOMM']."</a2>", 0, 0, "L" );
    $pdf->MultiCellTag( 150, 3.5, "<pb3>".$traduction['COrespchantier']."</pb3> <a2>".$tabInfos[$k]['CHCONTACT']." ".$tabInfos[$k]['CHTELEPHONE']."</a2>", 0, 0, "L" );


    //Block 3 (Information Machine / prix)
    $bloc_x = 19;
    $bloc_y = 100;

    $pdf->Rect( $bloc_x, $bloc_y, 171, 4.5 );
    $pdf->Rect( $bloc_x, $bloc_y + 4.5, 56.5, 6 );
    $pdf->Rect( $bloc_x + 56.5, $bloc_y + 4.5, 56.5, 6 );
    $pdf->Rect( $bloc_x + 113, $bloc_y + 4.5, 58, 6 );

    $pdf->Rect( $bloc_x, $bloc_y + 10.5, 56.5, 5 );
    $pdf->Rect( $bloc_x + 56.5, $bloc_y + 10.5, 56.5, 5 );
    $pdf->Rect( $bloc_x + 113, $bloc_y + 10.5, 58, 5 );

    $pdf->SetXY( $bloc_x, $bloc_y );
    $pdf->MultiCellTag( 60, 5, "<pb3>".$traduction['COdefmatlou�']."</pb3>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x + 63, $bloc_y );
    $pdf->MultiCellTag( 64, 5, "<pb3>".$traduction['COnparc'].$tabInfos[$k]['MANOPARC']."</pb3>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x + 118, $bloc_y );
    $pdf->MultiCellTag( 64, 5, "<pb3>".$traduction['COnserie'].$tabInfos[$k]['MANOSERIE']."</pb3>", 0, 1, "L" );

    $pdf->SetXY( $bloc_x , $bloc_y + 4.5 );
    $pdf->MultiCellTag( 56.5, 6, "<a3>".$traduction['COdesign'].": </a3>"."<pb3>".$tabInfos[$k]['MOMADESIGNATION']."</pb3>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x + 56.5, $bloc_y + 4.5 );
    $pdf->MultiCellTag( 56.5, 6, "<a3>".$traduction['COfamille'].": </a3>"."<pb3>".$tabInfos[$k]['FAMAMODELE']."</pb3>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x + 113 , $bloc_y + 4.5 );
    $pdf->MultiCellTag( 58, 6, "<a3>".$traduction['COmarque'].": </a3>"."<pb3>".$tabInfos[$k]['FOLIBELLE']."</pb3>", 0, 1, "L" );


    $pdf->SetXY( $bloc_x , $bloc_y + 11.5 );
    $pdf->MultiCellTag( 56.5, 3, "<a3>".$traduction['COenergie'].": </a3>"."<pb3>".$tabInfos[$k]['FAMAENERGIE']."</pb3>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x + 56.5, $bloc_y + 11.5 );
    $pdf->MultiCellTag( 56.5, 3, "<a3>".$traduction['COhaut'].": </a3>"."<pb3>".$tabInfos[$k]['FAMAELEVATION']."</pb3>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x + 113 , $bloc_y + 11.5 );
    $pdf->MultiCellTag( 58, 3, "<a3>".$traduction['COpoids'].": </a3>"."<pb3>".$tabInfos[$k]['MOMAPOIDS']."</pb3>", 0, 1, "L" );


    // Bloc 4 Condition g�n�rale de location
    $bloc_x = 19;
    $bloc_y = 117;

    $pdf->Rect( $bloc_x, $bloc_y, 171, 50.5 );
    $pdf->SetXY( $bloc_x, $bloc_y + 1 );
    $pdf->MultiCellTag( 150, 3.5, "<pb2>".$traduction['COinfo3']."</pb2> <pb>".$tabInfos[$k]['MOPALIBELLE']."</pb>", 0, 0, "L" );

    // Conditions g�n�rales
    $pdf->SetXY( $bloc_x, $bloc_y + 9 );
    $text = '<ub7>' . mb_strtoupper($traduction['COinfo28']) . '</ub7><a5>';
    if ($traduction['COinfo28'] != '')
    {
        $text .= ' ';
    }
    $text .= $traduction['COinfo29p'] . '</a5>';
    $pdf->MultiCellTag( 171, 3, $text, 0, "L" );

    $pdf->SetXY( $bloc_x, $bloc_y + 17 );
    $text = "<pb2>".$traduction['COinfo5']."</pb2>\n";
    $text .= "<pi>".$traduction['COinfo7']."</pi>";
    $pdf->MultiCellTag( 170, 3.5, $text, 0, 1, "L" );

    // Tribunal comp�tent
    $pdf->SetXY( $bloc_x, $bloc_y + 43 );
    $text = '<ub7>' . mb_strtoupper($traduction['COinfo26']) . '</ub7><pb2>';
    if ($traduction['COinfo26'] != '')
    {
        $text .= ' ';
    }
    $text .= $traduction['COinfo27'] . '</pb2>';
    $pdf->MultiCellTag( 171, 2.5, $text, 0, "L" );

    // signature
    $bloc_x = 19;
    $bloc_y = 169;

    $pdf->Rect( $bloc_x, $bloc_y, 85, 6 );
    $pdf->SetXY( $bloc_x, $bloc_y + 1 );
    $pdf->MultiCellTag( 100, 3.5, "<a2>".$traduction['COdatedep']."</a2> <pb3>".date('d-m-Y', strtotime($tabInfos[$k]['CONTRATDD']))."</pb3>", 0, 0, "L" );

    $pdf->Rect( $bloc_x + 85, $bloc_y, 87, 6 );
    $pdf->SetXY( $bloc_x + 85, $bloc_y + 1 );
    $pdf->MultiCellTag( 100, 3.5, "<a2>".$traduction['COdateret']."</a2> <pb3>".date('d-m-Y', strtotime($tabInfos[$k]['CONTRATDR']))."</pb3>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x, $bloc_y + 8 );
    $pdf->MultiCellTag(172, 3.5, "<a1>".$traduction['COinfo18']."</a1>", 0, 0, "C" );

    $pdf->Rect( $bloc_x, $bloc_y + 31, 82.5, 35 );
    $pdf->Rect( $bloc_x + 87.5, $bloc_y + 31, 84, 35 );

    $pdf->SetXY( $bloc_x + 7, $bloc_y + 33 );
    $pdf->MultiCellTag(82.5, 4, "<pb5>".$traduction['COinfo19']."</pb5>", 0, 0, "C" );

    $pdf->SetXY( $bloc_x , $bloc_y + 42 );
    $pdf->MultiCellTag(80, 6, "<a1>".$traduction['COinfo20']."</a1>", 0, "C" );

    $pdf->SetXY( $bloc_x , $bloc_y + 58 );
    $pdf->MultiCellTag(120, 6, "<a1>".$traduction['COinfo20bis']."</a1>", 0, "L" );

    $pdf->SetXY( $bloc_x + 110, $bloc_y + 33 );
    $pdf->MultiCellTag(80, 4, "<pb5>".$traduction['COinfo21']."</pb5>", 0, 0, "C" );

    $pdf->SetXY( $bloc_x + 87, $bloc_y + 49 );
    $pdf->MultiCellTag(84, 6, "<a1>".$traduction['COinfo22']."</a1>", 0, "C" );


    $pdf->Rect( $bloc_x, $bloc_y + 70, 172, 20 );
    $pdf->SetXY( $bloc_x , $bloc_y + 71 );
    $pdf->MultiCellTag(82.5, 4, "<pb2>".$traduction['COinfo23']."</pb2>", 0, 0, "L" );

    $pdf->SetXY( $bloc_x, $bloc_y + 95 );
    $pdf->MultiCellTag(84, 5.5, "<pb>".$traduction['COinfo24']."</pb>", 0, 1, "L" );

    $pdf->SetXY( $bloc_x, $bloc_y + 100);
    $pdf->MultiCellTag(84, 4, "<a1>".$traduction['COinfo25']."</a1>", 0, 0, "L" );


    $pdf->SetXY( $bloc_x, $bloc_y + 113 );
    $pdf->SetFont('Arial','',7);
    $pdf->CellFitScale( 171, 3, str_replace( "%capital_acces%", mb_convert_encoding($tabParams['capital'], 'cp1252', 'utf8'), $traduction['COinfo16'] ), 0, 0, "C" );


    // - Documents "apr�s"
    displayAdditionalDocuments($pdf, $tabAdditionalDocuments, 'after');
}


$pdf->Output( "document.pdf", "I" );

/**
 * Affiche des documents suppl�mentaires
 * @param $pdf
 * @param array $tabAdditionalDocuments - Tableau des documents
 * @param string $zone                  - Zone d'affichage
 */
function displayAdditionalDocuments(&$pdf, $tabAdditionalDocuments, $zone = 'before')
{
    if (array_key_exists($zone, $tabAdditionalDocuments))
    {
        foreach ($tabAdditionalDocuments[$zone] as $documentInfos)
        {
            $documentInfos = (array)$documentInfos;
            if ($documentInfos['active'] && file_exists($documentInfos['path']))
            {
                $pageCount = $pdf->setSourceFile($documentInfos['path']);
                for ($i = 1; $i <= $pageCount; $i++)
                {
                    $pdf->addPage();
                    // importe une page
                    $templateId = $pdf->importPage($i);
                    // utilise la page
                    $pdf->useTemplate($templateId);
                }
            }
        }
    }
}

function setWatermark(&$pdf, $tabParams, $traduction)
{
    // global $varglob, $traduction;

    if ($tabParams['env'] != 'exp')
    {
        $pdf->SetFont('Arial', 'BI', 72);

        $textWatermark = mb_strtoupper ($traduction['Specimen']);
        $xWatermark = $pdf->w / 2;
        $yWatermark = $pdf->h / 2;
        $rWatermark = 30;

        $rWatermark *= M_PI / 180;
        $cosWatermark = cos($rWatermark);
        $sinWatermark = sin($rWatermark);
        $cx = $xWatermark * $pdf->k;
        $cy = ($pdf->h - $yWatermark) * $pdf->k;
        $cx = 0;
        $cy = 0;

        $pdf->_out(
        sprintf('q %.3f g %.5f %.5f %.5f %.5f %.2f %.2f cm',
        203 / 255,
        $cosWatermark,
        $sinWatermark,
        -$sinWatermark,
        $cosWatermark,
        ($pdf->w / 2 - $pdf->GetStringWidth($textWatermark) / 2 * cos($rWatermark)) * $pdf->k,
        $pdf->h / 2 * $pdf->k - $pdf->GetStringWidth($textWatermark) / 2 * sin($rWatermark) * $pdf->k
        )
        );
        $pdf->_out(
        sprintf('BT (%s) Tj ET',
        $pdf->_escape($textWatermark)
        )
        );

        $pdf->_out('Q');
    }
}

?>