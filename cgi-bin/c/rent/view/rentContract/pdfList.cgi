use utf8;
use open (':encoding(UTF-8)');
binmode STDOUT, ":raw";    # Pour envoyer le flux en l'état

use strict;
use locale;

use LOC::Date;
use LOC::Locale;
use LOC::Pdf;
use LOC::Util;
use LOC::Browser;

use constant
{
    APPEALCODE             => 'GDR',
    APPEALCODE_NOTINVOICED => 'GDR NF'
};

our %tabViewData;

# Locale
my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});

my $tabLabels = {
    APPEALCODE()             => $locale->t('Gestion de recours'),
    APPEALCODE_NOTINVOICED() => $locale->t('Gestion de recours non facturée')
};


# Titre de la page
my @tabLocaltime = localtime();
my $title = $locale->t('Historique des contrats');

# Création de l'objet PDF
my $pdf = LOC::Pdf->new($title, $tabViewData{'user.agency.id'}, \$locale, LOC::Pdf::ORIENTATION_LANDSCAPE);
my $pdfObj = $pdf->getPdfObject();

# Dimensions de la page
my $margin = 10/mm;
my $pageWidth = $pdf->getPageWidth();
my $textWidth = $pageWidth - 2 * $margin;

my $pageHeight = $pdf->getPageHeight();
my $textHeight = $pageHeight - 2 * $margin;
my $pageLimit = $margin;

my $gap = 2/pt;


# Polices
my $regularFont    = $pdf->{'font'}->{'regular'};
my $italicFont     = $pdf->{'font'}->{'italic'};
my $boldFont       = $pdf->{'font'}->{'bold'};
my $boldItalicFont = $pdf->{'font'}->{'bolditalic'};
my $dingbatsFont   = $pdfObj->corefont('ZapfDingbats');


# Définition du sujet et des mots-clés
$pdf->setSubject($locale->t('Historique des contrats'));
$pdf->setKeywords($locale->t('Historique des contrats'));



# Nouvelle page
my $page = $pdf->addPage(LOC::Pdf::DISPLAY_TITLE | LOC::Pdf::DISPLAY_AGENCY | LOC::Pdf::DISPLAY_FULLFOOTER);
my $text = $page->text();
my $gfx  = $page->gfx();


# Etat et date de sortie
my $number = $locale->t('au %s', $locale->getDateFormat(undef, LOC::Locale::FORMAT_DATE_NUMERIC));
$gfx->textlabel($pageWidth - 10/mm - 95/mm, $pageHeight - 35/mm - 16/pt * 1.2, $boldFont, 10/pt, $number);


# Références
my $phoneNumber = $locale->getPhoneNumberFormat($tabViewData{'tabCustomerInfos'}->{'telephone'});
my $faxNumber   = $locale->getPhoneNumberFormat($tabViewData{'tabCustomerInfos'}->{'fax'});
my @tabReferences = (
    {'label' => $locale->t('Code client'), 'value' => $tabViewData{'tabCustomerInfos'}->{'code'}},
    {'label' => $locale->t('Téléphone'), 'value' => $phoneNumber},
    {'label' => $locale->t('Fax'), 'value' => $faxNumber}
);
$pdf->addCustomBlock(\$page, $locale->t('Références'), \@tabReferences, {'x' => 10/mm, 'y' => 55/mm});

# Coordonnées client
my $tabCustomer = [
    $tabViewData{'tabCustomerInfos'}->{'name'},
    $tabViewData{'tabCustomerInfos'}->{'address1'},
    $tabViewData{'tabCustomerInfos'}->{'address2'},
    ' ',
    $tabViewData{'tabCustomerInfos'}->{'locality.postalCode'} . ' ' . $tabViewData{'tabCustomerInfos'}->{'locality.name'},
];
$pdf->addCustomerBlock(\$page, $tabCustomer);

my $firstPage = $page;

my $x = $margin;
my $y = $pageHeight - 95/mm;

tie(my %tabColsList, 'Tie::IxHash');
%tabColsList = (
    'code'               => {'label' => $locale->t('N° Contrat'),         'width' => 27/mm},
    'model.label'        => {'label' => $locale->t('Modèle de machines'), 'width' => 35/mm},
    'machine.parkNumber' => {'label' => $locale->t('N° parc'),            'width' => 12/mm},
    'site'               => {'label' => $locale->t('Chantier'),           'width' => 56/mm},
    'beginDate'          => {'label' => $locale->t('Date début'),         'width' => 18/mm},
    'endDate'            => {'label' => $locale->t('Date fin'),           'width' => 18/mm},
    'duration'           => {'label' => $locale->t('Durée (j)'),          'width' => 13/mm},
    'tariff'             => {'label' => $locale->t('Location'),           'width' => 23/mm},
    'insurance'          => {'label' => $locale->t('Assurance'),          'width' => 16/mm},
    'deliveryAmount'     => {'label' => $locale->t('Livraison'),          'width' => 17/mm},
    'recoveryAmount'     => {'label' => $locale->t('Récupération'),       'width' => 17/mm},
    'state.label'        => {'label' => $locale->t('État'),               'width' => 25/mm}
);
# Affichage de l'entête du tableau
&printHeaderTabContent();

my $tabInsuranceTypes = {};
# Affichage des lignes de devis
foreach my $tabContractInfos (@{$tabViewData{'tabContractsList'}})
{
    $y = &displayTableLine($y, $tabContractInfos);
    &checkRemainingSpace($y - $pageLimit - 5/mm, 'tabContent');
}

# Ajout d'une légende pour les types d'assurances
my @tabInsuranceTypesLegend;
# - Tri par légende
my @tabInsuranceTypesKeys = sort {$tabInsuranceTypes->{$a} <=> $tabInsuranceTypes->{$b}} keys(%$tabInsuranceTypes);
foreach my $insuranceTypeId (@tabInsuranceTypesKeys)
{
    my $value = $tabViewData{'tabInsuranceTypesList'}->{$insuranceTypeId}->{'label'};
    $value =~ s/<%percent>//g;

    push(@tabInsuranceTypesLegend, {
        'label' => $locale->t('%s', $tabViewData{'tabInsuranceTypesList'}->{$insuranceTypeId}->{'code'}),
        'value' => $value
    });
}
# - Gestion de recours
my @tabAppealTypes;

my $tabAppealTypes = &LOC::Util::array_unique(\@tabAppealTypes);
foreach my $appealCode (@$tabAppealTypes)
{
    push(@tabInsuranceTypesLegend, {
        'label' => $appealCode,
        'value' => $tabLabels->{$appealCode}
    });
}


$pdf->addCustomBlock(\$firstPage, $locale->t('Assurance'), \@tabInsuranceTypesLegend, {'x' => 10/mm, 'y' => 75/mm});


$x = $margin;
$y -= 15/mm;
$text->translate($x, $y);
$text->text($locale->tn('Résultat %s sur %s',
                       'Résultats %s à %s sur %s', $tabViewData{'tabResultInfos'}->{'total'},
                            $tabViewData{'tabResultInfos'}->{'from'},
                            $tabViewData{'tabResultInfos'}->{'to'},
                            $tabViewData{'tabResultInfos'}->{'total'}));

# Numéros de page
&displayPageNumbers();
#
# Sortie
my $pdfString = $pdf->getString();
$pdf->end();

my $filename = sprintf('HistoriqueClient_%s_%04d-%02d-%02d.pdf', $tabViewData{'tabCustomerInfos'}->{'code'},
                                                                 $tabLocaltime[5] + 1900,
                                                                 $tabLocaltime[4] + 1,
                                                                 $tabLocaltime[3]);
&LOC::Browser::sendHeaders(
        "Content-type: application/pdf;\n"
        . "Content-Disposition: inline; filename=\"" . $filename . "\";\n\n"
    );

print $pdfString;


# Function: checkRemainingSpace
# Vérification de l'espace restant dans la page
#
# Parameters:
# string $value - Ordonnée ($y par défaut)
# string $origin - si tabContent, on répète l'entete du tableau des désignations
#
# Returns:
# bool - 1 si l'espace restant est suffisant, 0 sinon
sub checkRemainingSpace
{
    my ($value, $origin) = @_;

    unless (defined $value)
    {
        $value = $y;
    }

    if ($value < $pageLimit)
    {
        $page = $pdf->addPage(LOC::Pdf::DISPLAY_FULLHEADER);
        $gfx  = $page->gfx();
        $text = $page->text();

        $y = $pageHeight - 55/mm;

        if (defined($origin) && $origin eq 'tabContent')
        {
            &printHeaderTabContent();
        }
        return 1;
    }
    return 0;
}

# Function: printHeaderTabContent
# Affichage de la ligne d'entete du tableau des désignations
#
sub printHeaderTabContent
{
    $x  = $margin;
    
    $text->lead(10/pt);
    $y -= $text->lead() + 1/mm;
    
    $text->font($boldFont, 8/pt);
    $gfx->linewidth(1/pt);
    
    
    $text->translate($x, $y);
    foreach my $colId (keys(%tabColsList))
    {
        $text->text($tabColsList{$colId}->{'label'}, -maxWidth => $tabColsList{$colId}->{'width'} - $gap * 2);
        $gfx->move($x, $y - 1/mm);
        $x += $tabColsList{$colId}->{'width'};
        $text->translate($x + $gap, $y);
        $gfx->line($x - $gap, $y - 1/mm);
    }
  
    $gfx->stroke();
    
    # Lignes du tableau
    $text->font($regularFont, 10/pt);
    $y -= $text->lead() + 1/mm;

    return 1;
}

# Function: displayPageNumbers
# Affichage du numéro de page
sub displayPageNumbers
{
    my $nbPages = $pdfObj->pages();
    
    for (my $i = 0; $i < $nbPages; $i++)
    {
        my $page = $pdfObj->openpage($i + 1);
        my $text = $page->text();
        
        $text->font($italicFont, 8/pt);
        $text->translate($pageWidth - $margin, 1.5 * $margin);
        $text->text_right(sprintf('%d/%d', $i + 1, $nbPages));
    }
}

# Function: displayTableLine
# Affichage d'une ligne du tableau
#
# Parameters:
# string $value - Ordonnée ($y par défaut)
#
# Returns:
# bool - 1 si l'espace restant est suffisant, 0 sinon
sub displayTableLine
{
    my ($y, $tabInfos) = @_;

    $text->font($regularFont, 8/pt);
    $text->lead(9/pt);

    $gfx->linewidth(0.1/pt);

    my $x = $margin;
    $text->translate($x, $y);
    
    my @colTypeDate     = ['beginDate', 'endDate'];
    my @colTypeCurrency = ['deliveryAmount', 'recoveryAmount'];
    my @colAlignCenter  = [];
    my @colAlignRight   = ['duration', 'deliveryAmount', 'recoveryAmount'];
    my @colTypeTwoLines = ['site', 'model.label'];
    my @colTypeTwoLinesCenter = ['insurance'];

    foreach my $id (keys(%tabColsList))
    {
        my $value = $tabInfos->{$id};
        # Formatage pour le champ date
        if (&LOC::Util::in_array($id, @colTypeDate))
        {
            $value = $locale->getDateFormat($value, LOC::Locale::FORMAT_DATE_NUMERIC);
        }
        # Formatage pour les champs type nombre
        elsif (&LOC::Util::in_array($id, @colTypeCurrency))
        {
            $value = $locale->getCurrencyFormat($value, undef, $tabViewData{'currencySymbol'});
        }
        # Formatage pour le champ chantier
        elsif ($id eq 'site')
        {
            $value = $value->{'label'} . "\n" . $value->{'locality.label'};
        }
        # Formatage pour le champ assurance
        elsif ($id eq 'insurance')
        {
            my $index = 0;
            my $label = '';
            my @tab = keys(%$tabInsuranceTypes);
            if (!&LOC::Util::in_array($value->{'typeId'}, \@tab))
            {
                $index = @tab + 1;
                $tabInsuranceTypes->{$value->{'typeId'}} = $index;
            }
            else
            {
                $index = $tabInsuranceTypes->{$value->{'typeId'}};
            }
            my $rate;
            if ($value->{'flags'} & LOC::Insurance::Type::FLAG_ISINVOICED)
            {
                $rate = $locale->t('%s %%', $value->{'rate'} * 100);
            }
            $label = $locale->t('%s %s', $tabViewData{'tabInsuranceTypesList'}->{$value->{'typeId'}}->{'code'}, $rate);

            # - Gestion de recours
            if ($value->{'flags'} & LOC::Insurance::Type::FLAG_ISAPPEALACTIVATED)
            {
                my $tabAppealInfos = $value->{'appeal'};
                my $appealRate = '';
                my $appealCode = '';

                if ($tabAppealInfos->{'mode'} == LOC::Appeal::Type::MODE_NOTINVOICED)
                {
                    $appealCode = APPEALCODE_NOTINVOICED;
                }
                else
                {
                    $appealCode = APPEALCODE;
                    $appealRate = $locale->t('%s %%', $tabAppealInfos->{'value'} * 100);
                }

                my $appealValue = $locale->t('%s %s', $appealCode, $appealRate);
                push(@tabAppealTypes, $appealCode);

                $label .= "\n" . $appealValue; 
            }

            $value = $label;

        }
        # Formatage pour le champ Tarif
        elsif ($id eq 'tariff')
        {
            if ($value->{'amountType'} eq 'day')
            {
                $value = $locale->t('%s / jour', $locale->getCurrencyFormat($value->{'unitPrice'}, undef, $tabViewData{'currencySymbol'}));
            }
            elsif ($value->{'amountType'} eq 'month')
            {
                $value = $locale->t('%s / mois', $locale->getCurrencyFormat($value->{'unitPrice'}, undef, $tabViewData{'currencySymbol'}));
            }
        }

        $gfx->move($x, $y - $text->lead() - 1/mm);

        # Cellule alignée à droite
        if (&LOC::Util::in_array($id, @colAlignRight))
        {
            $x += $tabColsList{$id}->{'width'};
            $text->translate($x - $gap * 2, $y);
            $text->text_right($value, -maxWidth => $tabColsList{$id}->{'width'} - $gap);
        }
        # Centrée (contenu court)
        elsif (&LOC::Util::in_array($id, @colAlignCenter))
        {
            $text->translate($x + $tabColsList{$id}->{'width'} / 2, $y);
            $text->text_center($value);
            $x += $tabColsList{$id}->{'width'};
            $text->translate($x + $gap, $y);
        }
        # 2 lignes
        elsif (&LOC::Util::in_array($id, @colTypeTwoLines))
        {
            $text->translate($x + $gap, $y);
            $text->section($value, $tabColsList{$id}->{'width'} - $gap, 20/pt, -spillover => 0);
            $x += $tabColsList{$id}->{'width'};
            $text->translate($x + $gap, $y);
        }
        # 2 lignes centrées
        elsif (&LOC::Util::in_array($id, @colTypeTwoLinesCenter))
        {
            $text->translate($x + $tabColsList{$id}->{'width'} / 2, $y);
            $text->section($value, $tabColsList{$id}->{'width'} - $gap, 20/pt, -spillover => 0, -align => 'center');
            $x += $tabColsList{$id}->{'width'};
            $text->translate($x + $gap, $y);
        }
        # Par défaut
        else
        {
            $text->translate($x + $gap, $y);
            $text->text($value, -maxWidth => $tabColsList{$id}->{'width'} - $gap);
            $x += $tabColsList{$id}->{'width'};
            $text->translate($x + $gap, $y);
        }

        $gfx->line($x - $gap, $y - $text->lead() - 1/mm);
    }

    $gfx->stroke();

    $y -= $text->lead();
    
    $text->lead(10/pt);
    $y -= $text->lead() + 1/mm;

    return $y;
}


