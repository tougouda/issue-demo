use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Json;
use LOC::Locale;


our %tabViewData;

# Locale
our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});


my $view = LOC::Json->new();

if (defined $tabViewData{'errors'})
{
    $view->setHttpResponseCode(400);
}


print $view->displayHeader();

print $view->encode(\%tabViewData);
