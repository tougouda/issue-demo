use utf8;
use open (':encoding(UTF-8)');


tie(my %tabLines, 'Tie::IxHash');
%tabLines = ();
$tabLines{'rentTotal'}               = $locale->t('Location');
$tabLines{'insuranceAmount'}         = $locale->t('Assurance');
$tabLines{'appealAmount'}            = $locale->t('Gestion de recours');
$tabLines{'transportDeliveryAmount'} = $locale->t('Transport aller');
$tabLines{'transportRecoveryAmount'} = $locale->t('Transport retour');
$tabLines{'cleaningAmount'}          = $locale->t('Nettoyage');
$tabLines{'residuesAmount'}          = $locale->t('Participation au recyclage');
$tabLines{'fuelAmount'}              = $locale->t('Carburant');
$tabLines{'repairAmount'}            = $locale->t('Remise en état');
$tabLines{'otherServices'}           = $locale->t('Services complémentaires');

print '
<fieldset id="rentContractTotalBlock">
    <legend>' . $view->displayImage('rent/rentContract/summary.png') . ' ' . $locale->t('Résumé') . '</legend>

    <table id="rentContractTotalTable" class="standard" width="100%">
    <thead>
        <tr>
            <th>' . htmllocale('Prestation') . '</th>
            <th colspan="2" style="text-align: right;">' . htmllocale('Montant') . '</th>
        </tr>
    </thead>
    <tfoot>
        <tr class="total">
            <td class="label">' . htmllocale('Total') . '</td>
            <td colspan="2" class="amount" style="text-align: right;"><div>' .
                $view->displayFormInputCurrency('rentContractTotal[total]', 0,
                                                LOC::Html::CURRENCYMODE_TEXT,
                                                $currencySymbol) .
           '</div></td>
        </tr>
    </tfoot>
    <tbody>';

# Lignes
foreach (keys %tabLines)
{
    print '
        <tr id="rentContractTotal_' . $_ . '" class="line ' . $_ . '">
            <td class="label">' . $tabLines{$_} . '</td>
            <td class="oldAmount">' .
                $view->displayFormInputCurrency('rentContractInitTotal[' . $_ . ']', 0,
                                                LOC::Html::CURRENCYMODE_TEXT,
                                                $currencySymbol) .
           '</td>
            <td class="amount">' .
                $view->displayFormInputCurrency('rentContractTotal[' . $_ . ']', 0,
                                                LOC::Html::CURRENCYMODE_TEXT,
                                                $currencySymbol) .
           '</td>
        </tr>';
}

# Total
print '
    </tbody>
    </table>
</fieldset>';


1;
