use utf8;
use open (':encoding(UTF-8)');

my $tabModelInfos   = $tabViewData{'tabContractInfos'}->{'tabModelInfos'};
my $tabMachineInfos = $tabViewData{'tabContractInfos'}->{'tabMachineInfos'};
my $tabLinkedContracts = $tabViewData{'tabContractInfos'}->{'tabLinkedContracts'};

# Symbole champ requis
my $requiredIcon = $view->displayRequiredSymbol();

# Onglet Matériel
my @tabModelsList = ();
while (my($key, $tab) = each(%{$tabViewData{'tabModelsList'}}))
{
    my $attributes = {
        'value' => $key,
        'innerHTML' => sprintf('%s (%s, %s, %s)', $tab->{'label'},
                                                  $tab->{'manufacturer.label'},
                                                  $tab->{'machinesFamily.energy'},
                                                  $tab->{'machinesFamily.elevation'}),
        '_search' => $tab->{'fullName'}
    };
    if (!$tab->{'isRentable'})
    {
        $attributes->{'innerHTML'} .= ' [' . $locale->t('Non disponible') . ']';
        $attributes->{'norentable'} = 'norentable';
    }
    push(@tabModelsList, $attributes);
}

# Désactivation du select du modèle si on est pas en pré-contrat
my $tabModelSelectAttributes = {};
if (($tabViewData{'tabContractInfos'}->{'state.id'} ne LOC::Contract::Rent::STATE_PRECONTRACT &&
     $tabViewData{'tabContractInfos'}->{'state.id'} ne ''))
{
    $tabModelSelectAttributes = {'disabled' => 'disabled'};
}
my $modelInput = $view->displayFormSelect('rentContractModelId',
                                          \@tabModelsList, $tabModelInfos->{'id'},
                                          0, $tabModelSelectAttributes);
$view->addPageInput({
    'sel'  => '#rentContractModelId\.search, #rentContractModelId',
    'get'  => 'Location.searchBoxesManager.getSearchBox("rentContractModelId")',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});

# Modèle impératif
my $modelImperativeInput = $view->displayFormInputCheckBox('rentContractIsImperativeModel', 0,
                                                           {'id' => 'rentContractIsImperativeModel'},
                                                           $locale->t('Modèle impératif'), 1, 0);
$view->addPageInput({
    'sel'  => '#rentContractIsImperativeModel',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});

# Joindre document machine
my $docEquipmentInput = $view->displayFormInputCheckBox('rentContractIsJoinDoc', $tabViewData{'tabContractInfos'}->{'isJoinDoc'},
                                                           {'id' => 'rentContractIsJoinDoc'},
                                                           $locale->t('Fournir les documents de la machine au client'), 1, 0);
$view->addPageInput({
    'sel'  => '#rentContractIsJoinDoc',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});


# Création de l'URL de consultation des stocks
my $tabFilters = {'countryId' => $tabContractInfos->{'country.id'}, 'agencyId' => $tabContractInfos->{'agency.id'}};
my $stockUrl = &LOC::Globals::get('locationUrl') . '/cgi-bin/index.cgi?';
$stockUrl .= 'locModule=machine&locController=stock&locAction=view';
$stockUrl .= '&countryId=' . $tabContractInfos->{'country.id'} . '&agencyId=' . $tabContractInfos->{'agency.id'};

# Javascript d'ouverture de la popup
my $openStockUrl = 'var url = "' . $stockUrl . '"; ' .
                   'if ($ge("rentContractModelId.search").value == "") { ' .
                   'url += "&modelId=" + Location.searchBoxesManager.getSearchBox("rentContractModelId").getValue(); } ' .
                   'else { url += "&search=" + $ge("rentContractModelId.search").value; } ' .
                   'Location.popupsManager.getPopup("viewStock").open(url);';

# Bouton stock
my $stockBtnTitle = $locale->t('Consulter le stock de machines pour le modèle de machines sélectionné');
my $stockBtn = $view->displayTextButton('', 'common/stock/view(|Over).png',
                                        $openStockUrl, $stockBtnTitle);
$stockBtn = '<div id="stockBtn">' . $stockBtn . '</div>';

# Aide sur la recherche de modèle de machines
my $modelHelp = $view->displayHelp($locale->t('La recherche de modèle de machines s\'effectue parmi la famille de machines, l\'énergie, l\'élévation et le modèle de machines'));

my $wishedMachine = $view->displayFormSelect('rentContractWishedMachineId');

my $wishedMachineInfo = '';
if ($tabContractInfos->{'tabWishedMachineInfos'})
{
   $wishedMachineInfo = '<div class="wished" onclick="displayWishedMachineTooltip(this, 1500);">' .
                  '<div class="tip">' .
                      '<div title="' . htmllocale('Machine souhaitée') . '" class="icon"></div>' .
                      $view->displayParkNumber($tabContractInfos->{'tabWishedMachineInfos'}->{'parkNumber'}, 'rentContractWishedMachineParkNumber', 1, 'light') .
                  '</div>' .
               '</div>';
}

my $siteBlockedMachine = '';
if ($tabMachineInfos->{'isSiteBlocked'})
{
   $siteBlockedMachine = '<div title="' . htmllocale('Machine bloquée sur chantier') . '" class="icon"></div>';
}

print '
<div title="' . htmllocale('Matériel') . '">
    <table>
    <tr style="vertical-align: top">
        <td style="padding-right: 10px">

            <fieldset class="sub">
                <legend>' . htmllocale('Matériel demandé') . '</legend>

                <table class="formTable">
                <tr>
                    <td class="label">' . $stockBtn . htmllocale('Modèle de machines') . $requiredIcon . $modelHelp . '</td>
                    <td style="white-space: nowrap">' . $modelInput . '</td>
                </tr>
                <tr>
                    <td class="label">' . htmllocale('Famille de machines') . '</td>
                    <td id="rentContractMachinesFamily"></td>
                </tr>
                <tr>
                    <td class="label">' . htmllocale('Famille tarifaire') . '</td>
                    <td id="rentContractTariffFamily"></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">' . $modelImperativeInput . '</td>
                </tr>
                <tr>
                    <td colspan="2">' . $docEquipmentInput . '</td>
                </tr>
                </table>
            </fieldset>
        </td>
        <td id="rentContractModelUseRateBlock" class="loading">
            <fieldset class="sub">
                <legend>' . htmllocale('Taux d\'utilisation') . '</legend>
                <table class="formTable" id="rentContractModelUseRateTable">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th colspan="2">' . htmllocale('Modèle') . '</th>
                        <th colspan="2">' . htmllocale('Famille tarifaire') . '</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="label">
                            ' . $view->displayImage('agency.png') . '
                            ' . htmllocale('Agence') . '
                        </td>
                        <td class="useRate">-</td>
                        <td class="nbTotal">-</td>
                        <td class="useRate">-</td>
                        <td class="nbTotal">-</td>
                    </tr>
                    <tr>
                        <td class="label">
                            ' . $view->displayImage('area.png') . '
                            ' . htmllocale('Région') . '
                        </td>
                        <td class="useRate">-</td>
                        <td class="nbTotal">-</td>
                        <td class="useRate">-</td>
                        <td class="nbTotal">-</td>
                    </tr>
                    <tr>
                        <td class="label">
                            ' . $view->displayCountryImage($tabViewData{'tabContractInfos'}->{'country.id'}) . '
                            ' . htmllocale('National') . '
                        </td>
                        <td class="useRate">-</td>
                        <td class="nbTotal">-</td>
                        <td class="useRate">-</td>
                        <td class="nbTotal">-</td>
                    </tr>
                </tbody>
                </table>
            </fieldset>
        </td>
    </tr>
    <tr style="vertical-align: top">
        <td style="padding-right: 10px">
            <fieldset class="sub">
                <legend>' . htmllocale('Machine attribuée') . '</legend>';

if ($tabMachineInfos->{'parkNumber'})
{
    print '
                <table class="formTable">
                <tr>
                    <td class="label">
                        ' . htmllocale('Actuellement louée') . '
                    </td>
                    <td>
                        <span id="rentContractMachineInfos">
                       ' . $view->displayParkNumber($tabMachineInfos->{'parkNumber'}, 'rentContractParkNumber', 1, 'light') . '
                       ' . $tabMachineInfos->{'modelFullName'} . '
                        </span>
                    </td>
                    <td class="wishedMachine">' . $wishedMachineInfo . '</td>
                    <td class="siteBlockedMachine">' . $siteBlockedMachine . '</td>
                </tr>';
    if ($tabMachineInfos->{'isControlVgp'})
    {
        print '
                <tr>
                    <td class="label">
                        ' . htmllocale('Prochain contrôle') . '
                    </td>
                    <td>
                    '  . $locale->getDateFormat($tabMachineInfos->{'control.nextDate'},
                                                LOC::Locale::FORMAT_DATE_NUMERIC) . '
                    ' . $view->displayScore($tabMachineInfos->{'control.score'}, 4) . '
                    </td>
                </tr>';
    }


    my $breakdownDeclarationInfos = $tabMachineInfos->{'breakdownDeclarationInfos'};
    foreach my $breakdownDeclarationInfos (@{$breakdownDeclarationInfos->{'tabEndActions'}})
    {
        if ($breakdownDeclarationInfos->{'tabContent'}->{'content'} eq '')
        {
            $breakdownDeclarationInfos->{'tabContent'}->{'content'} = $locale->t('Pas de contact chantier');
        }
    }
    print '
                <tr>
                    <td class="label">
                        ' . htmllocale('Déclarer une panne') . '
                    </td>
                    <td>' .
                        $view->displayPrintButtons('breakdownDeclarationBtn', '', '',
                                                        $breakdownDeclarationInfos->{'defaultRecipient'},
                                                        $breakdownDeclarationInfos->{'defaultSubject'},
                                                        $breakdownDeclarationInfos->{'defaultContent'},
                                                        $breakdownDeclarationInfos->{'documentAgency.id'},
                                                        {
                                                            'buttons' => ['mail'],
                                                            'tabReplacesTpl' => $breakdownDeclarationInfos->{'tabReplacesTpl'},
                                                            'defaultCopyRecipient' => $breakdownDeclarationInfos->{'defaultCopyRecipient'},
                                                            'tabEndActions' => $breakdownDeclarationInfos->{'tabEndActions'}
                                                        }) . '
                    </td>
                </tr>';

    print '
                </table>';
}
else
{
    print '
                <span class="noresult">' . $view->displayMessages('info', [$locale->t('Pas de machine attribuée')], 0) . '</span>
                <table class="formTable" style="margin-top: 10px;">
                <tbody>
                    <tr>
                        <td class="label">' . htmllocale('Attribution souhaitée') . '</td>
                        <td>' . $wishedMachine .
                                $view->displayTextButton('', 'clear.gif', 'return false;', $locale->t('Effacer la recherche'), 'l', {'id' => 'rentContractClearWishedMachineBtn'}) .
                        '</td>
                    </tr>
                </tbody>
                </table>';

    print $view->displayJSBlock('
Location.searchBoxesManager.createSearchBox("rentContractWishedMachineId");')
}

# Tableau des contrats enfants
my $countChildren = keys %{$tabLinkedContracts->{'children'}};
my $countParent   = keys %{$tabLinkedContracts->{'parent'}};

# Tableau contenant soit le sous-tableau du parent, soit celui des enfants
my $tabLnkContracts;
my $contractTypeLabel;
if ($countParent > 0)
{
    $contractTypeLabel = htmllocale('Contrat parent');
    $tabLnkContracts   = $tabLinkedContracts->{'parent'};
}
else
{
    $contractTypeLabel = htmllocale('Contrats enfants');
    $tabLnkContracts   = $tabLinkedContracts->{'children'};
}

print '
            </fieldset>
        </td>
        <td>';

if ($countChildren > 0 || $countParent > 0)
{
    print '
            <fieldset class="sub">
                <legend>' . $contractTypeLabel . '</legend>

                <table class="standard">
                <tr>
                    <th>' . htmllocale('Famille') . '</th>
                    <th>' . htmllocale('Modèle') . '</th>
                    <th>' . htmllocale('N° de parc') . '</th>
                    <th>' . htmllocale('Contrat') . '</th>
                </tr>';
    my $onclickContractCode;
    while (my($key, $tab) = each(%$tabLnkContracts))
    {
        $onclickContractCode = 'window.open(\'' . $view->createURL('rent:rentContract:view', {'contractId' => $tab->{'id'}}) . '\');';
        print '
                <tr>
                    <td>' . $tab->{'machinesFamily.fullName'} . '</td>
                    <td>' . $tab->{'model.label'} . '</td>
                    <td>' . $view->displayParkNumber($tab->{'machine.parkNumber'}) . '</td>
                    <td><a href="#" onclick="' . $onclickContractCode . '">' . $tab->{'code'} . '</a></td>
                </tr>';
    }
    print '
                </table>

            </fieldset>';
}

print '
        </td>
    </tr>
    </table>
</div>';

# Création de la searchBox du modèle de machine, de la machine souhaitée et déclaration de la popup de consultation du stock
print $view->displayJSBlock('
Location.searchBoxesManager.createSearchBox("rentContractModelId");
Location.popupsManager.createPopup("viewStock", "", "center", "center", 900, 550);');


1;
