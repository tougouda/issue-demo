use utf8;
use open (':encoding(UTF-8)');

my $tabRentInvoice = $tabViewData{'tabContractInfos'}->{'tabRentInvoice'};
my $nbRentInvoices = keys(%$tabRentInvoice) * 1;

my $tabRepairInvoice = $tabViewData{'tabContractInfos'}->{'tabRepairInvoice'};
my $nbRepairInvoices = keys(%$tabRepairInvoice) * 1;

if ($nbRentInvoices > 0 || $nbRepairInvoices > 0)
{
    my $invoicePopupJS = 'var invoicePopup = ' .
                         'Location.popupsManager.createPopup("invoicePopup", "", "center", "center", 800, 600);';
    print $view->displayJSBlock($invoicePopupJS);
}

print '
<div title="' . htmllocale('Facturation') . '">';


# Factures de location
print '
<fieldset id="rentInvoices" class="sub">
<legend>' . htmllocale('Factures de location') . '</legend>';

if ($nbRentInvoices > 0)
{
    print '
    <table class="standard">
    <tr>
        <th>' . htmllocale('N°') . '</th>
        <th>' . htmllocale('Date') . '</th>
        <th>' . htmllocale('Client') . '</th>
        <th style="text-align: right;">' . htmllocale('Montant HT') . '</th>
    </tr>';

    my $totalAmount = 0;

    # Factures
    foreach my $invoiceId (keys(%$tabRentInvoice))
    {
        my $tabLine = $tabRentInvoice->{$invoiceId};

        my $invoiceUrl = $view->createURL('accounting:invoice:view',
                                          {'type' => LOC::Invoice::TYPE_RENT, 'invoiceId' => $invoiceId});
        my $invoiceLink = '<a href="javascript:;" onclick="invoicePopup.open(\'' . $invoiceUrl . '\');">' .
                                ($tabLine->{'number'} eq '' ? '#' : $tabLine->{'number'}) .
                          '</a>';

        my $customerLink = $tabLine->{'customer.code'};
        if ($tabLine->{'customer.id'} != 0)
        {
            my $customerUrl = &LOC::Globals::get('locationUrl') . '/cgi-bin/old/location/infoclient.cgi?value=' .
                              $tabLine->{'customer.id'};
            $customerLink = '<a href="' . $customerUrl . '" target="_blank">' . $customerLink . '</a>';
        }

        my $amountInput = $view->displayFormInputCurrency('rentInvoiceAmount[' . $invoiceId . ']',
                                                          $tabLine->{'amount'}, LOC::Html::CURRENCYMODE_TEXT,
                                                          $currencySymbol);
        $totalAmount += $tabLine->{'amount'};

        print '
    <tr>
        <td>' . $invoiceLink . '</td>
        <td>' . ($tabLine->{'date'} eq '' ? '' : $locale->getDateFormat($tabLine->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC)) . '</td>
        <td>' . $customerLink . '</td>
        <td style="text-align: right;">' . $amountInput . '</td>
    </tr>';
    }

    my $amountInput = $view->displayFormInputCurrency('rentInvoiceTotalAmount',
                                                      $totalAmount, LOC::Html::CURRENCYMODE_TEXT,
                                                      $currencySymbol);

    # Total
    print '
    <tr class="totalInvoices">
        <td>' . htmllocale('Total') . '</td>
        <td colspan="2">' . $locale->tn('%d facture', '%d factures', $nbRentInvoices, $nbRentInvoices) . '</td>
        <td style="text-align: right;">' . $amountInput . '</td>
    </tr>';
    
    print '
    </table>';
}
else
{
    print '<span class="noresult">' . htmllocale('Aucune') . '</span>';
}

print '
</fieldset>';


# Factures de remise en état
print '
<fieldset id="repairInvoices" class="sub">
<legend>' . htmllocale('Factures de remise en état') . '</legend>';

if ($nbRepairInvoices > 0)
{
    print '
    <table class="standard">
    <tr>
        <th>' . htmllocale('N°') . '</th>
        <th>' . htmllocale('Date') . '</th>
        <th>' . htmllocale('Client') . '</th>
        <th style="text-align: right;">' . htmllocale('Montant HT') . '</th>
    </tr>';
    
    my $totalAmount = 0;
    
    # Factures
    foreach my $invoiceId (keys(%$tabRepairInvoice))
    {
        my $tabLine = $tabRepairInvoice->{$invoiceId};
        
        my $invoiceUrl = $view->createURL('accounting:invoice:view',
                                          {'type' => $tabLine->{'type'}, 'invoiceId' => $invoiceId});
        my $invoiceLink = '<a href="javascript:;" onclick="invoicePopup.open(\'' . $invoiceUrl . '\');">' .
                                ($tabLine->{'number'} eq '' ? '#' : $tabLine->{'number'}) .
                          '</a>';

        my $customerLink = $tabLine->{'customer.code'};
        if ($tabLine->{'customer.id'} != 0)
        {
            my $customerUrl = &LOC::Globals::get('locationUrl') . '/cgi-bin/old/location/infoclient.cgi?value=' .
                              $tabLine->{'customer.id'};
            $customerLink = '<a href="' . $customerUrl . '" target="_blank">' . $customerLink . '</a>';
        }
        
        my $amountInput = $view->displayFormInputCurrency('repairInvoiceAmount[' . $invoiceId . ']',
                                                          $tabLine->{'amount'}, LOC::Html::CURRENCYMODE_TEXT,
                                                          $currencySymbol);
        $totalAmount += $tabLine->{'amount'};
        
        print '
    <tr>
        <td>' . $invoiceLink . '</td>
        <td>' . ($tabLine->{'date'} eq '' ? '' : $locale->getDateFormat($tabLine->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC)) . '</td>
        <td>' . $customerLink . '</td>
        <td style="text-align: right;">' . $amountInput . '</td>
    </tr>';
    }
    
    my $amountInput = $view->displayFormInputCurrency('repairInvoiceTotalAmount',
                                                      $totalAmount, LOC::Html::CURRENCYMODE_TEXT,
                                                      $currencySymbol);

    # Total
    print '
    <tr class="totalInvoices">
        <td>' . htmllocale('Total') . '</td>
        <td colspan="2">' . $locale->tn('%d facture', '%d factures', $nbRepairInvoices, $nbRepairInvoices) . '</td>
        <td style="text-align: right;">' . $amountInput . '</td>
    </tr>';
    
    print '
    </table>';
}
else
{
    print '<span class="noresult">' . htmllocale('Aucune') . '</span>';
}

print '
</fieldset>';

print '
</div>';


1;
