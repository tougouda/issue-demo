use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'rent/rentContract/';


our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}

# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'search.css');

# Js
$view->addJSSrc($dir . 'search.js');

$view->setPageTitle($locale->t('Contrat de location'))
     ->addPackage('popups')
     ->addPackage('tabBoxes')
     ->addPackage('tooltips')
     ->addPackage('searchboxes');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);


$view->setTitle($locale->t('Contrat de location'));

$view->addTranslations({
    'label-result-s' => $locale->t('1 résultat'),
    'label-result-p' => $locale->t('%s / %s résultats', '<%nb>', '<%total>'),
    'label-see-more-s' => $locale->t('Voir %s résultat supplémentaire', '<%step>'),
    'label-see-more-p' => $locale->t('Voir %s résultats supplémentaires', '<%step>')
});

# Formatage des dates
foreach my $tabInfos (@{$tabViewData{'result'}})
{
    $tabInfos->{'beginDate'} = $locale->getDateFormat($tabInfos->{'beginDate'},
                                                      LOC::Locale::FORMAT_DATE_NUMERIC);
    $tabInfos->{'endDate'} = $locale->getDateFormat($tabInfos->{'endDate'},
                                                      LOC::Locale::FORMAT_DATE_NUMERIC);
}

my $searchUrl = $view->createURL('rent:rentContract:jsonSearch');
my $tabData = {
    'result' => $tabViewData{'result'},
    'total'  => $tabViewData{'total'}
};

$view->addBodyEvent('load', 'search.init("' . $searchUrl . '", ' .
                                &LOC::Json::toJson($tabViewData{'tabFilters'}) . ', ' .
                                &LOC::Json::toJson($tabData) . ',' .
                                $tabViewData{'offset'} . ', ' .
                                $tabViewData{'step'} . ');');

# Barre de recherche
require($directory . '/rentContract/searchBar.cgi');

# Affichage de l'entête
print $view->displayHeader();

print '<h1 class="searchResult">' . $locale->t('Résultat de la recherche') . '</h1>';

# S'il n'y a pas de critères sélectionnés
print '<p id="msg-ok" class="result-msg"></p>';

print '
<table id="contractsList" class="basic list">
    <thead>
        <tr>
            <th class="code">' . $locale->t('N°') . '</th>
            <th class="customer">' . $locale->t('Client') . '</th>
            <th colspan="2" class="site">' . $locale->t('Chantier') . '</th>
            <th class="orderNo">' . $locale->t('N° de commande') . '</th>
            <th class="state">' . $locale->t('État') . '</th>
            <th class="beginDate">' . $locale->t('Début') . '</th>
            <th class="endDate">' . $locale->t('Fin') . '</th>
            <th class="negotiator">' . $locale->t('Négociateur') . '</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
        <tr class="pending-search">
            <td colspan="9">
                ' . $locale->t('Recherche en cours') . $view->displayImage('loading.gif') . '
            </td>
        </tr>
        <tr class="no-filters">
            <td colspan="9">' . $locale->t('Aucun critère de recherche sélectionné. Veuillez affiner votre recherche.') . '</td>
        </tr>
        <tr class="no-result">
            <td colspan="9">' . $locale->t('Aucun contrat ne correspond à votre recherche') . '</td>
        </tr>
        <tr class="more" id="search-more">
            <td colspan="9"></td>
        </tr>
        <tr class="error">
            <td colspan="9">' . $locale->t('Une erreur est survenue lors de la recherche.') . '</td>
        </tr>
    </tfoot>
</table>';


# Affichage du pied de page

print $view->displayFooter();
