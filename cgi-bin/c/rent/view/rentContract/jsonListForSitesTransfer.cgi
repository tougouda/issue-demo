use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Json;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Fonctions de formatage des données
require($directory . '/helper/format/transport.cgi');


our %tabViewData;


# Locale
my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});



foreach my $tabContractInfos (@{$tabViewData{'tabContracts'}})
{
    &hlpFormatTransportContractData($tabContractInfos, $locale, {});
}


my $view = LOC::Json->new();
print $view->displayHeader();
print $view->encode($tabViewData{'tabContracts'});
