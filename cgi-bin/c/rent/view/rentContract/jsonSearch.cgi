use strict;


use LOC::Json;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


our %tabViewData;


# Locale
my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});


foreach my $tabInfos (@{$tabViewData{'result'}})
{
    $tabInfos->{'beginDate'} = $locale->getDateFormat($tabInfos->{'beginDate'},
                                                      LOC::Locale::FORMAT_DATE_NUMERIC);
    $tabInfos->{'endDate'} = $locale->getDateFormat($tabInfos->{'endDate'},
                                                      LOC::Locale::FORMAT_DATE_NUMERIC);
}


my $view = LOC::Json->new();
print $view->displayHeader();
print $view->encode(\%tabViewData);
