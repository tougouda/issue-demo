use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'rent/rentContract/';


our %tabViewData;
my $errorType    = $tabViewData{'tabError'}->{'type'};
my $tabErrorData = $tabViewData{'tabError'}->{'data'};

our $inputsTabIndex = 1;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}

# Feuilles de style
$view->addCSSSrc('location.css');

$view->addPackage('searchboxes');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);


my $title = $locale->t('Contrat de location');
$view->setPageTitle($title);
$view->setTitle($locale->t('Contrat de location'));

# Barre de recherche
require($directory . '/rentContract/searchBar.cgi');

# Affichage de l'entête
print $view->displayHeader();

# Impossible de passer la ligne de devis en contrat
my $text = '';
if ($errorType eq 'convert')
{
    $text = $locale->t('Impossible de passer la ligne "%s" du devis "%s" en contrat',
                       $tabErrorData->{'estimateLine.id'}, $tabErrorData->{'estimate.code'});
}
elsif ($errorType eq 'unknown')
{
    $text = $locale->t('Impossible de trouver le contrat (%s)', $tabErrorData->{'country.label'});
}

print '
    <div class="messageBlock error">
    <div class="popup" style="height: 80px;">
        <div class="content"><b>' . $text . '.</b></div>
    </div>
</div>';

print $view->displayFooter();
