use utf8;
use open (':encoding(UTF-8)');

# File: duration.cgi
#
# Affichage du bloc de durée

# Traductions
$view->addTranslations({
    $prefix . 'label_to_complete'        => $locale->t('À compléter'),
    $prefix . 'label_unknown_date'       => $locale->t('Date non saisie'),
    $prefix . 'label_get_from_migration' => $locale->t('Récupéré de l\'ancien système'),
    $prefix . 'label_invoiced'           => $locale->t('Jour facturé'),
    $prefix . 'label_non_invoiced'        => $locale->t('Jour non facturé'),

    $prefix . 'title_rentalPeriod_s'    => $locale->t('%s jour ouvré', '<%duration>'),
    $prefix . 'title_rentalPeriod_p'    => $locale->t('%s jours ouvrés', '<%duration>'),

    $prefix . 'title_totalAdditionalDays_s'  => $locale->t('%s jour supplémentaire', '<%additionalDays>'),
    $prefix . 'title_totalAdditionalDays_p'  => $locale->t('%s jours supplémentaires', '<%additionalDays>'),

    $prefix . 'title_additionalDaysInvoiced_s'  => $locale->t('%s jour supplémentaire déjà facturé', '<%additionalDaysInvoiced>'),
    $prefix . 'title_additionalDaysInvoiced_p'  => $locale->t('%s jours supplémentaires déjà facturés', '<%additionalDaysInvoiced>'),

    $prefix . 'title_totalDeductedDays_s'  => $locale->t('%s jour déduit', '<%deductedDays>'),
    $prefix . 'title_totalDeductedDays_p'  => $locale->t('%s jours déduits', '<%deductedDays>'),

    $prefix . 'title_deductedDaysInvoiced_s'  => $locale->t('%s jour déduit déjà facturé', '<%deductedDaysInvoiced>'),
    $prefix . 'title_deductedDaysInvoiced_p'  => $locale->t('%s jours déduits déjà facturés', '<%deductedDaysInvoiced>'),

    $prefix . 'title_authorizedPeriod_beginDate_s' => $locale->t('Début de location%s %s jour ouvré', '<%appliedToContract>', '<%deltaBeginDate>'),
    $prefix . 'title_authorizedPeriod_beginDate_p' => $locale->t('Début de location%s %s jours ouvrés', '<%appliedToContract>', '<%deltaBeginDate>'),
    $prefix . 'title_authorizedPeriod_endDate_s' => $locale->t('Fin de location%s %s jour ouvré', '<%appliedToContract>', '<%deltaEndDate>'),
    $prefix . 'title_authorizedPeriod_endDate_p' => $locale->t('Fin de location%s %s jours ouvrés', '<%appliedToContract>', '<%deltaEndDate>'),

    $prefix . 'title_authorizedPeriod_appliedToContract' => $locale->t(' du contrat lié'),

    $prefix . 'label_additionalMore_s' => $locale->t('<%nb> autre jour supplémentaire...'),
    $prefix . 'label_additionalMore_p' => $locale->t('<%nb> autres jours supplémentaires...'),
    $prefix . 'label_deductedMore_s' => $locale->t('<%nb> autre jour déduit...'),
    $prefix . 'label_deductedMore_p' => $locale->t('<%nb> autres jours déduits...'),

    $prefix . 'button_edit'      => $locale->t('Modifier le jour'),
    $prefix . 'button_delete'    => $locale->t('Supprimer le jour'),
    $prefix . 'button_undo'      => $locale->t('Annuler l\'action en cours'),
    $prefix . 'button_invoice'   => $locale->t('Facturer le jour'),
    $prefix . 'button_uninvoice' => $locale->t('Ne pas facturer le jour'),

    $prefix . 'select_defaultChoice' => $locale->t('-- Choisir --')
});

my $tabErrors = {
    'noAppliedToContract' => $locale->t('Le contrat lié n\'est pas renseigné'),
    'noDate'              => $locale->t('La date n\'est pas renseignée ou n\'est pas valide'),
    'noReason'            => $locale->t('Le motif n\'est pas sélectionné'),
    'dateNotInPeriod'     => $locale->t('La date n\'est pas dans la période autorisée'),
    'existingDay'         => $locale->t('Un jour existe déjà sur la date')
};

print '
<div title="' . &htmllocale('Durée') . '" id="' . $prefix . 'content">
    <div class="container">';

# # # #
# Période de location
# # # #
# - Bouton d'édition des périodes de location
my $rentalPeriodBtn = $view->displayTextButton($locale->t('Modifier'), 'edit(|Over).png',
                                               'return false;',
                                               $locale->t('Modifier la période de location'),
                                               'l',
                                               {'id' => $prefix . 'periodBtn',
                                                'class' => 'locCtrlButton'}
                                              );
$view->addPageInput({
    'sel' => '#' . $prefix . 'periodBtn',
    'if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ') && this.style.visibility != "hidden"',
    'set' => 'this.onclick()'
});

# - Dates à confirmer
my $dateToConfirmInput = $view->displayFormInputCheckBox($prefix . 'datesToConfirm', '', {}, $locale->t('Dates à confirmer'));
print '
        <div class="rentalPeriod hasBadge">
            <fieldset class="sub">
                <legend>' .
                    &htmllocale('Période de location') . '
                    <div class="daysBadge" id="' . $prefix . 'badgeDuration"></div>
                </legend>
                <div class="legendBtn">' . $rentalPeriodBtn . '</div>
                <span id="' . $prefix . 'beginDate" class="rentalDate begin"></span>
                <span id="' . $prefix . 'endDate" class="rentalDate end"></span>';

if ($tabViewData{'documentType'} ne 'contract')
{
    print '
                <div class="dateToConfirm">' . $dateToConfirmInput . '</div>';
}
if ($tabViewData{'documentType'} eq 'contract')
{
    # Affichage de l'alerte tarif de base a changé
    print '
                <div id="' . $prefix . 'hasChangedBaseAmount">
                    <div class="icon"></div>
                    <div class="msg">'
                    . $locale->t('La tranche de durée a été changée, veuillez vérifier votre tarif') .
                    '</div>
                </div>';
}

print '
            </fieldset>
        </div>';

# # # #
# Durée de location
# # # #
print '
        <div class="totalDuration">
            <fieldset class="sub">
                <legend>' . &htmllocale('Durée totale') . '</legend>
                <div class="duration rental">
                    ' . $view->displayDuration('?', $prefix . 'duration') . '
                    <div id="' . $prefix . 'durationChanged" title="' . &htmllocale('Le nombre de jours ouvrés a changé') . '"></div>
                </div>
                <div class="duration calendar">
                    ' . $view->displayCalendarDuration('?', $prefix . 'calendarDuration') . '
                </div>
            </fieldset>
        </div>';


print '
    </div>
    <div class="container">';

# # # #
# Jours supplémentaires
# # # #
# - Popup pour la liste des jours saisis sur un autre contrat
my $getExternalDaysPopup = sub
{
    my ($type, $tabDays) = @_;

    my $id = '';
    my $legend = '';
    if ($type eq LOC::Day::TYPE_ADDITIONAL)
    {
        $legend = $locale->t('Jours supplémentaires saisis sur un autre contrat');
        $id = $prefix . 'externalAddDaysPopup';
    }
    if ($type eq LOC::Day::TYPE_DEDUCTED)
    {
        $legend = $locale->t('Jours déduits saisis sur un autre contrat');
        $id = $prefix . 'externalDedDaysPopup';
    }

    my $content = '
                <div class="' . $prefix . 'popup externalDays" style="display: none;" id="' . $id . '">
                    <fieldset class="sub">
                        <legend>' . $legend . '</legend>
                        <table class="basic">
                            <thead>
                                <tr>
                                    <th class="date">' . &htmllocale('Date') . '</th>
                                    <th class="invoiceDuration"></th>
                                    <th class="reason">' . &htmllocale('Motif') . '</th>
                                    <th class="comment"></th>
                                    <th class="status">' . &htmllocale('État') . '</th>
                                </tr>
                            </thead>
                            <tbody>';

    my $previousContractId;

    foreach my $tabDayInfo (@$tabDays)
    {
        my $date = '';
        if ($tabDayInfo->{'date'} ne '')
        {
            $date = $locale->getDateFormat($tabDayInfo->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC);
        }

        my $invoiceDuration = '';
        if ($tabDayInfo->{'invoiceDuration'}->{'duration'} != 1)
        {
            my $className = '';
            if ($tabDayInfo->{'invoiceDuration'}->{'duration'} == 0.5)
            {
                $className = 'half';
            }
            if ($tabDayInfo->{'invoiceDuration'}->{'duration'} == 2)
            {
                $className = 'double';
            }

            $invoiceDuration = '<span class="' . $className . '"></span>';
        }

        my $comment = '';
        if ($tabDayInfo->{'comment'} ne '')
        {
            my $commentText = $tabDayInfo->{'comment'};
            $commentText =~ s/\n/<br>/g;
            $commentText =~ s/'/\\'/g;
            $commentText = $view->toHTMLEntities($commentText);
            $comment = '<div onmouseover="Location.toolTipsObj.show(\'' . $commentText . '\', {corner:\'tl\'});" onmouseout="Location.toolTipsObj.hide();"></div>';
        }

        my $status;
        if ($tabDayInfo->{'isInvoiced'})
        {
            $status = $view->displayState(LOC::Html::Standard::STATE_INVOICED, $locale->t('Facturé'));
        }
        elsif (!$tabDayInfo->{'isInvoiceable'})
        {
            $status = $view->displayState(LOC::Html::Standard::STATE_CANCELED, $locale->t('Non facturable'));
        }
        else
        {
            $status = $view->displayState(LOC::Html::Standard::STATE_PENDING, $locale->t('Non facturé'));
        }

        # Ligne de titre du contrat
        if ($previousContractId ne $tabDayInfo->{'contract'}->{'id'})
        {
            my $contractUrl = $view->createURL(
                    'rent:rentContract:view',
                    {
                        'contractId' => $tabDayInfo->{'contract'}->{'id'},
                        'tabIndex'   => TABINDEX_DURATION
                    }
                );
            my $contract = '<a href="' . $contractUrl . '" target="_blank" class="action-btn openRentContract">' . $tabDayInfo->{'contract'}->{'code'} . '</a>';

            $content .= '
                                <tr class="contract">
                                    <td colspan="6">' . $contract . '</td>
                                </tr>';
            $previousContractId = $tabDayInfo->{'contract'}->{'id'};
        }

        $content .= '
                                <tr>
                                    <td class="date">' . $date . '</td>
                                    <td class="invoiceDuration">' . $invoiceDuration . '</td>
                                    <td class="reason">' . $tabDayInfo->{'reason'}->{'label'} . '</td>
                                    <td class="comment">' . $comment . '</td>
                                    <td class="status">' . $status . '</td>
                                </tr>';
    }

    $content .= '
                            </tbody>
                        </table>
                    </fieldset>
                </div>';

    return $content;
};

# -Notification de jours saisis sur un autre contrat
my $getExternalDaysNotif = sub
{
    my ($type, $tabDays) = @_;

    if (!defined $tabDays || $tabDays->{'count'} == 0)
    {
        return '';
    }

    my $id = '';
    if ($type eq LOC::Day::TYPE_ADDITIONAL)
    {
        $id = $prefix . 'externalAddDaysNotif';
    }
    if ($type eq LOC::Day::TYPE_DEDUCTED)
    {
        $id = $prefix . 'externalDedDaysNotif';
    }

    my $notification = '';
    if ($tabDays->{'count'} == $tabDays->{'duration'})
    {
        if ($type eq LOC::Day::TYPE_ADDITIONAL)
        {
            $notification = $locale->tn(
                    '%s autre jour supplémentaire a également été saisi sur un autre contrat.',
                    '%s autres jours supplémentaires ont également été saisis sur d’autres contrats.',
                    $tabDays->{'count'},
                    $locale->getNumberFormat($tabDays->{'count'})
                );
        }
        if ($type eq LOC::Day::TYPE_DEDUCTED)
        {
            $notification = $locale->tn(
                    '%s autre jour déduit a également été saisi sur un autre contrat.',
                    '%s autres jours déduits ont également été saisis sur d’autres contrats.',
                    $tabDays->{'count'},
                    $locale->getNumberFormat($tabDays->{'count'})
                );
        }
    }
    else
    {
        my $durationLabel = $locale->tn(
                'pour une durée de %s jour',
                'pour une durée de %s jours',
                $tabDays->{'duration'},
                $locale->getNumberFormat($tabDays->{'duration'})
            );

        if ($type eq LOC::Day::TYPE_ADDITIONAL)
        {
            $notification = $locale->tn(
                    '%s autre jour supplémentaire a également été saisi sur un autre contrat %s.',
                    '%s autres jours supplémentaires ont également été saisis sur d’autres contrats %s.',
                    $tabDays->{'count'},
                    $locale->getNumberFormat($tabDays->{'count'}),
                    $durationLabel
                );
        }
        if ($type eq LOC::Day::TYPE_DEDUCTED)
        {
            $notification = $locale->tn(
                    '%s autre jour déduit a également été saisi sur un autre contrat %s.',
                    '%s autres jours déduits ont également été saisis sur d’autres contrats %s.',
                    $tabDays->{'count'},
                    $locale->getNumberFormat($tabDays->{'count'}),
                    $durationLabel
                );
        }
    }

    if ($notification ne '')
    {
        $notification .= '<br>' . $locale->t('Cliquez pour voir le détail.');
    }

    return &$getExternalDaysPopup($type, $tabDays->{'list'}) . '
                <div id="' . $id . '" class="' . $prefix . 'viewExternalDays">
                    ' . $view->displayMessages('info', [$notification], 0) . '
                </div>';
};

# - Bouton d'ajout d'un jour supplémentaire
my $addAdditionalDayBtn = $view->displayTextButton($locale->t('Ajouter'), 'add(|Over).png',
                                               'return false;',
                                               $locale->t('Ajouter un jour supplémentaire'),
                                               'l',
                                               {'id' => $prefix . 'addAdditionalDayBtn',
                                                'class' => 'locCtrlButton'}
                                              );
$view->addPageInput({
    'sel' => '#' . $prefix . 'addAdditionalDayBtn',
    'if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ') && this.style.visibility != "hidden"'
});
# - Bouton d'ajout d'un jour supplémentaire sur contrat lié
my $addAdditionalDayOnContractBtn = $view->displayTextButton($locale->t('Ajouter sur un contrat lié'), 'add(|Over).png',
                                               'return false;',
                                               $locale->t('Ajouter un jour supplémentaire sur un contrat lié'),
                                               'l',
                                               {'id' => $prefix . 'addAdditionalDayOnContractBtn',
                                                'class' => 'locCtrlButton'}
                                              );
$view->addPageInput({
    'sel' => '#' . $prefix . 'addAdditionalDayOnContractBtn',
    'if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ') && this.style.visibility != "hidden"'
});

my $addAdditionalDayDropdown = $view->displayDropdownButton($prefix . 'addAdditionalDayDropdown',
                                                            [$addAdditionalDayBtn, $addAdditionalDayOnContractBtn], 'r');

my $tabExternalAddDays = $tabViewData{'tabContractInfos'}->{'tabDuration'}->{'days'}->{LOC::Day::TYPE_ADDITIONAL}->{'external'};

print '
        <div class="additionalDays hasBadge">
            <fieldset class="sub">
                <legend>
                    ' . &htmllocale('Jours supplémentaires') . '
                    <div class="daysBadge add" id="' . $prefix . 'badgeAdditionalDays"></div>';

if ($tabViewData{'documentType'} eq 'contract')
{
print '
                    <div class="daysBadge add invoiced" id="' . $prefix . 'badgeAdditionalDaysInvoiced"></div>';
}
print '
                    <div class="dayComment" id="' . $prefix . 'additionalDaysComment">
                        ' . $view->displayImage($dir . '../helper/duration/comment.png') . '
                    </div>
                </legend>
                <div class="legendBtn">
                    ' . $addAdditionalDayDropdown . '
                </div>
                <table class="basic days-list" id="' . $prefix . 'additionalDaysList">
                    <thead>
                        <tr>
                            <th class="date">' . &htmllocale('Date') . '</td>
                            <th class="invoiceDuration"></td>
                            <th class="reason">' . &htmllocale('Motif') . '</td>
                            <th class="appliedToContract"></td>
                            <th class="comment"></td>
                            <th class="actions"></td>
                        </tr>
                    </thead>
                    <tfoot class="no-additionaldays">
                        <tr>
                            <td colspan="6">' . &htmllocale('Aucun jour supplémentaire') . '</td>
                        </tr>
                    </tfoot>
                    <tbody class="add"></tbody>
                    <tbody class="list"></tbody>
                    <tbody class="more"></tbody>
                </table>
                ' . &$getExternalDaysNotif(LOC::Day::TYPE_ADDITIONAL, $tabExternalAddDays) . '
            </fieldset>
        </div>';


# # # #
# Jours déduits
# # # #
# - Bouton d'ajout d'un jour déduit
my $addDeductedDayBtn = $view->displayTextButton($locale->t('Ajouter'), 'add(|Over).png',
                                               'return false;',
                                               $locale->t('Ajouter un jour déduit'),
                                               'l',
                                               {'id' => $prefix . 'addDeductedDayBtn',
                                                'class' => 'locCtrlButton'}
                                              );
$view->addPageInput({
    'sel' => '#' . $prefix . 'addDeductedDayBtn',
    'if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ') && this.style.visibility != "hidden"'
});
# - Bouton d'ajout d'un jour supplémentaire sur contrat lié
my $addDeductedDayOnContractBtn = $view->displayTextButton($locale->t('Ajouter sur un contrat lié'), 'add(|Over).png',
                                               'return false;',
                                               $locale->t('Ajouter un jour déduit sur un contrat lié'),
                                               'l',
                                               {'id' => $prefix . 'addDeductedDayOnContractBtn',
                                                'class' => 'locCtrlButton'}
                                              );
$view->addPageInput({
    'sel' => '#' . $prefix . 'addDeductedDayOnContractBtn',
    'if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ') && this.style.visibility != "hidden"'
});

my $addDeductedDayDropdown = $view->displayDropdownButton($prefix . 'addDeductedDayDropdown',
                                                            [$addDeductedDayBtn, $addDeductedDayOnContractBtn], 'r');

my $tabExternalDedDays = $tabViewData{'tabContractInfos'}->{'tabDuration'}->{'days'}->{LOC::Day::TYPE_DEDUCTED}->{'external'};

print '
        <div class="deductedDays hasBadge">
            <fieldset class="sub">
                <legend>
                    ' . &htmllocale('Jours déduits') . '
                    <div class="daysBadge ded" id="' . $prefix . 'badgeDeductedDays"></div>';

if ($tabViewData{'documentType'} eq 'contract')
{
print '
                    <div class="daysBadge ded invoiced" id="' . $prefix . 'badgeDeductedDaysInvoiced"></div>';
}
print '
                    <div class="dayComment" id="' . $prefix . 'deductedDaysComment">
                        ' . $view->displayImage($dir . '../helper/duration/comment.png') . '
                    </div>
                </legend>
                <div class="legendBtn">' . $addDeductedDayDropdown . '</div>
                <table class="basic days-list" id="' . $prefix . 'deductedDaysList">
                    <thead>
                        <tr>
                            <th class="date">' . &htmllocale('Date') . '</td>
                            <th class="invoiceDuration"></td>
                            <th class="reason">' . &htmllocale('Motif') . '</td>
                            <th class="appliedToContract"></td>
                            <th class="comment"></td>
                            <th class="actions"></td>
                        </tr>
                    </thead>
                    <tfoot class="no-deducteddays">
                        <tr>
                            <td colspan="6">' . &htmllocale('Aucun jour déduit') . '</td>
                        </tr>
                    </tfoot>
                    <tbody class="add"></tbody>
                    <tbody class="list"></tbody>
                    <tbody class="more"></tbody>
                </table>
                ' . &$getExternalDaysNotif(LOC::Day::TYPE_DEDUCTED, $tabExternalDedDays) . '
            </fieldset>
        </div>';

print '
    </div>
';

# # # #
# Popup ajout/modif d'un jour
# # # #

# Symbole champ requis
my $requiredIcon = $view->displayRequiredSymbol();

my $dateInput               = $view->displayFormInputDate($prefix . 'addDate', undef, undef, undef, {}, 1);
my $invoiceDurationSelect   = $view->displayFormSelect($prefix . 'addInvoiceDurationId');
my $reasonSelect            = $view->displayFormSelect($prefix . 'addReasonId');
my $contractsListUrl        = $tabViewData{'appliedToContractSearchUrl'};
my $appliedToContractSelect = $view->displayFormSelect($prefix . 'addAppliedToContractId') . $view->displayJSBlock('
var appliedToContractSearchBoxObj = Location.searchBoxesManager.createSearchBox("' . $prefix . 'addAppliedToContractId", {isRequired: false, url: "' . $contractsListUrl . '"});
appliedToContractSearchBoxObj.createElement = function(key, value)
{
    var opt = new Option(value["code"], value["id"]);
    opt.dataset.data = JSON.stringify(value);
    return opt;
}');

my $commentInput      = $view->displayFormTextArea($prefix . 'addComment', '');
my $isNonInvoiceableCheckbox    = $view->displayFormInputCheckBox($prefix . 'addIsNonInvoiceable', 0, undef, undef, 1);

# Bouton d'ajout
my $validBtn = $view->displayTextButton($locale->t('Ajouter'),
                                      'add(|Over).png',
                                      'return false;',
                                      $locale->t('Ajouter')
                                      ,'l',
                                      {'id' => $prefix . 'addBtn',
                                       'tabindex' => -1,
                                       'onfocus' => 'this.blur();'});

print '
    <div class="' . $prefix . 'popup" style="display: none;" id="' . $prefix . 'addPopup">
        <fieldset class="sub content">
            <legend>
                <span class="legend-A">' . &htmllocale('Ajouter un jour supplémentaire') . '</span>
                <span class="legend-D">' . &htmllocale('Ajouter un jour déduit') . '</span>
                <span class="legend-appliedToContract">' . &htmllocale('sur un contrat lié') . '</span>
            </legend>

            <table class="formTable">
                <tbody>
                    <tr class="appliedToContract">
                        <td class="label">' . &htmllocale('Contrat lié') . $requiredIcon . '</td>
                        <td class="content">' .
                            $appliedToContractSelect . '
                            <a href="#" onclick="return false;" id="' . $prefix . 'addAppliedToContractIdClear">' .
                                $view->displayImage('clear.gif', $locale->t('Effacer la recherche')) . '
                            </a>' .
                            $view->displayTextButton('',
                                      'rent/helper/customer/customerHistory(|Over).png',
                                      'return true;',
                                      $locale->t('Historique client')
                                      ,'l',
                                      {'id' => $prefix . 'addCustomerHistoryBtn',
                                       'tabindex' => -1,
                                       'target'   => '_blank',
                                       'onfocus'  => 'this.blur();'})
                            . '
                        </td>
                    </tr>
                    <tr class="date">
                        <td class="label">' . &htmllocale('Date') . ($tabViewData{'documentType'} eq 'contract' ? $requiredIcon : '') . '</td>
                        <td class="content">' .
                            $dateInput . $invoiceDurationSelect . '
                            <div class="info" id="' . $prefix . 'addPeriodInfos">' .
                                $locale->t('entre le') . ' <span id="' . $prefix . 'addPeriodInfos_beginDate"></span> ' . $locale->t('et le') . ' <span id="' . $prefix . 'addPeriodInfos_endDate"></span>
                            </div>
                        </td>
                    </tr>
                    <tr class="reason">
                        <td class="label">' . &htmllocale('Motif') . $requiredIcon . '</td>
                        <td class="content">' . $reasonSelect . '</td>
                    </tr>
                    <tr class="comment">
                        <td class="label">' . &htmllocale('Commentaires') . '</td>
                        <td class="content">' . $commentInput . '</td>
                    </tr>
                    <tr class="noninvoiceable" id="' . $prefix . 'addIsNonInvoiceableLine">
                        <td class="label">' . &htmllocale('Ne pas facturer') . '</td>
                        <td class="content">' . $isNonInvoiceableCheckbox . '</td>
                    </tr>

                </tbody>
            </table>
        </fieldset>
        <fieldset class="sub ctrls">
            ' . $validBtn . '
            <div class="' . $prefix . 'popupErrors">
                <span class="' . $prefix . 'error-noAppliedToContract">' . $view->displayMessages('error', [$tabErrors->{'noAppliedToContract'}], 0) . '</span>
                <span class="' . $prefix . 'error-noDate">' . $view->displayMessages('error', [$tabErrors->{'noDate'}], 0) . '</span>
                <span class="' . $prefix . 'error-noReason">' . $view->displayMessages('error', [$tabErrors->{'noReason'}], 0) . '</span>
                <span class="' . $prefix . 'error-dateNotInPeriod">' . $view->displayMessages('error', [$tabErrors->{'dateNotInPeriod'}], 0) . '</span>
                <span class="' . $prefix . 'error-existingDay">' . $view->displayMessages('error', [$tabErrors->{'existingDay'}], 0) . '</span>
            </div>
        </fieldset>
        <div class="loading-layer"></div>
    </div>';

# # # #
# - Popup modif d'un jour
# # # #
my $dateInput               = $view->displayFormInputDate($prefix . 'editDate', undef, undef, undef, {}, 1);
my $invoiceDurationSelect   = $view->displayFormSelect($prefix . 'editInvoiceDurationId', \@tabInvoiceDurations, $defaultInvoiceDuration);
my $reasonSelect            = $view->displayFormSelect($prefix . 'editReasonId', \@tabReasons);
my $contractsListUrl        = $tabViewData{'appliedToContractSearchUrl'};
my $appliedToContractSelect = $view->displayFormSelect($prefix . 'editAppliedToContractId', {}, 0) . $view->displayJSBlock('
var appliedToContractSearchBoxObj = Location.searchBoxesManager.createSearchBox("' . $prefix . 'editAppliedToContractId", {isRequired: false, url: "' . $contractsListUrl . '"});
appliedToContractSearchBoxObj.createElement = function(key, value)
{
    var opt = new Option(value["code"], value["id"]);
    opt.dataset.data = JSON.stringify(value);
    return opt;
};');
my $commentInput      = $view->displayFormTextArea($prefix . 'editComment', '');
my $isNonInvoiceableCheckbox    = $view->displayFormInputCheckBox($prefix . 'editIsNonInvoiceable', 0, undef, undef, 1);

# Bouton d'ajout
my $validBtn = $view->displayTextButton($locale->t('Modifier'),
                                      'edit(|Over).png',
                                      'return false;',
                                      $locale->t('Ajouter')
                                      ,'l',
                                      {'id' => $prefix . 'editBtn',
                                       'tabindex' => -1,
                                       'onfocus' => 'this.blur();'});

print '
    <div class="' . $prefix . 'popup" style="display: none;" id="' . $prefix . 'editPopup">
        <fieldset class="sub content">
            <legend>
                <span class="legend-A">' . &htmllocale('Modifier un jour supplémentaire') . '</span>
                <span class="legend-D">' . &htmllocale('Modifier un jour déduit') . '</span>
                <span class="legend-appliedToContract">' . &htmllocale('sur un contrat lié') . '</span>
            </legend>

            <table class="formTable">
                <tbody>
                    <tr class="appliedToContract">
                        <td class="label">' . &htmllocale('Contrat lié') . $requiredIcon . '</td>
                        <td class="content">' .
                            $appliedToContractSelect . '
                            <a href="#" onclick="return false;" id="' . $prefix . 'editAppliedToContractIdClear">' .
                                $view->displayImage('clear.gif', $locale->t('Effacer la recherche')) . '
                            </a>' .
                            $view->displayTextButton('',
                                      'rent/helper/customer/customerHistory(|Over).png',
                                      'return true;',
                                      $locale->t('Historique client')
                                      ,'l',
                                      {'id' => $prefix . 'editCustomerHistoryBtn',
                                       'tabindex' => -1,
                                       'target'   => '_blank',
                                       'onfocus'  => 'this.blur();'})
                            . '
                        </td>
                    </tr>
                    <tr class="date">
                        <td class="label">' . &htmllocale('Date') . ($tabViewData{'documentType'} eq 'contract' ? $requiredIcon : '') . '</td>
                        <td class="content">' .
                            $dateInput . $invoiceDurationSelect . '
                            <div class="info" id="' . $prefix . 'editPeriodInfos">' .
                                $locale->t('entre le') . ' <span id="' . $prefix . 'editPeriodInfos_beginDate"></span> ' . $locale->t('et le') . ' <span id="' . $prefix . 'editPeriodInfos_endDate"></span>
                            </div>
                        </td>
                    </tr>
                    <tr class="reason">
                        <td class="label">' . &htmllocale('Motif') . $requiredIcon . '</td>
                        <td class="content">' . $reasonSelect . '</td>
                    </tr>
                    <tr class="comment">
                        <td class="label">' . &htmllocale('Commentaires') . '</td>
                        <td class="content">' . $commentInput . '</td>
                    </tr>
                    <tr class="noninvoiceable" id="' . $prefix . 'editIsNonInvoiceableLine">
                        <td class="label">' . &htmllocale('Ne pas facturer') . '</td>
                        <td class="content">' . $isNonInvoiceableCheckbox . '</td>
                    </tr>

                </tbody>
            </table>
        </fieldset>
        <fieldset class="sub ctrls">
            ' . $validBtn . '
            <div class="' . $prefix . 'popupErrors">
                <span class="' . $prefix . 'error-noAppliedToContract">' . $view->displayMessages('error', [$tabErrors->{'noAppliedToContract'}], 0) . '</span>
                <span class="' . $prefix . 'error-noDate">' . $view->displayMessages('error', [$tabErrors->{'noDate'}], 0) . '</span>
                <span class="' . $prefix . 'error-noReason">' . $view->displayMessages('error', [$tabErrors->{'noReason'}], 0) . '</span>
                <span class="' . $prefix . 'error-dateNotInPeriod">' . $view->displayMessages('error', [$tabErrors->{'dateNotInPeriod'}], 0) . '</span>
                <span class="' . $prefix . 'error-existingDay">' . $view->displayMessages('error', [$tabErrors->{'existingDay'}], 0) . '</span>
            </div>
        </fieldset>
        <div class="loading-layer"></div>
    </div>';



# # # #
# Popup de modification de la période de location
# # # #
# - Bouton de validation de la période de location
my $rentalPeriodValidBtn = $view->displayTextButton($locale->t('Valider'), 'valid(|Over).png', 'return false;',
                                                    $locale->t('Valider les modifications effectuées sur le contrat'),
                                                    'l',
                                                    {'id' => $prefix . 'periodValidBtn',
                                                     'class' => 'locCtrlButton'});

# - Date de début et de fin de location
my $beginDateInput = $view->displayFormInputDate($prefix . 'periodRequiredBeginDate', undef, undef, undef);
my $endDateInput = $view->displayFormInputDate($prefix . 'periodRequiredEndDate', undef, undef, undef);

print '
    <div style="display: none;" id="' . $prefix . 'period">
        <fieldset class="sub content">
            <legend>' . &htmllocale('Actuellement') . '</legend>
            <table class="formTable">
            <tbody>
                <tr>
                    <td class="label">' . &htmllocale('Date de début') . '</td>
                    <td class="ctrls"></td>
                    <td class="content"><span id="' . $prefix . 'periodBeginDate"></span></td>
                </tr>
                <tr>
                    <td class="label">' . &htmllocale('Date de fin') . '</td>
                    <td class="ctrls"></td>
                    <td class="content"><span id="' . $prefix . 'periodEndDate"></span></td>
                </tr>
                <tr>
                    <td class="label">' . &htmllocale('Durée') . '</td>
                    <td class="ctrls"></td>
                    <td class="content"><span id="' . $prefix . 'periodDurationStr"></span></td>
                </tr>
                <tr>
                    <td class="label">' . &htmllocale('Tranche') . '</td>
                    <td class="ctrls"></td>
                    <td class="content"><span id="' . $prefix . 'periodDurationRange"></span></td>
                </tr>
            </tbody>
            </table>
        </fieldset>

        <fieldset class="sub content">
            <legend>' . &htmllocale('Demandé') . '</legend>
            <table class="formTable">
            <tbody>
                <tr id="' . $prefix . 'periodRequiredBeginDateRow">
                    <td class="label">' . &htmllocale('Date de début') . '</td>
                    <td class="ctrls"></td>
                    <td class="content">' .
                        $beginDateInput . '
                        <div class="info" id="' . $prefix . 'beginDate_window">' .
                            $locale->t('entre le') . ' <span id="' . $prefix . 'beginDate_window_1"></span> ' . $locale->t('et le') . ' <span id="' . $prefix . 'beginDate_window_2"></span>
                        </div>
                        <div class="info" id="' . $prefix . 'beginDate_max">' .
                            $locale->t('jusqu\'au') . ' <span id="' . $prefix . 'beginDate_window_0"></span>
                        </div>
                    </td>
                </tr>
                <tr id="' . $prefix . 'periodRequiredEndDateRow" class="dateMode">
                    <td class="label">' . &htmllocale('Date de fin') . '</td>
                    <td class="ctrls">
                        <a href="#" class="date" onclick="this.parentNode.parentNode.className = \'daysMode\'; Location.spinControlsManager.getSpinControl(\'' . $prefix . 'periodRequiredRealDuration\').focus(); return false;" title="' . &htmllocale('Basculer en saisie de durée (en jours ouvrés)') . '"></a>
                        <a href="#" class="days" onclick="this.parentNode.parentNode.className = \'dateMode\'; $ge(\'' . $prefix . 'periodRequiredEndDate.masked\').focus(); return false;" title="' . &htmllocale('Basculer en saisie de date') . '"></a>
                    </td>
                    <td class="content">
                        <div class="date">' .
                            $endDateInput . '
                            <div class="info" id="' . $prefix . 'endDate_window">' .
                                $locale->t('entre le') . ' <span id="' . $prefix . 'endDate_window_1"></span> ' . $locale->t('et le') . ' <span id="' . $prefix . 'endDate_window_2"></span>
                            </div>
                            <div class="info" id="' . $prefix . 'endDate_min">' .
                                $locale->t('à partir du') . ' <span id="' . $prefix . 'endDate_window_0"></span>
                            </div>
                        </div>' .
                       '<div class="days">' .
                            '<input id="' . $prefix . 'periodRequiredRealDuration" type="text" value=""/>' .
                            '<input id="' . $prefix . 'periodRequiredRealDurationBtn" type="button" value="ok"/>' .
                            $view->displayJSBlock('Location.spinControlsManager.createSpinControl("' . $prefix . 'periodRequiredRealDuration", 0, null, 1, 10, 0);') .
                       '</div>' .
                       '<span id="' . $prefix . 'theorEndDate" title="' . &htmllocale('Date de fin théorique') . '"></span>' .
                   '</td>
                </tr>
                <tr>
                    <td class="label">' . &htmllocale('Durée') . '</td>
                    <td class="ctrls"></td>
                    <td class="content"><span id="' . $prefix . 'periodRequiredDurationStr"></span></td>
                </tr>
                <tr>
                    <td class="label">' . &htmllocale('Tranche') . '</td>
                    <td class="ctrls"></td>
                    <td class="content"><span id="' . $prefix . 'periodRequiredDurationRange"></span></td>
                </tr>
            </tbody>
            </table>
        </fieldset>

        <fieldset class="sub ctrls">
            ' . $rentalPeriodValidBtn . '
        </fieldset>
    </div>

</div>';


1;
