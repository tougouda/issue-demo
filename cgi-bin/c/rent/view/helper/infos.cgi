use utf8;
use open (':encoding(UTF-8)');

# File: infos.cgi
#
# Affichage du bloc d'informations générales sur le devis et le contrat.
# Le fichier a besoin d'une table de hachage appelée $tabViewData contenant les clés suivantes :
# - documentType : type de document (estimate ou contract) ;
# - tabDocumentInfos : référence à une table de hachage contenant les informations à afficher ;
#
# $tabViewData{'tabDocumentInfos'} contient les clés suivantes :
# - index : indice de modification (contrat seulement) ;
# - tabAgencies : référence à une table de hachage contenant la liste des agences ;
# - agency.id : identifiant de l'agence ;
# - locked : état de blocage du document ;
# - tabUsers : référence à une table de hachage contenant la liste des utilisateurs.
# - creator.id : identifiant de l'utilisateur qui a créé le document ;
# - creationDate : date de création du document ;


my $prefix;
my $title;
my $tabStates;
my $tabDocInfos = $tabViewData{'tabDocumentInfos'};

if ($tabViewData{'documentType'} eq 'estimate')
{
    $title = $locale->t('Devis de location');
    $prefix = 'rentEstimate';

    # Etats
    $tabStates = {
        LOC::Estimate::Rent::STATE_ACTIVE   => $locale->t('Actif'),
        LOC::Estimate::Rent::STATE_ABORTED  => $locale->t('Abandonné'),
        LOC::Estimate::Rent::STATE_CONTRACT => $locale->t('Contrat')
    };
}
elsif ($tabViewData{'documentType'} eq 'contract')
{
    $title = $locale->t('Contrat de location');
    $prefix = 'rentContract';

    $tabStates = {
        LOC::Contract::Rent::STATE_PRECONTRACT => $locale->t('Pré-contrat'),
        LOC::Contract::Rent::STATE_CONTRACT => $locale->t('Contrat'),
        LOC::Contract::Rent::STATE_STOPPED => $locale->t('Arrêt'),
        LOC::Contract::Rent::STATE_ARCHIVE => $locale->t('Archive'),
        LOC::Contract::Rent::STATE_CANCELED => $locale->t('Annulé'),
        LOC::Contract::Rent::STATE_BILLED => $locale->t('Mis en facturation')
    };
}


# Symbole champ requis
my $requiredIcon = $view->displayRequiredSymbol();

# Code du document
my $codeInput = $view->displayFormInputText($prefix . 'Code', '', '',
                                            {'id' => $prefix . 'Code', 'size' => 24, 'style' => 'font-weight: bold;',
                                             'readonly' => 'readonly'});

# FullService ?
my $fullServiceInput = $view->displayFormInputCheckBox($prefix . 'IsFullService', 0,
                                                       {'id' => $prefix . 'IsFullService',
                                                        'disabled' => ($tabViewData{'editMode'} eq 'update' ? 'disabled' : '')},
                                                       $locale->t('Full service (minimum 6 mois)'), 1);
$view->addPageInput({'sel'  => '#' . $prefix . 'IsFullService'});

# Agence
my $agencyLabel = ($tabDocInfos->{'tabAgencies'}->{$tabDocInfos->{'agency.id'}} ? $tabDocInfos->{'tabAgencies'}->{$tabDocInfos->{'agency.id'}} : $tabDocInfos->{'agency.id'});
my $agencyInput = $view->displayFormSelect($prefix . 'AgencyId', [{'value' => $tabDocInfos->{'agency.id'}, 'innerHTML' => $agencyLabel}],
                                           $tabDocInfos->{'agency.id'},
                                           0, {'id' => $prefix . 'AgencyId', 'disabled' => 'disabled'});

# Créateur
my $creatorInput = $view->displayFormSelect($prefix . 'CreatorId', $tabDocInfos->{'tabUsers'},
                                            $tabDocInfos->{'creator.id'},
                                            0, {'id' => $prefix . 'CreatorId', 'disabled' => 'disabled'});

my $icon = '';
if ($tabViewData{'documentType'} eq 'estimate')
{
    $icon = 'common/rentEstimate/ic_rent_estimate_16px.svg';
}
elsif ($tabViewData{'documentType'} eq 'contract')
{
    $icon = 'common/rentContract/ic_rent_contract_16px.svg';
}

print '
<fieldset id="rentInfosBlock">
    <legend>' . $view->displayImage($icon) . '&nbsp;' . $title . '</legend>';

if ($tabViewData{'documentType'} eq 'contract')
{
    print '<div class="index" title="' . htmllocale('Indice de modification') . '"> ' .
              $view->displayCounter('<span id="' . $prefix . 'Index">?</span>') .
          '</div>';
}


print '
    <table class="formTable">
    <tbody>
        <tr>
            <td class="label"><label for="code">' . htmllocale('Code') . '</label></td>
            <td class="input">' . $codeInput . '</td>
            <td class="label"><label for="agency.id">' . htmllocale('Agence') . '</label></td>
            <td class="input">' . $agencyInput . '</td>
            <td class="input">' . $fullServiceInput . '</td>
            <td class="input">
                <div class="status" id="' . $prefix . 'Status">';
foreach my $state (keys(%$tabStates))
{
    print '
                    <div state="' . $state . '">' .
                        $view->displayState($state, $tabStates->{$state}) .
                   '</div>';
}

print '
                </div>
            </td>';

# Blocage
print '
            <td class="input">' . $view->displayLocked($tabDocInfos->{'isLocked'}) . '</td>';


if ($tabViewData{'documentType'} eq 'contract')
{
    # État de facturation
    my $lastInvoicingDate = $locale->getDateFormat($tabDocInfos->{'lastInvoicingDate'}, LOC::Locale::FORMAT_DATE_NUMERIC);
    my $tabInvoicingStateLabel = {
                                  ''   => $locale->t('Non facturé'),
                                  'P'  => $locale->t('Location facturée jusqu\'au %s', $lastInvoicingDate),
                                  'PF' => $locale->t('Location entièrement facturée'),
                                  'F'  => $locale->t('Contrat entièrement facturé'),
                                 };

    my $displayedInvoicingStatus = ($tabDocInfos->{'invoicingStatus'} eq '' ? '&nbsp;' : $tabDocInfos->{'invoicingStatus'});
    my $invoicingStatus = $view->displayInvoicingState($displayedInvoicingStatus, $prefix . 'InvoicingState') .
                          ' ' . $tabInvoicingStateLabel->{$tabDocInfos->{'invoicingStatus'}};

    print '
            <td class="input">' . $invoicingStatus . '</td>';

    # Contrat clôturé ?
    if ($tabDocInfos->{'closureDate'} ne '')
    {
        my $date = $locale->getDateFormat($tabDocInfos->{'closureDate'}, LOC::Locale::FORMAT_DATETIME_NUMERIC2, $tabViewData{'timeZone'});
        print '
            <td class="input"><div class="closed" title="' . htmllocale('Clôturé le %s', $date) . '">' . htmllocale('Contrat clôturé') . '</div></td>';
    }
}


print '
        </tr>
    </tbody>
    </table>

    <hr />

    <table class="formTable">
    <tbody>
        <tr>
            <td class="label"><label for="creator.id">' . htmllocale('Créé par') . '</label></td>
            <td class="input">' . $creatorInput . '</td>
            <td class="label">' . htmllocale('le') . '</td>
            <td class="input">' . $locale->getDateFormat($tabDocInfos->{'creationDate'},
                                                         LOC::Locale::FORMAT_DATETIME_NUMERIC, $tabViewData{'timeZone'}) . '</td>
            <td class="label">' . htmllocale('modifié le') . '</td>
            <td class="input" id="' . $prefix . 'ModificationDate"></td>';

if ($tabDocInfos->{'linkedEstimateLine'})
{
    my $estimateStr = $tabDocInfos->{'linkedEstimateLine'}->{'estimateCode'} .
                      ' <small><i>(' . htmllocale('ligne %s', $tabDocInfos->{'linkedEstimateLine'}->{'index'}) . ')</i></small>';
    my $estimateUrl = $view->createURL('rent:rentEstimate:view',
                                       {'estimateId' => $tabDocInfos->{'linkedEstimateLine'}->{'estimateId'},
                                        'estimateLineId' => $tabDocInfos->{'linkedEstimateLine'}->{'id'}});

    print '
            <td class="label">' . htmllocale('Devis associé') . '</td>
            <td class="input"><a class="action-btn openRentEstimate" target="_blank" title="' . htmllocale('Voir le devis') . '" href="' . $estimateUrl . '">' . $estimateStr . '</a></td>';
}
print '
        </tr>
    </tbody>
    </table>

    <hr />

    <table class="formTable">
    <tbody>
        <tr>';

#Négociateur
# Zone de recherche utilisateur
my $estimateUserId = $view->displayFormSelect($prefix . 'NegotiatorId', $tabViewData{'creatorsList'}, $tabViewData{'estimateUserId'});
my $userListUrl = $view->createURL('default:user:jsonList', {'format' => 1});
$estimateUserId .= $view->displayJSBlock('
Location.searchBoxesManager.createSearchBox("' . $prefix . 'NegotiatorId", {isRequired: false, url: "' . $userListUrl . '"});');
$view->addPageInput({
    'sel' => '#' . $prefix . 'NegotiatorId\.search', '#' . $prefix . 'NegotiatorId',
    'get' => 'Location.searchBoxesManager.getSearchBox("' . $prefix . 'NegotiatorId")'
});

print '
            <td class="label">' . htmllocale('Négociateur') . $requiredIcon . '</td>
            <td class="input"><span>' . $estimateUserId . '</span>' .
                   $view->displayTextButton('', 'clear.gif', 'return false;', $locale->t('Effacer la recherche'), 'l', {'id' => $prefix . 'ClearNegotiatorBtn'}) . '
            </td>';

print '
        </tr>
    </tbody>
    </table>
</fieldset>';


1;
