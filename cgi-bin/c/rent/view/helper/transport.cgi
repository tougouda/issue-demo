use utf8;
use open (':encoding(UTF-8)');

# File: rentTariff.cgi
#
# Affichage du bloc de tarification transport


# Symbole champ requis
my $requiredIcon = $view->displayRequiredSymbol();
my $currentDir = $dir . '../helper/';


# Types d'erreurs
my $tabErrorTypes = [
    {
        'code'         => 'noMachine',
        'title'        => $locale->t('Machine non attribuée'),
        'isFatalError' => 0
    },
    {
        'code'         => 'customerNotVerified',
        'title'        => $locale->t('Client avec ouverture de compte en attente'),
        'isFatalError' => 1
    },
    {
        'code'         => 'proformaNotGen',
        'title'        => $locale->t('Pro forma non générée'),
        'isFatalError' => 0
    },
    {
        'code'         => 'proformaNotUpToDate',
        'title'        => $locale->t('Pro forma non à jour'),
        'isFatalError' => 0
    },
    {
        'code'         => 'proformaNotPayed',
        'title'        => $locale->t('Pro forma non finalisée'),
        'isFatalError' => 0
    },
    {
        'code'         => 'rentTariff',
        'title'        => $locale->t('Remise exceptionnelle de location en attente'),
        'isFatalError' => 0
    },
    {
        'code'         => 'transportTariff',
        'title'        => $locale->t('Remise exceptionnelle de transport en attente'),
        'isFatalError' => 0
    },
    {
        'code'         => 'deliveryNotDone',
        'title'        => $locale->t('Livraison non effectuée'),
        'isFatalError' => 0
    },
    {
        'code'         => 'onDelivery',
        'title'        => $locale->t('Erreur(s) sur la livraison du transfert'),
        'isFatalError' => 0
    },
    {
        'code'         => 'onRecovery',
        'title'        => $locale->t('Erreur(s) sur la récupération du transfert'),
        'isFatalError' => 0
    },
    {
        'code'         => 'noMainContract',
        'title'        => $locale->t('Pas de contrat principal sélectionné'),
        'isFatalError' => 0
    },
    {
        'code'         => 'machineSiteBlocked',
        'title'        => $locale->t('Machine bloquée sur chantier'),
        'isFatalError' => 0
    },
    {
        'code'         => 'machineNotTransfered',
        'title'        => $locale->t('Machine non transférée'),
        'isFatalError' => 0
    }
];

# Types d'infos complémentaires pour les contrats en transfert
my $tabInfosClpTypes = [
    {
        'code'         => 'self',
        'title'        => $locale->t('Enlèvement')
    },
    {
        'code'         => 'anticipatable',
        'title'        => $locale->t('Livraison anticipée possible')
    },
    {
        'code'         => 'planned',
        'title'        => $locale->t('Transport planifié')
    },
];

print '
<div title="' . htmllocale('Transport') . '">

<table class="formTable tariff ' . $prefix . 'content" id="' . $prefix . 'Table">
<tbody>
    <tr>';

if ($tabViewData{'isTrspTarification'})
{
    $view->addTranslations({
        'transportNoCustomer'    => $locale->t('Pas de client sélectionné.'),
        'transportNoZone'        => $locale->t('Zone incorrecte ou non définie.'),
        'transportNoModelId'     => $locale->t('Pas de modèle de machine sélectionné.'),
        'transportNoMachineSize' => $locale->t('Encombrement de machine incorrecte.')
    });

    # Définition des éléments du bloc
    my $baseAmountInput = $view->displayFormInputCurrency($prefix . 'BaseAmount', 0, LOC::Html::CURRENCYMODE_TEXT,
                                                          $currencySymbol, 0);

    my $reductionLabel = htmllocale('%s %% soit %s');

    my $stdReducInput = $view->displayFormInputText($prefix . 'StdReduc', 0, '', {'class' => 'reduction', 'disabled' => 'disabled'});
    my $stdReducAmountInput = $view->displayFormInputCurrency($prefix . 'StdReducAmount', 0,
                                                              LOC::Html::CURRENCYMODE_INPUT,
                                                              $currencySymbol,
                                                              undef,
                                                              {'class' => 'reductionAmount', 'disabled' => 'disabled'});

    my $reducInput = $view->displayFormInputText($prefix . 'Reduc', 0, '', {'class' => 'reduction', 'disabled' => 'disabled'});
    my $reducAmountInput = $view->displayFormInputCurrency($prefix . 'ReducAmount', 0,
                                                           LOC::Html::CURRENCYMODE_INPUT,
                                                           $currencySymbol,
                                                           undef,
                                                           {'class' => 'reductionAmount', 'disabled' => 'disabled'});

    my $excReducInput = $view->displayFormInputText($prefix . 'SpcReduc', 0, '', {'class' => 'reduction', 'disabled' => 'disabled'});
    my $excReducAmountInput = $view->displayFormInputCurrency($prefix . 'SpcReducAmount', 0,
                                                              LOC::Html::CURRENCYMODE_INPUT,
                                                              $currencySymbol,
                                                              undef,
                                                              {'class' => 'reductionAmount', 'disabled' => 'disabled'});

    my $recoMaxiLabel = htmllocale('Reco. %s, max. %s, soit %s');
    my $recoPcInput = '<span id="' . $prefix . 'Reco"></span> %';
    my $maxiPcInput = '<span id="' . $prefix . 'Maxi"></span> %';
    my $maxiAmountInput = $view->displayFormInputCurrency($prefix . 'MaxiAmount', 0, LOC::Html::CURRENCYMODE_TEXT,
                                                          $currencySymbol, 0);

    my $amountInput = $view->displayFormInputCurrency($prefix . 'Amount', 0,
                                                      LOC::Html::CURRENCYMODE_INPUT, $currencySymbol, undef,
                                                      {'class' => 'estimationAmount', 'disabled' => 'disabled'});

    my $btnEstimatationTrsp = $view->displayTextButton($locale->t('Estimer le transport'), 'calc.png',
                                                 'return false;',
                                                 $locale->t('Estimer le transport')
                                                 ,'l',
                                                 {'id' => $prefix . 'Btn',
                                                  'tabindex' => -1,
                                                  'onfocus' => 'this.blur();'}
                                                 );
    $view->addPageInput({
        'sel' => '#' . $prefix . 'Btn',
        'if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ') && this.style.visibility != "hidden"',
        'set' => 'this.onclick()'
    });

print '
        <td class="estimation">

            <fieldset class="sub">
                <legend>' . htmllocale('Estimation') . '</legend>

                <table class="formTable">
                <tbody>
                    <tr>
                        <td colspan="4" class="label right">' . htmllocale('Tarif de base') . '</td>
                        <td class="amount">' . $baseAmountInput . '</td>
                    </tr>
                    <tr>
                        <td colspan="5"><hr class="dotted"/></td>
                    </tr>
                    <tr>
                        <td class="label"><label for="' . $prefix . 'StdReduc">' . htmllocale('Remise standard') . '</label></td>
                        <td class="right">' .
                            sprintf($reductionLabel, $stdReducInput, $stdReducAmountInput) .
                            $view->displayJSBlock(
                                  'Location.spinControlsManager.createSpinControl("' . $prefix . 'StdReduc", 0, 100, 1, 10, 0);') . '
                        </td>
                        <td rowspan="5" class="totalReductionSeparator"></td>
                        <td rowspan="5" colspan="2" class="totalReduction">
                            <label for="' . $prefix . 'Reduc" class="totalReduction">' .
                            htmllocale('soit une réduction de') .
                           '</label><br />' .
                            sprintf($reductionLabel, $reducInput, $reducAmountInput) .
                            $view->displayJSBlock(
                                     'Location.spinControlsManager.createSpinControl("' . $prefix . 'Reduc", 0, 100, 1, 10, 0);') . '
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="text-align: right; font-size: 7pt;">' . sprintf($recoMaxiLabel, $recoPcInput, $maxiPcInput, $maxiAmountInput) . '</td>
                    </tr>
                    <tr>
                        <td colspan="2"><hr class="dotted"/></td>
                    </tr>
                    <tr>
                        <td class="label"><label for="' . $prefix . 'SpcReduc">' . htmllocale('Remise exceptionnelle') . '
                                          </label>
                        </td>
                        <td class="right">' .
                            sprintf($reductionLabel, $excReducInput, $excReducAmountInput) .
                            $view->displayJSBlock(
                                  'Location.spinControlsManager.createSpinControl("' . $prefix . 'SpcReduc", 0, 100, 1, 10, 0);') . '
                        </td>
                    </tr>
                    <tr>
                        <td class="status">
                            <div class="spcReducStatus" id="' . $prefix . 'ReducStatus">
                                <div state="REM01"> ' . $view->displayState('REM01', $locale->t('Validée')) . ' </div>
                                <div state="REM02"> ' . $view->displayState('REM02', $locale->t('En attente')) . ' </div>
                                <div state="REM03"> ' . $view->displayState('REM03', $locale->t('Refusée')) . ' </div>
                            </div>
                        </td>
                        <td colspan="2"></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="5"><hr class="dotted"/></td>
                    </tr>
                    <tr>
                        <td>' . $btnEstimatationTrsp . '</td>
                        <td colspan="3" class="label right">
                            <label for="' . $prefix . 'Amount">' . htmllocale('Estimation transport') . $requiredIcon . '</label>
                        </td>
                        <td class="amount">' . $amountInput . '</td>
                    </tr>
                    <tr>
                        <td colspan="5"><div class="warning" id="' . $prefix . 'ValidateEstimation">' . htmllocale('Veuillez valider pour prendre en compte l\'estimation') . '</div></td>
                    </tr>
                </tbody>
                </table>
            </fieldset>

        </td>';
}


## - Préparation du transport

# -- KO/Info sur les transports

my $errorsIcons = '';
foreach my $tabErrorTypeInfos (@$tabErrorTypes)
{
    $errorsIcons .= '<div class="infotip errorTip ' . $tabErrorTypeInfos->{'code'} . '">' .
                        '<div class="errorIcon ' . $tabErrorTypeInfos->{'code'} . '"></div>' .
                        '<div class="tip pick"></div>' .
                        '<div class="tip text">' . $view->toHTMLEntities($tabErrorTypeInfos->{'title'}) . '</div>' .
                    '</div>';
}



# -- Boutons et champs --
# - Livraison
my $d_pfx = $prefix . 'delivery';

my $deliveryDoBtn = '<a href="#" id="' . $d_pfx . 'DoBtn" class="btn do" title="' . htmllocale('Réaliser la livraison') . '"></a>';
my $deliveryUndoBtn = '<a href="#" id="' . $d_pfx . 'UndoBtn" class="btn undo" title="' . htmllocale('Annuler la réalisation de la livraison') . '"></a>';
my $deliveryResetBtn = '<a href="#" id="' . $d_pfx . 'ResetBtn" class="btn reset" title="' . htmllocale('Rétablir') . '"></a>';
my $deliveryGeneralResetBtn = '<a href="#" id="' . $d_pfx . 'GeneralResetBtn" class="btn reset" title="' . htmllocale('Rétablir') . '"></a>';
my $deliveryEditBtn = $view->displayTextButton($locale->t('Modifier'), $currentDir . 'transport/pen(|Over).png',
                                               'return false;',
                                               $locale->t('Modifier le transport')
                                               ,'l',
                                               {'id' => $d_pfx . 'EditBtn', 'tabindex' => -1, 'onfocus' => 'this.blur();'});

my $deliveryAmountInput = $view->displayFormInputCurrency($d_pfx . 'Amount', 0,
                                                          LOC::Html::CURRENCYMODE_TEXT,
                                                          $currencySymbol, undef,
                                                          {'class' => 'transportAmount'});

my $deliverySelfFormBtns = '<div class="btn selfform">' .
                               $view->displayTextButton('', $currentDir . 'transport/selfform.png',
                                                 'return false;',
                                                 $locale->t('Bordereau d\'enlèvement')
                                                 ,'l',
                                                 {'id' => $r_pfx . 'ViewSelfFormBtn',
                                                  'tabindex' => -1,
                                                  'onfocus' => 'this.blur();'}
                                                 ) .
                               $view->displayPrintButtons($d_pfx . 'SelfFormBtn', '', '', '', '', '', '',
                                                          {'buttons' => ['mail', 'pdf', 'print']}) .
                           '</div>';

my $deliveryMachineInventoryBtn = '<div class="btn machineinventory">' .
                                      $view->displayTextButton('', $currentDir . 'documents/machineInventory.png',
                                                 'return false;',
                                                 $locale->t('État des lieux')
                                                 ,'l',
                                                 {'id' => $d_pfx . 'ViewMachineInventoryBtn',
                                                  'tabindex' => -1,
                                                  'onfocus' => 'this.blur();'}
                                                 ) .
                                  '</div>';


# -Récupération
my $r_pfx = $prefix . 'recovery';

my $recoveryDoBtn = '<a href="#" id="' . $r_pfx . 'DoBtn" class="btn do" title="' . htmllocale('Réaliser la récupération') . '"></a>';
my $recoveryUndoBtn = '<a href="#" id="' . $r_pfx . 'UndoBtn" class="btn undo" title="' . htmllocale('Annuler la réalisation de la récupération') . '"></a>';
my $recoveryResetBtn = '<a href="#" id="' . $r_pfx . 'ResetBtn" class="btn reset" title="' . htmllocale('Rétablir') . '"></a>';
my $recoveryGeneralResetBtn = '<a href="#" id="' . $r_pfx . 'GeneralResetBtn" class="btn reset" title="' . htmllocale('Rétablir') . '"></a>';
my $recoveryEditBtn = $view->displayTextButton($locale->t('Modifier'), $currentDir . 'transport/pen(|Over).png',
                                               'return false;',
                                               $locale->t('Modifier le transport')
                                               ,'l',
                                               {'id' => $r_pfx . 'EditBtn', 'tabindex' => -1, 'onfocus' => 'this.blur();'});

my $recoveryAmountInput = $view->displayFormInputCurrency($r_pfx . 'Amount', 0,
                                                          LOC::Html::CURRENCYMODE_TEXT,
                                                          $currencySymbol, undef,
                                                          {'class' => 'transportAmount'});

my $recoverySelfFormBtns = '<div class="btn selfform">' .
                               '<label></label>' .
                               $view->displayTextButton('', $currentDir . 'transport/selfform.png',
                                                 'return false;',
                                                 $locale->t('Bordereau de restitution')
                                                 ,'l',
                                                 {'id' => $r_pfx . 'ViewSelfFormBtn',
                                                  'tabindex' => -1,
                                                  'onfocus' => 'this.blur();'}
                                                 ) .
                               $view->displayPrintButtons($r_pfx . 'SelfFormBtn', '', '', '', '', '', '',
                                                          {'buttons' => ['mail', 'pdf', 'print']}) .
                           '</div>';


my $recoveryMachineInventoryBtn = '<div class="btn machineinventory">' .
                                      '<label></label>' .
                                      $view->displayTextButton('', $currentDir . 'documents/machineInventory.png',
                                                 'return false;',
                                                 $locale->t('État des lieux')
                                                 ,'l',
                                                 {'id' => $r_pfx . 'ViewMachineInventoryBtn',
                                                  'tabindex' => -1,
                                                  'onfocus' => 'this.blur();'}
                                                 ) .
                                  '</div>';


print '
        <td>
            <div id="' . $prefix . 'main">
                <fieldset class="sub delivery">
                    <legend>' . htmllocale('Livraison') . $deliveryGeneralResetBtn . ' </legend>
                    <div class="fromto-label">' . htmllocale('De') . '</div>
                    <div class="fromto-content">
                        <span class="agency">' . $locale->t('Agence de %s', '<span id="' . $d_pfx . 'Agency"></span>') . '</span>
                        <a class="site" href="#" target="_blank" id="' . $d_pfx . 'Site"></a>
                        <div class="infotip">
                            <div class="icon"></div>
                            <div class="tip pick"></div>
                            <div class="tip text" id="' . $d_pfx . 'SiteInfo"></div>
                        </div>
                    </div>
                    <div class="separator"><hr class="dotted" /></div>
                    <div class="addMachine">
                        <div class="main" title="' . htmllocale('Machine principale') . '"></div>
                        <div class="add" title="' . htmllocale('Machine supplémentaire') .'">
                            <span class="withSelection">' . htmllocale('de') . '</span>
                            <a href="#" target="_blank" id="' . $d_pfx . 'AddMachLnkCtt"></a>';
if ($tabViewData{'documentType'} eq 'contract')
{
    print '                 <span class="withoutSelection">' . htmllocale('Pas de contrat principal sélectionné') . '</span>';
}
print '
                        </div>
                    </div>
                    <div class="total">
                        <div class="button">' . $deliveryEditBtn . ' </div>
                        <div class="amount">' . $deliveryAmountInput . '</div>
                    </div>
                </fieldset>
                <fieldset class="sub recovery">
                    <legend>' . htmllocale('Récupération') . $recoveryGeneralResetBtn . '</legend>
                    <div class="fromto-label">' . htmllocale('Vers') . '</div>
                    <div class="fromto-content">
                        <span class="agency">' . $locale->t('Agence de %s', '<span id="' . $r_pfx . 'Agency"></span>') . '</span>
                        <a class="site" href="#" target="_blank" id="' . $r_pfx . 'Site"></a>
                        <div class="infotip">
                            <div class="icon"></div>
                            <div class="tip pick"></div>
                            <div class="tip text" id="' . $r_pfx . 'SiteInfo"></div>
                        </div>
                    </div>
                    <div class="separator"><hr class="dotted" /></div>
                    <div class="addMachine">
                        <div class="main" title="' . htmllocale('Machine principale') . '"></div>
                        <div class="add" title="' . htmllocale('Machine supplémentaire') .'">
                            <span class="withSelection">' . htmllocale('de') . '</span>
                            <a href="#" target="_blank" id="' . $r_pfx . 'AddMachLnkCtt"></a>';
if ($tabViewData{'documentType'} eq 'contract')
{
    print '                 <span class="withoutSelection">' . htmllocale('Pas de contrat principal sélectionné') . '</span>';
}
print '
                        </div>
                    </div>
                    <div class="total">
                        <div class="button">' . $recoveryEditBtn . '</div>
                        <div class="amount">' . $recoveryAmountInput . '</div>
                    </div>
                </fieldset>

                <div class="workflow">
                    <canvas id="' . $prefix . 'workflow" width="332px" height="122px"></canvas>
                    <div class="cplmt delivery">
                        ' . $deliveryDoBtn . $deliveryUndoBtn . $deliveryResetBtn . '
                        <div class="doc-btns">
                            <label>' . htmllocale('Documents liés : ') . '</label>
                            ' . $deliveryMachineInventoryBtn . $deliverySelfFormBtns . '
                        </div>
                        <div class="errors">' . $errorsIcons . '</div>
                        <div class="infotip order">
                            <div class="icon"></div>
                            <div class="tip pick"></div>
                            <div class="tip text" id="' . $d_pfx . 'OrderInfo"></div>
                        </div>
                        <div class="no-sync" title="' . htmllocale('Non synchronisée avec OTD') . '"></div>
                    </div>
                    <div class="cplmt recovery">
                        ' . $recoveryDoBtn . $recoveryUndoBtn . $recoveryResetBtn . '
                        <div class="doc-btns">
                            <label>' . htmllocale('Documents liés : ') . '</label>
                            ' . $recoveryMachineInventoryBtn . $recoverySelfFormBtns . '
                        </div>
                        <div class="errors">' . $errorsIcons . '</div>
                        <div class="infotip order">
                            <div class="icon"></div>
                            <div class="tip pick"></div>
                            <div class="tip text" id="' . $r_pfx . 'OrderInfo"></div>
                        </div>
                        <div class="no-sync" title="' . htmllocale('Non synchronisée avec OTD') . '"></div>
                    </div>
                </div>';


# popup confirmation réalisation transport
print '
    <div style="display: none;" id="' . $prefix . 'doUndoConfirmPopup" class="confirmModal">
        <ul class="choices">
            <li class="current"><div class="icon"></div><div class="text"></div><div class="ctrls"><button type="button"><span>' . htmllocale('Choisir') . '</span></button></div></li>
            <li class="linked"><div class="icon"></div><div class="text"></div><div class="ctrls"><button type="button"><span>' . htmllocale('Choisir') . '</span></button></div></li>
            <li class="none"><div class="icon"></div><div class="text">' . htmllocale('Ne rien faire') . '</div><div class="ctrls"><button type="button"><span>' . htmllocale('Choisir') . '</span></button></div></li>
        </ul>
    </div>';



###########################
# Affichage de la popup
###########################

my $popupPrefix = $prefix . 'popup_';

my $popupAmountInput = $view->displayFormInputCurrency($popupPrefix . 'amount', 0,
                                                          LOC::Html::CURRENCYMODE_POSITIVEINPUT,
                                                          $currencySymbol, undef,
                                                          {'disabled' => 'disabled'});

my $isAddMachineCheckbox = $view->displayFormInputCheckBox($popupPrefix . 'addMachine', 0, {'id' => $popupPrefix . 'addMachine', 'disabled' => 'disabled'}, $locale->t('Machine supplémentaire'), 0);

my $addMachineContractSelect = $view->displayFormSelect($popupPrefix . 'addMachineContractId', {}, 0);
my $contractsListUrl = $view->createURL('default:contract:jsonList', {'format' => 0, 'currentContract' => $tabContractInfos->{'id'}});
$addMachineContractSelect .= $view->displayJSBlock('
var addMachineSearchBoxObj = Location.searchBoxesManager.createSearchBox("' . $popupPrefix . 'addMachineContractId", {isRequired: false, url: "' . $contractsListUrl . '"});
addMachineSearchBoxObj.createElement = function(key, value)
{
    var opt = new Option(value["code"], value["id"]);
    opt.setAttribute("data-url", value["url"]);
    return opt;
};');

my $addMachineHelp = $view->displayHelp($locale->t('Le forfait machine supplémentaire minimum est de %s',
                             $locale->getCurrencyFormat($tabViewData{'addMachineMinPrice'}, undef, $currencySymbol)));


my $isSelfCheckbox = $view->displayFormInputCheckBox($popupPrefix . 'self', 0, {'id' => $popupPrefix . 'self', 'disabled' => 'disabled'}, $locale->t('Enlèvement'), 0);
my $isSelfAgency   = $view->displayFormInputRadio($popupPrefix . 'selfType', 'agency', 0, {'id' => $popupPrefix . 'selfAgency', 'disabled' => 'disabled'}, $locale->t('Agence'));
my $isSelfSite     = $view->displayFormInputRadio($popupPrefix . 'selfType', 'site', 0, {'id' => $popupPrefix . 'selfSite', 'disabled' => 'disabled'}, $locale->t('Reprise sur chantier'));
my $isSelfHelp     = $view->displayHelp($locale->t('Choisissez un contrat'));

my $isTransferCheckbox = $view->displayFormInputCheckBox($popupPrefix . 'transfer', 0, {'id' => $popupPrefix . 'transfer', 'disabled' => 'disabled'}, $locale->t('Transfert inter-chantiers'), 0);
my $isTransferHelp     = $view->displayHelp($locale->t('Choisissez un contrat'));

my $isDelegatedAgcCheckbox  = $view->displayFormInputCheckBox($popupPrefix . 'delegate', 0, {'id' => $popupPrefix . 'delegate', 'disabled' => 'disabled'}, $locale->t('Déléguer le transport à'), 0);
my $isTransferToAgcCheckbox = $view->displayFormInputCheckBox($popupPrefix . 'transferToAgency', 0, {'id' => $popupPrefix . 'transferToAgency', 'disabled' => 'disabled'}, $locale->t('La machine est transférée à'), 0);
my @tabAgenciesForDelegation = ({'value' => '', 'innerHTML' => '-- ' . $locale->t('Sélectionner') . ' --'});
my @tabAgenciesForTransfer = ({'value' => '', 'innerHTML' => '-- ' . $locale->t('Sélectionner') . ' --'});
foreach my $agcId (keys(%{$tabViewData{'tabDocumentInfos'}->{'tabAgencies'}}))
{
    if ($agcId ne $tabViewData{'tabDocumentInfos'}->{'agency.id'})
    {
        push(@tabAgenciesForTransfer, {'value' => $agcId, 'innerHTML' => $tabViewData{'tabDocumentInfos'}->{'tabAgencies'}->{$agcId}});
    }
    push(@tabAgenciesForDelegation, {'value' => $agcId, 'innerHTML' => $tabViewData{'tabDocumentInfos'}->{'tabAgencies'}->{$agcId}});
}
my $delegatedAgcSelect     = $view->displayFormSelect($popupPrefix . 'delegatedAgencyId', \@tabAgenciesForDelegation, 0, undef, {'disabled' => 'disabled'});
my $transferToAgcSelect    = $view->displayFormSelect($popupPrefix . 'transferToAgencyId', \@tabAgenciesForTransfer, 0, undef, {'disabled' => 'disabled'});

# Coche "ne pas synchroniser avec OTD"
my $isSynchroDisabled;
my $isSynchroDisabledHelp;
if ($tabViewData{'documentType'} eq 'contract' &&
    $tabViewData{'tabContractInfos'}->{'tabRights'}->{'transport'}->{'synchro'})
{
    $isSynchroDisabled = $view->displayFormInputCheckBox($popupPrefix . 'synchroDisabled', 0, {'id' => $popupPrefix . 'synchroDisabled', 'disabled' => 'disabled'}, $locale->t('Ne pas synchroniser avec OTD'), 0);

    $isSynchroDisabledHelp = $view->displayHelp($locale->t('À l\'enregistrement du contrat, si cette case a été activée, la commande de transport sera envoyée une dernière fois à OTD. Toute modification ultérieure ne sera pas transmise à OTD.'));
}


my $popupEditBtn = $view->displayTextButton($locale->t('Modifier'), $currentDir . 'transport/pen(|Over).png',
                                                 'return false;',
                                                 $locale->t('Modifier')
                                                 ,'l',
                                                 {'id' => $popupPrefix . 'validBtn',
                                                  'tabindex' => -1,
                                                  'onfocus' => 'this.blur();'}
                                                 );


# Légende
my $legendHtml = '<div class="legend">' .
                    '<span class="title">' . htmllocale('Légende') . '&nbsp;:</span>';

foreach my $tabInfosCpl (@$tabInfosClpTypes)
{
    $legendHtml .= '<div class="infotip errorTip ' . $tabInfosCpl->{'code'} . '">' .
                        '<div class="infosCplIcon ' . $tabInfosCpl->{'code'} . '"></div>' .
                        '<div class="tip pick"></div>' .
                        '<div class="tip text">' . $view->toHTMLEntities($tabInfosCpl->{'title'}) . '</div>' .
                    '</div>';
}
$legendHtml .= $view->displayImage('base/controlPanels/sep.png', {'style' => 'vertical-align: top; padding: 0 5px;'});

foreach my $tabErrorTypeInfos (@$tabErrorTypes)
{
    my $text = ($tabErrorTypeInfos->{'isFatalError'} ? '<b>' . $locale->t('Erreur bloquante') . '&nbsp;:</b>&nbsp;' : '') .
               $view->toHTMLEntities($tabErrorTypeInfos->{'title'});
    $legendHtml .= '<div class="infotip errorTip ' . $tabErrorTypeInfos->{'code'} . '">' .
                        '<div class="errorIcon ' . $tabErrorTypeInfos->{'code'} . '"></div>' .
                        '<div class="tip pick"></div>' .
                        '<div class="tip text">' . $text . '</div>' .
                   '</div>';
}

$legendHtml .= $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png',
                                        'return false;',
                                        $locale->t('Actualiser la liste')
                                        ,'r',
                                        {'id' => $popupPrefix . 'refreshBtn', 'tabindex' => -1, 'onfocus' => 'this.blur();'}
                                        );

$legendHtml .= '</div>';

# Popup de modification de la livraison et de la récupération
print '
    <div id="' . $popupPrefix . 'modal" style="display: none;">
        <fieldset class="sub content ' . $prefix . 'content">
            <legend>
                <div class="delivery">' . htmllocale('Modification de la livraison') . '</div>
                <div class="recovery">' . htmllocale('Modification de la récupération') . '</div>
            </legend>

            <table class="formTable">
            <tbody>
                <tr>
                    <td class="label" style="width: 100%; text-align: right; padding-right: 20px;">' . htmllocale('Montant') . '</td>
                    <td class="content" style="text-align: right;">' . $popupAmountInput . '</td>
                </tr>
            </tbody>
            </table>

            <hr class="dotted" />

            <table class="formTable">
            <tbody>
                <tr>
                    <td class="label">' . $isAddMachineCheckbox . '&nbsp;' . $addMachineHelp . '</td>';
if ($tabViewData{'documentType'} eq 'contract')
{
    print'
                    <td class="content">' . $addMachineContractSelect .
                                '<a href="#" onclick="return false;" id="' . $popupPrefix . 'addMachineClear">' . $view->displayImage('clear.gif', $locale->t('Effacer la recherche')) . '</a>' .
                                '&nbsp;&nbsp;<a href="#" id="' . $popupPrefix . 'viewContractAddMachine" target="_blank" title="' . htmllocale('Voir le contrat') . '">' . $view->displayImage('viewOver.png') . '</a>' .
                   '</td>';
}
print '
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td class="label">' . $isSelfCheckbox . '</td>
                    <td class="content">' . $isSelfAgency;
if ($tabViewData{'documentType'} eq 'contract')
{
    print '
                                            <br />' . $isSelfSite . '&nbsp;' . $isSelfHelp .'</td>';
}
print '
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>';
if ($tabViewData{'documentType'} eq 'contract')
{
    print '
                <tr>
                    <td colspan="2" class="label">' . $isTransferCheckbox . '&nbsp;' . $isTransferHelp . '</td>
                </tr>';

    my $msgVisibility = $locale->t('La machine %s ayant été rendue visible sur %s, seuls les contrats de %s sont disponibles.',
                                        '<span id="' . $popupPrefix . 'msgParkNumber"></span>',
                                        '<span id="' . $popupPrefix . 'msgVisibleOnAgencyId1"></span>',
                                        '<span id="' . $popupPrefix . 'msgVisibleOnAgencyId2"></span>');

    print '
                <tr id="' . $popupPrefix . 'cttsRow">
                    <td colspan="2">
                        <p class="text">' . htmllocale('Choix pour') . ' "<span class="self">' . htmllocale('Reprise sur chantier') . '</span>" / "<span class="std">' . htmllocale('Transfert inter-chantiers') . '</span>"</p>
                        <div class="msgVisibility">
                            ' . $view->displayMessages('warning', [$msgVisibility], 0) . '
                        </div>
                        <div class="content">
                            ' . $legendHtml . '
                            <table class="basic" id="' . $popupPrefix . 'ctts">
                                <thead>
                                    <tr>
                                        <th class="check"></th>
                                        <th class="infosCpl"></th>
                                        <th class="code">' . htmllocale('Code contrat') . '</th>
                                        <th class="site">' . htmllocale('Chantier') . '</th>
                                        <th class="parkNumber">' . htmllocale('N° Parc') . '</th>
                                        <th class="date">
                                            <span id="sided">' . htmllocale('Récupération') . '</span>
                                            <span id="sider">' . htmllocale('Livraison') . '</span>
                                        </th>
                                        <th class="errors"></th>
                                    </tr>
                                </thead>
                                <tbody class="rows">
                                </tbody>
                                <tbody class="msgs">
                                    <tr class="noResult">
                                        <td colspan="7">' . htmllocale('Aucun contrat disponible') . '</td>
                                    </tr>
                                    <tr class="loading">
                                        <td colspan="7">' . $view->displayImage('loading2.gif') . '</td>
                                    </tr>
                                    <tr class="error">
                                        <td colspan="7">' . $view->displayImage('iconError.gif') . '</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="msg">
                                ' . $view->displayMessages('warning', [$locale->t('Le montant du contrat sélectionné peut être impacté par cette action.')], 0) . '
                            </div>
                        </div>
                    </td>
                </tr>';
}
print '
                <tr>
                    <td class="label">' . $isDelegatedAgcCheckbox . '</td>
                    <td class="content">' . $delegatedAgcSelect .'</td>
                </tr>';
if ($tabViewData{'documentType'} eq 'contract')
{
    print '
                <tr id="' . $popupPrefix . 'tsfToAgc">
                    <td class="label">' . $isTransferToAgcCheckbox . '</td>
                    <td class="content">' . $transferToAgcSelect .'</td>
                </tr>';
}
if ($isSynchroDisabled)
{
    print '
                <tr>
                    <td colspan="2"><hr class="dotted" /></td>
                </tr>
                <tr id="' . $popupPrefix . 'noSync">
                    <td class="label">' . $isSynchroDisabled . '&nbsp;' . $isSynchroDisabledHelp . '</td>
                    <td class="content"></td>
                </tr>';
}
print '
            </table>
        </fieldset>
        <fieldset class="sub ctrls">
            ' . $popupEditBtn . '
        </fieldset>
    </div>';


print '
                <div style="clear: both;"></div>
            </div>
        </td>
    </tr>
</tbody>
</table>

</div>';


1;
