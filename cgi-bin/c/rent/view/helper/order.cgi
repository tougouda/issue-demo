use utf8;
use open (':encoding(UTF-8)');

# File: order.cgi
#
# Affichage du bloc commande sur le devis et le contrat.
# Le fichier a besoin d'une table de hachage appelée $tabViewData contenant les clés suivantes :
# - tabCustomer : référence à une table de hachage contenant les informations à afficher.
#
# tabCustomer contient les clés suivantes :
# - id : identifiant du client ;
# - country.id : identifiant du pays ;
# - orderNo : numéro de commande ;
# - orderContact.fullName : nom complet du contact commande ;
# - orderContact.telephone : numéro de téléphone du contact commande.


# Données du client
my $tabCustInfos = ($tabViewData{'documentType'} eq 'contract' ? $tabViewData{'tabContractInfos'}->{'tabCustomer'} : $tabViewData{'tabEstimateInfos'}->{'tabCustomer'});


my $orderNoInput = $view->displayFormInputText('orderNo', $tabCustInfos->{'orderNo'}, '',
                                               {'id' => 'orderNo', 'size' => 25, 'maxlength' => 25});
$view->addPageInput({'sel' => '#orderNo'});


my $ordCtcFullnameInput = $view->displayFormInputText(
                                                      'orderContact.fullName',
                                                      '',
                                                      '',
                                                      {'id' => 'orderContact.fullName',
                                                       'size' => 40, 'maxlength' => 40}
                                                     );
$view->addPageInput({'sel' => '#orderContact\.fullName'});

my $ordCtcTelInput = $view->displayFormInputPhone(
                                                  'orderContact.telephone',
                                                  $tabCustInfos->{'orderContact.telephone'}
                                                 );
$view->addPageInput({'sel' => '#orderContact\.telephone\.code'});
$view->addPageInput({'sel' => '#orderContact\.telephone\.masked'});

my $ordCtcEmailInput = $view->displayFormInputText('orderContact.email', 
                                                      '', '',
                                                      {'size' => 40});
$view->addPageInput({'sel' => '#orderContact\.email'});
$ordCtcEmailInput .= '
    <a href="#" id="orderContact.email.send" class="hidden" mytitle="' . htmllocale('Envoyer un e-mail à %s') . '" title="">' .
        $view->displayImage('mail(|Over).png') .
   '</a>';

print '
<fieldset id="rentOrderFieldset">
    <legend>' . $locale->t('Commande') . '</legend>

    <div id="rentOrderBlock">
        <div class="content">';
        # N° de la commande et nom et téléphone du contact commande
print '
            <table class="formTable" style="margin-top: 5px;">
                <tr style="vertical-align: baseline;">
                    <td class="label"><label for="orderNo">' . htmllocale('N°') . '</label></td>
                    <td colspan="3">' . $orderNoInput . '</td>
                </tr>
                <tr style="vertical-align: baseline;">
                    <td class="label">' . $view->displayLabel('orderContact.fullName', $locale->t('Contact')) . '</td>
                    <td>' . $ordCtcFullnameInput . '</td>
                    <td class="label">' . $view->displayLabel('orderContact.telephone', $locale->t('Tél.')) . '</td>
                    <td>' . $ordCtcTelInput . '</td>
                </tr>
                <tr style="vertical-align: baseline;">
                    <td class="label">' . $view->displayLabel('orderContact.email', $locale->t('E-mail')) . '</td>
                    <td>' . $ordCtcEmailInput . '</td>
                    <td colspan="2"></td>
                </tr>
            </table>
        </div>
    </div>';


print '
</fieldset>';

1;
