use utf8;
use open (':encoding(UTF-8)');

# File: proforma.cgi
#
# Affichage du bloc pour les pro formas


# Traductions
$view->addTranslations({
    $prefix . 'state_active' => $locale->t('Active'),
    $prefix . 'state_replaced' => $locale->t('Périmée'),
    $prefix . 'state_aborted' => $locale->t('Abandonnée'),

    $prefix . 'expired' => $locale->t('Non à jour'),

    $prefix . 'action_edit' => $locale->t('Demande de modification'),
    $prefix . 'action_abort' => $locale->t('Demande d\'abandon'),
    $prefix . 'action_reactivate' => $locale->t('Demande de réactivation'),

    $prefix . 'button_view' => $locale->t('Visualisation'),
    $prefix . 'button_edit' => $locale->t('Modification'),
    $prefix . 'button_abort' => $locale->t('Abandon'),
    $prefix . 'button_reactivate' => $locale->t('Réactivation'),
    $prefix . 'button_undo' => $locale->t('Rétablir')
});


# Bouton d'ajout
my $addBtn = $view->displayTextButton($locale->t('Générer une pro forma'),
                                      'rent/helper/proforma/generate(|Over).png',
                                      'return false;',
                                      $locale->t('Générer une pro forma')
                                      ,'l',
                                      {'id' => $prefix . 'addPopupBtn',
                                       'tabindex' => -1,
                                       'onfocus' => 'this.blur();'});

# Affichage des causes de non possibilité de génération
my $labelForRentAmountTypeError = '';
if ($tabViewData{'documentType'} eq 'contract')
{
    $labelForRentAmountTypeError = $locale->t('Le type de montant du contrat est mensuel.');
}
else
{
    $labelForRentAmountTypeError = $locale->t('Le type de montant de la ligne de devis est mensuel.');
}
my $textInfoError = '
        <span class="generic">' . $locale->t('Impossible de générer une pro forma.') . '</span>
        <span class="repairServices">' . $locale->t('Des services de remise en état existent sur ce contrat.') . '</span>
        <span class="repairDays">' . $locale->t('Des jours de remise en état existent sur ce contrat.') . '</span>
        <span class="rentAmountType">' . $labelForRentAmountTypeError . '</span>
        <span class="genericExtension">' . $locale->t('Impossible de générer une pro forma de prolongation.') . '</span>';

my $infoError = $view->displayMessages('info', [$textInfoError], 0);

# Boutons d'impression
my $printBtns = $view->displayPrintButtons($prefix . 'printBtns', '', '', '', '', '', '',
                                           {'buttons' => ['mail', 'pdf', 'print']});


print '
<div title="' . htmllocale('Pro forma') . '">

    <div class="clonedCurrencies">
        ' . $view->displayFormInputCurrency($prefix . 'amount_clone', 0,
                                               LOC::Html::CURRENCYMODE_TEXT,
                                               $currencySymbol)
        . $view->displayFormInputCurrency($prefix . 'amountWithVat_clone', 0,
                                               LOC::Html::CURRENCYMODE_TEXT,
                                               $currencySymbol)
        . $view->displayFormInputCurrency($prefix . 'total_clone', 0,
                                               LOC::Html::CURRENCYMODE_TEXT,
                                               $currencySymbol)
        . $view->displayFormInputCurrency($prefix . 'cashed_clone', 0,
                                               LOC::Html::CURRENCYMODE_TEXT,
                                               $currencySymbol)
        . $view->displayFormInputCurrency($prefix . 'remainDue_clone', 0,
                                               LOC::Html::CURRENCYMODE_TEXT,
                                               $currencySymbol) . '
    </div>

<fieldset class="sub">
    <legend>' . htmllocale('Liste des documents') . '</legend>
    <table class="basic" id="' . $prefix . 'list">
    <thead>
        <tr>
            <td class="icon"><a id="' . $prefix . 'expandBtn" href="#" onclick="return false;" class="expand" title="' . htmllocale('Afficher ou non les pro formas périmées') . '"></a></td>
            <td class="id">' . htmllocale('N°') . '</td>
            <td class="type">' . htmllocale('Type') . '</td>
            <td class="paiement">' . htmllocale('Mode de règlement') . '</td>
            <td class="amount">' . htmllocale('Montant HT') . '</td>
            <td class="amountWithVat">' . htmllocale('Montant TTC') . '</td>
            <td class="total">' . htmllocale('Solde à payer') . '</td>
            <td class="cashed">' . htmllocale('Encaissé') . '</td>
            <td class="remainDue">' . htmllocale('Reste dû') . '</td>
            <td class="beginDate">' . htmllocale('Début de loc.') . '</td>
            <td class="justif"><span title="' . htmllocale('Justificatif') . '">' . htmllocale('Justif.') . '</span></td>
            <td class="validation"><span title="' . htmllocale('Validé par l\'agence') . '">' . htmllocale('Val. par ag.') . '</span></td>
            <td class="encashment">' . htmllocale('Encaissement') .'</td>
            <td class="ctrls"></td>
            <td class="editCtrls">' . $printBtns . '</td>
        </tr>
    </thead>
    <tfoot class="no-proforma">
        <tr>
            <td colspan="15">' . htmllocale('Aucune pro forma générée') . '</td>
        </tr>
    </tfoot>
    <tbody class="list">
    </tbody>
    <tbody class="new">
        <tr>
            <td class="icon" title="' . htmllocale('Demande d\'ajout') . '"><div></div></td>
            <td></td>
            <td class="type"><div></div></td>
            <td class="paiement"></td>
            <td colspan="9"></td>
            <td class="ctrls"><a class="delete" href="#" title="' . htmllocale('Ne pas générer cette pro forma') . '"></a></td>
            <td class="editCtrls"></td>
        </tr>
    </tbody>
    </table>

    ' . $addBtn . '
</fieldset>';

print '<div id="' . $prefix . 'infoError">' . $infoError . '</div>';

# Popup de génération
my @listType = ({'value' => 0, 'innerHTML' => '-- ' . $locale->t('Sélectionner') . ' --'});
foreach my $typeId (keys(%{$tabViewData{'tabProformaTypes'}}))
{
    push(@listType, {'value' => $typeId, 'innerHTML' => $tabViewData{'tabProformaTypes'}->{$typeId}});
}

my $typeInput = $view->displayFormSelect($prefix . 'addTypeId', \@listType);


my $paidAmountInput = $view->displayFormInputCurrency($prefix . 'addPaidAmt', 0,
                                                      LOC::Html::CURRENCYMODE_INPUT,
                                                      $currencySymbol, undef);

my $depositInput = $view->displayFormInputCurrency($prefix . 'addDeposit', 0,
                                                   LOC::Html::CURRENCYMODE_INPUT,
                                                   $currencySymbol, undef);

my $advanceAmountInput = $view->displayFormInputCurrency($prefix . 'addAdvanceAmt', 0,
                                                         LOC::Html::CURRENCYMODE_INPUT,
                                                         $currencySymbol, undef);

my @tabPaymentModes = ();
push(@tabPaymentModes, {'value' => 0, 'innerHTML' => '-- ' . $locale->t('Sélectionner') . ' --'});
while (my ($id, $name) = each(%{$tabViewData{'tabProformaPaymentModes'}}))
{
    push(@tabPaymentModes, {'value' => $id, 'innerHTML' => $name});
}
my $paymentModeInput = $view->displayFormSelect($prefix . 'addPmtModeId', \@tabPaymentModes);

my $endDateInput = undef;
if ($tabViewData{'documentType'} eq 'contract')
{
    $endDateInput = $view->displayFormInputDate($prefix . 'addEndDate', undef, undef, undef);
}

my $commentsInput = $view->displayFormTextArea($prefix . 'addComment', '');

my $fuelQtyInput = $view->displayFormInputText($prefix . 'addFuelQty', 0);

my $vatInput = $view->displayFormInputCheckBox($prefix . 'addIsVat', 0);


my $addBtn = $view->displayTextButton($locale->t('Générer'),
                                      'rent/helper/proforma/generate(|Over).png',
                                      'return false;',
                                      $locale->t('Générer')
                                      ,'l',
                                      {'id' => $prefix . 'addBtn',
                                       'tabindex' => -1,
                                       'onfocus' => 'this.blur();'});


print '
    <div class="proformaHlp_popup" style="display: none;" id="' . $prefix . 'addPopup">
        <fieldset class="sub content">
            <legend>' . htmllocale('Génération de la pro forma') . '</legend>

            <table class="formTable">
            <tbody>
                <tr class="type">
                    <td class="label">' . htmllocale('Type') . '</td>
                    <td class="content">' . $typeInput . '</td>
                </tr>
                <tr class="advance">
                    <td class="label">' . htmllocale('Montant de l\'acompte') . '</td>
                    <td class="content">' . $advanceAmountInput . '</td>
                </tr>
                <tr class="paidAmount">
                    <td class="label">' . htmllocale('Montant déjà versé') . '</td>
                    <td class="content">' . $paidAmountInput . ' ' . htmllocale('TTC') . '</td>
                </tr>
                <tr class="deposit">
                    <td class="label">' . htmllocale('Dépôt de garantie') . '</td>
                    <td class="content">' . $depositInput . '</td>
                </tr>
                <tr class="paymentMode">
                    <td class="label">' . htmllocale('Mode de règlement') . '</td>
                    <td class="content">' . $paymentModeInput . '</td>
                </tr>';
if (defined $endDateInput)
{
    print '
                <tr class="endDate">
                    <td class="label">' . htmllocale('Date de fin souhaitée') . '</td>
                    <td class="content">' . $endDateInput . '
                        <span class="info">' .
                            $locale->t('Date de fin actuelle') . ' : <span id="' . $prefix . 'addEndDateDoc"></span><br />
                            <span id="' . $prefix . 'addMaxDate">' . $locale->t('Date de fin max') . ' : <span id="' . $prefix . 'addMaxEndDate"></span></span>
                        </span>
                    </td>
                </tr>';
}
print '
                <tr class="fuelQty">
                    <td class="label">' . htmllocale('Estimation du carburant') . '</td>
                    <td class="content">' . $locale->t('%s L', $fuelQtyInput) .
                        $view->displayJSBlock('Location.spinControlsManager.createSpinControl("' . $prefix . 'addFuelQty", 0, null, 1, 10, 2);') .
                        ' <a id="' . $prefix . 'addCalc" href="#">' . $view->displayImage('calc.png', 'Calcul du carburant pour contrat cadre') . '</a><span id ="'
                        . $prefix . 'addWarningFuelEstimationExtension" style="visibility:hidden">' . $view->displayImage('iconWarning.gif', {'title' => htmllocale('Estimation carburant à saisir')}) . '</span>
                        <span class="info">' . $locale->t('%s L/jour', '<span id="' . $prefix . 'addFuelQtyEstimByDay"></span>') . ' (' . $locale->t('max %s L', '<span id="' . $prefix . 'addFuelQtyMax"></span>') . ')</span>
                    </td>
                </tr>
                <tr class="comment">
                    <td class="label">' . htmllocale('Commentaire') . '</td>
                    <td class="content">' . $commentsInput . '</td>
                </tr>
                <tr class="vat">
                    <td class="label">' . htmllocale('TVA facturée') . '</td>
                    <td class="content">' . $vatInput . '</td>
                </tr>
            </tbody>
            </table>

        </fieldset>

        <fieldset class="sub ctrls">
            ' . $addBtn . '
        </fieldset>
    </div>';



# Popup de modification
my $typeInput = '<span id="' . $prefix . 'editTypeLabel"></span>';

my $paidAmountInput = $view->displayFormInputCurrency($prefix . 'editPaidAmt', 0,
                                                      LOC::Html::CURRENCYMODE_INPUT,
                                                      $currencySymbol, undef);

my $depositInput = $view->displayFormInputCurrency($prefix . 'editDeposit', 0,
                                                   LOC::Html::CURRENCYMODE_INPUT,
                                                   $currencySymbol, undef);

my $advanceAmountInput = $view->displayFormInputCurrency($prefix . 'editAdvanceAmt', 0,
                                                         LOC::Html::CURRENCYMODE_INPUT,
                                                         $currencySymbol, undef);
my @tabPaymentModes = ();
while (my ($id, $name) = each(%{$tabViewData{'tabProformaPaymentModes'}}))
{
    push(@tabPaymentModes, {'value' => $id, 'innerHTML' => $name});
}
my $paymentModeInput = $view->displayFormSelect($prefix . 'editPmtModeId', \@tabPaymentModes);

my $endDateInput = undef;
if ($tabViewData{'documentType'} eq 'contract')
{
    $endDateInput = '<span id="' . $prefix . 'editEndDate"></span>';
}

my $commentsInput = $view->displayFormTextArea($prefix . 'editComment', '');

my $fuelQtyInput = $view->displayFormInputText($prefix . 'editFuelQty', 0);

my $vatInput = $view->displayFormInputCheckBox($prefix . 'editIsVat', 0);


my $editBtn = $view->displayTextButton($locale->t('Modifier'),
                                       'rent/helper/proforma/edit(|Over).png',
                                       'return false;',
                                       $locale->t('Modifier'),
                                       'l',
                                       {'id' => $prefix . 'editBtn',
                                        'tabindex' => -1,
                                        'onfocus' => 'this.blur();'});


print '
    <div class="proformaHlp_popup" style="display: none;" id="' . $prefix . 'editPopup">
        <fieldset class="sub content">
            <legend>' . htmllocale('Modification de la pro forma') . '</legend>

            <table class="formTable">
            <tbody>
                <tr class="type">
                    <td class="label">' . htmllocale('Type') . '</td>
                    <td class="content">' . $typeInput . '</td>
                </tr>
                <tr class="advance">
                    <td class="label">' . htmllocale('Montant de l\'acompte') . '</td>
                    <td class="content">' . $advanceAmountInput . '</td>
                </tr>
                <tr class="paidAmount">
                    <td class="label">' . htmllocale('Montant déjà versé') . '</td>
                    <td class="content">' . $paidAmountInput . ' ' . htmllocale('TTC') . '</td>
                </tr>
                <tr class="deposit">
                    <td class="label">' . htmllocale('Dépôt de garantie') . '</td>
                    <td class="content">' . $depositInput . '</td>
                </tr>
                <tr class="paymentMode">
                    <td class="label">' . htmllocale('Mode de règlement') . '</td>
                    <td class="content">' . $paymentModeInput . '</td>
                </tr>';
if (defined $endDateInput)
{
    print '
                <tr class="endDate">
                    <td class="label">' . htmllocale('Date de fin souhaitée') . '</td>
                    <td class="content">' . $endDateInput . '</td>
                </tr>';
}
print '
                <tr class="fuelQty">
                    <td class="label">' . htmllocale('Estimation du carburant') . '</td>
                    <td class="content">' . $locale->t('%s L', $fuelQtyInput) .
                    $view->displayJSBlock('Location.spinControlsManager.createSpinControl("' . $prefix . 'editFuelQty", 0, null, 1, 10, 2);') .
                    ' <a id="' . $prefix . 'editCalc" href="#">' . $view->displayImage('calc.png', 'Calcul du carburant pour contrat cadre') . '</a>
                    <span class="info">' . $locale->t('%s L/jour', '<span id="' . $prefix . 'editFuelQtyEstimByDay"></span>') . ' (' . $locale->t('max %s L', '<span id="' . $prefix . 'editFuelQtyMax"></span>') . ')</span></td>
                </tr>
                <tr class="comment">
                    <td class="label">' . htmllocale('Commentaire') . '</td>
                    <td class="content">' . $commentsInput . '</td>
                </tr>
                <tr class="vat">
                    <td class="label">' . htmllocale('TVA facturée') . '</td>
                    <td class="content">' . $vatInput . '</td>
                </tr>
                <tr class="justification">
                    <td colspan="2"><hr /></td>
                </tr>
                <tr class="justification">
                    <td class="label">' . htmllocale('Justificatif') . '</td>
                    <td class="content"><div id="' . $prefix . 'editJustification">' . $view->displayBoolean(0) . $view->displayBoolean(1) . '</div></td>
                </tr>
                <tr class="validation">
                    <td class="label">' . htmllocale('Validé par l\'agence') . '</td>
                    <td class="content"><div id="' . $prefix . 'editValidation">' . $view->displayBoolean(0) . $view->displayBoolean(1) . '</div>' .
                                        $view->displayTooltipIcon('comment', '-', {'id' => $prefix . 'editValidComment'}) . '</td>
                </tr>
                <tr class="encashment">
                    <td class="label">' . htmllocale('Encaissement') . '</td>
                    <td class="content"><div id="' . $prefix . 'editEncashment"></div></td>
                </tr>
            </tbody>
            </table>

        </fieldset>

        <fieldset class="sub ctrls">
            ' . $editBtn . '
        </fieldset>
    </div>

</div>';


1;
