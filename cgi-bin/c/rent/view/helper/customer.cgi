use utf8;
use open (':encoding(UTF-8)');

# File: customer.cgi
#
# Affichage du bloc client sur le devis et le contrat.
# Le fichier a besoin d'une table de hachage appelée $tabViewData contenant les clés suivantes :
# - tabCustomer : référence à une table de hachage contenant les informations à afficher.
#
# tabCustomer contient les clés suivantes :
# - id : identifiant du client ;
# - country.id : identifiant du pays ;
# - orderNo : numéro de commande ;
# - orderContact.fullName : nom complet du contact commande ;
# - orderContact.telephone : numéro de téléphone du contact commande.


# Données du client
my $tabCustInfos = ($tabViewData{'documentType'} eq 'contract' ? $tabViewData{'tabContractInfos'}->{'tabCustomer'} : $tabViewData{'tabEstimateInfos'}->{'tabCustomer'});


# Symbole champ requis
my $requiredIcon = $view->displayRequiredSymbol();

# Traductions
$view->addTranslations({
    'customer_temp_unlock'      => $locale->t('Client débloqué temporairement'),
    'customer_hasAreaExemption' => $locale->t('Dérogation RR en cours (<%refAgencyId>)')
});

# Choix du client
my $tabCustomersList = {};
if ($tabCustInfos->{'id'} != 0)
{
    $tabCustomersList = {$tabCustInfos->{'id'} => $tabCustInfos->{'name'}};
}
my $custInput = $view->displayFormSelect('customer.id', $tabCustomersList, 0, 0);

$view->addPageInput({
    'sel' => '#customer\.id\.search, #customer\.id',
    'get' => 'Location.searchBoxesManager.getSearchBox("customer.id")'
});


my $refreshBtn = '<a href="#" id="customer.refreshBtn" ' .
                    'title="' . htmllocale('Recharger les données') . '">' .
                    $view->displayImage('refresh(|Over).png') .
                 '</a>';


# Déclaration de la popup de création de client
my $newCustPopupJS = 'Location.popupsManager.createPopup("newCustomer", "", "center", "center", 700, 670);';
print $view->displayJSBlock($newCustPopupJS);

my $newCustUrl = &LOC::Globals::get('locationUrl') . '/cgi-bin/old/location/nvclient.cgi?type=devis&menu=nvclient';
my $newCustBtn = $view->displayTextButton(
                                      $locale->t('Nouveau'), 'common/customer/add(|Over).png',
                                      'Location.popupsManager.getPopup("newCustomer").open("' . $newCustUrl . '");',
                                      $locale->t('Créer un nouveau client'),
                                      'l',
                                      {'id' => 'customer.newBtn',
                                       'tabindex' => -1,
                                       'onfocus' => 'this.blur();'}
                                     );

my $customerHistoryBtn = $view->displayTextButton(
                                      $locale->t('Historique'), 'rent/helper/customer/history(|Over).png',
                                      'return true;',
                                      $locale->t('Historique client'),
                                      'l',
                                      {'id' => 'customer.historyBtn',
                                       'tabindex' => -1,
                                       'onfocus' => 'this.blur();',
                                       'target' => '_blank'}
                                     );

my $appCRMBtn = $view->displayTextButton(
                                      $locale->t('Fiche CRM'), 'rent/helper/customer/crm(|Over).png',
                                      'return true;',
                                      $locale->t('Fiche client CRM'),
                                      'l',
                                      {'id' => 'customer.appCRMBtn',
                                       'tabindex' => -1,
                                       'onfocus' => 'this.blur();',
                                       'target' => '_blank'}
                                     );

# Bouton demande de déblocage
my $unlockingTempPopupBtn = $view->displayTextButton($locale->t('Déblocage'),
                                      'rent/helper/customer/unlock(|Over).png',
                                      'return false;',
                                      $locale->t('Demander le déblocage du client')
                                      ,'l',
                                      {'id' => 'customer.unlockingTempPopupBtn',
                                       'tabindex' => -1,
                                       'onfocus' => 'this.blur();'});


# Code, niveau de blocage et bouton nouveau client
my $customerLink = '<span id="customer.code.none" class="hidden">[' . htmllocale('Aucun') . ']</span>';
$customerLink .= '<a href="#" id="customer.code" class="visible" target="_blank"></a>';
my $customerNewAccount =
                         '<span id="customer.accountOpening" class="hidden">' .
                              htmllocale('Ouverture de compte client à adresser') .
                         '</span>';

my $customerState = '';
for (my $i = 0; $i <= 4; $i++)
{
    $customerState .= '<span id="customerLockedLevel' . $i . '" class="hidden">' .
                            $view->displayCustomerLockLevel($i) .
                      '</span>';
}

my $customerUnderSurveillance = '<span id="customerUnderSurveillance" style="display:none;">' . htmllocale('Sous surveillance') . '</span>';
my $customerhasAreaExemption  = '<span id="customerHasAreaExemption"></span>';

# Adresse, téléphone, fax et e-mail
my $emailBtn = $view->displayFormInputText('customer.email', '', '', {'size' => 50, 'disabled' => 'disabled'});
$view->addPageInput({'sel' => '#customer\.email'});
$emailBtn .= ' <a href="#" id="customer.email.send" class="hidden" mytitle="' . htmllocale('Envoyer un e-mail à %s') . '" title="">' .
                    $view->displayImage('mail(|Over).png') .
             '</a>';

# N° commande, BC à fournir et facture proforma
my $orderImg = '<span id="customer.isOrderNoOnInvoice[0]" class="hidden" title="' . htmllocale('Non obligatoire') . '">' .
                    $view->displayBoolean(0, '') .
               '</span>' .
               '<span id="customer.isOrderNoOnInvoice[1]" class="hidden" title="' . htmllocale('Obligatoire') . '">' .
                    $view->displayBoolean(1, '') .
               '</span>';
my $orderRequiredImg = '<span id="customer.isOrderRequired[0]" class="hidden" title="' . htmllocale('Non obligatoire') . '">' .
                    $view->displayBoolean(0, '') .
               '</span>' .
               '<span id="customer.isOrderRequired[1]" class="hidden" title="' . htmllocale('Obligatoire') . '">' .
                    $view->displayBoolean(1, '') .
               '</span>';
my $proformaImg = '<span id="customer.isProformaRequired[0]" class="hidden" title="' . htmllocale('Non obligatoire') . '">' .
                        $view->displayBoolean(0, '') .
                  '</span>' .
                  '<span id="customer.isProformaRequired[1]" class="hidden" title="' . htmllocale('Obligatoire') . '">' .
                        $view->displayBoolean(1, '') .
                  '</span>';

# Contrat-cadre et conditions tarifaires
my $frmwrkContrat = '<span id="customer.frameworkContract[0]" class="hidden">' .
                        $view->displayBoolean(0, '') .
                    '</span>' .
                    '<a href="#" id="customer.frameworkContract[1]" class="hidden" target="_blank"></a>';
my $rentTariffGroup = '<span id="customer.rentTariffGroup[0]" class="hidden">' .
                            $view->displayBoolean(0, '') .
                      '</span>' .
                      '<span id="customer.rentTariffGroup[1]" class="hidden"></span>';
my $transportTariffGroup = '<span id="customer.transportTariffGroup[0]" class="hidden">' .
                                $view->displayBoolean(0, '') .
                           '</span>' .
                           '<span id="customer.transportTariffGroup[1]" class="hidden"></span>';

my $rentGridLabel    = htmllocale('Grille tarifaire de location');
my $transpGridLabel  = htmllocale('Grille tarifaire de transport');
my $rentGroupLabel   = htmllocale('Groupe tarifaire de location');
my $transpGroupLabel = htmllocale('Groupe tarifaire de transport');

# Assurance
my $insuranceTypes = '';

foreach my $insuranceTypeInfos (@{$tabViewData{'insurance.types'}})
{
    my $insuranceTypeId = $insuranceTypeInfos->{'id'};
    if (!$insuranceTypeInfos->{'isInvoiced'})
    {
        $insuranceTypes .= '<span data-type="' . $insuranceTypeId . ':0">' . $view->displayInsurance({'id' => $insuranceTypeId, 'infos' => $insuranceTypeInfos}) . '</span>';
    }
    else
    {
        # Assurance facturée, parcours des divers pourcentages
        foreach my $rate (@{$tabViewData{'insurance.rates'}})
        {
            $insuranceTypes .= '<span data-type="' . $insuranceTypeId . ':' . $rate . '">' . $view->displayInsurance({'id' => $insuranceTypeId, 'infos' => $insuranceTypeInfos, 'rate' => $rate}) . '</span>';
        }
    }
}

# Facturation de la gestion de recours
my $appealTypes = '' .
    '<span data-type="0">' .
        '<span class="locStandardAppealType">' . $view->displayBoolean(0, $locale->t('Non facturable')) . '</span>' .
    '</span>';
foreach my $appealTypeInfos (@{$tabViewData{'appeal.types'}})
{
    $appealTypes .= '' .
    '<span data-type="' . $appealTypeInfos->{'id'} . '">' .
        $view->displayAppeal({'id' => $appealTypeInfos->{'id'}, 'infos' => $appealTypeInfos, 'rate' => $tabViewData{'defaultAppealRate'}}) .
    '</span>';
}

# Facturation du nettoyage
my $cleaningImg = '<span id="customer.isCleaningInvoiced[0]" class="hidden">' .
                        $view->displayBoolean(0, $locale->t('Non facturé')) .
                  '</span>' .
                  '<span id="customer.isCleaningInvoiced[1]" class="hidden">' .
                        $view->displayBoolean(1, $locale->t('Facturé')) .
                  '</span>';

if ($tabViewData{'defaultResiduesMode'} != LOC::Common::RESIDUES_MODE_NONE)
{
    # Facturation de la participation au recyclage
    our $residuesImg = '<span id="customer.isResiduesInvoiced[0]" class="hidden">' .
                            $view->displayBoolean(0, $locale->t('Non facturé')) .
                      '</span>' .
                      '<span id="customer.isResiduesInvoiced[1]" class="hidden">' .
                            $view->displayBoolean(1, $locale->t('Facturé')) .
                      '</span>';
}


print '
<fieldset id="rentCustomerFieldset">
    <legend>' . $locale->t('Client') . '</legend>

    <div id="rentCustomerBlock">
        <div class="loadingMask"></div>
        <div class="content">
            <div id="rentCustomerControls">' . $newCustBtn . $unlockingTempPopupBtn . $customerHistoryBtn . $appCRMBtn . $refreshBtn . '</div>
            <div id="rentCustomerTabBox">';

# Onglet général
print '
                <div title="' . htmllocale('Général') . '">
                    <table class="formTable">
                    <tr>
                        <td class="label">' . $view->displayLabel('customer.id', $locale->t('Raison sociale')) .
                                              $requiredIcon .
                                              $view->displayHelp($locale->t('La recherche de client s\'effectue parmi la raison sociale, le code client, la localité, le code postal et le département')) .
                       '</td>
                        <td colspan="3">' . $custInput . '</td>
                    </tr>
                    <tr style="height: 20px;">
                        <td class="label">' . htmllocale('Code') . '</td>
                        <td style="height: 34px;" colspan="3">' . 
                            $customerLink . ' ' . 
                            $customerState . ' <br />' . 
                            $customerUnderSurveillance . ' ' . $customerNewAccount . 
                            $customerhasAreaExemption . '
                        </td>
                    </tr>
                    <tr>
                        <td class="label">' . htmllocale('Adresse') . '</td>
                        <td id="customer.address" style="height: 4em;" colspan="3"></td>
                    </tr>
                    <tr>
                        <td class="label">' . htmllocale('Tél.') . '</td>
                        <td id="customer.telephone"></td>
                        <td class="label" style="padding-left: 30px;">' . htmllocale('Fax') . '</td>
                        <td id="customer.fax"></td>
                    </tr>
                    <tr>
                        <td class="label">' . htmllocale('E-mail') . '</td>
                        <td colspan="3">' . $emailBtn . '</td>
                    </tr>
                    </table>
                    <hr class="dotted" />
                    <table class="formTable">
                    <tr>
                        <td class="label">' . htmllocale('N° de commande') . '</td>
                        <td class="customerDisplayBlock">' . $orderImg . '</td>
                        <td class="label" style="padding-left: 30px;">' . htmllocale('BC à fournir') . '</td>
                        <td class="customerDisplayBlock">' . $orderRequiredImg . '</td>
                        <td class="label" style="padding-left: 30px;">' . htmllocale('Facture proforma') . '</td>
                        <td class="customerDisplayBlock">' . $proformaImg . '</td>
                    </tr>
                    </table>
                </div>';

# Onglet Informations commerciales
print '
                <div title="' . htmllocale('Informations commerciales'). '">
                    <table class="formTable">
                    <tr style="vertical-align: baseline;">
                        <td class="label">' . htmllocale('Classe') . '</td>
                        <td>' . $view->displayCustomerClass('', 'customer.class') . '</td>
                        <td class="label" style="padding-left: 10px;">' . htmllocale('Potentiel') . '</td>
                        <td>' . $view->displayCustomerPotential('', 'customer.potential') . '</td>
                    </tr>
                    </table>
                    <table class="formTable">
                    <tr>
                        <td class="label">' . htmllocale('Contrat-cadre') . '</td>
                        <td class="customerDisplayBlock">' . $frmwrkContrat . '</td>
                        <td>
                            <table class="formTable">
                            <tr>
                                <td class="label" style="padding-left: 30px;" title="' . $rentGridLabel . '">' . htmllocale('GL') . '</td>
                                <td title="' . $rentGridLabel . '"><a href="#" id="customer.rentTariff.grid" target="_blank"></a></td>
                                <td class="label" style="padding-left: 15px;" title="' . $rentGroupLabel . '">' . htmllocale('Gpe loc.') . '</td>
                                <td class="customerDisplayBlock" title="' . $rentGroupLabel . '">' . $rentTariffGroup . '</td>
                            </tr>
                            <tr>
                                <td class="label" style="padding-left: 30px;" title="' . $transpGridLabel . '">' . htmllocale('GT') . '</td>
                                <td id="customer.transportTariff.grid" title="' . $transpGridLabel . '"></td>
                                <td class="label" style="padding-left: 15px;" title="' . $transpGroupLabel . '">' . htmllocale('Gpe transp.') . '</td>
                                <td class="customerDisplayBlock" title="' . $transpGroupLabel . '">' . $transportTariffGroup . '</td>
                            </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">' . htmllocale('Autres infos') . '</td>
                        <td id="rentCustomerFlags" colspan="2">' .
                            '<span class="none">' . htmllocale('Aucune') . '</span>' .
                            '<span class="electric">' . htmllocale('Électrique') . '</span>' .
                            '<span class="tempGrid">' . htmllocale('Grille provisoire') . '</span>' .
                       '</td>
                    </tr>
                    </table>
                    <hr class="dotted" />
                    <table class="formTable">
                    <tr>
                        <td class="label">' . htmllocale('Assurance') . '</td>
                        <td id="customer.insurance" colspan="3">' . $insuranceTypes . '</td>
                    </tr>
                    <tr>
                        <td class="label">' . htmllocale('Gestion de recours') . '</td>
                        <td id="customer.appeal">' . $appealTypes . '</td>
                    </tr>
                    <tr>
                        <td class="label">' . htmllocale('Nettoyage') . '</td>
                        <td class="customerDisplayBlock">' . $cleaningImg . '</td>
                    </tr>';
if ($tabViewData{'defaultResiduesMode'} != LOC::Common::RESIDUES_MODE_NONE)
{
    print          '<tr>
                        <td class="label">' . htmllocale('Participation au recyclage') . '</td>
                        <td class="customerDisplayBlock">' . $residuesImg . '</td>
                    </tr>';
}

print              '<tr>
                        <td class="label">' . htmllocale('Type paiement') . '</td>
                        <td id="customer.paymentMode.label"></td>
                    </tr>
                    </table>
                </div>';

print '
            </div>
        </div>
    </div>
</fieldset>';


# Popup de demande de déblocage temporaire
# Bouton demande de déblocage
my $unlockingTempBtn = $view->displayTextButton($locale->t('Demander le déblocage du client'),
                                      'rent/helper/customer/unlock(|Over).png',
                                      'return false;',
                                      $locale->t('Demander le déblocage du client')
                                      ,'l',
                                      {'id' => 'customerHlp_unlockingTempBtn',
                                       'tabindex' => -1,
                                       'onfocus' => 'this.blur();'});
my $commentInput = $view->displayFormTextArea('customerHlp_unlockComment', '');

print '
    <div class="customerHlp_popup" style="display: none;" id="customerHlp_unlockingTempPopup">
        <fieldset class="sub content">
            <legend>' . htmllocale('Déblocage temporaire') . '</legend>


            <table class="formTable">
            <tbody>
                <tr class="comment">
                    <td class="label">' . htmllocale('Commentaire') . '</td>
                    <td class="content">' . $commentInput . '</td>
                </tr>
            </tbody>
            </table>

        </fieldset>

        <fieldset class="sub ctrls">
            ' . $unlockingTempBtn . '
        </fieldset>
    </div>';


# SearchBox pour la recherche de client
print $view->displayJSBlock('
var customerSearchBoxObj = Location.searchBoxesManager.createSearchBox(
                                                                    "customer.id",
                                                                    {
                                                                     isRequired:false,
                                                                     url:"' . $tabViewData{'customersSearchUrl'} . '"
                                                                    }
                                                                   );
customerSearchBoxObj.createElement = function(key, value)
{
    var opt = new Option((value["name"] || "") + ", " + (value["locality.postalCode"] || ""), value["id"]);
    if (value["lockLevel"] > 0)
    {
        opt.className = "locStandardCustomerStatus level" + value["lockLevel"];
    }
    return opt;
}
Location.tabBoxesManager.createTabBox("rentCustomerTabBox", null, "175px");
');

1;
