use utf8;
use open (':encoding(UTF-8)');

use strict;


sub hlpFormatDocumentsData
{
    my ($tabData, $locale, $tabOptions) = @_;

    # Historiques
    foreach my $tabLine (@$tabData)
    {
        $tabLine->{'date'} = $locale->getDateFormat($tabLine->{'date'},
                                                    LOC::Locale::FORMAT_DATETIME_NUMERIC);
    }
}


1;
