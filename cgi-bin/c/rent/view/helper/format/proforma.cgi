use utf8;
use open (':encoding(UTF-8)');

use strict;


sub hlpFormatProformaData
{
    my ($tabData, $locale, $tabOptions) = @_;

    # Pro forma
    foreach my $tabPfmData (@{$tabData->{'tabList'}})
    {
        $tabPfmData->{'date'} = $locale->getDateFormat($tabPfmData->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC);
        $tabPfmData->{'beginDate'} = $locale->getDateFormat($tabPfmData->{'beginDate'}, LOC::Locale::FORMAT_DATE_NUMERIC);
        if ($tabPfmData->{'endDate'} ne '')
        {
            $tabPfmData->{'endDate'} = $locale->getDateFormat($tabPfmData->{'endDate'}, LOC::Locale::FORMAT_DATE_NUMERIC);
        }

        # Encaissement
        my $tabTmp = $tabPfmData->{'encashment'};
        $tabPfmData->{'encashment'} = '<span class="state">' . $tabTmp->[0] . '</span>';
        if ($tabTmp->[1] ne '')
        {
            $tabPfmData->{'encashment'} .= '<span class="closure">' . $locale->t('Clôturé') . ': ' . $tabTmp->[1] . '</span>';
        }
    }
}


1;
