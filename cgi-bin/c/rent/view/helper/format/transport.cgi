use utf8;
use open (':encoding(UTF-8)');

use strict;


sub hlpFormatTransportData
{
    my ($tabData, $locale, $tabOptions) = @_;

    # Liste des messages possibles pour la réalisation ou l'annulation d'un transport
    my $tabActionMsgs = {
        'tabDelivery' => {
            'do' => {
                'current' => $locale->t('Réaliser la livraison de ce contrat'),
                'linked'  => $locale->t('Réaliser la livraison de ce contrat et la récupération du contrat associé')
            },
            'undo' => {
                'current' => $locale->t('Annuler la réalisation de la livraison de ce contrat'),
                'linked'  => $locale->t('Annuler la réalisation de la livraison de ce contrat et la récupération du contrat associé')
            }
        },
        'tabRecovery' => {
            'do' => {
                'current' => $locale->t('Réaliser la récupération de ce contrat'),
                'linked'  => $locale->t('Réaliser la récupération de ce contrat et la livraison du contrat associé')
            },
            'undo' => {
                'current' => $locale->t('Annuler la réalisation de la récupération de ce contrat'),
                'linked'  => $locale->t('Annuler la réalisation de la récupération de ce contrat et la livraison du contrat associé')
            }
        }
    };


    foreach my $side (('tabDelivery', 'tabRecovery'))
    {
        my $tab = $tabData->{$side};

        my $extraInfos = '';
        if ($tab->{'extraInfos'}->{'order'})
        {
            $extraInfos .= '<div class="row"><span class="label">' . $locale->t('Commande') . '&nbsp;:</span>' .
                         '<span class="value">' . $tab->{'extraInfos'}->{'order'}->{'code'} . '</span>' .
                      '</div>';
        }
        $tab->{'extraInfos'} = $extraInfos;


        # Formatage des messages de confirmation pour la réalisation et l'annulation de la réalisation du transport
        if ($tab->{'action'})
        {
            my $tabMsgs = $tabActionMsgs->{$side}->{$tab->{'action'}->{'type'}};

            my $msg = $tab->{'action'}->{'msgs'}->{'current'};
            if ($msg)
            {
                $msg->{'title'} = $tabMsgs->{'current'};
            }
            my $msg = $tab->{'action'}->{'msgs'}->{'linked'};
            if ($msg)
            {
                $msg->{'title'} = '<div>' . $tabMsgs->{'linked'} . '</div>' .
                                  '<div class="contract">' .
                                      '<div class="code">' .
                                          '<a target="_blank" href="' . $tab->{'contract'}->{'url'} . '">' .
                                                $tab->{'contract'}->{'code'} .
                                          '</a>' .
                                      '</div>' .
                                      '<div class="site-label">' . $tab->{'contract'}->{'infos'}->[0] . '</div>' .
                                      '<div class="site-locality">' .
                                           $tab->{'contract'}->{'infos'}->[1] . ' ' . $tab->{'contract'}->{'infos'}->[2] .
                                      '</div>' .
                                  '</div>';
            }
        }

        # Informations sur le contrat associé
        if ($tab->{'contract'})
        {
            &hlpFormatTransportContractData($tab->{'contract'}, $locale, $tabOptions);
        }

        # État des lieux
        if ($tab->{'tabMachineInventoryInfos'})
        {
            $tab->{'tabMachineInventoryInfos'}->{'date'} = $locale->getDateFormat($tab->{'tabMachineInventoryInfos'}->{'date'},
                                                                                  LOC::Locale::FORMAT_DATETIME_NUMERIC);
        }
    }
}


sub hlpFormatTransportContractData
{
    my ($tabData, $locale, $tabOptions) = @_;

    my $siteInfos = '<div class="row"><span class="label">' . $locale->t('Libellé chantier') . '&nbsp;:</span>' .
                        '<span class="value">' . $tabData->{'siteInfos'}->{'label'} . '</span>' .
                    '</div>' .
                    '<div class="row"><span class="label">' . $locale->t('Ville') . '&nbsp;:</span>' .
                        '<span class="value">' .
                            $tabData->{'siteInfos'}->{'postalCode'} . ' ' . $tabData->{'siteInfos'}->{'city'} .
                        '</span>' .
                    '</div>';
    $tabData->{'siteInfos'} = $siteInfos;

}

1;
