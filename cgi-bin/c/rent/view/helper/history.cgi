use utf8;
use open (':encoding(UTF-8)');

# File: history.cgi
#
# Affichage du bloc d'historique sur le devis et le contrat.
#

my $currentDir = $dir . '../helper/history/';


# Traductions
$view->addTranslations({
    $prefix . 'entity_estimate'         => $locale->t('Devis'),
    $prefix . 'entity_estimateLine'     => $locale->t('Ligne de devis'),
    $prefix . 'entity_contract'         => $locale->t('Contrat'),
    $prefix . 'viewExtra'               => $locale->t('Voir les informations complémentaires'),
    $prefix . 'extra_to'                => $locale->t('À'),
    $prefix . 'extra_cc'                => $locale->t('Cc'),
    $prefix . 'extra_subject'           => $locale->t('Objet'),
    $prefix . 'extra_message'           => $locale->t('Message'),
    $prefix . 'extra_date'              => $locale->t('Date'),
    $prefix . 'extra_duration'          => $locale->t('Durée'),
    $prefix . 'extra_reason'            => $locale->t('Motif'),
    $prefix . 'extra_appliedToContract' => $locale->t('Contrat lié'),
    $prefix . 'extra_comment'           => $locale->t('Commentaire'),
    $prefix . 'extra_isInvoiceable'     => $locale->t('Facturé')
});


print '
<div title="' . htmllocale('Historique') . '">';

print '
<div id="' . $prefix . 'container">

    <table class="basic" id="' . $prefix . 'list">
    <thead>
        <tr>
            <th class="date">' . htmllocale('Date et heure') . '</th>
            <th class="user">' . htmllocale('Utilisateur') . '</th>
            <th class="entity">' . htmllocale('Origine') . '</th>
            <th class="event">' . htmllocale('Événement') . '</th>
            <th class="buttons"></th>
            <th class="desc">' . htmllocale('Description') . '</th>
        </tr>
    </thead>
    <tbody class="rows">
    </tbody>
    <tbody class="msgs">
        <tr class="noResult">
            <td colspan="6">' . htmllocale('Aucun historique disponible') . '</td>
        </tr>
    </tbody>
    </table>

</div>';


# Modale
print '
    <div style="display: none;" id="' . $prefix . 'modal">
        <fieldset class="sub">
            <legend>' . $locale->t('Informations complémentaires') . '</legend>
            <table class="formTable" id="' . $prefix . 'modalTable">
            <tbody>
            </tbody>
            </table>
        </fieldset>
    </div>';

print '
</div>';

1;
