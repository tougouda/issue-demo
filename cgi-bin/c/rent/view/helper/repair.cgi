use utf8;
use open (':encoding(UTF-8)');

# Affichage de l'onglet "Remise en état"

my $tabRepair = $tabViewData{'tabContractInfos'}->{'tabRepair'};
my $rights    = $tabViewData{'tabContractInfos'}->{'tabRights'}->{'repair'};

print '
<div title="' . htmllocale('Remise en état') . '" id="' . $prefix . 'content">
    <div class="container">';


# Fiches de remise en état
print '
        <fieldset id="repairSheets" class="sub">
            <legend>' . htmllocale('Fiches de remise en état') . '</legend>';

my $nbRepairSheets = @{$tabRepair->{'sheets'}};
if ($nbRepairSheets > 0)
{
    print '
            <table class="standard" id="' . $prefix . 'repairSheets">
                <thead>
                    <tr>
                        <th>' . htmllocale('N°') . '</th>
                        <th>' . htmllocale('Date') . '</th>
                        <th>' . htmllocale('Technicien') . '</th>
                        <th class="amount">' . htmllocale('Montant') . '</th>
                        <th>' . htmllocale('État') . '</th>
                    </tr>
                </thead>
                <tbody>';


    foreach my $tabSheetInfo (@{$tabRepair->{'sheets'}})
    {
        my $className = ($tabSheetInfo->{'state.id'} eq LOC::Repair::Sheet::STATE_TURNEDIN ? 'turned' : '');

        my $repairSheetUrl = $view->createURL('repair:sheet:edit', {'sheetId' => $tabSheetInfo->{'id'}});
        my $repairSheetLink = '<a href="' . $repairSheetUrl . '" target="_blank">' . $tabSheetInfo->{'id'} . '</a>';

        my $userClassName = ($tabSheetInfo->{'user.state.id'} eq LOC::User::STATE_INACTIVE ? 'inactive' : '');

        print '
                    <tr' . ($className eq '' ? '' : ' class="' . $className . '"') . '>
                        <td>' . $repairSheetLink . '</td>
                        <td>' . $locale->getDateFormat($tabSheetInfo->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC) . '</td>
                        <td class="' . $userClassName . '">' . $tabSheetInfo->{'user.fullName'} . '</td>
                        <td class="amount">' . $locale->getCurrencyFormat($tabSheetInfo->{'amount'}) . '</td>
                        <td>' . $view->displayState($tabSheetInfo->{'state.id'}, $tabSheetInfo->{'state.label'}) . '</td>
                    </tr>';

        if (defined $tabSheetInfo->{'repairEstimate'})
        {
            my $repairEstimateUrl = $view->createURL('repair:estimate:edit', {'id' => $tabSheetInfo->{'repairEstimate'}->{'id'}});
            my $repairEstimateLink = '<a href="' . $repairEstimateUrl . '" target="_blank">' . $tabSheetInfo->{'repairEstimate'}->{'code'} . '</a>';

            my $userClassName = ($tabSheetInfo->{'repairEstimate'}->{'user.state.id'} eq LOC::User::STATE_INACTIVE ? 'inactive' : '');
            print '
                    <tr class="repairEstimate">
                        <td>' . $repairEstimateLink . '</td>
                        <td>' . $locale->getDateFormat($tabSheetInfo->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC) . '</td>
                        <td class="' . $userClassName . '">' . $tabSheetInfo->{'repairEstimate'}->{'user.fullName'} . '</td>
                        <td class="amount">' . $locale->getCurrencyFormat($tabSheetInfo->{'repairEstimate'}->{'amount'}) . '</td>
                        <td>' . $view->displayState($tabSheetInfo->{'repairEstimate'}->{'state.id'}, $tabSheetInfo->{'repairEstimate'}->{'state.label'}) . '</td>
                    </tr>';
        }

        if (defined $tabSheetInfo->{'repairDays'} && $tabSheetInfo->{'repairDays'}->{'count'} > 0)
        {
            print '
                    <tr class="repairDays">
                        <td colspan="2">' . $locale->tn(
                            '%d jour de remise en état',
                            '%d jours de remise en état',
                            $tabSheetInfo->{'repairDays'}->{'count'},
                            $locale->getNumberFormat($tabSheetInfo->{'repairDays'}->{'count'})
                        ) . '</td>
                        <td></td>
                        <td class="amount">' . $locale->getCurrencyFormat($tabSheetInfo->{'repairDays'}->{'amount'}) . '</td>
                        <td></td>
                    </tr>';
        }

        if (defined $tabSheetInfo->{'repairServices'} && $tabSheetInfo->{'repairServices'}->{'count'} > 0)
        {
            print '
                    <tr class="repairServices">
                        <td colspan="2">' . $locale->tn(
                            '%d service de remise en état',
                            '%d services de remise en état',
                            $tabSheetInfo->{'repairServices'}->{'count'},
                            $locale->getNumberFormat($tabSheetInfo->{'repairServices'}->{'count'})
                        ) . '</td>
                        <td></td>
                        <td class="amount">' . $locale->getCurrencyFormat($tabSheetInfo->{'repairServices'}->{'amount'}) . '</td>
                        <td></td>
                    </tr>';
        }
    }

    print '
                </tbody>
            </table>';
}
else
{
    print '<span class="noresult">' . htmllocale('Aucune') . '</span>';
}

print '
        </fieldset>';


# Jours de remise en état

print '
        <fieldset id="repairDays" class="sub">
            <legend>' . htmllocale('Jours de remise en état') . '</legend>';

my $nbRepairDays = keys(%{$tabRepair->{'days'}});

if ($nbRepairDays == 0)
{
    print '
            <span class="noresult">' . htmllocale('Aucun') . '</span>';
}
else
{
    print '
            <table class="standard" id="' . $prefix . 'repairDays">
                <thead>
                    <tr>
                        <th class="unitPrice">' . htmllocale('PU') . '</th>
                        <th class="quantity">' . htmllocale('Nombre') . '</th>
                        <th class="amount">' . htmllocale('Montant') . '</th>
                        <th class="state">' . htmllocale('État') . '</th>
                        <th class="ctrls"></th>
                        <th class="sheet">' . htmllocale('Fiche n° %s', '') . '</th>
                    </tr>
                </thead>
                <tbody>';

    my $nbSheets = keys(%{$tabRepair->{'days'}}); # Nombre total de fiches de remise en état
    my $sheetNo = 0;                      # Numéro de la fiche de remise en état courante
    foreach my $sheetId (keys(%{$tabRepair->{'days'}}))
    {
        my $nbDays = @{$tabRepair->{'days'}->{$sheetId}};

        my $canReopenSheet = ($rights->{'reopenSheet'} ? 1 : 0);
        if ($canReopenSheet)
        {
            for (my $i = 0; $i < $nbDays; $i++)
            {
                if ($tabRepair->{'days'}->{$sheetId}->[$i]->{'state.id'} eq LOC::Contract::RepairDay::STATE_INVOICED)
                {
                    $canReopenSheet = 0;
                    last;
                }
            }
        }

        for (my $i = 0; $i < $nbDays; $i++)
        {
            my $tabDayInfos = $tabRepair->{'days'}->{$sheetId}->[$i];

            my $id = $tabDayInfos->{'id'};

            my $unitAmountInput = $view->displayFormInputCurrency('repairDaysUnitAmount[' . $id . ']',
                                                                  $tabDayInfos->{'unitAmount'}, LOC::Html::CURRENCYMODE_TEXT,
                                                                  $tabDayInfos->{'currency.symbol'});
            my $amountInput = $view->displayFormInputCurrency('repairDaysAmount[' . $id . ']',
                                                              $tabDayInfos->{'amount'}, LOC::Html::CURRENCYMODE_TEXT,
                                                              $tabDayInfos->{'currency.symbol'});

            my $stateInput = $view->displayState($tabDayInfos->{'state.id'}, $tabDayInfos->{'state.label'});
            my $ctrls = '';

            if ($canReopenSheet && $tabDayInfos->{'state.id'} ne LOC::Contract::RepairDay::STATE_INVOICED &&
                $rights->{'invoiceDays'})
            {
                # Jours abandonnés
                $ctrls .= $view->displayTextButton('', $dir . 'invoice(|Over).png',
                                                       'return false;',
                                                       $locale->t('Facturer ces jours de remise en état'), 'l',
                                                       {'class' => 'invoice',
                                                        'id' => $prefix . 'invoiceDayBtn[' . $id . ']'
                                                       });
                # Jours acceptés pour facturation
                $ctrls .= $view->displayTextButton('', $dir . 'uninvoice(|Over).png',
                                                        'return false;',
                                                        $locale->t('Ne pas facturer ces jours de remise en état'), 'l',
                                                        {'class' => 'uninvoice',
                                                         'id' => $prefix . 'uninvoiceDayBtn[' . $id . ']'
                                                        });
            }

            # Bouton de remise en cours de la fiche
            my $reopenInput = '';
            if ($canReopenSheet)
            {
                $reopenInput = $view->displayTextButton('', $dir . 'revert(|Over).png',
                                                        'Location.rentContractObj.showReopenRepairSheetPopup(' . $sheetId . ');',
                                                        $locale->t('Remettre en cours la fiche de remise en état'));
            }

            print '
                    <tr id="' . $prefix . 'repairDay[' . $tabDayInfos->{'id'} . ']">
                        <td class="unitPrice">' . $unitAmountInput . '</td>
                        <td class="quantity">' . $locale->t('%s j', $tabDayInfos->{'number'}) . '</td>
                        <td class="amount">' . $amountInput . '</td>
                        <td class="state">' . $stateInput . '</td>
                        <td class="ctrls">' . $ctrls . '</td>';
            if ($i == 0)
            {
                # Lien vers la fiche de remise en état
                my $sheetUrl = $view->createURL('repair:sheet:edit', {'sheetId' => $sheetId});
                my $sheetLink = '<a href="' . $sheetUrl . '" target="_blank">' . $sheetId . '</a>';

                print '
                        <td class="sheet" rowspan="' . $nbDays . '"><b>' . $sheetLink  . '</b>&nbsp;' . $reopenInput . '</td>';
            }
            print '
                    </tr>';
        }

        if ($sheetNo != $nbSheets - 1)
        {
            print '
                    <tr class="sep">
                        <td colspan="6"></td>
                    </tr>';
        }

        $sheetNo++;
    }

    print '
                </tbody>
            </table>';
}

print '
        </fieldset>';


# Services de remise en état
print '
        <fieldset id="repairServices" class="sub">
            <legend>' . htmllocale('Services de remise en état') . '</legend>';

my $nbRepairServices = keys(%{$tabRepair->{'services'}});

if ($nbRepairServices == 0)
{
    print '
            <span class="noresult">' . htmllocale('Aucun') . '</span>';
}
else
{
    print '
            <table class="standard" id="' . $prefix . 'repairSrvs">
                <thead>
                    <tr>
                        <th class="label">' . htmllocale('Service') . '</th>
                        <th class="unitPrice">' . htmllocale('PU') . '</th>
                        <th class="quantity">' . htmllocale('Qté') . '</th>
                        <th class="amount">' . htmllocale('Montant') . '</th>
                        <th class="state">' . htmllocale('État') . '</th>
                        <th class="ctrls"></th>
                        <th class="sheet">' . htmllocale('Fiche n° %s', '') . '</th>
                    </tr>
                </thead>
                <tbody>';

    my $nbSheets = keys(%{$tabRepair->{'services'}}); # Nombre total de fiches de remise en état
    my $sheetNo = 0;                          # Numéro de la fiche de remise en état courante
    foreach my $sheetId (keys(%{$tabRepair->{'services'}}))
    {
        my $nbServices = @{$tabRepair->{'services'}->{$sheetId}};

        my $canReopenSheet = ($rights->{'reopenSheet'} ? 1 : 0);
        if ($canReopenSheet)
        {
            for (my $i = 0; $i < $nbServices; $i++)
            {
                if ($tabRepair->{'services'}->{$sheetId}->[$i]->{'state.id'} eq LOC::Contract::RepairService::STATE_INVOICED)
                {
                    $canReopenSheet = 0;
                    last;
                }
            }
        }

        for (my $i = 0; $i < $nbServices; $i++)
        {
            my $tabServiceInfos = $tabRepair->{'services'}->{$sheetId}->[$i];
            my $id = $tabServiceInfos->{'id'};

            my $unitAmountInput = $view->displayFormInputCurrency('repairServiceUnitAmount[' . $id . ']',
                                                                  $tabServiceInfos->{'unitAmount'},
                                                                  LOC::Html::CURRENCYMODE_TEXT,
                                                                  $tabServiceInfos->{'currency.symbol'});
            my $amountInput = $view->displayFormInputCurrency('repairServiceAmount[' . $id . ']',
                                                              $tabServiceInfos->{'amount'},
                                                              LOC::Html::CURRENCYMODE_TEXT,
                                                              $tabServiceInfos->{'currency.symbol'});

            my $stateInput = $view->displayState($tabServiceInfos->{'state.id'}, $tabServiceInfos->{'state.label'});
            my $ctrls = '';

            if ($canReopenSheet && $tabServiceInfos->{'state.id'} ne LOC::Contract::RepairService::STATE_INVOICED &&
                $rights->{'invoiceServices'})
            {
                # Service abandonné
                $ctrls .= $view->displayTextButton('', $dir . 'invoice(|Over).png',
                                                   'return false;',
                                                   $locale->t('Facturer ce service de remise en état'), 'l',
                                                   {'class' => 'invoice',
                                                    'id' => $prefix . 'invoiceServiceBtn[' . $id . ']'});
                # Service accepté pour facturation
                $ctrls .= $view->displayTextButton('', $dir . 'uninvoice(|Over).png',
                                                   'return false;',
                                                   $locale->t('Ne pas facturer ce service de remise en état'), 'l',
                                                   {'class' => 'uninvoice',
                                                    'id' => $prefix . 'uninvoiceServiceBtn[' . $id . ']'});
            }
            # Bouton de remise en cours de la fiche
            my $reopenInput = '';
            if ($canReopenSheet)
            {
                $reopenInput = $view->displayTextButton('', $dir . 'revert(|Over).png',
                                                        'Location.rentContractObj.showReopenRepairSheetPopup(' . $sheetId . ');',
                                                        $locale->t('Remettre en cours la fiche de remise en état'));
            }

            my $label = $tabServiceInfos->{'label'};
            if ($label eq '' && $tabServiceInfos->{'type'} eq LOC::Repair::LINETYPE_DISPLACEMENT)
            {
                $label = $locale->t('Déplacement');
            }
            if ($label eq '' && $tabServiceInfos->{'type'} eq LOC::Repair::LINETYPE_LABOUR)
            {
                $label = $locale->t('Main d\'œuvre');
            }

            print '
                    <tr id="' . $prefix . 'repairSrv[' . $tabServiceInfos->{'id'} . ']">
                        <td class="label">' . $label . '</td>
                        <td class="unitPrice">' . $unitAmountInput . '</td>
                        <td class="quantity">' . $tabServiceInfos->{'quantity'} . '</td>
                        <td class="amount">' . $amountInput . '</td>
                        <td class="state">' . $stateInput . '</td>
                        <td class="ctrls">' . $ctrls . '</td>';
            if ($i == 0)
            {
                # Lien vers la fiche de remise en état
                my $sheetUrl = $view->createURL('repair:sheet:edit', {'sheetId' => $tabServiceInfos->{'repairsheet.id'}});
                my $sheetLink = '<a href="' . $sheetUrl . '" target="_blank">' . $tabServiceInfos->{'repairsheet.id'} . '</a>';

                print '
                        <td class="sheet" rowspan="' . $nbServices . '"><b>' . $sheetLink  . '</b>&nbsp;' . $reopenInput . '</td>';
            }
            print '
                    </tr>';
        }

        if ($sheetNo != $nbSheets - 1)
        {
            print '
                    <tr class="sep">
                        <td colspan="7"></td>
                    </tr>';
        }

        $sheetNo++;
    }

    print '
                </tbody>
            </table>';
}

print '
        </fieldset>';


print '
    </div>
</div>';


1;
