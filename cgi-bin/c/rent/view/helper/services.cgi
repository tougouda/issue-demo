use utf8;
use open (':encoding(UTF-8)');

# File: service.cgi
#
# Affichage du bloc "Autres services" sur le devis et le contrat.
#


# Traductions pour le javascript
$view->addTranslations({
    'confirmRemoveRentService' => $locale->t('Êtes-vous sûr de vouloir supprimer ce service ?'),
    'rentServiceUnderMinAmountError' => $locale->t('Le montant que vous avez saisi est inférieur au montant minimum autorisé : ')
});


# Droits sur cette partie
my $rights = $tabViewData{'tabContractInfos'}->{'tabRights'}->{'services'};


# Choix de l'assurance
my $insuranceInput = $view->displayInsurance({'rates' => $tabViewData{'insurance.rates'}},
                                             LOC::Html::Standard::DISPINS_SELECT,
                                             {'insType.id' => $prefix . 'insuranceType',
                                              'insRate.id' => $prefix . 'insuranceRate'});
$view->addPageInput({
    'sel'  => '#' . $prefix . 'insuranceType',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});
$view->addPageInput({
    'sel'  => '#' . $prefix . 'insuranceRate',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});

# Montant de l'assurance
my $insuranceAmountInput = $view->displayFormInputCurrency($prefix . 'insuranceAmount', 0,
                                                           LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);

# Types de gestion de recours
my $tabAppeal = {};
foreach my $appealInfos (@{$tabViewData{'appeal.types'}})
{
    $tabAppeal->{$appealInfos->{'id'}} = $appealInfos->{'label'};
}

my $appealMsgs = '<span id="' . $prefix . 'appealMsg" style="visibility: hidden;"></span>';

my $appealInput = $view->displayAppeal({'rate' => $tabViewData{'defaultAppealRate'}},
                                             LOC::Html::Standard::DISPINS_SELECT,
                                             {'insType.id' => $prefix . 'appealType'});

$view->addPageInput({
    'sel'  => '#' . $prefix . 'appealType',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});
my $nonInvoiceableAppeal = '<span id="' . $prefix . 'nonInvoiceableAppeal">' . $locale->t('Non facturable') . '</span>';


# Montant de la gestion de recours
my $appealAmountInput = $view->displayFormInputCurrency($prefix . 'appealAmount', 0,
                                                       LOC::Html::CURRENCYMODE_TEXT,
                                                       $currencySymbol, undef);
$view->addPageInput({
    'sel'  => '#' . $prefix . 'appealAmount',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});

# Types de nettoyage
my @tabCleanings = ();
push(@tabCleanings, {'value' => 0, 'innerHTML' => $locale->t('Non facturé')});
my $cleaningInput = $view->displayFormSelect($prefix . 'cleaningType', \@tabCleanings, 0, 0);
$view->addPageInput({
    'sel'  => '#' . $prefix . 'cleaningType',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});

# Montant du nettoyage
my $cleaningAmountInput = $view->displayFormInputCurrency($prefix . 'cleaningAmount', 0,
                                                          LOC::Html::CURRENCYMODE_INPUT,
                                                          $currencySymbol, undef);
$view->addPageInput({
    'sel'  => '#' . $prefix . 'cleaningAmount',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});

# Types de participation au recyclage
my $tabResidues = [
    {'value' => LOC::Common::RESIDUES_MODE_NONE, 'innerHTML' => $locale->t('Non facturé')},
    {'value' => LOC::Common::RESIDUES_MODE_PRICE, 'innerHTML' => $locale->t('Montant forfaitaire')},
    {'value' => LOC::Common::RESIDUES_MODE_PERCENT, 'innerHTML' => $locale->t('Pourcentage')}
];

my $residuesMsgs = '<span id="' . $prefix . 'residuesMsg_' . LOC::Common::RESIDUES_MODE_PERCENT . '" style="visibility: hidden;"> = ' .
                       $locale->t('%s %% du montant de la location et des services de remise en état',
                                    '<span id="' . $prefix . 'residuesValue_' . LOC::Common::RESIDUES_MODE_PERCENT . '"></span>') .
                   '</span>';

my $residuesInput = $view->displayFormSelect($prefix . 'residuesMode', $tabResidues, 0, 0) . $residuesMsgs;

$view->addPageInput({
    'sel'  => '#' . $prefix . 'residuesMode',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});


# Montant de la participation au recyclage
my $residuesAmountInput = $view->displayFormInputCurrency($prefix . 'residuesAmount', 0,
                                                       LOC::Html::CURRENCYMODE_TEXT,
                                                       $currencySymbol, undef);
$view->addPageInput({
    'sel'  => '#' . $prefix . 'residuesAmount',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});

# Nombre de litres de carburant
my $fuelQtyInput = $view->displayFormInputText($prefix . 'fuelQty', 0, 0);
$view->addPageInput({
    'sel'  => '#' . $prefix . 'fuelQty',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});

# Prix du litre carburant
my $fuelUnitPriceInput = $view->displayFormInputCurrency($prefix . 'fuelUnitPrice', 0,
                                                          LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);

# Montant du carburant
my $fuelAmountInput = $view->displayFormInputCurrency($prefix . 'fuelAmount', 0,
                                                      LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);

my $fuelEstimation = '';
if ($tabViewData{'documentType'} eq 'contract')
{
    my $fuelEstimUnitP = $view->displayFormInputCurrency($prefix . 'fuelEstimUnitP', 0,
                                                              LOC::Html::CURRENCYMODE_TEXT, $currencySymbol);
    $fuelEstimation = '<span id="' . $prefix . 'fuelEstimQtyZone" style="visibility:hidden">(' . htmllocale('Estimation carburant ')
                            . $locale->t('%s L à %s', '<span id="' . $prefix . 'fuelEstimQty"> </span>', $fuelEstimUnitP) . ')</span>';
}

# Info bulle sur l'icone de l'assurance
my $infoNotEditableInsurance;
if ($tabViewData{'documentType'} eq 'estimate')
{
    $infoNotEditableInsurance = $locale->t('L\'assurance n\'est pas modifiable car le devis est en Full service.');
}
elsif ($tabViewData{'documentType'} eq 'contract')
{
    $infoNotEditableInsurance = $locale->t('L\'assurance n\'est pas modifiable car le contrat est en Full service.');
}

print '
<div title="' . htmllocale('Autres services') . '" id="'. $prefix . 'container">
<table>
<tbody>
    <tr style="vertical-align: top">
        <td colspan="2">
            <fieldset class="sub">
                <legend>' . htmllocale('Services standards') . '</legend>
                <table class="formTable">
                <tbody>
                    <tr>
                        <td class="label">' . htmllocale('Assurance') .
                            $view->displayTooltipIcon('info', $infoNotEditableInsurance, {'id' => $prefix . 'infoNotEditableInsurance'})  .'
                        </td>
                        <td>' . $insuranceInput . '</td>
                        <td style="text-align: right">' . $insuranceAmountInput . '</td>
                    </tr>
                    <tr id="' . $prefix . 'insuranceChanged" style="display: none;">
                        <td></td>
                        <td colspan="2">
                            <div>
                                '. $view->displayMessages('warning', [$locale->t('Les conditions d\'assurance du client sont différentes.')], 0) .'
                            </div>
                        </td>
                    </tr>
                    <tr id="' . $prefix . 'appealRow" style="display: none;">
                        <td class="label">' . htmllocale('Gestion de recours') . '</td>
                        <td>' .  $appealInput . $nonInvoiceableAppeal . '</td>
                        <td style="text-align: right">' . $appealAmountInput . '</td>
                    </tr>
                    <tr>
                        <td class="label">' . htmllocale('Nettoyage') . '</td>
                        <td>' . $cleaningInput . '</td>
                        <td style="text-align: right">' . $cleaningAmountInput . '</td>
                    </tr>
                    <tr id="' . $prefix . 'residuesRow">
                        <td class="label">' . htmllocale('Participation au recyclage') . '</td>
                        <td>' . $residuesInput . '</td>
                        <td style="text-align: right">' . $residuesAmountInput . '</td>
                    </tr>
                    <tr id="' . $prefix . 'residuesModeChanged" style="display: none;">
                        <td></td>
                        <td colspan="2">
                            <div>
                                '. $view->displayMessages('warning', [$locale->t('Ces conditions de facturation de la participation au recyclage ne pourront pas être appliquées sur le contrat.')], 0) .'
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">' . ($tabViewData{'documentType'} eq 'contract' ? htmllocale('Carburant') : htmllocale('Estimation carburant')) . '</td>
                        <td>' . $locale->t('%s L à %s', $fuelQtyInput, $fuelUnitPriceInput) .
                                '<a id="' . $prefix . 'calc" href="#">' . $view->displayImage('calc.png') . '</a><span id ="' . $prefix .
                                'warningFuelEstimation" style="visibility:hidden">' . $view->displayImage('iconWarning.gif', {'title' => $locale->t('Carburant saisi différent de l\'estimation')}) . '</span>'
                            . $fuelEstimation . '</td>
                        <td style="text-align: right">' . $fuelAmountInput . '</td>

                    </tr>
                </tbody>
                </table>
            </fieldset>
        </td>
    </tr>
    <tr style="vertical-align: top">';


###################################
# Services location et transport
###################################

# Liste des services location et transport du contrat
my $tabRentServices = $tabViewData{'tabContractInfos'}->{'tabRentServices'};


# Nombre des services location et transport
my $countServices = @$tabRentServices;
my $hasEditRight = ($rights->{'editRentServices'});

print '
        <td>
            <fieldset id="' . $prefix . 'rentSrvsBlock" class="sub">
                <legend>' . htmllocale('Services location et transport') . '</legend>';

# Liste des services location et transport
my $tabRentServiceTypes = $tabViewData{'tabRentServiceTypes'};
my @tabRentServicesPairs = ();

# Ajout de la ligne de sélection des services location et transport
while (my ($id, $rentService) = each(%$tabRentServiceTypes))
{
    push(@tabRentServicesPairs, {'value'       => $id,
                                 'innerHTML'   => $rentService->{'name'},
                                 'data-amount' => &LOC::Util::toNumber($rentService->{'amount'}),
                                 'data-infos'  => $rentService->{'infos'}});
}

# Ligne de titre de la table
print '
            <table id="' . $prefix . 'rentSrvsTable" class="standard">
            <thead>
                <tr>
                    <th class="infos"></th>
                    <th class="name">' . htmllocale('Service') . '</th>
                    <th class="comment">' . htmllocale('Commentaire') . '</th>
                    <th class="amount">' . htmllocale('Montant') . '</th>';
if ($tabViewData{'documentType'} eq 'contract')
{
    print '
                    <th class="state">' . htmllocale('État') . '</th>';
}
print '
                    <th class="ctrls"></th>
                </tr>
            </thead>';

# Ajout de l'élément "Aucun"
unshift(@tabRentServicesPairs, {'value' => 0, 'innerHTML' => '-- ' . $locale->t('Sélectionner') . ' --', 'data-amount' => 0});

# nouvelle ligne avec aucun service sélectionné
my $rcsIdInput = $view->displayFormSelect($prefix . 'rentSrvsAddTypeId', \@tabRentServicesPairs, 0, 0);
# Commentaire du service
my $rcsCommentInput = $view->displayFormInputText($prefix . 'rentSrvsAddComment', '', '',
                                                  {'maxlength'   => 69,
                                                   'disabled' => 'disabled'});
# Montant
my $rcsAmountInput = $view->displayFormInputCurrency($prefix . 'rentSrvsAddAmount',
                                                     &LOC::Util::round(0, $locale->getFracDigits()),
                                                     LOC::Html::CURRENCYMODE_INPUT,
                                                     $currencySymbol,
                                                     undef,
                                                     {'disabled' => 'disabled'});

# Infobulle
my $rcsTooltip = $view->displayHelp('', {'id' => $prefix . 'rentSrvsAddTooltip'});

print '
            <tfoot>
                <tr id="' . $prefix . 'rentSrvsAddLine">
                    <td class="infos">' . $rcsTooltip . '</td>
                    <td class="name">' . $rcsIdInput . '</td>
                    <td class="comment">' . $rcsCommentInput . '</td>
                    <td class="amount">' . $rcsAmountInput . '</td>';
if ($tabViewData{'documentType'} eq 'contract')
{
    print '
                    <td class="state"></td>';
}
print '
                    <td class="ctrls"><a class="delete" href="#" onclick="return false;" title="' . htmllocale('Supprimer') . '"></a></td>
                </tr>
            </tfoot>
            <tbody>
            </tbody>
            </table>

            <span id="' . $prefix . 'rentSrvsNoResult" class="noresult">' . htmllocale('Aucun') . '</span>

            </fieldset>
        </td>
    </tr>';



if ($tabViewData{'documentType'} eq 'contract')
{
    ###########################################
    # Anciens services complémentaires
    ###########################################

    my $tabOldServices = $tabViewData{'tabContractInfos'}->{'tabOldServices'};

    # Nombre des anciens services complémentaires
    my $countServices = @$tabOldServices;

    # S'il y a des anciens services complémentaires à afficher
    if ($countServices > 0)
    {
        print '
    <tr>
        <td colspan="2">
            <fieldset class="sub">
                <legend>' . htmllocale('Services complémentaires') . '</legend>
                <table class="standard">
                <thead>
                    <tr>
                        <th>' . htmllocale('Service') . '</th>
                        <th>' . htmllocale('Montant') . '</th>
                        <th>' . htmllocale('État') . '</th>
                    </tr>
                </thead>
                <tbody>';
        my $i = 0;
        foreach my $tab (@$tabOldServices)
        {
            print '
                    <tr>
                        <td>' . $tab->{'label'} . '</td>
                        <td>' . $view->displayFormInputCurrency($prefix . 'oldSrvAmount_' . $i, $tab->{'amount'},
                                                                LOC::Html::CURRENCYMODE_TEXT, $currencySymbol) . '</td>
                        <td>' . $tab->{'stateLabel'} . '</td>
                    </tr>';
            $i++;
        }
        print '
                </tbody>
                </table>
            </fieldset>
        </td>
    </tr>';
    }
}
    print '
</tbody>
</table>
</div>';


1;
