use utf8;
use open (':encoding(UTF-8)');

# File: comments.cgi
#
# Affichage du bloc de commentaires sur le devis et le contrat.
#


my $prefix;
my $additionalNote;
if ($tabViewData{'documentType'} eq 'estimate')
{
    $additionalNote = $locale->t('figurent sur le devis');
    $prefix = 'rentEstimate';
}
elsif ($tabViewData{'documentType'} eq 'contract')
{
    $additionalNote = $locale->t('figurent sur le contrat');
    $prefix = 'rentContract';
}
# Traductions pour le javascript
$view->addTranslations({
    'commentCapacityOver' => $locale->t('Capacité de la zone commentaire dépassée'),
    'trackingCapacityOver' => $locale->t('Capacité de la zone suivi dépassée'),
});

# Commentaires
$commentsInput = $view->displayFormTextArea($prefix . 'Comments', '', {'data-maxlength' => 250});
$view->addPageInput({'sel' => '#' . $prefix . 'Comments'});

# Suivi
$trackingInput = $view->displayFormTextArea($prefix . 'Tracking', '', {'data-maxlength' => 65535});
$view->addPageInput({'sel' => '#' . $prefix . 'Tracking'});


print '
<fieldset id="rentCommentsFieldset">
    <legend>' . $view->displayImage('rent/helper/comments/icon.png') . '&nbsp;' .
                $locale->t('Informations complémentaires') . '</legend>

    <table class="formTable" style="width: 100%;">
    <tr>
        <td class="label">' . $locale->t('Commentaires') . '*</td>
        <td class="label">' . $locale->t('Suivi') . '</td>
    </tr>
    <tr>
        <td>' . $commentsInput . '</td>
        <td>' . $trackingInput . '</td>
    </tr>
    <tr>
        <td><span style="font-style: italic; font-size: 7pt;">*' . $additionalNote . '</span></td>
        <td></td>
    </tr>
    </table>
</fieldset>';


1;
