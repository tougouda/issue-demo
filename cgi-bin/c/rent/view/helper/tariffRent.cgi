use utf8;
use open (':encoding(UTF-8)');

# File: rentTariff.cgi
#
# Affichage du bloc de tarification location

# Symbole champ requis
my $requiredIcon = $view->displayRequiredSymbol();

my $amountTypeInput = $view->displayFormSelect($prefix . 'AmountType', [], '', 0);
$view->addPageInput({
    'sel'  => '#' . $prefix . 'AmountType',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});

my $baseAmountInput = $view->displayFormInputCurrency($prefix . 'BaseAmount', 0, LOC::Html::CURRENCYMODE_TEXT,
                                                      $currencySymbol, 0);

my $reductionLabel = htmllocale('%s %% soit %s');

# Remise standard
my $stdReducInput = $view->displayFormInputText($prefix . 'StdReduc', 0, '', {'class' => 'reduction'});
$view->addPageInput({
    'sel'  => '#' . $prefix . 'StdReduc',
    'get'  => 'Location.spinControlsManager.getSpinControl("' . $prefix . 'StdReduc")',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});
my $stdReducAmountInput = $view->displayFormInputCurrency($prefix . 'StdReducAmount', 0,
                                                          LOC::Html::CURRENCYMODE_INPUT, 
                                                          $currencySymbol, undef,
                                                          {'class' => 'reductionAmount'});
$view->addPageInput({
    'sel'  => '#' . $prefix . 'StdReducAmount',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});

my $recoMaxiLabel = htmllocale('Reco. %s, max. %s, soit %s');
my $recoPcInput = '<span id="' . $prefix . 'RecoReduc"></span> %';
my $maxiPcInput = '<span id="' . $prefix . 'MaxiReduc"></span> %';
my $maxiAmountInput = $view->displayFormInputCurrency($prefix . 'MaxiAmount', 0, LOC::Html::CURRENCYMODE_TEXT,
                                                      $currencySymbol, 0);


# Remise exceptionnelle
my $excReducInput = $view->displayFormInputText($prefix . 'SpcReduc', 0, '', {'class' => 'reduction'});
$view->addPageInput({
    'sel'  => '#' . $prefix . 'SpcReduc',
    'get'  => 'Location.spinControlsManager.getSpinControl("' . $prefix . 'SpcReduc")',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});
my $excReducAmountInput = $view->displayFormInputCurrency($prefix . 'SpcReducAmount', 0,
                                                          LOC::Html::CURRENCYMODE_POSITIVEINPUT, 
                                                          $currencySymbol, undef,
                                                          {'class' => 'reductionAmount'});
$view->addPageInput({
    'sel'  => '#' . $prefix . 'SpcReducAmount',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});
my $excReducBillLabel = $view->displayFormInputText($prefix . 'SpcReducBillLabel', 0, '', {'class' => 'spcReducBillLabel'});
$view->addPageInput({
    'sel'  => '#' . $prefix . 'SpcReducBillLabel',
    '*if'  => '!this.readOnly && setDocumentActiveTabIndex(' . $tabIndex . ')'
});
my $excReducJustif = $view->displayFormTextArea($prefix . 'SpcReducJustification', '', {'class' => 'spcReducJustification'});
$view->addPageInput({
    'sel'  => '#' . $prefix . 'SpcReducJustification',
    '*if'  => '!this.readOnly && setDocumentActiveTabIndex(' . $tabIndex . ')'
});

# Remise globale
my $reducInput = $view->displayFormInputText($prefix . 'Reduc', 0, '', {'class' => 'reduction'});
$view->addPageInput({
    'sel'  => '#' . $prefix . 'Reduc',
    'get'  => 'Location.spinControlsManager.getSpinControl("' . $prefix . 'Reduc")',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});
my $reducAmountInput = $view->displayFormInputCurrency($prefix . 'ReducAmount', 0,
                                                       LOC::Html::CURRENCYMODE_INPUT, 
                                                       $currencySymbol, undef,
                                                       {'class' => 'reductionAmount'});
$view->addPageInput({
    'sel'  => '#' . $prefix . 'ReducAmount',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});


my $amountInput = $view->displayFormInputCurrency($prefix . 'Amount', 0,
                                                  LOC::Html::CURRENCYMODE_POSITIVEINPUT, $currencySymbol, undef,
                                                  {'class' => 'estimationAmount', 'disabled' => 'disabled'});
$view->addPageInput({
    'sel'  => '#' . $prefix . 'Amount',
    '*if'  => 'setDocumentActiveTabIndex(' . $tabIndex . ')'
});

print '
<div title="' . htmllocale('Location') . '">

<table class="formTable tariff" id="' . $prefix . 'Table">
<tbody>
    <tr>
        <td colspan="4" class="label right">
            <label for="' . $prefix . 'AmountType">' . htmllocale('Type de montant') . '</label>
        </td>
        <td class="amount">' . $amountTypeInput . '</td>
        <td rowspan="2" style="padding-top: 5px;">
            <div class="warning" id="' . $prefix . 'TariffChangeMsg">' . $locale->t('Le tarif a changé') . '</div>
        </td>
    </tr>
    <tr>
        <td colspan="4" class="label right">' . htmllocale('Tarif de base') . '</td>
        <td class="amount">' . $baseAmountInput . '</td>
    </tr>
    <tr>
        <td colspan="6"><hr class="dotted"/></td>
    </tr>
    <tr>
        <td class="label">
            <label for="' . $prefix . 'StdReduc">' . htmllocale('Remise standard') . '</label>
        </td>
        <td class="right">' .
            sprintf($reductionLabel, $stdReducInput, $stdReducAmountInput) .
            $view->displayJSBlock('Location.spinControlsManager.createSpinControl("' . $prefix . 'StdReduc", null, 100, 1, 10, 3);') . '
        </td>
        <td rowspan="5" class="totalReductionSeparator"></td>
        <td rowspan="5" colspan="2" class="totalReduction">
            <label for="' . $prefix . 'Reduc" class="totalReduction">' . htmllocale('soit une réduction de') . '</label><br />' .
            sprintf($reductionLabel, $reducInput, $reducAmountInput) .
            $view->displayJSBlock('Location.spinControlsManager.createSpinControl("' . $prefix . 'Reduc", null, 100, 1, 10, 3);') . '
            <div class="warning" id="' . $prefix . 'ReducNegativeMsg">' . $locale->t('Remise négative') . '</div>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td></td>
        <td style="text-align: right; font-size: 7pt;">' . sprintf($recoMaxiLabel, $recoPcInput, $maxiPcInput, $maxiAmountInput) . '</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2"><hr class="dotted"/></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="label">
            <label for="' . $prefix . 'SpcReduc">' . htmllocale('Remise exceptionnelle') . '</label>
        </td>
        <td class="right">' .
            sprintf($reductionLabel, $excReducInput, $excReducAmountInput) .
            $view->displayJSBlock('Location.spinControlsManager.createSpinControl("' . $prefix . 'SpcReduc", 0, 100, 1, 10, 3);') . '
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" class="spcReducInfos">
            <table class="formTable spcReducInfos" id="' . $prefix . 'SpcReducInfos">
            <tr>
                <td>
                    <div class="spcReducStatus" id="' . $prefix . 'SpcReducStatus">
                        <div state="REM01"> ' . $view->displayState('REM01', $locale->t('Validée')) . ' </div>
                        <div state="REM02"> ' . $view->displayState('REM02', $locale->t('En attente')) . ' </div>
                        <div state="REM03"> ' . $view->displayState('REM03', $locale->t('Refusée')) . ' </div>
                    </div>
                </td>
                <td class="label" rowspan="2">
                    <label for="' . $prefix . 'SpcReducJustification">' . htmllocale('Justification') . $requiredIcon . '</label><br />' .
                    $excReducJustif . '
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="' . $prefix . 'SpcReducBillLabel">' . htmllocale('Libellé facture') . '</label><br />
                    ' . $excReducBillLabel . '
                </td>
            </tr>
            </table>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="6"><hr class="dotted"/></td>
    </tr>
    <tr class="finalPrice">
        <td colspan="4" class="label right"><label for="' . $prefix . 'Amount">' . htmllocale('Prix de location') . $requiredIcon . '</label>
        </td>
        <td class="amount">' . $amountInput . '</td>
        <td class="amountType"><span id="' . $prefix . 'AmountTypeLabel"></span></td>
    </tr>
</tbody>
</table>

</div>';


1;
