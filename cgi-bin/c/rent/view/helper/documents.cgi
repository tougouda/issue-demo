use utf8;
use open (':encoding(UTF-8)');

# File: documents.cgi
#
# Affichage du bloc pour les docs joints

my $tabDocInfos = $tabViewData{'tabDocumentInfos'};


# Constantes
$view->addJSSrc('
/* Modules pour les documents joints */
var ATTACHMENTS_MODULE_RENTCONTRACTDOCUMENT = "' . LOC::Attachments::MODULE_RENTCONTRACTDOCUMENT . '";
var ATTACHMENTS_MODULE_RENTESTIMATELINEDOCUMENT = "' . LOC::Attachments::MODULE_RENTESTIMATELINEDOCUMENT . '";
var ATTACHMENTS_MODULE_MACHINEINVENTORY = "' . LOC::Attachments::MODULE_MACHINEINVENTORY . '";
');


# Traductions
$view->addTranslations({
    $prefix . 'button_view_origin_estimate_line' => $locale->t('Voir les documents de la ligne de devis'),
    $prefix . 'button_view'                      => $locale->t('Visualiser'),
    $prefix . 'button_download'                  => $locale->t('Télécharger'),
    $prefix . 'button_edit'                      => $locale->t('Modifier'),
    $prefix . 'button_delete'                    => $locale->t('Supprimer'),
    $prefix . 'button_undo'                      => $locale->t('Rétablir'),
    $prefix . 'button_remove'                    => $locale->t('Annuler l\'ajout du document'),
    $prefix . 'button_view_photo_infos'          => $locale->t('Voir les informations sur la photo'),

    $prefix . 'checkbox_sendmail' => $locale->t('Sélectionner pour l\'envoi par e-mail'),

    $prefix . 'label_add'    => $locale->t('Demande d\'ajout'),
    $prefix . 'label_more_s' => $locale->t('<%nb> document supplémentaire...'),
    $prefix . 'label_more_p' => $locale->t('<%nb> documents supplémentaires...'),

    $prefix . 'title_machineInventory_D' => $locale->t('État des lieux de livraison n°<%id>'),
    $prefix . 'title_machineInventory_R' => $locale->t('État des lieux de récupération n°<%id>'),

    $prefix . 'error_maxSize'     => $locale->t('Taille supérieure à %s', $locale->getBytesFormat($tabViewData{'maxFileSize'}, 0)),
    $prefix . 'error_fatalError'  => $locale->t('Erreur de téléchargement'),
    $prefix . 'error_unknownFile' => $locale->t('Fichier vide'),

});

# Bouton d'ajout
my $addBtn = $view->displayTextButton($locale->t('Ajouter'),
                                      'add(|Over).png',
                                      'return false;',
                                      $locale->t('Ajouter')
                                      ,'l',
                                      {'id' => $prefix . 'addPopupBtn',
                                       'tabindex' => -1,
                                       'onfocus' => 'this.blur();'});

# Bouton d'envoi par mail
my $mailUrl = '#';
my $printBtns = $view->displayTextButton($locale->t('Envoyer par e-mail'),
                                            'base/printButtons/mail(|Over).png',
                                            'return false;',
                                            $locale->t('Envoyer par e-mail'),
                                            'l',
                                            {'id' => $prefix . 'mailBtn',
                                             'tabindex' => -1,
                                             'onfocus' => 'this.blur();',
                                             'class' => 'locCtrlButton disabled',
                                             'data-title' => $locale->t('Envoyer par e-mail'),
                                             'data-errortitle' => $locale->t('Le poids des fichiers dépasse le maximum autorisé pour l\'envoi d\'un mail (%s)' ,
                                                                             $locale->getBytesFormat($tabViewData{'maxMailAttachmentsSize'}, 0))
                                            });

my $fileExtension = $view->displayFileExtension($prefix . 'fileExtension');

print '
<div title="' . htmllocale('Documents') . '" id="' . $prefix . 'container">

    <fieldset class="sub list">
        <legend>'
            . htmllocale('Liste des documents') . '
        </legend>
        <div class="legendBtn">';

print '
            <div>' . $addBtn . '</div>
            <div>' . $printBtns . '</div>
        </div>
        <table class="basic ' . $prefix . 'list" id="' . $prefix . 'list">
            <thead>
                <tr>
                    <td class="type">' . htmllocale('Type') . $fileExtension . '</td>
                    <td class="title">' . htmllocale('Titre') . '</td>
                    <td class="size">' . htmllocale('Taille') . '</td>
                    <td class="date">' . htmllocale('Date de dépôt') . '</td>
                    <td class="origin"></td>
                    <td class="ctrls"></td>
                    <td class="editCtrls"></td>
                </tr>
            </thead>
            <tfoot class="no-document">
                <tr>
                    <td colspan="7">' . htmllocale('Aucun document rattaché') . '</td>
                </tr>
            </tfoot>
            <tbody class="list"></tbody>
            <tbody class="more"></tbody>';

print '
        </table>
    </fieldset>

    <!-- Documents en cours d\'ajout -->
    <fieldset class="sub add-list">
        <legend>En cours d\'ajout</legend>

        <table class="basic ' . $prefix . 'list" id="' . $prefix . 'list-add">
            <thead>
                <tr>
                    <td class="type">' . htmllocale('Type') . '</td>
                    <td class="title">' . htmllocale('Titre') . '</td>
                    <td class="progress"></td>
                    <td class="ctrls"></td>
                </tr>
            </thead>
            <tfoot class="no-document">
                <tr>
                    <td colspan="5">' . htmllocale('Aucun document en cours d\'ajout') . '</td>
                </tr>
            </tfoot>
            <tbody class="list"></tbody>
        </table>
    </fieldset>

    <!-- Drag and drop -->
    <div class="drag-layer">
        <p class="on">' . htmllocale('Déposer un fichier') . '</p>
        <p class="off">' . htmllocale('L\'ajout de fichier est désactivé') . '</p>
    </div>';

# Popup d'ajout de document
my $titleInput = $view->displayFormInputText($prefix . 'addTitle');
my $fileInput  = $view->displayFormInputFile($prefix . 'addFile');
my $fileInputButton = $view->displayTextButton('',
                                                'upload-white.png',
                                                'return false;', $locale->t('Choisissez un fichier'), 'l',
                                                {'id' => $prefix . 'addFileBtn'});
my $descInput  = $view->displayFormTextArea($prefix . 'addDescription','');
my $duplicateCheckbox = $view->displayFormInputCheckBox($prefix . 'addOnAllEstimateLines', 0, undef,
                                                         $locale->t('Ajouter sur toutes les lignes du devis'),
                                                         1);

# Bouton d'ajout
my $validBtn = $view->displayTextButton($locale->t('Ajouter') .  ' <span class="displayCounter"></span>',
                                      'add(|Over).png',
                                      'return false;',
                                      $locale->t('Ajouter')
                                      ,'l',
                                      {'id' => $prefix . 'addBtn',
                                       'tabindex' => -1,
                                       'onfocus' => 'this.blur();'});

my $fileError = $view->displayMessages('error',
                    [$locale->t('Aucun fichier sélectionné')]
                 , 0);

print '
    <div class="' . $prefix . 'popup" style="display: none;" id="' . $prefix . 'addPopup">
        <fieldset class="sub content">
            <legend>' . htmllocale('Ajouter un document') . '</legend>

            <table class="formTable">
                <tbody>
                    <tr class="file">
                        <td class="label">' . htmllocale('Fichier') . '</td>
                        <td class="content">
                            <div class="file-button">' .
                                $fileInput . $fileInputButton . '
                            </div>
                            <span id="' . $prefix . 'addFileName"></span>
                        </td>
                    </tr>
                    <tr class="title">
                        <td class="label">' . htmllocale('Titre') . '</td>
                        <td class="content">' . $titleInput . '</td>
                    </tr>
                    <tr class="description">
                        <td class="label">' . htmllocale('Description') . '</td>
                        <td class="content">' . $descInput . '</td>
                    </tr>';

if ($tabViewData{'documentType'} eq 'estimate')
{
    print '
                    <tr class="onAllEstimateLines">
                        <td class="label" colspan="2">' . $duplicateCheckbox . '</td>
                    </tr>';
}
print '
                </tbody>
            </table>
        </fieldset>
        <fieldset class="sub ctrls">
            ' . $validBtn . '
            <span id="' . $prefix . 'error-noFile">' .
                $fileError . '
            </span>
        </fieldset>
        <div class="drag-layer">
            <p>' . htmllocale('Déposer un fichier') . '</p>
        </div>
        <!-- Enregistrement en cours -->
        <div class="loading-layer"></div>
    </div>';


# Popup de modification de document
my $titleInput = $view->displayFormInputText($prefix . 'editTitle');
my $descInput  = $view->displayFormTextArea($prefix . 'editDescription','');

# Bouton de modification
my $editBtn = $view->displayTextButton($locale->t('Modifier'),
                                       'edit(|Over).png',
                                       'return false;',
                                       $locale->t('Modifier'),
                                       'l',
                                       {'id' => $prefix . 'editBtn',
                                        'tabindex' => -1,
                                        'onfocus' => 'this.blur();'});

print '
    <div class="' . $prefix . 'popup" style="display: none;" id="' . $prefix . 'editPopup">
        <fieldset class="sub content">
            <legend>' . htmllocale('Modifier un document') . '</legend>

            <table class="formTable">
                <tbody>
                    <tr class="file">
                        <td class="label">' . htmllocale('Fichier') . '</td>
                        <td class="content">
                            <span id="' . $prefix . 'editFileName"></span>
                        </td>
                    </tr>
                    <tr class="title">
                        <td class="label">' . htmllocale('Titre') . '</td>
                        <td class="content">' . $titleInput . '</td>
                    </tr>
                    <tr class="description">
                        <td class="label">' . htmllocale('Description') . '</td>
                        <td class="content">' . $descInput . '</td>
                    </tr>

                </tbody>
            </table>
        </fieldset>
        <fieldset class="sub ctrls" id="' . $prefix . 'editPopup">
            ' . $editBtn . '
        </fieldset>
    </div>';


# popup de téléversement en cours des documents
print '
    <div style="display: none;" id="' . $prefix . 'pendingModal" class="confirmModal">
        <h3>Des documents sont en cours de téléversement.</h3>
        <ul class="choices">
            <li class="force">
                <div class="icon"></div>
                <div class="text">
                    <span id="' . $prefix . 'pendingModalForceMsg"></span>
                </div>
                <div class="ctrls"><button type="button"><span>' . htmllocale('Choisir') . '</span></button></div>
            </li>
            <li class="wait disabled">
                <div class="icon"></div>
                <div class="text">
                    <span id="' . $prefix . 'pendingModalWaitMsg"></span>
                    <progress value="0" max="100"></progress>
                </div>
                <div class="ctrls"><button type="button" disabled><span>' . htmllocale('Choisir') . '</span></button></div>
            </li>
            <li class="none">
                <div class="icon"></div>
                <div class="text">
                    <span id="' . $prefix . 'pendingModalBackMsg"></span>
                </div>
                <div class="ctrls"><button type="button"><span>' . htmllocale('Choisir') . '</span></button></div>
            </li>
        </ul>
    </div>';

print '
</div>';

1;
