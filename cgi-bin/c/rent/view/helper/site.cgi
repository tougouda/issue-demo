use utf8;
use open (':encoding(UTF-8)');

# File: site.cgi
#
# Affichage du bloc chantier sur le devis et le contrat.
#

# Traductions pour le javascript
$view->addTranslations({
    'addressCapacityOver' => $locale->t('Capacité de la zone adresse dépassée')
});

# Symbole champ requis
my $requiredIcon = $view->displayRequiredSymbol();

# Zone
my $zone = '';
if ($tabViewData{'isTrspTarification'})
{
    $zone .= '<div id="' . $prefix . 'LabelZone">' . $locale->t('Zone') . ' &nbsp;'
              . $view->displayZone('?', $prefix . 'Zone') . '</div>';
}

# Premier onglet
$tabIndex = SITETABINDEX_GENERAL; # Id de l'onglet

# Libellé
my $labelInput = $view->displayFormInputText($prefix . 'Label', '', '', {'maxlength' => 50});
$view->addPageInput({
    'sel' => '#' . $prefix . 'Label',
    '*if' => 'setSiteActiveTabIndex(' . $tabIndex . ')'
});

# Adresse
my $addressInput = $view->displayFormTextArea($prefix . 'Address', '', {'data-maxlength' => 80});
$view->addPageInput({
    'sel' => '#' . $prefix . 'Address',
    '*if' => 'setSiteActiveTabIndex(' . $tabIndex . ')'
});

# Ville chantier
my $localityInput = $view->displayFormSelect($prefix . 'LocalityId', {}, [], 0);

$view->addPageInput({
    'sel' => '#' . $prefix . 'LocalityId\.search, #' . $prefix . 'LocalityId',
    '*if' => 'setSiteActiveTabIndex(' . $tabIndex . ')',
    'get' => 'Location.searchBoxesManager.getSearchBox("' . $prefix . 'LocalityId")'
});

# Contact
my $contactInput = $view->displayFormInputText($prefix . 'ContactFullName', '', '', {'maxlength' => 40});
$view->addPageInput({
    'sel' => '#' . $prefix . 'ContactFullName',
    '*if' => 'setSiteActiveTabIndex(' . $tabIndex . ')'
});

# Téléphone
my $tel = ($tabViewData{'documentType'} eq 'contract' ? $tabViewData{'tabContractInfos'}->{'tabSite'}->{'contactTelephone'} :
                                                        $tabViewData{'tabEstimateInfos'}->{'tabSite'}->{'contactTelephone'});
my $telInput = $view->displayFormInputPhone($prefix . 'ContactTelephone', $tel);
$view->addPageInput({
    'sel' => '#' . $prefix . 'ContactTelephone\.code',
    '*if' => 'setSiteActiveTabIndex(' . $tabIndex . ')'
});
$view->addPageInput({
    'sel' => '#' . $prefix . 'ContactTelephone\.masked',
    '*if' => 'setSiteActiveTabIndex(' . $tabIndex . ')'
});

# Email
my $emailInput = $view->displayFormInputText($prefix . 'ContactEmail', '', '', {'maxlength' => 70});
$view->addPageInput({
    'sel' => '#' . $prefix . 'ContactEmail',
    '*if' => 'setSiteActiveTabIndex(' . $tabIndex . ')'
});
$emailInput .= '
    <a href="#" id="site.email.send" class="hidden" mytitle="' . htmllocale('Envoyer un e-mail à %s') . '" title="">' .
        $view->displayImage('mail(|Over).png') .
   '</a>';

# Amiante
my $isAsbestosInput = $view->displayFormInputCheckBox($prefix . 'IsAsbestos', 0, {'id' => $prefix . 'IsAsbestos'}, '', 1);
$view->addPageInput({
    'sel' => '#' . $prefix . 'IsAsbestos',
    '*if' => 'setSiteActiveTabIndex(' . $tabIndex . ')'
});


# Second onglet
$tabIndex = SITETABINDEX_INFOS; # Id de l'onglet

# Livraison
my $deliveryInput = $view->displayMaskedFormInputText($prefix . 'DeliveryHour', '',
                                                      {'maxlength' => 5, 'style' => 'text-align: right;',
                                                       'format' => '__:__', 'separator' => ':', 'typeon' => '_',
                                                       'allowed' => '0123456789'});
$view->addPageInput({
    'sel' => '#' . $prefix . 'DeliveryHour\.masked',
    '*if' => 'setSiteActiveTabIndex(' . $tabIndex . ')'
});

my $deliveryTimeSlotInput = $view->displayFormSelect($prefix . 'DeliveryTimeSlotId', {}, [], 0);
$view->addPageInput({
    'sel' => '#' . $prefix . 'DeliveryTimeSlotId',
    '*if' => 'setSiteActiveTabIndex(' . $tabIndex . ')'
});

# Anticipation
my $isAnticipatedInput = $view->displayFormInputCheckBox($prefix . 'IsAnticipated', 0, {'id' => $prefix . 'IsAnticipated'}, $locale->t('Livraison anticipée possible'), 1);
$view->addPageInput({
    'sel' => '#' . $prefix . 'IsAnticipated',
    '*if' => 'setSiteActiveTabIndex(' . $tabIndex . ')'
});


# Récupération
my $recoveryInput = $view->displayMaskedFormInputText($prefix . 'RecoveryHour', '',
                                                      {'maxlength' => 5, 'style' => 'text-align: right;',
                                                       'format' => '__:__', 'separator' => ':', 'typeon' =>'_',
                                                       'allowed' => '0123456789'});
$view->addPageInput({
    'sel' => '#' . $prefix . 'RecoveryHour\.masked',
    '*if' => 'setSiteActiveTabIndex(' . $tabIndex . ')'
});

my $recoveryTimeSlotInput = $view->displayFormSelect($prefix . 'RecoveryTimeSlotId', {}, [], 0);
$view->addPageInput({
    'sel' => '#' . $prefix . 'RecoveryTimeSlotId',
    '*if' => 'setSiteActiveTabIndex(' . $tabIndex . ')'
});

# Quai
my $isHasWharfInput = $view->displayFormInputCheckBox($prefix . 'IsHasWharf', 0, {'id' => $prefix . 'IsHasWharf'}, '', 1);
$view->addPageInput({
    'sel' => '#' . $prefix . 'IsHasWharf',
    '*if' => 'setSiteActiveTabIndex(' . $tabIndex . ')'
});



# Commentaire
my $commentsInput = $view->displayFormTextArea($prefix . 'Comments', '', {'maxlength' => 250});
$view->addPageInput({
    'sel' => '#' . $prefix . 'Comments',
    '*if' => 'setSiteActiveTabIndex(' . $tabIndex . ')'
});

# Aide sur la recherche de ville
my $localityHelp = $view->displayHelp($locale->t('La recherche de ville s\'effectue parmi le nom de la localité, le code postal et le département '));

print '
<fieldset id="' . $prefix . 'Fieldset">
    <legend>' . $view->displayImage('rent/helper/site/icon.png') . '&nbsp;' . $locale->t('Chantier') . '</legend>

    <div id="' . $prefix . 'Block">
        <div class="loadingMask"></div>
        <div class="content">
            <div id="' . $prefix . 'Controls">' . $zone . '</div>
            <div id="' . $prefix . 'TabBox">';

# Onglet Général
print '
                <div title="' . htmllocale('Général') . '">
                    <table class="formTable">
                    <tr class="siteLabel">
                        <td class="label">' . $locale->t('Libellé chantier') . $requiredIcon . '</td>
                        <td class="content">' . $labelInput . '</td>
                    </tr>
                    <tr class="address">
                        <td class="label">' . $locale->t('Adresse') . '</td>
                        <td class="content">' . $addressInput . '</td>
                    </tr>
                    <tr class="locality">
                        <td class="label">' . $locale->t('Ville') . $requiredIcon . $localityHelp . '</td>
                        <td class="content">' . $localityInput . '</td>
                    </tr>
                    <tr class="contact">
                        <td class="label">' . $locale->t('Contact') . '</td>
                        <td class="content">' . $contactInput . '</td>
                    </tr>
                    <tr class="tel">
                        <td class="label">' . $locale->t('Téléphone') . '</td>
                        <td style="white-space: nowrap;" class="content">' . $telInput . '</td>
                    </tr>
                    <tr class="email">
                        <td class="label">' . $locale->t('E-mail') . '</td>
                        <td class="content">' . $emailInput . '</td>
                    </tr>
                    <tr class="asbestos">
                        <td class="label">' . $locale->t('Amiante') . '</td>
                        <td class="content">' . $isAsbestosInput . '</td>
                    </tr>
                    </table>
            </div>';

# Onglet Informations transport
print '
                <div title="' . htmllocale('Informations transport'). '">
                    <table class="formTable">
                    <tr class="deliveryHour">
                        <td class="label">' . $locale->t('Livraison à') . ($tabViewData{'documentType'} eq 'contract' ? $requiredIcon : '') . '</td>
                        <td>' . $deliveryInput . '&nbsp;' . $deliveryTimeSlotInput . '
                            <div id="' . $prefix . 'DeliveryLimitHour">' .
                                $locale->t('au plus tôt') . ' : <span id="' . $prefix . 'DeliveryLimitHour_content"></span>
                            </div>
                        </td>
                        <td class="label" style="padding-left: 30px">' . $isAnticipatedInput . '</td>
                    </tr>
                    <tr class="recoveryHour">
                        <td class="label">' . $locale->t('Récupération à') . '</td>
                        <td colspan="2">' . $recoveryInput . '&nbsp;' . $recoveryTimeSlotInput . '
                            <div id="' . $prefix . 'RecoveryLimitHour">' .
                                $locale->t('au plus tard') . ' : <span id="' . $prefix . 'RecoveryLimitHour_content"></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">' . $locale->t('Quai') . '</td>
                        <td colspan="2">' . $isHasWharfInput . '</td>
                    </tr>
                    <tr class="comment">
                        <td class="label">' . $locale->t('Commentaires chantier') . '</td>
                        <td colspan="2">' . $commentsInput . '</td>
                    </tr>
                    </table>
                </div>';
print '
            </div>
        </div>
    </div>
</fieldset>';


print $view->displayJSBlock('
var localitySearchBox = Location.searchBoxesManager.createSearchBox("' . $prefix . 'LocalityId", {url:" ' . $tabViewData{'siteLocalitiesSearchUrl'} . '"});
localitySearchBox.createElement = function(key, value)
{
    var opt = new Option(value["fullName"], value["id"]);
    opt.setAttribute("data-zone", value["zone"]);
    return opt;
}
Location.tabBoxesManager.createTabBox("' . $prefix . 'TabBox", null, "272px");
function setSiteActiveTabIndex(index)
{
    return Location.tabBoxesManager.getTabBox("' . $prefix . 'TabBox").setActiveTabIndex(index);
}
');



1;