use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;
use LOC::Json;



# Répertoire CSS/JS/Images
our $dir = 'statistics/budget';


our %tabViewData;
our $inputsTabIndex = 1;


our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . '/view.css');

# Scripts JS
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc($dir . '/view.js');


# Traductions JavaScript
$view->addTranslations({
    'CtrlClick' => $locale->t('Ctrl+clic pour étendre ou réduire toutes les régions')
});


# Événement au chargement de la page
$view->addBodyEvent('load', 'budget.init();');


$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);


# Titre
my $title = $locale->t('Suivi du budget de la région');
if ($tabViewData{'type'} eq TYPE_COUNTRY)
{
    $title = $locale->t('Suivi du budget des régions');
}

$view->setPageTitle($title);
$view->setTitle($title);


# Bouton "Actualiser"
my $msg = $locale->t('Voulez-vous vraiment actualiser sans enregistrer les attributions en cours ?');
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png',
                                          'budget.submitForm();',
                                          $locale->t('Actualiser la page'));
$view->addControlsContent([$refreshBtn], 'right', 1);


# Affichage de l'entête
print $view->displayHeader();


print '
<div id="budget">';


if (!defined $tabViewData{'data'})
{
    my $message = $locale->t('Nous ne disposons d\'aucune donnée à afficher.');

    if ($tabViewData{'type'} eq TYPE_AREA)
    {
        $message = $locale->t('Nous ne disposons d\'aucune donnée pour votre région.');
    }
    elsif ($tabViewData{'type'} eq TYPE_COUNTRY)
    {
        $message = $locale->t('Nous ne disposons d\'aucune donnée pour vos régions.');
    }

    print $view->displayMessages('info', [$message], 0);
}
else
{
    # Début du formulaire
    print $view->startForm(undef, undef, 'mainForm');


    # Messages
    print '
    <ul class="messages">';

    # Date de calcul des données
    if ($tabViewData{'calculationDate'} ne '')
    {
        print '
        <li>' . $locale->t(
                'Données du %s à %s',
                $locale->getDateFormat($tabViewData{'calculationDate'}, LOC::Locale::FORMAT_DATE_TEXT),
                $locale->getDateFormat($tabViewData{'calculationDate'}, LOC::Locale::FORMAT_TIMESHORT_NUMERIC)
            ) . '</li>';
    }

    # Mois passé non facturé
    if (!$tabViewData{'isCurrentMonth'} && !$tabViewData{'isComplete'})
    {
        print '
        <li class="warning">';

        print $locale->t('La facturation mensuelle n\'ayant pas encore été effectuée, les données affichées peuvent s\'avérer inexactes.');

        print '
        </li>';
    }

    print '
    </ul>';


    # Tableau des données
    print '
    <table class="basic fullWidth">';


    # En-tête du tableau
    print '
        <thead>
            <tr>
                <th colspan="3" rowspan="2">';

    &displayMonthSelector();

    print '
                </th>';

    foreach my $type (keys(%{$tabViewData{'types'}}))
    {
        &displayTypeHeader($tabViewData{'types'}->{$type});
    }
    &displayTypeHeader($locale->t('Total machines'));

    print'
                <th colspan="2" rowspan="2">CA Total<br>fin de mois</th>
            </tr>
            <tr>';

    my $nbTypes = scalar values(%{$tabViewData{'types'}});
    for (my $i = 0; $i < $nbTypes + 1; $i++)
    {
        &displayTypeSubHeader();
    }

    print '
            </tr>
        </thead>';


    # Corps du tableau
    print '
        <tbody>';


    foreach my $area (values(%{$tabViewData{'data'}->{'country'}->{'areas'}}))
    {
        # Total région
        &displayLine(
            $area->{'infos'}->{'label'},
            $area->{'data'},
            2    # Niveau 2
        );

        # Agences
        my $nbAgencies = values(%{$area->{'agencies'}});
        if ($nbAgencies > 0)
        {
            # Séparateur
            &displaySeparator(3);

            foreach my $agency (values(%{$area->{'agencies'}}))
            {
                &displayLine(
                    $agency->{'infos'}->{'id'} . '<div class="subLabel">' . $agency->{'infos'}->{'label'} . '</div>',
                    $agency->{'data'},
                    3    # Niveau 3
                );
            }
        }

        # Séparateur
        &displaySeparator(2);
    }


    # Total pays
    my $country = $tabViewData{'data'}->{'country'};
    &displayLine(
        $view->displayCountryImage($country->{'infos'}->{'id'}) . ' ' . uc($country->{'infos'}->{'label'}),
        $country->{'data'},
        1    # Niveau 1
    );

    print '
        </tbody>';


    # Fin du tableau
    print '
    </table>';


    # Fin du formulaire
    print $view->endForm();
}


print &displayTariffFamilies();


print '
</div>';


# Affichage du pied de page
print $view->displayFooter();



sub displayMonthSelector
{
    my @todayLocalTime = localtime();

    # Mois en cours
    my $thisMonth = sprintf('%04d-%02d', $todayLocalTime[5] + 1900, $todayLocalTime[4] + 1);

    # Mois précédent
    my $lastMonthYear = $todayLocalTime[5] + 1900;
    my $lastMonthMonth = $todayLocalTime[4];
    if ($lastMonthMonth == 0)
    {
        $lastMonthYear--;
        $lastMonthMonth = 12;
    }
    my $lastMonth = sprintf('%04d-%02d', $lastMonthYear, $lastMonthMonth);

    # Formatage de la liste des mois disponibles
    my $tabMonths = {};
    foreach my $date (reverse @{$tabViewData{'monthsList'}})
    {
        my $year  = substr($date, 0, 4) * 1;
        my $month = substr($date, 5, 2) * 1;

        if (!defined $tabMonths->{$year})
        {
            $tabMonths->{$year} = [];
        }

        my $firstDayDate = sprintf('%04d-%02d-01', $year, $month);

        my $label = ucfirst($locale->getDateFormat($firstDayDate, LOC::Locale::FORMAT_MONTHYEAR_TEXT));
        if (substr($firstDayDate, 0, 7) eq $thisMonth)
        {
            $label = $locale->t(
                '%s (ce mois-ci)',
                ucfirst($locale->getDateFormat($firstDayDate, LOC::Locale::FORMAT_MONTHYEAR_TEXT))
            );
        }
        elsif (substr($firstDayDate, 0, 7) eq $lastMonth)
        {
            $label = $locale->t(
                '%s (le mois dernier)',
                ucfirst($locale->getDateFormat($firstDayDate, LOC::Locale::FORMAT_MONTHYEAR_TEXT))
            );
        }

        push(@{$tabMonths->{$year}}, {
            'date'  => substr($firstDayDate, 0, 8) . '00',
            'label' => $label
        });
    }

    print '
<select id="month" name="month">';

    foreach my $year (reverse sort keys(%$tabMonths))
    {
        print '
    <optgroup label="' . $year . '">';

        foreach my $month (@{$tabMonths->{$year}})
        {
            my $selected = ($month->{'date'} eq $tabViewData{'month'} ? ' selected' : '');
            print '
        <option value="' . $month->{'date'} . '"' . $selected . '>' . $month->{'label'} . '</option>';
        }

        print '
        </optgroup>';
    }

    print '
</select>
<div class="pagination">
    <ul>
        <li id="previousMonth" class="btn previous" title="' . $locale->t('Mois précédent') . '"></li>
        <li id="nextMonth" class="btn next" title="' . $locale->t('Mois suivant') . '"></li>
    </ul>
</div>';
}

sub displayTypeHeader
{
    my ($label) = @_;

    print '
<th colspan="8">' . $label . '</th>';
}

sub displayTypeSubHeader
{
    print '
<th colspan="2">' . $locale->t('Parc') . '</th>
<th colspan="2">' . $locale->t('Tx util.') . '</th>
<th colspan="2">' . $locale->t('Px loc.') . '<br>' . $locale->t('fin de mois') . '</th>
<th colspan="2">' . $locale->t('CA loc') . '.<br>' . $locale->t('fin de mois') . '</th>';
}

sub formatData
{
    my ($value, $format) = @_;

    if ($format eq 'number')
    {
        return $locale->getNumberFormat($value);
    }
    elsif ($format eq 'percent')
    {
        return $locale->t('%s %%', $locale->getNumberFormat($value * 100));
    }
    elsif ($format eq 'amount')
    {
        return $locale->getCurrencyFormat($value);
    }
    elsif ($format eq 'roundedAmount')
    {
        return $locale->getCurrencyFormat($value, 0);
    }
    return $value;
}

sub displayData
{
    my ($class, $typeEntry, $property, $format) = @_;

    print '
<td class="number ' . $class . '">
    <ul>';

    foreach my $subType (keys(%{$tabViewData{'subTypes'}}))
    {
        if (defined $typeEntry->{$subType} &&
            defined $typeEntry->{$subType}->{$property})
        {
            print '
        <li>' . &formatData($typeEntry->{$subType}->{$property}, $format) . '</li>';
        }
    }

    print '
    </ul>
</td>
<td class="number ' . $class . '">' . &formatData($typeEntry->{'total'}->{$property}, $format) . '</td>';
}

sub displayLine
{
    my ($label, $entry, $level) = @_;

    my $trClass = sprintf('level%d', $level);
    my $isHidden = ($level == 3 && $tabViewData{'collipsable'} eq COLLIPSABLE_COLLAPSED);
    my $doesCollapseChildren = ($level == 2);

    print '
<tr class="' . $trClass . '" style="' . ($isHidden ? 'display: none;' : '') . '">';

    my @tdClasses = ('label');
    if ($doesCollapseChildren)
    {
        push(@tdClasses, 'collipsable');
        if ($tabViewData{'collipsable'} eq COLLIPSABLE_COLLAPSED)
        {
            push(@tdClasses, 'expand');
        }
        if ($tabViewData{'collipsable'} eq COLLIPSABLE_EXPANDED)
        {
            push(@tdClasses, 'collapse');
        }
    }

    print '
    <td class="' . join(' ', @tdClasses) . '">';

    if ($doesCollapseChildren)
    {
        print '<button></button>';
    }

    print $label . '</td>
    <td class="label">
        <ul>';

    foreach my $subType (keys(%{$tabViewData{'subTypes'}}))
    {
        print '
            <li>' . $tabViewData{'subTypes'}->{$subType} . '</li>';
    }

    print '
    </ul>
</td>
<td class="label">' . $locale->t('Total') . '</td>';

    my $index = 0;
    foreach my $type (keys(%{$tabViewData{'types'}}))
    {
        if (defined $entry->{$type})
        {
            my $class = ($index % 2 ? '' : 'odd');

            &displayData($class, $entry->{$type}, 'machinesCount', 'number');
            &displayData($class, $entry->{$type}, 'usePc', 'percent');
            &displayData($class, $entry->{$type}, 'monthRentalPrice', 'amount');
            &displayData($class, $entry->{$type}, 'monthRentalTurnover', 'roundedAmount');
        }

        $index++;
    }

    # Total
    my $class = 'total';

    &displayData($class, $entry->{'total'}, 'machinesCount', 'number');
    &displayData($class, $entry->{'total'}, 'usePc', 'percent');
    &displayData($class, $entry->{'total'}, 'monthRentalPrice', 'amount');
    &displayData($class, $entry->{'total'}, 'monthRentalTurnover', 'roundedAmount');

    # CA Total
    my $class = 'turnover';
    &displayData($class, $entry->{'total'}, 'monthTotalTurnover', 'roundedAmount');

    print '
</tr>';
}

sub displaySeparator
{
    my ($level) = @_;

    my $isHidden = ($level == 3 && $tabViewData{'collipsable'} eq COLLIPSABLE_COLLAPSED);

    print '
<tr class="separator" style="' . ($isHidden ? 'display: none;' : '') . '">
    <td colspan="29"></td>
</tr>';
}

sub displayTariffFamilies
{
    my $tariffFamilies = '
<ul class="tariffFamilies">';

    foreach my $type (keys(%{$tabViewData{'types'}}))
    {
        my $typeConfig = $tabViewData{'config'}->{$type};

        $tariffFamilies .= '
    <li>' . $locale->t('%s :', $tabViewData{'types'}->{$type});

        if (defined $typeConfig->{'subTypes'})
        {
            $tariffFamilies .= '
        <ul>';

            foreach my $subType (keys(%{$tabViewData{'subTypes'}}))
            {
                my $subTypeConfig = $typeConfig->{'subTypes'}->{$subType};

                $tariffFamilies .= '
            <li>' . $locale->t('%s :', $tabViewData{'subTypes'}->{$subType}) . '
                <ul class="list">';

                foreach my $tariffFamily (sort {
                    $tabViewData{'tariffFamilies'}->{$a} cmp $tabViewData{'tariffFamilies'}->{$b}
                } @{$subTypeConfig->{'tariffFamilies'}})
                {
                    $tariffFamilies .= '
                    <li>' . $tabViewData{'tariffFamilies'}->{$tariffFamily} . '</li>';
                }

                $tariffFamilies .= '
                </ul>
            </li>';
            }

            $tariffFamilies .= '
        </ul>';
        }
        else
        {
            $tariffFamilies .= '
        <ul class="list">';

            foreach my $tariffFamily (sort {
                $tabViewData{'tariffFamilies'}->{$a} cmp $tabViewData{'tariffFamilies'}->{$b}
            } @{$typeConfig->{'tariffFamilies'}})
            {
                $tariffFamilies .= '
                <li>' . $tabViewData{'tariffFamilies'}->{$tariffFamily} . '</li>';
            }

            $tariffFamilies .= '
        </ul>';
        }

        $tariffFamilies .= '
    </li>';
    }

    $tariffFamilies .= '
</ul>';

    return $tariffFamilies;
}
