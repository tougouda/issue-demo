use utf8;
use open (':encoding(UTF-8)');

# package: budget
# Suivi du budget
package budget;


# Constants: Mode de repli des régions
# COLLIPSABLE_COLLAPSED - Toutes les régions repliées
# COLLIPSABLE_EXPANDED  - Toutes les régions dépliées
use constant
{
    TYPE_AREA    => 'area',
    TYPE_COUNTRY => 'country'
};


# Constants: Mode de repli des régions
# COLLIPSABLE_COLLAPSED - Toutes les régions repliées
# COLLIPSABLE_EXPANDED  - Toutes les régions dépliées
use constant
{
    COLLIPSABLE_COLLAPSED => 'collapsed',
    COLLIPSABLE_EXPANDED  => 'expanded'
};


use strict;
use File::Basename;
use LOC::Area;
use LOC::Country;
use LOC::Statistics::Budget;
use LOC::TariffFamily;


sub _getCalculationDate
{
    my ($agenciesData) = @_;

    my $calculationDate;

    foreach my $agencyEntry (values(%$agenciesData))
    {
        if ($agencyEntry->{'calculationDate'} gt $calculationDate)
        {
            $calculationDate = $agencyEntry->{'calculationDate'};
        }
    }

    return $calculationDate;
}

sub _getIsComplete
{
    my ($agenciesData) = @_;

    foreach my $agencyEntry (values(%$agenciesData))
    {
        if (!$agencyEntry->{'isComplete'})
        {
            return 0;
        }
    }

    return 1;
}

sub _getIsCurrentMonth
{
    my ($month) = @_;

    my @todayLocalTime = localtime();

    # Mois en cours
    my $thisMonth = sprintf('%04d-%02d-00', $todayLocalTime[5] + 1900, $todayLocalTime[4] + 1);

    return ($month eq $thisMonth);
}

sub _getTypesList
{
    my ($agenciesData, $config) = @_;

    tie(my %tabTypes, 'Tie::IxHash');
    %tabTypes = ();

    foreach my $agencyEntry (values(%$agenciesData))
    {
        foreach my $data (@{$agencyEntry->{'data'}})
        {
            my $label = $data->{'type'};
            if (defined $config->{$data->{'type'}}->{'label'})
            {
                $label = $config->{$data->{'type'}}->{'label'};
            }
            $tabTypes{$data->{'type'}} = $label;
        }
    }

    return \%tabTypes;
}

sub _getSubTypesList
{
    my ($agenciesData, $config) = @_;

    tie(my %tabSubTypes, 'Tie::IxHash');
    %tabSubTypes = ();

    foreach my $agencyEntry (values(%$agenciesData))
    {
        foreach my $data (@{$agencyEntry->{'data'}})
        {
            if (defined $data->{'subType'})
            {
                my $label = $data->{'subType'};
                if (defined $config->{$data->{'type'}}->{'subTypes'}->{$data->{'subType'}}->{'label'})
                {
                    $label = $config->{$data->{'type'}}->{'subTypes'}->{$data->{'subType'}}->{'label'};
                }
                $tabSubTypes{$data->{'subType'}} = $label;
            }
        }
    }

    return \%tabSubTypes;
}

sub _initTotal
{
    return {
        'machinesCount'       => 0,
        'rentalDays'          => 0,
        'monthRentalDays'     => 0,
        'monthRentalTurnover' => 0,
        'monthTotalTurnover'  => 0
    };
}

sub _addToTotal
{
    my ($total, $data) = @_;

    $total->{'machinesCount'} +=       $data->{'machinesCount'};
    $total->{'rentalDays'} +=          $data->{'rentalDays'};
    $total->{'monthRentalDays'} +=     $data->{'monthRentalDays'};
    $total->{'monthRentalTurnover'} += $data->{'monthRentalTurnover'};
    $total->{'monthTotalTurnover'} +=  $data->{'monthTotalTurnover'};
}

sub _calculateUsePc
{
    my ($data, $workingDays) = @_;

    $data->{'usePc'} = 0;
    if ($data->{'machinesCount'} && $workingDays)
    {
        $data->{'usePc'} = $data->{'rentalDays'} / ($data->{'machinesCount'} * $workingDays);
    }
}

sub _calculateMonthRentalPrice
{
    my ($data) = @_;

    $data->{'monthRentalPrice'} = 0;
    if ($data->{'monthRentalDays'})
    {
        $data->{'monthRentalPrice'} = $data->{'monthRentalTurnover'} / $data->{'monthRentalDays'};
    }
}

sub _processAgenciesData
{
    my ($agenciesData) = @_;

    tie(my %tabResult, 'Tie::IxHash');
    %tabResult = ();

    foreach my $agencyEntry (values(%$agenciesData))
    {
        # Répartition des données brutes
        my $formattedData = {};

        foreach my $data (@{$agencyEntry->{'data'}})
        {
            if (!defined $formattedData->{$data->{'type'}})
            {
                $formattedData->{$data->{'type'}} = {};
            }

            # Présence d'un sous-type
            if (defined $data->{'subType'})
            {
                # Calcul des données finales
                $formattedData->{$data->{'type'}}->{$data->{'subType'}} = {
                    'machinesCount'       => $data->{'machinesCount'},
                    'rentalDays'          => $data->{'rentalDays'},
                    'monthRentalDays'     => $data->{'monthRentalDays'},
                    'monthRentalTurnover' => $data->{'monthRentalTurnover'},
                    'monthTotalTurnover'  => $data->{'monthTotalTurnover'}
                };

# TODO: Supprimer cette partie
#                # Ajout au total par sous-type de l'agence
#                if (!defined $formattedData->{'total'}->{$data->{'subType'}})
#                {
#                    $formattedData->{'total'}->{$data->{'subType'}} = &_initTotal();
#                }
#                &_addToTotal($formattedData->{'total'}->{$data->{'subType'}}, $data);
            }

            # Ajout au total par type de l'agence
            if (!defined $formattedData->{$data->{'type'}}->{'total'})
            {
                $formattedData->{$data->{'type'}}->{'total'} = &_initTotal();
            }
            &_addToTotal($formattedData->{$data->{'type'}}->{'total'}, $data);

            # Ajout au total de l'agence
            if (!defined $formattedData->{'total'}->{'total'})
            {
                $formattedData->{'total'}->{'total'} = &_initTotal();
            }
            &_addToTotal($formattedData->{'total'}->{'total'}, $data);

            # Nombre de jours ouvrés dans le mois dans l'agence
            $formattedData->{'total'}->{'total'}->{'workingDays'} = $agencyEntry->{'workingDays'};
        }

        # Calculs à partir des données brutes
        foreach my $type (keys(%$formattedData))
        {
            foreach my $subType (keys(%{$formattedData->{$type}}))
            {
                # Pourcentage d'utilisation
                &_calculateUsePc($formattedData->{$type}->{$subType}, $agencyEntry->{'workingDays'});

                # Prix de location fin de mois
                &_calculateMonthRentalPrice($formattedData->{$type}->{$subType});
            }
        }

        $tabResult{$agencyEntry->{'agency.id'}} = $formattedData;
    }

    return \%tabResult;
}

sub _getTotalData
{
    my ($formattedData) = @_;

    tie(my %tabResult, 'Tie::IxHash');
    %tabResult = ();

    my %tabWeightedWorkingDays = ();

    foreach my $agencyFormattedData (values(%$formattedData))
    {
        foreach my $type (keys(%$agencyFormattedData))
        {
            if (!defined $tabResult{$type})
            {
                $tabResult{$type} = {};
            }

            if (!defined $tabWeightedWorkingDays{$type})
            {
                $tabWeightedWorkingDays{$type} = {};
            }

            foreach my $subType (keys(%{$agencyFormattedData->{$type}}))
            {
                # Ajout au total par type et sous-type de la région
                if (!defined $tabResult{$type}->{$subType})
                {
                    $tabResult{$type}->{$subType} = &_initTotal();
                }
                &_addToTotal($tabResult{$type}->{$subType}, $agencyFormattedData->{$type}->{$subType});

                # Ajout au nombre de jours pondéré
                if (!defined $tabWeightedWorkingDays{$type}->{$subType})
                {
                    $tabWeightedWorkingDays{$type}->{$subType} = 0;
                }
                $tabWeightedWorkingDays{$type}->{$subType} += $agencyFormattedData->{$type}->{$subType}->{'machinesCount'} *
                    $agencyFormattedData->{'total'}->{'total'}->{'workingDays'};
            }
        }
    }

    # Calculs à partir des données brutes
    foreach my $type (keys(%tabResult))
    {
        foreach my $subType (keys(%{$tabResult{$type}}))
        {
            # Pourcentage d'utilisation
            # (en passant le nombre de jours ouvrés moyen = nombre de jours pondéré / nombre de machines)
            &_calculateUsePc(
                $tabResult{$type}->{$subType},
                $tabWeightedWorkingDays{$type}->{$subType} / $tabResult{$type}->{$subType}->{'machinesCount'}
            );

            # Prix de location fin de mois
            &_calculateMonthRentalPrice($tabResult{$type}->{$subType});
        }
    }

    return \%tabResult;
}


# Function: viewAction
# Interface de suivi du budget région
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos  = $tabParameters->{'tabUserInfos'};
    my $currentUserId = $tabUserInfos->{'id'};
    my $countryId     = $tabUserInfos->{'nomadCountry.id'};


    # Variables pour la vue
    our %tabViewData = (
                        'locale.id'      => $tabUserInfos->{'locale.id'},
                        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
                        'type'           => $tabParameters->{'_type'},
                        'collipsable'    => $tabParameters->{'_collipsable'}
                       );


    # Informations sur l'agence de l'utilisateur
    my $tabAgencyInfos = &LOC::Agency::getInfos($tabUserInfos->{'nomadAgency.id'});

    # Agences concernées :
    # - toutes les agences de la région en mode "area"
    # - toutes les agences du pays en mode "country"
    my $tabAgenciesFilter = {
        'type' => LOC::Agency::TYPE_AGENCY
    };
    if ($tabViewData{'type'} eq TYPE_AREA)
    {
        $tabAgenciesFilter->{'area'} = $tabAgencyInfos->{'area.id'};
    }
    if ($tabViewData{'type'} eq TYPE_COUNTRY)
    {
        $tabAgenciesFilter->{'country'} = $tabAgencyInfos->{'country.id'};
    }
    my $tabAgencies = &LOC::Agency::getList(LOC::Util::GETLIST_ASSOC, $tabAgenciesFilter);
    my @tabAgenciesIds = keys(%$tabAgencies);


    # Régions concernées
    my $tabAreas = {};
    my @tabAreasIds = ();
    foreach my $agency (values(%$tabAgencies))
    {
        if (!LOC::Util::in_array($agency->{'area.id'}, \@tabAreasIds))
        {
            push(@tabAreasIds, $agency->{'area.id'});
            $tabAreas->{$agency->{'area.id'}} = {
                'id'    => $agency->{'area.id'},
                'code'  => $agency->{'area.code'},
                'label' => $agency->{'area.label'}
            };
        }
    }


    # Liste des mois disponibles
    my $tabEntries = &LOC::Statistics::Budget::getList(
        LOC::Util::GETLIST_PAIRS,
        {'agency' => \@tabAgenciesIds}
    );
    $tabViewData{'monthsList'} = &LOC::Util::array_unique([values(%$tabEntries)]);


    # Mois sélectionné
    my $currentMonth = substr(&LOC::Date::getMySQLDate(), 0, 8) . '00';
    $tabViewData{'month'} = &LOC::Request::getString('month', $currentMonth);


    # Configuration de la statistique
    # Date de valeur de la caractéristique = date du jour
    my $valueDate = &LOC::Date::getMySQLDate();
    if ($tabViewData{'month'} ne $currentMonth)
    {
        # Date de valeur = dernier jour du mois si mois passé
        $valueDate = &LOC::Date::getMonthLastDayDate($tabViewData{'month'});
    }
    my $config = &LOC::Characteristic::getCountryValueByCode('STABUDGET', $countryId, $valueDate);
    $tabViewData{'config'} = ($config eq '' ? {} : &LOC::Json::fromJson($config));


    # Récupération des libellés des familles tarifaires
    my @tariffFamiliesId = ();
    foreach my $type (keys(%{$tabViewData{'config'}}))
    {
        if (defined $tabViewData{'config'}->{$type}->{'subTypes'})
        {
            foreach my $subType (keys(%{$tabViewData{'config'}->{$type}->{'subTypes'}}))
            {
                push(@tariffFamiliesId, @{$tabViewData{'config'}->{$type}->{'subTypes'}->{$subType}->{'tariffFamilies'}});
            }
        }
        else
        {
            push(@tariffFamiliesId, @{$tabViewData{'config'}->{$type}->{'tariffFamilies'}});
        }
    }
    $tabViewData{'tariffFamilies'} = &LOC::TariffFamily::getList($countryId, LOC::Util::GETLIST_PAIRS, {'id' => \@tariffFamiliesId});


    # Agences utilisées pour le calcul des données du pays
    my $tabCountryAgencies = $tabAgencies;
    if ($tabViewData{'type'} ne TYPE_COUNTRY)
    {
        my $tabCountryAgenciesFilter = {
            'type' => LOC::Agency::TYPE_AGENCY,
            'country' => $tabAgencyInfos->{'country.id'}
        };
        $tabCountryAgencies = &LOC::Agency::getList(LOC::Util::GETLIST_ASSOC, $tabCountryAgenciesFilter);
    }


    # Données du mois sélectionné
    my @tabAgenciesFilter = keys(%$tabCountryAgencies);
    my $agenciesData = &LOC::Statistics::Budget::getList(
        LOC::Util::GETLIST_ASSOC,
        {'agency' => \@tabAgenciesFilter, 'date' => $tabViewData{'month'}}
    );


    # Parcours des données pour divers calculs
    $tabViewData{'calculationDate'} = &_getCalculationDate($agenciesData);
    $tabViewData{'isComplete'} = &_getIsComplete($agenciesData);
    $tabViewData{'isCurrentMonth'} = &_getIsCurrentMonth($tabViewData{'month'});
    $tabViewData{'types'} = &_getTypesList($agenciesData, $tabViewData{'config'});
    $tabViewData{'subTypes'} = &_getSubTypesList($agenciesData, $tabViewData{'config'});

    my $tabCountryAgenciesData = &_processAgenciesData($agenciesData);


    # Données régions et agences
    my $tabViewDataAreas = {};
    tie(%$tabViewDataAreas, 'Tie::IxHash');
    foreach my $agency (values(%$tabAgencies))
    {
        if (!defined $tabCountryAgenciesData->{$agency->{'id'}})
        {
            next;
        }

        if (!defined $tabViewDataAreas->{$agency->{'area.id'}})
        {
            $tabViewDataAreas->{$agency->{'area.id'}} = {
                'infos' => {
                    'id'    => $tabAreas->{$agency->{'area.id'}}->{'id'},
                    'label' => $tabAreas->{$agency->{'area.id'}}->{'label'}
                },
                'agencies' => {}
            };
            tie(%{$tabViewDataAreas->{$agency->{'area.id'}}->{'agencies'}}, 'Tie::IxHash');
        }


        $tabViewDataAreas->{$agency->{'area.id'}}->{'agencies'}->{$agency->{'id'}} = {
            'infos' => {
                'id'    => $agency->{'id'},
                'label' => $agency->{'label'}
            },
            'data' => $tabCountryAgenciesData->{$agency->{'id'}}
        };
    }
    foreach my $area (values(%$tabViewDataAreas))
    {
        my $tabData = {};
        foreach my $agency (values(%{$area->{'agencies'}}))
        {
            $tabData->{$agency->{'infos'}->{'id'}} = $agency->{'data'};
        }

        $area->{'data'} = &_getTotalData($tabData);
    }


    # Données pays
    my $nbAreas = keys(%$tabViewDataAreas);
    if ($nbAreas > 0)
    {
        my $tabCountryInfos = &LOC::Country::getInfos($tabAgencyInfos->{'country.id'});
        $tabViewData{'data'} = {
            'country' => {
                'infos' => {
                    'id'    => $tabCountryInfos->{'id'},
                    'label' => $tabCountryInfos->{'label'}
                },
                'data'  => &_getTotalData($tabCountryAgenciesData),
                'areas' => $tabViewDataAreas
            }
        };
    }


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/budget/view.cgi';
}

# Function: viewAreaAction
# Suivi du budget de la région de connexion
sub viewAreaAction
{
    my $tabParameters = $_[0];
    $tabParameters->{'_type'}        = TYPE_AREA;
    $tabParameters->{'_collipsable'} = COLLIPSABLE_EXPANDED;

    # Appel de la liste des transports en cours
    &viewAction(@_);
}

# Function: viewCountryAction
# Suivi du budget de toutes les régions du pays
sub viewCountryAction
{
    my $tabParameters = $_[0];
    $tabParameters->{'_type'}        = TYPE_COUNTRY;
    $tabParameters->{'_collipsable'} = COLLIPSABLE_COLLAPSED;

    # Appel de la liste des transports en cours
    &viewAction(@_);
}

1;