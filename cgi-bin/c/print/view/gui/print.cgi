use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;

use CGI qw(-utf8 -nosticky :standard escapeHTML);


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;

# Répertoire CSS/JS/Images
my $dir = 'print/gui/';


our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'});
our $cgi = CGI->new();

# Ajout des traductions JavaScript
$view->addTranslations({
    'error' => $locale->t("Une erreur est survenue.\nCode de retour HTTP : %s"),
});

# Fermeture de la pop-up si l'impression s'est déroulée avec succès
if ($tabViewData{'valid'} eq 'ok')
{
    $view->addJSSrc('LOC_View.getCurrentPopup().close();');
    print $view->displayHeader();
    print $view->displayFooter();
    
    exit;
}

# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'print.css');
# JavaScript
$view->addJSSrc($dir . 'print.js');

$view->setPageTitle($locale->t('Imprimer'))
     ->addPackage('spincontrols');
$view->setTitle($locale->t('Imprimer'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_NONE);


# Traductions pour l'état de l'imprimante
my %tabPrinterState = (
                       0 => $locale->t('Inconnu'),
                       1 => $locale->t('Prête'),
                       2 => $locale->t('Non disponible'),
                      );
$view->addTranslations(\%tabPrinterState);

# Traduction des messages d'erreur
my %tabPrinterError = (
                       'impossible' => $locale->t('Impossible d\'imprimer le document'),
                      );
$view->addTranslations(\%tabPrinterError);

# URL de récupération des informations d'une imprimante
my $printerUrl = $view->createURL('print:gui:jsonPrinterInfos');

# JavaScript exécuté lors du changement d'imprimante
my $printerOnChange = 'updatePrinterInfos(this.options[this.selectedIndex].value, "' . $printerUrl . '");';
 
# Liste des imprimantes
my $printerInput = $view->displayFormSelect('printer.id', $tabViewData{'tabPrinters'}, $tabViewData{'printer.id'}, 0,
                                            {'onchange' => $printerOnChange});


# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();

# Sélection de l'imprimante
print $cgi->start_fieldset({-id => 'printerBlock'});
print $cgi->legend($locale->t('Sélection de l\'imprimante'));

print $cgi->table(
                  {'class' => 'formTable'},
                  $cgi->Tr(
                           $cgi->td({-class => 'label'}, $locale->t('Nom')),
                           $cgi->td($printerInput),
                          ),
                  $cgi->Tr(
                           $cgi->td({-class => 'label'}, $locale->t('État')),
                           $cgi->td({-id => 'printer.status'}, $tabPrinterState{$tabViewData{'printer.status'}}),
                          ),
                  $cgi->Tr(
                           $cgi->td({-class => 'label'}, $locale->t('Agence')),
                           $cgi->td({-id => 'agency.label'}, $tabViewData{'agency.label'}),
                          ),
                  $cgi->Tr(
                           $cgi->td({-class => 'label'}, $locale->t('Emplacement')),
                           $cgi->td({-id => 'printer.location'}, $tabViewData{'printer.location'}),
                          ),
                 );

print $view->displayFormInputHidden('printer.label', $tabViewData{'printer.label'});

print $cgi->end_fieldset();

# Choix du nombre de copies
print $cgi->start_fieldset({-id => 'nbCopiesBlock'});
print $cgi->legend($locale->t('Copies'));

print $cgi->table(
                  {'class' => 'formTable'},
                  $cgi->Tr(
                           $cgi->td({-class => 'label'}, $locale->t('Nombre de copies')),
                           $cgi->td($view->displayFormInputText('nbCopies', 1)),
                          ),
                 );

print $view->displayJSBlock('Location.spinControlsManager.createSpinControl("nbCopies", 1, 10, 1, 10, 0);');

print $cgi->end_fieldset();


# Actions
my $printURL = $view->createURL('print:render:print');
my $printAction = 'printDocument("' . $printURL . '", $ge("printer.label").value, "' . $tabViewData{'url'} . '", $ge("nbCopies").value);';

# Boutons
my @tabBtns = ();
push(@tabBtns, $view->displayTextButton($locale->t('OK'), 'valid(|Over).png', $printAction));
push(@tabBtns, $view->displayTextButton($locale->t('Annuler'), 'cancel(|Over).png', 'LOC_View.getCurrentPopup().close();'));

print $view->displayFormInputHidden('valid', '');
print $view->displayFormInputHidden('document', $tabViewData{'document'});
print $view->displayFormInputHidden('document.id', $tabViewData{'document.id'});

print $view->displayControlPanel({'left' => \@tabBtns, 'substyle' => 'middle'});


# Affichage du pied de page
print $view->endForm();
print $view->displayFooter();
