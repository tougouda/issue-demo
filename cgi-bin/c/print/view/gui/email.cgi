use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;

# Répertoire CSS/JS/Images
my $dir = 'print/gui/';


our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'});


# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'email.css');
# JavaScript
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc($dir . 'email.js');

$view->setPageTitle($locale->t('Envoyer par e-mail'));
$view->setTitle($locale->t('Envoyer par e-mail'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_NONE);

# Ajout des traductions JavaScript
$view->addTranslations({
    'delete' => $locale->t('Supprimer')
});

# Affichage de l'entête
print $view->displayHeader();
print $view->startForm(undef, 'multipart/form-data;charset=UTF-8');

# Symbole champ requis
my $requiredIcon = $view->displayRequiredSymbol();

# Destinataires
my $toTitle = $locale->t('Liste des adresses e-mail des destinataires séparées par des points-virgules');
my $ccTitle = $locale->t('Liste des adresses e-mail des destinataires en copie séparées par des points-virgules');

my $searchPanel = sub
{
    my ($type) = @_;

    return '
                    <div id="' . $type . '_search" class="dynamicSearch emailSearch">
                        <div class="container">
                            <div class="loading">' . $locale->t('Recherche en cours') . '</div>
                            <div class="error">' . $locale->t('Une erreur est survenue pendant la recherche') . '</div>
                            <ul>
                            </ul>
                        </div>
                    </div>';
};

print '
<fieldset id="recipientBlock">
    <legend>' . $locale->t('Destinataires') . '</legend>

    <table class="formTable">
        <tbody>
            <tr title="' . $toTitle . '">
                <td class="label">' . $locale->t('À') . $requiredIcon . '</td>
                <td class="field">
                    ' . $view->displayFormInputText('to', $tabViewData{'to'}, undef, {'autocomplete' => 'off'}) .
                    &$searchPanel('to') . '
                </td>
            </tr>
            <tr title="' . $ccTitle . '">
                <td class="label">' . $locale->t('Cc') . '</td>
                <td class="field">
                    ' . $view->displayFormInputText('cc', $tabViewData{'cc'}, undef, {'autocomplete' => 'off'}) .
                    &$searchPanel('cc') . '
                </td>
            </tr>
        </tbody>
    </table>

</fieldset>';


# Corps
my $bodyTitle = $locale->t('Message qui apparaitra dans votre e-mail. La signature sera ajoutée automatiquement');
my $attachmentTitle = $locale->t('Fichier joint à votre e-mail. Cliquez sur le lien pour le visualiser avant l\'envoi');

print '
<fieldset id="bodyBlock">
    <legend>' . $locale->t('Corps') . '</legend>

    <table class="formTable">
        <tbody>
            <tr title="' . $locale->t('Objet de votre message') . '">
                <td class="label">' . $locale->t('Objet') . $requiredIcon . '</td>
                <td class="field">
                    ' . $view->displayFormInputText('subject', $tabViewData{'subject'}) . '
                </td>
            </tr>
            <tr title="' . $bodyTitle . '">
                <td class="label" colspan="2">' . $locale->t('Message') . '</td>
            </tr>
            <tr title="' . $bodyTitle . '">
                <td colspan="2">' . $view->displayFormTextArea('message', $tabViewData{'message'}) . '</td>
            </tr>
        </tbody>
    </table>';

# Pièces jointes
my @tabAttachments = ();
my $nbAttachments = 0;

# Fichier principal
if ($tabViewData{'url'})
{
    $nbAttachments++;
    push(
        @tabAttachments,
        '<a target="_blank" href="' . $tabViewData{'url'} . '" title="' . $attachmentTitle . '">
            ' . $view->displayImage('standard/pdf.png') . ' ' . $tabViewData{'filename'} . '
        </a>'
    );
}

# Fichiers additionnels
if ($tabViewData{'tabAdditionalAttachments'} > 0)
{
    my $additionalAttachementsEl = '
    <div class="additional">';

    foreach my $attachment (@{$tabViewData{'tabAdditionalAttachments'}})
    {
        my $extensionImage;
        if (exists($attachment->{'extension'}))
        {
            $extensionImage = $view->displayFileExtension(undef, $attachment->{'extension'});
        }
        else
        {
            $extensionImage = $view->displayImage('standard/pdf.png');
        }
        $additionalAttachementsEl .= '
        <span class="attachment">' . $extensionImage . ' ' . $attachment->{'filename'} . '</span>';
        $nbAttachments++;
    }
    $additionalAttachementsEl .= '
    </div>';
    push(@tabAttachments, $additionalAttachementsEl);
}
my $attachmentLabel = $locale->tn('Fichier joint', 'Fichiers joints', $nbAttachments);

print '

<table class="formTable">
    <tbody>
        <tr>
            <td class="label">' . $attachmentLabel . '</td>
            <td class="field">
                ' . join('', @tabAttachments) . '
                <div id="customAttachments"></div>
            </td>
        </tr>
    </tbody>
</table>';

print '

</fieldset>';


# Boutons
my @tabBtns = ();
push(@tabBtns, $view->displayTextButton($locale->t('Envoyer'), 'mail(|Over).png', 'return email.send();'));
push(@tabBtns, $view->displayTextButton($locale->t('Annuler'), 'cancel(|Over).png', 'LOC_View.getCurrentPopup().close();'));

print $view->displayFormInputHidden('valid', '');
print $view->displayFormInputHidden('tabCustomAttachments', undef);

my @tabRightBtns = ();
my $fileInput  = $view->displayFormInputFile('addAttachment', 0, {'multiple' => 1});
my $fileInputButton = $view->displayTextButton($locale->t('Ajouter une pièce jointe'),
                                                'attachment.png',
                                                'return false;', $locale->t('Choisissez un fichier'), 'l',
                                                {'id' => 'addAttachmentBtn'});
push(@tabRightBtns, $fileInput . $fileInputButton);


print $view->displayControlPanel({'left' => \@tabBtns, 'right' => \@tabRightBtns, 'substyle' => 'middle'});

# Gestion des erreurs
if ($tabViewData{'valid'} eq 'ok' && !$tabViewData{'result'})
{
    my $errorCode = $tabViewData{'errors'}->[0];
    if ($errorCode == LOC::Mail::ERROR_FATALERROR)
    {
        # Erreur fatale
        print $view->displayMessages('error', [$locale->t('Erreur fatale ("%s")', $tabViewData{'errors'}->[1])], 0);
    }
    elsif ($errorCode == LOC::Mail::ERROR_INVALIDRECIPIENT)
    {
        # Destinataires vides ou invalides
        print $view->displayMessages('error', [$locale->t('Destinataire(s) invalide(s)')], 0);
    }
    elsif ($errorCode == LOC::Mail::ERROR_INVALIDSUBJECT)
    {
        # Objet du mail non renseigné
        print $view->displayMessages('error', [$locale->t('Objet non spécifié')], 0);
    }
    elsif ($errorCode == LOC::Mail::ERROR_UNKNOWNATTACHMENT)
    {
        # Pièce jointe non valide
        print $view->displayMessages('error', [$locale->t('Fichier inexistant : %s', $tabViewData{'errors'}->[1])], 0);
    }
}

# Message de confirmation
if ($tabViewData{'result'})
{
    print $view->displayMessages('valid', [$locale->t('L\'e-mail a été envoyé')], 0);
}


# Affichage du pied de page
print $view->endForm();

# Initialisation JavaScript
print $view->displayJSBlock('
$(function()
{
    email.init(' . &LOC::Json::toJson({
        'searchUrl' => $tabViewData{'searchUrl'},
        'tabCustomAttachments' => $tabViewData{'tabCustomAttachments'}
    }) . ');
});');

print $view->displayFooter();
