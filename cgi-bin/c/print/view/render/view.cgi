use utf8;
use open (':encoding(UTF-8)');

use strict;

# Variable: $stream
# Flux PDF brut à envoyer au navigateur
our $stream;

# Envoi de la réponse au navigateur
print $stream;
