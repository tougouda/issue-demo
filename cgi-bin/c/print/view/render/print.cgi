use utf8;
use open (':encoding(UTF-8)');

use strict;

# Variable: $stream
# Flux PDF brut à envoyer à l'imprimante
our $stream;

# Variable: $printer
# Nom de l'imprimante sur laquelle le flux PDF est envoyé
our $printer;

# Variable: $number
# Nombre de copies à imprimer
our $number;

# Succès de la récupération du flux
# Un flux PDF est retourné
if ($stream =~ /^%PDF/)
{
    # Envoi du flux à l'imprimante
    for (my $i = 0; $i < $number; $i++)
    {
        open(IMP, '| lpr -P ' . $printer);
        binmode IMP, ":raw";    # Pour envoyer le flux en l'état
        print IMP $stream;
        close(IMP);
    }

    # Code de réponse 204 (No Content)
    # Requête traitée mais pas d'information à renvoyer
    # L'utilisateur reste sur la page qu'il visualisait
    &LOC::Browser::sendHeaders("Status: 204 No Content\n\n");
}
# Une page web est retournée
else
{
    # Code de réponse 500 (Internal Server Error)
    &LOC::Browser::sendHeaders("Status: 500 Internal Server Error\n\n");
}
