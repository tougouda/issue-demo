use utf8;
use open (':encoding(UTF-8)');

# package: gui
# Contrôleur de paramétrage des impressions, envois d'e-mail et de fax
package gui;


# Constants: Type d'édition
# TYPE_PRINT - Impression
# TYPE_EMAIL - Envoi par e-mail
use constant
{
    TYPE_PRINT => 'print',
    TYPE_PDF   => 'pdf',
    TYPE_EMAIL => 'email'
};


my $viewDirectory = &dirname(__FILE__);
$viewDirectory =~ s/(.*)\/.+/$1/;
$viewDirectory .= '/view/gui/';


use strict;
use File::Basename;

use LOC::Agency;
use LOC::Country;
use LOC::Mail;
use LOC::Printer;
use LOC::Session;
use LOC::Globals;
use LOC::Contract::Rent;
use LOC::Attachments;
use LOC::Util;



# Function: printAction
# Interface de paramétrage de l'impression
sub _endActions
{
    my ($tabActions, $type, $tabParams) = @_;

    my $tabEndUpds = {};
    my $execEndUpdsAtEnd = 1;

    foreach my $actionInfos (@$tabActions)
    {
        # Impression générale de document
        if ($type eq TYPE_PRINT && $actionInfos->{'type'} eq 'logPrint')
        {
            if (defined $actionInfos->{'entityIds'})
            {
                foreach my $documentId (@{$actionInfos->{'entityIds'}})
                {
                    &LOC::Printer::addLog($actionInfos->{'entity'}, $documentId);
                }
            }
        }
        elsif ($actionInfos->{'type'} eq 'printContract')
        {
            # Impression du contrat
            my $tabTraces = {};

            if ($type eq TYPE_PRINT)
            {
                my $tabPrinterInfos = &LOC::Printer::getInfos($tabParams->{'printer.id'});
                $tabTraces->{'content'} = $tabPrinterInfos->{'label'};
            }
            if ($type eq TYPE_EMAIL)
            {
                $tabTraces->{'content'} = $tabParams->{'to'};
                if (defined $tabParams->{'cc'} && $tabParams->{'cc'} ne '')
                {
                    $tabTraces->{'content'} .= ' / Cc : ' . $tabParams->{'cc'};
                }
            }

            my $tabContractPrintTypes = {
                TYPE_PRINT() => LOC::Contract::Rent::PRINTTYPE_PRINT,
                TYPE_PDF()   => LOC::Contract::Rent::PRINTTYPE_PDF,
                TYPE_EMAIL() => LOC::Contract::Rent::PRINTTYPE_EMAIL
            };

            &LOC::Contract::Rent::print(
                $tabParams->{'country.id'},
                $actionInfos->{'contractId'},
                $tabParams->{'user.id'},
                {
                    'type'   => $tabContractPrintTypes->{$type},
                    'traces' => $tabTraces
                }
            );
        }
        elsif ($actionInfos->{'type'} eq 'printEstimate')
        {
            # Impression du devis
            my $tabTraces = {};

            if ($type eq TYPE_PRINT)
            {
                $tabTraces->{'code'} = LOC::Log::EventType::PRINT;
                my $tabPrinterInfos = &LOC::Printer::getInfos($tabParams->{'printer.id'});
                $tabTraces->{'content'} = $tabPrinterInfos->{'label'};
            }
            elsif ($type eq TYPE_EMAIL)
            {
                $tabTraces->{'code'} = LOC::Log::EventType::SENDMAIL;
                $tabTraces->{'content'} = $tabParams->{'to'};
                if (defined $tabParams->{'cc'} && $tabParams->{'cc'} ne '')
                {
                    $tabTraces->{'content'} .= ' / Cc : ' . $tabParams->{'cc'};
                }
            }
            elsif ($type eq TYPE_PDF)
            {
                $tabTraces->{'code'} = LOC::Log::EventType::UPLOADPDF;
            }

            # Ajout de trace
            &LOC::EndUpdates::addTrace($tabEndUpds, $actionInfos->{'entity'},
                                                    $actionInfos->{'entityId'},
                                                    $tabTraces);
        }
        elsif ($actionInfos->{'type'} eq 'addTrace')
        {
            my $tabContent = $actionInfos->{'tabContent'};

            # Formatage
            my $recipients = $tabParams->{'to'};
            if (defined $tabParams->{'cc'} && $tabParams->{'cc'} ne '')
            {
                $recipients .= ' / Cc : ' . $tabParams->{'cc'};
            }
            my $content   = $actionInfos->{'content'};

            $tabContent->{'content'} =~ s/<%recipient>/$recipients/g;
            $tabContent->{'content'} =~ s/<%content>/$content/g;

            # Ajout de trace
            &LOC::EndUpdates::addTrace($tabEndUpds, $actionInfos->{'functionality'},
                                                    $actionInfos->{'functionalityId'},
                                                    $tabContent);
        }

    }

    #Exécution des mises à jour de fin
    if ($execEndUpdsAtEnd)
    {
        &LOC::EndUpdates::exec($tabParams->{'country.id'}, $tabEndUpds, $tabParams->{'user.id'});
    }

}


# Function: _formatTrace
# Mise en forme des traces
sub _formatTrace
{
    my ($trace, $tabReplacements) = @_;

    if (ref($trace) eq 'ARRAY')
    {
        foreach my $value (@$trace)
        {
            $value = &_formatTrace($value, $tabReplacements);
        }
    }
    elsif (ref($trace) eq 'HASH')
    {
        foreach my $value (values(%$trace))
        {
            $value = &_formatTrace($value, $tabReplacements);
        }
    }
    else
    {
        foreach my $pattern (keys(%$tabReplacements))
        {
            my $replacement = $tabReplacements->{$pattern};
            $trace =~ s/<%\Q$pattern>/$replacement/g;
        }
    }

    return $trace;
};


# Function: printAction
# Interface de paramétrage de l'impression
sub printAction
{
    my $tabParameters = $_[0];
    my $type = TYPE_PRINT;

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    my $tabEndActions  = &LOC::Request::getArray('tabEndActions', []);
    our %tabViewData = (
                        'locale.id'      => $tabUserInfos->{'locale.id'},
                        'url'            => &LOC::Request::getString('url'),
                        'valid'          => &LOC::Request::getString('valid'),
                        'printer.id'     => &LOC::Request::getString('printer.id'),
                        'tabEndActions'  => $tabEndActions
                       );

    # Impression déroulée avec succès
    # Mise à jour de la dernière imprimante utilisée
    if ($tabViewData{'valid'} eq 'ok')
    {
        # Action de fin
        my $tabEndActionParams = {
            'user.id'     => $tabUserInfos->{'id'},
            'country.id'  => $tabUserInfos->{'nomadCountry.id'},
            'printer.id'  => $tabViewData{'printer.id'}
        };
        &_endActions($tabViewData{'tabEndActions'}, $type, $tabEndActionParams);

        &LOC::User::setPrinterId($tabUserInfos->{'id'}, $tabViewData{'printer.id'});
    }
    else
    {
        # Agence physique par rapport à l'adresse IP
        my $tabSession = &LOC::Session::getInfos();
        my $physicalAgency = &LOC::Agency::getByIP($tabSession->{'ip'});
        
        # Liste des imprimantes de cette agence
        $tabViewData{'tabPrinters'} = {};
        if ($physicalAgency ne '')
        {
            $tabViewData{'tabPrinters'} = &LOC::Printer::getList(LOC::Util::GETLIST_PAIRS, {'agency' => $physicalAgency});
        }
        
        $tabViewData{'printer.id'} = $tabUserInfos->{'printer.id'};
        if ($tabViewData{'printer.id'} == 0)
        {
            $tabViewData{'printer.id'} = (keys(%{$tabViewData{'tabPrinters'}}))[0];
        }
        
        my $tabPrinterInfos = &LOC::Printer::getInfos($tabViewData{'printer.id'});
        $tabViewData{'agency.id'}        = $tabPrinterInfos->{'agency.id'};
        $tabViewData{'agency.label'}     = $tabPrinterInfos->{'agency.label'};
        $tabViewData{'printer.label'}    = $tabPrinterInfos->{'label'};
        $tabViewData{'printer.location'} = $tabPrinterInfos->{'location'};
        $tabViewData{'printer.ip'}       = $tabPrinterInfos->{'ip'};
        $tabViewData{'printer.status'}   = &LOC::Printer::getStatus($tabViewData{'printer.ip'});
    }


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $viewDirectory . 'print.cgi';
}


# Function: pdfAction
# Interface de paramétrage de l'impression
sub pdfAction
{
    my $tabParameters = $_[0];
    my $type = TYPE_PDF;

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    my $tabEndActions  = &LOC::Request::getArray('tabEndActions', []);
    our %tabViewData = (
                        'url'            => &LOC::Request::getString('url'),
                        'tabEndActions'  => $tabEndActions
                       );

    # Action de fin
    my $tabEndActionParams = {
        'user.id'     => $tabUserInfos->{'id'},
        'country.id'  => $tabUserInfos->{'nomadCountry.id'}
    };
    &_endActions($tabViewData{'tabEndActions'}, $type, $tabEndActionParams);

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $viewDirectory . 'pdf.cgi';
}


# Function: emailAction
# Interface de paramétrage de l'envoi d'e-mail
sub emailAction
{
    my $tabParameters = $_[0];
    my $type = TYPE_EMAIL;

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    my $tabReplacesTpl = &LOC::Request::getHash('tabReplacesTpl', {});
    my $tabEndActions  = &LOC::Request::getArray('tabEndActions', []);
    my $tabAdditionalAttachments  = &LOC::Request::getArray('tabAdditionalAttachments', []);
    my $tabCustomAttachments = &LOC::Request::getString('tabCustomAttachments', '[]');
    $tabCustomAttachments = &LOC::Json::fromJson($tabCustomAttachments);

    my $to = &LOC::Util::trim(&LOC::Request::getString('to', &LOC::Request::getString('defaultRecipient')));
    if ($to ne '' && $to !~ /;$/)
    {
        $to .= ';';
    }
    my $cc = &LOC::Util::trim(&LOC::Request::getString('cc', &LOC::Request::getString('defaultCopyRecipient')));
    if ($cc ne '' && $cc !~ /;$/)
    {
        $cc .= ';';
    }
    our %tabViewData = (
                        'locale.id'          => $tabUserInfos->{'locale.id'},
                        'url'                => &LOC::Request::getString('url'),
                        'filename'           => &LOC::Request::getString('filename'),
                        'document'           => &LOC::Request::getString('document'),
                        'document.id'        => &LOC::Request::getString('document.id'),
                        'document.agency.id' => &LOC::Request::getString('documentAgency.id'),
                        'to'                 => $to,
                        'cc'                 => $cc,
                        'subject'            => &LOC::Util::trim(&LOC::Request::getString('subject', &LOC::Request::getString('defaultSubject'))),
                        'message'            => &LOC::Request::getString('message', &LOC::Request::getString('defaultContent')),
                        'tabReplacesTpl'     => $tabReplacesTpl,
                        'tabEndActions'      => $tabEndActions,
                        'tabAdditionalAttachments' => $tabAdditionalAttachments,
                        'tabCustomAttachments'     => $tabCustomAttachments,
                        'searchUrl'          => &LOC::Request::createRequestUri('GUI>default:contacts:search', undef, 0)
                       );

    my @tabTo = grep {&LOC::Util::trim($_) ne ''} split(/;/, $tabViewData{'to'});
    my $nbTo = @tabTo;
    my @tabCc = grep {&LOC::Util::trim($_) ne ''} split(/;/, $tabViewData{'cc'});
    my $nbCC = @tabCc;

    # Soumission du formulaire
    $tabViewData{'valid'} = &LOC::Request::getString('valid');

    if ($tabViewData{'valid'} eq 'ok')
    {
        # Vérifications
        # Destinataire vide
        if ($nbTo == 0)
        {
            $tabViewData{'result'} = 0;
            $tabViewData{'errors'} = [LOC::Mail::ERROR_INVALIDRECIPIENT, 0];
        }
        # Validité de chaque destinataire
        foreach my $to (@tabTo)
        {
            if (!&LOC::Util::isValidEmail($to))
            {
                $tabViewData{'result'} = 0;
                $tabViewData{'errors'} = [LOC::Mail::ERROR_INVALIDRECIPIENT, 0];
                last;
            }
        }
        # Destinataires en copie
        if ($nbCC == 0)
        {
            # Validité de chaque destinataire en copie
            foreach my $cc (split(/;/, $tabViewData{'cc'}))
            {
                if (!&LOC::Util::isValidEmail($cc))
                {
                    $tabViewData{'result'} = 0;
                    $tabViewData{'errors'} = [LOC::Mail::ERROR_INVALIDRECIPIENT, 0];
                    last;
                }
            }
        }
        # Objet vide
        if ($tabViewData{'subject'} eq '')
        {
            $tabViewData{'result'} = 0;
            $tabViewData{'errors'} = [LOC::Mail::ERROR_INVALIDSUBJECT, 0];
        }

        # Pas d'erreur
        if (!defined $tabViewData{'errors'})
        {
            # Informations de l'agence
            my $tabAgencyInfos = &LOC::Agency::getInfos($tabViewData{'document.agency.id'});

            # Informations du pays
            my $tabCountryInfos = &LOC::Country::getInfos($tabAgencyInfos->{'country.id'});

            my $mailObj = LOC::Mail->new();

            # Encodage
            $mailObj->setEncodingType(LOC::Mail::ENCODING_TYPE_UTF8);

            # Expéditeur
            my %sender = ();
            # Adresse de contact de l'agence si elle existe
            if ($tabAgencyInfos->{'email'})
            {
                %sender = ($tabAgencyInfos->{'email'} =>
                           sprintf('%s %s', $tabCountryInfos->{'subsidiaryName'}, $tabAgencyInfos->{'label'}));
            }
            # Adresse de l'utilisateur s'il en possède une
            elsif ($tabUserInfos->{'email'})
            {
                %sender = ($tabUserInfos->{'email'} => $tabUserInfos->{'firstName'} . ' ' . $tabUserInfos->{'name'});
            }
            $mailObj->setSender(%sender);

            # Corps
            my $file = &LOC::Globals::get('cgiPath') . '/cfg/templates/' . $tabAgencyInfos->{'country.id'} . '/email.html';
            my $template = &LOC::Util::getFileContent($file);

            my $message = $tabViewData{'message'};
            $message =~ s/\r//mg;                   # Suppression des retours chariots
            $message =~ s/\n/<br \/>/mg;            # Conversion des retours à la ligne
            $message .= "\n";
            $message = '<p>' . $message . '</p>';   # Encapsulation dans des paragraphes

            # Remplacement dans le template
            $template =~ s/<%title>/$tabViewData{'subject'}/g;
            $template =~ s/<%message>/$message/g;
            $template =~ s/<%user\.(.+?)>/$tabUserInfos->{$1}/g;
            $template =~ s/<%agency\.(.+?)>/$tabAgencyInfos->{$1}/g;

            if ($template =~ /"cid:(.+)"/ && -e &LOC::Globals::get('imgPath') . '/print/gui/' . $1)
            {
                $mailObj->addInlineAttachment(&LOC::Globals::get('imgUrl') . '/print/gui/' . $1, $1);
            }

            # Données template personnalisé
            foreach my $dataToReplace (keys(%$tabReplacesTpl))
            {
                my $tabReplaceInfos = $tabReplacesTpl->{$dataToReplace};
                # Type LIEN
                if ($tabReplaceInfos->{'type'} eq 'link')
                {
                    $template =~ s/<%$dataToReplace>/<a href="$tabReplaceInfos->{'url'}">$tabReplaceInfos->{'value'}<\/a>/g;
                }
            }

            $mailObj->setBodyType(LOC::Mail::BODY_TYPE_HTML);

            # Objet
            $mailObj->setSubject($tabViewData{'subject'});

            # Environnement production
            my $env = &LOC::Globals::get('env');
            if ($env eq 'exp')
            {
                # Destinataires
                for (my $i = 0; $i < $nbTo; $i++)
                {
                    $mailObj->addRecipient($tabTo[$i]);
                }
                if ($tabViewData{'cc'} ne '')
                {
                    for (my $i = 0; $i < $nbCC; $i++)
                    {
                        $mailObj->addCCRecipient($tabCc[$i]);
                    }
                }
                
                # Utilisateur en copie cachée
                if ($tabUserInfos->{'email'} ne '')
                {
                    my %bcc = ($tabUserInfos->{'email'} => $tabUserInfos->{'firstName'} . ' ' . $tabUserInfos->{'name'});
                    $mailObj->addBCCRecipient(%bcc);
                }
                
                $mailObj->setBody($template);
            }
            else
            {
                my $templatePlus  = 'Destinataires d\'origine : ' . $tabViewData{'to'} . '<br />';
                $templatePlus .= 'Copie à : ' . $tabViewData{'cc'};
                $mailObj->addRecipient($tabUserInfos->{'email'});
                $mailObj->setBody($templatePlus . $template);
            }


            # Pièces jointes
            if ($tabViewData{'url'} ne '')
            {
                my $attachment = $tabViewData{'url'};
                $mailObj->addFileAttachment($attachment, $tabViewData{'filename'});
            }

            # Pièces jointes supplémentaires
            my $nbAdditionalAttachments = keys(@{$tabViewData{'tabAdditionalAttachments'}});

            if ($nbAdditionalAttachments > 0)
            {
                foreach my $attachment (@{$tabViewData{'tabAdditionalAttachments'}})
                {
                    if ($attachment->{'url'})
                    {
                        $mailObj->addFileAttachment($attachment->{'url'}, $attachment->{'filename'});
                    }
                    elsif ($attachment->{'attachmentId'})
                    {
                        my $tabAttachmentInfos = &LOC::Attachments::getInfos($tabUserInfos->{'nomadCountry.id'}, $attachment->{'attachmentId'}, {'getContents' => 1});
                        if ($tabAttachmentInfos && $tabAttachmentInfos->{'fileContent'} ne '')
                        {
                            $mailObj->addStringAttachment($tabAttachmentInfos->{'fileContent'}, $attachment->{'filename'});
                        }
                        else
                        {
                            $tabViewData{'result'} = 0;
                            $tabViewData{'errors'} = [LOC::Mail::ERROR_UNKNOWNATTACHMENT, $attachment->{'filename'}];
                            last;
                        }
                    }
                }
            }
            # Pièces jointes supplémentaires (ajoutées par l'utilisateur)
            my $nbCustomAttachments = keys(@{$tabViewData{'tabCustomAttachments'}});
            if ($nbCustomAttachments > 0)
            {
                foreach my $attachment (@{$tabViewData{'tabCustomAttachments'}})
                {
                    my $content = $attachment->{'content'};
                    $content =~ s/^(.*)base64,//;
                    $mailObj->addStringAttachment(&LOC::Util::base64Decode($content), $attachment->{'name'});
                }
            }

            if (!defined $tabViewData{'errors'})
            {
                # Envoi
                $tabViewData{'result'} = $mailObj->send();
                $tabViewData{'errors'} = [LOC::Mail::ERROR_FATALERROR, $mailObj->getErrors()];


                my $tabReplacements = {
                    'to'      => join(', ', @tabTo),
                    'cc'      => join(', ', @tabCc),
                    'subject' => $tabViewData{'subject'},
                    'message' => $message
                };

                foreach my $action (@{$tabViewData{'tabEndActions'}})
                {
                    if (defined $action->{'tabContent'} && $action->{'type'} eq 'addTrace')
                    {
                        $action->{'tabContent'}->{'content'} = &_formatTrace(
                                $action->{'tabContent'}->{'content'},
                                $tabReplacements
                            );
                        $action->{'tabContent'}->{'contentExtra'} = &_formatTrace(
                                $action->{'tabContent'}->{'contentExtra'},
                                $tabReplacements
                            );
                    }
                }

                # Action de fin
                my $tabEndActionParams = {
                    'user.id'     => $tabUserInfos->{'id'},
                    'country.id'  => $tabUserInfos->{'nomadCountry.id'},
                    'to'          => $tabViewData{'to'},
                    'cc'          => $tabViewData{'cc'}
                };
                &_endActions($tabViewData{'tabEndActions'}, $type, $tabEndActionParams);
            }
        }
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $viewDirectory . 'email.cgi';
}


# Function: jsonPrinterInfosAction
# Récupérer les informations d'une imprimante
#
sub jsonPrinterInfosAction
{
    # Variables pour la vue
    our %tabViewData = ();

    # Récupération des paramètres
    my $printerId = &LOC::Request::getString('printerId', '');

    # Informations de l'imprimante
    $tabViewData{'tabPrinterInfos'} = &LOC::Printer::getInfos($printerId);
    $tabViewData{'tabPrinterInfos'}->{'status'} = &LOC::Printer::getStatus($tabViewData{'tabPrinterInfos'}->{'ip'});
    
    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/gui/jsonPrinterInfos.cgi';
}

1;
