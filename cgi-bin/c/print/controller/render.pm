use utf8;
use open (':encoding(UTF-8)');

# package: render
# Contrôleur de visualisation et d'impression directe de fichiers PDF
package render;

use strict;
use File::Basename;
use WWW::Mechanize;

use LOC::Controller::Front;
use LOC::Request;

my $viewDirectory = &dirname(__FILE__);
$viewDirectory =~ s/(.*)\/.+/$1/;
$viewDirectory .= '/view/render/';



# Function: viewAction
# Impression directe de fichier PDF
sub printAction
{
    my $url     = &LOC::Request::getString('url');
    my $options = &LOC::Request::getString('options', {});

    # Infos de l'utilisateur 
    my $tabUserInfos = &LOC::Session::getUserInfos();

    # Variable: $stream
    # Flux PDF brut à envoyer à l'imprimante
    our $stream  = &_getStream($url, $options, $tabUserInfos->{'login'}, $tabUserInfos->{'password'});

    # Variable: $printer
    # Nom de l'imprimante sur laquelle le flux PDF est envoyé
    our $printer = &LOC::Request::getString('printer');

    # Variable: $number
    # Nombre de copies à imprimer
    our $number = &LOC::Request::getInteger('number', 1);

    require $viewDirectory . 'print.cgi';
}

# Function: viewAction
# Visualisation de fichier PDF
sub viewAction
{
    my $url     = &LOC::Request::getString('url');
    my $options = &LOC::Request::getString('options', {});

    # Infos de l'utilisateur 
    my $tabUserInfos = &LOC::Session::getUserInfos();

    # Variable: $stream
    # Flux PDF brut à envoyer au navigateur
    our $stream = &_getStream($url, $options, $tabUserInfos->{'login'}, $tabUserInfos->{'password'});

    require $viewDirectory . 'view.cgi';
}

# Function: _getStream
# Récupération du flux brut du PDF
#
# Parameters:
# string $url        L'URL de PDF à récupérer sous la forme module:contrôleur:action.
# string $options    Les options.
# string $login      L'identifiant de l'utilisateur.
# string $password   Le mot de passe de l'utilisateur.
#
# Returns:
# string Flux brut du PDF
sub _getStream
{
    my ($url, $options, $login, $password) = @_;

    my $expr = LOC::Request::REGEXP_MCA_URL;

    # Module/Contrôleur/Vue de la Gestion des locations
    # Lancement direct du dispatch du front controller
    if ($url =~ m/$expr/)
    {
        my ($module, $controller, $action) = split(/:/, $url);

        my $stream;

        # Redirection de la sortie standard dans la variable
        open(my $fh, '>:raw', \$stream) or die "Erreur lors de la récupération du flux brut PDF.";
        my $oldFh = select $fh;
        &LOC::Controller::Front::dispatch(&dirname($0), $module, $controller, $action);
        select $oldFh;
        close $fh;

        return $stream;
    }
    # URL externe
    # Récupération du flux
    else
    {
        my $dev = &LOC::Globals::get('dev');
        if ($dev ne '')
        {
            $url .= ($url =~ /\?/ ? '&' : '?') . 'dev=' . $dev;
        }
        my $mech = WWW::Mechanize->new(autocheck => 0);
        $mech->credentials($login, $password);
        my $resGet = $mech->get($url);
        unless ($mech->success())
        {
            return 0;
        }
        return $mech->content();
    }
}

1;
