#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;

my $view = LOC::Html::Standard->new();

$view->setPageTitle('Unauthorized area');

print $view->displayHeader();

my $errors = ['<p>Vous n\'avez pas les droits nécessaires pour visualiser cette page</p>
<p>You do not have the necessary rights to visualize this page</p>
<p>No tiene los derechos necesarios para consultar esta página</p>
<p>Não tem os direitos necessários para visualizar esta página</p>
<p>Non avete i diritti necessari per visualizzare questa pagina</p>'];

print $view->displayMessages('warning', $errors, 0);

print $view->displayFooter();