use utf8;
use open (':encoding(UTF-8)');

# Package: controlOrder
# Contrôleur pour les commandes de contrôle
package controlOrder;

use strict;
use File::Basename;

use LOC::Customer;
use LOC::Site;
use LOC::Machine;
use LOC::Characteristic;
use LOC::Request;


# Function: pdfAction
# Affiche le bon de commande VGP au format PDF
#
# Parameters:
# string   countryId     - Pays
# string   agencyId      - Agence
# int      customerId    - Identifiant du client
# arrayref contractsId   - Tableau des identifiants des contrats à imprimer
# int      nbTotal       - Total des lignes de résultats (affichage)
# int      offset        - Début de la recherche (affichage)
# int      limit         - Nombre à afficher (affichage)
sub pdfAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};


    # Variables pour la vue
    our %tabViewData = (
        'locale.id' => $tabUserInfos->{'locale.id'}
    );

    # Récupération des paramètres
    my $countryId   = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $agencyId    = &LOC::Request::getString('agencyId', $tabUserInfos->{'nomadAgency.id'});
    my $currencySymbol = $tabUserInfos->{'currency.symbol'};

    my $machinesIds = &LOC::Request::getArray('machinesIds', []);

    # Récupération de la liste des machines
    my $tabMachinesList = &LOC::Machine::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                 {'id' => $machinesIds},
                                                 LOC::Machine::GETINFOS_CONTROLORDER);
    
    my @tabMachines;

    # Formatage du tableau de résultat
    foreach my $tabMachineInfos (values(%$tabMachinesList))
    {
        my $tabCustomerInfos = {};
        my $tabSiteInfos     = {};
        
        if ($tabMachineInfos->{'contract.id'} &&
            &LOC::Util::in_array($tabMachineInfos->{'state.id'}, [LOC::Machine::STATE_RENTED, LOC::Machine::STATE_RECOVERY]))
        {
            my $tabContractInfos = &LOC::Contract::Rent::getInfos($countryId, $tabMachineInfos->{'contract.id'});

            # Récupération infos client
            my $tabCustomer = &LOC::Customer::getInfos($countryId, $tabContractInfos->{'customer.id'});
            $tabCustomerInfos = {
                'label'     => $tabCustomer->{'name'},
                'telephone' => $tabCustomer->{'telephone'}
            };
            # Récupération infos chantier
            my $tabSite = &LOC::Site::getInfos($countryId, $tabContractInfos->{'site.id'});
            $tabSiteInfos = {
                'label' => $tabSite->{'label'},
                'address' => $tabSite->{'address'},
                'postalCode' => $tabSite->{'locality.postalCode'},
                'city'       => $tabSite->{'locality.name'},
                'contact'    => $tabSite->{'contact.fullName'},
                'telephone'  => $tabSite->{'contact.telephone'},
                'comments'   => $tabSite->{'comments'},
                'isAsbestos' => $tabSite->{'isAsbestos'}
            };
        }

        my $tabMachineAgencyInfos = &LOC::Agency::getInfos($tabMachineInfos->{'agency.id'});

        my $tabInfos = {
            'parkNumber'   => $tabMachineInfos->{'parkNumber'},
            'serialNumber' => $tabMachineInfos->{'serialNumber'},
            'model.label'  => $tabMachineInfos->{'model.label'},
            'state.id'     => $tabMachineInfos->{'state.id'},
            'agency.tabInfos' => $tabMachineAgencyInfos,
            'organization.tabInfos' => {
                'label'      => $tabMachineInfos->{'tabControlOrderInfos'}->{'organization.label'},
                'address'    => $tabMachineInfos->{'tabControlOrderInfos'}->{'organization.address'},
                'postalCode' => $tabMachineInfos->{'tabControlOrderInfos'}->{'organization.postalCode'},
                'city'       => $tabMachineInfos->{'tabControlOrderInfos'}->{'organization.city'},
                'contact'    => $tabMachineInfos->{'tabControlOrderInfos'}->{'organization.contact'},
                'telephone'  => $tabMachineInfos->{'tabControlOrderInfos'}->{'organization.telephone'},
                'fax'        => $tabMachineInfos->{'tabControlOrderInfos'}->{'organization.fax'},
                'agency.label'      => $tabMachineInfos->{'tabControlOrderInfos'}->{'organization.agency.label'},
                'agency.address'    => $tabMachineInfos->{'tabControlOrderInfos'}->{'organization.agency.address'},
                'agency.postalCode' => $tabMachineInfos->{'tabControlOrderInfos'}->{'organization.agency.postalCode'},
                'agency.city'       => $tabMachineInfos->{'tabControlOrderInfos'}->{'organization.agency.city'},
                'agency.telephone'  => $tabMachineInfos->{'tabControlOrderInfos'}->{'organization.agency.telephone'},
                'agency.fax'        => $tabMachineInfos->{'tabControlOrderInfos'}->{'organization.agency.fax'}
            },
            'controlOrder.tabInfos' => {
                'id'          => $tabMachineInfos->{'tabControlOrderInfos'}->{'id'},
                'date'        => $tabMachineInfos->{'tabControlOrderInfos'}->{'date'},
                'controlDate' => $tabMachineInfos->{'tabControlOrderInfos'}->{'controlDate'},
                'amount'      => $tabMachineInfos->{'tabControlOrderInfos'}->{'amount'}
            },
            'customer.tabInfos' => $tabCustomerInfos,
            'site.tabInfos'     => $tabSiteInfos
        };
        
        push(@tabMachines, $tabInfos);
    }

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'          => $tabUserInfos->{'locale.id'},
        'user.agency.id'     => $agencyId,
        'tabMachines'        => \@tabMachines
    );

    $tabViewData{'tabData'} = {
        'countryId'       => $countryId,
        'currencySymbol'  => $currencySymbol,
        'user.firstName'  => $tabUserInfos->{'firstName'},
        'user.name'       => $tabUserInfos->{'name'}
    };

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    
    $tabViewData{'pdfUrl'} = $directory . '/view/controlOrder/pdf.php';

    require $directory . '/view/controlOrder/pdf.cgi';
}


1;