use utf8;
use open (':encoding(UTF-8)');

# Package: endingContracts
# Contrôleur pour l'alerte des transferts inter-chantiers
package endingContracts;

use strict;
use File::Basename;

use LOC::Json;
use LOC::Transport;
use LOC::Characteristic;
use LOC::Kimoce;
use LOC::MachineInventory;

# Function: viewRecoveriesAction
# Alerte des récupérations
sub viewRecoveriesAction
{
    my $tabParameters = $_[0];

    # Appel de la liste des transferts inter-chantiers
    &_viewAction('recoveries', $tabParameters->{'tabUserInfos'});
}

# Function: viewTransfersAction
# Alerte des transferts inter-chantiers
sub viewTransfersAction
{
    my $tabParameters = $_[0];

    # Appel de la liste des transferts inter-chantiers
    &_viewAction('transfers', $tabParameters->{'tabUserInfos'});
}


# Function: _viewAction
# Contrôleur général
sub _viewAction
{
    my ($actionName, $tabUserInfos) = @_;

    my $countryId = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $agencyId  = &LOC::Request::getString('agencyId', $tabUserInfos->{'nomadAgency.id'});

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
        'actionName'     => $actionName
    );

    $tabViewData{'maxDiffTimeCounter'} = &LOC::Characteristic::getAgencyValueByCode('MAXCPTHORAIRE', $agencyId) * 1;
    $tabViewData{'nbConsidersMax'}     = &LOC::Characteristic::getAgencyValueByCode('PRCPTEMAX', $agencyId) * 1;
    $tabViewData{'isKimoceCallable'}   = &LOC::Kimoce::isCallable();

    # Récupération de la validation
    my $validInfos = &LOC::Request::getString('valid', '');
    my $tabErrors = {};
    if ($validInfos ne '')
    {
        my $tabValidInfos = &LOC::Json::fromJson($validInfos);
        if ($tabValidInfos->{'valid'} eq 'ok')
        {
            # Tableau de retour
            $tabViewData{'return'} = {
                'success' => [],
                'errors'  => [],
                'infos'   => []
            };

            # Délai entre deux prises en compte (en ms)
            my $delay = &LOC::Characteristic::getAgencyValueByCode('DELTAPRCPTE', $agencyId) * 1;

            # Traitement de chaque élément sélectionné
            my $isFirst = 1;
            foreach my $tabInfos (@{$tabValidInfos->{'actions'}})
            {
                my $tabOptions = {};
                if ($tabInfos->{'timeCounter'} != 0)
                {
                    $tabOptions->{'updateTimeCounter'} = $tabInfos->{'timeCounter'};
                }

                # Délai pour espacer les mises à jour
                if (!$isFirst && $delay != 0)
                {
                    # Sleep en millisecondes avec la fonction sleep
                    select(undef, undef, undef, $delay / 1000);
                }

                # Mise à jour
                if (&LOC::Transport::consider($countryId, $tabInfos->{'id'}, $tabInfos->{'considerType'},
                                              $tabUserInfos->{'id'}, $tabOptions, $tabErrors, undef))
                {
                    push(@{$tabViewData{'return'}->{'success'}}, {
                        'id'         => $tabInfos->{'id'},
                        'parkNumber' => $tabInfos->{'parkNumber'}
                    });
                }
                else
                {
                    my $errorCode = ();

                    my $nbErrors = @{$tabErrors->{'fatal'}};
                    if ($nbErrors > 0)
                    {
                        # Déjà fait
                        if (&LOC::Util::in_array(0x0301, $tabErrors->{'fatal'}))
                        {
                            $errorCode = 'alreadyDone'; # Déjà fait
                        }
                        # Erreur fatale
                        elsif (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
                        {
                            $errorCode = 'fatalError'; # Erreur fatale
                        }
                        # Erreur de droits
                        elsif (&LOC::Util::in_array(0x0003, $tabErrors->{'fatal'}))
                        {
                            $errorCode = 'rightsError'; # Droits
                        }
                    }

                    push(@{$tabViewData{'return'}->{'errors'}}, {
                        'id'         => $tabInfos->{'id'},
                        'parkNumber' => $tabInfos->{'parkNumber'},
                        'error'      => $errorCode
                    });

                }

                $isFirst = 0;

                if (!$tabViewData{'isKimoceCallable'})
                {
                    push(@{$tabViewData{'return'}->{'infos'}}, 'kimoce-unsaved');
                }
            }
        }
    }

    # Récupération des informations pour la vue
    my $tabFilters = {
        'from'         => {
            'type'    => LOC::Transport::FROMTOTYPE_CONTRACT,
            'stateId' => [LOC::Contract::Rent::STATE_STOPPED, LOC::Contract::Rent::STATE_BILLED]
        },
        'stateId'      => LOC::Transport::STATE_ACTIVE,
        'isConsidered' => 0
    };

    if ($actionName eq 'recoveries')
    {
        $tabFilters->{'type'} = LOC::Transport::TYPE_RECOVERY;
        $tabFilters->{'to'} = {
            'type' => LOC::Transport::FROMTOTYPE_AGENCY,
            'id'   => $agencyId
        };
    }
    elsif ($actionName eq 'transfers')
    {
        $tabFilters->{'type'} = LOC::Transport::TYPE_SITESTRANSFER;
        $tabFilters->{'to'} = {
            'type'     => LOC::Transport::FROMTOTYPE_CONTRACT,
            'agencyId' => $agencyId
        };
    }

    $tabViewData{'tabTransports'} = &LOC::Transport::getList($countryId,
                                                             LOC::Util::GETLIST_ASSOC,
                                                             $tabFilters,
                                                             LOC::Transport::GETINFOS_MACHINE |
                                                             LOC::Transport::GETINFOS_MODEL |
                                                             LOC::Transport::GETINFOS_RIGHTS,
                                                             {'orders' => [{'field' => 'valueDate', 'dir' => 'ASC'}]});

    foreach my $tabTsp (values %{$tabViewData{'tabTransports'}})
    {
        $tabTsp->{'from.tabInfos'} = &LOC::Contract::Rent::getInfos($countryId, $tabTsp->{'from.id'},
                                                                    LOC::Contract::Rent::GETINFOS_CUSTOMER |
                                                                    LOC::Contract::Rent::GETINFOS_ACTIONSDATES);
        $tabTsp->{'from.tabInfos'}->{'site.tabInfos'} = &LOC::Site::getInfos($countryId, $tabTsp->{'from.tabInfos'}->{'site.id'});
        $tabTsp->{'from.tabInfos'}->{'customer.url'} = &LOC::Customer::getUrl($tabTsp->{'from.tabInfos'}->{'customer.id'});

        if ($actionName eq 'transfers')
        {
            $tabTsp->{'to.tabInfos'} = &LOC::Contract::Rent::getInfos($countryId, $tabTsp->{'to.id'}, LOC::Contract::Rent::GETINFOS_CUSTOMER);
            $tabTsp->{'to.tabInfos'}->{'site.tabInfos'} = &LOC::Site::getInfos($countryId, $tabTsp->{'to.tabInfos'}->{'site.id'});
            $tabTsp->{'to.tabInfos'}->{'customer.url'} = &LOC::Customer::getUrl($tabTsp->{'to.tabInfos'}->{'customer.id'});
        }

        # Infos sur les états des lieux liés
        $tabTsp->{'machineInventory'} = &_getRecoveryMachineInventoriesInfos($countryId, $tabTsp->{'from.id'});

        # Récupération informations sur le compteur horaire de chaque machine
        # - Vérification connexion Kimoce
        if ($tabViewData{'isKimoceCallable'})
        {
            my $tabCounters = &LOC::Kimoce::getMachineCounters($tabTsp->{'machine.tabInfos'}->{'parkNumber'});
            if ($tabCounters)
            {
                $tabTsp->{'machine.tabInfos'}->{'timeCounter'} = $tabCounters->{LOC::Kimoce::COUNTER_TIME()};
            }
        }
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/endingContracts/view.cgi';
}


sub _getRecoveryMachineInventoriesInfos
{
    my ($countryId, $contractId) = @_;

    my $tabMachineInventories = &LOC::MachineInventory::getList(
            $countryId,
            LOC::Util::GETLIST_ASSOC,
            {
                'attachedToContractId' => $contractId,
                'type'                 => LOC::MachineInventory::TYPE_RECOVERY,
                'stateId'              => LOC::MachineInventory::STATE_ATTACHED
            },
            undef,
            {'orderBy' => ['date:ASC']}
        );

    my $tabResult = {
        'date'         => undef,
        'timeCounters' => []
    };

    foreach my $machineInventoryInfos (values %$tabMachineInventories)
    {
        if (!defined $tabResult->{'date'})
        {
            $tabResult->{'date'} = $machineInventoryInfos->{'date'};
        }
        push(@{$tabResult->{'timeCounters'}}, $machineInventoryInfos->{'timeCounter'});
    }

    return $tabResult;
}


1;