use utf8;
use open (':encoding(UTF-8)');

# package: repair
# Contrôleur d'affichage du carburant
package fuel;

use strict;
use File::Basename;

use LOC::Agency;
use LOC::Country;
use LOC::Session;
use LOC::Contract::Rent;
use LOC::Machine;
use LOC::Alert;

my $viewDirectory = &dirname(__FILE__);
$viewDirectory =~ s/(.*)\/.+/$1/;
$viewDirectory .= '/view/fuel/';



# Function: typeAction
# Interface de saisie du carburant
sub typeAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos  = $tabParameters->{'tabUserInfos'};
    my $currentUserId = $tabUserInfos->{'id'};

    my $countryId  = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $agencyId   = &LOC::Request::getString('agencyId', $tabUserInfos->{'nomadAgency.id'});

    # Variables pour la vue
    our %tabViewData = (
                        'locale.id'             => $tabUserInfos->{'locale.id'},
                        'user.agency.id'        => $tabUserInfos->{'nomadAgency.id'},
                        'countryId'             => $tabUserInfos->{'nomadCountry.id'},
                        'currencySymbol'        => $tabUserInfos->{'currency.symbol'},
                       );


    #Récupération des quantités de carburant saisies
    $tabViewData{'fuel'}          = &LOC::Request::getHash('fuel', {});
    #Récupération de la valdiation
    $tabViewData{'valid'}         = &LOC::Request::getString('valid', '');

    #Tableau d'erreur actions
    my @errorsTab;

    #Tableau contenant les lignes ayant des erreurs
    my %tabLineError;
    #Tableau contenant les lignes enregistrées avec succès
    my %tabLineValid;

     # Chargement des données de la liste des carburants
    $tabViewData{'tabFuel'} = &LOC::Contract::Rent::getFuelList($countryId, $agencyId, LOC::Util::GETLIST_ASSOC);

    # Prix de base du carburant du pays concerné
    my $tabCountryData = &LOC::Country::getInfos($countryId);
    $tabViewData{'CARBURANT'} = $tabCountryData->{'fuelUnitPrice'};

    #Récupération URL fiche client + url machine
    my $tabURL = $tabViewData{'tabFuel'};
    while (my ($id, $url) = each(%$tabURL))
    {
        #url vers la fiche client
        $url->{'urlCustomerInfo'} = &LOC::Customer::getUrl($url->{'customer.id'});
        #url vers la machine
        $url->{'UrlMachine'}      = &LOC::Machine::getUrl($url->{'machine.id'});
    }


    #Initalisation du compteur des alerets à mettre à jour
    my $cpt = 0;

    #Si on clique sur le bouton Attribuer
    if ($tabViewData{'valid'} eq 'ok')
    {
        # Boucle sur les montants de carburant saisis
        foreach my $contractId (keys %{$tabViewData{'fuel'}})
        {

            my $tabErrors = {
                             'errors'  => [],
                             'modules' => {}
                            };

            #Récupération infos du contrat
            my $tabInfos  = &LOC::Contract::Rent::getInfos($countryId, $contractId, &LOC::Contract::Rent::GETINFOS_FUEL);
            #Id du carburant
            my $fuelId = $tabInfos->{'fuel.id'};
            #Code du contrat
            my $code = $tabInfos->{'code'};


            #Quantité saisie de carburant
            my $quantity = $tabViewData{'fuel'}->{$contractId}->{'qty'};

            #Si mise à jour OK et si la saisie de la quantité est <> de vide
            if ($quantity ne '')
            {
                if (!$fuelId)
                {
                    #Mise à jour des carburants pour le  contrat concerné
                    my $result = &LOC::Contract::Rent::updateFuel($tabViewData{'countryId'}, $contractId, $quantity, $currentUserId, 0, $tabErrors, undef);
    
                    if ($result > 0)
                    {
                        #Mise à jour du compteur des alertes si Quantité >= 0 et si le carburant n'a jamais été saisi
                        if ((&LOC::Util::round($quantity, 2) >= 0) && ($fuelId == 0))
                        {
                            #Incrémentation du compteur
                            $cpt++;
                            $tabLineValid{$contractId} = 'Enregistrement effectué avec succès.' ;
                            $tabViewData{'valid'} = 'saved';
                        }
                    }
                    else
                    {
                        #Si une quantité est saisie : affichage des messages
                        if ((&LOC::Util::round($quantity, 2) >= 0))
                        {
                            if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
                            {
                                push(@errorsTab, $code);
                                $tabLineError{$contractId} = 'Erreur fatale.' ;# Erreur fatale
                            }
                            elsif (&LOC::Util::in_array(0x0002, $tabErrors->{'fatal'}))
                            {
                                push(@errorsTab, $code);
                                $tabLineError{$contractId} = 'Impossible de modifier le contrat.' ; # Bloqué en écriture
                            }
                            elsif (&LOC::Util::in_array(0x0003, $tabErrors->{'fatal'}))
                            {
                                push(@errorsTab, $code);
                                $tabLineError{$contractId} = 'Vous n\'avez pas les droits.' ; # Droits
                            }
                            else
                            {
                                #Autre message
                                push(@errorsTab, $code);
                                $tabLineError{$contractId} = 'Erreur fatale.' ;
                            }
                            $tabViewData{'valid'} = '';
                        }
                    }
                }
            }
        }#fin du foreach

        $tabViewData{'fuel'} = {};
        $tabViewData{'messages'}     = {'error' => \@errorsTab};

        #Affichage d'une icône en bout de ligne si c ok ou ko
        $tabViewData{'messagesLine'} = {'error' => \%tabLineError, 'valid' => \%tabLineValid};

    }#fin du valid


    # (Re) Chargement des données
    #Récupération de la quantité saisie de carburant pour l'afficher une fois validée.
    while (my ($id, $url) = each(%$tabURL))
    {
        $tabViewData{'CONTRACT'}  = &LOC::Contract::Rent::getInfos($countryId, $url->{'id'}, &LOC::Contract::Rent::GETINFOS_FUEL);
        $url->{'fuel.quantity'}   = $tabViewData{'CONTRACT'}->{'fuel.quantity'};
    }

    # Message de confirmation de succès si aucune erreur sur aucune ligne
    if (($tabViewData{'valid'} eq 'saved') && (@errorsTab == 0))
    {
        $tabViewData{'messages'} = {'valid' => 'Les modifications ont été enregistrées avec succès'};
    }
    if (($tabViewData{'valid'} eq 'ok') && (@errorsTab == 0))
    {
        $tabViewData{'messages'} = {'valid' => 'Aucune modification enregistrée'};
    }



    # Affichage de la vue
    require $viewDirectory . 'type.cgi';
}

1;