use utf8;
use open (':encoding(UTF-8)');

# package: technical
# Contrôleur d'affichage du carburant
package estimateFuel;

use strict;
use File::Basename;

use LOC::Agency;
use LOC::Country;
use LOC::Session;
use LOC::Contract::Rent;
use LOC::Machine;
use LOC::Alert;
use LOC::TariffFamily;
use LOC::Json;

my $viewDirectory = &dirname(__FILE__);
$viewDirectory =~ s/(.*)\/.+/$1/;
$viewDirectory .= '/view/estimateFuel/';



# Function: typeAction
# Interface de saisie du carburant
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos  = $tabParameters->{'tabUserInfos'};
    my $currentUserId = $tabUserInfos->{'id'};

    my $countryId  = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $agencyId   = &LOC::Request::getString('agencyId', $tabUserInfos->{'nomadAgency.id'});

    # Variables pour la vue
    our %tabViewData = (
                        'locale.id'             => $tabUserInfos->{'locale.id'},
                        'user.agency.id'        => $tabUserInfos->{'nomadAgency.id'},
                        'countryId'             => $tabUserInfos->{'nomadCountry.id'},
                        'currencySymbol'        => $tabUserInfos->{'currency.symbol'},
                       );


    #Récupération des quantités de carburant saisies
    my $tabEstimateFuel  = &LOC::Request::getHash('estimateFuel', {});

    #Récupération de la valdiation
    $tabViewData{'valid'}         = &LOC::Request::getString('valid', '');

    #Tableau d'erreur actions
    my @errorsTab;

    #Tableau contenant les lignes ayant des erreurs
    my %tabLineError;
    #Tableau contenant les lignes enregistrées avec succès
    my %tabLineValid;

    #Initalisation du compteur des alertes à mettre à jour
    my $cpt = 0;
    my $tabLineError = {};
    #Si on clique sur le bouton Attribuer
    if ($tabViewData{'valid'} eq 'ok')
    {
        # Boucle sur les montants de carburant saisis
        foreach my $familyId (keys %$tabEstimateFuel)
        {
            my $tabErrors = {
                             'errors'  => [],
                             'modules' => {}
                            };

            if ($tabEstimateFuel->{$familyId}->{'toUpdate'})
            {
                if (!&LOC::TariffFamily::updateFuel($tabViewData{'countryId'}, $familyId, $tabEstimateFuel->{$familyId}, $tabErrors))
                {
                    $tabViewData{'messages'}->{'error'} = 1;
                    if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
                    {
                        $tabViewData{'messagesLine'}->{$tabEstimateFuel->{$familyId}} = {'family.fullLabel' => $tabEstimateFuel->{$familyId}->{'fullLabel'},
                                                                                          'error' => 'fatalError'}; # erreur fatale
                    }
                    elsif (&LOC::Util::in_array(0x0002, $tabErrors->{'fatal'}))
                    {
                        $tabViewData{'messagesLine'}->{$tabEstimateFuel->{$familyId}} = {'family.fullLabel' => $tabEstimateFuel->{$familyId}->{'fullLabel'}, 
                                                                                          'error' => 'emptyQty'} ; # quantité vide
                    }
                    else
                    {
                        $tabViewData{'messagesLine'}->{$tabEstimateFuel->{$familyId}} = {'family.fullLabel' => $tabEstimateFuel->{$familyId}->{'fullLabel'},
                                                                                          'error' => 'fatalError'}; # erreur fatale
                    }
                    
                    $tabViewData{'infoLine'}->{$familyId} = 'isError';
                    $tabLineError->{$familyId} = $tabEstimateFuel->{$familyId};
                }
                else
                {
                    $tabViewData{'infoLine'}->{$familyId} = 'isUpdated';
                }
            }

        }#fin du foreach
        if (!defined $tabViewData{'messages'}->{'error'})
        {
            $tabViewData{'messages'}->{'valid'} = 'updated';
        }

    }#fin du valid

     # Chargement des données de la liste des carburants
    $tabViewData{'tabTariffFamily'}  = &LOC::TariffFamily::getList($countryId, LOC::Util::GETLIST_ASSOC);
    $tabViewData{'fuelEnergiesList'} = &LOC::MachinesFamily::getFuelEnergiesList();

    # (Re) Chargement des données
    #Récupération de la quantité saisie de carburant pour l'afficher une fois validée.
    if ($tabViewData{'valid'} ne '')
    {
        my @tabLineErrorId = keys(%$tabLineError);
        while (my ($id, $line) = each(%{$tabViewData{'tabTariffFamily'}}))
        {
            my $qty = (&LOC::Util::in_array($line->{'id'}, \@tabLineErrorId) ? $tabLineError->{$line->{'id'}}->{'qty'} : $line->{'family.estimateFuel'});
            my $max = (&LOC::Util::in_array($line->{'id'}, \@tabLineErrorId) ? $tabLineError->{$line->{'id'}}->{'max'} : $line->{'family.fuelMax'});
            
            $line->{'family.estimateFuel'} = $qty;
            $line->{'family.fuelMax'} = $max;
        }

    }

    # Affichage de la vue
    require $viewDirectory . 'view.cgi';
}

1;