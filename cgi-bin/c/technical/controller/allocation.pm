use utf8;
use open (':encoding(UTF-8)');

# Package: proformaAlerts
# Contrôleur pour les alertes de la proforma
package allocation;

use strict;
use File::Basename;

use LOC::Customer;
use LOC::Machine;
use LOC::Contract::Rent;
use LOC::Characteristic;


# Function: machinesAllocationListAction
# Alerte des machines à attribuer
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId      = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $agencyId       = &LOC::Request::getString('agencyId', $tabUserInfos->{'nomadAgency.id'});

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    $tabViewData{'valid'}        = &LOC::Request::getString('valid', '');

    my $tabAllocateMachine = &LOC::Request::getHash('allocate', {});

    # Traitement
    if ($tabViewData{'valid'} eq 'ok')
    {
        foreach my $contractId (keys %$tabAllocateMachine)
        {
            my $tabErrors = {
                'fatal'  => []
            };
            if ($tabAllocateMachine->{$contractId}->{'machineId'})
            {
                my $tabContractData = {
                    'machine.id'        => $tabAllocateMachine->{$contractId}->{'machineId'},
                    'isFinalAllocation' => 1,
                    'tabRentTariff'     => {'isKeepAmount' => 1}
                };
                if (!&LOC::Contract::Rent::update($countryId, $contractId, $tabContractData, $tabUserInfos->{'id'}, $tabErrors, undef))
                {
                    $tabViewData{'messages'}->{'error'} = 1;
                    if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
                    {
                         $tabViewData{'messagesLine'}->{$contractId} = {'code' => $tabAllocateMachine->{$contractId}->{'contractCode'}, 'error' => 'fatalError'};
                    }
                    elsif (&LOC::Util::in_array(0x0002, $tabErrors->{'fatal'}))
                    {
                         $tabViewData{'messagesLine'}->{$contractId} = {'code' => $tabAllocateMachine->{$contractId}->{'contractCode'}, 'error' => 'writeLocked'};
                    }
                    elsif (&LOC::Util::in_array(0x0301, $tabErrors->{'fatal'}))
                    {
                         $tabViewData{'messagesLine'}->{$contractId} = {'code' => $tabAllocateMachine->{$contractId}->{'contractCode'}, 'error' => 'rentTariffError'};
                    }
                    elsif (&LOC::Util::in_array(0x0302, $tabErrors->{'fatal'}))
                    {
                         $tabViewData{'messagesLine'}->{$contractId} = {'code' => $tabAllocateMachine->{$contractId}->{'contractCode'}, 'error' => 'transportTariffError'};
                    }
                    elsif (&LOC::Util::in_array(0x0303, $tabErrors->{'fatal'}))
                    {
                         $tabViewData{'messagesLine'}->{$contractId} = {'code' => $tabAllocateMachine->{$contractId}->{'contractCode'}, 'error' => 'customerNotVerified'};
                    }
                    elsif (&LOC::Util::in_array(0x0304, $tabErrors->{'fatal'}))
                    {
                         $tabViewData{'messagesLine'}->{$contractId} = {'code' => $tabAllocateMachine->{$contractId}->{'contractCode'}, 'error' => 'proformaNotGen'};
                    }
                    elsif (&LOC::Util::in_array(0x0305, $tabErrors->{'fatal'}))
                    {
                         $tabViewData{'messagesLine'}->{$contractId} = {'code' => $tabAllocateMachine->{$contractId}->{'contractCode'}, 'error' => 'proformaNotUpToDate'};
                    }
                    elsif (&LOC::Util::in_array(0x0306, $tabErrors->{'fatal'}))
                    {
                         $tabViewData{'messagesLine'}->{$contractId} = {'code' => $tabAllocateMachine->{$contractId}->{'contractCode'}, 'error' => 'proformaNotPayed'};
                    }
                    elsif (&LOC::Util::in_array(0x0307, $tabErrors->{'fatal'}))
                    {
                         $tabViewData{'messagesLine'}->{$contractId} = {'code' => $tabAllocateMachine->{$contractId}->{'contractCode'}, 'error' => 'machineUnavailable'};
                    }
                    elsif (&LOC::Util::in_array(0x0308, $tabErrors->{'fatal'}))
                    {
                         $tabViewData{'messagesLine'}->{$contractId} = {'code' => $tabAllocateMachine->{$contractId}->{'contractCode'}, 'error' => 'machineAllocated'};
                    }
                    elsif (&LOC::Util::in_array(0x0309, $tabErrors->{'fatal'}))
                    {
                         $tabViewData{'messagesLine'}->{$contractId} = {'code' => $tabAllocateMachine->{$contractId}->{'contractCode'}, 'error' => 'modelNotCorrect'};
                    }
                    elsif (&LOC::Util::in_array(0x0310, $tabErrors->{'fatal'}))
                    {
                         $tabViewData{'messagesLine'}->{$contractId} = {'code' => $tabAllocateMachine->{$contractId}->{'contractCode'}, 'error' => 'contractStateChange'};
                    }
                    elsif (&LOC::Util::in_array(0x0140, $tabErrors->{'fatal'}))
                    {
                        if (&LOC::Util::in_array(0x0003, $tabErrors->{'modules'}->{'machine'}->{'fatal'}))
                        {
                            $tabViewData{'messagesLine'}->{$contractId} = {'code' => $tabAllocateMachine->{$contractId}->{'contractCode'}, 'error' => 'rights'};
                        }
                    }

                    $tabViewData{'infoLine'}->{$contractId} = 'isError';
                }
                else
                {
                    $tabViewData{'infoLine'}->{$contractId} = 'isUpdated';
                }
            }
        }
        if (!defined $tabViewData{'messages'}->{'error'})
        {
            $tabViewData{'messages'}->{'valid'} = 'updated';
        }
    }


    # Récupération des informations pour la vue
    $tabViewData{'tabContracts'} = &_getContractsList($countryId, $agencyId);

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/allocation/view.cgi';
}

# Function _getContractsList
# Récupération de la liste des contrats avec machine à attribuer
#
# Params
# string    $countryId     - Pays
# string    $agencyId      - Agence
#
# Return
# hashref
sub _getContractsList
{
    my ($countryId, $agencyId) = @_;

    my $tabFilters = {
        'stateId'   => LOC::Contract::Rent::STATE_PRECONTRACT,
        'agencyId'  => $agencyId,
        'machineId' => 0
    };
    my $tabList = &LOC::Contract::Rent::getList($countryId,
                                                LOC::Util::GETLIST_ASSOC,
                                                $tabFilters,
                                                LOC::Contract::Rent::GETINFOS_CUSTOMER |
                                                LOC::Contract::Rent::GETINFOS_EXT_MODEL |
                                                LOC::Contract::Rent::GETINFOS_TARIFFRENT |
                                                LOC::Contract::Rent::GETINFOS_TARIFFTRSP |
                                                LOC::Contract::Rent::GETINFOS_EXT_WISHEDMACHINE |
                                                LOC::Contract::Rent::GETINFOS_EXT_SITE);

    foreach my $contract (values %$tabList)
    {
        $contract->{'customer.url'} = &LOC::Customer::getUrl($contract->{'customer.id'});
        $contract->{'tabAvailableMachines'} = &_getAvailableMachines($countryId, $agencyId, $contract->{'model.id'}, $contract->{'isOutOfPark'});
        $contract->{'tabErrors'} = &_getContractErrors($countryId, $contract);
    }

    return $tabList;
}

# Function _getAvailableMachines
# Récupère la liste des machines disponibles pour attribution sur un contrat
#
# Params
# string    $countryId     - Pays
# string    $agencyId      - Agence
# int       $modelId       - Modèle de machine
# bool      $isOutOfPark   - hors parc ?
#
# Return
# hashref
sub _getAvailableMachines
{
    my ($countryId, $agencyId, $modelId, $isOutOfPark) = @_;

    my $tabFilters = {
        'stateId'                    => LOC::Machine::STATE_AVAILABLE,
        'modelId'                    => $modelId,
        'isOutOfPark'                => $isOutOfPark,
        'visibleOnAgencyId'          => $agencyId,
        'isVisibleMachineInTransfer' => 1
    };

    my $tabMachinesList = &LOC::Machine::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters, LOC::Machine::GETINFOS_BASE, {'level' => LOC::Machine::LEVEL_MODEL});

    return $tabMachinesList;
}

# Function _getContractErrors
# Récupérer la liste des erreurs sur le contrat empêchant l'attribution de machine
#
# Params:
# string   $countryId       - Pays
# hashref  $tabInfoContract - Infos sur le contrat
#
# Return:
# arrayRef
sub _getContractErrors
{
    my ($countryId, $tabInfoContract) = @_;

    my $tabErrors = [];
    # Machines disponibles ?
    my $countMachineAvailable = keys(%{$tabInfoContract->{'tabAvailableMachines'}});
    if ($countMachineAvailable == 0)
    {
        push(@$tabErrors, 'noMachine');
    }
    # RE loc/transp
    if ($tabInfoContract->{'rentTariff.specialReductionStatus'} eq LOC::Tariff::Rent::REDUCTIONSTATE_PENDING)
    {
        push(@$tabErrors, 'rentTariff');
    }
    if ($tabInfoContract->{'transportTariff.specialReductionStatus'} eq LOC::Tariff::Transport::REDUCTIONSTATE_PENDING)
    {
        push(@$tabErrors, 'transportTariff');
    }

    # Client avec ouverture de compte en attente
    if ($tabInfoContract->{'isLocked'} || $tabInfoContract->{'customer.isNewAccount'})
    {
        push(@$tabErrors, 'customerNotVerified');
    }
    # Vérifications Pro forma
    # - Pro forma non générée
    if (($tabInfoContract->{'proformaFlags'} & LOC::Proforma::DOCFLAG_CT_ISCUSTOMERPFREQONCREATION &&
         $tabInfoContract->{'total'} > 0 &&  $tabInfoContract->{'customer.isProformaRequired'}) &&
         !($tabInfoContract->{'proformaFlags'} & LOC::Proforma::DOCFLAG_EXISTACTIVED))
    {
        push(@$tabErrors, 'proformaNotGen');
    }
    # - Pro forma non à jour
    if ($tabInfoContract->{'proformaFlags'} & LOC::Proforma::DOCFLAG_EXISTACTIVEDEXPIRED)
    {
        push(@$tabErrors, 'proformaNotUpToDate');
    }
    # - Pro forma non finalisée
    if ($tabInfoContract->{'proformaFlags'} & LOC::Proforma::DOCFLAG_EXISTACTIVED &&
        !($tabInfoContract->{'proformaFlags'} & LOC::Proforma::DOCFLAG_EXISTACTIVEDEXPIRED) &&
        !($tabInfoContract->{'proformaFlags'} & LOC::Proforma::DOCFLAG_ALLACTIVEDFINALIZED))
    {
        push(@$tabErrors, 'proformaNotPayed');
    }
    return $tabErrors;
}

1;