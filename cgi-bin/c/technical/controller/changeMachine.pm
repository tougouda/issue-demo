use utf8;
use open (':encoding(UTF-8)');

# Package: changeMachine
# Contrôleur pour les changements de machine sur contrat
package changeMachine;

use strict;
use File::Basename;

use LOC::Customer;
use LOC::Machine;
use LOC::Contract::Rent;
use LOC::Characteristic;
use LOC::Request;


# Function: pdfAction
# Affiche le bon de changement de machine sur contrat au format PDF
#
# Parameters:
# string   countryId     - Pays
# string   agencyId      - Agence
# int      customerId    - Identifiant du client
# arrayref contractsId   - Tableau des identifiants des contrats à imprimer
# int      nbTotal       - Total des lignes de résultats (affichage)
# int      offset        - Début de la recherche (affichage)
# int      limit         - Nombre à afficher (affichage)
sub pdfAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Récupération des paramètres
    my $countryId   = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $agencyId    = &LOC::Request::getString('agencyId', $tabUserInfos->{'nomadAgency.id'});
    
    my $contractId   = &LOC::Request::getInteger('contractId', 0);
    my $oldMachineId = &LOC::Request::getInteger('machineId', 0);

    my $tabContract = &LOC::Contract::Rent::getInfos($countryId, $contractId, LOC::Contract::Rent::GETINFOS_EXT_SITE |
                                                                              LOC::Contract::Rent::GETINFOS_EXT_MACHINE);

    my $tabMachineInfos = &LOC::Machine::getInfos($countryId, $oldMachineId, LOC::Machine::GETINFOS_ALL, {'level' => LOC::Machine::LEVEL_MACHINESFAMILY});
    my $tabCustomerInfos = &LOC::Customer::getInfos($countryId, $tabContract->{'customer.id'});

    my $tabContractInfos = {
        'code'               => $tabContract->{'code'},
        'machine.tabInfos' => {
            'parkNumber'                => $tabContract->{'machine.tabInfos'}->{'parkNumber'},
            'model.label'               => $tabContract->{'machine.tabInfos'}->{'model.label'},
            'machinesFamily.shortLabel' => $tabContract->{'machine.tabInfos'}->{'machinesFamily.shortLabel'},
            'machinesFamily.elevation'  => $tabContract->{'machine.tabInfos'}->{'machinesFamily.elevation'},
            'machinesFamily.energy'     => $tabContract->{'machine.tabInfos'}->{'machinesFamily.energy'},
            'serialNumber'              => $tabContract->{'machine.tabInfos'}->{'serialNumber'}
        },
        'site.tabInfos'    => {
            'label'             => $tabContract->{'site.tabInfos'}->{'label'},
            'address'           => $tabContract->{'site.tabInfos'}->{'address'},
            'locality.label'    => $tabContract->{'site.tabInfos'}->{'locality.label'},
            'contact.fullName'  => $tabContract->{'site.tabInfos'}->{'contact.fullName'},
            'contact.telephone' => $tabContract->{'site.tabInfos'}->{'contact.telephone'}
        }
    };

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'          => $tabUserInfos->{'locale.id'},
        'user.agency.id'     => $agencyId,
        'currencySymbol'     => $tabUserInfos->{'currency.symbol'},
        'tabContractInfos'   => $tabContractInfos,
        'tabOldMachineInfos' => $tabMachineInfos,
        'tabCustomerInfos'   => $tabCustomerInfos
    );
    

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;

    require $directory . '/view/changeMachine/pdf.cgi';
}


1;