use utf8;
use open (':encoding(UTF-8)');
binmode STDOUT, ":raw";    # Pour envoyer le flux en l'état

use strict;
use locale;

use LOC::Date;
use LOC::Locale;
use LOC::Pdf;
use LOC::Util;
use LOC::Browser;

our %tabViewData;

# Locale
my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});


# Titre de la page
my @tabLocaltime = localtime();
my $title = $locale->t('Changement de machine');

# Création de l'objet PDF
my $pdf = LOC::Pdf->new($title, $tabViewData{'user.agency.id'}, \$locale);
my $pdfObj = $pdf->getPdfObject();
#
# Dimensions de la page
my $margin = 10/mm;
my $pageWidth = $pdf->getPageWidth();
my $textWidth = $pageWidth - 2 * $margin;

my $pageHeight = $pdf->getPageHeight();
my $textHeight = $pageHeight - 2 * $margin;
my $pageLimit = $margin;

my $gap = 2/pt;


# Polices
my $regularFont    = $pdf->{'font'}->{'regular'};
my $italicFont     = $pdf->{'font'}->{'italic'};
my $boldFont       = $pdf->{'font'}->{'bold'};
my $boldItalicFont = $pdf->{'font'}->{'bolditalic'};
my $dingbatsFont   = $pdfObj->corefont('ZapfDingbats');


# Définition du sujet et des mots-clés
$pdf->setSubject($locale->t('Changement de machine'));
$pdf->setKeywords($locale->t('Changement de machine'));



# Nouvelle page
my $page = $pdf->addPage(LOC::Pdf::DISPLAY_TITLE | LOC::Pdf::DISPLAY_AGENCY | LOC::Pdf::DISPLAY_FULLFOOTER);
my $text = $page->text();
my $gfx  = $page->gfx();


# Etat et date de sortie
my $number = $locale->t('au %s', $locale->getDateFormat(undef, LOC::Locale::FORMAT_DATE_NUMERIC));
$gfx->textlabel($pageWidth - 10/mm - 95/mm, $pageHeight - 35/mm - 16/pt * 1.2, $boldFont, 10/pt, $number);


# Références
my $phoneNumber = $locale->getPhoneNumberFormat($tabViewData{'tabCustomerInfos'}->{'telephone'});
my $faxNumber   = $locale->getPhoneNumberFormat($tabViewData{'tabCustomerInfos'}->{'fax'});

my $siteInfos = $tabViewData{'tabContractInfos'}->{'site.tabInfos'}->{'label'} . "\n" .
                $tabViewData{'tabContractInfos'}->{'site.tabInfos'}->{'address'} . "\n" .
                $tabViewData{'tabContractInfos'}->{'site.tabInfos'}->{'locality.label'} . "\n";

my @tabReferences = (
    {'label' => $locale->t('N° contrat'), 'value' => $tabViewData{'tabContractInfos'}->{'code'}},
    {'label' => $locale->t('Chantier'), 'value' => $tabViewData{'tabContractInfos'}->{'site.tabInfos'}->{'label'}},
    {'label' => '', 'value' => $tabViewData{'tabContractInfos'}->{'site.tabInfos'}->{'address'}},
    {'label' => '', 'value' => $tabViewData{'tabContractInfos'}->{'site.tabInfos'}->{'locality.label'}},
    {'label' => $locale->t('Contact chantier'), 'value' => $tabViewData{'tabContractInfos'}->{'site.tabInfos'}->{'contact.fullName'}},
    {'label' => '', 'value' => $tabViewData{'tabContractInfos'}->{'site.tabInfos'}->{'contact.telephone'}},
);
$pdf->addCustomBlock(\$page, $locale->t('Références'), \@tabReferences, {'x' => 10/mm, 'y' => 55/mm});

# Coordonnées client
my $tabCustomer = [
    $tabViewData{'tabCustomerInfos'}->{'name'},
    $tabViewData{'tabCustomerInfos'}->{'address1'},
    $tabViewData{'tabCustomerInfos'}->{'address2'},
    ' ',
    $tabViewData{'tabCustomerInfos'}->{'locality.postalCode'} . ' ' . $tabViewData{'tabCustomerInfos'}->{'locality.name'},
    ' ',
    $locale->t('Tél.') . ' ' . $tabViewData{'tabCustomerInfos'}->{'telephone'},
];
$pdf->addCustomerBlock(\$page, $tabCustomer);


my $x = $margin;
my $y = $pageHeight - 95/mm;


$text->lead(10/pt);
$y -= $text->lead() + 1/mm;

$text->font($boldFont, 8/pt);
$gfx->linewidth(1/pt);

$text->translate($x, $y);
my $colWidth = ($pageWidth - $margin * 2) / 2;

$text->text($locale->t('Machine louée'), -maxWidth => $colWidth);
$gfx->move($x, $y - 1/mm);
$x += $colWidth;
$text->translate($x + $gap, $y);
$gfx->line($x - $gap, $y - 1/mm);

$text->text($locale->t('Nouvelle machine'), -maxWidth => $colWidth);
$gfx->move($x, $y - 1/mm);
$x += $colWidth;
$text->translate($x + $gap, $y);
$gfx->line($x - $gap, $y - 1/mm);

$gfx->stroke();

$text->font($regularFont, 9/pt);
$y -= $text->lead() * 2 + 1/mm;

my $topY = $y;

# Machine louée
$x = $margin;

my $tabMachinesInfos = [
    {'label' => $locale->t('N° parc'), 'new' => $tabViewData{'tabContractInfos'}->{'machine.tabInfos'}->{'parkNumber'}, 'old' => $tabViewData{'tabOldMachineInfos'}->{'parkNumber'}, 'options' => 'bold'},
    {'label' => $locale->t('Modèle de machines'), 'new' => $tabViewData{'tabContractInfos'}->{'machine.tabInfos'}->{'model.label'}, 'old' => $tabViewData{'tabOldMachineInfos'}->{'model.label'}},
    {'label' => $locale->t('Famille de machines'), 'new' => $tabViewData{'tabContractInfos'}->{'machine.tabInfos'}->{'machinesFamily.shortLabel'}, 'old' => $tabViewData{'tabOldMachineInfos'}->{'machinesFamily.shortLabel'}},
    {'label' => $locale->t('Hauteur'), 'new' => $tabViewData{'tabContractInfos'}->{'machine.tabInfos'}->{'machinesFamily.elevation'}, 'old' => $tabViewData{'tabOldMachineInfos'}->{'machinesFamily.elevation'}},
    {'label' => $locale->t('Énergie'), 'new' => $tabViewData{'tabContractInfos'}->{'machine.tabInfos'}->{'machinesFamily.energy'}, 'old' => $tabViewData{'tabOldMachineInfos'}->{'machinesFamily.energy'}},
    {'label' => $locale->t('N° série'), 'new' => $tabViewData{'tabContractInfos'}->{'machine.tabInfos'}->{'serialNumber'}, 'old' => $tabViewData{'tabOldMachineInfos'}->{'serialNumber'}}
];

foreach my $machinesInfos (@$tabMachinesInfos)
{
    $x = $margin;

    # Affichage ancienne machine
    $text->translate($x, $y);
    $text->text($machinesInfos->{'label'} . ' : ');
    $text->translate($x + 30/mm, $y);
    if (defined $machinesInfos->{'options'} && $machinesInfos->{'options'} eq 'bold')
    {
        $text->font($boldFont, 9/pt);
    }
    $text->text($machinesInfos->{'old'});

    $text->font($regularFont, 9/pt);

    # Affichage nouvelle machine
    $x += $colWidth + $gap;
    $text->translate($x, $y);
    $text->text($machinesInfos->{'label'} . ' : ');
    $text->translate($x + 30/mm, $y);
    if (defined $machinesInfos->{'options'} && $machinesInfos->{'options'} eq 'bold')
    {
        $text->font($boldFont, 9/pt);
    }
    $text->text($machinesInfos->{'new'});

    $text->font($regularFont, 9/pt);
    $y -= $text->lead() + 1/mm;
}

# Signature
$text->translate($x, $y - 30/mm);
$text->text($locale->t('Le responsable d\'agence'));


# Sortie
my $pdfString = $pdf->getString();
$pdf->end();

my $filename = sprintf('Changement_machine_%04d-%02d-%02d.pdf', $tabLocaltime[5] + 1900,
                                                                $tabLocaltime[4] + 1,
                                                                $tabLocaltime[3]);
&LOC::Browser::sendHeaders(
        "Content-type: application/pdf;\n"
        . "Content-Disposition: inline; filename=\"" . $filename . "\";\n\n"
    );

print $pdfString;