use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;
use LOC::Util;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;

# Répertoire CSS/JS/Images
my $dir = 'technical/fuel/';


our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});
our $currencySymbol = $tabViewData{'currencySymbol'};


sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'type.css');
$view->addJSSrc($dir . 'type.js');

# Titre
$view->setPageTitle($locale->t('Carburant'));
$view->setTitle($locale->t('Carburant'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

# CSS
$view->addCSSSrc('location.css');

# Message de confirmation de validation avec succès
if ($tabViewData{'messages'}->{'valid'})
{
    my $validBlock = '
<div id="validBlock" class="messageBlock valid" >
    <div class="popup" style="display: block;">
        <div class="content">
            <table>
            <tbody>
                <tr>
                    <td style="font-weight: bold;">' . $locale->t($tabViewData{'messages'}->{'valid'}) . '.</td>
                </tr>
                <tr class="buttons">
                    <td>' .
                    $view->displayTextButton($locale->t('Ok'), '',
                                             '$ge("validBlock").style.display = "none";',
                                             '', 'l', {'id' => 'validBlockBtn'}) .
                    '</td>
                </tr>
            </tbody>
            </table>
        </div>
    </div>
</div>';

    $view->addBodyContent($validBlock, 0, 1);
    $view->addBodyEvent('load', 'window.document.getElementById("validBlockBtn").focus();');
}

# Message d'erreur sur une ou plusieurs lignes
my $errorBlock = '
<div id="errorBlock" class="messageBlock error"' . ($tabViewData{'messages'}->{'error'} ? '' : ' style="display: none;"') . '>
    <div class="popup" style="display: block;">
        <div class="content">
            <table>
                <tbody id="errorsTable">
                    <tr>
                        <td>
                            <ul style="font-weight : bold;">' . $locale->t('Des erreurs ont été détectées sur le(s) contrat(s) ');
# Erreurs retournées depuis PERL
# N° de lignes en erreur
my $listNumLine = '';
for my $error (@{$tabViewData{'messages'}->{'error'}})
{
    $listNumLine .= $error . ', ';
}
$errorBlock .= substr($listNumLine, 0, -2) . '.</ul>
                        </td>
                    </tr>
                    <tr class="buttons" id="buttonRow">
                        <td>' .
                        $view->displayTextButton($locale->t('Fermer'), '', '$ge("errorBlock").style.display = "none";') .
                        '</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>';

$view->addBodyContent($errorBlock, 0, 1);



# Bouton "actualiser"
my $url = $view->createURL('technical:fuel:type');
my $reload = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png', $url,
                                      $locale->t('Actualiser la page'));
$view->addControlsContent([$reload], 'right', 1);



# Affichage de l'entête
print $view->displayHeader();

print $view->startForm();

my @tabContracts = values %{$tabViewData{'tabFuel'}};
if (@tabContracts == 0)
{
    # Pas de résultat
    print '<div class="noResult">' . $view->displayMessages('info', [$locale->t('Aucun carburant n\'est à saisir')], 0) . '</div>';
}
else
{
    our $tabIndex = 1;
    print '
<table class="standard ex">
<thead>
    <tr>
        <th class="parkNumber">' . htmllocale('N° Parc') . '</th>
        <th>' . htmllocale('Modèle') . '</th>
        <th>' . htmllocale('N° Contrat') . '</th>
        <th>' . htmllocale('Date Départ') . '</th>
        <th>' . htmllocale('Date Retour') . '</th>
        <th>' . htmllocale('Client') . '</th>
        <th>' . htmllocale('Quantité Carburant (L)') . '</th>
        <th>' . htmllocale('Estimation carburant (L)') . '</th>
        <th></th>
    </tr>
</thead>
<tbody>';
    my $i = 1;
    foreach my $fuel (@tabContracts)
    {
        # URL contrat de location
        my $urlContract = $view->createURL('rent:rentContract:view', {'contractId' => $fuel->{'id'}});

        print '
    <tr class="rowBg'. ($i % 2) .'">
        <td class="parkNumber">' . $view->displayParkNumber($fuel->{'machine.parkNumber'}) . '</td>
        <td><a href="' . $fuel->{'UrlMachine'} . '" target="_blank">' . $fuel->{'model.label'} . ' </a></td>
        <td><a target="_blank" href="' . $urlContract . '">' . $fuel->{'code'} . '</a></td>
        <td>' . $locale->getDateFormat($fuel->{'beginDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC) . '</td>
        <td>' . $locale->getDateFormat($fuel->{'endDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC) . '</td>
        <td><a href="' . $fuel->{'urlCustomerInfo'} . '" target="_blank">' . $fuel->{'customer.name'} . '</a></td>';
        # Zone de saisie du carburant
        my $fuelInput = $view->displayFormInputText('fuel[' . $fuel->{'id'} . '][qty]',
                                                    '', 
                                                    LOC::Html::TEXTMODE_NUMERIC, 
                                                    {
                                                        'size'     => 10,
                                                        'tabindex' => $tabIndex++,
                                                        'onchange' => (defined $fuel->{'fuelEstimate'} ? 'checkFuelEstimate(' . $fuel->{'id'} . ', this.value);' : '')
                                                    });

        # Contrat cadre : affichage de la petite calculette
        if ($fuel->{'fuel'} ne '')
        {
            # Montant du prix au litre du contrat cadre concerné
            my $fuelAmtInput = $view->displayFormInputHidden('calculate[' . $fuel->{'id'} . '][fuelAmount]',
                                                             &LOC::Util::round($fuel->{'fuel'}, $locale->getFracDigits()));

            # Affichage du bouton Calculette
            my $calculateBtns =  $view->displayTextButton('', 'calc.png', 'calcul_fuel(' . $fuel->{'id'} . ');', $locale->t('Contrat Cadre'));
            print '
        <td>' . ((!$tabViewData{'messagesLine'}->{'valid'}->{$fuel->{'id'}}) ? $fuelInput . ' ' . $calculateBtns . $fuelAmtInput : $fuel->{'fuel.quantity'}) . '</td>';
        }
        else
        {
            print '
        <td>' . ((!$tabViewData{'messagesLine'}->{'valid'}->{$fuel->{'id'}}) ? $fuelInput : $fuel->{'fuel.quantity'}) . '</td>';
        }

        # Estimation carburant sur la proforma
        if (defined $fuel->{'fuelEstimate'})
        {
            print '
        <td class="fuelEstimate"><input type="hidden" value="' . &LOC::Util::getNonLocaleFloat($fuel->{'fuelEstimate'}) . '" id="estimate_' . $fuel->{'id'} . '" >' . $fuel->{'fuelEstimate'} . 
                    ' <span id="estimateWarning_' . $fuel->{'id'} . '" style="visibility:hidden;">' . 
                    $view->displayFormInputImage('', 'iconWarning.gif', '', {'title' => $locale->t('Carburant saisi différent de l\'estimation')}) . '</span></td>';
        }
        else
        {
            print '
        <td></td>';
        }

        # Erreur ou validation détectée
        my $errorBtns = '<span id="lineBtnError_' . $fuel->{'id'} . '" style="display: ' . (($tabViewData{'messagesLine'}->{'error'}->{$fuel->{'id'}}) ? 'block;' : 'none;') . '">' .
                            $view->displayTextButton('', 'delete(|Over).png', '', $locale->t($tabViewData{'messagesLine'}->{'error'}->{$fuel->{'id'}})) .
                        '</span>';
        my $validBtns = '<span id="lineBtnValid_' . $fuel->{'id'} . '" style="display: ' . (($tabViewData{'messagesLine'}->{'valid'}->{$fuel->{'id'}}) ? 'block;' : 'none;') . '">' .
                            $view->displayTextButton('', 'validGreen(|Over).png', '', $locale->t($tabViewData{'messagesLine'}->{'valid'}->{$fuel->{'id'}})) .
                        '</span>';

        print '
        <td>' . (($tabViewData{'messagesLine'}->{'error'}->{$fuel->{'id'}}) ? $errorBtns : $validBtns)  . '</td>
    </tr>';

        $i++;
    }
    print '
</tbody>
</table>';

    # Bouton Valider
    my $btnValid = $view->displayTextButton(htmllocale('Valider'), 'valid(|Over).png', 'submitForm()', $locale->t('Attribuer les carburants'));

    # Affichage du Panel du bas
    print $view->displayControlPanel({'left' => [$btnValid], 'substyle' => 'bottom'});
}




print $view->displayFormInputHidden('valid', '');
print $view->displayFormInputHidden('fuelUnitPrice',&LOC::Util::round($tabViewData{'CARBURANT'}, $locale->getFracDigits()));

# Affichage du pied de page
print $view->endForm();


print $view->displayFooter();
