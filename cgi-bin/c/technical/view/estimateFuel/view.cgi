use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;

# Répertoire CSS/JS/Images
my $dir = 'technical/estimateFuel/';


our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});
our $currencySymbol = $tabViewData{'currencySymbol'};


sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'view.css');
$view->addJSSrc($dir . 'view.js');
$view->addJSSrc('common.js');

# Titre
$view->setPageTitle($locale->t('Grille d\'estimation de carburant'));
$view->setTitle($locale->t('Grille d\'estimation de carburant'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

# CSS
$view->addCSSSrc('location.css');


# Bouton "actualiser"
my $url = $view->createURL('technical:estimateFuel:view');
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png', $url,
                                          $locale->t('Actualiser la page'));
$view->addControlsContent([$refreshBtn], 'right', 1);

my $errorBlockRefreshBtn = '<span onclick="$ge(\'errorBlock\').style.display = \'none\';">' . $refreshBtn . '</span>';
my $fatalErrorMsg = htmllocale('Erreur fatale !') .
    '<p class="details">' .
    $locale->t('Veuillez %s la liste et renouveler l\'opération.', $errorBlockRefreshBtn) . '<br>' .
    htmllocale('Si l\'erreur se reproduit, merci de contacter le support informatique.') .
    '</p>';

# Messages de retour
my $tabTranslations = {
  'emptyQty'   => $locale->t('La quantité n\'est pas renseignée'),
  'fatalError' => $fatalErrorMsg,
  'updated'    => $locale->t('Les modifications ont été enregistrées'),
  'noUpdate'   => $locale->t('Aucune modification enregistrée')
};

# Message de confirmation de validation avec succès
if ($tabViewData{'messages'}->{'valid'})
{
    my $validBlock = '
    <div id="validBlock" class="messageBlock valid" >
        <div class="popup" style="display: block;">
            <div class="content">
                <table>
                <tr>
                    <td style="font-weight: bold;">' . $tabTranslations->{$tabViewData{'messages'}->{'valid'}} . '.</td>
                </tr>
                <tr class="buttons">
                    <td>' .
                    $view->displayTextButton($locale->t('Ok'), '',
                                             '$ge("validBlock").style.display = "none";',
                                             '', 'l', {'id' => 'validBlockBtn'}) .
                    '</td>
                </tr>
                </table>
            </div>
        </div>
    </div>';
    
    $view->addBodyContent($validBlock, 0, 1);
    $view->addBodyEvent('load', 'window.document.getElementById("validBlockBtn").focus();');
}


#Message d'erreur sur une ou plusieurs lignes
my $errorBlock = '
<div id="errorBlock" class="messageBlock error"'; 
($tabViewData{'messages'}->{'error'}) ?  $errorBlock .= 'style="display: visible;"' : $errorBlock .= 'style="display: none;"';
$errorBlock .= '>
    <div class="popup" style="display: block;">
        <div class="content">
            <table>
                <tbody id="errorsTable">
                    <tr>
                        <td>
                            <p style="font-weight : bold;">' . $locale->t('Des erreurs ont été détectées') . '</p>
                            <ul>';
                            #Erreurs retournées depuis PERL
                            while (my ($id, $errorLine) = each(%{$tabViewData{'messagesLine'}}))
                            {
                                $errorBlock .= '<li>' . $locale->t($errorLine->{'family.fullLabel'}) . ' : ' . $tabTranslations->{$errorLine->{'error'}} . '</li>';
                            }
                            $errorBlock .= '</ul>
                        </td>
                    </tr>
                    <tr class="buttons" id="buttonRow">
                        <td>' .
                        $view->displayTextButton($locale->t('Fermer'), '', '$ge("errorBlock").style.display = "none";') .
                        '</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>';

$view->addBodyContent($errorBlock, 0, 1);



# Affichage de l'entête
print $view->displayHeader();

print $view->startForm();


our $tabIndex = 1;
my $html = '';
$html .= '<table class="standard ex" id="listFuel" style="width: 100%;">
              <tr>
                  <th>' . htmllocale('Famille tarifaire') . '</th>
                  <th>' . htmllocale('Quantité Carburant (L) par jour') . '</th>
                  <th>' . htmllocale('Quantité Carburant max') . '</th>
              </tr>';
         my $i = 1;
         my $tabTariffFamily = $tabViewData{'tabTariffFamily'};

         while (my ($id, $family) = each(%$tabTariffFamily))
         {
             my $toUpdate = 0;
             my $startState = '';
             # définition de classe pour les familles nécessitant du carburant ou pas
             my $classFuel = 'fuelFalse';
             my $fuelDisabled = 'disabled';

             my @listFamilyEnergy = split(',', $family->{'family.energy'});
             foreach my $familyEnergy (@listFamilyEnergy)
             {
                 if (&LOC::Util::in_array($familyEnergy, $tabViewData{'fuelEnergiesList'}))
                 {
                     $classFuel    = 'fuelTrue';
                     $fuelDisabled = '';
                     if ($tabViewData{'infoLine'}->{$family->{'id'}} eq 'isUpdated')
                    {
                        $classFuel .= ' isUpdated';
                        $startState = 'isUpdated';
                    }
                    elsif ($tabViewData{'infoLine'}->{$family->{'id'}} eq 'isError')
                    {
                        $classFuel .= ' isError';
                        $startState = 'isError';
                        # si on ne modifie pas la ligne, forcer l'enregistrement à réessayer avec les valeurs en erreur. 
                        # Evite de pouvoir enregistrer en reprenant pour cette ligne les valeurs de départ.
                        $toUpdate = 2; 
                    }
                    last;
                 }
             }

             my $tabLineData = {
                 'qtyByDay'  => $family->{'family.estimateFuel'},
                 'qtyMax'    => $family->{'family.fuelMax'},
                 'state'     => $startState
             };
             my $lineData = &LOC::Json::toJson($tabLineData);
             
             $html .= '
                 <tr class="rowBg'. ($i % 2) .' ' . $classFuel . '">
                     <td>' . $family->{'fulllabel'} . 
                             $view->displayFormInputHidden('estimateFuel[' . $family->{'id'} . '][toUpdate]', $toUpdate) . 
                             $view->displayFormInputHidden('estimateFuel[' . $family->{'id'} . '][fullLabel]', $family->{'fulllabel'}) . '
                     </td>';
                     
                     #Zone de saisie du carburant
                     my $fuelInput = $view->displayFormInputText('estimateFuel[' . $family->{'id'} . '][qty]',
                                                                 $family->{'family.estimateFuel'}, 
                                                                 LOC::Html::TEXTMODE_NUMERIC, 
                                                                 {
                                                                     'size'     => 10,
                                                                     'tabindex' => $tabIndex++,
                                                                     'disabled' => $fuelDisabled,
                                                                     'onchange' => 'setUpdatesRow(this.parentNode.parentNode, ' . $lineData . ')'
                                                                 }
                                                                );

             $html .= '<td>' . $fuelInput . '</td>';

                    # Zone de saisie de carburant max
                    my $fuelMaxInput = $view->displayFormInputText('estimateFuel[' . $family->{'id'} . '][max]',
                                                                 $family->{'family.fuelMax'}, 
                                                                 LOC::Html::TEXTMODE_NUMERIC, 
                                                                 {
                                                                     'size'     => 10,
                                                                     'tabindex' => $tabIndex++,
                                                                     'disabled' => $fuelDisabled,
                                                                     'onchange' => 'setUpdatesRow(this.parentNode.parentNode, ' . $lineData . ')'
                                                                 }
                                                                );
             $html .= '<td>' . $fuelMaxInput . '</td>';

             $html .= '
                 </tr>';

             $i++;
         }
         $html .= '
             </table>';


# Bouton Valider
my $btnValid = $view->displayTextButton(htmllocale('Valider'), 'valid(|Over).png', 'submitForm()', $locale->t('Attribuer les carburants'));


#Affichage du tableau de la liste des carburants à saisir
print $html;

#Affichage du Panel du bas
print $view->displayControlPanel({'left' => [$btnValid], 'substyle' => 'bottom'});

print $view->displayFormInputHidden('valid', '');

# Affichage du pied de page
print $view->endForm();


print $view->displayFooter();
