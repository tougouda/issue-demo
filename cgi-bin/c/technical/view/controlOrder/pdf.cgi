use utf8;
binmode STDOUT, ":raw";    # Pour envoyer le flux en l'état

use LOC::Locale;
use LOC::Browser;

our %tabViewData;

my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});

my @tabParams;

foreach my $tabMachineInfos (@{$tabViewData{'tabMachines'}})
{
    my $tabInfos = 
    {
        'ORGALIBELLE'     => $tabMachineInfos->{'organization.tabInfos'}->{'label'},
        'ORGATELEPHONE'   => $tabMachineInfos->{'organization.tabInfos'}->{'telephone'},
        'ORGATELECOPIE'   => $tabMachineInfos->{'organization.tabInfos'}->{'fax'},
        'ORGACONTACT'     => $tabMachineInfos->{'organization.tabInfos'}->{'contact'},
        'ORGAADD'         => $tabMachineInfos->{'organization.tabInfos'}->{'address'},
        'ORGACP'          => $tabMachineInfos->{'organization.tabInfos'}->{'postalCode'},
        'ORGAVILLE'       => $tabMachineInfos->{'organization.tabInfos'}->{'city'},
        'ORGAGLIBELLE'    => $tabMachineInfos->{'organization.tabInfos'}->{'agency.label'},
        'ORGAGADRESSE'    => $tabMachineInfos->{'organization.tabInfos'}->{'agency.address'},
        'ORGAGCP'         => $tabMachineInfos->{'organization.tabInfos'}->{'agency.postalCode'},
        'ORGAGVILLE'      => $tabMachineInfos->{'organization.tabInfos'}->{'agency.city'},
        'ORGAGTEL'        => $tabMachineInfos->{'organization.tabInfos'}->{'agency.telephone'},
        'ORGAGFAX'        => $tabMachineInfos->{'organization.tabInfos'}->{'agency.fax'},
        'AGLIBELLE'       => $tabMachineInfos->{'agency.tabInfos'}->{'label'},
        'AGADRESSE'       => $tabMachineInfos->{'agency.tabInfos'}->{'address'},
        'AGCP'            => $tabMachineInfos->{'agency.tabInfos'}->{'postalCode'},
        'AGVILLE'         => $tabMachineInfos->{'agency.tabInfos'}->{'city'},
        'COMMANDEAUTO'    => $tabMachineInfos->{'controlOrder.tabInfos'}->{'id'},
        'COMMDATEJOUR'    => $tabMachineInfos->{'controlOrder.tabInfos'}->{'date'},
        'COMMDATEENTREE'  => $tabMachineInfos->{'controlOrder.tabInfos'}->{'controlDate'},
        'COMMANDEMONTANT' => $tabMachineInfos->{'controlOrder.tabInfos'}->{'amount'},
        'PAMONNAIE'       => $tabViewData{'tabData'}->{'currencySymbol'},
        'MOMADESIGNATION' => $tabMachineInfos->{'model.label'},
        'MANOPARC'        => $tabMachineInfos->{'parkNumber'},
        'MANOSERIE'       => $tabMachineInfos->{'serialNumber'},
        'LIETCODE'        => substr($tabMachineInfos->{'state.id'}, -2),
        'CLSTE'           => $tabMachineInfos->{'customer.tabInfos'}->{'label'},
        'CLTEL'           => $tabMachineInfos->{'customer.tabInfos'}->{'telephone'},
        'CHLIBELLE'       => $tabMachineInfos->{'site.tabInfos'}->{'label'},
        'CHADRESSE'       => $tabMachineInfos->{'site.tabInfos'}->{'address'},
        'VICP'            => $tabMachineInfos->{'site.tabInfos'}->{'postalCode'},
        'VINOM'           => $tabMachineInfos->{'site.tabInfos'}->{'city'},
        'CHCONTACT'       => $tabMachineInfos->{'site.tabInfos'}->{'contact'},
        'CHTELEPHONE'     => $tabMachineInfos->{'site.tabInfos'}->{'telephone'},
        'CHCOMMENTAIRE'   => $tabMachineInfos->{'site.tabInfos'}->{'comments'},
        'site.isAsbestos' => $tabMachineInfos->{'site.tabInfos'}->{'isAsbestos'},
        'PENOM'           => $tabViewData{'tabData'}->{'user.name'},
        'PEPRENOM'        => $tabViewData{'tabData'}->{'user.firstName'}
    };
    push(@tabParams, $tabInfos);
}

my $tabData = {
    'tabParams' => \@tabParams,
    'oldFrameworkPath' => &LOC::Globals::get('oldFrameworkPath'),
    'logoImg'          => &LOC::Globals::getLogo($tabViewData{'tabData'}->{'countryId'}, LOC::Globals::LOGO_TYPE_RASTER),
    'env'              => &LOC::Globals::getEnv(),
    'footer'           => &LOC::Characteristic::getCountryValueByCode('PIEDIMPR', $tabViewData{'tabData'}->{'countryId'}),
    'capital'          => $locale->getCurrencyFormat(&LOC::Country::getCapital($tabViewData{'tabData'}->{'countryId'}, undef, $tabViewData{'tabData'}->{'currencySymbol'}))
};

my $params = &LOC::Json::toJson($tabData);

&LOC::Browser::sendHeaders("Content-Type: application/pdf;\n\n");

my $command = 'php ' . $tabViewData{'pdfUrl'} . ' ' . &LOC::Util::utf8Base64Encode($params);
print qx($command);