<?php
// GENERATION DU PDF CONTRAT
$tabParams = (array)json_decode(base64_decode($argv[1]), false);
mb_internal_encoding('cp1252');

$rcs = mb_convert_encoding(str_replace('<%capital>', $tabParams['capital'], $tabParams['footer']), 'cp1252', 'utf8');

$traduction = array(
    // Specimen
    "Specimen" => "sp�cimen",

    /* COMCONT */
    "COMCOdest"      => "DESTINATAIRE : ",
    "COMCOntact"     => "Contact :",
    "COMCOtel"       => "T�l :",
    "COMCOfax"       => "Fax :",
    "COMCOdatecmd"   => "Date de commande :",
    "COMCOncmd"      => "Commande N� ",
    "COMCOdatevisit" => "Date de visite : ",
    "COMCOexp"       => "EXPEDITEUR",
    "COMCOag"        => "Agence ",
    "COMCOagtel"     => "T�l: ",
    "COMCOagfax"     => "Fax: ",
    "COMCOinterloc"  => "Interlocuteur: ",
    "COMCOadrfact"   => "ADRESSE DE FACTURATION\n\nACCES INDUSTRIE\nRue Albert Einstein - CS 90201\n47400 Tonneins\nT�l: +33 5 53 88 27 98\nFax: +33 5 53 84 41 94",
    "COMCOmachine"   => "D�signation Machine: ",
    "COMCOordre"     => "Veuillez proceder � la visite p�riodique",
    "COMCOnparc"     => "N� Parc ",
    "COMCOserie"     => "N� S�rie ",
    "COMCOlieucont"  => "Lieu du Contr�le",
    "COMCOchantier"  => "Chantier: ",
    "COMCOchcontact" => "Contact: ",
    "COMCOmontant"   => "Montant: ",
    "COMCOcomment"   => "Commentaire: ",
    "COMCOinfo1"     => "Le service SAV: ",
    "COMCOinfo2"     => $rcs,
    "asbestosLabel"  => "CHANTIER AMIANTE"
);



require_once($tabParams['oldFrameworkPath'] . '/inc/fpdf_multicelltagfit.class.php');
$pdf = new FPDF_MULTICELLTAGFIT( 'P','mm','A4' );
$pdf->Open();

$tabInfos = (array)$tabParams['tabParams'];

foreach ($tabInfos as &$info)
{
    $info = (array)$info;
    foreach ($info as &$data)
    {
        $data = mb_convert_encoding($data, 'cp1252', 'utf8');
    }
}
$nb_rows = count ($tabInfos);

for( $k = 0; $k < $nb_rows; $k++)
{
    // NOUVELLE PAGE
    $pdf->AddPage();

    $pdf->SetMargins( 28, 22, 9 );
    $pdf->SetAutoPageBreak( false );
    setWatermark($pdf, $tabParams, $traduction);

    /***********************
    G�n�ration du pdf commande controle
    ************************/

    //D�finition des balises de style
    $pdf->SetStyle("a8","arial","",8,"0,0,0");
    $pdf->SetStyle("a9","arial","",9,"0,0,0");
    $pdf->SetStyle("a","arial","",10,"0,0,0");

    $pdf->SetStyle("ab","arial","B",10,"0,0,0");
    $pdf->SetStyle("ab9","arial","B",9,"0,0,0");
    $pdf->SetStyle("ab12","arial","B",12,"0,0,0");

    $pdf->SetStyle("ai8","arial","I",8,"0,0,0");

    //Block 1 (Logo + Titre)
    $bloc_x = 28;
    $bloc_y = 22;
    $pdf->Image($tabParams['logoImg'], $bloc_x, $bloc_y, 60);

    $pdf->SetXY( $bloc_x + 65, $bloc_y );
    $pdf->SetFont('Arial','B',12);
    $pdf->CellFitScale( 110, 5, $traduction['COMCOdest'].$tabInfos[$k]['ORGALIBELLE'], 0,0, "L" );
    $pdf->SetXY( $bloc_x + 65, $bloc_y + 5.5 );
    $pdf->SetFont('Arial','',9);
    $pdf->MultiCell( 80, 3, $tabInfos[$k]['ORGAADD']."\n".$tabInfos[$k]['ORGACP']." ".$tabInfos[$k]['ORGAVILLE'], 0, 1, "L" );

    $pdf->SetXY( $bloc_x + 65, $bloc_y + 12.5 );
    $pdf->MultiCellTag( 103, 3, "<ab9>".$traduction['COMCOntact']."</ab9> <a9>".$tabInfos[$k]['ORGACONTACT']."</a9>", 0, 1, "L" );

    $pdf->SetXY( $bloc_x + 65, $bloc_y + 17 );
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell( 64, 3, $traduction['COMCOtel'], 0, 0, "L" );
    $pdf->SetXY( $bloc_x + 72, $bloc_y + 17 );
    $pdf->SetFont('Arial','',9);
    $pdf->MultiCell( 50, 3, $tabInfos[$k]['ORGATELEPHONE'], 0, 1, "L" );

    $pdf->SetXY( $bloc_x + 65, $bloc_y + 21 );
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell( 71, 3, $traduction['COMCOfax'], 0, 0, "L" );
    $pdf->SetXY( $bloc_x + 72.5, $bloc_y + 21 );
    $pdf->SetFont('Arial','',9);
    $pdf->MultiCell( 50, 3, $tabInfos[$k]['ORGATELECOPIE'], 0, 1, "L" );

    $pdf->SetXY( $bloc_x + 82, $bloc_y + 25 );
    $pdf->MultiCellTag( 100, 3, "<ab>".$traduction['COMCOdatecmd']."</ab> <a9>". date('d/m/Y', strtotime($tabInfos[$k]['COMMDATEJOUR'])) ."</a9>", 0, 1, "L" );


    //Block 2 (Commande)
    $bloc_x = 28;
    $bloc_y = 56;

    $pdf->SetXY( $bloc_x , $bloc_y );
    $pdf->MultiCellTag( 152, 9, "<ab>".$traduction['COMCOncmd'].$tabInfos[$k]['COMMANDEAUTO']."</ab>", 1, "C" );

    $pdf->SetXY( $bloc_x , $bloc_y + 9);
    $date = ($tabInfos[$k]['COMMDATEENTREE'] == '0000-00-00' ? '00/00/0000' : date('d/m/Y', strtotime($tabInfos[$k]['COMMDATEENTREE'])));
    $pdf->MultiCellTag( 152, 8, "<a>".$traduction['COMCOdatevisit']. $date ."</a>", 1, "C" );


    $pdf->rect( $bloc_x, $bloc_y + 28, 76, 33 );
    $pdf->rect( $bloc_x + 76, $bloc_y + 28, 76, 33 );

    $pdf->SetFont('Arial','B',10);
    $pdf->SetXY( $bloc_x, $bloc_y + 29 );
    $pdf->CellFitScale( 75, 4.5, $traduction['COMCOexp'], 0,0, "L" );
    $pdf->SetXY( $bloc_x, $bloc_y + 36 );
    $pdf->CellFitScale(75, 2, $traduction['COMCOag'].$tabInfos[$k]['ORGAGLIBELLE'], 0 ,0, "L");
    $pdf->SetXY( $bloc_x, $bloc_y + 40 );
    $pdf->CellFitScale(75, 2, $tabInfos[$k]['ORGAGADRESSE'],0,0, "L");
    $pdf->SetXY( $bloc_x, $bloc_y + 44 );
    $pdf->CellFitScale( 75, 2, $tabInfos[$k]['AGCP']." ".$tabInfos[$k]['ORGAGVILLE'], 0,0, "L" );
    $pdf->SetXY( $bloc_x, $bloc_y + 49 );
    $pdf->CellFitScale(75, 2, $traduction['COMCOagtel'].$tabInfos[$k]['ORGAGTEL'],0,0, "L");
    $pdf->SetXY( $bloc_x, $bloc_y + 53 );
    $pdf->CellFitScale(75, 2, $traduction['COMCOagfax'].$tabInfos[$k]['ORGAGFAX'],0,0 ,"L");
    $pdf->SetXY( $bloc_x, $bloc_y + 58 );
    $pdf->CellFitScale(75, 2, $traduction['COMCOinterloc'].$tabInfos[$k]['PENOM']." ".$tabInfos[$k]['PEPRENOM'], 0,0, "L");
    $pdf->SetXY( $bloc_x + 77, $bloc_y + 29 );
    $pdf->MultiCell( 75, 4, $traduction['COMCOadrfact'], 0, "L" );

    $pdf->rect( $bloc_x, $bloc_y + 68, 152, 23 );
    $pdf->SetXY( $bloc_x + 40, $bloc_y + 69 );
    $pdf->SetFont('Arial','B',12);
    $pdf->Cell( 80, 4.5, $traduction['COMCOordre'], 0, "L" );

    $pdf->SetXY( $bloc_x, $bloc_y + 79 );
    $pdf->CellFitScale( 152, 4.5, $traduction['COMCOmachine'].$tabInfos[$k]['MOMADESIGNATION'], 0, 0, "C" );

    $pdf->SetXY( $bloc_x + 3, $bloc_y + 83.5 );
    $pdf->MultiCellTag( 70, 6, "<ab12>".$traduction['COMCOnparc'].$tabInfos[$k]['MANOPARC']."</ab12>",0 , "L" );

    $pdf->SetXY( $bloc_x + 80, $bloc_y + 83.5 );
    $pdf->MultiCellTag( 70, 4.5, "<ab12>".$traduction['COMCOserie'].$tabInfos[$k]['MANOSERIE']."</ab12>",0 , "L" );

    $pdf->SetXY( $bloc_x, $bloc_y + 95 );
    $pdf->Cell( 152, 8, $traduction['COMCOlieucont'], 1, 0, "C" );
    $pdf->rect( $bloc_x, $bloc_y + 103, 76, 33 );
    $pdf->rect( $bloc_x + 76, $bloc_y + 103, 76, 33 );
    $pdf->SetXY( $bloc_x + 2, $bloc_y + 105 );

    $pdf->MultiCellTag( 60, 4, "<ab12>".$traduction['COMCOag'].$tabInfos[$k]['AGLIBELLE']."</ab12>", 0, 0, "L" );
    $pdf->SetXY( $bloc_x + 2, $bloc_y + 110 );
    $pdf->SetFont('Arial','',7);
    $pdf->MultiCell( 60, 3, $tabInfos[$k]['AGADRESSE']."\n".$tabInfos[$k]['AGCP']." ".$tabInfos[$k]['AGVILLE'], 0, "L" );
    $pdf->SetXY( $bloc_x + 2, $bloc_y + 118 );
    $pdf->MultiCellTag( 60, 4, "<ab>".$traduction['COMCOinterloc'].$tabInfos[$k]['PENOM']." ".$tabInfos[$k]['PEPRENOM']."</ab>", 0, 1, "L" );

    $pdf->SetXY( $bloc_x + 78, $bloc_y + 104 );
    $pdf->SetFont('Arial','',9);
    $pdf->Cell( 73, 3, substr($tabInfos[$k]['CLSTE']." ".$tabInfos[$k]['CLTEL'], 0, 36), 0, 0, "L" );
    $pdf->SetXY( $bloc_x + 78, $bloc_y + 107.5 );
    $pdf->SetFont('Arial','B',10);
    if($tabInfos[$k]['LIETCODE'] == 1 || $tabInfos[$k]['LIETCODE'] == 3)
    	$pdf->Cell( 60, 3, $traduction['COMCOchantier'], 0, 1, "L" );
    else
    	$pdf->Cell( 60, 3, "", 0, 1, "L" );

    $tabTxt = explode("\n", $tabInfos[$k]['CHADRESSE']);
    if(count($tabTxt) > 2)
    {
        $tabInfos[$k]['CHADRESSE'] = $tabTxt[0]."\n".$tabTxt[1];
    }

    $pdf->SetXY( $bloc_x + 78, $bloc_y + 111 );
    $pdf->SetFont('Arial','',9);
    $pdf->MultiCellTag( 74, 3, "<a9>".$tabInfos[$k]['CHLIBELLE']."</a9>\n<a8>".$tabInfos[$k]['CHADRESSE']."\n".$tabInfos[$k]['VICP']." ".$tabInfos[$k]['VINOM']."</a8>", 0, 1, "L" );

    //modifs****************************************************************************


    $pdf->SetXY( $bloc_x + 78, $bloc_y + 123 );
    $pdf->SetFont('Arial','B',10);
    if($tabInfos[$k]['LIETCODE'] == 1 || $tabInfos[$k]['LIETCODE'] == 3)
    {
        $pdf->Cell( 73, 3, $traduction['COMCOchcontact']." ".$tabInfos[$k]['CHCONTACT'], 0, 1, "L" );
    }
    else
    {
        $pdf->Cell( 73, 3, "" , 0, 1, "L" );
    }
    $pdf->SetXY( $bloc_x + 93, $bloc_y + 126 );
    $pdf->SetFont('Arial','',9);
    $pdf->MultiCell( 56, 3, $tabInfos[$k]['CHTELEPHONE'], 0, 1, "L" );

    if ($tabInfos[$k]['site.isAsbestos'])
    {
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetXY( $bloc_x + 78, $bloc_y + 132 );
        $pdf->Cell(73, 3, $traduction['asbestosLabel'], 0, 1, "L");
    }

    $pdf->SetFont('Arial', '', 9);

    //Block 3 (Montant + commentaires)

    $montant = $tabInfos[$k]['COMMANDEMONTANT'];
    round($montant, 2);


    $bloc_x = 28;
    $bloc_y = 197;

    $pdf->rect( $bloc_x, $bloc_y, 152, 17 );
    $pdf->SetXY( $bloc_x + 21, $bloc_y + 4 );
    $monnaie = $tabInfos[$k]['PAMONNAIE'];
    $pdf->MultiCellTag( 80, 4, "<ab12>".$traduction['COMCOmontant'].mynumber_format($montant)." " . $monnaie . "</ab12>", 0, 1, "L" );

    $pdf->rect( $bloc_x, $bloc_y + 30, 152, 18 );
    $pdf->SetXY( $bloc_x, $bloc_y + 31 );
    $pdf->SetFont('Arial','',10);
    $pdf->MultiCellTag( 80, 4, "<a>".$traduction['COMCOcomment']." ".$tabInfos[$k]['CHCOMMENTAIRE']."</a>", 0, 1, "L" );

    $pdf->SetXY( $bloc_x + 75, $bloc_y + 61 );
    $pdf->SetFont('Arial','',10);
    $pdf->Cell( 80, 4, $traduction['COMCOinfo1'], 0, 0, "L" );

    //Block  (Acces indutrie)
    $bloc_x = 10;
    $bloc_y = 273;

    $pdf->SetXY( $bloc_x + 10, $bloc_y );
    $pdf->SetFont('Arial','',7);
    $pdf->CellFitScale( 172, 3, str_replace( "%capital_acces%", mb_convert_encoding($tabParams['capital'], 'cp1252', 'utf8'), $traduction['COMCOinfo2'] ), 0, "C" );


}

$pdf->Output( "document.pdf", "I" );


function setWatermark(&$pdf, $tabParams, $traduction)
{
    if ($tabParams['env'] != 'exp')
    {
        $pdf->SetFont('Arial', 'BI', 72);

        $textWatermark = mb_strtoupper ($traduction['Specimen']);
        $xWatermark = $pdf->w / 2;
        $yWatermark = $pdf->h / 2;
        $rWatermark = 30;

        $rWatermark *= M_PI / 180;
        $cosWatermark = cos($rWatermark);
        $sinWatermark = sin($rWatermark);
        $cx = $xWatermark * $pdf->k;
        $cy = ($pdf->h - $yWatermark) * $pdf->k;
        $cx = 0;
        $cy = 0;

        $pdf->_out(
        sprintf('q %.3f g %.5f %.5f %.5f %.5f %.2f %.2f cm',
        203 / 255,
        $cosWatermark,
        $sinWatermark,
        -$sinWatermark,
        $cosWatermark,
        ($pdf->w / 2 - $pdf->GetStringWidth($textWatermark) / 2 * cos($rWatermark)) * $pdf->k,
        $pdf->h / 2 * $pdf->k - $pdf->GetStringWidth($textWatermark) / 2 * sin($rWatermark) * $pdf->k
        )
        );
        $pdf->_out(
        sprintf('BT (%s) Tj ET',
        $pdf->_escape($textWatermark)
        )
        );

        $pdf->_out('Q');
    }
}

function mynumber_format( $num )
{
    return number_format( round( $num, 2 ), 2, ',', ' ' );
}

?>
