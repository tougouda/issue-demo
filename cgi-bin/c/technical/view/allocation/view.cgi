use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;
use LOC::Json;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'technical/allocation/';


our %tabViewData;
our $inputsTabIndex = 1;


our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


# URL courante 
my $currentUrl = $view->createURL('technical:allocation:view');


# Types d'erreurs
my $tabErrorTypes = [
    {
        'code' => 'noMachine',
        'title' => $locale->t('Pas de machine disponible')
    },
    {
        'code' => 'proformaNotGen',
        'title' => $locale->t('Pro forma non générée')
    },
    {
        'code' => 'proformaNotUpToDate',
        'title' => $locale->t('Pro forma non à jour')
    },
    {
        'code' => 'proformaNotPayed',
        'title' => $locale->t('Pro forma non finalisée')
    },
    {
        'code' => 'rentTariff',
        'title' => $locale->t('Remise exceptionnelle de location en attente')
    },
    {
        'code' => 'transportTariff',
        'title' => $locale->t('Remise exceptionnelle de transport en attente')
    },
    {
        'code' => 'customerNotVerified',
        'title' => $locale->t('Client avec ouverture de compte en attente')
    }
];




# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'view.css');

# Scripts JS
# JS
$view->addJSSrc($dir . 'view.js');
# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc('jQuery/plugins/tablesorter.js');


$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);


my $title = $locale->t('Attribution des machines');
$view->setPageTitle($title);
$view->setTitle($title);


# Bouton "Actualiser"
my $msg = $locale->t('Voulez-vous vraiment actualiser sans enregistrer les attributions en cours ?');
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png',
                                          'if (!hasMachineAlloc() || confirm("' . $view->toJSEntities($msg) . '")) { window.location.href = "' . $currentUrl . '"} ;',
                                          $locale->t('Actualiser la page'));
$view->addControlsContent([$refreshBtn], 'right', 1);

my $errorBlockRefreshBtn = '<span onclick="$ge(\'errorBlock\').style.display = \'none\';">' . $refreshBtn . '</span>';
my $fatalErrorMsg = htmllocale('Erreur fatale !') .
    '<p class="details">' .
    $locale->t('Veuillez %s la liste et renouveler l\'opération.', $errorBlockRefreshBtn) . '<br>' .
    htmllocale('Si l\'erreur se reproduit, merci de contacter le support informatique.') .
    '</p>';

# Messages de retour
my $tabTranslations = {
  'updated'              => $locale->t('Les modifications ont été enregistrées'),
  'fatalError'           => $fatalErrorMsg,
  'writeLocked'          => $locale->t('Le contrat est bloqué en écriture pour maintenance, veuillez recommencer ultérieurement...'),
  'rentTariffError'      => $locale->t('Remise exceptionnelle de location en attente'),
  'transportTariffError' => $locale->t('Remise exceptionnelle de transport en attente'),
  'customerNotVerified'  => $locale->t('Client avec ouverture de compte en attente'),
  'proformaNotGen'       => $locale->t('Pro forma non générée'),
  'proformaNotUpToDate'  => $locale->t('Pro forma non à jour'),
  'proformaNotPayed'     => $locale->t('Pro forma non finalisée'),
  'machineUnavailable'   => $locale->t('La machine n\'est pas disponible'),
  'machineAllocated'     => $locale->t('Une machine est déjà attribuée sur le contrat'),
  'modelNotCorrect'      => $locale->t('Le modèle de la machine ne correspond pas à celui demandé sur le contrat'),
  'contractStateChange'  => $locale->t('L\'état du contrat a changé'),
  'rights'               => $locale->t('Certaines informations ne sont pas modifiables')
};

# Message de confirmation de validation avec succès
if ($tabViewData{'messages'}->{'valid'})
{
    my $validBlock = '
    <div id="validBlock" class="messageBlock valid" >
        <div class="popup" style="display: block;">
            <div class="content">
                <table>
                <tr>
                    <td style="font-weight: bold;">' . $tabTranslations->{$tabViewData{'messages'}->{'valid'}} . '.</td>
                </tr>
                <tr class="buttons">
                    <td>' .
                    $view->displayTextButton($locale->t('Ok'), '',
                                             '$ge("validBlock").style.display = "none";',
                                             '', 'l', {'id' => 'validBlockBtn'}) .
                    '</td>
                </tr>
                </table>
            </div>
        </div>
    </div>';
    
    $view->addBodyContent($validBlock, 0, 1);
    $view->addBodyEvent('load', '$("#validBlockBtn").focus();');
}

#Message d'erreur sur une ou plusieurs lignes
my $errorBlock = '
<div id="errorBlock" class="messageBlock error"'; 
($tabViewData{'messages'}->{'error'}) ?  $errorBlock .= 'style="display: visible;"' : $errorBlock .= 'style="display: none;"';
$errorBlock .= '>
    <div class="popup" style="display: block;">
        <div class="content">
            <table>
                <tbody id="errorsTable">
                    <tr>
                        <td>
                            <p style="font-weight : bold;">' . $locale->t('Des erreurs ont été détectées') . '</p>
                            <ul>';
                            #Erreurs retournées depuis PERL
                            while (my ($line, $type) = each(%{$tabViewData{'messagesLine'}}))
                            {
                                $errorBlock .= '<li>' . $locale->t('Contrat') . ' ' . $type->{'code'} . ' : ' . $tabTranslations->{$type->{'error'}} . '</li>';
                            }
                            $errorBlock .= '</ul>';
                            
                            $errorBlock .= '
                        </td>
                    </tr>
                    <tr class="buttons" id="buttonRow">
                        <td>' .
                        $view->displayTextButton($locale->t('Fermer'), '', '$ge("errorBlock").style.display = "none";') .
                        '</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>';

$view->addBodyContent($errorBlock, 0, 1);


# Légende
my $legendHtml = '<div class="legend">' . 
                    '<span class="title">' . htmllocale('Légende pour les cas erronés') . '&nbsp;:</span>';
foreach my $tabErrorTypeInfos (@$tabErrorTypes)
{
    $legendHtml .= '<div class="button ' . $tabErrorTypeInfos->{'code'} . '">' .
                        '<div class="errorImg ' . $tabErrorTypeInfos->{'code'} . '"></div>' .
                        '<div class="text"><div class="pick"></div>' . $view->toHTMLEntities($tabErrorTypeInfos->{'title'}) . '</div>' .
                   '</div>';
}
$legendHtml .= '</div>';

$view->addControlsContent([$legendHtml], 'left', 1);

## Bouton "Actualiser"
#my $msg = $locale->t('Voulez-vous vraiment actualiser sans enregistrer les attributions en cours ?');
#my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png',
#                                          'if (!hasMachineAlloc() || confirm("' . $view->toJSEntities($msg) . '")) { window.location.href = "' . $currentUrl . '"} ;',
#                                          $locale->t('Actualiser la page'));
#$view->addControlsContent([$refreshBtn], 'right', 1);


# Affichage de l'entête
print $view->displayHeader();


# On n'affiche pas le tableau si il est vide
my @tabContracts = values %{$tabViewData{'tabContracts'}};
if (@tabContracts == 0)
{
    # Pas de résultat
    print '<div class="noResult">' . $view->displayMessages('info', [$locale->t('Aucun contrat ne nécessite une attribution de machine')], 0) . '</div>';
}
else
{
    # Options pour tablesorter
    my $headersOption = '
1 : {sorter: "textAttr"},
10 : {sorter: false},
11 : {sorter: false}'
;
    my $sortListOption = '[[1,0]]'; # Tri par défaut: "Date de début" (ASC)

    print '
<form action="' . $currentUrl . '" method="post">

<table class="basic allocate">
<thead>
    <tr>
        <th class="code">' . htmllocale('N° Contrat') . '</th>
        <th class="beginDate">' . htmllocale('Date de début') . '</th>
        <th class="duration"><span title="' . htmllocale('Durée de location en jours ouvrés') . '">' . htmllocale('Durée (j.)') . '</span></th>
        <th class="customer">' . htmllocale('Client') . '</th>
        <th class="activity">' . htmllocale('Activité') . '</th>
        <th class="site">' . htmllocale('Chantier') . '</th>
        <th class="anticipateDelivery"><span title="' . htmllocale('Livraison anticipée possible') . '">' . htmllocale('Liv. anticipée poss.') . '</span></th>
        <th class="model">' . htmllocale('Désignation') . '</th>
        <th class="imperative"><span title="' . htmllocale('Modèle impératif') . '">' . htmllocale('Mod. imp.') . '</span></th>
        <th class="asbestos"><span title="' . htmllocale('Présence d\'amiante sur le chantier') . '">' . htmllocale('Amiante') . '</span></th>
        <th class="info"></th>
        <th class="allocate">' . htmllocale('N° Parc') . '</th>
    </tr>
</thead>
<tbody>';



    # Génération des icônes pour les lignes
    my $errorIcons = '';
    foreach my $tabErrorTypeInfos (@$tabErrorTypes)
    {
        $errorIcons .= '<div title="' . $view->toHTMLEntities($tabErrorTypeInfos->{'title'}) . '" class="errorImg ' . $tabErrorTypeInfos->{'code'} . '"></div>';
    }

    # Affichage des lignes
    my $tabConfigs = [];
    foreach my $tabContractInfos (@tabContracts)
    {
        my $errorsCount = @{$tabContractInfos->{'tabErrors'}};

        my $contractId = $tabContractInfos->{'id'};

        # URL's
        my $contractUrl = $view->createURL('rent:rentContract:view', {'contractId' => $contractId});


        my $className = '';
        my $machinesList = '';
        my $wishBlock = '';


        my $isWishedExists = 0;
        if ($errorsCount == 0)
        {
            my $tabContractConfigs = {
                'id'           => $contractId,
                'machineId'    => 0,
                'tabMachines'  => []
            };

            # Génération de la liste des machines
            foreach my $tabMachineInfos (values %{$tabContractInfos->{'tabAvailableMachines'}})
            {
                my $score = '*' x $tabMachineInfos->{'control.score'};

                my $isWished = 0;
                if ($tabContractInfos->{'wishedMachine.id'} == $tabMachineInfos->{'id'})
                {
                    $isWished = 1;
                    $isWishedExists = 1;
                }

                # Visibilité de la machine
                my $isVisibleOnAgency = ($tabMachineInfos->{'visibleOnAgency.id'} ? 1 : 0);

                push(@{$tabContractConfigs->{'tabMachines'}}, {
                    'id'        => $tabMachineInfos->{'id'},
                    'label'     => $tabMachineInfos->{'parkNumber'} . 
                                   ($isVisibleOnAgency ? '@' . $tabMachineInfos->{'agency.id'} : '')  . ' ' . 
                                   $score . ($isWished ? ' ♥' : ''),
                    'isActived' => 1,
                    'isWished'  => $isWished
                });
            }

            $machinesList  = '<div class="parkNumber none">';
            $machinesList .= '<div class="button" onclick="setMachineId(' . $contractId . ', 0);"></div>' .
                                $view->displayFormSelect('allocate[' . $contractId . '][machineId]', [],
                                                         0, 0, {
                                                                    'id' => 'machineList_' . $contractId,
                                                                   'onchange'  => 'setMachineId(' . $contractId . ', this.value);',
                                                                   'tabindex' => $inputsTabIndex++
                                                               }) .
                             '</div>';

            $className = 'ok';

            push(@$tabConfigs, $tabContractConfigs);

            if ($tabViewData{'infoLine'}->{$contractId} eq 'isError')
            {
                $className .= ' isError';
            }
        }
        else
        {
            $className = 'ko ' . join(' ', @{$tabContractInfos->{'tabErrors'}});
        }

        # Machine souhaitée
        if ($tabContractInfos->{'wishedMachine.id'})
        {
            $wishBlock = '<div class="wished" onclick="displayWishedMachineTooltip(this, 1500);">' .
                                '<div class="tip">' .
                                    '<div title="' . htmllocale('Machine souhaitée') . '" class="icon"' . ($isWishedExists ? ' onclick="setMachineId(' . $contractId . ', ' . $tabContractInfos->{'wishedMachine.id'} . ');"' : '') . '></div>' .
                                    $view->displayParkNumber($tabContractInfos->{'wishedMachine.tabInfos'}->{'parkNumber'}) .
                                '</div>' .
                             '</div>';
        }

        my $inputContractCode = $view->displayFormInputHidden('allocate[' . $contractId . '][contractCode]', $tabContractInfos->{'code'});

        print '
    <tr class="' . $className . '">
        <td class="code"><a href="' . $contractUrl . '" target="_blank">' . $tabContractInfos->{'code'} . '</a>' . $inputContractCode . '</td>
        <td class="beginDate" data-val="' . $tabContractInfos->{'beginDate'} . '">' . $locale->getDateFormat($tabContractInfos->{'beginDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC) . '</td>
        <td class="duration">' . $tabContractInfos->{'duration'} . '</td>
        <td class="customer"><a href="' . $tabContractInfos->{'customer.url'} . '" target="_blank">' . $tabContractInfos->{'customer.name'} . '</a></td>
        <td class="activity">' . $tabContractInfos->{'customer.activity'} . '</td>
        <td class="site">' . $tabContractInfos->{'site.tabInfos'}->{'locality.postalCode'} . ' ' . $tabContractInfos->{'site.tabInfos'}->{'locality.name'} . '</td>
        <td class="anticipateDelivery">' . $view->displayBoolean($tabContractInfos->{'site.tabInfos'}->{'isAnticipated'}) . '</td>
        <td class="model">' . $tabContractInfos->{'model.tabInfos'}->{'label'} . '</td>
        <td class="imperative">' . $view->displayBoolean($tabContractInfos->{'isImperativeModel'}) . '</td>
        <td class="asbestos">' . $view->displayBoolean($tabContractInfos->{'site.tabInfos'}->{'isAsbestos'}) . '</td>
        <td class="info">' . $wishBlock . '</td>
        <td class="allocate">' . $machinesList . $errorIcons . '</td>
    </tr>';

    }


    # Hidden
    print $view->displayFormInputHidden('valid', '');

    print '
</tbody>
</table>

</form>';

    # Tri du tableau
    print $view->displayJSBlock('
$.tablesorter.addParser({
    id: "textAttr",
    is: function(s) { return false; },
    format: function(s, table, cell, cellIndex) { return $(cell).attr("data-val"); },
    type: "text"
});
$.tablesorter.addParser({
    id: "integerAttr",
    is: function(s) { return false; },
    format: function(s, table, cell, cellIndex) { return $(cell).attr("data-val"); },
    type: "numeric"
});

$(function() 
{ 
    $("table.basic.allocate").tablesorter({
        headers: {' . $headersOption . '},
        sortList: ' . $sortListOption . ',
        widgets: ["zebra"]
    });
}
);');


    # Initialisation des listes des numéros de parc
    print $view->displayJSBlock('
var tabMachineAllocCfgs = ' . &LOC::Json::toJson($tabConfigs) . ';
displayMachineAlloc();');


    # Bouton Valider
    my $validBtn = $view->displayTextButton($locale->t('Valider'), 'valid(|Over).png',
                                                       'submitForm();',
                                                       $locale->t('Attribuer les machines'));

    #Affichage du Panel du bas
    print $view->displayControlPanel({'left' => [$validBtn], 'substyle' => 'middle'});
}



# Affichage du pied de page
print $view->displayFooter();
