use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;
use LOC::Json;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'technical/endingContracts/';


our %tabViewData;
our $inputsTabIndex = 1;


our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}

my $actionName = $tabViewData{'actionName'};
my $agencyId   = $tabViewData{'user.agency.id'};

# URL courante
my $currentUrl = $view->createURL('technical:endingContracts:view' . ucfirst($actionName));


# Notifications possibles
$view->addNotifications([
    {
        'id'      => 'max-considers-reached',
        'type'    => 'info',
        'message' => $locale->tn(
                        'Vous ne pouvez pas faire plusieurs prises en compte à la fois',
                        'Vous ne pouvez pas faire plus de %d prises en compte à la fois',
                        $tabViewData{'nbConsidersMax'},
                        $tabViewData{'nbConsidersMax'})
    }
]);


# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'view.css');

# Scripts JS
# JS
$view->addJSSrc($dir . 'view.js');
# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc('jQuery/plugins/tablesorter.js');

# Constantes pour le type de prise en compte
$view->addJSSrc('
var CONSIDERTYPE_NOKIMOCE = "' . LOC::Transport::CONSIDERTYPE_NOKIMOCE . '";
var CONSIDERTYPE_KIMOCE = "' . LOC::Transport::CONSIDERTYPE_KIMOCE . '";
');

$view->addPackage('modalwindow')
     ->addPackage('spincontrols');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS |
                  LOC::Html::Standard::DISPFLG_NOTIFS);


my $title;
my $helpMessage = '';
if ($actionName eq 'transfers')
{
    $title = $locale->t('Transferts inter-chantiers / Reprises sur chantier');
    $helpMessage = $locale->t('Liste des transferts inter-chantiers et des reprises sur chantier en cours ou venant d\'être réalisés.');
}
else
{
    $title = $locale->t('Récupérations / Restitutions');
    $helpMessage = $locale->t('Liste des récupérations et des restitutions agence en cours ou venant d\'être réalisées.');
}
$view->setPageTitle($title);
$view->setTitle($title);

# Nombre de transports à prendre en compte
my @tabTransports = values %{$tabViewData{'tabTransports'}};

# Connexion à Kimoce ?
if (!$tabViewData{'isKimoceCallable'} && @tabTransports > 0)
{
    my $msg = $locale->t('Vous ne pouvez pas mettre à jour le compteur horaire ou générer une demande d\'entretien car Kimoce n\'est pas disponible.');

    # Notifications possibles
    $view->addNotifications([
        {
            'id'      => 'kimoce-inactive',
            'type'    => 'info',
            'message' => $msg
        }
    ]);

    $view->addBodyEvent('load', 'Location.viewObj.displayNotification("kimoce-inactive", "show", {});');
}


# Bouton "Actualiser"
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png',
                                          'window.location.href = "' . $currentUrl . '";',
                                          $locale->t('Actualiser la page'));
$view->addControlsContent([$refreshBtn], 'right', 1);

# Message d'erreur sur une ou plusieurs lignes
my @tabErrorTspIds = ();
if ($tabViewData{'return'})
{
    # Message de succès
    my $nbSuccess = @{$tabViewData{'return'}->{'success'}};
    if ($nbSuccess > 0)
    {
        my @tabParkNumbers = ();
        foreach my $action (@{$tabViewData{'return'}->{'success'}})
        {
            push(@tabParkNumbers, $view->displayParkNumber($action->{'parkNumber'}));
        }

        # Message de succès
        my $msg;
        if ($actionName eq 'transfers')
        {
            $msg = $locale->tn(
                       'Le transfert inter-chantiers / reprise sur chantier de la machine %s a été marqué comme pris en compte par l\'atelier.',
                       'Les transferts inter-chantiers / reprises sur chantier des machines %s ont été marqués comme pris en compte par l\'atelier.',
                       $nbSuccess, join(', ', @tabParkNumbers));
        }
        else
        {
            $msg = $locale->tn(
                       'La récupération / restitution de la machine %s a été marquée comme prise en compte par l\'atelier.',
                       'Les récupérations / restitutions des machines %s ont été marquées comme prises en compte par l\'atelier.',
                       $nbSuccess, join(', ', @tabParkNumbers));
        }

        # Notifications possibles
        $view->addNotifications([
            {
                'id'      => 'valid-consider',
                'type'    => 'info',
                'message' => $msg
            }
        ]);

        $view->addBodyEvent('load', 'Location.viewObj.displayNotification("valid-consider", "show", {});');
    }

    # Message d'erreur
    my $nbErrors = @{$tabViewData{'return'}->{'errors'}};
    if ($nbErrors > 0)
    {
        my $errorBlockRefreshBtn = '<span onclick="$ge(\'errorBlock\').style.display = \'none\';">' . $refreshBtn . '</span>';
        my $fatalErrorMsg = htmllocale('Erreur fatale !') .
            '<p class="details">' .
            $locale->t('Veuillez %s la liste et renouveler l\'opération.', $errorBlockRefreshBtn) . '<br>' .
            htmllocale('Si l\'erreur se reproduit, merci de contacter le support informatique.') .
            '</p>';

        # Messages de retour
        my $tabTranslations = {
            'fatalError'  => $fatalErrorMsg,
            'rightsError' => $locale->t('Certaines informations ne sont pas modifiables'),
            'alreadyDone' => $locale->t('Transport déjà pris en compte')
        };

        # Message d'erreur
        my $msg;
        if ($actionName eq 'transfers')
        {
            $msg = $locale->tn(
                       'La prise en compte du transfert inter-chantiers / reprise sur chantier de la machine suivante n\'a pas été enregistrée',
                       'Les prises en compte des transferts inter-chantiers / reprises sur chantier des machines suivantes n\'ont pas été enregistrées',
                       $nbErrors);
        }
        else
        {
            $msg = $locale->tn(
                       'La prise en compte de la récupération / restitution agence de la machine suivante n\'a pas été enregistrée',
                       'Les prises en compte des récupérations / restitutions agence des machines suivantes n\'ont pas été enregistrées',
                       $nbErrors);
        }

        my $errorBlock = '
<div id="errorBlock" class="messageBlock error">
    <div class="popup" style="display: block;">
        <div class="content">
            <table>
                <tbody id="errorsTable">
                    <tr>
                        <td>
                            <p style="font-weight : bold;">' . $msg . ' :</p>
                            <ul>';
        # Erreurs retournées depuis PERL
        foreach my $action (@{$tabViewData{'return'}->{'errors'}})
        {
            push(@tabErrorTspIds, $action->{'id'});

            $errorBlock .= '<li>' . $view->displayParkNumber($action->{'parkNumber'}) .
                $tabTranslations->{$action->{'error'}} .
            '</li>';
        }

        $errorBlock .= '
                            </ul>
                        </td>
                    </tr>
                    <tr class="buttons" id="buttonRow">
                        <td>' .
                        $view->displayTextButton($locale->t('Fermer'), '', '$ge("errorBlock").style.display = "none";') .
                        '</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>';

        $view->addBodyContent($errorBlock, 0, 1);
    }

    # Message d'information
    my $nbInfos = @{$tabViewData{'return'}->{'infos'}};
    if ($nbInfos > 0)
    {
        my $msg = $locale->t('Les mises à jour Kimoce n\'ont pas eu lieu car celui-ci n\'est pas disponible.');

        # Notifications possibles
        $view->addNotifications([
            {
                'id'      => 'kimoce-unsaved',
                'type'    => 'info',
                'message' => $msg
            }
        ]);

        foreach my $codeInfo (@{$tabViewData{'return'}->{'infos'}})
        {
            $view->addBodyEvent('load', 'Location.viewObj.displayNotification("' . $codeInfo . '", "show", {});');
        }
    }
}


# Affichage de l'entête
print $view->displayHeader();


# Message d'attente lors de la validation
print '
<div class="loadingMessage">
    <span>' . $locale->t('Le traitement peut prendre quelques instants...') . '</span>
</div>';

# On n'affiche pas le tableau si il est vide
if (@tabTransports == 0)
{
    # Pas de résultat
    if ($actionName eq 'transfers')
    {
        print '<div class="noResult">' . $view->displayMessages('info', [$locale->t('Tous les transferts inter-chantiers et les reprises sur chantier sont pris en compte')], 0) . '</div>';
    }
    else
    {
        print '<div class="noResult">' . $view->displayMessages('info', [$locale->t('Toutes les récupérations et les restitutions agence sont prises en compte')], 0) . '</div>';
    }
}
else
{
    # Options pour tablesorter
    my $headersOption = '
{sorter: "textAttr"},
{sorter: "textAttr"},
{sorter: "textAttr"},
{sorter: "integerAttr"},';
    if ($actionName eq 'recoveries')
    {
        $headersOption .= '
{sorter: "textAttr"},';
    }
    $headersOption .= '
{sorter: "text"},';
    if ($actionName eq 'transfers')
    {
        $headersOption .= '
{sorter: false},
{sorter: "text"},';
    }
    $headersOption .= '
{sorter: "text"},';
    if ($actionName eq 'recoveries')
    {
        $headersOption .= '
{sorter: false},';
    }
    $headersOption .= '
{sorter: "integerAttr"},';
    $headersOption .= '
{sorter: "text"},
{sorter: "textAttr"},
{sorter: "textAttr"},
{sorter: "integerAttr"},
{sorter: false},
{sorter: false}';

    my $sortListOption = '[]';
    if ($actionName eq 'recoveries')
    {
        $sortListOption = '[[4,0]]'; # Tri par défaut: "Date d'arrêt" (ASC)
    }



    # Bouton de réalisation
    my $checkedNo = ' <span id="do-checked"></span>';
    my $doBtn = $view->displayTextButton($locale->t('Prendre en compte') . $checkedNo, 'valid(|Over).png',
                                         'return true;',
                                         '',
                                         'l',
                                         {'class' => 'locCtrlButton disabled', 'id' => 'do-btn'});

    # Bouton d'affichage de tous les transports
    my $viewModeFullLabel = $locale->t('Affichage : %s', '<span class="viewmode-label full">' . $locale->t('toutes les lignes') . '</span><span class="viewmode-label checked">' . $locale->t('lignes modifiées') . '</span>');
    my $viewModeFullBtn = $view->displayTextButton($viewModeFullLabel,
                                                   $dir . 'viewall.png',
                                                   'return true;',
                                                   '',
                                                   'l',
                                                   {'id'    => 'viewmode-btn',
                                                    'class' => 'locCtrlButton'});

    print $view->startForm();

    my $titleRecapKim = '';
    my $titleRecapNoKim = '';
    if ($actionName eq 'transfers')
    {
        $titleRecapKim   = htmllocale('Pris en compte avec génération d\'une demande d\'intervention');
        $titleRecapNoKim = htmllocale('Pris en compte sans génération de demande d\'intervention');
    }
    if ($actionName eq 'recoveries')
    {
        $titleRecapKim   = htmllocale('Pris en compte avec génération d\'une demande d\'entretien');
        $titleRecapNoKim = htmllocale('Pris en compte sans génération de demande d\'entretien');
    }

    print '
<div id="table-wrapper" class="list viewmode-full">
    <div id="table-toolbar-container" class="toolbar header">
        <div class="col content">
            <div>' . $view->displayMessages('help', [$helpMessage], 0) . '</div>
            <div class="fixed-content">
                <div id="header-filters-recap"></div>
            </div>
        </div>
        <div class="col controls">
            <ul>
                <li>' . $viewModeFullBtn . '</li>
            </ul>
            <ul>
                <li> ' . $doBtn . '</li>
            </ul>
            <div class="recap kim" id="header-updated-recap-kim" title="' . $titleRecapKim . '"></div>
            <div class="recap nokim" id="header-updated-recap-nokim" title="' . $titleRecapNoKim . '"></div>
            <div class="fixed-content">
                <a href="#table-filters-container" id="back-top-btn" title="' . htmllocale('Retourner en haut de la page') . '">' .
                    $view->displayImage($dir . 'up.png') .
               '</a>
            </div>
        </div>
    </div>
    <div class="content" id="table-container">

        <table class="basic list" id="table-main">
        <thead>
            <tr>
                <th class="type">' . htmllocale('Type') . '</th>
                <th class="code">' . htmllocale('N° Contrat') . '</th>
                <th class="period">' . htmllocale('Période de location') . '</th>
                <th class="duration"><span title="' . htmllocale('Durée de location') . ' (' . htmllocale('en jours ouvrés') . ')' . '">' . htmllocale('Durée (j.)') . '</span></th>';

    if ($actionName eq 'recoveries')
    {
        print '
                <th class="stopDate">' . htmllocale('Date d\'arrêt') . '</th>';
    }

    print '
                <th class="from">' . htmllocale('Provenance') . '</th>';

    if ($actionName eq 'transfers')
    {
        print '
                <th class="arrow"></th>
                <th class="to">' . htmllocale('Destination') . '</th>';
    }
    print '
                <th class="asbestos"><span title="' . htmllocale('Présence d\'amiante sur le chantier') . '">' . htmllocale('Amiante') . '</span></th>
                <th class="parkNumber">' . htmllocale('N° parc.') . '</th>';
    if ($actionName eq 'recoveries')
    {
        print '
                <th class="agency"></th>';
    }
    print '
                <th class="model">' . htmllocale('Modèle de machines') . '</th>
                <th class="customer">' . htmllocale('Client') . '</th>
                <th class="recovery" title="' . htmllocale('Date de récupération') . '">' . htmllocale('Date récup.') . '</th>
                <th class="timeCounterRead" title="' . htmllocale('Compteur horaire relevé') . '">' . htmllocale('Cpt. hor. relevé') . '</th>
                <th class="timeCounter" title="' . htmllocale('Compteur horaire Kimoce') . '">' . htmllocale('Cpt. hor. Kimoce') . '</th>
                <th class="consider"></th>
            </tr>
        </thead>
        <tbody>';

    # Affichage des lignes
    my @tabList = ();
    foreach my $tabTsp (@tabTransports)
    {
        my $typeLabel;
        if ($actionName eq 'transfers')
        {
            $typeLabel = ($tabTsp->{'isSelf'} ? htmllocale('Reprise sur chantier') :
                                                htmllocale('Transfert inter-chantiers'));
        }
        else
        {
            $typeLabel = ($tabTsp->{'isSelf'} ? htmllocale('Restitution') :
                                                htmllocale('Récupération'));
        }

        print '
            <tr id="tsp_' . $tabTsp->{'id'} . '"' . (&LOC::Util::in_array($tabTsp->{'id'}, \@tabErrorTspIds) ? ' class="error"' : '') . '>
                <td class="type" data-val="' . $typeLabel . '">
                    <div class="icon ' . ($tabTsp->{'isSelf'} ? 'self' : 'std') . '" title="' . $typeLabel . '"></div>
                </td>
                <td class="code" data-val="' . $tabTsp->{'from.tabInfos'}->{'code'} . '">';

        my $fromContractUrl = $view->createURL('rent:rentContract:view', {'contractId' => $tabTsp->{'from.id'},
                                                                          'tabIndex'   => LOC::Transport::DEFAULT_CONTRACT_TABINDEX});
        if ($actionName eq 'transfers')
        {
            my $toContractUrl   = $view->createURL('rent:rentContract:view', {'contractId' => $tabTsp->{'to.id'},
                                                                              'tabIndex'   => LOC::Transport::DEFAULT_CONTRACT_TABINDEX});

            print '
                    <ul class="rows">
                        <li class="top double">
                            <a href="' . $fromContractUrl . '" target="_blank">' . $tabTsp->{'from.tabInfos'}->{'code'} . '</a>
                        </li>
                        <li class="btm double">
                            <a href="' . $toContractUrl . '" target="_blank">' . $tabTsp->{'to.tabInfos'}->{'code'} . '</a>
                        </li>
                    </ul>';
        }
        else
        {
            print '
                    <a href="' . $fromContractUrl . '" target="_blank">' . $tabTsp->{'from.tabInfos'}->{'code'} . '</a>';
        }

        print '
                </td>
                <td class="period" data-val="' . $tabTsp->{'from.tabInfos'}->{'beginDate'} . '">';

        if ($actionName eq 'transfers')
        {
            print '
                    <ul class="rows">
                        <li class="top double">' .

                            $locale->getDateFormat($tabTsp->{'from.tabInfos'}->{'beginDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC) .
                            ' → ' .
                            $locale->getDateFormat($tabTsp->{'from.tabInfos'}->{'endDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC) .

                       '</li>
                        <li class="btm double">' .

                            $locale->getDateFormat($tabTsp->{'to.tabInfos'}->{'beginDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC) .
                            ' → ' .
                            $locale->getDateFormat($tabTsp->{'to.tabInfos'}->{'endDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC) .

                       '</li>
                    </ul>';
        }
        else
        {
            print $locale->getDateFormat($tabTsp->{'from.tabInfos'}->{'beginDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC) .
                  ' → ' .
                  $locale->getDateFormat($tabTsp->{'from.tabInfos'}->{'endDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC);
        }

        print '
                </td>';

        if ($actionName eq 'transfers')
        {
            print '
                <td class="duration" data-val="' . ($tabTsp->{'from.tabInfos'}->{'duration'} * 10) . '.' . ($tabTsp->{'to.tabInfos'}->{'duration'} * 10) . '">
                    <ul class="rows">
                        <li class="top double">' . $tabTsp->{'from.tabInfos'}->{'duration'} . '</li>
                        <li class="btm double">' . $tabTsp->{'to.tabInfos'}->{'duration'} . '</li>
                    </ul>
                </td>';
        }
        else
        {
            print '
                <td class="duration" data-val="' . ($tabTsp->{'from.tabInfos'}->{'duration'} * 10) . '">' .
                    $locale->getNumberFormat($tabTsp->{'from.tabInfos'}->{'duration'}) .
               '</td>';
        }

        if ($actionName eq 'recoveries')
        {
            print '
                <td class="stopDate" data-val="' . $tabTsp->{'from.tabInfos'}->{'actionsDates'}->{'stop'} . '">' .
                    $locale->getDateFormat($tabTsp->{'from.tabInfos'}->{'actionsDates'}->{'stop'},
                                               &LOC::Locale::FORMAT_DATE_NUMERIC) . '</td>';
        }

        print '
                <td class="from">' . $view->toHTMLEntities($tabTsp->{'from.tabInfos'}->{'site.tabInfos'}->{'label'}) . '<br />
                                 ' . $view->toHTMLEntities($tabTsp->{'from.tabInfos'}->{'site.tabInfos'}->{'locality.postalCode'} . ' ' .
                                                           $tabTsp->{'from.tabInfos'}->{'site.tabInfos'}->{'locality.name'}) . '
                </td>';

        if ($actionName eq 'transfers')
        {
            print '
                <td class="arrow">→</td>
                <td class="to">' . $view->toHTMLEntities($tabTsp->{'to.tabInfos'}->{'site.tabInfos'}->{'label'}) . '<br />
                               ' . $view->toHTMLEntities($tabTsp->{'to.tabInfos'}->{'site.tabInfos'}->{'locality.postalCode'} . ' ' .
                                                         $tabTsp->{'to.tabInfos'}->{'site.tabInfos'}->{'locality.name'}) . '
                </td>';
        }

        if ($actionName eq 'transfers')
        {
            print '
                <td class="asbestos" data-val="' . $tabTsp->{'from.tabInfos'}->{'site.tabInfos'}->{'isAsbestos'} . '.' . $tabTsp->{'site.tabInfos'}->{'isAsbestos'} . '">
                    <ul class="rows">
                        <li class="top double">' . $view->displayBoolean($tabTsp->{'from.tabInfos'}->{'site.tabInfos'}->{'isAsbestos'}) . '</li>
                        <li class="btm double">' . $view->displayBoolean($tabTsp->{'to.tabInfos'}->{'site.tabInfos'}->{'isAsbestos'}) . '</li>
                    </ul>
                </td>';
        }
        else
        {
            print '
                <td class="asbestos" data-val="' . $tabTsp->{'from.tabInfos'}->{'site.tabInfos'}->{'isAsbestos'} . '">' .
                    $view->displayBoolean($tabTsp->{'from.tabInfos'}->{'site.tabInfos'}->{'isAsbestos'}) .
               '</td>';
        }

        print '
                <td class="parkNumber" data-val="' . $tabTsp->{'machine.tabInfos'}->{'parkNumber'} . '">' .
                    $view->displayParkNumber($tabTsp->{'machine.tabInfos'}->{'parkNumber'}) .
               '</td>';
        if ($actionName eq 'recoveries')
        {
            my $otherAgency = ($tabTsp->{'machine.tabInfos'}->{'agency.id'} eq $agencyId ?
                                    '' : '<span title="' . htmllocale('La machine n\'est plus dans l\'agence') . '">' .
                                              $tabTsp->{'machine.tabInfos'}->{'agency.id'} . '</span>');
            print '
                <td class="agency">' . $otherAgency . '</td>';
        }
        print '
                <td class="model">' . $view->toHTMLEntities($tabTsp->{'model.tabInfos'}->{'label'}) . '</td>
                <td class="customer" data-val="' . $view->toHTMLEntities($tabTsp->{'from.tabInfos'}->{'customer.name'}) . '">';

        if ($actionName eq 'transfers')
        {
            print '
                    <ul class="rows">
                        <li class="top double">
                            <a href="' . $tabTsp->{'from.tabInfos'}->{'customer.url'} . '" target="_blank">' .
                                $view->toHTMLEntities($tabTsp->{'from.tabInfos'}->{'customer.name'}) . '
                            </a>
                        </li>
                        <li class="btm double">
                            <a href="' . $tabTsp->{'to.tabInfos'}->{'customer.url'} . '" target="_blank">' .
                                $view->toHTMLEntities($tabTsp->{'to.tabInfos'}->{'customer.name'}) . '
                            </a>
                        </li>
                    </ul>';
        }
        else
        {
            print '
                    <a href="' . $tabTsp->{'from.tabInfos'}->{'customer.url'} . '" target="_blank">' .
                        $view->toHTMLEntities($tabTsp->{'from.tabInfos'}->{'customer.name'}) . '
                    </a>';
        }

        # Champ pour le compteur horaire
        my $timeCounterInput = $view->displayFormInputText('timeCounter_' . $tabTsp->{'id'},
                                                     $tabTsp->{'machine.tabInfos'}->{'timeCounter'},
                                                     LOC::Html::TEXTMODE_NUMERIC,
                                                     {
                                                      'data-tspid' => $tabTsp->{'id'},
                                                      'data-max'   => $tabTsp->{'machine.tabInfos'}->{'timeCounter'} + $tabViewData{'maxDiffTimeCounter'},
                                                      'data-min'   => $tabTsp->{'machine.tabInfos'}->{'timeCounter'},
                                                      'class' => 'input-timeCounter',
                                                      'size'     => 6
                                                     });
        $timeCounterInput .= $view->displayJSBlock('var timeCounterSpin = Location.spinControlsManager.createSpinControl("timeCounter_' . $tabTsp->{'id'} . '", 0, null, 1, 10, 1);');
        if (!defined $tabTsp->{'machine.tabInfos'}->{'timeCounter'} || !$tabViewData{'isKimoceCallable'})
        {
            $timeCounterInput .= $view->displayJSBlock('timeCounterSpin.disable()');
        }

        my $timeCounterWarning = '
                <div class="warning">
                    <span class="warning-min" style="display: none;">' .
                    $view->displayFormInputImage('', 'iconWarning.gif', '', {'title' => $locale->t('Compteur horaire inférieur à %s', $locale->getNumberFormat($tabTsp->{'machine.tabInfos'}->{'timeCounter'}, 1))}) . '</span>
                    <span class="warning-max" style="display: none;">' .
                    $view->displayFormInputImage('', 'iconWarning.gif', '', {'title' => $locale->t('Compteur horaire saisi supérieur à %s', $tabViewData{'maxDiffTimeCounter'})}) . '</span>
                </div>';

        my $recovery = $tabTsp->{'machineInventory'}->{'date'};
        my $recoveryText = ($recovery eq '' ? '' : $locale->getDateFormat($recovery, LOC::Locale::FORMAT_DATETIMESHORT_NUMERIC));
        my @tabTimeCountersRead = sort {$a <=> $b} @{$tabTsp->{'machineInventory'}->{'timeCounters'}};
        my $timeCounterRead = $tabTimeCountersRead[0];
        my $timeCounterReadText = '';
        my $nbTimeCountersRead = scalar @tabTimeCountersRead;
        if ($nbTimeCountersRead == 1)
        {
            $timeCounterReadText = $locale->getNumberFormat($tabTsp->{'machineInventory'}->{'timeCounters'}->[0]);
        }
        elsif ($nbTimeCountersRead >= 2)
        {
            foreach my $timeCounter (sort {$a <=> $b} @{$tabTsp->{'machineInventory'}->{'timeCounters'}})
            {
                $timeCounterReadText .= $locale->getNumberFormat($timeCounter) . '<br>';
            }
            $timeCounterReadText = substr($timeCounterReadText, 0, -4);
            $timeCounterReadText = $view->displayTooltipIcon('choice', $timeCounterReadText);
        }

        print '
                </td>
                <td class="recovery" data-val="' . $recovery . '">' . $recoveryText . '</td>
                <td class="timeCounterRead" data-val="' . $timeCounterRead . '">' . $timeCounterReadText . '</td>
                <td class="timeCounter">' . $timeCounterInput . ' ' . $timeCounterWarning . '</td>
                <td class="consider">';

        if ($tabTsp->{'tabRights'}->{'actions'}->{'consider'})
        {
            my $titleKim = '';
            my $titleNoKim = '';
            if ($actionName eq 'transfers')
            {
                $titleKim   = htmllocale('Prendre en compte et générer une demande d\'intervention');
                $titleNoKim = htmllocale('Prendre en compte et ne pas générer de demande d\'intervention');
            }
            if ($actionName eq 'recoveries')
            {
                $titleKim   = htmllocale('Prendre en compte et générer une demande d\'entretien');
                $titleNoKim = htmllocale('Prendre en compte et ne pas générer de demande d\'entretien');
            }

            my $class = (!$tabViewData{'isKimoceCallable'} ? ' disabled-kim' : '');

            print '
                    <div class="consider-btn' . $class . '" data-tspid="' . $tabTsp->{'id'} . '">
                        <div class="kim" title="' . $titleKim . '"></div>
                        <div class="nokim" title="' . $titleNoKim . '"></div>
                    </div>';
        }

        print '
                </td>
            </tr>';


        # Ajout dans la liste
        push(@tabList, {
            'id'         => $tabTsp->{'id'},
            'parkNumber' => $tabTsp->{'machine.tabInfos'}->{'parkNumber'}
        });
    }

    print '
        </tbody>
        </table>

    </div>

</div>';

    # Affichage des champs cachés
    print $view->displayFormInputHidden('valid', '');

    # Affichage du pied de page
    print $view->endForm();



    # Tri du tableau
    print $view->displayJSBlock('
$.tablesorter.addParser({
    id: "textAttr",
    is: function(s) { return false; },
    format: function(s, table, cell, cellIndex) { return $(cell).attr("data-val"); },
    type: "text"
});
$.tablesorter.addParser({
    id: "integerAttr",
    is: function(s) { return false; },
    format: function(s, table, cell, cellIndex) { return $(cell).attr("data-val"); },
    type: "numeric"
});

$(function()
{
    $("#table-main").tablesorter({
        headers: [' . $headersOption . '],
        sortList: ' . $sortListOption . ',
        widgets: ["zebra"]
    });
}
);');


print $view->displayJSBlock('
$(function()
{
    transportsUI.init(' . &LOC::Json::toJson({
        'nbConsidersMax' => $tabViewData{'nbConsidersMax'},
        'list'           => \@tabList
    }) . ');
});');


}

# Affichage du pied de page
print $view->displayFooter();
