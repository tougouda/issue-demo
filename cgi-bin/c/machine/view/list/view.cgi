use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'machine/list/';


our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});


sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}

# CSS
$view->addCSSSrc($dir . 'view.css');
$view->addCSSSrc('location.css');

# JS
$view->addJSSrc($dir . 'view.js');
# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc('jQuery/plugins/ajaxTable.js');
$view->addCSSSrc('jQuery/plugins/ajaxTable.css');



# Affichage de l'entête
$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT);

$view->setPageTitle($locale->t('Recherche machines'));
$view->setTitle($locale->t('Recherche machines'));


# Traductions
$view->addTranslations({
    'isMachineWithTelematic' => $locale->t('Cette machine est équipée de télématique'),
    'isAsbestos'             => $locale->t('Présence d\'amiante sur le chantier du dernier contrat')
});


# Bouton "Actualiser"
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png',
                                          '',
                                          $locale->t('Actualiser la page'),
                                          'l',
                                          {'id' => 'refresh-btn'});
$view->addControlsContent([$refreshBtn], 'right', 1);

$view->{'_networkOptimization'} = 'machine-list-view';
print $view->displayHeader();


# Bouton d'export Excel
my $excelBtn = $view->displayTextButton($locale->t('Export Excel'), 'common/ajaxTable/excel(|Over).png',
                                        'return true;',
                                        '',
                                        'l',
                                        {'class' => 'locCtrlButton', 'id' => 'excel-btn'});

# Bouton pour plus de filtres
my $moreFiltersBtn = $view->displayTextButton($locale->t('Plus de filtres'), 'common/ajaxTable/expand(|Over).png',
                                               'return true;',
                                               '',
                                               'l',
                                               {'class' => 'locCtrlButton', 'id' => 'more-filters-btn'});

# Bouton pour moins de filtres
my $lessFiltersBtn = $view->displayTextButton($locale->t('Moins de filtres'), 'common/ajaxTable/collapse(|Over).png',
                                               'return true;',
                                               '',
                                               'l',
                                               {'class' => 'locCtrlButton', 'id' => 'less-filters-btn'});

# Bouton de réinitialisation des filtres
my $resetFiltersBtn = $view->displayTextButton($locale->t('Réinitialiser'), 'common/ajaxTable/reset.png',
                                               'return true;',
                                               '',
                                               'l',
                                               {'class' => 'locCtrlButton', 'id' => 'reset-filters-btn'});


print '
<fieldset id="table-filters-container" class="filters">
    <legend>' . htmllocale('Filtres') . '</legend>

    <ul class="content">
        <li class="part">
            <ul>
                <li id="filter-park-number"></li>
                <li id="filter-family"></li>
                <li id="filter-elevation"></li>
                <li id="filter-energy"></li>
                <li id="filter-model"></li>
                <li id="filter-state"></li>
            </ul>
        </li>
        <li class="part">
            <fieldset>
                <legend>' . htmllocale('Localisation') . '</legend>
                <ul>
                    <li id="filter-agency"></li>
                    <li id="filter-area"></li>
                </ul>
            </fieldset>
            <fieldset>
                <legend>' . htmllocale('Équipements') . '</legend>
                <ul>
                    <li id="filter-with-security-system"></li>
                    <li id="filter-with-stabilizers"></li>
                    <li id="filter-with-telematic"></li>
                </ul>
            </fieldset>
        </li>
    </ul>
    <ul class="content" id="additional-filters">
        <li class="part">
            <fieldset>
                <legend>' . htmllocale('Autres critères') . '</legend>
                <ul>
                    <li id="filter-tariff-family"></li>
                    <li id="filter-manufacturer"></li>
                    <li id="filter-serial-number"></li>
                    <li id="filter-accessory"></li>
                </ul>
            </fieldset>
            <fieldset>
                <legend>' . htmllocale('Paramètres de location') . '</legend>
                <ul>
                    <li id="filter-park"></li>
                    <li id="filter-subrent"></li>
                    <li id="filter-categoryb"></li>
                    <li id="filter-is-asbestos"></li>
                </ul>
            </fieldset>
        </li>
    </ul>
    <div class="ctrls left">
        ' . $moreFiltersBtn . '
        ' . $lessFiltersBtn . '
    </div>
    <div class="ctrls right">
        ' . $resetFiltersBtn . '
    </div>
</fieldset>

<div class="list">
    <div id="table-toolbar-container" class="toolbar header">
        <div class="col content">
            <div id="header-pagination-container"></div>
            <div class="fixed-content">
                <div id="header-filters-recap"></div>
            </div>
        </div>
        <div class="col controls">
            ' . $excelBtn . '
            <div class="fixed-content">
                <a href="#table-filters-container" id="back-top-btn" title="' . htmllocale('Retourner en haut de la page') . '">' . $view->displayImage('common/ajaxTable/up.png') . '</a>
            </div>
        </div>
    </div>
    <div class="content" id="table-container"></div>
</div>';


# Formatage de la liste
my $tabData = $tabViewData{'data'};
require($directory . '/list/format/list.cgi');

# Formatage des données de l'onglet Durée
&formatList($tabViewData{'user.country.id'}, $tabData, $locale, {});

# Paramètrage des colonnes
my @tabColumnsOptions = (
    {
        'id'         => 'agency.id',
        'title'      => $locale->t('Ag.'),
        'tip'        => $locale->t('Agence'),
        'cssClasses' => 'agency',
        'sort'       => 1
    },
    {
        'id'         => 'parkNumber',
        'title'      => $locale->t('N° parc'),
        'cssClasses' => 'number',
        'sort'       => 1
    },
    {
        'id'         => 'family.label',
        'title'      => $locale->t('Famille'),
        'cssClasses' => 'family',
        'sort'       => 1
    },
    {
        'id'         => 'model.label',
        'title'      => $locale->t('Mod. mach.'),
        'tip'        => $locale->t('Modèle de machine'),
        'cssClasses' => 'model',
        'sort'       => 1
    },
    {
        'id'         => 'manufacturer.label',
        'title'      => $locale->t('Fabricant'),
        'cssClasses' => 'manufacturer',
        'sort'       => 1
    },
    {
        'id'         => 'elevation',
        'title'      => $locale->t('Élév.'),
        'tip'        => $locale->t('Élévation'),
        'cssClasses' => 'elevation',
        'sort'       => 1
    },
    {
        'id'         => 'energy',
        'title'      => $locale->t('Énergie'),
        'cssClasses' => 'energy',
        'sort'       => 1
    },
    {
        'id'         => 'weight',
        'title'      => $locale->t('Poids'),
        'tip'        => $locale->t('Poids') . ' (' . $locale->t('kg') . ')',
        'cssClasses' => 'weight',
        'sort'       => 1
    },
    {
        'id'         => 'length',
        'title'      => $locale->t('Long.'),
        'tip'        => $locale->t('Longueur') . ' (' . $locale->t('m') . ')',
        'cssClasses' => 'length',
        'sort'       => 1
    },
    {
        'id'         => 'width',
        'title'      => $locale->t('Larg.'),
        'tip'        => $locale->t('Largeur') . ' (' . $locale->t('m') . ')',
        'cssClasses' => 'width',
        'sort'       => 1
    },
    {
        'id'         => 'height',
        'title'      => $locale->t('Haut.'),
        'tip'        => $locale->t('Hauteur repliée') . ' (' . $locale->t('m') . ')',
        'cssClasses' => 'height',
        'sort'       => 1
    },
    {
        'id'         => 'state.label',
        'title'      => $locale->t('État'),
        'cssClasses' => 'state',
        'sort'       => 1
    },
    {
        'id'         => 'isOutOfPark',
        'title'      => $locale->t('H.P.'),
        'tip'        => $locale->t('Hors parc'),
        'cssClasses' => 'park',
        'sort'       => 1
    },
    {
        'id'         => 'contract.isAsbestos',
        'title'      => '',
        'tip'        => '',
        'cssClasses' => 'asbestos',
        'sort'       => 0
    },
    {
        'id'         => 'contract.endDate',
        'title'      => $locale->t('Dern. ctt.'),
        'tip'        => $locale->t('Dernier contrat'),
        'cssClasses' => 'contract',
        'sort'       => 1
    },
    {
        'id'         => 'control.nextDate',
        'title'      => $locale->t('Pr. con.'),
        'tip'        => $locale->t('Prochain contrôle'),
        'cssClasses' => 'control',
        'sort'       => 1
    }
);


# Paramètrage des filtres de recherche
my $updateFilterDelay = 600;
my @tabFiltersOptions = (
    {
        'id' => 'parkNumber',
        'container' => '#filter-park-number',
        'type' => 'search',
        'options' => {
            'delay' => 500
        },
        'labels' => {
            'title'       => $locale->t('N° de parc'),
            'inactive'    => $locale->t('Tous les n° de parc'),
            'noSelection' => $locale->t('Choisir le n° de parc'),
            'selection'   => $locale->t('Le n° de parc %s', '<%search>'),
            'search'      => $locale->t('Saisir un n° de parc'),
            'clearSearch' => $locale->t('Vider la saisie')
        }
    },
    {
        'id' => 'family.id',
        'container' => '#filter-family',
        'type' => 'list',
        'options' => {
            'delay'  => $updateFilterDelay,
            'search' => 1
        },
        'labels' => {
            'title'        => $locale->t('Famille(s)'),
            'inactive'     => $locale->t('Toutes les familles'),
            'noSelection'  => $locale->t('Choisir les familles'),
            'selections'   => $locale->t('Les familles %s', '<%values>'),
            'selection'    => $locale->t('La famille %s', '<%value>'),
            'noItemAvlb'   => $locale->t('Aucune famille disponible'),
            'search'       => $locale->t('Rechercher une famille'),
            'clearSearch'  => $locale->t('Vider la saisie'),
            'noSearchRslt' => $locale->t('Pas de correspondance')
        }
    },
    {
        'id' => 'elevation',
        'container' => '#filter-elevation',
        'type' => 'list',
        'options' => {
            'delay' => $updateFilterDelay,
            'search' => 1
        },
        'labels' => {
            'title'        => $locale->t('Élévation(s)'),
            'inactive'     => $locale->t('Toutes les élévations'),
            'noSelection'  => $locale->t('Choisir les élévations'),
            'selections'   => $locale->t('Les élévations %s', '<%values>'),
            'selection'    => $locale->t('L\'élévation %s', '<%value>'),
            'noItemAvlb'   => $locale->t('Aucune élévation disponible'),
            'search'       => $locale->t('Rechercher une élévation'),
            'clearSearch'  => $locale->t('Vider la saisie'),
            'noSearchRslt' => $locale->t('Pas de correspondance')
        }
    },
    {
        'id' => 'energy',
        'container' => '#filter-energy',
        'type' => 'list',
        'options' => {
            'delay' => $updateFilterDelay
        },
        'labels' => {
            'title'       => $locale->t('Énergie(s)'),
            'inactive'    => $locale->t('Toutes les énergies'),
            'noSelection' => $locale->t('Choisir les énergies'),
            'selections'  => $locale->t('Les énergies %s', '<%values>'),
            'selection'   => $locale->t('L\'énergie %s', '<%value>'),
            'noItemAvlb'  => $locale->t('Aucune énergie disponible')
        }
    },
    {
        'id' => 'tariffFamily.id',
        'container' => '#filter-tariff-family',
        'type' => 'list',
        'options' => {
            'delay'  => $updateFilterDelay,
            'search' => 1
        },
        'labels' => {
            'title'        => $locale->t('Famille(s) tarifaire(s)'),
            'inactive'     => $locale->t('Toutes les familles tarifaires'),
            'noSelection'  => $locale->t('Choisir les familles tarifaires'),
            'selections'   => $locale->t('Les familles tarifaires %s', '<%values>'),
            'selection'    => $locale->t('La famille tarifaire %s', '<%value>'),
            'noItemAvlb'   => $locale->t('Aucune famille tarifaire disponible'),
            'search'       => $locale->t('Rechercher une famille tarifaire'),
            'clearSearch'  => $locale->t('Vider la saisie'),
            'noSearchRslt' => $locale->t('Pas de correspondance')
        }
    },
    {
        'id' => 'state.id',
        'container' => '#filter-state',
        'type' => 'list',
        'options' => {
            'delay' => $updateFilterDelay,
            'search' => 1
        },
        'labels' => {
            'title'        => $locale->t('État(s)'),
            'inactive'     => $locale->t('Tous les états'),
            'noSelection'  => $locale->t('Choisir les états'),
            'selections'   => $locale->t('Les états %s', '<%values>'),
            'selection'    => $locale->t('L\'état %s', '<%value>'),
            'noItemAvlb'   => $locale->t('Aucun état disponible'),
            'search'       => $locale->t('Rechercher un état'),
            'clearSearch'  => $locale->t('Vider la saisie'),
            'noSearchRslt' => $locale->t('Pas de correspondance')
        }
    },
    {
        'id' => 'model.id',
        'container' => '#filter-model',
        'type' => 'list',
        'options' => {
            'delay'  => $updateFilterDelay,
            'search' => 1
        },
        'labels' => {
            'title'        => $locale->t('Modèle(s) de machines'),
            'inactive'     => $locale->t('Tous les modèles de machines'),
            'noSelection'  => $locale->t('Choisir les modèles de machines'),
            'selections'   => $locale->t('Les modèles de machines %s', '<%values>'),
            'selection'    => $locale->t('Le modèle de machines %s', '<%value>'),
            'noItemAvlb'   => $locale->t('Aucun modèle disponible'),
            'search'       => $locale->t('Rechercher un modèle de machines'),
            'clearSearch'  => $locale->t('Vider la saisie'),
            'noSearchRslt' => $locale->t('Pas de correspondance')
        }
    },
    {
        'id' => 'manufacturer.id',
        'container' => '#filter-manufacturer',
        'type' => 'list',
        'options' => {
            'delay'  => $updateFilterDelay,
            'search' => 1
        },
        'labels' => {
            'title'        => $locale->t('Fabricant(s)'),
            'inactive'     => $locale->t('Tous les fabricants'),
            'noSelection'  => $locale->t('Choisir les fabricants'),
            'selections'   => $locale->t('Les fabricants %s', '<%values>'),
            'selection'    => $locale->t('Le fabricant %s', '<%value>'),
            'noItemAvlb'   => $locale->t('Aucun fabricant disponible'),
            'search'       => $locale->t('Rechercher un fabricant'),
            'clearSearch'  => $locale->t('Vider la saisie'),
            'noSearchRslt' => $locale->t('Pas de correspondance')
        }
    },
    {
        'id' => 'serialNumber',
        'container' => '#filter-serial-number',
        'type' => 'search',
        'options' => {
            'delay' => 500
        },
        'labels' => {
            'title'       => $locale->t('N° de série'),
            'inactive'    => $locale->t('Tous les n° de série'),
            'noSelection' => $locale->t('Choisir le n° de série'),
            'selection'   => $locale->t('Le n° de série %s', '<%search>'),
            'search'      => $locale->t('Saisir un n° de série'),
            'clearSearch' => $locale->t('Vider la saisie')
        }
    },
    {
        'id' => 'isOutOfPark',
        'container' => '#filter-park',
        'type' => 'radio',
        'labels' => {
            'inactive' => $locale->t('Parc normal et hors parc'),
            'items'    => [
                {'value' => 0, 'text' => 'Parc normal'},
                {'value' => 1, 'text' => 'Hors parc'}
            ]
        }
    },
    {
        'id' => 'isWithSecuritySystem',
        'container' => '#filter-with-security-system',
        'type' => 'list',
        'options' => {
            'delay'  => $updateFilterDelay
        },
        'labels' => {
            'title'        => $locale->t('GIES ou non'),
            'inactive'     => $locale->t('GIES ou non'),
            'noSelection'  => $locale->t('Choisir les valeurs'),
            'selections'   => '<%values>',
            'selection'    => '<%value>'
        }
    },
    {
        'id' => 'isWithStabilizers',
        'container' => '#filter-with-stabilizers',
        'type' => 'list',
        'options' => {
            'delay'  => $updateFilterDelay
        },
        'labels' => {
            'title'        => $locale->t('Stabilisateurs ou non'),
            'inactive'     => $locale->t('Stabilisateurs ou non'),
            'noSelection'  => $locale->t('Choisir les valeurs'),
            'selections'   => '<%values>',
            'selection'    => '<%value>'
        }
    },
    {
        'id' => 'isWithTelematic',
        'container' => '#filter-with-telematic',
        'type' => 'list',
        'options' => {
            'delay'  => $updateFilterDelay
        },
        'labels' => {
            'title'        => $locale->t('Télématique ou non'),
            'inactive'     => $locale->t('Télématique ou non'),
            'noSelection'  => $locale->t('Choisir les valeurs'),
            'selections'   => '<%values>',
            'selection'    => '<%value>'
        }
    },
    {
        'id' => 'isAccessory',
        'container' => '#filter-accessory',
        'type' => 'radio',
        'labels' => {
            'inactive' => $locale->t('Accessoire ou non'),
            'items'    => [
                {'value' => 0, 'text' => 'Non accessoire'},
                {'value' => 1, 'text' => 'Accessoire'}
            ]
        }
    },
    {
        'id' => 'isSubRent',
        'container' => '#filter-subrent',
        'type' => 'radio',
        'labels' => {
            'inactive' => $locale->t('Tous les types de location'),
            'items'    => [
                {'value' => 0, 'text' => 'Location'},
                {'value' => 1, 'text' => 'Sous-location'}
            ]
        }
    },
    {
        'id' => 'isCategoryB',
        'container' => '#filter-categoryb',
        'type' => 'radio',
        'labels' => {
            'inactive' => $locale->t('Toutes les catégories'),
            'items'    => [
                {'value' => 0, 'text' => 'Normale'},
                {'value' => 1, 'text' => 'Catégorie B'}
            ]
        }
    },
    {
        'id' => 'isAsbestos',
        'container' => '#filter-is-asbestos',
        'type' => 'radio',
        'labels' => {
            'inactive' => $locale->t('Chantiers amiante ou non'),
            'items'    => [
                {'value' => 0, 'text' => 'Chantier sans amiante'},
                {'value' => 1, 'text' => 'Chantier avec amiante'}
            ]
        }
    },
    {
        'id' => 'agency.id',
        'container' => '#filter-agency',
        'type' => 'list',
        'options' => {
            'delay' => $updateFilterDelay
        },
        'labels' => {
            'title'       => $locale->t('Agence(s)'),
            'inactive'    => $locale->t('Toutes les agences'),
            'noSelection' => $locale->t('Choisir les agences'),
            'selections'  => $locale->t('Les agences de %s', '<%values>'),
            'selection'   => $locale->t('L\'agence de %s', '<%value>'),
            'noItemAvlb'  => $locale->t('Aucune agence disponible')
        }
    },
    {
        'id' => 'area.id',
        'container' => '#filter-area',
        'type' => 'list',
        'options' => {
            'delay' => $updateFilterDelay
        },
        'labels' => {
            'title'       => $locale->t('Région(s)'),
            'inactive'    => $locale->t('Toutes les régions'),
            'noSelection' => $locale->t('Choisir les régions'),
            'selections'  => $locale->t('Les régions de %s', '<%values>'),
            'selection'   => $locale->t('La région de %s', '<%value>'),
            'emptyValue'  => $locale->t('Sans région'),
            'noItemAvlb'  => $locale->t('Aucune région disponible')
        }
    }
);


# Paramètrage du tableau
my %tabAjaxTableOptions = (
    'url'            => $view->createURL('machine:list:view', {'displayType' => 'json'}),
    'cssClasses'     => 'basic list',
    'startIndex'     => $tabViewData{'startIndex'},
    'pageSize'       => $tabViewData{'pageSize'},
    'sorts'          => $tabViewData{'sorts'},
    'activesFilters' => $tabViewData{'activesFilters'},
    'defaultActivesFilters' => $tabViewData{'defaultActivesFilters'},
    'data'           => $tabData,
    'paginations'    => '#header-pagination-container',
    'filtersRecaps'  => '#header-filters-recap',
    'columns'        => \@tabColumnsOptions,
    'filters'        => \@tabFiltersOptions,
    'labels'         => {
        'pagination' => {
            'noResult' => $locale->t('Pas de résultat'),
            'result'   => $locale->t('%s résultat', '<b>1</b>'),
            'results'  => $locale->t('%s-%s sur %s résultats', '<b><%start></b>', '<b><%end></b>', '<b><%total></b>')
        },
        'clearSel' => $locale->t('Vider la sélection'),
        'noResult' => $locale->t('Aucune machine trouvée'),
        'errorMsg' => $locale->t('Une erreur s\'est produite.Veuillez contacter le service informatique s.v.p.')
    }
);

# Variables à passer au JS
print $view->displayJSBlock('
var isTelematActived = ' . $tabViewData{'isTelematActived'} . ';');

# Tri du tableau
print $view->displayJSBlock('
$(function()
{
    pageInit(' . &LOC::Json::toJson({
        'tableOptions' => \%tabAjaxTableOptions,
        'excelRowsLimit' => $tabViewData{'excelRowsLimit'}
    }) . ');
});');


# Affichage du pied de page
print $view->displayFooter();

