use utf8;
use open (':encoding(UTF-8)');

use strict;


use Spreadsheet::WriteExcel;

use LOC::Locale;
use LOC::Browser;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;



our %tabViewData;

my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});


# Fonctions utiles
sub displayBoolean
{
    my ($value) = @_;
    return ($value ? 'X' : '');
}


# Téléchargement du fichier
my $fileName = $locale->t('Liste machines') . '_' . POSIX::strftime('%Y%m%d_%H%M', localtime()) . '.xls';
$fileName =~ tr/"<>&\/\\?#\*;/'{}+____\-\-/;

&LOC::Browser::sendHeaders(
        "Content-Type: application/force-download\n" .
        "Content-Disposition: attachment; filename=\"" . $fileName . "\"\n" .
        "\n"
    );


# Nouveau classeur Excel envoyé sur la sortie standard
my $workbook = Spreadsheet::WriteExcel->new('-');
my $title = $locale->t('Liste des machines');
my $author = $locale->t('Gestion des locations') . ' ' . &LOC::Globals::get('version');
my $company = 'Accès industrie';
$workbook->set_properties('title' => $title, 'author' => $author, 'company' => $company);

# Format de la ligne d'en-tête
my $headerFmt = $workbook->add_format();
$headerFmt->set_italic();
$headerFmt->set_bold();
$headerFmt->set_bg_color($workbook->set_custom_color(12, 232, 243, 249));
$headerFmt->set_bottom();

# Format de date
my $dateFmt = $workbook->add_format();
$dateFmt->set_num_format(14);

# Format des booléens
my $boolFmt = $workbook->add_format();
$boolFmt->set_align('center');


# Formatage de la liste
my $tabData = $tabViewData{'data'};


my $worsheetLabel = $locale->t('Liste des machines');
my $worksheet = $workbook->add_worksheet($worsheetLabel);
$worksheet->activate();

# Entête et pied de page pour l'impression
$worksheet->set_header('&C&12&",Bold"' . $title . ' - ' . $worsheetLabel);
$worksheet->set_footer('&C&8' . $locale->t('Page') . ' &P / &N');


# Figer les volets
$worksheet->freeze_panes(1, 0);


# Ligne d'en-tête
my $c = 0;
my $l = 0;
$worksheet->set_column($c, $c, 12);
$worksheet->write_string($l, $c++, $locale->t('Agence'), $headerFmt);
$worksheet->set_column($c, $c, 12);
$worksheet->write_string($l, $c++, $locale->t('N° parc'), $headerFmt);
$worksheet->set_column($c, $c, 20);
$worksheet->write_string($l, $c++, $locale->t('Famille'), $headerFmt);
$worksheet->set_column($c, $c, 22);
$worksheet->write_string($l, $c++, $locale->t('Modèle de machines'), $headerFmt);
$worksheet->set_column($c, $c, 20);
$worksheet->write_string($l, $c++, $locale->t('Fabricant'), $headerFmt);
$worksheet->set_column($c, $c, 14);
$worksheet->write_string($l, $c++, $locale->t('Élévation'), $headerFmt);
$worksheet->set_column($c, $c, 12);
$worksheet->write_string($l, $c++, $locale->t('Énergie'), $headerFmt);
$worksheet->set_column($c, $c, 14);
$worksheet->write_string($l, $c++, $locale->t('Poids' . ' (' . $locale->t('kg') . ')'), $headerFmt);
$worksheet->set_column($c, $c, 14);
$worksheet->write_string($l, $c++, $locale->t('Longueur' . ' (' . $locale->t('m') . ')'), $headerFmt);
$worksheet->set_column($c, $c, 14);
$worksheet->write_string($l, $c++, $locale->t('Largeur' . ' (' . $locale->t('m') . ')'), $headerFmt);
$worksheet->set_column($c, $c, 20);
$worksheet->write_string($l, $c++, $locale->t('Hauteur repliée' . ' (' . $locale->t('m') . ')'), $headerFmt);
$worksheet->set_column($c, $c, 14);
$worksheet->write_string($l, $c++, $locale->t('État'), $headerFmt);
$worksheet->set_column($c, $c, 12);
$worksheet->write_string($l, $c++, $locale->t('Hors parc'), $headerFmt);
$worksheet->set_column($c, $c, 12);
$worksheet->write_string($l, $c++, $locale->t('Amiante'), $headerFmt);
$worksheet->set_column($c, $c, 22);
$worksheet->write_string($l, $c++, $locale->t('Dernier contrat'), $headerFmt);
$worksheet->set_column($c, $c, 20);
$worksheet->write_string($l, $c++, $locale->t('Date de fin du dernier contrat'), $headerFmt);
$worksheet->set_column($c, $c, 20);
$worksheet->write_string($l, $c++, $locale->t('Prochain contrôle'), $headerFmt);

# Activation des filtres
$worksheet->autofilter(0, 0, 0, $c - 1);

$l++;



# Lignes de devis
foreach my $tabRow (@{$tabData->{'rows'}})
{
    $c = 0;

    my $agencyId = $tabRow->{'agency.id'};
    if ($tabRow->{'visibleOnAgency.id'})
    {
        $agencyId .= ' > ' . $tabRow->{'visibleOnAgency.id'};
    }

    $worksheet->write_string($l, $c++, $agencyId);
    $worksheet->write_string($l, $c++, $tabRow->{'parkNumber'});
    $worksheet->write_string($l, $c++, $tabRow->{'family.label'});
    $worksheet->write_string($l, $c++, $tabRow->{'model.label'});
    $worksheet->write_string($l, $c++, $tabRow->{'manufacturer.label'});
    $worksheet->write_string($l, $c++, $tabRow->{'elevation'});
    $worksheet->write_string($l, $c++, $tabRow->{'energy'});

    if ($tabRow->{'weight'} ne '')
    {
        $worksheet->write_number($l, $c, $tabRow->{'weight'});
    }
    $c++;

    if ($tabRow->{'length'} ne '')
    {
        $worksheet->write_number($l, $c, $tabRow->{'length'});
    }
    $c++;

    if ($tabRow->{'width'} ne '')
    {
        $worksheet->write_number($l, $c, $tabRow->{'width'});
    }
    $c++;

    if ($tabRow->{'height'} ne '')
    {
        $worksheet->write_number($l, $c, $tabRow->{'height'});
    }
    $c++;

    $worksheet->write_string($l, $c++, $tabRow->{'state.label'});
    $worksheet->write_string($l, $c++, &displayBoolean($tabRow->{'isOutOfPark'}), $boolFmt);
    $worksheet->write_string($l, $c++, &displayBoolean($tabRow->{'contract.isAsbestos'}), $boolFmt);
    $worksheet->write_string($l, $c++, $tabRow->{'contract.code'});
    if ($tabRow->{'contract.endDate'} ne '')
    {
        $worksheet->write_date_time($l, $c++, $tabRow->{'contract.endDate'} . 'T', $dateFmt);
    }
    else
    {
        $worksheet->write_string($l, $c++, '');
    }
    if ($tabRow->{'control.nextDate'} ne '' && $tabRow->{'control.nextDate'} ne '0000-00-00')
    {
        $worksheet->write_date_time($l, $c++, $tabRow->{'control.nextDate'} . 'T', $dateFmt);
    }
    else
    {
        $worksheet->write_string($l, $c++, '');
    }

    $l++;
}

# Fermeture du classeur
$workbook->close();
