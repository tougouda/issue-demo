use utf8;
use open (':encoding(UTF-8)');

use strict;


sub formatList
{
    my ($countryId, $tabData, $locale, $tabOptions) = @_;

    # Liste des choix pour le filtre des équipements GIES
    my %tabSecuritySystemLabels = (
        '1'    => {
            'label' => $locale->t('Avec GIES'),
            'order' => 1
        },
        '0'    => {
            'label' => $locale->t('Sans GIES'),
            'order' => 2
        },
        ''     => {
            'label' => $locale->t('GIES non renseigné'),
            'order' => 3
        },
        'null' => {
            'label' => $locale->t('Modèle non équipé GIES'),
            'order' => 4
        }
        );

    # Liste des choix pour le filtre des équipements STABI
    my %tabStabilizersLabels = (
        '1'    => {
            'label' => $locale->t('Avec stabilisateurs'),
            'order' => 1
        },
        '0'    => {
            'label' => $locale->t('Sans stabilisateurs'),
            'order' => 2
        },
        ''     => {
            'label' => $locale->t('Stabilisateurs non renseignés'),
            'order' => 3
        },
        'null' => {
            'label' => $locale->t('Modèle non équipé de stabilisateurs'),
            'order' => 4
        }
        );

    # Liste des choix pour le filtre des équipements TELEMAT
    my %tabTelematicLabels = (
        '1'    => {
            'label' => $locale->t('Avec télématique'),
            'order' => 1
        },
        '0'    => {
            'label' => $locale->t('Sans télématique'),
            'order' => 2
        },
        ''     => {
            'label' => $locale->t('Télématique non renseignée'),
            'order' => 3
        },
        'null' => {
            'label' => $locale->t('Modèle non équipé de télématique'),
            'order' => 4
        }
        );

    # Fonction d'ajout des libellés dans les listes sans libellés
    my $addLabels = sub {
        my ($tab, $tabLabels) = @_;

        foreach my $row (@$tab)
        {
            $row->{'label'} = $tabLabels->{$row->{'value'}};
        }
        my @tab2 = sort {$a->{'label'} cmp $b->{'label'}} @$tab;

        return \@tab2;
    };


    # Fonction d'ajout des libellés dans les listes des informations additionnelles et ordonnés dans un ordre prédéfini
    my $addLabelsAddInfos = sub {
        my ($tab, $tabLabels) = @_;
        foreach my $row (@$tab)
        {
            $row->{'label'} = $tabLabels->{$row->{'value'}}->{'label'};
            $row->{'order'} = $tabLabels->{$row->{'value'}}->{'order'};
        }

        my @tab2 = sort {$a->{'order'} cmp $b->{'order'}} @$tab;

        return \@tab2;
    };

    # Formatage des lignes
    foreach my $row (@{$tabData->{'rows'}})
    {
        # URL de la fiche machine
        $row->{'url'} = &LOC::Request::createRequestUri('machine:machine:view', {'machineId' => $row->{'id'}});

        # URL du dernier contrat associé
        if ($row->{'contract.code'} ne '' && $row->{'contract.country.id'} eq $countryId)
        {
            $row->{'contract.url'} = &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $row->{'contract.id'}});
        }

        # Formatage des données par rapport à la locale
        $row->{'weight'} = ($row->{'weight'} ne '' ? $locale->getNumberFormat($row->{'weight'}) : '');
        $row->{'length'} = ($row->{'length'} ne '' ? $locale->getNumberFormat($row->{'length'}) : '');
        $row->{'width'}  = ($row->{'width'} ne '' ? $locale->getNumberFormat($row->{'width'}) : '');
        $row->{'height'} = ($row->{'height'} ne '' ? $locale->getNumberFormat($row->{'height'}) : '');
        if ($row->{'control.nextDate'} ne '' && $row->{'control.nextDate'} ne '0000-00-00')
        {
            $row->{'control.nextDate'} = $locale->getDateFormat($row->{'control.nextDate'}, LOC::Locale::FORMAT_DATE_NUMERIC);
        }
        else
        {
            $row->{'control.nextDate'} = '';
        }
        if ($row->{'contract.endDate'} ne '')
        {
            $row->{'contract.endDate'} = $locale->t('Fin le %s', $locale->getDateFormat($row->{'contract.endDate'}, LOC::Locale::FORMAT_DATE_NUMERIC));
        }
        else
        {
            $row->{'contract.endDate'} =  '';
        }

        # Suppression des champs inutiles
        delete($row->{'contract.id'});
    }

    # Formatage des filtres
    if ($tabData->{'filters'})
    {
        my $tabFilters = $tabData->{'filters'};

        # Filtre sur l'équipement GIES
        if (exists $tabFilters->{'isWithSecuritySystem'})
        {
            $tabFilters->{'isWithSecuritySystem'}->{'list'} = &$addLabelsAddInfos($tabFilters->{'isWithSecuritySystem'}->{'list'}, \%tabSecuritySystemLabels);
            if (exists $tabFilters->{'isWithSecuritySystem'}->{'values'})
            {
                $tabFilters->{'isWithSecuritySystem'}->{'values'} = &$addLabelsAddInfos($tabFilters->{'isWithSecuritySystem'}->{'values'}, \%tabSecuritySystemLabels);
            }
        }

        # Filtre sur l'équipement STABI
        if (exists $tabFilters->{'isWithStabilizers'})
        {
            $tabFilters->{'isWithStabilizers'}->{'list'} = &$addLabelsAddInfos($tabFilters->{'isWithStabilizers'}->{'list'}, \%tabStabilizersLabels);
            if (exists $tabFilters->{'isWithStabilizers'}->{'values'})
            {
                $tabFilters->{'isWithStabilizers'}->{'values'} = &$addLabelsAddInfos($tabFilters->{'isWithStabilizers'}->{'values'}, \%tabStabilizersLabels);
            }
        }

        # Filtre sur l'équipement TELEMAT
        if (exists $tabFilters->{'isWithTelematic'})
        {
            $tabFilters->{'isWithTelematic'}->{'list'} = &$addLabelsAddInfos($tabFilters->{'isWithTelematic'}->{'list'}, \%tabTelematicLabels);
            if (exists $tabFilters->{'isWithTelematic'}->{'values'})
            {
                $tabFilters->{'isWithTelematic'}->{'values'} = &$addLabelsAddInfos($tabFilters->{'isWithTelematic'}->{'values'}, \%tabTelematicLabels);
            }
        }

    }
}


1;
