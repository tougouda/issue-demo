use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'machine/list/';


our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}



# CSS
$view->addCSSSrc($dir . 'view.css');
$view->addCSSSrc('location.css');

# JS
$view->addJSSrc($dir . 'view.js');
# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc('jQuery/plugins/ajaxTable.js');
$view->addCSSSrc('jQuery/plugins/ajaxTable.css');



# Affichage de l'entête
$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT);

$view->setPageTitle($locale->t('Machine(s) en visibilité'));
$view->setTitle($locale->t('Machine(s) en visibilité'));

#
# Bouton "Actualiser"
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png',
                                          '',
                                          $locale->t('Actualiser la page'),
                                          'l',
                                          {'id' => 'refresh-btn'});
$view->addControlsContent([$refreshBtn], 'right', 1);

$view->{'_networkOptimization'} = 'machine-list-viewVisibility';
print $view->displayHeader();


# Bouton d'export Excel
my $excelBtn = $view->displayTextButton($locale->t('Export Excel'), 'common/ajaxTable/excel(|Over).png',
                                        'return true;',
                                        '',
                                        'l',
                                        {'class' => 'locCtrlButton', 'id' => 'excel-btn'});


print '
<div class="list">
    <div id="table-toolbar-container" class="toolbar header">
        <div class="col content">
            <div id="header-pagination-container"></div>
            <div class="fixed-content">
                <div id="header-filters-recap"></div>
            </div>
        </div>
        <div class="col controls">
            ' . $excelBtn . '
            <div class="fixed-content">
                <a href="#table-filters-container" id="back-top-btn" title="' . htmllocale('Retourner en haut de la page') . '">' . $view->displayImage('common/ajaxTable/up.png') . '</a>
            </div>
        </div>
    </div>
    <div class="content" id="table-container"></div>
</div>';


# Formatage de la liste
my $tabData = $tabViewData{'data'};
require($directory . '/list/format/list.cgi');

# Formatage des données de l'onglet Durée
&formatList($tabViewData{'user.country.id'}, $tabData, $locale, {});

# Paramètrage des colonnes
my @tabColumnsOptions = (
    {
        'id'         => 'agency.id',
        'title'      => $locale->t('Ag.'),
        'tip'        => $locale->t('Agence'),
        'cssClasses' => 'agency',
        'sort'       => 1
    },
    {
        'id'         => 'parkNumber',
        'title'      => $locale->t('N° parc'),
        'cssClasses' => 'number',
        'sort'       => 1
    },
    {
        'id'         => 'family.label',
        'title'      => $locale->t('Famille'),
        'cssClasses' => 'family',
        'sort'       => 1
    },
    {
        'id'         => 'model.label',
        'title'      => $locale->t('Mod. mach.'),
        'tip'        => $locale->t('Modèle de machine'),
        'cssClasses' => 'model',
        'sort'       => 1
    },
    {
        'id'         => 'manufacturer.label',
        'title'      => $locale->t('Fabricant'),
        'cssClasses' => 'manufacturer',
        'sort'       => 1
    },
    {
        'id'         => 'elevation',
        'title'      => $locale->t('Élév.'),
        'tip'        => $locale->t('Élévation'),
        'cssClasses' => 'elevation',
        'sort'       => 1
    },
    {
        'id'         => 'energy',
        'title'      => $locale->t('Énergie'),
        'cssClasses' => 'energy',
        'sort'       => 1
    },
    {
        'id'         => 'weight',
        'title'      => $locale->t('Poids'),
        'tip'        => $locale->t('Poids') . ' (' . $locale->t('kg') . ')',
        'cssClasses' => 'weight',
        'sort'       => 1
    },
    {
        'id'         => 'length',
        'title'      => $locale->t('Long.'),
        'tip'        => $locale->t('Longueur') . ' (' . $locale->t('m') . ')',
        'cssClasses' => 'length',
        'sort'       => 1
    },
    {
        'id'         => 'width',
        'title'      => $locale->t('Larg.'),
        'tip'        => $locale->t('Largeur') . ' (' . $locale->t('m') . ')',
        'cssClasses' => 'width',
        'sort'       => 1
    },
    {
        'id'         => 'height',
        'title'      => $locale->t('Haut.'),
        'tip'        => $locale->t('Hauteur repliée') . ' (' . $locale->t('m') . ')',
        'cssClasses' => 'height',
        'sort'       => 1
    },
    {
        'id'         => 'state.label',
        'title'      => $locale->t('État'),
        'cssClasses' => 'state',
        'sort'       => 1
    },
    {
        'id'         => 'isOutOfPark',
        'title'      => $locale->t('H.P.'),
        'tip'        => $locale->t('Hors parc'),
        'cssClasses' => 'park',
        'sort'       => 1
    },
    {
        'id'         => 'contract.code',
        'title'      => $locale->t('Dern. ctt.'),
        'tip'        => $locale->t('Dernier contrat'),
        'cssClasses' => 'contract',
        'sort'       => 1
    },
    {
        'id'         => 'control.nextDate',
        'title'      => $locale->t('Pr. con.'),
        'tip'        => $locale->t('Prochain contrôle'),
        'cssClasses' => 'control',
        'sort'       => 1
    }
);


# Paramètrage des filtres de recherche
my @tabFiltersOptions = (
    {
        'id' => 'isVisibleOnAgency',
    }
);

# Paramètrage du tableau
my %tabAjaxTableOptions = (
    'url'            => $view->createURL('machine:list:viewVisibility', {'displayType' => 'json'}),
    'cssClasses'     => 'basic list',
    'startIndex'     => $tabViewData{'startIndex'},
    'pageSize'       => $tabViewData{'pageSize'},
    'sorts'          => $tabViewData{'sorts'},
    'activesFilters' => $tabViewData{'activesFilters'},
    'defaultActivesFilters' => $tabViewData{'defaultActivesFilters'},
    'data'           => $tabData,
    'paginations'    => '#header-pagination-container',
    'filtersRecaps'  => '#header-filters-recap',
    'columns'        => \@tabColumnsOptions,
    'filters'        => \@tabFiltersOptions,
    'labels'         => {
        'pagination' => {
            'noResult' => $locale->t('Pas de résultat'),
            'result'   => $locale->t('%s résultat', '<b>1</b>'),
            'results'  => $locale->t('%s-%s sur %s résultats', '<b><%start></b>', '<b><%end></b>', '<b><%total></b>')
        },
        'clearSel' => $locale->t('Vider la sélection'),
        'noResult' => $locale->t('Aucune machine trouvée'),
        'errorMsg' => $locale->t('Une erreur s\'est produite.Veuillez contacter le service informatique s.v.p.')
    }
);

# Variables à passer au JS
print $view->displayJSBlock('
var isTelematActived = ' . $tabViewData{'isTelematActived'} . ';');

# Tri du tableau
print $view->displayJSBlock('
$(function()
{
    pageInit(' . &LOC::Json::toJson({
        'tableOptions' => \%tabAjaxTableOptions,
        'excelRowsLimit' => $tabViewData{'excelRowsLimit'}
    }) . ');
});');


# Affichage du pied de page
print $view->displayFooter();

