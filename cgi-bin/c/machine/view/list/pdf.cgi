use utf8;
use open (':encoding(UTF-8)');
binmode STDOUT, ":raw";    # Pour envoyer le flux en l'état

use strict;
use locale;

use LOC::Date;
use LOC::Locale;
use LOC::Pdf;
use LOC::Browser;

our %tabViewData;

# Locale
my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});

# Titre de la page
my @tabLocaltime = localtime();
my $title = $locale->t('Liste des machines');

# Création de l'objet PDF
my $pdf = LOC::Pdf->new($title, $tabViewData{'user.agency.id'}, \$locale, LOC::Pdf::ORIENTATION_PORTRAIT);
my $pdfObj = $pdf->getPdfObject();

# Polices
my $regularFont    = $pdf->{'font'}->{'regular'};
my $italicFont     = $pdf->{'font'}->{'italic'};
my $boldFont       = $pdf->{'font'}->{'bold'};
my $boldItalicFont = $pdf->{'font'}->{'bolditalic'};
my $dingbatsFont   = $pdfObj->corefont('ZapfDingbats');


# Dimensions de la page
my $margin = 10/mm;
my $pageWidth = $pdf->getPageWidth();
my $textWidth = $pageWidth - 2 * $margin;

my $pageHeight = $pdf->getPageHeight();
my $textHeight = $pageHeight - 2 * $margin;
my $pageLimit = $margin;

my $gap = 2/pt;

# Définition du sujet et des mots-clés
$pdf->setSubject($locale->t('Liste des machines'));
$pdf->setKeywords($locale->t('Liste des machines'));



# Nouvelle page
my $page = $pdf->addPage(LOC::Pdf::DISPLAY_TITLE | LOC::Pdf::DISPLAY_AGENCY | LOC::Pdf::DISPLAY_FULLFOOTER);
my $text = $page->text();
my $gfx  = $page->gfx();


# Etat et date de sortie
my $number = $locale->t('au %s', $locale->getDateFormat(undef, LOC::Locale::FORMAT_DATE_NUMERIC));
$gfx->textlabel(10/mm + 95/mm, $pageHeight - 35/mm - 16/pt * 1.2, $boldFont, 10/pt, $number);


# Références
my @tabReferences = (
    {'label' => $locale->t('Édité par'), 'value' => $tabViewData{'user.fullName'}}
);
$pdf->addCustomBlock(\$page, $locale->t('Références'), \@tabReferences, {'x' => 10/mm, 'y' => 55/mm});



my $x = $margin;
my $y = $pageHeight - 65/mm;

tie(my %tabColsList, 'Tie::IxHash');
%tabColsList = (
    'family.label'       => {'label' => $locale->t('Famille'),        'width' => 18/mm},
    'manufacturer.label' => {'label' => $locale->t('Marque'),         'width' => 17/mm},
    'model.label'        => {'label' => $locale->t('Désignation'),    'width' => 38/mm},
    'weight'             => {'label' => $locale->t('Poids'),          'width' => 12/mm},
    'elevation'          => {'label' => $locale->t('Élévation'),      'width' => 15/mm},
    'energy'             => {'label' => $locale->t('Énergie'),        'width' => 15/mm},
    'parkNumber'         => {'label' => $locale->t('N° parc'),        'width' => 15/mm},
    'serialNumber'       => {'label' => $locale->t('N° série'),       'width' => 25/mm},
    'state.label'        => {'label' => $locale->t('État'),           'width' => 17/mm},
    'crashDate'          => {'label' => $locale->t('Dern. panne'),    'width' => 18/mm}
);
# Affichage de l'entête du tableau
&printHeaderTabContent();

# Affichage des machines
foreach my $tabMachineInfos (@{$tabViewData{'tabMachinesList'}})
{
    $y = &displayTableLine($y, $tabMachineInfos);
    &checkRemainingSpace($y - $pageLimit - 5/mm, 'tabContent');
}

# Numéros de page
&displayPageNumbers();

# Sortie
my $pdfString = $pdf->getString();
$pdf->end();

my $filename = sprintf('ListeMachines_%04d-%02d-%02d.pdf', $tabLocaltime[5] + 1900,
                                                           $tabLocaltime[4] + 1,
                                                           $tabLocaltime[3]);
&LOC::Browser::sendHeaders(
        "Content-type: application/force-download;\n"
        . "Content-Disposition: attachment; filename=\"" . $filename . "\";\n\n"
    );

print $pdfString;


# Function: checkRemainingSpace
# Vérification de l'espace restant dans la page
#
# Parameters:
# string $value - Ordonnée ($y par défaut)
# string $origin - si tabContent, on répète l'entete du tableau des désignations
#
# Returns:
# bool - 1 si l'espace restant est suffisant, 0 sinon
sub checkRemainingSpace
{
    my ($value, $origin) = @_;

    unless (defined $value)
    {
        $value = $y;
    }

    if ($value < $pageLimit)
    {
        $page = $pdf->addPage(LOC::Pdf::DISPLAY_FULLHEADER);
        $gfx  = $page->gfx();
        $text = $page->text();

        $y = $pageHeight - 55/mm;

        if (defined($origin) && $origin eq 'tabContent')
        {
            &printHeaderTabContent();
        }
        return 1;
    }
    return 0;
}

# Function: printHeaderTabContent
# Affichage de la ligne d'entete du tableau des désignations
#
sub printHeaderTabContent
{
    $x  = $margin;
    
    $text->lead(10/pt);
    $y -= $text->lead() + 1/mm;
    
    $text->font($boldFont, 8/pt);
    $gfx->linewidth(1/pt);
    
    
    $text->translate($x, $y);
    foreach my $colId (keys(%tabColsList))
    {
        $text->text($tabColsList{$colId}->{'label'}, -maxWidth => $tabColsList{$colId}->{'width'} - $gap);
        $gfx->move($x, $y - 1/mm);
        $x += $tabColsList{$colId}->{'width'};
        $text->translate($x + $gap, $y);
        $gfx->line($x - $gap, $y - 1/mm);
    }
  
    $gfx->stroke();
    
    # Lignes du tableau
    $text->font($regularFont, 10/pt);
    $text->lead(10/pt);
    $y -= $text->lead() + 1/mm;

    return 1;
}

# Function: displayPageNumbers
# Affichage du numéro de page
sub displayPageNumbers
{
    my $nbPages = $pdfObj->pages();
    
    for (my $i = 0; $i < $nbPages; $i++)
    {
        my $page = $pdfObj->openpage($i + 1);
        my $text = $page->text();
        
        $text->font($italicFont, 8/pt);
        $text->translate($pageWidth - $margin, 1.5 * $margin);
        $text->text_right(sprintf('%d/%d', $i + 1, $nbPages));
    }
}

# Function: displayTableLine
# Affichage d'une ligne du tableau
#
# Parameters:
# string $value - Ordonnée ($y par défaut)
#
# Returns:
# bool - 1 si l'espace restant est suffisant, 0 sinon
sub displayTableLine
{
    my ($y, $tabInfos) = @_;

    $text->font($regularFont, 8/pt);
    $text->lead(10/pt);

    my $x = $margin;
    $text->translate($x, $y);
    
    foreach my $id (keys(%tabColsList))
    {
        my $value = $tabInfos->{$id};
        # Formatage pour le champ date
        if ($id eq 'crashDate')
        {
            $value = $locale->getDateFormat($value, LOC::Locale::FORMAT_DATE_NUMERIC);
        }
        $text->text($value, -maxWidth => $tabColsList{$id}->{'width'} - $gap);
        $x += $tabColsList{$id}->{'width'};
        $text->translate($x + $gap, $y);
    }

    $y -= $text->lead() + 1/mm;

    return $y;
}


