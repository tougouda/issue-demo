use utf8;
use open (':encoding(UTF-8)');

use strict;


use LOC::Html::Standard;
use LOC::Locale;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'machine/stock/';


our %tabViewData;
our %tabSearchData;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view   = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

# CSS
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'view.css');
# JS
$view->addJSSrc($dir . 'view.js');

# Affichage de l'entête
$view->setDisplay(LOC::Html::Standard::DISPFLG_NONE);
$view->setPageTitle($locale->t('Consultation du stock'));


# Bouton recherche machine
my $machineSearchBtn = $view->displayTextButton($locale->t('Recherche machines'), 'search(|Over).png',
                                        $view->createURL('machine:list:view',
                                            {'filters' => 
                                                {'country.id' => [$tabViewData{'country.id'}], 
                                                 'agency.id' => [$tabViewData{'agency.id'}],
                                                 'state.id' => [LOC::Machine::STATE_RENTED,
                                                               LOC::Machine::STATE_AVAILABLE,
                                                               LOC::Machine::STATE_RECOVERY,
                                                               LOC::Machine::STATE_REVIEW,
                                                               LOC::Machine::STATE_CONTROL,
                                                               LOC::Machine::STATE_BROKEN,
                                                               LOC::Machine::STATE_CHECK,
                                                               LOC::Machine::STATE_TRANSFER,
                                                               LOC::Machine::STATE_DELIVERY]
                                               }
                                            }),
                                        $locale->t('Accéder à la recherche machine'),
                                        '',
                                        {'target' => '_blank'});
# Bouton tous les modèles de machine disponibles
my $machineAvailableBtn = $view->displayTextButton($locale->t('Tous'), 'reload(|Over).png',
                                        $view->createURL('machine:stock:view',{'countryId' => $tabViewData{'country.id'}, 'agencyId' => $tabViewData{'agency.id'}}),
                                        $locale->t('Afficher tous les modèles de machines disponibles'));

# Bouton retour
my $returnBtn = $view->displayTextButton($locale->t('Retour'), 'standard/btnBack(|Over).gif','history.back()',
                                        $locale->t('Page précédente'));

# Bouton à afficher
my $activeButtons = '';
    if ($tabViewData{'search'} ne '' || $tabViewData{'model.id'} != 0)
    {
        $activeButtons = $machineSearchBtn . '<span>&#8195</span>' . $machineAvailableBtn;
    }
    else
    {
        $activeButtons = $returnBtn;
    }
    
# Critères à afficher
my $activeFilters = '<td class="label">' . $locale->t('Agence') . '</td>
                     <td>' . $tabViewData{'agency.label'} . '</td>';
if ($tabViewData{'search'} ne '')
{
    $activeFilters .= '<td class="label padding">' . $locale->t('Recherche') . '</td>
                                       <td>' . $tabViewData{'search'} . '</td>';
}
if ($tabViewData{'model.id'} != 0)
{
    $activeFilters .= '<td class="label padding">' . $locale->t('Modèle de machines') . '</td>
                                       <td>' . $tabViewData{'model.label'}  . '</td>';
}

# Message information
my $message = '';
if ($tabViewData{'nbResults'} > 0) 
{ 
    $message = $view->displayMessages('info', [$locale->t('Cliquez sur une ligne pour sélectionner un modèle de machines')], 0);
}
else
{
    $message = $view->displayMessages('warning', [$locale->t('Il n\'y a pas de machine en stock correspondant aux critères sélectionnés')], 0);
}


# Affichage de l'entête
$view->{'_networkOptimization'} = 'machine-stock-view';
print $view->displayHeader();
print $view->startForm();

# Affichage zone critères
print '
<fieldset id="tabFiltersFieldset">
    <legend>' . $locale->t('Critères') . '</legend>
    <div id="noFilterBtn">
    ' . $activeButtons . '
    </div>
    <table class="formTable" id="tabFilters">
        <tr>
            ' . $activeFilters . '
        </tr>
    </table>
</fieldset>
' . $message;

# Affichage des résultats
if ($tabViewData{'nbResults'} > 0)
{
    # Entête du tableau
    print '
    <table class="standard ex">
        <thead>
            <tr>
                <th>' . $locale->t('Modèle de machines') . '</th>
                <th>' . $locale->t('Famille de machines') . '</th>
                <th>' . $locale->t('Élévation') . '</th>
                <th>' . $locale->t('Énergie') . '</th>
                <th class="right">' . $locale->t('Poids') . '</th>
                <th class="right" title="' . $locale->t('Réservées / Réservables') . '">' . $locale->t('Rés.') . '</th>
                <th class="right"></th>
                <th class="right">' . $locale->t('Total') . '</th>
                <th class="right" title="' . $locale->t('En livraison') . '">' . $locale->t('Livr.') . '</th>
                <th class="right" title="' . $locale->t('Louées') . '">' . $locale->t('Lou.') . '</th>
                <th class="right" title="' . $locale->t('En transfert') . '">' . $locale->t('Trsf.') . '</th>
                <th class="right" title="' . $locale->t('En panne') . '">' . $locale->t('Pan.') . '</th>
                <th class="right" title="' . $locale->t('En contrôle') . '">' . $locale->t('Ctrl.') . '</th>
                <th class="right" title="' . $locale->t('En récupération') . '">' . $locale->t('Récup.') . '</th>
                <th class="right" title="' . $locale->t('En vérification') . '">' . $locale->t('Verif.') . '</th>
                <th class="right" title="' . $locale->t('En révision') . '">' . $locale->t('Rév.') . '</th>
                <th class="right" title="' . $locale->t('Disponibles') . '">' . $locale->t('Disp.') . '</th>
            </tr>
        </thead>
        <tbody>';

    # Lignes du tableau
    foreach my $id (keys(%{$tabViewData{'tabStock'}}))
    {
        my $tabStock = $tabViewData{'tabStock'}->{$id};
        
        my $nbVisibles = '';
        if ($tabStock->{'nbBookableFromOthersAgencies'} > 0)
        {
            $nbVisibles = '(' . $tabStock->{'nbBookableFromOthersAgencies'} . ')';
        }
        print '
             <tr onclick="setDocumentModelId(' . $tabStock->{'model.id'} . ');">
                <td>' . $tabStock->{'model.label'}. '</td> 
                <td>' . $tabStock->{'machinesFamily.label'}. '</td> 
                <td>' . $tabStock->{'machinesFamily.elevation'}. '</td> 
                <td>' . $tabStock->{'machinesFamily.energy'}. '</td> 
                <td class="right">' . $locale->getNumberFormat($tabStock->{'model.weight'}, 0) . '</td> 
                <td class="bold">' . $tabStock->{'nbBooked'} . ' / ' . $tabStock->{'nbBookable'} . '</td> 
                <td class="small" title="' . $locale->t('Machines issues d\'une autre agence') . '">' . $nbVisibles . '</td> 
                <td class="bold">' . $tabStock->{'nbTotal'} . '</td> 
                <td class="red">' . $tabStock->{'nbDelivery'} . '</td> 
                <td class="red">' . $tabStock->{'nbRented'} . '</td> 
                <td class="red">' . $tabStock->{'nbTransfer'} . '</td> 
                <td class="red">' . $tabStock->{'nbBroken'} . '</td> 
                <td class="red">' . $tabStock->{'nbControl'} . '</td> 
                <td class="green">' . $tabStock->{'nbRecovery'} . '</td> 
                <td class="green">' . $tabStock->{'nbCheck'} . '</td> 
                <td class="green">' . $tabStock->{'nbReview'} . '</td> 
                <td class="green">' . $tabStock->{'nbAvailable'} . '</td>
             </tr>
        ';
    }
print '
        </tbody>
    </table>';
}

# Affichage du pied de page
print $view->endForm();
print $view->displayFooter();

