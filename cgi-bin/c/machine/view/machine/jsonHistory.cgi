use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Json;

our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});


$tabViewData{'result'}->{'title'} = '';

# Dates et heures au format local
foreach my $data (@{$tabViewData{'result'}->{'tabData'}})
{
    $data->{'date'} = $locale->getDateFormat($data->{'datetime'}, LOC::Locale::FORMAT_DATE_NUMERIC);
    $data->{'time'} = $locale->getDateFormat($data->{'datetime'}, LOC::Locale::FORMAT_TIME_NUMERIC);
    delete($data->{'datetime'});
}


my $view = LOC::Json->new();
print $view->displayHeader();
print $view->encode($tabViewData{'result'});
