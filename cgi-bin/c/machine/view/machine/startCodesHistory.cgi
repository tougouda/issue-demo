use utf8;
use open (':encoding(UTF-8)');

use strict;

use Tie::IxHash;

use LOC::Html::Standard;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;

# Répertoire CSS/JS/Images
our $dir = 'machine/machine/';

our %tabViewData;


our $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
our $view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});


$view->setPageTitle($locale->t('Historique des codes de démarrage'));

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}

# CSS
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'startCodesHistory.css');

print $view->displayHeader();

# Popup d'historique
print '
<fieldset class="sub content">
    <legend>' . htmllocale('Historique des codes télématiques') . '</legend>
    <table class="standard">
        <thead>
            <tr>
                <th>' . htmllocale('Type') . '</th>
                <th>' . htmllocale('Code') . '</th>
                <th>' . htmllocale('Contrat') . '</th>
                <th>' . htmllocale('Dates') . '</th>
            </tr>
        </thead>
        <tbody>';

# Historique des codes
my $tabStartCodesHistory = $tabViewData{'list'};
foreach my $historyInfos (@$tabStartCodesHistory)
{
    my $contractUrl = undef;
    if ($historyInfos->{'contractId'})
    {
        my $url = $view->createURL('rent:rentContract:view', {'contractId' => $historyInfos->{'contractId'}});
        $contractUrl = '<a href="' . $url . '" target="_blank">' . $historyInfos->{'contractCode'} . '</a>';
    }

    print '
            <tr>
                <td>' . $historyInfos->{'type'} . '</td>
                <td>' . $historyInfos->{'code'} . '</td>
                <td>' . $contractUrl . '</td>
                <td>';

    # - Affichage des dates
    my $beginDate = $historyInfos->{'beginDate'} ? $locale->getDateFormat($historyInfos->{'beginDate'}, LOC::Locale::FORMAT_DATE_NUMERIC) : '';
    my $endDate   = $historyInfos->{'endDate'} ? $locale->getDateFormat($historyInfos->{'endDate'}, LOC::Locale::FORMAT_DATE_NUMERIC) : '';
    if ($beginDate ne '' || $endDate ne '')
    {
        print $beginDate . ' &rarr; ' . $endDate;
    }

    print '
                </td>
            </tr>';
}

print '
        </tbody>
    </table>
</fieldset>';

print $view->displayFooter();
