use utf8;
use open (':encoding(UTF-8)');


$view->addCSSSrc($dir . 'search.css');

# N° de parc
my $parkNoInput = $view->displayFormInputText('search.parkNo', $tabSearchData{'search.parkNo'}, '',
                                              {'size' => 5, 'tabindex' => $inputsTabIndex++});

my $tabLines = [
    [
        {'name' => $locale->t('N° de parc'), 'input' => $parkNoInput}
    ],
];


# Fonction de vidage des champs de recherche
$view->addJSSrc('
function goSearch()
{
    if (LOC_Common.addEltClass(\'searchBar\', \'search\'))
    {
        window.document.getElementsByName("searchBar.form")[0].submit();
        return true;
    }
    return false;
}
function clearSearch()
{
    $ge("search.parkNo").value = "";
}');


# Formulaire de recherche
my $searchUrl = $view->createURL('machine:machine:search');
my $searchBar = '
<form method="post" action="' . $searchUrl . '" onsubmit="return goSearch();" autocomplete="off" name="searchBar.form">' .
    $view->displaySearchBar('searchBar', $tabLines,
                            {'onsearch' => 'goSearch()',
                             'onclear'  => 'clearSearch()'}) . '
    <div class="hidden-submit"><input type="submit" tabindex="-1"/></div>
</form>';

$view->addControlsContent($searchBar, 'left', 1);

1;
