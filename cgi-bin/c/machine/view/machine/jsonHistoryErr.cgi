use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Json;
use LOC::Locale;

our %tabViewData;

my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});


# Libellé de l'erreur ou avertissement
if ($tabViewData{'errorCode'} eq 'emptysearch')
{
    $tabViewData{'result'}->{'title'} = $locale->t('Des paramètres sont manquants');
}
elsif ($tabViewData{'errorCode'} eq 'noresult')
{
    $tabViewData{'result'}->{'title'} = $locale->t('Il n\'y a pas de résultat');
}

my $view = LOC::Json->new();
print $view->displayHeader();
print $view->encode($tabViewData{'result'});
