use utf8;
use open (':encoding(UTF-8)');

# popup confirmation désattribution machine
my $classRedirect = '';
if (!$tabViewData{'sameAgency'})
{
    $classRedirect = 'disabled';
}
print '
    <div style="display:none;" id="deallocatePopup" class="confirmModal">
        <ul class="choices">
            <li class="deallocate">
                <div class="icon"></div>
                <div class="text">' . htmllocale('Désattribuer la machine du contrat et rester sur la fiche machine') . '</div>
                <div class="ctrls"><button type="button" class="deallocate"><span>' . htmllocale('Choisir') . '</span></button></div>
            </li>
            <li class="deallocate-redirect ' . $classRedirect . '">
                <div class="icon"></div>
                <div class="text">' . htmllocale('Désattribuer la machine du contrat et aller sur l\'interface d\'attribution de machines') . '</div>
                <div class="ctrls">';

if (!$tabViewData{'sameAgency'})
{
    print $view->displayHelp($locale->t('Impossible de rediriger vers l\'attribution de machines car l\'agence de connexion n\'est pas la même que l\'agence de la machine'));
}
else
{
    print '
                    <button type="button" class="deallocate-redirect"><span>' . htmllocale('Choisir') . '</span></button>';
}
print '
                </div>
            </li>
            <li class="none">
                <div class="icon"></div>
                <div class="text">' . htmllocale('Ne rien faire') . '</div>
                <div class="ctrls"><button type="button" class="none"><span>' . htmllocale('Choisir') . '</span></button></div>
            </li>
        </ul>
    </div>
';

print '
<fieldset>
    <legend>' . $locale->t('Informations commerciales') . '</legend>';


if ($tabViewData{'viewType'} eq VIEWTYPE_FULL)
{
    # Taux d'utilisation
    print '
    <fieldset class="sub">
        <legend>' . $locale->t('Taux d\'utilisation') . '</legend>';

    if ($tabViewData{'useRates'})
    {
        my $tabModelUseRates  = $tabViewData{'useRates'}->{'model'};
        my $tabFamilyUseRates = $tabViewData{'useRates'}->{'family'};

        my $modelAgencyUseRate  = $locale->getNumberFormat($tabModelUseRates->{'useRate'}->{'agency'} * 100, 2);
        my $modelAreaUseRate    = $locale->getNumberFormat($tabModelUseRates->{'useRate'}->{'area'} * 100, 2);
        my $modelCountryUseRate = $locale->getNumberFormat($tabModelUseRates->{'useRate'}->{'country'} * 100, 2);

        my $modelAgencyNbTotal  = $locale->getNumberFormat($tabModelUseRates->{'nbTotal'}->{'agency'});
        my $modelAreaNbTotal    = $locale->getNumberFormat($tabModelUseRates->{'nbTotal'}->{'area'});
        my $modelCountryNbTotal = $locale->getNumberFormat($tabModelUseRates->{'nbTotal'}->{'country'});

        my $familyAgencyUseRate  = $locale->getNumberFormat($tabFamilyUseRates->{'useRate'}->{'agency'} * 100, 2);
        my $familyAreaUseRate    = $locale->getNumberFormat($tabFamilyUseRates->{'useRate'}->{'area'} * 100, 2);
        my $familyCountryUseRate = $locale->getNumberFormat($tabFamilyUseRates->{'useRate'}->{'country'} * 100, 2);

        my $familyAgencyNbTotal  = $locale->getNumberFormat($tabFamilyUseRates->{'nbTotal'}->{'agency'});
        my $familyAreaNbTotal    = $locale->getNumberFormat($tabFamilyUseRates->{'nbTotal'}->{'area'});
        my $familyCountryNbTotal = $locale->getNumberFormat($tabFamilyUseRates->{'nbTotal'}->{'country'});

        my $searchModelUrl        = $view->createURL($searchUrl, {'filters' => {
                                                                        'model.id' => [$tabInfos->{'model.id'}],
                                                                        %tabBaseSearchParams
                                                                    }});
        my $searchTariffFamilyUrl = $view->createURL($searchUrl, {'filters' => {
                                                                        'tariffFamily.id' => [$tabInfos->{'tariffFamily.id'}],
                                                                        %tabBaseSearchParams
                                                                    }});

        print '
        <table class="utilization">
            <tbody>
                <tr>
                    <td></td>
                    <td colspan="2">
                        <a href="' . $searchModelUrl . '"' . $linkTarget . ' title="' . $locale->t('Modèle de machines') . '">' . $tabInfos->{'model.label'} . '</a>
                    </td>
                    <td colspan="2">
                        <a href="' . $searchTariffFamilyUrl . '"' . $linkTarget . ' title="' . $locale->t('Famille tarifaire') . '">' . $tabInfos->{'tariffFamily.label'} . '</a>
                    </td>
                </tr>
                <tr>
                    <td class="agency" title="' . $tabInfos->{'agency.label'} . '"></td>
                    <td class="percent">' . $locale->t('%.2f %%', $modelAgencyUseRate) . '</td>
                    <td class="number">(' . $modelAgencyNbTotal . ')</td>
                    <td class="percent">' . $locale->t('%.2f %%', $familyAgencyUseRate) . '</td>
                    <td class="number">(' . $familyAgencyNbTotal . ')</td>
                </tr>
                <tr>
                    <td class="area" title="' . $tabInfos->{'area.label'} . '"></td>
                    <td class="percent">' . $locale->t('%.2f %%', $modelAreaUseRate) . '</td>
                    <td class="number">(' . $modelAreaNbTotal . ')</td>
                    <td class="percent">' . $locale->t('%.2f %%', $familyAreaUseRate) . '</td>
                    <td class="number">(' . $familyAreaNbTotal . ')</td>
                </tr>
                <tr>
                    <td class="country ' . lc($tabInfos->{'country.id'}) . '" title="' . $tabInfos->{'country.label'} . '"></td>
                    <td class="percent">' . $locale->t('%.2f %%', $modelCountryUseRate) . '</td>
                    <td class="number">(' . $modelCountryNbTotal . ')</td>
                    <td class="percent">' . $locale->t('%.2f %%', $familyCountryUseRate) . '</td>
                    <td class="number">(' . $familyCountryNbTotal . ')</td>
                </tr>
            </tbody>
        </table>';
    }
    else
    {
        print $locale->t('Informations non disponibles.');
    }

    print '
    </fieldset>';
}


# Historique des contrats
my $nbContracts = (keys(%{$tabViewData{'contracts'}}));
if ($tabViewData{'viewType'} eq VIEWTYPE_LIGHT)
{
    $nbContracts = &LOC::Util::round($nbContracts / 2, 0);
}

print '
    <fieldset class="sub">
        <legend>' . $locale->t('Historique des contrats') . '</legend>';

if ($nbContracts)
{
    my $label = $locale->t('%d derniers contrats portant sur cette machine :', $nbContracts);
    if ($nbContracts == 1)
    {
        $label = $locale->t('Contrat portant sur cette machine :');
    }

    print '
        <div class="lastContractsLabel">' . $label . '</div>
        <table class="standard">
            <thead>
                <tr>
                    <th>' . $locale->t('Contrat') . '</th>
                    <th>' . $locale->t('Client') . '</th>
                    <th colspan="3">' . $locale->t('Période de location') . '</th>
                    <th style="text-align: right" title="' . $locale->t('Durée de location (j)') . '">' . $locale->t('Durée') . '</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>';

    my $i = 0;
    foreach my $contractId (keys(%{$tabViewData{'contracts'}}))
    {
        if ($i >= $nbContracts)
        {
            last;
        }

        my $contract = $tabViewData{'contracts'}->{$contractId};

        my @tabClass = ();
        my @tabTooltip = ();
        my $deallocateBtn;
        if (!$contract->{'isAffected'})
        {
            push(@tabClass, 'deallocated');
            push(@tabTooltip, $locale->t('La machine n\'est plus affectée à ce contrat'));
        }
        if ($contract->{'isCurrent'})
        {
            push(@tabClass, 'current');
            push(@tabTooltip, $locale->t('La machine est en cours de location sur ce contrat'));
        }
        my $classname = join(' ', @tabClass);
        my $tooltip = '';
        if (@tabTooltip > 0)
        {
            $tooltip = $view->displayHelp(join('<br />', @tabTooltip));
        }

        if ($tabViewData{'viewType'} eq VIEWTYPE_FULL &&
                $contract->{'isUnaffectPossible'} &&
                $tabInfos->{'tabRights'}->{'actions'}->{'deallocate'})
        {
            $deallocateBtn = $view->displayTextButton('','machine/view/deallocateIcon.png',
                                                    'return false;',
                                                    $locale->t('Désattribuer la machine'),
                                                    'left',
                                                    {'id' => 'deallocateBtn',
                                                     'data-value'            => $contract->{'id'},
                                                     'data-modificationDate' => $tabInfos->{'modificationDate'}
                                                    });
        }

        # Liens pour le contrat et le client
        my $contractLink = $contract->{'code'};
        my $customerLink = $contract->{'customer.name'};
        if ($contract->{'country.id'} eq $tabViewData{'country.id'})
        {
            my $contractUrl = $view->createURL('rent:rentContract:view', {'contractId' => $contractId});
            $contractLink = '<a href="' . $contractUrl . '" target="_blank">' . $contract->{'code'} . '</a>';

            my $customerUrl = sprintf($tabViewData{'customerUrl'}, $contract->{'customer.id'});
            $customerLink = '<a href="' . $customerUrl . '" target="_blank">' . $contract->{'customer.name'} . '</a>';
        }

        print '
            <tr' . ($classname eq '' ? '' : ' class="' . $classname . '"') . '>
                <td>' . $contractLink . '</td>
                <td>' . $customerLink . '</td>
                <td style="text-align: right">' . $locale->getDateFormat($contract->{'beginDate'}, LOC::Locale::FORMAT_DATE_NUMERIC) . '</td>
                <td>&rarr;</td>
                <td style="text-align: left">' . $locale->getDateFormat($contract->{'endDate'}, LOC::Locale::FORMAT_DATE_NUMERIC) . '</td>
                <td style="text-align: right">' . $locale->getNumberFormat($contract->{'duration'}) . '</td>
                <td>' . $tooltip . '</td>
                <td>' . $deallocateBtn . '</td>
            </tr>';

        $i++;
    }
    print '
            </tbody>
        </table>';
}
else
{
    print $locale->t('Cette machine n\'a jamais été louée');
}

print '
    </fieldset>';


print '
</fieldset>';

1;