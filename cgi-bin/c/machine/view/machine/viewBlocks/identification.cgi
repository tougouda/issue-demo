use utf8;
use open (':encoding(UTF-8)');

print '
<fieldset id="identification">
    <legend>' . $locale->t('Identification') . '</legend>';


my $searchFamilyUrl       = $view->createURL($searchUrl, {'filters' => {
                                                                'family.id' => [$tabInfos->{'family.id'}],
                                                                %tabBaseSearchParams
                                                            }});
my $searchModelUrl        = $view->createURL($searchUrl, {'filters' => {
                                                                'model.id' => [$tabInfos->{'model.id'}],
                                                                %tabBaseSearchParams
                                                            }});
my $searchTariffFamilyUrl = $view->createURL($searchUrl, {'filters' => {
                                                                'tariffFamily.id' => [$tabInfos->{'tariffFamily.id'}],
                                                                %tabBaseSearchParams
                                                            }});

# Identifiant interne
if ($tabViewData{'isAdmin'} && $tabViewData{'viewType'} ne VIEWTYPE_LIGHT)
{
    print '

    <div class="id">
        <table class="formTable">
            <tbody>
                <tr>
                    <td class="label">' . $locale->t('Identifiant interne') . '</td>
                    <td>' . $tabInfos->{'id'} . '</td>
                </tr>
            </tbody>
        </table>
    </div>';
}

# Nom complet et familles
print '

    <div class="name">' . $tabInfos->{'model.fullName'} . '</div>

    <div class="general">
        <div class="tree">
            <div>
                <a href="' . $searchFamilyUrl . '"' . $linkTarget . ' title="' . $locale->t('Famille') . '">' . $tabInfos->{'family.fullName'} . '</a>
                <div>
                    <span title="' . $locale->t('Famille commerciale') . '">' . $tabInfos->{'businessFamily.label'} . '</span>
                    <div>
                        <span title="' . $locale->t('Famille de machines') . '">' . $tabInfos->{'machinesFamily.label'} . '</span>
                        <div>
                            <a href="' . $searchModelUrl . '"' . $linkTarget . ' title="' . $locale->t('Modèle de machines') . '">' . $tabInfos->{'model.label'} . '</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <table class="formTable">
            <tbody>
                <tr>
                    <td class="label">' . $locale->t('Famille comptable') . '</td>
                    <td>' . $tabInfos->{'accountingFamily.label'} . '</td>
                </tr>
                <tr>
                    <td class="label">' . $locale->t('Famille tarifaire') . '</td>
                    <td>
                        <a href="' . $searchTariffFamilyUrl . '"' . $linkTarget . '>' . $tabInfos->{'tariffFamily.label'} . '</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>';


# Dimensions et énergie
# Formatage des dimensions
my $width = '?';
if ($tabInfos->{'model.width'} ne '')
{
    $width = $locale->t('%s m', $locale->getNumberFormat($tabInfos->{'model.width'}));
}
my $length = '?';
if ($tabInfos->{'model.length'} ne '')
{
    $length = $locale->t('%s m', $locale->getNumberFormat($tabInfos->{'model.length'}));
}
my $height = '?';
if ($tabInfos->{'model.height'} ne '')
{
    $height = $locale->t('%s m', $locale->getNumberFormat($tabInfos->{'model.height'}));
}
# Formatage de l'énergie
my %tabEnergyClass = ('DIESEL' => 'fuel', 'ELECT' => 'electric', 'BI-EN' => 'bi', 'GAZ' => 'gas');
my $energyClass = 'energy';
if (grep($tabInfos->{'machinesFamily.energy'}, keys(%tabEnergyClass)))
{
    $energyClass .= ' ' . $tabEnergyClass{$tabInfos->{'machinesFamily.energy'}};
}

my $searchElevationUrl = $view->createURL($searchUrl, {'filters' => {
                                                            'elevation' => [$tabInfos->{'machinesFamily.elevation'}],
                                                            %tabBaseSearchParams
                                                        }});
my $searchEnergyUrl    = $view->createURL($searchUrl, {'filters' => {
                                                            'energy' => [$tabInfos->{'machinesFamily.energy'}],
                                                            %tabBaseSearchParams
                                                        }});

print '
    <div class="general">
        <div class="elevation" title="' . $locale->t('Élévation') . '">
            <a href="' . $searchElevationUrl . '"' . $linkTarget . '>' . $tabInfos->{'machinesFamily.elevation'} . '</a>
        </div>
        <div class="' . $energyClass. '" title="' . $locale->t('Énergie') . '">
            <a href="' . $searchEnergyUrl . '"' . $linkTarget . '>' . $tabInfos->{'machinesFamily.energy'} . '</a>
        </div>
        <table class="dimensions">
            <tbody>
                <tr>
                    <td class="height" title="' . $locale->t('Hauteur repliée') . '">' . $height . '</td>
                    <td class="width" title="' . $locale->t('Largeur') . '">' . $width . '</td>
                </tr>
                <tr>
                    <td></td>
                    <td class="length" title="' . $locale->t('Longueur') . '">' . $length . '</td>
                </tr>
            </tbody>
        </table>
    </div>';

# Fabricant, poids, capacité
# Formatage du poids
my $weight = '?';
if ($tabInfos->{'model.weight'} ne '')
{
    $weight = $locale->t('%s kg', $locale->getNumberFormat($tabInfos->{'model.weight'}));
}

my $searchManufacturerUrl = $view->createURL($searchUrl, {'filters' => {
                                                            'manufacturer.id' => [$tabInfos->{'manufacturer.id'}],
                                                            %tabBaseSearchParams
                                                        }});

# Date d'achat, date de prochain contrôle, numéro de série
# Formatage de la date d'achat
my $purchaseDate = $locale->t('Pas d\'information disponible');
if ($tabInfos->{'purchaseDate'} ne '' && $tabInfos->{'purchaseDate'} ne '0000-00-00')
{
    $purchaseDate = $locale->getDateFormat($tabInfos->{'purchaseDate'}, LOC::Locale::FORMAT_DATE_NUMERIC);
}
# Formatage de la date de prochain contrôle
my $nextControlDate = $locale->t('Pas d\'information disponible');
my $controlScore = '';
if ($tabInfos->{'control.nextDate'} ne '' && $tabInfos->{'control.nextDate'} ne '0000-00-00')
{
    $nextControlDate = $locale->getDateFormat($tabInfos->{'control.nextDate'}, LOC::Locale::FORMAT_DATE_NUMERIC);
    $controlScore = $view->displayScore($tabInfos->{'control.score'}, 4);
}

# Tableau sur les filtres des équipements
my %tabFiltersAddInfos = (
    'GIES'      => 'isWithSecuritySystem',
    'STABI'     => 'isWithStabilizers',
    'TELEMAT'   => 'isWithTelematic'
);
my $searchAddInfosUrl;

print '
    <div class="general">
            <table class="addInfos">
                <tbody>
                    <tr>';
foreach my $addInfoId (sort(keys(%{$tabInfos->{'tabAddInfos'}})))
{
    # Valeur de l'information additionnelle
    my $addInfoValue = $tabInfos->{'tabAddInfos'}->{$addInfoId};

    # Code externe
    my $code = $tabViewData{'tabAddInfos'}->{$addInfoId}->{'externalCode'};

    # Affichage du libellé de l'information additionnelle
    if (defined $tabFiltersAddInfos{$code})
    {
        my $tabUrlFilters = {
            'filters' => {
                $tabFiltersAddInfos{$code} => [$addInfoValue],
                %tabBaseSearchParams
            }
        };
        $searchAddInfosUrl = $view->createURL($searchUrl, $tabUrlFilters);
        print '
                        <td class="label">
                            <a href="' . $searchAddInfosUrl . '"' . $linkTarget . '>' . $tabViewData{'tabAddInfos'}->{$addInfoId}->{'name'} . '</a>
                        </td>';
    }
    else
    {
        print '
                        <td class="label">' . $tabViewData{'tabAddInfos'}->{$addInfoId}->{'name'} . '</td>';
    }

    # Type externe
    my $type = $tabViewData{'tabAddInfos'}->{$addInfoId}->{'externalType'};

    # Affichage de la valeur de l'information additionnelle
    if ($type eq LOC::Kimoce::CHARTYPE_BOOLEAN)
    {
        my $addInfoTooltip = (defined $addInfoValue && $addInfoValue == 0 ? $locale->t('Non') : ($addInfoValue == 1 ? $locale->t('Oui') : $locale->t('Non renseigné')));
        print '
                        <td class="value" title="' . $addInfoTooltip . '">' . $view->displayBoolean($addInfoValue, '') . '</td>';
    }
    else
    {
        print '
                        <td class="value">' . $addInfoValue . '</td>';
    }
}
print '
                    </tr>
                </tbody>
            </table>
            <div class="info">
            <table class="formTable machineInfo">
                <tbody>
                    <tr>
                        <td class="label">' . $locale->t('Fabricant') . '</td>
                        <td>
                            <a href="' . $searchManufacturerUrl . '"' . $linkTarget . '>' . $tabInfos->{'manufacturer.label'} . '</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">' . $locale->t('Poids') . '</td>
                        <td>' . $weight . '</td>
                    </tr>
                    <tr>
                        <td class="label">' . $locale->t('Capacité') . '</td>
                        <td>' . $locale->t('%s kg', $locale->getNumberFormat($tabInfos->{'model.capacity'})) . '</td>
                    </tr>
                </tbody>
            </table>
            <table class="formTable machineInfo">
                <tbody>
                    <tr>
                        <td class="label">' . $locale->t('Date d\'achat') . '</td>
                        <td colspan="2">' . $purchaseDate . '</td>
                    </tr>
                    <tr class="nextControl">
                        <td class="label">' . $locale->t('Prochain contrôle') . '</td>
                        <td> ' . $nextControlDate . '</td>
                        <td> ' . $controlScore . ' </td>
                    </tr>
                    <tr>
                        <td class="label">' . $locale->t('Numéro de série') . '</td>
                        <td colspan="2">' . $tabInfos->{'serialNumber'} . '</td>
                    </tr>
                </tbody>
            </table>
            </div>
    </div>
';

print '
</fieldset>';

1;