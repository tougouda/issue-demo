use utf8;
use open (':encoding(UTF-8)');

# Gestion du scroll sur la page
print $view->displayJSBlock('
var scrollFunc = function()
{
    if ($("#locStandardContent").scrollTop() >= 25)
    {
        $("#headband").width($("#identification").outerWidth() + 10);
        $("html > body").addClass("headband-fixed");
    }
    else
    {
        $("#headband").width("");
        $("html > body").removeClass("headband-fixed");
    }
};

$("#locStandardContent").scroll(scrollFunc);
$(window).resize(scrollFunc);
');

print '
<div id="headband" class="identification">
    <table>
        <tbody>
            <tr>
                <td class="left">';

# Lien vers la fiche machine complète lorsqu'on est pas en mode "full"
print $view->displayParkNumber($tabInfos->{'parkNumber'}, undef, ($tabViewData{'viewType'} ne VIEWTYPE_FULL), VIEWTYPE_FULL);

my %tabFilters = %tabBaseSearchParams;
$tabFilters{'agency.id'} = [$tabInfos->{'agency.id'}];

my $searchAgencyUrl = $view->createURL($searchUrl, {'filters' => \%tabFilters});

print '
                    <div class="agency">
                        <a href="' . $searchAgencyUrl . '"' . $linkTarget . '>' . $tabInfos->{'agency.id'} . '</a>';

if ($tabInfos->{'visibleOnAgency.id'} ne '')
{
    my %tabFilters = %tabBaseSearchParams;
    $tabFilters{'agency.id'} = [$tabInfos->{'visibleOnAgency.id'}];

    my $searchVisibleAgencyUrl = $view->createURL($searchUrl, {'filters' => \%tabFilters});

    print '&nbsp;&gt;&nbsp;<a href="' . $searchVisibleAgencyUrl . '"' . $linkTarget . '>' .
           $tabInfos->{'visibleOnAgency.id'} . '</a>';
}

print '
                    </div>
                </td>
                <td class="right">';

# Machine sur contrat amiante
if ($tabInfos->{'isAsbestos'})
{
    my $title = $locale->t('Présence d\'amiante sur le chantier');

    print '
                    <div class="siteAsbestos" title="' . $title . '">&nbsp;</div>';
}

# Machine bloquée sur chantier
if ($tabInfos->{'isSiteBlocked'})
{
    my $title = $locale->t('Bloquée sur chantier');
    if ($tabViewData{'viewType'} eq VIEWTYPE_LIGHT)
    {
        $title .= ' : ' . $tabInfos->{'siteBlockedComment'};
    }
    print '
                    <div class="siteBlocked" title="' . $title . '">&nbsp;</div>';
}

# Machine hors parc
if ($tabInfos->{'isOutOfPark'})
{
    my $searchOutOfParkUrl = $view->createURL($searchUrl, {'filters' => {
                                                                'isOutOfPark' => [$tabInfos->{'isOutOfPark'} ? 1 : 0],
                                                                %tabBaseSearchParams
                                                            }});

    print '
                    <div class="outOfPark" title="' . $locale->t('Hors parc') . '">
                        <a href="' . $searchOutOfParkUrl . '"' . $linkTarget . '>HP</a>
                    </div>';
}

my $searchStateUrl = $view->createURL($searchUrl, {'filters' => {
                                                        'state.id' => [$tabInfos->{'state.id'}],
                                                        %tabBaseSearchParams
                                                    }});

print '
                    <div class="state ' . $tabInfos->{'state.id'} . '">
                        <a href="' . $searchStateUrl . '"' . $linkTarget . '>' . $tabInfos->{'state.label'} . '</a>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>';

1;