use utf8;
use open (':encoding(UTF-8)');

if ($tabInfos->{'tabRights'}->{'actions'}->{'control'} ||
    $tabInfos->{'tabRights'}->{'actions'}->{'siteBlocked'} ||
    $tabInfos->{'tabRights'}->{'actions'}->{'outOfPark'} ||
    $tabInfos->{'tabRights'}->{'actions'}->{'state'} ||
    $tabInfos->{'tabRights'}->{'actions'}->{'createTransfer'} ||
    $tabInfos->{'tabRights'}->{'actions'}->{'doTransfer'})
{
    print $view->startForm(undef, undef, 'actionsForm');

    print '
<fieldset>
    <legend>' . htmllocale('Actions') . '</legend>';

    my $displayValidBtn = 0;


    # Date de dernier contrôle
    if ($tabInfos->{'tabRights'}->{'actions'}->{'control'})
    {
        my %tabAttributes = ('possible' => $tabInfos->{'possibilities'}->{'actions'}->{'control'} * 1);
        if (!$tabInfos->{'possibilities'}->{'actions'}->{'control'})
        {
            $tabAttributes{'disabled'} = 1;
        }
        else
        {
            $displayValidBtn = 1;
        }

        print '
    <fieldset class="sub">
        <legend>' . htmllocale('Visite générale périodique') . '</legend>';

        my $lastKnownDateMessage;
        my $controlPrintBtns;
        if ($tabInfos->{'isControlVgp'})
        {
            print '
            ' . $locale->t('Dernier contrôle effectué le %s',
                       $view->displayFormInputDate('lastControlDate', undef, undef, undef, \%tabAttributes));

            if ($tabInfos->{'control.lastDate'})
            {
                $lastKnownDateMessage = $locale->t('Dernière date connue : le %s',
                                                   $locale->getDateFormat($tabInfos->{'control.lastDate'},
                                                                          LOC::Locale::FORMAT_DATE_NUMERIC));
            }
            else
            {
                $lastKnownDateMessage = htmllocale('Pas de dernière date connue');
            }

            # si une date de contrôle existe pour la machine
            if ($tabInfos->{'control.lastDate'} && $tabInfos->{'tabControlOrderInfos'}->{'id'})
            {
                # Boutons d'impression du bon de commande VGP
                my $pdfUrl = {'url' => 'technical:controlOrder:pdf', 'options' => {'machinesIds' => [$tabInfos->{'id'}]}};
                my $pdfName = $locale->t('AI_VGP_%s.pdf', $tabInfos->{'parkNumber'});

                $controlPrintBtns = $locale->t('Dernier bon de commande') . ' : ' .
                                        $view->displayPrintButtons('printBtns',
                                               $pdfUrl,
                                               $pdfName,
                                               undef,
                                               $locale->t('Bon de commande VGP'),
                                               undef,
                                               $tabInfos->{'agency.id'},
                                               {
                                                   'buttons'     => ['pdf', 'print'],
                                                   'tabEndActions' => [
                                                       {
                                                           'type'      => 'logPrint',
                                                           'entity'    => 'VGP',
                                                           'entityIds' => [$tabInfos->{'id'}]
                                                       }
                                                   ]
                                               });
            }
        }
        else
        {
            $lastKnownDateMessage = htmllocale('Ce modèle de machines ne fait pas l\'objet d\'une VGP');
        }

        print '
        <div id="lastKnownControlDate">
            ' . $lastKnownDateMessage . '
        </div>

        <div id="control-print-btns">
            ' . $controlPrintBtns . '
        </div>';

       print '
    </fieldset>';
    }

    # Machine bloquée sur chantier
    if ($tabInfos->{'tabRights'}->{'actions'}->{'siteBlocked'})
    {
        my %tabChbAttributes = ('possible' => $tabInfos->{'possibilities'}->{'actions'}->{'siteBlocked'} * 1);
        if (!$tabInfos->{'possibilities'}->{'actions'}->{'siteBlocked'})
        {
            $tabChbAttributes{'disabled'} = 1;
        }
        else
        {
            $displayValidBtn = 1;
        }
        my %tabTextAttributes = %tabChbAttributes;
        $tabChbAttributes{'initialvalue'} = $tabInfos->{'isSiteBlocked'} * 1;
        $tabTextAttributes{'disabled'}    = !$tabInfos->{'isSiteBlocked'} * 1;

        my $checkbox = $view->displayFormInputCheckBox('isSiteBlocked', $tabInfos->{'isSiteBlocked'},
                                                       \%tabChbAttributes, htmllocale('Bloquée sur chantier'), 1, 1);
        my $textarea = $view->displayFormTextArea('siteBlockedComment', $tabInfos->{'siteBlockedComment'} . '',
                                                  {'cols' => 30, 'rows' => 4, %tabTextAttributes});

        print '

    <fieldset class="sub">
        <legend>' . htmllocale('Chantier') . '</legend>
        ' . $checkbox . '
        <br />
        ' . $textarea . '
    </fieldset>';
    }

    # Gestion
    if ($tabInfos->{'tabRights'}->{'actions'}->{'outOfPark'} ||
        $tabInfos->{'tabRights'}->{'actions'}->{'state'} ||
        $tabInfos->{'tabRights'}->{'actions'}->{'createTransfer'} ||
        $tabInfos->{'tabRights'}->{'actions'}->{'doTransfer'})
    {
        print '

    <fieldset class="sub">
        <legend>' . htmllocale('Gestion') . '</legend>';

        # Hors parc
        if ($tabInfos->{'tabRights'}->{'actions'}->{'outOfPark'})
        {
            my %tabAttributes = (
                    'possible'     => $tabInfos->{'possibilities'}->{'actions'}->{'outOfPark'} * 1,
                    'initialvalue' => $tabInfos->{'isOutOfPark'} * 1
                );
            if (!$tabInfos->{'possibilities'}->{'actions'}->{'outOfPark'})
            {
                $tabAttributes{'disabled'} = 1;
            }
            else
            {
                $displayValidBtn = 1;
            }

            my $checkbox = $view->displayFormInputCheckBox('isOutOfPark', (!$tabInfos->{'isOutOfPark'} ? 0 : 1),
                                                           \%tabAttributes, htmllocale('Hors parc'), 1, 1);

            print '
        ' . $checkbox . '
        <br />';
        }

        # Changement d'état
        if ($tabInfos->{'tabRights'}->{'actions'}->{'state'})
        {
            tie(my %tabStates, 'Tie::IxHash');
            %tabStates = ('' => '-- ' . htmllocale('Sélectionner') . ' --', %{$tabViewData{'states'}});

            my %tabAttributes = ('possible' => $tabInfos->{'possibilities'}->{'actions'}->{'state'} * 1);
            if (!$tabInfos->{'possibilities'}->{'actions'}->{'state'})
            {
                $tabAttributes{'disabled'} = 1;
            }
            else
            {
                $displayValidBtn = 1;
            }

            my $select = $view->displayFormSelect('state.id', \%tabStates, undef, undef, \%tabAttributes);

            print '
        ' . $locale->t('Passer la machine à l\'état %s', $select) . '
        <br />';
        }

        # Visibilité sur une autre agence
        if ($tabInfos->{'tabRights'}->{'actions'}->{'visibility'})
        {
            tie(my %tabAgencies, 'Tie::IxHash');
            %tabAgencies = (
                '' => '-- ' . htmllocale('Sélectionner') . ' --',
                %{$tabViewData{'visibilityAgencies'}}
            );

            my $isVisibleOnAgency = ($tabInfos->{'visibilityMode'} == LOC::Machine::VISIBILITYMODE_TRANSPORT
                                        ? 0 : ($tabInfos->{'visibleOnAgency.id'} ? 1 : 0));
            my $visibleOnAgencyId = ($tabInfos->{'visibilityMode'} == LOC::Machine::VISIBILITYMODE_TRANSPORT
                                        ? undef : $tabInfos->{'visibleOnAgency.id'});

            my $noResult;
            # - si un transfert inter-agences est en cours
            if (defined $tabInfos->{'agenciesTransfer.tabInfos'})
            {
                my $tabTransfer = $tabInfos->{'agenciesTransfer.tabInfos'};
                my $isDone = ($tabTransfer->{'loading'}->{'isDone'} ||
                              $tabTransfer->{'unloading'}->{'isDone'});
                # on restreint les choix pour l'agence de visibilité
                if (!$isDone)
                {
                    %tabAgencies = (
                        '' => '-- ' . htmllocale('Sélectionner') . ' --',
                        ($tabTransfer->{'to.id'} => $tabAgencies{$tabTransfer->{'to.id'}})
                    );
                    if (!$tabAgencies{$tabTransfer->{'to.id'}})
                    {
                        $noResult = 1;
                    }
                }
            }

            my %tabBaseAttributes = ('possible' => $tabInfos->{'possibilities'}->{'actions'}->{'visibility'} * 1);
            if (!$tabInfos->{'possibilities'}->{'actions'}->{'visibility'} || $noResult)
            {
                $tabBaseAttributes{'disabled'} = 1;
            }
            else
            {
                $displayValidBtn = 1;
            }

            my %tabAttributes = %tabBaseAttributes;
            $tabAttributes{'disabled'}     = (!$isVisibleOnAgency || $tabBaseAttributes{'disabled'} == 1);
            $tabAttributes{'initialvalue'} = $visibleOnAgencyId;
            $tabAttributes{'onclick'} = 'return false;'; # A ne pas sortir pour IE < 11
            my $visibleOnAgSelect  = $view->displayFormSelect('visibleOnAgency.id', \%tabAgencies, $visibleOnAgencyId,
                                                             undef, \%tabAttributes);

            my %tabAttributes = %tabBaseAttributes;
            $tabAttributes{'initialvalue'} = $isVisibleOnAgency;
            my $isVisibleOnAgencyChb = $view->displayFormInputCheckBox('isVisibleOnAgency', $isVisibleOnAgency,
                                                           \%tabAttributes,
                                                           $locale->t('Visible sur l\'agence de %s', $visibleOnAgSelect),
                                                           1, 1);
            my $isVisibleOnAgencyHlp = $view->displayHelp($locale->t('La machine apparaîtra dans les stocks de l\'agence sélectionnée'));
            print '
        ' . $isVisibleOnAgencyChb . ' ' . $isVisibleOnAgencyHlp . '
        <br />';
        }

        # Transfert inter-agences
        if ($tabInfos->{'tabRights'}->{'actions'}->{'doTransfer'} ||
            $tabInfos->{'tabRights'}->{'actions'}->{'cancelTransfer'})
        {
            if ($tabInfos->{'possibilities'}->{'actions'}->{'doTransfer'})
            {
                # Notifications possibles
                $view->addNotifications([
                    # Réalisation en attente de validation
                    {
                        'id'      => 'doTransfer',
                        'type'    => 'warning',
                        'message' => $locale->t('Vous souhaitez réaliser le transfert inter-agences. Votre demande sera enregistrée lorsque vous validerez.'),
                        'buttons' => [{
                                       'title'   => $locale->t('Valider'),
                                       'onclick' => 'machine.valid()'
                                      },
                                      {
                                       'title'   => $locale->t('Annuler'),
                                       'onclick' => 'machine.cancelDo()'
                                      }]
                    },
                    # Annulation de réalisation en attente de validation
                    {
                        'id'      => 'undoTransfer',
                        'type'    => 'warning',
                        'message' => $locale->t('Vous souhaitez annuler la réalisation du transfert inter-agences. Votre demande sera enregistrée lorsque vous validerez.'),
                        'buttons' => [{
                                       'title'   => $locale->t('Valider'),
                                       'onclick' => 'machine.valid()'
                                      },
                                      {
                                       'title'   => $locale->t('Annuler'),
                                       'onclick' => 'machine.cancelUndo()'
                                      }]
                    }
                ]);

                $displayValidBtn = 1;
            }
            if ($tabInfos->{'possibilities'}->{'actions'}->{'cancelTransfer'})
            {
                # Notifications possibles
                $view->addNotifications([
                    # Annulation en attente de validation
                    {
                        'id'      => 'cancelTransfer',
                        'type'    => 'warning',
                        'message' => $locale->t('Vous souhaitez annuler le transfert inter-agences. Votre demande sera enregistrée lorsque vous validerez.'),
                        'buttons' => [{
                                       'title'   => $locale->t('Valider'),
                                       'onclick' => 'machine.valid()'
                                      },
                                      {
                                       'title'   => $locale->t('Annuler'),
                                       'onclick' => 'machine.cancelCancel()'
                                      }]
                    }
                ]);

                $displayValidBtn = 1;
            }

            print '
        <div id="transfer">';

            my $hasTransfersToDisplay = 0;

            # Transfert inter-agences en cours
            if (defined $tabInfos->{'agenciesTransfer.tabInfos'})
            {
                my $tabTransfer = $tabInfos->{'agenciesTransfer.tabInfos'};

                my $doBtn;
                my $undoBtn;
                my $cancelBtn;
                my $transferLabel;
                my $delegationLabel;
                my $synchroLabel;
                my $canDoTransfer;

                my $isDone = ($tabTransfer->{'loading'}->{'isDone'} ||
                              $tabTransfer->{'unloading'}->{'isDone'});

                if ($tabInfos->{'tabRights'}->{'actions'}->{'doTransfer'})
                {
                    $canDoTransfer = $tabInfos->{'possibilities'}->{'actions'}->{'doTransfer'};

                    $doBtn = '<a href="#" id="doBtn" class="btn do' .
                        ($canDoTransfer ? '' : ' disabled') . ($isDone ? ' hidden' : '') . '" title="' .
                        htmllocale('Réaliser ce transfert inter-agences') . '" possible="' .
                        ($canDoTransfer * 1) . '"></a>';

                    $undoBtn = '<a href="#" id="undoBtn" class="btn undo' .
                        ($canDoTransfer ? '' : ' disabled') . ($isDone ? '' : ' hidden') . '" title="' .
                        htmllocale('Annuler la réalisation de ce transfert inter-agences') . '" possible="' .
                        ($canDoTransfer * 1) . '"></a>';
                }

                if ($isDone)
                {
                    my %tabParameters = (
                            $tabTransfer->{'from.label'},
                            $locale->getDateFormat($tabTransfer->{'unloading'}->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC)
                        );
                    $transferLabel = $locale->t('La machine a été transférée de <span class="strong">%s</span> le <span class="strong">%s</span>',
                                        %tabParameters);
                    if ($tabTransfer->{'delegatedAgency.id'} ne '')
                    {
                        $delegationLabel = $locale->t('Le transport a été effectué par <span class="strong">%s</span>',
                                                      $tabTransfer->{'delegatedAgency.label'});
                    }
                }
                else
                {
                    my %tabParameters = (
                            $tabTransfer->{'to.label'},
                            $locale->getDateFormat($tabTransfer->{'date'}, LOC::Locale::FORMAT_DATE_NUMERIC)
                        );
                    $transferLabel = $locale->t('Un transfert vers l\'agence de <span class="strong">%s</span> pour une arrivée le <span class="strong">%s</span> a été demandé.',
                                                %tabParameters);
                    if ($tabTransfer->{'delegatedAgency.id'} ne '')
                    {
                        $delegationLabel = $locale->t('Le transport doit être effectué par <span class="strong">%s</span>',
                                                      $tabTransfer->{'delegatedAgency.label'});
                    }
                }

                if ($tabInfos->{'tabRights'}->{'actions'}->{'cancelTransfer'})
                {
                    # Pour pouvoir annuler un transfert, il faut avoir les conditions suivantes :
                    # - Il est possible d'annnuler le transfert
                    # - Il faut avoir le droit et la possibilté d'annuler la réalisation de ce transfert
                    # -- OU
                    # -- Le transfert n'est pas encore réalisé

                    my $canCancelTransfer = $tabInfos->{'possibilities'}->{'actions'}->{'cancelTransfer'} &&
                                            ($canDoTransfer || !$isDone);

                    $cancelBtn = '<a href="#" id="cancelBtn" class="btn cancel' .
                        ($canCancelTransfer ? '' : ' disabled') . '" title="' .
                        htmllocale('Annuler ce transfert inter-agences') . '" possible="' .
                        ($canCancelTransfer * 1) . '"></a>';
                }

                my $orderTooltip = '';
                if (defined $tabTransfer->{'order.tabInfos'}->{'id'})
                {
                    $orderTooltip = '
                <div class="infotip">
                    <div class="icon"></div>
                    <div class="tip pick"></div>
                    <div class="tip text" id="toto">
                        <div class="row">
                            <span class="label">' . htmllocale('Commande') . '&nbsp;:</span>
                            <span class="value">' . $tabTransfer->{'order.tabInfos'}->{'code'} . '</span>
                        </div>';
                        $orderTooltip .= '
                    </div>
                </div>';
                }
                print '
            <div id="existingTransfer">
                <div class="label">
                    ' . $transferLabel . $orderTooltip . ($delegationLabel eq '' ? '' : '<br>' . $delegationLabel) . '
                </div>
                <div class="buttons">' . $doBtn . $undoBtn . $cancelBtn . '</div>
            </div>';

                print $view->displayFormInputHidden('isTransferActive', '1');
                print $view->displayFormInputHidden('transferAction', '');

                $hasTransfersToDisplay = 1;
            }

            # Demande de transfert inter-agences
            if ($tabInfos->{'tabRights'}->{'actions'}->{'createTransfer'} &&
                ($tabInfos->{'possibilities'}->{'actions'}->{'createTransfer'} ||
                    !$tabInfos->{'possibilities'}->{'actions'}->{'createTransfer'} &&
                        !$tabInfos->{'possibilities'}->{'actions'}->{'doTransfer'} &&
                        !$tabInfos->{'possibilities'}->{'actions'}->{'cancelTransfer'}))
            {
                tie(my %tabAgenciesForTransfer, 'Tie::IxHash');
                %tabAgenciesForTransfer = (
                        '' => '-- ' . htmllocale('Sélectionner') . ' --',
                        %{$tabViewData{'agenciesForTransfer'}}
                );

                tie(my %tabAgenciesForDelegation, 'Tie::IxHash');
                %tabAgenciesForDelegation = (
                        '' => '-- ' . htmllocale('Sélectionner') . ' --',
                        %{$tabViewData{'agenciesForDelegation'}}
                );

                my %tabBaseAttributes = ('possible' => $tabInfos->{'possibilities'}->{'actions'}->{'createTransfer'} * 1);
                if (!$tabInfos->{'possibilities'}->{'actions'}->{'createTransfer'})
                {
                    $tabBaseAttributes{'disabled'} = 1;
                }


                # Une agence de visibilité est sélectionnée pour la machine
                # la liste des agences destinataires possibles pour un transfert est restreinte à l'agence de visibilité
                if ($tabInfos->{'visibleOnAgency.id'})
                {
                    %tabAgenciesForTransfer = (
                        '' => '-- ' . htmllocale('Sélectionner') . ' --',
                        ($tabInfos->{'visibleOnAgency.id'} => $tabAgenciesForTransfer{$tabInfos->{'visibleOnAgency.id'}})
                    );
                }

                my %tabAttributes = %tabBaseAttributes;
                my $destinationSelect = $view->displayFormSelect('destinationAgency.id', \%tabAgenciesForTransfer, undef,
                                                                 undef, \%tabAttributes);

                my %tabAttributes = %tabBaseAttributes;
                my $date = $view->displayFormInputDate('transferDate', undef, undef, undef, \%tabAttributes);

                # Délégation du transport
                my %tabDelegateAttributes = ('possible' => $tabInfos->{'possibilities'}->{'actions'}->{'createTransfer'} * 1 &&
                                                           $tabInfos->{'tabRights'}->{'actions'}->{'delegate'});
                $tabDelegateAttributes{'disabled'} = 1;

                my %tabAttributes = %tabDelegateAttributes;
                $tabAttributes{'onclick'} = 'return false;'; # A ne pas sortir pour IE < 11
                my $delegationSelect  = $view->displayFormSelect('delegationAgency.id', \%tabAgenciesForDelegation, undef,
                                                                 undef, \%tabAttributes);

                my %tabAttributes = %tabDelegateAttributes;
                my $isDelegatedChb = $view->displayFormInputCheckBox('isDelegated', 0,
                                                               \%tabAttributes,
                                                               $locale->t('Déléguer le transport à %s', $delegationSelect),
                                                               1, 1);

                print '
            <div id="newTransfer">
                ' . $locale->t('Transférer la machine à %s pour une arrivée le %s', $destinationSelect, $date) . '
                <br>
                ' . $isDelegatedChb . '
            </div>';

                if ($tabInfos->{'possibilities'}->{'actions'}->{'createTransfer'})
                {
                    $displayValidBtn = 1;
                }

                $hasTransfersToDisplay = 1;
            }

            # Pas de transfert à afficher
            if (!$hasTransfersToDisplay)
            {
                print htmllocale('Il n\'y a aucun transfert inter-agences en cours actuellement');
            }

            print '
        </div>';
        }
    }

    print '
    </fieldset>';

    # Affichage du bouton "Valider" si l'utilisateur a au moins une possibilité
    if ($displayValidBtn)
    {
        print '
    <div id="actionsValid">
    ' . $view->displayControlPanel({'left' => [$view->displayTextButton(htmllocale('Valider'),
                                               'valid(|Over).png', 'machine.valid()', htmllocale('Valider'))],
                                    'substyle' => 'bottom'}) . '
    </div>';

    }
    print $view->displayFormInputHidden('modificationDate', $tabInfos->{'modificationDate'});
    print $view->displayFormInputHidden('valid', '');

    print '
</fieldset>';

    print $view->endForm();
}

1;