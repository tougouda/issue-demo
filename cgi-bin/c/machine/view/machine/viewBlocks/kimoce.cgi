use utf8;
use open (':encoding(UTF-8)');

if ($tabInfos->{'tabRights'}->{'kimoce'}->{'localization'} ||
    $tabInfos->{'tabRights'}->{'kimoce'}->{'infos'} ||
    $tabInfos->{'tabRights'}->{'kimoce'}->{'counters'} ||
    $tabInfos->{'tabRights'}->{'kimoce'}->{'characteristics'} || 
    $tabInfos->{'tabRights'}->{'kimoce'}->{'addInfos'})
{
    print '
<fieldset id="kimoceData">
    <legend>' . $locale->t('Données Kimoce') . '</legend>';


    if ($tabViewData{'kimoce'})
    {
        # Localisation
        if ($tabInfos->{'tabRights'}->{'kimoce'}->{'localization'})
        {
            my $tabKimoceLocalizationStored = $tabViewData{'kimoce'}->{'localization'}->{'stored'};
            my $tabKimoceLocalizationReal   = $tabViewData{'kimoce'}->{'localization'}->{'real'};

            my $className = '';
            if ($tabViewData{'kimoce'}->{'localization'}->{'nbErrors'})
            {
                $className = 'error';
            }

            print '
    <fieldset class="sub' . ($className eq '' ? '' : ' ' . $className) . '">
        <legend>' . $locale->t('Localisation') . '</legend>';

            if ($tabViewData{'kimoce'}->{'localization'}->{'nbErrors'})
            {
                print '
        <div class="wrong">';
            }
            print '
        ' . $tabKimoceLocalizationStored->{'CpyTrdNamDsc'} . '
        <br>
        ' . $tabKimoceLocalizationStored->{'CpyAddrExCde'};
            if ($tabViewData{'kimoce'}->{'localization'}->{'nbErrors'})
            {
                print '
        </div>
        <div class="right">' . $locale->t('Agence %s', $tabInfos->{'agency.label'}) . '</div>';
            }

            print '
    </fieldset>';
        }


        # Informations
        if ($tabInfos->{'tabRights'}->{'kimoce'}->{'infos'})
        {
            my $tabKimoceInfosStored = $tabViewData{'kimoce'}->{'infos'}->{'stored'};
            my $tabKimoceInfosReal   = $tabViewData{'kimoce'}->{'infos'}->{'real'};

            my $className = '';
            if ($tabViewData{'kimoce'}->{'infos'}->{'nbErrors'})
            {
                $className = 'error';
            }

            print '

    <fieldset class="sub' . ($className eq '' ? '' : ' ' . $className) . '">
        <legend>' . $locale->t('Informations') . '</legend>
        <table class="formTable">
            <tbody>
                <tr>
                    <td class="label">' . $locale->t('ObjInCde') . '</td>
                    <td>' . $tabKimoceInfosStored->{'ObjInCde'} . '</td>
                </tr>
                <tr>
                    <td class="label">' . $locale->t('DosInCde') . '</td>
                    <td>' . $tabKimoceInfosStored->{'DosInCde'} . '</td>
                </tr>
                <tr>
                    <td class="label">' . $locale->t('Situation') . '</td>
                    <td>' . &displayKimoceInfo($tabKimoceInfosStored->{'ObjSitDsc'},
                                               $tabKimoceInfosReal->{'ObjSitDsc'}) . '</td>
                </tr>
            </tbody>
        </table>
    </fieldset>';
        }


        # Compteurs
        if ($tabInfos->{'tabRights'}->{'kimoce'}->{'counters'})
        {
            my $tabKimoceCountersStored = $tabViewData{'kimoce'}->{'counters'}->{'stored'};

            print '

    <fieldset class="sub">
        <legend>' . $locale->t('Compteurs') . '</legend>
        <table class="formTable">
            <tbody>
                <tr>
                    <td class="label">' . $locale->t('Récupération') . '</td>
                    <td>' . $tabKimoceCountersStored->{'recoveryCounter'} . '</td>
                </tr>
                <tr>
                    <td class="label">' . $locale->t('Transfert') . '</td>
                    <td>' . $tabKimoceCountersStored->{'sitesTransferCounter'} . '</td>
                </tr>
                <tr>
                    <td class="label">' . $locale->t('Horaire') . '</td>
                    <td>' . $tabKimoceCountersStored->{'timeCounter'} . '</td>
                </tr>
            </tbody>
        </table>
    </fieldset>';
        }


        # Caractéristiques
        if ($tabInfos->{'tabRights'}->{'kimoce'}->{'characteristics'})
        {
            my $tabKimoceCharacteristicsStored = $tabViewData{'kimoce'}->{'characteristics'}->{'stored'};
            my $tabKimoceCharacteristicsReal   = $tabViewData{'kimoce'}->{'characteristics'}->{'real'};

            my $className = '';
            if ($tabViewData{'kimoce'}->{'characteristics'}->{'nbErrors'})
            {
                $className = 'error';
            }

            print '

    <fieldset class="sub' . ($className eq '' ? '' : ' ' . $className) . '">
        <legend>' . $locale->t('Caractéristiques') . '</legend>
        <table class="formTable">
            <tbody>
                <tr>
                    <td class="label">' . $locale->t('Chantier') . '</td>
                    <td>' . &displayKimoceInfo($tabKimoceCharacteristicsStored->{'site'},
                                               $tabKimoceCharacteristicsReal->{'site'}) . '</td>
                </tr>
                <tr>
                    <td class="label">' . $locale->t('Adresse') . '</td>
                    <td>' . &displayKimoceInfo($tabKimoceCharacteristicsStored->{'address'},
                                               $tabKimoceCharacteristicsReal->{'address'}) . '</td>
                </tr>
                <tr>
                    <td class="label">' . $locale->t('Code postal') . '</td>
                    <td>' . &displayKimoceInfo($tabKimoceCharacteristicsStored->{'postalCode'},
                                               $tabKimoceCharacteristicsReal->{'postalCode'}) . '</td>
                </tr>
                <tr>
                    <td class="label">' . $locale->t('Ville') . '</td>
                    <td>' . &displayKimoceInfo($tabKimoceCharacteristicsStored->{'city'},
                                               $tabKimoceCharacteristicsReal->{'city'}) . '</td>
                </tr>
                <tr>
                    <td class="label">' . $locale->t('Commentaire') . '</td>
                    <td>' . &displayKimoceInfo($tabKimoceCharacteristicsStored->{'siteComment'},
                                               $tabKimoceCharacteristicsReal->{'siteComment'}) . '</td>
                </tr>
                <tr>
                    <td class="label">' . $locale->t('Contact') . '</td>
                    <td>' . &displayKimoceInfo($tabKimoceCharacteristicsStored->{'contact'},
                                               $tabKimoceCharacteristicsReal->{'contact'}) . '</td>
                </tr>
                <tr>
                    <td class="label">' . $locale->t('Téléphone') . '</td>
                    <td>' . &displayKimoceInfo($tabKimoceCharacteristicsStored->{'telephone'},
                                               $tabKimoceCharacteristicsReal->{'telephone'}) . '</td>
                </tr>
            </tbody>
        </table>
        <table class="formTable">
            <tbody>
                <tr>
                    <td class="label">' . $locale->t('Client') . '</td>
                    <td>' . &displayKimoceInfo($tabKimoceCharacteristicsStored->{'customer'},
                                               $tabKimoceCharacteristicsReal->{'customer'}) . '</td>
                </tr>
                <tr>
                    <td class="label">' . $locale->t('Date de début de contrat') . '</td>
                    <td>' . &displayKimoceInfo($tabKimoceCharacteristicsStored->{'beginDate'},
                                               $tabKimoceCharacteristicsReal->{'beginDate'}) . '</td>
                </tr>
                <tr>
                    <td class="label">' . $locale->t('Date de fin de contrat') . '</td>
                    <td>' . &displayKimoceInfo($tabKimoceCharacteristicsStored->{'endDate'},
                                               $tabKimoceCharacteristicsReal->{'endDate'}) . '</td>
                </tr>
                <tr>
                    <td class="label">' . $locale->t('Contrat Full Service') . '</td>
                    <td>' . &displayKimoceInfo($tabKimoceCharacteristicsStored->{'fullService'},
                                               $tabKimoceCharacteristicsReal->{'fullService'}) . '</td>
                </tr>
                <tr>
                    <td class="label">' . $locale->t('Amiante') . '</td>
                    <td>' . &displayKimoceInfo($tabKimoceCharacteristicsStored->{'isAsbestos'},
                                               $tabKimoceCharacteristicsReal->{'isAsbestos'}) . '</td>
                </tr>
            </tbody>
        </table>
        <table class="formTable">
            <tbody>
                <tr>
                    <td class="label">' . $locale->t('Dernière panne') . '</td>
                    <td>' . &displayKimoceInfo($tabKimoceCharacteristicsStored->{'lastBreakdown'},
                                               $tabKimoceCharacteristicsReal->{'lastBreakdown'}) . '</td>
                </tr>
                <tr>
                    <td class="label">' . $locale->t('Dernier contrôle') . '</td>
                    <td>' . &displayKimoceInfo($tabKimoceCharacteristicsStored->{'lastControl'},
                                               $tabKimoceCharacteristicsReal->{'lastControl'}) . '</td>
                </tr>
                <tr>
                    <td class="label">' . $locale->t('Dernier passage agence') . '</td>
                    <td>' . &displayKimoceInfo($tabKimoceCharacteristicsStored->{'lastAgencyCheckIn'},
                                               $tabKimoceCharacteristicsReal->{'lastAgencyCheckIn'}) . '</td>
                </tr>
            </tbody>
        </table>
    </fieldset>';
        }

        # Informations additionnelles
        if ($tabInfos->{'tabRights'}->{'kimoce'}->{'addInfos'})
        {
            my $tabKimoceAddInfosStored = $tabViewData{'kimoce'}->{'addInfos'}->{'stored'};
            my $tabKimoceAddInfosReal   = $tabViewData{'kimoce'}->{'addInfos'}->{'real'};

            print '

    <fieldset class="sub">
        <legend>' . $locale->t('Informations additionnelles') . '</legend>
        <table class="formTable">
            <tbody>';

            foreach my $addInfoId (sort(keys(%{$tabKimoceAddInfosStored})))
            {
                print'
                <tr>
                    <td class="label">' . $tabViewData{'tabAddInfos'}->{$addInfoId}->{'name'} . '</td>
                    <td>' . &displayKimoceInfo($tabKimoceAddInfosStored->{$addInfoId}, $tabKimoceAddInfosReal->{$addInfoId}) . '</td>
                </tr>';
            }

            print '
            </tbody>
        </table>
    </fieldset>';
        
        }

    }
    else
    {
        print $view->displayMessages('warning', [$locale->t('Cette machine n\'existe pas dans Kimoce.')], 0);
    }

    print '
</fieldset>';
}


sub displayKimoceInfo
{
    my ($storedValue, $realValue) = @_;

    if ($storedValue eq $realValue)
    {
        return $storedValue;
    }
    else
    {
        return '<div class="wrong">' . $storedValue . '</div><div class="right">' . $realValue . '</div>';
    }
}

1;