use utf8;
use open (':encoding(UTF-8)');

print '
<fieldset id="history">
    <legend>' . $locale->t('Dernières modifications') . '</legend>';

print '
    <div id="waiting"></div>';

# Tableau de résultat
print '
    <div id="history.result" class="hidden">
        <table class="standard">
            <thead>
                <tr>
                    <th colspan="2">' . $locale->t('Date et heure') . '</th>
                    <th>' . $locale->t('Utilisateur') . '</th>
                    <th>' . $locale->t('Événement') . '</th>
                    <th>' . $locale->t('Description') . '</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>';

my $moreBtn = $view->displayTextButton($locale->t('Voir plus...'), 'machine/view/more(|Over).png', '',
                                       $locale->t('Voir plus de modifications'));

my $allBtn = $view->displayTextButton($locale->t('Voir tout'), 'machine/view/all(|Over).png', '',
                                      $locale->t('Voir toutes les modifications'));

print '
        <span id="waitingHistory" class="hidden"></span>
        <span id="allHistoryBtn">
            ' . $allBtn . '
        </span>
        <span id="moreHistoryBtn">
            ' . $moreBtn . '
        </span>
    </div>';

# Message s'il n'y a pas d'historique
print '
    <div id="history.noresult" class="hidden">
        ' . $view->displayMessages('info', [$locale->t('Il n\'y a pas de données disponibles.')], 0) . '
    </div>';

# Message d'erreur
print '
    <div id="history.error" class="hidden">
        ' . $view->displayMessages('error', [$locale->t('Une erreur est survenue lors de la récupération des dernières modifications.')], 0) . '
    </div>';

print '
</fieldset>';

1;