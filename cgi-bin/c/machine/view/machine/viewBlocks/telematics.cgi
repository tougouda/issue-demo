use utf8;
use open (':encoding(UTF-8)');

# Affichage ou non des éléments
my $tabDisplay = $tabViewData{'telematics'}->{'display'};

# Liste des codes
my $tabStartCodes = $tabViewData{'telematics'}->{'startCodes'};


print '
<fieldset id="telematics">
    <legend>' . $locale->t('Télématique') . '</legend>';


print $view->startForm(undef, undef, 'telematicsForm');

my @tabButtons = ();

# Bouton de désactivation de la télématique
if ($tabDisplay->{'disableBtn'})
{
    push(@tabButtons, $view->displayTextButton(htmllocale('Désactiver la télématique'),
                                               'cancel(|Over).png', 'machine.disableTelematics()',
                                               '') .
                      $view->displayFormInputHidden('disableTelematics', ''));
}

# Bouton de suspension ou de réactivation de la télématique
if ($tabDisplay->{'suspendBtn'})
{
    my $js = 'machine.suspendTelematics()';
    my $tabOptions = {};
    if ($tabViewData{'telematics'}->{'isTelematicSuspended'})
    {
        $js = 'return false';
        $tabOptions->{'class'} = 'locCtrlButton disabled';
    }

    push(@tabButtons, $view->displayTextButton(
                            htmllocale('Suspendre la télématique'),
                            'machine/view/telematics/suspend.svg',
                            $js,
                            '',
                            undef,
                            $tabOptions
                        ) .
                      $view->displayFormInputHidden('suspendTelematics', ''));
}

# Bouton d'activation de la génération des codes de démarrage contrats
if ($tabDisplay->{'activateRentContractScopeBtn'})
{
    push(@tabButtons, $view->displayTextButton(htmllocale('Activer la génération des codes contrats'),
                                               'machine/view/telematics/activate-contracts.png', 'machine.activateTelematicsRentContractScope()',
                                               htmllocale('Activer la génération des codes de démarrage contrats')) .
                      $view->displayFormInputHidden('activateTelematicsRentContractScope', ''));
}

# Bouton d'historique des codes
if ($tabDisplay->{'history'})
{
    my $historyUrl = $link = $view->createURL('machine:machine:startCodesHistory', {'machineId' => $tabViewData{'id'}});
    push(@tabButtons, $view->displayTextButton(htmllocale('Historique des codes'),
                                               'machine/view/telematics/history(|Over).png',
                                               'Location.modalWindowManager.show("' . $historyUrl . '", {contentType: 1, width: 700, height: 400});',
                                               $locale->t('Voir l\'historique des codes de la machine')));
}

# Bouton d'initialisation de boîtier télématique suite à un changement
if ($tabDisplay->{'changeEquipment'})
{
    push(@tabButtons, $view->displayTextButton(htmllocale('Finaliser le changement de boîtier'),
                                               'machine/view/telematics/change-equipment.png', 'machine.changeTelematicsEquipment();',
                                               '') .
                      $view->displayFormInputHidden('changeTelematicsEquipment', ''));
}

# Bouton d'activation de la génération des codes de démarrage contrats
if ($tabDisplay->{'digiKeyActivationBtn'})
{
    if ($tabViewData{'telematics'}->{'isDigiKeyDeactivated'})
    {
        push(@tabButtons, $view->displayTextButton(htmllocale('Activer le digicode'),
                                                   'machine/view/telematics/digikey-on.png', 'machine.activateDigiKey()',
                                                   htmllocale('Activer le digicode de la machine')) .
                          $view->displayFormInputHidden('activateDigiKey', ''));
    }
    else
    {
        push(@tabButtons, $view->displayTextButton(htmllocale('Désactiver le digicode'),
                                                   'machine/view/telematics/digikey-off.png', 'machine.deactivateDigiKey()',
                                                   htmllocale('Désactiver le digicode de la machine')) .
                          $view->displayFormInputHidden('deactivateDigiKey', ''));
    }
}

if (@tabButtons > 0)
{
    print $view->displayControlPanel({'left' => \@tabButtons, 'substyle' => 'top'});
}

# Immobilisation
if ($tabDisplay->{'immobilization'})
{
    print '
    <div class="warning-notification">
        <div class="icon"></div>
        <div class="msg">' . htmllocale('La machine est immobilisée et ne peut être démarrée.') . '</div>
    </div>';
}

# Suspension
if ($tabViewData{'telematics'}->{'isTelematicSuspended'})
{
    print '
    <div class="warning-notification">
        <div class="icon"></div>
        <div class="msg">' . htmllocale('La télématique est suspendue sur cette machine.') . '</div>
    </div>';
}

if ($tabViewData{'telematics'}->{'isDigiKeyDeactivated'})
{
    print '
    <div class="warning-notification">
        <div class="icon"></div>
        <div class="msg">' . htmllocale('Le digicode de cette machine est désactivé.') . '</div>
    </div>';
}

if ($tabViewData{'isAdmin'})
{
    print '
    <div class="id" id="telematicsId">
        <table class="formTable">
            <tbody>
                <tr>
                    <td class="label">' . $locale->t('IDTarget') . '</td>
                    <td>' . $tabInfos->{'telematicId'} . '</td>
                </tr>
            </tbody>
        </table>
    </div>';
}


# Si il y a des codes à afficher...
if ($tabDisplay->{'contract'} || $tabDisplay->{'static'})
{
    # Fonction d'affichage des codes
    sub displayStartCodes
    {
        my ($tabStartCodes, $contractId) = @_;

        print '
<table class="formTable startcodes">
    <tbody>';

        # On tri par ordre des types de code
        my @tabTypes = sort {$a->{'order'} <=> $b->{'order'}} values %{$tabStartCodes};

        foreach my $tabType (@tabTypes)
        {
            if (!$tabDisplay->{$tabViewData{'tabCodeTypes'}->{$tabType->{'code'}}})
            {
                next;
            }


            # On affiche en premier les codes appliqués, ensuite on tri par date de création des codes
            my @tabCodes = sort { ($b->{'state'} eq 'applied' ? 1 : 0) <=> ($a->{'state'} eq 'applied' ? 1 : 0) ||
                                  $a->{'creationDate'} cmp $b->{'creationDate'} } @{$tabType->{'startCodes'}};

            my $typeLabel = undef;
            foreach my $tabStartCode (@tabCodes)
            {
                # Evite la répétition du type de code
                $typeLabel = (!defined $typeLabel ? $view->toHTMLEntities($tabType->{'name'}) : '');


                # Bouton d'envoi de SMS
                my $sendSMSBtn;
                if (defined $tabStartCode->{'sendSMS'})
                {
                    my $image = 'sendSms.svg';
                    my $action = 'machine.sendStartCodeSMS(' . $contractId . ')';
                    my $tabAttributes = {};

                    if (!$tabStartCode->{'sendSMS'})
                    {
                        $image = 'sendSmsDisabled.svg';
                        $action = '';
                        $tabAttributes = {'class' => 'locCtrlButton disabled'};
                    }

                    $sendSMSBtn = $view->displayTextButton(
                        '',
                        'machine/view/' . $image,
                        $action,
                        $locale->t('Envoyer le code par SMS au contact chantier'),
                        'left',
                        $tabAttributes
                    );
                }

                my $tabTitles = {
                    'applied'       => '',
                    'pending'       => htmllocale('En cours d\'envoi'),
                    'unapp-pending' => htmllocale('En cours de suppression')
                };

                print '
        <tr class="startcode startcode-' . $tabStartCode->{'state'} . '">
            <td class="label">' . $typeLabel . '</td>
            <td>' . $sendSMSBtn . '</td>
            <td class="code">
                <span class="startcode" title="' . $tabTitles->{$tabStartCode->{'state'}} . '">' . $view->toHTMLEntities($tabStartCode->{'code'}) . '</span>
            </td>
            <td class="rules">';

                foreach my $ruleInfos (@{$tabStartCode->{'rules'}})
                {
                    my @ruleStates;
                    my $ruleApplicationState = ($ruleInfos->{'applicationId'} eq LOC::StartCode::STATE_RULE_APPLICATION_PENDING ? 'pending' : 'applied');

                    push(@ruleStates, $ruleApplicationState);
                    if ($ruleInfos->{'unapplicationId'} eq LOC::StartCode::STATE_RULE_UNAPPLICATION_PENDING)
                    {
                        push(@ruleStates, 'unapp-pending');
                    }

                    my $ruleClass = join(' ', map { 'rule-' . $_} @ruleStates);

                    print '
                <div class="rule ' . $ruleClass . '">';
                    if ($ruleInfos->{'beginDate'} || $ruleInfos->{'endDate'})
                    {
                        print '
                    <span class="date">' . ($ruleInfos->{'beginDate'} ? $locale->getDateFormat($ruleInfos->{'beginDate'}, LOC::Locale::FORMAT_DATE_NUMERIC) : '') . '</span>
                    &rarr;
                    <span class="date">' . ($ruleInfos->{'endDate'} ? $locale->getDateFormat($ruleInfos->{'endDate'}, LOC::Locale::FORMAT_DATE_NUMERIC) : '') . '</span>';
                    }
                    else
                    {
                        print '<i>' . htmllocale('sans limite') . '</i>';
                    }

                    print '<span class="rule-states">';
                    foreach my $ruleState (@ruleStates)
                    {
                        if ($ruleState ne 'applied')
                        {
                            print '<i class="rule-' . $ruleState . '" title="' . $tabTitles->{$ruleState} . '"></i>';
                        }
                    }
                    print '</span>';

                    print '
                </div>';
                }

                print '
            </td>
        </tr>';
            }
        }

        print '
    </tbody>
</table>';
        }

    print '
    <table>
        <tbody>
            <tr style="vertical-align: top;">';


    # On affiche les blocs contrats dans le cas où il y a des codes dynamiques
    if ($tabDisplay->{'contract'})
    {
        print '
                <td>';

        # Tri des contrats par date de fin
        my @tabContracts = sort { $a->{'order'} <=> $b->{'order'} || $a->{'beginDate'} cmp $b->{'beginDate'} || $a->{'endDate'} cmp $b->{'endDate'} } values %{$tabStartCodes->{'contract'}};

        foreach my $tabContract (@tabContracts)
        {
            my $title;
            my $button = '';
            if ($tabContract->{'id'})
            {
                $title = $locale->t('Contrat %s', '<a href="' . $tabContract->{'url'} . '" target="_blank">' . $tabContract->{'code'} . '</a>');

                if ($tabContract->{'regenerateBtn'})
                {
                    $button = $view->displayTextButton('', 'machine/view/telematics/regenerate.png', 'machine.regenerateTelematicsRentContractCodes(' . $tabContract->{'id'} . ')',
                                                       $locale->t('Régénérer les codes contrat'), 'left',
                                                       {'class' => 'manage-btn'});
                }
            }
            else
            {
                $title = $locale->t('Codes obsolètes');
            }
            print '
                    <fieldset class="sub">
                        <legend>' . $title . ' ' . $button . '</legend>';

            &displayStartCodes($tabContract->{'list'}, $tabContract->{'id'});

            print '
                    </fieldset>';
        }

        print $view->displayFormInputHidden('sendStartCodeSMS', 0);
        print $view->displayFormInputHidden('regenerateTelematicsRentContractCodes', 0);

        print '
                </td>';
    }

    # On affiche les codes techniques dans le cas où on est dans l'affichage complet et qu'il y a des codes statiques
    if ($tabDisplay->{'static'})
    {
        my $manageBtn = '';
        if ($tabDisplay->{'staticCodesPage'})
        {
            my $link = $view->createURL('superv:telematics:view');
            $manageBtn = $view->displayTextButton('', 'helpdesk/index/links.png', $link,
                                                  $locale->t('Gestion des codes statiques'), 'left',
                                                  {'class' => 'manage-btn'});
        }
        print '
                <td>
                    <fieldset class="sub">
                        <legend>' . htmllocale('Technique') . '</legend>' . $manageBtn;

        &displayStartCodes($tabStartCodes->{'static'});

        print '
                    </fieldset>
                </td>';
    }

    print '
            </tr>
        </tbody>
    </table>';
}

print $view->displayFormInputHidden('modificationDate', $tabInfos->{'modificationDate'});

print $view->endForm();

print '
</fieldset>';

1;