use utf8;
use open (':encoding(UTF-8)');

use strict;

use Tie::IxHash;

use LOC::Html::Standard;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;

# Répertoire CSS/JS/Images
our $dir = 'machine/machine/';

our %tabViewData;


our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}

# URL courante
my $currentUrl = $view->createURL('machine:machine:create');

# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'create.css');

# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc($dir . 'create.js');


#$view->addPackage('popups');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);


$view->addPackage('searchboxes');

my $title = $locale->t('Création de machine');
$view->setPageTitle($title);
$view->setTitle($title);


# Bouton "Actualiser"
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png',
                                          $currentUrl, $locale->t('Actualiser la page'));
$view->addControlsContent([$refreshBtn], 'right', 1);


# Messages de retour
my $errorBlockRefreshBtn = '<span onclick="$ge(\'errorBlock\').style.display = \'none\';">' . $refreshBtn . '</span>';
my $fatalErrorMsg = htmllocale('Erreur fatale !') .
    '<p class="details">' .
    $locale->t('Veuillez %s la page et renouveler l\'opération.', $errorBlockRefreshBtn) . '<br>' .
    htmllocale('Si l\'erreur se reproduit, merci de contacter le support informatique.') .
    '</p>';

my $displayMachine = undef;
if (defined $tabViewData{'tabData'}->{'parkNumber'})
{
    $displayMachine = $view->displayParkNumber($tabViewData{'tabData'}->{'parkNumber'});
}
my $tabTranslations = {
  'fatalError'              => $fatalErrorMsg,
  'requiredFields'          => $locale->t('Les champs obligatoires ne sont pas renseignés'),
  'modelUnknown'            => $locale->t('Le modèle choisi est inconnu'),
  'existingParkNumber'      => $locale->t('Le numéro de parc %s existe déjà', $displayMachine),
  'existingSerialNumber'    => $locale->t('Le numéro de série existe déjà pour le modèle choisi'),
  'unknownError'            => $locale->t('Erreur inconnue'),
  'success'                 => $locale->t('La machine %s a été correctement créée', $displayMachine)
};

# Message de confirmation de validation avec succès
if ($tabViewData{'tabData'}->{'@valid'})
{
    my $validBlock = '
<div id="validBlock" class="messageBlock valid" >
    <div class="popup">
        <div class="content">
            <table>
                <tbody>
                    <tr>
                        <td style="font-weight: bold; vertical-align: top;">' . $tabTranslations->{$tabViewData{'tabData'}->{'@valid'}} . '.</td>
                    </tr>
                    <tr class="buttons">
                        <td>' .
                        $view->displayTextButton($locale->t('Ok'), '',
                                                 '$("#validBlock").hide();',
                                                 '', 'l', {'id' => 'validBlockBtn'}) .
                        '</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>';

    $view->addBodyContent($validBlock, 0, 1);
    $view->addBodyEvent('load', '$("#validBlockBtn").focus();');
}

# Messages d'erreurs
if ($tabViewData{'tabData'}->{'@errors'})
{
    my $errorBlock = '
<div id="errorBlock" class="messageBlock error">
    <div class="popup">
        <div class="content">
            <table>
                <tbody>
                    <tr>
                        <td style="vertical-align: top;">
                            <p style="font-weight : bold;">'
                            . $locale->t('Les modifications sur la fiche machine n\'ont pas été enregistrées car les erreurs suivantes ont été détectées')
                            . '</p>
                            <ul>';
    #Erreurs retournées depuis PERL
    while (my ($index, $errorCode) = each(@{$tabViewData{'tabData'}->{'@errors'}}))
    {
       $errorBlock .= '<li>' . $locale->t($tabTranslations->{$errorCode}) . '</li>';
    }

    $errorBlock .= '
                            </ul>
                        </td>
                    </tr>
                    <tr class="buttons">
                        <td>' .
                        $view->displayTextButton($locale->t('Fermer'), '',
                                                 '$("#errorBlock").hide();',
                                                 '', 'l', {'id' => 'errorBlockBtn'}) .
                        '</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>';

    $view->addBodyContent($errorBlock, 0, 1);
    $view->addBodyEvent('load', '$("#errorBlockBtn").focus();');
}



# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();

# Symbole champ requis
my $requiredIcon = $view->displayRequiredSymbol();
my $inputsTabIndex = 1;

# - Modèle
my $tabModelsList = {};
if (defined $tabViewData{'tabModelInfos'} && $tabViewData{'tabData'}->{'model.id'} != 0)
{
    $tabModelsList = {$tabViewData{'tabData'}->{'model.id'} => $tabViewData{'tabModelInfos'}->{'label'}};
}
my $modelListUrl = $view->createURL('machine:machine:jsonModelList');
my $modelInput   = $view->displayFormSelect('model.id', $tabModelsList, 
                                         $tabViewData{'tabData'}->{'model.id'}, 0,
                                         {'tabindex' => $inputsTabIndex++});
$modelInput .= $view->displayJSBlock('var modelSearchBox = Location.searchBoxesManager.createSearchBox("model.id", {isRequired: true, url: "' . $modelListUrl . '"});
modelSearchBox.createElement = function(key, value)
{
    return new Option((value["label"] || ""), value["id"]);
}');

# - N° série
my $serialNumberInput = $view->displayFormInputText('serialNumber', $tabViewData{'tabData'}->{'serialNumber'}, '', 
                                                    {
                                                         'size'         => 25, 
                                                         'autocomplete' => 'off',
                                                         'tabindex'     => $inputsTabIndex++
                                                     });

# - Date apave
my $controlDateInput = $view->displayFormInputDate('control.date', 
                                                    ($tabViewData{'tabData'}->{'control.nextDate'} eq '' ? undef : $tabViewData{'tabData'}->{'control.nextDate'}),
                                                    undef, undef, {'tabindex' => $inputsTabIndex++});

# - Agence de création
tie(my %tabAgencies, 'Tie::IxHash');
foreach my $agencyId (keys(%{$tabViewData{'agenciesList'}}))
{
    $tabAgencies{$agencyId} = $tabViewData{'agenciesList'}->{$agencyId};
}
my $defaultAgencyId = (defined $tabViewData{'tabData'}->{'agency.id'} ? $tabViewData{'tabData'}->{'agency.id'} : $tabViewData{'defaultAgencyId'});
my $creationAgencySelect = $view->displayFormSelect('agency.id', \%tabAgencies, 
                                                     $defaultAgencyId, 0, 
                                                     {'tabindex' => $inputsTabIndex++});

# Boutons de vérification/validation
my $createButton = $view->displayTextButton(
                                        'Créer la machine',
                                        'valid(|Over).png',
                                        'submitForm();',
                                        'Créer la machine',
                                        'l',
                                        {'id'    => 'createBtn',
                                         'class' => 'locCtrlButton',
                                         'tabindex' => -1,
                                         'onfocus' => 'this.blur();'
                                        }
                                        );


## Affichage

print '
<fieldset id="createForm">
    <legend>' . $view->displayImage('helpdesk/common/form.png') . '&nbsp;' . $locale->t('Informations sur la machine') . '</legend>

    <table class="formTable">
        <tbody>
            <tr>
                <td class="label">' . htmllocale('N° parc') . '</td>
                <td class="parkNumber">' . $view->displayParkNumber($tabViewData{'maxParkNumber'}, undef, 0) . '</td>
            </tr>
            <tr>
                <td class="label">' . htmllocale('Modèle') . $requiredIcon . '</td>
                <td>' . $modelInput . ' ' .  $view->displayTextButton('', 'clear.gif', 'modelSearchBox.clear();', $locale->t('Effacer la recherche')) . '</td>
            </tr>
            <tr>
                <td class="label">' . htmllocale('N° de série') . $requiredIcon . '</td>
                <td>' . $serialNumberInput . '</td>
            </tr>
            <tr>
                <td class="label">' . htmllocale('Date apave') . $requiredIcon . '</td>
                <td>' . $controlDateInput . '</td>
            </tr>
            <tr>
                <td class="label">' . htmllocale('Agence de création') . $requiredIcon . '</td>
                <td>' . $creationAgencySelect . '</td>
            </tr>
        </tbody>
    </table>
</fieldset>';



print $view->displayControlPanel({'left' => $createButton, 'substyle' => 'bottom'});

print $view->displayFormInputHidden('parkNumber', $tabViewData{'maxParkNumber'});
print $view->displayFormInputHidden('valid', '');


# Affichage du pied de page
print $view->endForm();
print $view->displayFooter();