use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;

our %tabViewData;
our $view;

$view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

my $allocationUrl = $view->createURL('technical:allocation:view', {'countryId' => $tabViewData{'country.id'},
                                                              'agencyId' => $tabViewData{'user.agency.id'}, 
});

print "Location: $allocationUrl\n\n";
