use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;

# Répertoire CSS/JS/Images
our $dir = 'machine/machine/';

our %tabViewData;

our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

# Nombre de résultats
if ($tabViewData{'nbResult'} == 1)
{
    my $machineId = (keys(%{$tabViewData{'result'}}))[0];
    my $url = $view->createURL('machine:machine:view', {'machineId' => $machineId});
    # Redirection
    &LOC::Browser::sendHeaders("Status: 302 Found\nLocation: " . $url . "\n\n");
}
else
{
    # Feuilles de style
    $view->addCSSSrc('location.css');

    $view->setPageTitle($locale->t('Machine inexistante'));

    $view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                      LOC::Html::Standard::DISPFLG_FOOT |
                      LOC::Html::Standard::DISPFLG_BTCLOSE |
                      LOC::Html::Standard::DISPFLG_BTPRINT |
                      LOC::Html::Standard::DISPFLG_NOTIFS);


    $view->setTitle($locale->t('Fiche machine'));

    # CSS
    $view->addCSSSrc($dir . 'search.css');

    # Barre de recherche
    require($directory . '/machine/searchBar.cgi');

    # Affichage de l'entête
    print $view->displayHeader();

    print $view->startForm();

    print '<h1 class="searchResult">' . $locale->t('Résultat de la recherche') . '</h1>';

	# S'il n'y a pas de critères sélectionnés
    if ($tabViewData{'nbFilters'} == 0)
    {
        print '<p class="noResult">' . $locale->t('Aucun critère de recherche sélectionné. Veuillez affiner votre recherche.') . '</p>';
    }
    else
    {
        print '<p class="noResult">' . $locale->t('Aucune machine ne correspond à votre recherche') . '</p>';
    }

    # Affichage du pied de page
    print $view->endForm();

    print $view->displayFooter();
}