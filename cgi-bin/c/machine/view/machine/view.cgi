use utf8;
use open (':encoding(UTF-8)');

use strict;

use Tie::IxHash;

use LOC::Html::Standard;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;

# Répertoire CSS/JS/Images
our $dir = 'machine/machine/';

our %tabViewData;
our %tabSearchData;


our $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
our $view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});


my $title = (defined $tabViewData{'infos'} ? $locale->t('Machine %s', $tabViewData{'infos'}->{'parkNumber'}) :
                                             $locale->t('Machine inexistante'));
$view->setPageTitle($title);

$view->addPackage('modalwindow')
     ->addPackage('popups');

my $display;
if ($tabViewData{'viewType'} ne VIEWTYPE_LIGHT)
{
    $display = LOC::Html::Standard::DISPFLG_HEAD |
               LOC::Html::Standard::DISPFLG_FOOT |
               LOC::Html::Standard::DISPFLG_BTCLOSE |
               LOC::Html::Standard::DISPFLG_BTPRINT |
               LOC::Html::Standard::DISPFLG_NOTIFS;
}
$view->setDisplay($display);


# Messages pour javascript
$view->addTranslations({
    'answerAreYouSureDisableTelematics' => $locale->t('Êtes-vous sûr de vouloir désactiver la télématique de cette machine ?'),
    'answerAreYouSureSuspendTelematics' => $locale->t('Êtes-vous sûr de vouloir suspendre la télématique sur cette machine ?'),
    'answerAreYouSureActivateTelematicsRentContractScope' => $locale->t('Êtes-vous sûr de vouloir activer la génération des codes de démarrage contrats sur cette machine ?'),
    'answerAreYouSureRegenerateTelematicsRentContractCodes' => $locale->t('Êtes-vous sûr de vouloir régénérer les codes de démarrage pour ce contrat ?'),
    'answerAreYouSureChangeTelematicsEquipment' => $locale->t('Êtes-vous sûr de vouloir finaliser le changement du boîtier ?'),
    'answerAreYouSureActivateDigiKey' => $locale->t('Êtes-vous sûr de vouloir activer le digicode de la machine ?'),
    'answerAreYouSureDeactivateDigiKey' => $locale->t('Êtes-vous sûr de vouloir désactiver le digicode de la machine ?')
});


# Bouton "actualiser"
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'),
                                          'refresh(|Over).png',
                                          $view->createURL('machine:machine:view', {'machineId' => $tabViewData{'id'}}),
                                          $locale->t('Actualiser la fiche machine'),
                                          'l',
                                          {'tabindex' => -1,
                                           'onfocus' => 'this.blur();'
                                          });

if ($tabViewData{'viewType'} eq VIEWTYPE_FULL)
{
    # Barre de recherche
    require($directory . '/machine/searchBar.cgi');

    # Bouton "actualiser"
    $view->addControlsContent($refreshBtn, 'right', 1);
}

# Messages de retour
my $errorBlockRefreshBtn = '<span onclick="$ge(\'errorBlock\').style.display = \'none\';">' . $refreshBtn . '</span>';
my $fatalErrorMsg = htmllocale('Erreur fatale !') .
    '<p class="details">' .
    $locale->t('Veuillez %s la fiche machine et renouveler l\'opération.', $errorBlockRefreshBtn) . '<br>' .
    htmllocale('Si l\'erreur se reproduit, merci de contacter le support informatique.') .
    '</p>';

my $obsoleteMsg = $locale->t('Les données de la machine sont obsolètes ou vous avez appuyé sur la touche F5.<br>Veuillez cliquer sur le bouton %s.', $refreshBtn);

my $tabTranslations = {
  'update' => {
      'success'            => $locale->t('Les modifications sur la fiche machine ont été enregistrées'),
      'errors'             => $locale->t('Les modifications sur la fiche machine n\'ont pas été enregistrées car les erreurs suivantes ont été détectées :'),
      'fatalError'         => $fatalErrorMsg,
      'unknownError'       => $locale->t('Erreur inconnue'),
      'obsolete'           => $obsoleteMsg,
      'rightsError'        => $locale->t('Certaines informations ne sont pas modifiables'),
      'transferError'      => $locale->t('Les données du transfert inter-agences sont incohérentes'),
      'transferAgency'     => $locale->t('L\'agence n\'est pas renseignée pour le transfert inter-agences'),
      'transferDate'       => $locale->t('La date de transfert inter-agences n\'est pas renseignée'),
      'transferActive'     => $locale->t('Changement d\'état impossible : un transfert inter-agences est en cours'),
      'siteBlockedComment' => $locale->t('Le commentaire pour le blocage de la machine sur le chantier est obligatoire'),
      'controlDate'        => $locale->t('La date saisie pour le dernier contrôle est postérieure à aujourd’hui')
  },
  'deallocate' => {
      'success'       => $locale->t('La machine a été désattribuée'),
      'errors'        => $locale->t('La machine n\'a pas été désattribuée car les erreurs suivantes ont été détectées :'),
      'fatalError'    => $fatalErrorMsg,
      'unknownError'  => $locale->t('Erreur inconnue'),
      'obsolete'      => $obsoleteMsg,
      'rightsError'   => $locale->t('Vous n\'avez pas les droits nécessaires pour désattribuer la machine'),
      'contractError' => $locale->t('Impossible de désattribuer la machine : celle-ci n\'est plus sur le contrat'),
      'machineError'  => $locale->t('Impossible de désattribuer la machine')
  },
  'sendStartCodeSMS' => {
      'success'       => $locale->t('Le code de démarrage a été envoyé par SMS au contact chantier'),
      'errors'        => $locale->t('Le code de démarrage n\'a pas été envoyé par SMS car les erreurs suivantes ont été détectées :'),
      'fatalError'    => $fatalErrorMsg,
      'unknownError'  => $locale->t('Erreur inconnue'),
      'obsolete'      => $obsoleteMsg,
      'rightsError'   => $locale->t('Vous n\'avez pas les droits nécessaires pour envoyer le code de démarrage par SMS'),
      'receiverError' => $locale->t('Le numéro de téléphone du destinataire n\'est pas un numéro de téléphone mobile'),
      'noReceiver'    => $locale->t('Le numéro de téléphone du contact chantier n\'est pas renseigné')
  },
  'disableTelematics' => {
      'success'       => $locale->t('La télématique a été désactivée sur cette machine'),
      'errors'        => $locale->t('La télématique n\'a pas été désactivée car les erreurs suivantes ont été détectées :'),
      'fatalError'    => $fatalErrorMsg,
      'unknownError'  => $locale->t('Erreur inconnue'),
      'obsolete'      => $obsoleteMsg,
      'rightsError'   => $locale->t('Vous n\'avez pas les droits nécessaires pour désactiver la télématique')
  },
  'suspendTelematics' => {
      'success'       => $locale->t('La suspension de la télématique sur la machine a été effectuée'),
      'errors'        => $locale->t('La suspension de la télématique sur la machine n\'a pas été effectuée car les erreurs suivantes ont été détectées :'),
      'fatalError'    => $fatalErrorMsg,
      'unknownError'  => $locale->t('Erreur inconnue'),
      'obsolete'      => $obsoleteMsg,
      'rightsError'   => $locale->t('Vous n\'avez pas les droits nécessaires pour suspendre la télématique sur la machine')
  },
  'activateTelematicsRentContractScope' => {
      'success'       => $locale->t('La génération des codes de démarrage contrats a été activée'),
      'errors'        => $locale->t('La génération des codes de démarrage contrats n\'a pas été activée car les erreurs suivantes ont été détectées :'),
      'fatalError'    => $fatalErrorMsg,
      'unknownError'  => $locale->t('Erreur inconnue'),
      'obsolete'      => $obsoleteMsg,
      'rightsError'   => $locale->t('Vous n\'avez pas les droits nécessaires pour activer la génération des codes de démarrage contrats')
  },
  'regenerateTelematicsRentContractCodes' => {
      'success'       => $locale->t('La régénération des codes de démarrage du contrat a été effectuée'),
      'errors'        => $locale->t('La régénération des codes de démarrage du contrat n\'a pas été effectuée car les erreurs suivantes ont été détectées :'),
      'fatalError'    => $fatalErrorMsg,
      'unknownError'  => $locale->t('Erreur inconnue'),
      'obsolete'      => $obsoleteMsg,
      'rightsError'   => $locale->t('Vous n\'avez pas les droits nécessaires pour régénérer les codes de démarrage contrats')
  },
  'changeTelematicsEquipment' => {
      'success'       => $locale->t('Les règles ont été envoyées au boîtier'),
      'errors'        => $locale->t('Les règles n\'ont pas été envoyées au boîtier car les erreurs suivantes ont été détectées :'),
      'fatalError'    => $fatalErrorMsg,
      'unknownError'  => $locale->t('Erreur inconnue'),
      'obsolete'      => $obsoleteMsg,
      'rightsError'   => $locale->t('Vous n\'avez pas les droits nécessaires pour finaliser le changement de boîtier')
  },
  'activateDigiKey' => {
      'success'       => $locale->t('L\'activation du digicode de la machine a été effectuée'),
      'errors'        => $locale->t('L\'activation du digicode de la machine n\'a pas été effectuée car les erreurs suivantes ont été détectées :'),
      'fatalError'    => $fatalErrorMsg,
      'unknownError'  => $locale->t('Erreur inconnue'),
      'obsolete'      => $obsoleteMsg,
      'rightsError'   => $locale->t('Vous n\'avez pas les droits nécessaires pour activer le digicode de la machine')
  },
  'deactivateDigiKey' => {
      'success'       => $locale->t('La désactivation du digicode de la machine a été effectuée'),
      'errors'        => $locale->t('La désactivation du digicode de la machine n\'a pas été effectuée car les erreurs suivantes ont été détectées :'),
      'fatalError'    => $fatalErrorMsg,
      'unknownError'  => $locale->t('Erreur inconnue'),
      'obsolete'      => $obsoleteMsg,
      'rightsError'   => $locale->t('Vous n\'avez pas les droits nécessaires pour désactiver le digicode de la machine')
  }
};

# Message de confirmation de validation avec succès
if ($tabViewData{'tabData'}->{'@valid'})
{
    my $validBlock = '
<div id="validBlock" class="messageBlock valid" >
    <div class="popup">
        <div class="content">
            <table>
                <tbody>
                    <tr>
                        <td style="font-weight: bold; vertical-align: top;">' . $tabTranslations->{$tabViewData{'tabData'}->{'@action'}}->{$tabViewData{'tabData'}->{'@valid'}} . '.</td>
                    </tr>
                    <tr class="buttons">
                        <td>' .
                        $view->displayTextButton($locale->t('Ok'), '',
                                                 '$("#validBlock").hide();',
                                                 '', 'l', {'id' => 'validBlockBtn'}) .
                        '</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>';

    $view->addBodyContent($validBlock, 0, 1);
    $view->addBodyEvent('load', '$("#validBlockBtn").focus();');
}

# Messages d'erreurs
if ($tabViewData{'tabData'}->{'@errors'})
{
    my $errorBlock = '
<div id="errorBlock" class="messageBlock error">
    <div class="popup">
        <div class="content">
            <table>
                <tbody>
                    <tr>
                        <td style="vertical-align: top;">
                            <p style="font-weight : bold;">'
                            . $tabTranslations->{$tabViewData{'tabData'}->{'@action'}}->{'errors'}
                            . '</p>
                            <ul>';
    #Erreurs retournées depuis PERL
    while (my ($index, $errorCode) = each(@{$tabViewData{'tabData'}->{'@errors'}}))
    {
       $errorBlock .= '<li>' . $tabTranslations->{$tabViewData{'tabData'}->{'@action'}}->{$errorCode} . '</li>';
    }

    $errorBlock .= '
                            </ul>
                        </td>
                    </tr>
                    <tr class="buttons">
                        <td>' .
                        $view->displayTextButton($locale->t('Fermer'), '',
                                                 '$("#errorBlock").hide();',
                                                 '', 'l', {'id' => 'errorBlockBtn'}) .
                        '</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>';

    $view->addBodyContent($errorBlock, 0, 1);
    $view->addBodyEvent('load', '$("#errorBlockBtn").focus();');
}

# Chargement de l'historique en AJAX
if (defined $tabViewData{'infos'})
{
    my $historyUrl = $view->createURL('machine:machine:jsonHistory');
    $view->addBodyEvent('load', 'machine.init("' . $historyUrl . '", ' .
                                    $tabViewData{'infos'}->{'id'} . ', "' . $tabViewData{'locale.id'} . '", ' .
                                    $tabViewData{'history'}->{'nbLastModifications'} . ', ' .
                                    $tabViewData{'history'}->{'nbModificationsStep'} . ', ' .
                                    LOC::Json::toJson($tabViewData{'agencies'}) . ');');
}

$view->setTitle($locale->t('Fiche machine'));

# JavaScript
$view->addJSSrc($dir . 'view.js');
# jQuery
$view->addJSSrc('jQuery/jquery.js');

# CSS
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'view.css');

# Modules additionnels
$view->addPackage('tooltips');


sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}

# Affichage de l'entête
$view->{'_networkOptimization'} = 'machine-machine-view';
print $view->displayHeader();


# Machine existante
if (defined $tabViewData{'infos'})
{
    our $tabInfos = $tabViewData{'infos'};

    our $searchUrl = 'machine:list:view';
    our %tabBaseSearchParams = ('agency.id' => [$tabInfos->{'agency.id'}]);

    our $linkTarget = '';
    if ($tabViewData{'viewType'} eq VIEWTYPE_LIGHT)
    {
        $linkTarget = ' target="_blank"';
    }



    # Informations d'en-tête
    require($directory . '/machine/viewBlocks/header.cgi');


    # Identification
    require($directory . '/machine/viewBlocks/identification.cgi');

    # Télématique
    if ($tabViewData{'displayTelematics'})
    {
        require($directory . '/machine/viewBlocks/telematics.cgi');
    }

    # Informations commerciales
    require($directory . '/machine/viewBlocks/businessInfos.cgi');

    if ($tabViewData{'viewType'} eq VIEWTYPE_FULL)
    {
        # Actions
        require($directory . '/machine/viewBlocks/actions.cgi');

        # Historique
        require($directory . '/machine/viewBlocks/history.cgi');

        # Données Kimoce
        require($directory . '/machine/viewBlocks/kimoce.cgi');
    }
}
# Machine inexistante
else
{
    print '
<div class="unknownMachine">
    ' . $view->displayMessages('warning', [$locale->t('La machine demandée n\'existe pas.')], 0) . '
</div>';
}


print $view->displayFooter();
