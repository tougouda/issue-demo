use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;
use LOC::Locale;
use LOC::Json;


# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;


# Répertoire CSS/JS/Images
our $dir = 'machine/machine/';


our %tabViewData;
our $inputsTabIndex = 1;


our $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
our $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


# URL courante 
my $currentUrl = $view->createURL('machine:machine:toFinishList');


# Feuilles de style
$view->addCSSSrc('location.css');
$view->addCSSSrc($dir . 'toFinishList.css');

# Scripts JS
# JS
$view->addJSSrc($dir . 'toFinishList.js');
# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc('jQuery/plugins/tablesorter.js');


$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS|
                  LOC::Html::Standard::DISPFLG_NOTIFS);


my $title = $locale->t('Création de machine(s) à finaliser');
$view->setPageTitle($title);
$view->setTitle($title);

# On n'affiche pas le tableau si il est vide
my @tabMachines = @{$tabViewData{'tabMachines'}};

# Connexion à Kimoce ?
if (!$tabViewData{'isKimoceCallable'} && @tabMachines > 0)
{
    my $msg = $locale->t('Vous ne pouvez pas finaliser la création des machines car Kimoce n\'est pas disponible.');

    # Notifications possibles
    $view->addNotifications([
        {
            'id'      => 'kimoce-inactive',
            'type'    => 'info',
            'message' => $msg
        }
    ]);

    $view->addBodyEvent('load', 'Location.viewObj.displayNotification("kimoce-inactive", "show", {});');
}


# Bouton "Actualiser"
my $refreshBtn = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png',
                                          'window.location.href = "' . $currentUrl . '";',
                                          $locale->t('Actualiser la page'));
$view->addControlsContent([$refreshBtn], 'right', 1);

# Message d'erreur sur une ou plusieurs lignes
my @tabErrorIds = ();
if ($tabViewData{'return'})
{
    # Message de succès
    my $nbSuccess = @{$tabViewData{'return'}->{'success'}};
    if ($nbSuccess > 0)
    {
        my @tabParkNumbers = ();
        foreach my $machineInfos (@{$tabViewData{'return'}->{'success'}})
        {
            push(@tabParkNumbers, $view->displayParkNumber($machineInfos->{'parkNumber'}));
        }

        # Message de succès
        my $msg = $locale->tn(
           'La création de la machine %s a été finalisée.',
           'La création des machines %s a été finalisée.',
           $nbSuccess, join(', ', @tabParkNumbers));

        # Notifications possibles
        $view->addNotifications([
            {
                'id'      => 'valid-finish',
                'type'    => 'info',
                'message' => $msg
            }
        ]);

        $view->addBodyEvent('load', 'Location.viewObj.displayNotification("valid-finish", "show", {});');
    }

    # Message d'erreur
    my $nbErrors = @{$tabViewData{'return'}->{'errors'}};
    if ($nbErrors > 0)
    {
        my $errorBlockRefreshBtn = '<span onclick="$ge(\'errorBlock\').style.display = \'none\';">' . $refreshBtn . '</span>';
        my $fatalErrorMsg = htmllocale('Erreur fatale !') .
            '<p class="details">' .
            $locale->t('Veuillez %s la liste et renouveler l\'opération.', $errorBlockRefreshBtn) . '<br>' .
            htmllocale('Si l\'erreur se reproduit, merci de contacter le support informatique.') .
            '</p>';

        # Messages de retour
        my $tabTranslations = {
            'fatalError'             => $fatalErrorMsg,
            'rightsError'            => $locale->t('Certaines informations ne sont pas modifiables'),
            'kimoceUnavailable'      => $locale->t('Kimoce n\'est pas disponible'),
            'alreadyFinished'        => $locale->t('La création de la machine a déjà été finalisée'),
            'unknownMachineInKimoce' => $locale->t('La machine n\'existe pas dans Kimoce')
        };

        # Message d'erreur
        my $msg = $locale->tn(
           'La création de la machine suivante n\'a pas été finalisée',
           'La création des machines suivantes n\'a pas été finalisée',
           $nbErrors);

        my $errorBlock = '
<div id="errorBlock" class="messageBlock error">
    <div class="popup" style="display: block;">
        <div class="content">
            <table>
                <tbody id="errorsTable">
                    <tr>
                        <td>
                            <p style="font-weight : bold;">' . $msg . ' :</p>
                            <ul>';
        # Erreurs retournées depuis PERL
        foreach my $machineInfos (@{$tabViewData{'return'}->{'errors'}})
        {
            push(@tabErrorIds, $machineInfos->{'id'});

            $errorBlock .= '<li>' . $view->displayParkNumber($machineInfos->{'parkNumber'}) .
                $tabTranslations->{$machineInfos->{'error'}} .
            '</li>';
        }

        $errorBlock .= '
                            </ul>
                        </td>
                    </tr>
                    <tr class="buttons" id="buttonRow">
                        <td>' .
                        $view->displayTextButton($locale->t('Fermer'), '', '$ge("errorBlock").style.display = "none";') .
                        '</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>';

        $view->addBodyContent($errorBlock, 0, 1);
    }

    # Message d'information
    my $nbInfos = @{$tabViewData{'return'}->{'infos'}};
    if ($nbInfos > 0)
    {
        my $msg = $locale->t('Les mises à jour Kimoce n\'ont pas eu lieu car celui-ci n\'est pas disponible.');

        # Notifications possibles
        $view->addNotifications([
            {
                'id'      => 'kimoce-unsaved',
                'type'    => 'info',
                'message' => $msg
            }
        ]);

        foreach my $codeInfo (@{$tabViewData{'return'}->{'infos'}})
        {
            $view->addBodyEvent('load', 'Location.viewObj.displayNotification("' . $codeInfo . '", "show", {});');
        }
    }
}


# Affichage de l'entête
print $view->displayHeader();



if (@tabMachines == 0)
{
    # Pas de résultat
    print '<div class="noResult">' . $view->displayMessages('info', [$locale->t('Aucune création de machine à finaliser')], 0) . '</div>';
}
else
{
    # Options pour tablesorter
    my $headersOption = '
3 : {sorter: "textAttr"},
4 : {sorter: "textAttr"},
7 : {sorter: false}'
;

    my $sortListOption = '[[0,0]]'; # Tri par défaut: "Date de début" (ASC)


    # Bouton de réalisation
    my $checkedNo = ' <span id="do-checked"></span>';
    my $doBtn = $view->displayTextButton($locale->t('Marquer comme créée') . $checkedNo, 'valid(|Over).png',
                                         'return true;',
                                         '',
                                         'l',
                                         {'class' => 'locCtrlButton disabled', 'id' => 'do-btn'});

    # Bouton d'affichage de tous les transports
    my $viewModeFullLabel = $locale->t('Affichage : %s', '<span class="viewmode-label full">' . $locale->t('toutes les lignes') . '</span><span class="viewmode-label checked">' . $locale->t('lignes modifiées') . '</span>');
    my $viewModeFullBtn = $view->displayTextButton($viewModeFullLabel,
                                                   'machine/toFinish/viewall.png',
                                                   'return true;',
                                                   '',
                                                   'l',
                                                   {'id'    => 'viewmode-btn',
                                                    'class' => 'locCtrlButton'});

    print $view->startForm();

    print '
<div id="table-wrapper" class="list viewmode-full">
    <div id="table-toolbar-container" class="toolbar header">
        <div class="col content">
            <div class="fixed-content">
            </div>
        </div>
        <div class="col controls">
            <ul>
                <li>' . $viewModeFullBtn . '</li>
            </ul>
            <ul>
                <li> ' . $doBtn . '</li>
            </ul>
            <div class="recap" id="header-updated-recap" title="' . htmllocale('Machine créée dans Kimoce') . '"></div>
            <div class="fixed-content">
                <a href="#table-filters-container" id="back-top-btn" title="' . htmllocale('Retourner en haut de la page') . '">' .
                    $view->displayImage('machine/toFinish/up.png') .
               '</a>
            </div>
        </div>
    </div>
    <div class="content" id="table-container">';


    print '
        <table class="basic create" id="table-main">
        <thead>
            <tr>
                <th class="code">' . htmllocale('N° parc') . '</th>
                <th class="model">' . htmllocale('Modèle de machines') . '</th>
                <th class="serialNumber">' . htmllocale('N° de série') . '</th>
                <th class="purchaseDate">' . htmllocale('Date d\'achat') . '</th>
                <th class="controlDate">' . htmllocale('Date de dernière VGP') . '</th>
                <th class="agency">' . htmllocale('Agence') . '</th>
                <th class="state">' . htmllocale('État') . '</th>
                <th class="checkbox"></th>
            </tr>
        </thead>
        <tbody>';

    # Affichage des lignes
    my $tabConfigs = [];
    foreach my $tabMachineInfos (@tabMachines)
    {
#        my $errorsCount = @{$tabContractInfos->{'tabErrors'}};

        my $machineId = $tabMachineInfos->{'id'};
        my $className = '';

        my $purchaseDate    = $tabMachineInfos->{'purchaseDate'} ? $locale->getDateFormat($tabMachineInfos->{'purchaseDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC) : '';
        my $lastControlDate = $tabMachineInfos->{'control.lastDate'} ? $locale->getDateFormat($tabMachineInfos->{'control.lastDate'}, &LOC::Locale::FORMAT_DATE_NUMERIC) : '';

        my $chbAttributes = {};
        if (!$tabViewData{'isKimoceCallable'} || !$tabMachineInfos->{'isFinalizable'})
        {
            $chbAttributes->{'disabled'} = 'disabled';
        }

        print '
            <tr id="machine_' . $machineId . '" class="' . $className . '">
                <td class="code">' . $view->displayParkNumber($tabMachineInfos->{'parkNumber'}) . '</td>
                <td class="model">' . $tabMachineInfos->{'model.label'} . '</td>
                <td class="serialNumber">' . $tabMachineInfos->{'serialNumber'} . '</td>
                <td class="purchaseDate" data-val="' . $tabMachineInfos->{'purchaseDate'} . '">' . $purchaseDate . '</td>
                <td class="controlDate" data-val="' . $tabMachineInfos->{'control.lastDate'} . '">' . $lastControlDate . '</td>
                <td class="agency">' . $tabMachineInfos->{'agency.id'} . '</td>
                <td class="state">' . $tabMachineInfos->{'state.label'} . '</td>
                <td class="checkbox">' . $view->displayFormInputCheckBox('machineIds[' . $machineId . ']', 0, $chbAttributes, undef, $machineId) . '</td>
            </tr>';

    }


    # Hidden
    print $view->displayFormInputHidden('valid', '');

    print '
        </tbody>
    </table>
</div>';

    # Tri du tableau
    print $view->displayJSBlock('
$.tablesorter.addParser({
    id: "textAttr",
    is: function(s) { return false; },
    format: function(s, table, cell, cellIndex) { return $(cell).attr("data-val"); },
    type: "text"
});
$.tablesorter.addParser({
    id: "integerAttr",
    is: function(s) { return false; },
    format: function(s, table, cell, cellIndex) { return $(cell).attr("data-val"); },
    type: "numeric"
});

$(function() 
{ 
    $("table.basic.create").tablesorter({
        headers: {' . $headersOption . '},
        sortList: ' . $sortListOption . ',
        widgets: ["zebra"]
    });
}
);');


print $view->displayJSBlock('
$(function()
{
    machines.init(' . &LOC::Json::toJson({
        'list' => \@tabMachines
    }) . ');
});');

}


# Affichage du pied de page
print $view->displayFooter();
