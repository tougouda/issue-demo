use utf8;
use open (':encoding(UTF-8)');

# Package: machine
# Contrôleur de gestion de la recherche et de la fiche machine
package machine;


# Constants: Modes d'affichage des zones de texte
# VIEWTYPE_LIGHT - Vue simplifiée (pour affichage en pop-up)
# VIEWTYPE_FULL  - Vue complète
use constant
{
    VIEWTYPE_LIGHT => 'light',
    VIEWTYPE_FULL  => 'full'
};

# Constants: Paramètres d'affichage
# NBLASTCONTRACTS     - Nombre de derniers contrats à afficher
# NBLASTMODIFICATIONS - Nombre de dernières modifications à afficher
# NBMODIFICATIONSSTEP - Nombre de modifications supplémentaires affichées lors du clic sur "Voir plus"
use constant
{
    NBLASTCONTRACTS     => 10,
    NBLASTMODIFICATIONS => 10,
    NBMODIFICATIONSSTEP => 20
};


use strict;
use File::Basename;

use LOC::Date;
use LOC::Family;
use LOC::Kimoce;
use LOC::Machine;
use LOC::State;
use LOC::TariffFamily;
use LOC::Json;
use LOC::StartCode::Static;
use LOC::StartCode::Contract::Rent;


# Function: viewAction
# Fiche machine
#
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};


    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'country.id'     => $tabUserInfos->{'nomadCountry.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
        'viewType'       => &LOC::Request::getString('view'),
        'isAdmin'        => $tabUserInfos->{'isAdmin'}
    );

    # Vue "full" par défaut
    if ($tabViewData{'viewType'} ne VIEWTYPE_LIGHT)
    {
        $tabViewData{'viewType'} = VIEWTYPE_FULL;
    }

    # Redirection par défaut
    my $redirect = 'view';
    # Action dans le cas d'une désattribution
    my $action = '';

    # Identifiant de la machine
    $tabViewData{'id'} = &LOC::Request::getInteger('machineId', undef);
    $tabViewData{'parkNumber'} = &LOC::Request::getString('parkNumber', '',  LOC::Request::SRC_REQUEST);

    if (defined $tabViewData{'id'} || $tabViewData{'parkNumber'} ne '')
    {
        my $tabFilters = {};
        if (defined $tabViewData{'id'})
        {
            $tabFilters->{'id'} = $tabViewData{'id'};
        }
        if ($tabViewData{'parkNumber'} ne '')
        {
            $tabFilters->{'parkNumber'} = $tabViewData{'parkNumber'};
        }

        my $tabResult = &LOC::Machine::getList($tabViewData{'country.id'},
                                                LOC::Util::GETLIST_IDS,
                                                $tabFilters);
        if (@$tabResult == 1)
        {
            $tabViewData{'id'} = $tabResult->[0];

            my $modificationDate = &LOC::Request::get('modificationDate');

            # Récupération des paramètres
            my $isTransferActive = &LOC::Request::getBoolean('isTransferActive', 0);
            my $tabData = {
                'modificationDate'   => $modificationDate,
                'lastControlDate'    => &LOC::Request::get('lastControlDate'),
                'isOutOfPark'        => &LOC::Request::get('isOutOfPark'),
                'state.id'           => &LOC::Request::get('state.id'),
                'isSiteBlocked'      => &LOC::Request::get('isSiteBlocked'),
                'siteBlockedComment' => &LOC::Request::get('siteBlockedComment'),
                'isVisibleOnAgency'  => &LOC::Request::get('isVisibleOnAgency'),
                'visibilityMode'     => LOC::Machine::VISIBILITYMODE_MACHINE, # Mode de visibilité depuis la fiche machine
            };
            # Suppression des éléments valant undef (pas de mise à jour nécessaire)
            foreach my $key (keys(%$tabData))
            {
                if (!defined $tabData->{$key})
                {
                    delete $tabData->{$key};
                }
            }

            if (defined $tabData->{'isVisibleOnAgency'})
            {
                $tabData->{'visibleOnAgency.id'} = &LOC::Request::get('visibleOnAgency.id');
            }

            # On vide la date de dernier contrôle si elle vaut la chaîne vide
            if ($tabData->{'lastControlDate'} eq '')
            {
                delete $tabData->{'lastControlDate'};
            }
            # On vide le commentaire de blocage sur chantier si on décoche
            if (defined $tabData->{'isSiteBlocked'} && !$tabData->{'isSiteBlocked'})
            {
                $tabData->{'siteBlockedComment'} = undef;
            }
            my $transferAgency    = &LOC::Request::getString('destinationAgency.id', '');
            my $transferDate      = &LOC::Request::getString('transferDate', '');
            my $isDelegated       = &LOC::Request::getString('isDelegated', 0);
            my $delegationAgency  = &LOC::Request::getString('delegationAgency.id', '');
            if ($isTransferActive)
            {
                $tabData->{'transferAction'} = &LOC::Request::getString('transferAction', '');
                if ($transferAgency ne '')
                {
                    $tabData->{'agenciesTransfer.tabInfos'} = {
                        'to.id'              => $transferAgency,
                        'date'               => $transferDate,
                        'delegatedAgency.id' => ($isDelegated && $delegationAgency ne '' ? $delegationAgency : undef)
                    };
                }
            }
            else
            {
                # si on a pas de transfert actif, alors on récupère les nouvelles informations de transfert
                $tabData->{'agenciesTransfer.tabInfos'} = {
                    'to.id'              => $transferAgency,
                    'date'               => $transferDate,
                    'delegatedAgency.id' => ($isDelegated && $delegationAgency ne '' ? $delegationAgency : undef)
                };
            }

            my $validUpdate = &LOC::Request::getString('valid', '');
            my $tabErrors = {};

            # Enregistrement du bloc actions
            if ($validUpdate eq 'ok')
            {
                my $tabAction = {
                    'id' => $tabViewData{'id'},
                    'type' => undef,
                    'data' => $tabData
                };
                # Type d'action
                if ($tabData->{'agenciesTransfer.tabInfos'}->{'to.id'} ne '' ||
                    $tabData->{'transferAction'} ne '')
                {
                    $tabAction->{'type'} = 'transfer';
                }
                else
                {
                    $tabAction->{'type'} = 'update';
                }
                &LOC::Machine::updateInfosAndTransport($tabViewData{'country.id'},
                                                       $tabAction, $tabUserInfos->{'id'},
                                                       {'updateFrom' => 'machineForm'}, $tabErrors);

                # Construction de la trame de retour
                my $nbErrors = @{$tabErrors->{'fatal'}};
                if ($nbErrors > 0)
                {
                    $tabViewData{'tabData'} = {
                        '@action'     => 'update',
                        '@state'      => 'ko',
                        '@perlErrors' => $tabErrors
                    };

                    my @tabCodeErrors = (); # 0 pour être sûr d'avoir un numéro d'erreur dans le tableau !!

                    # Erreur sur la fiche machine
                    if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'fatalError'); # Erreur fatale
                    }
                    # Erreur de droits sur la fiche machine
                    if (&LOC::Util::in_array(0x0003, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'rightsError'); # Droits
                    }
                    # Erreur sur le transport
                    if (&LOC::Util::in_array(0x0010, $tabErrors->{'fatal'}))
                    {
                        my $tabTransportErrors = $tabErrors->{'modules'}->{'transport'};
                        if (&LOC::Util::in_array(0x0101, $tabTransportErrors->{'fatal'}))
                        {
                            push(@tabCodeErrors, 'transferError'); # Données incohérentes pour le transport
                        }
                        if (&LOC::Util::in_array(0x0201, $tabTransportErrors->{'fatal'}))
                        {
                            push(@tabCodeErrors, 'transferAgency'); # Agence non renseignée
                        }
                        if (&LOC::Util::in_array(0x0202, $tabTransportErrors->{'fatal'}))
                        {
                            push(@tabCodeErrors, 'transferDate'); # Date non renseignée
                        }
                    }
                    # Erreur sur la machine
                    if (&LOC::Util::in_array(0x0020, $tabErrors->{'fatal'}))
                    {
                        my $tabMachineErrors = $tabErrors->{'modules'}->{'machine'};
                        if (&LOC::Util::in_array(0x0100, $tabMachineErrors->{'fatal'}))
                        {
                            push(@tabCodeErrors, 'obsolete'); # Informations soumises obsolètes
                        }
                        if (&LOC::Util::in_array(0x0002, $tabMachineErrors->{'fatal'}))
                        {
                            push(@tabCodeErrors, 'transferActive'); # Changement d'état impossible
                                                                    # - un transfert est en cours
                        }
                        # Erreur de droits sur la fiche machine
                        if (&LOC::Util::in_array(0x0004, $tabMachineErrors->{'fatal'}))
                        {
                            push(@tabCodeErrors, 'siteBlockedComment'); # Commentaire blocage chantier obligatoire
                        }
                        # Erreur de date de dernière VGP
                        if (&LOC::Util::in_array(0x0007, $tabMachineErrors->{'fatal'}))
                        {
                            push(@tabCodeErrors, 'controlDate');
                        }
                    }

                    # Si pas d'erreur alors erreur inconnue !
                    if (@tabCodeErrors == 0)
                    {
                        push(@tabCodeErrors, 'unknownError'); # Erreur inconnue !
                    }

                    $tabViewData{'tabData'}->{'@errors'} = \@tabCodeErrors;
                }
                else
                {
                     $tabViewData{'tabData'} = {
                        '@action' => 'update',
                        '@state'  => 'ok',
                        '@valid'  => 'success'
                    };
                }
            }
            # Enregistrement de la désattribution de machine
            elsif ($validUpdate ne '')
            {
                my $tabValidInfos = &LOC::Json::fromJson($validUpdate);

                &LOC::Machine::deallocate(
                        $tabViewData{'country.id'},
                        $tabViewData{'id'},
                        $tabUserInfos->{'id'},
                        {
                            'contractId'       => $tabValidInfos->{'id'},
                            'modificationDate' => $tabValidInfos->{'modificationDate'}
                        },
                        $tabErrors
                    );

                # Construction de la trame de retour
                my $nbErrors = @{$tabErrors->{'fatal'}};
                if ($nbErrors > 0)
                {
                    $tabViewData{'tabData'} = {
                        '@action'     => 'deallocate',
                        '@state'      => 'ko',
                        '@perlErrors' => $tabErrors
                    };

                    my @tabCodeErrors = (); # 0 pour être sûr d'avoir un numéro d'erreur dans le tableau !!

                    # Erreur sur la fiche machine
                    if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'fatalError'); # Erreur fatale
                    }
                    # Erreur de droits sur la fiche machine
                    if (&LOC::Util::in_array(0x0003, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'rightsError'); # Droits
                    }
                    # La machine n'est plus sur le contrat
                    if (&LOC::Util::in_array(0x0005, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'contractError'); # Erreur contrat
                    }
                    # Impossible de désattribuer la machine
                    if (&LOC::Util::in_array(0x0006, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'machineError'); # Erreur machine
                    }
                    if (&LOC::Util::in_array(0x0100, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'obsolete'); # Informations soumises obsolètes
                    }

                    # Si pas d'erreur alors erreur inconnue !
                    if (@tabCodeErrors == 0)
                    {
                        push(@tabCodeErrors, 'unknownError'); # Erreur inconnue !
                    }

                    $tabViewData{'tabData'}->{'@errors'} = \@tabCodeErrors;
                }
                else
                {
                    $tabViewData{'tabData'} = {
                        '@action' => 'deallocate',
                        '@state'  => 'ok',
                        '@valid'  => 'success'
                    };
                    $action = $tabValidInfos->{'action'};
                }
            }


            # Désactivation de la télématique
            if (&LOC::Request::getString('disableTelematics') ne '')
            {
                my $tabErrors = {
                    'fatal' => []
                };

                # Déséquipement de la télématique
                &LOC::Machine::disableTelematics(
                        $tabViewData{'country.id'},
                        $tabViewData{'id'},
                        $tabUserInfos->{'id'},
                        {
                            'modificationDate' => $modificationDate
                        },
                        $tabErrors
                    );

                # Construction de la trame de retour
                my $nbErrors = @{$tabErrors->{'fatal'}};
                if ($nbErrors > 0)
                {
                    $tabViewData{'tabData'} = {
                        '@action'     => 'disableTelematics',
                        '@state'      => 'ko',
                        '@perlErrors' => $tabErrors
                    };

                    my @tabCodeErrors = (); # 0 pour être sûr d'avoir un numéro d'erreur dans le tableau !!

                    # Erreur fatale
                    if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'fatalError'); # Erreur fatale
                    }
                    # Erreur de droits
                    if (&LOC::Util::in_array(0x0002, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'rightsError');
                    }
                    # Informations soumises obsolètes
                    if (&LOC::Util::in_array(0x0100, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'obsolete');
                    }

                    # Si pas d'erreur alors erreur inconnue !
                    if (@tabCodeErrors == 0)
                    {
                        push(@tabCodeErrors, 'unknownError'); # Erreur inconnue !
                    }

                    $tabViewData{'tabData'}->{'@errors'} = \@tabCodeErrors;
                }
                else
                {
                    $tabViewData{'tabData'} = {
                        '@action' => 'disableTelematics',
                        '@state'  => 'ok',
                        '@valid'  => 'success'
                    };
                }
            }


            # Suspension/réactivation de la télématique
            if (&LOC::Request::getString('suspendTelematics') ne '')
            {
                my $tabErrors = {
                    'fatal' => []
                };

                # Suspension/réactivation de la télématique
                &LOC::Machine::suspendTelematics(
                        $tabViewData{'country.id'},
                        $tabViewData{'id'},
                        $tabUserInfos->{'id'},
                        {
                            'modificationDate' => $modificationDate
                        },
                        $tabErrors
                    );

                # Construction de la trame de retour
                my $nbErrors = @{$tabErrors->{'fatal'}};
                if ($nbErrors > 0)
                {
                    $tabViewData{'tabData'} = {
                        '@action'     => 'suspendTelematics',
                        '@state'      => 'ko',
                        '@perlErrors' => $tabErrors
                    };

                    my @tabCodeErrors = (); # 0 pour être sûr d'avoir un numéro d'erreur dans le tableau !!

                    # Erreur fatale
                    if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'fatalError'); # Erreur fatale
                    }
                    # Erreur de droits
                    if (&LOC::Util::in_array(0x0002, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'rightsError');
                    }
                    # Informations soumises obsolètes
                    if (&LOC::Util::in_array(0x0100, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'obsolete');
                    }

                    # Si pas d'erreur alors erreur inconnue !
                    if (@tabCodeErrors == 0)
                    {
                        push(@tabCodeErrors, 'unknownError'); # Erreur inconnue !
                    }

                    $tabViewData{'tabData'}->{'@errors'} = \@tabCodeErrors;
                }
                else
                {
                    $tabViewData{'tabData'} = {
                        '@action' => 'suspendTelematics',
                        '@state'  => 'ok',
                        '@valid'  => 'success'
                    };
                }
            }


            # Activation de la génération des codes de démarrage contrats
            if (&LOC::Request::getString('activateTelematicsRentContractScope') ne '')
            {
                my $tabErrors = {
                    'fatal' => []
                };

                # Activation des codes de contrats
                &LOC::Machine::activateTelematicsRentContractScope(
                        $tabViewData{'country.id'},
                        $tabViewData{'id'},
                        $tabUserInfos->{'id'},
                        {
                            'modificationDate' => $modificationDate
                        },
                        $tabErrors
                    );

                # Construction de la trame de retour
                my $nbErrors = @{$tabErrors->{'fatal'}};
                if ($nbErrors > 0)
                {
                    $tabViewData{'tabData'} = {
                        '@action'     => 'activateTelematicsRentContractScope',
                        '@state'      => 'ko',
                        '@perlErrors' => $tabErrors
                    };

                    my @tabCodeErrors = (); # 0 pour être sûr d'avoir un numéro d'erreur dans le tableau !!

                    # Erreur fatale
                    if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'fatalError'); # Erreur fatale
                    }
                    # Erreur de droits
                    if (&LOC::Util::in_array(0x0002, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'rightsError');
                    }
                    # Informations soumises obsolètes
                    if (&LOC::Util::in_array(0x0100, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'obsolete');
                    }

                    # Si pas d'erreur alors erreur inconnue !
                    if (@tabCodeErrors == 0)
                    {
                        push(@tabCodeErrors, 'unknownError'); # Erreur inconnue !
                    }

                    $tabViewData{'tabData'}->{'@errors'} = \@tabCodeErrors;
                }
                else
                {
                    $tabViewData{'tabData'} = {
                        '@action' => 'activateTelematicsRentContractScope',
                        '@state'  => 'ok',
                        '@valid'  => 'success'
                    };
                }
            }


            # Initialisation du boîtier suite à un changement
            if (&LOC::Request::getString('changeTelematicsEquipment') ne '')
            {
                my $tabErrors = {
                    'fatal' => []
                };

                # Déséquipement de la télématique
                &LOC::Machine::changeTelematicsEquipment(
                        $tabViewData{'country.id'},
                        $tabViewData{'id'},
                        $tabUserInfos->{'id'},
                        {
                            'modificationDate' => $modificationDate
                        },
                        $tabErrors
                    );

                # Construction de la trame de retour
                my $nbErrors = @{$tabErrors->{'fatal'}};
                if ($nbErrors > 0)
                {
                    $tabViewData{'tabData'} = {
                        '@action'     => 'changeTelematicsEquipment',
                        '@state'      => 'ko',
                        '@perlErrors' => $tabErrors
                    };

                    my @tabCodeErrors = (); # 0 pour être sûr d'avoir un numéro d'erreur dans le tableau !!

                    # Erreur fatale
                    if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'fatalError'); # Erreur fatale
                    }
                    # Erreur de droits
                    if (&LOC::Util::in_array(0x0002, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'rightsError');
                    }
                    # Informations soumises obsolètes
                    if (&LOC::Util::in_array(0x0100, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'obsolete');
                    }

                    # Si pas d'erreur alors erreur inconnue !
                    if (@tabCodeErrors == 0)
                    {
                        push(@tabCodeErrors, 'unknownError'); # Erreur inconnue !
                    }

                    $tabViewData{'tabData'}->{'@errors'} = \@tabCodeErrors;
                }
                else
                {
                    $tabViewData{'tabData'} = {
                        '@action' => 'changeTelematicsEquipment',
                        '@state'  => 'ok',
                        '@valid'  => 'success'
                    };
                }
            }


            # Activation/Désactivation du digicode
            if (&LOC::Request::getString('activateDigiKey') ne '' ||
                &LOC::Request::getString('deactivateDigiKey') ne '')
            {
                my $tabErrors = {
                    'fatal' => []
                };

                # Activation/désactivation du digicode
                my $isDeactivated = (&LOC::Request::getString('deactivateDigiKey') ne '');
                my $action = ($isDeactivated ? 'deactivateDigiKey' : 'activateDigiKey');
                &LOC::Machine::setDigiKeyDeactivated(
                        $tabViewData{'country.id'},
                        $tabViewData{'id'},
                        $isDeactivated,
                        $tabUserInfos->{'id'},
                        {
                            'modificationDate' => $modificationDate
                        },
                        $tabErrors
                    );

                # Construction de la trame de retour
                my $nbErrors = @{$tabErrors->{'fatal'}};
                if ($nbErrors > 0)
                {
                    $tabViewData{'tabData'} = {
                        '@action'     => $action,
                        '@state'      => 'ko',
                        '@perlErrors' => $tabErrors
                    };

                    my @tabCodeErrors = (); # 0 pour être sûr d'avoir un numéro d'erreur dans le tableau !!

                    # Erreur fatale
                    if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'fatalError'); # Erreur fatale
                    }
                    # Erreur de droits
                    if (&LOC::Util::in_array(0x0002, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'rightsError');
                    }
                    # Informations soumises obsolètes
                    if (&LOC::Util::in_array(0x0100, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'obsolete');
                    }

                    # Si pas d'erreur alors erreur inconnue !
                    if (@tabCodeErrors == 0)
                    {
                        push(@tabCodeErrors, 'unknownError'); # Erreur inconnue !
                    }

                    $tabViewData{'tabData'}->{'@errors'} = \@tabCodeErrors;
                }
                else
                {
                    $tabViewData{'tabData'} = {
                        '@action' => $action,
                        '@state'  => 'ok',
                        '@valid'  => 'success'
                    };
                }
            }


            # Envoi du digicode par SMS
            my $sendStartCodeSMS = &LOC::Request::getInteger('sendStartCodeSMS');
            if ($sendStartCodeSMS != 0)
            {
                my $tabErrors = {
                    'fatal' => []
                };

                &LOC::Machine::sendSmsTelematicsRentContractCodes(
                        $tabViewData{'country.id'},
                        $tabViewData{'id'},
                        $sendStartCodeSMS,
                        $tabUserInfos->{'id'},
                        {
                            'modificationDate' => $modificationDate
                        },
                        $tabErrors
                    );

                # Construction de la trame de retour
                my $nbErrors = @{$tabErrors->{'fatal'}};
                if ($nbErrors > 0)
                {
                    $tabViewData{'tabData'} = {
                        '@action'     => 'sendStartCodeSMS',
                        '@state'      => 'ko',
                        '@perlErrors' => $tabErrors
                    };

                    my @tabCodeErrors = (); # 0 pour être sûr d'avoir un numéro d'erreur dans le tableau !!

                    # Erreur sur l'appel en SOAP
                    if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'fatalError'); # Erreur fatale
                    }
                    # Un des numéros destinataires n'est pas correctement formaté
                    if (&LOC::Util::in_array(0x0002, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'receiverError');
                    }
                    # Erreur de droits sur l'envoi de SMS
                    if (&LOC::Util::in_array(0x0003, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'rightsError');
                    }
                    # Le contact chantier n'a pas de numéro de téléphone
                    if (&LOC::Util::in_array(0x0004, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'noReceiver');
                    }
                    # Informations soumises obsolètes
                    if (&LOC::Util::in_array(0x0100, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'obsolete');
                    }

                    # Si pas d'erreur alors erreur inconnue !
                    if (@tabCodeErrors == 0)
                    {
                        push(@tabCodeErrors, 'unknownError'); # Erreur inconnue !
                    }

                    $tabViewData{'tabData'}->{'@errors'} = \@tabCodeErrors;
                }
                else
                {
                    $tabViewData{'tabData'} = {
                        '@action' => 'sendStartCodeSMS',
                        '@state'  => 'ok',
                        '@valid'  => 'success'
                    };
                }
            }


            # Régénération des codes de démarrage d'un contrat
            my $regenerateContractId = &LOC::Request::getInteger('regenerateTelematicsRentContractCodes');
            if ($regenerateContractId != 0)
            {
                my $tabErrors = {
                    'fatal' => []
                };

                # Régénération des codes de démarrage associés au contrat
                &LOC::Machine::regenerateTelematicsRentContractCodes(
                        $tabViewData{'country.id'},
                        $tabViewData{'id'},
                        $regenerateContractId,
                        $tabUserInfos->{'id'},
                        {
                            'modificationDate' => $modificationDate
                        },
                        $tabErrors
                    );

                # Construction de la trame de retour
                my $nbErrors = @{$tabErrors->{'fatal'}};
                if ($nbErrors > 0)
                {
                    $tabViewData{'tabData'} = {
                        '@action'     => 'regenerateTelematicsRentContractCodes',
                        '@state'      => 'ko',
                        '@perlErrors' => $tabErrors
                    };

                    my @tabCodeErrors = (); # 0 pour être sûr d'avoir un numéro d'erreur dans le tableau !!

                    # Erreur fatale
                    if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'fatalError'); # Erreur fatale
                    }
                    # Erreur de droits
                    if (&LOC::Util::in_array(0x0002, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'rightsError');
                    }
                    # Informations soumises obsolètes
                    if (&LOC::Util::in_array(0x0100, $tabErrors->{'fatal'}))
                    {
                        push(@tabCodeErrors, 'obsolete');
                    }

                    # Si pas d'erreur alors erreur inconnue !
                    if (@tabCodeErrors == 0)
                    {
                        push(@tabCodeErrors, 'unknownError'); # Erreur inconnue !
                    }

                    $tabViewData{'tabData'}->{'@errors'} = \@tabCodeErrors;
                }
                else
                {
                    $tabViewData{'tabData'} = {
                        '@action' => 'regenerateTelematicsRentContractCodes',
                        '@state'  => 'ok',
                        '@valid'  => 'success'
                    };
                }
            }


            # Informations sur la machine
            my $tabInfos = &LOC::Machine::getInfos($tabViewData{'country.id'},
                                                   $tabViewData{'id'},
                                                   LOC::Machine::GETINFOS_ALL,
                                                   {'level' => LOC::Machine::LEVEL_FAMILY});
            if ($tabInfos)
            {
                # Si on a choisi la redirection vers l'attribution de machine
                if ($action eq 'deallocate-redirect' && $tabInfos->{'agency.id'} eq $tabViewData{'user.agency.id'})
                {
                    my $redirectUrl = &LOC::Request::createRequestUri('technical:allocation:view',
                                                                        {'countryId' => $tabViewData{'country.id'},
                                                                         'agencyId'  => $tabViewData{'user.agency.id'}});
                    $redirect = 'redirect';
                }
                else
                {
                    # Correspondance entre les types de code de démarrage et les droits
                    $tabViewData{'tabCodeTypes'} = &_getStartCodeTypesForRights();

                    # Informations sur la machine
                    $tabViewData{'infos'} = $tabInfos;

                    # Comparaison agence machine/agence nomade
                    $tabViewData{'sameAgency'} = ($tabInfos->{'agency.id'} eq $tabViewData{'user.agency.id'} ? 1 : 0);

                    # Récupération des informations additionnelles
                    $tabViewData{'tabAddInfos'} = &LOC::Kimoce::getAddInfos(LOC::Util::GETLIST_ASSOC, $tabUserInfos->{'locale.id'});

                    # Taux d'utilisation
                    my %tabModelUseRates = &LOC::Model::getUseRate($tabInfos->{'model.id'}, $tabInfos->{'agency.id'});
                    my %tabFamilyUseRates = &LOC::TariffFamily::getUseRate($tabInfos->{'tariffFamily.id'},
                                                                           $tabInfos->{'agency.id'});
                    $tabViewData{'useRates'} = {'model' => \%tabModelUseRates, 'family' => \%tabFamilyUseRates};

                    # Historique des contrats
                    $tabViewData{'contracts'} = &LOC::Machine::getLastContracts($tabViewData{'country.id'},
                                                                                $tabViewData{'id'}, NBLASTCONTRACTS);
                    $tabViewData{'customerUrl'} = &LOC::Globals::get('locationUrl') .
                                                  '/cgi-bin/old/location/infoclient.cgi?value=%s';

                    # États
                    my $tabStatesExceptions = [
                            LOC::Machine::STATE_RENTED,
                            LOC::Machine::STATE_RECOVERY,
                            LOC::Machine::STATE_ATNRENOVATION,
                            LOC::Machine::STATE_TRANSFER,
                            LOC::Machine::STATE_JLGRENOVATION,
                            LOC::Machine::STATE_DELIVERY,
                            $tabInfos->{'state.id'}
                        ];

                    my $tabStatesFilters = {
                        'module' => 'MACHINE',
                        'exception' => $tabStatesExceptions
                    };
                    # - si visibilité sur une agence, les états possibles sont restreints
                    if ($tabInfos->{'visibleOnAgency.id'})
                    {
                        $tabStatesFilters->{'category'} = [
                            LOC::Machine::STATE_AVAILABLE,
                            LOC::Machine::STATE_REVIEW,
                            LOC::Machine::STATE_CHECK,
                            LOC::Machine::STATE_TRANSFER
                        ];
                    }

                    $tabViewData{'states'} = &LOC::State::getList($tabViewData{'country.id'}, LOC::Util::GETLIST_PAIRS,
                                                                  $tabStatesFilters);

                    # Agences
                    $tabViewData{'agenciesForDelegation'} = &LOC::Agency::getList(LOC::Util::GETLIST_PAIRS);
                    $tabViewData{'agenciesForTransfer'}   = &LOC::Agency::getList(LOC::Util::GETLIST_PAIRS,
                                                                     {'exceptions' => $tabInfos->{'agency.id'}});
                    $tabViewData{'agencies'}              = &LOC::Agency::getList(LOC::Util::GETLIST_ASSOC);

                    # Agences de visibilité
                    $tabViewData{'visibilityAgencies'} = &LOC::Agency::getList(LOC::Util::GETLIST_PAIRS,
                                                                     {'country' => $tabInfos->{'country.id'},
                                                                      'exceptions' => $tabInfos->{'agency.id'}});

                    # Transfert inter-agences en cours
                    if (defined $tabInfos->{'agenciesTransfer.tabInfos'})
                    {
                        my $tabFromAgencyInfos = &LOC::Agency::getInfos($tabInfos->{'agenciesTransfer.tabInfos'}->{'from.id'});
                        $tabInfos->{'agenciesTransfer.tabInfos'}->{'from.label'} = $tabFromAgencyInfos->{'label'};

                        my $tabToAgencyInfos = &LOC::Agency::getInfos($tabInfos->{'agenciesTransfer.tabInfos'}->{'to.id'});
                        $tabInfos->{'agenciesTransfer.tabInfos'}->{'to.label'} = $tabToAgencyInfos->{'label'};
                    }

                    # Informations du contrat en cours
                    my ($currentContractId) = grep($tabViewData{'contracts'}->{$_}->{'isCurrent'} == 1,
                                                   keys(%{$tabViewData{'contracts'}}));
                    my $currentContractCountryId = $tabViewData{'contracts'}->{$currentContractId}->{'country.id'};
                    my $tabCurrentContract = &LOC::Contract::Rent::getInfos($currentContractCountryId, $currentContractId,
                                                                            LOC::Contract::Rent::GETINFOS_BASE |
                                                                            LOC::Contract::Rent::GETINFOS_CUSTOMER |
                                                                            LOC::Contract::Rent::GETINFOS_EXT_SITE);

                    # Présence d'amiante
                    $tabViewData{'infos'}->{'isAsbestos'} = ($currentContractId ? $tabCurrentContract->{'site.tabInfos'}->{'isAsbestos'} : 0);

                    # Paramètres d'affichage de l'historique des modifications
                    $tabViewData{'history'} = {
                            'nbLastModifications' => NBLASTMODIFICATIONS,
                            'nbModificationsStep' => NBMODIFICATIONSSTEP
                        };

                    # Informations Kimoce
                    if ($tabInfos->{'tabRights'}->{'kimoce'}->{'infos'} ||
                        $tabInfos->{'tabRights'}->{'kimoce'}->{'localization'})
                    {
                        my $tabKimoceInfos    = &LOC::Kimoce::getMachineInfos($tabViewData{'infos'}->{'parkNumber'});

                        # - Informations
                        if ($tabKimoceInfos && $tabInfos->{'tabRights'}->{'kimoce'}->{'infos'})
                        {
                            $tabViewData{'kimoce'}->{'infos'}->{'stored'} = {
                                    'ObjInCde'        => $tabKimoceInfos->{'ObjInCde'},
                                    'DosInCde'        => $tabKimoceInfos->{'DosInCde'},
                                    'ObjSitDsc'       => $tabKimoceInfos->{'ObjSitDsc'}
                                };
                            $tabViewData{'kimoce'}->{'infos'}->{'real'} = {
                                    'ObjSitDsc'       => $tabInfos->{'state.nonLocaleLabel'}
                                };
                        }

                        # - Localisation
                        if ($tabKimoceInfos && $tabInfos->{'tabRights'}->{'kimoce'}->{'localization'})
                        {
                            $tabViewData{'kimoce'}->{'localization'}->{'stored'} = {
                                    'CpyInCde'        => $tabKimoceInfos->{'CpyInCde'},
                                    'CpyAddrInCde'    => $tabKimoceInfos->{'CpyAddrInCde'},
                                    'CpyTrdNamDsc'    => $tabKimoceInfos->{'CpyTrdNamDsc'},
                                    'CpyAddrExCde'    => $tabKimoceInfos->{'CpyAddrExCde'}
                                };
                            $tabViewData{'kimoce'}->{'localization'}->{'real'} = {
                                    'CpyInCde'        => $tabInfos->{'CpyInCde'},
                                    'CpyAddrInCde'    => $tabInfos->{'CpyAddrInCde'}
                                };
                        }
                    }

                    # - Compteurs
                    if ($tabInfos->{'tabRights'}->{'kimoce'}->{'counters'})
                    {
                        my $tabKimoceCounters = &LOC::Kimoce::getMachineCounters($tabViewData{'infos'}->{'parkNumber'});

                        if ($tabKimoceCounters)
                        {
                            $tabViewData{'kimoce'}->{'counters'}->{'stored'} = {
                                    'recoveryCounter'      => $tabKimoceCounters->{LOC::Kimoce::COUNTER_RECOVERIES()},
                                    'sitesTransferCounter' => $tabKimoceCounters->{LOC::Kimoce::COUNTER_SITESTRANSFERS()},
                                    'timeCounter'         => $tabKimoceCounters->{LOC::Kimoce::COUNTER_TIME()}
                                };
                        }
                    }

                    # - Caractéristiques
                    if ($tabInfos->{'tabRights'}->{'kimoce'}->{'characteristics'})
                    {
                        my $tabKimoceFeatures = &LOC::Kimoce::getMachineFeatures($tabViewData{'infos'}->{'parkNumber'});

                        if ($tabKimoceFeatures)
                        {
                            $tabViewData{'kimoce'}->{'characteristics'}->{'stored'} = {
                                    'lastBreakdown'   => $tabKimoceFeatures->{LOC::Kimoce::FEAT_BREAKDOWNDATE()},
                                    'lastControl'     => $tabKimoceFeatures->{LOC::Kimoce::FEAT_CONTROLDATE()},
                                    'lastAgencyCheckIn' => $tabKimoceFeatures->{LOC::Kimoce::FEAT_LASTAGENCYCHECKINDATE()},
                                    'customer'        => $tabKimoceFeatures->{LOC::Kimoce::FEAT_CUSTOMERLABEL()},
                                    'fullService'     => $tabKimoceFeatures->{LOC::Kimoce::FEAT_CONTRACTFULLSERVICE()},
                                    'beginDate'       => $tabKimoceFeatures->{LOC::Kimoce::FEAT_CONTRACTBEGINDATE()},
                                    'endDate'         => $tabKimoceFeatures->{LOC::Kimoce::FEAT_CONTRACTENDDATE()},
                                    'site'            => $tabKimoceFeatures->{LOC::Kimoce::FEAT_SITELABEL()},
                                    'address'         => $tabKimoceFeatures->{LOC::Kimoce::FEAT_SITEADDRESS()},
                                    'postalCode'      => $tabKimoceFeatures->{LOC::Kimoce::FEAT_SITEPOSTALCODE()},
                                    'city'            => $tabKimoceFeatures->{LOC::Kimoce::FEAT_SITECITY()},
                                    'siteComment'     => $tabKimoceFeatures->{LOC::Kimoce::FEAT_SITECOMMENT()},
                                    'contact'         => $tabKimoceFeatures->{LOC::Kimoce::FEAT_SITECONTACT()},
                                    'telephone'       => $tabKimoceFeatures->{LOC::Kimoce::FEAT_SITECONTACTTEL()},
                                    'isAsbestos'      => $tabKimoceFeatures->{LOC::Kimoce::FEAT_SITEISASBESTOS()}
                                };
                            $tabViewData{'kimoce'}->{'characteristics'}->{'real'} = {
                                    'lastBreakdown'   => &formatDate($tabInfos->{'crashDate'}),
                                    'lastControl'     => &formatDate($tabInfos->{'control.lastDate'}),
                                    'lastAgencyCheckIn' => &formatDate($tabInfos->{'lastAgencyCheckIn'}),
                                    'customer'        =>
                                        substr($tabCurrentContract->{'customer.name'}, 0, LOC::Kimoce::FEATURE_MAXLENGTH),
                                    'fullService'     => ($tabCurrentContract->{'isFullService'} ?
                                                            'Oui' : ($tabCurrentContract->{'id'} ? 'Non' : '')),
                                    'beginDate'       => &formatDate($tabCurrentContract->{'beginDate'}),
                                    'endDate'         => &formatDate($tabCurrentContract->{'endDate'}),
                                    'site'            =>
                                        substr($tabCurrentContract->{'site.tabInfos'}->{'label'}, 0, LOC::Kimoce::FEATURE_MAXLENGTH),
                                    'address'         =>
                                        &LOC::Util::removeCRLF(substr($tabCurrentContract->{'site.tabInfos'}->{'address'}, 0, LOC::Kimoce::FEATURE_MAXLENGTH)),
                                    'postalCode'      => $tabCurrentContract->{'site.tabInfos'}->{'locality.postalCode'},
                                    'city'            => $tabCurrentContract->{'site.tabInfos'}->{'locality.name'},
                                    'siteComment'     =>
                                        &LOC::Util::removeCRLF(&LOC::Util::substr_ellipsis($tabCurrentContract->{'site.tabInfos'}->{'comments'},
                                                                                                LOC::Kimoce::FEATURE_MAXLENGTH)),
                                    'isAsbestos'      => ($tabCurrentContract->{'site.tabInfos'}->{'isAsbestos'} ?
                                                            'Oui' : 'Non'),
                                    'contact'         =>
                                        substr($tabCurrentContract->{'site.tabInfos'}->{'contact.fullName'},
                                                                0, LOC::Kimoce::FEATURE_MAXLENGTH),
                                    'telephone'       => $tabCurrentContract->{'site.tabInfos'}->{'contact.telephone'}
                            };
                        }
                    }

                    # - Informations additionnelles
                    if ($tabInfos->{'tabRights'}->{'kimoce'}->{'addInfos'})
                    {
                        my $tabKimoceAddInfos = &LOC::Kimoce::getMachineAddInfos($tabViewData{'infos'}->{'parkNumber'});

                        if ($tabKimoceAddInfos)
                        {
                            $tabViewData{'kimoce'}->{'addInfos'}->{'real'} = {};
                            $tabViewData{'kimoce'}->{'addInfos'}->{'stored'} = {};
                            foreach my $code (keys(%{$tabKimoceAddInfos->{$tabInfos->{'parkNumber'}}}))
                            {
                                my ($addInfoId) = grep {$tabViewData{'tabAddInfos'}->{$_}->{'externalCode'} eq $code} keys(%{$tabViewData{'tabAddInfos'}});
                                my $type = $tabKimoceAddInfos->{$tabInfos->{'parkNumber'}}->{$code}->{'EntryTypExCde'};

                                # Transformation de la valeur Gesloc pour le type "Booléen"
                                if ($type eq LOC::Kimoce::CHARTYPE_BOOLEAN)
                                {
                                    if (defined $tabInfos->{'tabAddInfos'}->{$addInfoId} &&
                                        $tabInfos->{'tabAddInfos'}->{$addInfoId} == 0)
                                    {
                                        $tabViewData{'kimoce'}->{'addInfos'}->{'real'}->{$addInfoId} = 'Non';
                                    }
                                    elsif ($tabInfos->{'tabAddInfos'}->{$addInfoId} == 1)
                                    {
                                        $tabViewData{'kimoce'}->{'addInfos'}->{'real'}->{$addInfoId} = 'Oui';
                                    }
                                    else
                                    {
                                        $tabViewData{'kimoce'}->{'addInfos'}->{'real'}->{$addInfoId} = 'Non renseigné';
                                    }
                                }
                                else
                                {
                                    $tabViewData{'kimoce'}->{'addInfos'}->{'real'}->{$addInfoId} =
                                        $tabInfos->{'tabAddInfos'}->{$addInfoId};
                                }

                                # Transformation de la valeur Kimoce pour le type "Booléen"
                                $tabViewData{'kimoce'}->{'addInfos'}->{'stored'}->{$addInfoId} =
                                    $tabKimoceAddInfos->{$tabInfos->{'parkNumber'}}->{$code}->{'ObjCharVal'};
                                if ($tabKimoceAddInfos->{$tabInfos->{'parkNumber'}}->{$code}->{'EntryTypExCde'} eq LOC::Kimoce::CHARTYPE_BOOLEAN &&
                                    $tabKimoceAddInfos->{$tabInfos->{'parkNumber'}}->{$code}->{'ObjCharVal'} eq ' ')
                                {
                                    $tabViewData{'kimoce'}->{'addInfos'}->{'stored'}->{$addInfoId} = 'Non renseigné';
                                }
                            }
                        }
                    }

                    # Télématique
                    my $displayTelematics = 0;

                    if ($tabInfos->{'telematicId'})
                    {
                        $tabViewData{'telematics'} = {};


                        # Liste des types de codes
                        $tabViewData{'telematics'}->{'types'} = &LOC::StartCode::getTypesList($tabInfos->{'country.id'}, LOC::Util::GETLIST_ASSOC);

                        # Correspondance entre les types de code de démarrage et les droits
                        $tabViewData{'tabCodeTypes'} = {
                            LOC::StartCode::Contract::Rent::TYPE_CUSTOMER => 'customer',
                            LOC::StartCode::Contract::Rent::TYPE_BACKUP   => 'backup',
                            LOC::StartCode::Static::TYPE_TECHNICIAN       => 'technician',
                            LOC::StartCode::Static::TYPE_DRIVER           => 'driver',
                            LOC::StartCode::Static::TYPE_AFTERSALES       => 'aftersales',
                            LOC::StartCode::Static::TYPE_HELP             => 'help',
                            LOC::StartCode::Static::TYPE_EXTERNALDRIVER   => 'externalDriver',
                            LOC::StartCode::Static::TYPE_EXTERNALTECHNICIAN => 'externalTechnician'
                        };


                        # Rangement des codes par types suivant si il s'agit d'un code statique ou non
                        my $tabStartCodes = {
                            'static'   => {},
                            'contract' => {}
                        };
                        my %tabStaticCodes   = ();
                        my %tabContractCodes = ();


                        if ($tabViewData{'viewType'} eq VIEWTYPE_FULL)
                        {
                            # Récupération des codes statiques
                            my $tabFilters = {
                                'rule' => {
                                    'machineId' => $tabViewData{'id'},
                                    'isCurrent' => 1
                                }
                            };
                            %tabStaticCodes = &LOC::StartCode::Static::getList(
                                $tabInfos->{'country.id'},
                                LOC::Util::GETLIST_ASSOC,
                                $tabFilters,
                                LOC::StartCode::Static::GETINFOS_RULESLIST
                            );

                            while (my ($startCodeId, $tabStartCode) = each(%tabStaticCodes))
                            {
                                my $tab = $tabStartCodes->{'static'};

                                if (!defined $tab->{$tabStartCode->{'type.id'}})
                                {
                                    $tab->{$tabStartCode->{'type.id'}} = {
                                        'name'       => $tabStartCode->{'type.name'},
                                        'code'       => $tabStartCode->{'type.code'},
                                        'order'      => $tabStartCode->{'type.order'},
                                        'startCodes' => []
                                    };
                                }

                                # Récupération de toutes les règles du code
                                my @tabRules = ();
                                while (my ($ruleId, $tabRule) = each(%{$tabStartCode->{'rulesList'}}))
                                {
                                    push(@tabRules, {
                                        'beginDate'       => $tabRule->{'beginDate'},
                                        'endDate'         => $tabRule->{'endDate'},
                                        'applicationId'   => $tabRule->{'application.id'},
                                        'unapplicationId' => $tabRule->{'unapplication.id'}
                                    });
                                }

                                my $nbRules        = @tabRules;
                                my $nbAppliedRules = (grep { $_->{'applicationId'} ne LOC::StartCode::STATE_RULE_APPLICATION_PENDING } @tabRules);
                                my $nbUnappPendingRules = (grep { $_->{'unapplicationId'} eq LOC::StartCode::STATE_RULE_UNAPPLICATION_PENDING } @tabRules);

                                push(@{$tab->{$tabStartCode->{'type.id'}}->{'startCodes'}}, {
                                    'code'  => $tabStartCode->{'code'},
                                    'date'  => $tabStartCode->{'creationDate'},
                                    'rules' => \@tabRules,
                                    'state' => ($nbRules == $nbUnappPendingRules ? 'unapp-pending' :
                                                        ($nbAppliedRules > 0 ? 'applied' : 'pending'))
                                });
                            }
                        }

                        # Récupération des codes contrats
                        my $tabFilters = {
                            'rule' => {
                                'machineId' => $tabViewData{'id'},
                                'isCurrent' => 1
                            }
                        };
                        %tabContractCodes = &LOC::StartCode::Contract::Rent::getList(
                            $tabInfos->{'country.id'},
                            LOC::Util::GETLIST_ASSOC,
                            $tabFilters,
                            LOC::StartCode::Contract::Rent::GETINFOS_RULESLIST |
                            LOC::StartCode::Contract::Rent::GETINFOS_LINKEDCONTRACTS
                        );

                        while (my ($startCodeId, $tabStartCode) = each(%tabContractCodes))
                        {
                            my @tabLinkedContracts = values %{$tabStartCode->{'linkedContracts'}};

                            # Cas des codes (en cours de suppression) non rattachés à un contrat
                            if (!$tabStartCode->{'hasBeenActivated'} || @tabLinkedContracts == 0)
                            {
                                @tabLinkedContracts = ({
                                    'id'      => undef,
                                    'code'    => '',
                                    'endDate' => ''
                                });
                            }

                            my $currentDate = &LOC::Date::getMySQLDate(LOC::Date::FORMAT_DATE);
                            foreach my $tabLinkedContract (@tabLinkedContracts)
                            {
                                my $contractId = $tabLinkedContract->{'id'};
                                if (!defined $tabStartCodes->{'contract'}->{$contractId})
                                {
                                    my $url = '';
                                    if ($contractId ne '')
                                    {
                                        $url = &LOC::Request::createRequestUri('rent:rentContract:view',
                                                                              {'contractId' => $contractId});
                                    }

                                    my $regenerateBtn = ($tabInfos->{'tabRights'}->{'telematics'}->{'regenerateRentContractCodes'});

                                    $tabStartCodes->{'contract'}->{$contractId} = {
                                        'id'            => $contractId,
                                        'code'          => $tabLinkedContract->{'code'},
                                        'endDate'       => $tabLinkedContract->{'endDate'},
                                        'beginDate'     => $tabLinkedContract->{'beginDate'},
                                        'url'           => $url,
                                        'order'         => ($contractId ne '' ? 1 : 2),
                                        'regenerateBtn' => $regenerateBtn,
                                        'list'          => {}
                                    };
                                }

                                my $tab = $tabStartCodes->{'contract'}->{$contractId}->{'list'};


                                if (!defined $tab->{$tabStartCode->{'type.id'}})
                                {
                                    $tab->{$tabStartCode->{'type.id'}} = {
                                        'name'       => $tabStartCode->{'type.name'},
                                        'code'       => $tabStartCode->{'type.code'},
                                        'order'      => $tabStartCode->{'type.order'},
                                        'startCodes' => []
                                    };
                                }

                                # Récupération de toutes les règles du code
                                my @tabRules = ();
                                while (my ($ruleId, $tabRule) = each(%{$tabStartCode->{'rulesList'}}))
                                {
                                    push(@tabRules, {
                                        'beginDate'       => $tabRule->{'beginDate'},
                                        'endDate'         => $tabRule->{'endDate'},
                                        'applicationId'   => $tabRule->{'application.id'},
                                        'unapplicationId' => $tabRule->{'unapplication.id'}
                                    });
                                }

                                my $nbRules        = @tabRules;
                                my $nbAppliedRules = (grep { $_->{'applicationId'} ne LOC::StartCode::STATE_RULE_APPLICATION_PENDING } @tabRules);
                                my $nbUnappPendingRules = (grep { $_->{'unapplicationId'} eq LOC::StartCode::STATE_RULE_UNAPPLICATION_PENDING } @tabRules);

                                push(@{$tab->{$tabStartCode->{'type.id'}}->{'startCodes'}}, {
                                    'code'    => $tabStartCode->{'code'},
                                    'date'    => $tabStartCode->{'creationDate'},
                                    'rules'   => \@tabRules,
                                    'state' => ($nbRules == $nbUnappPendingRules ? 'unapp-pending' :
                                                        ($nbAppliedRules > 0 ? 'applied' : 'pending')),
                                    'sendSMS' => &LOC::StartCode::Contract::Rent::isSmsSendable($tabStartCode, $tabLinkedContract)
                                });
                            }
                        }


                        $tabViewData{'telematics'}->{'isTelematicSuspended'} = $tabInfos->{'isTelematicSuspended'};
                        $tabViewData{'telematics'}->{'isDigiKeyDeactivated'} = $tabInfos->{'isDigiKeyDeactivated'};
                        $tabViewData{'telematics'}->{'startCodes'}           = $tabStartCodes;

                        $tabViewData{'telematics'}->{'history'} = [];#\@tabStartCodesHistory;

                        # Droits
                        my $tabRights = $tabInfos->{'tabRights'}->{'telematics'};

                        # Affichage ou non de chacun des éléments
                        my $tabDisplay = {};

                        # - Immobilisation
                        $tabDisplay->{'immobilization'} = ($tabRights->{'immobilization'} && $tabInfos->{'isImmobilized'}) * 1;

                        # - Chacun des codes
                        my %tabAllStartCodes = (%tabStaticCodes, %tabContractCodes);
                        foreach my $type (keys(%{$tabViewData{'tabCodeTypes'}}))
                        {
                            my $nbCodes = grep { $_->{'type.code'} eq $type } values(%tabAllStartCodes);
                            my $element = $tabViewData{'tabCodeTypes'}->{$type};
                            $tabDisplay->{$element} = ($tabRights->{$element} && $nbCodes > 0) * 1;
                        }

                        # Affichage des codes statiques
                        my @tabStaticTypes = map {
                            $tabViewData{'tabCodeTypes'}{$_->{'code'}}
                        } grep {
                            $_->{'scope'} eq LOC::StartCode::Static::SCOPE
                        } values(%{$tabViewData{'telematics'}->{'types'}});
                        $tabDisplay->{'static'} = 0;
                        foreach my $type (@tabStaticTypes)
                        {
                            if ($tabDisplay->{$type})
                            {
                                $tabDisplay->{'static'} = 1;
                                last;
                            }
                        }

                        # Affichage des codes contrats
                        my @tabContractTypes = map {
                            $tabViewData{'tabCodeTypes'}{$_->{'code'}}
                        } grep {
                            $_->{'scope'} eq LOC::StartCode::Contract::Rent::SCOPE
                        } values(%{$tabViewData{'telematics'}->{'types'}});
                        $tabDisplay->{'contract'} = 0;
                        foreach my $type (@tabContractTypes)
                        {
                            if ($tabDisplay->{$type})
                            {
                                $tabDisplay->{'contract'} = 1;
                                last;
                            }
                        }

                        # Affichage du bouton de déséquipement ?
                        $tabDisplay->{'disableBtn'} = $tabViewData{'viewType'} eq VIEWTYPE_FULL &&
                                                      $tabInfos->{'tabRights'}->{'telematics'}->{'disable'} &&
                                                      $tabInfos->{'possibilities'}->{'telematics'}->{'disable'};
                        # Affichage du bouton de suspension/réactivation ?
                        $tabDisplay->{'suspendBtn'} = $tabViewData{'viewType'} eq VIEWTYPE_FULL &&
                                                      $tabInfos->{'tabRights'}->{'telematics'}->{'toggle'} &&
                                                      $tabInfos->{'possibilities'}->{'telematics'}->{'toggle'};
                        # Affichage du bouton d'activation des codes contrats
                        $tabDisplay->{'activateRentContractScopeBtn'} = $tabViewData{'viewType'} eq VIEWTYPE_FULL &&
                                                                        $tabInfos->{'tabRights'}->{'telematics'}->{'activateRentContractScope'} &&
                                                                        $tabInfos->{'possibilities'}->{'telematics'}->{'activateRentContractScope'};

                        # Affichage du bouton d'historique
                        $tabDisplay->{'history'} = $tabDisplay->{'static'} || $tabDisplay->{'contract'};

                        # Affichage du bouton d'initialisation du boîtier suite à un changement
                        $tabDisplay->{'changeEquipment'} = $tabInfos->{'tabRights'}->{'telematics'}->{'changeEquipment'};

                        # Affichage d'activation/désactivation du digicode
                        $tabDisplay->{'digiKeyActivationBtn'} = $tabViewData{'viewType'} eq VIEWTYPE_FULL &&
                                                                $tabInfos->{'tabRights'}->{'telematics'}->{'digiKeyActivation'} &&
                                                                $tabInfos->{'possibilities'}->{'telematics'}->{'digiKeyActivation'};

                        # Affichage du bloc si au moins un de ses éléments doit être affiché
                        $displayTelematics = $tabDisplay->{'static'} ||
                                             $tabDisplay->{'contract'} ||
                                             $tabDisplay->{'disableBtn'} ||
                                             $tabDisplay->{'activateRentContractScopeBtn'};

                        # Lien vers la page de gestion des codes statiques
                        $tabDisplay->{'staticCodesPage'} = &LOC::Session::isAccessAuthorized('superv', 'telematics', 'view');

                        $tabViewData{'telematics'}->{'display'} = $tabDisplay;
                    }

                    $tabViewData{'displayTelematics'} = $displayTelematics;

                    # Calcul des erreurs dans les informations Kimoce
                    foreach my $key (keys(%{$tabViewData{'kimoce'}}))
                    {
                        my @tabKimoceErrors = grep(&LOC::Util::trim($tabViewData{'kimoce'}->{$key}->{'stored'}->{$_}) ne
                                                        &LOC::Util::trim($tabViewData{'kimoce'}->{$key}->{'real'}->{$_}),
                                                   keys(%{$tabViewData{'kimoce'}->{$key}->{'real'}}));
                        $tabViewData{'kimoce'}->{$key}->{'nbErrors'} = @tabKimoceErrors;
                    }
                }
            }
        }
    }


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/machine/' . $redirect . '.cgi';
}


# Function: createAction
# Création de machine
#
sub createAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};


    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'country.id'     => $tabUserInfos->{'nomadCountry.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
    );

    my $valid = &LOC::Request::getString('valid', '');
    my $tabErrors = {};

    # Enregistrement du bloc actions
    if ($valid eq 'ok')
    {
        $tabViewData{'tabData'} = {
            'parkNumber'       => &LOC::Request::getInteger('parkNumber', undef),
            'model.id'         => &LOC::Request::getInteger('model.id', undef),
            'serialNumber'     => &LOC::Request::getString('serialNumber', undef),
            'control.nextDate' => &LOC::Request::get('control.date'),
            'agency.id'        => &LOC::Request::getString('agency.id')
        };
        &LOC::Machine::insert($tabViewData{'country.id'}, $tabViewData{'tabData'}, $tabUserInfos->{'id'}, $tabErrors);

        # Construction de la trame de retour
        my $nbErrors = @{$tabErrors->{'fatal'}};
        if ($nbErrors > 0)
        {
            $tabViewData{'tabData'}->{'@state'} = 'ko';
            $tabViewData{'tabData'}->{'@perlErrors'} = $tabErrors;
            if (defined($tabViewData{'tabData'}->{'model.id'}))
            {
                $tabViewData{'tabModelInfos'} = &LOC::Model::getInfos($tabViewData{'country.id'}, $tabViewData{'tabData'}->{'model.id'});
            }

            my @tabCodeErrors = (); # 0 pour être sûr d'avoir un numéro d'erreur dans le tableau !!

            # Erreur sur la création de la machine
            if (&LOC::Util::in_array(0x0001, $tabErrors->{'fatal'}))
            {
                push(@tabCodeErrors, 'fatalError'); # Erreur fatale
            }
            # Champs non renseignés
            if (&LOC::Util::in_array(0x0030, $tabErrors->{'fatal'}))
            {
                push(@tabCodeErrors, 'requiredFields');
            }
            # Modèle de machine inconnu
            if (&LOC::Util::in_array(0x0031, $tabErrors->{'fatal'}))
            {
                push(@tabCodeErrors, 'unknownModel');
            }
            # Parc déjà existant
            if (&LOC::Util::in_array(0x0032, $tabErrors->{'fatal'}))
            {
                push(@tabCodeErrors, 'existingParkNumber');
            }
            # Numéro de série existant pour ce modèle
            if (&LOC::Util::in_array(0x0033, $tabErrors->{'fatal'}))
            {
                push(@tabCodeErrors, 'existingSerialNumber');
            }

            # Si pas d'erreur alors erreur inconnue !
            if (@tabCodeErrors == 0)
            {
                push(@tabCodeErrors, 'unknownError'); # Erreur inconnue !
            }

            $tabViewData{'tabData'}->{'@errors'} = \@tabCodeErrors;
        }
        else
        {
            $tabViewData{'tabData'} = {
                '@state' => 'ok',
                '@valid' => 'success',
                'parkNumber' => $tabViewData{'tabData'}->{'parkNumber'}
            };
        }
    }


    # Liste des agences
    my $agenciesList     = &LOC::Agency::getList(LOC::Util::GETLIST_PAIRS, {'type' => [LOC::Agency::TYPE_AGENCY, LOC::Agency::TYPE_AFTERSALES]});
    my $headOfficeAgency = &LOC::Agency::getList(LOC::Util::GETLIST_PAIRS, {'id' => LOC::Agency::ID_HEADOFFICE});
    my %agenciesListMerged = (%$agenciesList, %$headOfficeAgency);
    $tabViewData{'agenciesList'} = LOC::Util::sortHashByValues(\%agenciesListMerged);

    $tabViewData{'maxParkNumber'}   = &_getMaxParkNumber($tabViewData{'country.id'});
    $tabViewData{'defaultAgencyId'} = LOC::Agency::ID_HEADOFFICE;

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/machine/create.cgi';
}


# Function: jsonModelListAction
# Récupérer la liste des modèles de machines (avec recherche possible)
#
sub jsonModelListAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'  => $tabUserInfos->{'locale.id'},
        'country.id' => $tabUserInfos->{'nomadCountry.id'},
        'errorCode' => ''
    );

    # Récupération des paramètres
    my $countryId    = &LOC::Request::getString('countryId', $tabViewData{'country.id'});
    my $searchValue  = &LOC::Util::trim(&LOC::Request::getString('searchValue', ''));

    if ($searchValue eq '')
    {
        $tabViewData{'errorCode'} = 'emptysearch';
        $tabViewData{'result'} = {
            'returnCode' => 'error'
        };
    }
    else
    {
        # Liste des modèles
        my $tabFilters = {'fullName' => $searchValue};

        # Récupération de la liste des modèles de machine
        my $nbResult = &LOC::Model::getList($countryId, LOC::Util::GETLIST_COUNT, $tabFilters);
        if ($nbResult == 0)
        {
            $tabViewData{'errorCode'} = 'noresult';
            $tabViewData{'result'} = {
                'returnCode' => 'error'
            };
        }
        elsif ($nbResult < 100)
        {
            my $result = &LOC::Model::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters);

            my $tab = [];
            foreach (keys %$result)
            {
                push(@$tab, {'id'    => $result->{$_}->{'id'},
                             'label' => $result->{$_}->{'label'}});
            }

            $tabViewData{'result'} = {
                'returnCode' => 'ok',
                'tabData'    => $tab
            };
        }
        else
        {
            $tabViewData{'errorCode'} = 'exceeded';
            $tabViewData{'result'} = {
                'returnCode' => 'warning'
            };
        }
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/machine/jsonModelList' . ($tabViewData{'errorCode'} ne '' ? 'Err' : '') . '.cgi';
}


sub formatDate
{
    my ($date) = @_;

    if (length($date) != 10)
    {
        return $date;
    }

    return substr($date, 8, 2) . '/' . substr($date, 5, 2) . '/' . substr($date, 0, 4);
}

# Function: jsonHistoryAction
# Récupérer les dernières modifications sur une machine
#
sub jsonHistoryAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Récupération des paramètres
    my $machineId = &LOC::Request::getInteger('machineId');
    my $localeId  = &LOC::Request::getString('localeId');
    my $number    = &LOC::Request::getInteger('number');

    # Variables pour la vue
    our %tabViewData = (
        'locale.id' => $localeId,
        'errorCode' => ''
    );

    if (!$machineId || !$localeId)
    {
        $tabViewData{'errorCode'} = 'emptysearch';
        $tabViewData{'result'} = {
            'returnCode' => 'error'
        };
    }
    else
    {
        # Liste des pays
        my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);

        # Récupération de l'historique
        my @tabHistory = ();
        foreach my $countryId (@$tabCountries)
        {
            push(@tabHistory, &LOC::Machine::getHistory($countryId, $machineId, $localeId));
        }
        @tabHistory = sort { -($a->{'datetime'} cmp $b->{'datetime'}) } @tabHistory;

        # Pas de résultat
        if (@tabHistory == 0)
        {
            $tabViewData{'errorCode'} = 'noresult';
            $tabViewData{'result'} = {
                'returnCode' => 'error'
            };
        }
        # Résultat
        else
        {
            # On n'envoie que les données qui nous intéressent
            my @tabResult = ();
            foreach my $history (@tabHistory)
            {
                my $tabElement = {
                    'datetime'        => $history->{'datetime'},
                    'user.fullName'   => $history->{'user.fullName'},
                    'user.state.id'   => $history->{'user.state.id'},
                    'eventType.label' => $history->{'eventType.label'},
                    'content'         => $history->{'content'} . ''
                };
                push(@tabResult, $tabElement);

                if ($number && @tabResult == $number)
                {
                    last;
                }
            }

            $tabViewData{'result'} = {
                'returnCode' => 'ok',
                'nbTotal'    => scalar @tabHistory,
                'tabData'    => \@tabResult
            };
        }
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/machine/jsonHistory' . ($tabViewData{'errorCode'} ne '' ? 'Err' : '') . '.cgi';
}

# Function: searchAction
# Recherche de machine
sub searchAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    our %tabSearchData = ();

    my $countryId = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});

    # Données du formulaire
    $tabSearchData{'country.id'}             = $countryId;
    $tabSearchData{'search.parkNo'}          = &LOC::Request::getString('search.parkNo');

    # Résultats de la recherche
    my $tabFilters = {};

    # Filtre sur le n° de parc
    if ($tabSearchData{'search.parkNo'} ne '')
    {
        $tabFilters->{'parkNumber'} = $tabSearchData{'search.parkNo'};
    }

    # Nombre de critères sélectionnés
    $tabViewData{'nbFilters'} = keys %$tabFilters;

    # s'il y a assez de critères sélectionnés
    if ($tabViewData{'nbFilters'} > 0)
    {
        $tabViewData{'nbResult'} = &LOC::Machine::getList($countryId, LOC::Util::GETLIST_COUNT, $tabFilters);

        if ($tabViewData{'nbResult'} == 1)
        {
            $tabViewData{'result'} = &LOC::Machine::getList($countryId, LOC::Util::GETLIST_PAIRS, $tabFilters,
                                                            LOC::Machine::GETINFOS_BASE);
        }
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/machine/search.cgi';
}

sub startCodesHistoryAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'country.id'     => $tabUserInfos->{'nomadCountry.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    $tabViewData{'list'} = [];

    my $machineId = &LOC::Request::getInteger('machineId');

    # Informations sur la machine
    my $tabInfos = &LOC::Machine::getInfos($tabViewData{'country.id'},
                                           $machineId,
                                           LOC::Machine::GETINFOS_RIGHTS);
    if ($tabInfos)
    {
        # Correspondance entre les types de code de démarrage et les droits
        my $tabCodeTypes = &_getStartCodeTypesForRights();

        # Historique des codes
        my $tabFilters = {
            'rule' => {
                'machineId'       => $machineId,
                'stateId'         => LOC::StartCode::STATE_RULE_OBSOLETE,
                'applicationId'   => LOC::StartCode::STATE_RULE_APPLICATION_DONE,
                'unapplicationId' => [LOC::StartCode::STATE_RULE_UNAPPLICATION_DONE, LOC::StartCode::STATE_RULE_UNAPPLICATION_NOTDONE]
            }
        };

        my %tabLastContractCodes = &LOC::StartCode::Contract::Rent::getList(
            $tabInfos->{'country.id'},
            LOC::Util::GETLIST_ASSOC,
            $tabFilters,
            LOC::StartCode::Contract::Rent::GETINFOS_RULESLIST | LOC::StartCode::Contract::Rent::GETINFOS_LINKEDCONTRACTS);

        my %tabLastStaticCodes = &LOC::StartCode::Static::getList(
            $tabInfos->{'country.id'},
            LOC::Util::GETLIST_ASSOC,
            $tabFilters,
            LOC::StartCode::Contract::Rent::GETINFOS_RULESLIST);

        my %tabAllLastStartCodes = (%tabLastContractCodes, %tabLastStaticCodes);

        my @tabStartCodesHistory;

        while (my ($startCodeId, $tabStartCode) = each(%tabAllLastStartCodes))
        {
            if ($tabInfos->{'tabRights'}->{'telematics'}->{$tabCodeTypes->{$tabStartCode->{'type.code'}}})
            {
                my $element = {
                    'scope'        => $tabStartCode->{'type.scope'},
                    'type'         => $tabStartCode->{'type.name'},
                    'code'         => $tabStartCode->{'code'},
                    'contractId'   => undef,
                    'contractCode' => undef,
                    'beginDate'    => undef,
                    'endDate'      => undef
                };

                # - Récupération dates de la dernière règle
                my @tabRules = values(%{$tabStartCode->{'rulesList'}});
                @tabRules = sort { -($a->{'endDate'} cmp $b->{'endDate'}) } @tabRules;
                my $lastRule = $tabRules[0];

                $element->{'beginDate'} = $lastRule->{'beginDate'};
                $element->{'endDate'} = $lastRule->{'endDate'};

                # - Récupération dernier contrat associé
                if ($tabStartCode->{'type.scope'} eq LOC::StartCode::Contract::Rent::SCOPE)
                {
                    my @tabContracts = values(%{$tabStartCode->{'linkedContracts'}});
                    @tabContracts = sort { -($a->{'beginDate'} cmp $b->{'beginDate'}) } @tabContracts;
                    my $lastContract = @tabContracts[0];
                    $element->{'contractId'} = $lastContract->{'id'};
                    $element->{'contractCode'} = $lastContract->{'code'};
                }

                push(@tabStartCodesHistory, $element);
            }
        }

        # -- Tri par dates de fin puis date de début puis type de code
        @tabStartCodesHistory = sort {-($a->{'endDate'} cmp $b->{'endDate'}) ||
                                      -($a->{'beginDate'} cmp $b->{'beginDate'}) ||
                                      $a->{'type'} cmp $b->{'type'}} @tabStartCodesHistory;

        $tabViewData{'list'} = \@tabStartCodesHistory;
    }

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/machine/startCodesHistory.cgi';
}

# Function: toFinishListAction
# Création de machine(s) à finaliser
sub toFinishListAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId  = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $agencyId   = &LOC::Request::getString('agencyId', $tabUserInfos->{'nomadAgency.id'});
    

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );
    $tabViewData{'isKimoceCallable'}   = &LOC::Kimoce::isCallable();

    # Récupération de la validation
    my $validInfos = &LOC::Request::getString('valid', '');
    my $tabErrors = {};
    if ($validInfos ne '')
    {
        my $tabValidInfos = &LOC::Json::fromJson($validInfos);
        if ($tabValidInfos->{'valid'} eq 'ok')
        {
            # Tableau de retour
            $tabViewData{'return'} = {
                'success' => [],
                'errors'  => [],
                'infos'   => []
            };

            # Traitement de chaque élément sélectionné
            foreach my $machineInfos (@{$tabValidInfos->{'tabMachines'}})
            {
                my $tabOptions = {};
                # Mise à jour
                if (&LOC::Machine::finishCreation($countryId, $machineInfos->{'id'},
                                              $tabUserInfos->{'id'}, $tabOptions, $tabErrors, undef))
                {
                    push(@{$tabViewData{'return'}->{'success'}}, {
                        'id'         => $machineInfos->{'id'},
                        'parkNumber' => $machineInfos->{'parkNumber'}
                    });
                }
                else
                {
                    my $errorCode = ();

                    my $nbErrors = @{$tabErrors->{'fatal'}};
                    if ($nbErrors > 0)
                    {
                        # Erreur fatale
                        if (&LOC::Util::in_array(LOC::Machine::ERROR_FATAL, $tabErrors->{'fatal'}))
                        {
                            $errorCode = 'fatalError';
                        }
                        # Erreur de droits
                        elsif (&LOC::Util::in_array(LOC::Machine::ERROR_RIGHTS, $tabErrors->{'fatal'}))
                        {
                            $errorCode = 'rightsError';
                        }
                        # Erreur Kimoce indisponible
                        elsif (&LOC::Util::in_array(LOC::Machine::ERROR_KIMOCEUNAVAILABLE, $tabErrors->{'fatal'}))
                        {
                            $errorCode = 'kimoceUnavailable';
                        }
                        # Erreur création machine déjà finalisée
                        elsif (&LOC::Util::in_array(LOC::Machine::ERROR_CREATIONALREADYFINISHED, $tabErrors->{'fatal'}))
                        {
                            $errorCode = 'alreadyFinished';
                        }
                        # Erreur machine inconnue dans Kimoce
                        elsif (&LOC::Util::in_array(LOC::Machine::ERROR_UNKNOWNMACHINEINKIMOCE, $tabErrors->{'fatal'}))
                        {
                            $errorCode = 'unknownMachineInKimoce';
                        }
                        
                    }

                    push(@{$tabViewData{'return'}->{'errors'}}, {
                        'id'         => $machineInfos->{'id'},
                        'parkNumber' => $machineInfos->{'parkNumber'},
                        'error'      => $errorCode
                    });
                }

                if (!$tabViewData{'isKimoceCallable'})
                {
                    push(@{$tabViewData{'return'}->{'infos'}}, 'kimoce-unsaved');
                }
            }
        }
    }
    
    # Récupération des informations pour la vue
    $tabViewData{'tabMachines'} = &_getToFinishMachinesList($countryId, $agencyId);

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;

    require $directory . '/view/machine/toFinishList.cgi';
}


# Function: _getMachinesList
# Récupération des machines dont la création doit être finalisée
#
# Params:
# string  $countryId - Identifiant du pays
# string  $agencyId  - Identifiant de l'agence
sub _getToFinishMachinesList
{
    my ($countryId, $agencyId) = @_;

    my $tabFilters = {
        'isCreationFinished' => 0
    };
    my $tabOptions = {
        'level' => LOC::Machine::LEVEL_MODEL
    };

    my $tabList = &LOC::Machine::getList($countryId,
                                         LOC::Util::GETLIST_ASSOC,
                                         $tabFilters,
                                         LOC::Machine::GETINFOS_ALL,
                                         $tabOptions);

    my @tabMachinesList;
    foreach my $tabInfos (values %$tabList)
    {
        my $tabMachineInfos = {
            'id'               => $tabInfos->{'id'},
            'parkNumber'       => $tabInfos->{'parkNumber'},
            'model.label'      => $tabInfos->{'model.label'},
            'serialNumber'     => $tabInfos->{'serialNumber'},
            'purchaseDate'     => $tabInfos->{'purchaseDate'},
            'control.lastDate' => $tabInfos->{'control.lastDate'},
            'agency.id'        => $tabInfos->{'agency.id'},
            'state.label'      => $tabInfos->{'state.label'},
            'isFinalizable'    => $tabInfos->{'tabRights'}->{'actions'}->{'finishCreation'}
        };

        push (@tabMachinesList, $tabMachineInfos);
    }

    return \@tabMachinesList;
    
}

sub _getStartCodeTypesForRights
{
    return {
        LOC::StartCode::Contract::Rent::TYPE_CUSTOMER   => 'customer',
        LOC::StartCode::Contract::Rent::TYPE_BACKUP     => 'backup',
        LOC::StartCode::Static::TYPE_TECHNICIAN => 'technician',
        LOC::StartCode::Static::TYPE_DRIVER     => 'driver',
        LOC::StartCode::Static::TYPE_AFTERSALES => 'aftersales',
        LOC::StartCode::Static::TYPE_HELP       => 'help',
        LOC::StartCode::Static::TYPE_EXTERNALDRIVER   => 'externalDriver',
        LOC::StartCode::Static::TYPE_EXTERNALTECHNICIAN => 'externalTechnician'
    };
}

sub _getMaxParkNumber
{
    my ($countryId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $hasTransaction = $db->hasTransaction();
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    my $query = '
SELECT MAX(MANOPARC)
FROM AUTH.`MACHINE`';

    return $db->fetchOne($query) + 1;
}

1;