use utf8;
use open (':encoding(UTF-8)');

# Package: stock
# Exemple de contrôleur
package stock;

use strict;
use File::Basename;

use LOC::Agency;
use LOC::Model;

# Function: viewAction
# Affichage du stock
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'  => $tabUserInfos->{'locale.id'},
        'country.id' => &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'}, LOC::Request::SRC_REQUEST),
        'agency.id'  => &LOC::Request::getString('agencyId', $tabUserInfos->{'nomadAgency.id'}, LOC::Request::SRC_REQUEST),
        'model.id'   => &LOC::Request::getInteger('modelId', undef, LOC::Request::SRC_REQUEST),
        'search'     => &LOC::Request::getString('search', undef, LOC::Request::SRC_REQUEST),
    );

    my $tabAgencyInfos = &LOC::Agency::getInfos($tabViewData{'agency.id'});
    $tabViewData{'agency.label'} = $tabAgencyInfos->{'label'};

    my $tabStocksFilters = {
        'isRentable' => 1
    };
    if ($tabViewData{'search'} ne '')
    {
        $tabStocksFilters->{'search'} = $tabViewData{'search'};
    }
    elsif ($tabViewData{'model.id'} != 0)
    {
        $tabStocksFilters = {'model.id' => $tabViewData{'model.id'}};

        my $tabModelInfos = &LOC::Model::getInfos($tabViewData{'country.id'}, $tabViewData{'model.id'});
        $tabViewData{'model.label'} = $tabModelInfos->{'label'};
    }
    $tabViewData{'tabStock'}  = &LOC::Model::getStocksList($tabViewData{'country.id'}, $tabViewData{'agency.id'}, $tabStocksFilters);
    $tabViewData{'nbResults'} = keys(%{$tabViewData{'tabStock'}});


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/stock/view.cgi';
}


1;
