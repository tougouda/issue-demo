use utf8;
use open (':encoding(UTF-8)');

# Package: list
# Affichage de la liste des machines
package list;

use strict;
use File::Basename;

use LOC::Machine;
use LOC::AjaxTable;
use LOC::Area;
use LOC::Country;



# Function: viewAction
# Affichage de la liste des machines
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $countryId    = $tabUserInfos->{'nomadCountry.id'};

    my $isTelematActived = (&LOC::Characteristic::getCountryValueByCode('TELEMAT', $countryId) != 0 ? 1 : 0);


    # Variables pour la vue
    our %tabViewData = (
        'locale.id'        => $tabUserInfos->{'locale.id'},
        'user.id'          => $tabUserInfos->{'id'},
        'user.agency.id'   => $tabUserInfos->{'nomadAgency.id'},
        'user.country.id'  => $tabUserInfos->{'nomadCountry.id'},
        'currencySymbol'   => $tabUserInfos->{'currency.symbol'},
        'timeZone'         => $tabSession->{'timeZone'},
        'isTelematActived' => $isTelematActived
    );

    # Type d'affichage
    my $displayType = &LOC::Request::getString('displayType', '');
    my $excelRowsLimit = &LOC::Characteristic::getAgencyValueByCode('LSTMACHXLSMAX',
                                                                    $tabUserInfos->{'nomadAgency.id'},
                                                                    undef, 0) * 1;
    my $displayMode = $tabParameters->{'_mode'};


    # Récupération du nombre d'éléments par page
    my $pageSize   = &LOC::Request::getInteger('pageSize', 20);
    my $startIndex = &LOC::Request::getInteger('startIndex', 0);
    # Récupération des tris demandés
    my $tabSorts   = &LOC::Request::getArray('sorts', [{'col' => 'parkNumber', 'dir' => 'asc'}]);

    my $tabOptions = {};
    my $tabDefaultActivesFilters = {};

    # Interfaces basées sur la recherche machine 
    if ($displayMode eq 'visibility')
    {
        $tabDefaultActivesFilters->{'isVisibleOnAgency'} = [$tabViewData{'user.agency.id'}];
    }

    if ($displayType eq '' && !$displayMode)
    {
        # Filtres actifs par défaut
        my @tabAutorCountries = @{$tabUserInfos->{'tabAuthorizedCountries'}};
        my @tabAutorAreas = @{$tabUserInfos->{'tabAuthorizedAreas'}};
        my @tabAutorAgencies = keys %{$tabUserInfos->{'tabAuthorizedAgencies'}};

        # Pays
        if (@tabAutorCountries > 0)
        {
            if (@tabAutorCountries < &LOC::Country::getList(LOC::Util::GETLIST_COUNT))
            {
                $tabDefaultActivesFilters->{'country.id'} = \@tabAutorCountries;
            }
        }
        # Régions
        elsif (@tabAutorAreas > 0)
        {
            # ajout d'une région supplémentaire dans le calcul du nombre de régions
            # afin de pouvoir sélectionner toutes les régions sauf les agences "sans région"
            my $nbAreas = &LOC::Area::getList(LOC::Util::GETLIST_COUNT) + 1;
            if (@tabAutorAreas < $nbAreas)
            {
                $tabDefaultActivesFilters->{'area.id'} = \@tabAutorAreas;
            }
        }
        # Agences
        elsif (@tabAutorAgencies > 0)
        {
            if (@tabAutorAgencies < &LOC::Agency::getList(LOC::Util::GETLIST_COUNT))
            {
                $tabDefaultActivesFilters->{'agency.id'} = \@tabAutorAgencies;
            }
        }
        # États
        $tabDefaultActivesFilters->{'state.id'} = [LOC::Machine::STATE_RENTED,
                                                   LOC::Machine::STATE_AVAILABLE,
                                                   LOC::Machine::STATE_RECOVERY,
                                                   LOC::Machine::STATE_REVIEW,
                                                   LOC::Machine::STATE_CONTROL,
                                                   LOC::Machine::STATE_BROKEN,
                                                   LOC::Machine::STATE_CHECK,
                                                   LOC::Machine::STATE_TRANSFER,
                                                   LOC::Machine::STATE_DELIVERY];
    }
    elsif ($displayType eq 'excel')
    {
        # Pour le fichier excel, on ne récupère pas les données sur les filtres
        $tabOptions->{'getFiltersData'} = 0;
        $tabOptions->{'rowsLimit'} = $excelRowsLimit;
    }

    # Récupération des filtres demandés
    my $tabActivesFilters = &LOC::Request::getHash('filters', $tabDefaultActivesFilters);

    # Récupération de la liste des machines et des filtres
    $tabViewData{'data'} = &_getData($countryId, $tabActivesFilters, $tabSorts,
                                     $startIndex, $pageSize, $tabUserInfos, $tabOptions);

    if ($tabViewData{'data'}->{'result'} eq 'ko')
    {
        exit;
    }


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    if ($displayType eq '')
    {
        # Paramètres de tris et pagination
        $tabViewData{'pageSize'}              = $pageSize;
        $tabViewData{'startIndex'}            = $startIndex;
        $tabViewData{'sorts'}                 = $tabSorts;
        $tabViewData{'activesFilters'}        = $tabActivesFilters;
        $tabViewData{'defaultActivesFilters'} = $tabDefaultActivesFilters;

        $tabViewData{'excelRowsLimit'}        = $excelRowsLimit;


        # URL pour le fichier Excel
        $tabViewData{'data'}->{'excelUrl'} = &LOC::Request::createRequestUri('machine:list:view', {
                                                 'displayType' => 'excel',
                                                 'sorts'       => [{'col' => 'parkNumber', 'dir' => 'asc'}],
                                                 'filters'     => $tabActivesFilters,
                                                 'startIndex'  => 0,
                                                 'pageSize'    => 0
                                             });

        # Page HTML
        if ($displayMode eq 'visibility')
        {
            require $directory . '/view/list/viewVisibility.cgi';
        }
        else
        {
            require $directory . '/view/list/view.cgi';
        }
    }
    elsif ($displayType eq 'json')
    {
        # URL pour le fichier Excel
        $tabViewData{'data'}->{'excelUrl'} = &LOC::Request::createRequestUri('machine:list:view', {
                                                 'displayType' => 'excel',
                                                 'sorts'       => [{'col' => 'parkNumber', 'dir' => 'asc'}],
                                                 'filters'     => $tabActivesFilters,
                                                 'startIndex'  => 0,
                                                 'pageSize'    => 0
                                             });

        # Trame JSON
        require $directory . '/view/list/json.cgi';
    }
    elsif ($displayType eq 'excel')
    {
        # Fichier Excel
        require $directory . '/view/list/excel.cgi';
    }
}


# Function: viewVisibilityAction
# Affichage de la liste des machine en visibilité
sub viewVisibilityAction
{
    my $tabParameters = $_[0];
    $tabParameters->{'_mode'} = 'visibility';

    # Appel de la liste des machines
    &viewAction(@_);
}


# Function: _getData
# Récupérer les données du tableau et les filtres
#
# Parameters:
# string   $countryId         - Pays
# hashref  $tabActivesFilters - Filtres actifs
# arrayref $tabSorts          - Tris
# int      $startIndex        - Index de la ligne de départ à récupérer
# int      $pageSize          - Nombre de lignes à récupérer
# hashref  $tabUserInfos      - Informations sur l'utilisateur
# hashref  $tabOptions        - Options supplémentaires
#
# Returns:
# hashref - Données du tableau et des filtres
sub _getData
{
    my ($countryId, $tabActivesFilters, $tabSorts, $startIndex, $pageSize, $tabUserInfos, $tabOptions) = @_;


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);


    # Filtres disponibles
    my %tabFiltersCfgs = (
        # Partie 1
        'parkNumber' => {
            'type'    => 'search',
            'where'   => 'CAST(mch_parknumber AS CHAR) = <%values> OR ' .
                         'CONCAT(CONVERT(mch_parknumberprefix USING "utf8"), mch_parknumber) = <%values>'
        },
        'family.id' => {
            'type'    => 'list',
            'select'  => {
                'value' => 'mch_fly_id',
                'label' => 'mch_familylabel'
            },
            'where'   => 'mch_fly_id IN (<%values>)'
        },
        'elevation'    => {
            'type'    => 'list',
            'select'  => {
                'value' => 'mch_elevation'
            },
            'where'   => 'mch_elevation IN (<%values>)'
        },
        'energy'    => {
            'type'    => 'list',
            'select'  => {
                'value' => 'mch_energy'
            },
            'where'   => 'mch_energy IN (<%values>)'
        },
        'tariffFamily.id' => {
            'type'    => 'list',
            'select'  => {
                'value' => 'mch_tfl_id',
                'label' => 'mch_tarifffamilylabel'
            },
            'where'   => 'mch_tfl_id IN (<%values>)'
        },
        'state.id'    => {
            'type'    => 'list',
            'select'  => {
                'value' => 'mch_sta_id',
                'label' => 'mch_statelabel'
            },
            'where'   => 'mch_sta_id IN (<%values>)'
        },

        # Partie 2
        'model.id' => {
            'type'    => 'list',
            'select'  => {
                'value' => 'mch_mod_id',
                'label' => 'mch_modellabel'
            },
            'where'   => 'mch_mod_id IN (<%values>)'
        },
        'manufacturer.id' => {
            'type'    => 'list',
            'select'  => {
                'value' => 'mch_mnf_id',
                'label' => 'mch_manufacturerlabel'
            },
            'where'   => 'mch_mnf_id IN (<%values>)'
        },
        'serialNumber' => {
            'type'    => 'search',
            'where'   => 'mch_serialnumber = <%values>'
        },
        'isOutOfPark' => {
            'type'    => 'list',
            'select'  => {
                'value' => 'mch_is_outofpark'
            },
            'where'   => 'mch_is_outofpark IN (<%values>)'
        },
        'isWithSecuritySystem' => {
            'type'    => 'list',
            'select'  => {
                'value' => 'mch_is_withsecuritysystem'
            },
            'where'   => 'mch_is_withsecuritysystem IN (<%values>)'
        },
        'isWithStabilizers' => {
            'type'    => 'list',
            'select'  => {
                'value' => 'mch_is_withstabilizers'
            },
            'where'   => 'mch_is_withstabilizers IN (<%values>)'
        },
        'isWithTelematic' => {
            'type'    => 'list',
            'select'  => {
                'value' => 'mch_is_withtelematic'
            },
            'where'   => 'mch_is_withtelematic IN (<%values>)'
        },        
        'isAccessory' => {
            'type'    => 'list',
            'select'  => {
                'value' => 'mch_is_accessory'
            },
            'where'   => 'mch_is_accessory IN (<%values>)'
        },

        # Partie 3
        'isSubRent' => {
            'type'    => 'list',
            'select'  => {
                'value' => 'mch_is_subrent'
            },
            'where'   => 'mch_is_subrent IN (<%values>)'
        },
        'isCategoryB' => {
            'type'    => 'list',
            'select'  => {
                'value' => 'mch_is_categoryb'
            },
            'where'   => 'mch_is_categoryb IN (<%values>)'
        },
        'isAsbestos' => {
            'type'    => 'list',
            'select'  => {
                'value' => 'mch_is_asbestoslastcontract',
            },
            'where'   => 'mch_is_asbestoslastcontract IN (<%values>)'
        },

        # Partie 4
        'agency.id'    => {
            'type'    => 'list',
            'select'  => {
                'value' => 'IF (mch_agc_id_visibleon IS NOT NULL, mch_agc_id_visibleon, mch_agc_id)'
            },
            'where'   => '(mch_agc_id IN (<%values>) OR mch_agc_id_visibleon IN (<%values>))',
            'values' => {}
        },
        'isVisibleOnAgency' => {
            'type'  => 'search',
            'where' => '(mch_agc_id_visibleon = <%values> OR (mch_agc_id = <%values> AND mch_agc_id_visibleon IS NOT NULL))'
        },
        'area.id'    => {
            'type'    => 'list',
            'select'  => {
                'value' => 'IFNULL(mch_are_id, "")',
                'label' => 'IFNULL(mch_arealabel, "")'
            },
            'where'   => 'IFNULL(mch_are_id, "") IN (<%values>)'
        }
    );


    # ***********************************************************************************************************
    # Récupération de la liste
    # ***********************************************************************************************************

    # Colonnes disponibles
    my %tabColumnsCfgs = (
        'id'               => {
            'select' => 'mch_id'
        },
        'model.label'      => {
            'select' => 'mch_modellabel'
        },
        'manufacturer.label'   => {
            'select' => 'mch_manufacturerlabel'
        },
        'family.label'     => {
            'select' => 'mch_familylabel'
        },
        'energy'           => {
            'select' => 'mch_energy'
        },
        'elevation'        => {
            'select' => 'mch_elevation'
        },
        'weight'           => {
            'select' => 'mch_weight'
        },
        'length'           => {
            'select' => 'mch_length'
        },
        'width'            => {
            'select' => 'mch_width'
        },
        'height'           => {
            'select' => 'mch_height'
        },
        'agency.id'        => {
            'select' => 'mch_agc_id',
            'orderBy' => 'mch_agc_id <%dir>, mch_agc_id_visibleon <%dir>'
        },
        'visibleOnAgency.id' => {
            'select' => 'mch_agc_id_visibleon'
        },
        'parkNumber'       => {
            'select' => 'mch_parknumber'
        },
        'parkNumberPrefix' => {
            'select' => 'mch_parknumberprefix'
        },
        'isOutOfPark'      => {
            'select' => 'mch_is_outofpark'
        },
        'isAccessory'      => {
            'select' => 'mch_is_accessory'
        },
        'isSubRent'      => {
            'select' => 'mch_is_subrent'
        },
        'isCategoryB'      => {
            'select' => 'mch_is_categoryb'
        },
        'state.id'         => {
            'select' => 'mch_sta_id'
        },
        'state.label'      => {
            'select' => 'mch_statelabel'
        },
        'contract.id'      => {
            'select' => 'mch_rct_id_lastcontract'
        },
        'contract.isAsbestos' => {
            'select' => 'mch_is_asbestoslastcontract'
        },
        'contract.endDate'    => {
            'select' => 'mch_date_endlastcontract'
        },
        'contract.code'    => {
            'select' => 'mch_lastcontractcode'
        },
        'contract.country.id' => {
            'select' => 'mch_cty_id_lastcontract'
        },
        'control.nextDate' => {
            'select' => 'mch_date_nextcontrol'
        },
        'isWithTelematic' => {
            'select' => 'mch_is_withtelematic'
        }
    );

    # Jointures
    my @tabJoinsCfgs = ();

    # Conditions permanentes
    my @tabQueryFirstWheres = (
        'mch_sta_id NOT IN (' . $db->quote([LOC::Machine::STATE_STOLEN, LOC::Machine::STATE_RENOSC]) . ')'
    );


    return &LOC::AjaxTable::getData($db, 'v_machine', \@tabJoinsCfgs,
                                    \%tabColumnsCfgs, \%tabFiltersCfgs,
                                    \@tabQueryFirstWheres, $tabActivesFilters,
                                    $tabSorts, $startIndex, $pageSize, $tabOptions);
}

# Function: pdfAction
# Affiche la liste des machines au format PDF
#
# Parameters:
# string   $countryId         - Pays
sub pdfAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    # Récupération des paramètres
    my $countryId   = &LOC::Request::getString('countryId', $tabUserInfos->{'nomadCountry.id'});
    my $agencyId    = &LOC::Request::getString('agencyId', $tabUserInfos->{'nomadAgency.id'});
    my $stateId     = &LOC::Request::getString('stateId', undef, LOC::Request::SRC_GET);

    my $tabMachinesList = &LOC::Machine::getList($countryId, LOC::Util::GETLIST_ASSOC,
                                                 {'agencyId' => $agencyId, 'stateId' => $stateId},
                                                 LOC::Machine::GETINFOS_ALL,
                                                 {'level' => LOC::Machine::LEVEL_MACHINESFAMILY});

    my @tabMachines;
    foreach my $tabMachineInfos (values(%$tabMachinesList))
    {
        my $tabInfos = {
            'family.label'       => $tabMachineInfos->{'machinesFamily.shortLabel'},
            'manufacturer.label' => $tabMachineInfos->{'manufacturer.label'},
            'model.label'        => $tabMachineInfos->{'model.label'},
            'weight'             => $tabMachineInfos->{'model.weight'},
            'elevation'          => $tabMachineInfos->{'machinesFamily.elevation'},
            'energy'             => $tabMachineInfos->{'machinesFamily.energy'},
            'parkNumber'         => $tabMachineInfos->{'parkNumber'},
            'serialNumber'       => $tabMachineInfos->{'serialNumber'},
            'state.label'        => $tabMachineInfos->{'state.label'},
            'crashDate'          => $tabMachineInfos->{'crashDate'}
        };
        push(@tabMachines, $tabInfos);
    }

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'       => $tabUserInfos->{'locale.id'},
        'user.agency.id'  => $agencyId,
        'user.fullName'   => $tabUserInfos->{'firstName'} . ' ' . $tabUserInfos->{'name'},
        'tabMachinesList' => \@tabMachines
    );

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;

    require $directory . '/view/list/pdf.cgi';
}
1;
