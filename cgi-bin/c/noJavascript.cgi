#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)');

use strict;

print qq{Content-type: text/html; charset=UTF-8

<!DOCTYPE html>
<html>
    <head>

        <meta charset="UTF-8">
        <title>JavaScript disabled</title>
        <link rel="icon" type="image/png" sizes="16x16" href="../../web/img/icons/LOC_favicon_16x16.png"/>
        <link rel="icon" type="image/png" sizes="32x32" href="../../web/img/icons/LOC_favicon_32x32.png"/>
        <link rel="icon" type="image/png" sizes="128x128" href="../../web/img/icons/LOC_favicon_128x128.png"/>
        <link rel="apple-touch-icon" href="../../web/img/icons/LOC_favicon_152x152_precomposed.png"/>
        <link rel="stylesheet" href="../../web/css/base.css"/>

    </head>

    <body>

        <div class="locMessage">
            <img src="../../web/img/base/messages/error.png" />
            <div class="error">
                <p>JavaScript est désactivé sur votre navigateur. Vous devez l’activer pour utiliser cette application.</p>
                <p>JavaScript is disabled on this browser. You must enable it to use this application.</p>
            </div>
        </div>

    </body>
</html>};
