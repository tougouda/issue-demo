use utf8;
use open (':encoding(UTF-8)');

use strict;
use lib 'inc', 'lib';

use LOC::Html::Standard;
use LOC::Locale;
use LOC::Util;

our %tabViewData;


my $locale = &LOC::Locale::getLocale($tabViewData{'locale.id'});
my $view = LOC::Html::Standard->new($tabViewData{'locale.id'}, $tabViewData{'user.agency.id'});


# Function: checkRight
# Vérifie si le droit passé en paramètre est activé ou pas.
# Si ce n'est pas le cas, le tableau d'attributs est modifié pour que l'élément HTML soit désactivé.
#
# Parameters:
# bool    $right         - Le droit à vérifier.
# hashref $tabAttributes - Le tableau d'attributs d'un élément HTML.
#
# Returns:
# hashref - Le tableau d'attributs modifié en conséquence.
sub checkRight
{
    my ($right, $tabAttributes) = @_;

    if (!$right)
    {
        $tabAttributes->{'disabled'} = 1;
        if (defined $tabAttributes->{'class'})
        {
            $tabAttributes->{'class'} .= ' disabled';
        }
    }

    return $tabAttributes;
}


$view->setPageTitle($locale->t('Paramètres de l\'application'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT);

$view->setTitle($locale->t('Administration') . ' > ' . $locale->t('Paramètres de l\'application'));


# CSS
$view->addCSSSrc('location.css');
$view->addCSSSrc('admin/settings/view.css');

# JS
$view->addJSSrc('admin/settings/view.js');


# Bouton d'actualisation
my $refreshUrl = $view->createURL($tabViewData{'route'});
my $refreshBtn = $view->displayTextButton(
        'Actualiser',
        'refresh(|Over).png',
        $refreshUrl,
        $locale->t('Actualiser'),
        'l',
        {
            'tabindex' => -1,
            'onfocus' => 'this.blur();'
        }
    );
$view->addControlsContent($refreshBtn, 'right', 1);

# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();


# Message de confirmation
if ($tabViewData{'state'} eq 'ok')
{
    print $view->displayMessages('valid', [$locale->t('Les paramètres du mode maintenance ont été mis à jour.')]);
}


# Erreurs
my $tabMessages = [];
foreach my $error (@{$tabViewData{'tabErrors'}->{'fatal'}})
{
    if ($error == LOC::Maintenance::ERROR_RIGHTS_SET)
    {
        push(@$tabMessages, $locale->t(
                'Vous n\'avez pas les droits nécessaires pour modifier les paramètres du mode maintenance.'
            ));
    }
    elsif ($error == LOC::Maintenance::ERROR_MAINTENANCE_UNKNOWNCHARAC)
    {
        push(@$tabMessages, $locale->t(
                'Vous devez créer la caractéristique %s avant de pouvoir paramétrer le mode maintenance.',
                &LOC::Characteristic::MAINTENANCE
            ));
    }
    else
    {
        push(@$tabMessages, $locale->t('Erreur fatale'));
    }
}

if (@$tabMessages)
{
    print $view->displayMessages('error', $tabMessages);
}


# Activé ou pas
my $isActivatedInput = $view->displayFormInputCheckBox(
        'isActivated',
        $tabViewData{'isActivated'},
        &checkRight($tabViewData{'isEditable'}, {})
    );

# Niveau
my @tabLevels = (
    {
        'value'     => 0,
        'innerHTML' => '-- ' . $locale->t('Aucun') . ' --'
    }
);
my $nbLevels = @{$tabViewData{'tabLevels'}};
for (my $i = 0; $i < $nbLevels; $i++)
{
    push(@tabLevels, {
        'value'     => $tabViewData{'tabLevels'}->[$i]->{'id'},
        'innerHTML' => $tabViewData{'tabLevels'}->[$i]->{'label'}
    });
}
my $levelInput = $view->displayFormSelect(
        'level',
        \@tabLevels,
        $tabViewData{'level'},
        0,
        &checkRight($tabViewData{'isEditable'}, {})
    );

# Titre HTML
my $titleInput = $view->displayFormInputText(
        'title',
        $tabViewData{'title'},
        undef,
        &checkRight($tabViewData{'isEditable'}, {
            'size'      => 40,
            'maxlength' => 40
        })
    );

# Période de vérification de sortie du mode maintenance
my $periodInput = $view->displayFormInputText(
        'period',
        $tabViewData{'period'},
        undef,
        &checkRight($tabViewData{'isEditable'}, {
            'size'      => 3,
            'maxlength' => 3,
            'class'     => 'right'
        })
    );

# Intitulé
my $pendingHeadingInput = $view->displayFormInputText(
        'pending.heading',
        $tabViewData{'pending'}->{'heading'},
        undef,
        &checkRight($tabViewData{'isEditable'}, {
            'class'     => 'heading',
            'size'      => 80,
            'maxlength' => 80
        })
    );

# Message
my $pendingMessageInput = $view->displayFormTextArea(
        'pending.message',
        $tabViewData{'pending'}->{'message'},
        &checkRight($tabViewData{'isEditable'}, {
            'cols'      => 80,
            'rows'      => 10,
            'maxlength' => 512
        })
    );

# Intitulé de sortie du mode maintenance
my $completeHeadingInput = $view->displayFormInputText(
        'complete.heading',
        $tabViewData{'complete'}->{'heading'},
        undef,
        &checkRight($tabViewData{'isEditable'}, {
            'class'     => 'heading',
            'size'      => 80,
            'maxlength' => 80
        })
    );

# Message de sortie du mode maintenance
my $completeMessageInput = $view->displayFormTextArea(
        'complete.message',
        $tabViewData{'complete'}->{'message'},
        &checkRight($tabViewData{'isEditable'}, {
            'cols'      => 80,
            'rows'      => 10,
            'maxlength' => 512
        })
    );

print '
<fieldset>
    <legend>' . $view->htmllocale('Mode maintenance') . '</legend>

    <table class="formTable">
        <tr>
            <td class="label"><label for="isActivated[1]">' . $view->htmllocale('Activé') . '</label></td>
            <td>' . $isActivatedInput . '</td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td class="label"><label for="level">' . $view->htmllocale('Outrepasser à partir du niveau') . '</label></td>
            <td>' . $levelInput . '</td>
        </tr>
        <tr>
            <td></td>
            <td class="info">' . $view->htmllocale('Le niveau à partir duquel les utilisateurs auront quand même accès à l\'application malgré l\'activation du mode maintenance') . '</td>
        </tr>
        <tr>
            <td class="label"><label for="title">' . $view->htmllocale('Titre HTML') . '</label></td>
            <td>' . $titleInput .'</td>
        </tr>
        <tr>
            <td></td>
            <td class="info">' . $view->htmllocale('Le titre figurant dans la balise <title> de la page de maintenance.') . '</td>
        </tr>
        <tr>
            <td class="label"><label for="period">' . $view->htmllocale('Période de vérification') . '</label></td>
            <td>' . $locale->t('%s s', $periodInput) .'</td>
        </tr>
        <tr>
            <td></td>
            <td class="info">' . $view->htmllocale('La période (en secondes) de vérification de sortie du mode maintenance.') . '</td>
        </tr>
    </table>

    <fieldset class="sub">
        <legend>' . $view->htmllocale('Maintenance en cours') . '</legend>

        <table class="formTable">
            <tr>
                <td class="label"><label for="pending.heading">' . $view->htmllocale('Intitulé') . '</label></td>
                <td>' . $pendingHeadingInput .'</td>
            </tr>
            <tr>
                <td></td>
                <td class="info">' . $view->htmllocale('Le titre affiché sur la page de maintenance lorsque la maintenance est en cours. Il figurera dans une balise <h1>.') . '</td>
            </tr>
            <tr>
                <td class="label"><label for="pending.message">' . $view->htmllocale('Message') . '</label></td>
                <td>' . $pendingMessageInput .'</td>
            </tr>
            <tr>
                <td></td>
                <td class="info">' . $view->htmllocale('Le message affiché sur la page de maintenance lorsque la maintenance est en cours. Il peut contenir du code HTML. Les variables globales (du type %locationUrl%) sont remplacées par leur valeur.') . '</td>
            </tr>
        </table>
    </fieldset>

    <fieldset class="sub">
        <legend>' . $view->htmllocale('Maintenance terminée') . '</legend>

        <table class="formTable">
            <tr>
                <td class="label"><label for="complete.heading">' . $view->htmllocale('Intitulé') . '</label></td>
                <td>' . $completeHeadingInput .'</td>
            </tr>
            <tr>
                <td></td>
                <td class="info">' . $view->htmllocale('Le titre affiché sur la page de maintenance lorsque la maintenance est terminée. Il figurera dans une balise <h1>.') . '</td>
            </tr>
            <tr>
                <td class="label"><label for="complete.message">' . $view->htmllocale('Message') . '</label></td>
                <td>' . $completeMessageInput .'</td>
            </tr>
            <tr>
                <td></td>
                <td class="info">' . $view->htmllocale('Le message à afficher lors de la sortie du mode maintenance lorsque la maintenance est terminée. Les variables globales (du type %locationUrl%) sont remplacées par leur valeur.') . '</td>
            </tr>
        </table>
    </fieldset>
</fieldset>';


# Boutons de contrôle
my @tabLeftBtns = ();
# Validation
push(@tabLeftBtns, $view->displayTextButton(
        $locale->t('Valider'),
        'valid(|Over).png',
        ($tabViewData{'isEditable'} ? 'submitForm()' : ''),
        $locale->t('Enregistrer les modifications'),
        'l',
        &checkRight($tabViewData{'isEditable'}, {
            'class' => 'locCtrlButton'
        })
    ));
# Aperçu
push(@tabLeftBtns, $view->displayTextButton(
        $locale->t('Aperçu'),
        'admin/maintenance/preview.svg',
        ($tabViewData{'isPreviewable'} ? 'default:maintenance:info' : ''),
        $locale->t('Voir un aperçu de la page de maitenance'),
        'l',
        &checkRight($tabViewData{'isPreviewable'}, {
            'class' => 'locCtrlButton',
            'target' => '_blank'
        })
    ));

print $view->displayControlPanel({'left' => \@tabLeftBtns, 'substyle' => 'bottom'});


# Champs cachés
print $view->displayFormInputHidden('valid', $tabViewData{'valid'});


# Affichage du pied de page
print $view->endForm();
print $view->displayFooter();

