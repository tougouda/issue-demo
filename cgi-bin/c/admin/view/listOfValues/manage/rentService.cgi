use utf8;
use open (':encoding(UTF-8)');

use strict;

our %tabViewData;
our $view;
our $locale;

my $nameInput;
my $codeInput;
my $typeInput;
my $amountInput;
my $currencyInput;
my $infosInput;
my $removeBtn;
my $htmlHidden;

# Tableau des données
print '
<table class="standard" style="width: 100%">
    <tr>
        <th>' . htmllocale('Nom') . '</th>
        <th>' . htmllocale('Code') . '</th>
        <th>' . htmllocale('Type') . '</th>
        <th>' . htmllocale('Montant') . '</th>
        <th>' . htmllocale('Devise') . '</th>
        <th title="Informations complémentaires">' . htmllocale('Informations cpl.') . '</th>
        <th></th>
    </tr>';

my $tab = $tabViewData{'tabData'};
while (my ($id, $values) = each(%$tab))
{
    # nom
    $nameInput = $view->displayFormInputText('tabData[' . $id . '][name]', $values->{'name'}, '',
        {'size' => '40', 'maxlength' => '255', 'autocomplete' => 'off'});

    # code
    $codeInput = $view->displayFormInputText('tabData[' . $id . '][code]', $values->{'code'}, '',
        {'size' => '10', 'maxlength' => '10', 'autocomplete' => 'off'});

    # type
    $typeInput = $view->displayFormInputText('tabData[' . $id . '][type]', $values->{'type'}, '',
        {'size' => '10', 'maxlength' => '10', 'autocomplete' => 'off'});

    my $currencySymbol = $tabViewData{'tabCurrencies'}->{$values->{'currency.id'}};
    
    # amount
    $amountInput = $view->displayFormInputCurrency('tabData[' . $id . '][amount]',
        &LOC::Util::round($values->{'amount'}, $locale->getFracDigits()),
        LOC::Html::CURRENCYMODE_INPUT, $currencySymbol, undef,
        {'style' => 'width: 70px;', 'size' => '8', 'maxlength' => '15', 'autocomplete' => 'off'});

    # devise    
    $currencyInput = $view->displayFormSelect('tabData[' . $id . '][currency.id]',
        $tabViewData{'tabCurrencies'}, $values->{'currency.id'});

    # infos complémentaires
    $infosInput = $view->displayFormTextArea('tabData[' . $id . '][infos]', ($values->{'infos'} ? $values->{'infos'} : ''));

    # btn suppression
    $removeBtn = $view->displayTextButton('', 'remove(|Over).png',
        'if (confirm("' . htmllocale('Voulez-vous vraiment supprimer cette valeur ?') . '")) removeValue(' . $id . ')',
        htmllocale('Supprimer'));

    # hidden
    $htmlHidden = $view->displayFormInputHidden('tabData[' . $id . '][id]', $values->{'id'});
    $htmlHidden .= $view->displayFormInputHidden('tabData[' . $id . '][remove]', 0);

    print '
    <tr>
        <td>' . $nameInput . $htmlHidden . '</td>
        <td>' . $codeInput . '</td>
        <td>' . $typeInput . '</td>
        <td>' . $amountInput . '</td>
        <td>' . $currencyInput . '</td>
        <td>' . $infosInput . '</td>
        <td align="right">' . $removeBtn . '</td>
    </tr>';
}

# nom
$nameInput = $view->displayFormInputText('tabAdd[name]', '', '',
    {'size' => '40', 'maxlength' => '255', 'autocomplete' => 'off'});
    
# code
$codeInput = $view->displayFormInputText('tabAdd[code]', '', '',
    {'size' => '10', 'maxlength' => '10', 'autocomplete' => 'off'});

# type
$typeInput = $view->displayFormInputText('tabAdd[type]', '', '',
    {'size' => '10', 'maxlength' => '10', 'autocomplete' => 'off'});

# amount
$amountInput = $view->displayFormInputCurrency('tabAdd[amount]', '',
    LOC::Html::CURRENCYMODE_INPUT, $tabViewData{'country'}->{'currency.symbol'}, undef,
    {'style' => 'width: 70px;', 'size' => '8', 'maxlength' => '15', 'autocomplete' => 'off'});
    
# devise
$currencyInput = $view->displayFormSelect('tabAdd[currency.id]', $tabViewData{'tabCurrencies'},
    $tabViewData{'country'}->{'currency.id'});

# Informations complémentaires
$infosInput = $view->displayFormTextArea('tabAdd[infos]', '');

# btn ajout
my $addBtn = $view->displayTextButton('', 'add(|Over).png', 'addValue()', htmllocale('Ajouter'));

# champ caché
$htmlHidden = $view->displayFormInputHidden('tabAdd[add]', 0);

print '
    <tr>
        <td>' . $nameInput . $htmlHidden . '</td>
        <td>' . $codeInput . '</td>
        <td>' . $typeInput . '</td>
        <td>' . $amountInput . '</td>
        <td>' . $currencyInput . '</td>
        <td>' . $infosInput . '</td>
        <td align="right">' . $addBtn . '</td>
    </tr>
</table>';