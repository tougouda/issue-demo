use utf8;
use open (':encoding(UTF-8)');

use strict;

our %tabViewData;
our $view;
our $locale;

my $nameInput;
my $codeInput;
my $lowerCostInput;
my $upperCostInput;
my $amountInput;
my $currencyInput;
my $removeBtn;
my $htmlHidden;


# Tableau des données
print '<table class="standard" style="width: 100%">' . "\n";

print '<tr>' . "\n";
print '<th>' . htmllocale('Nom') . '</th>' . "\n";
print '<th>' . htmllocale('Code') . '</th>' . "\n";
print '<th>' . htmllocale('Coût (borne inf.)') . '</th>' . "\n";
print '<th>' . htmllocale('Coût (borne sup.)') . '</th>' . "\n";
print '<th>' . htmllocale('Montant') . '</th>' . "\n";
print '<th>' . htmllocale('Devise') . '</th>' . "\n";
print '<th></th>' . "\n";
print '</tr>' . "\n";

my $tab = $tabViewData{'tabData'};
while (my ($id, $values) = each(%$tab))
{
    # nom
    $nameInput = $view->displayFormInputText('tabData[' . $id . '][name]', $values->{'name'}, '',
        {'size' => '40', 'maxlength' => '255', 'autocomplete' => 'off'});
        
    # code
    $codeInput = $view->displayFormInputText('tabData[' . $id . '][code]', $values->{'code'}, '',
        {'size' => '10', 'maxlength' => '10', 'autocomplete' => 'off'});

    my $currencySymbol = $tabViewData{'tabCurrencies'}->{$values->{'currency.id'}};

    # borne inf
    $lowerCostInput = $view->displayFormInputCurrency('tabData[' . $id . '][lowerCost]',
        &LOC::Util::round($values->{'lowerCost'}, $locale->getFracDigits()), 
        LOC::Html::CURRENCYMODE_INPUT, $currencySymbol, undef,
        {'style' => 'width: 70px;', 'size' => '8', 'maxlength' => '15', 'autocomplete' => 'off'});

    # borne sup
    $upperCostInput = $view->displayFormInputCurrency('tabData[' . $id . '][upperCost]',
        &LOC::Util::round($values->{'upperCost'}, $locale->getFracDigits()),
        LOC::Html::CURRENCYMODE_INPUT, $currencySymbol, undef,
        {'style' => 'width: 70px;', 'size' => '8', 'maxlength' => '15', 'autocomplete' => 'off'});
        
    # amount
    $amountInput = $view->displayFormInputCurrency('tabData[' . $id . '][amount]',
        &LOC::Util::round($values->{'amount'}, $locale->getFracDigits()),
        LOC::Html::CURRENCYMODE_INPUT, $currencySymbol, undef,
        {'style' => 'width: 70px;', 'size' => '8', 'maxlength' => '15', 'autocomplete' => 'off'});        
      
    # devise    
    $currencyInput = $view->displayFormSelect('tabData[' . $id . '][currency.id]',
        $tabViewData{'tabCurrencies'}, $values->{'currency.id'});
        
    # btn suppression
    $removeBtn = $view->displayTextButton('', 'remove(|Over).png',
        'if (confirm("' . htmllocale('Voulez-vous vraiment supprimer cette valeur ?') . '")) removeValue(' . $id . ')',
        htmllocale('Supprimer'));

    # hidden
    $htmlHidden = $view->displayFormInputHidden('tabData[' . $id . '][id]', $values->{'id'});
    $htmlHidden .= $view->displayFormInputHidden('tabData[' . $id . '][remove]', 0);

    print '<tr>' . "\n";
    print '<td>' . $nameInput . $htmlHidden . '</td>' . "\n";
    print '<td>' . $codeInput . '</td>' . "\n";
    print '<td>' . $lowerCostInput . '</td>' . "\n";
    print '<td>' . $upperCostInput . '</td>' . "\n";
    print '<td>' . $amountInput . '</td>' . "\n";
    print '<td>' . $currencyInput . '</td>' . "\n";
    print '<td align="right">' . $removeBtn . '</td>' . "\n";
    print '</tr>' . "\n";
}

# nom
$nameInput = $view->displayFormInputText('tabAdd[name]', '', '',
    {'size' => '40', 'maxlength' => '255', 'autocomplete' => 'off'});
    
# code
$codeInput = $view->displayFormInputText('tabAdd[code]', '', '',
    {'size' => '10', 'maxlength' => '10', 'autocomplete' => 'off'});

# borne inf
$lowerCostInput = $view->displayFormInputCurrency('tabAdd[lowerCost]', '', 
    LOC::Html::CURRENCYMODE_INPUT, $tabViewData{'country'}->{'currency.symbol'}, undef,
    {'style' => 'width: 70px;', 'size' => '8', 'maxlength' => '15', 'autocomplete' => 'off'});

# borne sup
$upperCostInput = $view->displayFormInputCurrency('tabAdd[upperCost]', '',
    LOC::Html::CURRENCYMODE_INPUT, $tabViewData{'country'}->{'currency.symbol'}, undef,
    {'style' => 'width: 70px;', 'size' => '8', 'maxlength' => '15', 'autocomplete' => 'off'});
    
# amount
$amountInput = $view->displayFormInputCurrency('tabAdd[amount]', '',
    LOC::Html::CURRENCYMODE_INPUT, $tabViewData{'country'}->{'currency.symbol'}, undef,
    {'style' => 'width: 70px;', 'size' => '8', 'maxlength' => '15', 'autocomplete' => 'off'});
    
# devise    
$currencyInput = $view->displayFormSelect('tabAdd[currency.id]', $tabViewData{'tabCurrencies'},
    $tabViewData{'country'}->{'currency.id'});
    
# btn ajout
my $addBtn = $view->displayTextButton('', 'add(|Over).png', 'addValue()', htmllocale('Ajouter'));

# champ caché
$htmlHidden = $view->displayFormInputHidden('tabAdd[add]', 0);

print '<tr>' . "\n";
print '<td>' . $nameInput . $htmlHidden . '</td>' . "\n";
print '<td>' . $codeInput . '</td>' . "\n";
print '<td>' . $lowerCostInput . '</td>' . "\n";
print '<td>' . $upperCostInput . '</td>' . "\n";
print '<td>' . $amountInput . '</td>' . "\n";
print '<td>' . $currencyInput . '</td>' . "\n";
print '<td align="right">' . $addBtn . '</td>' . "\n";
print '</tr>' . "\n";

print '</table>';