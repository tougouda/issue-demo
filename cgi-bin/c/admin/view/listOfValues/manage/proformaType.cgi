use utf8;
use open (':encoding(UTF-8)');

use strict;

our %tabViewData;
our $view;
our $locale;


# Tableau des données
print '
<table class="standard" style="width: 100%">
<thead>
    <tr>
        <th>' . htmllocale('Libellé') . '</th>
        <th>' . htmllocale('Flags') . '</th>
        <th>' . htmllocale('Date de création') . '</th>
        <th>' . htmllocale('Date de modification') . '</th>
        <th style="width: 16px;"></th>
    </tr>
</thead>
<tbody>';

my $tab = $tabViewData{'tabData'};
while (my ($id, $values) = each(%$tab))
{
    my $idInput        = $view->displayFormInputHidden('tabData[' . $id . '][id]', $id);

    my $labelInput = $view->displayFormInputText('tabData[' . $id . '][label]',
                                                 $values->{'label'},
                                                 '',
                                                 {
                                                  'size' => 32,
                                                  'maxlength' => 255,
                                                  'autocomplete' => 'off'
                                                 });

    my $flagsInput = $view->displayFormInputText('tabData[' . $id . '][flags]',
                                                 $values->{'flags'},
                                                 '',
                                                 {
                                                  'size' => 4,
                                                  'maxlength' => 8,
                                                  'autocomplete' => 'off'
                                                 });

    # Bouton de suppression
    my $removeBtn = $view->displayTextButton('', 'remove(|Over).png',
                                             'if (confirm("' . $view->toJSEntities($locale->t('Voulez-vous vraiment supprimer cette valeur ?')) . '")) removeValue(' . $id . ')',
                                             $locale->t('Supprimer')) .
                    $view->displayFormInputHidden('tabData[' . $id . '][remove]', 0);


    print '
    <tr>
        <td>' . $labelInput . '</td>
        <td>' . $flagsInput . '</td>
        <td>' . $locale->getDateFormat($values->{'creationDate'}, LOC::Locale::FORMAT_DATETIME_NUMERIC) . '</td>
        <td>' . $locale->getDateFormat($values->{'modificationDate'}, LOC::Locale::FORMAT_DATETIME_NUMERIC) . '</td>
        <td>' . $removeBtn . $idInput . '</td>
    </tr>';
}


# Ajout d'une ligne
my $labelInput = $view->displayFormInputText('tabAdd[label]',
                                             '',
                                             '',
                                             {
                                              'size' => 32,
                                              'maxlength' => 255,
                                              'autocomplete' => 'off'
                                             });
my $flagsInput = $view->displayFormInputText('tabAdd[flags]',
                                             '',
                                             '',
                                             {
                                              'size' => 4,
                                              'maxlength' => 8,
                                              'autocomplete' => 'off'
                                             });

# Bouton d'ajout
my $addBtn = $view->displayTextButton('', 'add(|Over).png', 'addValue()', $locale->t('Ajouter')) .
             $view->displayFormInputHidden('tabAdd[add]', 0);



print '
    <tr>
        <td>' . $labelInput . '</td>
        <td>' . $flagsInput . '</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>' . $addBtn . '</td>
    </tr>';

print '
<tbody>
</table>';