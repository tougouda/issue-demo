use utf8;
use open (':encoding(UTF-8)');

use strict;

our %tabViewData;
our $view;
our $locale;

my $nameInput;
my $codeInput;
my $articleRefInput;
my $labelInput;
my $vatInput;
my $removeBtn;
my $htmlHidden;

# Tableau des données
print '<table class="standard" style="width: 100%">' . "\n";

print '<tr>' . "\n";
print '<th>' . htmllocale('Nom') . '</th>' . "\n";
print '<th>' . htmllocale('Code GesLoc') . '</th>' . "\n";
print '<th>' . htmllocale('Référence article Sage') . '</th>' . "\n";
print '<th>' . htmllocale('Libellé Sage') . '</th>' . "\n";
print '<th>' . htmllocale('Taux de TVA') . '</th>' . "\n";
print '<th></th>' . "\n";
print '</tr>' . "\n";

my $tab = $tabViewData{'tabData'};
while (my ($id, $values) = each(%$tab))
{
    # Nom
    $nameInput = $view->displayFormInputText('tabData[' . $id . '][name]', $values->{'name'}, '',
        {'size' => '40', 'maxlength' => '255', 'autocomplete' => 'off'});

    # Code
    $codeInput = $view->displayFormInputText('tabData[' . $id . '][code]', $values->{'code'}, '',
        {'size' => '15', 'maxlength' => '15', 'autocomplete' => 'off'});

    # Référence article
    $articleRefInput = $view->displayFormInputText('tabData[' . $id . '][value]', $values->{'value'}, '',
        {'size' => '18', 'maxlength' => '18', 'autocomplete' => 'off'});

    # Libellé
    $labelInput = $view->displayFormInputText('tabData[' . $id . '][label]', $values->{'label'}, '',
        {'size' => '80', 'maxlength' => '255', 'autocomplete' => 'off'});

    # Taux de TVA
    $vatInput = $view->displayFormInputText('tabData[' . $id . '][vat.id]', $values->{'vat.id'}, '',
        {'size' => '8', 'maxlength' => '8', 'autocomplete' => 'off'});

    # Bouton de suppression
    $removeBtn = $view->displayTextButton('', 'remove(|Over).png',
        'if (confirm("' . htmllocale('Voulez-vous vraiment supprimer cette référence article ?') . '")) removeValue(' . $id . ')',
        htmllocale('Supprimer'));

    # Champs cachés
    $htmlHidden = $view->displayFormInputHidden('tabData[' . $id . '][id]', $values->{'id'});
    $htmlHidden .= $view->displayFormInputHidden('tabData[' . $id . '][remove]', 0);

    print '<tr>' . "\n";
    print '<td>' . $nameInput . $htmlHidden . '</td>' . "\n";
    print '<td>' . $codeInput . '</td>' . "\n";
    print '<td>' . $articleRefInput . '</td>' . "\n";
    print '<td>' . $labelInput . '</td>' . "\n";
    print '<td>' . $vatInput . '</td>' . "\n";
    print '<td align="right">' . $removeBtn . '</td>' . "\n";
    print '</tr>' . "\n";
}

# Nom
$nameInput = $view->displayFormInputText('tabAdd[name]', '', '',
    {'size' => '40', 'maxlength' => '255', 'autocomplete' => 'off'});

# Code
$codeInput = $view->displayFormInputText('tabAdd[code]', '', '',
    {'size' => '15', 'maxlength' => '15', 'autocomplete' => 'off'});

# Référence article
$articleRefInput = $view->displayFormInputText('tabAdd[value]', '', '',
    {'size' => '18', 'maxlength' => '18', 'autocomplete' => 'off'});

# Référence article
$labelInput = $view->displayFormInputText('tabAdd[label]', '', '',
    {'size' => '80', 'maxlength' => '255', 'autocomplete' => 'off'});

# Taux de TVA
$vatInput = $view->displayFormInputText('tabAdd[vat.id]', '', '',
    {'size' => '8', 'maxlength' => '8', 'autocomplete' => 'off'});

# Bouton d'ajout
my $addBtn = $view->displayTextButton('', 'add(|Over).png', 'addValue()', htmllocale('Ajouter'));

# champ caché
$htmlHidden = $view->displayFormInputHidden('tabAdd[add]', 0);

print '<tr>' . "\n";
print '<td>' . $nameInput . $htmlHidden . '</td>' . "\n";
print '<td>' . $codeInput . '</td>' . "\n";
print '<td>' . $articleRefInput . '</td>' . "\n";
print '<td>' . $labelInput . '</td>' . "\n";
print '<td>' . $vatInput . '</td>' . "\n";
print '<td align="right">' . $addBtn . '</td>' . "\n";
print '</tr>' . "\n";

print '</table>';