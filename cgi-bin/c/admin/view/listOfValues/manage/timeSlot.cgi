use utf8;
use open (':encoding(UTF-8)');

use strict;

our %tabViewData;
our $view;
our $locale;


# Tableau des données
print '
<table class="standard" style="width: 100%">
<thead>
    <tr>
        <th>' . htmllocale('Libellé') . '</th>
        <th>' . htmllocale('Heure de début') . '</th>
        <th>' . htmllocale('Heure de fin') . '</th>
        <th>' . htmllocale('Date de création') . '</th>
        <th>' . htmllocale('Date de modification') . '</th>
        <th style="width: 16px;"></th>
    </tr>
</thead>
<tbody>';

my $tab = $tabViewData{'tabData'};
while (my ($id, $values) = each(%$tab))
{
    my $idInput        = $view->displayFormInputHidden('tabData[' . $id . '][id]', $id);

    my $beginHourInput = $view->displayFormInputText('tabData[' . $id . '][beginHour]',
                                                     $values->{'beginHour'},
                                                     '',
                                                     {
                                                      'size' => 2,
                                                      'maxlength' => 2,
                                                      'autocomplete' => 'off'
                                                     });
    my $endHourInput   = $view->displayFormInputText('tabData[' . $id . '][endHour]',
                                                     $values->{'endHour'},
                                                     '',
                                                     {
                                                      'size' => 2,
                                                      'maxlength' => 2,
                                                      'autocomplete' => 'off'
                                                     });

    # Bouton de suppression
    my $removeBtn = $view->displayTextButton('', 'remove(|Over).png',
                                             'if (confirm("' . $view->toJSEntities($locale->t('Voulez-vous vraiment supprimer cette valeur ?')) . '")) removeValue(' . $id . ')',
                                             $locale->t('Supprimer')) .
                    $view->displayFormInputHidden('tabData[' . $id . '][remove]', 0);


    print '
    <tr>
        <td>' . $view->toHTMLEntities($values->{'label'}) . '</td>
        <td>' . $beginHourInput . '</td>
        <td>' . $endHourInput . '</td>
        <td>' . $locale->getDateFormat($values->{'creationDate'}, LOC::Locale::FORMAT_DATETIME_NUMERIC) . '</td>
        <td>' . $locale->getDateFormat($values->{'modificationDate'}, LOC::Locale::FORMAT_DATETIME_NUMERIC) . '</td>
        <td>' . $removeBtn . $idInput . '</td>
    </tr>';
}


# Ajout d'une plage horaire
my $beginHourInput = $view->displayFormInputText('tabAdd[beginHour]',
                                                 '',
                                                 '',
                                                 {
                                                  'size' => 2,
                                                  'maxlength' => 2,
                                                  'autocomplete' => 'off'
                                                 });
my $endHourInput   = $view->displayFormInputText('tabAdd[endHour]',
                                                 '',
                                                 '',
                                                 {
                                                  'size' => 2,
                                                  'maxlength' => 2,
                                                  'autocomplete' => 'off'
                                                 });

# Bouton d'ajout
my $addBtn = $view->displayTextButton('', 'add(|Over).png', 'addValue()', $locale->t('Ajouter')) .
             $view->displayFormInputHidden('tabAdd[add]', 0);



print '
    <tr>
        <td>&nbsp;</td>
        <td>' . $beginHourInput . '</td>
        <td>' . $endHourInput . '</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>' . $addBtn . '</td>
    </tr>';

print '
<tbody>
</table>';