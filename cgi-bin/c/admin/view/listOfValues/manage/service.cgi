use utf8;
use open (':encoding(UTF-8)');

use strict;

our %tabViewData;
our $view;
our $locale;


my $js = "
/**
 * Sur changement du type de prestation
 */
function changeType(pattern)
{
    var type = \$(pattern + '[type]');
    var cost = Location.currenciesManager.getCurrency(pattern + '[cost]'); //\$(pattern + '[cost]');
    var amount = Location.currenciesManager.getCurrency(pattern + '[amount]'); //\$(pattern + '[amount]');
    var margin = \$(pattern + '[isMarginAllowed]');
    var quantity = \$(pattern + '[isQuantityAllowed]');
    
    // Désactive la saisie coût/montant sur une prestation externe
    if (type.value == 'EXT')
    {
        cost.setValue(0);
        amount.setValue(0);
        cost.getElement().style.visibility = 'hidden';
        amount.getElement().style.visibility = 'hidden';
        quantity.checked = false;
        quantity.style.visibility = 'hidden';
    }
    else if (type.value == 'INM')
    {
        amount.setValue(cost.getValue());
        cost.getElement().style.visibility = 'visible';
        cost.onchange = Function('Location.currenciesManager.getCurrency(\\'' + pattern + '[amount]\\').setValue(Location.currenciesManager.getCurrency(\\'' + pattern + '[cost]\\').getValue());');
        quantity.style.visibility = 'visible';
        amount.setValue(0);
        amount.getElement().style.visibility = 'hidden';
    }
    else
    {
        cost.getElement().style.visibility = 'visible';
        amount.getElement().style.visibility = 'visible';
        quantity.style.visibility = 'visible';
        amount.getElement().readOnly = false;
    }
    // Désactive la marge
    if (type.value == 'INT' || type.value == 'INM')
    {
        margin.checked = false;
        margin.style.visibility = 'hidden';
    }
    else
    {
        if (pattern == 'tabAdd')
        {
            margin.checked = true;
        }
        margin.style.visibility = 'visible';
    }
}";
print $view->displayJSBlock($js);

my $nameInput;
my $codeInput;
my $typeInput;
my $costInput;
my $amountInput;
my $marginInput;
my $quantityInput;
my $currencyInput;
my $infosInput;
my $removeBtn;
my $htmlHidden;

# Tableau des données
print '
<table class="standard" style="width: 100%">
    <tr>
        <th>' . htmllocale('Nom') . '</th>
        <th>' . htmllocale('Code') . '</th>
        <th>' . htmllocale('Type de presta') . '</th>
        <th>' . htmllocale('Coût') . '</th>
        <th>' . htmllocale('Montant') . '</th>
        <th>' . htmllocale('Marge') . '</th>
        <th>' . htmllocale('Quantité') . '</th>
        <th>' . htmllocale('Devise') . '</th>
        <th title="Informations complémentaires">' . htmllocale('Informations cpl') . '</th>
        <th></th>
    </tr>';

my $tab = $tabViewData{'tabData'};
while (my ($id, $values) = each(%$tab))
{
    # nom
    $nameInput = $view->displayFormInputText('tabData[' . $id . '][name]',
                                             $values->{'name'},
                                             '',
                                             {
                                              'size' => '40',
                                              'maxlength' => '255',
                                              'autocomplete' => 'off'
                                             });

    # code
    $codeInput = $view->displayFormInputText('tabData[' . $id . '][code]',
                                             $values->{'code'},
                                             '',
                                             {
                                              'size' => '10',
                                              'maxlength' => '10',
                                              'autocomplete' => 'off'
                                             });

    # type
    $typeInput = $view->displayFormSelect('tabData[' . $id . '][type]',
                                          $tabViewData{'tabTypes'},
                                          $values->{'type'},
                                          0,
                                          {
                                           'onchange' => 'changeType(\'tabData[' . $id . ']\')'
                                          });

    my $tabAttributes = {
        'style' => 'width: 70px;',
        'size' => '8',
        'maxlength' => '15',
        'autocomplete' => 'off'
    };

    if ($values->{'type'} eq 'EXT')
    {
        $tabAttributes->{'style'} = 'visibility: hidden;';
    }

    my $currencySymbol = $tabViewData{'tabCurrencies'}->{$values->{'currency.id'}};

    # cost
    $costInput = $view->displayFormInputCurrency('tabData[' . $id . '][cost]',
                                                 &LOC::Util::round($values->{'cost'},
                                                    $locale->getFracDigits()),
                                                 LOC::Html::CURRENCYMODE_INPUT,
                                                 $currencySymbol,
                                                 undef,
                                                 $tabAttributes);

    if ($values->{'type'} eq 'INM')
    {
        $tabAttributes->{'style'} = 'visibility: hidden;';
    }
    # amount
    $amountInput = $view->displayFormInputCurrency('tabData[' . $id . '][amount]',
                                                   &LOC::Util::round($values->{'amount'},
                                                        $locale->getFracDigits()),
                                                   LOC::Html::CURRENCYMODE_INPUT,
                                                   $currencySymbol,
                                                   undef,
                                                   $tabAttributes);

    $tabAttributes = {
        'style' => 'width: 70px;',
        'size' => '8',
        'maxlength' => '15',
        'autocomplete' => 'off'
    };

    # marge autorisée
    $tabAttributes = {};
    if ($values->{'type'} eq 'INT' || $values->{'type'} eq 'INM')
    {
        $tabAttributes->{'style'} = 'visibility: hidden;';
    }
    $marginInput = $view->displayFormInputCheckBox('tabData[' . $id . '][isMarginAllowed]',
                                                   $values->{'isMarginAllowed'},
                                                   $tabAttributes,
                                                   '',
                                                   1);

    # quantité autorisée
    $tabAttributes = {};
    if ($values->{'type'} eq 'EXT')
    {
        $tabAttributes->{'style'} = 'visibility: hidden;';
    }
    $quantityInput = $view->displayFormInputCheckBox('tabData[' . $id . '][isQuantityAllowed]',
                                                     $values->{'isQuantityAllowed'},
                                                     $tabAttributes,
                                                     '',
                                                     1);

    # devise    
    $currencyInput = $view->displayFormSelect('tabData[' . $id . '][currency.id]',
                                              $tabViewData{'tabCurrencies'},
                                              $values->{'currency.id'});

    # infos complémentaires
    $infosInput = $view->displayFormTextArea('tabData[' . $id . '][infos]', ($values->{'infos'} ? $values->{'infos'} : ''));

    # btn suppression
    $removeBtn = $view->displayTextButton('',
                                          'remove(|Over).png',
                                          'if (confirm("' . htmllocale('Voulez-vous vraiment supprimer cette valeur ?') . '")) removeValue(' . $id . ')',
                                          htmllocale('Supprimer'));

    # hidden
    $htmlHidden = $view->displayFormInputHidden('tabData[' . $id . '][id]', $values->{'id'});
    $htmlHidden .= $view->displayFormInputHidden('tabData[' . $id . '][remove]', 0);

    print '
    <tr>
        <td>' . $nameInput . $htmlHidden . '</td>
        <td>' . $codeInput . '</td>
        <td>' . $typeInput . '</td>
        <td>' . $costInput . '</td>
        <td>' . $amountInput . '</td>
        <td>' . $marginInput . '</td>
        <td>' . $quantityInput . '</td>
        <td>' . $currencyInput . '</td>
        <td>' . $infosInput . '</td>
        <td align="right">' . $removeBtn . '</td>
    </tr>';
}

# nom
$nameInput = $view->displayFormInputText('tabAdd[name]',
                                         '',
                                         '',
                                         {
                                          'size' => '40',
                                          'maxlength' => '255',
                                          'autocomplete' => 'off'
                                         });

# code
$codeInput = $view->displayFormInputText('tabAdd[code]',
                                         '',
                                         '',
                                         {
                                          'size' => '10',
                                          'maxlength' => '10',
                                          'autocomplete' => 'off'
                                         });

# type
$typeInput = $view->displayFormSelect('tabAdd[type]',
                                      $tabViewData{'tabTypes'},
                                      'INT',
                                      0,
                                      {
                                       'onchange' => 'changeType(\'tabAdd\')'
                                      });

# cost
$costInput = $view->displayFormInputCurrency('tabAdd[cost]',
                                             '',
                                             LOC::Html::CURRENCYMODE_INPUT,
                                             $tabViewData{'country'}->{'currency.symbol'},
                                             undef,
                                             {
                                              'style' => 'width: 70px;',
                                              'size' => '8',
                                              'maxlength' => '15',
                                              'autocomplete' => 'off'
                                             });

# amount
$amountInput = $view->displayFormInputCurrency('tabAdd[amount]',
                                               '',
                                               LOC::Html::CURRENCYMODE_INPUT,
                                               $tabViewData{'country'}->{'currency.symbol'},
                                               undef,
                                               {
                                                'style' => 'width: 70px;',
                                                'size' => '8',
                                                'maxlength' => '15',
                                                'autocomplete' => 'off'
                                               });

# marge autorisée
$marginInput = $view->displayFormInputCheckBox('tabAdd[isMarginAllowed]',
                                               0,
                                               {
                                                'style' => 'visibility: hidden;'
                                               },
                                               '',
                                               1);

# quantité autorisée
$quantityInput = $view->displayFormInputCheckBox('tabAdd[isQuantityAllowed]', 0);

# devise
$currencyInput = $view->displayFormSelect('tabAdd[currency.id]',
                                          $tabViewData{'tabCurrencies'},
                                          $tabViewData{'country'}->{'currency.id'});

# Informations complémentaires
$infosInput = $view->displayFormTextArea('tabAdd[infos]', '');

# btn ajout
my $addBtn = $view->displayTextButton('', 'add(|Over).png', 'addValue()', htmllocale('Ajouter'));

# champ caché
$htmlHidden = $view->displayFormInputHidden('tabAdd[add]', 0);

print '
    <tr>
        <td>' . $nameInput . $htmlHidden . '</td>
        <td>' . $codeInput . '</td>
        <td>' . $typeInput . '</td>
        <td>' . $costInput . '</td>
        <td>' . $amountInput . '</td>
        <td>' . $marginInput . '</td>
        <td>' . $quantityInput . '</td>
        <td>' . $currencyInput . '</td>
        <td>' . $infosInput . '</td>
        <td align="right">' . $addBtn . '</td>
    </tr>
</table>';