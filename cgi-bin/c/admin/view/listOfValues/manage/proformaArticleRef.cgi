use utf8;
use open (':encoding(UTF-8)');

use strict;

our %tabViewData;
our $view;
our $locale;

my $nameInput;
my $idInput;
my $labelInput;
my $labelExtraInput;
my $vatInput;
my $removeBtn;
my $htmlHidden;

# Tableau des données
print '<table class="standard" style="width: 100%">' . "\n";

print '<tr>' . "\n";
print '<th>' . htmllocale('Nom') . '</th>' . "\n";
print '<th>' . htmllocale('Code') . '</th>' . "\n";
print '<th>' . htmllocale('Libellé') . '</th>' . "\n";
print '<th>' . htmllocale('Libellé extra') . '</th>' . "\n";
print '<th>' . htmllocale('Taux de TVA') . '</th>' . "\n";
print '<th></th>' . "\n";
print '</tr>' . "\n";

my $tab = $tabViewData{'tabData'};
while (my ($id, $values) = each(%$tab))
{
    # Nom
    $nameInput = $view->displayFormInputText('tabData[' . $values->{'id'} . '][name]', $values->{'name'}, '',
        {'size' => '35', 'maxlength' => '255', 'autocomplete' => 'off'});

    # Code
    $idInput = $view->displayFormInputText('tabData[' . $values->{'id'} . '][code]', $values->{'id'}, '',
        {'size' => '15', 'maxlength' => '15', 'autocomplete' => 'off', 'disabled' => 'disabled'});

    # Libellé
    $labelInput = $view->displayFormInputText('tabData[' . $values->{'id'} . '][label]', $values->{'label'}, '',
        {'size' => '50', 'maxlength' => '255', 'autocomplete' => 'off'});

    # Libellé Extra
    $labelExtraInput = $view->displayFormInputText('tabData[' . $values->{'id'} . '][labelExtra]', $values->{'labelExtra'}, '',
        {'size' => '50', 'maxlength' => '255', 'autocomplete' => 'off'});

    # Taux de TVA
    $vatInput = $view->displayFormInputText('tabData[' . $values->{'id'} . '][vat.id]', $values->{'vat.id'}, '',
        {'size' => '8', 'maxlength' => '8', 'autocomplete' => 'off'});

    # Bouton de suppression
    $removeBtn = $view->displayTextButton('', 'remove(|Over).png',
        'if (confirm("' . htmllocale('Voulez-vous vraiment supprimer cette référence article ?') . '")) removeValue("' . $values->{'id'} . '")',
        htmllocale('Supprimer'));

    # Champs cachés
    $htmlHidden = $view->displayFormInputHidden('tabData[' . $values->{'id'} . '][id]', $values->{'id'});
    $htmlHidden .= $view->displayFormInputHidden('tabData[' . $values->{'id'} . '][remove]', 0);

    print '<tr>' . "\n";
    print '<td>' . $nameInput . $htmlHidden . '</td>' . "\n";
    print '<td>' . $idInput . '</td>' . "\n";
    print '<td>' . $labelInput . '</td>' . "\n";
    print '<td>' . $labelExtraInput . '</td>' . "\n";
    print '<td>' . $vatInput . '</td>' . "\n";
    print '<td align="right">' . $removeBtn . '</td>' . "\n";
    print '</tr>' . "\n";
}

# Nom
$nameInput = $view->displayFormInputText('tabAdd[name]', '', '',
    {'size' => '35', 'maxlength' => '255', 'autocomplete' => 'off'});

# Code
$idInput = $view->displayFormInputText('tabAdd[id]', '', '',
    {'size' => '15', 'maxlength' => '15', 'autocomplete' => 'off'});

# label article
$labelInput = $view->displayFormInputText('tabAdd[label]', '', '',
    {'size' => '50', 'maxlength' => '255', 'autocomplete' => 'off'});

# label extra article
$labelExtraInput = $view->displayFormInputText('tabAdd[labelExtra]', '', '',
    {'size' => '50', 'maxlength' => '255', 'autocomplete' => 'off'});

# Taux de TVA
$vatInput = $view->displayFormInputText('tabAdd[vat.id]', '', '',
    {'size' => '8', 'maxlength' => '8', 'autocomplete' => 'off'});

# Bouton d'ajout
my $addBtn = $view->displayTextButton('', 'add(|Over).png', 'addValue()', htmllocale('Ajouter'));

# champ caché
$htmlHidden = $view->displayFormInputHidden('tabAdd[add]', 0);

print '<tr>' . "\n";
print '<td>' . $nameInput . $htmlHidden . '</td>' . "\n";
print '<td>' . $idInput . '</td>' . "\n";
print '<td>' . $labelInput . '</td>' . "\n";
print '<td>' . $labelExtraInput . '</td>' . "\n";
print '<td>' . $vatInput . '</td>' . "\n";
print '<td align="right">' . $addBtn . '</td>' . "\n";
print '</tr>' . "\n";

print '</table>';