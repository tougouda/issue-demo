use utf8;
use open (':encoding(UTF-8)');

use strict;

our %tabViewData;
our $view;
our $locale;

my $nameInput;
my $codeInput;
my $removeBtn;
my $htmlHidden;

# Tableau des données
print '<table class="standard" style="width: 100%">' . "\n";

print '<tr>' . "\n";
print '<th>' . htmllocale('Nom') . '</th>' . "\n";
print '<th>' . htmllocale('Code') . '</th>' . "\n";
print '<th></th>' . "\n";
print '</tr>' . "\n";

my $tab = $tabViewData{'tabData'};
while (my ($id, $values) = each(%$tab))
{
    # nom
    $nameInput = $view->displayFormInputText('tabData[' . $id . '][name]', $values->{'name'}, '',
        {'size' => '40', 'maxlength' => '255', 'autocomplete' => 'off'});
        
    # code
    $codeInput = $view->displayFormInputText('tabData[' . $id . '][code]', $values->{'code'}, '',
        {'size' => '10', 'maxlength' => '10', 'autocomplete' => 'off'});
        
    # btn suppression
    $removeBtn = $view->displayTextButton('', 'remove(|Over).png',
        'if (confirm("' . htmllocale('Voulez-vous vraiment supprimer cette valeur ?') . '")) removeValue(' . $id . ')',
        htmllocale('Supprimer'));

    # hidden
    $htmlHidden = $view->displayFormInputHidden('tabData[' . $id . '][id]', $values->{'id'});
    $htmlHidden .= $view->displayFormInputHidden('tabData[' . $id . '][remove]', 0);

    print '<tr>' . "\n";
    print '<td>' . $nameInput . $htmlHidden . '</td>' . "\n";
    print '<td>' . $codeInput . '</td>' . "\n";
    print '<td align="right">' . $removeBtn . '</td>' . "\n";
    print '</tr>' . "\n";
}

# nom
$nameInput = $view->displayFormInputText('tabAdd[name]', '', '',
    {'size' => '40', 'maxlength' => '255', 'autocomplete' => 'off'});
    
# code
$codeInput = $view->displayFormInputText('tabAdd[code]', '', '',
    {'size' => '10', 'maxlength' => '10', 'autocomplete' => 'off'});
    
# btn ajout
my $addBtn = $view->displayTextButton('', 'add(|Over).png', 'addValue()', htmllocale('Ajouter'));

# champ caché
$htmlHidden = $view->displayFormInputHidden('tabAdd[add]', 0);

print '<tr>' . "\n";
print '<td>' . $nameInput . $htmlHidden . '</td>' . "\n";
print '<td>' . $codeInput . '</td>' . "\n";
print '<td align="right">' . $addBtn . '</td>' . "\n";
print '</tr>' . "\n";

print '</table>';