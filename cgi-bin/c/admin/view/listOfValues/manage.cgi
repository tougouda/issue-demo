use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;
use LOC::Locale;

our %tabViewData;
our $view;
our $locale;

$locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
$view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


$view->setPageTitle($locale->t($tabViewData{'listOfVal'}->{'name'}));

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle($locale->t($tabViewData{'listOfVal'}->{'description'}));

# CSS
$view->addCSSSrc('location.css');

# JS
$view->addJSSrc('admin/listOfValues/manage.js');

# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();

# Sélection du pays
if ($tabViewData{'listOfVal'}->{'isMultiCountries'})
{
    print '<br />
    <span><b>' .
        htmllocale('Pays') . '</b>&nbsp; : &nbsp;' .
        $view->displayFormSelect('countryId', $tabViewData{'tabCountries'}, $tabViewData{'countryId'}, 0,
            {'onchange' => 'refresh()'}) . '
    </span>
    <br /><br />';
}


# Affichage de la liste
require $tabViewData{'scriptPath'} . 'manage/' . $tabViewData{'listOfVal'}->{'fileName'};


# champs cachés
my $htmlHidden = '';
$htmlHidden .= $view->displayFormInputHidden('id', $tabViewData{'id'});
$htmlHidden .= $view->displayFormInputHidden('valid', '');

# Bouton de validation
my $btnValid = $view->displayTextButton(htmllocale('Modifier'), 'valid(|Over).png', 'submitForm()');

# Affichage des champs cachés
print '
    <div id="chpHiddens" style="display: none;">' .
        $htmlHidden . '
    </div>';

print '<br />';

# Affichage du panel
print $view->displayControlPanel({'left' => [$btnValid], 'substyle' => 'bottom'});

# Affichage des messages
my $messages = $tabViewData{'messages'};
my $count = scalar keys(%$messages);
if ($count > 0)
{
    print '<p>';
    while (my ($type, $msgs) = each(%$messages))
    {
        print $view->displayMessages($type, $msgs, 1);
    }
    print '</p>';
}

print $view->displayFooter();
