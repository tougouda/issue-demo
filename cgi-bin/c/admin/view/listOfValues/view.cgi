use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;
use LOC::Locale;

our %tabViewData;
my $view;
my $locale;


$locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
$view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


$view->setPageTitle($locale->t('Listes de valeurs'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle($locale->t('Listes de valeurs'));

# CSS
$view->addCSSSrc('location.css');
$view->addCSSSrc('tr.rowBg0, tr.rowBg1 {cursor: pointer;}');

# JS
$view->addJSSrc('admin/listOfValues/manage.js');
                                                 
# bouton de raffraîchissement
my $btnRefresh = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png', 'refresh()');

$view->addControlsContent($btnRefresh, 'right');

# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();

print '
<br />
<table id="tableLov" class="standard ex" style="width: 100%;">
    <tr>
        <th>' . htmllocale('Nom') . '</th>
        <th>' . htmllocale('Description') . '</th>
        <th>' . htmllocale('Code') . '</th>
        <th>' . htmllocale('Module') . '</th>
        <th>' . htmllocale('Multi-pays') . '</th>
    </tr>';
my $i = 1;
my $tabListOfValues = $tabViewData{'tabListOfValues'};
while (my ($id, $lov) = each(%$tabListOfValues))
{
    my $url = $view->createURL('admin:listOfValues:manage', {'id' => $id});
    
    print '
    <tr class="rowBg'. ($i % 2) .'" onclick="LOC_View.openWindow(\'listOfValues\', \'' . $url . '\')">
        <td>' .
            $lov->{'name'} . '
        </td>
        <td>' .
            $lov->{'description'} . '
        </td>
        <td>' .
            $lov->{'code'} . '
        </td>
        <td>' .
            $lov->{'package'} . '
        </td>
        <td>' .
            ($lov->{'isMultiCountries'} ? $locale->t('Oui') : $locale->t('Non')) . '
        </td>
    </tr>';
            
    $i++;
}
print '
</table>';

print $view->displayFooter();
