use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;
use LOC::Locale;

our %tabViewData;
our $view;
our $locale;
our %values;




$locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
$view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}

my $isNew = $tabViewData{'id'} ? 0 : 1; # Ajout ou modification

$view->setPageTitle($locale->t('Gestion des messages d\'information > ' . ($isNew ? 'Ajout' : 'Modification')))
     ->addPackage('spincontrols');

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle($locale->t('Gestion des messages d\'information > ' . ($isNew ? 'Ajout' : 'Modification')));

# CSS
$view->addCSSSrc('location.css');

# JS
$view->addJSSrc('admin/message/manage.js');

# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();

# Traductions pour le javascript
$view->addTranslations({
    'fieldRequired' => $locale->t('Le sujet et le message sont obligatoires')
});

# Affichage du formulaire d'édition

# - Éléments du formulaire
# -- sujet
my $subjectInput = $view->displayFormInputText('subject', 
                                               $tabViewData{'message'}->{'subject'}, 
                                               '', 
                                               {
                                                 'size' => '40', 
                                                 'maxlength' => '45', 
                                                 'autocomplete' => 'off'
                                               });

# -- date de début

my $dateStartInput = $view->displayFormInputDate('dateStart', $tabViewData{'message'}->{'dateStart'});
my $hourStartInput = $view->displayMaskedFormInputText('hourStart', $tabViewData{'message'}->{'hourStart'},
                                                      {'maxlength' => 5, 'size' => 4,
                                                       'format' => '__:__', 'separator' => ':', 'typeon' => '_',
                                                       'allowed' => '0123456789'});

# -- date de fin
my $dateEndInput = $view->displayFormInputDate('dateEnd', $tabViewData{'message'}->{'dateEnd'});
my $hourEndInput = $view->displayMaskedFormInputText('hourEnd', $tabViewData{'message'}->{'hourEnd'},
                                                      {'maxlength' => 5, 'size' => 4,
                                                       'format' => '__:__', 'separator' => ':', 'typeon' => '_',
                                                       'allowed' => '0123456789'});

# -- Priorité
my $priority = ($tabViewData{'message'}->{'priority'} ? $tabViewData{'message'}->{'priority'} : 3);
my $priorityInput = $view->displayFormInputText('priority', $priority, '', {'class' => 'spinInput', 'style' => 'width:20px'});
$priorityInput .= $view->displayJSBlock('Location.spinControlsManager.createSpinControl("priority", 1, 3, 1);');

#-- Message principal (FR)
my $contentFR = (defined $tabViewData{'message'}->{'content'} ? $tabViewData{'message'}->{'content'} : '');
my $messageTextarea = $view->displayFormTextArea('countryTA[fr_FR]', $contentFR, {'cols' => 50, 'rows' => 4});


# -- Traductions
my $count = 0;
my %countryLine;

while (my ($country, $tabCountry) = each(%{$tabViewData{'countries'}}))
{
    my $isChecked = 0;
    if (index($tabViewData{'message'}->{'countryFilter'}, $country) < 0)
    {
        $isChecked = 0;
    }
    else
    {
        $isChecked = 1;
    }
    if ($isNew && $country eq 'FR')
    {
        $isChecked = 1;
    }
    $countryLine{$count}->{'checkbox'} = $view->displayFormInputCheckBox('countryCB[' . $country . ']', $isChecked, {}, $country, 1);
    if ($country ne 'FR')
    {
        
        my $content = '';
        my $subject;

        $content = (defined $tabViewData{'message'}->{'trad'}->{$tabCountry->{'locale.id'}}->{'content'} ? $tabViewData{'message'}->{'trad'}->{$tabCountry->{'locale.id'}}->{'content'} : '');
        $subject = (defined $tabViewData{'message'}->{'trad'}->{$tabCountry->{'locale.id'}}->{'subject'} ? $tabViewData{'message'}->{'trad'}->{$tabCountry->{'locale.id'}}->{'subject'} : '');
        $countryLine{$count}->{'textSubject'} = $view->displayFormInputText('tradSubject[' . $tabCountry->{'locale.id'} . ']', $subject, '', 
                                                                             {
                                                                               'size' => '40', 
                                                                               'maxlength' => '45', 
                                                                               'autocomplete' => 'off'
                                                                             });

        $countryLine{$count}->{'textarea'} = $view->displayFormTextArea('countryTA[' . $tabCountry->{'locale.id'} . ']', $content,
                                                                         {'cols' => 50, 'rows' => 4});
        
    }
    $count++;
}

print '
<fieldset>
    <legend>' . htmllocale('Informations générales') . '</legend>
    <table class="formTable">
        <tr>
            <td class="label">' . htmllocale('Sujet') . $view->displayRequiredSymbol() . '</td>
            <td>' . $subjectInput . '</td>
        </tr>
        <tr>
            <td class="label">' . htmllocale('Date de début') . '</td>
            <td>' . $dateStartInput . ' ' . $hourStartInput . '</td>
        </tr>
        <tr>
            <td class="label">' . htmllocale('Date de fin') . '</td>
            <td>' . $dateEndInput . ' ' . $hourEndInput . '</td>
        </tr>
        <tr>
            <td class="label">' . htmllocale('Priorité') . '</td>
            <td>' . $priorityInput . '</td>
        </tr>
        <tr>
            <td class="label">' . htmllocale('Message') . $view->displayRequiredSymbol() . '</td>
            <td>' . $messageTextarea . '</td>
        </tr>
    </table>
</fieldset>';

print '
<fieldset>
    <legend>' . htmllocale('Message') . '</legend>
    <table class="formTable">';
    foreach my $rowCountry (values %countryLine)
    {
        print '
         <tr>
             <td class="label"> ' . $rowCountry->{'checkbox'} . '</td>
             <td class="label"> ' . $rowCountry->{'textSubject'} . '<br />' . $rowCountry->{'textarea'} . '</td>
         </tr>
         <tr>
            <td class="label">&nbsp;</td>
         </tr>';
    }

print '
    </table>
</fieldset>';



# champs cachés
my $htmlHidden = '';
$htmlHidden .= $view->displayFormInputHidden('id', $tabViewData{'id'});
$htmlHidden .= $view->displayFormInputHidden('valid', $tabViewData{'valid'});

# Bouton de validation
my $btnValid = $view->displayTextButton(htmllocale(($isNew ? 'Ajouter' : 'Modifier')), 'valid(|Over).png', 'submitForm()');

# Affichage des champs cachés
print '
    <div id="chpHiddens" style="display: none;">' .
        $htmlHidden . '
    </div>';

print '<br />';

# Affichage du panel
print $view->displayControlPanel({'left' => [$btnValid], 'substyle' => 'bottom'});

# Affichage des messages
my $retour = $tabViewData{'retour'};
my $count = scalar keys(%$retour);
if ($count > 0)
{
    print '<p>';
    while (my ($type, $msgs) = each(%$retour))
    {
        print $view->displayMessages($type, $msgs, 1);
    }
    print '</p>';
}

print $view->displayFooter();
