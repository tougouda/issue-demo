use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;
use LOC::Locale;

our %tabViewData;
my $view;
my $locale;


$locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
$view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


$view->setPageTitle($locale->t('Gestion des messages d\'information'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle($locale->t('Gestion des messages d\'information'));

# CSS
$view->addCSSSrc('location.css');
$view->addCSSSrc('tr.rowBg0, tr.rowBg1 {cursor: pointer;}');

# JS
$view->addJSSrc('admin/message/manage.js');

# Bouton d'ajout
my $btnAdd = $view->displayTextButton((htmllocale('Ajouter')),
                                       'add(|Over).png',
                                       'LOC_View.openWindow(\'message\', \'' . $view->createURL('admin:message:manage') . '\')');
# bouton de rafraîchissement
my $btnRefresh = $view->displayTextButton($locale->t('Actualiser'), 'refresh(|Over).png', 'refresh()');

$view->addControlsContent($btnAdd);
$view->addControlsContent($btnRefresh, 'right');

# Affichage de l'entête
print $view->displayHeader();
print $view->startForm();

print '
<br />
<table id="tableLov" class="standard ex" style="width: 100%;">
    <tr>
        <th></th>
        <th>' . htmllocale('Sujet') . '</th>
        <th>' . htmllocale('Priorité') . '</th>
        <th>' . htmllocale('Pays') . '</th>
        <th>' . htmllocale('Date de début') . '</th>
        <th>' . htmllocale('Date de fin') . '</th>
        <th></th>
    </tr>';
my $i = 1;

my $tabListMessages = $tabViewData{'tabListMessages'};
while (my ($id, $lov) = each(%$tabListMessages))
{
    my $url = $view->createURL('admin:message:manage', {'id' => $id});
    # Bouton de suppression
    my $removeBtn = $view->displayTextButton('', 'remove(|Over).png',
                                             'if (confirm("' . $view->toJSEntities($locale->t('Voulez-vous vraiment supprimer ce message ?')) . '")) removeMessage(' . $id . ');',
                                             $locale->t('Supprimer')) .
                    $view->displayFormInputHidden('tabData[' . $id . '][remove]', 0);
    
    # Flag d'indication de l'état du message
        # - terminé  : noir  
        # - à venir  : jaune 
        # - en cours : vert  
    my $colorFlag = '';
    my $altFlag = '';
    if ($lov->{'flag'} == 0)
    {
        $colorFlag = 'Black';
        $altFlag   = 'Terminé';
    }
    elsif ($lov->{'flag'} == 1)
    {
        $colorFlag = 'Yellow';
        $altFlag   = 'A venir';
    }
    else
    {
        $colorFlag = 'Green';
        $altFlag   = 'En cours';
    }
    my $flag = $view->displayImage('flag' . $colorFlag . '.png', 
                                   {
                                     'title' => $altFlag
                                   });
    
    # vérification date
    if ($lov->{'dateEnd'} eq '0000-00-00 00:00:00')
    {
        $lov->{'dateEnd'} = '';
    }
    if ($lov->{'dateStart'} eq '0000-00-00 00:00:00')
    {
        $lov->{'dateStart'} = '';
    }
    
    print '
    <tr class="rowBg'. ($i % 2) .'">
        <td onclick="LOC_View.openWindow(\'message\', \'' . $url . '\')">' .
            $flag . '
        </td>
        <td onclick="LOC_View.openWindow(\'message\', \'' . $url . '\')">' .
            $lov->{'subject'} . '
        </td>
        <td onclick="LOC_View.openWindow(\'message\', \'' . $url . '\')">' .
            $lov->{'priority'} . '
        </td>
        <td onclick="LOC_View.openWindow(\'message\', \'' . $url . '\')">' .
            $lov->{'countryFilter'} . '
        </td>
        <td onclick="LOC_View.openWindow(\'message\', \'' . $url . '\')">' .
            $locale->getDateFormat($lov->{'dateStart'}) . '
        </td>
        <td onclick="LOC_View.openWindow(\'message\', \'' . $url . '\')">' .
            $locale->getDateFormat($lov->{'dateEnd'}) . '
        </td>
        <td>' .
            $removeBtn . '
        </td>
    </tr>';
            
    $i++;
}
print '
</table>';
# Affichage des messages de retour
my $retour = $tabViewData{'retour'};
my $count = scalar keys(%$retour);
if ($count > 0)
{
    print '<p>';
    while (my ($type, $msgs) = each(%$retour))
    {
        print $view->displayMessages($type, $msgs, 1);
    }
    print '</p>';
}


print $view->displayFooter();
