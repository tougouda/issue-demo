use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;
use LOC::Locale;

our %tabViewData;


my $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
my $view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});

sub htmllocale
{
    return $view->toHTMLEntities($locale->t(@_));
}


$view->setPageTitle($locale->t('Gestion des utilisateurs'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle($locale->t('Gestion des utilisateurs'));

$view->addPackage('searchboxes');

$view->addCSSSrc('location.css');

$view->addCSSSrc('admin/users/view.css')
     ->addJSSrc('admin/users/view.js');


my $tabCfg = {
    'loadUrl' => $view->createURL('admin:users:load'),
    'saveUrl' => $view->createURL('admin:users:save')
};


$view->addBodyEvent('load', 'userManager.init(' . &LOC::Json::toJson($tabCfg) . ');');



my $userListUrl = $view->createURL('default:user:jsonList', {'format' => 1});
my $userList = $view->displayFormSelect('search_user_id', {}, 0, 0) .
$view->displayJSBlock('
var searchUserSearchBox = Location.searchBoxesManager.createSearchBox(
                                                                      "search_user_id",
                                                                      {
                                                                       isRequired:false,
                                                                       url:"' . $userListUrl . '"
                                                                      }
                                                                     );');
my $userSelectBtn = $view->displayTextButton('', 'search(|Over).png', 'userManager.addUser(window.document.getElementById(\'search_user_id\').value);', $locale->t('Rechercher')) .
$view->displayTextButton('', 'clear.gif', 'clearSearch();', $locale->t('Effacer la recherche'));

my $searchBar = '
<div id="searchBar">' .
    '<span class="title">' . htmllocale('Recherche') . '&nbsp;:</span>' .
    '<span class="content">' . $userList . '</span>' .
    '<span class="controls">' . $userSelectBtn . '</span>' .
'</div>';

$view->addControlsContent([$searchBar], 'left', 1);

# Bouton d'ajout rapide du profil de l'utilisateur connecté
if ($tabViewData{'currentUserId'})
{
    my $addMeBtn = $view->displayTextButton($locale->t('Ouvrir mon profil'),
                                            'add(|Over).png',
                                            'userManager.addUser(' . $tabViewData{'currentUserId'} . ');',
                                            $locale->t('Ouvrir mon profil'),
                                            'l',
                                            {'id'    => 'user_addmebtn',
                                             'class' => 'locCtrlButton disabled'
                                            });
    $view->addControlsContent([$addMeBtn], 'right', 1);
}


# Affichage de l'entête
print $view->displayHeader();


# Boutons de contrôle
my @tabLeftBtns = ();
push(@tabLeftBtns, $view->displayTextButton($locale->t('Sauvegarder'),
                                            'admin/users/save.png',
                                            'userManager.save();',
                                            $locale->t('Sauvegarder les informations de l\'utilisateur en cours'),
                                            'l',
                                            {'id'    => 'user_savebtn',
                                             'class' => 'locCtrlButton disabled'
                                            }));
push(@tabLeftBtns, $view->displayTextButton($locale->t('Réinitialiser'),
                                            'admin/users/reset.png',
                                            'userManager.reset();',
                                            $locale->t('Réinitialiser les informations de l\'utilisateur en cours'),
                                            'l',
                                            {'id'    => 'user_resetbtn',
                                             'class' => 'locCtrlButton disabled'
                                            }));
push(@tabLeftBtns, $view->displayTextButton($locale->t('Copier'),
                                            'admin/users/copy.png',
                                            'userManager.copy();',
                                            $locale->t('Copier les informations de l\'utilisateur en cours'),
                                            'l',
                                            {'id'    => 'user_copybtn',
                                             'class' => 'locCtrlButton disabled'
                                            }));
push(@tabLeftBtns, $view->displayTextButton($locale->t('Coller'),
                                            'admin/users/paste.png',
                                            'userManager.paste();',
                                            $locale->t('Coller les informations du presse papier sur l\'utilisateur en cours'),
                                            'l',
                                            {'id'    => 'user_pastebtn',
                                             'class' => 'locCtrlButton disabled'
                                            }));

my @tabProfilesList = ({'value' => 0, 'innerHTML' => '---'});
while (my ($id, $label) = each(%{$tabViewData{'tabProfiles'}}))
{
    push(@tabProfilesList, {
        'value'     => $id,
        'innerHTML' => $label
    });
}

my @tabLevels = ();
my $nbLevels = @{$tabViewData{'tabLevelsInfos'}};
for (my $i = 0; $i < $nbLevels; $i++)
{
    push(@tabLevels, {
        'value'     => $tabViewData{'tabLevelsInfos'}->[$i]->{'id'},
        'innerHTML' => $tabViewData{'tabLevelsInfos'}->[$i]->{'label'}
    });
}

print '
<fieldset id="userIU" class="none">
<legend>' . htmllocale('Utilisateur') . '</legend>

<div id="user_message">' . htmllocale('Veuillez sélectionner un utilisateur s.v.p.') . '</div>

<div id="user_form">
    <div class="loading"></div>
    <div class="content">
        <div id="userTabBox">
            <div class="tabs"></div>
            <div class="content">
                <table class="formTable" id="user_table">
                    <tbody>
                        <tr class="login">
                            <td class="label">' . htmllocale('Login') . '&nbsp;:</td>
                            <td class="content"><span id="user_login"></span></td>
                        </tr>
                        <tr class="profile">
                            <td class="label">' . htmllocale('Profil') . '&nbsp;:</td>
                            <td class="content">' . $view->displayFormSelect('user_profile', \@tabProfilesList, 0, 0, {'onchange' => 'userManager.setProfileId(this.value);'}) . '</td>
                        </tr>
                        <tr class="level">
                            <td class="label">' . htmllocale('Niveau') . '&nbsp;:</td>
                            <td class="content">' . $view->displayFormSelect('user_level', \@tabLevels, 0, 0, {'onchange' => 'userManager.setLevel(this.value);'}) . '</td>
                        </tr>
                        <tr class="isNomad">
                            <td class="label">' . htmllocale('Nomade') . '&nbsp;:</td>
                            <td class="content"><div id="user_nomad" class="checkbox" onclick="userManager.toggleNomad();"></div></td>
                        </tr>
                        <tr class="nomadAgencies">
                            <td class="label">&nbsp;</td>
                            <td class="content">
                                <table class="nomadAgencies">
                                <tbody>';

my @tabAgenciesList;
my $tabCountriesInfos = $tabViewData{'tabCountriesInfos'};
my $nbCountries = @$tabCountriesInfos;
for (my $i = 0; $i < $nbCountries; $i++)
{
    print '
                                    <tr class="country">
                                        <td class="name" onclick="userManager.switchNomadAgencies(getNomadAgenciesBySpans(this.parentNode.cells[1].getElementsByTagName(\'span\')));">' . $tabCountriesInfos->[$i]->{'label'} . '</td>
                                        <td class="areas">
                                            <table>
                                            <tbody>';
    my $tabAreas = $tabCountriesInfos->[$i]->{'tabAreas'};
    my $nbAreas  = @$tabAreas;
    for (my $j = 0; $j < $nbAreas; $j++)
    {
        print '
                                                <tr class="area' . ($tabAreas->[$j]->{'id'} ? '' : ' notaffected') . '">
                                                    <td class="name" onclick="userManager.switchNomadAgencies(getNomadAgenciesBySpans(this.parentNode.cells[1].getElementsByTagName(\'span\')));">' . ($tabAreas->[$j]->{'id'} ? $tabAreas->[$j]->{'label'} : htmllocale('Pas de région')) . '</td>
                                                    <td class="agencies">';
        my $tabAgencies = $tabAreas->[$j]->{'tabAgencies'};
        my $nbAgencies = @$tabAgencies;
        for (my $k = 0; $k < $nbAgencies; $k++)
        {
            print '
<span class="agency off" title="' . $tabAgencies->[$k]->{'label'} . '" oncontextmenu="userManager.setAgencyId(this.innerHTML); return false;" onclick="userManager.switchNomadAgency(this.innerHTML);">' . $tabAgencies->[$k]->{'id'} . '</span>';

            push(@tabAgenciesList, {
                'value'     => $tabAgencies->[$k]->{'id'},
                'innerHTML' => $tabAgencies->[$k]->{'label'}
            });
        }
        print '
                                                    </td>
                                                </tr>';
    }
    print '
                                            </tbody>
                                            </table>
                                        </td>
                                    </tr>';
}
# Tri des régions par ordre alpha
@tabAgenciesList = sort {$a->{'value'} cmp $b->{'value'}} @tabAgenciesList;

print '
                                </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr class="agencyId">
                            <td class="label">' . htmllocale('Agence') . '&nbsp;:</td>
                            <td class="content">' . $view->displayFormSelect('user_agency', \@tabAgenciesList, 0, 0, {'onchange' => 'userManager.setAgencyId(this.value);'}) . '</td>
                        </tr>
                        <tr class="groups">
                            <td class="label">' . htmllocale('Groupes') . '&nbsp;:</td>
                            <td class="content">
                                <div class="groups">';

my $tabGroups = $tabViewData{'tabGroupsInfos'};
my $nbGroups = @$tabGroups;
for (my $i = 0; $i < $nbGroups; $i++)
{
print '
<span class="group off" onclick="userManager.switchGroup(this.getAttribute(\'value\'));" value="' . $tabGroups->[$i]->{'id'} . '">' . $tabGroups->[$i]->{'label'} . '</span>'
}
print '
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            ' . $view->displayControlPanel({'left' => \@tabLeftBtns, 'substyle' => 'bottom'}) . '
        </div>
    </div>
</div>


</fieldset>';



print $view->displayFooter();




