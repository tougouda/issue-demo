use utf8;
use open (':encoding(UTF-8)');

use strict;

use LOC::Html::Standard;
use LOC::Locale;

# Répertoire courant
my $directory = dirname(__FILE__);
$directory =~ s/(.*)\/.+/$1/;

# Répertoire CSS/JS/Images
our $dir = 'admin/characteristics/';

our %tabViewData;


my $locale = &LOC::Locale::getLocale($tabViewData{'localeId'});
my $view = LOC::Html::Standard->new($tabViewData{'localeId'}, $tabViewData{'user.agency.id'});


$view->setPageTitle($locale->t('Liste des caractéristiques'));

$view->setDisplay(LOC::Html::Standard::DISPFLG_HEAD |
                  LOC::Html::Standard::DISPFLG_FOOT |
                  LOC::Html::Standard::DISPFLG_BTCLOSE |
                  LOC::Html::Standard::DISPFLG_BTPRINT |
                  LOC::Html::Standard::DISPFLG_CTRLS);

$view->setTitle($locale->t('Liste des caractéristiques'));
require($directory . '/characteristics/format/list.cgi');
&formatList($tabViewData{'list'}, $locale);

# jQuery
$view->addJSSrc('jQuery/jquery.js');
$view->addJSSrc($dir . '/list.js');
$view->addCSSSrc($dir . '/list.css');

$view->addPackage('modalwindow');

# Bouton "actualiser"
my $btnRefresh = $view->displayTextButton($locale->t('Actualiser'),
                                          'refresh(|Over).png',
                                          $view->createURL('admin:characteristics:list'),
                                          $locale->t('Actualiser la liste'),
                                          'l',
                                          {'tabindex' => -1,
                                           'onfocus' => 'this.blur();'
                                          });

$view->addControlsContent($btnRefresh, 'right', 1);


# Affichage de l'entête
print $view->displayHeader();

$view->addTranslations({
    'type_G' => $locale->t('Global'),
    'type_P' => $locale->t('Pays'),
    'type_A' => $locale->t('Agence')
});

my $filterInput = $view->displayFormInputText('filter', undef, undef,
                                              {'placeholder' => $locale->t('Recherche')});

print $filterInput;

print '
<table class="basic" id="list">
    <thead>
        <tr>
            <th class="state"></th>
            <th class="id">' . $locale->t('Id.') . '</th>
            <th class="code">' . $locale->t('Code') . '</th>
            <th class="label">' . $locale->t('Libellé') . '</th>
            <th class="type">' . $locale->t('Type') . '</th>
            <th class="externId"></th>
            <th class="beginDate">' . $locale->t('Date de début') . '</th>
            <th class="value">' . $locale->t('Valeur') . '</th>
            <th class="history" title="Historique">' . $locale->t('Hist.') . '</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>';

# Popup d'historique
print '
<div class="popup" style="display: none;" id="history-popup">
    <h1></h1>
    <table class="basic">
        <thead>
            <tr>
                <th class="type">' . $locale->t('Type') . '</th>
                <th class="externId"></th>
                <th class="beginDate">' . $locale->t('Date de début') . '</th>
                <th class="beginDate">' . $locale->t('Date de fin') . '</th>
                <th class="value">' . $locale->t('Valeur') . '</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>';


print $view->displayFooter();


# Variables JS et initialisation
print $view->displayJSBlock('
STATE_ACTIVE = "' . LOC::Characteristic::STATE_ACTIVE . '";
STATE_DELETED = "' . LOC::Characteristic::STATE_DELETED . '";
charac.init(' . &LOC::Json::toJson($tabViewData{'list'}) . ',
            ' . &LOC::Json::toJson({'tabCountries' => $tabViewData{'tabCountries'},
                                    'tabAgencies' => $tabViewData{'tabAgencies'}}) . ');');


# Méthodes d'affichage

# Function: _getType
# Récupérer le type de caractéristiques (global, country, agency)
sub _getType
{
    my ($externId) = @_;

    if (!defined $externId)
    {
        return 'G';
    } 
    if (&LOC::Util::in_array($externId, $tabViewData{'tabCountries'}))
    {
        return 'P';
    }
    if (&LOC::Util::in_array($externId, $tabViewData{'tabAgencies'}))
    {
        return 'A';
    }
}


