use utf8;
use open (':encoding(UTF-8)');

use strict;


sub formatList
{
    my ($tabData, $locale, $tabOptions) = @_;

    # Formatage des lignes
    foreach my $tabCharacInfos (values %$tabData)
    {
        foreach my $tabInfos (@$tabCharacInfos)
        {
            if (ref $tabInfos eq 'ARRAY')
            {
                foreach my $row (@$tabInfos)
                {
                    $row->[2] = $row->[2] ? $locale->getDateFormat($row->[2], LOC::Locale::FORMAT_DATE_NUMERIC) : '';
                    $row->[3] = $row->[3] ? $locale->getDateFormat($row->[3], LOC::Locale::FORMAT_DATE_NUMERIC) : '';
                }
#
#                my @tab = grep {!defined $_->[3]} @$tabInfos;
#                $tabInfos = \@tab;
            }
        }

    }

}

1;
