use utf8;
use open (':encoding(UTF-8)');

# Package: message
# Contrôleur Messages d'information
package message;

use strict;
use File::Basename;

use LOC::User;
use LOC::Util;
use LOC::Country;
use LOC::Message;

# Function: manageAction
# Prépare les données pour l'affichage de l'interface d'édition / création d'un message
sub manageAction
{
    my $user = &LOC::Session::getUserInfos();
    my $_POST;
    
    # Variables pour la vue
    our %tabViewData = (
        'user.agency.id' => $user->{'nomadAgency.id'},
    );
    
    # hachage de l'utilisateur
    $tabViewData{'localeId'} = $user->{'locale.id'};
    
    # Récupération des variables du formulaire
    $tabViewData{'id'}                     = &LOC::Request::getInteger('id', 0);
    $tabViewData{'tabData'}                = &LOC::Request::getHash('tabData');
    $tabViewData{'valid'}                  = &LOC::Request::getString('valid');
    $tabViewData{'message'}->{'subject'}   = &LOC::Request::getString('subject');
    $tabViewData{'message'}->{'dateStart'} = &LOC::Request::getDate('dateStart');
    $tabViewData{'message'}->{'hourStart'} = &LOC::Request::getString('hourStart');
    $tabViewData{'message'}->{'dateEnd'}   = &LOC::Request::getDate('dateEnd');
    $tabViewData{'message'}->{'hourEnd'}   = &LOC::Request::getString('hourEnd');
    $tabViewData{'message'}->{'priority'}  = &LOC::Request::getInteger('priority', 3);
    $tabViewData{'countryFilter'}          = &LOC::Request::getHash('countryCB');
    $tabViewData{'allContent'}             = &LOC::Request::getHash('countryTA');
    $tabViewData{'message'}->{'content'}   = $tabViewData{'allContent'}->{'fr_FR'};
    delete($tabViewData{'allContent'}->{'fr_FR'});
    $tabViewData{'tradSubject'}            = &LOC::Request::getHash('tradSubject');
    
    $tabViewData{'countries'}         = &LOC::Country::getList(LOC::Util::GETLIST_ASSOC, {});

    # Ajout / modification
    our $values;
    if ($tabViewData{'valid'} eq 'ok')
    {
        # Préparation des données
        # - Vérification des dates
        my $dateStart;
        my $dateEnd;
        if ($tabViewData{'message'}->{'dateStart'} ne '')
        {
            $dateStart = $tabViewData{'message'}->{'dateStart'} . ' ' . $tabViewData{'message'}->{'hourStart'} . ':00';
        }
        else
        {
            $dateStart = $tabViewData{'message'}->{'dateStart'} . ' 00:00:00';
        }
        if ($tabViewData{'message'}->{'dateEnd'} ne '')
        {
            $dateEnd = $tabViewData{'message'}->{'dateEnd'} . ' ' . $tabViewData{'message'}->{'hourEnd'} . ':00';
        }
        else
        {
            $dateEnd = $tabViewData{'message'}->{'dateEnd'} . ' 00:00:00';
        }
        
        
        $values = {
                    'subject'       => $tabViewData{'message'}->{'subject'},
                    'dateStart'     => $dateStart,
                    'dateEnd'       => $dateEnd,
                    'priority'      => $tabViewData{'message'}->{'priority'},
                    'countryFilter' => join(';', keys(%{$tabViewData{'countryFilter'}})),
                    'content'       => $tabViewData{'message'}->{'content'},
                    'trad'          => {
                                         'content' => $tabViewData{'allContent'},
                                         'subject' => $tabViewData{'tradSubject'}
                    }
        };
        if ($tabViewData{'id'} != 0)
        {
            $values->{'id'} = $tabViewData{'id'};
        }
        my $tabResult = &LOC::Message::set($values);
        if ($tabResult->{'error'} > 0)
        {
            if ($tabViewData{'id'} != 0)
            {
                $tabViewData{'retour'} = {'error' => ['Une erreur s\'est produite lors de la modification']};
            }
            else
            {
                $tabViewData{'retour'} = {'error' => ['Une erreur s\'est produite lors de la création']};
            }
        }
        else
        {
            if ($tabViewData{'id'} != 0)
            {
                $tabViewData{'retour'} = {'valid' => ['Modifications enregistrées']};
            }
            else
            {
                $tabViewData{'retour'} = {'valid' => ['Création effectuée']};
                $tabViewData{'id'} = $tabResult->{'id'};
            }
        }
    }
    
    
    $tabViewData{'message'}           = &LOC::Message::getInfos($tabViewData{'id'});
    if ($tabViewData{'message'} == 0)
    {
        $tabViewData{'message'} = {};
    }

    $tabViewData{'message'}->{'trad'} = &LOC::Message::getTraductions($tabViewData{'id'});
    
    # Chemin d'accès aux scripts de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    $directory .= '/view/message/';
    $tabViewData{'scriptPath'} = $directory;

    # Affichage de la vue
    require $tabViewData{'scriptPath'} . 'manage.cgi';
}

sub viewAction
{
    my $user = &LOC::Session::getUserInfos();
    
    # Variables pour la vue
    our %tabViewData = ( 
        'user.agency.id' => $user->{'nomadAgency.id'},
    );
    
    # hachage de l'utilisateur
    $tabViewData{'localeId'} = $user->{'locale.id'};
    
    # Récupération des variables du formulaire
    $tabViewData{'tabData'}     = &LOC::Request::getHash('tabData');
    
    # Suppression
    my $tab = $tabViewData{'tabData'};
    while (my ($id, $values) = each(%$tab))
    {
        if ($values->{'remove'})
        {
            $tabViewData{'result'} = &LOC::Message::delete($id);
            if ($tabViewData{'result'} > 0)
            {
                $tabViewData{'retour'} = {'valid' => ['La ligne a été supprimée']};
            }
            else
            {
                $tabViewData{'retour'} = {'error' => ['Une erreur s\'est produite lors de la suppression']};
            }
        }
    }
    
    # Récupère la liste des listes de valeurs
    $tabViewData{'tabListMessages'} = &LOC::Message::getList(
                                                  LOC::Util::GETLIST_ASSOC,
                                                  $tabViewData{'localeId'},
                                                  {
                                                     'period' => 'no',
                                                     'order' => 'desc'
                                                  });
    
    

    # chemin d'accès aux scripts de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    $directory .= '/view/message/';

    # affichage de la vue
    require $directory . 'view.cgi';
}

1;