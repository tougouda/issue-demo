use utf8;
use open (':encoding(UTF-8)');

# Package: listOfValues
# Contrôleur listes de valeurs
package listOfValues;

use strict;
use File::Basename;

use LOC::User;
use LOC::Util;
use LOC::Country;
use LOC::ListOfValues;


# Function: manageAction
# Prépare les données pour l'affichage de l'interface d'édition d'une liste
sub manageAction
{
    my $user = &LOC::Session::getUserInfos();

    # Variables pour la vue
    our %tabViewData = (
        'user.agency.id' => $user->{'nomadAgency.id'},
    );

    # hachage de l'utilisateur
    $tabViewData{'user'} = $user;
    $tabViewData{'localeId'} = $user->{'locale.id'};

    # Récupération des variables du formulaire
    $tabViewData{'id'}          = &LOC::Request::getInteger('id');
    $tabViewData{'countryId'}   = &LOC::Request::getString('countryId', '');
    $tabViewData{'tabData'}     = &LOC::Request::getHash('tabData');
    $tabViewData{'tabAdd'}      = &LOC::Request::getHash('tabAdd');
    $tabViewData{'valid'}       = &LOC::Request::getString('valid');

    # Infos concernant la liste de valeurs
    my $listOfVal = &LOC::ListOfValues::getInfos($tabViewData{'countryId'}, $tabViewData{'id'});
    $listOfVal->{'fileName'}  = $listOfVal->{'code'} . '.cgi';
    $tabViewData{'listOfVal'} = $listOfVal;
    if ($tabViewData{'countryId'} eq '')
    {
        $tabViewData{'countryId'} = $user->{'nomadCountry.id'};
    }
    eval('use ' . $listOfVal->{'package'} . ';');

    # Récupération des listes
    if ($listOfVal->{'isMultiCountries'} == 1)
    {
        $tabViewData{'tabCountries'} = &LOC::Country::getList(LOC::Util::GETLIST_PAIRS);
    }

    # Récupération des infos du pays sélectionné
    if ($tabViewData{'countryId'} ne '')
    {
        $tabViewData{'country'} = &LOC::Country::getInfos($tabViewData{'countryId'});
    }

    # Inclusion du contrôleur spécifique à la liste de valeurs
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    $directory .= '/controller/listOfValues/';
    if (-e $directory . $listOfVal->{'fileName'})
    {
        require $directory . $listOfVal->{'fileName'};
    }

    # Mise à jour des lignes
    if ($tabViewData{'valid'} ne '')
    {
        my $result;

        # Itère sur toutes les lignes de la liste
        my $tab = $tabViewData{'tabData'};
        while (my ($id, $values) = each(%$tab))
        {
            # Suppression
            if ($values->{'remove'})
            {
                eval('$result = &' . $listOfVal->{'package'} . '::delete($tabViewData{\'countryId\'}, $id);');
                if ($result > 0)
                {
                    $tabViewData{'valid'} = 'deleted';
                }
            }
            # Modification
            else
            {
                eval('$result = &' . $listOfVal->{'package'} . '::set($tabViewData{\'countryId\'}, $values);');
                if ($result > 0)
                {
                    $tabViewData{'valid'} = 'saved';
                }
            }
        }

        # Ajout
        if ($tabViewData{'tabAdd'}->{'add'} == 1)
        {
            eval('$result = &' . $listOfVal->{'package'} . '::set($tabViewData{\'countryId\'}, $tabViewData{\'tabAdd\'})');
            if ($result > 0)
            {
                $tabViewData{'valid'} = 'added';
            }
            else
            {
                $tabViewData{'valid'} = '';
                $tabViewData{'messages'} = {'error' => ['Une erreur s\'est produite lors de l\'enregistrement']};
            }
        }
    }

    # (Re)chargement des données
    if ($tabViewData{'id'} != 0 || $tabViewData{'valid'} ne '')
    {
        # Initialise les données
        eval('$tabViewData{\'tabData\'} = &' . $listOfVal->{'package'} . '::getList($tabViewData{\'countryId\'}, LOC::Util::GETLIST_ASSOC);');
    }

    # Message de confirmation
    if ($tabViewData{'valid'} eq 'added')
    {
        $tabViewData{'messages'} = {'valid' => ['La ligne a été ajoutée']};
    }
    elsif ($tabViewData{'valid'} eq 'saved')
    {
        $tabViewData{'messages'} = {'valid' => ['Les modifications ont été enregistrées']};
    }
    elsif ($tabViewData{'valid'} eq 'deleted')
    {
        $tabViewData{'messages'} = {'valid' => ['La ligne a été supprimée']};
    }

    # Chemin d'accès aux scripts de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    $directory .= '/view/listOfValues/';
    $tabViewData{'scriptPath'} = $directory;

    # Affichage de la vue
    require $tabViewData{'scriptPath'} . 'manage.cgi';
}

sub viewAction
{
    my $user = &LOC::Session::getUserInfos();

    # Variables pour la vue
    our %tabViewData = (
        'user.agency.id' => $user->{'nomadAgency.id'},
    );

    # hachage de l'utilisateur
    $tabViewData{'localeId'} = $user->{'locale.id'};

    # Récupère la liste des listes de valeurs
    $tabViewData{'tabListOfValues'} = &LOC::ListOfValues::getList($user->{'nomadCountry.id'},
                                                                  LOC::Util::GETLIST_ASSOC);

    # chemin d'accès aux scripts de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    $directory .= '/view/listOfValues/';

    # affichage de la vue
    require $directory . 'view.cgi';
}

1;