use utf8;
use open (':encoding(UTF-8)');

# Package: characteristic
# Contrôleur pour la gestion des caractéristiques
package characteristics;

use strict;
use File::Basename;

use LOC::Country;
use LOC::Agency;
use LOC::Characteristic;
use LOC::Session;
use LOC::Util;
use LOC::Json;


# Function: viewAction
# Liste des caractéristiques
sub listAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};

    my $countryId      = $tabUserInfos->{'nomadCountry.id'};
    my $agencyId       = $tabUserInfos->{'nomadAgency.id'};

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'currencySymbol' => $tabUserInfos->{'currency.symbol'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    # Liste des caractéristiques
    my $tabCharacteristics = &LOC::Characteristic::getList();
    $tabViewData{'list'} = &LOC::Util::sortHashByKeys($tabCharacteristics);

    # Données pour la vue
    $tabViewData{'tabCountries'} = &LOC::Country::getList(LOC::Util::GETLIST_IDS, {'stateId' => [LOC::Country::STATE_ACTIVE, LOC::Country::STATE_DELETED]});
    $tabViewData{'tabAgencies'}  = &LOC::Agency::getList(LOC::Util::GETLIST_IDS);


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/characteristics/list.cgi';
}


1;