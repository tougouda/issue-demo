use utf8;
use open (':encoding(UTF-8)');

# Package: users
# Contrôleur de gestion des utilisateurs
package users;

use strict;
use File::Basename;

use LOC::User;
use LOC::Agency;
use LOC::Area;
use LOC::Country;

sub viewAction
{
    my $tabParameters = $_[0];

    my $tabSession   = &LOC::Session::getInfos();
    my $tabUserInfos = $tabParameters->{'tabUserInfos'};


    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'}
    );

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('auth');

    # Récupération des données des agences et des pays
    my $tabAgencies  = &LOC::Agency::getList(LOC::Util::GETLIST_ASSOC, {});
    my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_ASSOC, {});
    my $tabAreas     = &LOC::Area::getList(LOC::Util::GETLIST_ASSOC, {});

    my $tabCountriesInfos = [];
    while (my ($countryId, $tabCountryInfos) = each(%$tabCountries))
    {
        my $tabAr = [];
        while (my ($areaId, $tabAreaInfos) = each(%$tabAreas))
        {
            if ($tabAreaInfos->{'country.id'} eq $countryId)
            {
                my $tabAg = [];
                while (my ($agencyId, $tabAgencyInfos) = each(%$tabAgencies))
                {
                    if ($tabAgencyInfos->{'area.id'} eq $areaId)
                    {
                        push(@$tabAg, {'id' => $agencyId, 'label' => $tabAgencyInfos->{'label'}});
                    }
                }

                push(@$tabAr, {'id' => $areaId, 'label' => $tabAreaInfos->{'label'}, 'tabAgencies' => $tabAg});
            }
        }

        my $tabAg = [];
        while (my ($agencyId, $tabAgencyInfos) = each(%$tabAgencies))
        {
            if ($tabAgencyInfos->{'country.id'} eq $countryId && $tabAgencyInfos->{'area.id'} eq '')
            {
                push(@$tabAg, {'id' => $agencyId, 'label' => $tabAgencyInfos->{'label'}});
            }
        }
        my $nbAg = @$tabAg;
        if ($nbAg > 0)
        {
            push(@$tabAr, {'id' => 0, 'label' => '', 'tabAgencies' => $tabAg});
        }


        push(@$tabCountriesInfos, {'id' => $countryId, 'label' => $tabCountryInfos->{'label'}, 'tabAreas' => $tabAr});
    }

    my $tabGroupsInfos = &LOC::User::getGroupsList();

    my $tabLevelsInfos = &LOC::User::getLevelsList();

    my $query = '
SELECT
    PROFILAUTO,
    CONCAT(LPAD(PROFILAUTO, 3, "000"), " - ", CONVERT(PROFILLIBELLE USING utf8))
FROM PROFIL
ORDER BY PROFILAUTO;';

    my $tabProfiles = $db->fetchPairs($query);


    # Préparation des variables pour la vue
    $tabViewData{'tabProfiles'}       = $tabProfiles;
    $tabViewData{'tabCountriesInfos'} = $tabCountriesInfos;
    $tabViewData{'tabGroupsInfos'}    = $tabGroupsInfos;
    $tabViewData{'tabLevelsInfos'}    = $tabLevelsInfos;
    $tabViewData{'currentUserId'}     = $tabUserInfos->{'id'};


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/users/view.cgi';
}



sub loadAction
{
    # Variables pour la vue
    our %tabViewData = ();

    # Récupération des paramètres
    my $id = &LOC::Request::getInteger('id');

    # Récupération des données de l'utilisateur
    $tabViewData{'tabUserInfos'} = &_getUserInfos($id);

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/users/load.cgi';
}

sub saveAction
{
    # Variables pour la vue
    our %tabViewData = ();

    # Récupération des paramètres
    my $id               = &LOC::Request::getInteger('id');
    my $profileId        = &LOC::Request::getInteger('profileId');
    my $level            = &LOC::Request::getInteger('level');
    my $isNomad          = &LOC::Request::getBoolean('isNomad');
    my $tabGroups        = &LOC::Request::getArray('tabGroups');
    my $tabNomadAgencies = &LOC::Request::getArray('tabNomadAgencies');
    my $agencyId         = &LOC::Request::getString('agencyId');


    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('auth');

    if ($profileId == 0)
    {
        $level   = 0;
        $isNomad = 0;
        $tabNomadAgencies = [];
        $tabGroups = [];
    }

    # Sauvegarde des informations de l'utilisateur
    $db->update('PERSONNEL', {'PENIVEAU'   => $level * 1,
                              'NOMADEOK'   => ($isNomad ? -1 : 0),
                              'PEGROUPES'  => join(';', @$tabGroups)}, {'PEAUTO' => $id});
    $db->delete('AGENCEAUTORISEE', {'PEAUTO' => $id});
    if ($isNomad)
    {
        my $nbAgencies = @$tabNomadAgencies;
        if ($nbAgencies > 0)
        {
            for (my $i = 0; $i < $nbAgencies; $i++)
            {
                $db->insert('AGENCEAUTORISEE', {'PEAUTO' => $id, 'AGAUTO' => $tabNomadAgencies->[$i]});
            }
        }
    }

    # Sauvegarde du profil et de l'agence
    &LOC::User::setProfile($id, $profileId);
    &LOC::User::setNomadAgency($id, $agencyId);



    # Récupération des données de l'utilisateur
    $tabViewData{'tabUserInfos'} = &_getUserInfos($id);

    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/users/save.cgi';

}


sub _getUserInfos
{
    my ($id) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('auth');

    my $query = '
SELECT
    T1.PEAUTO AS `id`,
    T1.AGAUTO AS `agency.id`,
    T1.PELOGIN AS `login`,
    CONCAT_WS(" ", T1.PENOM, T1.PEPRENOM) AS `fullName`,
    T1.PENIVEAU AS `level`,
    -T1.NOMADEOK AS `isNomad`,
    (SELECT GROUP_CONCAT(ST1.AGAUTO SEPARATOR ";") FROM AGENCEAUTORISEE ST1 WHERE ST1.PEAUTO = T1.PEAUTO) AS `tabNomadAgencies`,
    T1.PEGROUPES AS `tabGroups`,
    T1.PROFILAUTO AS `profile.id`
FROM PERSONNEL T1
WHERE T1.PEAUTO = ' . $id . ';';
    my $tabUserInfos = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);

    my @tabNomadAgencies = split(';', $tabUserInfos->{'tabNomadAgencies'});
    my @tabGroups        = split(';', $tabUserInfos->{'tabGroups'});

    $tabUserInfos->{'id'}               *= 1;
    $tabUserInfos->{'profile.id'}       *= 1;
    if ($tabUserInfos->{'profile.id'} == 0)
    {
        $tabUserInfos->{'level'}             = 0;
        $tabUserInfos->{'isNomad'}           = 0;
        $tabUserInfos->{'tabNomadAgencies'}  = [];
        $tabUserInfos->{'tabGroups'}         = [];
    }
    else
    {
        $tabUserInfos->{'level'}            *= 1;
        $tabUserInfos->{'isNomad'}          *= 1;
        if ($tabUserInfos->{'isNomad'})
        {
            $tabUserInfos->{'tabNomadAgencies'}  = \@tabNomadAgencies;
        }
        else
        {
            $tabUserInfos->{'tabNomadAgencies'}  = [];
        }
        $tabUserInfos->{'tabGroups'}         = \@tabGroups;
    }

    return $tabUserInfos;
}

1;