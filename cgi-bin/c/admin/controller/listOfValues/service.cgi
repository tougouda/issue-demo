use utf8;
use open (':encoding(UTF-8)');

use LOC::Currency;

our %tabViewData;

# liste des devises
$tabViewData{'tabCurrencies'} = &LOC::Currency::getList($tabViewData{'countryId'}, LOC::Util::GETLIST_PAIRS);

# liste des types de presta
$tabViewData{'tabTypes'} = &LOC::Repair::Service::getTypesList($tabViewData{'countryId'}, LOC::Util::GETLIST_ASSOC, $tabViewData{'localeId'});