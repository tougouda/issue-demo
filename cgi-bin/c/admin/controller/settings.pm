use utf8;
use open (':encoding(UTF-8)');

# Contrôleur de gestion du paramétrage de l'application.
package settings;


use strict;
use File::Basename;

use LOC::Maintenance;


# Function: viewAction
# Gère l'affichage de la page de paramétrage de l'application.
#
# Parameters:
# hashref $tabParameters - Les paramètres.
sub viewAction
{
    my $tabParameters = $_[0];

    my $tabUserInfos = $tabParameters->{'tabUserInfos'};
    my $tabRights = &LOC::Maintenance::getRights($tabUserInfos);

    # Variables pour la vue
    our %tabViewData = (
        'locale.id'      => $tabUserInfos->{'locale.id'},
        'user.agency.id' => $tabUserInfos->{'nomadAgency.id'},
        'isEditable'     => $tabRights->{'set'},
        'isPreviewable'  => $tabRights->{'preview'},
        'route'          => &LOC::Controller::Front::getRouteName(),
        'tabLevels'      => &LOC::User::getLevelsList(),
        'isActivated'    => 0,
        'level'          => LOC::User::LEVEL_ADMIN,
        'title'          => '',
        'period' => 0,
        'pending' => {
            'heading' => '',
            'message' => ''
        },
        'complete' => {
            'heading' => 0,
            'message' => ''
        },
        'tabErrors'      => {}
    );


    # Soumission du formulaire
    $tabViewData{'valid'} = &LOC::Request::getString('valid');
    if ($tabViewData{'valid'} eq 'ok')
    {
        # Données du formulaire
        my $tabData = {
            'isActivated'    => &LOC::Request::getBoolean('isActivated', 0),
            'level'          => &LOC::Request::getInteger('level', 0),
            'title'          => &LOC::Request::getString('title', ''),
            'period' => &LOC::Request::getInteger('period', 0),
            'pending' => {
                'heading' => &LOC::Request::getString('pending.heading', ''),
                'message' => &LOC::Request::getString('pending.message', '')
            },
            'complete' => {
                'heading' => &LOC::Request::getString('complete.heading', ''),
                'message' => &LOC::Request::getString('complete.message', '')
            }
        };
        if ($tabData->{'level'} == 0)
        {
            delete $tabData->{'level'};
        }

        # Mise à jour des paramètres
        $tabViewData{'state'} = (&LOC::Maintenance::setSettings($tabData, $tabUserInfos->{'id'}, $tabViewData{'tabErrors'}) ? 'ok' : 'ko');
    }


    # Récupération des paramètres
    my $charac = &LOC::Maintenance::getSettings(1);
    if ($charac)
    {
        $tabViewData{'isActivated'}           = $charac->{'isActivated'};
        $tabViewData{'level'}                 = $charac->{'level'};
        $tabViewData{'title'}                 = $charac->{'title'};
        $tabViewData{'period'}                = $charac->{'period'};
        $tabViewData{'pending'}->{'heading'}  = $charac->{'pending'}->{'heading'};
        $tabViewData{'pending'}->{'message'}  = $charac->{'pending'}->{'message'};
        $tabViewData{'complete'}->{'heading'} = $charac->{'complete'}->{'heading'};
        $tabViewData{'complete'}->{'message'} = $charac->{'complete'}->{'message'};
    }


    # Affichage de la vue
    my $directory = dirname(__FILE__);
    $directory =~ s/(.*)\/.+/$1/;
    require $directory . '/view/settings/view.cgi';
}

1;