#!/usr/bin/perl

use utf8;
use open (':encoding(UTF-8)', ':std');

$| = 1;

use constant {
    DEFAULT_PORT => 1080
};

use strict;
use lib 'inc', 'lib';

# Modules internes
use LOC::Globals;
use LOC::Soap::Daemon;
use LOC::WebService;


# Récupération du numéro de port dans la configuration
my $soapServerUrl = &LOC::Globals::get('soapServerUrl');
my $port = DEFAULT_PORT;
if ($soapServerUrl->{'location'} =~ /:([0-9]+)\/?$/)
{
    $port = $1;
}


# Lancement du daemon
my $daemon = LOC::Soap::Daemon->new(LocalPort => $port, Reuse => 1)
                        ->dispatch_to('LOC::WebService');

print "Le serveur SOAP est disponible à ", $daemon->url, "\n"; 

# Attente de connexions
$daemon->handle();
