use utf8;
use open (':encoding(UTF-8)');

# Class: LOC::Soap::Daemon
# Module gérant le daemon SOAP (étend SOAP::Transport::HTTP::Daemon)
package LOC::Soap::Daemon;


use strict;
use vars qw(@ISA);

# Modules externes
use SOAP::Transport::HTTP;

# Modules internes
use LOC::ForkProcess;


@ISA = qw(SOAP::Transport::HTTP::Daemon);


# Pour ne pas accumuler les zombies
$SIG{CHLD} = 'IGNORE';


sub handle
{
    my $self = shift(@_)->new();

    # Attente de requête entrante
    while (my $connection = $self->accept())
    {
        print "\tConnexion reçue...\n"; 

        &LOC::ForkProcess::fork(sub {
            # Partie réservée au processus fils
            # Fermeture de la socket d'écoute
            $self->close();

            # Traitement de la requête
            while (my $request = $connection->get_request())
            {
                $self->request($request);
                $self->SOAP::Transport::HTTP::Server::handle();
                $connection->send_response($self->response);
            }

            # Fermeture de la connexion et fin du processus
            $connection->close();
        });
    }
    continue
    { 
        # Fermeture de la connexion
        $connection->close();
    }
}

1;
