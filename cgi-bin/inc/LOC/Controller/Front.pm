use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Controller::Front
# Module de gestion du contrôleur frontal
package LOC::Controller::Front;
use strict;

use LOC::Maintenance;
use LOC::Request;
use LOC::Session;
use LOC::Timer;

# Timer pour getElapsedSecs
my $timer;

# Attributs
my $module = undef;
my $controller = undef;
my $action = undef;

my $defaultModule = undef;
my $defaultController = undef;
my $defaultAction = undef;


# Function: setDefaultModuleName
# Définir le nom du module par défaut
#
# Parameters:
# string $module - 
sub setDefaultModuleName
{
    $defaultModule = $_[0];
}


# Function: setDefaultControllerName
# Définir le nom du contrôleur par défaut
#
# Parameters:
# string $controller - 
sub setDefaultControllerName
{
    $defaultController = $_[0];
}


# Function: setDefaultActionName
# Définir le nom de l'action par défaut
#
# Parameters:
# string $action - 
sub setDefaultActionName
{
    $defaultAction = $_[0];
}


# Function: getModuleName
# Récupérer le nom du module
#
# Returns:
# string - 
sub getModuleName
{
    if (defined $module)
    {
        return $module;
    }
    return &LOC::Request::getString('locModule', $defaultModule, LOC::Request::SRC_REQUEST);
}


# Function: getControllerName
# Récupérer le nom du contrôleur
#
# Returns:
# string - 
sub getControllerName
{
    if (defined $controller)
    {
        return $controller;
    }
    return &LOC::Request::getString('locController', $defaultController, LOC::Request::SRC_REQUEST);
}


# Function: getActionName
# Récupérer le nom de l'action
#
# Returns:
# string - 
sub getActionName
{
    if (defined $action)
    {
        return $action;
    }
    return &LOC::Request::getString('locAction', $defaultAction, LOC::Request::SRC_REQUEST);
}


# Function: getRouteName
# Récupérer le nom de la route
#
# Returns:
# string - 
sub getRouteName
{
    return getModuleName() . ':' . getControllerName() . ':' . getActionName();
}


# Function: getElapsedSecs
# Récupérer les secondes écoulées depuis le dispatch
#
# Returns:
# float - 
sub getElapsedSecs
{
    if (defined $timer)
    {
        return &LOC::Timer::end($timer);
    }
    return undef;
}


# Function: dispatch
# Exécute le contrôleur d'action approprié
#
# Parameters:
# string $currentDir - Répertoire courant
# string $module     - Nom du module
# string $controller - Nom du contrôleur
# string $action     - Nom de l'action
sub dispatch
{
    my ($currentDir, $module, $controller, $action) = @_;

    # Démarrage du chrono
    $timer = &LOC::Timer::start();

    # Charge la session
    if (&LOC::Session::load() > 0)
    {
        # Récupération des noms du module, du contrôleur et de l'action
        $module     = (defined $module     ? $module     : &getModuleName());
        $controller = (defined $controller ? $controller : &getControllerName());
        $action     = (defined $action     ? $action     : &getActionName());

        my $tabUserInfos = &LOC::Session::getUserInfos();

        if (
            &LOC::Maintenance::isActivated()
            && !&LOC::Maintenance::canOverride($module, $controller, $action, $tabUserInfos)
        )
        {
            # Mode maintenance
            my $tabMaintenanceMca = &LOC::Maintenance::getInfoPageMca();
            $module     = $tabMaintenanceMca->{'module'};
            $controller = $tabMaintenanceMca->{'controller'};
            $action     = $tabMaintenanceMca->{'action'}
        }

        # Vérifie les droits d'accès à la fonctionnalité
        if (&LOC::Session::isAccessAuthorized($module, $controller, $action))
        {
            # Fichier du controller d'action
            my $file = $currentDir . '/c/';
            if ($module)
            {
                $file .= $module . '/';
            }
            $file .= 'controller/' . $controller . '.pm';

            require $file;

            my $tabParameters = {'tabUserInfos' => $tabUserInfos};
            my $expr = $controller . '::' . $action . 'Action($tabParameters)';
            eval($expr) or die "Error executing expression \"$expr\" :\n" . $@;
        }
        else
        {
            require $currentDir . '/c/unauthorizedArea.cgi';
        }
    }
    else
    {
        require $currentDir . '/c/doubleConnection.cgi';
    }
}


sub _printTrace
{
    my $elapsed = (defined $timer ? &LOC::Timer::end($timer) : 0);
    print STDERR 'Etape [' . $_[0] . '] : ' . sprintf('%.6f', $elapsed) . "\n";
}

1;