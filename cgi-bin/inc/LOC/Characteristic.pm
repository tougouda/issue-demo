use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Characteristic
# Module d'accès aux caractéristiques
package LOC::Characteristic;


# Constants: Les différentes caractéristiques
# MAINTENANCE - Maintenance
use constant {
    MAINTENANCE        => 'MAINTENANCE',
    CUST_MAX_FILE_SIZE => 'MAXSIZECTRLCUST'
};

# Constants: États
# STATE_ACTIVE  - Actif
# STATE_DELETED - Supprimé
use constant {
    STATE_ACTIVE  => 'GEN01',
    STATE_DELETED => 'GEN03'
};

use strict;

# Modules
use LOC::Db;
use LOC::Session;
use LOC::Date;


my $tabCharacteristics;
my $tabAgencies;


# Function: getList
# Récupère la liste des caractéristiques
#
# Returns
# hashref
sub getList
{
    if (!defined $tabCharacteristics)
    {
        &_getCharacs();
    }
    return $tabCharacteristics;
}

# Function: getValueByCode
# Récupère la valeur instantanée de la caractéristique dans l'agence de l'utilisateur connecté
#
# Parameters:
# string $code    - Le code de la caractéristique
# string $date    - La date de valeur de la caractéristique (format date MySQL, optionnelle, date du jour par défaut)
# string $default - Valeur par défaut
#
# Returns:
# mixed|undef - Valeur ou indéfini si il y a une erreur (valeur ou caractéristique introuvable)
sub getValueByCode
{
    my ($code, $date, $default) = @_;
    return &getAgencyValueByCode($code, undef, $date, $default);
}

# Function: getAgencyValueByCode
# Récupère la valeur de la caractéristique dans une agence
#
# Parameters:
# string $code         - Le code de la caractéristique
# string $agencyId     - Le code de l'agence (optionnel, agence de l'utilisateur par défaut)
# string $date         - La date de valeur de la caractéristique (format date MySQL, optionnelle, date du jour par défaut)
# string $default      - Valeur par défaut
# int    $disableCache - Désactivation du cache (0 : cache non désactivé, 1 : cache désactivé). Vaut 0 par défaut
#
# Returns:
# mixed|undef Valeur ou indéfini si il y a une erreur (valeur ou caractéristique introuvable)
sub getAgencyValueByCode
{
    my ($code, $agencyId, $date, $default, $disableCache) = @_;

    # Si pas d'agence en paramètre, on récupère l'agence de la personne connectée
    unless ($agencyId)
    {
        my $userInfos = &LOC::Session::getUserInfos();
        unless ($userInfos)
        {
            return $default;
        }
        $agencyId = $userInfos->{'nomadAgency.id'};
    }
    # Si pas de date en paramètre, date du jour
    unless ($date)
    {
        $date = &LOC::Date::getMySQLDate();
    }
    # Cache non désactivé par défaut
    unless ($disableCache)
    {
        $disableCache = 0;
    }

    if ($disableCache || !defined $tabCharacteristics)
    {
        &_getCharacs();
    }

    if (exists $tabCharacteristics->{$code})
    {
        my $tab = $tabCharacteristics->{$code}->[2];
        my $nbElements = @$tab;
        for (my $i = 0; $i < $nbElements; $i++)
        {
            if ($tab->[$i]->[0] eq $agencyId &&
                ($tab->[$i]->[2] eq '' || $date ge $tab->[$i]->[2]) &&
                ($tab->[$i]->[3] eq '' || $date le $tab->[$i]->[3]))
            {
                return $tab->[$i]->[1];
            }
        }

        if ($disableCache || !defined $tabAgencies)
        {
            &_getAgencies();
        }

        if (exists $tabAgencies->{$agencyId})
        {
            return &getCountryValueByCode($code, $tabAgencies->{$agencyId}, $date, $default);
        }
    }
    return $default;
}

# Function: getCountryValueByCode
# Récupère la valeur de la caractéristique dans un pays
#
# Parameters:
# string $code         - Le code de la caractéristique
# string $countryId    - Le code du pays (optionnel, pays de l'utilisateur par défaut)
# string $date         - La date de valeur de la caractéristique (format date MySQL, optionnelle, date du jour par défaut)
# string $default      - Valeur par défaut
# int    $disableCache - Désactivation du cache (0 : cache non désactivé, 1 : cache désactivé). Vaut 0 par défaut
#
# Returns:
# mixed|undef - Valeur ou indéfini si il y a une erreur (valeur ou caractéristique introuvable)
sub getCountryValueByCode
{
    my ($code, $countryId, $date, $default, $disableCache) = @_;

    # si pas de pays en paramètre, on récupère l'agence de la personne connectée
    unless ($countryId)
    {
        my $userInfos = &LOC::Session::getUserInfos();
        # si pas cd'agence utilisateur, retour
        unless ($userInfos)
        {
            return undef;
        }
        $countryId = $userInfos->{'nomadCountry.id'};
    }
    # si pas de date en paramètre, date du jour
    unless ($date)
    {
        $date = &LOC::Date::getMySQLDate();
    }
    # On ne prend que la date, pas l'heure
    $date = substr($date, 0, 10);
    # Cache non désactivé par défaut
    unless ($disableCache)
    {
        $disableCache = 0;
    }

    if ($disableCache || !defined $tabCharacteristics)
    {
        &_getCharacs();
    }

    if (exists $tabCharacteristics->{$code})
    {
        my $tab = $tabCharacteristics->{$code}->[1];
        my $nbElements = @$tab;
        for (my $i = 0; $i < $nbElements; $i++)
        {
            if ($tab->[$i]->[0] eq $countryId &&
                ($tab->[$i]->[2] eq '' || $date ge $tab->[$i]->[2]) &&
                ($tab->[$i]->[3] eq '' || $date le $tab->[$i]->[3]))
            {
                return $tab->[$i]->[1];
            }
        }
        return &getGlobalValueByCode($code, $date, $default);
    }
    return $default;
}

# Function: getGlobalValueByCode
# Récupère la valeur globale de la caractéristique 
#
# Parameters:
# string $code         - Le code de la caractéristique
# string $date         - La date de valeur de la caractéristique (format date MySQL, optionnelle, date du jour par défaut)
# string $default      - Valeur par défaut
# int    $disableCache - Désactivation du cache (0 : cache non désactivé, 1 : cache désactivé). Vaut 0 par défaut
#
# Returns:
# mixed|undef - Valeur ou indéfini si il y a une erreur (valeur ou caractéristique introuvable)
sub getGlobalValueByCode
{
    my ($code, $date, $default, $disableCache) = @_;  

    # si pas de date en paramètre, date du jour
    unless ($date)
    {
        $date = &LOC::Date::getMySQLDate();
    }
    # Cache non désactivé par défaut
    unless ($disableCache)
    {
        $disableCache = 0;
    }

    if ($disableCache || !defined $tabCharacteristics)
    {
        &_getCharacs();
    }

    if (exists $tabCharacteristics->{$code})
    {
        my $tab = $tabCharacteristics->{$code}->[0];
        my $nbElements = @$tab;
        for (my $i = 0; $i < $nbElements; $i++)
        {
            if (($tab->[$i]->[2] eq '' || $date ge $tab->[$i]->[2]) &&
                ($tab->[$i]->[3] eq '' || $date le $tab->[$i]->[3]))
            {
                return $tab->[$i]->[1];
            }
        }
    }
    return $default;
}

# Function: _getAgencies
# Récupère la liste des agences et leur pays
#
# Returns:
# hashref -
sub _getAgencies
{
    # connexion à la base auth
    my $db = &LOC::Db::getConnection('auth');

    # Récupération des agences et des pays associés
    my $query = '
SELECT
     agc_id,
     agc_cty_id
FROM frmwrk.p_agency;';
    $tabAgencies = $db->fetchPairs($query);
}


# Function: _getCharacs
#
# Returns:
# void
sub _getCharacs
{
    # Réinitialisation du tableau de caractéristiques
    $tabCharacteristics = undef;

    # connexion à la base auth
    my $db = &LOC::Db::getConnection('auth');

    # Récupération des données essentielles au module
    my $query = '
SELECT
    chr_id AS `id`,
    chr_code AS `code`,
    cha_type AS `type`,
    cha_extern_id AS `extern.id`,
    cha_value AS `value`,
    cha_date_begin AS `beginDate`,
    cha_date_end AS `endDate`,
    chr_name AS `label`,
    chr_sta_id AS `state.id`
FROM
(SELECT
     cgl_chr_id AS cha_chr_id,
     0 AS cha_type,
     NULL AS cha_extern_id,
     cgl_value AS cha_value,
     cgl_date_begin AS cha_date_begin,
     cgl_date_end AS cha_date_end
 FROM p_characteristic_global
 UNION ALL
 SELECT
     ccy_chr_id AS cha_chr_id,
     1 AS cha_type,
     ccy_cty_id AS cha_extern_id,
     ccy_value AS cha_value,
     ccy_date_begin AS cha_date_begin,
     ccy_date_end AS cha_date_end
 FROM p_characteristic_country
 UNION ALL
 SELECT
     cag_chr_id AS cha_chr_id,
     2 AS cha_type,
     cag_agc_id AS cha_extern_id,
     cag_value AS cha_value,
     cag_date_begin AS cha_date_begin,
     cag_date_end AS cha_date_end
 FROM p_characteristic_agency) AS p_characteristic_all
LEFT JOIN p_characteristic
ON cha_chr_id = chr_id
ORDER BY chr_code ASC, cha_type DESC, cha_date_begin ASC, cha_date_end DESC;';
    my $stmt = $db->query($query);
    while (my $tabRow = $stmt->fetch(LOC::Db::FETCH_NUM))
    {
        my $id   = $tabRow->[0];
        my $code = $tabRow->[1];
        my $type = $tabRow->[2];
        if (!exists $tabCharacteristics->{$code})
        {
            $tabCharacteristics->{$code} = [
                [],              # Valeurs globales
                [],              # Valeurs au niveau pays
                [],              # Valeurs au niveau agence
                $tabRow->[7],    # Libellé
                $tabRow->[8],    # État
                $tabRow->[0]     # Id.
            ];
        }
        push(@{$tabCharacteristics->{$code}->[$type]}, [
            $tabRow->[3],    # Id. externe
            $tabRow->[4],    # Valeur
            $tabRow->[5],    # Date de début d'application
            $tabRow->[6]     # Date de fin d'application
        ]);
    }
}

1;