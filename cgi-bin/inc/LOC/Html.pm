use utf8;
use open (':encoding(UTF-8)');

# Class: LOC::Html
# Classe de gestion de l'affichage en HTML
package LOC::Html;


# Constants: Modes d'affichage des zones de texte
# TEXTMODE_NORMAL  - Saisie de texte normale
# TEXTMODE_ALPHA   - Saisie de texte alphanumérique
# TEXTMODE_NUMERIC - Saisie de texte numérique
# TEXTMODE_UPPER   - Saisie de texte en majuscules
# TEXTMODE_DIGIT   - Saisie de chiffres seulement
# TEXTMODE_LOWER   - Saisie de texte en minuscules
use constant
{
    TEXTMODE_NORMAL    => 0x00,
    TEXTMODE_ALPHA     => 0x00,
    TEXTMODE_NUMERIC   => 0x01,
    TEXTMODE_UPPER     => 0x02,
    TEXTMODE_DIGIT     => 0x04,
    TEXTMODE_LOWER     => 0x20
};


# Constants: Modes d'affichage des zones monétaires
# CURRENCYMODE_TEXT          - Affichage sous forme de texte simple
# CURRENCYMODE_INPUT         - Affichage sous forme de zone de texte
# CURRENCYMODE_POSITIVEINPUT - Affichage sous forme de zone de texte (force la valeur positive)
use constant
{
    CURRENCYMODE_TEXT  => 0,
    CURRENCYMODE_INPUT => 1,
    CURRENCYMODE_POSITIVEINPUT => 2
};


use strict;

use LOC::Json;

use LOC::Locale;
use LOC::Currency;
use LOC::Browser;
use LOC::Maintenance;
use JavaScript::Minifier;
use CSS::Minifier;


# Variable: $VERSION
# Version de la classe
my $VERSION = '1.00';



# Function: new
# Constructeur de la classe
#
# Parameters:
# string $localeId - Id de la locale
#
# Returns:
# LOC::Html - Instance créée
sub new
{
    my ($class, $localeId) = @_;
    if (!defined $localeId)
    {
        $localeId = LOC::Locale::DEFAULT_LOCALE_ID;
    }

    my $this = {};
    bless($this, $class);

    # Paquets JavaScript/CSS
    $this->{'_tabPackages'} = {
                        'popups' => {
                                      'css' => ['popups.css'],
                                      'js' => ['popups.js']
                                    },
                        'tabboxes' => {
                                        'css' => ['tabBoxes.css'],
                                        'js' => ['tabBoxes.js']
                                      },
                        'searchboxes' => {
                                        'css' => ['searchBoxes.css'],
                                        'js' => ['searchBoxes.js']
                                      },
                        'spincontrols' => {
                                        'css' => ['spinControls.css'],
                                        'js' => ['spinControls.js']
                                      },
                        'modalwindow' => {
                                        'css' => ['modalWindow.css'],
                                        'js' => ['modalWindow.js']
                                      },
                        'currency' => {
                                        'css' => ['currencies.css'],
                                        'js' => ['currencies.js']
                                      },
                        'notifications' => {
                                        'css' => ['notifications.css'],
                                        'js' => ['notifications.js']
                                      },
                        'dropdownbuttons' => {
                                        'css' => ['dropdownButtons.css'],
                                        'js' => ['dropdownButtons.js']
                                      }
    };

    # Liste des pays connus
    $this->{'_tabCountries'} = {
                'FR' => 'France',
                'ES' => 'Espagne',
                'PT' => 'Portugal',
                'IT' => 'Italie',
                'CH' => 'Suisse',
                'LU' => 'Luxembourg',
                'BE' => 'Belgique',
                'DE' => 'Allemagne',
                'NL' => 'Pays-Bas',
                'FI' => 'Finlande',
                'PL' => 'Pologne',
                'AD' => 'Andorre',
                'DK' => 'Danemark',
                'SK' => 'République slovaque',
                'GB' => 'Royaume-Uni',
                'AT' => 'Autriche',
                'NG' => 'Nigeria',
                'TR' => 'Turquie',
                'RO' => 'Roumanie',
                'MA' => 'Maroc'
    };

    # Id de la locale
    $this->{'_localeId'} = $localeId;
    # Titre de la page HTML
    $this->{'_pageTitle'} = '';
    # Langue de la page
    $this->{'_pageLang'} = substr($localeId, 0, 2);
    # Evenements sur la balise <body>
    $this->{'_tabBodyEvts'} = {'onkeydown' => 'if (event.keyCode == 27) return false;'};
    # Encodage de la page
    $this->{'_pageEncoding'} = 'UTF-8';
    # Astuce pour le cache JS et CSS
    $this->{'_cacheQuery'} = '?bld=' . &LOC::Globals::get('build');
    # Tableau des sources CSS
    $this->{'_tabCSSSources'} = [];
    $this->{'_tabCSSCodes'} = '';
    # Tableau des sources JS
    $this->{'_tabJSSources'} = [];
    $this->{'_tabJSCodes'} = '';
    # Instance de la locale
    $this->{'_localeObj'} = &LOC::Locale::getLocale($localeId);
    # Traductions
    $this->{'_tabTranslations'} = {};
    $this->{'_isDisplayedHeader'} = 0;
    # Codes HTML à insérer avant la fermeture de la balise BODY
    $this->{'_tabEndHtml'} = '';
    # Tableau des entêtes
    $this->{'_tabHeaders'} = [
        ['Content-Type', 'text/html; charset=' . $this->{'_pageEncoding'}],
        ['Expires', 'no-cache'],
        ['X-UA-Compatible', &LOC::Globals::get('X-UA-Compatible')]
    ];

    # Liste des metas
    $this->{'_tabMetas'} = {};
    # Liste des champs de la page
    $this->{'_tabPageInputs'} = [];


    # Type de navigateur
    $this->{'_agent'} = 'firefox';
    if (defined $ENV{'HTTP_USER_AGENT'})
    {
        if ($ENV{'HTTP_USER_AGENT'} =~ /msie (7|8|9|10)/i)
        {
            $this->{'_agent'} = 'msie' . $1;
        }
    }
    # Optimisations réseau CSS + JS
    $this->{'_networkOptimization'} = '';

    # Mode maintenance activé
    $this->{'_isMaintenanceActivated'} = &LOC::Maintenance::isActivated();

    # Styles par défaut
    $this->addCSSSrc('base.css');
    # Script JS par défaut
    $this->addJSSrc('common.js')
         ->addJSSrc('view.js')
         ->addJSSrc('httpRequest.js')
         ->addJSSrc('maskedInput.js')
         ->addJSSrc('inputDate.js')
         ->addJSSrc('calendar.js')
         ->addJSSrc('phoneFormat.js')
         ->addJSSrc('tooltips.js')
         ->addPackage('currency')
         ->addPackage('dropdownbuttons');

    # Initialisation
    $this->addBodyEvent('load', 'LOC_View.init();');

    return $this;
}


# Function: addPageInput
# Ajouter un champ à la liste des champs de la page
#
# Parameters:
# hashref  $tab - Configuration du champ
#
# Returns:
# int - Index du champ dans la liste
sub addPageInput
{
    my ($this, $tab) = @_;

    my $index = @{$this->{'_tabPageInputs'}};
    push(@{$this->{'_tabPageInputs'}}, $tab);

    return $index;
}


# Function: displayJSBlock
# Ajouter un bloc Javascript
#
# Parameters:
# string $script - Script Javascript
#
# Returns:
# string -
sub displayJSBlock
{
    my ($this, $script) = @_;
    $script =~ s/^\s+//;
    $script =~ s/\s+$//;
    return '<script type="text/javascript">//<![CDATA[' .
               "\n" . $script . "\n" .
           '//]]></script>';
}


# Function: addPackage
# Ajouter un paquet JS/CSS
#
# Parameters:
# string $name - Nom du paquet
#
# Returns:
# LOC::Html - Instance courante
sub addPackage
{
    my ($this, $name) = @_;

    $name = lc($name);
    if (defined $this->{'_tabPackages'}->{$name})
    {
        my $i;
        my @tabCSS = @{$this->{'_tabPackages'}->{$name}->{'css'}};
        for ($i = 0; $i < @tabCSS; $i++)
        {
            $this->addCSSSrc($tabCSS[$i]);
        }

        my @tabJS = @{$this->{'_tabPackages'}->{$name}->{'js'}};
        for ($i = 0; $i < @tabJS; $i++)
        {
            $this->addJSSrc($tabJS[$i]);
        }
    }
    return $this;
}


# Function: addTranslations
# Ajouter des traductions pour JavaScript
#
# Parameters:
# hashref $tabTranslations - Tableau des traductions
#
# Returns:
# LOC::Html - Instance courante
sub addTranslations
{
    my ($this, $tabTranslations) = @_;

    while (my($msgId, $msgStr) = each(%$tabTranslations))
    {
        $this->{'_tabTranslations'}->{$msgId} = $msgStr;
    }

    return $this;
}


# Function: getTranslation
# Récupérer une traduction pour JavaScript
#
# Parameters:
# string $msgId - Identifiant de la traduction
#
# Returns:
# string - La traduction
sub getTranslation
{
    my ($this, $msgId) = @_;

    return (exists $this->{'_tabTranslations'}->{$msgId} ? $this->{'_tabTranslations'}->{$msgId} : $msgId);
}


# Function: addBodyEndCode
# Ajouter du code HTML avant la fermeture de la balise BODY
#
# Parameters:
# string $html - Code HTML à insérer juste avant la fermeture de la balise body
#
# Returns:
# LOC::Html - Instance courante
sub addBodyEndCode
{
    my ($this, $html) = @_;

    $this->{'_tabEndHtml'} .= $html;

    return $this;
}


# Function: getAgent
# Récupérer le type de navigateur
#
# Parameters:
#
# Returns:
# string -
sub getAgent
{
    my ($this) = @_;

    return $this->{'_agent'};
}


# Function: startForm
# Ouvrir une balise formulaire
#
# Parameters:
# string $action         - Action
# string $enctype        - Type d'encodage du formulaire
# string $name           - Nom du formulaire
# bool   $isAutoComplete - Auto remplissage ?
#
# Returns:
# string - Code HTML
sub startForm
{
    my ($this, $action, $enctype, $name, $isAutoComplete) = @_;
    if (!defined $action)
    {
        $action = $ENV{'SCRIPT_URI'} . '?' . $ENV{'QUERY_STRING'};
    }
    if (!defined $enctype)
    {
        $enctype = 'application/x-www-form-urlencoded';
    }

    my $html = '<form method="post" action="' . $action . '" enctype="' . $enctype . '" ' .
               'autocomplete="' . ($isAutoComplete ? 'on' : 'off') . '"';
    if ($name ne '')
    {
        $html .= ' name="' . $this->toHTMLEntities($name) . '"';
    }
    $html .= '>';
    return $html;
}


# Function: endForm
# Fermer une balise formulaire
#
# Parameters:
#
# Returns:
# string - Code HTML
sub endForm
{
    return '</form>';
}


# Function: displayHeader
# Affichage de l'en-tête
#
# Parameters:
#
# Returns:
# string - Code HTML
sub displayHeader
{
    my ($this) = @_;
    my $networkOptimizationName = '';
    my $networkOptimizationMode = &LOC::Globals::get('enableNetworkOptimizations');
    if ($this->{'_networkOptimization'} && $networkOptimizationMode != 0)
    {
        $networkOptimizationName = $this->{'_networkOptimization'} . ($networkOptimizationMode == 2 ? '-min' : '');
    }

    # Titre
    my $pageTitle = $this->{'_pageTitle'};
    my $env = &LOC::Globals::getEnv();
    if ($env ne 'exp')
    {
        $pageTitle = sprintf('[%s] %s', uc($env), $pageTitle);
    }

    # Code HTML
    my $html = '';
    # Headers
    my $headers = '';
    my $nbHeaders = @{$this->{'_tabHeaders'}};
    for (my $i = 0; $i < $nbHeaders; $i++)
    {
        $headers .= $this->{'_tabHeaders'}->[$i]->[0] . ': ' . $this->{'_tabHeaders'}->[$i]->[1] . "\n";
    }
    $headers .= "\n";
    $html .= &LOC::Browser::getDisplayableHeaders($headers);

    $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" ' .
             '"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
    $html .= "\n<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"" . $this->{'_pageLang'} . "\" " .
                                                            "xml:lang=\"" . $this->{'_pageLang'} . "\">\n" .
             "<head>\n<title>" . $pageTitle . "</title>\n" .
             "<noscript><meta http-equiv=\"refresh\" content=\"0;URL=" .
                &LOC::Globals::get('locationUrl') .
                "/cgi-bin/c/noJavascript.cgi\"></noscript>\n";
    # Metas
    $html .= '<meta name="google" content="notranslate"/>' . "\n";
    foreach my $metaName (keys %{$this->{'_tabMetas'}})
    {
        my $metaContent = $this->{'_tabMetas'}->{$metaName};
        $html .= '<meta http-equiv="' . $metaName . '" content="' . $this->toHTMLEntities($metaContent) . "\"/>\n";
    }
    $html .= $this->displayJSBlock('window.Location = new Object();') . "\n";
    # Icône
    my $faviconsUrl = &LOC::Globals::get('imgUrl') . '/icons/' . ($env eq 'exp' ? '' : 'test/');
    $html .= "<link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"" .$faviconsUrl . "LOC_favicon_16x16.png" .
                                                               $this->{'_cacheQuery'} . "\">\n";
    $html .= "<link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"" .$faviconsUrl . "LOC_favicon_32x32.png" .
                                                               $this->{'_cacheQuery'} . "\">\n";
    $html .= "<link rel=\"icon\" type=\"image/png\" sizes=\"128x128\" href=\"" .$faviconsUrl . "LOC_favicon_128x128.png" .
                                                               $this->{'_cacheQuery'} . "\">\n";
    $html .= "<link rel=\"apple-touch-icon\" href=\"" .$faviconsUrl . "LOC_favicon_152x152_precomposed.png" .
                                                               $this->{'_cacheQuery'} . "\" />\n";

    # CSS EXTERNES
    if ($networkOptimizationName ne '')
    {
        my $packFile = '_combined_bld' . &LOC::Globals::get('build') . '_' . $this->{'_agent'} . '_' . $networkOptimizationName . '.css';
        my $packFilePath = &LOC::Util::getCommonFilesPath('temp/') . $packFile;
        my $packFileUrl = &LOC::Globals::get('commonFilesUrl') . '/temp/' . $packFile;

        if (!-e $packFilePath)
        {
            my $content = '';
            foreach my $cssFile (@{$this->{'_tabCSSSources'}})
            {
                my @tabFiles = $this->_getCSSFiles($cssFile, 'Path', '');
                foreach my $file (@tabFiles)
                {
                    $content .= "/* Fichier: " . $cssFile . " */\n";
                    open(MYFILESRC, $file);
                    if ($networkOptimizationMode == 2)
                    {
                        $content .= &CSS::Minifier::minify(('input' => *MYFILESRC));
                    }
                    else
                    {
                        while (<MYFILESRC>)
                        {
                            $content .= $_;
                        }
                    }
                    $content .= "\n";
                    close(MYFILESRC);
                }
            }

            my $cssPath = &LOC::Globals::get('imgUrl');
            $content =~ s/(\.\.\/){1,}img\//$cssPath\//g;

            if (!-e $packFilePath) # En cas
            {
                open(MYFILEDST, '>' . $packFilePath);
                print MYFILEDST $content;
                close(MYFILEDST);
            }
            undef $content;
        }
        $html .= '<link rel="stylesheet" type="text/css" href="' . $packFileUrl . '" />' . "\n";
    }
    else
    {
        foreach my $cssFile (@{$this->{'_tabCSSSources'}})
        {
            my @tabFiles = $this->_getCSSFiles($cssFile, 'Url', $this->{'_cacheQuery'});
            foreach my $file (@tabFiles)
            {
                $html .= '<link rel="stylesheet" type="text/css" href="' . $file . '" />' . "\n";
            }
        }
    }

    # JavaScript EXTERNES
    my $tabVars = {
        'currencyFormat'             => $this->{'_localeObj'}->getCurrencyFormatString(),
        'currencySymbol'             => $this->{'_localeObj'}->getCurrencySymbol(),
        'decimalPoint'               => $this->{'_localeObj'}->getDecimalPoint(),
        'fracDigits'                 => $this->{'_localeObj'}->getFracDigits(),
        'grouping'                   => $this->{'_localeObj'}->getMonetaryGrouping(),
        'thousandsSep'               => $this->{'_localeObj'}->getMonetaryThousandsSeparator(),
        'doesCurrencySymbolPrecedes' => $this->{'_localeObj'}->doesCurrencySymbolPrecedes(),
        'datePattern'                => $this->{'_localeObj'}->getDateFormatString(LOC::Locale::FORMAT_DATE_NUMERIC),
        'dateTimePattern'            => $this->{'_localeObj'}->getDateFormatString(LOC::Locale::FORMAT_DATETIME_NUMERIC)
    };
    my $jsCodes = '
Location.viewObj.setVars(' . &LOC::Json::toJson($tabVars) . ');';

    if ($networkOptimizationName ne '')
    {
        my $packFile = '_combined_bld' . &LOC::Globals::get('build') . '_' . $this->{'_agent'} . '_' . $networkOptimizationName . '.js';
        my $packFilePath = &LOC::Util::getCommonFilesPath('temp/') . $packFile;
        my $packFileUrl = &LOC::Globals::get('commonFilesUrl') . '/temp/' . $packFile;

        if (!-e $packFilePath)
        {
            my $content = '';
            foreach my $jsFile (@{$this->{'_tabJSSources'}})
            {
                my @tabFiles = $this->_getJSFiles($jsFile, 'Path', '');
                foreach my $file (@tabFiles)
                {
                    $content .= "/* Fichier: " . $jsFile . " */\n";
                    open(MYFILESRC, $file);
                    if ($networkOptimizationMode == 2)
                    {
                        $content .= &JavaScript::Minifier::minify(('input' => *MYFILESRC));
                    }
                    else
                    {
                        while (<MYFILESRC>)
                        {
                            $content .= $_;
                        }
                    }
                    $content .= ";\n";
                    close(MYFILESRC);
                }
            }

            if (!-e $packFilePath) # En cas
            {
                open(MYFILEDST, '>' . $packFilePath);
                print MYFILEDST $content;
                close(MYFILEDST);
            }
            undef $content;
        }
        $html .= '<script src="' . $packFileUrl . '" type="text/javascript"></script>' . "\n";
    }
    else
    {
        foreach my $jsFile (@{$this->{'_tabJSSources'}})
        {
            my @tabFiles = $this->_getJSFiles($jsFile, 'Url', $this->{'_cacheQuery'});
            foreach my $file (@tabFiles)
            {
                $html .= '<script src="' . $file . '" type="text/javascript"></script>' . "\n";
            }
        }
    }

    # CSS EN LIGNE
    if ($this->{'_tabCSSCodes'} ne '')
    {
        $html .= $this->displayCSSBlock($this->{'_tabCSSCodes'}) . "\n";
    }

    # JS EN LIGNE
    $html .= $this->displayJSBlock($jsCodes . "\n\n" . $this->{'_tabJSCodes'});

    $html .= "\n</head>\n";

    # Balise BODY
    $html .= '<body';
    foreach my $key (keys %{$this->{'_tabBodyEvts'}})
    {
        $html .= ' ' . $key  . '="' . $this->toHTMLEntities($this->{'_tabBodyEvts'}->{$key}) . '"';
    }
    $html .= ">\n";

    $this->{'_isDisplayedHeader'} = 1;

    return $html;
}


# Function: displayHelp
# Affichage d'un bouton affichant une info-bulle contenant un texte d'aide
#
# Parameters:
# string  $text        - Texte à afficher
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# string - Code HTML
sub displayHelp
{
    my ($this, $text, $tabOptions) = @_;

    return $this->displayTooltipIcon('help', $text, $tabOptions);
}


# Function: displayHelp
# Affichage d'un bouton affichant une info-bulle contenant un texte d'aide
#
# Parameters:
# string  $style      - Style de l'infobulle (help, info, clock, comment)
# string  $text       - Texte à afficher
# hashref $tabOptions - Options supplémentaires
#
# Returns:
# string - Code HTML
sub displayTooltipIcon
{
    my ($this, $style, $text, $tabOptions) = @_;

    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    my $html = '<div data-text="' . $this->toHTMLEntities($text) . '" class="tooltipIcon' . ($style ? ' ' . $style : '') . '" ' .
                  (exists $tabOptions->{'id'} ? 'id="' . $tabOptions->{'id'} . '" ' : '') .
                  'onmouseover="var e = event || window.event; LOC_View.showTooltip({x: e.clientX, y: e.clientY}, this.getAttribute(\'data-text\'), this.className.replace(\'tooltipIcon\', \'\'));" ' .
                  'onmouseout="LOC_View.hideTooltip(' . (exists $tabOptions->{'timeout'} ? $tabOptions->{'timeout'} : 300) . ');">' .
               '&nbsp;</div>';

    return $html;
}


# Function: getImageUrl
# Affichage du pied de page
#
# Parameters:
#
# Returns:
# string - Code HTML
sub displayFooter
{
    my ($this) = @_;

    my $out = '';
    if (@{$this->{'_tabPageInputs'}} > 0)
    {
        $out .= $this->displayJSBlock('Location.viewObj.setPageInputs(' . &LOC::Json::toJson($this->{'_tabPageInputs'}) . ');');
    }

    return $out . $this->displayJSBlock('Location.viewObj.setTranslations(' . &LOC::Json::toJson($this->{'_tabTranslations'}) . ');') .
           $this->{'_tabEndHtml'} .
           "\n</body>\n</html>";
}


# Function: displayFormInputFile
# Afficher un champ Fichier
#
# Parameters:
# string  $name       - Nom de l'élément
# hashref $attributes - Attributs (facultatif)
#
# Returns:
# string Code HTML -
sub displayFormInputFile
{
    my ($this, $name, $fileSizeMax, $attributes) = @_;

    if (!defined $attributes)
    {
        $attributes = {};
    }
    $attributes->{'type'} = 'file';
    $attributes->{'name'} = $name;
    if (!defined $attributes->{'id'})
    {
        $attributes->{'id'} = $name;
    }
    return $this->displayElement('input', $attributes);
}


# Function:
# Afficher un bouton image
#
# Parameters:
# string  $name      - Nom de l'élément
# string  $img       - Image
# string  $value     - Valeur
# hashref $attributs - Attributs (facultatif)
#
# Returns:
# string - Code HTML
sub displayFormInputImage
{
    my ($this, $name, $img, $value, $attributes) = @_;

    if (!defined $attributes)
    {
        $attributes = {};
    }

    if (ref($img) ne 'ARRAY')
    {
        $img = ($img =~ m/(.*)\((.*)\|(.*)\)(.*)/ ? [$1 . $2 . $4, $1 . $3 . $4] : [$img]);
    }

    $img->[0] = $this->getImageUrl($img->[0]);

    $attributes->{'src'} = $img->[0];
    if (@$img > 1)
    {
        $img->[1] = $this->getImageUrl($img->[1]);
        $attributes->{'onmouseover'} = 'LOC_View.setImageUrl(this, "' . $img->[1] . '");';
        $attributes->{'onmouseout'}  = 'LOC_View.setImageUrl(this, "' . $img->[0] . '");';
    }

    $attributes->{'type'}  = 'image';
    $attributes->{'name'}  = $name;
    $attributes->{'value'} = $value;

    return $this->displayElement('input', $attributes);
}


# Function: displayFormInputPassword
# Afficher un champ mot de passe
#
# Parameters:
# string  $name       - Nom de l'élément
# string  $value      - Valeur
# int     $mode       - Mode de saisie (facultatif)
# hashref $attributes - Attributs (facultatif)
#
# Returns:
# string - Code HTML
sub displayFormInputPassword
{
    my ($this, $name, $value, $mode, $attributes) = @_;

    if (!defined $attributes)
    {
        $attributes = {};
    }
    $attributes->{'type'}  = 'password';
    $attributes->{'name'}  = $name;
    $attributes->{'value'} = $value;
    if (!defined $attributes->{'id'})
    {
        $attributes->{'id'} = $name;
    }

    return $this->displayElement('input', $attributes);
}


# Function: displayFormInputText
# Afficher un champ texte
#
# Parameters:
# string  $name       - Nom de l'élément
# string  $value      - Valeur
# int     $mode       - Mode de saisie (facultatif)
# hashref $attributes - Attributs (facultatif)
#
# Returns:
# string - Code HTML
sub displayFormInputText
{
    my ($this, $name, $value, $mode, $attributes) = @_;

    if (!defined $attributes)
    {
        $attributes = {};
    }

    if (&LOC::Util::isNumeric($value))
    {
        my $decimalPoint = $this->{'_localeObj'}->getDecimalPoint();
        $value =~ s/$decimalPoint/\./;
    }

    $attributes->{'type'}  = 'text';
    $attributes->{'name'}  = $name;
    $attributes->{'value'} = $value;
    if ($mode == TEXTMODE_NUMERIC)
    {
        $attributes->{'onkeyup'}    = 'inputTextNumeric(event); ' . $attributes->{'onkeyup'};
        $attributes->{'onkeypress'} = 'inputTextNumeric(event); ' . $attributes->{'onkeypress'};
    }
    elsif ($mode == TEXTMODE_UPPER)
    {
        $attributes->{'onkeyup'}    = 'inputTextCase(event, "upper"); ' . $attributes->{'onkeyup'};
        $attributes->{'onkeypress'} = 'inputTextCase(event, "upper"); ' . $attributes->{'onkeypress'};
    }
    elsif ($mode == TEXTMODE_LOWER)
    {
        $attributes->{'onkeyup'}    = 'inputTextCase(event, "lower"); ' . $attributes->{'onkeyup'};
        $attributes->{'onkeypress'} = 'inputTextCase(event, "lower"); ' . $attributes->{'onkeypress'};
    }
    elsif ($mode == TEXTMODE_DIGIT)
    {
        $attributes->{'onkeyup'}    = 'inputTextDigit(event); ' . $attributes->{'onkeyup'};
        $attributes->{'onkeypress'} = 'inputTextDigit(event); ' . $attributes->{'onkeypress'};
    }
    if (!defined $attributes->{'id'})
    {
        $attributes->{'id'} = $name;
    }

    return $this->displayElement('input', $attributes);
}


# Function: displayFormInputDate
# Afficher un champ de date
#
# Parameters:
# string  $name        - Nom de l'élément
# string  $date        - Date
# int     $yearMin     - Année de début du calendrier
# int     $yearMax     - Année de fin du calendrier
# hashref $attributes  - Attributs (facultatif)
# int     $isClearable - Afficher ou non le bouton pour vider la date ?
#
# Returns:
# string Code HTML
sub displayFormInputDate
{
    my ($this, $name, $date, $yearMin, $yearMax, $attributes, $isClearable) = @_;

    my $dateFormat = $this->{'_localeObj'}->getDateFormat($date, LOC::Locale::FORMAT_DATE_NUMERIC);
    my $localDate = (defined $date ? $dateFormat : '');
    $dateFormat =~ s/\d/_/g;

    my $datePattern = $this->{'_localeObj'}->getDateFormatString(LOC::Locale::FORMAT_DATE_NUMERIC);

    my $separator = $dateFormat;
    $separator =~ s/_//g;
    $separator = substr($separator, 0, 1);

    $attributes->{'format'}         = $dateFormat;
    $attributes->{'allowed'}        = '0123456789';
    $attributes->{'separator'}      = $separator;
    $attributes->{'typeon'}         = '_';
    $attributes->{'jsFunction'}     = 'function(value) { return updateDateHidden(value, "' . $name . '", "' . $datePattern . '"); }';
    $attributes->{'displayedValue'} = $localDate;

    # Bouton d'affichage du calendrier
    my @tabLocaltime = localtime();
    my @tabDate = split(/\-/, $date);
    my $monthValue = $tabDate[1];
    my $yearValue  = $tabDate[0];

    if (!defined $yearMin)
    {
        $yearMin = 2000;
    }
    if (!defined $yearMax)
    {
        $yearMax = $tabLocaltime[5] + 1900 + 3;
    }

    if (!defined $tabDate[0])
    {
        $monthValue = $tabLocaltime[4] + 1;
        $yearValue  = $tabLocaltime[5] + 1900;
    }

    if ($yearMin == 0 || $yearValue < $yearMin)
    {
        $yearMin = $yearValue;
    }
    if ($yearMax == 0 || $yearValue > $yearMax)
    {
        $yearMax = $yearValue;
    }

    if (defined $attributes->{'readonly'} || defined $attributes->{'disabled'})
    {
        return $this->displayMaskedFormInputText($name, $date, $attributes);
    }
    else
    {
        my $calendarURL = $this->createURL('default:calendar:view',
                                           {
                                            'localeId' => $this->getLocaleId(),
                                            'name'     => $name,
                                            'year'     => $yearValue,
                                            'month'    => $monthValue,
                                            'yearMin'  => $yearMin,
                                            'yearMax'  => $yearMax,
                                            'pattern'  => $datePattern
                                           });
        my $onClick = 'Location.CalendarManager.showCalendar(event, "' . $name . '", "' . $calendarURL . '"); ' .
                      'return false;';
        my $calendarBtn = '<a href="#" onclick="' . $this->toHTMLEntities($onClick) . '" ' .
                             'id="' . $this->toHTMLEntities($name) . '.selectBtn" ' .
                             'class="locCalendarButton" ' .
                             'title="' . $this->toHTMLEntities($this->_t('Afficher le calendrier')) . '">' .
                                $this->displayImage('base/dates/btnCalendar(|Over).gif') .
                          '</a>';

        return '<div class="locDate' . ($isClearable ? ' clearable' : '') . '">' .
                    $this->displayMaskedFormInputText($name, $date, $attributes) .
                    ($isClearable ? '<button type="button" class="clear-btn" ' .
                                     'onclick="clearMaskedInputValue(\'' . $name . '\');" ' .
                                     'title="' . $this->toHTMLEntities($this->_t('Vider le champ')) . '"></button>' : '') .
                '</div>' .
                $calendarBtn;
    }
}


# Function: displayFormInputPhone
# Afficher un champ de numéro de téléphone
#
# Parameters:
# string  $name       - Nom de l'élément
# string  $phoneNo    - Numéro de téléphone
# hashref $attributes - Attributs (facultatif)
#
# Returns:
# string - Code HTML
sub displayFormInputPhone
{
    my ($this, $name, $phoneNo, $attributes) = @_;

    my $namePattern = $name . '.%s';
    if ($name =~ /(.+)(\[.+\])$/)
    {
        $namePattern = $1 . '.%s' . $2;
    }
    my $tabIndex = 0;
    if (defined $attributes->{'tabindex'} && $attributes->{'tabindex'} > 0)
    {
        $tabIndex = $attributes->{'tabindex'};
        $attributes->{'tabindex'}++;
    }

    # Récupération des différentes composantes du numéro de téléphone
    my %phoneNoFormat = $this->{_localeObj}->getPhoneNumberFormat($phoneNo);

    # js sur l'action coller
    my $jsOnPaste = 'return onPastePhone(event, this, $ge("' . $name . '.code").value, "' . $phoneNoFormat{'localCode'} . '", $ge("' . $name . '").value); ';

    # Mise à jour du champ caché lors de l'appui sur une touche du champ préfixe
    $attributes->{'jsFunction'} = 'function(value) { return updatePhoneHidden(value, "' . $name . '"); }';
    my $codeOnKeyUp = 'updateMaskedInputHidden("' . $name . '", "_", ' . $attributes->{'jsFunction'} . ');';

    # Zone de saisie du numéro de téléphone
    my $phoneObj = '';
    if ($phoneNoFormat{'code'} eq '' || $phoneNoFormat{'code'} ne $phoneNoFormat{'localCode'})
    {
        delete($attributes->{'beforeValue'});

        $attributes->{'_format'}   = $phoneNoFormat{'format'};
        $attributes->{'class'}     = 'locStandardPhoneNumber' . ($phoneNoFormat{'code'} eq '' ? ' error' : '');
        $attributes->{'size'}      = length($phoneNoFormat{'format'});
        $attributes->{'onfocus'}  .= 'var el = this; window.setTimeout(function () { el.select(); }, 50);';
        $attributes->{'phoneCode'} = $phoneNoFormat{'code'};

        $attributes->{'onpaste'} = $jsOnPaste;
        $attributes->{'onkeyup'} = $attributes->{'onkeyup'} . $codeOnKeyUp;

        $phoneObj  = $this->displayFormInputText(
                                                 sprintf($namePattern, 'masked'),
                                                 $phoneNoFormat{'no'},
                                                 0,
                                                 $attributes
                                                );
        $phoneObj .= $this->displayFormInputHidden($name, $phoneNoFormat{'fullNo'});
    }
    else
    {
        $attributes->{'format'}         = $phoneNoFormat{'format'};
        $attributes->{'allowed'}        = '0123456789';
        $attributes->{'separator'}      = '+() ';
        $attributes->{'typeon'}         = '_';
        $attributes->{'class'}          = 'locStandardPhoneNumber';
        $attributes->{'displayedValue'} = $phoneNoFormat{'no'};

        $attributes->{'onpaste'} = $jsOnPaste;

        $phoneObj = $this->displayMaskedFormInputText($name, $phoneNoFormat{'fullNo'}, $attributes);
    }

    # Zone de saisie du préfixe international
    my $codeObj = '';
    # Javascript de vérification
    my $js = 'checkIntlCode("' . $name . '", "' .
                                 $phoneNoFormat{'localCode'} . '", "' .
                                 $phoneNoFormat{'format'} . '");';

    # Attributs
    my $codeObjAttr = {
                       'size'      => 3,
                       'maxlength' => 3,
                       'class'     => $attributes->{'class'},
                       'onchange'  => $js . $codeOnKeyUp,
                       'onkeyup'   => $codeOnKeyUp,
                      };
    if ($tabIndex > 0)
    {
        $codeObjAttr->{'tabindex'} = $tabIndex;
    }
    $codeObj = '+' . $this->displayFormInputText(
                                                 sprintf($namePattern, 'code'),
                                                 $phoneNoFormat{'code'},
                                                 TEXTMODE_DIGIT,
                                                 $codeObjAttr
                                                );

    return $codeObj . ' ' . $phoneObj;
}


# Function: displayMaskedFormInputText
# Afficher un champ texte avec masque de saisie
#
# Parameters:
# string  $name       - Nom de l'élément
# string  $value      - Valeur
# hashref $attributes - Attributs (facultatif)
#
# Returns:
# string - Code HTML
sub displayMaskedFormInputText
{
    my ($this, $name, $value, $attributes) = @_;
    if (!defined $attributes->{'typeon'})
    {
        $attributes->{'typeon'} = '_';
    }
    if (defined $attributes->{'format'} && !defined $attributes->{'size'})
    {
        $attributes->{'size'} = length($attributes->{'format'});
        $attributes->{'maxlength'} = $attributes->{'size'};
    }

    my $namePattern = $name . '.%s';
    if ($name =~ /(.+)(\[.+\])$/)
    {
        $namePattern = $1 . '.%s' . $2;
    }

    my $js = 'MaskedInput({elm: $ge("' . sprintf($namePattern, 'masked') . '")';
    if (defined $attributes->{'format'})
    {
        $js .= ', format: "' . $attributes->{'format'} . '"';
    }
    if (defined $attributes->{'allowed'})
    {
        $js .= ', allowed: "' . $attributes->{'allowed'} . '"';
    }
    if (defined $attributes->{'separator'})
    {
        $js .= ', separator: "' . $attributes->{'separator'} . '"';
    }
    if (defined $attributes->{'typeon'})
    {
        $js .= ', typeon: "' . $attributes->{'typeon'} . '"';
    }
    $js .= '});';

    # Désactivation du copier/coller
    if (!defined $attributes->{'onpaste'})
    {
        $js .= '$ge("' . sprintf($namePattern, 'masked') . '").onpaste = function() { return false; };';
    }

    # Mise à jour du champ caché
    my $replace = &LOC::Util::escapeRegex($attributes->{'typeon'});
    if (!defined $attributes->{'jsFunction'})
    {
        $attributes->{'jsFunction'} = 'null';
    }
    my $onkeyup = 'updateMaskedInputHidden("' . $name . '", "' .
                                                $replace . '", ' .
                                                $attributes->{'jsFunction'} . ');';
    $attributes->{'onkeyup'} = $onkeyup . $attributes->{'onkeyup'};
    if (defined $attributes->{'onpaste'})
    {
        $attributes->{'onchange'} = $attributes->{'onkeyup'};
    }

    # Sélection du texte à l'entrée dans la zone
    $attributes->{'onfocus'} .= 'var el = this; window.setTimeout(function () { el.select(); }, 50);';

    my $displayedValue = (defined $attributes->{'displayedValue'} ? $attributes->{'displayedValue'} : $value);

    $attributes->{'_format'} = $attributes->{'format'};
    delete($attributes->{'format'});
    delete($attributes->{'allowed'});
    delete($attributes->{'separator'});
    delete($attributes->{'typeon'});
    delete($attributes->{'displayedValue'});
    delete($attributes->{'jsFunction'});

    my $html = $this->displayFormInputText(sprintf($namePattern, 'masked'), $displayedValue, '', $attributes);
    $html .= $this->displayFormInputHidden($name, $value);
    $html .= $this->displayJSBlock($js);

    return $html;
}


# Function: displayFormInputCurrency
# Afficher un champ monétaire
#
# Parameters:
# string  $name           - Nom de l'élément
# string  $value          - Valeur
# int     $mode           - Mode d'affichage du widget (texte non modifiable ou zone de saisie)
# string  $currencySymbol - Symbole monétaire
# int     $precision      - Nombre de décimales à afficher
# hashref $attributes     - Attributs (facultatif)
#
# Returns:
# string - Code HTML
sub displayFormInputCurrency
{
    my ($this, $name, $value, $mode, $currencySymbol, $precision, $attributes) = @_;

    if (!defined $currencySymbol)
    {
        $currencySymbol = LOC::Locale::DEFAULT_CURRENCY_SYMBOL;
    }
    if (!defined $precision)
    {
        $precision = $this->{'_localeObj'}->getFracDigits();
    }

    my $decimalPoint = $this->{'_localeObj'}->getDecimalPoint();
    $value =~ s/$decimalPoint/\./;

#    my $doesSymbolPrecedes = $this->{'_localeObj'}->doesCurrencySymbolPrecedes();

    my $hasTooltip = ($currencySymbol ne LOC::Locale::DEFAULT_CURRENCY_SYMBOL);
    my $rate;
    if ($hasTooltip)
    {
        my $currencyId = &LOC::Currency::getBySymbol($currencySymbol);
        if ($currencyId ne '0')
        {
            my $currencyInfos = &LOC::Currency::getInfos($currencyId);
            $rate = $currencyInfos->{'euroRate'};
            $rate =~ s/$decimalPoint/\./;
        }
    }

    my $display;
    if ($mode == CURRENCYMODE_TEXT)
    {
        # Champ caché contenant la valeur
        $display = $this->displayFormInputHidden($name, $value);
        my $js;
        if ($hasTooltip)
        {
            $js = 'Location.currenciesManager.createCurrency("%s", ' . $mode . ', "%s", "' . $rate . '", %d);';
        }
        else
        {
            $js = 'Location.currenciesManager.createCurrency("%s", ' . $mode . ', "%s", null, %d);';
        }
        $display .= $this->displayJSBlock(sprintf($js, $name, $currencySymbol, $precision));
    }
    else
    {
#        if (!$doesSymbolPrecedes)
#        {
            $attributes->{'style'} .= 'text-align: right;';
#        }
        if (!defined $attributes->{'size'})
        {
            $attributes->{'size'} = 12;
        }

        # Champ texte contenant la valeur non formatée
        $display = $this->displayFormInputText($name, $value, '', $attributes);
        my $js;
        if ($hasTooltip)
        {
            $js = 'Location.currenciesManager.createCurrency("%s", ' . $mode . ', "%s", "' . $rate . '", %d);';
        }
        else
        {
            $js = 'Location.currenciesManager.createCurrency("%s", ' . $mode . ', "%s", null, %d);';
        }
        $display .= $this->displayJSBlock(sprintf($js, $name, $currencySymbol, $precision));
    }

    return $display;
}


# Function: displayFormInputHidden
# Ajouter un hidden au formulaire
#
# Parameters:
# string  $name       - Nom de l'élément
# string  $value      - Valeur
# hashref $attributes - Attributs (facultatif)
#
# Returns:
# string - Code HTML
sub displayFormInputHidden
{
    my ($this, $name, $value, $mode, $attributes) = @_;

    if (!defined $attributes)
    {
        $attributes = {};
    }
    if (&LOC::Util::isNumeric($value))
    {
        my $decimalPoint = $this->{'_localeObj'}->getDecimalPoint();
        $value =~ s/$decimalPoint/\./;
    }
    $attributes->{'type'}  = 'hidden';
    $attributes->{'name'}  = $name;
    $attributes->{'value'} = $value;
    if (!defined $attributes->{'id'})
    {
        $attributes->{'id'} = $name;
    }

    return $this->displayElement('input', $attributes);
}


# Function: displayFormInputButton
# Afficher un bouton
#
# Parameters:
# string  $name       - Nom de l'élément
# string  $value      - Valeur
# hashref $attributes - Attributs (facultatif)
#
# Returns:
# string Code HTML
sub displayFormInputButton
{
    my ($this, $name, $value, $attributes) = @_;

    if (!defined $attributes)
    {
        $attributes = {};
    }
    $attributes->{'type'}  = 'button';
    $attributes->{'name'}  = $name;
    $attributes->{'value'} = $value;

    return $this->displayElement('input', $attributes);
}


# Function: displayFormInputCheckBox
# Afficher une checkBox
#
# Parameters:
# string $name              - nom de la checkbox
# int isChecked             - checkbox cochée (0:false, 1:true)
# hashReference $attributes - référence de hashage d'attributs supplémentaires
# string $label             - le label de la coche
# int $value                - la valeur de la coche
# int $generateHidden       - (0:ne pas generer, 1:generer)
sub displayFormInputCheckBox
{
    my ($this, $name, $isChecked, $attributes, $label, $value, $generateHidden) = @_;

    if (!defined $attributes)
    {
        $attributes = {};
    }
    if (!defined $value)
    {
        $value = 1;
    }

    $attributes->{'type'}  = 'checkbox';
    $attributes->{'name'}  = $name;
    $attributes->{'value'} = $value;

    if (!defined $attributes->{'id'} || $attributes->{'id'} eq '')
    {
        $attributes->{'id'} = $attributes->{'name'} . '[' . $attributes->{'value'} . ']';
    }

    if (defined $isChecked && $isChecked == 1)
    {
        $attributes->{'checked'} = 'checked';
    }

    my $html = '';

    my $isDisabled = 0;
    if (exists $attributes->{'disabled'})
    {
        if ($attributes->{'disabled'})
        {
            $isDisabled = 1;
            $attributes->{'disabled'} = 'disabled';
        }
        else
        {
            delete $attributes->{'disabled'};
        }
    }

    if ($generateHidden)
    {
        $html .= '<input type="hidden" ' . ($isDisabled ? 'disabled="disabled" ' : '') .
                 'name="' . $this->toHTMLEntities($name) . '" value="0"/>';
    }

    $html .= $this->displayElement('input', $attributes);

    if ($attributes->{'id'} ne '' && $label ne '')
    {
        $html .= '<label for="' . $attributes->{'id'} . '"' .
                 ($isDisabled ? '' : ' onmouseover="this.style.color=&quot;#007AC2&quot;;" ' .
                                      'onmouseout="this.style.color=&quot;&quot;;"') . '>' .
                     $label .
                 '</label>';
    }

    return $html;
}


# Function: displayFormInputCheckBoxGroup
# Afficher un groupe de checkBox
#
# Parameters:
# string $name              - Nom
# arrayReference $elements  - Tableau des éléments
# arrayReference $checkeds  - Elements cochés
# string $separator         - Separateur
# hashReference $attributes - Attributs
# int $generateHidden       - Activer/Désactiver l'ajout du hidden après l'input checkbox
#
# Returns:
# string - Code HTML
sub displayFormInputCheckBoxGroup
{
    my ($this, $name, $elements, $checkeds, $separator, $attributes, $generateHidden) = @_;

    my @tabCheckBoxes;
    my %eltAttributes;
    my $element;
    my $arrayIndice = 0;

    foreach $element (@$elements)
    {
        %eltAttributes = %$attributes;
        $eltAttributes{'id'} = $name . '[' . $arrayIndice . ']';

        if (!defined $eltAttributes{'label'})
        {
            $eltAttributes{'label'} = $element
        }

        push(@tabCheckBoxes, $this->displayFormInputCheckBox(
                                                        $name,
                                                        (&LOC::Util::in_array($arrayIndice, $checkeds)),
                                                        \%eltAttributes,
                                                        $eltAttributes{'label'},
                                                        $arrayIndice,
                                                        $generateHidden));

        $arrayIndice++;

        if (defined $attributes->{'tabindex'})
        {
            $attributes->{'tabindex'} = $attributes->{'tabindex'}++;
        }
    }

    return join($separator, @tabCheckBoxes);
}

# Function: displayFormInputRadio
# Afficher une Radio
#
# Parameters:
# string $name              - Nom
# string $value             - Valeur
# bool $isChecked            - Coché
# hashReference $attributes - Attributs
# string $label      - Libellé
#
# Returns:
# string - Code HTML
sub displayFormInputRadio
{
    my ($this, $name, $value, $isChecked, $attributes, $label) = @_;

    if (!defined $attributes)
    {
        $attributes = {};
    }

    $attributes->{'type'}  = 'radio';
    $attributes->{'name'}  = $name;
    $attributes->{'value'} = $value;

    if (!defined $attributes->{'id'} || $attributes->{'id'} eq '')
    {
        $attributes->{'id'} = $attributes->{'name'} . '[' . $attributes->{'value'} . ']';
    }

    if (defined $isChecked && $isChecked == 1)
    {
        $attributes->{'checked'} = 'checked';
    }

    $attributes->{'label'} = $label;

    my $isDisabled = (defined $attributes->{'disabled'} && $attributes->{'disabled'} ne '');

    my $html = $this->displayElement('input', $attributes);

    if ($attributes->{'id'} ne '' && $attributes->{'label'} ne '')
    {
        $html .= '<label for="' . $attributes->{'id'} . '"' .
                ($isDisabled ? '' : ' onmouseover="this.style.color=&quot;#007AC2&quot;;" ' .
                                     'onmouseout="this.style.color=&quot;&quot;;"') . '>' .
                    $attributes->{'label'} .
                '</label>';
    }
    return $html;
}

# Function: displayFormInputRadioGroup
# Afficher un groupe de radioButtons
#
# Parameters:
# string $name              - Nom
# arrayReference $elements  - Tableau des éléments
# int $value                - Valeur
# string $separator         - Separateur
# hashReference $attributes - Attributs
#
# Returns:
# string - Code HTML
sub displayFormInputRadioGroup
{
    my ($this, $name, $elements, $value, $separator, $attributes) = @_;

    my @tabRadios;
    my %eltAttributes;
    my $element;
    my $arrayIndice = 0;

    foreach $element (@$elements)
    {
        %eltAttributes = %$attributes;
        $eltAttributes{'id'} = $name . '[' . $arrayIndice . ']';

        if (!defined $eltAttributes{'label'})
        {
            $eltAttributes{'label'} = $element
        }

        push(@tabRadios, $this->displayFormInputRadio($name,
                                                      $arrayIndice,
                                                      ($arrayIndice == $value),
                                                      \%eltAttributes,
                                                      $eltAttributes{'label'}));
        $arrayIndice++;
        if (defined $attributes->{'tabindex'})
        {
            $attributes->{'tabindex'} = $attributes->{'tabindex'}++;
        }
    }

    return join($separator, @tabRadios);
}

# Fucntion: displayFormInputSubmit
# Afficher un bouton de soumission
#
# Parameters:
# string  $name       - Nom de l'élément
# string  $value      - Valeur
# hashref $attributes - Attributs (facultatif)
#
# Returns:
# string - Code HTML
sub displayFormInputSubmit
{
    my ($this, $name, $value, $attributes) = @_;

    if (!defined $attributes)
    {
        $attributes = {};
    }
    $attributes->{'type'}  = 'submit';
    $attributes->{'name'}  = $name;
    $attributes->{'value'} = $value;

    return $this->displayElement('input', $attributes);
}


# Function: displayFormTextArea
# Afficher un textArea
#
# Parameters:
# string  $name       - Nom de l'élément
# string  $value      - Valeur
# hashref $attributes - Attributs
#
# Returns:
# string - Code HTML
sub displayFormTextArea
{
    my ($this, $name, $value, $attributes) = @_;

    if (!defined $attributes)
    {
        $attributes = {};
    }
    $attributes->{'name'} = $name;
    if (!defined $attributes->{'id'})
    {
        $attributes->{'id'} = $name;
    }
    if (exists $attributes->{'maxlength'})
    {
        # Implémentation du maxlength
        $value = substr($value, 0, $attributes->{'maxlength'});
        $attributes->{'onkeydown'}  = 'if (!LOC_View.limitTextArea(event)) {return false};' .
                                      (exists $attributes->{'onkeydown'} ? $attributes->{'onkeydown'} : '');
        $attributes->{'onchange'} = 'LOC_View.limitTextArea(event);' .
                                    (exists $attributes->{'onchange'} ? $attributes->{'onchange'} : '');
    }

    return $this->displayElement('textarea', $attributes, $value);
}


# Function: displayFormSelect
# Afficher une liste
#
# Parameters:
# string           $name            - Nom la liste
# hashref|arrayref $tabElements     - Liste des éléments de la liste
# arrayref|string  $selectedIndexes - Liste des éléments séléctionnés
# bool             $isMultiple      - Indique si la liste permet les valeurs multiples
# array            $attributes      - Les attributs de l'objet avec les valeurs associées
#
# Returns:
# string - Code HTML
sub displayFormSelect
{
    my ($this, $name, $tabElements, $selectedIndexes, $isMultiple, $attributes) = @_;
    if (!defined $tabElements)
    {
        $tabElements = {};
    }
    if (!defined $selectedIndexes)
    {
        $selectedIndexes = [];
    }
    if (!defined $isMultiple)
    {
        $isMultiple = 0;
    }
    if (!defined $attributes)
    {
        $attributes = {};
    }

    $attributes->{'name'} = $name;
    if (!defined $attributes->{'id'} || $attributes->{'id'} eq '')
    {
        $attributes->{'id'} = $name;
    }
    if ($isMultiple)
    {
        $attributes->{'multiple'} = 'multiple';
        if (&LOC::Util::isNumeric($isMultiple) && $isMultiple > 1)
        {
            $attributes->{'size'} = $isMultiple;
        }
    }
    elsif (exists $attributes->{'multiple'})
    {
        delete $attributes->{'multiple'};
    }

    if (ref($selectedIndexes) ne 'ARRAY')
    {
        $selectedIndexes = [$selectedIndexes];
    }

    my $innerHTML = '';
    if (ref($tabElements) eq 'HASH')
    {
        my @tabOptions = ();
        my @tabGroups = ();
        my $n = 0;
        foreach my $value (keys %$tabElements)
        {
            my $label = $tabElements->{$value};
            if (ref($label) eq 'HASH')
            {
                $tabGroups[$n] = $value;
                foreach my $value2 (keys %$label)
                {
                    push(@tabOptions, [$value2, $label->{$value2}, &LOC::Util::in_array($value2, $selectedIndexes), $n]);
                }
                $n++;
            }
            else
            {
                push(@tabOptions, [$value, $label, &LOC::Util::in_array($value, $selectedIndexes), -1]);
            }
        }

        # Eléments
        my $oldGroupId = -1;
        foreach my $option (@tabOptions)
        {
            my $value = $option->[0];
            my $label = $option->[1];
            $label =~ s/\t/&nbsp;&nbsp;&nbsp;&nbsp;/;
            my $isSelected = $option->[2];
            my $groupId = $option->[3];
            # Gestion des groupes
            if ($oldGroupId != $groupId)
            {
                if ($oldGroupId != -1)
                {
                    $innerHTML .= "\n\t</optgroup>";
                }
                $innerHTML .= "\n\t<optgroup label=\"" .
                                $this->toHTMLEntities($tabGroups[$groupId]) .
                              '">';
                $oldGroupId = $groupId;
            }

            $innerHTML .= "\n\t";
            $innerHTML .= '<option value="' . $this->toHTMLEntities($value) . '"' .
                          ($isSelected ? ' selected="selected"' : '') .
                          '>' . $this->toHTMLEntities($label) . '</option>';
        }
        $innerHTML .= "\n";
    }
    elsif (ref($tabElements) eq 'ARRAY')
    {
        foreach my $option (@$tabElements)
        {
            my %attribs = %$option;
            my $label = $attribs{'innerHTML'};
            delete $attribs{'innerHTML'};
            if (&LOC::Util::in_array($attribs{'value'}, $selectedIndexes))
            {
                $attribs{'selected'} = 'selected';
            }
            elsif (exists $attribs{'selected'})
            {
                delete $attribs{'selected'};
            }

            $innerHTML .= $this->displayElement('option', \%attribs, $label);
        }
    }

    return $this->displayElement('select', $attributes, $innerHTML);
}


# Function: displayImage
# Afficher une image
#
# Parameters:
# string  $img        - Image
# hashref $attributes - Attributs
#
# Returns:
# string -
sub displayImage
{
    my ($this, $img, $attributes) = @_;
    if (!defined $attributes)
    {
        $attributes = {};
    }
    if (!ref($attributes))
    {
        $attributes = {'title' => $attributes};
    }

    if (ref($img) ne 'ARRAY')
    {
        $img = ($img =~ m/(.*)\((.*)\|(.*)\)(.*)/ ? [$1 . $2 . $4, $1 . $3 . $4] : [$img]);
    }

    $img->[0] = $this->getImageUrl($img->[0]);

    $attributes->{'src'} = $img->[0];
    if (@$img > 1)
    {
        $img->[1] = $this->getImageUrl($img->[1]);
        $attributes->{'onmouseover'} = 'LOC_View.setImageUrl(this, "' . $img->[1] . '");';
        $attributes->{'onmouseout'}  = 'LOC_View.setImageUrl(this, "' . $img->[0] . '");';
    }

    return $this->displayElement('img', $attributes);
}


# Function: displayControlPanel
# Afficher la barre de contrôles
#
# Parameters:
# hashref $tabConfigs - Configurations
#
# string - Code HTML
sub displayControlPanel
{
    my ($this, $tabConfigs) = @_;
    if (!defined $tabConfigs)
    {
        $tabConfigs = {};
    }

    my $subStyle  = (defined $tabConfigs->{'substyle'} ? ' ' . $tabConfigs->{'substyle'} : '');
    my $separator = (defined $tabConfigs->{'separator'} ? $tabConfigs->{'separator'} : '&nbsp;&nbsp;&nbsp;&nbsp;');


    my $out = '
<div class="locControlPanel' . $subStyle . '">
    <div class="c_tl"></div><div class="c_tr"></div><div class="c_bl"></div><div class="c_br"></div>
    <div class="b_t"></div><div class="b_b"></div><div class="b_l"></div><div class="b_r"></div>';

    $out .= '
    <table>
    <tbody>
        <tr>';

    # Eléments à gauche
    if (defined $tabConfigs->{'left'})
    {
        if (ref($tabConfigs->{'left'}) ne 'ARRAY')
        {
            $tabConfigs->{'left'} = [$tabConfigs->{'left'}];
        }

        $out .= '
            <td class="left">' . join($separator, @{$tabConfigs->{'left'}}) . '</td>';
    }

    # Eléments du milieu
    if (defined $tabConfigs->{'middle'})
    {
        if (ref($tabConfigs->{'middle'}) ne 'ARRAY')
        {
            $tabConfigs->{'middle'} = [$tabConfigs->{'middle'}];
        }

        $out .= '
            <td class="middle">' . join($separator, @{$tabConfigs->{'middle'}}) . '</td>';
    }

    # Eléments à droite
    if (defined $tabConfigs->{'right'})
    {
        if (ref($tabConfigs->{'right'}) ne 'ARRAY')
        {
            $tabConfigs->{'right'} = [$tabConfigs->{'right'}];
        }

        $out .= '
            <td class="right">' . join($separator, @{$tabConfigs->{'right'}}) . '</td>';
    }
    $out .= '
        </tr>
    </tbody>
    </table>';

    $out .= '
</div>';

    return $out;
}

# Function: displayMessages
# Afficher les messages
#
# Parameters:
# string   $type        - Type de message (error, help, info, valid, warning)
# arrayref $tabMessages - Liste des messages
# bool     $useLocale   - Utilisation de la locale (optionnel) si égal à 1
#
# Returns:
# string - Code HTML
sub displayMessages
{
    my ($this, $type, $tabMessages, $useLocale) = @_;

    if (!defined $useLocale)
    {
        $useLocale = 1;
    }

    my $out = '';
    if (@$tabMessages > 0)
    {
        $out .= '<div class="locMessage">' . $this->displayImage('base/messages/' . $type . '.png') . "\n";
        $out .= '<div class="' . $type . '">' . "\n";
        for (my $i = 0; $i < @$tabMessages; $i++)
        {
            if ($useLocale == 1)
            {
                $out .= $this->_t($tabMessages->[$i]) . "\n";
            }
            else
            {
                $out .= $tabMessages->[$i] . "\n";
            }
            $out .= '<br />' . "\n";
        }
        $out .= '</div>' . "\n";
        $out .= '</div>' . "\n";
    }

    return $out;
}


# Function: displayPrintButtons
# Affichage des boutons d'impression, génération de PDF, envoi par fax et par e-mail
#
# Parameters:
# string   $id               - Identifiant du groupe de boutons
# string   $url              - URL du fichier à imprimer (sous la forme module:contrôleur:action)
# string   $filename         - Nom du fichier à imprimer
# string   $defaultRecipient - Adresse e-mail par défaut dans la zone "À"
# string   $defaultSubject   - Objet par défaut pour l'envoi par e-mail
# string   $defaultContent   - Contenu par défaut de l'e-mail
# string   $documentAgencyId - Identifiant de l'agence du document imprimé
# arrayref $tabParams        - Paramètres additionnels :
#                                 - 'buttons', liste des boutons à afficher ("print" : impression, "pdf" : affichage en PDF,
#                                     "mail" : envoi par e-mail)
#                                 - 'tabReplacesTpl', données pour le template personnalisé
#                                 - 'defaultCopyRecipient', adresse e-mail par défaut dans la zone "Cc"
#                                 - 'tabEndActions', actions à effectuer après l'impression/pdf/mail
#                                 - 'tabAdditionalAttachments', fichiers supplémentaires à joindre au mail (url ou path, filename, extension (facult.)
#
# Returns:
# string - Code HTML
sub displayPrintButtons
{
    my ($this, $id, $url, $filename, $defaultRecipient, $defaultSubject, $defaultContent, $documentAgencyId, $tabParams) = @_;

    if (!defined $tabParams->{'buttons'})
    {
        $tabParams->{'buttons'} = ['print', 'pdf', 'mail'];
    }
    if (!defined $tabParams->{'tabReplacesTpl'})
    {
        $tabParams->{'tabReplacesTpl'} = {};
    }
    if (!defined $tabParams->{'tabEndActions'})
    {
        $tabParams->{'tabEndActions'} = [];
    }
    if (!defined $tabParams->{'tabAdditionalAttachments'})
    {
        $tabParams->{'tabAdditionalAttachments'} = [];
    }

    my $html = '';
    my $js   = '';

    # Vérification si c'est une URL externe ou une URL interne à l'application
    my $expr = LOC::Request::REGEXP_MCA_URL;
    my $realUrl = (ref($url) eq 'HASH' ? $url->{'url'} : $url);
    if ($realUrl =~ m/$expr/)
    {
        $realUrl = $this->createURL('print:render:view', {'url' => $realUrl, %{$url->{'options'}}});
    }

    my $formatedId = $this->toHTMLEntities($id);
    $html .= '<div id="' . $formatedId . '" class="printButtons">';
    if (exists $tabParams->{'label'} && $tabParams->{'label'} ne '')
    {
        $html .= '<span class="label">' . $this->toHTMLEntities($tabParams->{'label'}) . '&nbsp;:</span>';
    }

    # Bouton d'impression
    if (grep(/print/, @{$tabParams->{'buttons'}}))
    {
        # Déclaration de la popup des paramètres d'impression
        $js .= 'var printPopup = Location.popupsManager.createPopup("printPopup", "", "center", "center", 450, 185);';

        my $printUrl = '#';
        if ($realUrl ne '')
        {
            $printUrl = $this->createURL('print:gui:print', {
                                             'url'           => $realUrl,
                                             'tabEndActions' => $tabParams->{'tabEndActions'}
                                         });
        }

        # Location.modalWindowManager.show(this.href, {contentType: 1, width: \'460px\', height: \'195px\'});
        $html .= '<a class="print" href="' . $printUrl . '" id="' . $formatedId . '.print" title="' . $this->toHTMLEntities($this->_t('Imprimer')) . '" onclick="var url = this.getAttribute(\'href\'); if (url != \'#\') { printPopup.open(url); } return false;"></a>';
    }

    # Bouton d'édition de PDF
    if (grep(/pdf/, @{$tabParams->{'buttons'}}))
    {
        my $pdfUrl = '#';
        if ($realUrl ne '')
        {
            $pdfUrl = $this->createURL('print:gui:pdf', {
                                             'url'           => $realUrl,
                                             'tabEndActions' => $tabParams->{'tabEndActions'}
                                         });
        }

        $html .= '<a class="pdf" href="' . $pdfUrl . '" id="' . $formatedId . '.pdf" title="' . $this->toHTMLEntities($this->_t('Ouvrir en PDF')) . '" target="_blank" onclick="return (this.getAttribute(\'href\') != \'#\');"></a>';
    }

    # Bouton d'envoi par e-mail
    if (grep(/mail/, @{$tabParams->{'buttons'}}))
    {
        # Déclaration de la popup des paramètres d'envoi d'e-mail
        $js .= 'var emailPopup = Location.popupsManager.createPopup("emailPopup", "", "center", "center", 500, 375);';

        my $mailUrl = $this->createURL('print:gui:email', {
                                            'url'                  => $realUrl,
                                            'filename'             => $filename,
                                            'documentAgency.id'    => $documentAgencyId,
                                            'defaultRecipient'     => $defaultRecipient,
                                            'defaultCopyRecipient' => $tabParams->{'defaultCopyRecipient'},
                                            'defaultSubject'       => $defaultSubject,
                                            'defaultContent'       => $defaultContent,
                                            'tabReplacesTpl'       => $tabParams->{'tabReplacesTpl'},
                                            'tabEndActions'        => $tabParams->{'tabEndActions'},
                                            'tabAdditionalAttachments' => $tabParams->{'tabAdditionalAttachments'}
                                        });

        $html .= '<a class="mail" href="' . $mailUrl . '" id="' . $formatedId . '.mail" title="' . $this->toHTMLEntities($this->_t('Envoyer par e-mail')) . '" onclick="var url = this.getAttribute(\'href\'); if (url != \'#\') { emailPopup.open(url); } return false;"></a>';
    }

    $html .= '</div>';

    # Ajout du script JS si il y en a un
    if ($js ne '')
    {
        $html .= $this->displayJSBlock($js);
    }

    return $html;
}


# Function: displayTextButton
# Afficher un bouton avec du texte sur sa droite ou sa gauche
#
# Parameters:
# string          $text         - Texte
# string|arrayref $img          - Image(s) choisie(s)
# string          $action       - Adresse URL ou code JavaScript à éxécuter sur l'évenement onClick
# string          $title        - Description (apparaît en infobulle)
# string          $iconPosition - Position de l'image ('left': Gauche, 'right': Droite)
# hashref         $attributes   - Attributs (facultatif)
#
# Returns:
# string - Code HTML
sub displayTextButton
{
    my ($this, $text, $img, $action, $title, $iconPosition, $attributes) = @_;
    if (!defined $title)
    {
        $title = '';
    }
    if (!defined $iconPosition || $iconPosition eq '')
    {
        $iconPosition = 'left';
    }
    if (!defined $attributes)
    {
        $attributes = {};
    }

    if (ref($img) ne 'ARRAY')
    {
        $img = ($img =~ m/(.*)\((.*)\|(.*)\)(.*)/ ? [$1 . $2 . $4, $1 . $3 . $4] : [$img]);
    }

    # Info-bulle
    if (!exists $attributes->{'title'} && $title ne '')
    {
        $attributes->{'title'} = $title;
    }
    if (!exists $attributes->{'class'})
    {
        $attributes->{'class'} = 'locCtrlButton';
    }

    if (@$img > 1)
    {
        $attributes->{'onmouseover'} = 'if (!Location.commonObj.hasEltClass(this, "disabled")) ' .
                                       '{ LOC_View.setImageUrl(this, \'' . $this->getImageUrl($img->[1]) . '\'); }';
        $attributes->{'onmouseout'}  = 'LOC_View.setImageUrl(this, \'' . $this->getImageUrl($img->[0]) . '\');';
        $attributes->{'data-imgshover'} = '1';
    }

    # Action du du type 'module:controller:action' ?
    my $expr = LOC::Request::REGEXP_MCA_URL;
    if ($action =~ m/$expr/)
    {
        $action = $this->createURL($action);
    }

    # Action javascript ou location ?
    $expr = LOC::Request::REGEXP_HTTP_URL;
    if ($action =~ m/$expr/)
    {
        $attributes->{'href'} = $action;
    }
    else
    {
        $attributes->{'href'} = '#';
        $attributes->{'onclick'} = $action . '; return false;';
    }

    my $imgHtml = '';
    if ($img->[0] ne '')
    {
        $imgHtml = $this->displayImage($img->[0]);
    }
    my $innerHTML;
    if ($text ne '')
    {
        if (lc(substr($iconPosition, 0, 1)) eq 'l')
        {
            if ($imgHtml ne '')
            {
                $innerHTML = $imgHtml . '&nbsp;';
            }
            $innerHTML .= '<span>' . &LOC::Util::trim($text) . '</span>';
        }
        else
        {
            $innerHTML = '<span>' . &LOC::Util::trim($text) . '</span>';
            if ($imgHtml ne '')
            {
                $innerHTML .= '&nbsp;' . $imgHtml;
            }
        }
    }
    else
    {
        $innerHTML = $imgHtml;
    }

    return $this->displayElement('a', $attributes, $innerHTML);
}


# Function: displayDropdownButton
# Affiche un bouton déroulant vers le bas.
#
# Parameters:
# string  $name        - Le nom.
# string  $tabElements - La liste des éléments à afficher.
#                        Chaque élement peut être :
#                        - une chaîne de caractères qui sera affichée en l'état ;
#                        - un tableau associatif pouvant contenir les attributs suivants :
#                          * icon : le code HTML de l'icône ;
#                          * label : le code HTML du libellé ;
#                          * action : l'action (URL, code JavaScript ou action interne) à exécuter lorsqu'on clique
#                            sur l'élément dans le menu.
# string $menuPosition - L'alignement du menu par rapport au bouton :
#                        - 'left' pour qu'il soit aligné à gauche,
#                        - 'right' pour qu'il soit aligné à droite.
# hashref $attributes  - Les attributs (facultatif).
#
# Returns:
# string - Code HTML
sub displayDropdownButton
{
    my ($this, $name, $tabElements, $menuPosition, $attributes) = (@_);

    return $this->displayMenuButton($name, $tabElements, $menuPosition, 'down', $attributes);
}


# Function: displayDropupButton
# Affiche un bouton déroulant vers le haut.
#
# Parameters:
# string  $name        - Le nom.
# string  $tabElements - La liste des éléments à afficher.
#                        Chaque élement peut être :
#                        - une chaîne de caractères qui sera affichée en l'état ;
#                        - un tableau associatif pouvant contenir les attributs suivants :
#                          * icon : le code HTML de l'icône ;
#                          * label : le code HTML du libellé ;
#                          * action : l'action (URL, code JavaScript ou action interne) à exécuter lorsqu'on clique
#                            sur l'élément dans le menu.
# string $menuPosition - L'alignement du menu par rapport au bouton :
#                        - 'left' pour qu'il soit aligné à gauche,
#                        - 'right' pour qu'il soit aligné à droite.
# hashref $attributes  - Les attributs (facultatif).
#
# Returns:
# string - Code HTML
sub displayDropupButton
{
    my ($this, $name, $tabElements, $menuPosition, $attributes) = (@_);

    return $this->displayMenuButton($name, $tabElements, $menuPosition, 'up', $attributes);
}


# Function: displayMenuButton
# Affiche un bouton déroulant.
#
# Parameters:
# string  $name        - Le nom.
# string  $tabElements - La liste des éléments à afficher.
#                        Chaque élement peut être :
#                        - une chaîne de caractères qui sera affichée en l'état ;
#                        - un tableau associatif pouvant contenir les attributs suivants :
#                          * icon : le code HTML de l'icône ;
#                          * label : le code HTML du libellé ;
#                          * action : l'action (URL, code JavaScript ou action interne) à exécuter lorsqu'on clique
#                            sur l'élément dans le menu.
# string $menuPosition - L'alignement du menu par rapport au bouton :
#                        - 'left' pour qu'il soit aligné à gauche ;
#                        - 'right' pour qu'il soit aligné à droite.
# string $orientation  - L'orientation du menu :
#                        - 'up' pour que le menu se déroule vers le haut ;
#                        - 'down' pour que le menu se déroule vers le bas.
# hashref $attributes  - Les attributs (facultatif).
#
# Returns:
# string - Code HTML
sub displayMenuButton
{
    my ($this, $name, $tabElements, $menuPosition, $orientation, $attributes) = (@_);

    if ($menuPosition eq 'r')
    {
        $menuPosition = 'right';
    }
    if ($menuPosition ne 'right')
    {
        $menuPosition = 'left';
    }

    if ($orientation eq 'd')
    {
        $orientation = 'down';
    }
    if ($orientation ne 'down')
    {
        $orientation = 'up';
    }

    $attributes->{'name'} = $name;
    if (!defined $attributes->{'id'})
    {
        $attributes->{'id'} = $name;
    }
    $attributes->{'class'} = 'dropdown ' . $menuPosition . ' ' . $orientation;
    if ($attributes->{'disabled'})
    {
        $attributes->{'class'} .= ' disabled';
        undef $attributes->{'disabled'};
    }

    if (ref($tabElements) ne 'ARRAY')
    {
        $tabElements = [$tabElements];
    }


    # Pas d'éléments, rien à faire
    my $nbElements = @$tabElements;
    if ($nbElements == 0)
    {
        return '';
    }


    # Fonction d'affichage d'un élément du dropdown button
    my $display = sub
    {
        my ($element) = @_;

        # Icône + text
        if (ref($element) eq 'HASH')
        {
            my $attributes = {
                'class' => 'dropdown-item-container'
            };

            my $text = '
                <div class="dropdown-item-icon">' . $element->{'icon'} . '</div>
                <div class="dropdown-item-label">' . $element->{'label'} . '</div>';

            return $this->displayTextButton($text, undef, $element->{'action'}, undef, undef, $attributes);
        }
        # Texte tout seul
        else
        {
            return $element;
        }
    };


    # Construction du contenu du bouton déroulant
    my $html = '';

    # Premier élément
    $html .= '
    <div class="dropdown-button">
        ' . &$display($tabElements->[0]) . '
    </div>';


    if ($nbElements > 1)
    {
        # Bouton d'affichage du menu
        $html .= '
        <div class="dropdown-toggle-split"></div>';

        # Menu
        $html .= '
        <div class="dropdown-menu">';

        for (my $i = 1; $i < $nbElements; $i++)
        {
            $html .= '
            <div class="dropdown-item">
                ' . &$display($tabElements->[$i]) . '
            </div>';
        }

        $html .= '
        </div>';
    }


    return $this->displayElement('div', $attributes, $html);
}


# Function: displaySearchBar
# Afficher une barre de recherche
#
# Parameters:
# string   $id         - Identifiant
# arrayref $tabLines   - Lignes de critères
# bool     $tabOptions - Options supplémentaires ('isExpanded', 'onexpand', 'oncollapse', 'onsearch', 'onclear')
# Returns:
# string - Code HTML
sub displaySearchBar
{
    my ($this, $id, $tabLines, $tabOptions) = @_;

    my $nbLines = @$tabLines;

    my $expandAction = ($nbLines > 1 ? 'if (Location.commonObj.toggleEltClass(\'' . $id . '\', \'expand\') == 1)' .
                                       ' {' . $tabOptions->{'onexpand'} . '} else {' . $tabOptions->{'oncollapse'} . '} ' : '');

    my $html = '
<table id="' . $id . '" class="locSearchBar' . ($nbLines > 1 ? ' more' : '') . ($tabOptions->{'isExpanded'} ? ' expand' : '') . '">
<tbody>
    <tr>
        <td class="title"><a href="#" onclick="' . $expandAction . 'return false;">' . $this->toHTMLEntities($this->_t('Recherche')) . '</a></td>
        <td class="filters">';

    my $lineIndex = 0;
    foreach my $tabElts (@$tabLines)
    {
        $html .= '
            <div class="' . ($lineIndex == 0 ? 'base' : 'more') . '">';

        my $eltIndex = 0;
        foreach my $tabEltInfos (@$tabElts)
        {
            my $help = ($tabEltInfos->{'help'} ? $this->displayHelp($tabEltInfos->{'help'}) . '&nbsp;' : '');
            my $title = ($tabEltInfos->{'description'} ? ' title="' . $this->toHTMLEntities($tabEltInfos->{'description'}) . '"' : '');

            $html .= '
                    <div class="filter">' .
                        '<div class="name"' . $title . '>' . $tabEltInfos->{'name'} . '</div>' .
                        '<div class="input">' . $help . $tabEltInfos->{'input'} . '</div>' .
                   '</div>';

            $eltIndex++;
        }
        $html .= '
            </div>';
        $lineIndex++;
    }

    $html .= '
        </td>
        <td class="controls">';

    if ($tabOptions->{'onsearch'})
    {
        my $jsAction = $this->toHTMLEntities($tabOptions->{'onsearch'});
        $html .= '
            <a class="button go" href="#" onclick="'. $jsAction . '; return false;" title="' . $this->toHTMLEntities($this->_t('Rechercher')) . '" tabindex="-1" onfocus="this.blur();"></a>';
    }
    if ($tabOptions->{'onclear'})
    {
        my $jsAction = $this->toHTMLEntities($tabOptions->{'onclear'});
        $html .= '
            <a class="button clear" href="#" onclick="' . $jsAction . '; return false;" title="' . $this->toHTMLEntities($this->_t('Effacer la recherche')) . '" tabindex="-1" onfocus="this.blur();"></a>';
    }

    $html .= '
        </td>
    </tr>
</tbody>
</table>';

    return $html;
}

# Function: displayLabel
# Afficher le libellé d'un champ de formulaire
#
# Parameters:
# string  $for        - Identifiant du champ de formulaire
# hashref $label      - Libellé à afficher
# hashref $attributes - Autres attributs
#
# Returns:
# string - Code HTML
sub displayLabel
{
    my ($this, $for, $label, $attributes) = @_;

    if (defined $for)
    {
        $attributes->{'for'} = $for;
        return $this->displayElement('label', $attributes, $label);
    }

    return $label;
}


# Function: displayElement
# Afficher un élément HTML
#
# Parameters:
# string  $tagName    - Type d'élément HTML
# hashref $attributes - Attributs associés
# string  $innerHTML  - Contenu
#
# Returns:
# string - Code HTML
sub displayElement
{
    my ($this, $tagName, $attributes, $innerHTML) = @_;

    my $html = '<' . $tagName;
    while (my($attr, $value) = each(%$attributes))
    {
        $value =~ s/^\s+//;
        $value =~ s/\s+$//;
        if ($attr =~ m/^(readonly|disabled|checked)$/)
        {
            if ($value ne '' && $value ne '0')
            {
                $html .= ' ' . $attr . '="' . $attr . '"';
            }
        }
        else
        {
            $html .= ' ' . $attr . '="' . $this->toHTMLEntities($value) . '"';
        }
    }
    $html .= (!defined $innerHTML ? '/>' : '>' . $innerHTML . '</' . $tagName . '>' );
    return $html;
}


# Function: displayBoolean
# Affichage d'un booléen sous forme d'une image accompagnée d'un libellé
#
# Parameters:
# bool   $bool  - Booléen à afficher
# string $label - Libellé complémentaire
#
# Returns:
# string - Code HTML
sub displayBoolean
{
    my ($this, $bool, $label) = @_;
    if (!defined $label)
    {
        $label = (defined $bool && $bool == 0 ? $this->_t('Non') : ($bool == 1 ? $this->_t('Oui') : $this->_t('Non renseigné')));
    }
    my $class = (defined $bool && $bool == 0 ? 'no' : ($bool == 1 ? 'yes' : 'unknown'));

    return '<div class="locBoolean '  . $class . '">' . $label . '</div>';
}


# Function: displayCountryImage
# Afficher l'image d'un pays
#
# Parameters:
# string  $country     - Pays
# hashref $attributes  - Attributs de l'image
# bool    $isViewLabel - Afficher le libellé ?
#
# Returns:
# string - Code HTML
sub displayCountryImage
{
    my ($this, $country, $attributes, $isViewLabel) = @_;
    if (!defined $attributes)
    {
        $attributes = {};
    }

    my $imgFile = 'base/country/' . lc($country) . '.gif';
    my $fileName = &LOC::Globals::get('imgPath') . '/' . $imgFile;
    my $imgKey = uc($country);
    if (defined $this->{'_tabCountries'}->{$imgKey})
    {
        $attributes->{'title'} = $this->_t($this->{'_tabCountries'}->{$imgKey});
        return $this->displayImage($imgFile, $attributes) .
               ($isViewLabel ? '&nbsp;' . $attributes->{'title'} : '');
    }
    return '';
}


# Function: displayCSSBlock
# Ajouter un block de styles CSS
#
# Parameters:
# string $styles - Styles CSS
#
# Returns:
# string -
sub displayCSSBlock
{
    my ($this, $styles) = @_;
    $styles =~ s/^\s+//;
    $styles =~ s/\s+$//;
    return '<style type="text/css">' .
               "\n" . $styles . "\n" .
           '</style>';
}


# Function: displayFileExtension
# Afficher l'image correspondant à l'extension d'un fichier
#
# Parameters:
# string  $extension     - extension
#
# Returns:
# string - Code HTML
sub displayFileExtension
{
    my ($this, $id, $extension) = @_;

    if (!defined $extension)
    {
        $extension = '';
    }

    my $attributes = {
        'class' => 'fileExtension ' . $extension,
        'title' => $extension
    };

    if (defined $id)
    {
        $attributes->{'id'} = $id;
    }

    return $this->displayElement('div', $attributes, '');
}


# Function: displayRequiredSymbol
# Afficher le symbole indiquant un champ obligatoire
#
# Parameters:
#
# Returns:
# string - Code HTML
sub displayRequiredSymbol
{
    my ($this) = @_;

    my $attributes = {'title' => $this->_t('Champ requis'), 'style' => 'vertical-align: middle;'};

    my $symbol = "\n";
    $symbol .= $this->displayImage('required.png', $attributes);

    return $symbol;
}


# Function: getImageUrl
# Récupérer le nom du fichier complet d'une image
#
# Parameters:
# string $img - Image
#
# Returns:
# string -
sub getImageUrl()
{
    my ($this, $img) = @_;

    $img = &LOC::Util::trim($img);
    my $expr = LOC::Request::REGEXP_HTTP_URL;
    if ($img eq '')
    {
        return '';
    }
    elsif ($img =~ /$expr/)
    {
        return $img;
    }
    else
    {
        return &LOC::Globals::get('imgUrl') . '/' . $img;
    }
}


# Function: setPageTitle
# Définir le titre de la page
#
# Parameters:
# string $title - Titre
#
# Returns:
# LOC::Html - Instance courante
sub setPageTitle
{
    my ($this, $pageTitle) = @_;

    $this->{'_pageTitle'} = $pageTitle;
    return $this;
}


# Function: setHeader
# Définir un en-tête HTTP
#
# Parameters:
# string $name       - Nom
# string $value      - Valeur
# bool   $isReplaced - Indique si cet en-tête doit remplacer les en-têtes du même type
#
# Returns:
# LOC::Html - Instance courante
sub setHeader
{
    my ($this, $name, $value, $isReplaced) = @_;

    # Normalise le nom de l'entête
    $name =~ s/[\s_]/-/g;

    # Remplacement de l'entête si nécessaire
    if ($isReplaced)
    {
        my $tabHeaders = [];
        my $nbHeaders = @{$this->{'_tabHeaders'}};
        for (my $i = 0; $i < $nbHeaders; $i++)
        {
            if ($this->{'_tabHeaders'}->[$i]->[0] ne $name)
            {
                push(@$tabHeaders, [$this->{'_tabHeaders'}->[$i]->[0], $this->{'_tabHeaders'}->[$i]->[1]]);
            }
        }
        delete $this->{'_tabHeaders'};
        $this->{'_tabHeaders'} = $tabHeaders;
    }

    push(@{$this->{'_tabHeaders'}}, [$name, $value]);
    return $this;
}


# Function: addBodyEvent
# Ajouter des evenements au BODY
#
# Parameters:
# string $event  - Événement
# string $action - Action JS
#
# Returns:
# LOC::Html - Instance courante
sub addBodyEvent
{
    my ($this, $event, $action) = @_;

    if (substr($event, 0, 2) ne 'on')
    {
        $event = 'on' . $event;
    }

    if (exists $this->{'_tabBodyEvts'}->{$event})
    {
        $this->{'_tabBodyEvts'}->{$event} .= $action;
    }
    else
    {
        # Rajout du ; en fin de chaîne si oubli
        $this->{'_tabBodyEvts'}->{$event} = $action . (substr($action, -1) eq ';' ? '' : ';');
    }
    return $this;
}


# Function: addCSSSrc
# Ajouter un fichier CSS
#
# Parameters:
# string $cssFile    - Nom du fichier
#
# Returns:
# LOC::Html - Instance courante
sub addCSSSrc
{
    my ($this, $cssFile) = @_;

    if ($this->{'_isDisplayedHeader'} == 1)
    {
        # Entête déjà affichée
        if (substr($cssFile, -4) eq '.css')
        {
            # Inclusion du/des fichiers CSS
            my @tabFiles = $this->_getCSSFiles($cssFile, 'Url', $this->{'_cacheQuery'});
            foreach my $href (@tabFiles)
            {
                print '<link rel="stylesheet" type="text/css" href="' . $href . '" />' . "\n";
            }
        }
        else
        {
            print $this->displayCSSBlock($cssFile);
        }
    }
    else
    {
        # Entête non affichée
        if (substr($cssFile, -4) eq '.css')
        {
            if (!&LOC::Util::in_array($cssFile, $this->{'_tabCSSSources'}))
            {
                push(@{$this->{'_tabCSSSources'}}, $cssFile);
            }
        }
        else
        {
            $this->{'_tabCSSCodes'} .= "\n" . $cssFile . "\n";
        }
    }

    return $this;
}


# Function: addJSSrc
# Ajouter un fichier JS
#
# Parameters:
# string $jsFile     - Nom du fichier
#
# Returns:
# LOC::Html - Instance courante
sub addJSSrc
{
    my ($this, $jsFile) = @_;

    if ($this->{'_isDisplayedHeader'} == 1)
    {
        # Entête déjà affichée
        if (substr($jsFile, -3) eq '.js')
        {
            # Inclusion du/des fichiers JavaScript
            my @tabFiles = $this->_getJSFiles($jsFile, 'Url', $this->{'_cacheQuery'});
            foreach my $src (@tabFiles)
            {
                print '<script src="' . $src . '" type="text/javascript">';
            }
        }
        else
        {
            print $this->displayJSBlock($jsFile);
        }
    }
    else
    {
        # Entête non affichée
        if (substr($jsFile, -3) eq '.js')
        {
            if (!&LOC::Util::in_array($jsFile, $this->{'_tabJSSources'}))
            {
                push(@{$this->{'_tabJSSources'}}, $jsFile);
            }
        }
        else
        {
            $this->{'_tabJSCodes'} .= "\n" . $jsFile . "\n";
        }
    }
    return $this;
}


# Function: addMeta
# Ajouter un meta
#
# Parameters:
# string $name    - Nom
# string $content - Contenu
#
# Returns:
# LOC::Html - Instance courante
sub addMeta
{
    my ($this, $name, $content) = @_;

    $this->{'_tabMetas'}->{$name} = $content;
    return $this;
}

# Function: createURL
# Générer une adresse URL
#
# Parameters:
# string  $file      - Fichier
# hashref $tabParams - Les paramétres à passer
# bool    $isCrypted - Crypté ou pas ?
#
# Returns:
# string -
sub createURL
{
    my ($this, $file, $tabParams, $isCrypted) = @_;
    if (!defined $tabParams)
    {
        $tabParams = ();
    }
    if (!defined $isCrypted)
    {
        $isCrypted = 1;
    }
    return &LOC::Request::createRequestUri($file, $tabParams, $isCrypted);
}


# Function: toHTMLEntities
# Convertit tous les caractères éligibles en entités HTML
#
# Parameters:
# string $str - Chaîne
#
# Returns:
# string -
sub toHTMLEntities
{
    my ($this, $str) = @_;

    return &LOC::Util::toHTMLEntities($str);
}

# Function: toJSEntities
# Convertir tous les caractères éligibles en entités JavaScript
#
# Parameters:
# string $str - Chaîne
#
# Returns:
# string -
sub toJSEntities
{
    my ($this, $str) = @_;

    return &LOC::Util::toJSEntities($str);
}


# Function: getLocaleId
# Récupérer la locale courante
#
# Returns:
# string -
sub getLocaleId
{
    my ($this) = @_;

    return $this->{'_localeObj'}->getId();
}


# Function: htmllocale
# Traduit une chaîne et remplace les caractères spéciaux en entités HTML.
#
# Parameters:
# array - Les mêmes paramètres que la fonction &LOC::Locale::t().
#
# Returns:
# string - Une chaîne de caractères traduite et avec des entités HTML.
sub htmllocale
{
    my $this = shift;

    return $this->toHTMLEntities($this->{'_localeObj'}->t(@_));
}


# Function: isMaintenanceActivated
# Inique si le mode maintenance est activé.
#
# Returns:
# bool - 1 si le mode maintenance est activé, 0 sinon.
sub isMaintenanceActivated
{
    my ($this) = @_;

    return $this->{'_isMaintenanceActivated'};
}


# Function: _getCSSFiles
# Récupération de la liste des chemins des fichiers CSS correspondant à un nom de fichier
#
# Parameters:
# string $cssFile    - Nom du fichier
# string $type       - Type du chemin (Path ou Url)
# string $cacheQuery - Nom du fichier
#
# Returns:
# array - Liste des chemins de fichier CSS
sub _getCSSFiles
{
    my ($this, $cssFile, $type, $cacheQuery) = @_;

    my @tabFiles;
    if (-e &LOC::Globals::get('cssPath') . '/' . $cssFile)
    {
        push(@tabFiles, &LOC::Globals::get('css' . $type) . '/' . $cssFile . $cacheQuery);
    }
    if (-e &LOC::Globals::get('cssPath') . '/' . $this->{'_agent'} . '/' . $cssFile)
    {
        push(@tabFiles, &LOC::Globals::get('css' . $type) . '/' . $this->{'_agent'} . '/' . $cssFile . $cacheQuery);
    }
    return @tabFiles;
}


# Function: _getJSFiles
# Récupération de la liste des chemins des fichiers JS correspondant à un nom de fichier
#
# Parameters:
# string $jsFile     - Nom du fichier
# string $type       - Type du chemin (Path ou Url)
# string $cacheQuery - Nom du fichier
#
# Returns:
# array - Liste des chemins de fichier JS
sub _getJSFiles
{
    my ($this, $jsFile, $type, $cacheQuery) = @_;

    my @tabFiles;
    if (-e &LOC::Globals::get('jsPath') . '/' . $jsFile)
    {
        push(@tabFiles, &LOC::Globals::get('js' . $type) . '/' . $jsFile . $cacheQuery);
    }
    if (-e &LOC::Globals::get('jsPath') . '/' . $this->{'_agent'} . '/' . $jsFile)
    {
        push(@tabFiles, &LOC::Globals::get('js' . $type) . '/' . $this->{'_agent'} . '/' . $jsFile . $cacheQuery);
    }
    return @tabFiles;
}


# Function: _t
# Récupération d'une traduction avec la locale courante
#
# Parameters:
# string $str     - Libellé à traduire
# array  @tabArgs - Arguments de remplacement dans le libellé (même principe que printf)
#
# Returns:
# string - Libellé traduit
sub _t
{
    my ($this, $str, @tabArgs) = @_;

    return $this->{'_localeObj'}->t($str, @tabArgs);
}
# Function: _tn
# Récupération d'une traduction avec gestion du pluriel avec la locale courante
#
# Parameters:
# string $str     - Libellé à traduire
# array  @tabArgs - Arguments de remplacement dans le libellé (même principe que printf)
#
# Returns:
# string - Libellé traduit
sub _tn
{
    my ($this, $str, @tabArgs) = @_;

    return $this->{'_localeObj'}->tn($str, @tabArgs);
}

1;