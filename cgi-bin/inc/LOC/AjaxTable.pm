use utf8;
use open (':encoding(UTF-8)');

# Class: LOC::AjaxTable
# Module de fonctions utilitaires pour la gestion des tableaux Ajax (voir le plugin jQuery AjaxTable)
package LOC::AjaxTable;
use strict;



# Modules internes
use LOC::Globals;
use LOC::Db;



# Function: getData
# Récupérer des donnés d'une table en base de données (+gestion des filtres)
#
# Parameters:
# LOC::Db::Adapter $db                  - Instance de connexion à la base de données
# string           $tableName           - Nom de la table
# arrayref         $tabJoinsCfgs        - Jointures
# hashref          $tabColumnsCfgs      - Colonnes
# hashref          $tabFiltersCfgs      - Filtres
# arrayref         $tabQueryFirstWheres - Conditions permanentes
# hashref          $tabActivesFilters   - Filtres actifs
# arrayref         $tabSorts            - Tris
# int              $startIndex          - Index de la ligne de départ à récupérer
# int              $pageSize            - Nombre de lignes à récupérer
# hashref          $tabUserInfos        - Informations sur l'utilisateur
# hashref          $tabOptions          - Options supplémentaires
#
# Returns:
# hashref - Données du tableau et des filtres
sub getData
{
    my ($db, $tableName, $tabJoinsCfgs, $tabColumnsCfgs, $tabFiltersCfgs, $tabQueryFirstWheres, $tabActivesFilters,
        $tabSorts, $startIndex, $pageSize, $tabOptions) = @_;

    # Sous routine permettant de générer la clause where d'un filtre
    my $genFilterWhere = sub {
        my ($filterId) = @_;

        my $tabFltCfgs = $tabFiltersCfgs->{$filterId};
        my $tabValues  = $tabActivesFilters->{$filterId};

        my $where;
        if (ref($tabFltCfgs->{'where'}) eq 'CODE')
        {
            $where = &{$tabFltCfgs->{'where'}}($tabValues);
        }
        else
        {
            my $values = '';
            my $operator = '';
            if ($tabFltCfgs->{'options'} && $tabFltCfgs->{'options'}->{'operator'})
            {
                $tabValues->[0] =~ /(.*?):(.*)/;
                $operator = $1;
                $values = $2;
            }
            else
            {
                $values = $db->quote($tabValues);
            }

            $where = $tabFltCfgs->{'where'};
            $where =~ s/\<\%op\>/$operator/g;
            $where =~ s/\<\%values\>/$values/g;
        }

        return $where;
    };



    # Tableau contenant le résultat
    my %tabData = (
        'result' => 'ko'
    );

    # Calcul des champs (SELECT)
    my @tabQueryFields = ();
    foreach my $columnId (keys %$tabColumnsCfgs)
    {
        my $select = $tabColumnsCfgs->{$columnId}->{'select'};
        if (defined $tabColumnsCfgs->{$columnId}->{'table'})
        {
            $select = $tabColumnsCfgs->{$columnId}->{'table'} . '.' . $select;
        }
        push(@tabQueryFields, $select . ' AS ' . $db->quoteIdentifier($columnId));
    }

    # Calcul des filtres
    my @tabQueryWheres = @$tabQueryFirstWheres;
    foreach my $activeFilterId (keys %$tabActivesFilters)
    {
        if (exists $tabFiltersCfgs->{$activeFilterId} && @{$tabActivesFilters->{$activeFilterId}} > 0)
        {
            push(@tabQueryWheres, '(' . &$genFilterWhere($activeFilterId) . ')');
        }
    }

    # Calcul des jointures
    my @tabQueryJoins  = ();
    foreach my $joinInfos (@$tabJoinsCfgs)
    {
        my $alias = ($joinInfos->{'alias'} ? $joinInfos->{'alias'} : '');
        push(@tabQueryJoins, $joinInfos->{'type'} . ' JOIN ' . $joinInfos->{'table'} . ' ' . $alias . ' ON ' . $joinInfos->{'on'});
    }

    # Calcul des colonnes de tris (ORDER BY)
    my @tabQueryOrdersBy = ();
    foreach my $sort (@$tabSorts)
    {
        if (exists $tabColumnsCfgs->{$sort->{'col'}})
        {
            my $col = $tabColumnsCfgs->{$sort->{'col'}};
            my $dir = uc($sort->{'dir'});

            if (exists $col->{'orderBy'})
            {
                my $sortStr = $col->{'orderBy'};
                $sortStr =~ s/\<\%dir\>/$dir/g;
                push(@tabQueryOrdersBy, $sortStr);
            }
            else
            {
                my $select = $db->quoteIdentifier($col->{'select'});
                if (defined $col->{'table'})
                {
                    $select = $db->quoteIdentifier($col->{'table'}) . '.' . $select;
                }
                push(@tabQueryOrdersBy, $select . ' ' . $dir);
            }
        }
    }


    # Exécution de la requête
    my $tabQueryResult = &_execQuery($db, $tableName, \@tabQueryJoins,
                                     \@tabQueryFields, \@tabQueryWheres, \@tabQueryOrdersBy,
                                     $startIndex, $pageSize);

    # Récupération du dernier contrat de chaque machine
    if ($tabQueryResult)
    {
        # Si une limite au niveau du nombre de lignes est définie et qu'elle est dépassée, alors on retourne une erreur
        if ($tabOptions->{'rowsLimit'} && ($tabQueryResult->{'count'} > $tabOptions->{'rowsLimit'}))
        {
            return \%tabData;
        }

        # Récupération des données
        $tabData{'rowsCount'}      = $tabQueryResult->{'count'} * 1;
        $tabData{'totalRowsCount'} = $tabQueryResult->{'totalCount'} * 1;
        $tabData{'rows'}           = $tabQueryResult->{'results'};
    }



    # ***********************************************************************************************************
    # Récupération des filtres
    # ***********************************************************************************************************
    if (!exists $tabOptions->{'getFiltersData'} || $tabOptions->{'getFiltersData'})
    {
        $tabData{'filters'} = {};

        # Récupération des valeurs des filtres
        foreach my $filterId (keys %$tabFiltersCfgs)
        {
            my $tabCfgs = $tabFiltersCfgs->{$filterId};

            $tabData{'filters'}->{$filterId} = {};

            # Dans le cas du type "search", on veut les valeurs seulement si il est activé
            if ($tabCfgs->{'type'} eq 'list')
            {
                my @tabQueryOrdersBy = ();
                # Calcul des champs (SELECT)
                my @tabQueryFields = ();
                push(@tabQueryFields, 'DISTINCT ' . $tabCfgs->{'select'}->{'value'} . ' AS ' . $db->quoteIdentifier('value'));
                if ($tabCfgs->{'select'}->{'label'})
                {
                    push(@tabQueryOrdersBy, $db->quoteIdentifier('label'));
                    push(@tabQueryFields, $tabCfgs->{'select'}->{'label'} . ' AS ' . $db->quoteIdentifier('label'));
                }
                else
                {
                    push(@tabQueryOrdersBy, $db->quoteIdentifier('value'));
                }
                if ($tabCfgs->{'select'}->{'extra'})
                {
                    push(@tabQueryFields, $tabCfgs->{'select'}->{'extra'});
                }

                # Calcul des jointures
                my @tabQueryJoins  = ();
                foreach my $joinInfos (@$tabJoinsCfgs)
                {
                    my $alias = ($joinInfos->{'alias'} ? $joinInfos->{'alias'} : '');
                    push(@tabQueryJoins, $joinInfos->{'type'} . ' JOIN ' . $joinInfos->{'table'} . ' ' . $alias . ' ON ' . $joinInfos->{'on'});
                }

                # Conditions permanentes
                my @tabQueryWheres = @$tabQueryFirstWheres;


                # On ne calcule pas la liste si c'est un filtre caché
                if (!$tabCfgs->{'hidden'})
                {
                    # Calcul des filtres
                    foreach my $activeFilterId (keys %$tabActivesFilters)
                    {
                        # Les valeurs d'un filtre ne sont pas filtrées par lui même
                        if (exists $tabFiltersCfgs->{$activeFilterId} &&
                            @{$tabActivesFilters->{$activeFilterId}} > 0 &&
                            $activeFilterId ne $filterId)
                        {
                            push(@tabQueryWheres, '(' . &$genFilterWhere($activeFilterId) . ')');
                        }
                    }

                    # Exécution de la requête
                    my $tabQueryResult = &_execQuery($db, $tableName, \@tabQueryJoins,
                                                     \@tabQueryFields, \@tabQueryWheres, \@tabQueryOrdersBy,
                                                     0, 0);

                    # Ajout du filtre dans les données renvoyées
                    my $tabRows = $tabQueryResult->{'results'};
                    if ($tabCfgs->{'func'})
                    {
                        $tabRows = &{$tabCfgs->{'func'}}($tabRows, $tabCfgs, undef);
                    }
                    $tabData{'filters'}->{$filterId}->{'list'} = $tabRows;
                }


                # Si le filtre est activé
                if ($tabActivesFilters->{$filterId} && @{$tabActivesFilters->{$filterId}} > 0)
                {
                    my $tabRows = [];
                    if (!exists $tabCfgs->{'values'})
                    {
                        # Conditions permanentes
                        my @tabQueryWheres2 = @$tabQueryFirstWheres;

                        push(@tabQueryWheres2, '(' . &$genFilterWhere($filterId) . ')');

                        # Exécution de la requête
                        my $tabQueryResult = &_execQuery($db, $tableName, \@tabQueryJoins,
                                                          \@tabQueryFields, \@tabQueryWheres2, \@tabQueryOrdersBy,
                                                          0, 0);
                        $tabRows = $tabQueryResult->{'results'};
                    }
                    elsif ($tabCfgs->{'values'}->{'table'})
                    {
                        my @tabQueryOrdersBy2 = ();
                        # Calcul des champs (SELECT)
                        my @tabQueryFields2 = ();
                        push(@tabQueryFields2, $tabCfgs->{'values'}->{'select'}->{'value'} . ' AS ' . $db->quoteIdentifier('value'));
                        if ($tabCfgs->{'values'}->{'select'}->{'label'})
                        {
                            push(@tabQueryOrdersBy2, $db->quoteIdentifier('label'));
                            push(@tabQueryFields2, $tabCfgs->{'values'}->{'select'}->{'label'} . ' AS ' . $db->quoteIdentifier('label'));
                        }
                        else
                        {
                            push(@tabQueryOrdersBy2, $db->quoteIdentifier('value'));
                        }
                        if ($tabCfgs->{'values'}->{'select'}->{'extra'})
                        {
                            push(@tabQueryFields2, $tabCfgs->{'values'}->{'select'}->{'extra'});
                        }

                        # Calcul des jointures
                        my @tabQueryJoins2  = ();
                        if ($tabCfgs->{'values'}->{'select'}->{'joins'})
                        {
                            foreach my $joinInfos (@{$tabCfgs->{'values'}->{'select'}->{'joins'}})
                            {
                                my $alias = ($joinInfos->{'alias'} ? $joinInfos->{'alias'} : '');
                                push(@tabQueryJoins2, $joinInfos->{'type'} . ' JOIN ' . $joinInfos->{'table'} . ' ' . $alias . ' ON ' . $joinInfos->{'on'});
                            }
                        }

                        my $values = $db->quote($tabActivesFilters->{$filterId});
                        my $where = $tabCfgs->{'values'}->{'where'};
                        $where =~ s/\<\%values\>/$values/g;

                        my @tabQueryWheres2 = ('(' . $where . ')');

                        # Exécution de la requête
                        my $tabQueryResult = &_execQuery($db, $tabCfgs->{'values'}->{'table'}, \@tabQueryJoins2,
                                                          \@tabQueryFields2, \@tabQueryWheres2, \@tabQueryOrdersBy2,
                                                          0, 0);
                        $tabRows = $tabQueryResult->{'results'};
                    }
                    else
                    {
                        foreach my $value (@{$tabActivesFilters->{$filterId}})
                        {
                            push(@$tabRows, {'value' => $value});
                        }
                    }

                    # Ajout du filtre dans les données renvoyées
                    if ($tabCfgs->{'func'})
                    {
                        $tabRows = &{$tabCfgs->{'func'}}($tabRows, $tabCfgs, $tabActivesFilters->{$filterId});
                    }
                    $tabData{'filters'}->{$filterId}->{'values'} = $tabRows;
                }

            }
            elsif ($tabCfgs->{'type'} eq 'search')
            {
                $tabData{'filters'}->{$filterId} = {};

                # Si le filtre est activé
                if ($tabActivesFilters->{$filterId})
                {
                    $tabData{'filters'}->{$filterId}->{'search'} = $tabActivesFilters->{$filterId}->[0];
                };
            }
        }
    }

    $tabData{'result'} = 'ok';

    return \%tabData;
}


# Function: _execQuery
# Exécuter une requête pour récupérer les données du tableau ou d'un des filtres
#
# Parameters:
# LOC::Db::Adapter $db               - Instance de connexion à la base de données
# string           $tableName        - Nom de la table
# arrayref         $tabQueryJoins    - Jointures
# arrayref         $tabQueryFields   - Champs sélectionnés
# arrayref         $tabQueryWheres   - Conditions
# arrayref         $tabQueryOrdersBy - Tris
# int              $startIndex       - Index de la ligne de départ à récupérer
# int              $pageSize         - Nombre de lignes à récupérer
#
# Returns:
# hashref - Données du tableau ou d'un des filtres
sub _execQuery
{
    my ($db, $tableName, $tabQueryJoins,
        $tabQueryFields, $tabQueryWheres, $tabQueryOrdersBy,
        $startIndex, $pageSize) = @_;


    # Construction de la requête
    my $query = '
SELECT' . ($pageSize ? ' SQL_CALC_FOUND_ROWS' : '') . '
    ' . join(', ', @$tabQueryFields) . '
FROM ' . $tableName;

    # Jointures
    if (@$tabQueryJoins > 0)
    {
        $query .= '
' . join ("\n", @$tabQueryJoins);
    }

    # Conditions
    if (@$tabQueryWheres > 0)
    {
        $query .= '
WHERE ' . join(' AND ', @$tabQueryWheres);
    }

    # Tris
    if (@$tabQueryOrdersBy > 0)
    {
        $query .= '
ORDER BY ' . join(', ', @$tabQueryOrdersBy);
    }

    # Limite et offset
    if ($pageSize)
    {
        $query .= '
LIMIT ' . ($startIndex ? $startIndex : 0) . ', ' . $pageSize;
    }
    $query .= ';';

    # Exécution de la requête
    my $stmt = $db->query($query);
    if (!$stmt)
    {
        return 0;
    }

    my $tab = {
        'count'   => $stmt->getRowsCount() * 1,
        'results' => []
    };
    $tab->{'totalCount'} = ($pageSize ? $db->fetchOne('SELECT FOUND_ROWS();') : $tab->{'count'});

    # Récupération des données
    while (my $row = $stmt->fetch(LOC::Db::FETCH_ASSOC))
    {
        push(@{$tab->{'results'}}, $row);
    }

    return $tab;
}


1;
