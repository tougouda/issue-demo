use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Request
# Module de gestion des requêtes HTTP
package LOC::Request;


# Constants: Expressions régulières
# REGEXP_MCA_URL  - Expression régulière permettant de détecter si la chaîne est au format APPLICATION>module:contrôleur:action
# REGEXP_HTTP_URL - Expression régulière permettant de détecter si la chaîne est une URL HTTP ou HTTPS

# Constants: Types de source
# SRC_REQUEST   - Type de source POST et GET pour la récupération de paramètres
# SRC_POST      - Type de source POST pour la récupération de paramètres
# SRC_GET       - Type de source GET pour la récupération de paramètres
# FILE_IMG_JPG  - Extension de fichier image .jpg
# FILE_IMG_GIF  - Extension de fichier image .gif
# FILE_IMG_PNG  - Extension de fichier image .png
# FILE_IMG_BMP  - Extension de fichier image .bmp

use constant
{
    REGEXP_MCA_URL  => '^([A-Z]{3}\>|)([a-zA-Z0-9_]*)\:([a-zA-Z0-9_]*)\:([a-zA-Z0-9_]*)$',
    REGEXP_HTTP_URL => '^(http|https):\/\/',
    SRC_REQUEST     => '_REQUEST',
    SRC_POST        => '_POST',
    SRC_GET         => '_GET',
    FILE_IMG_JPG    => 'image/jpeg',
    FILE_IMG_GIF    => 'image/gif',
    FILE_IMG_PNG    => 'image/x-png',
    FILE_IMG_BMP    => 'image/x-bmp',
};


use strict;

use LOC::Json;
use LOC::Globals;
use LOC::Util;
use File::Temp;
use File::Type;



my $_GET;
my $_POST;
my $_FILE = {};
my $_isJsonRequested = 0;

# Récupération des paramètres en GET et POST
&_getRequestTab();





# Function: getArray
# Récupérer un tableau passé en GET/POST/REQUEST
#
# Parameters:
# string $name    - Nom du paramètre
# string $default - Valeur par défaut (tableau vide par défaut)
# string $sources - Sources (GET|POST(par défaut)|REQUEST)
#
# Returns:
# hashref -
sub getArray
{
    my ($name, $default, $sources) = @_;
    my $result = &get($name, $default, $sources);
    if (!defined($result) || ref($result) ne 'ARRAY')
    {
        $result = [];
    }
    return (wantarray ? @$result : $result);
}


# Function: getString
# Récupérer une chaîne de caractères passée en GET/POST/REQUEST
#
# Parameters:
# string $name    - Nom du paramètre
# string $default - Valeur par défaut (chaîne vide par défaut)
# string $sources - Sources (GET|POST(par défaut)|REQUEST)
#
# Returns:
# string -
sub getString
{
    my ($name, $default, $sources) = @_;
    return &get($name, (defined $default ? $default : ''), $sources);
}


# Function: getFile
# Récupérer un entier passé en GET/POST/REQUEST
#
# Parameters:
# string $name    - Nom du paramètre
# string $default - Valeur par défaut
#
# Returns:
# string -
sub getFile
{
    my ($name, $default) = @_;

    if (exists $_FILE->{$name})
    {
        return $_FILE->{$name};
    }
    elsif (defined $default)
    {
        return $default;
    }
    return {
            'name'     => '',
            'type'     => '',
            'tmp_name' => '',
            'error'    => 4,
            'size'     => 0
    };
}


# Function: getInteger
# Récupérer un entier passé en GET/POST/REQUEST
#
# Parameters:
# string $name    - Nom du paramètre
# string $default - Valeur par défaut (undef par défaut)
# string $sources - Sources (GET|POST(par défaut)|REQUEST)
#
# Returns:
# string -
sub getInteger
{
    my ($name, $default, $sources) = @_;
    my $result = &get($name, $default, $sources);
    if (!defined($result))
    {
        return undef;
    }
    return $result * 1; # CAST en INTEGER
}


# Function: getBoolean
# Récupérer un booléen passé en GET/POST/REQUEST
#
# Parameters:
# string $name    - Nom du paramètre
# string $default - Valeur par défaut (0 par défaut)
# string $sources - Sources (GET|POST(par défaut)|REQUEST)
#
# Returns:
# string -
sub getBoolean
{
    my ($name, $default, $sources) = @_;
    my $result = &get($name, $default, $sources);
    if (!defined($result))
    {
        return 0;
    }
    return ($result * 1 ? 1 : 0);
}


# Function: getHash
# Récupérer une table de hashage passée en GET/POST/REQUEST
#
# Parameters:
# string $name    - Nom du paramètre
# string $default - Valeur par défaut (table de hashage vide par défaut)
# string $sources - Sources (GET|POST(par défaut)|REQUEST)
#
# Returns:
# hashref -
sub getHash
{
    my ($name, $default, $sources) = @_;
    my $result = &get($name, $default, $sources);
    if (!defined($result) || ref($result) ne 'HASH')
    {
        $result = {};
    }
    return (wantarray ? %$result : $result);
}


# Function: get
# Récupérer une valeur passée en GET/POST/REQUEST
#
# Parameters:
# string $name    - Nom du paramètre
# string $default - Valeur par défaut
# string $sources - Sources (GET|POST(par défaut)|REQUEST)
#
# Returns:
# string -
sub get
{
    my ($name, $default, $sources) = @_;
    unless (defined $sources)
    {
        $sources = SRC_POST;
    }

    if ($sources eq SRC_POST || $sources eq SRC_REQUEST)
    {
        if (!defined $name)
        {
            return $_POST;
        }
        if (exists $_POST->{$name})
        {
            return $_POST->{$name};
        }
    }
    if ($sources eq SRC_GET || $sources eq SRC_REQUEST)
    {
        if (!defined $name)
        {
            return $_GET;
        }
        if (exists $_GET->{$name})
        {
            return $_GET->{$name};
        }
    }
    return $default;
}



# Function: createRequestUri
# Générer une adresse URL
#
# Parameters:
# string $file      - Fichier
# array  $tabParams - Les paramétres à passer
# bool   $isCrypted - Crypté ou pas ?
#
# Returns:
# string -
sub createRequestUri
{
    my ($file, $tabParams, $isCrypted) = @_;
    if (!defined $tabParams)
    {
        $tabParams = {};
    }
    if (!defined $isCrypted)
    {
        $isCrypted = 1;
    }

    my $applicationCode = 'LOC';

    my $expr = REGEXP_MCA_URL;
    if ($file =~ m/$expr/)
    {
        if ($1 ne '')
        {
            $applicationCode = substr($1, 0, -1);
        }

        if ($applicationCode eq 'LOC')
        {
            $tabParams->{'locModule'} = $2;
            $tabParams->{'locController'} = $3;
            $tabParams->{'locAction'} = $4;
            $file = &LOC::Globals::get('locationUrl') . '/cgi-bin/index.cgi';
        }
        else
        {
            $tabParams->{'fwModule'} = $2;
            $tabParams->{'fwController'} = $3;
            $tabParams->{'fwAction'} = $4;
            $file = &LOC::Globals::get('fwApplicationsCfgs')->{$applicationCode}->{'url'} . '/index.php';
        }
    }


    # Tri des paramètres
    my @tabParamsKeys = sort keys(%$tabParams);

    tie(my %tabSortedParams, 'Tie::IxHash');
    %tabSortedParams = %{$tabParams}{@tabParamsKeys};


    my $dev = &LOC::Globals::get('dev');

    if ($isCrypted)
    {
        my $crypted = &LOC::Util::utf8Base64Encode(&LOC::Json::toJson(\%tabSortedParams));
        if ($applicationCode eq 'LOC')
        {
            return $file . '?locParams=' . &urlEncode($crypted) . ($dev eq '' ? '' : '&dev=' . $dev);
        }
        else
        {
            return $file . '?fwParams=' . &urlEncode($crypted) . ($dev eq '' ? '' : '&dev=' . $dev);
        }
    }
    else
    {
        my $p = '';
        while (my ($key, $value) = each(%tabSortedParams))
        {
            $p .= &_urlParams($key, $value);
        }
        $p = substr($p, 0, -1);
        return $file . '?' . $p . ($dev eq '' ? '' : '&dev=' . $dev);
    }
}


# Function: getDate
# Récupérer une date passée en GET/POST/COOKIE (du type 2008-12-30)
#
# Parameters:
# string $name    - Nom du paramètre
# string $default - Valeur par défaut
# string $sources - Sources (GET|POST(par défaut)|REQUEST)
#
# Returns:
# string -
sub getDate
{
    my $date = &get($_[0], (defined $_[1] ? $_[1] : ''), $_[2]);
    return substr($date, 0, 10);
}


# Function: getDateTime
# Récupérer une date/heure passée en GET/POST/COOKIE (du type 2008-12-30 11:38:45)
#
# Parameters:
# string $name    - Nom du paramètre
# string $default - Valeur par défaut
# string $sources - Sources (GET|POST(par défaut)|REQUEST)
#
# Returns:
# string -
sub getDateTime
{
    my $date = &get($_[0], (defined $_[1] ? $_[1] : ''), $_[2]);
    return substr($date, 0, 19);
}


# Function: urlEncode
# Encode une chaîne en URL
#
# Parameters:
# string $str - Chaîne
#
# Returns:
# string -
sub urlEncode
{
    my $str = $_[0];
    $str =~ s/([\W])/"%" . uc(sprintf("%2.2x",ord($1)))/eg;
    return $str;
}


# Function: urlEncode
# Décode une chaîne encodée URL
#
# Parameters:
# string $str - Chaîne
#
# Returns:
# string -
sub urlDecode
{
    my $str = $_[0];
    $str =~ tr/+/ /;
    $str =~ s/%([a-fA-F0-9]{2,2})/chr(hex($1))/eg;
    $str =~ s/<!–(.|\n)*–>//g;
    return $str;
}


# Function: isJsonRequested
# Indique si du la requête a demandé du JSON.
#
# Returns:
# bool - Un booléen indiquant si la requête a demandé du JSON (1) ou non (0).
sub isJsonRequested
{
    return $_isJsonRequested;
}



#################################
# Fonctions privées
#################################
sub _urlParams
{
    my ($name, $value) = @_;

    my $val = '';
    my $type = ref($value);
    if ($type eq 'HASH')
    {
        my $p = '';
        while (my ($subName, $subValue) = each(%{$value}))
        {
            $p .= &_urlParams($name . '[' . $subName . ']', $subValue);
        }
        return $p;
    }
    elsif ($type eq 'ARRAY')
    {
        my $p = '';
        foreach my $subValue (@{$value})
        {
            $p .= &_urlParams($name . '[]', $subValue);
        }
        return $p;
    }
    else
    {
        return &urlEncode($name) . '=' . &urlEncode($value) . '&';
    }
}

sub _addParam
{
    my ($tabVars, $name, $value) = @_;

    if ($name =~ m/^(.*?)(\[.*\])$/)
    {
        $name = $1;
        my @tabKeys = split(/\]\[/, substr($2, 1, -1));

        if (!exists($tabVars->{$name}))
        {
            $tabVars->{$name} = {};
            tie(%{$tabVars->{$name}}, 'Tie::IxHash');
        }

        my $ref = $tabVars->{$name};
        my $nb = @tabKeys;
        my $key;
        for (my $i = 0; $i < $nb - 1; $i++)
        {
            $key = $tabKeys[$i];
            if (!exists($ref->{$key}))
            {
                $ref->{$key} = {};
                tie(%{$ref->{$key}}, 'Tie::IxHash');
            }
            $ref = $ref->{$key};
        }
        $key = $tabKeys[$nb - 1];
        $ref->{$key} = $value;
    }
    else
    {
        $tabVars->{$name} = $value;
    }
}


sub _splitQueryString
{
    my @tabParts = split(/[\&\;]/, $_[0]);

    my $tabVars = {};
    tie(%$tabVars, 'Tie::IxHash');

    my $name;
    my $value;
    foreach my $part (@tabParts)
    {
        ($name, $value) = split(/\=/, $part);
        $value = &LOC::Util::decodeUtf8(&urlDecode($value));
        $name  = &LOC::Util::decodeUtf8(&urlDecode($name));

        &_addParam($tabVars, $name, $value);
    }
    return $tabVars;
}


sub _getRequestTab
{
    $_isJsonRequested = ($ENV{'HTTP_X_REQUESTED_WITH'} eq 'XMLHttpRequest');

    if (!defined $_POST)
    {
        $_POST = {};
        if ($ENV{'REQUEST_METHOD'} eq 'POST' && defined $ENV{'CONTENT_LENGTH'})
        {
            if ($ENV{'CONTENT_TYPE'} =~ m/^multipart\/form-data/)
            {
                binmode STDIN, ":raw";

                my ($boundary) = $ENV{'CONTENT_TYPE'} =~ /boundary=\"?([^\";,]+)\"?/;

                tie(%$_POST, 'Tie::IxHash');

                my $partIndex = -1;
                my $oldPartIndex = -1;
                my $partName = '';
                my $partContent = '';
                my $partFileName = '';
                my $partType = '';
                while (my $line = <STDIN>)
                {
                    if (index($line, $boundary) != -1)
                    {
                        $oldPartIndex = $partIndex;
                        $partIndex++;

                        if ($partIndex != -1 && $partIndex != $oldPartIndex)
                        {
                            if ($partType eq 'file')
                            {
                                $partContent = substr($partContent, 2, -2);


                                my $fh;
                                my $fileName = '';
                                if ($partFileName ne '')
                                {
                                    $fh = File::Temp->new();
                                    $fh->write($partContent);
                                    $fh->unlink_on_destroy(0);
                                    $fh->seek(0, 0);
                                    $fh->close();
                                    $fileName = $fh->filename();
                                }

                                $partFileName =~ s/\\/\//g;
                                my $ft = File::Type->new();
                                $_FILE->{$partName} = {
                                    'name' => &LOC::Util::decodeUtf8(&File::Basename::basename($partFileName)),
                                    'type' => $ft->mime_type($fileName),
                                    'tmp_name' => $fileName,
                                    'error' => ($partFileName eq '' ? 4 : 0),
                                    'size' => length($partContent)
                                };
                            }
                            else
                            {
                                $partContent = substr($partContent, 0, -2);
                                &_addParam($_POST, $partName, $partContent);
                            }
                            $partContent = '';
                        }
                    }
                    elsif ($partIndex != -1)
                    {
                        if (substr($line, 0, 20) eq 'Content-Disposition:')
                        {
                            ($partName) = $line =~ / name="([^"]*)"/;
                            ($partFileName) = $line =~ / filename="([^"]*)"/;
                            $partType = (defined $partFileName ? 'file' : 'param');

                            $line = <STDIN>;
                        }
                        else
                        {
                            $partContent .= $line;
                        }
                    }
                }
            }
            elsif ($ENV{'CONTENT_TYPE'} =~ m/^application\/json/)
            {
                read(STDIN, my $data, $ENV{'CONTENT_LENGTH'});
                if (length($data) > 0)
                {
                    $_POST = &LOC::Json::fromJson($data);
                }
            }
            else
            {
                read(STDIN, my $data, $ENV{'CONTENT_LENGTH'});
                $_POST = &_splitQueryString($data);
            }
        }
    }
    if (!defined $_GET)
    {
        $_GET = {};
        if (defined $ENV{'QUERY_STRING'} && $ENV{'QUERY_STRING'} ne '')
        {
            $_GET = &_splitQueryString($ENV{'QUERY_STRING'});
        }

        # Decrypte l'URL
        if (defined $_GET->{'locParams'})
        {
            my $params = $_GET->{'locParams'};
            my $tabParams = &LOC::Json::fromJson(&LOC::Util::utf8Base64Decode($params));
            while (my($key, $value) = each(%{$tabParams}))
            {
                $_POST->{$key} = $value;
            }
        }
    }
}

1;
