use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Log
# Module de fonctions de traces de l'application
package LOC::Log;


# Constants: Modes
# MODE_SEND_MAIL - Envoie un mail
# MODE_NO_MAIL   - N'envoie pas de mail
use constant
{
    MODE_SEND_MAIL => 1,
    MODE_NO_MAIL   => 0
};


# Modules externes
use Sys::Syslog;

# Constants: Facilités
# FACILITY - La facilité utilisée.
use constant
{
    FACILITY => Sys::Syslog::LOG_LOCAL2
};

# Constants: Motifs pour le formatage des messages
# PATTERN_DEBUG - Débogage
use constant
{
    PATTERN_DEBUG => '%s: %s'
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Globals;
use LOC::Session;
use LOC::Characteristic;
use LOC::Mail;
use LOC::Browser;
use LOC::Log::EventType;
use LOC::Log::Functionality;



# Function: writeDBLog
# Ecrit une nouvelle ligne dans le log en base de données (table h_log)
#
# Parameters:
# string   $functionality   - La fonctionnalité (CONTRAT, DEVIS etc...)
# int      $functionalityId - L'identifiant de la fonctionnalité (CONTRATAUTO, DEVISAUTO etc...)
# string   $eventTypeId     - Identifiant du type de l'évenement (identifiant de la table p_eventtype)
# hashref  $tabContent      - Le contenu à mettre dans le log.
# hashref  $tabOptions      - Tableau des options ('db' : connexion à la base de données)
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, 0 sinon
sub writeDBLog
{
    my ($functionality, $functionalityId, $eventTypeId, $tabContent, $tabOptions) = @_;

    # Récupération de la connection à la base de données
    my $db = (defined $tabOptions->{'db'} ? $tabOptions->{'db'} : &LOC::Db::getConnection('auth'));
    my $hasTransaction = $db->hasTransaction();
    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $userInfos = &LOC::Session::getUserInfos();
    my $userId = $userInfos->{'id'};
    my $connParams = &LOC::Globals::getConnParam('location', $userInfos->{'nomadCountry.id'});
    my $schema = $connParams->{'schema'};

    # Contenu
    my $content = $tabContent->{'content'};
    my $isSQLContent = 0;
    if (defined $tabContent->{'sqlQuery'})
    {
        $content = $tabContent->{'sqlQuery'};
        $isSQLContent = 1;
    }
    # Anciennes valeurs
    my $oldValues = undef;
    if (defined $tabContent->{'old'})
    {
        $oldValues = $tabContent->{'old'};
        if (ref($oldValues) ne '')
        {
            $oldValues = &LOC::Json::toJson($oldValues);
        }
    }
    # Nouvelles valeurs
    my $newValues = undef;
    if (defined $tabContent->{'new'})
    {
        $newValues = $tabContent->{'new'};
        if (ref($newValues) ne '')
        {
            $newValues = &LOC::Json::toJson($newValues);
        }
    }

    my $result = $db->insert('h_log', {
                                       'log_schema' => $schema,
                                       'log_functionality' => $functionality,
                                       'log_functionalityid' => $functionalityId,
                                       'log_etp_id' => $eventTypeId,
                                       'log_content' . ($isSQLContent ? '*' : '') => $content,
                                       'log_oldvalues' => $oldValues,
                                       'log_newvalues' => $newValues,
                                       'log_usr_id' => $userId,
                                       'log_date*' => 'Now()'
                                      }
                                     , 'AUTH');
    if ($result == -1)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }

        return 0;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    return 1;
}


# Function: writeDbFunctionalityLogs
# Ecrit de nouvelles lignes dans le log en base de données pour une même fonctionnalité (table h_log)
#
# Parameters:
# string   $functionality   - La fonctionnalité (CONTRAT, DEVIS etc...)
# int      $functionalityId - L'identifiant de la fonctionnalité (CONTRATAUTO, DEVISAUTO etc...)
# arrayref $tabTraces       - Tableau des traces
# hashref  $tabOptions      - Tableau des options ('db' : connexion à la base de données)
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, 0 sinon
sub writeDbFunctionalityLogs
{
    my ($functionality, $functionalityId, $tabTraces, $tabOptions) = @_;

    if (@$tabTraces == 0)
    {
        return 1;
    }

    # Récupération de la connection à la base de données
    my $db = (defined $tabOptions->{'db'} ? $tabOptions->{'db'} : &LOC::Db::getConnection('auth'));
    my $hasTransaction = $db->hasTransaction();
    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $userInfos = &LOC::Session::getUserInfos();
    my $countryId = (exists $tabOptions->{'countryId'} ? $tabOptions->{'countryId'} : $userInfos->{'nomadCountry.id'});
    my $userId    = (exists $tabOptions->{'userId'} ? $tabOptions->{'userId'} : $userInfos->{'id'});
    my $connParams = &LOC::Globals::getConnParam('location', $userInfos->{'nomadCountry.id'});
    my $schema = $connParams->{'schema'};

    # Ecriture dans la table d'historique
    my @tabInserts = ();
    my $nbTraces = @$tabTraces;
    for (my $i = 0; $i < $nbTraces; $i++)
    {
        my $eventTypeId = $tabTraces->[$i]->{'code'};

        # Contenu
        my $content;
        if (defined $tabTraces->[$i]->{'sqlQuery'})
        {
            $content = $tabTraces->[$i]->{'sqlQuery'};
        }
        else
        {
            $content = $db->quote($tabTraces->[$i]->{'content'})
        }
        # Contenu complémentaire
        my $contentExtra = undef;
        if (defined $tabTraces->[$i]->{'contentExtra'})
        {
            if (ref($tabTraces->[$i]->{'contentExtra'}) eq 'ARRAY' || ref($tabTraces->[$i]->{'contentExtra'}) eq 'HASH')
            {
                $contentExtra = &LOC::Json::toJson($tabTraces->[$i]->{'contentExtra'});
            }
            else
            {
                $contentExtra = $tabTraces->[$i]->{'contentExtra'};
            }
        }
        # Anciennes valeurs
        my $oldValues = undef;
        if (defined $tabTraces->[$i]->{'old'})
        {
            $oldValues = $tabTraces->[$i]->{'old'};
            if (ref($oldValues) ne '')
            {
                $oldValues = &LOC::Json::toJson($oldValues);
            }
        }
        # Nouvelles valeurs
        my $newValues = undef;
        if (defined $tabTraces->[$i]->{'new'})
        {
            $newValues = $tabTraces->[$i]->{'new'};
            if (ref($newValues) ne '')
            {
                $newValues = &LOC::Json::toJson($newValues);
            }
        }

        # Schéma
        if (defined $tabTraces->[$i]->{'schema'})
        {
            $schema = $tabTraces->[$i]->{'schema'};
        }

        # Date
        my $date = 'Now()';
        if (defined $tabTraces->[$i]->{'date'})
        {
            $date = $db->quote($tabTraces->[$i]->{'date'});
        }

        push(@tabInserts, '(' . $db->quote($schema) . ', ' . $db->quote($functionality) . ', ' . $functionalityId . ', ' .
                                '"' . $eventTypeId .'", ' . $content . ', ' . $db->quote($contentExtra) . ', ' .
                                $db->quote($oldValues) . ', ' . $db->quote($newValues) . ', ' .
                                $db->quote($userId) . ', ' . $date . ')');
    }
    my $query = '
INSERT INTO AUTH.h_log (log_schema, log_functionality, log_functionalityid, log_etp_id, log_content, log_contentextra, log_oldvalues, log_newvalues, log_usr_id, log_date)
VALUES ' . join(', ', @tabInserts);
    my $resultObj = $db->query($query);

    if (!$resultObj)
    {
        # Annule la transaction
        if (!$hasTransaction)
        {
            $db->rollBack();
        }

        return 0;
    }

    # Valide la transaction
    if (!$hasTransaction)
    {
        if (!$db->commit())
        {
            $db->rollBack();
            return 0;
        }
    }

    $resultObj->close();
    return 1;
}


# Function: writeFileLog
# Ecrit une trace dans un fichier de log du common_files
#
# Parameters:
# string $directory - Nom du répertoire dans le common_files
# string $content   - Contenu à insérer dans le fichier
# string $fileName  - Nom du fichier de log (optionnel, le même que le nom du répertoire si non spécifié)
# string $mode      - optionnel, deux possibilités : MODE_NO_MAIL (par défaut) ou MODE_SEND_MAIL
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, 0 sinon
sub writeFileLog
{
    my ($directory, $content, $fileName, $mode) = @_;

    # si pas de nom de fichier
    if (!$fileName)
    {
        $fileName = $directory;
    }

    # Pas d'envoi de mail par défaut
    if (!defined $mode)
    {
        $mode = MODE_NO_MAIL;
    }

    my @tabLocaltime = localtime();
    my $logFile = sprintf('%s%s_%04d-%02d.txt', &LOC::Util::getCommonFilesPath('logs/' . $directory . '/'),
                                                $fileName,
                                                $tabLocaltime[5] + 1900,
                                                $tabLocaltime[4] + 1);
    open(LOG, '>>' . $logFile) || return 0;
    my $line = sprintf('[%04d-%02d-%02d %02d:%02d:%02d] %s', $tabLocaltime[5] + 1900, $tabLocaltime[4] + 1,
                                                             $tabLocaltime[3], $tabLocaltime[2],
                                                             $tabLocaltime[1], $tabLocaltime[0], $content);
    print LOG $line . "\r\n";
    close(LOG);

    # Envoi du mail
    if ($mode == MODE_SEND_MAIL)
    {
        my $mailObj = LOC::Mail->new();

        # Encodage
        $mailObj->setEncodingType(LOC::Mail::ENCODING_TYPE_UTF8);

        # Expéditeur
        my $sender = &LOC::Globals::get('errorsSender');
        $mailObj->setSender(%$sender);

        # Priorité du mail
        $mailObj->setMailPriority(LOC::Mail::MAIL_PRIORITY_HIGH);

        # Sujet
        $mailObj->setSubject('[GesLoc-' . ucfirst($directory) . '] Erreur');

        # Corps du mail
        $mailObj->setBodyType(LOC::Mail::BODY_TYPE_TEXT);
        $mailObj->setBody('Fichier de log : ' . $logFile . "\n\nMessage d'erreur : \n" . $line);

        # Destinataire
        my $tabErrorsRecipients = &LOC::Globals::get('errorsRecipients');
        $mailObj->addRecipients(%$tabErrorsRecipients);

        # Envoi
        return $mailObj->send();
    }

    return 1;
}

# Function: writeFileDebug
# Ecrit une trace dans un fichier de log du common_files
#
# Parameters:
# string $entity    - Entité à tracer (répertoire dans le common_files)
# string $content   - Contenu à insérer dans le fichier
# string $fileName  - Nom du fichier de log
# string $mode      - optionnel, deux possibilités : MODE_NO_MAIL (par défaut) ou MODE_SEND_MAIL
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, 0 sinon
sub writeFileDebug
{
    my ($entity, $content, $fileName, $mode) = @_;

    my $debugMode = &LOC::Json::fromJson(&LOC::Characteristic::getGlobalValueByCode('DEBUG'));
    if ($debugMode->{$entity})
    {
        &LOC::Log::writeFileLog($entity, $content, 'debug_' . $fileName);
    }
    return 1;
}


# Function: getFunctionalityHistory
# Récupérer l'historique d'une fonctionnalité
#
# Parameters:
# string $countryId       - Pays
# string $functionality   - Nom de la fonctionnalité
# mixed  $functionalityId - Identifiant de la fonctionnalité
# string $localeId        - Identifiant de la locale pour les traductions
#
# Returns:
# array|arrayref - Retourne la liste des historiques de la fonctionnalité
sub getFunctionalityHistory
{
    my ($countryId, $functionality, $functionalityId, $localeId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('location', $countryId);

    # Functionnalités gérées
    my %tabFunctionalities = (
        'contract' => {
            'name'     => 'CONTRAT',
            'oldField' => 'CONTRATAUTO'
        },
        'estimate' => {
            'name'     => 'DEVIS',
            'oldField' => 'DEVISAUTO'
        },
        'estimateLine' => {
            'name' => 'LIGNEDEVIS'
        },
        'machine' => {
            'name'              => 'MACHINE',
            'oldField'          => 'MAAUTO',
            'oldFieldCondition' => 'AND (CONTRATAUTO IS NULL OR CONTRATAUTO = 0) AND (DEVISAUTO IS NULL OR DEVISAUTO = 0)'
        },
        'machineInventory' => {
            'name' => 'ETATDESLIEUX'
        }
    );

    if (!exists $tabFunctionalities{$functionality})
    {
        return undef;
    }

    my $func = $tabFunctionalities{$functionality};

    my $query = '
SELECT
    log_id AS `id`,
    "' . $functionality . '" AS `functionality.name`,
    log_functionalityid AS `functionality.id`,
    etp_id AS `eventType.code`,
    IFNULL(etl_label, etp_label) AS `eventType.label`,
    log_usr_id AS `user.id`,
    usr_name AS `user.name`,
    usr_firstname AS `user.firstName`,
    CONCAT_WS(" ", usr_firstname, usr_name) AS `user.fullName`,
    usr_sta_id AS `user.state.id`,
    log_date AS `datetime`,
    log_content AS `content`,
    log_contentextra AS `contentExtra`
FROM AUTH.h_log
LEFT JOIN frmwrk.f_user
ON log_usr_id = usr_id
LEFT JOIN AUTH.p_eventtype
ON log_etp_id = etp_id
LEFT JOIN AUTH.p_eventtype_locale
ON etp_id = etl_etp_id AND etl_lcl_id = "' . $localeId . '"
WHERE log_schema = ' . $db->quote($db->{'_configs'}->{'schema'}) . '
AND log_functionality = "' . $func->{'name'} . '"
AND log_functionalityid = "' . $functionalityId . '"';

    # Récupération des vieilles traces
    if ($func->{'oldField'})
    {
        my $queryOldField = '
(SELECT
    tbl_h_history.TRACEAUTO AS `id`,
    "' . $functionality . '" AS `functionality.name`,
    tbl_h_history.' . $func->{'oldField'} . ' AS `functionality.id`,
    tbl_p_state.ETCODE AS `eventType.code`,
    tbl_p_state.ETLIBELLE AS `eventType.label`,
    tbl_h_history.PEAUTO AS `user.id`,
    usr_name AS `user.name`,
    usr_firstname AS `user.firstName`,
    CONCAT_WS(" ", usr_firstname, usr_name) AS `user.fullName`,
    usr_sta_id AS `user.state.id`,
    tbl_h_history.TRACETEMPS AS `datetime`,
    NULL AS `content`,
    NULL AS `contentExtra`
FROM TRACE tbl_h_history
LEFT JOIN frmwrk.f_user
ON tbl_h_history.PEAUTO = usr_id
LEFT JOIN ETATTABLE tbl_p_state
ON tbl_h_history.ETCODE = tbl_p_state.ETCODE
WHERE tbl_h_history.' . $func->{'oldField'} . ' = ' . $functionalityId;
    if ($func->{'oldFieldCondition'})
    {
         $queryOldField .= ' ' . $func->{'oldFieldCondition'};
    }
    $queryOldField .=  ')';
    $query = $queryOldField . '
UNION (' . $query . ')
ORDER BY `datetime` DESC, `id` DESC';
    }
    $query .= ';';

    my $tabHistory = $db->fetchAssoc($query);
    if (!$tabHistory)
    {
        return undef;
    }
    return (wantarray ? @$tabHistory : $tabHistory);
}


# Function: writeSysLog
# Écrit dans les logs système.
#
# Parameters:
# string $facility - La facilité.
# string $priority - La priorité.
# string $message  - Le message.
sub _writeSysLog
{
    my ($facility, $priority, $message) = @_;

    &Sys::Syslog::openlog(&LOC::Globals::get('appProjectName'), 'pid', $facility);
    &Sys::Syslog::syslog($priority, $message);
    &Sys::Syslog::closelog();
}


# Function: _formatDebugMessage
# Formate un message de débogage.
#
# Parameters:
# string $message - Le message.
sub _formatDebugMessage
{
    my ($message) = @_;

    my %session = &LOC::Session::getInfos();

    if ($session{'ip'} eq '')
    {
        return $message;
    }

    return sprintf(PATTERN_DEBUG, $session{'ip'}, $message);
}


# Function: debug
# Écrit une trace de débogage.
#
# Parameters:
# string $message - Le message.
sub debug
{
    my ($message) = @_;

    &_writeSysLog(FACILITY, Sys::Syslog::LOG_DEBUG, &_formatDebugMessage($message));
}


# Function: info
# Écrit une information.
#
# Parameters:
# string $message  - Le message.
sub info
{
    my ($message) = @_;

    &_writeSysLog(FACILITY, Sys::Syslog::LOG_INFO, $message);
}


# Function: notice
# Écrit une notice.
#
# Parameters:
# string $message  - Le message.
sub notice
{
    my ($message) = @_;

    &_writeSysLog(FACILITY, Sys::Syslog::LOG_NOTICE, $message);
}


# Function: warning
# Écrit un avertissement.
#
# Parameters:
# string $message  - Le message.
sub warning
{
    my ($message) = @_;

    &_writeSysLog(FACILITY, Sys::Syslog::LOG_WARNING, $message);
}


# Function: debug
# Écrit une erreur.
#
# Parameters:
# string $message  - Le message.
sub error
{
    my ($message) = @_;

    &_writeSysLog(FACILITY, Sys::Syslog::LOG_ERR, $message);
}


# Function: critical
# Écrit une erreur critique.
#
# Parameters:
# string $message  - Le message.
sub critical
{
    my ($message) = @_;

    &_writeSysLog(FACILITY, Sys::Syslog::LOG_CRIT, $message);
}


# Function: alert
# Écrit une alerte.
#
# Parameters:
# string $message  - Le message.
sub alert
{
    my ($message) = @_;

    &_writeSysLog(FACILITY, Sys::Syslog::LOG_ALERT, $message);
}


# Function: emergency
# Écrit une urgence.
#
# Parameters:
# string $message  - Le message.
sub emergency
{
    my ($message) = @_;

    &_writeSysLog(FACILITY, Sys::Syslog::LOG_EMERG, $message);
}


# Function: dump
# Affiche les informations d'une variable.
#
# Parameters:
# string $var  - La variable à afficher.
sub dump
{
    my ($var) = @_;

    if (ref($var) ne '')
    {
        use Data::Dumper;
        $var = &Data::Dumper::Dumper($var);
    }

    &LOC::Browser::sendHeaders("Content-type: text/html; charset=UTF-8\n\n");
    my $style = 'border: 1px solid #CCCCCC; '
        . 'background-color: rgba(0, 0, 0, 0.01); '
        . 'color: #333333; '
        . 'padding: 5px; '
        . 'margin: 5px; '
        . 'font-family: \'SFMono-Regular\', Consolas, \'Liberation Mono\', Menlo, Courier, monospace; '
        . 'font-size: 13px; '
        . 'max-height: 400px; '
        . 'overflow: auto;';
    print '<pre style="' . $style . '">' . &LOC::Util::toHTMLEntities($var) . '</pre>' . "\n";
}


1;
