use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Alert
# Module de fonctions de mise à jour des alertes
package LOC::Alert;


# Constants: États
# STATE_ACTIVE - Alerte active
# STATE_INACTIVE - Alerte inactive
use constant
{
    STATE_ACTIVE   => 'GEN01',
    STATE_INACTIVE => 'GEN03',
};

# Constants: Liste des alertes
# ESTIMATELINESTOCALL         - Lignes de devis à rappeler
# ACCESPTEDESTIMATELINES      - Lignes de devis acceptées
# FIRSTPRINTRENTCONTRACTS     - (Pré)-contrats à éditer
# CONTRACTSTOSTOP             - Contrats à arrêter
# PRECONTRACTS                - Pré-contrats effectués
# MACHINESTOALLOCATE          - Machines à attribuer
# MACHINESTOCONTROL1WEEK      - Machines à contrôler dans une semaine
# MACHINESTOCONTROL2WEEKS     - Machines à contrôler dans 2 semaines
# MACHINESTOCONTROL2MONTHS    - Machines à contrôler dans 2 mois
# FUEL                        - Carburants à saisir
# CUSTOMERSTOCHECK            - Clients à vérifier
# ORDERNUMTOPROVIDE_AGENCY    - N° de commande à fournir (agence)
# ORDERNUMTOPROVIDE_ALL       - N° de commande à fournir
# ORDERCOMPULSORYTOPROVIDE    - BC client à fournir
# ORDERCOMPULSORYTOTORECEIVE  - BC client à réceptionner
# CONTRACTSTOINVOICE          - Contrats à passer en facturation
# DELIVERYRECOVERYNOTESTOSIGN - BL/BR à signer
# REPAIRSHEETS                - Fiches de remise en état en attente
# REPAIRESTIMATES             - Devis de remise en état en attente 
# PROFORMA_ALL                - Règlements pro forma en attente
# PROFORMA_AGENCY             - Règlements pro forma en attente (agence)
# PENDINGTRANSPORTS           - Transports en cours
# PENDINGSELFTRANSPORTS       - Enlèvements agence / Reprises chantier
# ENDINGCONTRACTSTRANSFERS    - Transferts inter-chantiers / Reprises chantier
# MACHINESVISIBILITY          - Machines en visibilité
# ENDINGCONTRACTSRECOVERIES   - Récupérations / Restitutions
# MACHINEINVENTORIES          - États des lieux à traiter
# MACHINESCREATIONTOFINALIZE  - Création de machines à finaliser
use constant
{
    ESTIMATELINESTOCALL         => 0,
    ACCESPTEDESTIMATELINES      => 1,
    FIRSTPRINTRENTCONTRACTS     => 2,
    CONTRACTSTOSTOP             => 3,
    PRECONTRACTS                => 4,
    MACHINESTOALLOCATE          => 7,
    MACHINESTOCONTROL1WEEK      => 8,
    MACHINESTOCONTROL2WEEKS     => 9,
    MACHINESTOCONTROL2MONTHS    => 10,
    FUELS                       => 12,
    CUSTOMERSTOCHECK            => 13,
    ORDERNUMTOPROVIDE_AGENCY    => 20,
    ORDERNUMTOPROVIDE_ALL       => 21,
    ORDERCOMPULSORYTOPROVIDE    => 22,
    ORDERCOMPULSORYTOTORECEIVE  => 23,
    CONTRACTSTOINVOICE          => 27,
    DELIVERYRECOVERYNOTESTOSIGN => 28,
    REPAIRSHEETS                => 29,
    REPAIRESTIMATES             => 30,
    PROFORMA_ALL                => 31,
    PROFORMA_AGENCY             => 32,
    PENDINGTRANSPORTS           => 33,
    PENDINGSELFTRANSPORTS       => 34,
    ENDINGCONTRACTSTRANSFERS    => 35,
    MACHINESVISIBILITY          => 36,
    ENDINGCONTRACTSRECOVERIES   => 37,
    MACHINEINVENTORIES          => 38,
    MACHINESCREATIONTOFINALIZE  => 39
};


use strict;

use LWP::Simple;

# Modules internes
use LOC::Globals;
use LOC::Db;
use LOC::Util;
use LOC::Locale;


# Function: setDeltaCounterAlert
# Met à jour le compteur de l'alerte dans l'agence demandée avec le delta passé en paramètre
#
# Parameters:
# int     $alertId  - Identifiant de l'alerte
# string $agencyId - Identifiant de l'agence
# int    $delta    - la différence à appliquer au compteur
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, 0 sinon
sub setDeltaCounterAlert
{
    my ($alertId, $agencyId, $delta) = @_;

    $alertId *= 1;
    $delta   *= 1;

    if ($delta != 0)
    {
        # Récupère les paramètres de connexion à AUTH
        my $db = &LOC::Db::getConnection('auth');

        if ($db->update('ALERTE', {'AL' . $alertId . '*' => $db->quoteIdentifier('AL' . $alertId) .
                                                            ($delta > 0 ? ' + ' : ' - ') . abs($delta)},
                                  {'AGAUTO' => $agencyId}, 'AUTH') == -1)
        {
            return 0;
        }
    }
    return 1;
}


# Function: setDeltaCounterAlerts
# Met à jour le compteur des alertes dans l'agence demandée avec les delta passés en paramètre
#
# Parameters:
# string  $agencyId  - Identifiant de l'agence
# hashref $tabDeltas - la différence à appliquer au compteur pour chaque alerte
#
# Returns:
# bool - Retourne 1 si la mise à jour s'est correctement effectuée, 0 sinon
sub setDeltaCounterAlerts
{
    my ($agencyId, $tabDeltas) = @_;

    # Récupère les paramètres de connexion à AUTH
    my $db = &LOC::Db::getConnection('auth');

    my @tabAlerts = keys %$tabDeltas;
    if (@tabAlerts == 0)
    {
        return 1;
    }

    my $query  = '
SELECT
    alt_number AS `number`,
    alt_level AS `level`,
    agc_cty_id AS `country.id`,
    agc_are_id AS `area.id`';
    foreach my $alertId (@tabAlerts)
    {
        $query .= ',
    AL' . $alertId . ' AS `count_' . $alertId . '`';
    }
    $query .= '
FROM p_alert
INNER JOIN ALERTE
ON AGAUTO = "' . $agencyId . '"
INNER JOIN frmwrk.p_agency
ON AGAUTO = agc_id
WHERE alt_number IN (' . join(', ', @tabAlerts) . ');';

    my $stmt = $db->query($query);
    while (my $tabRow = $stmt->fetch(LOC::Db::FETCH_ASSOC))
    {
        my $level     = $tabRow->{'level'};
        my $number    = $tabRow->{'number'};
        my $count     = ($tabRow->{'count_' . $number} + $tabDeltas->{$number});
        my $areaId    = $tabRow->{'area.id'};
        my $countryId = $tabRow->{'country.id'};

        $query = '
UPDATE ALERTE
INNER JOIN frmwrk.p_agency
ON agc_id = AGAUTO
SET AL' . $number . ' = ' . $count . '
WHERE ';

        if ($level eq 'AGC')
        {
            $query .= 'agc_id = "' . $agencyId . '";';
        }
        elsif ($level eq 'ARE' && $areaId ne '')
        {
            $query .= 'agc_are_id = ' . $areaId . ';';
        }
        elsif ($level eq 'CTY')
        {
            $query .= 'agc_cty_id = "' . $countryId . '";';
        }
        else
        {
            next;
        }

        if (!$db->query($query))
        {
            return 0;
        }
    }

    return 1;
}

# Function: getInfos
# Retourne les infos d'une alerte
# 
# Contenu du hashage :
# - id => identifiant de l'alerte
# - name => nom de l'alerte
# - group.name => nom de la catégorie (intitulé du logiciel)
# - group.order => ordre d'affichage de la catégorie
# - label => libellé traduit de l'alerte
# - number => n° de l'alerte
# - url => url d'affichage des résultats de l'alerte
# - state.id => identifant de l'état
#
#
# Parameters:
# string $id        - Identifiant de l'alerte
# string $countryId - Identifiant du pays
# string $localeId  - Identifiant de la locale
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getInfos
{
    my ($id, $countryId, $localeId) = @_;
    unless (defined $localeId)
    {
        $localeId = LOC::Locale::DEFAULT_LOCALE_ID;
    }

    # connexion à AUTH
    my $db = &LOC::Db::getConnection('location', $countryId);
    
    # Récupère les données
    my $query = '
SELECT
    a1.alt_id AS `id`,
    a1.alt_name AS `name`,
    L1.LONOM AS `group.name`,
    (
        SELECT
            MIN(a2.alt_number)
        FROM AUTH.p_alert a2
        WHERE a2.alt_sft_id = a1.alt_sft_id
        GROUP BY a2.alt_sft_id
    ) AS `group.order`,
    IFNULL(all_label, a1.alt_label) AS `label`,
    a1.alt_number AS `number`,
    CONCAT(' . $db->quote(&LOC::Globals::get('locationUrl')) . ', IF(a1.alt_url IS NULL, CONCAT("/cgi-bin/old/commun/alerte.cgi?num=", a1.alt_name), a1.alt_url)) AS `url`,
    a1.alt_sta_id AS `state.id`
FROM AUTH.p_alert a1
    INNER JOIN LOGICIEL L1 ON L1.LOAUTO = alt_sft_id
    LEFT JOIN AUTH.p_alert_locale ON (a1.alt_id = all_alt_id AND all_lcl_id = ' . $db->quote($localeId) . ')
WHERE a1.alt_id = ' . $db->quote($id) . ';';

    my $dataRef = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    unless ($dataRef)
    {
        return 0;
    }
    if (wantarray)
    {
        return %$dataRef;
    }
    else
    {
        return $dataRef;
    }
}

# Function: getList
# Retourne la liste des alertes
#
# Parameters:
# int     $format     - Format de retour de la liste
# string  $countryId  - Identifiant du pays
# string  $localeId   - Identifiant de la locale
# hashref $tabFilters - Filtres ('user' : id d'un utilisateur, 'agency' : id d'une agence)
#
# Returns:
# hash|hashref|0 - Liste des alertes
sub getList
{
    my ($format, $countryId, $localeId, $tabFilters) = @_;
    unless (defined $localeId)
    {
        $localeId = LOC::Locale::DEFAULT_LOCALE_ID;
    }

    # connexion à Location
    my $db = &LOC::Db::getConnection('location', $countryId);

    my $query = '';

    # Filtres
    my $whereQuery = '';
    # - Utilisateur
    if ($tabFilters->{'user'} ne '')
    {
        $whereQuery .= '
AND a1.alt_sft_id IN (
    SELECT L2.LOAUTO
    FROM AUTH.MEPR MP
        INNER JOIN MENU M ON M.MEAUTO = MP.MEAUTO
        INNER JOIN AUTH.PERSONNEL P ON P.PROFILAUTO = MP.PROFILAUTO
        INNER JOIN LOGICIEL L2 ON L2.LOAUTO = M.LOAUTO
    WHERE P.PEAUTO = ' . $tabFilters->{'user'} . '
 )';
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    a1.alt_id as `id`,
    a1.alt_name AS `name`,
    L1.LONOM AS `group.name`,
    (
        SELECT
            MIN(a2.alt_number)
        FROM AUTH.p_alert a2
        WHERE a2.alt_sft_id = a1.alt_sft_id
        GROUP BY a2.alt_sft_id
    ) AS `group.order`,
    IFNULL(all_label, a1.alt_label) AS `label`,
    a1.alt_number AS `number`,
    CONCAT(' . $db->quote(&LOC::Globals::get('locationUrl')) . ', "/", IF(a1.alt_url IS NULL, CONCAT("cgi-bin/old/commun/alerte.cgi?num=", a1.alt_name), a1.alt_url)) AS `url`,
    a1.alt_sta_id AS `state.id`
FROM AUTH.p_alert a1
    INNER JOIN LOGICIEL L1 ON L1.LOAUTO = alt_sft_id
    LEFT JOIN AUTH.p_alert_locale ON (a1.alt_id = all_alt_id AND all_lcl_id = ' . $db->quote($localeId) . ')
WHERE a1.alt_sta_id <> ' . $db->quote(STATE_INACTIVE) .
$whereQuery . '
ORDER BY `group.order`, `number`;';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*) AS `label`
FROM AUTH.p_alert a1
WHERE a1.alt_sta_id <> ' . $db->quote(STATE_INACTIVE) .
$whereQuery . ';';
    }
    else
    {
        $query = '
SELECT
    a1.alt_id as `id`,
    IFNULL(all_label, a1.alt_label) AS `label`
FROM AUTH.p_alert a1
    LEFT JOIN AUTH.p_alert_locale ON (a1.alt_id = all_alt_id AND all_lcl_id = ' . $db->quote($localeId) . ')
WHERE a1.alt_sta_id <> ' . $db->quote(STATE_INACTIVE) .
$whereQuery . '
ORDER BY alt_number;';
    }

    # récupère les alertes de location
    my $alerts = $db->fetchList($format, $query, {}, 'id');

    # Récupération des compteurs de l'agence
    if ($tabFilters->{'agency'} ne '' && $format == LOC::Util::GETLIST_ASSOC)
    {
        my $dbAuth = &LOC::Db::getConnection('auth');
        my $queryCnt = 'SELECT * FROM ALERTE WHERE AGAUTO = "' . $tabFilters->{'agency'} . '"';
        my $resultCnt = $dbAuth->fetchRow($queryCnt, {}, LOC::Util::GETLIST_ASSOC);

        if (%$alerts > 0 && %$resultCnt > 0)
        {
            while (my ($id, $alert) = each(%$alerts))
            {
                my $colName = 'AL' . $alert->{'number'};
                my $count = $resultCnt->{$colName};
                $alert->{'count'} = $count;
            }
        }
    }

    return $alerts;
}

# Function: getCrmList
# Récupère la liste des alertes CRM
sub getCrmList
{
    my ($format, $countryId, $localeId, $tabFilters) = @_;

    # liste des alertes
    my $alert = {};

    if ($countryId eq 'FR' || $countryId eq 'ES')
    {
        # définit la locale
        my $locale = LOC::Locale->new($localeId);

        # connexion au framework
        my $connFrm = &LOC::Db::getConnection('frmwrk');

        # vérifie l'accès à l'application 
        my $query = '
SELECT
    COUNT(*)
FROM f_user_application
WHERE uap_usr_id = ' . $tabFilters->{'user'} . '
    AND uap_app_id=2';
        my $result = $connFrm->fetchOne($query);
        if ($result > 0)
        {
            # nombre d'alertes CRM
            my $url = &LOC::Globals::get('fwApplicationsCfgs')->{'CRM'}->{'url'} .
                      '?_exec=public/userAlertsCount&userId=' . $tabFilters->{'user'} . '&country=' . $countryId;
            my $count = &LWP::Simple::get($url);

            # définit les alertes CRM
            $alert = {
                     'CRM' => {
                              'id' => 'CRM',
                              'name' => 'CRM',
                              'group.name' => 'CRM',
                              'group.order' => 2000,
                              'label' => $locale->t('Alerte(s) CRM'),
                              'number' => '',
                              'url' => &LOC::Globals::get('fwApplicationsCfgs')->{'CRM'}->{'url'},
                              'count' => $count
                              }
                     };
        }
    }

    return $alert;
}
1;