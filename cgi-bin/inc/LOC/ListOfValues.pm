use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::ListOfValues
# Module de gestion des listes de valeurs
package LOC::ListOfValues;


# Constants: États
# STATE_ACTIVE   - Alerte active
# STATE_INACTIVE - Alerte inactive
use constant
{
    STATE_ACTIVE   => 'GEN01',
    STATE_INACTIVE => 'GEN03',
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Util;


# Function: getInfos
# Retourne un hachage contenant les infos d'une liste de valeurs
#
# Parameters:
# string $countryId - Pays
# int    $id        - Id de la liste
#
# Returns:
# hash|hashref
sub getInfos
{
    my ($countryId, $id, $flags) = @_;

    my $tab = getList($countryId, LOC::Util::GETLIST_ASSOC, {'id' => $id}, $flags);
    return (exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: getList
# Récupérer la liste des listes de valeurs
#
# Contenu du hachage:
# - id          => id
# - name        => libellé
# - state.id    => état
# - currency.id => id devise
# - type        => type 
# - cost        => coût du service
# - amount      => montant du service
#
# Parameters:
# string  $countryId  - Pays
# int     $format     - Format de la liste en sortie
# hashref $tabFilters - Liste des filtres (id)
# int     $flags      - Flags
#
# Returns:
# hash|hashref
sub getList
{
    my ($countryId, $format, $tabFilters, $flags) = @_;
    unless (defined $flags)
    {
        $flags = 0;
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('auth');

    my $query = '';

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    lov_id AS `id`,
    lov_code AS `code`,
    lov_name AS `name`,
    lov_description AS `description`,
    lov_sta_id AS `state.id`,
    lov_table AS `table`,
    lov_package AS `package`,
    lov_is_multicountries AS `isMultiCountries`,
    lov_date_create AS `dateCreate`,
    lov_date_modif AS `dateModif`
FROM p_listofvalues
WHERE lov_sta_id <> ' . $db->quote(STATE_INACTIVE);
        if (defined $tabFilters->{'id'})
        {
            if (ref($tabFilters->{'id'}) eq 'ARRAY')
            {
                $query .= '
    AND `lov_id` IN (' . join(', ', @{$tabFilters->{'id'}}) . ')';
            }
            else
            {
                $query .= '
    AND `lov_id` = ' . $tabFilters->{'id'};
            }
        }
        $query .= '
ORDER BY lov_name'; 
    }

    my $tab = $db->fetchList($format, $query, {}, 'id');
    if (!$tab)
    {
        return undef;
    }

    return (wantarray ? %$tab : $tab);
}

1;