use utf8;
use open (':encoding(UTF-8)');

# Class: LOC::EndUpdates
# Module de gestion des mise à jour de fin
package LOC::EndUpdates;


# Constants
use constant
{
    KIMUPD_FEATURES            => 0x0001,
    KIMUPD_SITUATION           => 0x0002,
    KIMUPD_LOCALIZATION        => 0x0004,
    KIMUPD_RECOVERYCOUNTER     => 0x0008,
    KIMUPD_SITETRANSFERCOUNTER => 0x0010,
    KIMUPD_TIMECOUNTER         => 0x0020
};


use strict;


# Modules internes
use LOC::Alert;
use LOC::Log;
use LOC::Kimoce;
use LOC::Transport;
use LOC::Machine;
use LOC::StartCode::Contract::Rent;

# Tableau de correspondances avec les fonctionnalités
my $tabFunctionalities = {
    'contract'         => LOC::Log::Functionality::CONTRACT,
    'estimate'         => LOC::Log::Functionality::ESTIMATE,
    'estimateLine'     => LOC::Log::Functionality::ESTIMATELINE,
    'transport'        => LOC::Log::Functionality::TRANSPORT,
    'machinesFamily'   => LOC::Log::Functionality::MACHINESFAMILY,
    'model'            => LOC::Log::Functionality::MODEL,
    'machine'          => LOC::Log::Functionality::MACHINE,
    'machineInventory' => LOC::Log::Functionality::MACHINEINVENTORY
};



# Function: addTrace
# Ajout d'une trace
#
# Parameters:
# hashref  $tabEndUpds      - Tableau des mises à jour de fin
# string   $functionality   - La fonctionnalité (CONTRAT, DEVIS etc...)
# int      $functionalityId - L'identifiant de la fonctionnalité (CONTRATAUTO, DEVISAUTO etc...)
# hashref  $tabContent      - Le contenu à mettre dans le log.
#
# Return:
# bool
sub addTrace
{
    my ($tabEndUpds, $functionality, $functionalityId, $tabContent) = @_;


    if (!exists $tabEndUpds->{'traces'})
    {
        $tabEndUpds->{'traces'} = {};
    }
    my $tab = $tabEndUpds->{'traces'};

    if (!exists $tab->{$functionality})
    {
        $tab->{$functionality} = {};
    }
    if (!exists $tab->{$functionality}->{$functionalityId})
    {
        $tab->{$functionality}->{$functionalityId} = [];
    }

    # Traces
    push(@{$tab->{$functionality}->{$functionalityId}}, $tabContent);

    return 1;
}


# Function: exec
# Exécution des mises à jour de fin
#
# Parameters:
# string   $countryId  - Pays
# hashref  $tabEndUpds - Tableau des mises à jour de fin
# int      $userId     - Utilisateur responsable des mises à jour
#
# Return:
# bool
sub exec
{
    my ($countryId, $tabEndUpds, $userId) = @_;


    # Mise à jour des alertes pour chaque agence
    if (exists $tabEndUpds->{'alerts'})
    {
        while (my ($agencyId, $tabDeltas) = each(%{$tabEndUpds->{'alerts'}}))
        {
            &LOC::Alert::setDeltaCounterAlerts($agencyId, $tabDeltas);
        }
    }

    # Ecriture des traces dans la table d'historique
    if (exists $tabEndUpds->{'traces'})
    {
        while (my ($functionality, $tabFunctionalityIds) = each(%{$tabEndUpds->{'traces'}}))
        {
            if (defined $tabFunctionalities->{$functionality})
            {
                $functionality = $tabFunctionalities->{$functionality};
            }
            while (my ($functionalityId, $tabTraces) = each(%$tabFunctionalityIds))
            {
                &LOC::Log::writeDbFunctionalityLogs($functionality, $functionalityId, $tabTraces,
                                                    {'countryId' => $countryId,
                                                        'userId' => $userId});
            }
        }
    }

    # Mise à jour des stocks
    if (exists $tabEndUpds->{'stocks'})
    {
        while (my ($agencyId, $tabModelStocks) = each(%{$tabEndUpds->{'stocks'}}))
        {
            while (my ($modelId, $tabStockUpdates) = each(%$tabModelStocks))
            {
                &LOC::Model::updateStock($agencyId, $modelId, $tabStockUpdates);
            }
        }
    }

    # Mise à jour de kimoce
    if (exists $tabEndUpds->{'kimoces'})
    {
        while (my ($parkNumber, $tabMachineUpdates) = each(%{$tabEndUpds->{'kimoces'}}))
        {
            my $tabOptions = $tabMachineUpdates->{'tabOptions'};
            if ($tabMachineUpdates->{'flags'} & KIMUPD_FEATURES)
            {
                &LOC::Kimoce::syncMachineFeatures($parkNumber);
            }
            if ($tabMachineUpdates->{'flags'} & KIMUPD_SITUATION)
            {
                &LOC::Kimoce::setMachineSituation($parkNumber, $tabOptions->{'state.id'});
            }
            if ($tabMachineUpdates->{'flags'} & KIMUPD_LOCALIZATION)
            {
                &LOC::Kimoce::setMachineLocalization($parkNumber, $tabOptions->{'agency.id'});
            }
            # - Mise à jour des compteurs
            my $tabCounters = {};
            if ($tabMachineUpdates->{'flags'} & KIMUPD_RECOVERYCOUNTER)
            {
                $tabCounters->{LOC::Kimoce::COUNTER_RECOVERIES()} = 1;
            }
            if ($tabMachineUpdates->{'flags'} & KIMUPD_SITETRANSFERCOUNTER)
            {
                $tabCounters->{LOC::Kimoce::COUNTER_SITESTRANSFERS()} = 1;
            }
            if ($tabMachineUpdates->{'flags'} & KIMUPD_TIMECOUNTER)
            {
                $tabCounters->{LOC::Kimoce::COUNTER_TIME()} = $tabOptions->{'timeCounter'};
            }
            if (keys(%$tabCounters) > 0)
            {
                &LOC::Kimoce::updateMachineCounters($parkNumber, $tabCounters);
            }
        }
    }

    # Digicodes
    if (exists $tabEndUpds->{'startCodes'})
    {
        while (my ($countryId, $tabCountrySendSMS) = each(%{$tabEndUpds->{'startCodes'}}))
        {
            foreach my $tabSendSMS (@$tabCountrySendSMS)
            {
                &LOC::StartCode::Contract::Rent::sendSms(
                    $countryId,
                    $userId,
                    $tabSendSMS->{'tabReceivers'},
                    $tabSendSMS->{'tabInfos'}
                );
            }
        }
    }

    # Actions
    if (exists $tabEndUpds->{'actions'})
    {
        # Tri des actions
        my @tabSorted = sort {$b->{'priority'} <=> $a->{'priority'} || $a->{'index'} <=> $b->{'index'}} @{$tabEndUpds->{'actions'}->{'list'}};

        # Exécution de toutes les actions dans l'ordre
        foreach my $tabActionsInfos (@tabSorted)
        {
            &{$tabActionsInfos->{'callback'}}($tabActionsInfos->{'params'});
        }
    }

    return 1;
}


# Function: getTraces
# Récupérer la liste des traces pour une fonctionnalité
#
# Parameters:
# hashref  $tabEndUpds      - Tableau des mises à jour de fin
# string   $functionality   - La fonctionnalité (CONTRAT, DEVIS etc...)
# int      $functionalityId - L'identifiant de la fonctionnalité (CONTRATAUTO, DEVISAUTO etc...)
#
# Return:
# arrayref
sub getTraces
{
    my ($tabEndUpds, $functionality, $functionalityId) = @_;

    if (!exists $tabEndUpds->{'traces'} ||
        !exists $tabEndUpds->{'traces'}->{$functionality} ||
        !exists $tabEndUpds->{'traces'}->{$functionality}->{$functionalityId})
    {
        return [];
    }

    return $tabEndUpds->{'traces'}->{$functionality}->{$functionalityId};
}


# Function: deleteTraces
# Supprimer les traces pour une fonctionnalité
#
# Parameters:
# hashref  $tabEndUpds      - Tableau des mises à jour de fin
# string   $functionality   - La fonctionnalité (CONTRAT, DEVIS etc...)
# int      $functionalityId - L'identifiant de la fonctionnalité (CONTRATAUTO, DEVISAUTO etc...)
#
# Return:
# bool
sub deleteTraces
{
    my ($tabEndUpds, $functionality, $functionalityId) = @_;

    if (!exists $tabEndUpds->{'traces'} ||
        !exists $tabEndUpds->{'traces'}->{$functionality} ||
        !exists $tabEndUpds->{'traces'}->{$functionality}->{$functionalityId})
    {
        return 1;
    }

    delete $tabEndUpds->{'traces'}->{$functionality}->{$functionalityId};

    return 1;
}


# Function: updateAlert
# Mise à jour du compteur d'une alerte
#
# Parameters:
# hashref  $tabEndUpds - Tableau des mises à jour de fin
# string   $agencyId   - Id de l'agence dans laquelle on veut mettre à jour l'alerte
# int      $alertId    - Id de l'alerte
# int      $delta      - Delta
#
# Return:
# bool
sub updateAlert
{
    my ($tabEndUpds, $agencyId, $alertId, $delta) = @_;


    if (!exists $tabEndUpds->{'alerts'})
    {
        $tabEndUpds->{'alerts'} = {};
    }
    if (!exists $tabEndUpds->{'alerts'}->{$agencyId})
    {
        $tabEndUpds->{'alerts'}->{$agencyId} = {};
    }

    my $tab = $tabEndUpds->{'alerts'}->{$agencyId};
    if (!exists $tab->{$alertId})
    {
        $tab->{$alertId} = $delta;
    }
    else
    {
        $tab->{$alertId} += $delta;
    }

    return 1;
}


# Function: updateKimoceMachine
# Mise à jour d'une machine dans Kimoce
#
# Parameters:
# hashref $tabEndUpds - Tableau des mises à jour de fin
# int     $parkNumber - Numéro de parc de la machine
# int     $updFlags   - Mises à jour à effectuer (voir constantes KIMUPD_*)
# hashref $tabOptions - Informations nécessaires pour les mises à jour
#
# Return:
# bool
sub updateKimoceMachine
{
    my ($tabEndUpds, $parkNumber, $updFlags, $tabOptions) = @_;

    if (!exists $tabEndUpds->{'kimoces'})
    {
        $tabEndUpds->{'kimoces'} = {};
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    if (!exists $tabEndUpds->{'kimoces'}->{$parkNumber})
    {
        $tabEndUpds->{'kimoces'}->{$parkNumber} = {
            'flags' => $updFlags,
            'tabOptions' => $tabOptions
        };
    }
    else
    {
        my %newTabOptions = (%{$tabEndUpds->{'kimoces'}->{$parkNumber}->{'tabOptions'}}, %$tabOptions);
        $tabEndUpds->{'kimoces'}->{$parkNumber} = {
            'flags' => $tabEndUpds->{'kimoces'}->{$parkNumber}->{'flags'} | $updFlags,
            'tabOptions' => \%newTabOptions
        };
    }

    return 1;
}


# Function: updateModelStock
# Mise à jour du stock d'un modèle de machine sur une agence
#
# Parameters:
# hashref  $tabEndUpds - Tableau des mises à jour de fin
# string   $agencyId   - Id de l'agence dans laquelle on veut mettre à jour l'alerte
# int      $property   - Id de l'alerte
# int      $delta      - Delta
#
# Return:
# bool
sub updateModelStock
{
    my ($tabEndUpds, $agencyId, $modelId, $property, $delta) = @_;


    if (!exists $tabEndUpds->{'stocks'})
    {
        $tabEndUpds->{'stocks'} = {};
    }
    if (!exists $tabEndUpds->{'stocks'}->{$agencyId})
    {
        $tabEndUpds->{'stocks'}->{$agencyId} = {};
    }

    my $tab = $tabEndUpds->{'stocks'}->{$agencyId};
    if (!exists $tab->{$modelId})
    {
        $tab->{$modelId} = {$property => $delta};
    }
    elsif (!exists $tab->{$modelId}->{$property})
    {
        $tab->{$modelId}->{$property} = $delta;
    }
    else
    {
        $tab->{$modelId}->{$property} += $delta;
    }

    return 1;
}


# Function: sendStartCodeSms
# Envoi d'un SMS contenant le digicode
#
# Parameters:
# hashref $tabEndUpds   - Tableau des mises à jour de fin
# string  $countryId    - Identifiant du pays
# hashref $tabReceivers - Liste des destinataires au format suivant : id contrat => numéro de téléphone
# hashref $tabInfos     - Informations du SMS :
#                         - machinesFamily.label : libellé complet de la famille de machines
#                         - machine.parkNumber : numéro de parc
#                         - code : digiocde
#                         - beginDate : date de début de validité du digicode
#                         - endDate : date de fin de validité du digicode
#
# Return:
# bool
sub sendStartCodeSms
{
    my ($tabEndUpds, $countryId, $tabReceivers, $tabInfos) = @_;


    if (!exists $tabEndUpds->{'startCodes'})
    {
        $tabEndUpds->{'startCodes'} = {};
    }
    if (!exists $tabEndUpds->{'startCodes'}->{$countryId})
    {
        $tabEndUpds->{'startCodes'}->{$countryId} = [];
    }

    my $tab = $tabEndUpds->{'startCodes'}->{$countryId};

    my $tabSmsInfos = {
        'tabReceivers' => $tabReceivers,
        'tabInfos'     => $tabInfos
    };

    my @tabReceivers = values(%$tabReceivers);

    # Si le SMS n'est pas déjà dans les éléments stockés, on le rajoute
    if (!grep {
            my @myTabReceivers = values(%{$_->{'tabReceivers'}});

            &LOC::Util::areArraysEquals(\@myTabReceivers, \@tabReceivers) &&
            &LOC::Util::hash_compare($_->{'tabInfos'}, $tabSmsInfos->{'tabInfos'})
        } @$tab)
    {
        push(@$tab, $tabSmsInfos);
    }

    return 1;
}


# Function: addAction
# Ajoute une action
#
# Parameters:
# hashref $tabEndUpds - Tableau des mises à jour de fin
# coderef $callback   - Description de la routine à exécuter
# hashref $tabOptions - Options supplémentaires :
#                           - 'id'       : identifiant pour l’unicité (non défini par défaut)
#
# Return:
# bool
sub addAction
{
    my ($tabEndUpds, $callback, $tabOptions) = @_;

    if (!defined $tabOptions || ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }

    if (!exists $tabEndUpds->{'actions'})
    {
        $tabEndUpds->{'actions'} = {
            'currentIndex' => 1,
            'uniqueRefs'   => {},
            'list'         => []
        };
    }

    my $id = (defined $tabOptions->{'id'} ? $tabOptions->{'id'} : '');
    if ($id eq '' || !exists $tabEndUpds->{'actions'}->{'uniqueRefs'}->{$id})
    {
        push(@{$tabEndUpds->{'actions'}->{'list'}}, {
            'index'    => $tabEndUpds->{'actions'}->{'currentIndex'}++,
            'id'       => $id,
            'callback' => $callback,
            'priority' => (defined $tabOptions->{'priority'} ? $tabOptions->{'priority'} * 1 : 0),
            'category' => (defined $tabOptions->{'category'} ? $tabOptions->{'category'} : ''),
            'params'   => (defined $tabOptions->{'params'}   ? $tabOptions->{'params'} : {})
        });

        if ($id ne '')
        {
            $tabEndUpds->{'actions'}->{'uniqueRefs'}->{$id} = 1;
        }
    }

    return 1;
}


1;
