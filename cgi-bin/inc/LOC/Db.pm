use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Db
# Module de gestion de la connexion aux bases de données
package LOC::Db;


use constant
{
    FETCH_ASSOC => 1,
    FETCH_NUM   => 2
};

use strict;

use LOC::Country;
use LOC::Db::Adapter;
use LOC::Db::Result;
use LOC::Globals;

my %tabInstances = ();


# Function: getConnection
# Retourne une connexion à une base de données
#
# Parameters:
# string $connName  - Nom de la connexion
# string $countryId - Id du pays, si nécessaire
#
# Returns:
# LOC::Db::Adapter -
sub getConnection
{
    my $connName = $_[0];
    my $countryId = $_[1];

    my $connectionId = $connName . '_' . $countryId;
    if (!defined $tabInstances{$connectionId})
    {
        my $configs = &LOC::Globals::getConnParam($connName, $countryId);

        $tabInstances{$connectionId} = LOC::Db::Adapter->new(%$configs);
    }

    return $tabInstances{$connectionId};
}


sub closeConnection
{
    my $connName = $_[0];
    my $countryId = $_[1];

    my $connectionId = $connName . '_' . $countryId;

    if (defined $tabInstances{$connectionId})
    {
        $tabInstances{$connectionId}->close();
        $tabInstances{$connectionId} = undef;
    }
}

sub closeConnections
{
    my $key;
    my $connection;
    foreach my $key (keys %tabInstances)
    {
        my ($connName, $countryId) = split('_', $key);
        &closeConnection($connName, $countryId);
    }
}

sub getSchemaName
{
    my ($countryId) = @_;

    my $connParam = &LOC::Globals::getConnParam('location', $countryId);
    return $connParam->{'schema'};
}


# Function: END
# Destruction du module
#
sub END
{
    &closeConnections();
}




1;