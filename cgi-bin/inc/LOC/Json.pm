use utf8;
use open (':encoding(UTF-8)');

# Class: LOC::Json
# Classe de gestion de l'affichage en JSON
package LOC::Json;

use strict;

use JSON::XS;
use POSIX;
use LOC::Util;
use LOC::Browser;


# Variable: $VERSION
# Version de la classe
my $VERSION = '1.00';



# Function: new
# Constructeur de la classe
#
# Returns:
# LOC::Json - Instance créée
sub new
{
    my ($class) = @_;

    my $this = {};
    bless($this, $class);

    # Encodage de la page 
    $this->{'_pageEncoding'} = 'UTF-8';
    # Instance de l'objet JSON
    $this->{'_json'} = JSON::XS->new();

    return $this;
}


# Function: getJson
# Récupérer l'instance de JSON
#
# Parameters:
#
# Returns:
# JSON - Instance de JSON
sub getJson
{
    my ($this) = @_;

    return $this->{'_json'};
}


# Function: displayHeader
# Affichage de l'en-tête
#
# Parameters:
#
# Returns:
# string - Code HTML
sub displayHeader
{
    my ($this) = @_;
    my $header = '';
    if (defined $this->{'_responseCode'})
    {
        $header .= 'Status: ' . $this->{'_responseCode'} . "\n";
    }
    $header .= "Content-Type: application/json; charset=" . $this->{'_pageEncoding'} . "\n\n";

    return &LOC::Browser::getDisplayableHeaders($header);
}

# Function: setHttpResponseCode
# Définir le code HTTP de la réponse
#
# Parameters:
# int $responseCode - Code HTTP
#
# Returns

sub setHttpResponseCode
{
    my ($this, $responseCode) = @_;
    $this->{'_responseCode'} = $responseCode;
}


# Function: encode
# Encoder en JSON
#
# Parameters:
# scalar|arrayref|hashref $param - Structure à encoder en JSON
#
# Returns:
# string - Chaîne JSON
sub encode
{
    my ($this, $param) = @_;

    my $oldLocale = &POSIX::setlocale(LC_NUMERIC);
    &POSIX::setlocale(LC_NUMERIC, 'C');
    my $jsonStr = $this->{'_json'}->encode($param);
    &POSIX::setlocale(LC_NUMERIC, $oldLocale);
    return $jsonStr;
}


# Function: toJson
# Encoder en JSON
#
# Parameters:
# scalar|arrayref|hashref $obj - Structure à encoder en JSON
#
# Returns:
# string - Chaîne JSON
sub toJson
{
    my ($obj) = @_;

    my $oldLocale = &POSIX::setlocale(LC_NUMERIC);
    &POSIX::setlocale(LC_NUMERIC, 'C');
    my $jsonStr = JSON::XS->new->allow_nonref(1)->encode($obj);
    &POSIX::setlocale(LC_NUMERIC, $oldLocale);
    return $jsonStr;
}


# Function: fromJson
# Décoder une chaîne JSON
#
# Parameters:
# string $str - Chaîne à décoder
#
# Returns:
# scalar|arrayref|hashref
sub fromJson
{
    my ($str) = @_;
    my $obj = JSON::XS->new->allow_nonref(1)->decode($str);
    return &_reencode($obj);
}


# Function: isJson
# Détermine si une chaîne est du JSON.
#
# Parameters:
# string $str - La chaîne à tester.
#
# Returns:
# int - 1 si la chaîne est du JSON, 0 sinon
sub isJson
{
    my ($str) = @_;

    # Source de l'expression régulière : https://regex101.com/library/tA9pM8 
    return ($str =~ /
(?(DEFINE)
# Note that everything is atomic, JSON does not need backtracking if it's valid
# and this prevents catastrophic backtracking
(?<json>(?>\s*(?&object)\s*|\s*(?&array)\s*))
(?<object>(?>\{\s*(?>(?&pair)(?>\s*,\s*(?&pair))*)?\s*\}))
(?<pair>(?>(?&STRING)\s*:\s*(?&value)))
(?<array>(?>\[\s*(?>(?&value)(?>\s*,\s*(?&value))*)?\s*\]))
(?<value>(?>true|false|null|(?&STRING)|(?&NUMBER)|(?&object)|(?&array)))
(?<STRING>(?>"(?>\\(?>["\\\/bfnrt]|u[a-fA-F0-9]{4})|[^"\\\0-\x1F\x7F]+)*"))
(?<NUMBER>(?>-?(?>0|[1-9][0-9]*)(?>\.[0-9]+)?(?>[eE][+-]?[0-9]+)?))
)
\A(?&json)\z
/x);
}


# Function: _reencode
# Réencode un objet provenant d'une trame JSON dans les bons types (nombres notamment).
#
# Parameters:
# arrayref|hashref $obj - Objet à réencoder
#
# Returns:
# arrayref|hashref
sub _reencode
{
    my $obj = $_[0];

    if (ref($obj) eq 'HASH')
    {
        foreach my $key (keys(%$obj))
        {
            if (ref($obj->{$key}) ne '')
            {
                &_reencode($obj->{$key});
            }
            elsif (&LOC::Util::isInt($obj->{$key}))
            {
                $obj->{$key} = &LOC::Util::toNumber($obj->{$key});
            }
        }
    }
    elsif (ref($obj) eq 'ARRAY')
    {
        my $nb = @$obj;
        for (my $i = 0; $i < $nb; $i++)
        {
            &_reencode($obj->[$i]);
            if (ref($obj->[$i]) ne '')
            {
                &_reencode($obj->[$i]);
            }
            elsif (&LOC::Util::isInt($obj->[$i]))
            {
                $obj->[$i] = &LOC::Util::toNumber($obj->[$i]);
            }
        }
    }

    return $obj;
}


1;