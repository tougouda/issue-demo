use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Date
# Module de fonctions de dates.
#
# Tous les paramètres au format MySQL peuvent être passés au format 'date' ou 'datetime'
package LOC::Date;


# Constants: Formats de date
# FORMAT_DATE - Date MySQL au format date YYYY-MM-DD
# FORMAT_DATETIME - Date MySQL au format datetime YYYY-MM-DD HH:II:SS
use constant
{
    FORMAT_DATE     => 0,
    FORMAT_DATETIME => 1
};


# Constants: Unités de temps
# UNIT_YEARS   - Années
# UNIT_WEEKS   - Semaines
# UNIT_HOURS   - Heures
# UNIT_MINUTES - Minutes
# UNIT_SECONDS - Secondes
use constant
{
    UNIT_YEARS   => 'years',
    UNIT_WEEKS   => 'weeks',
    UNIT_HOURS   => 'hours',
    UNIT_MINUTES => 'minutes',
    UNIT_SECONDS => 'seconds'
};


# Constants: Nombre de jours ouvrés
# AVERAGE_MONTH_WORKING_DAYS_COUNT - Nombre moyen de jours ouvrés dans un mois
use constant
{
    AVERAGE_MONTH_WORKING_DAYS => 21
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Util;
use DateTime;
use DateTime::Format::Strptime;

# Autres modules
use Date::Calc;
use POSIX;


# Function: getDeltaDays
# Retourne un entier correspondant au nombre de jours calendaires entre deux dates (incluses)
# sur le principe que si les dates sont identiques, cela retourne 1
#
# Parameters:
# string $beginDate - Date de début au format MySQL
# string $endDate   - Date de fin au format MySQL
#
# Returns:
# int -
sub getDeltaDays
{
    my ($beginDate, $endDate) = @_;
    my %hashDateBegin = &getHashDate($beginDate);
    my %hashDateEnd   = &getHashDate($endDate);

    my $nbDays = &Date::Calc::Delta_Days($hashDateBegin{'y'}, $hashDateBegin{'m'}, $hashDateBegin{'d'},
                                         $hashDateEnd{'y'}, $hashDateEnd{'m'}, $hashDateEnd{'d'});
    return ($nbDays + 1);
}


# Function: getInterval
# Retourne l'intervalle de temps entre 2 dates dans l'unité choisie
#
# Parameters:
# string $beginDate - Date de début au format MySQL
# string $endDate   - Date de fin au format MySQL
# string $unit      - Unité de temps
#
# Returns:
# int - Intervalle de temps dans l'unité choisie
sub getInterval
{
    my ($beginDate, $endDate, $unit) = @_;

    if (!defined $unit)
    {
        $unit = UNIT_MINUTES;
    }

    my $format = DateTime::Format::Strptime->new({
        'pattern' => '%Y-%m-%d %H:%M:%S'
    });

    my $start = $format->parse_datetime($beginDate);
    my $stop  = $format->parse_datetime($endDate);

    $_->set_time_zone('local') for $start, $stop;

    return $start->delta_ms($stop)->in_units($unit);
}


# Function: getRentEndDate
# Retourne la date de fin de location correspondant à la date de début passée en paramètre
# plus la durée de location (sans samedi, dimanche et jours fériés) dans l'agence concernée
#
#
# Parameters:
# string $beginDate - Date de début de location au format MySQL
# int    $duration  - Nombre de jours ouvrés
# string $agency    - Identifiant de l'agence
#
# Returns:
# string - Date de fin de location au format MySQL
sub getRentEndDate
{
    my ($beginDate, $duration, $agency) = @_;

    my $endDate = $beginDate;
    my $nbDays  = $duration;

    if (&isWorkingDay($endDate, $agency))
    {
        $nbDays--;
    }
    return &addWorkingDays($endDate, $nbDays, $agency);
}


# Function: getRentDuration
# Retourne un entier correspondant au nombre de jours de location entre deux dates (incluses) dans l'agence concernée
# durée = date de fin - date de début - week-ends - jours fériés.
# Conditions particulières :
#    - si le contrat s'arrête un samedi, un dimanche ou un jour férié, on compte tous les jours;
#    - si le contrat débute un samedi, un dimanche ou un jour férié, on ne change rien.
#
# *TODO* : Que faire quand un contrat se termine par un weekend précédé par un vendredi férié ?
#
# Parameters:
# string $beginDate       - Date de début de la période au format MySQL
# string $endDate         - Date de fin de la période au format MySQL
# string $agency          - Identifiant de l'agence
# string $contractEndDate - Date de fin du contrat au format MySQL (vaut $endDate par défaut)
#
# Returns:
# int -
sub getRentDuration
{
    my ($beginDate, $endDate, $agency, $contractEndDate) = @_;

    if ($beginDate gt $endDate)
    {
        return 0;
    }

    if (!defined $contractEndDate)
    {
        $contractEndDate = $endDate;
    }

    # Récupération des jours fériés
    my $tabLegalHolidays = &getLegalHolidays($beginDate, $contractEndDate, $agency);

    my $duration = &_getRentDuration($beginDate, $endDate, $tabLegalHolidays, $contractEndDate);

    return $duration;
}
# Function: _getRentDuration
# Retourne un entier correspondant au nombre de jours de location entre deux dates (incluses) dans l'agence concernée
# durée = date de fin - date de début - week-ends - jours fériés.
# Conditions particulières :
#    - si le contrat s'arrête un samedi, un dimanche ou un jour férié, on compte tous les jours;
#    - si le contrat débute un samedi, un dimanche ou un jour férié, on ne change rien.
#
# *TODO* : Que faire quand un contrat se termine par un weekend précédé par un vendredi férié ?
#
# Parameters:
# string $beginDate       - Date de début de la période au format MySQL
# string $endDate         - Date de fin de la période au format MySQL
# string $agency          - Identifiant de l'agence
# string $contractEndDate - Date de fin du contrat au format MySQL (vaut $endDate par défaut)
#
# Returns:
# int -
sub _getRentDuration
{
    my ($beginDate, $endDate, $tabLegalHolidays, $contractEndDate) = @_;

    if (!defined $contractEndDate)
    {
        $contractEndDate = $endDate;
    }

    if ($endDate gt $contractEndDate)
    {
        $endDate = $contractEndDate;
    }

    my $nbWorkingDays = &_getDeltaWorkingDays($beginDate, $endDate, $tabLegalHolidays);
    my $duration = $nbWorkingDays;

    # Parcours de la période de location en partant de la fin
    my $date = $contractEndDate;
    my $isLegalHoliday = 1;

    while ($isLegalHoliday && $date ge $beginDate)
    {
        # Jour week-end ou férié => on rajoute un jour
        if (&isWeekendDay($date) || &LOC::Util::in_array($date, $tabLegalHolidays))
        {
            if ($date le $endDate)
            {
                $duration++;
            }
        }
        # Sinon, fin de la boucle
        else
        {
            $isLegalHoliday = 0;
        }

        $date = &subDays($date, 1);
    }

    return $duration;
}

# Function: getDeltaWorkingDays
# Retourne un entier correspondant au nombre de jours ouvrables (sans samedi, dimanche et jours fériés) entre deux dates
# (incluses) dans l'agence concernée
#
#
# Parameters:
# string $beginDate - Date de début au format MySQL
# string $endDate   - Date de fin au format MySQL
# string $agency    - Identifiant de l'agence
#
# Returns:
# int -
sub getDeltaWorkingDays
{
    my ($beginDate, $endDate, $agency) = @_;

    if ($beginDate gt $endDate)
    {
        return 0;
    }

    # nombre de jours fériés non weekend
    my $tabLegalHolidays = &getLegalHolidays($beginDate, $endDate, $agency);

    my $nbDeltaWorkingDays = &_getDeltaWorkingDays($beginDate, $endDate, $tabLegalHolidays);

    return $nbDeltaWorkingDays;
}



# Function: _getDeltaWorkingDays
# Retourne un entier correspondant au nombre de jours ouvrables (sans samedi, dimanche et jours fériés) entre deux dates
# (incluses) dans l'agence concernée
#
#
# Parameters:
# string $beginDate - Date de début au format MySQL
# string $endDate   - Date de fin au format MySQL
# string $agency    - Identifiant de l'agence
#
# Returns:
# int -
sub _getDeltaWorkingDays
{
    my ($beginDate, $endDate, $tabLegalHolidays) = @_;

    if (!defined $tabLegalHolidays)
    {
        $tabLegalHolidays = [];
    }

    my $nbDeltaWorkingDays = 0;

    # nombre de jours fériés non weekend
    my $nbLegalHolidays = @$tabLegalHolidays;
    my $nbDeltaDays = &getDeltaDays($beginDate, $endDate);
    foreach my $holiday (values(@$tabLegalHolidays))
    {
        # si le jour férié n'est pas dans la période
        # - ou dans la période et tombe un week-end
        if ($holiday lt $beginDate || $holiday gt $endDate ||
            ($holiday ge $beginDate && $holiday le $endDate && &isWeekendDay($holiday)))
        {
            $nbLegalHolidays -= 1;
        }
    }

    # nombre de jours weekend
    my $nbWeekendDays = 0;
    my $date = $beginDate;
    for (my $i = 1; $i <= $nbDeltaDays; $i++)
    {
        if (&isWeekendDay($date))
        {
            $nbWeekendDays++;
        }
        $date = &addDays($date, 1)
    }

    $nbDeltaWorkingDays = $nbDeltaDays - $nbWeekendDays - $nbLegalHolidays;

    return $nbDeltaWorkingDays;
}


# Function: getNextDay
# Retourne pour une date MySQL passée en paramètre le lendemain au format MySQL

# Returns:
# string -
sub getNextDay
{
    my ($date) = @_;

    return &addDays($date, 1);
}

# Function: getPreviousDay
# Retourne pour une date MySQL passée en paramètre la veille au format MySQL

# Returns:
# string -
sub getPreviousDay
{
    my ($date) = @_;

    return &subDays($date, 1);
}

# Function: getHashDate
# Retourne pour une date MySQL passée en paramètre un tableau de hashage
#
# - d => le jour
# - m => le mois
# - y => l'année
# - h => les heures
# - i => les minutes
# - s => les secondes
#
# Retourne h, i et s à 0 si c'est un format MySQL 'date'
#
# Parameters:
# string $date - Date au format MySQL (optionnel, retourne la date du jour sinon)
#
# Returns:
# hash -
sub getHashDate
{
    my ($date) = @_;

    my @tab;

    if ($date)
    {
        @tab = (0, 0, 0, substr($date, 8, 2) * 1, substr($date, 5, 2) * 1, substr($date, 0, 4) * 1);

        # si la date MySQL contient des heures/minutes/secondes
        if (length($date) == 19)
        {
            $tab[0] = substr($date, 17, 2) * 1;
            $tab[1] = substr($date, 14, 2) * 1;
            $tab[2] = substr($date, 11, 2) * 1;
        }
    }
    else
    {
        @tab = localtime();
        $tab[5] += 1900;
        $tab[4] += 1;
    }

    return (
        'd' => $tab[3],
        'm' => $tab[4],
        'y' => $tab[5],
        'h' => $tab[2],
        'i' => $tab[1],
        's' => $tab[0]
    );
}

# Function: getMySQLDate
# Retourne pour un tableau de hashage une date MySQL
#
# Le tableau de hashage en entrée doit contenir:
# - d => le jour
# - m => le mois
# - y => l'année
# - h => les heures (pour un type datetime)
# - i => les minutes (pour un type datetime)
# - s => les secondes (pour un type datetime)
#
# Parameters:
# string $dateType - Type de date MySQL : FORMAT_DATE (date) par défaut ou FORMAT_DATETIME (datetime)
# hash %hashDate   - Le tableau de hashage (optionnel, retourne la date du jour sinon)
#
# Returns:
# string -
sub getMySQLDate
{
    my ($dateType, %hashDate) = @_;

    # si pas de date en paramètre, on prend la date du jour
    if (!%hashDate)
    {
        ($hashDate{'s'}, $hashDate{'i'}, $hashDate{'h'}, $hashDate{'d'}, $hashDate{'m'}, $hashDate{'y'},
                                                                        undef, undef ,undef) = localtime;
        $hashDate{'m'} += 1;
        $hashDate{'y'} += 1900;

    }

    my $dateFormat = "%04d-%02d-%02d %02d:%02d:%02d";
    if ($dateType == FORMAT_DATE)
    {
        $dateFormat = "%04d-%02d-%02d";
    }

    my $date = sprintf($dateFormat, $hashDate{'y'},
                                    $hashDate{'m'},
                                    $hashDate{'d'},
                                    $hashDate{'h'},
                                    $hashDate{'i'},
                                    $hashDate{'s'});

    return $date;
}

# Function: getLegalHolidays
# Retourne une référence de tableau contenant les jours fériés entre les deux dates (incluses) dans l'agence concernée
#
# Parameters:
# string $beginDate - Date de début au format MySQL
# string $endDate   - Date de fin au format MySQL
# string $agency    - Identifiant de l'agence
#
# Returns:
# array|arrayref|false -
sub getLegalHolidays
{
    my ($beginDate, $endDate, $agency) = @_;

    # récupère les paramètres de connexion à AUTH
    my $conn = &LOC::Db::getConnection('auth');

    my $query = '
SELECT
    JOJOURS AS `holiday`
FROM PAJOAG
WHERE AGAUTO = "' . $agency . '"
AND JOJOURS BETWEEN "' . $beginDate . '" AND "' . $endDate . '";';

    return $conn->fetchCol($query);
}

# Function: isLegalHoliday
# Retourne un booléen indiquant si le jour est férié dans l'agence concernée
#
# Parameters:
# string $date   - Date de début au format MySQL
# string $agency - Identifiant de l'agence
#
# Returns:
# bool -
sub isLegalHoliday
{
    my ($date, $agency) = @_;

    # récupère les paramètres de connexion à AUTH
    my $conn = &LOC::Db::getConnection('auth');

    my $query = '
SELECT
    *
FROM PAJOAG
WHERE AGAUTO = "' . $agency . '"
AND JOJOURS = "' . $date . '";';
    my $result = $conn->query($query);
    my $nbRows = $result->getRowsCount();

    return ($nbRows > 0 ? 1 : 0);
}

# Function: isWeekendDay
# Retourne un booléen indiquant si le date passée en paramètre est un jour weekend (samedi ou dimanche) ou pas
#
# Parameters:
# string $date - Date au format MySQL
#
# Returns:
# bool -
sub isWeekendDay
{
    my ($date) = @_;

    my %hashDate = &getHashDate($date);
    my $dayNumber = &Date::Calc::Day_of_Week($hashDate{'y'}, $hashDate{'m'}, $hashDate{'d'});

    # si c'est un samedi ou un dimanche, on retourne vrai, faux sinon
    return ($dayNumber == 6 || $dayNumber == 7 ? 1 : 0);
}

# Function: isWorkingDay
# Retourne un booléen indiquant si le date passée en paramètre est un jour ouvré ou non dans l'agence concernée
#
# Parameters:
# string $date   - Date au format MySQL
# string $agency - Identifiant de l'agence
#
# Returns:
# bool -
sub isWorkingDay
{
    my ($date, $agency) = @_;

    return (!&isLegalHoliday($date, $agency) && !&isWeekendDay($date) ? 1 : 0);
}

# Function: compare
# Compare deux dates
#
# Parameters:
# string $date1 - Date au format MySQL
# string $date2 - Date au format MySQL
#
# Returns:
# int - < 0 si $date1 est inférieure à $date2; > 0 si $date1 est supérieure à $date2; 0 si les deux dates sont égales
sub compare
{
    my ($date1, $date2) = @_;

    # obtention du hachage des deux dates
    my %hd1 = &getHashDate($date1);
    my %hd2 = &getHashDate($date2);

    # obtention du timestamp des deux dates
    my $ts1 = mktime($hd1{'s'}, $hd1{'i'}, $hd1{'h'}, $hd1{'d'}, ($hd1{'m'} - 1), ($hd1{'y'} - 1900));
    my $ts2 = mktime($hd2{'s'}, $hd2{'i'}, $hd2{'h'}, $hd2{'d'}, ($hd2{'m'} - 1), ($hd2{'y'} - 1900));

    # comparaison
    if ($ts1 > $ts2)
    {
        return 1;
    }
    elsif ($ts1 < $ts2)
    {
        return -1;
    }
    else
    {
        return 0;
    }
}
# Function: checkMySQLDate
# Vérifie que la date MySQL est valide
#
# Parameters:
# string $date - Date au format MySQL
#
# Returns:
# int 1 si $date est valide, 0 sinon
sub checkMySQLDate
{
    my ($date) = @_;

    # obtention du hachage de la date
    my %hd = &getHashDate($date);
    if ($hd{'y'} == 0 && $hd{'m'} == 0 && $hd{'d'} == 0)
    {
        return 0;
    }

    return &Date::Calc::check_date($hd{'y'}, $hd{'m'}, $hd{'d'});
}


# Function: addDays
# Retourne une date correspondant à la date passée en paramètre plus le nombre de jours
#
#
# Parameters:
# string $date   - Date au format MySQL
# int    $nbDays - Nombre de jours
#
# Returns:
# string - Date au format MySQL avec le nombre de jours en plus
sub addDays
{
    my ($date, $nbDays) = @_;

    my %hashDate = &getHashDate($date);

    my ($year, $month, $day) = &Date::Calc::Add_Delta_Days($hashDate{'y'}, $hashDate{'m'}, $hashDate{'d'}, $nbDays);
    $hashDate{'y'} = $year;
    $hashDate{'m'} = $month;
    $hashDate{'d'} = $day;

    return &getMySQLDate(FORMAT_DATE, %hashDate);
}


# Function: subDays
# Retourne une date correspondant à la date passée en paramètre moins le nombre de jours
#
#
# Parameters:
# string $date   - Date au format MySQL
# int    $nbDays - Nombre de jours
#
# Returns:
# string - Date au format MySQL avec le nombre de jours en moins
sub subDays
{
    my ($date, $nbDays) = @_;

    return &addDays($date, - $nbDays);
}


# Function: addWorkingDays
# Retourne une date correspondant à la date passée en paramètre plus le nombre de jours ouvrés
#
#
# Parameters:
# string $date   - Date au format MySQL
# int    $nbDays - Nombre de jours
# string $agency - Identifiant de l'agence
#
# Returns:
# string - Date au format MySQL avec le nombre de jours ouvrés en plus
sub addWorkingDays
{
    my ($date, $nbDays, $agency) = @_;

    if ($nbDays < 0)
    {
        return &subWorkingDays($date, - $nbDays, $agency);
    }

    my $endDate = $date;

    my $i = 0;
    while ($i < $nbDays)
    {
        $endDate = &getNextDay($endDate);
        if (&isWorkingDay($endDate, $agency))
        {
            $i++;
        }
    }
    return $endDate;
}

# Function: subWorkingDays
# Retourne une date correspondant à la date passée en paramètre moins le nombre de jours ouvrés
#
#
# Parameters:
# string $date   - Date au format MySQL
# int    $nbDays - Nombre de jours
# string $agency - Identifiant de l'agence
#
# Returns:
# string - Date au format MySQL avec le nombre de jours ouvrés en plus
sub subWorkingDays
{
    my ($date, $nbDays, $agency) = @_;

    my $endDate = $date;

    my $i = 0;
    while ($i < $nbDays)
    {
        $endDate = &getPreviousDay($endDate);
        if (&isWorkingDay($endDate, $agency))
        {
            $i++;
        }
    }
    return $endDate;
}


# Function: getMonthLastDayDate
# Retourne la date du dernier jour du mois
#
# Parameters:
# string $date - Date au format MySQL
#
# Returns:
# string - Date du dernier jour du mois au format MySQL
sub getMonthLastDayDate
{
    my ($date) = @_;

    return substr($date, 0, 8) . &Date::Calc::Days_in_Month(substr($date, 0, 4), substr($date, 5, 2));
}


1;