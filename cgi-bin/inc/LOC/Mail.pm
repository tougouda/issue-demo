use utf8;
use open (':encoding(UTF-8)');

# Class: LOC::Mail
# Classe d'envoi de mail
package LOC::Mail;


# Constant: SMTP_SERVER_ACCES
# Serveur SMTP
use constant SMTP_SERVER_ACCES => $ENV{'SMTP_HOST'};

# Constants: Priorités
# MAIL_PRIORITY_LOW    - Priorité de mail faible
# MAIL_PRIORITY_NORMAL - Priorité de mail normale
# MAIL_PRIORITY_HIGH   - Priorité de mail élevée
use constant
{
    MAIL_PRIORITY_LOW    => 5,
    MAIL_PRIORITY_NORMAL => 3,
    MAIL_PRIORITY_HIGH   => 1
};

# Constants: Encodage
# ENCODING_TYPE_ISO885915 - Encodage du contenu en ISO-8859-15
# ENCODING_TYPE_UTF8      - Encodage du contenu en UTF-8
use constant
{
    ENCODING_TYPE_ISO885915  => 'ISO-8859-15',
    ENCODING_TYPE_UTF8  => 'UTF-8'
};

# Constants: Format du corps du message
# BODY_TYPE_HTML - Corps du message au format HTML
# BODY_TYPE_TEXT - Corps du message au format texte brut
use constant
{
    BODY_TYPE_HTML  => 'text/html',
    BODY_TYPE_TEXT  => 'text/plain'
};

# Constants: Erreurs lors de l'envoi du mail
# ERROR_FATALERROR        - Erreur fatale
# ERROR_INVALIDRECIPIENT  - Destinataire non renseigné ou invalide
# ERROR_INVALIDSUBJECT    - Objet du mail non renseigné
# ERROR_UNKNOWNATTACHMENT - Fichier joint inexistant
use constant
{
    ERROR_FATALERROR        => 1,
    ERROR_INVALIDRECIPIENT  => 2,
    ERROR_INVALIDSUBJECT    => 3,
    ERROR_UNKNOWNATTACHMENT => 4
};


use strict;

# Modules externes
use Mail::Sender;
use MIME::QuotedPrint;
use File::Temp;
use WWW::Mechanize;
use LOC::Locale;
use LOC::Log;
use LOC::Characteristic;


# Constructor: new
# Constructeur de la classe de mail
#
# Paramètres définis par défaut :
# - le serveur SMTP : SMTP_SERVER_ACCES;
# - le type du corps du message : BODY_TYPE_HTML;
# - le type d'encodage du mail : ENCODING_TYPE_UTF8;
# - la priorité du message : MAIL_PRIORITY_NORMAL.
sub new
{
    my ($class) = @_;
    my $this = {};
    bless($this, $class);

    my $userInfos = &LOC::Session::getUserInfos();

    $this->{'_subject'} = '';                                               # sujet
    $this->{'_body'} = '';                                                  # corps
    $this->{'_sender'} = undef;                                             # envoyeur
    $this->{'_recipients'} = undef;                                         # destinataires
    $this->{'_CCRecipients'} = undef;                                       # destinataires en copie
    $this->{'_BCCRecipients'} = undef;                                      # destinataires en copie cachée
    $this->{'_smtpServer'} = LOC::Mail::SMTP_SERVER_ACCES;                  # serveur SMTP
    $this->{'_bodyType'} = LOC::Mail::BODY_TYPE_HTML;                       # type du corps du message
    $this->{'_encodingType'} = LOC::Mail::ENCODING_TYPE_UTF8;               # encodage
    $this->{'_priority'} = LOC::Mail::MAIL_PRIORITY_NORMAL;                 # priorité
    $this->{'_errors'} = '';                                                # erreurs
    $this->{'_files'} = undef;                                              # pièces jointes
    $this->{'_inlineFiles'} = undef;                                        # fichiers en-ligne
    $this->{'_credentialLogin'} = $userInfos->{'login'};                    # login (pour les pièces jointes)
    $this->{'_credentialPassword'} = $userInfos->{'password'};              # mot de passe (pour les pièces jointes)
    $this->{'_locale'} = &LOC::Locale::getLocale($userInfos->{'locale.id'}, undef, [POSIX::LC_MESSAGES, POSIX::LC_CTYPE]); # locale

    return $this;
}

# Function: setSmtpServer
# Définit le serveur SMTP
#
# Parameters:
# string $smtpServer - Le serveur SMTP
sub setSmtpServer
{
    my ($this, $smtpServer) = @_;

    $this->{'_smtpServer'} = $smtpServer;
}

# Function: setBodyType
# Définit le type du corps du message
#
# Deux valeurs possibles :
# - BODY_TYPE_HTML : corps HTML;
# - BODY_TYPE_TEXT : corps texte brut.
#
# Parameters:
# string $bodyType - Le type du corps du message
sub setBodyType
{
    my ($this, $bodyType) = @_;

    $this->{'_bodyType'} = $bodyType;
}

# Function: setEncodingType
# Définit le type d'encodage du message
#
# Deux valeurs possibles :
# - ENCODING_TYPE_ISO885915 : ISO-8859-15;
# - ENCODING_TYPE_UTF8 : UTF-8.
#
# Parameters:
# string $encodingType - Le type du corps du message
sub setEncodingType
{
    my ($this, $encodingType) = @_;

    $this->{'_encodingType'} = $encodingType;
}

# Function: addRecipient
# Ajouter un destinataire au mail
#
# Un message en cas d'erreur est ajouté et est récupérable par getErrors()
#
# Parameters:
# string $mail - Le mail du destinataire
# string $name - Le nom du destinataire (optionnel)
#
# Returns:
# bool - Retourne 1 si le destinataire est ajouté , 0 sinon
sub addRecipient
{
    my ($this, $mail, $name) = @_;

    if(!$name)
    {
        $name = $mail;
    }
    else
    {
        if ($name ne '')
        {
            $name = '"' . $this->_encodeHeader($name) . '"';
        }
        $name = $name . ' <' . $mail . '>';
    }

    if (!$this->{'_BCCRecipients'}{$mail} && !$this->{'_CCRecipients'}{$mail} && !$this->{'_recipients'}{$mail})
    {
        $this->{'_recipients'}{$mail} = $name;
        return 1;
    }
    $this->{'_errors'} .= $this->{'_locale'}->t('Ce mail est déjà dans les destinataires : %s.', $mail) . "\r\n";
    return 0;
}

# Function: addRecipients
# Ajouter plusieurs destinataires au mail
#
# Un message en cas d'erreur est ajouté et est récupérable par getErrors()
#
# Parameters:
# hash %tabRecipients - Liste des destinataires
sub addRecipients
{
    my ($this, %tabRecipients) = @_;

    foreach my $email (keys(%tabRecipients))
    {
        $this->addRecipient($email, $tabRecipients{$email});
    }
}

# Function: addCCRecipient
# Ajouter un destinataire en copie au mail
#
# Un message en cas d'erreur est ajouté et est récupérable par getErrors()
#
# Parameters:
# string $mail - Le mail du destinataire en copie
# string $name - Le nom du destinataire en copie (optionnel)
#
# Returns:
# bool - Retourne 1 si le destinataire en copie est ajouté , 0 sinon
sub addCCRecipient
{
    my ($this, $mail, $name) = @_;

    if(!$name)
    {
        $name = $mail;
    }
    else
    {
        if ($name ne '')
        {
            $name = '"' . $this->_encodeHeader($name) . '"';
        }
        $name = $name . ' <' . $mail . '>';
    }

    if (!$this->{'_BCCRecipients'}{$mail} && !$this->{'_CCRecipients'}{$mail} && !$this->{'_recipients'}{$mail})
    {
        $this->{'_CCRecipients'}{$mail} = $name;
        return 1;
    }
    $this->{'_errors'} .= $this->{'_locale'}->t('Ce mail est déjà dans les destinataires : %s.', $mail) . "\r\n";
    return 0;
}

# Function: addBCCRecipient
# Ajouter un destinataire en copie cachée au mail
#
# Un message en cas d'erreur est ajouté et est récupérable par getErrors()
#
# Parameters:
# string $mail - Le mail du destinataire en copie cachée
# string $name - Le nom du destinataire en copie cachée (optionnel)
#
# Returns:
# bool - Retourne 1 si le destinataire en copie cachée est ajouté , 0 sinon
sub addBCCRecipient
{
    my ($this, $mail, $name) = @_;

    if(!$name)
    {
        $name = $mail;
    }
    else
    {
        if ($name ne '')
        {
            $name = '"' . $this->_encodeHeader($name) . '"';
        }
        $name = $name . ' <' . $mail . '>';
    }

    if (!$this->{'_BCCRecipients'}{$mail} && !$this->{'_CCRecipients'}{$mail} && !$this->{'_recipients'}{$mail})
    {
        $this->{'_BCCRecipients'}{$mail} = $name;
        return 1;
    }
    $this->{'_errors'} .= $this->{'_locale'}->t('Ce mail est déjà dans les destinataires : %s.', $mail) . "\r\n";
    return 0;
}

# Function: addStringAttachment
# Ajouter un fichier en pièce jointe par son contenu
#
# Un message en cas d'erreur est ajouté et est récupérable par getErrors()
#
# Parameters:
# string $content  - Contenu du fichier
# string $fileName - le nom du fichier (AVEC L'EXTENSION)
#
# Returns:
# bool - 1
sub addStringAttachment
{
    my ($this, $content, $fileName) = @_;

    $this->{'_filesContents'}{$fileName} = $content;

    return 1;
}

# Function: addFileAttachment
# Ajouter un fichier en pièce jointe
#
# Un message en cas d'erreur est ajouté et est récupérable par getErrors()
#
# Parameters:
# string $url      - L'url du fichier
# string $fileName - le nom du fichier (AVEC L'EXTENSION)
#
# Returns:
# bool - 1 si la récupération du fichier est faite, 0 sinon
sub addFileAttachment
{
    my ($this, $url, $fileName) = @_;

    unless ($fileName)
    {
        $fileName = basename($url);
    }
    my($fh, $tempfile) = &File::Temp::tempfile(UNLINK => 1);
    my $mech = WWW::Mechanize->new(autocheck => 0);
    $mech->credentials($this->{'_credentialLogin'}, $this->{'_credentialPassword'});
    my $resGet = $mech->get($url);
    unless ($mech->success())
    {
        $this->{'_errors'} .= $this->{'_locale'}->t('Pièce jointe introuvable : %s.', $url) . "\r\n";
        return 0;
    }
    $mech->save_content($tempfile);
    $this->{'_files'}{$fileName} = $tempfile;
    return 1;
}

# Function: addLocalFileAttachment
# Ajouter un fichier local en pièce jointe
#
# Un message en cas d'erreur est ajouté et est récupérable par getErrors()
#
# Parameters:
# string $path     - Le chemin complet vers le fichier
# string $fileName - le nom du fichier (AVEC L'EXTENSION)
#
# Returns:
# bool - 1 si la récupération du fichier est faite, 0 sinon
sub addLocalFileAttachment
{
    my ($this, $path, $fileName) = @_;

    unless (-e $path)
    {
        $this->{'_errors'} .= $this->{'_locale'}->t('Pièce jointe introuvable : %s.', $path) . "\r\n";
        return 0;
    }
    unless ($fileName)
    {
        $fileName = basename($path);
    }
    $this->{'_files'}{$fileName} = $path;
    return 1;
}

# Function: addInlineAttachment
# Ajouter un fichier pour utilisation dans le corps du mail
#
# Un message en cas d'erreur est ajouté et est récupérable par getErrors()
#
# Parameters:
# string $url      - L'url du fichier
# string $fileName - le nom du fichier (AVEC L'EXTENSION)
#
# Returns:
# bool - 1 si la récupération du fichier est faite, 0 sinon
sub addInlineAttachment
{
    my ($this, $url, $fileName) = @_;

    unless ($fileName)
    {
        $fileName = basename($url);
    }
    my($fh, $tempfile) = &File::Temp::tempfile(UNLINK => 1);
    my $mech = WWW::Mechanize->new(autocheck => 0);
    $mech->credentials($this->{'_credentialLogin'}, $this->{'_credentialPassword'});
    my $resGet = $mech->get($url);
    unless ($mech->success())
    {
        $this->{'_errors'} .= $this->{'_locale'}->t('Fichier introuvable : %s.', $url) . "\r\n";
        return 0;
    }
    $mech->save_content($tempfile);
    $this->{'_inlineFiles'}{$fileName} = $tempfile;
    return 1;
}

# Function: setBody
# Définit le contenu du corps du mail
#
# Parameters:
# string $body - Le corps du mail
sub setBody
{
    my ($this, $body) = @_;

    $this->{'_body'} = $body;
}

# Function: setSubject
# Définit le sujet du mail
#
# Parameters:
# string $subject - Le sujet du mail
sub setSubject
{
    my ($this, $subject) = @_;

    $this->{'_subject'} = $subject;
}

# Function: setSender
# Définit l'émetteur du mail
#
# Parameters:
# string $mail - Le mail de l'émetteur
# string $name - Le nom de l'émetteur (optionnel)
sub setSender
{
    my ($this, $mail, $name) = @_;

    if (!$name)
    {
        $name = $mail;
    }
    else
    {
        if ($name ne '')
        {
            $name = '"' . $this->_encodeHeader($name) . '"';
        }
        $name = $name . ' <' . $mail . '>';
    }
    $this->{'_sender'}{$mail} = $name;
}

# Function: send
# Envoie le mail
#
# Un message en cas d'erreur est ajouté et est récupérable par getErrors()
#
# Returns:
# bool - Retourne 1 si le mail est envoyé , 0 sinon
sub send
{
    my ($this) = @_;

    my $stringTo = '';
    my $stringCC = '';
    my $stringBCC = '';
    my @tabRecipients = ();
    my $stringHeaders = '';

    # si il n'y a pas d'expéditeur
    unless ($this->{'_sender'})
    {
        $this->{'_errors'} .= $this->{'_locale'}->t('Il n\'y a pas d\'expéditeur.') . "\r\n";
        return 0;
    }

    # si il n'y a pas de destinataire
    if (!$this->{'_recipients'})
    {
        $this->{'_errors'} .= $this->{'_locale'}->t('Il n\'y a pas de destinataire.') . "\r\n";
        return 0;
    }
    else
    {
        $stringTo = join(', ', values(%{$this->{'_recipients'}}));
        push(@tabRecipients, keys(%{$this->{'_recipients'}}));
    }

    # Vérification de la taille des pièces jointes
    my $attachmentsSize = 0;
    my $maxAttachmentsSize = &LOC::Characteristic::getGlobalValueByCode('MAXTAILLEDOCMAIL') * 1;
    if ($this->{'_filesContents'})
    {
        foreach my $content (values %{$this->{'_filesContents'}})
        {
            $attachmentsSize += length($content);
        }
    }
    if ($this->{'_files'})
    {
        foreach my $path (values %{$this->{'_files'}})
        {
            $attachmentsSize += -s $path;
        }
    }
    if ($attachmentsSize > $maxAttachmentsSize)
    {
        $this->{'_errors'} .= $this->{'_locale'}->t('La taille des pièces jointes est supérieure à %s', $this->{'_locale'}->getBytesFormat($maxAttachmentsSize, 0)) . "\r\n";
        return 0;
    }


    # Headers pour la priorité et la confirmation
    $stringHeaders .= 'X-Priority: ' . $this->{'_priority'};

    # X-Mailer
    $Mail::Sender::NO_X_MAILER = 1;

    # Création du mail
    my @tabSenderName = values(%{$this->{'_sender'}});
    my $sender = new Mail::Sender{smtp    => $this->{'_smtpServer'},
                                  from    => $tabSenderName[0],
                                  headers => $stringHeaders};
    if ($sender < 0)
    {
        $this->{'_errors'} .= $this->{'_locale'}->t('Erreur sur la création du mail : code %s.', $sender). "\r\n";
        $sender->Cancel();
    }
    # Destinataires en copie
    if($this->{'_CCRecipients'})
    {
        $stringCC = join(', ', values(%{$this->{'_CCRecipients'}}));
        push(@tabRecipients, keys(%{$this->{'_CCRecipients'}})); # Pour le log
    }

    # Destinataires en copie cachée
    if($this->{'_BCCRecipients'})
    {
        $stringBCC = join(', ', values(%{$this->{'_BCCRecipients'}}));
        push(@tabRecipients, keys(%{$this->{'_BCCRecipients'}})); # Pour le log
    }

    my $subject = $this->{'_subject'};
    my $env = &LOC::Globals::getEnv();
    if ($env ne 'exp')
    {
        $subject = '[' . uc($env) . '] ' . $subject;
    }
    $subject = $this->_encodeHeader($subject);
    $sender->OpenMultipart({subject => $subject, to => $stringTo, cc => $stringCC, bcc => $stringBCC, multipart => 'mixed'});

    # Corps
	my $resBody = $sender->Body({
	    'charset'  => $this->{'_encodingType'},
	    'encoding' => 'base64',
	    'ctype'    => $this->{'_bodyType'} . '; charset=' . $this->{'_encodingType'},
	    'msg'      => $this->_encodeBody($this->{'_body'})
	});
    if ($resBody < 0)
    {
        $this->{'_errors'} .= $this->{'_locale'}->t('Erreur sur le corps du mail : code %s.', $resBody) . "\r\n";
        $sender->Cancel();
        return 0;
    }

    # Fichiers attachés par contenu
    if ($this->{'_filesContents'})
    {
        foreach my $key (sort keys %{$this->{'_filesContents'}})
        {
            my $resFile = $sender->Part({ctype => 'application/octet-stream',
                                         encoding => 'Base64',
                                         disposition => 'attachment; filename="' . $this->_encodeBody($key) . '"',
                                         msg => $this->{'_filesContents'}->{$key}});
            if ($resFile < 0)
            {
                $this->{'_errors'} .= $this->{'_locale'}->t('Erreur sur une pièce jointe : code %s.', $resFile) . "\r\n";
                $sender->Cancel();
                return 0;
            }
        }
    }

    # Fichiers attachés
    if ($this->{'_files'})
    {
        foreach my $key (sort keys %{$this->{'_files'}})
        {
            my $resFile = $sender->SendFile({file => $this->{'_files'}->{$key},
                                             disposition => 'attachment; filename="' . $this->_encodeBody($key) . '"'});
            if ($resFile < 0)
            {
                $this->{'_errors'} .= $this->{'_locale'}->t('Erreur sur une pièce jointe : code %s.', $resFile) . "\r\n";
                $sender->Cancel();
                return 0;
            }
        }
    }

    # Fichiers en-ligne
    if($this->{'_inlineFiles'})
    {
        foreach my $key (keys %{$this->{'_inlineFiles'}})
        {
            my $resFile = $sender->SendFile({file => $this->{'_inlineFiles'}->{$key},
                                             content_id => $key});
            if ($resFile < 0)
            {
                $this->{'_errors'} .= $this->{'_locale'}->t('Erreur sur un fichier en ligne : code %s.', $resFile) . "\r\n";
                $sender->Cancel();
                return 0;
            }
        }
    }

    $sender->Close();

    # ajout d'une trace dans les logs pour chaque destinataire
    my @tabSenderMail = keys(%{$this->{'_sender'}});
    my $logLine;
    foreach (@tabRecipients)
    {
        $logLine = substr($tabSenderMail[0] . '                              ', 0, 50);
        $logLine .= '   ' . substr($_ . '                                        ', 0, 60);
        $logLine .= '   ' . substr($this->{'_subject'} . '                                        ', 0, 40) . '   ';
        foreach my $key (sort keys %{$this->{'_files'}})
        {
            $logLine .= substr($key . '                                        ', 0, 30) . ' ';
        }
        &LOC::Log::writeFileLog('mail', $logLine);
    }
    return 1;
}

# Function: setMailPriority
# Définit la priorité du mail
#
# Trois valeurs possibles :
# - MAIL_PRIORITY_LOW : priorité faible;
# - MAIL_PRIORITY_NORMAL : priorité normale;
# - MAIL_PRIORITY_HIGH : priorité haute.
#
# Parameters:
# string $priority - La priorité du mail
sub setMailPriority
{
    my ($this, $priority) = @_;

    $this->{'_priority'} = $priority;
}

# Function: getErrors
# Récupère les différentes erreurs générées
#
# Codes d'erreurs de retour de mail Sender (codes affichées dans les messages d'erreurs) :
# - -1 = $smtphost unknown
# - -2 = socket() failed
# - -3 = connect() failed
# - -4 = service not available
# - -5 = unspecified communication error
# - -6 = local user $to unknown on host $smtp
# - -7 = transmission of message failed
# - -8 = argument $to empty
# - -9 = no message specified in call to MailMsg or MailFile
# - -10 = no file name specified in call to SendFile or MailFile
# - -11 = file not found
# - -12 = not available in singlepart mode
# - -13 = site specific error
# - -14 = connection not established. Did you mean MailFile instead of SendFile?
# - -15 = no SMTP server specified
# - -16 = no From: address specified
# - -17 = authentication protocol not accepted by the server
# - -18 = login not accepted
# - -19 = authentication protocol is not implemented
#
# Returns:
# string - Les erreurs si il y en a avec retour à la ligne entre chaque
sub getErrors
{
    my ($this) = @_;

    return $this->{'_errors'};
}

# Function: resetErrors
# Efface les erreurs
sub resetErrors
{
    my ($this) = @_;

    $this->{'_errors'} = '';
}

# Function: _encodeHeader
# Encoder le contenu d'une entête (si il contient des caractères non imprimables)
#
# Parameters:
# string $str - La chaîne à encoder
sub _encodeHeader
{
    my ($this, $str) = @_;

    my $quotedValue = &MIME::QuotedPrint::encode_qp(&LOC::Util::encodeUtf8($str), '');
    if ($quotedValue eq $str)
    {
        return $str;
    }
    $quotedValue =~ s/\?/=3F/g;
    $quotedValue =~ s/ /=20/g;
    my $result = '=?' . $this->{'_encodingType'} . '?Q?' . $quotedValue . '?=';

    return $result;
}

# Function: _encodeBody
# Encoder le contenu du corps de l'e-mail
#
# Parameters:
# string $str - La chaîne à encoder
sub _encodeBody
{
    my ($this, $str) = @_;

    return &LOC::Util::encodeUtf8($str);
}


1;