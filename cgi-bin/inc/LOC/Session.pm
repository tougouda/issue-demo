use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Session
# Module de gestion des sessions
package LOC::Session;
use strict;

use LOC::Agency;
use LOC::User;
use LOC::Db;
use LOC::Util;

my %session;
my %user;


# Function: getInfos
# Retourne un hachage contenant les informations de session
#
# Returns:
# hash|hashref -
sub getInfos
{
    if (wantarray)
    {
        return %session;
    }
    else
    {
        return \%session;
    }
}

# Function: getUserInfos
# Retourne un hachage contenant des informations sur l'utilisateur ayant ouvert la session
#
# Returns:
# hash|hashref -
sub getUserInfos
{
    my ($reload) = @_;

    if (!%user || $reload)
    {
        %user = &LOC::User::getInfos($session{'login'});
    }

    if (wantarray)
    {
        return %user;
    }
    else
    {
        return \%user;
    }
}


# Function: getMenuList
# Retourne la liste des menus de l'utilisateur
#
# Contenu du hachage:
# - parent.id     => id du menu principal (logiciel)
# - parent.label  => libellé du menu principal
# - parent.dir    => répertoire du menu principal
# - parent.order  => ordre d'affichage du menu principal
# - id            => id du menu
# - label         => libellé du menu
# - url           => url du menu
# - order         => ordre d'affichage du menu
#
# Returns:
# array -
sub getMenuList
{
    my @menus = ();
    
    if (%user)
    {
        my $db = &LOC::Db::getConnection('location', $user{'nomadCountry.id'});

        my $query = '
SELECT
    L.LOAUTO AS `parent.id`,
    L.LONOM AS `parent.label`,
    L.LODIR AS `parent.dir`,
    (
        SELECT
            MIN(MP2.PROFILORDRE)
        FROM AUTH.MEPR MP2
            LEFT JOIN MENU M2 ON M2.MEAUTO = MP2.MEAUTO
        WHERE M2.LOAUTO = M.LOAUTO AND
            MP2.PROFILAUTO = MP.PROFILAUTO
    ) AS `parent.order`,
    M.MEAUTO AS `id`,
    M.MEFONCTION AS `label`,
    M.MEFICHIER AS `url`,
    MP.PROFILORDRE AS `order`
FROM AUTH.MEPR MP
INNER JOIN MENU M
ON M.MEAUTO = MP.MEAUTO
INNER JOIN LOGICIEL L
ON L.LOAUTO = M.LOAUTO
WHERE MP.PROFILAUTO = ' . $user{'profile.id'} . '
ORDER BY `parent.order`, `parent.label`, `order`, `label`;';
        my $rMenus = $db->fetchAssoc($query);
        @menus = @$rMenus;
    }
    
    return @menus;
}

# Function: getRightList
# Retourne la liste des droits de l'utilisateur
#
# Contenu du hachage:
# - id          => id
# - module      => nom du module
# - controller  => nom du controller
# - action      => intitulé de l'action
# - parameters  => paramètres supplémentaires
#
# Returns:
# array -
sub getRightList
{
    my @rights = ();

    if (%user)
    {
        my $db = &LOC::Db::getConnection('auth');

        my $query = '
SELECT
    rgt_id AS `id`,
    rgt_module AS `module`,
    rgt_controller AS `controller`,
    rgt_action AS `action`,
    rgt_parameters AS `parameters`
FROM p_right_profile
INNER JOIN p_right
ON rgt_id = rip_rgt_id
WHERE rip_prf_id = ' . $user{'profile.id'} . ';';
        my $rRights = $db->fetchAssoc($query);
        @rights = @$rRights;
    }

    return @rights;
}

# Function: isAccessAuthorized
# Retourne 1 si une personne a le droit d'accéder à une fonctionnalité
# 
# Parameters:
# string $module     - nom du module
# string $controller - nom du contrôleur
# string $action     - nom de l'action
# arrayref $rParam   - référence vers un tableau de droits supplémentaires (optionel)
#
# Returns:
# int -
sub isAccessAuthorized
{
    my ($module, $controller, $action, $rParam) = @_;
    
    # valeur de retour
    my $ok = 0;
    
    # initialisation du tableau des droits supplémentaires
    my @parameters;
    if (defined $rParam)
    {
        @parameters = @$rParam;
    }
    else
    {
        @parameters = ();
    }

    # Récupère la liste des droits de l'utilsateur
    my @rights = &getRightList();
    my @authParameters;
    # Boucle sur les droits jusqu'à trouver une correspondance
    foreach my $right (@rights)
    {
        if ($right->{'module'} eq $module && (!$right->{'controller'} || $right->{'controller'} eq $controller))
        {
            if (!$right->{'action'} || $right->{'action'} eq $action)
            {
                # s'il n'y a pas de droits supplémentaires
                if (!@parameters)
                {
                    $ok = 1;
                    last;
                }
                else
                {
                    # liste des droits définis sur le profil
                    @authParameters = split(/;/, $right->{'parameters'});
                    if (@authParameters)
                    {
                        $ok = 1;
                        # vérifie pour chaque droit demandé si la personne est autorisée
                        foreach my $param (@parameters)
                        {
                            if (!&LOC::Util::in_array($param, \@authParameters))
                            {
                                $ok = 0;
                                last;
                            }
                        }
                    }
                }
            }
        }
    }

    return $ok;
}

# Function load
# Ouvre ou prolonge une session utilisateur
#
# Returns:
# int - code erreur : 1 si OK, -1 si double connexion 
sub load
{
    $session{'login'} = &_getLogin();
    my $ip = &_getIp();
    if ($ip != $session{'ip'})
    {
        $session{'ip'} = $ip;
        $session{'physicalAgency'} = &LOC::Agency::getByIP($session{'ip'});
        my $agencyObj = &LOC::Agency::getInfos($session{'physicalAgency'});
        $session{'timeZone'} = $agencyObj->{'timeZone'};
    }
    

    # récupère les paramètres de connexion à AUTH
    my $db = &LOC::Db::getConnection('auth');

    # vérifie si il y a déjà une session en cours
    my $query = '
SELECT
    SESSIONLOGIN,
    SESSIONIP,
    SESSIONDD,
    SESSIONDF,
    SESSIONAUTO
FROM AUTH.SESSION
WHERE SESSIONLOGIN = "' . $session{'login'} . '"
AND NOW() BETWEEN SESSIONDD AND SESSIONDF;';
    my $dataRef = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    if ($dataRef)
    {
        my %data = %$dataRef;

        # prolonge la session
        $session{'id'} = $data{'SESSIONAUTO'};
        &_continue();
    }
    # crée une nouvelle session
    elsif ($session{'ip'} ne $ENV{'SERVER_ADDR'})
    {
        &_create();
    }

    # Charge les infos utilisateur
    &getUserInfos();

    return 1;
}

# Function: unload
# Clôt une session utilisateur
sub unload
{
    &_close();
}


# Function: _create
# Crée la session en BDD
sub _create
{
    my $db = &LOC::Db::getConnection('auth');

    my $query = '
INSERT INTO SESSION (SESSIONLOGIN, SESSIONIP, SESSIONDD, SESSIONDF)
VALUES ("' . $session{'login'} . '", "' . $session{'ip'} . '", NOW(), DATE_ADD(NOW(), INTERVAL 330 SECOND));';
    $db->query($query);
}

# Function: _continue
# Prolonge la session en BDD
sub _continue
{
    my $db = &LOC::Db::getConnection('auth');

    my $query = '
UPDATE SESSION
SET SESSIONDF = DATE_ADD(NOW(), INTERVAL 330 SECOND)
WHERE SESSIONAUTO = ' . $session{'id'} . ';';
    $db->query($query);
}

# Function: _close
# Clôt la session en BDD
sub _close
{
    my $db = &LOC::Db::getConnection('auth');

    my $query = '
UPDATE SESSION
SET SESSIONDF = NOW()
WHERE SESSIONAUTO = ' . $session{'id'} . ';';
    $db->query($query);
}

# Function: _getLogin
# Retourne le login de session
#
# Returns:
# string -
sub _getLogin
{
    return &LOC::Util::trim($ENV{'REMOTE_USER'});
}

# Function: _getIp
# Retourne l'ip de la session
#
# Returns:
# string -
sub _getIp
{
    if ($ENV{'HTTP_X_FORWARDED_FOR'})
    {
        return $ENV{'HTTP_X_FORWARDED_FOR'};
    }
    else
    {
        return $ENV{'REMOTE_ADDR'};
    }
}

1;