use utf8;
use open (':encoding(UTF-8)');

# Class: LOC::Sms
# Module d'envoi de SMS
package LOC::Sms;


# Constants: Erreurs
# ERROR_FATAL                - Erreur fatale
# ERROR_NORECEIVER           - Liste des destinataires vide
# ERROR_NONMOBILEPHONENUMBER - Numéro de téléphone non mobile
use constant
{
    ERROR_FATAL                => 0x0001,

    ERROR_NORECEIVER           => 0x0100,
    ERROR_NONMOBILEPHONENUMBER => 0x0200
};


use strict;

# Modules internes
use LOC::Globals;
use SOAP::Lite;


my $fatalErrMsg = 'Erreur fatale: module LOC::Sms, fonction %s(), ligne %d, fichier "%s"%s.' . "\n";



# Function: send
# Envoi un sms
#
# Parameters:
# string   $message      - Message
# arrayref $tabReceivers - Liste des destinataires
# hashref  $tabOptions   - Options supplémentaires (facultatif)
# hashref  $tabErrors    - Tableau d'erreurs
#
# Returns:
# hashref - Liste des identifiants des SMS envoyés
sub send
{
    my ($message, $tabReceivers, $tabOptions, $tabErrors) = @_;

    if (!defined $tabOptions || ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    $tabErrors->{'fatal'} = [];

    # Liste des destinataires vide
    if (@$tabReceivers == 0)
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_NORECEIVER);
        return 0;
    }

    # Destinataires
    my @tabSoapReceivers = ();
    foreach my $receiver (@$tabReceivers)
    {
        push(@tabSoapReceivers, SOAP::Data->value($receiver)->type('string'));
    }

    # Envoi du SMS
    my $servicesWs = &LOC::Globals::get('servicesWs');

    my $soap = SOAP::Lite->uri($servicesWs->{'sms'}->{'uri'})
                         ->proxy($servicesWs->{'sms'}->{'location'});

    my $som = $soap->call('sendSMS',
        SOAP::Data->name('message' => $message),    # Ne pas préciser le type "string"
                                                     # pour éviter les problèmes d'encodage
        SOAP::Data->name('receivers' => \@tabSoapReceivers)
    );

    my $fault = $som->fault();
    if ($fault)
    {
        # Numéro de téléphone non mobile
        if ($fault->{'faultcode'} eq 'NonMobilePhoneNumber')
        {
            printf STDERR ('Envoi de SMS : [%s] %s', $fault->{'faultcode'}, $fault->{'faultstring'});
            push(@{$tabErrors->{'fatal'}}, ERROR_NONMOBILEPHONENUMBER);
        }
        # Erreur fatale
        else
        {
            print STDERR sprintf($fatalErrMsg, 'send', __LINE__, __FILE__, '');
            printf STDERR ('[%s] %s', $fault->{'faultcode'}, $fault->{'faultstring'});
            push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
        }
        return 0;
    }

    return $som->result();
}

1;