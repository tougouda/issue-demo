#!/usr/bin/perl

use utf8;    # Tout le code du scope est considéré comme encodé en UTF-8
use open (':encoding(UTF-8)');

# Class: LOC::Browser
# Module gérant les interactions avec le navigateur web
package LOC::Browser;

use strict;

use CGI::Carp qw(fatalsToBrowser warningsToBrowser);
use Data::Dumper;
use JSON::XS;
use MIME::Base64;
use IPC::Shareable;


# Constants: En-têtes HTTP
# HTTP_HEADERS - La liste des en-têtes HTTP.
use constant
{
    HTTP_HEADERS => 'Content-Type|Expires|X-UA-Compatible|Status|Location'
};


# Indique si les en-têtes HTTP ont été envoyés ou non.
our $httpHeadersSent = 0;

# Variable utilisable dans les processus fils (via la mémoire partagée)
my $handle;

# Processus fils pour la sortie standard
my $childPid;


# Function: BEGIN
# La fonction exécutée au début du chargement du module.
BEGIN
{
    # Affichage des erreurs dans le navigateur et envoi d'un mail + error.log
    my $sender = $ENV{'ERRORS_SENDER'};
    if ($sender =~ /(.+?) +<(.+?)>/)
    {
        $sender = $2;
    }

    my $recipient = $ENV{'ERRORS_RECIPIENTS'};
    if ($recipient =~ /(.+?) +<(.+?)>/)
    {
        $recipient = $2;
    }
    if (defined $ENV{'DEV'})
    {
        $recipient = $ENV{'DEV'} . '@acces-industrie.com';
    }


    # Function: getLocParams
    # Retourne, sous forme de chaîne, les paramètres de la page passés en URL.
    #
    # Returns:
    # string - Les paramètres passés en URL.
    sub getLocParams
    {
        if ($ENV{'QUERY_STRING'} =~ /locParams=(.+?)(?:&|$)/)
        {
            my $params = $1;

            # URL decode
            $params =~ tr/+/ /;
            $params =~ s/%([a-fA-F0-9]{2,2})/chr(hex($1))/eg;
            $params =~ s/<!–(.|\n)*–>//g;

            # Décodage base 64
            $params = &MIME::Base64::decode_base64($params);

            # Décodage JSON
            $params = JSON::XS->new->allow_nonref(1)->decode($params);

            return &Data::Dumper::Dumper($params) . "\n";
        }

        return '';
    }


    # Function: handleErrors
    # Gère les erreurs.
    sub handleErrors
    {
        my $error = shift;

        # Afficher dans le navigateur
        print '<!DOCTYPE html>';
        print '<html>';
        print '<head>';
        print '<meta charset="UTF-8">';
        print '</head>';
        print '<body>';
        print '<h1>Une erreur a été détectée</h1>';
        print '<p>Veuillez contacter le 5000 en précisant le message d’erreur suivant :</p>';
        print '<code style="white-space: pre;">' . $error . '</code>';
        print '</body>';
        print '</html>';

        # Remplacement des entités HTML
        $error=~s/&amp;/&/g;
        $error=~s/&gt;/>/g;
        $error=~s/&lt;/</g;
        $error=~s/&quot;/"/g;

        # Envoi par e-mail
        my $subject = 'Gesloc - Erreur Perl';
        if ($ENV{'ENV'} ne 'exp')
        {
            $subject = sprintf('[%s] %s', uc($ENV{'ENV'}), $subject);
        }
        my $message = '';
        $message .= 'Adresse IP du client : ' . ($ENV{'HTTP_X_FORWARDED_FOR'} ? $ENV{'HTTP_X_FORWARDED_FOR'} : $ENV{'REMOTE_ADDR'}) . "\n";
        $message .= 'URL : ' . $ENV{'LOC_URL'} . '/cgi-bin/index.cgi?' . $ENV{'QUERY_STRING'} . "\n";
        $message .= "Paramètres : " . &getLocParams() . "\n";
        $message .= 'L’erreur suivante a été détectée :' . "\n\n" . $error;

        open (EMAIL,"| /usr/sbin/sendmail $recipient");
        print EMAIL "To: $recipient\n";
        print EMAIL "From: $sender\n";
        print EMAIL "Subject: $subject\n";
        print EMAIL "Content-Type: text/plain; charset=\"UTF-8\"\n\n";
        print EMAIL "$message\n";
        print EMAIL "\n";
        close (EMAIL);
    }
    &CGI::Carp::set_message(\&handleErrors);
}


# Function: checkHttpHeaders
# Vérifie si des en-têtes HTTP sont envoyés au navigateur.
# Pour ce faire, on ouvre la sortie standard dans un processus fils et on filtre l'entrée standard.
# Si on détecte que des en-têtes HTTP sont envoyés à la sortie standard,
# la variable $LOC::Browser::headersSent est mise à 1. Cette variable est utilisable dans toute l'application.
# Voir Perl Cookbook 16.1.
sub checkHttpHeaders
{
    # Partage de la variable $httpHeadersSent entre les processus père et fils
    $handle = tie($httpHeadersSent, 'IPC::Shareable', $$, { create => 1 });
    $SIG{INT} = sub { die "$$ dying\n" };

    # Création du fils
    $childPid = open(STDOUT, '|-');

    # Erreur
    if (!defined $childPid)
    {
        die "Impossible de créer un processus fils dans la méthode checkHttpHeaders : $!";
    }
    # Fin du père
    if ($childPid)
    {
        return;
    }

    # Flux bruts sur les entrée/sortie standards
    binmode STDIN, ":raw";
    binmode STDOUT, ":raw";

    # Filtrage de STDIN
    # On passe par un open car on avait des problèmes avec les caractères UTF-8 avec "while (<STDIN>)"
    open(my $STDIN, '<&STDIN');
    local $/ = undef;
    while (<$STDIN>)
    {
        # Un des en-têtes recherchés est détecté
        if (!$httpHeadersSent && $_ =~ m/^(${\(HTTP_HEADERS)}):/i)
        {
            &setHttpHeadersSent();
        }
        # Écriture sur STDOUT
        print $_;
    }
    close $STDIN;

    exit;
}


# Function: setHttpHeadersSent
# Définit le fait que les en-têtes HTTP ont été envoyés.
sub setHttpHeadersSent
{
    if (defined $handle)
    {
        $handle->shlock();
    }

    $httpHeadersSent = 1;

    if (defined $handle)
    {
        $handle->shunlock();
    }
}


# Function: cleanIpc
# Supprime la variable partagée.
sub cleanIpc
{
    if (defined $handle)
    {
        $handle->remove();
        $handle = undef;

        IPC::Shareable->clean_up();
        IPC::Shareable->clean_up_all();
    }
}


# Function: getDisplayableHeaders
# Afficher des en-têtes HTTP.
#
# Parameters:
# string $headers  - Les en-têtes HTTP à afficher.
#
# Returns:
# string - Une chaîne de caractères contenant les en-têtes HTTP qui seront réellement affichés.
sub getDisplayableHeaders
{
    my ($headers) = @_;

    return ($httpHeadersSent ? '' : $headers);
}


# Function: sendHeaders
# Envoie des en-têtes au navigateur.
#
# Parameters:
# string $headers  - Les en-têtes à envoyer au navigateur.
sub sendHeaders
{
    my ($headers) = @_;

    print &getDisplayableHeaders($headers);
}


# Function: init
# Initialise le navigateur.
sub init
{
    # Vérification des en-têtes HTTP
    &checkHttpHeaders();

    # Affichage des warnings en commentaires HTML + error.log
    &CGI::Carp::warningsToBrowser(1);
}


# Function: END
# La fonction exécutée à la fin de l'exécution du programme.
sub END
{
    # Dans le cas où on est dans le processus père
    if (!$childPid)
    {
        # On fait le ménage dans la mémoire partagée et les sémaphores
        &cleanIpc();
    }
}

1;
