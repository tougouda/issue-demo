use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Timer
# Module de fonctions de chronométrage
package LOC::Timer;
use strict;

use Time::HiRes;


# Function: start
# Initialisation du chrono
#
# Returns:
# arrayref - Tableau contenant le nombre de secondes et le nombre de microsecondes courants
sub start
{
    return [&Time::HiRes::gettimeofday()];
}


# Function: end
# Arrêt du chrono
#
# Parameters
# arrayref - Tableau contenant le nombre de secondes et le nombre de microsecondes de l'instant de départ
#
# Returns:
# float - Temps écoulé
sub end
{
    my ($timer) = @_;

    return &Time::HiRes::tv_interval($timer, [&Time::HiRes::gettimeofday()]);
}

1;