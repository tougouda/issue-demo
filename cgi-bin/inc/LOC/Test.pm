use utf8;
use open (':encoding(UTF-8)');

# Class: LOC::Test
# Module de fonctions utilitaires
package LOC::Test;

# Constants: Informations sur les fichiers de test
# DEFAULT_FILEEXTENSION  - extension *.t
# DEFAULT_DIRECTORY      - répertoire où sont situés les fichiers de test par défaut
use constant {
    DEFAULT_FILEEXTENSION     => '.t',
    DEFAULT_DIRECTORY         => '/test/',
    DEFAULT_SUBROUTINEPATTERN => qr/Test$/
};

# Constants: Type de test
# TYPE_IS    - Compare 2 valeurs
# TYPE_DEEP  - Compare 2 structures de données (compare des références)
use constant {
    TYPE_IS   => 1,
    TYPE_DEEP => 2
};


use strict;

use Test::More;
use Test::Deep;
use File::Find;
use File::Basename;
use LOC::Globals;


# Function: exec
# Exécute une liste de tests unitaires
#
# Parameters: 
# arrayref  $tabFiles  - Tableau des fichiers tests à exécuter
#    chaque entrée contient un hashref
#    {'path'    => 'chemin du fichier',
#     'package' => 'nom du package du module de test'}
#
sub exec
{
    my ($tabFiles) = @_;


    if (!defined $tabFiles)
    {
        my @files;
        my @tabRoutines;

        my $directory = &LOC::Globals::get('cgiPath') . DEFAULT_DIRECTORY;
        find({'wanted' => \&wanted, 'follow' => 1}, $directory);

        # - Parcours de l'arborescence
        sub wanted {
            my $file = $_;
            my ($fileName, $filePath, $extension) = fileparse($File::Find::name, '\.[^\.]*');
            # - Récupération des fichiers tests
            if (-f $file && $extension eq DEFAULT_FILEEXTENSION)
            {
                my $packageName = 'Test::' . substr($filePath . $fileName, length $directory);
                $packageName =~ s/\//::/g;
                push(@files, {'path' => $File::Find::name, 'package' => $packageName});
            }
        }

        $tabFiles = \@files;
    } 

    # - Parcours des fichiers tests
    foreach my $fileInfos (@$tabFiles)
    {
        require $fileInfos->{'path'};
        no strict 'refs';

        print '
---------------------
-- ' . $fileInfos->{'path'} . '
---------------------';

        # - Recherche et lancement des routines de test
        foreach my $subroutine (keys(%{$fileInfos->{'package'} . '::'}))
        {
            if ($subroutine =~ DEFAULT_SUBROUTINEPATTERN)
            {
                my $sub = $fileInfos->{'package'} . '::' . $subroutine;
                subtest ' ' => sub {
                    &$sub();
                };
            }
        }

        print "\n";
    }
    done_testing();
}


# Function: printTestLabel
# Écrit le libellé d'un test
#
# Parameters:
# string $label   - Libellé
sub printTestLabel
{
    my ($label) = @_;

    $label =
"---
-> " . $label . "
---\n";

    print $label;
}


# Function: test
# Effectue les cas de test donnés
#
# Parameters:
# string   $title            - Titre du test
# hashref  $tabDefaultParams - Tableau des paramètres par défaut
# arrayref $tabCases         - Tableau des cas de test
# subref   $got              - Routine à lancer pour obtenir le résultat à comparer
# int      $test             - Type de test à lancer
sub test
{
    my($title, $tabDefaultParams, $tabCases, $got, $test, $tabOptions) = @_;

    if (!defined $tabDefaultParams)
    {
        $tabDefaultParams = {};
    }
    if (!defined $tabCases)
    {
        $tabCases = [];
    }
    if (!defined $test)
    {
        $test = TYPE_IS;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    &printTestLabel($title);

    foreach my $case (@$tabCases)
    {
        my %tab = %$tabDefaultParams;
        foreach my $prop (keys(%{$case->{'props'}}))
        {
            $tab{$prop} = $case->{'props'}->{$prop};
        }
        my $result = &$got(\%tab);
        # - Type de test : is()
        if ($test == TYPE_IS)
        {
            is($result, $case->{'expected'}, $case->{'label'});
        }
        # - Type de test : cmp_deeply()
        elsif ($test == TYPE_DEEP)
        {
            cmp_deeply($result, superhashof($case->{'expected'}), $case->{'label'});
        }
        # - Type de test non géré
        else
        {
            fail($case->{'label'});
        }
    }
}

1;
