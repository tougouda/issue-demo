use utf8;
use open (':encoding(UTF-8)');

# Class: LOC::Pdf
# Classe de génération de fichiers PDF
package LOC::Pdf;


# Constants: Unités de mesure
# mm - Millimètre (exportée par défaut)
# Exemple d'utilisation :
# (start code)
# my $x = 210/mm;
# (end code)
# in - Pouce (exportée par défaut)
# Exemple d'utilisation :
# (start code)
# my $x = 10/in;
# (end code)
# pt - Point (exportée par défaut)
# Exemple d'utilisation :
# (start code)
# my $fontSize = 18/pt;
# (end code)
use constant
{
    mm => 25.4 / 72,
    in => 1 / 72,
    pt => 1,
};

# Constants: Propriétés du fichier PDF
# PDF_VERSION  - Version du PDF
# PDF_AUTHOR   - Auteur du fichier PDF
# PDF_CREATOR  - Créateur du fichier PDF
# PDF_PRODUCER - Producteur du fichier PDF
use constant
{
    PDF_VERSION  => 4,
    PDF_AUTHOR   => 'Acces industrie',
    PDF_CREATOR  => 'Gestion des locations',
    PDF_PRODUCER => 'Générateur de PDF de la Gestion des locations',
};

# Constants: Orientation de la page
# ORIENTATION_PORTRAIT - portrait
# ORIENTATION_LANDSCAPE - paysage
use constant
{
    ORIENTATION_PORTRAIT  => 0,
    ORIENTATION_LANDSCAPE => 1
};

# Constants: Couleurs
# COLOR_EMPHASIS - Couleur de mise en évidence
use constant
{
    COLOR_EMPHASIS => '#479EDD',
};

# Constants: Affichages
# DISPLAY_NONE       - Aucun affichage
# DISPLAY_TITLE      - Affichage du titre
# DISPLAY_AGENCY     - Affichage des coordonnées de l'agence
# DISPLAY_FULLHEADER - Tout l'en-tête
# DISPLAY_FULLFOOTER - Tout le pied
use constant
{
    DISPLAY_NONE       => 0x000,
    DISPLAY_TITLE      => 0x001,
    DISPLAY_AGENCY     => 0x010,
    DISPLAY_FULLHEADER => 0x011,
    DISPLAY_FULLFOOTER => 0x100,
};


use strict;
use locale;
use Exporter;

use PDF::API2;

use LOC::Globals;
use LOC::Country;
use LOC::Agency;
use LOC::Locale;
use LOC::Util;

our @ISA = qw(Exporter);
our @EXPORT = qw(mm in pt);


my $VERSION = '1.1';

# Constructor: new
# Constructeur de la classe
#
# Parameters:
# string      $title    - Titre du document
# string      $agencyId - Identifiant de l'agence
#
# LOC::Locale $locale   - Référence à la locale
#
# Returns:
# hash
sub new
{
    my ($class, $title, $agencyId, $locale, $format) = @_;
    my $this = {};
    bless($this, $class);

    if (!defined $format)
    {
        $format = ORIENTATION_PORTRAIT;
    }

    $this->{title}    = $title;
    $this->{agencyId} = $agencyId;
    $this->{locale}   = $$locale;
    $this->{format}   = {
        'width'  => ($format eq ORIENTATION_PORTRAIT ? 210/mm : 297/mm),
        'height' => ($format eq ORIENTATION_PORTRAIT ? 297/mm : 210/mm)
    };

    my $tabCountryInfos = &LOC::Country::getInfos($this->{locale}->getCountryId());
    $this->{currencySymbol} = $tabCountryInfos->{'currency.symbol'};

    $this->{pdfObj} = PDF::API2->new();
    $this->{pdfObj}->{pdf}->{' version'} = PDF_VERSION;
    $this->{pdfObj}->mediabox(0, 0, $this->{format}->{'width'}, $this->{format}->{'height'});

    $this->{pdfObj}->preferences(-fit => 1);

    $this->_addFonts();
    $this->_setGeneralInfos();

    return $this;
}

# Function: addContactsBlock
# Ajoute le bloc des contacts Acces industrie
#
# Parameters:
# PDF::API2::Page  $page            - Référence à une page de PDF
# arrayref|string  $additionalField - Champ(s) additionnel(s) (si c'est une chaîne alors le champ négociateur sera ajouté)
sub addContactsBlock
{
    my ($this, $page, $additionalField) = @_;

    # Chargement des informations de l'agence
    $this->_loadAgencyInfos();

    my $gfx = $$page->gfx();
    my $y = $this->{format}->{'height'} - 55/mm;
    $gfx->textlabel(10/mm, $y, $this->{font}->{regular}, 9/pt, uc($this->{locale}->t('Contacts')), -color => COLOR_EMPHASIS);


    # Calcul de la position
    my @tabFields = ();

    # Chef d'agence
    if (defined $this->{agencyInfos}->{'manager.name'})
    {
        my $value = $this->{agencyInfos}->{'manager.firstname'} . ' ' . $this->{agencyInfos}->{'manager.name'};
        if (defined $this->{agencyInfos}->{'manager.mobile'})
        {
            $value .= ' - ' . $this->{locale}->t('Tél. %s', $this->{agencyInfos}->{'manager.mobile'});
        }
        push(@tabFields, {'label' => $this->{locale}->t('Chef d\'agence'), 'value' => $value, 'height' => 9/pt});
    }

    # Champs supplémentaires
    if (defined $additionalField)
    {
        if (ref($additionalField) eq 'ARRAY')
        {
            foreach (@$additionalField)
            {
                push(@tabFields, {'label' => $_->{'label'}, 'value' => $_->{'value'}, 'height' => 8/pt});
            }
        }
        elsif (ref($additionalField) eq 'HASH')
        {
            while (my ($label, $value) = each(%$additionalField))
            {
                push(@tabFields, {'label' => $label, 'value' => $value, 'height' => 8/pt});
            }
        }
        else
        {
            # Par défaut c'est l'interlocuteur qui est ajouté si on passe une chaîne de caractères
            push(@tabFields, {'label' => $this->{locale}->t('Interlocuteur'), 'value' => $additionalField, 'height' => 8/pt});
        }
    }
    my $offset = 0;
    foreach (@tabFields)
    {
        my $lineWidth = $gfx->advancewidth($_->{'label'} . ' :   ', 'font' => $this->{font}->{regular}, 'fontsize' => 7/pt);
        if ($offset < $lineWidth)
        {
            $offset = $lineWidth;
        }
    }

    # Affichage des champs
    my $height = 9/pt;
    foreach (@tabFields)
    {
        $y -= $height * 1.2;
        $gfx->textlabel(10/mm, $y, $this->{font}->{regular}, 7/pt, $_->{'label'} . ' :');
        $gfx->textlabel(10/mm + $offset, $y, $this->{font}->{regular}, 8/pt, $_->{'value'});

        $height = 8/pt;
    }
}


# Function: addCustomBlock
# Ajoute le bloc d'informations personnalisées
#
# Parameters:
# PDF::API2::Page $page     - Référence à une page de PDF
# string          $title    - titre du bloc
# arrayRef        $tabData  - Données à insérer
# hashref         $position - (contient x et y)
sub addCustomBlock
{
    my ($this, $page, $title, $tabData, $position) = @_;

    my $gfx = $$page->gfx();
    my $y = $this->{format}->{'height'} - $position->{'y'};
    my $x = $position->{'x'};
    $gfx->textlabel($x, $y, $this->{font}->{regular}, 9/pt, uc($this->{locale}->t($title)), -color => COLOR_EMPHASIS);


    my $offset = 0;
    foreach (@$tabData)
    {
        my $lineWidth = $gfx->advancewidth($_->{'label'} . ' :   ', 'font' => $this->{font}->{regular}, 'fontsize' => 7/pt);
        if ($offset < $lineWidth)
        {
            $offset = $lineWidth;
        }
    }

    # Affichage des champs
    my $height = 9/pt;
    foreach (@$tabData)
    {
        $y -= $height * 1.2;
        my $label = ($_->{'label'} ne '' ? $_->{'label'} . ' :   ' : '');
        $gfx->textlabel($x, $y, $this->{font}->{regular}, 7/pt, $label);
        $gfx->textlabel($x + $offset, $y, $this->{font}->{regular}, 8/pt, $_->{'value'});

        $height = 9/pt;
    }
}


# Function: addCustomerBlock
# Ajoute le bloc des coordonnées du client
#
# Parameters:
# PDF::API2::Page $page    - Référence à une page de PDF
# arrayref        $tabData - Référence à un tableau contenant les coordonnées du client
sub addCustomerBlock
{
    my ($this, $page, $tabData) = @_;

    my $gfx = $$page->gfx();
    my $text = $$page->text();

    my $x = $this->{format}->{'width'} - 10/mm - 95/mm - 2/mm;
    my $y = $this->{format}->{'height'} - 55/mm;

    my $width = $this->{format}->{'width'} - $x - 10/mm;
    my $height = 30/mm;

    $gfx->fillcolor('#ECF5FB');
    $gfx->rect($x, $y - $height, $width, $height);
    $gfx->fill();
    $gfx->fillcolor('black');

    $text->lead(10/pt * 1.2);

    $y -= 5/mm;
    my $nbLines = @$tabData;
    for (my $i = 0; $i < $nbLines; $i++)
    {
        unless($tabData->[$i] eq '')
        {
            $text->translate($this->{format}->{'width'} - 10/mm - 95/mm, $y);
            $text->font($this->{font}->{regular}, 10/pt);
            $text->text($tabData->[$i], -maxWidth => $width - 4/mm);
            $y -= $text->lead();
        }
    }
}

# Function: addFile
#
# Parameters:
# string   $file  - Chemin du fichier à inclure
sub addFile
{
    my ($this, $file) = @_;

    my $pdf = $this->getPdfObject();

    my $pdfToImport = PDF::API2->open($file);
    my $nbPages = $pdfToImport->pages();
    # Import de chaque page du fichier
    for (my $i = 0; $i < $nbPages; $i++)
    {
        $pdf->import_page($pdfToImport, $i + 1);
    }
}


# Function: addStamp
# Ajoute un tampon rouge sur le document
#
# Parameters:
# PDF::API2::Page $page       - Référence à une page de PDF
# string          $labelStamp - Texte à afficher
sub addStamp
{
    my ($this, $page, $labelStamp) = @_;

    my $pdf = $this->getPdfObject();

    my $transparentMode = $pdf->egstate();
    my $normalMode  = $pdf->egstate();

    $transparentMode->transparency(0.7);
    $normalMode->transparency(0);

    my $text = $page->text();

    # Passage en mode transparence
    $text->egstate($transparentMode);

    $text->font($this->{font}->{bold}, 80/pt);
    $text->translate($this->{format}->{'width'} / 2, $this->{format}->{'height'} / 1.5);
    $text->transform_rel(-scale => [0.8, 1], -rotate => 30);
    $text->fillcolor('#ff0000');
    $text->text_center($labelStamp);
    $text->fillcolor('black');

    # Retour mode normal
    $text->egstate($normalMode);
}



# Function: addPage
# Ajoute une page standard
#
# Returns:
# PDF::API2::Page - Instance de page de PDF
sub addPage
{
    my ($this, $displayFlag) = @_;

    unless (defined $displayFlag)
    {
        $displayFlag = DISPLAY_FULLHEADER | DISPLAY_FULLFOOTER;
    }
    my $page = $this->{pdfObj}->page();

    my $maxWidth = $this->{format}->{'width'} - 10/mm - 70/mm;

    # Filigrane
    $this->_addWatermark($page);

    my $gfx = $page->gfx();
    my $text = $page->text();

    if ($displayFlag & DISPLAY_AGENCY || $displayFlag & DISPLAY_TITLE || $displayFlag & DISPLAY_FULLFOOTER)
    {
        # Récupération des informations de l'agence
        $this->_loadAgencyInfos();
    }

    if ($displayFlag & DISPLAY_AGENCY || $displayFlag & DISPLAY_TITLE)
    {
        # Logo
        my $logoPath = &LOC::Globals::getLogo($this->{agencyInfos}->{'country.id'}, LOC::Globals::LOGO_TYPE_VECTOR);
        my $pdfLogo = PDF::API2->open($logoPath);
        my $pageLogo = $pdfLogo->openpage();
        my $logoWidth  = $pageLogo->{MediaBox}->val()->[2]->val() - $pageLogo->{MediaBox}->val()->[0]->val();
        my $logoHeight = $pageLogo->{MediaBox}->val()->[3]->val() - $pageLogo->{MediaBox}->val()->[1]->val();
        my $scale = 55/mm / $logoWidth;

        my $logo = $this->{pdfObj}->importPageIntoForm($pdfLogo);
        my $logoY = $this->{format}->{'height'} - 4/mm - $logoHeight * $scale;
        $logoY -= (35/mm - 4/mm - $logoHeight * $scale) / 2;
        $gfx->formimage($logo, 10/mm, $logoY, $scale);

        # Slogan
        my $label = uc($this->_getSlogan($this->{agencyInfos}->{'country.id'}));
        $text->font($this->{font}->{bold}, 11/pt);
        $text->translate(70/mm, $this->{format}->{'height'} - 20/mm);
        $text->fillcolor(COLOR_EMPHASIS);
        $text->text($label, -maxWidth => $maxWidth);
        $text->fillcolor('black');
    }

    if ($displayFlag & DISPLAY_AGENCY)
    {
        # Coordonnées de l'agence
        $text->lead(8/pt * 1.2);

        my $startY = $this->{format}->{'height'} - 35/mm;
        my $x = 10/mm;
        my $y = 0;

        $text->font($this->{font}->{bold}, 8/pt);
        $text->translate($x, $startY - $y);
        $text->text(uc($this->{locale}->t('Agence de %s', $this->{agencyInfos}->{label})));
        $y += $text->lead();

        $text->font($this->{font}->{regular}, 8/pt);
        $text->translate($x, $startY - $y);
        my $address = $this->{agencyInfos}->{address};
        $address =~ s/[\r\n]/ /g;
        $text->text($address);
        $y += $text->lead();

        $text->translate($x, $startY - $y);
        $text->text($this->{agencyInfos}->{postalCode} . ' ' . $this->{agencyInfos}->{city});
        $y += $text->lead();

        $text->translate($x, $startY - $y);
        $text->text($this->{locale}->t('Tél. %s Fax %s', $this->{agencyInfos}->{telephone}, $this->{agencyInfos}->{fax}));
        $y += $text->lead();
    }

    if ($displayFlag & DISPLAY_TITLE)
    {
        # Titre
        $text->font($this->{font}->{bold}, 16/pt);
        $text->translate($this->{format}->{'width'} - 10/mm - 95/mm, $this->{format}->{'height'} - 35/mm);
        $text->text(uc($this->{title}), -maxWidth => $this->{format}->{'width'} - 10/mm - 10/mm - 95/mm);
    }

    if ($displayFlag & DISPLAY_FULLFOOTER)
    {
        # Pied
        my $footer = $this->_getFooter($this->{agencyInfos}->{'country.id'});
        $text->font($this->{font}->{regular}, 8/pt);
        $maxWidth = $this->{format}->{'width'} - 2 * 10/mm;
        $text->translate($this->{format}->{'width'} / 2, 10/mm);
        $text->text_center($footer, -maxWidth => $maxWidth);
    }

    return $page;
}

# Function: addReferencesBlock
# Ajoute le bloc des références du client
#
# Parameters:
# PDF::API2::Page $page    - Référence à une page de PDF
# hashref         $tabData - Référence à une table de hachage contenant les références à afficher
#                            (la clé contient le libellé)
sub addReferencesBlock
{
    my ($this, $page, $tabData) = @_;

    my $gfx = $$page->gfx();
    my $txt = $$page->text();
    my $y = $this->{format}->{'height'} - 68/mm;

    $gfx->textlabel(10/mm, $y, $this->{font}->{regular}, 9/pt, uc($this->{locale}->t('Références')), -color => COLOR_EMPHASIS);
    $y -= 9/pt * 1.2;

    # Calcul de la position
    my $offset = 0;
    foreach (keys(%$tabData))
    {
        my $lineWidth = $gfx->advancewidth($_ . '   ', 'font' => $this->{font}->{regular}, 'fontsize' => 7/pt);
        if ($offset < $lineWidth)
        {
            $offset = $lineWidth;
        }
    }

    foreach (keys(%$tabData))
    {
        $gfx->textlabel(10/mm, $y, $this->{font}->{regular}, 7/pt, $_);
        $txt->translate(10/mm + $offset, $y);
        $txt->font($this->{font}->{regular}, 8/pt);
        my $width = $gfx->advancewidth($tabData->{$_});
        if ($width > 70/mm)
        {
            $txt->text_justified($tabData->{$_}, 70/mm);
        }
        else
        {
            $gfx->textlabel(10/mm + $offset, $y, $this->{font}->{regular}, 8/pt, , $tabData->{$_});
        }
        $y -= 8/pt * 1.2;
    }
}

# Function: end
# Termine la construction du document PDF
sub end
{
    my ($this) = @_;

    $this->{pdfObj}->end();
}

# Function: getString
# Retourne le document PDF sous forme de chaîne de caractères
#
# Returns:
# string - Flux PDF sous forme de chaîne de caractères
sub getString
{
    my ($this) = @_;

    return $this->{pdfObj}->stringify();
}

# Function: getPdfObject
# Retourne l'objet PDF
#
# Returns:
# PDF::API2 - Objet PDF
sub getPdfObject
{
    my ($this) = @_;

    return $this->{pdfObj};
}

# Function: getPageWidth
#
# Returns
# float - Largeur de la page
sub getPageWidth
{
    my ($this) = @_;
    
    return $this->{format}->{'width'};
}

# Function: getPageHeight
#
# Returns
# float - Hauteur de la page
sub getPageHeight
{
    my ($this) = @_;
    
    return $this->{format}->{'height'};
}

# Function: setKeywords
# Définit les mots-clés du document
#
# Parameters:
# array @keywords - Liste des mots-clés
sub setKeywords
{
    my ($this, @keywords) = @_;

    $this->{pdfObj}->info('Keywords' => join(' ', @keywords));
}

# Function: setSubject
# Définit les informations générales du document : créateur, producteur, dates de création et de modification, auteur
# et titre
#
# Parameters:
# string $subject - Sujet
sub setSubject
{
    my ($this, $subject) = @_;

    $this->{pdfObj}->info('Subject' => $subject);
}

# Function: _addFonts
# Ajoute les polices de caractères de base au document. Elles sont accessibles de la manière suivante :
# (start code)
# my $pdf = LOC::Pdf->new('Titre', 'AIX');
# $pdf->{font}->{regular};    # Police romaine (Myriad Pro Regular)
# $pdf->{font}->{italic};     # Police italique (Myriad Pro Italic)
# $pdf->{font}->{bold};       # Police grasse (Myriad Pro Bold)
# $pdf->{font}->{bolditalic}; # Police grasse italique (Myriad Pro Bold Italic)
# (end code)
sub _addFonts
{
    my ($this) = @_;

    # Ajout du chemin vers le répertoire des polices de caractères
    $this->{pdfObj}->addFontDirs(&LOC::Globals::get('cgiPath') . '/inc/LOC/Pdf/Fonts');

    $this->{font} = {};

    my %tabFonts = (
                    'extralight'       => 'SourceSansPro-ExtraLight',
                    'extralightitalic' => 'SourceSansPro-ExtraLightIt',
                    'light'            => 'SourceSansPro-Light',
                    'lightitalic'      => 'SourceSansPro-LightIt',
                    'regular'          => 'SourceSansPro-Regular',
                    'italic'           => 'SourceSansPro-It',
                    'semibold'         => 'SourceSansPro-Semibold',
                    'semibolditalic'   => 'SourceSansPro-SemiboldIt',
                    'bold'             => 'SourceSansPro-Bold',
                    'bolditalic'       => 'SourceSansPro-BoldIt',
                    'black'            => 'SourceSansPro-Black',
                    'blackitalic'      => 'SourceSansPro-BlackIt',
                   );

    foreach my $style (keys %tabFonts)
    {
        $this->{font}->{$style} = $this->{pdfObj}->ttfont($tabFonts{$style} . '.ttf',
                                                          -dokern => 1,
                                                          -encode => 'UTF-8');
    }
}

# Function: _addWatermark
# Ajoute un filigranne sur les documents produits par les environnements autres que celui d'exploitation
sub _addWatermark
{
    my ($this, $page) = @_;

    if (&LOC::Globals::getEnv() ne 'exp')
    {
        my $text = $page->text();

        $text->font($this->{font}->{bold}, 72/pt);
        $text->translate($this->{format}->{'width'} / 2, $this->{format}->{'height'} / 2);
        $text->transform_rel(-scale => [0.8, 1], -rotate => 30);
        $text->fillcolor('#cbcbcb');
        $text->text_center(uc($this->{locale}->t('Spécimen')));
        $text->fillcolor('black');
    }
}

# Function: _loadAgencyInfos
# Charge les informations de l'agence
#
# Parameters:
# string $agencyId - Identifiant de l'agence
#
# Returns:
# hashref - Référence à une table de hachage contenant les clés suivantes :
# - id : identifiant de l'agence ;
# - country.id : identifiant du pays de l'agence ;
# - label : nom de l'agence ;
# - address : adresse de l'agence ;
# - postalCode : code postal de l'agence ;
# - city : vilel de l'agence ;
# - telephone : numéro de téléphone de l'agence ;
# - fax : numéro de fax de l'agence ;
# - manager.name : nom du chef d'agence ;
# - manager.firstname : prénom du chef d'agence.
sub _loadAgencyInfos
{
    my ($this, $agencyId) = @_;
    unless (defined $agencyId)
    {
        $agencyId = $this->{agencyId};
    }

    unless (defined $this->{agencyInfos})
    {
        $this->{agencyInfos} = &LOC::Agency::getInfos($agencyId);
    }
}

# Function: _getFooter
# Récupère le libellé à afficher en pied de document
# *Voir pour externaliser ce paramétrage*
#
# Parameters:
# string $countryId - Identifiant du pays
#
# Returns:
# string - Libellé à afficher en pied de document
sub _getFooter
{
    my ($this, $countryId) = @_;

    # Récupération du texte de pied de page
    my $footer = &LOC::Characteristic::getCountryValueByCode('PIEDIMPR', $countryId);

    # Récupération du montant du capital
    my $capitalValue = &LOC::Country::getCapital($countryId);
    $capitalValue = $this->{locale}->getCurrencyFormat($capitalValue, undef, $this->{currencySymbol});

    # Remplacement dans le texte de pied de page
    $footer =~ s/<%capital>/$capitalValue/g;

    return $footer;
}

# Function: _getSlogan
# Récupère la phrase à afficher en tête de document à côté du logo
# *Voir pour externaliser ce paramétrage*
#
# Parameters:
# string $countryId - Identifiant du pays
#
# Returns:
# string - Phrase à afficher en tête de document à côté du logo
sub _getSlogan
{
    my ($this, $countryId) = @_;

    # Récupération du slogan
    return &LOC::Characteristic::getCountryValueByCode('SLOGANIMP', $countryId);
}

# Function: _setGeneralInfos
# Définit les informations générales du document : créateur, producteur, dates de création et de modification, auteur
# et titre
sub _setGeneralInfos
{
    my ($this) = @_;

    my @tabCurDate = localtime();
    my $creationDate = sprintf('D:%04d%02d%02d%02d%02d%02d', $tabCurDate[5] + 1900, $tabCurDate[4] + 1, $tabCurDate[3],
                                                             $tabCurDate[2], $tabCurDate[1], $tabCurDate[0]);

    $this->{pdfObj}->info(
                          'Creator'      => $this->{locale}->t(PDF_CREATOR),
                          'Producer'     => $this->{locale}->t(PDF_PRODUCER),
                          'CreationDate' => $creationDate,
                          'ModDate'      => '',
                          'Author'       => PDF_AUTHOR,
                          'Title'        => $this->{title},
                         );
}

1;
