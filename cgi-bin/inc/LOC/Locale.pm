use utf8;
use open (':encoding(UTF-8)');

# Class: LOC::Locale
# Classe de gestion des locales
package LOC::Locale;


# Constants: Valeurs par défaut
# DEFAULT_LOCALE_ID - Identifiant de la locale par défaut
# DEFAULT_DOMAIN    - Domaine par défaut
use constant
{
    DEFAULT_LOCALE_ID       => 'fr_FR',
    DEFAULT_DOMAIN          => 'location',
    DEFAULT_CURRENCY_SYMBOL => '€'
};

# Constants: Formats de date et heure
# FORMAT_DATETIME_NUMERIC       - Date et heure au format numérique
# FORMAT_DATETIME_TEXT          - Date et heure au format texte
# FORMAT_DATETIME_ABBREV        - Date et heure au format texte abrégé
# FORMAT_DATETIME_NUMERIC2      - Date et heure au format numérique alternatif
# FORMAT_DATETIMESHORT_NUMERIC  - Date et heure courte au format numérique
# FORMAT_DATETIMESHORT_NUMERIC2 - Date et heure courte au format numérique alternatif
# FORMAT_DATEYEAR_TEXT          - Date complète au format texte
# FORMAT_DATEYEAR_ABBREV        - Date complète au format texte abrégé
# FORMAT_DATE_NUMERIC           - Date complète au format numérique
# FORMAT_DATE_TEXT              - Date au format texte sans l'année
# FORMAT_DATE_ABBREV            - Date au format texte abrégé sans l'année
# FORMAT_DATESHORT_NUMERIC      - Date courte au format numérique
# FORMAT_MONTHYEAR_TEXT         - Mois et année au format texte
# FORMAT_TIME_NUMERIC           - Heure au format numérique
# FORMAT_TIMESHORT_NUMERIC      - Heure courte au format numérique
use constant
{
    FORMAT_DATETIME_NUMERIC       => 0,
    FORMAT_DATETIME_TEXT          => 1,
    FORMAT_DATETIME_ABBREV        => 2,
    FORMAT_DATETIME_NUMERIC2      => 3,
    FORMAT_DATETIMESHORT_NUMERIC  => 4,
    FORMAT_DATETIMESHORT_NUMERIC2 => 5,
    FORMAT_DATEYEAR_TEXT          => 12,
    FORMAT_DATEYEAR_ABBREV        => 14,
    FORMAT_DATE_NUMERIC           => 10,
    FORMAT_DATE_TEXT              => 11,
    FORMAT_DATE_ABBREV            => 13,
    FORMAT_DATESHORT_NUMERIC      => 15,
    FORMAT_MONTHYEAR_TEXT         => 20,
    FORMAT_TIME_NUMERIC           => 30,
    FORMAT_TIMESHORT_NUMERIC      => 31
};

# Constants: Formats des numéros de téléphone
# FORMAT_TELEPHONE_LOCAL - Numéro de téléphone au format local
# FORMAT_TELEPHONE_INTL -  Numéro de téléphone au format international
use constant
{
    FORMAT_TELEPHONE_LOCAL => 0,
    FORMAT_TELEPHONE_INTL  => 1,
};


use strict;
use locale;

use POSIX;
use DateTime;
use Locale::gettext;
use Number::Format;
use LOC::Log;
use LOC::Util;

use LOC::Globals;


# Variable: $VERSION
# Version de la classe
my $VERSION = '1.00';

# Tableau des instances de locales
my %tabInstances = ();


# Constructor: new
# Constructeur de la classe
#
# Parameters:
# string $id     - Identifiant de la locale (fr_FR par défaut)
# string $domain - Domaine pour les traductions (location par défaut)
#
# Returns:
# LOC::Locale - Instance courante
sub new
{
    my ($class, $id, $domain, $category) = @_;
    my $this = {};
    bless($this, $class);

    if (!defined $id)
    {
        $id = DEFAULT_LOCALE_ID;
    }
    if (!defined $domain)
    {
        $domain = DEFAULT_DOMAIN;
    }
    if (!defined $category)
    {
        $category = [POSIX::LC_ALL];
    }
    elsif (ref($category) eq '')
    {
        $category = [$category];
    }

    # Recherche de la locale
    my @tabLocales = `locale -a`;
    unless (grep(/$id/, @tabLocales))
    {
        $id = DEFAULT_LOCALE_ID;
    }

    $this->{'id'} = $id;

    my $name = $id;
    if (grep(/$id\.utf8/, @tabLocales))
    {
        $name .= '.utf8';
    }
    foreach my $c (@$category)
    {
        &POSIX::setlocale($c, $name);
    }

    $this->_loadMOFile();


    $this->{'_gettext'} = Locale::gettext->domain($domain);
    $this->{'_gettext'}->dir(&LOC::Globals::get('cgiPath') . '/locale/');

    return $this;
}


# Function: getId
# Récupérer l'id de la locale
#
# Returns:
# string - Identifiant de la locale
sub getId
{
    my ($this) = @_;

    return $this->{'id'};
}

# Function: getCountryId
# Récupérer l'id du pays
#
# Returns:
# string - Identifiant du pays
sub getCountryId
{
    my ($this) = @_;

    my @tabSplit = split(/_/, $this->{'id'});
    return $tabSplit[1];
}

# Function: getLanguage
# Récupérer le code de la langue
#
# Returns:
# string - Code de la langue
sub getLanguage
{
    my ($this) = @_;

    my @tabSplit = split(/_/, $this->{'id'});
    return $tabSplit[0];
}


# Function: getLocale
# Récupérer une instance de locale (Abstract Factory + Singleton)
#
# Parameters:
# string $localeId     - Id de la locale
# string|false $domain - Nom du domaine (false par défaut)
#
# Returns:
# LOC::Locale - Instance de LOC::Locale
sub getLocale
{
    my ($id, $domain, $category) = @_;
    if (!defined $domain)
    {
        $domain = DEFAULT_DOMAIN;
    }
    if (!defined $category)
    {
        $category = [POSIX::LC_ALL];
    }
    elsif (ref($category) eq '')
    {
        $category = [$category];
    }

    my $localeUniqueId = $id . '_' . $domain . '_' . join('-', @$category);
    if (!defined $tabInstances{$localeUniqueId})
    {
        $tabInstances{$localeUniqueId} = LOC::Locale->new($id, $domain, $category);
    }

    return $tabInstances{$localeUniqueId};
}


# Function: getBytesFormat
# Affiche un nombre d'octets formaté
#
# Parameters:
# float $nb        - Nombre d'octets à formater
# int   $precision - Nombre de décimales
#
# Returns:
# string - Nombre d'octets formaté
sub getBytesFormat
{
    my ($this, $nb, $precision) = @_;

    $this->_loadLocaleInfos();
    return $this->{'number'}->format_bytes($nb * 1, -precision => $precision * 1);
}

# Function: getCurrencyFormat
# Retourne un nombre au format monétaire
#
# Parameters:
# float  $nb             - Nombre à formater
# int    $precision      - Nombre de décimales (par défaut, celui de la locale)
# string $currencySymbol - Symbole monétaire (par défaut, celui de la locale)
#
# Returns:
# string - Nombre au format monétaire
sub getCurrencyFormat
{
    my ($this, $nb, $precision, $currencySymbol) = @_;

    $this->_loadLocaleInfos();
    if (!defined $nb)
    {
        $nb = 0;
    }
    if (!defined $precision)
    {
        $precision = $this->{'tabConv'}->{'frac_digits'};
    }
    if (!defined $currencySymbol)
    {
        $currencySymbol = &LOC::Util::decodeUtf8($this->{'tabConv'}->{'currency_symbol'});
    }

    return $this->{'number'}->format_price($nb, $precision, $currencySymbol);
}



# Function: getSignPosition
# Récupérer la position du signe des valeurs positives
#
# Parameters:
# bool $isNegative - Nombre négatif ou non
#
# Returns:
# int
sub getSignPosition
{
    my ($this, $isNegative) = @_;

    $this->_loadLocaleInfos();
    if ($isNegative)
    {
        return $this->{'tabConv'}->{'n_sign_posn'};
    }
    return $this->{'tabConv'}->{'p_sign_posn'};
}

# Function: isSepBySpace
# Savoir si un espace sépare le symbole monétaire d'une valeur positive ou négative
#
# Parameters:
# bool $isNegative - Spécifie s'il s'agit d'une valeur négative
#
# Returns:
# bool
sub isSepBySpace
{
    my ($this, $isNegative) = @_;

    $this->_loadLocaleInfos();
    if ($isNegative)
    {
        return $this->{'tabConv'}->{'n_sep_by_space'};
    }
    return $this->{'tabConv'}->{'p_sep_by_space'};
}

# Function: doesCurrencySymbolPrecedes
# Savoir si le symbole monétaire précède le nombre
#
# Parameters:
# bool $isNegative - Nombre négatif
#
# Returns:
# bool - 1 si le symbole monétaire précède le nombre, 0 sinon
sub doesCurrencySymbolPrecedes
{
    my ($this, $isNegative) = @_;

    $this->_loadLocaleInfos();
    return ($isNegative ? $this->{'tabConv'}->{'n_cs_precedes'} : $this->{'tabConv'}->{'p_cs_precedes'});
}

# Function: getNegativeSign
# Récupére le signe des valeurs négatives
#
# returns:
# string
sub getNegativeSign
{
    my ($this) = @_;

    $this->_loadLocaleInfos();
    return &LOC::Util::decodeUtf8($this->{'tabConv'}->{'negative_sign'});
}

# Function: getPositiveSign
# Récupérer le signe des valeurs positives
#
# Returns:
# string
sub getPositiveSign
{
    my ($this) = @_;

    $this->_loadLocaleInfos();
    return &LOC::Util::decodeUtf8($this->{'tabConv'}->{'positive_sign'});
}

# Function: getCurrencySymbol
# Récupère le symbole monétaire
#
# Returns:
# string - Symbole monétaire
sub getCurrencySymbol
{
    my ($this) = @_;

    $this->_loadLocaleInfos();
    return &LOC::Util::decodeUtf8($this->{'tabConv'}->{'currency_symbol'});
}

# Function: getCurrencyFormatString
# Retourne la chaîne de formatage des nombres monétaires
#
# Paramaters:
# string $currencyId - Identifiant de la devise
# bool   $isNegative - Nombre négatif ou non
#
# Retruns:
# string - La chaîne de formatage des nombres monétaires
sub getCurrencyFormatString
{
    my ($this, $currencyId, $isNegative) = @_;

    # Signe
    my $signPosn       = $this->getSignPosition($isNegative);
    my $isSepBySpace   = $this->isSepBySpace($isNegative);
    my $doesCsPrecedes = $this->doesCurrencySymbolPrecedes($isNegative);
    my $sign = ($isNegative ? $this->getNegativeSign() : $this->getPositiveSign());

    # Formattage monétaire
    my $result = '%f';

    # Espace entre le symbole monétaire et le nombre
    my $space = ($isSepBySpace ? ' ' : '');

    # Récupérer la devise de l'utilisateur si elle n'est pas spécifiée
    my $currencySymbol = $this->getCurrencySymbol();

    # Position du signe
    if ($signPosn == 0)
    {
        # Position du symbole monétaire
        if ($doesCsPrecedes)
        {
            $result = '(' . $currencySymbol . $space . $result . ')';
        }
        else
        {
            $result = '(' . $result . $space . $currencySymbol . ')';
        }
    }
    elsif ($signPosn == 1)
    {
        # Position du symbole monétaire
        if ($doesCsPrecedes)
        {
            $result = $sign . $currencySymbol . $space . $result;
        }
        else
        {
            $result = $sign . $result . $space . $currencySymbol;
        }
    }
    elsif ($signPosn == 2)
    {
        # Position du symbole monétaire
        if ($doesCsPrecedes)
        {
            $result = $currencySymbol . $space . $result . $sign;
        }
        else
        {
            $result = $result . $space . $currencySymbol . $sign;
        }
    }
    elsif ($signPosn == 3)
    {
        # Position du symbole monétaire
        if ($doesCsPrecedes)
        {
            $result = $sign . $currencySymbol . $space . $result;
        }
        else
        {
            $result = $result . $space . $sign . $currencySymbol;
        }
    }
    elsif ($signPosn == 4)
    {
        # Position du symbole monétaire
        if ($doesCsPrecedes)
        {
            $result = $currencySymbol . $space . $sign . $result;
        }
        else
        {
            $result = $result . $space . $currencySymbol . $sign;
        }
    }

    return $result;
}

# Function: getCurrencyFormatStringById
# Retourne la chaîne de formatage des nombres monétaires en fonction de l'identifiant de la locale
#
# Paramaters:
# string $currencyId - Identifiant de la devise
# bool   $isNegative - Nombre négatif ou non
#
# Retruns:
# string - La chaîne de formatage des nombres monétaires
sub getCurrencyFormatStringById
{
    my ($localeId, $currencyId, $isNegative) = @_;

    # Sauvegarde de la locale initiale
    my $oldLocale = &POSIX::setlocale(POSIX::LC_ALL);

    # Définition de la locale
    my $idCpl = '';
    my @tabLocales = `locale -a`;
    unless (grep(/$localeId/, @tabLocales))
    {
        $localeId = DEFAULT_LOCALE_ID;
    }
    if (grep(/$localeId\.utf8/, @tabLocales))
    {
        $idCpl = '.utf8';
    }
    &POSIX::setlocale(POSIX::LC_ALL, $localeId . $idCpl);

    # Chargement des paramètres de la locale
    my $tabConv = &POSIX::localeconv();

    # Signe
    my $signPosn       = ($isNegative ? $tabConv->{'n_sign_posn'} : $tabConv->{'p_sign_posn'});
    my $isSepBySpace   = ($isNegative ? $tabConv->{'n_sep_by_space'} : $tabConv->{'p_sep_by_space'});
    my $doesCsPrecedes = ($isNegative ? $tabConv->{'n_cs_precedes'} : $tabConv->{'p_cs_precedes'});
    my $sign = &LOC::Util::decodeUtf8($isNegative ? $tabConv->{'negative_sign'} : $tabConv->{'positive_sign'});

    # Formattage monétaire
    my $result = '%f';

    # Espace entre le symbole monétaire et le nombre
    my $space = ($isSepBySpace ? ' ' : '');

    # Récupérer la devise de l'utilisateur si elle n'est pas spécifiée
    my $currencySymbol = &LOC::Util::decodeUtf8($tabConv->{'currency_symbol'});

    # Position du signe
    if ($signPosn == 0)
    {
        # Position du symbole monétaire
        if ($doesCsPrecedes)
        {
            $result = '(' . $currencySymbol . $space . $result . ')';
        }
        else
        {
            $result = '(' . $result . $space . $currencySymbol . ')';
        }
    }
    elsif ($signPosn == 1)
    {
        # Position du symbole monétaire
        if ($doesCsPrecedes)
        {
            $result = $sign . $currencySymbol . $space . $result;
        }
        else
        {
            $result = $sign . $result . $space . $currencySymbol;
        }
    }
    elsif ($signPosn == 2)
    {
        # Position du symbole monétaire
        if ($doesCsPrecedes)
        {
            $result = $currencySymbol . $space . $result . $sign;
        }
        else
        {
            $result = $result . $space . $currencySymbol . $sign;
        }
    }
    elsif ($signPosn == 3)
    {
        # Position du symbole monétaire
        if ($doesCsPrecedes)
        {
            $result = $sign . $currencySymbol . $space . $result;
        }
        else
        {
            $result = $result . $space . $sign . $currencySymbol;
        }
    }
    elsif ($signPosn == 4)
    {
        # Position du symbole monétaire
        if ($doesCsPrecedes)
        {
            $result = $currencySymbol . $space . $sign . $result;
        }
        else
        {
            $result = $result . $space . $currencySymbol . $sign;
        }
    }

    # Retour à la locale initiale
    &POSIX::setlocale(POSIX::LC_ALL, $oldLocale);

    return $result;
}

# Function: getDateFormat
# Affiche une date formatée
#
# Parameters:
# string $date       - Date à formater (dans le fuseau horaire du serveur : Europe/Paris)
# int    $formatType - Format à appliquer
# string $timeZone   - Nom du fuseau horaire dans lequel doit être convertie la date
#                      (liste diponible par la fonction DateTime::TimeZone->all_names).
#                      S'il n'est pas renseigné, la date n'est pas convertie
#
# Returns:
# string - Date convertie dans le fuseau horaire donné et formatée
sub getDateFormat
{
    my ($this, $date, $formatType, $timeZone) = @_;

    if (!defined $date)
    {
        $date = POSIX::strftime('%Y-%m-%d %H:%M:%S', localtime());
    }
    if ($date eq '')
    {
        return '';
    }

    if ($timeZone)
    {
        my $oldLocale = setlocale(POSIX::LC_NUMERIC);
        my $localTimeZone = DateTime::TimeZone->new(name => 'local');
        my $dateObj = DateTime->new(
                                    year      => substr($date, 0, 4),
                                    month     => substr($date, 5, 2),
                                    day       => substr($date, 8, 2),
                                    hour      => substr($date, 11, 2),
                                    minute    => substr($date, 14, 2),
                                    second    => substr($date, 17, 2),
                                    time_zone => $localTimeZone,
                                   );
        $dateObj->set_time_zone($timeZone) or return '';
        $date = scalar $dateObj;
        $date =~ s/T/ /;
        &POSIX::setlocale(POSIX::LC_NUMERIC, $oldLocale);
    }

    if (!defined $formatType)
    {
        $formatType = LOC::Locale::FORMAT_DATETIME_NUMERIC;
    }
    $this->_loadDateFormats();
    my $format = $this->{'tabDateFormat'}->{$formatType};

    my ($date, $time) = split(/ /, $date);
    my @tabDate = split(/\-/, $date);
    my @tabTime = split(/:/, $time);

    return &LOC::Util::decodeUtf8(
            &POSIX::strftime(&LOC::Util::encodeUtf8($format), $tabTime[2], $tabTime[1], $tabTime[0], $tabDate[2], $tabDate[1] - 1, $tabDate[0] - 1900)
        );
}

# Function: getDateFormatString
# Retourne une chaîne contenant le formatage de la date
#
# Parameters:
# string $formatType - Format de date
#
# Returns:
# string Formatage de la date
sub getDateFormatString
{
    my ($this, $formatType) = @_;

    $this->_loadDateFormats();
    return $this->{'tabDateFormat'}->{$formatType};
}

# Function: getDateFormatStringById
# Retourne une chaîne contenant le formatage de la date à partir de l'identifiant d'une locale
#
# Parameters:
# string $id         - Identifiant de la locale
# string $formatType - Format de date
#
# Returns:
# string Formatage de la date
sub getDateFormatStringById
{
    my ($id, $formatType) = @_;

    my $file = &LOC::Globals::get('cgiPath') . '/locale/' . $id . '/LC_TIME';
    unless (-e $file)
    {
        $file = &LOC::Globals::get('cgiPath') . '/locale/' . DEFAULT_LOCALE_ID . '/LC_TIME';
    }

    open(LCTIME, $file);
    my @tabFileContent = <LCTIME>;
    close(LCTIME);

    my %tabDateFormat = eval(join(' ', @tabFileContent));

    return $tabDateFormat{$formatType};
}

# Function: getPhoneNumberFormat
# Retourne un numéro de téléphone formaté (format international)
#
# Parameters:
# string $phoneNo - Numéro de téléphone à formater
#
# Returns:
# string Le numéro de téléphone tel qu'il doit être affiché
#
# hash - Table de hachage comprenant les clés suivantes :
# - format : format d'affichage du numéro de téléphone (tel qu'il doit être passé au masked input text) ;
# - localCode : préfixe international du pays de la locale ;
# - code : préfixe international du numéro de téléphone passé en paramètre ;
# - no : numéro de téléphone (sans le préfixe international) ;
# - fullNo : numéro de téléphone complet tel qu'il doit être affiché.
sub getPhoneNumberFormat
{
    my ($this, $phoneNo) = @_;

    unless (defined $phoneNo)
    {
        $phoneNo = '';
    }

    # Suppression des espaces en début et fin
    $phoneNo = &LOC::Util::trim($phoneNo);

    # Remplacement du 00 de début de chaîne par +
    $phoneNo =~ s/^00[\s\.\-]*(\d+)/\+\1/;

    # Récupération du format d'affichage (international)
    my $format = $this->getPhoneNumberFormatString(FORMAT_TELEPHONE_INTL);

    # Position théorique de début du numéro de téléphone (hors indicatif international)
    my $noStartPosition = index($format, ' ') + 1;

    # Code international
    my $localCode = substr($format, 1, $noStartPosition - 2); # Local
    my $code = $localCode;                                    # Dans le numéro passé en paramètre

    # Élimination du préfixe international dans le format
    # et remplacement des %c par des _
    $format = substr($format, $noStartPosition);
    $format =~ s/%c/_/g;

    # Variable temporaire
    my $tmpDisplayedPhoneNo = '';

    # Détermination du format de numéro de téléphone en entrée
    # Avec indicatif international
    if (substr($phoneNo, 0, 1) eq '+')
    {
        if ($phoneNo =~ /^\+(\d+)\D?(.*)/)
        {
            $code = $1;
            $tmpDisplayedPhoneNo = $2;
        }
        else
        {
            $tmpDisplayedPhoneNo = $phoneNo;
        }
    }
    else
    {
        $tmpDisplayedPhoneNo = $phoneNo;
    }

    my $displayedPhoneNo = '';

    # Le numéro de téléphone contient des caractères autres que chiffres, séparateurs, etc.
    # Ce n'est donc pas un numéro de téléphone
    if ($phoneNo =~ /[^\d\s\(\)\.\-\+]/)
    {
        $code = '';
        $displayedPhoneNo = $phoneNo;
    }
    else
    {
        # Il s'agit d'un numéro de téléphone national
        if ($code eq $localCode && $tmpDisplayedPhoneNo ne '')
        {
            # Suppression des caractères qui ne sont pas des chiffres
            $tmpDisplayedPhoneNo =~ s/\D//g;

            # Reformatage du numéro
            # Les caractères _ du masque sont remplacés par les caractères du numéro de téléphone
            my $i = 0;
            my $j = 0;
            while ($i < length($format))
            {
                my $current = substr($format, $i, 1);

                if ($current =~ /[0-9_]/)
                {
                    $displayedPhoneNo .= substr($tmpDisplayedPhoneNo, $j, 1);
                    $j++;
                }
                else
                {
                    $displayedPhoneNo .= $current;
                }

                $i++;
            }
        }
        else
        {
            $displayedPhoneNo = $tmpDisplayedPhoneNo;
        }
    }

    # Retour sous forme de table de hachage
    if (wantarray)
    {
        return (
                'format'    => $format,
                'localCode' => $localCode,
                'code'      => $code,
                'no'        => $displayedPhoneNo,
                'fullNo'    => (($code eq '' || $displayedPhoneNo eq '') ? '' : '+' . $code . ' ') . $displayedPhoneNo,
               );
    }
    # Retour sous forme de scalaire
    else
    {
        return ($code eq '' ? '' : '+' . $code . ' ') . $displayedPhoneNo;
    }
}

# Function: getPhoneNumberFormatString
# Retourne la chaîne de formatage de la date
#
# Parameters:
# int $type - Type d'affichage (LOC::Locale::FORMAT_TELEPHONE_LOCAL ou LOC::Locale::FORMAT_TELEPHONE_INTL)
#
# Returns:
# string - Chaîne de formatage de la date
sub getPhoneNumberFormatString
{
    my ($this, $type) = @_;

    $this->_loadPhoneNumberFormats();
    return $this->{'tabPhoneNumberFormat'}->{$type};
}

# Function: getDecimalPoint
# Retourne le séparateur décimal
#
# Returns:
# string - Séparateur décimal
sub getDecimalPoint
{
    my ($this) = @_;

    $this->_loadLocaleInfos();

    return &LOC::Util::decodeUtf8($this->{'tabConv'}->{'decimal_point'});
}

# Function: getFracDigits
# Retourne le nombre local de décimales
#
# Returns:
# int - Nombre local de décimales
sub getFracDigits
{
    my ($this) = @_;

    $this->_loadLocaleInfos();

    return $this->{'tabConv'}->{'frac_digits'};
}

# Function: getThousandsSeparator
# Retourne le séparateur de milliers monétaire
#
# Returns:
# string - Séparateur de milliers
sub getMonetaryThousandsSeparator
{
    my ($this) = @_;

    $this->_loadLocaleInfos();
    return &LOC::Util::decodeUtf8($this->{'tabConv'}->{'mon_thousands_sep'});
}

# Function: getMonetaryGrouping
# Retourne le tableau contenant les regroupements numériques
#
# Returns:
# arrayref - Tableau contenant les regroupements numériques
sub getMonetaryGrouping
{
    my ($this) = @_;

    $this->_loadLocaleInfos();
    my @grouping = unpack("C*", $this->{'tabConv'}->{'mon_grouping'});
    return \@grouping;
}

# Function: getNumberFormat
# Affiche un nombre formaté (avec séparateurs décimaux et de milliers)
#
# Parameters:
# float $nb             - Nombre à formater
# int   $precision      - Nombre de décimales
# bool  $trailingZeroes - Affichage des zéros dans la partie décimale
#
# Returns:
# string - Nombre formaté
sub getNumberFormat
{
    my ($this, $nb, $precision, $trailingZeroes) = @_;

    $this->_loadLocaleInfos();
    if (!defined $nb)
    {
        $nb = 0;
    }
    return &LOC::Util::decodeUtf8($this->{'number'}->format_number($nb, $precision, $trailingZeroes));
}

# Function: getPictureFormat
# Affiche un nombre formaté avec le motif passé en paramètre
#
# Parameters:
# float  $nb      - Nombre à formater
# string $picture - Motif de formatage
#
# Returns:
# string - Nombre formaté avec le motif
sub getPictureFormat
{
    my ($this, $nb, $picture) = @_;

    $this->_loadLocaleInfos();
    if (!defined $nb)
    {
        $nb = 0;
    }
    return &LOC::Util::decodeUtf8($this->{'number'}->format_picture($nb, $picture));
}

# Function: getBytesFormat
# Retourne le nombre d'octets formaté suivant la grandeur
#
# Parameters:
# int   $bytes     - Nombre à formater
# int   $precision - Nombre de décimales à afficher
#
# Returns:
# string
sub getBytesFormat
{
    my ($this, $bytes, $precision) = @_;

    foreach my $posfix (qw(o Ko Mo Go To Po Eo Zo Yo))
    {
        if ($bytes < 1024)
        {
            # Espace entre l'unité et le nombre
            my $space = ($this->isSepBySpace(0) ? ' ' : '');

            my $numberFormat = $this->getNumberFormat($bytes, $precision);
            return sprintf('%s' . $space . '%s', $numberFormat, $posfix);
        }
        $bytes = $bytes / 1024;
    }
}

# Function: t
# Affiche la traduction d'un libellé
#
# Parameters:
# string $str     - Libellé à traduire
# array  @tabArgs - Arguments de remplacement dans le libellé (même principe que printf)
#
# Returns:
# string - Libellé traduit
sub t
{
    my ($this, $str, @tabArgs) = @_;

    my $result;
    if (defined $this->{'_gettext'})
    {
        $result = $this->{'_gettext'}->get($str);
    }
    else
    {
        $result = $str;
    }

    if (@tabArgs > 0)
    {
        $result = sprintf($result, @tabArgs);
    }
    if (&LOC::Globals::get('debugMode') && !grep(/$str/, keys(%{$this->{'_tabFileTranslates'}})))
    {
        $this->_logUnknownTranslate($str);
    }

    return $result;
}

# Function: tn
# Affiche la traduction d'un libellé au singulier ou au pluriel
#
# Parameters:
# string $str1    - Libellé au singulier à traduire
# string $str2    - Libellé au pluriel à traduire
# int    $n       - Nombre
# array  @tabArgs - Arguments de remplacement dans le libellé (même principe que printf)
#
# Returns:
# string - Libellé traduit
sub tn
{
    my ($this, $str1, $str2, $n, @tabArgs) = @_;

    my $result;
    if (defined $this->{'_gettext'})
    {
        $result = $this->{'_gettext'}->nget($str1, $str2, $n);
    }
    else
    {
        $result = ($n > 1 ? $str2 : $str1);
    }

    if (@tabArgs > 0)
    {
        $result = sprintf($result, @tabArgs);
    }
    if (&LOC::Globals::get('debugMode') && !grep(/$str1\x00$str2/, keys(%{$this->{'_tabFileTranslates'}})))
    {
        $this->_logUnknownTranslate($str1 . '/' . $str2);
    }
    return $result;
}


# Function: tpl
# Transforme un chaîne "template" en fonction de la locale
#
# Parameters:
# string $template - Chaîne "template"
#
# Returns:
# string - Chaîne
sub tpl
{
    my ($this, $template) = @_;

    $template =~ s/(\<%([a-z]{3})\:(.*?)\>)/@{[$this->_replaceTplPart($2, $3)]}/g;

    return $template;
}

# Function: _replaceTplPart
# Remplace une partie d'une chaîne "template"
sub _replaceTplPart
{
    my ($this, $code, $value) = @_;

    my @params = split(/\|/, $value);
    if ($code eq 'dte')
    {
        return $this->getDateFormat(@params);
    }
    elsif ($code eq 'cur')
    {
        return $this->getCurrencyFormat(@params);
    }
    elsif ($code eq 'str')
    {
        return $this->t(@params);
    }
    elsif ($code eq 'stn')
    {
        return $this->tn(@params);
    }
    elsif ($code eq 'num')
    {
        return $this->getNumberFormat(@params);
    }
    return $value;
}


# Function: _loadLocaleInfos
# Charge les informations sur la locale
sub _loadLocaleInfos
{
    my ($this) = @_;

    # Chargement des informations sur les locales
    if (!defined $this->{'tabConv'})
    {
        $this->{'tabConv'} = &POSIX::localeconv();
    }

    if ($this->{'tabConv'}->{'currency_symbol'})
    {
        $this->{'number'} = Number::Format->new(
                -int_curr_symbol => &LOC::Util::decodeUtf8($this->{'tabConv'}->{'currency_symbol'})
            );
    }
    else
    {
        $this->{'number'} = Number::Format->new();
    }
}

# Function: _loadDateFormats
# Charge les formats de date correspondant à la locale
sub _loadDateFormats
{
    my ($this) = @_;

    if (!defined $this->{'tabDateFormat'})
    {
        my $file = &LOC::Globals::get('cgiPath') . '/locale/' . $this->{'id'} . '/LC_TIME';
        unless (-e $file)
        {
            $file = &LOC::Globals::get('cgiPath') . '/locale/' . DEFAULT_LOCALE_ID . '/LC_TIME';
        }

        open(LCTIME, $file);
        my @tabFileContent = <LCTIME>;
        close(LCTIME);

        my %tabDateFormat = eval(join(' ', @tabFileContent));
        $this->{'tabDateFormat'} = \%tabDateFormat;
    }
}

# Function: _loadPhoneNumberFormats
# Charge les formats de numéros de téléphone correspondant à la locale
sub _loadPhoneNumberFormats
{
    my ($this) = @_;

    if (!defined $this->{'tabPhoneNumberFormat'})
    {
        my $file = &LOC::Globals::get('cgiPath') . '/locale/' . $this->{'id'} . '/LC_TELEPHONE';
        unless (-e $file)
        {
            $file = &LOC::Globals::get('cgiPath') . '/locale/' . DEFAULT_LOCALE_ID . '/LC_TELEPHONE';
        }

        open(LCTIME, $file);
        my @tabFileContent = <LCTIME>;
        close(LCTIME);

        my %tabPhoneNumberFormat = eval(join(' ', @tabFileContent));
        $this->{'tabPhoneNumberFormat'} = \%tabPhoneNumberFormat;
    }
}

# Function: _loadMOFile
# Charge le fichier MO contenant les traductions
sub _loadMOFile
{
    my ($this) = @_;

    if (&LOC::Globals::get('debugMode'))
    {
        use Locale::Maketext::Gettext;

        my $moFile = &LOC::Globals::get('cgiPath') . '/locale/' . $this->{'id'} . '/LC_MESSAGES/'
                     . $this->{'_gettext'}->{'domain'} . '.mo';
        unless (-e $moFile)
        {
            $moFile = &LOC::Globals::get('cgiPath') . '/locale/' . DEFAULT_LOCALE_ID . '/LC_MESSAGES/'
                      . $this->{'_gettext'}->{'domain'} . '.mo';
        }

        my %tabMOFile = &Locale::Maketext::Gettext::read_mo($moFile);
        $this->{'_tabFileTranslates'} = \%tabMOFile;
    }
}

# Function: _logUnknownTranslate
# Trace les traductions manquantes lorsque l'application est en mode debug
sub _logUnknownTranslate
{
    my ($this, $str) = @_;

    my @tabLocaltime = localtime();
    my $logBaseFileName = sprintf('translates[%s]', $this->{'id'});
    my $logFileName = $logBaseFileName . sprintf('_%04d-%02d.txt', $tabLocaltime[5] + 1900, $tabLocaltime[4] + 1);
    my $logFile = &LOC::Util::getCommonFilesPath('logs/locale/') . $logFileName;

    my @tabFileContent = ();

    # Lecture du contenu du fichier de log s'il existe
    if (-e $logFile)
    {
        open(LOG, $logFile);
        # Suppression des \r et \n et chaîne à partir de la position 22
        @tabFileContent = map { s/\r|\n//g; substr($_, 22); } <LOG>;
        close(LOG);
    }

    # Ajout de la traduction manquante si elle n'y est pas déjà
    my $regex = &LOC::Util::escapeRegex($str);
    unless (grep(/$regex/, @tabFileContent))
    {
        &LOC::Log::writeFileLog('locale', $str, $logBaseFileName);
    }
}

1;
