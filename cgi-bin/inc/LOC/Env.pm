#!/usr/bin/perl

use utf8;    # Tout le code du scope est considéré comme encodé en UTF-8
use open (':encoding(UTF-8)', ':std');    # Tous les flux d'entrée et de sortie sont considérés comme encodés en UTF-8
                                          # - :std affecte STDIN, STDOUT et STDERR et le changement est global
                                          # - pour les autres flux, le changement ne concerne que le scope

# Class: LOC::Env
# Module gérant les variables d'environnement
package LOC::Env;

use strict;

# Conversion des variables serveur en chaînes UTF-8
foreach (keys(%ENV))
{
    $ENV{$_} = $1 if $ENV{$_} =~ /^([^;{}"`|&<>\n\r]*)$/;
    &utf8::decode($ENV{$_});
}

1;
