use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Otd
# Module de communication avec OTD
package LOC::Otd;
use strict;


# Modules internes
use LOC::Globals;
use LOC::Country;
use LOC::Mail;
use LOC::Xml;



# Function: deleteOrder
# Supprime une commande dans OTD
#
# Parameters:
# string  $countryId      - Pays
# string  $id             - Id de la commande (ex: FR0000001)
# hashref $tabSendOptions - Options d'envoi
#
# Returns:
# bool
sub deleteOrder
{
    my ($countryId, $id, $tabSendOptions) = @_;

    # Création de la trame
    my @tab = (
        ['id', $id]
    );


    # Envoi de la trame à OTD et historisation
    if (!&_sendXML($countryId, 'order', 'del', $id, [['deleteOrder', \@tab]], $tabSendOptions))
    {
        return 0;
    }

    return 1;
}


# Function: setOrder
# Créér ou mettre à jour une commande dans OTD
#
# Parameters:
# string  $countryId      - Pays
# hashref $tabInfos       - Informations de la commande
# hashref $tabSendOptions - Options d'envoi
#
# Returns:
# bool
sub setOrder
{
    my ($countryId, $tabInfos, $tabSendOptions) = @_;

    # Récupération des informations sur le pays
    my $tabCountryInfos = LOC::Country::getInfos($countryId);


    # Id et numéro de la commande
    my $id = $tabInfos->{'id'};

    # Informations sur le chargement :
    my @tabPickup = ();

    # - Adresse
    if (ref $tabInfos->{'loading'}->{'address'} eq 'HASH')
    {
        my $siteId = sprintf('%s%010s_%03s', $countryId, $tabInfos->{'loading'}->{'address'}->{'id'}, $tabInfos->{'loading'}->{'address'}->{'updateCount'});
        my $formattedName = $tabInfos->{'loading'}->{'address'}->{'name'};
        my $formattedAddress = &LOC::Util::removeCRLF($tabInfos->{'loading'}->{'address'}->{'address'});

        push(@tabPickup,
                ['address', [
                        ['id', $siteId],
                        ['code', $siteId],
                        ['name', &_substr($formattedName, 80)],
                        ['address_kind', [
                                ['code', 'customer']
                            ]
                        ],
                        ['street_name', &_substr($formattedAddress, 80)],
                        ['door_number', ''],
                        ['zipcode', $tabInfos->{'loading'}->{'address'}->{'postalCode'}],
                        ['city', &_substr($tabInfos->{'loading'}->{'address'}->{'city'}, 80)],
                        ['country_code', $tabInfos->{'loading'}->{'address'}->{'country'}],
                        ['plan_region', [
                                ['code', ($tabInfos->{'agency.area.code'} ? $tabInfos->{'agency.area.code'} : $countryId)],
                                ['plan_group', [
                                        ['code', 'allRegions']
                                    ]
                                ]
                            ]
                        ],
                        ['temporary', 1]
                    ]
                ]);
    }
    else
    {
        push(@tabPickup,
                ['addressId', $tabInfos->{'loading'}->{'address'}]);
    }

    # - Horaires
    push(@tabPickup,
                ['start_window', [
                        ['from_instant', &_toDateTime($tabInfos->{'loading'}->{'window'}->{'from'})],
                        ['till_instant', &_toDateTime($tabInfos->{'loading'}->{'window'}->{'till'})]
                    ]
                ]);


    # Ajout des détails de la livraison
    my @tabCapabilities = ();

    # - Y'a-t-il un quai
    if ($tabInfos->{'loading'}->{'isHasWharf'})
    {
        push(@tabCapabilities,
                        ['capability', [
                                ['code', 'wharf'],
                                ['required', 'true']
                            ]
                        ]);
    }

    push(@tabPickup,
            ['capabilities', \@tabCapabilities]);



    # Informations sur le déchargement
    my @tabDelivery = ();

    # - Adresse
    if (ref $tabInfos->{'unloading'}->{'address'} eq 'HASH')
    {
        my $siteId = sprintf('%s%010s_%03s', $countryId, $tabInfos->{'unloading'}->{'address'}->{'id'}, $tabInfos->{'unloading'}->{'address'}->{'updateCount'});
        my $formattedName = $tabInfos->{'unloading'}->{'address'}->{'name'};
        my $formattedAddress = &LOC::Util::removeCRLF($tabInfos->{'unloading'}->{'address'}->{'address'});

        push(@tabDelivery,
                ['address', [
                        ['id', $siteId],
                        ['code', $siteId],
                        ['name', &_substr($formattedName, 80)],
                        ['address_kind', [
                                ['code', 'customer']
                            ]
                        ],
                        ['street_name', &_substr($formattedAddress, 80)],
                        ['door_number', ''],
                        ['zipcode', $tabInfos->{'unloading'}->{'address'}->{'postalCode'}],
                        ['city', &_substr($tabInfos->{'unloading'}->{'address'}->{'city'}, 80)],
                        ['country_code', $tabInfos->{'unloading'}->{'address'}->{'country'}],
                        ['plan_region', [
                                ['code', ($tabInfos->{'agency.area.code'} ? $tabInfos->{'agency.area.code'} : $countryId)],
                                ['plan_group', [
                                        ['code', 'allRegions']
                                    ]
                                ]
                            ]
                        ],
                        ['temporary', 1]
                    ]
                ]);
    }
    else
    {
        push(@tabDelivery,
                ['addressId', $tabInfos->{'unloading'}->{'address'}]);
    }

    # - Horaires
    push(@tabDelivery,
                ['start_window', [
                        ['from_instant', &_toDateTime($tabInfos->{'unloading'}->{'window'}->{'from'})],
                        ['till_instant', &_toDateTime($tabInfos->{'unloading'}->{'window'}->{'till'})]
                    ]
                ]);


    # Ajout des détails du chargement
    my @tabCapabilities = ();

    # - Y'a-t-il un quai
    if ($tabInfos->{'unloading'}->{'isHasWharf'})
    {
        push(@tabCapabilities,
                        ['capability', [
                                ['code', 'wharf'],
                                ['required', 'true']
                            ]
                        ]);
    }

    push(@tabDelivery,
            ['capabilities', \@tabCapabilities]);



    # Champs utilisateurs
    my %tabUDFValues = ();
    if ($tabInfos->{'type'} ne 'agenciesTransfer')
    {
        my $tabPtInfos = {};
        if ($tabInfos->{'type'} eq 'delivery')
        {
            $tabPtInfos = $tabInfos->{'unloading'}->{'infos'};

            $tabUDFValues{'trspDate'}      = substr($tabPtInfos->{'beginDate'}, 0, 10);
            $tabUDFValues{'trspBeginTime'} = substr($tabInfos->{'unloading'}->{'window'}->{'from'}, 11, 8);
            $tabUDFValues{'trspEndTime'}   = substr($tabInfos->{'unloading'}->{'window'}->{'till'}, 11, 8);
        }
        else
        {
            $tabPtInfos = $tabInfos->{'loading'}->{'infos'};

            $tabUDFValues{'trspDate'}      = substr($tabPtInfos->{'endDate'}, 0, 10);
            $tabUDFValues{'trspBeginTime'} = substr($tabInfos->{'loading'}->{'window'}->{'from'}, 11, 8);
            $tabUDFValues{'trspEndTime'}   = substr($tabInfos->{'loading'}->{'window'}->{'till'}, 11, 8);
        }

        if ($tabPtInfos->{'type'} eq 'contract')
        {
            $tabUDFValues{'rentContractCode1'} = $tabPtInfos->{'code'};
            $tabUDFValues{'rentContractCode2'} = '';
        }
        else
        {
            $tabUDFValues{'rentEstimateCode'} = $tabPtInfos->{'code'};
        }

        $tabUDFValues{'beginDate1'}    = substr($tabPtInfos->{'beginDate'}, 0, 10);
        $tabUDFValues{'endDate1'}      = substr($tabPtInfos->{'endDate'}, 0, 10);
        $tabUDFValues{'document1Url'}  = $tabPtInfos->{'url'};
        $tabUDFValues{'customerName1'} = &_substr($tabPtInfos->{'customer.name'}, 55);
        $tabUDFValues{'orderNo1'}      = &_substr($tabPtInfos->{'orderNo'}, 200);
        $tabUDFValues{'orderCttFN1'}   = &_substr($tabPtInfos->{'orderContact.fullName'}, 40);
        $tabUDFValues{'orderCttTel1'}  = &_substr($tabPtInfos->{'orderContact.telephone'}, 25);
        $tabUDFValues{'siteCttFN1'}    = &_substr($tabPtInfos->{'siteContact.fullName'}, 40);
        $tabUDFValues{'siteCttTel1'}   = &_substr($tabPtInfos->{'siteContact.telephone'}, 25);

        if ($tabInfos->{'type'} eq 'sitesTransfer')
        {
            $tabPtInfos = $tabInfos->{'unloading'}->{'infos'};

            $tabUDFValues{'trspDate'}      = substr($tabPtInfos->{'beginDate'}, 0, 10);
            $tabUDFValues{'trspBeginTime'} = substr($tabInfos->{'unloading'}->{'window'}->{'from'}, 11, 8);
            $tabUDFValues{'trspEndTime'}   = substr($tabInfos->{'unloading'}->{'window'}->{'till'}, 11, 8);

            $tabUDFValues{'beginDate2'}    = substr($tabPtInfos->{'beginDate'}, 0, 10);
            $tabUDFValues{'endDate2'}      = substr($tabPtInfos->{'endDate'}, 0, 10);
            $tabUDFValues{'document2Url'}  = $tabPtInfos->{'url'};
            $tabUDFValues{'customerName2'} = &_substr($tabPtInfos->{'customer.name'}, 55);
            $tabUDFValues{'orderNo2'}      = &_substr($tabPtInfos->{'orderNo'}, 200);
            $tabUDFValues{'orderCttFN2'}   = &_substr($tabPtInfos->{'orderContact.fullName'}, 40);
            $tabUDFValues{'orderCttTel2'}  = &_substr($tabPtInfos->{'orderContact.telephone'}, 25);
            $tabUDFValues{'siteCttFN2'}    = &_substr($tabPtInfos->{'siteContact.fullName'}, 40);
            $tabUDFValues{'siteCttTel2'}   = &_substr($tabPtInfos->{'siteContact.telephone'}, 25);

            $tabUDFValues{'rentContractCode2'} = $tabPtInfos->{'code'};
        }

        $tabUDFValues{'unconfirmedDates'} = $tabInfos->{'isDatesToConfirm'};
    }
    else
    {
        $tabUDFValues{'trspDate'} = substr($tabInfos->{'trspDate'}, 0, 10);
    }

    # Date du report de réalisation
    $tabUDFValues{'additionalDate'} = substr($tabInfos->{'doneDate'}, 0, 10);


    # Balises Amount
    my @tabAmounts = ();

    # - Poids
    if ($tabInfos->{'model'}->{'weight'} ne '')
    {
        push(@tabAmounts,
            ['amount', [
                    ['unit_code', 'weight'],
                    ['value', $tabInfos->{'model'}->{'weight'}],
                    ['specified_amount', 'TRUE']
                ]
            ]);
    }

    # - Hauteur
    if ($tabInfos->{'model'}->{'height'} ne '')
    {
        push(@tabAmounts,
            ['amount', [
                    ['unit_code', 'height'],
                    ['value', $tabInfos->{'model'}->{'height'}],
                    ['specified_amount', 'FALSE']
                ]
            ]);
    }

    # - Longueur
    if ($tabInfos->{'model'}->{'length'} ne '')
    {
        push(@tabAmounts,
            ['amount', [
                    ['unit_code', 'length'],
                    ['value', $tabInfos->{'model'}->{'length'}],
                    ['specified_amount', 'FALSE']
                ]
            ]);
    }

    # - Largeur
    if ($tabInfos->{'model'}->{'width'} ne '')
    {
        push(@tabAmounts,
            ['amount', [
                    ['unit_code', 'width'],
                    ['value', $tabInfos->{'model'}->{'width'}],
                    ['specified_amount', 'FALSE']
                ]
            ]);
    }

    push(@tabAmounts,
            ['amount', [
                    ['unit_code', 'number'],
                    ['value', 1],
                    ['specified_amount', 'FALSE']
                ]
            ]);



    # Création de la trame
    my @tab = (
        ['id', $id],
        ['order_number', $id],
        ['order_line_number', 1],
        ['order_date', $tabInfos->{'date'}],
        ['order_status', [
                ['code', $tabInfos->{'status'}]
            ]
        ],
        ['order_kind', [
                ['code', $tabInfos->{'type'}]
            ]
        ],
        ['contact', [
                ['id', '1ORC']
            ]
        ],

        ['productId', $tabInfos->{'model'}->{'id'}],
        ['amounts', \@tabAmounts],

        ['sum', $tabInfos->{'amount'}],
        ['currency_code', $tabCountryInfos->{'currency.id'}],
        ['department', [
                ['code', $tabInfos->{'agency.id'}]
            ]
        ],
        ['reference', ''],
        ['comment', (defined $tabInfos->{'isAsbestos'} ?
                        ($tabInfos->{'isAsbestos'} ? "CHANTIER AMIANTE\r\n" : '') :
                        '') . &_substr(&LOC::Util::removeCRLF($tabInfos->{'comments'}), undef)],

        ['udfields', [
                ['udfield', $tabUDFValues{'rentContractCode1'}, [['name', 'rentContractCode1']]],
                ['udfield', $tabUDFValues{'rentContractCode2'}, [['name', 'rentContractCode2']]],
                ['udfield', $tabUDFValues{'rentEstimateCode'}, [['name', 'rentEstimateCode']]],
                ['udfield', ($tabInfos->{'machine'} ? $tabInfos->{'machine'}->{'url'} : ''), [['name', 'machineUrl']]],
                ['udfield', ($tabInfos->{'machine'} ? $tabInfos->{'machine'}->{'parkNumber'} : ''), [['name', 'parkNo']]],
                ['udfield', ($tabInfos->{'machine'} ? $tabInfos->{'machine'}->{'energy'} : ''), [['name', 'machineEnergy']]],
                ['udfield', (defined $tabInfos->{'isFinalAllocation'} ?
                                ($tabInfos->{'isFinalAllocation'} ? 'Oui' : 'Non') :
                                undef), [['name', 'finalAllocation']]],
                ['udfield', ($tabInfos->{'machine'} ? ($tabInfos->{'machine'}->{'isSiteBlocked'} ? 'Oui' : 'Non') : ''), [['name', 'isMachineBlocked']]],

                ['udfield', $tabUDFValues{'document1Url'}, [['name', 'document1Url']]],
                ['udfield', $tabUDFValues{'customerName1'}, [['name', 'customerName1']]],
                ['udfield', $tabUDFValues{'orderNo1'}, [['name', 'orderNo1']]],
                ['udfield', $tabUDFValues{'orderCttFN1'}, [['name', 'orderContactFullName1']]],
                ['udfield', $tabUDFValues{'orderCttTel1'}, [['name', 'orderContactTelephone1']]],
                ['udfield', $tabUDFValues{'siteCttFN1'}, [['name', 'siteContactFullName1']]],
                ['udfield', $tabUDFValues{'siteCttTel1'}, [['name', 'siteContactTelephone1']]],
                ['udfield', $tabUDFValues{'beginDate1'}, [['name', 'beginDate1']]],
                ['udfield', $tabUDFValues{'endDate1'}, [['name', 'endDate1']]],

                ['udfield', $tabUDFValues{'document2Url'}, [['name', 'document2Url']]],
                ['udfield', $tabUDFValues{'customerName2'}, [['name', 'customerName2']]],
                ['udfield', $tabUDFValues{'orderNo2'}, [['name', 'orderNo2']]],
                ['udfield', $tabUDFValues{'orderCttFN2'}, [['name', 'orderContactFullName2']]],
                ['udfield', $tabUDFValues{'orderCttTel2'}, [['name', 'orderContactTelephone2']]],
                ['udfield', $tabUDFValues{'siteCttFN2'}, [['name', 'siteContactFullName2']]],
                ['udfield', $tabUDFValues{'siteCttTel2'}, [['name', 'siteContactTelephone2']]],
                ['udfield', $tabUDFValues{'beginDate2'}, [['name', 'beginDate2']]],
                ['udfield', $tabUDFValues{'endDate2'}, [['name', 'endDate2']]],

                ['udfield', ($tabInfos->{'isDoable'} ? 'Oui' : 'Non'), [['name', 'isOrderFinalizable']]],
                ['udfield', (defined $tabInfos->{'isAnticipatable'} ?
                                ($tabInfos->{'isAnticipatable'} ? 'Oui' : 'Non') :
                                undef), [['name', 'isAnticipatable']]],
                ['udfield', ($tabInfos->{'isAddMachine'} ?
                                ($tabInfos->{'isAddMachine'} ? 'Oui' : 'Non') :
                                undef), [['name', 'isAddMachine']]],
                ['udfield', $tabInfos->{'addMachineLnkCttUrl'}, [['name', 'addMachineLinkedContractUrl']]],

                ['udfield', $tabUDFValues{'trspDate'}, [['name', 'transportDate']]],
                ['udfield', $tabUDFValues{'trspBeginTime'}, [['name', 'transportBeginTime']]],
                ['udfield', $tabUDFValues{'trspEndTime'}, [['name', 'transportEndTime']]],
                ['udfield', (defined $tabUDFValues{'unconfirmedDates'} ?
                                ($tabUDFValues{'unconfirmedDates'} ? 'Oui' : 'Non') :
                                undef), [['name', 'unconfirmedDates']]],
                ['udfield', $tabUDFValues{'additionalDate'}, [['name', 'additionalDate']]]
            ]
        ],

        ['pickup_task', \@tabPickup],

        ['delivery_task', \@tabDelivery]
    );


    # Envoi de la trame à OTD et historisation
    if (!&_sendXML($countryId, 'order', 'upd', $id, [['transport_order', \@tab]], $tabSendOptions))
    {
        return 0;
    }

    return 1;
}


# Function: setProduct
# Créér ou mettre à jour un modèle de machine dans OTD
#
# Parameters:
# string  $countryId      - Pays
# hashref $tabInfos       - Informations de la commande
# hashref $tabSendOptions - Options d'envoi
#
# Returns:
# bool
sub setProduct
{
    my ($countryId, $tabInfos, $tabSendOptions) = @_;

    # Informations sur la famille de machine
    my $tabMachineFamilyInfos = &LOC::MachinesFamily::getInfos($countryId, $tabInfos->{'machinesFamily.id'});

    # Création de la trame
    my @tab = (
        ['id', $tabInfos->{'id'}],
        ['code', $tabInfos->{'id'}],
        ['name', $tabInfos->{'label'}],
        ['product_kind', [
                ['code', $tabInfos->{'type.id'}],
                ['name', $tabInfos->{'type.label'}]
            ]
        ]);

    # Envoi de la trame à OTD et historisation
    if (!&_sendXML($countryId, 'model', 'upd', $tabInfos->{'id'}, [['product', \@tab]], $tabSendOptions))
    {
        return 0;
    }

    return 1;
}


# Function: _substr
# Formater et tronquer une chaîne de caractères pour la trame XML
#
# Parameters:
# string $str    - Chaîne de caractères
# int    $length - Longueur
#
# Returns:
# string - Chaîne formatée
sub _substr
{
    my ($str, $length) = @_;

    # Suppression des espaces avant et après
    $str = &LOC::Util::trim($str);

    # OTD utilise des fins de lignes Windows
    # Remplacement des \r et des \n par des \r\n
    $str =~ s/[\r\n]{1,2}/\r\n/g;

    # Tronque la chaîne de caractères
    if (defined $length)
    {
        $str = substr($str, 0, $length);

        # Suppression des \r ou \n isolés en fin de chaîne suite au tronçonnage à $length caractères
        $str =~ s/([^\r\n])[\r\n]$/$1/g;
    }

    return $str;
}


# Function: _generateXMLFileName
# Générer le nom d'un fichier XML envoyé à OTD
#
# Parameters:
# string    $type          - Type du fichier
# string    $action        - Action
# mixed     $id            - Id de l'entitée
# int|undef $sendingsCount - Nombre d'envois
#
# Returns:
# string - Nom du fichier XML
sub _generateXMLFileName
{
    my ($type, $action, $id, $sendingsCount) = @_;

    my @tabLocaltime = localtime();

    # Préfixe de l'application
    my $fileName = 'LOC';

    # Type du fichier
    $fileName .= '_' . $type;

    # Id de l'entitée
    $fileName .= '_' . $id;

    # Date
    $fileName .= sprintf('_%04d%02d%02d%02d%02d%02d', $tabLocaltime[5] + 1900,
                                                      $tabLocaltime[4] + 1,
                                                      $tabLocaltime[3],
                                                      $tabLocaltime[2],
                                                      $tabLocaltime[1],
                                                      $tabLocaltime[0]);

    # Nombre d'envois
    $fileName .= sprintf('_%04d', $sendingsCount);

    # Action
    $fileName .= '_' . $action;

    # Extension XML
    $fileName .= '.xml';

    return $fileName;
}


# Function: _sendXML
# Sérialiser les données en XML pour les envoyer à OTD (+ Historisation)
#
# Parameters:
# string   $countryId      - Pays
# string   $type           - Type du fichier
# string   $action         - Action
# mixed    $id             - Id de l'entitée
# arrayref $tabData        - Données à sérialiser
# hashref  $tabSendOptions - Options d'envoi
#
# Returns:
# bool
sub _sendXML
{
    my ($countryId, $type, $action, $id, $tabData, $tabSendOptions) = @_;

    my $OTDMountPath  = &LOC::Globals::get('OTDMountPath');

    # Si le répertoire d'échange avec OTD n'est pas configuré, on ne fait rien
    if ($OTDMountPath eq '')
    {
        return 1;
    }


    # Génération de la trame XML
    my $content = '<?xml version="1.0" encoding="UTF-8"?><comtec version="2010">';
    my $nb = @$tabData;
    for (my $i = 0; $i < $nb; $i++)
    {
        $content .= &LOC::Xml::getTagOuterXML(@{$tabData->[$i]});
    }
    $content .= '</comtec>';


    # Calcul de l'empreinte MD5 du contenu XML
    my $newHash = &LOC::Util::getHexMd5Digest($content);


    # Si la nouvelle empreinte est identique à la précédente alors on ne renvoie pas le fichier XML
    if ($tabSendOptions->{'lastHash'} eq $newHash)
    {
        return 1;
    }

    # Nombre d'envois si tout se passe bien
    my $newSendingsCount = $tabSendOptions->{'sendingsCount'} + 1;


    # Répertoires de stockage des fichiers XML
    my $OTDInMountPath  = &LOC::Globals::get('OTDMountPath') . '/In/';
    my $commonFilesPath = &LOC::Util::getCommonFilesPath('transport/out/');


    # Génération du nom du fichier XML
    my $fileName = &_generateXMLFileName($type, $action, $id, $newSendingsCount);

    # Enregistrement du fichier dans notre dossier d'archive
    if (!open(FILE, '>', $commonFilesPath . '/' . $fileName))
    {
        # Envoi de l'email d'erreur
        my $eMailObj = LOC::Mail->new();

        # Encodage
        $eMailObj->setEncodingType(LOC::Mail::ENCODING_TYPE_UTF8);
        # Expéditeur
        my $sender = &LOC::Globals::get('errorsSender');
        $eMailObj->setSender(%$sender);
        # Destinataire
        my $tabErrorsRecipients = &LOC::Globals::get('errorsRecipients');
        $eMailObj->addRecipients(%$tabErrorsRecipients);
        # Corps
        my $bodyEMail = 'Bonjour,

Il y a eu une erreur lors de la sauvegarde du fichier "' . $fileName . '" dans la Gestion des locations.
Vous trouverez le fichier en pièces jointes afin de le déposer dans les dossiers "' . $commonFilesPath . '" et "' . $OTDInMountPath . '".

Bonne journée.';
        $eMailObj->setBodyType(LOC::Mail::BODY_TYPE_TEXT);
        $eMailObj->setBody($bodyEMail);

        my $subject = '[GesLoc-OTD] Erreur lors de la sauvegarde du fichier ' . $fileName;
        $eMailObj->setSubject($subject);

        # Pièces jointes
        $eMailObj->addStringAttachment($content, $fileName);
        $eMailObj->send();

        return 0;
    }
    print FILE $content;
    close(FILE);

    # Enregistrement du fichier dans le dossier In d'OTD
    # - Est-on en mode OTD non dégradé ?
    my $isSendXMLActive = &LOC::Characteristic::getCountryValueByCode('OTDENVOIXML', $countryId);
    if ($isSendXMLActive)
    {
        if (!open(FILE, '>', $OTDInMountPath . $fileName))
        {
            # Envoi de l'email d'erreur
            my $eMailObj = LOC::Mail->new();

            # Encodage
            $eMailObj->setEncodingType(LOC::Mail::ENCODING_TYPE_UTF8);
            # Expéditeur
            my $sender = &LOC::Globals::get('errorsSender');
            $eMailObj->setSender(%$sender);
            # Destinataire
            my $tabErrorsRecipients = &LOC::Globals::get('errorsRecipients');
            $eMailObj->addRecipients(%$tabErrorsRecipients);
            # Corps
            my $bodyEMail = 'Bonjour,

Il y a eu une erreur lors de la sauvegarde du fichier "' . $fileName . '" dans OTD.
Vous trouverez le fichier en pièces jointes afin de le déposer dans le dossier "' . $OTDInMountPath . '".

Bonne journée.';
            $eMailObj->setBodyType(LOC::Mail::BODY_TYPE_HTML);
            $eMailObj->setBody($bodyEMail);

            my $subject = '[GesLoc-OTD] Erreur lors de la sauvegarde du fichier ' . $fileName;
            $eMailObj->setSubject($subject);

            # Pièces jointes
            $eMailObj->addStringAttachment($content, $fileName);

            $eMailObj->send();
            return 0;
        }
        print FILE $content;
        close(FILE);
    }

    # Mise à jour du nombre d'envois
    $tabSendOptions->{'sendingsCount'} = $newSendingsCount;
    # Mise à jour de l'empreinte MD5 de la dernière trame XML
    $tabSendOptions->{'lastHash'} = $newHash;

    return 1;
}


# Function: _toDateTime
# Convertie la date MySQL (Y-m-d H:i:s) au format accepté par OTD (Y-m-d\TH:i)
#
# Parameters:
# string $dateTime - Date
#
# Returns:
# string
sub _toDateTime
{
    my ($dateTime) = @_;

    return (length($dateTime) > 10 ? substr($dateTime, 0, 10) . 'T' . substr($dateTime, 11, 5) : $dateTime);
}


1;