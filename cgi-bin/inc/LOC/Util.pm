use utf8;
use open (':encoding(UTF-8)');

no if $] >= 5.017011, warnings => 'experimental::smartmatch';

# Class: LOC::Util
# Module de fonctions utilitaires
package LOC::Util;


# Constants: Récupération d'informations sur les contrats éligibles pour un transfert inter-chantiers
# GETLIST_PAIRS - Liste au format clé => valeur
# GETLIST_ASSOC - Liste au format clé => tableau associatif
# GETLIST_IDS   - Liste des identifiants
# GETLIST_COUNT - Nombre d'éléments de la liste
use constant {
    GETLIST_PAIRS => 0,
    GETLIST_ASSOC => 1,
    GETLIST_IDS   => 3,
    GETLIST_COUNT => 4
};

# Constants: Caractères d'ellipse d'une chaîne de caractères
# STRING_ELLIPSIS - ...
use constant {
    STRING_ELLIPSIS => '...'
};

# Constants: Arrondi
# ROUND_ADDDEC - Nombre de décimales à ajouter pour l'arrondi
use constant {
    ROUND_ADDDEC => 4
};

# Constants: Versions XML
# XML_1_0 - XML 1.0
# XML_1_1 - XML 1.1
use constant {
    XML_1_0 => '1.0',
    XML_1_1 => '1.1'
};

# Constantes
# ENCODING_CP1252 - Encodage en CP1252
# ENCODING_UTF8   - Encodage en UTF-8
use constant
{
    ENCODING_CP1252 => 'CP1252',
    ENCODING_UTF8   => 'UTF-8'
};


use strict;
use locale;

use B;
use Scalar::Util;
use POSIX qw(setlocale LC_NUMERIC);
use Tie::IxHash;
use Clone;
use Encode;
use Unicode::Normalize;
use File::Path;
use Crypt::CBC;
use MIME::Base64;
use Digest::MD5;



# Function: isNumeric
# Indique si la variable passée en paramètre est de type numérique ou non
#
# Parameters:
# mixed $x - Variable à tester
#
# Returns:
# int - 1 si oui, 0 sinon
sub isNumeric
{
    my ($var) = @_;
    return (Scalar::Util::looks_like_number($var) != 0);
}


# Function: isInt
# Indique si la variable passée en paramètre est un entier ou non
#
# Parameters:
# mixed $x - Variable à tester
#
# Returns:
# int - 1 si oui, 0 sinon
sub isInt
{
    my $b_obj = B::svref_2object(\$_[0]);  # for round trip problem
    my $flags = $b_obj->FLAGS;

    return ((($flags & B::SVf_IOK or $flags & B::SVp_IOK or
              $flags & B::SVf_NOK or $flags & B::SVp_NOK)
              and !($flags & B::SVf_POK)) ? 1 : 0); # SvTYPE is IV or NV?
}


# Function: trim
# Supprime les espaces en début et fin de chaîne
#
# Parameters:
# string $str - Chaîne
#
# Returns:
# string - Chaîne traitée
sub trim
{
    my $str = $_[0];
    $str =~ s/^\s+//;
    $str =~ s/\s+$//;
    return $str;
}


# Function: ltrim
# Supprime les espaces en début de chaîne
#
# Parameters:
# string $str - Chaîne
#
# Returns:
# string - Chaîne traitée
sub ltrim
{
    my $str = $_[0];
    $str =~ s/^\s+//;
    return $str;
}


# Function: rtrim
# Supprime les espaces en fin de chaîne
#
# Parameters:
# string $str - Chaîne
#
# Returns:
# string - Chaîne traitée
sub rtrim
{
    my $str = $_[0];
    $str =~ s/\s+$//;
    return $str;
}


# Function: in_array
# Indique si une valeur appartient à un tableau
#
# Parameters:
# string   $search - La valeur à rechercher
# arrayref $tabRef - Référence du tableau
#
# Returns:
# int 0|1 - Booléen indiquant si la valeur appartient au tableau ou non
sub in_array
{
    my ($search, $tabRef) = @_;

    if (&isInt($search))
    {
        foreach my $value (@$tabRef)
        {
            if ($value == $search)
            {
                return 1;
            }
        }
    }
    else
    {
        foreach my $value (@$tabRef)
        {
            if ($value eq $search)
            {
                return 1;
            }
        }
    }
    return 0;
}


# Function: areArraysEquals
# Indique si deux tableaux sont identiques
#
# Parameters:
# arrayref $array1 - Premier tableau
# arrayref $array2 - Second tableau
#
# Returns:
# int 0|1 - Booléen indiquant si les deux  tableaux sont identiques
sub areArraysEquals
{
    my ($array1, $array2) = @_;

    if (ref($array1) ne 'ARRAY' || ref($array2) ne 'ARRAY')
    {
        return 0;
    }

    my @array1 = sort(@$array1);
    my @array2 = sort(@$array2);

    my $json1 = &LOC::Json::toJson(\@array1);
    my $json2 = &LOC::Json::toJson(\@array2);

    return ($json1 eq $json2);
}


# Function: isValidEmail
# Indique si l'adresse mail est valide ou non
#
# Parameters:
# string $email - Adresse e-mail
#
# Returns:
# bool -
sub isValidEmail
{
    my $email = $_[0];

    my $atom   = '[-a-z0-9!#$%&\'*+\\/=?^_`{|}~]';
    my $domain = '([a-z0-9]([-a-z0-9]*[a-z0-9]+)?)';
    my $regex = '^' . $atom . '+(\.' . $atom . '+)*@(' . $domain . '{1,63}\.)+' . $domain . '{2,63}$';
    if ($email =~ m/$regex/i)
    {
        return 1;
    }
    return 0;
}


# Function: round
# Arrondit un nombre avec la précision désirée
#
# Parameters:
# float $nb           - Nombre à arrondir
# int   $precision    - Nombre de décimales
# int   $nbSrcMaxDecs - Nombre de décimales maximum en entrée (facultatif) (pour les problèmes d'arrondis)
#
# Returns:
# float - Nombre arrondi
sub round
{
    my ($nb, $precision, $nbSrcMaxDecs) = @_;
    $nb = &toNumber($nb);

    my $neg = ($nb >= 0 ? 1 : -1);

    if ($nbSrcMaxDecs)
    {
        $nb += (10 ** -($nbSrcMaxDecs + 1)) * $neg;
    }

    my $result = int((10 ** $precision) * $neg * $nb + 0.5) / (10 ** $precision);
    my $format = '%.' . $precision . 'f';

    return sprintf($format, $neg * $result) * 1;
}


# Function: min
# Retourne la valeur minimale entre les deux valeurs fournies
#
# Parameters:
# float $a - Première valeur
# float $b - Seconde valeur
#
# Returns:
# float - Valeur minimale
sub min
{
    my ($a, $b) = @_;

    return ($a < $b ? $a : $b);
}


# Function: max
# Retourne la valeur maximale entre les deux valeurs fournies
#
# Parameters:
# float $a - Première valeur
# float $b - Seconde valeur
#
# Returns:
# float - Valeur maximale
sub max
{
    my ($a, $b) = @_;

    return ($a > $b ? $a : $b);
}


# Function: floor
# Arrondit un nombre à l'entier inférieur
#
# Parameters:
# float $nb - Nombre à arrondir
#
# Returns:
# int - Nombre arrondi
sub floor
{
    my ($nb) = @_;

    return int($nb);
}


# Function: ceil
# Arrondit un nombre à l'entier supérieur
#
# Parameters:
# float $nb - Nombre à arrondir
#
# Returns:
# int - Nombre arrondi
sub ceil
{
    my ($nb) = @_;

    return &floor($nb + 0.99);
}


# Function: escapeRegex
# Protège les caractères spéciaux dans une expression régulière
#
# Parameters:
# string $regex - Expression régulière à protéger
#
# Returns:
# string - Expression régulière protégée
sub escapeRegex
{
    my ($regex) = @_;

    $regex =~ s/([\/\^\$\(\)\<\>\[\]\{\}\\\|\.\*\+\?])/\\$1/g;

    return $regex;
}

# Function: escapeRegexForSql
# Protège les caractères spéciaux dans une expression régulière pour les requêtes SQL
#
# Parameters:
# string $regex - Expression régulière à protéger
#
# Returns:
# string - Expression régulière protégée
sub escapeRegexForSqlRegex
{
    my ($regex) = @_;

    $regex = escapeRegex($regex);
    # échappement supplémentaire des \
    $regex =~ s/\\/\\\\/g;

    return $regex;
}

# Function: getNonLocaleFloat
# Récupérer la représentation d'un float indépendamment de la locale
#
# Parameters:
# float $value - Valeur
#
# Returns:
# string
sub getNonLocaleFloat
{
    my ($value) = @_;

    my $oldLocale = &POSIX::setlocale(POSIX::LC_NUMERIC);
    &POSIX::setlocale(POSIX::LC_NUMERIC, 'C');
    $value = ($value * 1) . '';
    &POSIX::setlocale(POSIX::LC_NUMERIC, $oldLocale);
    return $value;
}


# Function: toString
# Convertir un entier en chaîne de caractères
#
# Parameters:
# int $num - Entier à convertir
#
# Returns:
# string
sub toString
{
    return $_[0] . '';
}


# Function: toNumber
# Convertir une chaîne de caractères en entier
#
# Parameters:
# string $str - Chaîne de caractères à convertir
#
# Returns:
# int
sub toNumber
{
    return ($_[0] . '') * 1;
}


# Function: sortHashByKeys
# Trie une table de hachage par ordre de ses clés
#
# Parameters:
# hashref $tab  - Table de hachage à trier
# bool    $desc - Tri descendant (1) ou ascendant (0)
# subref  $sub  - La référence à la méthode de tri.
#
# Returns:
# hashref - Table de hachage triée
sub sortHashByKeys
{
    my ($tab, $desc, $sub) = @_;

    # Tri alphabétique par défaut
    if (!defined $sub)
    {
        $sub = sub
        {
            my ($a, $b) = @_;
            return ($a cmp $b);
        };
    }

    tie(my %tabResult, 'Tie::IxHash');
    foreach my $key (sort { $desc ? &$sub($b, $a) : &$sub($a, $b) } keys(%$tab))
    {
        $tabResult{$key} = $tab->{$key};
    }

    return \%tabResult;
}


# Function: sortHashByValues
# Trie une table de hachage par ordre de ses valeurs
#
# Parameters:
# hashref $tab  - Table de hachage à trier
# bool    $desc - Tri descendant (1) ou ascendant (0)
# subref  $sub  - La référence à la méthode de tri.
#
# Returns:
# hashref - Table de hachage triée
sub sortHashByValues
{
    my ($tab, $desc, $sub) = @_;

    # Tri alphabétique par défaut
    if (!defined $sub)
    {
        $sub = sub
        {
            my ($a, $b) = @_;
            return ($a cmp $b);
        };
    }

    tie(my %tabResult, 'Tie::IxHash');
    foreach my $key (sort { $desc ? &$sub($tab->{$b}, $tab->{$a}) : &$sub($tab->{$a}, $tab->{$b}) } keys(%$tab))
    {
        $tabResult{$key} = $tab->{$key};
    }

    return \%tabResult;
}


# Function: getHashValues
# Renvoi les valeurs d'une table de hachage
#
# Parameters:
# hashref $hash - Table de hachage
#
# Returns:
# arrayref | array - Valeurs
sub getHashValues
{
    my ($hash) = @_;
    my @array = values(%$hash);
    return (wantarray ? @array : \@array);
}


# Function: join
# Rassembler les valeurs d'un tableau ou d'une table de hashage en une chaîne
#
# Parameters:
# string $sep    - Séparateur
# mixed  $values - Valeurs à rassembler
#
# Returns:
# string - Chaîne
sub join
{
    my ($sep, $values) = @_;

    if (@_ > 2)
    {
        return join(@_);
    }
    elsif (ref($values) eq 'ARRAY')
    {
        return join($sep, @$values);
    }
    elsif (ref($values) eq 'HASH')
    {
        return join($sep, values(%$values));
    }
    return $values;
}


# Function: clone
# Cloner recursivement un tableau (hash ou array)
#
# Parameters:
# hashref|arrayref $tab - Tableau à cloner
#
# Returns:
# hashref|arrayref - Tableau cloné
sub clone
{
    my ($tab) = @_;

    return &Clone::clone($tab);
}


# Function: getCommonFilesPath
# Récupérer le chemin vers les fichiers communs (si le répertoire n'existe pas, il sera créé)
#
# Parameters:
# string  $subDir     - Sous-répertoire (facultatif)
# hashref $tabOptions - Options supplémentaires (facultatif)
#
# Returns:
# string - Chemin vers les fichiers communs
sub getCommonFilesPath
{
    my ($subDir, $tabOptions) = @_;

    if (!defined $tabOptions || ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }

    # Récupération du répertoire de base des fichiers communs
    my $path = &LOC::Globals::get('commonFilesPath');

    # Ajout du sous-répertoire si défini
    if ($subDir ne '')
    {
        $path .= '/' . $subDir;
    }

    # Si le répertoire n'existe pas alors on le crée
    if (!$tabOptions->{'disableCreateIfNotExists'})
    {
        &File::Path::make_path($path);
    }

    return $path;
}


# Function: setTreeValues
# Définir la valeur des feuilles d'un arbre
#
# Parameters:
# hashref          $tab           - Arbre
# hashref|arrayref $tabProperties - Propriétés à définir
# mixed            $value         - Valeur à affecter
#
# Returns:
# void
sub setTreeValues
{
    my ($tab, $tabProperties, $value) = @_;

    if (ref($tabProperties) eq 'ARRAY')
    {
        foreach my $prop (@$tabProperties)
        {
            $tab->{$prop} = $value;
        }
    }
    elsif (ref($tabProperties) eq 'HASH')
    {
        foreach my $prop (keys %$tabProperties)
        {
            if (!exists $tab->{$prop})
            {
                $tab->{$prop} = {};
            }
            &setTreeValues($tab->{$prop}, $tabProperties->{$prop}, $value);
        }
    }
}


# Function: execTreeFunc
# Exécute une fonction sur des feuilles d'un arbre
#
# Parameters:
# hashref          $tab           - Arbre
# hashref|arrayref $tabProperties - Propriétés sur lesquelles lancer la fonction
# coderef          $func          - Fonction à éxécuter
#
# Returns:
# void
sub execTreeFunc
{
    my ($tab, $tabProperties, $func) = @_;

    if (ref($tabProperties) eq 'ARRAY')
    {
        foreach my $prop (@$tabProperties)
        {
            &$func($tab->{$prop});
        }
    }
    elsif (ref($tabProperties) eq 'HASH')
    {
        foreach my $prop (keys %$tabProperties)
        {
            if (exists $tab->{$prop})
            {
                &execTreeFunc($tab->{$prop}, $tabProperties->{$prop}, $func);
            }
        }
    }
}


# Function: substr_ellipsis
# Tronque une chaîne de caractères à la longueur donnée et rajoute le caractère d'ellipse '...'
#
# Parameters:
# string  $str    - Chaîne de départ
# int     $length - Indice du premier caractère de la sous-chaîne à extraire
#
# Returns:
# string - Chaîne tronquée
sub substr_ellipsis
{
    my ($str, $length) = @_;

    if (length($str) > $length)
    {
        my $substr = (defined $str ? substr($str, 0, $length - length(STRING_ELLIPSIS)) : undef);
        return $substr . STRING_ELLIPSIS;
    }
    return $str;
}

# Function: removeCRLF
# Remplacer les caractères de retour chariot et de fin de ligne par une chaîne de caractères (espace par défaut)
#
# Parameters:
# string $str - Chaîne de caractères
#
# Returns:
# string - Chaîne formatée
sub removeCRLF
{
    my ($str, $replace) = @_;

    if (!defined $replace)
    {
        $replace = ' ';
    }

    $str =~ s/[\r\n]+/$replace/g;

    return $str;
}


# Function: toUnaccented
# Supprimer tous les caractères accentués
#
# Parameters:
# string  $str - Chaîne de départ
#
# Returns:
# string - Chaîne convertie
sub toBasicLatin
{
    my ($str) = @_;

    # Décomposition des caractères accentués
    my $decomposed = NFKD($str);

    # Suppression de tous les caractères non ASCII
    $decomposed =~ s/[^\p{Block: Basic Latin}]//g;

    return $decomposed;
}


# Function: getNonLocaleNumber
# Retourne un nombre sans tenir compte de la locale
#
# Parameters:
# float  $number - Nombre
#
# Returns:
# string - Nombre indépendant de la locale
sub getNonLocaleNumber
{
    my ($number) = @_;

    my $decimalPoint = &POSIX::localeconv()->{'decimal_point'};
    if ($decimalPoint eq '.')
    {
        $decimalPoint = '\\' . $decimalPoint;
    }
    $number =~ s/$decimalPoint/\./g;

    return $number;
}


# Function: getFileContent
# Récupère le contenu d'un fichier
#
# Parameters:
# string $filename - Nom du fichier
#
# Returns:
# string - Contenu du fichier
sub getFileContent
{
    my ($filename) = @_;

    open(TPL, $filename);
    my $content = join('', <TPL>);
    close(TPL);

    return $content;
}


sub decryptPassword
{
    my ($password, $key, $iv) = @_;

    my $cipher = Crypt::CBC->new(
        {
            'key'         => &base64Decode($key),
            'cipher'      => 'Crypt::OpenSSL::AES',
            'iv'          => &base64Decode($iv),
            'literal_key' => 1,
            'header'      => 'none',
            'keysize'     => 32
        }
    );

    my $decrypted = $cipher->decrypt(&base64Decode($password));
    return substr($decrypted, 0, -ord(substr($decrypted, -1)));
}

# Function: array_unique
# Dédoublonne un tableau
#
# Parameters:
# arrayref $array - Tableau à trier
#
# Returns:
# arrayref | array - Tableau trié
sub array_unique
{
    my ($array) = @_;

    my %seen = ();
    my @tab = ();
    foreach my $a (@$array)
    {
        if (!$seen{$a})
        {
            push (@tab, $a);
            $seen{$a} = 1;
        }
    }
    return (wantarray ? @tab : \@tab);
}


# Function: array_intersect
# Retourne un tableau contenant les valeurs de array1 qui sont présentes
# dans le tableau array2
#
# Parameters:
# arrayref $array1 - Tableau 1
# arrayref $array2 - Tableau 2
#
# Returns:
# arrayref | array - Intersection des 2 tableaux
sub array_intersect
{
    my ($array1, $array2) = @_;

    my %e = map { $_ => undef } @$array1;
    my @tab = grep { exists($e{$_}) } @$array2;

    return (wantarray ? @tab : \@tab);
}


# Function: array_minus
# Retourne un tableau contenant les valeurs du tableau array1 qui ne sont pas
# présentes dans le tableau array2
#
# Parameters:
# arrayref $array1 - Tableau 1
# arrayref $array2 - Tableau 2
#
# Returns:
# arrayref | array - Différence entre les 2 tableaux
sub array_minus
{
    my ($array1, $array2) = @_;

    my %e = map { $_ => undef } @$array2;
    my @tab = grep { !exists($e{$_}) } @$array1;

    return (wantarray ? @tab : \@tab);
}

sub hash_compare
{
    my ($hash1, $hash2) = @_;

    # Si les clés des deux tableaux sont différentes, alors les deux tableaux sont différents
    if (keys(%$hash1) != keys(%$hash2) || !(keys(%$hash1) ~~ keys(%$hash2)))
    {
        return 0;
    }

    foreach my $key (keys(%$hash1))
    {
        # Si une des valeurs est différente entre les deux tableaux, alors les deux tableaux sont différents
        if ($hash1->{$key} ne $hash2->{$key})
        {
            return 0;
        }
    }

    # Sinon, ils sont identiques
    return 1;
}

# Function: stripXmlIllegalCharacters
# Supprime les caractères qui ne sont pas valides en XML.
#
# Parameters:
# string $string     -    La chaîne de caractères sur laquelle appliquer la suppression.
# string $xmlVersion -    La version XML cible (XML_1_0 par défaut).
#
# Returns:
# string La chaîne sans les caractères non valides.
sub stripXmlIllegalCharacters
{
    my ($string, $xmlVersion) = @_;

    if (!defined $xmlVersion)
    {
        $xmlVersion = XML_1_0;
    }

    if (ref $string ne '')
    {
        return $string;
    }

    my $regex;

    if ($xmlVersion eq XML_1_0)
    {
        # https://en.wikipedia.org/wiki/Valid_characters_in_XML#XML_1.0
        # [^caractères valides]|[caractères non recommandés]
        $regex = '[^\x{0009}\x{000A}\x{000D}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}\x{10000}-\x{10FFFF}]|[\x{007F}-\x{0084}\x{0086}-\x{009F}]';
    }
    elsif ($xmlVersion eq XML_1_1)
    {
        # https://en.wikipedia.org/wiki/Valid_characters_in_XML#XML_1.1
        # [^caractères valides]|[caractères non recommandés]
        $regex = '[^\x{0001}-\x{D7FF}\x{E000}-\x{FFFD}\x{10000}-\x{10FFFF}]|[\x{0001}-\x{0008}\x{000B}-\x{000C}\x{000E}-\x{001F}\x{007F}-\x{0084}\x{0086}-\x{009F}]';
    }

    $string =~ s/$regex//ug;

    return $string;
}

# Function: encode
# Convertit toutes les chaînes de caractères d'un objet (clés et valeurs) en séquences d'octets de l'encodage demandé.
#
# Parameters:
# string $encoding - L'encodage de destination.
# string $obj      - L'objet à préparer.
#
# Returns:
# string L'objet préparé.
sub encode
{
    my ($encoding, $obj) = @_;

    if (ref($obj) eq 'ARRAY')
    {
        foreach (values(@$obj))
        {
            $_ = &encode($encoding, $_);
        }
    }
    elsif (ref($obj) eq 'HASH')
    {
        foreach (keys(%$obj))
        {
            $_ = &encode($encoding, $_);
            $obj->{$_} = &encode($encoding, $obj->{$_});
        }
    }
    else
    {
        $obj = &Encode::encode($encoding, $obj);
    }
    return $obj;
}

# Function: encodeUtf8
# Convertit toutes les chaînes de caractères d'un objet (clés et valeurs) en séquences d'octets UTF-8.
#
# Parameters:
# string $obj - L'objet à préparer.
#
# Returns:
# string L'objet préparé.
sub encodeUtf8
{
    my ($obj) = @_;

    return &encode(ENCODING_UTF8, $obj);
}

# Function: decode
# Considère toutes les chaînes de caractères d'un objet (clés et valeurs) comme des séquences d'octets
# de l'encodage demandé et les convertit en séquences de caractères.
#
# Parameters:
# string $encoding - L'encodage source.
# string $obj      - L'objet à préparer.
#
# Returns:
# string L'objet préparé.
sub decode
{
    my ($encoding, $obj) = @_;

    if (ref($obj) eq 'ARRAY')
    {
        foreach (values(@$obj))
        {
            $_ = &decode($encoding, $_);
        }
    }
    elsif (ref($obj) eq 'HASH')
    {
        foreach (keys(%$obj))
        {
            $_ = &decode($encoding, $_);
            $obj->{$_} = &decode($encoding, $obj->{$_});
        }
    }
    else
    {
        $obj = &Encode::decode($encoding, $obj);
    }
    return $obj;
}

# Function: decodeUtf8
# Considère toutes les chaînes de caractères d'un objet (clés et valeurs) comme des séquences d'octets UTF-8
# et les convertit en séquences de caractères.
#
# Parameters:
# string $obj - L'objet à préparer.
#
# Returns:
# string L'objet préparé.
sub decodeUtf8
{
    my ($obj) = @_;

    return &decode(ENCODING_UTF8, $obj);
}

# Function: latin1ToUtf8
# Convertit en UTF-8 une chaîne de caractères encodée en CP1252.
#
# Parameters:
# string $string - La chaîne de caractères en CP1252.
#
# Returns:
# string La chaîne de caractères en UTF-8.
sub latin1ToUtf8
{
    my ($string) = @_;

    return &encodeUtf8(&decode(ENCODING_CP1252, $string));
}

# Function: base64Decode
# Décode une chaîne de caractères encodée en Base 64.
#
# Parameters:
# string $string - La chaîne de caractères à décoder.
#
# Returns:
# string La chaîne décodée.
sub base64Decode
{
    my ($string) = @_;

    return &MIME::Base64::decode_base64($string);
}

# Function: base64Encode
# Encode une chaîne de caractères en Base 64.
#
# Parameters:
# string $string - La chaîne de caractères à encoder.
#
# Returns:
# string La chaîne encodée.
sub base64Encode
{
    my ($string) = @_;

    return &MIME::Base64::encode_base64($string, '');
}

# Function: utf8Base64Decode
# Décode une chaîne de caractères UTF-8 encodée en Base 64.
#
# Parameters:
# string $string - La chaîne de caractères à décoder.
#
# Returns:
# string La chaîne décodée.
sub utf8Base64Decode
{
    my ($string) = @_;

    return &decodeUtf8(&base64Decode($string));
}

# Function: utf8Base64Encode
# Encode une chaîne de caractères UTF-8 en Base 64.
#
# Parameters:
# string $string - La chaîne de caractères à encoder.
#
# Returns:
# string La chaîne encodée.
sub utf8Base64Encode
{
    my ($string) = @_;

    return &base64Encode(&encodeUtf8($string));
}

# Function: getHexMd5Digest
# Retourne l'empreinte MD5 au format hexadécimal d'une chaîne de caractères.
#
# Parameters:
# string $string - La chaîne de caractères.
#
# Returns:
# string L'empreinte MD5 au format hexadécimal.
sub getHexMd5Digest
{
    my ($string) = @_;

    return Digest::MD5->new()->add(&LOC::Util::encodeUtf8($string))->hexdigest();
}


# Function: toHTMLEntities
# Convertit tous les caractères éligibles en entités HTML
#
# Parameters:
# string $str - Chaîne
#
# Returns:
# string -
sub toHTMLEntities
{
    my ($str) = @_;

    $str =~ s{&}{&amp;}gso;
    $str =~ s{<}{&lt;}gso;
    $str =~ s{>}{&gt;}gso;
    $str =~ s{'}{&#39;}gso; # &apos;
    $str =~ s{"}{&quot;}gso; # &#34;

    return $str;
}

# Function: toJSEntities
# Convertir tous les caractères éligibles en entités JavaScript
#
# Parameters:
# string $str - Chaîne
#
# Returns:
# string -
sub toJSEntities
{
    my ($str) = @_;

    $str =~ s{\\}{\\x5c}gso;
    $str =~ s{"}{\\x22}gso;
    $str =~ s{'}{\\x27}gso;
    $str =~ s{\n}{\\x0A}gso;
    $str =~ s{\r}{\\x0D}gso;
    $str =~ s{\t}{\\x09}gso;
    $str =~ s{&}{\\x26}gso;
    $str =~ s{<}{\\x3c}gso;
    $str =~ s{>}{\\x3e}gso;

    return $str;
}

1;
