use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::User
# Module permettant d'obtenir les informations d'un utilisateur
package LOC::User;


# Constants: États
# STATE_ACTIVE   - Utilisateur actif (avec un profil)
# STATE_INACTIVE - Utilisateur inactif (sans profil)
use constant
{
    STATE_ACTIVE   => 'GEN01',
    STATE_INACTIVE => 'GEN03',
};

# Constants: États
# GET_ALL    - Récupérer tous les utilisateurs (même supprimés)
# GET_ACTIVE - Récupérer seulement les utilisateurs actifs
use constant
{
    GET_ACTIVE => 0x0000,
    GET_ALL    => 0x0001
};

# Constants: Groupes
# GROUP_AGENCYMANAGER     - Chef d'agence
# GROUP_TECHNICAL         - Technicien
# GROUP_COMMERCIAL        - Commercial
# GROUP_ACCOUNTING        - Comptabilité
# GROUP_TRANSPORT         - Transport
# GROUP_FOREMAN           - Chef d'atelier
# GROUP_TECHNICALPLATFORM - PSTN
# GROUP_TECHNICALMANAGER  - Responsable technique
# GROUP_TELEMATICS        - Télématique
use constant
{
    GROUP_AGENCYMANAGER     => 'CHEFAG',
    GROUP_TECHNICAL         => 'TECH',
    GROUP_COMMERCIAL        => 'COMM',
    GROUP_ACCOUNTING        => 'COMPTA',
    GROUP_TRANSPORT         => 'TRANSP',
    GROUP_FOREMAN           => 'CHEFAT',
    GROUP_TECHNICALPLATFORM => 'PSTN',
    GROUP_TECHNICALMANAGER  => 'RESPTECH',
    GROUP_TELEMATICS        => 'TELEMAT'
};

# Constants: Niveaux
# LEVEL_ADMIN  - Administrateur
# LEVEL_SUPERV - Superviseur
# LEVEL_USER   - Utilisateur
use constant
{
    LEVEL_ADMIN  => 100,
    LEVEL_SUPERV => 80,
    LEVEL_USER   => 20
};

# Constants: Identifiant de l'utilisateur admin
# ADMIN_ID  - Identifiant de l'utilisateur admin
use constant
{
    ADMIN_ID => 1
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Util;
use LOC::Agency;


# Sauvegarde des utilisateurs existants et actifs
my $_tabStatuses = {};


# Function: getInfos
# Retourne un hachage contenant les infos d'un utilisateur
#
# Contenu du hachage: Voir getList()
#
# Parameters:
# string|int $id         - Id ou login de l'utilisateur
# int        $flags      - Flags
# hashref    $tabOptions - Options supplémentaires (
#                           'isTitleDisplayed' : afficher le titre dans le nom complet (1) ou non (0),
#                           'formatMode' : formatage du nom complet (0 : nom en premier, 1 : prénom en premier) )
#
# Returns:
# hash -
sub getInfos
{
    my ($id, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GET_ACTIVE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Permet de saisir un login avec une ou plusieurs majuscules
    $id = lc($id);

    my $tabFilters = {};
    if (&LOC::Util::isNumeric($_[0]))
    {
        $tabOptions->{'index'} = 'id';
        $tabFilters->{'id'} = $_[0];
    }
    elsif ($_[0] ne '')
    {
        $tabOptions->{'index'} = 'login';
        $tabFilters->{'login'} = $_[0];
    }
    else
    {
        return (wantarray ? () : {});
    }

    my $tab = &getList(LOC::Util::GETLIST_ASSOC, $tabFilters, $flags, $tabOptions);
    return ($tab && exists $tab->{$id} ? (wantarray ? %{$tab->{$id}} : $tab->{$id}) : undef);
}

# Function: exists
# Vérifie si l'utilisateur correspondant à l'identifiant passé en paramètre existe.
#
# Parameters:
# string  $id - Identifiant de l'utilisateur
#
# Returns:
# int - 1 si l'utilisateur existe, 0 sinon.
sub exists
{
    my ($id) = @_;

    if ($id eq '')
    {
        return 0;
    }

    if (!defined $_tabStatuses->{$id}->{'exists'})
    {
        $_tabStatuses->{$id}->{'exists'} = (&getList(LOC::Util::GETLIST_COUNT, {'id' => $id}, GET_ALL) > 0);
    }

    return $_tabStatuses->{$id}->{'exists'};
}

# Function: isActive
# Vérifie si l'utilisateur correspondant à l'identifiant passé en paramètre est actif.
#
# Parameters:
# string  $id - Identifiant de l'utilisateur
#
# Returns:
# int - 1 si l'utilisateur est actif, 0 sinon.
sub isActive
{
    my ($id) = @_;

    if ($id eq '')
    {
        return 0;
    }

    if (!defined $_tabStatuses->{$id}->{'isActive'})
    {
        $_tabStatuses->{$id}->{'isActive'} = (&getList(LOC::Util::GETLIST_COUNT, {'id' => $id}, GET_ACTIVE) > 0);
    }

    return $_tabStatuses->{$id}->{'isActive'};
}

# Function: getList
# Retourne la liste des utilisateurs
#
# Parameters:
# int     $format     - Format de retour de la liste
# hashref $tabFilters - Filtres ('fullName' : recherche dans le nom et le prénom,
#                                'id' : recherche un id spécifique, même dans les utilisateurs supprimés,
#                                'agencyId' : liste d'agence)
# int     $flags      - Flags
# hashref $tabOptions - Options supplémentaires (
#                           'index' : index du tableau en sortie ('id' par défaut ou 'login'),
#                           'isTitleDisplayed' : afficher le titre dans le nom complet (1) ou non (0),
#                           'formatMode' : formatage du nom complet (0 : nom en premier, 1 : prénom en premier) )
#
# Returns:
# hash|hashref|0 - Liste des utilisateurs
sub getList
{
    my ($format, $tabFilters, $flags, $tabOptions) = @_;
    if (!defined $flags)
    {
        $flags = GET_ACTIVE;
    }
    if (!defined $tabOptions)
    {
        $tabOptions = {};
    }

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('frmwrk');

    my $query = '';

    # Index du tableau en sortie
    my $index = (defined $tabOptions->{'index'} ? $tabOptions->{'index'} : 'id');
    # Format d'affichage des noms
    my $isTitleDisplayed = (defined $tabOptions->{'isTitleDisplayed'} ? $tabOptions->{'isTitleDisplayed'} : 0);
    my $formatMode = (defined $tabOptions->{'formatMode'} ? $tabOptions->{'formatMode'} : 0);
    my $fullNameSql = 'CONCAT_WS(" ", ';
    if ($isTitleDisplayed)
    {
        $fullNameSql .= '"", ';
    }
    if ($formatMode == 0)
    {
        $fullNameSql .= 'usr_name, usr_firstname)';
    }
    else
    {
        $fullNameSql .= 'usr_firstname, usr_name)';
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    usr_id AS `id`,
    usr_login AS `login`,
    usr_title AS `title`,
    usr_name AS `name`,
    usr_firstname AS `firstName`,
    ' . $fullNameSql . ' AS `fullName`,
    usr_mail AS `email`,
    PE.PEPASSWORD AS `password`,
    PE.PROFILAUTO AS `profile.id`,
    PE.IMPRIMAUTO AS `printer.id`,
    usr_agc_id AS `agency.id`,
    agency.agc_id AS `agency.label`,
    agency.agc_cty_id AS `country.id`,
    country.cty_label AS `country.label`,
    nomadAgency.agc_id AS `nomadAgency.id`,
    nomadAgency.agc_label AS `nomadAgency.label`,
    nomadCountry.cty_id AS `nomadCountry.id`,
    nomadCountry.cty_label AS `nomadCountry.label`,
    nomadCountry.cty_lcl_id AS `locale.id`,
    cur_id AS `currency.id`,
    cur_symbol AS `currency.symbol`,
    -PE.NOMADEOK AS `isNomad`,
    PE.PENIVEAU AS `level`,
    PE.PEGROUPES AS `tabGroups`,
    usr_date_create AS `creationDate`,
    usr_date_modif AS `modificationDate`,
    usr_sta_id AS `state.id`,
    IF (PE.NOMADEOK <> 0, (SELECT COUNT(*) FROM AUTH.AGENCEAUTORISEE AA WHERE AA.PEAUTO = PE.PEAUTO), 0) AS `nbAuthorizedAgencies`
FROM f_user
LEFT JOIN AUTH.`PERSONNEL` PE
ON PE.PEAUTO = usr_id
LEFT JOIN p_agency agency
ON usr_agc_id = agency.agc_id
LEFT JOIN frmwrk.p_country country
ON agc_cty_id = country.cty_id
LEFT JOIN p_agency nomadAgency
ON nomadAgency.agc_id = PE.AGAUTO
LEFT JOIN frmwrk.p_country nomadCountry
ON nomadCountry.cty_id = nomadAgency.agc_cty_id
LEFT JOIN frmwrk.p_currency
ON cur_id = nomadCountry.cty_cur_id
WHERE 1';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*) AS `fullName`
FROM f_user
WHERE 1';
    }
    else
    {
        $query = '
SELECT
    usr_id AS `id`,
    ' . $fullNameSql . ' AS `fullName`
FROM f_user
WHERE 1';
    }

    if ($flags == GET_ACTIVE)
    {
        $query .= '
AND usr_sta_id != ' . $db->quote(STATE_INACTIVE);
    }

    # Recherche par le nom complet
    if ($tabFilters->{'fullName'} ne '')
    {
        my $search = $tabFilters->{'fullName'};
        if ($search !~ /%/)
        {
            $search = '%' . $search . '%';
        }
        $search = $db->quote($search);

        $query .= '
AND (usr_name LIKE ' . $search . ' OR usr_firstname LIKE ' . $search . ')';
    }

    # Recherche par id
    if ($tabFilters->{'id'} ne '')
    {
        $query .= '
AND usr_id = ' . $tabFilters->{'id'};
    }
    # Recherche par login
    if ($tabFilters->{'login'} ne '')
    {
        $query .= '
AND usr_login = "' . $tabFilters->{'login'} . '"';
    }

    # Recherche par agence
    if ($tabFilters->{'agencyId'} ne '')
    {
        $query .= '
AND usr_agc_id IN (' . $db->quote($tabFilters->{'agencyId'}) . ')';
    }

    $query .= '
ORDER BY `fullName`;';

    my $tab = $db->fetchList($format, $query, {}, $index);

    # Formatage des données
    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        foreach my $tabRow (values %$tab)
        {
            $tabRow->{'id'}         *= 1;
            $tabRow->{'profile.id'} *= 1;
            $tabRow->{'printer.id'} *= 1;
            $tabRow->{'isNomad'}    *= 1;
            $tabRow->{'level'}      *= 1;

            $tabRow->{'isSuperv'}   = ($tabRow->{'level'} >= LEVEL_SUPERV);
            $tabRow->{'isAdmin'}    = ($tabRow->{'level'} >= LEVEL_ADMIN);

            my @tabGroups = split(/\;/, $tabRow->{'tabGroups'});
            $tabRow->{'tabGroups'} = \@tabGroups;

            # Liste des agences autorisées
            $tabRow->{'tabAuthorizedAgencies'}  = &_getAuthorizedAgenciesList($db, $tabRow);
            $tabRow->{'tabAuthorizedAreas'}     = &_getAuthorizedAreasList($tabRow);
            $tabRow->{'tabAuthorizedCountries'} = &_getAuthorizedCountriesList($tabRow);
        }
    }
    return (wantarray ? %$tab : $tab);
}

# Function: getGroupsList
# Récupère la liste des groupes utilisateurs
#
# Returns:
# arrayref
sub getGroupsList
{
    return [
        {'id' => GROUP_ACCOUNTING,        'label' => 'Comptabilité'},
        {'id' => GROUP_TRANSPORT,         'label' => 'Transport'},
        {'id' => GROUP_COMMERCIAL,        'label' => 'Commercial'},
        {'id' => GROUP_AGENCYMANAGER,     'label' => 'Chef d\'agence'},
        {'id' => GROUP_TECHNICAL,         'label' => 'Technicien'},
        {'id' => GROUP_FOREMAN,           'label' => 'Chef d\'atelier'},
        {'id' => GROUP_TECHNICALPLATFORM, 'label' => 'PSTN'},
        {'id' => GROUP_TECHNICALMANAGER,  'label' => 'Resp. technique'},
        {'id' => GROUP_TELEMATICS,        'label' => 'Télématique'}
    ];
}

# Function: getLevelsList
# Récupère la liste des niveaux
#
# Returns:
# arrayref
sub getLevelsList
{
    return [
        {'id' => LEVEL_USER,   'label' => 'Utilisateur'},
        {'id' => LEVEL_SUPERV, 'label' => 'Superviseur'},
        {'id' => LEVEL_ADMIN,  'label' => 'Administrateur'}
    ];
}


# Function: setPrinterId
# Met à jour la dernière imprimante utilisée
#
# Parameters:
# int $userId    - Identifiant de l'utilisateur
# int $printerId - Identifiant de l'imprimante
#
# Returns:
# bool - 0 en cas d'erreur, 1 sinon
sub setPrinterId
{
    my ($userId, $printerId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('auth');

    if ($db->update('PERSONNEL', {'IMPRIMAUTO' => $printerId}, {'PEAUTO' => $userId}) == -1)
    {
        return 0;
    }
    return 1;
}


# Function: setNomadAgency
# Met à jour l'agence nomade d'un utilisateur
#
# Parameters:
# int    $userId   - Identifiant de l'utilisateur
# string $agencyId - Identifiant de l'agence nomade
#
# Returns:
# bool - 0 en cas d'erreur, 1 sinon
sub setNomadAgency
{
    my ($userId, $agencyId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('auth');

    # Mise à jour dans la base de données

    # Démarre une transaction
    $db->beginTransaction();

    if ($db->update('PERSONNEL', {'AGAUTO' => $agencyId}, {'PEAUTO' => $userId}) == -1)
    {
        # Annule la transaction
        $db->rollBack();
        return 0;
    }
    if ($db->update('PAPE', {'PACODE*' => '(SELECT agc_cty_id FROM frmwrk.p_agency WHERE agc_id = "' . $agencyId . '")'},
                            {'PEAUTO' => $userId}) == -1)
    {
        # Annule la transaction
        $db->rollBack();
        return 0;
    }

    # Valide la transaction
    $db->commit();

    return 1;
}


# Function: setProfile
# Met à jour le profil d'un utilisateur
#
# Parameters:
# int $userId    - Identifiant de l'utilisateur
# int $profileId - Identifiant du profil
#
# Returns:
# bool - 0 en cas d'erreur, 1 sinon
sub setProfile
{
    my ($userId, $profileId) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('auth');

    # Mise à jour dans la base de données
    if ($db->update('PERSONNEL', {'PROFILAUTO' => $profileId}, {'PEAUTO' => $userId}) == -1)
    {
        return 0;
    }

    return 1;
}


# Function: _getAuthorizedAgenciesList
# Récupère la liste des agences autorisées de l'utilisateur
#
# Parameters:
# LOC::Db::Adapter $db           - Connexion à la base de données
# hashref          $tabUserInfos - Informations sur l'utilisateur
#
# Returns:
# hashref - Tableau des agences autorisées

sub _getAuthorizedAgenciesList
{
    my ($db, $tabUserInfos) = @_;

    # Récupération de la liste des agences
    my $query = '
SELECT
    agc_id AS `id`,
    agc_label AS `name`,
    agc_type AS `type`,
    are_id AS `area.id`,
    are_label AS `area.name`,
    cty_id AS `country.id`,
    cty_label AS `country.name`
FROM p_agency
INNER JOIN p_country
ON cty_id = agc_cty_id
LEFT JOIN p_area
ON are_id = agc_are_id
WHERE agc_sta_id <> "' . LOC::Agency::STATE_DELETED . '"';
    if (!$tabUserInfos->{'isNomad'})
    {
        $query .= '
AND agc_id = "' . $tabUserInfos->{'nomadAgency.id'} . '"';
    }
    elsif ($tabUserInfos->{'nbAuthorizedAgencies'} > 0)
    {
        $query .= '
AND (agc_id IN (SELECT AA.AGAUTO FROM AUTH.AGENCEAUTORISEE AA WHERE AA.PEAUTO = ' . $tabUserInfos->{'id'} . ') OR
     agc_id = "' . $tabUserInfos->{'nomadAgency.id'} . '")';
    }
$query .= '
ORDER BY cty_label, agc_label;';
    my $tab = $db->fetchAssoc($query, {}, 'id');

    foreach my $tabRow (values %$tab)
    {
        $tabRow->{'area.id'} *= 1;
    }

    return $tab;
}


# Function: _getAuthorizedAreasList
# Récupère la liste des régions complètes autorisées de l'utilisateur
#
# Parameters:
# hashref  $tabUserInfos - Informations sur l'utilisateur
#
# Returns:
# arrayref - Tableau des régions autorisées

sub _getAuthorizedAreasList
{
    my ($tabUserInfos) = @_;

    my @tabAreas;
    my $total = 0;
    my @tab;

    foreach my $tabRow (values %{$tabUserInfos->{'tabAuthorizedAgencies'}})
    {
        if (!&LOC::Util::in_array($tabRow->{'area.id'}, \@tabAreas))
        {
            push(@tabAreas, $tabRow->{'area.id'});
        }
        $total++;
    }

    # Les régions autorisées sont-elles complètes ?
    my $nbAreas = @tabAreas;
    if ($nbAreas > 0)
    {
        my $countAgencies = &LOC::Agency::getList(LOC::Util::GETLIST_COUNT, {'area' => \@tabAreas});

        if ($countAgencies == $total)
        {
            @tab = @tabAreas;
        }
    }

    return \@tab;
}

# Function: _getAuthorizedCountriesList
# Récupère la liste des pays autorisés de l'utilisateur
#
# Parameters:
# arrayref  $tabUserInfos - Informations sur l'utilisateur
#
# Returns:
# arrayref - Tableau des pays autorisés

sub _getAuthorizedCountriesList
{
    my ($tabUserInfos) = @_;

    my @tabCountries;
    my $total = 0;
    my @tab;

    foreach my $tabRow (values %{$tabUserInfos->{'tabAuthorizedAgencies'}})
    {
        if (!&LOC::Util::in_array($tabRow->{'country.id'}, \@tabCountries))
        {
            push(@tabCountries, $tabRow->{'country.id'});
        }
        $total++;
    }

    # Les pays autorisés sont-ils complets ?
    my $nbCountries = @tabCountries;
    if ($nbCountries > 0)
    {
        my $countAgencies = &LOC::Agency::getList(LOC::Util::GETLIST_COUNT, {'country' => \@tabCountries});

        if ($countAgencies == $total)
        {
            @tab = @tabCountries;
        }
    }

    return \@tab;
}



1;
