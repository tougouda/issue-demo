use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Log::Functionality
# Module des fonctionnalités d'historique
package LOC::Log::Functionality;


# Constants: Liste des fonctionnalités
# CONTRACT         - Contrat
# ESTIMATE         - Devis
# ESTIMATELINE     - Lignedevis
# TRANSPORT        - Transport
# MACHINESFAMILY   - Famille de machines
# MODEL            - Modele
# MACHINE          - Machine
# MACHINEINVENTORY - Etat des lieux
use constant
{
    CONTRACT         => 'CONTRAT',
    ESTIMATE         => 'DEVIS',
    ESTIMATELINE     => 'LIGNEDEVIS',
    TRANSPORT        => 'TRANSPORT',
    MACHINESFAMILY   => 'FAMILLEMACHINES',
    MODEL            => 'MODELE',
    MACHINE          => 'MACHINE',
    MACHINEINVENTORY => 'ETATDESLIEUX'
};


use strict;

1;