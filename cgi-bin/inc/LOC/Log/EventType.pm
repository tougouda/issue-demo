use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Log::EventType
# Module des types d'évènements
package LOC::Log::EventType;


# Constants: Liste des types d'évènements
# ACTIVATESTARTCODEACTIVE        - Activation du digicode
# UPDDELIVERYDELEGAGENCY         - Changement de l'agence déléguée du transport de livraison
# UPDRECOVERYDELEGAGENCY         - Changement de l'agence déléguée du transport de récupération
# UPDRECOVERYTSFAGENCY           - Changement de l'agence de transfert du transport de récupération
# ADDDOCUMENT                    - Ajout d'un document
# ADDDEDUCTEDDAYS                - Ajout d'un jour déduit
# ADDADDEDDAYS                   - Ajout d'un jour supplémentaire
# ADDRENTSERVICE                 - Ajout d'un service de location ou de transport
# TELEMATICSEQUIPMENT            - Équipement télématique
# CANCELINVOICING                - Annulation de la facturation
# CANCEL                         - Annulation
# CANCELAGENCIESTRANSFER         - Annulation du transfert inter-agences
# STOP                           - Arrêt
# CONTRACTALLOCATION             - Attribution à un contrat
# MACHINEALLOCATION              - Attribution de machine
# CONSIDERRECOVERY               - Prise en compte de la récupération par l'atelier
# CONTRACTATTACHMENT             - Attachement à un contrat
# MACHINEINVENTORYDELATTACHMENT  - Attachement de l'état des lieux de livraison
# MACHINEINVENTORYRECATTACHMENT  - Attachement de l'état des lieux de récupération
# CONSIDERSITETRANSFER           - Prise en compte du transfert inter-chantiers par l'atelier
# TURNTOREPAIRDAYS               - Bascule en jours de remise en état
# TURNTOREPAIRSERVICES           - Bascule en services de remise en état
# UPDSITEADDRESS                 - Modification de l'adresse du chantier
# CHANGEAGENCY                   - Changement d’agence
# UPDSITEASBESTOS                - Modification de la présence d'amiante
# UPDINSURANCE                   - Modification de l'assurance
# UPDFUEL                        - Modification du carburant
# UPDCHARACTERISTIC              - Modification des caractéristiques
# CHANGECUSTOMER                 - Changement de client
# UPDDEDUCTEDDAYCOMMENT          - Modification du commentaire lié aux jours déduits
# UPDADDESDAYCOMMENT             - Modification du commentaire lié aux jours supplémentaires
# UPDSITECOMMENT                 - Modification des commentaires du chantier
# UPDCOST                        - Modification du coût
# UDPSITECONTACTNAME             - Modification du contact chantier
# CHANGECONTRACT                 - Changement de contrat
# UPDCALENDARDURATION            - Modification de la durée calendaire
# UPDRESIDUESMODE                - Modification de la gestion des déchets
# UPDDOCUMENT                    - Modification d'un document
# UPDBEGINDATE                   - Modification de la date de début
# UPDENDDATE                     - Modification de la date de fin
# UPDDURATION                    - Modification de la durée
# UPDELEVATION                   - Modification de l’élévation
# UPDLOAD                        - Modification de l’encombrement
# UPDBUSINESSFAMILY              - Modification de la famille commerciale
# UPDACCOUNTFAMILY               - Modification de la famille comptable
# UPDMACHINEFAMILY               - Modification de la famille de machines
# UPDTARIFFFAMILY                - Modification de la famille tarifaire
# UPDMANUFACTURER                - Modification du fournisseur
# UPDSTATGROUP                   - Modification du groupe statistique
# UPDDELIVERYHOUR                - Modification de l'horaire de livraison
# UPDRECOVERYHOUR                - Modification de l'horaire de récupération
# UPDHEIGHT                      - Modification de la hauteur
# UPDDEDUCTEDDAY                 - Modification d'un jour déduit
# UPDADDEDDAY                    - Modification d'un jour supplémentaire
# DODELIVERY                     - Réalisation de la livraison
# DORECOVERY                     - Réalisation de la récupération
# DOAGENCIESTRANSFER             - Réalisation du transfert inter-agences
# UPDLABEL                       - Modification du libellé
# UPDSITELABEL                   - Modification du libellé du chantier
# UPDISDELIVERYANTICIPATED       - Modification de la livraison anticipée possible
# CHANGEMACHINE                  - Changement de machine
# CHANGEWISHEDMACHINE            - Changement de machine souhaitée
# UPDSITECONTACTEMAIL            - Modification du mail chantier
# CHANGEMODEL                    - Changement de modèle de machines
# UPDDELIVERYAMOUNT              - Changement du montant du transport de livraison
# UPDRECOVERYAMOUNT              - Changement du montant du transport de récupération
# UPDCLEANING                    - Modification du nettoyage
# UPDENERGY                      - Modification de l’énergie
# UPDCONTROLPERIODICTY           - Modification de la périodicité des VGP
# UPDRENTAMOUNT                  - Modification du prix de location
# UPDHASWHARF                    - Modification du quai
# UPDAPPEAL                      - Modification de la gestion de recours
# UPDRENTSERVICE                 - Services de location et de transport
# UPDSITECONTACTTELEPHONE        - Modification du téléphone chantier
# UPDDELIVERY                    - Changement du transport de livraison
# UPDRECOVERY                    - Changement du transport de récupération
# UPDMODELTYPE                   - Modification du typage
# UPDATESITECITY                 - Changement de ville chantier (ville et zone)
# UPDMACHINEVISIBILITY           - Modification de la visibilité
# ACCEPTESTIMATELINE             - Acceptation de la ligne
# CREATION                       - Création
# MOBILECREATION                 - Saisie sur l'application mobile
# CHECKCUSTOMERS                 - Contrôle des clients
# CHECKCONTRACTS                 - Contrôle des contrats
# DESACTIVATESTARTCODE           - Désactivation du digicode
# CONTRACTDEALLOCATION           - Désattribution d'un contrat
# MACHINEDEALLOCATION            - Désattribution de machine
# CONTRACTDETACHMENT             - Détachement d'un contrat
# MACHINEINVENTORYDELDETACHMENT  - Détachement de l'état des lieux de livraison
# MACHINEINVENTORYRECDETACHMENT  - Détachement de l'état des lieux de récupération
# SMSSTARTCODE                   - Envoi du code de démarrage par SMS
# SAVE                           - Enregistrement
# SAGEFILEEXPORT                 - Export du fichier Sage
# GENRENTINVOICE                 - Génération de facture de location
# INVOICE                        - Mise en facturation
# INVOICEDEDUCTEDDAY             - Mise dans l'état facturable d'un jour déduit
# INVOICEREPAIRDAY               - Mise en facturation de jours de remise en état
# INVOICEADDEDDAY                - Mise dans l'état facturable d'un jour supplémentaire
# INVOICEREPAIRSERVICE           - Mise en facturation d'un service de remise en état
# PRINT                          - Impression
# GENKIMOCEMAINTENANCE           - Génération de demande d'entretien Kimoce
# GENKIMOCEINTERVENTION          - Génération de demande d'intervention Kimoce
# LAUNCHINVOICING                - Lancement de la facturation
# UPDMACHINESITEBLOCKED          - Blocage sur le chantier
# CHANGEMACHINEAGENCY            - Modification d'agence
# UPDMACHINEDATECONTROL          - Saisie d'une date de contrôle
# UPDMACHINESITEUNBLOCKED        - Déblocage sur le chantier
# UPDMACHINESTATE                - Modification de l'état de la machine
# UPDMACHINEOUTOFPARK            - Modification de l'état parc
# UPDMACHINESERIALNUMBER         - Modification du numéro de série
# AGENCIESTRANSFER               - Transfert inter-agences
# SENDMAIL                       - Envoi par e-mail
# SENDMAILDOCUMENTS              - Envoi de documents par e-mail
# SENDMAILBREAKDOWNDECLARATION   - Envoi d'une déclaration de panne
# UPDADDMACHINEDELIVERY          - Changement de la machine supplémentaire de livraison
# UPDADDMACHINERECOVERY          - Changement de la machine supplémentaire de récupération
# UNINVOICEDEDUCTEDDAY           - Mise dans l'état non facturable d'un jour déduit
# UNINVOICEADDEDDAY              - Mise dans l'état non facturable d'un jour supplémentaire
# UNINVOICEREPAIRDAY             - Mise en non facturation de jours de remise en état
# UNINVOICEREPAIRSERVICE         - Mise en non facturation d'un service de remise en état
# UNSYNCOTDDELIVERY              - Désynchronisation OTD du transport de livraison
# UNSYNCOTDRECOVERY              - Désynchronisation OTD du transport de récupération
# SYNCOTDDELIVERY                - Resynchronisation OTD du transport de livraison
# SYNCOTDRECOVERY                - Resynchronisation OTD du transport de récupération
# UPLOADPDF                      - Téléchargement du fichier PDF
# REACTIVATEREPAIRSHEET          - Remise en cours d'une fiche de remise en état
# RESTOP                         - Remise en arrêt
# REACTIVATE                     - Remise en cours
# REACTIVATETELEMATICS           - Réactivation télématique
# SENDSMS                        - Envoi par SMS
# SUSPENDTELEMATICS              - Suspension télématique
# DELDOCUMENT                    - Suppression d'un document
# DELDEDUCTEDDAY                 - Suppression d'un jour déduit
# DELADDEDDAY                    - Suppression d'un jour supplémentaire
# DELETE                         - Suppression
# DELRENTSERVICE                 - Suppression d'un service de location ou de transport
# DISABLETELEMATICS              - Déséquipement télématique
# UNDODELIVERY                   - Annulation de la réalisation de la livraison
# UNDORECOVERY                   - Annulation de la réalisation de la récupération
# UNDOAGENCIESTRANSFER           - Annulation de la réalisation du transfert inter-agences
# UNACCEPTESTIMATELINE           - Annulation de l'acceptation de la ligne
use constant
{
    ACTIVATESTARTCODEACTIVE       => 'ACTDIGI',
    UPDDELIVERYDELEGAGENCY        => 'AGDELLIV',
    UPDRECOVERYDELEGAGENCY        => 'AGDELREC',
    UPDRECOVERYTSFAGENCY          => 'AGTSFREC',
    ADDDOCUMENT                   => 'AJDOC',
    ADDDEDUCTEDDAYS               => 'AJJDED',
    ADDADDEDDAYS                  => 'AJJSUP',
    ADDRENTSERVICE                => 'AJSLT',
    TELEMATICSEQUIPMENT           => 'AJTELEM',
    CANCELINVOICING               => 'ANFAC',
    CANCEL                        => 'ANNUL',
    CANCELAGENCIESTRANSFER        => 'ANTSF',
    STOP                          => 'ARRET',
    CONTRACTALLOCATION            => 'ATCTT',
    MACHINEALLOCATION             => 'ATMAC',
    CONSIDERRECOVERY              => 'ATREC',
    CONTRACTATTACHMENT            => 'ATTACTT',
    MACHINEINVENTORYDELATTACHMENT => 'ATTEDLL',
    MACHINEINVENTORYRECATTACHMENT => 'ATTEDLR',
    CONSIDERSITETRANSFER          => 'ATTSF',
    TURNTOREPAIRDAYS              => 'BAJRE',
    TURNTOREPAIRSERVICES          => 'BASRE',
    UPDSITEADDRESS                => 'CHADRCH',
    CHANGEAGENCY                  => 'CHAG', # non utilisé
    UPDSITEASBESTOS               => 'CHAMIA',
    UPDINSURANCE                  => 'CHASS',
    UPDFUEL                       => 'CHCAR',
    UPDCHARACTERISTIC             => 'CHCARAC',
    CHANGECUSTOMER                => 'CHCLI', # non utilisé
    UPDDEDUCTEDDAYCOMMENT         => 'CHCOMJRD',
    UPDADDEDDAYCOMMENT            => 'CHCOMJRS',
    UPDSITECOMMENT                => 'CHCOMMCH',
    UPDCOST                       => 'CHCOUT',
    UDPSITECONTACTNAME            => 'CHCTCCH',
    CHANGECONTRACT                => 'CHCTT',
    UPDCALENDARDURATION           => 'CHDCAL',
    UPDRESIDUESMODE               => 'CHDEC',
    UPDDOCUMENT                   => 'CHDOC',
    UPDBEGINDATE                  => 'CHDTD',
    UPDENDDATE                    => 'CHDTF',
    UPDDURATION                   => 'CHDUR',
    UPDELEVATION                  => 'CHELEV',
    UPDLOAD                       => 'CHENCOM',
    UPDBUSINESSFAMILY             => 'CHFACOM',
    UPDACCOUNTFAMILY              => 'CHFACPTA',
    UPDMACHINEFAMILY              => 'CHFAMA',
    UPDTARIFFFAMILY               => 'CHFAMTAR',
    UPDMANUFACTURER               => 'CHFOUR',
    UPDSTATGROUP                  => 'CHGPSTAT',
    UPDDELIVERYHOUR               => 'CHHLIV',
    UPDRECOVERYHOUR               => 'CHHREC',
    UPDHEIGHT                     => 'CHHT',
    UPDDEDUCTEDDAY                => 'CHJDED',
    UPDADDEDDAY                   => 'CHJSUP',
    DODELIVERY                    => 'CHKLIV',
    DORECOVERY                    => 'CHKREC',
    DOAGENCIESTRANSFER            => 'CHKTSF',
    UPDLABEL                      => 'CHLIB',
    UPDSITELABEL                  => 'CHLIBCH',
    UPDISDELIVERYANTICIPATED      => 'CHLIVANT',
    CHANGEMACHINE                 => 'CHMAC',
    CHANGEWISHEDMACHINE           => 'CHMACSH',
    UPDSITECONTACTEMAIL           => 'CHMAILCH',
    CHANGEMODEL                   => 'CHMOD',
    UPDDELIVERYAMOUNT             => 'CHMTLIV',
    UPDRECOVERYAMOUNT             => 'CHMTREC',
    UPDCLEANING                   => 'CHNET',
    UPDENERGY                     => 'CHNRJ',
    UPDCONTROLPERIODICTY          => 'CHPERVGP',
    UPDRENTAMOUNT                 => 'CHPLO',
    UPDHASWHARF                   => 'CHQUAI',
    UPDAPPEAL                     => 'CHRECO',
    UPDRENTSERVICE                => 'CHSLT',
    UPDSITECONTACTTELEPHONE       => 'CHTELCH',
    UPDDELIVERY                   => 'CHTSPLIV',
    UPDRECOVERY                   => 'CHTSPREC',
    UPDMODELTYPE                  => 'CHTYPAG',
    UPDATESITECITY                => 'CHVIL',
    UPDMACHINEVISIBILITY          => 'CHVIS',
    ACCEPTESTIMATELINE            => 'CKACC',
    CREATION                      => 'CREAT',
    MOBILECREATION                => 'CREATMOB',
    CHECKCUSTOMERS                => 'CTCLT',
    CHECKCONTRACTS                => 'CTCTT',
    DEACTIVATESTARTCODE           => 'DACDIGI',
    CONTRACTDEALLOCATION          => 'DATCTT',
    MACHINEDEALLOCATION           => 'DATMAC',
    CONTRACTDETACHMENT            => 'DETACTT',
    MACHINEINVENTORYDELDETACHMENT => 'DETEDLL',
    MACHINEINVENTORYRECDETACHMENT => 'DETEDLR',
    SMSSTARTCODE                  => 'DIGISMS',
    SAVE                          => 'ENREG',
    SAGEFILEEXPORT                => 'EXPSG',
    GENRENTINVOICE                => 'FACLO',
    INVOICE                       => 'FACTU',
    INVOICEDEDUCTEDDAY            => 'FAJDED',
    INVOICEREPAIRDAY              => 'FAJRE',
    INVOICEADDEDDAY               => 'FAJSUP',
    INVOICEREPAIRSERVICE          => 'FASRE',
    PRINT                         => 'IMPR',
    GENKIMOCEMAINTENANCE          => 'KIMENT',
    GENKIMOCEINTERVENTION         => 'KIMINT',
    LAUNCHINVOICING               => 'LCFAC',
    UPDMACHINESITEBLOCKED         => 'MACBLQ',
    CHANGEMACHINEAGENCY           => 'MACCHAG',
    UPDMACHINECONTROLDATE         => 'MACCTRL',
    UPDMACHINESITEUNBLOCKED       => 'MACDEBLQ',
    UPDMACHINESTATE               => 'MACETAT',
    UPDMACHINEOUTOFPARK           => 'MACPARC',
    UPDMACHINESERIALNUMBER        => 'MACSERIE',
    AGENCIESTRANSFER              => 'MACTSF',
    SENDMAIL                      => 'MAIL',
    SENDMAILDOCUMENTS             => 'MAILDOC',
    SENDMAILBREAKDOWNDECLARATION  => 'MAILDPAN',
    UPDADDMACHINEDELIVERY         => 'MASUPLIV',
    UPDADDMACHINERECOVERY         => 'MASUPREC',
    UNINVOICEDEDUCTEDDAY          => 'NFAJDED',
    UNINVOICEADDEDDAY             => 'NFAJSUP',
    UNINVOICEREPAIRDAY            => 'NFJRE',
    UNINVOICEREPAIRSERVICE        => 'NFSRE',
    UNSYNCOTDDELIVERY             => 'OTDDSYNL',
    UNSYNCOTDRECOVERY             => 'OTDDSYNR',
    SYNCOTDDELIVERY               => 'OTDSYNL',
    SYNCOTDRECOVERY               => 'OTDSYNR',
    UPLOADPDF                     => 'PDF',
    REACTIVATEREPAIRSHEET         => 'RCFIC',
    RESTOP                        => 'REMAR',
    REACTIVATE                    => 'REMCO',
    REACTIVATETELEMATICS          => 'RETELEM',
    SENDSMS                       => 'SMS',
    SUSPENDTELEMATICS             => 'SPTELEM',
    DELDOCUMENT                   => 'SUDOC',
    DELDEDUCTEDDAY                => 'SUJDED',
    DELADDEDDAY                   => 'SUJSUP',
    DELETE                        => 'SUPPR',
    DELRENTSERVICE                => 'SUSLT',
    DISABLETELEMATICS             => 'SUTELEM',
    UNDODELIVERY                  => 'UCHKLIV',
    UNDORECOVERY                  => 'UCHKREC',
    UNDOAGENCIESTRANSFER          => 'UCHKTSF',
    UNACCEPTESTIMATELINE          => 'UCKACC'
};


use strict;

1;