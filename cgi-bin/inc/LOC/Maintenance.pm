use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Maintenance
# Module de gestion du mode maintenance.
package LOC::Maintenance;


# Constants: Erreurs
# ERROR_FATAL                     - Erreur fatale.
# ERROR_RIGHTS                    - Droits insuffisants.
# ERROR_MAINTENANCE_UNKNOWNCHARAC - Caractéristique pour la maintenance inconnue.
use constant
{
    ERROR_FATAL                     => 0x0001,
    ERROR_RIGHTS_SET                => 0x0002,
    ERROR_MAINTENANCE_UNKNOWNCHARAC => 0x0010
};


# Constants: URL
# URL_INFO_PAGE - L'URL de la page de maintenance.
use constant
{
    URL_INFO_PAGE => 'default:maintenance:info',
    URL_CHECK     => 'default:maintenance:check'
};


use strict;

# Modules internes
use LOC::Characteristic;
use LOC::Db;
use LOC::Json;
use LOC::Request;
use LOC::Session;


# Function: getSettings
# Retourne le paramétrage du mode maintenance.
#
# Parameters:
# int $disableCache - Indique si le cache doit être désactivé (1) ou non (0, valeur par défaut).
#
# Returns:
# hash|hashref|0 - Le paramétrage du mode maintenance.
sub getSettings
{
    my ($disableCache) = @_;  

    my $charac = &LOC::Characteristic::getGlobalValueByCode(
            LOC::Characteristic::MAINTENANCE,
            undef,
            undef,
            $disableCache
        );

    if ($charac ne '')
    {
        my $tabInfo = &LOC::Json::fromJson($charac);
        return (wantarray ? %$tabInfo : $tabInfo);
    }

    return (wantarray ? () : {});
}


# Function: isActivated
# Indique si le mode maintenance est activé.
#
# Returns:
# bool - 1 si le mode maintenance est activé, 0 sinon.
sub isActivated
{
    my $tabSettings = &getSettings();
    return ($tabSettings->{'isActivated'} ? 1 : 0);
}


# Function: canOverride
# Indique si l'utilisateur peut outrepasser le mode maintenance.
#
# Parameters:
# hashref $tabUserInfos - Informations sur l'utilisateur
#
# Returns:
# bool - 1 si l'utilisateur peut outrepasser le mode maintenance, 0 sinon.
sub canOverride
{
    my ($module, $controller, $action, $tabUserInfos) = @_;

    my @tabParts = split(/:/, URL_CHECK);

    if ($module . ':' . $controller . ':' . $action eq URL_CHECK)
    {
        return 1;
    }

    my $tabSettings = &getSettings();
    return (exists $tabSettings->{'level'} && $tabUserInfos->{'level'} >= $tabSettings->{'level'} ? 1 : 0);
}


# Function: setSettings
# Définit le paramétrage du mode maintenance.
#
# Parameters:
# hashref  $tabData   - Les paramètres.
# int      $userId    - L'identifiant de l'utilisateur qui fait la modification.
# hashref  $tabErrors - Le tableau des erreurs.
#
# Returns:
# bool - 1 si les paramètres ont été mis à jour, 0 sinon.
sub setSettings
{
    my ($tabData, $userId, $tabErrors) = @_;

    if (!defined $tabErrors)
    {
        $tabErrors = {};
    }
    if (!defined $tabErrors->{'fatal'})
    {
        $tabErrors->{'fatal'} = [];
    }


    # Informations sur l'utilisateur
    my $tabUserInfos = &LOC::Session::getUserInfos();
    if ($tabUserInfos->{'id'} != $userId)
    {
        $tabUserInfos = &LOC::User::getInfos($userId);
    }

    # Vérification des droits
    my $tabRights = &getRights($tabUserInfos);
    if (!$tabRights->{'set'})
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_RIGHTS_SET);
        return 0;
    }

    my $db = &LOC::Db::getConnection('auth');
    my $hasTransaction = $db->hasTransaction();

    # Démarre la transaction
    if (!$hasTransaction)
    {
        $db->beginTransaction();
    }

    my $tabCharacInfo = &_getCharacInfo();

    if ($tabCharacInfo == 0)
    {
        push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
    }
    else
    {
        my $tabUpdates = {
            'cgl_value'  => &LOC::Json::toJson($tabData)
        };

        if ($tabCharacInfo->{'global.id'})
        {
            if ($db->update('p_characteristic_global', $tabUpdates, {'cgl_id' => $tabCharacInfo->{'global.id'}}) == -1)
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            }
        }
        elsif ($tabCharacInfo->{'id'})
        {
            $tabUpdates->{'cgl_chr_id'} = $tabCharacInfo->{'id'};
            if ($db->insert('p_characteristic_global', $tabUpdates) == -1)
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            }
        }
        else
        {
            push(@{$tabErrors->{'fatal'}}, ERROR_MAINTENANCE_UNKNOWNCHARAC);
        }

        # Valide la transaction
        if (!$hasTransaction)
        {
            if (!$db->commit())
            {
                push(@{$tabErrors->{'fatal'}}, ERROR_FATAL);
            }
        }
    }

    # Erreurs
    if (@{$tabErrors->{'fatal'}})
    {
        $db->rollBack();
        return 0;
    }

    return 1;
}


# Function: getRights
# Récupère la liste des droits.
#
# Parameters:
# hashref $tabUserInfos - Les informations sur l'utilisateur.
#
# Returns:
# hashref - Le tableau des droits.
sub getRights
{
    my ($tabUserInfos) = @_;

    my $isUserAdmin = $tabUserInfos->{'isAdmin'};

    my $tabRights = {};

    # Initialisation des droits
    &LOC::Util::setTreeValues($tabRights, ['set', 'preview'], 0);

    # Administrateurs
    if ($isUserAdmin)
    {
        &LOC::Util::setTreeValues($tabRights, ['set', 'preview'], 1);
    }

    return $tabRights;
}


# Function: getInfoPageMca
# Récupère l'URL de la page de maintenance sous la forme Module/Contrôleur/Action.
#
# Returns:
# hashref - Un tableau contenant les clés suivantes : module, controller, action.
sub getInfoPageMca
{
    my @tabParts = split(/:/, URL_INFO_PAGE);

    return {
        'module'     => $tabParts[0],
        'controller' => $tabParts[1],
        'action'     => $tabParts[2]
    };
}


# Function: getInfoPageUrl
# Récupère l'URL de la page de maintenance.
#
# Returns:
# string - L'URL de la page de maintenance.
sub getInfoPageUrl
{
    return &LOC::Request::createRequestUri(URL_INFO_PAGE);
}


# Function: _getCharacInfo
# Récupère les informations sur la caractéristique :
# - id : identifiant de la caractéristique
# - global.id : identifiant de la valeur globale de la caractéristique
#
# Returns:
# hash|hashref|0 - Un tableau contenat les informations sur la caractéristique, 0 en cas d'erreur.
sub _getCharacInfo
{
    my $db = &LOC::Db::getConnection('auth');

    # Récupération de l'identifiant
    my $query = '
SELECT
    chr_id AS `id`,
    cgl_id AS `global.id`
FROM p_characteristic
LEFT JOIN p_characteristic_global ON (chr_id = cgl_chr_id AND cgl_date_end IS NULL)
WHERE chr_code = ' . $db->quote(LOC::Characteristic::MAINTENANCE) . ';';
    my $tabInfo = $db->fetchRow($query);

    return (wantarray ? %$tabInfo : $tabInfo);
}

1;