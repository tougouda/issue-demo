use utf8;
use open (':encoding(UTF-8)');

# Class: LOC::Template
# Module de formatage de template
package LOC::Template;


use strict;

use Number::Format;
use POSIX;
use LOC::Globals;
use LOC::Locale;


# Function: formatDate
# Formate une date à partir d'un template
#
# Parameters:
# string $template     - la chaîne template à formater
# string $tabReplaces - les valeurs de la chaîne à mettre dans le template
#
# Returns:
# string
sub formatDate
{
    my ($template, $tabReplaces) = @_;

    my @params = &_getParams($template);

    my $file = &LOC::Globals::get('cgiPath') . '/locale/' . $params[2] . '/LC_TIME';
    open(LCTIME, $file);
    my @tabFileContent = <LCTIME>;
    close(LCTIME);

    my %tabDateFormat = eval(join(' ', @tabFileContent));
    my $tabFormat = \%tabDateFormat;
    my $template = $tabFormat->{$params[1]};

    my ($date, $time) = split(/ /, $tabReplaces->{$params[0]});

    my @tabDate = split(/\-/, $date);
    my @tabTime = split(/:/, $time);

    if ($date ne '')
    {
        return POSIX::strftime($template, $tabTime[2], $tabTime[1], $tabTime[0],
                           $tabDate[2], $tabDate[1] - 1, $tabDate[0] - 1900);
    }
    else
    {
        return '';
    }
}


# Function: formatCurrency
# Formate une valeur monétaire à partir d'un template
#
# Parameters:
# string $template    - la chaîne template à formater
# string $tabReplaces - les valeurs de la chaîne à mettre dans le template
#
# Returns:
# string
sub formatCurrency
{
    my ($template, $tabReplaces) = @_;

    my @params = &_getParams($template);

    my $oldLocale = &POSIX::setlocale(POSIX::LC_MONETARY);
    &POSIX::setlocale(POSIX::LC_MONETARY, $params[3]);
    my $numberFormat = Number::Format->new();
    my $return = $numberFormat->format_price($tabReplaces->{$params[0]} * 1, $params[1] * 1, $params[2]);
    &POSIX::setlocale(POSIX::LC_MONETARY, $oldLocale);

    return $return;
}

# Function: formatSingularPlural
# Formate une chaîne singulier/pluriel suivant un nombre donné
#
# Parameters:
# string $template    - la chaîne template à formater
# string $tabReplaces - les valeurs de la chaîne à mettre dans le template
#
# Returns:
# string
sub formatSingularPlural
{
    my ($template, $tabReplaces) = @_;

    my @params = &_getParams($template);

    my $nb = $tabReplaces->{$params[0]};

    # Locale
    my $locale = &LOC::Locale::getLocale($params[3], undef, [POSIX::LC_MESSAGES, POSIX::LC_CTYPE]);

    return $locale->tn($params[1], $params[2], $nb, $nb);
}


# Function: _getParams
# Récupère les paramètres à partir d'un template à formater
#
# Parameters:
# string $template - Template
#
# Returns:
# array
sub _getParams
{
    my ($template) = @_;

    return split(/\|/, $template);
}
1;
