use utf8;
use open (':encoding(UTF-8)');

# Class: LOC::Xml
# Module gérant les données XML
package LOC::Xml;


use strict;

use LOC::Util;

# Function: toXMLEntity
# Remplace les caractères &, ", < et >.
#
# Parameters:
# string   $value  - Chaine de caractères à convertir
#
# Returns:
# string
sub toXMLEntity
{
    my $value = $_[0];

    $value =~ s/&/&amp;/g;
    $value =~ s/"/&quot;/g;
    $value =~ s/</&lt;/g;
    $value =~ s/>/&gt;/g;

    return &LOC::Util::stripXmlIllegalCharacters($value);
}


# Function: getTagOuterXML
# Génére une trame Xml par rapport au tableau passé en paramètre
#
# Parameters:
# string          $tagName          Le nom de la balise.
# string|arrayref $content          Le contenu de la balise.
# arrayref        $tabAttributes    La liste des attributs de la balise.
#
# Returns:
# string - Trame xml
sub getTagOuterXML
{
    my ($tagName, $content, $tabAttributes) = @_;

    # Contenu
    my $contentXml = '';
    if (ref $content ne 'ARRAY')
    {
        $contentXml = &toXMLEntity($content);
    }
    else
    {
        my $nb = @$content;
        for (my $i = 0; $i < $nb; $i++)
        {
            $contentXml .= &getTagOuterXML(@{$content->[$i]});
        }
    }

    # Nom de la balise
    my $xml = '<' . $tagName;

    # Attributs de la balise
    if (defined $tabAttributes)
    {
        my $nb = @$tabAttributes;
        for (my $i = 0; $i < $nb; $i++)
        {
            $xml .=  ' ' . $tabAttributes->[$i]->[0] . '="' . &toXMLEntity($tabAttributes->[$i]->[1]) . '"';
        }
    }

    # Fermuture de la balise
    if ($contentXml ne '')
    {
        $xml .= '>' . $contentXml . '</' . $tagName . '>';
    }
    else
    {
        $xml .= '/>';
    }

    return $xml;
}


1;
