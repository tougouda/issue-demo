use utf8;
use open (':encoding(UTF-8)');

# Class: LOC::ForkProcess
# Module gérant les forks de processus
package LOC::ForkProcess;


use strict;

use LOC::Db;


sub fork
{
    my ($childSub, $isCloseOutput) = @_;

    if (!$childSub)
    {
        return;
    }

    # Lors de la fermeture du processus fils, celui-ci ferme toutes les connexions existantes, y compris celles en cours
    # d'utilisation par le processus père.
    # En fermant les connexions actives avant de faire le fork, les processus père et fils recréeront chacun leurs connexions.
    &LOC::Db::closeConnections();

    # Pour ne pas accumuler les zombies
    $SIG{CHLD} = 'IGNORE';

    # Création du processus fils
    my $pid = fork();

    # Problème de création du processus fils
    if (!defined $pid)
    {
        die "Impossible de créer le processus fils : $!\n";
    }

    if ($pid == 0)
    {
        if ($isCloseOutput)
        {
            close STDOUT;
            close STDERR;
        }

        my $result = $childSub->();
        if (!$result)
        {
            exit 1;
        }
        exit;
    }
}

1;
