use utf8;
use open (':encoding(UTF-8)');

# Class: LOC::Globals
# Classe de gestion des locales
package LOC::Globals;


# Constants: Type de logo
# LOGO_TYPE_VECTOR - Logo vectoriel.
# LOGO_TYPE_RASTER - Logo bitmap.
use constant
{
    LOGO_TYPE_VECTOR => 'vector',
    LOGO_TYPE_RASTER => 'raster',
};

use strict;

use LOC::Characteristic;
use LOC::Json;
use LOC::Util;
use LOC::Log;

# Variable: %globals
# Contient les variables globales de l'application
my %globals = (
    'appProjectName'  => 'loc.web',
    'version'         => '3.12.1-p2',
    'release'         => 1,
    'build'           => 4,
    'X-UA-Compatible' => 'IE=edge, chrome=1',
    'appId'           => 3
);

# Variable: %servers
# Liste des serveurs
my %servers;

# Variable: %connections
# Liste des connexions aux bases de données
my %connections;



# -------------------------------------------------------------------------------------------------


# Environnement
if (!defined $ENV{'ENV'})
{
    &LOC::Log::alert('Impossible de détecter l\'environnement. Vérifiez que la variable serveur ENV est définie.');
    exit;
}

# Vecteur d'initialisation clé de cryptage
my $iv = '3/iCm+PaSayitFx9Sf9HsQ==';

$globals{'env'}              = $ENV{'ENV'};

# Développement
if ($globals{'env'} eq 'dev')
{
    # Numéro de la base de données
    # - passé en argument de la ligne de commande (exemple : perl script.pl dev=2)
    foreach my $arg (@ARGV)
    {
        if ($arg =~ /dev=(.+?)/)
        {
            $globals{'dev'} = $1;
            last;
        }
    }
    # - passé en paramètre de l'URL (exemple : http://intranet.acces-industrie.com/appli/location/cgi-bin/script.pl?dev=2)
    if ($globals{'dev'} eq '')
    {
        ($globals{'dev'}) = ($ENV{'QUERY_STRING'} =~ /dev=(.+?)/);
    }
    # - 1 par défaut
    if ($globals{'dev'} eq '')
    {
        $globals{'dev'} = 1;
    }
}

# URL et chemins vers les applications
$globals{'locationUrl'}      = $ENV{'LOC_URL'};
$globals{'intranetUrl'}      = $ENV{'INTRANET_URL'};
$globals{'reportUrl'}        = $ENV{'RPT_URL'};
$globals{'tariffTrspUrl'}    = $ENV{'TFT_URL'};
$globals{'glpiUrl'}          = $ENV{'GLPI_URL'};
$globals{'oldFrameworkPath'} = $ENV{'FWOLD_HOME'} . '/v139';
$globals{'servicesUrl'}      = $ENV{'SERVICES_URL'};

$globals{'cgiPath'}      = $ENV{'LOC_HOME'} . '/cgi-bin';
$globals{'cgiOldPath'}   = $globals{'cgiPath'} . '/old';
$globals{'webPath'}      = $ENV{'LOC_HOME'} . '/web';
$globals{'servicesPath'} = $ENV{'SERVICES_HOME'};

# Fichiers communs
$globals{'commonFilesPath'} = $ENV{'COMMONFILES_HOME'} . '/loc';
$globals{'commonFilesUrl'}  = $ENV{'COMMONFILES_URL'} . '/loc';

# Montages
$globals{'OTDMountPath'} = $ENV{'OTDEXCHANGE_HOME'};

# Configurations des applications Framework v2.x.x
$globals{'fwApplicationsCfgs'} = {
    'GUI' => {
        'url' => $ENV{'GUI_URL'}
    },
    'CRM' => {
        'url' => $ENV{'CRM_URL'}
    },
    'CRC' => {
        'url' => $ENV{'CRC_URL'}
    },
    'GRA' => {
        'url' => $ENV{'GRA_URL'}
    }
};

$globals{'imgUrl'} = $globals{'locationUrl'} . '/web/img'; # répertoire des images
$globals{'cssUrl'} = $globals{'locationUrl'} . '/web/css'; # répertoire des css
$globals{'jsUrl'}  = $globals{'locationUrl'} . '/web/js'; # répertoire des fichiers js

$globals{'imgPath'} = $globals{'webPath'} . '/img'; # répertoire des images
$globals{'cssPath'} = $globals{'webPath'} . '/css'; # répertoire des css
$globals{'jsPath'}  = $globals{'webPath'} . '/js'; # répertoire des fichiers js

# Tarification transport
my $dev = ($globals{'dev'} eq '' ? '' : '?dev=' . $globals{'dev'});
$globals{'tariffTrspWebServicesUrl'} = $globals{'tariffTrspUrl'} . '/inc/webservices.php' . $dev; # Url webservices transport
$globals{'tariffTrspEstimationUrl'} = $globals{'tariffTrspUrl'} . '/c/transport.php'; # Url d'estimation transport
if ($ENV{'TFT_SOAP'} ne '')
{
    my $tariffTrspWsinfos = &LOC::Json::fromJson($ENV{'TFT_SOAP'});
    $globals{'tariffTrspServicesUrl'} = $tariffTrspWsinfos->{'location'} . $dev; # Url des services transport
}


# Paramètres des e-mails
my $parseEmailString = sub
{
    my ($string) = @_;

    my $result = {};
    if ($string =~ /^(.+?)(?: +<(.+)>)?$/)
    {
        if (defined $2)
        {
            $result->{$2} = $1;
        }
        else
        {
            $result->{$1} = $1;
        }
    }

    return $result;
};

$globals{'mailsSender'} = &$parseEmailString($ENV{'MAILS_SENDER'});
$globals{'errorsSender'} = &$parseEmailString($ENV{'ERRORS_SENDER'});

$globals{'errorsRecipients'} = {};
my @tabRecipients = split(/,/, $ENV{'ERRORS_RECIPIENTS'});
foreach my $recipient (@tabRecipients)
{
    my $parsed = &$parseEmailString($recipient);
    $globals{'errorsRecipients'} = {%{$globals{'errorsRecipients'}}, %$parsed};
}


my $tabSoapCryptInfos = {
    'KIMOCE_SOAP' => {
        'key' => 'LSZyEa7Mc2x1mzHa2tB9OZKtMXvephzkChTInVUOCQU=',
        'iv'  => $iv
    }
};

# Services
$globals{'servicesWs'} = {};
if ($ENV{'SERVICES_SMS_SOAP'} ne '')
{
    $globals{'servicesWs'}{'sms'} = &LOC::Json::fromJson($ENV{'SERVICES_SMS_SOAP'});
};
if ($ENV{'SERVICES_TELEMATICS_SOAP'} ne '')
{
    $globals{'servicesWs'}{'telematics'} = &LOC::Json::fromJson($ENV{'SERVICES_TELEMATICS_SOAP'});
};

# GUI
if ($ENV{'GUI_SOAP'} ne '')
{
    $globals{'guiWs'} = &LOC::Json::fromJson($ENV{'GUI_SOAP'});
}

# Kimoce
if ($ENV{'KIMOCE_SOAP'} ne '')
{
    my $tabSoapInfos = &LOC::Json::fromJson($ENV{'KIMOCE_SOAP'});
    $globals{'kimoceWs'} = {
        'location' => $tabSoapInfos->{'location'},
        'wsdl'     => $tabSoapInfos->{'wsdl'},
        'user'     => $tabSoapInfos->{'user'},
        'password' => &LOC::Util::decryptPassword(
            $tabSoapInfos->{'password'},
            $tabSoapCryptInfos->{'KIMOCE_SOAP'}->{'key'},
            $tabSoapCryptInfos->{'KIMOCE_SOAP'}->{'iv'}
        )
    };
}

# Liens pour le HelpDesk
$globals{'helpDeskWikiUrl'} = $ENV{'WIKI_URL'} . '/index.php/Accueil'; # Url du wiki pour le support
$globals{'addressBookUrl'}  = $globals{'fwApplicationsCfgs'}->{'GRA'}->{'url'} . '/index.php?fwModule=default&fwController=directory&fwAction=view'; # Url de l'annuaire interne
$globals{'supervConsoleUrl'} = $ENV{'CONSOLE_URL'};


# Configurations
$globals{'debugMode'}                  = 0;
$globals{'enableNetworkOptimizations'} = 1;

# TODO: Passer en caractéristique
$globals{'connectToKimoce'} = ($ENV{'KIMOCE_DB'} eq '' ? 0 : 1);

# Répertoire de développement
if ($globals{'env'} eq 'dev' && defined $ENV{'DEV'})
{
    #$globals{'debugMode'}       = 1;
    $globals{'enableNetworkOptimizations'} = 0;
    $globals{'build'} = time();

    $globals{'env'} .= '/' . $ENV{'DEV'};
}


# Serveur SOAP
if ($ENV{'LOC_SOAP'} ne '')
{
    $globals{'soapServerUrl'} = &LOC::Json::fromJson($ENV{'LOC_SOAP'});
}


# Serveurs de base de données
my $tabDBCryptInfos = {
    'DB' => {
        'key' => 'nNxA8CDOi2Ikt9EbIFamyT2RojFejf0s2ra2Y/bXYN0=',
        'iv'  => $iv
    },
    'STATS_DB' => {
        'key' => 'vDjeV8edN8FjpD2vWyzNXzrfGoEcZFVQYmiEG/EGnGU=',
        'iv'  => $iv
    },
    'KIMOCE_DB' => {
        'key' => 'yXTfL5LxtCyYyn0wAQkK4s1Jo5x2t9ZOx+IPcI9c3I8=',
        'iv'  => $iv
    }
};

# - Principale
my $dbInfos = ($ENV{'DB'} eq '' ? '' : &LOC::Json::fromJson($ENV{'DB'}));

if (ref($dbInfos) eq 'HASH')
{
    if (defined $globals{'dev'})
    {
        if (defined $dbInfos->{$globals{'dev'}})
        {
            $dbInfos = $dbInfos->{$globals{'dev'}};
        }
        elsif ($globals{'dev'} ne '1')
        {
            &LOC::Log::alert(sprintf('L\'environnement de DEV %s demandé n\'est pas configuré', $globals{'dev'}));
            exit;
        }
    }

    $servers{'mysql'} = {
        'type'    => 'database',
        'host'    => $dbInfos->{'host'},
        'configs' => {
            'dbtype'   => 'MySQL',
            'version'  => '5',
            'username' => $dbInfos->{'user'},
            'password' => &LOC::Util::decryptPassword(
                $dbInfos->{'password'},
                $tabDBCryptInfos->{'DB'}->{'key'},
                $tabDBCryptInfos->{'DB'}->{'iv'}
            )
        }
    };
}

# - Statistiques
my $statsDbInfos = ($ENV{'STATS_DB'} eq '' ? '' : &LOC::Json::fromJson($ENV{'STATS_DB'}));
if (defined $globals{'dev'} && defined $statsDbInfos->{$globals{'dev'}})
{
    $statsDbInfos = $statsDbInfos->{$globals{'dev'}};
}

if (ref($statsDbInfos) eq 'HASH')
{
    $servers{'mysql-stats'} = {
        'type'    => 'database',
        'host'    => $statsDbInfos->{'host'},
        'configs' => {
            'dbtype'   => 'MySQL',
            'version'  => '5',
            'username' => $statsDbInfos->{'user'},
            'password' => &LOC::Util::decryptPassword(
                $statsDbInfos->{'password'},
                $tabDBCryptInfos->{'STATS_DB'}->{'key'},
                $tabDBCryptInfos->{'STATS_DB'}->{'iv'}
            )
        }
    };
}

# - Kimoce
my $kimoceDbInfos = ($ENV{'KIMOCE_DB'} eq '' ? '' : &LOC::Json::fromJson($ENV{'KIMOCE_DB'}));
if (defined $globals{'dev'} && ref($kimoceDbInfos) eq 'HASH' && defined $kimoceDbInfos->{$globals{'dev'}})
{
    $kimoceDbInfos = $kimoceDbInfos->{$globals{'dev'}};
}

if (ref($kimoceDbInfos) eq 'HASH')
{
    $servers{'kimoce'} = {
        'type' => 'database',
        'host' => $kimoceDbInfos->{'host'},
        'configs' => {
            'dbtype' => 'MSSQL',
            'version' => '1',
            'username' => $kimoceDbInfos->{'user'},
            'password' => &LOC::Util::decryptPassword(
                $kimoceDbInfos->{'password'},
                $tabDBCryptInfos->{'KIMOCE_DB'}->{'key'},
                $tabDBCryptInfos->{'KIMOCE_DB'}->{'iv'}
            )
        }
    };
}


# Définit la liste des connexions aux bases de données
%connections = (
    'auth' => {
        'server' => 'mysql',
        'schema' => 'AUTH'
    },

    'location' => {
        'server' => 'mysql',
        'FR'  => { 'schema' => 'LOCATION' },
        'ES'  => { 'schema' => 'LOCATIONES' },
        'PT'  => { 'schema' => 'LOCATIONPT' },
        'MA'  => { 'schema' => 'LOCATIONMA' },
        'EXT' => { 'schema' => 'LOCATION' }
    },

    'location-stats' => {
        'server' => 'mysql-stats',
        'FR'  => { 'schema' => 'LOCATION' },
        'ES'  => { 'schema' => 'LOCATIONES' },
        'PT'  => { 'schema' => 'LOCATIONPT' },
        'MA'  => { 'schema' => 'LOCATIONMA' },
        'EXT' => { 'schema' => 'LOCATION' }
    },

    'gestiontrans' => {
        'server' => 'mysql',
        'FR' => { 'schema' => 'GESTIONTRANSFR' },
        'ES' => { 'schema' => 'GESTIONTRANSES' },
        'PT' => { 'schema' => 'GESTIONTRANSPT' },
        'MA' => { 'schema' => 'GESTIONTRANSMA' }
    },

    'gestiontarif' => {
        'server' => 'mysql',
        'FR' => { 'schema' => 'GESTIONTARIFFR' },
        'ES' => { 'schema' => 'GESTIONTARIFES' },
        'PT' => { 'schema' => 'GESTIONTARIFPT' },
        'MA' => { 'schema' => 'GESTIONTARIFMA' }
    },

    'crc' => {
        'server' => 'mysql',
        'FR' => { 'schema' => 'crc_fr' },
        'ES' => { 'schema' => 'crc_es' },
        'PT' => { 'schema' => 'crc_pt' },
        'MA' => { 'schema' => 'crc_ma' }
    },

    'crm' => {
        'server' => 'mysql',
        'FR' => { 'schema' => 'crm_fr' },
        'ES' => { 'schema' => 'crm_es' },
        'PT' => { 'schema' => 'crm_pt' },
        'MA' => { 'schema' => 'crm_ma' }
    },

    'kimoce' => {
        'server' => 'kimoce',
        'schema' => 'kimoce'
    },

    'framework' => {
        'server' => 'mysql',
        'schema' => 'FRAMEWORK'
    },

    'frmwrk' => {
        'server' => 'mysql',
        'schema' => 'frmwrk'
    },

    'statistics' => {
        'server' => 'mysql',
        'schema' => 'statistics'
    },
);


# Function: getEnv
# Retourne l'environnement courant
#
# Returns:
# string
sub getEnv
{
    return $globals{'env'};
}


# --------------------------------------------------------------------------------------------------


# Function: get
# Retourne la valeur d'une variable globale de l'application
#
# Parameters:
# string $val  - Nom de la variable parmi :
#                - build : build de l'application ;
#                - cgiPath : répertoire contenant les fichiers CGI (les nouveaux);
#                - cgiOldPath : répertoire contenant les fichiers CGI (les anciens, partie 'portail');
#                - cssPath : répertoire contenant les feuilles de style CSS ;
#                - cssUrl : URL des feuilles de style CSS ;
#                - commonFilesPath : répertoire des fichiers communs ;
#                - commonFilesUrl : URL des fichiers communs ;
#                - debugMode : débogage activé (1) ou non (0) ;
#                - documentRoot : répertoire racine ;
#                - env : environnement (exp, int ou dev) ;
#                - fwApplicationsCfgs : configurations des applications Framework v2.x.x (CRM, CRC et GRA) ;
#                - httpHost : serveur HTTP ;
#                - intranetUrl : URL de l'intranet ;
#                - imgPath : répertoire contenant les images ;
#                - imgUrl : URL des images ;
#                - jsPath : répertoire contenant les fichiers JavaScript ;
#                - jsUrl : URL des fichiers JavaScript ;
#                - locationUrl : URL de l'application ;
#                - reportUrl : URL de l'application Reports
#                - version : version de l'application ;
#                - webPath : répertoire contenant les pages Web ;
#
# Returns:
# mixed -
sub get
{
    my $val = $globals{$_[0]};

    return $val;
}


# Function: getConnParam
# Retourne les paramètres de connexion à une base de données
#
# Parameters:
# string $connName  - Nom de la connexion
# string $countryId - Identifiant du pays, si nécessaire
#
# Returns:
# hash - Table de hachage contenant les informations suivantes :
# - dbtype : type de base de données ;
# - dsn : data source ;
# - host : nom du serveur de base de données ;
# - password : mot de passe ;
# - schema : schéma ;
# - username : nom d'utilisateur.
sub getConnParam
{
    my $connName = $_[0];
    my $countryId = $_[1];
    my %connHash;

    # si la connexion est définie
    if ($connections{$connName})
    {
        my $serverName = $connections{$connName}->{'server'};
        # si le serveur est défini
        if ($servers{$serverName})
        {
            # définition des paramètres de connexion
            %connHash = (
                         'host'     => $servers{$serverName}->{'host'},
                         'username' => $servers{$serverName}->{'configs'}->{'username'},
                         'password' => $servers{$serverName}->{'configs'}->{'password'},
                         'dbtype'   => $servers{$serverName}->{'configs'}->{'dbtype'},
                        );

            # définition du schéma en fonction de l'id pays si fourni
            if ($countryId)
            {
                $connHash{'schema'} = $connections{$connName}->{$countryId}->{'schema'};
            }
            else
            {
                $connHash{'schema'} = $connections{$connName}->{'schema'};
            }

            # construction du data source en fonction du type de serveur
            if (lc($servers{$serverName}->{'configs'}->{'dbtype'}) eq 'mssql')
            {
                $connHash{'dsn'} = 'dbi:Sybase:' . $connHash{'host'};
            }
            else
            {
                $connHash{'dsn'} = 'DBI:mysql:' . $connHash{'schema'} . ':' . $connHash{'host'};
            }
        }
    }

    return \%connHash;
}



# Function: getLogo
# Retourne le nom du logo en fonction du pays
#
# Parameters:
# string $countryId - Identifiant du pays, si nécessaire
#
# Returns:
# string - Nom du logo
sub getLogo
{
    my ($countryId, $type) = @_;

    if (!defined $type)
    {
        $type = LOGO_TYPE_VECTOR;
    }

    my $imgName = &LOC::Characteristic::getCountryValueByCode('LOGOPRINT', $countryId);

    my $extension = '';
    if ($type eq LOGO_TYPE_VECTOR)
    {
        $extension = '.pdf';
    }
    if ($type eq LOGO_TYPE_RASTER)
    {
        $extension = '.jpg';
    }

    return &get('imgPath') . '/logo/' . $imgName . $extension;
}


# Function: replaceVariables
# Remplace toutes les variables globales par leur valeur.
#
# Parameters:
# string $string - La chaîne de caractères dans laquelle il faut remplacer les variables globales.
#
# Returns:
# string - La chaîne de caractères avec les variables globales remplacées par leur valeur.
sub replaceVariables
{
    my ($string) = @_;

    foreach my $var (keys(%globals))
    {
        $string =~ s/%$var%/$globals{$var}/g;
    }

    return $string;
}

1;