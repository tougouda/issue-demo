use utf8;
use open (':encoding(UTF-8)');

# Class: LOC::Html
# Classe de gestion de l'affichage des pages standard en HTML
package LOC::Html::Standard;


# Constants: Priorités
# DISPINS_SELECT - Affichage de l'assurance en saisie
# DISPINS_VIEW   - Affichage de l'assurance en visualisation

use constant {
        DISPFLG_ALL     => 0xFFFF,
        DISPFLG_NONE    => 0x0000,
        DISPFLG_HEAD    => 0x0001,
        DISPFLG_CTRLS   => 0x0002,
        DISPFLG_BACK    => 0x0006,
        DISPFLG_FOOT    => 0x0020,
        DISPFLG_CHUSER  => 0x0040,
        DISPFLG_BGLOGO  => 0x0800,
        DISPFLG_BTCLOSE => 0x1000,
        DISPFLG_BTPRINT => 0x2000,
        DISPFLG_NOTIFS  => 0x4000,

        DISPINS_SELECT  => 'SELECT',
        DISPINS_VIEW    => 'VIEW',

        FLGMODE_SET     => 0x00,
        FLGMODE_ADD     => 0x01,
        FLGMODE_SUB     => 0x02,

        STATE_CONFIRMED => 'confirmed',
        STATE_INVOICED  => 'invoiced',
        STATE_CANCELED  => 'canceled',
        STATE_PENDING   => 'pending'
};


use strict;


use LOC::Html;
our @ISA = ('LOC::Html');

use LOC::Request;
use LOC::Insurance::Type;


# Variable: $VERSION
# Version de la classe
my $VERSION = '1.00';




# Fucntion: new
# Constructeur de la classe
#
# Parameters:
# string $localeId - Identifiant de la locale
# string $agencyId - Identifiant de l'agence dans laquelle se trouve l'utilisateur dans l'application
#
# Returns:
# LOC::Html::Standard - Instance créée
sub new
{
    my ($class, $localeId, $agencyId) = @_;

    my $this = $class->SUPER::new($localeId);
    bless($this, $class);

    $this->{'_agencyId'} = $agencyId;
    $this->{'_closeButtonAction'} = '';
    $this->{'_title'} = '';
    $this->{'_dispFlags'} = DISPFLG_NONE;
    $this->{'_logoImg'} = 'standard/bgLogo.gif';
    $this->{'_tabCtrlsBarContents'} = {'left' => [], 'right' => [], 'middle' => []};
    $this->{'_tabPackages'}->{'popups'}->{'js'}->[1] = 'Location.popupsManager.setContainer("locStandardContent");';
    $this->{'_bodyContent'} = ['', ''];
    $this->{'_tabNotifications'} = [];

    $this->addCSSSrc('standard.css');
    $this->addJSSrc('standard.js');

    $this->addPackage('notifications');

    return $this;
}


# Function: displayCustomerClass
# Affichage de la classe d'un client
#
# Parameters:
# string $class - Classe
#
# Returns:
# string - Code HTML
sub displayCustomerClass
{
    my ($this, $class, $id) = @_;
    my $html = '<span class="locStandardCustomerClass"';
    if (defined $id)
    {
        $html .= ' id="' . $this->toHTMLEntities($id) . '"';
    }
    $html .= '>' . $class . '</span>';
    return $html;
}


# Function: displayCustomerPotential
# Affichage du potentiel d'un client
#
# Parameters:
# string $potential - Potentiel
# string $id        - Id de l'élément (facultatif)
#
# Returns:
# string - Code HTML
sub displayCustomerPotential
{
    my ($this, $potential, $id) = @_;
    my $html = '<span class="locStandardCustomerPotential"';
    if (defined $id)
    {
        $html .= ' id="' . $this->toHTMLEntities($id) . '"';
    }
    $html .= '>' . $potential . '</span>';
    return $html;
}


# Function: displayCounter
# Affichage d'un compteur
#
# Parameters:
# int $nb - Nombre
#
# Returns:
# string - Code HTML
sub displayCounter
{
    my ($this, $nb) = @_;

    return '<div class="locStandardCounter">' . $nb . '</div>';
}

# Function: displayDuration
# Affichage d'un compteur de jours de location
#
# Parameters:
# int $nb - Nombre
#
# Returns:
# string - Code HTML
sub displayDuration
{
    my ($this, $nb, $id) = @_;
    my $html = '<div class="locStandardDuration"';
    if (defined $id)
    {
        $html .= ' id="' . $this->toHTMLEntities($id) . '"';
    }
    $html .= '>' . $nb . '</div>';
    return $this->_tn('%s jour ouvré', '%s jours ouvrés', $nb, $html);
}

# Function: displayCalendarDuration
# Affichage d'un compteur de jours calendaires
#
# Parameters:
# int $nb - Nombre
#
# Returns:
# string - Code HTML
sub displayCalendarDuration
{
    my ($this, $nb, $id) = @_;
    my $html = '<div class="locStandardCalendarDuration"';
    if (defined $id)
    {
        $html .= ' id="' . $this->toHTMLEntities($id) . '"';
    }
    $html .= '>' . $nb . '</div>';
    return $this->_tn('%s jour calendaire', '%s jours calendaires', $nb, $html);
}

# Function: displayCustomerLockLevel
# Affichage de l'état de blocage d'un client.
#
# Parameters:
# bool $status - Statut de blocage du client (0, 1, 2 ou 3)
#
# Returns:
# string - Code HTML
sub displayCustomerLockLevel
{
    my ($this, $status) = @_;

    my $class = 'locStandardCustomerStatus level' . $status;
    if ($status == 1 || $status == 2)
    {
        my $label = $this->_t('Bloqué au contrat');
        return '<div class="' . $class . '">' . $label . '</div>';
    }
    if ($status == 3 || $status == 4)
    {
        my $label = $this->_t('Bloqué au devis et au contrat');
        return '<div class="' . $class . '">' . $label . '</div>';
    }
    return '';
}


# Function: displayParkNumber
# Affichage du numéro de parc
#
# Parameters:
# int $num         - Numéro de parc
# int $id          - Id du div qui encapsule le numéro de parc
# int $isClickable - le numéro de parc est cliquable (oui par défaut)
# string $viewType - Type d'affichage (facultatif)
#
# Returns:
# string - Code HTML
sub displayParkNumber
{
    my ($this, $num, $id, $isClickable, $viewType) = @_;

    # Informations nécessaires pour l'affichage du numéro de parc
    my $tabDisplayInfos = &LOC::Machine::getDisplayInfos($num, $viewType);

    my @tabClasses = ('locStandardParkNumber');
    my @tabTitles = ();
    if ($tabDisplayInfos->{'isWithTelematic'})
    {
        push(@tabClasses, 'special');
        push (@tabTitles, $this->_t('Cette machine est équipée de télématique'));
    }

    my $js = '';
    my $html = '<div ';
    if (defined $id && $id ne '')
    {
        $html .= ' id="' . $this->toHTMLEntities($id) . '"';
    }
    if (@tabClasses > 0)
    {
        $html .= 'class="' . join(' ', @tabClasses) . '"';
    }
    if (@tabTitles > 0)
    {
        $html .= ' title="' . join(' ', @tabTitles) . '"';
    }
    if (!defined $isClickable || $isClickable)
    {
        my $url = $tabDisplayInfos->{'url'};

        # si le type d'affichage est light, affichage dans une popup
        if ($viewType eq 'light')
        {
            # Création de la popup pour la fiche machine
            $js   .= $this->displayJSBlock('Location.popupsManager.createPopup("machine", "", "center", "center", 850, 500);');
            $html .= ' onclick="if (!LOC_View.isCtrlPressed && !LOC_View.isShiftPressed && !LOC_View.isMouseWheelPressed) { Location.popupsManager.getPopup(\'machine\').open(\'' . $tabDisplayInfos->{'url'} . '\'); return false; }"';
            $url = $tabDisplayInfos->{'fullUrl'};
        }

        $num = '<a href="' . $url . '" target="_blank">' . $num . '</a>';
    }
    $html .= '>' . $num . '</div>
            ' . $js;

    return $html;
}


# Function: displayZone
# Affichage de la zone
#
# Parameters:
# int $nb - Zone
# int $id - Id du div qui encapsule la zone
# Returns:
# string - Code HTML
sub displayZone
{
    my ($this, $nb, $id) = @_;

    return '<div class="locStandardZone" id="' . $id . '" >' . $nb . '</div>';
}


# Function: displayScore
# Affichage d'un score
#
# Parameters:
# int $nb    - Score
# int $total - Nombre total
#
# Returns:
# string - Code HTML
sub displayScore
{
    my ($this, $nb, $total) = @_;
    unless (defined $total)
    {
        $total = 5;
    }

    if ($total < $nb)
    {
        $total = $nb;
    }

    my $defaultColor = 'Grey';
    my @tabColors = ();
    for (my $i = 0; $i < $total; $i++)
    {
        $tabColors[$i] = $defaultColor;
    }

    $tabColors[1] = 'Green';
    $tabColors[2] = 'Yellow';
    $tabColors[3] = 'Orange';
    $tabColors[4] = 'Red';

    my $html = '<div class="locStandardScore">';

    for (my $i = 0; $i < $total; $i++)
    {
        $html .= $this->displayImage('standard/rating' . ($i < $nb ? $tabColors[$nb] : $defaultColor) . '.png');
    }

    $html .= '</div>';

    return $html;
}


# Function: displayState
# Affichage d'un état
#
# Parameters:
# int    $stateId - Identifiant de l'état
# string $label   - Libellé de l'état
#
# Returns:
# string - Code HTML
sub displayState
{
    my ($this, $stateId, $label) = @_;

    my $color = 'black';

    if ($stateId eq STATE_CONFIRMED ||
        $stateId eq 'CON01' || $stateId eq 'DEV06' || $stateId eq 'MAC01')
    {
        $color = 'blue';
    }
    elsif ($stateId eq STATE_INVOICED ||
           $stateId eq 'CAS05' || $stateId eq 'CAS02' || $stateId eq 'CAS03' || $stateId eq 'CON02' ||
           $stateId eq 'DEV03' || $stateId eq 'MAC02' || $stateId eq 'SER02' || $stateId eq 'REM01')
    {
        $color = 'green';
    }
    elsif ($stateId =~ /KIM0\d/)
    {
        $color = 'green';
    }
    elsif ($stateId eq STATE_CANCELED ||
           $stateId eq 'CAS04' || $stateId eq 'CAS06' || $stateId eq 'CAS07' || $stateId eq 'CON06' || $stateId eq 'DEV02' ||
           $stateId eq 'PRE03' || $stateId eq 'PRE04' || $stateId eq 'SER03' || $stateId eq 'REM03')
    {
        $color = 'red';
    }
    elsif ($stateId =~ /KIM2\d|MAC0[45689]|MAC[123]\d/)
    {
        $color = 'red';
    }
    elsif ($stateId eq STATE_PENDING ||
           $stateId eq 'CAS01' || $stateId eq 'CON03' || $stateId eq 'DEV01' ||
           $stateId eq 'DEV05' || $stateId eq 'MAC03' || $stateId eq 'MAC07' || $stateId eq 'REM02')
    {
        $color = 'yellow';
    }

    my $classname = 'locStandardState ' . $color;
    my $imagename = 'standard/flag' . ucfirst($color) . '.png';

    return '<div class="' . $classname . '">' .
                $this->displayImage($imagename) . '&nbsp;' .
                $this->toHTMLEntities($label) .
           '</div>';
}


# Function: displayLocked
# Affichage d'un état de blocage. Si $isLocked est vrai, $label (ou le message par défaut s'il n'est pas renseigné)
# est affiché. Si $isLocked est faux, le bloc n'est pas affiché, sauf si $label est renseigné.
#
# Parameters:
# bool   $isLocked - Bloqué ou non
# string $label    - Libellé de l'état
#
# Returns:
# string - Code HTML
sub displayLocked
{
    my ($this, $isLocked, $label) = @_;
    unless (defined $label)
    {
        $label = ($isLocked ? $this->_t('Bloqué') : '');
    }
    if ($label eq '')
    {
        return '';
    }
    my $className = 'locStandardLocked ' . ($isLocked ? 'locked' : 'unlocked');
    my $imageName = ($isLocked ? 'no.png' : 'yes.png');

    return '<div class="' . $className . '">' . $this->displayImage($imageName) . '&nbsp;' . $label . '</div>';
}


# Function: displayInsurance
# Affichage de l'assurance
#
# Parameters:
# array  $params     - tableau de paramètres : id    => int, identifiant de l'assurance choisie,
#                                              rate  => float, pourcentage de l'assurance choisie
#                                              rates => array, tableau des différents taux d'assurances possibles
# string $mode       - Mode de l'affichage, optionnel : DISPINS_SELECT pour le mode saisie,
#                                                       DISPINS_VIEW pour la mode visualisation (par défaut)
# array  $attributes - insType    => Le tableau des attributs du select du type en saisie (DISPINS_SELECT), optionnel
#                      insRate    => Le tableau des attributs du select du % en mode saisie (DISPINS_SELECT), optionnel
#                      insType.id => L'id du select du type d'assurance (optionnel, 'insuranceTypeId' par défaut)
#                      insRate.id => L'id du select du type d'assurance (optionnel, 'insuranceRateId' par défaut)
#
# Returns:
# string - Code HTML
sub displayInsurance
{
    my ($this, $params, $mode, $attributes) = @_;

    unless ($mode)
    {
        $mode = LOC::Html::Standard::DISPINS_VIEW;
    }

    unless (defined $params->{'rate'})
    {
        $params->{'rate'} = 0;
    }
    my $rate = $params->{'rate'};
    my $id = $params->{'id'};

    my %tabLabels;
    my $result = '';
    my $jsBlock = '';

    my $userInfos = &LOC::Session::getUserInfos();

    # mode visualisation
    if ($mode eq LOC::Html::Standard::DISPINS_VIEW)
    {
        my $tabInsuranceInfos = (defined $params->{'infos'} ? $params->{'infos'} :
                                                              &LOC::Insurance::Type::getInfos($userInfos->{'nomadCountry.id'}, $id));

        my $label = $tabInsuranceInfos->{'fullLabel'};
        my $percent = $this->{'_localeObj'}->t('%s %%', $rate * 100);
        $label =~ s/<%percent>/$percent/g;

        my $flag = 0;
        if ($tabInsuranceInfos->{'flags'} & LOC::Insurance::Type::FLAG_ISINVOICED)
        {
            $flag = LOC::Insurance::Type::FLAG_ISINVOICED;
        }
        elsif ($tabInsuranceInfos->{'flags'} & LOC::Insurance::Type::FLAG_ISCUSTOMER)
        {
            $flag = LOC::Insurance::Type::FLAG_ISCUSTOMER;
        }
        elsif ($tabInsuranceInfos->{'flags'} & LOC::Insurance::Type::FLAG_ISINCLUDED)
        {
            $flag = LOC::Insurance::Type::FLAG_ISINCLUDED;
        }

        $result .= '<span class="locStandardInsuranceType type_' . $flag . '">' . $label . '</span>';
    }
    # mode saisie
    elsif ($mode eq LOC::Html::Standard::DISPINS_SELECT)
    {
        my $tabInsuranceTypesList = &LOC::Insurance::Type::getList($userInfos->{'nomadCountry.id'}, LOC::Util::GETLIST_ASSOC,
                                                                  {'stateId' => [LOC::Insurance::Type::STATE_ACTIVE, LOC::Insurance::Type::STATE_DELETED]});

        # nom par défaut des
        if (!$attributes->{'insType.id'})
        {
            $attributes->{'insType.id'} = 'insuranceTypeId';
        }

        if (!$attributes->{'insRate.id'})
        {
            $attributes->{'insRate.id'} = 'insuranceRateId';
        }

        # ajout des selects et du texte
        tie(my %tabRates, 'Tie::IxHash');
        foreach my $val (@{$params->{'rates'}})
        {
            $tabRates{&LOC::Util::getNonLocaleFloat($val)} = $this->{'_localeObj'}->t('%s %%', ($val * 100));
        }
        if (exists $attributes->{'tabindex'})
        {
            $attributes->{'insType'}->{'tabindex'} = $attributes->{'tabindex'};
            $attributes->{'insRate'}->{'tabindex'} = $attributes->{'tabindex'} + 1;
        }

        my $tabInsuranceTypes = {};
        foreach my $insuranceInfos (values(%$tabInsuranceTypesList))
        {
            $tabInsuranceTypes->{$insuranceInfos->{'id'}} = $insuranceInfos->{'label'};
        }

        my $insuranceTypesSelect = $this->displayFormSelect($attributes->{'insType.id'}, $tabInsuranceTypes, $id, 0, $attributes->{'insType'});
        my $insuranceRatesSelect = $this->displayFormSelect($attributes->{'insRate.id'}, \%tabRates, $rate, 0, $attributes->{'insRate'});


        $result .= '
        <div id="insurance-block">
            ' . $insuranceTypesSelect . '
            ' . $insuranceRatesSelect . '
        </div>
        <div id="insurance-block-temp" style="display: none;"></div>';

        my @tabInsuranceTypesList = values(%$tabInsuranceTypesList);
        $jsBlock .= '
<script type="text/javascript">
    var tabInsuranceTypes = ' . &LOC::Json::toJson(\@tabInsuranceTypesList) . ';

    var insuranceTypesSelect = $ge("' . $attributes->{'insType.id'} . '");
    var insuranceRatesSelect = $ge("' . $attributes->{'insRate.id'} . '");
    ';


    # Évènements
    $jsBlock .= '
    var insuranceContainer = $ge("insurance-block");

    var onInsuranceTypeChange = function() {';

        if ($attributes->{'insType'}->{'onchange'})
        {
            $jsBlock .= '
        ' . $attributes->{'insType'}->{'onchange'} . ';';
        }

        $jsBlock .= '
        var insuranceContainer = $ge("insurance-block"); 

        var insuranceContainerTemp = $ge("insurance-block-temp");
        insuranceContainerTemp.appendChild(insuranceTypesSelect);
        insuranceContainerTemp.appendChild(insuranceRatesSelect);

        var value = this.value;
        var tabInsuranceInfos = tabInsuranceTypes.find(function(item){
            return (item.id == value);
        });
        if (tabInsuranceInfos && tabInsuranceInfos.fullLabelMask)
        {
            insuranceContainer.innerHTML = tabInsuranceInfos.fullLabelMask.replace(/<%label>/g, "<span class=\"insurance-span-type\"></span>")
                                                                          .replace(/<%extraLabel>/g, tabInsuranceInfos.extraLabel)
                                                                          .replace(/<%percent>/g, "<span class=\"insurance-span-rate\"></span>");
        }
        else
        {
            insuranceContainer.innerHTML = "<span class=\"insurance-span-type\"></span>";
        }

        // attachement des listes
        var typesSpan = insuranceContainer.getElementsByClassName("insurance-span-type")[0];
        var ratesSpan = insuranceContainer.getElementsByClassName("insurance-span-rate")[0];

        if (typesSpan)
        {
            typesSpan.appendChild(insuranceTypesSelect);
        }
        if (ratesSpan)
        {
            ratesSpan.appendChild(insuranceRatesSelect);
        }
    }; 
    insuranceTypesSelect.onchange = onInsuranceTypeChange;
</script>
';

    }
    return $result . $jsBlock;
}
# Function: displayAppeal
# Affichage de la gestion de recours
#
# Parameters:
# array  $params     - tableau de paramètres : id    => int, identifiant de la gestion de recours,
#                                              rate  => float, pourcentage de la gestion de recours choisie
# string $mode       - Mode de l'affichage, optionnel : DISPINS_SELECT pour le mode saisie,
#                                                       DISPINS_VIEW pour la mode visualisation (par défaut)
# array  $attributes - insType    => Le tableau des attributs du select du type en saisie (DISPINS_SELECT), optionnel
#                      insType.id => L'id du select du type d'assurance (optionnel, 'insuranceTypeId' par défaut)
#
# Returns:
# string - Code HTML
sub displayAppeal
{
    my ($this, $params, $mode, $attributes) = @_;

    unless ($mode)
    {
        $mode = LOC::Html::Standard::DISPINS_VIEW;
    }

    unless (defined $params->{'rate'})
    {
        $params->{'rate'} = 0;
    }

    my $rate = $params->{'rate'};
    my $id = $params->{'id'};

    my %tabLabels;
    my $result = '';
    my $jsBlock = '';

    my $userInfos = &LOC::Session::getUserInfos();

    # mode visualisation
    if ($mode eq LOC::Html::Standard::DISPINS_VIEW)
    {
        my $tabAppealInfos = (defined $params->{'infos'} ? $params->{'infos'} :
                                                           &LOC::Appeal::Type::getInfos($userInfos->{'nomadCountry.id'}, $id));

        my $label = $tabAppealInfos->{'fullLabel'};
        my $percent = $this->{'_localeObj'}->t('%s %%', $rate * 100);
        $label =~ s/<%percent>/$percent/g;

        $result .= '<span class="locStandardAppealType">' . $this->displayBoolean($tabAppealInfos->{'mode'}, $label) . '</span>';
    }
    # mode saisie
    elsif ($mode eq LOC::Html::Standard::DISPINS_SELECT)
    {
        my $tabAppealTypesList = &LOC::Appeal::Type::getList($userInfos->{'nomadCountry.id'}, LOC::Util::GETLIST_ASSOC,
                                                                  {'stateId' => [LOC::Insurance::Type::STATE_ACTIVE, LOC::Insurance::Type::STATE_DELETED]});

        # nom par défaut des
        if (!$attributes->{'insType.id'})
        {
            $attributes->{'insType.id'} = 'insuranceTypeId';
        }

        if (exists $attributes->{'tabindex'})
        {
            $attributes->{'insType'}->{'tabindex'} = $attributes->{'tabindex'};
        }

        my $tabAppealTypes = {};
        foreach my $appealInfos (values(%$tabAppealTypesList))
        {
            $tabAppealTypes->{$appealInfos->{'id'}} = $appealInfos->{'label'};
        }

        my $appealTypesSelect = $this->displayFormSelect($attributes->{'insType.id'}, $tabAppealTypes, $id, 0, $attributes->{'insType'});


        $result .= '
        <div id="appeal-block">
            ' . $appealTypesSelect . '
        </div>
        <div id="appeal-block-temp" style="display: none;"></div>';

        my @tabAppealTypesList = values(%$tabAppealTypesList);
        $jsBlock .= '
<script type="text/javascript">
    var tabAppealTypes = ' . &LOC::Json::toJson(\@tabAppealTypesList) . ';
    var appealRate = ' . $rate . ';
    var appealTypesSelect = $ge("' . $attributes->{'insType.id'} . '");
    ';


    # Évènements
    $jsBlock .= '
    var appealContainer = $ge("appeal-block");

    var onAppealTypeChange = function() {';

        if ($attributes->{'insType'}->{'onchange'})
        {
            $jsBlock .= '
        ' . $attributes->{'insType'}->{'onchange'} . ';';
        }

        $jsBlock .= '
        var appealContainer = $ge("appeal-block"); 

        var appealContainerTemp = $ge("appeal-block-temp");
        appealContainerTemp.appendChild(appealTypesSelect);

        var value = this.value;
        var tabAppealInfos = tabAppealTypes.find(function(item){
            return (item.id == value);
        });

        if (tabAppealInfos && tabAppealInfos.fullLabelMask)
        {
            appealContainer.innerHTML = tabAppealInfos.fullLabelMask.replace(/<%label>/g, "<span class=\"appeal-span-type\"></span>")
                                                                          .replace(/<%extraLabel>/g, tabAppealInfos.extraLabel)
                                                                          .replace(/<%percent>/g, (appealRate * 100) + " %");
        }
        else
        {
            appealContainer.innerHTML = "<span class=\"appeal-span-type\"></span>";
        }

        // attachement des listes
        var typesSpan = appealContainer.getElementsByClassName("appeal-span-type")[0];

        if (typesSpan)
        {
            typesSpan.appendChild(appealTypesSelect);
        }
    }; 
    appealTypesSelect.onchange = onAppealTypeChange;
</script>
';

    }
    return $result . $jsBlock;
}



# Function: displayInvoicingState
# Affichage de l'état de facturation
#
# Parameters:
# string $state - État
# int    $id    - Id du div qui encapsule l'état
# Returns:
# string - Code HTML
sub displayInvoicingState
{
    my ($this, $state, $id) = @_;

    return '<div class="locStandardInvoicingState" id="' . $id . '" >' . $state . '</div>';
}


# Function: addNotifications
# Ajouter des notifications
#
# Parameters:
# arrayref $tabNotifications - Tableau des notifications
#
# Returns:
# LOC::Html - Instance courante
sub addNotifications
{
    my ($this, $tabNotifications) = @_;

    foreach my $notif (@$tabNotifications)
    {
        push(@{$this->{'_tabNotifications'}}, $notif);
    }
    return $this;
}


# Function: setTitle
# Définir le titre
#
# Parameters:
# string $title - Titre
#
# Returns:
# LOC::Html::Standard - Instance courante
sub setTitle
{
    my ($this, $title) = @_;

    $this->{'_title'} = $title;
}


# Function: addBodyContent
# Ajouter du contenu personnalisé au début ou à la fin de la balise BODY
#
# Parameters:
# string|array $content  - Contenu personnalisé
# string       $position - Au début ou à la fin ? (0: début, 1: fin)
# bool         $isAppend - Ajouter à la fin ou au début de la trame ?
#
# Returns:
# LOC::Html::Standard - Instance courante
sub addBodyContent
{
    my ($this, $content, $position, $isAppend) = @_;

    $position = (defined($position) && $position ? 1 : 0);
    unless (defined $isAppend)
    {
        $isAppend = 1;
    }

    if ($isAppend)
    {
        $this->{'_bodyContent'}->[$position] .= $content;
    }
    else
    {
        $this->{'_bodyContent'}->[$position] = $content . $this->{'_bodyContent'}->[$position];
    }
    return $this;
}


# Function: addCloseButtonAction
# Définir l'action javascript du bouton précédent
#
# Parameters:
# string $action - Action javascript à effectuer
#
# Returns:
# LOC::Html::Standard - Instance courante
sub addCloseButtonAction
{
    my ($this, $action) = @_;
    $this->{'_closeButtonAction'} = $action;
    return $this;
}


# Function: addControlsContent
# Ajouter du contenu personnalisé dans la barre de contrôles
#
# Parameters:
# string|array $content  - Contenu personnalisé
# string       $side     - De quel coté ? ('left' ou 'right', idem 'l' ou 'r')
# bool         $isAppend - Ajouter à la fin ou au début ?
#
# Returns:
# LOC::Html::Standard - Instance courante
sub addControlsContent
{
    my ($this, $content, $side, $isAppend) = @_;
    unless (defined $side)
    {
        $side = 'left';
    }
    unless (defined $isAppend)
    {
        $isAppend = 1;
    }

    if (ref($content) eq 'ARRAY')
    {
        foreach my $val (@$content)
        {
            $this->addControlsContent($val, $side, $isAppend);
        }
    }
    else
    {
        $this->setDisplay(DISPFLG_CTRLS, FLGMODE_ADD);

        my $s = (substr($side, 0, 1) eq 'l' ? 'left' : (substr($side, 0, 1) eq 'r' ? 'right' : 'middle'));
        if ($isAppend)
        {
            push(@{$this->{'_tabCtrlsBarContents'}->{$s}}, $content);
        }
        else
        {
            unshift(@{$this->{'_tabCtrlsBarContents'}->{$s}}, $content);
        }
    }
    return $this;
}


# Function: setDisplay
# Choix de l'affichage de la page
#
# Parameters:
# int $disp     - Flag d'affichage
# int $flagmode - Mode pour les flags (FLGMODE_SET: définir, FLGMODE_ADD: ajouter, FLGMODE_SUB: enlever)
#
# Returns:
# LOC::Html::Standard - Instance courante
sub setDisplay
{
    my ($this, $disp, $flagmode) = @_;
    unless (defined $flagmode)
    {
         $flagmode = FLGMODE_SET;
    }

    if ($flagmode == FLGMODE_SET)
    {
        $this->{'_dispFlags'} = $disp;
    }
    elsif ($flagmode == FLGMODE_ADD)
    {
        $this->{'_dispFlags'} |= $disp;
    }
    elsif ($flagmode == FLGMODE_SUB)
    {
        $this->{'_dispFlags'} &= DISPFLG_ALL - $disp;
    }
    return $this;
}


# Function
# Définir le l'image du logo en bas à droite
#
# Parameters:
# string $img - Image
#
# Returns:
# LOC::Html::Standard - Instance courante
sub setLogoImage
{
    my ($this, $img) = @_;

    $this->setDisplay(DISPFLG_BGLOGO, FLGMODE_ADD);
    $this->{'_logoImg'} = $img;
    return $this;
}


# Function: displayHeader
# Affichage de l'en-tête
#
# Parameters:
#
# Returns:
# string - Code HTML
sub displayHeader
{
    my ($this) = @_;


    # Ajout des notifications
    if (@{$this->{'_tabNotifications'}} > 0)
    {
        $this->addJSSrc('Location.viewObj.addNotifications(' . &LOC::Json::toJson($this->{'_tabNotifications'}) . ');');
        # On enlève du tableau les notifications envoyées à JavaScript
        $this->{'_tabNotifications'} = [];
    }

    my $html = $this->SUPER::displayHeader();

    # Contenu personalisé au début de la balise BODY
    $html .= $this->{'_bodyContent'}->[0] . "\n";

    if ($this->{'_dispFlags'} & DISPFLG_HEAD)
    {
        # Environnement
        my $env = &LOC::Globals::getEnv();
        my $dev = &LOC::Globals::get('dev');
        $dev = ($dev eq '' ? '' : ' DB' . $dev);
        my $envText;
        if ($env =~ m/^dev(.*)/)
        {
            $envText = '[' . $this->_t('Développement') . $dev . ($1 eq '' ? '' : ' ' . $1) . ']';
        }
        elsif ($env =~ m/^int(.*)/)
        {
            $envText = '[' . $this->_t('Intégration') . $dev . ($1 eq '' ? '' : ' ' . $1) . ']';
        }
        elsif ($env =~ m/^for(.*)/)
        {
            $envText = '[' . $this->_t('Formation') . $dev . ($1 eq '' ? '' : ' ' . $1) . ']';
        }
        else
        {
            $envText = '';
        }

        $html .= '<div id="locStandardTitle">
                  <a href="' . &LOC::Globals::get('locationUrl') . '/cgi-bin/index.cgi' . '" target="_blank" class="index" title="' . $this->toHTMLEntities($this->_t('Ouvrir la page d\'accueil')) . '"></a>
                  <div class="logo"></div><div class="right"></div>' .
                 '<div class="title" id="locStandardTitleText">' . $this->{'_title'} . '</div>' .
                 '<div class="env">' . $envText . '</div>' .
                 '<div class="controls">';

        if ($this->{'_dispFlags'} & DISPFLG_BTPRINT)
        {
            $html .= '&nbsp;<a href="#" class="print" onclick="window.print(); return false;" title="' . $this->toHTMLEntities($this->_t('Imprimer la page')) . '"></a>';
        }
        if ($this->{'_dispFlags'} & DISPFLG_BTCLOSE)
        {
            $html .= '&nbsp;<a href="#" class="close" onclick="' . $this->toHTMLEntities($this->{'_closeButtonAction'}) . ';' .
                     ' if (parent) { parent.close(); } else { window.close(); } return false;" title="' . $this->toHTMLEntities($this->_t('Fermer la page')) . '">' . '</a>';
        }
        $html .= '</div>' .
                 "</div>\n";
    }

    if ($this->{'_dispFlags'} & DISPFLG_CTRLS)
    {
        $html .= '<div id="locStandardCtrls">';

        # Boutons à gauche
        if (($this->{'_dispFlags'} & DISPFLG_BACK) == DISPFLG_BACK)
        {
            my $backBtn = $this->displayTextButton($this->_t('Retour'),
                                                   'standard/btnBack(|Over).gif',
                                                   'window.history.back();',
                                                   $this->_t('Retourner à la page précédente'));
            $this->addControlsContent($backBtn, 'left', 0);
        }

        $html .= $this->displayControlPanel($this->{'_tabCtrlsBarContents'});

        $html .= "\n</div>\n";
    }

    # Notifications
    if ($this->{'_dispFlags'} & DISPFLG_NOTIFS)
    {
        $html .= '<div id="locStandardNotifs"></div>';
    }

    $html .= "<div id=\"locStandardMiddle\">\n<div class=\"loadingMask\"></div>\n<div id=\"locStandardContent\">";

    return $html;
}


# Function: displayFooter
# Affichage du pied de page
#
# Parameters:
#
# Returns:
# string - Code HTML
sub displayFooter
{
    my ($this) = @_;

    my $html = "</div>\n</div>\n";

    if ($this->{'_dispFlags'} & DISPFLG_FOOT)
    {
        $html .= "<div id=\"locStandardFooter\">\n";

        my $currentYear = POSIX::strftime('%Y', localtime());
        $html .= "\t<div class=\"copyright\" id=\"locStandardFooterText\">" .
                        '<b>C</b>opyright <b>&copy;</b> 2005-' . $currentYear . ' <b>A</b>ccès Industrie. ' .
                        '<b>T</b>ous droits réservés.' .
                        sprintf('&nbsp;<span class="elapsed">[%.3f&quot;]</span>',
                                &LOC::Controller::Front::getElapsedSecs()) .
                 '</div>';

        # Maintenance
        my $maintenanceObj = '';
        if ($this->{'_isMaintenanceActivated'})
        {
            $maintenanceObj = "\t<div class=\"maintenance\">" . $this->_t('Maintenance en cours') . '</div>';
        }
        # Agence
        my $agencyObj = '';
        if (defined $this->{'_agencyId'})
        {
            $agencyObj = "\t<div class=\"agency\">" . $this->{'_agencyId'} . '</div>';
        }
        # Date et heure en base à droite
        my $date = $this->{'_localeObj'}->getDateFormat(undef, LOC::Locale::FORMAT_DATEYEAR_TEXT);
        my $timeObj = '<a href="javascript:;" class="time" id="locStandardTime" title="' .
                    $this->toHTMLEntities($date) . '">---</a>';


        $html .= $maintenanceObj . $agencyObj . "\t<div class=\"right\">" . $timeObj . "</div>\n";
        $html .= '</div>';
    }

    # Contenu personalisé à la fin de la balise BODY
    $html .= $this->{'_bodyContent'}->[1] . "\n";

    # Ajout des notifications
    if (@{$this->{'_tabNotifications'}} > 0)
    {
        $html .= $this->displayJSBlock('Location.viewObj.addNotifications(' . &LOC::Json::toJson($this->{'_tabNotifications'}) . ');');
        # On enlève du tableau les notifications envoyées à JavaScript
        $this->{'_tabNotifications'} = [];
    }

    # Fermeture du document HTML
    $html .= $this->SUPER::displayFooter();

    return $html;
}

1;