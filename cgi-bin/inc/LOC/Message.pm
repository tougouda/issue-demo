use utf8;
use open (':encoding(UTF-8)');

# Package: LOC::Message
# Module de gestion des messages d'informations de la page d'accueil
package LOC::Message;


# Constants: États
# STATE_ACTIVE   - Message actif
# STATE_INACTIVE - Message inactif
use constant
{
    STATE_ACTIVE   => 'GEN01',
    STATE_INACTIVE => 'GEN03',
};


use strict;

# Modules internes
use LOC::Db;
use LOC::Util;


# Function: getInfos
# Retourne les infos d'un message d'information
# 
# Contenu du hashage :
# - id => identifiant du message
# - subject => intitulé du message
# - dateSart => date de début d'affichage
# - dateEnd => date de fin d'affichage
# - content => contenu du message
# - priority => priorité d'affichage
# - countryFilter => chaîne de filtre par pays
# - dateCreation => date de création du message
# - state.id => identifant de l'état
#
#
# Parameters:
# string $id        - Identifiant du message
# string $localeId  - Identifiant de la locale
#
# Returns:
# hash|hashref|0 - Retourne un hash ou un hashref suivant la demande, 0 si pas de résultat
sub getInfos
{
    my ($id, $localeId) = @_;
    unless (defined $localeId)
    {
        $localeId = LOC::Locale::DEFAULT_LOCALE_ID;
    }

    # connexion à AUTH
    my $db = &LOC::Db::getConnection('auth');
    
    # Récupère les données
    my $query = '
SELECT
    msg_id AS `id`,
    IFNULL(msl_subject, msg_subject) AS `subject`,
    IF(msg_date_start <> "0000-00-00", msg_date_start, null) AS `dateStart`,
    SUBSTR(msg_date_start, -8, 5) as `hourStart`,
    IF(msg_date_end <> "0000-00-00", msg_date_end, null) AS `dateEnd`,
    SUBSTR(msg_date_end, -8, 5) as `hourEnd`,
    IFNULL(msl_content, msg_content) AS `content`,
    msg_priority AS `priority`,
    msg_countryfilter AS `countryFilter`,
    msg_date_creation AS `dateCreation`,
    msg_sta_id AS `state.id`
FROM f_message
    LEFT JOIN f_message_locale ON (msl_msg_id = msg_id AND msl_lcl_id = ' . $db->quote($localeId) . ')
WHERE msg_id = ' . $db->quote($id) . ';';

    my $dataRef = $db->fetchRow($query, {}, LOC::Db::FETCH_ASSOC);
    unless ($dataRef)
    {
        return 0;
    }
    if (wantarray)
    {
        return %$dataRef;
    }
    else
    {
        return $dataRef;
    }
}

# Function: getList
# Retourne la liste des messages d'information
#
# Parameters:
# int     $format     - Format de retour de la liste
# string  $localeId   - Identifiant de la locale
# hashref $tabFilters - Filtres ('country' : id d'un pays)
#
# Returns:
# hash|hashref|0 - Liste des alertes
sub getList
{
    my ($format, $localeId, $tabFilters) = @_;
    unless (defined $localeId)
    {
        $localeId = LOC::Locale::DEFAULT_LOCALE_ID;
    }

    # connexion à Location
    my $db = &LOC::Db::getConnection('auth');
    
    my $query = '';
    
    # Filtres
    my $whereQuery = '';
    my $orderQuery = '';
    
    # - Utilisateur
    if ($tabFilters->{'country'} ne '')
    {
        $whereQuery .= '
    AND (
        msg_countryfilter IS NULL OR
        msg_countryfilter = "" OR
        msg_countryfilter like "%' . $tabFilters->{'country'} . '%"
    )';
    }
    
    # - Période
    if ($tabFilters->{'period'} eq '')
    {
        $whereQuery .= ' AND NOW() BETWEEN msg_date_start AND msg_date_end';
    }
    
    # - Ordre
    if ($tabFilters->{'order'} eq '')
    {
        $orderQuery = ' ORDER BY msg_priority, msg_date_creation';
    }
    elsif ($tabFilters->{'order'} eq 'desc')
    {
        $orderQuery = ' ORDER BY msg_date_creation DESC';
    }

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        $query = '
SELECT
    msg_id AS `id`,
    IFNULL(msl_subject, msg_subject) AS `subject`,
    msg_date_start AS `dateStart`,
    msg_date_end AS `dateEnd`,
    IFNULL(msl_content, msg_content) AS `content`,
    msg_priority AS `priority`,
    msg_countryfilter AS `countryFilter`,
    msg_date_creation AS `dateCreation`,
    msg_sta_id AS `state.id`,
    IF (NOW() BETWEEN msg_date_start AND msg_date_end, 2, IF(NOW() < msg_date_start, 1, 0)) as `flag`
FROM f_message
    LEFT JOIN f_message_locale ON (msl_msg_id = msg_id AND msl_lcl_id = ' . $db->quote($localeId) . ')
WHERE msg_sta_id <> ' . $db->quote(STATE_INACTIVE) . 
    $whereQuery . 
    $orderQuery . ';';
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        $query = '
SELECT
    COUNT(*) AS `label`
FROM f_message
WHERE msg_sta_id <> ' . $db->quote(STATE_INACTIVE) . 
    $whereQuery;
    }
    else
    {
        $query = '
SELECT
    msg_id AS `id`,
    IFNULL(msl_subject, msg_subject) AS `subject`
FROM f_message
    LEFT JOIN f_message_locale ON (msl_msg_id = msg_id AND msl_lcl_id = ' . $db->quote($localeId) . ')
WHERE msg_sta_id <> ' . $db->quote(STATE_INACTIVE) . 
    $whereQuery . '
ORDER BY msg_priority, msg_date_creation;';
    }

    return $db->fetchList($format, $query, {}, 'id');
}

# Function: getTraductions
# Récupère les traductions des messages
#
# Parameters:
# integer $id   - Id du message
#
# Returns:
# hashref       - Liste des traductions pour le message 
sub getTraductions
{
    my ($id) = @_;
    my $format = LOC::Util::GETLIST_ASSOC;

    # connexion à Location
    my $db = &LOC::Db::getConnection('auth');
    
    my $query = '';
    
    $query = '
SELECT
    msl_lcl_id as `locale`,
    msl_subject as `subject`,
    msl_content as `content`
FROM f_message_locale
WHERE msl_msg_id = ' . $id;

    return $db->fetchList($format, $query, {}, 'locale');
}

# Function: set
# Insertion/modification d'un message d'information
#
# Contenu du hachage à passer en paramètre:
# - id            => id du message
# - subject       => sujet du message (en français)
# - dateStart     => date de début d'affichage
# - dateEnd       => date de fin d'affichage
# - content       => contenu principal du message (en français)
# - countryFilter => pays où est utilisé le message
# - priority      => priorité du message
# - trad          => tableau hash contenant les traductions nécessaires
#
# Contenu du hachage trad
# - subject       => tableau locale->subject
# - content       => tableau locale->content
#
# Parameters:
# hashref   $values    - Valeurs du message
#
# Returns:
# integer
sub set
{
     my ($values) = @_;

    # Récupération de la connexion à la base de données
    my $db = &LOC::Db::getConnection('auth');

    my $countError = 0;
    # Mise à jour
    if (defined $values->{'id'} && $values->{'id'} != 0)
    {
        
        # informations générales f_message
        my $result = $db->update('f_message', {
                                                 'msg_subject'       => $values->{'subject'},
                                                 'msg_date_start'    => $values->{'dateStart'},
                                                 'msg_date_end'      => $values->{'dateEnd'},
                                                 'msg_content'       => $values->{'content'},
                                                 'msg_priority'      => $values->{'priority'},
                                                 'msg_countryfilter' => $values->{'countryFilter'}
                                                 },
                                                 {
                                                 'msg_id'                => $values->{'id'}
                                                 });
        if ($result == -1)
        {
            $countError++;
        }

        # traductions f_message_locale
        # - restructuration du tableau des traductions
        my %tabTrad;
        while (my ($locale, $tradSubject) = each(%{$values->{'trad'}->{'subject'}}))
        {
            $tabTrad{$locale}->{'subject'} = $tradSubject;
        }
        while (my ($locale, $tradContent) = each(%{$values->{'trad'}->{'content'}}))
        {
            $tabTrad{$locale}->{'content'} = $tradContent;
        }
        
        my $query = '
SELECT 
    msl_lcl_id AS `locale.id`, 
    msl_subject AS `oldSubject`,
    msl_content AS `oldContent`
FROM f_message_locale 
WHERE msl_msg_id = ' . $values->{'id'};
        
        my $tabTradExist = $db->fetchAssoc($query, {}, 'locale.id');
        
        while (my ($locale, $trad) = each(%tabTrad))
        {
            # action si le sujet ou le message n'est pas vide
            if ($trad->{'subject'} ne '' || $trad->{'content'} ne '')
            {
                # une traduction pour cette locale existe déjà -> mise à jour
                if (defined ($tabTradExist->{$locale}) 
                        && ($trad->{'subject'} ne $tabTradExist->{$locale}->{'oldSubject'} 
                            || $trad->{'content'} ne $tabTradExist->{$locale}->{'oldContent'}
                        ))
                {
                    my $updateTrad = $db->update('f_message_locale', {
                                                                        'msl_subject' => $trad->{'subject'},
                                                                        'msl_content' => $trad->{'content'}
                                                                     },
                                                                     {
                                                                         'msl_msg_id' => $values->{'id'},
                                                                         'msl_lcl_id' => $locale
                                                                     });
                   if ($updateTrad == -1)
                   {
                       $countError++;
                   }
                }
                # une traduction pour cette locale n'existe pas -> création
                if (not(defined $tabTradExist->{$locale}) && ($trad->{'subject'} ne '' || $trad->{'content'} ne ''))
                {
                    my $insertTrad = $db->insert('f_message_locale', {
                                                                        'msl_msg_id'  => $values->{'id'},
                                                                        'msl_lcl_id'  => $locale,
                                                                        'msl_subject' => $trad->{'subject'},
                                                                        'msl_content' => $trad->{'content'}
                                                                     });
                   if ($insertTrad == -1)
                   {
                       $countError++;
                   }
                }
            }
        }
    }
    
    # Création
    else
    {
        # informations générales f_message
        my $result = $db->insert('f_message', {
                                                 'msg_subject'       => $values->{'subject'},
                                                 'msg_date_start'    => $values->{'dateStart'},
                                                 'msg_date_end'      => $values->{'dateEnd'},
                                                 'msg_content'       => $values->{'content'},
                                                 'msg_priority'      => $values->{'priority'},
                                                 'msg_countryfilter' => $values->{'countryFilter'}
                                                 });
        if ($result == -1)
        {
            $countError++;
        }
        $values->{'id'} = $result; 

        # traductions f_message_locale
        # - restructuration du tableau des traductions
        my %tabTrad;
        while (my ($locale, $tradSubject) = each(%{$values->{'trad'}->{'subject'}}))
        {
            $tabTrad{$locale}->{'subject'} = $tradSubject;
        }
        while (my ($locale, $tradContent) = each(%{$values->{'trad'}->{'content'}}))
        {
            $tabTrad{$locale}->{'content'} = $tradContent;
        }

        if ($result != -1)
        {
            while (my ($locale, $trad) = each(%tabTrad))
            {
                # une traduction pour cette locale n'existe pas -> création
                if ($trad->{'subject'} ne '' || $trad->{'content'} ne '')
                {
                    my $insertTrad = $db->insert('f_message_locale', {
                                                                        'msl_msg_id'  => $result,
                                                                        'msl_lcl_id'  => $locale,
                                                                        'msl_subject' => $trad->{'subject'},
                                                                        'msl_content' => $trad->{'content'}
                                                                     });
                   if ($insertTrad == -1)
                   {
                       $countError++;
                   }
                }
            }
        }
    }
    return {'id' => $values->{'id'}, 'error' => $countError};
}

# Function: delete
# Supprime un message d'information (passe son état à STATE_INACTIVE)
#
# Parameters:
# integer  $id     - Id du message à supprimer
#
# Returns:
# -1|0|1           - résultat de la requête
sub delete
{
    my ($id) = @_;
    
    # connexion à Location
    my $db = &LOC::Db::getConnection('auth');
    
    my $result = $db->update(
                        'f_message',
                        {'msg_sta_id' => STATE_INACTIVE},
                        {'msg_id' => $id}
                        );

    return $result;
}
    
    
    
1;