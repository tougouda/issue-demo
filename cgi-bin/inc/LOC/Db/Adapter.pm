use utf8;
use open (':encoding(UTF-8)');

# Class: LOC::Db::Adapter
# Classe de gestion de l'adapteur de bases de données
package LOC::Db::Adapter;
use strict;

use DBI;

use LOC::Util;


# Function: new
# Constructeur de la classe
#
# Parameters:
# array $configs - Configurations de la connexion
sub new
{
    my ($class, %configs) = @_;

    my $this = {};
    bless($this, $class);
    $this->{'_connection'} = undef;
    $this->{'_configs'}  = \%configs;
    $this->{'_fetchMode'} = LOC::Db::FETCH_ASSOC;
    $this->{'_lastQuery'} = '';
    $this->{'_hasTransaction'} = 0;
    $this->{'_tabTransactionActions'} = {};

    return $this;
}


# Function: addTransactionAction
# Ajouter une action programmée lors du commit ou du rollback de la transaction.
#
# Parameters:
# string  $event      - Nom de l’événement ('commit.pre', 'commit.post', 'rollback.pre', 'rollback.post')
# coderef $callback   - Description de la routine à exécuter
# hashref $tabOptions - Options supplémentaires :
#                           - 'priority' : priorité (0 par défaut)
#                           - 'params'   : paramètres qui seront passés au callback
#                           - 'id'       : identifiant pour l’unicité (non défini par défaut)
#                           - 'category' : catégorie ('' par défaut)
#
# Returns:
# bool - 1 en cas de succès, 0 en cas d'échec
sub addTransactionAction
{
    my ($this, $event, $callback, $tabOptions) = @_;

    if (!$this->{'_hasTransaction'})
    {
        return 0;
    }

    if (!defined $tabOptions || ref($tabOptions) ne 'HASH')
    {
        $tabOptions = {};
    }

    if (!exists $this->{'_tabTransactionActions'}->{$event})
    {
        $this->{'_tabTransactionActions'}->{$event} = {
            'currentIndex' => 1,
            'uniqueRefs'   => {},
            'list'         => []
        };
    }

    # Raccourci
    my $tabEvent = $this->{'_tabTransactionActions'}->{$event};

    my $id = (defined $tabOptions->{'id'} ? $tabOptions->{'id'} : '');

    if ($id eq '' || !exists $tabEvent->{'uniqueRefs'}->{$id})
    {
        my $tabActionInfos = {
            'index'    => $tabEvent->{'currentIndex'}++,
            'id'       => $id,
            'callback' => $callback,
            'priority' => (defined $tabOptions->{'priority'} ? $tabOptions->{'priority'} * 1 : 0),
            'category' => (defined $tabOptions->{'category'} ? $tabOptions->{'category'} : ''),
            'params'   => (defined $tabOptions->{'params'}   ? $tabOptions->{'params'} : {})
        };

        push(@{$tabEvent->{'list'}}, $tabActionInfos);

        if ($id ne '')
        {
            $tabEvent->{'uniqueRefs'}->{$id} = 1;
        }
    }

    return 1;
}


# Function: beginTransaction
# Commencer une transaction (désactive l'autocommit)
#
# Returns:
# bool - 1 en cas de succès, 0 en cas d'échec
sub beginTransaction
{
    my ($this) = @_;

    if (!$this->connect())
    {
        return 0;
    }
    if ($this->{'_connection'}->begin_work())
    {
        $this->{'_tabTransactionActions'} = {};
        $this->{'_hasTransaction'} = 1;
        return 1;
    }
    return 0;
}

# Function: connect
# Se connecter à la base de donnée
#
sub connect
{
    my ($this) = @_;

    # Connexion jamais établie ou connexion perdue
    if (!defined $this->{'_connection'} || !$this->{'_connection'}->ping())
    {
        # Paramètres de la connexion à la base
        my $tabParams = {};
        if (lc($this->{'_configs'}->{'dbtype'}) eq 'mssql')
        {
            $tabParams->{'syb_chained_txn'} = 0;
        }
        if (lc($this->{'_configs'}->{'dbtype'}) eq 'mysql')
        {
            $tabParams->{'mysql_enable_utf8'} = 1;
        }

        # Connection à la base de données
        $this->{'_connection'} = DBI->connect($this->{'_configs'}->{'dsn'},
                                             $this->{'_configs'}->{'username'},
                                             $this->{'_configs'}->{'password'},
                                             $tabParams);
    }

    if (!$this->{'_connection'})
    {
        return 0;
    }
    return 1;
}


# Function: close
# Se déconnecter de la base de données
#
sub close
{
    my ($this) = @_;

    # Fermeture de la base de données
    if ($this->{'_connection'})
    {
        $this->{'_connection'}->disconnect();
    }
    $this->{'_connection'} = undef;
}


# Function: commit
# Valider une transaction (réactive l'autocommit)
#
# Returns:
# bool - 1 en cas de succès, 0 en cas d'échec
sub commit
{
    my ($this) = @_;

    if (!$this->connect())
    {
        return 0;
    }

    # Exécution des actions de transaction avant commit
    if (!$this->_execTransactionActions('commit.pre', 1))
    {
        return 0;
    }

    if (!$this->{'_connection'}->commit())
    {
        return 0;
    }

    $this->{'_hasTransaction'} = 0;

    # Exécution des actions de transaction après commit
    $this->_execTransactionActions('commit.post', 0);

    # On vide toutes les actions
    $this->{'_tabTransactionActions'} = {};

    return 1;
}


# Function: delete
# Supprimer une ou plusieurs lignes dans une table
#
# Parameters:
# string       $tableName  - Nom de la table
# hashref      $where      - Clause WHERE
# string|undef $schemaName - Nom du schéma (facultatif)
# bool         $isDebug    - Mode debug ?
#
# Returns:
# int|-1 - Nombre de lignes affectées par la requête
sub delete
{
    my ($this, $tableName, $wheres, $schemaName, $isDebug) = @_;

    my @tabConds = ();

    my $column;
    my $value;
    my $cond;
    while (($column, $value) = each(%$wheres))
    {
        if (ref($value) eq 'ARRAY' || ref($value) eq 'HASH')
        {
            if (substr($column, -1, 1) eq '*')
            {
                $column = substr($column, 0, -1);
            }
            push(@tabConds, $this->quoteIdentifier($column) . ' IN (' . $this->quote($value) . ')');
        }
        else
        {
            if (substr($column, -1, 1) eq '*')
            {
                push(@tabConds, $this->quoteIdentifier(substr($column, 0, -1)) . ' = ' . $value);
            }
            elsif (!defined($value))
            {
                push(@tabConds, $this->quoteIdentifier($column) . ' IS NULL');
            }
            else
            {
                push(@tabConds, $this->quoteIdentifier($column) . ' = ' . $this->quote($value));
            }
        }
    }

    # Génération de la requête SQL
    my $table = '';
    if ($schemaName ne '')
    {
        $table = $this->quoteIdentifier($schemaName) . '.';
    }
    $table .= $this->quoteIdentifier($tableName);
    my $query = 'DELETE FROM ' . $table;
    if (@tabConds > 0)
    {
        $query .= ' WHERE ' . join(' AND ', @tabConds);
    }
    $query .= ';';


    # Exécution de la requête
    if ($isDebug)
    {
        #$this->{'_lastQuery'} = $query;
        print STDERR "\nLOC::Db::Adapter::delete(): " . $query . "\n";
        return 0;
    }
    else
    {
        my $resultObj = $this->query($query);
        if (!$resultObj)
        {
            return -1;
        }
        $resultObj->close();
        return $resultObj->getAffectedRowsCount();
    }
}


# Function: deleteTransactionActions
# Supprimer des actions de transaction.
#
# Parameters:
# string  $event      - Nom de l’événement ('commit.pre', 'commit.post', 'rollback.pre', 'rollback.post')
# hashref $tabFilters - Filtres de recherche des actions (facultatif)
#                           - 'id'       : filtre par identifiant
#                           - 'category' : filtre par catégorie
#                           - 'priority' : filtre par priorité
#
# Returns:
# int - Nombre de suppressions
sub deleteTransactionActions
{
    my ($this, $event, $tabFilters) = @_;

    if (!exists $this->{'_tabTransactionActions'}->{$event})
    {
        return 0;
    }

    if (!defined $tabFilters || ref($tabFilters) ne 'HASH')
    {
        $tabFilters = {};
    }

    # Raccourci
    my $tabEvent = $this->{'_tabTransactionActions'}->{$event};

    # Nombre de suppressions effectuées
    my $nbDeletes = 0;

    # On recherche les actions de l'événement sélectionné respectant le filtre (par id et/ou priorité et/ou catégorie)
    my $i = 0;
    while ($i < @{$tabEvent->{'list'}})
    {
        my $tabActionInfos = $tabEvent->{'list'}->[$i];

        if ((!exists $tabFilters->{'id'} || $tabActionInfos->{'id'} eq $tabFilters->{'id'}) &&
            (!exists $tabFilters->{'category'} || $tabActionInfos->{'category'} eq $tabFilters->{'category'}) &&
            (!exists $tabFilters->{'priority'} || $tabActionInfos->{'priority'} == $tabFilters->{'priority'}))
        {
            # Si l'action courante respecte le filtre de recherche, alors on la supprime de la liste
            if ($tabActionInfos->{'id'} && exists $tabEvent->{'uniqueRefs'}->{$tabActionInfos->{'id'}})
            {
                # Si jamais cette action est identifiée par un identifiant unique alors on supprime son index
                # de la liste des références uniques
                delete($tabEvent->{'uniqueRefs'}->{$tabActionInfos->{'id'}});
            }

            # Suppression de l'action
            splice(@{$tabEvent->{'list'}}, $i, 1);
            $nbDeletes++;
        }
        else
        {
            # Sinon on passe à la suivante
            $i++;
        }
    }

    return $nbDeletes;
}


# Function: disableQueryCache
# Désactiver le cache sur les requêtes SQL pour la session en cours (pour du déboguage)
#
# Returns:
# bool - 1 en cas de succès, 0 en cas d'échec
sub disableQueryCache
{
    my ($this) = @_;

    if (!$this->connect())
    {
        return 0;
    }

    my $result = $this->{'_connection'}->do('SET query_cache_type = "OFF";');

    return (defined $result ? 1 : 0);
}


# Function: do
# Exécuter une requête SQL
#
# Parameters:
# string   $sqlQuery - Requête à éxécuter
#
# Returns:
# 0 -
sub do
{
    my ($this, $sqlQuery, $tabBinds) = @_;

    my $resultObj = $this->query($sqlQuery, $tabBinds);
    if (!$resultObj)
    {
        return -1;
    }
    my $nb = $resultObj->getAffectedRowsCount();
    $resultObj->close();
    return $nb;
}


# Function: fetchAll
# Récupérer la totalité du résultat d'une requête
# Utilise le mode de récupération courant du connecteur
#
# Parameters:
# string   $sqlQuery       - Requête SQL
# arrayref $tabBinds       - Tableau des variables à associer
# string   $columnUniqueId - Id de la colonne unique
#
# Returns:
# hashref|arrayref|0 - Tableau contenant toutes les lignes du résultat, sinon 0 en cas d'erreur
sub fetchAll
{
    my ($this, $sqlQuery, $tabBinds, $columnUniqueId) = @_;

    my $resultObj = $this->query($sqlQuery, $tabBinds);
    if (!$resultObj)
    {
        return 0;
    }

    my $tab = $resultObj->fetchAll($this->{'_fetchMode'}, $columnUniqueId);
    $resultObj->close();
    return $tab;
}


# Function: fetchPairs
# Récupére toutes les lignes du résultat sous forme d'un tableau Clé => Valeur
# La clé doit être dans la première colonne et la valeur dans la deuxième
#
# Parameters:
# string   $sqlQuery - Requête SQL
# arrayref $tabBinds - Tableau des variables à associer
#
# Returns:
# hashref
sub fetchPairs
{
    my ($this, $sqlQuery, $tabBinds) = @_;

    my $resultObj = $this->query($sqlQuery, $tabBinds);
    if (!$resultObj)
    {
        return 0;
    }

    my $tab = $resultObj->fetchPairs();
    $resultObj->close();
    return $tab;
}


# Function: fetchAssoc
# Récupérer la totalité du résultat d'une requête sous forme de tableaux associatifs
#
# Parameters:
# string   $sqlQuery       - Requête SQL
# arrayref $tabBinds       - Tableau des variables à associer
# string   $columnUniqueId - Id de la colonne unique
#
# Returns:
# hashref|arrayref|0 - Tableau contenant toutes les lignes du résultat, sinon 0 en cas d'erreur
sub fetchAssoc
{
    my ($this, $sqlQuery, $tabBinds, $columnUniqueId) = @_;

    my $resultObj = $this->query($sqlQuery, $tabBinds);
    if (!$resultObj)
    {
        return 0;
    }

    my $tab = $resultObj->fetchAll(LOC::Db::FETCH_ASSOC, $columnUniqueId);
    if (!$resultObj)
    {
        return 0;
    }
    $resultObj->close();
    return $tab;
}


# Function: fetchCol
# Récupérer la totalité de la première colonne du résultat d'une requête sous forme d'une liste
#
# Parameters:
# string $sqlQuery - Requête SQL
# array  $tabBinds - Tableau des variables ou valeurs à associer
#
# Returns:
# array|false - Liste des valeurs dans la première colonne du résultat, sinon 0 en cas d'erreur
sub fetchCol
{
    my ($this, $sqlQuery, $tabBinds) = @_;

    my $stmt = $this->query($sqlQuery, $tabBinds);
    if (!$stmt)
    {
        return 0;
    }
    my @tab = ();
    my $row;
    while ($row = $stmt->fetch(LOC::Db::FETCH_NUM))
    {
        push(@tab, $row->[0]);
    }
    $stmt->close();
    return (wantarray ? @tab : \@tab);
}


# Function: fetchList
# Récupérer une liste dans un certain format
#
# Parameters:
# int      $format   - Format de la liste en sortie
# string   $sqlQuery - Requête
# arrayref $tabBinds - Tableau des variables à associer
# string   $colId    - Id de la colonne unique (facultatif)
#
# Returns:
# hashref|arrayref - En fonction de $format
sub fetchList
{
    my ($this, $format, $sqlQuery, $tabBinds, $columnUniqueId) = @_;

    if ($format == LOC::Util::GETLIST_ASSOC)
    {
        return $this->fetchAssoc($sqlQuery, $tabBinds, $columnUniqueId);
    }
    elsif ($format == LOC::Util::GETLIST_IDS)
    {
        return $this->fetchCol($sqlQuery, $tabBinds);
    }
    elsif ($format == LOC::Util::GETLIST_COUNT)
    {
        return $this->fetchOne($sqlQuery, $tabBinds);
    }
    return $this->fetchPairs($sqlQuery);
}


# Function: fetchOne
# Récupérer la première valeur de la première colonne du résultat d'une requête
#
# Parameters:
# string   $sqlQuery - Requête SQL
# arrayref $tabBinds - Tableau des variables à associer
#
# Returns:
# int|string - Valeur
sub fetchOne
{
    my ($this, $sqlQuery, $tabBinds) = @_;

    my $resultObj = $this->query($sqlQuery, $tabBinds);
    if (!$resultObj)
    {
        return undef;
    }

    my $row = $resultObj->fetch(LOC::Db::FETCH_NUM);
    $resultObj->close();
    return $row->[0];
}


# Function: fetchRow
# Récupérer la première ligne du résultat dans un tableau
# Utilise le mode de récupération courant du connecteur si il n'est pas renseigné en 3ème paramètre
#
# Parameters:
# string   $sqlQuery  - Requête SQL
# arrayref $tabBinds  - Tableau des variables à associer
# int      $fetchMode - Mode de récupération
#
# Returns:
# hashref|arrayref|0 - Tableau contenant la première ligne du résultat, sinon 0 en cas d'erreur
sub fetchRow
{
    my ($this, $sqlQuery, $tabBinds, $fetchMode) = @_;

    # Mode de répération par défaut ?
    if (!defined $fetchMode)
    {
        $fetchMode = $this->{'_fetchMode'};
    }

    my $resultObj = $this->query($sqlQuery, $tabBinds);
    if (!$resultObj)
    {
        return 0;
    }

    my $row = $resultObj->fetch($fetchMode);
    $resultObj->close();
    return $row;
}


# Function: hasTransaction
# Indique si une transaction est en cours ou non
#
# Returns:
# bool -
sub hasTransaction
{
    my ($this) = @_;
    return $this->{'_hasTransaction'};
}


# Function: query
# Exécuter une requête SQL
#
# Parameters:
# string   $sqlQuery - Requête à éxécuter
# arrayref $tabBinds - Tableau des variables à associer
#
# Returns:
# LOC::Db::Result|0 -
sub query
{
    my ($this, $sqlQuery, $tabBinds) = @_;

    if (!$this->connect())
    {
        return 0;
    }

    $this->{'_lastQuery'} = $sqlQuery;

    return LOC::Db::Result->new($this, $sqlQuery);
}


# Function: quote
# Echapper une chaîne de caractères
#
# Parameters:
# string $value - Chaîne
#
# Returns:
# string - Chaîne échappée, sinon FALSE en cas d'erreur
sub quote
{
    my ($this, $value) = @_;
    if (!$this->connect())
    {
        return $value;
    }

    if (@_ > 2)
    {
        shift(@_);
        return join(', ', map($this->quote($_), @_));
    }
    elsif (ref($value) eq 'ARRAY')
    {
        return join(', ', map($this->quote($_), @$value));
    }
    elsif (ref($value) eq 'HASH')
    {
        return join(', ', map($this->quote($_), values(%$value)));
    }
    elsif (&LOC::Util::isInt($value))
    {
        return $value;
    }
    return $this->{'_connection'}->quote($value);
}


# Function: quoteIdentifier
# Echapper un nom de colonne ou de table
#
# Parameters:
# string $value - Valeur
#
# Returns:
# string - Valeur échappée
sub quoteIdentifier
{
    my ($this, $value) = @_;

    # Rectificatif pour MSSQL
    if (lc($this->{'_configs'}->{'dbtype'}) eq 'mssql')
    {
        return '[' . $value . ']';
    }

    if (!$this->connect())
    {
        return $value;
    }
    return $this->{'_connection'}->quote_identifier($value);
}


# Function: getErrorCode
# Récupérer le dernier code d'erreur
#
# Returns:
# int -
sub getErrorCode
{
    my ($this) = @_;

    return $this->{'_connection'}->err;
}


# Function: getErrorMsg
# Récupérer le dernier message d'erreur
#
# Returns:
# string -
sub getErrorMsg
{
    my ($this) = @_;

    return $this->{'_connection'}->errstr;
}


# Function: getFetchMode
# Récupérer le mode de récupération des lignes du jeu de résultats
#
# Returns:
# int -
sub getFetchMode
{
    my ($this) = @_;

    return $this->{'_fetchMode'};
}


# Function: getLastInsertId
# Récupérer l'identifiant généré par la dernière requête INSERT
#
# Parameters:
# string|undef $catalog -
# string|undef $schema  -
# string|undef $table   -
# string|undef $field   -
#
# Returns:
# int|0 - Identifiant ou 0 en cas d'échec
sub getLastInsertId
{
    my ($this, $catalog, $schema, $table, $field) = @_;

    if (!$this->connect())
    {
        return 0;
    }
    return $this->{'_connection'}->last_insert_id($catalog, $schema, $table, $field);
}


# Function: getLastQuery
# Récupérer la dernière requête éxécutée
#
# Returns:
# string -
sub getLastQuery()
{
    my ($this) = @_;

    return $this->{'_lastQuery'};
}


# Function: getResource
# Récupérer la connexion à la base de données
#
# Returns:
# DBI -
sub getResource
{
    my ($this) = @_;

    return $this->{'_connection'};
}


# Function: getType
# Récupérer le type de base de données
#
# Returns:
# string -
sub getType
{
    my ($this) = @_;

    return $this->{'_configs'}->{'dbtype'};
}


# Function: setFetchMode
# Définir le mode de récupération des lignes du jeu de résultats
#
# Parameters:
# int $mode - Mode de récupération
#
# Returns:
# Db::Adapter - Instance courante
sub setFetchMode
{
    my ($this, $mode) = @_;
    $this->{'_fetchMode'} = $mode;
    return $this;
}


# Function: rollBack
# Annuler une transaction
#
sub rollBack
{
    my ($this) = @_;

    if (!$this->connect())
    {
        return 0;
    }

    # Exécution des actions de transaction avant rollback
    $this->_execTransactionActions('rollback.pre', 0);

    my $result = $this->{'_connection'}->rollback();
    $this->{'_hasTransaction'} = 0;

    # Exécution des actions de transaction après rollback
    $this->_execTransactionActions('rollback.post', 0);

    # On vide toutes les actions
    $this->{'_tabTransactionActions'} = {};

    return $result;
}

# Function: insert
# Insérer une ligne dans une table
#
# Parameters:
# string       $tableName  - Nom de la table
# hashref      $values     - Tableau des colonnes avec leurs valeurs
# string|undef $schemaName - Nom du schéma (facultatif)
# bool         $isDebug    - Mode debug ?
#
# Returns:
# int|-1 - Identifiant généré par la requête
sub insert
{
    my ($this, $tableName, $values, $schemaName, $isDebug) = @_;

    my @tabColumns = ();
    my @tabValues  = ();

    my $column;
    my $value;
    while (($column, $value) = each(%$values))
    {
        if (substr($column, -1, 1) eq '*')
        {
            push(@tabColumns, $this->quoteIdentifier(substr($column, 0, -1)));
            push(@tabValues, $value);
        }
        else
        {
            push(@tabColumns, $this->quoteIdentifier($column));
            push(@tabValues, $this->quote($value));
        }
    }

    # Génération de la requête SQL
    my $table = '';
    if ($schemaName ne '')
    {
        $table = $this->quoteIdentifier($schemaName) . '.';
    }
    $table .= $this->quoteIdentifier($tableName);
    my $query = 'INSERT INTO ' . $table . ' (' . join(', ', @tabColumns) . ') ' .
                'VALUES (' . join(', ', @tabValues) . ');';


    # Exécution de la requête
    if ($isDebug)
    {
        #$this->{'_lastQuery'} = $query;
        print STDERR "\nLOC::Db::Adapter::insert(): " . $query . "\n";
        return 0;
    }
    else
    {
        my $resultObj = $this->query($query);
        if (!$resultObj)
        {
            return -1;
        }
        $resultObj->close();
        return $this->getLastInsertId();
    }
}

# Function: insertOrUpdate
# Insérer ou modifier une ligne dans une table
#
# Parameters:
# string       $tableName  - Nom de la table
# hashref      $values     - Tableau des colonnes avec leurs valeurs
# hashref      $keys       - Tableau des clées
# string|undef $schemaName - Nom du schéma (facultatif)
# bool         $isDebug    - Mode debug ?
#
# Returns:
# int - Identifiant généré par la requête
sub insertOrUpdate
{
    my ($this, $tableName, $values, $keys, $schemaName, $isDebug) = @_;

    my @tabColumns = ();
    my @tabValues  = ();
    my @tabSetters = ();

    my $key;
    my $column;
    my $value;
    while (($column, $key) = each(%$keys))
    {
        if (substr($column, -1, 1) eq '*')
        {
            push(@tabColumns, $this->quoteIdentifier(substr($column, 0, -1)));
            push(@tabValues, $key);
        }
        else
        {
            push(@tabColumns, $this->quoteIdentifier($column));
            push(@tabValues, $this->quote($key));
        }
    }
    while (($column, $value) = each(%$values))
    {
        if (substr($column, -1, 1) eq '*')
        {
            push(@tabSetters, $this->quoteIdentifier(substr($column, 0, -1)) . ' = ' . $value);
            if (!exists $keys->{$column})
            {
                push(@tabColumns, $this->quoteIdentifier(substr($column, 0, -1)));
                push(@tabValues, $value);
            }
        }
        else
        {
            push(@tabSetters, $this->quoteIdentifier($column) . ' = ' . $this->quote($value));
            if (!exists $keys->{$column})
            {
                push(@tabColumns, $this->quoteIdentifier($column));
                push(@tabValues, $this->quote($value));
            }
        }
    }

    # Génération de la requête SQL
    my $table = '';
    if ($schemaName ne '')
    {
        $table = $this->quoteIdentifier($schemaName) . '.';
    }
    $table .= $this->quoteIdentifier($tableName);
    my $query = 'INSERT INTO ' . $table . ' (' . join(', ', @tabColumns) . ') ' .
                'VALUES (' . join(', ', @tabValues) . ') ' .
                'ON DUPLICATE KEY UPDATE ' . join(', ', @tabSetters) . ';';


    # Exécution de la requête
    if ($isDebug)
    {
        #$this->{'_lastQuery'} = $query;
        print STDERR "\nLOC::Db::Adapter::insertOrUpdate(): " . $query . "\n";
        return 0;
    }
    else
    {
        my $resultObj = $this->query($query);
        if (!$resultObj)
        {
            return -1;
        }
        $resultObj->close();
        return $this->getLastInsertId();
    }
}

# Function: update
# Mettre à jour une ou plusieurs lignes dans une table
#
# Parameters:
# string       $tableName  - Nom de la table
# hashref      $values     - Tableau des colonnes avec leurs valeurs
# hashref      $wheres     - Clause WHERE
# string|undef $schemaName - Nom du schéma (facultatif)
# bool         $isDebug    - Mode debug ?
#
# Returns:
# int|-1 - Nombre de lignes affectées par la requête
sub update
{
    my ($this, $tableName, $values, $wheres, $schemaName, $isDebug) = @_;

    my @tabSetters = ();
    my @tabConds = ();

    my $column;
    my $value;
    while (($column, $value) = each(%$values))
    {
        if (substr($column, -1, 1) eq '*')
        {
            push(@tabSetters, $this->quoteIdentifier(substr($column, 0, -1)) . ' = ' . $value);
        }
        else
        {
            push(@tabSetters, $this->quoteIdentifier($column) . ' = ' . $this->quote($value));
        }
    }
    my $cond;
    while (($column, $value) = each(%$wheres))
    {
        if (ref($value) eq 'ARRAY' || ref($value) eq 'HASH')
        {
            if (substr($column, -1, 1) eq '*')
            {
                $column = substr($column, 0, -1);
            }
            push(@tabConds, $this->quoteIdentifier($column) . ' IN (' . $this->quote($value) . ')');
        }
        else
        {
            if (substr($column, -1, 1) eq '*')
            {
                push(@tabConds, $this->quoteIdentifier(substr($column, 0, -1)) . ' = ' . $value);
            }
            elsif (!defined($value))
            {
                push(@tabConds, $this->quoteIdentifier($column) . ' IS NULL');
            }
            else
            {
                push(@tabConds, $this->quoteIdentifier($column) . ' = ' . $this->quote($value));
            }
        }
    }

    # Génération de la requête SQL
    my $table = '';
    if ($schemaName ne '')
    {
        $table = $this->quoteIdentifier($schemaName) . '.';
    }
    $table .= $this->quoteIdentifier($tableName);
    my $query = 'UPDATE ' . $table . ' SET ' . join(', ', @tabSetters);
    if (@tabConds > 0)
    {
        $query .= ' WHERE ' . join(' AND ', @tabConds);
    }
    $query .= ';';


    # Exécution de la requête
    my $rowsCount;
    if ($isDebug)
    {
        #$this->{'_lastQuery'} = $query;
        print STDERR "\nLOC::Db::Adapter::update(): " . $query . "\n";
        $rowsCount = 0;
    }
    else
    {
        my $resultObj = $this->query($query);
        if (!$resultObj)
        {
            return -1;
        }
        $rowsCount = $resultObj->getAffectedRowsCount();
        $resultObj->close();
    }
    return ($rowsCount == -1 ? 0 : $rowsCount);
}


# Function: _execTransactionActions
# Exécution, dans l'ordre, des actions programmées lors du commit ou du rollback de la transaction.
#
# Parameters:
# string  $event            - Nom de l’événement ('commit.pre', 'commit.post', 'rollback.pre', 'rollback.post')
# bool    $isStoppedOnError - Est-ce que la fonction doit s'arrêter dès qu'un callback retourne une erreur ?
#
# Returns:
# bool - 1 en cas de succès, 0 en cas d'échec
sub _execTransactionActions
{
    my ($this, $event, $isStoppedOnError) = @_;

    if (exists $this->{'_tabTransactionActions'}->{$event})
    {
        # Raccourci
        my $tabEvent = $this->{'_tabTransactionActions'}->{$event};

        # Tant qu'il y a des actions pour cet événement
        # (une action peut rajouter des actions à son tour sur l'événement courant, attention aux boucles infinies !)
        while (@{$tabEvent->{'list'}} > 0)
        {
            # Tri des actions de l'événement
            my @tabSorted = sort {$b->{'priority'} <=> $a->{'priority'} || $a->{'index'} <=> $b->{'index'}} @{$tabEvent->{'list'}};

            # On vide la liste des actions de cet événement
            $tabEvent->{'currentIndex'} = 1;
            $tabEvent->{'uniqueRefs'}   = {};
            $tabEvent->{'list'}         = [];

            # Exécution de toutes les actions dans l'ordre
            foreach my $tabActionsInfos (@tabSorted)
            {
                my $result = &{$tabActionsInfos->{'callback'}}($this, $event, $tabActionsInfos->{'params'});
                if ($isStoppedOnError && !$result)
                {
                    # Dans le cas où une seule erreur provoque l'arrêt de l'éxécution
                    delete($this->{'_tabTransactionActions'}->{$event});
                    return 0;
                }
            }
        }

        delete($this->{'_tabTransactionActions'}->{$event});
    }

    return 1;
}

# Function: truncate
# Vider une table
#
# Parameters:
# string       $tableName  - Nom de la table
# string|undef $schemaName - Nom du schéma (facultatif)
# bool         $isDebug    - Mode debug ?
#
# Returns:
# 0|1|-1
sub truncate
{
    my ($this, $tableName, $schemaName, $isDebug) = @_;

    # Génération de la requête SQL
    my $table = '';
    if ($schemaName ne '')
    {
        $table = $this->quoteIdentifier($schemaName) . '.';
    }
    $table .= $this->quoteIdentifier($tableName);
    my $query = 'TRUNCATE TABLE ' . $table . ';';

    # Exécution de la requête
    if ($isDebug)
    {
        print STDERR "\nLOC::Db::Adapter::truncate(): " . $query . "\n";
        return 0;
    }
    else
    {
        my $resultObj = $this->query($query);
        if (!$resultObj)
        {
            return -1;
        }
        $resultObj->close();
        return 1;
    }

}



1;