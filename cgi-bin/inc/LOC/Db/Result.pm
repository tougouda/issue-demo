use utf8;
use open (':encoding(UTF-8)');

# Class: LOC::Db::Result
# Classe de gestion des bases de données
package LOC::Db::Result;


# Constants:
# MAXATTEMPTS - Nombre maximal de tentatives lors d'un interblocage
use constant
{
    MAXATTEMPTS => 3,
};


use strict;


use LOC::Log;
use Tie::IxHash;

# Function: new
# Constructeur de la classe
#
# Parameters:
# array $dbObj - Configurations de la connexion
# array $query - Requête
sub new
{
    my ($class, $dbObj, $query) = @_;

    my $this = {};
    bless($this, $class);

    $this->{'_dbObj'} = $dbObj;
    $this->{'_query'} = $query;

    # Décodage UTF-8 pour les bases MSSQL
    if (lc($this->{'_dbObj'}->getType()) eq 'mssql')
    {
        # Convertit les chaînes UTF-8 en séquences d'octets
        $query = &LOC::Util::encodeUtf8($query);
    }

    my $hasError = 0;
    my $hasDeadlock = 0;
    my @tabLogContent;
    local $SIG{__WARN__} = sub
    {
        my $error = $_[0];
        if ($error =~ /DBD::mysql::st execute failed: Deadlock found when trying to get lock/)
        {
            $hasDeadlock = 1;
        }
        else
        {
            push(@tabLogContent, $query . "\n" . $error . "----------");
            $hasError = 1;
        }
    };

    for (my $i = 0; $i < MAXATTEMPTS; $i++)
    {
        $hasDeadlock = 0;

        $this->{'_result'} = $dbObj->getResource()->prepare($query);
        $this->{'_result'}->execute();

        if ($hasDeadlock)
        {
            my $message = 'Interblocage sur la requête suivante après l\'essai n° ' . ($i + 1) . " :\n$query";
            if ($i == MAXATTEMPTS - 1)
            {
                $message .= "\nOpération interrompue";
            }
            else
            {
                $message .= "\nNouvel essai";
            }
            $message .= "\n----------";
            &LOC::Log::writeFileLog('db', $message, 'error', LOC::Log::MODE_SEND_MAIL);
        }
        else
        {
            last;
        }

        sleep(0.5);
    }

    if ($hasError || $hasDeadlock)
    {
        my $logContent = "\n";
        my $nbLogContent = @tabLogContent;
        for (my $i = 0; $i < $nbLogContent; $i++)
        {
            $logContent .= ($nbLogContent > 1 ? 'Tentative n° ' . ($i + 1) . ":\n" : '') . $tabLogContent[$i] . "\n";
        }
        &LOC::Log::writeFileLog('db', $logContent, 'error', LOC::Log::MODE_SEND_MAIL);
        return 0;
    }
    return $this;
}


# Function: close
# Fermer la requête préparée
#
sub close
{
    my ($this) = @_;

    $this->{'_result'}->finish;
}


# Function: dataSeek
# Déplacer le pointeur interne de résultat
#
# Parameters:
# int $offset - La position de la ligne désirée
#
# Returns:
# int -
sub dataSeek
{
    my ($this, $offset) = @_;
}


# Function: fetch
# Récupérer la ligne courante du résultat
#
# Parameters:
# int|undef $fetchMode - Mode de récupération
#
# Returns:
# arrayref|hashref - Ligne du résultat courante
sub fetch
{
    my ($this, $fetchMode) = @_;

    if (!defined $fetchMode)
    {
        $fetchMode = $this->{'_dbObj'}->getFetchMode();
    }

    if ($fetchMode == LOC::Db::FETCH_NUM)
    {
        my @data = $this->{'_result'}->fetchrow_array();
        unless (@data)
        {
            return undef;
        }
        # Encodage UTF-8 pour les bases MSSQL
        if (lc($this->{'_dbObj'}->getType()) eq 'mssql')
        {
            # Convertit les séquences d'octets en chaînes UTF-8
            return &LOC::Util::decodeUtf8(\@data);
        }

        return \@data;
    }
    elsif ($fetchMode == LOC::Db::FETCH_ASSOC)
    {
        my $data = $this->{'_result'}->fetchrow_hashref();
        unless ($data)
        {
            return undef;
        }
        # Encodage UTF-8 pour les bases MSSQL
        if (lc($this->{'_dbObj'}->getType()) eq 'mssql')
        {
            # Convertit les séquences d'octets en chaînes UTF-8
            return &LOC::Util::decodeUtf8($data);
        }
        return $data;
    }
}


# Function: fetchAll
# Récupérer la totalité du résultat d'une requête
# Utilise le mode de récupération courant du connecteur
#
# Parameters:
# int|null $fetchMode      - Mode de récupération
# string   $columnUniqueId - Id de la colonne unique
#
# Returns:
# hashref|arrayref - Tableau contenant toutes les lignes du résultat, sinon 0 en cas d'erreur
sub fetchAll
{
    my ($this, $fetchMode, $columnUniqueId) = @_;

    if (!defined $fetchMode)
    {
        $fetchMode = $this->{'_dbObj'}->getFetchMode();
    }

    # Remet à zéro le pointeur sur les données
    $this->dataSeek(0);

    my $data;
    if (defined $columnUniqueId)
    {
        tie(my %tab, 'Tie::IxHash');
        %tab = ();
        while ($data = $this->fetch($fetchMode))
        {
            if ($fetchMode == LOC::Db::FETCH_NUM)
            {
                $tab{$data->[$columnUniqueId]} = $data;
            }
            elsif ($fetchMode == LOC::Db::FETCH_ASSOC)
            {
                $tab{$data->{$columnUniqueId}} = $data;
            }
        }
        return \%tab;
    }
    else
    {
        my @tab = ();
        while ($data = $this->fetch($fetchMode))
        {
            push(@tab, $data);
        }
        return \@tab;
    }
}


# Function: fetchPairs
# Récupére toutes les lignes du résultat sous forme d'un tableau Clé => Valeur
# La clé doit être dans la première colonne et la valeur dans la deuxième
#
# Returns:
# hashref - Tableau contenant les paires Clé => Valeur
sub fetchPairs
{
    my ($this) = @_;

    # Remet à zéro le pointeur sur les données
    $this->dataSeek(0);

    tie(my %tab, 'Tie::IxHash');
    %tab = ();

    my $row;
    if ($this->getColumnsCount() > 1)
    {
        # Récupération des lignes
        while ($row = $this->fetch(LOC::Db::FETCH_NUM))
        {
            $tab{$row->[0]} = $row->[1];
        }
    }
    else
    {
        # Récupération des lignes
        while ($row = $this->fetch(LOC::Db::FETCH_NUM))
        {
            $tab{$row->[0]} = $row->[0];
        }
    }

    return \%tab;
}


# Function: getAffectedRowsCount
# Récupérer le nombre de lignes affectées par la requête (de type INSERT, UPDATE, DELETE)
#
# Returns:
# int - Nombre de lignes affectées
sub getAffectedRowsCount
{
    my ($this) = @_;

    return $this->{'_result'}->rows;
}


# Function: getColumnsCount
# Récupérer le nombre de colonnes de la requête
#
# Returns:
# int -
sub getColumnsCount
{
    my ($this) = @_;

    return $this->{'_result'}->{'NUM_OF_FIELDS'};
}


# Function: getErrorCode
# Récupérer le code d'erreur
#
# Returns:
# int -
sub getErrorCode
{
    my ($this) = @_;

    return $this->{'_result'}->err;
}


# Function: getErrorMsg
# Récupérer le message de l'erreur
#
# Returns:
# string -
sub getErrorMsg
{
    my ($this) = @_;

    return $this->{'_result'}->errstr;
}


# Function: getResource
# Récupérer la resource de la requête
#
# Returns:
# resource -
sub getResource
{
    my ($this) = @_;

    return $this->{'_result'};
}


# Function: getRowsCount
# Récupérer le nombre de lignes du résultat
#
# Returns:
# int|-1 -
sub getRowsCount
{
    my ($this) = @_;

    return $this->{'_result'}->rows;
}

1;