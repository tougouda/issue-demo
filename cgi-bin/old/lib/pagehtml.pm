package pagehtml;
require Exporter;
@ISA    = qw( Exporter );
@EXPORT = qw(Entete Pied);

use varglob;
use lib '../../inc', '../../lib';

use LOC::Globals;


my $build = &LOC::Globals::get('build');

sub Entete {
	
print <<EOT;
content-type: text/html\n
<HTML>
<HEAD>
<TITLE>$_[0]</TITLE>
<script language="JavaScript" src="$URL_CONF/super.js?bld=$build"></script>
<script language=JavaScript>
<!--
document.ondblclick=dblclick;
-->
</script>
<style type="text/css">
<!--
INPUT:focus{background-color:red}
INPUT:hover{background-color:red}
.select{background:#99CCFF;color:#0033CC;font-family:Courier New,Courier,Arial,Helvetica,Verdana;font-size:10pt;font-weight: bold;}
.tableau{font-family:Arial,Helvetica}
-->
</STYLE>
</HEAD>	
<BODY BACKGROUND="$URL_IMG/fond-logpr.gif" onload="focus_vide()">
<H1 align="center">$_[0]</H1>
<BR><BR><BR><BR><BR>
EOT
}

sub Pied {
  
print <<EOT;
</BODY>
</HTML>
EOT

}

1;
#onBlur="this.style.backgroundColor='' "onFocus="this.style.backgroundColor='FFFFCC'
#<style>
#<!--
#A:link {text-decoration: none}  
#A:visited {text-decoration: none}  
#A:active {text-decoration: none}
#-->
#</style>
#.Rollover { 
#   border       : thin solid blue; 
#   onmouseover  : "this.src = this.getAttribute('oversrc');this.style.borderColor = 'red';statusText.data = this.getAttribute('status');" 
#   onmouseout   : "this.src = this.getAttribute('outsrc');this.style.borderColor = 'blue';statusText.data = '';"
#}

#input.cancel { key-equivalent: esc cmd-. N } /* plusieurs possibilitÚs */
#input.ok     { key-equivalent: return enter Y }
#input.open   { key-equivalent: ctrl-O cmd-O accesskey-O}
#    /* accesskey : comme ACCESSKEY en HTML4.0 */
#input.close  { key-equivalent: alt-f4 cmd-W}
#input.copy   { key-equivalent: ctrl-C cmd-C copy accesskey-C}
#input.cancel { key-equivalent: system-cancel }
#input.ok     { key-equivalent: system-ok }

