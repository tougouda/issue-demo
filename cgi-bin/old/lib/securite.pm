# $Id: securite.pm,v 1.1.1.1 2006/08/01 07:48:07 clliot Exp $
# $Log: securite.pm,v $
# Revision 1.1.1.1  2006/08/01 07:48:07  clliot
# no message
#
# Revision 2.3  2006/02/06 16:52:40  julien
# V. 1.4 : Tarification v2
#
# Revision 2.2.2.1  2006/01/23 11:13:04  julien
# no message
#
# Revision 2.2  2005/08/26 10:01:38  julien
# Pr�fixage des tables de AUTH dans les requ�tes.
#
# Revision 2.1  2005/06/21 18:45:03  julien
# while -> if.
#
# Revision 2.0  2004/09/16 15:07:22  julien
# Version r�cup�r�e de Lancelot et nettoy�e
#
# Revision 1.2  2004/09/16 14:34:47  julien
# Ajout des identifiants
#

package securite;

require Exporter;
@ISA =qw(Exporter);
@EXPORT =qw(ip big_brother droit lang login doubl_conn trace_imp message ver_nav);
use strict;
use varglob;
use DBI;
use CGI;

#navigateur
sub ver_nav {
my $version;
my $var;
my $val;
foreach $var (sort(keys(%ENV))) 	
	{
    $val = $ENV{$var};
  	if (${var} eq "HTTP_USER_AGENT")
   		{
   		$version=substr(${val},30,1);
   		}	
	}
	if ($version eq "") {$version=0};
	return $version;
}


# adrresse IP de la machine;

sub ip{

my $var;
my $val;
my $ip;
foreach $var (sort(keys(%ENV))) {
    $val = $ENV{$var};
   # $val =~ s|\n|\\n|g;
    $val =~ s|"|\\"|g;
  
    if (${var} eq "HTTP_X_FORWARDED_FOR") {   
   # print "${var}=\"${val}\"\n";
     my @ipprox;
     @ipprox=split(/\,/,${val});
        $ip=$ipprox[0];
         last;}
    if (${var} eq "REMOTE_ADDR") {
        $ip=${val};
       last;}
   
}
  return $ip;
  
  }
#
#  login utilisateur
#
sub login {
 	my $var;
	my $val;
	my $util;

foreach $var (sort(keys(%ENV))) {
    $val = $ENV{$var};
   # $val =~ s|\n|\\n|g;
    $val =~ s|"|\\"|g;
    if (${var} eq "REMOTE_USER")
    {  
      $util=${val};
      last;
	}
   
}

return $util;

}

#
#  langue
#
sub lang {
 	my $var;
	my $val;
	my $lang;

foreach $var (sort(keys(%ENV))) {
    $val = $ENV{$var};
    $val =~ s|\n|\\n|g;
    $val =~ s|"|\\"|g;
    if (${var} eq "HTTP_ACCEPT_LANGUAGE")
    {  
      $lang=${val};
      last;
	}
   }
   return $lang;
   
}



#
#  droit 
#
sub droit { 
  my $cgi=new CGI;
  my $nom;
  my $value;
  
 
  $nom="acces_industrie_loc";
  $value="tonneins";
# $cookie=$cgi->cookie(-name=>"$nom" , -value=>"$value" , -expires=>'+18h');
  if (not(defined($cgi->cookie($nom))))
   {    my $cookie;
  	$cookie=$cgi->cookie(-name=>$nom , -value=>$value , -expires=>'+18h');
  	print header(-type=>'text/html',
  	-cookie=>$cookie);
    if (not($cgi->param('login') eq "tonneins"))
   	{my $tableau;
   	$tableau =$cgi->start_form(-method=>'GET' , action=>'DBase.cgi');
        $tableau .=qq{<table border="0" >\n};
        $tableau .=qq{<center>\n};
        $tableau .=qq{<tr><td><b>login</b><td>\n<td>};
        $tableau .=$cgi->textfield('login');
        $tableau .=qq{</td></tr>\n};
        $tableau .=qq{</form>\n};
        print $tableau;
	
  	
 }
 }
 

}	

# inersiont table 
  
sub big_brother {
    
    my ($ip,$db,$type,$message)=@_;
    my ($sec,$min,$hr,$jourmr,$mois,$an,$joursem,$jouran,$ete) =localtime;
    if ($an>100) {$an -=100};
    if ($an<10) {$an="0".$an};
    my $timebb;
    $timebb="$an-$mois-$jourmr $hr:$min:$sec";
    my $dbh;
    $dbh=DBI->connect("","","")|| die "erreur connection de Big-Brother";
    my $sql=qq{INSERT INTO TRACE (IP, HEURE , DB, TYPE , MESSAGE ) values ("$ip", "$timebb" , "$db" , "$type" , "$message")};
    $dbh->do($sql);
    $dbh->disconnect;
   
}    
 
 sub doubl_conn {
    	my $ip = &ip;
    	my $login = &login;
    	my $dbh;
    	my $sth;
    	my $boole = 0;
    	my $reqsec = qq{
            SELECT SESSIONLOGIN, SESSIONIP, SESSIONDD, SESSIONDF, SESSIONAUTO
            FROM AUTH.`SESSION`
            WHERE SESSIONLOGIN="$login"
                AND SESSIONDF signe NOW()
                AND SESSIONDD <= NOW()
            };
    	$dbh = DBI->connect($SRV1_DATASRC, $SRV1_LOGIN, $SRV1_PASSWD);
    	my $req1 = $reqsec;
    	my $signe1 = qq{>=};
    	$req1 =~ s/signe/$signe1/;
    	$sth = $dbh->prepare("$req1");
    	$sth->execute();
    	my $nbrow = $sth->rows;
    	if ($nbrow != 0) {
            my $ip2;
            my $df;
            my $num;
            if (my @data = $sth->fetchrow_array()) {
                $ip2 = $data[1];
                $df = $data[3];
                $num = $data[4];
                if ($ip2 eq $ip) {
            		# Prolongement de session
            		my $pro = qq{
                        UPDATE AUTH.`SESSION`
                        SET SESSIONDF=DATE_ADD(NOW(),INTERVAL 330 SECOND)
                        WHERE SESSIONAUTO="$num"
                    };
            		$dbh->do($pro)
                }
        		else {
                    my %ms = (
                        "0" => "Double connexion",
                        "1" => "Vous �tes d�j� connect� avec l'adresse IP $ip2"
                    );
                    my $tb = "";
                    my @script = split(/\//, $ENV{"SCRIPT_NAME"});
                    my $script = $script[@script - 1];
                    if ($script eq "portail_news.cgi") {
                        $tb .= qq{<P align="center">\n};
                        $tb .= qq{<TABLE style="background-color: #EDEDE0;" cellpadding="5">\n};
                        $tb .= qq{<TR>\n};
                        $tb .= qq{<TD align="center">\n};
                        $tb .= qq{<IMG src="$URL_IMG/logo-entrenous.gif" alt="Double connexion">\n};
                        $tb .= qq{<BR><BR>\n};
                        $tb .= qq{<FONT style="font-size: 10pt; color: black;">Vous �tes d�j� connect� avec l'adresse IP $ip2.</FONT>\n};
                        $tb .= qq{</TD>\n};
                        $tb .= qq{</TR>\n};
                        $tb .= qq{</TABLE>\n};
                        $tb .= qq{</P>\n};
                    }
                    elsif ($script ne "travail_frame.cgi" && $script ne "alerte.cgi" && $script ne "portail_news.cgi" && $script ne "identif.cgi") {
                        $tb = qq{Content-type: text/html\n\n};
                        $tb .= qq{<HTML>\n};
                        $tb .= qq{    <HEAD>\n};
                        $tb .= qq{        <TITLE>Double connexion</TITLE>\n};
                        $tb .= qq{        <LINK rel=stylesheet href="$URL_CONF/base.css" type="text/css">\n};
                        $tb .= qq{    </HEAD>\n};
                        $tb .= qq{    <BODY>\n};
                        $tb .= qq{        <P align="center">\n};
                        $tb .= qq{            <IMG src="$URL_IMG/logo-entrenous.gif" alt="Double connexion">\n};
                        $tb .= qq{            <BR><BR>\n};
                        $tb .= qq{            Vous �tes d�j� connect� avec l'adresse IP $ip2.\n};
                        $tb .= qq{        </P>\n};
                        $tb .= qq{    </BODY>\n};
                        $tb .= qq{</HTML>\n};
                    }

#             		my $tb = qq{<CENTER>\n};
#                     $tb .= qq{<IMG src="$URL_IMG/danger.gif" alt="Double connexion" border="0"><BR>\n};
#                     $tb .= qq{Double connexion\n};
#                     $tb .= qq{</CENTER>\n};
            		print $tb;
            		$boole = 1;
            		my $doubl = qq{
                        INSERT INTO AUTH.`ALERTESESSION`
                        (SESSIONAUTO ,ALERTSESSIONIP)
                        VALUES ("$num", "$ip")
                    };
            		$dbh->do($doubl);
        		}
            }
		}
    	else {
            # Pas de session existante
            if ($login ne "") {
            my $insert = qq{
                INSERT INTO AUTH.`SESSION`
                (SESSIONLOGIN, SESSIONIP, SESSIONDD, SESSIONDF)
                VALUES ("$login", "$ip", NOW(), DATE_ADD(NOW(), INTERVAL 330 SECOND))
            };
    	  $dbh->do($insert);
        }
	}
	$sth->finish;
	$dbh->disconnect;
	
	return $boole;
}

sub trace_imp {
	my ($doc,$identif)=@_;
	my $dbh;
    	my $sth;
    	
    	my $ip=&ip;
    	my $login=&login;
    	
    	my $req=qq{select  SESSIONAUTO FROM AUTH.`SESSION` WHERE SESSIONLOGIN="$login" AND SESSIONIP="$ip" AND SESSIONDF >= NOW() AND SESSIONDD <= NOW()};
    	$dbh=DBI->connect($SRV1_DATASRC, $SRV1_LOGIN, $SRV1_PASSWD);
    	$sth=$dbh->prepare("$req");
    	$sth->execute();
	 while (my @data=$sth->fetchrow_array())
		        { 
		        	
		my $insert=qq{INSERT INTO AUTH.`SESSIONIMP` (SESSIONAUTO , SESSIONIMPDOC,SESSIONIMPIDENTIF) values ("$data[0]","$doc","$identif")};       	
		 $dbh->do($insert);       	
	}
	$sth->finish;
	$dbh->disconnect;
}	

# message ecrit en dur

sub message {
 my @param=@_;
 my @data;
 my $i;	
my $ps=$param[0];
my $fichier="message";
$ps=~ tr/A-Z/a-z/;
	
my $fich=$ps."\/";
$fich .=$fichier;
if ($ps ne "fr") 
{
	
$fich .=$ps;	
	}
$fich .="\.cfg";

open(FICH , "$fich") or die "impossible a ouvrir message '$fich'";
  while (<FICH>)
   {
   	chomp;	
  if (($_ =~ /\#/)||($_ eq ""))
	  {
	  	next;
	}
	my @data1 =split(/\|/,$_);
$data[$i]=$data1[1];
$i++;	
   
   
  	
  } 
	
return @data;

}		
