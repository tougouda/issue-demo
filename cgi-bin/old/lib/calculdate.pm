# $Id: calculdate.pm,v 1.1.1.1 2006/08/01 07:48:07 clliot Exp $
# $Log: calculdate.pm,v $
# Revision 1.1.1.1  2006/08/01 07:48:07  clliot
# no message
#
# Revision 2.6  2006/02/06 16:52:38  julien
# V. 1.4 : Tarification v2
#
# Revision 2.5.2.1  2005/12/13 15:46:30  julien
# no message
#
# Revision 2.5.4.1  2005/12/13 09:32:51  julien
# Tarification v2
#
# Revision 2.5  2005/08/26 10:01:38  julien
# Pr�fixage des tables de AUTH dans les requ�tes.
#
# Revision 2.4  2005/03/07 16:21:08  julien
# Passage � l'� pour l'Espagne et le Portugal.
#
# Revision 2.3  2004/11/04 17:38:53  julien
# Correction bug "-0.00" (bis).
#
# Revision 2.2  2004/11/04 17:35:20  julien
# Correction bug "-0.00".
#
# Revision 2.1  2004/11/04 16:09:43  julien
# Correction du bug du signe moins de la fonction &fnombre.
#
# Revision 2.0  2004/09/16 15:07:21  julien
# Version r�cup�r�e de Lancelot et nettoy�e
#
# Revision 1.2  2004/09/16 14:34:46  julien
# Ajout des identifiants
#

package calculdate;

# changements par Hubert : format_monetaire modifi� pour que l'unit� reste coll�e � la valeur (&nbsp;)

require Exporter;
@ISA =qw(Exporter);
@EXPORT =qw(date_cont tauxLocalToEuro formattimestamp rib fornum_contrat converttodate tabledate jour_ferie jour_fer_ag jour_fer_ag_cnx corps_jour num_jour bisextile duree jour_we datetojjmsan pleine_lune converttodatime format_datetime format_monetaire format_monetaire_wu fnombre taux_euro to_euro to_euroar euro_to jours_ouvrables corps_mois);
# correspondance jour et numero de jour; format jj/mm/aa;
use strict;
use varglob;
use DBI;
use Date::Calc qw(Day_of_Week Add_Delta_Days Delta_Days );
use mymath;


sub corps_jour {
	my ($jr, $mois, $annee, $pays) = @_;
	my $date=Day_of_Week($annee,$mois,$jr);
	my @jour;

	if (!$pays || $pays eq "FR" || $pays eq "LU" || $pays eq "MA") {
		@jour=qw:erreur lundi mardi mercredi jeudi vendredi samedi dimanche:;
	} elsif ($pays eq "ES") {
		@jour=qw:error lunes martes mi�rcoles jueves viernes s�bado domingo:;
	} elsif ($pays eq "PT") {
		@jour=qw:erreur segunda-feira ter�a-feira quarta-feira quinta-feira sexta-feira s�bado domingo:;
	} elsif ($pays eq "IT") {
		@jour=qw:erreur luned� marted� mercoled� gioved� venerd� sabato domenica:;
	}

	return $jour[$date];
}

sub corps_mois {
	my ($jr, $mois, $annee, $pays) = @_;
	my @mois;

	if (!$pays || $pays eq "FR" || $pays eq "LU" || $pays eq "MA") {
		@mois = qw{janvier f�vrier mars avril mai juin juillet ao�t septembre octobre novembre d�cembre};
	} elsif ($pays eq "ES") {
		@mois = qw{enero febrero marzo abril mayo junio julio agosto septiembre octubre noviembre diciembre};
	} elsif ($pays eq "PT") {
		@mois = qw{Janeiro Fevereiro Mar�o Abril Maio Junho Julho Agosto Setembro Outubro Novembro Dezembro};
	}

	return $mois[$mois - 1];
}

sub num_jour {
	my($jr,$mois,$annee)=@_;
	my  $date=Day_of_Week($annee,$mois,$jr)||die "jour impossible";

	return $date;
}

# annee bisextile
sub bisextile {
	my @param=@_;
	my $annee=$param[0];
	my $dp=2000;
	my$delta=abs($annee-$dp);
	$delta %= 4;
	return ($delta==0) ? 1:0;
}

sub datetojjmsan {
	my @param=@_;
	my $date=$param[0];
	my ($an,$ms,$jr);
	($an,$ms,$jr)=split(/-/, $date);
   return ($jr,$ms,$an);
}


#duree  depart , arrivee au format jj/mm/aa;



sub duree{
   my ($jrdp,$msdp,$andp,$jrar,$msar,$anar)=@_;
   my $nbjourar=$msar % 2;
   my $nbjourdp=$msdp % 2;
my $dureee;
my $dureedp;
my $dureear;
$dureedp=365*$andp;
$dureear=365*$anar;
my $i;
my $bisexdp=&bisextile($andp);
my $bisexar=&bisextile($anar);
for ($i=1;$i<=12;$i++) { my $mois=$i % 2;
				my  $jrmois;
if ($mois==0) {if ($i==2) {if ($bisexar==0){$jrmois=29} else {$jrmois=28};} else {$jrmois=30};} else {$jrmois=31};
if ($i<$msar) {$dureear +=$jrmois} else {last};
};
$dureear +=$jrar;
my $j;
for ($j=1;$j<=12;$i++) {my $mois=$j % 2;
				my  $jrmois;
if ($mois==0) {if ($j==2) {if ($bisexdp==0){$jrmois=29} else {$jrmois=28};} else {$jrmois=30};} else {$jrmois=31};
if ($j<$msdp) {$dureedp +=$jrmois} else {last};
};
$dureedp +=$jrdp;



$dureee=$dureear-$dureedp;
return $dureee;
}

# jour ouvrable sans Weekend format : duree ,jour ,mois, annee
sub jour_we {
	my $duree=$_[0];
	my $jr=$_[1];
	my $ms=$_[2];
	my $an=$_[3];
	my $jrdep=&num_jour($jr,$ms,$an);
	my $dur=0;
	my $i;
	for($i=1;$i<= $duree;$i++) {
		if (($jrdep==6) || ($jrdep==7)) {
			$dur++;
		}
		$jrdep++;
		if ($jrdep==8) {
			$jrdep=1
		};
	};
	return $dur;
}

sub pleine_lune {
	my ($jr,$ms,$an)=@_;
	 my $jrdp=21;
	 my $msdp=01;
	 my $andp=2000;
	my $jrcurs=$jrdp;
	my $mscurs=$msdp;
	my $ancurs=$andp;
	my ($jrpred,$mspred,$anpred);
	my $i=1;
	my $diff;
	$diff=Delta_Days($ancurs,$mscurs,$jrcurs,$an,$ms,$jr);

while ($diff>0)
 { ($anpred,$mspred,$jrpred)=($ancurs,$mscurs,$jrcurs);
 	my $j=$i%2;

 	my  $jour = ($j==0) ? 30 : 29;
 	$diff=Delta_Days($ancurs,$mscurs,$jrcurs,$an,$ms,$jr);

 	($ancurs,$mscurs,$jrcurs)=Add_Delta_Days($ancurs,$mscurs,$jrcurs,$jour);
 	$diff=Delta_Days($ancurs,$mscurs,$jrcurs,$an,$ms,$jr);
 	$i++;

   }

 return  ($jrpred,$mspred,$anpred);
}




# formatage date de datetime

sub format_datetime {
  my @datetime=@_;
  my $d=$datetime[0];
  my $annee=substr($d , 0 , 4);
  my $mois=substr($d , 5 ,2);
  my $jour=substr($d , 8,2);
  my $date=join ('/' , $jour,$mois,$annee);
  return $date;
}

sub tabledate {

  my @datetime=@_;
  my $d=$datetime[0];
  my $annee=substr($d , 0 , 4);
  my $mois=substr($d , 5 ,2);
  my $jour=substr($d , 8,2);
  return  $jour,$mois,$annee;

 }

sub converttodate {
	my ($jour,$mois,$annee)=@_;
	my $date;
	$date=join ':' , $annee , $mois,$jour;
	return $date;


}

sub converttodatime {
	my ($jour,$mois,$annee)=split $_;
	my $datetime;
	my $hr="00:00:00";
	$datetime=join ':' , $annee , $mois,$annee;
	my $date;
	$date=join ' ' ,$datetime,$hr;

  return $date;
}

sub format_monetaire1 {
	my ($money,$unit)=@_;
	my $arondi=&arrondi($money, 2);

	#$arondi .=" ".$unit;
	my $entier=int($money);
	$arondi -=$entier;
	$arondi=&arrondi($arondi, 2);
	$arondi=substr($arondi,2,2);
	my $result;
	my $reste;
	my $i=0;
	my @divende;
	my $diviseur;
	my $j;

	while ( $entier>=1000)
	 {$divende[$i]=$entier % 1000;
	  print "$divende[$i] $entier $i\n";
	  my $div;
	  $div=$entier/1000;
	  $entier=int($div);
	  $i++;
	}

	$result=$entier;
	if ($i!=0)
	{
	 for($j=$i;$j>=0;$j--)
	   { $result .= " ".$divende[$j]};
	}

	 $result .="\.".$arondi;
#	 $result .=" ".$unit;

	return $result;

}

# Formattage des nombres (ex. : 1 245)
sub fnombre {
  my ($nb, $sep, $dec)=@_;
  my ($long);
  my $neg = ($nb <= 0);

  $nb .= ".";
# ----- suppression des caracteres <> chiffres et '.'
  $nb=~tr/(0-9.)//cd;
# ----- longueur doit etre un multiple de 3
  $long=length($nb);
##  $long=$long+(3-$long%3);
  $long=$long+(6-$long%3);
  $nb =sprintf("%$long.2f", &arrondi($nb, 2));
# -----  remplace les espaces par des 'X'
  $nb=~s/ /X/g;
  if ($sep) {
# ----- un espace tous les 3 caracteres
    $nb=~s/(\w{3})/$1 /g;
# ----- suppression de l'espace devant le point decimal
    $nb=~s/ \././;
  }
# ----- suppression des 'non chiffres' de debut
  $nb=~s/( ?X ?)*//g;
# ----- suppression des decimales si = '.00'
  if (! $dec) {
    $nb=~s/\.00//;
  }
  if ($neg && $nb != 0) {
  	$nb = "-".$nb;
}

  return ($nb);
}

sub format_monetaire {
	my ($money,$unit)=@_;
       $money=&fnombre($money,1,1);

#       $unit="�" if ($unit eq "FF");
       #$unit="�";

       $money .=" ".$unit;
###       $money .="&nbsp;".$unit;
	$money =~ s/\./,/g;
###	$money =~ s/\s/&nbsp;/g;

       return $money;
}

sub format_monetaire_wu
{
	my ($money)=@_;
    $money=&fnombre($money,1,1);
	$money =~ s/\./,/g;

    return $money;
}

# taux de change zone Euro

sub taux_euro {
	my @moneylist=@_;
	my $money=$moneylist[0];
	my $taux;
	my $BD;
	$BD = DBI->connect($SRV1_DATASRC, $SRV1_LOGIN, $SRV1_PASSWD);
	my $val = 0;
	if($money eq "DH" )
	{
		my $requete;
		my $sth = $BD->prepare(qq|SELECT P.TXCONVTOEUR FROM AUTH.PAYS P WHERE P.PACODE='MA';|);
		$sth->execute();
		my @data = $sth->fetchrow_array();
		$val = $data[0];		
	}
	if ($money eq "DM") {$taux=1/1.955583}
	elsif ($money eq "ATS") {$taux=1/13.7603}
	elsif ($money eq "FB") {$taux=1/40.3399}
	elsif ($money eq "PTS") {$taux=1/166.386}
	elsif ($money eq "FIM") {$taux=1/5.94573}
	elsif ($money eq "FF") {$taux=1/6.55957}
	elsif ($money eq "�IR") {$taux=1/0.787564}
	elsif ($money eq "LIT") {$taux=1/1936.27}
	elsif ($money eq "FLUX") {$taux=1/40.3399}
	elsif ($money eq "FB") {$taux=1/2.20371}
	elsif ($money eq "ESC") {$taux=1/200.482}
	elsif ($money eq "EUR" || $money eq "�") {$taux=1}
	elsif ($money eq "DH") {$taux=1/$val};
	return $taux;
}

#R�cup�ration du taux Local [CodePays] - [Date]
sub tauxLocalToEuro
{
	my @list=@_;
	my $pays=$list[0];
	my $BD;
	$BD = DBI->connect($SRV1_DATASRC, $SRV1_LOGIN, $SRV1_PASSWD);
	my $val = 0;

if (defined($list[1]))
{
	my $sth = $BD->prepare(qq|SELECT crr_value FROM frmwrk.p_country INNER JOIN frmwrk.h_currencyrate ON crr_cur_id = cty_cur_id WHERE cty_id = '$pays' AND crr_date_begin <= '$list[1]' ORDER BY crr_date_begin DESC LIMIT 1;|);
	$sth->execute();
	my @data = $sth->fetchrow_array();
    $val = $data[0];
    if($val eq "")
    {
        my $sth = $BD->prepare(qq|SELECT P.TXCONVTOEUR FROM AUTH.PAYS P WHERE P.PACODE='$pays';|);
        $sth->execute();
        my @data = $sth->fetchrow_array();
        $val = $data[0];
    }
}
else
{
	my $sth = $BD->prepare(qq|SELECT P.TXCONVTOEUR FROM AUTH.PAYS P WHERE P.PACODE='$pays';|);
	$sth->execute();
	my @data = $sth->fetchrow_array();
    $val = $data[0];
}		
	return $val;
}

# conversion vers Euro non arrondi
# au 01/01/2002 $taux=1.0;

sub to_euro {
	my ($mt,$moneys)=@_;
	my $taux;
	my $montant;

	$taux=&taux_euro($moneys);

	$montant =$mt/$taux;

	return $montant;

}

# conversion en Euro arrondi

sub to_euroar {
	 my ($mt,$money)=@_;
	 my $euro;
	 my $euroar;
	 $euro=&to_euro($mt,$money);
	 $euroar=&arrondi($euro, 2);

	 return $euroar;
}

# conversion en monais locale ;


sub euro_to {
	my ($euro,$money)=@_;
	my $montant;
	my $taux;
	$taux=&taux_euro($money);
	$montant=$euro*$taux;
	return $montant;
}

sub jour_ferie {
	my ($pays,$jrdp,$msdp,$andp,$duree)=@_;
	my $dbh;
	my $sth;
	my $requete;
	$duree=int($duree);
	my $jourenmoins;
	my $datedp = sprintf('%04d-%02d-%02d', $andp, $msdp, $jrdp);
	$requete=qq{select JOJOURS FROM AUTH.`PAJO` WHERE PACODE="$pays" AND PAJO.JOJOURS >= "$datedp" AND PAJO.JOJOURS <= DATE_ADD("$datedp",INTERVAL $duree DAY)};
	$dbh=DBI->connect($SRV1_DATASRC, $SRV1_LOGIN, $SRV1_PASSWD)||die "erreur de connection", $dbh->errstr;
	$sth=$dbh->prepare("$requete");
	$sth->execute();
	$jourenmoins=$sth->rows;
	my $feriewe=0;

	while (my @data=$sth->fetchrow_array())  {
		my ($jr,$ms,$an)=&tabledate($data[0]);
		my $test=&num_jour($jr,$ms,$an);

		if (($test ==6) ||($test ==7))  {$feriewe += 1};
       	}

	$sth->finish;
	$dbh->disconnect;
	$jourenmoins -= $feriewe;

	return $jourenmoins;
}

sub jour_fer_ag {
	my ($pays, $agauto, $jrdp, $msdp, $andp, $duree) = @_;
	my $BD;
	$BD = DBI->connect($SRV1_DATASRC, $SRV1_LOGIN, $SRV1_PASSWD) || die "erreur de connection", $BD->errstr;
	my $requete;
	$duree = int($duree);
	my $jourenmoins;
	my $datedp = sprintf('%04d-%02d-%02d', $andp, $msdp, $jrdp);
	my $sth = $BD->prepare(qq|select JOJOURS FROM AUTH.`PAJOAG` where PACODE="$pays" and AGAUTO="$agauto" and PAJOAG.JOJOURS >= "$datedp" and PAJOAG.JOJOURS <= DATE_ADD("$datedp", INTERVAL $duree DAY)|);
	$sth->execute();
	$jourenmoins = $sth->rows;
	my $feriewe = 0;

	while (my @data = $sth->fetchrow_array()) {
       		my ($jr, $ms, $an) = &tabledate($data[0]);
		my $test = &num_jour($jr, $ms, $an);

		if (($test == 6) || ($test == 7)) {$feriewe += 1;}
       	}

	$sth->finish;
	$jourenmoins -= $feriewe;
	$BD->disconnect;

	return  $jourenmoins;
}

sub jour_fer_ag_cnx {
	my ($pays, $agauto, $jrdp, $msdp, $andp, $duree, $BD) = @_;
	my $requete;
	$duree = int($duree);
	my $jourenmoins;
	my $datedp = sprintf('%04d-%02d-%02d', $andp, $msdp, $jrdp);
	my $sth = $BD->prepare(qq|select JOJOURS FROM AUTH.`PAJOAG` where PACODE="$pays" and AGAUTO="$agauto" and PAJOAG.JOJOURS >= "$datedp" and PAJOAG.JOJOURS <= DATE_ADD("$datedp", INTERVAL $duree DAY)|);
	$sth->execute();
	$jourenmoins = $sth->rows;
	my $feriewe = 0;

	while (my @data = $sth->fetchrow_array()) {
       		my ($jr, $ms, $an) = &tabledate($data[0]);
		my $test = &num_jour($jr, $ms, $an);

		if (($test == 6) || ($test == 7)) {$feriewe += 1;}
       	}

	$sth->finish;
	$jourenmoins -= $feriewe;
	
	return $jourenmoins;
}

sub fornum_contrat
{
	my ($num, $div) = @_;

	my ($sec, $min, $hr, $jour, $mois, $an, $joursm, $jouran, $ete) = localtime;
	$mois++;
	$an -= 100;

    return sprintf("%02d%02d%06d", $an, $mois, $num);
}



# abcdefghijklmnopqrstuvwxyz
# 12345678912345678923456789
# 1 aj
# 2 bks
# 3 clt
# 4 dmu
# 5 env
# 6 fow
# 7 gpx
# 8 hqy
# 9 irz

sub rib{
 my @dt;
 my $cle;
 my $i;
  ($dt[0],$dt[1],$dt[2],$cle)=@_;
 for($i=0;$i<3;$i++)
 {
 $dt[$i] =~ tr/A-Z/a-z/;

 if ($dt[$i] =~ /[aj]/)
  {
  	$dt[$i] =~ s/[aj]/1/eg;
  	}
 if ($dt[$i] =~ /[bks]/)
  {
  	$dt[$i] =~ s/[bks]/2/eg;
}
if ($dt[$i] =~ /[clt]/)
  {
  	$dt[$i] =~ s/[clt]/3/eg;
  	}
 if ($dt[$i] =~ /[dmu]/)
  {
  	$dt[$i] =~ s/[dmu]/4/eg;
}
if ($dt[$i] =~ /[env]/)
  {
  	$dt[$i] =~ s/[env]/5/eg;
}
if ($dt[$i] =~ /[fow]/)
  {
  	$dt[$i] =~ s/[fow]/6/eg;
  	}
 if ($dt[$i] =~ /[gpx]/)
  {
  	$dt[$i] =~ s/[gpx]/7/eg;
}
if ($dt[$i] =~ /[hqy]/)
  {
  	$dt[$i] =~ s/[hqy]/8/eg;
  	}
 if ($dt[$i] =~ /[irz]/)
  {
  	$dt[$i] =~ s/[irz]/9/eg;
}


}
my $reste=$dt[0];

$reste=$reste % 97;

$reste .=$dt[1];

$reste=$reste % 97;


my ($un,$dx);

$un =substr($dt[2],0,6);
$reste .=$un;

$reste=$reste % 97;

$dx =substr($dt[2],6,5);
$reste .=$dx;

$reste=$reste % 97;


$reste .=$cle;
$reste=$reste % 97;

return ($reste==0) ? 1:0;

 }

sub formattimestamp {
 my @data=@_;
  my $timestamp=$data[0];
  my $an=substr($timestamp,0,4);
  my $mois=substr($timestamp,4,2);
   my $jr=substr($timestamp,6,2);
    my $hr=substr($timestamp,8,2);
  my $min=substr($timestamp,10,2);
  my $sc=substr($timestamp,12,2);
my $date=qq{$jr-$mois-$an $hr:$min:$sc};
return $date;
}

#
#  date controle
#
sub date_cont {
	my ($jrdp , $msdp , $andp)=@_;
	my $position=0;
	if (($jrdp eq "00")&&($msdp eq "00")&&($andp eq "0000"))
	 {
	 	return $position;
	 	}
	#innitialsation periode
	my @period;
	$period[0]=0;
	$period[1]=7;
	$period[2]=15;
	$period[3]=60;
	my ($sec,$min,$hr,$jour,$mois,$an,$joursm,$jouran,$ete)=localtime;
	$an +=1900;
	$mois +=1;

	my $delta2 = Delta_Days(1900, 1, 1, $an, $mois, $jour);
	my $delta1 = Delta_Days(1900, 1, 1, $andp, $msdp, $jrdp);

	my $delta = ($delta1-$delta2) + 1;

	for(my $i=0;$i<@period;$i++)
	{my $sth;
	 my $d=$delta;
	 $d -=$period[$i];

	if ($d <=0) {$position =4-$i;
			last;};

	}

    return $position;
}

# Calcule le nombre de jours ouvrables entre les 2 dates saisies
# Param�tres :
# 0 : Date de d�but au format yyyy:mm:jj
# 1 : Date de fin au format yyyy:mm:jj
# 2 : Pays
# 3 : Agence
# 4 : Base de donn�es
sub jours_ouvrables {
	my ($debutms, $finms, $pays, $agence, $db) = @_;
	my ($andm, $msdm, $jrdm) = split(/:/, $debutms);
	my $dm = Delta_Days(1900, 1, 1, $andm, $msdm, $jrdm);
	my ($anfm, $msfm, $jrfm) = split(/:/, $finms);
	my $fm = Delta_Days(1900, 1, 1, $anfm, $msfm, $jrfm);
	my $jourmsouv = Delta_Days($andm, $msdm, $jrdm, $anfm, $msfm, $jrfm);
	my $totj = $jourmsouv + 1;
	my $uvfer = &jour_fer_ag_cnx($pays, $agence, $jrdm, $msdm, $andm, $jourmsouv, $db);
	$jourmsouv += 1;
	my $ouvrwe = &jour_we($jourmsouv, $jrdm, $msdm, $andm);
	$jourmsouv = $jourmsouv - $ouvrwe - $uvfer;
	return $jourmsouv;
}
