package stock;
require Exporter;
require SelfLoader;
@ISA = qw(Exporter SelfLoader);
@EXPORT = qw(stock);

use strict;
use accesbd;
use varglob;
use CGI;
use autentif;
#use lectdon;
use calculdate;
#use securite; 
use titanic;
use Date::Calc qw(Day_of_Week  Delta_Days Add_Delta_Days);
use message;


#
# affichage des stocks
#
sub stock {
	my $req = @_[0];
	my $champs_id = @_[1];
	my $nom_cgi = @_[2]; 
	my $pays = @_[3];
	my $cgi= new CGI;
	$cgi->autoEscape(undef);
	my @mess=&message($pays);
	my $DB=&Environnement("LOCATION");
	my $nomfh=$cgi->param('type');
	my $fich=$cgi->param('menu');

	my @stockopt;
	my @stocklib;
	$stockopt[5]="abs";
	$stockopt[6]="abs";
	$stockopt[7]="abs";
	$stockopt[8]="abs";
	$stockopt[9]="abs";
	$stockopt[10]="abs";
	$stockopt[11]="abs";
	$stockopt[12]="abs";
	$stockopt[13]="abs";
	$stockopt[14]="abs";
	$stockopt[15]="abs";
	for(my $i=0;$i<16;$i++) {
		$stocklib[$i]=$mess[$i+2700];
	}

	my $tableau="";
	my $color;
	print qq{<table border="0">\n};
	my $tpform="";
	my $entete="";
	$entete .=qq{<tr bgcolor="#C6CDC5" align="center">\n};
	for(my $j=0;$j<16;$j++) {
		if ($j>11) {$color="green"}
		elsif ($j>7) {$color="red"}
		else {$color="black"};
		$entete .=qq{<td><font color="$color"><b>$stocklib[$j]</b></font></td>\n};
	}
	$entete .=qq{</tr>};
	my $ident="";
	if (not($cgi->param($champs_id."rech") eq "")) {
		$ident=$cgi->param($champs_id."rech");
	}
	$req =~ s/ident/$ident/eg;
	my $sth = $DB->prepare("$req");
	$sth->execute();
	my @data;
	my @aff;
	my @valsel;
	my $cp=0; 
	my $nbdonne;
	my $ordre=0;
	$cgi->delete("stock.x");
	$cgi->delete("stock.y");
	my @names = $cgi->param;
	my $nb_names = @names;
	my $l;
	
# 	FA 8724 : Ajout d'un bouton page pr�c�dente (uniquement dans le cas de la saisie du pr�co)
    if ($nom_cgi eq "saisie.cgi")
    {
		$tpform = $cgi->start_form(-method=>'get', action=>"$nom_cgi");
		$tpform .= qq{<tr><td colspan="17" align="right">};
		for ($l = 0 ; $l < $nb_names ; $l++) 
        {
			$cgi->param("$names[$l]");
			$cgi->param("$names[$l]rech");
			$cgi->param("$names[$l]jr");
			$cgi->param("$names[$l]ms");
			$cgi->param("$names[$l]an");
			$tpform .=$cgi->hidden("$names[$l]");
			$tpform .=$cgi->hidden("$names[$l]rech");
			$tpform .=$cgi->hidden("$names[$l]jr");
			$tpform .=$cgi->hidden("$names[$l]ms");
			$tpform .=$cgi->hidden("$names[$l]an");
		}
		
		$tpform .=$cgi->image_button(-name=>'Previous', -alt=>$mess[108], -border=>0, -src=>"$URL_IMG/precedent.gif");
		$tpform .=$cgi->hidden('menu', $fich);
		$tpform .=$cgi->hidden('type', $nomfh);
		$tpform .=qq{</td></tr></form>\n};
		print $tpform;
	}
	
	$tpform = "";
	
    while (@data=$sth->fetchrow_array()) {
		my $c=$ordre % 2;
		my $color="";
		$color=qq{''};
		if ($c == 1) { $color=qq{'#D6D6D6'};}
		my $euclid= $ordre % 20;
		if ($euclid == 0) {$tpform .=$entete;}
		$tpform .=qq{<tr bgColor=$color onmouseover="bgColor='#FFCC33'" onmouseout="bgColor=$color">};
		if ($nom_cgi ne "devis.cgi"){
			$tpform .=$cgi->start_form(-method=>'get', action=>"$nom_cgi");
		}
		for(my $k=0;$k<@data;$k++)
			{if ($stockopt[$k] eq "abs") {$data[$k]=abs($data[$k])};
			if (($k ==5)||($k == 6)||($k == 7)) {
				$tpform .=qq{<td align="center"><font><b>$data[$k]</b></font></td>\n};
			}
			else {
				$tpform .=qq{<td align="center"><font class="PT">$data[$k]</font></td>\n};
			}
			$tpform .=$cgi->hidden(-name=>"$stocklib[$k]", -value=>$data[$k]);
		}
		$tpform .=qq{<td><font class="PT">};
		for (my $l=0;$l<$nb_names;$l++) {
			if ($nom_cgi ne "devis.cgi"){
				if ($names[$l] eq "reserve") {
					$cgi->delete("$names[$l]");
					$tpform .=$cgi->hidden(-name=>"$names[$l]", -value=>'on');
					$tpform .=$cgi->hidden(-name=>"designation", -value=>'');
				}
				elsif ($names[$l]."rech" eq "tpmachinesrech") {
					$cgi->delete("$names[$l]rech");
					$tpform .=$cgi->hidden(-name=>"$names[$l]rech", -value=>$data[0]);
				}
				else {
					$cgi->param("$names[$l]");
					$cgi->param("$names[$l]rech");
					$cgi->param("$names[$l]jr");
					$cgi->param("$names[$l]ms");
					$cgi->param("$names[$l]an");
					$tpform .=$cgi->hidden("$names[$l]");
					$tpform .=$cgi->hidden("$names[$l]rech");
					$tpform .=$cgi->hidden("$names[$l]jr");
					$tpform .=$cgi->hidden("$names[$l]ms");
					$tpform .=$cgi->hidden("$names[$l]an");
				}
			}
		}
		
		$tpform .=$cgi->image_button(-name=>'Reserver', -alt=>$mess[200], -border=>0, -src=>"$URL_IMG/bouton.gif");
		$tpform .=$cgi->hidden('menu', $fich);
		$tpform .=$cgi->hidden('type', $nomfh);
		if ($nom_cgi ne "devis.cgi"){
			$tpform .=qq{</font></td></tr></form>\n};
		}
		else{
			$tpform .=qq{</font></td></tr>\n};		
		}
		$ordre++;
		print $tpform;
		$tpform="";
	}
	$sth->finish;
	print $tpform;
}

1;
