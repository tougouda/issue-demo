# $Id: clemail.pm,v 2.5 2006/02/06 16:52:38 julien Exp $
# $Log: clemail.pm,v $
# Revision 2.5  2006/02/06 16:52:38  julien
# V. 1.4 : Tarification v2
#
# Revision 2.4.2.1  2006/01/09 08:22:17  julien
# Utilisation de la variable globale $CH_BASE pour le r�pertoire des traces mail.
#
# Revision 2.4  2005/09/22 16:27:04  julien
# Gestion correcte du codage de caract�res.
#
# Revision 2.3  2005/08/26 10:01:38  julien
# Pr�fixage des tables de AUTH dans les requ�tes.
#
# Revision 2.1  2004/11/18 12:59:25  julien
# Tra�age de l'envoi des mails.
#
# Revision 2.0  2004/09/16 15:07:21  julien
# Version r�cup�r�e de Lancelot et nettoy�e
#
# Revision 1.2  2004/09/16 14:34:46  julien
# Ajout des identifiants
#

package clemail;
require         Exporter;
require         SelfLoader;
@ISA                 = qw(Exporter SelfLoader);

@EXPORT         = qw(
                     env_email envoie_email trace_mail
                    );

use Mail::Sender;
use File::Path;
use varglob;

# sub env-email :envoie de mail


#emploi &envoi-email($smtp,$orgasm,$destin,$sbj,$email,$file,$enc)
# ou $stmp=serveur smtp
# ou $orgasm=email origine
# ou $destin=eamail destinataire
# ou  $sbj=sujet du mail
# ou $email=corps du mail
#option $file=fichier a charger
#          possibilit� de passer le nom du fichier sous forme de tableau (fichier,nom)


sub env_email {
    my @param  = @_;
    my $taille = @param;
    my ($smtp, $orgasm, $destin, $sbj, $type, $email, $file);
    if ($taille == 6) {
        ($smtp, $orgasm, $destin, $sbj, $type, $email) = @param;
    }
    if ($taille >= 7) {
        $smtp   = shift @param;
        $orgasm = shift @param;
        $destin = shift @param;
        $sbj    = shift @param;
        $type   = shift @param;
        $email  = shift @param;
    }

    my $sender = new Mail::Sender {
        from => $orgasm,
        smtp => $smtp
    };

    $sender->OpenMultipart( {
        to       => $destin, 
        subject  => $sbj
    } );

    # Corps HTML
    if ($type eq "HTML") {
        $sender->Body('iso-8859-15', 'base64', 'text/html');
	}
	# Corps Texte
    else {
    	$sender->Body('iso-8859-15', 'base64', 'text/plain');
    }
    
    $sender->SendEnc($email);
    
    if ($taille > 6) {
        for (my $i=0; $i<@param; $i++) {
            if (ref($param[$i]) eq 'ARRAY')
            {
            	$sender->SendFile({file => $param[$i][0],
                                   disposition => 'attachment; filename="' . $param[$i][1] . '"'});
            }
            else
            {
            	$sender->SendFile({file => $param[$i]});       
            }
        }
    }
    
    $sender->Close();

	if ($taille == 6) {
		&trace_mail($orgasm, $destin, $sbj, "");
	}
	if ($taille >= 7) {
		&trace_mail($orgasm, $destin, $sbj, $param[0]);
	}
}

sub envoie_email {
    my @param  = @_;
    my $taille = @param;
    my ($smtp, $orgasm, $destin, $sbj, $type, $email, $file);

    if ($taille == 6) {
        ($smtp, $orgasm, $destin, $sbj, $type, $email) = @param;
    }
    if ($taille >= 7) {
        ($smtp, $orgasm, $destin, $sbj, $type, $email, $file) = @param;
    }

	my $sender = new Mail::Sender {from => $orgasm, smtp => $smtp};
	$sender->OpenMultipart( {
	    to      => $destin, 
	    subject => $sbj
	} );

	if ($type eq "HTML") {
	    $sender->Body('iso-8859-15', 'base64', 'text/html');
	}
	else {
	    $sender->Body('iso-8859-15', 'base64', 'text/plain');
	}

	$sender->SendEnc($email);

	if ($taille > 6) {
		for (my $i=6;$i<@param;$i++) {
			$sender->SendFile( {
			    file => $param[$i]
			} );
		}
	}

	$sender->Close();
	if ($taille == 6) {
		&trace_mail($orgasm, $destin, $sbj, "");
	}
	if ($taille >= 7) {
		&trace_mail($orgasm, $destin, $sbj, $file);
	}
}

# Trace de l'envoi des e-mails
# Param�tres :
# - adresse de l'exp�diteur
# - adresse du destinataire
# - sujet du message
# - nom du fichier joint
sub trace_mail {
	my ($exped, $dest, $subj, $fich) = @_;
	my ($sec, $min, $hr, $jourmr, $mois, $an, $joursem, $jouran, $ete)
		= localtime;
	my @mois_lib = ("Jan", "Fev", "Mar", "Avr", "Mai", "Jun", "Jui", "Aou", 
		"Sep", "Oct", "Nov", "Dec");
	$an += 1900;
	$jourmr = "0".$jourmr if ($jourmr < 10);
	$hr = "0".$hr if ($hr < 10);
	$min = "0".$min if ($min < 10);
	$sec = "0".$sec if ($sec < 10);
	my $date = "$jourmr $mois_lib[$mois] $an";
	my $heure = "$hr:$min:$sec";
	my $nomfich = "TraceMail_".$mois_lib[$mois]."-".$an.".txt";
	my @dest = split(/\,/, $dest);
	my $rep = $PATH_COMMONFILES_LOC_LOGS;
#	my $rep = "../trace";
    &File::Path::make_path($rep);
	
#	chdir("$rep")
#		or die ("Impossible d'ouvrir le r�pertoire $rep");
	
	open (TRACE, ">>$rep/$nomfich")
		or die "Impossible d'�crire dans le fichier $rep/$nomfich";
	
	foreach (@dest) {
		$exped   = substr($exped."                              ", 0, 30);
		$_       = substr($_."                                        ", 0, 40);
		$subj    = substr($subj."                                        ", 0, 40);
		$fichier = substr($fichier."               ", 0, 15);
		print TRACE qq{$date $heure   $exped   $_   $subj   $fichier\r\n};
	}
	
	close TRACE;
}


1;
