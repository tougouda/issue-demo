package mymath;
require Exporter;
@ISA =qw(Exporter);
@EXPORT =qw(tricr tridecr max min somme moyenne variance arrondi);
use strict;  



sub tricr {
	return sort { $a <=> $b } @_;
}

sub tridecr {
	return reverse sort { $a <=> $b } @_;
}

sub max {
	my @tableau = @_;
	my @tri = &tridecr(@tableau);
	return shift(@tri);
}

sub min {
	my @tableau = @_;
	my @tri = &tricr(@tableau);
	return shift(@tri);
}

sub somme {
	my @t = @_;
	my $taille = @t;
	my $tmp = 0;
	my $i;
	
    for ($i=0; $i<$taille; $i++) {
        $tmp += $t[$i];
    }
  return $tmp;
}

sub moyenne {
	my @t = @_;
	my $taille = @t;
	my $moyenne = 0;
	if ($taille == 0) {
		return  $moyenne;
	}
	$moyenne = &somme(@t);
	$moyenne /= $taille;
                 
    return $moyenne;
}

sub variance {
    my @t = @_;
    my $taille = @t;
    my $moyenne = &moyenne(@t);
    my $var = 0;
    my $i;
    if ($taille == 0) {
        return $var;
    }
    for ($i=0; $i<$taille; $i++) {
        $var += ($t[$i] - $moyenne) * ($t[$i] - $moyenne);
    }

    $var /= $taille;
    $var = sqrt $var;

    return $var;
}

# Fonction d'arrondi
# Param�tres :
#   - nombre � arrondir
#   - nombre de d�cimales
sub arrondi {
    my $n = $_[0];
    my $precision = $_[1];
    my $neg = 1;
    $neg = -1   if ($n < 0);
    
    my $result = int((10**$precision) * $neg * $n + 0.5) / (10**$precision);
    my $format = "%.".$precision."f";
    
    return sprintf($format, $neg * $result);
}
