# $Id: accesbd.pm,v 1.1.1.1 2006/08/01 07:48:07 clliot Exp $
# $Log: accesbd.pm,v $
# Revision 1.1.1.1  2006/08/01 07:48:07  clliot
# no message
#
# Revision 2.5  2006/02/06 16:52:38  julien
# V. 1.4 : Tarification v2
#
# Revision 2.4.2.3  2006/01/19 08:08:56  julien
# no message
#
# Revision 2.4.2.2  2006/01/16 08:17:52  julien
# Gestion des r�sidus � 6 .
#
# Revision 2.4.2.1  2005/12/15 12:58:37  julien
# Log des erreurs MySql.
#
# Revision 2.4  2005/08/25 19:24:48  julien
# Nouveaux acces � la base.
#
# Revision 2.3  2005/07/12 15:39:54  julien
# Facturation des services et du nettoyage version lourde.
#
# Revision 2.2  2005/05/03 15:39:29  julien
# Tra�age du temps d'ex�cution des scripts.
#
# Revision 2.1  2005/01/04 16:20:05  julien
# Taux d'assurance � 7% pour l'Espagne.
#
# Revision 2.0  2004/09/16 15:07:20  julien
# Version r�cup�r�e de Lancelot et nettoy�e
#
# Revision 1.2  2004/09/16 14:34:46  julien
# Ajout des identifiants
#

package accesbd;

require Exporter;
@ISA 		= qw( Exporter );
@EXPORT 	= qw(
	%CONNECTION $hl_color $DROIT_ACCES $G_AGENCE $G_PEAUTO $G_PENOM $G_PEPRENOM
    $G_PEMAIL $G_PROFIL $G_IMPRIM $G_PAYS $CONNECTION $LOGIN $PASSWD $G_LST_IMP
    $G_TVA $G_MONNAIE $G_CARBURANT $G_AGLIBELLE authentification ListePaysCnx
    InitConnexion imprimante Environnement verif_droit $G_MOD $G_KM $G_TX_ASS
    $G_COEFF_COUT $G_PELOGIN $G_PEPASSWORD $G_RESIDUS $G_AGTEL
);

use DBI;
use varglob;
use securite;
use vars qw(
	%CONNECTION $DROIT_ACCES $G_AGENCE $G_PEAUTO $G_PENOM $G_PEPRENOM $G_PEMAIL
    $G_PROFIL $G_IMPRIM $G_PAYS $CONNECTION $LOGIN $PASSWD $G_LST_IMP $G_TVA
    $G_MONNAIE $G_CARBURANT $G_AGLIBELLE $hl_color $G_MOD $G_KM $G_TX_ASS
    $G_COEFF_COUT $G_PELOGIN $G_PEPASSWORD $G_RESIDUS $G_AGTEL
);

#
# InitConnexion
# param(s) :
# 1er  : obligatoire (La base de connection)
# 2eme : optionnel (Pacode ex: 'FR' si vide = Pays lu dans profil/PAPE)
# retourne une liste contennant les param de connection
#

sub InitConnexion {
  my ($base) = $_[0];
  my ($g_pays) = $_[1] ? $_[1] : $G_PAYS;

  $x[0] = $CONNECTION{$base}{"$g_pays"}{"datsrc"};
  $x[1] = $CONNECTION{$base}{"$g_pays"}{"login"};
  $x[2] = $CONNECTION{$base}{"$g_pays"}{"passwd"};

  return @x;
}

#
# authentification
# param(s) :
# 1 seul optionnel (Le login du personnel)
# renseigne les variables globales utilis�es dans authentification
#

sub authentification {
	$BD_AUTH = DBI->connect($CONNECTION{"LOCATION"}{"FR"}{"datsrc"},$CONNECTION{"LOCATION"}{"FR"}{"login"},$CONNECTION{"LOCATION"}{"FR"}{"passwd"});
	$logi=$_[0]?$_[0]:&login;

	$temp = $BD_AUTH->prepare(qq|SELECT PERSONNEL.AGAUTO, PERSONNEL.PEAUTO, PERSONNEL.PENOM, PERSONNEL.PEPRENOM, PERSONNEL.PEMAIL, PERSONNEL.PROFILAUTO, PERSONNEL.IMPRIMAUTO, PAPE.PACODE, PAYS.PATVA, PAYS.PAMONNAIE, PAYS.PACARBURANT, AGLIBELLE, PAYS.PAMO , PAYS.PAKM, PAYS.PAASSURANCE, PERSONNEL.PELOGIN,PERSONNEL.PEPASSWORD, AGENCE.AGTEL FROM AUTH.`PERSONNEL` left join AUTH.`PAPE` on PERSONNEL.PEAUTO=PAPE.PEAUTO left join AUTH.`PAYS` on PAYS.PACODE=PAPE.PACODE left join AUTH.`AGENCE` on AGENCE.AGAUTO=PERSONNEL.AGAUTO WHERE PELOGIN = "$logi"|);
	$temp->execute();
	($G_AGENCE,$G_PEAUTO,$G_PENOM,$G_PEPRENOM,$G_PEMAIL,$G_PROFIL,$G_IMPRIM,$G_PAYS,$G_TVA,$G_MONNAIE,$G_CARBURANT,$G_AGLIBELLE,$G_MOD,$G_KM,$G_TX_ASS,$G_PELOGIN,$G_PEPASSWORD, $G_AGTEL) = $temp->fetchrow_array();
	if ($G_PAYS eq "FR") {
		$G_RESIDUS = "0";
		#$G_TX_ASS = "0.10";
		$G_COEFF_COUT = "3";
	} elsif ($G_PAYS eq "ES") {
		$G_RESIDUS = "10";
		#$G_TX_ASS = "0.09";
		$G_COEFF_COUT = "3";
	} elsif ($G_PAYS eq "MA") {
		$G_RESIDUS = "0";
		#$G_TX_ASS = "0.09";
		$G_COEFF_COUT = "3";
	} elsif ($G_PAYS eq "IT") {
		$G_RESIDUS = "0";
		#$G_TX_ASS = "0.09";
		$G_COEFF_COUT = "3";
	}elsif ($G_PAYS eq "PT") {
		$G_RESIDUS = "0";
		#$G_TX_ASS = "0.05";
		$G_COEFF_COUT = "2";
	} else {
		$G_RESIDUS = "0";
		$G_TX_ASS = "0.00";
		$G_COEFF_COUT = "0";
	}
	$temp->finish;
	$BD_AUTH->disconnect;

	$CONNECTION = $CONNECTION{"LOCATION"}{"$G_PAYS"}{"datsrc"};
	$LOGIN      = $CONNECTION{"LOCATION"}{"$G_PAYS"}{"login"};
	$PASSWD     = $CONNECTION{"LOCATION"}{"$G_PAYS"}{"passwd"};
}

#
# ListePaysCnx
# param(s) :
# 1 seul optionnel (La base de connection)
# retourne la liste des pacode de la base en question
#

sub ListePaysCnx {
  my ($log) = $_[0] ? $_[0] : "LOCATION";

  foreach (sort keys %{$CONNECTION{$log}}) {
    push (@lst, $_) if $_;
  }
  return @lst;
}

#
# imprimante
# param(s) :
# aucun
# renseigne la variable globale $G_LST_IMP (contenant le menu d�roulant des imprimantes de l'utilisateur)
# attention le menu d�roulant se nomme imprimante
#

sub imprimante {
	$BD_AUTH = DBI->connect($CONNECTION{"LOCATION"}{"FR"}{"datsrc"},$CONNECTION{"LOCATION"}{"FR"}{"login"},$CONNECTION{"LOCATION"}{"FR"}{"passwd"});
	my $ip=&ip;
	my ($un,$de,$tr,$qua) =  split (/\./,$ip);
	my $sth;
	my $req;

	$req =qq{
SELECT prt_label, prt_id
FROM frmwrk.p_printer
LEFT JOIN frmwrk.p_agency ON prt_agc_id = agc_id
WHERE agc_ip REGEXP "(^|;)$tr(;|\$)"
ORDER BY prt_label};
	$sth=$BD_AUTH->prepare("$req");
	$sth->execute();
	$G_LST_IMP =qq{<select name="imprimante">\n};

	while (my @data=$sth->fetchrow_array())	{
		my $sel="";
		if ($data[1] eq $G_IMPRIM) {
			$sel=qq{ selected };
		}
		$G_LST_IMP .=qq{<option value="$data[0],$data[1]"$sel>$data[0]\n};
	}

	$sth->finish;
	$G_LST_IMP .=qq{</select>\n};
	$BD_AUTH->disconnect;
}

# Environnement
# param(s) :
# 1er  : obligatoire (La base de connection souhait�e)
# 2�me : optionnel (Le pacode)
# utilise authentification et imprimante et retourne la connection � la base voulue
#

sub Environnement {
	&authentification();
	&imprimante();
	$hl_color = "CCFFCC";
# 	$x = InitConnexion;
# 	print qq{$x[0] $x[1] $x[2]};
	my $DB = DBI->connect(&InitConnexion, { HandleError => \&mysql_err });
    $DB->{HandleError} = \&mysql_err;
	return $DB;
}

sub mysql_err {
	my ($sec, $min, $hr, $jourmr, $mois, $an, $joursem, $jouran, $ete)
		= localtime;
	my @mois_lib = ("Jan", "Fev", "Mar", "Avr", "Mai", "Jun", "Jui", "Aou",
		"Sep", "Oct", "Nov", "Dec");
	$an += 1900;
	$jourmr = "0".$jourmr if ($jourmr < 10);
	$hr = "0".$hr if ($hr < 10);
	$min = "0".$min if ($min < 10);
	$sec = "0".$sec if ($sec < 10);
	my $date = "$jourmr $mois_lib[$mois] $an";
	my $heure = "$hr:$min:$sec";

# 	my $nomfich = "error_".lc($mois_lib[$mois]).$an.".log";
# 	my $rep = $PATH_COMMONFILES_LOC_LOGS;#/mysql
# 
# 	open (TRACE, ">>$rep/$nomfich")
# 		or die "Impossible d'�crire dans le fichier $nomfich";
# 
# 	my $mess = qq{[$date $heure] [$0] }.join(" ", @_).qq{\n};
# 	print TRACE $mess;
# 
# 	close TRACE;

	return 0;
}

sub verif_droit {
    # Nom du fichier pass� en param�tre ou fichier courant
    $pgm = $_[0] ? $_[0] : $0;
    $pgm =~ s#.*/(.*)#$1#;
    $pgm .= "?";
	
	my $BD = DBI->connect(
        $CONNECTION{"LOCATION"}{"$G_PAYS"}{"datsrc"},
        $CONNECTION{"LOCATION"}{"$G_PAYS"}{"login"},
        $CONNECTION{"LOCATION"}{"$G_PAYS"}{"passwd"}
    );

    my $req = qq{
        SELECT PROFILAUTO
        FROM AUTH.`MEPR`
        LEFT JOIN MENU ON MENU.MEAUTO=MEPR.MEAUTO
        WHERE MEFICHIER LIKE "$pgm%" AND PROFILAUTO="$G_PROFIL"
    };
	my $temp = $BD->prepare($req);
	$temp->execute();
	$DROIT_ACCES = $temp->rows;
	$temp->finish;
	$BD->disconnect;
	
	# L'utilisateur n'a pas le droit d'ouvrir cette page
	if ($DROIT_ACCES == 0) {
        # Traductions
		my %ms= (
            "FR" => {
				"0" => "Erreur",
				"1" => "Vous n'avez pas acc�s � cette fonctionnalit�",
				"2" => "Fermer"
			},
            "IT" => {
				"0" => "Errore",
				"1" => "Non hai accesso a questa funzionalit�.",
				"2" => "Chiudere"
			},
    		"ES" => {
    			"0" => "Error",
    			"1" => "No tienen acceso a esta funcionalidad",
				"2" => "Cerrar"
    		},
    		"PT" =>	{
    			"0" => "Error",
    			"1" => "Voc� n�o t acesso este funcionalidade",
				"2" => "Fechar"
    		},
    		"EXT" => {
    			"0" => "Erreur",
    			"1" => "Vous n'avez pas acc�s � cette fonctionnalit�",
				"2" => "Fermer"
    		},
            "MA" => {
				"0" => "Erreur",
				"1" => "Vous n'avez pas acc�s � cette fonctionnalit�",
				"2" => "Fermer"
			}

    	);

        # Affichage d'un message d'erreur
		print qq{Content-type: text/html

<HTML>
    <HEAD>
        <TITLE>$ms{"$G_PAYS"}{"0"}</TITLE>
        <LINK rel=stylesheet href="$URL_CONF/base.css" type="text/css">
    </HEAD>
    <BODY>
        <P align="center">
            <IMG src="$URL_IMG/logo-entrenous.gif" alt="Entre nous">
            <BR><BR>
            $ms{"$G_PAYS"}{"1"}.
        </P>
    </BODY>
</HTML>};

        # Fin de la page
		exit;
		}

	return $DROIT_ACCES;
}

1;
