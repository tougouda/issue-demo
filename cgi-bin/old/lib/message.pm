# $Id: message.pm,v 1.1.1.1 2006/08/01 07:48:07 clliot Exp $
# $Log: message.pm,v $
# Revision 1.1.1.1  2006/08/01 07:48:07  clliot
# no message
#
# Revision 2.36  2006/04/06 13:53:41  julien
# Traduction du message d'attente.
#
# Revision 2.35  2006/04/06 13:48:33  julien
# Traduction du message d'attente.
#
# Revision 2.34  2006/04/05 08:26:07  julien
# no message
#
# Revision 2.33  2006/03/24 10:03:26  julien
# no message
#
# Revision 2.32  2006/03/23 13:56:34  julien
# DE 252 (alerte saisie date d�but ant�rieur au jour).
#
# Revision 2.31  2006/03/22 16:06:28  julien
# DE258 (contrats anticip�s).
#
# Revision 2.29  2006/03/06 08:35:42  julien
# no message
#
# Revision 2.28  2006/02/27 14:32:54  julien
# Interdiction de demander une remise strictement sup�rieure � 100 %.
#
# Revision 2.27  2006/02/10 11:30:06  julien
# Modification du libell� imprim� pour les remises exceptionnelles.
#
# Revision 2.26  2006/02/08 15:38:23  julien
# Indication du nombre de caract�res maxi sur la saisie de remise exceptionnelle.
#
# Revision 2.25  2006/02/06 16:52:39  julien
# V. 1.4 : Tarification v2
#
# Revision 2.24.4.6  2006/01/17 14:48:15  julien
# Affichage du nom de la personne traitant la demande de remise exceptionnelle.
#
# Revision 2.24.4.5  2006/01/12 11:06:55  julien
# Modification du libell� "1 mois (*)" en "2 mois (*).
#
# Revision 2.24.4.4  2006/01/10 08:35:32  julien
# Probl�me de retours � la ligne en 1024.
#
# Revision 2.24.4.3  2006/01/06 15:57:28  julien
# Affichage de la remise exceptionnelle demand�e.
#
# Revision 2.24.4.2  2006/01/06 10:08:30  julien
# Correction de bugs.
#
# Revision 2.24.4.1  2005/12/13 15:46:31  julien
# no message
#
# Revision 2.24.2.1  2005/12/13 09:32:52  julien
# Tarification v2
#
# Revision 2.23  2005/10/19 14:17:01  julien
# Gestion des documents administratifs.
#
# Revision 2.22  2005/10/11 18:50:06  julien
# Tarification v. 1.
#
# Revision 2.21.2.1  2005/10/05 10:15:42  julien
# no message
#
# Revision 2.21  2005/08/25 16:29:02  julien
# Correction d'un libell� espagnol.
#
# Revision 2.20  2005/08/19 11:51:04  julien
# Conditions g�n�rales de location � approuver.
#
# Revision 2.19  2005/08/05 08:50:28  julien
# Interdiction de l'arr�t d'un contrat si la livraison n'a pas �t� valid�e.
#
# Revision 2.18  2005/07/15 15:40:30  julien
# Libell� blocage passage en facturation si pas de r�cup�ration.
#
# Revision 2.17  2005/07/13 08:42:24  julien
# Jours plus et moins sur la modification de contrat.
#
# Revision 2.16  2005/07/12 15:46:06  julien
# Facturation des services et du nettoyage version lourde.
#
# Revision 2.14  2005/04/04 10:23:17  julien
# Bloquage de la finalisation des tourn�es si toutes les machines n'ont pas �t� attribu�es.
#
# Revision 2.13  2005/03/24 08:12:32  julien
# Tarif personnalis� sur le devis.
#
# Revision 2.12  2005/03/02 14:33:13  julien
# Traductions espagnoles et portugaises.
#
# Revision 2.11  2005/03/01 16:29:32  julien
# Facturation des services (light) et contact commande.
#
# Revision 2.10  2005/01/31 14:45:53  julien
# Machines hors parc.
#
# Revision 2.9  2005/01/05 16:34:56  julien
# Modification de l'assurance (France).
#
# Revision 2.8  2005/01/05 15:41:12  julien
# Modification de l'assurance (France).
#
# Revision 2.7  2005/01/04 16:21:08  julien
# Taux d'assurance � 7% pour l'Espagne.
#
# Revision 2.6  2004/12/14 08:46:38  julien
# Joel Chauvin ne recevait pas les nouveaux clients -> 6 devient 7 dans le nombre de destinataires.
#
# Revision 2.5  2004/12/07 11:22:51  julien
# Ajout de Jo�l Chauvin.
#
# Revision 2.4  2004/11/18 10:57:48  julien
# Ajout de Agn�s Doucet dans les destinataires des mails nouveaux clients.
#
# Revision 2.3  2004/10/19 08:24:09  julien
# Gestion des r�sidus pour l'Espagne.
#
# Revision 2.2  2004/09/27 12:53:05  julien
# Ajout messages changement d'agence.
#
# Revision 2.1  2004/09/17 10:04:59  julien
# Retrait de Mireille Picard pour l'envoi des mails des nouveaux clients.
#
# Revision 2.0  2004/09/16 15:07:22  julien
# Version r�cup�r�e de Lancelot et nettoy�e
#
# Revision 1.2  2004/09/16 14:34:47  julien
# Ajout des identifiants
#

package message;
require Exporter;
require SelfLoader;
@ISA = qw(Exporter SelfLoader);
@EXPORT = qw(message liste_fr liste_es liste_pt);

use strict;
use varglob;
use vars qw(@mess);

sub message {
	my $pays = $_[0];
	if ($pays eq "FR") {@mess=&liste_fr();}
	elsif ($pays eq "LU") {@mess=&liste_fr();}
	elsif ($pays eq "ES") {@mess=&liste_es();}
	elsif ($pays eq "PT") {@mess=&liste_pt();}
	elsif ($pays eq "IT") {@mess=&liste_it();}
	elsif ($pays eq "MA") {@mess=&liste_ma();}
	else  {@mess=&liste_fr();}
	return @mess;
};


sub liste_fr {
my @mess;
#message
#sub saisie
$mess[0] = qq{Assurance};
$mess[1] = qq{comprise};
$mess[2] = qq{recherche};
$mess[3] = qq{ non };
$mess[4] = qq{Rechercher};
$mess[5] = qq{Veuillez saisir ce champ};
$mess[6] = qq{Nouveau Client};
$mess[7] = qq{Veuillez saisir le(s) champ(s)};
$mess[8] = qq{Veuillez saisir un Tarif Journalier OU un Forfait OU un Tarif Mensuel ET une Dur�e Mensuelle};
$mess[9] = qq{Non renseign�};
$mess[10] = qq{Conditions g�n�rales de location � approuver};
$mess[11] = qq{Convention cadre - ouverture de compte client � adresser};
$mess[12] = qq{Pour rattachement � un groupe contacter le Service Support Commercial};
#sub traitment
$mess[50] = qq{La date de d�but est ant�rieure � aujourd'hui};
$mess[51] = qq{La date de fin est ant�rieure � la date de d�but};
$mess[52] = qq{Mauvaise date !};
$mess[53] = qq{Date de d�but ant�rieure � aujourd\\'hui.\\nConserver cette date ?};
$mess[54] = qq{Nb. de jour(s) f�ri�s};
$mess[55] = qq{La dur�e};
$mess[56] = qq{Montant en �};
$mess[57] = qq{Montant de l'assurance en �};
$mess[58] = qq{Valider};
$mess[59] = qq{Dupliquer Contrat};
$mess[60] = qq{Le contrat};
$mess[61] = qq{est saisi, Duplication des donn�es};
$mess[62] = qq{Date non valide};
#sub recherche2
$mess[100] = qq{Le nombre des r�sultats de votre recherche est de};
$mess[101] = qq{Recherche};
$mess[102] = qq{Rechercher};
$mess[103] = qq{Veuillez saisir un des champs};
$mess[104] = qq{Validez votre Recherche};
$mess[105] = qq{r�sultat};
$mess[106] = qq{r�sultats};
$mess[107] = qq{Retour � la recherche};
$mess[108] = qq{Retour};
#sub traitment2
$mess[150] = qq{Le nombre de r�sultats de votre recherche est de};
$mess[151] = qq{Pas de r�sultat pour votre recherche};
$mess[152] = qq{Retour};
$mess[153] = qq{Voir devis};
$mess[154] = qq{CONTR�LER};
$mess[155] = qq{ARR�TER Contrat};
$mess[156] = qq{MODIFIER Contrat};
$mess[157] = qq{ANNULER Contrat};
$mess[158] = qq{Jour Plus/Moins};
$mess[159] = qq{Duplication des donn�es};
$mess[160] = qq{MODIFIER Chantier};
$mess[161] = qq{MODIFIER Info. Client};
$mess[162] = qq{Voir Contrat};
$mess[163] = qq{R�server Mod�le};
$mess[164] = qq{Trouver};
$mess[165] = qq{bloqu�};
$mess[166] = qq{d�bloqu�};
$mess[167] = qq{Client <ste>(<code>) <libbloc>};
$mess[168] = qq{Bonjour\nle Client <ste>(<code>) de <cp>,<ville>\nest <libbloc>\n@+\nBonne journ�e\n};

#sub stock
$mess[200] = qq{R�server Mod�le};
#sub nvclient
$mess[250] = qq{V�rifier le RIB};
$mess[251] = qq{RIB};
$mess[252] = qq{correct};
$mess[253] = qq{incorrect};
$mess[254] = qq{Nouveau Client};
$mess[255] = qq{Cherche Lycos ! Cherche !};
#sub bas form hauttab
$mess[300] = qq{Imprimer};
$mess[301] = qq{Fermer};
#sub saisiecnt
$mess[350] = qq{le prospect};
$mess[351] = qq{code client};
$mess[352] = qq{de l'agence};
$mess[353] = qq{Siret};
$mess[354] = qq{RIB};
$mess[355] = qq{T�l.};
$mess[356] = qq{Fax};
$mess[357] = qq{devient un Client d'Acc�s Industrie};
$mess[358] = qq{pour le contrat N�};
$mess[359] = qq{qui d�bute le};
$mess[360] = qq{devient client};
$mess[361] = qq{N� APE};
#sub traitnvclient
$mess[400] = qq{Origine};
$mess[401] = qq{Document origine};
$mess[402] = qq{Devis};
$mess[403] = qq{Pr�-contrat};
$mess[404] = qq{N� APE};
$mess[405] = qq{Siret};
$mess[406] = qq{Libell�};
$mess[407] = qq{Adresse};
$mess[408] = qq{Ville};
$mess[409] = qq{T�l.};
$mess[410] = qq{Portable};
$mess[411] = qq{Fax};
$mess[412] = qq{Contact};
$mess[413] = qq{Commentaire};
$mess[414] = qq{Banque};
$mess[415] = qq{RIB};
$mess[416] = qq{Mode de paiement};
$mess[417] = qq{Nouveau client de};
$mess[418] = qq{Secteur d'activit�};
#sub arretmodif
$mess[450] = qq{Nb. de jour(s) f�ri�(s)};
$mess[451] = qq{La date de fin est ant�rieure � la date de d�but};
$mess[452] = qq{Le contrat};
$mess[453] = qq{est arr�t�};
$mess[454] = qq{est modifi�};
#sub transport
$mess[500] = qq{Montant};
$mess[501] = qq{Contrat � �diter};
$mess[502] = qq{Collectif};
$mess[503] = qq{Code Client};
$mess[504] = qq{Client Valide ?};
$mess[505] = qq{Transport � attribuer};
$mess[506] = qq{Devis transporteur};
$mess[507] = qq{Commande transporteur};
$mess[508] = qq{Passer � Ordonner Tourn�e};
$mess[509] = qq{Date de transport};
$mess[510] = qq{Imprimer contrat};
$mess[511] = qq{Nouveau Client};
$mess[512] = qq{Panne machine};
$mess[513] = qq{Impossible de supprimer le transporteur sur le contrat %s};
#sub traitattmach
$mess[550] = qq{La Machine};
$mess[551] = qq{est s�lectionn�e au moins 2 fois};
$mess[552] = qq{La machine %s n'a pas le m�me mod�le que le contrat %s auquel vous essayez de l'attribuer};
$mess[553] = qq{Vous avez dej� attribu� cette machine};
#sub attmach
$mess[600] = qq{Veuillez saisir un num�ro de Parc d'une machine disponible};
$mess[601] = qq{Ce num�ro est incompatible avec le parc actuel de l'agence};
$mess[602] = qq{Attribuer Machine};
$mess[603] = qq{Imprimer liste machine};
#sub carburant
$mess[650] = qq{Contrat, D�signation};
$mess[651] = qq{Attribuer Carburant};
#sub transfert
$mess[700] = qq{Machines � r�cup�rer};
$mess[701] = qq{Transf�rer};
$mess[702] = qq{Passer � BL/BR/Tourn�e};
$mess[703] = qq{Machines � livrer};
$mess[704] = qq{Transport � attribuer};
$mess[705] = qq{N� Parc};
#sub tournee
$mess[750] = qq{Tourn�e N�};
$mess[751] = qq{Reste Capacit� (Kg) avant L/R/T};
$mess[752] = qq{En cours};
$mess[753] = qq{Effectu�e};
$mess[754] = qq{�chou�e};
$mess[755] = qq{Capacit�};
$mess[756] = qq{Reste};
$mess[757] = qq{Masse de r�cup.};
$mess[758] = qq{Masse de livraison};
$mess[759] = qq{Passer � BL/BR/Tourn�e};
$mess[760] = qq{Valider Tourn�e};
$mess[761] = qq{Le contr�le de la Tourn�e};
$mess[762] = qq{n'est pas complet, };
$mess[763] = qq{machine(s) � Pointer};
$mess[764] = qq{Le contr�le de la Tourn�e N�};
$mess[765] = qq{est complet};
$mess[780] = qq{Date tourn�e};
#sub valtourn
$mess[800] = qq{La Tourn�e N�};
$mess[801] = qq{devra �tre valid�e};
$mess[802] = qq{est valid�e};
#sub verification
$mess[850] = qq{Date de Contr�le : < 2 mois(*), < 15 jours(**), < 1 semaine (***), D�pass�e (****)};
$mess[851] = qq{Machines en Livraison};
$mess[852] = qq{Machines en transfert inter-chantiers};
$mess[853] = qq{Machine en Livraison};
$mess[854] = qq{Machine en R�cup�ration};
$mess[855] = qq{Machine en Transfert};
$mess[856] = qq{Machine en V�rification};
$mess[857] = qq{V�rification � R�vision};
$mess[858] = qq{R�vision � V�rification};
$mess[859] = qq{Disponible � V�rification};
$mess[860] = qq{Panne � V�rification};
$mess[861] = qq{V�rification � Panne};
$mess[862] = qq{Machine en R�vision};
$mess[863] = qq{Panne � R�vision};
$mess[864] = qq{R�vision � Panne};
$mess[865] = qq{Machine en Panne};
$mess[866] = qq{R�vision � Disponible};
$mess[867] = qq{V�rification � Disponible};
$mess[868] = qq{Panne � Disponible};
$mess[869] = qq{Machine Disponible};
$mess[870] = qq{�tat};
$mess[871] = qq{Impression Liste Machine};
$mess[872] = qq{Machines en R�cup�ration};
$mess[873] = qq{Machines en transfert inter-agences};
$mess[874] = qq{Impression envoy�e sur l'imprimante};
#sub devis
$mess[900] = qq{Le Devis};
$mess[901] = qq{est enregistr�};
$mess[902] = qq{Historique};
$mess[903] = qq{Nouveau Client};
$mess[904] = qq{Assurance};
$mess[905] = qq{ non };
$mess[906] = qq{comprise};
$mess[907] = qq{Passer au Pr�-contrat};
$mess[908] = qq{Assurance comprise};
$mess[909] = qq{Assurance Non comprise};
$mess[910] = qq{Modifier le pied de devis};
$mess[911] = qq{Abandonner};
$mess[912] = qq{Impression Devis};
$mess[913] = qq{Envoi par fax ou impression locale};
$mess[914] = qq{Erreur};
$mess[915] = qq{Pr�visualisation du fichier Acrobat};
$mess[916] = qq{Aucune page � afficher};
$mess[917] = qq{La zone est obligatoire};
$mess[918] = qq{Vous devez faire une estimation du transport.};
$mess[919] = qq{Impression impossible : remise exceptionnelle transport en attente ou transport incorrect.};
$mess[920] = qq{Impression impossible : remise exceptionnelle location en attente.};
$mess[921] = qq{Etes-vous s�r de vouloir annuler la remise exceptionnelle?};
$mess[922] = qq{Annuler la remise excep.};
$mess[923] = qq{Fiche client};
#sub traitdevis
$mess[950] = qq{Le devis};
$mess[951] = qq{devient un contrat};
$mess[952] = qq{est abandonn�};
$mess[953] = qq{Choisissez un motif d'abandon};
$mess[954] = qq{Autre};
$mess[955] = qq{Veuillez choisir un motif avant d'abandonner le devis};
$mess[956] = qq{Devis abandonn�};
$mess[957] = qq{Objet : Votre demande de devis location Acces Industrie};
$mess[958] = qq{Madame, monsieur, <br><br>
Suite � votre demande, vous trouverez, ci-joint, le devis de location �tabli sur la base des informations que vous nous avez communiqu�es.
Je reste � votre disposition au : _TELAG_ pour tout compl�ment d'information.<br><br>
Souhaitant avoir r�pondu � vos attentes, je vous prie d'agr�er, Madame, monsieur, l'expression de mes sinc�res salutations.<br><br>
  _NOMEXP_};
#sub ville
$mess[1000] = qq{Agence(s)};
$mess[1001] = qq{ou};
#sub facturation
$mess[1050] = qq{Nous sommes le };
$mess[1051] = qq{contrat(s) � facturer};
$mess[1052] = qq{Facturer};
$mess[1053] = qq{La facturation du};
$mess[1054] = qq{mois � d�j� �t� effectu�e};
$mess[1055] = qq{Il y a};
$mess[1056] = qq{Contrats � passer en facturation};
$mess[1057] = qq{On facture};
$mess[1058] = qq{Exportation vers Sage};
$mess[1059] = qq{La facturation ira};
$mess[1060] = qq{La facturation a d�j� �t� effectu�e};
$mess[1061] = qq{� saisir};
$mess[1062] = qq{jusqu'au};
$mess[1063] = qq{Contrats � arr�ter};
$mess[1064] = qq{Carburants � saisir};
#sub traitfac
$mess[1100] = qq{Le montant de};
$mess[1101] = qq{est de};
$mess[1102] = qq{pour};
$mess[1103] = qq{factures};
$mess[1104] = qq{Le montant Total des factures est de};
#sub tableau
$mess[1150] = qq{Tri par date};
$mess[1151] = qq{Tri par ville};
$mess[1152] = qq{R�vision};
$mess[1153] = qq{Panne};
$mess[1154] = qq{Contr�le};
$mess[1155] = qq{Disponible};
$mess[1156] = qq{Total machine(s)};
$mess[1157] = qq{�tat};
$mess[1158] = qq{Impression Liste Machine};
$mess[1159] = qq{V�rification};
#sub transfagag
$mess[1200] = qq{Transf�rer};
#sub validnvclient
$mess[1250] = qq{le Code Client};
$mess[1251] = qq{le Collectif};
$mess[1252] = qq{entre};
$mess[1253] = qq{� la ligne};
#sub mois_stat
$mess[1300] = qq{Historique};
#sub stat_mois
$mess[1350] = qq{D�but du mois };
$mess[1351] = qq{ et fin de mois};
$mess[1352] = qq{Nombre};
$mess[1353] = qq{Total};
$mess[1354] = qq{Moyenne};
$mess[1355] = qq{�cart type};
$mess[1356] = qq{C.A. agence};
$mess[1357] = qq{Dur�e};
$mess[1358] = qq{Assurance Non Comprise};
$mess[1359] = qq{Transport};
$mess[1360] = qq{Sous Traitant Transport};
$mess[1361] = qq{Carburant};
$mess[1362] = qq{Contrat Actif};
$mess[1363] = qq{Contrat Arr�t�};
$mess[1364] = qq{Contrat de moins de 5 jours};
$mess[1365] = qq{Contrat de plus de 5 jours};
$mess[1366] = qq{Contrat Mensuel};
$mess[1367] = qq{Forfait};
$mess[1368] = qq{Famille};
$mess[1369] = qq{C.A.};
$mess[1370] = qq{Nb. Parc};
$mess[1371] = qq{jour(s) Lou�(s)};
$mess[1372] = qq{Nb. Contrat};
$mess[1373] = qq{Jours Mois};
$mess[1374] = qq{Moyenne Prix Lou�};
$mess[1375] = qq{Moyenne Prix Parc};
$mess[1376] = qq{Rotation(Rentabilit�) en %};
$mess[1377] = qq{Ratio};
$mess[1378] = qq{Moyenne Jours};
$mess[1379] = qq{TOTAL};
$mess[1380] = qq{D�signation};
#sub nomade
$mess[1400] = qq{Je suis �};
$mess[1401] = qq{Agences};
$mess[1402] = qq{Je me t�l�porte};
#sub saisiemach
$mess[1450] = qq{La Machine};
$mess[1451] = qq{est saisie};
$mess[1452] = qq{Parc};
$mess[1453] = qq{ou};
$mess[1454] = qq{de S�rie};
$mess[1455] = qq{existe d�j�};
$mess[1456] = qq{Valider};
$mess[1457] = qq{Pr�-contrat};
$mess[1458] = qq{Contrat};
$mess[1459] = qq{Arr�t};
$mess[1460] = qq{Archive};
$mess[1461] = qq{Annul�};
$mess[1462] = qq{Livraison};
$mess[1463] = qq{R�cup�ration};
$mess[1464] = qq{Transfert Chantier};
$mess[1465] = qq{Mis en facturation};
#message impression
$mess[1500] = qq{BON DE LIVRAISON};
$mess[1501] = qq{BON DE RETRAIT};
#imprssion ass
$mess[1550] = qq{Assurance factur�e au client = % du prix de location jour fois jours calendaires};
$mess[1551] = qq{Assurance non factur�e};
$mess[1552] = qq{Assur�e par le client};
$mess[1553] = qq{Assurance en plus = 10% du prix de location};
$mess[1554] = qq{};
$mess[1555] = qq{MACHINE ASSUR�E PAR LE CLIENT};
$mess[1556] = qq{Assurance factur�e au client = };
$mess[1557] = qq{% du prix de location jour fois jours calendaires};
$mess[1558] = qq{Assurance incluse};
#sub traitfac
$mess[1600] = qq{Tarif Mensuel};
$mess[1601] = qq{Forfait};
$mess[1602] = qq{Avoir};
$mess[1603] = qq{Transport aller};
$mess[1604] = qq{Transport retour};
$mess[1605] = qq{Carburant};
$mess[1606] = qq{Assurance 10%};
$mess[1607] = qq{ Du };
$mess[1608] = qq{ au };
$mess[1609] = qq{Heures deduites};
$mess[1610] = qq{Heures supplementaires};
$mess[1611] = qq{Heures samedi};
$mess[1612] = qq{Heures dimanche};
$mess[1613] = qq{Jours deduits};
$mess[1614] = qq{Jours supplementaires};
$mess[1615] = qq{Cette facture comprend les jours travailles des 2 mois};
$mess[1616] = qq{Participation au recyclage};
$mess[1617] = qq{Nettoyage};
$mess[1618] = qq{Service};
$mess[1619] = qq{Remise exceptionnelle transport de};
$mess[1620] = qq{Rem. except. XX% sur Aller};
$mess[1621] = qq{Rem. except. XX% sur Retour};
$mess[1622] = qq{Rem. except. XX% sur Aller (remise pour cumul * mach. incluse)};
$mess[1623] = qq{Rem. except. XX% sur Retour(remise pour cumul * mach. incluse)};
$mess[1624] = qq{Rem. pour cumul * mach. XX%};
$mess[1625] = qq{Rem. pour cumul * mach. XX%};
$mess[1626] = qq{Assurance n�goci�e 5%};
$mess[1627] = qq{Rem. except. sur Aller};
$mess[1628] = qq{Rem. except. sur Retour};
#arret contratraie
$mess[1650] = qq{Arr�t Impossible : contacter le responsable de la logistique pour pointer la livraison dans la tourn�e N�};
#sub traitcasse
$mess[1700] = qq{le Devis est saisi};
$mess[1701] = qq{Impression du document};
#annuler client
$mess[1750] = qq{Vous allez supprimer un Client ! Validez pour confirmer !};
$mess[1751] = qq{Le 1er Client est supprim�. Les Devis et Contrats ont �t� report�s sur le 2nd.};
$mess[1752] = qq{&nbsp;At&nbsp;&nbsp;&nbsp;Acc&nbsp;&nbsp;Ref};
$mess[1753] = qq{At&nbsp;:&nbsp;Attente&nbsp;&nbsp;&nbsp;Acc&nbsp;:&nbsp;Accept�&nbsp;&nbsp;&nbsp;Ref&nbsp;:&nbsp;Refus�};
#doublon client
$mess[1800] = qq{Le Client/Code Client };
$mess[1801] = qq{existe d�j� : Recherchez � ce libell�};
$mess[1802] = qq{a �t� cr��, vous pouvez rechercher � ce libell�};
$mess[1803] = qq{Un champ n'a pas �t� rempli correctement};
$mess[1804] = qq{Ce SIRET est d�j� utilis� par les client(s) suivant(s) : %s. Veuillez contacter le service Support Commercial afin de poursuivre la cr�ation du client.};
#sub verification
$mess[1850] = qq{Contr�le � V�rification};
$mess[1851] = qq{V�rification � Contr�le};
$mess[1852] = qq{Machine(s) en Contr�le};
#chg client Contrat
$mess[1900] = qq{Le Client va �tre chang� ! Validez pour confirmer !};
$mess[1901] = qq{Le Client est chang�};
#remettre en cours contrat
$mess[1950] = qq{Le Contrat va �tre remis en cours ! Validez pour confirmer !};
$mess[1951] = qq{Le Contrat est remis en cours};
$mess[1952] = qq{Un transport est planifi� pour ce contrat ! Il faut l'annuler avant de pouvoir remettre en cours le contrat !};
#newstat
$mess[2000] = qq{Agence};
$mess[2001] = qq{C.A. Mois};
$mess[2002] = qq{C.A. Transport};
$mess[2003] = qq{C.A. Carburant};
$mess[2004] = qq{C.A. SAV};
$mess[2005] = qq{C.A. Forfait};
$mess[2006] = qq{Co�t Financier};
$mess[2007] = qq{Famille};
$mess[2008] = qq{Nb. Machines};
$mess[2009] = qq{C.A. Machine Jour};
$mess[2010] = qq{C.A. Moyen Jour};
$mess[2011] = qq{Taux de Rotation Financier en %};
$mess[2012] = qq{Taux de Rotation Machine en %};
$mess[2013] = qq{Mod�le};
$mess[2014] = qq{Pour la famille};
$mess[2015] = qq{Taux d'occupation (%)};
$mess[2016] = qq{Taux d'occupation (%) depuis le d�but du mois};
$mess[2017] = qq{Taux d'occupation (%) du mois pr�c�dent};
$mess[2018] = qq{Comparatif Mensuel au mois pr�c�dent};
#consultation contrat
$mess[2050] = qq{N� Contrat};
$mess[2051] = qq{�tat Contrat};
$mess[2052] = qq{Client};
$mess[2053] = qq{T�l. Client};
$mess[2054] = qq{Fax Client};
$mess[2055] = qq{Contact Client};
$mess[2056] = qq{Chantier};
$mess[2057] = qq{Adr. Chantier};
$mess[2058] = qq{Ville Chantier};
$mess[2059] = qq{T�l. Chantier};
$mess[2060] = qq{Contact Chantier};
$mess[2061] = qq{D�signation Machine};
$mess[2062] = qq{N� Parc};
$mess[2063] = qq{Date D�part};
$mess[2064] = qq{Date Retour};
$mess[2065] = qq{Jour Plus};
$mess[2066] = qq{Jour Moins};
$mess[2067] = qq{Dur�e};
$mess[2068] = qq{Prix jour};
$mess[2069] = qq{Prix Mensuel};
$mess[2070] = qq{Montant Forfaitaire};
$mess[2071] = qq{Transport Aller};
$mess[2072] = qq{Transport Retour};
$mess[2073] = qq{Transport Planifi�};
$mess[2074] = qq{Chauffeur ou Sous Traitant (r�cup.)};
$mess[2075] = qq{Assurance non comprise};
$mess[2076] = qq{ non};
$mess[2077] = qq{Assurance};
$mess[2078] = qq{Montant Total};
$mess[2079] = qq{Commentaire};
$mess[2080] = qq{Fermer};
$mess[2081] = qq{Chauffeur ou Sous Traitant (aller)};
$mess[2082] = qq{hors jours d�j� factur�s : };
$mess[2083] = qq{Contact Commande};
$mess[2084] = qq{T�l�phone Contact Commande};
$mess[2085] = qq{Nettoyage};
$mess[2086] = qq{Service};
# transportint.cfg
$mess[2100] = qq{Transport Interne};
$mess[2101] = qq{Commande Transport externe};
$mess[2102] = qq{Num�ro};
$mess[2103] = qq{N� Contrat};
$mess[2104] = qq{Client};
$mess[2105] = qq{Chantier};
$mess[2106] = qq{C.P.};
$mess[2107] = qq{Ville};
$mess[2108] = qq{Quai};
$mess[2109] = qq{Livraison anticip�e possible};
$mess[2110] = qq{Date D�part};
$mess[2111] = qq{Date Retour};
$mess[2112] = qq{Montant};
$mess[2113] = qq{Poids};
$mess[2114] = qq{D�signation Machine};
$mess[2115] = qq{N� Parc};
$mess[2116] = qq{�tat Contrat};
$mess[2117] = qq{Heure de Liv./R�cup.};
$mess[2118] = qq{Tp. planifi�};
$mess[2119] = qq{Chauffeur};
$mess[2120] = qq{Commande transporteur};
$mess[2130] = qq{Plage horaire};
$mess[2131] = qq{Pas de commentaire};
#contrat.cfg et contrat2.cfg
$mess[2150] = qq{Num�ro};
$mess[2151] = qq{N� Contrat};
$mess[2152] = qq{Client};
$mess[2153] = qq{Chantier};
$mess[2154] = qq{C.P.};
$mess[2155] = qq{Ville};
$mess[2156] = qq{Date D�part};
$mess[2157] = qq{Date Retour};
$mess[2158] = qq{D�signation Machine};
$mess[2159] = qq{N� Parc};
$mess[2160] = qq{�tat Contrat};
$mess[2161] = qq{�dition Contrat(s)};
$mess[2162] = qq{�dition Contrat(s) pour 1�re impression};
#comcont.cfg
$mess[2200] = qq{Num�ro};
$mess[2201] = qq{Dernier Contrat};
$mess[2202] = qq{Indice};
$mess[2203] = qq{Modif.};
$mess[2204] = qq{Code postal chantier};
$mess[2205] = qq{D�signation Machine};
$mess[2206] = qq{N� Parc};
$mess[2207] = qq{�tat};
$mess[2208] = qq{Date Fin Contrat};
$mess[2209] = qq{Date �ch�ance};
$mess[2210] = qq{Organisme};
$mess[2211] = qq{Commande de Visite P�riodique};
$mess[2212] = qq{Commande de Contr�le};
$mess[2250] = qq{Imp. Unit�};
$mess[2251] = qq{N� Comm};
$mess[2252] = qq{Date de Visite};
#nvclient.cfg
$mess[2300] = qq{Nouveaux clients};
$mess[2301] = qq{Num�ro};
$mess[2302] = qq{Agence};
$mess[2303] = qq{Soci�t�};
$mess[2304] = qq{Adresse};
$mess[2305] = qq{C.P.};
$mess[2306] = qq{Ville};
$mess[2307] = qq{SIRET};
$mess[2308] = qq{T�l.};
$mess[2309] = qq{Fax};
$mess[2310] = qq{Mode Paiement};
$mess[2311] = qq{RIB};
$mess[2312] = qq{est d�j� attribu� au client};
$mess[2313] = qq{Modes Paiements};
$mess[2320] = qq{Recherche par nom de client};
#nvclient.cfg
$mess[2350] = qq{APE};
$mess[2351] = qq{Secteur d'activit�};
$mess[2352] = qq{SIRET};
$mess[2353] = qq{Libell�};
$mess[2354] = qq{Adresse};
$mess[2355] = qq{Cmpt. adresse};
$mess[2356] = qq{Ville};
$mess[2357] = qq{T�l�phone};
$mess[2358] = qq{Portable};
$mess[2359] = qq{Fax};
$mess[2360] = qq{Contact};
$mess[2361] = qq{Commentaire};
$mess[2362] = qq{Mode Paiement};
$mess[2363] = qq{Banque};
$mess[2364] = qq{RIB};
$mess[2365] = qq{Grille de base};
$mess[2366] = qq{Groupe};
$mess[2367] = qq{Qualification};
$mess[2368] = qq{Email Client};
$mess[2370] = qq{Aucun};
$mess[2371] = qq{Fin de validit� (Ass.)};
$mess[2372] = qq{Potentiel};
$mess[2373] = qq{Cl.};
$mess[2374] = qq{Pot.};
$mess[2375] = qq{Aucun};
$mess[2380] = qq{Classe};

$mess[2390] = qq{Nouveau client};

$mess[2392] = qq{archive.dev\@acces-industrie.com};
$mess[2393] = qq{audrey.guysens\@acces-industrie.com};
$mess[2394] = qq{christine.bellanger\@acces-industrie.com};
$mess[2395] = qq{valerie.penicaud\@acces-industrie.com};
$mess[2396] = qq{agnes.doucet\@acces-industrie.com};
$mess[2397] = qq{fabienne.cazassus\@acces-industrie.com};
$mess[2398] = qq{nabila.yemloul\@acces-industrie.com};
$mess[2399] = qq{6};
#Devis.cfg
$mess[2400] = qq{Clients};
$mess[2401] = qq{Ville Chantier};
$mess[2402] = qq{Libell� Chantier};
$mess[2403] = qq{Adresse Chantier};
$mess[2404] = qq{Contact Chantier};
$mess[2405] = qq{T�l. Chantier};
$mess[2406] = qq{N� Commande};
$mess[2407] = qq{Type Machine};
$mess[2408] = qq{Hors parc};
$mess[2409] = qq{Quantit�};
$mess[2410] = qq{Prix Jour/Mois};
$mess[2411] = qq{Type Montant};
$mess[2412] = qq{Dur�e};
$mess[2413] = qq{Date Pr�visionnelle};
$mess[2414] = qq{Montant};
$mess[2415] = qq{Transport Aller};
$mess[2416] = qq{Transport Retour};
$mess[2417] = qq{Assurance};
$mess[2418] = qq{Montant assurance (estimation)};
$mess[2419] = qq{Montant Total};
$mess[2420] = qq{Commentaire};
$mess[2421] = qq{Notes Commerciales (ne figurent pas sur le devis)};
$mess[2422] = qq{Devis Accept�};
$mess[2423] = qq{Calculer};
$mess[2424] = qq{Valider};
$mess[2425] = qq{Stock};
$mess[2426] = qq{Devis};
$mess[2427] = qq{Aller};
$mess[2428] = qq{Retour};
$mess[2429] = qq{Tarif de base};
$mess[2430] = qq{Remise};
$mess[2431] = qq{Ajouter une machine};
$mess[2432] = qq{Client};
$mess[2433] = qq{Machine};
$mess[2434] = qq{Dur�e};
$mess[2435] = qq{Prix};
$mess[2436] = qq{Montant};
$mess[2437] = qq{Transport};
$mess[2438] = qq{Demander une remise exceptionnelle};
$mess[2439] = qq{Rem. excep.};
$mess[2440] = qq{Zone};
$mess[2441] = qq{Estimation de transport};
$mess[2442] = qq{Montant aller et/ou retour incorrect(s).};
$mess[2443] = qq{Le montant aller ou retour est incorrect.};
$mess[2444] = qq{(entre 1 et 6)};
$mess[2445] = qq{Full service (minimum 6 mois)};
#tournee.cfg
$mess[2450] = qq{N� Contrat};
$mess[2451] = qq{Chantier};
$mess[2452] = qq{Adresse};
$mess[2453] = qq{C.P.};
$mess[2454] = qq{Ville};
$mess[2455] = qq{Contact};
$mess[2456] = qq{D�signation};
$mess[2457] = qq{N� Parc};
$mess[2458] = qq{Poids};
$mess[2459] = qq{�tat};
$mess[2460] = qq{�tat contrat};
$mess[2461] = qq{Nom chauffeur};
$mess[2462] = qq{Heure de Liv./R�cup.};
$mess[2463] = qq{Ordre};
#retour.cfg
$mess[2500] = qq{Ordonner Tourn�e};
$mess[2501] = qq{�dition BL/BR/Tourn�e};
$mess[2502] = qq{Pointage LV/RC/Transfert/Tourn�e};

$mess[2505] = qq{N� Contrat};
$mess[2506] = qq{Chantier};
$mess[2507] = qq{Adresse};
$mess[2508] = qq{C.P.};
$mess[2509] = qq{Ville};
$mess[2510] = qq{Contact};
$mess[2511] = qq{D�signation};
$mess[2512] = qq{N� Parc};
$mess[2513] = qq{�tat};
$mess[2514] = qq{�tat contrat};
$mess[2515] = qq{Nom chauffeur};
$mess[2516] = qq{Ordre};
$mess[2517] = qq{Heure de Liv./R�cup.};
$mess[2518] = qq{Effectu�e};
$mess[2519] = qq{Chauffeur Interne};
$mess[2520] = qq{Sous Traitant};
#Type de montant
$mess[2550] = qq{Type Mt};
$mess[2551] = qq{Jour};
$mess[2552] = qq{Mensuel};
$mess[2553] = qq{Forfaitaire};
#libell�s devis
$mess[2600] = qq{Modifier l'ent�te};
$mess[2601] = qq{Modifier la ligne};
$mess[2602] = qq{Modifier le mod�le};
$mess[2603] = qq{Vous devez imp�rativement enregistrer la ligne avant toute autre chose!};
$mess[2604] = qq{Vous devez imp�rativement enregistrer le contrat avant toute autre chose!};
#modifiarret modif2.cfg
$mess[2650] = qq{Modification/Arr�t de contrat};
$mess[2651] = qq{Num�ro};
$mess[2652] = qq{�tat Contrat};
$mess[2653] = qq{N� Contrat};
$mess[2654] = qq{Indice};
$mess[2655] = qq{Client};
$mess[2656] = qq{Libell� Chantier};
$mess[2657] = qq{Ville Chantier};
$mess[2658] = qq{Contact chantier};
$mess[2659] = qq{T�l�phone chantier};
$mess[2660] = qq{Contact commande};
$mess[2661] = qq{T�l�phone contact commande};
$mess[2662] = qq{D�signation Machine};
$mess[2663] = qq{N� Parc};
$mess[2664] = qq{Hors parc};
$mess[2665] = qq{Date D�part};
$mess[2666] = qq{Date Retour};
$mess[2667] = qq{Dur�e};
$mess[2668] = qq{Jour en plus};
$mess[2669] = qq{Jour en moins};
$mess[2670] = qq{Prix Jour};
$mess[2671] = qq{Prix Mois};
$mess[2672] = qq{Forfait};
$mess[2673] = qq{Assurance Comprise};
$mess[2674] = qq{Montant Assurance};
$mess[2675] = qq{Transport Aller};
$mess[2676] = qq{Enl�vement Aller};
$mess[2677] = qq{Transport Retour};
$mess[2678] = qq{Enl�vement Retour};
$mess[2679] = qq{Quai};
$mess[2680] = qq{Montant Total HT};
$mess[2681] = qq{Commande};
$mess[2682] = qq{Commentaire};
$mess[2683] = qq{Note};
$mess[2684] = qq{Paiement � la livraison};
$mess[2685] = qq{Client};
$mess[2686] = qq{N� Parc};
$mess[2687] = qq{Ce contrat a d�j� �t� arr�t�};
$mess[2688] = qq{Le Type de machine est chang�};
$mess[2689] = qq{Le contrat est annul�};
$mess[2690] = qq{Accessoires li�s au contrat};
$mess[2691] = qq{Accessoire (N� Parc)};
$mess[2692] = qq{Contrat (�tat)};
$mess[2693] = qq{Montant};
$mess[2694] = qq{Date D�part};
$mess[2695] = qq{Date Retour};
$mess[2696] = qq{Machine lou�e sans l'accessoire par d�faut};
$mess[2697] = qq{Contrat d'origine};
#Stock
$mess[2700] = qq{D�signation};
$mess[2701] = qq{Mod�le};
$mess[2702] = qq{�l�vation};
$mess[2703] = qq{�nergie};
$mess[2704] = qq{Poids};
$mess[2705] = qq{Stock R�serv�};
$mess[2706] = qq{Stock R�servable};
$mess[2707] = qq{Stock Total};
$mess[2708] = qq{LOUE 0%};
$mess[2709] = qq{TRSF 0%};
$mess[2710] = qq{PANN 0%};
$mess[2711] = qq{CONT 90%};
$mess[2712] = qq{RECU 50%};
$mess[2713] = qq{VERF 80%};
$mess[2714] = qq{REVI 80%};
$mess[2715] = qq{DISP 100%};
#Saisie pr�-contrat
$mess[2750] = qq{Saisie pr�-contrat};
$mess[2751] = qq{Clients};
$mess[2752] = qq{Type machine};
$mess[2753] = qq{Hors parc};
$mess[2754] = qq{Ville chantier};
$mess[2755] = qq{Libell� chantier};
$mess[2756] = qq{Adresse chantier};
$mess[2757] = qq{Contact chantier};
$mess[2758] = qq{Tel. chantier};
$mess[2759] = qq{Contact commande};
$mess[2760] = qq{Tel. contact commande};
$mess[2761] = qq{N� Commande};
$mess[2762] = qq{Date D�part};
$mess[2763] = qq{Date Retour};
$mess[2764] = qq{Jour Plus};
$mess[2765] = qq{Jour Moins};
$mess[2766] = qq{Prix apr�s rem. excep.};
$mess[2767] = qq{Dur�e};
$mess[2768] = qq{Prix Mois};
$mess[2769] = qq{Montant Forfaitaire};
$mess[2770] = qq{Assurance};
$mess[2771] = qq{Aller};
$mess[2772] = qq{Enl�v. sur place};
$mess[2773] = qq{Retour};
$mess[2774] = qq{Enl�v. sur place};
$mess[2775] = qq{Quai};
$mess[2776] = qq{Commentaire};
$mess[2777] = qq{Mod�le Imp�ratif};
$mess[2778] = qq{Transfert Chantier � Chantier};
$mess[2779] = qq{Paiement avant livraison};
$mess[2780] = qq{Calculer};
$mess[2781] = qq{Attention, le mod�le s�lectionn� n'est pas r�serv� tant que le pr�-contrat n'est pas valid� !};
$mess[2782] = qq{La derni�re machine r�servable de ce mod�le vient d'�tre r�serv�e, s�lectionnez un autre mod�le !};
$mess[2783] = qq{Type montant};
$mess[2784] = qq{Tarif de base};
$mess[2785] = qq{Remise};
$mess[2786] = qq{Remise exceptionnelle};
$mess[2787] = qq{Nettoyage};
$mess[2788] = qq{Enlevement sur place};
$mess[2789] = qq{Machine supp.};
$mess[2790] = qq{Machine supp.};

#Etat pr�-contrat
$mess[2800] = qq{Agence};
$mess[2801] = qq{Responsable};
$mess[2802] = qq{Adresse};
$mess[2803] = qq{T�l.};
$mess[2804] = qq{Fax};
$mess[2805] = qq{C.P.};
$mess[2806] = qq{Ville};
$mess[2807] = qq{Client};
$mess[2808] = qq{Adresse};
$mess[2809] = qq{C.P.};
$mess[2810] = qq{Ville};
$mess[2811] = qq{T�l.};
$mess[2812] = qq{Fax};
$mess[2813] = qq{Chantier};
$mess[2814] = qq{Adresse};
$mess[2815] = qq{C.P.};
$mess[2816] = qq{Ville};
$mess[2817] = qq{Responsable};
$mess[2818] = qq{Marque};
$mess[2819] = qq{Famille};
$mess[2820] = qq{�nergie};
$mess[2821] = qq{D�signation};
$mess[2822] = qq{Poids};
$mess[2823] = qq{Location Journali�re};
$mess[2824] = qq{Assurance};
$mess[2825] = qq{Transport Aller};
$mess[2826] = qq{Transport Retour};
$mess[2827] = qq{Date d�part};
$mess[2828] = qq{Date retour};
$mess[2829] = qq{Jour en plus};
$mess[2830] = qq{Jour en moins};
$mess[2831] = qq{Dur�e};
$mess[2832] = qq{Agent loueur};
$mess[2833] = qq{Indice};
$mess[2834] = qq{Assurance comprise};
#conscont.cfg
$mess[2850] = qq{Consultation Contrat};
$mess[2851] = qq{Num�ro};
$mess[2852] = qq{N� Contrat};
$mess[2853] = qq{Client};
$mess[2854] = qq{Libell� Chantier};
$mess[2855] = qq{C.P. Chantier};
$mess[2856] = qq{Ville Chantier};
$mess[2857] = qq{Contact Chantier};
$mess[2858] = qq{Contact Commande};
$mess[2859] = qq{D�signation Machine};
$mess[2860] = qq{N� Parc};
$mess[2861] = qq{Date D�part};
$mess[2862] = qq{Date Retour};
$mess[2863] = qq{Dur�e};
$mess[2864] = qq{Prix Jour};
$mess[2865] = qq{Prix Mois};
$mess[2866] = qq{Montant Assurance};
$mess[2867] = qq{Montant Gestion de recours};
$mess[2868] = qq{Transport Aller};
$mess[2869] = qq{Transport Retour};
$mess[2870] = qq{Quantit� Carbu (L)};
$mess[2871] = qq{Nettoyage};
$mess[2872] = qq{Service};
$mess[2873] = qq{Montant Total HT};
$mess[2874] = qq{�tat contrat};
$mess[2875] = qq{N� Commande};
$mess[2876] = qq{Note};

$mess[2882] = qq{N� Contrat};
$mess[2883] = qq{Client};
$mess[2884] = qq{N� Parc};
$mess[2885] = qq{Chantier};
$mess[2886] = qq{Libell� Chantier};
$mess[2887] = qq{D�partement Chantier};
$mess[2888] = qq{Ann�e};
$mess[2889] = qq{�tat contrat};
$mess[2890] = qq{soit};
$mess[2891] = qq{jour(s)};
$mess[2892] = qq{/&nbsp;jour};
$mess[2893] = qq{/&nbsp;mois};
$mess[2894] = qq{au forfait};
$mess[2895] = qq{Tarif ind�terminable !};
$mess[2896] = qq{Seuls les jours en plus peuvent �tre modifi�s};
#machine.cfg
$mess[2900] = qq{Liste machine / agence de };
$mess[2901] = qq{D�signation};
$mess[2902] = qq{N� Parc};
$mess[2903] = qq{Hors parc};
$mess[2904] = qq{N� S�rie};
$mess[2905] = qq{Date d'achat};
$mess[2906] = qq{Date Contr�le};
$mess[2907] = qq{Mod�le};
$mess[2908] = qq{Marque};
$mess[2909] = qq{�l�vation};
$mess[2910] = qq{�nergie};
$mess[2911] = qq{Poids};
$mess[2912] = qq{Longueur};
$mess[2913] = qq{Largeur};
$mess[2914] = qq{Capacit�};
$mess[2915] = qq{�tat};
$mess[2916] = qq{Probabilit� de location en %};
$mess[2917] = qq{Agence};
$mess[2918] = qq{Dernier contrat auquel cette machine a �t� attribu�e};
$mess[2919] = qq{N� Parc};
$mess[2920] = qq{N� S�rie};
$mess[2921] = qq{Mod�le Machine};
$mess[2922] = qq{Famille machine};
$mess[2923] = qq{Fournisseur};
$mess[2924] = qq{�l�vation ou Pression};
$mess[2925] = qq{�nergie};
$mess[2926] = qq{�tat machine};
$mess[2927] = qq{Parc};
$mess[2928] = qq{Agence};
$mess[2929] = qq{Pays};
$mess[2930] = qq{Dernier contrat portant sur cette machine};
$mess[2931] = qq{Historique des contrats};
$mess[2932] = qq{Voir l'historique};
$mess[2933] = qq{%d derniers :};
$mess[2934] = qq{Aucun contrat en France};
$mess[2935] = qq{Liste machine globale};
$mess[2936] = qq{R�gion};
#chantier.cfg
$mess[2950] = qq{Modification des Informations du Chantier};
$mess[2951] = qq{Num�ro};
$mess[2952] = qq{N� Contrat};
$mess[2953] = qq{Client};
$mess[2954] = qq{Libell� Chantier};
$mess[2955] = qq{Adresse Chantier};
$mess[2956] = qq{Ville};
$mess[2957] = qq{Contact};
$mess[2958] = qq{T�l�phone};
$mess[2959] = qq{Hauteur};
$mess[2960] = qq{Surface};
$mess[2961] = qq{Heure de Liv.};
$mess[2962] = qq{Heure de R�cup.};
$mess[2963] = qq{Commentaire};
$mess[2964] = qq{N� Contrat};
$mess[2965] = qq{Client};
$mess[2966] = qq{Libell� Chantier};
$mess[2967] = qq{Ville Chantier};
#infoclient.cfg
$mess[3000] = qq{Modification des Informations du Client};
$mess[3001] = qq{Num�ro};
$mess[3002] = qq{Code Client};
$mess[3003] = qq{APE};
$mess[3004] = qq{Siret};
$mess[3005] = qq{Libell�};
$mess[3006] = qq{Adresse};
$mess[3007] = qq{Adresse comp.};
$mess[3008] = qq{Ville};
$mess[3009] = qq{T�l.};
$mess[3010] = qq{Fax};
$mess[3011] = qq{Portable};
$mess[3012] = qq{Email};
$mess[3013] = qq{Contact};
$mess[3014] = qq{Collectif};
$mess[3015] = qq{LI_NO};
$mess[3016] = qq{Banque};
$mess[3017] = qq{Type Paiement};
$mess[3018] = qq{RIB};
$mess[3019] = qq{Nb. Facture(s)};
$mess[3020] = qq{Facturation Locale};
$mess[3021] = qq{Facture non group�e};
$mess[3022] = qq{Commentaire};
$mess[3023] = qq{Activit�};
$mess[3024] = qq{Grille de base};
$mess[3025] = qq{Sommeil/Bloqu�};
$mess[3026] = qq{Assurance};
$mess[3027] = qq{N� commande obligatoire};
$mess[3028] = qq{BC � fournir};
$mess[3029] = qq{Paiement avant livraison};
$mess[3030] = qq{Conditions g�n�rales de vente};
$mess[3031] = qq{Facturation du nettoyage};
$mess[3032] = qq{Tarif personnalis�};
$mess[3033] = qq{Oui};
$mess[3034] = qq{Non};
$mess[3035] = qq{Date d'application de l'assurance souhait�e.};
$mess[3036] = qq{Le SIRET n'est pas renseign�};
$mess[3037] = qq{Format de SIRET non valide};
$mess[3038] = qq{Le SIRET est d�j� utilis�};
$mess[3039] = qq{SIRET compl�mentaire};

$mess[3040] = qq{Code Client};
$mess[3041] = qq{Libell� Client};
$mess[3042] = qq{Ville};
$mess[3043] = qq{D�partement};
$mess[3044] = qq{Siret};
$mess[3045] = qq{Grille de base};
$mess[3046] = qq{Bons de livr./r�cup. obligatoires};

$mess[3047] = qq{Facturation de la participation au recyclage};
$mess[3048] = qq{Qualification};
$mess[3060] = qq{Participation au recyclage};

$mess[3049] = qq{Le champ "Facturation Locale" a �t� modifi�. Veuillez contacter le 5000 pour que les actions n�cessaires soient effectu�es};

#anncl.cfg
$mess[3050] = qq{Suppression de Client};
$mess[3051] = qq{Client erron�};
$mess[3052] = qq{Client correct};
$mess[3053] = qq{Calculer};
# Assurance souhait�e
$mess[3054] = qq{Assurance souhait�e};
# Assurance actuelle
$mess[3055] = qq{Assurance actuelle};
# Gestion de recours actuelle
$mess[3056] = qq{Gestion de recours};
# Gestion de recours souhait�e
$mess[3057] = qq{Gestion de recours souhait�e};
$mess[3058] = qq{Date d'application souhait�e.};
$mess[3059] = qq{Non facturable};

#chclient.cfg
$mess[3100] = qq{Changement Client/Contrat};
$mess[3101] = qq{Mauvais Client sur Contrat};
$mess[3102] = qq{Bon Client};
$mess[3103] = qq{Valider};
$mess[3104] = qq{Attention, sur les deux clients, les informations suivantes sont diff�rentes (elles doivent �tre mises � jour <span style="text-decoration:underline;">manuellement</span>) :};
$mess[3105] = qq{ATTENTION, � la validation les tarifs de location et transport sont <span style="text-decoration:underline;">automatiquement maintenus suivant les conditions du premier client</span>};
$mess[3106] = qq{L�assurance du contrat n�est pas compatible avec l�assurance du nouveau client :};
$mess[3107] = qq{Un probl�me est survenu lors du changement de client sur le contrat.};
#chmach.cfg
$mess[3150] = qq{Changement Machine/Contrat};
$mess[3151] = qq{Contrat/N� Parc};
$mess[3152] = qq{Bonne Machine (Disponible)};
$mess[3153] = qq{Valider};
$mess[3154] = qq{Vous avez d�j� valid�};
$mess[3155] = qq{La machine est chang�e};
$mess[3156] = qq{Le Type de machine est chang�};
$mess[3157] = qq{Le contrat est annul�};
$mess[3158] = qq{Le Devis est saisie};
$mess[3159] = qq{Impossible de faire le changement, la tourn�e de livraison est point�e effectu�e};
$mess[3160] = qq{Impossible de faire le changement, le typage du mod�le de machines du contrat est diff�rent de celui de la machine saisie};
#contcourt.cfg
$mess[3200] = qq{Remise Contrat en Cours};
$mess[3201] = qq{Contrat Arr�t�};
$mess[3202] = qq{Valider};
$mess[3203] = qq{N'oubliez pas d'enregistrer la note avant d'annuler un contrat.};
#saisiemachine.cfg
$mess[3250] = qq{Saisie Machine};
$mess[3251] = qq{Recherche du Mod�le};
$mess[3252] = qq{N� de Parc};
$mess[3253] = qq{N� de S�rie};
$mess[3254] = qq{Date Apave};
$mess[3255] = qq{Calculer};
#sousloc.cfg
$mess[3300] = qq{Sous Location};
$mess[3301] = qq{Clients};
$mess[3302] = qq{Ville Chantier};
$mess[3303] = qq{Libell� Chantier};
$mess[3304] = qq{Adresse Chantier};
$mess[3305] = qq{Contact Chantier};
$mess[3306] = qq{T�l. Chantier};
$mess[3307] = qq{Contact Commande};
$mess[3308] = qq{T�l. Contact Commande};
$mess[3309] = qq{Fournisseur};
$mess[3310] = qq{Mod�le machine};
$mess[3311] = qq{Date D�part};
$mess[3312] = qq{Date Retour};
$mess[3313] = qq{Prix Jour};
$mess[3314] = qq{Dur�e Mensuelle};
$mess[3315] = qq{Prix Mois};
$mess[3316] = qq{Montant Forfaitaire};
$mess[3317] = qq{Assurance Comprise};
$mess[3318] = qq{Transport Aller};
$mess[3319] = qq{Enl�vement sur place Aller};
$mess[3320] = qq{Transport Retour};
$mess[3321] = qq{Enl�vement sur place Retour};
$mess[3322] = qq{Quai};
$mess[3323] = qq{Calculer};
$mess[3324] = qq{Machine suppl�mentaire Aller};
$mess[3325] = qq{Machine suppl�mentaire Retour};
#tableau.cfg
$mess[3350] = qq{Tableau};
$mess[3351] = qq{Machine(s) lou�e(s)};
$mess[3352] = qq{Machine(s) en livraison};
$mess[3353] = qq{Machine(s) en r�cup�ration};
$mess[3354] = qq{Machine(s) en };
$mess[3355] = qq{Mod�le};
$mess[3356] = qq{Famille};
$mess[3357] = qq{Hauteur};
$mess[3358] = qq{�nergie};
$mess[3359] = qq{N� Parc};
$mess[3360] = qq{N� Contrat};
$mess[3361] = qq{Client};
$mess[3362] = qq{Chantier};
$mess[3363] = qq{C.P.};
$mess[3364] = qq{Ville};
$mess[3365] = qq{Date de fin};
$mess[3366] = qq{POT};
$mess[3367] = qq{Tarif};
$mess[3368] = qq{Co�t};
$mess[3369] = qq{Ratio};
$mess[3370] = qq{Dur�e};
$mess[3380] = qq{Enl�vement};
$mess[3381] = qq{R�cup�ration};
$mess[3382] = qq{Transfert inter-chantiers};
$mess[3383] = qq{Reprise sur chantier};

$mess[3410] = qq{Num�ro};
$mess[3411] = qq{Contrat};
$mess[3412] = qq{Date d�but};
$mess[3413] = qq{Date fin};
$mess[3414] = qq{N� de parc};
$mess[3415] = qq{Mod�le};
$mess[3416] = qq{Chantier};
$mess[3417] = qq{Code postal};
$mess[3418] = qq{Ville};
#nomade.cfg
$mess[3450] = qq{Changement Agence};
$mess[3451] = qq{Agence destinataire };
$mess[3452] = qq{Vous &ecirc;tes &aacute;};
$mess[3453] = qq{Se t�l�porter !};
#statms.cfg
$mess[3500] = qq{Chiffres du mois};
#stat1.cfg
$mess[3550] = qq{Chiffres du mois 2};
#stat2.cfg
$mess[3600] = qq{Chiffres globaux};
#hisstat.cfg
$mess[3650] = qq{Historique des Chiffres};
$mess[3651] = qq{Mois};
$mess[3652] = qq{Ann�e};
$mess[3653] = qq{Janvier};
$mess[3654] = qq{F�vrier};
$mess[3655] = qq{Mars};
$mess[3656] = qq{Avril};
$mess[3657] = qq{Mai};
$mess[3658] = qq{Juin};
$mess[3659] = qq{Juillet};
$mess[3660] = qq{Ao�t};
$mess[3661] = qq{Septembre};
$mess[3662] = qq{Octobre};
$mess[3663] = qq{Novembre};
$mess[3664] = qq{D�cembre};
$mess[3665] = qq{5};
$mess[3666] = qq{2000};
$mess[3667] = qq{2001};
$mess[3668] = qq{2002};
$mess[3669] = qq{2003};
$mess[3670] = qq{2004};
#validagag.cfg
$mess[3700] = qq{Validation Agence � Agence};
$mess[3701] = qq{Machine};
$mess[3702] = qq{Machine(s)};
#transfagag.cfg
$mess[3750] = qq{Transfert Agence � Agence};
$mess[3751] = qq{Machine};
$mess[3752] = qq{Camion};
$mess[3753] = qq{Chauffeur};
$mess[3754] = qq{Agence destinataire};
$mess[3755] = qq{Machine(s)};
$mess[3756] = qq{Camion(s)};
$mess[3757] = qq{Chauffeur(s)};
$mess[3758] = qq{Agences};
#camcha.cfg
$mess[3800] = qq{Affectation Camion/Chauffeur};
$mess[3801] = qq{Camion};
$mess[3802] = qq{Chauffeur};
$mess[3803] = qq{Couple Camion/Chauffeur};
$mess[3804] = qq{Camion(s)};
$mess[3805] = qq{Chauffeur(s)};
$mess[3806] = qq{Couple(s)};
#transfert.cfg
$mess[3850] = qq{Transfert de chantier � chantier};
$mess[3851] = qq{Transport};
$mess[3852] = qq{Transport};
$mess[3853] = qq{N� Parc};
$mess[3854] = qq{Num�ro};
$mess[3855] = qq{N� Contrat};
$mess[3856] = qq{Client};
$mess[3857] = qq{Adresse};
$mess[3858] = qq{C.P.};
$mess[3859] = qq{Ville};
$mess[3860] = qq{D�signation Machine};
$mess[3861] = qq{N� Parc};
#carbu.cfg
$mess[3900] = qq{Carburant};
$mess[3901] = qq{Contrat, Client, D�signation, N� Parc};
$mess[3902] = qq{Quantit� Carburant (L)};
#verif.cfg
$mess[3950] = qq{V�rification Machine};
$mess[3951] = qq{Machine};
#Contr�le Apave
$mess[4000] = qq{Contr�le Machine};
$mess[4001] = qq{N� Parc};
$mess[4002] = qq{Mod�le Machine};
$mess[4003] = qq{Famille machine};
$mess[4004] = qq{�l�vation ou Pression};
$mess[4005] = qq{�nergie};
$mess[4006] = qq{�tat machine};
$mess[4007] = qq{D�signation};
$mess[4008] = qq{D�signation};
$mess[4009] = qq{N� Parc};
$mess[4010] = qq{N� S�rie};
$mess[4011] = qq{Mod�le};
$mess[4012] = qq{Marque};
$mess[4013] = qq{�l�vation};
$mess[4014] = qq{�nergie};
$mess[4015] = qq{Poids};
$mess[4016] = qq{Longueur};
$mess[4017] = qq{Largeur};
$mess[4018] = qq{Capacit�};
$mess[4019] = qq{�tat};
$mess[4020] = qq{Date Appave};
$mess[4021] = qq{Effectu�};
$mess[4022] = qq{Num};
#Consultation Devis
$mess[4050] = qq{Consultation Devis};
$mess[4051] = qq{N� Devis};
$mess[4052] = qq{Client};
$mess[4053] = qq{�tat Devis};
$mess[4054] = qq{Agent};
$mess[4055] = qq{N� Devis};
$mess[4056] = qq{Client};
$mess[4057] = qq{Lib. Chantier};
$mess[4058] = qq{Date};
$mess[4059] = qq{D�signation};
$mess[4060] = qq{Prix Jour};
$mess[4061] = qq{Dur�e};
$mess[4062] = qq{C.T};
$mess[4063] = qq{Montant};
$mess[4064] = qq{Pass� en contrat};
$mess[4065] = qq{�tat};
$mess[4066] = qq{Agent};
#Remise en cours de contrat
$mess[4100] = qq{Remise Contrat en Cours};
$mess[4101] = qq{Contrat Arr�t�};
$mess[4102] = qq{Valider};
#Cr�ation de machine
$mess[4150] = qq{Saisie Machine};
$mess[4151] = qq{Recherche du Mod�le};
$mess[4152] = qq{N� Parc};
$mess[4153] = qq{N� S�rie};
$mess[4154] = qq{Date Apave};
$mess[4155] = qq{Agence de cr�ation};
$mess[4156] = qq{Calculer};
#Facturation
$mess[4200] = qq{Facturation};
$mess[4201] = qq{N� Contrat};
$mess[4202] = qq{Date de Fin};
$mess[4203] = qq{Agence};
$mess[4204] = qq{Contact};
$mess[4205] = qq{T�l. Agence};
$mess[4206] = qq{Agence};
$mess[4207] = qq{� arr�ter};
$mess[4208] = qq{Carburant � saisir};
$mess[4209] = qq{� passer en facturation};
$mess[4210] = qq{T�l. Agence};
$mess[4211] = qq{R�capitulatif};
$mess[4212] = qq{Facturation hebdomadaire};
$mess[4213] = qq{Facturation mensuelle};
$mess[4214] = qq{Date de d�but};
$mess[4215] = qq{Date de cr�ation};
$mess[4216] = qq{Contrats anticip�s};
#Energies
$mess[4250] = qq{Bi-�nergie};
$mess[4251] = qq{Diesel};
$mess[4252] = qq{�lectrique};
$mess[4253] = qq{Manivelle};
$mess[4254] = qq{Gaz};
#Familles
$mess[4300] = qq{Boom};
$mess[4301] = qq{Chariot};
$mess[4302] = qq{Ciseau};
$mess[4303] = qq{Fl�che};
$mess[4304] = qq{Fl�che Chenille};
$mess[4305] = qq{Monte-charge};
$mess[4306] = qq{Mat droit};
$mess[4307] = qq{Push};
$mess[4308] = qq{Compacteur};
$mess[4309] = qq{Chariot Rotatif};
$mess[4310] = qq{Divers};
#Etat
$mess[4350] = qq{Contr�le};
$mess[4351] = qq{Disponible};
$mess[4352] = qq{Lou�e};
$mess[4353] = qq{Panne};
$mess[4354] = qq{R�cuperation};
$mess[4355] = qq{R�vision};
$mess[4356] = qq{V�rification};

$mess[4357] = qq{Vendue};
$mess[4358] = qq{En Transfert};
$mess[4359] = qq{Rendue};
$mess[4360] = qq{Restitu�};

$mess[4361] = qq{A r�cup�rer};

#Ann�es
$mess[4400] = qq{2001};
$mess[4401] = qq{2000};
$mess[4402] = qq{1999};
$mess[4403] = qq{1998};
#Info client
$mess[4450] = qq{Informations Client};
$mess[4451] = qq{Nom Client};
$mess[4452] = qq{Adresse};
$mess[4453] = qq{SIRET};
$mess[4454] = qq{APE};
$mess[4455] = qq{Secteur d'activit�};
$mess[4456] = qq{Contact client};
$mess[4457] = qq{T�l�phone};
$mess[4458] = qq{Fax};
$mess[4459] = qq{Portable};
$mess[4460] = qq{Adresse mail};
$mess[4461] = qq{Commentaires};
$mess[4462] = qq{Statistiques};
$mess[4463] = qq{Agence(s)};
$mess[4476] = qq{Total};
$mess[4483] = qq{Fermer};

# stati.cgi
#entete
$mess[5000] = qq{Nbr. contrats};
$mess[5001] = qq{Tx util.<br>J+1};
$mess[5002] = qq{TOT};
$mess[5003] = qq{ARR};
$mess[5004] = qq{ACT};
$mess[5005] = qq{ASS<br>FACT.};
$mess[5006] = qq{CT<5j};
$mess[5007] = qq{MENS};
$mess[5008] = qq{FORF};
$mess[5009] = qq{Arr�te au J=};
$mess[5010] = qq{Arr�te � la fin du mois};
$mess[5011] = qq{CA};
$mess[5012] = qq{TRANSP};
$mess[5013] = qq{CARB};
$mess[5014] = qq{COUT};
$mess[5015] = qq{CT Jr};
#titre groupe
$mess[5016] = qq{Parc};
$mess[5017] = qq{JR Loc};
$mess[5018] = qq{% Util};
$mess[5019] = qq{Px Jr};
$mess[5020] = qq{COUT};
$mess[5021] = qq{CT Jr};
#tittre bloc
$mess[5022] = qq{Fam. Comptable};
$mess[5023] = qq{Fam. Commerciale};
#date
$mess[5024] = qq{MOIS EN COURS};
$mess[5025] = qq{LOC};
$mess[5026] = qq{j. o.};
# Autres
$mess[5027] = qq{LIVR};
# BC � envoyer/r�ceptionner
$mess[5050] = qq{BC client � fournir};
$mess[5051] = qq{N� de Contrat};
$mess[5052] = qq{Ville Chantier};
$mess[5053] = qq{Client};
$mess[5054] = qq{N� T�l};
$mess[5055] = qq{Mod�le Machine};
$mess[5056] = qq{N� de Parc};
$mess[5057] = qq{Date de D�but};
$mess[5058] = qq{Date de Fin};
$mess[5059] = qq{Valid�};
$mess[5060] = qq{En attente};
# Paiement � la livraison
$mess[5600] = qq{Paiement � la livraison};
$mess[5601] = qq{N� de Contrat};
$mess[5602] = qq{Ville Chantier};
$mess[5603] = qq{Client};
$mess[5604] = qq{N� T�l};
$mess[5605] = qq{Mod�le Machine};
$mess[5606] = qq{N� de Parc};
$mess[5607] = qq{Date de D�but};
$mess[5608] = qq{Date de Fin};
$mess[5609] = qq{Montant acompte};
$mess[5610] = qq{N� acompte};
$mess[5611] = qq{Date versement};
$mess[5612] = qq{Type};
# Paiement � la livraison
$mess[5650] = qq{Paiement � la livraison};
$mess[5651] = qq{N� de Contrat};
$mess[5652] = qq{Client};
$mess[5653] = qq{Code Client};
$mess[5654] = qq{N� T�l};
$mess[5655] = qq{Date de D�but};
$mess[5656] = qq{Date de Fin};
$mess[5657] = qq{Montant acompte};
$mess[5658] = qq{N� acompte};
$mess[5659] = qq{Date versement};
$mess[5660] = qq{Type de paiement};
$mess[5661] = qq{Valid�};
# Devis (nouvelle formule)
# En-t�te
$mess[5800] = qq{Devis};
$mess[5801] = qq{Stock};
$mess[5802] = qq{Clients};
$mess[5803] = qq{Ville Chantier};
$mess[5804] = qq{Libell� Chantier};
$mess[5805] = qq{Adresse Chantier};
$mess[5806] = qq{Contact Chantier};
$mess[5807] = qq{T�l. Chantier};
$mess[5808] = qq{N� Commande};
# Colonnes
$mess[5820] = qq{Prix objectif};
$mess[5821] = qq{Co�t/jour};
$mess[5822] = qq{Ratio};
$mess[5823] = qq{% util};
$mess[5824] = qq{Quantit�};
$mess[5825] = qq{Type Montant};
$mess[5826] = qq{Dur�e};
$mess[5827] = qq{Date Pr�visionnelle};
$mess[5828] = qq{Prix Jour/Mois/Forfait};
$mess[5829] = qq{Ratio};
$mess[5830] = qq{Montant};
$mess[5831] = qq{Transport Aller};
$mess[5832] = qq{Transport Retour};
# Totaux
$mess[5850] = qq{Assurance};
$mess[5851] = qq{Montant Assurance};
$mess[5852] = qq{Montant Total};
# Pied
$mess[5860] = qq{Commentaire};
$mess[5861] = qq{Notes Commerciales (ne figurent pas sur le devis)};
$mess[5862] = qq{Devis Accept�};
$mess[5863] = qq{Calculer};
$mess[5864] = qq{Valider};
# Infos ratio Pr�-contrat
$mess[5900] = qq{Taux d'utilisation};
$mess[5901] = qq{Dur�e};
$mess[5902] = qq{Co�t/jour};
$mess[5903] = qq{Ratio objectif};
$mess[5904] = qq{Prix objectif/jour};
$mess[5905] = qq{Prix objectif/mois};
$mess[5906] = qq{Ratio};
# L�gende clients
$mess[6000] = qq{red};
$mess[6001] = '#'.qq{00CC33};
#$mess[6001] = qq{green};
$mess[6010] = qq{Client en rouge};
$mess[6011] = qq{Client en vert};
$mess[6020] = qq{Client n'ayant pas fait de chiffre d'affaires depuis plus de 14 mois};
$mess[6021] = qq{Client bloqu� en cours};
$mess[6022] = qq{Client � d�bloquer pour prolonger le contrat};
# Machine hors parc
$mess[6050] = qq{Parc normal};
$mess[6051] = qq{Hors parc};
#modifiarret modif2.cfg
$mess[6100] = qq{Modification/Arr�t de contrat};
$mess[6101] = qq{Num�ro};
$mess[6102] = qq{�tat Contrat};
$mess[6103] = qq{N� Contrat};
$mess[6104] = qq{Indice};
$mess[6105] = qq{Client};
$mess[6106] = qq{Libell� Chantier};
$mess[6107] = qq{Ville Chantier};
$mess[6108] = qq{Contact chantier};
$mess[6109] = qq{T�l�phone chantier};
$mess[6110] = qq{Contact commande};
$mess[6111] = qq{T�l�phone contact commande};
$mess[6112] = qq{D�signation Machine};
$mess[6113] = qq{N� Parc};
$mess[6114] = qq{Hors parc};
$mess[6115] = qq{Date D�part};
$mess[6116] = qq{Date Retour};
$mess[6117] = qq{Dur�e};
$mess[6118] = qq{Jour en plus};
$mess[6119] = qq{Jour en moins};
$mess[6120] = qq{Prix Jour};
$mess[6121] = qq{Prix Mois};
$mess[6122] = qq{Forfait};
$mess[6123] = qq{Assurance Comprise};
$mess[6124] = qq{Montant Assurance};
$mess[6125] = qq{Transport Aller};
$mess[6126] = qq{Enl�vement Aller};
$mess[6127] = qq{Transport Retour};
$mess[6128] = qq{Enl�vement Retour};
$mess[6129] = qq{Quai};
$mess[6130] = qq{Nettoyage};
$mess[6131] = qq{Libell� du service};
$mess[6132] = qq{Montant service};
$mess[6133] = qq{Montant Total HT};
$mess[6134] = qq{Commande};
$mess[6135] = qq{Commentaire};
$mess[6136] = qq{Note};
$mess[6137] = qq{Paiement � la livraison};
$mess[6138] = qq{Client};
$mess[6139] = qq{N� Parc};
$mess[6140] = qq{Ce contrat a d�j� �t� arr�t�};
$mess[6141] = qq{Le Type de machine est chang�};
$mess[6142] = qq{Le contrat est annul�};
$mess[6143] = qq{Accessoires li�s au contrat};
$mess[6144] = qq{Accessoire (N� Parc)};
$mess[6145] = qq{Contrat (�tat)};
$mess[6146] = qq{Montant};
$mess[6147] = qq{Date D�part};
$mess[6148] = qq{Date Retour};
$mess[6149] = qq{Machine lou�e sans l'accessoire par d�faut};
$mess[6150] = qq{Contrat d'origine};
# Contrat imprim�
$mess[6200] = qq{Location jour HT};
$mess[6201] = qq{Location mensuelle HT};
$mess[6202] = qq{Location forfaitaire HT};
# Assurance recherche rapide
$mess[6300] = qq{Factur�e};
$mess[6301] = qq{Non factur�e};
$mess[6302] = qq{Par le client};
$mess[6303] = qq{Incluse};
# BL-BR
$mess[6400] = qq{Aucune machine n'est attribu�e sur le contrat n� numcont.<BR>Vous devez l'attribuer pour finaliser la tourn�e.};
$mess[6401] = qq{Aucune machine n'est attribu�e sur les contrats n� numcont.<BR>Vous devez les attribuer pour finaliser la tourn�e.};
# Modification/Arr�t nouvelle formule
$mess[6500] = qq{Modification de contrat};
# Version tableau
# Colonnes
$mess[6510] = qq{N� contrat};
$mess[6511] = qq{Etat};
$mess[6512] = qq{Client};
$mess[6513] = qq{Chantier};
$mess[6514] = qq{Contact commande};
$mess[6515] = qq{Contact chantier};
$mess[6516] = qq{Machine};
$mess[6517] = qq{Dur�e};
$mess[6518] = qq{Jours en plus};
$mess[6519] = qq{Jours en moins};
$mess[6520] = qq{Prix};
$mess[6521] = qq{Assurance};
$mess[6522] = qq{Transport};
$mess[6523] = qq{Enl�vement};
$mess[6524] = qq{Quai};
$mess[6525] = qq{Montant HT};
$mess[6526] = qq{N� commande};
$mess[6527] = qq{Commentaire};
$mess[6528] = qq{Note};
# Autres
$mess[6550] = qq{hors parc};
$mess[6551] = qq{Aller};
$mess[6552] = qq{Retour};
$mess[6553] = qq{/ jour};
$mess[6554] = qq{/ mois};
$mess[6555] = qq{forfait};
$mess[6556] = qq{jour};
$mess[6557] = qq{jours};
# Affichage contrat
$mess[6600] = qq{Client};
$mess[6601] = qq{Nom};
$mess[6602] = qq{Grille de base};
$mess[6603] = qq{Contact commande};
$mess[6604] = qq{Chantier};
$mess[6605] = qq{Libell�};
$mess[6606] = qq{Ville};
$mess[6607] = qq{Contact chantier};
$mess[6608] = qq{Machine};
$mess[6609] = qq{D�signation};
$mess[6610] = qq{Energie};
$mess[6611] = qq{N� de parc};
$mess[6612] = qq{Hors parc};
$mess[6613] = qq{Taux d'utilisation};
$mess[6614] = qq{Co�t};
$mess[6615] = qq{Ratio objectif};
$mess[6616] = qq{Prix objectif};
$mess[6617] = qq{Ratio};
$mess[6618] = qq{Dur�e};
$mess[6619] = qq{Date d�part};
$mess[6620] = qq{Date retour};
$mess[6621] = qq{soit};
$mess[6622] = qq{jour};
$mess[6623] = qq{jours};
$mess[6624] = qq{ouvrable};
$mess[6625] = qq{ouvrables};
$mess[6626] = qq{calendaire};
$mess[6627] = qq{calendaires};
$mess[6628] = qq{Jours en plus};
$mess[6629] = qq{Jours en moins};
$mess[6630] = qq{Tarif de base};
$mess[6631] = qq{Prix};
$mess[6632] = qq{par jour};
$mess[6633] = qq{par mois};
$mess[6634] = qq{forfait};
$mess[6635] = qq{Assurance};
$mess[6636] = qq{Transport};
$mess[6637] = qq{Aller};
$mess[6638] = qq{Retour};
$mess[6639] = qq{Enl�vement};
$mess[6640] = qq{Quai};
$mess[6641] = qq{Services compl�mentaires};
$mess[6642] = qq{Ajouter un service};
$mess[6643] = qq{Supprimer un service};
$mess[6644] = qq{Montant total HT};
$mess[6645] = qq{Informations compl�mentaires};
$mess[6646] = qq{N� de commande};
$mess[6647] = qq{Commentaire};
$mess[6648] = qq{Note};
$mess[6649] = qq{Recalculer};
$mess[6650] = qq{Modifier le contrat};
$mess[6651] = qq{Arr�ter le contrat};
$mess[6652] = qq{Passer le contrat en facturation};
$mess[6653] = qq{Contrat d'origine};
$mess[6654] = qq{Accessoires};
$mess[6655] = qq{du};
$mess[6656] = qq{au};
$mess[6657] = qq{/ jour};
$mess[6658] = qq{Supprim�};
$mess[6659] = qq{Factur�};
$mess[6660] = qq{Tarif};
$mess[6661] = qq{Divers};
$mess[6662] = qq{Factur� en fin de contrat};
$mess[6663] = qq{Veuillez choisir un service dans la liste d�roulante};
$mess[6664] = qq{Enti�rement factur�};
$mess[6665] = qq{Factur� partiellement jusqu'au};
$mess[6666] = qq{Non factur�};
$mess[6667] = qq{Visualiser le tarif};
$mess[6668] = qq{Carburant};
$mess[6669] = qq{Quantit�};
$mess[6670] = qq{litre};
$mess[6671] = qq{litres};
$mess[6672] = qq{Montant};
$mess[6673] = qq{Fournisseur};
$mess[6674] = qq{d�j� factur�};
$mess[6675] = qq{d�j� factur�s};
$mess[6676] = qq{Livraison non planifi�e ou non point�e};
$mess[6677] = qq{Remise};
$mess[6678] = qq{Remise exceptionnelle};
# Messages de confirmation
$mess[6750] = qq{Impossible d'arr�ter le contrat plus de 4 jours avant la date de retour};
$mess[6751] = qq{Le contrat a �t� arr�t�};
$mess[6752] = qq{Le contrat fils a �t� arr�t�};
$mess[6753] = qq{Le contrat est pr�t � �tre factur� et ne peut plus �tre modifi�};
$mess[6754] = qq{Le contrat fils est pr�t � �tre factur�};
$mess[6755] = qq{Le contrat fils a �t� mis � jour};
$mess[6756] = qq{Le contrat a �t� mis � jour};
$mess[6757] = qq{Date de retour ant�rieure � la date de derni�re facturation};
$mess[6758] = qq{Date de retour incorrecte};
$mess[6759] = qq{Date de d�part incorrecte};
$mess[6760] = qq{Date de retour ant�rieure � la date de d�part};
$mess[6761] = qq{Impossible de passer le contrat en facturation car le carburant n'a pas �t� saisi};
$mess[6762] = qq{Impossible de passer le contrat en facturation car le transport retour n'a pas �t� planifi�};
$mess[6763] = qq{Impossible de passer le contrat en facturation car la remise exceptionnelle n'est pas valid�e};
$mess[6764] = qq{Seuls les jours en plus et les services compl�mentaires dont le montant n'est pas n�gatif peuvent �tre saisis};
$mess[6765] = qq{Vous ne pouvez pas saisir de service dont le montant est n�gatif};
$mess[6766] = qq{Montant du transport inf�rieur au forfait machine suppl�mentaire minimum.};
$mess[6767] = qq{Montant du service inf�rieur au montant minimum autoris� : };

# Remise en cours de contrat (nouvelle version)
$mess[6800] = qq{Remise en cours de contrat};
# Recherche
$mess[6810] = qq{Contrat � remettre en cours};
$mess[6811] = qq{Remettre en cours le contrat};
# Messages validation
$mess[6820] = qq{Le contrat a �t� remis en cours};
$mess[6821] = qq{Veuillez s�lectionner un contrat � remettre en cours};
$mess[6822] = qq{Le contrat a d�j� �t� remis en cours};

# Documents types
$mess[6900] = qq{Documents types};
$mess[6901] = qq{T�l�charger};
$mess[6910] = qq{R�pertoire};
$mess[6911] = qq{Type de document inconnu};
$mess[6912] = qq{Document Microsoft Word};
$mess[6913] = qq{Document Microsoft Excel};
$mess[6914] = qq{Document Acrobat PDF};
$mess[6915] = qq{Image JPEG};
$mess[6916] = qq{Image GIF};
$mess[6917] = qq{Image BMP};
$mess[6918] = qq{Image TIFF};
$mess[6919] = qq{Document Microsoft PPT};

# Remise exceptionnelle
$mess[6950] = qq{Remise exceptionnelle};
$mess[6951] = qq{Prix avant remise};
$mess[6952] = qq{Remise exceptionnelle};
$mess[6953] = qq{Prix remis�};
$mess[6954] = qq{Justification };
$mess[6955] = qq{Libell� facture};
$mess[6956] = qq{Remise accept�e};
$mess[6957] = qq{Remise en attente de validation};
$mess[6958] = qq{Remise refus�e};
$mess[6959] = qq{Remise exceptionnelle de };   # Lib impression contrat
$mess[6960] = qq{Remise exceptionnelle de };   # Lib impression devis
$mess[6961] = qq{demand�};
$mess[6962] = qq{Trait�e par};
$mess[6963] = qq{Limit� � 36 caract�res};
$mess[6964] = qq{Remise sup�rieure � 100 %};
$mess[6965] = qq{Prix net};

# Modification du tarif
$mess[7000] = qq{Tarif de base};
$mess[7001] = qq{Prix par jour HT};
$mess[7002] = qq{TARIF DE BASE};
$mess[7003] = qq{Avril 2006};
$mess[7004] = qq{�};
$mess[7005] = qq{plus de};

# Chargement de la page
$mess[7050] = qq{Chargement de la page en cours};
$mess[7051] = qq{Chargement de la page};

# Modification commande transport
$mess[7100] = qq{Modification de commande de transport externe};
$mess[7101] = qq{Pointage de commande de transport externe};
#
$mess[7110] = qq{N� commande};
$mess[7111] = qq{Sous-traitant};
$mess[7112] = qq{N� contrat};
$mess[7113] = qq{Date de transport};
$mess[7114] = qq{Consulter la commande};
$mess[7115] = qq{Enregistrer les modifications};
$mess[7116] = qq{Attention};
$mess[7118] = qq{Pas de ligne pour cette commande};
$mess[7119] = qq{Commande};
$mess[7120] = qq{Agence};
$mess[7121] = qq{�tabli par};
$mess[7123] = qq{Type};
$mess[7124] = qq{Date de pointage facture};
$mess[7125] = qq{N� facture};
$mess[7126] = qq{Montant factur�};
$mess[7127] = qq{Supprimer la commande};
$mess[7128] = qq{La commande a �t� supprim�e};
$mess[7129] = qq{Nouvelle recherche};
$mess[7130] = qq{La commande a �t� modifi�e};
$mess[7131] = qq{La date de transport n'est pas valide :};
$mess[7132] = qq{Commande non rapproch�e};
$mess[7133] = qq{Entre le};
$mess[7134] = qq{et le};
$mess[7135] = qq{=};
$mess[7136] = qq{&ge;};
$mess[7137] = qq{La date n'est pas valide :};
$mess[7138] = qq{Commandes point�es};
$mess[7139] = qq{Non};
$mess[7140] = qq{Oui};
$mess[7141] = qq{Indiff�rent};

# Contrat cadre
$mess[7150] = qq{Contrat Cadre};

# Suffixe Sous-location et Full service
$mess[7200] = qq{SL};
$mess[7201] = qq{FS};

# Commandes de transport (nouvelle interface)
$mess[7300] = qq{Nouvelle commande};
$mess[7301] = qq{Ajouter une nouvelle commande};
$mess[7302] = qq{Cocher/d�cocher tous};
$mess[7303] = qq{Valider les modifications};
$mess[7304] = qq{Imprimer le bon de commande};
$mess[7305] = qq{Tourn�e};
$mess[7306] = qq{Cr�er la tourn�e};
$mess[7307] = qq{Veuillez choisir un sous-traitant};
$mess[7308] = qq{La commande #XXX# est cr��e};
$mess[7309] = qq{Les modifications sont enregistr�es};
$mess[7310] = qq{La tourn�e #XXX# a �t� cr��e};
$mess[7311] = qq{Il existe d'autres tourn�es pour le m�me transporteur qui ne pourront �tre ordonn�es que lorsque celle-ci le sera};
$mess[7312] = qq{Il n'y a actuellement aucun transport � attribuer};
$mess[7313] = qq{Actualiser};
$mess[7314] = qq{Retirer de la commande};
$mess[7315] = qq{La tourn�e est d�j� cr��e (n� #XXX#)};
$mess[7316] = qq{Impossible de modifier la commande n� #YYY# car il existe d�j� une tourn�e (n� #XXX#)};
$mess[7317] = qq{Attention, toute commande pass�e en tourn�e n'est pas modifiable};
$mess[7318] = qq{Attention, au moins un contrat en erreur (ligne en rouge) a une commande de transport. R�gulariser le(s) pour pouvoir finaliser la(les) commande(s) correspondante(s)};

# Remise en arr�t de contrat
$mess[7400] = qq{Remise en arr�t de contrat};
# Recherche
$mess[7410] = qq{Contrat � remettre en arr�t};
$mess[7411] = qq{Remettre en arr�t le contrat};
# Messages validation
$mess[7420] = qq{Le contrat a �t� remis en arr�t};
$mess[7421] = qq{Veuillez s�lectionner un contrat � remettre en arr�t};
$mess[7422] = qq{Le contrat a d�j� �t� remis en arr�t};
$mess[7423] = qq{Le contrat ne peut pas �tre remis en arr�t :};
$mess[7424] = qq{ - le contrat n'est pas dans l'�tat 'mis en facturation'};
$mess[7425] = qq{ - le contrat a d�j� �t� totalement factur�};
# L�gende
$mess[7430] = qq{Etat de facturation};
$mess[7431] = qq{Facturation Partielle};
$mess[7432] = qq{Facturation Partielle Finale (location enti�rement factur�e)};
$mess[7433] = qq{Facturation Finale (contrat enti�rement factur�)};

# Remise en cours du pointage d'une machine dans une tourn�e
$mess[7500] = qq{Remise en cours du pointage d'une machine dans une tourn�e};
$mess[7501] = qq{S�lection};
$mess[7502] = qq{Rechercher};
# Recherche
$mess[7510] = qq{Contrat};
$mess[7511] = qq{Machine};
$mess[7512] = qq{Echou�};
$mess[7513] = qq{Effectu�};
$mess[7514] = qq{R�attribu�};
$mess[7515] = qq{En cours};
$mess[7516] = qq{Pas attribu�};
$mess[7517] = qq{Remettre en cours le pointage};
# Messages validation
$mess[7520] = qq{Le pointage de la machine dans la tourn�e a �t� remis en cours};
$mess[7521] = qq{Veuillez saisir un n� de contrat et/ou une machine};
$mess[7522] = qq{Le pointage de la machine dans la tourn�e ne peut pas �tre remis en cours :};
$mess[7523] = qq{ - impossible sur un };
$mess[7524] = qq{ - pointage actuel : };
$mess[7525] = qq{Le pointage de la machine dans la tourn�e a d�j� �t� remis en cours};
$mess[7526] = qq{Veuillez s�lectionner une ligne de tourn�e � remettre en cours};
$mess[7527] = qq{ - l'�tat actuel de la machine ne le permet pas};
$mess[7528] = qq{ - le contrat est dans l'�tat "Mis en facturation"};
$mess[7529] = qq{ - le contrat est totalement factur�};
$mess[7540] = qq{ - possible uniquement sur le dernier contrat};
# Colonnes du tableau
$mess[7530] = qq{Tourn�e};
$mess[7531] = qq{Contrat};
$mess[7532] = qq{Etat contrat};
$mess[7533] = qq{Machine};
$mess[7537] = qq{Etat machine};
$mess[7534] = qq{Type};
$mess[7535] = qq{Pointage};
$mess[7536] = qq{Seules les lignes du dernier contrat de type : "Livraison" ou "R�cup�ration" et dont le pointage est : "Echou�" ou "Effectu�" sont s�lectionnables};

# Mise � jour des alertes et des stocks
$mess[7600] = qq{Mise � jour des alertes et des stocks};
$mess[7601] = qq{Pour effectuer les mises � jour sur l'agence de %s : };

# Visu des transports par r�gion
$mess[7620] = qq{Visualisation des transports sur la r�gion};
$mess[7621] = qq{R�-actualiser};
$mess[7622] = qq{OUI};
$mess[7623] = qq{non};

# Visu des transports par r�gion
$mess[7649] = qq{Taux d'utilisation au %s - };
$mess[7650] = qq{Taux d'utilisation au %s - R�gion };
$mess[7651] = qq{Famille tarifaire};
$mess[7652] = qq{% util. inst. (nb mach.)};
$mess[7653] = qq{R�gional};
$mess[7654] = qq{National};
$mess[7655] = qq{TOTAL};
$mess[7656] = qq{Vous n'avez pas acc�s � cette fonctionnalit�.};

# Report des libell�s de l'interface "Nouveau client"
$mess[7700] = qq{APE};
$mess[7701] = qq{Secteur d'activit�};
$mess[7702] = qq{SIRET};
$mess[7703] = qq{Libell�};
$mess[7704] = qq{Adresse};
$mess[7705] = qq{Cmpt. adresse};
$mess[7706] = qq{Ville};
$mess[7707] = qq{T�l�phone};
$mess[7708] = qq{Portable};
$mess[7709] = qq{Fax};
$mess[7710] = qq{Contact};
$mess[7711] = qq{Commentaire};
$mess[7712] = qq{Mode Paiement};
$mess[7713] = qq{Banque};
$mess[7714] = qq{RIB};
$mess[7715] = qq{Grille de base};
$mess[7716] = qq{Groupe};
$mess[7717] = qq{Qualification};
$mess[7718] = qq{Email Client};
$mess[7719] = qq{Cr�ateur};
$mess[7720] = qq{Agence de cr�ation};
$mess[7721] = qq{Toujours fournir les documents de la machine};
$mess[7800] = qq{P�rim�tre national};

# Transport et Pro forma
$mess[8000] = qq{L�gende pour les cas erron�s};
$mess[8001] = qq{Pro forma non g�n�r�e};
$mess[8002] = qq{Pro forma non � jour};
$mess[8003] = qq{Pro forma non finalis�e};
$mess[8004] = qq{Remise exceptionnelle de location en attente};
$mess[8005] = qq{Remise exceptionnelle de transport en attente};
$mess[8006] = qq{Client avec ouverture de compte en attente};

$mess[8100] = qq{Impossible de planifier le transport sur le contrat #XXX#};
$mess[8101] = qq{Erreur sur le contrat #XXX#};
$mess[8102] = qq{Le contrat comporte une facture pro forma ou une facture d'acompte qui ne sera plus valide apr�s confirmation};
$mess[8103] = qq{Le client a des factures pro formas ou des factures d'acomptes qui ne seront plus valides apr�s confirmation, sur les documents suivant};

return @mess;
}	

sub liste_es {
my @mess;
#message
#sub saisie
$mess[0] = qq{Seguro};
$mess[1] = qq{incluido};
$mess[2] = qq{B�squeda};
$mess[3] = qq{ no };
$mess[4] = qq{Buscar};
$mess[5] = qq{Rellenar este campo};
$mess[6] = qq{Cliente nuevo};
$mess[7] = qq{Rellenar el campo};
$mess[8] = qq{Rellenar UN Campo de tarifa mensual o diaria};
$mess[9] = qq{No informado};
$mess[10] = qq{Condiciones generales de venta que aprobar};
$mess[11] = qq{Enviar una apertura de cuenta};
$mess[12] = qq{Para atribuir un grupo contactar el responsable de control tarifario};
#sub traitment
$mess[50] = qq{La fecha de inicio es antes de hoy};
$mess[51] = qq{La fecha de cierre antes de la fecha de inicio};
$mess[52] = qq{Fecha err�nea !};
$mess[53] = qq{Fecha de principio anterior a hoy.\\n�Conservar esta fecha?};
$mess[54] = qq{N�mero de d�as festivos};
$mess[55] = qq{La duraci�n};
$mess[56] = qq{Importe en �};
$mess[57] = qq{Importe del seguro en �};
$mess[58] = qq{Validar};
$mess[59] = qq{Duplicar contrato};
$mess[60] = qq{El contrato};
$mess[61] = qq{est� entrada, Duplicar datos};
$mess[62] = qq{Fecha no v�lida};
#sub recherche2
$mess[100] = qq{El n�mero de resultados de tu b�squeda es de};
$mess[101] = qq{B�squeda};
$mess[102] = qq{Buscar};
$mess[103] = qq{Rellenar uno de los campos};
$mess[104] = qq{Validar tu b�squeda};
$mess[105] = qq{resultado};
$mess[106] = qq{resultados};
$mess[107] = qq{Volver a la b�squeda};
$mess[108] = qq{Volver};
#sub traitment2
$mess[150] = qq{El n�mero de resultados de tu b�squeda es de};
$mess[151] = qq{No hay resultados en tu b�squeda};
$mess[152] = qq{Retorno};
$mess[153] = qq{Ver oferta};
$mess[154] = qq{CONTROLAR};
$mess[155] = qq{CERRAR contrato};
$mess[156] = qq{MODIFICAR contrato};
$mess[157] = qq{Anular contrato};
$mess[158] = qq{Sumar/Restar d�as};
$mess[159] = qq{Duplicar datos};
$mess[160] = qq{MODIFICAR obra};
$mess[161] = qq{MODIFICAR Info. cliente};
$mess[162] = qq{Ver Contrato};
$mess[163] = qq{Reservar Sis. Traslaci�n};
$mess[164] = qq{Encontrado};
$mess[165] = qq{bloqueado};
$mess[166] = qq{desbloqueado};
$mess[167] = qq{Client <ste>(<code>) <libbloc>};
$mess[168] = qq{Hola\nel client <ste>(<code>) de <cp>,<ville>\nest� <libbloc>\n@+\nAtentamente\n};
#sub Stock 
$mess[200] = qq{Reservar Sis. Traslaci�n};
#sub nvclient
$mess[250] = qq{Verificar el C.C.C.};
$mess[251] = qq{C.C.C.};
$mess[252] = qq{Correcto};
$mess[253] = qq{Incorrecto};
$mess[254] = qq{Cliente nuevo};
$mess[255] = qq{Buscar !};
#sub bas form hauttab
$mess[300] = qq{Imprimir};
$mess[301] = qq{Cerrar};
#sub saisiecnt
$mess[350] = qq{Prospecci�n};
$mess[351] = qq{C�digo cliente};
$mess[352] = qq{de la delegaci�n};
$mess[353] = qq{C.I.F. / NIF};
$mess[354] = qq{C.C.C.};
$mess[355] = qq{Tel�fono};
$mess[356] = qq{Fax};
$mess[357] = qq{vuelve cliente de Acc�s Industrie};
$mess[358] = qq{para el contrato N�};
$mess[359] = qq{que empieza el};
$mess[360] = qq{vuelve cliente};
$mess[361] = qq{N� NIF};
#sub traitnvclient
$mess[400] = qq{Origen};
$mess[401] = qq{Documento origen};
$mess[402] = qq{Oferta};
$mess[403] = qq{Pre-contrato};
$mess[404] = qq{N� NIF};
$mess[405] = qq{C.I.F. / NIF};
$mess[406] = qq{Obra};
$mess[407] = qq{Direcci�n};
$mess[408] = qq{Ciudad};
$mess[409] = qq{Tel};
$mess[410] = qq{M�vil};
$mess[411] = qq{Fax};
$mess[412] = qq{Contacto};
$mess[413] = qq{Comentario};
$mess[414] = qq{Banco};
$mess[415] = qq{C.C.C.};
$mess[416] = qq{Forma de Pago};
$mess[417] = qq{Nuevo cliente de};
$mess[418] = qq{Sector de actividad};
#sub arretmodif
$mess[450] = qq{N�mero de d�a(s) festivo(s)};
$mess[451] = qq{La fecha de cierre antes de la fecha de inicio};
$mess[452] = qq{El contrato};
$mess[453] = qq{est� cerrado};
$mess[454] = qq{est� modificado};
#sub transport
$mess[500] = qq{Importe};
$mess[501] = qq{Contrato a editar};
$mess[502] = qq{Colectivo};
$mess[503] = qq{C�digo cliente};
$mess[504] = qq{Cliente v�lido};
$mess[505] = qq{Asignar transporte};
$mess[506] = qq{Oferta transportista};
$mess[507] = qq{Pedido transportista};
$mess[508] = qq{Pasar a ordenar ruta};
$mess[509] = qq{Fecha de transporte};
$mess[510] = qq{Imprimir contrato};
$mess[511] = qq{Cliente nuevo};
$mess[512] = qq{Aver�a m�quina};
$mess[513] = qq{Impossible de supprimer le transporteur sur le contrat};
#sub traitattmach
$mess[550] = qq{La m�quina};
$mess[551] = qq{est� seleccionada dos veces por lo menos};
$mess[552] = qq{La m�quina %s no corresponde al modelo del contrato al cual le quiere atribuir};
$mess[553] = qq{Ya ha atribuido esta m�quina};
#sub attmach
$mess[600] = qq{Asignar un n�mero de parque de una m�quina disponible};
$mess[601] = qq{Este n�mero es incompatible con el parque actual de la delegaci�n};
$mess[602] = qq{Asignar m�quina};
$mess[603] = qq{Imprimir lista m�quinas};
#sub carburant
$mess[650] = qq{Contrato, Modelo};
$mess[651] = qq{Asignar carburante};
#sub transfert 
$mess[700] = qq{M�quinas pte. recoger};
$mess[701] = qq{Trasladar};
$mess[702] = qq{Pasar a ALB-E/ALB-R/Ruta};
$mess[703] = qq{M�quinas a entregar};
$mess[704] = qq{Asignar transporte};
$mess[705] = qq{N� parque};
#sub tournee
$mess[750] = qq{Ruta N�};
$mess[751] = qq{Queda Capacidad;(Kg) antes de E/R/R};
$mess[752] = qq{Vigente};
$mess[753] = qq{Efectuada};
$mess[754] = qq{Anulada};
$mess[755] = qq{Capacidad};
$mess[756] = qq{Resto};
$mess[757] = qq{Masa de recuperaci�n};
$mess[758] = qq{Masa de entrega};
$mess[759] = qq{Pasar a ALB-E/ALB-R/Ruta};
$mess[760] = qq{Validar ruta};
$mess[761] = qq{El control de la ruta};
$mess[762] = qq{no est� completo};
$mess[763] = qq{M�quinas a validar};
$mess[764] = qq{El control de la ruta N�};
$mess[765] = qq{est� completo};
$mess[780] = qq{Fecha ruta};
#sub valtourn
$mess[800] = qq{La Ruta N�};
$mess[801] = qq{debe estar validada};
$mess[802] = qq{est� validada};
#sub verification
$mess[850] = qq{Fecha de control : 2 mes(*) , 15 d�as(**) , 1 semana (***) , est� adelantada(****)};
$mess[851] = qq{M�quinas en entrega};
$mess[852] = qq{M�quinas en traslado de obra a obra};
$mess[853] = qq{M�quina en entrega};
$mess[854] = qq{M�quina en recogida};
$mess[855] = qq{M�quina en traslado};
$mess[856] = qq{M�quina en verificaci�n};
$mess[857] = qq{Verificaci�n a Revisi�n};
$mess[858] = qq{Revisi�n a Verificaci�n};
$mess[859] = qq{Disponible a Verificaci�n};
$mess[860] = qq{Averiada a Verificaci�n};
$mess[861] = qq{Verificaci�n a Averiada};
$mess[862] = qq{M�quina en revisi�n};
$mess[863] = qq{Averiada a revisi�n};
$mess[864] = qq{Revisi�n a averiada};
$mess[865] = qq{M�quina averiada};
$mess[866] = qq{Revisi�n a disponible};
$mess[867] = qq{Verificaci�n a disponible};
$mess[868] = qq{Averiada a disponible};
$mess[869] = qq{M�quina disponible};
$mess[870] = qq{Estado};
$mess[871] = qq{Imprimir lista m�quinas};
$mess[872] = qq{M�quinas en recogida};
$mess[873] = qq{M�quinas en traslado de Ag. � Ag.};
$mess[874] = qq{Impression envoy�e sur l'imprimante};
#sub devis
$mess[900] = qq{La oferta};
$mess[901] = qq{est� registrada};
$mess[902] = qq{Hist�rico};
$mess[903] = qq{Cliente nuevo};
$mess[904] = qq{Seguro};
$mess[905] = qq{ no };
$mess[906] = qq{incluido};
$mess[907] = qq{Pasar a pre-contrato};
$mess[908] = qq{Seguro incluido};
$mess[909] = qq{Seguro no incluido};
$mess[910] = qq{Modificar Comentarios y Notas};
$mess[911] = qq{Abandonar};
$mess[912] = qq{Imprimir oferta};
$mess[913] = qq{Env�o por fax o impresi�n local};
$mess[914] = qq{Error};
$mess[915] = qq{Previsualizaci�n del archivo Acrobat};
$mess[916] = qq{Ninguna p�gina que indicar};
$mess[917] = qq{La zona es obligatoria};
$mess[918] = qq{Deben hacer una estimaci�n del transporte.};
$mess[919] = qq{Impresi�n imposible: reducci�n excepcional transporte en espera o transporte incorrecto.};
$mess[920] = qq{Impresi�n imposible: reducci�n excepcional en espera};
$mess[921] = qq{�Son seguro de querer cancelar la reducci�n excepcional?};
$mess[922] = qq{cancelar la reducci�n excep.};
$mess[923] = qq{Ficha cliente};
#sub traitdevis
$mess[950] = qq{La oferta};
$mess[951] = qq{pasa a contrato};
$mess[952] = qq{est� denegada};
$mess[953] = qq{Elija una raz�n de negativa};
$mess[954] = qq{Otro};
$mess[955] = qq{Por favor, elija una raz�n antes de denegar la oferta};
$mess[956] = qq{Oferta denegada};
$mess[957] = qq{Asunto: su petici�n de oferta de alquiler ACCES INDUSTRIE};
$mess[958] = qq{Sr., Sra.,<br><br>
Respondiendo a su petici�n adjuntamos una oferta de alquiler seg�n los datos indicados por Ud.<br><br>
Nos quedamos a su disposici�n para cualquier complemento de informaci�n que pueda necesitar.<br><br>
Agradecemos su inter�s por nuestra empresa y aprovechamos la ocasi�n para saludarle,<br><br>
Muy atentamente };
#sub ville
$mess[1000] = qq{Delegaci�n(es)};
$mess[1001] = qq{o};
#sub facturation
$mess[1050] = qq{Estamos a};
$mess[1051] = qq{Contrato(s) a facturar};
$mess[1052] = qq{Facturar};
$mess[1053] = qq{La Facturaci�n del};
$mess[1054] = qq{mes ya est� hecha};
$mess[1055] = qq{Hay};
$mess[1056] = qq{Contratos a pasar en facturaci�n};
$mess[1057] = qq{Se factura};
$mess[1058] = qq{Exportaci�n a Sage};
$mess[1059] = qq{La facturaci�n ir�};
$mess[1060] = qq{Ya se efectu� la facturaci�n};
$mess[1061] = qq{a agarrar};
$mess[1062] = qq{hasta el};
$mess[1063] = qq{Contratos a cerrar};
$mess[1064] = qq{Carburante a entrar};
#sub traitfac
$mess[1100] = qq{El Importe de};
$mess[1101] = qq{est� de};
$mess[1102] = qq{para};
$mess[1103] = qq{facturas};
$mess[1104] = qq{El importe total de las facturas es de};
#sub tableau
$mess[1150] = qq{Selecci�n por fecha};
$mess[1151] = qq{Selecci�n por ciudad};
$mess[1152] = qq{Revisi�n};
$mess[1153] = qq{Averiada};
$mess[1154] = qq{Control};
$mess[1155] = qq{Dispo};
$mess[1156] = qq{Total m�quina(s)};
$mess[1157] = qq{Estado};
$mess[1158] = qq{Imprimir lista m�quinas};
$mess[1159] = qq{Verificaci�n};
#sub transfagag
$mess[1200] = qq{Trasladar};
#sub validnvclient
$mess[1250] = qq{El c�digo cliente};
$mess[1251] = qq{El colectivo};
$mess[1252] = qq{entre};
$mess[1253] = qq{en parr�fo aparte};
#sub mois_stat
$mess[1300] = qq{Hist�rico};
#sub stat_mois
$mess[1350] = qq{principio del mes};
$mess[1351] = qq{y fin de mes};
$mess[1352] = qq{N�mero};
$mess[1353] = qq{Total};
$mess[1354] = qq{Media};
$mess[1355] = qq{Ecart type?};
$mess[1356] = qq{Facturaci�n delegaci�n};
$mess[1357] = qq{Duraci�n};
$mess[1358] = qq{Seguro no incluido};
$mess[1359] = qq{Transporte};
$mess[1360] = qq{Transporte externo};
$mess[1361] = qq{Carburante};
$mess[1362] = qq{Contrato activo};
$mess[1363] = qq{Contrato cerrado};
$mess[1364] = qq{Contrato inferior a 5 d�as};
$mess[1365] = qq{Contrato superior de 5 d�as};
$mess[1366] = qq{Contrato mensual};
$mess[1367] = qq{Forfait};
$mess[1368] = qq{Tipo};
$mess[1369] = qq{Facturaci�n};
$mess[1370] = qq{N�mero parque};
$mess[1371] = qq{D�as alquilados};
$mess[1372] = qq{N�mero Contrato};
$mess[1373] = qq{D�as Mes};
$mess[1374] = qq{Precio medio alquilado};
$mess[1375] = qq{Precio medio parque};
$mess[1376] = qq{Rotaci�n (Rentabilidad) en %};
$mess[1377] = qq{Ratio};
$mess[1378] = qq{D�as medios alquilados};
$mess[1379] = qq{TOTAL};
$mess[1380] = qq{Modelo};
#sub nomade
$mess[1400] = qq{Estoy};
$mess[1401] = qq{Delegaci�n(es)};
$mess[1402] = qq{Teleportaci�n};
#sub saisiemach
$mess[1450] = qq{La M�quina};
$mess[1451] = qq{est� entrada};
$mess[1452] = qq{Parque};
$mess[1453] = qq{o};
$mess[1454] = qq{de serie};
$mess[1455] = qq{ya existe};
$mess[1456] = qq{Validar};
$mess[1457] = qq{Precontrato};
$mess[1458] = qq{Contrato};
$mess[1459] = qq{Parada};
$mess[1460] = qq{Archivo};
$mess[1461] = qq{Anular};
$mess[1462] = qq{Entraga};
$mess[1463] = qq{R�cuperacion};
$mess[1464] = qq{Transfer Obra};
$mess[1465] = qq{Puesto en facturaci�n};
#message impression
$mess[1500] = qq{ALBARAN DE ENTREGA};
$mess[1501] = qq{ALBARAN DE RECOGIDA};
#imprssion ass
$mess[1550] = qq{Cobertura Ries. facturada al cliente = % del precio del alquiler (dias naturales)};
$mess[1551] = qq{Cobertura Ries. no facturada};
$mess[1552] = qq{Asegurado x cliente};
$mess[1553] = qq{Cobertura Ries. 10% sobre alquiler};
$mess[1554] = qq{};
$mess[1555] = qq{ASEGURADO X CLIENTE};
$mess[1556] = qq{Cobertura Ries. facturada al cliente = };
$mess[1557] = qq{ % del precio del alquiler (dias naturales)};
$mess[1558] = qq{Cobertura Ries. incluida en el precio de alquiler};
#sub traitfac
$mess[1600] = qq{Tarifa mensual};
$mess[1601] = qq{Forfait};
$mess[1602] = qq{Abono};
$mess[1603] = qq{Entrega};
$mess[1604] = qq{Recogida};
$mess[1605] = qq{Consumo carb.};
$mess[1606] = qq{Cobertura Ries. 10%};
$mess[1607] = qq{ Del };
$mess[1608] = qq{ al };
$mess[1609] = qq{Horas deducidas};
$mess[1610] = qq{Horas suplementarias};
$mess[1611] = qq{Horas sabado};
$mess[1612] = qq{Horas domingo};
$mess[1613] = qq{Dias deducidos};
$mess[1614] = qq{Dias suplementarios};
$mess[1615] = qq{Esta factura incluye los dias trabajados de los 2 meses};
$mess[1616] = qq{Gestion de residuos};
$mess[1617] = qq{Limpieza};
$mess[1618] = qq{Servicio};
$mess[1619] = qq{Reducci�n excepcional transporte de};
$mess[1620] = qq{Red. excep. XX% sobre entrega};
$mess[1621] = qq{Red. excep. XX% sobre recogida};
$mess[1622] = qq{Red. excep. XX% sobre entrega(red. para cumulo * maqu. incluida)};
$mess[1623] = qq{Red. excep. XX% sobre recogida(red. para cumulo * maqu. incluida)};
$mess[1624] = qq{Red. para cumulo * maqu. XX%};
$mess[1625] = qq{Red. para cumulo * maqu. XX%};
$mess[1626] = qq{Seguro negociado 5%};
$mess[1627] = qq{Red. excep. sobre Entrega};
$mess[1628] = qq{Red. excep. sobre Recogida};
#arret contratraie
$mess[1650] = qq{Cierre imposible : Contactar con el responsable de log�stica para validar la entrega en la ruta N�;};
#sub traitcasse
$mess[1700] = qq{Oferta entrada};
$mess[1701] = qq{Imprimir el documento};
#annuler client
$mess[1750] = qq{Vas a suprimir a un cliente ! Validar para confirmar !};
$mess[1751] = qq{El primer cliente est� suprimido. Ofertas y contratos trasladados en el segundo contrato};
$mess[1752] = qq{&nbsp;At&nbsp;&nbsp;&nbsp;Acc&nbsp;&nbsp;Ref};
$mess[1753] = qq{At&nbsp;:&nbsp;Attente&nbsp;&nbsp;&nbsp;Acc&nbsp;:&nbsp;Accepte&nbsp;&nbsp;&nbsp;Ref&nbsp;:&nbsp;Refuse};
#doublon client
$mess[1800] = qq{El cliente/C�digo cliente};
$mess[1801] = qq{ya existe, puede buscar con este nombre};
$mess[1802] = qq{ha sido creado, puede buscar con este nombre};
$mess[1803] = qq{Uno de los campos no se ha rellenado correctamente};
$mess[1804] = qq{Este NIF ya se ha utilizado para los clientes siguientes: %s. Conactar con el dpto clientes para poder crear este cliente.};
#sub verification
$mess[1850] = qq{Control a verificaci�n};
$mess[1851] = qq{Verificaci�n a control};
$mess[1852] = qq{M�quina(s) en control};
#chg cleint Contrato
$mess[1900] = qq{El cliente va estar cambiado ! Validar para confirmar !};
$mess[1901] = qq{El Cliente est� cambiado};
#remettre en cours contrat
$mess[1950] = qq{El Contrato va a ser reabierto ! Validar para confirmar !};
$mess[1951] = qq{El contrato est� abierto};
$mess[1952] = qq{Un transporte est� previsto para este contrato ! Anularlo antes de poder reabrir el contrato !};
#newstat
$mess[2000] = qq{Delegaci�n};
$mess[2001] = qq{Facturaci�n mes};
$mess[2002] = qq{Facturaci�n transporte};
$mess[2003] = qq{Facturaci�n carburante};
$mess[2004] = qq{Facturaci�n SAT};
$mess[2005] = qq{Facturaci�n forfait};
$mess[2006] = qq{Coste financiero};
$mess[2007] = qq{Tipo};
$mess[2008] = qq{N� m�quinas};
$mess[2009] = qq{Facturaci�n m�quinas/d�a};
$mess[2010] = qq{Facturaci�n media/d�a};
$mess[2011] = qq{Tasa de rotaci�n financiera en %};
$mess[2012] = qq{Tasa de rotaci�n m�quina en %};
$mess[2013] = qq{Sis. traslaci�n};
$mess[2014] = qq{Para el tipo};
$mess[2015] = qq{Tasa de ocupaci�n en %};
$mess[2016] = qq{Tasa de ocupaci�n en % desde el principio del mes};
$mess[2017] = qq{Tasa de ocupaci�n (%) mes pasado};
$mess[2018] = qq{Comparativo mensual mes pasado};
#consultation contrat
$mess[2050] = qq{N� contrato};
$mess[2051] = qq{Estado contrato};
$mess[2052] = qq{Nombre cliente};
$mess[2053] = qq{Tel cliente};
$mess[2054] = qq{Fax cliente};
$mess[2055] = qq{Contacto cliente};
$mess[2056] = qq{Obra};
$mess[2057] = qq{Direcci�n obra};
$mess[2058] = qq{Ciudad obra};
$mess[2059] = qq{Tel obra};
$mess[2060] = qq{Contacto obra};
$mess[2061] = qq{Modelo m�quina};
$mess[2062] = qq{N� parque};
$mess[2063] = qq{Fecha salida};
$mess[2064] = qq{Fecha retorno};
$mess[2065] = qq{Sumar d�as};
$mess[2066] = qq{Restar d�as};
$mess[2067] = qq{Duraci�n};
$mess[2068] = qq{Precio diario};
$mess[2069] = qq{Precio mensual};
$mess[2070] = qq{Importe global};
$mess[2071] = qq{Entrega};
$mess[2072] = qq{Transport vuelta};
$mess[2073] = qq{Transporte previsto};
$mess[2074] = qq{Ch�fer o transporte externo (vuelta)};
$mess[2075] = qq{Seguro no incluido};
$mess[2076] = qq{no};
$mess[2077] = qq{Seguro};
$mess[2078] = qq{Importe total};
$mess[2079] = qq{Comentario};
$mess[2080] = qq{Cerrar};
$mess[2081] = qq{Ch�fer o transporte externo (ida)};
$mess[2082] = qq{sin de los d�as ya facturados : };
$mess[2083] = qq{Pedido por};
$mess[2084] = qq{Tel�fono};
$mess[2085] = qq{Nettoyage};
$mess[2086] = qq{Service};
# transportint.cfg
$mess[2100] = qq{Transporte interno};
$mess[2101] = qq{Pedido transporte externo};
$mess[2102] = qq{N�mero};
$mess[2103] = qq{N� contrato};
$mess[2104] = qq{Cliente};
$mess[2105] = qq{Obra};
$mess[2106] = qq{C.P.};
$mess[2107] = qq{Ciudad};
$mess[2108] = qq{Muelle};
$mess[2109] = qq{Entrega anticipada posible};
$mess[2110] = qq{Fecha salida};
$mess[2111] = qq{Fecha retorno};
$mess[2112] = qq{Importe};
$mess[2113] = qq{Peso};
$mess[2114] = qq{Modelo m�quina};
$mess[2115] = qq{N� parque};
$mess[2116] = qq{Estado contrato};
$mess[2117] = qq{Hora de entrada/recogida};
$mess[2118] = qq{Transporte previsto};
$mess[2119] = qq{Ch�fer};
$mess[2120] = qq{Pedido transportista};
$mess[2130] = qq{Franja horaria};
$mess[2131] = qq{No hay comentario};
#contrat.cfg et contrat2.cfg
$mess[2150] = qq{N�mero};
$mess[2151] = qq{N� contrato};
$mess[2152] = qq{Cliente};
$mess[2153] = qq{Obra};
$mess[2154] = qq{C.P.};
$mess[2155] = qq{Ciudad};
$mess[2156] = qq{Fecha salida};
$mess[2157] = qq{Fecha retorno};
$mess[2158] = qq{Modelo m�quina};
$mess[2159] = qq{N� parque};
$mess[2160] = qq{Estado contrato};
$mess[2161] = qq{Editar contrato(s)};
$mess[2162] = qq{Editar contrato(s) para una primera impresi�n};

$mess[2200] = qq{N�mero};
$mess[2201] = qq{Ultimo contrato};
$mess[2202] = qq{Indice};
$mess[2203] = qq{Modificaci�n};
$mess[2204] = qq{C.P. obra};
$mess[2205] = qq{Modelo m�quina};
$mess[2206] = qq{N� parque};
$mess[2207] = qq{Estado};
$mess[2208] = qq{Date Fin Contrat};
$mess[2209] = qq{Fecha vencimiento};
$mess[2210] = qq{Organismo};
$mess[2211] = qq{Pedido de visita peri�dica};
$mess[2212] = qq{Pedido de Control};
$mess[2250] = qq{Imp. Unit�};
$mess[2251] = qq{N� Comm};
$mess[2252] = qq{Date de Visite};

$mess[2300] = qq{Clientes nuevos};
$mess[2301] = qq{N�};
$mess[2302] = qq{Delegaci�n};
$mess[2303] = qq{Empresa};
$mess[2304] = qq{Direcci�n};
$mess[2305] = qq{C.P.};
$mess[2306] = qq{Ciudad};
$mess[2307] = qq{C.I.F. / NIF};
$mess[2308] = qq{Tel};
$mess[2309] = qq{Fax};
$mess[2310] = qq{Forma de pago};
$mess[2311] = qq{C.C.C.};
$mess[2312] = qq{ya se asigna al cliente};
$mess[2313] = qq{Formas de pagos};
$mess[2320] = qq{B�squeda por nombre de cliente};

$mess[2350] = qq{APE};
$mess[2351] = qq{Actividad};
$mess[2352] = qq{C.I.F. / NIF};
$mess[2353] = qq{Clientes};
$mess[2354] = qq{Direcci�n};
$mess[2355] = qq{Comp. direcci�n};
$mess[2356] = qq{Ciudad};
$mess[2357] = qq{Tel};
$mess[2358] = qq{M�vil};
$mess[2359] = qq{Fax};
$mess[2360] = qq{Contacto};
$mess[2361] = qq{Comentario};
$mess[2362] = qq{Forma de pago};
$mess[2363] = qq{Banco};
$mess[2364] = qq{C.C.C.};
$mess[2365] = qq{Precio alquiler};
$mess[2366] = qq{Grupo};
$mess[2367] = qq{Calificaci�n};
$mess[2368] = qq{Email Cliente};
$mess[2370] = qq{Ning�n};
$mess[2371] = qq{Final de validez (seguro)};
$mess[2372] = qq{Potencial};
$mess[2373] = qq{Cat.};
$mess[2374] = qq{Pot.};
$mess[2375] = qq{Ning�n};
$mess[2380] = qq{categor�a};

$mess[2390] = qq{Cliente Nuevo};

$mess[2397] = qq{archive.dev\@acces-industrie.com};
$mess[2398] = qq{gisela.guardia\@accesplataformas.com};
$mess[2399] = qq{1};

$mess[2400] = qq{Clientes};
$mess[2401] = qq{Ciudad obra};
$mess[2402] = qq{Obra};
$mess[2403] = qq{Direcci�n Obra};
$mess[2404] = qq{Contacto Obra};
$mess[2405] = qq{Tel obra};
$mess[2406] = qq{N� pedido};
$mess[2407] = qq{Tipo m�quina};
$mess[2408] = qq{Fuera del parque};
$mess[2409] = qq{Cantidad};
$mess[2410] = qq{Precio D�a/Mes};
$mess[2411] = qq{Tipo Importe};
$mess[2412] = qq{Duraci�n};
$mess[2413] = qq{Fecha previsional};
$mess[2414] = qq{Importe};
$mess[2415] = qq{Entrega};
$mess[2416] = qq{Recogida};
$mess[2417] = qq{Seguro incluido<br>(los precios de cobertura no toman en cuenta los weekends y las fiestas)};
$mess[2418] = qq{Importe seguro};
$mess[2419] = qq{Importe total};
$mess[2420] = qq{Comentario};
$mess[2421] = qq{Notas comerciales (no aparecen en la oferta)};
$mess[2422] = qq{Oferta validada};
$mess[2423] = qq{Calcular};
$mess[2424] = qq{Validar};
$mess[2425] = qq{Stock};
$mess[2426] = qq{Oferta};
$mess[2427] = qq{Entrega};
$mess[2428] = qq{Recogida};
$mess[2429] = qq{Tarifa};
$mess[2430] = qq{Reducci�n};
$mess[2431] = qq{A�adir una m�quina};
$mess[2432] = qq{Cliente};
$mess[2433] = qq{M�quina};
$mess[2434] = qq{Duraci�n};
$mess[2435] = qq{Precio};
$mess[2436] = qq{Importe};
$mess[2437] = qq{Transporte};
$mess[2438] = qq{Pedir una reducci�n excepcional};
$mess[2439] = qq{Red. excep.};
$mess[2440] = qq{Zona};
$mess[2441] = qq{Estimaci�n de transporte};
$mess[2442] = qq{Sube ir y/o vuelta incorrecta(s).};
$mess[2443] = qq{Faltan informaciones o �stas han cambiado, rehacer una estimaci�n o completar los datos.};
$mess[2444] = qq{(entre 1 y 6)};
$mess[2445] = qq{Alquiler largo (m�nimo 6 meses)};

$mess[2450] = qq{N� contrato};
$mess[2451] = qq{Obra};
$mess[2452] = qq{Direcci�n};
$mess[2453] = qq{C.P.};
$mess[2454] = qq{Ciudad};
$mess[2455] = qq{Contacto};
$mess[2456] = qq{Modelo};
$mess[2457] = qq{N� parque};
$mess[2458] = qq{Peso};
$mess[2459] = qq{Estado};
$mess[2460] = qq{Estado contrato};
$mess[2461] = qq{Nombre ch�fer};
$mess[2462] = qq{Hora de entrada/recogida};
$mess[2463] = qq{Orden};

$mess[2500] = qq{Ordenar ruta};
$mess[2501] = qq{Edici�n ALB-E/ALB-R/Ruta};
$mess[2502] = qq{Validar Entrega/Recogida/Cambio/Ruta};

$mess[2505] = qq{N� contrato};
$mess[2506] = qq{Obra};
$mess[2507] = qq{Direcci�n};
$mess[2508] = qq{C.P.};
$mess[2509] = qq{Ciudad};
$mess[2510] = qq{Contacto};
$mess[2511] = qq{Modelo};
$mess[2512] = qq{N� parque};
$mess[2513] = qq{Estado};
$mess[2514] = qq{Estado contrato};
$mess[2515] = qq{Nombre ch�fer};
$mess[2516] = qq{Orden};
$mess[2517] = qq{Hora de entrada/recogida};
$mess[2518] = qq{Efectuada};
$mess[2519] = qq{Ch�fer interno};
$mess[2520] = qq{Transporte externo};

$mess[2550] = qq{Tipo importe};
$mess[2551] = qq{D�a};
$mess[2552] = qq{Mensual};
$mess[2553] = qq{Forfait};

$mess[2600] = qq{Modificar el membrete};
$mess[2601] = qq{Modificar la l�nea};
$mess[2602] = qq{Modificar el Sis. Traslaci�n};
$mess[2603] = qq{�Deben imperativamente registrar la l�nea antes de cualquier otra cosa!};
$mess[2604] = qq{�Deben imperativamente registrar el contrato antes de cualquier otra cosa!};

$mess[2650] = qq{Modificar/Cerrar contrato};
$mess[2651] = qq{N�mero};
$mess[2652] = qq{Estado contrato};
$mess[2653] = qq{N� contrato};
$mess[2654] = qq{Indice contrato};
$mess[2655] = qq{Cliente};
$mess[2656] = qq{Obra};
$mess[2657] = qq{Ciudad obra};
$mess[2658] = qq{Modelo m�quina};
$mess[2659] = qq{N� parque};
$mess[2660] = qq{Fuera del parque};
$mess[2661] = qq{Fecha salida};
$mess[2662] = qq{Fecha retorno};
$mess[2663] = qq{Duraci�n};
$mess[2664] = qq{Sumar d�a};
$mess[2665] = qq{Restar d�a};
$mess[2666] = qq{Precio por d�a};
$mess[2667] = qq{Precio por mes};
$mess[2668] = qq{Forfait};
$mess[2669] = qq{Seguro incluido};
$mess[2670] = qq{Importe seguro};
$mess[2671] = qq{Entrega};
$mess[2672] = qq{Recogida ida};
$mess[2673] = qq{Recogida};
$mess[2674] = qq{Recogida vuelta};
$mess[2675] = qq{Muelle};
$mess[2676] = qq{Importe total IVA excluido};
$mess[2677] = qq{Pedido};
$mess[2678] = qq{Comentario};
$mess[2679] = qq{Notas};
$mess[2680] = qq{N� contrato};
$mess[2681] = qq{Cliente};
$mess[2682] = qq{N� Parque};
$mess[2683] = qq{Este contrato ya ha sido cerrado};
$mess[2684] = qq{El Tipo de m�quina es cambiado};
$mess[2685] = qq{El contrato se ha cancelado};
$mess[2686] = qq{Accesorios ataron al contrato};
$mess[2687] = qq{Accesorio (N� Parque)};
$mess[2688] = qq{Contrato (Estado)};
$mess[2689] = qq{Precio};
$mess[2690] = qq{Fecha salida};
$mess[2691] = qq{Fecha salida};
$mess[2692] = qq{La m�quina alquil� (elogiado) sin el accesorio por omisi�n(defecto)};
$mess[2693] = qq{Contrato de origen};

$mess[2700] = qq{Modelo};
$mess[2701] = qq{Sis. Traslaci�n};
$mess[2702] = qq{Elevaci�n};
$mess[2703] = qq{Energ�a};
$mess[2704] = qq{Peso};
$mess[2705] = qq{Stock reservado};
$mess[2706] = qq{Stock reservable};
$mess[2707] = qq{Stock total};
$mess[2708] = qq{Alquilada 0%};
$mess[2709] = qq{En transferencia 0%};
$mess[2710] = qq{Averiada 0%};
$mess[2711] = qq{Control 90%};
$mess[2712] = qq{Pte. recoger 50%};
$mess[2713] = qq{En verificaci�n 80%};
$mess[2714] = qq{En revisi�n 80%};
$mess[2715] = qq{Disponible 100%};

$mess[2750] = qq{Entrar pre-contrato};
$mess[2751] = qq{Clientes};
$mess[2752] = qq{Tipo m�quina};
$mess[2753] = qq{Fuera del parque};
$mess[2754] = qq{Ciudad obra};
$mess[2755] = qq{Obra};
$mess[2756] = qq{Direcci�n obra};
$mess[2757] = qq{Contacto obra};
$mess[2758] = qq{Tel. obra};
$mess[2759] = qq{Pedido por};
$mess[2760] = qq{Tel.};
$mess[2761] = qq{N� pedido};
$mess[2762] = qq{Fecha salida};
$mess[2763] = qq{Fecha retorno};
$mess[2764] = qq{Sumar d�as};
$mess[2765] = qq{Restar d�as};
$mess[2766] = qq{Precio};
$mess[2767] = qq{Duraci�n};
$mess[2768] = qq{Precio por mes};
$mess[2769] = qq{Importe global};
$mess[2770] = qq{Seguro};
$mess[2771] = qq{Entrega};
$mess[2772] = qq{Sus medios};
$mess[2773] = qq{Recogida};
$mess[2774] = qq{Sus medios};
$mess[2775] = qq{Muelle};
$mess[2776] = qq{Comentario};
$mess[2777] = qq{Sis. Traslaci�n imperativo};
$mess[2778] = qq{Cambio de obra a obra};
$mess[2779] = qq{Pago a la entrega};
$mess[2780] = qq{Calcular};
$mess[2781] = qq{Cuidado, el modelo seleccionado no ser� reservado hasta que el pre-contrato no est� validado !};
$mess[2782] = qq{La �ltima m�quina reservable de este modelo se acaba de reservar, reservar otra modelo !};
$mess[2783] = qq{Tipo Importe};
$mess[2784] = qq{Tarifa};
$mess[2785] = qq{Reducci�n};
$mess[2786] = qq{Reducci�n exceptional};
$mess[2787] = qq{Limpieza};
$mess[2788] = qq{Sus Medios};
$mess[2789] = qq{M�quina ad.};
$mess[2790] = qq{M�quina ad.};

$mess[2800] = qq{Delegaci�n};
$mess[2801] = qq{Responsable};
$mess[2802] = qq{Direcci�n};
$mess[2803] = qq{Tel};
$mess[2804] = qq{Fax};
$mess[2805] = qq{C.P.};
$mess[2806] = qq{Ciudad};
$mess[2807] = qq{Cliente};
$mess[2808] = qq{Direcci�n};
$mess[2809] = qq{C.P.};
$mess[2810] = qq{Ciudad};
$mess[2811] = qq{Tel};
$mess[2812] = qq{Fax};
$mess[2813] = qq{Obra};
$mess[2814] = qq{Direcci�n};
$mess[2815] = qq{C.P.};
$mess[2816] = qq{Ciudad};
$mess[2817] = qq{Responsable};
$mess[2818] = qq{Marca};
$mess[2819] = qq{Tipo};
$mess[2820] = qq{Energ�a};
$mess[2821] = qq{Modelo};
$mess[2822] = qq{Peso};
$mess[2823] = qq{Alquiler diario};
$mess[2824] = qq{Seguro};
$mess[2825] = qq{Entrega};
$mess[2826] = qq{Recogida};
$mess[2827] = qq{Fecha salida};
$mess[2828] = qq{Fecha retorno};
$mess[2829] = qq{Sumar d�as};
$mess[2830] = qq{Restar d�as};
$mess[2831] = qq{Duraci�n};
$mess[2832] = qq{Comercial};
$mess[2833] = qq{Indice};
$mess[2834] = qq{Seguro incluido};

$mess[2850] = qq{Consulta contrato};
$mess[2851] = qq{N�mero};
$mess[2852] = qq{N� contrato};
$mess[2853] = qq{Cliente};
$mess[2854] = qq{Obra};
$mess[2855] = qq{C.P. Obra};
$mess[2856] = qq{Ciudad obra};
$mess[2857] = qq{Contacto obra};
$mess[2858] = qq{Contacto commande};
$mess[2859] = qq{Modelo m�quina};
$mess[2860] = qq{N� parque};
$mess[2861] = qq{Fecha salida};
$mess[2862] = qq{Fecha retorno};
$mess[2863] = qq{Duraci�n};
$mess[2864] = qq{Precio por d�a};
$mess[2865] = qq{Precio por mes};
$mess[2866] = qq{Importe seguro};
$mess[2867] = qq{Montant Gestion de recours};
$mess[2868] = qq{Entrega};
$mess[2869] = qq{Recogida};
$mess[2870] = qq{Cantidad carburante (L)};
$mess[2871] = qq{Nettoyage};
$mess[2872] = qq{Service};
$mess[2873] = qq{Importe total IVA excluido};
$mess[2874] = qq{Estado contrato};
$mess[2875] = qq{N� pedido};
$mess[2876] = qq{Notas};

$mess[2882] = qq{N� Contrato};
$mess[2883] = qq{Cliente};
$mess[2884] = qq{N� Parque};
$mess[2885] = qq{Obra};
$mess[2886] = qq{Obra};
$mess[2887] = qq{D�partement Obra};
$mess[2888] = qq{a�o};
$mess[2889] = qq{Estado contrato};
$mess[2890] = qq{ser};
$mess[2891] = qq{dia(s)};
$mess[2892] = qq{/&nbsp;dia};
$mess[2893] = qq{/&nbsp;mes};
$mess[2894] = qq{per forfait};
$mess[2895] = qq{Tarifa vaga !};
$mess[2896] = qq{S�lo los d�as de m�s se pueden modificar};

$mess[2900] = qq{Lista m�quinas / agencia de };
$mess[2901] = qq{Modelo};
$mess[2902] = qq{N� parque};
$mess[2903] = qq{Fuera del parque};
$mess[2904] = qq{N� serie};
$mess[2905] = qq{Fecha de compra};
$mess[2906] = qq{Fecha de control};
$mess[2907] = qq{Sis. traslaci�n};
$mess[2908] = qq{Marca};
$mess[2909] = qq{Elevaci�n};
$mess[2910] = qq{Energ�a};
$mess[2911] = qq{Peso};
$mess[2912] = qq{Largo};
$mess[2913] = qq{Ancho};
$mess[2914] = qq{Capacidad};
$mess[2915] = qq{Estado};
$mess[2916] = qq{Probabilidad de alquiler en %};
$mess[2917] = qq{Delegaci�n};
$mess[2918] = qq{Ultimo contrato};
$mess[2919] = qq{N� Parque};
$mess[2920] = qq{N� Serie};
$mess[2921] = qq{Sis. Traslaci�n m�quina};
$mess[2922] = qq{Familia m�quina};
$mess[2923] = qq{Proveedor};
$mess[2924] = qq{Elevaci�n};
$mess[2925] = qq{Energ�a};
$mess[2926] = qq{Estado};
$mess[2927] = qq{Parque};
$mess[2928] = qq{Delegaci�n};
$mess[2929] = qq{Pa�s};
$mess[2930] = qq{�ltimo contrato referente a esta m�quina};
$mess[2931] = qq{Hist�rico de los contratos};
$mess[2932] = qq{Ver hist�rico};
$mess[2933] = qq{%d �ltimos:};
$mess[2934] = qq{Ning�n contrato en Espa�a};
$mess[2935] = qq{Lista m�quinas global};
$mess[2936] = qq{Zona};

$mess[2950] = qq{Modificaci�n Info. obra};
$mess[2951] = qq{N�mero};
$mess[2952] = qq{N� contrato};
$mess[2953] = qq{Cliente};
$mess[2954] = qq{Obra};
$mess[2955] = qq{Direcci�n obra};
$mess[2956] = qq{Ciudad};
$mess[2957] = qq{Contacto};
$mess[2958] = qq{Tel�fono};
$mess[2959] = qq{Altura};
$mess[2960] = qq{Superficie};
$mess[2961] = qq{Hora de entrega};
$mess[2962] = qq{Hora de recogida};
$mess[2963] = qq{Comentario};
$mess[2964] = qq{N� Contrato};
$mess[2965] = qq{Cliente};
$mess[2966] = qq{Obra};
$mess[2967] = qq{Ciudad};

$mess[3000] = qq{Modificaci�n Info. cliente};
$mess[3001] = qq{N�mero};
$mess[3002] = qq{C�digo cliente};
$mess[3003] = qq{APE};
$mess[3004] = qq{C.I.F. / NIF};
$mess[3005] = qq{Obra};
$mess[3006] = qq{Direcci�n};
$mess[3007] = qq{Direcci�n Comp.};
$mess[3008] = qq{Ciudad};
$mess[3009] = qq{Tel�fono};
$mess[3010] = qq{Fax};
$mess[3011] = qq{M�vil};
$mess[3012] = qq{E-mail};
$mess[3013] = qq{Contacto};
$mess[3014] = qq{Colectivo};
$mess[3015] = qq{LI_NO};
$mess[3016] = qq{Banco};
$mess[3017] = qq{Forma de pago};
$mess[3018] = qq{C.C.C.};
$mess[3019] = qq{N�mero factura};
$mess[3020] = qq{Factura local};
$mess[3021] = qq{Factura no agrupada};
$mess[3022] = qq{Comentario};
$mess[3023] = qq{Activitidad};
$mess[3024] = qq{Precio alquiler};
$mess[3025] = qq{Bloqueo clientes};

$mess[3026] = qq{Seguro};
$mess[3027] = qq{N� de pedido obligatorio};
$mess[3028] = qq{Orden de Pedido que debe proporcionarse};
$mess[3029] = qq{Pago a la entrega};
$mess[3030] = qq{Condiciones generales de venta};
$mess[3031] = qq{Facturaci�n de la limpieza};
$mess[3032] = qq{Contrato Marco};
$mess[3033] = qq{Si};
$mess[3034] = qq{No};
$mess[3035] = qq{Date d'application de l'assurance souhait�e.};
$mess[3036] = qq{Falta el NIF};
$mess[3037] = qq{Formato de NIF incorrecto};
$mess[3038] = qq{Este NIF ya existe};

$mess[3040] = qq{Codigo Cliente};
$mess[3041] = qq{Cliente};
$mess[3042] = qq{Ciudad};
$mess[3043] = qq{Provincia};
$mess[3044] = qq{NIF};
$mess[3045] = qq{Precio alquiler};
$mess[3046] = qq{Albaranes firmados};

$mess[3047] = qq{Facturaci�n de la gesti�n de residuos};
$mess[3048] = qq{Calificaci�n};
$mess[3060] = qq{Participation au recyclage};

$mess[3049] = qq{El campo "facturaci�n local" ha sido modificado, Por favor contacten con  el 5000 para que las acciones necesarias sean emprendidas};

$mess[3050] = qq{Suprimir cliente};
$mess[3051] = qq{Cliente err�neo};
$mess[3052] = qq{Cliente correcto};
$mess[3053] = qq{Calcular};
# Assurance souhait�e
$mess[3054] = qq{Assurance souhait�e};
# Assurance actuelle
$mess[3055] = qq{Assurance actuelle};
# Gestion de recours actuelle
$mess[3056] = qq{Gestion de recours};
# Gestion de recours souhait�e
$mess[3057] = qq{Gestion de recours souhait�e};
$mess[3058] = qq{Date d'application souhait�e.};
$mess[3059] = qq{Non facturable};

$mess[3100] = qq{Cambio Cliente/Contrato};
$mess[3101] = qq{Cliente err�neo en el contrato};
$mess[3102] = qq{Buen cliente};
$mess[3103] = qq{Calcular};
$mess[3104] = qq{Attention, sur les deux clients, les informations suivantes sont diff�rentes (elles doivent �tre mises � jour <span style="text-decoration:underline;">manuellement</span>) :};
$mess[3105] = qq{ATTENTION, � la validation les tarifs de location et transport sont <span style="text-decoration:underline;">automatiquement maintenus suivant les conditions du premier client</span>};
$mess[3106] = qq{L�assurance du contrat n�est pas compatible avec l�assurance du nouveau client :};
$mess[3107] = qq{Un probl�me est survenu lors du changement de client sur le contrat.};


$mess[3150] = qq{Cambio M�quina/Contrato};
$mess[3151] = qq{Contrato/N� parque};
$mess[3152] = qq{Buena m�quina (Disponible)};
$mess[3153] = qq{Calcular};
$mess[3154] = qq{Ud ya ha validado};
$mess[3155] = qq{La m�quina es cambiada};
$mess[3156] = qq{El Tipo de m�quina es cambiado};
$mess[3157] = qq{El contrato se ha cancelado};
$mess[3158] = qq{La Oferta est� entrada};
$mess[3159] = qq{Imposible hacer el cambio, la gira de entrega es efectuada};
$mess[3160] = qq{Impossible de faire le changement, le typage du mod�le de machines du contrat est diff�rent de celui de la machine saisie};

$mess[3200] = qq{Reabrir contrato};
$mess[3201] = qq{Contrato cerrado};
$mess[3202] = qq{Calcular};
$mess[3203] = qq{Debe grabar la nota antes de anular el contrato.};

$mess[3250] = qq{Entrar m�quina};
$mess[3251] = qq{B�squeda de la designaci�n};
$mess[3252] = qq{N� parque};
$mess[3253] = qq{N� serie};
$mess[3254] = qq{Fecha "Control" };
$mess[3255] = qq{Calcular};

$mess[3300] = qq{Realquiler};
$mess[3301] = qq{Clientes};
$mess[3302] = qq{Ciudad obra};
$mess[3303] = qq{Obra};
$mess[3304] = qq{Direcci�n obra};
$mess[3305] = qq{Contacto obra};
$mess[3306] = qq{Tel obra};
$mess[3307] = qq{Pedido por};
$mess[3308] = qq{T�l.};
$mess[3309] = qq{Machine};
$mess[3310] = qq{Alquilador};
$mess[3311] = qq{Fecha salida};
$mess[3312] = qq{Fecha retorno};
$mess[3313] = qq{Precio por d�a};
$mess[3314] = qq{Duraci�n mensual};
$mess[3315] = qq{Precio por mes};
$mess[3316] = qq{Importe global};
$mess[3317] = qq{Seguro incluido};
$mess[3318] = qq{Entrega};
$mess[3319] = qq{Sus medios ida};
$mess[3320] = qq{Recogida};
$mess[3321] = qq{Sus medios vuelta};
$mess[3322] = qq{Muelle};
$mess[3323] = qq{Calcular};
$mess[3324] = qq{M�quina adicional ida};
$mess[3325] = qq{M�quina adicional vuelta};

$mess[3350] = qq{Planning};
$mess[3351] = qq{M�quina(s) alquilada(s)};
$mess[3352] = qq{M�quina(s) en entrega};
$mess[3353] = qq{M�quina(s) en recogida};
$mess[3354] = qq{M�quina(s) en};
$mess[3355] = qq{Modelo};
$mess[3356] = qq{Familia};
$mess[3357] = qq{Elevaci�n};
$mess[3358] = qq{Energ�a};
$mess[3359] = qq{N� Parque};
$mess[3360] = qq{N� Contrato};
$mess[3361] = qq{Cliente};
$mess[3362] = qq{Obra};
$mess[3363] = qq{C.P.};
$mess[3364] = qq{Ciudad};
$mess[3365] = qq{Fecha};
$mess[3366] = qq{POT};
$mess[3367] = qq{Tarifa};
$mess[3368] = qq{Coste};
$mess[3369] = qq{Cociente};
$mess[3370] = qq{Duraci�n};
$mess[3380] = qq{Enl�vement};
$mess[3381] = qq{R�cup�ration};
$mess[3382] = qq{Transfert inter-chantiers};
$mess[3383] = qq{Reprise sur chantier};


$mess[3410] = qq{N�mero};
$mess[3411] = qq{Contrato};
$mess[3412] = qq{Fecha salida};
$mess[3413] = qq{Fecha retorno};
$mess[3414] = qq{N� Parque};
$mess[3415] = qq{Modelo};
$mess[3416] = qq{Obra};
$mess[3417] = qq{C.P.};
$mess[3418] = qq{Ciudad};

$mess[3450] = qq{Cambio delegaci�n};
$mess[3451] = qq{Delegaci�n destino};
$mess[3452] = qq{Est&aacute; en};
$mess[3453] = qq{�Teleportarse!};

$mess[3500] = qq{Cifras del mes};

$mess[3550] = qq{Cifras del mes 2};

$mess[3600] = qq{Cifras global};

$mess[3650] = qq{Hist�rico de cifras};
$mess[3651] = qq{Mes};
$mess[3652] = qq{A�o};
$mess[3653] = qq{Enero};
$mess[3654] = qq{Febrero};
$mess[3655] = qq{Marzo};
$mess[3656] = qq{Abril};
$mess[3657] = qq{Mayo};
$mess[3658] = qq{Junio};
$mess[3659] = qq{Julio};
$mess[3660] = qq{Agosto};
$mess[3661] = qq{Septiembre};
$mess[3662] = qq{Octubre};
$mess[3663] = qq{Noviembre};
$mess[3664] = qq{Diciembre};
$mess[3665] = qq{5};
$mess[3666] = qq{2000};
$mess[3667] = qq{2001};
$mess[3668] = qq{2002};
$mess[3669] = qq{2003};
$mess[3670] = qq{2004};

$mess[3700] = qq{Validar delegaci�n a delegaci�n};
$mess[3701] = qq{M�quina};
$mess[3702] = qq{M�quina(s)};

$mess[3750] = qq{Cambio de delegaci�n a delegaci�n};
$mess[3751] = qq{M�quina};
$mess[3752] = qq{Cami�n};
$mess[3753] = qq{Ch�fer};
$mess[3754] = qq{Delegaci�n destino};
$mess[3755] = qq{M�quina(s)};
$mess[3756] = qq{Cami�n(es)};
$mess[3757] = qq{Ch�fer(es)};
$mess[3758] = qq{Delegaciones};

$mess[3800] = qq{Asignar Cami�n/Ch�fer};
$mess[3801] = qq{Cami�n};
$mess[3802] = qq{Ch�fer};
$mess[3803] = qq{Pareja Cami�n/Ch�fer};
$mess[3804] = qq{Cami�n(es)};
$mess[3805] = qq{Ch�fer(es)};
$mess[3806] = qq{Pareja(s)};

$mess[3850] = qq{Cambio de obra a obra};
$mess[3851] = qq{Transporte};
$mess[3852] = qq{Transporte};
$mess[3853] = qq{N� parque};
$mess[3854] = qq{N�mero};
$mess[3855] = qq{N� contrato};
$mess[3856] = qq{Cliente};
$mess[3857] = qq{Direcci�n};
$mess[3858] = qq{C.P.};
$mess[3859] = qq{Ciudad};
$mess[3860] = qq{Modelo m�quina};
$mess[3861] = qq{N� parque};

$mess[3900] = qq{Carburante};
$mess[3901] = qq{Contrato, Cliente, Modelo, N� parque};
$mess[3902] = qq{Cantidad carburante (L)};

$mess[3950] = qq{Verificaci�n m�quina};
$mess[3951] = qq{M�quina};

#Contr�le Apave
$mess[4000] = qq{Control m�quina};
$mess[4001] = qq{N� Parque};
$mess[4002] = qq{Sis. Traslaci�n m�quina};
$mess[4003] = qq{Familia m�quina};
$mess[4004] = qq{Elevaci�n o Presi�n};
$mess[4005] = qq{Energ�a};
$mess[4006] = qq{Estado M�quina};
$mess[4007] = qq{Modelo};
$mess[4008] = qq{Modelo};
$mess[4009] = qq{N�mero Parque};
$mess[4010] = qq{N�mero Serie};
$mess[4011] = qq{Sis.Traslaci�n};
$mess[4012] = qq{Marca};
$mess[4013] = qq{Elevaci�n};
$mess[4014] = qq{Energ�a};
$mess[4015] = qq{Peso};
$mess[4016] = qq{Largo};
$mess[4017] = qq{Ancho};
$mess[4018] = qq{Capacidad};
$mess[4019] = qq{Estado};
$mess[4020] = qq{Fecha Control};
$mess[4021] = qq{Efectuada};
$mess[4022] = qq{N�mero};
#Consultation Devis
$mess[4050] = qq{Consulta oferta};
$mess[4051] = qq{N� Oferta};
$mess[4052] = qq{Cliente};
$mess[4053] = qq{Estado Oferta};
$mess[4054] = qq{Agente};
$mess[4055] = qq{N� Oferta};
$mess[4056] = qq{Cliente};
$mess[4057] = qq{Obra};
$mess[4058] = qq{Fecha};
$mess[4059] = qq{Modelo};
$mess[4060] = qq{Precio por D�a};
$mess[4061] = qq{Duraci�n};
$mess[4062] = qq{C.T};
$mess[4063] = qq{Importe};
$mess[4064] = qq{Pasar en Contrato};
$mess[4065] = qq{Estado};
$mess[4066] = qq{Agente};
#Remise en cours de contrat
$mess[4100] = qq{Reabrir contrato};
$mess[4101] = qq{Contrato cerrado};
$mess[4102] = qq{Calcular};
#Cr�ation de machine
$mess[4150] = qq{Entrar m�quina};
$mess[4151] = qq{B�squeda de la designaci�n};
$mess[4152] = qq{N� parque};
$mess[4153] = qq{N� serie};
$mess[4154] = qq{Fecha control};
$mess[4155] = qq{Agence de cr�ation};
$mess[4156] = qq{Calcular};
#Facturation
$mess[4200] = qq{Facturaci�n};
$mess[4201] = qq{N� Contrato};
$mess[4202] = qq{Fecha retorno};
$mess[4203] = qq{Delegaci�n};
$mess[4204] = qq{Contacto};
$mess[4205] = qq{Tel�fono Delegaci�n};
$mess[4206] = qq{Delegaci�n};
$mess[4207] = qq{A cerrar};
$mess[4208] = qq{Carburante a entrar};
$mess[4209] = qq{A pasar en facturaci�n};
$mess[4210] = qq{Tel�fono Delegaci�n};
$mess[4211] = qq{Recapitulativo};
$mess[4212] = qq{Facturaci�n semanal};
$mess[4213] = qq{Facturaci�n mensual};
$mess[4214] = qq{Fecha salida};
$mess[4215] = qq{Fecha de creaci�n};
$mess[4216] = qq{Contratos anticipados};
#Energies
$mess[4250] = qq{Bi-Energ�a};
$mess[4251] = qq{Diesel};
$mess[4252] = qq{El�ctrica};
$mess[4253] = qq{Manivela/Manubrio ?};
$mess[4254] = qq{Gaz};
#Familles
$mess[4300] = qq{Brazo Art};
$mess[4301] = qq{Teleporter};
$mess[4302] = qq{Tijera};
$mess[4303] = qq{Brazotel};
$mess[4304] = qq{Brazo con Orugas};
$mess[4305] = qq{Monta-Cargas};
$mess[4306] = qq{M�stil};
$mess[4307] = qq{Personal};
$mess[4308] = qq{Prensas ?!};
$mess[4309] = qq{Manipulador Tel�fonoesc�pico};
$mess[4310] = qq{Diversos};
#Etat
$mess[4350] = qq{Control};
$mess[4351] = qq{Disponible};
$mess[4352] = qq{Lou�e};
$mess[4353] = qq{Panne};
$mess[4354] = qq{R�cuperation};
$mess[4355] = qq{Revisi�n};
$mess[4356] = qq{V�rification};

$mess[4357] = qq{Vendue};
$mess[4358] = qq{En Transfert};
$mess[4359] = qq{Rendue};
$mess[4360] = qq{Restitu�};

$mess[4361] = qq{Pte. recoger};

#Ann�es
$mess[4400] = qq{2001};
$mess[4401] = qq{2000};
$mess[4402] = qq{1999};
$mess[4403] = qq{1998};
#Info client
$mess[4450] = qq{Informaciones Cliente};
$mess[4451] = qq{Cliente};
$mess[4452] = qq{Dirrecci�n};
$mess[4453] = qq{SIREN};
$mess[4454] = qq{NIF};
$mess[4455] = qq{Sector de actividade};
$mess[4456] = qq{Contacto client};
$mess[4457] = qq{Tel�fono};
$mess[4458] = qq{Fax};
$mess[4459] = qq{M�vil};
$mess[4460] = qq{E-mail};
$mess[4461] = qq{Comentario};
$mess[4462] = qq{Estad�sticas};
$mess[4463] = qq{Agencia(s)};
$mess[4476] = qq{Total};
$mess[4483] = qq{Cerrar};

# stati.cgi
#entete
$mess[5000] = qq{Nbr. contratos};
$mess[5001] = qq{Tx util.<br>J+1};
$mess[5002] = qq{TOT};
$mess[5003] = qq{CER};
$mess[5004] = qq{ACT};
$mess[5005] = qq{SEG<br>FACT.};
$mess[5006] = qq{CT<5j};
$mess[5007] = qq{MENS};
$mess[5008] = qq{FORF};
$mess[5009] = qq{Cerrado al J=};
$mess[5010] = qq{Cerrado al fin del mes};
$mess[5011] = qq{CA};
$mess[5012] = qq{TRANSP};
$mess[5013] = qq{GAS};
$mess[5014] = qq{COSTE};
$mess[5015] = qq{CT Dia};
#titre groupe
$mess[5016] = qq{Parque};
$mess[5017] = qq{DIA AL.};
$mess[5018] = qq{% Util};
$mess[5019] = qq{P Dia};
$mess[5020] = qq{COSTE};
$mess[5021] = qq{CT Dia};
#tittre bloc
$mess[5022] = qq{Fam. Contable};
$mess[5023] = qq{Fam. Comercial};
#date
$mess[5024] = qq{Mes en curso};
$mess[5025] = qq{ALQ};
$mess[5026] = qq{d. l.};
# Autres
$mess[5027] = qq{Livr.};
# BC � envoyer/r�ceptionner
$mess[5050] = qq{Orden de Pedido que debe adjuntarse a la factura};
$mess[5051] = qq{N� de Contrato};
$mess[5052] = qq{Ciudad Obra};
$mess[5053] = qq{Cliente};
$mess[5054] = qq{N� Tel};
$mess[5055] = qq{Modelo M�quina};
$mess[5056] = qq{N� Parque};
$mess[5057] = qq{Fecha salida};
$mess[5058] = qq{Fecha retorno};
$mess[5059] = qq{Validado};
$mess[5060] = qq{En espera};
# Paiement � la livraison
$mess[5600] = qq{Pago a la entrega};
$mess[5601] = qq{N� de Contrato};
$mess[5602] = qq{Ciudad Obra};
$mess[5603] = qq{Cliente};
$mess[5604] = qq{N� Tel};
$mess[5605] = qq{Modelo M�quina};
$mess[5606] = qq{N� de Parque};
$mess[5607] = qq{Fecha salida};
$mess[5608] = qq{Fecha retorno};
$mess[5609] = qq{Importe a cuenta};
$mess[5610] = qq{N� a cuenta};
$mess[5611] = qq{Fetcha pago};
$mess[5612] = qq{Typo};
# Paiement � la livraison
$mess[5650] = qq{Pago a la entrega};
$mess[5651] = qq{N� Contrato};
$mess[5652] = qq{Cliente};
$mess[5653] = qq{Codigo Cliente};
$mess[5654] = qq{N� Tel};
$mess[5655] = qq{Fecha salida};
$mess[5656] = qq{Fecha retorno};
$mess[5657] = qq{Importe a cuenta};
$mess[5658] = qq{N� a cuenta};
$mess[5659] = qq{Fetcha pago};
$mess[5660] = qq{Typo};
$mess[5661] = qq{Validado};
# Devis (nouvelle formule)
# En-t�te
$mess[5800] = qq{Oferta};
$mess[5801] = qq{Stock};
$mess[5802] = qq{Clientes};
$mess[5803] = qq{Ciudad obra};
$mess[5804] = qq{Obra};
$mess[5805] = qq{Direcci�n Obra};
$mess[5806] = qq{Contacto Obra};
$mess[5807] = qq{Tel obra};
$mess[5808] = qq{N� pedido};
# Colonnes
$mess[5820] = qq{Precio objetivo};
$mess[5821] = qq{Coste/d�a};
$mess[5822] = qq{Cociente};
$mess[5823] = qq{% util};
$mess[5824] = qq{Cantidad};
$mess[5825] = qq{Tipo Importe};
$mess[5826] = qq{Duraci�n};
$mess[5827] = qq{Fecha previsional};
$mess[5828] = qq{Precio D�a/Mes/Forfait};
$mess[5829] = qq{Cociente};
$mess[5830] = qq{Importe};
$mess[5831] = qq{Entrega};
$mess[5832] = qq{Recogida};
# Totaux
$mess[5850] = qq{Seguro incluido<br>(los precios de cobertura no toman en cuenta los weekends y las fiestas)};
$mess[5851] = qq{Importe seguro};
$mess[5852] = qq{Importe total};
# Pied
$mess[5860] = qq{Comentario};
$mess[5861] = qq{Notas comerciales (no aparecen en la oferta)};
$mess[5862] = qq{Oferta validada};
$mess[5863] = qq{Calcular};
$mess[5864] = qq{Validar};
# Infos ratio Pr�-contrat
$mess[5900] = qq{Tasa de utilizaci�n};
$mess[5901] = qq{Duraci�n};
$mess[5902] = qq{Coste/d�a};
$mess[5903] = qq{Cociente objetivo};
$mess[5904] = qq{Precio objetivo/d�a};
$mess[5905] = qq{Precio objetivo/mes};
$mess[5906] = qq{Cociente};
# L�gende clients
$mess[6000] = qq{red};
$mess[6001] = qq{green};
$mess[6010] = qq{Cliente en rojo};
$mess[6011] = qq{Cliente en verde};
$mess[6020] = qq{Cliente que no hace volumen de negocios desde m�s de 14 meses};
$mess[6021] = qq{Cliente bloqueado en curso};
$mess[6022] = qq{Desbloquear el cliente para poder prolongar el contrato};
# Machine hors parc
$mess[6050] = qq{Parque normal};
$mess[6051] = qq{Fuera del parque};
#modifiarret modif2.cfg
$mess[6100] = qq{Modificar/Cerrar contrato};
$mess[6101] = qq{N�mero};
$mess[6102] = qq{Estado contrato};
$mess[6103] = qq{N� contrato};
$mess[6104] = qq{Indice contrato};
$mess[6105] = qq{Cliente};
$mess[6106] = qq{Obra};
$mess[6107] = qq{Ciudad obra};
$mess[6108] = qq{Contacto obra};
$mess[6109] = qq{Tel�fono obra};
$mess[6110] = qq{Contacto commande};
$mess[6111] = qq{Tel�fono contacto commande};
$mess[6112] = qq{Modelo m�quina};
$mess[6113] = qq{N� parque};
$mess[6114] = qq{Fuera del parque};
$mess[6115] = qq{Fecha salida};
$mess[6116] = qq{Fecha retorno};
$mess[6117] = qq{Duraci�n};
$mess[6118] = qq{Sumar d�a};
$mess[6119] = qq{Restar d�a};
$mess[6120] = qq{Precio por d�a};
$mess[6121] = qq{Precio por mes};
$mess[6122] = qq{Forfait};
$mess[6123] = qq{Seguro incluido};
$mess[6124] = qq{Importe seguro};
$mess[6125] = qq{Entrega};
$mess[6126] = qq{Recogida ida};
$mess[6127] = qq{Recogida};
$mess[6128] = qq{Recogida vuelta};
$mess[6129] = qq{Muelle};
$mess[6130] = qq{Nettoyage};
$mess[6131] = qq{Libell� du service};
$mess[6132] = qq{Montant service};
$mess[6133] = qq{Importe total IVA excluido};
$mess[6134] = qq{Pedido};
$mess[6135] = qq{Comentario};
$mess[6136] = qq{Notas};
$mess[6137] = qq{Pago a la entrega};
$mess[6138] = qq{Cliente};
$mess[6139] = qq{N� Parque};
$mess[6140] = qq{Este contrato ya ha sido cerrado};
$mess[6141] = qq{El Tipo de m�quina es cambiado};
$mess[6142] = qq{El contrato se ha cancelado};
$mess[6143] = qq{Accesorios ataron al contrato};
$mess[6144] = qq{Accesorio (N� Parque)};
$mess[6145] = qq{Contrato (Estado)};
$mess[6146] = qq{Precio};
$mess[6147] = qq{Fecha salida};
$mess[6148] = qq{Fecha salida};
$mess[6149] = qq{La m�quina alquil� (elogiado) sin el accesorio por omisi�n(defecto)};
$mess[6150] = qq{Contrato de origen};
# Contrat imprim�
$mess[6200] = qq{Alquiler d�a IVA excluido};
$mess[6201] = qq{Alquiler mensual IVA excluido};
$mess[6202] = qq{Alquiler forfait IVA excluido};
# Assurance
$mess[6300] = qq{Facturado};
$mess[6301] = qq{No facturado};
$mess[6302] = qq{Por el cliente};
$mess[6303] = qq{Incluido};
# BL-BR
$mess[6400] = qq{Ninguna m�quina fue asignada sobre el contrato n� numcont.<BR>Debe asignarla para terminar la ruta.};
$mess[6401] = qq{Ninguna m�quina fue asignada sobre los contratos n� numcont.<BR>Debe asignarlas para terminar la ruta.};

# Modification de contrat
$mess[6500] = qq{Modificaci�n de contrato};
# Version tableau
# Colonnes
$mess[6510] = qq{N� contrato};
$mess[6511] = qq{Estado};
$mess[6512] = qq{Cliente};
$mess[6513] = qq{Obra};
$mess[6514] = qq{Pedido por};
$mess[6515] = qq{Contacto obra};
$mess[6516] = qq{M�quina};
$mess[6517] = qq{Duraci�n};
$mess[6518] = qq{Sumar d�a};
$mess[6519] = qq{Restar d�a};
$mess[6520] = qq{Precio};
$mess[6521] = qq{Seguro};
$mess[6522] = qq{Transporte};
$mess[6523] = qq{Recogida};
$mess[6524] = qq{Muelle};
$mess[6525] = qq{Importe total IVA excluido};
$mess[6526] = qq{Pedido};
$mess[6527] = qq{Comentario};
$mess[6528] = qq{Notas};
# Autres
$mess[6550] = qq{fuera del parque};
$mess[6551] = qq{Entrega};
$mess[6552] = qq{Recogida};
$mess[6553] = qq{/ d�a};
$mess[6554] = qq{/ mes};
$mess[6555] = qq{forfait};
$mess[6556] = qq{d�a};
$mess[6557] = qq{d�as};
# Affichage contrat
$mess[6600] = qq{Cliente};
$mess[6601] = qq{Nombre};
$mess[6602] = qq{Precio alquiler};
$mess[6603] = qq{Pedido por};
$mess[6604] = qq{Obra};
$mess[6605] = qq{Nombre};
$mess[6606] = qq{Ciudad};
$mess[6607] = qq{Contacto obra};
$mess[6608] = qq{M�quina};
$mess[6609] = qq{Modelo};
$mess[6610] = qq{Energ�a};
$mess[6611] = qq{N� parque};
$mess[6612] = qq{Fuera del parque};
$mess[6613] = qq{Tasa de utilizaci�n};
$mess[6614] = qq{Coste};
$mess[6615] = qq{Cociente objetivo};
$mess[6616] = qq{Precio objetivo};
$mess[6617] = qq{Cociente};
$mess[6618] = qq{Duraci�n};
$mess[6619] = qq{Fecha salida};
$mess[6620] = qq{Fecha retorno};
$mess[6621] = qq{o sea};
$mess[6622] = qq{d�a};
$mess[6623] = qq{d�as};
$mess[6624] = qq{laborable};
$mess[6625] = qq{laborables};
$mess[6626] = qq{calendario};
$mess[6627] = qq{calendarios};
$mess[6628] = qq{Sumar d�a};
$mess[6629] = qq{Restar d�a};
$mess[6630] = qq{Tarifa};
$mess[6631] = qq{Precio};
$mess[6632] = qq{por d�a};
$mess[6633] = qq{por mes};
$mess[6634] = qq{forfait};
$mess[6635] = qq{Seguro};
$mess[6636] = qq{Transporte};
$mess[6637] = qq{Entrega};
$mess[6638] = qq{Recogida};
$mess[6639] = qq{Sus medios};
$mess[6640] = qq{Muelle};
$mess[6641] = qq{Servicios complementarios};
$mess[6642] = qq{A�adir un servicio};
$mess[6643] = qq{Suprimir un servicio};
$mess[6644] = qq{Importe total IVA excluido};
$mess[6645] = qq{Informaci�n complementaria};
$mess[6646] = qq{Pedido};
$mess[6647] = qq{Comentario};
$mess[6648] = qq{Notas};
$mess[6649] = qq{Recalcular};
$mess[6650] = qq{Modificar el contrato};
$mess[6651] = qq{Cerrar el contrato};
$mess[6652] = qq{Pasar el contrato en facturaci�n};
$mess[6653] = qq{Contrato de origen};
$mess[6654] = qq{Accesorios};
$mess[6655] = qq{del};
$mess[6656] = qq{al};
$mess[6657] = qq{/ d�a};
$mess[6658] = qq{Cancelado};
$mess[6659] = qq{Facturado};
$mess[6660] = qq{Tarifa};
$mess[6661] = qq{Varios};
$mess[6662] = qq{Facturado en final de contrato};
$mess[6663] = qq{Elija un servicio en la lista};
$mess[6664] = qq{Enteramente facturado};
$mess[6665] = qq{Facturado parcialmente hasta el};
$mess[6666] = qq{No facturado};
$mess[6667] = qq{Ver la tarifa};
$mess[6668] = qq{Carburante};
$mess[6669] = qq{Cantidad};
$mess[6670] = qq{litro};
$mess[6671] = qq{litros};
$mess[6672] = qq{Importe};
$mess[6673] = qq{Proveedor};
$mess[6674] = qq{ya facturado};
$mess[6675] = qq{ya facturados};
$mess[6676] = qq{Entrega no planeada o no validada};
$mess[6677] = qq{Reducci�n};
# Messages de confirmation
$mess[6750] = qq{Imposible de cerrar el contrato m�s de 4 d�as antes de la fecha de vuelta};
$mess[6751] = qq{Se cerr� el contrato};
$mess[6752] = qq{Se cerr� el contrat fils};
$mess[6753] = qq{Este contrato se puede facturar y ya no se puede modificar};
$mess[6754] = qq{El contrato hijo est� dispuesto facturarse};
$mess[6755] = qq{El contrato hijo est� actualizado};
$mess[6756] = qq{El contrato est� actualizado};
$mess[6757] = qq{Fecha de retorno previa a la fecha de �ltima facturaci�n};
$mess[6758] = qq{Fecha de retorno incorrecta};
$mess[6759] = qq{Fecha de salida incorrecta};
$mess[6760] = qq{Fecha de retorno previa a la fecha inicial};
$mess[6761] = qq{Imposible de pasar el contrato en facturaci�n ya que el carburante no se ha agarrado};
$mess[6762] = qq{Imposible de pasar el contrato en facturaci�n ya que no se plane� la recogida};
$mess[6763] = qq{Imposible de pasar el contrato en facturaci�n ya que la reducci�n excepcional no fue validada};
$mess[6764] = qq{S�lo los d�as de m�s y los cargos complementarios que no tengan importe negativo se pueden indicar};
$mess[6765] = qq{No se puede indicar un cargo con importe negativo};
$mess[6766] = qq{Montant du transport inf�rieur au forfait machine suppl�mentaire minimum.};
$mess[6767] = qq{Montant du service inf�rieur au montant minimum autoris� : };

# Remise en cours de contrat (nouvelle version)
$mess[6800] = qq{Reapertura de contrato};
# Recherche
$mess[6810] = qq{Contrato que reabrir};
$mess[6811] = qq{Reabrir el contrato};
# Messages validation
$mess[6820] = qq{El contrato fue reabierto};
$mess[6821] = qq{Seleccione un contrato que reabrir};
$mess[6822] = qq{El contrato ya fue reabierto};

# Documents types
$mess[6900] = qq{Documentos modelo};
$mess[6901] = qq{Descargar};
$mess[6910] = qq{Carpeta};
$mess[6911] = qq{Tipo de documento desconocido};
$mess[6912] = qq{Documento Microsoft Word};
$mess[6913] = qq{Documento Microsoft Excel};
$mess[6914] = qq{Documento Acrobat PDF};
$mess[6915] = qq{Imagen JPEG};
$mess[6916] = qq{Imagen GIF};
$mess[6917] = qq{Imagen BMP};
$mess[6918] = qq{Imagen TIFF};

# Remise exceptionnelle
$mess[6950] = qq{Reducci�n excepcional};
$mess[6951] = qq{Precio antes de la reducci�n};
$mess[6952] = qq{Reducci�n excepcional};
$mess[6953] = qq{Precio con reducci�n};
$mess[6954] = qq{Justificaci�n};
$mess[6955] = qq{Redacci�n factura};
$mess[6956] = qq{Reducci�n aceptada};
$mess[6957] = qq{Reducci�n en espera de validaci�n};
$mess[6958] = qq{Reducci�n rechazada};
$mess[6959] = qq{Despu�s reducc. excepcional de };   # Lib impression contrat
$mess[6960] = qq{Despu�s reducc. excepcional de };   # Lib impression devis
$mess[6961] = qq{pedido};
$mess[6962] = qq{Tratada por};
$mess[6963] = qq{Limitado a 36 caracteres};
$mess[6964] = qq{Reducci�n superior al 100%};
$mess[6965] = qq{Precio neto};

# Modification du tarif
$mess[7000] = qq{Tarifa b�sica};
$mess[7001] = qq{Precio al d�a (IVA excluido)};
$mess[7002] = qq{TARIFA B�SICA};
$mess[7003] = qq{Abril 2006};
$mess[7004] = qq{a};
$mess[7005] = qq{m�s de};

# Chargement de la page
$mess[7050] = qq{Cargamento de la p�gina en curso};
$mess[7051] = qq{Cargamento de la p�gina};

# Modification commande transport
$mess[7100] = qq{Modificaci�n de transporte externo};
$mess[7101] = qq{Control de transporte externo};
#
$mess[7110] = qq{N� pedido};
$mess[7111] = qq{Transportista};
$mess[7112] = qq{N� contrato};
$mess[7113] = qq{Fecha de transporte};
$mess[7114] = qq{Consultar el pedido};
$mess[7115] = qq{Salvar las modificaciones};
$mess[7116] = qq{Atenci�n};
$mess[7118] = qq{No hay l�nea para este pedido};
$mess[7119] = qq{Pedido};
$mess[7120] = qq{Agencia};
$mess[7121] = qq{Establecido por};
$mess[7123] = qq{Tipo};
$mess[7124] = qq{Fecha control factura};
$mess[7125] = qq{N� factura};
$mess[7126] = qq{Importe facturado};
$mess[7127] = qq{Supprimir el pedido};
$mess[7128] = qq{El pedido se ha suprimido};
$mess[7129] = qq{Nueva b�squeda};
$mess[7130] = qq{Se modific� el pedido};
$mess[7131] = qq{La fecha de transporte no es v�lida:};
$mess[7132] = qq{Pedido no controlado};
$mess[7133] = qq{Entre el};
$mess[7134] = qq{y el};
$mess[7135] = qq{=};
$mess[7136] = qq{&ge;};
$mess[7137] = qq{La fecha no es v�lida:};
$mess[7138] = qq{Pedidos controlados};
$mess[7139] = qq{No};
$mess[7140] = qq{Si};
$mess[7141] = qq{Indiferente};

# Contrat cadre
$mess[7150] = qq{Contrat Cadre};

# Suffixe Sous-location et Full service
$mess[7200] = qq{RA};
$mess[7201] = qq{AL};

# Commandes de transport (nouvelle interface)
$mess[7300] = qq{Nuevo pedido};
$mess[7301] = qq{A�adir un nuevo pedido};
$mess[7302] = qq{Seleccionar/Deseleccionar todos};
$mess[7303] = qq{Validar las modificaciones};
$mess[7304] = qq{Imprimir el pedido};
$mess[7305] = qq{Ruta};
$mess[7306] = qq{Crear la ruta};
$mess[7307] = qq{Elija un transportista};
$mess[7308] = qq{Se crea el pedido #XXX#};
$mess[7309] = qq{Se registran las modificaciones};
$mess[7310] = qq{Se crea la ruta #XXX#};
$mess[7311] = qq{Existe otras rutas para el mismo transportista que no podr�n ordenarse mientras �sta no lo ser�};
$mess[7312] = qq{No hay actualmente ning�n transporte que asignar};
$mess[7313] = qq{Actualizar};
$mess[7314] = qq{Retirarse del pedido};
$mess[7315] = qq{Ya se crea la ruta (n� #XXX#)};
$mess[7316] = qq{Imposible modificar el pedido n� #YYY# ya que ya existe una ruta (n� #XXX#)};
$mess[7317] = qq{Atenci�n, cualquier pedido hecho en ruta no es modificables};
$mess[7318] = qq{Attention, au moins un contrat en erreur (ligne en rouge) a une commande de transport. R�gulariser le(s) pour pouvoir finaliser la(les) commande(s) correspondante(s)};


# Remise en arr�t de contrat
$mess[7400] = qq{Cierre de contrato};
# Recherche
$mess[7410] = qq{Contrato para cerrar};
$mess[7411] = qq{Cerrar el contrato};
# Messages validation
$mess[7420] = qq{El contrato se ha cerrado};
$mess[7421] = qq{Gracias por seleccionar un contrato para cerrar};
$mess[7422] = qq{El contrato ya se ha cerrado};
$mess[7423] = qq{El contrato no se puede cerrar :};
$mess[7424] = qq{ - el contrato no est� 'puesto en facturaci�n'};
$mess[7425] = qq{ - el contrato ya ha sido totalmente facturado};
# L�gende
$mess[7430] = qq{Estado de facturaci�n};
$mess[7431] = qq{Facturaci�n Parcial};
$mess[7432] = qq{Facturaci�n Parcial Final (alquiler totalmente cobrado)};
$mess[7433] = qq{Facturaci�n Final (contrato totalmente cobrado)};

# Remise en cours du pointage d'une machine dans une tourn�e
$mess[7500] = qq{Modificaci�n de la validaci�n de una m�quina en ruta};
$mess[7501] = qq{Selecci�n};
$mess[7502] = qq{Buscar};
# Recherche
$mess[7510] = qq{Contrato};
$mess[7511] = qq{M�quina};
$mess[7512] = qq{Anulada};
$mess[7513] = qq{Efectuada};
$mess[7514] = qq{Reatribuido};
$mess[7515] = qq{Vigente};
$mess[7516] = qq{No atribuido};
$mess[7517] = qq{Modificar la validaci�n};
# Messages validation
$mess[7520] = qq{La validaci�n de la m�quina en ruta ha sido modificada};
$mess[7521] = qq{Se tiene que indicar un n� de contrato  y/o una m�quina};
$mess[7522] = qq{La validaci�n de la m�quina en ruta no se puede modificar :};
$mess[7523] = qq{ - imposible con un };
$mess[7524] = qq{ - validaci�n actual : };
$mess[7525] = qq{La validaci�n de la m�quina en ruta ya ha sido modificada};
$mess[7526] = qq{Se tiene que seleccionar una l�nea de ruta para modificar};
$mess[7527] = qq{ - el estado actual de la m�quina no lo permite};
$mess[7528] = qq{ - el contrato est� en estado "puesto en facturaci�n"};
$mess[7529] = qq{ - el contrato est� totalement facturado};
$mess[7540] = qq{ - posible s�lo en el �ltimo contrato};
# Colonnes du tableau
$mess[7530] = qq{Ruta};
$mess[7531] = qq{Contrato};
$mess[7532] = qq{Estado contrato};
$mess[7533] = qq{M�quina};
$mess[7537] = qq{Estado m�quina};
$mess[7534] = qq{Tipo};
$mess[7535] = qq{Validaci�n};
$mess[7536] = qq{S�lo las l�neas del �ltimo contrato de tipo: "Entrega" o "Recuperaci�n"  y validadas como: "Cancelado" o "Efectuado" se pueden seleccionar};

# Mise � jour des alertes et des stocks
$mess[7600] = qq{Actualizaci�n de las alertas y stocks};
$mess[7601] = qq{Para realizar cambios en la agencia %s : };

# Visu des transports par r�gion
$mess[7620] = qq{Visualisation des transports sur la r�gion};
$mess[7621] = qq{R�-actualiser};
$mess[7622] = qq{OUI};
$mess[7623] = qq{non};

# Visu des transports par r�gion
$mess[7649] = qq{Porcentaje de utilizaci�n a %s - };
$mess[7650] = qq{Porcentaje de utilizaci�n a %s - Zona };
$mess[7651] = qq{Familia tarifaria};
$mess[7652] = qq{% util. inst. (n�mero m�quinas)};
$mess[7653] = qq{Regional};
$mess[7654] = qq{Nacional};
$mess[7655] = qq{TOTAL};
$mess[7656] = qq{No tienen acceso a esta funcionalidad.};

# Report des libell�s de l'interface "Nouveau client"
$mess[7700] = qq{APE};
$mess[7701] = qq{Actividad};
$mess[7702] = qq{C.I.F. / NIF};
$mess[7703] = qq{Clientes};
$mess[7704] = qq{Direcci�n};
$mess[7705] = qq{Comp. direcci�n};
$mess[7706] = qq{Ciudad};
$mess[7707] = qq{Tel};
$mess[7708] = qq{M�vil};
$mess[7709] = qq{Fax};
$mess[7710] = qq{Contacto};
$mess[7711] = qq{Comentario};
$mess[7712] = qq{Forma de pago};
$mess[7713] = qq{Banco};
$mess[7714] = qq{C.C.C.};
$mess[7715] = qq{Precio alquiler};
$mess[7716] = qq{Grupo};
$mess[7717] = qq{Calificaci�n};
$mess[7718] = qq{Email Cliente};
$mess[7719] = qq{Creador};
$mess[7720] = qq{Agencia creadora};
$mess[7721] = qq{Siempre proporcionar los documentos de la m�quina};
$mess[7800] = qq{�mbito nacional};

# Pro forma
$mess[8000] = qq{Leyenda para los casos err�neos};
$mess[8001] = qq{La pro forma no ha sido generada};
$mess[8002] = qq{La pro forma no ha sido actualizada};
$mess[8003] = qq{La pro forma no est� finalizada};
$mess[8004] = qq{Rebaja excepcional de alquiler en espera};
$mess[8005] = qq{Rebaja excepcional de transporte en espera};
$mess[8006] = qq{Cliente con apertura de cuenta en espera};

$mess[8100] = qq{Imposible planificar el transporte sobre este contrato #XXX#};
$mess[8101] = qq{Error sobre el contrato #XXX#};
$mess[8102] = qq{El contrato lleva una factura pro forma o una factura de anticipo que no seran validas despu�s de la confirmaci�n};
$mess[8103] = qq{El cliente tiene facturas pro forma o anticipos de facturas que no ser�n validos despu�s de la confirmaci�n sobre los documentos siguientes};

return @mess;
}

sub liste_pt {
my @mess;
#message
#sub saisie
$mess[0] = qq{seguro};
$mess[1] = qq{incluido};
$mess[2] = qq{procura};
$mess[3] = qq{ n�o };
$mess[4] = qq{procurar};
$mess[5] = qq{queira aproveitar este campo};
$mess[6] = qq{novo cliente};
$mess[7] = qq{queira aproveitar estes campos};
$mess[8] = qq{Queira aproveitar uma tarifa di�ria ou uma empreitada ou uma tarifa mensal e uma de dura��o mensal};
$mess[9] = qq{N�o informado};
$mess[10] = qq{Condi��es gerais de venda a aprovarem};
$mess[11] = qq{Enviar uma abertura de conta};
$mess[12] = qq{ara um link para contato com o Servi�o de Apoio Comercial};
#sub traitment
$mess[50] = qq{a data do in�cio � anterior � de hoje};
$mess[51] = qq{a data do fim � anterior � data do in�cio};
$mess[52] = qq{m� data};
$mess[53] = qq{Data de in�cio anterior a hoje.\\nConservar esta data?};
$mess[54] = qq{n�mero de dias feriados};
$mess[55] = qq{a dura��o};
$mess[56] = qq{montante em �};
$mess[57] = qq{montante do seguro em �};
$mess[58] = qq{valida��o};
$mess[59] = qq{duplicar contrato};
$mess[60] = qq{o contracto};
$mess[61] = qq{est� devedor, duplica��o de dados};
$mess[62] = qq{Data n�o v�lida};
#sub recherche2
$mess[100] = qq{o n�mero de resultados da vossa investiga��o � de};
$mess[101] = qq{Procura};
$mess[102] = qq{procurar};
$mess[103] = qq{queira aproveitar um dos campos};
$mess[104] = qq{ valide a sua procura};
$mess[105] = qq{resultado};
$mess[106] = qq{resultados};
$mess[107] = qq{Retomar a procura};
$mess[108] = qq{Retorno};
#sub traitment2
$mess[150] = qq{o n�mero de resultados da vossa procura};
$mess[151] = qq{n�o h� resultados na vossa procura};
$mess[152] = qq{recolha};
$mess[153] = qq{ver a oferta};
$mess[154] = qq{Controlador};
$mess[155] = qq{ paragem contratual};
$mess[156] = qq{modificar o contrato};
$mess[157] = qq{anular um contrato};
$mess[158] = qq{dia mais/menos};
$mess[159] = qq{duplica��o de dados};
$mess[160] = qq{mudan�a de obra};
$mess[161] = qq{modificar as informa��es do cliente};
$mess[162] = qq{ver o contracto};
$mess[163] = qq{reservar o modelo};
$mess[164] = qq{encontrar};
$mess[165] = qq{bloqu�};
$mess[166] = qq{d�bloqu�};
$mess[167] = qq{Client <ste>(<code>) <libbloc>};
$mess[168] = qq{Bonjour\nle Client <ste>(<code>) de <cp>,<ville>\nest <libbloc>\n@+\nBonne journ�e\n};
#sub stock
$mess[200] = qq{reservar o modelo};
#sub nvclient
$mess[250] = qq{verifique o RIB};
$mess[251] = qq{RIB};
$mess[252] = qq{correcto};
$mess[253] = qq{incorrecto};
$mess[254] = qq{novo cliente};
$mess[255] = qq{procura Lycos! Procura};
#sub bas form hauttab
$mess[300] = qq{imprimir};
$mess[301] = qq{fechar};
#sub saisiecnt
$mess[350] = qq{o prospecto};
$mess[351] = qq{c�digo cliente};
$mess[352] = qq{da ag�ncia};
$mess[353] = qq{NIF};
$mess[354] = qq{RIB ?};
$mess[355] = qq{telefone};
$mess[356] = qq{Fax};
$mess[357] = qq{torna-se um cliente de Acc�s Industrie};
$mess[358] = qq{para o contracto N�};
$mess[359] = qq{quem come�a o};
$mess[360] = qq{torna-se um cliente};
$mess[361] = qq{N� APE ?};
#sub traitnvclient
$mess[400] = qq{origem};
$mess[401] = qq{documento de origem};
$mess[402] = qq{oferta};
$mess[403] = qq{pr�-contracto};
$mess[404] = qq{N� APE ?};
$mess[405] = qq{Siret ?};
$mess[406] = qq{cliente};
$mess[407] = qq{morada};
$mess[408] = qq{localidade};
$mess[409] = qq{telefone};
$mess[410] = qq{telemovel};
$mess[411] = qq{Fax};
$mess[412] = qq{contacto};
$mess[413] = qq{Coment�rio};
$mess[414] = qq{banco};
$mess[415] = qq{RIB};
$mess[416] = qq{modo de pagamento};
$mess[417] = qq{novo cliente de};
$mess[418] = qq{Sector de actividade};
#sub arretmodif
$mess[450] = qq{n� de dias feriados};
$mess[451] = qq{a data do fim � anterior � data do in�cio};
$mess[452] = qq{o contracto};
$mess[453] = qq{est� parado};
$mess[454] = qq{est� modificado};
#sub transport
$mess[500] = qq{montante};
$mess[501] = qq{contrato a editar};
$mess[502] = qq{colectivo};
$mess[503] = qq{c�digo do cliente};
$mess[504] = qq{cliente validado};
$mess[505] = qq{transporte a atribuir};
$mess[506] = qq{Or�amento transportador};
$mess[507] = qq{pedido de transportador};
$mess[508] = qq{organizar a rota};
$mess[509] = qq{data de transporte};
$mess[510] = qq{imprimir o contrato};
$mess[511] = qq{novo cliente};
$mess[512] = qq{Avaria m�quina};
$mess[513] = qq{Impossible de supprimer le transporteur sur le contrat};
#sub traitattmach
$mess[550] = qq{a m�quina};
$mess[551] = qq{est� selecionado pelo menos duas vezes};
$mess[552] = qq{A m�quina %s n�o tem o mesmo modelo que o contrato %s que est� a tentar atribuir};
$mess[553] = qq{Esta m�quina j� est� atribu�da};
#sub attmach
$mess[600] = qq{Queira aproveitar um n�mero de parque dispon�vel};
$mess[601] = qq{este numero � incompativel com o parque actual da empresa};
$mess[602] = qq{atribuir m�quina};
$mess[603] = qq{imprimir lista m�quina};
#sub carburant
$mess[650] = qq{contrato,designa��o};
$mess[651] = qq{atribuir combustivel};
#sub transfert
$mess[700] = qq{m�quina a recolher};
$mess[701] = qq{transferir};
$mess[702] = qq{passar GE/GR/ pedido de recolha};
$mess[703] = qq{m�quina a entregar};
$mess[704] = qq{transporte a atribuir};
$mess[705] = qq{n� parque};
#sub tournee
$mess[750] = qq{turno N�};
$mess[751] = qq{resto da capacidade em Kg. Antes de L/R/T};
$mess[752] = qq{em curso};
$mess[753] = qq{efectuada};
$mess[754] = qq{falhado};
$mess[755] = qq{capacidade};
$mess[756] = qq{resto};
$mess[757] = qq{total m�quinas a recolher};
$mess[758] = qq{total m�quinas a entregar};
$mess[759] = qq{passar GE/GR/ pedido de recolha};
$mess[760] = qq{validar o turno};
$mess[761] = qq{o controle de turno};
$mess[762] = qq{n�o � completo, };
$mess[763] = qq{m�quinas a registrar};
$mess[764] = qq{o controle do turno N�};
$mess[765] = qq{est� completo};
$mess[780] = qq{Data turno};
#sub valtourn
$mess[800] = qq{a volta N�};
$mess[801] = qq{dever� ser validado};
$mess[802] = qq{est� validado};
#sub verification
$mess[850] = qq{data de controle: < 2 m�s(*), < 15 dias(**), < 1 semana (***), expirado(****)};
$mess[851] = qq{M�quinas em entrega};
$mess[852] = qq{M�quina em transfer de obra a obra};
$mess[853] = qq{m�quina em entrega};
$mess[854] = qq{m�quina em recupera��o};
$mess[855] = qq{m�quina em transfer�ncia};
$mess[856] = qq{m�quina em verifica��o};
$mess[857] = qq{verifica��o � revis�o};
$mess[858] = qq{revis�o � verifica��o};
$mess[859] = qq{disponivel para verifica��o};
$mess[860] = qq{avariada a verifica��o};
$mess[861] = qq{verfica��o a avarias};
$mess[862] = qq{m�quina em revis�o};
$mess[863] = qq{Avaria � Revis�o};
$mess[864] = qq{revis�o � avaria};
$mess[865] = qq{m�quina com avaria};
$mess[866] = qq{revis�o � disponibilidade};
$mess[867] = qq{verifica��o a dispon�veis};
$mess[868] = qq{avariada a dispon�vel};
$mess[869] = qq{m�quina dispon�vel};
$mess[870] = qq{estado};
$mess[871] = qq{impress�o lista m�quina};
$mess[872] = qq{m�quinas em recolha};
$mess[873] = qq{M�quina em transfer de Ag. � Ag.};
$mess[874] = qq{Impression envoy�e sur l'imprimante};
#sub devis
$mess[900] = qq{o or�amento};
$mess[901] = qq{� registado};
$mess[902] = qq{Historial};
$mess[903] = qq{novo cliente};
$mess[904] = qq{seguro};
$mess[905] = qq{ n�o };
$mess[906] = qq{incluido};
$mess[907] = qq{passar para pr�-contracto};
$mess[908] = qq{Seguro compreendido};
$mess[909] = qq{Excluindo seguro};
$mess[910] = qq{modificar a validade da oferta};
$mess[911] = qq{abandonar};
$mess[912] = qq{impress�o da oferta};
$mess[913] = qq{Envio por telefax ou impress�o local};
$mess[914] = qq{Erro};
$mess[915] = qq{Previsualiza��o do ficheiro Acrobat};
$mess[916] = qq{Nenhuma p�gina a afixar};
$mess[917] = qq{A zona � obrigat�ria};
$mess[918] = qq{Deve fazer uma estimativa do transporte.};
$mess[919] = qq{Impress�o imposs�vel: diminui��o excepcional transporte em espera ou transporte incorrecto.};
$mess[920] = qq{Impress�o imposs�vel: diminui��o excepcional em espera.};
$mess[921] = qq{s�o certos querer anular a diminui��o excepcional?};
$mess[922] = qq{anular a diminui��o excep.};
$mess[923] = qq{Cart�o de cliente};
#sub traitdevis
$mess[950] = qq{o or�amento};
$mess[951] = qq{passa a ser um contrato};
$mess[952] = qq{est� abandonado};
$mess[953] = qq{Escolha um motivo de abandono};
$mess[954] = qq{Outro};
$mess[955] = qq{Selecione um motivo antes de abandonar a oferta};
$mess[956] = qq{Oferta abandonada};
$mess[957] = qq{Objecto : Vossa pedido de cota��o de aluguer ACCES INDUSTRIE};
$mess[958] = qq{Ex.ma(o) senhora, senhor, <br><br>
Em seguimento ao vosso pedido, encontar�o em anexo, o or�amento para aluguer, 
feito de acordo com as informa��es que nos comunicaram.<br>
Estou � vossa disposi��o : _TELAG_ para informa��es complementares.<br>
Esperando ter respondido �s vossas expectativas, apresento sinceros cumprimentos, atenciosamente, ex.ma(o)senhora,
senhor,<br><br>
  _NOMEXP_};
#sub ville
$mess[1000] = qq{Ag�ncia(s)};
$mess[1001] = qq{ ou };
#sub facturation
$mess[1050] = qq{Somos o};
$mess[1051] = qq{contratos a facturar};
$mess[1052] = qq{facturar};
$mess[1053] = qq{a factura��o de};
$mess[1054] = qq{m�s j� efectuado};
$mess[1055] = qq{h�};
$mess[1056] = qq{contratos a passarem em factura��o};
$mess[1057] = qq{na factura};
$mess[1058] = qq{exporta��o para Sage};
$mess[1059] = qq{A factura��o ir�};
$mess[1060] = qq{A factura��o j� tem sido efectuada};
$mess[1061] = qq{a apreender};
$mess[1062] = qq{at� o};
$mess[1063] = qq{Contratos a parar};
$mess[1064] = qq{Combust�veis a apreender};
#sub traitfac
$mess[1100] = qq{o montante de};
$mess[1101] = qq{est� de};
$mess[1102] = qq{para};
$mess[1103] = qq{facturas};
$mess[1104] = qq{o montante total das facturas � de};
#sub tableau
$mess[1150] = qq{separar por data};
$mess[1151] = qq{separar por cidade};
$mess[1152] = qq{revis�o};
$mess[1153] = qq{avaria};
$mess[1154] = qq{controle};
$mess[1155] = qq{dispon�vel};
$mess[1156] = qq{total de m�quinas};
$mess[1157] = qq{estado};
$mess[1158] = qq{impress�o lista m�quina};
$mess[1159] = qq{verifica��o};
#sub transfagag
$mess[1200] = qq{transferir};
#sub validnvclient
$mess[1250] = qq{o c�digo do cliente};
$mess[1251] = qq{o colectivo};
$mess[1252] = qq{entre};
$mess[1253] = qq{a direito};
#sub mois_stat
$mess[1300] = qq{Historial};
#sub stat_mois
$mess[1350] = qq{In�cio do m�s };
$mess[1351] = qq{ e fim de meses};
$mess[1352] = qq{n�mero};
$mess[1353] = qq{Total};
$mess[1354] = qq{meio de pagamento};
$mess[1355] = qq{desvio tipo};
$mess[1356] = qq{CA ag�ncia};
$mess[1357] = qq{dura��o};
$mess[1358] = qq{seguro n�o incluido};
$mess[1359] = qq{transporte};
$mess[1360] = qq{sub contracto de transporte};
$mess[1361] = qq{combustivel};
$mess[1362] = qq{contrato activo};
$mess[1363] = qq{contrato parado};
$mess[1364] = qq{contrato de menos de 5 dias};
$mess[1365] = qq{contrato de mais de 5 dias};
$mess[1366] = qq{contrato mensal};
$mess[1367] = qq{empreitada};
$mess[1368] = qq{fam�lias};
$mess[1369] = qq{CA};
$mess[1370] = qq{n� parque};
$mess[1371] = qq{dia alugado};
$mess[1372] = qq{n� contrato};
$mess[1373] = qq{dias do m�s};
$mess[1374] = qq{pre�o m�dio de aluguer};
$mess[1375] = qq{pre�o m�dio do parque};
$mess[1376] = qq{Rentabilidade em %};
$mess[1377] = qq{Ratio};
$mess[1378] = qq{m�dia de dias};
$mess[1379] = qq{TOTAL};
$mess[1380] = qq{designa��o};
#sub nomade
$mess[1400] = qq{eu estou em};
$mess[1401] = qq{Ag�ncias};
$mess[1402] = qq{desloca��o};
#sub saisiemach
$mess[1450] = qq{a m�quina};
$mess[1451] = qq{est� devedor};
$mess[1452] = qq{parque};
$mess[1453] = qq{ou};
$mess[1454] = qq{de s�rie};
$mess[1455] = qq{j� existe};
$mess[1456] = qq{valida��o};
$mess[1457] = qq{Pr�-contrato};
$mess[1458] = qq{Contrato};
$mess[1459] = qq{Parado};
$mess[1460] = qq{Arquivo};
$mess[1461] = qq{Anulado};
$mess[1462] = qq{entrega};
$mess[1463] = qq{Recupera��o};
$mess[1464] = qq{transferencia de obra};
$mess[1465] = qq{Postos em factura��o};
#message impression
$mess[1500] = qq{guia de entrega};
$mess[1501] = qq{guia de recolha};
#imprssion ass
$mess[1550] = qq{Seguro facturado ao cliente = % do pre�o do aluguer};
$mess[1551] = qq{Seguro no facturado};
$mess[1552] = qq{Segurada pelo cliente };
$mess[1553] = qq{Seguro a mais = 5% do pre�o do aluguer };
$mess[1554] = qq{};
$mess[1555] = qq{MAQUINA ASSEGURADA PELO CLIENTE};
$mess[1556] = qq{Seguro facturado ao cliente = };
$mess[1557] = qq{% do pre�o do aluguer};
$mess[1558] = qq{Seguro compreendido no pre�o de aluguer};
#sub traitfac
$mess[1600] = qq{tarifa mensal};
$mess[1601] = qq{empreitada};
$mess[1602] = qq{ter};
$mess[1603] = qq{Entrega};
$mess[1604] = qq{transporte de recolha};
$mess[1605] = qq{combustivel};
$mess[1606] = qq{seguro a 5%};
$mess[1607] = qq{ do };
$mess[1608] = qq{ ao };
$mess[1609] = qq{Horas deduzidas};
$mess[1610] = qq{Horas suplementares};
$mess[1611] = qq{Horas sabado};
$mess[1612] = qq{Horas domingo};
$mess[1613] = qq{Dias deduzidos};
$mess[1614] = qq{Dias suplementares};
$mess[1615] = qq{Esta factura compreende os dias trabalhados dos 2 meses};
$mess[1616] = qq{Participation au recyclage};
$mess[1617] = qq{Nettoyage};
$mess[1618] = qq{Service};
$mess[1619] = qq{Remise exceptionnelle transport de};
$mess[1620] = qq{Remise exceptionnelle transport aller de};
$mess[1621] = qq{Remise exceptionnelle transport retour de};
$mess[1622] = qq{Apr�s rem. except. XX% sur Aller (remise pour cumul * mach. incluse)};
$mess[1623] = qq{Apr�s rem. except. XX% sur Retour(remise pour cumul * mach. incluse)};
$mess[1624] = qq{Apr�s rem. pour cumul * mach. XX%};
$mess[1625] = qq{Apr�s rem. pour cumul * mach. XX%};
$mess[1626] = qq{Assurance n�goci�e 5%};
$mess[1627] = qq{Apr�s rem. except. sur Aller};
$mess[1628] = qq{Apr�s rem. except. sur Retour};
#arret contratraie
$mess[1650] = qq{paragem impossivel.contactar o respons�vel da logistica para tomar nota da entrega N�};
#sub traitcasse
$mess[1700] = qq{o or�amento � devedor};
$mess[1701] = qq{impress�o do documento};
#annuler client
$mess[1750] = qq{Vai suprimir um Cliente !  Validem para confirmar !};
$mess[1751] = qq{O 1� cliente foi suprimido. As ofertas e contractos v�o ser transferidos para o segundo};
$mess[1752] = qq{&nbsp;Esp&nbsp;&nbsp;&nbsp;Ace&nbsp;&nbsp;Rec};
$mess[1753] = qq{Esp&nbsp;:&nbsp;Espera&nbsp;&nbsp;&nbsp;Ace&nbsp;:&nbsp;Aceitado&nbsp;&nbsp;&nbsp;Rec&nbsp;:&nbsp;Recusado};
#doublon client
$mess[1800] = qq{o cliente/c�digo cliente};
$mess[1801] = qq{j� existe};
$mess[1802] = qq{foi criados, pode procurar � esta minuta};
$mess[1803] = qq{Um campo n�o for preenchido corretamente};
$mess[1804] = qq{Este NIF j� est� criado para outro cliente : %s. Contactar com Catarina Matos ou Am�lia Neves para poder continuar.};
#sub verification
$mess[1850] = qq{controle na verifica��o};
$mess[1851] = qq{verifica��o a controle};
$mess[1852] = qq{m�quina em controle};
#chg client Contrat
$mess[1900] = qq{O cliente vai ser trocado! Valide para confirmar!};
$mess[1901] = qq{o cliente mudou};
#remettre en cours contrat
$mess[1950] = qq{O contracto foi posto em andamento valide para confirmar};
$mess[1951] = qq{o contracto foi posto em andamento};
$mess[1952] = qq{Um transporte est� planificado para este contracto! � necess�rio anul�-lo antes de poder repor em andamento o contracto};
#newstat
$mess[2000] = qq{Ag�ncia};
$mess[2001] = qq{CA m�s};
$mess[2002] = qq{CA transporte};
$mess[2003] = qq{CA combustivel};
$mess[2004] = qq{CA servi�o t�cnico};
$mess[2005] = qq{CA empreitada};
$mess[2006] = qq{custo fianceiro};
$mess[2007] = qq{fam�lias};
$mess[2008] = qq{N� de m�quinas};
$mess[2009] = qq{CA m�quina do dia};
$mess[2010] = qq{CA media dia};
$mess[2011] = qq{taxa de rota��o financeira em %};
$mess[2012] = qq{taxa de rota��o de m�quinas em percentagem};
$mess[2013] = qq{modelo};
$mess[2014] = qq{para a fam�lia};
$mess[2015] = qq{taxa de ocupa��p em percentagem};
$mess[2016] = qq{taxa de ocupa��o (%) depois do inicio do m�s};
$mess[2017] = qq{Taxa de ocupa��o (%)do m�s anterior};
$mess[2018] = qq{comparativo mensal com m�s anterior};
#consultation contrat
$mess[2050] = qq{N� de contracto};
$mess[2051] = qq{Estado do Contrato};
$mess[2052] = qq{cliente};
$mess[2053] = qq{telefone do cliente};
$mess[2054] = qq{fax cliente};
$mess[2055] = qq{contacto do cliente};
$mess[2056] = qq{obra};
$mess[2057] = qq{endere�o das obras};
$mess[2058] = qq{localidade da obra};
$mess[2059] = qq{Telefone obra};
$mess[2060] = qq{contacto na obra};
$mess[2061] = qq{designa��o m�quina};
$mess[2062] = qq{N� parque};
$mess[2063] = qq{data de partida};
$mess[2064] = qq{data de regresso};
$mess[2065] = qq{dia mais};
$mess[2066] = qq{dia menos};
$mess[2067] = qq{dura��o};
$mess[2068] = qq{pr��o dia};
$mess[2069] = qq{pr��o mensal};
$mess[2070] = qq{montante da empreitada};
$mess[2071] = qq{Transporte Ida};
$mess[2072] = qq{Transporte Regresso};
$mess[2073] = qq{Transporte planificado};
$mess[2074] = qq{Motorista ou sob Tratando (recup.)};
$mess[2075] = qq{Excluindo seguro};
$mess[2076] = qq{ n�o};
$mess[2077] = qq{seguro};
$mess[2078] = qq{montante total};
$mess[2079] = qq{Coment�rio};
$mess[2080] = qq{fechar};
$mess[2081] = qq{Motorista ou sob Tratando (ida)};
$mess[2082] = qq{sem dias j� facturados : };
$mess[2083] = qq{Contacto comando};
$mess[2084] = qq{Telefone contacto comando};
$mess[2085] = qq{Nettoyage};
$mess[2086] = qq{Service};
# transportint.cfg
$mess[2100] = qq{transporte interno};
$mess[2101] = qq{pedido de transporte externo};
$mess[2102] = qq{n�mero};
$mess[2103] = qq{N� de contracto};
$mess[2104] = qq{cliente};
$mess[2105] = qq{obra};
$mess[2106] = qq{CP};
$mess[2107] = qq{localidade};
$mess[2108] = qq{cais};
$mess[2109] = qq{Entrega antecipada poss�vel};
$mess[2110] = qq{data de partida};
$mess[2111] = qq{data de regresso};
$mess[2112] = qq{Montante};
$mess[2113] = qq{peso};
$mess[2114] = qq{designa��o m�quina};
$mess[2115] = qq{N� parque};
$mess[2116] = qq{Estado do Contrato};
$mess[2117] = qq{hora de entrega e recolha};
$mess[2118] = qq{Transporte planificado};
$mess[2119] = qq{motorista};
$mess[2120] = qq{pedido de transportador};
$mess[2130] = qq{Janela};
$mess[2131] = qq{Nenhum comentario};
#contrat.cfg et contrat2.cfg
$mess[2150] = qq{n�mero};
$mess[2151] = qq{N� de contracto};
$mess[2152] = qq{cliente};
$mess[2153] = qq{obra};
$mess[2154] = qq{CP};
$mess[2155] = qq{localidade};
$mess[2156] = qq{data de partida};
$mess[2157] = qq{data de regresso};
$mess[2158] = qq{designa��o m�quina};
$mess[2159] = qq{N� parque};
$mess[2160] = qq{Estado do Contrato};
$mess[2161] = qq{editar contrato};
$mess[2162] = qq{editar contrato para 1� impress�o};
#comcont.cfg
$mess[2200] = qq{n�mero};
$mess[2201] = qq{ultimo contrato};
$mess[2202] = qq{indice};
$mess[2203] = qq{modifica��es};
$mess[2204] = qq{C.P. obra};
$mess[2205] = qq{designa��o m�quina};
$mess[2206] = qq{N� parque};
$mess[2207] = qq{estado};
$mess[2208] = qq{Date Fin Contrat};
$mess[2209] = qq{data do vencimento};
$mess[2210] = qq{organismo};
$mess[2211] = qq{pedido visita peri�dica};
$mess[2212] = qq{pedido de controle};
$mess[2250] = qq{Imp. Unit�};
$mess[2251] = qq{N� Comm};
$mess[2252] = qq{Date de Visite};
#nvclient.cfg
$mess[2300] = qq{Novos clientes};
$mess[2301] = qq{n�mero};
$mess[2302] = qq{Ag�ncia};
$mess[2303] = qq{sociedade};
$mess[2304] = qq{morada};
$mess[2305] = qq{CP};
$mess[2306] = qq{localidade};
$mess[2307] = qq{NIF};
$mess[2308] = qq{telefone};
$mess[2309] = qq{Fax};
$mess[2310] = qq{modo pagamento};
$mess[2311] = qq{RIB ?};
$mess[2312] = qq{j� � atribu�do ao cliente};
$mess[2313] = qq{modos pagamento};
$mess[2320] = qq{Procura por nome de cliente};
#nvclient.cfg
$mess[2350] = qq{APE};
$mess[2351] = qq{sector de actividade};
$mess[2352] = qq{SIRET};
$mess[2353] = qq{nome};
$mess[2354] = qq{morada};
$mess[2355] = qq{morada comp};
$mess[2356] = qq{localidade};
$mess[2357] = qq{telefone};
$mess[2358] = qq{telemovel};
$mess[2359] = qq{Fax};
$mess[2360] = qq{contacto};
$mess[2361] = qq{Coment�rio};
$mess[2362] = qq{modo pagamento};
$mess[2363] = qq{banco};
$mess[2364] = qq{RIB ?};
$mess[2365] = qq{Grelha};
$mess[2366] = qq{Grupo};
$mess[2367] = qq{Qualifica��o};
$mess[2368] = qq{Email Cliente};
$mess[2370] = qq{Nenhum};
$mess[2371] = qq{Fim de validade (seguro)};
$mess[2372] = qq{Potencial};
$mess[2373] = qq{Cat.};
$mess[2374] = qq{Pot.};
$mess[2375] = qq{Nenhum};
$mess[2380] = qq{Categoria};

$mess[2390] = qq{Novo cliente};

$mess[2397] = qq{archive.dev\@acces-industrie.com};
$mess[2398] = qq{smmoreira\@acces-industrie.pt};
$mess[2399] = qq{1};

#Devis.cfg
$mess[2400] = qq{clientes};
$mess[2401] = qq{localidade da obra};
$mess[2402] = qq{nome da obra};
$mess[2403] = qq{morada da obra};
$mess[2404] = qq{contacto na obra};
$mess[2405] = qq{telefone da obra};
$mess[2406] = qq{N� de pedido};
$mess[2407] = qq{tipo de m�quina};
$mess[2408] = qq{Fora de parque};
$mess[2409] = qq{quantidade};
$mess[2410] = qq{pr�co do dia/m�s};
$mess[2411] = qq{tipo de valor};
$mess[2412] = qq{dura��o};
$mess[2413] = qq{data prevista};
$mess[2414] = qq{montante};
$mess[2415] = qq{Transporte ir};
$mess[2416] = qq{Transporte Regresso};
$mess[2417] = qq{seguro incluido};
$mess[2418] = qq{montante do seguro};
$mess[2419] = qq{montante total};
$mess[2420] = qq{Coment�rio};
$mess[2421] = qq{notas comerciais};
$mess[2422] = qq{oferta aceite};
$mess[2423] = qq{calcular};
$mess[2424] = qq{valida��o};
$mess[2425] = qq{Stock};
$mess[2426] = qq{oferta};
$mess[2427] = qq{Ir};
$mess[2428] = qq{Regresso};
$mess[2429] = qq{Tarifa};
$mess[2430] = qq{Redu��o};
$mess[2431] = qq{Acrescentar uma m�quina};
$mess[2432] = qq{Cliente};
$mess[2433] = qq{M�quina};
$mess[2434] = qq{Dura��o};
$mess[2435] = qq{Montante};
$mess[2436] = qq{Montante};
$mess[2437] = qq{Transporte};
$mess[2438] = qq{Pedir uma redu��o excepcional};
$mess[2439] = qq{Red. excep.};
$mess[2440] = qq{Zona};
$mess[2441] = qq{Estimativa de transporte};
$mess[2442] = qq{Montando ir e/ou regresso incorrecto (s).};
$mess[2443] = qq{Falta informa��es e/ou estas alteraram, querer�o refazer uma estimativa ou completar.};
$mess[2444] = qq{(entre 1 e 6)};
$mess[2445] = qq{Full service};

#tournee.cfg
$mess[2450] = qq{N� de contracto};
$mess[2451] = qq{obra};
$mess[2452] = qq{morada};
$mess[2453] = qq{CP};
$mess[2454] = qq{localidade};
$mess[2455] = qq{contacto};
$mess[2456] = qq{designa��o};
$mess[2457] = qq{N� parque};
$mess[2458] = qq{peso};
$mess[2459] = qq{estado};
$mess[2460] = qq{estado contrato};
$mess[2461] = qq{nome do motorista};
$mess[2462] = qq{hora de entrega e recolha};
$mess[2463] = qq{ordem};
#retour.cfg
$mess[2500] = qq{ordenar o turno};
$mess[2501] = qq{editar GE/GR/ Pedido recolha};
$mess[2502] = qq{verificar pontos de entrega e recolha};

$mess[2505] = qq{N� de contracto};
$mess[2506] = qq{obra};
$mess[2507] = qq{morada};
$mess[2508] = qq{CP};
$mess[2509] = qq{localidade};
$mess[2510] = qq{contacto};
$mess[2511] = qq{designa��o};
$mess[2512] = qq{n� parque};
$mess[2513] = qq{estado};
$mess[2514] = qq{estado contrato};
$mess[2515] = qq{nome do motorista};
$mess[2516] = qq{ordem};
$mess[2517] = qq{hora de entrega e recolha};
$mess[2518] = qq{efectuada};
$mess[2519] = qq{motorista interno};
$mess[2520] = qq{sob tratando};
#Type de montant
$mess[2550] = qq{Tipo Montante};
$mess[2551] = qq{dia};
$mess[2552] = qq{mensal};
$mess[2553] = qq{semanal};
#libell�s devis
$mess[2600] = qq{modificar cabe�alho};
$mess[2601] = qq{modificar a linha};
$mess[2602] = qq{modificar o modelo};
$mess[2603] = qq{Deve imperativamente registar a linha antes de qualquer outra coisa!};
$mess[2604] = qq{Deve imperativamente registar o contrato antes de qualquer outra coisa!};

#modifiarret modif2.cfg
$mess[2650] = qq{modifica��o/paragem do contracto};
$mess[2651] = qq{n�mero};
$mess[2652] = qq{Estato contracto};
$mess[2653] = qq{N� de contracto};
$mess[2654] = qq{indice};
$mess[2655] = qq{cliente};
$mess[2656] = qq{nome da obra};
$mess[2657] = qq{localidade da obra};
$mess[2658] = qq{designa��o m�quina};
$mess[2659] = qq{N� parque};
$mess[2660] = qq{Fora de parque};
$mess[2661] = qq{data de partida};
$mess[2662] = qq{data de regresso};
$mess[2663] = qq{dura��o};
$mess[2664] = qq{um dia a mais};
$mess[2665] = qq{um dia a menos};
$mess[2666] = qq{pr��o dia};
$mess[2667] = qq{pr��o do m�s};
$mess[2668] = qq{empreitada};
$mess[2669] = qq{seguro incluido};
$mess[2670] = qq{montante do seguro};
$mess[2671] = qq{transporte entrega};
$mess[2672] = qq{entrega com transporte cliente};
$mess[2673] = qq{transporte recolha};
$mess[2674] = qq{recolha com transporte cliente};
$mess[2675] = qq{cais};
$mess[2676] = qq{montante total sem taxas};
$mess[2677] = qq{pedido};
$mess[2678] = qq{Coment�rio};
$mess[2679] = qq{nota};
$mess[2680] = qq{n� contrato};
$mess[2681] = qq{cliente};
$mess[2682] = qq{Este contrato j� tem sido parado};
$mess[2683] = qq{o Tipo de m�quina � alterado};
$mess[2684] = qq{O tipo da m�quina mudou};
$mess[2685] = qq{o contracto est� anulado};
$mess[2686] = qq{Acess�rios ligados ao contrato};
$mess[2687] = qq{Acess�rio (N� parque)};
$mess[2688] = qq{Contrato (Estato)};
$mess[2689] = qq{montante};
$mess[2690] = qq{data de partida};
$mess[2691] = qq{data de regresso};
$mess[2692] = qq{M�quina alugada sem o acess�rio por defeito};
$mess[2693] = qq{Contrato de origem};
#Stock
$mess[2700] = qq{designa��o};
$mess[2701] = qq{modelo};
$mess[2702] = qq{eleva��o};
$mess[2703] = qq{energia};
$mess[2704] = qq{peso};
$mess[2705] = qq{stock reservado};
$mess[2706] = qq{stock reserv�vel};
$mess[2707] = qq{Stock Total};
$mess[2708] = qq{Alugado 0%};
$mess[2709] = qq{transfer�ncia 0%};
$mess[2710] = qq{Avarias 0%};
$mess[2711] = qq{controlo 90%};
$mess[2712] = qq{recebido 50%};
$mess[2713] = qq{verifica��o 80%};
$mess[2714] = qq{revis�o 80%};
$mess[2715] = qq{dispon�vel 100%};
#Saisie pr�-contrat
$mess[2750] = qq{embargo de pr�-contracto};
$mess[2751] = qq{clientes};
$mess[2752] = qq{tipo de m�quina};
$mess[2753] = qq{Fora de parque};
$mess[2754] = qq{localidade da obra};
$mess[2755] = qq{nome da obra};
$mess[2756] = qq{morada da obra};
$mess[2757] = qq{contacto na obra};
$mess[2758] = qq{telefone da obra};
$mess[2759] = qq{Contacto comando};
$mess[2760] = qq{Tel. contacto comando};
$mess[2761] = qq{N� de pedido};
$mess[2762] = qq{data de partida};
$mess[2763] = qq{data de regresso};
$mess[2764] = qq{dia mais};
$mess[2765] = qq{dia menos};
$mess[2766] = qq{pr��o};
$mess[2767] = qq{dura��o};
$mess[2768] = qq{pr��o do m�s};
$mess[2769] = qq{montante da empreitada};
$mess[2770] = qq{seguro};
$mess[2771] = qq{Ida};
$mess[2772] = qq{Pelo cliente};
$mess[2773] = qq{Regresso};
$mess[2774] = qq{Pelo cliente};
$mess[2775] = qq{cais};
$mess[2776] = qq{Coment�rio};
$mess[2777] = qq{modelo imperativo};
$mess[2778] = qq{transfer�ncia de obras};
$mess[2779] = qq{Pagamento � entrega};
$mess[2780] = qq{calcular};
$mess[2781] = qq{Aten��o, o modelo escolhido n�o est� reservado enquanto o pr�-contrato nao f�r validado};
$mess[2782] = qq{a �ltima m�quina deste modelo acaba de ser reservada, selecione outro modelo.};
$mess[2783] = qq{Tipo de valor};
$mess[2784] = qq{Tarifa};
$mess[2785] = qq{Redu��o};
$mess[2786] = qq{Redu��o exceptional};
$mess[2787] = qq{Limpieza};
$mess[2788] = qq{Enlevement sur place};
$mess[2789] = qq{M�quina supl.};
$mess[2790] = qq{M�quina supl.};

#Etat pr�-contrat
$mess[2800] = qq{Ag�ncia};
$mess[2801] = qq{respons�vel};
$mess[2802] = qq{morada};
$mess[2803] = qq{telefone};
$mess[2804] = qq{Fax};
$mess[2805] = qq{CP};
$mess[2806] = qq{localidade};
$mess[2807] = qq{cliente};
$mess[2808] = qq{morada};
$mess[2809] = qq{CP};
$mess[2810] = qq{localidade};
$mess[2811] = qq{telefone};
$mess[2812] = qq{Fax};
$mess[2813] = qq{obra};
$mess[2814] = qq{morada};
$mess[2815] = qq{CP};
$mess[2816] = qq{localidade};
$mess[2817] = qq{respons�vel};
$mess[2818] = qq{construtor};
$mess[2819] = qq{fam�lias};
$mess[2820] = qq{energia};
$mess[2821] = qq{designa��o};
$mess[2822] = qq{peso};
$mess[2823] = qq{aluguer di�rio};
$mess[2824] = qq{seguro};
$mess[2825] = qq{Transporte Ida};
$mess[2826] = qq{Transporte Regresso};
$mess[2827] = qq{Data de partida};
$mess[2828] = qq{Data de regresso};
$mess[2829] = qq{um dia a mais};
$mess[2830] = qq{um dia a menos};
$mess[2831] = qq{dura��o};
$mess[2832] = qq{Funcion�rio que alugou};
$mess[2833] = qq{indice};
$mess[2834] = qq{seguro compreendido};
#conscont.cfg
$mess[2850] = qq{consulta de contrato};
$mess[2851] = qq{n�mero};
$mess[2852] = qq{N� de contracto};
$mess[2853] = qq{cliente};
$mess[2854] = qq{Nome da obra};
$mess[2855] = qq{CP obra};
$mess[2856] = qq{localidade da obra};
$mess[2857] = qq{contacto na obra};
$mess[2858] = qq{contacto commande};
$mess[2859] = qq{designa��o m�quina};
$mess[2860] = qq{N� parque};
$mess[2861] = qq{data de partida};
$mess[2862] = qq{data de regresso};
$mess[2863] = qq{dura��o};
$mess[2864] = qq{pr��o dia};
$mess[2865] = qq{pr��o do m�s};
$mess[2866] = qq{montante do seguro};
$mess[2867] = qq{Montant Gestion de recours};
$mess[2868] = qq{Transporte ida};
$mess[2869] = qq{Transporte regresso};
$mess[2870] = qq{Quantidade de combust�vel (L)};
$mess[2871] = qq{Nettoyage};
$mess[2872] = qq{Service};
$mess[2873] = qq{montante total sem taxas};
$mess[2874] = qq{estado contrato};
$mess[2875] = qq{N� de pedido};
$mess[2876] = qq{nota};

$mess[2882] = qq{N� de contracto};
$mess[2883] = qq{cliente};
$mess[2884] = qq{N� Parque};
$mess[2885] = qq{obra};
$mess[2886] = qq{Nome da obra};
$mess[2887] = qq{departamento da obra};
$mess[2888] = qq{ano};
$mess[2889] = qq{estado contrato};
$mess[2890] = qq{seja};
$mess[2891] = qq{dia};
$mess[2892] = qq{/&nbsp;dia};
$mess[2893] = qq{/&nbsp;m�s};
$mess[2894] = qq{empreitada};
$mess[2895] = qq{Imposs�vel determinar a tarifa !};
$mess[2896] = qq{S� os dias a mais podem ser modificados};
#machine.cfg
$mess[2900] = qq{Lista m�quina / ag�ncia de };
$mess[2901] = qq{designa��o};
$mess[2902] = qq{N� parque};
$mess[2903] = qq{Fora de parque};
$mess[2904] = qq{N� Serie};
$mess[2905] = qq{data de compra};
$mess[2906] = qq{data de controle};
$mess[2907] = qq{modelo};
$mess[2908] = qq{construtor};
$mess[2909] = qq{eleva��o};
$mess[2910] = qq{energia};
$mess[2911] = qq{peso};
$mess[2912] = qq{comprimento};
$mess[2913] = qq{largura};
$mess[2914] = qq{capacidade};
$mess[2915] = qq{estado};
$mess[2916] = qq{probabildade de aluguer em %};
$mess[2917] = qq{ag�ncia};
$mess[2918] = qq{�ltimo interlocutor};
$mess[2919] = qq{n� parque};
$mess[2920] = qq{N� Serie};
$mess[2921] = qq{modelo m�quina};
$mess[2922] = qq{fam�lia m�quina};
$mess[2923] = qq{Fornecedor};
$mess[2924] = qq{eleva��o ou press�o};
$mess[2925] = qq{energia};
$mess[2926] = qq{estado m�quina};
$mess[2927] = qq{Parque};
$mess[2928] = qq{Ag�ncia};
$mess[2929] = qq{Pa�s};
$mess[2930] = qq{�ltimo contrato sobre esta m�quina};
$mess[2931] = qq{Historial dos contratos};
$mess[2932] = qq{Ver o historial};
$mess[2933] = qq{%d �ltimos:};
$mess[2934] = qq{Nenhum contrato em Portugal};
$mess[2935] = qq{Lista m�quina global};
$mess[2936] = qq{Regi�o};

#chantier.cfg
$mess[2950] = qq{modifica�ao das informa��es da obra};
$mess[2951] = qq{n�mero};
$mess[2952] = qq{N� de contracto};
$mess[2953] = qq{cliente};
$mess[2954] = qq{nome da obra};
$mess[2955] = qq{morada da obra};
$mess[2956] = qq{localidade};
$mess[2957] = qq{contacto};
$mess[2958] = qq{telefone};
$mess[2959] = qq{altura};
$mess[2960] = qq{superficie};
$mess[2961] = qq{Hora de Entrega};
$mess[2962] = qq{Hora de recolha};
$mess[2963] = qq{Coment�rio};
$mess[2964] = qq{N� de contracto};
$mess[2965] = qq{cliente};
$mess[2966] = qq{Nome da obra};
$mess[2967] = qq{localidade da obra};
#infoclient.cfg
$mess[3000] = qq{modifica��o das informa��es do cliente};
$mess[3001] = qq{n�mero};
$mess[3002] = qq{c�digo do cliente};
$mess[3003] = qq{APE ?};
$mess[3004] = qq{NIF};
$mess[3005] = qq{Nome};
$mess[3006] = qq{morada};
$mess[3007] = qq{morada comp.};
$mess[3008] = qq{localidade};
$mess[3009] = qq{telefone};
$mess[3010] = qq{Fax};
$mess[3011] = qq{telemovel};
$mess[3012] = qq{E-mail};
$mess[3013] = qq{contacto};
$mess[3014] = qq{colectivo};
$mess[3015] = qq{LI_NO};
$mess[3016] = qq{banco};
$mess[3017] = qq{tipo de pagamento};
$mess[3018] = qq{RIB ?};
$mess[3019] = qq{n� de facturas};
$mess[3020] = qq{factura��o local};
$mess[3021] = qq{factura��o n�o agrupada};
$mess[3022] = qq{Coment�rio};
$mess[3023] = qq{actividade};
$mess[3024] = qq{Grelha};
$mess[3025] = qq{Sono/Bloqueado};

$mess[3026] = qq{Seguro};
$mess[3027] = qq{N� encomenda obrigat�rio};
$mess[3028] = qq{Bom de Encomenda fornecer};
$mess[3029] = qq{Pagamento � entrega};
$mess[3030] = qq{Condi��es gerais de venda};
$mess[3031] = qq{Factura��o da limpeza};
$mess[3032] = qq{Contrato-quadro};
$mess[3033] = qq{Si};
$mess[3034] = qq{No};
$mess[3035] = qq{Date d'application de l'assurance souhait�e.};
$mess[3036] = qq{O Contribuinte n�o est� atribu�do};
$mess[3037] = qq{Formato de Contribuinte n�o v�lido};
$mess[3038] = qq{Contribuinte j� utilizado};

$mess[3040] = qq{c�digo do cliente};
$mess[3041] = qq{nome do cliente};
$mess[3042] = qq{localidade};
$mess[3043] = qq{departamento};
$mess[3044] = qq{NIF};
$mess[3045] = qq{Grelha};
$mess[3046] = qq{Bons de livr./r�cup. obligatoires};

$mess[3047] = qq{Factura��o da Gest�o de Res�duos};
$mess[3048] = qq{Qualifica��o};
$mess[3060] = qq{Participation au recyclage};

$mess[3049] = qq{Le champ "Facturation Locale" a �t� modifi�. Veuillez contacter le 5000 pour que les actions n�cessaires soient effectu�es};

#anncl.cfg
$mess[3050] = qq{supress�o do cliente};
$mess[3051] = qq{cliente errado};
$mess[3052] = qq{cliente correcto};
$mess[3053] = qq{calcular};
# Assurance souhait�e
$mess[3054] = qq{Assurance souhait�e};
# Assurance actuelle
$mess[3055] = qq{Assurance actuelle};
# Gestion de recours actuelle
$mess[3056] = qq{Gestion de recours};
# Gestion de recours souhait�e
$mess[3057] = qq{Gestion de recours souhait�e};
$mess[3058] = qq{Date d'application souhait�e.};
$mess[3059] = qq{Non facturable};

#chclient.cfg
$mess[3100] = qq{mudan�a de cliente/contrato};
$mess[3101] = qq{mau cliente no contracto};
$mess[3102] = qq{guia de cliente};
$mess[3103] = qq{valida��o};
$mess[3104] = qq{Attention, sur les deux clients, les informations suivantes sont diff�rentes (elles doivent �tre mises � jour <span style="text-decoration:underline;">manuellement</span>) :};
$mess[3105] = qq{ATTENTION, � la validation les tarifs de location et transport sont <span style="text-decoration:underline;">automatiquement maintenus suivant les conditions du premier client</span>};
$mess[3106] = qq{L�assurance du contrat n�est pas compatible avec l�assurance du nouveau client :};
$mess[3107] = qq{Un probl�me est survenu lors du changement de client sur le contrat.};

#chmach.cfg
$mess[3150] = qq{mudan�a de m�quina /contracto};
$mess[3151] = qq{Contrato/N� parque};
$mess[3152] = qq{m�quina disponivel};
$mess[3153] = qq{valida��o};
$mess[3154] = qq{j� validou};
$mess[3155] = qq{a m�quina foi trocada};
$mess[3156] = qq{O tipo da m�quina mudou};
$mess[3157] = qq{o contracto est� anulado};
$mess[3158] = qq{o or�amento est� embargado};
$mess[3159] = qq{N�o � poss�vel fazer a modifica��o, o transporte j� esta fechado};
$mess[3160] = qq{Impossible de faire le changement, le typage du mod�le de machines du contrat est diff�rent de celui de la machine saisie};
#contcourt.cfg
$mess[3200] = qq{p�r o contracto em andamento};
$mess[3201] = qq{contrato parado};
$mess[3202] = qq{valida��o};
$mess[3203] = qq{N'oubliez pas d'enregistrer la note avant d'annuler un contrat.};
#saisiemachine.cfg
$mess[3250] = qq{embargo m�quina};
$mess[3251] = qq{procura do modelo};
$mess[3252] = qq{n� de parque};
$mess[3253] = qq{n� de s�rie};
$mess[3254] = qq{data certificado seguran�a};
$mess[3255] = qq{calcular};
#sousloc.cfg
$mess[3300] = qq{sub-loca��o};
$mess[3301] = qq{clientes};
$mess[3302] = qq{localidade da obra};
$mess[3303] = qq{nome da obra};
$mess[3304] = qq{morada da obra};
$mess[3305] = qq{contacto na obra};
$mess[3306] = qq{Telepone da obra};
$mess[3307] = qq{Contacto comando};
$mess[3308] = qq{Tel. contacto comando};
$mess[3309] = qq{sob tratando};
$mess[3310] = qq{designa��o do sub-contratado};
$mess[3311] = qq{data de partida};
$mess[3312] = qq{data de regresso};
$mess[3313] = qq{pr��o dia};
$mess[3314] = qq{dura��o mensal};
$mess[3315] = qq{pr��o do m�s};
$mess[3316] = qq{montante da empreitada};
$mess[3317] = qq{seguro incluido};
$mess[3318] = qq{transporte ida};
$mess[3319] = qq{posto no local pelo cliente};
$mess[3320] = qq{transporte regresso};
$mess[3321] = qq{devolvido pelo cliente};
$mess[3322] = qq{cais};
$mess[3323] = qq{calcular};
$mess[3324] = qq{M�quina suplementar ida};
$mess[3325] = qq{M�quina suplementar regresso};
#tableau.cfg
$mess[3350] = qq{quadro};
$mess[3351] = qq{m�quina alugada};
$mess[3352] = qq{m�quinas em entrega};
$mess[3353] = qq{m�quina em recolha};
$mess[3354] = qq{m�quina em };
$mess[3355] = qq{modelo};
$mess[3356] = qq{fam�lias};
$mess[3357] = qq{altura};
$mess[3358] = qq{energia};
$mess[3359] = qq{N� parque};
$mess[3360] = qq{N� de contracto};
$mess[3361] = qq{cliente};
$mess[3362] = qq{obra};
$mess[3363] = qq{CP};
$mess[3364] = qq{localidade};
$mess[3365] = qq{data do fim};
$mess[3366] = qq{POT};
$mess[3367] = qq{tarifa};
$mess[3368] = qq{Custo};
$mess[3369] = qq{R�cio};
$mess[3370] = qq{Dura��o};
$mess[3380] = qq{Enl�vement};
$mess[3381] = qq{R�cup�ration};
$mess[3382] = qq{Transfert inter-chantiers};
$mess[3383] = qq{Reprise sur chantier};


$mess[3410] = qq{N�mero};
$mess[3411] = qq{Contrato};
$mess[3412] = qq{Data de partida};
$mess[3413] = qq{Data de regresso};
$mess[3414] = qq{N� parque};
$mess[3415] = qq{Modelo};
$mess[3416] = qq{Obra};
$mess[3417] = qq{C.P.};
$mess[3418] = qq{Localidade};
#nomade.cfg
$mess[3450] = qq{mudan�a de ag�ncia};
$mess[3451] = qq{Ag�ncia de destino};
$mess[3452] = qq{Est&aacute; em};
$mess[3453] = qq{Alterar};
#statms.cfg
$mess[3500] = qq{Valores do m�s};
#stat1.cfg
$mess[3550] = qq{valores de 2 meses};
#stat2.cfg
$mess[3600] = qq{Valores global};
#hisstat.cfg
$mess[3650] = qq{hist�rico dos valores};
$mess[3651] = qq{m�s};
$mess[3652] = qq{ano};
$mess[3653] = qq{Janeiro};
$mess[3654] = qq{Fevereiro};
$mess[3655] = qq{Mar�o};
$mess[3656] = qq{Abril};
$mess[3657] = qq{Maio};
$mess[3658] = qq{Junho};
$mess[3659] = qq{Julho};
$mess[3660] = qq{Ag�sto};
$mess[3661] = qq{Setembro};
$mess[3662] = qq{Outubro};
$mess[3663] = qq{Novembro};
$mess[3664] = qq{Dezembro};
$mess[3665] = qq{5};
$mess[3666] = qq{2000};
$mess[3667] = qq{2001};
$mess[3668] = qq{2002};
$mess[3669] = qq{2003};
$mess[3670] = qq{2004};
#validagag.cfg
$mess[3700] = qq{valida��o ag�ncia a ag�ncia};
$mess[3701] = qq{m�quina};
$mess[3702] = qq{M�quina};
#transfagag.cfg
$mess[3750] = qq{transferencia de empresa para empresa};
$mess[3751] = qq{m�quina};
$mess[3752] = qq{cami�o};
$mess[3753] = qq{motorista};
$mess[3754] = qq{Ag�ncia de destino};
$mess[3755] = qq{M�quina};
$mess[3756] = qq{cami�es};
$mess[3757] = qq{motoristas};
$mess[3758] = qq{ag�ncias};
#camcha.cfg
$mess[3800] = qq{motorista};
$mess[3801] = qq{cami�o};
$mess[3802] = qq{motorista};
$mess[3803] = qq{Pare Cami�o/Motorista};
$mess[3804] = qq{cami�es};
$mess[3805] = qq{motoristas};
$mess[3806] = qq{pares};
#transfert.cfg
$mess[3850] = qq{transfer�ncia de obras};
$mess[3851] = qq{transporte};
$mess[3852] = qq{transporte};
$mess[3853] = qq{N� parque};
$mess[3854] = qq{n�mero};
$mess[3855] = qq{N� de contracto};
$mess[3856] = qq{cliente};
$mess[3857] = qq{morada};
$mess[3858] = qq{CP};
$mess[3859] = qq{localidade};
$mess[3860] = qq{designa��o m�quina};
$mess[3861] = qq{N� parque};
#carbu.cfg
$mess[3900] = qq{combustivel};
$mess[3901] = qq{Contrato, cliente, designa��o, N�Parque};
$mess[3902] = qq{quantidade de combustivel em litros};
#verif.cfg
$mess[3950] = qq{verifica��o m�quina};
$mess[3951] = qq{m�quina};
#Contr�le Apave
$mess[4000] = qq{controle m�quina};
$mess[4001] = qq{N� parque};
$mess[4002] = qq{modelo m�quina};
$mess[4003] = qq{fam�lia m�quina};
$mess[4004] = qq{eleva��o ou press�o};
$mess[4005] = qq{energia};
$mess[4006] = qq{estado m�quina};
$mess[4007] = qq{designa��o};
$mess[4008] = qq{designa��o};
$mess[4009] = qq{N� parque};
$mess[4010] = qq{N� Serie};
$mess[4011] = qq{modelo};
$mess[4012] = qq{construtor};
$mess[4013] = qq{eleva��o};
$mess[4014] = qq{energia};
$mess[4015] = qq{peso};
$mess[4016] = qq{comprimento};
$mess[4017] = qq{largura};
$mess[4018] = qq{capacidade};
$mess[4019] = qq{estado};
$mess[4020] = qq{Data de controle};
$mess[4021] = qq{efectuado};
$mess[4022] = qq{N�};
#Consultation Devis
$mess[4050] = qq{consulta de or�amento};
$mess[4051] = qq{N� da oferta};
$mess[4052] = qq{cliente};
$mess[4053] = qq{Estato da oferta};
$mess[4054] = qq{Agente};
$mess[4055] = qq{N� da oferta};
$mess[4056] = qq{cliente};
$mess[4057] = qq{Nome da Obra};
$mess[4058] = qq{data};
$mess[4059] = qq{designa��o};
$mess[4060] = qq{pr��o dia};
$mess[4061] = qq{dura��o};
$mess[4062] = qq{C.T};
$mess[4063] = qq{montante};
$mess[4064] = qq{passou para contracto};
$mess[4065] = qq{estado};
$mess[4066] = qq{Agente};
#Remise en cours de contrat
$mess[4100] = qq{p�r o contracto em andamento};
$mess[4101] = qq{contrato parado};
$mess[4102] = qq{valida��o};
#Cr�ation de machine
$mess[4150] = qq{embargo m�quina};
$mess[4151] = qq{procura do modelo};
$mess[4152] = qq{n� parque};
$mess[4153] = qq{N� Serie};
$mess[4154] = qq{data certificado seguran�a};
$mess[4155] = qq{Agence de cr�ation};
$mess[4156] = qq{Calcular};
#Facturation
$mess[4200] = qq{Factura��o};
$mess[4201] = qq{N� de contracto};
$mess[4202] = qq{data de fim};
$mess[4203] = qq{Ag�ncia};
$mess[4204] = qq{contacto};
$mess[4205] = qq{telefone da ag�ncia};
$mess[4206] = qq{Ag�ncia};
$mess[4207] = qq{A parar};
$mess[4208] = qq{Combust�veis a apreender};
$mess[4209] = qq{A pasar en facturaci�n};
$mess[4210] = qq{Telefone da ag�ncia};
$mess[4211] = qq{Sum�rio};
$mess[4212] = qq{Factura��o semanal};
$mess[4213] = qq{Factura��o mensal};
$mess[4214] = qq{Data de partida};
$mess[4215] = qq{Data de cria��o};
$mess[4216] = qq{Contratos antecipados};
#Energies
$mess[4250] = qq{bi-energia};
$mess[4251] = qq{Diesel};
$mess[4252] = qq{electrica};
$mess[4253] = qq{manivela};
$mess[4254] = qq{Gaz};
#Familles
$mess[4300] = qq{Boom};
$mess[4301] = qq{empilhador};
$mess[4302] = qq{tesoura};
$mess[4303] = qq{bra�o};
$mess[4304] = qq{bra�o de lagartas};
$mess[4305] = qq{monta-cargas};
$mess[4306] = qq{bra�o direito};
$mess[4307] = qq{apertar};
$mess[4308] = qq{compactador};
$mess[4309] = qq{empilhador rotativo};
$mess[4310] = qq{diversos};
#Etat
$mess[4350] = qq{controle};
$mess[4351] = qq{dispon�vel};
$mess[4352] = qq{alugado};
$mess[4353] = qq{avaria};
$mess[4354] = qq{recupera��o};
$mess[4355] = qq{revis�o};
$mess[4356] = qq{verifica��o};

$mess[4357] = qq{Vendue};
$mess[4358] = qq{En Transfert};
$mess[4359] = qq{Rendue};
$mess[4360] = qq{Restitu�};

$mess[4361] = qq{A recolher};

#Ann�es
$mess[4400] = qq{2001};
$mess[4401] = qq{2000};
$mess[4402] = qq{1999};
$mess[4403] = qq{1998};
#Info client
$mess[4450] = qq{informa��es cliente};
$mess[4451] = qq{nome do cliente};
$mess[4452] = qq{morada};
$mess[4453] = qq{NIF};
$mess[4454] = qq{APE ?};
$mess[4455] = qq{Sector de actividade};
$mess[4456] = qq{Contacto cliente};
$mess[4457] = qq{Telefone};
$mess[4458] = qq{Fax};
$mess[4459] = qq{Telem�vel};
$mess[4460] = qq{E-mail};
$mess[4461] = qq{coment�rios};
$mess[4462] = qq{estatisticas};
$mess[4463] = qq{Ag�ncia(s)};
$mess[4476] = qq{Total};
$mess[4483] = qq{fechar};

# stati.cgi
#entete
$mess[5000] = qq{Nbr. contratos};
$mess[5001] = qq{Tx util.<br>J+1};
$mess[5002] = qq{TOT};
$mess[5003] = qq{CER};
$mess[5004] = qq{ACT};
$mess[5005] = qq{SEG<br>FACT.};
$mess[5006] = qq{CT<5j};
$mess[5007] = qq{MENS};
$mess[5008] = qq{FORF};
$mess[5009] = qq{CERRADO AL J=};
$mess[5010] = qq{CERRADO AL FIN DEL MES};
$mess[5011] = qq{CA};
$mess[5012] = qq{TRANSP};
$mess[5013] = qq{GAS};
$mess[5014] = qq{CUSTO};
$mess[5015] = qq{CT Dia};
#titre groupe
$mess[5016] = qq{PARQUE};
$mess[5017] = qq{DIA AL.};
$mess[5018] = qq{% Util};
$mess[5019] = qq{P Dia};
$mess[5020] = qq{CUSTO};
$mess[5021] = qq{CT Dia};
#tittre bloc
$mess[5022] = qq{Fam. Contable};
$mess[5023] = qq{Fam. Comercial};
#date
$mess[5024] = qq{Mes en curso};
$mess[5025] = qq{ALQ};
$mess[5026] = qq{d. �.};
# Autres
$mess[5027] = qq{Livr.};
# BC � envoyer/r�ceptionner
$mess[5050] = qq{Bom de Encomenda a juntar-se � factura};
$mess[5051] = qq{N� de Contrato};
$mess[5052] = qq{Cidade Estaleiro};
$mess[5053] = qq{Cliente};
$mess[5054] = qq{N� Tel};
$mess[5055] = qq{Exemplar M�quina};
$mess[5056] = qq{N� de Parque};
$mess[5057] = qq{Data de In�cio};
$mess[5058] = qq{Data de Fim};
$mess[5059] = qq{Validado};
$mess[5060] = qq{em espera};
# Paiement � la livraison
$mess[5600] = qq{Pagamento � entrega};
$mess[5601] = qq{N� de Contrato};
$mess[5602] = qq{Cidade Estaleiro};
$mess[5603] = qq{Cliente};
$mess[5604] = qq{N� Tel};
$mess[5605] = qq{Exemplar M�quina};
$mess[5606] = qq{N� de Parc};
$mess[5607] = qq{Data de In�cio};
$mess[5608] = qq{Data de Fim};
$mess[5609] = qq{Monta presta��o};
$mess[5610] = qq{N� presta��o};
$mess[5611] = qq{Data pagamento};
$mess[5612] = qq{Tipo};
# Paiement � la livraison
$mess[5650] = qq{Pagamento � entrega};
$mess[5651] = qq{N� de Contrato};
$mess[5652] = qq{Cliente};
$mess[5653] = qq{C�digo Cliente};
$mess[5654] = qq{N� Tel};
$mess[5655] = qq{Data de In�cio};
$mess[5656] = qq{Data de Fim};
$mess[5657] = qq{Monta presta��o};
$mess[5658] = qq{N� presta��o};
$mess[5659] = qq{Data pagamento};
$mess[5660] = qq{Tipo pagamento};
$mess[5661] = qq{Validado};
# Devis (nouvelle formule)
# En-t�te
$mess[5800] = qq{oferta};
$mess[5801] = qq{Stock};
$mess[5802] = qq{clientes};
$mess[5803] = qq{localidade da obra};
$mess[5804] = qq{nome da obra};
$mess[5805] = qq{morada da obra};
$mess[5806] = qq{contacto na obra};
$mess[5807] = qq{telefone da obra};
$mess[5808] = qq{N� de pedido};
# Colonnes
$mess[5820] = qq{Pre�o objectivo};
$mess[5821] = qq{Custo/dia};
$mess[5822] = qq{R�cio};
$mess[5823] = qq{% util};
$mess[5824] = qq{quantidade};
$mess[5825] = qq{tipo de valor};
$mess[5826] = qq{dura��o};
$mess[5827] = qq{data prevista};
$mess[5828] = qq{pr�co do dia/m�s/empreitada};
$mess[5829] = qq{R�cio};
$mess[5830] = qq{montante};
$mess[5831] = qq{Transporte ir};
$mess[5832] = qq{Transporte Regresso};
# Totaux
$mess[5850] = qq{seguro incluido};
$mess[5851] = qq{montante do seguro};
$mess[5852] = qq{montante total};
# Pied
$mess[5860] = qq{Coment�rio};
$mess[5861] = qq{notas comerciais};
$mess[5862] = qq{oferta aceite};
$mess[5863] = qq{calcular};
$mess[5864] = qq{valida��o};
# Infos ratio Pr�-contrat
$mess[5900] = qq{Taxa de utiliza��o};
$mess[5901] = qq{Dura��o};
$mess[5902] = qq{Custo/dia};
$mess[5903] = qq{R�cio objectivo};
$mess[5904] = qq{Pre�o objectivo/dia};
$mess[5905] = qq{Pre�o objectivo/m�s};
$mess[5906] = qq{R�cio};
# L�gende clients
$mess[6000] = qq{red};
$mess[6001] = qq{green};
$mess[6010] = qq{Cliente vermelho};
$mess[6011] = qq{Cliente em verde};
$mess[6020] = qq{Cliente n�o fazendo volume de neg�cios desde mais de 14 meses};
$mess[6021] = qq{Cliente bloqueado em curso};
$mess[6022] = qq{� necess�rio desbloquear o cliente para prolongar o contrato };
# Machine hors parc
$mess[6050] = qq{Parque normal};
$mess[6051] = qq{Fora de parque};
#modifiarret modif2.cfg
$mess[6100] = qq{modifica��o/paragem do contracto};
$mess[6101] = qq{n�mero};
$mess[6102] = qq{Estato contracto};
$mess[6103] = qq{N� de contracto};
$mess[6104] = qq{indice};
$mess[6105] = qq{cliente};
$mess[6106] = qq{nome da obra};
$mess[6107] = qq{localidade da obra};
$mess[6108] = qq{Contacto obra};
$mess[6109] = qq{telefone obra};
$mess[6110] = qq{Contacto commande};
$mess[6111] = qq{telefone contacto commande};
$mess[6112] = qq{designa��o m�quina};
$mess[6113] = qq{N� parque};
$mess[6114] = qq{Fora de parque};
$mess[6115] = qq{data de partida};
$mess[6116] = qq{data de regresso};
$mess[6117] = qq{dura��o};
$mess[6118] = qq{um dia a mais};
$mess[6119] = qq{um dia a menos};
$mess[6120] = qq{pr��o dia};
$mess[6121] = qq{pr��o do m�s};
$mess[6122] = qq{empreitada};
$mess[6123] = qq{seguro incluido};
$mess[6124] = qq{montante do seguro};
$mess[6125] = qq{transporte entrega};
$mess[6126] = qq{entrega com transporte cliente};
$mess[6127] = qq{transporte recolha};
$mess[6128] = qq{recolha com transporte cliente};
$mess[6129] = qq{cais};
$mess[6130] = qq{Nettoyage};
$mess[6131] = qq{Libell� du service};
$mess[6132] = qq{Montant service};
$mess[6133] = qq{montante total sem taxas};
$mess[6134] = qq{pedido};
$mess[6135] = qq{Coment�rio};
$mess[6136] = qq{nota};
$mess[6137] = qq{Pagamento � entrega};
$mess[6138] = qq{cliente};
$mess[6139] = qq{N� parque};
$mess[6140] = qq{Este contrato j� tem sido parado};
$mess[6141] = qq{o Tipo de m�quina � alterado};
$mess[6142] = qq{o contracto est� anulado};
$mess[6143] = qq{Acess�rios ligados ao contrato};
$mess[6144] = qq{Acess�rio (N� parque)};
$mess[6145] = qq{Contrato (Estato)};
$mess[6146] = qq{montante};
$mess[6147] = qq{data de partida};
$mess[6148] = qq{data de regresso};
$mess[6149] = qq{M�quina alugada sem o acess�rio por defeito};
$mess[6150] = qq{Contrato de origem};
# Contrat imprim�
$mess[6200] = qq{Aluguer dia sem taxas};
$mess[6201] = qq{Aluguer m�s sem taxas};
$mess[6202] = qq{Aluguer empreitada sem taxas};
# Assurance
$mess[6300] = qq{Facturado};
$mess[6301] = qq{No facturado};
$mess[6302] = qq{Pelo cliente};
$mess[6303] = qq{Compreendido};
# BL-BR
$mess[6400] = qq{Nenhuma maquina foi atribu�da no contrato n� numcont.<BR>Deve atribu�-lo para finalizar a rota.};
$mess[6401] = qq{Nenhuma maquina foi atribu�da nos contratos n� numcont.<BR>Deve atribu�-los para finalizar a rota.};

# Modification de contrat
$mess[6500] = qq{Modifica��o de contrato};
# Version tableau
# Colonnes
$mess[6510] = qq{N� contrato};
$mess[6511] = qq{Estato};
$mess[6512] = qq{Cliente};
$mess[6513] = qq{Obra};
$mess[6514] = qq{Contacto comando};
$mess[6515] = qq{Contacto obra};
$mess[6516] = qq{M�quina};
$mess[6517] = qq{Dura��o};
$mess[6518] = qq{Dia a mais};
$mess[6519] = qq{Dia a menos};
$mess[6520] = qq{Pr��o};
$mess[6521] = qq{Seguro};
$mess[6522] = qq{Transporte};
$mess[6523] = qq{Transporte cliente};
$mess[6524] = qq{Cais};
$mess[6525] = qq{Montante total sem taxas};
$mess[6526] = qq{Pedido};
$mess[6527] = qq{Coment�rio};
$mess[6528] = qq{Nota};
# Autres
$mess[6550] = qq{fora de parque};
$mess[6551] = qq{Entrega};
$mess[6552] = qq{Recolha};
$mess[6553] = qq{/ dia};
$mess[6554] = qq{/ m�s};
$mess[6555] = qq{empreitada};
$mess[6556] = qq{dia};
$mess[6557] = qq{dias};
# Affichage contrat
$mess[6600] = qq{Cliente};
$mess[6601] = qq{Nome};
$mess[6602] = qq{Grelha};
$mess[6603] = qq{Contacto comando};
$mess[6604] = qq{Obra};
$mess[6605] = qq{Nome};
$mess[6606] = qq{Localidade};
$mess[6607] = qq{Contacto obra};
$mess[6608] = qq{M�quina};
$mess[6609] = qq{Designa��o};
$mess[6610] = qq{Energia};
$mess[6611] = qq{N� parque};
$mess[6612] = qq{Fora de parque};
$mess[6613] = qq{Taxa de utiliza��o};
$mess[6614] = qq{Custo};
$mess[6615] = qq{R�cio objectivo};
$mess[6616] = qq{Pre�o objectivo};
$mess[6617] = qq{R�cio};
$mess[6618] = qq{Dura��o};
$mess[6619] = qq{Data de partida};
$mess[6620] = qq{Data de regresso};
$mess[6621] = qq{ou seja};
$mess[6622] = qq{dia};
$mess[6623] = qq{dias};
$mess[6624] = qq{�til};
$mess[6625] = qq{�teis};
$mess[6626] = qq{calend�rio};
$mess[6627] = qq{calend�rios};
$mess[6628] = qq{Dia a mais};
$mess[6629] = qq{Dia a menos};
$mess[6630] = qq{Tarifa};
$mess[6631] = qq{Pr��o};
$mess[6632] = qq{por dia};
$mess[6633] = qq{por m�s};
$mess[6634] = qq{empreitada};
$mess[6635] = qq{Seguro};
$mess[6636] = qq{Transporte};
$mess[6637] = qq{Entrega};
$mess[6638] = qq{Recolha};
$mess[6639] = qq{Transporte cliente};
$mess[6640] = qq{Cais};
$mess[6641] = qq{Servi�os complementares};
$mess[6642] = qq{Acrescentar um servi�o};
$mess[6643] = qq{Suprimir um servi�o};
$mess[6644] = qq{Montante total sem taxas};
$mess[6645] = qq{Informa��es complementares};
$mess[6646] = qq{Pedido};
$mess[6647] = qq{Coment�rio};
$mess[6648] = qq{Nota};
$mess[6649] = qq{Calcular};
$mess[6650] = qq{Alterar o contrato};
$mess[6651] = qq{Fechar o contrato};
$mess[6652] = qq{Passar o contrato em factura��o};
$mess[6653] = qq{Contrato de origem};
$mess[6654] = qq{Acess�rios};
$mess[6655] = qq{do};
$mess[6656] = qq{ao};
$mess[6657] = qq{/ dia};
$mess[6658] = qq{Suprimido};
$mess[6659] = qq{Facturado};
$mess[6660] = qq{Tarifa};
$mess[6661] = qq{Diversos};
$mess[6662] = qq{Facturado em fim de contrato};
$mess[6663] = qq{Escolham um servi�o na lista};
$mess[6664] = qq{Inteiramente facturado};
$mess[6665] = qq{Facturado parcialmente at�};
$mess[6666] = qq{N�o facturado};
$mess[6667] = qq{Ver a tarifa};
$mess[6668] = qq{Combust�vel};
$mess[6669] = qq{Quantidade};
$mess[6670] = qq{litro};
$mess[6671] = qq{litros};
$mess[6672] = qq{Montante};
$mess[6673] = qq{Fornecedor};
$mess[6674] = qq{j� facturado};
$mess[6675] = qq{j� facturados};
$mess[6676] = qq{Entrega n�o planificada ou n�o validada};
$mess[6677] = qq{Redu��o};
# Messages de confirmation
$mess[6750] = qq{Imposs�vel fechar o contrato mais de 4 dias antes da data de regresso};
$mess[6751] = qq{O contrato foi parado};
$mess[6752] = qq{O contrato filho foi parado};
$mess[6753] = qq{O contrato est� pronto para ser facturado e n�o pode mais ser alterado};
$mess[6754] = qq{O contrato filho est� pronto para ser facturado};
$mess[6755] = qq{O contrato foi posto em dia};
$mess[6756] = qq{O contrato posto em dia};
$mess[6757] = qq{Data de regresso anterior � data de �ltima factura��o};
$mess[6758] = qq{Data de regresso incorrecta};
$mess[6759] = qq{Data de partida incorrecta};
$mess[6760] = qq{Data de regresso anterior � data de partida};
$mess[6761] = qq{Imposs�vel passar o contrato em factura��o porque o combust�vel n�o foi apreendido};
$mess[6762] = qq{Imposs�vel passar o contrato em factura��o porque o transporte regresso n�o foi planificado};
$mess[6763] = qq{Imposs�vel passar o contrato em factura��o porque a redu��o excepcional n�o � validado};
$mess[6764] = qq{S� os dias a mais e os servi�os suplementares cujo valor n�o � negativo podem ser inseridos};
$mess[6765] = qq{N�o se pode inserir um servi�o cujo o valor � negativo};
$mess[6766] = qq{Montant du transport inf�rieur au forfait machine suppl�mentaire minimum.};
$mess[6767] = qq{Montant du service inf�rieur au montant minimum autoris� : };

# Remise en cours de contrat (nouvelle version)
$mess[6800] = qq{Reabertura de contrato};
# Recherche
$mess[6810] = qq{Contrato a reabrir};
$mess[6811] = qq{Reabrir o contrato};
# Messages validation
$mess[6820] = qq{O contrato foi reaberto};
$mess[6821] = qq{Seleccionar um contrato a reabrir};
$mess[6822] = qq{O contrato j� foi reaberto};

# Documents types
$mess[6900] = qq{Documentos tipo};
$mess[6901] = qq{Download};
$mess[6910] = qq{Folder};
$mess[6911] = qq{Tipo de documento desconhecido};
$mess[6912] = qq{Documento Microsoft Word};
$mess[6913] = qq{Documento Microsoft Excel};
$mess[6914] = qq{Documento Acrobat PDF};
$mess[6915] = qq{Imagem JPEG};
$mess[6916] = qq{Imagem GIF};
$mess[6917] = qq{Imagem BMP};
$mess[6918] = qq{Imagem TIFF};

# Remise exceptionnelle
$mess[6950] = qq{Redu��o excepcional};
$mess[6951] = qq{Pr��o antes de redu��o};
$mess[6952] = qq{Redu��o excepcional};
$mess[6953] = qq{Pr��o com redu��o};
$mess[6954] = qq{Justifica��o};
$mess[6955] = qq{Minuta factura};
$mess[6956] = qq{Redu��o aceite};
$mess[6957] = qq{Redu��o em espera de valida��o};
$mess[6958] = qq{Redu��o recusado};
$mess[6959] = qq{Redu��o excepcional};
$mess[6960] = qq{Redu��o excepcional};
$mess[6961] = qq{pedido};
$mess[6962] = qq{Tratado por};
$mess[6963] = qq{Limitado � 36 car�cteres};
$mess[6964] = qq{Redu��o superior � 100%};
$mess[6965] = qq{Pre�o};

# Modification du tarif
$mess[7000] = qq{Tarifa b�sica};
$mess[7001] = qq{Pre�o por dia (sem taxas)};
$mess[7002] = qq{TARIFA B�SICA};
$mess[7003] = qq{Abril 2006};
$mess[7004] = qq{a};
$mess[7005] = qq{mais de};

# Chargement de la page
$mess[7050] = qq{Carregamento da p�gina em curso};
$mess[7051] = qq{Carregamento da p�gina};

# Modification commande transport
$mess[7100] = qq{Modifica��o de encomenda de transporte externa};
$mess[7101] = qq{Controlo de encomenda de transporte externo};
#
$mess[7110] = qq{N� encomenda};
$mess[7111] = qq{Transportador};
$mess[7112] = qq{N� contrato};
$mess[7113] = qq{Data de encomenda};
$mess[7114] = qq{Consultar a encomenda};
$mess[7115] = qq{Registar as modifica��es};
$mess[7116] = qq{Aten��o};
$mess[7118] = qq{N�o linha para esta encomenda};
$mess[7119] = qq{Encomenda};
$mess[7120] = qq{Ag�ncia};
$mess[7121] = qq{Estabelecido por};
$mess[7123] = qq{Tipo};
$mess[7124] = qq{Data de controlo factura};
$mess[7125] = qq{N� factura};
$mess[7126] = qq{Montante facturado};
$mess[7127] = qq{Suprimir a encomenda};
$mess[7128] = qq{A encomenda foi suprimida};
$mess[7129] = qq{Nova procura};
$mess[7130] = qq{A encomenda foi alterada};
$mess[7131] = qq{A data de transporte n�o � v�lida:};
$mess[7132] = qq{Encomenda n�o controlada};
$mess[7133] = qq{Entre o};
$mess[7134] = qq{e o};
$mess[7135] = qq{=};
$mess[7136] = qq{&ge;};
$mess[7137] = qq{A data n�o � v�lida:};
$mess[7138] = qq{Encomendas controladas};
$mess[7139] = qq{N�o};
$mess[7140] = qq{Sim};
$mess[7141] = qq{Indiferente};

# Contrat cadre
$mess[7150] = qq{Contrat Cadre};

# Suffixe Sous-location et Full service
$mess[7200] = qq{SL};
$mess[7201] = qq{FS};

# Commandes de transport (nouvelle interface)
$mess[7300] = qq{Nova encomenda};
$mess[7301] = qq{Acrescentar uma nova encomenda};
$mess[7302] = qq{Seleccionar/Deseleccionar todos};
$mess[7303] = qq{Validar as modifica��es};
$mess[7304] = qq{Imprimir a encomenda};
$mess[7305] = qq{Volta};
$mess[7306] = qq{Criar a volta};
$mess[7307] = qq{Queiram escolher um transportador};
$mess[7308] = qq{A encomenda #XXX# � criada};
$mess[7309] = qq{As modifica��es s�o registadas};
$mess[7310] = qq{A volta #XXX# foi criada};
$mess[7311] = qq{Existe outras voltas para o mesmo transportador que poder�o ser ordenadas apenas quando esta for};
$mess[7312] = qq{N�o h� actualmente nenhum transporte a atribuir};
$mess[7313] = qq{Actualizar};
$mess[7314] = qq{Retirar da encomenda};
$mess[7315] = qq{A volta j� � criada (n� #XXX#)};
$mess[7316] = qq{Imposs�vel alterar a encomenda n� #YYY# porque existe j� uma volta (n� #XXX#)};
$mess[7317] = qq{Aten��o, qualquer encomenda passada em volta n�o � pass�veis de altera��o};
$mess[7318] = qq{Aten��o, ao menos un contrato est� errado (linha vermelha) tem uma encomenda de transporte. Regularizar o(s) para poder finalizar a(s) encomenda(s) correspondente(s)};


# Remise en arr�t de contrat
$mess[7400] = qq{Repor Contrato parado};
# Recherche
$mess[7410] = qq{Contrato a colocar em parado};
$mess[7411] = qq{Repor em parado o contrato};
# Messages validation
$mess[7420] = qq{O contrato foi posto em parado};
$mess[7421] = qq{Seleccionar um contrato a repor em parado};
$mess[7422] = qq{O contrato j� foi posto em parado};
$mess[7423] = qq{O contrato n�o pode ser posto em parado :};
$mess[7424] = qq{ - o contrato n�o est� no estado 'posto em factura��o'};
$mess[7425] = qq{ - o contrato j� foi totalmente facturado};
# L�gende
$mess[7430] = qq{Factura��o Estado};
$mess[7431] = qq{Factura��o Parcial};
$mess[7432] = qq{Factura��o Parcial Final (alugar totalmente carregada)};
$mess[7433] = qq{Factura��o Final (contrato totalmente carregada)};

# Remise en cours du pointage d'une machine dans une tourn�e
$mess[7500] = qq{Repor en curso o transporte};
$mess[7501] = qq{Selec��o};
$mess[7502] = qq{Procurar};
# Recherche
$mess[7510] = qq{Contrato};
$mess[7511] = qq{M�quina};
$mess[7512] = qq{Falhado};
$mess[7513] = qq{Efectuada};
$mess[7514] = qq{Re-atribui��o};
$mess[7515] = qq{Em curso};
$mess[7516] = qq{N�o atribu�do};
$mess[7517] = qq{Repor em curso a marca��o};
# Messages validation
$mess[7520] = qq{A marca��o da m�quina na volta foi reposto em curso};
$mess[7521] = qq{Por favor colocar um n� de contrato e/ou uma m�quina};
$mess[7522] = qq{A marca��o da m�quina na volta n�o pode ser reposto em curso :};
$mess[7523] = qq{ - imposs�vel sobre um };
$mess[7524] = qq{ - marca��o actual : };
$mess[7525] = qq{A marca��o da m�quina na volta foi reposto em curso};
$mess[7526] = qq{Por favor  seleccionar uma linha na volta a repor em curso};
$mess[7527] = qq{ - O estado actual da m�quina n�o permite};
$mess[7528] = qq{ - O contrato est� no estado �Posto em factura��o�};
$mess[7529] = qq{ - O contrato est� totalmente facturado};
$mess[7540] = qq{ - poss�vel apenas no �ltimo contrato};
# Colonnes du tableau
$mess[7530] = qq{Turno};
$mess[7531] = qq{Contrato};
$mess[7532] = qq{Estato contrato};
$mess[7533] = qq{M�quina};
$mess[7537] = qq{Estato m�quina};
$mess[7534] = qq{Tipo};
$mess[7535] = qq{Marca��o};
$mess[7536] = qq{S� as linhas do �ltimo contrato de tipo : "Entrega" ou "Recupera��o" e que tem a marca��o : "Falhado" ou "Efectuado" s�o seleccion�veis};

# Mise � jour des alertes et des stocks
$mess[7600] = qq{Actualiza��o alertas e Stocks};
$mess[7601] = qq{Para realizar atualiza��es na ag�ncia %s : };

# Visu des transports par r�gion
$mess[7620] = qq{Visualisation des transports sur la r�gion};
$mess[7621] = qq{R�-actualiser};
$mess[7622] = qq{OUI};
$mess[7623] = qq{non};

# Visu des transports par r�gion
$mess[7649] = qq{Taxa de utiliza��o no %s - };
$mess[7650] = qq{Taxa de utiliza��o no %s - Regi�o };
$mess[7651] = qq{Fam�lia de pre�os};
$mess[7652] = qq{% Utiliza��o instant�nea (nb maq.)};
$mess[7653] = qq{Regional};
$mess[7654] = qq{Nacional};
$mess[7655] = qq{TOTAL};
$mess[7656] = qq{Voc� n�o t acesso este funcionalidade.};

# Report des libell�s de l'interface "Nouveau client"
$mess[7700] = qq{APE};
$mess[7701] = qq{sector de actividade};
$mess[7702] = qq{SIRET};
$mess[7703] = qq{nome};
$mess[7704] = qq{morada};
$mess[7705] = qq{morada comp};
$mess[7706] = qq{localidade};
$mess[7707] = qq{telefone};
$mess[7708] = qq{telemovel};
$mess[7709] = qq{Fax};
$mess[7710] = qq{contacto};
$mess[7711] = qq{Coment�rio};
$mess[7712] = qq{modo pagamento};
$mess[7713] = qq{banco};
$mess[7714] = qq{RIB ?};
$mess[7715] = qq{Grelha};
$mess[7716] = qq{Grupo};
$mess[7717] = qq{Qualifica��o};
$mess[7718] = qq{Email Cliente};
$mess[7719] = qq{Criador};
$mess[7720] = qq{Ag�ncia criadora};
$mess[7721] = qq{Fornecer sempre documentos da m�quina};
$mess[7800] = qq{Per�metro nacional};

# Pro forma
$mess[8000] = qq{Legenda para casos err�neos};
$mess[8001] = qq{Pro forma n�o criada};
$mess[8002] = qq{Pro forma n�o atualizada};
$mess[8003] = qq{Pro forma n�o finalizada};
$mess[8004] = qq{Or�amento excecional de aluguer em espera};
$mess[8005] = qq{Or�amento excecional de transporte em espera};
$mess[8006] = qq{Cliente com abertura de conta em espera};

$mess[8100] = qq{Imposs�vel de planificar o transporte no contrato #XXX#};
$mess[8101] = qq{Erro de contrato #XXX#};
$mess[8102] = qq{O Contrato comporta uma fatura pro forma ou uma fatura de dep�sito que n�o ser� v�lida ap�s confirma��o};
$mess[8103] = qq{O cliente tem faturas pro formas ou de faturas de dep�sito que n�o ser�o v�lidas depois de confirma��o, nos documentos seguintes};


return @mess;
}

sub liste_ma {
my @mess;
#message
#sub saisie
$mess[0] = qq{Assurance};
$mess[1] = qq{comprise};
$mess[2] = qq{recherche};
$mess[3] = qq{ non };
$mess[4] = qq{Rechercher};
$mess[5] = qq{Veuillez saisir ce champ};
$mess[6] = qq{Nouveau Client};
$mess[7] = qq{Veuillez saisir le(s) champ(s)};
$mess[8] = qq{Veuillez saisir un Tarif Journalier OU un Forfait OU un Tarif Mensuel ET une Dur�e Mensuelle};
$mess[9] = qq{Non renseign�};
$mess[10] = qq{Conditions g�n�rales de location � approuver};
$mess[11] = qq{Convention cadre - ouverture de compte client � adresser};
$mess[12] = qq{Pour rattachement � un groupe contacter le Service Support Commercial};
#sub traitment
$mess[50] = qq{La date de d�but est ant�rieure � aujourd'hui};
$mess[51] = qq{La date de fin est ant�rieure � la date de d�but};
$mess[52] = qq{Mauvaise date !};
$mess[53] = qq{Date de d�but ant�rieure � aujourd\\'hui.\\nConserver cette date ?};
$mess[54] = qq{Nb. de jour(s) f�ri�s};
$mess[55] = qq{La dur�e};
$mess[56] = qq{Montant en DH};
$mess[57] = qq{Montant de l'assurance en DH};
$mess[58] = qq{Valider};
$mess[59] = qq{Dupliquer Contrat};
$mess[60] = qq{Le contrat};
$mess[61] = qq{est saisi, Duplication des donn�es};
$mess[62] = qq{Date non valide};
#sub recherche2
$mess[100] = qq{Le nombre des r�sultats de votre recherche est de};
$mess[101] = qq{Recherche};
$mess[102] = qq{Rechercher};
$mess[103] = qq{Veuillez saisir un des champs};
$mess[104] = qq{Validez votre Recherche};
$mess[105] = qq{r�sultat};
$mess[106] = qq{r�sultats};
$mess[107] = qq{Retour � la recherche};
$mess[108] = qq{Retour};
#sub traitment2
$mess[150] = qq{Le nombre de r�sultats de votre recherche est de};
$mess[151] = qq{Pas de r�sultat pour votre recherche};
$mess[152] = qq{Retour};
$mess[153] = qq{Voir devis};
$mess[154] = qq{CONTR�LER};
$mess[155] = qq{ARR�TER Contrat};
$mess[156] = qq{MODIFIER Contrat};
$mess[157] = qq{ANNULER Contrat};
$mess[158] = qq{Jour Plus/Moins};
$mess[159] = qq{Duplication des donn�es};
$mess[160] = qq{MODIFIER Chantier};
$mess[161] = qq{MODIFIER Info. Client};
$mess[162] = qq{Voir Contrat};
$mess[163] = qq{R�server Mod�le};
$mess[164] = qq{Trouver};
$mess[165] = qq{bloqu�};
$mess[166] = qq{d�bloqu�};
$mess[167] = qq{Client <ste>(<code>) <libbloc>};
$mess[168] = qq{Bonjour\nle Client <ste>(<code>) de <cp>,<ville>\nest <libbloc>\n@+\nBonne journ�e\n};
#sub stock
$mess[200] = qq{R�server Mod�le};
#sub nvclient
$mess[250] = qq{V�rifier le RIB};
$mess[251] = qq{RIB};
$mess[252] = qq{correct};
$mess[253] = qq{incorrect};
$mess[254] = qq{Nouveau Client};
$mess[255] = qq{Cherche Lycos ! Cherche !};
#sub bas form hauttab
$mess[300] = qq{Imprimer};
$mess[301] = qq{Fermer};
#sub saisiecnt
$mess[350] = qq{le prospect};
$mess[351] = qq{code client};
$mess[352] = qq{de l'agence};
$mess[353] = qq{Siret};
$mess[354] = qq{RIB};
$mess[355] = qq{T�l.};
$mess[356] = qq{Fax};
$mess[357] = qq{devient un Client d'Acc�s Industrie};
$mess[358] = qq{pour le contrat N�};
$mess[359] = qq{qui d�bute le};
$mess[360] = qq{devient client};
$mess[361] = qq{N� APE};
#sub traitnvclient
$mess[400] = qq{Origine};
$mess[401] = qq{Document origine};
$mess[402] = qq{Devis};
$mess[403] = qq{Pr�-contrat};
$mess[404] = qq{N� APE};
$mess[405] = qq{Siret};
$mess[406] = qq{Libell�};
$mess[407] = qq{Adresse};
$mess[408] = qq{Ville};
$mess[409] = qq{T�l.};
$mess[410] = qq{Portable};
$mess[411] = qq{Fax};
$mess[412] = qq{Contact};
$mess[413] = qq{Commentaire};
$mess[414] = qq{Banque};
$mess[415] = qq{RIB};
$mess[416] = qq{Mode de paiement};
$mess[417] = qq{Nouveau client de};
$mess[418] = qq{Secteur d'activit�};
#sub arretmodif
$mess[450] = qq{Nb. de jour(s) f�ri�(s)};
$mess[451] = qq{La date de fin est ant�rieure � la date de d�but};
$mess[452] = qq{Le contrat};
$mess[453] = qq{est arr�t�};
$mess[454] = qq{est modifi�};
#sub transport
$mess[500] = qq{Montant};
$mess[501] = qq{Contrat � �diter};
$mess[502] = qq{Collectif};
$mess[503] = qq{Code Client};
$mess[504] = qq{Client Valide ?};
$mess[505] = qq{Transport � attribuer};
$mess[506] = qq{Devis transporteur};
$mess[507] = qq{Commande transporteur};
$mess[508] = qq{Passer � Ordonner Tourn�e};
$mess[509] = qq{Date de transport};
$mess[510] = qq{Imprimer contrat};
$mess[511] = qq{Nouveau Client};
$mess[512] = qq{Panne machine};
$mess[513] = qq{Impossible de supprimer le transporteur sur le contrat};
#sub traitattmach
$mess[550] = qq{La Machine};
$mess[551] = qq{est s�lectionn�e au moins 2 fois};
$mess[552] = qq{La machine %s n'a pas le m�me mod�le que le contrat %s auquel vous essayez de l'attribuer};
$mess[553] = qq{Vous avez dej� attribu� cette machine};
#sub attmach
$mess[600] = qq{Veuillez saisir un num�ro de Parc d'une machine disponible};
$mess[601] = qq{Ce num�ro est incompatible avec le parc actuel de l'agence};
$mess[602] = qq{Attribuer Machine};
$mess[603] = qq{Imprimer liste machine};
#sub carburant
$mess[650] = qq{Contrat, D�signation};
$mess[651] = qq{Attribuer Carburant};
#sub transfert
$mess[700] = qq{Machines � r�cup�rer};
$mess[701] = qq{Transf�rer};
$mess[702] = qq{Passer � BL/BR/Tourn�e};
$mess[703] = qq{Machines � livrer};
$mess[704] = qq{Transport � attribuer};
$mess[705] = qq{N� Parc};
#sub tournee
$mess[750] = qq{Tourn�e N�};
$mess[751] = qq{Reste Capacit� (Kg) avant L/R/T};
$mess[752] = qq{En cours};
$mess[753] = qq{Effectu�e};
$mess[754] = qq{�chou�e};
$mess[755] = qq{Capacit�};
$mess[756] = qq{Reste};
$mess[757] = qq{Masse de r�cup.};
$mess[758] = qq{Masse de livraison};
$mess[759] = qq{Passer � BL/BR/Tourn�e};
$mess[760] = qq{Valider Tourn�e};
$mess[761] = qq{Le contr�le de la Tourn�e};
$mess[762] = qq{n'est pas complet, };
$mess[763] = qq{machine(s) � Pointer};
$mess[764] = qq{Le contr�le de la Tourn�e N�};
$mess[765] = qq{est complet};
$mess[780] = qq{Date tourn�e};
#sub valtourn
$mess[800] = qq{La Tourn�e N�};
$mess[801] = qq{devra �tre valid�e};
$mess[802] = qq{est valid�e};
#sub verification
$mess[850] = qq{Date de Contr�le : < 2 mois(*), < 15 jours(**), < 1 semaine (***), D�pass�e (****)};
$mess[851] = qq{Machines en Livraison};
$mess[852] = qq{Machines en transfert inter-chantiers};
$mess[853] = qq{Machine en Livraison};
$mess[854] = qq{Machine en R�cup�ration};
$mess[855] = qq{Machine en Transfert};
$mess[856] = qq{Machine en V�rification};
$mess[857] = qq{V�rification � R�vision};
$mess[858] = qq{R�vision � V�rification};
$mess[859] = qq{Disponible � V�rification};
$mess[860] = qq{Panne � V�rification};
$mess[861] = qq{V�rification � Panne};
$mess[862] = qq{Machine en R�vision};
$mess[863] = qq{Panne � R�vision};
$mess[864] = qq{R�vision � Panne};
$mess[865] = qq{Machine en Panne};
$mess[866] = qq{R�vision � Disponible};
$mess[867] = qq{V�rification � Disponible};
$mess[868] = qq{Panne � Disponible};
$mess[869] = qq{Machine Disponible};
$mess[870] = qq{�tat};
$mess[871] = qq{Impression Liste Machine};
$mess[872] = qq{Machines en R�cup�ration};
$mess[873] = qq{Machines en transfert inter-agences};
$mess[874] = qq{Impression envoy�e sur l'imprimante};
#sub devis
$mess[900] = qq{Le Devis};
$mess[901] = qq{est enregistr�};
$mess[902] = qq{Historique};
$mess[903] = qq{Nouveau Client};
$mess[904] = qq{Assurance};
$mess[905] = qq{ non };
$mess[906] = qq{comprise};
$mess[907] = qq{Passer au Pr�-contrat};
$mess[908] = qq{Assurance comprise};
$mess[909] = qq{Assurance Non comprise};
$mess[910] = qq{Modifier le pied de devis};
$mess[911] = qq{Abandonner};
$mess[912] = qq{Impression Devis};
$mess[913] = qq{Envoi par fax ou impression locale};
$mess[914] = qq{Erreur};
$mess[915] = qq{Pr�visualisation du fichier Acrobat};
$mess[916] = qq{Aucune page � afficher};
$mess[917] = qq{La zone est obligatoire};
$mess[918] = qq{Vous devez faire une estimation du transport.};
$mess[919] = qq{Impression impossible : remise exceptionnelle transport en attente ou transport incorrect.};
$mess[920] = qq{Impression impossible : remise exceptionnelle location en attente.};
$mess[921] = qq{Etes-vous s�r de vouloir annuler la remise exceptionnelle?};
$mess[922] = qq{Annuler la remise excep.};
$mess[923] = qq{Fiche client};
#sub traitdevis
$mess[950] = qq{Le devis};
$mess[951] = qq{devient un contrat};
$mess[952] = qq{est abandonn�};
$mess[953] = qq{Choisissez un motif d'abandon};
$mess[954] = qq{Autre};
$mess[955] = qq{Veuillez choisir un motif avant d'abandonner le devis};
$mess[956] = qq{Devis abandonn�};
$mess[957] = qq{Objet : Votre demande de devis location MAROC ELEVATION};
$mess[958] = qq{Madame, monsieur, <br><br>
Suite � votre demande, vous trouverez, ci-joint, le devis de location �tabli sur la base des informations que vous nous avez communiqu�es.
Je reste � votre disposition au : _TELAG_ pour tout compl�ment d'information.<br><br>
Souhaitant avoir r�pondu � vos attentes, je vous prie d'agr�er, Madame, monsieur, l'expression de mes sinc�res salutations.<br><br>
  _NOMEXP_};
#sub ville
$mess[1000] = qq{Agence(s)};
$mess[1001] = qq{ou};
#sub facturation
$mess[1050] = qq{Nous sommes le };
$mess[1051] = qq{contrat(s) � facturer};
$mess[1052] = qq{Facturer};
$mess[1053] = qq{La facturation du};
$mess[1054] = qq{mois � d�j� �t� effectu�e};
$mess[1055] = qq{Il y a};
$mess[1056] = qq{Contrats � passer en facturation};
$mess[1057] = qq{On facture};
$mess[1058] = qq{Exportation vers Sage};
$mess[1059] = qq{La facturation ira};
$mess[1060] = qq{La facturation a d�j� �t� effectu�e};
$mess[1061] = qq{� saisir};
$mess[1062] = qq{jusqu'au};
$mess[1063] = qq{Contrats � arr�ter};
$mess[1064] = qq{Carburants � saisir};
#sub traitfac
$mess[1100] = qq{Le montant de};
$mess[1101] = qq{est de};
$mess[1102] = qq{pour};
$mess[1103] = qq{factures};
$mess[1104] = qq{Le montant Total des factures est de};
#sub tableau
$mess[1150] = qq{Tri par date};
$mess[1151] = qq{Tri par ville};
$mess[1152] = qq{R�vision};
$mess[1153] = qq{Panne};
$mess[1154] = qq{Contr�le};
$mess[1155] = qq{Disponible};
$mess[1156] = qq{Total machine(s)};
$mess[1157] = qq{�tat};
$mess[1158] = qq{Impression Liste Machine};
$mess[1159] = qq{V�rification};
#sub transfagag
$mess[1200] = qq{Transf�rer};
#sub validnvclient
$mess[1250] = qq{le Code Client};
$mess[1251] = qq{le Collectif};
$mess[1252] = qq{entre};
$mess[1253] = qq{� la ligne};
#sub mois_stat
$mess[1300] = qq{Historique};
#sub stat_mois
$mess[1350] = qq{D�but du mois };
$mess[1351] = qq{ et fin de mois};
$mess[1352] = qq{Nombre};
$mess[1353] = qq{Total};
$mess[1354] = qq{Moyenne};
$mess[1355] = qq{�cart type};
$mess[1356] = qq{C.A. agence};
$mess[1357] = qq{Dur�e};
$mess[1358] = qq{Assurance Non Comprise};
$mess[1359] = qq{Transport};
$mess[1360] = qq{Sous Traitant Transport};
$mess[1361] = qq{Carburant};
$mess[1362] = qq{Contrat Actif};
$mess[1363] = qq{Contrat Arr�t�};
$mess[1364] = qq{Contrat de moins de 5 jours};
$mess[1365] = qq{Contrat de plus de 5 jours};
$mess[1366] = qq{Contrat Mensuel};
$mess[1367] = qq{Forfait};
$mess[1368] = qq{Famille};
$mess[1369] = qq{C.A.};
$mess[1370] = qq{Nb. Parc};
$mess[1371] = qq{jour(s) Lou�(s)};
$mess[1372] = qq{Nb. Contrat};
$mess[1373] = qq{Jours Mois};
$mess[1374] = qq{Moyenne Prix Lou�};
$mess[1375] = qq{Moyenne Prix Parc};
$mess[1376] = qq{Rotation(Rentabilit�) en %};
$mess[1377] = qq{Ratio};
$mess[1378] = qq{Moyenne Jours};
$mess[1379] = qq{TOTAL};
$mess[1380] = qq{D�signation};
#sub nomade
$mess[1400] = qq{Je suis �};
$mess[1401] = qq{Agences};
$mess[1402] = qq{Je me t�l�porte};
#sub saisiemach
$mess[1450] = qq{La Machine};
$mess[1451] = qq{est saisie};
$mess[1452] = qq{Parc};
$mess[1453] = qq{ou};
$mess[1454] = qq{de S�rie};
$mess[1455] = qq{existe d�j�};
$mess[1456] = qq{Valider};
$mess[1457] = qq{Pr�-contrat};
$mess[1458] = qq{Contrat};
$mess[1459] = qq{Arr�t};
$mess[1460] = qq{Archive};
$mess[1461] = qq{Annul�};
$mess[1462] = qq{Livraison};
$mess[1463] = qq{R�cup�ration};
$mess[1464] = qq{Transfert Chantier};
$mess[1465] = qq{Mis en facturation};
#message impression
$mess[1500] = qq{BON DE LIVRAISON};
$mess[1501] = qq{BON DE RETRAIT};
#imprssion ass
$mess[1550] = qq{Assurance factur�e au client = % du prix de location jour fois jours calendaires};
$mess[1551] = qq{Assurance non factur�e};
$mess[1552] = qq{Assur�e par le client};
$mess[1553] = qq{Assurance en plus = 10% du prix de location};
$mess[1554] = qq{};
$mess[1555] = qq{MACHINE ASSUR�E PAR LE CLIENT};
$mess[1556] = qq{Assurance factur�e au client = };
$mess[1557] = qq{% du prix de location jour fois jours calendaires};
$mess[1558] = qq{Assurance incluse};
#sub traitfac
$mess[1600] = qq{Tarif Mensuel};
$mess[1601] = qq{Forfait};
$mess[1602] = qq{Avoir};
$mess[1603] = qq{Transport aller};
$mess[1604] = qq{Transport retour};
$mess[1605] = qq{Carburant};
$mess[1606] = qq{Assurance 10%};
$mess[1607] = qq{ Du };
$mess[1608] = qq{ au };
$mess[1609] = qq{Heures deduites};
$mess[1610] = qq{Heures supplementaires};
$mess[1611] = qq{Heures samedi};
$mess[1612] = qq{Heures dimanche};
$mess[1613] = qq{Jours deduits};
$mess[1614] = qq{Jours supplementaires};
$mess[1615] = qq{Cette facture comprend les jours travailles des 2 mois};
$mess[1616] = qq{Participation au recyclage};
$mess[1617] = qq{Nettoyage};
$mess[1618] = qq{Service};
$mess[1619] = qq{Remise exceptionnelle transport de};
$mess[1620] = qq{Rem. except. XX% sur Aller};
$mess[1621] = qq{Rem. except. XX% sur Retour};
$mess[1622] = qq{Rem. except. XX% sur Aller (remise pour cumul * mach. incluse)};
$mess[1623] = qq{Rem. except. XX% sur Retour(remise pour cumul * mach. incluse)};
$mess[1624] = qq{Rem. pour cumul * mach. XX%};
$mess[1625] = qq{Rem. pour cumul * mach. XX%};
$mess[1626] = qq{Assurance n�goci�e 5%};
$mess[1627] = qq{Rem. except. sur Aller};
$mess[1628] = qq{Rem. except. sur Retour};
#arret contratraie
$mess[1650] = qq{Arr�t Impossible : contacter le responsable de la logistique pour pointer la livraison dans la tourn�e N�};
#sub traitcasse
$mess[1700] = qq{le Devis est saisi};
$mess[1701] = qq{Impression du document};
#annuler client
$mess[1750] = qq{Vous allez supprimer un Client ! Validez pour confirmer !};
$mess[1751] = qq{Le 1er Client est supprim�. Les Devis et Contrats ont �t� report�s sur le 2nd.};
$mess[1752] = qq{&nbsp;At&nbsp;&nbsp;&nbsp;Acc&nbsp;&nbsp;Ref};
$mess[1753] = qq{At&nbsp;:&nbsp;Attente&nbsp;&nbsp;&nbsp;Acc&nbsp;:&nbsp;Accept�&nbsp;&nbsp;&nbsp;Ref&nbsp;:&nbsp;Refus�};
#doublon client
$mess[1800] = qq{Le Client/Code Client };
$mess[1801] = qq{existe d�j� : Recherchez � ce libell�};
$mess[1802] = qq{a �t� cr��, vous pouvez rechercher � ce libell�};
$mess[1803] = qq{Un champ n'as pas �t� rempli correctement};
$mess[1804] = qq{Ce SIRET est d�j� utilis� par les client(s) suivant(s) : %s. Veuillez contacter le service Support Commercial afin de poursuivre la cr�ation du client.};
#sub verification
$mess[1850] = qq{Contr�le � V�rification};
$mess[1851] = qq{V�rification � Contr�le};
$mess[1852] = qq{Machine(s) en Contr�le};
#chg client Contrat
$mess[1900] = qq{Le Client va �tre chang� ! Validez pour confirmer !};
$mess[1901] = qq{Le Client est chang�};
#remettre en cours contrat
$mess[1950] = qq{Le Contrat va �tre remis en cours ! Validez pour confirmer !};
$mess[1951] = qq{Le Contrat est remis en cours};
$mess[1952] = qq{Un transport est planifi� pour ce contrat ! Il faut l'annuler avant de pouvoir remettre en cours le contrat !};
#newstat
$mess[2000] = qq{Agence};
$mess[2001] = qq{C.A. Mois};
$mess[2002] = qq{C.A. Transport};
$mess[2003] = qq{C.A. Carburant};
$mess[2004] = qq{C.A. SAV};
$mess[2005] = qq{C.A. Forfait};
$mess[2006] = qq{Co�t Financier};
$mess[2007] = qq{Famille};
$mess[2008] = qq{Nb. Machines};
$mess[2009] = qq{C.A. Machine Jour};
$mess[2010] = qq{C.A. Moyen Jour};
$mess[2011] = qq{Taux de Rotation Financier en %};
$mess[2012] = qq{Taux de Rotation Machine en %};
$mess[2013] = qq{Mod�le};
$mess[2014] = qq{Pour la famille};
$mess[2015] = qq{Taux d'occupation (%)};
$mess[2016] = qq{Taux d'occupation (%) depuis le d�but du mois};
$mess[2017] = qq{Taux d'occupation (%) du mois pr�c�dent};
$mess[2018] = qq{Comparatif Mensuel au mois pr�c�dent};
#consultation contrat
$mess[2050] = qq{N� Contrat};
$mess[2051] = qq{�tat Contrat};
$mess[2052] = qq{Client};
$mess[2053] = qq{T�l. Client};
$mess[2054] = qq{Fax Client};
$mess[2055] = qq{Contact Client};
$mess[2056] = qq{Chantier};
$mess[2057] = qq{Adr. Chantier};
$mess[2058] = qq{Ville Chantier};
$mess[2059] = qq{T�l. Chantier};
$mess[2060] = qq{Contact Chantier};
$mess[2061] = qq{D�signation Machine};
$mess[2062] = qq{N� Parc};
$mess[2063] = qq{Date D�part};
$mess[2064] = qq{Date Retour};
$mess[2065] = qq{Jour Plus};
$mess[2066] = qq{Jour Moins};
$mess[2067] = qq{Dur�e};
$mess[2068] = qq{Prix jour};
$mess[2069] = qq{Prix Mensuel};
$mess[2070] = qq{Montant Forfaitaire};
$mess[2071] = qq{Transport Aller};
$mess[2072] = qq{Transport Retour};
$mess[2073] = qq{Transport Planifi�};
$mess[2074] = qq{Chauffeur ou Sous Traitant (r�cup.)};
$mess[2075] = qq{Assurance non comprise};
$mess[2076] = qq{ non};
$mess[2077] = qq{Assurance};
$mess[2078] = qq{Montant Total};
$mess[2079] = qq{Commentaire};
$mess[2080] = qq{Fermer};
$mess[2081] = qq{Chauffeur ou Sous Traitant (aller)};
$mess[2082] = qq{hors jours d�j� factur�s : };
$mess[2083] = qq{Contact Commande};
$mess[2084] = qq{T�l�phone Contact Commande};
$mess[2085] = qq{Nettoyage};
$mess[2086] = qq{Service};
# transportint.cfg
$mess[2100] = qq{Transport Interne};
$mess[2101] = qq{Commande Transport externe};
$mess[2102] = qq{Num�ro};
$mess[2103] = qq{N� Contrat};
$mess[2104] = qq{Client};
$mess[2105] = qq{Chantier};
$mess[2106] = qq{C.P.};
$mess[2107] = qq{Ville};
$mess[2108] = qq{Quai};
$mess[2109] = qq{Livraison anticip�e possible};
$mess[2110] = qq{Date D�part};
$mess[2111] = qq{Date Retour};
$mess[2112] = qq{Montant};
$mess[2113] = qq{Poids};
$mess[2114] = qq{D�signation Machine};
$mess[2115] = qq{N� Parc};
$mess[2116] = qq{�tat Contrat};
$mess[2117] = qq{Heure de Liv./R�cup.};
$mess[2118] = qq{Tp. planifi�};
$mess[2119] = qq{Chauffeur};
$mess[2120] = qq{Commande transporteur};
$mess[2130] = qq{Plage horaire};
$mess[2131] = qq{Pas de commentaire};
#contrat.cfg et contrat2.cfg
$mess[2150] = qq{Num�ro};
$mess[2151] = qq{N� Contrat};
$mess[2152] = qq{Client};
$mess[2153] = qq{Chantier};
$mess[2154] = qq{C.P.};
$mess[2155] = qq{Ville};
$mess[2156] = qq{Date D�part};
$mess[2157] = qq{Date Retour};
$mess[2158] = qq{D�signation Machine};
$mess[2159] = qq{N� Parc};
$mess[2160] = qq{�tat Contrat};
$mess[2161] = qq{�dition Contrat(s)};
$mess[2162] = qq{�dition Contrat(s) pour 1�re impression};
#comcont.cfg
$mess[2200] = qq{Num�ro};
$mess[2201] = qq{Dernier Contrat};
$mess[2202] = qq{Indice};
$mess[2203] = qq{Modif.};
$mess[2204] = qq{Code postal chantier};
$mess[2205] = qq{D�signation Machine};
$mess[2206] = qq{N� Parc};
$mess[2207] = qq{�tat};
$mess[2208] = qq{Date Fin Contrat};
$mess[2209] = qq{Date �ch�ance};
$mess[2210] = qq{Organisme};
$mess[2211] = qq{Commande de Visite P�riodique};
$mess[2212] = qq{Commande de Contr�le};
$mess[2250] = qq{Imp. Unit�};
$mess[2251] = qq{N� Comm};
$mess[2252] = qq{Date de Visite};

#nvclient.cfg
$mess[2300] = qq{Nouveaux clients};
$mess[2301] = qq{Num�ro};
$mess[2302] = qq{Agence};
$mess[2303] = qq{Soci�t�};
$mess[2304] = qq{Adresse};
$mess[2305] = qq{C.P.};
$mess[2306] = qq{Ville};
$mess[2307] = qq{SIRET};
$mess[2308] = qq{T�l.};
$mess[2309] = qq{Fax};
$mess[2310] = qq{Mode Paiement};
$mess[2311] = qq{RIB};
$mess[2312] = qq{est d�j� attribu� au client};
$mess[2313] = qq{Modes Paiements};
$mess[2320] = qq{Recherche par nom de client};
#nvclient.cfg
$mess[2350] = qq{APE};
$mess[2351] = qq{Secteur d'activit�};
$mess[2352] = qq{SIRET};
$mess[2353] = qq{Libell�};
$mess[2354] = qq{Adresse};
$mess[2355] = qq{Cmpt. adresse};
$mess[2356] = qq{Ville};
$mess[2357] = qq{T�l�phone};
$mess[2358] = qq{Portable};
$mess[2359] = qq{Fax};
$mess[2360] = qq{Contact};
$mess[2361] = qq{Commentaire};
$mess[2362] = qq{Mode Paiement};
$mess[2363] = qq{Banque};
$mess[2364] = qq{RIB};
$mess[2365] = qq{Grille de base};
$mess[2366] = qq{Groupe};
$mess[2367] = qq{Qualification};
$mess[2368] = qq{Email Client};
$mess[2370] = qq{Aucun};
$mess[2371] = qq{Fin de validit� (Ass.)};
$mess[2372] = qq{Potentiel};
$mess[2373] = qq{Cl.};
$mess[2374] = qq{Pot.};
$mess[2375] = qq{Aucun};
$mess[2380] = qq{Classe};

$mess[2390] = qq{Nouveau client};

$mess[2395] = qq{archive.dev\@acces-industrie.com};
$mess[2396] = qq{juanfrancisco.agredano\@accesplataformas.com};
$mess[2397] = qq{valerie.penicaud\@acces-industrie.com};
$mess[2398] = qq{hasnaa.iounes\@maroc-elevation.com};
$mess[2399] = qq{2};
#Devis.cfg
$mess[2400] = qq{Clients};
$mess[2401] = qq{Ville Chantier};
$mess[2402] = qq{Libell� Chantier};
$mess[2403] = qq{Adresse Chantier};
$mess[2404] = qq{Contact Chantier};
$mess[2405] = qq{T�l. Chantier};
$mess[2406] = qq{N� Commande};
$mess[2407] = qq{Type Machine};
$mess[2408] = qq{Hors parc};
$mess[2409] = qq{Quantit�};
$mess[2410] = qq{Prix Jour/Mois};
$mess[2411] = qq{Type Montant};
$mess[2412] = qq{Dur�e};
$mess[2413] = qq{Date Pr�visionnelle};
$mess[2414] = qq{Montant};
$mess[2415] = qq{Transport Aller};
$mess[2416] = qq{Transport Retour};
$mess[2417] = qq{Assurance};
$mess[2418] = qq{Montant assurance (estimation)};
$mess[2419] = qq{Montant Total};
$mess[2420] = qq{Commentaire};
$mess[2421] = qq{Notes Commerciales (ne figurent pas sur le devis)};
$mess[2422] = qq{Devis Accept�};
$mess[2423] = qq{Calculer};
$mess[2424] = qq{Valider};
$mess[2425] = qq{Stock};
$mess[2426] = qq{Devis};
$mess[2427] = qq{Aller};
$mess[2428] = qq{Retour};
$mess[2429] = qq{Tarif de base};
$mess[2430] = qq{Remise};
$mess[2431] = qq{Ajouter une machine};
$mess[2432] = qq{Client};
$mess[2433] = qq{Machine};
$mess[2434] = qq{Dur�e};
$mess[2435] = qq{Prix};
$mess[2436] = qq{Montant};
$mess[2437] = qq{Transport};
$mess[2438] = qq{Demander une remise exceptionnelle};
$mess[2439] = qq{Rem. excep.};
$mess[2440] = qq{Zone};
$mess[2441] = qq{Estimation de transport};
$mess[2442] = qq{Montant aller et/ou retour incorrect(s).};
$mess[2443] = qq{Le montant aller ou retour est incorrect.};
$mess[2444] = qq{(entre 1 et 6)};
$mess[2445] = qq{Full service (minimum 6 mois)};
#tournee.cfg
$mess[2450] = qq{N� Contrat};
$mess[2451] = qq{Chantier};
$mess[2452] = qq{Adresse};
$mess[2453] = qq{C.P.};
$mess[2454] = qq{Ville};
$mess[2455] = qq{Contact};
$mess[2456] = qq{D�signation};
$mess[2457] = qq{N� Parc};
$mess[2458] = qq{Poids};
$mess[2459] = qq{�tat};
$mess[2460] = qq{�tat contrat};
$mess[2461] = qq{Nom chauffeur};
$mess[2462] = qq{Heure de Liv./R�cup.};
$mess[2463] = qq{Ordre};
#retour.cfg
$mess[2500] = qq{Ordonner Tourn�e};
$mess[2501] = qq{�dition BL/BR/Tourn�e};
$mess[2502] = qq{Pointage LV/RC/Transfert/Tourn�e};

$mess[2505] = qq{N� Contrat};
$mess[2506] = qq{Chantier};
$mess[2507] = qq{Adresse};
$mess[2508] = qq{C.P.};
$mess[2509] = qq{Ville};
$mess[2510] = qq{Contact};
$mess[2511] = qq{D�signation};
$mess[2512] = qq{N� Parc};
$mess[2513] = qq{�tat};
$mess[2514] = qq{�tat contrat};
$mess[2515] = qq{Nom chauffeur};
$mess[2516] = qq{Ordre};
$mess[2517] = qq{Heure de Liv./R�cup.};
$mess[2518] = qq{Effectu�e};
$mess[2519] = qq{Chauffeur Interne};
$mess[2520] = qq{Sous Traitant};
#Type de montant
$mess[2550] = qq{Type Mt};
$mess[2551] = qq{Jour};
$mess[2552] = qq{Mensuel};
$mess[2553] = qq{Forfaitaire};
#libell�s devis
$mess[2600] = qq{Modifier l'ent�te};
$mess[2601] = qq{Modifier la ligne};
$mess[2602] = qq{Modifier le mod�le};
$mess[2603] = qq{Vous devez imp�rativement enregistrer la ligne avant toute autre chose!};
$mess[2604] = qq{Vous devez imp�rativement enregistrer le contrat avant toute autre chose!};
#modifiarret modif2.cfg
$mess[2650] = qq{Modification/Arr�t de contrat};
$mess[2651] = qq{Num�ro};
$mess[2652] = qq{�tat Contrat};
$mess[2653] = qq{N� Contrat};
$mess[2654] = qq{Indice};
$mess[2655] = qq{Client};
$mess[2656] = qq{Libell� Chantier};
$mess[2657] = qq{Ville Chantier};
$mess[2658] = qq{Contact chantier};
$mess[2659] = qq{T�l�phone chantier};
$mess[2660] = qq{Contact commande};
$mess[2661] = qq{T�l�phone contact commande};
$mess[2662] = qq{D�signation Machine};
$mess[2663] = qq{N� Parc};
$mess[2664] = qq{Hors parc};
$mess[2665] = qq{Date D�part};
$mess[2666] = qq{Date Retour};
$mess[2667] = qq{Dur�e};
$mess[2668] = qq{Jour en plus};
$mess[2669] = qq{Jour en moins};
$mess[2670] = qq{Prix Jour};
$mess[2671] = qq{Prix Mois};
$mess[2672] = qq{Forfait};
$mess[2673] = qq{Assurance Comprise};
$mess[2674] = qq{Montant Assurance};
$mess[2675] = qq{Transport Aller};
$mess[2676] = qq{Enl�vement Aller};
$mess[2677] = qq{Transport Retour};
$mess[2678] = qq{Enl�vement Retour};
$mess[2679] = qq{Quai};
$mess[2680] = qq{Montant Total HT};
$mess[2681] = qq{Commande};
$mess[2682] = qq{Commentaire};
$mess[2683] = qq{Note};
$mess[2684] = qq{Paiement � la livraison};
$mess[2685] = qq{Client};
$mess[2686] = qq{N� Parc};
$mess[2687] = qq{Ce contrat a d�j� �t� arr�t�};
$mess[2688] = qq{Le Type de machine est chang�};
$mess[2689] = qq{Le contrat est annul�};
$mess[2690] = qq{Accessoires li�s au contrat};
$mess[2691] = qq{Accessoire (N� Parc)};
$mess[2692] = qq{Contrat (�tat)};
$mess[2693] = qq{Montant};
$mess[2694] = qq{Date D�part};
$mess[2695] = qq{Date Retour};
$mess[2696] = qq{Machine lou�e sans l'accessoire par d�faut};
$mess[2697] = qq{Contrat d'origine};
#Stock
$mess[2700] = qq{D�signation};
$mess[2701] = qq{Mod�le};
$mess[2702] = qq{�l�vation};
$mess[2703] = qq{�nergie};
$mess[2704] = qq{Poids};
$mess[2705] = qq{Stock R�serv�};
$mess[2706] = qq{Stock R�servable};
$mess[2707] = qq{Stock Total};
$mess[2708] = qq{LOUE 0%};
$mess[2709] = qq{TRSF 0%};
$mess[2710] = qq{PANN 0%};
$mess[2711] = qq{CONT 90%};
$mess[2712] = qq{RECU 50%};
$mess[2713] = qq{VERF 80%};
$mess[2714] = qq{REVI 80%};
$mess[2715] = qq{DISP 100%};
#Saisie pr�-contrat
$mess[2750] = qq{Saisie pr�-contrat};
$mess[2751] = qq{Clients};
$mess[2752] = qq{Type machine};
$mess[2753] = qq{Hors parc};
$mess[2754] = qq{Ville chantier};
$mess[2755] = qq{Libell� chantier};
$mess[2756] = qq{Adresse chantier};
$mess[2757] = qq{Contact chantier};
$mess[2758] = qq{Tel. chantier};
$mess[2759] = qq{Contact commande};
$mess[2760] = qq{Tel. contact commande};
$mess[2761] = qq{N� Commande};
$mess[2762] = qq{Date D�part};
$mess[2763] = qq{Date Retour};
$mess[2764] = qq{Jour Plus};
$mess[2765] = qq{Jour Moins};
$mess[2766] = qq{Prix apr�s rem. excep.};
$mess[2767] = qq{Dur�e};
$mess[2768] = qq{Prix Mois};
$mess[2769] = qq{Montant Forfaitaire};
$mess[2770] = qq{Assurance};
$mess[2771] = qq{Aller};
$mess[2772] = qq{Enl�v. sur place};
$mess[2773] = qq{Retour};
$mess[2774] = qq{Enl�v. sur place};
$mess[2775] = qq{Quai};
$mess[2776] = qq{Commentaire};
$mess[2777] = qq{Mod�le Imp�ratif};
$mess[2778] = qq{Transfert Chantier � Chantier};
$mess[2779] = qq{Paiement avant livraison};
$mess[2780] = qq{Calculer};
$mess[2781] = qq{Attention, le mod�le s�lectionn� n'est pas r�serv� tant que le pr�-contrat n'est pas valid� !};
$mess[2782] = qq{La derni�re machine r�servable de ce mod�le vient d'�tre r�serv�e, s�lectionnez un autre mod�le !};
$mess[2783] = qq{Type montant};
$mess[2784] = qq{Tarif de base};
$mess[2785] = qq{Remise};
$mess[2786] = qq{Remise exceptionnelle};
$mess[2787] = qq{Nettoyage};
$mess[2788] = qq{Enlevement sur place};
$mess[2789] = qq{Machine supp.};
$mess[2790] = qq{Machine supp.};

#Etat pr�-contrat
$mess[2800] = qq{Agence};
$mess[2801] = qq{Responsable};
$mess[2802] = qq{Adresse};
$mess[2803] = qq{T�l.};
$mess[2804] = qq{Fax};
$mess[2805] = qq{C.P.};
$mess[2806] = qq{Ville};
$mess[2807] = qq{Client};
$mess[2808] = qq{Adresse};
$mess[2809] = qq{C.P.};
$mess[2810] = qq{Ville};
$mess[2811] = qq{T�l.};
$mess[2812] = qq{Fax};
$mess[2813] = qq{Chantier};
$mess[2814] = qq{Adresse};
$mess[2815] = qq{C.P.};
$mess[2816] = qq{Ville};
$mess[2817] = qq{Responsable};
$mess[2818] = qq{Marque};
$mess[2819] = qq{Famille};
$mess[2820] = qq{�nergie};
$mess[2821] = qq{D�signation};
$mess[2822] = qq{Poids};
$mess[2823] = qq{Location Journali�re};
$mess[2824] = qq{Assurance};
$mess[2825] = qq{Transport Aller};
$mess[2826] = qq{Transport Retour};
$mess[2827] = qq{Date d�part};
$mess[2828] = qq{Date retour};
$mess[2829] = qq{Jour en plus};
$mess[2830] = qq{Jour en moins};
$mess[2831] = qq{Dur�e};
$mess[2832] = qq{Agent loueur};
$mess[2833] = qq{Indice};
$mess[2834] = qq{Assurance comprise};
#conscont.cfg
$mess[2850] = qq{Consultation Contrat};
$mess[2851] = qq{Num�ro};
$mess[2852] = qq{N� Contrat};
$mess[2853] = qq{Client};
$mess[2854] = qq{Libell� Chantier};
$mess[2855] = qq{C.P. Chantier};
$mess[2856] = qq{Ville Chantier};
$mess[2857] = qq{Contact Chantier};
$mess[2858] = qq{Contact Commande};
$mess[2859] = qq{D�signation Machine};
$mess[2860] = qq{N� Parc};
$mess[2861] = qq{Date D�part};
$mess[2862] = qq{Date Retour};
$mess[2863] = qq{Dur�e};
$mess[2864] = qq{Prix Jour};
$mess[2865] = qq{Prix Mois};
$mess[2866] = qq{Montant Assurance};
$mess[2867] = qq{Montant Gestion de recours};
$mess[2868] = qq{Transport Aller};
$mess[2869] = qq{Transport Retour};
$mess[2870] = qq{Quantit� Carbu (L)};
$mess[2871] = qq{Nettoyage};
$mess[2872] = qq{Service};
$mess[2873] = qq{Montant Total HT};
$mess[2874] = qq{�tat contrat};
$mess[2875] = qq{N� Commande};
$mess[2876] = qq{Note};

$mess[2882] = qq{N� Contrat};
$mess[2883] = qq{Client};
$mess[2884] = qq{N� Parc};
$mess[2885] = qq{Chantier};
$mess[2886] = qq{Libell� Chantier};
$mess[2887] = qq{D�partement Chantier};
$mess[2888] = qq{Ann�e};
$mess[2889] = qq{�tat contrat};
$mess[2890] = qq{soit};
$mess[2891] = qq{jour(s)};
$mess[2892] = qq{/&nbsp;jour};
$mess[2893] = qq{/&nbsp;mois};
$mess[2894] = qq{au forfait};
$mess[2895] = qq{Tarif ind�terminable !};
$mess[2896] = qq{Seuls les jours en plus peuvent �tre modifi�s};
#machine.cfg
$mess[2900] = qq{Liste machine / agence de };
$mess[2901] = qq{D�signation};
$mess[2902] = qq{N� Parc};
$mess[2903] = qq{Hors parc};
$mess[2904] = qq{N� S�rie};
$mess[2905] = qq{Date d'achat};
$mess[2906] = qq{Date Contr�le};
$mess[2907] = qq{Mod�le};
$mess[2908] = qq{Marque};
$mess[2909] = qq{�l�vation};
$mess[2910] = qq{�nergie};
$mess[2911] = qq{Poids};
$mess[2912] = qq{Longueur};
$mess[2913] = qq{Largeur};
$mess[2914] = qq{Capacit�};
$mess[2915] = qq{�tat};
$mess[2916] = qq{Probabilit� de location en %};
$mess[2917] = qq{Agence};
$mess[2918] = qq{Dernier contrat auquel cette machine a �t� attribu�e};
$mess[2919] = qq{N� Parc};
$mess[2920] = qq{N� S�rie};
$mess[2921] = qq{Mod�le Machine};
$mess[2922] = qq{Famille machine};
$mess[2923] = qq{Fournisseur};
$mess[2924] = qq{�l�vation ou Pression};
$mess[2925] = qq{�nergie};
$mess[2926] = qq{�tat machine};
$mess[2927] = qq{Parc};
$mess[2928] = qq{Agence};
$mess[2929] = qq{Pays};
$mess[2930] = qq{Dernier contrat portant sur cette machine};
$mess[2931] = qq{Historique des contrats};
$mess[2932] = qq{Voir l'historique};
$mess[2933] = qq{%d derniers :};
$mess[2934] = qq{Aucun contrat au Maroc};
$mess[2935] = qq{Liste machine globale};
$mess[2936] = qq{R�gion};

#chantier.cfg
$mess[2950] = qq{Modification des Informations du Chantier};
$mess[2951] = qq{Num�ro};
$mess[2952] = qq{N� Contrat};
$mess[2953] = qq{Client};
$mess[2954] = qq{Libell� Chantier};
$mess[2955] = qq{Adresse Chantier};
$mess[2956] = qq{Ville};
$mess[2957] = qq{Contact};
$mess[2958] = qq{T�l�phone};
$mess[2959] = qq{Hauteur};
$mess[2960] = qq{Surface};
$mess[2961] = qq{Heure de Liv.};
$mess[2962] = qq{Heure de R�cup.};
$mess[2963] = qq{Commentaire};
$mess[2964] = qq{N� Contrat};
$mess[2965] = qq{Client};
$mess[2966] = qq{Libell� Chantier};
$mess[2967] = qq{Ville Chantier};
#infoclient.cfg
$mess[3000] = qq{Modification des Informations du Client};
$mess[3001] = qq{Num�ro};
$mess[3002] = qq{Code Client};
$mess[3003] = qq{APE};
$mess[3004] = qq{Siret};
$mess[3005] = qq{Libell�};
$mess[3006] = qq{Adresse};
$mess[3007] = qq{Adresse comp.};
$mess[3008] = qq{Ville};
$mess[3009] = qq{T�l.};
$mess[3010] = qq{Fax};
$mess[3011] = qq{Portable};
$mess[3012] = qq{Email};
$mess[3013] = qq{Contact};
$mess[3014] = qq{Collectif};
$mess[3015] = qq{LI_NO};
$mess[3016] = qq{Banque};
$mess[3017] = qq{Type Paiement};
$mess[3018] = qq{RIB};
$mess[3019] = qq{Nb. Facture(s)};
$mess[3020] = qq{Facturation Locale};
$mess[3021] = qq{Facture non group�e};
$mess[3022] = qq{Commentaire};
$mess[3023] = qq{Activit�};
$mess[3024] = qq{Grille de base};
$mess[3025] = qq{Sommeil/Bloqu�};
$mess[3026] = qq{Assurance};
$mess[3027] = qq{N� commande obligatoire};
$mess[3028] = qq{BC � fournir};
$mess[3029] = qq{Paiement avant livraison};
$mess[3030] = qq{Conditions g�n�rales de vente};
$mess[3031] = qq{Facturation du nettoyage};
$mess[3032] = qq{Tarif personnalis�};
$mess[3033] = qq{Oui};
$mess[3034] = qq{Non};
$mess[3035] = qq{Date d'application de l'assurance souhait�e.};
$mess[3036] = qq{Le SIRET n'est pas renseign�};
$mess[3037] = qq{Format de SIRET non valide};
$mess[3038] = qq{Le SIRET est d�j� utilis�};

$mess[3040] = qq{Code Client};
$mess[3041] = qq{Libell� Client};
$mess[3042] = qq{Ville};
$mess[3043] = qq{D�partement};
$mess[3044] = qq{Siret};
$mess[3045] = qq{Grille de base};
$mess[3046] = qq{Bons de livr./r�cup. obligatoires};

$mess[3047] = qq{Facturation de la participation au recyclage};
$mess[3048] = qq{Qualification};
$mess[3060] = qq{Participation au recyclage};

$mess[3049] = qq{Le champ "Facturation Locale" a �t� modifi�. Veuillez contacter le 5000 pour que les actions n�cessaires soient effectu�es};

#anncl.cfg
$mess[3050] = qq{Suppression de Client};
$mess[3051] = qq{Client erron�};
$mess[3052] = qq{Client correct};
$mess[3053] = qq{Calculer};
# Assurance souhait�e
$mess[3054] = qq{Assurance souhait�e};
# Assurance actuelle
$mess[3055] = qq{Assurance actuelle};
# Gestion de recours actuelle
$mess[3056] = qq{Gestion de recours};
# Gestion de recours souhait�e
$mess[3057] = qq{Gestion de recours souhait�e};
$mess[3058] = qq{Date d'application souhait�e.};
$mess[3059] = qq{Non facturable};

#chclient.cfg
$mess[3100] = qq{Changement Client/Contrat};
$mess[3101] = qq{Mauvais Client sur Contrat};
$mess[3102] = qq{Bon Client};
$mess[3103] = qq{Valider};
$mess[3104] = qq{Attention, sur les deux clients, les informations suivantes sont diff�rentes (elles doivent �tre mises � jour <span style="text-decoration:underline;">manuellement</span>) :};
$mess[3105] = qq{ATTENTION, � la validation les tarifs de location et transport sont <span style="text-decoration:underline;">automatiquement maintenus suivant les conditions du premier client</span>};
$mess[3106] = qq{L�assurance du contrat n�est pas compatible avec l�assurance du nouveau client :};
$mess[3107] = qq{Un probl�me est survenu lors du changement de client sur le contrat.};

#chmach.cfg
$mess[3150] = qq{Changement Machine/Contrat};
$mess[3151] = qq{Contrat/N� Parc};
$mess[3152] = qq{Bonne Machine (Disponible)};
$mess[3153] = qq{Valider};
$mess[3154] = qq{Vous avez d�j� valid�};
$mess[3155] = qq{La machine est chang�e};
$mess[3156] = qq{Le Type de machine est chang�};
$mess[3157] = qq{Le contrat est annul�};
$mess[3158] = qq{Le Devis est saisie};
$mess[3159] = qq{Impossible de faire le changement, la tourn�e de livraison est point�e effectu�e};
$mess[3160] = qq{Impossible de faire le changement, le typage du mod�le de machines du contrat est diff�rent de celui de la machine saisie};
#contcourt.cfg
$mess[3200] = qq{Remise Contrat en Cours};
$mess[3201] = qq{Contrat Arr�t�};
$mess[3202] = qq{Valider};
$mess[3203] = qq{N'oubliez pas d'enregistrer la note avant d'annuler un contrat.};
#saisiemachine.cfg
$mess[3250] = qq{Saisie Machine};
$mess[3251] = qq{Recherche du Mod�le};
$mess[3252] = qq{N� de Parc};
$mess[3253] = qq{N� de S�rie};
$mess[3254] = qq{Date Apave};
$mess[3255] = qq{Calculer};
#sousloc.cfg
$mess[3300] = qq{Sous Location};
$mess[3301] = qq{Clients};
$mess[3302] = qq{Ville Chantier};
$mess[3303] = qq{Libell� Chantier};
$mess[3304] = qq{Adresse Chantier};
$mess[3305] = qq{Contact Chantier};
$mess[3306] = qq{T�l. Chantier};
$mess[3307] = qq{Contact Commande};
$mess[3308] = qq{T�l. Contact Commande};
$mess[3309] = qq{Fournisseur};
$mess[3310] = qq{Mod�le machine};
$mess[3311] = qq{Date D�part};
$mess[3312] = qq{Date Retour};
$mess[3313] = qq{Prix Jour};
$mess[3314] = qq{Dur�e Mensuelle};
$mess[3315] = qq{Prix Mois};
$mess[3316] = qq{Montant Forfaitaire};
$mess[3317] = qq{Assurance Comprise};
$mess[3318] = qq{Transport Aller};
$mess[3319] = qq{Enl�vement sur place Aller};
$mess[3320] = qq{Transport Retour};
$mess[3321] = qq{Enl�vement sur place Retour};
$mess[3322] = qq{Quai};
$mess[3323] = qq{Calculer};
$mess[3324] = qq{Machine suppl�mentaire Aller};
$mess[3325] = qq{Machine suppl�mentaire Retour};
#tableau.cfg
$mess[3350] = qq{Tableau};
$mess[3351] = qq{Machine(s) lou�e(s)};
$mess[3352] = qq{Machine(s) en livraison};
$mess[3353] = qq{Machine(s) en r�cup�ration};
$mess[3354] = qq{Machine(s) en };
$mess[3355] = qq{Mod�le};
$mess[3356] = qq{Famille};
$mess[3357] = qq{Hauteur};
$mess[3358] = qq{�nergie};
$mess[3359] = qq{N� Parc};
$mess[3360] = qq{N� Contrat};
$mess[3361] = qq{Client};
$mess[3362] = qq{Chantier};
$mess[3363] = qq{C.P.};
$mess[3364] = qq{Ville};
$mess[3365] = qq{Date de fin};
$mess[3366] = qq{POT};
$mess[3367] = qq{Tarif};
$mess[3368] = qq{Co�t};
$mess[3369] = qq{Ratio};
$mess[3370] = qq{Dur�e};
$mess[3380] = qq{Enl�vement};
$mess[3381] = qq{R�cup�ration};
$mess[3382] = qq{Transfert inter-chantiers};
$mess[3383] = qq{Reprise sur chantier};


$mess[3410] = qq{Num�ro};
$mess[3411] = qq{Contrat};
$mess[3412] = qq{Date d�but};
$mess[3413] = qq{Date fin};
$mess[3414] = qq{N� de parc};
$mess[3415] = qq{Mod�le};
$mess[3416] = qq{Chantier};
$mess[3417] = qq{Code postal};
$mess[3418] = qq{Ville};
#nomade.cfg
$mess[3450] = qq{Changement Agence};
$mess[3451] = qq{Agence destinataire };
$mess[3452] = qq{Vous &ecirc;tes &aacute;};
$mess[3453] = qq{Se t�l�porter !};
#statms.cfg
$mess[3500] = qq{Chiffres du mois};
#stat1.cfg
$mess[3550] = qq{Chiffres du mois 2};
#stat2.cfg
$mess[3600] = qq{Chiffres globaux};
#hisstat.cfg
$mess[3650] = qq{Historique des Chiffres};
$mess[3651] = qq{Mois};
$mess[3652] = qq{Ann�e};
$mess[3653] = qq{Janvier};
$mess[3654] = qq{F�vrier};
$mess[3655] = qq{Mars};
$mess[3656] = qq{Avril};
$mess[3657] = qq{Mai};
$mess[3658] = qq{Juin};
$mess[3659] = qq{Juillet};
$mess[3660] = qq{Ao�t};
$mess[3661] = qq{Septembre};
$mess[3662] = qq{Octobre};
$mess[3663] = qq{Novembre};
$mess[3664] = qq{D�cembre};
$mess[3665] = qq{5};
$mess[3666] = qq{2000};
$mess[3667] = qq{2001};
$mess[3668] = qq{2002};
$mess[3669] = qq{2003};
$mess[3670] = qq{2004};
#validagag.cfg
$mess[3700] = qq{Validation Agence � Agence};
$mess[3701] = qq{Machine};
$mess[3702] = qq{Machine(s)};
#transfagag.cfg
$mess[3750] = qq{Transfert Agence � Agence};
$mess[3751] = qq{Machine};
$mess[3752] = qq{Camion};
$mess[3753] = qq{Chauffeur};
$mess[3754] = qq{Agence destinataire};
$mess[3755] = qq{Machine(s)};
$mess[3756] = qq{Camion(s)};
$mess[3757] = qq{Chauffeur(s)};
$mess[3758] = qq{Agences};
#camcha.cfg
$mess[3800] = qq{Affectation Camion/Chauffeur};
$mess[3801] = qq{Camion};
$mess[3802] = qq{Chauffeur};
$mess[3803] = qq{Couple Camion/Chauffeur};
$mess[3804] = qq{Camion(s)};
$mess[3805] = qq{Chauffeur(s)};
$mess[3806] = qq{Couple(s)};
#transfert.cfg
$mess[3850] = qq{Transfert de chantier � chantier};
$mess[3851] = qq{Transport};
$mess[3852] = qq{Transport};
$mess[3853] = qq{N� Parc};
$mess[3854] = qq{Num�ro};
$mess[3855] = qq{N� Contrat};
$mess[3856] = qq{Client};
$mess[3857] = qq{Adresse};
$mess[3858] = qq{C.P.};
$mess[3859] = qq{Ville};
$mess[3860] = qq{D�signation Machine};
$mess[3861] = qq{N� Parc};
#carbu.cfg
$mess[3900] = qq{Carburant};
$mess[3901] = qq{Contrat, Client, D�signation, N� Parc};
$mess[3902] = qq{Quantit� Carburant (L)};
#verif.cfg
$mess[3950] = qq{V�rification Machine};
$mess[3951] = qq{Machine};
#Contr�le Apave
$mess[4000] = qq{Contr�le Machine};
$mess[4001] = qq{N� Parc};
$mess[4002] = qq{Mod�le Machine};
$mess[4003] = qq{Famille machine};
$mess[4004] = qq{�l�vation ou Pression};
$mess[4005] = qq{�nergie};
$mess[4006] = qq{�tat machine};
$mess[4007] = qq{D�signation};
$mess[4008] = qq{D�signation};
$mess[4009] = qq{N� Parc};
$mess[4010] = qq{N� S�rie};
$mess[4011] = qq{Mod�le};
$mess[4012] = qq{Marque};
$mess[4013] = qq{�l�vation};
$mess[4014] = qq{�nergie};
$mess[4015] = qq{Poids};
$mess[4016] = qq{Longueur};
$mess[4017] = qq{Largeur};
$mess[4018] = qq{Capacit�};
$mess[4019] = qq{�tat};
$mess[4020] = qq{Date Appave};
$mess[4021] = qq{Effectu�};
$mess[4022] = qq{Num};
#Consultation Devis
$mess[4050] = qq{Consultation Devis};
$mess[4051] = qq{N� Devis};
$mess[4052] = qq{Client};
$mess[4053] = qq{�tat Devis};
$mess[4054] = qq{Agent};
$mess[4055] = qq{N� Devis};
$mess[4056] = qq{Client};
$mess[4057] = qq{Lib. Chantier};
$mess[4058] = qq{Date};
$mess[4059] = qq{D�signation};
$mess[4060] = qq{Prix Jour};
$mess[4061] = qq{Dur�e};
$mess[4062] = qq{C.T};
$mess[4063] = qq{Montant};
$mess[4064] = qq{Pass� en contrat};
$mess[4065] = qq{�tat};
$mess[4066] = qq{Agent};
#Remise en cours de contrat
$mess[4100] = qq{Remise Contrat en Cours};
$mess[4101] = qq{Contrat Arr�t�};
$mess[4102] = qq{Valider};
#Cr�ation de machine
$mess[4150] = qq{Saisie Machine};
$mess[4151] = qq{Recherche du Mod�le};
$mess[4152] = qq{N� Parc};
$mess[4153] = qq{N� S�rie};
$mess[4154] = qq{Date Apave};
$mess[4155] = qq{Agence de cr�ation};
$mess[4156] = qq{Calculer};
#Facturation
$mess[4200] = qq{Facturation};
$mess[4201] = qq{N� Contrat};
$mess[4202] = qq{Date de Fin};
$mess[4203] = qq{Agence};
$mess[4204] = qq{Contact};
$mess[4205] = qq{T�l. Agence};
$mess[4206] = qq{Agence};
$mess[4207] = qq{� arr�ter};
$mess[4208] = qq{Carburant � saisir};
$mess[4209] = qq{� passer en facturation};
$mess[4210] = qq{T�l. Agence};
$mess[4211] = qq{R�capitulatif};
$mess[4212] = qq{Facturation hebdomadaire};
$mess[4213] = qq{Facturation mensuelle};
$mess[4214] = qq{Date de d�but};
$mess[4215] = qq{Date de cr�ation};
$mess[4216] = qq{Contrats anticip�s};
#Energies
$mess[4250] = qq{Bi-�nergie};
$mess[4251] = qq{Diesel};
$mess[4252] = qq{�lectrique};
$mess[4253] = qq{Manivelle};
$mess[4254] = qq{Gaz};
#Familles
$mess[4300] = qq{Boom};
$mess[4301] = qq{Chariot};
$mess[4302] = qq{Ciseau};
$mess[4303] = qq{Fl�che};
$mess[4304] = qq{Fl�che Chenille};
$mess[4305] = qq{Monte-charge};
$mess[4306] = qq{Mat droit};
$mess[4307] = qq{Push};
$mess[4308] = qq{Compacteur};
$mess[4309] = qq{Chariot Rotatif};
$mess[4310] = qq{Divers};
#Etat
$mess[4350] = qq{Contr�le};
$mess[4351] = qq{Disponible};
$mess[4352] = qq{Lou�e};
$mess[4353] = qq{Panne};
$mess[4354] = qq{R�cuperation};
$mess[4355] = qq{R�vision};
$mess[4356] = qq{V�rification};

$mess[4357] = qq{Vendue};
$mess[4358] = qq{En Transfert};
$mess[4359] = qq{Rendue};
$mess[4360] = qq{Restitu�};

$mess[4361] = qq{A r�cup�rer};

#Ann�es
$mess[4400] = qq{2001};
$mess[4401] = qq{2000};
$mess[4402] = qq{1999};
$mess[4403] = qq{1998};
#Info client
$mess[4450] = qq{Informations Client};
$mess[4451] = qq{Nom Client};
$mess[4452] = qq{Adresse};
$mess[4453] = qq{SIRET};
$mess[4454] = qq{APE};
$mess[4455] = qq{Secteur d'activit�};
$mess[4456] = qq{Contact client};
$mess[4457] = qq{T�l�phone};
$mess[4458] = qq{Fax};
$mess[4459] = qq{Portable};
$mess[4460] = qq{Adresse mail};
$mess[4461] = qq{Commentaires};
$mess[4462] = qq{Statistiques};
$mess[4463] = qq{Agence(s)};
$mess[4476] = qq{Total};
$mess[4483] = qq{Fermer};
# stati.cgi
#entete
$mess[5000] = qq{Nbr. contrats};
$mess[5001] = qq{Tx util.<br>J+1};
$mess[5002] = qq{TOT};
$mess[5003] = qq{ARR};
$mess[5004] = qq{ACT};
$mess[5005] = qq{ASS<br>FACT.};
$mess[5006] = qq{CT<5j};
$mess[5007] = qq{MENS};
$mess[5008] = qq{FORF};
$mess[5009] = qq{Arr�te au J=};
$mess[5010] = qq{Arr�te � la fin du mois};
$mess[5011] = qq{CA};
$mess[5012] = qq{TRANSP};
$mess[5013] = qq{CARB};
$mess[5014] = qq{COUT};
$mess[5015] = qq{CT Jr};
#titre groupe
$mess[5016] = qq{Parc};
$mess[5017] = qq{JR Loc};
$mess[5018] = qq{% Util};
$mess[5019] = qq{Px Jr};
$mess[5020] = qq{COUT};
$mess[5021] = qq{CT Jr};
#tittre bloc
$mess[5022] = qq{Fam. Comptable};
$mess[5023] = qq{Fam. Commerciale};
#date
$mess[5024] = qq{MOIS EN COURS};
$mess[5025] = qq{LOC};
$mess[5026] = qq{j. o.};
# Autres
$mess[5027] = qq{Livr.};
# BC � envoyer/r�ceptionner
$mess[5050] = qq{BC client � fournir};
$mess[5051] = qq{N� de Contrat};
$mess[5052] = qq{Ville Chantier};
$mess[5053] = qq{Client};
$mess[5054] = qq{N� T�l};
$mess[5055] = qq{Mod�le Machine};
$mess[5056] = qq{N� de Parc};
$mess[5057] = qq{Date de D�but};
$mess[5058] = qq{Date de Fin};
$mess[5059] = qq{Valid�};
$mess[5060] = qq{En attente};
# Paiement � la livraison
$mess[5600] = qq{Paiement � la livraison};
$mess[5601] = qq{N� de Contrat};
$mess[5602] = qq{Ville Chantier};
$mess[5603] = qq{Client};
$mess[5604] = qq{N� T�l};
$mess[5605] = qq{Mod�le Machine};
$mess[5606] = qq{N� de Parc};
$mess[5607] = qq{Date de D�but};
$mess[5608] = qq{Date de Fin};
$mess[5609] = qq{Montant acompte};
$mess[5610] = qq{N� acompte};
$mess[5611] = qq{Date versement};
$mess[5612] = qq{Type};
# Paiement � la livraison
$mess[5650] = qq{Paiement � la livraison};
$mess[5651] = qq{N� de Contrat};
$mess[5652] = qq{Client};
$mess[5653] = qq{Code Client};
$mess[5654] = qq{N� T�l};
$mess[5655] = qq{Date de D�but};
$mess[5656] = qq{Date de Fin};
$mess[5657] = qq{Montant acompte};
$mess[5658] = qq{N� acompte};
$mess[5659] = qq{Date versement};
$mess[5660] = qq{Type de paiement};
$mess[5661] = qq{Valid�};
# Devis (nouvelle formule)
# En-t�te
$mess[5800] = qq{Devis};
$mess[5801] = qq{Stock};
$mess[5802] = qq{Clients};
$mess[5803] = qq{Ville Chantier};
$mess[5804] = qq{Libell� Chantier};
$mess[5805] = qq{Adresse Chantier};
$mess[5806] = qq{Contact Chantier};
$mess[5807] = qq{T�l. Chantier};
$mess[5808] = qq{N� Commande};
# Colonnes
$mess[5820] = qq{Prix objectif};
$mess[5821] = qq{Co�t/jour};
$mess[5822] = qq{Ratio};
$mess[5823] = qq{% util};
$mess[5824] = qq{Quantit�};
$mess[5825] = qq{Type Montant};
$mess[5826] = qq{Dur�e};
$mess[5827] = qq{Date Pr�visionnelle};
$mess[5828] = qq{Prix Jour/Mois/Forfait};
$mess[5829] = qq{Ratio};
$mess[5830] = qq{Montant};
$mess[5831] = qq{Transport Aller};
$mess[5832] = qq{Transport Retour};
# Totaux
$mess[5850] = qq{Assurance};
$mess[5851] = qq{Montant Assurance};
$mess[5852] = qq{Montant Total};
# Pied
$mess[5860] = qq{Commentaire};
$mess[5861] = qq{Notes Commerciales (ne figurent pas sur le devis)};
$mess[5862] = qq{Devis Accept�};
$mess[5863] = qq{Calculer};
$mess[5864] = qq{Valider};
# Infos ratio Pr�-contrat
$mess[5900] = qq{Taux d'utilisation};
$mess[5901] = qq{Dur�e};
$mess[5902] = qq{Co�t/jour};
$mess[5903] = qq{Ratio objectif};
$mess[5904] = qq{Prix objectif/jour};
$mess[5905] = qq{Prix objectif/mois};
$mess[5906] = qq{Ratio};
# L�gende clients
$mess[6000] = qq{red};
$mess[6001] = '#'.qq{00CC33};
#$mess[6001] = qq{green};
$mess[6010] = qq{Client en rouge};
$mess[6011] = qq{Client en vert};
$mess[6020] = qq{Client n'ayant pas fait de chiffre d'affaires depuis plus de 14 mois};
$mess[6021] = qq{Client bloqu� en cours};
$mess[6022] = qq{Client � d�bloquer pour prolonger le contrat};
# Machine hors parc
$mess[6050] = qq{Parc normal};
$mess[6051] = qq{Hors parc};
#modifiarret modif2.cfg
$mess[6100] = qq{Modification/Arr�t de contrat};
$mess[6101] = qq{Num�ro};
$mess[6102] = qq{�tat Contrat};
$mess[6103] = qq{N� Contrat};
$mess[6104] = qq{Indice};
$mess[6105] = qq{Client};
$mess[6106] = qq{Libell� Chantier};
$mess[6107] = qq{Ville Chantier};
$mess[6108] = qq{Contact chantier};
$mess[6109] = qq{T�l�phone chantier};
$mess[6110] = qq{Contact commande};
$mess[6111] = qq{T�l�phone contact commande};
$mess[6112] = qq{D�signation Machine};
$mess[6113] = qq{N� Parc};
$mess[6114] = qq{Hors parc};
$mess[6115] = qq{Date D�part};
$mess[6116] = qq{Date Retour};
$mess[6117] = qq{Dur�e};
$mess[6118] = qq{Jour en plus};
$mess[6119] = qq{Jour en moins};
$mess[6120] = qq{Prix Jour};
$mess[6121] = qq{Prix Mois};
$mess[6122] = qq{Forfait};
$mess[6123] = qq{Assurance Comprise};
$mess[6124] = qq{Montant Assurance};
$mess[6125] = qq{Transport Aller};
$mess[6126] = qq{Enl�vement Aller};
$mess[6127] = qq{Transport Retour};
$mess[6128] = qq{Enl�vement Retour};
$mess[6129] = qq{Quai};
$mess[6130] = qq{Nettoyage};
$mess[6131] = qq{Libell� du service};
$mess[6132] = qq{Montant service};
$mess[6133] = qq{Montant Total HT};
$mess[6134] = qq{Commande};
$mess[6135] = qq{Commentaire};
$mess[6136] = qq{Note};
$mess[6137] = qq{Paiement � la livraison};
$mess[6138] = qq{Client};
$mess[6139] = qq{N� Parc};
$mess[6140] = qq{Ce contrat a d�j� �t� arr�t�};
$mess[6141] = qq{Le Type de machine est chang�};
$mess[6142] = qq{Le contrat est annul�};
$mess[6143] = qq{Accessoires li�s au contrat};
$mess[6144] = qq{Accessoire (N� Parc)};
$mess[6145] = qq{Contrat (�tat)};
$mess[6146] = qq{Montant};
$mess[6147] = qq{Date D�part};
$mess[6148] = qq{Date Retour};
$mess[6149] = qq{Machine lou�e sans l'accessoire par d�faut};
$mess[6150] = qq{Contrat d'origine};
# Contrat imprim�
$mess[6200] = qq{Location jour HT};
$mess[6201] = qq{Location mensuelle HT};
$mess[6202] = qq{Location forfaitaire HT};
# Assurance recherche rapide
$mess[6300] = qq{Factur�e};
$mess[6301] = qq{Non factur�e};
$mess[6302] = qq{Par le client};
$mess[6303] = qq{Incluse};
# BL-BR
$mess[6400] = qq{Aucune machine n'est attribu�e sur le contrat n� numcont.<BR>Vous devez l'attribuer pour finaliser la tourn�e.};
$mess[6401] = qq{Aucune machine n'est attribu�e sur les contrats n� numcont.<BR>Vous devez les attribuer pour finaliser la tourn�e.};
# Modification/Arr�t nouvelle formule
$mess[6500] = qq{Modification de contrat};
# Version tableau
# Colonnes
$mess[6510] = qq{N� contrat};
$mess[6511] = qq{Etat};
$mess[6512] = qq{Client};
$mess[6513] = qq{Chantier};
$mess[6514] = qq{Contact commande};
$mess[6515] = qq{Contact chantier};
$mess[6516] = qq{Machine};
$mess[6517] = qq{Dur�e};
$mess[6518] = qq{Jours en plus};
$mess[6519] = qq{Jours en moins};
$mess[6520] = qq{Prix};
$mess[6521] = qq{Assurance};
$mess[6522] = qq{Transport};
$mess[6523] = qq{Enl�vement};
$mess[6524] = qq{Quai};
$mess[6525] = qq{Montant HT};
$mess[6526] = qq{N� commande};
$mess[6527] = qq{Commentaire};
$mess[6528] = qq{Note};
# Autres
$mess[6550] = qq{hors parc};
$mess[6551] = qq{Aller};
$mess[6552] = qq{Retour};
$mess[6553] = qq{/ jour};
$mess[6554] = qq{/ mois};
$mess[6555] = qq{forfait};
$mess[6556] = qq{jour};
$mess[6557] = qq{jours};
# Affichage contrat
$mess[6600] = qq{Client};
$mess[6601] = qq{Nom};
$mess[6602] = qq{Grille de base};
$mess[6603] = qq{Contact commande};
$mess[6604] = qq{Chantier};
$mess[6605] = qq{Libell�};
$mess[6606] = qq{Ville};
$mess[6607] = qq{Contact chantier};
$mess[6608] = qq{Machine};
$mess[6609] = qq{D�signation};
$mess[6610] = qq{Energie};
$mess[6611] = qq{N� de parc};
$mess[6612] = qq{Hors parc};
$mess[6613] = qq{Taux d'utilisation};
$mess[6614] = qq{Co�t};
$mess[6615] = qq{Ratio objectif};
$mess[6616] = qq{Prix objectif};
$mess[6617] = qq{Ratio};
$mess[6618] = qq{Dur�e};
$mess[6619] = qq{Date d�part};
$mess[6620] = qq{Date retour};
$mess[6621] = qq{soit};
$mess[6622] = qq{jour};
$mess[6623] = qq{jours};
$mess[6624] = qq{ouvrable};
$mess[6625] = qq{ouvrables};
$mess[6626] = qq{calendaire};
$mess[6627] = qq{calendaires};
$mess[6628] = qq{Jours en plus};
$mess[6629] = qq{Jours en moins};
$mess[6630] = qq{Tarif de base};
$mess[6631] = qq{Prix};
$mess[6632] = qq{par jour};
$mess[6633] = qq{par mois};
$mess[6634] = qq{forfait};
$mess[6635] = qq{Assurance};
$mess[6636] = qq{Transport};
$mess[6637] = qq{Aller};
$mess[6638] = qq{Retour};
$mess[6639] = qq{Enl�vement};
$mess[6640] = qq{Quai};
$mess[6641] = qq{Services compl�mentaires};
$mess[6642] = qq{Ajouter un service};
$mess[6643] = qq{Supprimer un service};
$mess[6644] = qq{Montant total HT};
$mess[6645] = qq{Informations compl�mentaires};
$mess[6646] = qq{N� de commande};
$mess[6647] = qq{Commentaire};
$mess[6648] = qq{Note};
$mess[6649] = qq{Recalculer};
$mess[6650] = qq{Modifier le contrat};
$mess[6651] = qq{Arr�ter le contrat};
$mess[6652] = qq{Passer le contrat en facturation};
$mess[6653] = qq{Contrat d'origine};
$mess[6654] = qq{Accessoires};
$mess[6655] = qq{du};
$mess[6656] = qq{au};
$mess[6657] = qq{/ jour};
$mess[6658] = qq{Supprim�};
$mess[6659] = qq{Factur�};
$mess[6660] = qq{Tarif};
$mess[6661] = qq{Divers};
$mess[6662] = qq{Factur� en fin de contrat};
$mess[6663] = qq{Veuillez choisir un service dans la liste d�roulante};
$mess[6664] = qq{Enti�rement factur�};
$mess[6665] = qq{Factur� partiellement jusqu'au};
$mess[6666] = qq{Non factur�};
$mess[6667] = qq{Visualiser le tarif};
$mess[6668] = qq{Carburant};
$mess[6669] = qq{Quantit�};
$mess[6670] = qq{litre};
$mess[6671] = qq{litres};
$mess[6672] = qq{Montant};
$mess[6673] = qq{Fournisseur};
$mess[6674] = qq{d�j� factur�};
$mess[6675] = qq{d�j� factur�s};
$mess[6676] = qq{Livraison non planifi�e ou non point�e};
$mess[6677] = qq{Remise};
$mess[6678] = qq{Remise exceptionnelle};
# Messages de confirmation
$mess[6750] = qq{Impossible d'arr�ter le contrat plus de 4 jours avant la date de retour};
$mess[6751] = qq{Le contrat a �t� arr�t�};
$mess[6752] = qq{Le contrat fils a �t� arr�t�};
$mess[6753] = qq{Le contrat est pr�t � �tre factur� et ne peut plus �tre modifi�};
$mess[6754] = qq{Le contrat fils est pr�t � �tre factur�};
$mess[6755] = qq{Le contrat fils a �t� mis � jour};
$mess[6756] = qq{Le contrat a �t� mis � jour};
$mess[6757] = qq{Date de retour ant�rieure � la date de derni�re facturation};
$mess[6758] = qq{Date de retour incorrecte};
$mess[6759] = qq{Date de d�part incorrecte};
$mess[6760] = qq{Date de retour ant�rieure � la date de d�part};
$mess[6761] = qq{Impossible de passer le contrat en facturation car le carburant n'a pas �t� saisi};
$mess[6762] = qq{Impossible de passer le contrat en facturation car le transport retour n'a pas �t� planifi�};
$mess[6763] = qq{Impossible de passer le contrat en facturation car la remise exceptionnelle n'est pas valid�e};
$mess[6764] = qq{Seuls les jours en plus et les services compl�mentaires dont le montant n'est pas n�gatif peuvent �tre saisis};
$mess[6765] = qq{Vous ne pouvez pas saisir de service dont le montant est n�gatif};
$mess[6766] = qq{Montant du transport inf�rieur au forfait machine suppl�mentaire minimum.};
$mess[6767] = qq{Montant du service inf�rieur au montant minimum autoris� : };

# Remise en cours de contrat (nouvelle version)
$mess[6800] = qq{Remise en cours de contrat};
# Recherche
$mess[6810] = qq{Contrat � remettre en cours};
$mess[6811] = qq{Remettre en cours le contrat};
# Messages validation
$mess[6820] = qq{Le contrat a �t� remis en cours};
$mess[6821] = qq{Veuillez s�lectionner un contrat � remettre en cours};
$mess[6822] = qq{Le contrat a d�j� �t� remis en cours};

# Documents types
$mess[6900] = qq{Documents types};
$mess[6901] = qq{T�l�charger};
$mess[6910] = qq{R�pertoire};
$mess[6911] = qq{Type de document inconnu};
$mess[6912] = qq{Document Microsoft Word};
$mess[6913] = qq{Document Microsoft Excel};
$mess[6914] = qq{Document Acrobat PDF};
$mess[6915] = qq{Image JPEG};
$mess[6916] = qq{Image GIF};
$mess[6917] = qq{Image BMP};
$mess[6918] = qq{Image TIFF};
$mess[6919] = qq{Document Microsoft PPT};

# Remise exceptionnelle
$mess[6950] = qq{Remise exceptionnelle};
$mess[6951] = qq{Prix avant remise};
$mess[6952] = qq{Remise exceptionnelle};
$mess[6953] = qq{Prix remis�};
$mess[6954] = qq{Justification };
$mess[6955] = qq{Libell� facture};
$mess[6956] = qq{Remise accept�e};
$mess[6957] = qq{Remise en attente de validation};
$mess[6958] = qq{Remise refus�e};
$mess[6959] = qq{Remise exceptionnelle de };   # Lib impression contrat
$mess[6960] = qq{Remise exceptionnelle de };   # Lib impression devis
$mess[6961] = qq{demand�};
$mess[6962] = qq{Trait�e par};
$mess[6963] = qq{Limit� � 36 caract�res};
$mess[6964] = qq{Remise sup�rieure � 100 %};
$mess[6965] = qq{Prix net};

# Modification du tarif
$mess[7000] = qq{Tarif de base};
$mess[7001] = qq{Prix par jour HT};
$mess[7002] = qq{TARIF DE BASE};
$mess[7003] = qq{Avril 2006};
$mess[7004] = qq{�};
$mess[7005] = qq{plus de};

# Chargement de la page
$mess[7050] = qq{Chargement de la page en cours};
$mess[7051] = qq{Chargement de la page};

# Modification commande transport
$mess[7100] = qq{Modification de commande de transport externe};
$mess[7101] = qq{Pointage de commande de transport externe};
#
$mess[7110] = qq{N� commande};
$mess[7111] = qq{Sous-traitant};
$mess[7112] = qq{N� contrat};
$mess[7113] = qq{Date de transport};
$mess[7114] = qq{Consulter la commande};
$mess[7115] = qq{Enregistrer les modifications};
$mess[7116] = qq{Attention};
$mess[7118] = qq{Pas de ligne pour cette commande};
$mess[7119] = qq{Commande};
$mess[7120] = qq{Agence};
$mess[7121] = qq{�tabli par};
$mess[7123] = qq{Type};
$mess[7124] = qq{Date de pointage facture};
$mess[7125] = qq{N� facture};
$mess[7126] = qq{Montant factur�};
$mess[7127] = qq{Supprimer la commande};
$mess[7128] = qq{La commande a �t� supprim�e};
$mess[7129] = qq{Nouvelle recherche};
$mess[7130] = qq{La commande a �t� modifi�e};
$mess[7131] = qq{La date de transport n'est pas valide :};
$mess[7132] = qq{Commande non rapproch�e};
$mess[7133] = qq{Entre le};
$mess[7134] = qq{et le};
$mess[7135] = qq{=};
$mess[7136] = qq{&ge;};
$mess[7137] = qq{La date n'est pas valide :};
$mess[7138] = qq{Commandes point�es};
$mess[7139] = qq{Non};
$mess[7140] = qq{Oui};
$mess[7141] = qq{Indiff�rent};

# Contrat cadre
$mess[7150] = qq{Contrat Cadre};

# Suffixe Sous-location et Full service
$mess[7200] = qq{SL};
$mess[7201] = qq{FS};

# Commandes de transport (nouvelle interface)
$mess[7300] = qq{Nouvelle commande};
$mess[7301] = qq{Ajouter une nouvelle commande};
$mess[7302] = qq{Cocher/d�cocher tous};
$mess[7303] = qq{Valider les modifications};
$mess[7304] = qq{Imprimer le bon de commande};
$mess[7305] = qq{Tourn�e};
$mess[7306] = qq{Cr�er la tourn�e};
$mess[7307] = qq{Veuillez choisir un sous-traitant};
$mess[7308] = qq{La commande #XXX# est cr��e};
$mess[7309] = qq{Les modifications sont enregistr�es};
$mess[7310] = qq{La tourn�e #XXX# a �t� cr��e};
$mess[7311] = qq{Il existe d'autres tourn�es pour le m�me transporteur qui ne pourront �tre ordonn�es que lorsque celle-ci le sera};
$mess[7312] = qq{Il n'y a actuellement aucun transport � attribuer};
$mess[7313] = qq{Actualiser};
$mess[7314] = qq{Retirer de la commande};
$mess[7315] = qq{La tourn�e est d�j� cr��e (n� #XXX#)};
$mess[7316] = qq{Impossible de modifier la commande n� #YYY# car il existe d�j� une tourn�e (n� #XXX#)};
$mess[7317] = qq{Attention, toute commande pass�e en tourn�e n'est pas modifiable};
$mess[7318] = qq{Attention, au moins un contrat en erreur (ligne en rouge) a une commande de transport. R�gulariser le(s) pour pouvoir finaliser la(les) commande(s) correspondante(s)};


# Remise en arr�t de contrat
$mess[7400] = qq{Remise en arr�t de contrat};
# Recherche
$mess[7410] = qq{Contrat � remettre en arr�t};
$mess[7411] = qq{Remettre en arr�t le contrat};
# Messages validation
$mess[7420] = qq{Le contrat a �t� remis en arr�t};
$mess[7421] = qq{Veuillez s�lectionner un contrat � remettre en arr�t};
$mess[7422] = qq{Le contrat a d�j� �t� remis en arr�t};
$mess[7423] = qq{Le contrat ne peut pas �tre remis en arr�t :};
$mess[7424] = qq{ - le contrat n'est pas dans l'�tat 'mis en facturation'};
$mess[7425] = qq{ - le contrat a d�j� �t� totalement factur�};
# L�gende
$mess[7430] = qq{Etat de facturation};
$mess[7431] = qq{Facturation Partielle};
$mess[7432] = qq{Facturation Partielle Finale (location enti�rement factur�e)};
$mess[7433] = qq{Facturation Finale (contrat enti�rement factur�)};

# Remise en cours du pointage d'une machine dans une tourn�e
$mess[7500] = qq{Remise en cours du pointage d'une machine dans une tourn�e};
$mess[7501] = qq{S�lection};
$mess[7502] = qq{Rechercher};
# Recherche
$mess[7510] = qq{Contrat};
$mess[7511] = qq{Machine};
$mess[7512] = qq{Echou�};
$mess[7513] = qq{Effectu�};
$mess[7514] = qq{R�attribu�};
$mess[7515] = qq{En cours};
$mess[7516] = qq{Pas attribu�};
$mess[7517] = qq{Remettre en cours le pointage};
# Messages validation
$mess[7520] = qq{Le pointage de la machine dans la tourn�e a �t� remis en cours};
$mess[7521] = qq{Veuillez saisir un n� de contrat et/ou une machine};
$mess[7522] = qq{Le pointage de la machine dans la tourn�e ne peut pas �tre remis en cours :};
$mess[7523] = qq{ - impossible sur un };
$mess[7524] = qq{ - pointage actuel : };
$mess[7525] = qq{Le pointage de la machine dans la tourn�e a d�j� �t� remis en cours};
$mess[7526] = qq{Veuillez s�lectionner une ligne de tourn�e � remettre en cours};
$mess[7527] = qq{ - l'�tat actuel de la machine ne le permet pas};
$mess[7528] = qq{ - le contrat est dans l'�tat "Mis en facturation"};
$mess[7529] = qq{ - le contrat est totalement factur�};
$mess[7540] = qq{ - possible uniquement sur le dernier contrat};
# Colonnes du tableau
$mess[7530] = qq{Tourn�e};
$mess[7531] = qq{Contrat};
$mess[7532] = qq{Etat contrat};
$mess[7533] = qq{Machine};
$mess[7537] = qq{Etat machine};
$mess[7534] = qq{Type};
$mess[7535] = qq{Pointage};
$mess[7536] = qq{Seules les lignes du dernier contrat de type : "Livraison" ou "R�cup�ration" et dont le pointage est : "Echou�" ou "Effectu�" sont s�lectionnables};

# Mise � jour des alertes et des stocks
$mess[7600] = qq{Mise � jour des alertes et des stocks};
$mess[7601] = qq{Pour effectuer les mises � jour sur l'agence de %s : };

# Visu des transports par r�gion
$mess[7620] = qq{Visualisation des transports sur la r�gion};
$mess[7621] = qq{R�-actualiser};
$mess[7622] = qq{OUI};
$mess[7623] = qq{non};

# Visu des transports par r�gion
$mess[7649] = qq{Taux d'utilisation au %s - };
$mess[7650] = qq{Taux d'utilisation au %s - R�gion };
$mess[7651] = qq{Famille tarifaire};
$mess[7652] = qq{% util. inst. (nb mach.)};
$mess[7653] = qq{R�gional};
$mess[7654] = qq{National};
$mess[7655] = qq{TOTAL};
$mess[7656] = qq{Vous n'avez pas acc�s � cette fonctionnalit�.};

# Report des libell�s de l'interface "Nouveau client"
$mess[7700] = qq{APE};
$mess[7701] = qq{Secteur d'activit�};
$mess[7702] = qq{SIRET};
$mess[7703] = qq{Libell�};
$mess[7704] = qq{Adresse};
$mess[7705] = qq{Cmpt. adresse};
$mess[7706] = qq{Ville};
$mess[7707] = qq{T�l�phone};
$mess[7708] = qq{Portable};
$mess[7709] = qq{Fax};
$mess[7710] = qq{Contact};
$mess[7711] = qq{Commentaire};
$mess[7712] = qq{Mode Paiement};
$mess[7713] = qq{Banque};
$mess[7714] = qq{RIB};
$mess[7715] = qq{Grille de base};
$mess[7716] = qq{Groupe};
$mess[7717] = qq{Qualification};
$mess[7718] = qq{Email Client};
$mess[7719] = qq{Cr�ateur};
$mess[7720] = qq{Agence de cr�ation};
$mess[7721] = qq{Toujours fournir les documents de la machine};
$mess[7800] = qq{P�rim�tre national};

# Pro forma
$mess[8000] = qq{L�gende pour les cas erron�s};
$mess[8001] = qq{Pro forma non g�n�r�e};
$mess[8002] = qq{Pro forma non � jour};
$mess[8003] = qq{Pro forma non finalis�e};
$mess[8004] = qq{Remise exceptionnelle de location en attente};
$mess[8005] = qq{Remise exceptionnelle de transport en attente};
$mess[8006] = qq{Client avec ouverture de compte en attente};

$mess[8100] = qq{Impossible de planifier le transport sur le contrat #XXX#};
$mess[8101] = qq{Erreur sur le contrat #XXX#};
$mess[8102] = qq{Le contrat comporte une facture pro forma ou une facture d'acompte qui ne sera plus valide apr�s confirmation};
$mess[8103] = qq{Le client a des factures pro formas ou des factures d'acomptes qui ne seront plus valides apr�s confirmation, sur les documents suivant};

return @mess;
}	

sub liste_it {
my @mess;
#message
#sub saisie
$mess[0] = qq{Assurance};
$mess[1] = qq{comprise};
$mess[2] = qq{recherche};
$mess[3] = qq{ non };
$mess[4] = qq{Rechercher};
$mess[5] = qq{Veuillez saisir ce champ};
$mess[6] = qq{Nouveau Client};
$mess[7] = qq{Veuillez saisir le(s) champ(s)};
$mess[8] = qq{Veuillez saisir un Tarif Journalier OU un Forfait OU un Tarif Mensuel ET une Dur�e Mensuelle};
$mess[9] = qq{Non renseign�};
$mess[10] = qq{Conditions g�n�rales de location � approuver};
$mess[11] = qq{Convention cadre - ouverture de compte client � adresser};
$mess[12] = qq{Pour rattachement � un groupe contacter le Service Support Commercial};
#sub traitment
$mess[50] = qq{La date de d�but est ant�rieure � aujourd'hui};
$mess[51] = qq{La date de fin est ant�rieure � la date de d�but};
$mess[52] = qq{Mauvaise date !};
$mess[53] = qq{Date de d�but ant�rieure � aujourd\\'hui.\\nConserver cette date ?};
$mess[54] = qq{Nb. de jour(s) f�ri�s};
$mess[55] = qq{La dur�e};
$mess[56] = qq{Montant en �};
$mess[57] = qq{Montant de l'assurance en �};
$mess[58] = qq{Valider};
$mess[59] = qq{Dupliquer Contrat};
$mess[60] = qq{Le contrat};
$mess[61] = qq{est saisi, Duplication des donn�es};
$mess[62] = qq{Date non valide};
#sub recherche2
$mess[100] = qq{Le nombre des r�sultats de votre recherche est de};
$mess[101] = qq{Recherche};
$mess[102] = qq{Rechercher};
$mess[103] = qq{Veuillez saisir un des champs};
$mess[104] = qq{Validez votre Recherche};
$mess[105] = qq{r�sultat};
$mess[106] = qq{r�sultats};
$mess[107] = qq{Retour � la recherche};
$mess[108] = qq{Retour};
#sub traitment2
$mess[150] = qq{Le nombre de r�sultats de votre recherche est de};
$mess[151] = qq{Pas de r�sultat pour votre recherche};
$mess[152] = qq{Retour};
$mess[153] = qq{Voir devis};
$mess[154] = qq{CONTR�LER};
$mess[155] = qq{ARR�TER Contrat};
$mess[156] = qq{MODIFIER Contrat};
$mess[157] = qq{ANNULER Contrat};
$mess[158] = qq{Jour Plus/Moins};
$mess[159] = qq{Duplication des donn�es};
$mess[160] = qq{MODIFIER Chantier};
$mess[161] = qq{MODIFIER Info. Client};
$mess[162] = qq{Voir Contrat};
$mess[163] = qq{R�server Mod�le};
$mess[164] = qq{Trouver};
$mess[165] = qq{bloqu�};
$mess[166] = qq{d�bloqu�};
$mess[167] = qq{Client <ste>(<code>) <libbloc>};
$mess[168] = qq{Bonjour\nle Client <ste>(<code>) de <cp>,<ville>\nest <libbloc>\n@+\nBonne journ�e\n};
#sub stock
$mess[200] = qq{R�server Mod�le};
#sub nvclient
$mess[250] = qq{V�rifier le RIB};
$mess[251] = qq{RIB};
$mess[252] = qq{correct};
$mess[253] = qq{incorrect};
$mess[254] = qq{Nouveau Client};
$mess[255] = qq{Cherche Lycos ! Cherche !};
#sub bas form hauttab
$mess[300] = qq{Imprimer};
$mess[301] = qq{Fermer};
#sub saisiecnt
$mess[350] = qq{le prospect};
$mess[351] = qq{code client};
$mess[352] = qq{de l'agence};
$mess[353] = qq{Siret};
$mess[354] = qq{RIB};
$mess[355] = qq{T�l.};
$mess[356] = qq{Fax};
$mess[357] = qq{devient un Client d'Acc�s Industrie};
$mess[358] = qq{pour le contrat N�};
$mess[359] = qq{qui d�bute le};
$mess[360] = qq{devient client};
$mess[361] = qq{N� APE};
#sub traitnvclient
$mess[400] = qq{Origine};
$mess[401] = qq{Document origine};
$mess[402] = qq{Devis};
$mess[403] = qq{Pr�-contrat};
$mess[404] = qq{N� APE};
$mess[405] = qq{Siret};
$mess[406] = qq{Libell�};
$mess[407] = qq{Adresse};
$mess[408] = qq{Ville};
$mess[409] = qq{T�l.};
$mess[410] = qq{Portable};
$mess[411] = qq{Fax};
$mess[412] = qq{Contact};
$mess[413] = qq{Commentaire};
$mess[414] = qq{Banque};
$mess[415] = qq{RIB};
$mess[416] = qq{Mode de paiement};
$mess[417] = qq{Nouveau client de};
$mess[418] = qq{Secteur d'activit�};
#sub arretmodif
$mess[450] = qq{Nb. de jour(s) f�ri�(s)};
$mess[451] = qq{La date de fin est ant�rieure � la date de d�but};
$mess[452] = qq{Le contrat};
$mess[453] = qq{est arr�t�};
$mess[454] = qq{est modifi�};
#sub transport
$mess[500] = qq{Montant};
$mess[501] = qq{Contrat � �diter};
$mess[502] = qq{Collectif};
$mess[503] = qq{Code Client};
$mess[504] = qq{Client Valide ?};
$mess[505] = qq{Transport � attribuer};
$mess[506] = qq{Devis transporteur};
$mess[507] = qq{Commande transporteur};
$mess[508] = qq{Passer � Ordonner Tourn�e};
$mess[509] = qq{Date de transport};
$mess[510] = qq{Imprimer contrat};
$mess[511] = qq{Nouveau Client};
$mess[512] = qq{Panne machine};
$mess[513] = qq{Impossible de supprimer le transporteur sur le contrat};
#sub traitattmach
$mess[550] = qq{La Machine};
$mess[551] = qq{est s�lectionn�e au moins 2 fois};
$mess[552] = qq{La machine %s n'a pas le m�me mod�le que le contrat %s auquel vous essayez de l'attribuer};
$mess[553] = qq{Vous avez dej� attribu� cette machine};
#sub attmach
$mess[600] = qq{Veuillez saisir un num�ro de Parc d'une machine disponible};
$mess[601] = qq{Ce num�ro est incompatible avec le parc actuel de l'agence};
$mess[602] = qq{Attribuer Machine};
$mess[603] = qq{Imprimer liste machine};
#sub carburant
$mess[650] = qq{Contrat, D�signation};
$mess[651] = qq{Attribuer Carburant};
#sub transfert
$mess[700] = qq{Machines � r�cup�rer};
$mess[701] = qq{Transf�rer};
$mess[702] = qq{Passer � BL/BR/Tourn�e};
$mess[703] = qq{Machines � livrer};
$mess[704] = qq{Transport � attribuer};
$mess[705] = qq{N� Parc};
#sub tournee
$mess[750] = qq{Tourn�e N�};
$mess[751] = qq{Reste Capacit� (Kg) avant L/R/T};
$mess[752] = qq{En cours};
$mess[753] = qq{Effectu�e};
$mess[754] = qq{�chou�e};
$mess[755] = qq{Capacit�};
$mess[756] = qq{Reste};
$mess[757] = qq{Masse de r�cup.};
$mess[758] = qq{Masse de livraison};
$mess[759] = qq{Passer � BL/BR/Tourn�e};
$mess[760] = qq{Valider Tourn�e};
$mess[761] = qq{Le contr�le de la Tourn�e};
$mess[762] = qq{n'est pas complet, };
$mess[763] = qq{machine(s) � Pointer};
$mess[764] = qq{Le contr�le de la Tourn�e N�};
$mess[765] = qq{est complet};
$mess[780] = qq{Date tourn�e};
#sub valtourn
$mess[800] = qq{La Tourn�e N�};
$mess[801] = qq{devra �tre valid�e};
$mess[802] = qq{est valid�e};
#sub verification
$mess[850] = qq{Date de Contr�le : < 2 mois(*), < 15 jours(**), < 1 semaine (***), D�pass�e (****)};
$mess[851] = qq{Machines en Livraison};
$mess[852] = qq{Machines en Transfert de Ch. � Ch.};
$mess[853] = qq{Machines en Livraison};
$mess[854] = qq{Machine en R�cup�ration};
$mess[855] = qq{Machine en Transfert};
$mess[856] = qq{Machine en V�rification};
$mess[857] = qq{V�rification � R�vision};
$mess[858] = qq{R�vision � V�rification};
$mess[859] = qq{Disponible � V�rification};
$mess[860] = qq{Panne � V�rification};
$mess[861] = qq{V�rification � Panne};
$mess[862] = qq{Machine en R�vision};
$mess[863] = qq{Panne � R�vision};
$mess[864] = qq{R�vision � Panne};
$mess[865] = qq{Machine en Panne};
$mess[866] = qq{R�vision � Disponible};
$mess[867] = qq{V�rification � Disponible};
$mess[868] = qq{Panne � Disponible};
$mess[869] = qq{Machine Disponible};
$mess[870] = qq{�tat};
$mess[871] = qq{Impression Liste Machine};
$mess[872] = qq{Machines en R�cup�ration};
$mess[873] = qq{Machines en Transfert de Ag. � Ag.};
$mess[874] = qq{Impression envoy�e sur l'imprimante};
#sub devis
$mess[900] = qq{Le Devis};
$mess[901] = qq{est enregistr�};
$mess[902] = qq{Historique};
$mess[903] = qq{Nouveau Client};
$mess[904] = qq{Assurance};
$mess[905] = qq{ non };
$mess[906] = qq{comprise};
$mess[907] = qq{Passer au Pr�-contrat};
$mess[908] = qq{Assurance comprise};
$mess[909] = qq{Assurance Non comprise};
$mess[910] = qq{Modifier le pied de devis};
$mess[911] = qq{Abandonner};
$mess[912] = qq{Impression Devis};
$mess[913] = qq{Envoi par fax ou impression locale};
$mess[914] = qq{Erreur};
$mess[915] = qq{Pr�visualisation du fichier Acrobat};
$mess[916] = qq{Aucune page � afficher};
$mess[917] = qq{La zone est obligatoire};
$mess[918] = qq{Vous devez faire une estimation du transport.};
$mess[919] = qq{Impression impossible : remise exceptionnelle transport en attente ou transport incorrect.};
$mess[920] = qq{Impression impossible : remise exceptionnelle location en attente.};
$mess[921] = qq{Etes-vous s�r de vouloir annuler la remise exceptionnelle?};
$mess[922] = qq{Annuler la remise excep.};
$mess[923] = qq{Fiche client};
#sub traitdevis
$mess[950] = qq{Le devis};
$mess[951] = qq{devient un contrat};
$mess[952] = qq{est abandonn�};
$mess[953] = qq{Choisissez un motif d'abandon};
$mess[954] = qq{Autre};
$mess[955] = qq{Veuillez choisir un motif avant d'abandonner le devis};
$mess[956] = qq{Devis abandonn�};
$mess[957] = qq{Objet : Votre demande de devis location Acces Industrie};
$mess[958] = qq{Madame, monsieur, <br><br>
Suite � votre demande, vous trouverez, ci-joint, le devis de location �tabli sur la base des informations que vous nous avez communiqu�es.
Je reste � votre disposition au : _TELAG_ pour tout compl�ment d'information.<br><br>
Souhaitant avoir r�pondu � vos attentes, je vous prie d'agr�er, Madame, monsieur, l'expression de mes sinc�res salutations.<br><br>
  _NOMEXP_};
#sub ville
$mess[1000] = qq{Agence(s)};
$mess[1001] = qq{ou};
#sub facturation
$mess[1050] = qq{Nous sommes le };
$mess[1051] = qq{contrat(s) � facturer};
$mess[1052] = qq{Facturer};
$mess[1053] = qq{La facturation du};
$mess[1054] = qq{mois � d�j� �t� effectu�e};
$mess[1055] = qq{Il y a};
$mess[1056] = qq{Contrats � passer en facturation};
$mess[1057] = qq{On facture};
$mess[1058] = qq{Exportation vers Sage};
$mess[1059] = qq{La facturation ira};
$mess[1060] = qq{La facturation a d�j� �t� effectu�e};
$mess[1061] = qq{� saisir};
$mess[1062] = qq{jusqu'au};
$mess[1063] = qq{Contrats � arr�ter};
$mess[1064] = qq{Carburants � saisir};
#sub traitfac
$mess[1100] = qq{Le montant de};
$mess[1101] = qq{est de};
$mess[1102] = qq{pour};
$mess[1103] = qq{factures};
$mess[1104] = qq{Le montant Total des factures est de};
#sub tableau
$mess[1150] = qq{Tri par date};
$mess[1151] = qq{Tri par ville};
$mess[1152] = qq{R�vision};
$mess[1153] = qq{Panne};
$mess[1154] = qq{Contr�le};
$mess[1155] = qq{Disponible};
$mess[1156] = qq{Total machine(s)};
$mess[1157] = qq{�tat};
$mess[1158] = qq{Impression Liste Machine};
$mess[1159] = qq{V�rification};
#sub transfagag
$mess[1200] = qq{Transf�rer};
#sub validnvclient
$mess[1250] = qq{le Code Client};
$mess[1251] = qq{le Collectif};
$mess[1252] = qq{entre};
$mess[1253] = qq{� la ligne};
#sub mois_stat
$mess[1300] = qq{Historique};
#sub stat_mois
$mess[1350] = qq{D�but du mois };
$mess[1351] = qq{ et fin de mois};
$mess[1352] = qq{Nombre};
$mess[1353] = qq{Total};
$mess[1354] = qq{Moyenne};
$mess[1355] = qq{�cart type};
$mess[1356] = qq{C.A. agence};
$mess[1357] = qq{Dur�e};
$mess[1358] = qq{Assurance Non Comprise};
$mess[1359] = qq{Transport};
$mess[1360] = qq{Sous Traitant Transport};
$mess[1361] = qq{Carburant};
$mess[1362] = qq{Contrat Actif};
$mess[1363] = qq{Contrat Arr�t�};
$mess[1364] = qq{Contrat de moins de 5 jours};
$mess[1365] = qq{Contrat de plus de 5 jours};
$mess[1366] = qq{Contrat Mensuel};
$mess[1367] = qq{Forfait};
$mess[1368] = qq{Famille};
$mess[1369] = qq{C.A.};
$mess[1370] = qq{Nb. Parc};
$mess[1371] = qq{jour(s) Lou�(s)};
$mess[1372] = qq{Nb. Contrat};
$mess[1373] = qq{Jours Mois};
$mess[1374] = qq{Moyenne Prix Lou�};
$mess[1375] = qq{Moyenne Prix Parc};
$mess[1376] = qq{Rotation(Rentabilit�) en %};
$mess[1377] = qq{Ratio};
$mess[1378] = qq{Moyenne Jours};
$mess[1379] = qq{TOTAL};
$mess[1380] = qq{D�signation};
#sub nomade
$mess[1400] = qq{Je suis �};
$mess[1401] = qq{Agences};
$mess[1402] = qq{Je me t�l�porte};
#sub saisiemach
$mess[1450] = qq{La Machine};
$mess[1451] = qq{est saisie};
$mess[1452] = qq{Parc};
$mess[1453] = qq{ou};
$mess[1454] = qq{de S�rie};
$mess[1455] = qq{existe d�j�};
$mess[1456] = qq{Valider};
$mess[1457] = qq{Pr�-Contrat};
$mess[1458] = qq{Contrat};
$mess[1459] = qq{Arr�t};
$mess[1460] = qq{Archive};
$mess[1461] = qq{Annul�};
$mess[1462] = qq{Livraison};
$mess[1463] = qq{R�cup�ration};
$mess[1464] = qq{Transfert Chantier};
$mess[1465] = qq{Mis en facturation};
#message impression
$mess[1500] = qq{BON DE LIVRAISON};
$mess[1501] = qq{BON DE RETRAIT};
#imprssion ass
$mess[1550] = qq{Assurance factur�e au client = % du prix de location jour fois jours calendaires};
$mess[1551] = qq{Assurance non factur�e};
$mess[1552] = qq{Assur�e par le client};
$mess[1553] = qq{Assurance en plus = 10% du prix de location};
$mess[1554] = qq{};
$mess[1555] = qq{MACHINE ASSUR�E PAR LE CLIENT};
$mess[1556] = qq{Assurance factur�e au client = };
$mess[1557] = qq{% du prix de location jour fois jours calendaires};
$mess[1558] = qq{Assurance incluse};
#sub traitfac
$mess[1600] = qq{Tarif Mensuel};
$mess[1601] = qq{Forfait};
$mess[1602] = qq{Avoir};
$mess[1603] = qq{Transport aller};
$mess[1604] = qq{Transport retour};
$mess[1605] = qq{Carburant};
$mess[1606] = qq{Assurance 10%};
$mess[1607] = qq{ Du };
$mess[1608] = qq{ au };
$mess[1609] = qq{Heures deduites};
$mess[1610] = qq{Heures supplementaires};
$mess[1611] = qq{Heures samedi};
$mess[1612] = qq{Heures dimanche};
$mess[1613] = qq{Jours deduits};
$mess[1614] = qq{Jours supplementaires};
$mess[1615] = qq{Cette facture comprend les jours travailles des 2 mois};
$mess[1616] = qq{Participation au recyclage};
$mess[1617] = qq{Nettoyage};
$mess[1618] = qq{Service};
$mess[1619] = qq{Remise exceptionnelle transport de};
$mess[1620] = qq{Rem. except. XX% sur Aller};
$mess[1621] = qq{Rem. except. XX% sur Retour};
$mess[1622] = qq{Rem. except. XX% sur Aller (remise pour cumul * mach. incluse)};
$mess[1623] = qq{Rem. except. XX% sur Retour(remise pour cumul * mach. incluse)};
$mess[1624] = qq{Rem. pour cumul * mach. XX%};
$mess[1625] = qq{Rem. pour cumul * mach. XX%};
$mess[1626] = qq{Assurance n�goci�e 5%};
$mess[1627] = qq{Rem. except. sur Aller};
$mess[1628] = qq{Rem. except. sur Retour};
#arret contratraie
$mess[1650] = qq{Arr�t Impossible : contacter le responsable de la logistique pour pointer la livraison dans la tourn�e N�};
#sub traitcasse
$mess[1700] = qq{le Devis est saisi};
$mess[1701] = qq{Impression du document};
#annuler client
$mess[1750] = qq{Vous allez supprimer un Client ! Validez pour confirmer !};
$mess[1751] = qq{Le 1er Client est supprim�. Les Devis et Contrats ont �t� report�s sur le 2nd.};
$mess[1752] = qq{&nbsp;At&nbsp;&nbsp;&nbsp;Acc&nbsp;&nbsp;Ref};
$mess[1753] = qq{At&nbsp;:&nbsp;Attente&nbsp;&nbsp;&nbsp;Acc&nbsp;:&nbsp;Accept�&nbsp;&nbsp;&nbsp;Ref&nbsp;:&nbsp;Refus�};
#doublon client
$mess[1800] = qq{Le Client/Code Client };
$mess[1801] = qq{existe d�j� : Recherchez � ce libell�};
$mess[1802] = qq{a �t� cr��, vous pouvez rechercher � ce libell�};
$mess[1803] = qq{Un champ n'as pas �t� rempli correctement};
$mess[1804] = qq{Ce SIRET est d�j� utilis� par les client(s) suivant(s) : %s. Veuillez contacter le service Support Commercial afin de poursuivre la cr�ation du client.};
#sub verification
$mess[1850] = qq{Contr�le � V�rification};
$mess[1851] = qq{V�rification � Contr�le};
$mess[1852] = qq{Machine(s) en Contr�le};
#chg client Contrat
$mess[1900] = qq{Le Client va �tre chang� ! Validez pour confirmer !};
$mess[1901] = qq{Le Client est chang�};
#remettre en cours contrat
$mess[1950] = qq{Le Contrat va �tre remis en cours ! Validez pour confirmer !};
$mess[1951] = qq{Le Contrat est remis en cours};
$mess[1952] = qq{Un transport est planifi� pour ce contrat ! Il faut l'annuler avant de pouvoir remettre en cours le contrat !};
#newstat
$mess[2000] = qq{Agence};
$mess[2001] = qq{C.A. Mois};
$mess[2002] = qq{C.A. Transport};
$mess[2003] = qq{C.A. Carburant};
$mess[2004] = qq{C.A. SAV};
$mess[2005] = qq{C.A. Forfait};
$mess[2006] = qq{Co�t Financier};
$mess[2007] = qq{Famille};
$mess[2008] = qq{Nb. Machines};
$mess[2009] = qq{C.A. Machine Jour};
$mess[2010] = qq{C.A. Moyen Jour};
$mess[2011] = qq{Taux de Rotation Financier en %};
$mess[2012] = qq{Taux de Rotation Machine en %};
$mess[2013] = qq{Mod�le};
$mess[2014] = qq{Pour la famille};
$mess[2015] = qq{Taux d'occupation (%)};
$mess[2016] = qq{Taux d'occupation (%) depuis le d�but du mois};
$mess[2017] = qq{Taux d'occupation (%) du mois pr�c�dent};
$mess[2018] = qq{Comparatif Mensuel au mois pr�c�dent};
#consultation contrat
$mess[2050] = qq{N� Contrat};
$mess[2051] = qq{�tat Contrat};
$mess[2052] = qq{Client};
$mess[2053] = qq{T�l. Client};
$mess[2054] = qq{Fax Client};
$mess[2055] = qq{Contact Client};
$mess[2056] = qq{Chantier};
$mess[2057] = qq{Adr. Chantier};
$mess[2058] = qq{Ville Chantier};
$mess[2059] = qq{T�l. Chantier};
$mess[2060] = qq{Contact Chantier};
$mess[2061] = qq{D�signation Machine};
$mess[2062] = qq{N� Parc};
$mess[2063] = qq{Date D�part};
$mess[2064] = qq{Date Retour};
$mess[2065] = qq{Jour Plus};
$mess[2066] = qq{Jour Moins};
$mess[2067] = qq{Dur�e};
$mess[2068] = qq{Prix jour};
$mess[2069] = qq{Prix Mensuel};
$mess[2070] = qq{Montant Forfaitaire};
$mess[2071] = qq{Transport Aller};
$mess[2072] = qq{Transport Retour};
$mess[2073] = qq{Transport Planifi�};
$mess[2074] = qq{Chauffeur ou Sous Traitant (r�cup.)};
$mess[2075] = qq{Assurance non comprise};
$mess[2076] = qq{ non};
$mess[2077] = qq{Assurance};
$mess[2078] = qq{Montant Total};
$mess[2079] = qq{Commentaire};
$mess[2080] = qq{Fermer};
$mess[2081] = qq{Chauffeur ou Sous Traitant (aller)};
$mess[2082] = qq{hors jours d�j� factur�s : };
$mess[2083] = qq{Contact Commande};
$mess[2084] = qq{T�l�phone Contact Commande};
$mess[2085] = qq{Nettoyage};
$mess[2086] = qq{Service};
# transportint.cfg
$mess[2100] = qq{Transport Interne};
$mess[2101] = qq{Commande Transport externe};
$mess[2102] = qq{Num�ro};
$mess[2103] = qq{N� Contrat};
$mess[2104] = qq{Client};
$mess[2105] = qq{Chantier};
$mess[2106] = qq{C.P.};
$mess[2107] = qq{Ville};
$mess[2108] = qq{Quai};
$mess[2109] = qq{Livraison anticip�e possible};
$mess[2110] = qq{Date D�part};
$mess[2111] = qq{Date Retour};
$mess[2112] = qq{D�signation Machine};
$mess[2113] = qq{Poids};
$mess[2114] = qq{N� Parc};
$mess[2115] = qq{�tat Contrat};
$mess[2116] = qq{Tp. planifi�};
$mess[2117] = qq{Chauffeur};
$mess[2118] = qq{Commande transporteur};
#contrat.cfg et contrat2.cfg
$mess[2150] = qq{Num�ro};
$mess[2151] = qq{N� Contrat};
$mess[2152] = qq{Client};
$mess[2153] = qq{Chantier};
$mess[2154] = qq{C.P.};
$mess[2155] = qq{Ville};
$mess[2156] = qq{Date D�part};
$mess[2157] = qq{Date Retour};
$mess[2158] = qq{D�signation Machine};
$mess[2159] = qq{N� Parc};
$mess[2160] = qq{�tat Contrat};
$mess[2161] = qq{�dition Contrat(s)};
$mess[2162] = qq{�dition Contrat(s) pour 1�re impression};
#comcont.cfg
$mess[2200] = qq{Num�ro};
$mess[2201] = qq{Dernier Contrat};
$mess[2202] = qq{Indice};
$mess[2203] = qq{Modif.};
$mess[2204] = qq{Code postal chantier};
$mess[2205] = qq{D�signation Machine};
$mess[2206] = qq{N� Parc};
$mess[2207] = qq{�tat};
$mess[2208] = qq{Date Fin Contrat};
$mess[2209] = qq{Date �ch�ance};
$mess[2210] = qq{Organisme};
$mess[2211] = qq{Commande de Visite P�riodique};
$mess[2212] = qq{Commande de Contr�le};
$mess[2250] = qq{Imp. Unit�};
$mess[2251] = qq{N� Comm};
$mess[2252] = qq{Date de Visite};
#nvclient.cfg
$mess[2300] = qq{Nouveaux clients};
$mess[2301] = qq{Num�ro};
$mess[2302] = qq{Agence};
$mess[2303] = qq{Soci�t�};
$mess[2304] = qq{Adresse};
$mess[2305] = qq{C.P.};
$mess[2306] = qq{Ville};
$mess[2307] = qq{SIRET};
$mess[2308] = qq{T�l.};
$mess[2309] = qq{Fax};
$mess[2310] = qq{Mode Paiement};
$mess[2311] = qq{RIB};
$mess[2312] = qq{est d�j� attribu� au client};
$mess[2313] = qq{Modes Paiements};
$mess[2320] = qq{Recherche par nom de client};
#nvclient.cfg
$mess[2350] = qq{APE};
$mess[2351] = qq{Secteur d'activit�};
$mess[2352] = qq{SIRET};
$mess[2353] = qq{Libell�};
$mess[2354] = qq{Adresse};
$mess[2355] = qq{Cmpt. adresse};
$mess[2356] = qq{Ville};
$mess[2357] = qq{T�l�phone};
$mess[2358] = qq{Portable};
$mess[2359] = qq{Fax};
$mess[2360] = qq{Contact};
$mess[2361] = qq{Commentaire};
$mess[2362] = qq{Mode Paiement};
$mess[2363] = qq{Banque};
$mess[2364] = qq{RIB};
$mess[2365] = qq{Grille de base};
$mess[2366] = qq{Groupe};
$mess[2367] = qq{Qualification};
$mess[2368] = qq{Email Client};
$mess[2370] = qq{Aucun};
$mess[2371] = qq{Fin de validit� (Ass.)};
$mess[2372] = qq{Potentiel};
$mess[2373] = qq{Cl.};
$mess[2374] = qq{Pot.};
$mess[2375] = qq{Aucun};
$mess[2380] = qq{Classe};

$mess[2390] = qq{Nouveau client};

$mess[2393] = qq{archive.dev\@acces-industrie.com};
$mess[2394] = qq{christine.bellanger\@acces-industrie.com};
$mess[2395] = qq{valerie.penicaud\@acces-industrie.com};
$mess[2396] = qq{agnes.doucet\@acces-industrie.com};
$mess[2397] = qq{fabienne.cazassus\@acces-industrie.com};
$mess[2398] = qq{nabila.yemloul\@acces-industrie.com};
$mess[2399] = qq{6};
#Devis.cfg
$mess[2400] = qq{Clients};
$mess[2401] = qq{Ville Chantier};
$mess[2402] = qq{Libell� Chantier};
$mess[2403] = qq{Adresse Chantier};
$mess[2404] = qq{Contact Chantier};
$mess[2405] = qq{T�l. Chantier};
$mess[2406] = qq{N� Commande};
$mess[2407] = qq{Type Machine};
$mess[2408] = qq{Hors parc};
$mess[2409] = qq{Quantit�};
$mess[2410] = qq{Prix Jour/Mois};
$mess[2411] = qq{Type Montant};
$mess[2412] = qq{Dur�e};
$mess[2413] = qq{Date Pr�visionnelle};
$mess[2414] = qq{Montant};
$mess[2415] = qq{Transport Aller};
$mess[2416] = qq{Transport Retour};
$mess[2417] = qq{Assurance};
$mess[2418] = qq{Montant assurance (estimation)};
$mess[2419] = qq{Montant Total};
$mess[2420] = qq{Commentaire};
$mess[2421] = qq{Notes Commerciales (ne figurent pas sur le devis)};
$mess[2422] = qq{Devis Accept�};
$mess[2423] = qq{Calculer};
$mess[2424] = qq{Valider};
$mess[2425] = qq{Stock};
$mess[2426] = qq{Devis};
$mess[2427] = qq{Aller};
$mess[2428] = qq{Retour};
$mess[2429] = qq{Tarif de base};
$mess[2430] = qq{Remise};
$mess[2431] = qq{Ajouter une machine};
$mess[2432] = qq{Client};
$mess[2433] = qq{Machine};
$mess[2434] = qq{Dur�e};
$mess[2435] = qq{Prix};
$mess[2436] = qq{Montant};
$mess[2437] = qq{Transport};
$mess[2438] = qq{Demander une remise exceptionnelle};
$mess[2439] = qq{Rem. excep.};
$mess[2440] = qq{Zone};
$mess[2441] = qq{Estimation de transport};
$mess[2442] = qq{Montant aller et/ou retour incorrect(s).};
$mess[2443] = qq{Le montant aller ou retour est incorrect.};
$mess[2444] = qq{(entre 1 et 6)};
$mess[2445] = qq{Full service (minimum 6 mois)};
#tournee.cfg
$mess[2450] = qq{N� Contrat};
$mess[2451] = qq{Chantier};
$mess[2452] = qq{Adresse};
$mess[2453] = qq{C.P.};
$mess[2454] = qq{Ville};
$mess[2455] = qq{Contact};
$mess[2456] = qq{D�signation};
$mess[2457] = qq{N� Parc};
$mess[2458] = qq{Poids};
$mess[2459] = qq{�tat};
$mess[2460] = qq{�tat contrat};
$mess[2461] = qq{Nom chauffeur};
$mess[2462] = qq{Heure de Liv./R�cup.};
$mess[2463] = qq{Ordre};
#retour.cfg
$mess[2500] = qq{Ordonner Tourn�e};
$mess[2501] = qq{�dition BL/BR/Tourn�e};
$mess[2502] = qq{Pointage LV/RC/Transfert/Tourn�e};

$mess[2505] = qq{N� Contrat};
$mess[2506] = qq{Chantier};
$mess[2507] = qq{Adresse};
$mess[2508] = qq{C.P.};
$mess[2509] = qq{Ville};
$mess[2510] = qq{Contact};
$mess[2511] = qq{D�signation};
$mess[2512] = qq{N� Parc};
$mess[2513] = qq{�tat};
$mess[2514] = qq{�tat contrat};
$mess[2515] = qq{Nom chauffeur};
$mess[2516] = qq{Ordre};
$mess[2517] = qq{Heure de Liv./R�cup.};
$mess[2518] = qq{Effectu�e};
$mess[2519] = qq{Chauffeur Interne};
$mess[2520] = qq{Sous Traitant};
#Type de montant
$mess[2550] = qq{Type Mt};
$mess[2551] = qq{Jour};
$mess[2552] = qq{Mensuel};
$mess[2553] = qq{Forfaitaire};
#libell�s devis
$mess[2600] = qq{Modifier l'ent�te};
$mess[2601] = qq{Modifier la ligne};
$mess[2602] = qq{Modifier le mod�le};
$mess[2603] = qq{Vous devez imp�rativement enregistrer la ligne avant toute autre chose!};
$mess[2604] = qq{Vous devez imp�rativement enregistrer le contrat avant toute autre chose!};
#modifiarret modif2.cfg
$mess[2650] = qq{Modification/Arr�t de contrat};
$mess[2651] = qq{Num�ro};
$mess[2652] = qq{�tat Contrat};
$mess[2653] = qq{N� Contrat};
$mess[2654] = qq{Indice};
$mess[2655] = qq{Client};
$mess[2656] = qq{Libell� Chantier};
$mess[2657] = qq{Ville Chantier};
$mess[2658] = qq{Contact chantier};
$mess[2659] = qq{T�l�phone chantier};
$mess[2660] = qq{Contact commande};
$mess[2661] = qq{T�l�phone contact commande};
$mess[2662] = qq{D�signation Machine};
$mess[2663] = qq{N� Parc};
$mess[2664] = qq{Hors parc};
$mess[2665] = qq{Date D�part};
$mess[2666] = qq{Date Retour};
$mess[2667] = qq{Dur�e};
$mess[2668] = qq{Jour en plus};
$mess[2669] = qq{Jour en moins};
$mess[2670] = qq{Prix Jour};
$mess[2671] = qq{Prix Mois};
$mess[2672] = qq{Forfait};
$mess[2673] = qq{Assurance Comprise};
$mess[2674] = qq{Montant Assurance};
$mess[2675] = qq{Transport Aller};
$mess[2676] = qq{Enl�vement Aller};
$mess[2677] = qq{Transport Retour};
$mess[2678] = qq{Enl�vement Retour};
$mess[2679] = qq{Quai};
$mess[2680] = qq{Montant Total HT};
$mess[2681] = qq{Commande};
$mess[2682] = qq{Commentaire};
$mess[2683] = qq{Note};
$mess[2684] = qq{Paiement � la livraison};
$mess[2685] = qq{Client};
$mess[2686] = qq{N� Parc};
$mess[2687] = qq{Ce contrat a d�j� �t� arr�t�};
$mess[2688] = qq{Le Type de machine est chang�};
$mess[2689] = qq{Le contrat est annul�};
$mess[2690] = qq{Accessoires li�s au contrat};
$mess[2691] = qq{Accessoire (N� Parc)};
$mess[2692] = qq{Contrat (�tat)};
$mess[2693] = qq{Montant};
$mess[2694] = qq{Date D�part};
$mess[2695] = qq{Date Retour};
$mess[2696] = qq{Machine lou�e sans l'accessoire par d�faut};
$mess[2697] = qq{Contrat d'origine};
#Stock
$mess[2700] = qq{D�signation};
$mess[2701] = qq{Mod�le};
$mess[2702] = qq{�l�vation};
$mess[2703] = qq{�nergie};
$mess[2704] = qq{Poids};
$mess[2705] = qq{Stock R�serv�};
$mess[2706] = qq{Stock R�servable};
$mess[2707] = qq{Stock Total};
$mess[2708] = qq{LOUE 0%};
$mess[2709] = qq{TRSF 0%};
$mess[2710] = qq{PANN 0%};
$mess[2711] = qq{CONT 90%};
$mess[2712] = qq{RECU 50%};
$mess[2713] = qq{VERF 80%};
$mess[2714] = qq{REVI 80%};
$mess[2715] = qq{DISP 100%};
#Saisie pr�-contrat
$mess[2750] = qq{Saisie pr�-contrat};
$mess[2751] = qq{Clients};
$mess[2752] = qq{Type machine};
$mess[2753] = qq{Hors parc};
$mess[2754] = qq{Ville chantier};
$mess[2755] = qq{Libell� chantier};
$mess[2756] = qq{Adresse chantier};
$mess[2757] = qq{Contact chantier};
$mess[2758] = qq{Tel. chantier};
$mess[2759] = qq{Contact commande};
$mess[2760] = qq{Tel. contact commande};
$mess[2761] = qq{N� Commande};
$mess[2762] = qq{Date D�part};
$mess[2763] = qq{Date Retour};
$mess[2764] = qq{Jour Plus};
$mess[2765] = qq{Jour Moins};
$mess[2766] = qq{Prix apr�s rem. excep.};
$mess[2767] = qq{Dur�e};
$mess[2768] = qq{Prix Mois};
$mess[2769] = qq{Montant Forfaitaire};
$mess[2770] = qq{Assurance};
$mess[2771] = qq{Aller};
$mess[2772] = qq{Enl�v. sur place};
$mess[2773] = qq{Retour};
$mess[2774] = qq{Enl�v. sur place};
$mess[2775] = qq{Quai};
$mess[2776] = qq{Commentaire};
$mess[2777] = qq{Mod�le Imp�ratif};
$mess[2778] = qq{Transfert Chantier � Chantier};
$mess[2779] = qq{Paiement avant livraison};
$mess[2780] = qq{Calculer};
$mess[2781] = qq{Attention, le mod�le s�lectionn� n'est pas r�serv� tant que le pr�-contrat n'est pas valid� !};
$mess[2782] = qq{La derni�re machine r�servable de ce mod�le vient d'�tre r�serv�e, s�lectionnez un autre mod�le !};
$mess[2783] = qq{Type montant};
$mess[2784] = qq{Tarif de base};
$mess[2785] = qq{Remise};
$mess[2786] = qq{Remise exceptionnelle};
$mess[2787] = qq{Nettoyage};
$mess[2788] = qq{Enlevement sur place};
$mess[2789] = qq{Machine supp.};
$mess[2790] = qq{Machine supp.};

#Etat pr�-contrat
$mess[2800] = qq{Agence};
$mess[2801] = qq{Responsable};
$mess[2802] = qq{Adresse};
$mess[2803] = qq{T�l.};
$mess[2804] = qq{Fax};
$mess[2805] = qq{C.P.};
$mess[2806] = qq{Ville};
$mess[2807] = qq{Client};
$mess[2808] = qq{Adresse};
$mess[2809] = qq{C.P.};
$mess[2810] = qq{Ville};
$mess[2811] = qq{T�l.};
$mess[2812] = qq{Fax};
$mess[2813] = qq{Chantier};
$mess[2814] = qq{Adresse};
$mess[2815] = qq{C.P.};
$mess[2816] = qq{Ville};
$mess[2817] = qq{Responsable};
$mess[2818] = qq{Marque};
$mess[2819] = qq{Famille};
$mess[2820] = qq{�nergie};
$mess[2821] = qq{D�signation};
$mess[2822] = qq{Poids};
$mess[2823] = qq{Location Journali�re};
$mess[2824] = qq{Assurance};
$mess[2825] = qq{Transport Aller};
$mess[2826] = qq{Transport Retour};
$mess[2827] = qq{Date d�part};
$mess[2828] = qq{Date retour};
$mess[2829] = qq{Jour en plus};
$mess[2830] = qq{Jour en moins};
$mess[2831] = qq{Dur�e};
$mess[2832] = qq{Agent loueur};
$mess[2833] = qq{Indice};
$mess[2834] = qq{Assurance comprise};
#conscont.cfg
$mess[2850] = qq{Consultation Contrat};
$mess[2851] = qq{Num�ro};
$mess[2852] = qq{N� Contrat};
$mess[2853] = qq{Client};
$mess[2854] = qq{Libell� Chantier};
$mess[2855] = qq{C.P. Chantier};
$mess[2856] = qq{Ville Chantier};
$mess[2857] = qq{Contact Chantier};
$mess[2858] = qq{Contact Commande};
$mess[2859] = qq{D�signation Machine};
$mess[2860] = qq{N� Parc};
$mess[2861] = qq{Date D�part};
$mess[2862] = qq{Date Retour};
$mess[2863] = qq{Dur�e};
$mess[2864] = qq{Prix Jour};
$mess[2865] = qq{Prix Mois};
$mess[2866] = qq{Montant Assurance};
$mess[2867] = qq{Montant Gestion de recours};
$mess[2868] = qq{Transport Aller};
$mess[2869] = qq{Transport Retour};
$mess[2870] = qq{Quantit� Carbu (L)};
$mess[2871] = qq{Nettoyage};
$mess[2872] = qq{Service};
$mess[2873] = qq{Montant Total HT};
$mess[2874] = qq{�tat contrat};
$mess[2875] = qq{N� Commande};
$mess[2876] = qq{Note};

$mess[2882] = qq{N� Contrat};
$mess[2883] = qq{Client};
$mess[2884] = qq{N� Parc};
$mess[2885] = qq{Chantier};
$mess[2886] = qq{Libell� Chantier};
$mess[2887] = qq{D�partement Chantier};
$mess[2888] = qq{Ann�e};
$mess[2889] = qq{�tat contrat};
$mess[2890] = qq{soit};
$mess[2891] = qq{jour(s)};
$mess[2892] = qq{/&nbsp;jour};
$mess[2893] = qq{/&nbsp;mois};
$mess[2894] = qq{au forfait};
$mess[2895] = qq{Tarif ind�terminable !};
$mess[2896] = qq{Seuls les jours en plus peuvent �tre modifi�s};
#machine.cfg
$mess[2900] = qq{Liste machine / agence de };
$mess[2901] = qq{D�signation};
$mess[2902] = qq{N� Parc};
$mess[2903] = qq{Hors parc};
$mess[2904] = qq{N� S�rie};
$mess[2905] = qq{Date d'achat};
$mess[2906] = qq{Date Contr�le};
$mess[2907] = qq{Mod�le};
$mess[2908] = qq{Marque};
$mess[2909] = qq{�l�vation};
$mess[2910] = qq{�nergie};
$mess[2911] = qq{Poids};
$mess[2912] = qq{Longueur};
$mess[2913] = qq{Largeur};
$mess[2914] = qq{Capacit�};
$mess[2915] = qq{�tat};
$mess[2916] = qq{Probabilit� de location en %};
$mess[2917] = qq{Agence};
$mess[2918] = qq{Dernier contrat auquel cette machine a �t� attribu�e};
$mess[2919] = qq{N� Parc};
$mess[2920] = qq{N� S�rie};
$mess[2921] = qq{Mod�le Machine};
$mess[2922] = qq{Famille machine};
$mess[2923] = qq{Fournisseur};
$mess[2924] = qq{�l�vation ou Pression};
$mess[2925] = qq{�nergie};
$mess[2926] = qq{�tat machine};
$mess[2927] = qq{Parc};
$mess[2928] = qq{Agence};
$mess[2929] = qq{Pays};
$mess[2930] = qq{Dernier contrat portant sur cette machine};
$mess[2931] = qq{Historique des contrats};
$mess[2932] = qq{Voir l'historique};
$mess[2933] = qq{%d derniers :};
$mess[2934] = qq{Aucun contrat en Italie};
$mess[2935] = qq{Liste machine globale};
$mess[2936] = qq{R�gion};
#chantier.cfg
$mess[2950] = qq{Modification des Informations du Chantier};
$mess[2951] = qq{Num�ro};
$mess[2952] = qq{N� Contrat};
$mess[2953] = qq{Client};
$mess[2954] = qq{Libell� Chantier};
$mess[2955] = qq{Adresse Chantier};
$mess[2956] = qq{Ville};
$mess[2957] = qq{Contact};
$mess[2958] = qq{T�l�phone};
$mess[2959] = qq{Hauteur};
$mess[2960] = qq{Surface};
$mess[2961] = qq{Heure de Liv.};
$mess[2962] = qq{Heure de R�cup.};
$mess[2963] = qq{Commentaire};
$mess[2964] = qq{N� Contrat};
$mess[2965] = qq{Client};
$mess[2966] = qq{Libell� Chantier};
$mess[2967] = qq{Ville Chantier};
#infoclient.cfg
$mess[3000] = qq{Modification des Informations du Client};
$mess[3001] = qq{Num�ro};
$mess[3002] = qq{Code Client};
$mess[3003] = qq{APE};
$mess[3004] = qq{Siret};
$mess[3005] = qq{Libell�};
$mess[3006] = qq{Adresse};
$mess[3007] = qq{Adresse comp.};
$mess[3008] = qq{Ville};
$mess[3009] = qq{T�l.};
$mess[3010] = qq{Fax};
$mess[3011] = qq{Portable};
$mess[3012] = qq{Email};
$mess[3013] = qq{Contact};
$mess[3014] = qq{Collectif};
$mess[3015] = qq{LI_NO};
$mess[3016] = qq{Banque};
$mess[3017] = qq{Type Paiement};
$mess[3018] = qq{RIB};
$mess[3019] = qq{Nb. Facture(s)};
$mess[3020] = qq{Facturation Locale};
$mess[3021] = qq{Facture non group�e};
$mess[3022] = qq{Commentaire};
$mess[3023] = qq{Activit�};
$mess[3024] = qq{Grille de base};
$mess[3025] = qq{Sommeil/Bloqu�};
$mess[3026] = qq{Assurance};
$mess[3027] = qq{N� commande obligatoire};
$mess[3028] = qq{BC � fournir};
$mess[3029] = qq{Paiement avant livraison};
$mess[3030] = qq{Conditions g�n�rales de vente};
$mess[3031] = qq{Facturation du nettoyage};
$mess[3032] = qq{Tarif personnalis�};
$mess[3033] = qq{Oui};
$mess[3034] = qq{Non};
$mess[3035] = qq{Date d'application de l'assurance souhait�e.};
$mess[3036] = qq{Le SIRET n'est pas renseign�};
$mess[3037] = qq{Format de SIRET non valide};
$mess[3038] = qq{Le SIRET est d�j� utilis�};

$mess[3040] = qq{Code Client};
$mess[3041] = qq{Libell� Client};
$mess[3042] = qq{Ville};
$mess[3043] = qq{D�partement};
$mess[3044] = qq{Siret};
$mess[3045] = qq{Grille de base};
$mess[3046] = qq{Bons de livr./r�cup. obligatoires};

#anncl.cfg
$mess[3050] = qq{Suppression de Client};
$mess[3051] = qq{Client erron�};
$mess[3052] = qq{Client correct};
$mess[3053] = qq{Calculer};
# Assurance souhait�e
$mess[3054] = qq{Assurance souhait�e};
# Assurance actuelle
$mess[3055] = qq{Assurance actuelle};
# Gestion de recours actuelle
$mess[3056] = qq{Gestion de recours};
# Gestion de recours souhait�e
$mess[3057] = qq{Gestion de recours souhait�e};
$mess[3058] = qq{Date d'application souhait�e.};
$mess[3059] = qq{Non facturable};

#chclient.cfg
$mess[3100] = qq{Changement Client/Contrat};
$mess[3101] = qq{Mauvais Client sur Contrat};
$mess[3102] = qq{Bon Client};
$mess[3103] = qq{Valider};
$mess[3104] = qq{Attention, sur les deux clients, les informations suivantes sont diff�rentes (elles doivent �tre mises � jour <span style="text-decoration:underline;">manuellement</span>) :};
$mess[3105] = qq{ATTENTION, � la validation les tarifs de location et transport sont <span style="text-decoration:underline;">automatiquement maintenus suivant les conditions du premier client</span>};
$mess[3106] = qq{L�assurance du contrat n�est pas compatible avec l�assurance du nouveau client :};
$mess[3107] = qq{Un probl�me est survenu lors du changement de client sur le contrat.};

#chmach.cfg
$mess[3150] = qq{Changement Machine/Contrat};
$mess[3151] = qq{Contrat/N� Parc};
$mess[3152] = qq{Bonne Machine (Disponible)};
$mess[3153] = qq{Valider};
$mess[3154] = qq{Vous avez d�j� valid�};
$mess[3155] = qq{La machine est chang�e};
$mess[3156] = qq{Le Type de machine est chang�};
$mess[3157] = qq{Le contrat est annul�};
$mess[3158] = qq{Le Devis est saisie};
$mess[3159] = qq{Impossible de faire le changement, la tourn�e de livraison est point�e effectu�e};
$mess[3160] = qq{Impossible de faire le changement, le typage du mod�le de machines du contrat est diff�rent de celui de la machine saisie};
#contcourt.cfg
$mess[3200] = qq{Remise Contrat en Cours};
$mess[3201] = qq{Contrat Arr�t�};
$mess[3202] = qq{Valider};
$mess[3203] = qq{N'oubliez pas d'enregistrer la note avant d'annuler un contrat.};
#saisiemachine.cfg
$mess[3250] = qq{Saisie Machine};
$mess[3251] = qq{Recherche du Mod�le};
$mess[3252] = qq{N� de Parc};
$mess[3253] = qq{N� de S�rie};
$mess[3254] = qq{Date Apave};
$mess[3255] = qq{Calculer};
#sousloc.cfg
$mess[3300] = qq{Sous Location};
$mess[3301] = qq{Clients};
$mess[3302] = qq{Ville Chantier};
$mess[3303] = qq{Libell� Chantier};
$mess[3304] = qq{Adresse Chantier};
$mess[3305] = qq{Contact Chantier};
$mess[3306] = qq{T�l. Chantier};
$mess[3307] = qq{Contact Commande};
$mess[3308] = qq{T�l. Contact Commande};
$mess[3309] = qq{Fournisseur};
$mess[3310] = qq{Mod�le machine};
$mess[3311] = qq{Date D�part};
$mess[3312] = qq{Date Retour};
$mess[3313] = qq{Prix Jour};
$mess[3314] = qq{Dur�e Mensuelle};
$mess[3315] = qq{Prix Mois};
$mess[3316] = qq{Montant Forfaitaire};
$mess[3317] = qq{Assurance Comprise};
$mess[3318] = qq{Transport Aller};
$mess[3319] = qq{Enl�vement sur place Aller};
$mess[3320] = qq{Transport Retour};
$mess[3321] = qq{Enl�vement sur place Retour};
$mess[3322] = qq{Quai};
$mess[3323] = qq{Calculer};
$mess[3324] = qq{Machine suppl�mentaire Aller};
$mess[3325] = qq{Machine suppl�mentaire Retour};
#tableau.cfg
$mess[3350] = qq{Tableau};
$mess[3351] = qq{Machine(s) lou�e(s)};
$mess[3352] = qq{Machine(s) en livraison};
$mess[3353] = qq{Machine(s) en r�cup�ration};
$mess[3354] = qq{Machine(s) en };
$mess[3355] = qq{Mod�le};
$mess[3356] = qq{Famille};
$mess[3357] = qq{Hauteur};
$mess[3358] = qq{�nergie};
$mess[3359] = qq{N� Parc};
$mess[3360] = qq{N� Contrat};
$mess[3361] = qq{Client};
$mess[3362] = qq{Chantier};
$mess[3363] = qq{C.P.};
$mess[3364] = qq{Ville};
$mess[3365] = qq{Date de fin};
$mess[3366] = qq{POT};
$mess[3367] = qq{Tarif};
$mess[3368] = qq{Co�t};
$mess[3369] = qq{Ratio};
$mess[3370] = qq{Dur�e};
$mess[3380] = qq{Enl�vement};
$mess[3381] = qq{R�cup�ration};
$mess[3382] = qq{Transfert inter-chantiers};
$mess[3383] = qq{Reprise sur chantier};

$mess[3410] = qq{Num�ro};
$mess[3411] = qq{Contrat};
$mess[3412] = qq{Date d�but};
$mess[3413] = qq{Date fin};
$mess[3414] = qq{N� de parc};
$mess[3415] = qq{Mod�le};
$mess[3416] = qq{Chantier};
$mess[3417] = qq{Code postal};
$mess[3418] = qq{Ville};
#nomade.cfg
$mess[3450] = qq{Changement Agence};
$mess[3451] = qq{Agence destinataire };
$mess[3452] = qq{Vous &ecirc;tes &aacute;};
$mess[3453] = qq{Se t�l�porter !};
#statms.cfg
$mess[3500] = qq{Chiffres du mois};
#stat1.cfg
$mess[3550] = qq{Chiffres du mois 2};
#stat2.cfg
$mess[3600] = qq{Chiffres globaux};
#hisstat.cfg
$mess[3650] = qq{Historique des Chiffres};
$mess[3651] = qq{Mois};
$mess[3652] = qq{Ann�e};
$mess[3653] = qq{Janvier};
$mess[3654] = qq{F�vrier};
$mess[3655] = qq{Mars};
$mess[3656] = qq{Avril};
$mess[3657] = qq{Mai};
$mess[3658] = qq{Juin};
$mess[3659] = qq{Juillet};
$mess[3660] = qq{Ao�t};
$mess[3661] = qq{Septembre};
$mess[3662] = qq{Octobre};
$mess[3663] = qq{Novembre};
$mess[3664] = qq{D�cembre};
$mess[3665] = qq{5};
$mess[3666] = qq{2000};
$mess[3667] = qq{2001};
$mess[3668] = qq{2002};
$mess[3669] = qq{2003};
$mess[3670] = qq{2004};
#validagag.cfg
$mess[3700] = qq{Validation Agence � Agence};
$mess[3701] = qq{Machine};
$mess[3702] = qq{Machine(s)};
#transfagag.cfg
$mess[3750] = qq{Transfert Agence � Agence};
$mess[3751] = qq{Machine};
$mess[3752] = qq{Camion};
$mess[3753] = qq{Chauffeur};
$mess[3754] = qq{Agence destinataire};
$mess[3755] = qq{Machine(s)};
$mess[3756] = qq{Camion(s)};
$mess[3757] = qq{Chauffeur(s)};
$mess[3758] = qq{Agences};
#camcha.cfg
$mess[3800] = qq{Affectation Camion/Chauffeur};
$mess[3801] = qq{Camion};
$mess[3802] = qq{Chauffeur};
$mess[3803] = qq{Couple Camion/Chauffeur};
$mess[3804] = qq{Camion(s)};
$mess[3805] = qq{Chauffeur(s)};
$mess[3806] = qq{Couple(s)};
#transfert.cfg
$mess[3850] = qq{Transfert de chantier � chantier};
$mess[3851] = qq{Transport};
$mess[3852] = qq{Transport};
$mess[3853] = qq{N� Parc};
$mess[3854] = qq{Num�ro};
$mess[3855] = qq{N� Contrat};
$mess[3856] = qq{Client};
$mess[3857] = qq{Adresse};
$mess[3858] = qq{C.P.};
$mess[3859] = qq{Ville};
$mess[3860] = qq{D�signation Machine};
$mess[3861] = qq{N� Parc};
#carbu.cfg
$mess[3900] = qq{Carburant};
$mess[3901] = qq{Contrat, Client, D�signation, N� Parc};
$mess[3902] = qq{Quantit� Carburant (L)};
#verif.cfg
$mess[3950] = qq{V�rification Machine};
$mess[3951] = qq{Machine};
#Contr�le Apave
$mess[4000] = qq{Contr�le Machine};
$mess[4001] = qq{N� Parc};
$mess[4002] = qq{Mod�le Machine};
$mess[4003] = qq{Famille machine};
$mess[4004] = qq{�l�vation ou Pression};
$mess[4005] = qq{�nergie};
$mess[4006] = qq{�tat machine};
$mess[4007] = qq{D�signation};
$mess[4008] = qq{D�signation};
$mess[4009] = qq{N� Parc};
$mess[4010] = qq{N� S�rie};
$mess[4011] = qq{Mod�le};
$mess[4012] = qq{Marque};
$mess[4013] = qq{�l�vation};
$mess[4014] = qq{�nergie};
$mess[4015] = qq{Poids};
$mess[4016] = qq{Longueur};
$mess[4017] = qq{Largeur};
$mess[4018] = qq{Capacit�};
$mess[4019] = qq{�tat};
$mess[4020] = qq{Date Appave};
$mess[4021] = qq{Effectu�};
$mess[4022] = qq{Num};
#Consultation Devis
$mess[4050] = qq{Consultation Devis};
$mess[4051] = qq{N� Devis};
$mess[4052] = qq{Client};
$mess[4053] = qq{�tat Devis};
$mess[4054] = qq{Agent};
$mess[4055] = qq{N� Devis};
$mess[4056] = qq{Client};
$mess[4057] = qq{Lib. Chantier};
$mess[4058] = qq{Date};
$mess[4059] = qq{D�signation};
$mess[4060] = qq{Prix Jour};
$mess[4061] = qq{Dur�e};
$mess[4062] = qq{C.T};
$mess[4063] = qq{Montant};
$mess[4064] = qq{Pass� en contrat};
$mess[4065] = qq{�tat};
$mess[4066] = qq{Agent};
#Remise en cours de contrat
$mess[4100] = qq{Remise Contrat en Cours};
$mess[4101] = qq{Contrat Arr�t�};
$mess[4102] = qq{Valider};
#Cr�ation de machine
$mess[4150] = qq{Saisie Machine};
$mess[4151] = qq{Recherche du Mod�le};
$mess[4152] = qq{N� Parc};
$mess[4153] = qq{N� S�rie};
$mess[4154] = qq{Date Apave};
$mess[4155] = qq{Agence de cr�ation};
$mess[4156] = qq{Calculer};
#Facturation
$mess[4200] = qq{Facturation};
$mess[4201] = qq{N� Contrat};
$mess[4202] = qq{Date de Fin};
$mess[4203] = qq{Agence};
$mess[4204] = qq{Contact};
$mess[4205] = qq{T�l. Agence};
$mess[4206] = qq{Agence};
$mess[4207] = qq{� arr�ter};
$mess[4208] = qq{Carburant � saisir};
$mess[4209] = qq{� passer en facturation};
$mess[4210] = qq{T�l. Agence};
$mess[4211] = qq{R�capitulatif};
$mess[4212] = qq{Facturation hebdomadaire};
$mess[4213] = qq{Facturation mensuelle};
$mess[4214] = qq{Date de d�but};
$mess[4215] = qq{Date de cr�ation};
$mess[4216] = qq{Contrats anticip�s};
#Energies
$mess[4250] = qq{Bi-�nergie};
$mess[4251] = qq{Diesel};
$mess[4252] = qq{�lectrique};
$mess[4253] = qq{Manivelle};
$mess[4254] = qq{Gaz};
#Familles
$mess[4300] = qq{Boom};
$mess[4301] = qq{Chariot};
$mess[4302] = qq{Ciseau};
$mess[4303] = qq{Fl�che};
$mess[4304] = qq{Fl�che Chenille};
$mess[4305] = qq{Monte-charge};
$mess[4306] = qq{Mat droit};
$mess[4307] = qq{Push};
$mess[4308] = qq{Compacteur};
$mess[4309] = qq{Chariot Rotatif};
$mess[4310] = qq{Divers};
#Etat
$mess[4350] = qq{Contr�le};
$mess[4351] = qq{Disponible};
$mess[4352] = qq{Lou�e};
$mess[4353] = qq{Panne};
$mess[4354] = qq{R�cuperation};
$mess[4355] = qq{R�vision};
$mess[4356] = qq{V�rification};

$mess[4357] = qq{Vendue};
$mess[4358] = qq{En Transfert};
$mess[4359] = qq{Rendue};
$mess[4360] = qq{Restitu�};

$mess[4361] = qq{A r�cup�rer};

#Ann�es
$mess[4400] = qq{2001};
$mess[4401] = qq{2000};
$mess[4402] = qq{1999};
$mess[4403] = qq{1998};
#Info client
$mess[4450] = qq{Informations Client};
$mess[4451] = qq{Nom Client};
$mess[4452] = qq{Adresse};
$mess[4453] = qq{SIRET};
$mess[4454] = qq{APE};
$mess[4455] = qq{Secteur d'activit�};
$mess[4456] = qq{Contact client};
$mess[4457] = qq{T�l�phone};
$mess[4458] = qq{Fax};
$mess[4459] = qq{Portable};
$mess[4460] = qq{Adresse mail};
$mess[4461] = qq{Commentaires};
$mess[4462] = qq{Statistiques};
$mess[4463] = qq{Agence(s)};
$mess[4476] = qq{Total};
$mess[4483] = qq{Fermer};
# stati.cgi
#entete
$mess[5000] = qq{Nbr. contrats};
$mess[5001] = qq{Tx util.<br>J+1};
$mess[5002] = qq{TOT};
$mess[5003] = qq{ARR};
$mess[5004] = qq{ACT};
$mess[5005] = qq{ASS<br>FACT.};
$mess[5006] = qq{CT<5j};
$mess[5007] = qq{MENS};
$mess[5008] = qq{FORF};
$mess[5009] = qq{Arr�te au J=};
$mess[5010] = qq{Arr�te � la fin du mois};
$mess[5011] = qq{CA};
$mess[5012] = qq{TRANSP};
$mess[5013] = qq{CARB};
$mess[5014] = qq{COUT};
$mess[5015] = qq{CT Jr};
#titre groupe
$mess[5016] = qq{Parc};
$mess[5017] = qq{JR Loc};
$mess[5018] = qq{% Util};
$mess[5019] = qq{Px Jr};
$mess[5020] = qq{COUT};
$mess[5021] = qq{CT Jr};
#tittre bloc
$mess[5022] = qq{Fam. Comptable};
$mess[5023] = qq{Fam. Commerciale};
#date
$mess[5024] = qq{MOIS EN COURS};
$mess[5025] = qq{LOC};
$mess[5026] = qq{j. o.};
# Autres
$mess[5027] = qq{Livr.};
# BC � envoyer/r�ceptionner
$mess[5050] = qq{BC client � fournir};
$mess[5051] = qq{N� de Contrat};
$mess[5052] = qq{Ville Chantier};
$mess[5053] = qq{Client};
$mess[5054] = qq{N� T�l};
$mess[5055] = qq{Mod�le Machine};
$mess[5056] = qq{N� de Parc};
$mess[5057] = qq{Date de D�but};
$mess[5058] = qq{Date de Fin};
$mess[5059] = qq{Valid�};
$mess[5060] = qq{En attente};
# Paiement � la livraison
$mess[5600] = qq{Paiement � la livraison};
$mess[5601] = qq{N� de Contrat};
$mess[5602] = qq{Ville Chantier};
$mess[5603] = qq{Client};
$mess[5604] = qq{N� T�l};
$mess[5605] = qq{Mod�le Machine};
$mess[5606] = qq{N� de Parc};
$mess[5607] = qq{Date de D�but};
$mess[5608] = qq{Date de Fin};
$mess[5609] = qq{Montant acompte};
$mess[5610] = qq{N� acompte};
$mess[5611] = qq{Date versement};
$mess[5612] = qq{Type};
# Paiement � la livraison
$mess[5650] = qq{Paiement � la livraison};
$mess[5651] = qq{N� de Contrat};
$mess[5652] = qq{Client};
$mess[5653] = qq{Code Client};
$mess[5654] = qq{N� T�l};
$mess[5655] = qq{Date de D�but};
$mess[5656] = qq{Date de Fin};
$mess[5657] = qq{Montant acompte};
$mess[5658] = qq{N� acompte};
$mess[5659] = qq{Date versement};
$mess[5660] = qq{Type de paiement};
$mess[5661] = qq{Valid�};
# Devis (nouvelle formule)
# En-t�te
$mess[5800] = qq{Devis};
$mess[5801] = qq{Stock};
$mess[5802] = qq{Clients};
$mess[5803] = qq{Ville Chantier};
$mess[5804] = qq{Libell� Chantier};
$mess[5805] = qq{Adresse Chantier};
$mess[5806] = qq{Contact Chantier};
$mess[5807] = qq{T�l. Chantier};
$mess[5808] = qq{N� Commande};
# Colonnes
$mess[5820] = qq{Prix objectif};
$mess[5821] = qq{Co�t/jour};
$mess[5822] = qq{Ratio};
$mess[5823] = qq{% util};
$mess[5824] = qq{Quantit�};
$mess[5825] = qq{Type Montant};
$mess[5826] = qq{Dur�e};
$mess[5827] = qq{Date Pr�visionnelle};
$mess[5828] = qq{Prix Jour/Mois/Forfait};
$mess[5829] = qq{Ratio};
$mess[5830] = qq{Montant};
$mess[5831] = qq{Transport Aller};
$mess[5832] = qq{Transport Retour};
# Totaux
$mess[5850] = qq{Assurance};
$mess[5851] = qq{Montant Assurance};
$mess[5852] = qq{Montant Total};
# Pied
$mess[5860] = qq{Commentaire};
$mess[5861] = qq{Notes Commerciales (ne figurent pas sur le devis)};
$mess[5862] = qq{Devis Accept�};
$mess[5863] = qq{Calculer};
$mess[5864] = qq{Valider};
# Infos ratio Pr�-contrat
$mess[5900] = qq{Taux d'utilisation};
$mess[5901] = qq{Dur�e};
$mess[5902] = qq{Co�t/jour};
$mess[5903] = qq{Ratio objectif};
$mess[5904] = qq{Prix objectif/jour};
$mess[5905] = qq{Prix objectif/mois};
$mess[5906] = qq{Ratio};
# L�gende clients
$mess[6000] = qq{red};
$mess[6001] = '#'.qq{00CC33};
#$mess[6001] = qq{green};
$mess[6010] = qq{Client en rouge};
$mess[6011] = qq{Client en vert};
$mess[6020] = qq{Client n'ayant pas fait de chiffre d'affaires depuis plus de 14 mois};
$mess[6021] = qq{Client bloqu� en cours};
$mess[6022] = qq{Client � d�bloquer pour prolonger le contrat};
# Machine hors parc
$mess[6050] = qq{Parc normal};
$mess[6051] = qq{Hors parc};
#modifiarret modif2.cfg
$mess[6100] = qq{Modification/Arr�t de contrat};
$mess[6101] = qq{Num�ro};
$mess[6102] = qq{�tat Contrat};
$mess[6103] = qq{N� Contrat};
$mess[6104] = qq{Indice};
$mess[6105] = qq{Client};
$mess[6106] = qq{Libell� Chantier};
$mess[6107] = qq{Ville Chantier};
$mess[6108] = qq{Contact chantier};
$mess[6109] = qq{T�l�phone chantier};
$mess[6110] = qq{Contact commande};
$mess[6111] = qq{T�l�phone contact commande};
$mess[6112] = qq{D�signation Machine};
$mess[6113] = qq{N� Parc};
$mess[6114] = qq{Hors parc};
$mess[6115] = qq{Date D�part};
$mess[6116] = qq{Date Retour};
$mess[6117] = qq{Dur�e};
$mess[6118] = qq{Jour en plus};
$mess[6119] = qq{Jour en moins};
$mess[6120] = qq{Prix Jour};
$mess[6121] = qq{Prix Mois};
$mess[6122] = qq{Forfait};
$mess[6123] = qq{Assurance Comprise};
$mess[6124] = qq{Montant Assurance};
$mess[6125] = qq{Transport Aller};
$mess[6126] = qq{Enl�vement Aller};
$mess[6127] = qq{Transport Retour};
$mess[6128] = qq{Enl�vement Retour};
$mess[6129] = qq{Quai};
$mess[6130] = qq{Nettoyage};
$mess[6131] = qq{Libell� du service};
$mess[6132] = qq{Montant service};
$mess[6133] = qq{Montant Total HT};
$mess[6134] = qq{Commande};
$mess[6135] = qq{Commentaire};
$mess[6136] = qq{Note};
$mess[6137] = qq{Paiement � la livraison};
$mess[6138] = qq{Client};
$mess[6139] = qq{N� Parc};
$mess[6140] = qq{Ce contrat a d�j� �t� arr�t�};
$mess[6141] = qq{Le Type de machine est chang�};
$mess[6142] = qq{Le contrat est annul�};
$mess[6143] = qq{Accessoires li�s au contrat};
$mess[6144] = qq{Accessoire (N� Parc)};
$mess[6145] = qq{Contrat (�tat)};
$mess[6146] = qq{Montant};
$mess[6147] = qq{Date D�part};
$mess[6148] = qq{Date Retour};
$mess[6149] = qq{Machine lou�e sans l'accessoire par d�faut};
$mess[6150] = qq{Contrat d'origine};
# Contrat imprim�
$mess[6200] = qq{Location jour HT};
$mess[6201] = qq{Location mensuelle HT};
$mess[6202] = qq{Location forfaitaire HT};
# Assurance recherche rapide
$mess[6300] = qq{Factur�e};
$mess[6301] = qq{Non factur�e};
$mess[6302] = qq{Par le client};
$mess[6303] = qq{Incluse};
# BL-BR
$mess[6400] = qq{Aucune machine n'est attribu�e sur le contrat n� numcont.<BR>Vous devez l'attribuer pour finaliser la tourn�e.};
$mess[6401] = qq{Aucune machine n'est attribu�e sur les contrats n� numcont.<BR>Vous devez les attribuer pour finaliser la tourn�e.};
# Modification/Arr�t nouvelle formule
$mess[6500] = qq{Modification de contrat};
# Version tableau
# Colonnes
$mess[6510] = qq{N� contrat};
$mess[6511] = qq{Etat};
$mess[6512] = qq{Client};
$mess[6513] = qq{Chantier};
$mess[6514] = qq{Contact commande};
$mess[6515] = qq{Contact chantier};
$mess[6516] = qq{Machine};
$mess[6517] = qq{Dur�e};
$mess[6518] = qq{Jours en plus};
$mess[6519] = qq{Jours en moins};
$mess[6520] = qq{Prix};
$mess[6521] = qq{Assurance};
$mess[6522] = qq{Transport};
$mess[6523] = qq{Enl�vement};
$mess[6524] = qq{Quai};
$mess[6525] = qq{Montant HT};
$mess[6526] = qq{N� commande};
$mess[6527] = qq{Commentaire};
$mess[6528] = qq{Note};
# Autres
$mess[6550] = qq{hors parc};
$mess[6551] = qq{Aller};
$mess[6552] = qq{Retour};
$mess[6553] = qq{/ jour};
$mess[6554] = qq{/ mois};
$mess[6555] = qq{forfait};
$mess[6556] = qq{jour};
$mess[6557] = qq{jours};
# Affichage contrat
$mess[6600] = qq{Client};
$mess[6601] = qq{Nom};
$mess[6602] = qq{Grille de base};
$mess[6603] = qq{Contact commande};
$mess[6604] = qq{Chantier};
$mess[6605] = qq{Libell�};
$mess[6606] = qq{Ville};
$mess[6607] = qq{Contact chantier};
$mess[6608] = qq{Machine};
$mess[6609] = qq{D�signation};
$mess[6610] = qq{Energie};
$mess[6611] = qq{N� de parc};
$mess[6612] = qq{Hors parc};
$mess[6613] = qq{Taux d'utilisation};
$mess[6614] = qq{Co�t};
$mess[6615] = qq{Ratio objectif};
$mess[6616] = qq{Prix objectif};
$mess[6617] = qq{Ratio};
$mess[6618] = qq{Dur�e};
$mess[6619] = qq{Date d�part};
$mess[6620] = qq{Date retour};
$mess[6621] = qq{soit};
$mess[6622] = qq{jour};
$mess[6623] = qq{jours};
$mess[6624] = qq{ouvrable};
$mess[6625] = qq{ouvrables};
$mess[6626] = qq{calendaire};
$mess[6627] = qq{calendaires};
$mess[6628] = qq{Jours en plus};
$mess[6629] = qq{Jours en moins};
$mess[6630] = qq{Tarif de base};
$mess[6631] = qq{Prix};
$mess[6632] = qq{par jour};
$mess[6633] = qq{par mois};
$mess[6634] = qq{forfait};
$mess[6635] = qq{Assurance};
$mess[6636] = qq{Transport};
$mess[6637] = qq{Aller};
$mess[6638] = qq{Retour};
$mess[6639] = qq{Enl�vement};
$mess[6640] = qq{Quai};
$mess[6641] = qq{Services compl�mentaires};
$mess[6642] = qq{Ajouter un service};
$mess[6643] = qq{Supprimer un service};
$mess[6644] = qq{Montant total HT};
$mess[6645] = qq{Informations compl�mentaires};
$mess[6646] = qq{N� de commande};
$mess[6647] = qq{Commentaire};
$mess[6648] = qq{Note};
$mess[6649] = qq{Recalculer};
$mess[6650] = qq{Modifier le contrat};
$mess[6651] = qq{Arr�ter le contrat};
$mess[6652] = qq{Passer le contrat en facturation};
$mess[6653] = qq{Contrat d'origine};
$mess[6654] = qq{Accessoires};
$mess[6655] = qq{du};
$mess[6656] = qq{au};
$mess[6657] = qq{/ jour};
$mess[6658] = qq{Supprim�};
$mess[6659] = qq{Factur�};
$mess[6660] = qq{Tarif};
$mess[6661] = qq{Divers};
$mess[6662] = qq{Factur� en fin de contrat};
$mess[6663] = qq{Veuillez choisir un service dans la liste d�roulante};
$mess[6664] = qq{Enti�rement factur�};
$mess[6665] = qq{Factur� partiellement jusqu'au};
$mess[6666] = qq{Non factur�};
$mess[6667] = qq{Visualiser le tarif};
$mess[6668] = qq{Carburant};
$mess[6669] = qq{Quantit�};
$mess[6670] = qq{litre};
$mess[6671] = qq{litres};
$mess[6672] = qq{Montant};
$mess[6673] = qq{Fournisseur};
$mess[6674] = qq{d�j� factur�};
$mess[6675] = qq{d�j� factur�s};
$mess[6676] = qq{Livraison non planifi�e ou non point�e};
$mess[6677] = qq{Remise};
$mess[6678] = qq{Remise exceptionnelle};
# Messages de confirmation
$mess[6750] = qq{Impossible d'arr�ter le contrat plus de 4 jours avant la date de retour};
$mess[6751] = qq{Le contrat a �t� arr�t�};
$mess[6752] = qq{Le contrat fils a �t� arr�t�};
$mess[6753] = qq{Le contrat est pr�t � �tre factur� et ne peut plus �tre modifi�};
$mess[6754] = qq{Le contrat fils est pr�t � �tre factur�};
$mess[6755] = qq{Le contrat fils a �t� mis � jour};
$mess[6756] = qq{Le contrat a �t� mis � jour};
$mess[6757] = qq{Date de retour ant�rieure � la date de derni�re facturation};
$mess[6758] = qq{Date de retour incorrecte};
$mess[6759] = qq{Date de d�part incorrecte};
$mess[6760] = qq{Date de retour ant�rieure � la date de d�part};
$mess[6761] = qq{Impossible de passer le contrat en facturation car le carburant n'a pas �t� saisi};
$mess[6762] = qq{Impossible de passer le contrat en facturation car le transport retour n'a pas �t� planifi�};
$mess[6763] = qq{Impossible de passer le contrat en facturation car la remise exceptionnelle n'est pas valid�e};
$mess[6764] = qq{Seuls les jours en plus et les services compl�mentaires dont le montant n'est pas n�gatif peuvent �tre saisis};
$mess[6765] = qq{Vous ne pouvez pas saisir de service dont le montant est n�gatif};
$mess[6766] = qq{Montant du transport inf�rieur au forfait machine suppl�mentaire minimum.};
$mess[6767] = qq{Montant du service inf�rieur au montant minimum autoris� : };

# Remise en cours de contrat (nouvelle version)
$mess[6800] = qq{Remise en cours de contrat};
# Recherche
$mess[6810] = qq{Contrat � remettre en cours};
$mess[6811] = qq{Remettre en cours le contrat};
# Messages validation
$mess[6820] = qq{Le contrat a �t� remis en cours};
$mess[6821] = qq{Veuillez s�lectionner un contrat � remettre en cours};
$mess[6822] = qq{Le contrat a d�j� �t� remis en cours};

# Documents types
$mess[6900] = qq{Documents types};
$mess[6901] = qq{T�l�charger};
$mess[6910] = qq{R�pertoire};
$mess[6911] = qq{Type de document inconnu};
$mess[6912] = qq{Document Microsoft Word};
$mess[6913] = qq{Document Microsoft Excel};
$mess[6914] = qq{Document Acrobat PDF};
$mess[6915] = qq{Image JPEG};
$mess[6916] = qq{Image GIF};
$mess[6917] = qq{Image BMP};
$mess[6918] = qq{Image TIFF};
$mess[6919] = qq{Document Microsoft PPT};

# Remise exceptionnelle
$mess[6950] = qq{Remise exceptionnelle};
$mess[6951] = qq{Prix avant remise};
$mess[6952] = qq{Remise exceptionnelle};
$mess[6953] = qq{Prix remis�};
$mess[6954] = qq{Justification };
$mess[6955] = qq{Libell� facture};
$mess[6956] = qq{Remise accept�e};
$mess[6957] = qq{Remise en attente de validation};
$mess[6958] = qq{Remise refus�e};
$mess[6959] = qq{Remise exceptionnelle de };   # Lib impression contrat
$mess[6960] = qq{Remise exceptionnelle de };   # Lib impression devis
$mess[6961] = qq{demand�};
$mess[6962] = qq{Trait�e par};
$mess[6963] = qq{Limit� � 36 caract�res};
$mess[6964] = qq{Remise sup�rieure � 100 %};
$mess[6965] = qq{Prix net};

# Modification du tarif
$mess[7000] = qq{Tarif de base};
$mess[7001] = qq{Prix par jour HT};
$mess[7002] = qq{TARIF DE BASE};
$mess[7003] = qq{Avril 2006};
$mess[7004] = qq{�};
$mess[7005] = qq{plus de};

# Chargement de la page
$mess[7050] = qq{Chargement de la page en cours};
$mess[7051] = qq{Chargement de la page};

# Modification commande transport
$mess[7100] = qq{Modification de commande de transport externe};
$mess[7101] = qq{Pointage de commande de transport externe};
#
$mess[7110] = qq{N� commande};
$mess[7111] = qq{Sous-traitant};
$mess[7112] = qq{N� contrat};
$mess[7113] = qq{Date de transport};
$mess[7114] = qq{Consulter la commande};
$mess[7115] = qq{Enregistrer les modifications};
$mess[7116] = qq{Attention};
$mess[7118] = qq{Pas de ligne pour cette commande};
$mess[7119] = qq{Commande};
$mess[7120] = qq{Agence};
$mess[7121] = qq{�tabli par};
$mess[7123] = qq{Type};
$mess[7124] = qq{Date de pointage facture};
$mess[7125] = qq{N� facture};
$mess[7126] = qq{Montant factur�};
$mess[7127] = qq{Supprimer la commande};
$mess[7128] = qq{La commande a �t� supprim�e};
$mess[7129] = qq{Nouvelle recherche};
$mess[7130] = qq{La commande a �t� modifi�e};
$mess[7131] = qq{La date de transport n'est pas valide :};
$mess[7132] = qq{Commande non rapproch�e};
$mess[7133] = qq{Entre le};
$mess[7134] = qq{et le};
$mess[7135] = qq{=};
$mess[7136] = qq{&ge;};
$mess[7137] = qq{La date n'est pas valide :};
$mess[7138] = qq{Commandes point�es};
$mess[7139] = qq{Non};
$mess[7140] = qq{Oui};
$mess[7141] = qq{Indiff�rent};

# Contrat cadre
$mess[7150] = qq{Contrat Cadre};

# Suffixe Sous-location et Full service
$mess[7200] = qq{SL};
$mess[7201] = qq{FS};

# Commandes de transport (nouvelle interface)
$mess[7300] = qq{Nouvelle commande};
$mess[7301] = qq{Ajouter une nouvelle commande};
$mess[7302] = qq{Cocher/d�cocher tous};
$mess[7303] = qq{Valider les modifications};
$mess[7304] = qq{Imprimer le bon de commande};
$mess[7305] = qq{Tourn�e};
$mess[7306] = qq{Cr�er la tourn�e};
$mess[7307] = qq{Veuillez choisir un sous-traitant};
$mess[7308] = qq{La commande #XXX# est cr��e};
$mess[7309] = qq{Les modifications sont enregistr�es};
$mess[7310] = qq{La tourn�e #XXX# a �t� cr��e};
$mess[7311] = qq{Il existe d'autres tourn�es pour le m�me transporteur qui ne pourront �tre ordonn�es que lorsque celle-ci le sera};
$mess[7312] = qq{Il n'y a actuellement aucun transport � attribuer};
$mess[7313] = qq{Actualiser};
$mess[7314] = qq{Retirer de la commande};
$mess[7315] = qq{La tourn�e est d�j� cr��e (n� #XXX#)};
$mess[7316] = qq{Impossible de modifier la commande n� #YYY# car il existe d�j� une tourn�e (n� #XXX#)};
$mess[7317] = qq{Attention, toute commande pass�e en tourn�e n'est pas modifiable};
$mess[7318] = qq{Attention, au moins un contrat en erreur (ligne en rouge) a une commande de transport. R�gulariser le(s) pour pouvoir finaliser la(les) commande(s) correspondante(s)};


# Remise en arr�t de contrat
$mess[7400] = qq{Remise en arr�t de contrat};
# Recherche
$mess[7410] = qq{Contrat � remettre en arr�t};
$mess[7411] = qq{Remettre en arr�t le contrat};
# Messages validation
$mess[7420] = qq{Le contrat a �t� remis en arr�t};
$mess[7421] = qq{Veuillez s�lectionner un contrat � remettre en arr�t};
$mess[7422] = qq{Le contrat a d�j� �t� remis en arr�t};
$mess[7423] = qq{Le contrat ne peut pas �tre remis en arr�t :};
$mess[7424] = qq{ - le contrat n'est pas dans l'�tat 'mis en facturation'};
$mess[7425] = qq{ - le contrat a d�j� �t� totalement factur�};
# L�gende
$mess[7430] = qq{Etat de facturation};
$mess[7431] = qq{Facturation Partielle};
$mess[7432] = qq{Facturation Partielle Finale (location enti�rement factur�e)};
$mess[7433] = qq{Facturation Finale (contrat enti�rement factur�)};

# Remise en cours du pointage d'une machine dans une tourn�e
$mess[7500] = qq{Remise en cours du pointage d'une machine dans une tourn�e};
$mess[7501] = qq{S�lection};
$mess[7502] = qq{Rechercher};
# Recherche
$mess[7510] = qq{Contrat};
$mess[7511] = qq{Machine};
$mess[7512] = qq{Echou�};
$mess[7513] = qq{Effectu�};
$mess[7514] = qq{R�attribu�};
$mess[7515] = qq{En cours};
$mess[7516] = qq{Pas attribu�};
$mess[7517] = qq{Remettre en cours le pointage};
# Messages validation
$mess[7520] = qq{Le pointage de la machine dans la tourn�e a �t� remis en cours};
$mess[7521] = qq{Veuillez saisir un n� de contrat et/ou une machine};
$mess[7522] = qq{Le pointage de la machine dans la tourn�e ne peut pas �tre remis en cours :};
$mess[7523] = qq{ - impossible sur un };
$mess[7524] = qq{ - pointage actuel : };
$mess[7525] = qq{Le pointage de la machine dans la tourn�e a d�j� �t� remis en cours};
$mess[7526] = qq{Veuillez s�lectionner une ligne de tourn�e � remettre en cours};
$mess[7527] = qq{ - l'�tat actuel de la machine ne le permet pas};
$mess[7528] = qq{ - le contrat est dans l'�tat "Mis en facturation"};
$mess[7529] = qq{ - le contrat est totalement factur�};
$mess[7540] = qq{ - possible uniquement sur le dernier contrat};
# Colonnes du tableau
$mess[7530] = qq{Tourn�e};
$mess[7531] = qq{Contrat};
$mess[7532] = qq{Etat contrat};
$mess[7533] = qq{Machine};
$mess[7537] = qq{Etat machine};
$mess[7534] = qq{Type};
$mess[7535] = qq{Pointage};
$mess[7536] = qq{Seules les lignes du dernier contrat de type : "Livraison" ou "R�cup�ration" et dont le pointage est : "Echou�" ou "Effectu�" sont s�lectionnables};

# Mise � jour des alertes et des stocks
$mess[7600] = qq{Mise � jour des alertes et des stocks};
$mess[7601] = qq{Pour effectuer les mises � jour sur l'agence de %s : };

# Visu des transports par r�gion
$mess[7620] = qq{Visualisation des transports sur la r�gion};
$mess[7621] = qq{R�-actualiser};
$mess[7622] = qq{OUI};
$mess[7623] = qq{non};

# Visu des transports par r�gion
$mess[7649] = qq{Taux d'utilisation au %s - };
$mess[7650] = qq{Taux d'utilisation au %s - R�gion };
$mess[7651] = qq{Famille tarifaire};
$mess[7652] = qq{% util. inst. (nb mach.)};
$mess[7653] = qq{R�gional};
$mess[7654] = qq{National};
$mess[7655] = qq{TOTAL};
$mess[7656] = qq{Vous n'avez pas acc�s � cette fonctionnalit�.};

# Report des libell�s de l'interface "Nouveau client"
$mess[7700] = qq{APE};
$mess[7701] = qq{Secteur d'activit�};
$mess[7702] = qq{SIRET};
$mess[7703] = qq{Libell�};
$mess[7704] = qq{Adresse};
$mess[7705] = qq{Cmpt. adresse};
$mess[7706] = qq{Ville};
$mess[7707] = qq{T�l�phone};
$mess[7708] = qq{Portable};
$mess[7709] = qq{Fax};
$mess[7710] = qq{Contact};
$mess[7711] = qq{Commentaire};
$mess[7712] = qq{Mode Paiement};
$mess[7713] = qq{Banque};
$mess[7714] = qq{RIB};
$mess[7715] = qq{Grille de base};
$mess[7716] = qq{Groupe};
$mess[7717] = qq{Qualification};
$mess[7718] = qq{Email Client};
$mess[7719] = qq{Cr�ateur};
$mess[7720] = qq{Agence de cr�ation};
$mess[7721] = qq{Toujours fournir les documents de la machine};
$mess[7800] = qq{P�rim�tre national};

# Pro forma
$mess[8000] = qq{L�gende pour les cas erron�s};
$mess[8001] = qq{Pro forma non g�n�r�e};
$mess[8002] = qq{Pro forma non � jour};
$mess[8003] = qq{Pro forma non finalis�e};
$mess[8004] = qq{Remise exceptionnelle de location en attente};
$mess[8005] = qq{Remise exceptionnelle de transport en attente};
$mess[8006] = qq{Client avec ouverture de compte en attente};

$mess[8100] = qq{Impossible de planifier le transport sur le contrat #XXX#};
$mess[8101] = qq{Erreur sur le contrat #XXX#};
$mess[8102] = qq{Le contrat comporte une facture pro forma ou une facture d'acompte qui ne sera plus valide apr�s confirmation};
$mess[8103] = qq{Le client a des factures pro formas ou des factures d'acomptes qui ne seront plus valides apr�s confirmation, sur les documents suivant};

return @mess;
}	

1;
