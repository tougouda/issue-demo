# $Id: autentif.pm,v 1.1.1.1 2006/08/01 07:48:07 clliot Exp $
# $Log: autentif.pm,v $
# Revision 1.1.1.1  2006/08/01 07:48:07  clliot
# no message
#
# Revision 2.17  2006/04/06 13:53:41  julien
# Traduction du message d'attente.
#
# Revision 2.16  2006/04/06 13:48:33  julien
# Traduction du message d'attente.
#
# Revision 2.15  2006/04/05 08:26:06  julien
# no message
#
# Revision 2.14  2006/02/13 08:49:36  julien
# Consultation facture autoris� pour tout le monde.
#
# Revision 2.13  2006/02/08 14:20:52  julien
# creation_pdf.cgi doit �chapper � verif_droit.
#
# Revision 2.12  2006/02/07 08:30:29  julien
# Ajout de bon_comm.cgi � la liste des fichiers autoris�s.
#
# Revision 2.11  2006/02/07 08:25:53  julien
# Affectation de la variable $nomfich pour la v�rification des droits sur les fichiers.
#
# Revision 2.10  2006/02/06 16:52:38  julien
# V. 1.4 : Tarification v2
#
# Revision 2.9.4.5  2006/01/24 15:49:15  julien
# Exceptions sur le verif_droit.
#
# Revision 2.9.4.4  2006/01/23 11:13:03  julien
# no message
#
# Revision 2.9.4.2  2006/01/13 10:24:06  julien
# Suprression de l'appel � la fonction focus_vide dans le onLoad du body.
#
# Revision 2.9.4.1  2005/12/14 15:52:15  julien
# no message
#
# Revision 2.9  2005/11/25 10:07:09  julien
# Menu contextuel de nouveau disponible.
#
# Revision 2.8  2005/11/25 09:00:02  julien
# D�sactivation de la touche F5 et du menu contextuel.
#
# Revision 2.7  2005/10/11 18:50:05  julien
# Tarification v. 1.
#
# Revision 2.6.2.4  2005/10/05 15:41:02  julien
# no message
#
# Revision 2.6.2.3  2005/10/05 15:40:19  julien
# no message
#
# Revision 2.6.2.2  2005/10/05 14:40:29  julien
# no message
#
# Revision 2.6.2.1  2005/10/05 10:15:42  julien
# no message
#
# Revision 2.5  2005/06/21 10:22:23  julien
# V�rification par rapport � $HTTP_PORTAIL.
#
# Revision 2.4  2005/05/03 16:28:26  julien
# Tra�age du temps d'ex�cution des scripts.
#
# Revision 2.3  2005/05/03 12:28:10  julien
# Export de la fontion trace_temps.
#
# Revision 2.2  2005/05/03 09:41:08  julien
# Tra�age du temps d'ex�cution des scripts.
#
# Revision 2.1  2004/11/18 12:59:32  julien
# Tra�age de l'envoi des mails.
#
# Revision 2.0  2004/09/16 15:07:21  julien
# Version r�cup�r�e de Lancelot et nettoy�e
#
# Revision 1.2  2004/09/16 14:34:46  julien
# Ajout des identifiants
#

package autentif;
require 	Exporter;
@ISA 		= qw(Exporter);
@EXPORT 	= qw(
                     trim ente enteLight $agence $tva $unitmonaie $prixcarbu $agent $nomag $pemag $pays $pemail $profil $chimp $testacces autantif lectbase LectureConfig %Param @LstParam $NbParam $numw $bloc %Config $dbi @dbh @nombase $fich basform hauttab email Pied trace_temps
                    );

#use 		strict;
use varglob;
use CGI;
use File::Path;
use clemail;
use tracetemps;
use accesbd;
use securite;
use message;
#use baseDB;
use 		vars qw(
                     $agence $tva $unitmonaie $prixcarbu $agent $nomag $pemag $pays $pemail $profil $chimp $testacces  $dbi @dbh @nombase %Param @LstParam $NbParam $numw $bloc %Config $fich
                       );


use lib '../../inc', '../../lib';
use LOC::Globals;

#
# sub autantif : remplir les parmaetre d'autenfication
# sub lectbase : lit la bonne BDD
# sub LectureConfig : lit le fichier de config
# sub basform et sub hauttab : :affiche entete de fichier html
# sub ville :: ville correspondante
# sub email

my $cgi= new CGI;
$cgi->autoEscape(undef);

my $build = &LOC::Globals::get('build');


#envirmonnement
 $agence;
 $tva;
 $unitmonaie;
 $prixcarbu;
 $agent;
 $nomag;
$pemag;
 $pays;
 $pemail;
 $profil;
 $chimp;
 $testacces;
#base
 $dbi=1;
 @dbh;
 @nombase;

#
%Param;
 @LstParam;
 $NbParam;
 $numw ;
 $bloc ;
 %Config;
$fich=$cgi->param('menu');
#autentification

sub autantif {
	my @identif=@_;
	my $logi=$identif[0];

	my $dbh;
	$dbh=DBI->connect($SRV1_DATASRC, $SRV1_LOGIN, $SRV1_PASSWD);
	my $sth;
	my $sth2;
	my $sth3;
	my $requete;
	my $requete2;

	#agence

	$requete=qq{SELECT PERSONNEL.AGAUTO , PEAUTO, PENOM,PEPRENOM,PEMAIL,PROFILAUTO,IMPRIMAUTO FROM AUTH.PERSONNEL WHERE PERSONNEL.PELOGIN = "$logi"};

	$sth=$dbh->prepare($requete);
	$sth->execute();
	my @data;
	while (@data=$sth->fetchrow_array())
	  {
	  $agence=$data[0];
	  $agent=$data[1];
	  $nomag=$data[2];
	  $pemag=$data[3];
	  $pemail=$data[4];
	  $profil=$data[5];
	  $chimp=$data[6];
	 }
	 $sth->finish;

	my $f=$cgi->param('menu');

	# test profil
	#my $requete3=qq{select PROFILAUTO FROM AUTH.MEPR LEFT join LOCATION.MENU ON MENU.MEAUTO=MEPR.MEAUTO WHERE MEFICHIER="$f" AND PROFILAUTO="$profil"};
	#$sth3=$dbh->prepare($requete3);
	#
	#$sth3->execute();
	#$testacces=$sth3->rows;
	$testacces=1;
	#$sth3->finish;




	#pays

	$requete2=qq{SELECT  PATVA , PAMONNAIE  , PACARBURANT , PAYS.PACODE  FROM AUTH.PAYS LEFT JOIN AUTH.PAPE ON PAYS.PACODE=PAPE.PACODE WHERE PAPE.PEAUTO = "$agent"};

	$sth2=$dbh->prepare($requete2);
	$sth2->execute();
	my @data2;
	while (@data2=$sth2->fetchrow_array())
	  {
	  $tva=$data2[0];
	  $unitmonaie=$data2[1];
	  $prixcarbu=$data2[2];
	  $pays=$data2[3];
	 }
	 $sth2->finish;
	$dbh->disconnect;



 }

#lecture de la base

sub lectbase{
my $ps=$pays;
	$ps=~ tr/A-Z/a-z/;

	my $fich=$ps."\/DB";
	if ($pays ne "FR")
	{

	$fich .=$ps;
		}
	$fich .="\.cfg";

&LectureConfig($fich);
for ($numw=0; $numw<$NbParam; $numw++) {
 my $bloc       = $LstParam[$numw];

  # DataBase exist
  if ($bloc =~/db/)
  {  my $connection=$Param{$bloc}{'con'};
    my $user=$Param{$bloc}{'user'};
    my $passe=$Param{$bloc}{'password'};
     #print "bonjour=$connection,$user,$passe\n";
  	$dbh[$dbi]=DBI->connect($connection,$user,$passe)||die "erreur de connection";
  	$nombase[$dbi]=$Param{$bloc}{'nom'};
  	$dbi++;



   };

}

}

#--------------------------------------------------------------------------
# LectureConfig - Lecture fichier de configuration
#
sub LectureConfig {
   ($FicConf) = shift (@_);
  ($nom_bloc, $lst_bloc);
   ($cle, $op, $data);
   ($bloc);

  $NbParam = 0;
  open (CONF, $FicConf);
  undef $bloc;
  while (<CONF>) {
    s/[\012\015]/ /g;
    s/#.*//;
    s/^\s+//g;
    s/\s+$//g;
    s/\s*(\+?=|{|})\s*/$1/g;
    next unless length;
    $bloc .= $_;
    if (/}/) {
##print "$bloc<===\n";
      if ($bloc =~ /([\w\-_]+){(.*)}/) {
        $nom_bloc  = $1;
        $lst_bloc  = $2;
        foreach (split(/;/, $lst_bloc)) {
          if    (/(\w+)(\+?=)(.*)/) {      # ----- x+=y ou x=y
            $cle=$1; $op=$2; $data=$3;
          }
          if ($nom_bloc eq "config") {     # ----- Param. de config.
            $Config{$cle} = $data;
          }
          else {
            if ($op eq "+=") {
              $Param{$nom_bloc}{$cle} .= " $data";
            }
            else {
              $Param{$nom_bloc}{$cle} = $data;
           }
          }
        }
        $LstParam[$NbParam++] = $nom_bloc if $nom_bloc !~ /(global|config)/;
      }
      undef $bloc;
    }
  }
  close(CONF);
}

#
#   bas des formulaire de saisie
#

sub basform {
 my $tableau;
 $tableau =qq{<center><tr><td>\n};
 $tableau .=qq{<div align="center"><a href="javascript:history.back()"><b>$mess[152]</b></a></div>\n};
 #
 #$tableau .=qq{</td><td><div align="center"><a href="javascript:init()"><b>Imprimer</b></a></div>\n};
$frsaisi=$cgi->param('nbsaisie');
#if (($frsaisi eq "oui")&&((defined $input{'saisie'})||(defined $input{'Rechercher'})))
#{
$tableau .=qq{
<SCRIPT Language="Javascript">
function printit(){
if (NS) {
window.print() ;
} else {
var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
WebBrowser1.ExecWB(6, 2);
//Use a 1 vs. a 2 for a prompting dialog box
WebBrowser1.outerHTML = "";
}
}

var NS = (navigator.appName == "Netscape");
var VERSION = parseInt(navigator.appVersion);
if (VERSION > 3) {
document.write('</td><td><div align="center"><a href="javascript:printit()"><b>$mess[300]</b></a></div>');
};
</SCRIPT>
};
#}
 $tableau .=qq{</td><td><div align="center"><a href="javascript:window.close()"><b>$mess[301]</b></a></div>\n};
 $tableau .=qq{</td></tr></center>\n};
 return $tableau;
}

sub hauttab {
	my $tableau;
	$tableau = qq{
		<td width="54"><a href="javascript:history.back(1)"><img src="$URL_IMG/img_2.gif" align="MIDDLE" border="0" width="54" height="36" alt=$mess[152]></a>
		<SCRIPT Language="Javascript">
		function printit() {
			window.print() ;
		}

		var NS = (navigator.appName == "Netscape");
		var VERSION = parseInt(navigator.appVersion);

		if (VERSION > 3) {
			document.write('</td><td width="54"><a href="javascript:printit()"><img src="$URL_IMG/img_3.gif" align="MIDDLE" border="0" width="54" height="36" alt="$mess[300]"></a>');
		}
		</SCRIPT>
		</td>
		<td width="48"><a href="javascript:window.close()"><img src="$URL_IMG/img_4.gif" align="MIDDLE" border="0" width="48" height="36" alt="$mess[301]"></a></td>
	};

	return $tableau;
}

# Envoyer Un Email
sub email {
	my ($orgasm,$destin,$sbj,$email)=@_;

  open (MESSAGE,"| /usr/sbin/sendmail $destin");
  print MESSAGE "To: $destin\n";
  print MESSAGE "From: $orgasm\n";
  print MESSAGE "Subject: $sbj\n";
  print MESSAGE "\n";
  print MESSAGE "$email\n";
  print MESSAGE "\n";
  close (MESSAGE);

  &trace_mail($orgasm, $destin, $sbj, "");

	}

# FA 8724 : rajout du param[4] pour retourner � la recherche sur la page pr�c�dente 
sub ente {
	my @param = @_;

    my @mess = &message($G_PAYS);

#	if (&doubl_conn) {
#        exit;
#	}

    my @nomfich = split(/\//, $0);
    my $nomfich = $nomfich[@nomfich-1];

    if ($nomfich ne "nvclient.cgi"
        && $nomfich ne "bon_comm.cgi"
        && $nomfich ne "creation_pdf.cgi"
        && $nomfich ne "consultationFacture.cgi"
        && $nomfich ne "pop_fact.cgi"
        && $nomfich ne "fichmach.cgi"
        && $nomfich ne "impcomtransp.cgi"
        && $nomfich ne "chgtTxTVA.cgi"
        && $nomfich ne "modification.cgi"
    ) {
	   &verif_droit($0);
    }

	my $entete = &hauttab;

    if ($G_DEV ne '')
    {
        $dev = '<td style="text-align: right; font-size: 1.5em; font-style: italic;">[DB' . $G_DEV . ']</td>';
    }

    if ($G_ENVIR eq '') {
        $tps_exec = qq{<TD align="right"><SPAN id="tps_exec"></SPAN></TD>};
    }

	my $margin = "0px";
	$margin = "5px"   if($isIE);
    
    # d�sactivation du F5 sur l'exploitation et l'int�gration
    # en gros, sauf sur le d�v (� une vache pr�s).
    $toucheF5 = '';

    if ($G_ENVIR ne 'dev')
    {
        $toucheF5 = qq{onKeyDown="return test_f5(event, '$G_PAYS')"};
    }

    # fond suivant l'environnement
    my $styleBody = "background-image: url('$URL_IMG/fond-log.gif');"; ;
    if ($G_ENVIR =~ /^dev/)
    {
        $styleBody = "background-image: url('$URL_IMG/fond-log-devel.gif');";
    }
    elsif ($G_ENVIR eq 'int')
    {
        $styleBody = "background-image: url('$URL_IMG/fond-log-recette.gif');";
    }
    elsif ($G_ENVIR eq 'for')
    {
        $styleBody = "background-image: url('$URL_IMG/fond-log-formation.gif');";
    }

    my $faviconsUrl = $HTTP_PORTAIL . '/web/img/icons/' . ($G_ENVIR eq '' ? '' : 'test/');

    print $cgi->header({-type => 'text/html', -pragma => 'no-cache', -expires => '-1d', -cache_control => 'no-cache, must-revalidate'});
    if (defined($param[5]) && $param[5])
    {
        print qq{<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">};
    }
    print qq{<HTML>
<HEAD>
<meta name="google" content="notranslate"/>
<META http-equiv="Pragma" content="no-cache">
<META http-equiv="Expires" content="-1">
<TITLE>$param[0] $G_ENVIR</TITLE>
<META http-equiv="content-type" content="text/html; charset=iso-8859-15" />
<link rel="icon" type="image/png" sizes="16x16" href="${faviconsUrl}LOC_favicon_16x16.png">
<link rel="icon" type="image/png" sizes="32x32" href="${faviconsUrl}LOC_favicon_32x32.png">
<link rel="icon" type="image/png" sizes="128x128" href="${faviconsUrl}LOC_favicon_128x128.png">
<link rel="apple-touch-icon" href="${faviconsUrl}LOC_favicon_152x152_precomposed.png" />
<LINK rel=stylesheet href="$URL_CONF/base.css" type="text/css">
<NOSCRIPT>
    <META http-equiv="refresh" content="0;URL=$HTTP_PORTAIL/cgi-bin/nojavascript.cgi">
</NOSCRIPT>
<SCRIPT language="JavaScript" src="$URL_CONF/super.js?bld=$build"></SCRIPT>
<SCRIPT language="JavaScript" src="$URL_CONF/toucheF5.js"></SCRIPT>
<SCRIPT language="JavaScript">
// Masquage du message d'attente en cas d'arr�t du chargement de la page
// Ne fonctionne pas sous Netscape
function hideMessage() {
    document.getElementById('patience').style.display = 'none';
}

document.onstop = hideMessage;
</SCRIPT>

</HEAD>

<BODY $toucheF5 onLoad="document.getElementById('patience').style.display = 'none';" style="$styleBody">
<SCRIPT language="JavaScript" src="$URL_CONF/wz_tooltip.js"></SCRIPT>
<!-- Message d'attente -->
<DIV id="patience" style="position: absolute; top: 0px; left: 0px; padding: 2px 5px; border-bottom: 1px solid infotext; background-color: infobackground; color: infotext;">
<TABLE id="tab_patience" name="tab_patience" style="width: 100%; margin: 0px $margin;">
    <TR>
        <TD align="left">$mess[7050]</TD>
        <TD align="right"><IMG src="$URL_IMG/progressbar.gif" alt="$mess[7051]" title="$mess[7051]" align="absmiddle"></TD>
    </TR>
</TABLE>
</DIV>

<!-- Barre de titre -->
<TABLE bgcolor="C6CDC5" border="0" cellspacing="0" cellpadding="0" width="100%">
	<TR>
		<TD align="left" width="27" style="padding-left: 5px"><IMG src="$URL_IMG/minilogo.svg" alt=""></TD>
		<TD align="left" style="padding-left: 10px"><FONT class="TITRE">$param[1]</FONT></TD>
		$dev
        $tps_exec
		$entete
	</TR>
</TABLE>

<!-- Nom d'utilisateur -->
<FONT>
<TABLE width="100%">
<tr>
    <td>
    <B>$param[2] $param[3]</B>
    </td>
};
if (defined($param[4]))
{
    print qq{
    <td align="right">
    <a href=$param[4]>$mess[107]</a>
    </td>};
}
print qq{
</tr>
</TABLE>
</FONT>

<!-- Corps de la page -->
};
}

sub enteLight {
    my @param = @_;

    my @mess = &message($G_PAYS);

#    if (&doubl_conn) {
#        exit;
#    }

    my @nomfich = split(/\//, $0);
    my $nomfich = $nomfich[@nomfich-1];

    if ($nomfich ne "nvclient.cgi"
        && $nomfich ne "bon_comm.cgi"
        && $nomfich ne "creation_pdf.cgi"
        && $nomfich ne "consultationFacture.cgi"
        && $nomfich ne "pop_fact.cgi"
        && $nomfich ne "fichmach.cgi"
        && $nomfich ne "impcomtransp.cgi"
        && $nomfich ne "chgtTxTVA.cgi"
    ) {
       &verif_droit($0);
    }

    my $entete;# = &hauttab;

    if ($G_DEV ne '')
    {
        $dev = '<td style="text-align: right; font-size: 1.5em; font-style: italic;">[DB' . $G_DEV . ']</td>';
    }

    if ($G_ENVIR ne '') {
        $tps_exec = qq{<TD align="right"><SPAN id="tps_exec"></SPAN></TD>};
    }

    my $margin = "0px";
    $margin = "5px"   if($isIE);
    
    # d�sactivation du F5 sur l'exploitation et l'int�gration
    # en gros, sauf sur le d�v (� une vache pr�s).
    $toucheF5 = '';

    if ($G_ENVIR ne 'dev')
    {
        $toucheF5 = qq{onKeyDown="return test_f5(event, '$G_PAYS')"};        
    }
    
    # fond suivant l'environnement
    my $styleBody = "background-image: url('$URL_IMG/fond-log.gif');"; ;    
    if ($G_ENVIR eq 'dev')
    {
        $styleBody = "background-image: url('$URL_IMG/fond-log-devel.gif');";     
    }
    elsif ($G_ENVIR eq 'int')
    {
        $styleBody = "background-image: url('$URL_IMG/fond-log-recette.gif');";     
    }
    elsif ($G_ENVIR eq 'for')
    {
        $styleBody = "background-image: url('$URL_IMG/fond-log-formation.gif');";     
    }

    my $faviconsUrl = $HTTP_PORTAIL . '/web/img/icons/' . ($G_ENVIR eq '' ? '' : 'test/');

    # TODO: <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

    print $cgi->header({-type => 'text/html', -pragma => 'no-cache', -expires => '-1d', -cache_control => 'no-cache, must-revalidate'});
    print qq{<HTML>
<HEAD>
<meta name="google" content="notranslate"/>
<META http-equiv="Pragma" content="no-cache">
<META http-equiv="Expires" content="-1">
<TITLE>$param[0] $G_ENVIR</TITLE>
<META http-equiv="content-type" content="text/html; charset=iso-8859-15" />
<link rel="icon" type="image/png" sizes="16x16" href="${faviconsUrl}LOC_favicon_16x16.png">
<link rel="icon" type="image/png" sizes="32x32" href="${faviconsUrl}LOC_favicon_32x32.png">
<link rel="icon" type="image/png" sizes="128x128" href="${faviconsUrl}LOC_favicon_128x128.png">
<link rel="apple-touch-icon" href="${faviconsUrl}LOC_favicon_152x152_precomposed.png" />
<LINK rel=stylesheet href="$URL_CONF/base.css" type="text/css">
<NOSCRIPT>
    <META http-equiv="refresh" content="0;URL=$HTTP_PORTAIL/cgi-bin/nojavascript.cgi">
</NOSCRIPT>
<SCRIPT language="JavaScript" src="$URL_CONF/super.js?bld=$build"></SCRIPT>
<SCRIPT language="JavaScript" src="$URL_CONF/toucheF5.js"></SCRIPT>
<SCRIPT language="JavaScript">
// Masquage du message d'attente en cas d'arr�t du chargement de la page
// Ne fonctionne pas sous Netscape
function hideMessage() {
    document.getElementById('patience').style.display = 'none';
}

document.onstop = hideMessage;
</SCRIPT>

</HEAD>

<BODY $toucheF5 onLoad="document.getElementById('patience').style.display = 'none';" style="$styleBody">
<SCRIPT language="JavaScript" src="$URL_CONF/wz_tooltip.js"></SCRIPT>
<!-- Message d'attente -->
<DIV id="patience" style="position: absolute; top: 0px; left: 0px; padding: 2px 5px; border-bottom: 1px solid infotext; background-color: infobackground; color: infotext;">
<TABLE id="tab_patience" name="tab_patience" style="width: 100%; margin: 0px $margin;">
    <TR>
        <TD align="left">$mess[7050]</TD>
        <TD align="right"><IMG src="$URL_IMG/progressbar.gif" alt="$mess[7051]" title="$mess[7051]" align="absmiddle"></TD>
    </TR>
</TABLE>
</DIV>

<!-- Barre de titre -->
<TABLE bgcolor="C6CDC5" border="0" cellspacing="0" cellpadding="0" width="100%">
    <TR>
        <TD align="left" width="27" style="padding-left: 5px"><IMG src="$URL_IMG/minilogo.svg" alt=""></TD>
        <TD align="left" style="padding-left: 10px"><FONT class="TITRE">$param[1]</FONT></TD>
        $dev
        $tps_exec
        $entete
    </TR>
</TABLE>

<!-- Nom d'utilisateur -->
<FONT><B>$param[2] $param[3]</B></FONT>

<!-- Corps de la page -->
};
}

sub Pied {
	my $tps = $_[0];
	
	# Affichage du temps d'ex�cution sur les serveurs de test
	if ($G_ENVIR ne '') {
		print qq|
<!-- Affichage du temps d'ex�cution -->
<script language="JavaScript">
document.getElementById("tps_exec").innerHTML = "Page g�n�r�e en $tps s";
</script>
	|;
	}
	print qq|

</body>
</html>
	|;
	
	# Fichier trace du temps d'ex�cution
	&trace_temps(@_);
}

# Fichier trace du temps d'ex�cution
# Param�tres :
# 	- Temps d'ex�cution (en secondes)
# 	- Variable du CGI
sub trace_temps {
	my $temps = shift(@_);
	my $sep = ";";
	my $param = join($sep, @_);
	my ($sec, $min, $hr, $jourmr, $mois, $an, $joursem, $jouran, $ete)
		= localtime;
	my @mois_lib = ("Jan", "Fev", "Mar", "Avr", "Mai", "Jun", "Jui", "Aou", 
		"Sep", "Oct", "Nov", "Dec");
	$an += 1900;
	$jourmr = "0".$jourmr if ($jourmr < 10);
	$hr = "0".$hr if ($hr < 10);
	$min = "0".$min if ($min < 10);
	$sec = "0".$sec if ($sec < 10);
	my $date = "$jourmr $mois_lib[$mois] $an";
	my $heure = "$hr:$min:$sec";
	my $nomfich = "TraceTemps_".$mois_lib[$mois]."-".$an.".csv";
	my $rep = $PATH_COMMONFILES_LOC_LOGS;
#	my $rep = "../trace";
    &File::Path::make_path($rep);
	
#	chdir("$rep")
#		or die ("Impossible d'ouvrir le r�pertoire $rep");
	
 	open (TRACE, ">>$rep/$nomfich")
 		or die "Impossible d'�crire dans le fichier $rep/$nomfich";
 	
 	print TRACE $date.$sep.$heure.$sep.$G_PELOGIN.$sep.$G_AGENCE.$sep.$temps
 		.$sep.&nom_fichier().$sep.$param."\r\n";
 	
 	close TRACE;
 	
 	chmod(0664, "$rep/$nomfich");
}

sub trim 
{
    my @out = @_;
    for (@out) 
    {
        s/^\s+//;
        s/\s+$//;
    }
    return wantarray ? @out : $out[0];
}


1;                    
