package kimoce;

use message;
use accesbd;
use varglob;
use DBI;
use DBD::Sybase;

require Exporter;
@ISA     = qw( Exporter );
@EXPORT  = qw( 
                &situation &carac
             );

# Gestion des erreurs sur la base de Kimoce
# Affichage sur la sortie d'erreur standard
# Insertion dans le fichier log
sub err_handler {
	my($err, $sev, $state, $line, $server, $proc, $msg, $sql, $err_type) = @
_;

	my ($sec, $min, $hr, $jourmr, $mois, $an, $joursem, $jouran, $ete)
		= localtime;
	my @mois_lib = ("Jan", "Fev", "Mar", "Avr", "Mai", "Jun", "Jui", "Aou", 
		"Sep", "Oct", "Nov", "Dec");
	$an += 1900;
	$jourmr = "0".$jourmr if ($jourmr < 10);
	$hr = "0".$hr if ($hr < 10);
	$min = "0".$min if ($min < 10);
	$sec = "0".$sec if ($sec < 10);
	my $date = "$jourmr $mois_lib[$mois] $an";
	my $heure = "$hr:$min:$sec";

	my $nomfich = "Kimoce-".$mois_lib[$mois].$an.".txt";
	my $rep = "/home/web/kimoce/cgi-bin/log";
#	my $rep = "./log";

	open (TRACE, ">>$rep/$nomfich")
		or die "Impossible d'�crire dans le fichier $nomfich";

	my @msg = ();
	if($err_type eq 'server') {
		push @msg,
			('',
				'Server message',
				sprintf('Message number: %ld, Severity %ld, State %ld, Line %ld',
					$err,$sev,$state,$line),
				(defined($server) ? "Server '$server' " : '') .
					(defined($proc) ? "Procedure '$proc'" : ''),
				"Message String:$msg");
    }
    else {
		push @msg,
			('',
				'Open Client Message:',
				sprintf('Message number: SEVERITY = (%ld) NUMBER = (%ld)',
					$sev, $err),
				"Message String: $msg");
	}
	print STDERR join("\n",@msg);
	print STDERR "\n";
	
	foreach (@msg) {
		$_ =~ s/\n/ /g;
		$_ =~ s/\r/ /g;
	}
	print TRACE qq{$date $heure   }.join("\r\n\t",@msg).qq{\r\n};
	
	close TRACE;
	return 0;
}


# Mise � jour de la situation d'une machine
# Param�tre :
#       - Num�ro de la machine (MAAUTO)
#		- Logiciel (LOAUTO)
#		- Contrat (CONTRATAUTO)
#		- Tourn�e (TOAUTO)
#		- Devis (DEVISAUTO)
sub situation {
    if ($CONNEXION_KIMOCE)
    {
        # Param�tres
        my $machine  = $_[0];
        my $logiciel = $_[1];
        my $contrat  = $_[2];
        my $tournee  = $_[3];
        my $devis    = $_[4];

        # R�ussite de l'op�ration
        my $succes = 1;

        # Connexion aux bases de donn�es
        my $DB = &Environnement("LOCATION")
                or $succes = 0;
        my $DBK = DBI->connect("dbi:Sybase:".$KIMOCE_NOM{$G_PAYS}.":1433", $KIMOCE_LOGIN{$G_PAYS},
                $KIMOCE_PASSWD{$G_PAYS}, { syb_err_handler => \&err_handler })
                or $succes = 0;
        $DBK->{syb_err_handler} = \&err_handler;
        $DBK->do("use ".$KIMOCE_BASE{$G_PAYS})
                or $succes = 0;

        # Messages
        my @mess = &message($G_PAYS);

        # R�cup�ration du num�ro de parc et de la situation de la machine
        my $req_sit = qq{
                SELECT MANOPARC, ObjSitInCde, MACHINE.ETCODE
                FROM AUTH.`MACHINE`
                LEFT JOIN ETATTABLE ON MACHINE.ETCODE = ETATTABLE.ETCODE
                WHERE MAAUTO = '$machine'
        };
        my $sth = $DB->prepare("$req_sit")
                or $succes = 0;
        $sth->execute();
        my ($parc, $situation, $etcode) = $sth->fetchrow_array();
        $sth->finish;

        # Mise � jour de la situation dans Kimoce
		my $upd = qq{
			UPDATE p_dos SET ObjSitInCde = '$situation' WHERE ObjIdentVal = '$parc'
		};
		$succes = $DBK->do("$upd") if ($succes);

		$succes = ($succes > 1) ? 1 : $succes;

#        print "$upd<br>";
#        print "$succes\n";

        $DBK->disconnect;
    }
} 

# Mise � jour des caract�riqtiques d'une machine
# Param�tre :
#       - Num�ro de la machine (MAAUTO)
#		- Logiciel (LOAUTO)
#		- Contrat (CONTRATAUTO)
#		- Tourn�e (TOAUTO)
#		- Devis (DEVISAUTO)
sub carac {
    if ($CONNEXION_KIMOCE)
    {
        # Param�tres
        my $machine  = $_[0];
        my $logiciel = $_[1];
        my $contrat  = $_[2];
        my $tournee  = $_[3];
        my $devis    = $_[4];

        # R�ussite de l'op�ration
        my $succes = 1;

        # Connexion aux bases de donn�es
        my $DB = &Environnement("LOCATION")
                or $succes = 0;
        my $DBK = DBI->connect("dbi:Sybase:".$KIMOCE_NOM{$G_PAYS}.":1433", $KIMOCE_LOGIN{$G_PAYS},
                $KIMOCE_PASSWD{$G_PAYS}, { syb_err_handler => \&err_handler })
                or $succes = 0;
        $DBK->{syb_err_handler} = \&err_handler;
        $DBK->do("use ".$KIMOCE_BASE{$G_PAYS})
                or $succes = 0;

        # Messages
        my @mess = &message($G_PAYS);

        # R�cup�ration du num�ro de parc et des caract�ristiques de la machine
        my $req_etat = qq{
                SELECT LIETCODE
                FROM AUTH.`MACHINE`
                WHERE MACHINE.MAAUTO = '$machine'
        };
        my $sth = $DB->prepare("$req_etat")
                or $succes = 0;
        $sth->execute();
        my ($etat) = $sth->fetchrow_array();
        $sth->finish;

        # R�cup�ration du num�ro de parc et des caract�ristiques de la machine
        my $req_car;
        # Machine lou�e ou en r�cup : r�cup�ration des infos du chantier
        if ($etat == 1 || $etat == 3 || $etat == 12) {
	        $req_car = qq{
	                SELECT MANOPARC, CHADRESSE, CHLIBELLE, CLSTE, CHCONTACT,
	                	CHTELEPHONE, VINOM, VICP, DATE_FORMAT(MADATEPANNE, '%Y%m%d'),
	                	DATE_FORMAT(CONTRATDD, '%Y%m%d'), DATE_FORMAT(CONTRATDR, '%Y%m%d'), IF(CONTRATFULLSERVICE=-1,"Oui","Non")
	                FROM AUTH.`MACHINE`
	                LEFT JOIN CONTRAT ON MACHINE.CONTRATAUTO = CONTRAT.CONTRATAUTO
	                LEFT JOIN CHANTIER ON CONTRAT.CHAUTO = CHANTIER.CHAUTO
	                LEFT JOIN CLIENT ON CONTRAT.CLAUTO = CLIENT.CLAUTO
	                LEFT JOIN VILLECHA ON CHANTIER.VILLEAUTO = VILLECHA.VILLEAUTO
	                WHERE MACHINE.MAAUTO = "$machine"
	        };
	    }
	    # Machine non lou�e : les champs sont laiss�s vides
        else {
	        $req_car = qq{
	                SELECT MANOPARC, "", "", "", "", "", "", "", DATE_FORMAT(MADATEPANNE, '%Y%m%d'), "", "", ""
	                FROM AUTH.`MACHINE`
	                WHERE MACHINE.MAAUTO = "$machine"
	        };
	    }
        my $sth4 = $DB->prepare("$req_car")
                or $succes = 0;
        $sth4->execute();
        my %carac;
        my $parc;
        ($parc, $carac{"ADR"}, $carac{"CHA"}, $carac{"CLI"}, $carac{"CTC"},
        		$carac{"TEL"}, $carac{"VIL"}, $carac{"CP"}, $carac{"DTEPA"},
        		$carac{"DTEDC"}, $carac{"DTEFC"}, $carac{"CONFS"}) =
        	$sth4->fetchrow_array();
        $sth4->finish;

        # R�cup�ration identifiant Kimoce de la machine
        my $req_dosincde = qq{
                SELECT DosInCde
                FROM p_dos
                WHERE ObjIdentVal = '$parc'
        };
        my $sth2 = $DBK->prepare("$req_dosincde")
                or $succes = 0;
        $sth2->execute();
        my ($DosInCde) = $sth2->fetchrow_array();
        $sth2->finish;

        # Mise � jour des caract�ristiques
        foreach (keys %carac) {
	        # R�cup�ration identifiant Kimoce de la caract�ristique
	        my $req_charincde = qq{
				SELECT CharInCde
				FROM r_char
				WHERE CharExCde = '$_'
	        };
	        my $sth3 = $DBK->prepare("$req_charincde")
	                or $succes = 0;
	        $sth3->execute();
	        my ($CharInCde) = $sth3->fetchrow_array();
	        $sth3->finish;

	        # Mise � jour de la caract�ristique dans Kimoce
	        $carac{$_} =~ s/\n/\\n/g;
	        $carac{$_} =~ s/\r/\\r/g;
	        $carac{$_} =~ s/\"/'/g;
			my $upd = qq{
				UPDATE p_objchar
				SET ObjCharVal = '}.substr($carac{$_}, 0, 50).qq{'
				WHERE CharInCde = '$CharInCde'
					AND DosInCde = '$DosInCde'
			};
			$succes = $DBK->do("$upd") if ($succes && $DosInCde && $CharInCde);
			$succes = ($succes > 1) ? 1 : $succes;
#			print "$upd<br>";
		}

#        print "$succes\n";

        $DBK->disconnect;
    }
} 

1;
