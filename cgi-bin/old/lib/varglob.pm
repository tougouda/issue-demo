package varglob;

require Exporter;
@ISA		= qw(Exporter);
@EXPORT		= qw(
    $SRV1_NOM $SRV1_DATASRC $SRV1_LOGIN $SRV1_PASSWD
    $SRV1_STATS_NOM $SRV1_STATS_DATASRC $SRV1_STATS_LOGIN $SRV1_STATS_PASSWD
    $HTTP_PORTAIL $CH_BASE $PATH_COMMONFILES_LOC_LOGS %CONNECTION $URL_IMG
    $URL_CONF %KIMOCE_NOM %KIMOCE_BASE %KIMOCE_LOGIN
    %KIMOCE_PASSWD %GESCOM_BASE %GESTIONTRANS_BASE $isIE
    $G_ENVIR $CONNEXION_KIMOCE $G_DEV
);
use strict;
use vars qw(
    $SRV1_NOM $SRV1_DATASRC $SRV1_LOGIN $SRV1_PASSWD
    $SRV1_STATS_NOM $SRV1_STATS_DATASRC $SRV1_STATS_LOGIN $SRV1_STATS_PASSWD
    $HTTP_PORTAIL $CH_BASE $PATH_COMMONFILES_LOC_LOGS %CONNECTION $URL_IMG
    $URL_CONF %KIMOCE_NOM %KIMOCE_BASE %KIMOCE_LOGIN
    %KIMOCE_PASSWD %GESCOM_BASE %GESTIONTRANS_BASE $isIE
    $G_ENVIR $CONNEXION_KIMOCE $G_DEV
);

use lib '../../inc', '../../lib';

use LOC::Globals;

$isIE  = ($ENV{'HTTP_USER_AGENT'} =~ /MSIE/);



my $env = &LOC::Globals::getEnv();
$G_ENVIR        = ($env eq 'exp' ? '' : $env);
$HTTP_PORTAIL   = &LOC::Globals::get('locationUrl');   # url de base
$URL_IMG        = $HTTP_PORTAIL . '/web/old/i';
$URL_CONF       = $HTTP_PORTAIL . '/web/old/c';

$PATH_COMMONFILES_LOC_LOGS = &LOC::Globals::get('commonFilesPath') . '/logs/old';

$CH_BASE        = &LOC::Globals::get('cgiOldPath');


# Serveurs de base de donn�es
%CONNECTION = ();

# - Principale
my $dbInfos = {
    'FR'  => &LOC::Globals::getConnParam('location', 'FR'),
    'EXT' => &LOC::Globals::getConnParam('location', 'EXT'),
    'ES'  => &LOC::Globals::getConnParam('location', 'ES'),
    'PT'  => &LOC::Globals::getConnParam('location', 'PT'),
    'MA'  => &LOC::Globals::getConnParam('location', 'MA')
};

$SRV1_NOM       = $dbInfos->{'FR'}->{'host'};        # Nom du serveur
$SRV1_DATASRC   = $dbInfos->{'FR'}->{'dsn'};         # Data source
$SRV1_LOGIN     = $dbInfos->{'FR'}->{'username'};    # Nom d'utilisateur
$SRV1_PASSWD    = $dbInfos->{'FR'}->{'password'};    # Mot de passe

foreach my $countryId ('FR', 'EXT', 'ES', 'PT', 'MA')
{
    $CONNECTION{'LOCATION'}->{$countryId} = {
        'login'  => $dbInfos->{$countryId}->{'username'},
        'passwd' => $dbInfos->{$countryId}->{'password'},
        'datsrc' => $dbInfos->{$countryId}->{'dsn'}
    };
}


# - Statistiques
my $statsDbInfos = {
    'FR'  => &LOC::Globals::getConnParam('location-stats', 'FR'),
    'EXT' => &LOC::Globals::getConnParam('location-stats', 'EXT'),
    'ES'  => &LOC::Globals::getConnParam('location-stats', 'ES'),
    'PT'  => &LOC::Globals::getConnParam('location-stats', 'PT'),
    'MA'  => &LOC::Globals::getConnParam('location-stats', 'MA')
};

$SRV1_STATS_NOM     = $statsDbInfos->{'FR'}->{'host'};        # Nom du serveur de stats
$SRV1_STATS_DATASRC = $statsDbInfos->{'FR'}->{'dsn'};         # Data source
$SRV1_STATS_LOGIN   = $statsDbInfos->{'FR'}->{'username'};    # Nom d'utilisateur
$SRV1_STATS_PASSWD  = $statsDbInfos->{'FR'}->{'password'};    # Mot de passe

foreach my $countryId ('FR', 'EXT', 'ES', 'PT', 'MA')
{
    $CONNECTION{'STATS'}->{$countryId} = {
        'login'  => $statsDbInfos->{$countryId}->{'username'},
        'passwd' => $statsDbInfos->{$countryId}->{'password'},
        'datsrc' => $statsDbInfos->{$countryId}->{'dsn'}
    };
}


# - Kimoce
%KIMOCE_NOM = ();
%KIMOCE_BASE = ();
%KIMOCE_LOGIN = ();
%KIMOCE_PASSWD = ();

my $kimoceDbInfos = &LOC::Globals::getConnParam('kimoce');

foreach my $countryId ('FR', 'EXT', 'ES', 'PT', 'MA')
{
    $KIMOCE_NOM{$countryId}    = $kimoceDbInfos->{'host'};
    $KIMOCE_BASE{$countryId}   = $kimoceDbInfos->{'schema'};
    $KIMOCE_LOGIN{$countryId}  = $kimoceDbInfos->{'username'};
    $KIMOCE_PASSWD{$countryId} = $kimoceDbInfos->{'password'};
}


# - Tarifications location et transport
%GESCOM_BASE = ();
%GESTIONTRANS_BASE = ();

foreach my $countryId ('FR', 'ES', 'PT', 'MA')
{
    my $rentTariffDbInfos = &LOC::Globals::getConnParam('gestiontarif', $countryId);
    $GESCOM_BASE{$countryId}       = $rentTariffDbInfos->{'schema'};

    my $tspTariffDbInfos = &LOC::Globals::getConnParam('gestiontrans', $countryId);
    $GESTIONTRANS_BASE{$countryId} = $tspTariffDbInfos->{'schema'};
}


# TODO: Passer en caract�ristique
$CONNEXION_KIMOCE = &LOC::Globals::get('connectToKimoce');


# Num�ro de l'environnement de d�v
$G_DEV = &LOC::Globals::get('dev');

1;
