package infobase;

require Exporter;
@ISA =qw(Exporter SelfLoader);
@EXPORT =qw(ville);
use strict;
use DBI;
use accesbd;
use varglob;
use message;

my @mess = &message($G_PAYS);


sub ville {
	my $sth;
	my $ville=$_[0];	
	my $req;
	my $nombre;
	my $boole;
#	$req=qq{select CORLIBELLE FROM VILLECHA LEFT join CORRESPONDANCE ON CORRESPONDANCE.CORAUTO=VILLECHA.CORAUTO WHERE VILLEAUTO = "$ville" };
	$req=qq{select CORLIBELLE FROM VILLECHA LEFT join CORRESPONDANCE ON CORRESPONDANCE.CORAUTO=VILLECHA.DEPVILLE WHERE VILLEAUTO = "$ville" };
	my $DB = &Environnement("LOCATION");
	$sth=$DB->prepare("$req");
	$sth->execute();
	$nombre =$sth->rows;
	my $lib;
	my $nb=1;
	while (my @data=$sth->fetchrow_array()) {
		$lib .= $data[0];
		if ($nb!=$nombre) {
			$lib .= " ".$mess[1001]." ";
		}
		$nb++;
	};
	$sth->finish;
	if ($lib) {
		$boole=", $mess[1000] $lib";	
	}
	$DB->disconnect;
	return  $boole;
}

1;
