package titanic;
require 	Exporter;
require 	SelfLoader;
@ISA 		= qw(Exporter SelfLoader);
 #@ISA 		= qw(Exporter);
@EXPORT 	= qw(
                     alerte_pl_ms alerte_null
                    );

use varglob; 
use CGI;
use DBI;  
#use lectdon; 
use autentif;


# alerte_pl_ms (agence,base,numero champ,delta)  delta=+-1;
sub  alerte_pl_ms {
	my @param=@_;
	my $agenceal=$param[0];
	my $baseal=$param[1];
	my $champal=$param[2];
	my $epsilon=$param[3];
	if ($epsilon >= 0) {
	 	$epsilon=qq{+$epsilon};
	}
	my $reqal;
	if ($agenceal =~ /ALL/) {
		my @ag=();
		my $ps=substr($agenceal,3,2);		
		my $where="";
		if ($ps ne "") {$where=qq{ WHERE PACODE="$ps"}};
		my $rq=qq{select AGAUTO FROM AUTH.`AGENCE` $where};
		my $sth=$baseal->prepare($rq);
		$sth->execute();
		while (my @data=$sth->fetchrow_array()) {
			push @ag , $data[0];
		}
		$sth->finish;
		for(my $i=0;$i<@ag;$i++) {
		      	$reqal=qq{UPDATE AUTH.`ALERTE` SET AL$champal=AL$champal$epsilon WHERE AGAUTO="$ag[$i]"};	 
			$baseal->do($reqal) || die "alterte $reqal\n";
	      	}	
	} else {
		$reqal=qq{UPDATE AUTH.`ALERTE` SET AL$champal=AL$champal$epsilon WHERE AGAUTO="$agenceal"};
		$baseal->do("$reqal") || die "alterte $reqal\n";
	}
}

sub  alerte_null {
	my @param=@_;
	my $agenceal=$param[0];
	my $baseal=$param[1];
	my $champal=$param[2];
	my $reqal;
	if ($agenceal =~ /ALL/) {
	 	my @ag=();
		my $ps=substr($agenceal,3,2);		
		my $where="";
		if ($ps ne "") {$where=qq{ WHERE PACODE="$ps"}};
		my $rq=qq{select AGAUTO FROM AUTH.`AGENCE` $where};
		my $sth=$baseal->prepare($rq);
		$sth->execute();
		while (my @data=$sth->fetchrow_array()) {
			push @ag , $data[0];
		}
		$sth->finish;
		for(my $i=0;$i<@ag;$i++) {
		      	$reqal=qq{UPDATE AUTH.`ALERTE` SET AL$champal=0 WHERE AGAUTO="$ag[$i]"};
			$baseal->do($reqal) || die "alterte $reqal\n";
	      	}	
	} else {
		$reqal=qq{UPDATE AUTH.`ALERTE` SET AL$champal=0 WHERE AGAUTO="$agenceal"};
		$baseal->do($reqal) || die "alterte $reqal\n";
	}
}

1;
