# $Id: tracetemps.pm,v 1.1.1.1 2006/08/01 07:48:07 clliot Exp $
# $Log: tracetemps.pm,v $
# Revision 1.1.1.1  2006/08/01 07:48:07  clliot
# no message
#
# Revision 2.1  2005/05/03 09:41:08  julien
# Tra�age du temps d'ex�cution des scripts.
#

package tracetemps;

use message;
use accesbd;
use varglob;
use DBI;

#require 'sys/syscall.ph';
use Time::HiRes;
require Exporter;
@ISA     = qw( Exporter );
@EXPORT  = qw( &init_chrono &stop_chrono &nom_fichier );

# Initialisation du chrono
sub init_chrono
{
    return [&Time::HiRes::gettimeofday()];
}

# Arr�t du chrono
sub stop_chrono
{
    my ($timer) = @_;

    return &Time::HiRes::tv_interval($timer, [&Time::HiRes::gettimeofday()]);
}

# R�cup�ration du nom de fichier ex�cut�
sub nom_fichier {
	my @fichier_temp = split(/\//, $0);
	my $fichier = $fichier_temp[@fichier_temp - 1];
	
	return $fichier;
}

1;
