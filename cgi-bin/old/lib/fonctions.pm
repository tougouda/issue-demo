package fonctions;

use lib '../../inc', '../../lib';

use message;
use accesbd;
use varglob;
use clemail;
use calculdate;
use Mail::Sender;
use tracetemps;
use MIME::Base64;
use File::Path;
use LOC::Insurance::Type;
use LOC::Json;

require Exporter;
@ISA     = qw( Exporter );
@EXPORT  = qw(
    &fonct &get_date &verif_droits &aff_assurance &infoclch &legende_clients
    &accentsHTML &mail &contratvalid &calendrier &personnel &liste_pays
    &maj_stock &cont_al &couleurClient &traceBlocageClients
    &isSubmitBtnClicked
);

# Retourne l'�tat du client
# param�tre 1 : code collectif du client
# param�tre 2 : CLSAGE (etat de blocage du client)
# en sortie parametre 1 : 0 : rien de particulier, client noir
#                         1 : en rouge, client bloqu� au contrat
#                         2 : en vert, client bloqu� au devis et au contrat
#                         3 : en vert mais prolongation sur contrat <=5 jours autoris�e
sub couleurClient
{
    my ($collectif, $blocage) = @_;

    # clients nationaux : collectifs en 4111111, 4112222 et 4113333
    # -> client en noir
#     if ($collectif =~ /4111111/ || $collectif =~ /4112222/ || $collectif =~ /4113333/)
#     {
#         return 0;
#     }

    # collectif en 4116999
    # -> client en rouge
    if ($collectif =~ /4116999/)
    {
        return 1;
    }

    # collectif en 4106, 4107, 4108, 4116, 4117, 4118
    # -> client en vert
    if ($collectif =~ /41[01][678][0-9]{3}/)
    {
        return 2;
    }

    # collectif en 4100700, 4100710 et 4100711
    # -> client en vert
    if ($collectif =~ /410071[01]/ || $collectif =~ /4100700/)
    {
        return 2;
    }

    # collectif en 4114
    # -> client vert
    if ($collectif =~ /4114[0-9]{3}/)
    {
        return 2;
    }
    # collectif en 4119999 (bloqu�s administratif)
    # -> client en vert
    if ($collectif =~ /4119999/)
    {
        return 2;
    }

    # si ca ne rentre dans aucun des autres cas
    # si il est bloqu� : rouge sinon noir
    if ($blocage eq "-2")
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

sub fonct {
	if ($_[0]==0) {
		@tableau = split(/,/, $_[1]);
		if ($tableau[0]==5) {
			return &get_date($tableau[0],$tableau[1],$tableau[2]);
		} else {
			return &get_date($_[1]);
		}
	}
}

sub get_date {

    # Definie le tableau des jours de la semaine et mois de l'ann�e.         #
    @days   = ('Dimanche','Lundi','Mardi','Mercredi',
               'Jeudi','Vendredi','Samedi');
    @months = ('Janvier','F�vrier','Mars','Avril','Mai','Juin','Juillet',
	         'Aout','Septembre','Octobre','Novembre','D�cembre');

    # R�cup�re la date courante et formatte l'heure, minutes et secondes.    #
    # Ajoute 1900 � l'ann�e pour avoir les 4 chiffres de l'ann�e.            #
    ($sec,$min,$hour,$mday,$mon,$year,$wday) = (localtime(time))[0,1,2,3,4,5,6];
    $year += 1900;
    $time = sprintf("%02d:%02d:%02d",$hour,$min,$sec);

    # Formate la date.                                                       #
    if ($_[0]==2) {

    	# Retourne par ex: lundi, 2 avril, 2001 at 10:52:04

    	return $da = "$days[$wday], $mday $months[$mon] , $year at $time";
    } else {
    	$mday = "0".$mday if ($mday<10);
    	$mon  = "0".$mon  if ($mon<10);
    	$mon++;
    	if ($_[0]==1) {

    		# Retourne la date sous le format 02/04/2001

    		return $da = "$mday/$mon/$year";
    	} elsif ($_[0]==3) {

    		# Retourne la date sous le format 0204
    		# ceci �tant pour une gestion de cl� auto (TON0204L1)

    		return $da= "$mday$mon";
    	} elsif ($_[0]==4) {

    		# Retourne la date sous le format 2001/04/02

    		return $da= "$year/$mon/$mday";
     	} elsif ($_[0]==5) {

     		# Appel de la fonction se faisant comme suit
     		# &get_date(5,$ladate,"s�parateur") par ex:
     		# &get_date(5,"2001-04-02","-") et retourne
     		# 02/04/2001 et inversement
     		$_[3] = "/" if !$_[3];
     		@temporaire=split(/$_[2]/, $_[1]);
     		$temporaire2=$temporaire[0];
     		$temporaire[0]=$temporaire[2];
     		$temporaire[2]=$temporaire2;
     		return $da=join($_[3], @temporaire);
     	} elsif ($_[0]==6) {

    		# Retourne la date sous le format 20010402

    		return $da= "$year$mon$mday";
     	}

    }
}

# type d'affichage
#   2 : affichage "consultation"
#   4 : condens�
#   6 : affichage de l'assurance actuelle en liste d�roulante non modifiable
#   7 : liste d�roulante avec possibilit� de modifier le pourcentage et le type
sub aff_assurance {
    my ($nom_ass, $val, $type_aff, $assp, $ass_modif, $date1, $date2, $disabled) = @_;

    if ($type_aff != 7 && !$val)
    {
        $val = &LOC::Insurance::Type::getDefaultId($G_PAYS);
    }

    if ($type_aff eq "")
    {
       $type_aff = 0;
    }

    my $DB = &Environnement("LOCATION");
    my @mess = &message($G_PAYS);
    my %type_ass;
    my $texte;
    my @tab_assp = split(/:/, $G_TX_ASS);
    my @dt_val = split (/-/, $date1);
    my @dt_deb = split (/-/, $date2);

    my $tabInsuranceTypesList = &LOC::Insurance::Type::getList($G_PAYS, LOC::Util::GETLIST_ASSOC);
    my @tabInsuranceTypes= values(%$tabInsuranceTypesList);
    my $tabInsuranceTypeInfos;
    if ($val)
    {
        $tabInsuranceTypeInfos = (defined $tabInsuranceTypesList->{$val} ?
                                    $tabInsuranceTypesList->{$val} : 
                                    &LOC::Insurance::Type::getInfos($G_PAYS, $val));
    }

    my $display  = ($tabInsuranceTypeInfos && $tabInsuranceTypeInfos->{'isCustomer'} ? 'inline' : 'none');
    my $displayp = ($tabInsuranceTypeInfos && $tabInsuranceTypeInfos->{'isInvoiced'} ? 'inline' : 'none');

    my $insuranceRateLabel;
    if ($assp ne "")
    {
        $insuranceRateLabel = $assp * 100  . ' %';
    }
    else
    {
        $insuranceRateLabel = "";
    }

    # Affichage consultation contrat/devis
    if ($type_aff == 2)
    {
        $texte = $tabInsuranceTypeInfos->{'fullLabel'};
        $texte =~ s/<%percent>/$insuranceRateLabel/g;
    }
    # Affichage consultation contrat/devis
    elsif ($type_aff == 4)
    {
        $texte = $tabInsuranceTypeInfos->{'label'};
        $texte =~ s/<%percent>/$insuranceRateLabel/g;
    }
    # Affichage assurance actuelle en liste d�roulante non modifiable
    elsif ($type_aff == 6)
    {
        my $disp = "none";
        my $nb = 0;

        $texte .= '
            <input type="hidden" name="' . $nom_ass . '_old" id="' . $nom_ass . '_old" value="' . $val . '" />
            <select name="' . $nom_ass . '" id="' . $nom_ass . '" ' . $disabled . ' value="' . $val . '">';

        # - Affichage si le type d'assurance n'est pas dans la liste
        if ($val && !defined $tabInsuranceTypesList->{$val})
        {
            my $label = $tabInsuranceTypeInfos->{'fullLabel'};
            $label =~ s/<%percent>/%/g;
            $texte .= '
                <option value="' . $tabInsuranceTypeInfos->{'id'} . '" selected>' . $label . '</option>';
            $nb++;
        }
        foreach my $insuranceTypeInfos (@tabInsuranceTypes)
        {
            my $label = $insuranceTypeInfos->{'fullLabel'};
            $label =~ s/<%percent>/%/g;
            $texte .= '
                <option value="' . $insuranceTypeInfos->{'id'} . '"' . ($val == $insuranceTypeInfos->{'id'}? ' selected' : '') . '>' . $label . '</option>';
            $nb++;
        }
        $texte .= '
            </select> <b>' . $nb . '</b> ';
;
        # Selection du pourcentage de l'assurance actuelle
        my $nb = 0;
        $texte .=  '
            <select name="clpassurance" id="clpassurance" value="' . $assp . '"
                    style="display:' . $displayp . ';"
                    ' . $disabled . '>';
        for (my $i = 0; $i < @tab_assp; $i++)
        {
            $texte .= '
                <option value="' . $tab_assp[$i] . '"' . ($tab_assp[$i] eq $assp ? ' selected' : '') . '>' . $tab_assp[$i]*100 . '%</option>';
            $nb++;
        }
        if ($G_PAYS eq "FR")
        {
            $disp = 'inline';
        }
        $texte .= '
            </select> <b style="display:' . $displayp . ';">' . $nb . '</b>';

        $texte .= '
            <br/>
            <div id="assfinval" style="display:' . $display . ';">
                <input type="text" name="cldatevalass0" value="' . $dt_val[2] . '" size="1" maxlength="2"/> /
                <input type="text" name="cldatevalass1" value="' . $dt_val[1] . '" size="1" maxlength="2"/> /
                <input type="text" name="cldatevalass2" value="' . $dt_val[0] . '" size="2" maxlength="4"/>
                ' . $mess[2371] . '
            </div>';
    }
    # Affichage liste d�roulante avec possibilit� de modifier le pourcentage et le type
    elsif ($type_aff == 7)
    {
        my $disp = "none";
        my $nb   = 0;

        # - Liste des types d'assurance
        my $jsBlock = '
<script type="text/javascript">
    var elt = document.getElementById("' . $nom_ass . '");
    var tabInsuranceTypes = ' . &LOC::Json::toJson(\@tabInsuranceTypes) . ';

    elt.onchange = function() {
        var value = this.value;
        var clnvpassuranceElt = document.getElementById("clnvpassurance");

        if (this.options[this.selectedIndex].value != 0)
        {
            document.getElementById("assdebval").style.display = "inline";
        }
        else
        {
            document.getElementById("assdebval").style.display = "none";
            clnvpassuranceElt.value = "0.00";
        }
        document.getElementById("ass_modif").value = 1;

        var tabInsuranceTypeInfos = tabInsuranceTypes.find(function(item){
            return (item.id == value);
        });
        clnvpassuranceElt.style.display = clnvpassuranceElt.nextElementSibling.style.display = (tabInsuranceTypeInfos && tabInsuranceTypeInfos.isInvoiced ? "inline" : "none");

        var old_value = document.getElementById("' . $nom_ass . '_old").value;
        if (old_value == 0 && this.options[this.selectedIndex].value != 0)
        {
            if (document.getElementById("cldatedebass0")) document.getElementById("cldatedebass0").value = "00";
            if (document.getElementById("cldatedebass1")) document.getElementById("cldatedebass1").value = "00";
            if (document.getElementById("cldatedebass2")) document.getElementById("cldatedebass2").value = "0000";
        }

        displayAppeal(tabInsuranceTypes, document.getElementById("cltypeassurance").value, value);
    };
</script>';

        $texte .= '
            <input type="hidden" name="' . $nom_ass . '_old" id="' . $nom_ass . '_old" value="' . $val . '" />';
        $texte.= '
            <select name="' . $nom_ass . '" id="' . $nom_ass . '">
                <option value="0"' . (!$val ? ' selected' : '') . '>' . $mess[7501] . '</option>';

        if ($val && !defined $tabInsuranceTypesList->{$val})
        {
            my $label = $tabInsuranceTypeInfos->{'fullLabel'};
            $label =~ s/<%percent>/%/g;
            $texte .= '
                <option value="' . $tabInsuranceTypeInfos->{'id'} . '" selected>' . $label . '</option>';
            $nb++;
        }
        foreach my $insuranceTypeInfos (@tabInsuranceTypes)
        {
            my $label = $insuranceTypeInfos->{'fullLabel'};
            $label =~ s/<%percent>/%/g;
            $texte .= '
                <option value="' . $insuranceTypeInfos->{'id'} . '"' . ($val == $insuranceTypeInfos->{'id'} ? ' selected' : '') . '>' . $label . '</option>';
            $nb++;
        }
        $texte .= '
            </select> <b>' . $nb . '</b>';
        $texte .= $jsBlock;

        # Selection du pourcentage de l'assurance souhait�e
        my $nb = 0;
        $texte .=  '
            <select name="clnvpassurance" id="clnvpassurance" style="display:' . $displayp . ';">';
        for(my $i = 0; $i < @tab_assp; $i++)
        {
            $texte .= '
                <option value="' . $tab_assp[$i] . '"' . ($tab_assp[$i] eq $assp ? ' selected': '') . '>'. $tab_assp[$i]*100 . '%</option>';
            $nb++;
        }

        if ($val)
        {
             $disp = 'inline';
        }
        $texte .='
            </select> <b style="display:' . $displayp . ';">' . $nb . '</b>
            <div id="assdebval" style="display:' . $disp . '">
                <input type="text" name="cldatedebass0" value="' . $dt_deb[2] . '" size="1" maxlength="2"/> /
                <input type="text" name="cldatedebass1" value="' . $dt_deb[1] . '" size="1" maxlength="2" /> /
                <input type="text" name="cldatedebass2" value="' . $dt_deb[0] . '" size="2" maxlength="4" />
                <i>' . $mess[3035] . '</i>
            </div>';
    }
    else
    {
        $texte = "";
    }
    if (grep {$_ == $type_aff } (0, 6, 7))
    {
        $texte .= '
            <input type="hidden" name="ass_modif" id="ass_modif" value="' . $ass_modif . '">';
    }
    return $texte;
}


#
# infoclient
#
sub infoclch {
## Prends 2 param�tres :
# CLAUTO
# VILLEAUTO
## Retourne un tableau
# 0 : Cat�gorie tarifaire
# 1 : Pays du chantier
# 2 : Contrat cadre
# 3 : Assurance
# 4 : Paiement � la livraison
# 5 : N� de commande obligatoire
# 6 : BC � fournir
# 7 : Conditions g�n�rales de vente
# 8 : code client du client
# 9 : classe du client
# 10 : potentiel du client
	my $clauto=$_[0];
	my $villeauto=$_[1];
	my $DB=&Environnement("LOCATION");
	if ($clauto ne "") {
		my $req=qq|SELECT CLCADRE, CLTYPEASSURANCE, TARIFCLIENTREF, CLPROFORMA, CLNUMCMDOBLI, BCJOIN, CLCGVENTE,CLCODE,CLCLASSE, CLPOTENTIEL, CCAUTO FROM CLIENT  LEFT join CLIENTTARIF ON CLIENTTARIF.CLIENTTARIFAUTO=CLIENT.CLIENTTARIFAUTO WHERE CLAUTO="$clauto"|;
		my $sth=$DB->prepare("$req");
		$sth->execute();
		($cadre, $cltypeassurance, $tarif, $clproforma, $numcommobli, $bcjoin, $clcgvente, $codeclient, $clclasse, $clpotentiel, $ccauto)=$sth->fetchrow_array();
		$sth->finish;
	}
	if ($villeauto ne "") {
		my $req=qq|SELECT VIPAYS FROM VILLECHA WHERE VILLEAUTO="$villeauto" AND VIPAYS !="$villecha{$G_PAYS}"|;
		my $sth=$DB->prepare("$req");
		$sth->execute();
		($pys)=$sth->fetchrow_array();
		$sth->finish;
	}
	return ($tarif, $pys, $cadre, $cltypeassurance, $clproforma, $numcommobli, $bcjoin, $clcgvente, $codeclient, $clclasse, $clpotentiel, $ccauto);
}

sub contratvalid {
	my $contratauto = $_[0];
	my $moremiseex;
    my $moremiseok;

	my $DB = &Environnement("LOCATION");

	if ($contratauto ne "") {
		my $req=qq{
            SELECT MOREMISEEX, MOREMISEOK
            FROM CONTRAT
            LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
            WHERE CONTRATAUTO="$contratauto"};
		my $sth = $DB->prepare("$req");
		$sth->execute();
		($moremiseex, $moremiseok) = $sth->fetchrow_array();
		$sth->finish;
	}

	my $return = 0;
	$return = 1   if ($moremiseok == -2 || ($moremiseok == 0 && $moremiseex == 0));

	return $return;
}


# Code couleur pour l'affichage des clients
sub legende_clients {
	if ($G_PAYS ne "FR") {
		return "";
	}
	my @mess=&message($G_PAYS);
	$texte  = qq{<p>};
	$texte .= qq{<table border="0" cellspacing="0">\n};

	for ($i=0; $i<2; $i++) {
		$texte .= qq{	<tr bgcolor="#C6CDC5">\n};
		$texte .= qq{		<td>&nbsp;</td>\n};
		$texte .= qq{		<td><font class="PT" color="$mess[6000+$i]">$mess[6010+$i]</font></td>\n};
		$texte .= qq{		<td>&nbsp;</td>\n};
		$texte .= qq{		<td><font class="PT">$mess[6020+$i]</font></td>\n};
		$texte .= qq{		<td>&nbsp;</td>\n};
		$texte .= qq{	</tr>\n};
	}

	$texte .= qq{</table>\n};
	return $texte;
}

1;


# Conversion des caract�res sp�ciaux en code HTML
sub accentsHTML {
	my $texte = $_[0];
	my %accents = (
		"192" => "�",
		"193" => "�",
		"194" => "�",
		"195" => "�",
		"196" => "�",
		"197" => "�",
		"198" => "�",
		"199" => "�",
		"200" => "�",
		"201" => "�",
		"202" => "�",
		"203" => "�",
		"204" => "�",
		"205" => "�",
		"206" => "�",
		"207" => "�",
		"208" => "�",
		"209" => "�",
		"210" => "�",
		"211" => "�",
		"212" => "�",
		"213" => "�",
		"214" => "�",
		"215" => "�",
		"216" => "�",
		"217" => "�",
		"218" => "�",
		"219" => "�",
		"220" => "�",
		"221" => "�",
		"222" => "�",
		"223" => "�",
		"224" => "�",
		"225" => "�",
		"226" => "�",
		"227" => "�",
		"228" => "�",
		"229" => "�",
		"230" => "�",
		"231" => "�",
		"232" => "�",
		"233" => "�",
		"234" => "�",
		"235" => "�",
		"236" => "�",
		"237" => "�",
		"238" => "�",
		"239" => "�",
		"240" => "�",
		"241" => "�",
		"242" => "�",
		"243" => "�",
		"244" => "�",
		"245" => "�",
		"246" => "�",
		"247" => "�",
		"248" => "�",
		"249" => "�",
		"250" => "�",
		"251" => "�",
		"252" => "�",
		"253" => "�",
		"254" => "�",
		"255" => "�"
	);

	# Substitution
	foreach (keys %accents) {
		$texte =~ s/$accents{$_}/&#$_;/g;
	}

	return $texte;
}


# Envoie d'un e-mail format� avec la charte graphique du portail
# Param�tres :
#	0 : Exp�diteur
#	1 : Destinataire
#	2 : Sujet
#	3 : Corps du message (format HTML)
sub mail {
	my $smtp			= $ENV{'SMTP_HOST'};
	my $expediteur		= $_[0];
	my $destinataire	= $_[1];
	my $sujet			= $_[2];
    if ($G_ENVIR ne '')
    {
        my $prefix = uc($G_ENVIR);
        $prefix =~ s/ //;
        $sujet = '[' . $prefix . ']' . $sujet;
    }
	my $corps			= $_[3];

	my $debut_html		= qq{<html>
<head>
<style type="text/css">
BODY {
	background: #EDEDE0;
	color:#000000;
	font-size:10pt;
	font-family: Arial, Helvetica, sans-serif;
FONT {font-size:10pt;}
FONT.PT {font-size:8pt;}
}
</style>
</head>

<body>
};
	my $fin_html		= qq{
</body>
</html>
};

	my $sender = new Mail::Sender{
			from => $expediteur,
			smtp => $smtp
		};

	$sender->Open({
			to		=> $destinataire,
			subject	=> $sujet,
			ctype	=> 'text/html',
			encoding=> 'quoted-printable'}
		);

	$sender->SendEnc($debut_html.$corps.$fin_html);

	$sender->Close();

	# Trace
	&trace_mail($expediteur, $destinataire, $sujet, "");
}

sub calendrier {
    my ($jour, $mois, $annee, $pays) = @_;
    my $jour_lib = &corps_jour($jour, $mois, $annee, $pays);
    my $mois_lib = &corps_mois($jour, $mois, $annee, $pays);
    $mois_lib =~ s/�/�/;
    $mois_lib =~ s/�/�/;

    my $result = qq{
	<TABLE width="100" height="120" border="0" cellspacing="0" cellpadding="0" style="table-layout: fixed;">
        <TR height="5">
            <TD width="5" style="background-color: white;"></TD>
            <TD style="background-color: white;"></TD>
            <TD width="5"></TD>
        </TR>
        <TR>
           <TD colspan="2" style="text-align: center; background-color: white; padding-bottom: 5px">
                <FONT style="font-family: Verdana, Arial, Helvetica, Sans-Serif; font-size: 8pt; font-weight: bold;">}.uc($jour_lib).qq{</FONT><BR>
                <FONT style="font-family: Impact, Arial, Helvetica, Sans-Serif; font-size: 36pt; color: red;">$jour</FONT><BR>
                <FONT style="font-family: Verdana, Arial, Helvetica, Sans-Serif; font-size: 8pt; font-weight: bold;">}.uc($mois_lib).qq{</FONT><BR>
                <FONT style="font-family: Verdana, Arial, Helvetica, Sans-Serif; font-size: 8pt; font-weight: bold;">$annee</FONT><BR>
            </TD>
            <TD width="5" style="background-color: gray;"></TD>
        </TR>
        <TR height="5">
            <TD width="5"></TD>
            <TD style="background-color: gray;"></TD>
            <TD width="5" style="background-color: gray;"></TD>
        </TR>
    </TABLE>
    };

    return $result;
}


# Informations sur un utilisateur
# Arguments :
#   - identifiant base de donn�es
#   - le PEAUTO
sub personnel {
    my ($DB, $peauto) = @_;

    my $req = qq{SELECT PEPRENOM, PENOM FROM AUTH.PERSONNEL P WHERE P.PEAUTO="$peauto" AND P.PROFILAUTO!=0};
    my $sth = $DB->prepare("$req");
    $sth->execute();
    my ($prenom, $nom) = $sth->fetchrow_array();
    $sth->finish;

    return ($prenom, $nom);
}

# Liste des pays
sub liste_pays {
    my ($DB) = @_;

    my @pays = ();
    my $req = qq{SELECT PACODE FROM AUTH.`PAYS` WHERE PANOM IS NOT NULL AND PACODE != "EXT"};
    my $sth = $DB->prepare($req);
    $sth->execute();

    while (my $data = $sth->fetchrow_hashref()) {
    	push (@pays, $data->{"PACODE"});
    }
    $sth->finish;

    return @pays;
}

sub maj_stock
{
    my $bd=&Environnement("LOCATION");

    my $i;
    my $pourcent;
    my $j;
    my $loue;
    my $trans;
    my $nbmod;
    my $veri;
    my $tran;
    my $pann;
    my $revi;
    my $disp;
    my $recu;
    my $cont;
    my $livr;
    my $nbtotal;
    my @temp;
    my $nbreservable;
    my $nbreserve;
    my %etat;
    my $verif;
    my %rend;
    my @tempbis;
    my $tempbis;
    my $temp;
    my $req;
    my @param = @_;
    my $lag = $param[0];

	$i = 0;
	$j = 1;
	$req = qq|
SELECT
    MOMAAUTO,
    STMORESERVABLE,
    STMORESERVE,
    STMOTOTAL,
    STMOLOU,
    STMODIS,
    STMOREC,
    STMOREV,
    STMOCON,
    STMOPAN,
    STMOVER,
    STMOTRA
FROM AUTH.STOCKMODELE
WHERE AGAUTO = "$lag"
ORDER BY MOMAAUTO|;

	print qq{Mise � jour des stocks de $lag :\n};
    $temp = $bd->prepare($req);
	$temp->execute();
	while (@temp = $temp->fetchrow_array())
	{
		$verif = 0;
		$etat{"MAC01"} = 0;
		$etat{"MAC02"} = 0;
		$etat{"MAC03"} = 0;
		$etat{"MAC04"} = 0;
		$etat{"MAC05"} = 0;
		$etat{"MAC06"} = 0;
		$etat{"MAC07"} = 0;
		$etat{"MAC09"} = 0;
		$etat{"MAC12"} = 0;
		$etat{"GRU11"} = 0;$|=1;
		$etat{"GRU12"} = 0;
		$etat{"GRU13"} = 0;
		$etat{"GRU14"} = 0;
		$etat{"GRU15"} = 0;
		$etat{"GRU16"} = 0;
		$etat{"GRU17"} = 0;
		$etat{"GRU19"} = 0;
		my $tabReservable = {};
		$nbreserve = 0;
		$nbreservable = 0;
		$nbtotal = 0;
		$loue = 0;
		$disp = 0;
		$recu = 0;
		$revi = 0;
		$cont = 0;
		$pann = 0;
		$veri = 0;
		$tran = 0;
		$livr = 0;
		$tempbis = $bd->prepare(qq|select ETCODE,count(*) from AUTH.MACHINE where AGAUTO="$lag" and MOMAAUTO=$temp[0] and ETCODE not in ("MAC08","GRU18") AND LIETCODE<20 group by ETCODE|);
		$tempbis->execute();
		while (@tempbis = $tempbis->fetchrow_array())
		{
			$etat{$tempbis[0]} = $tempbis[1] if $tempbis[0] =~ /MAC\d+/ || $tempbis[0] =~ /GRU\d+/;
			$verif = 1 if $tempbis[0] =~ /MAC\d+/ && $verif == 0;
            $verif = 2 if $tempbis[0] =~ /GRU\d+/ && $verif == 0;
		}
		$tempbis->finish;

        # Calcul du r�servable :
        # - le mod�le de machine est sur l'agence concern�e
        #       et il n'est pas visible sur une autre agence
        #       et il n'est pas dans l'�tat "Transfert"
        # - Ou le mod�le de machine n'est pas sur l'agence concern�e mais il est visible sur cette agence
        my $queryReservable = "
SELECT
    ETCODE,
    COUNT(*)
FROM AUTH.MACHINE
WHERE
    (AGAUTO = '" . $lag . "' AND MASUPPAGAUTO IS NULL AND ETCODE != 'MAC09'
        OR MASUPPAGAUTO = '" . $lag . "')
AND MOMAAUTO = " . $temp[0] . "
AND ETCODE NOT IN ('MAC08','GRU18')
AND LIETCODE < 20
GROUP BY ETCODE";
        my $result = $bd->prepare($queryReservable);
        $result->execute;
        while (my @tempReservable = $result->fetchrow_array())
        {
            if ($tempReservable[0] =~ /MAC\d+/ || $tempReservable[0] =~ /GRU\d+/)
            {
                $tabReservable->{$tempReservable[0]} = $tempReservable[1];
            }
            $verif = 1 if $tempReservable[0] =~ /MAC\d+/ && $verif == 0;
            $verif = 2 if $tempReservable[0] =~ /GRU\d+/ && $verif == 0;
        }
        foreach (keys %etat)
        {
            if ($verif == 1)
            {
                $tempbis = $bd->prepare(qq|select count(MOMAAUTO) FROM CONTRAT WHERE MOMAAUTO=$temp[0] AND CONTRATOK="-1" AND LIETCONTRATAUTO="1" AND (MAAUTO="0" OR MAAUTO != 0 AND CONTRATMACHATTR = 0) AND SOLOAUTO="0" AND AGAUTO="$lag"|);
                $tempbis->execute();
                ($nbreserve) = $tempbis->fetchrow_array();
                $tempbis->finish;
            }
            if (($verif == 1 && $_ =~ /\w+02/) ||
                ($verif == 2 && $_ =~ /\w+[0-9]2/) ||
                ($_ =~ /\w+[0-9]3/) ||
                ($_ =~ /\w+[0-9]4/) ||
                ($_ =~ /\w+[0-9]7/) ||
                ($_ =~ /\w+[0-9]9/))
            {
                $nbreservable += (exists $tabReservable->{$_} ? $tabReservable->{$_} : 0);
            }
            $nbtotal += $etat{$_};
        }

        if ($verif == 1)
        {
            $loue = $etat{"MAC01"};
            $disp = $etat{"MAC02"};
            $recu = $etat{"MAC03"};
            $revi = $etat{"MAC04"};
            $cont = $etat{"MAC05"};
            $pann = $etat{"MAC06"};
            $veri = $etat{"MAC07"};
            $tran = $etat{"MAC09"};
            $livr = $etat{"MAC12"};
        }

        if ($verif == 2)
        {
            $loue = $etat{"GRU11"};
            $disp = $etat{"GRU12"};
            $recu = $etat{"GRU13"};
            $revi = $etat{"GRU14"};
            $cont = $etat{"GRU15"};
            $pann = $etat{"GRU16"};
            $veri = $etat{"GRU17"};
            $tran = $etat{"GRU19"};
		}

		if ($rend{$temp[0]}{$lag}>0)
		{
			#print "rendu=$temp[0], $lag, $rend{$temp[0]}{$lag}\n";
			$nbtotal -=$rend{$temp[0]}{$lag};
			$tran -=$rend{$temp[0]}{$lag};
		}
		my $res_temp = $bd->do(qq|update AUTH.STOCKMODELE set STMORESERVABLE=$nbreservable,STMORESERVE=$nbreserve,STMOTOTAL=$nbtotal,STMOLOU=$loue,STMODIS=$disp,STMOREC=$recu,STMOREV=$revi,STMOCON=$cont,STMOPAN=$pann,STMOVER=$veri,STMOTRA=$tran, STMOLIV=$livr where MOMAAUTO=$temp[0] and AGAUTO="$lag"|);
		print ".\n";
        if ($res_temp ne "0E0")
        {
			$i++;

# 			print "\nMOMAAUTO = $temp[0]\n";
# 			print qq|RESERVABLE = $nbreservable  RESERVE = $nbreserve TOTAL = $nbtotal\nLOU = $loue  DIS = $disp  REC = $recu TRA = $tran\nREV = $revi  CON = $cont  PAN = $pann  VER = $veri\n|;
# 			print FIC "MOMAAUTO = $temp[0]  AGAUTO = $lag lignes modifi�es = $res_temp\n";
# 			print FIC qq|AVANT : RESERVABLE = $temp[1], RESERVE = $temp[2], TOTAL = $temp[3], LOU = $temp[4], DIS = $temp[5], REC = $temp[6], REV = $temp[7], CON = $temp[8], PAN = $temp[9], VER = $temp[10], TRA = $temp[11]\n|;
# 			print FIC qq|APRES : RESERVABLE = $nbreservable, RESERVE = $nbreserve, TOTAL = $nbtotal, LOU = $loue, DIS = $disp, REC = $recu, REV = $revi, CON = $cont, PAN = $pann, VER = $veri, TRA = $tran\n\n|;
		}
		$j++;
	}

	print "&nbsp;OK\n";
}

sub cont_al
{
    my $bd=&Environnement("LOCATION");

	$pays  = $_[0];
	my $ag = $_[1];
	my $pas = $pays;
	$pas =~ tr/A-Z/a-z/;
	my $al = $CH_BASE . '/commun/alerte';

	if ($pays ne "FR") {
    	$al .= $pas;
	}


	$al .= "\.cfg";
	&LectureConfig($al);
	# Alertes
	my @reqalerte;
	my @nomfich;
	my @posalerte;
	my @lienalerte;
	my @libalerte;
	my @reqoption;
	my @lienopt;
	my @affopt;
	my $nblog;
	my $nbfction = 0;
	my $nbalerte = 0;
	my $nba      = 0;
    print qq{Mise � jour des alertes de $ag :\n};
	for ($numw=0; $numw<$NbParam; $numw++)
	{
		$bloc = $LstParam[$numw];
		# bloc db
		# bloc logiciel

		if ($bloc =~ /alerte/)
		{
            my $nbrows = 0;

			$nomfich[$nba]   = $Param{$bloc}{'nom'};
			$posalerte[$nba] = $Param{$bloc}{'pos'};
			$reqalerte[$nba] = $Param{$bloc}{'requete'};
			my $r = $reqalerte[$nba];
			if ($r ne '')
			{
    			my $tmpt = $GESTIONTRANS_BASE{"$G_PAYS"};
    			$r =~ s/agence/$ag/g;
                $r =~ s/agent/$agent/;
                $r =~ s/basetrans/$tmpt/g;
    			my $str = $bd->prepare($r);
    			$str->execute();
    			$nbrows = $str->rows;
    			$str->finish;
			}
			my $aln = &nb_cont($ag,$posalerte[$nba]);

			if ($nbrows != $aln) {
				&alerte_ag($ag, $posalerte[$nba], $nbrows);
        	}

			$nba++;
		}

		my $pc = int($numw / $NbParam * 100);
		my $nb = int($numw / $NbParam * 30);
	    print qq{.\n};
	}
	print qq{OK\n};
}

# ------------------------------------------------
# LectureConfig - Lecture fichier de configuration
#
sub LectureConfig
{
	my ($FicConf) = shift (@_);
	my ($nom_bloc, $lst_bloc);
	my ($cle, $op, $data);
	($bloc);
	$NbParam = 0;
	open (CONF, $FicConf);
	undef $bloc;

	while (<CONF>) {
		s/[\012\015]/ /g;
		s/#.*//;
		s/^\s+//g;
		s/\s+$//g;
		s/\s*(\+?=|{|})\s*/$1/g;
		next unless length;
		$bloc .= $_;

		if (/}/) {
			if ($bloc =~ /([\w\-_]+){(.*)}/) {
				$nom_bloc  = $1;
				$lst_bloc  = $2;

				foreach (split(/;/, $lst_bloc)) {
					if (/(\w+)(\+?=)(.*)/) {
						# ----- x+=y ou x=y
						$cle  = $1;
						$op   = $2;
						$data = $3;
					}
					if ($nom_bloc eq "config") {
						# ----- Param. de config.
						$Config{$cle} = $data;
					}
					else {
						if ($op eq "+=") {
							$Param{$nom_bloc}{$cle} .= " $data";
						}
						else {
							$Param{$nom_bloc}{$cle} = $data;
						}
					}
				}

				$LstParam[$NbParam++] = $nom_bloc if $nom_bloc !~ /(global|config)/;
			}

			undef $bloc;
		}
	}

	close(CONF);
}

sub nb_cont
{
    my $bd=&Environnement("LOCATION");

    my @param = @_;
	my $agenceal = $param[0];
	my $champal = $param[1];
	my $reqal;
	$reqal = qq{SELECT AL$champal FROM AUTH.ALERTE WHERE AGAUTO = "$agenceal"};
	my $str = $bd->prepare($reqal);
	$str->execute();
	my $nal = $str->fetchrow_array();
	$str->finish;
	return $nal;
}

sub  alerte_ag
{
    my $bd=&Environnement("LOCATION");

	my @param = @_;
	my $agenceal = $param[0];
	my $champal  = $param[1];
	my $epsilon  = $param[2];

	if ($epsilon > 0) {
	 	$epsilon = qq{$epsilon};
	}

	my @ag = &lst_agence("ALL");
	my $reqal;

	if ($agenceal eq "ALL") {
		for(my $i=0; $i<@ag; $i++) {
			$reqal = qq{UPDATE AUTH.ALERTE SET AL$champal = "$epsilon" WHERE AGAUTO = "$ag[$i]"};
			$bd->do($reqal);
	    }
	}
	else {
		$reqal=qq{UPDATE AUTH.ALERTE SET AL$champal = "$epsilon" WHERE AGAUTO = "$agenceal"};
		$bd->do($reqal);
	}
}

sub lst_agence
{
    my $bd=&Environnement("LOCATION");

	my @param = @_;
	my $where = "";
	my @agence = ();

	if ($param[0] ne "ALL") {
		$where = qq{WHERE PACODE = "$param[0]"};
	}

	my $reqag = qq{select AGAUTO FROM AUTH.AGENCE $where ORDER BY AGAUTO};
	my $stag = $bd->prepare($reqag);
	$stag->execute();

	while (my @data = $stag->fetchrow_array()) {
         	push @agence, $data[0];
         }

	$stag->finish;
	return @agence;
}


# Fichier trace des blocages / d�blocages clients
# DE7783
# Param�tres :
# 	- CLCODE
# 	- CLAUTO
# 	- CLSAGE 1 (avant modif)
# 	- CLSAGE 2 (apr�s modif)
# 	- CLTMPUNLOCK 1 (avant modif)
# 	- CLTMPUNLOCK 2 (apr�s modif)
# 	- CLCOLLECTIF 1 (avant modif)
# 	- CLCOLLECTIF 2 (apr�s modif)
sub traceBlocageClients
{
	my $sep = ';';
	my $param = join($sep, @_);
	my ($sec, $min, $hr, $jourmr, $mois, $an, $joursem, $jouran, $ete) = localtime;
	$an += 1900;
	$mois++;
	my $date = sprintf('%02d/%02d/%04d %02d:%02d:%02d', $jourmr, $mois, $an, $hr, $min, $sec);
	my $rep = $PATH_COMMONFILES_LOC_LOGS . '/../customer';
    &File::Path::make_path($rep);
# 	print 'rep : ' . $rep;
	my $nomFich = 'LocksCustomer_' . sprintf('%04d-%02d',$an,$mois) . '.csv';
	my $nomCompletFich = $rep . '/' . $nomFich;
	my $line = '';

 	open (TRACE, ">>$nomCompletFich")
 		or die "Impossible d'�crire dans le fichier $nomFich";

    # Si le fichier est vide : �crire la ligne d'en-t�te.
    if (-z $nomCompletFich)
    {
        $line = 'DATE' . $sep . 'UTILISATEUR' . $sep . 'AGENCE' . $sep . 'CLCODE' . $sep . 'CLAUTO'
            . $sep . 'CLSAGE 1' . $sep . 'CLSAGE 2' . $sep . 'CLTMPUNLOCK 1' . $sep . 'CLTMPUNLOCK 2'
            . $sep . 'CLCOLLECTIF 1' . $sep . 'CLCOLLECTIF 2' . $sep . qq{FICHIER \r\n};

        print TRACE $line;
    }

    # Ecriture des lignes de trace
    $line = $date . $sep . $G_PELOGIN . $sep . $G_AGENCE . $sep . $param
 		. $sep . &nom_fichier() . qq{\r\n};

    print TRACE $line;

 	close TRACE;
}


# Function: isSubmitBtnClicked
# Indique si un bouton de soumission a �t� cliqu�
#
# Parameters:
# CGI    $cgi  - Instance CGI
# string $name - Nom du bouton image
#
# Returns:
# int - Valeurs de retour: 1 -> oui, 0 -> non
sub isSubmitBtnClicked
{
    my ($cgi, $name) = @_;
    return ($cgi->param($name . '.x') ne '' && $cgi->param($name . '.x') > 0 ? 1 : 0);
}


1;
