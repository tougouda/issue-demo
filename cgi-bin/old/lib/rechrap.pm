package rechrap;

require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(aff_rech);

use lib '../lib';

use strict;
use CGI;


sub aff_rech {
	my @param = @_;
	
	my $cgi = new CGI;
    my $lib = $param[0];
	my $nom = $param[1];
	my $chemin = $param[2];
	my $nav = &navigateur;
	
	my $tableau = "";
	
	$tableau .= qq{<tr bgcolor="#DEE3F6"><td nowrap align="left">\n};
	$tableau .= qq{<font class="PT"><b>$lib : </b></font><BR>\n&nbsp;<font class="TX" align="left">};
	$tableau .= $cgi->textfield(
        -name     => $nom,
        -size     => 15,
        -onChange => "var brouz='$chemin' + encodeURIComponent(this.value); this.value='';return fullrs('$nom',brouz)"
    );
    $tableau .= qq{</font></td>\n} ;
    $tableau .= qq{<td></td></tr>\n};
                 
    return $tableau;
}


sub navigateur {
    my $var;
	my $val;
	my $util;
	my $boole = 1;
	
    foreach $var (sort keys(%ENV)) {
        $val = $ENV{$var};
        $val =~ s|"|\\"|g;
        if (${var} eq "HTTP_USER_AGENT") {
        	$util = ${val};
        	last;
    	}
    }
    if ($util =~ /MSIE/) {
    	$boole = 0;
    }

    return $boole;
}	