#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';
use alim_al;
use clemail;
use fonctions;
use varglob;
use LOC::Characteristic;

my @pays = qw(FR);

# Param�tres des mails
my @email;
my %emailOrigin;
if ($G_ENVIR eq '') 
{
	@email   = split(',', &LOC::Characteristic::getCountryValueByCode('STAMAJALERTE', 'FR'));
}
else 
{
	@email   = qw(archive.dev@acces-industrie.com);
	$emailOrigin{'FR'} = &LOC::Characteristic::getCountryValueByCode('STAMAJALERTE', 'FR');
}

# Exp�diteur (juste l'adresse mail)
my $mailsSender = (keys(%{&LOC::Globals::get('mailsSender')}))[0];
my $stmp     = $ENV{'SMTP_HOST'};
my $type     = "HTML";

for(my $i=0; $i<@pays; $i++) {
	&veille($pays[$i]);
	&maj_al($pays[$i]);
	&list_al($pays[$i]);
}

my @alfr = split(/\|/,$Lib{"FR"});

# En-t�te HTML
my $tableau = &Debut_HTML;
$tableau .= qq{
<body bgcolor="#FFFFFF">
<table width="100%" border="0" cellspacing="2" cellpadding="0">};

# En-t�te FR
my $entfr = qq{
	<tr bgcolor="#C6CDC5">
		<th><font class="PT">Agence FR</font></th>};
for(my $i=0; $i<@alfr; $i++) {
    if ($alfr[$i] eq '')
    {
        next;
    }

	if ($i > 12 && $i < 21) {
		next;
	}
	my $colspan = "";
	if ($i == 3 || $i == 8) {
		$colspan = qq{ colspan="2"};
	}
	$entfr .= qq{
		<th$colspan><font class="PT">$alfr[$i]</font></th>};
}
$entfr .= qq{
	</tr>};

# Pied
my $pied = qq{
</table>
</body>
<html>
};

# Pied
my $pied = qq{
</table>
</body>
<html>
};

# Ligne alerte
my %index_color = (
	"FR" => 0,
);
my @color = ("", "#D6D6D6");
foreach (sort keys(%Nbr)) {
	my $ag = $_;

	my @tab  = split(/\|/,$Nbr{$ag});
	my @tab1 = split(/\|/,$Nbr1{$ag});
	
	my $lig = "";
	$lig .= qq{
	<tr bgColor='$color[$index_color{$tab[0]}]' onmouseover="bgColor='#FFCC33'" onmouseout="bgColor='$color[$index_color{$tab[0]}]'">};
	$lig .= qq{
		<td align="center"><font class="PT"><b>$ag</b></font></td>};

	for (my $i=1; $i<@tab; $i++) {
        if ($tab[0] eq 'FR' && $alfr[$i - 1] eq '')
        {
            next;
        }
		if ($i > 13 && $i < 22)	{
			next;
		}
		my $aff = "";
		if ($i == 4) {
			$aff = qq{
		<td align="center"><font class="PT"><b>$tab1[$i]</b></font></td>};
		}

		if ($i == 9) {
			$aff = qq{
		<td align="center"><font class="PT"><b>$tab1[@tab]</b></font></td>};
		}
		$lig .= qq{
		<td align="center"><font class="PT">$tab[$i]</font></td>$aff};
	}
	
	$lig .= qq{
	</tr>};
	
	if ($tab[0] eq "FR") {
		$entfr .= $lig;
	}
	$index_color{$tab[0]} = 1 - $index_color{$tab[0]};
}


my $htm   = &accentsHTML($tableau.$entfr.$entes.$entpt.$entma.$pied);
my $htmfr = &accentsHTML($tableau.$entfr.$pied);

my $subfr = qq{Alertes des agences du };
if ($G_ENVIR ne '')
{
    my $prefix = uc($G_ENVIR);
    $prefix =~ s/ //;
	$subfr = '[' . $prefix . ']' . $subfr;
	
}
my ($sec, $min, $hr, $jour, $mois, $an, $joursm, $jouran, $ete) = localtime;
$an += 1900;
$mois++;
my $datejr = $jour."/".$mois."/".$an;
for (my $j=0; $j<@email; $j++) {
	my $sbu = $subfr;
	$sbu .= $datejr;
	if (defined $emailOrigin{'FR'})
	{
	   $htm = 'Destinataires d\'origine : ' . $emailOrigin{'FR'} . $htm;
	}
	&env_email($stmp, $mailsSender, $email[$j], $sbu, $type, $htm);
}
for (my $j=0; $j<@emailfr; $j++) {
	my $sbu = $subfr;
	$sbu .= $datejr;
}

# Rapport d'erreur
my $subject = "Alertes corrigees le $datejr";
if ($G_ENVIR ne '')
{
    my $prefix = uc($G_ENVIR);
    $prefix =~ s/ //;
    $subject = '[' . $prefix . '] ' . $subject;
}

&env_email($stmp, $mailsSender, qq{archive.dev\@acces-industrie.com}, $subject, "TEXT", $rapport);


# D�but de page HTML (envoi par mail)
sub Debut_HTML {
	return qq{
<html>
<head>
<title>Alertes des agences du $date</title>
<style type="text/css">
BODY {
	background: #EDEDE0;
	color:#000000;
	font-size:10pt;
	font-family: Arial, Helvetica, sans-serif;
FONT {font-size:10pt;}
FONT.PT {font-size:8pt;}
}
</style>
</head>
};
}
