#!/usr/bin/perl

# $Id: alerte.cgi,v 2.11 2006/02/13 14:10:47 julien Exp $
# $Log: alerte.cgi,v $
# Revision 2.11  2006/02/13 14:10:47  julien
# Alertes sans le nowrap sur les cellules.
#
# Revision 2.10  2006/02/07 13:38:52  julien
# Correction bug tri dans l'alerte "devis � rappeler".
#
# Revision 2.9  2006/02/06 16:52:32  julien
# V. 1.4 : Tarification v2
#
# Revision 2.8.2.2  2006/01/10 15:50:53  julien
# Alerte remise exceptionnelle sur devis � rappeler.
#
# Revision 2.8.2.1  2005/12/13 15:46:21  julien
# no message
#
# Revision 2.8.4.1  2005/12/13 10:31:35  julien
# Inclusion du r�pertoire lib dans chaque fichier.
#
# Revision 2.8  2005/08/26 10:16:15  julien
# Pr�fixage des tables de AUTH dans les requ�tes.
#
# Revision 2.7  2005/07/12 15:47:30  julien
# Facturation des services et du nettoyage version lourde.
#
# Revision 2.1  2004/09/29 12:32:11  julien
# Remplacement de tous les "agence" dans les requ�tes (option g).
#
# Revision 2.0  2004/09/16 15:07:08  julien
# Version r�cup�r�e de Lancelot et nettoy�e
#
# Revision 1.2  2004/09/16 14:40:38  julien
# Ajout des identifiants
#

use lib '../lib', '../../inc', '../../lib';

use DBI;
use CGI;
use securite;
use calculdate;
use accesbd;
use varglob;
use autentif;
use tracetemps;
use fonctions;
use DateTime;
use LWP::Simple;
use MIME::Base64;

use LOC::Globals;


# Initialisation du chrono
my $timer = &init_chrono();

my $build = &LOC::Globals::get('build');

require "cgi-lib.pl";
my $cgi;
$cgi= new CGI;
$|=1;
print <<"ENTETE";
Content-type: text/html

ENTETE

# Param�tre de lecture du fichier de config connection.cfg
my %Param;
my @LstParam;
my $NbParam;
my $numw;
my $bloc;

$cgi->autoEscape(undef);
&ReadParse(*input);

#identification

my @langue;
my $ip;
my $agence;
my $tva;
my $unitmonaie;
my $prixcarbu;
my $agent;
my $idpers;
my $nom;
my $prenom;
my $pays;
my $logincon;

$logincon=&login;

&autantif($logincon);
$ip=&ip;
my $boolcon;

#$boolcon=&doubl_conn;
my $dbh;
$dbh=&Environnement("LOCATION");
#&lectbase;
my @resal=&alerte_agence;

my $idarl=$cgi->param('num');
my $tri=$cgi->param('tri');
if ($idarl ne "") {
	my $tb;

my $tps_exec;
if ($SRV1_NOM ne "sgbd-srv.acces-industrie.com") {
	$tps_exec = qq{<td align="right"><font><span id="tps_exec"></span></font></td>};
}

print <<"ENTETE";
<html>
<head>
<title>$idarl</title>
<meta http-equiv="Content-Type" content="text/html">
<script language="JavaScript" src="$URL_CONF/super.js?bld=$build">
</script>
<link rel=stylesheet href="$URL_CONF/alerte.css" type="text/css">
</head>
<body  bgcolor="#7F7FFF"  text="black" link="blue" vlink="blue" alink="blue"  >
<table bgcolor="C6CDC5" border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr>
<td bgcolor=C6CDC5><p align="left"><font class="TITRE"><b>ALERTE(s)</b></font></td>
$tps_exec

ENTETE

	&hauttab;
	print "</tr></table>\n";
}
else
{
print qq{
<HTML>
    <HEAD>
        <TITLE>
            Alertes
        </TITLE>
        <META http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
        <META http-equiv="Refresh" content="300">
        <LINK rel="stylesheet" href="$URL_CONF/alerte.css">
        <SCRIPT language="JavaScript" src="$URL_CONF/super.js&bld=$build">
        </SCRIPT>
    </HEAD>
    <BODY bgcolor="#7f7fff" leftmargin="0" topmargin="0">
        <TABLE>
            <TR>
                <TD valign="top">
                    <A href="#" onClick="window.location.reload(false)">
                        <IMG src="$URL_IMG/refresh_me.gif" alt="Rafra�chir les alertes" border="0"></A>
                </TD>
                <TD>};
}

if ($boolcon == 0) {
	if (not ($logincon eq "")) {
		my $pas=$pays;
		$pas=~ tr/A-Z/a-z/;
		my $al ="alerte";

		if ($pays ne "FR") {
			$al  .=$pas;
		}

		$al .="\.cfg";
		&LectureConfig($al);

		my $numw;
		#blocs
		my $base;
		my $personne;
		my $logiciel;
		my $fichier;
		my $nomfich;
		my @cle;
		# bloc db
		#bloc personne
		my $reqpers;
		#bloc logiciel
		my $reqlog;
		my @logiciel;
		my @lienlog;
		my @fonction;
		my @fichier;
		# pays
		my $reqpays;
		my $tva;
		my $unitmonaie;
		my $carburant;
		#  my $pays;
		my $euro;
		#alertes
		my @reqalerte;
		my @lienalerte;
		my @libalerte;
		my $nbalerte=0;
		my @reqoption;
		my @lienopt;
		my @affopt;
		my $nbfction=0;
		my $nblog;

		for ($numw=0; $numw<$NbParam; $numw++) {
			$bloc = $LstParam[$numw];

			# bloc db
			# bloc logiciel
			if ($bloc =~/logiciel/) {
				$reqlog=$Param{$bloc}{'requete'};
				$nblog=$Param{$bloc}{'nblog'};
				$reqlog =~ s/num/$idpers/eg;

				my $sth;
				$sth=$dbh->prepare("$reqlog");
				$sth->execute();
				my @data;

				while (@data=$sth->fetchrow_array()) {
					$logiciel[$nbfction]=$data[0];
					$nbfction++;
				}
				$sth->finish;
			}
		}

		print qq|<FORM method="get" action="" name="FORM1">\n|;
		my $lg_res = $cgi->param('largeur');
		my $ht_res = $cgi->param('hauteur');
		my $req_snif = qq|select RESOLUTION from AUTH.`PERSONNEL` where PEAUTO = "$agent"|;
		my $sth_snif=$dbh->prepare($req_snif);
		$sth_snif->execute();
		my $resol_old = $sth_snif->fetchrow_array();

		if (($lg_res ne "")&&(($resol_old eq " x ")||($resol_old eq ""))) {
			my $req_snif = qq|update AUTH.`PERSONNEL` set RESOLUTION = "$lg_res x $ht_res" where PEAUTO = "$agent"|;
			my $sth_snif2=$dbh->prepare($req_snif);
			$sth_snif2->execute();
		};

		$sth_snif->finish;
		print qq|<input type="hidden" name="largeur" value="$lg_res">|;
		print qq|<input type="hidden" name="hauteur" value="$ht_res">|;
		print qq|</form>|;


#fin message veille de facturation**********************************************

		#entete
		if ($idarl eq "") {
			#print $messH;
# 			## Message Administratif
    		#print qq{<table border="0" cellspacing="0" cellpadding="0"><tr valign="middle"><td><img src="$URL_IMG/PanneauDanger.gif"></td><td>};
 			if ($pays eq "FR") {
			#message veille de facturation**************************************************
				#variables
				$okM = 1;
				$okH = 1;
				#hebdomadaire
		 		$messH = qq|<div style="height : 48px;border:2px outset darkgray; padding:6px; margin:4px; background-color:gray">
				 		 	<img src="$URL_IMG/PanneauDanger.gif" style="float:left">
							  <center>
							  <font class="MESS" style="color:white">
							  FACTURATION HEBDOMADAIRE : Merci d'arr�ter ou prolonger les contrats ce soir et de passer un maximum de contrats en facturation.
							  </font>
							  </center>
							</div>\n|;
				#mensuelle
				$messM = qq|<div style="height : 48px;border:2px outset darkgray; padding:6px; margin:4px; background-color:gray">
				 			<img src="$URL_IMG/PanneauDanger.gif" style="float:left">
							<center>
							<font class="MESS" style="font-family:verdana ;color:white">
							FACTURATION MENSUELLE : Merci de saisir les carburants sur TOUS les contrats arr�t�s, ainsi que de prolonger ou arr�ter les contrats, ce soir.
							</font>
							</center>
						</div>\n|;
				#date du jour
				my ($secondes, $minutes, $heures, $jour_mois, $mois, $an, $jour_semaine, $jour_calendaire, $heure_ete) = localtime(time);
				#traitement uniquement si il est plus de 14heures
				if($heures >= 9){
					$mois+=1;
					#$mois=8;$jour_mois=15;$jour_semaine=2;
					$an += 1900;
					$mois = $mois < 10 ? $mois = "0".$mois : $mois;
					$jour_mois = $jour_mois < 10 ? $jour_mois = "0".$jour_mois : $jour_mois;
					$datedujour = "$an-$mois-$jour_mois";
					my $dj = DateTime->new( year => $an, month => $mois, day => $jour_mois);
					my $djplusun = DateTime->new( year => $an, month => $mois, day => $jour_mois);
					$djplusun = $djplusun->add(days => 1);
					#dernier jour ouvr� du mois
					my $dt = DateTime->last_day_of_month( year => $an, month => $mois );
					while ( $dt->day_of_week >= 6 ) { $dt->subtract( days => 1 ) }
					$findumois = $dt->ymd;
					while (is_fr_holiday($dt->year,$dt->month,$findumois) == 1){
						$findumois -= 1;
					}
					#print "|".$datedujour."|".$jour_semaine."|".$findumois;
					#verification de personnalisation dans la base
			        $requete=qq{select TYPE,ETAT from AUTH.MESSAGEFACTU where DATEF='$datedujour'};

					my $sth=$dbh->prepare($requete);
			    	$sth->execute();
			    	my $nbrow=$sth->rows;
			    	if ($nbrow > 0){

					    while(my @data=$sth->fetchrow_array())
					    {
					        my $type = $data[0];
					        my $etat = $data[1];
					        #cas mensuelle
					        if ($type eq "M" && $etat eq "GEN01"){
								print $messM.'';
					        }
					        #cas hebdo
					        elsif($type eq "H" && $etat eq "GEN01"){
								print $messH.'';
					        }
					    }
			        }
			        else{
				 		if ($datedujour eq $findumois){
							print $messM.'';
						}
						#si on est mercredi ou si le lendemain est un mercredi et qu'il est feri�
						elsif(($jour_semaine eq "3" && is_fr_holiday($dj->year,$dj->month,$dj->day) != 1)||($jour_semaine eq "2" && is_fr_holiday($djplusun->year,$djplusun->month,$djplusun->day) == 1)){
							print $messH.'';
						}
					}
			        $sth->finish;
				}
#    		print qq{<table border="0" cellspacing="0" cellpadding="0"><tr valign="middle"><td></td><td>};
# 				print qq|<div style="border:2px outset yellow; padding:6px; margin:4px;"><img src="$URL_IMG/PanneauDanger.gif" style="float:left"><center><font class="MESS" style="color:white">Contr�le exceptionnel de la facturation MENSUELLE. Merci de saisir les carburants sur TOUS les contrats arr�t�s, ainsi que de prolonger ou arr�ter les contrats, ce soir.</font></center></div>\n|;
# 				print qq|<center><font class="MESS">Portail indisponible � partir de 21H00 en raison d'une intervention sur le r�seau</font></center>\n|;
## 				print qq|<center><font class="MESS">Portail en maintenance mercredi de 13h30 � 14h00</font></center>\n|;
### 				print qq|<center><font class="MESS">Portail inaccessible ce matin : 09h45 -> 09h50(FACTURATION)</font></center>\n|;
####print qq{<table border="0" cellspacing="0" cellpadding="0"><tr valign="middle"><td><img src="$URL_IMG/PanneauCompta.gif"></td><td>};
####print qq|<center><font class="MESS">&nbsp;&nbsp;Comptabilit� clients indisponible cet apr�s-midi de 14H � 15H</font></center>\n|;
 			} elsif ($pays eq "ES") {
# 				print qq|<center><font class="MESS">"Entre nous" indisponible a partir 21H00 debido a una intervenci�n sobre la red</font></center>\n|;
## 				print qq|<center><font class="MESS">"Entre nous" en mantenimiento mi�rcoles de 13h30 a 14h00</font></center>\n|;
###				print qq|<center><font class="MESS">"Entre nous" inaccesible esta ma�ana : 09h45 -> 09h50(FACTURATION)</font></center>\n|;
 			} elsif ($pays eq "PT") {
# 				print qq|<center><font class="MESS">"Entre nous" em manuten��o a partir de 21H00 (hora francesa) devido � uma interven��o sobre a rede</font></center>\n|;
## 				print qq|<center><font class="MESS">"Entre nous" em manuten��o quarta-feira de 13h30 a 14h00 (hora francesa)</font></center>\n|;
### 				print qq|<center><font class="MESS">Portail inacess�vel esta manh�  : 09h45 -> 09h50(FACTURATION)</font></center>\n|;
			} elsif ($pays eq "MA") {
# 				print qq|<center><font class="MESS">Portail indisponible � partir de 21H00 en raison d'une intervention sur le r�seau</font></center>\n|;
## 				print qq|<center><font class="MESS">Portail en maintenance mercredi de 13h30 � 14h00 (heure fran�aise)</font></center>\n|;
### 				print qq|<center><font class="MESS">Portail inaccessible ce matin : 09h45 -> 09h50(FACTURATION)</font></center>\n|;
#print qq{<table border="0" cellspacing="0" cellpadding="0"><tr valign="middle"><td><img src="$URL_IMG/PanneauCompta.gif"></td><td>};
#print qq|<center><font class="MESS">Comptabilit� clients indisponible cet apr�s-midi de 14H � 15H</font></center>\n|;
			} else {
# 				print qq|<center><font class="MESS">Portail indisponible � partir de 21H00 en raison d'une intervention sur le r�seau</font></center>\n|;
## 				print qq|<center><font class="MESS">Portail en maintenance mercredi de 13h30 � 14h00</font></center>\n|;
### 				print qq|<center><font class="MESS">Portail inaccessible ce matin : 09h45 -> 09h50(FACTURATION)</font></center>\n|;
#print qq{<table border="0" cellspacing="0" cellpadding="0"><tr valign="middle"><td><img src="$URL_IMG/PanneauCompta.gif"></td><td>};
#print qq|<center><font class="MESS">Comptabilit� clients indisponible cet apr�s-midi de 14H � 15H</font></center>\n|;
			};
 			print qq{</td></tr></table>};
# 			## Fin du message administratif

			print qq{<table width="100%" border="0" cellspacing="1" cellpadding="0">\n<tr bgcolor="#FFC003">\n};
			my $nba=0;

            #alerte CRM
            #requete pour savoir si la personne possede un acces CRM
            if ($G_PAYS eq "FR" || $G_PAYS eq "ES")
            {
                my $pays = "";
                $pays = $G_PAYS if ($G_PAYS ne "FR");

                $requete=qq{
                    select count(*) from frmwrk.f_user_application where uap_usr_id = $G_PEAUTO and uap_app_id=2
                };
            	my $sth=$dbh->prepare($requete);
            	$sth->execute();
            	my @data=$sth->fetchrow_array();
                my $nbCRM = $data[0];
                if ($nbCRM > 0)
                {
                    my $nbAlerteCRM = get($ENV{'CRM_URL'} . "/index.php?_exec=public/userAlertsCount&userId=$G_PEAUTO&country=$G_PAYS" );
                    print qq{
        <td>
        <a href="#" onclick="fullrs('crm','} . $ENV{'CRM_URL'} . qq{'); return false;" >
            <img src="$URL_IMG/fire.gif" border="0" alt="$Param{$bloc}{"lib"}">
        </a>
        <font class="GD">
            <b>$nbAlerteCRM alertes CRM</b>
        </font>
        $js
    </td>
</tr>\n
<tr bgcolor="#FFC003">};

                }
            }


			for ($numw=0; $numw<$NbParam; $numw++) {
				$bloc = $LstParam[$numw];
				my $div =$nba % 3;

				if ($nbfction != 0) {
					if ($bloc =~ /alerte/) {
						my $i;

						for($i=0;$i<$nbfction;$i++) {
							if ($Param{$bloc}{'lien'} eq $logiciel[$i]) {
								$nomfich[$nba]=$Param{$bloc}{'nom'};
								my $numal=$Param{$bloc}{'pos'};

#								if ($resal[$numal] !=0) {
									print qq{<td><a href="$HTTP_PORTAIL/cgi-bin/old/commun/alerte.cgi?num=$nomfich[$nba]" onclick="return fullrs('$nomfich[$nba]','$HTTP_PORTAIL/cgi-bin/old/commun/alerte.cgi?num=$nomfich[$nba]')" ><img src="$URL_IMG/fire.gif" border="0" alt="$Param{$bloc}{"lib"}"></a><font class="GD"><b>$resal[$numal] $Param{$bloc}{"lib"}</b></font></td>};
#								} else {
#									print qq{<td>&nbsp;</td>\n};
#
#									if ($div ==0) {
#										print qq{</tr>\n<tr bgcolor="#FFC003">}
#									};
#								}
							}
						}
					}
				}

				if ($div == 0) {
					print qq{</tr>\n<tr bgcolor="#FFC003">}
				};
				$nba++;
			}
			print qq{</tr>\n</table>\n};
		} else {
			for ($numw=0; $numw<$NbParam; $numw++) {
				$bloc = $LstParam[$numw];
				if ($nbfction != 0) {
					if ($bloc =~/alerte/) {
						my $i;
						for($i=0;$i<$nbfction;$i++) {
							if ($Param{$bloc}{'lien'} eq $logiciel[$i]) {
								$reqalerte[$nbalerte]=$Param{$bloc}{'requete'};
								if ($tri ne "") {
									my @reqd=split (/ORDER BY /,$reqalerte[$nbalerte]);

                                    $reqalerte[$nbalerte] = "";
                                    for (my $idx=0; $idx<@reqd-1 ;$idx++) {
    									$reqalerte[$nbalerte] .= $reqd[$idx]. qq{ORDER BY };
                                    }

									$reqalerte[$nbalerte] .= $tri;
								}
								my $tmpt = $GESTIONTRANS_BASE{"$G_PAYS"};
								$reqalerte[$nbalerte]=~ s/agence/$agence/g;
								$reqalerte[$nbalerte]=~ s/agent/$agent/;
								$reqalerte[$nbalerte]=~ s/basetrans/$tmpt/g;

								$fichier[$nbalerte]=$Param{$bloc}{'fichier'};
								if ($Param{$bloc}{'option'} eq "oui") {
									$reqoption[$nbalerte]=$Param{$bloc}{'reqoption'};
									$reqoption[$nbalerte]=~ s/agence/$agence/;
									$lienopt[$nbalerte]=$Param{$bloc}{'lienopt'};
									$affopt[$nbalerte]=$Param{$bloc}{'affopt'};
								}
								$nomfich[$nbalerte]=$Param{$bloc}{'nom'};
								$cle[$nbalerte]=$Param{$bloc}{'cle'};
								if ($idarl ne $nomfich[$nbalerte]) {
									next;
								}
								my $sth;
						$sth=$dbh->prepare($reqalerte[$nbalerte]);
								$sth->execute();
								my @label=@{$sth->{NAME}};
								my $nbrow=$sth->rows;
								if ($nbrow) {
									my @data;
									my $nbaff;
									my @opchp;
									my $k;
									$nbaff=$Param{$bloc}{'aff'};
									print qq{<table width="100%" border="0" cellspacing="0" cellpadding="0">\n<tr bgcolor="#FFC003">\n};
									print qq{<td nowrap><a name="$nomfich[$nbalerte]"></a><font class="PT"><b>$nbrow $Param{$bloc}{"lib"}</b></font></td>\n};
									print qq{<td  align="right" valign="top"><img src="$URL_IMG/arrondi3.gif"  width="5" height="5"></td></tr>\n};
									print qq{</table>\n};
									print qq{<table width="100%" border="0" cellspacing="1" cellpadding="1">\n<tr bgcolor="#FEDE7C">\n};
									for ($k=0;$k<$nbaff;$k++) {
										$l=$k+1;
										$opchp[$k]=$Param{$bloc}{"optchamp$l"};
										my $debln=qq{<a href="$HTTP_PORTAIL/cgi-bin/old/commun/alerte.cgi?num=$nomfich[$nba]&tri=$label[$k]"  onclick="return fullrs('_self','$HTTP_PORTAIL/cgi-bin/old/commun/alerte.cgi?num=$nomfich[$nba]&tri=$label[$k]')" >\n};
										my $finln=qq{</a>\n};
										print qq{<td nowrap style="text-align: center;"><font>$debln<div style="font-weight: bold;">$Param{$bloc}{"champ$l"}</div>$finln</font></td>\n};
									}
									print qq{<td style="width: 24px;">&nbsp;</td></tr>\n};
									while (@data=$sth->fetchrow_array()) {
										my $fich=$fichier[$nbalerte];
										my $option="";
										if ($Param{$bloc}{'option'} eq "oui") {
											my $num=$data[$lienopt[$nbalerte]];
											my $req=$reqoption[$nbalerte];
											$req=~ s/num/$num/;
											my $sthop=$dbh->prepare("$req");
											$sthop->execute();
											my $row=$sthop->rows;
											if ($row !=0) {
												$option=qq{<b>$affopt[$nbalerte]</b>};
											}
											$sthop->finish;
										}
										if ($nomfich[$nbalerte] ne"pointtourne") {
											$fich =~ s/cle/$data[$cle[$nbalerte]]/;
										} else {
											if ($data[@data-2] !=0) {
												$fich =~ s/cle1/$data[$cle[$nbalerte]]/;
												$fich =~ s/cle2//;
											}
											if ($data[@data-1] !=0) {
												$fich =~ s/cle1//;
												$fich =~ s/cle2/$data[$cle[$nbalerte]]/;
											}
										}
										if ($fich =~ m/\?locParams\=(.+)/)
										{
										    my $base64 = &MIME::Base64::encode_base64('{' . $1 . '}', '');
										    $base64 =~ s/([\W])/"%" . uc(sprintf("%2.2x",ord($1)))/eg;
										    $fich =~ s/\?locParams\=(.+)/\?locParams\=$base64/;
										}
										print qq{<tr bgcolor="#FFEBB2" onmouseover="bgColor='#FFCC33'" onmouseout="bgColor='#FFEBB2'">\n};
										for($k=0;$k<$nbaff;$k++) {
											my $op="";
											if ($opchp[$k] eq "date") {
												$data[$k] =&format_datetime($data[$k]);
											}
											if ($opchp[$k] eq "num") {
												$data[$k] = $data[$k] * 1;
											}
											if ($k==$lienopt[$nbalerte]) {
												$op=$option;
											}
# 											$data[$k] =substr ($data[$k],0,17);
											print qq{<td><font>$data[$k]$op</font></td>\n};
										}
										print qq{<td nowrap bgcolor="#FEDE7C" align="right">\n};
										print qq{<a href="$HTTP_PORTAIL/cgi-bin/old/$fich" target="_blank"><img src="$URL_IMG/bouton2-big.gif" border="0" alt="$Param{$bloc}{"lib"}"></a></td>\n</tr>};
									}
									$sth->finish;
									print qq{</table>\n};
									$nbalerte++;
								}
							}
						}
					}
				}
			}
		}
		$dbh->disconnect;
	}
}

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

if ($idarl ne "") {
	# Pied de la page HTML
	my $paramType = $cgi->param("type");
	my $paramMenu = $cgi->param("menu");
	&Pied($delta_time, "$G_PENOM;$G_AGENCE;" . &nom_fichier() . ";" . $paramType . ";" . $paramMenu);
}
else
{
print qq{                </TD>
            </TR>
        </TABLE>
    </BODY>
</HTML>};
}

#--------------------------------------------------------------------------
# LectureConfig - Lecture fichier de configuration
#
sub LectureConfig {
	my ($FicConf) = shift (@_);
	my ($nom_bloc, $lst_bloc);
	my ($cle, $op, $data);
	my ($bloc);

	$NbParam = 0;
	open (CONF, $FicConf);
	undef $bloc;
	while (<CONF>) {
		s/[\012\015]/ /g;
		s/#.*//;
		s/^\s+//g;
		s/\s+$//g;
		s/\s*(\+?=|{|})\s*/$1/g;
		next unless length;
		$bloc .= $_;
		if (/}/) {
			if ($bloc =~ /([\w\-_]+){(.*)}/) {
				$nom_bloc  = $1;
				$lst_bloc  = $2;
				foreach (split(/;/, $lst_bloc)) {
					if (/(\w+)(\+?=)(.*)/) {
						$cle=$1;
						$op=$2;
						$data=$3;
					}
					if ($nom_bloc eq "config") {
						$Config{$cle} = $data;
					} else {
						if ($op eq "+=") {
							$Param{$nom_bloc}{$cle} .= " $data";
						} else {
							$Param{$nom_bloc}{$cle} = $data;
						}
					}
				}
				$LstParam[$NbParam++] = $nom_bloc if $nom_bloc !~ /(global|config)/;
			}
			undef $bloc;
		}
	}
	close(CONF);
}

sub autantif {
	my @identif=@_;
	my $logi=$identif[0];
	my $dbh;
	$dbh=DBI->connect($SRV1_DATASRC,$SRV1_LOGIN,$SRV1_PASSWD);
	my $sth;
	my $sth2;
	my $requete;
	my $requete2;
	#agence
	$requete=qq{SELECT PERSONNEL.AGAUTO , PEAUTO ,PROFILAUTO FROM AUTH.`PERSONNEL` WHERE PERSONNEL.PELOGIN = "$logi"};
	$sth=$dbh->prepare($requete);
	$sth->execute();
	my @data;
	while (@data=$sth->fetchrow_array()) {
		$agence=$data[0];
		$agent=$data[1];
		$idpers=$data[2];
	}
	$sth->finish;
	#pays
	$requete2=qq{SELECT  PATVA , PAMONNAIE  , PACARBURANT , PAYS.PACODE  FROM AUTH.`PAYS` LEFT JOIN AUTH.`PAPE` ON PAYS.PACODE=PAPE.PACODE WHERE PAPE.PEAUTO = "$agent"};
	$sth2=$dbh->prepare($requete2);
	$sth2->execute();
	my @data2;
	while (@data2=$sth2->fetchrow_array()) {
		$tva=$data2[0];
		$unitmonaie=$data2[1];
		$prixcarbu=$data2[2];
		$pays=$data2[3];
	}
	$sth2->finish;
	$dbh->disconnect;
}

sub lectbase {
	my $ps=$pays;
	$ps=~ tr/A-Z/a-z/;
	my $fich ="annudb";
	if ($pays ne "FR") {
		$fich .=$ps;
	}
	$fich .="\.cfg";

	&LectureConfig($fich);
	for ($numw=0; $numw<$NbParam; $numw++) {
		$bloc       = $LstParam[$numw];

		# DataBase exist
		if ($bloc =~/base/) {
			$connection=$Param{$bloc}{'con'};
			$user=$Param{$bloc}{'user'};
			$passe=$Param{$bloc}{'password'};

			$dbh=DBI->connect($connection,$user,$passe)||die "erreur de connection";
		}
	}
}

#haut de la fenetre
sub hauttab {
	print qq{<td width="54">\n};
	print qq{<a href="javascript:history.back(1)"><img src="$URL_IMG/img_2.gif" align="MIDDLE" border="0" width="54" height="36" alt="Retour Arriere"></a>\n};
	print qq|
		<SCRIPT Language="Javascript">
		function printit() {
			window.print() ;
		}

		var NS = (navigator.appName == "Netscape");
		var VERSION = parseInt(navigator.appVersion);

		if (VERSION > 3) {
			document.write('</td><td width="54"><a href="javascript:printit()"><img src="$URL_IMG/img_3.gif" align="MIDDLE" border="0" width="54" height="36" alt="$mess[300]"></a>');
		}
		</SCRIPT>
|;

	print qq{</td><td width="48"><a href="javascript:window.close()"><img src="$URL_IMG/img_4.gif" align="MIDDLE" border="0" width="48" height="36" alt="Fermer"></a>\n};
	print qq{</td>\n};
}

# lect compte alerte
sub alerte_agence {
	my @result;
	my $liste_agence;
	for ($i = 0; $i <= 28; $i++) {
		if ($i==19) {next};
		$liste_agence .= ", A.AL$i";
	}
	$liste_agence = substr($liste_agence, 2);
	my $reqalag=qq{SELECT $liste_agence FROM AUTH.`ALERTE` A WHERE A.AGAUTO="$agence" };
	my $stalag=$dbh->prepare($reqalag);
	$stalag->execute();

	while (my @data=$stalag->fetchrow_array()) {
		for(my $i=0;$i<@data;$i++) {
			if ($i >= 19) {$k=$i+1;}
			else {$k=$i;}
			$result[$k]=$data[$i];
		}
	}
	$stalag->finish;

	return @result;
}

sub easter {
    my ($year) = @_;
    my ( $G, $C, $H, $I, $J, $L, $month, $day, );
    $G = $year % 19;
    $C = int( $year / 100 );
    $H = ( $C - int( $C / 4 ) - int( ( 8 * $C ) / 25 ) + 19 * $G + 15 ) % 30;
    $I = $H - int( $H / 28 ) *
      ( 1 - int( $H / 28 ) * int( 29 / ( $H + 1 ) ) * int( ( 21 - $G ) / 11 ) );
    $J     = ( $year + int( $year / 4 ) + $I + 2 - $C + int( $C / 4 ) ) % 7;
    $L     = $I - $J;
    $month = 3 + int( ( $L + 40 ) / 44 );
    $day   = $L + 28 - ( 31 * int( $month / 4 ) );
    return ( $month, $day );
}
sub get_easter {
	my ($year) = @_;

	return easter($year);
}

sub get_ascension {
	my ($year) = @_;

	return _compute_date_from_easter($year, 39);
}

sub get_pentecost {
	my ($year) = @_;

	return _compute_date_from_easter($year, 50);
}

sub _compute_date_from_easter {
	my ($year, $delta) = @_;

	my ($easter_month, $easter_day) = get_easter($year);
	my $easter_date = Time::Local::timelocal(0, 0, 1, $easter_day, $easter_month - 1, $year - 1900);
	my ($date_month, $date_day) = (localtime($easter_date + $delta * 86400))[4, 3];
	$date_month++;

	return ($date_month, $date_day);
}

sub is_fr_holiday {
	my ($year, $month, $day) = @_;

	if ($day == 1 and $month == 1) { return 1; }
	elsif ($day == 1 and $month == 5) { return 1; }
	elsif ($day == 8 and $month == 5) { return 1; }
	elsif ($day == 14 and $month == 7) { return 1; }
	elsif ($day == 15 and $month == 8) { return 1; }
	elsif ($day == 1 and $month == 11) { return 1; }
	elsif ($day == 11 and $month == 11) { return 1; }
	elsif ($day == 25 and $month == 12) { return 1; }
	else {
		my ($easter_month, $easter_day) = get_easter($year);
		my ($ascension_month, $ascension_day) = _compute_date_from_easter($year, 39);
		my ($pentecost_month, $pentecost_day) = _compute_date_from_easter($year, 50);

		if ($day == ($easter_day + 1) and $month == $easter_month) { return 1; }
		elsif ($day == $ascension_day and $month == $ascension_month) { return 1; }
		elsif ($day == $pentecost_day and $month == $pentecost_month) { return 1; }
	}
	return false;
}


