# $Id: alim_al.pm,v 1.1.1.1 2006/08/01 07:48:01 clliot Exp $
# $Log: alim_al.pm,v $
# Revision 1.1.1.1  2006/08/01 07:48:01  clliot
# no message
#
# Revision 2.4  2005/08/26 10:14:00  julien
# Préfixage des tables de AUTH dans les requêtes.
#
# Revision 2.3  2005/08/03 07:15:18  julien
# Affichage des alertes manquantes.
#
# Revision 2.2  2005/07/20 08:20:26  julien
# Pb AL27 pas récupérée.
#
# Revision 2.1  2005/06/21 10:05:42  julien
# Modifications.
#
# Revision 2.0  2004/09/16 15:07:07  julien
# Version récupérée de Lancelot et nettoyée
#
# Revision 1.2  2004/09/16 14:41:52  julien
# Ajout des identifiants
#

require Exporter;
@ISA =qw(Exporter);
@EXPORT =qw(alim_al crea_al maj_al %Lib %Nbr %Lib1 %Nbr1 list_al veille $rapport);
use vars qw(%Lib %Nbr %Lib1 %Nbr1 $rapport);
use strict;
use DBI;
use CGI;
use accesbd;
use varglob;
#use varglob;
#use autentif;

#use calculdate;
my $pays;
#parametre de lecture du fichier de config connection.cfg
my %Param;
my @LstParam;
my $NbParam;
my $numw;
my $bloc;
my %Config;

my $dbh;
#my $dbi=0;
#my @nombase;

sub alim_al
{

    $pays=$_[0];


    &lectbase;

    my $pas=$pays;
    $pas=~ tr/A-Z/a-z/;
    my $al ="alerte";
    if ($pays ne "FR")
    {
        $al  .=$pas;
    }
    $al .="\.cfg";

    &LectureConfig($al);
    #alertes
    my @nomfich;
    my @reqalerte;
    my @posalerte;
    my @lienalerte;
    my @libalerte;
    my $nbalerte=0;
    my @reqoption;
    my @lienopt;
    my @affopt;
    my $nbfction=0;
    my $nblog;
    my $nba=0;
   #  my @ag=qw(AIX TON REN LYO PAN SAR POI ART SAV SIE BCN MAD VAL ZAR DIJ ROU LIL TLS);
    my @ag=&lst_agence($pays);
    for ($numw=0; $numw<$NbParam; $numw++)
    {
        $bloc = $LstParam[$numw];

    # bloc db

    # bloc logiciel

        if ($bloc =~/alerte/)
        {
            $nomfich[$nba]=$Param{$bloc}{'nom'};
            $posalerte[$nba]=$Param{$bloc}{'pos'};
            $reqalerte[$nba]=$Param{$bloc}{'requete'};
            for(my $i=0;$i<@ag;$i++)
            {
                my $r=$reqalerte[$nba];

                my $tmpt = $GESTIONTRANS_BASE{"$pays"};
                $r=~ s/basetrans/$tmpt/g;
                $r=~ s/agence/$ag[$i]/;

                ##print "al$i=$r\n";
                my $str=$dbh->prepare($r);
                $str->execute();
                my $nbrows=$str->rows;
                 $str->finish;
                ##print "nb$i=$nbrows\n";
                alerte_pl_ms($ag[$i],$posalerte[$nba],$nbrows) if $nbrows;

            }
            $nba++;
        }
	}

    $dbh->disconnect;


}

sub crea_al
{
    $pays=$_[0];
    my @ag;
    &lectbase;
    @ag=&lst_agence($pays);
    #FRance
    #if ($pays eq "FR")
    # {
     #	@ag=qw(AIX TON REN LYO PAN SAR POI ART SAV SIE DIJ ROU LIL TLS);

    #}
    #Espange
    #if ($pays eq "ES")
     #{
     #	@ag=qw(BCN MAD VAL ZAR);

    #}

    #parametre de lecture du fichier de config connection.cfg


    #my @dbh;


    # Insertion table Alerte;
    for(my $i=0;$i<@ag;$i++)
    {
        my $r=qq{REPLACE INTO AUTH.`ALERTE` (AGAUTO) values ("$ag[$i]")};
        #print "$r\n#";
        $dbh->do($r);
    }


    $dbh->disconnect;

}

sub maj_al {
    $pays = $_[0];
    &lectbase;

    my $pas=  $pays;
    $pas =~ tr/A-Z/a-z/;
    my $al = "../commun/alerte";

    if ($pays ne "FR")
    {
        $al .= $pas;
    }
    $al .= "\.cfg";

    &LectureConfig($al);

    #alertes
    my @reqalerte;
    my @nomfich;
    my @posalerte;
    my @lienalerte;
    my @libalerte;
    my $nbalerte = 0;
    my @reqoption;
    my @lienopt;
    my @affopt;
    my $nbfction = 0;
    my $nblog;
    my $nba = 0;
    my @ag;
    @ag = &lst_agence($pays);

    # R�cup�ration des alertes
    my %alerte;
    my $req = qq{
        SELECT AGAUTO, AL0, AL1, AL2, AL3, AL4, AL5, AL6, AL7, AL8, AL9, AL10,
            AL11, AL12, AL13, AL14, AL15, AL16, AL17, AL18, AL19, AL20, AL21,
            AL22, AL23, AL24, AL25, AL26, AL27, AL28, AL29, AL30, AL31, AL32,
            AL33, AL34, AL35, AL36, AL37, AL38, AL39
        FROM AUTH.`ALERTE`
        WHERE AGAUTO IN ("}.join(qq{", "}, @ag).qq{")};

    my $sth = $dbh->prepare($req);
    $sth->execute();
    while (my @data = $sth->fetchrow_array())
    {
        for (my $i=0; $i<@data-1; $i++)
        {
            $alerte{$data[0]}{$i} = $data[$i+1];
        }
    }
	$sth->finish;
	for ($numw=0; $numw<$NbParam; $numw++)
	{
        $bloc = $LstParam[$numw];

        if ($bloc =~ /alerte/)
        {
            $nomfich[$nba] = $Param{$bloc}{'nom'};
            $posalerte[$nba] = $Param{$bloc}{'pos'};
            $reqalerte[$nba] = $Param{$bloc}{'requete'};

            for(my $i=0; $i<@ag; $i++)
            {
                my $r = $reqalerte[$nba];
                if ($r ne '')
                {
                    $r =~ s/agence/$ag[$i]/g;
                    my $tmpt = $GESTIONTRANS_BASE{"$pays"};
                    $r=~ s/basetrans/$tmpt/g;
                    my $str=$dbh->prepare($r);
                    $str->execute();
                    my $nbrows = $str->rows;
                    $str->finish;

                    # Alerte divergente
                    if ($nbrows != $alerte{$ag[$i]}{$posalerte[$nba]})
                    {
                        $rapport .= "Alerte $posalerte[$nba] de $ag[$i] : $alerte{$ag[$i]}{$posalerte[$nba]} <> $nbrows\n";
                        # Ajout de la différence
                        alerte_pl_ms($ag[$i], $posalerte[$nba], $nbrows - $alerte{$ag[$i]}{$posalerte[$nba]});
                    }
                }
            }
            $nba++;
        }
    }
    $dbh->disconnect;
}

sub list_al
{
    $pays=$_[0];

    &lectbase;

    my $pas=$pays;
    $pas=~ tr/A-Z/a-z/;
    my $al ="alerte";
    if ($pays ne "FR")
    {
        $al  .=$pas;
    }
    $al .="\.cfg";

    &LectureConfig($al);
     #alertes

    my @reqalerte;
    my @nomfich;
    my @posalerte;
    my @lienalerte;
    my @libalerte;
    my $nbalerte=0;
    my @reqoption;
    my @lienopt;
    my @affopt;
    my $nbfction=0;
    my $nblog;
     my $nba=0;
    my @ag=();
    @ag=&lst_agence($pays);
    for(my $i=0;$i<@ag;$i++)
    {
         $Nbr{$ag[$i]}=qq{$pays|};
         $Lib{"$pays"}="";
    }
    my $selected=q{ALERTE.AGAUTO};
    for(my $k = 0; $k < 40; $k++)
    {
        $selected .= qq{,AL$k}
    }
    my $r = qq{select $selected FROM AUTH.`ALERTE` LEFT join AUTH.`AGENCE` ON AGENCE.AGAUTO=ALERTE.AGAUTO WHERE PACODE="$pays"};

    my $str=$dbh->prepare($r);
    $str->execute();
    while (my @data=$str->fetchrow_array())
    {
        my $dt="";
        for(my $j=1;$j<@data;$j++) {$dt .=$data[$j]."|"};
        chop $dt;
        $Nbr{$data[0]} .=$dt;
    }
    $str->finish;

    for ($numw=0; $numw<$NbParam; $numw++)
    {
        $bloc = $LstParam[$numw];
# bloc db
# bloc logiciel
        if ($bloc =~/alerte/)
        {
            $nomfich[$nba]=$Param{$bloc}{'nom'};
            $posalerte[$nba]=$Param{$bloc}{'pos'};
            $libalerte[$nba]=$Param{$bloc}{'lib'};
            $Lib{"$pays"} .=$libalerte[$nba]."|";
             # if (($posalerte[$nba] !=3)&&($posalerte[$nba] !=8)&&($posalerte[$nba] !=9)&&($posalerte[$nba] !=10)) {next};
            $nba++;
        }
    }
    $dbh->disconnect;
    chop $Lib{"$pays"};
}
#liste de la Veille
sub veille {

$pays=$_[0];

&lectbase;

 my $pas=$pays;
	$pas=~ tr/A-Z/a-z/;
	my $al ="alerte";
if ($pays ne "FR")
{
    	$al  .=$pas;
	}
  $al .="\.cfg";

   &LectureConfig($al);
  #alertes

    my @reqalerte;
    my @nomfich;
    my @posalerte;
    my @lienalerte;
    my @libalerte;
    my $nbalerte=0;
    my @reqoption;
    my @lienopt;
    my @affopt;
    my $nbfction=0;
    my $nblog;
     my $nba=0;
    my @ag=();
    @ag=&lst_agence($pays);

     for(my $i=0;$i<@ag;$i++)
     	{
     		$Nbr1{$ag[$i]}=qq{$pays|};
     		$Lib1{"$pays"}="";
     	}

   my $r=qq{select ALERTE.* FROM AUTH.`ALERTE` LEFT join AUTH.`AGENCE` ON AGENCE.AGAUTO=ALERTE.AGAUTO WHERE PACODE="$pays"};

         my $str=$dbh->prepare($r);
         $str->execute();
         while (my @data=$str->fetchrow_array())
         	{
         	my $dt="";
         	for(my $j=1;$j<@data;$j++) {$dt .=$data[$j]."|"};
         	chop $dt;

         	$Nbr1{$data[0]} .=$dt;
         	}
        $str->finish;
    $r=qq{SELECT  MACHINE.AGAUTO,count(*) FROM CONTROLEMACHINE LEFT JOIN AUTH.`MACHINE` ON MACHINE.MAAUTO=CONTROLEMACHINE.MAAUTO LEFT join AUTH.`AGENCE` ON AGENCE.AGAUTO=MACHINE.AGAUTO WHERE PACODE="$pays" AND  COMAAPAVE<CURDATE()  GROUP BY MACHINE.AGAUTO};
   	$str=$dbh->prepare($r);
         $str->execute();
         while (my @data=$str->fetchrow_array())
         	{
         	$Nbr1{$data[0]} .="|".$data[1];
         	}
        $str->finish;
   for ($numw=0; $numw<$NbParam; $numw++) {
      $bloc = $LstParam[$numw];

# bloc db

# bloc logiciel


       if ($bloc =~/alerte/) {
        	$nomfich[$nba]=$Param{$bloc}{'nom'};
               $posalerte[$nba]=$Param{$bloc}{'pos'};
               $libalerte[$nba]=$Param{$bloc}{'lib'};
               $Lib1{"$pays"} .=$libalerte[$nba]."|";

             # if (($posalerte[$nba] !=3)&&($posalerte[$nba] !=8)&&($posalerte[$nba] !=9)&&($posalerte[$nba] !=10)) {next};



        $nba++;
     	 }




	}

 $dbh->disconnect;
  chop $Lib1{"$pays"};


}
#lecture de la base

sub lectbase{
#my $ps=$pays;
	#$ps=~ tr/A-Z/a-z/;
	$dbh=&Environnement("LOCATION",$pays);

}


#--------------------------------------------------------------------------
# LectureConfig - Lecture fichier de configuration
#
sub LectureConfig {
 my   ($FicConf) = shift (@_);
 my ($nom_bloc, $lst_bloc);
my   ($cle, $op, $data);
   ($bloc);

  $NbParam = 0;
  open (CONF, $FicConf);
  undef $bloc;
  while (<CONF>) {
    s/[\012\015]/ /g;
    s/#.*//;
    s/^\s+//g;
    s/\s+$//g;
    s/\s*(\+?=|{|})\s*/$1/g;
    next unless length;
    $bloc .= $_;
    if (/}/) {
##print "$bloc<===\n";
      if ($bloc =~ /([\w\-_]+){(.*)}/) {
        $nom_bloc  = $1;
        $lst_bloc  = $2;
        foreach (split(/;/, $lst_bloc)) {
          if    (/(\w+)(\+?=)(.*)/) {      # ----- x+=y ou x=y
            $cle=$1; $op=$2; $data=$3;
          }
          if ($nom_bloc eq "config") {     # ----- Param. de config.
            $Config{$cle} = $data;
          }
          else {
            if ($op eq "+=") {
              $Param{$nom_bloc}{$cle} .= " $data";
            }
            else {
              $Param{$nom_bloc}{$cle} = $data;
           }
          }
        }
        $LstParam[$NbParam++] = $nom_bloc if $nom_bloc !~ /(global|config)/;
      }
      undef $bloc;
    }
  }
  close(CONF);
}

sub  alerte_pl_ms {
	my @param=@_;
	my $agenceal=$param[0];
	#my $baseal=$param[1];
	my $champal=$param[1];
	my $epsilon=$param[2];
	if ($epsilon >= 0)
	 {
	 	$epsilon=qq{+$epsilon};
	}
	#my @ag=qw(AIX TON REN LYO PAN SAR POI ART SAV SIE BCN MAD VAL ZAR DIJ ROU LIL TLS);
	my @ag=&lst_agence("ALL");
	my $reqal;
	if ($agenceal eq "ALL")
	 {
	     for(my $i=0;$i<@ag;$i++)
	      {
	      	$reqal=qq{UPDATE AUTH.`ALERTE` SET AL$champal=AL$champal $epsilon WHERE AGAUTO="$ag[$i]"};
	$dbh->do($reqal);
	      	}
	}
	else {
	 $reqal=qq{UPDATE AUTH.`ALERTE` SET AL$champal=AL$champal $epsilon WHERE AGAUTO="$agenceal"};
	# print "magal=$reqal\n";
	$dbh->do($reqal);
	}



}

sub  alerte_null {
	my @param=@_;
	my $agenceal=$param[0];
	#my $baseal=$param[1];
	my $champal=$param[1];
	#my $epsilon=$param[3];
	#if ($epsilon >0)
	 #{
	 #	$epsilon=qq{+$epsilon};
	#}
	#my @ag=qw(AIX TON REN LYO PAN SAR POI ART SAV SIE BCN MAD VAL ZAR DIJ ROU LIL TLS);
	my @ag=&lst_agence("ALL");
	my $reqal;
	if ($agenceal eq "ALL")
	 {
	     for(my $i=0;$i<@ag;$i++)
	      {
	      	$reqal=qq{UPDATE AUTH.`ALERTE` SET AL$champal="0" WHERE AGAUTO="$ag[$i]"};
	$dbh->do($reqal) || die "alterte $reqal\n";
	      	}
	}
	else {
	 #print "req=$reqal\n";
	 $reqal=qq{UPDATE AUTH.`ALERTE` SET AL$champal="0" WHERE AGAUTO="$agenceal"};
	#print "req=$reqal\n";
	$dbh->do($reqal) || die "alterte $reqal\n";
	}



}

sub lst_agence {
my @param=@_;
my $where="";
my @agence=();
if ($param[0] ne "ALL")
	{
	$where=qq{WHERE PACODE="$param[0]"};
	}
my $reqag=qq{select AGAUTO FROM AUTH.`AGENCE` $where ORDER BY AGAUTO};

my $stag=$dbh->prepare($reqag);
$stag->execute();
while (my @data=$stag->fetchrow_array())
         	{
         	push @agence , $data[0];
         	}
$stag->finish;

return @agence;
}

1;
