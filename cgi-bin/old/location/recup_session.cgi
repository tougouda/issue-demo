#!/usr/bin/perl

use lib '../lib';

use DBI;
use strict;
use accesbd;
use varglob;
use autentif;

# Connexion � la base de donn�es
my $dbh = &Environnement("LOCATION");


print "Content-type: text/html; charset=CP1252\n\n";


# Info bulle
print qq{
<DIV id="bulle" style="font-family: Tahoma, Arial, Helvetica, Sans-Serif; font-size: 8pt; background-color: #ffffe1; border: 1px solid black; padding: 0px 3px; position: absolute; top: 200px; left: 150px; visibility: hidden;">
</DIV>
};

# Requ�tes SQL
my @req;
$req[0] = qq{
    SELECT PENOM, PEPRENOM, AGAUTO, SESSIONIP,
        DATE_FORMAT(SESSIONDD, "%d/%m/%Y %H:%i:%S") AS SESSIONDD,
        DATE_FORMAT(SESSIONDF, "%d/%m/%Y %H:%i:%S") AS SESSIONDF,
        SESSIONAUTO
    FROM AUTH.`SESSION`
    LEFT JOIN AUTH.`PERSONNEL` ON PERSONNEL.PELOGIN=SESSION.SESSIONLOGIN
    WHERE SESSIONDD<=NOW() AND SESSIONDF>=NOW()
    ORDER BY SESSION.SESSIONDF DESC, SESSIONIP
};
$req[1] = qq{
    SELECT DISTINCT ALERTSESSIONIP,
        DATE_FORMAT(ALTERTSESSIONDATE, "%d/%m/%Y %H:%i:%S") AS ALTERTSESSIONDATE
    FROM AUTH.`ALERTESESSION`
    WHERE ALTERTSESSIONDATE IS NOT NULL AND SESSIONAUTO=num
};
$req[2] = qq{
    SELECT DISTINCT SESSIONIMPDOC,
        DATE_FORMAT(DATE(SESSIONIMPDATE), "%d/%m/%Y") AS SESSIONIMPDATE,
        TIME_FORMAT(TIME(SESSIONIMPDATE), "%H:%i:%S") AS SESSIONIMPHEURE,
        SESSIONIMPIDENTIF
    FROM AUTH.`SESSIONIMP`
    WHERE SESSIONIMPDATE IS NOT NULL AND SESSIONAUTO=num
        AND SESSIONIMP.SESSIONIMPDATE>=DATE_SUB(NOW(), INTERVAL 15 MINUTE)
};

my $option = qq{ AND SESSION.SESSIONAUTO="num"};

# Liste des sessions en cours
my $sth = $dbh->prepare($req[0]);
$sth->execute() or print $sth->errstr;
my $nbrow = $sth->rows;

my $style = qq{padding: 2px 10px;};
my $style_imp = qq{padding: 0px 10px 0px 0px;};

if ($nbrow > 0) {
    my $mess_sessions = "session";
    $mess_sessions = "sessions"   if ($nbrow >= 2);
    print qq{
        <TABLE align="center"><TR><TD>
            <FIELDSET><LEGEND>$nbrow $mess_sessions en cours</LEGEND>
    };
    print qq{
        <TABLE>
            <TR bgcolor="#C6CDC5">
                <TD style="$style"><B>Session</B></TD>
                <TD style="$style"><B>Utilisateur</B></TD>
                <TD style="$style"><B>Agence</B></TD>
                <TD style="$style"><B>Adresse IP</B></TD>
                <TD style="$style"><B>D�but de session</B></TD>
                <TD style="$style"><B>Fin th�orique de session</B></TD>
            </TR>
    };

 	my $i = 0;
 	while (my $data = $sth->fetchrow_hashref()) {
 		my @color = ("", "#D6D6D6");

        # Requ�te impressions en cours
        my $req = $req[2];
        $req =~ s/num/$data->{"SESSIONAUTO"}/g;
        my $sth_imp = $dbh->prepare($req);
        $sth_imp->execute() or print $sth_imp->errstr;
        my $nbimp = $sth_imp->rows;
        
        my $agrandir = "";
        if ($nbimp > 0) {
            $agrandir = qq{<A href="javascript:;"><IMG id="img$data->{"SESSIONAUTO"}" src="$URL_IMG/agrandir.gif" alt="Impressions" border="0" onclick="bloc('$data->{"SESSIONAUTO"}')"></A>};
        }

        # Requ�te doublons de connexion
        $req = $req[1];
        $req =~ s/num/$data->{"SESSIONAUTO"}/g;
        my $sth_con = $dbh->prepare($req);
        $sth_con->execute() or print $sth_con->errstr;
        my $nbcon = $sth_con->rows;

        my $dblcon = "";
        if ($nbcon > 0) {
            my $text = ($nbcon + 1)." connexions<BR>";
            while (my $data_con = $sth_con->fetchrow_hashref()) {
                $text .= "&nbsp;&nbsp;&ndash;&nbsp;".$data_con->{"ALERTSESSIONIP"}."<BR>";
            }
            $dblcon = qq{<IMG src="$URL_IMG/fire.gif" alt="" border="0" onmouseover="afficherbulle(event, '$text')" onmouseout="cacherbulle()">};
        }
        
        $sth_con->finish;

	 	print qq{
            <TR bgcolor="$color[$i % 2]">
                <TD style="$style">
                    <TABLE width="100%" cellspacing="0" cellpadding="0">
                        <TR>
                            <TD align="left">$agrandir&nbsp;</TD>
                            <TD align="right">$data->{"SESSIONAUTO"}</TD>
                        </TR>
                    </TABLE>
                </TD>
                <TD style="$style">
                    <TABLE width="100%" cellspacing="0" cellpadding="0">
                        <TR>
                            <TD align="left">$data->{"PEPRENOM"} }.uc($data->{"PENOM"}).qq{</TD>
                            <TD align="right">$dblcon</TD>
                        </TR>
                    </TABLE>
                </TD>
                <TD style="$style">$data->{"AGAUTO"}</TD>
                <TD style="$style">$data->{"SESSIONIP"}</TD>
                <TD style="$style">$data->{"SESSIONDD"}</TD>
                <TD style="$style">$data->{"SESSIONDF"}</TD>
            </TR>
        };
        
        # Liste des impressions en cours
        if ($nbimp > 0) {
            print qq{
                <TR bgcolor="$color[$i % 2]">
                    <TD></TD>
                    <TD colspan="5" style="$style">
                    <DIV id="imp$data->{"SESSIONAUTO"}" style="display: none;">
                        <TABLE cellspacing="0" cellpadding="0">
            };
            while (my $data_imp = $sth_imp->fetchrow_hashref()) {
                print qq{
                            <TR>
                                <TD style="$style_imp">$data_imp->{"SESSIONIMPDATE"}</TD>
                                <TD style="$style_imp">$data_imp->{"SESSIONIMPHEURE"}</TD>
                                <TD style="$style_imp">$data_imp->{"SESSIONIMPDOC"}</TD>
                            </TR>
                };
            }
            print qq{
                        </TABLE>
                    </DIV>
                    </TD>
                </TR>
            };
        }
        
        $sth_imp->finish;

	 	$i++;
	}
    print qq{
        </TABLE>
    };
    print qq{
            </FIELDSET>
        </TD></TR></TABLE>
    };
}
else {
    print qq{Aucune session en cours.};
}
 
$sth->finish;

$dbh->disconnect;
