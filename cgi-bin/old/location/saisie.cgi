#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';

use CGI;
use autentif;
use calculdate;
use titanic;
use accesbd;
use varglob;
use stock;
use Date::Calc qw(Day_of_Week Delta_Days Add_Delta_Days check_date Days_in_Month);
use message;
use fonctions;
use mymath;
use tracetemps;
use SOAP::Lite;
use MIME::Words;
use LOC::Globals;
use LOC::Util;
use LOC::Alert;
use LOC::Proforma;
use LOC::Characteristic;
use LOC::Transport;
use LOC::EndUpdates;
use LOC::Machine;
use LOC::Request;
use LOC::StartCode::Contract::Rent;
use LOC::Insurance::Type;
use LOC::Appeal::Type;

# Initialisation du chrono
my $timer = &init_chrono();

#connexion
my $DB=&Environnement("LOCATION");

#initialisation CGI
my $cgi=new CGI;
$cgi->autoEscape(undef);
my @mess=&message($G_PAYS);

$| = 1;


#type / menu
my $nomfh=$cgi->param('type');
my $fich=$cgi->param('menu');

#Variables
my $titre;
my %tabDocumentsToExpire;

my $nbchamps;
my @entityId;
my @nameform;
my @typeform;
my @libform;
my @oblform;
my @reqform;
my @reqtrait;

my @fieldsToDisplay;

my $nbchstock;
my @libstock;
my @optstock;

my @emailnvcl;

my $i;
my $j;
my $l;

my $ident;
my $cp;
my $eps;
my $sth;
my @data;
my @req;

my $agence=$G_AGENCE;

&init_var;

&ente($titre, $titre, $G_PENOM, $G_PEPRENOM.", ".$G_AGENCE);

if ($nomfh eq "chmach")
{
    print qq{<SCRIPT language="JavaScript" src="$HTTP_PORTAIL/web/js/common.js"></SCRIPT>};
    print qq{<SCRIPT language="JavaScript" src="$HTTP_PORTAIL/web/js/httpRequest.js"></SCRIPT>};

    print '
    <script type="text/javascript">
        var printHttpObj;
        function printChangeMachine (file, printer)
        {
            printer = printer.substr(0, 7);
            var url     = "' . &LOC::Request::createRequestUri('print:render:print') . '";

            if (!printHttpObj)
            {
                printHttpObj = new Location.HTTPRequest(url);
                // Codes de retour valide :
                // 204 : pas de contenu (code de retour normal)
                // 1223 : code transform� par IE
                // 0 : code transform� par Google Chrome Frame
                printHttpObj.setValidResponseStatusRegExp("^0|204|1223$");
            }

            printHttpObj.oncompletion = function()
            {
            };
            printHttpObj.onerror = function()
            {
                // Erreur
                alert("Erreur " + printHttpObj.getResponseStatus());
            };

            // Passage de l\'id du client en param�tre
            printHttpObj.setVar(\'printer\', printer);
            printHttpObj.setVar(\'url\', file);
            printHttpObj.setVar(\'number\', 1);

            // Envoi de la requ�te HTTP
            printHttpObj.send(true);

            return true;
        }
    </script>
    ';
}

print qq{
<style>
.disabled {
    color: gray;
    background-color: lightgrey;
}
</style>
};

my $frsaisi = $cgi->param('nbsaisie');

if (($nomfh eq "chmach")&&($cgi->param('imprimer.x') !=0)) {&up_mach;};
if (($frsaisi eq "oui")&&($cgi->param('saisie.x') !=0)) {
    if ($nomfh eq "chmach") {&chg_machine;}
    if ($nomfh eq "anncl") {&anncl;}
    if ($nomfh eq "chcl") {&form_chgt;}
}
else {
    &saisie;
}
$DB->disconnect;
print qq{</table>\n};

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

# Pied de la page HTML
my $paramType = $cgi->param("type");
my $paramMenu = $cgi->param("menu");
&Pied($delta_time, $paramType, $paramMenu);



#
#Initialisation des variables
#
sub init_var {
    if ($nomfh eq 'anncl') {
        &init_var_anncl;
    }
    elsif ($nomfh eq 'chmach') {
        &init_var_chmach;
        &init_var_stock;
    }
    elsif ($nomfh eq 'chcl') {
        &init_var_chcl;
    }
};


#
#Initialisation des variables pour le stock
#
sub init_var_stock {
    $nbchstock=16;
    for (my $i=0;$i<$nbchstock;$i++) {
        $libstock[$i]=$mess[$i+2700];
    }
    $optstock[5]="abs";
    $optstock[6]="abs";
    $optstock[7]="abs";
    $optstock[8]="abs";
    $optstock[9]="abs";
    $optstock[10]="abs";
    $optstock[11]="abs";
    $optstock[12]="abs";
    $optstock[13]="abs";
    $optstock[14]="abs";
    $optstock[15]="abs";
};

#
#Initialisation des variables pour le remplacement d'un client par un autre
#
sub init_var_anncl {
    $titre=$mess[3050];

    $nbchamps=3;

    $nameform[0]="mvclient";
    $nameform[1]="bnclient";
    $nameform[2]="saisie";

    $typeform[0]="listesql";
    $typeform[1]="listesql";
    $typeform[2]="submit";

    $oblform[0]="oui";
    $oblform[1]="oui";

    $reqform[0]='
SELECT
    CLSTE AS `customer.label`,
    CLCODE AS `customer.code`,
    concat_ws(" ", CLADRESSE, CLADRESSE2) as `customer.address`,
    CLTEL AS `customer.telephone`,
    VICLCP AS `customer.locality.postalCode`,
    VICLNOM AS `customer.locality.city`,
    CLAUTO AS `customer.id`
FROM CLIENT
LEFT JOIN AUTH.`VICL` ON VICL.VICLAUTO = CLIENT.VICLAUTO
WHERE CLIENT.CLSTE LIKE "ident%"
    OR CLCODE like "ident%"
ORDER BY CLSTE';

    $reqform[1]='
SELECT
    CLSTE AS `customer.label`,
    CLCODE AS `customer.code`,
    concat_ws(" ", CLADRESSE, CLADRESSE2) as `customer.address`,
    CLTEL AS `customer.telephone`,
    VICLCP AS `customer.locality.postalCode`,
    VICLNOM AS `customer.locality.city`,
    CLSAGE AS `customer.isLocked`,
    CLAUTO AS `customer.id`,
    CLCOLLECTIF AS `customer.accountingCode`
FROM CLIENT
LEFT JOIN AUTH.`VICL` ON VICL.VICLAUTO = CLIENT.VICLAUTO
WHERE CLIENT.CLSTE LIKE "ident%"
    OR CLCODE like "ident%"
ORDER BY CLSTE';

    $entityId[0] = 'customer.id';
    $fieldsToDisplay[0] = [
        'customer.label',
        'customer.code',
        'customer.address',
        'customer.telephone',
        'customer.locality.postalCode',
        'customer.locality.city'
    ];
    $entityId[1] = 'customer.id';
    $fieldsToDisplay[1] = [
        'customer.label',
        'customer.code',
        'customer.address',
        'customer.telephone',
        'customer.locality.postalCode',
        'customer.locality.city'
    ];

    $reqtrait[0]=qq{UPDATE CONTRAT SET CLAUTO="val0", CONTRATVERIFIER="0" WHERE CLAUTO="val1"};
    $reqtrait[1]=qq{UPDATE DEVIS SET CLAUTO="val0" WHERE CLAUTO="val1"};
    $reqtrait[2]=qq{INSERT INTO TRACE (CONTRATAUTO, ETCODE, LOAUTO, PEAUTO) VALUES ("val0", "TRA27", 6, "agent")};
    $reqtrait[3]=qq{DELETE FROM CLIENT WHERE CLAUTO="val1"};
    $reqtrait[4]=qq{UPDATE $GESCOM_BASE{"$G_PAYS"}.GC_GRILLE SET CLAUTO="val0" WHERE CLAUTO="val1"};
    $reqtrait[5]=qq{UPDATE $GESCOM_BASE{"$G_PAYS"}.GC_ALERTES SET CLAUTO="val0" WHERE CLAUTO="val1"};
    $reqtrait[6]=qq{UPDATE $GESTIONTRANS_BASE{"$G_PAYS"}.TRANSPORT SET CLIENT="val0", CLASSE=(SELECT CLCLASSE FROM CLIENT WHERE CLAUTO="val0") WHERE CLIENT="val1"};

    for (my $i=0;$i<$nbchamps;$i++) {
        $libform[$i]=$mess[$i+3051];
    }
    for (my $i=0;$i<@reqform;$i++) {
        $reqform[$i]=~ s/agent/$G_PEAUTO/;
        $reqform[$i]=~ s/agence/$G_AGENCE/;
    }
    for (my $i=0;$i<@reqtrait;$i++) {
        $reqtrait[$i]=~ s/agent/$G_PEAUTO/;
        $reqtrait[$i]=~ s/agence/$G_AGENCE/;
    }
};

#
#Initialisation des variables pour le changement de client sur un contrat
#
sub init_var_chcl {
    $titre=$mess[3100];

    $nbchamps=3;

    $nameform[0]="mvclient";
    $nameform[1]="bnclient";
    $nameform[2]="saisie";

    $typeform[0]="listesql";
    $typeform[1]="listesql";
    $typeform[2]="submit";

    $oblform[0]="oui";
    $oblform[1]="oui";

    $reqform[0]='
SELECT
    CLSTE AS `customer.label`,
    CONTRATCODE AS `contract.code`,
    CLIENT.CLCODE AS `customer.code`,
    concat_ws(" ", CLADRESSE, CLADRESSE2) AS `customer.address`,
    CLTEL AS `customer.telephone`,
    VICLCP AS `customer.locality.postalCode`,
    VICLNOM AS `customer.locality.city`,
    CONTRAT.CONTRATAUTO AS `contract.id`,
    CLNETTOYAGE AS `customer.cleaning`,
    CLDECHETSFACT AS `customer.residues`,
    CLTYPEASSURANCE AS `customer.insuranceType.id`,
    IF (ity_flags & ' . LOC::Insurance::Type::FLAG_ISINVOICED . ', 1, 0) AS `customer.insuranceType.isInvoiced`,
    CLTYPERECOURS AS `customer.appealType.id`,
    apl_label AS `customer.appealType.label`,
    CLPASSURANCE AS `customer.insuranceRate`,
    CONTRATTYPEASSURANCE AS `contract.insuranceType.id`,
    CONTRATPASSURANCE AS `contract.insuranceRate`,
    CONTRATFULLSERVICE `contract.isFullService`
FROM CONTRAT
LEFT JOIN CLIENT ON CONTRAT.CLAUTO = CLIENT.CLAUTO
LEFT JOIN AUTH.`VICL` ON VICL.VICLAUTO = CLIENT.VICLAUTO
LEFT JOIN  p_insurancetype on ity_id = CLTYPEASSURANCE
LEFT JOIN  p_appealtype on apl_id = CLTYPERECOURS
WHERE (CLIENT.CLSTE LIKE "ident%" OR CLIENT.CLCODE like "ident%" OR CONTRATCODE LIKE "ident%")
    AND CONTRATOK in ("-1","-2")
    AND LIETCONTRATAUTO IN ("1", "2", "3")
    AND CONTRAT.AGAUTO = "agence"
    AND CLIENT.CLCOLLECTIF != "4129999"
ORDER BY CLSTE';

    $reqform[1]='
SELECT
    CLSTE AS `customer.label`,
    CLIENT.CLCODE AS `customer.code`,
    concat_ws(" ", CLADRESSE, CLADRESSE2) AS `customer.address`,
    CLTEL AS `customer.telephone`,
    VICLCP AS `customer.locality.postalCode`,
    VICLNOM AS `customer.locality.city`,
    CLSAGE AS `customer.isLocked`,
    CLAUTO AS `customer.id`,
    CLCOLLECTIF AS `customer.accountingCode`,
    CLNETTOYAGE AS `customer.cleaning`,
    CLDECHETSFACT AS `customer.residues`,
    CLTYPEASSURANCE AS `customer.insuranceType.id`,
    IF (ity_flags & ' . LOC::Insurance::Type::FLAG_ISINVOICED . ', 1, 0) AS `customer.insuranceType.isInvoiced`,
    CLPASSURANCE AS `customer.insuranceRate`,
    CLTYPERECOURS AS `customer.appealType.id`,
    apl_label AS `customer.appealType.label`
FROM CLIENT
LEFT JOIN AUTH.`VICL` ON VICL.VICLAUTO = CLIENT.VICLAUTO
LEFT JOIN  p_insurancetype on ity_id = CLTYPEASSURANCE
LEFT JOIN  p_appealtype on apl_id = CLTYPERECOURS
WHERE ( CLIENT.CLSTE LIKE "ident%" OR CLCODE like "ident%")
AND CLIENT.CLCOLLECTIF != "4129999"
ORDER BY CLSTE';

    $entityId[0] = 'contract.id';
    $fieldsToDisplay[0] = [
        'customer.label',
        'contract.code',
        'customer.code',
        'customer.address',
        'customer.telephone',
        'customer.locality.postalCode',
        'customer.locality.city'
    ];
    $entityId[1] = 'customer.id';
    $fieldsToDisplay[1] = [
        'customer.label',
        'customer.code',
        'customer.address',
        'customer.telephone',
        'customer.locality.postalCode',
        'customer.locality.city'
    ];

    $reqtrait[0]=qq{UPDATE CONTRAT SET CLAUTO="val0" WHERE CONTRATAUTO="val1"};
    $reqtrait[1]=qq{INSERT INTO TRACE (CONTRATAUTO, ETCODE, LOAUTO, PEAUTO, DEVISAUTO) VALUES ("val1", "TRA29", 6, "agent", "val0")};
    $reqtrait[2]=qq{UPDATE $GESTIONTRANS_BASE{"$G_PAYS"}.TRANSPORT SET CLIENT="val0" WHERE VALEUR="val1"};
    $reqtrait[3]=qq{UPDATE $GESCOM_BASE{"$G_PAYS"}.GC_ALERTES SET CLAUTO="val0" WHERE CONTRATAUTO="val1"};
    $reqtrait[4]=qq{UPDATE $GESCOM_BASE{"$G_PAYS"}.GC_ARCHIVES SET CLAUTO="val0" WHERE CONTRATAUTO="val1"};

    for (my $i=0;$i<$nbchamps;$i++) {
        $libform[$i]=$mess[$i+3101];
    }
    for (my $i=0;$i<@reqform;$i++) {
        $reqform[$i]=~ s/agent/$G_PEAUTO/;
        $reqform[$i]=~ s/agence/$G_AGENCE/;
    }
    for (my $i=0;$i<@reqtrait;$i++) {
        $reqtrait[$i]=~ s/agent/$G_PEAUTO/;
        $reqtrait[$i]=~ s/agence/$G_AGENCE/;
    }
    #Nouveau client
    @emailnvcl = split(',', &LOC::Characteristic::getCountryValueByCode('STANVCLIENT', $G_PAYS));
};

#
#Initialisation des variables pour le changement de machine sur un contrat
#
sub init_var_chmach {
    $titre=$mess[3150];

    $nbchamps=3;

    $nameform[0]="contrat";
    $nameform[1]="machine";
    $nameform[2]="saisie";

    $typeform[0]="listesql";
    $typeform[1]="listesql";
    $typeform[2]="submit";

    $oblform[0]="oui";
    $oblform[1]="oui";

    $reqform[0] = '
SELECT
    CONTRATCODE AS `contract.code`,
    CLSTE AS `customer.label`,
    MOMADESIGNATION AS `model.label`,
    CONCAT_WS("", MACHINE.MANOPARC_AUX, MACHINE.MANOPARC, IF (MACHINE.MASUPPAGAUTO = "agence", CONCAT("@", MACHINE.AGAUTO), "")) AS `machine.parkNumber`,
    DATE_FORMAT(CONTRATDR, "%d/%m/%Y") AS `contract.endDate`,
    CONTRAT.CONTRATAUTO AS `contract.id`,
    CONTRAT.CONTRATHORSPARC AS `contract.isOutOfPark`
FROM CONTRAT
INNER JOIN f_transport tsp_d
ON tsp_d.tsp_rct_id_to = CONTRAT.CONTRATAUTO AND tsp_d.tsp_type = "' . LOC::Transport::SIDE_DELIVERY . '" AND tsp_d.tsp_sta_id = "' . LOC::Transport::STATE_ACTIVE . '"
INNER JOIN f_transport tsp_r
ON tsp_r.tsp_rct_id_from = CONTRAT.CONTRATAUTO AND tsp_r.tsp_type = "' . LOC::Transport::SIDE_RECOVERY . '" AND tsp_r.tsp_sta_id = "' . LOC::Transport::STATE_ACTIVE . '"
LEFT JOIN AUTH.MACHINE
ON MACHINE.MAAUTO = CONTRAT.MAAUTO
LEFT JOIN CLIENT
ON CONTRAT.CLAUTO = CLIENT.CLAUTO
LEFT JOIN MONTANT
ON CONTRAT.MOAUTO = MONTANT.MOAUTO
LEFT JOIN AUTH.MODELEMACHINE
ON MODELEMACHINE.MOMAAUTO = CONTRAT.MOMAAUTO
WHERE CONTRATOK = -1
AND CONTRAT.AGAUTO = "agence"
AND LIETCONTRATAUTO IN (1, 2)
AND CLIENT.CLCODE != ""
AND (CLIENT.CLSTE LIKE "ident%" OR CONTRATCODE LIKE "ident%" OR MANOPARC LIKE "ident%" OR MOMADESIGNATION LIKE "ident%")
AND (MOREMISEOK = -2 OR (MOREMISEOK = 0 AND MOREMISEEX = 0))
ORDER BY CONTRATCODE;';
    $reqform[1]='
SELECT
    CONCAT_WS("", MACHINE.MANOPARC_AUX, MACHINE.MANOPARC, IF (MACHINE.MASUPPAGAUTO = "agence", CONCAT("@", MACHINE.AGAUTO), "")) AS `machine.parkNumber`,
    MOMADESIGNATION AS `model.label`,
    FAMAMODELE AS `machinesFamily.label`,
    MACHINE.MAAUTO AS `machine.id`
FROM AUTH.`MACHINE`
LEFT JOIN AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=MACHINE.MOMAAUTO
LEFT JOIN FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
WHERE (MACHINE.AGAUTO = "agence" AND MACHINE.MASUPPAGAUTO IS NULL OR MACHINE.MASUPPAGAUTO = "agence")
AND LIETCODE = "2"
AND MAHORSPARC = "val1"
AND (MANOPARC LIKE "ident%" OR MOMADESIGNATION LIKE "ident%" OR FAMAMODELE LIKE "ident%")
ORDER BY MACHINE.MANOPARC';

    $entityId[0] = 'contract.id';
    $fieldsToDisplay[0] = [
        'contract.code',
        'customer.label',
        'model.label',
        'machine.parkNumber',
        'contract.endDate'
    ];
    $entityId[1] = 'machine.id';
    $fieldsToDisplay[1] = [
        'machine.parkNumber',
        'model.label',
        'machinesFamily.label'
    ];

    $reqtrait[0]=qq{UPDATE AUTH.`MACHINE` SET ETCODE="MAC07", LIETCODE="7" WHERE MAAUTO="val0"};
    $reqtrait[1]=qq{UPDATE AUTH.`STOCKMODELE` SET STMORESERVABLE=STMORESERVABLE+1, STMOLIV=STMOLIV-1, STMOVER=STMOVER+1 WHERE MOMAAUTO="val1" AND AGAUTO="agence"};
    $reqtrait[2]=qq{INSERT INTO TRACE (CONTRATAUTO, ETCODE, LOAUTO, PEAUTO, MAAUTO) VALUES ("val2", "TRA31", 3, "agent", "val0")};
    $reqtrait[3]=qq{UPDATE AUTH.`MACHINE` SET ETCODE="MAC12", LIETCODE="12", CONTRATAUTO="val2" WHERE MAAUTO="val3"};
    $reqtrait[4]=qq{UPDATE CONTRAT SET MAAUTO="val3", MOMAAUTO="val4", CONTRATMACHATTR = -1, MAAUTO_SOUHAITE = NULL WHERE CONTRATAUTO="val2"};
    $reqtrait[5]=qq{UPDATE AUTH.`STOCKMODELE` SET STMORESERVABLE=STMORESERVABLE-1, STMOLIV=STMOLIV+1, STMODIS=STMODIS-1 WHERE MOMAAUTO="val4" AND AGAUTO="agence"};
    $reqtrait[6]=qq{UPDATE AUTH.`STOCKMODELE` SET STMORESERVE=STMORESERVE-1 WHERE MOMAAUTO="val1" AND AGAUTO="agence"};
    $reqtrait[7]=qq{UPDATE CONTRATNETTOYAGE SET NETAUTO="val5", CONETMONTANT="val6", CONETMONTANTEU="val6a" WHERE CONTRATAUTO="val2"};
    $reqtrait[8]=qq{UPDATE MONTANT SET MOHTFRS = MOHTFRS + val7,MOHTEU = MOHTEU + val9 WHERE MOAUTO="val8"};

    for (my $i=0;$i<$nbchamps;$i++) {
        $libform[$i]=$mess[$i+3151];
    }
    for (my $i=0;$i<@reqform;$i++) {
        $reqform[$i]=~ s/agent/$G_PEAUTO/;
        $reqform[$i]=~ s/agence/$G_AGENCE/g;
    }
    for (my $i=0;$i<@reqtrait;$i++) {
        $reqtrait[$i]=~ s/agent/$G_PEAUTO/;
        $reqtrait[$i]=~ s/agence/$G_AGENCE/;
    }
};


sub up_mach
{
    my $countryId = $G_PAYS;
    my $userId    = $G_PEAUTO;

    my $contractId   = $cgi->param('contrat') * 1;
    my $newMachineId = $cgi->param('machine') * 1;
    my $query;

    # R�cup�ration de l'id et du mod�le de la machine pr�sente sur le contrat
    $query = '
SELECT
    C.CONTRATCODE AS `contract.code`,
    C.MAAUTO AS `machine.id`,
    M.ETCODE AS `machine.state.id`,
    C.MOMAAUTO AS `model.id`,
    MM.MOMAGRP AS `isAccessory`,
    C.AGAUTO AS `agency.id`,
    C.MAAUTO_SOUHAITE AS `wishedMachine.id`
FROM CONTRAT C
LEFT JOIN AUTH.MACHINE M
ON C.MAAUTO = M.MAAUTO
LEFT JOIN AUTH.MODELEMACHINE MM
ON C.MOMAAUTO = MM.MOMAAUTO
WHERE C.CONTRATAUTO = ' . $contractId . ';';
    my $stmt = $DB->prepare($query);
    $stmt->execute();
    my $tabRow = $stmt->fetchrow_hashref();
    $stmt->finish();

    my $agencyId           = $tabRow->{'agency.id'};
    my $contractCode       = $tabRow->{'contract.code'};
    my $oldMachineId       = $tabRow->{'machine.id'} * 1;
    my $oldModelId         = $tabRow->{'model.id'} * 1;
    my $oldIsAccessory     = $tabRow->{'isAccessory'} * 1;
    my $oldMachineStateId  = $tabRow->{'machine.state.id'};
    my $oldWishedMachineId = ($tabRow->{'wishedMachine.id'} ? $tabRow->{'wishedMachine.id'} : undef);

    # R�cup�ration de la date de valeur du contrat
    my $contractValueDate = LOC::Contract::Rent::getValueDate($countryId, $contractId);


    if ($newMachineId == $oldMachineId)
    {
        print qq{<center><font class="GD" color="red"><b>$mess[3154]</b></font></center>\n};
    }
    else
    {
        my $isExpireProformas = 0;
        my $tabEndUpds = {};
        my $schema = &LOC::Db::getSchemaName($countryId);

        # Y'avait-il une machine attribu�e ?
        if ($oldMachineId)
        {
            # Mise � jour de l'�tat de l'ancienne machine : passe en v�rif
            &LOC::Machine::update($countryId, $oldMachineId, {'state.id' => LOC::Machine::STATE_CHECK}, $userId);
        }
        else
        {
            # Mise � jour du nombre de machines r�serv�es pour l'ancien mod�le de machine
            # (STMORESERVE--)
            $query = $reqtrait[6];
            $query =~ s/val1/$oldModelId/;
            $DB->do($query);

            # D�cr�mentation de l'alerte des "Machine(s) � attribuer"
            alerte_pl_ms($agence, $DB, LOC::Alert::MACHINESTOALLOCATE, -1);
        }

        # Attribution d�finitive sur le contrat
        # - R�cup�ration des informations sur la nouvelle machine
        $query = '
SELECT
    M.MOMAAUTO AS `model.id`,
    FM.FAMAENERGIE AS `energy`,
    MM.MOMAGRP AS `isAccessory`
FROM AUTH.MACHINE M
LEFT JOIN AUTH.MODELEMACHINE MM
ON M.MOMAAUTO = MM.MOMAAUTO
LEFT JOIN FAMILLEMACHINE FM
ON MM.FAMAAUTO = FM.FAMAAUTO
WHERE MAAUTO = ' . $newMachineId . ';';
        $stmt = $DB->prepare($query);
        $stmt->execute();
        my $tabRow = $stmt->fetchrow_hashref();
        $stmt->finish();

        my $newModelId     = $tabRow->{'model.id'} * 1;
        my $newEnergy      = $tabRow->{'energy'};
        my $newIsAccessory = $tabRow->{'isAccessory'} * 1;

        # - Mise � jour de la machine et du mod�le de machine sur le contrat
        $query = $reqtrait[4];
        $query =~ s/val4/$newModelId/;
        $query =~ s/val3/$newMachineId/;
        $query =~ s/val2/$contractId/;
        $DB->do($query);

        # Ajout d'un trace "Changement de la machine sur le contrat" (TRA31)
        $query = $reqtrait[2];
        $query =~ s/val2/$contractId/;
        $query =~ s/val0/$oldMachineId/;
        $DB->do($query);

        # Passage de la nouvelle machine en "Livraison" sur le contrat s�lectionn�
        my $tabData = {
            'state.id'    => LOC::Machine::STATE_DELIVERY,
            'contract.id' => $contractId
        };
         &LOC::Machine::update($countryId, $newMachineId, $tabData, $userId);

        # Trace sur l'ancienne machine
        # - D�sattribution d'un contrat
        my $tabContent = {
            'schema'  => $schema,
            'code'    => LOC::Log::EventType::CONTRACTDEALLOCATION,
            'old'     => $contractId,
            'new'     => '',
            'content' => $contractCode . " \N{U+2192} "
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $oldMachineId, $tabContent);

        # Traces sur la nouvelle machine
        # - Attribution sur un contrat
        my $tabContent = {
            'schema'  => $schema,
            'code'    => LOC::Log::EventType::CONTRACTALLOCATION,
            'old'     => '',
            'new'     => $contractId,
            'content' => " \N{U+2192} " . $contractCode
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $newMachineId, $tabContent);

        # - Attribution de la machine
        my $tabContent = {
            'schema'  => $schema,
            'code'    => LOC::Log::EventType::MACHINEALLOCATION,
            'old'     => $oldMachineId,
            'new'     => $newMachineId,
            'content' => $oldMachineId . " \N{U+2192} " . $newMachineId
        };
        &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabContent);

        # - D�sattribution de la machine souhait�e s'il y en avait une
        if ($oldWishedMachineId)
        {
            my $tabContent = {
                'schema'  => $schema,
                'code'    => LOC::Log::EventType::CHANGEWISHEDMACHINE,
                'old'     => $oldWishedMachineId,
                'new'     => undef,
                'content' => $oldWishedMachineId . " \N{U+2192} "
            };
            &LOC::EndUpdates::addTrace($tabEndUpds, 'contract', $contractId, $tabContent);
        }

        # R�cup�ration des informations sur le nettoyage et la participation au recyclage
        $query = '
SELECT
    IFNULL(CN.CONETAUTO, 0) AS `id`,
    IFNULL(CN.CONETMONTANT, IFNULL(N.NETMONTANT, 0)) AS `amount`,
    IFNULL(CN.NETAUTO, 0) AS `type.id`,
    N.NETMONTANT AS `type.amount`,
    N.NETENERGIE AS `type.energy`,
    -C.CONTRATDECHETSFACT AS `residues.mode`,
    -CL.CLNETTOYAGE AS `customer.isCleaningInvoiced`
FROM CONTRAT C
INNER JOIN CLIENT CL
ON C.CLAUTO = CL.CLAUTO
INNER JOIN MONTANT M
ON C.MOAUTO = M.MOAUTO
LEFT JOIN CONTRATNETTOYAGE CN
ON C.CONTRATAUTO = CN.CONTRATAUTO
LEFT JOIN NETTOYAGE N
ON CN.NETAUTO = N.NETAUTO
WHERE C.CONTRATAUTO = ' . $contractId . ';';
        my $stmt = $DB->prepare($query);
        $stmt->execute();
        my $tabRow = $stmt->fetchrow_hashref();
        $stmt->finish();

        my $oldCleaningId               = $tabRow->{'id'} * 1;
        my $oldCleaningAmount           = $tabRow->{'amount'} * 1;
        my $oldCleaningTypeId           = $tabRow->{'type.id'} * 1;
        my $oldCleaningTypeAmount       = $tabRow->{'type.amount'} * 1;
        my $oldCleaningTypeEnergy       = $tabRow->{'type.energy'};
        my $oldResiduesMode             = $tabRow->{'residues.mode'} * 1;
        my $isCustomerCleaningInvoiced  = $tabRow->{'customer.isCleaningInvoiced'} * 1;


        # Mise � jour du nettoyage
        if (!$oldIsAccessory && $newIsAccessory && $oldCleaningId)
        {
            # Si on passe d'un mod�le standard � un accessoire alors on supprime le nettoyage
            $query = '
DELETE FROM CONTRATNETTOYAGE
WHERE CONETAUTO = ' . $oldCleaningId . ';';
            $DB->do($query);


            # Le nettoyage a chang�
            $isExpireProformas = 1;
        }
        elsif (($oldIsAccessory && !$newIsAccessory && $isCustomerCleaningInvoiced) ||
               ($oldCleaningTypeId && $oldCleaningTypeEnergy ne ''))
        {
            $query = '
SELECT
    N.NETAUTO,
    N.NETMONTANT
FROM NETTOYAGE N
WHERE N.NETENERGIE = "' . $newEnergy . '";';
            my $stmt = $DB->prepare($query);
            $stmt->execute();
            my ($newCleaningTypeId, $newCleaningAmount) = $stmt->fetchrow_array();
            $stmt->finish();

            if ($oldCleaningId)
            {
                if ($oldCleaningTypeId && $oldCleaningTypeEnergy ne '' &&
                    $oldCleaningAmount != $oldCleaningTypeAmount)
                {
                    $newCleaningAmount = $oldCleaningAmount;
                }

                if ($newCleaningTypeId != $oldCleaningTypeId || $newCleaningAmount != $oldCleaningAmount)
                {
                    # Mise � jour de la ligne de nettoyage existante
                    $query = '
UPDATE CONTRATNETTOYAGE
SET NETAUTO = ' . $newCleaningTypeId . ',
    CONETMONTANT = ' . $newCleaningAmount . ',
    CONETMONTANTEU = ' . &to_euro($newCleaningAmount, $G_MONNAIE) . '
WHERE CONETAUTO = ' . $oldCleaningId . ';';

                    # Le nettoyage a chang� ?
                    $isExpireProformas = 1;
                }
            }
            else
            {
                # Insertion d'une ligne de nettoyage
                $query = '
INSERT INTO CONTRATNETTOYAGE (CONTRATAUTO, NETAUTO, CONETMONTANT, CONETMONTANTEU)
VALUES (' . $contractId . ', ' . $newCleaningTypeId . ', ' . $newCleaningAmount . ', ' . &to_euro($newCleaningAmount, $G_MONNAIE) . ');';

                # Le nettoyage a chang� ?
                $isExpireProformas = 1;
            }

            $DB->do($query);
        }

        # Mise � jour de la participation au recyclage
        my $isResiduesChanged = 0;
        my $newResiduesMode;
        my $newResiduesValue;

        my $useResiduesForAccessories = &LOC::Characteristic::getAgencyValueByCode('ACCDECHETS', $agencyId, $contractValueDate) * 1;
        if ($useResiduesForAccessories == 0)
        {
            if (!$oldIsAccessory && $newIsAccessory && $oldResiduesMode != 0)
            {
                # Si on passe d'un mod�le standard � un accessoire alors on ne facture plus la participation au recyclage
                $isResiduesChanged = 1;
                $newResiduesMode  = 0;
                $newResiduesValue = undef;
            }
            elsif ($oldIsAccessory && !$newIsAccessory && $oldResiduesMode == 0)
            {
                # Si on passe d'un accessoire � un mod�le standard alors on facture la participation au recyclage
                my $defaultResiduesMode = &LOC::Characteristic::getAgencyValueByCode('DECHETSMODE', $agencyId, $contractValueDate) * 1;

                if ($defaultResiduesMode != LOC::Common::RESIDUES_MODE_NONE)
                {
                    $isResiduesChanged = 1;
                    $newResiduesMode = $defaultResiduesMode;
                    if ($newResiduesMode == LOC::Common::RESIDUES_MODE_PRICE)
                    {
                        $newResiduesValue = &LOC::Characteristic::getAgencyValueByCode('MTDECHETS', $agencyId, $contractValueDate) * 1;
                    }
                    elsif ($newResiduesMode == LOC::Common::RESIDUES_MODE_PERCENT)
                    {
                        $newResiduesValue = &LOC::Characteristic::getAgencyValueByCode('PCDECHETS', $agencyId, $contractValueDate) * 1;
                    }
                }
            }
        }

        if ($isResiduesChanged)
        {
            $query = '
UPDATE CONTRAT
SET CONTRATDECHETSFACT = ' . -$newResiduesMode . ',
    CONTRATDECHETSVAL = ' . ($newResiduesValue eq '' ? 'NULL' : $newResiduesValue) . '
WHERE CONTRATAUTO = ' . $contractId . ';';
            $DB->do($query);

            # La participation au recyclage a chang�
            $isExpireProformas = 1;
        }



        # Recalcul du montant total du contrat
        my $tabErrors = {
            'fatal' => []
        };
        &LOC::Contract::Rent::updateAmounts($countryId, $contractId, $tabErrors);


        # Si le montant du contrat a chang� alors on p�rime toutes ses pro formas
        if ($isExpireProformas)
        {
            &LOC::Proforma::expireAll($countryId, 'contract', $contractId, $userId, {}, $tabEndUpds);
        }
        elsif ($oldModelId != $newModelId)
        {
            # Sinon on remet � jour la ligne de location si il y a eu un changement de mod�le de machine dans le
            # cas o� il y a eu un changement de mod�le de machine
            my $tabFilters = {
                'stateId'        => [LOC::Proforma::STATE_ACTIVE, LOC::Proforma::STATE_ABORTED],
                'isExpired'      => 0,
                'estimateLineId' => undef
            };
            &LOC::Proforma::updateGroupLines($countryId, 'contract', $contractId, $tabFilters, ['modelOrPeriod'], $tabErrors)
        }

        # Mise � jour des transports
        &LOC::Transport::setDocumentDelivery($countryId, 'contract', $contractId, {}, $userId, {}, {}, $tabEndUpds);
        &LOC::Transport::setDocumentRecovery($countryId, 'contract', $contractId, {}, $userId, {}, {}, $tabEndUpds);

        # Mise � jour des codes de d�marrage
        &LOC::StartCode::Contract::Rent::save($countryId, $contractId, $userId, {}, {}, $tabEndUpds);

        # Lancement des mises � jour de fin
        &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);

        # Impression
        my $url = &LOC::Request::createRequestUri('technical:changeMachine:pdf',
                                                   {'contractId' => $contractId,
                                                    'machineId'  => $oldMachineId});
        my $printer = $cgi->param('imprimante');

        print '<script type="text/javascript">printChangeMachine("' . $url . '", "' . $printer . '")</script>';

    }
}

#
#annulation client
#
sub anncl {
    &saisie;
    print "</table>\n";
    print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"0\">\n";
    print $cgi->start_form(-method=>'get', action=>'saisie.cgi');
    print $cgi->hidden('type', $nomfh);
    for ($l=0;$l<$nbchamps;$l++) {
        $cgi->param("$nameform[$l]");
        $cgi->param("$nameform[$l]rech");
        print $cgi->hidden("$nameform[$l]");
        print $cgi->hidden("$nameform[$l]rech")
    }

    print qq{<center>\n};

    my $countryId = $G_PAYS;
    my $customerId = $cgi->param('mvclient');
    my $tabFilters = {
        'customerId' => $customerId,
        'isExpired'  => 0
    };
    $tabProformas = &LOC::Proforma::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters, LOC::Proforma::GETINFOS_CONTRACT | LOC::Proforma::GETINFOS_ESTIMATELINE);
    if (keys %$tabProformas > 0)
    {
        print '<b><font class="GD" color="red"><b>' . $mess[8103] . ' : <br/>';
#        recherche des contrats ayant une proforma

        my @tabCodes = ();
        foreach my $tabProformaInfos (values %$tabProformas)
        {
            my $documentType = defined $tabProformaInfos->{'contract.id'} ? 'contract' : 'estimateLine';
            my $documentId   = defined $tabProformaInfos->{'contract.id'} ? $tabProformaInfos->{'contract.id'} : $tabProformaInfos->{'estimateLine.id'};
            my $documentCode = defined $tabProformaInfos->{'contract.id'} ? $tabProformaInfos->{'contract.code'} : $tabProformaInfos->{'estimateLine.estimate.code'} . ' (' . $tabProformaInfos->{'estimateLine.index'} . ')';
            if (!exists $tabDocumentsToExpire{$documentType . '_' . $documentId})
            {
                $tabDocumentsToExpire{$documentType . '_' . $documentId} = {'type' => $documentType, 'id' => $documentId};
                my $documentUrl = '';
                if ($documentType eq 'contract')
                {
                    $documentUrl = &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $documentId});
                }
                else
                {
                    $documentUrl = &LOC::Request::createRequestUri('rent:rentEstimate:view', {'estimateId' => $tabProformaInfos->{'estimateLine.estimate.id'}});
                }
                my $lien = '<a href="' . $documentUrl . '" target="_blank">' . $documentCode . '</a>';
                push(@tabCodes, $lien);
            }
        }
        print join(', ', @tabCodes) . '.</b></font></b><br /><br />';
    }
    if ($cgi->param('annuc.x')==0) {
        print qq{<b><font class="GD" color="red"><b>$mess[1750]</b></font></b><br>\n};
        print $cgi->image_button(-name=>'annuc', -src=>"$URL_IMG/valid_off.gif", -align=>'center', -border=>0, -alt=>$mess[58]);
        print $cgi->hidden('nbsaisie');
        print $cgi->hidden('saisie.x', 1);
    }
    else {
        &supp_client;

    }
    print qq{</center>\n};
    print "</form>\n";
}

sub fact_existe
    {
    my @param=@_;
    my $req=qq|select CLCODE,CLSTE FROM CLIENT WHERE CLAUTO="$param[0]"|;
    my $sth=$DB->prepare("$req");
    $sth->execute();
    my ($clcode,$ste)=$sth->fetchrow_array();
    $sth->finish;
    my $nb=0;
    if ($clcode)
        {
        $req=qq|select count(*) FROM ENTETEFACTURE WHERE CLCODE="$clcode"|;
        my $sth=$DB->prepare("$req");
        $sth->execute();
        ($nb)=$sth->fetchrow_array();
        $sth->finish;
        }

    return ($ste,$nb);
    }

sub supp_client {
    my @val;

    my $countryId = $G_PAYS;
    my $userId = $G_PEAUTO;
    #mauvais client
    $val[1]=$cgi->param('mvclient');
    #bon cleint
    my @dt=&fact_existe($val[1]);

    $val[0]=$cgi->param('bnclient');
    if ($dt[1])
    {
        print qq|<font class="GD" color="red"><b>Impossible: le Client $dt[0] a $dt[1] facture(s)!!</b></font>\n|;
    }
    else
    {
        if ($val[1] ne $val[0])
        {
            my @req;
            my $bl0=0;
            my $bl1=0;
            #alerte prospect et nouvcleint;
            my $reqt=qq{select CLVERIFIER, CLNONVALIDE FROM CLIENT WHERE CLAUTO="$val[1]"};
            my $sth=$DB->prepare("$reqt");
            $sth->execute();
            while (my @data=$sth->fetchrow_array())
            {
                if ($data[0] !=0) {$bl0=1};
                if ($data[1] ==1) {$bl1=1};
            }
            $sth->finish;

            # Mauvais client
            my $oldCustomerId = $cgi->param('mvclient');
            # Bon client
            my $newCustomerId = $cgi->param('bnclient');
            # Filtres
            my $tabFilters = {
                'customerId' => $oldCustomerId
            };

            # R�cup�rer la liste des contrats du mauvais client
            my $tabContracts = &LOC::Contract::Rent::getList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters);
            # R�cup�rer la liste des lignes de devis du mauvais client
            my $tabEstimateLines = &LOC::Estimate::Rent::getLinesList($countryId, LOC::Util::GETLIST_ASSOC, $tabFilters);

            $req[0]=$reqtrait[0];# Modification sur le contrat
            $req[1]=$reqtrait[1];# Modification sur le devis
            $req[2]=$reqtrait[2];
            $req[3] = 'UPDATE f_proforma SET pfm_cus_id =' . $val[0] .' WHERE pfm_cus_id =' . $val[1];
            $req[4]=$reqtrait[3];
            $req[5]=$reqtrait[4];
            $req[6]=$reqtrait[5];
            $req[7]=$reqtrait[6];
            for (my $i=0;$i<@req;$i++)
            {
                $req[$i]=~ s/val0/$val[0]/g;
                $req[$i]=~ s/val1/$val[1]/;
                if ($i != 5)
                {
                    $DB->do($req[$i]);
                }
            }
            #alerte nv client
            my $all="ALL".$countryId;
            if ($bl0 !=0) {&alerte_pl_ms($all, $DB, LOC::Alert::CUSTOMERSTOCHECK, -1)};

            # Expire toutes les pro formas du contrat
            foreach my $tabInfo (values %tabDocumentsToExpire)
            {
                &LOC::Proforma::expireAll($G_PAYS, $tabInfo->{'type'}, $tabInfo->{'id'}, $G_PEAUTO, {}, undef);
            }

            # Communication au Transport du changement de client sur tous les contrats concern�s
            if (keys %$tabContracts > 0)
            {
                foreach my $tabContractInfos (values %$tabContracts)
                {
                    # Changements pouvant impacter le transport ? => on le lui communique
                    &LOC::Transport::onExternalChanges($countryId, {
                        'contract' => {
                            'id'    => $tabContractInfos->{'id'},
                            'event' => 'update',
                            'props' => {
                                'customer.id' => {'old' => $oldCustomerId, 'new' => $newCustomerId}
                            }
                        }
                    }, $G_PEAUTO, undef, undef);
                }
            }

            # Communication au Transport du changement de client sur tous les lignes de devis concern�es
            if (keys %$tabEstimateLines > 0)
            {
                foreach my $tabEstimateLinesInfos (values %$tabEstimateLines)
                {
                    # Changements pouvant impacter le transport ? => on le lui communique
                    &LOC::Transport::onExternalChanges($countryId, {
                        'estimateLine' => {
                            'id'    => $tabEstimateLinesInfos->{'id'},
                            'event' => 'update',
                            'props' => {
                                'customer.id' => {'old' => $oldCustomerId, 'new' => $newCustomerId}
                            }
                        }
                    }, $G_PEAUTO, undef, undef);
                }
            }

            # Lancement des mises � jour de fin
            &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);

            print qq{<b><font class="GD" color="red"><b>$mess[1751]</b></font></b><br>\n};
        }
        else
        {
            print qq{<font class="GD" color="red"><b>Impossible: M�me Client !!</b></font>\n};
        }
    }
}

sub chg_machine
{
    &saisie;
    print qq{</table>\n};
    print $cgi->start_form(-method=>'get', action=>'saisie.cgi');
    my $num       = $cgi->param('contrat');
    my $machineId = $cgi->param('machine');
    my $countryId = $G_PAYS;

    for ($l = 0 ; $l < $nbchamps ; $l++)
    {
        $cgi->param("$nameform[$l]");
        $cgi->param("$nameform[$l]rech");
        print $cgi->hidden("$nameform[$l]");
        print $cgi->hidden("$nameform[$l]rech");
    }

    # R�cup�ration du typage du modele de machines sur le contrat
    my $queryContractModelType = '
SELECT
    MM.MOMATYPAGE
FROM CONTRAT C
LEFT JOIN AUTH.MODELEMACHINE MM
ON C.MOMAAUTO = MM.MOMAAUTO
WHERE C.CONTRATAUTO = ' . $num .';';
    my $sth = $DB->prepare($queryContractModelType);
    $sth->execute();
    my ($contractModelType) = $sth->fetchrow_array;
    $sth->finish;

    my $tabContractModelType = ($contractModelType ne '' ? &LOC::Json::fromJson($contractModelType) : undef);

    # L'affichage du bouton "imprimer" (validation du chg/machine) est autoris� :
    my $isTypeCompatible = 0;
    # - si le mod�le de machines du contrat n'a pas de typage
    if (!defined $tabContractModelType || @$tabContractModelType == 0)
    {
        $isTypeCompatible = 1;
    }
    # - ou si l'un des typages de la machine saisie
    #   figure dans les typages du mod�le de machines du contrat
    else
    {
        # R�cup�ration du typage de la machine saisie
        my $queryMachineType = '
SELECT
    MM.MOMATYPAGE
FROM AUTH.MACHINE M
INNER JOIN AUTH.MODELEMACHINE MM
ON M.MOMAAUTO = MM.MOMAAUTO
WHERE M.MAAUTO = ' . $machineId . ';';
        my $sth = $DB->prepare($queryMachineType);
        $sth->execute();
        my ($machineType) = $sth->fetchrow_array;
        $sth->finish;

        my $tabMachineType = ($machineType ne '' ? &LOC::Json::fromJson($machineType) : undef);

        # Recherche des typages de la machine dans les typages du mod�le de machines du contrat
        foreach my $machineType (@$tabMachineType)
        {
            if (&LOC::Util::in_array($machineType, $tabContractModelType))
            {
                $isTypeCompatible = 1;
                last;
            }
        }
    }

#    $contractModelTypePrefix ne '' && $machineTypePrefix ne $contractModelTypePrefix
#    => $isTypeCompatible = 0

#    $contractModelTypePrefix eq '' || $machineTypePrefix eq $contractModelTypePrefix
#    => $isTypeCompatible = 1

    # On supprime l'affichage du bouton "imprimer" (validation du chg/machine) si la tourn�e aller est point�e"
    # - Filtre de recherche du transport :
    # -- Transport actif
    # -- qui va vers le contrat recherch�
    # -- r�alis�
    my $tabFilters = {
        'stateId'         => LOC::Transport::STATE_ACTIVE,
        'isUnloadingDone' => 1,
        'to'              => {
            'type' => LOC::Transport::FROMTOTYPE_CONTRACT,
            'id'   => $num
        }
    };
    my $nbDeliveryDone = &LOC::Transport::getList($countryId, LOC::Util::GETLIST_COUNT, $tabFilters);


    if (!$isTypeCompatible)
    {
        print qq{<center><font class="GD" color="red"><b>$mess[3160]</b></font></center>\n};
    }
    elsif ($nbDeliveryDone == 1)
    {
        print qq{<center><font class="GD" color="red"><b>$mess[3159]</b></font></center>\n};
    }
    else
    {
        if ($cgi->param('imprimer.x') == 0)
        {
            my $imp = $G_LST_IMP;
            # affichage des bonnes imprimantes
            print qq{<div id="printList" style="text-align: center;">$imp\n};
            print $cgi->image_button(-name    => 'imprimer',
                                     -id      => 'printBtnChangeMachine',
                                     -alt     => 'imprimer le BR/BL de Changement de machine',
                                     -border  => 0,
                                     -src     => "$URL_IMG/imprimer.gif",
                                     -onClick => "document.getElementById('printList').style.display = 'none'");
            print qq{</div>\n};
            print qq{<input type="hidden" name="type" value="$nomfh">\n};
            print $cgi->hidden('impfich', 'chmach');
            print $cgi->hidden('nbimpr', 1);
            print $cgi->hidden('contrat0', $num);
        }
        else
        {
            print qq{<center><font class="GD" color="red"><b>$mess[3155]</b></font></center>\n};
        }
        print $cgi->hidden('nbsaisie');
        print $cgi->hidden('saisie.x', 1);
        print "</form>\n";

    }
}

sub form_chgt {
    &saisie;

#V�rification de la validit� du client selectionn�
    my @val;
    $clautoBonClient=$cgi->param('bnclient');
    my $verifcl=qq{select C.CLCOLLECTIF, C.CLVERIFIER, C.CLSAGE, C.CLCODE, CLSTE FROM CLIENT C WHERE C.CLAUTO = $clautoBonClient;};
    my $verifcl=$DB->prepare($verifcl);
    $verifcl->execute();
    my @verifclient = $verifcl->fetchrow_array();
    $verifcl->finish;
    print "</table>\n";
#    print "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
    print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"0\">\n";
    print $cgi->start_form(-method=>'get', action=>'saisie.cgi');
    print $cgi->hidden('menu', $fich);
    print $cgi->hidden('type', $nomfh);
    for ($l=0;$l<$nbchamps;$l++) {
        $cgi->param("$nameform[$l]");
        $cgi->param("$nameform[$l]rech");
        print $cgi->hidden("$nameform[$l]");
        print $cgi->hidden("$nameform[$l]rech");
    }
    print qq{<center>\n};
    if ($cgi->param('clmod.x')==0)
    {
        #if (($verifclient[0] =~ /411[2478][0-9]{3}/ || $verifclient[0] =~ /4116[0-9][0-9][0-9]/ || $verifclient[2] eq -2) && ($G_PAYS eq "FR")) {
        if ((($verifclient[2] eq -2) or ($verifclient[3] eq "")) && (($G_PAYS eq "MA") || ($G_PAYS eq "FR") or ($G_PAYS eq "ES")))
        {
            print qq{<b><font class="GD" color="red"><b>$verifclient[4] => $mess[504]</b></font></b><br>\n};
        }
        elsif (!$cgi->param('assuranceIncompatible'))
        {
            my $contractId = $cgi->param('mvclient');

            my $tabFilters = {
                'contractId' => $contractId,
                'isExpired'  => 0
            };
            $countProformas = &LOC::Proforma::getList($G_PAYS, LOC::Util::GETLIST_COUNT, $tabFilters);
            if ($countProformas > 0)
            {
                print '<b><font class="GD" color="red"><b>' . $mess[8102] . '.</b></font></b><br /><br />';
            }

            print qq{<b><font class="GD" color="red"><b>$mess[1900]</b></font></b><br /><br />\n};
            print $cgi->image_button(-name=>'clmod', -src=>"$URL_IMG/valid_off.gif", -align=>'center', -border=>0, -alt=>$mess[58]);
            print $cgi->hidden('nbsaisie');
            print $cgi->hidden('saisie.x', 1);
        }
    }
    else
    {
        if (&chg_client)
        {
            print qq{<b><font class="GD" color="red"><b>$mess[1901]</b></font></b><br>\n};
        }
        else
        {
            print qq{<b><font class="GD" color="red"><b>$mess[3107]</b></font></b><br>\n};
        }
    }
    print qq{</center>\n};
    print "</form>\n";
}

sub chg_client
{
    my $etatcontrat; #lietcontratauto, etat du contrat
    my $etatfacture; #etatfacture du contrat

    my $countryId = $G_PAYS;
    my $userId = $G_PEAUTO;
    # Nouveau client
    my $customerId = $cgi->param('bnclient');
    # Ancien client
    my $contractId = $cgi->param('mvclient');


    # R�cup�ration des informations du contrat
    my $reqprosp = qq{
        SELECT CONTRATCODE, DATE_FORMAT(CONTRATDD,"%d/%m/%Y"), LIETCONTRATAUTO, CONTRATTYPEASSURANCE, CONTRATPASSURANCE,
            CONTRATFULLSERVICE, ity_flags
        FROM CONTRAT
        LEFT JOIN p_insurancetype ON ity_id = CONTRATTYPEASSURANCE
        WHERE CONTRATAUTO = "$contractId"
    };
    my $sthprosp = $DB->prepare($reqprosp);
    $sthprosp->execute();
    my ($contrat, $dtdb, $etatcontrat, $assuranceContrat, $passuranceContrat, $contratFS, $contractInsuranceFlags) = $sthprosp->fetchrow_array();
    $sthprosp->finish;

    # R�cup�ration des informations du nouveau client
    my $reqcl = qq{
        SELECT CLCODE, CLTYPEASSURANCE, CLPASSURANCE, ity_flags
        FROM CLIENT
        LEFT JOIN p_insurancetype ON ity_id = CLTYPEASSURANCE
        WHERE CLAUTO = "$customerId"
    };
    my $sthcl = $DB->prepare($reqcl);
    $sthcl->execute();
    my ($client, $assuranceClient, $passuranceClient, $customerInsuranceFlags) = $sthcl->fetchrow_array();
    $sthcl->finish;

    my $tauxDefaut = (split(/:/, $G_TX_ASS))[0];
    # Assurance n�goci�e sur le contrat
    my $isNegotiatedInsuranceContract     = ($contractInsuranceFlags & LOC::Insurance::Type::FLAG_ISINVOICED) &&
                                            $passuranceContrat != $tauxDefaut;
    # Assurance n�goci�e sur le client
    my $isNegotiatedInsuranceCustomer     = ($customerInsuranceFlags & LOC::Insurance::Type::FLAG_ISINVOICED) &&
                                            $passuranceClient != $tauxDefaut;
    # Diff�rence de taux entre le client et le contrat
    my $isDifferentInsuranceRate        =   ($customerInsuranceFlags & LOC::Insurance::Type::FLAG_ISINVOICED) &&
                                            $passuranceClient != $passuranceContrat;

    # V�rification de l'incompatibilit� des assurances du contrat et du nouveau client :
    # - assurance n�goci�e sur le contrat et le nouveau client mais � des taux diff�rents
    # - assurance n�goci�e sur le contrat mais pas sur le nouveau client
    # - assurance n�goci�e sur le nouveau client mais pas sur le contrat
    if (!$contratFS &&
        ($isNegotiatedInsuranceContract && ($isDifferentInsuranceRate||
                                            !$isNegotiatedInsuranceCustomer) ||
             $isNegotiatedInsuranceCustomer && !$isNegotiatedInsuranceContract)
       )
    {
        return 0;
    }


    # Requ�tes de mise � jour
    my $reqetatfacture = qq{
        SELECT ETATFACTURE
        FROM CONTRAT
        WHERE CONTRATAUTO = "$contractId"
    };
    my $sthetatfacture = $DB->prepare($reqetatfacture);
    $sthetatfacture->execute();
    my ($etatfacture) = $sthetatfacture->fetchrow_array();
    $sthetatfacture->finish;

    my @req;
    $req[0] = $reqtrait[0];
    $req[1] = $reqtrait[1];
    if ($sthetatfacture ne 'F')
    {
        $req[2] = $reqtrait[2];
        $req[3] = $reqtrait[3];
        $req[4] = $reqtrait[4];
    }
    for (my $i=0; $i<@req; $i++)
    {
        $req[$i] =~ s/val0/$customerId/;
        $req[$i] =~ s/val1/$contractId/;
        $DB->do($req[$i]);
    }

    # Le nouveau client est un prospect
    my $reqprosp = qq{
        SELECT CLAUTO, CLIENT.AGAUTO, CLSTE, CLADRESSE, VICLCP, VICLNOM,
            CLSIREN, CLTEL, CLFAX, MOPALIBELLE, CLRIB, CLCODE, CLVERIFIER,
            CLAPE
        FROM CLIENT
        LEFT JOIN AUTH.`VICL` ON VICL.VICLAUTO = CLIENT.VICLAUTO
        LEFT JOIN MODEPAIE ON MODEPAIE.MOPAAUTO = CLIENT.MOPAAUTO
        WHERE CLAUTO = "$customerId" AND (CLNONVALIDE = "1" OR CLVERIFIER = "1" OR CLCODE = "")
    };
    my $sthprosp = $DB->prepare($reqprosp);
    $sthprosp->execute();

    while (my @data = $sthprosp->fetchrow_array())
    {
        # $data[11] = CLCODE
        # $data[12] = CLVERIFIER
        if (($data[12] eq "1") || ($data[11] eq ""))
        {
            # Alerte des clients � v�rifier
            &alerte_pl_ms($agence, $DB, LOC::Alert::CUSTOMERSTOCHECK, -1);
            $data[12] = -1;
        }
        my $r = qq{
            UPDATE CLIENT SET CLNONVALIDE = "0", CLVERIFIER = "$data[12]"
            WHERE CLAUTO = "$customerId"
        };
        $DB->do($r);

        # Corps de l'e-mail
        my $message = qq{ $mess[350] $data[2] ($mess[351] $data[11]) $mess[352] $data[1]\n};
        $message .= qq{$mess[361] : $data[13]\n};
        $message .= qq{$mess[353] : $data[6]\n};
        $message .= qq{$mess[354] : $data[10]\n};
        $message .= qq{$data[3], $data[4], $data[5]\n};
        $message .= qq{$mess[355] : $data[7] $mess[356] : $data[8]\n};
        $message .= qq{$mess[357]\n};
        $message .= qq{$mess[358] : $contrat $mess[359] $dtdb\n};
        $message .= qq{@+\n};

        # Sujet de l'e-mail
        my $subj = qq{$mess[350] $data[2] $mess[360]\n};
        #FA 2474 -> enelevement && ($contratverifier == -1)
        if ($G_ENVIR eq '')
        {
            for ($i=0; $i < @emailnvcl; $i++)
            {
                &email($G_PEMAIL, $emailnvcl[$i], $subj, $message);
            }
        }
        else
        {
            $subj  .= "TEST";
            $message = "Destinataires d'origine : " . join(', ', @emailnvcl). "\n\n" . $message;
            &email("dev@acces-industrie.com", "archive.dev@acces-industrie.com", $subj, $message);
        }

    }

    # D�blocage du contrat si le nouveau client est valide
    my $req = qq{SELECT CLVERIFIER FROM CLIENT WHERE CLAUTO = "$customerId"};
    my $sth = $DB->prepare($req);
    $sth->execute();

    while (my @data = $sth->fetchrow_array())
    {
        my $contratverifier = ($data[0] == 0) ? 0 : -1;
        my $req = qq{UPDATE CONTRAT SET CONTRATVERIFIER = "$contratverifier" WHERE CLAUTO = "$customerId"};
        $DB->do($req);
    }

    $sthprosp->finish;

    # le nouveau client est en "P" et au est au moins en contrat
    if($etatcontrat ne "1")
    {
        my $sthY = $DB->prepare("select CLCLASSE, CLPOTENTIEL FROM CLIENT WHERE CLAUTO=".$customerId);
        $sthY->execute();
        my @dataY=$sthY->fetchrow_array();
        if ($dataY[0] eq "P" && $dataY[1] eq "")
        {
             my $sthX = $DB->do("UPDATE CLIENT SET CLCLASSE='C', CLPOTENTIEL='C' WHERE CLAUTO=".$customerId."  limit 1");
        }
        elsif ($dataY[0] eq "P")
        {
             my $sthX = $DB->do("UPDATE CLIENT SET CLCLASSE='C' WHERE CLAUTO=".$customerId."  limit 1");
        }
    }

    # FA 7510 Mise � jour dans le cas du blocage temporaire.
    my $clauto = $cgi->param("clients");
    $requete = "SELECT CLSAGE, CLTMPUNLOCK, CLCOLLECTIF, CLCODE FROM CLIENT WHERE CLAUTO = $customerId;";
    $sth = $DB->prepare( $requete );
    $sth->execute;
    my @data = $sth->fetchrow_array;
    if( ( $data[0] eq 0 ) && ( $data[1] eq 1 ) && ( $data[2] !~ m/4114[0-9]{3}/ ) )
    {
        $requete = "UPDATE CLIENT SET CLSAGE = -2, CLTMPUNLOCK = 0 WHERE CLAUTO = $customerId;";
        $sth = $DB->prepare( $requete );
        $sth->execute;
        # Trace blocage/d�blocage clients (DE7783)
        &traceBlocageClients($data[3], $customerId, $data[0], -2, $data[1], 0, $data[2], $data[2]);
    }

    my $tabEndUpds = {};

    # Expire toutes les pro formas du contrat
    &LOC::Proforma::expireAll($G_PAYS, 'contract', $contractId, $G_PEAUTO, {}, undef);

    # Renouveler les transports li�s � ce contrat
    &LOC::Transport::renew($countryId, {'from-to' => {'type' => 'contract', 'id' => $contractId}});

    # Lancement des mises � jour de fin
    &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);

    return 1;
}

sub saisie {
    my $cptetoile=0;
    my $tarifnb=1;
    my $tableau;
    my @value;


    $tableau=qq{<table border="0" cellspacing="0" cellpadding="0">\n};
    $tableau .= $cgi->start_multipart_form(
        -method => 'get',
        -action => 'saisie.cgi',
        -name   => 'frm'
    );
    $tableau .= qq{<input type="hidden" name="numnvclt" value="0">\n};
    print $tableau;

    #
    # different type de champ de saisie
    #

    my @cltmpunlock;
    my $clbloc = 0;
    my $tabInfoClient0, $tabInfoClient1, $tabInfoContrat;

    for (my $i = 0; $i < $nbchamps; $i++)
    {
        my $tpform="";

        print qq{
            <tr>
            <td nowrap valign="middle"><font class="PT"><b>$libform[$i] :&nbsp;</b></font></td>
        };
        print qq{
            <td nowrap valign="middle"><font class="PT">
        };


        # liste popup dans une base
        if ($typeform[$i] eq "listesql") {
            if ((defined ($cgi->param("$nameform[$i]rech")))&&(not($cgi->param("$nameform[$i]rech") eq "")))
            {
                $ident=$cgi->param("$nameform[$i]rech")
            }
            else
            {
                $ident="\\"
            }
            $reqform[$i] =~ s/ident/$ident/eg;
            $tpform .= qq{<select name=$nameform[$i] tabindex=-1 onChange="submit();">\n};
            if ($ident ne "\\")
            {
                my $sel = $cgi->param("$nameform[$i]");

                $sth = $DB->prepare("$reqform[$i]");
                $sth->execute();
                my @aff;
                my @valsel;
                my @opsel;
                $cp=0;
                while (my $tabRow = $sth->fetchrow_hashref())
                {
                    if ($nomfh eq "chmach" && $nameform[$i] eq "contrat")
                    {
                        $reqform[1] =~ s/val1/$tabRow->{'CONTRATHORSPARC'}/g;
                    }
                    $aff[$cp] = "";
                    $valsel[$cp] = $tabRow->{$entityId[$i]};

                    # Si cet �l�ment est s�lectionn�, alors on r�cup�re certaines informations pour comparer les 2 clients
                    if ($nomfh eq 'chcl' && $sel eq $valsel[$cp])
                    {
                        if ($i == 0)
                        {
                            $tabInfoClient0 = {
                                'Client'              => $tabRow->{'customer.label'},
                                'Assurance'           => $tabRow->{'customer.insuranceType.id'} * 1,
                                'PAssurance'          => $tabRow->{'customer.insuranceRate'} * 1,
                                'isInsuranceInvoiced' => $tabRow->{'customer.insuranceType.isInvoiced'} * 1,
                                'Nettoyage'           => $tabRow->{'customer.cleaning'} * 1,
                                'Recyclage'           => $tabRow->{'customer.residues'} * 1,
                                'appealTypeId'        => $tabRow->{'customer.appealType.id'} * 1,
                                'appealTypeLabel'     => $tabRow->{'customer.appealType.label'}
                            };
                            $tabInfoContrat = {
                                'Contrat'       => $tabRow->{'contract.code'},
                                'Assurance'     => $tabRow->{'contract.insuranceType.id'} * 1,
                                'PAssurance'    => $tabRow->{'contract.insuranceRate'},
                                'isFullService' => $tabRow->{'contract.isFullService'}
                            };
                        }
                        elsif ($i == 1)
                        {
                            $tabInfoClient1 = {
                                'Client'              => $tabRow->{'customer.label'},
                                'Assurance'           => $tabRow->{'customer.insuranceType.id'} * 1,
                                'isInsuranceInvoiced' => $tabRow->{'customer.insuranceType.isInvoiced'},
                                'PAssurance'          => $tabRow->{'customer.insuranceRate'} * 1,
                                'Nettoyage'           => $tabRow->{'customer.cleaning'} * 1,
                                'Recyclage'           => $tabRow->{'customer.residues'} * 1,
                                'appealTypeId'        => $tabRow->{'customer.appealType.id'} * 1,
                                'appealTypeLabel'     => $tabRow->{'customer.appealType.label'}
                            };
                        }
                    }

                    if ($nameform[$i] eq "bnclient")
                    {
                        my $collectif = $tabRow->{'customer.accountingCode'};
                        my $blocage  = $tabRow->{'customer.isLocked'};
                        $opsel[$cp] = couleurClient($collectif, $blocage);
                    }
                    else
                    {
                        $opsel[$cp] = 0;
                    }

                    # - Affichage du contenu de l'option
                    my @tabFields;
                    for (my $k = 0; $k < @{$fieldsToDisplay[$i]}; $k++)
                    {
                        push(@tabFields, $tabRow->{$fieldsToDisplay[$i]->[$k]});
                        
                    }
                    $aff[$cp] = join(', ', @tabFields);

                    $cp++;
                }
                $sth->finish;

                if ($cp == 0)
                {
                    $tpform .= qq{<option value="">$mess[2]\n};
                }
                else
                {
                    for ($eps = 0; $eps < $cp; $eps++)
                    {
                        my $selected = "";
                        if ($valsel[$eps] eq $sel)
                        {
                            $selected=" selected";
                            if ($opsel[$eps])
                            {
                                $clbloc = -1;
                            }
                        }
                        my $style = "";
                        if ($opsel[$eps])
                        {
                            $style = qq{ STYLE="color: $mess[6000+$opsel[$eps]-1]"};
                        }
                        $tpform .= qq{<option value="$valsel[$eps]"$selected$style>$aff[$eps]</option>\n};
                    }
                }
            }
            else {
                $tpform .=qq{<option value="">$mess[2]\n};
                $ident="";
            }
            $tpform .=qq{</select>\n\n};
            $tpform .=qq{</td><td nowrap valign=top>};

            $tpform .=$cgi->textfield(-name=>"$nameform[$i]rech", -default=>$ident, -size=>10, -onChange=>"taille(this.value, 3)");
            $tpform .=qq{</td><td nowrap>};
            my $sele=$cgi->param("$nameform[$i]");

            if ((($sele eq "")||($cp==0))&&($oblform[$i] eq "oui")) {
                $tpform .=qq{<img src="$URL_IMG/fire.gif" width="19" height="18" alt="$mess[5]">\n};
                $cptetoile++;
            }
        }

        # bouton submit et reset
        if ($typeform[$i] eq "submit") {
            $tpform .=
                $cgi->image_button(
                    -name   => $nameform[$i],
                    -src    => "$URL_IMG/recalk_off.gif",
                    -border => 0,
                    -alt    => $value[$i]
                ) .
                qq{<input type="hidden" name="clbloc" value="$clbloc">};
        }

        $tpform .=qq{</font></td>\n</tr>};
        print $tpform;
    }

    my $assuranceIncompatible = 0;
    # Gestion des diff�rences entre l'ancien et le nouveau client
    if ($nomfh eq 'chcl' && defined($tabInfoClient0) && defined($tabInfoClient1) && defined($tabInfoContrat))
    {
        my $assuranceError = '';
        my $recyclageError = '';
        my $nettoyageError = '';
        my $appealError    = '';

        my $tauxDefaut = (split(/:/, $G_TX_ASS))[0];

        # Assurance n�goci�e sur le contrat
        my $isNegotiatedInsuranceContract      = $tabInfoContrat->{'Assurance'} == LOC::Insurance::TYPE_INVOICED &&
                                                 $tabInfoContrat->{'PAssurance'} != $tauxDefaut;
        # Assurance n�goci�e sur le client
        my $isNegotiatedInsuranceCustomer     = $tabInfoClient1->{'Assurance'} == LOC::Insurance::TYPE_INVOICED &&
                                                $tabInfoClient1->{'PAssurance'} != $tauxDefaut;
        # Diff�rence de taux entre le client et le contrat
        my $isDifferentInsuranceRate          = $tabInfoClient1->{'Assurance'} == LOC::Insurance::TYPE_INVOICED &&
                                                $tabInfoClient1->{'PAssurance'} != $tabInfoContrat->{'PAssurance'};

        # D�termination de l'incompatibilit� des assurances du contrat et du nouveau client :
        # - assurance n�goci�e sur le contrat et le nouveau client mais � des taux diff�rents
        # - assurance n�goci�e sur le contrat mais pas sur le nouveau client
        # - assurance n�goci�e sur le nouveau client mais pas sur le contrat
        if (!$tabInfoContrat->{'isFullService'} &&
            ($isNegotiatedInsuranceContract && ($isDifferentInsuranceRate||
                                                !$isNegotiatedInsuranceCustomer) ||
                 $isNegotiatedInsuranceCustomer && !$isNegotiatedInsuranceContract)
           )
        {
            $assuranceIncompatible = 1;

            print '<tr><td colspan="4">';
            print '<p style="margin-top: 20px; text-align: center;"><b style="color: red;">
                ' . $mess[3106] . '
                <ul style="text-align: center; list-style-type: none;">
                    <li>' . $mess[1458] . ' ' . $tabInfoContrat->{'Contrat'} . ' : ' . &aff_assurance('', $tabInfoContrat->{'Assurance'}, 2, $tabInfoContrat->{'PAssurance'}) . '</li>
                    <li>' . $mess[2052] . ' ' . $tabInfoClient1->{'Client'} . ' : ' . &aff_assurance('', $tabInfoClient1->{'Assurance'}, 2, $tabInfoClient1->{'PAssurance'}) . '</li>
                </ul>
            </b></p>';
            print '</td></tr>';
        }
        else
        {
            # Diff�rence d'assurance
            if (($tabInfoClient0->{'Assurance'} != $tabInfoClient1->{'Assurance'}) ||
                ($tabInfoClient0->{'isInsuranceInvoiced'} && $tabInfoClient1->{'isInsuranceInvoiced'} &&
                  $tabInfoClient0->{'PAssurance'} != $tabInfoClient1->{'PAssurance'}))
            {
                my $assurance0 = &aff_assurance('', $tabInfoClient0->{'Assurance'}, 2, $tabInfoClient0->{'PAssurance'});
                my $assurance1 = &aff_assurance('', $tabInfoClient1->{'Assurance'}, 2, $tabInfoClient1->{'PAssurance'});

                $assuranceError = '<tr><td style="background-color: #C6CDC5;">' . $mess[0] . '</td><td>' . $assurance0 . '</td><td>' . $assurance1 . '</td></tr>';
            }

            # Diff�rence de gestion de recours
            if ($tabInfoClient0->{'appealTypeId'} != $tabInfoClient1->{'appealTypeId'})
            {
                my $appealLabelOrigin = $tabInfoClient0->{'appealTypeId'} ? $tabInfoClient0->{'appealTypeLabel'} : $mess[3059];
                my $appealLabelDest   = $tabInfoClient1->{'appealTypeId'} ? $tabInfoClient1->{'appealTypeLabel'} : $mess[3059];

                $appealError = '
                    <tr>
                        <td style="background-color: #C6CDC5;">' . $mess[3056] . '</td>
                        <td>' . $appealLabelOrigin . '</td>
                        <td>' . $appealLabelDest . '</td>
                    </tr>';
            }

            # Diff�rence de participation au recyclage
            if ($tabInfoClient0->{'Recyclage'} != $tabInfoClient1->{'Recyclage'})
            {
                $recyclageError = '<tr><td style="background-color: #C6CDC5;">' . $mess[1616] . '</td><td>' . ($tabInfoClient0->{'Recyclage'} == -1 ? $mess[6300] : $mess[6301]) . '</td><td>' . ($tabInfoClient1->{'Recyclage'} == -1 ? $mess[6300] : $mess[6301]) . '</td></tr>';
            }

            # Diff�rence de facturation du nettoyage
            if ($tabInfoClient0->{'Nettoyage'} != $tabInfoClient1->{'Nettoyage'})
            {
                $nettoyageError = '<tr><td style="background-color: #C6CDC5;">' . $mess[1617] . '</td><td>' . ($tabInfoClient0->{'Nettoyage'} == -1 ? $mess[6659] : $mess[6666]) . '</td><td>' . ($tabInfoClient1->{'Nettoyage'} == -1 ? $mess[6659] : $mess[6666]) . '</td></tr>';
            }

            if ($assuranceError ne '' || $appealError ne '' || $recyclageError ne '' || $nettoyageError ne '')
            {
                print '<tr><td colspan="4">';

                print '<p style="margin-top: 20px; text-align: center;"><b style="color: red;">' . $mess[3104] . '</b></p>';

                print '<table style="margin-bottom: 30px;" align="center" cellpadding="2" cellspacing="2" border="0" width="100%">';
                print '<tr><td></td><td style="background-color: #C6CDC5; font-weight: bold;"> '.  $tabInfoClient0->{'Client'} . ' </td><td style="background-color: #C6CDC5;font-weight:bold">' .  $tabInfoClient1->{'Client'} . '</td></tr>';
                print $assuranceError . $appealError . $recyclageError . $nettoyageError;

                print '</table>';
                print '</td></tr>';
            }
        }
    }

    if ($nomfh eq 'chcl' && !$assuranceIncompatible)
    {
        print '<tr><td colspan="4">';
        my $client = (defined $tabInfoClient0->{'Client'} ? ' (' . $tabInfoClient0->{'Client'} . ')' : '' );
        print '<p style="margin-top: 20px; text-align: center; font-size: 12pt"><b style="color: red;">' . $mess[3105] . $client . '.</b></p>';
        print '</td></tr>';
    }

    $cgi->param(-name => 'assuranceIncompatible', -value => $assuranceIncompatible);
    print $cgi->hidden(-name => 'assuranceIncompatible', -value => $assuranceIncompatible, -override => 1);

    $tableau ="";
    $tableau .=$cgi->hidden('type', $nomfh);
    for ($j=0;$j<$nbchstock;$j++)
        {
        $tableau .=$cgi->hidden("$libstock[$j]");
        $tableau .=qq{\n};
        }

    $tarifnb=1;
    if (($cptetoile != 0)||($tarifnb !=1)||($nomfh eq "saisie"))
        {
        $tableau .=qq{<p align="center">\n};
        }
    if ($nomfh eq "saisie")
        {
        $tableau .=qq{<BR><font color="#00CC33" class="PT"><b>$mess[2781]</b></font><BR>\n};
        }
    if (($cptetoile != 0)||($tarifnb !=1))
        {
        $tableau .=qq{<font color="red" class="PT">\n};
        $tableau .=qq{<BR>};
        if ($cptetoile != 0)
            {
            $tableau .=qq{<img src="$URL_IMG/fire.gif" width="19" height="18" alt="$mess[6]"><b> $mess[7]</b>\n};
            }
        if (($tarifnb !=1)&&(not($nomfh eq "impfac"))&&(not($nomfh eq "saisiemach")))
             {
            $tableau .=qq{<b>$mess[8]</b>\n};
            }
         $tableau .=qq{</font>\n};
        }
    else
        {
        $tableau .=$cgi->hidden('nbsaisie', 'oui');
        }
    if (($cptetoile != 0)||($tarifnb !=1)||($nomfh eq "saisie"))
        {
         $tableau .=qq{</p>\n};
        }
    print $tableau, "\n</form>\n";

    print qq{<script language="javascript">\n};
    print qq{pays = '$G_PAYS';\n};
    print qq{</script>\n};

}
