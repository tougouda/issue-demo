#!/usr/bin/perl

# $Id: modele.cgi,v 1.1.1.1 2006/08/01 07:48:10 clliot Exp $
# $Log: modele.cgi,v $
# Revision 1.1.1.1  2006/08/01 07:48:10  clliot
# no message
#
# Revision 2.4  2006/02/06 16:52:45  julien
# V. 1.4 : Tarification v2
#
# Revision 2.3.2.1  2005/12/13 15:46:40  julien
# no message
#
# Revision 2.3.4.1  2005/12/13 10:31:52  julien
# Inclusion du r�pertoire lib dans chaque fichier.
#
# Revision 2.3  2005/08/26 12:10:55  julien
# Pr�fixage des tables de AUTH dans les requ�tes.
#
# Revision 2.2  2005/05/03 16:51:22  julien
# Tra�age du temps d'ex�cution des scripts.
#
# Revision 2.1  2005/05/03 14:03:54  julien
# Tra�age du temps d'ex�cution des scripts.
#
# Revision 2.0  2004/09/16 15:07:30  julien
# Version r�cup�r�e de Lancelot et nettoy�e
#
# Revision 1.2  2004/09/16 14:23:11  julien
# Ajout des identifiants
#

use lib '../lib';

use strict;

use CGI;
use autentif;
use calculdate;
#use securite;
use titanic;
#use accesbases;
use accesbd;
use varglob;
use Date::Calc qw(Day_of_Week Delta_Days Add_Delta_Days);
use message;
use tracetemps;

# Initialisation du chrono
my $timer = &init_chrono();


my $DB=&Environnement("LOCATION");
#&authentification(&login);
#my $pe_login = &login;

#initialisation CGI
my $cgi=new CGI;
$cgi->autoEscape(undef);

#connexion

$|=1;


my $fich=$cgi->param('menu');



my $titre=qq{Cr�ation Mod�le Machine};
if ($fich eq "famille")	 
	{
	$titre=qq{Cr�ation Famille Machine};
	}



&ente($titre, $titre, $G_PENOM, $G_PEPRENOM);
if ($fich eq "famille")	 
	{
	if ($cgi->param('creation.x') !=0)
		{
		&insert_famille
		} 
	&crea_famille;
	}
if ($fich eq "modele")	 
	{
	if ($cgi->param('creation.x') !=0)
		{
		&insert_modele;
		} 
	&crea_modele;
	}



# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

# Pied de la page HTML
my $paramMenu = $cgi->param("menu");
&Pied($delta_time, $paramMenu);


$DB->disconnect;

#
# creation de modele
#

sub crea_modele {

print qq{<table width="100%" border="0" cellspacing="0" cellpadding="0">\n};
print $cgi->start_form;
print $cgi->hidden('menu',$fich);
#
# Famille machine
#
print qq{<tr>\n};
print qq{<td align="right"><b>Famille Machine</b></td>\n};
print qq{<td>\n};
print qq{<select name="fam">\n};
my $req=qq{select FAMAAUTO,FAMAMODELE,FAMAELEVATION,FAMAENERGIE,CHARGE FROM FAMILLEMACHINE ORDER BY FAMAMODELE,FAMAELEVATION};
my $sth=$DB->prepare($req);
$sth->execute();
my $nbrow=$sth->rows;
print qq{<option value="0">$nbrow choix\n};
while (my @data=$sth->fetchrow_array()) 	
	{
	print qq{<option value="$data[0]">$data[1], $data[2], $data[3], $data[4]\n};
	}
$sth->finish;
print qq{</select>\n};
print qq{</td>\n</tr>\n};

#
# Fournisseur
#
print qq{<tr>\n};
print qq{<td align="right"><b>Fournisseur</b></td>\n};
print qq{<td>\n};
print qq{<select name="fou">\n};
my $req=qq{select FOAUTO,FOLIBELLE FROM FOURNISSEUR ORDER BY FOLIBELLE};
my $sth=$DB->prepare($req);
$sth->execute();
my $nbrow=$sth->rows;
print qq{<option  value="0">$nbrow choix\n};
while (my @data=$sth->fetchrow_array()) 	
	{
	print qq{<option value="$data[0]">$data[1]\n};
	}
$sth->finish;
print qq{</select>\n};
print qq{</td>\n</tr>\n};

#
# MOdele
#
print qq{<tr>\n};
print qq{<td align="right"><b>D�signation</b></td>\n};
print qq{<td>\n};
print $cgi->textfield(-name=>'des',-maxlength=>25,size=>25);
print qq{</td>\n};
print qq{</td>\n</tr>\n};

#
# Ht Travail
#
print qq{<tr>\n};
print qq{<td align="right"><b>Ht Travail</b></td>\n};
print qq{<td>\n};
print $cgi->textfield(-name=>'htt',-maxlength=>5,size=>5);
print qq{</td>\n};
print qq{</td>\n</tr>\n};

#
# Ht Plancher
#
print qq{<tr>\n};
print qq{<td align="right"><b>Ht Plancher</b></td>\n};
print qq{<td>\n};
print $cgi->textfield(-name=>'htp',-maxlength=>5,size=>5);
print qq{</td>\n};
print qq{</td>\n</tr>\n};

#
#  Deport
#
print qq{<tr>\n};
print qq{<td align="right"><b>D�port</b></td>\n};
print qq{<td>\n};
print $cgi->textfield(-name=>'dep',-maxlength=>5,size=>5);
print qq{</td>\n};
print qq{</td>\n</tr>\n};

#
#  Capacit�
#
print qq{<tr>\n};
print qq{<td align="right"><b>Capacit�</b></td>\n};
print qq{<td>\n};
print $cgi->textfield(-name=>'cap',-maxlength=>5,size=>5);
print qq{</td>\n};
print qq{</td>\n</tr>\n};

#
#  Lng
#
print qq{<tr>\n};
print qq{<td align="right"><b>Longueur</b></td>\n};
print qq{<td>\n};
print $cgi->textfield(-name=>'lng',-maxlength=>5,size=>5);
print qq{</td>\n};
print qq{</td>\n</tr>\n};
#
#  Largeur
#
print qq{<tr>\n};
print qq{<td align="right"><b>Largeur</b></td>\n};
print qq{<td>\n};
print $cgi->textfield(-name=>'lrg',-maxlength=>5,size=>5);
print qq{</td>\n};
print qq{</td>\n</tr>\n};
#
#  Poids
#
print qq{<tr>\n};
print qq{<td align="right"><b>Poids</b></td>\n};
print qq{<td>\n};
print $cgi->textfield(-name=>'pds',-maxlength=>5,size=>5);
print qq{</td>\n};
print qq{</td>\n</tr>\n};

#
# Famille tarifaire
#
print qq{<tr>\n};
print qq{<td align="right"><b>Famille Tarifaire</b></td>\n};
print qq{<td>\n};
print qq{<select name="famtar">\n};
my $req=qq{select FAMTARAUTO, FAMTARLIBELLE, FAMTARSOUSLIBELLE FROM LOCATION.FAMILLETARIFAIRE ORDER BY FAMTARLIBELLE,FAMTARSOUSLIBELLE};
my $sth=$DB->prepare($req);
$sth->execute();
my $nbrow=$sth->rows;
print qq{<option value="0">$nbrow choix\n};
while (my @data=$sth->fetchrow_array()) 	
	{
	print qq{<option value="$data[0]">$data[1] - $data[2]\n};
	}
$sth->finish;
print qq{</select>\n};
print qq{</td>\n</tr>\n};

print qq{<tr>\n};
print qq{<td colspan="2" align="center">\n}; 
print $cgi->image_button(-name=>'creation', -src=>"$URL_IMG/att_mach.gif",-alt=>'Creation Mod�le');
print qq{</td>\n</tr>\n};  
				
print qq{</form>\n};
print qq{</table>\n};

}

#
# creation Stock Modele 
#

sub crea_stock {
my @param=@_;

my $req=qq{select AGAUTO FROM AUTH.`AGENCE` ORDER BY AGAUTO};
my $sth=$DB->prepare($req);
$sth->execute();

while (my @data=$sth->fetchrow_array()) 	
	{
	$DB->do(qq{INSERT INTO AUTH.`STOCKMODELE` (AGAUTO,MOMAAUTO) values ("$data[0]","$param[0]")});
	}
$sth->finish;

}

#
# creation du modele
#

sub insert_modele {
	
my $fam=$cgi->param('fam');
my $fat=$cgi->param('famtar');
my $fou=$cgi->param('fou');
my $des=$cgi->param('des');	
my $htt=$cgi->param('htt');
my $htp=$cgi->param('htp');
my $dep=$cgi->param('dep');	
my $cap=$cgi->param('cap');
my $lng=$cgi->param('lng');
my $lrg=$cgi->param('lrg');	
my $pds=$cgi->param('pds');	

my $ins=qq{INSERT INTO AUTH.`MODELEMACHINE` (FAMAAUTO,FOAUTO,MOMADESIGNATION,MOMAHTTRAVAIL,MOMAHTPLANCHER,MOMADEPORT,MOMACAPACITE,MOMALNG,MOMALG,MOMAPOIDS,FAMTARAUTO) values ("$fam","$fou","$des","$htt","$htp","$dep","$cap","$lng","$lrg","$pds","$fat")};

$DB->do($ins);
my $mod=$DB->{mysql_insertid}; 
&crea_stock($mod);
print qq{<center><font class="GD" color="green"><b>Le Mod�le $des a �t� ajout�.</font><br><font class="GD" color="red">N'oubliez pas de v�rifier l'existence de la famille tarifaire dans tous les pays.</b></center>\n};
}

#
# liste pays
#
sub lst_pays {
my @pays=();
 my $req=qq{select PACODE FROM AUTH.`PAYS` ORDER BY PACODE};
my $sth=$DB->prepare($req);
$sth->execute();

while (my @data=$sth->fetchrow_array()) 	
	{
	push @pays , $data[0];
	}
$sth->finish;
return @pays;
}

#
# creation Famille
# 

sub crea_famille {
my @ps=&lst_pays;
my $nbp=@ps;
print qq{<table width="100%" border="0" cellspacing="0" cellpadding="0">\n};
print $cgi->start_form;
print $cgi->hidden('menu',$fich);
#
# Famille machine
#

print qq{<tr>\n};
print qq{<td align="left" colspan="3"><b>Famille</b></td>\n};
print qq{\n</tr>\n};
print qq{<tr>\n};

for(my $k=0;$k<$nbp;$k++)
	{
	my $div=$k % 3;
	if ($div==0) 
		{
		print qq{<tr>\n};
		}
	print qq{<td align="center"><b>$ps[$k]</b>\n};
	print $cgi->textfield(-name=>"FAM$ps[$k]",-maxlength=>30,size=>30);
	print qq{\n</td>\n};
	if ($div==2) 
		{
		print qq{</tr>\n};
		}
	}


my $nbt=2;

#
# Elevation
#

print qq{<tr>\n};
print qq{<td align="right"><b>Elevation</b></td>\n};
print qq{<td colspan="$nbt" align="left">\n};
print $cgi->textfield(-name=>'elv',-maxlength=>5,size=>5);
print qq{</td>\n</tr>\n};

#
# Energie
#

print qq{<tr>\n};
print qq{<td align="right"><b>Energie</b></td>\n};
print qq{<td colspan="$nbt" align="left">\n};
print $cgi->textfield(-name=>'nrj',-maxlength=>5,size=>5);
print qq{</td>\n</tr>\n};

#
# Famille Compta
#
print qq{<tr>\n};
print qq{<td align="right"><b>Famille Compta</b></td>\n};
print qq{<td colspan="$nbt" align="left">\n};
print qq{<select name="cpt">\n};
my $req=qq{select FAMACOMPTAAUTO,FAMACOMPTA FROM FAMILLECOMPTA ORDER BY FAMACOMPTA};
my $sth=$DB->prepare($req);
$sth->execute();
my $nbrow=$sth->rows;
print qq{<option value="0">$nbrow choix\n};
while (my @data=$sth->fetchrow_array()) 	
	{
	print qq{<option value="$data[0]">$data[1]\n};
	}
$sth->finish;
print qq{</select>\n};

print qq{</td>\n</tr>\n};

#
#  Charge
#
print qq{<tr>\n};
print qq{<td align="right"><b>Charge</b></td>\n};
print qq{<td colspan="$nbt" align="left">\n};
print $cgi->textfield(-name=>'chrg',-maxlength=>5,size=>5);
print qq{</td>\n};
print qq{</td>\n</tr>\n};



print qq{<tr>\n};
print qq{<td colspan="2" align="center">\n}; 
print $cgi->image_button(-name=>'creation', -src=>"$URL_IMG/att_mach.gif",-alt=>'Creation Famille');
print qq{</td>\n</tr>\n};  
				
print qq{</form>\n};
print qq{</table>\n};
}

sub insert_famille {
my @ps=&lst_pays;
my $elv=$cgi->param('elv');
my $nrj=$cgi->param('nrj');
my $chrg=$cgi->param('chrg');
if ($chrg eq "") {$chrg="NULL"} else {$chrg=qq{"$chrg"}};
my $cpt=$cgi->param('cpt');
my @DBC;
my $max=0;
for(my $i=0;$i<@ps;$i++)
	{
	if ($ps[$i] ne $G_PAYS) 
		{
		$DBC[$i]=&Environnement("LOCATION",$ps[$i]);
		}
	else
		{
		$DBC[$i]=$DB;
		}
	my $req=qq{select MAX(FAMAAUTO) FROM FAMILLEMACHINE};
	my $sth=$DBC[$i]->prepare($req);
	$sth->execute();
	my ($maxauto)=$sth->fetchrow_array();
	
	$sth->finish;
	if ($maxauto>$max) {$max=$maxauto};
	}
$max +=1;
for(my $i=0;$i<@ps;$i++)
	{
	my $fam=$cgi->param("FAM$ps[$i]");
	my $felv =$elv." m";
	my $req=qq{INSERT INTO FAMILLEMACHINE (FAMAAUTO,FAMAMODELE,FAMAELEVATION,FAMAENERGIE,FAMACOMPTAAUTO,FAMAHAUTEUR,CHARGE) values ("$max","$fam","$felv","$nrj","$cpt","$elv",$chrg)};
	
	$DBC[$i]->do($req);
	if ($ps[$i] ne $G_PAYS) 
		{
		$DBC[$i]->disconnect;	
		}
	}
print qq{<center><font class="GD" color="red"><b>La Famille est saise</b></center>\n};
}
