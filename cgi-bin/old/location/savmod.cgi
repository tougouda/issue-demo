#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';

use strict;

use CGI;
use Encode;
use autentif;
use calculdate;
#use securite;
use titanic;
#use accesbases;
use accesbd;
use varglob;
use Date::Calc qw(Day_of_Week Delta_Days Add_Delta_Days);
use message;
use kimoce;
use tracetemps;
use LOC::Globals;
use LOC::Machine;
use LOC::Country;
use LOC::Transport;
use LOC::EndUpdates;
use LOC::Request;

# Initialisation du chrono
my $timer = &init_chrono();

#connexion
my $DB=&Environnement("LOCATION");
#&authentification(&login);
#my $pe_login = &login;

#initialisation CGI
my $cgi=new CGI;
$cgi->autoEscape(undef);
my @mess=&message($G_PAYS);

$|=1;


#Variables
my $titre;
#my $hl_color = "00CCFF";

my $frsaisi;
my $tableau;
my $tpform;

my $nbchamps;
my @nbchform;
my @nameform;
my @typeform;
my @libform;
my @selform;
my @reqform;
my @reqtrait;
my @reqrech;
my @optrech;
my @optionsup;

my $optsp;

my $i;
my $j;
my $k;
my $l;

my $ident;
my $cp;
my $eps;
my $sth;
my @aff;
my @valsel;
my @data;
my @req;

my $agence=$G_AGENCE;

&init_var;

&ente($titre, $titre, $G_PENOM, $G_PEPRENOM);
print qq{<SCRIPT language="JavaScript" src="$HTTP_PORTAIL/web/js/common.js"></SCRIPT>};
print qq{<SCRIPT language="JavaScript" src="$HTTP_PORTAIL/web/js/httpRequest.js"></SCRIPT>};

print '
<script type="text/javascript">
    var printHttpObj;
    function printList ()
    {
        var state   = document.getElementsByName("etatimp")[0].value;
        var printer = document.getElementsByName("imprimante")[0].value.substr(0, 7);

        var file    = "' . &LOC::Request::createRequestUri('machine:list:pdf') . '&stateId=" + state;
        var url     = "' . &LOC::Request::createRequestUri('print:render:print') . '";

        // Affichage d\'un indicateur de chargement
        var loadingImg = document.getElementById("img-loading");
        loadingImg.style.visibility = "visible";
        // Message de retour
        var resultMsg = document.getElementById("msg-result");
        resultMsg.style.display = "none";

        if (!printHttpObj)
        {
            printHttpObj = new Location.HTTPRequest(url);
            // Codes de retour valide :
            // 204 : pas de contenu (code de retour normal)
            // 1223 : code transform� par IE
            // 0 : code transform� par Google Chrome Frame
            printHttpObj.setValidResponseStatusRegExp("^0|204|1223$");
        }

        printHttpObj.oncompletion = function()
        {
            loadingImg.style.visibility = "hidden";
            resultMsg.style.display = "block";
        };
        printHttpObj.onerror = function()
        {
            // Erreur
            alert("Erreur " + printHttpObj.getResponseStatus());
            loadingImg.style.visibility = "hidden";
        };

        // Passage de l\'id du client en param�tre
        printHttpObj.setVar(\'printer\', printer);
        printHttpObj.setVar(\'url\', file);
        printHttpObj.setVar(\'number\', 1);

        // Envoi de la requ�te HTTP
        printHttpObj.send(true);

        return true;
    }
</script>
<style type="text/css">
    #btn-print {
        vertical-align: bottom;
    }
    #img-loading
    {
        margin-left: 10px;
        vertical-align: text-bottom;
        visibility: hidden;
    }
    #msg-result
    {
        display: none;
        font-style: italic;
    }
</style>
';

&verification;

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

# Pied de la page HTML
my $paramType = $cgi->param("type");
my $paramMenu = $cgi->param("menu");
&Pied($delta_time, $paramType, $paramMenu);


sub verification {
    my $tableau;

    if (($cgi->param('verif.x') != 0) ||
         ($cgi->param('revision.x') != 0)||
         ($cgi->param('panne.x') != 0)||
         ($cgi->param('disp1.x') != 0)||
         ($cgi->param('disp2.x') != 0)||
         ($cgi->param('disp3.x') != 0)||
         ($cgi->param('invrevision.x') != 0)||
         ($cgi->param('invpanne.x') != 0)||
         ($cgi->param('invdisp1.x') != 0)||
         ($cgi->param('pantorev.x') != 0)||
         ($cgi->param('revtopan.x') != 0)||
         ($cgi->param('contverif.x') != 0)||
         ($cgi->param('verifcont.x') != 0))
    {
        &traitverif;
    }

    $tableau =qq{<div align="center"><font color="red"><b>$mess[850]</b></font></div>\n};
    $tableau .=qq{<table width="100%" border="0" cellspacing="2" cellpadding="0">\n};
    $tableau .=$cgi->start_form(-method=>'get', action=>'savmod.cgi');
    $tableau .=qq{\n<tr bgcolor="#C6CDC5" align="center">\n};
    $tableau .=qq{<td><font class="PT"><b>$mess[851]</b></font></td>\n};
    $tableau .=qq{<td><font class="PT"><b>$mess[872]</b></font></td>\n};
    $tableau .=qq{<td><font class="PT"><b>$mess[852]</b></font></td>\n};
    $tableau .=qq{<td><font class="PT"><b>$mess[873]</b></font></td>\n};
    $tableau .=qq{</tr>};
    my $req;
    my $sth;
    my $nbmach;
    $req   = $reqrech[0];
    $optsp = $optionsup[0];
    $optsp =~ s/agence/$agence/;
    # 1 ligne
    $tableau .= qq{<tr align="center">};

    # -- Machines en livraison --
    my $deliv;
    $deliv = $req . " " . $optrech[0];
    $deliv =~ s/acont/, CONTRATDD /;
    $deliv =~ s/ident/1/;
    $deliv .= $optsp;
    my $stateDelivery = substr(LOC::Machine::STATE_DELIVERY, -2);
    $deliv =~ s/num/$stateDelivery/eg;

    $sth = $DB->prepare($deliv);
    $sth->execute();
    $nbmach = $sth->rows;
    if ($nbmach <= 0)
    {
        $nbmach = 0;
    }
    $tableau .= qq{<td><select name="machdeliv">\n};
    $tableau .= qq{<option value="">$mess[853]\n};

    while (my @data = $sth->fetchrow_array())
    {
        # R�cup�ration des �toiles pour les VGP
        my $star = &getStars($data[4]);
        $data[5] = &format_datetime($data[5]);
        $tableau .=qq{<option value="$data[2], $data[3]">$data[0], $data[1], $data[5] $star\n};
    }
    $sth->finish;
    $tableau .= qq{</select>\n};
    $tableau .= qq{<font><b>$nbmach</b></font>};
    $tableau .= qq{</td>\n};

    # -- Machines en r�cup�ration --
    my $enrecup;
    $enrecup = $req . " " . $optrech[0];
    $enrecup =~ s/acont/, CONTRATDR /;
    $enrecup =~ s/ident/3\, 7/;
    $enrecup .= $optsp;
    my $stateRecovery = substr(LOC::Machine::STATE_RECOVERY, -2);
    $enrecup =~ s/num/$stateRecovery/eg;

    $sth = $DB->prepare($enrecup);
    $sth->execute();
    $nbmach = $sth->rows;
    if ($nbmach <= 0)
    {
        $nbmach = 0;
    }

    $tableau .= qq{<td><select name="machenrecup">\n};
    $tableau .= qq{<option value="">$mess[854]\n};

    while (my @data = $sth->fetchrow_array())
    {
        # R�cup�ration des �toiles pour les VGP
        my $star = &getStars($data[4]);
        $data[5] = &format_datetime($data[5]);
        $tableau .=qq{<option value="$data[2], $data[3]">$data[0], $data[1], $data[5] $star\n};
    }
    $sth->finish;
    $tableau .= qq{</select>\n};
    $tableau .= qq{<font><b>$nbmach</b></font>};
    $tableau .= qq{</td>\n};

    # -- Machines en transfert inter-chantiers --
    my $entrans;
    my $type = LOC::Transport::TYPE_SITESTRANSFER;
    $entrans = $req . $optrech[1];
    $entrans =~ s/acont//;
    $entrans =~ s/\<\%type\>/$type/;

    $sth=$DB->prepare($entrans);
    $sth->execute();
    $nbmach = $sth->rows;
    if ($nbmach <= 0)
    {
        $nbmach = 0;
    }

    $tableau .= qq{<td><select name="machtrans">\n};
    $tableau .= qq{<option value="">$mess[855]\n};

    while (my @data = $sth->fetchrow_array())
    {
        # R�cup�ration des �toiles pour les VGP
        my $star = &getStars($data[4]);
        $tableau .=qq{<option value=$data[2]>$data[0], $data[1] $star\n};
    }
    $sth->finish;
    $tableau .= qq{</select>\n};
    $tableau .= qq{<font><b>$nbmach</b></font>};
    $tableau .= qq{</td>\n};

    # -- Machines en transfert inter-agences --
    my $entrans;
    my $type = LOC::Transport::TYPE_AGENCIESTRANSFER;
    $entrans = $req .  $optrech[1];
    $entrans =~ s/acont/, tbl_f_transport.tsp_date, tbl_f_transport.tsp_agc_id_to /;
    $entrans =~ s/\<\%type\>/$type/;

    $sth=$DB->prepare($entrans);
    $sth->execute();
    $nbmach = $sth->rows;
    if ($nbmach <= 0)
    {
        $nbmach=0;
    }

    $tableau .= qq{<td><select name="machtransag">\n};
    $tableau .= qq{<option value="">$mess[855]\n};

    while (my @data = $sth->fetchrow_array())
    {
        # R�cup�ration des �toiles pour les VGP
        my $star = &getStars($data[4]);
        $data[5] = &format_datetime($data[5]);
        $tableau .=qq{<option value=$data[2]>$data[0], $data[1] $star, $data[5] --> $data[6]\n};
    }
    $sth->finish;
    $tableau .=qq{</select>\n};
    $tableau .=qq{<font><b>$nbmach</b></font>};
    $tableau .=qq{</td>\n};

    $tableau .=qq{</tr>\n};
    $tableau .=qq{</table>\n};
    $tableau .=qq{&nbsp;\n};

    # Sch�ma des modifications d'�tat

    $tableau .=qq{<table width="100%" border="0" cellspacing="2" cellpadding="0">\n};
    $tableau .=qq{<tr align="center">\n};
    $tableau .=qq{<td>&nbsp;</td>\n};
    $tableau .=qq{<td colspan=3>\n};

    # Machine en V�rification
    my $verif;
    $verif=$req;
    $verif =~ s/acont/ /;
    $verif .=qq{ WHERE };
    $verif .=$optsp;
    $verif =~ s/num/7/eg;
    $sth=$DB->prepare($verif);
    $sth->execute();
    $nbmach=$sth->rows;
    if ($nbmach<=0) {
        $nbmach=0
    };
    $tableau .=qq{<select name="machver">\n};
    $tableau .=qq{<option value="">$mess[856]\n};
    while (my @data=$sth->fetchrow_array())
    {
        # R�cup�ration des �toiles pour les VGP
        my $star = &getStars($data[4]);
        $tableau .=qq{<option value="$data[2], $data[3]">$data[0], $data[1] $star\n};
    }
    $sth->finish;
    $tableau .=qq{</select>\n};
    $tableau .=qq{<font><b>$nbmach</b></font>};
    $tableau .=qq{</td>\n};
    $tableau .=qq{<td nowrap align="left">};
    $tableau .=$cgi->image_button(-name=>'contverif', -alt=>$mess[1850], src=>"$URL_IMG/control_bas2.gif", -border=>0);
    $tableau .=qq{&nbsp;};
    $tableau .=$cgi->image_button(-name=>'verifcont', -alt=>$mess[1851], src=>"$URL_IMG/control_haut2.gif", -border=>0);
    $tableau .=qq{&nbsp;};
    # Machine en Contr�le
    my $controle;
    $controle=$req;
    $controle =~ s/acont/ /;
    $controle .=qq{ WHERE };
    $controle .=$optsp;
    $controle =~ s/num/5/eg;
    $sth=$DB->prepare($controle);
    $sth->execute();
    $nbmach=$sth->rows;
    if ($nbmach<=0) {$nbmach=0};
    $tableau .=qq{<select name="machcont">\n};
    $tableau .=qq{<option value="">$mess[1852]\n};
    while (my @data=$sth->fetchrow_array())
    {
        # R�cup�ration des �toiles pour les VGP
        my $star = &getStars($data[4]);
        $tableau .=qq{<option value="$data[2], $data[3]">$data[0], $data[1] $star\n};
    }
    $sth->finish;
    $tableau .=qq{</select>\n};
    $tableau .=qq{<font><b>$nbmach</b></font>};
    $tableau .=qq{</td>\n};
    $tableau .=qq{</tr>\n<tr align="center">\n};
    # 3 ligne
    $tableau .=qq{<td align="right">\n};
    $tableau .=$cgi->image_button(-name=>'revision', -alt=>$mess[857], src=>"$URL_IMG/verrev.gif", -border=>0);
    $tableau .=qq{</td>\n};
    $tableau .=qq{<td align="left">};
    $tableau .=$cgi->image_button(-name=>'invrevision', -alt=>$mess[858], src=>"$URL_IMG/revver.gif", -border=>0);
    $tableau .=qq{</td>\n};
    $tableau .=qq{<td>\n};
    $tableau .=$cgi->image_button(-name=>'invdisp1', -alt=>$mess[859], src=>"$URL_IMG/disver.gif", -border=>0);
    $tableau .=qq{</td>\n};
    $tableau .=qq{<td align="right">};
    $tableau .=$cgi->image_button(-name=>'invpanne', -alt=>$mess[860], src=>"$URL_IMG/panver.gif", -border=>0);
    $tableau .=qq{</td>\n};
    $tableau .=qq{<td align="left">\n};
    $tableau .=$cgi->image_button(-name=>'panne', -alt=>$mess[861], src=>"$URL_IMG/verpan.gif", -border=>0);
    $tableau .=qq{</td>\n};
    # Machine en revision
    $tableau .=qq{</tr>\n<tr align="center">\n};
    my $rev;
    $rev=$req;
    $rev =~ s/acont/ /;
    $rev .=qq{ WHERE };
    $rev .=$optsp;
    $rev =~ s/num/4/eg;
    $sth=$DB->prepare($rev);
    $sth->execute();
    $nbmach=$sth->rows;
    if ($nbmach<=0) {
        $nbmach=0
    };
    $tableau .=qq{<td><select name="machrev">\n};
    $tableau .=qq{<option value="">$mess[862]\n};
    while (my @data=$sth->fetchrow_array())
    {
        # R�cup�ration des �toiles pour les VGP
        my $star = &getStars($data[4]);
        $tableau .=qq{<option value="$data[2], $data[3]">$data[0], $data[1] $star\n};
    }
    $sth->finish;
    $tableau .=qq{</select>\n};
    $tableau .=qq{<font><b>$nbmach</b></font>};
    $tableau .=qq{</td>\n};
    $tableau .=qq{<td align="right">\n};
    $tableau .=$cgi->image_button(-name=>'pantorev', -alt=>$mess[863], src=>"$URL_IMG/panrev.gif", -border=>0);
    $tableau .=qq{</td>\n};
    $tableau .=qq{<td>&nbsp;</td>\n};
    $tableau .=qq{<td align="left">\n};
    $tableau .=$cgi->image_button(-name=>'revtopan', -alt=>$mess[864], src=>"$URL_IMG/revpan.gif", -border=>0);
    $tableau .=qq{</td>\n};
    # Machine en panne
    my $panne;
    $panne=$req;
    $panne =~ s/acont/, MADATEPANNE /;
    $panne .=qq{ WHERE };
    $panne .=$optsp;
    $panne =~ s/num/6/eg;
    $sth=$DB->prepare($panne);
    $sth->execute();
    $nbmach=$sth->rows;
    if ($nbmach<=0) {
        $nbmach=0
    };
    $tableau .=qq{<td><select name="machpanne">\n};
    $tableau .=qq{<option value="">$mess[865]\n};
    while (my @data=$sth->fetchrow_array())
    {
        # R�cup�ration des �toiles pour les VGP
        my $star = &getStars($data[4]);
        $data[5]=&format_datetime($data[5]);
        $tableau .=qq{<option value="$data[2], $data[3]">$data[0], $data[1], $data[5] $star\n};
    }
    $sth->finish;
    $tableau .=qq{</select>\n};
    $tableau .=qq{<font><b>$nbmach</b></font>};
    $tableau .=qq{</td>\n};
    # 4 ligne
    $tableau .=qq{</tr>\n<tr align="center">\n};
    $tableau .=qq{<td align="right">};
    $tableau .=$cgi->image_button(-name=>'disp2', -alt=>$mess[866], src=>"$URL_IMG/ok2.gif", -border=>0);
    $tableau .=qq{</td>\n};
    $tableau .=qq{<td>&nbsp;\n};
    $tableau .=qq{</td>\n};
    $tableau .=qq{<td>\n};
    $tableau .=$cgi->image_button(-name=>'disp1', -alt=>$mess[867], src=>"$URL_IMG/ok1.gif", -border=>0);
    $tableau .=qq{</td>\n};
    $tableau .=qq{<td>&nbsp;\n};
    $tableau .=qq{</td>\n};
    $tableau .=qq{<td align="left">};
    $tableau .=$cgi->image_button(-name=>'disp3', -alt=>$mess[868], src=>"$URL_IMG/ok3.gif", -border=>0);
    $tableau .=qq{</td>\n};
    $tableau .=qq{</tr>\n<tr align="center">\n};
    #5 ligne
    # Machine en dispo
    $tableau .=qq{<td>&nbsp;\n};
    $tableau .=qq{</td>\n};
    $tableau .=qq{<td colspan=3>\n};
    my $dispo;
    $dispo=$req;
    $dispo =~ s/acont/ /;
    $dispo .=qq{ WHERE };
    $dispo .=$optsp;
    $dispo =~ s/num/2/eg;
    $sth=$DB->prepare($dispo);
    $sth->execute();
    $nbmach=$sth->rows;
    if ($nbmach<=0) {
        $nbmach=0
    };
    my $tabMachines = $sth->fetchall_hashref("MAAUTO");
    my $tabAvailDates = &date_disp([keys(%$tabMachines)]);
    my $tabOption = [];
    $sth->execute();
    while (my @data=$sth->fetchrow_array())
    {
        # R�cup�ration des �toiles pour les VGP
        my $star = &getStars($data[4]);

        my $date_disp = $tabAvailDates->{$data[2]};
        my ($jrDisp, $msDisp, $anDisp) = split(/\//, $date_disp);
        push(@$tabOption, {'MAAUTO' => $data[2],
                           'MOMAAUTO' => $data[3],
                           'MANOPARC' => $data[0],
                           'MOMADESIGNATION' => $data[1],
                           'DATEDISP' => sprintf('%04d-%02d-%02d', $anDisp, $msDisp, $jrDisp),
                           'star' => $star});
    }
    $sth->finish;
    $tableau .=qq{<select name="machdisp">\n};
    $tableau .=qq{<option value="">$mess[869]\n};
    foreach (@$tabOption)
    {
        my $maauto          = $_->{'MAAUTO'};
        my $momaauto        = $_->{'MOMAAUTO'};
        my $manoparc        = $_->{'MANOPARC'};
        my $momadesignation = $_->{'MOMADESIGNATION'};
        my $datedisp        = &format_datetime($_->{'DATEDISP'});
        my $star            = $_->{'star'};
        $tableau .=qq{<option value="$maauto, $momaauto">$manoparc, $momadesignation, $datedisp $star\n};
    }
    $tableau .=qq{</select>\n};
    $tableau .=qq{<font><b>$nbmach</b></font>};
    $tableau .=qq{</td>\n};
    $tableau .=qq{<td>&nbsp;};
    $tableau .=qq{</td>\n};
    $tableau .=qq{</tr>};
    #6 ligne
    $tableau .=qq{<tr align="center">};
    $tableau .=qq{<td colspan=5></td>\n};
    $tableau .=qq{</tr>};
    #7 ligne
    $tableau .=qq{<tr align="center">};
    $tableau .=qq{<td></td>\n};
    $tableau .=qq{<td colspan=3><font class="PT">$mess[870] : </font>};
    # liste etat
    my $reqet=qq{select ETLIBELLE, ETCODE FROM ETATTABLE WHERE ETCODE LIKE "MAC%" ORDER BY ETLIBELLE};
    $sth=$DB->prepare($reqet);
    $sth->execute();
    $tableau .=qq{<select name="etatimp">\n};
    while (my @data=$sth->fetchrow_array()) {
        $tableau .=qq{<option value="$data[1]" >$data[0]\n};
    }
    $sth->finish;
    $tableau .=qq{</select>&nbsp;};
    $tableau .=$G_LST_IMP;
    $tableau .=qq{&nbsp;};

    $tableau .= '<a href="#" onclick="printList(); return false;"><img src="' . $URL_IMG . '/imprimer.gif" alt="' . $mess[871] . '" id="btn-print" /></a>';
    $tableau .= '<img src="' . $URL_IMG . '/../../img/loading.gif" alt="" id="img-loading" />';
    $tableau .= '<p id="msg-result">' . $mess[874] . '</p>';

    $tableau .=$cgi->hidden('impfich', 'lstmach');
    $tableau .=qq{</td>\n};
    $tableau .=qq{<td>&nbsp;</td>\n};
    $tableau .=qq{</tr>\n};
    $tableau .=qq{</form>};
    print $tableau;
}

sub init_var
{
    $titre = $mess[3950];
    $reqrech[0] = qq{
SELECT
    CONCAT_WS("", MACHINE.MANOPARC_AUX, MACHINE.MANOPARC, IF (MASUPPAGAUTO IS NOT NULL, CONCAT(" > ", MASUPPAGAUTO), "")) AS MANOPARC,
    MOMADESIGNATION,
    MACHINE.MAAUTO,
    MODELEMACHINE.MOMAAUTO,
    IF (MOMACONTROLEVGP != 0, COMAAPAVE, "") acont
FROM AUTH.`MACHINE`
LEFT JOIN AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=MACHINE.MOMAAUTO
LEFT JOIN CONTROLEMACHINE ON CONTROLEMACHINE.MAAUTO=MACHINE.MAAUTO };

    # options pour recherche standard
    $optrech[0] = qq{
LEFT JOIN CONTRAT ON MACHINE.CONTRATAUTO=CONTRAT.CONTRATAUTO
WHERE
    LIETCONTRATAUTO IN (ident)
    AND CONTRATOK="-1" AND };

    # options pour recherche transferts
    $optrech[1] = '
INNER JOIN LOCATION.f_transport tbl_f_transport ON tbl_f_transport.tsp_mac_id = MACHINE.MAAUTO
WHERE
    tbl_f_transport.tsp_type = \'<%type>\'
    AND tbl_f_transport.tsp_is_unloadingdone = 0
    AND tbl_f_transport.tsp_sta_id = \'' . LOC::Transport::STATE_ACTIVE . '\'
    AND tbl_f_transport.tsp_agc_id = \'' . $G_AGENCE. '\'
ORDER BY MACHINE.MANOPARC, MOMADESIGNATION';

    # options suppl�mentaires
    $optionsup[0] = qq{(LIETCODE="num" OR MACHINE.ETCODE="MAC0num")
    AND MACHINE.AGAUTO="agence"
ORDER BY MACHINE.MANOPARC, MOMADESIGNATION};

    $reqtrait[0] = qq{UPDATE AUTH.`MACHINE` SET LIETCODE="val0", ETCODE="MAC0val0" WHERE MAAUTO="val1"};
    $reqtrait[1] = qq{UPDATE AUTH.`STOCKMODELE` SET STMORESERVABLE = "val8", STMODIS = "val9", STMOREC = "val10", STMOREV = "val11", STMOCON = "val12", STMOPAN = "val13", STMOVER = "val14" WHERE AGAUTO="agence" AND MOMAAUTO="val15"};
    $reqtrait[2] = qq{UPDATE AUTH.`MACHINE` SET MADATEPANNE="val16" WHERE MAAUTO="val1"};
    for (my $i = 0 ; $i < @reqrech ; $i++)
    {
        $reqrech[$i] =~ s/agent/$G_PEAUTO/;
        $reqrech[$i] =~ s/agence/$G_AGENCE/;
    }
    for (my $i=0 ; $i < @reqtrait ; $i++)
    {
        $reqtrait[$i] =~ s/agent/$G_PEAUTO/;
        $reqtrait[$i] =~ s/agence/$G_AGENCE/;
    }
}


#
# traitement verification
#
sub traitverif {
    my @val;
    my @req;
    my $countryId = $G_PAYS;
    # machine
    $req[0] = $reqtrait[0];
    $val[0] = 0; # nouveau lietcode de la machine
    $val[1] = 0; # maaauto de la machine
    my $etatmach=0;
    my $automach=0;

    my $oldStateId = undef;
    my $newStateId = undef;
    my $isVisibleMachine = 0;
    # Passage en disponible
    if ((($cgi->param('disp1.x') != 0) && ($cgi->param('machver') ne '')) ||
        (($cgi->param('disp2.x') != 0) && ($cgi->param('machrev') ne '')) ||
        (($cgi->param('disp3.x') != 0) && ($cgi->param('machpanne') ne '')))
    {
        $val[0]=2;
        $newStateId = LOC::Machine::STATE_AVAILABLE;

        # depuis "en v�rification"
        if ($cgi->param('disp1.x') != 0)
        {
            my $param = $cgi->param('machver');
            my ($v1, $v2) = split(/, /, $param);
            $val[1]   = $v1;
            $val[15]  = $v2;
            $etatmach = 7;
            $oldStateId = LOC::Machine::STATE_CHECK;
            $automach = $val[1];
        }
        if ($cgi->param('disp2.x') != 0)
        {
            my $param = $cgi->param('machrev');
            my ($v1, $v2) = split(/, /, $param);
            $val[1]   = $v1;
            $val[15]  = $v2;
            $etatmach = 4;
            $oldStateId = LOC::Machine::STATE_REVIEW;
            $automach = $val[1];
        }
        if ($cgi->param('disp3.x') != 0)
        {
            my $param = $cgi->param('machpanne');
            my ($v1, $v2) = split(/, /, $param);
            $val[1]   = $v1;
            $val[15]  = $v2;
            $etatmach = 6;
            $oldStateId = LOC::Machine::STATE_BROKEN;
            $automach = $val[1];
        }
    }
    # dispo to verif, panne to verif, rev to verif
    if ((($cgi->param('invpanne.x') != 0) && ($cgi->param('machpanne') ne '')) ||
        (($cgi->param('invrevision.x') != 0) && ($cgi->param('machrev') ne '')) ||
        (($cgi->param('invdisp1.x') != 0) && ($cgi->param('machdisp') ne '')))
    {
        $val[0] = 7;
        $newStateId = LOC::Machine::STATE_CHECK;

        if ($cgi->param('machpanne') ne '')
        {
            my $param = $cgi->param('machpanne');
            my ($v1, $v2) = split(/, /, $param);
            $val[1]   = $v1;
            $val[15]  = $v2;
            $etatmach = 6;
            $oldStateId = LOC::Machine::STATE_BROKEN;
            $automach = $val[1];
        }
        if ($cgi->param('machrev') ne '')
        {
            my $param = $cgi->param('machrev');
            my ($v1, $v2) = split(/, /, $param);
            $val[1]   = $v1;
            $val[15]  = $v2;
            $etatmach = 4;
            $oldStateId = LOC::Machine::STATE_REVIEW;
            $automach = $val[1];
        }
        if ($cgi->param('machdisp') ne '')
        {
            my $param = $cgi->param('machdisp');
            my ($v1, $v2) = split(/, /, $param);
            $val[1] = $v1;
            $val[15] = $v2;
            $etatmach = 2;
            $oldStateId = LOC::Machine::STATE_AVAILABLE;
            $automach = $val[1];
        }
    }
    # verif to panne, rev to panne
    if ((($cgi->param('panne.x') != 0) && ($cgi->param('machver') ne '')) ||
        (($cgi->param('revtopan.x') != 0) && ($cgi->param('machrev') ne '')))
    {
        $val[0] = 6;
        $newStateId = LOC::Machine::STATE_BROKEN;

        # Passage de l'�tat V�rification -> Panne
        if ($cgi->param('machver') ne '')
        {
            my $param = $cgi->param('machver');
            my ($v1, $v2) = split(/, /, $param);
            $val[1]   = $v1;
            $val[15]  = $v2;
            $etatmach = 7;
            $oldStateId = LOC::Machine::STATE_CHECK;
            $automach = $val[1];
            # La machine est-elle visible sur une autre agence?
            $isVisibleMachine = &isVisibleMachine($automach, $etatmach);
        }
        # Passage de l'�tat R�vision ->  Panne
        if ($cgi->param('machrev') ne '')
        {
            my $param = $cgi->param('machrev');
            my ($v1, $v2) = split(/, /, $param);
            $val[1]   = $v1;
            $val[15]  = $v2;
            $etatmach = 4;
            $oldStateId = LOC::Machine::STATE_REVIEW;
            $automach = $val[1];
            # La machine est-elle visible sur une autre agence?
            $isVisibleMachine = &isVisibleMachine($automach, $etatmach);
        }
        # Mise � jour de la date de la panne si la machine n'est pas visible sur une autre agence
        if ($isVisibleMachine == 0)
        {
            my ($sec, $min, $hr, $jour, $mois, $an, $joursm, $jouran, $ete) = localtime;
            $mois +=1;
            $an +=1900;
            $val[16] = $an.":".$mois.":".$jour;
            $req[2]  = $reqtrait[2];
            $req[2]  =~ s/val16/$val[16]/;
            $req[2]  =~ s/val1/$val[1]/;
            $DB->do($req[2]);
        }
    }
    # verif to revison, panne to rev
    if ((($cgi->param('revision.x') != 0) && ($cgi->param('machver') ne '')) ||
        (($cgi->param('pantorev.x') != 0) && ($cgi->param('machpanne') ne '')))
    {
        $val[0] = 4;
        $newStateId = LOC::Machine::STATE_REVIEW;

        if ($cgi->param('revision.x') != 0)
        {
            my $param = $cgi->param('machver');
            my ($v1, $v2) = split(/, /, $param);
            $val[1]   = $v1;
            $val[15]  = $v2;
            $etatmach = 7;
            $oldStateId = LOC::Machine::STATE_CHECK;
            $automach = $val[1];
        }
        if ($cgi->param('pantorev.x') != 0)
        {
            my $param = $cgi->param('machpanne');
            my ($v1, $v2) = split(/, /, $param);
            $val[1]   = $v1;
            $val[15]  = $v2;
            $etatmach = 6;
            $oldStateId = LOC::Machine::STATE_BROKEN;
            $automach = $val[1];
        }
    }

    # controle to verif
    if (($cgi->param('contverif.x') != 0) && ($cgi->param('machcont') ne ''))
    {
        $val[0] = 7;
        $newStateId = LOC::Machine::STATE_CHECK;

        my $param = $cgi->param('machcont');
        my ($v1, $v2) = split(/, /, $param);
        $val[1]   = $v1;
        $val[15]  = $v2;
        $etatmach = 5;
        $oldStateId = LOC::Machine::STATE_CONTROL;
        $automach = $val[1];
    }
    # Passage de V�rification -> Contr�le
    if (($cgi->param('verifcont.x') != 0) && ($cgi->param('machver') ne ''))
    {
        $val[0] = 5;
        $newStateId = LOC::Machine::STATE_CONTROL;
        my $param = $cgi->param('machver');
        my ($v1, $v2) = split(/, /, $param);
        $val[1]   = $v1;
        $val[15]  = $v2;
        $etatmach = 7;
        $oldStateId = LOC::Machine::STATE_CHECK;
        $automach = $val[1];
        # La machine est-elle visible sur une autre agence?
        $isVisibleMachine = &isVisibleMachine($automach, $etatmach);
    }
    my $booleetat = &verif_etat($automach, $etatmach);
    if ($booleetat == 0)
    {
        print qq{<center><b><font color="red">Vous avez d�j� valid� cette machine.</font></b></center>\n};
    }
    # La machine est visible sur une autre agence : les changements "V�rif->Panne", "V�rif->Contr�le" et "R�vision -> Panne" ne sont pas possibles
    elsif ($isVisibleMachine == 1)
    {
        print qq{<center><b><font color="red">Vous ne pouvez modifier l'�tat de la machine par l'�tat s�lectionn� car la machine est visible sur une autre agence.</font></b></center>\n};
    }
    else
    {
        if (($val[0] != 0 ) && ($val[1] != 0))
        {
            # Mise � jour de l'�tat de la machine
            $req[0] =~ s/val0/$val[0]/eg;
            $req[0] =~ s/val1/$val[1]/;
            $DB->do($req[0]);

            # R�cup�ration de la connexion � la base de donn�es
            my $db = &LOC::Db::getConnection('location', $countryId);
            $db->beginTransaction();

            # Changements pouvant impacter le transport ? => on le lui communique
            &LOC::Transport::onExternalChanges($countryId, {
                'machine' => {
                    'id'    => $val[1],
                    'event' => 'update',
                    'props' => {
                        'state.id' => {
                            'old' => $oldStateId,
                            'new' => $newStateId
                        }
                    }
                }
            }, $G_PEAUTO, undef, undef);

            # Valide la transaction
            if (!$db->commit())
            {
                $db->rollBack();
            }

        }

        # stock
        my $i;
        $req[1] = $reqtrait[1];
        my $rqstck = qq{SELECT STMORESERVABLE, STMODIS, STMOREC, STMOREV, STMOCON, STMOPAN, STMOVER FROM AUTH.`STOCKMODELE` WHERE AGAUTO="$agence" AND MOMAAUTO="$val[15]"};
        my $sth = $DB->prepare($rqstck);
        $sth->execute();
        while (my @data = $sth->fetchrow_array())
        {
            for($i = 0 ; $i < 7 ; $i++)
            {
                my $l = $i + 8;
                $val[$l] = $data[$i];
            }
        }
        $sth->finish;

        # Mise � jour des stocks
        $val[7] = 0; # MAAUTO de la machine

        # Disponible -> V�rification
        if (($cgi->param('invdisp1.x') != 0) && ($cgi->param('machdisp') ne ''))
        {
            my $param = $cgi->param('machdisp');
            my ($v1, $v2) = split(/, /, $param);
            $val[7]   = $v1;
            $val[9]  -= 1; # DIS
            $val[14] += 1; # VER
        }
        # Contr�le -> V�rification
        if (($cgi->param('contverif.x') != 0) && ($cgi->param('machcont') ne ''))
        {
            my $param = $cgi->param('machrev');
            my ($v1, $v2) = split(/, /, $param);
            $val[7]   = $v1;
            $val[12] -= 1; # CON
            $val[14] += 1; # VER
            $val[8]  += 1; # RESERVABLE
        }
        # V�rification -> Contr�le
        if (($cgi->param('verifcont.x') != 0) && ($cgi->param('machver') ne ''))
        {
            my $param = $cgi->param('machrev');
            my ($v1, $v2) = split(/, /, $param);
            $val[7]   = $v1;
            $val[12] += 1; # CON
            $val[14] -= 1; # VER
            $val[8]  -= 1; # RESERVABLE
        }
        # R�vision -> V�rification
        if (($cgi->param('invrevision.x') != 0) && ($cgi->param('machrev') ne ''))
        {
            my $param = $cgi->param('machrev');
            my ($v1, $v2) = split(/, /, $param);
            $val[7]   = $v1;
            $val[11] -= 1; # REV
            $val[14] += 1; # VER
        }
        # Panne -> V�rification
        if (($cgi->param('invpanne.x') != 0) && ($cgi->param('machpanne') ne ''))
        {
            my $param = $cgi->param('machpanne');
            my ($v1, $v2) = split(/, /, $param);
            $val[7] = $v1;
            $val[13] -= 1; # PAN
            $val[14] += 1; # VER
            $val[8]  += 1; # RESERVABLE
        }
        # R�vision -> Panne
        if (($cgi->param('revtopan.x') != 0) && ($cgi->param('machrev') ne ''))
        {
            my $param = $cgi->param('machrev');
            my ($v1, $v2) = split(/, /, $param);
            $val[7]   = $v1;
            $val[11] -= 1; # REV
            $val[13] += 1; # PAN
            $val[8]  -= 1; # RESERVABLE
        }
        # Panne -> R�vision
        if (($cgi->param('pantorev.x') != 0) && ($cgi->param('machpanne') ne ''))
        {
            my $param = $cgi->param('machpanne');
            my ($v1, $v2) = split(/, /, $param);
            $val[7]   = $v1;
            $val[13] -= 1; # PAN
            $val[11] += 1; # REV
            $val[8]  += 1; # RESERVABLE
        }
        # V�rification -> Disponible
        if (($cgi->param('disp1.x') != 0) && ($cgi->param('machver') ne ''))
        {
            my $param = $cgi->param('machver');
            my ($v1, $v2) = split(/, /, $param);
            $val[7]   = $v1;
            $val[14] -= 1; # VER
            $val[9]  += 1; # DIS
        }
        # R�vision -> Disponible
        if (($cgi->param('disp2.x') != 0) && ($cgi->param('machrev') ne ''))
        {
            my $param = $cgi->param('machrev');
            my ($v1, $v2) = split(/, /, $param);
            $val[7]   = $v1;
            $val[11] -= 1; # REV
            $val[9]  += 1; # DIS
        }
        # Panne -> Disponible
        if (($cgi->param('disp3.x') != 0) && ($cgi->param('machpanne') ne ''))
        {
            my $param = $cgi->param('machpanne');
            my ($v1, $v2) = split(/, /, $param);
            $val[7]   = $v1;
            $val[13] -= 1; # PAN
            $val[9]  += 1; # DIS
            $val[8]  += 1; # RESERVABLE
        }
        # V�rification -> Panne
        if (($cgi->param('panne.x') != 0) && ($cgi->param('machver') ne ''))
        {
            my $param = $cgi->param('machver');
            my ($v1, $v2) = split(/, /, $param);
            $val[7]   = $v1;
            $val[14] -= 1; # VER
            $val[13] += 1; # PAN
            $val[8]  -= 1; # RESERVABLE
        }
        # V�rification -> R�vision
        if (($cgi->param('revision.x') != 0) && ($cgi->param('machver') ne ''))
        {
            my $param = $cgi->param('machver');
            my ($v1, $v2) = split(/, /, $param);
            $val[7]   = $v1;
            $val[14] -= 1; # VER
            $val[11] += 1; # REV
        }

        for($i = 8 ; $i < 16 ; $i++)
        {
            $req[1] =~ s/val$i/$val[$i]/;
        }
        $DB->do($req[1]);

        # Impacts sur le transport
        # - Verrouillage des transferts inter-agences r�alis�s sur la machine
        my $tabEndUpds = {};
        my $tabFilters = {
            'type'            => LOC::Transport::TYPE_AGENCIESTRANSFER,
            'isLocked'        => 0,
            'machineId'       => $val[1],
            'isUnloadingDone' => 1
        };
        # Liste des pays
        my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);
        # Verrouillage pour chaque pays
        foreach my $country (@$tabCountries)
        {
            &LOC::Transport::lock($country, $tabFilters, $G_PEAUTO, $tabEndUpds);
        }

        # Cr�ation de la trace de changement d'�tat
        my $oldStateLabel = '';
        my $newStateLabel = '';
        my $query = '
SELECT
    ETCODE as `state.id`,
    ETLIBELLE as `state.label`
FROM
    ETATTABLE
WHERE
    ETCODE IN("' . $oldStateId . '", "' . $newStateId . '");';

        my $sth = $DB->prepare($query);
        $sth->execute();
        while (my $tabRow = $sth->fetchrow_hashref())
        {
            if ($tabRow->{'state.id'} eq $oldStateId)
            {
                $oldStateLabel = encode('UTF-8', $tabRow->{'state.label'});
            }
            else
            {
                $newStateLabel = encode('UTF-8', $tabRow->{'state.label'});
            }
        }
        $sth->finish;

        my $schema = &LOC::Db::getSchemaName($G_PAYS);
        my $tabContent = {
            'schema'  => $schema,
            'code'    => LOC::Log::EventType::UPDMACHINESTATE,
            'old'     => $oldStateId,
            'new'     => $newStateId,
            'content' => $oldStateLabel . ' -> ' . $newStateLabel
        };

        &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $val[1], $tabContent);
        &LOC::EndUpdates::exec($G_PAYS, $tabEndUpds, $G_PEAUTO);

        # Mise � jour Kimoce
        &situation($val[1], 3, 0, 0, 0);
        &carac($val[1], 3, 0, 0, 0);
    }
}

sub verif_etat
{
    my @param=@_;
    my $booleet = 0;
    my $reqetat = qq{select MAAUTO FROM AUTH.`MACHINE` WHERE MAAUTO="$param[0]" AND LIETCODE="$param[1]"};
    my $stetat = $DB->prepare($reqetat);
    $stetat->execute();
    $booleet = $stetat->rows;
    $stetat->finish;
    return $booleet;
}


sub isVisibleMachine
{
    my @param = @_;
    my $isVisibleMachine;
    my $query = "
SELECT
    MASUPPAGAUTO
FROM AUTH.MACHINE WHERE MAAUTO = " . $param[0] . "
AND LIETCODE = " . $param[1];
    my $result = $DB->prepare($query);
    $result->execute;
    my ($agencySupp) = $result->fetchrow_array();
    $result->finish;
    $isVisibleMachine = ($agencySupp ne '' ? 1 : 0);
    return $isVisibleMachine;
}

# Function: date_disp
# Retourne la derni�re date de mise en disponibilit� d'une machine
#
# Returns:
# string - Date
sub date_disp {
    my ($tabMachinesId) = @_;

    my $tabAvailDates = {};

    my $nbMachines = scalar @$tabMachinesId;

    if ($nbMachines > 0)
    {
        my $query = '
SELECT
    log_functionalityid,
    DATE_FORMAT(MAX(log_date), "%d/%m/%Y")
FROM AUTH.h_log
WHERE log_functionality = "MACHINE"
    AND log_functionalityid IN (' . join(", ", @$tabMachinesId). ')
    AND log_etp_id = "' . LOC::Log::EventType::UPDMACHINESTATE . '"
    AND log_newvalues = "' . LOC::Machine::STATE_AVAILABLE . '"
GROUP BY log_functionalityid;';

        my $stetat = $DB->prepare($query);
        $stetat->execute();
        while (@data = $stetat->fetchrow_array())
        {
            if ($data[1] eq "")
            {
                my $queryTrace = '
SELECT
    DATE_FORMAT(TRACETEMPS,"%d/%m%/%Y")
FROM TRACE
WHERE MAAUTO = ' . $data[0] . '
ORDER BY TRACEAUTO DESC LIMIT 1';

                my $sthTrace = $DB->prepare($queryTrace);
                $sthTrace->execute();
                ($tabAvailDates->{$data[0]}) = $sthTrace->fetchrow_array();
                $sthTrace->finish;
            }
            else
            {
                $tabAvailDates->{$data[0]} = $data[1];
            }
        }
        $stetat->finish;
    }

    return $tabAvailDates;
}

sub getStars
{
    my ($nextControlDate) = @_;

    my $nbstar = 0;
    my ($jr, $ms, $an) = &datetojjmsan($nextControlDate);
    if ($nextControlDate ne '')
    {
        $nbstar = &date_cont($jr, $ms, $an);
    }
    my $star = '';
    my $k;
    for($k = 0 ; $k < $nbstar ; $k++)
    {
        $star .= '*';
    }
    return $star;
}
