#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';

#use strict;

use CGI;
use autentif;
use calculdate;
#use securite;
use titanic;
use LOC::Globals;
use LOC::Machine;
use LOC::Request;

#use accesbases;
use accesbd;
use varglob;
use Date::Calc qw(Day_of_Week Delta_Days Add_Delta_Days);
use message;
use tracetemps;

# Initialisation du chrono
my $timer = &init_chrono();


#connexion
my $DB=&Environnement("LOCATION");
#&authentification(&login);
#my $pe_login = &login;

#initialisation CGI
my $cgi=new CGI;
$cgi->autoEscape(undef);
my @mess=&message($G_PAYS);

$|=1;

#Variables
my $titre;
#my $hl_color = "00CCFF";
#my $HTTP_PORTAIL;

my $nbchamps;
my @namerech;
my @typerech;
my @librech;
my @reqrech;
my @optrech;

my @libetat;

my $i;

my $agence=$G_AGENCE;

my $nbjoursmois = 21;
my $taux = 0.0192;

&init_var;

&ente($titre, $titre, $G_PENOM, $G_PEPRENOM);
print qq{<SCRIPT language="JavaScript" src="$HTTP_PORTAIL/web/js/common.js"></SCRIPT>};
print qq{<SCRIPT language="JavaScript" src="$HTTP_PORTAIL/web/js/httpRequest.js"></SCRIPT>};

print '
<script type="text/javascript">
    var printHttpObj;
    function printList ()
    {
        var state   = document.getElementsByName("etatimp")[0].value;
        var printer = document.getElementsByName("imprimante")[0].value.substr(0, 7);

        var file    = "' . &LOC::Request::createRequestUri('machine:list:pdf') . '&stateId=" + state;
        var url     = "' . &LOC::Request::createRequestUri('print:render:print') . '";

        // Affichage d\'un indicateur de chargement
        var loadingImg = document.getElementById("img-loading");
        loadingImg.style.visibility = "visible";
        // Message de retour
        var resultMsg = document.getElementById("msg-result");
        resultMsg.style.display = "none";

        if (!printHttpObj)
        {
            printHttpObj = new Location.HTTPRequest(url);
            // Codes de retour valide :
            // 204 : pas de contenu (code de retour normal)
            // 1223 : code transform� par IE
            // 0 : code transform� par Google Chrome Frame
            printHttpObj.setValidResponseStatusRegExp("^0|204|1223$");
        }

        printHttpObj.oncompletion = function()
        {
            loadingImg.style.visibility = "hidden";
            resultMsg.style.display = "inline-block";
        };
        printHttpObj.onerror = function()
        {
            // Erreur
            alert("Erreur " + printHttpObj.getResponseStatus());
            loadingImg.style.visibility = "hidden";
        };

        // Passage de l\'id du client en param�tre
        printHttpObj.setVar(\'printer\', printer);
        printHttpObj.setVar(\'url\', file);
        printHttpObj.setVar(\'number\', 1);

        // Envoi de la requ�te HTTP
        printHttpObj.send(true);

        return true;
    }
</script>
<style type="text/css">
    #btn-print {
        vertical-align: bottom;
    }
    #img-loading
    {
        margin-left: 10px;
        vertical-align: text-bottom;
        visibility: hidden;
    }
    #msg-result
    {
        margin-left: 10px;
        display: none;
        font-style: italic;
    }
</style>
';

#recuperation taux de change
my $tauxE = tauxLocalToEuro($G_PAYS);

&tableau;

$DB->disconnect;

print qq{</table>\n};

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

# Pied de la page HTML
my $paramType = $cgi->param("type");
my $paramMenu = $cgi->param("menu");
&Pied($delta_time, $paramType, $paramMenu);


#
# Tableau contrats Lou�s Diff�rent �tats machines
#
sub tableau {
	my $tableau="";
	my @req;
	my $sth;
	my $nombre=0;
	$tableau .=$cgi->start_form(-method=>'get', action=>'tableau.cgi');
	#entete
	$tableau .=$cgi->hidden('menu');
	$tableau .=$cgi->hidden('type');
	$tableau .=qq{<table width="100%" border="0" cellspacing="2" cellpadding="0">\n};
	#1ligne
	#louee
	$tableau .=qq{<tr>\n};
	$tableau .=qq{<td>\n};
	$req[0]=$reqrech[0];

	my $boole=0;
	my $num=$cgi->param("$namerech[0]");
	#if ($cgi->param('date.x') !=0) {$boole=1};
	#if ($cgi->param('ville.x') !=0) {$boole=2};
	my $type=$cgi->param('ordre');

	if ($type ne "") {$req[0] .=" ORDER BY ".$type} else {$req[0] .=" ".$optrech[$boole]};
	#$req[0] .=" ".$optrech[$boole];

	$sth=$DB->prepare($req[0]);
	$sth->execute();
	my $nbrow=$sth->rows;

	my @label;
	@label=@{$sth->{NAME}};
	$nombre +=$nbrow;
	$tableau .=qq{<font><b>$nbrow $librech[0]</b>\n};
	#$tableau .=$cgi->image_button(-name=>'date', -alt=>$mess[1150], src=>"$URL_IMG/recalk_off.gif", -border=>0);
	#$tableau .=$cgi->image_button(-name=>'ville', -alt=>$mess[1151], src=>"$URL_IMG/recalk_off.gif", -border=>0);
	$tableau .=qq{</font></td></tr>\n};
	$tableau .=qq{<tr>\n};
	$tableau .=qq{<td align="left">\n};
	$tableau .=qq{<table width="100%" border="0" cellspacing="2" cellpadding="0">\n};
	my $ordre=0;

	while (my @data=$sth->fetchrow_array()) {
		my $c=$ordre % 2;
		my $color="''";
		my $euclide = $ordre % 20;
		if ($euclide == 0) {
			$tableau .=qq{<tr bgcolor="#C6CDC5">\n};
			for (my $i=0;$i<$nbcol;$i++) {
                $label = '';
                if ($i < 12)
                {
                    $label = $label[$i];
                }
                elsif ($i == 15)
                {
                    $label = $label[$i + 3];
                }
				$tableau .=qq{<th>};
				$tableau .=qq{<font><b>};
				if ($label eq '')
				{
				    $tableau .= qq{<font style="font-size: 8pt">$libcol[$i]</font>};
				}
				else
				{
				    $tableau .= qq{<a href="$HTTP_PORTAIL/cgi-bin/old/location/tableau.cgi?menu=tableau&type=tableau&ordre=$label">$libcol[$i]</a>};
				}
				$tableau .=qq{</b></font>};
				$tableau .=qq{</th>\n};
			}
			$tableau .=qq{</tr>\n};
		}
		if ($c == 1) {$color=qq{'#D6D6D6'};}
		$tableau .=qq{<TR bgColor=$color onmouseover="bgColor='#FFCC33'" onmouseout="bgColor=$color">\n};
		my $tarif = 0;
		my $mensuel = "";
		my $date_debut = "";
		my $date_fin = "";

		for($i=0;$i<@data-6;$i++) {
			my $soc="";
			if ($label[$i] eq "CLSTE") {$soc=$data[$i];
										if ($soc=~ /\'/) {$soc=~s/\'/\\\'/g};
										}
#			$data[$i]=substr($data[$i], 0, 13);
			if ($label[$i] eq "CONTRATDR") {
				$date_fin = $data[$i];
				$data[$i]=&format_datetime($data[$i]);
			}
			elsif ($label[$i] eq "CONTRATCODE") {
                my $contractUrl = &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $data[@data-5]});
				$data[$i]=qq{<a href="$contractUrl" target="_blank">$data[$i]</a>\n};
			}
			elsif ($label[$i] eq "MOPXJOURFR") {
				$data[$i] = &fnombre($data[$i], 3, 2);
			}
			elsif ($label[$i] eq "MOTARIFMSFR") {
				# Contrat mensuel
				if ($data[$i] > 0) {
					$mensuel = "M";
				}
				$data[$i] = &fnombre($data[$i] / $nbjoursmois, 3, 2);
			}
			elsif ($label[$i] eq "MOFORFAITFR") {
				# Contrat forfaitaire
				if ($data[$i] > 0) {
					$mensuel = "F";
				}
				if ($data[@data - 3] == 0) {
					$data[$i] = "?";
				}
				else {
					$data[$i] = &fnombre($data[$i] / $data[@data - 3], 3, 2);
				}
			}
			elsif ($label[$i] eq "CLSTE") {
				$data[$i]=qq{<a href="#" onclick="return rs('infoclient', '$HTTP_PORTAIL/cgi-bin/old/location/infoclient.cgi?value=$data[@data-4]', 500, 450)">$data[$i]</a>\n};
			}
            # Colonne N� Parc
            elsif ($label[$i] eq "MANOPARC")
            {
                my $parkNumber = $data[$i];
                # Si une machine est attribu�e sur le contrat
                if ($data[20] ne '')
                {
                    my $score = 0;
                    # R�cup�ration du score de la machine
                    my $machObj = &LOC::Machine::getInfos($G_PAYS, $data[20], undef, {'level' => LOC::Machine::LEVEL_MODEL});
                    if ($machObj->{'isControlVgp'})
                    {
                        $score = $machObj->{'control.score'};
                    }
                    $parkNumber .= '*' x $score;
                }
                $data[$i] = $parkNumber;
            }
#			elsif ($label[$i] eq "FAMAELEVATION") {
#				$data[$i]=qq{<font class="PT" color="red"><b>$data[$i]</b></font>\n};
#			}
# 			elsif ($label[$i] eq "CLPOTENTIEL") {
# 				$data[$i]=qq{<b>$data[$i]</b>\n};
# 			}
			if (!(($label[$i] eq "MOPXJOURFR" || $label[$i] eq "MOTARIFMSFR" || $label[$i] eq "MOFORFAITFR")&&
				$data[$i] eq &fnombre(0, 3, 2))	&& $data[$i] ne "?") {
				if($i == 12)
				{
					$tableau .=qq{<td>\n};
					$tableau .=qq{<font class="PT">\n};
					$tableau .=qq{<span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">
					$data[$i]</span></font></td>\n};
					$tarif = $data[$i];
				}
				else
				{
					$tableau .=qq{<td>\n};
					$tableau .=qq{<font class="PT">\n};
					$tableau .=qq{$data[$i]</font></td>\n};
					$tarif = $data[$i];
				}
			}
		}
		# Co�t
		if ($mensuel eq "M" || $mensuel eq "F") {
			$date_debut = $data[@data - 6];
			$date_debut =~ s/-/:/g;
			$date_fin =~ s/-/:/g;
			$nbjoursloc = &jours_ouvrables($date_debut, $date_fin, $G_PAYS, $G_AGENCE, $DB);
		}
		else {
			$nbjoursloc = $data[@data - 3];
		}
		$cout = ($nbjoursloc == 0) ? 0 :
			$data[@data - 2] * $taux / $nbjoursmois * ($nbjoursloc + $G_COEFF_COUT) / $nbjoursloc;
		$cout = &fnombre($cout, 3, 2);
		$tableau .=qq{<td>\n};
		$tableau .=qq{<font class="PT">\n};
		$tableau .=qq{<span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$cout</span></font></td>\n};
		# Ratio n�2
		$ratio = ($cout == 0) ? 0 : &fnombre($tarif / $cout, 3, 2);
		$tableau .=qq{<td>\n};
		$tableau .=qq{<font class="PT">\n};
		$tableau .=qq{$ratio</font></td>\n};
		# Dur�e
		$nbjoursloc = &fnombre($nbjoursloc, 3, 0);
		# Suppression des 0 inutiles dans les d�cimales
		if ($nbjoursloc =~ /\./) {
			while (substr($nbjoursloc, length($nbjoursloc) - 1, 1) eq "0") {
				$nbjoursloc = substr($nbjoursloc, 0, length($nbjoursloc) - 1);
			}
		}
		$tableau .=qq{<td>\n};
		if ($mensuel eq "M" || $mensuel eq "F") {
			$tableau .=qq{<font class="PT" color="red">\n};
		}
		else {
			$tableau .=qq{<font class="PT">\n};
		}
		$tableau .=qq{$nbjoursloc</font></td>\n};
		$tableau .=qq{<td>\n};

		$tableau .=qq{</tr>\n};
		print $tableau;
		$tableau = "";
		$ordre++;
	}
	$sth->finish;

#	#
#	#sous location
#	#
#	$tableau .=qq{<tr>\n};
#	$tableau .=qq{<td colspan=$nbcol>\n};
#	$req[4]=$reqrech[4];
#	$sth=$DB->prepare($req[4]);
#	$sth->execute();
#	my $nbrow=$sth->rows;
#	my @label;
#	@label=@{$sth->{NAME}};
#	#$nombre +=$nbrow;
#	$tableau .=qq{<font><b>$nbrow $mess[3300]</b>\n};
#	#$tableau .=$cgi->image_button(-name=>'date', -alt=>$mess[1150], src=>"$URL_IMG/recalk_off.gif", -border=>0);
#	#$tableau .=$cgi->image_button(-name=>'ville', -alt=>$mess[1151], src=>"$URL_IMG/recalk_off.gif", -border=>0);
#	$tableau .=qq{</font></td></tr>\n};
#	$tableau .=qq{<tr>\n};
#	$tableau .=qq{<td align="left">\n};
#	#$tableau .=qq{<table width="100%" border="0" cellspacing="2" cellpadding="0">\n};
#	my $ordre=0;
#	while (my @data=$sth->fetchrow_array()) {
#		my $c=$ordre % 2;
#		my $color="''";
#		my $euclide = $ordre % 20;
#		if ($euclide == 0) {
#			$tableau .=qq{<tr bgcolor="#C6CDC5">\n};
#			for (my $i=0;$i<$nbcol;$i++) {
#				$tableau .=qq{<th>};
#		#		$tableau .=qq{<center>};
#				$tableau .=qq{<font><b>};
#				if ($i>4)
#					{
#					$tableau .=qq{<a href="$HTTP_PORTAIL/cgi-bin/old/location/tableau.cgi?menu=tableau&type=tableau&ordre=$label[$i]">$libcol[$i]</a>};
#					}
#				else {
#					$tableau .=qq{&nbsp;};
#					}
#				$tableau .=qq{</b></font>};
#		#		$tableau .=qq{</center>};
#				$tableau .=qq{</th>\n};
#			}
#			$tableau .=qq{</tr>\n};
#		}
#		if ($c == 1) {$color=qq{'#D6D6D6'};}
#		$tableau .=qq{<TR bgColor=$color onmouseover="bgColor='#FFCC33'" onmouseout="bgColor=$color">\n};
#		my $tarif = 0;
#		my $mensuel = "";
#		my $date_debut = "";
#		my $date_fin = "";
#		for($i=0;$i<@data-5;$i++) {
#			my $soc="";
#			if ($label[$i] eq "CLSTE") {$soc=$data[$i];
#										if ($soc=~ /\'/) {$soc=~s/\'/\\\'/g};
#										}
##			$data[$i]=substr($data[$i], 0, 13);
#			if ($label[$i] eq "CONTRATDR") {
#				$date_fin = $data[$i];
#				$data[$i]=&format_datetime($data[$i]);
#			}
#			elsif ($label[$i] eq "CONTRATCODE") {
#			    my $contractUrl = &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $data[@data-2]});
#				$data[$i]=qq{<a href="$contractUrl" target="_blank">$data[$i]</a>\n};
#			}
#			elsif ($label[$i] eq "MOPXJOURFR") {
#				$data[$i]=&fnombre($data[$i], 3, 2);
#			}
#			elsif ($label[$i] eq "MOTARIFMSFR") {
#				# Contrat mensuel
#				if ($data[$i] > 0) {
#					$mensuel = "M";
#				}
#				$data[$i] = &fnombre($data[$i] / $nbjoursmois, 3, 2);
#			}
#			elsif ($label[$i] eq "MOFORFAITFR") {
#				# Contrat forfaitaire
#				if ($data[$i] > 0) {
#					$mensuel = "F";
#				}
#				if ($data[@data - 2] == 0) {
#					$data[$i] = "?";
#				}
#				else {
#					$data[$i] = &fnombre($data[$i] / $data[@data - 2], 3, 2);
#				}
#			}
#			elsif ($label[$i] eq "CLSTE") {
#				$data[$i]=qq{<a href="#" onclick="return rs('infoclient', '$HTTP_PORTAIL/cgi-bin/old/location/infoclient.cgi?value=$data[@data-3]', 500, 450)">$data[$i]</a>\n};
#			}
## 			elsif ($label[$i] eq "CLPOTENTIEL") {
## 				$data[$i]=qq{<b>$data[$i]</b>\n};
## 			}
#			if ($label[$i] eq "MOPXJOURFR"
#					|| $label[$i] eq "MOTARIFMSFR"
#					|| $label[$i] eq "MOFORFAITFR") {
#                if ($data[$i] ne &fnombre(0, 3, 2))
#                {
#        			$tableau .=qq{<td>\n};
#    				$tableau .=qq{<font class="PT">\n};
#    				$tableau .=qq{$data[$i]</font></td>\n};
#    				$tarif = $data[$i];
#                }
#			}
#			else
#			{
#    			$tableau .=qq{<td>\n};
#    			$tableau .=qq{<font class="PT">\n};
#    			$tableau .=qq{$data[$i]</font></td>\n};
#            }
#			if ($i == 1) {$tableau .=qq{<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>\n}};
#		}
#		# Co�t
#		if ($mensuel eq "M" || $mensuel eq "F") {
#			$date_debut = $data[@data - 5];
#			$date_debut =~ s/-/:/g;
#			$date_fin =~ s/-/:/g;
#			$nbjoursloc = &jours_ouvrables($date_debut, $date_fin, $G_PAYS, $G_AGENCE, $DB);
#		}
#		else {
#			$nbjoursloc = $data[@data - 2];
#		}
#		$cout = "";
#		$tableau .=qq{<td>\n};
#		$tableau .=qq{<font class="PT">\n};
#		$tableau .=qq{$cout</font></td>\n};
#		# Ratio n�2
#		$ratio = "";
#		$tableau .=qq{<td>\n};
#		$tableau .=qq{<font class="PT">\n};
#		$tableau .=qq{$ratio</font></td>\n};
#		# Dur�e
#		$nbjoursloc = &fnombre($nbjoursloc, 3, 0);
#		# Suppression des 0 inutiles dans les d�cimales
#		if ($nbjoursloc =~ /\./) {
#			while (substr($nbjoursloc, length($nbjoursloc) - 1, 1) eq "0") {
#				$nbjoursloc = substr($nbjoursloc, 0, length($nbjoursloc) - 1);
#			}
#		}
#		$tableau .=qq{<td>\n};
#		if ($mensuel eq "M" || $mensuel eq "F") {
#			$tableau .=qq{<font class="PT" color="red">\n};
#		}
#		else {
#			$tableau .=qq{<font class="PT">\n};
#		}
#		$tableau .=qq{$nbjoursloc</font></td>\n};
#		$tableau .=qq{<td>\n};
#
#		$tableau .=qq{</tr>\n};
#
#		print $tableau;
#		$tableau = "";
#		$ordre++;
#	}
#	$sth->finish;
#
#	$tableau .=qq{</table>\n};
#	$tableau .=qq{</td></tr>\n};
	$tableau .=qq{<tr>\n};
	$tableau .=qq{<td colspan=$nbcol><hr></td></tr>\n};

	#2 ligne
	$tableau .=qq{<tr>\n};
	$tableau .=qq{<td align="center" colspan=$nbcol>\n};
	$req[1]=$reqrech[1];

	# -- Machines en livraison --
	$sth=$DB->prepare($req[1]);
	$sth->execute();
	$nbrow = $sth->rows;
	$nombre += $nbrow;
	$tableau .= qq{<select name=$namerech[1]>\n};
	$tableau .= qq{<option value="">$librech[1]\n};
    while (my @data = $sth->fetchrow_array())
    {
        my $parkNumber = $data[4];
        if ($data[6] ne '')
        {
            my $score = 0;
            # R�cup�ration du score de la machine
            my $machObj = &LOC::Machine::getInfos($G_PAYS, $data[6], undef, {'level' => LOC::Machine::LEVEL_MODEL});
            if ($machObj->{'isControlVgp'})
            {
                $score = $machObj->{'control.score'};
            }
            $parkNumber .= '*' x $score;
        }
        $data[4] = $parkNumber;

        $tableau .= qq{<option value="$data[0]">$data[0], $data[1], $data[2], $data[3], $data[4], $data[5]\n};
    }
	$sth->finish;
	$tableau .= qq{</select><font><b>$nbrow</b></font></td>\n};
	print $tableau;
	$tableau = "";
	$tableau .= qq{</tr>\n};
	$tableau .= qq{<tr>\n};
	$tableau .= qq{<td align="center" colspan=$nbcol>\n};

	$req[2] =  $reqrech[2];
	$req[2] =~ s/\<\%self\>/$mess[3380]/;
	$req[2] =~ s/\<\%selfTsf\>/$mess[3383]/;
	$req[2] =~ s/\<\%std\>/$mess[3381]/;
	$req[2] =~ s/\<\%stdTsf\>/$mess[3382]/;

	# -- Machines en r�cup�ration --
	$sth = $DB->prepare($req[2]);
	$sth->execute();
	$nbrow = $sth->rows;
	$nombre += $nbrow;
	$tableau .= qq{<select name=$namerech[2]>\n};
	$tableau .= qq{<option value="">$librech[2]\n};
    while (my @data = $sth->fetchrow_array())
    {
        my $parkNumber = $data[4];
        if ($data[8] ne '')
        {
            my $score = 0;
            # R�cup�ration du score de la machine
            my $machObj = &LOC::Machine::getInfos($G_PAYS, $data[8], undef, {'level' => LOC::Machine::LEVEL_MODEL});
            if ($machObj->{'isControlVgp'})
            {
                $score = $machObj->{'control.score'};
            }
            $parkNumber .= '*' x $score;
        }
        $data[4] = $parkNumber;

        $tableau .= qq{<option value="$data[0]">$data[0], $data[1], $data[2], $data[3], $data[4], $data[5], $data[6], $data[7]\n};
    }
	$sth->finish;
	$tableau .= qq{</select><font><b>$nbrow</b></font></td>\n};
	print $tableau;
	$tableau = "";
	$tableau .= qq{</tr>\n};
	$tableau .= qq{<tr>\n};
	$tableau .= qq{<td colspan=$nbcol><hr></td></tr>\n};

	#3ligne
	$tableau .=qq{<tr>\n};
	$tableau .=qq{<td align="center" colspan=$nbcol>\n};
	$req[3]=$reqrech[3];

	#machine en verif;
	$req[3]=~ s/num/7/eg;
	$sth=$DB->prepare($req[3]);
	$sth->execute();
	$nbrow=$sth->rows;
	$nombre +=$nbrow;
	$tableau .=qq{<select name="$namerech[3]7">\n};
	$tableau .=qq{<option value="">$librech[3]$mess[1159]\n};
    while (my @data=$sth->fetchrow_array())
    {
        my $parkNumber = $data[4];
        if ($data[6] ne '')
        {
            my $score = 0;
            # R�cup�ration du score de la machine
            my $machObj = &LOC::Machine::getInfos($G_PAYS, $data[6], undef, {'level' => LOC::Machine::LEVEL_MODEL});
            if ($machObj->{'isControlVgp'})
            {
                $score = $machObj->{'control.score'};
            }
            $parkNumber .= '*' x $score;
        }
        $data[4] = $parkNumber;

        $tableau .=qq{<option value="$data[0]">$data[0], $data[1], $data[2], $data[3], $data[4]\n};
    }
	$sth->finish;
	$tableau .=qq{</select><font><b>$nbrow</b></font></td>\n};
	print $tableau;
	$tableau = "";
	$tableau .=qq{</tr>\n};

	#4 ligne
	$tableau .=qq{<tr>\n};
	$tableau .=qq{<td align="center" colspan=$nbcol>\n};
	#machine en revision
	$req[3]=$reqrech[3];
	#machine en verif;
	$req[3]=~ s/num/4/eg;
	$sth=$DB->prepare($req[3]);
	$sth->execute();
	$nbrow=$sth->rows;
	$nombre +=$nbrow;
	$tableau .=qq{<select name="$namerech[3]4">\n};
	$tableau .=qq{<option value="">$librech[3]$mess[1152]\n};
    while (my @data=$sth->fetchrow_array())
    {
        my $parkNumber = $data[4];
        if ($data[6] ne '')
        {
            my $score = 0;
            # R�cup�ration du score de la machine
            my $machObj = &LOC::Machine::getInfos($G_PAYS, $data[6], undef, {'level' => LOC::Machine::LEVEL_MODEL});
            if ($machObj->{'isControlVgp'})
            {
                $score = $machObj->{'control.score'};
            }
            $parkNumber .= '*' x $score;
        }
        $data[4] = $parkNumber;

        $tableau .=qq{<option value="$data[0]">$data[0], $data[1], $data[2], $data[3], $data[4]\n};
    }
	$sth->finish;
	$tableau .=qq{</select><font><b>$nbrow</b></font></td>\n};
	print $tableau;
	$tableau = "";
	$tableau .=qq{</tr><tr>\n};
	$tableau .=qq{<td align="center" colspan=$nbcol>\n};
	#machine en panne
	$req[3]=$reqrech[3];
	$req[3]=~ s/num/6/eg;
	$sth=$DB->prepare($req[3]);
	$sth->execute();
	$nbrow=$sth->rows;
	$nombre +=$nbrow;
	$tableau .=qq{<select name="$namerech[3]7">\n};
	$tableau .=qq{<option value="">$librech[3]$mess[1153]\n};
    while (my @data=$sth->fetchrow_array())
    {
        my $parkNumber = $data[4];
        if ($data[6] ne '')
        {
            my $score = 0;
            # R�cup�ration du score de la machine
            my $machObj = &LOC::Machine::getInfos($G_PAYS, $data[6], undef, {'level' => LOC::Machine::LEVEL_MODEL});
            if ($machObj->{'isControlVgp'})
            {
                $score = $machObj->{'control.score'};
            }
            $parkNumber .= '*' x $score;
        }
        $data[4] = $parkNumber;

        $data[5]=&format_datetime($data[5]);

        $tableau .=qq{<option value="$data[0]">$data[0], $data[1], $data[2], $data[3], $data[4], $data[5]\n};
    }
	$sth->finish;
	$tableau .=qq{</select><font><b>$nbrow</b></font></td>\n};
	print $tableau;
	$tableau = "";
	$tableau .=qq{</tr>\n};
	$tableau .=qq{<tr>\n};
	#5ligne
	$tableau .=qq{<tr>\n};
	$tableau .=qq{<td align="center" colspan=$nbcol>\n};
	#machine en controle
	$req[3]=$reqrech[3];
	$req[3]=~ s/num/5/eg;
	$sth=$DB->prepare($req[3]);
	$sth->execute();
	$nbrow=$sth->rows;
	$nombre +=$nbrow;
	$tableau .=qq{<select name="$namerech[3]5">\n};
	$tableau .=qq{<option value="">$librech[3]$mess[1154]\n};
    while (my @data=$sth->fetchrow_array())
    {
        my $parkNumber = $data[4];
        if ($data[6] ne '')
        {
            my $score = 0;
            # R�cup�ration du score de la machine
            my $machObj = &LOC::Machine::getInfos($G_PAYS, $data[6], undef, {'level' => LOC::Machine::LEVEL_MODEL});
            if ($machObj->{'isControlVgp'})
            {
                $score = $machObj->{'control.score'};
            }
            $parkNumber .= '*' x $score;
        }
        $data[4] = $parkNumber;

        $tableau .=qq{<option value="$data[0]">$data[0], $data[1], $data[2], $data[3], $data[4]\n};
    }
	$sth->finish;
	$tableau .=qq{</select><font><b>$nbrow</b></font></td>\n};
	print $tableau;
	$tableau = "";
	$tableau .=qq{</tr>\n};
	$tableau .=qq{<tr><td colspan=$nbcol><hr></td></tr>\n};
	#6ligne
	$tableau .=qq{<tr>\n};
	$tableau .=qq{<td align="center" colspan=$nbcol>\n};
	#machine en dispo
	$req[3]=$reqrech[3];
	$req[3]=~ s/num/2/eg;
	$sth=$DB->prepare($req[3]);
	$sth->execute();
	$nbrow=$sth->rows;
	$nombre +=$nbrow;
	$tableau .=qq{<select name="$namerech[3]2">\n};
	$tableau .=qq{<option value="">$librech[3]$mess[1155]\n};
    while (my @data=$sth->fetchrow_array())
    {
        my $parkNumber = $data[4];
        if ($data[6] ne '')
        {
            my $score = 0;
            # R�cup�ration du score de la machine
            my $machObj = &LOC::Machine::getInfos($G_PAYS, $data[6], undef, {'level' => LOC::Machine::LEVEL_MODEL});
            if ($machObj->{'isControlVgp'})
            {
                $score = $machObj->{'control.score'};
            }
            $parkNumber .= '*' x $score;
        }
        $data[4] = $parkNumber;

        $tableau .=qq{<option value="$data[0]">$data[0], $data[1], $data[2], $data[3], $data[4]\n};
    }
	$sth->finish;
	$tableau .=qq{</select><font><b>$nbrow</b></font></td>\n};
	print $tableau;
	$tableau = "";
	$tableau .=qq{</tr>\n};
	$tableau .=qq{<tr>\n};
	$tableau .=qq{<td colspan=$nbcol><hr></td></tr>\n};
	#7 ligne
	$tableau .=qq{<tr>};
	$tableau .=qq{<td align="center" colspan=$nbcol><font><b>$mess[1156] : $nombre</b></font></td>\n};
	$tableau .=qq{</tr>};
	#8 ligne
	$tableau .=qq{<tr>\n};
	$tableau .=qq{<td colspan=$nbcol><hr></td></tr>\n};
	$tableau .=qq{<tr>};
	$tableau .=qq{<td colspan=$nbcol><font class="PT">$mess[1157] : </font>\n};
	# liste etat
	my $reqet=qq{select ETLIBELLE, ETCODE FROM ETATTABLE WHERE ETCODE LIKE "MAC%" ORDER BY ETLIBELLE};
	$sth=$DB->prepare($reqet);
	$sth->execute();
	$tableau .=qq{<select name="etatimp">\n};
	while (my @data=$sth->fetchrow_array()) {
		$tableau .=qq{<option value="$data[1]" >$data[0]\n};
	}
	$sth->finish;
	$tableau .=qq{</select>\n};
	$tableau .=$G_LST_IMP;


    $tableau .= '<a href="#" onclick="printList(); return false;"><img src="' . $URL_IMG . '/imprimer.gif" alt="' . $mess[871] . '" id="btn-print" /></a>';
    $tableau .= '<img src="' . $URL_IMG . '/../../img/loading.gif" alt="" id="img-loading" />';
    $tableau .= '<span id="msg-result">' . $mess[874] . '</span>';

	#$tableau .=$cgi->image_button(-name=>'implistmach', -alt=>$mess[1158], src=>"$URL_IMG/imprimer.gif", -border=>0);
	$tableau .=$cgi->hidden('impfich', 'lsma');
	$tableau .=qq{</td>\n};
	$tableau .=qq{</tr>\n};
	$tableau .=qq{</form>\n};
	print $tableau;
}

sub init_var {
	$titre=$mess[3350];
	$nbchamps=4;

	$namerech[0]="louee";
	$namerech[1]="liv";
	$namerech[2]="enrecup";
	$namerech[3]="mach";

	$typerech[0]="listesql";
	$typerech[1]="listesql";
	$typerech[2]="listesql";
	$typerech[3]="listesql";

	$reqrech[0]=qq{SELECT MOMADESIGNATION, FAMAMODELE, FAMAELEVATION,
			FAMAENERGIE,
			CONCAT_WS("",MACHINE.MANOPARC_AUX,MACHINE.MANOPARC) AS MANOPARC,
			CONTRATCODE, CLSTE, CHLIBELLE, VICP, VINOM, CONTRATDR,
			CLPOTENTIEL, MOPXJOURFR, MOTARIFMSFR, MOFORFAITFR, CONTRATDD,
			MACHINE.CONTRATAUTO, CONTRAT.CLAUTO, CONTRAT.CONTRATDUREE,
			MOMACOUTFR,
			CONTRAT.MAAUTO AS `machine.id`
		FROM AUTH.`MACHINE`
		LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=MACHINE.CONTRATAUTO
		LEFT JOIN AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=MACHINE.MOMAAUTO
		LEFT JOIN FAMILLEMACHINE
			ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
		LEFT JOIN CHANTIER ON CHANTIER.CHAUTO=CONTRAT.CHAUTO
		LEFT JOIN VILLECHA ON VILLECHA.VILLEAUTO=CHANTIER.VILLEAUTO
		LEFT JOIN CLIENT ON CLIENT.CLAUTO=CONTRAT.CLAUTO
		LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
		LEFT JOIN CLIENTTARIF
			ON CLIENTTARIF.CLIENTTARIFAUTO=CLIENT.CLIENTTARIFAUTO
		WHERE (LIETCODE="1" OR MACHINE.ETCODE="MAC01")
			AND MACHINE.AGAUTO="agence" AND CONTRATOK="-1"};
    # Machines en livraison
	$reqrech[1]=qq{
SELECT
    MOMADESIGNATION,
    FAMAMODELE,
    FAMAELEVATION,
    FAMAENERGIE,
    CONCAT_WS("",MACHINE.MANOPARC_AUX,MACHINE.MANOPARC) AS MANOPARC,
    CONTRATCODE,
    MACHINE.MAAUTO
FROM AUTH.`MACHINE`
LEFT JOIN AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=MACHINE.MOMAAUTO
LEFT JOIN FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=MACHINE.CONTRATAUTO
WHERE
    (LIETCODE="12" OR MACHINE.ETCODE="MAC12")
    AND MACHINE.AGAUTO="agence"
    AND CONTRATOK="-1"
ORDER BY FAMAMODELE, FAMAELEVATION, FAMAENERGIE};

	# Machines en r�cup�ration
    $reqrech[2] = qq{
SELECT
    MOMADESIGNATION,
    FAMAMODELE,
    FAMAELEVATION,
    FAMAENERGIE,
    CONCAT_WS("",MACHINE.MANOPARC_AUX,MACHINE.MANOPARC) AS MANOPARC,
    CONTRATCODE,
    CONCAT_WS(" ", VICP, VINOM),
    IF(tsp_is_self = 1, IF(tsp_type = 'R', '<%self>', IF(tsp_type = 'TS', '<%selfTsf>', '')), IF(tsp_type = 'R', '<%std>', IF(tsp_type = 'TS', '<%stdTsf>', ''))),
    MACHINE.MAAUTO
FROM AUTH.`MACHINE`
LEFT JOIN AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=MACHINE.MOMAAUTO
LEFT JOIN FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=MACHINE.CONTRATAUTO
LEFT JOIN CHANTIER ON CHANTIER.CHAUTO=CONTRAT.CHAUTO
LEFT JOIN VILLECHA ON VILLECHA.VILLEAUTO=CHANTIER.VILLEAUTO
LEFT JOIN f_transport ON (tsp_rct_id_from = CONTRAT.CONTRATAUTO AND tsp_sta_id <> "TSP03")
WHERE
    (LIETCODE="3" OR MACHINE.ETCODE="MAC03")
    AND MACHINE.AGAUTO="agence"
    AND CONTRATOK="-1"
ORDER BY FAMAMODELE, FAMAELEVATION, FAMAENERGIE};

    $reqrech[3]=qq{SELECT MOMADESIGNATION, FAMAMODELE, FAMAELEVATION, FAMAENERGIE, CONCAT_WS("",MACHINE.MANOPARC_AUX,MACHINE.MANOPARC) AS MANOPARC, MADATEPANNE, MACHINE.MAAUTO FROM AUTH.`MACHINE` LEFT JOIN AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=MACHINE.MOMAAUTO LEFT JOIN FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO WHERE (LIETCODE="num" OR MACHINE.ETCODE="MAC0num") AND MACHINE.AGAUTO="agence" ORDER BY FAMAMODELE, FAMAELEVATION, FAMAENERGIE};
	$reqrech[4]=qq{
        SELECT SOLONOM, SOLODESIGNATION, CONTRATCODE, CLSTE,
            CHLIBELLE, VICP, VINOM, CONTRATDR,
            CLPOTENTIEL, MOPXJOURFR, MOTARIFMSFR, MOFORFAITFR, CONTRATDD,
            CONTRAT.CONTRATAUTO, CONTRAT.CLAUTO, CONTRAT.CONTRATDUREE, NULL
        FROM  CONTRAT
        LEFT JOIN SOLO ON CONTRAT.SOLOAUTO=SOLO.SOLOAUTO
        LEFT JOIN CHANTIER ON CHANTIER.CHAUTO=CONTRAT.CHAUTO
        LEFT JOIN VILLECHA ON VILLECHA.VILLEAUTO=CHANTIER.VILLEAUTO
        LEFT JOIN CLIENT ON CLIENT.CLAUTO=CONTRAT.CLAUTO
        LEFT JOIN CLIENTTARIF ON CLIENTTARIF.CLIENTTARIFAUTO=CLIENT.CLIENTTARIFAUTO
        LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
        WHERE CONTRAT.SOLOAUTO !="0" AND LIETCONTRATAUTO="2" AND CONTRAT.AGAUTO="agence" AND CONTRATOK="-1"};

	$optrech[0]=qq{ORDER BY FAMAMODELE, FAMAENERGIE, FAMAELEVATION, MOMADESIGNATION};
	$optrech[1]=qq{ORDER BY CONTRATDR, VICP, FAMAMODELE, FAMAELEVATION, FAMAENERGIE};
	$optrech[2]=qq{ORDER BY VICP, CONTRATDR, FAMAMODELE, FAMAELEVATION, FAMAENERGIE};

	for (my $i=0;$i<$nbchamps;$i++) {
		$librech[$i]=$mess[$i+3351];
	}
	for (my $i=0;$i<@reqrech;$i++) {
		$reqrech[$i]=~ s/agent/$G_PEAUTO/;
		$reqrech[$i]=~ s/agence/$G_AGENCE/;
	}
	$nbcol=16;
	for (my $i=0;$i<$nbcol;$i++) {
		$libcol[$i]=$mess[$i+3355];
	}
}
