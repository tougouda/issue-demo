#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';

use strict;
use File::Basename;
use CGI;
use autentif;
use securite;
use titanic;
use accesbd;
use varglob;
use message;
use calculdate;
use tracetemps;
use LOC::Characteristic;
use LOC::Globals;
use LOC::Customer;

# Initialisation du chrono
my $timer = &init_chrono();

#connexion
my $DB=&Environnement("LOCATION");

my $cgi= new CGI;
$cgi->autoEscape(undef);

my @mess=&message($G_PAYS);

#type / menu
my $nomfh=$cgi->param('type'); # attendu : 'contrat' ou 'devis'
my $fich=$cgi->param('menu'); # attendu : 'nvclient'

my $sth;
my @data;

$|=1;

my @nameform;
my @libform;
my $reqnvclient;
my $nbemailnvcl;
my @emailnvcl;
my %emailOrigin;
my $titre;
my @aff;
my @valsel;
my $cp=0;
my $nbdonne;

if ($fich eq "nvclient") {
    &init_var
    &enteLight($titre, $titre, $G_PENOM, $G_PEPRENOM);
    my $siret = $cgi->param('siret');
    $siret =~ s/^\s+//;
    $siret =~ s/\s+$//;
    $cgi->param('siret' => $siret);
    if ( $cgi->param('saisienvclt.x') != 0 ) {
        my $nbinsert=0;
        my $lib=$cgi->param("libelle");
        my $villauto=$cgi->param("ville");
        my $tele=$cgi->param("tel");
        my @resnvclient=&client_cmp($lib, $villauto, $tele);
        my $saisie_controle = &saisie_controle();
        if ( $saisie_controle ne 0 ) {
            print qq{<center><font class="GD" color="red"><b>$saisie_controle};
            if( ( $G_PAYS eq "FR" ) && ( $cgi->param('apelettre') eq "" ) && ($cgi->param('apev1lettre') eq "") ) { print " ( ".$mess[7700]." )"; }
            if( $cgi->param("siret") eq "" ) { print " ( ".$mess[7702]." )"; }
            $lib =~ s/^\s+//;
            $lib =~ s/\s+$//;
            if( $lib eq "" ) { print " ( ".$mess[7703]." )"; }
            if( $cgi->param('ville') eq '' ) { print " ( ".$mess[7706]." )"; }
            if( $G_PAYS eq 'FR' && $cgi->param('qualif') eq '' ) { print " ( ".$mess[7717]." )"; }
            if( $cgi->param('createur') eq '' ) { print " ( ".$mess[7719]." )"; }
            if( $cgi->param('agence') eq '' ) { print " ( ".$mess[7720]." )"; }
            print "</font></b></center>\n";
        }
        elsif ( $resnvclient[0] ne "0" ) {
            print qq{<center><font class="GD" color="red"><b>$mess[1800] $resnvclient[0], $resnvclient[1] $mess[1801]</b></center>\n};
        }
        else {
            print qq{<center><font class="GD" color="green"><b>$mess[1800] $lib $mess[1802]</b></center>\n};
            $nbinsert=&traitnvclient;
        }
        print qq{<input type="hidden" name="numnvclt" value="$nbinsert">\n};
        #print "<H1>$saisie_controle</H1>";
    }
    else {
        print qq{<input type="hidden" name="numnvclt" value="0">\n};
    }
    &nvclient;
};

$DB->disconnect;
print qq{</table>\n};

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

# Pied de la page HTML
my $paramType = $cgi->param("type");
my $paramMenu = $cgi->param("menu");
&Pied($delta_time, $paramType, $paramMenu);


sub init_var {
    $titre=$mess[2390];

    if ($G_ENVIR eq 'dev')
    {
        $nbemailnvcl = 1;
        for(my $i=0; $i<$nbemailnvcl; $i++) {
            $emailnvcl[$i] = qq{archive.dev\@acces-industrie.com};
        }
        $emailOrigin{$G_PAYS} = &LOC::Characteristic::getCountryValueByCode('STANVCLIENT', $G_PAYS);
    }
    else
    {
        @emailnvcl = split(',', &LOC::Characteristic::getCountryValueByCode('STANVCLIENT', $G_PAYS));

    }


    $reqnvclient=qq{select VICLNOM, VICLCP, VICLAUTO FROM AUTH.`VICL` WHERE VICLNOM LIKE "ident%" OR VICLCP LIKE "ident%" ORDER BY VICLNOM};

    $nameform[0]="ape";
    $nameform[1]="activ";
    $nameform[2]="siret";
    $nameform[3]="libelle";
    $nameform[4]="adresse";
    $nameform[5]="adresse2";
    $nameform[6]="ville";
    $nameform[7]="tel";
    $nameform[8]="port";
    $nameform[9]="fax";
    $nameform[10]="contact";
    $nameform[11]="comment";
    $nameform[12]="modepaie";
    $nameform[13]="banque";
    $nameform[14]="rib";
    $nameform[15]="tarif";
    $nameform[16]="groupe";
    $nameform[17]="qualif";
    $nameform[18]="email";
    $nameform[19]="createur";
    $nameform[20]="agence";
    $nameform[21]="joindocmach";
    for(my $i=0;$i<@nameform;$i++) {
        $libform[$i]=$mess[$i+7700];
    }
    $reqnvclient=~ s/agent/$G_PEAUTO/;
    $reqnvclient=~ s/agence/$G_AGENCE/;

}

sub nvclient {
    $nomfh = $cgi->param('type');
    $fich  = $cgi->param('menu');
    print $cgi->start_form(
        -method => 'post'
    );
    print qq{<table border="0" style="border-collapse: collapse">\n};
    print $cgi->delete('menu');
    print $cgi->delete('type');
    print $cgi->hidden('menu', $fich);
    print $cgi->hidden('type', $nomfh);
    for(my $i=0;$i<@nameform;$i++) {
        print qq{<tr><td style="white-space: nowrap; padding: 0px"><font class="PT"><b>$libform[$i] : </b></font></td><td style="white-space: nowrap; padding: 0px"};
        print qq{><font class="PT">\n};
        if ($nameform[$i] eq "ville") {
            print qq{<select name="$nameform[$i]" tabindex="-1">\n};
            my $ident;
            if ((defined ($cgi->param($nameform[$i]."rech")))&&(not($cgi->param($nameform[$i]."rech") eq ""))) {
                $ident=$cgi->param($nameform[$i]."rech");
                my $sel=$cgi->param("$nameform[$i]");
                $reqnvclient =~ s/ident/$ident/eg;
                $sth=$DB->prepare("$reqnvclient");
                $sth->execute();
                @data;
                @aff;
                @valsel;
                $cp=0;
                $nbdonne;
                while (@data=$sth->fetchrow_array()) {
                    $aff[$cp]="";
                    $valsel[$cp]=$data[2];
                    for(my $eps=0;$eps<1;$eps++) {
                        $aff[$cp] .=$data[$eps].", ";
                    }
                    $aff[$cp] .=$data[1];
                    $cp++;
                }
                $sth->finish;
                if ($cp==0) {
                    print qq{<option value="">$mess[2]\n};
                }
                else {
                    for(my $eps=0;$eps<$cp;$eps++)
                        {
                        my $selected="";
                        if ($valsel[$eps] eq $sel) {
                            $selected=" selected";
                        };
                        print qq{<option value="$valsel[$eps]"$selected>$aff[$eps]\n};
                    }
                };
            }
            else {
                $ident="\\";
                print qq{<option value="">$mess[2]\n};
                $ident="";
            };
            print qq{</select>\n};
            print qq{</font> <font class="PT">\n};
            print $cgi->textfield(
                -name    => $nameform[$i]."rech",
                -default => $ident,
                -size    => 10
            );
            # Si le champ ville est vide on affiche un message
            if($ident eq "")
            {
                  print qq{<img src="$URL_IMG/fire.gif" width="19" height="18" title="$mess[5]" style="vertical-align: middle">\n};
            }
        }
        elsif ($nameform[$i] eq "rib") {
            print $cgi->textfield(
                -name => "bq",
                -size => 5
            );
            print $cgi->textfield(
                -name => "gct",
                -size => 5
            );
            print $cgi->textfield(
                -name => "cpt",
                -size => 11
            );
            print $cgi->textfield(
                -name => "cle",
                -size => 2
            );
            print qq{ \n};
            print $cgi->image_button(
                -name   => 'nvclients',
                -src    => "$URL_IMG/recalk_off.gif",
                -align  => 'absmiddle',
                -border => 0,
                -alt    => $mess[250]
            );
            if ($cgi->param('bq') ne "" && $cgi->param('gct') ne "" && $cgi->param('cpt') ne "" && $cgi->param('cle') ne "") {
                my $boole = 2;
                my $bq  = $cgi->param('bq');
                my $gct = $cgi->param('gct');
                my $cpt = $cgi->param('cpt');
                my $cle = $cgi->param('cle');
                $boole  = &rib($bq, $gct, $cpt, $cle);

                my $m = qq{<font face="Arial, Helveticta" color="red" size="1">$mess[251] };
                if ($boole == 1) {
                    $m .=qq{$mess[252]\n};
                }
                else {
                    $m .=qq{$mess[253]\n};
                }
                $m .= qq{</font>\n};
                print $m;
            }
        }
        elsif ($nameform[$i] eq "modepaie" && $G_PAYS eq "FR") {
            print qq{<input type="hidden" name="modepaie" id="modepaie" value="61">};
            my $sel=$cgi->param("$nameform[$i]");
            my $req=qq{select MOPAAUTO, MOPALIBELLE FROM MODEPAIE ORDER BY MOPALIBELLE};
            $sth=$DB->prepare("$req");
            $sth->execute();
            @data;
            @aff;
            @valsel;
            $cp=0;
            $nbdonne;
            print qq{<select name=modepaiedis disabled>\n};
            while (my @data=$sth->fetchrow_array()) {
                my $selected="";
                if ($data[0] eq $sel) {
                    $selected=" selected";
                }
                print qq{<option value="$data[0]"$selected>$data[1]\n};
            }
            $sth->finish;
            print qq{</select>};
        }
        elsif ($nameform[$i] eq "modepaie" && $G_PAYS ne "FR") {
            my $sel=$cgi->param("$nameform[$i]");
            my $req=qq{select MOPAAUTO, MOPALIBELLE FROM MODEPAIE ORDER BY MOPALIBELLE};
            $sth=$DB->prepare("$req");
            $sth->execute();
            @data;
            @aff;
            @valsel;
            $cp=0;
            $nbdonne;
            print qq{<select name=modepaie>\n};
            while (my @data=$sth->fetchrow_array()) {
                my $selected="";
                if ($data[0] eq $sel) {
                    $selected=" selected";
                }
                print qq{<option value="$data[0]"$selected>$data[1]\n};
            }
            $sth->finish;
            print qq{</select>};
        }        
        elsif ($nameform[$i] eq "tarif") {
            my $sel=$cgi->param("$nameform[$i]");
            my $req=qq{select CLIENTTARIFAUTO, TARIFCLIENTREF FROM CLIENTTARIF WHERE TARIFCLIENTACTIF = '1' ORDER BY TARIFCLIENTREF DESC};
            $sth=$DB->prepare("$req");
            $sth->execute();
            @data;
            @aff;
            @valsel;
            $cp=0;
            $nbdonne;
            # Ins�rtion d'un hidden : Le select en DISABLED ne renvoi pas la valeur
            $sel=32;
            print qq{<input type="hidden" name="tarif" value="$sel" >\n};
            print qq{<select name=tarif2 DISABLED>\n};
            #print qq{<option value="27">$mess[101]\n};
            while (my @data=$sth->fetchrow_array())
                 {
                    my $selected="";
                    if ($data[0] eq $sel) {
                    $selected=" selected";
                    }
                print qq{<option value="$data[0]"$selected>$data[1]\n};
                }
            $sth->finish;
            print qq{</select>};
        }
        elsif ($nameform[$i] eq "ape" && $G_PAYS eq "FR") {
            # Code APE v2 (4 chiffres, 1 lettre, table APE2)
            my $chiffres = substr($cgi->param("$nameform[$i]"), 0, 4);
            my $lettre   = substr($cgi->param("$nameform[$i]"), 4);
            print $cgi->textfield(
                -name      => "$nameform[$i]chiffres",
                -id        => "$nameform[$i]chiffres",
                -value     => "$chiffres",
                -maxlength => "4",
                -size      => "4",
                -onChange  => "document.getElementById('$nameform[$i]').value=$nameform[$i]chiffres.value+$nameform[$i]lettre.value;submit()"
            ),
            qq{<select name="$nameform[$i]lettre" id="$nameform[$i]lettre" onchange="document.getElementById('$nameform[$i]').value = document.getElementById('$nameform[$i]chiffres').value + document.getElementById('$nameform[$i]lettre').value;submit()">\n};

            my $reqape = qq{
                SELECT RIGHT(APE2.CODEAPE,1) AS LETTRE, APELIBELLE, METIERAUTO
                FROM APE2
                WHERE LEFT(APE2.CODEAPE, 4) = "$chiffres"
                ORDER BY LETTRE ASC
            };
            $sth=$DB->prepare("$reqape");
            $sth->execute();

            my @lettres;
            my @metier;
            my $libelle = $mess[151];
            my $activ;
            while (my @data = $sth->fetchrow_array()) {
                push(@lettres, $data[0]);
                push(@metier, $data[1]);
                # Libell� par d�faut (le 1er)
                if (@lettres == 1) {
                    $libelle  = $data[1];
                    $activ    = $data[2];   # Activit� si le code APE a chang�
                }
                my $selected = "";
                if ($data[0] eq $lettre) {
                    $selected = " selected";
                    $libelle  = $data[1];
                    $activ    = $data[2];   # Activit� si le code APE a chang�
                }
                print qq{<option value="$data[0]"$selected>$data[0]\n};
            }
            # Lettre par d�faut (la 1re)
            if (!grep($_ eq $lettre, @lettres)) {
                $lettre = $lettres[0];
            }

            if (@lettres == 0)
            {
                $chiffres = "";
                $lettre   = "";
            }

            my $ape = $chiffres.$lettre;   # Nouveau code APE
            my $old_ape = $cgi->param("old_$nameform[$i]");   # Code APE avant modification par l'utilisateur
            my $new_ape = $cgi->param("$nameform[$i]");       # Code APE apr�s modification par l'utilisateur
            my $ape_modifie = ($new_ape ne $old_ape);         # 0 : l'utilisateur n'a pas chang� de code APE, 1 : sinon
            # Si l'utilisateur a chang� de code APE,
            # on pr�selectionne l'activit� par d�faut du code APE
            # sinon on laisse l'activit� soumise
            if ($ape_modifie)
            {
               $cgi->param("activ" => $activ);
            }

            print qq{</select>};

            print qq{<input type="hidden" name="$nameform[$i]" id="$nameform[$i]" value="$ape" />\n};

            # Code APE avant changement
            print qq{<input type="hidden" name="old_$nameform[$i]" id="old_$nameform[$i]" value="$ape" />\n};

            # Fin code APE v2

            print qq{
<div id="infos" style="position:absolute; left:415px; width:270px" align="left">
    <table cellpadding="0" cellspacing="2" bgcolor="#D6D6D6">};
            for (my $k=0; $k<@metier; $k++) {
                my $bold_debut = "";
                my $bold_fin   = "";
            if ($libelle eq $metier[$k]) {
                $bold_debut = "<b>";
                $bold_fin   = "</b>";
            }
                print qq{
        <tr valign="top"> 
            <td nowrap><font class="PT">$bold_debut&nbsp;$lettres[$k] :$bold_fin</font></td>
            <td><font class="PT">$bold_debut$metier[$k]&nbsp;$bold_fin</font></td>
        </tr>};
            }
            print qq{
    </table>
</div>};
            # Si le champ APE est vide on affiche un message
            if ($G_PAYS eq 'FR' && $cgi->param($nameform[$i]) eq '')
            {
                  print qq{<img src="$URL_IMG/fire.gif" width="19" height="18" title="$mess[5]" style="vertical-align: middle">\n};
            }
        }
        elsif ($nameform[$i] eq "activ") {

            my $sel = $cgi->param("$nameform[$i]");

            my $req = qq{select METIERAUTO, MEACTIVITE  FROM METIER ORDER BY MEACTIVITE};
            $sth = $DB->prepare("$req");
            $sth->execute();
            print qq{<SELECT name=activ>\n};
            print qq{<OPTION value="0">$mess[101]\n};
            while (my @data=$sth->fetchrow_array()) {
                my $selected="";
                if ($data[0] eq $sel) {
                    $selected=" selected";
                }
                print qq{<OPTION value="$data[0]"$selected>$data[1]\n};
            }
            $sth->finish;
            print qq{</SELECT>\n};
        }
        elsif ($nameform[$i] eq 'libelle')
        {
            print $cgi->textfield(
                -name    => "$nameform[$i]",
                -onblur => "this.value = this.value.toUpperCase();"
            );
        }
        elsif ($nameform[$i] eq "groupe") {
            my $sel = $cgi->param("$nameform[$i]");

            my $req = qq{
                SELECT G.CLGROUPEAUTO, G.CLGRLIBELLE, CLIENTTARIFAUTO
                FROM $GESCOM_BASE{"$G_PAYS"}.GC_CLGROUPE G
                ORDER BY G.CLGRLIBELLE};
            $sth = $DB->prepare("$req");
            $sth->execute();
            #selection du groupe d�sactiv�e
            print qq{<SELECT name="groupe" DISABLED onChange="document.getElementById('gbgroupe').value=this.options[this.selectedIndex].getAttribute('grplabel');">\n};
            print qq{<OPTION value="0">$mess[2370]\n};
            while (my @data = $sth->fetchrow_array()) {
                my $selected = "";
                if ($data[0] eq $sel) {
                    $selected = " selected";
                }
                print qq{<OPTION value="$data[0]" grplabel="$data[2]" $selected >$data[1]\n}; #onClick="alert('$data[2]'); document.getElementById('gbgroupe').value=this.value;"
            }
            $sth->finish;
            my $bgval = $cgi->param("gbgroupe");
            print qq{</SELECT>\n <br /><font style="color:red;font-weight:bold">$mess[12]</font><input type="hidden" name="gbgroupe" id="gbgroupe" value="$bgval">\n};
            # hidden pour la valeur du groupe (� 0)
            print qq{<input type="hidden" name="groupe" value="0" >\n};
        }
        elsif ($nameform[$i] eq 'createur')
        {
            # Valeur s�lectionn�e
            my $value = $cgi->param("$nameform[$i]");

            # R�cup�ration de la liste en base
            my $query = 'SELECT usr_id, usr_name, usr_firstname ' .
                        'FROM frmwrk.f_user ' .
                        'LEFT JOIN frmwrk.p_agency ON usr_agc_id = agc_id ' .
                        'WHERE usr_sta_id = "GEN01" ' .
                        'AND agc_cty_id = "' . $G_PAYS . '" ' .
                        'ORDER BY usr_name';
            $sth = $DB->prepare($query);
            $sth->execute();

            # Affichage du select
            print "<select name=\"$nameform[$i]\">\n";
            print "<option value=\"\">---</option>\n";
            while (my @tabData = $sth->fetchrow_array())
            {
                my $selected = ($tabData[0] eq $value ? ' selected' : '');
                print "<option value=\"$tabData[0]\" $selected >$tabData[1] $tabData[2]</option>\n";
            }
            $sth->finish;
            print "</select>\n";

            # Si le champ est vide on affiche une image
            if ($value eq '')
            {
                  print qq{<img src="$URL_IMG/fire.gif" width="19" height="18" title="$mess[5]" style="vertical-align: middle">\n};
            }
        }
        elsif ($nameform[$i] eq 'agence')
        {
            # Valeur s�lectionn�e
            my $value = $cgi->param("$nameform[$i]");
            if ($value eq '')
            {
                $value = $G_AGENCE;
            }

            # R�cup�ration de la liste en base
            my $query = 'SELECT agc_id, agc_label ' .
                        'FROM frmwrk.p_agency ' .
                        'WHERE agc_sta_id = "GEN01" ' .
                        'AND agc_cty_id = "' . $G_PAYS . '" ' .
                        'AND agc_type = "AGC" ' .
                        'ORDER BY agc_label';
            $sth = $DB->prepare($query);
            $sth->execute();

            # Affichage du select
            print "<select name=\"$nameform[$i]\">\n";
            print "<option value=\"PN\">$mess[7800]</option>\n";
            while (my @tabData = $sth->fetchrow_array())
            {
                my $selected = ($tabData[0] eq $value ? ' selected' : '');
                print "<option value=\"$tabData[0]\" $selected >$tabData[2] $tabData[1]</option>\n";
            }
            $sth->finish;
            print "</select>\n";

            # Si le champ est vide on affiche une image
            if ($value eq '')
            {
                  print qq{<img src="$URL_IMG/fire.gif" width="19" height="18" title="$mess[5]" style="vertical-align: middle">\n};
            }
        }
        elsif ($nameform[$i] eq 'joindocmach')
        {
            print $cgi->checkbox( -name=>'joindocmach'.$data[0], -checked=>0, -value=>'-1', -label=>'' );
        }

        else
        {
            print $cgi->textfield(
                -name    => "$nameform[$i]"
            );
            # Si l'un des champs libell�, SIRET ou qualification (France seulement) est vide on affiche un message
            if (($nameform[$i] eq 'libelle' || $nameform[$i] eq 'siret' || $G_PAYS eq 'FR' && $nameform[$i] eq 'qualif')
                && $cgi->param($nameform[$i]) eq '')
            {
                  print qq{<img src="$URL_IMG/fire.gif" width="19" height="18" title="$mess[5]" style="vertical-align: middle">\n};
            }
        }
        print qq{</font></td></tr>\n};
    }
    print qq{<tr><td><font class="PT">};
    print qq{&nbsp;};
    print qq{</font></td><td><font class="PT">\n};
    print $cgi->image_button(
        -name   => 'saisienvclt',
        -src    => "$URL_IMG/newclient_off.gif",
        -border => 0,
        -alt    => $mess[254],
        -onclick => "this.style.display = 'none'"
    );
    print qq{</font></td>\n};
    print qq{</tr>\n};
    my $param=$cgi->param($nameform[3]);
    $cgi->delete('clientsrech');
    print $cgi->hidden('clientsrech', $param);
    print qq{\n</form>\n};
}

sub traitnvclient {
    my @val;
    my $i;

    my $ape = $cgi->param('ape');
    # Code NAF r�v. 1 si le code NAF r�v. 2 n'est pas saisi
    if ($ape eq "")
    {
       $ape = $cgi->param('apev1');
    }
    my $baseTariffId = ($cgi->param('groupe') ne 0 ? $cgi->param('gbgroupe') : $cgi->param('tarif'));

    my $siren = $cgi->param('siret');

    # G�n�ration automatique d'un SIRET fictif pour les particuliers
    if( $cgi->param('siret') =~ /^PA/i ){
        my $requete = "SELECT MAX(CLSIREN) FROM CLIENT C WHERE CLSIREN LIKE 'PA0 000 000%'";
        $sth=$DB->prepare("$requete");
        $sth->execute();
        my @siret = $sth->fetchrow_array;
        $siren = "PA0 000 000 " . sprintf("%05d", (substr($siret[0], -5) + 1));
    }

    # G�n�ration automatique d'un SIRET fictif pour les �trangers
    if( $cgi->param('siret') =~ /^ET/i ){
        my $requete = "SELECT MAX(CLSIREN) FROM CLIENT C WHERE CLSIREN LIKE 'ET0 000 000%'";
        $sth=$DB->prepare("$requete");
        $sth->execute();
        my @siret = $sth->fetchrow_array;
        $siren = "ET0 000 000 " . sprintf("%05d", (substr($siret[0], -5) + 1));
    }
    $cgi->param('siret' => $siren);

    my $label      = $cgi->param('libelle');
    my $address    = $cgi->param('adresse');
    my $address2   = $cgi->param('adresse2');
    my $localityId = $cgi->param('ville');
    my $telephone  = $cgi->param('tel');
    my $mobile     = $cgi->param('port');
    my $fax        = $cgi->param('fax');
    my $email      = $cgi->param('email');
    my $contact    = $cgi->param('contact');
    my $comment    = $cgi->param('comment');
    my $statement  = $cgi->param('bq') . $cgi->param('gct') . $cgi->param('cpt') . $cgi->param('cle');
    my $paymentModeId = $cgi->param('modepaie');
    my $bank       = $cgi->param('banque');
    my $activityId = $cgi->param('activ');
    my $rentTariffGroupId = $cgi->param('groupe');
    my $definition = $cgi->param('qualif');
    my $creatorId  = $cgi->param('createur');
    my $agencyId   = $cgi->param('agence');
    my $isJoinDoc  = ($cgi->param('joindocmach') ? 0 : -1);

    # Insertion
    my $countryId  = $G_PAYS;
    my $userId     = $G_PEAUTO;
    my $tabData    = {
        'statCode'           => &LOC::Util::trim($siren),
        'label'              => &LOC::Util::latin1ToUtf8(uc(&LOC::Util::trim($label))),
        'address'            => &LOC::Util::latin1ToUtf8(&LOC::Util::trim($address)),
        'address2'           => &LOC::Util::latin1ToUtf8(&LOC::Util::trim($address2)),
        'locality.id'        => $localityId,
        'telephone'          => &LOC::Util::trim($telephone),
        'mobile'             => &LOC::Util::trim($mobile),
        'fax'                => &LOC::Util::trim($fax),
        'email'              => &LOC::Util::latin1ToUtf8(&LOC::Util::trim($email)),
        'contactName'        => &LOC::Util::latin1ToUtf8(&LOC::Util::trim($contact)),
        'comment'            => &LOC::Util::latin1ToUtf8(&LOC::Util::trim($comment)),
        'verificationStatus' => ($nomfh eq 'devis' ? 1 : -1),
        'statement'          => $statement,
        'paymentMode.id'     => $paymentModeId,
        'activity.statCode'  => $ape,
        'bank'               => &LOC::Util::latin1ToUtf8(&LOC::Util::trim($bank)),
        'activity.id'        => $activityId,
        'baseTariff.id'      => $baseTariffId,
        'rentTariffGroup.id' => $rentTariffGroupId,
        'class'              => 'P',
        'definition'         => &LOC::Util::latin1ToUtf8(&LOC::Util::trim($definition)),
        'creator.id'         => $creatorId,
        'creationAgency.id'  => $agencyId,
        'isJoinDoc'          => $isJoinDoc
    };

    my $customerId = &LOC::Customer::insert($countryId, $tabData, $userId, {'caller' => basename($0)}, undef, undef);

    return $customerId;
}

sub client_cmp {
    my $st;
    my $libelle=$cgi->param("libelle");
    $libelle =~ s/"/\\"/g;
    my $ville=$cgi->param("ville");
    my $tel=$cgi->param("tel");
    $tel =~ s/"/\\"/g;
    if ($ville) {
        if ($tel ne "") {
            $st = $DB->prepare(qq|select CLSTE, CLCODE from CLIENT where VICLAUTO = "$ville" and CLSTE like "$libelle%" and CLTEL = "$tel"|);
        }
        else {
            $st = $DB->prepare(qq|select CLSTE, CLCODE from CLIENT where VICLAUTO = "$ville" and CLSTE like "$libelle%"|);
        }
    }
    else {
        if ($tel ne "") {
            $st = $DB->prepare(qq|select CLSTE, CLCODE from CLIENT where CLSTE like "$libelle%" and CLTEL = "$tel"|);
        }
        else {
            $st = $DB->prepare(qq|select CLSTE, CLCODE from CLIENT where CLSTE like "$libelle%"|);
        }
    }
    $st->execute();
    @data;
    @aff;
    @valsel;
    $cp=0;
    $nbdonne;
    my $rows = $st->rows;
    my @res = $st->fetchrow_array;
    $st->finish;
    if ($rows) {return @res;} else {return 0;}
}

sub saisie_controle{
    my $st;
    my $siret = uc( $cgi->param("siret") );
    my $ville = $cgi->param("ville");
    my $qualif = $cgi->param("qualif");
    my $createur = $cgi->param("createur");
    my $agence = $cgi->param("agence");

    my $controle = 0;

    # V�rification du SIRET
    $st = $DB->prepare("SELECT CLCODE, CLSTE, CLSIREN FROM CLIENT WHERE CLSIREN = '$siret' AND CLSIRETCPL = ''");
    $st->execute();
    my $rows = $st->rows;

    if ( ( $G_PAYS eq "FR" ) && ( length($siret) eq 17 ) && ( $siret =~ /[0-9]{3}\s[0-9]{3}\s[0-9]{3}\s[0-9]{5}/ ) && ( $ville ne "" ) ) {
        $controle = 0;
    } elsif( $G_PAYS eq "FR" ) {
        $controle = $mess[1803];
    }
    if ( (( $G_PAYS eq "PT" ) && ( length($siret) > 17 )) || ( $ville eq "" ) ) {
        $controle = $mess[1803];
    }
    if ( (( $G_PAYS eq "ES" ) && (length($siret) > 17 || length($siret) < 9)) || ( $ville eq "" ) ) {
        $controle = $mess[1803];
    }
    if ( (( $G_PAYS eq "IT" ) && ( length($siret) == 0)) || ( $ville eq "" ) ) {
        $controle = $mess[1803];
    }
    if ( (( $G_PAYS eq "MA" ) && ( length($siret) == 0)) || ( $ville eq "" ) ) {
        $controle = $mess[1803];
    }
    if ( ( $siret =~ /^PA/i || $siret =~ /^ET/i ) && ( $ville ne "" ) ) {
        $controle = 0;
    }
    # Siret + compl�ment d�j� utilis�
    if ( ($rows ne 0) ){
       my @clients;
       while (my @data = $st->fetchrow_array)
       {
            push(@clients, $data[1]);
       }
       $controle = sprintf($mess[1804], join(', ', @clients));
    }

    # V�rification de l'APE
    if ( ( $G_PAYS eq "FR" ) && ($cgi->param('apelettre') eq "") && ($cgi->param('apev1lettre') eq "") ){
    $controle = $mess[1803];
    }

    # V�rification du libell�
    my $libelle = uc( $cgi->param("libelle") );
    $libelle =~ s/^\s+//;
    $libelle =~ s/\s+$//;
    if ($libelle eq "")
    {
        $controle = $mess[1803];
    }

    # V�rification de la qualification
    if ($G_PAYS eq 'FR' && $qualif eq '')
    {
        $controle = $mess[1803];
    }

    #format de l'email
    my $email = uc( $cgi->param("email") );
    if ($email ne "")
    {
        if($email !~ m/[\w\-]+@[\w\-]+\.[\w\-]+/)
        {
            $controle = $mess[1803];
        }
    }

    # V�rification du cr�ateur
    if ($createur eq '')
    {
        $controle = $mess[1803];
    }

    # V�rification de l'agence de cr�ation
    if ($agence eq '')
    {
        $controle = $mess[1803];
    }

    $st->finish;

    return $controle;
}

sub format_champ {
    my @param=@_;

    my $i;
    for($i=0;$i<@param;$i++)
        {
        if ($param[$i]  =~ /\\/) { $param[$i]  =~ s/\\/\\\\/g;}
        if ($param[$i]  =~ /\'/) { $param[$i]  =~ s/\'/\'/g;}
        if ($param[$i]  =~ /\"/) { $param[$i]  =~ s/\"//g;} #"

        }
    return @param;
}
