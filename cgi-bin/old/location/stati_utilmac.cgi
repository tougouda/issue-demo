#!/usr/bin/perl

use lib '../lib';

use CGI;
use autentif;
use calculdate;
use accesbd;
use varglob;
use Date::Calc qw(Delta_Days Days_in_Month Add_Delta_Days Add_Delta_YMD);
use message;
use tracetemps;
use Data::Dumper;
use mymath;

# Initialisation du chrono
my $timer = &init_chrono();

#*******************************************************************************
#initialisation des variables
#*******************************************************************************
# Connexion
my $DB = &Environnement("STATS");

# Initialisation CGI
my $cgi = new CGI;
$cgi->autoEscape(undef);
my @mess = &message($G_PAYS);
$|=1;

my $tmp; # variable temporaire poubelle
my $titre="Utilisation machines" ; # titre de page

my ($tmp,$tmp,$tmp,$jour,$mois,$an,$tmp,$tmp,$tmp) = localtime; # recup date du jour
#$jour=30;
#$mois=7;
$an +=1900; #ann�e au format 19XX
$mois +=1; # mois en 1->12 au lieu de 0->11
my ($an2,$mois2,$jour2) = Add_Delta_Days(($an,$mois,$jour),-1);
# my @lstAgences = &liste_agences; # liste des agences
# my @lstRegions = &liste_regions; # liste des r�gions

my $fatarSel = $cgi->param('fatarSel'); # famille tarifaire s�lectionn�e
my $facomSel = $cgi->param('facomSel'); # famille commerciale s�lectionn�e
my $momaSel  = $cgi->param('momaSel');  # modele de machine s�lectionn�
my $agRegSel = $cgi->param('agRegSel');  # Affichage par agence ou par r�gion (0:agence ; 1:r�gion)

my @lstZones = ($agRegSel == 0) ? &liste_agences : &liste_regions; # liste des agences ou des r�gions (selon option s�lectionn�e) 

#*******************************************************************************
# debut de page
#*******************************************************************************
# en tete de page HTML
&ente($titre, $titre, $G_PENOM, $G_PEPRENOM);

# div d'attente
print qq{<div id="attente" style="visibility: hidden; position:absolute; width:400px; height:200px;">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr valign="middle">
		<td align="center">
		<table cellpadding="1" cellspacing="0" border="0">
			<tr>
				<td bgcolor=black>
				<table border="0" width="100%" cellpadding="5" cellspacing="0">
					<tr valign="middle">
						<td bgcolor=\"#FFFFCC\" align="center">
						<font class=\"PT2\">
							Veuillez patienter pendant le chargement des statistiques...<br>
							Merci de ne pas cliquer sur le bouton <i>Actualiser</i> et de ne pas appuyer sur F5.<p>
							<img src="$URL_IMG/sablier.gif" border="0">
						</font>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</div>

<script>
var x = (document.body.clientWidth - 400) / 2;
var y = (document.body.clientHeight - 200) / 2;
attente.style.left = x + "px";
attente.style.top = y + "px";
attente.style.visibility = 'visible';
attente.style.cursor = 'wait';
</script>};

#*******************************************************************************
#corps de page
#*******************************************************************************

#FORM pour la s�lection
print "<FORM><FIELDSET><LEGEND>S�lection</LEGEND>";
print &MenuDeroulantFamilleTarifaire;
print "&nbsp&nbsp";
print &MenuDeroulantFamilleCommerciale;   
print "<br>";
print &MenuDeroulantModeleMachine;
print "&nbsp&nbsp";
print &RadioBoutonAgenceRegion;
print "</FIELDSET></FORM>";

my @tabMois = ("Janvier","F�vrier","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","D�cembre");

if (($momaSel ne "" || $facomSel ne "" || $fatarSel ne "") && $agRegSel ne "")
{
    my $labelZone = ($agRegSel == 0) ? "Agence" : "R�gion";
    print qq{
    <table cellpadding="2" cellspacing="2" border=0 align="center">
        <tr bgcolor="#C6CDC5">
            <td rowspan=2 class=PT2>
                <font class="PT"><b>$labelZone</b></font>
            </td>
            <td colspan=4 align=center>
                <font class="PT"><b>};
                print $tabMois[$mois-4];
    print qq{</b></font>
            </td>
            <td colspan=4 align=center>
                <font class="PT"><b>};
                print $tabMois[$mois-3];
    print qq{</b></font>
            </td>
            <td colspan=4 align=center>
                <font class="PT"><b>};
                print $tabMois[$mois-2];
    print qq{</b></font>
            </td>
            <td colspan=4 align=center>
                <font class="PT"><b>};
                print $tabMois[$mois-1];
    print qq{</b>&nbsp;(arr�t� au $jour2/$mois2/$an2)</font>
            </td>
            <td rowspan=2><font class="PT" style="color:blue"><b>TU INST</b></font></td>
            <td rowspan=2><font class="PT" style="color:PURPLE"><b>TU MOY</b></font></td>
        </tr>    
        <tr bgcolor="#C6CDC5">
            <td><font class="PT" style="color:green"><b>NB MAC</b></font></td>
            <td><font class="PT_HL">TX UTIL MOY</font></td>
            <td><font class="PT_HL">PX LOC MOY</font></td>
            <td><font class="PT" style="color:red"><b>RATIO CASH</b></font></td>
            <td><font class="PT" style="color:green"><b>NB MAC</b></font></td>
            <td><font class="PT_HL">TX UTIL MOY</font></td>
            <td><font class="PT_HL">PX LOC MOY</font></td>
            <td><font class="PT" style="color:red"><b>RATIO CASH</b></font></td>
            <td><font class="PT" style="color:green"><b>NB MAC</b></font></td>
            <td><font class="PT_HL">TX UTIL MOY</font></td>
            <td><font class="PT_HL">PX LOC MOY</font></td>
            <td><font class="PT" style="color:red"><b>RATIO CASH</font></b></td>
            <td><font class="PT" style="color:green"><b>NB MAC</b></font></td>
            <td><font class="PT_HL">TX UTIL MOY</font></td>
            <td><font class="PT_HL">PX LOC MOY</font></td>
            <td><font class="PT"style="color:red"><b>RATIO CASH</b></font></td>
        </tr>         
    };
    
    my $type;
    my $val;
    if ($fatarSel ne "")
    {
        $type = "FT";
        $val = $fatarSel;
    }
    if ($facomSel ne "")
    {
        $type = "FC";
        $val = $facomSel;
    }
    if($momaSel)
    {
        $type = "MM";
        $val = $momaSel;
    }
    # pour chaque agence ou r�gion
    my $cpt=0;
    foreach my $zone (@lstZones)
    {
        # Construction de la chaine qui liste les agences � prendre en compte dans les requ�tes
    	my $lstag = '';
        if ($agRegSel == 1) # Liste des agences de la r�gion
    	{
            # Recherche de l'identifiant de la r�gion
            my $req = qq{SELECT are_id FROM frmwrk.p_area WHERE are_code = "$zone"};
            my $st = $DB->prepare($req);
            $st->execute();
            my $idRegion = $st->fetchrow_array(); 
            $st->finish;
            # tableau des agences
            my @lstAgences = &liste_agences($G_PAYS,$idRegion);
            # chaine des agences pr�te � l'emploi pour la clause WHERE
            $lstag = join("', '", @lstAgences);
    	}
    	else   # uniquement l'agence en cours
    	{
            $lstag = $zone;
        }

        my $color = "''";

		if (($cpt % 2) == 0) {$color = qq{'#D6D6D6'};}
        my $chaine ="";
        $chaine .= qq{
            <tr bgColor=$color onmouseover="bgColor='#FFCC33'" onmouseout="bgColor=$color">
                <td>
                    <font class="PT">
                        <b>$zone</b>
                    </font>
                </td>
        };
        my $j = 0;

        my $i=0;
        if ($i > 0){
            $chaine .= qq{<tr bgColor=$color onmouseover="bgColor='#FFCC33'" onmouseout="bgColor=$color">};
        }
        #M-3
        my $tmut = 0;
        my ($at,$mt,$jt) = Add_Delta_YMD($an,$mois,$jour,0,-3,0);       
        my ($nbm,$tum,$plm,$rc) = &calculs($lstag,$type,$val,$mt,$at,"0");
        $chaine .= qq{
                <td style="padding-right:5px" align=center><font class="PT" style="color:green"><b>$nbm</b></font></td>
                <td style="padding-right:5px" align=right><font class="PT">$tum</font></td>
                <td style="padding-right:5px" align=right><font class="PT">$plm</font></td>
                <td style="padding-right:5px" align=right><font class="PT" style="color:red"><b>$rc</b></font></td>
                };
        $tmut  += $tum;
        #M-2
        my ($at,$mt,$jt) = Add_Delta_YMD($an,$mois,$jour,0,-2,0);       
        my ($nbm,$tum,$plm,$rc) = &calculs($lstag,$type,$val,$mt,$at,"0");
        $chaine .= qq{
                <td style="padding-right:5px" align=center><font class="PT" style="color:green"><b>$nbm</b></font></td>
                <td style="padding-right:5px" align=right><font class="PT">$tum</font></td>
                <td style="padding-right:5px" align=right><font class="PT">$plm</font></td>
                <td style="padding-right:5px" align=right><font class="PT" style="color:red"><b>$rc</b></font></td>
                };
        $tmut  += $tum;
        #M-1
        my ($at,$mt,$jt) = Add_Delta_YMD($an,$mois,$jour,0,-1,0);       
        my ($nbm,$tum,$plm,$rc) = &calculs($lstag,$type,$val,$mt,$at,"0");
        $chaine .= qq{
                <td style="padding-right:5px" align=center><font class="PT" style="color:green"><b>$nbm</b></font></td>
                <td style="padding-right:5px" align=right><font class="PT">$tum</font></td>
                <td style="padding-right:5px" align=right><font class="PT">$plm</font></td>
                <td style="padding-right:5px" align=right><font class="PT" style="color:red"><b>$rc</b></font></td>
                };
        $tmut += $tum;
        #Mois en cours
        my ($nbm,$tum,$plm,$rc,$txi) = &calculs($lstag,$type,$val,$mois,$an,"1");
        $chaine .= qq{
                <td style="padding-right:5px" align=center><font class="PT" style="color:green"><b>$nbm</b></font></td>
                <td style="padding-right:5px" align=right><font class="PT">$tum</font></td>
                <td style="padding-right:5px" align=right><font class="PT">$plm</font></td>
                <td style="padding-right:5px" align=right><font class="PT" style="color:red"><b>$rc</b></font></td>
                <td style="padding-right:5px" align=right><font class="PT" style="color:blue"><b>$txi</b></font></td>
                };
        $tmut /= 3;
        $tmut  = &arrondi($tmut,2);
                        
        $chaine .= qq{<td tyle="padding-right:5px" align=right><font class="PT" style="color:purple"><b>$tmut</b><font></td></tr>}; 
        print qq{$chaine};
        $cpt++;        
        
        #chiffre de bas de colonnes
        
    }

    # Calcul des totaux (uniquement pour l'affichage par r�gion sinon il faut revoir le calcul des totaux !!!)
    if ($agRegSel == 1)
    {
        # tableau de toutes les agences
        my @lstAgences = &liste_agences;
        # chaine des agences pr�te � l'emploi pour la clause WHERE
        $lstag = join("', '", @lstAgences);
        #M-3
        my ($at,$mt,$jt) = Add_Delta_YMD($an,$mois,$jour,0,-3,0);       
        my ($nbmm3,$Ftum3,$Fplm3,$Frc3) = &calculs($lstag,$type,$val,$mt,$at,"0");
        print qq{<tr style="">
                <td style=";background-color:orange"><font class="PT"><b>TOTAL</b></font></td>
                <td style="padding-right:5px;background-color:orange" align=center><font class="PT" style="color:green"><b>$nbmm3</b></font></td>
                <td style="padding-right:5px;background-color:orange" align=right><font class="PT" style="color:black"><b>$Ftum3</b></font></td>
                <td style="padding-right:5px;background-color:orange" align=right><font class="PT" style="color:black"><b>$Fplm3</b></font></td>
                <td style="padding-right:5px;background-color:orange" align=right><font class="PT" style="color:red"><b>$Frc3</b></font></td>
                };    
    #            <td style="padding-right:5px" align=right colspan=3><font class="PT">&nbsp;</font></td>
        #M-2
        my ($at,$mt,$jt) = Add_Delta_YMD($an,$mois,$jour,0,-2,0);       
        my ($nbmm2,$Ftum2,$Fplm2,$Frc2) = &calculs($lstag,$type,$val,$mt,$at,"0");
        print qq{
                <td style="padding-right:5px;background-color:orange" align=center><font class="PT" style="color:green"><b>$nbmm2</b></font></td>
                <td style="padding-right:5px;background-color:orange" align=right><font class="PT" style="color:black"><b>$Ftum2</b></font></td>
                <td style="padding-right:5px;background-color:orange" align=right><font class="PT" style="color:black"><b>$Fplm2</b></font></td>
                <td style="padding-right:5px;background-color:orange" align=right><font class="PT" style="color:red"><b>$Frc2</b></font></td>
                };  
        #M-1
        my ($at,$mt,$jt) = Add_Delta_YMD($an,$mois,$jour,0,-1,0);       
        my ($nbmm1,$Ftum1,$Fplm1,$Frc1) = &calculs($lstag,$type,$val,$mt,$at,"0");
        print qq{
                <td style="padding-right:5px;background-color:orange" align=center><font class="PT" style="color:green"><b>$nbmm1</b></font></td>
                <td style="padding-right:5px;background-color:orange" align=right><font class="PT" style="color:black"><b>$Ftum1</b></font></td>
                <td style="padding-right:5px;background-color:orange" align=right><font class="PT" style="color:black"><b>$Fplm1</b></font></td>
                <td style="padding-right:5px;background-color:orange" align=right><font class="PT" style="color:red"><b>$Frc1</b></font></td>
                };  
        #Mois en cours
        my ($nbmm,$Ftum,$Fplm,$Frc,$Fti) = &calculs($lstag,$type,$val,$mois,$an,"1");
        my $Ftu = $Ftum3 + $Ftum2 + $Ftum1 ;
        $Ftu /= 3;
        $Ftu  = &arrondi($Ftu,2);
        print qq{
                <td style="padding-right:5px;background-color:orange" align=center><font class="PT" style="color:green"><b>$nbmm</b></font></td>
                <td style="padding-right:5px;background-color:orange" align=right><font class="PT" style="color:black"><b>$Ftum</b></font></td>
                <td style="padding-right:5px;background-color:orange" align=right><font class="PT" style="color:black"><b>$Fplm</b></font></td>
                <td style="padding-right:5px;background-color:orange" align=right><font class="PT" style="color:red"><b>$Frc</b></font></td>
                <td style="padding-right:5px;background-color:orange" align=right><font class="PT" style="color:blue"><b>$Fti</b></font></td>
                <td style="padding-right:5px;background-color:orange" align=right><font class="PT" style="color:purple"><b>$Ftu</b></font></td>
    
                </tr>
                };      
    }
    # L�gende
    print qq{
        </table><br><font class="PT" style="color:blue"><b>TU INST :</b></font>&nbsp; Taux d'utilisation au moment du chargement de la page.  
        <font class="PT" style="color:purple"><b>TU MOY :</b></font>&nbsp; Taux d'utilisation moyen sur les trois derniers mois (hors mois en cours).  
        <br><font class="PT" style="color:red"><b>ATTENTION :</b></font>&nbsp; Les chiffres du mois pr�c�dent le mois en cours ne sont d�finitifs qu'� partir du lendemain du jour de facturation mensuelle.  
    };
}   
#*******************************************************************************
# fin de page
#*******************************************************************************
#on enleve le div d'attente
print qq{
<script>
attente.style.visibility='hidden';
attente.style.cursor = 'default';
</script>
};

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

# Pied de la page HTML
&Pied($delta_time);

#*******************************************************************************
# Fonctions
#*******************************************************************************
# Retourne 4 dates,
# d�but mois de 2 formats diff�rents
# fin mois de 2 formats diff�rents
# ("2003-05-01","01/05/2003","2003-05-31","31/05/2003")
sub debutFinMois {
#	print qq|<br>debfinmois|;
	my ($mois, $an) = @_;
	my $debutms = "$an-$mois-01";
	my $debaff = "01/$mois/$an";
	my $finmois = Days_in_Month($an, $mois);
	my $finms  = "$an-$mois-$finmois";
	my $finaff = "$finmois/$mois/$an";

	return ($debutms, $debaff, $finms, $finaff);
}
#*******************************************************************************
#Retourne un tableau avec les AGAUTO tri� ASC
#du pays pass� en parametre ('FR','ES','PT'...)
#si pas de parametre, FR est pris par d�faut
#Autre param�tre : are_id (id de la r�gion, pour retourner les agences de la r�gion)
sub liste_agences {
	my @param = @_;
	my $where = "";
	my @agence = ();

	if ($param[0] ne "") {
		$where = qq{WHERE agc_cty_id = "$param[0]"};
	}else {
		$where = qq{WHERE agc_cty_id = "FR"};
	}
	if ($param[1] ne "") 
    {
		$where .= qq{ AND agc_are_id = "$param[1]"};
    }

	my $reqag = qq{select agc_id FROM frmwrk.p_agency $where AND agc_type='AGC' and agc_sta_id='GEN01' ORDER BY agc_id};
    my $stag = $DB->prepare($reqag);
	$stag->execute();

	while (my @data = $stag->fetchrow_array()) {
         	push @agence, $data[0];
         }

	$stag->finish;
	return @agence;
}
#*******************************************************************************
#Retourne un tableau avec les codes r�gion (are_code)
#du pays pass� en parametre ('FR','ES','PT'...)
#si pas de parametre, FR est pris par d�faut
sub liste_regions {
	my @param = @_;
	my $where = "";
	my @region = ();

	if ($param[0] ne "") {
		$where = qq{WHERE are_cty_id = "$param[0]"};
	}else {
		$where = qq{WHERE are_cty_id = "FR"};
	}

	my $reqreg = qq{select are_code FROM frmwrk.p_area $where and are_sta_id='GEN01' ORDER BY are_order};
	my $streg = $DB->prepare($reqreg);
	$streg->execute();

	while (my @data = $streg->fetchrow_array()) {
         	push @region, $data[0];
         }

	$streg->finish;
	return @region;
}

#*******************************************************************************
#Retourne les informations suivantes
# $nbm = nombre de machines � la fin du mois
# $tum = taux d'utilisation moyen
#      = nb jours ouvrables / nb jours ouvr�s
# $plm = prix de location moyen
#      = CA location  / jour lou�s  
# $rc  = ratio cash
#      = CA location / co�t 
# $txi = taux d'utilisation instantan�
# co�t = (co�t mensuel machine x nb jours ouvr�s) / 21 (machines en parc)
# co�t = 0,5 x CA location (machines hors parc) 
# co�t mensuel machine = prix achat x 0.0192

# en entr�e
# $ag    = liste des AGAUTO � traiter (pr�-format�e pour la clause WHERE)
# $type  = 'FC' pour Famille commerciale, 'MM' pour mod�le de machine pour le 
#          code auto correspondant � $val
# $val   = le code auto s�lectionn�
# $mois  = mois
# $annee = ann�e
# $ti Si egal � 1, retourne le taux d'utilisation instantan�.
sub calculs{

    my $sth2;
    my $isRegion = $agRegSel;
    
    my @param = @_;
    
    my $ag    = $param[0];    
	my $type  = $param[1];
	my $val   = $param[2];
	my $mois  = $param[3];
	my $annee = $param[4];
	my $ti    = $param[5];
# 	my $ag    = $param[6];
		
	my $nbm = 0;
	my $tum;
	my $plm;
	my $rc;
	my $txi;
	my $caloc = 0;
	my $nbjrsloc = 0;
	my $coutsloc = 0;
	my $nbjrspresmac = 0;
    my $nbmi = 0;
    my $nbmil = 0;	
	
	my $req;
	if ($type eq 'FT')
	{
    	$req = qq{
            select TXUTILMOY,PXLOCMOY,RATIOCASH,NBMAC,SUM(CALOC), SUM(NBJRSLOC), SUM(COUTSLOC),SUM(NBJRSPRESMAC) from STATSFR.UTILISATIONMACHINE
            where MOIS=$mois and ANNEE=$annee
            and FAMTARAUTO=$val and AGAUTO in ('$ag') group by momaauto, AGAUTO;
    	};
    	$reqi = qq{
        select COUNT(*),ETCODE from AUTH.MACHINE M
        left join AUTH.MODELEMACHINE MM on MM.MOMAAUTO = M.MOMAAUTO
        where MM.FAMTARAUTO=$val and AGAUTO in ('$ag')
         and ETCODE in ("MAC01","MAC02","MAC03","MAC04","MAC05","MAC06","MAC07","MAC09", "MAC12")
        group by ETCODE;
	   };
	}
	elsif ($type eq 'FC')
	{
    	$req = qq{
            select TXUTILMOY,PXLOCMOY,RATIOCASH,NBMAC,SUM(CALOC), SUM(NBJRSLOC), SUM(COUTSLOC),SUM(NBJRSPRESMAC) from STATSFR.UTILISATIONMACHINE
            where MOIS=$mois and ANNEE=$annee
            and FACOMAUTO=$val and AGAUTO in ('$ag') group by momaauto, AGAUTO;
    	};
    	$reqi = qq{
        select COUNT(*),ETCODE from AUTH.MACHINE M
        left join AUTH.MODELEMACHINE MM on MM.MOMAAUTO = M.MOMAAUTO
        left join FAMILLEMACHINE FM on FM.FAMAAUTO = MM.FAMAAUTO
        where FM.FACOMAUTO=$val and AGAUTO in ('$ag')
         and ETCODE in ("MAC01","MAC02","MAC03","MAC04","MAC05","MAC06","MAC07","MAC09", "MAC12")
        group by ETCODE;            
	   };
	}
	elsif ($type eq 'MM' && $agRegSel == 1)
	{
    	$req = qq{
            select TXUTILMOY,PXLOCMOY,RATIOCASH,NBMAC,SUM(CALOC), SUM(NBJRSLOC), SUM(COUTSLOC),SUM(NBJRSPRESMAC) from STATSFR.UTILISATIONMACHINE
            where MOIS=$mois and ANNEE=$annee
            and MOMAAUTO=$val and AGAUTO in ('$ag') group by momaauto, AGAUTO;
    	};
    	$reqi = qq{
        select COUNT(*),ETCODE from AUTH.MACHINE M
        where MOMAAUTO=$val and AGAUTO in ('$ag')
         and ETCODE in ("MAC01","MAC02","MAC03","MAC04","MAC05","MAC06","MAC07","MAC09", "MAC12")
        group by ETCODE;            
	   };
	}
	else
	{
    	$req = qq{
            select SUM(TXUTILMOY),SUM(PXLOCMOY),SUM(RATIOCASH) from STATSFR.UTILISATIONMACHINE
            where MOIS=$mois and ANNEE=$annee
            and MOMAAUTO=$val and AGAUTO in ('$ag');            
    	};
    	$req2 = qq{
            select NBMAC from STATSFR.UTILISATIONMACHINE
            where MOIS=$mois and ANNEE=$annee
            and MOMAAUTO=$val and AGAUTO in ('$ag');            
    	};	 	   
    	$reqi = qq{
        select COUNT(*),ETCODE from AUTH.MACHINE
        where MOMAAUTO=$val and AGAUTO in ('$ag')
        and ETCODE in ("MAC01","MAC02","MAC03","MAC04","MAC05","MAC06","MAC07","MAC09", "MAC12")
        group by ETCODE;
        };  
	}
	$sth = $DB->prepare("$req");
	$sth->execute();
	if ($type eq 'FC' || $type eq 'FT' || ($type eq 'MM' && $agRegSel == 1))
	{
# 	   print $req."<br>";
       my $i=0;
        while (my @data = $sth->fetchrow_array())
        {
            # TXUTILMOY,PXLOCMOY,RATIOCASH,NBMAC,SUM(CALOC), SUM(NBJRSLOC), SUM(COUTSLOC)
            #$tum += ($data[0] * $data[3]);
        	$caloc += $data[4];
        	$nbjrsloc += $data[5];
        	$coutsloc += $data[6];
        	$nbjrspresmac += $data[7];
            $nbm += $data[3];
            $i++;
        }      

        $tum = ($nbjrsloc / $nbjrspresmac) if ($nbjrspresmac != 0);
        $tum = &arrondi($tum*100,2);
        $plm = ($caloc / $nbjrsloc) if ($nbjrsloc != 0);
        $rc  = ($caloc / $coutsloc) if ($coutsloc != 0);
        $plm = &arrondi($plm,2);
        $rc  = &arrondi($rc,2);
    }
    else
    {
    	$sth2 = $DB->prepare("$req2");
    	$sth2->execute();
    	
    	my @data = $sth->fetchrow_array();
    	my @data2 = $sth2->fetchrow_array();
        $tum = &arrondi($data[0]*100,2);
        if ($data[1] == 0)
        {
            $plm = 0;
        }
        $plm = &arrondi($data[1],2);
        $nbm = "$data2[0]";
        $rc = &arrondi($data[2],2);
        if ($nbm eq "")
        {
            $nbm = 0;
        }    
        $sth2->finish; 
    }
	if ($ti eq "1")
	{
		$sth3 = $DB->prepare("$reqi");
		$sth3->execute();
        while (my @data3 = $sth3->fetchrow_array())
        { 
            $nbmi += $data3[0];
            # pour le calcul du taux d'utilisation, on a besoin du nombre de machines lou�es et en livraison
            if ($data3[1] eq "MAC01" || $data3[1] eq "MAC12")
            {
                $nbmil += $data3[0]; 
            }
        }
        if ($nbmi != 0)
        {
            $txi =($nbmil / $nbmi);
            #print $ti."-".$zone."-".$nbmil."-".$nbmi."<br>";
            $txi = &arrondi($txi*100,2);         
        }
        else
        {
            $txi = 0;
        }
        $txi = &arrondi($txi,2);
        $sth3->finish; 
    }
    $sth->finish;

    if ($ti eq "1")
    {  
      #print "------> $nbmi $tum $plm $rc<br>";
       return ($nbmi,$tum,$plm,$rc,$txi);
	}
	else
	{
	   return ($nbm,$tum,$plm,$rc);	
	}
}
sub MenuDeroulantFamilleTarifaire
{
    my $sel;
    my $tab="<b>Famille tarifaire:</b>&nbsp;&nbsp;<select name=fatarSel onChange=\"submit()\">
    <option value=''>Choisir</option>";
	my $sth;
    my $req;
	$req =qq{
    	select FT.FAMTARAUTO, CONCAT(FAMTARLIBELLE, ' ', FAMTARSOUSLIBELLE),COUNT(*) AS NB
    	from FAMILLETARIFAIRE FT
    	left join AUTH.`MODELEMACHINE` on MODELEMACHINE.FAMTARAUTO=FT.FAMTARAUTO
    	left join AUTH.MACHINE on MACHINE.MOMAAUTO=MODELEMACHINE.MOMAAUTO
    	where MOMAGRP is not NULL
        and ETCODE in ("MAC01","MAC02","MAC03","MAC04","MAC05","MAC06","MAC07","MAC09", "MAC12")
    	group by FT.FAMTARAUTO, MOMAGRP
    	order by MOMAGRP ASC,FAMTARORDRE
    };

	$sth=$DB->prepare($req);
	$sth->execute();
	while (my @data=$sth->fetchrow_array())
    {
        if ($data[0] eq $fatarSel)
        {
            $sel ="selected";
        }
        else
        {
            $sel = "";
        }
        $tab .= "<option value='$data[0]' $sel>$data[1]</option>";
    }
    $tab .= qq{</select>};

	$sth->finish;
	return $tab;
}

sub MenuDeroulantFamilleCommerciale
{
    my $sel;
    my $tab="<b>Famille commerciale:</b>&nbsp;&nbsp;<select name=facomSel onChange=\"submit()\">
    <option value=''>Choisir</option>";
	my $sth;
    my $req;
	$req =qq{
    	select FAMILLECOMMERCIALE.FACOMAUTO, FACOMLIBELLE,COUNT(*) AS NB
    	from FAMILLECOMMERCIALE
    	left join FAMILLEMACHINE on FAMILLEMACHINE.FACOMAUTO=FAMILLECOMMERCIALE.FACOMAUTO
    	left join AUTH.`MODELEMACHINE` on MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO
    	left join AUTH.MACHINE on MACHINE.MOMAAUTO=MODELEMACHINE.MOMAAUTO
    	where MOMAGRP is not NULL
        and ETCODE in ("MAC01","MAC02","MAC03","MAC04","MAC05","MAC06","MAC07","MAC09", "MAC12")
    	group by FAMILLECOMMERCIALE.FACOMAUTO, MOMAGRP
    	order by MOMAGRP ASC,FACOMORDRE
    };
	$sth=$DB->prepare($req);
	$sth->execute();
	while (my @data=$sth->fetchrow_array())
    {           
        if ($data[0] eq $facomSel)
        {
            $sel ="selected";
        }
        else
        {
            $sel = "";
        }
        $tab .= "<option value='$data[0]' $sel>$data[1]</option>";
    }
    $tab .= qq{</select>};

	$sth->finish;
	return $tab;
}
sub MenuDeroulantModeleMachine
{
    my $where;
    if ($facomSel ne "")
    {
        $where ="and FC.FACOMAUTO=$facomSel";
    }
    if ($fatarSel ne "")
    {
        $where .=" and MODELEMACHINE.FAMTARAUTO=$fatarSel";
    }
    my $sel;
    my $tab="<b>Mod�le(s):</b>&nbsp;&nbsp;<select name=momaSel style=\"margin-top:5px;\" onChange=\"submit()\">
    <option value=''>Tous</option>";
	my $sth;
	my $req;
	$req =qq{
	select MOMAAUTO, MOMADESIGNATION,FC.FACOMLIBELLE,FOLIBELLE
	from AUTH.`MODELEMACHINE`
	left join FAMILLEMACHINE on FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
    left join FAMILLECOMMERCIALE FC on FC.FACOMAUTO=FAMILLEMACHINE.FACOMAUTO
    left join FOURNISSEUR FO on FO.FOAUTO = MODELEMACHINE.FOAUTO
	where MOMAGRP is not NULL
    $where
	order by MOMADESIGNATION
    };
	$sth=$DB->prepare($req);
	$sth->execute();
	while (my @data=$sth->fetchrow_array())
    {           
        if ($data[0] eq $momaSel)
        {
            $sel ="selected";
        }
        else
        {
            $sel = "";
        }
        $tab .= "<option value='$data[0]' $sel>$data[1] - $data[2] - $data[3]</option>";
    }
    $tab .= qq{</select>};

	$sth->finish;
	return $tab;
}
#Affichage du radio bouton pour le choix du calcul par agence ou par r�gion
sub RadioBoutonAgenceRegion
{
    my @sel = ();
    $sel[0] = "";
    $sel[1] = "";
    $sel[$agRegSel] = "checked";

    my $tab = "<input type=\"radio\" name=agRegSel value=0 $sel[0] style=\"margin-top:5px;\" onClick=\"submit()\" />&nbsp;&nbsp;<b>Agence</b>&nbsp&nbsp";
    $tab .= "<input type=\"radio\" name=agRegSel value=1 $sel[1] onClick=\"submit()\" />&nbsp;&nbsp;<b>R�gion</b>";
    return $tab;
}
