#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';

use File::Basename;

use CGI;
use autentif;
use calculdate;
use titanic;
use accesbd;
use varglob;
use message;
use tracetemps;
use fonctions;
use mymath;
use Date::Calc qw(Days_in_Month Add_Delta_Days);
use LOC::Globals;
use LOC::Util;
use LOC::Alert;
use LOC::Transport;
use LOC::Transport::TimeSlot;
use LOC::Site;
use LOC::Proforma;
use LOC::Customer;
use LOC::Machine;
use LOC::Request;


# Initialisation du chrono
my $timer = &init_chrono();


# Connexion
my $DB = &Environnement("LOCATION");
my $cgi= new CGI;
$cgi->autoEscape(undef);
my @mess=&message($G_PAYS);

# Titre
my $nomfh = $cgi->param('type');
my $titre;

$|=1;

my @chmprech;
my $nbchamp;
my $nbrech;
my @reqrech;
my @librech;
my @libform;
my $nbform;
my @reqform;
my @typeform;
my @nameform;
my @reqtrait;
my @reqetat;
my @tabErrorTypes = ();

#
# Pr�paration des requ�tes
#
# Validation Nv Client
if ($nomfh eq 'nvclient')
{
    $titre = $mess[2300];

    # Traitement
    $reqtrait[0] = qq{UPDATE CLIENT SET CLCODE="val0",CLCOLLECTIF="val1",CLVERIFIER="0", CLNONVALIDE="val2", MOPAAUTO="val6",CLSAGE="val7" WHERE CLAUTO="val3" };
    $reqtrait[1] = qq{UPDATE CONTRAT SET CONTRATVERIFIER="0", CONTRATINDICE=CONTRATINDICE+1 WHERE CLAUTO="val3"};
    $reqtrait[2] = qq{UPDATE AUTH.STOCKMODELE SET   STMORESERVE= STMORESERVE -1   WHERE AGAUTO="agence" AND MOMAAUTO="val5"};
    $reqtrait[3] = qq{INSERT INTO TRACE  (  ETCODE , LOAUTO, PEAUTO  ) VALUES ( "TRA21" , 6, "agent")};
    $reqtrait[4] = qq{UPDATE DEVIS SET ETCODE="DEV02" WHERE CLAUTO="val3"};
    $reqtrait[5] = ''; # Requ�te supprim�e depuis la FA 12189
    $reqtrait[6] = qq{INSERT INTO TRACE  (  ETCODE , LOAUTO, PEAUTO, CONTRATAUTO  ) VALUES ( "TRA37" , 6, "agent", "valContratauto")};
    $reqtrait[7] = qq{SELECT CONTRATAUTO FROM CONTRAT WHERE CLAUTO="val3"};

    # Requete Formule
    $reqform[0] = qq{SELECT MOPALIBELLE ,MOPAAUTO FROM MODEPAIE ORDER BY MOPALIBELLE};
    $libform[0] = $mess[2313];
    $nameform[0] = "paie";
    $typeform[0] = "listesql";
    $nbform = 1;
    $librech[0] = qq{Nouveau client};
    $reqrech[0] = qq{
SELECT
    CLAUTO,
    CLIENT.AGAUTO,
    CLSTE,
    concat_ws(" ", CLADRESSE, CLADRESSE2) AS CLADRESSE,
    VICLCP,
    VICLNOM,
    CLSIREN,
    CLTEL,
    CLFAX,
    MOPALIBELLE,
    CLRIB
FROM CLIENT
LEFT JOIN AUTH.VICL
ON VICL.VICLAUTO = CLIENT.VICLAUTO
LEFT JOIN MODEPAIE
ON MODEPAIE.MOPAAUTO = CLIENT.MOPAAUTO
WHERE (CLVERIFIER="-1" or CLVERIFIER="1")
ORDER BY CLSTE};
    $nbrech = 1;
    $nbchamp = 11;

    for (my $i = 0; $i < $nbchamp; $i++)
    {
        $chmprech[$i] = $mess[$i + 2301];
    }

}

# Commande Controle
if ($nomfh eq 'comcont')
{
    $titre = $mess[2211];

    # Traitement
    $reqtrait[0] = qq{INSERT INTO COMMANDE ( COMMDATEJOUR ,COMMDATEENTREE,ORGAAUTO,COMMANDEMONTANT,COMMANDEMONTANTEU) VALUES ("val0" , "val1" ,"val2","val4","val4a")};
    $reqtrait[1] = qq{INSERT INTO COMMCONT ( COMMANDEAUTO ,COMAAUTO) VALUES ("ident0" , "val3" )};
    $reqtrait[2] = qq{UPDATE CONTROLEMACHINE  SET ORGAAUTO="val2", COMAOK="0", COMACOMMANDE="val5", COMAINDICE = COMAINDICE + 1 WHERE COMAAUTO="val3"};
    $reqtrait[3] = qq{INSERT INTO TRACE   ( ETCODE , LOAUTO, PEAUTO  ) VALUES ( "TRA23", 3, "agent")};
    $reqtrait[4] = qq{UPDATE CONTROLEMACHINE SET COMACOMMANDE="val7", COMAINDICE = COMAINDICE + 1 WHERE COMAAUTO="val8"};

    # Requete Formule
    $reqform[0] = qq{
SELECT
    ORGALIBELLE,
    ORGATELEPHONE,
    ORGACONTACT,
    ORGAAUTO,
    ORGATFAG1,
    ORGATFAG2,
    ORGATFAG3,
    ORGATFAG4,
    ORGATFCH1,
    ORGATFCH2,
    ORGATFCH3,
    ORGATFCH4,
    PAMONNAIE
FROM ORGANISMECONTROLE
LEFT JOIN AUTH.AGENCE
ON ORGANISMECONTROLE.AGAUTO = AGENCE.AGAUTO
LEFT JOIN AUTH.PAYS
ON AGENCE.PACODE = PAYS.PACODE
WHERE ORGANISMECONTROLE.AGAUTO = "agence"
ORDER BY ORGALIBELLE};
    $libform[0]  = $mess[2210];
    $nameform[0] = "organisme";
    $typeform[0] = "listesql";

    $nbform = 1;

    $librech[0] = $mess[2212];

    $reqrech[0] = qq{
SELECT
    CONTRAT.CONTRATAUTO,
    CONTRATCODE,
    CONTRATINDICE,
    COMACOMMANDE,
    VILLECHA.VICP,
    MOMADESIGNATION,
    MANOPARC,
    ETLIBELLE,
    CONTRATDR,
    COMAAPAVE,
    COMAAUTO,
    MACHINE.ETCODE,
    MACHINE.MAAUTO,
    COMAINDICE
FROM CONTROLEMACHINE
LEFT JOIN AUTH.MACHINE
ON MACHINE.MAAUTO = CONTROLEMACHINE.MAAUTO
LEFT JOIN AUTH.MODELEMACHINE
ON MODELEMACHINE.MOMAAUTO = MACHINE.MOMAAUTO
LEFT JOIN ETATTABLE
ON ETATTABLE.ETCODE = MACHINE.ETCODE
LEFT JOIN CONTRAT
ON CONTRAT.CONTRATAUTO = MACHINE.CONTRATAUTO
LEFT JOIN CHANTIER
ON CHANTIER.CHAUTO = CONTRAT.CHAUTO
LEFT JOIN VILLECHA
ON VILLECHA.VILLEAUTO = CHANTIER.VILLEAUTO
WHERE MACHINE.AGAUTO = "agence"
AND COMAAPAVE<=DATE_ADD(NOW(), INTERVAL 2 MONTH)
AND (MOMACONTROLEVGP = 1 OR MOMACONTROLEVGP LIKE '%"$G_PAYS":1%')
AND LIETCODE NOT IN (8, 18)
AND LIETCODE < 20 <%triage>};
    $nbrech  = 1;
    $nbchamp = 10;

    for (my $i = 0; $i < $nbchamp; $i++)
    {
        $chmprech[$i] = $mess[$i + 2200];
    }

    # Etat
    $reqetat[0] = qq{
SELECT
    COMMANDE.COMMANDEAUTO,
    DATE_FORMAT(COMMDATEJOUR, "%d/%m/%y"),
    truncate(COMMANDEMONTANT,2),
    DATE_FORMAT(COMMDATEENTREE, "%d") AS JR,
    DATE_FORMAT(COMMDATEENTREE, "%m") AS MS,
    DATE_FORMAT(COMMDATEENTREE, "%Y") AS AN,
    PAMONNAIE
FROM COMMCONT
LEFT JOIN CONTROLEMACHINE
ON CONTROLEMACHINE.COMAAUTO = COMMCONT.COMAAUTO
LEFT JOIN COMMANDE
ON COMMANDE.COMMANDEAUTO = COMMCONT.COMMANDEAUTO
LEFT JOIN AUTH.MACHINE
ON CONTROLEMACHINE.MAAUTO = MACHINE.MAAUTO
LEFT JOIN AUTH.AGENCE
ON MACHINE.AGAUTO = AGENCE.AGAUTO
LEFT JOIN AUTH.PAYS
ON AGENCE.PACODE = PAYS.PACODE
WHERE COMMCONT.COMAAUTO = "num" AND COMAOK = "0"
ORDER BY COMMANDE.COMMANDEAUTO
DESC LIMIT 1};
    $reqetat[1] = qq{
SELECT
    COMMDATEENTREE
FROM COMMANDE
WHERE COMMANDE.COMMANDEAUTO = "num"
AND COMMDATEENTREE <= NOW()};
}

my $agence     = $G_AGENCE;
my $agent      = $G_PEAUTO;
my $unitmonaie = $G_MONNAIE;

my $regCtrlAccounts = &LOC::Characteristic::getAgencyValueByCode('OUVCPTE', $agence);

for (my $i = 0; $i < @reqrech; $i++)
{
    $reqrech[$i] =~ s/agent/$G_PEAUTO/eg;
    $reqrech[$i] =~ s/agence/$G_AGENCE/eg;
}
for (my $i = 0; $i < @reqform; $i++)
{
    $reqform[$i] =~ s/agent/$G_PEAUTO/eg;
    $reqform[$i] =~ s/agence/$G_AGENCE/eg;
}
for (my $i = 0; $i < @reqtrait; $i++)
{
    $reqtrait[$i] =~ s/agent/$G_PEAUTO/eg;
    $reqtrait[$i] =~ s/agence/$G_AGENCE/eg;
}
for(my $i = 0; $i < @reqetat; $i++)
{
    $reqetat[$i] =~ s/agent/$G_PEAUTO/eg;
    $reqetat[$i] =~ s/agence/$G_AGENCE/eg;
}

# Entete
&ente($titre, $titre, $G_PENOM, $G_PEPRENOM . ', ' . $G_AGENCE, undef, 0);


if ($nomfh eq "comcont")
{
    print qq{<SCRIPT language="JavaScript" src="$HTTP_PORTAIL/web/js/common.js"></SCRIPT>};
    print qq{<SCRIPT language="JavaScript" src="$HTTP_PORTAIL/web/js/httpRequest.js"></SCRIPT>};

    print '
    <script type="text/javascript">
        var printHttpObj;
        function hideButtons()
        {
            // On masque la barre des boutons
            var rowButtons = document.getElementById(\'rowButtons\');
            if (rowButtons)
            {
                rowButtons.style.visibility = "hidden";
            }
            return true;
        }
        function printControlOrder (file, printer)
        {
            printer = printer.substr(0, 7);
            var url     = "' . &LOC::Request::createRequestUri('print:render:print') . '";

            if (!printHttpObj)
            {
                printHttpObj = new Location.HTTPRequest(url);
                // Codes de retour valide :
                // 204 : pas de contenu (code de retour normal)
                // 1223 : code transform� par IE
                // 0 : code transform� par Google Chrome Frame
                printHttpObj.setValidResponseStatusRegExp("^0|204|1223$");
            }

            printHttpObj.oncompletion = function()
            {
            };
            printHttpObj.onerror = function()
            {
                // Erreur
                alert("Erreur " + printHttpObj.getResponseStatus());
            };
    
            // Passage de l\'id du client en param�tre
            printHttpObj.setVar(\'printer\', printer);
            printHttpObj.setVar(\'url\', file);
            printHttpObj.setVar(\'number\', 1);
    
            // Envoi de la requ�te HTTP
            printHttpObj.send(true);
    
            return true;
        }
    </script>
    ';
}


my $taux_euro = &taux_euro($G_MONNAIE);
print qq{<SCRIPT language="JavaScript" src="$HTTP_PORTAIL/web/js/maskedInput.js"></SCRIPT>};
print qq{<SCRIPT language="JavaScript">function \$(elmt) { return document.getElementById(elmt); }</SCRIPT>};
#
# Programme Principal
#
if ($nomfh eq "nvclient" || $nomfh eq "comcont")
{
    # Commande Controle
    if ($nomfh eq "comcont" && ($cgi->param('btnComControle.x') != 0 || $cgi->param('btnPdf.x') != 0))
    {
        &commandcont();
    }

    # Nouveau Client
    if (($nomfh eq "nvclient")&&($cgi->param('nvclt.x') != 0))
    {
        &validnvclient();
    }

    print qq{<BR>\n};
    print qq{<CENTER>\n};
    if ($nomfh eq "nvclient")
    {
        &nvclient();
     }
     else
     {
        if ($nomfh eq "comcont")
        {
            my $url = $ENV{'SCRIPT_URI'} . '?type=' . $nomfh;
            print '<div style="margin-bottom: 1em"><a href="' . $url . '">' . $mess[7313] . '</a></div>';
        }
        &affichage();
    }
    print qq{</CENTER>\n};
}

print qq{
        </table>
};
$DB->disconnect;

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

# Pied de la page HTML
my $paramType = $cgi->param("type");
my $paramMenu = $cgi->param("menu");
&Pied($delta_time, $paramType, $paramMenu);


#
# Affichage
#
sub affichage
{
    my $tableau;
    my @liste;
    my @liste1;
    my $liste2;
    my @transauto;

    # Variables pour le tri du tableau
    my $triIndex = $cgi->param('tri');
    my $triSens  = $cgi->param('tri_sens');
    my @triage = ();
    my $triQuery = '';

    # Variables pour la pagination du tableau
    my $nombre = '';
    my $limitQuery  = '';

    # Commande de contr�le
    if ($nomfh eq 'comcont')
    {
        # Tri
        @triage = ('CONTRATCODE', 'CONTRATINDICE', undef,
                   'VILLECHA.VICP', 'MOMADESIGNATION', 'MANOPARC', 
                   'ETLIBELLE', 'CONTRATDR','COMAAPAVE');

        $triIndex = (defined $triIndex && defined $triage[$triIndex] ? $triIndex : 8);
        $triSens  = (defined $triSens ? $triSens : 0);
        $triQuery = 'ORDER BY ' . $triage[$triIndex] . ($triSens == 0 ? ' ASC' : ' DESC');


        # Limite
        $nombre = $cgi->param('nombre');
        if ($cgi->param('btnComControle.x') != 0 || $cgi->param('btnPdf.x') != 0)
        {
            $nombre -= 30;
        }
        if ($nombre eq '')
        {
            $nombre = 0;
        }
        $limitQuery = ' LIMIT ' . $nombre . ', 30';


        # Code JS pour l'affichage des informations sur l'organisme
        print qq{
<script language="javascript">
function showDiv(ligne, objet)
{
    for (i=0; i < objet.length - 1; i++)
    {
        document.getElementById('div_' + ligne + '_' + i).style.display = 'none';
    }
    if (document.getElementById('div_' + ligne + '_' + (objet.selectedIndex - 1)))
    {
        document.getElementById('div_' + ligne + '_' + (objet.selectedIndex - 1)).style.display = 'block';
    }
}
</script>};
    }

    my $nbl = 0;
    my $nblE = 0;
    my $i;
    my $j;
    my $entete = '';
    # Titre
    print qq{\n},
        qq{<TABLE width="100%" border="0" cellspacing="2" cellpadding="2">\n},
        $cgi->start_form(-method=>'post', name=>"frm"), $cgi->hidden('type', $nomfh), $cgi->hidden('tri',$triIndex), $cgi->hidden('tri_sens',$triSens);

    #
    # Entete
    #
     $entete .= qq{
         <tr style="background-color: #C6CDC5;">
     };

    for ($i = 1; $i < $nbchamp; $i++)
    {
        my $dehtm = '';
        my $fihtm = '';

        if (defined $triage[$i - 1])
        {
            $dehtm = '<a href="' . $HTTP_PORTAIL . '/cgi-bin/old/location/transp.cgi?type=' . $nomfh . '&tri=' . ($i - 1) . '&tri_sens=' . ($triIndex == ($i - 1) ? (!$triSens) * 1 : $triSens) . '">';
            $fihtm = '</a>';
            if ($triIndex == ($i - 1))
            {
                $dehtm .= '[';
                $fihtm = '&nbsp;' . ($triSens == 0 ? '&uArr;' : '&dArr;') . ']' . $fihtm;
            }
        }

        $entete .= '<td><font class="PT">' . $dehtm . '<b>' . $chmprech[$i] . '</b>' . $fihtm . '</font></td>' . "\n";
    }

    $entete .= qq{<td><font class="PT"><b>$libform[0]</b></font></td>\n};
    if ($nomfh eq "nvclient")
    {
        
        $entete .= qq{<td><font class="PT"><b>$mess[502]</b></font></td>\n};
        $entete .= qq{<td><font class="PT"><b>$mess[503]</b></font></td>\n};
        $entete .= qq{<td><font class="PT"><b>$mess[504]</b></font></td>\n};
    }
    if ($nomfh eq "comcont")
    {
        $entete .= qq{<td><font class="PT"><b>$mess[2250]</b></font></td>\n};
        $entete .= qq{<td><font class="PT"><b>$mess[2251]</b></font></td>\n};
        $entete .= qq{<td><font class="PT"><b>$mess[2252]</b></font></td>\n};

        my $org = $reqform[0];
        my $storg = $DB->prepare($org);
        $storg->execute();

        while (my @data3 = $storg->fetchrow_array())
        {
            $liste[$nbl]   = qq{$data3[0]};
            $liste3[$nbl]  = "";
            $liste3[$nbl] .= $data3[1] . "<BR>"    if ($data3[1]);
            $liste3[$nbl] .= $data3[2] . "<BR>"    if ($data3[2]);

            for(my $mt = 4; $mt < 12; $mt++)
            {
                
                $data3[$mt] = &fnombre($data3[$mt], 3, 2);
            }
            my $monnaie = $data3[@data3 - 1];
            $liste1[$nbl]    = qq{$mess[500] : $data3[4] $monnaie, $data3[5] $monnaie, $data3[6] $monnaie, $data3[7] $monnaie};
            $liste2[$nbl]    = qq{$mess[500] : $data3[8] $monnaie, $data3[9] $monnaie, $data3[10] $monnaie, $data3[11] $monnaie};
            $transauto[$nbl] = qq{$data3[3],};
            $nbl++;
        }
        
         $storg->finish;
    }

    $entete .=qq{</tr>\n};
    print $entete;

    my $ordre=0;
    my $ind=0;
    for ($i = 0; $i < $nbrech; $i++)
    {
        my $sth;

        if ($nomfh eq "comcont")
        {
            $reqrech[$i] =~ s/\<\%triage\>/$triQuery/;
            $reqrech[$i] =~ s/\<\%filtre\>//;
            $reqrech[$i] .= $limitQuery;
        }

        $sth = $DB->prepare($reqrech[$i]);
        $sth->execute();
        my @label;
        @label = @{$sth->{NAME}};


        while (my @data = $sth->fetchrow_array())
        {
            my $l;
            my $nba = @data;
            my @tabErrors = ();

            if ($nomfh eq "comcont")
            {
                
                $nba = @data - 4;
            }


            # Couleur de fond de la ligne
            my $c = $ind % 2;
            my $color = '';
            if (@tabErrors > 0)
            {
                $color = ($c == 1 ? '#dc8480' : '#e5a5a1');
            }
            else
            {
                $color = ($c == 1 ? '#D6D6D6' : 'transparent');
            }

            print qq{<tr style="background-color: $color;" onmouseover="this.style.backgroundColor='#FFCC33';" onmouseout="this.style.backgroundColor='$color';">\n};

            for ($l = 1; $l < $nba; $l++)
            {
                if ($label[$l] eq "CONTRATDR")
                {
                    if ($data[11] ne LOC::Machine::STATE_RENTED)
                    { # Etat lou�e -> on n'affiche pas la date de fin de contrat
                        $data[$l] = qq{&nbsp;};
                    }
                    else
                    {
                        if($data[$l] ge $data[9])
                        { # Date �ch�ance < Date Fin contrat -> On �crit en rouge
                            $data[$l] = '<font color="red">' . &format_datetime($data[$l]) . '</font>';
                        }
                        else
                        {
                            $data[$l] = &format_datetime($data[$l]);
                        }
                    }
                }

                if ($label[$l] eq "COMAAPAVE")
                {
                    my $date = $data[$l];
                    $data[$l] = &format_datetime($date);
                    print '<input type="hidden" name="indicectrl' . $ordre . '" value="' . $data[@data - 1] . '">';
                }

                my $lien = "";
                my $fnlien = "";

                if ($label[$l] eq "COMACOMMANDE")
                {
                    if ($data[3] eq $data[2])
                    { # Si COMACOMMANDE = COMAINDICE pas de case � cocher
                        $data[3] = qq{&nbsp;};
                        $cgi->delete("okk$ordre");
                       print $cgi->hidden("okk$ordre",0);
                    }
                    else
                    {
                       $data[3] = qq{<input type="checkbox" name="modif$ordre" checked>};
                       $cgi->delete("indice$ordre");
                       print $cgi->hidden("indice$ordre",$data[2]);
                       $cgi->delete("okk$ordre");
                       print $cgi->hidden("okk$ordre",1);
                    }
                 }

                my $lienmach = "";
                my $fnlienmach = "";

                if (($label[$l] eq "MANOPARC") && ($nomfh eq "comcont"))
                {
                    my $urlMachine = &LOC::Machine::getUrlByParkNumber($data[$l]);
                      $lienmach = qq{<a href="#" onclick="return fullrs('machinecont', '$urlMachine')">\n};
                      $fnlienmach = qq{</a>\n};
                 }

                # Colonne N� Parc
                if ($label[$l] eq "MANOPARC")
                {
                    my $parkNumber = $data[$l];
                    # Si une machine est attribu�e sur le contrat
                    if ($data[23] ne '')
                    {
                        my $score = 0;
                        # R�cup�ration du score de la machine
                        my $machObj = &LOC::Machine::getInfos($G_PAYS, $data[23], undef, {'level' => LOC::Machine::LEVEL_MODEL});
                        if ($machObj->{'isControlVgp'})
                        {
                            $score = $machObj->{'control.score'};
                                                        print $score;
                        }
                        $parkNumber .= '*' x $score;
                    }
                    $data[$l] = $parkNumber;
                }

                if ($label[$l] eq "CONTRATCODE")
                {
                    my $contractUrl = &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $data[0]});
                      $lien = qq{<a href="$contractUrl" target="_blank"><b>};
                      $fnlien = qq{</b></a>};
                      $cgi->delete("contratcode$ordre");
                      print $cgi->hidden("contratcode$ordre",$data[1]);
                 }
                 print qq{<td><font>$lienmach$lien$data[$l]$fnlien$fnlienmach</font></td>\n};
            }

            # Controle Technique
            if ($nomfh eq "comcont")
            {
                  my ($comm, $jr, $ms, $an, $blct) = &date_ct($data[@data - 4]);
                  $cgi->delete("verif$ordre");
                  print $cgi->hidden("verif$ordre",$data[@data - 4]),
                  qq{
                  <td valign="top">
                <select name="$nameform[0]$ordre" onchange="showDiv($ordre, this)">
                 <option value="">$libform[0]
                 };

                 for ($l = 0; $l < $nbl; $l++)
                 {
                     my $aff = $liste1[$l];
                     if ($data[@data - 3] == 1 || $data[@data - 3] == 3)
                     {
                         $aff = $liste2[$l]
                     };
                     print qq{<option value="$transauto[$l]$data[0],$data[@data-4],$data[@data-3]">$liste[$l]\n};
                 }

                 print qq{
                 </select>
                <br>};

                 for ($l = 0; $l < $nbl; $l++)
                 {
                        my $aff = $liste1[$l];
                     if ($data[@data - 3] == 1 || $data[@data - 3] == 3)
                     {
                        $aff = $liste2[$l];
                    }
                    print qq{<DIV id="div_} . $ordre . "_" . $l . qq{" style="display: none;">$liste3[$l]$aff</DIV>\n};
                }

                print qq{
                </td>
                 <td nowrap><font class="PT">
                 };

                $cgi->delete("montant$ordre");

                print $cgi->textfield(
                    -name      => "montant$ordre",
                    -size      => 5,
                    -maxlength => 15
                );

                $cgi->delete("contrat$ordre");

                print $cgi->hidden("contrat$ordre",$data[@data - 4]);

                $cgi->delete("cont$ordre");

                 print $cgi->checkbox(
                    -name  => "cont$ordre",
                    -label => '',
                    -value => $data[@data - 2]
                );

                 $cgi->delete("contrat$ordre");

                print $cgi->hidden("contrat$ordre", $data[0]);
                 print qq{</font></td>\n};

                 my $cl = "";

                 if ($blct != 0)
                 {
                     $cl = qq{ bgcolor="red"};
                 }

                 print qq{
                 <td$cl><font class="PT"><b>$comm</b></font></td>
                 <td$cl nowrap>
                 };
                 $cgi->delete("transjr$ordre");
                 print $cgi->textfield(
                    -name      => "transjr$ordre",
                    -value     => $jr,
                    -size      => 1,
                    -onChange  => "taille(this.value,2);nombremyst(this.value)",
                    -maxlength => 2
                );
                print qq{ /\n};
                $cgi->delete("transms$ordre");
                print $cgi->textfield(
                    -name      => "transms$ordre",
                    -value     => $ms,
                    -size      => 1,
                    -onChange  => "taille(this.value,2);nombremyst(this.value)",
                    -maxlength => 2
                );
                print qq{ /\n};
                $cgi->delete("transan$ordre");
                print qq{<select name="transan$ordre">\n};

                $an = (localtime)[5] + 1900   if (!$an);
                  for (0..4)
                  {
                      my $selectedan = "";
                       my $ann = substr("0000" . ((localtime)[5] + 1897 + $_), -4);

                       if ($ann eq $an)
                       {
                            $selectedan =" selected";
                        }

                     print qq{<option value="$ann"$selectedan>$ann\n};
                 }

                print qq{</select>\n};
                 print qq{</td>\n};
              }

            # Nouveau Client
            if ($nomfh eq "nvclient")
            {
                 my $rqpaie = $reqform[0];
                 my $stpaie = $DB->prepare($rqpaie);
                 my $selpaie = $data[9];
                 $stpaie->execute();
                 print qq{<td><font class="PT">\n};
                 print qq{<select name="$nameform[0]$ordre">\n};

                 while (my @data3 = $stpaie->fetchrow_array())
                 {
                     my $affpaie = $data3[0];
                     my $valpaie = $data3[1];
                     my $selectpaie = "";

                     if ($affpaie eq $selpaie)
                     {
                          $selectpaie = qq{ selected};
                     };

                     print qq{<option value="$valpaie"$selectpaie>$affpaie\n};
                 }

                 $stpaie->finish;
                print qq{</select></font></td>\n};
                print qq{<td><font class="PT">\n};
                 $cgi->delete("collectif$ordre");
                 print $cgi->textfield(-name=>"collectif$ordre",size=>6);
                 print qq{</font></td>\n};
                 print qq{<td><font class="PT">\n};
                 $cgi->delete("codect$ordre");
                 print $cgi->textfield(-name=>"codect$ordre",size=>6);
                 print qq{</font></td>\n};
                 print qq{<td><font class="PT">\n};
                 $cgi->delete("effect$ordre");
                 print $cgi->checkbox(-name=>"effect$ordre" , -label=>'');
                 print qq{</font></td>\n};
                print $cgi->hidden("client$ordre",$data[0]);
             }

             print qq{</tr></font>\n};

             $ind++;
             $ordre++;
        }

        $sth->finish();
    }

    my $imprim = $G_LST_IMP;
    print qq{</TABLE>\n};

    print qq{<P id="rowButtons" align="center">\n};

    # Boutons de validation
    if ($nomfh eq "comcont")
    {
        print $cgi->image_button(
                -id      => "btnPdf",
                -name     => 'btnPdf',
                -alt     => $mess[913],
                -title   => $mess[913],
                -src     => "$URL_IMG/export_acrobat.gif",
                -border  => 0,
                -align   => "absmiddle",
                -class   => "btnAction",
                -onclick => "hideButtons();"
            ),
            " ",
            $cgi->image_button(
                -id     => "btnComControle",
                -name   => "btnComControle",
                -alt    => "$librech[0]",
                -title  => "$librech[0]",
                -src    => "$URL_IMG/apave.gif",
                -border => 0,
                -align  => "absmiddle",
                -class => "btnAction",
                -onclick => "hideButtons();"
            ),
            " ",
            $imprim,
            $cgi->hidden('nbimpr',$ordre),
            $cgi->hidden('impfich','comcont');

        $cgi->delete('nombre');

        $nombre += 30;

        print $cgi->hidden('nombre', $nombre),
            " ",
            $cgi->image_button(
                -id     => "btnSuivant",
                -name   => "btnSuivant",
                -alt    => "Page Suivante",
                -title  => "Page Suivante",
                -src    => "$URL_IMG/suivant.gif",
                -border => 0,
                -align  => "absmiddle",
                -class => "btnAction",
            );
    }

    print $cgi->hidden('menu', $fish),
          $cgi->hidden('nbtransport', "$ordre");
    print qq{<div id="tps_exec" hidden></div>\n};

    if ($nomfh eq "nvclient")
    {
        print $cgi->image_button(
                -name   => 'nvclt',
                -alt    => $mess[511],
                -title  => $mess[511],
                -src    => "$URL_IMG/newclient_off.gif",
                -border => 0,
                -align  => "absmiddle"
            ),
            $cgi->hidden('nbimpr',$ordre);
    }

    print qq{</P>\n};
}

# Commande Controle
sub commandcont {
    my $i;
    my @req;
    my @val;
    my $nbcomm=$cgi->param('nbimpr');
    my $jourcom=$cgi->param('transjr');
    my $moiscom=$cgi->param('transms');
    my $anneecom=$cgi->param('transan');
    my $datecomm=$anneecom."-".$moiscom."-".$jourcom;
    my $reqcomm=qq{select COMMANDEAUTO FROM COMMANDE WHERE ORGAAUTO="num" AND COMMDATEENTREE="datecomm"};

    my @tabMachinesIds;
    
    for($i=0;$i<$nbcomm;$i++) {
        my $jourcoml=$cgi->param("transjr$i");
        my $moiscoml=$cgi->param("transms$i");
        my $anneecoml=$cgi->param("transan$i");
        my $montant=$cgi->param("montant$i");
        my $datect=$anneecoml."-".$moiscoml."-".$jourcoml;
        my $param= $cgi->param("$nameform[0]$i");

        if ($param eq "") {next;}

        my ($org,$contratauto,$mach,$etat)=split(/,/,$param);

        # V�rification du COMAINDICE
        my $reqindice = 'SELECT COMAAUTO FROM CONTROLEMACHINE WHERE COMAAUTO = "' . $mach . '" AND COMAINDICE = "' . $cgi->param("indicectrl$i") . '"';
        my $sth=$DB->prepare($reqindice);
        $sth->execute();
        if (!$sth->rows)
        {
            next;
        }

        if (defined $cgi->param("cont$i"))
        {
               $datecomm=$datect;
           }

        $val[2]=$org;
        $val[1]=$datecomm;
        $val[4]=$montant;
        my ($sec,$min,$hr,$jour,$mois,$an,$joursm,$jouran,$ete)=localtime;
        $mois +=1;
        $an +=1900;
        $val[0]=$an."-".$mois."-".$jour;
        my $reqco=$reqcomm;
        $reqco=~ s/num/$org/;
        $reqco=~ s/datecomm/$datecomm/;
        my $sth=$DB->prepare($reqco);
        $sth->execute();
        my $nbrow=$sth->rows;
        my $numcomm;

        if (defined $cgi->param("cont$i"))
        {
               $nbrow=0;
        }

        # Commande De Controle
        if ($nbrow !=0) {
            while (my @data=$sth->fetchrow_array()) {
                  $numcomm=$data[0];
            }
        } else {
            my $montantEuros = &to_euro($val[4], $G_MONNAIE);
            $req[0]=$reqtrait[0];
            $req[0]=~ s/val0/$val[0]/;
            $req[0]=~ s/val1/$val[1]/;
            $req[0]=~ s/val2/$val[2]/;
            $req[0]=~ s/val4/$val[4]/;
            $req[0]=~ s/val4a/$montantEuros/;
            $DB->do($req[0]);
            $numcomm=$DB->{mysql_insertid};
        }

        $sth->finish;
        # Lien controle mach et comm
        $req[1]=$reqtrait[1];
        $req[1]=~ s/ident0/$numcomm/;
        $val[3]=$mach;
        $req[1]=~ s/val3/$val[3]/;
        $req[1]=~ s/val2/$val[2]/;
        $DB->do($req[1]);

        # Controle Machine
        $req[2]=$reqtrait[2];
        $req[2]=~ s/val3/$val[3]/;
        $req[2]=~ s/val1/$val[1]/;
        $req[2]=~ s/val2/$val[2]/;
        $val[5]="";
        my $reqind=qq{select CONTRATINDICE FROM CONTRAT WHERE CONTRATAUTO="$contratauto"};
        my $sth=$DB->prepare($reqind);
        $sth->execute();

        while (my @data=$sth->fetchrow_array()) {
            $val[5]=$data[0];
        }

            $sth->finish;
        $req[2]=~ s/val5/$val[5]/;
        $DB->do($req[2]);
    }

    for($i=0;$i<$nbcomm;$i++) {
        $req[4]=$reqtrait[4];
        my $py=$cgi->param("okk$i");

        if (defined $cgi->param("cont$i"))
        {
            my $paramCont = $cgi->param("cont$i");
            push(@tabMachinesIds, $paramCont);
           }
        if ($py==0) {next};

        $val[7]=$cgi->param("indice$i");
        $val[8]=$cgi->param("verif$i");
        my $tst=$cgi->param("modif$i");

        if ($tst ne 'on') {
                  $req[4]=~ s/val7/$val[7]/;
            $req[4]=~ s/val8/$val[8]/;
            $DB->do($req[4]);
              }
    }

    # Trace
    $req[3]=$reqtrait[3];
    $DB->do($req[3]);

    if (@tabMachinesIds > 0) {
        # Impression
        my $url = &LOC::Request::createRequestUri('technical:controlOrder:pdf', 
                                                   {'machinesIds' => \@tabMachinesIds});

        if ($cgi->param('btnComControle.x') != 0)
        {
            my $printer = $cgi->param('imprimante');
            print '<script type="text/javascript">printControlOrder("' . $url . '", "' . $printer . '");</script>';
        }
        elsif ($cgi->param('btnPdf.x') != 0)
        {
            print '<script type="text/javascript">window.open("' . $url .'");</script>';
        }
    }
}


sub date_ct
{
    my @param = @_;
    my $rqctet = $reqetat[0];
    $rqctet =~ s/num/$param[0]/;
    my $stt = $DB->prepare($rqctet);
    $stt->execute();

    my ($numcom, $datecom, $pxcom, $jr, $ms, $an, $monnaie) = $stt->fetchrow_array();

    $comm = "";
    $comm .= $numcom."<BR>"      if ($numcom);
    $comm .= $datecom."<BR>"     if ($datecom);
    $comm .= $pxcom." " . $monnaie    if ($pxcom);
    $stt->finish;

    $rqctet = $reqetat[1];
    my $blct = 0;
    $rqctet =~ s/num/$numcom/;
    $stt = $DB->prepare($rqctet);
    $stt->execute();

    $blct = $stt->rows;
    $stt->finish;
    return ($comm, $jr, $ms, $an, $blct);
}


# Affichage de la liste des nouveaux clients
sub nvclient
{
    my $pagelimit = 15;
    my $rech = $cgi->param("rech");
    my $page = $cgi->param("rechpages");
    if ($page <= 0)
    {
        $page = 1;
    }

    # Fonctions JavaScript pour la recherche par nom de client
    print qq{
    <script type="text/javascript">
    var timer = null;

    var clients = new Array();
    };

    my $options = "";
    my $req = $reqrech[0];

    my $sth = $DB->prepare($req);
       $sth->execute();
       my $nbresults = $sth->rows;

    my $numpage = 1;
    my $index = 0;
    my $clste_deb = '';
    my $clste_fin = '';

    while (my @data = $sth->fetchrow_array())
    {
        if ($index % $pagelimit == 0)
        {
            $data[2] =~ s/\\/\\\\/g;
            $clste_deb = (length($data[2]) > 20) ? substr($data[2], 0, 20) . "..." : $data[2];
            $data[2] =~ s/'/\\'/g;
            print qq{    clients[} . ($numpage - 1) . qq{] = new Array('} . $data[2] . qq{', };
        }
        if ($index % $pagelimit == $pagelimit - 1 || $index == $nbresults - 1)
        {
            $data[2] =~ s/\\/\\\\/g;
            $clste_fin = (length($data[2]) > 20) ? substr($data[2], 0, 20) . "..." : $data[2];
            $data[2] =~ s/'/\\'/g;
            print qq{'} . $data[2] . qq{');\n};

            my $selected = ($numpage == $page) ? " selected" : "";

            $options .= qq{<option value="$numpage"$selected>Page $numpage : $clste_deb &rarr; $clste_fin</option>\n};

            $numpage++;
        }

        $index++;
    }

    $sth->finish;

    print qq{

    function selectPage(rech)
    {
        if (timer != null)
        {
            clearTimeout(timer);
        }

        if (rech == '' || document.getElementById('rech_old').value == rech)
        {
            return false;
        }

        document.getElementById('rech_old').value = rech;

        var page = 0;
        for (var i = 0; i < clients.length; i++)
        {
            if (rech.toUpperCase() >= clients[i][0].toUpperCase())
            {
                page = i + 1;
            }
        }

        document.getElementById('rechpages').selectedIndex = page - 1;
        document.getElementById('frm_rech').submit();

        return page;
    }
    </script>
    };

    # Zone de recherche de clients et choix de la page
    print qq{<form method="post" name="frm_rech" id="frm_rech" onsubmit="selectPage(document.getElementById('rech').value); return false;">\n};
    print qq{<input type="hidden" name="type" id="type" value="$nomfh" />\n};
    print qq{<input type="hidden" name="menu" id="menu" value="$fish" />\n};

    print qq{<input type="text" name="rech" id="rech" title="$mess[2320]" size="3" value="$rech" onkeyup="if (timer != null) clearTimeout(timer); timer = setTimeout('selectPage(\\\'' + this.value + '\\\')', 1000);" />\n};
    print qq{<input type="hidden" name="rech_old" id="rech_old" value="$rech" />\n};

    print qq{<select name="rechpages" id="rechpages" onchange="submit();">\n};
    print $options;
    print qq{</select>\n};

    print qq{</form>\n},

    my $i;
    my $j;
    my $entete = "";
    # Titre
    print qq{<form method="post" name="frm">\n},
        qq{<input type="hidden" name="type" id="type" value="$nomfh" />\n},
        qq{<table width="100%" border="0" cellspacing="2" cellpadding="2">\n};

    # Ligne d'en-t�te
     $entete .= qq{<tr bgcolor="#C6CDC5">\n};

     for($i=1; $i<$nbchamp; $i++)
    {
        
            $entete .= qq{<td style="text-align: center; padding: 2px 4px;"><font class="PT"><b>$chmprech[$i]</b></font></td>\n};
    }

      for($i=0; $i<$nbform-1; $i++)
    {
           $entete .= qq{<td style="text-align: center; padding: 2px 4px;"><font class="PT"><b>$libform[$i]</b></font></td>\n};
       }
    $entete .= qq{<td style="text-align: center; padding: 2px 4px;"><font class="PT"><b>$libform[0]</b></font></td>\n};
    $entete .= qq{<td style="text-align: center; padding: 2px 4px;"><font class="PT"><b>$mess[502]</b></font></td>\n};
    $entete .= qq{<td style="text-align: center; padding: 2px 4px;"><font class="PT"><b>$mess[503]</b></font></td>\n};
    $entete .= qq{<td style="text-align: center; padding: 2px 4px;"><font class="PT"><b>$mess[504]</b></font></td>\n};
    $entete .= qq{</tr>\n};

     my $ordre = 0;
     my $ind = 0;

    # Affichage des donn�es
    my $numdebut = ($page - 1) * $pagelimit;
    my $req = $reqrech[0] . qq{ LIMIT $numdebut, $pagelimit};

    my $sth = $DB->prepare($req);
       $sth->execute();
    my @label = @{$sth->{NAME}};

    while (my @data = $sth->fetchrow_array())
    {
        my $l;
        $e = $ind % 25;

        if ($e == 0)
        {
            print $entete;
        }

        my $c = $ind % 2;
        my $color  = "";
        my $color2 = qq{ bgColor=''};

        if ($c == 1)
        {
             $color  = qq{ bgcolor='#D6D6D6'};
             $color2 = qq{ bgColor='#D6D6D6'};
         }

        print qq{<tr$color onmouseover="bgColor='#FFCC33'" onmouseout="$color2">\n};

        for($l=1; $l<@data; $l++)
        {
            if ($label[$l] eq "CLSIREN" || $label[$l] eq "CLTEL" || $label[$l] eq "CLFAX")
            {
                 print qq{<td nowrap><font class="PT">$data[$l]</font></td>\n};
            }
            else
            {
                 print qq{<td><font class="PT">$data[$l]</font></td>\n};
            }
        }

        # Liste des modes de paiement
         my $rqpaie = $reqform[0];
         my $stpaie = $DB->prepare($rqpaie);
         my $selpaie = $data[9];
         $stpaie->execute();

         print qq{<td><font class="PT">\n};
         print qq{<select name="$nameform[0]$ordre" style="font-size: 8pt;">\n};

         while (my @data3 = $stpaie->fetchrow_array())
        {
             my $affpaie = $data3[0];
             my $valpaie = $data3[1];
             my $selectpaie = "";

             if ($affpaie eq $selpaie)
            {
                  $selectpaie = qq{ selected};
             }

             print qq{<option value="$valpaie"$selectpaie>$affpaie\n};
         }

         $stpaie->finish;

        print qq{</select>\n};
        print qq{</font></td>\n};

        # Code collectif
        print qq{<td><font class="PT">};
         $cgi->delete("collectif$ordre");
        print qq{<input type="text" name="collectif$ordre" id="collectif$ordre" size="6" style="font-size: 8pt;" />};
         print qq{</font></td>\n};

         # Code client
         print qq{<td><font class="PT">};
         $cgi->delete("codect$ordre");
        print qq{<input type="text" name="codect$ordre" id="codect$ordre" size="6" style="font-size: 8pt;" />};
         print qq{</font></td>\n};

         # Coche de validation
         print qq{<td style="text-align: center;"><font class="PT">};
         $cgi->delete("effect$ordre");
        print qq{<input type="checkbox" name="effect$ordre" id="effect$ordre" />};
        print qq{<input type="hidden" name="client$ordre" id="client$ordre" value="$data[0]" />};
         print qq{</font></td>\n};

        print qq{</tr></font>\n};
        $ind++;
        $ordre++;
    }

    $sth->finish();

    print qq{</table>\n};

    print qq{<p style="text-align: center;">\n};

    # Boutons de validation
    print qq{<input type="hidden" name="menu" id="menu" value="$fish" />\n};
#     print qq{<input type="hidden" name="nbtransport" id="nbtransport" value="$ordre" />\n};
    print qq{<input type="hidden" name="nbimpr" id="nbimpr" value="$ordre" />\n};
    print qq{<input type="hidden" name="rechpages" id="rechpages_hidden" value="$page" />\n};

    print qq{<input type="image" name="nvclt" id="nvclt" alt="$mess[511]" title="$mess[511]" src="$URL_IMG/newclient_off.gif" border="0" />\n};

    print qq{</p>\n};

    print qq{</form>\n};
}



#
# Validation des nouveaux clients
#
sub validnvclient
{
    my @req;
    my @val;
    my $ordre = $cgi->param('nbimpr');
    my $i;

    for ($i = 0; $i < $ordre; $i++)
    {
         # Client
         $val[3]=$cgi->param("client$i");
         my $reqnv=qq{select CLAUTO, CLVERIFIER FROM CLIENT WHERE CLAUTO="$val[3]" AND (CLVERIFIER="-1" or CLVERIFIER="1")};
         my $sth=$DB->prepare("$reqnv");
        $sth->execute();
         my $nvro=$sth->rows;
        my $etclient=-1;

         while (my @data=$sth->fetchrow_array()) {
            $etclient=$data[1];
        }

         $sth->finish;

         if ($nvro == 0) {next};

         $val[0]=$cgi->param("codect$i");
         $val[1]=$cgi->param("collectif$i");
         $val[2]=$cgi->param("effect$i");
        $val[6]=$cgi->param("$nameform[0]$i");

        if (($val[0] eq "")||($val[1] eq "")) {
            my $k=$i+1;
             my $erreur="";

            if ($val[0] eq "") {
                  $erreur .=qq{ $mess[1250]};
             }
             if ($val[1] eq "") {
                  if ($erreur ne "") {
                      $erreur .=qq{,};
                  }

                  $erreur .=qq{ $mess[1251]};
            }
             if (($val[0] ne "")||($val[1] ne "")) {
                 my $tableau=qq{<center><font color="red"><b>$G_PEPRENOM $mess[1252] $erreur $mess[1253] $k</b></font></center><br>\n};
                 print $tableau;
             }

             next;
        }

        # Test Existence Doublon Code Client
        my $testcode=&verif_code_client($val[0]);

        if ($testcode ne "0") {
            print $testcode;
            next;
        }

        $val[7]=0;

         if ($val[2] eq 'on') {
               $val[2]=0;
        }
        else
        {
            # si le client n'est pas valid�, ajout de la trace sur les pr�contrats de ce client
            $req[7] = $reqtrait[7];
            $req[7] =~ s/val3/$val[3]/;
            my $sth=$DB->prepare("$req[7]");
            $sth->execute();
            while (my @data=$sth->fetchrow_array()) {
               $reqContrat = $reqtrait[6];
               $reqContrat =~ s/valContratauto/$data[0]/;
               my $sthTrace = $DB->prepare("$reqContrat");
              $sthTrace->execute();
            }

            $val[2]="-1";
             $val[7]=-2;
         }

         if (($etclient ==1)||($val[2] !=0)) {
             $val[2]=1;
        }

        # Alerte Nvclient
        my $all="ALL".$G_PAYS;
        alerte_pl_ms($all,$DB,LOC::Alert::CUSTOMERSTOCHECK,-1);

        $val[3]=$cgi->param("client$i");

        # Contrat
        $req[1]=$reqtrait[1];

        if ($val[2] ==0) {
            $val[4]=1;
        }
        else
        {
            $val[4]=6;

            # R�cup�ration des identifiants des contrats du client
            my $query = '
SELECT
    CONTRATAUTO
FROM CONTRAT
WHERE CLAUTO = "' . $val[3] . '";';

            my $stmt = $DB->prepare($query);
            $stmt->execute();
            while (my @tabData = $stmt->fetchrow_array())
            {
                # Abandon des contrats
                &LOC::Contract::Rent::update($G_PAYS, $tabData[0], {'subAction' => 'cancel'}, $G_PEAUTO);
            }
            $stmt->finish();
        }

        my @contrat;
        my @modele;
        my $reqcont=qq{select CONTRATAUTO ,MOMAAUTO FROM CONTRAT WHERE CLAUTO="$val[3]"};
        my $sth=$DB->prepare("$reqcont");
        $sth->execute();
        my $k=0;

        while (my @data=$sth->fetchrow_array()) {
             $contrat[$k]=$data[0];
             $modele[$k]=$data[1];
            $k++;
        }

        $sth->finish;
        $req[1]=~ s/val0/$val[0]/;
        $req[1]=~ s/val4/$val[4]/eg;
        $req[1]=~ s/val3/$val[3]/;
        $req[1]=~ s/val6/$val[6]/;

        if ($val[2] !=1) {
            $DB->do($req[1]);
            alerte_pl_ms($G_AGENCE, $DB, LOC::Alert::MACHINESTOALLOCATE, 1);
        }

        # Stock
        if ($val[2] !=0) {
             my $k;

             for($k=0;$k<@modele;$k++) {
                 $req[2]=$reqtrait[2];
                 $val[5]=$modele[$k];
                 $req[2]=~ s/val5/$val[5]/;
                 $DB->do($req[2]);
            }
        }

        # Trace
        $req[3]=$reqtrait[3];
        $req[3]=~ s/val6/$val[6]/;
        $req[3]=~ s/val3/$val[3]/;
         $DB->do($req[3]);

        # Mise � jour des informations du client
        # (plac� en dernier pour que le renew du transport utilise les informations du contrat mises � jour)
        my $tabData    = {
            'code'               => $val[0], # CLCODE
            'accountingCode'     => $val[1], # CLCOLLECTIF
            'verificationStatus' => 0,       # CLVERIFIER
            'validityStatus'     => $val[2], # CLNONVALIDE
            'paymentMode.id'     => $val[6], # MOPAAUTO
            'isLocked'           => $val[7]  # CLSAGE
        };
        &LOC::Customer::update($G_PAYS, $val[3], $tabData, $G_PEAUTO, {'caller' => basename($0)});
    }
}

sub verif_code_client
{
    my $boole = 0;
    my @param = @_;
    my $req = qq{select CLSTE,CLCODE FROM CLIENT WHERE CLCODE="$param[0]"};
    my $stclient = $DB->prepare($req);
    $stclient->execute();
    $boole = $stclient->rows;

    if ($boole != 0)
    {
         my @data = $stclient->fetchrow_array();
         $boole = qq{<div style="text-align: center; color: red; font-weight: bold;">$data[1] $mess[2312] $data[0]</div>\n};
    }

    $stclient->finish();
    return $boole;
}

