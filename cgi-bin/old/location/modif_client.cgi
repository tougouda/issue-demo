#!/usr/bin/perl

# $Id: modification.cgi,v 1.2 2006/08/01 12:24:10 clliot Exp $
# $Log: modification.cgi,v $
# Revision 1.2  2007/03/07 12:24:10  clliot
# Syncro Exploit 01-08-2006
#

use lib '../lib', '../../inc', '../../lib';

use File::Basename;

use CGI;
use autentif;
use calculdate;
use accesbd;
use varglob;
use Date::Calc qw(Delta_Days Add_Delta_Days Days_in_Month check_date);
use message;
use tracetemps;
use fonctions;
use titanic;
use LOC::Characteristic;
use LOC::Globals;
use LOC::Util;
use LOC::Alert;
use LOC::Session;

use LOC::Customer;
use LOC::Insurance::Type;
use LOC::Appeal::Type;

# Initialisation du chrono
my $timer = &init_chrono();

&LOC::Session::load();
#
# Environnement
#
my $DB = &Environnement("LOCATION");
my $cgi = new CGI;
my @mess = &message($G_PAYS);

# Titre
my $nomfich = $cgi->param('type');
my $titre;
# D�claration des variables
my @pe_blc = qw(744 158 229 277 321 504 480 429 470 699 725 701 512 312 917 448 1086 1255 698 702 1619 1431 1232 603 732 558 1611 1453 1480 1672 1293 1127 1722 576 2022);
my $blocage = &auth_bloc;
my @champs;
my $titre = $mess[3000];
my $mode = $cgi->param('mode');
#my $clauto = $cgi->param('clauto');
my $champ_ape;
# Entete
# FA 8724 : lien pour retourner � la recherche (car le bouton page pr�c ne fonctionne pas)
# Ajout de l'URL en cinqui�me param�tre 
&ente($titre, $titre, $G_PENOM, $G_PEPRENOM, "$HTTP_PORTAIL/cgi-bin/old/location/modif_client.cgi?");

print '
<style type="text/css">
    #infos 
    {
        position: absolute;
        left: 650px;
        top: 90px;
        max-width: 500px;
        height: auto;
        text-align: left;
        background-color:#D6D6D6;
        font-size: 8pt;
        padding: 3px;
    }
    #infos tr
    {
        vertical-align: top;
    }
    #infos tr.selected
    {
        font-weight: bold;
    }
    #infos td
    {
        white-space: nowrap;
        font-size: 8pt;
    }
</style>
';



print $cgi->start_form(-method=>'post');
my $siret = $cgi->param('clsiren');
$siret =~ s/^\s+//;
$siret =~ s/\s+$//;
$cgi->param('clsiren' => $siret);

if (&isSubmitBtnClicked($cgi, 'valider'))
{
    &resultat;
}
elsif (($mode eq '') || ($mode eq 'recherche'))
{
    &recherche;
}
elsif ($mode eq 'saisie')
{
    &cl_form;
}
else
{
    print "<br/><br/><h3>System Error please contact technical support <i>5000@acces-industrie.com</i><h3>";
}
######################
#   Fonctions perl   #
######################
sub cl_form{
# Nom du champ affich� (renomm� s'il est gris�) 
$nomChampAff = '';

# Cat�gorie tarifaire, classe et potentiel accessibles en modification
# pour le profil administrateur
$disabled = "DISABLED";
if ($G_PROFIL == 28)
{
    $disabled = "";
}

# DE 8023 : Interface modification infos client pour le commerce ES
$disabledCommES = "";
if (&modif_commerce_ES)
{
    $disabledCommES = "DISABLED";
    $disabled  = "DISABLED";
}

# Utilisation de la participation au recyclage
$utiliseGestionDechets = &utilise_participation_recyclage();

if ( $_[0] eq 'consult' )
{
    &getCustomerInfos($_[1]);
}
# R�cup�ration des informations saisie ou en base
elsif( ($mode eq 'saisie') || ($mode eq 'recherche') )
{
    my @param = $cgi->param();
    foreach (@param) 
    {
        $form_val{$_} = $cgi->param($_);
    }
    $form_val{'cldatevalass'} = $form_val{'cldatevalass2'}."-".$form_val{'cldatevalass1'}."-".$form_val{'cldatevalass0'};
    $form_val{'cldatedebass'} = $form_val{'cldatedebass2'}."-".$form_val{'cldatedebass1'}."-".$form_val{'cldatedebass0'};
    $form_val{'cldatedebtyperecours'} = $form_val{'cldatedebtyperecours2'}."-".$form_val{'cldatedebtyperecours1'}."-".$form_val{'cldatedebtyperecours0'};

}
#Particularit�s Espagne - Portugal
if ( ($G_PAYS eq 'ES') || ($G_PAYS eq 'PT') || ($G_PAYS eq 'MA') || ($G_PAYS eq 'IT') ){
    $nomChampAff = ($disabledCommES eq '' ? 'clape' : 'clapeAff');  
  $champ_ape = $cgi->textfield( -name=>$nomChampAff, -default=>$form_val{'clape'}, -size=>3, -maxlength=>4, -id=>'clape', $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden('clape', $form_val{'clape'} ));
} else {
    $nomChampAff = ($disabledCommES eq '' ? 'clape' : 'clapeAff');  
    $champ_ape = $cgi->textfield( -name=>'clape1', -default=>substr($form_val{'clape'}, 0, 4), -size=>4, -maxlength=>4, -id=>'clape1', $disabledCommES) .
    &make_select($nomChampAff, 'ape', $form_val{'clape'}, $form_val{'clape1'}, $disabledCommES, "id='clape' onChange='this.form.submit()'") . ($disabledCommES eq '' ? '' : $cgi->hidden('clape', $form_val{'clape'} )) . ($disabledCommES eq '' ? '' : $cgi->hidden('clape1', substr($form_val{'clape'}, 0, 4)));
}
# Gestion des recours par d�faut
$form_val{'defaultAppealTypeId'} = &LOC::Appeal::Type::getDefaultId($G_PAYS);

# Enregistrement des donn�es
if (&isSubmitBtnClicked($cgi, 'enregistrer'))
{
    &cl_update;
}

# D�finition des champs du formulaire :
# [Message, Champ du formulaire, Options], # commentaire
$nomChampAff = ($disabledCommES eq '' ? '' : 'Aff');


my @appealBeginDate = split (/-/, ($form_val{'cldatedebtyperecours'} ? $form_val{'cldatedebtyperecours'} : '0000-00-00'));
my $divAppealBeginDate = '
    <div style="display: inline-block;">
        <input type="text" name="cldatedebtyperecours0" value="' . $appealBeginDate[2] . '" size="1" maxlength="2"/> /
        <input type="text" name="cldatedebtyperecours1" value="' . $appealBeginDate[1] . '" size="1" maxlength="2" /> /
        <input type="text" name="cldatedebtyperecours2" value="' . $appealBeginDate[0] . '" size="2" maxlength="4" />
        <i>' . $mess[3058] . '</i>
    </div>';

@champs = ( [ $mess[3002], $cgi->textfield( -name=>'clcode' . $nomChampAff, -default=>$form_val{'clcode'}, $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden( 'clcode', $form_val{'clcode'} )) . $cgi->hidden( -name=>'clauto', -value=>$form_val{'clauto'} ) ], # Code client
            [ $mess[3003], $champ_ape ], #APE
            [ $mess[3023], &make_select('metierauto', 'jobTitle', $form_val{'metierauto'}, 1 ) ],   # Activit�
            [ $mess[3004], qq|<input type="text" name="clsiren" value="$form_val{'clsiren'}" />| . $cgi->textfield( -name=>'clsiretcpl', -default=>$form_val{'clsiretcpl'}, -size=>5, -title=>$mess[3039], -maxlength=>5 ) ], # Siret
            #[ $mess[3004], $cgi->textfield( -name=>'clsiren', -value=>$form_val{'clsiren'} ) ], # Siren
            [ $mess[3005], $cgi->textfield( -name=>'clste', -default=>$form_val{'clste'}, -onblur=>'this.value = this.value.toUpperCase();' ) ], # Nom client (libell�)
            [ $mess[3006], $cgi->textfield( -name=>'cladresse', -default=>$form_val{'cladresse'} ) ], # Adresse
            [ $mess[3007], $cgi->textfield( -name=>'cladresse2', -default=>$form_val{'cladresse2'} ) ], # Adresse comp.
            [ $mess[3008], &make_select( 'viclauto', 'locality', $form_val{'viclauto'}, $form_val{'viclnom'} ) . $cgi->textfield( -name=>'viclnom', -default=>$form_val{'viclnom'}, -size=>10 ) ], # Ville
            [ $mess[3009], $cgi->textfield( -name=>'cltel', -default=>$form_val{'cltel'} ) ], # Tel
            [ $mess[3010], $cgi->textfield( -name=>'clfax', -default=>$form_val{'clfax'} ) ], # Fax
            [ $mess[3011], $cgi->textfield( -name=>'clportable', -default=>$form_val{'clportable'} ) ], # Portable
            [ $mess[3012], $cgi->textfield( -name=>'clemail', -default=>$form_val{'clemail'} ) ], # E-mail
            [ $mess[3013], $cgi->textfield( -name=>'clcontact', -default=>$form_val{'clcontact'} ) ], # Contact
            [ $mess[3014], $cgi->textfield( -name=>'clcollectif' . $nomChampAff, -default=>$form_val{'clcollectif'}, $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden( 'clcollectif', $form_val{'clcollectif'} ))], # Collectif
            [ $mess[3015], $cgi->textfield( -name=>'cllino' . $nomChampAff, -default=>$form_val{'cllino'}, $disabledCommES ) . ($disabledCommES eq '' ? '' : $cgi->hidden( 'cllino', $form_val{'cllino'} ))], # LI_NO
            [ $mess[3016], $cgi->textfield( -name=>'clbanque' . $nomChampAff, -default=>$form_val{'clbanque'}, $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden( 'clbanque', $form_val{'clbanque'} ))], # Banque
            [ $mess[3017], &make_select( 'mopaauto' . $nomChampAff, 'paymentMode', $form_val{'mopaauto'}, 1, $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden( 'mopaauto', $form_val{'mopaauto'} ))], # Type Paiement
            [ $mess[3018], $cgi->textfield( -name=>'clrib' . $nomChampAff, -default=>$form_val{'clrib'}, $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden( 'clrib', $form_val{'clrib'} ))], # RIB
            [ $mess[3019], $cgi->textfield( -name=>'clnbfact' . $nomChampAff, -default=>$form_val{'clnbfact'}, $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden( 'clnbfact', $form_val{'clnbfact'} ))], # Nb.Factures(s)
            [ $mess[3020], $cgi->checkbox( -name=>'clcee', -checked=>$form_val{'clcee'}, -value=>'1', -label=>' ', $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden( 'clcee', $form_val{'clcee'} ))], # Facturation Locale
            [ $mess[3021], $cgi->checkbox( -name=>'clfactgrp', -checked=>$form_val{'clfactgrp'}, -value=>'1', -label=>' ', $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden( 'clfactgrp', $form_val{'clfactgrp'} ))], # Facture non group�e
            [ $mess[3022], $cgi->textarea( -name=>'clcommentaire' . $nomChampAff, -default=>$form_val{'clcommentaire'}, -rows=>3, -columns=>50, $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden( 'clcommentaire', $form_val{'clcommentaire'} ))], # Commentaire
            [ $mess[3024], &make_select( 'clienttarifauto' . $nomChampAff, 'gridTariff', $form_val{'clienttarifauto'}, 1, $disabled ) . ($disabled eq "" ? "" : $cgi->hidden( 'clienttarifauto', $form_val{'clienttarifauto'} )) ], # Cat. Tarif
            [ $mess[3025], $cgi->checkbox( -name=>'clsage', -checked=>$form_val{'clsage'}, -value=>'-2', -label=>' ', -$blocage ) ], # Sommeil/Bloqu�
            [ $mess[3055], &aff_assurance('cltypeassurance', $form_val{'cltypeassurance'}, ($disabledCommES eq "" ? 6 : 5), $form_val{'clpassurance'}, '', $form_val{'cldatevalass'}, '', 'DISABLED') . $cgi->hidden( -name=>'cltypeassurance', -value=>$form_val{'cltypeassurance'}, -override=>1) . $cgi->hidden( -name=>'clpassurance', -value=>$form_val{'clpassurance'}, -override=>1)], # Assurance actuelle
            [ $mess[3054], &aff_assurance('clnvassurance', $form_val{'clnvassurance'}, ($disabledCommES eq "" ? 7 : 5), $form_val{'clnvpassurance'}, '', '', $form_val{'cldatedebass'}, $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden( -name=>'clnvassurance', -value=>$form_val{'clnvassurance'}) . $cgi->hidden( -name=>'clnvpassurance', -value=>$form_val{'clnvpassurance'}))], # Assurance souhait�e

            [ $mess[3056], &make_select( 'cltyperecours' . $nomChampAff, 'appeal', $form_val{'cltyperecours'}, 0, 'disabled="disabled"'). $cgi->hidden( 'cltyperecours', $form_val{'cltyperecours'}), {'class' => 'appeal'} ],
            [ $mess[3057], &make_select( 'clnvtyperecours' . $nomChampAff, 'appeal', (!$form_val{'cltyperecours'} && !$form_val{'clnvtyperecours'} ? $form_val{'defaultAppealTypeId'} : $form_val{'clnvtyperecours'}), 1) . $divAppealBeginDate . $cgi->hidden( 'clnvtyperecours', $form_val{'clnvtyperecours'}), {'class' => 'appeal'} ],

            [ $mess[3027], $cgi->checkbox( -name=>'clnumcmdobli', -checked=>$form_val{'clnumcmdobli'}, -value=>'-1', -label=>' ', $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden( 'clnumcmdobli', $form_val{'clnumcmdobli'} ))], # N� commande obligatoire
            [ $mess[3046], $cgi->checkbox( -name=>'clbonsoblig', -checked=>$form_val{'clbonsoblig'}, -value=>'-1', -label=>' ', $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden( 'clbonsoblig', $form_val{'clbonsoblig'} ))], # Bons de commandes obligatoire
            [ $mess[3028], $cgi->checkbox( -name=>'bcjoin', -checked=>$form_val{'bcjoin'}, -value=>'-1', -label=>' ', $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden( 'bcjoin', $form_val{'bcjoin'} ))], # BC � fournir
            [ $mess[3029], $cgi->checkbox( -name=>'clproforma', -checked=>$form_val{'clproforma'}, -value=>'-1', -label=>' ', $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden( 'clproforma', $form_val{'clproforma'} ))], # Paiement � la livraison
            [ $mess[3030], $cgi->checkbox( -name=>'clcgvente', -checked=>$form_val{'clcgvente'}, -value=>'-1', -label=>' ', $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden( 'clcgvente', $form_val{'clcgvente'} ))], # CGV
            [ $mess[3031], $cgi->checkbox( -name=>'clnettoyage', -checked=>$form_val{'clnettoyage'}, -value=>'-1', -label=>' ', $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden( 'clnettoyage', $form_val{'clnettoyage'} ))], # Facturation nettoyage
        );
if ($utiliseGestionDechets)
{
    push(@champs,
            [ $mess[3047], $cgi->checkbox( -name=>'cldechetsfact', -checked=>$form_val{'cldechetsfact'}, -value=>'-1', -label=>' ', $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden( 'cldechetsfact', $form_val{'cldechetsfact'} ))], # Facturation participation au recyclage
        );
}
push(@champs,
            [ $mess[2366], &make_select( 'clgroupeauto2', 'group', $form_val{'clgroupeauto'}, 1, 'DISABLED') . $cgi->hidden( -name=>'clgroupeauto', -value=>$form_val{'clgroupeauto'} ) ], # Groupe
            [ $mess[7150], &make_select( 'ccauto' . $nomChampAff, 'frameworkContract', $form_val{'ccauto'}, 1, $disabledCommES) . $cgi->hidden( -name=>'ccauto', -value=>$form_val{'ccauto'} ) ], # Contrat cadre
            [ $mess[2380], &make_select( 'clclasse' . $nomChampAff, 'class', $form_val{'clclasse'}, 1, $disabled) . ($disabled eq "" ? "" : $cgi->hidden( 'clclasse', $form_val{'clclasse'} )) ], # Classe
            [ $mess[2372], &make_select( 'clpotentiel' . $nomChampAff, 'potential', $form_val{'clpotentiel'}, 1, $disabled) . ($disabled eq "" ? "" : $cgi->hidden( 'clpotentiel', $form_val{'clpotentiel'} )) . # Potentiel
            "<input type='hidden' name='mode' value='saisie'/>" ], # Saisie / lecture
            [ $mess[3048], $cgi->textfield( -name=>'clqualification', -default=>$form_val{'clqualification'} ) ], # Qualification
            [ $mess[7721], $cgi->checkbox( -name=>'cljoindocmach', -checked=>$form_val{'cljoindocmach'}, -value=>'-1', -label=>' ', $disabledCommES) . ($disabledCommES eq '' ? '' : $cgi->hidden( 'cljoindocmach', $form_val{'cljoindocmach'} ))] #Document machine
        );
# D�marrage du formulaire
print "<TABLE border='0' STYLE='font-family:Arial;font-size:8pt'>
<tr>";
#for $i (0..32)
for(my $i=0;$i<@champs;$i++)
{
    if ($champs[$i][0] eq $mess[3046] && $G_PAYS ne 'ES')
    {
        print "<tr style='display:none'><td><font class='PT'><b>" . $champs[$i][0] . " : </b></font></td><td><font class='PT'>" . $champs[$i][1] . "</font></td></tr>";
    }
    else
    {
        my $class = ($champs[$i][2] && $champs[$i][2]->{'class'} ? $champs[$i][2]->{'class'} : '');
        print "<tr class='" . $class . "'><td><font class='PT'><b>" . $champs[$i][0] . " : </b></font></td><td><font class='PT'>" . $champs[$i][1] . "</font></td></tr>";
    }
}

print '
<script type="text/javascript">
    displayAppeal(tabInsuranceTypes, "' . $form_val{'cltypeassurance'} . '", "' . $form_val{'clnvassurance'} . '");
</script>';

if( &isEditAuthorized() || &modif_commerce_ES)
{
    print "<tr><td></td><td>".$cgi->image_button(-name=>'enregistrer', -alt=>$mess[161], -border=>0, -src=>"$URL_IMG/modif_off.gif")."</td></tr>";
}

print "</tr></TABLE>";
# Affichage de la liste des CODES APE
print &cl_ape( $form_val{'clape'} );
}
# G�n�rateur de <SELECT> make_select(nom du champ, num_requ�te, val selected default, condition ou lib vide, option)
sub make_select
{
    my %requete; # Requ�tes
    my $select;
    my $tmp_select;
    my $selected = "";
    my @data;
    my $i = 0;

    my ($fieldName, $query, $defaultValue, $search, $attributes) = @_;

    $search =~ s/'/\\'/g;

    $requete{'jobTitle'}    = "SELECT METIERAUTO, MEACTIVITE FROM METIER ORDER BY MEACTIVITE;";
    $requete{'locality'}    = "SELECT VICLAUTO, CONCAT(VICLCP,', ', VICLNOM,', ', VICLPAYS,', ', VICLAUTO) AS VILLE FROM AUTH.VICL  WHERE VICLCP LIKE '".$search."%' OR VICLNOM LIKE '".$search."%' ORDER BY  VICLCP;";
    $requete{'paymentMode'} = "SELECT MOPAAUTO, MOPALIBELLE FROM MODEPAIE ORDER BY MOPALIBELLE;";
    $requete{'gridTariff'}  = "SELECT CLIENTTARIFAUTO, TARIFCLIENTREF FROM CLIENTTARIF WHERE TARIFCLIENTACTIF = '1' ORDER BY TARIFCLIENTREF;";
    $requete{'group'}       = qq{SELECT G.CLGROUPEAUTO, G.CLGRLIBELLE FROM $GESCOM_BASE{"$G_PAYS"}.GC_CLGROUPE G ORDER BY G.CLGRLIBELLE};
    $requete{'class'}       = "SELECT DISTINCT CLCLASSE, CLCLASSE FROM CLIENT ORDER BY CLCLASSE;";
    $requete{'ape'}         = "SELECT CODEAPE, SUBSTRING(CODEAPE, 5, 1) AS CODEAPE FROM APE2 WHERE SUBSTRING(CODEAPE, 1, 4) = '" . $search . "';";
    $requete{'code'}        = "SELECT CLAUTO, CLCODE FROM CLIENT WHERE CLCODE like '".$search."%'";
    $requete{'label'}       = "SELECT CLAUTO, CLSTE FROM CLIENT WHERE CLSTE like '".$search."%'";
    $requete{'city'}        = "SELECT VICLAUTO, VICLNOM FROM AUTH.VICL WHERE VICLCP LIKE '".$search."%' OR VICLNOM LIKE '".$search."%' ORDER BY VICLNOM";
    $requete{'department'}  = "SELECT DEP, DEP FROM AUTH.VICL WHERE DEP = '".$search."' LIMIT 1";
    $requete{'siren'}       = "SELECT CLSIREN, CLSIREN FROM CLIENT WHERE CLSIREN like '".$search."%'";
    $requete{'potential'}   = "SELECT DISTINCT CLPOTENTIEL, IFNULL(CLPOTENTIEL, '$mess[2375]') FROM CLIENT ORDER BY CLCLASSE;";
    $requete{'frameworkContract'} = qq{SELECT G.CCAUTO, G.CCLIBELLE FROM $GESCOM_BASE{"$G_PAYS"}.GC_CONTRATCADRE G ORDER BY G.CCLIBELLE};
    $requete{'appeal'}      = "SELECT apl_id, IF (apl_labelfull IS NOT NULL, REPLACE(REPLACE(REPLACE(apl_labelfull, '<%label>', apl_label), '<%extraLabel>', apl_labelext), '<%percent>', '%'), apl_label) FROM p_appealtype WHERE apl_sta_id != 'GEN03';";

    $select .= "<SELECT name='$fieldName' $attributes>";
    if ( $search ne "" )
    {
        $sth = $DB->prepare($requete{$query});
        $sth->execute;
        while ( @data = $sth->fetchrow_array() ) 
        {
            if( $data[0] eq $defaultValue)
            {
                $selected = "SELECTED";
            }
            else
            {
                $selected = "";
            }
            $i++;
            $tmp_select .= "<OPTION value='$data[0]' $selected>" . $data[1] . "</OPTION>";
        }
        $sth->finish;
    } 

    if ( ( $i eq 0 ) || ( $search eq "1" ) )
    { 
        $select .= "<OPTION value=''>" . 'Selection' . "</OPTION>";
    }

    $select .= $tmp_select;
    $select .= "</SELECT> <b>$i</b> ";
    return $select;
}

# Recherche des codes ape et affichage de la liste (encart gris)
sub cl_ape
{
    my $requete = "SELECT A.CODEAPE, A.APELIBELLE, SUBSTRING(A.CODEAPE, 5, 1) AS LETTRE FROM APE2 A WHERE SUBSTRING(A.CODEAPE, 1, 4) = '" . $form_val{'clape1'} . "';"; # Requ�tes
    my $cadre;
    $sth = $DB->prepare($requete);
    if ( $form_val{'clape1'} ne "" ){
        $sth->execute;
        $cadre = '
        <div id="infos">
            <table cellpadding="0" cellspacing="2">';
        while ( my @data = $sth->fetchrow_array() ) 
        {
            my $className = ($_[0] eq $data[0] ? 'selected' : '');
            $cadre .= '
                <tr class="' . $className . '">
                    <td>' . $data[2] . ' : </td>
                    <td>' . $data[1] . '</td>
                </tr>';
        }
        $cadre .= '
            </table>
        </div>';
    }
    return $cadre;
}

# Fonction de recherche d'un client
sub recherche {
    # R�cup�ration automatique de toutes les varibles
    my @param = $cgi->param();
    foreach (@param) 
    {
        $form_val{$_} = $cgi->param($_);
    }
    @champs = ( [ $mess[3040], &make_select( 'Rclauto', 'code', $form_val{'Rclauto'}, $form_val{'Rrclcode'} ) . $cgi->textfield( -name=>'Rrclcode', -default=>$form_val{'Rrclcode'}, -size=>10 ) ],
                [ $mess[3041], &make_select( 'Rclste', 'label', $form_val{'Rclste'}, $form_val{'Rrclste'} ) . $cgi->textfield( -name=>'Rrclste', -default=>$form_val{'Rrclste'}, -size=>10 ) ],
                [ $mess[3042], &make_select( 'Rviclauto', 'city', $form_val{'Rviclauto'}, $form_val{'Rrvicl'} ) . $cgi->textfield( -name=>'Rrvicl', -default=>$form_val{'Rrvicl'}, -size=>10 ) ],
                [ $mess[3043], &make_select( 'Rvidep', 'department', $form_val{'Rvidep'}, $form_val{'Rrvidep'} ) . $cgi->textfield( -name=>'Rrvidep', -default=>$form_val{'Rrvidep'}, -size=>10 ) ],
                [ $mess[3044], &make_select( 'Rclsiren', 'siren', $form_val{'Rclsiren'}, $form_val{'Rrclsiren'} ) . $cgi->textfield( -name=>'Rrclsiren', -default=>$form_val{'Rrclsiren'}, -size=>10 ) ],
                [ $mess[3045], &make_select( 'Rclienttarifauto', 'gridTariff', $form_val{'Rclienttarifauto'}, 1 ) . $cgi->hidden( -name=>'mode', -default=>'recherche', -id=>'mode' ) . $cgi->hidden( -name=>'curseur', -default=>'0', -id=>'curseur' ) ]
                ); # Code client

    # D�marrage du formulaire
    print "<TABLE border='0' STYLE='font-family:Arial;font-size:8pt'>
    <tr>";
    print $cgi->start_form(-method=>'post');
    
    for(my $i=0;$i<@champs;$i++)
    {
        print "<tr><td><font class='PT'><b>" . $champs[$i][0] . " : </b></font></td><td><font class='PT'>" . $champs[$i][1] . "</font></td></tr>";
    }
    print "<tr><td>".$cgi->image_button(-name=>'valider', -alt=>$mess[161], -border=>0, -src=>"$URL_IMG/valid_off.gif" )."</td><td></td></tr>";
    print "</tr></TABLE>";
    print $cgi->end_form();
}
# R�sultat de la recherche. Si 1 ligne > consult, si plusieurs > Tableau
sub resultat {
    # Utilisation de la participation au recyclage
    $utiliseGestionDechets = &utilise_participation_recyclage();

    my $clause;
    my $clauto;
    my $curseur        = $cgi->param('curseur');
    my $nbligne        = 20;
    if ( $cgi->param('Rclauto') ne "" ) {
        $clauto = $cgi->param('Rclauto'); 
        $clause = "C.CLAUTO = $clauto";
        $curseur = 0;
    }
    elsif ( $cgi->param('Rclste') ne "" ){
        $clauto = $cgi->param('Rclste'); 
        $clause = "C.CLAUTO = $clauto"; 
    }
    else {
        $clause = "V.VICLAUTO like '".$cgi->param('Rviclauto')."%' AND (V.DEP like '".$cgi->param('Rvidep')."%' or V.DEP is NULL) AND C.CLSIREN like '".$cgi->param('Rclsiren')."%' AND C.CLIENTTARIFAUTO like '".$cgi->param('Rclienttarifauto')."%'";
    }
    my $requete = "SELECT C.CLAUTO, C.CLCODE, C.CLAPE, C.CLSIREN, C.CLSTE, C.CLADRESSE, C.CLADRESSE2, V.VICLNOM, 
    C.CLTEL, C.CLFAX, C.CLPORTABLE, C.CLEMAIL, C.CLCONTACT, C.CLCOLLECTIF, C.CLLINO, C.CLBANQUE, M.MOPALIBELLE, C.CLRIB, 
    C.CLNBFACT, C.CLCEE, C.CLFACTGRP, C.CLCOMMENTAIRE, CT.TARIFCLIENTREF, C.CLSAGE, C.CLTYPEASSURANCE, C.CLPASSURANCE, C.CLNUMCMDOBLI, C.BCJOIN, 
    C.CLPROFORMA, C.CLCGVENTE, C.CLNETTOYAGE, G.CLGRLIBELLE, C.CLCLASSE, C.CLPOTENTIEL, C.CLDECHETSFACT, C.CLQUALIFICATION, C.CLJOINDOCMACH
    FROM CLIENT C
    LEFT JOIN AUTH.VICL V ON V.VICLAUTO = C.VICLAUTO
    LEFT JOIN MODEPAIE M ON M.MOPAAUTO = C.MOPAAUTO
    LEFT JOIN CLIENTTARIF CT ON CT.CLIENTTARIFAUTO = C.CLIENTTARIFAUTO
    LEFT JOIN ".$GESCOM_BASE{"$G_PAYS"}.".GC_CLGROUPE G ON G.CLGROUPEAUTO = C.CLGROUPEAUTO 
    WHERE ".$clause." LIMIT $curseur, $nbligne";
    $sth = $DB->prepare($requete);
    $sth->execute;
#     print "<pre>$requete</pre>";
    my $i             = 0;
    my $tableau     = "<br/>";
    my $background  = "bgcolor='C6CDC5'";
    my $fonto         = "<font class='PT'>";
    my $fontf         = "</font>";

    my $paramMode = $cgi->param('mode');
    my $Rclauto   = $cgi->param('Rclauto');
    my $Rclste    = $cgi->param('Rclste');
    my $Rviclauto = $cgi->param('Rviclauto');
    my $Rvidep    = $cgi->param('Rvidep');
    my $Rclsiren  = $cgi->param('Rclsiren');
    my $Rclienttarifauto = $cgi->param('Rclienttarifauto');
    
    $tableau .= $cgi->hidden( -name=>'mode', -default => $paramMode, -id=>'mode' ).
    $cgi->hidden( -name=>'Rclauto', -default => $Rclauto, -id=>'Rclauto' ).
    $cgi->hidden( -name=>'Rclste', -default => $Rclste, -id=>'Rclste' ).
    $cgi->hidden( -name=>'Rviclauto', -default => $Rviclauto, -id=>'Rviclauto' ).
    $cgi->hidden( -name=>'Rvidep', -default => $Rvidep, -id=>'Rvidep' ).
    $cgi->hidden( -name=>'Rclsiren', -default => $Rclsiren, -id=>'Rclsiren' ).
    $cgi->hidden( -name=>'Rclienttarifauto', -default => $Rclienttarifauto, -id=>'Rclienttarifauto' ).
    $cgi->hidden( -name=>'curseur', -default=>$curseur, -id=>'curseur' );
    if ( $curseur > 0 ){
    $tableau .= $cgi->image_button(-name=>'valider', -alt=>'>>>', -border=>0, -src=>"$URL_IMG/precedent.gif", -onclick=>"document.getElementById('curseur').value=$curseur - 20;");
    }
    $tableau .= $cgi->image_button(-name=>'valider', -alt=>'>>>', -border=>0, -src=>"$URL_IMG/suivant.gif", -onclick=>"document.getElementById('curseur').value=$curseur + 20;");
    $tableau .= "<table>";
    $tableau .= "<tr>
    <td bgcolor=C6CDC5>$fonto<b>".$mess[3002]."</b>$fontf</td><td bgcolor=C6CDC5>$fonto<b>".$mess[3003]."</b>$fontf</td>
    <td bgcolor=C6CDC5>$fonto<b>".$mess[3004]."</b>$fontf</td><td bgcolor=C6CDC5>$fonto<b>".$mess[3005]."</b>$fontf</td>
    <td bgcolor=C6CDC5>$fonto<b>".$mess[3006]."</b>$fontf</td><td bgcolor=C6CDC5>$fonto<b>".$mess[3007]."</b>$fontf</td>
    <td bgcolor=C6CDC5>$fonto<b>".$mess[3008]."</b>$fontf</td><td bgcolor=C6CDC5>$fonto<b>".$mess[3009]."</b>$fontf</td>
    <td bgcolor=C6CDC5>$fonto<b>".$mess[3013]."</b>$fontf</td>
    <td bgcolor=C6CDC5>$fonto<b>".$mess[3014]."</b>$fontf</td>
    <td bgcolor=C6CDC5>$fonto<b>".$mess[3016]."</b>$fontf</td><td bgcolor=C6CDC5>$fonto<b>".$mess[3017]."</b>$fontf</td>
    <td bgcolor=C6CDC5>$fonto<b>".$mess[3019]."</b>$fontf</td>
    <td bgcolor=C6CDC5>$fonto<b>".$mess[3020]."</b>$fontf</td><td bgcolor=C6CDC5>$fonto<b>".$mess[3021]."</b>$fontf</td>
    <td bgcolor=C6CDC5>$fonto<b>".$mess[3022]."</b>$fontf</td><td bgcolor=C6CDC5>$fonto<b>".$mess[3024]."</b>$fontf</td>
    <td bgcolor=C6CDC5>$fonto<b>".$mess[3025]."</b>$fontf</td><td bgcolor=C6CDC5>$fonto<b>".$mess[3026]."</b>$fontf</td>
    <td bgcolor=C6CDC5>$fonto<b>".$mess[3027]."</b>$fontf</td><td bgcolor=C6CDC5>$fonto<b>".$mess[3028]."</b>$fontf</td>
    <td bgcolor=C6CDC5>$fonto<b>".$mess[3029]."</b>$fontf</td><td bgcolor=C6CDC5>$fonto<b>".$mess[3030]."</b>$fontf</td>
    <td bgcolor=C6CDC5>$fonto<b>".$mess[3031]."</b>$fontf</td>
    " . ($utiliseGestionDechets ? "<td bgcolor=C6CDC5>$fonto<b>".$mess[3047]."</b>$fontf</td>" : "") . "
    <td bgcolor=C6CDC5>$fonto<b>".$mess[2366]."</b>$fontf</td>
    <td bgcolor=C6CDC5>$fonto<b>".$mess[2380]."</b>$fontf</td><td bgcolor=C6CDC5>$fonto<b>".$mess[2372]."</b>$fontf</td>
    <td bgcolor=C6CDC5>$fonto<b>".$mess[3048]."</b>$fontf</td>
    <td bgcolor=C6CDC5>$fonto<b>".$mess[7721]."</b>$fontf</td>
    <td bgcolor=C6CDC5>$fonto<b></b>$fontf</td></tr>";
    while ( @data = $sth->fetchrow_array() )
    {
        if ( $i % 2 ) { $background = "bgColor='#D6D6D6' onmouseover=\"bgColor='#FFCC33'\" onmouseout=\"bgColor='#D6D6D6'\""; } else { $background = "bgColor='' onmouseover=\"bgColor='#FFCC33'\" onmouseout=\"bgColor=''\""; } # $i Pair ou impair
        if ( $data[23] eq -2 ) { $fonto = "<font class='PT' style='color:red'>"; } else { $fonto = "<font class='PT'>"; }
        $tableau .= "<tr $background>
        <td>$fonto".$data[1]."$fontf</td><td>$fonto".$data[2]."$fontf</td>
        <td>$fonto".$data[3]."$fontf</td><td>$fonto".$data[4]."$fontf</td><td>$fonto".$data[5]."$fontf</td>
        <td>$fonto".$data[6]."$fontf</td><td>$fonto".$data[7]."$fontf</td><td>$fonto".$data[8]."$fontf</td>
        <td>$fonto".$data[12]."$fontf</td><td>$fonto".$data[13]."$fontf</td>
        <td>$fonto".$data[15]."$fontf</td><td>$fonto".$data[16]."$fontf</td>
        <td>$fonto".$data[18]."$fontf</td>
        <td>$fonto".$cgi->checkbox( -name=>'clcee'.$data[0], -checked=>$data[19], -value=>'1', -label=>' ' )."$fontf</td>
        <td>$fonto".$cgi->checkbox( -name=>'clfactgrp'.$data[0], -checked=>$data[20], -value=>'1', -label=>' ' )."$fontf</td>
        <td title='".$data[21]."'>$fonto".substr($data[21], 0, 60)."$fontf</td><td>$fonto".$data[22]."$fontf</td>
        <td>$fonto".$cgi->checkbox( -name=>'clsage'.$data[0], -checked=>$data[23], -value=>'-2', -label=>' ' )."$fontf</td>
        <td>$fonto".&aff_assurance('cltypeassurance'.$data[0], $data[23], 4, $data[24])."$fontf</td>
        <td>$fonto".$cgi->checkbox( -name=>'clnumcmdobli'.$data[0], -checked=>$data[26], -value=>'-1', -label=>' ' )."$fontf</td>
        <td>$fonto".$cgi->checkbox( -name=>'bcjoin'.$data[0], -checked=>$data[27], -value=>'-1', -label=>' ' )."$fontf</td>
        <td>$fonto".$cgi->checkbox( -name=>'clproforma'.$data[0], -checked=>$data[28], -value=>'-1', -label=>' ' )."$fontf</td>
        <td>$fonto".$cgi->checkbox( -name=>'clcgvente'.$data[0], -checked=>$data[29], -value=>'-1', -label=>' ' )."$fontf</td>
        <td>$fonto".$cgi->checkbox( -name=>'clnettoyage'.$data[0], -checked=>$data[30], -value=>'-1', -label=>' ' )."$fontf</td>
        " . ($utiliseGestionDechets ? "<td>$fonto".$cgi->checkbox( -name=>'cldetchetsfact'.$data[0], -checked=>$data[34], -value=>'-1', -label=>' ' )."$fontf</td>" : "") . "
        <td>$fonto".$data[31]."$fontf</td>
        <td>$fonto".$data[32]."$fontf</td>
        <td>$fonto".$data[33]."$fontf</td>
        <td>$fonto".$data[35]."$fontf</td>
        <td>$fonto".$cgi->checkbox( -name=>'cljoindocmach'.$data[0], -checked=>$data[36], -value=>'-1', -label=>' ' )."$fontf</td>
        <td>$fonto".$cgi->image_button(-name=>'valider', -alt=>$mess[161], -border=>0, -src=>"$URL_IMG/loupe_off.gif", -onclick=>"document.getElementById('Rclauto').value=$data[0]" )."$fontf</td></tr>";
        $i++;
        $clauto = $data[0];
    }
    $tableau .= "</table>";
    if ( $curseur > 0 ){
    $tableau .= $cgi->image_button(-name=>'valider', -alt=>'>>>', -border=>0, -src=>"$URL_IMG/precedent.gif", -onclick=>"document.getElementById('curseur').value=$curseur - 20;");
    }
    $tableau .= $cgi->image_button(-name=>'valider', -alt=>'>>>', -border=>0, -src=>"$URL_IMG/suivant.gif", -onclick=>"document.getElementById('curseur').value=$curseur + 20;");
    $sth->finish;
    if ( $i eq 1 ) { &cl_form('consult', $clauto); }
    elsif ( $i eq 0 ) { 
    print "<div align='left'><b><a href='javascript:history.back(1)'>->$mess[152]</a></b></div>
     <font color='red' size=2><i><b>$mess[151]</b></i></font>"; }
    else { print $tableau; }
}

# Enregistrement des info de la fiche client
sub cl_update
{
    my $countryId  = $G_PAYS;

    if (!($siret =~ /(PA|ET)[0-9]\s[0-9]{3}\s[0-9]{3}\s[0-9]{5}/))
    {
        # G�n�ration automatique d'un SIRET fictif pour les particuliers
        if ($form_val{'clsiren'} =~ /^PA/i)
        {
            my $requeteS = "SELECT MAX(CLSIREN) FROM CLIENT C WHERE CLSIREN LIKE 'PA0 000 000%'";
            $sth=$DB->prepare("$requeteS");
            $sth->execute();
            my @siret = $sth->fetchrow_array;
            $form_val{'clsiren'} = "PA0 000 000 " . sprintf("%05d", (substr($siret[0], -5) + 1));
        }

        # G�n�ration automatique d'un SIRET fictif pour les �trangers
        if ($form_val{'clsiren'} =~ /^ET/i)
        {
            my $requeteS = "SELECT MAX(CLSIREN) FROM CLIENT C WHERE CLSIREN LIKE 'ET0 000 000%'";
            $sth=$DB->prepare("$requeteS");
            $sth->execute();
            my @siret = $sth->fetchrow_array;
            $form_val{'clsiren'} = "ET0 000 000 " . sprintf("%05d", (substr($siret[0], -5) + 1));
        }
    }

    # V�rifications du SIRET sauf pour les collectifs 4129999 en France
    if ($countryId ne "FR" || $form_val{'clcollectif'} ne "4129999")
    {
        # V�rification SIRET vide
        my $siret = $form_val{'clsiren'};
        if ($siret eq "")
        {
           print qq{<span style="color: red; font-weight: bold;">$mess[3036]</span>};
           return;
        }

        # V�rification du formattage du SIRET
        my $siretok = 1;
        if ($countryId eq "FR" && !($siret =~ /(PA|ET|[0-9]{2})[0-9]\s[0-9]{3}\s[0-9]{3}\s[0-9]{5}/))
        {
            $siretok = 0;
        }
        elsif ($countryId eq "ES" && (length($siret) > 17 || length($siret) < 9))
        {
            $siretok = 0;
        }
        elsif ($countryId eq "PT" && length($siret) > 17)
        {
            $siretok = 0;
        }
        elsif ($countryId eq "IT" && length($siret) == 0)
        {
            $siretok = 0;
        }
        elsif ($countryId eq "MA" && length($siret) == 0)
        {
            $siretok = 0;
        }

        # Message d'erreur
        if ($siretok == 0)
        {
           print qq{<span style="color: red; font-weight: bold;">$mess[3037]</span>};
           return;
        }

        # V�rification de l'existence du (SIRET + SIRET Compl�mentaire)
        $siretok = 1;
        $st = $DB->prepare("SELECT CLSIREN FROM CLIENT WHERE CLSIREN = '" . $form_val{'clsiren'} . "'
            AND CLSIRETCPL = UPPER(TRIM('" . $form_val{'clsiretcpl'} . "')) AND CLAUTO != '" . $form_val{'clauto'} . "'");
        $st->execute();
        my $rows = $st->rows;
        $st->finish;

        # SIRET d�j� utilis�
        if ($rows != 0)
        {
            $siretok = 0;
        }

        # Message d'erreur
        if ($siretok == 0)
        {
           print qq{<span style="color: red; font-weight: bold;">$mess[3038]</span>};
           return;
        }
    }

    # Code NAF
    my $clape;
    if ($form_val{'clape'} eq "")
    {
        $clape = $form_val{'clape1'};
    }
    else
    {
        $clape = $form_val{'clape'};
    }

    # Envoi du mail de blocage/d�blocage
    &email_blocke( $form_val{'clauto'}, $form_val{'clcode'}, $form_val{'clste'}, $form_val{'clsage'} );
    
    # Recuperation de l'ancien �tat de clbonsoblig pour la DE 7644
    # R�cup�ration de l'ancien code client et de l'ancien CLCEE (DE 10121)
    # R�cup�ration de l'ancien �tat CLPROFORMA (DE-10569)
    my $reqBons = "SELECT CLCODE, CLBONSOBLIG, BCJOIN, CLCEE, CLPROFORMA
                    FROM CLIENT
                    WHERE CLAUTO = '".$form_val{'clauto'}."'";
    my $sth=$DB->prepare($reqBons);
    $sth->execute();
    my ($oldClcode, $oldClbonsoblig, $oldBCJoin, $oldClcee, $oldClproforma) = $sth->fetchrow_array();
    $sth->finish;    

    # Date de validit� de l'assurance
    my $cldatevalass = ($form_val{'cldatevalass0'} eq '00' || $form_val{'cldatevalass0'} eq '' ?
                            undef :
                            sprintf('%04d-%02d-%02d', $form_val{'cldatevalass2'}, $form_val{'cldatevalass1'}, $form_val{'cldatevalass0'}));
    # Date d'application de l'assurance souhait�e
    my $cldatedebass = ($form_val{'cldatedebass0'} eq '00' || $form_val{'cldatedebass0'} eq '' ?
                            undef :
                            sprintf('%04d-%02d-%02d', $form_val{'cldatedebass2'}, $form_val{'cldatedebass1'}, $form_val{'cldatedebass0'}));
    # Date d'application de la gestion de recours souhait�e
    my $cldatedebtyperecours = ($form_val{'cldatedebtyperecours0'} eq '00' || $form_val{'cldatedebtyperecours0'} eq '' ?
                                    undef :
                                    sprintf('%04d-%02d-%02d', $form_val{'cldatedebtyperecours2'}, $form_val{'cldatedebtyperecours1'}, $form_val{'cldatedebtyperecours0'}));

    # Type de gestion de recours
    my $tabInsuranceInfos;
    if ($form_val{'clnvassurance'})
    {
        $tabInsuranceInfos = &LOC::Insurance::Type::getInfos($countryId, $form_val{'clnvassurance'});
    }
    else
    {
        $tabInsuranceInfos = &LOC::Insurance::Type::getInfos($countryId, $form_val{'cltypeassurance'});
    }
    if (!$tabInsuranceInfos->{'isAppealActivated'})
    {
        $form_val{'clnvtyperecours'} = undef;
    }

    my $tabUpdates = {
        'CLAPE'                 => $DB->quote($clape),
        'CLPORTABLE'            => $DB->quote($form_val{'clportable'}),
        'CLCONTACT'             => $DB->quote($form_val{'clcontact'}),
        'CLLINO'                => $DB->quote($form_val{'cllino'}),
        'CLBANQUE'              => $DB->quote($form_val{'clbanque'}),
        'CLRIB'                 => $DB->quote($form_val{'clrib'}),
        'CLNBFACT'              => $DB->quote($form_val{'clnbfact'}),
        'CLCEE'                 => $form_val{'clcee'} ? $form_val{'clcee'} : 0,
        'CLFACTGRP'             => $form_val{'clfactgrp'} ? $form_val{'clfactgrp'} : 0,
        'METIERAUTO'            => $form_val{'metierauto'} ? $form_val{'metierauto'} : 0,
        'CLIENTTARIFAUTO'       => $form_val{'clienttarifauto'} ? $form_val{'clienttarifauto'} : 'NULL',
        'CLNUMCMDOBLI'          => $form_val{'clnumcmdobli'} ? $form_val{'clnumcmdobli'} : 0,
        'BCJOIN'                => $form_val{'bcjoin'} ? $form_val{'bcjoin'} : 0,
        'CLCGVENTE'             => $form_val{'clcgvente'} ? $form_val{'clcgvente'} : 0,
        'CLCADRE'               => $form_val{'clcadre'} ? $DB->quote($form_val{'clcadre'}) : 'NULL',
        'CLNETTOYAGE'           => $form_val{'clnettoyage'} ? $form_val{'clnettoyage'} : 0,
        'CLDECHETSFACT'         => $form_val{'cldechetsfact'} ? $form_val{'cldechetsfact'} : 0,
        'CLGROUPEAUTO'          => $form_val{'clgroupeauto'} ? $form_val{'clgroupeauto'} : 0,
        'CLCLASSE'              => $form_val{'clclasse'} ? $DB->quote($form_val{'clclasse'}) : 'NULL',
        'CLPOTENTIEL'           => $form_val{'clpotentiel'} ? $DB->quote($form_val{'clpotentiel'}) : 'NULL',
        'CLQUALIFICATION'       => $DB->quote($form_val{'clqualification'}),
        'CCAUTO'                => $form_val{'ccauto'} ? $form_val{'ccauto'} : 0,
        'CLBONSOBLIG'           => $form_val{'clbonsoblig'} ? $form_val{'clbonsoblig'} : 0,
        'CLJOINDOCMACH'         => $form_val{'cljoindocmach'} ? $form_val{'cljoindocmach'} : 0
    };

    my @tabFields;
    foreach my $fieldName (keys(%$tabUpdates))
    {
        push(@tabFields, $fieldName . ' = ' . $tabUpdates->{$fieldName});
    }

    # Mise � jour des donn�es du client
    my $requete = 'UPDATE CLIENT SET';
    $requete .= '
' . join(', ', @tabFields);
    $requete .= '
    WHERE CLAUTO = ' .$form_val{'clauto'} . ';';

    $sth = $DB->prepare($requete);
    $result = $sth->execute or print "Error " . $DB->{'mysql_errno'} . ": " . $DB->{'mysql_error'};

        # Mise � jour des informations sur les factures non export�es
    my $category = ($form_val{'clcee'} == 2 ? 0 : $form_val{'clcee'});
    $requete = "UPDATE ENTETEFACTURE EF
LEFT JOIN LIGNEFACTURE LF ON EF.ENTETEAUTO = LF.ENTETEAUTO
SET EF.CLCODE = '" . $form_val{'clcode'} . "',
EF.ENTETECATEGORIE = '" . $category . "',
EF.ENTETECOLLECTIF = '" . $form_val{'clcollectif'} . "',
LF.LIGNETIERS = '" . $form_val{'clcode'} . "'
WHERE EF.CLCODE = '". $oldClcode ."'
    AND EF.ETCODE = 'FAC01'";

    $sth = $DB->prepare($requete);
    $result = $sth->execute or print "Error " . $DB->{'mysql_errno'} . ": " . $DB->{'mysql_error'};

    # Changement au niveau du CLCEE (DE 10121)
    if ($oldClcee != $form_val{'clcee'})
    {
        print '<div style="font-weight: bold; color: #FF8800">' . $mess[3049] . '</div>';
    }

    #DE9265 : Liste des BC � fournir
    if ($oldBCJoin == 0 && $form_val{'bcjoin'} == -1)
    {
        # pour tous les contrats du client (contratok � -1) on met le BCENVOI � -1 (BC � fournir OK) = initialisation
        my $requeteBCRecu = "
UPDATE CONTRAT SET BCENVOI = -1, BCRECU = 0
WHERE CLAUTO = '" . $form_val{'clauto'} . "'
AND CONTRATOK= -1";
       $sth = $DB->prepare($requeteBCRecu);
       $sth->execute;

       # pour les contrats non compl�tement factur�s et qui ne sont pas en �tat annul�, on met le BCENVOI � 0.
        my $requeteContrat ="
SELECT
    CONTRATAUTO,
    AGAUTO
FROM CONTRAT
WHERE CLAUTO ='" . $form_val{'clauto'} . "'
AND ETATFACTURE != 'F'
AND CONTRATOK= -1 
AND ETCODE <> 'CON06'
AND CONTRATDR> '2004-01-01'";

        $sth = $DB->prepare($requeteContrat);
        $sth->execute;

        while (@resultContat = $sth->fetchrow_array())
        {
            $requeteUpdate ="
UPDATE CONTRAT SET BCENVOI = 0, BCRECU = 0
WHERE CLAUTO = '" . $form_val{'clauto'} . "'
AND CONTRATAUTO ='" . $resultContat[0] . "'";
            $sUP = $DB->prepare($requeteUpdate);
            $sUP->execute or print "Error " . $DB->{'mysql_errno'} . ": " . $DB->{'mysql_error'};
            alerte_pl_ms($resultContat[1],$DB,LOC::Alert::ORDERCOMPULSORYTOPROVIDE,+1);
        }
        
    }
    # Mise � jour de l'alerte des BC � envoyer si on d�coche "BC � fournir"
    if ($oldBCJoin == -1 && $form_val{'bcjoin'} == 0)
    {
        my $requeteContrat ="
SELECT
    count(*),
    AGAUTO
FROM CONTRAT
WHERE CLAUTO ='" . $form_val{'clauto'} . "'
AND ETATFACTURE != 'F'
AND CONTRATOK= -1 
AND ETCODE <> 'CON06'
AND CONTRATDR> '2004-01-01'
GROUP BY AGAUTO";
        $sth = $DB->prepare($requeteContrat);
        $sth->execute;

        while (@resultContat = $sth->fetchrow_array())
        {
            alerte_pl_ms($resultContat[1],$DB,LOC::Alert::ORDERCOMPULSORYTOPROVIDE,-$resultContat[0]);
        }
    }

    # Mise � jour des informations du client avec la nouvelle m�thode
    # Prend en compte :
    # - si la case "Paiement avant livraison" est d�coch�e
    # - adresse e-mail
    my $userId     = $G_PEAUTO;
    my $customerId = $form_val{'clauto'};
    my $tabData    = {
        'code'               => &LOC::Util::latin1ToUtf8(&LOC::Util::trim($form_val{'clcode'})),        # CLCODE
        'name'               => &LOC::Util::latin1ToUtf8(uc($form_val{'clste'})),  # CLSTE
        'address1'           => &LOC::Util::latin1ToUtf8(&LOC::Util::trim($form_val{'cladresse'})),     # CLADRESSE
        'address2'           => &LOC::Util::latin1ToUtf8(&LOC::Util::trim($form_val{'cladresse2'})),    # CLADRESSE2
        'locality.id'        => $form_val{'viclauto'},                        # VICLAUTO
        'telephone'          => &LOC::Util::trim($form_val{'cltel'}),         # CLTEL
        'fax'                => &LOC::Util::trim($form_val{'clfax'}),         # CLFAX
        'statCode'           => &LOC::Util::trim($form_val{'clsiren'}),       # CLSIREN
        'statCodeCpl'        => &LOC::Util::trim($form_val{'clsiretcpl'}),    # CLSIRETCPL
        'isProformaRequired' => ($form_val{'clproforma'} != 0 ? 1 : 0),       # CLPROFORMA
        'email'              => &LOC::Util::latin1ToUtf8(&LOC::Util::trim($form_val{'clemail'})),       # CLEMAIL
        'accountingCode'     => &LOC::Util::trim($form_val{'clcollectif'}),   # CLCOLLECTIF
        'isLocked'           => ($form_val{'clsage'} == -2 ? 1 : 0),          # CLSAGE
        'paymentMode.id'     => $form_val{'mopaauto'},                        # MOPAAUTO
        'comments'           => &LOC::Util::latin1ToUtf8(&LOC::Util::trim($form_val{'clcommentaire'})),  # CLCOMMENTAIRE
        'wishedInsuranceType.id' => $form_val{'clnvassurance'},   # CLNVASSURANCE
        'insurance.wishedRate'   => $form_val{'clnvpassurance'},  # CLNVPASSURANCE
        'insurance.validityDate' => $cldatevalass,  # CLDATEVALASS
        'insurance.beginDate'    => $cldatedebass,  # CLDATEDEBASS
        'wishedAppealType.id'    => $form_val{'clnvtyperecours'}, # CLNVTYPERECOURS
        'appeal.beginDate'       => $cldatedebtyperecours   # CLDATEDEBTYPERECOURS
    };

    &LOC::Customer::update($countryId, $customerId, $tabData, $userId, {'caller' => basename($0)}, undef, undef);


    # Message de confirmation
    if ($result)
    {
        print "<b><span style='color: #00CC33'>$mess[3000] $mess[753]</span></b>";

        # DE 7644 : si on change l'�tat d'un client en espagne sur le champ 'bons sign�s obligatoires', maj alerte 28
        if ($countryId eq "ES" && $oldClbonsoblig ne $form_val{'clbonsoblig'})
        {
            my $multiple = -1;
            if ($form_val{'clbonsoblig'} eq "-1")
            {
                $multiple = 1;
            }
            
            my $queryNb = qq{SELECT CONTRAT.AGAUTO,COUNT(*) FROM CONTRAT
            LEFT JOIN CLIENT ON CLIENT.CLAUTO=CONTRAT.CLAUTO
            WHERE
            LIETCONTRATAUTO in (2,3,7) AND
            CONTRATOK="-1" AND
            CLIENT.CLAUTO = "} . $form_val{'clauto'} . qq{" AND
            ETATFACTURE != "F"
            GROUP BY CONTRAT.AGAUTO};
            my $sth = $DB->prepare($queryNb);
            $sth->execute();
            while (my @data = $sth->fetchrow_array())
            {
                alerte_pl_ms($data[0],$DB,LOC::Alert::DELIVERYRECOVERYNOTESTOSIGN,$data[0] * $multiple);
            }
            $sth->finish;
        }

        # R�cup�ration des infos du client
        &getCustomerInfos($customerId);
    }
}

# Email Client Blocke
sub email_blocke {
    my ($clauto,$cc,$ste,$etat)=@_;
    my $req=qq{select VICLCP, VICLNOM, TARIFCLIENTREF, CLSAGE FROM CLIENT LEFT join AUTH.VICL ON VICL.VICLAUTO=CLIENT.VICLAUTO LEFT join CLIENTTARIF ON CLIENTTARIF.CLIENTTARIFAUTO=CLIENT.CLIENTTARIFAUTO WHERE CLAUTO="$clauto"};
    my $sth=$DB->prepare($req);
    $sth->execute();
    my ($cp,$ville,$ct, $etat2)=$sth->fetchrow_array();
    $sth->finish;
    if ( $etat eq "" ) { $etat = 0; }
    if( $etat ne $etat2 )
    {
        my @tabMail = split(',', &LOC::Characteristic::getCountryValueByCode('STABLOCAGE', $G_PAYS));
        
        my $libbloc = $mess[165];
    
        if ($etat ne "-2")
        {
            $libbloc= $mess[166];
        }
        # Objet du mail
        my $subj = $mess[167];
        $subj =~ s/<ste>/$ste/;
        $subj =~ s/<code>/$ct/;
        $subj =~ s/<libbloc>/$libbloc/;        
        # corps du mail
        my $message = $mess[168];
        $message =~ s/<ste>/$ste/;
        $message =~ s/<code>/$ct/;
        $message =~ s/<cp>/$cp/;
        $message =~ s/<ville>/$ville/;
        $message =~ s/<libbloc>/$libbloc/;        
        my $pemail=$G_PEMAIL;

        if ($G_ENVIR ne '')
        {
            my $prefix = uc($G_ENVIR);
            $prefix =~ s/ //;
            $subj = '[' . $prefix . ']' . $subj;
            $message = "Destinataires d'origine : " . &LOC::Characteristic::getCountryValueByCode('STABLOCAGE', $G_PAYS) . "\n\n" . $message;
            &email($pemail,'dev@acces-industrie.com',$subj, $message);
        }
        else
        {
            foreach my $email (@tabMail)
            {
                &email($pemail, $email, $subj, $message);
            }
        }
    }
}

sub auth_bloc {
    my $blocage = "style='visibility:hidden'";
    for(my $k = 0; $k < @pe_blc; $k++) {
        if ($G_PEAUTO eq $pe_blc[$k]) {
            $blocage = "style";
        }
    }
    return $blocage;
}

sub isEditAuthorized {
    return &LOC::Session::isAccessAuthorized('accounting', 'customer', 'edit');
}

sub modif_commerce_ES {
# DE 8023 : Interface de modification des infos client pour les assistantes et les commerciaux d'Espagne     
  my @param=@_;
    my $req=qq{select MEAUTO FROM AUTH.`MEPR` WHERE PROFILAUTO="$G_PROFIL" AND MEAUTO = 172 AND "$G_PAYS"='ES';};
    my $stcont=$DB->prepare($req);
    $stcont->execute();
    my $rows=$stcont->rows;
    $stcont->finish;
     return $rows;
}

sub utilise_participation_recyclage
{
    return &LOC::Characteristic::getAgencyValueByCode('DECHETSMODE', $G_AGENCE);
}

sub getCustomerInfos {
    my ($customerId) = @_;

    # R�cup�ration des informations du client
    $requete = "
SELECT 
    C.CLAUTO,
    C.CLCODE,
    C.CLAPE,
    C.CLSIREN,
    C.CLSTE,
    C.CLADRESSE,
    C.CLADRESSE2,
    V.VICLCP, 
    V.VICLNOM,
    V.VICLPAYS,
    C.VICLAUTO,
    C.CLTEL, 
    C.CLFAX, 
    C.CLPORTABLE,
    C.CLEMAIL, 
    C.CLCONTACT,
    C.CLCOLLECTIF,
    C.CLLINO, 
    C.CLBANQUE,
    C.MOPAAUTO,
    C.CLRIB,
    C.CLNBFACT,
    C.CLCEE,
    C.CLFACTGRP,
    C.CLCOMMENTAIRE,
    C.CLIENTTARIFAUTO,
    C.CLSAGE,
    C.CLTYPEASSURANCE,
    C.CLNUMCMDOBLI,
    C.BCJOIN,
    C.CLPROFORMA,
    C.METIERAUTO,
    C.CLCLASSE,
    C.CLGROUPEAUTO,
    C.CLCGVENTE,
    C.CLNETTOYAGE,
    C.CLPASSURANCE,
    C.CLDATEVALASS,
    C.CLDATEDEBASS,
    C.CLPOTENTIEL,
    CCAUTO, 
    C.CLSIRETCPL,
    C.CLBONSOBLIG,
    C.CLDECHETSFACT,
    C.CLQUALIFICATION,
    C.CLJOINDOCMACH,
    C.CLNVASSURANCE,
    C.CLNVPASSURANCE,
    C.CLTYPERECOURS,
    C.CLNVTYPERECOURS,
    C.CLDATEDEBTYPERECOURS
FROM CLIENT C
LEFT JOIN AUTH.VICL V ON V.VICLAUTO = C.VICLAUTO
LEFT JOIN MODEPAIE M ON M.MOPAAUTO = C.MOPAAUTO
WHERE C.CLAUTO = '". $customerId ."'";

    $sth = $DB->prepare( $requete );
    $sth->execute;
    my @data = $sth->fetchrow_array;
    $form_val{'clauto'} = $data[0];          $form_val{'clcode'} = $data[1];
    $form_val{'clape'} = $data[2];           $form_val{'clsiren'} = $data[3]; 
    $form_val{'clste'} = $data[4];           $form_val{'cladresse'} = $data[5]; 
    $form_val{'cladresse2'} = $data[6];     $form_val{'viclcp'} = $data[7];
    $form_val{'viclnom'} = $data[8];           $form_val{'viclpays'} = $data[9];
    $form_val{'viclauto'} = $data[10];         $form_val{'cltel'} = $data[11];
    $form_val{'clfax'} = $data[12];         $form_val{'clportable'} = $data[13];
    $form_val{'clemail'} = $data[14];         $form_val{'clcontact'} = $data[15];
    $form_val{'clcollectif'} = $data[16];   $form_val{'cllino'} = $data[17];
    $form_val{'clbanque'} = $data[18];        $form_val{'mopaauto'} = $data[19];
    $form_val{'clrib'} = $data[20];         $form_val{'clnbfact'} = $data[21];
    $form_val{'clcee'} = $data[22];         $form_val{'clfactgrp'} = $data[23];
    $form_val{'clcommentaire'} = $data[24]; $form_val{'clienttarifauto'} = $data[25];
    $form_val{'clsage'} = $data[26];         $form_val{'cltypeassurance'} = $data[27];
    $form_val{'clnumcmdobli'} = $data[28];  $form_val{'bcjoin'} = $data[29];
    $form_val{'clproforma'} = $data[30];     $form_val{'metierauto'} = $data[31];
    $form_val{'clclasse'} = $data[32];        $form_val{'clgroupeauto'} = $data[33];
    $form_val{'clcgvente'} = $data[34];        $form_val{'clnettoyage'} = $data[35];
    $form_val{'clpassurance'} = $data[36];  $form_val{'cldatevalass'} = $data[37];
    $form_val{'cldatedebass'} = $data[38];  $form_val{'clpotentiel'} = $data[39];
    $form_val{'ccauto'} = $data[40];        $form_val{'clape1'}=substr($data[2], 0, 4);
    $form_val{'clsiretcpl'} = $data[41];    $form_val{'clbonsoblig'} = $data[42];
    # Assurance souhait�e
    $form_val{'clnvassurance'}  = $data[46];
    $form_val{'clnvpassurance'} = $data[47];

    # Gestion de recours
    $form_val{'cltyperecours'}        = $data[48];
    $form_val{'clnvtyperecours'}      = $data[49];
    $form_val{'cldatedebtyperecours'} = $data[50];

    $utiliseGestionDechets = &utilise_participation_recyclage();
    if ($utiliseGestionDechets)
    {
        $form_val{'cldechetsfact'} = $data[43];
    }
    $form_val{'clqualification'} = $data[44];
    $form_val{'cljoindocmach'} = $data[45];
}


#########################
# Fonctions perl => FIN #
#########################

########################
# Fonctions Javascript #
########################

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);
# Pied de la page HTML
print $cgi->end_form();
$DB->disconnect;
&Pied($delta_time, $nomfich);
