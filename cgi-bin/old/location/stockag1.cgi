#!/usr/bin/perl

# $Id: stockag1.cgi,v 1.1.1.1 2006/08/01 07:48:09 clliot Exp $
# $Log: stockag1.cgi,v $
# Revision 1.1.1.1  2006/08/01 07:48:09  clliot
# no message
#
# Revision 2.4  2006/02/06 16:52:50  julien
# V. 1.4 : Tarification v2
#
# Revision 2.3.2.1  2005/12/13 15:46:46  julien
# no message
#
# Revision 2.3.4.1  2005/12/13 10:31:58  julien
# Inclusion du r�pertoire lib dans chaque fichier.
#
# Revision 2.3  2005/08/26 12:10:59  julien
# Pr�fixage des tables de AUTH dans les requ�tes.
#
# Revision 2.2  2005/05/03 16:57:12  julien
# Tra�age du temps d'ex�cution des scripts.
#
# Revision 2.1  2005/05/03 14:41:48  julien
# Tra�age du temps d'ex�cution des scripts.
#
# Revision 2.0  2004/09/16 15:07:36  julien
# Version r�cup�r�e de Lancelot et nettoy�e
#
# Revision 1.2  2004/09/16 14:23:15  julien
# Ajout des identifiants
#

use lib '../lib';

use strict;
use CGI;
use DBI;
use autentif;
use accesbd;
use varglob;
use tracetemps;

# Initialisation du chrono
my $timer = &init_chrono();


my $cgi = CGI::new();

use Date::Calc qw(Day_of_Week  Delta_Days Add_Delta_Days);
$|=1;
#my @pays=qw(FR ES);
my @ag=();
#connexion
#my $DBF=&Environnement("LOCATION",$pays[0]);
#my $DBE=&Environnement("LOCATION",$pays[1]);
my $DBF=&Environnement("LOCATION");


#entete
my $titre=qq{STOCK MACHINE};
&ente($titre, $titre, $G_PENOM, $G_PEPRENOM);
#if (!(&verif_droit)) {print "Je n ai pas le droit\n"};

my $reqag=qq{select AGAUTO FROM AUTH.`AGENCE` ORDER BY AGAUTO};
my $sth=$DBF->prepare($reqag);
$sth->execute();

while(my @data=$sth->fetchrow_array)
	{
	push @ag , $data[0];
}

$sth->finish;

my $nbag=@ag;
my %stockd;
my %stockr;
my %stockra;
my %stockt;
my %stag;
my @libmach=();
my @momaauto=();
#
#Modele
#

my $reqag=qq{select MOMAAUTO,MOMADESIGNATION FROM AUTH.`MODELEMACHINE` ORDER BY MOMADESIGNATION};
$sth=$DBF->prepare($reqag);
$sth->execute();
while(my @data=$sth->fetchrow_array)
	{
	for(my $i=0;$i<@ag;$i++)
		{
		$stockd{$data[0]}->{$ag[$i]}=0;
		$stockr{$data[0]}->{$ag[$i]}=0;
		$stockra{$data[0]}->{$ag[$i]}=0;
		$stockt{$data[0]}->{$ag[$i]}=0;		
		$stag{$ag[$i]}=0;
		}
	push @libmach , $data[1];
	push @momaauto, $data[0];
	}
$sth->finish;




my $requete=qq{select  MOMADESIGNATION,STOCKMODELE.AGAUTO,STMORESERVABLE,STMORESERVE,STMOTOTAL,STMODIS,STOCKMODELE.MOMAAUTO FROM AUTH.`STOCKMODELE` LEFT JOIN  AUTH.`MODELEMACHINE` ON STOCKMODELE.MOMAAUTO=MODELEMACHINE .MOMAAUTO ORDER BY MOMADESIGNATION};

#	
#  Stock 
#

my $rq=$requete;

$sth=$DBF->prepare($rq);
$sth->execute();
while(my @data=$sth->fetchrow_array)
	{
	for(my $i=0;$i<@ag;$i++)
		{
		$stockd{$data[6]}->{$data[1]}=$data[5];
		$stockr{$data[6]}->{$data[1]}=$data[3];
		$stockra{$data[6]}->{$data[1]}=$data[2];
		$stockt{$data[6]}->{$data[1]}=$data[4];		
		}
	$stag{$data[1]} +=$data[4];
	}
$sth->finish;


$DBF->disconnect;

print qq|<table width="100%">\n|;
my $total=0;
my $lign=0;	
for(my $modu=0;$modu<@momaauto;$modu++)
	{
	my $modele=$momaauto[$modu];	 
	my $mod=$lign % 20;
	if ($mod ==0)
		{
		my $nbc=$nbag+2;	
		print qq{<tr><TD align="center" colspan="$nbc" ><font class="GD"><b>Disp / Total</b></font></TD></tr>\n};
		print qq{<tr bgcolor="C6CDC5">\n};
		print qq|<td>&nbsp;</td>\n|;	
			
		for(my $k=0;$k<@ag;$k++)
			{
			
			print qq|<td align="center"><b>$ag[$k]</b></td>\n|;		
			}
			
		print qq|<td align="center"><b>TOTAL</b></td>\n|;		
		print qq{</tr>\n};
		}
	my $color;
	my $color2=qq{bgColor=''};
	if ($lign % 2 == 0)
    	{
      	$color = "";
    	}
    else
    	{
      	$color2=qq{bgColor='#D6D6D6'};
 		 $color=qq{ bgcolor='#D6D6D6'};
    	}
	print qq{<tr$color onmouseover="bgColor='#FFCC33'" onmouseout="$color2">\n};
	 my $tot=0;
	print qq|<td nowrap><font><b>$libmach[$modu]</b></font></td>\n|;		
	for(my $i=0;$i<@ag;$i++)
		{ 		 				
		if ($stockt{$modele}->{$ag[$i]} ==0)
			{
			print qq|<td nowrap align="center"><font class="PT">&nbsp;</font></td>\n|;
			}
		else
			{
					
		print qq|<td nowrap align="center"><font class="PT">$stockd{$modele}->{$ag[$i]} / <b>$stockt{$modele}->{$ag[$i]}</font></b></td>\n|;	
				
				}
			 $tot +=$stockt{$modele}->{$ag[$i]};
			
		}
	$total +=$tot;	
	print qq|<td><b>$tot<b></td>\n|;		
	print qq{</tr>\n};
	$lign++;	
	}
print qq{<tr bgcolor="C6CDC5">\n};
print qq|<td><b>TOTAL</b></td>\n|;	
for(my $j=0;$j<@ag;$j++)
			{
			print qq|<td nowrap align="center"><b><font>$stag{$ag[$j]}</font></b></td>\n|;				
				}
print qq|<td><b>$total<b></td>\n|;	
print qq{</tr>\n};
print qq{</table>\n};

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

# Pied de la page HTML
&Pied($delta_time);


