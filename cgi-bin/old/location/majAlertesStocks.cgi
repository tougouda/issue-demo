#!/usr/bin/perl

use lib '../lib';

use strict;
use CGI;
use accesbd;
use varglob;
use message;
use fonctions;
use autentif;
use tracetemps;

# Initialisation du chrono
my $timer = &init_chrono();

#
# Environnement
#
my $DB = &Environnement("LOCATION");
my $cgi = new CGI;
$cgi->autoEscape(undef);
my @mess = &message($G_PAYS);
$|=1;


my $agencyId  = $cgi->param('agencyId');
my $countryId = $cgi->param('countryId');
if (!$agencyId || !$countryId)
{
    # En-t�te de la page HTML
    my $titre = $mess[7600];
    &ente($titre, $titre, $G_PENOM, $G_PEPRENOM);
    print $cgi->start_form(
                        -name   => 'frm',
                        -method => 'get');
    #
    if ($cgi->param("valid.x"))
    {
        &maj_stock($G_AGENCE);
        print '<br /><br />';
        &cont_al($G_PAYS, $G_AGENCE);
    }
    # 
    else
    {
    	print "\n";
    	print "<table>";
    	print "<tr><td>";
    	my $lib = sprintf($mess[7601],$G_AGENCE);
        print qq{<font style="color: black; font-weight: bold;">$lib};
    	print "<td>";
        print $cgi->image_button(
                            -name   => "valid",
                            -alt    => $mess[7600],
                            -title  => $mess[7600],
                            -src    => "$URL_IMG/recalk_off.gif",
                            -border => 0,
                            -align  => "left");
    	print "</tr></td></td>";
        print qq{</font>};
    }
    
    print $cgi->end_form();

    # Arr�t du chrono
    my $delta_time = &stop_chrono($timer);

    # Pied de la page HTML
    my $paramOrigin = $cgi->param("origine");
    &Pied($delta_time, $paramOrigin);
}
else
{
    print "Content-Type: text/html; charset=iso-8859-15\n\n";
    print '
    <html>
        <body style="background:#000; font-family: \'Courier new\'; color:#ccc; font-size: 8pt; font-weight: bold;">
    ';
    &maj_stock($agencyId);
    print '<br /><br />';
    &cont_al($countryId, $agencyId);
    print '
        </body>
    </html>
    ';
}



