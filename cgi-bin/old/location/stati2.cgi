#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';

use CGI;
use autentif;
use calculdate;
use accesbd;
use varglob;
use Date::Calc qw(Delta_Days Days_in_Month Add_Delta_Days);
use message;
use tracetemps;

use JSON::XS;

use LOC::Date;
use LOC::Insurance::Type;
use LOC::Common::Rent;

# Initialisation du chrono
my $timer = &init_chrono();

# Connexion
my $DB = &Environnement("STATS");
&verif_droit();


# R�cup�ration des formats Sage (quantit�, prix et montant)
my $charac;
my $req = '
SELECT IFNULL(ccy_value, cgl_value)
FROM AUTH.p_characteristic
LEFT JOIN AUTH.p_characteristic_global ON chr_id = cgl_chr_id
LEFT JOIN AUTH.p_characteristic_country ON (cgl_chr_id = ccy_chr_id AND ccy_cty_id = "' . $G_PAYS . '")
WHERE chr_code = "FORMATSSAGE"';

my $sth = $DB->prepare($req);
$sth->execute();

while (my @data = $sth->fetchrow_array())
{
    $charac = $data[0];
}

$sth->finish();

my $tabSageFormats = {'quantity' => 2, 'price' => 4, 'amount' => 2};
if ($charac ne '')
{
    $tabSageFormats = JSON::XS->new->decode($charac);
}

# Initialisation CGI
my $cgi = new CGI;
$cgi->autoEscape(undef);
my @mess = &message($G_PAYS);
$|=1;
my $coutatotal = 0;
my $date   = $cgi->param('date');
my $modl   = $cgi->param('modele');
my ($deb, $dbaff, $fin, $fnaff, $dtjour, $affdte, $mimois, $tsdt) = &datemois;
my $day = $mimois;
my @tdt = qw(2002 03 23);

#recuperation taux de change 
my $datefinmois = $mimois;
$datefinmois =~ s/\:/\-/g;
my $tauxE = tauxLocalToEuro($G_PAYS, $datefinmois);

# init variable
# donn�e de la table des famille commerciale
my %mach_mod;
my %mach_com;
my %cout_mod;
my %cout_com;
my $nbjrmoy = 21;
my $coef    = 0.65; # 0.7;
my %comm;
my @ordcomm    = ();
my @ordcommtot = ();
my %fmcomm;
my %htcomm;
my $caloc  = 0;
my $agence = $G_AGENCE;
my $titre = $mess[3500];
my $date_chgt_fact = &retour_date($G_PAYS);

if ($modl ne "") {$titre .= " ".$cgi->param("aff");}

&ente($titre, $titre, $G_PENOM, $G_PEPRENOM);

# Message d'attente
print qq{<div id="attente" style="visibility: hidden; position:absolute; width:400px; height:200px;">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr valign="middle">
		<td align="center">
		<table cellpadding="1" cellspacing="0" border="0">
			<tr>
				<td bgcolor=black>
				<table border="0" width="100%" cellpadding="5" cellspacing="0">
					<tr valign="middle">
						<td bgcolor="#FFFFCC" align="center">
						<font class="PT2">
							Veuillez patienter pendant le chargement des statistiques...<br>
							Merci de ne pas cliquer sur le bouton <i>Actualiser</i> et de ne pas appuyer sur F5.<p>
							<img src="$URL_IMG/sablier.gif" border="0">
						</font>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</div>

<script>
var x = (document.body.clientWidth - 400) / 2;
var y = (document.body.clientHeight - 200) / 2;
attente.style.left = x + "px";
attente.style.top = y + "px";
attente.style.visibility = 'visible';
attente.style.cursor = 'wait';
</script>};

print "<br><br>";
&nb_mach($mimois);

my $ctaux = &cout_taux($mimois);
my @rest = ();
my @resc = ();

#
# Programme Principal
#
my $jfr = qq{&nbsp;};

if ($modl eq "") {$jfr = &jourdate;}
if ($tsdt) {&hissstatistique;}
else {&statistique;}

&entete;
print qq{
	<tr>
		<td colspan=18><font class="PT">&nbsp;</font></td>
	</tr>
};
&corps;
print qq{
</table>

<script>
attente.style.visibility='hidden';
attente.style.cursor = 'default';
</script>
};
$DB->disconnect;

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

# Pied de la page HTML
&Pied($delta_time);


# ex : casse('2003:5:31')
# Somme les casses accept� en facturation et pass�es en facturation du mois en cours
# ne marche que depuis la mise en place de CADATEFACT donc ant�rieur fauss� (>=01/05/2003)
sub casse
{
    my ($date) = @_;

    my $req;
    my $result = 0;

    # Casse
    $req = '
SELECT SUM(CAPXFINALEU)
FROM CASSE
LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO = CASSE.CONTRATAUTO
WHERE CADATEFACT IS NOT NULL
    AND CONTRAT.AGAUTO = "' . $G_AGENCE . '"
    AND YEAR(CADATEFACT) = YEAR("' . $date . '")
    AND MONTH(CADATEFACT) = MONTH("' . $date . '")';

    my $sth = $DB->prepare($req);
    $sth->execute();
    my @data = $sth->fetchrow_array();

    $data[0] = ($data[0] eq "" ? 0 : $data[0]);
    $result += $data[0];

    $sth->finish();

    # Factures de remise en �tat
    $req = '
SELECT SUM(rpi_total)
FROM f_repairinvoice
WHERE rpi_agc_id = "' . $G_AGENCE . '"
    AND YEAR(rpi_date) = YEAR("' . $date . '")
    AND MONTH(rpi_date) = MONTH("' . $date . '")';

    $sth = $DB->prepare($req);
    $sth->execute();
    my @data = $sth->fetchrow_array();

    $data[0] = ($data[0] eq "" ? 0 : $data[0]);
    $result += $data[0];

    $sth->finish();

    return $result;
}

# ex : test_date('2003:5:31')
# Renvoie la diff�rence entre la date de fin de mois et une date pallier (d�e certainement � un seuil au niveau informatisation agence)
# et renseigne $day de la date du jour si la date du jour est strictement inf�rieure � la date du mois
# avant pas d'historique stat mod�le
sub test_date {
	my @param = @_;
	my @dt = split(/\:/, $param[0]);
	my $diff1 = Delta_Days(1900, 1, 1, $tdt[0], $tdt[1], $tdt[2]);
	my $diff2 = Delta_Days(1900, 1, 1, $dt[0], $dt[1], $dt[2]);
	my ($sec, $min, $hr, $jour, $mois, $an, $joursm, $jouran, $ete) = localtime;
	$an += 1900;
	$mois += 1;
	my $diff3 = Delta_Days(1900, 1, 1, $an, $mois, $jour);
	my $dt = $diff2 - $diff3;

	if ($dt>0) {$day = "$an:$mois:$jour";}

	my $diff = $diff2 - $diff1;
	return $diff;
}

# ex : cout_taux('2003:05:21')
# pr�vision �volution des taux
# en fonction des dates il faut garder
# la tra�abilit� des �volutions pour garder
# une bonne estimation
sub cout_taux {
	my ($dtt) = @_;
	my ($ant, $mst, $jrt) = split(/:/, $dtt);
	my @tx = ();
	my @dt = ();

	# ordonner de facon croissante de date et taux
	# en fonction des changements des taux
	$dt[0] = qq{13/01/2003};
	$tx[0] = 1.92;
	my $t = @tx;
	$t--;
	my $tx = $tx[$t];

	for (my $i=$t;$i>=0;$i--) {
		my ($jrc, $msc, $anc) = split(/\//, $dt[$i]);
		my $diff1 = Delta_Days(1900, 1, 1, $ant, $mst, $jrt);
		my $diff2 = Delta_Days(1900, 1, 1, $anc, $msc, $jrc);
		my $diff = $diff1 - $diff2;

		if ($diff <= 0) {$tx = $tx[$i];}
		else {next;}
	}

	return $tx;
}

# ex : datemois()
# Renvoie sous 2 formats diff.
# la date de d�but mois
# la date de fin mois
# ("2003:05:01","01/05/2003","2003:05:31","31/05/2003","2003:05:21","21/05/2003","2003:05:31 ou date pass�e en param�tre","0 ou 1")
sub datemois {
	my ($sec,$min,$hr,$jour,$mois,$an,$joursm,$jouran,$ete) = localtime;
	
    $an += 1900;
	$mois += 1;
	my $debutms = "$an:$mois:01";
	my $finmois = Days_in_Month($an, $mois);
	my $tsdate = 0;
	my $mmois = "$an:$mois:$finmois";

	if ($date) {
		my ($tsan, $tsms, $tsjr) = split(/\:/, $date);

		if (($an ne $tsan) || ($mois ne $tsms) || ($jour ne $tsjr)) {
	    	$tsdate = 1;
			$mmois = "$tsan:$tsms:$tsjr";
		}
	}

	my $finms = "$an:$mois:$finmois";
	my $dt = "$an:$mois:$jour";
	my $dtaff = "$jour/$mois/$an";
	my $debaff = "01/$mois/$an";
	my $finaff = "$finmois/$mois/$an";

	return ($debutms, $debaff, $finms, $finaff, $dt, $dtaff, $mmois, $tsdate);
}

# R�cup�re l'ensemble des donn�es concernant tous les contrats en cours arr�t�s ou archiv�s entre 2 dates donn�es
# Avec calcul des diverses sommes et taux d'utilisation et de rotation des mac
# Retourne nb contrat, CA, CA transport, CA carbu, nb dur�e -, nb dur�e +, nb contrat en cours, nb contrat arr�t�, nb assurance comprise, nb tarif mensuel, nb forfait, jourmsouv, dur�e totale
sub statjr {
	my @param   = @_;
	my $debutms = $param[0]; # Intervalle Date D�but
	my $finms   = $param[1]; # Intervalle Date Fin
	my $posi    = $param[2]; # Positionnement ds le tableau associatif d'affichage de comm
	my $requete;
	my $champ = "FACOMAUTO";
	my $where = "";

	if ($modl) {
		$champ = "MODELEMACHINE.MOMAAUTO";
		$where = qq| and FACOMAUTO="$modl"|;
	}

	$requete = qq{
	select CONTRATDD, CONTRATDR, CONTRATJOURSPLUS, CONTRATJOURSMOINS, MOPXJOURFR,
	MOTARIFMSFR, CONTRATTYPEASSURANCE, MOTRANSPORTDFR, MOTRANSPORTRFR, CAMONTANTFR,
	$champ, MOFORFAITFR, CONTRATDUREE, SOLOAUTO, LIETCONTRATAUTO,
	ETATFACTURE, MAHORSPARC, CONTRATPASSURANCE, ity_flags, CONTRATTYPERECOURS, CONTRATRECOURSVAL
	from CONTRAT
	left join MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	left join CARBURANT ON CARBURANT.CARBUAUTO=CONTRAT.CARBUAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	left join AUTH.`MACHINE` ON MACHINE.MAAUTO=CONTRAT.MAAUTO
	left join p_insurancetype ON ity_id = CONTRATTYPEASSURANCE
    left join p_appealtype ON apl_id = CONTRATTYPERECOURS
	where CONTRAT.AGAUTO="$G_AGENCE"
	and CONTRATOK in ("-1","-2")
	and CONTRAT.LIETCONTRATAUTO in ("1","2","3","4","7")
	and DATE(CONTRATDD) <= "$finms"
	and DATE(CONTRATDR) >= "$debutms"
	AND CONTRAT.MAAUTO != 0
	AND CONTRATMACHATTR = -1
	and (ETATFACTURE in ("P", "") or ETATFACTURE is null)
	$where
	order by $champ
	};
	my ($andm, $msdm, $jrdm) = split(/:/, $debutms);
	my $dm = Delta_Days(1900, 1, 1, $andm, $msdm, $jrdm);
	my ($anfm, $msfm, $jrfm) = split(/:/, $finms);
	my $fm = Delta_Days(1900, 1, 1, $anfm, $msfm, $jrfm);
	my $jourmois = Delta_Days($andm, $msdm, $jrdm, $anfm, $msfm, $jrfm); # Diff�rence de jours entre les 2 params pass�s
	my $jourmsouv = $jourmois;   # Diff�rence de jours entre les 2 params pass�s
	my $uvfer = &jour_fer_ag_cnx($G_PAYS, $G_AGENCE, $jrdm, $msdm, $andm, $jourmsouv, $DB); # nb jours f�ri�s entre 2 dates avec pays en plus
	$jourmsouv += 1; # jours ouvrables
	my $ouvrwe = &jour_we($jourmsouv, $jrdm, $msdm, $andm); # nb de jours w-e
	$jourmsouv = $jourmsouv-$ouvrwe-$uvfer; # jours ouvrables = jourmois - nbwe - nbferie
	my $sth = $DB->prepare($requete);
	$sth->execute();
	my $nbrow = $sth->rows;
	my $durtot = 0;
    my $nbcontlivr = 0;
	my $nbcontcours = 0;
	my $nbcontarret = 0;
	my $nbdureems = 0;
	my $nbdureeplus = 0;
	my $nbforfait = 0;
	my $nbtarifms = 0;
	my $nbasscomp = 0;
	my $total = 0.0;
	my $catr = 0;
	my $cacarb = 0;
	my $temp = Days_in_Month($anfm, $msfm);

	while (my @data = $sth->fetchrow_array()) {
		# FACOMAUTO
		my $modele = $data[10];

		# SOLOAUTO
		if ($data[13] != 0) {$modele = 0;}

		# CONTRATDD
		my ($jrdd, $msdd, $andd) = &datetojjmsan($data[0]);
		my $dd = Delta_Days(1900, 1, 1, $andd, $msdd, $jrdd);
		# CONTRATDR
		my ($jrdf, $msdf, $andf) = &datetojjmsan($data[1]);
		my $df = Delta_Days(1900, 1, 1, $andf, $msdf, $jrdf);
		my $dureecont = $df - $dd + 1;
		my $boolms    = 0;
		my $ta        = 0.0;
		my $tr        = 0.0;
		my $carbu     = 0.0;
		# CONTRATDD
		my $ddf       = $data[0];
		# CONTRATDR
		my $dff       = $data[1];
		my $jrplus    = 0.0;
		my $jrmoins   = 0.0;
		# MOPXJOURFR
		my $tarif     = $data[4];
		# MOTARIFMSFR
		my $tarifm    = $data[5];
		# MOFORFAITFR
		my $forfait   = $data[11];
		my $boole     = 0;
		my $booleta   = 0;
		my $booletr   = 0;
		my $boolemens = 0;
		my $booleforf = 0;
		my $boolecarb = 0;

		# Contrat Mensuel
		if ($tarifm != 0) {
			$boole = 1;
			$tarif = $tarifm;
			# Calculer le nb de jours ouvrables
			$nbjourmois = $temp;
			$nbnbjrwe   = &jour_we($nbjourmois, 01, $msfm, $anfm);
			$nbnbferie  = &jour_fer_ag_cnx($G_PAYS, $G_AGENCE, 01, $msfm, $anfm, $nbjourmois-1, $DB);
			my $joursupmensuel = 0;

			if ($df > $fm) {$joursupmensuel = 0;}
			else {
				my $testjoursupmensuel = &num_jour($jrdf, $msdf, $andf);

				if ($testjoursupmensuel == 6) {$joursupmensuel = 1;}
				if ($testjoursupmensuel == 7) {$joursupmensuel = 2;}
			}

			$jourmois = $nbjourmois - $nbnbjrwe - $nbnbferie + $joursupmensuel;
			$boolemens = 1;
		}
		# Contrat Forfaitaire
		if ($forfait != 0) {
			$boole = 2;
			$tarif = $forfait;
			my $dureecont = Delta_Days($andd, $msdd, $jrdd, $andf, $msdf, $jrdf);
			my $nbferieforfait = &jour_fer_ag_cnx($G_PAYS, $G_AGENCE, $jrdd, $msdd, $andd, $dureecont, $DB);
	  		my $joursupforfait = 0;
	  		my $testjoursupforfait;

			if ($df > $fm) {$joursupforfait = 0;}
			else {
				$testjoursupforfait = &num_jour($jrdf, $msdf, $andf);

				if ($testjoursupforfait == 6) {$joursupforfait = 1;}
				if ($testjoursupforfait == 7) {$joursupforfait = 2;}
			}

			$dureecont++;

			if ($dureecont == 1 && $testjoursupforfait == 7) {$joursupforfait = 1;}

			$nbjrweforfait = &jour_we($dureecont, $jrdd, $msdd, $andd);
	  		$dureecont = $dureecont - $nbferieforfait - $nbjrweforfait + $joursupforfait;
			$jourmois = $dureecont;
			$booleforf = 1;
		}
		# Ici en fonction de la date de d�part, si elle est dans le mois en cours
		# on comptabilise le transport aller sinon non
		if ($data[15] eq "P") {$ddf = $debutms;}
		elsif (!$data[15] || $data[15] eq "") {
			$ta = $data[7];
			$booleta = 1;
		}
		# Ici en fonction de la date de fin, si elle est dans le mois en cours
		# on comptabilise le transport retour et le carburant sinon non
		if ($df > $fm) {
			$dff = $finms;
			$dff2 = "$anfm:$msfm:$temp";
			$boolms = 1;
		} else {
			$carbu = $data[9]; # CAMONTANTFR
			$dff2 = $dff;

			if ($carbu != 0) {$boolecarb = 1;}

			$tr = $data[8]; # MOTRANSPORTRFR
			$booletr = 1;
		}

		$jrplus  = $data[2]; # CONTRATJOURSPLUS
		$jrmoins = $data[3]; # CONTRATJOURSMOINS
		my ($dda, $ddm, $ddj);
		my ($dfa, $dfm, $dfj);

		if ($ddf =~ /:/) {($dda, $ddm, $ddj) = split(/:/, $ddf);}
		else {($dda, $ddm, $ddj) = split(/-/, $ddf);}
		if ($dff =~ /:/) {($dfa,$dfm,$dfj) = split(/:/, $dff);}
		else {($dfa, $dfm, $dfj) = split(/-/, $dff);}
		if ($dff2 =~ /:/) {($dfa2, $dfm2, $dfj2) = split(/:/, $dff2);}
		else {($dfa2, $dfm2, $dfj2) = split(/-/, $dff2);}

		my $duree = Delta_Days($dda, $ddm, $ddj, $dfa, $dfm, $dfj);
		my $duree2 = Delta_Days($dda, $ddm, $ddj, $dfa2, $dfm2, $dfj2);

		if ($duree < 0) {$nbrow--;next;}

		# $duree2 ici sert � calculer les contrats chevauchant la fin de mois
		# avec -4j ds le mois : d� � la nouvelle facturation qui ne prend pas en compte ces
		# contrats l�
		my $nbferie2 = &jour_fer_ag_cnx($G_PAYS, $G_AGENCE, $ddj, $ddm, $dda, $duree2, $DB);
		my $nbjrwe2  = 0;
  		my $joursup2 = 0;
  		my $testjoursup2;

		if (Delta_Days($anfm, $msfm, $temp, $andf, $msdf, $jrdf) > 0) {$joursup2 = 0;}
		else {
			$testjoursup2 = &num_jour($dfj2, $dfm2, $dfa2);

			if ($testjoursup2 == 6) {$joursup2 = 1;}
			if ($testjoursup2 == 7) {$joursup2 = 2;}
		}

		$duree2 += 1;

		if ($duree2 == 1 && $testjoursup2 == 7) {$joursup2 = 1;}

		$nbjrwe2 = &jour_we($duree2, $ddj, $ddm, $dda);
		$duree2 = $duree2 - $nbferie2 - $nbjrwe2 + $joursup2 + $jrplus - $jrmoins;

		if (Delta_Days($anfm, $msfm, $temp, $andf, $msdf, $jrdf) > 0 && $duree2 <= 4) {
			# $nbrow--;

			if ($posi) {
				# lietcontratauto
                if (($data[14] eq "1")) {
                    $nbcontlivr++;
                    $comm{$modele}->[13] += 1;
                }
				elsif (($data[14] eq "2")) {
					$nbcontcours++;
					$comm{$modele}->[2] += 1;
				} else {
					$nbcontarret++;
					$comm{$modele}->[1] += 1;
				}

				$comm{$modele}->[0] += 1;
			}

			next;
		}
		if ($booleta) {$catr += $ta;}
		if ($booletr) {$catr += $tr;}
		if ($boolemens) {$nbtarifms++;}
		if ($booleforf) {$nbforfait++;}
		if ($boolecarb) {$cacarb += $carbu;}

		# Nouveau calcul, dur�e de jours ouvrables pour tous
        $duree = &LOC::Date::getRentDuration(
            sprintf('%04d-%02d-%02d', $dda, $ddm, $ddj),
            sprintf('%04d-%02d-%02d', $dfa, $dfm, $dfj),
            $G_AGENCE,
            $data[1]
        ) + $jrplus - $jrmoins;
		$comm{$modele}->[4+$posi] += $duree;
		$durtot += $duree;


		# Le calcul de la dur�e s'effectue en fonction du nombre
		# de jours ouvrables dans le mois ainsi que de sa dur�e et
		# des j+ j-, jours f�ri�s et w-e
		# si dur�e sup au mois, joursouvrables, sinon dur�e et le reste
		# Surtout important c'est le calcul des contrats de -5j CONTRATDD et CONTRATDR
		# Et non pas le nb de contrats dont la dur�e ds le mois < 5
		# $duree (nb de jours du contrat ds le mois) <> $dureereel (nb de jours du contrat)
		my ($dareel, $dmreel, $djreel) = split(/-/, $data[0]);
		my ($fareel, $fmreel, $fjreel) = split(/-/, $data[1]);
		my $dureereel = Delta_Days($dareel, $dmreel, $djreel, $fareel, $fmreel, $fjreel);
		my $nbferiereel = &jour_fer_ag_cnx($G_PAYS, $G_AGENCE, $djreel, $dmreel, $dareel, $dureereel, $DB);
		my $testjoursupreel = &num_jour($fjreel, $fmreel, $fareel);
		my $joursupreel = 0;

		if ($testjoursupreel == 6) {$joursupreel = 1};
		if ($testjoursupreel == 7) {$joursupreel = 2};
		if ($boolms != 0) {$joursupreel = 0;}

		$dureereel += 1;

		if ($dureereel == 1 && $testjoursupreel == 7) {$joursupreel = 1;}

		my $nbjrwereel = &jour_we($dureereel, $djreel, $dmreel, $dareel);
		$dureereel = $dureereel + $jrplus - $jrmoins - $nbferiereel - $nbjrwereel + $joursupreel;

		if ($dureereel < 5) {
			$nbdureems++;
		} else {$nbdureeplus++;}
		if ($boole == 1 || $boole == 2) {$tarif = $tarif / $jourmois;}


        my $beginDate = sprintf('%04d-%02d-%02d', $dda, $ddm, $ddj);
        my $endDate   = sprintf('%04d-%02d-%02d', $dfa, $dfm, $dfj);

        # Informations sur l'assurance
        my $ass = $data[6]; # CONTRATTYPEASSURANCE
        my $insuranceFlags = $data[18];
		my $montantass = 0.0;
		$montantass = &LOC::Common::Rent::calculateInsurance($G_PAYS, $G_AGENCE, $ass, $data[17], $tarif,
		                                                     $beginDate, $endDate, $jrplus, $jrmoins);

		if ($insuranceFlags & LOC::Insurance::Type::FLAG_ISINVOICED)
		{
			$nbasscomp++;
		}

		# Gestion de recours
		my $appealTypeId = $data[19];
		my $appealRate   = $data[20];

        my $appealAmount = 0.0;
        if ($appealTypeId)
        {
            $appealAmount = &LOC::Common::Rent::calculateAppeal($G_PAYS, $G_AGENCE, $appealTypeId, $appealRate, $tarif,
                                                                $beginDate, $endDate, $jrplus, $jrmoins);
        }

		$duree = sprintf("%.2f", $duree);
		my $montant = $tarif * $duree;
		my $mtloc = $montant + $montantass + $appealAmount;
		my $montotal = $montant + $ta + $tr + $carbu + $montantass + $appealAmount;
		$total += $montotal;
		$comm{$modele}->[3+$posi] += $montotal;
		
		# Pour les machines hors parc, le co�t imput�
		# est �gal au CA / 2
		if ($data[16] == -1) {
			if ($posi) {
				$comm{$modele}->[12] += $mtloc / 2;
			}
			else {
				$comm{$modele}->[11] += $mtloc / 2;
			}
		}
		
		if ($posi) {
			$comm{$modele}->[10] += $mtloc;
			$caloc += $mtloc;

			# LIETCONTRATAUTO
            if (($data[14] eq "1")) {
                $nbcontlivr++;
                $comm{$modele}->[13] += 1;
            }
			elsif (($data[14] eq "2")) {
				$nbcontcours++;
				$comm{$modele}->[2] += 1;
			} else {
				$nbcontarret++;
				$comm{$modele}->[1] += 1;
			}
			
			$comm{$modele}->[0] += 1;
		} else {$comm{$modele}->[5] += $mtloc;}
	}

	$sth->finish;
	my $cond = "MODELEMACHINE.MOMAAUTO";
	my $where = "";

	if ($modl eq "") {$cond = "FACOMAUTO";}
	$where = " and FACOMAUTO=$modl" if $modl;

	my $requete = $DB->prepare(qq{
	select MOTARIFMSFR, MOFORFAITFR, SOLOAUTO, CONTRATDUREE, $cond
	from CONTRAT
	left join MONTANT on CONTRAT.MOAUTO=MONTANT.MOAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where CONTRAT.AGAUTO="$G_AGENCE"
	and CONTRATOK in ("-1","-2")
	and CONTRAT.LIETCONTRATAUTO in ("2","3","4","7")
	and DATE(CONTRATDD) <= "$finms"
	and DATE(CONTRATDR) >= "$debutms"
	and ETATFACTURE="F"
	$where
	order by $cond
	});
	$requete->execute();

	while (my @data = $requete->fetchrow_array()) {
		my $modele = $data[4];

		if ($data[0] != 0) {$nbtarifms++;}
		if ($data[1] != 0) {$nbforfait++;}
		if ($data[2] != 0) {$modele = 0;}
		if ($data[3] < 5 && $data[0] == 0) {
			$nbdureems++;
		} else {$nbdureeplus++;}
		if ($posi) {
			$nbcontarret++;
			$comm{$modele}->[1]++;
			$comm{$modele}->[0]++;

		}

		$nbrow++;
	}

	$requete->finish();

	my $requete = $DB->prepare(qq|
	select LIGNEREFARTICLE,
	   sum(ROUND(ROUND(LIGNEPRIX, |.$tabSageFormats->{'price'}.qq|)*ROUND(LIGNEQTITE, |.$tabSageFormats->{'quantity'}.qq|), |.$tabSageFormats->{'amount'}.qq|)),
	   count(LIGNEREFARTICLE), sum(LIGNEQTITE), $cond
	from ENTETELIGNE
	left join ENTETEFACTURE on ENTETELIGNE.ENTETEAUTO=ENTETEFACTURE.ENTETEAUTO
	left join LIGNEFACTURE on LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	left join CONTRAT on ENTETEFACTURE.CONTRATAUTO=CONTRAT.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where ENTETEFACTURE.AGAUTO="$G_AGENCE"
    AND ((DATE(`DATE`)>="$debutms" and DATE(`DATE`)<="$finms")
    OR (ENTETEIDFACTU IS NULL AND DATE(CONTRATDD) <= "$finms" and DATE(CONTRATDR) >= "$debutms"))
	and SOLOAUTO=0
	$where
	group by $cond, LIGNEREFARTICLE
	|);

	$requete->execute();

	while (my @data = $requete->fetchrow_array()) {
		$total += $data[1];
		$comm{$data[4]}->[3+$posi] += $data[1];

		if ($data[0] =~ /^TRANS/ || $data[0] eq "SERT") {$catr += $data[1];}
		if ($data[0] eq "CARB") {$cacarb += $data[1];}
		if ($data[0] eq "ASS") {
			$nbasscomp += $data[2];

			if ($posi) {
				$comm{$data[4]}->[10] += $data[1];
				$caloc += $data[1];
			} else {$comm{$data[4]}->[5] += $data[1];}
		}
		if ($data[0] eq "GDR")
		{
		    if ($posi)
		    {
                $comm{$data[4]}->[10] += $data[1];
                $caloc += $data[1];
            }
            else
            {
                $comm{$data[4]}->[5] += $data[1];
            }
		}
		if ($data[0] eq "LOC" || $data[0] eq "REM" || $data[0] eq "J+" || $data[0] eq "J-") {
            if ($data[0] ne "REM") {
			     $durtot += $data[3];
			     $comm{$data[4]}->[4+$posi] += $data[3];
            }

			if ($posi) {
				$comm{$data[4]}->[10] += $data[1];
				$caloc += $data[1];
			} else {$comm{$data[4]}->[5] += $data[1];}
		}
	}

	$requete->finish();

	my $requete = $DB->prepare(qq|
	select LIGNEREFARTICLE,
	   sum(ROUND(ROUND(LIGNEPRIX, |.$tabSageFormats->{'price'}.qq|)*ROUND(LIGNEQTITE, |.$tabSageFormats->{'quantity'}.qq|), |.$tabSageFormats->{'amount'}.qq|)),
	   count(LIGNEREFARTICLE), sum(LIGNEQTITE), $cond
	from ENTETELIGNE
	left join ENTETEFACTURE on ENTETELIGNE.ENTETEAUTO=ENTETEFACTURE.ENTETEAUTO
	left join LIGNEFACTURE on LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	left join CONTRAT on ENTETEFACTURE.CONTRATAUTO=CONTRAT.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
    where ENTETEFACTURE.AGAUTO="$G_AGENCE"
    AND ((DATE(`DATE`)>="$debutms" and DATE(`DATE`)<="$finms")
    OR (ENTETEIDFACTU IS NULL AND DATE(CONTRATDD) <= "$finms" and DATE(CONTRATDR) >= "$debutms"))
	and SOLOAUTO!=0
	$where
	group by $cond, LIGNEREFARTICLE
	|);

	$requete->execute();

	while (my @data = $requete->fetchrow_array()) {
		$total += $data[1];
		$comm{0}->[3+$posi] += $data[1];
		$comm{""}->[3+$posi] -= $data[1];

		if ($data[0] =~ /^TRANS/ || $data[0] eq "SERT") {$catr += $data[1];}
		if ($data[0] eq "CARB") {$cacarb += $data[1];}
		if ($data[0] eq "ASS") {
			$nbasscomp += $data[2];

			if ($posi) {
				$comm{0}->[10] += $data[1];
				$comm{""}->[10] -= $data[1];
				$caloc += $data[1];
			} else {
				$comm{0}->[5] += $data[1];
				$comm{""}->[5] -= $data[1];
			}
		}
        if ($data[0] eq "GDR")
        {
            if ($posi)
            {
                $comm{0}->[10] += $data[1];
                $comm{""}->[10] -= $data[1];
                $caloc += $data[1];
            }
            else
            {
                $comm{0}->[5] += $data[1];
                $comm{""}->[5] -= $data[1];
            }
        }
		if ($data[0] eq "LOC" || $data[0] eq "REM" || $data[0] eq "J+" || $data[0] eq "J-") {
            if ($data[0] ne "REM") {
    			$durtot += $data[3];
    			$comm{0}->[4+$posi] += $data[3];
    			$comm{""}->[4+$posi] -= $data[3];
			}

			if ($posi) {
				$comm{0}->[10] += $data[1];
				$comm{""}->[10] -= $data[1];
				$caloc += $data[1];
			} else {
				$comm{0}->[5] += $data[1];
				$comm{""}->[5] -= $data[1];
			}
		}
	}

	$requete->finish();

	my $ca = sprintf("%.2f", $total);

	return ($nbrow, $ca, $catr, $cacarb, $nbdureems, $nbdureeplus, $nbcontcours, $nbcontarret, $nbasscomp, $nbtarifms, $nbforfait, $jourmsouv, $durtot, $nbcontlivr);
}

# Renseigne divers tableaux sur l'ensemble des mod�les machines et de leurs liaisons
# avec les familles comptables et familles commerciales ainsi que le co�t d�taill� par machine
sub list_mach {
	my @param = @_;
	my $where = "";
	my $where2 = "";

	if ($modl ne "") {
		$where  = qq{WHERE FACOMAUTO="$modl"};
		$where2 = qq{AND FACOMAUTO="$modl"};
	}

	my $req = qq{
	select MOMAAUTO, FACOMAUTO
	from AUTH.`MODELEMACHINE`
	left join FAMILLEMACHINE ON MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO
	$where
	};
	my $sth = $DB->prepare($req);
	$sth->execute();

	while (my @data= $sth->fetchrow_array()) {
        $mach_mod{$data[0]} = 0;
        $mach_com{$data[1]} = 0;
		$cout_mod{$data[0]} = 0;
		$cout_com{$data[1]} = 0;
	}

	$sth->finish;
	$req = qq{
	select MAAUTO, AGAUTO, MODELEMACHINE.MOMAAUTO, FACOMAUTO, MOMACOUTFR, MAHORSPARC, ETCODE
	from AUTH.`MACHINE`
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=MACHINE.MOMAAUTO
	left join FAMILLEMACHINE ON MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO
	where MADATEACHAT<="$param[0]"
	$where2
	};
	
	$sth = $DB->prepare($req);
	$sth->execute();

	while (my @data = $sth->fetchrow_array()) {
		my $agence = &transfert($data[0], $param[0]);

		if ($agence ne "") {$data[1] = $agence;}
		if ($data[1] eq $G_AGENCE) {
			$mach_mod{$data[2]}++;
			$mach_com{$data[3]}++;
			if ($data[5] != -1) {
                # Co�t du mod�le en euros converti en devise locale
				$cout_mod{$data[2]} += $data[4] / $tauxE;
				$cout_com{$data[3]} += $data[4] / $tauxE;
				$coutatotal += $data[4] / $tauxE;
			}
		}
	}

	$sth->finish;
}

# ex : transfert('maauto', 'date'))
# Retourne l'agence d'arriv�e du dernier transfert
# d'une machine donn�e ou si cela ne donne rien
# retourne la 1�re agence de d�part apr�s une date donn�e
# d'un transfert
sub transfert {
	my @param=@_;
	my $req=qq{select TRANSFERTAGA FROM AUTH.`TRANSFERT` WHERE MAAUTO="$param[0]" and TRANSFERTDATEAR<="$param[1]" ORDER BY TRANSFERTAUTO DESC LIMIT 1};
	my $sth=$DB->prepare($req);
	$sth->execute();
	my ($ag)=$sth->fetchrow_array();
	$sth->finish;

	if ($ag eq "") {
		$req=qq{select TRANSFERTAGD FROM AUTH.`TRANSFERT` WHERE MAAUTO="$param[0]" and TRANSFERTDATEAR>"$param[1]" ORDER BY TRANSFERTAUTO LIMIT 1};
		$sth=$DB->prepare($req);
		$sth->execute();
		($ag)=$sth->fetchrow_array();
	}

	return $ag;
}

# ex : list_mach2('datejour')
# Retourne la somme des machines par modeles, fam compta et fam commer
# soit des sotcks, soit de l'historique des stocks suivant la date
# sauf 18 (GRU18 : Rendue pour les grues) et < 20 (MAC21 Restitu�e, MAC22 Epave, MAC23 Vol, MAC24 Perdu)
sub list_mach2 {
	my @param = @_;
	my $where = "";

	if ($modl ne "") {$where = qq{ AND FACOMAUTO="$modl"};}

	my $req = qq{
	select MOMAAUTO, FACOMAUTO
	from AUTH.`MODELEMACHINE`
	left join FAMILLEMACHINE on MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO
	$where
	};
	my $sth = $DB->prepare($req);
	$sth->execute();

	while (my @data = $sth->fetchrow_array()) {
        $mach_mod{$data[0]} = 0;
        $mach_com{$data[1]} = 0;
		$cout_mod{$data[0]} = 0;
		$cout_com{$data[1]} = 0;
	}

	$sth->finish;
	my $req = "";

	if (!$date) {
        # R�cup�ration des machines dans les �tats qui vont bien (< 20 et != 10)
        # et qui sont soit :
        # - affect�es � l'agence
        # -- sans visibilit�
        # -- avec visibilit� sur une autre agence mais pas en transfert ou en livraison
        # -- avec visibilit� sur une autre agence, en transfert ou en livraison,
        #    et attribu�es � un contrat de l'agence de visibilit�
        # - en visibilit� sur l'agence, en transfert ou en livraison et attribu�es � un contrat de l'agence
		$req = qq{
		select M.MOMAAUTO, count(*), 0, M.MAHORSPARC
		from AUTH.`MACHINE` M
		LEFT JOIN CONTRAT C ON (M.CONTRATAUTO = C.CONTRATAUTO AND M.MAAUTO = C.MAAUTO AND C.CONTRATMACHATTR = -1)
		where M.LIETCODE not IN ("10")
		and M.LIETCODE is not NULL
		and M.LIETCODE<20
		AND (M.AGAUTO = "$G_AGENCE" AND (
                M.MASUPPAGAUTO IS NULL
                OR M.MASUPPAGAUTO IS NOT NULL AND (M.ETCODE NOT IN ("MAC09", "MAC12") OR C.ETCODE != "CON01" OR C.ETCODE IS NULL))
            OR M.MASUPPAGAUTO = "$G_AGENCE" AND (M.ETCODE IN ("MAC09", "MAC12") AND C.ETCODE = "CON01"))
		group by M.MOMAAUTO, M.MAHORSPARC
		};
	} else {
		my @time = split(/:/, $param[0]);
		my ($anhist, $mshist, $jrhist) = Add_Delta_Days($time[0], $time[1], $time[2], 1);
		$req = qq{
		select HISTOSTOCKMODELE.MOMAAUTO, sum(HISTSTMOTOTAL), sum(HISTSTMOHORSPARC), 0
		from AUTH.`HISTOSTOCKMODELE`
		where HISTDATE between "$anhist:$mshist:$jrhist 00:00:00" and "$anhist:$mshist:$jrhist 23:59:59"
		and AGAUTO="$G_AGENCE"
		group by HISTOSTOCKMODELE.MOMAAUTO
		};
	}
	
	
	$sth = $DB->prepare("$req");
	$sth->execute();

	while (my @data = $sth->fetchrow_array()) {
		my @tp = &tp_mach($data[0]);
		
		if (@tp) {
            $mach_mod{$data[0]} += $data[1];
            $mach_com{$tp[0]}   += $data[1];
			if ($data[3] != -1) {
				$cout_mod{$data[0]} += $tp[1] * ($data[1] - $data[2]);
				$cout_com{$tp[0]}   += $tp[1] * ($data[1] - $data[2]);
				$coutatotal         += $tp[1] * ($data[1] - $data[2]);
			}
		}
	}

	$sth->finish;
}

# Renvoie pour un mod�le donn� sa famille comptable, commerciale et son co�t
sub tp_mach {
	my @param = @_;
	my $where = "";

	if ($modl ne "") {$where = qq{ AND FACOMAUTO="$modl"};}

	my $req = qq{
	select FACOMAUTO, MOMACOUTFR
	from AUTH.`MODELEMACHINE`
	left join FAMILLEMACHINE ON MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO
	where MOMAAUTO="$param[0]"
	$where
	};
	$sth = $DB->prepare("$req");
	$sth->execute();
	my @data = $sth->fetchrow_array();
	$sth->finish;
	
	# Co�t du mod�le converti en devise locale
	$data[1] = $data[1] / $tauxE;

	return @data;
}

# Renvoie les libell�s, et le groupe des familles comptables
# et commerciales
sub nb_mach {
    my @param = @_;

    # famille compta
    my $t = &test_date($mimois);

    if ($t < 0) {&list_mach($param[0]);}
    else {&list_mach2($day);}
    
    # famille commerciale
    my $modele;

    if ($modl eq "") {
        $modele = qq{
        select FAMILLECOMMERCIALE.FACOMAUTO, FACOMLIBELLE, FACOMORDRE, MOMAGRP
        from FAMILLECOMMERCIALE
        left join FAMILLEMACHINE on FAMILLEMACHINE.FACOMAUTO=FAMILLECOMMERCIALE.FACOMAUTO
        left join AUTH.`MODELEMACHINE` on MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO
        where MOMAGRP is not NULL
        group by FAMILLECOMMERCIALE.FACOMAUTO, MOMAGRP
        order by FACOMORDRE
        };
    } else {
        $modele = qq{
        select MOMAAUTO, MOMADESIGNATION, MOMAGRP, FAMAELEVATION
        from AUTH.`MODELEMACHINE`
        left join FAMILLEMACHINE on MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO
        where FACOMAUTO="$modl"
        order by FAMAHAUTEUR, MOMADESIGNATION, MOMAGRP
        };
    }

    $sth = $DB->prepare("$modele");
    $sth->execute();

    while (my @data = $sth->fetchrow_array()) {
        $fmcomm{$data[0]} = $data[1];

            if ($modl eq "") {
                $ordcomm[$data[2]] = $data[0];
                $ordcommtot[$data[2]] = $data[3];
            } else {
            push(@ordcomm, $data[0]);
            push(@ordcommtot, $data[2]);
            $data[3] = "" if ($data[3] eq "n/a");
            $htcomm{$data[0]} = $data[3];
            }

        for (my $i=0;$i<14;$i++) {$comm{$data[0]}->[$i] = 0;}
            if ($modl eq "") {
            $comm{$data[0]}->[8]  = $mach_com{$data[0]};
            $comm{$data[0]}->[9]  = $cout_com{$data[0]};
        } else {
            $comm{$data[0]}->[8]  = $mach_mod{$data[0]};
            $comm{$data[0]}->[9]  = $cout_mod{$data[0]};
        }
        }

    $sth->finish;

    if ($modl eq "") {
            $ordcomm[203] = 0;
            $ordcommtot[203] = 1;
        }

    #sous location
    $fmcomm{0} = $mess[3300];

    for (my $i=0;$i<14;$i++) {$comm{0}->[$i] = 0;}
}

# Calcul des diverses stats
sub statistique {
	@rest = &statjr($deb, $fin, 3);
	@resc = &statjr($deb, $dtjour, 0);
}

# Affichage et met en forme
sub entete {
	my $db    = $dbaff;
	my $fn    = $fnaff;
	my $fna   = $affdte;
	my $casse = &casse($mimois);

	if ($tsdt) {
		my @data = split(/\:/,$date);
		my ($dbf,$dbff,$fnf,$fnff) = &debfinmois($data[1],$data[0]);
		$db  = $dbff;
		$fn  = $fnff;
		$fna = $fn;
	}

	my $heure = sprintf("%02d:%02d:%02d", (localtime)[2], (localtime)[1], (localtime)[0]);
	my $cde = "";
	print qq{
<table width="100%" border="0" cellspacing="2" cellpadding="0">
	<tr>
		<td rowspan=4 colspan=3 align="left"><font class="PT"><b>$mess[2000]:&nbsp;$G_AGENCE<br>$affdte $heure</b>$cde</font></th>
		<td colspan=11 align="center">$jfr</td>
		<td rowspan=4 colspan=4 align="right"><font class="PT"><b>$mess[1350]:&nbsp;$db<br>$mess[1351]:&nbsp;$fn</b></font></td>
	</tr>
	};

	my $total = $rest[6]+$rest[7]+$rest[13];
	my $tot   = 0;

	if ($modl ne "") {$tot = $mach_com{$modl};}

	# Pourcentage d'utilisation sur la totalit�
	print qq{
	<tr bgcolor="#C6CDC5">
		<th rowspan=2><font class="PT">$mess[5002]</font></th>
		<th rowspan=2><font class="PT">$mess[5005]</font></th>
		<th rowspan=2><font class="PT">$mess[5006]</font></th>
		<th rowspan=2><font class="PT">$mess[5007]</font></th>
		<th rowspan=2><font class="PT">$mess[5008]</font></th>
		<th colspan=2><font class="PT">$mess[5009]$fna</font></th>
		<th colspan=4><font class="PT">$mess[5010]</font></th>
	</tr>
	<tr bgcolor="#C6CDC5">
		<th align="right"><font class="PT">$mess[5012]</font></th>
		<th align="right"><font class="PT">$mess[5013]</font></th>
		<th align="right"><font class="PT">$mess[5012]</font></th>
		<th align="right"><font class="PT">$mess[5013]</font></th>
		<th align="right"><font class="PT">$mess[2004]</font></th>
		<th align="right"><font class="PT">$mess[5011] + $mess[2004]</font></th>
	</tr>
	<tr>
		<td align="center"><font class="PT">$total</font></td>
		<td align="center"><font class="PT">$rest[8]</font></td>
		<td align="center"><font class="PT">$rest[4]</font></td>
		<td align="center"><font class="PT">$rest[9]</font></td>
		<td align="center"><font class="PT">$rest[10]</font></td>
	};
	my $ca_fm = $rest[1];

	for (my $i=1;$i<4;$i++) {
		$rest[$i] = sprintf("%.0f", $rest[$i]);
		$resc[$i] = sprintf("%.0f", $resc[$i]);
		$rest[$i] = &fnombre2($rest[$i], 3, 0);
		$resc[$i] = &fnombre2($resc[$i], 3, 0);
	}

	my $affcaloc = sprintf("%.0f", $caloc);
	$affcaloc = &fnombre2($affcaloc,3,0);
	print qq{
		<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$resc[2]</span></font></td>
		<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$resc[3]</span></font></td>
		<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$rest[2]</span></font></td>
		<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$rest[3]</span></font></td>
	};
	$casse = sprintf("%.0f", $casse);
	my $ca_casav = $casse + $ca_fm;
	$casse = &fnombre2($casse,3,0);
	$ca_casav = sprintf("%.0f", $ca_casav);
	$ca_casav = &fnombre2($ca_casav,3,0);
	print qq|
		<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$casse</span></font></td>
		<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$ca_casav</span></font></td>
	<tr>
		<td colspan=18>&nbsp;</td>
	</tr>
	|;
}

# Affichage tableau d'ent�te pour Fam Comptable et commerciale
sub tire_tab {
	my $jourmsouv = 0;
	if (!$tsdt) {
		$jourmsouv = &jours_ouvrables($deb, $dtjour, $G_PAYS, $G_AGENCE, $DB);
	}
	else {
		$jourmsouv = &jours_ouvrables(substr($mimois, 0,
			length($mimois)-2)."01", $mimois, $G_PAYS, $G_AGENCE, $DB);
	}
	
	my @param = @_;
	my $fna = $affdte;

	if ($tsdt) {
		my @data = split(/\:/,$date);
		my ($dbf,$dbff,$fnf,$fnff) = &debfinmois($data[1],$data[0]);
		$fna = $fnff;
	}

	print qq{
	<tr bgcolor="#C6CDC5">
		<th rowspan=2><font class="PT">$param[0]</font></th>
	};

	if ($modl) {print qq|<th rowspan=2><font class="PT">$mess[2959]</font></th>|;}

	print qq{
        <th rowspan=2><font class="PT">$mess[5016]</font></th>
		<th colspan=4><font class="PT">$mess[5000]</font></th>
		<th rowspan=2><font class="PT">$mess[5001]</font></th>
	};
	print qq{
		<th colspan=4><font class="PT">$mess[5009]$fna</font></th>
		<th align="right"><font class="PT">$jourmsouv&nbsp;</font></th>
		<th align="left"><font class="PT">&nbsp;$mess[5026]</font></th>
	};
	print qq{
		<th colspan=8><font class="PT">$mess[5010]</font></th>
	</tr>
	<tr bgcolor="#C6CDC5">
		<th><font class="PT">$mess[5002]</font></th>
		<th><font class="PT">$mess[5003]</font></th>
		<th><font class="PT">$mess[5004]</font></th>
        <th><font class="PT">$mess[5027]</font></th>
		<th align="right"><font class="PT">$mess[5011]</font></th>
		<th align="right"><font class="PT">$mess[5011] $mess[5025]</font></th>
		<th><font class="PT">$mess[5017]</font></th>
		<th align="right"><font class="PT">$mess[5018]</font></th>
		<th align="right"><font class="PT">$mess[5020]</font></th>
		<th align="right"><font class="PT">Ratio cash</font></th>
		<th align="right"><font class="PT">$mess[5011]</font></th>
		<th align="right"><font class="PT">$mess[5011] $mess[5025]</font></th>
		<th><font class="PT">$mess[5017]</font></th>
		<th ><font class="PT">$mess[5019]</font></th>
		<th><font class="PT">$mess[5019] $mess[5025]</font></th>
		<th align="right"><font class="PT">$mess[5020]</font></th>
		<th><font class="PT">$mess[5021]</font></th>
		<th align="right"><font class="PT">Ratio prix</font></th>
	</tr>
	};
}

# Affichage du corps de la page
# toutes les stats r�cup�r�es ds le hachage
# Commerciale
sub corps {
	my $jourmsouv = 0;
	if (!$tsdt) {
		$jourmsouv = &jours_ouvrables($deb, $dtjour, $G_PAYS, $G_AGENCE, $DB);
	}
	else {
		$jourmsouv = &jours_ouvrables(substr($mimois, 0,
			length($mimois)-2)."01", $mimois, $G_PAYS, $G_AGENCE, $DB);
	}
	&tire_tab($mess[5023]);
	my @totaux = ();
	my @sstot  = ();
	my @sstot_divers  = ();
	my $aff_mac = "";
	my $aff_div = "";

	for (my $i=0;$i<9;$i++) {
		$totaux[$i] = 0;
		$sstot[$i] = 0;
		$sstot_divers[$i] = 0;
	}

	#corps
	my $k = 0;

	for(my $i=0;$i<@ordcomm;$i++) {
		my $boole_aff = 0;
		my $j = $ordcomm[$i];

		if ($j eq "") {next};
		# FA 8964 : Test sur le nb de parc et sur le CA total fin de mois
		if (($comm{$j}->[8] == 0) && ($comm{$j}->[6] == 0)) {next;}

		my $a = "";
		my $f = "";

		if (($modl eq "") && ($j)) {
			$a = qq{<a href="#" onclick="return rs('modele$j', '}.$cgi->url().qq{?date=$date&modele=$j&aff=$fmcomm{$j}', 630, 400)">};
			$f = qq{</a>};
		}

		my $c = $k % 2;
		my $color = "''";

		if ($c == 1) {$color = qq{'#D6D6D6'};}

		$totaux[8] += $comm{$j}->[8];
		$totaux[0] += $comm{$j}->[0];
		$totaux[1] += $comm{$j}->[1];
		$totaux[2] += $comm{$j}->[2];
		$totaux[3] += $comm{$j}->[3];
		$totaux[4] += $comm{$j}->[4];
		$totaux[6] += $comm{$j}->[6];
		$totaux[7] += $comm{$j}->[7];
		$totaux[9] += $comm{$j}->[9];
		$totaux[10] += $comm{$j}->[10];
		$totaux[11] += $comm{$j}->[11];
		$totaux[12] += $comm{$j}->[12];
		$totaux[5] += $comm{$j}->[5];
        $totaux[13] += $comm{$j}->[13];

		if (!$ordcommtot[$i]) {
			# Sous-total "machines"
			$sstot[8] += $comm{$j}->[8];
			$sstot[0] += $comm{$j}->[0];
			$sstot[1] += $comm{$j}->[1];
			$sstot[2] += $comm{$j}->[2];
			$sstot[3] += $comm{$j}->[3];
			$sstot[4] += $comm{$j}->[4];
			$sstot[6] += $comm{$j}->[6];
			$sstot[7] += $comm{$j}->[7];
			$sstot[9] += $comm{$j}->[9];
			$sstot[10] += $comm{$j}->[10];
			$sstot[11] += $comm{$j}->[11];
			$sstot[12] += $comm{$j}->[12];
            $sstot[5] += $comm{$j}->[5];
            $sstot[13] += $comm{$j}->[13];
			$boole_aff = 1;
		} else {
			# Sous-total "divers"
			$sstot_divers[8] += $comm{$j}->[8];
			$sstot_divers[0] += $comm{$j}->[0];
			$sstot_divers[1] += $comm{$j}->[1];
			$sstot_divers[2] += $comm{$j}->[2];
			$sstot_divers[3] += $comm{$j}->[3];
			$sstot_divers[4] += $comm{$j}->[4];
			$sstot_divers[6] += $comm{$j}->[6];
			$sstot_divers[7] += $comm{$j}->[7];
			$sstot_divers[9] += $comm{$j}->[9];
			$sstot_divers[10] += $comm{$j}->[10];
			$sstot_divers[11] += $comm{$j}->[11];
			$sstot_divers[12] += $comm{$j}->[12];
            $sstot_divers[5] += $comm{$j}->[5];
            $sstot_divers[13] += $comm{$j}->[13];
		}
		# Fam. commerciale, Parc, Nb contrats TOT, ARR, ACT
		if ($boole_aff) {
			$aff_mac .= qq|
			<tr bgColor=$color onmouseover="bgColor='#FFCC33'" onmouseout="bgColor=$color">
				<td align="left"><font class="PT">$a<b>$fmcomm{$j}</b>$f</font></td>
			|;

			if ($modl) {$aff_mac .= qq|<td align="right"><font class="PT">$htcomm{$j}</font></td>|;}

			$aff_mac .= qq|
                <td align="center" nowrap><font class="PT">$comm{$j}->[8]</font></td>
				<td align="center" nowrap><font class="PT">$comm{$j}->[0]</font></td>
				<td align="center" nowrap><font class="PT">$comm{$j}->[1]</font></td>
				<td align="center" nowrap><font class="PT">$comm{$j}->[2]</font></td>
                <td align="center" nowrap><font class="PT">$comm{$j}->[13]</font></td>
			|;
		} else {
			$aff_div .= qq|
			<tr bgColor=$color onmouseover="bgColor='#FFCC33'" onmouseout="bgColor=$color">
				<td align="left"><font class="PT">$a<b>$fmcomm{$j}</b>$f</font></td>
			|;

			if ($modl) {$aff_div .= qq|<td align="right"><font class="PT">$htcomm{$j}</font></td>|;}

			$aff_div .= qq|
                <td align="center" nowrap><font class="PT">$comm{$j}->[8]</font></td>
				<td align="center" nowrap><font class="PT">$comm{$j}->[0]</font></td>
				<td align="center" nowrap><font class="PT">$comm{$j}->[1]</font></td>
				<td align="center" nowrap><font class="PT">$comm{$j}->[2]</font></td>
                <td align="center" nowrap><font class="PT">$comm{$j}->[13]</font></td>
			|;
		}

		# Util. Inst.
		my $ut = 0;

		if ($comm{$j}->[8]) {$ut = (100 * ($comm{$j}->[2] + $comm{$j}->[13])) / $comm{$j}->[8];}

		$ut = sprintf("%.1f",$ut);

		if ($boole_aff) {$aff_mac .= qq|<td align="right" nowrap><font class="PT">$ut%</font></td>\n|;}
		else{$aff_div .= qq|<td align="right" nowrap><font class="PT">$ut%</font></td>\n|;}

		# CA, CA LOC, JR Loc
		my $aff  = sprintf("%.0f", $comm{$j}->[3]);
		my $aff2 = sprintf("%.1f", $comm{$j}->[4]);
		my $aff3 = sprintf("%.0f", $comm{$j}->[5]);

		if ($aff>0) {
			$aff = &fnombre2($aff,3,0);
			$aff3 = &fnombre2($aff3,3,0);
		}
		if ($boole_aff) {
			$aff_mac .= qq|
			<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff</span></font></td>
			<td align="center" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff3</span></font></td>
			<td align="center" nowrap><font class="PT">$aff2</font></td>
			|;
		} else {
			$aff_div .= qq|
			<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff</span></font></td>
			<td align="center" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff3</span></font></td>
			<td align="center" nowrap><font class="PT">$aff2</font></td>
			|;
		}

		# % util
		$ut=0;

		if (($resc[11])&&($comm{$j}->[8])) {$ut = (100*($comm{$j}->[4]))/($resc[11]*$comm{$j}->[8]);}

		$ut = sprintf("%.1f",$ut);

		if ($boole_aff) {
			$aff_mac .= qq|
			<td align="right" nowrap><font class="PT">$ut%</font></td>
			|;
		} else {
			$aff_div .= qq|
			<td align="right" nowrap><font class="PT">$ut%</font></td>
			|;
		}

		# Co�t
		# Calcul du co�t mensuel (repris plus bas)
		my $cout=0;
		my $affcout=0;

		if ($resc[11]) {
			$cout=$ctaux*$comm{$j}->[9]/100;
		}
		# Calcul du co�t sur la p�riode
		my $cout_periode = $cout / $nbjrmoy * $jourmsouv + $comm{$j}->[11];
		if ($resc[11]) {
			$cout += $comm{$j}->[12];
			$cout=sprintf("%.0f", $cout);
			$affcout=&fnombre2($cout,3,0);
		}
		if ($boole_aff) {
			$aff_mac .= qq|
			<td align="right" nowrap><font class="PT" color="red"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">|
			.&fnombre2($cout_periode,3,0).qq|</span></b></font></td>
			|;
		} else {
			$aff_div .= qq|<td nowrap></td>|;
		}

		# Ratio 2
		my $ratio2 = ($cout_periode == 0) ? 0 : $comm{$j}->[5] / $cout_periode;
		$ratio2 = sprintf("%.2f",$ratio2);
		if ($boole_aff) {
			$aff_mac .= qq|
			<td align="right" nowrap><font class="PT" color="red"><b>
			$ratio2</b></font></td>
			|;
		} else {
			$aff_div .= qq|<td nowrap></td>|;
		}

		# CA, CA LOC, JR Loc
		my $aff  = sprintf("%.0f", $comm{$j}->[6]);
		my $aff2 = sprintf("%.1f", $comm{$j}->[7]);
		my $aff3 = sprintf("%.0f", $comm{$j}->[10]);
		$aff     = &fnombre2($aff,3,0);
		$aff3    = &fnombre2($aff3,3,0);

		if ($boole_aff) {
			$aff_mac .= qq|
			<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff</span></font></td>
			<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff3</span></font></td>
			<td align="center" nowrap><font class="PT">$aff2</font></td>
			|;
		} else {
			$aff_div .= qq|
			<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff</span></font></td>
			<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff3</span></font></td>
			<td align="center" nowrap><font class="PT">$aff2</font></td>
			|;
		}

		# Px Jr, Px Jr LOC
		$ut=0;
		my $ut1=0;

		if ($comm{$j}->[7]!=0) {
			$ut=$comm{$j}->[6]/$comm{$j}->[7];
			$ut1=$comm{$j}->[10]/$comm{$j}->[7];
		}

		$ut=sprintf("%.1f",$ut);
		$ut1=sprintf("%.1f",$ut1);

		if ($boole_aff) {
			$aff_mac .= qq|
			<td align="center" nowrap><font class="PT"><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$ut</span></font></td>
			<td align="center" nowrap><font class="PT"><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$ut1</span></font></td>
			|;
		} else {
			$aff_div .= qq|
			<td align="center" nowrap><font class="PT"><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$ut</span></font></td>
			<td align="center" nowrap><font class="PT"><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$ut1</span></font></td>
			|;
		}

		# COUT
		if ($boole_aff) {
			$aff_mac .= qq|
			<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$affcout</span></font></td>
			|;
		}

		# CT Jr
		my $cjm=0;
		my $affcjm=0;

		if ($comm{$j}->[8]!=0) {
			$cjm=$cout/($nbjrmoy*$coef*$comm{$j}->[8]);
			$cjm=sprintf("%.1f",$cjm);
			$affcjm=$cjm;
		}
		if ($boole_aff) {
			$aff_mac .= qq|
			<td align="center" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$affcjm</span></font></td>
			|;
		}

		#RATIO
		my $pc=0;

		if ($cjm!=0) {$pc=$ut1/$cjm;}

		$pc=sprintf("%.2f",$pc);

		if ($boole_aff) {
			$aff_mac .= qq|
			<td align="right" nowrap><font class="PT">$pc</font></td>
		</tr>
			|;
		}

		$k++;
	}
  	# sous totaux
  	if ($modl eq "") {
	  	$aff_mac .= qq|
	  	<tr bgcolor="orange">
			<td align="left" nowrap><font class="PT"><b>TOTAL MACH</b></font></td>
            <td align="center" nowrap><font class="PT"><b>$sstot[8]</b></font></td>
			<td align="center" nowrap><font class="PT"><b>$sstot[0]</b></font></td>
			<td align="center" nowrap><font class="PT"><b>$sstot[1]</b></font></td>
			<td align="center" nowrap><font class="PT"><b>$sstot[2]</b></font></td>
            <td align="center" nowrap><font class="PT"><b>$sstot[13]</b></font></td>
		|;
		my $ut=0;

		if ($sstot[8]) {
			$ut=(100*($sstot[2]+$sstot[13]))/$sstot[8];
		}

		$ut=sprintf("%.1f",$ut);
		$aff_mac .= qq|<td align="right" nowrap><font class="PT"><b>$ut%</b></font></td>\n|;
		my $aff=sprintf("%.0f", $sstot[3]);
		my $aff2=sprintf("%.1f",$sstot[4]);
		my $aff3=sprintf("%.0f",$sstot[5]);

		if ($aff>0) {
			$aff=&fnombre2($aff,3,0);
			$aff3=&fnombre2($aff3,3,0);
		}

		$aff_mac .= qq|
			<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff</span></b></font></td>
			<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff3</span></b></font></td>
			<td align="center" nowrap><font class="PT"><b>$aff2</b></font></td>
		|;
		$ut=0;

		if (($resc[11])&&($sstot[8])) {
			$ut=(100*$sstot[4])/($resc[11]*$sstot[8]);
		}

		$ut=sprintf("%.1f",$ut);
		$aff_mac .= qq|<td align="right" nowrap><font class="PT"><b>$ut%</b></font></td>\n|;
		
		# Co�t p�riode
		my $couto=0;
		my $affcouto=0;

		if ($resc[11]) {
			$couto=$ctaux*$sstot[9]/100;
		}
		my $sstot_cout_periode = $couto / $nbjrmoy * $jourmsouv + $sstot[11];
		if ($resc[11]) {
			$couto += $sstot[12];
			$couto=sprintf("%.0f", $couto);
			$affcouto=&fnombre2($couto,3,0);
		}
		$aff_mac .= qq|<td align="right" nowrap><font class="PT" color="red"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">|
			.&fnombre2($sstot_cout_periode,3,0).qq|</span></b></font></td>\n|;
		
		# Ratio 2
		my $sstot_ratio2 = ($sstot_cout_periode == 0) ? 0 : $sstot[5] / $sstot_cout_periode;
		$ratio2 = sprintf("%.2f",$ratio2);
		$aff_mac .= qq|<td align="right" nowrap><font class="PT" color="red"><b>|
			.sprintf("%.2f",$sstot_ratio2).qq|</b></font></td>\n|;
			
		my $aff=sprintf("%.0f", $sstot[6]);
		my $aff2=sprintf("%.1f",$sstot[7]);
		my $aff3=sprintf("%.0f", $sstot[10]);
		$aff=&fnombre2($aff,3,0);
		$aff3=&fnombre2($aff3,3,0);
		$aff_mac .= qq|
			<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff</span></b></font></td>
			<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff3</span></b></font></td>
			<td align="center" nowrap><font class="PT"><b>$aff2</b></font></td>
		|;
		$ut=0;
		my $ut1=0;

		if ($sstot[7]) {
			$ut=$sstot[6]/$sstot[7];
			$ut1=$sstot[10]/$sstot[7];
		}

		$ut=sprintf("%.1f",$ut);
		$ut1=sprintf("%.1f",$ut1);
		$aff_mac .= qq|
			<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$ut</span></b></font></td>
			<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$ut1</span></b></font></td>
		|;
		#cout
		$aff_mac .= qq|<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$affcouto</span></b></font></td>\n|;
		my $cjm=0;
		my $affcjm=0;

		if ($sstot[8]!=0) {
			$cjm=$couto/($nbjrmoy*$coef*$sstot[8]);
			$cjm=sprintf("%.1f",$cjm);
			$affcjm=$cjm;
		}

		$aff_mac .= qq|<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$affcjm</span></b></font></td>\n|;
		my $pc=0;

		if ($cjm!=0) {
			$pc=$ut1/$cjm;
		}

		$pc=sprintf("%.2f",$pc);
		$aff_mac .= qq|
			<td align="right" nowrap><font class="PT"><b>$pc</b></font></td>
		</tr>
		|;
	}
	# Ajout 06/02/2004
	# sous totaux
  	if ($modl eq "") {
	  	$aff_div .= qq|
	  	<tr bgcolor="yellow">
			<td align="left" nowrap><font class="PT"><b>TOTAL DIVERS</b></font></td>
            <td align="center" nowrap><font class="PT"><b>$sstot_divers[8]</b></font></td>
			<td align="center" nowrap><font class="PT"><b>$sstot_divers[0]</b></font></td>
			<td align="center" nowrap><font class="PT"><b>$sstot_divers[1]</b></font></td>
			<td align="center" nowrap><font class="PT"><b>$sstot_divers[2]</b></font></td>
            <td align="center" nowrap><font class="PT"><b>$sstot_divers[13]</b></font></td>
		|;
		my $ut=0;

		if ($sstot_divers[8]) {$ut = (100 * ($sstot_divers[2] + $sstot_divers[13])) / $sstot_divers[8];}

		$ut=sprintf("%.1f",$ut);
		$aff_div .= qq|<td align="right" nowrap><font class="PT"><b>$ut%</b></font></td>\n|;
		my $aff=sprintf("%.0f", $sstot_divers[3]);
		my $aff2=sprintf("%.1f",$sstot_divers[4]);
		my $aff3=sprintf("%.0f",$sstot_divers[5]);

		if ($aff>0) {
			$aff=&fnombre2($aff,3,0);
			$aff3=&fnombre2($aff3,3,0);
		}

		$aff_div .= qq|
			<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff</span></b></font></td>
			<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff3</span></b></font></td>
			<td align="center" nowrap><font class="PT"><b>$aff2</b></font></td>
		|;
		$ut=0;

		if (($resc[11])&&($sstot_divers[8])) {
			$ut=(100*$sstot_divers[4])/($resc[11]*$sstot_divers[8]);
		}

		$ut=sprintf("%.1f",$ut);
		$aff_div .= qq|<td align="right" nowrap><font class="PT"><b>$ut%</b></font></td>\n|;
		
		# Co�t p�riode et ratio 2
		$aff_div .= qq|<td align="right" nowrap></td>
			<td align="right" nowrap></td>\n|;

		my $aff=sprintf("%.0f", $sstot_divers[6]);
		my $aff2=sprintf("%.1f",$sstot_divers[7]);
		my $aff3=sprintf("%.0f", $sstot_divers[10]);
		$aff=&fnombre2($aff,3,0);
		$aff3=&fnombre2($aff3,3,0);
		$aff_div .= qq|
			<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff</span></b></font></td>
			<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff3</span></b></font></td>
			<td align="center" nowrap><font class="PT"><b>$aff2</b></font></td>
		|;
		$ut=0;
		my $ut1=0;

		if ($sstot_divers[7]) {
			$ut=$sstot_divers[6]/$sstot_divers[7];
			$ut1=$sstot_divers[10]/$sstot_divers[7];
		}

		$ut=sprintf("%.1f",$ut);
		$ut1=sprintf("%.1f",$ut1);
		$aff_div .= qq|
			<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$ut</span></b></font></td>
			<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$ut1</span></b></font></td>
		|;
	}
# Ajout 06/02/2004

	print "$aff_div<tr><td colspan=21 align=center><hr></td></tr>$aff_mac<tr><td colspan=21 align=center><hr></td></tr>" if $modl eq "";
	print $aff_div.$aff_mac if $modl ne "";


  	# totaux
  	print qq|
  		<tr bgcolor="#C6CDC5">
			<td align="left" nowrap><font class="PT"><b>TOTAUX</b></font></td>
	|;

	if ($modl) {print "<td>&nbsp;</td>"};

	print qq|
            <td align="center" nowrap><font class="PT"><b>$totaux[8]</b></font></td>
			<td align="center" nowrap><font class="PT"><b>$totaux[0]</b></font></td>
			<td align="center" nowrap><font class="PT"><b>$totaux[1]</b></font></td>
			<td align="center" nowrap><font class="PT"><b>$totaux[2]</b></font></td>
            <td align="center" nowrap><font class="PT"><b>$totaux[13]</b></font></td>
	|;
	my $ut=0;

	if ($totaux[8]) {$ut = (100 * ($totaux[2] + $totaux[13])) / $totaux[8];}

	$ut=sprintf("%.1f",$ut);
	print qq|<td align="right" nowrap><font class="PT"><b>$ut%</b></font></td>\n|;
	my $aff=sprintf("%.0f", $totaux[3]);
	my $aff2=sprintf("%.1f",$totaux[4]);
	my $aff3=sprintf("%.0f",$totaux[5]);

	if ($aff>0) {$aff=&fnombre2($aff,3,0)};

	print qq|
			<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff</span></b></font></td>
			<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff3</span></b></font></td>
			<td align="center" nowrap><font class="PT"><b>$aff2</b></font></td>
	|;
	$ut=0;

	if (($resc[11])&&($totaux[8])) {
		$ut=(100*$totaux[4])/($resc[11]*$totaux[8]);
	}

	$ut=sprintf("%.1f",$ut);
	print qq|<td align="right" nowrap><font class="PT"><b>$ut%</b></font></td>\n|;
	print qq|<td nowrap></td>\n|;
	print qq|<td nowrap></td>\n|;
	my $aff=sprintf("%.0f", $totaux[6]);
	my $aff2=sprintf("%.1f",$totaux[7]);
	my $aff3=sprintf("%.0f", $totaux[10]);
	$aff=&fnombre2($aff,3,0);
	$aff3=&fnombre2($aff3,3,0);
	print qq|
			<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff</span></b></font></td>
			<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$aff3</span></b></font></td>
			<td align="center" nowrap><font class="PT"><b>$aff2</b></font></td>
	|;
	$ut=0;
	my $ut1=0;

	if ($totaux[7]) {
		$ut=$totaux[6]/$totaux[7];
		$ut1=$totaux[10]/$totaux[7];
	}

	$ut=sprintf("%.1f",$ut);
	$ut1=sprintf("%.1f",$ut1);
	print qq|
			<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$ut</span></b></font></td>
			<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$ut1</span></b></font></td>
	|;
	# cout
	my $couto=0;
	my $affcouto=0;

	if ($resc[11]) {
		$couto=$ctaux*$totaux[9]/100 + $totaux[12];
		$couto=sprintf("%.0f", $couto);
		$affcouto=&fnombre2($couto,3,0);
	}

	print qq|<td align="right" nowrap><font class="PT"><b>$affcouto</b></font></td>\n| if $modl ne "";
	my $cjm=0;
	my $affcjm=0;

	if ($totaux[8]!=0) {
		$cjm=$couto/($nbjrmoy*$coef*$totaux[8]);
		$cjm=sprintf("%.1f",$cjm);
		$affcjm=$cjm;
	}

	print qq|<td align="center" nowrap><font class="PT"><b>$affcjm</b></font></td>\n| if $modl ne "";
	my $pc=0;

	if ($cjm!=0) {
		$pc=$ut1/$cjm;
	}

	$pc=sprintf("%.2f",$pc);
	print qq|
			<td align="right" nowrap><font class="PT"><b>$pc</b></font></td>
		</tr>
	| if $modl ne "";
}

#
sub statjr1 {
	my ($debutms, $finms, $blt) = @_;
	my ($andm, $msdm, $jrdm) = split(/:/, $debutms);
	my $dm = Delta_Days(1900, 1, 1, $andm, $msdm, $jrdm);
	my ($anfm, $msfm, $jrfm) = split(/:/, $finms);
	my $fm = Delta_Days(1900, 1, 1, $anfm, $msfm, $jrfm);
	my $jourmsouv = Delta_Days($andm, $msdm, $jrdm, $anfm, $msfm, $jrfm);
	my $totj = $jourmsouv + 1;
	my $uvfer = &jour_fer_ag_cnx($G_PAYS, $G_AGENCE, $jrdm, $msdm, $andm, $jourmsouv, $DB);
	$jourmsouv += 1;
	my $ouvrwe = &jour_we($jourmsouv, $jrdm, $msdm, $andm);
	$jourmsouv = $jourmsouv - $ouvrwe - $uvfer;
	my $an = $anfm;
	my $ms = $msfm;
	my $ca = 0;
	my $forf = 0;
	my $mens = 0;
	my $cours = 0;
	my $arret = 0;
	my $tot = 0;
	my $transp = 0;
	my $carb = 0;
	my $ass = 0;
	my $durtot = 0;
	@req = ();
	my $clause = "FACOMAUTO";

	if ($modl) {$clause = qq{CONTRAT.MOMAAUTO};}
	if ($date < $date_chgt_fact) {@req = &reqhist($clause, $an, $ms);}
	else {@req = &reqhistnew($clause, $an, $ms);}

	$sth = $DB->prepare($req[0]);
	$sth->execute();

	while (my @data = $sth->fetchrow_array()) {
		if ($data[0] eq "") {$data[0] = 0;}

		$comm{$data[0]}->[3] += $data[1];
		$comm{$data[0]}->[6] += $data[1];

		if ($blt) {$ca += $data[1];}
		
		# Pour les machines hors parc, le co�t imput�
		# est �gal au CA / 2
		if ($data[2] == -1 && ($data[3] eq "LOC" || $data[3] eq "REM" || $data[3] eq "REALQ" || $data[3] eq "ASS" || $data[3] eq "GDR" || $data[3] eq "J+" || $data[3] eq "J-")) {
			if ($blt) {
				$comm{$data[0]}->[12] += $data[1] / 2;
				$comm{$data[0]}->[11] += $data[1] / 2;
			}
		}
	}

	$sth->finish;
	$sth = $DB->prepare($req[1]);
	$sth->execute();

	while (my @data = $sth->fetchrow_array()) {
		if ($data[0] eq "") {$data[0] = 0;}
		if ($data[1] eq "2") {
			$comm{$data[0]}->[2] += $data[2];

			if ($blt) {$cours += $data[2];}
		} else {
			$comm{$data[0]}->[1] += $data[2];

			if ($blt) {$arret += $data[2];}
		}
		if ($blt) {$tot += $data[2];}
		if ($comm{$data[0]}->[0]) {$comm{$data[0]}->[0] += $data[2];}
		else {$comm{$data[0]}->[0] = $data[2];}
	}

	$sth->finish;
	$sth = $DB->prepare($req[5]);
	$sth->execute();

	while (my @data = $sth->fetchrow_array()) {
		if ($data[0] eq "") {$data[0] = 0;}
		if ($comm{$data[0]}->[0] ne $data[1]) {
      	 		my $diff = $comm{$data[0]}->[0] - $data[1];
      	 		$comm{$data[0]}->[2] -= $diff;
      	 		$comm{$data[0]}->[0] -= $diff;

			if ($blt && $diff) {
      	 			$cours -= $diff;
      	 			$tot -= $diff;
      	 		}
      	 	}
	}

	$sth->finish;
	$sth = $DB->prepare($req[6]);
	$sth->execute();

	while (my @data = $sth->fetchrow_array()) {
		if ($data[0] eq "") {$data[0] = 0;}

  	 	$comm{$data[0]}->[2] += $data[1];
  	 	$comm{$data[0]}->[1] -= $data[1];
	}

	$sth->finish;
	$sth = $DB->prepare($req[2]);
	$sth->execute();

	while (my @data = $sth->fetchrow_array()) {
		if ($data[0] eq "") {$data[0] = 0;}

		$comm{$data[0]}->[4] = $data[1];
		$comm{$data[0]}->[7] = $data[1];
		$durtot += $data[1];
	}

	$sth->finish;
	$sth = $DB->prepare($req[3]);
	$sth->execute();
	my %contratcode = ();

	while (my @data = $sth->fetchrow_array()) {
		if ($date < $date_chgt_fact) {
			if ($data[0] eq "") {$data[0] = 0;}

			my ($anff, $msff, $jrff) = split(/-/, $data[1]);
			my ($anfd, $msfd, $jrfd) = split(/-/, $data[2]);

			my $durft = Delta_Days($anfd, $msfd, $jrfd, $anff, $msff, $jrff);

			if ($durft < 0) {next;}

			my $nbferief = &jour_fer_ag_cnx($G_PAYS, $G_AGENCE, $jrfd, $msfd, $anfd, $durft, $DB);
			my $nbjrwef  = 0;
  			my $joursupf = 0;
  			my $testjoursupf = &num_jour($jrff, $msff, $anff);

			if ($testjoursupf == 6) {$joursupf = 1;}
			if ($testjoursupf == 7) {$joursupf = 2;}

			$durft += 1;

			if ($durft == 1 && $testjoursupf == 7) {$joursupf = 1;}

			$nbjrwef = &jour_we($durft, $jrfd, $msfd, $anfd);
			$durft = $durft - $nbferief - $nbjrwef + $joursupf;

			if ($comm{$data[0]}->[4]) {
				$comm{$data[0]}->[4] += $durft;
				$comm{$data[0]}->[7] += $durft;
	      	 	} else {
				$comm{$data[0]}->[4] = $durft;
				$comm{$data[0]}->[7] = $durft;
	      	 	}

			$durtot += $durft;

			if ($blt) {
				if (!$contratcode{$data[3]}) {$mens++;$contratcode{$data[3]}=1;}
				else {$contratcode{$data[3]}++;}
			}
		} else {
			if ($data[0] eq "") {$data[0] = 0;}
			if ($comm{$data[0]}->[4]) {
				$comm{$data[0]}->[4] += $data[1];
				$comm{$data[0]}->[7] += $data[1];
	      	 	} else {
				$comm{$data[0]}->[4] = $data[1];
				$comm{$data[0]}->[7] = $data[1];
	      	 	}

	      	 	$durtot += $data[1];

			if ($blt) {$mens += $data[2];}
		}
      	 }

	$sth->finish;
	$sth=$DB->prepare($req[4]);
	$sth->execute();
	my %contratcode = ();

	while (my @data=$sth->fetchrow_array()) {
		if ($date < $date_chgt_fact) {
			if ($data[0] eq "") {$data[0] = 0;}

			my ($anff, $msff, $jrff) = split(/-/, $data[1]);
			my ($anfd, $msfd, $jrfd) = split(/-/, $data[2]);

			my $durft = Delta_Days($anfd, $msfd, $jrfd, $anff, $msff, $jrff);

			if ($durft < 0) {next;}

			my $nbferief = &jour_fer_ag_cnx($G_PAYS, $G_AGENCE, $jrfd, $msfd, $anfd, $durft, $DB);
			my $nbjrwef  = 0;
  			my $joursupf = 0;
  			my $testjoursupf = &num_jour($jrff, $msff, $anff);

			if ($testjoursupf == 6) {$joursupf = 1;}
			if ($testjoursupf == 7) {$joursupf = 2;}

			$durft += 1;

			if ($durft == 1 && $testjoursupf == 7) {$joursupf = 1;}

			$nbjrwef = &jour_we($durft, $jrfd, $msfd, $anfd);
			$durft = $durft - $nbferief - $nbjrwef + $joursupf;

			if ($comm{$data[0]}->[4]) {
				$comm{$data[0]}->[4] += $durft;
				$comm{$data[0]}->[7] += $durft;
	      	 	} else {
				$comm{$data[0]}->[4] = $durft;
				$comm{$data[0]}->[7] = $durft;
	      	 	}

			$durtot += $durft;

			if ($blt) {
				if (!$contratcode{$data[3]}) {$mens++;$contratcode{$data[3]}=1;}
				else {$contratcode{$data[3]}++;}
			}
		} else {
			if ($data[0] eq "") {$data[0] = 0;}
			if ($comm{$data[0]}->[4]) {
	      	 		$comm{$data[0]}->[4] += $data[1];
		      		$comm{$data[0]}->[7] += $data[1];
	      	 	} else {
				$comm{$data[0]}->[4] = $data[1];
				$comm{$data[0]}->[7] = $data[1];
			}

			$durtot += $data[1];

	      		if ($blt) {$forf += $data[2];}
	      	}
	}

	$sth->finish;
	$sth = $DB->prepare($req[9]);
	$sth->execute();

	while (my @data = $sth->fetchrow_array()) {
		if ($data[0] eq "") {$data[0] = 0;}
		if ($comm{$data[0]}->[10]) {
			$comm{$data[0]}->[10] += $data[1];
			$comm{$data[0]}->[5] += $data[1];
		} else {
			$comm{$data[0]}->[10] = $data[1];
			$comm{$data[0]}->[5] = $data[1];
		}

		$caloc += $data[1];
	}

	$sth->finish;

	# carb transp
	if ($blt) {
		my $where = "";
		my $joined = "";

		if ($modl) {
			$where = qq{AND FAMILLEMACHINE.FACOMAUTO="$modl"};
			$joined = qq{LEFT join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO};
		}

		my $req = qq|
		select LIGNEREFARTICLE,
		  SUM(ROUND(ROUND(LIGNEQTITE, |.$tabSageFormats->{'quantity'}.qq|)*ROUND(LIGNEPRIX, |.$tabSageFormats->{'price'}.qq|), |.$tabSageFormats->{'amount'}.qq|)),
		  count(ENTETEFACTURE.ENTETEAUTO)
		from ENTETELIGNE
		left join LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
		left join ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
		$joined
		where YEAR(DATE)="$an"
		AND MONTH(DATE)="$ms"
		AND ENTETEFACTURE.AGAUTO="$G_AGENCE"
		$where
		GROUP BY LIGNEREFARTICLE
		|;

		$sth = $DB->prepare($req);
		$sth->execute();

		while (my @data = $sth->fetchrow_array()) {
		      	if ($data[0] =~ /^TRANS/ || $data[0] eq "SERT") {$transp += $data[1];}
			if ($data[0] eq "CARB") {$carb = $data[1];}
		}

		$sth->finish;
	}

	# jour<5
	my $nbdureems = 0;
	my $nbdureeplus = 0;

	if ($blt) {
		my $req = $req[7];
		$sth = $DB->prepare($req);
		$sth->execute();

		if ($date < $date_chgt_fact) {
			while (my @data = $sth->fetchrow_array()) {
				my ($dareel, $dmreel, $djreel) = split(/-/, $data[1]);
				my ($fareel, $fmreel, $fjreel) = split(/-/, $data[2]);
				my $dureereel = Delta_Days($dareel, $dmreel, $djreel, $fareel, $fmreel, $fjreel);
				my $nbferiereel = &jour_fer_ag_cnx($G_PAYS, $G_AGENCE, $djreel, $dmreel, $dareel, $dureereel, $DB);
				my $testjoursupreel = &num_jour($fjreel, $fmreel, $fareel);

				if ($testjoursupreel == 6) {my $joursupreel = 1};
				if ($testjoursupreel == 7) {my $joursupreel = 2};

				my ($tan, $tms, $tjr) = split(/\:/, $date);

				if (Delta_Days($tan, $tms, $tjr, $fareel, $fmreel, $fjreel) > 0) {my $joursupreel = 0;}

				$dureereel += 1;

				if ($dureereel == 1 && $testjoursupreel == 7) {my $joursupreel = 1;}

				my $nbjrwereel = &jour_we($dureereel, $djreel, $dmreel, $dareel);
				$dureereel = $dureereel + $data[3] - $data[4] - $nbferiereel - $nbjrwereel + $joursupreel;

				if ($dureereel < 5) {$nbdureems++;}
			}
		} else {($nbdureems) += $sth->fetchrow_array();}

		$sth->finish;
	}

	# Assurance
	if ($blt) {
		my $req = $req[8];
		$sth = $DB->prepare($req);
		$sth->execute();
		($ass) = $sth->fetchrow_array();
		$sth->finish;
	}

	return ($tot, $ca, $transp, $carb, $nbdureems, $nbdureeplus, $cours, $arret, $ass, $mens, $forf, $jourmsouv, $durtot);
}

# Les diverses requ�tes de stats et CA et taux d'utilisation
# calcul�s
sub reqhist {
	my ($chp, $an, $ms) = @_;
	my ($db, $dba, $fn, $fna) = &debfinmois($ms, $an);
	my @req = ();
	my $where = "";

	if ($modl) {$where = qq{AND FAMILLEMACHINE.FACOMAUTO="$modl"}}

	# CA
	$req[0] = qq{
	select $chp, SUM(ENTETEMONTANTFR)
	FROM ENTETEFACTURE
	LEFT join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(ENTETEDATED)="$an"
	AND MONTH(ENTETEDATED)="$ms"
	AND ENTETEFACTURE.AGAUTO="$G_AGENCE"
	$where
	GROUP BY $chp
	};

	# NB PAR ETAT
	$req[1] = qq{
	select $chp, LIETCONTRATAUTO, count(DISTINCT ENTETEFACTURE.CONTRATCODE)
	FROM ENTETEFACTURE
	LEFT join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE ENTETEFACTURE.AGAUTO="$G_AGENCE"
	AND YEAR(ENTETEDATED)="$an"
	AND MONTH(ENTETEDATED)="$ms"
	$where
	GROUP BY $chp, LIETCONTRATAUTO
	};

	# Dur�e contrat jour ENTETEABO=0
	$req[2] = qq{
	select $chp, SUM(LIGNEQTITE)
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE="LOC"
	AND ENTETEABO="0"
	AND ENTETEFACTURE.AGAUTO="$G_AGENCE"
	$where
	GROUP BY $chp
	};

	$req[3] = qq{
	select $chp, ENTETEDATEF, ENTETEDATED, ENTETEFACTURE.CONTRATCODE
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE="LOC"
	AND ENTETEABO="-1"
	AND MOTARIFMSFR !=0
	AND ENTETEFACTURE.AGAUTO="$G_AGENCE"
	$where
	};

	$req[4] = qq{
	select $chp, ENTETEDATEF, ENTETEDATED, ENTETEFACTURE.CONTRATCODE
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE="LOC"
	AND ENTETEABO="-1"
	AND MOFORFAITFR !=0
	AND ENTETEFACTURE.AGAUTO="$G_AGENCE"
	$where
	};

	# �liminer les doublons arr�t de contrat entre 2 facturation
	$req[5] = qq{
	select $chp, count(DISTINCT ENTETEFACTURE.CONTRATCODE)
	FROM ENTETEFACTURE
	LEFT join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(ENTETEDATED)="$an"
	AND MONTH(ENTETEDATED)="$ms"
	AND ENTETEFACTURE.AGAUTO="$G_AGENCE"
	$where
	GROUP BY $chp
	};

	# Contrat en cours de grue
	$req[6] = qq{
	select $chp, count(*)
	FROM CONTRAT
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE CONTRATOK="-2"
	AND CONTRATDD<="$fn"
	AND CONTRATDR>"$fn"
	AND CONTRAT.AGAUTO="$G_AGENCE"
	$where
	GROUP BY $chp
	};

	# Nbs contrats < 5
	$req[7] = qq{
	select $chp, CONTRATDD, CONTRATDR, CONTRATJOURSPLUS, CONTRATJOURSMOINS
	FROM CONTRAT
	left join MONTANT on MONTANT.MOAUTO=CONTRAT.MOAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE CONTRATOK in ("-1","-2")
	AND LIETCONTRATAUTO != 6
	AND CONTRATDD<="$fn"
	AND CONTRATDR>="$db"
	AND AGAUTO="$G_AGENCE"
	and MOTARIFMSFR = 0
	$where
	};

	# Nb Ass
	$req[8] = qq{
	select count(DISTINCT ENTETEFACTURE.CONTRATCODE)
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE="ASS"
	AND ENTETEFACTURE.AGAUTO="$G_AGENCE"
	$where
	};

	# CA LOC
	$req[9] = qq|
	SELECT $chp,
	   SUM(ROUND(ROUND(LIGNEQTITE, |.$tabSageFormats->{'quantity'}.qq|)*ROUND(LIGNEPRIX, |.$tabSageFormats->{'price'}.qq|), |.$tabSageFormats->{'amount'}.qq|)),
	   count(distinct ENTETEFACTURE.ENTETEAUTO)
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE in ("LOC", "ASS", "GDR", "REM")
	AND ENTETEFACTURE.AGAUTO="$G_AGENCE"
	$where
	GROUP BY $chp
	|;

	return @req;
}

# Idem reqhist mais avec nouvelle fa�on de calculer
# suite au changement de facturation du 01/01/2004 pour la FRANCE
# Voir avec les autres pays pour le dispatchage
sub reqhistnew {
	my ($chp, $an, $ms) = @_;
	my ($db, $dba, $fn, $fna) = &debfinmois($ms, $an);
	my @req = ();
	my $where = "";

	if ($modl) {$where = qq{AND FAMILLEMACHINE.FACOMAUTO = "$modl"};}

	# CA
	$req[0] = qq|
	select $chp,
	   ROUND(ROUND(LIGNEPRIX, |.$tabSageFormats->{'price'}.qq|)*ROUND(LIGNEQTITE, |.$tabSageFormats->{'quantity'}.qq|), |.$tabSageFormats->{'amount'}.qq|),
	   CONTRAT.CONTRATHORSPARC, LIGNEREFARTICLE
	from ENTETELIGNE
	left join ENTETEFACTURE on ENTETELIGNE.ENTETEAUTO=ENTETEFACTURE.ENTETEAUTO
	left join LIGNEFACTURE on LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	left join CONTRAT on ENTETEFACTURE.CONTRATAUTO=CONTRAT.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where ENTETEFACTURE.AGAUTO="$G_AGENCE"
	and YEAR(DATE)="$an"
	and MONTH(DATE)="$ms"
	$where
#	group by $chp, CONTRAT.CONTRATHORSPARC
	|;

	# NB PAR ETAT
	$req[1] = qq{
	select $chp, LIETCONTRATAUTO, count(DISTINCT ENTETEFACTURE.CONTRATCODE)
	from ENTETELIGNE
	left join ENTETEFACTURE on ENTETELIGNE.ENTETEAUTO=ENTETEFACTURE.ENTETEAUTO
	left join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where ENTETEFACTURE.AGAUTO="$G_AGENCE"
	and YEAR(DATE)="$an"
	and MONTH(DATE)="$ms"
	$where
	group by $chp, LIETCONTRATAUTO
	};

	# Dur�e contrat jour ENTETEABO=0
	$req[2] = qq{
	select $chp, SUM(LIGNEQTITE)
	from ENTETELIGNE
	left join LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	left join ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	left join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where YEAR(DATE)="$an"
	and MONTH(DATE)="$ms"
	and LIGNEREFARTICLE in ("LOC", "J+", "J-")
	and ENTETEABO="0"
	and ENTETEFACTURE.AGAUTO="$G_AGENCE"
	$where
	group by $chp
	};

	# Dur�e et nombre contrat mensuel ENTETEABO=-1
	$req[3] = qq{
	select $chp, SUM(LIGNEQTITE), count(distinct ENTETEFACTURE.CONTRATCODE)
	from ENTETELIGNE
	left join LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	left join ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	left join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	left join MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where YEAR(DATE)="$an"
	and MONTH(DATE)="$ms"
	and LIGNEREFARTICLE in ("LOC", "J+", "J-")
	and ENTETEABO="-1"
	and MOTARIFMSFR != 0
	and ENTETEFACTURE.AGAUTO="$G_AGENCE"
	$where
	group by $chp
	};

	# Dur�e et nombre contrat forfait ENTETEABO=-1
	$req[4] = qq{
	select $chp, SUM(LIGNEQTITE), count(distinct ENTETEFACTURE.CONTRATCODE)
	from ENTETELIGNE
	left join LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	left join ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	left join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	left join MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where YEAR(DATE)="$an"
	and MONTH(DATE)="$ms"
	and LIGNEREFARTICLE in ("LOC", "J+", "J-")
	and ENTETEABO="-1"
	and MOFORFAITFR != 0
	and ENTETEFACTURE.AGAUTO="$G_AGENCE"
	$where
	group by $chp
	};

	# �liminer les doublons arr�t de contrat entre 2 facturation
	$req[5] = qq{
	select $chp, count(DISTINCT ENTETEFACTURE.CONTRATCODE)
	from ENTETELIGNE
	left join ENTETEFACTURE on ENTETELIGNE.ENTETEAUTO=ENTETEFACTURE.ENTETEAUTO
	left join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where YEAR(DATE)="$an"
	and MONTH(DATE)="$ms"
	and ENTETEFACTURE.AGAUTO="$G_AGENCE"
	$where
	group by $chp
	};

	# Contrat en cours de grue
	$req[6] = qq{
	select $chp, count(*)
	from CONTRAT
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where CONTRATOK="-2"
	and CONTRATDD<="$fn"
	and CONTRATDR>"$fn"
	and CONTRAT.AGAUTO="$G_AGENCE"
	$where
	group by $chp
	};

	# Nbs contrats < 5
	# A voir ici s'il n'est pas mieux de faire la liaison entre entetefacture et contrat sur CONTRATAUTO
	$req[7] = qq{
	select count(distinct ENTETEFACTURE.CONTRATCODE)
	from ENTETELIGNE
	left join ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	left join CONTRAT ON CONTRAT.CONTRATCODE=ENTETEFACTURE.CONTRATCODE
	left join MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where YEAR(DATE)="$an"
	and MONTH(DATE)="$ms"
	and CONTRATOK in (-1, -2)
	and MOTARIFMSFR = 0
	and ENTETEFACTURE.AGAUTO="$G_AGENCE"
	and CONTRATDUREE<5
	$where
	};

	# Nb Ass
	$req[8] = qq{
	select count(DISTINCT ENTETEFACTURE.CONTRATCODE)
	from ENTETELIGNE
	left join LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	left join ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	left join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where YEAR(DATE)="$an"
	and MONTH(DATE)="$ms"
	and LIGNEREFARTICLE="ASS"
	and ENTETEFACTURE.AGAUTO="$G_AGENCE"
	$where
	};

	# CA LOC
	$req[9] = qq|
	select $chp,
	   SUM(ROUND(ROUND(LIGNEQTITE, |.$tabSageFormats->{'quantity'}.qq|)*ROUND(LIGNEPRIX, |.$tabSageFormats->{'price'}.qq|), |.$tabSageFormats->{'amount'}.qq|)),
        count(distinct ENTETEFACTURE.ENTETEAUTO)
	from ENTETELIGNE
	left join LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	left join ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	left join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	left join MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where YEAR(DATE)="$an"
	and MONTH(DATE)="$ms"
	and LIGNEREFARTICLE in ("LOC", "REM", "ASS", "GDR", "J+", "J-")
	and ENTETEFACTURE.AGAUTO="$G_AGENCE"
	$where
	GROUP BY $chp
	|;

	return @req;
}

# historique
sub hissstatistique {
	my @data = split(/\:/, $date);
	my ($db, $dba, $fn, $fna) = &debfinmois($data[1], $data[0]);

	@rest = &statjr1($db, $date, 1);

	for (my $i=0;$i<@rest;$i++) {push(@resc, $rest[$i]);}
}

# Retourne 4 dates,
# d�but mois de 2 formats diff�rents
# fin mois de 2 formats diff�rents
# ("2003:05:01","01/05/2003","2003:05:31","31/05/2003")
sub debfinmois {
	my ($mois, $an) = @_;
	my $debutms = "$an:$mois:01";
	my $debaff = "01/$mois/$an";
	my $finmois = Days_in_Month($an, $mois);
	my $finms  = "$an:$mois:$finmois";
	my $finaff = "$finmois/$mois/$an";

	return ($debutms, $debaff, $finms, $finaff);
}

# R�cup�re les mois de facturation d'une agence donn�e
# Pour construire le menu d�roulant de visu
sub jourdate {
	my $html   = "";

	$html .= $cgi->start_form;
	$html .= $cgi->hidden("clauto",$clauto);
	$html .= qq{<select name="date" onchange="submit()">\n};
	$html .= qq{<option value="">$mess[5024]\n};

	my $req = qq{
        SELECT MONTH(DATE),YEAR(DATE)
        FROM ENTETELIGNE
        LEFT join ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
        WHERE AGAUTO="$G_AGENCE"
            AND DATE < CONCAT_WS(":",YEAR(NOW()),MONTH(NOW()),"01")
            AND DATE >= "$tdt[0]-$tdt[1]-$tdt[2]"
        GROUP BY MONTH(DATE),YEAR(DATE) ORDER BY DATE DESC};
	$sth = $DB->prepare($req);
	$sth->execute();

	while (my @data = $sth->fetchrow_array()) {
		my $selected = "";
		my ($db,$dba,$fn,$fna) = &debfinmois($data[0],$data[1]);

		if ($fn eq $date) {$selected = " selected";};

		$html .= qq{<option value="$fn"$selected>$dba -- $fna\n};
	}

	$sth->finish;

	$html .= qq{</select>\n};
	$html .= qq{</form>\n};

	return $html;
}

sub retour_date {
	my ($lepays) = @_;
	my $df;

	if ($lepays eq "FR") {$df = "2004:01:01";}
	if ($lepays eq "PT") {$df = "2004:03:01";}
	if ($lepays eq "ES") {$df = "2004:03:01";}
	if ($lepays eq "MA") {$df = "2004:03:01";}
	if ($lepays eq "IT") {$df = "2004:03:01";}

	return $df;
}

sub fnombre2 {
	my ($nb, $sep, $dec) = @_;
	my ($long);
	my $negatif = ($nb =~ tr/-//);
	$nb .= ".";
	# ----- suppression des caracteres <> chiffres et '.'
	$nb =~ tr/(0-9.)//cd;
	# ----- longueur doit etre un multiple de 3
	$long = length($nb);
	$long = $long + (6 - $long % 3);
	$nb = sprintf("%$long.2f", $nb);
	# -----  remplace les espaces par des 'X'
	$nb =~ s/ /X/g;

	if ($sep) {
		# ----- un espace tous les 3 caracteres
		$nb =~ s/(\w{3})/$1 /g;
		# ----- suppression de l'espace devant le point decimal
		$nb =~ s/ \././;
	}

	# ----- suppression des 'non chiffres' de debut
	$nb =~ s/( ?X ?)*//g;
	# ----- suppression des decimales si = '.00'

	if (!$dec) {$nb=~s/\.00//;}

	$nb = "-".$nb if $negatif;

	return ($nb);
}
