#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';

use CGI;
use autentif;
use calculdate;
use accesbd;
use varglob;
use Date::Calc qw(Delta_Days Days_in_Month Add_Delta_Days);
use message;
use tracetemps;

use JSON::XS;

use LOC::Util;
use LOC::Date;
use LOC::Insurance::Type;
use LOC::Common::Rent;

# Initialisation du chrono
my $timer = &init_chrono();


# Connexion
my $DB = &Environnement("STATS");
&verif_droit();


# R�cup�ration des formats Sage (quantit�, prix et montant)
my $charac;
my $req = '
SELECT IFNULL(ccy_value, cgl_value)
FROM AUTH.p_characteristic
LEFT JOIN AUTH.p_characteristic_global ON chr_id = cgl_chr_id
LEFT JOIN AUTH.p_characteristic_country ON (cgl_chr_id = ccy_chr_id AND ccy_cty_id = "' . $G_PAYS . '")
WHERE chr_code = "FORMATSSAGE"';

my $sth = $DB->prepare($req);
$sth->execute();

while (my @data = $sth->fetchrow_array())
{
    $charac = $data[0];
}

$sth->finish();

my $tabSageFormats = {'quantity' => 2, 'price' => 4, 'amount' => 2};
if ($charac ne '')
{
    $tabSageFormats = JSON::XS->new->decode($charac);
}

# Initialisation CGI
my $cgi = new CGI;
$cgi->autoEscape(undef);
my @mess = &message($G_PAYS);
my $titre = $mess[3600];
my @tdt = qw(2002 03 23);

&ente($titre, $titre, $G_PENOM, $G_PEPRENOM);

# Message d'attente
print qq{<div id="attente" style="visibility: hidden; position:absolute; width:400px; height:200px;">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr valign="middle">
		<td align="center">
		<table cellpadding="1" cellspacing="0" border="0">
			<tr>
				<td bgcolor=black>
				<table border="0" width="100%" cellpadding="5" cellspacing="0">
					<tr valign="middle">
						<td bgcolor=\"#FFFFCC\" align="center">
						<font class=\"PT2\">
							Veuillez patienter pendant le chargement des statistiques...<br>
							Merci de ne pas cliquer sur le bouton <i>Actualiser</i> et de ne pas appuyer sur F5.<p>
							<img src="$URL_IMG/sablier.gif" border="0">
						</font>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</div>

<script>
var x = (document.body.clientWidth - 400) / 2;
var y = (document.body.clientHeight - 200) / 2;
attente.style.left = x + "px";
attente.style.top = y + "px";
attente.style.visibility = 'visible';
attente.style.cursor = 'wait';
</script>};

$|=1;

# Date est la fin de mois du mois choisi sur le menu d�roulant
# donc quand historique choisi
my $date   = $cgi->param('date');
#
# Tranche D'affichage (groupe choisi)
#
my $groupe  = $cgi->param('groupe');
my $lg      = 0;
#
# Nb Agence affich�e par parge
#
my $tranche = 2;

if ($groupe eq "") {$groupe = 0;}

my ($datedebutmois, $datedebutmoisaff, $datefinmois, $datefinmoisaff, $datedujour, $datedujouraff, $mimois, $moisencours_ou_histo) = &datemois;
my $day = $mimois;
my $dteFinMois = $mimois;
$dteFinMois =~ s/\:/\-/g;
my $tauxE = tauxLocalToEuro($G_PAYS, $dteFinMois);

# Init Variable
# Donn�e Des Tables Des Famille Comptable
my $nbjrmoy = 21;
my $coef    = 0.65; # 0.7;
my $cout;
my %mach_mod;
my %mach_cpt;
my %compta;
my @ordcpt = ();
my @ordtot = ();
my %famille;
my %totmc;
my %gpag;
my @agence = ();
my %casse;
my %durtot;
my %caloc = ();
my $date_chgt_fact = &retour_date($G_PAYS);
&lst_agence($datedebutmois, $datefinmois);
&nb_mach($mimois);
&casse($mimois);

my $ctaux = &cout_taux($mimois);
my %rest;
my %resc;

#
# Programme Principal
#
&entete;

for (my $i=0; $i<@agence; $i++) {
	if ($groupe ne $gpag{$agence[$i]}) {next};
	if ($moisencours_ou_histo) {&hissstatistique($agence[$i]);}
	else {&statistique($agence[$i]);}

	&corps($agence[$i]);
}

print qq{
	<tr>
		<td colspan=12>&nbsp;</td>
	</tr>
</table>

<script>
attente.style.visibility='hidden';
attente.style.cursor = 'default';
</script>
};

$DB->disconnect;

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

# Pied de la page HTML
&Pied($delta_time);


# ex : casse('2003:5:31')
# Somme les casses accept�es en facturation et pass�es en facturation du mois en cours
# ne marche que depuis la mise en place de CADATEFACT donc ant�rieur fauss� (>=01/05/2003)
sub casse
{
    my ($date) = @_;

    # Casse
    my $req = '
SELECT CONTRAT.AGAUTO, SUM(CAPXFINALEU)
FROM CASSE
LEFT JOIN CONTRAT
ON CONTRAT.CONTRATAUTO=CASSE.CONTRATAUTO
WHERE CADATEFACT IS NOT NULL
    AND YEAR(CADATEFACT)=YEAR("' . $date . '")
    AND MONTH(CADATEFACT)=MONTH("' . $date . '")
GROUP BY CONTRAT.AGAUTO';

    my $sth = $DB->prepare($req);
    $sth->execute();

    while (my @data = $sth->fetchrow_array())
    {
        $casse{$data[0]} = $data[1];
    }

    $sth->finish();

    # Factures de remise en �tat
    $req = '
SELECT rpi_agc_id, SUM(rpi_total)
FROM f_repairinvoice
WHERE YEAR(rpi_date) = YEAR("' . $date . '")
    AND MONTH(rpi_date) = MONTH("' . $date . '")
GROUP BY rpi_agc_id';

    $sth = $DB->prepare($req);
    $sth->execute();
    while (my @data = $sth->fetchrow_array())
    {
        $casse{$data[0]} += $data[1];
    }

    $sth->finish();
}

# ex : cout_taux('2003:05:21')
# pr�vision �volution des taux
# en fonction des dates il faut garder
# la tra�abilit� des �volutions pour garder
# une bonne estimation
sub cout_taux {
#	print qq|<br>cout_taux|;
	my ($dtt) = @_;
	my ($ant, $mst, $jrt) = split(/:/, $dtt);
	my @tx = ();
	my @dt = ();

	# ordonner de fa�on croissante les dates et taux
	# en fonction des changements des taux
	$dt[0] = qq{13/01/2003};
	$tx[0] = 1.92;

	my $t = @tx;
	$t--;
	my $tx = $tx[$t];

	for (my $i=$t;$i>=0;$i--) {
		my ($jrc, $msc, $anc) = split(/\//, $dt[$i]);
		my $diff1 = Delta_Days(1900, 1, 1, $ant, $mst, $jrt);
		my $diff2 = Delta_Days(1900, 1, 1, $anc, $msc, $jrc);
		my $diff  = $diff1 - $diff2;

		if ($diff <= 0) {$tx = $tx[$i];}
		else {next;}
	}

	return $tx;
}

# liste des agences
# Si historique on sort la liste des agences qui ont �t� factur�es pour le mois choisi
# Si mois en cours on cherche ds la loc tous les agences qui ont des contrats en cours
sub lst_agence {
	my @param = @_;
	my $req;
	my $posi = 0;

	if ($date) {
		$req = qq{
		select AGAUTO
		FROM ENTETELIGNE
		LEFT join ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
		WHERE MONTH(DATE)=MONTH("$date")
		AND YEAR(DATE)=YEAR("$date")
		GROUP BY AGAUTO
		ORDER BY AGAUTO
		};
	} else {
		$req = qq{
		select AGAUTO
		FROM CONTRAT
		WHERE CONTRATOK in ("-1","-2")
		AND CONTRAT.LIETCONTRATAUTO in ("2","3","4","7")
		AND CONTRATDD <= "$param[1]"
		AND CONTRATDR >= "$param[0]"
		GROUP BY AGAUTO
		ORDER BY AGAUTO
		};
	}

	my $sth = $DB->prepare($req);
	$sth->execute();

	# $tranche sert pour l'affichage des stats
	# ici pour l'instant est � 3 par d�faut
	# donc group� par 3 agences
	while (my @data = $sth->fetchrow_array()) {
		push (@agence, $data[0]);
		my $div = $posi % $tranche;

		if (($div == 0) && $posi) {$lg++;}

		# Hachage du num�ro de groupe
		# des agences
		$gpag{$data[0]}  = $lg;
		$totmc{$data[0]} = 0;
		$posi++;
	}

	$sth->finish;
}

# ex : datemois()
# Renvoie sous 2 formats diff.
# la date de d�but mois
# la date de fin mois
# ("2003:05:01","01/05/2003","2003:05:31","31/05/2003","2003:05:21","21/05/2003","2003:05:31 ou date pass�e en param�tre","0 ou 1")
sub datemois {
	my ($jour, $mois, $an) = (localtime(time))[3,4,5];
	$an += 1900;
	$mois += 1;
	my $debutms = "$an:$mois:01";
	my $finmois = Days_in_Month($an, $mois);
	my $tsdate  = 0;
	my $mmois   = "$an:$mois:$finmois";

	if ($date) {
		my ($tsan, $tsms, $tsjr) = split(/\:/, $date);

		if (($an ne $tsan) || ($mois ne $tsms) || ($jour ne $tsjr)) {
	    		$tsdate = 1;
			$mmois  = "$tsan:$tsms:$tsjr";
	    	}
	}

	my $finms  = "$an:$mois:$finmois";
	my $dt     = "$an:$mois:$jour";
	my $dtaff  = "$jour/$mois/$an";
	my $debaff = "01/$mois/$an";
	my $finaff = "$finmois/$mois/$an";

	return ($debutms, $debaff, $finms, $finaff, $dt, $dtaff, $mmois, $tsdate);
}

# R�cup les contrats en cours et actifs et arr�t�s et archiv�s
# Etablit les calculs une fois la requ�te faite
# Avec ou non le mod�le en condition
sub statjr {
	my @param   = @_;
	my $debutms = $param[0];
	my $finms   = $param[1];
	my $posi    = $param[2];
	my $agence  = $param[3];
	my $requete;
	$requete = qq{
	select CONTRATDD, CONTRATDR, CONTRATJOURSPLUS, CONTRATJOURSMOINS, MOPXJOURFR,
	MOTARIFMSFR, CONTRATTYPEASSURANCE, MOTRANSPORTDFR, MOTRANSPORTRFR, CAMONTANTFR,
	MOFORFAITFR, CONTRATDUREE, FAMACOMPTAAUTO, SOLOAUTO, LIETCONTRATAUTO,
	ETATFACTURE, MAHORSPARC, CONTRATPASSURANCE, ity_flags, CONTRATTYPERECOURS, CONTRATRECOURSVAL
	FROM CONTRAT
	LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	LEFT JOIN CARBURANT ON CARBURANT.CARBUAUTO=CONTRAT.CARBUAUTO
	LEFT JOIN AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT JOIN FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	LEFT JOIN AUTH.`MACHINE` ON MACHINE.MAAUTO=CONTRAT.MAAUTO
	LEFT JOIN p_insurancetype ON ity_id = CONTRATTYPEASSURANCE
    left join p_appealtype ON apl_id = CONTRATTYPERECOURS
	where CONTRAT.AGAUTO="$agence"
	AND CONTRATOK in ("-1","-2")
	AND CONTRAT.LIETCONTRATAUTO in ("1","2","3","4","7")
	AND DATE(CONTRATDD) <= "$finms"
	AND DATE(CONTRATDR) >= "$debutms"
    AND CONTRAT.MAAUTO != 0
    AND CONTRATMACHATTR = -1
	and (ETATFACTURE in ("P", "") or ETATFACTURE is null)
	ORDER BY FAMACOMPTAAUTO
	};
	my ($andm, $msdm, $jrdm) = split(/:/, $debutms);
	my $dm = Delta_Days(1900, 1, 1, $andm, $msdm, $jrdm);
	my ($anfm, $msfm, $jrfm) = split(/:/, $finms);
	my $fm = Delta_Days(1900, 1, 1, $anfm, $msfm, $jrfm);
	my $jourmois = Delta_Days($andm,$msdm,$jrdm,$anfm,$msfm,$jrfm);
	my $jourmoisd = $jourmois+1;
	my $jourmsouv = $jourmois;
	my $uvfer = &jour_fer_ag_cnx($G_PAYS, $agence, $jrdm, $msdm, $andm, $jourmsouv, $DB);
	$jourmsouv += 1;
	my $ouvrwe = &jour_we($jourmsouv,$jrdm,$msdm,$andm);
	$jourmsouv = $jourmsouv - $ouvrwe - $uvfer;
	my $sth = $DB->prepare($requete);
	$sth->execute();
	my $nbrow = $sth->rows;
    my $nbcontlivr = 0;
	my $nbcontcours = 0;
	my $nbcontarret = 0;
	my $nbdureems = 0;
	my $nbdureeplus = 0;
	my $nbforfait = 0;
	my $nbtarifms = 0;
	my $nbasscomp = 0;
	my $total = 0.0;
	my $catr = 0;
	my $cacarb = 0;
	my $temp = Days_in_Month($anfm, $msfm);
	my $caloc = 0;
	
	while (my @data = $sth->fetchrow_array()) {
		# FAMACOMPTAAUTO
		my $fam = $data[12];

		# SOLOAUTO
		if ($data[13] != 0) {$fam = 0;}

		# CONTRATDD
		my ($jrdd, $msdd, $andd) = &datetojjmsan($data[0]);
		my $dd = Delta_Days(1900, 1, 1, $andd, $msdd, $jrdd);
		# CONTRATDR
		my ($jrdf, $msdf, $andf) = &datetojjmsan($data[1]);
		my $df = Delta_Days(1900, 1, 1, $andf, $msdf, $jrdf);
		my $dureecont = $df - $dd + 1;
		my $boolms = 0;
		my $ta = 0.0;
		my $tr = 0.0;
		my $carbu = 0.0;
		# CONTRATDD
		my $ddf = $data[0];
		# CONTRATDR
		my $dff = $data[1];
		my $jrplus = 0.0;
		my $jrmoins = 0.0;
		# MOPXJOURFR
		my $tarif = $data[4];
		# MOTARIFMSFR
		my $tarifm = $data[5];
		# MOFORFAITFR
		my $forfait = $data[10];
		my $boole = 0;
		my $booleta   = 0;
		my $booletr   = 0;
		my $boolemens = 0;
		my $booleforf = 0;
		my $boolecarb = 0;

		# Contrat Mensuel
		if ($tarifm != 0) {
			$boole = 1;
			$tarif = $tarifm;
			# Calculer le nb de jours ouvrables
			$nbjourmois = $temp;
			$nbnbjrwe   = &jour_we($nbjourmois, 01, $msfm, $anfm);
			$nbnbferie  = &jour_fer_ag_cnx($G_PAYS, $agence, 01, $msfm, $anfm, $nbjourmois-1, $DB);
			my $joursupmensuel = 0;

			if ($df > $fm) {$joursupmensuel = 0;}
			else {
				my $testjoursupmensuel = &num_jour($jrdf, $msdf, $andf);

				if ($testjoursupmensuel == 6) {$joursupmensuel = 1;}
				if ($testjoursupmensuel == 7) {$joursupmensuel = 2;}
			}

			$jourmois = $nbjourmois - $nbnbjrwe - $nbnbferie + $joursupmensuel;
			$boolemens = 1;
		}
		# Contrat Forfaitaire
		if ($forfait != 0) {
			$boole = 2;
			$tarif = $forfait;
			my $dureecont = Delta_Days($andd, $msdd, $jrdd, $andf, $msdf, $jrdf);
			my $nbferieforfait = &jour_fer_ag_cnx($G_PAYS, $agence, $jrdd, $msdd, $andd, $dureecont, $DB);
	  		my $joursupforfait = 0;
	  		my $testjoursupforfait;

			if ($df > $fm) {$joursupforfait = 0;}
			else {
				$testjoursupforfait = &num_jour($jrdf, $msdf, $andf);

				if ($testjoursupforfait == 6) {$joursupforfait = 1;}
				if ($testjoursupforfait == 7) {$joursupforfait = 2;}
			}

			$dureecont++;

			if ($dureecont == 1 && $testjoursupforfait == 7) {$joursupforfait = 1;}

			$nbjrweforfait = &jour_we($dureecont, $jrdd, $msdd, $andd);
	  		$dureecont = $dureecont - $nbferieforfait - $nbjrweforfait + $joursupforfait;
			$jourmois = $dureecont;
			$booleforf = 1;
		}
		# Ici en fonction de la date de d�part, si elle est dans le mois en cours
		# on comptabilise le transport aller sinon non
		if ($data[15] eq "P") {$ddf = $debutms;}
		elsif (!$data[15] || $data[15] eq "") {
			$ta = $data[7];
			$booleta = 1;
		}
		# Ici en fonction de la date de fin, si elle est dans le mois en cours
		# on comptabilise le transport retour et le carburant sinon non
		if ($df > $fm) {
			$dff = $finms;
			$dff2 = "$anfm:$msfm:$temp";
			$boolms = 1;
		} else {
		 	$carbu = $data[9];# CAMONTANTFR
		 	$dff2 = $dff;

			if ($carbu != 0) {$boolecarb = 1;}

		 	$tr = $data[8];# MOTRANSPORTRFR
			$booletr = 1;
		}

		$jrplus = $data[2];# CONTRATJOURSPLUS
		$jrmoins = $data[3];# CONTRATJOURSMOINS
		my ($dda, $ddm, $ddj);
		my ($dfa, $dfm, $dfj);

		if ($ddf =~ /:/) {($dda, $ddm, $ddj) = split(/:/,$ddf);}
		else {($dda,$ddm,$ddj) = split(/-/,$ddf);}
		if ($dff =~ /:/) {($dfa,$dfm,$dfj)=split(/:/,$dff);}
		else {($dfa,$dfm,$dfj)=split(/-/,$dff);}
		if ($dff2 =~ /:/) {($dfa2,$dfm2,$dfj2) = split(/:/,$dff2);}
		else {($dfa2,$dfm2,$dfj2) = split(/-/,$dff2);}

		my $duree = Delta_Days($dda, $ddm, $ddj, $dfa, $dfm, $dfj);
		my $duree2 = Delta_Days($dda,$ddm,$ddj,$dfa2,$dfm2,$dfj2);

		if ($duree < 0) {$nbrow--;next;}

		# $duree2 ici sert � calculer les contrats chevauchant la fin de mois
		# avec -4j ds le mois : d� � la nouvelle facturation qui ne prend pas en compte ces
		# contrats l�
		my $nbferie2 = &jour_fer_ag_cnx($G_PAYS, $G_AGENCE, $ddj, $ddm, $dda, $duree2, $DB);
		my $nbjrwe2  = 0;
  		my $joursup2 = 0;
  		my $testjoursup2;

		if (Delta_Days($anfm, $msfm, $temp, $andf, $msdf, $jrdf) > 0) {$joursup2 = 0;}
		else {
			$testjoursup2 = &num_jour($dfj2,$dfm2,$dfa2);

			if ($testjoursup2 == 6) {$joursup2 = 1;}
			if ($testjoursup2 == 7) {$joursup2 = 2;}
		}

		$duree2 += 1;

		if ($duree2 == 1 && $testjoursup2 == 7) {$joursup2 = 1;}

		$nbjrwe2 = &jour_we($duree2, $ddj, $ddm, $dda);
		$duree2 = $duree2 - $nbferie2 - $nbjrwe2 + $joursup2 + $jrplus - $jrmoins;

		if (Delta_Days($anfm, $msfm, $temp, $andf, $msdf, $jrdf) > 0 && $duree2 <= 4) {

			if ($posi) {
                if (($data[14] eq "1")) {
                    $nbcontlivr++;
                    $compta{$agence}{$fam}->[13] += 1;
                }
				elsif (($data[14] eq "2")) {
					$nbcontcours++;
					$compta{$agence}{$fam}->[2] += 1;
				} else {
					$nbcontarret++;
					$compta{$agence}{$fam}->[1] += 1;
				}

			 	$compta{$agence}{$fam}->[0] += 1;
			}

			next;
		}
		if ($booleta) {$catr += $ta;}
		if ($booletr) {$catr += $tr;}
		if ($boolemens) {$nbtarifms++;}
		if ($booleforf) {$nbforfait++;}
		if ($boolecarb) {$cacarb += $carbu;}

		# Nouveau calcul, dur�e de jours ouvrables pour tous
        $duree = &LOC::Date::getRentDuration(
            sprintf('%04d-%02d-%02d', $dda, $ddm, $ddj),
            sprintf('%04d-%02d-%02d', $dfa, $dfm, $dfj),
            $agence,
            $data[1]
        ) + $jrplus - $jrmoins;
		$compta{$agence}{$fam}->[4+$posi] += $duree;
		$durtot{$agence} += $duree if ($posi);

		# Le calcul de la dur�e s'effectue en fonction du nombre
		# de jours ouvrables dans le mois ainsi que de sa dur�e et
		# des j+ j-, jours f�ri�s et w-e
		# si dur�e sup au mois, joursouvrables, sinon dur�e et le reste
		# Surtout important c'est le calcul des contrats de -5j CONTRATDD et CONTRATDR
		# Et non pas le nb de contrats dont la dur�e ds le mois < 5
		# $duree (nb de jours du contrat ds le mois) <> $dureereel (nb de jours du contrat)
		my ($dareel, $dmreel, $djreel) = split(/-/, $data[0]);
		my ($fareel, $fmreel, $fjreel) = split(/-/, $data[1]);
		my $dureereel = Delta_Days($dareel, $dmreel, $djreel, $fareel, $fmreel, $fjreel);
		my $nbferiereel = &jour_fer_ag_cnx($G_PAYS, $agence, $djreel, $dmreel, $dareel, $dureereel, $DB);
		my $testjoursupreel = &num_jour($fjreel, $fmreel, $fareel);
		my $joursupreel = 0;

		if ($testjoursupreel == 6) {$joursupreel = 1};
		if ($testjoursupreel == 7) {$joursupreel = 2};
		if ($boolms != 0) {$joursupreel = 0;}

		$dureereel += 1;
		my $nbjrwereel = &jour_we($dureereel, $djreel, $dmreel, $dareel);
		$dureereel = $dureereel + $jrplus - $jrmoins - $nbferiereel - $nbjrwereel + $joursupreel;

		if ($dureereel < 5) {$nbdureems++;}
		else {$nbdureeplus++;}
		if ($boole == 1 || $boole == 2) {$tarif = $tarif / $jourmois;}

        my $beginDate = sprintf('%04d-%02d-%02d', $dda, $ddm, $ddj);
        my $endDate   = sprintf('%04d-%02d-%02d', $dfa, $dfm, $dfj);

		# Informations sur l'assurance
        my $ass = $data[6];# CONTRATTYPEASSURANCE
		my $insuranceFlags = $data[18];

		my $montantass = 0.0;

        $montantass = &LOC::Common::Rent::calculateInsurance($G_PAYS, $agence, $ass, $data[17], $tarif,
                                                             $beginDate, $endDate, $jrplus, $jrmoins);

		if ($insuranceFlags & LOC::Insurance::Type::FLAG_ISINVOICED) {
			$nbasscomp++;
		}

        # Gestion de recours
        my $appealTypeId = $data[19];
        my $appealRate   = $data[20];

        my $appealAmount = 0.0;
        if ($appealTypeId)
        {
            $appealAmount = &LOC::Common::Rent::calculateAppeal($G_PAYS, $agence, $appealTypeId, $appealRate, $tarif,
                                                                $beginDate, $endDate, $jrplus, $jrmoins);
        }

		$duree = sprintf("%.2f", $duree);
		my $montant = $tarif * $duree;
		my $mtloc = $montant + $montantass + $appealAmount;
		my $montotal = $montant + $ta + $tr + $carbu + $montantass + $appealAmount;
		$total += $montotal;
		$compta{$agence}{$fam}->[3+$posi] += $montotal;
		$caloc += $mtloc;
		
		# Pour les machines hors parc, le co�t imput�
		# est �gal au CA / 2
		if ($data[16] == -1) {
			if ($posi) {
				$compta{$agence}{$fam}->[12] += $mtloc / 2;
			}
			else {
				$compta{$agence}{$fam}->[11] += $mtloc / 2;
			}
		}
		
		if ($posi) {
			$compta{$agence}{$fam}->[10] += $mtloc;
			$caloc{$agence} += $mtloc;

            if (($data[14] eq "1")) {
                $nbcontlivr++;
                $compta{$agence}{$fam}->[13] += 1;
            }
			elsif (($data[14] eq "2")) {
				$nbcontcours++;
				$compta{$agence}{$fam}->[2] += 1;
			} else {
				$nbcontarret++;
				$compta{$agence}{$fam}->[1] += 1;
			}

		 	$compta{$agence}{$fam}->[0] += 1;
		} else {$compta{$agence}{$fam}->[5] += $mtloc;}
	}

	$sth->finish;
	my $cond = "FAMACOMPTAAUTO";
	my $requete = $DB->prepare(qq{
	select MOTARIFMSFR, MOFORFAITFR, SOLOAUTO, CONTRATDUREE, $cond, CONTRAT.AGAUTO
	from CONTRAT
	left join MONTANT on CONTRAT.MOAUTO=MONTANT.MOAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where CONTRAT.AGAUTO="$agence"
	and CONTRATOK in ("-1","-2")
	and CONTRAT.LIETCONTRATAUTO in ("2","3","4","7")
	and DATE(CONTRATDD) <= "$finms"
	and DATE(CONTRATDR) >= "$debutms"
	and ETATFACTURE="F"
	order by $cond
	});
	$requete->execute();

	while (my @data = $requete->fetchrow_array()) {
		my $modele = $data[4];

		if ($data[0] != 0) {$nbtarifms++;}
		if ($data[1] != 0) {$nbforfait++;}
		if ($data[2] != 0) {$modele = 0;}
		if ($data[3] < 5 && $data[0] == 0) {$nbdureems++;}
		else {$nbdureeplus++;}
		if ($posi) {
			$nbcontarret++;
			$compta{$data[5]}{$modele}->[1]++;
			$compta{$data[5]}{$modele}->[0]++;
		}

		$nbrow++;
	}

	$requete->finish();
	my $requete = $DB->prepare(qq|
	select LIGNEREFARTICLE,
	   sum(ROUND(ROUND(LIGNEPRIX, |.$tabSageFormats->{'price'}.qq|)*ROUND(LIGNEQTITE, |.$tabSageFormats->{'quantity'}.qq|), |.$tabSageFormats->{'amount'}.qq|)),
	   count(LIGNEREFARTICLE), sum(LIGNEQTITE), $cond, ENTETEFACTURE.AGAUTO
	from ENTETELIGNE
	left join ENTETEFACTURE on ENTETELIGNE.ENTETEAUTO=ENTETEFACTURE.ENTETEAUTO
	left join LIGNEFACTURE on LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	left join CONTRAT on ENTETEFACTURE.CONTRATAUTO=CONTRAT.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
    where ENTETEFACTURE.AGAUTO="$agence"
    AND ((DATE(`DATE`)>="$debutms" and DATE(`DATE`)<="$finms")
    OR (ENTETEIDFACTU IS NULL AND DATE(CONTRATDD) <= "$finms" and DATE(CONTRATDR) >= "$debutms"))
	and SOLOAUTO=0
	group by ENTETEFACTURE.AGAUTO, $cond, LIGNEREFARTICLE
	|);
	$requete->execute();

	while (my @data = $requete->fetchrow_array()) {
		$total += $data[1];
		$compta{$data[5]}{$data[4]}->[3+$posi] += $data[1];

		if ($data[0] =~ /^TRANS/ || $data[0] eq "SERT") {$catr += $data[1];}
		if ($data[0] eq "CARB") {$cacarb += $data[1];}
		if ($data[0] eq "ASS") {
			$nbasscomp += $data[2];
			$caloc += $data[1];

			if ($posi) {
				$compta{$data[5]}{$data[4]}->[10] += $data[1];
				$caloc{$data[5]} += $data[1];
			} else {$compta{$data[5]}{$data[4]}->[5] += $data[1];}
		}
        if ($data[0] eq "GDR") {
            $caloc += $data[1];

            if ($posi)
            {
                $compta{$data[5]}{$data[4]}->[10] += $data[1];
                $caloc{$data[5]} += $data[1];
            }
            else
            {
                $compta{$data[5]}{$data[4]}->[5] += $data[1];
            }
        }
		if ($data[0] eq "LOC" || $data[0] eq "REM" || $data[0] eq "J+" || $data[0] eq "J-") {
            if ($data[0] ne "REM") {
    			$durtot{$agence} += $data[3] if $posi;
    			$compta{$data[5]}{$data[4]}->[4+$posi] += $data[3];
    		}
			$caloc += $data[1];

			if ($posi) {
				$compta{$data[5]}{$data[4]}->[10] += $data[1];
				$caloc{$data[5]} += $data[1];
			} else {$compta{$data[5]}{$data[4]}->[5] += $data[1];}
		}
	}

	$requete->finish();
	my $requete = $DB->prepare(qq|
	select LIGNEREFARTICLE,
	   sum(ROUND(ROUND(LIGNEPRIX, |.$tabSageFormats->{'price'}.qq|)*ROUND(LIGNEQTITE, |.$tabSageFormats->{'quantity'}.qq|), |.$tabSageFormats->{'amount'}.qq|)),
	   count(LIGNEREFARTICLE), sum(LIGNEQTITE), $cond, ENTETEFACTURE.AGAUTO
	from ENTETELIGNE
	left join ENTETEFACTURE on ENTETELIGNE.ENTETEAUTO=ENTETEFACTURE.ENTETEAUTO
	left join LIGNEFACTURE on LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	left join CONTRAT on ENTETEFACTURE.CONTRATAUTO=CONTRAT.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
    where ENTETEFACTURE.AGAUTO="$agence"
    AND ((DATE(`DATE`)>="$debutms" and DATE(`DATE`)<="$finms")
    OR (ENTETEIDFACTU IS NULL AND DATE(CONTRATDD) <= "$finms" and DATE(CONTRATDR) >= "$debutms"))
	and SOLOAUTO!=0
	group by ENTETEFACTURE.AGAUTO, $cond, LIGNEREFARTICLE
	|);

	$requete->execute();

	while (my @data = $requete->fetchrow_array()) {
		$total += $data[1];
		$compta{$data[5]}{0}->[3+$posi] += $data[1];
		$compta{$data[5]}{""}->[3+$posi] -= $data[1];

		if ($data[0] =~ /^TRANS/ || $data[0] eq "SERT") {$catr += $data[1];}
		if ($data[0] eq "CARB") {$cacarb += $data[1];}
		if ($data[0] eq "ASS") {
			$nbasscomp += $data[2];
			$caloc += $data[1];

			if ($posi) {
				$compta{$data[5]}{0}->[10] += $data[1];
				$compta{$data[5]}{""}->[10] -= $data[1];
				$caloc{$data[5]} += $data[1];
			} else {
				$compta{$data[5]}{0}->[5] += $data[1];
				$compta{$data[5]}{""}->[5] -= $data[1];
			}
		}
        if ($data[0] eq "GDR")
        {
            $caloc += $data[1];
            if ($posi)
            {
                $compta{$data[5]}{0}->[10] += $data[1];
                $compta{$data[5]}{""}->[10] -= $data[1];
                $caloc{$data[5]} += $data[1];
            }
            else
            {
                $compta{$data[5]}{0}->[5] += $data[1];
                $compta{$data[5]}{""}->[5] -= $data[1];
            }
        }
		if ($data[0] eq "LOC" || $data[0] eq "REM" || $data[0] eq "J+" || $data[0] eq "J-") {
            if ($data[0] ne "REM") {
    			$durtot{$agence} += $data[3] if $posi;
    			$compta{$data[5]}{0}->[4+$posi] += $data[3];
    			$compta{$data[5]}{""}->[4+$posi] -= $data[3];
    		}
			$caloc += $data[1];

			if ($posi) {
				$compta{$data[5]}{0}->[10] += $data[1];
				$compta{$data[5]}{""}->[10] -= $data[1];
				$caloc{$data[5]} += $data[1];
			} else {
				$compta{$data[5]}{0}->[5] += $data[1];
				$compta{$data[5]}{""}->[5] -= $data[1];
			}
		}
	}

	$requete->finish();
	my $ca = sprintf("%.2f", $total);

	return ($nbrow, $ca, $catr, $cacarb, $nbdureems, $nbdureeplus, $nbcontcours, $nbcontarret, $nbasscomp, $nbtarifms, $nbforfait, $jourmsouv, $caloc, $nbcontlivr);
}

# R�cup�re l'agence de d�part
# du dernier transfert �tabli
# de la machine s'il y a lieu
sub transfert {
#	print qq|<br>transfert|;
	my @param = @_;
	my $sth = $DB->prepare(qq{
	select TRANSFERTAGA
	from AUTH.`TRANSFERT`
	where MAAUTO="$param[0]"
	and TRANSFERTDATEAR<="$param[1]"
	order by TRANSFERTAUTO desc limit 1}
	);
	$sth->execute();
	my ($ag) = $sth->fetchrow_array();
	$sth->finish;

	if ($ag eq "") {
		$req = qq{select TRANSFERTAGD FROM AUTH.`TRANSFERT` WHERE MAAUTO="$param[0]" and TRANSFERTDATEAR>"$param[1]" ORDER BY TRANSFERTAUTO LIMIT 1};
		$sth = $DB->prepare($req);
		$sth->execute();
		($ag) = $sth->fetchrow_array();
	}

	return $ag;
}

# Liste les machines et les mod�les avec leur correspondance
# avec les familles machines, familles comptables
# et leur co�t avant une date donn�e par rapport � leur date d'achat
sub list_mach {
	my @param  = @_;
	my $req = qq{
	select MAAUTO, AGAUTO, MODELEMACHINE.MOMAAUTO, FAMACOMPTAAUTO, MOMACOUTFR, MAHORSPARC, ETCODE
	FROM AUTH.`MACHINE`
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=MACHINE.MOMAAUTO
	left join FAMILLEMACHINE ON MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO
	where MADATEACHAT<="$param[0]"
	};
	$sth=$DB->prepare($req);
	$sth->execute();
	while (my @data= $sth->fetchrow_array()) {
		my $maauto         = $data[0];
		my $agauto         = $data[1];
		my $momaauto       = $data[2];
		my $famacomptaauto = $data[3];
		my $momacoutfr     = $data[4] / $tauxE;
		my $horsparc       = $data[5];
		my $agence = &transfert($maauto, $param[0]);

		if ($agence ne "") {$agauto = $agence;}

		$mach_mod{$agauto}{$momaauto}++;
		$mach_cpt{$agauto}{$famacomptaauto}++;
		if ($horsparc != -1) {
			$cout_mod{$agauto}{$momaauto}       += $momacoutfr;
			$cout_cpt{$agauto}{$famacomptaauto} += $momacoutfr;
			$coutatotal{$agauto}                += $momacoutfr;
		}
	}

	$sth->finish;
}

# R�cup�re les mod�les machines r�partis par familles machines, comptables
# ainsi que les stocks r�els des machines par mod�le par agence ou de l'historique � un temps T
# ainsi que leur cout
sub list_mach2 {
	my @param = @_;
	my $req   = "";

	if (!$date) {
        # R�cup�ration des machines dans les �tats qui vont bien (< 20 et != 8 et 10)
        # Les machines sont comptabilis�es sur leur agence de visibilit� si elles sont en transfert
        # ou en livraison et qu'elles sont attribu�es � un contrat.
        # Sinon, elles sont comptabilis�es sur leur agence d'appartenance
		$req = qq{
		select M.MOMAAUTO, count(*), 0,
		IF(M.MASUPPAGAUTO IS NOT NULL AND M.ETCODE IN ("MAC09", "MAC12") AND C.ETCODE = "CON01",
            M.MASUPPAGAUTO,
            M.AGAUTO) AS AG_MODIF,
        M.MAHORSPARC
		from AUTH.`MACHINE` M
        LEFT JOIN CONTRAT C ON (M.CONTRATAUTO = C.CONTRATAUTO AND M.MAAUTO = C.MAAUTO AND C.CONTRATMACHATTR = -1)
		where M.LIETCODE not IN ("8","10") AND M.LIETCODE is not NULL AND M.LIETCODE<20
		GROUP BY AG_MODIF, M.MOMAAUTO, M.MAHORSPARC
		};
	} else {
		my @time = split(/:/, $param[0]);
		my ($anhist, $mshist, $jrhist) = Add_Delta_Days($time[0], $time[1], $time[2], 1);
		$req = qq{
		select HISTOSTOCKMODELE.MOMAAUTO, sum(HISTSTMOTOTAL), sum(HISTSTMOHORSPARC), AGAUTO, 0
		from AUTH.`HISTOSTOCKMODELE`
		where HISTDATE between "$anhist:$mshist:$jrhist 00:00:00" and "$anhist:$mshist:$jrhist 23:59:59"
		group by AGAUTO, HISTOSTOCKMODELE.MOMAAUTO
		};
	}

	my $sth = $DB->prepare("$req");
	$sth->execute();

	while (my @data = $sth->fetchrow_array()) {
		my $momaauto = $data[0];
		my $somme    = $data[1];
		my $sommehp  = $data[2];
		my $agauto   = $data[3];
        my $horsparc = $data[4];
        my $livr     = $data[5];
		my @tp       = &tp_mach($momaauto);

		if (@tp) {
            $mach_mod{$agauto}{$momaauto} += $somme;
            $mach_cpt{$agauto}{$tp[0]}    += $somme;

			if ($horsparc != -1) {
				$cout_mod{$agauto}{$momaauto} += ($tp[1] / $tauxE ) * ($somme - $sommehp);
				$cout_cpt{$agauto}{$tp[0]}    += ($tp[1] / $tauxE ) * ($somme - $sommehp);
				$coutatotal{$agauto}          += ($tp[1] / $tauxE ) * ($somme - $sommehp);
			}
		}
	}

	$sth->finish;
}

# Renvoie le co�t d'un mod�le et sa famille : comptable, machine
sub tp_mach {
	my @param = @_;
	my $req = qq{
	select FAMACOMPTAAUTO, MOMACOUTFR
	FROM AUTH.`MODELEMACHINE`
	LEFT join FAMILLEMACHINE ON MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO
	WHERE MOMAAUTO="$param[0]"
	};
	my $sth = $DB->prepare("$req");
	$sth->execute();
	my @data = $sth->fetchrow_array();
	$sth->finish;

	return @data;
}

# Ante informatisation des chiffres
sub test_date {
	my @param = @_;
	my @dt = split(/\:/, $param[0]);
	my $diff1 = Delta_Days(1900,1,1,$tdt[0],$tdt[1],$tdt[2]);
	my $diff2 = Delta_Days(1900,1,1,$dt[0],$dt[1],$dt[2]);
	my ($jour, $mois, $an) = (localtime(time))[3,4,5];
	$an += 1900;
	$mois += 1;
	my $diff3 = Delta_Days(1900,1,1,$an,$mois,$jour);
	my $dt = $diff2 - $diff3;

	if ($dt > 0) {$day = "$an:$mois:$jour";}

	my $diff = $diff2 - $diff1;

	return $diff;
}

# nb machine
sub nb_mach {
	my @param = @_;
	my $req = qq{
	select MOMAAUTO, FAMACOMPTAAUTO
	FROM AUTH.`MODELEMACHINE`
	LEFT join FAMILLEMACHINE ON MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO
	};
	my $sth=$DB->prepare($req);
	$sth->execute();

	while (my @data = $sth->fetchrow_array()) {
		my $momaauto       = $data[0];
		my $famacomptaauto = $data[1];

		for (my $i=0;$i<@agence;$i++) {
			$mach_mod{$agence[$i]}{$momaauto}       = 0;
			$mach_cpt{$agence[$i]}{$famacomptaauto} = 0;

			$cout_mod{$agence[$i]}{$momaauto}       = 0;
			$cout_cpt{$agence[$i]}{$famacomptaauto} = 0;
		}
	}

	$sth->finish;

	# famille compta
	my $t = &test_date($mimois);

	if ($t < 0) {&list_mach($param[0]);}
	else {&list_mach2($day);}

	# R�cup�re les familles comptables
	# Il semble que MOMAGRP=1 => Accessoires ou Grues
	# MOMAGRP = 0 => Machines 'normales'
	my $famille = qq{
	select FAMILLECOMPTA.FAMACOMPTA, FAMILLECOMPTA.FAMACOMPTAAUTO, MOMAGRP
	FROM FAMILLECOMPTA
	LEFT JOIN FAMILLEMACHINE ON FAMILLEMACHINE.FAMACOMPTAAUTO=FAMILLECOMPTA.FAMACOMPTAAUTO
	LEFT JOIN AUTH.`MODELEMACHINE` ON MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO
	WHERE MOMAGRP is not NULL
	GROUP BY FAMILLECOMPTA.FAMACOMPTAAUTO, MOMAGRP
	ORDER BY FAMILLECOMPTA.FAMACOMPTAAUTO
	};
	$sth=$DB->prepare("$famille");
	$sth->execute();

	while (my @data=$sth->fetchrow_array()) {
		push(@ordcpt, $data[1]);
		push(@ordtot, $data[2]);

       		for(my $j=0;$j<@agence;$j++) {
		       	$famille{$agence[$j]}{$data[1]} = $data[0];

		       	for (my $i=0;$i<14;$i++) {
	       			$compta{$agence[$j]}{$data[1]}->[$i] = 0;
	       		}

			$compta{$agence[$j]}{$data[1]}->[8]  = $mach_cpt{$agence[$j]}{$data[1]};
			$compta{$agence[$j]}{$data[1]}->[9]  = $cout_cpt{$agence[$j]}{$data[1]};
			$compta{$agence[$j]}{$data[1]}->[10] = 0;
			$compta{$agence[$j]}{$data[1]}->[5]  = 0;
			$totmc{$agence[$j]} += $mach_cpt{$agence[$j]}{$data[1]};
      		}
       	}

	$sth->finish;
	my $sl  = 0;
	my $sl1 = 1;
	push(@ordcpt, $sl);
	push(@ordtot, $sl1);

	for (my $j=0;$j<@agence;$j++) {
		# sous location
		$famille{$agence[$j]}{0} = $mess[3300];

		for (my $i=0;$i<14;$i++) {
   			$compta{$agence[$j]}{0}->[$i] = 0;
   		}
	}
}

sub statistique {
	my @param = @_;
	my @restt = &statjr($datedebutmois, $datefinmois, 3, $param[0]);
	my @resct = &statjr($datedebutmois, $datedujour, 0, $param[0]);
	$rest{$param[0]} = \@restt;
	$resc{$param[0]} = \@resct;
}

sub entete {
	my $db  = $datedebutmoisaff;
	my $fn  = $datefinmoisaff;
	my $fna = $datedujouraff;

	if ($moisencours_ou_histo) {
		my @data = split(/\:/, $date);
		my ($dbf, $dbff, $fnf, $fnff) = &debfinmois($data[1], $data[0]);
		$db  = $dbff;
		$fn  = $fnff;
		$fna = $fn;
	}

	print qq{
	<table width="100%" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td colspan=5 align="left"><font class="PT"><b>$mess[1350] : $db<br>$mess[1351] : $fn</font></b></td>
			<td colspan=10 align="left">
	}.&jourdate.
	qq{
			</td>
		</tr>
		<tr>
			<td colspan=15 align="center"><b><font class="PT">
	};

	for (my $i=0;$i<@agence;$i++) {
		my $a=qq|<a href="|.$cgi->url().qq|?date=$date&groupe=$gpag{$agence[$i]}">|;
		my $fa=qq|</a>|;

		if ($groupe eq $gpag{$agence[$i]}) {$a=qq|<a href="#$agence[$i]">|;}
		if ((!($i % $tranche))&&($i)) {print qq{&nbsp;&nbsp;&nbsp;};}

		print qq{$a$agence[$i]$fa&nbsp;};
	}

	print qq{
		</b></font></td>
		</tr>
	</table>
	};
}

# $param[0] = ?
# $param[1] = Agence
sub tire_tab {
	my @param=@_;
	my $joursouvr = 0;
	if (!$moisencours_ou_histo) {
		$joursouvr = &jours_ouvrables($datedebutmois, $datedujour, $G_PAYS, 
			$param[1], $DB);
	}
	else {
		$joursouvr = &jours_ouvrables(substr($mimois, 0,
			length($mimois)-2)."01", $mimois, $G_PAYS, $param[1], $DB);
	}
	my $fna=$datedujouraff;

	if ($moisencours_ou_histo) {
		my @data=split(/\:/,$date);
		my ($dbf,$dbff,$fnf,$fnff)=&debfinmois($data[1],$data[0]);
		$fna=$fnff;
	}

	print qq{
		<tr bgcolor="#C6CDC5">
			<th rowspan=2><font class="PT">$param[0]</font></th>
            <th rowspan=2><font class="PT">$mess[5016]</font></th>
			<th colspan=4><font class="PT">$mess[5000]</font></th>
			<th rowspan=2><font class="PT">$mess[5001]</font></th>
			<th colspan=4><font class="PT">$mess[5009]$fna</font></th>
			<th align="right"><font class="PT">$joursouvr&nbsp;</font></th>
			<th align="left"><font class="PT">&nbsp;$mess[5026]</font></th>
			<th colspan=8><font class="PT">$mess[5010]</font></th>
		</tr>
		<tr bgcolor="#C6CDC5">
			<th><font class="PT">$mess[5002]</font></th>
			<th><font class="PT">$mess[5003]</font></th>
			<th><font class="PT">$mess[5004]</font></th>
            <th><font class="PT">$mess[5027]</font></th>
			<th align="right"><font class="PT">$mess[5011]</font></th>
			<th align="right"><font class="PT">$mess[5011] $mess[5025]</font></th>
			<th align="right"><font class="PT">$mess[5017]</font></th>
			<th align="right"><font class="PT">$mess[5018]</font></th>
			<th align="right"><font class="PT">$mess[5020]</font></th>
			<th align="right"><font class="PT">Ratio cash</font></th>
			<th align="right"><font class="PT">$mess[5011]</font></th>
			<th align="right"><font class="PT">$mess[5011] $mess[5025]</font></th>
			<th align="right"><font class="PT">$mess[5017]</font></th>
			<th align="right"><font class="PT">$mess[5019]</font></th>
			<th align="right"><font class="PT">$mess[5019] $mess[5025]</font></th>
			<th align="right"><font class="PT">$mess[5020]</font></th>
			<th align="right"><font class="PT">$mess[5021]</font></th>
			<th align="right"><font class="PT">Ratio prix</font></th>
		</tr>
	};
}

sub aff_glob {
	my ($ag)=@_;
	my $fna=$datedujouraff;

	if ($moisencours_ou_histo) {
		my @data=split(/\:/,$date);
		my ($dbf,$dbff,$fnf,$fnff)=&debfinmois($data[1],$data[0]);
		$fna=$fnff;
	}

	my $tot=$totmc{$ag};
	print qq{
		<tr bgcolor="#C6CDC5">
			<th rowspan=2><font class="GD"><a name="$param[0]">$ag</a></font></th>
			<th rowspan=2><font class="PT">$mess[5005]</font></th>
			<th rowspan=2><font class="PT">$mess[5006]</font></th>
			<th rowspan=2><font class="PT">$mess[5007]</font></th>
			<th rowspan=2><font class="PT">$mess[5008]</font></th>


			<th colspan=7><font class="PT">$mess[5009]$fna</font></th>
			<th colspan=8><font class="PT">$mess[5010]</font></th>
		</tr>
		<tr bgcolor="#C6CDC5">
			<th align="right"><font class="PT">$mess[5011]</font></th>
			<th align="right" colspan=2><font class="PT">$mess[5011] $mess[5025]</font></th>
			<th align="right" colspan=2><font class="PT">$mess[5012]</font></th>
			<th align="right" colspan=2><font class="PT">$mess[5013]</font></th>
			<th align="right"><font class="PT">$mess[5011]</font></th>
			<th align="right" colspan=2><font class="PT">$mess[5011] $mess[5025]</font></th>
			<th align="right"><font class="PT">$mess[5012]</font></th>
			<th align="right"><font class="PT">$mess[5013]</font></th>
			<th align="right"><font class="PT">$mess[2004]</font></th>
			<th align="right" colspan=2><font class="PT">$mess[5011] + $mess[2004]</font></th>

		</tr>
		<tr>
			<th align="right">&nbsp;</th>
            <td align="center"><font class="PT">$rest{$ag}->[8]</font></td>
			<td align="center"><font class="PT">$rest{$ag}->[4]</font></td>
			<td align="center"><font class="PT">$rest{$ag}->[9]</font></td>
			<td align="center"><font class="PT">$rest{$ag}->[10]</font></td>
	};
	my $cat = sprintf("%.0f", $rest{$ag}->[1]); # $rest[1];

	for(my $i=1;$i<4;$i++) {
		$rest[$i]=sprintf("%.0f", $rest{$ag}->[$i]);
		$resc[$i]=sprintf("%.0f", $resc{$ag}->[$i]);

		$rest[$i]=&fnombre2($rest[$i],3,0);
		$resc[$i]=&fnombre2($resc[$i],3,0);
	}

	$resc[12]=sprintf("%.0f", $resc{$ag}->[12]);
	$resc[12]=&fnombre2($resc[12],3,0);
	my $affcaloc = sprintf("%.0f", $caloc{$ag});
	$affcaloc=&fnombre2($affcaloc,3,0);
	print qq{
			<td align="right" nowrap><font class="PT">$resc[1]</font></td>
			<td align="right" nowrap colspan=2><font class="PT">$resc[12]</font></td>
			<td align="right" nowrap colspan=2><font class="PT">$resc[2]</font></td>
			<td align="right" nowrap colspan=2><font class="PT">$resc[3]</font></td>
			<td align="right" nowrap><font class="PT">$rest[1]</font></td>
			<td align="right" nowrap colspan=2><font class="PT">$affcaloc</font></td>
			<td align="right" nowrap><font class="PT">$rest[2]</font></td>
			<td align="right" nowrap><font class="PT">$rest[3]</font></td>
	};
	my $cs=sprintf("%.0f", $casse{$ag});

	if ($cs eq "") {$cs=0;}

	$cs=&fnombre2($cs,3,0);
	print qq{
		<td align="right" nowrap><font class="PT">$cs</font></td>
		<td align="right" nowrap colspan=2><font class="PT">}.&fnombre2(sprintf("%.0f", $rest{$ag}->[1]+$casse{$ag}), 3, 0).qq{</font></td>
	};

	print qq|

		</tr>
	|;


}

# $param[0] = Agence
sub corps {
	my @param = @_;
	my $joursouvr = 0;
	if (!$moisencours_ou_histo) {
		$joursouvr = &jours_ouvrables($datedebutmois, $datedujour, $G_PAYS,
			$param[0], $DB);
	}
	else {
		$joursouvr = &jours_ouvrables(substr($mimois, 0,
			length($mimois)-2)."01", $mimois, $G_PAYS, $param[0], $DB);
	}
	print qq{
	<hr>
	<table width="100%" border="0" cellspacing="2" cellpadding="0">
	};
	&aff_glob($param[0]);
	&tire_tab(qq{$mess[5022]}, $param[0]);
	my @totaux = ();
	my @sstot  = ();
	my @sstot_divers = ();

	for (my $i=0;$i<14;$i++) {
	   	$totaux[$i] = 0;
	   	$sstot[$i]  = 0;
	   	$sstot_divers[$i] = 0;
	}

	# corps
	my $k = 0;

	for(my $i=0;$i<@ordcpt;$i++) {
		my $boole_aff = 0;
		my $j         = $ordcpt[$i];
		my $c         = $k % 2;
		my $color     = "''";

		if ($j eq "") {next};
		# FA 8964 : Test sur le nb de parc et sur le CA total fin de mois
		if (($compta{$param[0]}{$j}->[8]==0)&&($compta{$param[0]}{$j}->[6]==0)) {next;}
		if ($c == 1) {$color=qq{'#D6D6D6'};}

		print qq|
		<tr bgColor=$color onmouseover="bgColor='#FFCC33'" onmouseout="bgColor=$color">
			<td align="left"><font class="PT"><b>$famille{$param[0]}{$j}</b></font></td>
            <td align="center" nowrap><font class="PT">$compta{$param[0]}{$j}->[8]</font></td>
			<td align="center" nowrap><font class="PT">$compta{$param[0]}{$j}->[0]</font></td>
			<td align="center" nowrap><font class="PT">$compta{$param[0]}{$j}->[1]</font></td>
			<td align="center" nowrap><font class="PT">$compta{$param[0]}{$j}->[2]</font></td>
            <td align="center" nowrap><font class="PT">$compta{$param[0]}{$j}->[13]</font></td>
		|;

		$totaux[8]  += $compta{$param[0]}{$j}->[8];
		$totaux[0]  += $compta{$param[0]}{$j}->[0];
		$totaux[1]  += $compta{$param[0]}{$j}->[1];
		$totaux[2]  += $compta{$param[0]}{$j}->[2];
		$totaux[3]  += $compta{$param[0]}{$j}->[3];
		$totaux[4]  += $compta{$param[0]}{$j}->[4];
		$totaux[6]  += $compta{$param[0]}{$j}->[6];
		$totaux[7]  += $compta{$param[0]}{$j}->[7];
		$totaux[9]  += $compta{$param[0]}{$j}->[9];
		$totaux[10] += $compta{$param[0]}{$j}->[10];
		$totaux[11] += $compta{$param[0]}{$j}->[11];
		$totaux[12] += $compta{$param[0]}{$j}->[12];
		$totaux[5]  += $compta{$param[0]}{$j}->[5];
        $totaux[13] += $compta{$param[0]}{$j}->[13];

		if (!$ordtot[$i]) {
			$sstot[9]  += $compta{$param[0]}{$j}->[9];
			$sstot[8]  += $compta{$param[0]}{$j}->[8];
			$sstot[0]  += $compta{$param[0]}{$j}->[0];
			$sstot[1]  += $compta{$param[0]}{$j}->[1];
			$sstot[2]  += $compta{$param[0]}{$j}->[2];
			$sstot[3]  += $compta{$param[0]}{$j}->[3];
			$sstot[4]  += $compta{$param[0]}{$j}->[4];
			$sstot[6]  += $compta{$param[0]}{$j}->[6];
			$sstot[7]  += $compta{$param[0]}{$j}->[7];
			$sstot[10] += $compta{$param[0]}{$j}->[10];
			$sstot[11] += $compta{$param[0]}{$j}->[11];
			$sstot[12] += $compta{$param[0]}{$j}->[12];
			$sstot[5]  += $compta{$param[0]}{$j}->[5];
            $sstot[13] += $compta{$param[0]}{$j}->[13];
		}

		my $ut=0;

		if ($compta{$param[0]}{$j}->[8]) {
			$ut=(100*($compta{$param[0]}{$j}->[2]+$compta{$param[0]}{$j}->[13]))/$compta{$param[0]}{$j}->[8];
		}

		$ut=sprintf("%.1f",$ut);
		print qq|
			<td align="right" nowrap><font class="PT">$ut%</font></td>
		|;
		my $aff=sprintf("%.0f", $compta{$param[0]}{$j}->[3]);
		my $aff2=sprintf("%.0f", $compta{$param[0]}{$j}->[4]);
		my $aff3=sprintf("%.0f", $compta{$param[0]}{$j}->[5]);
		$aff=&fnombre2($aff,3,0);
		$aff3=&fnombre2($aff3,3,0);
		print qq|
			<td align="right" nowrap><font class="PT">$aff</font></td>
			<td align="right" nowrap><font class="PT">$aff3</font></td>
			<td align="center" nowrap><font class="PT">$aff2</font></td>
		|;
		$ut=0;

		if (($resc{$param[0]}->[11])&&($compta{$param[0]}{$j}->[8])) {
			$ut=(100*($compta{$param[0]}{$j}->[4]))/($resc{$param[0]}->[11]*$compta{$param[0]}{$j}->[8]);
		}

		$ut=sprintf("%.1f",$ut);
		print qq|
			<td align="right" nowrap><font class="PT">$ut%</font></td>
		|;

		# Co�t
		# Calcul du co�t mensuel (repris plus bas)
		my $cout=0;
		my $affcouto=0;

		if ($resc{$param[0]}->[11]) {
			$cout=($ctaux/100.0)*$compta{$param[0]}{$j}->[9];
		}
		# Calcul du co�t sur la p�riode
		my $cout_periode = $cout / $nbjrmoy * $joursouvr + $compta{$param[0]}{$j}->[11];
		if ($resc{$param[0]}->[11]) {
			$cout += $compta{$param[0]}{$j}->[12];
			$cout=sprintf("%.0f", $cout);
			$affcouto=&fnombre2($cout,3,0);
		}
		
		print qq|
			<td align="right" nowrap><font class="PT" color="red"><b>|
			.&fnombre2(sprintf("%.0f", $cout_periode),3,0).qq|</b></font></td>
		|;

		# Ratio 2
		my $ratio2 = ($cout_periode == 0) ? 
			0 : $compta{$param[0]}{$j}->[5] / $cout_periode;
		$ratio2 = sprintf("%.2f",$ratio2);
		print qq|
			<td align="right" nowrap><font class="PT" color="red"><b>
			$ratio2</b></font></td>
		|;

		my $aff=sprintf("%.0f", $compta{$param[0]}{$j}->[6]);
		my $aff2=sprintf("%.1f",$compta{$param[0]}{$j}->[7]);
		my $aff3=sprintf("%.0f", $compta{$param[0]}{$j}->[10]);
		$aff=&fnombre2($aff,3,0);
		$aff3=&fnombre2($aff3,3,0);
		print qq|
			<td align="right" nowrap><font class="PT">$aff</font></td>
			<td align="right" nowrap><font class="PT">$aff3</font></td>
			<td align="center" nowrap><font class="PT">$aff2</font></td>
		|;
		$ut=0;
		$ut1=0;

		if ($compta{$param[0]}{$j}->[7] != 0) {
			$ut=$compta{$param[0]}{$j}->[6]/$compta{$param[0]}{$j}->[7];
			$ut1=$compta{$param[0]}{$j}->[10]/$compta{$param[0]}{$j}->[7];
		}

		$ut=sprintf("%.1f",$ut);
		$ut1=sprintf("%.1f",$ut1);
		print qq|
			<td align="center" nowrap><font class="PT">$ut</font></td>
			<td align="center" nowrap><font class="PT">$ut1</font></td>
		|;
		# co�t
		print qq|
			<td align="right" nowrap><font class="PT">$affcouto</font></td>
		|;
		my $pc=0;
		my $affcouto=0;
		my $cjm=0;
		my $affcjm=0;

		if ($compta{$param[0]}{$j}->[8]!=0) {
			$cjm=$cout/($nbjrmoy*$coef*$compta{$param[0]}{$j}->[8]);
			$cjm=sprintf("%.1f",$cjm);
			$affcjm=$cjm;
		}

		print qq|
			<td align="right" nowrap><font class="PT">$affcjm</font></td>
		|;

		if ($cjm>0) {
			$pc=($ut1)/$cjm;
		}

		$pc=sprintf("%.2f",$pc);
		print qq|
			<td align="right" nowrap><font class="PT">$pc</font></td>
		</tr>
		|;
		$k++;
	}

	# pied
	# sous total
	print qq|
		<tr bgcolor="orange">
			<td align="left" nowrap><font class="PT"><b>SOUS TOT MACH</b></font></td>
            <td align="center" nowrap><font class="PT"><b>$sstot[8]</font></b></td>
			<td align="center" nowrap><font class="PT"><b>$sstot[0]</font></b></td>
			<td align="center" nowrap><font class="PT"><b>$sstot[1]</font></b></td>
			<td align="center" nowrap><font class="PT"><b>$sstot[2]</font></b></td>
            <td align="center" nowrap><font class="PT"><b>$sstot[13]</font></b></td>
	|;
	my $ut=0;

	if ($sstot[8]) {
		$ut=(100*($sstot[2]+$sstot[13]))/$sstot[8];
	}

	$ut=sprintf("%.1f",$ut);
	print qq|<td align="right" nowrap><font class="PT"><b>$ut%</font></b></td>\n|;
	my $aff=sprintf("%.0f", $sstot[3]);
	my $aff2=sprintf("%.1f",$sstot[4]);
	my $aff3=sprintf("%.0f",$sstot[5]);
	$aff=&fnombre2($aff,3,0);
	$aff3=&fnombre2($aff3,3,0);
	print qq|
			<td align="right" nowrap><font class="PT"><b>$aff</font></b></td>
			<td align="right" nowrap><font class="PT"><b>$aff3</font></b></td>
			<td align="center" nowrap><font class="PT"><b>$aff2</font></b></td>
	|;
	$ut=0;

	if (($resc{$param[0]}->[11])&&($sstot[8])) {
		$ut=(100*$sstot[4])/($resc{$param[0]}->[11]*$sstot[8]);
	}

	$ut=sprintf("%.2f",$ut);
	print qq|
			<td align="right" nowrap><font class="PT"><b>$ut%</font></b></td>
	|;
	
	# Co�t p�riode
	my $couto=0;
	my $affcouto=0;

	if ($resc{$param[0]}->[11]) {
		$couto=($ctaux/100.0)*$sstot[9];
	}
	my $sstot_cout_periode = $couto / $nbjrmoy * $joursouvr + $sstot[11];
	if ($resc{$param[0]}->[11]) {
		$couto += $sstot[12];
		$couto=sprintf("%.0f", $couto);
		$affcouto=&fnombre2($couto,3,0);
	}
	print qq|<td align="right" nowrap><font class="PT" color="red"><b>|
		.&fnombre2(sprintf("%.0f", $sstot_cout_periode),3,0)
		.qq|</b></font></td>\n|;
	
	# Ratio 2
	my $sstot_ratio2 = ($sstot_cout_periode == 0) ? 0 : $sstot[5] / $sstot_cout_periode;
	$ratio2 = sprintf("%.2f",$ratio2);
	print qq|<td align="right" nowrap><font class="PT" color="red"><b>|
		.sprintf("%.2f",$sstot_ratio2).qq|</b></font></td>\n|;
		
	my $aff=sprintf("%.0f", $sstot[6]);
	my $aff2=sprintf("%.1f",$sstot[7]);
	my $aff3=sprintf("%.0f", $sstot[10]);
	$aff=&fnombre2($aff,3,0);
	$aff3=&fnombre2($aff3,3,0);
	print qq|
			<td align="right" nowrap><font class="PT"><b>$aff</font></b></td>
			<td align="right" nowrap><font class="PT"><b>$aff3</b></font></td>
			<td align="center" nowrap><font class="PT"><b>$aff2</font></b></td>
	|;
	$ut=0;
	my $ut1=0;

	if ($sstot[7]) {
		$ut=$sstot[6]/$sstot[7];
		$ut1=$sstot[10]/$sstot[7];
	}

	$ut=sprintf("%.1f",$ut);
	$ut1=sprintf("%.1f",$ut1);
	print qq|
			<td align="center" nowrap><font class="PT"><b>$ut</font></b></td>
			<td align="center" nowrap><font class="PT"><b>$ut1</font></b></td>
	|;
	# cout
	print qq|
			<td align="right" nowrap><font class="PT"><b>$affcouto</b></font></td>
	|;
	my $cjm=0;
	my $affcjm=0;

	if ($sstot[8]!=0) {
		$cjm=$couto/($nbjrmoy*$coef*$sstot[8]);
		$cjm=sprintf("%.1f",$cjm);
		$affcjm=$cjm;
	}

	print qq|
			<td align="right" nowrap><font class="PT"><b>$affcjm</b></font></td>
	|;
	my $pc=0;

	if ($cjm>0) {
		$pc=$ut1/$cjm;
	}

	$pc=sprintf("%.2f",$pc);
	print qq|
			<td align="right" nowrap><font class="PT"><b>$pc</b></font></td>
		</tr>
	|;

	# total
	print qq|
		<tr bgcolor="#C6CDC5">
			<td align="left" nowrap><font class="PT"><b>TOTAUX</b></font></td>
            <td align="center" nowrap><font class="PT"><b>$totaux[8]</font></b></td>
			<td align="center" nowrap><font class="PT"><b>$totaux[0]</font></b></td>
			<td align="center" nowrap><font class="PT"><b>$totaux[1]</font></b></td>
			<td align="center" nowrap><font class="PT"><b>$totaux[2]</font></b></td>
            <td align="center" nowrap><font class="PT"><b>$totaux[13]</font></b></td>
	|;
	my $ut=0;

	if ($totaux[8]) {
		$ut=(100*($totaux[2]+$totaux[13]))/$totaux[8];
	}

	$ut=sprintf("%.1f",$ut);
	print qq|
			<td align="right" nowrap><font class="PT"><b>$ut%</font></b></td>
	|;
	my $aff=sprintf("%.0f", $totaux[3]);
	my $aff2=sprintf("%.1f",$totaux[4]);
	my $aff3=sprintf("%.0f", $totaux[5]);
	$aff=&fnombre2($aff,3,0);
	$aff3=&fnombre2($aff3,3,0);
	print qq|
			<td align="right" nowrap><font class="PT"><b>$aff</font></b></td>
			<td align="right" nowrap><font class="PT"><b>$aff3</font></b></td>
			<td align="center" nowrap><font class="PT"><b>$aff2</font></b></td>
	|;
	$ut=0;

	if (($resc{$param[0]}->[11])&&($totaux[8])) {
		$ut=(100*$totaux[4])/($resc{$param[0]}->[11]*$totaux[8]);
	}

	$ut=sprintf("%.2f",$ut);
	print qq|
			<td align="right" nowrap><font class="PT"><b>$ut%</font></b></td>
	|;
	print qq|
			<td>&nbsp;</td>
			<td>&nbsp;</td>
	|;
	my $aff=sprintf("%.0f", $totaux[6]);
	my $aff2=sprintf("%.1f",$totaux[7]);
	my $aff3=sprintf("%.0f", $totaux[10]);
	$aff=&fnombre2($aff,3,0);
	$aff3=&fnombre2($aff3,3,0);
	print qq|
			<td align="right" nowrap><font class="PT"><b>$aff</font></b></td>
			<td align="right" nowrap><font class="PT"><b>$aff3</b></font></td>
			<td align="center" nowrap><font class="PT"><b>$aff2</font></b></td>
	|;
	$ut=0;
	my $ut1=0;

	if ($totaux[7]) {
		$ut=$totaux[6]/$totaux[7];
		$ut1=$totaux[10]/$totaux[7];
	}

	$ut=sprintf("%.1f",$ut);
	$ut1=sprintf("%.1f",$ut1);
	print qq|
			<td align="center" nowrap><font class="PT"><b>$ut</font></b></td>
			<td align="center" nowrap><font class="PT"><b>$ut1</font></b></td>
	|;
	# cout
	my $couto=0;
	my $affcouto=0;

	if ($resc{$param[0]}->[11]) {
		$couto=($ctaux/100.0)*$totaux[9];
		$couto=sprintf("%.0f", $couto);
		$affcouto=&fnombre2($couto,3,0);
	}

	print qq|
		</tr>
	</table>
	|;
}

sub statjr1 {
	my ($debutms, $finms, $blt, $ag) = @_;
	my ($andm, $msdm, $jrdm) = split(/:/, $debutms);
	my $dm = Delta_Days(1900, 1, 1, $andm, $msdm, $jrdm);
	my ($anfm, $msfm, $jrfm) = split(/:/, $finms);
	my $fm = Delta_Days(1900, 1, 1, $anfm, $msfm, $jrfm);
	my $jourmsouv = Delta_Days($andm, $msdm, $jrdm, $anfm, $msfm, $jrfm);
	my $totj = $jourmsouv + 1;
	my $uvfer = &jour_fer_ag_cnx($G_PAYS, $ag, $jrdm, $msdm, $andm, $jourmsouv, $DB);
	$jourmsouv += 1;
	my $ouvrwe = &jour_we($jourmsouv, $jrdm, $msdm, $andm);
	$jourmsouv = $jourmsouv - $ouvrwe - $uvfer;
	my $an = $anfm;
	my $ms = $msfm;
	my $ca = 0;
	my $forf = 0;
	my $mens = 0;
	my $cours = 0;
	my $arret = 0;
	my $tot = 0;
	my $transp = 0;
	my $carb = 0;
	my $ass = 0;
	# famille compta
	my $famill = "FAMACOMPTAAUTO";
	my @req;

	if ($date < $date_chgt_fact) {@req = &reqhist($famill, $an, $ms, $ag);}
	else {@req = &reqhistnew($famill, $an, $ms, $ag);}

	# CA
	$sth = $DB->prepare($req[0]);
	$sth->execute();

	while (my @data=$sth->fetchrow_array()) {
		if ($data[0] eq "") {$data[0] = 0;}

	      	$compta{$ag}{$data[0]}->[3] += $data[1];
      		$compta{$ag}{$data[0]}->[6] += $data[1];

		if ($blt) {$ca += $data[1];}
		
		# Pour les machines hors parc, le co�t imput�
		# est �gal au CA / 2
		if ($data[2] == -1 && ($data[3] eq "LOC" || $data[3] eq "REM" || $data[3] eq "REALQ" || $data[3] eq "ASS" || $data[3] eq "GDR" || $data[3] eq "J+" || $data[3] eq "J-")) {
			if ($blt) {
				$compta{$ag}{$data[0]}->[12] += $data[1] / 2;
				$compta{$ag}{$data[0]}->[11] += $data[1] / 2;
			}
		}
	}

	$sth->finish;
	# NB PAR ETAT
	$sth=$DB->prepare($req[1]);
	$sth->execute();

	while (my @data=$sth->fetchrow_array()) {
		if ($data[0] eq "") {$data[0] = 0;}
		if ($data[1] eq "2") {
      	 		$compta{$ag}{$data[0]}->[2] += $data[2];

			if ($blt) {$cours += $data[2];}
      	 	} else {
      	 		$compta{$ag}{$data[0]}->[1] += $data[2];

	      	 	if ($blt) {$arret += $data[2];}
      	 	}
		if ($blt) {$tot += $data[2];}
		if ($compta{$ag}{$data[0]}->[0]) {$compta{$ag}{$data[0]}->[0] += $data[2];}
		else {$compta{$ag}{$data[0]}->[0] = $data[2];}
	}

	$sth->finish;

	# Eliminer Les Doublons Arret De Contrat Entre 2 Facturation
	$sth=$DB->prepare($req[5]);
	$sth->execute();

	while (my @data=$sth->fetchrow_array()) {
		if ($data[0] eq "") {$data[0]=0;}
		if ($compta{$ag}{$data[0]}->[0] ne $data[1]) {
      	 		my $diff=$compta{$ag}{$data[0]}->[0]-$data[1];
      	 		$compta{$ag}{$data[0]}->[2] -= $diff;
      	 		$compta{$ag}{$data[0]}->[0] -= $diff;

      	 		if ($blt && $diff) {
      	 			$cours -= $diff;
      	 			$tot -= $diff;
      	 		}
      	 	}
	}

	$sth->finish;
	# Contrat En Cours De Grue
	$sth=$DB->prepare($req[6]);
	$sth->execute();

	while (my @data=$sth->fetchrow_array()) {
		if ($data[0] eq "") {$data[0] = 0;}

  	 	$compta{$ag}{$data[0]}->[2] += $data[1];
  	 	$compta{$ag}{$data[0]}->[1] -= $data[1];
	}

	$sth->finish;
	# Duree Contrat Jour
	$sth=$DB->prepare($req[2]);
	$sth->execute();

	while (my @data=$sth->fetchrow_array()) {
		if ($data[0] eq "") {$data[0] = 0;}

		$compta{$ag}{$data[0]}->[4] = $data[1];
      	 	$compta{$ag}{$data[0]}->[7] = $data[1];
      	 	$durtot{$ag} += $data[1];
	}

	$sth->finish;
	# duree et nombre contrat mensuel
	$sth=$DB->prepare($req[3]);
	$sth->execute();
	my %contratcode = ();

	while (my @data=$sth->fetchrow_array()) {
		if ($date < $date_chgt_fact) {
			if ($data[0] eq "") {$data[0] = 0;}

			my ($anff, $msff, $jrff) = split(/-/, $data[1]);
			my ($anfd, $msfd, $jrfd) = split(/-/, $data[2]);

			my $durft = Delta_Days($anfd, $msfd, $jrfd, $anff, $msff, $jrff);

			if ($durft < 0) {next;}

			my $nbferief = &jour_fer_ag_cnx($G_PAYS, $ag, $jrfd, $msfd, $anfd, $durft, $DB);
			my $nbjrwef  = 0;
  			my $joursupf = 0;
  			my $testjoursupf = &num_jour($jrff, $msff, $anff);

			if ($testjoursupf == 6) {$joursupf = 1;}
			if ($testjoursupf == 7) {$joursupf = 2;}

			$durft += 1;

			if ($durft == 1 && $testjoursupf == 7) {$joursupf = 1;}

			$nbjrwef = &jour_we($durft, $jrfd, $msfd, $anfd);
			$durft = $durft - $nbferief - $nbjrwef + $joursup;

			if ($compta{$ag}{$data[0]}->[4]) {
				$compta{$ag}{$data[0]}->[4] += $durft;
				$compta{$ag}{$data[0]}->[7] += $durft;
	      	 	} else {
				$compta{$ag}{$data[0]}->[4] = $durft;
				$compta{$ag}{$data[0]}->[7] = $durft;
	      	 	}

			$durtot{$ag} += $durft;

			if ($blt) {
				if (!$contratcode{$data[3]}) {$mens++;$contratcode{$data[3]}=1;}
				else {$contratcode{$data[3]}++;}
			}
		} else {
			if ($data[0] eq "") {$data[0] = 0;}
			if ($compta{$ag}{$data[0]}->[4]) {
				$compta{$ag}{$data[0]}->[4] += $data[1];
				$compta{$ag}{$data[0]}->[7] += $data[1];
	      	 	} else {
				$compta{$ag}{$data[0]}->[4] = $data[1];
				$compta{$ag}{$data[0]}->[7] = $data[1];
	      	 	}

	      	 	$durtot{$ag} += $data[1];

			if ($blt) {$mens += $data[2];}
		}
	}

	$sth->finish;

	# duree et nombre contrat forfait
	$sth=$DB->prepare($req[4]);
	$sth->execute();
	my %contratcode = ();

	while (my @data=$sth->fetchrow_array()) {
		if ($date < $date_chgt_fact) {
			if ($data[0] eq "") {$data[0] = 0;}

			my ($anff, $msff, $jrff) = split(/-/, $data[1]);
			my ($anfd, $msfd, $jrfd) = split(/-/, $data[2]);

			my $durft = Delta_Days($anfd, $msfd, $jrfd, $anff, $msff, $jrff);

			if ($durft < 0) {next;}

			my $nbferief = &jour_fer_ag_cnx($G_PAYS, $ag, $jrfd, $msfd, $anfd, $durft, $DB);
			my $nbjrwef  = 0;
  			my $joursupf = 0;
  			my $testjoursupf = &num_jour($jrff, $msff, $anff);

			if ($testjoursupf == 6) {$joursupf = 1;}
			if ($testjoursupf == 7) {$joursupf = 2;}

			$durft += 1;

			if ($durft == 1 && $testjoursupf == 7) {$joursupf = 1;}

			$nbjrwef = &jour_we($durft, $jrfd, $msfd, $anfd);
			$durft = $durft - $nbferief - $nbjrwef + $joursup;

			if ($compta{$ag}{$data[0]}->[4]) {
				$compta{$ag}{$data[0]}->[4] += $durft;
				$compta{$ag}{$data[0]}->[7] += $durft;
	      	 	} else {
				$compta{$ag}{$data[0]}->[4] = $durft;
				$compta{$ag}{$data[0]}->[7] = $durft;
	      	 	}

			$durtot{$ag} += $durft;

			if ($blt) {
				if (!$contratcode{$data[3]}) {$mens++;$contratcode{$data[3]}=1;}
				else {$contratcode{$data[3]}++;}
			}
		} else {
			if ($data[0] eq "") {$data[0] = 0;}
			if ($compta{$ag}{$data[0]}->[4]) {
	      	 		$compta{$ag}{$data[0]}->[4] += $data[1];
		      		$compta{$ag}{$data[0]}->[7] += $data[1];
	      	 	} else {
				$compta{$ag}{$data[0]}->[4] = $data[1];
				$compta{$ag}{$data[0]}->[7] = $data[1];
			}

			$durtot{$ag} += $data[1];

	      		if ($blt) {$forf += $data[2];}
	      	}
	}

	$sth->finish;
	$sth=$DB->prepare($req[9]);
	$sth->execute();

	while (my @data=$sth->fetchrow_array()) {
		if ($data[0] eq "") {$data[0]=0;}
		if ($compta{$ag}{$data[0]}->[10]) {
			$compta{$ag}{$data[0]}->[10] += $data[1];
			$compta{$ag}{$data[0]}->[5] += $data[1];
		} else {
			$compta{$ag}{$data[0]}->[10] = $data[1];
			$compta{$ag}{$data[0]}->[5] = $data[1];
		}

		$caloc{$ag} += $data[1];
	}

	$sth->finish;
	# carb transp

	if ($blt) {
		my $req = qq|
		select LIGNEREFARTICLE,
		  SUM(ROUND(ROUND(LIGNEQTITE, |.$tabSageFormats->{'price'}.qq|)*ROUND(LIGNEPRIX, |.$tabSageFormats->{'quantity'}.qq|), |.$tabSageFormats->{'amount'}.qq|)),
		  count(ENTETEFACTURE.ENTETEAUTO)
		FROM ENTETELIGNE
		LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
		LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
		WHERE ENTETEFACTURE.AGAUTO="$ag"
		AND YEAR(DATE)="$an"
		AND MONTH(DATE)="$ms"
		GROUP BY LIGNEREFARTICLE
		|;
		$sth = $DB->prepare($req);
		$sth->execute();

		while (my @data=$sth->fetchrow_array()) {
			if ($data[0] =~ /^TRANS/ || $data[0] eq "SERT") {$transp += $data[1];}
			if ($data[0] eq "CARB") {$carb = $data[1];}
		}

		$sth->finish;
	}

	my $nbdureems=0;
	my $nbdureeplus=0;
	# jour<5

	if ($blt) {
     		my $req=$req[7];
     		# nb contrat<5
	     	$sth=$DB->prepare($req);
		$sth->execute();

		if ($date < $date_chgt_fact) {
			while (my @data=$sth->fetchrow_array()) {
				my ($dareel, $dmreel, $djreel) = split(/-/, $data[1]);
				my ($fareel, $fmreel, $fjreel) = split(/-/, $data[2]);
				my $dureereel = Delta_Days($dareel, $dmreel, $djreel, $fareel, $fmreel, $fjreel);
				my $nbferiereel = &jour_fer_ag_cnx($G_PAYS, $ag, $djreel, $dmreel, $dareel, $dureereel, $DB);
				my $testjoursupreel = &num_jour($fjreel, $fmreel, $fareel);

				if ($testjoursupreel == 6) {my $joursupreel = 0};
				if ($testjoursupreel == 7) {my $joursupreel = 1};

				$dureereel += 1;

				if ($dureereel == 1 && $testjoursupreel == 7) {my $joursupreel = 1;}

				my $nbjrwereel = &jour_we($dureereel, $djreel, $dmreel, $dareel);
				$dureereel = $dureereel + $data[3] - $data[4] - $nbferiereel - $nbjrwereel + $joursupreel;

				if ($dureereel < 5) {$nbdureems++;}
			}
		} else {($nbdureems) += $sth->fetchrow_array();}
		$sth->finish;
	}
	# assurance
	if ($blt) {
     		my $req = $req[8];
	     	$sth = $DB->prepare($req);
		$sth->execute();
		($ass) = $sth->fetchrow_array();
     		$sth->finish;
     	}

	return ($tot, $ca, $transp, $carb, $nbdureems, $nbdureeplus, $cours, $arret, $ass, $mens, $forf, $jourmsouv);
}

sub reqhist {
	my ($chp, $an, $ms, $ag) = @_;
	my ($db, $dba, $fn, $fna) = &debfinmois($ms, $an);
	my @req = ();

	# CA
	$req[0] = qq{
	select $chp, SUM(ENTETEMONTANTFR)
	FROM ENTETEFACTURE
	LEFT join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE ENTETEFACTURE.AGAUTO="$ag"
	AND YEAR(ENTETEDATED)="$an"
	AND MONTH(ENTETEDATED)="$ms"
	GROUP BY $chp
	};

	# NB PAR ETAT
	$req[1] = qq{
	select $chp, LIETCONTRATAUTO, count(DISTINCT ENTETEFACTURE.CONTRATCODE)
	FROM ENTETEFACTURE
	LEFT join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE ENTETEFACTURE.AGAUTO="$ag"
	AND YEAR(ENTETEDATED)="$an"
	AND MONTH(ENTETEDATED)="$ms"
	GROUP BY $chp, LIETCONTRATAUTO
	};

	# duree contrat jour
	$req[2] = qq{
	SELECT $chp, SUM(LIGNEQTITE)
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE ENTETEFACTURE.AGAUTO="$ag"
	AND YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE="LOC"
	AND ENTETEABO="0"
	GROUP BY $chp
	};

	# duree et nombre contrat mensuel
	#################################################################
	$req[3] = qq{
	SELECT $chp, ENTETEDATEF, ENTETEDATED, ENTETEFACTURE.CONTRATCODE
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE ENTETEFACTURE.AGAUTO="$ag"
	AND YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE="LOC"
	AND ENTETEABO="-1"
	AND MOTARIFMSFR !=0
	};

	# duree et nombre contrat forfait
	#################################################################
	$req[4] = qq{
	select $chp, ENTETEDATEF, ENTETEDATED, ENTETEFACTURE.CONTRATCODE
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE ENTETEFACTURE.AGAUTO="$ag"
	AND YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE="LOC"
	AND ENTETEABO="-1"
	AND MOFORFAITFR !=0
	};

	# eliminer les doublons arret de contrat entre 2 facturation
	$req[5] = qq{
	select $chp, count(DISTINCT ENTETEFACTURE.CONTRATCODE)
	FROM ENTETEFACTURE
	LEFT join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE ENTETEFACTURE.AGAUTO="$ag"
	AND YEAR(ENTETEDATED)="$an"
	AND MONTH(ENTETEDATED)="$ms"
	GROUP BY $chp
	};

	# contrat en cours de grue
	$req[6] = qq{
	select $chp, count(*)
	FROM CONTRAT
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE CONTRATOK="-2"
	AND CONTRAT.AGAUTO="$ag"
	AND CONTRATDD<="$fn"
	AND CONTRATDR>"$fn"
	GROUP BY $chp
	};

	# nb contrat<5
	$req[7] = qq{
	select $chp, CONTRATDD, CONTRATDR, CONTRATJOURSPLUS, CONTRATJOURSMOINS
	FROM CONTRAT
	left join MONTANT on MONTANT.MOAUTO=CONTRAT.MOAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE CONTRATOK in ("-1","-2")
	AND LIETCONTRATAUTO != 6
	AND CONTRATDD<="$fn"
	AND CONTRATDR>="$db"
	AND AGAUTO="$ag"
	and MOTARIFMSFR = 0
	};

	$req[8] = qq{
	select count(DISTINCT ENTETEFACTURE.CONTRATCODE)
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND ENTETEFACTURE.AGAUTO="$ag"
	AND LIGNEREFARTICLE="ASS"
	};

	$req[9] = qq|
	SELECT $chp,
	   SUM(ROUND(ROUND(LIGNEQTITE, |.$tabSageFormats->{'quantity'}.qq|)*ROUND(LIGNEPRIX, |.$tabSageFormats->{'price'}.qq|), |.$tabSageFormats->{'amount'}.qq|),
	   count(ENTETEFACTURE.ENTETEAUTO)
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE in ("LOC", "ASS", "GDR", "REM")
	AND ENTETEFACTURE.AGAUTO="$ag"
	GROUP BY $chp
	|;

	return @req;
}

sub reqhistnew {
	my ($chp, $an, $ms, $ag) = @_;
	my ($db, $dba, $fn, $fna) = &debfinmois($ms, $an);
	my @req = ();

	# CA
	$req[0] = qq|
	select $chp,
	   ROUND(ROUND(LIGNEPRIX, |.$tabSageFormats->{'price'}.qq|)*ROUND(LIGNEQTITE, |.$tabSageFormats->{'quantity'}.qq|), |.$tabSageFormats->{'amount'}.qq|),
	   CONTRAT.CONTRATHORSPARC, LIGNEREFARTICLE
	from ENTETELIGNE
	left join ENTETEFACTURE on ENTETELIGNE.ENTETEAUTO=ENTETEFACTURE.ENTETEAUTO
	left join LIGNEFACTURE on LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	left join CONTRAT on ENTETEFACTURE.CONTRATAUTO=CONTRAT.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE ENTETEFACTURE.AGAUTO="$ag"
	AND YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
#	GROUP BY $chp
	|;

	# NB PAR ETAT
	$req[1] = qq{
	select $chp, LIETCONTRATAUTO, count(DISTINCT ENTETEFACTURE.CONTRATCODE)
	from ENTETELIGNE
	left join ENTETEFACTURE on ENTETELIGNE.ENTETEAUTO=ENTETEFACTURE.ENTETEAUTO
	left join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE ENTETEFACTURE.AGAUTO="$ag"
	AND YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	GROUP BY $chp, LIETCONTRATAUTO
	};

	# duree contrat jour
	$req[2] = qq{
	SELECT $chp, SUM(LIGNEQTITE)
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE ENTETEFACTURE.AGAUTO="$ag"
	AND YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE in ("LOC", "J+", "J-")
	AND ENTETEABO="0"
	GROUP BY $chp
	};

	# duree et nombre contrat mensuel
	$req[3] = qq{
	SELECT $chp, SUM(LIGNEQTITE), count(distinct ENTETEFACTURE.CONTRATCODE)
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE ENTETEFACTURE.AGAUTO="$ag"
	AND YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE in ("LOC", "J+", "J-")
	AND ENTETEABO="-1"
	AND MOTARIFMSFR !=0
	GROUP BY $chp
	};

	# duree et nombre contrat forfait
	$req[4] = qq{
	select $chp, SUM(LIGNEQTITE), count(distinct ENTETEFACTURE.CONTRATCODE)
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE ENTETEFACTURE.AGAUTO="$ag"
	AND YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE in ("LOC", "J+", "J-")
	AND ENTETEABO="-1"
	AND MOFORFAITFR !=0
	GROUP BY $chp
	};

	# eliminer les doublons arret de contrat entre 2 facturation
	$req[5] = qq{
	select $chp, count(DISTINCT ENTETEFACTURE.CONTRATCODE)
	from ENTETELIGNE
	left join ENTETEFACTURE on ENTETELIGNE.ENTETEAUTO=ENTETEFACTURE.ENTETEAUTO
	left join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE ENTETEFACTURE.AGAUTO="$ag"
	AND YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	GROUP BY $chp
	};

	# contrat en cours de grue
	$req[6] = qq{
	select $chp, count(*)
	FROM CONTRAT
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE CONTRATOK="-2"
	AND CONTRAT.AGAUTO="$ag"
	AND CONTRATDD<="$fn"
	AND CONTRATDR>"$fn"
	GROUP BY $chp
	};

	# nb contrat<5
	$req[7] = qq{
	select count(distinct ENTETEFACTURE.CONTRATCODE)
	from ENTETELIGNE
	left join ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	left join CONTRAT ON CONTRAT.CONTRATCODE=ENTETEFACTURE.CONTRATCODE
	left join MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where YEAR(DATE)="$an"
	and MONTH(DATE)="$ms"
	and CONTRATOK in (-1, -2)
	and MOTARIFMSFR = 0
	and ENTETEFACTURE.AGAUTO="$G_AGENCE"
	and CONTRATDUREE<5
	};
	$req[8] = qq{
	select count(DISTINCT ENTETEFACTURE.CONTRATCODE)
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND ENTETEFACTURE.AGAUTO="$ag"
	AND LIGNEREFARTICLE="ASS"
	};
	$req[9] = qq|
	SELECT $chp,
	   SUM(ROUND(ROUND(LIGNEQTITE, |.$tabSageFormats->{'quantity'}.qq|)*ROUND(LIGNEPRIX, |.$tabSageFormats->{'price'}.qq|), |.$tabSageFormats->{'amount'}.qq|)),
	   count(distinct ENTETEFACTURE.ENTETEAUTO)
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE in ("LOC", "REM", "ASS", "GDR", "J+", "J-")
	AND ENTETEFACTURE.AGAUTO="$ag"
	GROUP BY $chp
	|;

	return @req;
}

sub hissstatistique {
	my @param = @_;
	my @data  = split(/\:/, $date);
	my ($db, $dba, $fn, $fna) = &debfinmois($data[1], $data[0]);
	my @restt = &statjr1($db, $date, 1, $param[0]);
	my @resct = ();

	for(my $i=0;$i<@restt;$i++) {
		push(@resct, $restt[$i]);
	}

	$rest{$param[0]} = \@restt;
	$resc{$param[0]} = \@resct;
}

sub debfinmois {
	my ($mois, $an) = @_;
	my $debutms = "$an:$mois:01";
	my $debaff  = "01/$mois/$an";
	my $finmois = Days_in_Month($an, $mois);
	my $finms   = "$an:$mois:$finmois";
	my $finaff  = "$finmois/$mois/$an";

	return ($debutms, $debaff, $finms, $finaff);
}

sub jourdate {
	my $ht="";

	$ht .=$cgi->start_form;
	$ht .=$cgi->hidden("clauto",$clauto);
	$ht .=$cgi->hidden("groupe",$groupe);
	$ht .=qq{<select name="date" onchange="submit()">\n};
	$ht .=qq{<option value="">$mess[5024]\n};
	my $req=qq{
        SELECT MONTH(DATE),YEAR(DATE)
        FROM ENTETELIGNE
        LEFT join ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
        WHERE DATE < CONCAT_WS(":",YEAR(NOw()),MONTH(NOw()),"01")
            AND DATE >= "$tdt[0]-$tdt[1]-$tdt[2]"
        GROUP BY MONTH(DATE),YEAR(DATE)
        ORDER BY DATE DESC};
	$sth=$DB->prepare($req);
	$sth->execute();

	while (my @data=$sth->fetchrow_array()) {
		my $selected="";
		my ($db,$dba,$fn,$fna)=&debfinmois($data[0],$data[1]);

		if ($fn eq $date) {$selected=" selected";};

		$ht .=qq{<option value="$fn"$selected>$dba -- $fna\n};
      	 }

	$sth->finish;
	$ht .=qq{</select>\n};

	return $ht;
}

sub retour_date {
	my ($lepays) = @_;
	my $df;

	if ($lepays eq "FR") {$df = "2004:01:01";}
	if ($lepays eq "PT") {$df = "2004:03:01";}
	if ($lepays eq "ES") {$df = "2004:03:01";}

	return $df;
}

sub fnombre2 {
	my ($nb, $sep, $dec) = @_;
	my ($long);
	my $negatif = ($nb =~ tr/-//);
	$nb .= ".";
	# ----- suppression des caracteres <> chiffres et '.'
	$nb =~ tr/(0-9.)//cd;
	# ----- longueur doit etre un multiple de 3
	$long = length($nb);
	$long = $long + (6 - $long % 3);
	$nb = sprintf("%$long.2f", $nb);
	# -----  remplace les espaces par des 'X'
	$nb =~ s/ /X/g;

	if ($sep) {
		# ----- un espace tous les 3 caracteres
		$nb =~ s/(\w{3})/$1 /g;
		# ----- suppression de l'espace devant le point decimal
		$nb =~ s/ \././;
	}

	# ----- suppression des 'non chiffres' de debut
	$nb =~ s/( ?X ?)*//g;
	# ----- suppression des decimales si = '.00'

	if (!$dec) {$nb=~s/\.00//;}

	$nb = "-".$nb if $negatif;

	return ($nb);
}
