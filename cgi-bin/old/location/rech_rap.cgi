#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';

use CGI;
use DBI;
use autentif;
use pagehtml;
use accesbd;
use varglob;
use calculdate;
use fonctions;
use tracetemps;
use mymath;
use message;
use LOC::Globals;
use LOC::Machine;
use LOC::Request;

# Initialisation du chrono
my $timer = &init_chrono();


$url2 = "$HTTP_PORTAIL/cgi-bin/old/location";
$url = $url2."/rech_rap.cgi";
$doc = "modeles/";
$nblignes_aff = 20;
$hl_color = "#00ccff";
$BD = &Environnement("LOCATION");
$q = new CGI;
$q->autoEscape(undef);
my @mess=&message($G_PAYS);

#recuperation taux de change
my $tauxE = tauxLocalToEuro($G_PAYS);

push @valeur1,0;
$label1{"0"} = "Recherche";
push @valeur2,0;
$label2{"0"} = "Recherche";
%traduction = (
    "FR" => {
        "0"  => "Historique client",
        "1"  => "Contrat",
        "2"  => "Devis",
        "3"  => "Client",
        "4"  => "Mod�le Machine",
        "5"  => "Rechercher",
        "6"  => "de location(s)",
        "7"  => "N�Contrat",
        "8"  => "N�Parc",
        "9"  => "Chantier",
        "10" => "Date D�but",
        "11" => "Date Fin",
        "12" => "Dur�e",
        "13" => "Px Jour",
        "14" => "Px Mois",
        "15" => "Forfait",
        "16" => "Assurance",
        "17" => "Transport",
        "18" => "Transport Retour",
        "19" => "Etat",
        "20" => "N�Devis",
        "21" => "Date",
        "22" => "Qt�",
        "23" => "Tarif J/M/F",
        "24" => "Jour",
        "25" => "Mois",
        "26" => "Forfait",
        "27" => "Famille",
        "28" => "El�vation",
        "29" => "Energie",
        "30" => "D�signation",
        "31" => "Hauteur Travail",
        "32" => "Hauteur Plancher",
        "33" => "N�S�rie",
        "34" => "Agence",
        "35" => "Date Achat",
        "36" => "Fermer",
        "37" => "Choix",
        "38" => "Ville",
        "39" => " (optionnel)",
        "40" => "N�Parc",
        "41" => "Vous n'avez pas l'acc�s � cette fonctionnalit�",
        "42" => "Nouvelle recherche",
        "43" => "comprise",
        "44" => "Pr�-Contrat",
        "45" => "Arr�t",
        "46" => "Chantier",
        "47" => "Commande",
        "48" => "Remise",
        "49" => "Rem. excep.",
        "50" => "Contact",
        "51" => "Aller",
        "52" => "Retour"
    },
    "MA" => {
        "0"  => "Historique client",
        "1"  => "Contrat",
        "2"  => "Devis",
        "3"  => "Client",
        "4"  => "Mod�le Machine",
        "5"  => "Rechercher",
        "6"  => "de location(s)",
        "7"  => "N�Contrat",
        "8"  => "N�Parc",
        "9"  => "Chantier",
        "10" => "Date D�but",
        "11" => "Date Fin",
        "12" => "Dur�e",
        "13" => "Px Jour",
        "14" => "Px Mois",
        "15" => "Forfait",
        "16" => "Assurance",
        "17" => "Transport",
        "18" => "Transport Retour",
        "19" => "Etat",
        "20" => "N�Devis",
        "21" => "Date",
        "22" => "Qt�",
        "23" => "Tarif J/M/F",
        "24" => "Jour",
        "25" => "Mois",
        "26" => "Forfait",
        "27" => "Famille",
        "28" => "El�vation",
        "29" => "Energie",
        "30" => "D�signation",
        "31" => "Hauteur Travail",
        "32" => "Hauteur Plancher",
        "33" => "N�S�rie",
        "34" => "Agence",
        "35" => "Date Achat",
        "36" => "Fermer",
        "37" => "Choix",
        "38" => "Ville",
        "39" => " (optionnel)",
        "40" => "N�Parc",
        "41" => "Vous n'avez pas l'acc�s � cette fonctionnalit�",
        "42" => "Nouvelle recherche",
        "43" => "comprise",
        "44" => "Pr�-Contrat",
        "45" => "Arr�t",
        "46" => "Chantier",
        "47" => "Commande",
        "48" => "Remise",
        "49" => "Rem. excep.",
        "50" => "Contact",
        "51" => "Aller",
        "52" => "Retour"
    },
    "ES" => {
        "0"  => "Rese�a cliente",
        "1"  => "Contrato",
        "2"  => "Oferta",
        "3"  => "Cliente",
        "4"  => "Sis.Traslaci�n M�quina:",
        "5"  => "Buscar",
        "6"  => "de locac�on(es)",
        "7"  => "N�Contrato",
        "8"  => "N�Parque",
        "9"  => "Obra",
        "10" => "Data Salida",
        "11" => "Data Vuelta",
        "12" => "Duraci�n",
        "13" => "Precio D�a",
        "14" => "Precio Mes",
        "15" => "Cord�n",
        "16" => "Seguro",
        "17" => "Transporte ida",
        "18" => "Transporte vuelta",
        "19" => "Estado",
        "20" => "N�Oferta",
        "21" => "Data",
        "22" => "Cuantidad",
        "23" => "Arancel D/M/C",
        "24" => "D�a",
        "25" => "Mes",
        "26" => "Cord�n",
        "27" => "Familia",
        "28" => "Elevaci�n",
        "29" => "Energ�a",
        "30" => "Modelo",
        "31" => "Elevaci�n Trabajo",
        "32" => "Elevaci�n Pavimento",
        "33" => "N�S�ria",
        "34" => "Delegaci�n",
        "35" => "Data adquisici�n",
        "36" => "Cerrar",
        "37" => "Opci�n(es)",
        "38" => "Ciudad de obra",
        "39" => " (opcional)",
        "40" => "N�Parque",
        "41" => "Ud no tiene ning�n acceso a este rasgo",
        "42" => "Nueva busca",
        "43" => "incluida",
        "44" => "Pre Contrato",
        "45" => "Parada",
        "46" => "Obra",
        "47" => "Commande",
        "48" => "Reducci�n",
        "49" => "Red. excep.",
        "50" => "Contacto",
        "51" => "Ida",
        "52" => "Vuelta"
    },
    "IT" => {
        "0"  => "Storico cliente",
        "1"  => "Contratto",
        "2"  => "Preventivo",
        "3"  => "Cliente",
        "4"  => "Modello Macchina",
        "5"  => "Ricercare",
        "6"  => "di noleggi",
        "7"  => "N�Contratto",
        "8"  => "N�Parco",
        "9"  => "Cantiere",
        "10" => "Data di Inizio",
        "11" => "Data di Ritorno",
        "12" => "Durata",
        "13" => "Prezzo Giorno",
        "14" => "Prezzo Mese",
        "15" => "Forfait",
        "16" => "Assicurazione",
        "17" => "Trasporto Andata",
        "18" => "Trasporto Ritorno",
        "19" => "Stato",
        "20" => "N�Preventivo",
        "21" => "Data",
        "22" => "Quantit�",
        "23" => "Tariffa G/M/F",
        "24" => "Giorno",
        "25" => "Mese",
        "26" => "Forfait",
        "27" => "Famiglia",
        "28" => "Elevazione",
        "29" => "Energia",
        "30" => "Designazione",
        "31" => "Altezza Lavoro",
        "32" => "Altezza pavimento",
        "33" => "N�Serie",
        "34" => "Agenzia",
        "35" => "Data di acquisto",
        "36" => "Chiudere",
        "37" => "Scelta",
        "38" => "citt�",
        "39" => " (opzionale)",
        "40" => "N�Parco",
        "41" => "Non avete il diritto di accedere a questa funzionalit�",
        "42" => "Nuova ricerca",
        "43" => "compresa",
        "44" => "Pre-contratto",
        "45" => "Chiusura",
        "46" => "Obra",
        "47" => "Commande",
        "48" => "Remise",
        "49" => "Rem. excep.",
        "50" => "Contatto",
        "51" => "Andata",
        "52" => "Ritorno"
    },
    "PT" => {
        "0"  => "Historial cliente",
        "1"  => "Contrato",
        "2"  => "Oferta",
        "3"  => "Cliente",
        "4"  => "Modelo M�quina",
        "5"  => "Procurar",
        "6"  => "de aluguer(es)",
        "7"  => "N�Contrato",
        "8"  => "N�Parque",
        "9"  => "Obra",
        "10" => "Data de Partida",
        "11" => "Data de Regresso",
        "12" => "Dura��o",
        "13" => "Pr��o Dia",
        "14" => "Pr��o Mensal",
        "15" => "Empreitada",
        "16" => "Seguro",
        "17" => "Transporte",
        "18" => "Transporte de recolha",
        "19" => "Estado",
        "20" => "N�oferta",
        "21" => "Data",
        "22" => "Quantidade",
        "23" => "Tarifa D/M/E",
        "24" => "Dia",
        "25" => "Mensal",
        "26" => "Empreitada",
        "27" => "Fam�lias",
        "28" => "Eleva��o",
        "29" => "Energia",
        "30" => "Designa��o",
        "31" => "Altura Trabalho",
        "32" => "Altura Assoalho",
        "33" => "N�S�ries",
        "34" => "Ag�ncia",
        "35" => "Data de Compra",
        "36" => "Fechar",
        "37" => "Escolha(s)",
        "38" => "Localidade",
        "39" => " (opcional)",
        "40" => "N�Parque",
        "41" => "Voc� n�o tem o acesso a esta funcionalidade",
        "42" => "Novo procura",
        "43" => "compreendido",
        "44" => "Pr�-Contracto",
        "45" => "Paragem",
        "46" => "Obra",
        "47" => "Commande",
        "48" => "Redu��o",
        "49" => "Red. excep.",
        "50" => "Contacto",
        "51" => "Entrega",
        "52" => "Recolha"
    },
    "GB" => {
        "0"  => "History customer",
        "1"  => "Contract",
        "2"  => "Estimate",
        "3"  => "Customer",
        "4"  => "Model Machine",
        "5"  => "Look for",
        "6"  => "of rent(s)",
        "7"  => "Contract number",
        "8"  => "Park number",
        "9"  => "Construction site",
        "10" => "Beginning Date",
        "11" => "End Date",
        "12" => "Duration",
        "13" => "Daily Price",
        "14" => "Monthly Price",
        "15" => "Fixed price(package)",
        "16" => "Assurance",
        "17" => "Transport To go",
        "18" => "Transport Return",
        "19" => "Status",
        "20" => "N�Estimate",
        "21" => "Date",
        "22" => "Qty",
        "23" => "Rate D/M/F",
        "24" => "Day",
        "25" => "Mois",
        "26" => "Fixed price(package)",
        "27" => "Family",
        "28" => "Rise",
        "29" => "Energy",
        "30" => "Name",
        "31" => "Height Work",
        "32" => "Height To work",
        "33" => "Serial number",
        "34" => "Agency",
        "35" => "Date Purchase",
        "36" => "Close",
        "37" => "Choice",
        "38" => "City",
        "39" => " (optional)",
        "40" => "Park number",
        "41" => "You have no access to this feature",
        "42" => "New search",
        "43" => "included",
        "44" => "Pre-contract",
        "45" => "Stop",
        "46" => "Chantier",
        "47" => "Commande",
        "48" => "Remise",
        "49" => "Rem. excep.",
        "50" => "Contact"
    },
    "LU" => {
        "0"  => "Historique client",
        "1"  => "Contrat",
        "2"  => "Devis",
        "3"  => "Client",
        "4"  => "Mod�le Machine",
        "5"  => "Rechercher",
        "6"  => "de location(s)",
        "7"  => "N�Contrat",
        "8"  => "N�Parc",
        "9"  => "Chantier",
        "10" => "Date D�but",
        "11" => "Date Fin",
        "12" => "Dur�e",
        "13" => "Px Jour",
        "14" => "Px Mois",
        "15" => "Forfait",
        "16" => "Assurance",
        "17" => "Transport Aller",
        "18" => "Transport Retour",
        "19" => "Etat",
        "20" => "N�Devis",
        "21" => "Date",
        "22" => "Qt�",
        "23" => "Tarif J/M/F",
        "24" => "Jour",
        "25" => "Mois",
        "26" => "Forfait",
        "27" => "Famille",
        "28" => "El�vation",
        "29" => "Energie",
        "30" => "D�signation",
        "31" => "Hauteur Travail",
        "32" => "Hauteur Plancher",
        "33" => "N�S�rie",
        "34" => "Agence",
        "35" => "Date Achat",
        "36" => "Fermer",
        "37" => "Choix",
        "38" => "Ville",
        "39" => " (optionnel)",
        "40" => "N�Parc",
        "41" => "Vous n'avez pas l'acc�s � cette fonctionnalit�",
        "42" => "Nouvelle recherche",
        "43" => "comprise",
        "44" => "Pr�-Contrat",
        "45" => "Arr�t",
        "46" => "Chantier",
        "47" => "Commande",
        "48" => "Remise",
        "49" => "Rem. excep.",
        "50" => "Contact"
    }
);

&ente($traduction{$G_PAYS}{0}, $traduction{$G_PAYS}{0}, $G_PENOM, $G_PEPRENOM);

print qq{<SCRIPT language="JavaScript" src="$HTTP_PORTAIL/web/js/common.js"></SCRIPT>};
print qq{<SCRIPT language="JavaScript" src="$HTTP_PORTAIL/web/js/httpRequest.js"></SCRIPT>};

print '
<script type="text/javascript">
    var printHttpObj;
    function printList (file)
    {
        var printer = document.getElementsByName("imprimante")[0].value.substr(0, 7);
        var url     = "' . &LOC::Request::createRequestUri('print:render:print') . '";

        // Affichage d\'un indicateur de chargement
        var loadingImg = document.getElementById("img-loading");
        loadingImg.style.visibility = "visible";
        // Message de retour
        var resultMsg = document.getElementById("msg-result");
        resultMsg.style.display = "none";

        if (!printHttpObj)
        {
            printHttpObj = new Location.HTTPRequest(url);
            // Codes de retour valide :
            // 204 : pas de contenu (code de retour normal)
            // 1223 : code transform� par IE
            // 0 : code transform� par Google Chrome Frame
            printHttpObj.setValidResponseStatusRegExp("^0|204|1223$");
        }

        printHttpObj.oncompletion = function()
        {
            loadingImg.style.visibility = "hidden";
            resultMsg.style.display = "block";
        };
        printHttpObj.onerror = function()
        {
            // Erreur
            alert("Erreur " + printHttpObj.getResponseStatus());
            loadingImg.style.visibility = "hidden";
        };

        // Passage de l\'id du client en param�tre
        printHttpObj.setVar(\'printer\', printer);
        printHttpObj.setVar(\'url\', file);
        printHttpObj.setVar(\'number\', 1);

        // Envoi de la requ�te HTTP
        printHttpObj.send(true);

        return true;
    }
</script>
<style type="text/css">
    #btn-print {
        vertical-align: bottom;
    }
    #img-loading
    {
        margin-left: 10px;
        vertical-align: text-bottom;
        visibility: hidden;
    }
    #msg-result
    {
        display: none;
        font-style: italic;
    }
    table.tableau
    {
         width:100%;
         border: 0;
    }
    table.tableau td
    {
        font-size: 8pt;
        margin: 2px;
        padding: 0;
    }
</style>
';

print $q->start_form;
if (!&verif_droit) {
    print "<center><h1><font color=red>$traduction{$G_PAYS}{41}</font></h1></center>";
    exit;
}
$moma = $q->param('moma');
$manoparc = $q->param('manoparc');
$chantier = $q->param('chantier');
$verif_client = $q->param('client2')?$q->param('client2'):$q->param('client');
$verif_rech_client = $q->param('rech_client2')?$q->param('rech_client2'):$q->param('rech_client');
$verif_vicha = $q->param('vicha2')?$q->param('vicha2'):$q->param('vicha');
$verif_chant = $q->param('chant2')?$q->param('chant2'):$q->param('chant');
$q->param('chant2',$verif_chant);
$q->param('vicha2',$verif_vicha);
$q->param('client2',$verif_client);
$rech_client2 = $verif_rech_client;
$rech_client2 =~ s/"/&quot;/g;
$q->param('rech_client2', $rech_client2);
$nb = $q->param('nbpage')?$q->param('nbpage'):0;

if ($q->param('parc'))
{
    $noparc = $q->param('parc');
    $temp = $BD->prepare(qq|select FAMILLEMACHINE.FAMAMODELE,FAMILLEMACHINE.FAMAELEVATION,FAMILLEMACHINE.FAMAENERGIE,MODELEMACHINE.MOMADESIGNATION,MODELEMACHINE.MOMAHTTRAVAIL,MODELEMACHINE.MOMAHTPLANCHER,MACHINE.MANOSERIE,MACHINE.AGAUTO,date_format(MACHINE.MADATEACHAT,"%d/%m/%Y"),ETATTABLE.ETLIBELLE from AUTH.`MACHINE` left join AUTH.`MODELEMACHINE` on MODELEMACHINE.MOMAAUTO=MACHINE.MOMAAUTO left join FAMILLEMACHINE on FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO left join ETATTABLE on MACHINE.ETCODE=ETATTABLE.ETCODE where MACHINE.MANOPARC=$noparc|);
    $temp->execute();
    @valeureux = $temp->fetchrow_array();
    print qq|<table border=0 width=50% align=center bordercolor="#CCCCCC" cellspacing="2" cellpadding="0">\n|;
    print "<tr bgcolor=#FFFFFF><td><b>$traduction{$G_PAYS}{27}</b></td><td>$valeureux[0]</td></tr>\n";
    print "<tr bgcolor=''><td><b>$traduction{$G_PAYS}{28}</b></td><td>$valeureux[1]</td></tr>\n";
    print "<tr bgcolor=#FFFFFF><td><b>$traduction{$G_PAYS}{29}</b></td><td>$valeureux[2]</td></tr>\n";
    print "<tr bgcolor=''><td><b>$traduction{$G_PAYS}{30}</b></td><td>$valeureux[3]</td></tr>\n";
    print "<tr bgcolor=#FFFFFF><td><b>$traduction{$G_PAYS}{31}</b></td><td>$valeureux[4]</td></tr>\n";
    print "<tr bgcolor=''><td><b>$traduction{$G_PAYS}{32}</b></td><td>$valeureux[5]</td></tr>\n";
    print "<tr bgcolor=#FFFFFF><td><b>$traduction{$G_PAYS}{33}</b></td><td>$valeureux[6]</td></tr>\n";
    print "<tr bgcolor=''><td><b>$traduction{$G_PAYS}{34}</b></td><td>$valeureux[7]</td></tr>\n";
    print "<tr bgcolor=#FFFFFF><td><b>$traduction{$G_PAYS}{35}</b></td><td>$valeureux[8]</td></tr>\n";
    print "<tr bgcolor=''><td><b>$traduction{$G_PAYS}{19}</b></td><td>$valeureux[9]</td></tr>\n";
    print "</table>\n";
    $temp->finish;
    print qq|<br><br><center><a href="#" onclick="window.close()">$traduction{$G_PAYS}{36}</a></center>\n|;
    print $q->end_form();
    &Pied;
    $BD->disconnect;
    exit;
}

if ($q->param('chant'))
{
    @valeur2 = ();
    $crite = $q->param('chant');
    if ($crite ne $verif_chant)
    {
        $q->param('vicha',"");
        $q->delete('moma');
        $q->delete('chant2');
        $q->delete('manoparc');
        $q->delete('chantier');
    }
    print $q->hidden('chant2');
    $temp = $BD->prepare(qq|select VILLECHA.VILLEAUTO,VILLECHA.VINOM,VILLECHA.VICP from VILLECHA where (VINOM like "$crite%" or VICP like "$crite%") group by VILLECHA.VILLEAUTO|);
    $temp->execute();
    $rows = $temp->rows;
    if ($rows > 1)
    {
        push @valeur2,"0";
        $label2{"0"} = "---- $rows $traduction{$G_PAYS}{37} ----";
    }
    while (@temp = $temp->fetchrow_array())
    {
        push @valeur2,$temp[0];
        $label2{"$temp[0]"} = "$temp[1],$temp[2]";
    }
    $temp->finish;
}
else
{
    $q->delete('vicha');
    $crite = "";
}

if ($q->param('rech_client'))
{
    print &legende_clients;
    @valeur1 = ();
    @couleur1 = ();
    $crit_rech_client = $q->param('rech_client');
    if ($crit_rech_client ne $verif_rech_client)
    {
        $q->param('client',"");
        $q->delete('rech_client2');
        $q->delete('moma');
        $q->delete('manoparc');
        $q->delete('chantier');
    }
    print $q->hidden('rech_client2');
    my $crit_rech_client_sql = $crit_rech_client;
    $crit_rech_client_sql =~ s/"/\\"/g;
    $temp = $BD->prepare(qq|
    select CLAUTO, CLSTE, TARIFCLIENTREF,
        concat_ws(" ", CLADRESSE, CLADRESSE2) as CLADRESSE, VICLCP, VICLNOM,
        CLSAGE, CLCOLLECTIF, CLCLASSE, CLPOTENTIEL
    from CLIENT
    LEFT JOIN AUTH.`VICL` ON VICL.VICLAUTO = CLIENT.VICLAUTO
    LEFT join CLIENTTARIF ON CLIENTTARIF.CLIENTTARIFAUTO=CLIENT.CLIENTTARIFAUTO
    where CLSTE like "$crit_rech_client_sql%"
    order by CLSTE
    |);
    $temp->execute();
    $rows = $temp->rows;
    if ($rows > 1)
    {
        push @valeur1,"0";
        push @couleur1,"color: black";
        $label1{"0"} = "---- $rows $traduction{$G_PAYS}{37} ----";
    }
    while (@temp = $temp->fetchrow_array())
    {
        push @valeur1,$temp[0];
        my $collectif = $temp[7];
        my $blocage  = $temp[6];
        my $val = couleurClient($collectif, $blocage);
        if ($val == 1)
        {
            push @couleur1,"color: red";
        }
        elsif ($val == 2)
        {
            push @couleur1,"color: #00CC33";
        }
        else
        {
            push @couleur1,"color: black";
        }
        $label1{"$temp[0]"} = $temp[1].", ".$mess[2373]." ".$temp[8];
        if ($temp[9] ne "")
        {
            $label1{"$temp[0]"} .= ", ".$mess[2374]." ".$temp[9];
        }
        $label1{"$temp[0]"} .= ", ".$temp[2].", ".$temp[3].", ".$temp[4]." ".$temp[5];
    }
    $temp->finish;
}
else
{
    $q->delete('client');
    $crit_rech_client="";
}

$crit_client = $q->param('client');
$crit_vicha = $q->param('vicha');

print "<table border=0 align=center>\n";
print "<tr><td colspan=3 align=center><FONT class='PT'>",$q->radio_group(-name=>"choix", -value=>["$traduction{$G_PAYS}{1}","$traduction{$G_PAYS}{2}"]),"</FONT></td></tr>\n";
print "<tr><td><FONT class='PT'><b>$traduction{$G_PAYS}{3}</b></FONT></td>\n<td>";
print qq{<SELECT NAME="client" CLASS="select" TABINDEX="-1">\n};
for ($i=0; $i<@valeur1; $i++)
{
    my $select = ($crit_client eq $valeur1[$i]) ? " selected" : "";
    print qq{<OPTION VALUE="$valeur1[$i]" STYLE="$couleur1[$i]"$select>$label1{$valeur1[$i]}\n};
}
print qq{</select>\n};
my $rech_client = $q->param('rech_client');
$rech_client =~ s/"/&quot;/g;
$q->param('rech_client', $rech_client);
print "</td>\n<td>" ,$q->textfield(-name=>'rech_client');
if ($q->param('client'))
{
    $id_client = $q->param('client');
    print qq{&nbsp;<a href="#" onclick="return rs('infoclient', '$HTTP_PORTAIL/cgi-bin/old/location/infoclient.cgi?value=$id_client', 500, 450)">};
    print qq{<img src="$URL_IMG/histo.gif" width="50" height="22" alt="$mess[923]" border="0"></a>\n};
}
print "</td></tr>\n";
print "<tr><td><b><FONT class='PT'>$traduction{$G_PAYS}{38} $traduction{$G_PAYS}{9}</b></FONT></td>\n<td>",$q->popup_menu(-name=>'vicha', -value=>\@valeur2, -labels=>\%label2, -class=>"select", -tabindex=>"-1"),"</td>\n<td>",$q->textfield(-name=>'chant'),"</td></tr>";
print "<tr><td><b><FONT class='PT'>$traduction{$G_PAYS}{9}$traduction{$G_PAYS}{39}</b></FONT></td>\n<td>",$q->textfield(-name=>"chantier"),"</td></tr>";
print "<tr><td><b><FONT class='PT'>$traduction{$G_PAYS}{4}$traduction{$G_PAYS}{39}</b></FONT></td>\n<td colspan=2>",$q->textfield(-name=>'moma'),"</td></tr>";
print "<tr><td><b><FONT class='PT'>$traduction{$G_PAYS}{40}$traduction{$G_PAYS}{39}</b></FONT></td>\n<td colspan=2>",$q->textfield(-name=>'manoparc'),"</td></tr>";
print "<tr><td align=center colspan=3>",$q->image_button(-name=>'Rechercher', -src=>"$URL_IMG/loupe_off.gif", -alt=>"$traduction{$G_PAYS}{5}"),"</td></tr>\n";
print "</table>\n";
if ($q->param('client'))
{
    my $clauto=$q->param('client');
    my $req=qq{select TARIFCLIENTREF FROM CLIENT LEFT join CLIENTTARIF ON CLIENTTARIF.CLIENTTARIFAUTO=CLIENT.CLIENTTARIFAUTO where CLAUTO="$clauto"};
    my $sth = $BD->prepare($req);
    $sth->execute();
    my @tp = $sth->fetchrow_array();
    $sth->finish;
    print qq{<center><font class="GD" color="red"><b>Cat Tarif :$tp[0]</b></font></center>\n};
}

if ($q->param('client') || $q->param('vicha'))
{
    if ($crit_client != $verif_client)
    {
        $q->delete('client2');
    }
    if ($crit_vicha != $verif_vicha)
    {
        $q->delete('vicha2');
    }
    print $q->hidden('vicha2');
    print $q->hidden('client2');

    $ordre2 = $q->param('ordre') if $q->param('ordre');
    $ordre = "order by ".$q->param('ordre') if $q->param('ordre');

    if ($q->param('choix') eq "$traduction{$G_PAYS}{1}")
    {
        if ($q->param('ordre') eq "")
        {
            $ordre2 = "CONTRAT.CONTRATDD DESC";
            $ordre = "order by CONTRAT.CONTRATDD DESC" ;
        }
        $where = "CONTRATOK=-1 and CONTRAT.AGAUTO='$G_AGENCE'";
        if ($crit_vicha && !$crit_client)
        {
            $where .= " and CHANTIER.VILLEAUTO=$crit_vicha";
            $table = "CHANTIER left join CONTRAT on CONTRAT.CHAUTO=CHANTIER.CHAUTO left join AUTH.`MODELEMACHINE` on MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO left join AUTH.`MACHINE` on CONTRAT.MAAUTO=MACHINE.MAAUTO left join MONTANT on MONTANT.MOAUTO=CONTRAT.MOAUTO left join LIETCONTRAT on LIETCONTRAT.LIETCONTRATAUTO=CONTRAT.LIETCONTRATAUTO left join VILLECHA on VILLECHA.VILLEAUTO=CHANTIER.VILLEAUTO left join CLIENT on CONTRAT.CLAUTO=CLIENT.CLAUTO";
        }
        else
        {
            $where .= " and CONTRAT.CLAUTO=$crit_client";
            $where .= " and CHANTIER.VILLEAUTO=$crit_vicha" if $crit_vicha;
            $table = "CONTRAT left join AUTH.`MODELEMACHINE` on MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO left join AUTH.`MACHINE` on CONTRAT.MAAUTO=MACHINE.MAAUTO left join MONTANT on MONTANT.MOAUTO=CONTRAT.MOAUTO left join LIETCONTRAT on LIETCONTRAT.LIETCONTRATAUTO=CONTRAT.LIETCONTRATAUTO left join CHANTIER on CONTRAT.CHAUTO=CHANTIER.CHAUTO left join VILLECHA on VILLECHA.VILLEAUTO=CHANTIER.VILLEAUTO";
        }
        $table .= " left join `p_appealtype` on CONTRAT.CONTRATTYPERECOURS = apl_id ";

        $selection = qq|
            CONTRAT.CONTRATCODE AS `code`, MODELEMACHINE.MOMADESIGNATION AS `model.label`,
            CONCAT_WS("",MACHINE.MANOPARC_AUX,MACHINE.MANOPARC) AS `machine.parkNumber`,
            date_format(CONTRAT.CONTRATDD,"%d/%m/%Y") AS `beginDate`,
            date_format(CONTRAT.CONTRATDR,"%d/%m/%Y") AS `endDate`,
            CONTRAT.CONTRATDUREE AS `duration`, MONTANT.MOREMISE AS `reduction.amount`, MONTANT.MOPXJOURFR AS `unitPrice`,
            CONTRAT.CONTRATTYPEASSURANCE AS `insuranceType.id`, CONTRATTYPERECOURS AS `appealType.id`, apl_label AS `appealType.label`,
            MONTANT.MOTRANSPORTDFR AS `delivery.amount`,
            MONTANT.MOTRANSPORTRFR AS `recovery.amount`, LIETCONTRAT.LIETCONTRATLIBELLE AS `state.label`,
            CONTRAT.CONTRATAUTO AS `id`, MONTANT.MOTARIFMSFR AS `monthTariff`, MONTANT.MOFORFAITFR AS `packageTariff`,
            concat_ws("",CHLIBELLE,"<BR>",VILLECHA.VICP," ",VILLECHA.VINOM) AS `site.label`,
            CHANTIER.CHCONTACT AS `site.contactName`, CONTRAT.CONTRATCONTACTCOMM AS `orderContact`, MONTANT.MOREMISEEX AS `specialReduction.amount`
        |;

        if ($crit_vicha && !$crit_client)
        {
            $selection .= ",CLIENT.CLSTE AS `customer.label`";
        }

        $devcon = "$traduction{$G_PAYS}{6}";
    }
    elsif ($q->param('choix') eq "$traduction{$G_PAYS}{2}")
    {
        if ($q->param('ordre') eq "")
        {
            $ordre2 = "DEVIS.DEVISAUTO DESC";
            $ordre = "order by DEVIS.DEVISAUTO DESC" ;
        }
        $where = "DEVIS.AGAUTO='$G_AGENCE'";
        if ($crit_vicha && !$crit_client)
        {
            $where .= " and CHANTIER.VILLEAUTO=$crit_vicha";
            $table = "CHANTIER left join DEVIS on CHANTIER.CHAUTO=DEVIS.CHAUTO left join DETAILSDEVIS on DETAILSDEVIS.DEVISAUTO=DEVIS.DEVISAUTO left join AUTH.`MODELEMACHINE` on DETAILSDEVIS.MOMAAUTO=MODELEMACHINE.MOMAAUTO left join ETATTABLE on DEVIS.ETCODE=ETATTABLE.ETCODE left join VILLECHA on VILLECHA.VILLEAUTO=CHANTIER.VILLEAUTO left join CLIENT on DEVIS.CLAUTO=CLIENT.CLAUTO";
        }
        else
        {
            $where .= " and DEVIS.CLAUTO=$crit_client";
            $where .= " and CHANTIER.VILLEAUTO=$crit_vicha" if $crit_vicha;
            $table = "DEVIS left join DETAILSDEVIS on DETAILSDEVIS.DEVISAUTO=DEVIS.DEVISAUTO left join AUTH.`MODELEMACHINE` on DETAILSDEVIS.MOMAAUTO=MODELEMACHINE.MOMAAUTO left join ETATTABLE on DEVIS.ETCODE=ETATTABLE.ETCODE left join CHANTIER on CHANTIER.CHAUTO=DEVIS.CHAUTO left join VILLECHA on VILLECHA.VILLEAUTO=CHANTIER.VILLEAUTO";
        }
        $table .= " left join `p_appealtype` on DETAILSDEVIS.DETAILSDEVISTYPERECOURS = apl_id ";
        $selection = qq|
            DEVIS.DEVISAUTO AS `estimate.id`, MODELEMACHINE.MOMADESIGNATION as `model.label`,
            concat_ws("",CHANTIER.CHLIBELLE,"<BR>",VILLECHA.VICP," ",VILLECHA.VINOM) AS `site.label`,
            date_format(DETAILSDEVISDATE,"%d/%m/%Y") AS `beginDate`, DETAILSDEVISQTE AS `quantity`,
            DETAILSDEVISREMISE AS `reduction.amount`, DETAILSDEVISPXJOURSFR AS `unitPrice`, DETAILSDEVISDUREE AS `duration`,
            DETAILSDEVISTYPEASSURANCE AS `insuranceType.id`, DETAILSDEVISTYPERECOURS AS `appealType.id`, apl_label AS `appealType.label`,
            DETAILSDEVISTRD AS `delivery.amount`, DETAILSDEVISTRR AS `recovery.amount`,
            ETATTABLE.ETLIBELLE AS `state.label`, DETAILSDEVISTYPEMONTANT AS `amountType`,
            CHANTIER.CHCONTACT AS `site.contactName`, DEVIS.DEVISCONTACTCOM AS `orderContact`,
            DETAILSDEVISREMISEEX AS `specialReduction.amount`, DETAILSDEVISAUTO AS `id`
        |;
        $devcon = "$traduction{$G_PAYS}{2}";

        if ($crit_vicha && !$crit_client)
        {
            $selection .= ",CLIENT.CLSTE AS `customer.label`";
        }
    }

    $wheress = " and MOMADESIGNATION='$moma' " if $moma;
    $wheress .= " and CHLIBELLE='$chantier' " if $chantier;
    $wheress .= " and MANOPARC='$manoparc' " if $manoparc and $q->param('choix') eq "$traduction{$G_PAYS}{1}";
    $wheress .= " and CHANTIER.VILLEAUTO=$crit_vicha" if $crit_vicha;
    $wheress .= " and CLAUTO=$crit_client" if $crit_client;
    if ($q->param('choix') eq "$traduction{$G_PAYS}{1}")
    {
        if ($crit_vicha && !$crit_client)
        {
            $tabless = "CHANTIER left join CONTRAT on CONTRAT.CHAUTO=CHANTIER.CHAUTO ";
        }
        else
        {
            $tabless = "CONTRAT ";
            $tabless .= "left join CHANTIER on CHANTIER.CHAUTO=CONTRAT.CHAUTO " if $crit_vicha || $chantier;
        }
        $tabless .= " left join AUTH.`MODELEMACHINE` on CONTRAT.MOMAAUTO=MODELEMACHINE.MOMAAUTO " if $moma;
        $tabless .= " left join AUTH.`MACHINE` on MACHINE.MAAUTO=CONTRAT.MAAUTO " if $manoparc;
        $tabless .= " left join `p_appealtype` on CONTRAT.CONTRATTYPERECOURS = apl_id ";
        $temp = $BD->prepare(qq|select count(CONTRAT.CONTRATAUTO) from $tabless where CONTRATOK=-1 and CONTRAT.AGAUTO="$G_AGENCE" $wheress|);
    }
    elsif ($q->param('choix') eq "$traduction{$G_PAYS}{2}")
    {
        if ($crit_vicha && !$crit_client)
        {
            $tabless = "CHANTIER left join DEVIS on DEVIS.CHAUTO=CHANTIER.CHAUTO ";
        }
        else
        {
            $tabless = "DEVIS";
            $tabless .= " left join DETAILSDEVIS on DETAILSDEVIS.DEVISAUTO=DEVIS.DEVISAUTO";
            $tabless .= " left join CHANTIER on CHANTIER.CHAUTO=DEVIS.CHAUTO " if $crit_vicha || $chantier;
            $tabless .= " left join `p_appealtype` on DETAILSDEVIS.DETAILSDEVISTYPERECOURS = apl_id ";
        }
        $tablemoma = "left join AUTH.`MODELEMACHINE` on DETAILSDEVIS.MOMAAUTO=MODELEMACHINE.MOMAAUTO " if $moma;

        $temp = $BD->prepare(qq|select count(DEVIS.DEVISAUTO) from $tabless $tablemoma where DEVIS.AGAUTO="$G_AGENCE" $wheress|);
    }
    $temp->execute();
    $nbTotal = $temp->fetchrow_array();
    $temp->finish;
    $nbpage = $nbTotal/$nblignes_aff>int($nbTotal/$nblignes_aff)?int($nbTotal/$nblignes_aff)+1:int($nbTotal/$nblignes_aff);
    $where .= " and MOMADESIGNATION='$moma' " if $moma;
    $where .= " and CHLIBELLE='$chantier' " if $chantier;
    $where .= " and MANOPARC='$manoparc'" if $manoparc and $q->param('choix') eq "$traduction{$G_PAYS}{1}";

    $selimp = "select $selection from $table where $where $ordre limit $nb,$nblignes_aff";
    my $req = qq|select $selection from $table where $where $ordre limit $nb,$nblignes_aff|;


    $temp = $BD->prepare($req);
    $temp->execute() or print $temp->errstr;

    $rows = $temp->rows;
    $rows += $nb;
    print "<center><font color=\"#00CC33\"><B>Nb $devcon : $rows / $nbTotal</b></font></center>\n";

    if ($rows > 0)
    {
        $varia = "nbpage=$nb";
        $varia .= "&rech_client=$crit_rech_client&client=$crit_client" if $crit_client;
        $varia .= "&chant=$crite&vicha=$crit_vicha" if $crit_vicha;
        $varia .= "&moma=$moma" if $moma;
        $varia .= "&chantier=$chantier" if $chantier;
        $varia .= "&manoparc=$manoparc" if $manoparc and $q->param('choix') eq "$traduction{$G_PAYS}{1}";
        $choix = $q->param('choix');
        # Contrat
        if ($q->param('choix') eq "$traduction{$G_PAYS}{1}")
        {
            $aff = qq|
                    <tr bgcolor='#c6cdc5'>
                        <th><a href="$url?choix=$choix&ordre=CONTRAT.CONTRATCODE&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{7}';return true;" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{7}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=MODELEMACHINE.MOMADESIGNATION&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{4}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{4}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=MACHINE.MANOPARC&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{8}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{8}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=CHANTIER.CHLIBELLE&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{9}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{9}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=CONTRAT.CONTRATDD&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{10}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{10}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=CONTRAT.CONTRATDR&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{11}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{11}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=CONTRAT.CONTRATDUREE&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{12}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{12}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=MONTANT.MOPXJOURFR&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{48}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{48}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=MONTANT.MOPXJOURFR&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{23}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{23}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=MONTANT.MOREMISEEX&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{49}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{49}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=CONTRAT.CONTRATTYPEASSURANCE&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{16}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{16}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=MONTANT.MOTRANSPORTDFR&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{17}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{17}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=LIETCONTRAT.LIETCONTRATAUTO&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{19}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{19}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=CHANTIER.CHCONTACT&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{50}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{50}</font></a></th>
                |;

            if ($crit_vicha && !$crit_client)
            {
                $aff .= qq|
                        <th><a href="$url?choix=$choix&ordre=CLIENT.CLSTE&$varia" onMouseOver="window.status='Client';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>Client</font></a></th>
                |;
            }
            $aff .= qq|
                    </tr>
                |;
        }
        # Devis
        elsif ($q->param('choix') eq "$traduction{$G_PAYS}{2}")
        {
            $aff = qq|
                    <tr bgcolor='#c6cdc5'>
                        <th><a href="$url?choix=$choix&ordre=DEVIS.DEVISAUTO&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{20}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{20}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=MODELEMACHINE.MOMADESIGNATION&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{4}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{4}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=CHANTIER.CHLIBELLE&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{9}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{9}</a></th>
                        <th><a href="$url?choix=$choix&ordre=DETAILSDEVISDATE&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{21}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{21}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=DETAILSDEVISQTE&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{22}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{22}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=DETAILSDEVISREMISE&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{48}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{48}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=DETAILSDEVISPXJOURSFR&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{23}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{23}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=DETAILSDEVISREMISEEX&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{49}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{49}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=DETAILSDEVISDUREE&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{12}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{12}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=DETAILSDEVISTYPEASSURANCE&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{16}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{16}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=DETAILSDEVISTRD&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{17}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{17}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=ETLIBELLE&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{19}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{19}</font></a></th>
                        <th><a href="$url?choix=$choix&ordre=CHANTIER.CHCONTACT&$varia" onMouseOver="window.status='$traduction{$G_PAYS}{50}';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>$traduction{$G_PAYS}{50}</font></a></th>
                |;

            if ($crit_vicha && !$crit_client)
            {
                $aff .= qq|
                        <th><a href="$url?choix=$choix&ordre=CLIENT.CLSTE&$varia" onMouseOver="window.status='Client';return(true);" onMouseOut="window.status='';return(true);"><font class='PT'>Client</font></a></th>
                |;
            }
            $aff .= qq|
                    </tr>
                    |;
        }

        $nb2 = 0;
        print "<center>";
        $varia .= "&ordre=$ordre2" if $ordre;
        $varia2 = $varia;
        $popupmenu = qq|<SELECT NAME="pagination" onChange="document.location=this.options[this.selectedIndex].value">\n|;

        for ($i=1;$i<=$nbpage;$i++)
        {
            $varia2 =~ s/nbpage=\d+/nbpage=$nb2/;
            if ($nb == $nb2)
            {
                $popupmenu.= qq{<OPTION VALUE="page $i" selected>page $i};
            }
            else
            {
                $popupmenu.= qq{<OPTION VALUE="$url?choix=$choix&$varia2">page $i};
            }
            $nb2 += $nblignes_aff;
        }

        $popupmenu .= qq|</SELECT>\n|;
        print $popupmenu;
        print "</center>";
        print "<br>";
        print qq|<table class="tableau">\n|;

        $i=0;

        # Tableau des identifiants de lignes de devis affich�es
        my @tabEstimateLinesIds;
        my @tabContractsIds;

        while (my $tabRow = $temp->fetchrow_hashref())
        {
            $couleur2 = ($i % 2 == 0 ? "#d6d6d6" : "");
            # Contrat
            if ($q->param('choix') eq "$traduction{$G_PAYS}{1}")
            {
                # R�cup�ration des identifiants des contrats affich�s
                push(@tabContractsIds, $tabRow->{'id'});

                my $contractUrl = &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $tabRow->{'id'}});
                $tabRow->{'state.label'} = qq|<a href="$contractUrl" target="_blank">$tabRow->{'state.label'}</a>|;
                my $prix = 0;
                if ($tabRow->{'specialReduction.amount'} == 100)
                {
                    $tabRow->{'specialReduction.amount'} = 99.99;
                }
                if ($tabRow->{'unitPrice'} != 0)
                {
                    $prix = $tabRow->{'unitPrice'};
                    $tabRow->{'unitPrice'} = qq{<span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">}.&format_monetaire_wu(&arrondi($tabRow->{'unitPrice'} * 100 / (100 - $tabRow->{'specialReduction.amount'}), 0))."</span> $G_MONNAIE / ".$traduction{$G_PAYS}{24};
                }
                elsif ($tabRow->{'monthTariff'} != 0)
                {
                    $prix = $tabRow->{'monthTariff'};
                    $tabRow->{'unitPrice'} = qq{<span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">}.&format_monetaire_wu(&arrondi($tabRow->{'monthTariff'} * 100 / (100 - $tabRow->{'specialReduction.amount'}), 0))."</span> $G_MONNAIE / ".$traduction{$G_PAYS}{25};
                }
                elsif ($tabRow->{'packageTariff'} != 0)
                {
                    $prix = $tabRow->{'packageTariff'};
                    $tabRow->{'unitPrice'} = qq{<span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">}.&format_monetaire_wu(&arrondi($tabRow->{'packageTariff'} * 100 / (100 - $tabRow->{'specialReduction.amount'}), 0))."</span> $G_MONNAIE / ".$traduction{$G_PAYS}{26};
                }
                $prix = &arrondi($prix, 2);
                # Assurance + gestion de recours
                $src = qq{<font class='PT'>}.&aff_assurance("", $tabRow->{'insuranceType.id'}, 4).qq{</font>};
                if ($tabRow->{'appealType.id'})
                {
                    $src .= '<br /><i>' . $mess[3056]  . '</i> : ' . $tabRow->{'appealType.label'};
                }
                $tabRow->{'delivery.amount'} = "<i>".$traduction{$G_PAYS}{51}." :</i> ".qq{<span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">}.&format_monetaire_wu($tabRow->{'delivery.amount'})."</span> $G_MONNAIE";
                $tabRow->{'recovery.amount'} = "<i>".$traduction{$G_PAYS}{52}." :</i> ".qq{<span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">}.&format_monetaire_wu($tabRow->{'recovery.amount'})."</span> $G_MONNAIE";
                $tabRow->{'reduction.amount'} = sprintf("%.0f %", $tabRow->{'reduction.amount'});
                $tabRow->{'site.contactName'} = "<i>".$traduction{$G_PAYS}{46}." :</i> " . $tabRow->{'site.contactName'};
                $tabRow->{'orderContact'} = "<i>".$traduction{$G_PAYS}{47}." :</i> " . $tabRow->{'orderContact'};

                # Remise exceptionnelle
                $tabRow->{'specialReduction.amount'} =~ s/(\d+\.0*?[1-9]*)0+$/$1/g;
                $tabRow->{'specialReduction.amount'} =~ s/(\d+)\.$/$1/g;
                if ($tabRow->{'specialReduction.amount'} != 0)
                {
                    $tabRow->{'specialReduction.amount'} = "&ndash; ". $tabRow->{'specialReduction.amount'} ." %<BR>= ".qq{<span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">}."$prix</span> $G_MONNAIE";
                }
                else
                {
                    $tabRow->{'specialReduction.amount'} = "";
                }

                $newmac = $tabRow->{'machine.parkNumber'};
                if ($tabRow->{'machine.parkNumber'} =~ /(\d+)\s\(/)
                {
                    $tabRow->{'machine.parkNumber'} = $1;
                }

                my $contractUrl = &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $tabRow->{'id'}});
                my $machineUrl  = &LOC::Machine::getUrlByParkNumber($tabRow->{'machine.parkNumber'});

                $aff .= qq|
                    <tr bgColor='$couleur2' onmouseout="bgColor=persist_couleur(this.bgColor,1,'$couleur2')" onmouseover="bgColor=persist_couleur(this.bgColor,0,'$couleur2')" onclick="bgColor=couleur(this.bgColor,'$couleur2')">
                        <td><a href="$contractUrl" target="_blank"><font class='PT'>$tabRow->{'code'}</font></a></td>
                        <td>$tabRow->{'model.label'}</td>
                        <td><a href="$machineUrl" target="_blank"><font class='PT'>$newmac</font></a></td>
                        <td>$tabRow->{'site.label'}</td>
                        <td>$tabRow->{'beginDate'}</td>
                        <td>$tabRow->{'endDate'}</td>
                        <td>$tabRow->{'duration'}</td>
                        <td align="right">$tabRow->{'reduction.amount'}</td>
                        <td align="right" nowrap>$tabRow->{'unitPrice'}</td>
                        <td align="right" nowrap>$tabRow->{'specialReduction.amount'}</td>
                        <td>$src</td>
                        <td align="right" nowrap>$tabRow->{'delivery.amount'}<BR>$tabRow->{'recovery.amount'}</td>
                        <td>$tabRow->{'state.label'}</td>
                        <td>$tabRow->{'site.contactName'}<br>$tabRow->{'orderContact'}</td>|;

                if ($crit_vicha && !$crit_client)
                {
                    $aff .= qq|
                        <td>$tabRow->{'customer.label'}</td>|;
                }
                $aff .= qq|
                    </tr>|;
            }
            # Devis
            elsif ($q->param('choix') eq "$traduction{$G_PAYS}{2}")
            {
                # R�cup�ration des identifiants des lignes de devis affich�es
                push(@tabEstimateLinesIds, $tabRow->{'id'});

                # Assurance + gestion de recours
                $src = qq{<font class='PT'>}.&aff_assurance("", $tabRow->{'insuranceType.id'}, 4).qq{</font>};
                if ($tabRow->{'appealType.id'})
                {
                    $src .= '<br /><i>' . $mess[3056] . '</i> : ' . $tabRow->{'appealType.label'};
                }
                my $prix = &arrondi($tabRow->{'unitPrice'}, 2);
                if ($tabRow->{'specialReduction.amount'} == 100)
                {
                    $tabRow->{'specialReduction.amount'} = 99.99;
                }
                $tabRow->{'unitPrice'} = qq{<span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">}.&format_monetaire_wu(&arrondi($tabRow->{'unitPrice'} * 100 / (100 - $tabRow->{'specialReduction.amount'}), 0))."</span> $G_MONNAIE";
                $tabRow->{'amountType'} = "" if $tabRow->{'amountType'}==0;
                $tabRow->{'amountType'} = "$traduction{$G_PAYS}{24}" if $tabRow->{'amountType'}==1;
                $tabRow->{'amountType'} = "$traduction{$G_PAYS}{25}" if $tabRow->{'amountType'}==2;
                $tabRow->{'amountType'} = "$traduction{$G_PAYS}{26}" if $tabRow->{'amountType'}==3;
                $tabRow->{'delivery.amount'} = qq{<span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">}.&format_monetaire_wu($tabRow->{'delivery.amount'})."</span> $G_MONNAIE";
                $tabRow->{'recovery.amount'} = qq{<span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">}.&format_monetaire_wu($tabRow->{'recovery.amount'})."</span> $G_MONNAIE";
                $tabRow->{'reduction.amount'} = sprintf("%.0f %", $tabRow->{'reduction.amount'});
                $tabRow->{'delivery.amount'} = "<i>".$traduction{$G_PAYS}{51}." :</i> " . $tabRow->{'delivery.amount'};
                $tabRow->{'recovery.amount'} = "<i>".$traduction{$G_PAYS}{52}." :</i> " . $tabRow->{'recovery.amount'};
                $tabRow->{'site.contactName'} = "<i>".$traduction{$G_PAYS}{46}." :</i> " . $tabRow->{'site.contactName'};
                $tabRow->{'orderContact'} = "<i>".$traduction{$G_PAYS}{47}." :</i> " . $tabRow->{'orderContact'};

                # Remise exceptionnelle
                $tabRow->{'specialReduction.amount'} =~ s/(\d+\.0*?[1-9]*)0+$/$1/g;
                $tabRow->{'specialReduction.amount'} =~ s/(\d+)\.$/$1/g;
                if ($tabRow->{'specialReduction.amount'} != 0)
                {
                    $tabRow->{'specialReduction.amount'} = "&ndash; " . $tabRow->{'specialReduction.amount'} . " %<BR>= ".qq{<span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">}."$prix</span> $G_MONNAIE";
                }
                else
                {
                    $tabRow->{'specialReduction.amount'} = "";
                }

                my $urlDevis = &LOC::Request::createRequestUri('rent:rentEstimate:view', {'estimateId' => $tabRow->{'estimate.id'}});

                $aff .= qq|
                    <tr bgColor='$couleur2' onmouseout="bgColor=persist_couleur(this.bgColor,1,'$couleur2')" onmouseover="bgColor=persist_couleur(this.bgColor,0,'$couleur2')" onclick="bgColor=couleur(this.bgColor,'$couleur2')">
                        <td><a href="$urlDevis" target="_blank"><font class='PT'>$tabRow->{'estimate.id'}</font></a></td>
                        <td>$tabRow->{'model.label'}</td>
                        <td>$tabRow->{'site.label'}</td>
                        <td>$tabRow->{'beginDate'}</td>
                        <td>$tabRow->{'quantity'}</td>
                        <td align="right">$tabRow->{'reduction.amount'}</td>
                        <td align="right" nowrap>$tabRow->{'unitPrice'} /&nbsp;$tabRow->{'amountType'}</td>
                        <td align="right" nowrap>$tabRow->{'specialReduction.amount'}</td>
                        <td>$tabRow->{'duration'}</td>
                        <td>$src</td>
                        <td align="right" nowrap>$tabRow->{'delivery.amount'}<BR>$tabRow->{'recovery.amount'}</td>
                        <td>$tabRow->{'state.label'}</td>
                        <td>$tabRow->{'site.contactName'}<br>$tabRow->{'orderContact'}</td>
                    |;

                if ($crit_vicha && !$crit_client)
                {
                    $aff .= qq|
                        <td>$tabRow->{'customer.label'}</td>
                        |;
                }
                $aff .= qq|
                    </tr>
                    |;
            }
            $i++;
        }
        $temp->finish();
        print $aff;
        print "</table>\n<br>";
        # R�cup�ration des param�tres pour l'impression
        my $url        = undef;
        my $customerId = $q->param('client');
        my $offset     = $nb;

        if ($customerId)
        {
            print $G_LST_IMP,"&nbsp;";

            if ($q->param('choix') eq "$traduction{$G_PAYS}{1}")
            {
                $url = &LOC::Request::createRequestUri('rent:rentContract:pdfList',
                                                       {'customerId' => $customerId,
                                                        'contractsIds' => \@tabContractsIds,
                                                        'nbTotal'    => $nbTotal,
                                                        'limit'      => 20,
                                                        'offset'     => $offset});
            }
            if ($q->param('choix') eq "$traduction{$G_PAYS}{2}")
            {
                $url = &LOC::Request::createRequestUri('rent:rentEstimate:pdfList',
                                                       {'customerId' => $customerId,
                                                        'estimateLinesIds' => \@tabEstimateLinesIds,
                                                        'nbTotal'    => $nbTotal,
                                                        'limit'      => 20,
                                                        'offset'     => $offset});
            }

            print '<a href="#" onclick="printList(\'' . $url . '\'); return false;"><img src="' . $URL_IMG . '/imprimer.gif" alt="' . $mess[871] . '" id="btn-print" /></a>';
            print '<img src="' . $URL_IMG . '/../../img/loading.gif" alt="" id="img-loading" />';
            print '<p id="msg-result">' . $mess[874] . '</p>';
        }
    }
}
print qq|<center><a href="$url">$traduction{$G_PAYS}{42}</a></center>|;
print $q->end_form();
if ($temp)
{
    $temp->finish;
}
$BD->disconnect;

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

# Pied de la page HTML
my $paramType = $q->param("type");
my $paramMenu = $q->param("menu");
&Pied($delta_time, "$G_PENOM;$G_AGENCE;" . &nom_fichier() . ";" . $paramType . ";" . $paramMenu);

