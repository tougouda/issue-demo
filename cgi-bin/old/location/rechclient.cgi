#!/usr/bin/perl

use lib '../lib';

use DBI;
use CGI;
use accesbd;
use varglob;
use autentif;
use tracetemps;

# Initialisation du chrono
my $timer = &init_chrono();


 %LANGUE = ( 
 
	   "FR" => {
  	
  	
  		  "Tarifs des locations"	          	        => "Tarifs des locations",	
          	  "Recherche du client par le nom"	                => "Recherche du client par le nom",
          	  "Liste des clients"					=> "Liste des clients",
       		  "Clients cadres"					=> "Clients cadres",	
       		  "Clients non cadres"					=> "Clients non cadres",
       		  "Nouveau client"					=> "Nouveau client",
       		  "Client"						=> "Client",
       		  "Recherche machine par site      ( * optionnel  )"	=> "Recherche machine par site      ( * optionnel  )", 
       		  "Chantier et machine"					=> "Chantier et machine",
       		  "Pas de chantiers dans cette ville"			=> "Pas de chantiers dans cette ville",
       		  "Famille machine"					=> "Famille machine",
       		  "Divers"						=> "Divers",
       		  "Diesel"						=> "Diesel",
       		  "Electrique"						=> "Electrique",
       		  "Hauteur souhait�e"					=> "Hauteur souhait�e",
       		  "Etape suivante"					=> "Etape suivante",
       		  "Villes chantiers du mois en cours"			=> "Villes chantiers du mois en cours",
       		  "MODELE"						=> "MODELE",
       		  "El�vation travail"					=> "El�vation travail",
       		  "D�port"						=> "D�port",
       		  "Stock r�servable"					=> "Stock r�servable",
       		  "Stock disponible"					=> "Stock disponible",
       		  "Dernier chantier"					=> "Dernier chantier",
       		  "Transport HT"					=> "Transport HT",
       		  "Prix jour"						=> "Prix jour",
       		  "Prix mois"						=> "Prix mois",
       		  "Forfait"						=> "Forfait",
       		  "Devis"						=> "Devis",
       		  "Contrat"						=> "Contrat",
       		  "Le"							=> "Le",
       		  "Chantier"						=> "Chantier",
       		  "avec"						=> "avec",
       		  "N�parc"						=> "N�parc",
       		  "Retour choix du client"                              => "Retour choix du client",
       		  "Retour page pr�cedente"                              => "Retour page pr�cedente",
       		  "Aucune s�l�ction de machine"                         => "Aucune s�l�ction de machine",
       		  "Haut"                     			        => "Haut",
       		  "Jour"                     			        => "Jour",
       		  "Semaine"                     			=> "Semaine",
       		  "Mois-min"                     			=> "Mois-min",
       		  "Mois"                     				=> "Mois"
       		 },
       		 
	   "MA" => {
  	
  	
  		  "Tarifs des locations"	          	        => "Tarifs des locations",	
          	  "Recherche du client par le nom"	                => "Recherche du client par le nom",
          	  "Liste des clients"					=> "Liste des clients",
       		  "Clients cadres"					=> "Clients cadres",	
       		  "Clients non cadres"					=> "Clients non cadres",
       		  "Nouveau client"					=> "Nouveau client",
       		  "Client"						=> "Client",
       		  "Recherche machine par site      ( * optionnel  )"	=> "Recherche machine par site      ( * optionnel  )", 
       		  "Chantier et machine"					=> "Chantier et machine",
       		  "Pas de chantiers dans cette ville"			=> "Pas de chantiers dans cette ville",
       		  "Famille machine"					=> "Famille machine",
       		  "Divers"						=> "Divers",
       		  "Diesel"						=> "Diesel",
       		  "Electrique"						=> "Electrique",
       		  "Hauteur souhait�e"					=> "Hauteur souhait�e",
       		  "Etape suivante"					=> "Etape suivante",
       		  "Villes chantiers du mois en cours"			=> "Villes chantiers du mois en cours",
       		  "MODELE"						=> "MODELE",
       		  "El�vation travail"					=> "El�vation travail",
       		  "D�port"						=> "D�port",
       		  "Stock r�servable"					=> "Stock r�servable",
       		  "Stock disponible"					=> "Stock disponible",
       		  "Dernier chantier"					=> "Dernier chantier",
       		  "Transport HT"					=> "Transport HT",
       		  "Prix jour"						=> "Prix jour",
       		  "Prix mois"						=> "Prix mois",
       		  "Forfait"						=> "Forfait",
       		  "Devis"						=> "Devis",
       		  "Contrat"						=> "Contrat",
       		  "Le"							=> "Le",
       		  "Chantier"						=> "Chantier",
       		  "avec"						=> "avec",
       		  "N�parc"						=> "N�parc",
       		  "Retour choix du client"                              => "Retour choix du client",
       		  "Retour page pr�cedente"                              => "Retour page pr�cedente",
       		  "Aucune s�l�ction de machine"                         => "Aucune s�l�ction de machine",
       		  "Haut"                     			        => "Haut",
       		  "Jour"                     			        => "Jour",
       		  "Semaine"                     			=> "Semaine",
       		  "Mois-min"                     			=> "Mois-min",
       		  "Mois"                     				=> "Mois"
       		 },
       		 
	   "LU" => {
  	
  	
  		  "Tarifs des locations"	          	        => "Tarifs des locations",	
          	  "Recherche du client par le nom"	                => "Recherche du client par le nom",
          	  "Liste des clients"					=> "Liste des clients",
       		  "Clients cadres"					=> "Clients cadres",	
       		  "Clients non cadres"					=> "Clients non cadres",
       		  "Nouveau client"					=> "Nouveau client",
       		  "Client"						=> "Client",
       		  "Recherche machine par site      ( * optionnel  )"	=> "Recherche machine par site      ( * optionnel  )", 
       		  "Chantier et machine"					=> "Chantier et machine",
       		  "Pas de chantiers dans cette ville"			=> "Pas de chantiers dans cette ville",
       		  "Famille machine"					=> "Famille machine",
       		  "Divers"						=> "Divers",
       		  "Diesel"						=> "Diesel",
       		  "Electrique"						=> "Electrique",
       		  "Hauteur souhait�e"					=> "Hauteur souhait�e",
       		  "Etape suivante"					=> "Etape suivante",
       		  "Villes chantiers du mois en cours"			=> "Villes chantiers du mois en cours",
       		  "MODELE"						=> "MODELE",
       		  "El�vation travail"					=> "El�vation travail",
       		  "D�port"						=> "D�port",
       		  "Stock r�servable"					=> "Stock r�servable",
       		  "Stock disponible"					=> "Stock disponible",
       		  "Dernier chantier"					=> "Dernier chantier",
       		  "Transport HT"					=> "Transport HT",
       		  "Prix jour"						=> "Prix jour",
       		  "Prix mois"						=> "Prix mois",
       		  "Forfait"						=> "Forfait",
       		  "Devis"						=> "Devis",
       		  "Contrat"						=> "Contrat",
       		  "Le"							=> "Le",
       		  "Chantier"						=> "Chantier",
       		  "avec"						=> "avec",
       		  "N�parc"						=> "N�parc",
       		  "Retour choix du client"                              => "Retour choix du client",
       		  "Retour page pr�cedente"                              => "Retour page pr�cedente",
       		  "Aucune s�l�ction de machine"                         => "Aucune s�l�ction de machine",
       		  "Haut"                     			        => "Haut",
       		  "Jour"                     			        => "Jour",
       		  "Semaine"                     			=> "Semaine",
       		  "Mois-min"                     			=> "Mois-min",
       		  "Mois"                     				=> "Mois"
       		 },
       		 
       		 
       	
       	  "ES" => {
  	
  	
  		  "Tarifs des locations"	          	        => "Las tarifas del alquilan ",		
          	  "Recherche du client par le nom"	                => "Investigaci&oacute;n de b�squeda para el cliente por el nombre",
          	  "Liste des clients"					=> "Lista de los clientes",
       		  "Clients cadres"					=> "Ejecutivos de clientes ",	
       		  "Clients non cadres"					=> "Clientes no ejecutivos ",
       		  "Nouveau client"					=> "Cliente nuevo",
       		  "Client"						=> "Cliente",
       		  "Recherche machine par site      ( * optionnel  )"	=> "Busque la m�quina por el sitio     ( * Opcional  )", 
       		  "Chantier et machine"					=> "Obra de la construcci�n y m�quina",
       		  "Pas de chantiers dans cette ville"			=> "Ninguna obra de la construcci�n en esta ciudad",
       		  "Famille machine"					=> "Familia de m�quina",
       		  "Divers"						=> "Mixto",
       		  "Diesel"						=> "Gasoil",
       		  "Electrique"						=> "El�ctrico",
       		  "Hauteur souhait�e"					=> "Altura Deseada",
       		  "Etape suivante"					=> "Etapa siguiente",
       		  "Villes chantiers du mois en cours"			=> "Las obras de la construcci�n de ciudades del mes corriente",
       		  "MODELE"						=> "MODELO",
       		  "El�vation travail"					=> "Trabajo en la subida ",
       		  "D�port"						=> "Deportaci�n",
       		  "Stock r�servable"					=> "reserva reservable",
       		  "Stock disponible"					=> "reserva disponible",
       		  "Dernier chantier"					=> "Obra de la construcci�n pasada",
       		  "Transport HT"					=> "Transporte HT",
       		  "Prix jour"						=> "Precio / d�a",
       		  "Prix mois"						=> "Precio / mes",
       		  "Forfait"						=> "Forfait",
       		  "Devis"						=> "Estimaci�n",
       		  "Contrat"						=> "Contracto",
       		  "Le"							=> "El",
       		  "Chantier"						=> "Obra",
       		  "avec"						=> "con",
       		  "N�parc"						=> "N�parque",
       		  "Retour choix du client"                              => "vuelta elecci�n del cliente.",
       		  "Retour page pr�cedente"                              => "Vuelta p�gina pr�ced.",
       		  "Aucune s�l�ction de machine"                         => "Ning�n s�l�ction de m�quina.",
       		  "Haut"                     			        => "Altura",
       		  "Jour"                     			        => "Dia",
       		  "Semaine"                     			=> "Semana",
       		  "Mois-min"                     			=> "Mes-min",
       		  "Mois"                     				=> "Mes"
       		 }, 
       		 
       	 "GB" => {
  	
          	  "Recherche du client par le nom"	                => "Search customer by name",
          	  "Liste des clients"					=> "List of customers",
       		  "Clients cadres"					=> "Customer with special s contracts ",	
       		  "Clients non cadres"					=> "Normal s customers ",
       		  "Nouveau client"					=> "New customer",
       		  "Client"						=> "Customer",
       		  "Recherche machine par site      ( * optionnel  )"	=> "Search machine by place     ( * Optionnal  )", 
       		  "Chantier et machine"					=> "Build s places and machines",
       		  "Pas de chantiers dans cette ville"			=> "No buisness in this town",
       		  "Famille machine"					=> "Machine family",
       		  "Divers"						=> "Mixt",
       		  "Diesel"						=> "Gasoil",
       		  "Electrique"						=> "Electric",
       		  "Hauteur souhait�e"					=> "Height desired",
       		  "Etape suivante"					=> "Next step",
       		  "Villes chantiers du mois en cours"			=> "Monthly Towns build place",
       		  "MODELE"						=> "Models",
       		  "El�vation travail"					=> "Height work ",
       		  "D�port"						=> "deport",
       		  "Stock r�servable"					=> "reservable stock",
       		  "Stock disponible"					=> "Free stock",
       		  "Dernier chantier"					=> "Last work place",
       		  "Transport HT"					=> "Transport HT",
       		  "Prix jour"						=> "Price / day",
       		  "Prix mois"						=> "Price / month",
       		  "Forfait"						=> "Forfait",
       		  "Devis"						=> "Estimation",
       		  "Contrat"						=> "Contract",
       		  "Le"							=> "The",
       		  "Chantier"						=> "work place",
       		  "avec"						=> "with",
       		  "N�parc"						=> "N�park",
       		  "Retour choix du client"                              => "return choice of the customer.",
       		  "Retour page pr�cedente"                              => "Return pr�cedente page.",
       		  "Aucune s�l�ction de machine"                         => "No s�l�ction of machine.",
       		  "Haut"                     			        => "Height",
       		  "Jour"                     			        => "Day",
       		  "Semaine"                     			=> "Week",
       		  "Mois-min"                     			=> "Month-min",
       		  "Mois"                     				=> "Month"
       		 },        	         		 	 
       		
       	   "IT" => {
  	
          	  "Recherche du client par le nom"	                => "Ricerca del nome di un cliente",
          	  "Liste des clients"					=> "Lista dei clienti",
       		  "Clients cadres"					=> "Clienti importanti",	
       		  "Clients non cadres"					=> "Clienti non importanti",
       		  "Nouveau client"					=> "nuovo cliente",
       		  "Client"						=> "Cliente",
       		  "Recherche machine par site      ( * optionnel  )"	=> "Ricerca macchina per cantiere ( * facultativo )", 
       		  "Chantier et machine"					=> "Cantiere e macchina",
       		  "Pas de chantiers dans cette ville"			=> "Non ci sono cantieri in questa citt�",
       		  "Famille machine"					=> "Famiglia macchina",
       		  "Divers"						=> "Vari",
       		  "Diesel"						=> "diesel",
       		  "Electrique"						=> "elettrico",
       		  "Hauteur souhait�e"					=> "altezza desiderata",
       		  "Etape suivante"					=> "Fase seguente",
       		  "Villes chantiers du mois en cours"			=> "citt� cantieri del mese in corso",
       		  "MODELE"						=> "Modello",
       		  "El�vation travail"					=> "Elevazione lavoro",
       		  "D�port"						=> "Sfalsamento",
       		  "Stock r�servable"					=> "Stoc riservabile",
       		  "Stock disponible"					=> "Stoc disponibile",
       		  "Dernier chantier"					=> "Ultimo cantiere",
       		  "Transport HT"					=> "Trasporto iva esclusa",
       		  "Prix jour"						=> "Prezzo giorno",
       		  "Prix mois"						=> "Prezzo mese",
       		  "Forfait"						=> "Forfait",
       		  "Devis"						=> "Preventivo",
       		  "Contrat"						=> "Contratto",
       		  "Le"							=> "il",
       		  "Chantier"						=> "cantiere",
       		  "avec"						=> "con",
       		  "N�parc"						=> "N� parco",
       		  "Retour choix du client"                              => "Ritorno scelta del cliente.",
       		  "Retour page pr�cedente"                              => "Ritorno pagina pr�cedente.",
       		  "Aucune s�l�ction de machine"                         => "Nessuna s�l�ction di macchina.",  
       		  "Haut"                     			        => "Altezza",
       		  "Jour"                     			        => "giorno",
       		  "Semaine"                     			=> "Settimana",
       		  "Mois-min"                     			=> "mese-min",
       		  "Mois"                     				=> "Mese"
       		 },        	         		 	 		 
       		
       	"PT" => {
  	
          	  "Recherche du client par le nom"	                => "Investiga��o cliente pelo nome.",
          	  "Liste des clients"					=> "Lista dos clientes.",
       		  "Clients cadres"					=> "Clientes quadros.",	
       		  "Clients non cadres"					=> "Clientes n�o quadros.",
       		  "Nouveau client"					=> "Novo cliente.",
       		  "Client"						=> "Cliente",
       		  "Recherche machine par site      ( * optionnel  )"	=> "Investiga��o m�quina por s�tio. (* n�o obrigat�rio.)", 
       		  "Chantier et machine"					=> "Estaleiro e m�quina.",
       		  "Pas de chantiers dans cette ville"			=> "N�o estaleiros nesta cidade.",
       		  "Famille machine"					=> "Fam�lia m�quina.",
       		  "Divers"						=> "Diversos",
       		  "Diesel"						=> "Diesel",
       		  "Electrique"						=> "El�ctrico",
       		  "Hauteur souhait�e"					=> "Altura desejada.",
       		  "Etape suivante"					=> "Etapa seguinte.",
       		  "Villes chantiers du mois en cours"			=> "Cidades estaleiros do m�s em curso.",
       		  "MODELE"						=> "MODELO",
       		  "El�vation travail"					=> "Eleva��o trabalho.",
       		  "D�port"						=> "Recusa",
       		  "Stock r�servable"					=> "Exist�ncias r�servable.",
       		  "Stock disponible"					=> "Exist�ncias dispon�veis.",
       		  "Dernier chantier"					=> "�ltimo estaleiro.",
       		  "Transport HT"					=> "Transporte GH.",
       		  "Prix jour"						=> "Pr�mio dia",
       		  "Prix mois"						=> "Pr�mio m�s",
       		  "Forfait"						=> "Pre�o fixo",
       		  "Devis"						=> "Or�amento",
       		  "Contrat"						=> "Contrato",
       		  "Le"							=> "Le",
       		  "Chantier"						=> "Estaleiro",
       		  "avec"						=> "com",
       		  "N�parc"						=> "N� parque",
       		  "Retour choix du client"                              => "Regresso escolha do cliente.",
       		  "Retour page pr�cedente"                              => "Regresso p�gina pr�cedente.",
       		  "Aucune s�l�ction de machine"                         => "Nenhum s�l�ction de m�quina.",  
       		  "Haut"                     			        => "Altura",
       		  "Jour"                     			        => "dia",
       		  "Semaine"                     			=> "Semana",
       		  "Mois-min"                     			=> "m�s-min",
       		  "Mois"                     				=> "m�s"
       		 }        	         		 	 		 
       		 
       		 
       		        	         		 	               
          );

$BD = &Environnement("LOCATION");

$q=new CGI;
$q->autoEscape(undef);


$BORDER  = 1;
$BORDER2 = 1;
$color = '#FFFFFF';

  
my $titre = qq|$LANGUE{$G_PAYS}{'Tarifs des locations'}|;
&ente($titre, $titre, $G_PENOM, $G_PEPRENOM);
  
     
   $Type = $q->param('type');
   $art=$q->param("entree");
   $art2=$q->param("entree2");
   
   $famille=$q->param("REFER2");	     
   $client=$q->param("REFER");	
   $hauteur=$q->param("Hauteur");
   $d2r2=$q->param("RESULT");
   $selected=$q->param("Ville");
     
   $d3r3=$q->param("RESULT2");
   $famille=$q->param("FF");	
   $energie=$q->param("EE");	
   $budg=$q->param("Budget");
   $durr=$q->param("Duree");
   	
   &get_date();

if ($q->param("bouton1.x")) {
 		&Suite;exit;
 }	

if ($Type eq "PREMIS")       { &AffPageStandard ;exit;}      	  
if ($Type eq "MACH")       { &machines ;exit;}    
if ($Type eq "CHANT")       { &rechchantier ;exit;}    
if ($Type eq "SUITE")       { &Suite ;exit;}    



&AffPageStandard();

#-------------- Sub saisie du client -----------------------------------

sub AffPageStandard { #1

print $q->start_form(-name=>"FORM1");  
	
print "<center>";
print "<tr bgcolor='#F5F3F8'><td><FONT color='#4489FF' size=3 face=' Arial, Helvetica, sans-serif'><b>$LANGUE{$G_PAYS}{'Recherche du client par le nom'} </b></FONT></TD><td>" ,  $q->textfield(-name=>"entree", -class=>'select2'), "</td></tr>\n";
print "</center>";
print "<BR>";

if ($q->param("entree")) {#1.1

   	$po=0; 
	my $goel=qq{select CLAUTO, CLSTE from CLIENT WHERE CLSTE like "$art%"};
  	
	my $aviso=$BD->prepare("$goel");
	$aviso->execute();
	
	$nb=$aviso->rows();
				
		 if ($nb>0) {#32
				
		  push @space,qq|$LANGUE{$G_PAYS}{'Liste des clients'}|;			 	 
    		  push @space2,qq|$LANGUE{$G_PAYS}{'Liste des clients'}|;
    			 while (@tabl = $aviso->fetchrow_array()) {#44
     	 			push @space,qq|$tabl[0]|;
     	 			$labels{$tabl[0]}=$tabl[1];
    		     	}#44	
    		 
    		 $verifcadre=@space;
    		 
    		  		     	
     	     	 print"<BR>";    	     	     	     	     		     	
   		 print "<center>";   
 		
 		#if ($verifcadre >1) {
 		 print "<tr><td><b><FONT color='#4489FF' size=3 face=' Arial, Helvetica, sans-serif'>$LANGUE{$G_PAYS}{'Clients cadres'} </FONT></b></td><td>",  $q->popup_menu(-name=>"REFER", -value=>\@space, -labels=>\%labels, -class=>'select2', -onchange=>"window.open('" . $ENV{'TFL_URL'} . "/c/grille_consult.php?clauto='+this.value)"), "</td></tr>\n";
	 	#}
	 	
		 #print "<tr><td><b><FONT color='#4489FF' size=3 face=' Arial, Helvetica, sans-serif'>$LANGUE{$G_PAYS}{'Clients non cadres'} </FONT></b></td><td>",  $q->popup_menu(-name=>"REFER", -value=>\@space2, -labels=>\%labels, -class=>'select2',-onchange=>"document.location=this.value"), "</td></tr>\n";	 
		 
		 print "</center>";  
		 print"<BR>";
		
	} else {
		
		print "<center>";
		print "<FONT color='#E53200' size=4>$LANGUE{$G_PAYS}{'Nouveau client'} !</FONT>";
		print "</center>";
		
		$client=$art;
		print $q->hidden('REFER',$client);	
			
				
		}#32
	 $aviso->finish();
	
	
}#1.1 		
						
print $q->end_form();
}#1
	
#------------------------------------------------------------------------------------------------------------------------------------------		

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
sub Suite {#2

print $q->start_form(-name=>"FORM3");  

	 #--------------------------------------------------------------------
	# my $albator=qq{select CLSTE from CLIENT WHERE CLSTE = "$client"};
  	 #my $bioman=$BD->prepare("$albator");
	# $bioman->execute();
	# $nb=$bioman->rows();
	# $bioman->finish();
	# print $nb;
 	 #------------------------------------------------------------------------
print "<center>";
print qq|<td><a href="#" onClick="return rs('infoclient','$HTTP_PORTAIL/cgi-bin/old/location/infoclient.cgi?value=$client',750,460)"  alt="d�tails bon de commande"><FONT color='#5B00FF' size=4 face=' Arial, Helvetica, sans-serif'>$client</FONT></a></td>|;
print "</center>";
print "<BR>";

print qq|<TABLE border=1 width=50% bordercolor="" cellpadding=0 cellspacing=2 class=select4>|;
print qq|<tr bgColor='#C6CDC5'><TH>Zone</TH><TH>Prix</TH><TH>temps</TH></tr>|;
print qq|<TR><TD>Zone1</TD><TD>91 � aller et 91 � retour</TD><TD>1 heure de route</TD></tr>|;
print qq|<TR><TD>Zone2</TD><TD>152 � aller et 152 � retour</TD><TD>2 heure de route</TD></tr>|;
print qq|<TR><TD>Zone3</TD><TD>198 � aller et 198 � retour</TD><TD>3 heure de route</TD></tr>|;
print qq|<TR><TD>Zone4</TD><TD>244 � aller et 244 � retour</TD><TD>4 heure de route</TD></tr>|;
print "<FONT size=2>Forfait incluant le transport et l'assurance</FONT>";
print "<BR><FONT size=2>Prix au km 1,52 �</FONT>";
print qq|</TABLE>|;

			
			#if ($_ =~ /QTE_(\d+)_(\d+)_(\d+)/) {#3
$ancien="zoulou";
foreach ($q->param) {#7
 			
 			#if ($_ =~ /FAMILLE_(\w+\-\w+)/ || $_ =~ /FAMILLE_(\w+\s\w+)/ || $_ =~ /FAMILLE_(\w+)/  ) {#4
			
			
			
			#push @fammach,$1;
			
								
			#}#4
			
if ($_ =~ /MOTOR_(\w+)_(\w+\-\w+)/  || $_ =~ /MOTOR_(\w+)_(\w+)/ || $_ =~ /MOTOR_(\w+\-\w+)_(\w+\-\w+)/ || $_ =~ /MOTOR_(\w+\-\w+)_(\w+)/ || $_ =~ /MOTOR_(\w+\s\w+)_(\w+\-\w+)/ || $_ =~ /MOTOR_(\w+\s\w+)_(\w+)/) {
					
		if ($1 ne $ancien) {	
					
		push @fammach,$1;
		$ancien=$1;
		}
				
		push @$1,$2;
										
		}
											
}#7

$comptfam=0;
$comptfam=@fammach;
	if ($comptfam==0){
		print "<center>";
		print qq|<FONT color='#E53200' size=4>$LANGUE{$G_PAYS}{'Aucune s�l�ction de machine'}</FONT>|;
		print "</center>";
		}
  			
			  my $rech=qq{select CLAUTO from CLIENT WHERE CLSTE = "$client" };
  			  my $tabat=$BD->prepare("$rech");
			  $tabat->execute();	
			  $nono=$tabat->rows(); 
			  
			  ($clauto) = $tabat->fetchrow_array();
				
				$tabat->finish();	
					
				    
    foreach  $mod (@fammach) {
    	
               	 
               	 #print "famille = $mod\n";         
  	    
  	    foreach $energ (@$mod) {
  	    	
  	         #print "energie = $energ\n";
  	           
 print qq|<TABLE border=0 width=100% bordercolor="" cellpadding=0 cellspacing=2 align=center class=select3>|; #align=left permet d'avoir les autres tableaux � droite du premier 
   	print "<BR>";
	print "<center>";
	print "<FONT size=4 face=' Arial, Helvetica, sans-serif'>$mod $energ </FONT>";
	print "</center>";
	print "<BR>";
  	
 print "<tr bgColor='#C6CDC5'><th>$LANGUE{$G_PAYS}{'MODELE'}</th><th>$LANGUE{$G_PAYS}{'Haut'}</th><th>D�p.</th><th>St. r�serv.</th><th>St. disp.</th><th>$LANGUE{$G_PAYS}{'Dernier chantier'}</th><th>$LANGUE{$G_PAYS}{'Transport HT'}</th><th>ForfaitZ1</th><th>ForfaitZ2</th><th>$LANGUE{$G_PAYS}{'Jour'}</th><th>$LANGUE{$G_PAYS}{'Semaine'}</th><th>$LANGUE{$G_PAYS}{'Mois'}</th><th>$LANGUE{$G_PAYS}{'Mois-min'}</th><th>$LANGUE{$G_PAYS}{'Devis'}</th><th>$LANGUE{$G_PAYS}{'Contrat'}</th></tr>\n";   	
  	
 $mod2=0;
 $colori2="#D6D6D6"; 	
  	
	
my $goel4=qq{select MODELEMACHINE.MOMAAUTO,MODELEMACHINE.MOMADESIGNATION,FAMILLEMACHINE.FAMAHAUTEUR,MODELEMACHINE.MOMADEPORT,STOCKMODELE.STMORESERVABLE,STOCKMODELE.STMODIS,FAMILLEMACHINE.FAMFORFZ1,FAMILLEMACHINE.FAMFORFZ2,FAMILLEMACHINE.FAMPXJOUR,FAMILLEMACHINE.FAMPXSEM,FAMILLEMACHINE.FAMMOISIND,FAMILLEMACHINE.FAMMOISMIN,STOCKMODELE.STMORESERVE from FAMILLEMACHINE left join AUTH.`MODELEMACHINE` on MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO left join AUTH.`STOCKMODELE` on STOCKMODELE.MOMAAUTO=MODELEMACHINE.MOMAAUTO WHERE FAMILLEMACHINE.FAMAMODELE = "$mod" and FAMILLEMACHINE.FAMAENERGIE ="$energ" and STOCKMODELE.AGAUTO="$G_AGENCE" order by FAMAHAUTEUR};
  	
		my $aviso4=$BD->prepare("$goel4");
		$aviso4->execute();	
		
	 		 
		while (@tabl3 = $aviso4->fetchrow_array()) {#2 tous les momaauto d'1 famille et d'1 motorisation
		
		#-------------partie qui g�re les couleurs altern�es----------------
				if ($mod2==1) {
  				$colori2="#D6D6D6";
  				} else {
  				$colori2="";
  				}
				#---------------------------------------------------------
			 				 						  
			  if ($nono>0) { # si client connu

			  
$rech2=qq{select MONTANT.MOTRANSPORTDFR,MONTANT.MOFORFAITFR,MONTANT.MOPXJOURFR,MONTANT.MOTARIFMSFR,CHANTIER.CHLIBELLE from CONTRAT left join MONTANT on CONTRAT.MOAUTO=MONTANT.MOAUTO left join CHANTIER on CONTRAT.CHAUTO=CHANTIER.CHAUTO WHERE  CONTRAT.CLAUTO = "$clauto" and CONTRAT.MOMAAUTO = "$tabl3[0]"  and AGAUTO = "$G_AGENCE" order by CONTRATDR desc limit 1};			  
#and CONTRAT.CONTRATOK="-1" and (CONTRAT.ETCODE="CON02" or CONTRAT.ETCODE="CON03")
			  
		$tabat3=$BD->prepare("$rech2");
  		$tabat3->execute();	
  	
  		($transport,$forfaitz1,$pxjour,$pxmoisind,$libchantier) = $tabat3->fetchrow_array();
		 			  			 			  
				$transport=sprintf("%.0f",$transport); 
			  	$forfaitz1=sprintf("%.0f",$forfaitz1); 
			  	$pxjour=sprintf("%.0f",$pxjour); 
			  	$pxmoisind=sprintf("%.0f",$pxmoisind); 
			    			
			    			
			    			$iforfaitz1=sprintf("%.0f",$tabl3[6]); 
					$forfaitz2=sprintf("%.0f",$tabl3[7]); 
						$ipxjour=sprintf("%.0f",$tabl3[8]); 
					$pxsem=sprintf("%.0f",$tabl3[9]*5); 
						$ipxmoisind=sprintf("%.0f",$tabl3[10]*20); 	  
					$pxmoismin=sprintf("%.0f",$tabl3[11]*20); 	
			    
			    			if ($transport==0) {
			    				$transport="";
			    				}
			    
			 			   if ($forfaitz1 ==0) {
			    				$forfaitz1=$iforfaitz1;
			    				} else {
			    				$forfaitz1=$forfaitz1."/".$iforfaitz1;
			   				 }
			    
			 					  if ($pxjour ==0) {
			    						$pxjour=$ipxjour;
			    						} else {
			    						 $pxjour=$pxjour."/".$ipxjour;
			   						 }
			       
			   							 if ($pxmoisind ==0) {
			    								$pxmoisind=$ipxmoisind;
			    								} else {
			    								 $pxmoisind=$pxmoisind."/".$ipxmoisind;
			   								}
			   								
			  $tabat3->finish(); 
			  } #  fin si client connu
 			  
 			  if ($nono == 0) { # si client inconnu
					$transport="";
					$forfaitz1=sprintf("%.0f",$tabl3[6]); 
					$forfaitz2=sprintf("%.0f",$tabl3[7]); 
					$pxjour=sprintf("%.0f",$tabl3[8]); 
					$pxsem=sprintf("%.0f",$tabl3[9]*5); 
					$pxmoisind=sprintf("%.0f",$tabl3[10]*20); 	  
					$pxmoismin=sprintf("%.0f",$tabl3[11]*20); 	  		  
			  								
			  } # fin si client inconnu
			  
# si il n'y a pas de mod�le r�servable

if ($tabl3[4]<=$tabl3[12]) {#55	

$tabl3[4]="-";

if ($tabl3[5] == 0) {
	$tabl3[5]="-";
}

if ($hauteur == $tabl3[2]) {
	
print qq|<tr bgColor="#64B4FF" onmouseout="bgColor='#64B4FF'" onmouseover="bgColor='#64B4FF'"><td><a href="#" onClick="return rs('infomodele','$HTTP_PORTAIL/cgi-bin/old/location/renseignement.cgi?cle=MOMA&value=$tabl3[0]',750,460)"  alt="d�tails mod�le">$tabl3[1]</a></td><td align=center>$tabl3[2]</td><td>$tabl3[3]</td><td align=center>$tabl3[4]</td><td align=center>&nbsp;$tabl3[5]</td><td>&nbsp;$libchantier</td><td>&nbsp;$transport</td><td>&nbsp;$forfaitz1</td><td>&nbsp;$forfaitz2</td><td>&nbsp;$pxjour</td><td>&nbsp;$pxsem</td><td>&nbsp;$pxmoisind</td><td>&nbsp;$pxmoismin</td><td align=center>|, $q->image_button(-name=>"bouton",-src=>"$URL_IMG/pressmejaune.gif",-align=>"MIDDLE",-alt=>"Envoi devis",-onclick=>"return fullrs('contrat','$HTTP_PORTAIL/cgi-bin/location/devis.cgi?menu=devis&clientsrech=$client&machrech0=$tabl3[1]&type=devis;',750,460)"), qq|</td><td>&nbsp;</TD></tr>\n|;  	


}else{
	
print qq|<tr bgColor="$colori2" onmouseout="bgColor='$colori2'" onmouseover="bgColor='#64B4FF'"><td><a href="#" onClick="return rs('infomodele','$HTTP_PORTAIL/cgi-bin/old/location/renseignement.cgi?cle=MOMA&value=$tabl3[0]',750,460)"  alt="d�tails mod�le">$tabl3[1]</a></td><td align=center>$tabl3[2]</td><td>$tabl3[3]</td><td align=center>$tabl3[4]</td><td align=center>&nbsp;$tabl3[5]</td><td>&nbsp;$libchantier</td><td>&nbsp;$transport</td><td>&nbsp;$forfaitz1</td><td>&nbsp;$forfaitz2</td><td>&nbsp;$pxjour</td><td>&nbsp;$pxsem</td><td>&nbsp;$pxmoisind</td><td>&nbsp;$pxmoismin</td><td align=center>|, $q->image_button(-name=>"bouton",-src=>"$URL_IMG/pressmejaune.gif",-align=>"MIDDLE",-alt=>"Envoi devis",-onclick=>"return fullrs('contrat','$HTTP_PORTAIL/cgi-bin/location/devis.cgi?menu=devis&clientsrech=$client&machrech0=$tabl3[1]&type=devis;',750,460)"), qq|</td><td>&nbsp;</TD></tr>\n|;  	
}


}#55

# si il y a  des mod�les r�servables

if ($tabl3[4]>$tabl3[12]) {
	
if ($tabl3[5] == 0) {
	$tabl3[5]="-";
}	

if ($hauteur == $tabl3[2]) {

print qq|<tr bgColor="#64B4FF" onmouseout="bgColor='#64B4FF'" onmouseover="bgColor='#64B4FF'"><td><a href="#" onClick="return rs('infomodele','$HTTP_PORTAIL/cgi-bin/old/location/renseignement.cgi?cle=MOMA&value=$tabl3[0]',750,460)"  alt="d�tails mod�le">$tabl3[1]</a></td><td align=center>$tabl3[2]</td><td>$tabl3[3]</td><td align=center>$tabl3[4]</td><td align=center>&nbsp;$tabl3[5]</td><td>&nbsp;$libchantier</td><td>&nbsp;$transport</td><td>&nbsp;$forfaitz1</td><td>&nbsp;$forfaitz2</td><td>&nbsp;$pxjour</td><td>&nbsp;$pxsem</td><td>&nbsp;$pxmoisind</td><td>&nbsp;$pxmoismin</td><td align=center>|, $q->image_button(-name=>"bouton",-src=>"$URL_IMG/pressmejaune.gif",-align=>"MIDDLE",-alt=>"Envoi devis",-onclick=>"return fullrs('contrat','$HTTP_PORTAIL/cgi-bin/location/devis.cgi?menu=devis&clientsrech=$client&machrech0=$tabl3[1]&type=devis;',750,460)"), qq|</td><td align=center>|, $q->image_button(-name=>"bouton",-src=>"$URL_IMG/pressmebleu.gif",-align=>"MIDDLE",-alt=>"Envoi contrat",-onclick=>"return fullrs('contrat','$HTTP_PORTAIL/cgi-bin/location/saisie.cgi?menu=saisie&clientsrech=$client&tpmachinesrech=$tabl3[1]&type=saisie;',750,460)"), qq|</td></tr>\n|;  

}else{
				   			  	
print qq|<tr bgColor="$colori2" onmouseout="bgColor='$colori2'" onmouseover="bgColor='#64B4FF'"><td><a href="#" onClick="return rs('infomodele','$HTTP_PORTAIL/cgi-bin/old/location/renseignement.cgi?cle=MOMA&value=$tabl3[0]',750,460)"  alt="d�tails mod�le">$tabl3[1]</a></td><td align=center>$tabl3[2]</td><td>$tabl3[3]</td><td align=center>$tabl3[4]</td><td align=center>&nbsp;$tabl3[5]</td><td>&nbsp;$libchantier</td><td>&nbsp;$transport</td><td>&nbsp;$forfaitz1</td><td>&nbsp;$forfaitz2</td><td>&nbsp;$pxjour</td><td>&nbsp;$pxsem</td><td>&nbsp;$pxmoisind</td><td>&nbsp;$pxmoismin</td><td align=center>|, $q->image_button(-name=>"bouton",-src=>"$URL_IMG/pressmejaune.gif",-align=>"MIDDLE",-alt=>"Envoi devis",-onclick=>"return fullrs('contrat','$HTTP_PORTAIL/cgi-bin/location/devis.cgi?menu=devis&clientsrech=$client&machrech0=$tabl3[1]&type=devis;',750,460)"), qq|</td><td align=center>|, $q->image_button(-name=>"bouton",-src=>"$URL_IMG/pressmebleu.gif",-align=>"MIDDLE",-alt=>"Envoi contrat",-onclick=>"return fullrs('contrat','$HTTP_PORTAIL/cgi-bin/location/saisie.cgi?menu=saisie&clientsrech=$client&tpmachinesrech=$tabl3[1]&type=saisie;',750,460)"), qq|</td></tr>\n|;  

}
}
					$mod2++;
    			   		if ($mod2==2){
    			   		    $mod2=0;
    			   		}				
	  	
	}#2
	
			
 $aviso4->finish();		
 print "</TABLE>";  

}


}
 
 print "<BR>";
 print "<center>";
 print qq|<br><a href="$HTTP_PORTAIL/cgi-bin/old/location/rechclient.cgi" align=center>$LANGUE{$G_PAYS}{'Nouveau client'}</a><br>|;		
 print qq|<br><a href="javascript:history.go(-1)" align=center>$LANGUE{$G_PAYS}{'Retour page pr�cedente'}</a><br>|;		
 #$HTTP_PORTAIL/cgi-bin/old/location/
 
 print "</center>";
 

 print $q->end_form();

}#2


  $BD->disconnect;
 

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

# Pied de la page HTML
&Pied($delta_time);

  
  

#----------------------------------------------------------------------------------------------------------------  

sub get_date {

    # Definie le tableau des jours de la semaine et mois de l'ann�e.         #
    @days   = ('Dimanche','Lundi','Mardi','Mercredi',
               'Jeudi','Vendredi','Samedi');
    @months = ('01','02','03','04','05','06','07',
	         '08','09','10','11','12');

    # R�cup�re la date courante et formatte l'heure, minutes et secondes.    #
    # Ajoute 1900 � l'ann�e pour avoir les 4 chiffres de l'ann�e.            #
    ($sec,$min,$hour,$mday,$mon,$year,$wday) = (localtime(time))[0,1,2,3,4,5,6];
    $time = sprintf("%02d:%02d:%02d",$hour,$min,$sec);
    $year += 1900;
   	 $mday = "0".$mday if ($mday<10);
    	 $mon  = "0".$mon  if ($mon<10);
    
    	$mon=$mon-1;	 

    # Formate la date.                                                       #
    #$date = "$days[$wday], $mday $months[$mon], $year at $time";
     $date = "$year/$months[$mon]/$mday";
     
     return $date;
    
} 
