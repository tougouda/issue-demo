#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';

use CGI;
use DBI;
use POSIX;
use securite;
use autentif;
#use lectdon;
use accesbd;
use varglob;
use tracetemps;
use LOC::EndUpdates;
use LOC::Util;
use LOC::Request;

# Initialisation du chrono
my ($timer) = &init_chrono();


my $DB = Environnement("LOCATION");

##$logincon=&login;
##&autantif($logincon);
##&lectbase($pays);

my $req = new CGI;
$req->autoEscape(undef);

#my $dbh[1] = DBI->connect('DBI:mysql:LOCATION:sgbd-test.acces-industrie.com', 'agence', 'agencepw');

my $i, $sth, $color, $page=1, $cpt=0, $nbligne=20;
my @data;

my $momaauto = $req->param('momaauto');
my $entete=qq{V�rification des N� de S�rie};

&ente($entete, $entete, $G_PENOM, $G_PEPRENOM);

#print "Content-type: text/html\n\n";
#print <<"entete";
#<HTML><HEAD>
#<TITLE>$entete</TITLE>
#<SCRIPT>
#function test(val, chx) {
#	var valserie = "document.frm.serie"+val;
#	var valbon = "document.frm.bon"+val;
#	if (eval(valserie+".value") == "" && eval(valbon+".checked")) {
#		alert("Champ vide");
#		eval(valbon+".checked=false");
#		eval(valserie).focus();
#	}
#}
#</SCRIPT>
#</HEAD><BODY BGCOLOR="#7F7FFF" COLOR="#000000" TEXT="black" link="blue" VLINK="blue" ALINK="blue">
#<TABLE BGCOLOR="#C6CDC5" BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
#<tr><TD BGCOLOR="#C6CDC5"><P ALIGN="left"><FONT SIZE="5" FACE="arial"><B>$entete</B></FONT></TD>
#<TD WIDTH="54">
#</TD><TD WIDTH="48"><A HREF="javascript:window.close()"><IMG SRC="$URL_IMG/img_4.gif" ALIGN="MIDDLE" BORDER="0" WIDTH="48" HEIGHT="36" ALT="Fermer"></A></TD>
#</TR></TABLE>
#<FORM METHOD=post NAME="frm">
#entete

print <<"entete";
<SCRIPT lANGUAGE="JavaScript">
function test(val, chx) {
	var valserie = "document.frm.serie"+val;
	var valbon = "document.frm.bon"+val;
	if (eval(valserie+".value") == "" && eval(valbon+".checked")) {
		alert("Champ vide");
		eval(valbon+".checked=false");
		eval(valserie).focus();
	}
}
</SCRIPT>
<FORM METHOD=post NAME="frm">
entete


if ($req->param('page')) {
	$page = $req->param('page');
}
if ($req->param('valid.x')) {
	&traitement;
}
if ($req->param('precedent.x')) {
	$page--;
}
if ($req->param('suivant.x')) {
	$page++;
}
$req->delete('page');
print $req->hidden('page', $page);

# Debut
# Initialisation de la liste deroulante
$sth = $DB->prepare(qq|select MOMAAUTO, MOMADESIGNATION, FAMAMODELE, FOLIBELLE, FAMAELEVATION, FAMAENERGIE from AUTH.`MODELEMACHINE` left join FAMILLEMACHINE on MODELEMACHINE.FAMAAUTO = FAMILLEMACHINE.FAMAAUTO left join FOURNISSEUR on MODELEMACHINE.FOAUTO = FOURNISSEUR.FOAUTO ORDER BY MOMADESIGNATION, FAMAMODELE, FOLIBELLE|);
$sth->execute();
my $selected="";
print qq|<CENTER><SELECT NAME="momaauto" ONCHANGE="page.value = 1;submit()">\n|;
print qq|<OPTION VALUE=""$selected>mod�le machine\n|;
while (@data = $sth->fetchrow_array) {
	$selected="";
	if ($data[0] eq $momaauto) {
		$selected=qq| SELECTED|;
	}
	print qq|<OPTION VALUE="$data[0]"$selected>$data[1] - $data[2] - $data[3] - $data[4] - $data[5]\n|;
}
print qq|</select></CENTER><BR><BR>\n|;
# Initialisation de la liste deroulante
# Fin

# Initialisation du tableau sur les modeles de machine
# Debut

my $nbm=qq|select count(MAAUTO) from AUTH.`MODELEMACHINE` left join AUTH.`MACHINE` on MODELEMACHINE.MOMAAUTO = MACHINE.MOMAAUTO where MODELEMACHINE.MOMAAUTO = "$momaauto"|;
$sth = $DB->prepare($nbm);
$sth->execute();

my $rows = $sth->fetchrow_array;
my $nbpage = ceil($rows / $nbligne);
my $start = ($page - 1) * $nbligne;
my $rq=qq|select MACHINE.AGAUTO, DATE_FORMAT(MADATEACHAT, "%d/%m/%Y"), DATE_FORMAT(COMAAPAVE, "%d/%m/%Y"), ETLIBELLE, CONCAT_WS("",MACHINE.MANOPARC_AUX,MACHINE.MANOPARC) AS MANOPARC, MANOSERIE, MACHINE.MAAUTO,MACHINE.MANOPARC AS NO FROM AUTH.`MACHINE` LEFT join CONTROLEMACHINE ON CONTROLEMACHINE.MAAUTO=MACHINE.MAAUTO LEFT join ETATTABLE ON ETATTABLE.ETCODE=MACHINE.ETCODE where MOMAAUTO = "$momaauto" ORDER BY MACHINE.MANOPARC DESC, MADATEACHAT DESC Limit $start, $nbligne|;
$sth = $DB->prepare($rq);
$sth->execute();

if ($rows) {
	print qq|<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="2">\n|;
	print qq|<tr><TD COLSPAN="4" ALIGN="left"><B>$rows machines � v�rifier</B></TD><TD COLSPAN="4" ALIGN="right"><B>page $page/$nbpage</B></TD></tr>\n|;
	print qq|<tr BGCOLOR="#C6CDC5"><TD NOWRAP><FONT FACE="arial"><B>Agence</B></FONT></TD><TD NOWRAP><FONT FACE="arial"><B>Date Achat</B></FONT></TD><TD NOWRAP><FONT FACE="arial"><B>Date Contr�le</B></FONT></TD><TD NOWRAP><FONT FACE="arial"><B>�tat</B></FONT></TD><TD NOWRAP><FONT FACE="arial"><B>N� Parc</B></FONT></TD><TD NOWRAP><FONT FACE="arial"><B>N� S�rie Actuel</B></FONT></TD><TD NOWRAP><FONT FACE="arial"><B>Nouveau N� S�rie</B></FONT></TD><TD NOWRAP WIDTH="20"><FONT FACE="arial"><B>Valider</B></FONT></TD></TR>|;
}

while (@data = $sth->fetchrow_array) {
	if ($req->param("bon$cpt") eq 'on') {
		$color = "'#FFFABC'";
	}
 	else {
		if ($cpt % 2 == 0) {
			$color = "'#D6D6D6'";
		}
		else {
			$color = "''";
		}
	}
	my $pc=$data[4];
	if ($pc =~ /(.*)\s\(/)
		{
		$pc=$1;
		}

	my $machineUrl = &LOC::Request::createRequestUri('machine:machine:view', {'machineId' => $data[6]});

	print qq|<tr BGCOLOR=$color onmouseover="bgColor='#FFCC33'" onmouseout="bgColor=$color">|;
    print qq|<TD NOWRAP><FONT FACE="arial" SIZE="2">&nbsp;$data[0]</FONT></TD><TD NOWRAP><FONT FACE="arial" SIZE="2">&nbsp;$data[1]</FONT></TD>
    <TD NOWRAP><FONT FACE="arial" SIZE="2">&nbsp;$data[2]</FONT></TD>
    <TD NOWRAP><FONT FACE="arial" SIZE="2">&nbsp;$data[3]</FONT>
    </TD><TD NOWRAP><FONT FACE="arial" SIZE="2">&nbsp;<a href="$machineUrl" target="_blank">$data[4]</a></FONT></TD>
    <TD NOWRAP><FONT FACE="arial" SIZE="2">&nbsp;$data[5]</FONT></TD><TD WIDTH="100">|;
	$req->delete("serie$cpt");
	print $req->textfield(-name=>"serie$cpt", -face=>"arial", -value=>"$data[5]", -onblur=>"javascript:test($cpt, 0)");
	print qq|</TD>|;
	$req->delete("bon$cpt");
	print qq|<TD ALIGN="center">|;
	print $req->checkbox(-name=>"bon$cpt", -label=>'', -onclick=>"javascript:test($cpt, 1)");
	print qq|</TD></TR>\n|;
	$req->delete("machine$cpt");
	print $req->hidden("machine$cpt", $data[6]);
	$cpt++;
}

if ($rows) {
	print qq|<tr><TD NOWRAP COLSPAN="8" ALIGN="right"><B>page $page/$nbpage</B></TD></tr>\n|;
	print qq|<tr><TD NOWRAP COLSPAN="8" ALIGN="center">\n|;
	if ($page > 1) {
		print $req->image_button(-name=>'precedent', -alt=>'Page pr�c�dente', -src=>"$URL_IMG/precedent.gif", -border=>0), "&nbsp;";
	}
	print "&nbsp;", $req->image_button(-name=>'valid', -alt=>'Validez vos modifications!!', -src=>"$URL_IMG/valid_off.gif", -border=>0), "&nbsp;";
	if ($page < $nbpage) {
		print "&nbsp;", $req->image_button(-name=>'suivant', -alt=>'Page suivante', -src=>"$URL_IMG/suivant.gif", -border=>0);
	}
	print qq|</TD></tr>\n|;
}

print qq|</TABLE>|;
$req->delete("nbligne");
print $req->hidden('nbligne', $cpt);
print $req->hidden('menu');
print <<fin;
</FORM>
fin
# Fin
# Initialisation du tableau sur les modeles de machine

$sth->finish;
$DB->disconnect;

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

# Pied de la page HTML
my $paramMenu = $req->param("menu");
&Pied($delta_time, $paramMenu);


sub traitement
{
	my $i;
    my $countryId = $G_PAYS;
    my $userId    = $G_PEAUTO;

	for ($i = 0 ; $i < $req->param('nbligne') ; $i++)
	{
		my $newSerie = &LOC::Util::trim(scalar $req->param("serie$i"));
		my $machine  = $req->param("machine$i");

		if ($req->param("bon$i") eq 'on')
		{
		    # R�cup�ration de l'ancien n� de s�rie
		    my $query = '
SELECT
    MANOSERIE
FROM AUTH.MACHINE
WHERE MAAUTO = ' . $machine;

            my $stmt = $DB->prepare($query);
            $stmt->execute();

            my ($oldSerie) = $stmt->fetchrow_array();

            $stmt->finish;

            # Mise � jour du n� de s�rie en BDD
            $DB->do(qq|update AUTH.`MACHINE` set MANOSERIE = "$newSerie" where MAAUTO = "$machine"|);

            if ($oldSerie ne $newSerie)
            {
                # Cr�ation de la trace du changement du n� de s�rie
                my $tabEndUpds = {};
                my $schema = &LOC::Db::getSchemaName($countryId);
                my $tabContent = {
                    'schema'  => $schema,
                    'code'    => LOC::Log::EventType::UPDMACHINESERIALNUMBER,
                    'old'     => $oldSerie,
                    'new'     => $newSerie,
                    'content' => $oldSerie . ' -> ' . $newSerie
                };

                &LOC::EndUpdates::addTrace($tabEndUpds, 'machine', $machine, $tabContent);
                &LOC::EndUpdates::exec($countryId, $tabEndUpds, $userId);
            }
        }
    }
}
