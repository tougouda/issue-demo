#!/usr/bin/perl

# $Id: trace_scripts.cgi,v 1.1.1.1 2006/08/01 07:48:11 clliot Exp $
# $Log: trace_scripts.cgi,v $
# Revision 1.1.1.1  2006/08/01 07:48:11  clliot
# no message
#
# Revision 1.4  2006/02/06 16:52:51  julien
# V. 1.4 : Tarification v2
#
# Revision 1.3.2.1  2005/12/13 15:46:47  julien
# no message
#
# Revision 1.3.4.1  2005/12/13 10:32:00  julien
# Inclusion du r�pertoire lib dans chaque fichier.
#
# Revision 1.3  2005/06/24 14:51:23  julien
# Affichage des traces de scripts.
#

use lib '../lib';

use strict;

use CGI;
use File::Path;
use autentif;
use accesbd;
use varglob;
use message;


# Initialisation CGI
my $cgi=new CGI;
$cgi->autoEscape(undef);

# Connexion
my $DB = &Environnement("LOCATION");

$|=1;

my $titre = qq{Traces scripts};
&ente($titre, $titre, $G_PENOM, $G_PEPRENOM);

my %mois = (
	"Jan" => "01", 
	"Fev" => "02", 
	"Mar" => "03", 
	"Avr" => "04", 
	"Mai" => "05", 
	"Jun" => "06", 
	"Jui" => "07", 
	"Aou" => "08", 
	"Sep" => "09", 
	"Oct" => "10", 
	"Nov" => "11", 
	"Dec" => "12",
	"01"  => "Janvier", 
	"02"  => "F�vrier", 
	"03"  => "Mars", 
	"04"  => "Avril", 
	"05"  => "Mai", 
	"06"  => "Juin", 
	"07"  => "Juillet", 
	"08"  => "Ao�t", 
	"09"  => "Septembre", 
	"10"  => "Octobre", 
	"11"  => "Novembre", 
	"12"  => "D�cembre"
);

my %labels;
my $rep = $PATH_COMMONFILES_LOC_LOGS;
&File::Path::make_path($rep);
opendir(DIR, $rep);
my @files = grep {/^TraceTemps.+\.csv$/} readdir DIR;
@files = reverse sort {&moisannee($a) <=> &moisannee($b)} @files;
unshift (@files, "");
foreach my $files (@files) {
	$labels{"$files"} = &nom($files);
#	$labels{"$files"} = &moisannee($files);
}
closedir DIR;

print qq{<form name="frm" method="post">\n};

print qq{<p>\n};
print qq{<select name="fichier" onchange="submit();">\n};
foreach (@files) {
	my $selected="";
	$selected = " selected"	if ($_ eq $cgi->param("fichier"));
	print qq{<option value="$_"$selected>$labels{$_}</option>\n};
}
print qq{</select>\n};
print qq{</p>\n};

my $entete = "";
my $texte  = "";
my @date   = ("");
my @heure  = ("");
my @login  = ("");
my @agence = ("");
my @tps    = ("");
my @script = ("");
my @entete = ("Date", "Heure", "Login", "Agence", "Temps d'ex�cution", "Script");

my $mois = 5;
my $an = 2005;

# Corps du tableau
	if ($cgi->param("fichier") ne "") {
	open(FICH, "<$rep/".$cgi->param("fichier"))
		or print "Impossible d'ouvrir le fichier $rep/".$cgi->param("fichier")." : $!";
	
	my @color = ("", "#D6D6D6");
	my $index = 0;
	while(<FICH>) {
		my @temp = split(/;/, $_);
		
		if ($cgi->param($entete[0]) ne "" && $temp[0] ne $cgi->param($entete[0])) {next;}
		if ($cgi->param($entete[1]) ne "" 
			&& (substr($temp[1], 0, 2) >= substr($cgi->param($entete[1]), 6, 2) 
				|| substr($temp[1], 0, 2) < substr($cgi->param($entete[1]), 0, 2))) {next;}
		if ($cgi->param($entete[2]) ne "" && $temp[2] ne $cgi->param($entete[2])) {next;}
		if ($cgi->param($entete[3]) ne "" && $temp[3] ne $cgi->param($entete[3])) {next;}
		if ($cgi->param($entete[4]) ne "" 
			&& $cgi->param($entete[4]) eq "< 4 s" 
			&& $temp[4] > 4) {next;}
		if ($cgi->param($entete[4]) ne "" 
			&& $cgi->param($entete[4]) eq "> 4 s" 
			&& $temp[4] < 4) {next;}
		if ($cgi->param($entete[5]) ne "" && $temp[5] ne $cgi->param($entete[5])) {next;}
		
		$texte .= qq{	<tr bgcolor="$color[$index]" onmouseover="bgColor='#FFCC33'" onmouseout="bgColor='$color[$index]'">\n};
		for (my $i=0; $i<6; $i++) {
			$texte .= qq{		<td><font>$temp[$i]</font></td>\n};
		}
		$texte .= qq{	</tr>\n};
		
		$temp[1] = sprintf("%02d", substr($temp[1], 0, 2));
		$temp[1] = $temp[1]."h - ".sprintf("%02d", $temp[1] + 1)."h";
		
		push (@date, $temp[0])		if (!grep($_ eq $temp[0], @date));
		push (@heure, $temp[1])		if (!grep($_ eq $temp[1], @heure));
		push (@login, $temp[2])		if (!grep($_ eq $temp[2], @login));
		push (@agence, $temp[3])	if (!grep($_ eq $temp[3], @agence));
		push (@tps, "< 4 s")		if (!grep($_ eq "< 4 s", @tps) && $temp[4] < 4);
		push (@tps, "> 4 s")		if (!grep($_ eq "> 4 s", @tps) && $temp[4] >= 4);
		push (@script, $temp[5])	if (!grep($_ eq $temp[5], @script));
		
		$index = 1 - $index;
	}
	
	close(FICH);
}

$texte .= qq{</table>\n};


push (@date, $cgi->param($entete[0]))		if (!grep($_ eq $cgi->param($entete[0]), @date));
push (@heure, $cgi->param($entete[1]))		if (!grep($_ eq $cgi->param($entete[1]), @heure));
push (@login, $cgi->param($entete[2]))		if (!grep($_ eq $cgi->param($entete[2]), @login));
push (@agence, $cgi->param($entete[3]))	if (!grep($_ eq $cgi->param($entete[3]), @agence));
push (@tps, $cgi->param($entete[4]))		if (!grep($_ eq $cgi->param($entete[4]), @tps));
push (@script, $cgi->param($entete[5]))	if (!grep($_ eq $cgi->param($entete[5]), @script));

# En-t�te
$entete .= qq{<table width="100%" border="0" cellspacing="2" cellpadding="2">\n};
$entete .= qq{	<tr bgcolor="#C6CDC5">\n};

# Date
$entete .= qq{		<th align="left">\n};
$entete .= qq{			<table width="100%"><tr><th align="left"><font>$entete[0]</font></th>\n};
$entete .= qq{			<td align="right">\n};
$entete .= qq{			<select name="$entete[0]" onchange="submit();">\n};
foreach (sort @date) {
	my $selected = "";
	$selected = " selected"   if ($_ eq $cgi->param($entete[0]));
	$entete .= qq{				<option value="$_"$selected>$_</option>\n};
}
$entete .= qq{			</select>\n};
$entete .= qq{			</td></tr></table>\n};
$entete .= qq{		</th>\n};

# Heure
$entete .= qq{		<th align="left">\n};
$entete .= qq{			<table width="100%"><tr><th align="left"><font>$entete[1]</font></th>\n};
$entete .= qq{			<td align="right">\n};
$entete .= qq{			<select name="$entete[1]" onchange="submit();">\n};
foreach (sort {$a <=> $b} @heure) {
	my $selected = "";
	$selected = " selected"   if ($_ eq $cgi->param($entete[1]));
	$entete .= qq{				<option value="$_"$selected>$_</option>\n};
}
$entete .= qq{			</select>\n};
$entete .= qq{			</td></tr></table>\n};
$entete .= qq{		</th>\n};

# Login
$entete .= qq{		<th align="left">\n};
$entete .= qq{			<table width="100%"><tr><th align="left"><font>$entete[2]</font></th>\n};
$entete .= qq{			<td align="right">\n};
$entete .= qq{			<select name="$entete[2]" onchange="submit();">\n};
foreach (sort @login) {
	my $selected = "";
	$selected = " selected"   if ($_ eq $cgi->param($entete[2]));
	$entete .= qq{				<option value="$_"$selected>$_</option>\n};
}
$entete .= qq{			</select>\n};
$entete .= qq{			</td></tr></table>\n};
$entete .= qq{		</th>\n};

# Agence
$entete .= qq{		<th align="left">\n};
$entete .= qq{			<table width="100%"><tr><th align="left"><font>$entete[3]</font></th>\n};
$entete .= qq{			<td align="right">\n};
$entete .= qq{			<select name="$entete[3]" onchange="submit();">\n};
foreach (sort @agence) {
	my $selected = "";
	$selected = " selected"   if ($_ eq $cgi->param($entete[3]));
	$entete .= qq{				<option value="$_"$selected>$_</option>\n};
}
$entete .= qq{			</select>\n};
$entete .= qq{			</td></tr></table>\n};
$entete .= qq{		</th>\n};

# Temps d'ex�cution
$entete .= qq{		<th align="left">\n};
$entete .= qq{			<table width="100%"><tr><th align="left"><font>$entete[4]</font></th>\n};
$entete .= qq{			<td align="right">\n};
$entete .= qq{			<select name="$entete[4]" onchange="submit();">\n};
foreach (sort @tps) {
	my $selected = "";
	$selected = " selected"   if ($_ eq $cgi->param($entete[4]));
	$entete .= qq{				<option value="$_"$selected>$_</option>\n};
}
$entete .= qq{			</select>\n};
$entete .= qq{			</td></tr></table>\n};
$entete .= qq{		</th>\n};

# Script
$entete .= qq{		<th align="left">\n};
$entete .= qq{			<table width="100%"><tr><th align="left"><font>$entete[5]</font></th>\n};
$entete .= qq{			<td align="right">\n};
$entete .= qq{			<select name="$entete[5]" onchange="submit();">\n};
foreach (sort @script) {
	my $selected = "";
	$selected = " selected"   if ($_ eq $cgi->param($entete[5]));
	$entete .= qq{				<option value="$_"$selected>$_</option>\n};
}
$entete .= qq{			</select>\n};
$entete .= qq{			</td></tr></table>\n};
$entete .= qq{		</th>\n};

$entete .= qq{	</tr>\n};

$DB->disconnect;

# Pied de la page HTML
print "$entete\n$texte\n";
print qq{</form>\n};
print qq{</body>\n</html>\n};



sub moisannee {
	my ($nomfich) = @_;
	$nomfich =~ s/\.csv$//;
	my $annee = substr($nomfich, -4);
	$nomfich =~ s/$annee$//;
	my $mois = substr($nomfich, -3);
	$mois = $mois{$mois};
	
	return $annee.$mois;
}

sub nom {
	my ($nomfich) = @_;
	$nomfich =~ s/\.csv$//;
	my $annee = substr($nomfich, -4);
	$nomfich =~ s/$annee$//;
	my $mois = substr($nomfich, -3);
	$mois = $mois{$mois{$mois}};
	
	return $mois." ".$annee;
}
