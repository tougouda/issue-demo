#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';

use strict;
use CGI;
use autentif;
use calculdate;
use accesbd;
use varglob;
use Date::Calc qw(Days_in_Month Delta_Days Add_Delta_Days);
use message;
use tracetemps;

use JSON::XS;

use LOC::Util;
use LOC::Date;
use LOC::Insurance::Type;
use LOC::Common::Rent;

# Initialisation du chrono
my $timer = &init_chrono();


# Connexion
my $cgi = new CGI;
$cgi->autoEscape(undef);
$|=1;
my $pacode = $cgi->param('paysauto');
my @pys = ();
my @BDC = ();
my $connec = "";
my @tdt = qw(2002 03 23);

if ($pacode ne "")
{
    push(@pys, $pacode);
}
else
{
    @pys = qw(FR ES PT MA);
}
for(my $i=0;$i<@pys;$i++)
{
	if ($pacode eq $pys[$i])
    {
		$connec = $i;
	}
	$BDC[$i] = &Environnement("STATS", $pys[$i]);
}

my $DB = $BDC[0];
&verif_droit();


# R�cup�ration des formats Sage (quantit�, prix et montant)
my $charac;
my $req = '
SELECT IFNULL(ccy_value, cgl_value)
FROM AUTH.p_characteristic
LEFT JOIN AUTH.p_characteristic_global ON chr_id = cgl_chr_id
LEFT JOIN AUTH.p_characteristic_country ON (cgl_chr_id = ccy_chr_id AND ccy_cty_id = "' . $G_PAYS . '")
WHERE chr_code = "FORMATSSAGE"';

my $sth = $DB->prepare($req);
$sth->execute();

while (my @data = $sth->fetchrow_array())
{
    $charac = $data[0];
}

$sth->finish();

my $tabSageFormats = {'quantity' => 2, 'price' => 4, 'amount' => 2};
if ($charac ne '')
{
    $tabSageFormats = JSON::XS->new->decode($charac);
}

my @mess = &message($G_PAYS);
my $coutatotal = 0;
my $date = $cgi->param('date');
my $modl = $cgi->param('modele');
my ($deb, $dbaff, $fin, $fnaff, $dtjour, $affdte, $mimois, $tsdt) = &datemois;
my $day = $mimois;
my %mach_mod;
my %mach_com;
my %mach_com_ag;
my %cout_mod;
my %cout_com;
my %jourmsouv = ();
my %agences   = ();
my $nbjrmoy   = 21;
my $coef      = 0.65;#0.7;
my %comm;
my @ordcpt    = ();
my @ordtot    = ();
my %famille;
my @ordcomm   = ();
my @ordcommtot = ();
my %fmcomm;
my $totmc = 0;
my $caloc = 0;
my $calocc = 0;
my $titre = $mess[3500];
my %htcomm;
my %date_chgt_fact = %{&retour_date($G_PAYS)};
my $nbjours_pondere;

if ($modl ne "") {$titre .= " ".$cgi->param("aff");}

#recuperation taux de change
my $datefinmois = $mimois;
$datefinmois =~ s/\:/\-/g;

my %tauxE;
my $tauxE = 1 / tauxLocalToEuro($G_PAYS, $datefinmois);

for(my $i = 0; $i < @pys; $i++)
{
    $tauxE{$pys[$i]} = 1 / tauxLocalToEuro($pys[$i], $datefinmois);
}

&ente($titre, $titre, $G_PENOM, $G_PEPRENOM);

# Message d'attente
print qq{<div id="attente" style="visibility: hidden; position:absolute; width:400px; height:200px;">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr valign="middle">
		<td align="center">
		<table cellpadding="1" cellspacing="0" border="0">
			<tr>
				<td bgcolor=black>
				<table border="0" width="100%" cellpadding="5" cellspacing="0">
					<tr valign="middle">
						<td bgcolor="#FFFFCC" align="center">
						<font class="PT2">
							Veuillez patienter pendant le chargement des statistiques...<br>
							Merci de ne pas cliquer sur le bouton <i>Actualiser</i> et de ne pas appuyer sur F5.<p>
							<img src="$URL_IMG/sablier.gif" border="0">
						</font>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</div>

<script>
var x = (document.body.clientWidth - 400) / 2;
var y = (document.body.clientHeight - 200) / 2;
attente.style.left = x + "px";
attente.style.top = y + "px";
attente.style.visibility = 'visible';
attente.style.visibility = 'visible';
attente.style.cursor = 'wait';
</script>};

&nb_mach($mimois);
my $ctaux = &cout_taux($mimois);
my @rest = ();
my @resc = ();
# Variables
my $tableau;
my $duree;
my $montant;
my $montotal;
my $total;
my $nbferie;
my $montantass;
my $ass;
my $nbcontcours;
my $nbtarif;
my $jfr = qq{&nbsp;};
#
# Programme Principal
#
if ($modl eq "") {
	$connec = $BDC[$connec] if $pacode;
	$jfr = &jourdate($connec);
}
if ($tsdt) {&hissstatistique;}
else {&statistique;}

my $devise = getDevise();

&entete($devise);
&corps($devise);
print qq{
</table>
</body>
</html>

<script>
attente.style.visibility='hidden';
attente.style.cursor = 'default';
</script>
};

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

# Pied de la page HTML
&Pied($delta_time);


for (my $i=0;$i<@BDC;$i++) 
{
    $BDC[$i]->disconnect;
}

sub getDevise {
    #R�cup�ration de la monnaie
    my $sth = $DB->prepare(qq|SELECT P.PAMONNAIE FROM AUTH.PAYS P WHERE P.PACODE='$G_PAYS';|);
    $sth->execute();
    my @data = $sth->fetchrow_array();
    return $data[0];
}

sub test_date {
	my @param = @_;
	my @dt = split(/\:/, $param[0]);
	my $diff1 = Delta_Days(1900, 1, 1, $tdt[0], $tdt[1], $tdt[2]);
	my $diff2 = Delta_Days(1900, 1, 1, $dt[0], $dt[1], $dt[2]);
	my ($sec, $min, $hr, $jour, $mois, $an, $joursm, $jouran, $ete) = localtime;
	$an += 1900;
	$mois += 1;
	my $diff3 = Delta_Days(1900, 1, 1, $an, $mois, $jour);
	my $dt = $diff2 - $diff3;

	if ($dt>0) {$day = "$an:$mois:$jour";}

	my $diff = $diff2 - $diff1;
	return $diff;
}

sub cout_taux {
	my ($dtt) = @_;
	my ($ant, $mst, $jrt) = split(/:/, $dtt);
	my @tx = ();
	my @dt = ();
	# ordonner de facon croissante de date
	$dt[0] = qq{13/01/2003};
	$tx[0] = 1.92;
	my $t = @tx;
	$t--;
	my $tx = $tx[$t];

	for (my $i=$t;$i>=0;$i--) {
		my ($jrc, $msc, $anc) = split(/\//, $dt[$i]);
		my $diff1 = Delta_Days(1900, 1, 1, $ant, $mst, $jrt);
		my $diff2 = Delta_Days(1900, 1, 1, $anc, $msc, $jrc);
		my $diff = $diff1 - $diff2;

		if ($diff<=0) {$tx = $tx[$i];}
		else {next;}
	}

	return $tx;
}

sub datemois {
	my ($sec, $min, $hr, $jour, $mois, $an, $joursm, $jouran, $ete) = localtime;
	$an += 1900;
	$mois += 1;
	my $debutms = "$an:$mois:01";
	my $finmois = Days_in_Month($an, $mois);
	my $tsdate = 0;
	my $mmois = "$an:$mois:$finmois";

	if ($date) {
		my ($tsan, $tsms, $tsjr) = split(/\:/, $date);

		if (($an ne $tsan) || ($mois ne $tsms) || ($jour ne $tsjr)) {
			$tsdate = 1;
		    	$mmois = "$tsan:$tsms:$tsjr"
	    	}
	}

	my $finms = "$an:$mois:$finmois";
	my $dt = "$an:$mois:$jour";
	my $dtaff = "$jour/$mois/$an";
	my $debaff = "01/$mois/$an";
	my $finaff = "$finmois/$mois/$an";

	return ($debutms, $debaff, $finms, $finaff, $dt, $dtaff, $mmois, $tsdate);
}

sub statjr {
	my @param   = @_;
	my $debutms = $param[0];
	my $finms   = $param[1];
	my $posi    = $param[2];
	my $DB      = $param[3];
	my $pas     = $param[4];
	my $champ   = "FACOMAUTO";
	my $where   = "";
	my $requete = "";

	if ($modl) {
		$champ = "MODELEMACHINE.MOMAAUTO";
		$where = qq| and FACOMAUTO="$modl"|;
	}

	$requete = qq{
	select CONTRATDD, CONTRATDR, CONTRATJOURSPLUS, CONTRATJOURSMOINS, MOPXJOURFR,
	MOTARIFMSFR, CONTRATTYPEASSURANCE, MOTRANSPORTDFR, MOTRANSPORTRFR, CAMONTANTFR,
	$champ, MOFORFAITFR, CONTRATDUREE, SOLOAUTO, LIETCONTRATAUTO,
	ETATFACTURE, CONTRAT.AGAUTO, MAHORSPARC, CONTRATPASSURANCE, ity_flags, CONTRATTYPERECOURS, CONTRATRECOURSVAL
	from CONTRAT
	left join AUTH.`MACHINE` ON CONTRAT.MAAUTO=MACHINE.MAAUTO
	left join MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	left join CARBURANT ON CARBURANT.CARBUAUTO=CONTRAT.CARBUAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	LEFT JOIN p_insurancetype ON ity_id = CONTRATTYPEASSURANCE
    LEFT JOIN p_appealtype ON apl_id = CONTRATTYPERECOURS
	where CONTRATOK in ("-1","-2")
	and CONTRAT.LIETCONTRATAUTO in ("1","2","3","4","7")
	and DATE(CONTRATDD) <= "$finms"
	and DATE(CONTRATDR) >= "$debutms"
	and (ETATFACTURE in ("P", "") or ETATFACTURE is null)
    AND CONTRAT.MAAUTO != 0
    AND CONTRATMACHATTR = -1
	$where
	order by $champ
	};
	my ($andm, $msdm, $jrdm) = split(/:/, $debutms);
	my $dm = Delta_Days(1900, 1, 1, $andm, $msdm, $jrdm);
	my ($anfm, $msfm, $jrfm) = split(/:/, $finms);
	my $fm = Delta_Days(1900, 1, 1, $anfm, $msfm, $jrfm);
	my $jourmois;
	my $jourmsouv;

	if (!$posi) {
		$jourmois = Delta_Days($andm, $msdm, $jrdm, $anfm, $msfm, $jrfm);
		$jourmsouv = $jourmois;
		$jourmsouv += 1;
		my $ouvrwe = &jour_we($jourmsouv, $jrdm, $msdm, $andm);
		$agences{$pas} = ();
		my $temp = $DB->prepare(qq|select AGAUTO from AUTH.`AGENCE` where PACODE="$pas" and AGAUTO not in ("SIE")|);
		$temp->execute();

		while (my @temp = $temp->fetchrow_array()) {push @{$agences{$pas}}, $temp[0];}

		$temp->finish();
		my $uvfer;

		foreach (@{$agences{$pas}}) {
			$uvfer = &jour_fer_ag_cnx($pas, $_, $jrdm, $msdm, $andm, $jourmsouv-1, $DB);
			$jourmsouv{$_} = $jourmsouv - $ouvrwe - $uvfer;
		}

		$jourmsouv = $jourmsouv - $ouvrwe - $uvfer;
	}

	my $sth = $DB->prepare($requete);
	$sth->execute();
	my $nbrow = $sth->rows;
	my $durtot = 0;
    my $nbcontlivr = 0;
	my $nbcontcours = 0;
	my $nbcontarret = 0;
	my $nbdureems = 0;
	my $nbdureeplus = 0;
	my $nbforfait = 0;
	my $nbtarif = 0;
	my $nbtarifms = 0;
	my $nbasscomp = 0;
	my $total = 0.0;
	my $catr = 0;
	my $cacarb = 0;
	my $temp = Days_in_Month($anfm, $msfm);

	while (my @data = $sth->fetchrow_array())
    {
		# FACOMAUTO ou MOMAAUTO
		my $modele = $data[10];
		my $agence = $data[16];
		
		$data[4]  = $data[4] / $tauxE{$pas};
		$data[5]  = $data[5] / $tauxE{$pas};
		$data[7]  = $data[7] / $tauxE{$pas};
		$data[8]  = $data[8] / $tauxE{$pas};
		$data[9]  = $data[9] / $tauxE{$pas};
		$data[11] = $data[11] / $tauxE{$pas};

		# SOLOAUTO
		if ($data[13] != 0) {$modele = 0;}

		my ($jrdd, $msdd, $andd) = &datetojjmsan($data[0]);#contratdd
		my $dd = Delta_Days(1900, 1, 1, $andd, $msdd, $jrdd);
		my ($jrdf, $msdf, $andf) = &datetojjmsan($data[1]);#contratdr
		my $df = Delta_Days(1900, 1, 1, $andf, $msdf, $jrdf);
		my $dureecont = $df - $dd + 1;
		my $boolms = 0;
		my $ta = 0.0;
		my $tr = 0.0;
		my $carbu = 0.0;
		my $ddf = $data[0];#contratdd
		my $dff = $data[1];#contratdr
		my $jrplus = 0.0;
		my $jrmoins = 0.0;
		my $tarif = $data[4];#MOPXJOURFR
		my $tarifm = $data[5];#MOTARIFMSFR
		my $forfait = $data[11];#MOFORFAITFR
		my $boole = 0;
		my $booleta   = 0;
		my $booletr   = 0;
		my $boolemens = 0;
		my $booleforf = 0;
		my $boolecarb = 0;
		my $dff2;

		if ($tarifm != 0) {
			$boole = 1;
			$tarif = $tarifm;
			# Calculer le nb de jours ouvrables
			my $nbjourmois = $temp;
			my $nbnbjrwe   = &jour_we($nbjourmois, 01, $msfm, $anfm);
			my $nbnbferie  = &jour_fer_ag_cnx($pas, $agence, 01, $msfm, $anfm, $nbjourmois-1, $DB);
			my $joursupmensuel = 0;

			if ($df > $fm) {$joursupmensuel = 0;}
			else {
				my $testjoursupmensuel = &num_jour($jrdf, $msdf, $andf);

				if ($testjoursupmensuel == 6) {$joursupmensuel = 1;}
				if ($testjoursupmensuel == 7) {$joursupmensuel = 2;}
			}

			$jourmois = $nbjourmois - $nbnbjrwe - $nbnbferie + $joursupmensuel;
			$boolemens = 1;
		}
		if ($forfait != 0) {
			$boole = 2;
			$tarif = $forfait;
			my $dureecont = Delta_Days($andd, $msdd, $jrdd, $andf, $msdf, $jrdf);
			my $nbferieforfait = &jour_fer_ag_cnx($pas, $agence, $jrdd, $msdd, $andd, $dureecont, $DB);
	  		my $joursupforfait = 0;
	  		my $testjoursupforfait;

			if ($df > $fm) {$joursupforfait = 0;}
			else {
				$testjoursupforfait = &num_jour($jrdf, $msdf, $andf);

				if ($testjoursupforfait == 6) {$joursupforfait = 1;}
				if ($testjoursupforfait == 7) {$joursupforfait = 2;}
			}

			$dureecont++;

			if ($dureecont == 1 && $testjoursupforfait == 7) {$joursupforfait = 1;}

			my $nbjrweforfait = &jour_we($dureecont, $jrdd, $msdd, $andd);
	  		$dureecont = $dureecont - $nbferieforfait - $nbjrweforfait + $joursupforfait;
			$jourmois = $dureecont;
			$booleforf = 1;
		}
		if ($data[15] eq "P") {$ddf = $debutms;}
		elsif (!$data[15] || $data[15] eq "") {
			$ta = $data[7];
			$booleta = 1;
		}
		if ($df>$fm) {
			$dff = $finms;
			$dff2 = "$anfm:$msfm:$temp";
			$boolms = 1;
		} else {
		 	$carbu = $data[9];#CAMONTANTFR
		 	$dff2 = $dff;

			if ($carbu != 0) {$boolecarb = 1;}

		 	$tr = $data[8];#MOTRANSPORTRFR
			$booletr = 1;
		}

		$jrplus = $data[2];  # CONTRATJOURSPLUS
	 	$jrmoins = $data[3]; # CONTRATJOURSMOINS
		my ($dda, $ddm, $ddj);
		my ($dfa, $dfm, $dfj);
		my ($dfa2, $dfm2, $dfj2);

		if ($ddf =~ /:/) {($dda, $ddm, $ddj) = split(/:/, $ddf);}
		else {($dda, $ddm, $ddj) = split(/-/, $ddf);}
		if ($dff =~ /:/) {($dfa, $dfm, $dfj) = split(/:/, $dff);}
		else {($dfa, $dfm, $dfj)=split(/-/, $dff);}
		if ($dff2 =~ /:/) {($dfa2, $dfm2, $dfj2) = split(/:/, $dff2);}
		else {($dfa2, $dfm2, $dfj2) = split(/-/, $dff2);}

		my $duree = Delta_Days($dda, $ddm, $ddj, $dfa, $dfm, $dfj);
		my $duree2 = Delta_Days($dda, $ddm, $ddj, $dfa2, $dfm2, $dfj2);

		if ($duree < 0) {$nbrow--;next;}

		my $nbferie2 = &jour_fer_ag_cnx($pas, $agence, $ddj, $ddm, $dda, $duree2, $DB);
		my $nbjrwe2  = 0;
  		my $joursup2 = 0;
  		my $testjoursup2;

		if (Delta_Days($anfm, $msfm, $temp, $andf, $msdf, $jrdf) > 0) {$joursup2 = 0;}
		else {
			$testjoursup2 = &num_jour($dfj2, $dfm2, $dfa2);

			if ($testjoursup2 == 6) {$joursup2 = 1;}
			if ($testjoursup2 == 7) {$joursup2 = 2;}
		}

		$duree2 += 1;

		if ($duree2 == 1 && $testjoursup2 == 7) {$joursup2 = 1;}

		$nbjrwe2 = &jour_we($duree2, $ddj, $ddm, $dda);
		$duree2 = $duree2 - $nbferie2 - $nbjrwe2 + $joursup2 + $jrplus - $jrmoins;

		if (Delta_Days($anfm, $msfm, $temp, $andf, $msdf, $jrdf) > 0 && $duree2 <= 4) {
			if ($posi) {
                if (($data[14] eq "1")) {
                    $nbcontlivr++;
                    $comm{$modele}->[13] += 1;
                }
				elsif (($data[14] eq "2")) {
					$nbcontcours++;
					$comm{$modele}->[2] += 1;
				} else {
					$nbcontarret++;
					$comm{$modele}->[1] += 1;
				}

				$comm{$modele}->[0] += 1;
			}

			next;
		}
		if ($booleta) {$catr += $ta;}
		if ($booletr) {$catr += $tr;}
		if ($boolemens) {$nbtarifms++;}
		if ($booleforf) {$nbforfait++;}
		if ($boolecarb) {$cacarb += $carbu;}

		# Nouveau calcul, dur�e de jours ouvrables pour tous
        $duree = &LOC::Date::getRentDuration(
            sprintf('%04d-%02d-%02d', $dda, $ddm, $ddj),
            sprintf('%04d-%02d-%02d', $dfa, $dfm, $dfj),
            $agence,
            $data[1]
        ) + $jrplus - $jrmoins;
		$comm{$modele}->[4+$posi] += $duree;
		$durtot += $duree;

		my ($dareel, $dmreel, $djreel) = split(/-/, $data[0]);
		my ($fareel, $fmreel, $fjreel) = split(/-/, $data[1]);
		my $dureereel = Delta_Days($dareel, $dmreel, $djreel, $fareel, $fmreel, $fjreel);
		my $nbferiereel = &jour_fer_ag_cnx($pas, $agence, $djreel, $dmreel, $dareel, $dureereel, $DB);
		my $testjoursupreel = &num_jour($fjreel, $fmreel, $fareel);
		my $joursupreel = 0;

		if ($testjoursupreel == 6) {$joursupreel = 1};
		if ($testjoursupreel == 7) {$joursupreel = 2};
		if ($boolms != 0) {$joursupreel = 0;}

		$dureereel += 1;

		if ($dureereel == 1 && $testjoursupreel == 7) {$joursupreel = 1;}

		my $nbjrwereel = &jour_we($dureereel, $djreel, $dmreel, $dareel);
		$dureereel = $dureereel + $jrplus - $jrmoins - $nbferiereel - $nbjrwereel + $joursupreel;

		if ($dureereel < 5) {$nbdureems++;}
		else {$nbdureeplus++;}
		if ($boole == 1 || $boole == 2) {$tarif = $tarif / $jourmois;}

        my $beginDate = sprintf('%04d-%02d-%02d', $dda, $ddm, $ddj);
        my $endDate   = sprintf('%04d-%02d-%02d', $dfa, $dfm, $dfj);

        # Informations sur l'assurance
        my $ass = $data[6];#CONTRATTYPEASSURANCE
        my $tx = $data[18]; # CONTRATPASSURANCE
        my $insuranceFlags = $data[19];
		my $montantass = 0.0;

        $montantass = &LOC::Common::Rent::calculateInsurance($G_PAYS, $agence, $ass, $tx, $tarif,
                                                             $beginDate, $endDate, $jrplus, $jrmoins);

		if ($insuranceFlags & LOC::Insurance::Type::FLAG_ISINVOICED)
		{
			$nbasscomp++;
		}

        # Gestion de recours
        my $appealTypeId = $data[20];
        my $appealRate   = $data[21];

        my $appealAmount = 0.0;
        if ($appealTypeId)
        {
            $appealAmount = &LOC::Common::Rent::calculateAppeal($G_PAYS, $agence, $appealTypeId, $appealRate, $tarif,
                                                                $beginDate, $endDate, $jrplus, $jrmoins);
        }
		

		$duree = sprintf("%.2f", $duree);
		my $montant = ($tarif * $duree);
		my $mtloc = $montant + $montantass + $appealAmount;
		my $montotal = $montant + $ta + $tr + $carbu + $montantass + $appealAmount;
		$total += $montotal;
		$comm{$modele}->[3+$posi] += $montotal;

		# Pour les machines hors parc, le co�t imput�
		# est �gal au CA / 2
		if ($data[17] == -1) {
			if ($posi) {
				$comm{$modele}->[12] += $mtloc / 2;
			}
			else {
				$comm{$modele}->[11] += $mtloc / 2;
			}
		}
		
		if ($posi) {
			$comm{$modele}->[10] += $mtloc;
			$caloc += $mtloc;

            if (($data[14] eq "1")) {
                $nbcontlivr++;
                $comm{$modele}->[13] += 1;
            }
			elsif (($data[14] eq "2")) {
				$nbcontcours++;
				$comm{$modele}->[2] += 1;
			} else {
				$nbcontarret++;
				$comm{$modele}->[1] += 1;
			}

			$comm{$modele}->[0] += 1;
		} else {
			$comm{$modele}->[5] += $mtloc;
			$calocc += $mtloc;
		}
	}

	$sth->finish;
	my $cond = "MODELEMACHINE.MOMAAUTO";
	my $where = "";

	if ($modl eq "") {$cond = "FACOMAUTO";}
	$where = " and FACOMAUTO=$modl" if $modl;

	my $requete = $DB->prepare(qq{
	select MOTARIFMSFR, MOFORFAITFR, SOLOAUTO, CONTRATDUREE, $cond
	from CONTRAT
	left join MONTANT on CONTRAT.MOAUTO=MONTANT.MOAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where CONTRATOK in ("-1","-2")
	and CONTRAT.LIETCONTRATAUTO in ("2","3","4","7")
	and DATE(CONTRATDD) <= "$finms"
	and DATE(CONTRATDR) >= "$debutms"
	and ETATFACTURE="F"
	$where
	order by $cond
	});
	$requete->execute();

	while (my @data = $requete->fetchrow_array()) {
		$data[0] = $data[0] / $tauxE{$pas};
		$data[1] = $data[1] / $tauxE{$pas};

		my $modele = $data[4];

		if ($data[0] != 0) {$nbtarifms++;}
		if ($data[1] != 0) {$nbforfait++;}
		if ($data[2] != 0) {$modele = 0;}
		if ($data[3] < 5 && $data[0] == 0) {$nbdureems++;}
		else {$nbdureeplus++;}
		if ($posi) {
			$nbcontarret++;
			$comm{$modele}->[1]++;
			$comm{$modele}->[0]++;
		}

		$nbrow++;
	}

	$requete->finish();
	my $requete = $DB->prepare(qq|
	select LIGNEREFARTICLE,
	   sum(ROUND(ROUND(LIGNEPRIX, |.$tabSageFormats->{'price'}.qq|)*ROUND(LIGNEQTITE, |.$tabSageFormats->{'quantity'}.qq|), |.$tabSageFormats->{'amount'}.qq|)),
	   count(LIGNEREFARTICLE), sum(LIGNEQTITE), $cond
	from ENTETELIGNE
	left join ENTETEFACTURE on ENTETELIGNE.ENTETEAUTO=ENTETEFACTURE.ENTETEAUTO
	left join LIGNEFACTURE on LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	left join CONTRAT on ENTETEFACTURE.CONTRATAUTO=CONTRAT.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where ((DATE(`DATE`)>="$debutms" and DATE(`DATE`)<="$finms")
	OR (ENTETEIDFACTU IS NULL AND DATE(CONTRATDD) <= "$finms" and DATE(CONTRATDR) >= "$debutms"))
	and SOLOAUTO=0
	$where
	group by $cond, LIGNEREFARTICLE
	|);
	$requete->execute();

	while (my @data = $requete->fetchrow_array())
    {
        $data[1] = $data[1] / $tauxE{$pas};
        
		$total += $data[1];
		$comm{$data[4]}->[3+$posi] += $data[1];

		if ($data[0] =~ /^TRANS/ || $data[0] eq "SERT") {$catr += $data[1];}
		if ($data[0] eq "CARB") {$cacarb += $data[1];}
		if ($data[0] eq "ASS") {
			$nbasscomp += $data[2];

			if ($posi) {
				$comm{$data[4]}->[10] += $data[1];
				$caloc += $data[1];
			} else {
				$comm{$data[4]}->[5] += $data[1];
				$calocc += $data[1];
			}
		}
        if ($data[0] eq "GDR")
        {
            if ($posi)
            {
                $comm{$data[4]}->[10] += $data[1];
                $caloc += $data[1];
            }
            else
            {
                $comm{$data[4]}->[5] += $data[1];
                $calocc += $data[1];
            }
        }
		if ($data[0] eq "LOC" || $data[0] eq "REM" || $data[0] eq "J+" || $data[0] eq "J-") {
            if ($data[0] ne "REM") {
    			$durtot += $data[3];
    			$comm{$data[4]}->[4+$posi] += $data[3];
            }

			if ($posi) {
				$comm{$data[4]}->[10] += $data[1];
				$caloc += $data[1];
			} else {
				$comm{$data[4]}->[5] += $data[1];
				$calocc += $data[1];
			}
		}
	}

	$requete->finish();

	my $requete = $DB->prepare(qq|
	select LIGNEREFARTICLE,
	   sum(ROUND(ROUND(LIGNEPRIX, |.$tabSageFormats->{'price'}.qq|)*ROUND(LIGNEQTITE, |.$tabSageFormats->{'quantity'}.qq|), |.$tabSageFormats->{'amount'}.qq|)),
	   count(LIGNEREFARTICLE), sum(LIGNEQTITE), $cond
	from ENTETELIGNE
	left join ENTETEFACTURE on ENTETELIGNE.ENTETEAUTO=ENTETEFACTURE.ENTETEAUTO
	left join LIGNEFACTURE on LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	left join CONTRAT on ENTETEFACTURE.CONTRATAUTO=CONTRAT.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
    where ((DATE(`DATE`)>="$debutms" and DATE(`DATE`)<="$finms")
    OR (ENTETEIDFACTU IS NULL AND DATE(CONTRATDD) <= "$finms" and DATE(CONTRATDR) >= "$debutms"))
	and SOLOAUTO!=0
	$where
	group by $cond, LIGNEREFARTICLE
	|);

	$requete->execute();

	while (my @data = $requete->fetchrow_array())
    {
        $data[1] = $data[1] / $tauxE{$pas};

		$total += $data[1];
		$comm{0}->[3+$posi] += $data[1];
		$comm{""}->[3+$posi] -= $data[1];

		if ($data[0] =~ /^TRANS/ || $data[0] eq "SERT") {$catr += $data[1];}
		if ($data[0] eq "CARB") {$cacarb += $data[1];}
		if ($data[0] eq "ASS") {
			$nbasscomp += $data[2];

			if ($posi) {
				$comm{0}->[10] += $data[1];
				$comm{""}->[10] -= $data[1];
				$caloc += $data[1];
			} else {
				$comm{0}->[5] += $data[1];
				$comm{""}->[5] -= $data[1];
				$calocc += $data[1];
			}
		}
        if ($data[0] eq "GDR")
        {
            if ($posi)
            {
                $comm{0}->[10] += $data[1];
                $comm{""}->[10] -= $data[1];
                $caloc += $data[1];
            }
            else
            {
                $comm{0}->[5] += $data[1];
                $comm{""}->[5] -= $data[1];
                $calocc += $data[1];
            }
        }
		if ($data[0] eq "LOC" || $data[0] eq "REM" || $data[0] eq "J+" || $data[0] eq "J-") {
            if ($data[0] ne "REM") {
    			$durtot += $data[3];
    			$comm{0}->[4+$posi] += $data[3];
    			$comm{""}->[4+$posi] -= $data[3];
    		}

			if ($posi) {
				$comm{0}->[10] += $data[1];
				$comm{""}->[10] -= $data[1];
				$caloc += $data[1];
			} else {
				$comm{0}->[5] += $data[1];
				$comm{""}->[5] -= $data[1];
				$calocc += $data[1];
			}
		}
	}

	$requete->finish();

	my $ca = sprintf("%.2f", $total);

	return ($nbrow, $ca, $catr, $cacarb, $nbdureems, $nbdureeplus, $nbcontcours, $nbcontarret, $nbasscomp, $nbtarifms, $nbforfait, $jourmsouv, $durtot, $nbcontlivr);
}

sub list_ag_pays {
	my $notin = qq|"SIE"|;

	if ($pacode) {$notin = $notin.qq|, "SAV"|;}

	my $req = qq{select AGAUTO, PACODE FROM AUTH.`AGENCE` WHERE AGAUTO not in ($notin)};
	my $sth = $DB->prepare($req);
	$sth->execute();
	my %pd;

	while (my @data = $sth->fetchrow_array()) {$pd{$data[0]} = $data[1];}

	$sth->finish;
	return \%pd;
}

sub list_mach {
	my @param = @_;
	my $where = "";
	my $where2 = "";

	if ($modl ne "") {
		$where = qq{WHERE FACOMAUTO="$modl"};
		$where2 = qq{AND FACOMAUTO="$modl"};
	}

	my $req = qq{select MOMAAUTO, FACOMAUTO FROM AUTH.`MODELEMACHINE` LEFT join FAMILLEMACHINE ON MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO $where};
	my $sth = $DB->prepare($req);
	$sth->execute();
	my %po = %{&list_ag_pays()};

	while (my @data = $sth->fetchrow_array()) {
		$mach_mod{$data[0]} = 0;
		$mach_com{$data[1]} = 0;
		$cout_mod{$data[0]} = 0;
		$cout_com{$data[1]} = 0;
	}

	$sth->finish;
	$req = qq{
	select MAAUTO, AGAUTO, MODELEMACHINE.MOMAAUTO, FACOMAUTO, MOMACOUTFR, MAHORSPARC, ETCODE
	FROM AUTH.`MACHINE`
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=MACHINE.MOMAAUTO
	LEFT join FAMILLEMACHINE ON MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO
	WHERE MADATEACHAT<="$param[0]"
	$where2
	};
	$sth = $DB->prepare($req);
	$sth->execute();

	while (my @data = $sth->fetchrow_array()) {
		my $agence = &transfert($data[0], $param[0]);

		if ($agence ne "") {$data[1] = $agence;}
		if ($data[1] eq "SIE") {next};
		if ($data[1] eq "SAV" && $pacode) {next};
		if ($pacode) {
			if ($po{$data[1]} eq $pacode) {
				$mach_mod{$data[2]}++;
				$mach_com{$data[3]}++;

				if ($modl eq "") {$mach_com_ag{$data[3]}{$data[1]}++;}
				else {$mach_com_ag{$data[2]}{$data[1]}++;}

				if ($data[5] != -1) {
					$cout_mod{$data[2]} += $data[4];
					$cout_com{$data[3]} += $data[4];
					$coutatotal += $data[4];
				}
			}
		} else {
			$mach_mod{$data[2]}++;
			$mach_com{$data[3]}++;

			if ($modl eq "") {$mach_com_ag{$data[3]}{$data[1]}++;}
			else {$mach_com_ag{$data[2]}{$data[1]}++;}
			
			if ($data[5] != -1) {
				$cout_mod{$data[2]} += $data[4];
				$cout_com{$data[3]} += $data[4];
				$coutatotal += $data[4];
			}
		}
	}

	$sth->finish;
}

sub transfert {
	my @param = @_;
	my $req = qq{select TRANSFERTAGA FROM AUTH.`TRANSFERT` WHERE MAAUTO="$param[0]" and TRANSFERTDATEAR<="$param[1]" ORDER BY TRANSFERTAUTO DESC LIMIT 1};
	my $sth = $DB->prepare($req);
	$sth->execute();
	my ($ag) = $sth->fetchrow_array();
	$sth->finish;

	if ($ag eq "") {
		$req = qq{select TRANSFERTAGD FROM AUTH.`TRANSFERT` WHERE MAAUTO="$param[0]" and TRANSFERTDATEAR>"$param[1]" ORDER BY TRANSFERTAUTO LIMIT 1};
		$sth = $DB->prepare($req);
		$sth->execute();
		($ag) = $sth->fetchrow_array();
	}

	return $ag;
}

sub list_mach2 {
	my @param = @_;
	my $where = "";

	if ($modl ne "") {$where = qq{ AND FACOMAUTO="$modl"};}

	my $req = qq{select MOMAAUTO, FACOMAUTO FROM AUTH.`MODELEMACHINE` LEFT join FAMILLEMACHINE ON MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO $where};
	my $sth = $DB->prepare($req);
	$sth->execute();

	while (my @data = $sth->fetchrow_array()) {
		$mach_mod{$data[0]} = 0;
		$mach_com{$data[1]} = 0;
		$cout_mod{$data[0]} = 0;
		$cout_com{$data[1]} = 0;
	}

	$sth->finish;
	my $req = "";
	my $notin = qq|"SIE"|;

	if ($pacode) {$notin = $notin.qq|, "SAV"|;}

	if (!$date) {
        # R�cup�ration des machines dans les �tats qui vont bien (< 20 et != 8 et 10)
        # Les machines sont comptabilis�es sur leur agence de visibilit� si elles sont en transfert
        # ou en livraison et qu'elles sont attribu�es � un contrat.
        # Sinon, elles sont comptabilis�es sur leur agence d'appartenance
		$req = qq{
		select M.MOMAAUTO, count(*),
        IF(M.MASUPPAGAUTO IS NOT NULL AND M.ETCODE IN ("MAC09", "MAC12") AND C.ETCODE = "CON01",
            M.MASUPPAGAUTO,
            M.AGAUTO) AS AG_MODIF,
        0, M.MAHORSPARC
		FROM AUTH.`MACHINE` M
        LEFT JOIN CONTRAT C ON (M.CONTRATAUTO = C.CONTRATAUTO AND M.MAAUTO = C.MAAUTO AND C.CONTRATMACHATTR = -1)
		WHERE M.LIETCODE not IN ("8","10")
		AND M.LIETCODE is not NULL
		AND M.AGAUTO not in ($notin)
		AND M.LIETCODE<20
		GROUP BY M.MOMAAUTO, M.AGAUTO, M.MAHORSPARC
		};
	} else {
		my @time = split(/:/, $param[0]);
		my ($anhist, $mshist, $jrhist) = Add_Delta_Days($time[0], $time[1], $time[2], 1);
		$req = qq{
		select HISTOSTOCKMODELE.MOMAAUTO, sum(HISTSTMOTOTAL), AGAUTO, sum(HISTSTMOHORSPARC), 0
		FROM AUTH.`HISTOSTOCKMODELE`
		where HISTDATE between "$anhist:$mshist:$jrhist 00:00:00" and "$anhist:$mshist:$jrhist 23:59:59"
		AND AGAUTO not in ($notin)
		GROUP BY HISTOSTOCKMODELE.MOMAAUTO, AGAUTO
		};
	}

	$sth = $DB->prepare("$req");
	$sth->execute();
	my %po = %{&list_ag_pays()};

	while (my @data = $sth->fetchrow_array()) {
		my @tp = &tp_mach($data[0]);

		if (@tp) {
       		if ($pacode) {
				if ($po{$data[2]} eq $pacode) {
					$mach_mod{$data[0]} += $data[1];
					$mach_com{$tp[0]}   += $data[1];

					if ($modl eq "") {$mach_com_ag{$tp[0]}{$data[2]} += $data[1];}
					else {$mach_com_ag{$data[0]}{$data[2]} += $data[1];}

					if ($data[4] != -1) {
						$cout_mod{$data[0]}      += $tp[1] * ($data[1] - $data[3]);
						$cout_com{$tp[0]}        += $tp[1] * ($data[1] - $data[3]);
						$coutatotal              += $tp[1] * ($data[1] - $data[3]);
					}
				}
			} else {
				$mach_mod{$data[0]} += $data[1];
				$mach_com{$tp[0]} += $data[1];

				if ($modl eq "") {$mach_com_ag{$tp[0]}{$data[2]} += $data[1];}
				else {$mach_com_ag{$data[0]}{$data[2]} += $data[1];}

				if ($data[4] != -1) {
					$cout_mod{$data[0]}      += $tp[1] * ($data[1] - $data[3]);
					$cout_com{$tp[0]}        += $tp[1] * ($data[1] - $data[3]);
					$coutatotal              += $tp[1] * ($data[1] - $data[3]);
				}
			}
		}
	}

	$sth->finish;
}

sub tp_mach {
	my @param = @_;
	my $where = "";

	if ($modl ne "") {$where = qq{ AND FACOMAUTO="$modl"};}

	my $req = qq{
	select FACOMAUTO, MOMACOUTFR
	FROM AUTH.`MODELEMACHINE`
	LEFT join FAMILLEMACHINE ON MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO
	WHERE MOMAAUTO="$param[0]"
	$where
	};
	my $sth = $DB->prepare("$req");
	$sth->execute();
	my @data = $sth->fetchrow_array();
	$sth->finish;
	return @data;
}

sub nb_mach {
	my @param = @_;
	my $t = &test_date($mimois);

	if ($t<0) {&list_mach($param[0]);}
	else {&list_mach2($day);}

	my $modele;

	if ($modl eq "") {
		$modele = qq{
		select FAMILLECOMMERCIALE.FACOMAUTO, FACOMLIBELLE, FACOMORDRE, MOMAGRP
		from FAMILLECOMMERCIALE
		left join FAMILLEMACHINE ON FAMILLEMACHINE.FACOMAUTO=FAMILLECOMMERCIALE.FACOMAUTO
		left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO
		where MOMAGRP is not NULL
		group by FAMILLECOMMERCIALE.FACOMAUTO, MOMAGRP
		order by FACOMORDRE
		};
	} else {
		$modele = qq{
		select MOMAAUTO, MOMADESIGNATION, MOMAGRP, FAMAELEVATION
		from AUTH.`MODELEMACHINE`
		left join FAMILLEMACHINE ON MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO
		where FACOMAUTO="$modl"
		order by MOMADESIGNATION, MOMAGRP
		};
	}

	my $sth = $DB->prepare("$modele");
	$sth->execute();

	while (my @data = $sth->fetchrow_array()) {
	       	$fmcomm{$data[0]} = $data[1];

	       	if ($modl eq "") {
       			$ordcomm[$data[2]] = $data[0];
       			$ordcommtot[$data[2]] = $data[3];
       		} else {
       			push(@ordcomm, $data[0]);
       			push(@ordcommtot, $data[2]);
       			$data[3] = "" if ($data[3] eq "n/a");
			$htcomm{$data[0]} = $data[3];

       		}
		for (my $i=0;$i<14;$i++) {$comm{$data[0]}->[$i] = 0;}
       		if ($modl eq "") {
			$comm{$data[0]}->[8]  = $mach_com{$data[0]};
			$comm{$data[0]}->[9]  = $cout_com{$data[0]};
		} else {
			$comm{$data[0]}->[8]  = $mach_mod{$data[0]};
			$comm{$data[0]}->[9]  = $cout_mod{$data[0]};
		}

		$totmc += $mach_com{$data[0]};
       	}

	$sth->finish;

	if ($modl eq "") {
       		$ordcomm[203] = 0;
       		$ordcommtot[203] = 1;
	}

	$fmcomm{0} = $mess[3300];

	for (my $i=0;$i<14;$i++) {$comm{0}->[$i] = 0;}
}

sub statistique {
	for(my $i=0;$i<@pys;$i++) {
		my @rs  = &statjr($deb, $fin, 3, $BDC[$i], $pys[$i]);
		my @rs1 = &statjr($deb, $dtjour, 0, $BDC[$i], $pys[$i]);

		for(my $k=0;$k<@rs;$k++) {
			if (($k==11)&&($i)) {next};

			$rest[$k] += $rs[$k];
			$resc[$k] += $rs1[$k];
		}
	}
}

sub entete {
    my $conversion = $_[0];
	my $db = $dbaff;
	my $fn = $fnaff;
	my $fna = $affdte;

	if ($tsdt) {
		my @data = split(/\:/, $date);
		my ($dbf, $dbff, $fnf, $fnff) = &debfinmois($data[1], $data[0]);
		$db = $dbff;
		$fn = $fnff;
		$fna = $fn;
	}

	my $heure = sprintf("%02d:%02d:%02d", (localtime)[2], (localtime)[1], (localtime)[0]);
	
	#
	# Tableau r�capitulatif
	#
   	print qq{
	<table width="100%" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td rowspan=6 colspan=2 align="left"><font class="PT"><b>$affdte $heure<br>}.($pacode ? $pacode : "GR").qq{</b></font></th>
			<th colspan=4><font class="PT">$mess[5000]</font></th>
			<th rowspan=2><font class="PT">$mess[5001]</font></th>
			<td rowspan=2 colspan=6 align="center">$jfr</td>
			<td rowspan=3 colspan=5 align="right"><font class="PT"><b>$mess[1350]:&nbsp;$db<br>$mess[1351]:&nbsp;$fn</b></font></td>
		</tr>
		<tr bgcolor="#C6CDC5">
			<th><font class="PT">$mess[5002]</font></th>
			<th><font class="PT">$mess[5003]</font></th>
			<th><font class="PT">$mess[5004]</font></th>
            <th><font class="PT">$mess[5027]</font></th>
		</tr>
	};
	my $total = $rest[6] + $rest[7] + $rest[13];
	my $ut = 0;
	my $tot = $totmc;

	if ($modl ne "") {$tot = $mach_com{$modl};}
	if ($tot) {$ut = (($rest[6]+$rest[13])*100)/$tot;}

	$ut = sprintf("%.2f",$ut);
	
	# Synth�se des chiffres
	#
	# --- Nb contrats ---
	# TOT
	# ARR
	# ACT
    #
    # Util. inst.
	#
	# ASS FACT
	# CT<5j
	# MENS
	# FORF
	# --- Arr�t� au jour J ---
	# CA
	# CA LOC
	# TRANSP 
	# CARB
    # --- Arr�t� � la fin du mois ---
	# CA
	# CA LOC
	# TRANSP
	# CARB
	# Px Jr LOC
	# COUT
	# CT Jr
	# RATIO 
	print qq{
	<tr>
		<td align="center"><font class="PT">$total</font></td>
		<td align="center"><font class="PT">$rest[7]</font></td>
		<td align="center"><font class="PT">$rest[6]</font></td>
        <td align="center"><font class="PT">$rest[13]</font></td>
		<td align="center"><font class="PT">$ut%</font></td>
	</tr>
	<tr bgcolor="#C6CDC5">
		<th rowspan=2><font class="PT">$mess[5005]</font></th>
		<th rowspan=2><font class="PT">$mess[5006]</font></th>
		<th rowspan=2><font class="PT">$mess[5007]</font></th>
		<th rowspan=2><font class="PT">$mess[5008]</font></th>
		<th colspan=6><font class="PT">$mess[5009]$fna</font></th>
		<th colspan=8><font class="PT">$mess[5010]</font></th>
	</tr>
	<tr bgcolor="#C6CDC5">
		<th align="right"><font class="PT">$mess[5011]</font></th>
		<th align="right"><font class="PT">$mess[5011] $mess[5025]</font></th>
		<th align="right" colspan=2><font class="PT">$mess[5012]</font></th>
		<th align="right" colspan=2><font class="PT">$mess[5013]</font></th>
		<th align="right"><font class="PT">$mess[5011]</font></th>
		<th align="right"><font class="PT">$mess[5011] $mess[5025]</font></th>
		<th align="right"><font class="PT">$mess[5012]</font></th>
		<th align="right"><font class="PT">$mess[5013]</font></th>
		<th align="right"><font class="PT">$mess[5019] $mess[5025]</font></th>
		<th align="right"><font class="PT">$mess[5014]</font></th>
		<th align="right"><font class="PT">$mess[5015]</font></th>
		<th align="right"><font class="PT">RATIO</font></th>
	</tr>
	<tr>
		<td align="center"><font class="PT">$rest[8]</font></td>
		<td align="center"><font class="PT">$rest[4]</font></td>
		<td align="center"><font class="PT">$rest[9]</font></td>
		<td align="center"><font class="PT">$rest[10]</font></td>
	};

	my $cat = $rest[1];

	for(my $i=1;$i<4;$i++) {
		$rest[$i] = sprintf("%.0f",$rest[$i]); # CA, TRANSP, CARB arr�t� fin de mois
		$resc[$i] = sprintf("%.0f",$resc[$i]); # CA, TRANSP CARB jour J
		$rest[$i] = &fnombre($rest[$i], 3, 0);
		$resc[$i] = &fnombre($resc[$i], 3, 0);
	}

	my $affcaloc = sprintf("%.0f", $caloc);
	$affcaloc = &fnombre($affcaloc, 3, 0); # CA LOC arr�t� fin de mois
	my $affcalocc = sprintf("%.0f", $calocc);
	$affcalocc = &fnombre($affcalocc, 3, 0); # CA LOC arr�t� jour J
	print qq{
		<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$resc[1]</span></font></td>
		<td align="center"><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$affcalocc</span></font></td>
		<td align="right" nowrap colspan=2><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$resc[2]</span></font></td>
		<td align="right" nowrap colspan=2><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$resc[3]</span></font></td>
		<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$rest[1]</span></font></td>
		<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$affcaloc</span></font></td>
		<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$rest[2]</span></font></td>
		<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$rest[3]</span></font></td>
	};
	my $pml = 0;

	if ($rest[12]) {
		$pml = ($caloc / $rest[12]);
		$pml = sprintf("%.1f", $pml);
	}

    # Px Jr LOC
	print qq{<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$pml</span></font></td>\n};
	
	my $ct = ($ctaux / 100) * $coutatotal;
	my $aff = sprintf("%.0f", $ct);
	$aff = &fnombre($aff, 3, 0);
	
	# COUT
	print qq{<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$aff</span></font></td>\n};
	
	my $cjm = 0;
	my $affcjm = 0;

	if ($tot) {
		$cjm = $ct / ($nbjrmoy * $tot * $coef);
		$cjm = sprintf("%.1f", $cjm);
		$affcjm = $cjm;
	}

    # CT Jr
	print qq|<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$affcjm</span></font></td>\n|;
	my $pc = 0;

	if ($rest[12]) {
		my $pm = $cat / $rest[12];
		$pm = sprintf("%.0f",$pm);

		if ($cjm != 0) {$pc = $pml/$cjm;}
	}

	$pc = sprintf("%.2f", $pc);
	print qq{<td align="right" nowrap><font class="PT">$pc</font></td>};
	print qq{
	</tr>
	<tr>
		<td colspan=18 >&nbsp;</td>
	</tr>
	};
}

sub tire_tab {
	my $jourmsouv = 0;
	my $parc_tot = 0;
	for (my $i=0; $i<@pys; $i++) {
		my $pas = $pys[$i];
		$agences{$pas} = ();
		my $temp = $DB->prepare(qq|select AGAUTO from AUTH.`AGENCE` where PACODE="$pas" and AGAUTO not in ("SIE")|);
		$temp->execute();
	
		while (my @temp = $temp->fetchrow_array()) {push @{$agences{$pas}}, $temp[0];}
	
		$temp->finish();
		
		foreach (@{$agences{$pas}}) {
			my $req = "";
			my $j = 0;
			if (!$tsdt) {
				$j = &jours_ouvrables($deb, $dtjour, $pas, $_, $DB);
				$req = qq{
					select count(*)
					FROM AUTH.`MACHINE`
					WHERE LIETCODE not IN ("8","10")
						AND LIETCODE is not NULL
						AND LIETCODE<20
						AND AGAUTO="$_"
				};
			}
			else {
				$req = qq{
					select sum(HISTSTMOTOTAL)
					FROM AUTH.`HISTOSTOCKMODELE`
					where HISTDATE between "$mimois 00:00:00" and "$mimois 23:59:59"
						AND AGAUTO="$_"
				};
				$j = &jours_ouvrables(substr($mimois, 0,
					length($mimois)-2)."01", $mimois, $pas, $_, $DB);
			}
			my $sth_j = $DB->prepare("$req");
			$sth_j->execute();
			my ($parc) = $sth_j->fetchrow_array();
			$sth_j->finish;
			$parc_tot += $parc;
			$jourmsouv += $j * $parc;
		}
	}
	if($parc_tot != 0)
    {
        $jourmsouv /= $parc_tot;
    }
	$jourmsouv = sprintf("%.1f", $jourmsouv);
	$nbjours_pondere = $jourmsouv;
	
	my @param = @_;
	my $fna = $affdte;

	if ($tsdt) {
		my @data = split(/\:/, $date);
		my ($dbf, $dbff, $fnf, $fnff) = &debfinmois($data[1], $data[0]);
		$fna = $fnff;
	}

	print qq{
	<tr bgcolor="#C6CDC5">
		<th rowspan=2><font class="PT">$param[0]</font></th>
	};

	if ($modl) {print qq|<th rowspan=2><font class="PT">$mess[2959]</font></th>|;}

	print qq{
		<th rowspan=2><font class="PT">$mess[5016]</font></th>
		<th colspan=4><font class="PT">$mess[5000]</font></th>
		<th rowspan=2><font class="PT">$mess[5001]</font></th>
		<th colspan=4><font class="PT">$mess[5009]$fna</font></th>
		<th align="right"><font class="PT">$jourmsouv&nbsp;</font></th>
		<th align="left"><font class="PT">&nbsp;$mess[5026]</font></th>
		<th colspan=8><font class="PT">$mess[5010]</font></th>
	</tr>
	<tr bgcolor="#C6CDC5">
		<th><font class="PT">$mess[5002]</font></th>
		<th><font class="PT">$mess[5003]</font></th>
		<th><font class="PT">$mess[5004]</font></th>
        <th><font class="PT">$mess[5027]</font></th>
		<th align="right"><font class="PT">$mess[5011]</font></th>
		<th align="right"><font class="PT">$mess[5011] $mess[5025]</font></th>
		<th><font class="PT">$mess[5017]</font></th>
		<th align="right"><font class="PT">$mess[5018]</font></th>
		<th align="right"><font class="PT">$mess[5020]</font></th>
		<th align="right"><font class="PT">Ratio cash</font></th>
		<th align="right"><font class="PT">$mess[5011]</font></th>
		<th align="right"><font class="PT">$mess[5011] $mess[5025]</font></th>
		<th><font class="PT">$mess[5017]</font></th>
		<th ><font class="PT">$mess[5019]</font></th>
		<th><font class="PT">$mess[5019] $mess[5025]</font></th>
		<th align="right"><font class="PT">$mess[5020]</font></th>
		<th><font class="PT">$mess[5021]</font></th>
		<th align="right"><font class="PT">Ratio prix</font></th>
	</tr>
	};
}

sub corps {
    my $conversion = $_[0];
	my $jourmsouv = 0;
	if (!$tsdt) {
		$jourmsouv = &jours_ouvrables($deb, $dtjour, $G_PAYS, $G_AGENCE, $DB);
	}
	else {
		$jourmsouv = &jours_ouvrables(substr($mimois, 0,
			length($mimois)-2)."01", $mimois, $G_PAYS, $G_AGENCE, $DB);
	}
		
	&tire_tab($mess[5023]);
	$jourmsouv = $nbjours_pondere;
	
	my @totaux = ();
	my @sstot = ();
	my @sstot_divers = ();
	my %ponderation;
	my @sspond = ();
	my @affichage = ();

	for (my $i=0;$i<9;$i++) {
		$totaux[$i]=0;
		$sstot_divers[$i] = 0;
	}

	#corps
	my @k = ();

	for(my $i=0;$i<@ordcomm;$i++) {
		my $boole_aff = 0;
		my $j = $ordcomm[$i];

		if ($j eq "") {next};
		# FA 8964 : Test sur le nb de parc et sur le CA total fin de mois
		if (($comm{$j}->[8] == 0) && ($comm{$j}->[6] == 0)) {next;}

		my $a = "";
		my $f = "";

		if (($modl eq "")&&($j)) {
			$a = qq{<a href="#" onclick="return rs('modele$j', '}.$cgi->url().qq{?paysauto=$pacode&date=$date&modele=$j&aff=$fmcomm{$j}', 630, 400)">};
			$f = qq{</a>};
		}

		$totaux[8]  += $comm{$j}->[8];
		$totaux[0]  += $comm{$j}->[0];
		$totaux[1]  += $comm{$j}->[1];
		$totaux[2]  += $comm{$j}->[2];
		$totaux[3]  += $comm{$j}->[3];
		$totaux[4]  += $comm{$j}->[4];
		$totaux[6]  += $comm{$j}->[6];
		$totaux[7]  += $comm{$j}->[7];
		$totaux[9]  += $comm{$j}->[9];
		$totaux[10] += $comm{$j}->[10];
		$totaux[11] += $comm{$j}->[11];
		$totaux[12] += $comm{$j}->[12];
		$totaux[5]  += $comm{$j}->[5];
        $totaux[13] += $comm{$j}->[13];

		if (!$ordcommtot[$i]) {
			$sstot[8]  += $comm{$j}->[8];
			$sstot[0]  += $comm{$j}->[0];
			$sstot[1]  += $comm{$j}->[1];
			$sstot[2]  += $comm{$j}->[2];
			$sstot[3]  += $comm{$j}->[3];
			$sstot[4]  += $comm{$j}->[4];
			$sstot[6]  += $comm{$j}->[6];
			$sstot[7]  += $comm{$j}->[7];
			$sstot[9]  += $comm{$j}->[9];
			$sstot[10] += $comm{$j}->[10];
			$sstot[11] += $comm{$j}->[11];
			$sstot[12] += $comm{$j}->[12];
			$sstot[5]  += $comm{$j}->[5];
            $sstot[13] += $comm{$j}->[13];
			$boole_aff = 1;
			push @sspond, $j;
		} else {
			$sstot_divers[8]  += $comm{$j}->[8];
			$sstot_divers[0]  += $comm{$j}->[0];
			$sstot_divers[1]  += $comm{$j}->[1];
			$sstot_divers[2]  += $comm{$j}->[2];
			$sstot_divers[3]  += $comm{$j}->[3];
			$sstot_divers[4]  += $comm{$j}->[4];
			$sstot_divers[6]  += $comm{$j}->[6];
			$sstot_divers[7]  += $comm{$j}->[7];
			$sstot_divers[9]  += $comm{$j}->[9];
			$sstot_divers[10] += $comm{$j}->[10];
			$sstot_divers[11] += $comm{$j}->[11];
			$sstot_divers[12] += $comm{$j}->[12];
			$sstot_divers[5]  += $comm{$j}->[5];
            $sstot_divers[13] += $comm{$j}->[13];
		}

		my $c = $k[$boole_aff] % 2;
		my $color = "''";

		if ($c == 1) {$color = qq{'#D6D6D6'};}

        # Famille commerciale
		$affichage[$boole_aff] .=  qq|
		<tr bgColor=$color onmouseover="bgColor='#FFCC33'" onmouseout="bgColor=$color">
			<td align="left"><font class="PT">$a<b>$fmcomm{$j}</b>$f</font></td>
		|;

		if ($modl) {$affichage[$boole_aff] .=  qq|<td align="right"><font class="PT">$htcomm{$j}</font></td>|;}

        # Parc
        # --- Nbr contrats ---
        # TOT
        # ARR
        # ACT
		$affichage[$boole_aff] .=  qq|
			<td align="center" nowrap><font class="PT">$comm{$j}->[8]</font></td>
			<td align="center" nowrap><font class="PT">$comm{$j}->[0]</font></td>
			<td align="center" nowrap><font class="PT">$comm{$j}->[1]</font></td>
			<td align="center" nowrap><font class="PT">$comm{$j}->[2]</font></td>
            <td align="center" nowrap><font class="PT">$comm{$j}->[13]</font></td>
		|;

		my $ut = 0;

		if ($comm{$j}->[8]) {$ut = (100 * ($comm{$j}->[2] + $comm{$j}->[13])) / $comm{$j}->[8];}

        # Util. inst.
		$ut = sprintf("%.1f", $ut);
		$affichage[$boole_aff] .= qq|<td align="right" nowrap><font class="PT">$ut%</font></td>\n|;
		my $aff = sprintf("%.0f", $comm{$j}->[3]);
		my $aff2 = sprintf("%.1f", $comm{$j}->[4]);
		my $aff3 = sprintf("%.0f", $comm{$j}->[5]);

		if ($aff > 0) {
			$aff = &fnombre($aff, 3, 0);
			$aff3 = &fnombre($aff3, 3, 0);
		}

        # --- Arr�t� au jour J ---
        # CA
        # CA LOC
        # JR Loc
		$affichage[$boole_aff] .=  qq|
			<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$aff</span></font></td>
			<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$aff3</span></font></td>
			<td align="center" nowrap><font class="PT">$aff2</font></td>
		|;
		$ut = 0;

		# A voir suivant l'Espagne si on fait une moyenne des jours ouvrables ou non pour l'ensemble du pays
		if (($resc[11]) && ($comm{$j}->[8])) {
			$ponderation{$j} = 0;

			foreach my $pais (@pys) {
				foreach (@{$agences{$pais}}) {
					if ($mach_com_ag{$j}{$_} && $jourmsouv{$_}) {$ponderation{$j} += $mach_com_ag{$j}{$_} * $jourmsouv{$_};}
				}
			}

			$ut = ($ponderation{$j}) ? (100 * ($comm{$j}->[4]))/($ponderation{$j}) : 0;
		}

		$ut = sprintf("%.1f", $ut);
		
		# Util. inst.
		$affichage[$boole_aff] .=  qq|<td align="right" nowrap><font class="PT">$ut%</font></td>\n|;

		# Co�t
		# Calcul du co�t mensuel (repris plus bas)
		my $cout = 0;
		my $affcout = 0;
		
		if ($resc[11]) {
			$cout = $ctaux * $comm{$j}->[9] / 100;
		}
		# Calcul du co�t sur la p�riode
		my $cout_periode = $cout / $nbjrmoy * $jourmsouv + $comm{$j}->[11];
		if ($resc[11]) {
			$cout += $comm{$j}->[12];
			$cout=sprintf("%.0f", $cout);
			$affcout=&fnombre($cout,3,0);
		}
		
		# COUT
		$affichage[$boole_aff] .= qq|
			<td align="right" nowrap><font class="PT" color="red"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">|
			.&fnombre(sprintf("%.0f", $cout_periode), 3, 0).qq|</span></b></font></td>
		|;

		# Ratio 2
		my $ratio2 = ($cout_periode == 0) ? 0 : ($comm{$j}->[5]) / $cout_periode;
		$ratio2 = sprintf("%.2f",$ratio2);
		
		# Ratio cash
		$affichage[$boole_aff] .= qq|
			<td align="right" nowrap><font class="PT" color="red"><b>
			$ratio2</b></font></td>
		|;

		my $aff = sprintf("%.0f", $comm{$j}->[6]);
		my $aff2 = sprintf("%.1f", $comm{$j}->[7]);
		my $aff3 = sprintf("%.0f", $comm{$j}->[10]);
		$aff = &fnombre($aff, 3, 0);
		$aff3 = &fnombre($aff3, 3, 0);
		
		# --- Arr�t� � la fin du mois ---
		# CA
		# CA LOC
		# JR Loc
		$affichage[$boole_aff] .=  qq|
			<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$aff</span></font></td>
			<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$aff3</span></font></td>
			<td align="center" nowrap><font class="PT">$aff2</span></font></td>
		|;
		$ut = 0;
		my $ut1 = 0;

		if ($comm{$j}->[7]!=0) {
			$ut = ($comm{$j}->[6] / $comm{$j}->[7]);
			$ut1 = ($comm{$j}->[10] / $comm{$j}->[7]);
		}

		$ut=sprintf("%.1f",$ut);
		$ut1=sprintf("%.1f",$ut1);
		
		# Px Jr
		# Px Jr LOC
		$affichage[$boole_aff] .=  qq|
			<td align="center" nowrap><font class="PT"><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$ut</span></font></td>
			<td align="center" nowrap><font class="PT"><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$ut1</span></font></td>
		|;

		# COUT
		$affichage[$boole_aff] .=  qq|<td align="right" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$affcout</span></font></td>\n|;
		my $cjm = 0;
		my $affcjm = 0;

		if ($comm{$j}->[8] != 0) {
			$cjm = $cout / ($nbjrmoy * $coef * $comm{$j}->[8]);
			$cjm = sprintf("%.1f", $cjm);
			$affcjm = $cjm;
		}

        # CT Jr
		$affichage[$boole_aff] .=  qq|<td align="center" nowrap><font class="PT"><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$affcjm</span></font></td>\n|;
		my $pc=0;

		if ($cjm != 0) {$pc = $ut1 / $cjm;}

		$pc = sprintf("%.2f", $pc);
		
		# Ratio prix
		$affichage[$boole_aff] .=  qq|<td align="right" nowrap><font class="PT">$pc</font></td>\n|;
		$affichage[$boole_aff] .=  qq{</tr>\n};
		$k[$boole_aff]++;
	}
  	# sous totaux divers
  	if ($modl eq "") {
        # Parc
        # --- Nbr contrats ---
        # TOT
        # ARR
        # ACT
	  	$affichage[0] .=  qq|
	  	<tr bgcolor="yellow">
	  		<td align="left" nowrap><font class="PT"><b>SOUS TOT DIVERS</b></font></td>
	  		<td align="center" nowrap><font class="PT"><b>$sstot_divers[8]</b></font></td>
	  		<td align="center" nowrap><font class="PT"><b>$sstot_divers[0]</b></font></td>
	  		<td align="center" nowrap><font class="PT"><b>$sstot_divers[1]</b></font></td>
	  		<td align="center" nowrap><font class="PT"><b>$sstot_divers[2]</b></font></td>
            <td align="center" nowrap><font class="PT"><b>$sstot_divers[13]</b></font></td>
	  	|;
		my $ut = 0;

		if ($sstot_divers[8]) {$ut = (100 * ($sstot_divers[2] + $sstot_divers[13])) / $sstot_divers[8];}

		$ut = sprintf("%.1f", $ut);
		
		# Util. inst.
		$affichage[0] .=  qq|<td align="right" nowrap><font class="PT"><b>$ut%</b></font></td>\n|;
		my $aff  = sprintf("%.0f", $sstot_divers[3]);
		my $aff2 = sprintf("%.1f", $sstot_divers[4]);
		my $aff3 = sprintf("%.0f", $sstot_divers[5]);

		if ($aff>0) {
			$aff = &fnombre($aff, 3, 0);
			$aff3 = &fnombre($aff3, 3, 0);
		}

        # --- Arr�t� au jour J ---
        # CA
        # CA LOC
        # JR Loc
		$affichage[0] .=  qq|
			<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$aff</span></b></font></td>
			<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$aff3</span></b></font></td>
			<td align="center" nowrap><font class="PT"><b>$aff2</b></font></td>
		|;
		$ut = 0;

		if ($resc[11] && $sstot_divers[8]) {
			$ut = (100 * $sstot_divers[4]) / ($jourmsouv * $sstot_divers[8]);
		}

		$ut = sprintf("%.1f", $ut);
		
		# % Util
		$affichage[0] .=  qq|<td align="right" nowrap><font class="PT"><b>$ut%</b></font></td>\n|;

		# Co�t
		# Calcul du co�t mensuel (repris plus bas)
		my $couto = 0;
		my $affcouto = 0;

		if ($resc[11]) {
			$couto = $ctaux * $sstot_divers[9] / 100;
		}
		# Calcul du co�t sur la p�riode
		my $couto_periode = $couto / $nbjrmoy * $nbjours_pondere + $sstot_divers[11];
		if ($resc[11]) {
			$couto += $sstot_divers[12];
			$couto = sprintf("%.0f", $couto);
			$affcouto = &fnombre($couto, 3, 0);
		}
		
		# COUT
		$affichage[0] .= qq|
			<td align="right" nowrap><font class="PT" color="red"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">|
			.&fnombre(sprintf("%.0f", $couto_periode), 3, 0).qq|</span></b></font></td>
		|;

		# Ratio 2
		my $ratio2 = ($couto_periode == 0) ? 
			0 : ($sstot_divers[5]) / $couto_periode;
		$ratio2 = sprintf("%.2f",$ratio2);
		
		# Ratio cash
		$affichage[0] .= qq|
			<td align="right" nowrap><font class="PT" color="red"><b>
			$ratio2</b></font></td>
		|;
		
		my $aff  = sprintf("%.0f", $sstot_divers[6]);
		my $aff2 = sprintf("%.1f", $sstot_divers[7]);
		my $aff3 = sprintf("%.0f", $sstot_divers[10]);
		$aff  = &fnombre($aff, 3, 0);
		$aff3 = &fnombre($aff3, 3, 0);
		
		# --- Arr�t� � la fin du mois ---
		# CA
		# CA LOC
		# JR Loc
		$affichage[0] .=  qq|
			<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$aff</span></b></font></td>
			<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$aff3</span></b></font></td>
			<td align="center" nowrap><font class="PT"><b>$aff2</b></font></td>
		|;
		$ut = 0;
		my $ut1 = 0;

		if ($sstot_divers[7]) {
			$ut = ($sstot_divers[6] / $sstot_divers[7]);
			$ut1 = ($sstot_divers[10] / $sstot_divers[7]);
		}

		$ut  = sprintf("%.1f", $ut);
		$ut1 = sprintf("%.1f", $ut1);
		
		# Px Jr
		# Px Jr LOC
		$affichage[0] .=  qq|
			<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$ut</span></b></font></td>
			<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$ut1</span></b></font></td>
		|;
		
		# COUT
		$affichage[0] .=  qq|<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$affcouto</span></b></font></td>\n|;
		my $cjm = 0;
		my $affcjm = 0;

		if ($sstot_divers[8] != 0) {
			$cjm = $couto / ($nbjrmoy * $coef * $sstot_divers[8]);
			$cjm = sprintf("%.1f", $cjm);
			$affcjm = $cjm;
		}

        # CT Jr
		$affichage[0] .=  qq|<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$affcjm</span></b></font></td>\n|;
		my $pc = 0;

		if ($cjm != 0) {$pc = $ut1 / $cjm;}

		$pc = sprintf("%.2f", $pc);
		
		# Ratio prix
		$affichage[0] .=  qq|
			<td align="right" nowrap><font class="PT"><b>$pc</b></font></td>
		</tr>
		|;
		$affichage[0] .= "\n<tr><td colspan=21 align=center><hr></td></tr>\n";
	}

  	# sous totaux machines
  	if ($modl eq "") {
        # Parc
        # --- Nbr contrats ---
        # TOT
        # ARR
        # ACT
	  	$affichage[1] .=  qq|
	  	<tr bgcolor="orange">
	  		<td align="left" nowrap><font class="PT"><b>SOUS TOT MACH</b></font></td>
	  		<td align="center" nowrap><font class="PT"><b>$sstot[8]</b></font></td>
	  		<td align="center" nowrap><font class="PT"><b>$sstot[0]</b></font></td>
	  		<td align="center" nowrap><font class="PT"><b>$sstot[1]</b></font></td>
	  		<td align="center" nowrap><font class="PT"><b>$sstot[2]</b></font></td>
            <td align="center" nowrap><font class="PT"><b>$sstot[13]</b></font></td>
	  	|;
		my $ut = 0;

		if ($sstot[8]) {$ut = (100 * ($sstot[2] + $sstot[13])) / $sstot[8];}

		$ut = sprintf("%.1f", $ut);
		
		# Util. Inst.
		$affichage[1] .=  qq|<td align="right" nowrap><font class="PT"><b>$ut%</b></font></td>\n|;
		
		my $aff  = sprintf("%.0f", $sstot[3]);
		my $aff2 = sprintf("%.1f", $sstot[4]);
		my $aff3 = sprintf("%.0f", $sstot[5]);

		if ($aff>0) {
			$aff = &fnombre($aff, 3, 0);
			$aff3 = &fnombre($aff3, 3, 0);
		}

        # --- Arr�t� au jour J ---
        # CA
        # CA LOC
        # JR Loc
		$affichage[1] .=  qq|
			<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$aff</span></b></font></td>
			<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$aff3</span></b></font></td>
			<td align="center" nowrap><font class="PT"><b>$aff2</b></font></td>
		|;
		$ut = 0;

		if ($resc[11] && $sstot[8]) {
			# $ut = (100 * $sstot[4]) / ($resc[11] * $sstot[8]);
			my $ponderation;

			foreach my $type (@sspond) {
				foreach my $pais (@pys) {
					foreach (@{$agences{$pais}}) {
						if ($mach_com_ag{$type}{$_} && $jourmsouv{$_}) {$ponderation += $mach_com_ag{$type}{$_} * $jourmsouv{$_};}
					}
				}
			}

			$ut = (100 * ($sstot[4]))/$ponderation;
		}

		$ut = sprintf("%.1f", $ut);
		
		# % Util
		$affichage[1] .=  qq|<td align="right" nowrap><font class="PT"><b>$ut%</b></font></td>\n|;

		# Co�t
		# Calcul du co�t mensuel (repris plus bas)
		my $couto = 0;
		my $affcouto = 0;

		if ($resc[11]) {
			$couto = $ctaux * $sstot[9] / 100;
		}
		# Calcul du co�t sur la p�riode
		my $couto_periode = $couto / $nbjrmoy * $nbjours_pondere + $sstot[11];
		if ($resc[11]) {
			$couto += $sstot[12];
			$couto = sprintf("%.0f", $couto);
			$affcouto = &fnombre($couto, 3, 0);
		}
		
		# COUT
		$affichage[1] .= qq|
			<td align="right" nowrap><font class="PT" color="red"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">|
			.&fnombre(sprintf("%.0f", $couto_periode), 3, 0).qq|</span></b></font></td>
		|;

		# Ratio 2
		my $ratio2 = ($couto_periode == 0) ? 
			0 : ($sstot[5]) / $couto_periode;
		$ratio2 = sprintf("%.2f",$ratio2);
		
		# Ratio cash
		$affichage[1] .= qq|
			<td align="right" nowrap><font class="PT" color="red"><b>
			$ratio2</b></font></td>
		|;
		
		my $aff  = sprintf("%.0f", $sstot[6]);
		my $aff2 = sprintf("%.1f", $sstot[7]);
		my $aff3 = sprintf("%.0f", $sstot[10]);
		$aff  = &fnombre($aff, 3, 0);
		$aff3 = &fnombre($aff3, 3, 0);
		
		# --- Arr�t� � la fin du mois ---
		# CA
		# CA LOC
		# JR Loc
		$affichage[1] .=  qq|
			<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$aff</span></b></font></td>
			<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$aff3</span></b></font></td>
			<td align="center" nowrap><font class="PT"><b>$aff2</b></font></td>
		|;
		$ut = 0;
		my $ut1 = 0;

		if ($sstot[7]) {
			$ut = ($sstot[6] / $sstot[7]);
			$ut1 = ($sstot[10] / $sstot[7]);
		}

		$ut  = sprintf("%.1f", $ut);
		$ut1 = sprintf("%.1f", $ut1);
		
		# Px Jr
		# Px Jr LOC
		$affichage[1] .=  qq|
			<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$ut</span></b></font></td>
			<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$ut1</span></b></font></td>
		|;
		
		# COUT
		$affichage[1] .=  qq|<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$affcouto</span></b></font></td>\n|;
		my $cjm = 0;
		my $affcjm = 0;

		if ($sstot[8] != 0) {
			$cjm = $couto / ($nbjrmoy * $coef * $sstot[8]);
			$cjm = sprintf("%.1f", $cjm);
			$affcjm = $cjm;
		}

        # CT Jr
		$affichage[1] .=  qq|<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$affcjm</span></b></font></td>\n|;
		my $pc = 0;

		if ($cjm != 0) {$pc = $ut1 / $cjm;}

		$pc = sprintf("%.2f", $pc);
		
		# Ratio prix
		$affichage[1] .=  qq|
			<td align="right" nowrap><font class="PT"><b>$pc</b></font></td>
		</tr>
		|;
		$affichage[1] .= "\n<tr><td colspan=21 align=center><hr></td></tr>\n";
	}

	print $affichage[0].$affichage[1];

  	# totaux
  	print qq|
  	<tr bgcolor="#C6CDC5">
  		<td align="left" nowrap><font class="PT"><b>TOTAUX</b></font></td>
  	|;

	if ($modl) {print "<td>&nbsp;</td>"};

    # Parc
    # --- Nbr contrats ---
    # TOT
    # ARR
    # ACT
	print qq|
		<td align="center" nowrap><font class="PT"><b>$totaux[8]</b></font></td>
		<td align="center" nowrap><font class="PT"><b>$totaux[0]</b></font></td>
		<td align="center" nowrap><font class="PT"><b>$totaux[1]</b></font></td>
		<td align="center" nowrap><font class="PT"><b>$totaux[2]</b></font></td>
        <td align="center" nowrap><font class="PT"><b>$totaux[13]</b></font></td>
	|;
	my $ut=0;

	if ($totaux[8]) {$ut=(100*($totaux[2]+$totaux[13]))/$totaux[8];}

	$ut=sprintf("%.1f",$ut);
	
	# Util. inst.
	print qq|<td align="right" nowrap><font class="PT"><b>$ut%</b></font></td>\n|;
	
	my $aff=sprintf("%.0f",$totaux[3]);
	my $aff2=sprintf("%.1f",$totaux[4]);
	my $aff3=sprintf("%.0f",$totaux[5]);

	if ($aff>0) {
		$aff=&fnombre($aff,3,0);
		$aff3=&fnombre($aff3,3,0);
	}

    # --- Arr�t� aujour J ---
    # CA
    # CA LOC
    # JR Loc
	print qq|
		<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$aff</span></b></font></td>
		<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$aff3</span></b></font></td>
		<td align="center" nowrap><font class="PT"><b>$aff2</b></font></td>
	|;
	$ut=0;

	if (($resc[11])&&($totaux[8])) {
		my $ponderation;

		for(my $i=0;$i<@ordcomm;$i++) {
			my $j = $ordcomm[$i];

			if ($j eq "") {next};
			if (($totaux[8] == 0) && ($totaux[7] == 0)) {next;}
			foreach my $pais (@pys) {
				foreach (@{$agences{$pais}}) {
					if ($mach_com_ag{$j}{$_} && $jourmsouv{$_}) {$ponderation += $mach_com_ag{$j}{$_} * $jourmsouv{$_};}
				}
			}
		}

		if ($ponderation != 0)
		{
            $ut=(100*$totaux[4])/$ponderation;
        }
	}


	$ut=sprintf("%.1f",$ut);
	
	# % Util
	print qq|<td align="right" nowrap><font class="PT"><b>$ut%</b></font></td>\n|;

	# Co�t
	# Calcul du co�t mensuel (repris plus bas)
	my $couto = 0;
	my $affcouto = 0;

	if ($resc[11]) {
		$couto = $ctaux * $totaux[9] / 100;
	}
	# Calcul du co�t sur la p�riode
	my $couto_periode = $couto / $nbjrmoy * $nbjours_pondere + $totaux[11];
	if ($resc[11]) {
		$couto += $totaux[12];
		$couto = sprintf("%.0f", $couto);
		$affcouto = &fnombre($couto, 3, 0);
	}
	
	# COUT
	print qq|
		<td align="right" nowrap><font class="PT" color="red"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">|
		.&fnombre(sprintf("%.0f", $couto_periode), 3, 0).qq|</span></b></font></td>
	|;

	# Ratio 2
	my $ratio2 = ($couto_periode == 0) ? 
		0 : ($totaux[5]) / $couto_periode;
	$ratio2 = sprintf("%.2f",$ratio2);
	
	# Ratio cash
	print qq|
		<td align="right" nowrap><font class="PT" color="red"><b>
		$ratio2</b></font></td>
	|;
	
	my $aff=sprintf("%.0f",$totaux[6]);
	my $aff2=sprintf("%.1f",$totaux[7]);
	my $aff3=sprintf("%.0f",$totaux[10]);
	$aff=&fnombre($aff,3,0);
	$aff3=&fnombre($aff3,3,0);
	
	# --- Arr�t� � la fin du mois ---
	# CA
	# CA LOC
	# JR Loc
	print qq|<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$aff</span></b></font></td>\n|;
	print qq|<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$aff3</span></b></font></td>\n|;
	print qq|<td align="center" nowrap><font class="PT"><b>$aff2</b></font></td>\n|;
	$ut=0;
	my $ut1=0;

	if ($totaux[7]) {
		$ut = ($totaux[6]/$totaux[7]);
		$ut1 = ($totaux[10]/$totaux[7]);
	}

	$ut=sprintf("%.1f",$ut);
	$ut1=sprintf("%.1f",$ut1);
	
	# Px Jr
	# Px Jr LOC
	print qq|<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$ut</span></b></font></td>\n|;
	print qq|<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$ut1</span></b></font></td>\n|;
	
	# COUT
	print qq|<td align="right" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$affcouto</span></b></font></td>\n|;
	my $cjm=0;
	my $affcjm=0;

	if ($totaux[8]!=0) {
		$cjm=$couto/($nbjrmoy*$coef*$totaux[8]);
		$cjm=sprintf("%.1f",$cjm);
		$affcjm=$cjm;
	}

    # CT Jr
	print qq|<td align="center" nowrap><font class="PT"><b><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' $conversion')" onmouseout="UnTip()">$affcjm</span></b></font></td>\n|;
	my $pc=0;

	if ($cjm!=0) {$pc=$ut1/$cjm;}

	$pc=sprintf("%.2f",$pc);
	
	# Ratio prix
	print qq|<td align="right" nowrap><font class="PT"><b>$pc</b></font></td>\n|;
	print qq{</tr>\n};
}

sub statjr1 {
	my ($DB, $debutms, $finms, $pas, $blt) = @_;
	my ($andm, $msdm, $jrdm) = split(/:/, $debutms);
	my $dm = Delta_Days(1900, 1, 1, $andm, $msdm, $jrdm);
	my ($anfm, $msfm, $jrfm) = split(/:/, $finms);
	my $fm = Delta_Days(1900, 1, 1, $anfm, $msfm, $jrfm);
	my $jourmsouv;
	my $jourmois = Delta_Days($andm, $msdm, $jrdm, $anfm, $msfm, $jrfm);
	$jourmsouv = $jourmois;
	$jourmsouv += 1;
	my $ouvrwe = &jour_we($jourmsouv, $jrdm, $msdm, $andm);
	$agences{$pas} = ();
	my $temp = $DB->prepare(qq|select AGAUTO from AUTH.`AGENCE` where PACODE="$pas" and AGAUTO not in ("SIE")|);
	$temp->execute();

	while (my @temp = $temp->fetchrow_array()) {push @{$agences{$pas}}, $temp[0];}

	$temp->finish();
	my $uvfer;

	foreach (@{$agences{$pas}}) {
		$uvfer = &jour_fer_ag_cnx($pas, $_, $jrdm, $msdm, $andm, $jourmsouv-1, $DB);
		$jourmsouv{$_} = $jourmsouv - $ouvrwe - $uvfer;
	}

	$jourmsouv = $jourmsouv - $ouvrwe - $uvfer;
	my $an = $anfm;
	my $ms = $msfm;
	my $ca=0;
	my $forf=0;
	my $mens=0;
	my $cours=0;
	my $arret=0;
	my $tot=0;
	my $transp=0;
	my $carb=0;
	my $ass=0;
	my $durtot=0;
	my @req=();
	my $clause="FACOMAUTO";

	if ($modl) {$clause = qq{CONTRAT.MOMAAUTO};}
	if ($date < $date_chgt_fact{$pas}) {@req = &reqhist($clause, $an, $ms);}
	else {@req = &reqhistnew($clause, $an, $ms);}

	my $sth = $DB->prepare($req[0]);
	$sth->execute();

	while (my @data = $sth->fetchrow_array())
    {
        $data[1] = $data[1] / $tauxE{$pas};

		if ($data[0] eq "") {$data[0] = 0;}

		$comm{$data[0]}->[3] += $data[1];
		$comm{$data[0]}->[6] += $data[1];

		if ($blt) {$ca += $data[1];}
		
		# Pour les machines hors parc, le co�t imput�
		# est �gal au CA / 2
		if ($data[2] == -1 && ($data[3] eq "LOC" || $data[3] eq "REM" || $data[3] eq "REALQ" || $data[3] eq "ASS" || $data[3] eq "GDR" || $data[3] eq "J+" || $data[3] eq "J-")) {
			if ($blt) {
				$comm{$data[0]}->[12] += $data[1] / 2;
				$comm{$data[0]}->[11] += $data[1] / 2;
			}
		}
    }

	$sth->finish;
	my $sth=$DB->prepare($req[1]);
	$sth->execute();
	my %co;

	while (my @data=$sth->fetchrow_array()) {
		if ($data[0] eq "") {$data[0] = 0;}
		if ($data[1] eq "2") {
			$comm{$data[0]}->[2] += $data[2];

			if ($blt) {$cours += $data[2];}
		} else {
			$comm{$data[0]}->[1] += $data[2];

			if ($blt) {$arret += $data[2];}
		}
		if ($blt) {$tot += $data[2];}
		if ($comm{$data[0]}->[0]) {
      	 		$comm{$data[0]}->[0] += $data[2];
			$co{$data[0]} += $data[2];
      	 	} else {
      			$comm{$data[0]}->[0] = $data[2];
			$co{$data[0]} += $data[2];
		}
	}

	$sth->finish;
	my $sth=$DB->prepare($req[5]);
	$sth->execute();

	while (my @data=$sth->fetchrow_array()) {
		if ($data[0] eq "") {$data[0] = 0;}
		if ($co{$data[0]} ne $data[1]) {
      	 		my $diff = $co{$data[0]} - $data[1];
      	 		$comm{$data[0]}->[2] -= $diff;
      	 		$comm{$data[0]}->[0] -= $diff;

			if (($blt)&&($diff)) {
      	 			$cours -= $diff;
      	 			$tot -= $diff;
      	 		}
      	 	}
	}

	$sth->finish;
	my $sth=$DB->prepare($req[6]);
	$sth->execute();

	while (my @data=$sth->fetchrow_array()) {
		if ($data[0] eq "") {$data[0] = 0;}

  	 	$comm{$data[0]}->[2] += $data[1];
  	 	$comm{$data[0]}->[1] -= $data[1];
	}

	$sth->finish;
	my $sth=$DB->prepare($req[2]);
	$sth->execute();

	while (my @data=$sth->fetchrow_array()) {
		if ($data[0] eq "") {$data[0] = 0;}

		$comm{$data[0]}->[4] += $data[1];
		$comm{$data[0]}->[7] += $data[1];
		$durtot += $data[1];
	}

	$sth->finish;
	my $sth=$DB->prepare($req[3]);
	$sth->execute();
	my %contratcode = ();

	while (my @data = $sth->fetchrow_array()) {
		if ($date < $date_chgt_fact{$pas}) {
			if ($data[0] eq "") {$data[0] = 0;}

			my $ags = $data[4];
			my ($anff, $msff, $jrff) = split(/-/, $data[1]);
			my ($anfd, $msfd, $jrfd) = split(/-/, $data[2]);

			my $durft = Delta_Days($anfd, $msfd, $jrfd, $anff, $msff, $jrff);

			if ($durft < 0) {next;}

			my $nbferief = &jour_fer_ag_cnx($pas, $ags, $jrfd, $msfd, $anfd, $durft, $DB);
			my $nbjrwef  = 0;
  			my $joursupf = 0;
  			my $testjoursupf = &num_jour($jrff, $msff, $anff);

			if ($testjoursupf == 6) {$joursupf = 1;}
			if ($testjoursupf == 7) {$joursupf = 2;}

			$durft += 1;

			if ($durft == 1 && $testjoursupf == 7) {$joursupf = 1;}

			$nbjrwef = &jour_we($durft, $jrfd, $msfd, $anfd);
			my $durtemp = $durft;
			$durft = $durft - $nbferief - $nbjrwef + $joursupf;

			if ($comm{$data[0]}->[4]) {
				$comm{$data[0]}->[4] += $durft;
				$comm{$data[0]}->[7] += $durft;
	      	 	} else {
				$comm{$data[0]}->[4] = $durft;
				$comm{$data[0]}->[7] = $durft;
	      	 	}

			$durtot += $durft;

			if ($blt) {
				if (!$contratcode{$data[3]}) {$mens++;$contratcode{$data[3]} = 1;}
				else {$contratcode{$data[3]}++;}
			}
		} else {
			if ($data[0] eq "") {$data[0] = 0;}
			if ($comm{$data[0]}->[4]) {
	      	 		$comm{$data[0]}->[4] += $data[1];
				$comm{$data[0]}->[7] += $data[1];
			} else {
				$comm{$data[0]}->[4] = $data[1];
				$comm{$data[0]}->[7] = $data[1];
			}

			$durtot +=$data[1];

	      	  	if ($blt) {$mens += $data[2];}
	      	  }
	}

	$sth->finish;
	my $sth=$DB->prepare($req[4]);
	$sth->execute();
	my %contratcode = ();

	while (my @data=$sth->fetchrow_array()) {
		if ($date < $date_chgt_fact{$pas}) {
			if ($data[0] eq "") {$data[0] = 0;}

			my $ags = $data[4];
			my ($anff, $msff, $jrff) = split(/-/, $data[1]);
			my ($anfd, $msfd, $jrfd) = split(/-/, $data[2]);

			my $durft = Delta_Days($anfd, $msfd, $jrfd, $anff, $msff, $jrff);

			if ($durft < 0) {next;}

			my $nbferief = &jour_fer_ag_cnx($pas, $ags, $jrfd, $msfd, $anfd, $durft, $DB);
			my $nbjrwef  = 0;
  			my $joursupf = 0;
  			my $testjoursupf = &num_jour($jrff, $msff, $anff);

			if ($testjoursupf == 6) {$joursupf = 1;}
			if ($testjoursupf == 7) {$joursupf = 2;}

			$durft += 1;

			if ($durft == 1 && $testjoursupf == 7) {$joursupf = 1;}

			$nbjrwef = &jour_we($durft, $jrfd, $msfd, $anfd);
			$durft = $durft - $nbferief - $nbjrwef + $joursupf;

			if ($comm{$data[0]}->[4]) {
				$comm{$data[0]}->[4] += $durft;
				$comm{$data[0]}->[7] += $durft;
	      	 	} else {
				$comm{$data[0]}->[4] = $durft;
				$comm{$data[0]}->[7] = $durft;
	      	 	}

			$durtot += $durft;

			if ($blt) {
				if (!$contratcode{$data[3]}) {$mens++;$contratcode{$data[3]}=1;}
				else {$contratcode{$data[3]}++;}
			}
		} else {
			if ($data[0] eq "") {$data[0]=0;}
			if ($comm{$data[0]}->[4]) {
	      	 		$comm{$data[0]}->[4] +=$data[1];
		      		$comm{$data[0]}->[7] +=$data[1];
	      	 	} else {
				$comm{$data[0]}->[4]=$data[1];
				$comm{$data[0]}->[7]=$data[1];
	      	 	}

	      	 	$durtot +=$data[1];

	      		if ($blt) {$forf +=$data[2];}
	      	}
	}

	$sth->finish;
	my $sth = $DB->prepare($req[9]);
	$sth->execute();

	while (my @data=$sth->fetchrow_array())
    {
        $data[1] = $data[1] / $tauxE{$pas};

		if ($data[0] eq "") {$data[0] = 0;}
		if ($comm{$data[0]}->[10]) {
			$comm{$data[0]}->[10] += $data[1];
			$comm{$data[0]}->[5] += $data[1];
		} else {
			$comm{$data[0]}->[10] = $data[1];
			$comm{$data[0]}->[5] = $data[1];
		}

      	 	$caloc += $data[1];
      	 	$calocc += $data[1];
	}

	$sth->finish;

	# carb transp
	if ($blt) {
		my $where = "";
		my $joined = "";

		if ($modl) {
			$where = qq{AND FAMILLEMACHINE.FACOMAUTO="$modl"};
			$joined = qq{LEFT join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO};
		}

		my $req = qq|
		select LIGNEREFARTICLE,
		  SUM(ROUND(ROUND(LIGNEQTITE, |.$tabSageFormats->{'quantity'}.qq|)*ROUND(LIGNEPRIX, |.$tabSageFormats->{'price'}.qq|), |.$tabSageFormats->{'amount'}.qq|)),
		  count(ENTETEFACTURE.ENTETEAUTO)
		FROM ENTETELIGNE
		LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
		LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
		$joined
		WHERE YEAR(DATE)="$an"
		AND MONTH(DATE)="$ms"
		$where
		GROUP BY LIGNEREFARTICLE
		|;
		my $sth=$DB->prepare($req);
		$sth->execute();

		while (my @data=$sth->fetchrow_array())
        {
            $data[1] = $data[1] / $tauxE{$pas};

	    	if ($data[0] =~ /^TRANS/ || $data[0] eq "SERT") {$transp += $data[1];}
			if ($data[0] eq "CARB") {$carb += $data[1];}
		}

		$sth->finish;
     	}

	my $nbdureems = 0;
	my $nbdureeplus = 0;
	# jour<5
	if ($blt) {
     		my $req=$req[7];
     		my $sth=$DB->prepare($req);
		$sth->execute();

		if ($date < $date_chgt_fact{$pas}) {
			while (my @data = $sth->fetchrow_array()) {
				my $ags = $data[5];
				my ($dareel, $dmreel, $djreel) = split(/-/, $data[1]);
				my ($fareel, $fmreel, $fjreel) = split(/-/, $data[2]);
				my $dureereel = Delta_Days($dareel, $dmreel, $djreel, $fareel, $fmreel, $fjreel);
				my $nbferiereel = &jour_fer_ag_cnx($pas, $ags, $djreel, $dmreel, $dareel, $dureereel, $DB);
				my $testjoursupreel = &num_jour($fjreel, $fmreel, $fareel);
				my $joursupreel = 0;

				if ($testjoursupreel == 6) {$joursupreel = 1};
				if ($testjoursupreel == 7) {$joursupreel = 2};

				my ($tan, $tms, $tjr) = split(/\:/, $date);

				if (Delta_Days($tan, $tms, $tjr, $fareel, $fmreel, $fjreel) > 0) {$joursupreel = 0;}

				$dureereel += 1;

				if ($dureereel == 1 && $testjoursupreel == 7) {$joursupreel = 1;}

				my $nbjrwereel = &jour_we($dureereel, $djreel, $dmreel, $dareel);
				$dureereel = $dureereel + $data[3] - $data[4] - $nbferiereel - $nbjrwereel + $joursupreel;

				if ($dureereel < 5) {$nbdureems++;}
			}
		} else {($nbdureems) += $sth->fetchrow_array();}

		$sth->finish;
	}
	# assurance
	if ($blt) {
		my $req = $req[8];
		my $sth = $DB->prepare($req);
		$sth->execute();
		($ass) = $sth->fetchrow_array();
     		$sth->finish;
     	}

	return ($tot, $ca, $transp, $carb, $nbdureems, $nbdureeplus, $cours, $arret, $ass, $mens, $forf, $jourmsouv, $durtot);
}

sub reqhist {
	my ($chp, $an, $ms) = @_;
	my ($db, $dba, $fn, $fna) = &debfinmois($ms, $an);
	my @req = ();
	my $where = "";

	if ($modl) {$where = qq{AND FAMILLEMACHINE.FACOMAUTO = "$modl"}}

	#CA
	$req[0] = qq{
	select $chp, SUM(ENTETEMONTANTFR)
	FROM ENTETEFACTURE
	LEFT join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(ENTETEDATED)="$an"
	AND MONTH(ENTETEDATED)="$ms"
	$where
	GROUP BY $chp
	};

	#NB PAR ETAT
	$req[1] = qq{
	select $chp, LIETCONTRATAUTO, count(DISTINCT ENTETEFACTURE.CONTRATCODE)
	FROM ENTETEFACTURE
	LEFT join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(ENTETEDATED)="$an"
	AND MONTH(ENTETEDATED)="$ms"
	$where
	GROUP BY $chp, LIETCONTRATAUTO
	};

	# duree contrat jour
	$req[2] = qq{
	select $chp, SUM(LIGNEQTITE)
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE="LOC"
	AND ENTETEABO="0"
	$where
	GROUP BY $chp
	};

	# duree et nombre contrat menseul
	$req[3] = qq{
	select $chp, ENTETEDATEF, ENTETEDATED, ENTETEFACTURE.CONTRATCODE, CONTRAT.AGAUTO
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE="LOC"
	AND ENTETEABO="-1"
	AND MOTARIFMSFR !=0
	$where
	};

	# duree et nombre contrat forfait
	$req[4] = qq{
	select $chp, ENTETEDATEF, ENTETEDATED, ENTETEFACTURE.CONTRATCODE, CONTRAT.AGAUTO
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE="LOC"
	AND ENTETEABO="-1"
	AND MOFORFAITFR !=0
	$where
	};

	# Eliminer les doublons arret de contrat entre 2 facturation
	$req[5] = qq{
	select $chp, count(DISTINCT ENTETEFACTURE.CONTRATCODE)
	FROM ENTETEFACTURE
	LEFT join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(ENTETEDATED)="$an"
	AND MONTH(ENTETEDATED)="$ms"
	$where
	GROUP BY $chp
	};

	#contrat en cours de grue
	$req[6] = qq{
	select $chp, count(*)
	FROM CONTRAT
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE CONTRATOK="-2"
	AND CONTRATDD<="$fn"
	AND CONTRATDR>"$fn"
	$where
	GROUP BY $chp
	};

	 #nb contrat<5
	$req[7] = qq{
	select $chp, CONTRATDD, CONTRATDR, CONTRATJOURSPLUS, CONTRATJOURSMOINS, CONTRAT.AGAUTO
	FROM CONTRAT
	left join MONTANT on MONTANT.MOAUTO=CONTRAT.MOAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE CONTRATOK in ("-1","-2")
	AND LIETCONTRATAUTO != 6
	AND CONTRATDD<="$fn"
	AND CONTRATDR>="$db"
	and MOTARIFMSFR = 0
	$where
	};

	$req[8] = qq{
	select count(DISTINCT ENTETEFACTURE.CONTRATCODE)
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE="ASS"
	$where
	};

	$req[9] = qq|
	SELECT $chp,
	   SUM(ROUND(ROUND(LIGNEQTITE, |.$tabSageFormats->{'quantity'}.qq|)*ROUND(LIGNEPRIX, |.$tabSageFormats->{'price'}.qq|), |.$tabSageFormats->{'amount'}.qq|)),
	   count(distinct ENTETEFACTURE.ENTETEAUTO)
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE in ("LOC", "ASS", "GDR", "REM")
	$where
	GROUP BY $chp
	|;

	return @req;
}

sub reqhistnew {
	my ($chp, $an, $ms) = @_;
	my ($db, $dba, $fn, $fna) = &debfinmois($ms, $an);
	my @req = ();
	my $where = "";

	if ($modl) {$where = qq{AND FAMILLEMACHINE.FACOMAUTO = "$modl"}}

	#CA
	$req[0] = qq|
	select $chp,
	   SUM(ROUND(ROUND(LIGNEPRIX, |.$tabSageFormats->{'price'}.qq|)*ROUND(LIGNEQTITE, |.$tabSageFormats->{'quantity'}.qq|), |.$tabSageFormats->{'amount'}.qq|)),
	   CONTRAT.CONTRATHORSPARC, LIGNEREFARTICLE
	from ENTETELIGNE
	left join ENTETEFACTURE on ENTETELIGNE.ENTETEAUTO=ENTETEFACTURE.ENTETEAUTO
	left join LIGNEFACTURE on LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	left join CONTRAT on ENTETEFACTURE.CONTRATAUTO=CONTRAT.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where YEAR(DATE)="$an"
	and MONTH(DATE)="$ms"
	$where
	group by $chp, CONTRAT.CONTRATHORSPARC
	|;

	#NB PAR ETAT
	$req[1] = qq{
	select $chp, LIETCONTRATAUTO, count(DISTINCT ENTETEFACTURE.CONTRATCODE)
	from ENTETELIGNE
	left join ENTETEFACTURE on ENTETELIGNE.ENTETEAUTO=ENTETEFACTURE.ENTETEAUTO
	left join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where YEAR(DATE)="$an"
	and MONTH(DATE)="$ms"
	$where
	group by $chp, LIETCONTRATAUTO
	};

	# duree contrat jour
	$req[2] = qq{
	select $chp, SUM(LIGNEQTITE)
	from ENTETELIGNE
	left join LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	left join ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	left join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE in ("LOC", "J+", "J-")
	AND ENTETEABO="0"
	$where
	group by $chp
	};

	# duree et nombre contrat menseul
	$req[3] = qq{
	select $chp, SUM(LIGNEQTITE), count(distinct ENTETEFACTURE.CONTRATCODE)
	from ENTETELIGNE
	left join LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	left join ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	left join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	left join MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE in ("LOC", "J+", "J-")
	AND ENTETEABO="-1"
	AND MOTARIFMSFR !=0
	$where
	GROUP BY $chp
	};

	# duree et nombre contrat forfait
	$req[4] = qq{
	select $chp, SUM(LIGNEQTITE), count(distinct ENTETEFACTURE.CONTRATCODE)
	from ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE in ("LOC", "J+", "J-")
	AND ENTETEABO="-1"
	AND MOFORFAITFR !=0
	$where
	GROUP BY $chp
	};

	#eliminer les doublons arret de contrat entre 2 facturation
	$req[5] = qq{
	select $chp, count(DISTINCT ENTETEFACTURE.CONTRATCODE)
	from ENTETELIGNE
	left join ENTETEFACTURE on ENTETELIGNE.ENTETEAUTO=ENTETEFACTURE.ENTETEAUTO
	left join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	$where
	GROUP BY $chp
	};

	#contrat en cours de grue
	$req[6] = qq{
	select $chp, count(*)
	FROM CONTRAT
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE CONTRATOK="-2"
	AND CONTRATDD<="$fn"
	AND CONTRATDR>"$fn"
	$where
	GROUP BY $chp
	};

	 #nb contrat<5
	$req[7] = qq{
	select count(distinct ENTETEFACTURE.CONTRATCODE)
	from ENTETELIGNE
	left join ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	left join CONTRAT ON CONTRAT.CONTRATCODE=ENTETEFACTURE.CONTRATCODE
	left join MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	where YEAR(DATE)="$an"
	and MONTH(DATE)="$ms"
	and CONTRATOK in (-1, -2)
	and MOTARIFMSFR = 0
	and CONTRATDUREE<5
	$where
	};

	$req[8] = qq{
	select count(DISTINCT ENTETEFACTURE.CONTRATCODE)
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT join CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE="ASS"
	$where
	};

	$req[9] = qq|
	SELECT $chp,
	   SUM(ROUND(ROUND(LIGNEQTITE, |.$tabSageFormats->{'quantity'}.qq|)*ROUND(LIGNEPRIX, |.$tabSageFormats->{'price'}.qq|), |.$tabSageFormats->{'amount'}.qq|)),
	   count(distinct ENTETEFACTURE.ENTETEAUTO)
	FROM ENTETELIGNE
	LEFT JOIN LIGNEFACTURE ON LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	LEFT JOIN ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
	LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
	LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
	LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	LEFT join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
	WHERE YEAR(DATE)="$an"
	AND MONTH(DATE)="$ms"
	AND LIGNEREFARTICLE in ("LOC", "REM", "ASS", "GDR", "J+", "J-")
	$where
	GROUP BY $chp
	|;

	return @req;
}

sub hissstatistique {
	my @data = split(/\:/, $date);
	my ($db, $dba, $fn, $fna) = &debfinmois($data[1], $data[0]);

	for(my $j=0;$j<@pys;$j++) {
		my @rs = &statjr1($BDC[$j], $db, $date, $pys[$j], 1);

		for(my $k=0;$k<@rs;$k++) {
			if (($k == 11) && $j) {next};

			$rest[$k] += $rs[$k];
		}
	}
	for(my $i=0;$i<@rest;$i++) {push(@resc, $rest[$i]);}
}

sub debfinmois {
	my ($mois, $an) = @_;
	my $debutms = "$an:$mois:01";
	my $debaff  = "01/$mois/$an";
	my $finmois = Days_in_Month($an, $mois);
	my $finms   = "$an:$mois:$finmois";
	my $finaff  = "$finmois/$mois/$an";

	return ($debutms, $debaff, $finms, $finaff);
}

sub jourdate {
	my @param = @_;
	my $base = $_[0] ? $_[0] : $DB;
	my $where2 = "";
	my $htlm   = "";
	$htlm .= $cgi->start_form;
	$htlm .= $cgi->hidden("paysauto", $pacode);
	$htlm .= qq{<select name="date" onchange="submit()">\n};
	$htlm .= qq{<option value="">$mess[5024]\n};
	my $req = qq{
        SELECT MONTH(DATE), YEAR(DATE)
        FROM ENTETELIGNE
        LEFT join ENTETEFACTURE ON ENTETEFACTURE.ENTETEAUTO=ENTETELIGNE.ENTETEAUTO
        WHERE DATE < CONCAT_WS(":",YEAR(NOw()),MONTH(NOw()),"01")
            AND DATE >= "$tdt[0]-$tdt[1]-$tdt[2]"
        GROUP BY MONTH(DATE),YEAR(DATE) ORDER BY DATE DESC};
	my $sth = $DB->prepare($req);
	$sth->execute();

	while (my @data = $sth->fetchrow_array()) {
		my $selected = "";
		my ($db, $dba, $fn, $fna) = &debfinmois($data[0], $data[1]);

		if ($fn eq $date) {$selected = " selected";};

		$htlm .= qq{<option value="$fn"$selected>$dba -- $fna\n};
	}

	$sth->finish;
	$htlm .= qq{</select>\n};
	$htlm .= qq{</form>\n};
	return $htlm;
}

sub retour_date {
	my %lepays;
	$lepays{"FR"} = "2004:01:01";
	$lepays{"PT"} = "2004:03:01";
	$lepays{"ES"} = "2004:03:01";

	return \%lepays;
}
