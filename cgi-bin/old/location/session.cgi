#!/usr/bin/perl

use lib '../lib';

use DBI;
use CGI;
use strict;
use accesbd;
use varglob;
use message;
use autentif;
use tracetemps;

# D�lai de rafraichissement en millisecondes
my $refresh = 30;
$refresh *= 1000;

my $DB = &Environnement("LOCATION");
my $cgi = new CGI;
$cgi->autoEscape(undef);
my @mess = &message($G_PAYS);
$| = 1;

# Initialisation du chrono
my $timer = &init_chrono();

# D�but de la page HTML
my $titre = qq{Sessions en cours};
my $cgi = new CGI;
$cgi->autoEscape(undef);
&ente($titre, $titre, $G_PENOM, $G_PEPRENOM);

print qq{
<P>
<DIV id="boite" style="display: block; text-align: center;"></DIV>
</P>

<SCRIPT type="text/javascript">
// Gestion des blocs
var ie4 = (document.all) ? true : false;
var ns6 = (document.getElementById&&!document.all) ? true : false;

function agrandir(num) {
    var objet = document.getElementById('imp' + num);
    var img = document.getElementById('img' + num);
    objet.style.display = 'block';
    img.src = '$URL_IMG/reduire.gif';
}

function reduire(num) {
    var objet = document.getElementById('imp' + num);
    var img = document.getElementById('img' + num);
    objet.style.display = 'none';
    img.src = '$URL_IMG/agrandir.gif';
}

function bloc(num) {
    var objet = document.getElementById('imp' + num);
    if (objet.style.display == 'none') {
        agrandir(num);
    }
    else {
        reduire(num);
    }
}

function afficherbulle(ev, text) {
 	var x = (ie4) ? ev.x : ev.pageX;
	var y = (ie4) ? ev.y : ev.pageY;
	document.getElementById('bulle').innerHTML = text;
    document.getElementById('bulle').style.left = eval(x) + 'px';
    document.getElementById('bulle').style.top = eval(y + 10) + 'px';
    document.getElementById('bulle').style.visibility = 'visible';
}

function cacherbulle() {
    document.getElementById('bulle').style.visibility = 'hidden';
}


// XMLHttpRequest
function reload_sessions() {
    var xhr_object = null;
    // Firefox
    if(window.XMLHttpRequest)
       xhr_object = new XMLHttpRequest();
    // Internet Explorer
    else if(window.ActiveXObject)
       xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
    // XMLHttpRequest non support� par le navigateur
    else {
       alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
       return;
    }

    document.getElementById('boite').innerHTML = "Chargement...";
    xhr_object.open("GET", "recup_session.cgi", true);
    xhr_object.setRequestHeader("Cache-Control", "no-cache, must-revalidate");
    xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr_object.setRequestHeader("Content-type", "text/html; charset=iso-8859-15");
    xhr_object.setRequestHeader("Content-Transfer-Encoding", "charset=iso-8859-15");

    xhr_object.onreadystatechange = function() {
        if (xhr_object.readyState == 4) document.getElementById('boite').innerHTML = xhr_object.responseText;
    }

    xhr_object.send(null);
    setTimeout("reload_sessions()", $refresh);
}

reload_sessions();
</SCRIPT>

};

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

# Pied de la page HTML
&Pied($delta_time);
