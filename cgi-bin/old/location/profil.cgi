#!/usr/bin/perl

# $Id: profil.cgi,v 1.1.1.1 2006/08/01 07:48:10 clliot Exp $
# $Log: profil.cgi,v $
# Revision 1.1.1.1  2006/08/01 07:48:10  clliot
# no message
#
# Revision 2.4  2006/02/06 16:52:47  julien
# V. 1.4 : Tarification v2
#
# Revision 2.3.2.1  2005/12/13 15:46:42  julien
# no message
#
# Revision 2.3.4.1  2005/12/13 10:31:54  julien
# Inclusion du r�pertoire lib dans chaque fichier.
#
# Revision 2.3  2005/08/26 12:10:57  julien
# Pr�fixage des tables de AUTH dans les requ�tes.
#
# Revision 2.2  2005/05/03 16:51:23  julien
# Tra�age du temps d'ex�cution des scripts.
#
# Revision 2.1  2005/05/03 14:24:56  julien
# Tra�age du temps d'ex�cution des scripts.
#
# Revision 2.0  2004/09/16 15:07:32  julien
# Version r�cup�r�e de Lancelot et nettoy�e
#
# Revision 1.2  2004/09/16 14:23:13  julien
# Ajout des identifiants
#

use lib '../lib';

use strict;

use CGI;
use autentif;
use calculdate;
#use securite;
use titanic;
#use accesbases;
use accesbd;
use varglob;
use Date::Calc qw(Day_of_Week Delta_Days Add_Delta_Days);
use message;
use tracetemps;

# Initialisation du chrono
my $timer = &init_chrono();


my $DB=&Environnement("LOCATION");
#&authentification(&login);
#my $pe_login = &login;

#initialisation CGI
my $cgi=new CGI;
$cgi->autoEscape(undef);

#connexion

$|=1;


my $fich=$cgi->param('menu');



my $titre=qq{Gestion Profil};
#if ($fich eq "menu")	 
#	{
#	$titre=qq{Cr�ation Menu};
#	}



&ente($titre, $titre, $G_PENOM, $G_PEPRENOM);
#if ($fich eq "menu")	 
#	{
#	if ($cgi->param('creation.x') !=0)
#		{
#		&insert_menu;
#		} 
#	&crea_menu;
#	}

if ($fich eq "profil")	 
	{
	if ($cgi->param('creation.x') !=0)
		{
		&insert_profil;
		} 
	&crea_profil;
	}


$DB->disconnect;

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

# Pied de la page HTML
my $paramMenu = $cgi->param("menu");
&Pied($delta_time, $paramMenu);


#
# creation de logiciel
#

sub crea_profil {
my $profil=$cgi->param('profil');
if ($cgi->param('nvpro.x')!=0) {$profil=&nv_profil;}
my $menu=$cgi->param('admenu');
print qq{<table width="100%" border="0" cellspacing="0" cellpadding="0">\n};
print $cgi->start_form;
print $cgi->hidden('menu',$fich);
#
# Libelle Profil
#

print qq{<tr>\n};
print qq{<td align="right" colspan="2"><b>Libelle Profil:</b></td>\n};
print qq{<td align="left" colspan="2">\n};
print qq{<select name="profil">\n};
my $req=qq{select PROFILAUTO,PROFILLIBELLE FROM AUTH.`PROFIL` ORDER BY PROFILAUTO};
my $sth=$DB->prepare($req);
$sth->execute();
my $nbrow=$sth->rows;
print qq{<option value="0">$nbrow choix\n};
while (my @data=$sth->fetchrow_array()) 	
	{
	my $selected="";
	if ($profil eq $data[0]) {$selected=qq{ selected}};
	print qq{<option value="$data[0]"$selected>$data[0], $data[1]\n};
	}
$sth->finish;
print qq{</select>\n};


print qq{\n</td>\n};

print qq{<tr>\n};
#nouveau profil
print qq{<tr>\n};
print qq{<td align="right" colspan="2"><b>Ou Nouveau Profil:</b></td>\n};
print qq{<td align="left" colspan="2">\n};
print $cgi->textfield(-name=>"nvprof",-maxlength=>25,-size=>25);
print $cgi->image_button(-name=>'nvpro', -src=>"$URL_IMG/newclient_off.gif",-alt=>'Nouveau profil');
print qq{\n</td>\n};
print qq{<tr>\n};


#
# menu
#

print qq{<tr>\n};

print qq{<td align="right" colspan="2"><b>Menu:</b>\n</td>};
print qq{<td align="left" colspan="2">\n};
print qq{<select name="admenu">\n};
my $req=qq{select MEAUTO,LONOM,MEFONCTION FROM MENU  LEFT join LOGICIEL ON MENU.LOAUTO=LOGICIEL.LOAUTO ORDER BY LONOM,MEFONCTION};
my $sth=$DB->prepare($req);
$sth->execute();
my $nbrow=$sth->rows;
print qq{<option value="0">$nbrow choix\n};
while (my @data=$sth->fetchrow_array()) 	
	{
	my $selected="";
	if ($menu eq $data[0]) {$selected=qq{ selected}};
	print qq{<option value="$data[0]"$selected>$data[0], $data[1], $data[2]\n};
	}
$sth->finish;
print qq{</select>\n};


print qq{\n</td>\n};
print qq{\n</tr>\n};

print qq{<tr>\n};
print qq{<td colspan="4" align="center">\n}; 
print $cgi->image_button(-name=>'lst_menu', -src=>"$URL_IMG/att_mach.gif",-alt=>'Affiche Menu');
print qq{</td>\n</tr>\n}; 
 
if ($cgi->param('lst_menu.x') !=0) 
	{
	print $cgi->hidden("lst_menu.x");
	print qq{<tr>\n};
	print qq{<td align="right"  colspan="2"><b>Ordre du Nouveau:</b>\n<td align="left"  colspan="2">}; 
	print $cgi->textfield(-name=>"nvordre",-maxlength=>5,-size=>5);
	print qq{</td>\n</tr>\n};
	
	print qq{<tr>\n};
	print qq{<td colspan="2" align="center">\n}; 
	print qq{&nbsp};
	print qq{</td>\n</tr>\n};
	
	my $req=qq{select MEPR.MEAUTO,PROFILAUTO,MEFONCTION,PROFILORDRE,LOAUTO FROM AUTH.`MEPR`  LEFT join MENU ON MENU.MEAUTO=MEPR.MEAUTO WHERE PROFILAUTO="$profil" ORDER BY PROFILORDRE};
	my $sth=$DB->prepare($req);
	$sth->execute();
	my $nbrow=$sth->rows;
	my $loauto=0; 
	my $cp=0;
	while (my @data=$sth->fetchrow_array()) 	
		{
		my $div=$cp % 2;
		if (($div==0)||($loauto ne $data[4]))
			{
			if (($cp !=0)&&($loauto ne $data[4])) 
				{
				print qq{</tr>\n};
				print qq{<tr><td colspan="4"><hr></td></tr>\n};
				$loauto=$data[4];
				}
			 if ($cp ==0) 
				{				
				$loauto=$data[4];
				}  
			print qq{<tr>\n};
			}
		print qq{<td align="right"><b>$data[2]</b>\n</td>\n<td align="left">};
		print $cgi->textfield(-name=>"ordre$cp",-maxlength=>5,-size=>5,value=>"$data[3]");
		print $cgi->checkbox(-name=>"ok$cp",-label=>'', alt=>"Modif ordrede $data[2]");
		print $cgi->hidden("menu$cp",$data[0]);		
		print qq{\n</td>\n};
		if ($div==1) 
			{
			print qq{</tr>\n};
			}		
		$cp++;
		}
	$sth->finish;
	print $cgi->hidden("nb",$cp);
	
	
	print qq{<tr>\n};
	print qq{<td colspan="4" align="center">\n}; 
	print qq{&nbsp};
	print qq{</td>\n</tr>\n};
	print qq{<tr>\n};
	print qq{<td colspan="4" align="center">\n}; 
	print $cgi->image_button(-name=>'creation', -src=>"$URL_IMG/valid.gif",-alt=>'Creer Menu');
	print qq{</td>\n</tr>\n}; 
	}






				
print qq{</form>\n};
print qq{</table>\n};
}



#
# creation/modif profil
#

sub insert_profil {
	
my $profil=$cgi->param('profil');
my $menu=$cgi->param('admenu');
my $nvordre=$cgi->param('nvordre');
my $nb=$cgi->param('nb');
for(my $i=0;$i<$nb;$i++)
	{
	my $ordr=$cgi->param("ordre$i");
	my $mn=$cgi->param("menu$i");
	if ($cgi->param("ok$i") ne "on") {next};
	my $update=qq{UPDATE AUTH.`MEPR` SET PROFILORDRE="$ordr" WHERE MEAUTO="$mn" AND PROFILAUTO="$profil"};
	
	$DB->do($update);
	print qq{<center><font class="GD" color="red"><b>Le Menu est modifi�</b></font></center>\n};
	}
if (($menu ne "")&&($nvordre ne ""))
	{
	my $ins=qq{INSERT INTO AUTH.`MEPR` (PROFILORDRE,MEAUTO,PROFILAUTO) values ("$nvordre" , "$menu" ,"$profil")};
	
	
	$DB->do($ins);
	print qq{<center><font class="GD" color="red"><b>Le Menu est inser�</b></font></center>\n};
	}

}
#creation nv profil
sub nv_profil 
	{
	my $nopro=$cgi->param('nvprof');
	$DB->do(qq{INSERT INTO AUTH.`PROFIL` (PROFILLIBELLE) values ("$nopro")});
	my $ins=$DB->{mysql_insertid};
	return $ins;
	}
	



