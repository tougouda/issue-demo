#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';

use CGI;
use autentif;
use accesbd;
use varglob;
use message;
use fonctions;
use calculdate;
use titanic;
use tracetemps;
use LOC::Alert;

# Initialisation du chrono
my $timer = &init_chrono();


#connexion
my $DB=&Environnement("LOCATION");

#initialisation CGI
my $cgi=new CGI;
$cgi->autoEscape(undef);
my @mess=&message($G_PAYS);

$|=1;

#type / menu
my $type=$cgi->param('type');

#Variables
my $titre=$mess[5050];
my @libcol;
my @typcol;

$nbcol=9;

for (my $i=0;$i<$nbcol;$i++) {
	$libcol[$i]=$mess[$i+5051];
}
$typecol[6]="date";
$typecol[7]="date";
$typecol[8]="valid";

&ente($titre, $titre, $G_PENOM, $G_PEPRENOM.", ".$G_AGENCE);

if ($type eq "envoi") {
	$crit_ag = qq{C.AGAUTO="$G_AGENCE" AND };
	$champ_bc = "BCENVOI";
} elsif ($type eq "reception") {
	$crit_ag = qq{};
	$champ_bc = "BCRECU";
};

## Requ�tes
$req_update=qq{update CONTRAT set champ_bc="-1" where CONTRATAUTO=id_cont};
$req_select=qq{SELECT C.CONTRATCODE, VC.VINOM, CL.CLSTE, CL.CLTEL, MM.MOMADESIGNATION, M.MANOPARC, C.CONTRATDD, C.CONTRATDR, C.CONTRATAUTO, C.BCENVOI FROM CONTRAT C LEFT JOIN CLIENT CL ON CL.CLAUTO=C.CLAUTO LEFT JOIN AUTH.`MODELEMACHINE` MM ON MM.MOMAAUTO=C.MOMAAUTO LEFT JOIN AUTH.`MACHINE` M ON M.MAAUTO=C.MAAUTO LEFT JOIN CHANTIER CH ON CH.CHAUTO=C.CHAUTO LEFT JOIN VILLECHA VC ON VC.VILLEAUTO=CH.VILLEAUTO WHERE $crit_ag BCJOIN="-1" AND CONTRATOK="-1" AND champ_bc="0" AND CONTRATDR> "2004-01-01" AND C.ETCODE <> 'CON06' ORDER BY CLSTE, CONTRATDR;};

$req_update =~ s/champ_bc/$champ_bc/;
$req_select =~ s/champ_bc/$champ_bc/;

if ($cgi->param('valider.x') != 0) {
	foreach $key ($cgi->param) {
		if ($key =~ /valid/) {
		## R�cup�ration du costatiauto
			($id_cont) = ($key =~ /^\w*\.(\d*)/);
			if($id_cont ne "")
			{
    			$req_updt = $req_update;
    			$req_updt =~ s/id_cont/$id_cont/;
    			$count = $DB->do($req_updt);
    			if ($count == 1) {
    				if ($cgi->param('type') eq "envoi") {
    					alerte_pl_ms($G_AGENCE, $DB, LOC::Alert::ORDERCOMPULSORYTOPROVIDE, -1);
    				} elsif ($cgi->param('type') eq "reception") {
    					alerte_pl_ms("ALL".$G_PAYS, $DB, LOC::Alert::ORDERCOMPULSORYTOTORECEIVE, -1);
    				};
    			};
			}
		};
	}	
};

my $sth=$DB->prepare($req_select);
$sth->execute();
$count = $sth->rows;

$ligne_titre = qq{<tr bgcolor="#C6CDC5">\n};
for (my $i=0;$i<$nbcol;$i++) {
	$ligne_titre .= qq{<th>$libcol[$i]</th>\n};
}
$ligne_titre .= qq{</tr>\n};

print $cgi->start_form(-method=>'get', action=>'bon_comm.cgi');
print $cgi->hidden("type");
print qq{<table width="100%" border="0" cellspacing="2" cellpadding="0">};
my $cpt_ligne=0;
if ($count > 0) {
	while (my @data=$sth->fetchrow_array()) {
		my $type_ligne=$cpt_ligne % 2;
		my $color="''";
		if ($type_ligne == 1) {$color=qq{'#D6D6D6'};}
		my $ligne_titre_on = $cpt_ligne % 20;
		if ($ligne_titre_on == 0) {
			print qq{$ligne_titre};
		}
		print qq{<TR bgColor=$color onmouseover="bgColor='#FFCC33'" onmouseout="bgColor=$color">\n};
		for ($i=0; $i<@data-1; $i++) {
			if ($typecol[$i] eq "date") {
				$data[$i]=&format_datetime($data[$i]);
			};
			if ($typecol[$i] eq "valid") {
				$data[$i]=$cgi->checkbox(-name=>"valid\.$data[$i]", -label=>'');
			};
			print qq{<td>$data[$i]</td>\n};
		};
		print qq{</tr>\n};
		$cpt_ligne++;
	}

	print qq{</table>\n};

	print "<center>";
	print $cgi->image_button(-name=>'valider', -src=>"$URL_IMG/valid_off.gif", -align=>'center', -border=>0, -alt=>$mess[58]);
	print "</center>\n";
};

$sth->finish();

$DB->disconnect;
print qq{</table>\n};

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

# Pied de la page HTML
my $paramType = $cgi->param("type");
&Pied($delta_time, $paramType);

