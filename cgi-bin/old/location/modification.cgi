#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';

use CGI;
use autentif;
use calculdate;
use titanic;
use accesbd;
use varglob;
use Date::Calc qw(Delta_Days Add_Delta_Days check_date Add_Delta_YM);
use message;
use infobase;
use fonctions;
use tracetemps;
use LOC::Characteristic;
use LOC::Globals;
use LOC::Request;

# Initialisation du chrono
my $timer = &init_chrono();

#
# Environnement
#
my $DB = &Environnement("LOCATION");
my $cgi = new CGI;
$cgi->autoEscape(undef);
my @mess = &message($G_PAYS);

# Titre
my $titre;
my $nblignes_aff = 20;

$|=1;


my @chmprech;
my $nbchamp;
my $nbrech;
my @reqrech;
my @optrech;
my @lienopt;
my $nbform;
my @nameform;

# Consultation Contrat
$titre=$mess[2850];

# Formul
$nameform[0]="contrat";
$nameform[1]="client";
$nameform[2]="machine";
$nameform[3]="chantier";
$nameform[4]="chantier";
$nameform[5]="dep";
$nameform[6]="etat";

$nbform=7;

$reqrech[0]=qq{
SELECT
    CONTRAT.CONTRATAUTO AS `id`,
    CONTRATCODE AS `code`,
    CLSTE AS `customer.label`,
    CHLIBELLE AS `site.label`,
    VICP AS `site.postalCode`,
    VINOM AS `site.city`,
    CHCONTACT AS `site.contact`,
    CONTRATCONTACTCOMM AS `order.contactName`,
    MOMADESIGNATION AS `model.label`,
    CONCAT_WS("",MANOPARC_AUX,MANOPARC) AS `machine.parkNumber`,
    CONTRATDD AS `beginDate`,
    CONTRATDR AS `endDate`,
    CONTRATDUREE AS `duration`,
    MOPXJOURFR AS `unitPrice`,
    MOTARIFMSFR AS `amount.monthlyPrice`,
    MOASSFR AS `insurance.amount`,
    MORECOURS AS `appeal.amount`,
    MOTRANSPORTDFR AS `delivery.amount`,
    MOTRANSPORTRFR AS `recovery.amount`,
    CAQTE AS `fuel.quantity`,
    MONETTOYAGE AS `cleaning.amount`,
    MOSERVICE AS `service.amount`,
    MOHTFRS AS `total.amount`,
    LIETCONTRATLIBELLE AS `state.label`,
    CONTRATNUMCOMM AS `order.no`,
    CONTRATNOTE AS `note`,
    CONTRAT.CLAUTO AS `customer.id`,
    CHTELEPHONE AS `site.telephone`,
    MOFORFAITFR AS `amount.fixedPrice`,
    CONTRATCOMMENTAIRE AS `comment`
FROM CONTRAT
LEFT JOIN CLIENT ON CLIENT.CLAUTO=CONTRAT.CLAUTO
LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
LEFT JOIN CHANTIER ON CHANTIER.CHAUTO=CONTRAT.CHAUTO
LEFT JOIN VILLECHA ON VILLECHA.VILLEAUTO=CHANTIER.VILLEAUTO
LEFT JOIN AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
LEFT JOIN AUTH.`MACHINE` ON MACHINE.MAAUTO=CONTRAT.MAAUTO
LEFT JOIN LIETCONTRAT ON LIETCONTRAT.LIETCONTRATAUTO=CONTRAT.LIETCONTRATAUTO
LEFT JOIN CARBURANT ON CARBURANT.CARBUAUTO=CONTRAT.CARBUAUTO
WHERE CONTRAT.AGAUTO ="agence"
    AND CONTRATOK="-1"};
$nbopt=7;
$optrech[0]=qq{CONTRAT.CONTRATCODE = "ident"};
$lienopt[0]=qq{contrat};
$optrech[1]=qq{(CLIENT.CLSTE LIKE "ident%" or CLIENT.CLAUTO = "ident")};
$lienopt[1]=qq{client};
$optrech[2]=qq{MACHINE.MANOPARC = "ident"};
$lienopt[2]=qq{machine};
$optrech[3]=qq{CHANTIER.CHLIBELLE="ident"};
$lienopt[3]=qq{chantier};
$optrech[4]=qq{VILLECHA.VICP LIKE "ident%"};
$lienopt[4]=qq{dep};
$optrech[5]=qq{CONTRATDD>="ident:01:01"};
$lienopt[5]=qq{annee};
$optrech[6]=qq{LIETCONTRATLIBELLE="ident"};
$lienopt[6]=qq{etat};
$nbrech=1;
$nbchamp = 26;

for(my $i = 0; $i < $nbchamp; $i++)
{
    $chmprech[$i] = $mess[$i+2851];
}

for (my $i = 0; $i < @reqrech; $i++)
{
    $reqrech[$i] =~ s/agent/$G_PEAUTO/eg;
    $reqrech[$i] =~ s/agence/$G_AGENCE/eg;
}

# Entete
&ente($titre, $titre, $G_PENOM, $G_PEPRENOM);

#
# Programme Principal
#
&liste_conscont();

$DB->disconnect;
print qq{
</table>
};

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);


# Pied de la page HTML
my $paramType = $cgi->param("type");
my $paramMenu = $cgi->param("menu");
&Pied($delta_time, "$G_PENOM;$G_AGENCE;" . &nom_fichier() . ";" . $paramType . ";" . $paramMenu);



sub liste_conscont {
    print qq{
    <table border="0" STYLE="font-family:Arial;font-size:8pt" WIDTH="95%">
    <font class="PT">
    };

    for($i = 0; $i < $nbform; $i++)
    {
        for($j = 0; $j < $nbopt; $j++)
        {
            if (($nameform[$i] eq $lienopt[$j]) && (not($cgi->param("$nameform[$i]") eq "")))
            {
                $optrech[$j] =~ s/ident/$cgi->param("$nameform[$i]")/eg;
            };
        }
    }

    my $opt="";

    for($j = 0; $j < $nbopt; $j++)
    {
        if ($optrech[$j] =~ /ident/)
        {
            $optrech[$j] = "";
        };
    }
    for($j = 0; $j < $nbopt; $j++)
    {
        if (not($optrech[$j] eq ""))
        {
            $opt .=" AND ";
            $opt .=$optrech[$j];
        }
    }
    if (not($opt eq ""))
    {
        for($n = 0; $n < $nbrech;$n++)
        {
            $reqrech[$n] .= $opt;
        }
    }
    for($n = 0; $n < $nbrech; $n++)
    {
        if (defined($cgi->param('number')))
        {
            $nbcons = $cgi->param('number');
        }
        else
        {
            $nbcons = 0;
            $cgi->hidden('number', $nbcons);
        }

        $reqrech[$n] .=qq{ ORDER BY CONTRATDD DESC LIMIT $nbcons, 20};
        $reqtest = $reqrech[$n];
        $reqtest =~ s/(.|\n)+FROM/SELECT COUNT(CONTRAT.CONTRATAUTO) FROM/;
        $reqtest =~ s/LIMIT.+//;
        $reqtest =~ s/ORDER.+//;
        $sth=$DB->prepare("$reqtest");
        $sth->execute();
        $nbresult=$sth->fetchrow_array();
        $sth->finish;

        if ($nbresult == 0) {
            print $cgi->start_form(-method=>'get', action=>'modification.cgi');
            qq{
            <center>
            <tr>
                <td><p align="center"><font color="red" size=2><b>$mess[151]</b></font></p></td>
            </tr>
            </center>
            </form>
            };

        }
        else
        {
            # Plusieurs Contrat
            $sth = $DB->prepare("$reqrech[$n]");
            $sth->execute();
            print $cgi->start_form(-method=>'get', action=>'modification.cgi', name=>"pagination");
            $cgi->delete('number');
            $nb2 = 0;
            $nbpage = $nbresult/$nblignes_aff > int($nbresult/$nblignes_aff) ? int($nbresult/$nblignes_aff) + 1 : int($nbresult/$nblignes_aff);
            print "<center>";
            $popupmenu = qq|<SELECT NAME="number" onChange="pagination.submit()">\n|;

            for ($i = 1; $i <= $nbpage; $i++)
            {
                if ($nbcons == $nb2)
                {
                    $select = "selected";
                }
                else
                {
                    $select = "";
                }

                $popupmenu.= qq{<OPTION VALUE="$nb2" $select>page $i\n};
                $nb2 += $nblignes_aff;
            }

            $popupmenu.= qq|</SELECT>\n|;
            print $popupmenu;
            print "<br>";

            for($k = 0; $k < $nbform; $k++)
            {
                my $nfo = $cgi->param("$nameform[$k]");
                print $cgi->hidden("$nameform[$k]", $nfo);
            }

            print qq{
            </form>
            <tr>
                <font class="PT">
                <td bgcolor=C6CDC5><font class="PT"><b>$chmprech[1]<BR>$chmprech[23]</b></font></td>
                <td bgcolor=C6CDC5><font class="PT"><b>$chmprech[2]<BR>$chmprech[24]</b></font></td>
                <td bgcolor=C6CDC5><font class="PT"><b>$chmprech[3]<BR>$chmprech[6]</b></font></td>
                <td bgcolor=C6CDC5><font class="PT"><b>$chmprech[8]<BR>$chmprech[9]</b></font></td>
                <td bgcolor=C6CDC5><font class="PT"><b>$chmprech[10]<BR>$chmprech[12]</b></font></td>
                <td bgcolor=C6CDC5><font class="PT"><b>$chmprech[13]&nbsp;/&nbsp;$mess[3024]<BR>$chmprech[15]&nbsp;/&nbsp;$chmprech[16]&nbsp;/&nbsp;$chmprech[19]<BR>$chmprech[17]&nbsp;/&nbsp;$chmprech[18]<BR>$chmprech[22]</b></font></td>
                <td bgcolor=C6CDC5><font class="PT"><b>$chmprech[25]</b></font></td>
                </font>
            </tr>
            };

            my $num = 0;

            while (my $tabRow = $sth->fetchrow_hashref())
            {
                my $taux_euro = &taux_euro($G_MONNAIE);
                my $c = $ordre % 2;
                my $color="''";

                if ($c == 1)
                {
                    $color=qq{'#D6D6D6'};
                }

                my $contractUrl = &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $tabRow->{'id'}});
                print $cgi->start_form(-method=>'get', action=>'modification.cgi');
                print qq{
                <tr bgColor=$color onmouseover="bgColor='#FFCC33'" onmouseout="bgColor=$color">
                    <td><a href="$contractUrl" target="_blank"><font size=+0><b>$tabRow->{'code'}</b></a></font><BR>$tabRow->{'state.label'}</td>
                };

                my $debhtm = "";
                my $finhtm = "";
                $debhtm = qq{<a target="client" href="$HTTP_PORTAIL/cgi-bin/old/location/infoclient.cgi?value=$tabRow->{'customer.id'}">};
                $finhtm = qq{</a>};
                print qq{
                    <td>$debhtm$tabRow->{'customer.label'}$finhtm<BR>$tabRow->{'order.no'}</TD>
                    <td noWrap>$tabRow->{'site.label'}<BR>$tabRow->{'site.postalCode'} $tabRow->{'site.city'}<BR>$tabRow->{'site.contact'}<BR>$tabRow->{'site.telephone'}</td>
                    <td noWrap>$tabRow->{'model.label'}<br>N&deg; $tabRow->{'machine.parkNumber'}<br>
                };

                print qq{
                </TD>
                <TD noWrap>
                };

                my $date;
                $date = &format_datetime($tabRow->{'beginDate'});
                print qq{$date<br>\n};
                my $date;
                $date = &format_datetime($tabRow->{'endDate'});
                print qq{$date<br>\n};
                my $duree = &fnombre($tabRow->{'duration'}, 1, 0);
                my $unitduree = $mess[2891];

                if (($tabRow->{'unitPrice'} == 0) && ($tabRow->{'amount.monthlyPrice'} != 0) && ($tabRow->{'amount.fixedPrice'} == 0))
                {
                    $unitduree = $mess[2896];
                }

                print qq{
                $mess[2890] $duree $unitduree </TD>
                <TD>
                };

                my $cattarif = &infotaf($tabRow->{'customer.id'});

                if (($tabRow->{'unitPrice'} != 0) && ($tabRow->{'amount.monthlyPrice'} == 0) && ($tabRow->{'amount.fixedPrice'} == 0))
                {
                    my $cv = sprintf("%.2f", $tabRow->{'unitPrice'} / $taux_euro);
                    $prix = &fnombre($tabRow->{'unitPrice'});
                    print qq{<font class="PT_HL"><B>$prix</b> ($cv)&nbsp;$mess[2892]/ <b>$cattarif</b></font><br>\n};
                }
                elsif (($tabRow->{'unitPrice'} == 0) && ($tabRow->{'amount.monthlyPrice'} != 0) && ($tabRow->{'amount.fixedPrice'} == 0))
                {
                    my $cv = sprintf("%.2f", $tabRow->{'amount.monthlyPrice'} / $taux_euro);
                    $prix = &fnombre($tabRow->{'amount.monthlyPrice'});
                    print qq{<font class="PT_HL">$prix</b> ($cv)&nbsp;$mess[2893]/ <b>$cattarif</b></font><br>\n};
                }
                elsif (($tabRow->{'unitPrice'} == 0) && ($tabRow->{'amount.monthlyPrice'} == 0) && ($tabRow->{'amount.fixedPrice'} != 0))
                {
                    my $cv = sprintf("%.2f", $tabRow->{'amount.fixedPrice'}/$taux_euro);
                    $prix = &fnombre($tabRow->{'amount.fixedPrice'});
                    print qq{<font class="PT_HL">$prix ($cv) $mess[2894]/ <b>$cattarif</b></font><br>\n};
                }
                else
                {
                    print qq{<font class="PT_HL">$mess[2895]</font><br>\n};
                }

                # Assurance / Gestion de recours / Carburant
                my $cv = sprintf("%.2f", $tabRow->{'insurance.amount'} / $taux_euro);
                my $prix = &fnombre($tabRow->{'insurance.amount'});

                my $appealAmountEuros = sprintf("%.2f", $tabRow->{'appeal.amount'} / $taux_euro);
                my $appealAmount = &fnombre($tabRow->{'appeal.amount'});

                my $prix2 = &fnombre($tabRow->{'fuel.quantity'});
                print qq{<b>A&nbsp;$prix</b> ($cv)&nbsp;/<b>R&nbsp;$appealAmount</b> ($appealAmountEuros)&nbsp;/&nbsp;<b>C&nbsp;$prix2 l.</b><br>\n};

                # Transport aller / Transport retour
                my $cv = sprintf("%.2f", $tabRow->{'delivery.amount'} / $taux_euro);
                my $prix = &fnombre($tabRow->{'delivery.amount'});
                my $cv2 = sprintf("%.2f", $tabRow->{'recovery.amount'} / $taux_euro);
                my $prix2 = &fnombre($tabRow->{'recovery.amount'});
                print qq{<FONT class="PT_HL"><b>TA&nbsp;$prix</b> ($cv)&nbsp;/&nbsp;<b>TR&nbsp;$prix2</b> ($cv2)</FONT><BR>\n};

                # Montant total HT
                my $cv = sprintf("%.2f", $tabRow->{'total.amount'} / $taux_euro);
                my $prix = &fnombre($tabRow->{'total.amount'});
                print qq{
                <span align="right"><b>$prix</b> ($cv)</span></TD>
                <TD>
                };

                if (($tabRow->{'note'} ne "") || ($tabRow->{'comment'} ne "")) {
                    my $contractUrl = &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $tabRow->{'id'}});
                    print qq{
                    <a href="$contractUrl" target="_blank">$chmprech[25]</a>
                    };
                }

                print qq{
                </TD>
                };

                print qq{
                </td>
            </tr></font></form>
                };
                $ordre++;
            }

            $sth->finish;
            print "</center>";
        }
    }

    print qq{</font>\n};
}

sub infotaf {
    my ($clauto)=@_;
    my $req=qq|SELECT TARIFCLIENTREF FROM CLIENT  LEFT join CLIENTTARIF ON CLIENTTARIF.CLIENTTARIFAUTO=CLIENT.CLIENTTARIFAUTO WHERE CLAUTO="$clauto"|;
    my $sth=$DB->prepare("$req");
    $sth->execute();
    my ($tarif)=$sth->fetchrow_array();
    $sth->finish;
    return $tarif;
}
