#!/usr/bin/perl

# $Id: verif_casse.cgi,v 2.4 2006/02/06 16:52:51 julien Exp $
# $Log: verif_casse.cgi,v $
# Revision 2.4  2006/02/06 16:52:51  julien
# V. 1.4 : Tarification v2
#
# Revision 2.3.2.1  2005/12/13 15:46:49  julien
# no message
#
# Revision 2.3.4.1  2005/12/13 10:32:01  julien
# Inclusion du r�pertoire lib dans chaque fichier.
#
# Revision 2.3  2005/08/26 12:11:01  julien
# Pr�fixage des tables de AUTH dans les requ�tes.
#
# Revision 2.2  2005/05/03 16:57:14  julien
# Tra�age du temps d'ex�cution des scripts.
#
# Revision 2.1  2005/05/03 14:54:21  julien
# Tra�age du temps d'ex�cution des scripts.
#
# Revision 2.0  2004/09/16 15:07:38  julien
# Version r�cup�r�e de Lancelot et nettoy�e
#
# Revision 1.2  2004/09/16 14:23:17  julien
# Ajout des identifiants
#

use lib '../lib', '../../inc', '../../lib';

use CGI;
use DBI;
use autentif;
use accesbd;
use varglob;
use pagehtml;
use calculdate;
use fonctions;
use titanic;
use Date::Calc qw(Add_Delta_YMD);
use tracetemps;
use MIME::Base64;
use LOC::Request;
use LOC::Alert;
use LOC::Globals;

# Initialisation du chrono
my $timer = &init_chrono();
my $build = &LOC::Globals::get('build');


$BD = &Environnement("LOCATION");

$url2 = "infoclient.cgi";
$url3 = "verif_casse.cgi";

$couleur = '#ffffff';

$q = new CGI;
#print $q->header;
$q->autoEscape(undef);
$taux = &taux_euro($G_MONNAIE);

%traduction = (
	"FR" => {
#		"0"  => "V�rification DEVIS SERVICE",
		"0"  => "Historique des devis de casse",
		"1"  => "Vous n'avez pas l'acc�s � cette fonctionnalit�",
		"2"  => "Attente",
		"3"  => "Num. Devis",
		"4"  => "Num. Contrat",
		"5"  => "Soci�t�",
		"6"  => "Objet du devis",
		"7"  => "Prix propos� en euro",
		"8"  => "Prix propos� en franc",
		"9"  => "Refuser",
		"10" => "Valider",
		"11" => "Pas de devis de service en attente",
		"12" => "Cause",
		"13" => "Main d'oeuvre",
		"14" => "Montant total",
		"15" => "R�f. Article",
		"16" => "Prix r�paration",
		"17" => "Quantit�",
		"19" => "Commentaire",
		"20" => "v�rification",
		"21" => "Fermer",
		"22" => "D�tail",
		"23" => "D�placement",
		"24" => "Veuillez saisir un commentaire pour tout refus de devis",
		"25" => "Etat",
		"26" => "Prix D�part/Final",
		"27" => "Date",
		"28" => "Remise",
		"29" => "Type",
		"30" => "Co�t r�el",
		"31" => "Pi�ces r�f�renc�es",
		"32" => "Aucun r�sultat ne correspond"
	},
	"MA" => {
#		"0"  => "V�rification DEVIS SERVICE",
		"0"  => "Historique des devis de casse",
		"1"  => "Vous n'avez pas l'acc�s � cette fonctionnalit�",
		"2"  => "Attente",
		"3"  => "Num. Devis",
		"4"  => "Num. Contrat",
		"5"  => "Soci�t�",
		"6"  => "Objet du devis",
		"7"  => "Prix propos� en dirham",
		"8"  => "Prix propos� en franc",
		"9"  => "Refuser",
		"10" => "Valider",
		"11" => "Pas de devis de service en attente",
		"12" => "Cause",
		"13" => "Main d'oeuvre",
		"14" => "Montant total",
		"15" => "R�f. Article",
		"16" => "Prix r�paration",
		"17" => "Quantit�",
		"19" => "Commentaire",
		"20" => "v�rification",
		"21" => "Fermer",
		"22" => "D�tail",
		"23" => "D�placement",
		"24" => "Veuillez saisir un commentaire pour tout refus de devis",
		"25" => "Etat",
		"26" => "Prix D�part/Final",
		"27" => "Date",
		"28" => "Remise",
		"29" => "Type",
		"30" => "Co�t r�el",
		"31" => "Pi�ces r�f�renc�es",
		"32" => "Aucun r�sultat ne correspond"
	},
	"ES" => {
#		"0"  => "Verificaci�n del PRESUPUESTO de SERVICIO",
		"0"  => "Historique des devis de casse",
		"1"  => "Ud no tiene ning�n acceso a este rasgo",
		"2"  => "En espera",
		"3"  => "N� Presupuesto",
		"4"  => "N� Contrato",
		"5"  => "Sociedad",
		"6"  => "Motivo del Presupuesto",
		"7"  => "Precio euro",
		"8"  => "Precio pta",
		"9"  => "Rechazado",
		"10" => "Validar",
		"11" => "Ningun servicios de presupuesto en espera",
		"12" => "Causa",
		"13" => "Mano de obra",
		"14" => "Cantidad total",
		"15" => "Articulo o Refer�ncia",
		"16" => "Precio Piezas",
		"17" => "Cantidad",
		"19" => "Comentario",
		"20" => "",
		"21" => "Cerrar",
		"22" => "Descripci�n",
		"23" => "Desplazamiento",
		"24" => "Quiere entender un comentario para toda la denegaci�n de presupuesto",
		"25" => "Estado",
		"26" => "Precio Salida/Final",
		"27" => "Fecha",
		"28" => "Descuento",
		"29" => "Tipo",
		"30" => "Coste real",
		"31" => "Partes referencias",
		"32" => "Ning�n resultado corresponde"
	},
	"IT" => {
#		"0"  => "Verifica PREVENTIVO SERVIZIO",
		"0"  => "Historique des devis de casse",
		"1"  => "Non avete l'accesso a questa funzionalit�",
		"2"  => "Attesa",
		"3"  => "N�Preventivo",
		"4"  => "N�Contratto",
		"5"  => "Societ�",
		"6"  => "Oggetto di preventivo",
		"7"  => "Prezzo proposto in euro",
		"8"  => "Prezzo proposto nel lire",
		"9"  => "Rifiutare",
		"10" => "Inviare",
		"11" => "Nessun preventivo di servizio in attesa",
		"12" => "Causa",
		"13" => "Mano d'opera",
		"14" => "Montante totale",
		"15" => "Ref. Articolo",
		"16" => "Prezzo riparazione",
		"17" => "Quantit�",
		"19" => "Commento",
		"20" => "verifica",
		"21" => "Chiudere",
		"22" => "D�tail",
		"23" => "Spostamento",
		"24" => "Volete osservare un commento per qualsiasi rifiuto di stima",
		"25" => "Stato",
		"26" => "Prezzo Partenza/Finale",
		"27" => "Data",
		"28" => "Riduzione",
		"29" => "Tipo",
		"30" => "Costo reale",
		"31" => "Parti rinviate",
		"32" => "Nessun risultato corrisponde"
	},
	"PT" => {
		"0"  => "Verifica��o ESTIMATIVA SERVI�O",
		"1"  => "Voc� n�o tem o acesso a esta funcionalidade",
		"2"  => "Espera",
		"3"  => "N�m. Estimativa",
		"4"  => "N�m. Contrato",
		"5"  => "Sociedade",
		"6"  => "Objeto da estimativa",
		"7"  => "Pre�o proposto em euro",
		"8"  => "Pre�o proposto em escudo",
		"9"  => "Recusar",
		"10" => "Validar",
		"11" => "Nenhum a estimativa da servi�o na espera",
		"12" => "Causa",
		"13" => "M�o de obra",
		"14" => "Montante total",
		"15" => "Refer�ncia Artigo",
		"16" => "Pre�o reparo",
		"17" => "Quantidade",
		"19" => "Coment�rio",
		"20" => "verifica��o",
		"21" => "Fechar",
		"22" => "D�tail",
		"23" => "Desloca��o",
		"24" => "Queiram apreender um coment�rio para qualquer recusa de or�amentos",
		"25" => "Estado",
		"26" => "Pr�mio Partida/Final",
		"27" => "Data",
		"28" => "Diminui��o",
		"29" => "Tipo",
		"30" => "Custo real",
		"31" => "Pe�as remetidas",
		"32" => "Nenhum resultado corresponde"
	},
	"GB" => {
		"0"  => "CHECKING estimate service",
		"1"  => "You don't have the access to this functionality",
		"2"  => "Wait",
		"3"  => "N�Estimate",
		"4"  => "N�Contract",
		"5"  => "Company",
		"6"  => "Object of the estimate",
		"7"  => "Price suggested in euro",
		"8"  => "Price suggested in dollar",
		"9"  => "To refuse",
		"10" => "To validate",
		"11" => "No estimate of service on standby",
		"12" => "Cause",
		"13" => "Workforce",
		"14" => "Total amount",
		"15" => "Ref. Article",
		"16" => "Price repair",
		"17" => "Quantity",
		"19" => "Comment",
		"20" => "checking",
		"21" => "Close",
		"22" => "Detail",
		"23" => "Displacement",
		"24" => "Please write a comment for any refusal of estimate ",
		"25" => "State",
		"26" => "Price Start/Final",
		"27" => "Date",
		"28" => "Handing-Over",
		"29" => "Type",
		"30" => "Real cost",
		"31" => "Referred Parts",
		"32" => "No result corresponds"
	},
	"LU" => {
		"0"  => "V�rification DEVIS SERVICE",
		"1"  => "Vous n'avez pas l'acc�s � cette fonctionnalit�",
		"2"  => "Attente",
		"3"  => "Num. Devis",
		"4"  => "Num. Contrat",
		"5"  => "Soci�t�",
		"6"  => "Objet du devis",
		"7"  => "Prix propos� en euro",
		"8"  => "Prix propos� en franc",
		"9"  => "Refuser",
		"10" => "Valider",
		"11" => "Pas de devis de service en attente",
		"12" => "Cause",
		"13" => "Main d'oeuvre",
		"14" => "Montant total",
		"15" => "R�f. Article",
		"16" => "Prix r�paration",
		"17" => "Quantit�",
		"19" => "Commentaire",
		"20" => "v�rification",
		"21" => "Fermer",
		"22" => "D�tail",
		"23" => "D�placement",
		"24" => "Veuillez saisir un commentaire pour tout refus de devis",
		"25" => "Etat",
		"26" => "Prix D�part/Final",
		"27" => "Date",
		"28" => "Remise",
		"29" => "Type",
		"30" => "Co�t r�el",
		"31" => "Pi�ces r�f�renc�es",
		"32" => "Aucun r�sultat ne correspond"
	}
);

$datedujour = &get_date(1);

my $titre = "$traduction{$G_PAYS}{0}";
#&Entete("$traduction{$G_PAYS}{0}");
&ente($titre,$titre,$G_PENOM,$G_PEPRENOM);

print $q->start_form;

#@choix = ("V", "H");
#%choix = ("V"=>"V�rification", "H"=>"Historique");

if (!&verif_droit) {
	print "<center><h1><font color=red>$traduction{$G_PAYS}{1}</font></h1></center>";
	exit;
}

if ($q->param('numdevis')) {
	$numdevis = $q->param('numdevis');
	&affdeviscasse($numdevis);
	print $q->end_form();
	$BD->disconnect;

	# Arr�t du chrono
	my $delta_time = &stop_chrono($timer);

	# Pied de la page HTML
	&Pied($delta_time);

	exit;
}

#print qq|<center>|, $q->radio_group(-name=>'choix', -values=>\@choix, -default=>"V", -labels=>\%choix, -onclick=>"submit()"), qq|</center>|;
#$q->param('choix', 'V') if !$q->param('choix');

#if ($q->param('choix') eq 'V') {
#	if ($q->param('Valider.x')) {
#		&traitvercasse();
#	}
#
#	$temp = $BD->prepare(qq|select CASSE.CONTRATAUTO, CONTRATCODE, CONTRAT.CLAUTO, CLSTE, CAOBJET, CAPXEU, CASSE.CASSEAUTO, CANUM, COMMENTAIRE FROM CASSE LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=CASSE.CONTRATAUTO LEFT JOIN CLIENT ON CLIENT.CLAUTO=CONTRAT.CLAUTO WHERE CASSE.ETCODE="CAS01" and CONTRAT.AGAUTO="$G_AGENCE" group by CASSE.CASSEAUTO order by CASSE.CASSEAUTO|);
#	$temp->execute();
#	$rows = $temp->rows;
#
#	if ($rows > 0) {
#		print qq|
#		<table width="100%" border="0"  cellspacing="3" cellpadding="0">
#			<tr bgcolor="#D6D6D6">
#				<th>$traduction{$G_PAYS}{3}</th>
#				<th>$traduction{$G_PAYS}{4}</th>
#				<th>$traduction{$G_PAYS}{5}</th>
#				<th>$traduction{$G_PAYS}{6}</th>
#				<th>$traduction{$G_PAYS}{19}</th>
#				<th>$traduction{$G_PAYS}{7}</th>
#				<th>$traduction{$G_PAYS}{30}<br>$traduction{$G_PAYS}{31}</th>
#				<th>$traduction{$G_PAYS}{2}</th>
#				<th nowrap>$traduction{$G_PAYS}{9}</th>
#			</tr>
#		|;
#
#		$i = 0;
#		while (@temp = $temp->fetchrow_array()) {
#			$cout_reel = &calcul_cout($temp[6]);
#
#			if ($i%2==0) {
#				$couleur2 = "";
#			} else {
#				$couleur2 = "#D6D6D6";
#			}
#
#			$q->param("contratauto_$temp[6]","$temp[0]");
#			print $q->hidden("contratauto_$temp[6]");
#			$temp[5] = sprintf("%.2f",$temp[5]);
#			$q->param("prix_$temp[6]", "$temp[5]");
#			print $q->hidden("prix_$temp[6]");
#
#			print qq|
#			<tr bgColor='$couleur2' onmouseover="bgColor='$couleur'" onmouseout="bgColor='$couleur2'">
#				<td>&nbsp;<a href="#" onclick="return rs('consult', '$url3?numdevis=$temp[6]',500,450)">$temp[7]</a>&nbsp;</td>
#				<td>&nbsp;<a href="#" onclick="return rs('consult', '$url?num=$temp[0]',250,450)">$temp[1]</a>&nbsp;</td>
#				<td>&nbsp;<a href="#" onclick="return rs('client', '$url2?value=$temp[2]',450,450)">$temp[3]</a>&nbsp;</td>
#				<td>$temp[4]</td>
#				<td>|, $q->textarea(-name=>"commentaire_$temp[6]", -default=>"$temp[8]"), qq|</td>
#				<td><div id="prixcasseeuro_$temp[6]">$temp[5]</div>|,$q->textfield(-name=>"rem_$temp[6]", -value=>"0", -size=>6, -onblur=>"if (isNaN(rem_$temp[6].value) || rem_$temp[6].value > 100) {alert('Entrez un chiffre entre 0 et 100');rem_$temp[6].value='0';} else {prixcasseeuro_$temp[6].innerText='$temp[5] / '+Math.round(((1 - (this.value/100)) * $temp[5]) * 100)/100}"),qq| %<input type="radio" name="selection_$temp[6]" value="2"></td>|,
#				"<td>".($cout_reel != 0 ? $cout_reel : "&nbsp;").qq|</td>
#				<th><input type="radio" name="selection_$temp[6]" value="1" checked></th>
#				<th><input type="radio" name="selection_$temp[6]" value="4"></th>
#			</tr>
#			|;
#			$i++;
#		}
#		print "</table>";
#		print "<center>$G_LST_IMP &nbsp; ",$q->image_button(-name=>"Valider", -src=>"$URL_IMG/press-me4.gif", -alt=>"$traduction{$G_PAYS}{10}", -align=>"absmiddle"),"</center>";
#	} else {
#		print "<center><font color=red size=5>$traduction{$G_PAYS}{11}</font></center>";
#	}
#	$temp->finish;
#} elsif ($q->param('choix') eq 'H') {
	$crit_rech_histo_client = $q->param("rech_histo_client");
	$crit_histo_client      = $q->param("histo_client");
	$histo_etat             = $q->param("histo_etat");
	$dj                     = $q->param("dj");
	$dm                     = $q->param("dm");
	$da                     = $q->param("da");
	$fj                     = $q->param("fj");
	$fm                     = $q->param("fm");
	$fa                     = $q->param("fa");

	if ($crit_rech_histo_client) {
		@lst_histo_client = ();
		$lab_histo_client = ();

		$requ = $BD->prepare(qq|select CLAUTO, CLSTE, CLCODE from CLIENT where CLSTE like "$crit_rech_histo_client%" order by CLSTE|);
		$requ->execute();
		$rows = $requ->rows;

		@lst_histo_client = ("");
		$lab_histo_client{""} = "---- $rows choix ----";
		while (@temp = $requ->fetchrow_array()) {
			push @lst_histo_client, $temp[0];
			$lab_histo_client{$temp[0]} = "$temp[1], $temp[2]";
		}
		$requ->finish;
	} else {
		@lst_histo_client = ("");
		$lab_histo_client{""} = "Recherche";
	}

	if ($q->param('Rechercher.x') || $q->param('Valider.x')) {
		@datedep = ();
		@datefin = ();
	} else {
		$datejour = &get_date(1);
		@datedep  = split(/\//, $datejour);
		@datefin  = Add_Delta_YMD($datedep[2], $datedep[1], $datedep[0], 0, -3, 0);
		$datefin[2] = $datefin[2] < 10 ? "0".$datefin[2] : $datefin[2];
		$datefin[1] = $datefin[1] < 10 ? "0".$datefin[1] : $datefin[1];
	}

	$requ = $BD->prepare(qq|select ETCODE, ETLIBELLE from ETATTABLE where ETCODE like "CAS%" and ETCODE not in ("CAS06", "CAS99") order by ETLIBELLE|);
	$requ->execute();
	$rows = $requ->rows;

	@lst_histo_etat = ("");
	$lab_histo_etat{""} = "---- $rows choix ----";
	while (@temp = $requ->fetchrow_array()) {
		push @lst_histo_etat, $temp[0];
		$lab_histo_etat{$temp[0]} = $temp[1];
	}
	$requ->finish;

	print qq|
		<table border="0" cellspacing="3" cellpadding="0" align=center>
			<tr>
				<td>Client</td>
				<td>|, $q->popup_menu(-name=>'histo_client', -values=>\@lst_histo_client, -labels=>\%lab_histo_client), qq|</td>
				<td>|, $q->textfield(-name=>"rech_histo_client"),qq|</td>
			</tr>
			<tr>
				<td>Etat</td>
				<td>|, $q->popup_menu(-name=>'histo_etat', -values=>\@lst_histo_etat, -labels=>\%lab_histo_etat), qq|</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Date d�but</td>
				<td>|,
					$q->textfield(-name=>"dj", -size=>2, -maxlength=>2, -default=>$datefin[2]), "/",
					$q->textfield(-name=>"dm", -size=>2, -maxlength=>2, -default=>$datefin[1]), "/",
					$q->textfield(-name=>"da", -size=>4, -maxlength=>4, -default=>$datefin[0]),
				qq|</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Date fin</td>
				<td>|,
					$q->textfield(-name=>"fj", -size=>2, -maxlength=>2, -default=>$datedep[0]), "/",
					$q->textfield(-name=>"fm", -size=>2, -maxlength=>2, -default=>$datedep[1]), "/",
					$q->textfield(-name=>"fa", -size=>4, -maxlength=>4, -default=>$datedep[2]),
				qq|</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan=4 align=center>|, $q->image_button(-name=>"Rechercher", -src=>"$URL_IMG/loupe_off.gif", -align=>"absmiddle"), qq|</td>
			</tr>
		</table>
	|;

	if ($q->param('Valider.x')) {
		&imphistcasse();
	}
	if ($q->param('Rechercher.x') || $q->param('Valider.x')) {
		$requ_global = $BD->prepare(qq|select LOGICIEL.LOAUTO from MENU left join LOGICIEL on MENU.LOAUTO=LOGICIEL.LOAUTO left join AUTH.`MEPR` on MEPR.MEAUTO=MENU.MEAUTO where MEPR.PROFILAUTO=$G_PROFIL and LONOM="Nomade" and MEFICHIER="verif_casse.cgi?"|);
		$requ_global->execute();
		$global = $requ_global->fetchrow_array();
		$requ_global->finish;

		$where = "";
		$param = "&Rechercher.x=1";
		$nblignes_aff = 20;
		$ordre = $q->param('ordre') ? $q->param('ordre') : 3;
		$limit = $q->param('limit') ? $q->param('limit') : 0;
		%tri = ("3" => "CLSTE", "7" => "CANUM", "1" => "CONTRATCODE", "10" => "ETLIBELLE", "11" => "CADATE", "12" => "CAETAT");

		$param .= "&rech_histo_client=$crit_rech_histo_client" if $crit_rech_histo_client;
		$where .= qq| and CONTRAT.AGAUTO="$G_AGENCE"| if !$global;
		if ($crit_histo_client) {
			$where .= qq| and CLIENT.CLAUTO=$crit_histo_client|;
			$param .= "&histo_client=$crit_histo_client";
		}
		if ($histo_etat) {
			$where .= qq| and ETATTABLE.ETCODE="$histo_etat"|;
			$param .= "&histo_etat=$histo_etat";
		}
		if ($dj && $dm && $da && $fj && $fm && $fa) {
			$where .= qq| and CASSE.CADATE>="$da/$dm/$dj" and CASSE.CADATE<="$fa/$fm/$fj"|;
			$param .= "&dj=$dj&dm=$dm&da=$da&fj=$fj&fm=$fm&fa=$fa";
		}

		$temp = $BD->prepare(qq|select CASSE.CONTRATAUTO, CONTRATCODE, CONTRAT.CLAUTO, CLSTE, CAOBJET, CAPXEU, CASSE.CASSEAUTO, CANUM, COMMENTAIRE, CAPXFINALEU, ETLIBELLE, date_format(CADATE, "%d/%m/%Y"), CAETAT FROM CASSE LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=CASSE.CONTRATAUTO LEFT JOIN CLIENT ON CLIENT.CLAUTO=CONTRAT.CLAUTO left join ETATTABLE on ETATTABLE.ETCODE=CASSE.ETCODE WHERE CASSE.ETCODE not in ("CAS06", "CAS99") and CASSE.CONTRATAUTO != 0 and CANUM != ""$where group by CASSE.CASSEAUTO order by $tri{$ordre}|);
		$temp->execute();
		$rows = $temp->rows;
		$nbpage = $rows/$nblignes_aff>int($rows/$nblignes_aff)?int($rows/$nblignes_aff)+1:int($rows/$nblignes_aff);

		if ($rows > 0) {
			if ($nbpage > 1) {
				@lasituation = ();
				$inc = 0;
				$j   = 1;

				while (@valeurs = $temp->fetchrow_array()) {
					if ($inc % $nblignes_aff == 0) {
						$lasituation[$j] = "$valeurs[$ordre] --> ";
					}
					if (($inc % (($nblignes_aff * $j) - 1) == 0 && $inc != 0) || $inc == ($rows - 1)) {
						$lasituation[$j] .= $valeurs[$ordre];
						$j++;
					}
					$inc++;
				}
				$temp->finish;

				$nb = 0;
				$popupmenu = "<SELECT NAME=\"pagination\" onChange=\"document.location=this.options[this.selectedIndex].value\">\n";
				for ($i=1;$i<=$nbpage;$i++) {
					if ($nb == $limit) {
						$popupmenu .= "<OPTION VALUE=\"0\" selected>page $i : $lasituation[$i]\n";
					} else {
						$popupmenu .= "<OPTION VALUE=\"$url3?choix=H&limit=$nb&ordre=$ordre$param\">page $i : $lasituation[$i]\n";
					}
					$nb += $nblignes_aff;
				}
				$popupmenu .= "</SELECT>\n";

				$menu = "<center>$popupmenu<br><b>".($limit + $nblignes_aff > $rows?$rows:$limit + $nblignes_aff)." / $rows</b></center>";
				print "<br>$menu";
			}

			print qq|
			<table width="100%" border="0"  cellspacing="3" cellpadding="0">
				<tr bgcolor="#D6D6D6">
					<th><a href="$url3?choix=H&limit=$limit&ordre=3$param">$traduction{$G_PAYS}{5}</a></th>
					<th><a href="$url3?choix=H&limit=$limit&ordre=7$param">$traduction{$G_PAYS}{3}</a></th>
					<th><a href="$url3?choix=H&limit=$limit&ordre=1$param">$traduction{$G_PAYS}{4}</a></th>
					<th>$traduction{$G_PAYS}{6}</th>
					<th>$traduction{$G_PAYS}{19}</th>
					<th>$traduction{$G_PAYS}{26}<br>$traduction{$G_PAYS}{28}</th>
					<th><a href="$url3?choix=H&limit=$limit&ordre=10$param">$traduction{$G_PAYS}{25}</a></th>
					<th><a href="$url3?choix=H&limit=$limit&ordre=11$param">$traduction{$G_PAYS}{27}</a></th>
					<th><a href="$url3?choix=H&limit=$limit&ordre=12$param">$traduction{$G_PAYS}{29}</a></th>
				</tr>
			|;

			$i = 0;
			$temp = $BD->prepare(qq|select CASSE.CONTRATAUTO, CONTRATCODE, CONTRAT.CLAUTO, CLSTE, CAOBJET, CAPXEU, CASSE.CASSEAUTO, CANUM, COMMENTAIRE, CAPXFINALEU, ETLIBELLE, date_format(CADATE, "%d/%m/%Y"), CAETAT FROM CASSE LEFT JOIN CONTRAT ON CONTRAT.CONTRATAUTO=CASSE.CONTRATAUTO LEFT JOIN CLIENT ON CLIENT.CLAUTO=CONTRAT.CLAUTO left join ETATTABLE on ETATTABLE.ETCODE=CASSE.ETCODE where CASSE.ETCODE not in ("CAS06", "CAS99") and CANUM != ""$where group by CASSE.CASSEAUTO order by $tri{$ordre} limit $limit, $nblignes_aff|);
			$temp->execute();
			while (@temp = $temp->fetchrow_array()) {
				$pourcentage = 0;
				if ($i % 2 == 0) {
					$couleur2 = "";
				} else {
					$couleur2 = "#D6D6D6";
				}

				$temp[5] = sprintf("%.2f",$temp[5]);
				$temp[9] = sprintf("%.2f",$temp[9]);
				$pourcentage = sprintf("%.2f", (1 - ($temp[9] / $temp[5])) * 100) if ($temp[9] != 0 && $temp[5] != 0);

				my $contractUrl = &LOC::Request::createRequestUri('rent:rentContract:view', {'contractId' => $temp[0]});
				print qq|
				<tr bgColor='$couleur2' onmouseover="bgColor='$couleur'" onmouseout="bgColor='$couleur2'">
					<td>&nbsp;<a href="#" onclick="return rs('client', '$url2?value=$temp[2]',450,450)">$temp[3]</a>&nbsp;</td>
					<td>&nbsp;<a href="#" onclick="return rs('consult', '$url3?numdevis=$temp[6]',500,450)">$temp[7]</a>&nbsp;</td>
					<td>&nbsp;<a href="$contractUrl" target="_blank">$temp[1]</a>&nbsp;</td>
					<td>&nbsp;$temp[4]</td>
					<td>&nbsp;$temp[8]</td>
				|;

				if ($temp[9] != 0 && $temp[9] != $temp[5]) {
					print qq|<td align=right>&nbsp;$temp[5]/$temp[9]<br>$pourcentage%</td>|;
				} else {
					print qq|<td align=right>&nbsp;$temp[5]</td>|;
				}

				print qq|
					<td>&nbsp;$temp[10]</td>
					<td>&nbsp;$temp[11]</td>
					<td>&nbsp;$temp[12]</td>
					<td>|,$q->checkbox(-name=>"selection_$temp[6]", -label=>''),qq|</td>
				</tr>
				|;

				$i++;
			}
			print qq|
			</table>
			<center>$G_LST_IMP &nbsp; |,$q->image_button(-name=>"Valider", -src=>"$URL_IMG/press-me4.gif", -alt=>"$traduction{$G_PAYS}{10}", -align=>"absmiddle"),qq|</center>
			<input type=hidden name=limit value=$limit>
			<input type=hidden name=ordre value=$ordre>
			|;
		} else {
			print "<center><font color=red size=5>$traduction{$G_PAYS}{32}</font></center>";
		}
	}
    if ($temp)
    {
        $temp->finish;
    }
#}

print $q->end_form();
if ($temp)
{
    $temp->finish;
}
$BD->disconnect;

# Arr�t du chrono
my $delta_time = &stop_chrono($timer);

# Pied de la page HTML
&Pied($delta_time);


sub imphistcasse {
	foreach ($q->param) {
		if ($_ =~ /selection_(\d+)/) {
			$noligne = $1;
			&impression("$noligne",1);
		}
	}
}

sub traitvercasse {
	$bon = 1;
	foreach ($q->param) {
		if ($_ =~ /selection_(\d+)/) {
			$noligne = $1;
			$casseetat = $q->param("selection_$1");
			if ($casseetat > 1) {
				if ($casseetat == 4 && !$q->param("commentaire_$noligne")) {
					$bon = 0;
					next;
				}
				# Traitement 1 maj CASSE
				$commentaire = $q->param("commentaire_$noligne");
				$BD->do(qq|UPDATE CASSE SET ETCODE="CAS0$casseetat", COMMENTAIRE="$commentaire" WHERE CASSE.CASSEAUTO="$noligne"|);

				if ($casseetat == 2) {
					$prix_euro   = $q->param("prix_$noligne");
					$rem_euro    = $q->param("rem_$noligne");
					$montanteuro = sprintf("%.2f", (1 - ($rem_euro / 100)) * $prix_euro);

					$montantfranc = to_euro($montanteuro,$G_MONNAIE) ;
					$contratauto = $q->param("contratauto_$noligne");

					# Traitement 2 update montant prix casse en fr et en euro
					$BD->do(qq|update CASSE set CAPXFINALEU="$montanteuro",CAPXFINALFR="$montantfranc", CADATEFACT=NOW() where CASSEAUTO="$noligne"|);

					# Traitement 3 update montant total du prix du montant de casse
					$BD->do(qq|INSERT INTO TRACE (ETCODE,CONTRATAUTO,CASSEAUTO,PEAUTO) VALUES ("TRA28","$contratauto","$noligne","$G_PEAUTO")|);

					# Traitement d'impression du pm
					&impression("$noligne");
				}
				if ($casseetat == 4) {
					$contratauto = $q->param("contratauto_$noligne");
					$BD->do(qq|INSERT INTO TRACE (ETCODE,CONTRATAUTO,CASSEAUTO,PEAUTO) VALUES ("CAS04","$contratauto","$noligne","$G_PEAUTO")|);
				}
			}
		}
	}
	if (!$bon) {
		print qq|<center><font color=red>$traduction{$G_PAYS}{24}</font></center>|;
	}
}

sub affdeviscasse {
	$temp = $BD->prepare(qq|select CANUM, CAOBJET, CAETAT, CATRAVAUX1, CAPXEU, MOOBJET, COMMENTAIRE, CAKMEU from CASSE where CASSE.CASSEAUTO="$_[0]"|);
	$temp->execute();
	@result = $temp->fetchrow_array();
	$temp->finish;
	print qq|
	<table border=0 align=center width=90%>
		<tr bgcolor="#D6D6D6">
			<th>$traduction{$G_PAYS}{3}</th>
			<th>$traduction{$G_PAYS}{6}</th>
			<th>$traduction{$G_PAYS}{12}</th>
		</tr>
		<tr>
			<td>$result[0]</td>
			<td>$result[1]</td>
			<td>$result[2]</td>
		</tr>
		<tr>
			<td></td>
		</tr>
		<tr>
			<td colspan=3>
	|;

	$k=0;
	$temp = $BD->prepare(qq|select DETAILSCASSEQTE, DETAILSCASSEPXE, '', LIBDIVERS, DETAILSCASSE.AIAUTO from DETAILSCASSE where CASSEAUTO="$_[0]" and CASSEST=1|);
	$temp->execute();
	$lerows = $temp->rows;
	if ($lerows > 0) {
		print qq|
				<table border=1 align=center width=100%>
					<tr bgcolor="#D6D6D6">
						<th>$traduction{$G_PAYS}{15}</th>
						<th>$traduction{$G_PAYS}{16}</th>
						<th>$traduction{$G_PAYS}{17}</th>
						<th>$traduction{$G_PAYS}{30}</th>
					</tr>
		|;
		while (@temp = $temp->fetchrow_array()) {
			if ($k%2==0) {
				$couleur2 = "";
			} else {
				$couleur2 = "#D6D6D6";
			}
			$temp[1] = sprintf("%.2f",$temp[1]);
			if ($temp[4] == 0) {
				$libref = $temp[3];
				$cout_reel_moy = "&nbsp;";
			} else {
				$libref = $temp[2];
				$cout_reel_moy = (&calcul_prix($temp[4]) * $temp[0]);
			}
			print qq|
					<tr bgColor='$couleur2' onmouseover="bgColor='$couleur'" onmouseout="bgColor='$couleur2'">
						<td>$libref</td>
						<td>$temp[1]</td>
						<td>$temp[0]</td>
						<td>$cout_reel_moy</td>
					</tr>
			|;
			$k++;
		}
		print "</table>";
	}
	print "</td></tr>";
	print "<tr><td></td></tr>";
	$result[3] = sprintf("%.2f",$result[3]);
	$result[4] = sprintf("%.2f",$result[4]);
	$result[7] = sprintf("%.2f",$result[7]);
	if ($G_PAYS eq "MA")
	{
	    print qq|<tr bgcolor="#D6D6D6"><th colspan="3">$traduction{$G_PAYS}{14}</th></tr>|;
    	print "<tr><td align=center colspan=3>$result[4]</td></tr>";
	}
	else
	{
	    print qq|<tr bgcolor="#D6D6D6"><th align=left>$traduction{$G_PAYS}{13}</th><th>$traduction{$G_PAYS}{23}</th><th align=right>$traduction{$G_PAYS}{14}</th></tr>|;
    	print "<tr><td align=left>$result[3]</td><td align=center>$result[7]</td><td align=right>$result[4]</td></tr>";
    }
	print qq|<tr bgcolor="#D6D6D6"><th colspan=3 align=left>$traduction{$G_PAYS}{22} $traduction{$G_PAYS}{13}</th></tr>|;
	print "<tr><td colspan=3 align=left>$result[5]</td></tr>";
	print qq|<tr bgcolor="#D6D6D6"><th colspan=3 align=left>$traduction{$G_PAYS}{19} $traduction{$G_PAYS}{20}</th></tr>|;
	print "<tr><td colspan=3 align=left>$result[6]</td></tr>";
	print qq|<tr><td colspan=3 align=middle><a href="#" onclick="window.close()">$traduction{$G_PAYS}{21}</a></td></tr>|;
	print "</table>";
	$temp->finish;
}

sub impression {
	$casseauto = $_[0];
	$histo = $_[1];

	if ($G_PAYS eq "ES") {
		$fich = "modeles/Casse_es.ps";
	} elsif ($G_PAYS eq "FR" || $G_PAYS eq "LU") {
		$fich = "modeles/Casse_fr.ps";
	} elsif ($G_PAYS eq "PT") {
		$fich = "modeles/Casse_pt.ps";
	} elsif ($G_PAYS eq "IT") {
		$fich = "modeles/Casse_it.ps";
	}

	$imprimante = $q->param('imprimante');
	($imprimante,$imprimauto) = split(/,/,$imprimante);
	$fichierps="";
	open (FIC, "$fich");
	while (<FIC>){
		$fichierps .= $_;
	}
	close(FIC);

	$temp = $BD->prepare(qq|select AGADRESSE, AGCP, AGVILLE, AGFAX, AGTEL, AGLIBELLE, CANUM, CONTRATCODE from CASSE left join CONTRAT on CONTRAT.CONTRATAUTO=CASSE.CONTRATAUTO left join AUTH.`AGENCE` on AGENCE.AGAUTO=CONTRAT.AGAUTO where CASSEAUTO="$casseauto"|);
	$temp->execute();
	@agence=$temp->fetchrow_array();
	$temp->finish;
	$temp = $BD->prepare(qq|select CLSTE, CLADRESSE, CLTEL, CLFAX, CLCONTACT, VICLCP, VICLNOM from CASSE left join CONTRAT on CONTRAT.CONTRATAUTO=CASSE.CONTRATAUTO left join CLIENT on CLIENT.CLAUTO=CONTRAT.CLAUTO left join AUTH.`VICL` on VICL.VICLAUTO=CLIENT.VICLAUTO where CASSEAUTO="$casseauto"|);
	$temp->execute();
	@client = $temp->fetchrow_array();
	$temp->finish;
	$temp = $BD->prepare(qq|select CHLIBELLE, CHADRESSE, MOMADESIGNATION, CONCAT_WS("",MACHINE.MANOPARC_AUX,MACHINE.MANOPARC) AS MANOPARC, VICP, VINOM, CHCONTACT from CASSE left join CONTRAT on CONTRAT.CONTRATAUTO=CASSE.CONTRATAUTO left join CHANTIER on CHANTIER.CHAUTO=CONTRAT.CHAUTO left join VILLECHA on VILLECHA.VILLEAUTO=CHANTIER.VILLEAUTO left join AUTH.`MACHINE` on CONTRAT.MAAUTO=MACHINE.MAAUTO left join AUTH.`MODELEMACHINE` on MACHINE.MOMAAUTO=MODELEMACHINE.MOMAAUTO where CASSEAUTO="$casseauto"|);
	$temp->execute();
	@chantier = $temp->fetchrow_array();
	$temp->finish;
	$temp = $BD->prepare(qq|select CAKMEU, CAOBJET, CATRAVAUX1, CAPXEU, CAPXFINALEU, CASSE.ETCODE, CONTRAT.CONTRATDUREECASSE, MOOBJET from CASSE left join CONTRAT on CONTRAT.CONTRATAUTO=CASSE.CONTRATAUTO where CASSEAUTO="$casseauto"|);
	$temp->execute();
	@casse = $temp->fetchrow_array();
	$temp->finish;

	#-------ENTETE---------------------------------
	$labonnedate = &get_date(1);
	$fichierps =~ s/%DATE%/$labonnedate/;

	$agence[0] =~ s/\(/\\\(/g;
	$agence[0] =~ s/\)/\\\)/g;
	$fichierps =~ s/%AGADRESSE%/$agence[0]/;
	$fichierps =~ s/%AGCP%/$agence[1]/;

	$agence[2] =~ s/\(/\\\(/g;
	$agence[2] =~ s/\)/\\\)/g;
	$fichierps =~ s/%AGVILLE%/$agence[2]/;
	$fichierps =~ s/%AGFAX%/$agence[3]/;
	$fichierps =~ s/%AGTEL%/$agence[4]/;

	$agence[5] =~ s/\(/\\\(/g;
	$agence[5] =~ s/\)/\\\)/g;
	$fichierps =~ s/%AGLIBELLE%/$agence[5]/;
	$fichierps =~ s/%NUMDOC%/$agence[6]/;
	$fichierps =~ s/%NUMCONTRAT%/$agence[7]/;
	#----------------------------------------------
	$fichierps =~ s/%AGENCE%//;
	$fichierps =~ s/%PEPRENOM%/$G_PEPRENOM/;
	$fichierps =~ s/%PENOM%//;
	#----------------------------------------------

	$client[0] =~ s/\(/\\\(/g;
	$client[0] =~ s/\)/\\\)/g;
	$fichierps =~ s/%CLSTE%/$client[0]/;

	$client[1] =~ s/\(/\\\(/g;
	$client[1] =~ s/\)/\\\)/g;
	$fichierps =~ s/%CLADRESSE%/$client[1]/;
	$fichierps =~ s/%CLTEL%/$client[2]/;
	$fichierps =~ s/%CLFAX%/$client[3]/;
	$fichierps =~ s/%CLCONTACT%/$chantier[6]/;
	$fichierps =~ s/%VICLCP%/$client[5]/;
	$fichierps =~ s/%VICLNOM%/$client[6]/;
	#----------------------------------------------

	$chantier[0] =~ s/\(/\\\(/g;
	$chantier[0] =~ s/\)/\\\)/g;
	$fichierps =~ s/%CHLIBELLE%/$chantier[0]/;

	$chantier[1] =~ s/\(/\\\(/g;
	$chantier[1] =~ s/\)/\\\)/g;
	$fichierps =~ s/%CHADRESSE%/$chantier[1]/;
	$fichierps =~ s/%MOMADESIGNATION%/$chantier[2]/;

	$chantier[3] =~ s/\(/\\\(/g;
	$chantier[3] =~ s/\)/\\\)/g;
	$fichierps =~ s/%MANOPARC%/$chantier[3]/;
	$fichierps =~ s/%VICP%/$chantier[4]/;
	$fichierps =~ s/%VINOM%/$chantier[5]/;
	#----------------------------------------------
	$casse[0] = sprintf("%.2f",$casse[0]);
	$fichierps =~ s/%CAKMEU%/$casse[0]/;
	$G_MOD = $G_MOD ? $G_MOD : 40;
	$fichierps =~ s/%TARIFMO%/$G_MOD/;
	$fichierps =~ s/%TARIFDEP%/$G_KM/;

	$casse[7] =~ s/\(/\\\(/g;
	$casse[7] =~ s/\)/\\\)/g;
	$borne_comment = 50;
	if (length($casse[7]) > $borne_comment) {
		$borne = length($casse[7]) / $borne_comment > int(length($casse[7]) / $borne_comment)?int(length($casse[7]) / $borne_comment) + 1:int(length($casse[7]) / $borne_comment);
		$borne = 5 if $borne > 5;
		$enplus = 0;
		for ($o = 0;$o < $borne ; $o++) {
			$effet = substr($casse[7],$enplus,$borne_comment);
			$enplus += $borne_comment;
			$fichierps =~ s/%COMMENT%/$effet/;
		}
		$fichierps =~ s/%COMMENT%//g;
	} else {
		$fichierps =~ s/%COMMENT%/$casse[7]/;
	}
	##############################
	$casse[1] =~ s/\(/\\\(/g;
	$casse[1] =~ s/\)/\\\)/g;
	$borne_comment = 100;
	if (length($casse[1]) > $borne_comment) {
		$borne = length($casse[1]) / $borne_comment > int(length($casse[1]) / $borne_comment)?int(length($casse[1]) / $borne_comment) + 1:int(length($casse[1]) / $borne_comment);
		$borne = 3 if $borne > 3;
		$enplus = 0;
		for ($o = 0;$o < $borne ; $o++) {
			$effet = substr($casse[1],$enplus,$borne_comment);
			$enplus += $borne_comment;
			$fichierps =~ s/%CAOBJET%/$effet/;
		}
		$fichierps =~ s/%CAOBJET%//g;
	} else {
		$fichierps =~ s/%CAOBJET%/$casse[1]/;
	}
	##############################
	$casse[2] = sprintf("%.2f",$casse[2]);
	$fichierps =~ s/%MO%/$casse[2]/;
	$casse[3] = sprintf("%.2f",$casse[3]);
	$fichierps =~ s/%PXTE%/$casse[3]/;
	$casse[4] = sprintf("%.2f",$casse[4]);

	if ($casse[5] eq "CAS02") {
		$fichierps =~ s/%PXRTE%/$casse[4]/;

		if ($casse[3] != 0) {
			$remise = (1 - ($casse[4] / $casse[3])) * 100;
		} else {
			$remise = 0;
		}
		if ($remise == 0) {
			$fichierps =~ s/%REMISE%//;
			$remise = "";
		} else {
			if ($remise >= 0) {
				if ($G_PAYS eq "FR" || $G_PAYS eq "LU") {
					$fichierps =~ s/%REMISE%/REMISE/;
				} elsif ($G_PAYS eq "ES") {
					$fichierps =~ s/%REMISE%/DESCUENTO/;
				} elsif ($G_PAYS eq "PT") {
					$fichierps =~ s/%REMISE%/DIMINUI��O/;
				} elsif ($G_PAYS eq "IT") {
					$fichierps =~ s/%REMISE%/RIDUZIONE/;
				}
			} else {
				if ($G_PAYS eq "FR" || $G_PAYS eq "LU") {
					$fichierps =~ s/%REMISE%/SURPLUS/;
				} elsif ($G_PAYS eq "ES") {
					$fichierps =~ s/%REMISE%/EXCEDENTE/;
				} elsif ($G_PAYS eq "PT") {
					$fichierps =~ s/%REMISE%/EXCESSO/;
				} elsif ($G_PAYS eq "IT") {
					$fichierps =~ s/%REMISE%/ECCEDENZA/;
				}
			}
			$remise = sprintf("%.2f",$remise);
			$remise .= " %";
		}
	} elsif ($histo) {
		if ($casse[5] eq "CAS03") {
			$fichierps =~ s/%PXRTE%/$casse[4]/;
			$casse[6] = sprintf("%.0f",$casse[6]);
			$remise = "+$casse[6]";
			if ($G_PAYS eq "FR" || $G_PAYS eq "LU") {
				$fichierps =~ s/%REMISE%/Jours de loc./;
			} elsif ($G_PAYS eq "ES") {
				$fichierps =~ s/%REMISE%/Dias de emplear/;
			} elsif ($G_PAYS eq "PT") {
				$fichierps =~ s/%REMISE%/Dias de aluguer/;
			} elsif ($G_PAYS eq "IT") {
				$fichierps =~ s/%REMISE%/Giorni de noleggio/;
			}
		} elsif ($casse[5] eq "CAS05") {
			$fichierps =~ s/%PXRTE%/$casse[4]/;

			if ($casse[3] != 0) {
				$remise = (1 - ($casse[4] / $casse[3])) * 100;
			} else {
				$remise = 0;
			}
			if ($remise == 0) {
				$fichierps =~ s/%REMISE%//;
				$remise = "";
			} else {
				if ($remise >= 0) {
					if ($G_PAYS eq "FR" || $G_PAYS eq "LU") {
						$fichierps =~ s/%REMISE%/REMISE/;
					} elsif ($G_PAYS eq "ES") {
						$fichierps =~ s/%REMISE%/DESCUENTO/;
					} elsif ($G_PAYS eq "PT") {
						$fichierps =~ s/%REMISE%/DIMINUI��O/;
					} elsif ($G_PAYS eq "IT") {
						$fichierps =~ s/%REMISE%/RIDUZIONE/;
					}
				} else {
					if ($G_PAYS eq "FR" || $G_PAYS eq "LU") {
						$fichierps =~ s/%REMISE%/SURPLUS/;
					} elsif ($G_PAYS eq "ES") {
						$fichierps =~ s/%REMISE%/EXCEDENTE/;
					} elsif ($G_PAYS eq "PT") {
						$fichierps =~ s/%REMISE%/EXCESSO/;
					} elsif ($G_PAYS eq "IT") {
						$fichierps =~ s/%REMISE%/ECCEDENZA/;
					}
				}
				$remise = sprintf("%.2f",$remise);
				$remise .= " %";
			}
		} else {
			$fichierps =~ s/%REMISE%//;
			$remise = "";
			$fichierps =~ s/%PXRTE%/$casse[3]/;
		}
	}

	$fichierps =~ s/%REM%/$remise/;
	$fichierps =~ s/%CAOBJET%//g;
	$fichierps =~ s/%COMMENT%//g;

	$fich_impr = $fichierps;

	#----------------------------------------------
	$temp = $BD->prepare(qq|select DETAILSCASSEQTE, DETAILSCASSEPXE, '', LIBDIVERS, DETAILSCASSE.AIAUTO from DETAILSCASSE where CASSEAUTO="$casseauto" and CASSEST=1|);
	$temp->execute();
	$rows_art = $temp->rows;
	$compteur = 0;
	$nombre   = 0;
	$nbpages = $rows_art / 6>int($rows_art / 6)?int($rows_art / 6)+1:int($rows_art / 6);
	$page_cours = 1;

#	open(IMP, "| lpr -P $imprimante");
	while (@temp = $temp->fetchrow_array()) {
  		$fich_impr =~ s/%Q%/$temp[0]/;
		$temp[1] = sprintf("%.2f",$temp[1]);
		$fich_impr =~ s/%PE%/$temp[1]/;
		if ($temp[4] == 0) {
			$fich_impr =~ s/%DES%/$temp[3]/;
		} else {
			$fich_impr =~ s/%DES%/$temp[2]/;
		}

		$compteur++;

		if ($compteur == 6) {
			$nombre += $compteur;

			if ($rows_art != $nombre) {
				$compteur = 0;
				$fich_impr =~ s/%NB%/$page_cours \/ $nbpages/g;
				$page_cours++;
			}

			$fich_impr =~ s/%DES%//g;
  			$fich_impr =~ s/%Q%//g;
			$fich_impr =~ s/%PE%//g;

#			print IMP $fich_impr;

			$fich_impr = $fichierps;
		}
	}

	if ($compteur != 6) {
		$fich_impr =~ s/%DES%//g;
		$fich_impr =~ s/%Q%//g;
		$fich_impr =~ s/%PE%//g;

		if ($page_cours > 1) {
			$fich_impr =~ s/%NB%/$page_cours \/ $nbpages/g;
		} else {
			$fich_impr =~ s/%NB%//g;
		}

#		print IMP $fich_impr;

	}

#	close(IMP);
	$BD->do(qq|update AUTH.`PERSONNEL` set IMPRIMAUTO=$imprimauto where PEAUTO=$G_PEAUTO|);
	$temp->finish;
	##Envoi vers le systeme d'impression PHP
	my $encode64 = encode_base64("casse;$casseauto;$remise;;$G_PEAUTO;$imprimante;;$G_DEV", "");
# 	if ( $G_ENVIR eq '' )
# 	{
# 		$shell_cmd = "| export PHP_AUTH_USER=$G_PELOGIN; export PHP_AUTH_PW=$G_PEPASSWORD; php /home/web/portail.acces-industrie.com/htdocs/PRINT/c/impression.php $encode64";
# 	}
# 	if ( $G_ENVIR eq 'int' )
# 	{
# 		$shell_cmd = "| export PHP_AUTH_USER=$G_PELOGIN; export PHP_AUTH_PW=$G_PEPASSWORD; php /home/web/portail-int.acces-industrie.com/htdocs/PRINT/c/impression.php $encode64";
# 	}
# 	if ( $G_ENVIR eq 'dev' )
# 	{
# 		$shell_cmd = "| export PHP_AUTH_USER=$G_PELOGIN; export PHP_AUTH_PW=$G_PEPASSWORD; php /home/web/portail-dev.acces-industrie.com/htdocs/PRINT/c/impression.php $encode64";
# 	}
#    $shell_cmd = "| export PHP_AUTH_USER=$G_PELOGIN; export PHP_AUTH_PW=$G_PEPASSWORD; php $IMP_HOME/c/impression.php $encode64";

	open(IMP2, $shell_cmd);
	close(IMP2);
}

sub Entete {
	$entete = &hauttab;
	$en = qq|
<HTML>
<HEAD>
<TITLE>$_[0]</TITLE>
<link rel=stylesheet href="$URL_CONF/base.css" type="text/css">
<script language="JavaScript" src="$URL_CONF/super.js?bld=$build"></script>
<style>
TD {font-size:8pt};
TH {font-size:8pt;}
</style>
</HEAD>
<BODY>
<table bgcolor="C6CDC5" border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td><p align="left"><font class="TITRE"><b>$_[0]</b></font></td>
		$entete
	</tr>
</table>
<font><b>$G_PEPRENOM, $G_PENOM</b></font>
|;

print $en;
}

sub hauttab {
	$tableau  = qq{<td width="54">\n};
	$tableau .= qq{<a href="javascript:history.back(1)"><img src="$URL_IMG/img_2.gif" align="MIDDLE" border="0" width="54" height="36" alt=$mess[152]></a>\n};
	$tableau .= qq{
<SCRIPT Language="Javascript">
function printit() {
	if (NS) {
		window.print() ;
	} else {
		var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
		document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
		WebBrowser1.ExecWB(6, 2);
		// Use a 1 vs. a 2 for a prompting dialog box
		WebBrowser1.outerHTML = "";
	}
}

var NS = (navigator.appName == "Netscape");
var VERSION = parseInt(navigator.appVersion);

if (VERSION > 3) {
	document.write('</td><td width="54"><a href="javascript:printit()"><img src="$URL_IMG/img_3.gif" align="MIDDLE" border="0" width="54" height="36" alt="$mess[300]"></a>');
}
</SCRIPT>
};

	$tableau .= qq{</td><td width="48"><a href="javascript:window.close()"><img src="$URL_IMG/img_4.gif" align="MIDDLE" border="0" width="48" height="36" alt="$mess[301]"></a>\n};
	$tableau .= qq{</td>\n};
	return $tableau;
}

sub calcul_cout {
	# Penser � la remise pour les co�ts r�els
	$temp0 = $BD->prepare(qq|select AIAUTO, DETAILSCASSEQTE from DETAILSCASSE where CASSEAUTO=$_[0] and AIAUTO != 0 and CASSEST=1|);
	$temp0->execute();
	$rows = $temp0->rows;

	if ($rows <= 0)
	{
       $temp0->finish;
	   return 0;
	}

	$totalfinal = 0;
	while (@temp0 = $temp0->fetchrow_array()) {
		$totalfinal += (&calcul_prix($temp0[0]) * $temp0[1]);
	}
	$temp0->finish;
	$totalfinal = sprintf("%.2f", $totalfinal);

	return $totalfinal;
}

sub calcul_prix {
	$requ0 = $BD->prepare(qq|select ARPRIXEUR, ARCONV FROM ARTICLE WHERE AIAUTO=$_[0] and AROK=1|);
	$requ0->execute();

	$total = 0;
	$moy   = 0;
	while (@tot = $requ0->fetchrow_array()) {
		$total += ($tot[0] / $tot[1]);
		$moy++;
	}

	$requ0->finish;
	my $prixee=0;

	if ($moy!=0) {
		$prixee=$total / $moy;
	}

	return $prixee;
}
