#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';

use CGI;
use autentif;
use securite;
use titanic;
use accesbd;
use varglob;
use message;
use calculdate;
use Date::Calc qw(Day_of_Week  Delta_Days Add_Delta_Days);
use tracetemps;
use fonctions;
use LOC::Globals;
use LOC::Request;
use LOC::Appeal::Type;
use LOC::Common::Rent;

# Initialisation du chrono
my $timer = &init_chrono();
my $build = &LOC::Globals::get('build');

$|=1;

#connexion
my $DB = &Environnement("STATS");

$q = new CGI;
$q->autoEscape(undef);
my @mess=&message($G_PAYS);

#recuperation taux de change
my $tauxE = tauxLocalToEuro($G_PAYS);

$client = $q->param('value');
$client = int($client);
$clnom = $q->param('value');

if ($client ne $clnom)  {
	$MYBD=$DB->prepare("SELECT CLAUTO FROM CLIENT WHERE CLSTE = '$clnom'");
	$MYBD->execute();
	$client=$MYBD->fetchrow_array;
	$MYBD->finish;
}
my $requete = qq{
SELECT CLSTE, concat_ws(" ", CLADRESSE, CLADRESSE2) as CLADRESSE, VICLCP, VICLNOM, CLSIREN, CLCONTACT, CLTEL, CLFAX,
    CLPORTABLE, CLEMAIL, CLAPE, CLCOMMENTAIRE, MEACTIVITE, CLCODE, TARIFCLIENTREF, CLTYPEASSURANCE, CLNETTOYAGE, CLDECHETSFACT,
    CLGRLIBELLE, CLCADRE, CLPASSURANCE, CLQUALIFICATION, CLTYPERECOURS
FROM CLIENT
LEFT JOIN AUTH.`VICL` ON VICL.VICLAUTO=CLIENT.VICLAUTO
LEFT JOIN METIER ON METIER.METIERAUTO=CLIENT.METIERAUTO
LEFT JOIN CLIENTTARIF ON CLIENTTARIF.CLIENTTARIFAUTO=CLIENT.CLIENTTARIFAUTO
LEFT JOIN $GESCOM_BASE{"$G_PAYS"}.GC_CLGROUPE ON GC_CLGROUPE.CLGROUPEAUTO=CLIENT.CLGROUPEAUTO
WHERE CLAUTO = '$client'};

$MYBD=$DB->prepare($requete);
$MYBD->execute();
($nomclient, $addresclient, $cpclient, $villeclient, $sirenclient, $contactclient, $telclient, $faxclient, $portableclient,
 $mailclient, $apeclient, $commentclient, $metierclient, $codeclient, $cattarif, $assurance, $nettoyage, $residues, $groupe, $clcadre,
 $clpassurance, $qualification, $appealTypeId) = $MYBD->fetchrow_array;
$MYBD->finish;

$addresclient .= "<br>".$cpclient." ".$villeclient;
$nomclient =~ s/ /&nbsp;/g;

if ($metierclient eq "")
{
    $MYBD=$DB->prepare(qq{SELECT APELIBELLE FROM CLIENT LEFT JOIN APE2 ON APE2.CODEAPE=CLIENT.CLAPE WHERE CLAUTO = '$client'});
    $MYBD->execute();
    ($metierclient)=$MYBD->fetchrow_array;
    $MYBD->finish;
}
my %totag;
my %caag;

my %nbcont;
my %catot;

my $nbjoursmois = 21;
my $taux = 0.0192;

my ($sec, $min, $hr, $jour, $mois, $an, $joursm, $jouran, $ete)=localtime;
$mois +=1;

my $anc =$an+1900;
my $anp =$an+1899;

my @agen=();
my $sth=$DB->prepare(qq{select AGAUTO FROM AUTH.`AGENCE` WHERE PACODE="$G_PAYS" ORDER BY AGAUTO});
$sth->execute();

while (my @data=$sth->fetchrow_array)
{
	push (@agen, $data[0]);
	for (my $i = 0; $i <= 12; $i++)
	{
		$totag{$data[0]}->[$i] = 0;
		$caag{$data[0]}->[$i]  = 0;
	}
 	$nbcont{$data[0]} = 0;
	$catot{$data[0]}  = 0;
}
$sth->finish;

# Assurance par d�faut
$assurance = &aff_assurance("assurance", $assurance, 2, $clpassurance);

# Gestion de recours :
my $appealLabel;
if ($appealTypeId)
{
    my $tabAppealInfos = &LOC::Appeal::Type::getInfos($G_PAYS, $appealTypeId);
    my $appealValue = &LOC::Characteristic::getCountryValueByCode('PCRECOURS', $G_PAYS) * 100;
    $appealLabel = $tabAppealInfos->{'fullLabel'};
    $appealLabel =~ s/<%percent>/$appealValue %/;
}
else
{
    $appealLabel = $mess[3059];
}

my %img = ("0" => "down.gif", "-1" => "up.gif");
my $image = qq{<img src="$URL_IMG/main_news.gif" border="0" align="absmiddle" />&nbsp;};

# page html
print qq{content-type:text/html\n\n
<html>
<head>
<title>$mess[4450]</title>
<meta http-equiv="Content-Type" content="text/html">
<link rel=stylesheet href="$URL_CONF/base.css" type="text/css">
<style type="text/css">
    table > tbody > tr > td
    {
        padding: 2px 5px 2px 2px;
    }
    fieldset
    {
        margin-top: 10px;
    }
    fieldset > legend
    {
        font-weight: bold;
    }
</style>
<script type="text/javascript" src="$URL_CONF/super.js?bld=$build"></script>
</head>

<body>
<script type="text/javascript" src="$URL_CONF/wz_tooltip.js"></script>
<table width="100%" border="0">
    <tr>
        <td>
            <table border="0"><tr><td align="center">
                <font style="font-family: Impact, Arial, Helvetica, Sans-Serif; font-size: 18pt; color: #0039d6;">$nomclient</font><BR />\n};
if ($groupe ne "")
{
    print qq{        <font style="color: #0039d6; font-weight: bold;">($mess[2366]&nbsp;$groupe)</font><BR />\n};
}
print qq{            $codeclient<BR />
            </td></tr></table>
        </td>
        <td align="right" nowrap>
            <font style="font-size: 14pt;">$cattarif</font>};

print qq{
            &nbsp;
            <a href="javascript:;" onclick="return fullrs('tarif_}.sprintf('%.0f', rand 1000).qq{', '} . $ENV{'TFL_URL'} . qq{/c/grille_consult.php?clauto=$client')"><img src="$URL_IMG/tarif.gif" align="absmiddle" border="0" alt="$mess[6667]" /></a>};

print qq{
        </td>
    </tr>
</table>

<fieldset>
    <legend>$mess[4452]</legend>
    $addresclient<BR />
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr><td>$mess[4457]</td><td>$mess[4458]</td></tr>
        <tr><td>$telclient</td><td>$faxclient</td></tr>
    </table>

    <table style="margin-top: 10px;">
        <tr>
            <td>$mess[4456]</td>
            <td>$contactclient</td>
        </tr>
        <tr>
            <td>$mess[4459]</td><td>$portableclient</td>
        </tr>
        <tr>
            <td>$mess[4460]</td><td><a href="mailto:$mailclient" ">$mailclient</a></td>
        </tr>
    </table>
</fieldset>

<fieldset>
    <legend>$mess[4450]</legend>
    <table>
        <tr>
            <td>$mess[4453]</td>
            <td>$sirenclient</td>
        </tr>
        <tr>
            <td>$mess[4454]</td>
            <td>$apeclient</td>
        </tr>
        <tr>
            <td>$mess[4455]</td>
            <td>$metierclient</td>
        </tr>
    </table>
</fieldset>

<fieldset>
    <legend>$mess[6645]</legend>
    <table cellspacing="0" cellpadding="0">
        <tr>
            <td>$mess[3026]&nbsp;&nbsp;&nbsp;</td>
            <td>$assurance</td>
        </tr>
        <tr>
            <td>$mess[3056]&nbsp;&nbsp;&nbsp;</td>
            <td>$appealLabel</td>
        </tr>
        <tr>
            <td>$mess[3031]&nbsp;&nbsp;&nbsp;</td>
            <td><img src="$URL_IMG/$img{$nettoyage}" alt="$mess[3034 + $nettoyage]" border="0" align="absmiddle" /></td>
        </tr>
        <tr>
            <td>$mess[3060]&nbsp;&nbsp;&nbsp;</td>
            <td><img src="$URL_IMG/$img{$residues}" alt="$mess[3034 + $residues]" border="0" align="absmiddle" /></td>
        </tr>
        <tr>
            <td>$mess[2367]&nbsp;&nbsp;&nbsp;</td><td>$qualification</td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0">

    </table>
    <table cellspacing="0" cellpadding="0">
        <tr><td colspan="2">$mess[4461]</td></tr>
        <tr><td width="30"></td><td>$commentclient</td></tr>
    </table>
</fieldset>

<fieldset>
    <legend>$mess[4462]</legend>
    <a name="stats">
    <table cellspacing="0" cellpadding="0">
};

my $statistique=$q->param('stat');
$statistique = ($statistique eq "") ? "2" : $statistique;

print qq{<tr><td>};
print $image    if ($statistique eq "2");
print qq{</td><td><a href="$HTTP_PORTAIL/cgi-bin/old/location/infoclient.cgi?value=$client&stat=2#stats">Contrats en cours</a></td></tr>\n};
print qq{<tr><td>};
print $image    if ($statistique eq "3");
print qq{</td><td><a href="$HTTP_PORTAIL/cgi-bin/old/location/infoclient.cgi?value=$client&stat=3#stats">Familles machines</a></td></tr>\n};
print qq{<tr><td>};
print $image    if ($statistique eq "0");
print qq{</td><td><a href="$HTTP_PORTAIL/cgi-bin/old/location/infoclient.cgi?value=$client&stat=0#stats">Facturation (hors avoirs)</a></td></tr>\n};
print qq{<tr><td>};
print $image    if ($statistique eq "1");
print qq{</td><td><a href="$HTTP_PORTAIL/cgi-bin/old/location/infoclient.cgi?value=$client&stat=1#stats">Devis</a></td></tr>\n};

print qq{<table>
<br>
<table cellspacing="2" cellpadding="2" border="0">\n};


if ($statistique eq "0")
{
	&aff_facture();
}

if ($statistique eq "1")
{
	&stat_devis();
}

if ($statistique eq "2")
{
	&stat_encours();
}

if ($statistique eq "3")
{
	&stat_familles();
}

print qq{
</table>
</fieldset>
</a>
</form>
};

$DB->disconnect;

print qq{<table width="100%"><tr valign="middle">\n};
print qq{<td align="right"><BUTTON onClick="window.close()">$mess[2080]</BUTTON></td>\n};
print qq{</tr></table>\n};

print qq{</body>\n};
print qq{</html>\n};


sub statjr
{
	my @param = @_;
	
	my $agencyId = $param[0];
	my $requete = qq{
SELECT
    CONTRAT.CONTRATAUTO,
    CONTRATCODE,
    CLIENT.CLCODE,
    CHLIBELLE,
    VICP, VINOM,
    MANOPARC,
    FAMAMODELE,
    MOMADESIGNATION,
    CONTRATDD, CONTRATDR,
    CONTRATJOURSPLUS, CONTRATJOURSMOINS,
    MOPXJOURFR ,
    MOTARIFMSFR ,
    CONTRATTYPEASSURANCE,
    CONTRATPASSURANCE,
    MOASSFR,
    MOTRANSPORTDFR, MOTRANSPORTRFR,
    MOHTFRS,
    CAMONTANTFR, CAQTE,
    CONTRAT.FAMAAUTO,
    CONTRAT.MAAUTO,
    CONTRAT.CLAUTO,
    CONTRAT.CHAUTO,
    CONTRAT.PEAUTO,
    CONTRAT.MOAUTO,
    CONTRAT.CARBUAUTO,
    CONTRAT.SOLOAUTO,
    CONTRATDATECREATION,
    CONTRATVERIFIER,
    CONTRAT.LIETCONTRATAUTO,
    CONTRAT.MOMAAUTO,
    CONTRATTRANSPLAN,
    CONTRATDUREE,
    CONTRATINDICE,
    MACHINE.ETCODE,
    CLIENT.CLCOLLECTIF,
    CONTRAT.AGAUTO,
    MOFORFAITFR,
    CONTRATNUMCOMM,
    FAMAENERGIE,
    MODECHETS,
    NETMONTANT,
    CONTRATTYPERECOURS,
    CONTRATRECOURSVAL
FROM CONTRAT
    LEFT JOIN CLIENT ON CLIENT.CLAUTO = CONTRAT.CLAUTO
    LEFT JOIN MONTANT ON MONTANT.MOAUTO = CONTRAT.MOAUTO
    LEFT JOIN CHANTIER ON CHANTIER.CHAUTO = CONTRAT.CHAUTO
    LEFT JOIN VILLECHA ON VILLECHA.VILLEAUTO = CHANTIER.VILLEAUTO
    LEFT JOIN CARBURANT ON CARBURANT.CARBUAUTO = CONTRAT.CARBUAUTO
    LEFT JOIN AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO = CONTRAT.MOMAAUTO
    LEFT JOIN AUTH.`MACHINE` ON MACHINE.MAAUTO = CONTRAT.MAAUTO
    LEFT JOIN FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO = MODELEMACHINE.FAMAAUTO
    LEFT JOIN CONTRATNETTOYAGE ON CONTRAT.CONTRATAUTO = CONTRATNETTOYAGE.CONTRATAUTO
    LEFT JOIN NETTOYAGE ON CONTRATNETTOYAGE.NETAUTO = NETTOYAGE.NETAUTO
WHERE
    CONTRAT.AGAUTO = "agence"
    AND CONTRAT.CLAUTO = "$client"
    AND CONTRATOK = "-1"
    AND (CONTRAT.LIETCONTRATAUTO ="2" OR CONTRAT.LIETCONTRATAUTO="3")
    AND CONTRATDD <= "finms" AND CONTRATDR >=  "departms"
};
	my $req = $requete;

	$req =~ s/agence/$agencyId/;
	my ($sec,$min,$hr,$jour,$mois,$an,$joursm,$jouran,$ete) = localtime;
	my($jrdate,$msdate,$andate) = ($jour,$mois,$an);
	$andate += 1900;
	$msdate += 1;
	$an     += 1900;
	$mois   += 1;

	my $debutms = sprintf("%04d:%02d:%02d", $an, $mois, 1);
	my $bisexar = &bisextile($an);
 	my $ms = $mois % 2;

	my  $finmois;
	if ($mois <= 7)
	{
	    if (($ms == 1) || ($mois == 7))
	    {
	       $finmois = 31;
        }
        else
        {
            if ($mois == 2)
            {
				if ($bisexar == 1)
				{
				    $finmois=29;
				}
				else
				{
				    $finmois=28;
				}
			}
			else
			{
			    $finmois=30;
			}
		}
	}
	else
	{
	    if ((($ms == 1) || ($mois == 7))&& ($mois>=8))
	    {
	        $finmois=30;
	    }
	    else
	    {
	        $finmois=31;
	    }
	}

	my $finms = sprintf("%04d:%02d:%02d", $an, $mois, $finmois);

	my ($andm,$msdm,$jrdm)=split(/:/,$debutms);

	my $dm=Delta_Days(1900,1,1,$andm,$msdm,$jrdm);

	my ($anfm,$msfm,$jrfm)=split(/:/,$finms);

	my $fm=Delta_Days(1900,1,1,$anfm,$msfm,$jrfm);
	my $jourmois=Delta_Days($andm,$msdm,$jrdm,$anfm,$msfm,$jrfm);
	my $jourmoisd=$jourmois;
	my $jourmsouv=$jourmois;
	my $uvfer=&jour_fer_ag($G_PAYS,$agencyId,$jrdm,$msdm,$andm,$jourmsouv);
	$jourmsouv +=1;
	my $ouvrwe=&jour_we($jourmsouv,$jrdm,$msdm,$andm);


	$jourmsouv =$jourmsouv-$ouvrwe-$uvfer;

	$req=~ s/finms/$finms/;
	$req=~ s/departms/$debutms/;
	
	my $sth=$DB->prepare($req);
	$sth->execute();
	my $nbrow=$sth->rows;

	my $totalag=0.0;

	my $nbcont=0;
	my $nbcontcours=0;
	my $nbcontarret=0;
	my $nbdureems=0;
	my $nbdureeplus=0;
	my $nbforfait=0;
	my $nbtarif=0;
	my $nbtarifms=0;
	my $nbasscomp=0;
	my $total=0.0;
	my @mtcontrat;
	my @dur;
	my @mtass;
	my $nbass=0;
	my @qtcarbu;
	my $nbcarb=0;
	my @soustrait;
	my $nbsout=0;
	my @transport;
	my $nbtr=0;

	#modele
	my @nummod;
	my @modele;
	my @nbcont;
	my @modur;
	my @modmont;
	my @modparc;
	my $nbmod=0;

	#famille
	my @numfam;
	my @famille;
	my @nbcontf;
	my @modurf;
	my @modmontf;
	my @modparcf;
	my $nbfam=0;

    my @mtforfait;
    while (my @data=$sth->fetchrow_array())
	{
		my @val;
		my @reqt;
		my @ident;
		my $stt=$data[43];


		my ($jrdd,$msdd,$andd) = &datetojjmsan($data[9]);

		my $dd = Delta_Days(1900,1,1,$andd,$msdd,$jrdd);

		my ($jrdf,$msdf,$andf) = &datetojjmsan($data[10]);
		my $df = Delta_Days(1900,1,1,$andf,$msdf,$jrdf);
		my $dureecont = $df-$dd+1;

		my $boolms  = 0;
		my $ta      = 0.0;
		my $tr      = 0.0;
		my $carbu   = 0.0;
		my $ddf     = $data[9];
		my $dff     = $data[10];
		my $jrplus  = 0.0;
		my $jrmoins = 0.0;
		my $tarif   = $data[13];
		my $tarifm  = $data[14];
		my $forfait = $data[41];
		my $boole   = 0;
		if ($tarifm != 0)
		{
			$boole = 1;
			$tarif = $tarifm;
			$nbtarifms++;
		}
		if ($forfait != 0)
		{
			$boole = 2;
			$tarif = $forfait;
			$jourmois = $dureecont;
			$mtforfait[$nbforfait] = $forfait;
			$nbforfait++;
		}

		if ($dd < $dm)
		{
			$ddf = $debutms;
		}
		else
		{
			$ta = $data[18];
			$transport[$nbtr] = $ta;
			$nbtr++;
		}

		if ($df > $fm)
		{
			$dff = $finms;
			$boolms = 1;
		}
		else
		{
			$carbu = $data[21];
			if ($carbu != 0)
			{
				$qtcarbu[$nbcarb] = $carbu;
				$nbcarb++;
			}

			$tr = $data[19];
			$transport[$nbtr] = $tr;
			$nbtr++;
			$jrplus = $data[11];
			$jrmoins = $data[12];
		}

		my ($dda,$ddm,$ddj);
		my ($dfa,$dfm,$dfj);
		if ($ddf =~ /:/)
		{
			($dda,$ddm,$ddj) = split(/:/,$ddf);
		}
		else
		{
			($dda,$ddm,$ddj) = split(/-/,$ddf);
		}
		if ($dff =~ /:/)
		{
		    ($dfa,$dfm,$dfj) = split(/:/,$dff);
		}
		else
		{
			($dfa,$dfm,$dfj) = split(/-/,$dff);
		}

		my $duree = Delta_Days($dda,$ddm,$ddj,$dfa,$dfm,$dfj);
		my $dureeAss = $duree + 1;
		if ($boole == 0)
		{
			my  $nbferie=&jour_fer_ag($G_PAYS,$agencyId,$ddj,$ddm,$dda,$duree);
			my  $nbjrwe = 0;
			my $joursup = 0;
			my $testjoursup;
			$testjoursup = &num_jour($dfj,$dfm,$dfa);

			if ($testjoursup == 6) {$joursup=1};
			if ($testjoursup == 7) {$joursup=2};
			if ( $boolms != 0 )
			{
				$joursup = 0;
			}
			$duree += 1;
			$nbjrwe = &jour_we($duree,$ddj,$ddm,$dda);
			$duree  = $duree + $jrplus  - $jrmoins;
			$nbtarif++;

			$duree -= $nbferie;
			$duree  = $duree-$nbjrwe+$joursup;

			$dur[$nbcont] = $duree;
			if ($duree <= 5)
			{
				$nbdureems++;
			}
			else
			{
				$nbdureeplus++
			}

		}
		else
		{
			if ($boole == 2)
			{
				$duree += 1;
			}
			$dur[$nbcont] = $duree;
			if ($duree <= 5)
			{
				$nbdureems++;
			}
			else
			{
				$nbdureeplus++
			}
			if ($boole == 1)
		 	{
		 		$jourmois = $jourmoisd;
		 	}
			$duree = $duree / $jourmois;

			if ($boole == 1)
			{}

	    }
		my $ass = $data[15];
        my $tauxAss = $data[16];
        my $montantass = &LOC::Common::Rent::calculateInsurance($G_PAYS, $agencyId, $ass, $tauxAss, $data[13],
                                                                $ddf, $dff, $data[11], $data[12]);

        my $montantrecycl = $data[44];
        my $montantnett   = $data[45];

        my $appealAmount = 0;
        if ($data[46])
        {
            $appealAmount = &LOC::Common::Rent::calculateAppeal($G_PAYS, $agencyId, $data[46], $data[47], $data[13],
                                                                $ddf, $dff, $data[11], $data[12]);
        }


		$duree = sprintf("%.2f", $duree);
		my $montant = ($tarif * $duree);
		my $montotal = $montant + $ta + $tr + $carbu + $montantass + $appealAmount + $montantrecycl + $montantnett;

		$mtcontrat[$nbcont] = $montotal;
		$total += $montotal;

		if ($data[33] eq "2")
		{
			$nbcontcours++;
		}
		else
		{
			$nbcontarret++;
		}
		$nbcont++;
	}
	$sth->finish;

	$nbcont{$agencyId} += $nbcont;

	my $ca = sprintf("%.2f",$total);
	$catot{$agencyId} += $ca;

	return ($jrdate,$msdate,$andate,$nbcont,$ca);
}


sub stat_devis
{
	#nb contrat
	my $req = qq{select AGAUTO,count(*),MAX(CONTRATDD),MAX(CONTRATDR) FROM CONTRAT WHERE CLAUTO="$client"  AND CONTRATOK in ("-1","-2") GROUP BY AGAUTO ORDER BY AGAUTO};
	#devis
	my $nbmois = 6;
	my $req = qq{SELECT DEVIS.DEVISAUTO,DEVIS.AGAUTO,ETATTABLE.ETLIBELLE,CONCAT_WS(", ",PENOM,LEFT(PEPRENOM,1)),DATE_FORMAT(DEVIS.DEVISDATECREATION,"%d/%m/%Y")   FROM DEVIS LEFT join ETATTABLE ON  DEVIS.ETCODE=ETATTABLE.ETCODE LEFT join AUTH.`PERSONNEL` ON PERSONNEL.PELOGIN=DEVIS.PELOGIN WHERE DEVIS.CLAUTO="$client" AND DEVIS.DEVISDATECREATION>=DATE_SUB(NOW(), INTERVAL $nbmois MONTH) ORDER BY DEVIS.AGAUTO,DEVIS.DEVISDATECREATION DESC};

	$MYBD1  = $DB->prepare($req);
	$MYBD1->execute();
	my $nbr = $MYBD1->rows;
	print qq{<tr bgcolor="#C6CDC5">\n};
	print qq{<th><font class="PT">N� devis<br>($nbr devis sur $nbmois mois)</font></th>\n};
	print qq{<th><font class="PT">Agence</font></th>\n};
	print qq{<th><font class="PT">&Eacute;tat devis</font></th>\n};
	print qq{<th><font class="PT">Commercial</font></th>\n};
	print qq{<th><font class="PT">Date du devis</font></th>\n};
	print qq{</tr>\n};
	my $lk = 0;
	while(my @data = $MYBD1->fetchrow_array)
	{
		my $compt = $lk % 2;
		my $color = "''";
		if ($compt == 1)
		{
			$color = qq{'#D6D6D6'};
		}
		print qq{<tr bgColor=$color onmouseover="bgColor='#FFCC33'" onmouseout="bgColor=$color">\n};
		for(my $i = 0; $i < @data; $i++)
		{
			if ($i == 0)
			{
                my $urlDevis = &LOC::Request::createRequestUri('rent:rentEstimate:view', {'estimateId' => $data[$i]});
				print qq{<td nowrap><font class="PT"><a href="#" onclick="return fullrs('devis$data[$i]','$urlDevis')">$data[$i]</a></font></td>\n};
			}
			else
			{
				print qq{<td nowrap><font class="PT">$data[$i]</font></td>\n};
			}
		}
		print qq{</tr>\n};
		$lk++;
	}
	$MYBD1->finish;
}

sub stat_encours
{
	my %contrats;	# Nombre de contrats par agence
	my %montants;	# Montant total par agence

	my $total_contrats = 0;	# Nombre total de contrats
	my $total_montant = 0;	# Montant total de l'en cours

	# Ligne des agences
	my $ligne1 = qq{<tr bgcolor="#C6CDC5">\n};
	$ligne1 .= qq{<td><font class="PT"><b>Agence</b></font></td>\n};

	# Ligne des donn�es
	my $ligne2 = qq{<tr onmouseover="bgColor='#FFCC33'" onmouseout="bgColor=''">\n};
	$ligne2 .= qq{<td><font class="PT"><b>Nb de contrats<br>Montant</b></font></td>\n};

	# Calcul de l'en cours
	# (= montant total des contrats - montant total d�j� factur�)
	my $req=qq{
		select CONTRAT.AGAUTO, MOHTFRS-IFNULL(sum(ENTETEMONTANTFR), 0)
		from CONTRAT
		left join MONTANT
			on CONTRAT.MOAUTO=MONTANT.MOAUTO
		left join ENTETEFACTURE
			on CONTRAT.CONTRATCODE=ENTETEFACTURE.CONTRATCODE
		where CONTRATOK=-1
			and CONTRAT.ETATFACTURE!='F'
			and CLAUTO='$client'
			and LIETCONTRATAUTO in ('1','2','3','7')
		group by CONTRAT.CONTRATCODE
		order by AGAUTO
	};
	$MYBD1=$DB->prepare($req);
	$MYBD1->execute();

	# R�cup�ration des donn�es
	while(my @data = $MYBD1->fetchrow_array)
	{
		$contrats{$data[0]}++;
		$montants{$data[0]} += $data[1];
	}

	# Affichage du r�sultat dans le tableau, par agence
	foreach (keys %contrats)
	{
		$ligne1 .= qq{<td><font class="PT">$_</font></td>\n};
		$ligne2 .= qq{<td><font class="PT">$contrats{$_}<br><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">}
			.&fnombre($montants{$_}, 3, 2).qq{</span></font></td>\n};

		# Total sur toutes les agences
		$total_contrats += $contrats{$_};
		$total_montant += $montants{$_};
	}


	$MYBD1->finish;

	# Affichage du total
	$ligne1 .= qq{<td><font class="PT">Global</font></td>\n</tr>\n};
	$ligne2 .= qq{<td><font class="PT">$total_contrats<br><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">}
		.&fnombre($total_montant, 3, 2).qq{</span></font></td>\n</tr>\n};

	print $ligne1.$ligne2;
}

sub stat_familles
{
	my $texte = "Pas de statistique disponible";
	if ($G_PAYS eq "ES")
	{
		$texte = "No estadistica disponible";
	}
	elsif ($G_PAYS eq "PT")
	{
		$texte = "N�o estatistica disponivel";
	}
	elsif ($G_PAYS eq "IT")
	{
		$texte = "Non una statistica disponibile";
	}
	print qq{<tr>\n<td><font class="PT2"><b>$texte.</b></font></td>\n</tr>\n};
}

sub aff_facture
{
	#facture de l'ann�e en cours
	my $option = qq{ AND ENTETEDATED >= DATE_SUB(DATE_FORMAT(CURDATE(), '%Y-%m-01'), INTERVAL 11 MONTH) };
	my $req = qq{select AGAUTO,count(*),SUM(ENTETEMONTANTFR) ,YEAR(ENTETEDATED), MONTH(ENTETEDATED) FROM ENTETEFACTURE WHERE CLCODE="$codeclient"  AND ENTETEDATED >= DATE_SUB(DATE_FORMAT(CURDATE(), '%Y-%m-01'), INTERVAL 11 MONTH)   GROUP BY AGAUTO ,YEAR(ENTETEDATED)
	,MONTH(ENTETEDATED) ORDER BY AGAUTO,YEAR(ENTETEDATED)
	,MONTH(ENTETEDATED)};


	$MYBD1 = $DB->prepare($req);
	$MYBD1->execute();

	my $agpred1  = qq{AGAUTO};
	my $ancms    = qq{MOIS};
	my $totcont1 = 0;
	my $catot1   = 0;

	my $tocours1 = 0;
	my $tocac1   = 0.0;

	my $toarret1 = 0;
	my $tocaar1  = 0.0;

	my $toarch1  = 0;
	my $tocaac1  = 0.0;

	my $topre1   = 0;
	my $tocap1   = 0.0;


	while(my @data = $MYBD1->fetchrow_array)
	{
		my $nb     = $data[1];
		my $agence = $data[0];
		my $ca     = $data[2];
		my $ms     = $data[4];

		if ($agence ne $agpred1)
		{
			$agpred1 = $agence;
			$ancms   = $ms;
			$totag{$agence}->[$ms] = $nb;
			$caag{$agence}->[$ms]  = $ca;
			$nbcont{$agence}       = $nb;
			$catot{$agence}        = $ca;
		}
		else
		{
			$nbcont{$agence} += $nb;
			$catot{$agence}  += $ca;
			if ($ancms ne $ms)
			{
				$totag{$agence}->[$ms] = $nb;
				$caag{$agence}->[$ms]  = $ca;
				$ancms=$ms;
			}
			else
			{
				$totag{$agence}->[$ms] += $nb;
				$caag{$agence}->[$ms]  += $ca;
			}
		}
	}
	$MYBD1->finish;

	#facturation du mois
	for(my $l = 0; $l < @agen; $l++)
	{
		my @result = &statjr($agen[$l]);
		$totag{$agen[$l]}->[$result[1]] += $result[3];
		$caag{$agen[$l]}->[$result[1]] += $result[4];
	}

	print qq{<tr bgcolor="#C6CDC5">\n};
	print qq{<th nowrap colspan="2"><font class="PT">$mess[4463]</font></th>\n};
	print qq{<th><font class="PT">$mess[4476]</font></th>\n};
	for (my $i=$mois;$i>=1;$i--)
	{
		print qq{<th><font class="PT"><b>$mess[3652+$i] $anc</b></font></th>\n};
	}
	for (my $i=12;$i>$mois;$i--)
	{
		print qq{<th><font class="PT"><b>$mess[3652+$i] $anp</b></font></th>\n};
	}

	print qq{</tr>\n};
	my @totms;
	my @cams;
	my $j = 0;
	for (my $i = 0; $i < @agen; $i++)
	{
		if ($nbcont{$agen[$i]} == 0)
		{
		    next
		}
		my $compt = $j % 2;
		my $color = "''";
		if ($compt == 1)
		{
			$color = qq{'#D6D6D6'};
	 	}
		print qq{<tr bgColor=$color onmouseover="bgColor='#FFCC33'" onmouseout="bgColor=$color">\n};
		print qq{<td><font class="PT"><b>$agen[$i]</b></font></td>\n};
		print qq{<td nowrap><font class="PT"><b>Nb fact<br>CA fact</b></font></td>\n};
		$totms[0] += $nbcont{$agen[$i]};
		$cams[0]  += $catot{$agen[$i]};
		$catot{$agen[$i]} = &fnombre($catot{$agen[$i]}, 3, 0);
		print qq{<td nowrap><font class="PT"><b>$nbcont{$agen[$i]}<br><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$catot{$agen[$i]}</span></b></font></td>\n};
		for (my $j = $mois; $j >= 1; $j--)
		{
			$totms[$j] += $totag{$agen[$i]}->[$j];
			$cams[$j]  += $caag{$agen[$i]}->[$j];
			$caag{$agen[$i]}->[$j] = &fnombre($caag{$agen[$i]}->[$j], 3, 0);
			if ($totag{$agen[$i]}->[$j] eq "0")
			{
				$totag{$agen[$i]}->[$j] = qq{&nbsp;};
				$caag{$agen[$i]}->[$j]  = qq{&nbsp;};
			}
			print qq{<td nowrap><font class="PT">$totag{$agen[$i]}->[$j]<br><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$caag{$agen[$i]}->[$j]</span></font><br></td>\n};
		}
		for (my $j = 12; $j > $mois; $j--)
		{
			$totms[$j] += $totag{$agen[$i]}->[$j];
			$cams[$j]  += $caag{$agen[$i]}->[$j];
			$caag{$agen[$i]}->[$j] = &fnombre($caag{$agen[$i]}->[$j], 3, 0);
			if ($totag{$agen[$i]}->[$j] eq "0")
			{
				$totag{$agen[$i]}->[$j] = qq{&nbsp;};
				$caag{$agen[$i]}->[$j]  = qq{&nbsp;};
			}
			print qq{<td nowrap><font class="PT">$totag{$agen[$i]}->[$j]<br><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$caag{$agen[$i]}->[$j]</span></font><br></td>\n};
		}
		print qq{</tr>\n};
		$j++;
	}
	print qq{<tr bgcolor="#C6CDC5">\n};
	print qq{<td nowrap colspan="2"><font class="PT"><b>$mess[4476]</b></font></td>\n};
	$cams[0] = &fnombre($cams[0], 3, 0);
	print qq{<td nowrap><font class="PT"><b>$totms[0]<BR><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$cams[0]</span></b></font></td>\n};
	for (my $i = $mois; $i >= 1; $i--)
	{
		my $post = "";
		if ($totms[$i] == 0)
		{
			$totms[$i] = "&nbsp;";
			$cams[$i]  = "&nbsp;";
		}
		else
		{
			$cams[$i] = &fnombre($cams[$i], 3, 0);
		}
		print qq{<td nowrap><font class="PT"><b>$totms[$i]<BR><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$cams[$i]</span></b></font></td>\n};
	}
	for (my $i = 12; $i > $mois; $i--)
	{
		my $post = "";
		if ($totms[$i] == 0)
		{
			$totms[$i] = "&nbsp;";
			$cams[$i]  = "&nbsp;";
		}
		else
		{
			$cams[$i] = &fnombre($cams[$i], 3, 0);
		}
		print qq{<td nowrap><font class="PT"><b>$totms[$i]<BR><span onmouseover="Tip(toEuro(this.innerHTML,$tauxE)+' euros')" onmouseout="UnTip()">$cams[$i]</span></b></font></td>\n};
	}
	print qq{</tr>\n};
	print qq{<br>\n};

	print qq{</table>\n};
	print qq{<br>\n};
	print qq{<table cellspacing="2" cellpadding="2" border="0">};
	#nb contrat
	my $req=qq{select AGAUTO,count(*),MAX(CONTRATDR) FROM CONTRAT WHERE CLAUTO="$client"  AND CONTRATOK in ("-1","-2") AND CONTRAT.CONTRATDR>=DATE_SUB(NOW(), INTERVAL 12 MONTH) GROUP BY AGAUTO ORDER BY AGAUTO};

	print qq{<tr bgcolor="#C6CDC5">\n};
	print qq{<th><font class="PT">Agence</font></th>\n};
	print qq{<th><font class="PT">Nbre Contrat(s)</font></th>\n};
	print qq{<th><font class="PT">Derni�re Date Fin</font></th>\n};
	print qq{</tr>\n};
	$MYBD1=$DB->prepare($req);
	$MYBD1->execute();



	my $j=0;
	while(my @data=$MYBD1->fetchrow_array)
	{
		my $compt=$j % 2;
		my $color="''";
		if ($compt == 1)
		{
			$color=qq{'#D6D6D6'};
	 	}
		print qq{<tr bgColor=$color onmouseover="bgColor='#FFCC33'" onmouseout="bgColor=$color">\n};
		for(my $i=0;$i<@data;$i++)
		{

			if ($i>=2)
			{
				$data[$i]=&format_datetime($data[$i]);
			}
			print qq{<td nowrap align="right"><font class="PT">$data[$i]</font></td>\n};

		}
		print qq{</tr>\n};
		$j++;
	}
	$MYBD1->finish;

}
