#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';

use strict;
use DBI;
use clemail;
use accesbd;
use fonctions;
use varglob;
use LOC::Characteristic;

my %BD = ();
my @pays = qw(FR);
my $etcode = "('MAC06')";
my %email = ();
my %emailOrigin;
if ($G_ENVIR eq '') {
	$email{"FR"} = &LOC::Characteristic::getCountryValueByCode('STAPANNE', 'FR');
}
else 
{
	$email{"FR"} = "archive.dev\@acces-industrie.com";
	$emailOrigin{'FR'} = &LOC::Characteristic::getCountryValueByCode('STAPANNE', 'FR');
}
my %fichier = ();

foreach (@pays) {
	$fichier{$_} = "liste_machine_$_.csv";
	open(FIC, ">$fichier{$_}") || die "pas ouvert";
	print FIC qq|AGENCE,DESIGNATION,PARC,SERIE,DATE_PANNE\n|;
	$BD{$_}  = &Environnement("LOCATION", $_);
	my $stat_etcode = $BD{$_}->prepare(
	qq{
	select MACHINE.AGAUTO, MOMADESIGNATION, MANOPARC, MANOSERIE, MADATEPANNE
	from AUTH.`MACHINE`
	left join AUTH.`AGENCE` on AGENCE.AGAUTO=MACHINE.AGAUTO
	left join AUTH.`MODELEMACHINE` on MACHINE.MOMAAUTO=MODELEMACHINE.MOMAAUTO
	where PACODE="$_"
	and ETCODE in $etcode
	order by MACHINE.AGAUTO, MANOPARC
	}
	);
	$stat_etcode->execute();

	while (my @data = $stat_etcode->fetchrow_array()) {
		print FIC qq|$data[0],$data[1],$data[2],$data[3],$data[4]\n|;
	}

	$stat_etcode->finish();
	close(FIC);
}

my ($sec,$min,$hr,$jour,$mois,$an,$joursm,$jouran,$ete)=localtime;
$an += 1900;
$mois += 1;
my $sbj     = qq{Liste des machines en panne le $jour/$mois/$an};
if ($G_ENVIR ne '')
{
    my $prefix = uc($G_ENVIR);
    $prefix =~ s/ //;
    $sbj = '[' . $prefix . ']' . $sbj;
}
# Exp�diteur (juste l'adresse mail)
my $mailsSender = (keys(%{&LOC::Globals::get('mailsSender')}))[0];
my $smtp    = $ENV{'SMTP_HOST'};
my $type    = "HTML";
my $html	= &Debut_HTML;
$html		.= qq{
<body bgcolor="#FFFFFF">
Voici en pi�ce jointe sous forme de tableau Excel la liste des machines en panne le $jour/$mois/$an.
</body>
</html>
};

foreach (@pays) {
    if ($G_ENVIR ne '')
    {
        $html = "<p>Destinataires d'origine : " . $emailOrigin{$_} . "</p>" . $html;
    }
	&envoie_email($smtp, $mailsSender, $email{$_}, $sbj, $type,
		&accentsHTML($html), $fichier{$_});
	$BD{$_}->disconnect;
	unlink($fichier{$_});
}


# D�but de page HTML (envoi par mail)
sub Debut_HTML {
	return qq{
<html>
<head>
<style type="text/css">
BODY {
	background: #EDEDE0;
	color:#000000;
	font-size:10pt;
	font-family: Arial, Helvetica, sans-serif;
FONT {font-size:10pt;}
FONT.PT {font-size:8pt;}
}
</style>
</head>
};
}
