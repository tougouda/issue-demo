#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';

use strict;
use stockter;
use varglob;
use fonctions;
use DBI;
use LOC::Characteristic;

#use mymath;
use clemail;
use accesbd;
my @pays=();


my $dbh = &Environnement("LOCATION", "FR");

my ($sec, $min, $hr, $jour, $mois, $an, $joursm, $jouran, $ete) = localtime;
$an += 1900;
$mois += 1;

my $fichier = qq{stockag$jour$mois$an.csv};

my %modele;

my @email = split(',', &LOC::Characteristic::getCountryValueByCode('STAMODELE', 'FR'));


&designation;

my $sbj     = qq{Stat journaliere des stocks des agences du $jour/$mois/$an};
if ($G_ENVIR ne '')
{
    my $prefix = uc($G_ENVIR);
    $prefix =~ s/ //;
    $sbj = '[' . $prefix . ']' . $sbj;
}
# Exp�diteur (juste l'adresse mail)
my $mailsSender = (keys(%{&LOC::Globals::get('mailsSender')}))[0];
my $smtp    = $ENV{'SMTP_HOST'};
my $type    = "HTML";

my $html    = &Debut_HTML
                .qq{<body>\nVoici en pi�ce jointe sous forme de tableau Excel la statistique journali�re des stocks des agences pour le $jour/$mois/$an.\n<p>\n};
$html      .= qq{<table border="0" cellspacing="0" cellpadding="2">\n\t<tr bgcolor="white">\n\t\t<td><tt>\n};

if ($G_ENVIR ne '')
{
    $html = "<p>Destinaires d'origine : " . &LOC::Characteristic::getCountryValueByCode('STAMODELE', 'FR') . "</p>" . $html;
}


&lst_pays;

$dbh->disconnect;

for(my $j = 0; $j < @pays; $j++)
{
    &maj_stockmodele($pays[$j]);
    $html .= qq{<br>Maj des stocks du pays $pays[$j]<br>};
    $txt_aff =~ s/\n/<br>/g;
    $html .= $txt_aff;
}

$dbh = &Environnement("LOCATION", "FR");

open(FICH, ">$fichier");
&stockmodele;
close(FICH);

$dbh->disconnect;

$html .= qq{\t\t</tt></td>\n\t</tr>\n</table>\n</body>\n</html>\n};
$html = &accentsHTML($html);

for(my $j = 0; $j < @email; $j++)
{   
    &env_email($smtp, $mailsSender, $email[$j], $sbj, $type, $html, $fichier);
}

unlink($fichier);



sub designation
{
    my $reqcont = qq{SELECT MOMAAUTO, MOMADESIGNATION FROM AUTH.`MODELEMACHINE` ORDER BY MOMADESIGNATION};
    my $sth = $dbh->prepare("$reqcont");

    $sth->execute();
    while (my @data = $sth->fetchrow_array())
    {
        $modele{$data[0]} = $data[1];
    }
    $sth->finish();
}



sub stockmodele 
{
    my $stag = $dbh->prepare(qq{SELECT MOMAAUTO AS MODELE, AGAUTO AS AGENCE, STMORESERVABLE AS RESERVABLE, STMORESERVE AS RESERVE, STMOTOTAL AS TOTAL, STMOLOU AS LOUE, STMODIS AS DISPO, STMOREC AS RECU, STMOREV AS REVI, STMOCON AS CONT, STMOPAN AS PANN, STMOVER AS VERI, STMOTRA AS TRSF FROM AUTH.`STOCKMODELE` ORDER BY MOMAAUTO, AGAUTO});
    $stag->execute();
    
    my @label = @{$stag->{NAME}};
    my $lb = join("\;", @label);
    print FICH qq{$lb\n};
    while (my @data = $stag->fetchrow_array())
    {
        # Historisation du nombre de machines hors parc
        my $hp = $dbh->prepare(qq{SELECT COUNT(*) FROM AUTH.`MACHINE` WHERE MOMAAUTO="$data[0]" AND MAHORSPARC="-1" AND AGAUTO = "$data[1]"});
        $hp->execute();
        if (my ($nb_hp) = $hp->fetchrow_array())
        {
            push(@data, $nb_hp);
        }
        else
        {
            push(@data, 0);
        }
        my $ins = join("\",\"", @data);
        $ins = qq{\"$ins\"};
        print FICH $modele{$data[0]};
        
        for (my $i = 1; $i < @data; $i++)
        {
            $data[$i] =~ s/\n/ /g;
            $data[$i] =~ s/\r/ /g;
            print FICH qq{;$data[$i]};
        }
        print FICH qq{\n};

        my $req = qq{INSERT INTO AUTH.`HISTOSTOCKMODELE` (MOMAAUTO, AGAUTO, HISTSTMORESERVABLE, HISTSTMORESERVE, HISTSTMOTOTAL, HISTSTMOLOU, HISTSTMODIS, HISTSTMOREC, HISTSTMOREV, HISTSTMOCON, HISTSTMOPAN, HISTSTMOVER, HISTSTMOTRA, HISTSTMOHORSPARC) VALUES ($ins)};
        $dbh->do($req);
    }
    $stag->finish;
}



sub lst_pays
{
    my $temp = $dbh->prepare(qq|SELECT DISTINCT PACODE FROM AUTH.AGENCE WHERE AGETAT = 1|);
    $temp->execute();
    while (my @temp = $temp->fetchrow_array())
    {
        push(@pays, $temp[0]);
    }
    $temp->finish;
}


# D�but de page HTML (envoi par mail)
sub Debut_HTML
{
    return qq{
<html>
<head>
<style type="text/css">
BODY {
    background: #EDEDE0;
    color:#000000;
    font-size:10pt;
    font-family: Arial, Helvetica, sans-serif;
FONT {font-size:10pt;}
FONT.PT {font-size:8pt;}
}
</style>
</head>
};
}