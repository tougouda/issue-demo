#!/usr/bin/perl

use strict;
use lib '../lib', '../../inc', '../../lib';

use CGI;
# use securite;
use accesbd;
use varglob;
use Date::Calc qw( Days_in_Month Easter_Sunday Add_Delta_Days Day_of_Week Delta_Days);
use Data::Dumper;

use LOC::Insurance::Type;
use LOC::Common::Rent;

# Connexion
use DBI;
my $cgi = new CGI;
$|=1;
my $pays = "FR";
my $dbh=&Environnement("LOCATION",$pays);

#recuperation de la date du jour
my ($sec,$min,$hr,$jour,$mois,$an,$joursm,$jouran,$ete)=localtime;

#formatage date du jour
$an +=1900;
$mois +=1;

#force pour faire l'historique d'un mois
my $force = $ARGV[0];
my $inMonth = $ARGV[1];
my $inAnnee = $ARGV[2];

if ($force == 1)
{
    $jour = 1;
    print "mode force=1\n";

    my $rq=qq{SELECT count(*) FROM LOCATION.ENTETELIGNE WHERE TO_DAYS(DATEAUTO)=TO_DAYS(now())-1};
	my $rgq=$dbh->prepare("$rq");
	$rgq->execute();
	my ($nb) = $rgq->fetchrow_array();
	$rgq->finish;
    if (defined $inMonth)
    {
        $mois = $inMonth + 1;
    }
    if (defined $inAnnee)
    {
        $an = $inAnnee;
    }
}

($an,$mois,$jour) = Add_Delta_Days(($an,$mois,$jour),-1);
print "$jour / $mois / $an\n";

#creation de moiss et jours, mois et jour de la date du jour format�e sur 2 characteres
my $moiss = $mois;
my $jours = $jour;


#preparation du padding
my $pad_char="0";
my $pad_len=2;

print "debut de script\n";
my $finmois = 0;

if ($force == 1)
{
    # fin du mois en cours
    $finmois = Days_in_Month($an, $moiss);
}
else
{
    $finmois = $jours;  
}



#padding
substr( $moiss, 0, 0 ) = $pad_char x ( $pad_len - length( $moiss ) );
substr( $jours, 0, 0 ) = $pad_char x ( $pad_len - length( $jours ) );
substr( $finmois, 0, 0 ) = $pad_char x ( $pad_len - length( $finmois ) );

#date de fin de mois +1 pour borner l'historique
my ($afin,$mfin,$jfin) = Add_Delta_Days(($an,$moiss,$finmois),2);

#suppression des informations en base du mois en cours
my $reqd = qq{
DELETE FROM STATSFR.UTILISATIONMACHINE
WHERE ANNEE="$an" AND MOIS="$moiss";
};
#print $reqd."\n";
$dbh->do($reqd);	  
print "Requete recuperation historique:";
my $where="";

#recuperation de l'historique du mois en cours
#jour -1 car histo fait a 00:15
my $req1 = qq{
    SELECT
        F.FACOMAUTO,
        H.MOMAAUTO,
        H.AGAUTO,
        H.HISTSTMOTOTAL,
        H.HISTSTMOLOU,
        DATE_SUB(DATE_FORMAT(H.HISTDATE,'%Y-%m-%d'),INTERVAL 1 DAY),
        MOMACOUTFR,
        HISTSTMOHORSPARC,
        M.FAMTARAUTO
    FROM AUTH.HISTOSTOCKMODELE H
    left join AUTH.MODELEMACHINE M on M.MOMAAUTO=H.MOMAAUTO
    left join LOCATION.FAMILLEMACHINE F on F.FAMAAUTO=M.FAMAAUTO
    left join AUTH.AGENCE A on A.AGAUTO = H.AGAUTO
    where A.PACODE='FR' and H.HISTDATE >= "$an-$moiss-02" and H.HISTDATE <= "$afin-$mfin-$jfin" and H.MOMAAUTO != 201 and  H.HISTSTMOTOTAL != 0
};
print "OK\n";
my $stag=$dbh->prepare($req1);
$stag->execute();

my $nbr = $stag->rows;

#tableau de nombre total de jours ouvrables (nb jour ouvr�s x nb machines)
my %nb_jours_ouvrables_total;
#nombre de machine total par agence, famille com et modele de machine
my %nb_mac; 
# info suppl�mentaires pourt le calcul du ratio cash
my %ratio_cash;

#pour chaque ligne de l'historique
while (my @data=$stag->fetchrow_array())
{   
    #date de l'historique
    my($a,$m,$j) = split(/-/,$data[5]);
    #si c'est un jour ouvr�    
    if (&is_jour_ouvre($j,$m,$a) == 1)
    {
        #nombre total de jours ouvrables (nbjourouvr�s x nbmachines)
        $nb_jours_ouvrables_total{$data[2]}{$data[0]}{$data[1]} += $data[3];
        #nb de machines en parc x nb jour ouvr�s
        $ratio_cash{$data[2]}{$data[0]}{$data[1]}{'nbenparc'} += $data[3] - $data[7];      
        #cout du modele en cours
        $ratio_cash{$data[2]}{$data[0]}{$data[1]}{'cout'} = $data[6]; 
        #famille tarifaire correspondante
        $ratio_cash{$data[2]}{$data[0]}{$data[1]}{'famtarauto'} = $data[8];
    }
    #si on est le dernier jour, on recupere le nombre de machines
    if ($j == $jour-1)
    {
        $nb_mac{$data[2]}{$data[0]}{$data[1]} = $data[3];
        # et on double (parce que pas d'histo pour le jour en cours)
#         $nb_jours_ouvrables_total{$data[2]}{$data[0]}{$data[1]} += $data[3];
#         $ratio_cash{$data[2]}{$data[0]}{$data[1]}{'nbenparc'} += $data[3] - $data[7];      
#         $ratio_cash{$data[2]}{$data[0]}{$data[1]}{'cout'} = $data[6];
    }
}
print "ok\n";
#calcul du taux de location moyen
my %tab;    #tableau temporaire
my %tab2;   #tableau temporaire 2
my $c2;     #variable temporaire 
my $c3;     #variable temporaire 
my $c4;     #nombre de jours louables * nb machines
my $ag;     #agence en cours
my $facom;  #famille commerciale en cours
my $moma;   #modele de machine en cours
# chaque agence
print "Traitement de chaque agence :";
while (($ag,$c2) = each(%nb_jours_ouvrables_total))
{
     %tab = %{$c2};
     print "\n$ag : ";
     while (($facom,$c3) = each(%tab))     
     {
         %tab2 = %{$c3};
         while (($moma,$c4) = each(%tab2))     
         {
            # requete de recuperation des montants de tout ce qui est deja factur�
        	my $requete = qq{
            	SELECT
                   CONTRATDUREE,
                   LIGNEREFARTICLE,
                   C.CLAUTO,
                   LIGNEQTITE,
                   LIGNEPRIX,
                   CONTRATHORSPARC,
                   C.CONTRATCODE
                FROM ENTETEFACTURE
                left join LIGNEFACTURE
                    on LIGNEFACTURE.ENTETEAUTO=ENTETEFACTURE.ENTETEAUTO
                left join CONTRAT C on 
                    C.CONTRATAUTO=ENTETEFACTURE.CONTRATAUTO
                WHERE 
                    ENTETEFACTURE.AGAUTO = "$ag" AND
                    ENTETEFACTURE.ETCODE = "FAC02" AND
                    ENTETEDATEDOC>="$an-$moiss-01" AND
                    ENTETEDATEDOC<="$an-$moiss-$jours" AND
                    C.MOMAAUTO = $moma
                and LIGNEREFARTICLE in ("LOC", "ASS", "GDR", "J+", "J-","REM","TRANS","CARB","NET","SER","TRANSA","TRANSR","JRE+","SERRE","SERL","SERT")
                and C.LIETCONTRATAUTO in (2,3,4,7)
                GROUP BY ENTETEFACTURE.AGAUTO,CLCODE,LIGNEREFARTICLE,LIGNEFACTURE.LIGNEAUTO
        	};

            my $st=$dbh->prepare($requete);
            $st->execute();
			my  $nbjourloc=0.00;
            my  $caloc=0.00;
            my  $calochorsparc=0.00;
            my  $tmp=0.00;
            my  $tmp2=0.00;
            while (my @data2=$st->fetchrow_array())
            {   
                #jours de locs
                if ($data2[1] eq "J+" || $data2[1] eq "J-" || $data2[1] eq "LOC")   
                {
                    $nbjourloc += "$data2[3]";
                    $tmp2 = "$data2[3]";
                }
                #ca de loc
                if ($data2[1] eq "J+" || $data2[1] eq "J-" || $data2[1] eq "LOC" || $data2[1] eq "ASS" || $data2[1] eq "GDR" || $data2[1] eq "REM")   
                {
                    $caloc += ("$data2[3]" * "$data2[4]");
                    #ca de loc hors parc
                    if ($data2[5] eq "-1")
                    {
                        $calochorsparc += ("$data2[3]" * "$data2[4]");
                        $tmp = ("$data2[3]" * "$data2[4]");
                    }
                }                             
            }
            # si on est pas en mode 'force' (mois en cours)
            if ($force !=1)
            {
                #tout ce qui est en cours de facturation
            	my $requete2= qq{
            		select CONTRATCODE, CONTRATDD, CONTRATDR, CONTRATJOURSPLUS, CONTRATJOURSMOINS, MOPXJOURFR,
                	MOTARIFMSFR, CONTRATTYPEASSURANCE, MOTRANSPORTDFR, MOTRANSPORTRFR, CAMONTANTFR,
                	MOFORFAITFR, CONTRATDUREE, SOLOAUTO, LIETCONTRATAUTO,
                	ETATFACTURE, MAHORSPARC,CONTRATDATEFACTURE, ity_flags, ity_mode, CONTRATPASSURANCE,
                	CONTRATTYPERECOURS, CONTRATRECOURSVAL
                	from CONTRAT
                	left join MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
                	left join CARBURANT ON CARBURANT.CARBUAUTO=CONTRAT.CARBUAUTO
                	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
                	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
                	left join AUTH.`MACHINE` ON MACHINE.MAAUTO=CONTRAT.MAAUTO
                	left join p_insurancetype ON ity_id = CONTRATTYPEASSURANCE
                    left join p_appealtype ON apl_id = CONTRATTYPERECOURS
                	where CONTRAT.AGAUTO="$ag"
                	AND CONTRAT.MOMAAUTO=$moma
                	AND CONTRATOK in ("-1","-2")
                	and CONTRAT.LIETCONTRATAUTO in ("2","3","4","7")
                	and CONTRATDD <= "$an-$moiss-$jours"
                	and CONTRATDR >= "$an-$moiss-01"
                    and (ETATFACTURE in ("P", "") or ETATFACTURE is null)
                };
                my $st2=$dbh->prepare($requete2);
                $st2->execute();
                while (my @data3=$st2->fetchrow_array())
                {
                    
                    my $n=0.00;
                    my $nc=0.00;
                    my $nm=0.00;
                    my $nmc=0.00;
    
                    
                    #debut contrat
                    my ($ad,$md,$jd) =  split(/-/,$data3[1]);
                    #date de derniere facturation
                    #fin contrat
                    my ($afc,$mfc,$jfc) =  split(/-/,$data3[2]);
                    my ($adf,$mdf,$jdf) =  split(/-/,$data3[2]);
                    
                    my $beginDate = $data3[1];
                    my $endDate   = $data3[2];
                    
                    #si debut de mois < fin de contrat
                    if (Delta_Days($an,$moiss,01,$ad,$md,$jd)>0 || $data3[15] ne "P")
                    {
                        # si jour en cours < fin de contrat
                        if (Delta_Days($an,$moiss,$jour,$afc,$mfc,$jfc)>0 )
                        {
                            ($n,$nc) = nb_jours_ouvres($jd,$md,$ad,$jour,$moiss,$an);

                            $beginDate = sprintf('%04d-%02d-%02d', $ad, $md, $jd);
                            $endDate   = sprintf('%04d-%02d-%02d', $an, $moiss, $jour);

                            if (Day_of_Week($an,$moiss,$jour) == 6)
                            {
                                $n += 1;                                
                            }
                            elsif (Day_of_Week($an,$moiss,$jour) == 7)
                            {
                                $n += 2;                               
                            }
                        }
                        else
                        {
                            ($n,$nc) = nb_jours_ouvres($jd,$md,$ad,$jfc,$mfc,$afc);

                            $beginDate = sprintf('%04d-%02d-%02d', $ad, $md, $jd);
                            $endDate   = sprintf('%04d-%02d-%02d', $afc, $mfc, $jfc);

                            if (Day_of_Week($afc,$mfc,$jfc) == 6)
                            {
                                $n += 1;
                               
                            }
                            elsif (Day_of_Week($afc,$mfc,$jfc) == 7)
                            {
                                $n += 2; 
                              
                            }
                        }
    
                    }
                    else
                    {
                        if (Delta_Days($an,$moiss,$jour,$afc,$mfc,$jfc)>0)
                        {
                            ($n,$nc) = nb_jours_ouvres(01,$moiss,$an,$jour,$moiss,$an);

                            $beginDate = sprintf('%04d-%02d-%02d', $an, $moiss, 01);
                            $endDate   = sprintf('%04d-%02d-%02d', $an, $moiss, $jour);

                            if (Day_of_Week($an,$moiss,$jour) == 6)
                            {
                                $n += 1;

                            }
                            elsif (Day_of_Week($an,$moiss,$jour) == 7)
                            {
                                $n += 2;
     
                            }
                        }
                        else
                        {
                            ($n,$nc) = nb_jours_ouvres(01,$moiss,$an,$jfc,$mfc,$afc);

                            $beginDate = sprintf('%04d-%02d-%02d', $an, $moiss, 01);
                            $endDate   = sprintf('%04d-%02d-%02d', $afc, $mfc, $jfc);

                            if (Day_of_Week($afc,$mfc,$jfc) == 6)
                            {
                                $n += 1;
  
                            }
                            elsif (Day_of_Week($afc,$mfc,$jfc) == 7)
                            {
                                $n += 2;
                            
                            }
                        }
                   }
                    #si contrat au jour
                    if ($data3[5] > 0.00)
                    {
                        $caloc += ($n + $data3[3] - $data3[4])* $data3[5];
                        $nbjourloc += $n + $data3[3] - $data3[4];
        
                        my $montantass = &LOC::Common::Rent::calculateInsurance($pays, $ag, $data3[7], $data3[20], $data3[5],
                                                                                $beginDate, $endDate, $data3[3], $data3[4]);

                        #si assurance � facturer
                        if ($data3[18] & LOC::Insurance::Type::FLAG_ISINVOICED)
                        {
                            $caloc += $montantass; 
                        }
                        
                        # Gestion de recours
                        my $appealAmount = 0;
                        if ($data3[21])
                        {
                            $appealAmount = &LOC::Common::Rent::calculateAppeal($pays, $ag, $data3[21], $data3[22], $data3[5],
                                                                                $beginDate, $endDate, $data3[3], $data3[4]);
                        }
                        $caloc += $appealAmount;
                    }
                    #si forfait mensuel
                    if ($data3[6]>0)
                    {
                        ($nm,$nmc) = &nb_jours_ouvres(01,$moiss,$an,$finmois,$moiss,$an); 

                        my $beginDate = sprintf('%04d-%02d-%02d', $an, $moiss, 01);
                        my $endDate   = sprintf('%04d-%02d-%02d', $an, $moiss, $finmois);

                        # fin du mois en cours
    					my $Xfinmois = Days_in_Month($an, $moiss);
                        my ($Xnm,$Xnmc) = &nb_jours_ouvres(01,$moiss,$an,$Xfinmois,$moiss,$an); 
						$caloc += ($n + $data3[3] - $data3[4])* ($data3[6]/$Xnm);
                        $nbjourloc += $n + $data3[3] - $data3[4];

                        my $montantass = &LOC::Common::Rent::calculateInsurance($pays, $ag, $data3[7], $data3[20], ($data3[6]/$Xnm),
                                                                                $beginDate, $endDate, $data3[3], $data3[4]);

                        #si assurance � facturer
                        if ($data3[18] & LOC::Insurance::Type::FLAG_ISINVOICED)
                        {
                            $caloc += $montantass; 
                        }

                        # Gestion de recours
                        my $appealAmount = 0;
                        if ($data3[21])
                        {
                            $appealAmount = &LOC::Common::Rent::calculateAppeal($pays, $ag, $data3[21], $data3[22], ($data3[6]/$Xnm),
                                                                                $beginDate, $endDate, $data3[3], $data3[4]);
                        }
                        $caloc += $appealAmount;
                    }
                }
            }
            #calcul du taux d'utilisation moyen
            my $tx;
            if ($c4 != 0)
            {
                $tx = $nbjourloc / $c4;
            }
            else
            {
                $tx = 0;
            }
            #calcul du px moy de loc
            my $px;
            if ($nbjourloc != 0)
            {
                $px = $caloc / $nbjourloc;
            }
            else
            {
                $px = 0;
            }
            #nombre de machines � la fin du mois
            my $nb_mac_final = $nb_mac{$ag}{$facom}{$moma};
            if ($nb_mac_final eq "")
            {
                $nb_mac_final = 0;
            }
            #ratio cash
            my $ratiocash=0.00;
            my $coutmoma = $ratio_cash{$ag}{$facom}{$moma}{'cout'};
            my $nbenparc = $ratio_cash{$ag}{$facom}{$moma}{'nbenparc'};
            my $famtar   = $ratio_cash{$ag}{$facom}{$moma}{'famtarauto'};
            
            my $coutperiodeep = (($coutmoma * 0.0192) * $nbenparc)/21;
            my $coutperiodehp = $calochorsparc * 0.5;
            
            my $coutperiode = $coutperiodeep + $coutperiodehp;
            
            if ($coutperiode != 0)
            {
                $ratiocash = $caloc / $coutperiode;
            }
            #insertion dans la base
            my $req=qq{
            INSERT INTO STATSFR.`UTILISATIONMACHINE` (FAMTARAUTO, FACOMAUTO ,MOMAAUTO,AGAUTO ,NBMAC,TXUTILMOY,PXLOCMOY,RATIOCASH,MOIS ,ANNEE, CALOC, NBJRSLOC, COUTSLOC,NBJRSPRESMAC)
             values ($famtar,$facom,$moma,"$ag",$nb_mac_final,$tx,$px,$ratiocash,"$moiss","$an", $caloc, $nbjourloc, $coutperiode, $nbenparc)};
            $dbh->do($req);	            
         }
     
     }
     print " ok";   
}
print "\n ok \n Fin du script";   

# retourne si le jour donn� en parametre est ouvr�
# en entr�e:
#       - $j le jour
#       - $m le mois
#       - $a l'ann�e
sub is_jour_ouvre
{
    my @param = @_;
    my $j = $param[0];
    my $m = $param[1];
    my $a = $param[2];
    
    if (Day_of_Week($a,$m,$j)>5)
    {
        return 0;
    }
    else
    {
        #paques
        my ($a_paques,$m_paques,$j_paques) = Add_Delta_Days(Easter_Sunday($a), 1); 
        if ($m_paques == $m && $j_paques == $j)    
        {
            return 0;
        }
        #ascension
        my ($a_asc,$m_asc,$j_asc) = Add_Delta_Days(Easter_Sunday($a), 39); 
        if ($m_asc == $m && $j_asc == $j)    
        {
            return 0;
        }
                
        
        return 0 if($j == 1 && $m == 1); # 1er janvier 
        return 0 if($j == 1 && $m == 5); # 1er mai 
        return 0 if($j == 8 && $m == 5); # 5 mai 
        return 0 if($j == 14 && $m == 7); # 14 juillet 
        return 0 if($j == 15 && $m == 8); # 15 aout 
        return 0 if($j == 1 && $m == 11); # 1 novembre 
        return 0 if($j == 11 && $m == 11); # 11 novembre 
        return 0 if($j == 25 && $m == 12); # 25 d�cembre 
    
        return 1;
    }
}

# retourne si le nombre de jours ouvr�s
# en entr�e:
#       - $j le jour de debut (inclus)
#       - $m le mois de debut
#       - $a l'ann�e de debut
#       - $jf le jour de fin (inclus)
#       - $mf le mois de fin
#       - $af l'ann�e de fin
sub nb_jours_ouvres
{
    my @param = @_;
    my $j  = $param[0];
    my $m  = $param[1];
    my $a  = $param[2];
    my $jf = $param[3];
    my $mf = $param[4];
    my $af = $param[5];        
    
    my $at = $a;
    my $mt = $m;
    my $jt = $j;
    my $ok=0;
    my $nb=0;
    my $nb2=0;
    while ($ok == 0)
    {
        if (&is_jour_ouvre($jt,$mt,$at))
        {
            $nb++;
        }
        $nb2++;
        if (($at == $af) && ($mt == $mf) && ($jt == $jf))
        {
            $ok = 1;
        }
        my ($at2,$mt2,$jt2) = Add_Delta_Days(($at,$mt,$jt),1);
        $at=$at2;
        $mt=$mt2;
        $jt=$jt2;

    } 
    return ($nb,$nb2);       
}
