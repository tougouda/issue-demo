#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';

use strict;
use LOC::Globals;
use LOC::Db;
use LOC::Country;
use LOC::Timer;
use LOC::Insurance::Type;
use LOC::Appeal::Type;
my @start = &LOC::Timer::start();

print "Content-type: text/plain; charset=UTF-8\n\n";
print "D�but de la mise � jour...\n";
print "\n";

# Erreurs
my $hasErrors = 0;

my $tabCountries = &LOC::Country::getList(LOC::Util::GETLIST_IDS);

foreach my $countryId (@$tabCountries)
{
    # R�cup�ration de la connexion � la base de donn�es
    my $db = &LOC::Db::getConnection('location', $countryId);
    my $schemaName = &LOC::Db::getSchemaName($countryId);

    print '### ' . $countryId . " ###\n\n";

    # R�cup�ration des clients concern�s
    my $query = "
    SELECT
        CLAUTO AS `customerId`,
        CLCODE AS `customerCode`,
        CLSTE AS `customerLabel`,
        CLNVASSURANCE AS `insuranceTypeId`,
        CLNVPASSURANCE AS `insuranceRate`,
        CLDATEDEBASS AS `insuranceBegindate`,
        IF (ity_flags & " . $db->quote(LOC::Insurance::Type::FLAG_ISAPPEALACTIVATED) . ", 1, 0) AS `isAppealActivated`,
        CLNVTYPERECOURS AS `appealTypeId`,
        CLDATEDEBTYPERECOURS AS `appealTypeBeginDate`,
        IF(CLDATEDEBASS <= NOW() AND CLDATEDEBASS != '0000-00-00', 1, 0) AS `updateInsurance`,
        IF(CLDATEDEBTYPERECOURS <= NOW() AND CLDATEDEBTYPERECOURS != '0000-00-00', 1, 0) AS `updateAppeal`
    FROM CLIENT
    LEFT JOIN p_insurancetype ON ity_id = CLNVASSURANCE 
    WHERE (CLDATEDEBASS <= NOW() AND CLDATEDEBASS != '0000-00-00')
    OR (CLDATEDEBTYPERECOURS <= NOW() AND CLDATEDEBTYPERECOURS != '0000-00-00');";
    my $tabResult = $db->fetchAssoc($query);

    foreach my $tabRow (values @$tabResult)
    {
        print $tabRow->{'customerCode'} . " - " . $tabRow->{'customerLabel'} . "\n";

        my $tabUpdates = {};
        # Mise � jour de l'assurance
        if ($tabRow->{'updateInsurance'})
        {
            $tabUpdates->{'CLTYPEASSURANCE'} = $tabRow->{'insuranceTypeId'};
            $tabUpdates->{'CLPASSURANCE'}    = $tabRow->{'insuranceRate'};
            $tabUpdates->{'CLNVASSURANCE'}   = undef;
            $tabUpdates->{'CLNVPASSURANCE'}  = 0;
            $tabUpdates->{'CLDATEDEBASS'}    = undef;
            
            # Gestion de recours par d�faut
            if ($tabRow->{'isAppealActivated'})
            {
                $tabUpdates->{'CLTYPERECOURS'} = &LOC::Appeal::Type::getDefaultId($countryId);
            }
        }
        # Mise � jour de la gestion de recours
        if ($tabRow->{'updateAppeal'})
        {
            $tabUpdates->{'CLTYPERECOURS'}        = $tabRow->{'appealTypeId'};
            $tabUpdates->{'CLNVTYPERECOURS'}      = undef;
            $tabUpdates->{'CLDATEDEBTYPERECOURS'} = undef;
        }

        my $result = $db->update('CLIENT', $tabUpdates, {'CLAUTO' => $tabRow->{'customerId'}}, $schemaName);
        if ($result == -1)
        {
            $hasErrors = 1;
            print 'Erreur lors de la mise � jour de l\'assurance du client ' . $tabRow->{'CLCODE'} . "\n";
        }
    }

}
my $end = &LOC::Timer::end(@start);
print "Mise � jour termin�e (Temps �coul� : " . $end . "s).";

if ($hasErrors)
{
    die();
}