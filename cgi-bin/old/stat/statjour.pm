#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';

use strict;
use DBI;
use calculdate;
use mymath;
use clemail;
use accesbd;
use varglob;
use Date::Calc qw(Delta_Days Days_in_Month);

use JSON::XS;

use LOC::Characteristic;
use LOC::Date;
use LOC::Insurance::Type;
use LOC::Common::Rent;

my $dbh  = &Environnement("LOCATION", "FR");
my $dbh6 = &Environnement("LOCATION", "EXT");

# Jour du mois
my $temp;
my $dteFinMois;
my ($deb, $fin, $dtjour, $affdte,$temp, $temp, $dteFinMois) = &datemois;
$dteFinMois =~ s/\:/\-/g;

my $ctaux = &cout_taux($fin);
my $jrmy  = 21;
my $coef  = .65; #.7;

my @agence    = ();
my @agenceext = ();
my %nmach;
my %cout;
my %cout_hp;
my %cout_hp_per;
my $stag = $dbh->prepare(qq{select AGAUTO, PACODE FROM AUTH.`AGENCE` WHERE AGAUTO not IN ("SIE") ORDER BY AGAUTO});
$stag->execute();
my %agpys;

while (my @data = $stag->fetchrow_array()) {
	if ($data[1] eq "FR")  {push @agence, $data[0];}
	if ($data[1] eq "EXT") {push @agenceext, $data[0];}

	$agpys{$data[0]} = $data[1];
	$nmach{$data[0]} = 0;
}

$stag->finish;

my @email;
my %emailOrigin;
if ($G_ENVIR eq '') {
    @email   = split(',', &LOC::Characteristic::getCountryValueByCode('STAJOUR', 'FR'));
}
else {
	@email   = qw(archive.dev@acces-industrie.com);
	$emailOrigin{'FR'} = &LOC::Characteristic::getCountryValueByCode('STAJOUR', 'FR');
}

my $requete = qq{
select CONTRATDD, CONTRATDR, CONTRATJOURSPLUS, CONTRATJOURSMOINS, MOPXJOURFR,
MOTARIFMSFR, CONTRATTYPEASSURANCE, MOTRANSPORTDFR, MOTRANSPORTRFR, CAMONTANTFR,
CONTRAT.LIETCONTRATAUTO, MOFORFAITFR, MOMAGRP, ETATFACTURE, MAHORSPARC,CONTRATPASSURANCE, ity_flags,
CONTRATTYPERECOURS, CONTRATRECOURSVAL
FROM CONTRAT
LEFT JOIN AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
LEFT JOIN FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
LEFT JOIN MONTANT ON MONTANT.MOAUTO=CONTRAT.MOAUTO
LEFT JOIN CARBURANT ON CARBURANT.CARBUAUTO=CONTRAT.CARBUAUTO
LEFT JOIN AUTH.`MACHINE` ON MACHINE.MAAUTO=CONTRAT.MAAUTO
LEFT JOIN p_insurancetype ON ity_id = CONTRATTYPEASSURANCE
LEFT JOIN p_appealtype ON apl_id = CONTRATTYPERECOURS
WHERE SOLOAUTO=0
	AND FAMACOMPTAAUTO!=10
	AND CONTRAT.AGAUTO="agence"
	AND CONTRATOK in ("-1","-2")
	AND CONTRAT.LIETCONTRATAUTO IN ("1","2","3","7")
	AND DATE(CONTRATDD) <= "finms"
	AND DATE(CONTRATDR) >= "departms"
    AND CONTRAT.MAAUTO != 0
    AND CONTRATMACHATTR = -1
	AND (ETATFACTURE in ("P", "") OR ETATFACTURE is null)
};
my $reqmach = qq{
SELECT
    IF(M.MASUPPAGAUTO IS NOT NULL AND M.ETCODE IN ("MAC09", "MAC12") AND C.ETCODE = "CON01",
        M.MASUPPAGAUTO,
        M.AGAUTO) AS AG_MODIF,
    M.ETCODE
FROM AUTH.`MACHINE` M
LEFT JOIN CONTRAT C ON (M.CONTRATAUTO = C.CONTRATAUTO AND M.MAAUTO = C.MAAUTO AND C.CONTRATMACHATTR = -1)
LEFT JOIN AUTH.`MODELEMACHINE` MM ON MM.MOMAAUTO = M.MOMAAUTO
LEFT JOIN AUTH.`AGENCE` A ON A.AGAUTO = M.AGAUTO
LEFT JOIN FAMILLEMACHINE FM ON FM.FAMAAUTO = MM.FAMAAUTO
WHERE A.PACODE="pays"
	AND M.LIETCODE NOT IN ("8","10")
	AND M.LIETCODE IS NOT NULL
	AND M.LIETCODE<20
	AND FM.FAMACOMPTAAUTO!=10
};

# S�lection des machines hormis les rendues, vendues, perdues, �paves ... et accessoires
my $reqcout = qq{
select MACHINE.AGAUTO, PACODE, MOMACOUTFR, MAHORSPARC
FROM AUTH.`MACHINE`
LEFT join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=MACHINE.MOMAAUTO
LEFT JOIN FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
LEFT join AUTH.`AGENCE` ON AGENCE.AGAUTO=MACHINE.AGAUTO
WHERE LIETCODE not IN ("8", "10")
AND LIETCODE is not NULL
AND LIETCODE<20
AND FAMACOMPTAAUTO!=10
};

my $tableau = &Debut_HTML;

$tableau .= qq{
<table width="100%" border="0" cellspacing="2" cellpadding="0">
	<tr bgcolor="#C6CDC5">
		<th><font class="PT">&nbsp;</font></th>
		<th><font class="PT">&nbsp;</font></th>
		<th><font class="PT">&nbsp;</font></th>
		<th colspan="5"><font class="PT">Nombre de contrats</font></th>
		<th rowspan="2"><font class="PT">Tx util.<br>J+1</font></th>
		<th colspan="6"><font class="PT">Arr&ecirc;t&eacute; au jour J=$affdte</font></th>
		<th colspan="8"><font class="PT">Arr&ecirc;t&eacute; &agrave; la fin du mois</font></th>
	</tr>
	<tr bgcolor="#C6CDC5">
		<th><font class="PT">AGENCE</font></th>
		<th><font class="PT">J.O.</font></th>
		<th><font class="PT">PARC</font></th>
		<th><font class="PT">TOT</font></th>
		<th><font class="PT">ARR</font></th>
		<th><font class="PT">ACT</font></th>
        <th><font class="PT">LIVR</font></th>
		<th><font class="PT">CT<5j</font></th>
		<th><font class="PT">CA</font></th>
		<th><font class="PT">CA LOC</font></th>
		<th><font class="PT">JR LOC</font></th>
		<th><font class="PT">%Util</font></th>
		<th><font class="PT">COUT</font></th>
		<th><font class="PT">Ratio cash</font></th>
		<th><font class="PT">CA</font></th>
		<th><font class="PT">CA LOC</font></th>
		<th><font class="PT">Jr LOC</font></th>
		<th><font class="PT">Px Jr</font></th>
		<th><font class="PT">Px JR LOC</font></th>
		<th><font class="PT">COUT</font></th>
		<th><font class="PT">CT Jr</font></th>
		<th><font class="PT">Ratio prix</font></th>
	</tr>
};

my $bas = qq{
</table>
</body>
</html>
};

my $sbj = qq{Stat journaliere de la location};
if ($G_ENVIR ne '')
{
    my $prefix = uc($G_ENVIR);
    $prefix =~ s/ //;
    $sbj = '[' . $prefix . ']' . $sbj;
}

# Exp�diteur (juste l'adresse mail)
my $mailsSender = (keys(%{&LOC::Globals::get('mailsSender')}))[0];
my $stmp = $ENV{'SMTP_HOST'};
my $type = "HTML";
my $html = $tableau;
my $datejr = $affdte;

&cout;
&nb_mach("FR");
&nb_mach("EXT");

my $htmlfr = $tableau;

my $totcaglob  = 0.0;
my $totca1glob = 0.0;
my $totcalocglob = 0.0;
my $totcaloc1glob = 0.0;
my $durcaglob = 0.0;
my $durca1glob = 0.0;
my $ouvrglob = 0;
my $ouvr1glob = 0;
my $totcontglob = 0;
my $totcrsglob = 0;
my $tot4jglob = 0;
my $totarrglob = 0;
my $totmacglob = 0;
my $totlivglob = 0;
my $ctglob = 0;
my $totcoutglob = 0;
my $totratio2glob = 0;

my $corpsfr  = &corp_mail($dbh, "FR", @agence);
my $corpsext = &corp_mail($dbh6, "EXT", @agenceext);
$dbh6->disconnect;

$html   .= $corpsfr.$corpsext;
$htmlfr .= $corpsfr.$corpsext.$bas;

# Total global
my $pied = "";

$pied  = qq{<tr bgcolor="#C6CDC5">\n<td nowrap align="LEFT" colspan="2"><font class="PT"><b>TOTAL</b></font></td>\n};
$pied .= qq{<td nowrap align="RIGHT"><font class="PT"><b>$totmacglob</b></font></td>\n};
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totcontglob</b></font></td>\n|;
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totarrglob</b></font></td>\n|;
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totcrsglob</b></font></td>\n|;
$pied .= qq{<td nowrap align="RIGHT"><font class="PT"><b>$totlivglob</b></font></td>\n};
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$tot4jglob</b></font></td>\n|;
my $totutilglob = 0;

if ($totmacglob != 0) {
	$totutilglob = ($totcrsglob + $totlivglob) / $totmacglob * 100;
}

$totutilglob = sprintf("%.2f", $totutilglob);
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totutilglob</b></font></td>\n|;
my $ut1 = 0;

if ($totmacglob) {
	$ouvr1glob = $ouvr1glob / $totmacglob;
}
if ($totmacglob && $ouvr1glob) {
	$ut1 = $durca1glob * 100 / ($totmacglob * $ouvr1glob);
}

$ut1 = sprintf("%.2f", $ut1);
my $ut = 0;
my $u = 0;
my $ut2 = 0;
my $u2 = 0;

if ($durcaglob) {
	$ut = $totcaglob / $durcaglob;
	$ut2 = $totcalocglob / $durcaglob;
	$u2 = $ut2;
	$u = $ut;
}

my $totratio2glob = $totcaloc1glob / $totcoutglob;
$totratio2glob = sprintf("%.2f", $totratio2glob);
$ut = sprintf("%.1f", $ut);
$ut2 = sprintf("%.1f", $ut2);
$totcaglob = sprintf("%.0f", $totcaglob);
$totca1glob = sprintf("%.0f", $totca1glob);
$totcalocglob = sprintf("%.0f", $totcalocglob);
$totcaloc1glob = sprintf("%.0f", $totcaloc1glob);
$durca1glob = sprintf("%.1f", $durca1glob);
$durcaglob = sprintf("%.1f", $durcaglob);
$totcoutglob = sprintf("%.0f", $totcoutglob);
$totratio2glob = sprintf("%.2f", $totratio2glob);
$totcaglob = &fnombre2($totcaglob, 3, 0);
$totca1glob = &fnombre2($totca1glob, 3, 0);
$totcalocglob = &fnombre2($totcalocglob, 3, 0);
$totcaloc1glob = &fnombre2($totcaloc1glob, 3, 0);
$totcoutglob = &fnombre2($totcoutglob, 3, 0);
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totca1glob</b></font></td>\n|;
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totcaloc1glob</b></font></td>\n|;
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$durca1glob</b></font></td>\n|;
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$ut1</b></font></td>\n|;
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totcoutglob</b></font></td>\n|;
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totratio2glob</b></font></td>\n|;
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totcaglob</b></font></td>\n|;
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totcalocglob</b></font></td>\n|;
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$durcaglob</b></font></td>\n|;
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$ut</b></font></td>\n|;
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$ut2</b></font></td>\n|;
my $affc = $ctglob;
my $ctt = $affc;
$affc = sprintf("%.0f", $affc);
$affc = &fnombre2($affc, 3, 0);
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$affc</b></font></td>\n|;
my $ctmj = 0;
my $pc = 0;

if ($totmacglob > 0) {
	$ctmj = $ctt / ($totmacglob * $jrmy * $coef);

	if ($ctmj > 0) {
		$pc =$ u2 / $ctmj;
	}
}

$ctmj = sprintf("%.1f", $ctmj);
$pc = sprintf("%.2f", $pc);
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$ctmj</b></font></td>\n|;
$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$pc</b></font></td>\n|;
$pied .= qq{</tr>\n};

$html   .= $pied.$bas;
$htmlfr .= $pied.$bas;

for(my $j=0; $j<@email; $j++) {
	my $sbu = $sbj;
	$sbu .= " du ".$datejr;
	if (defined $emailOrigin{'FR'})
    {
       $html = 'Destinataires d\'origine : ' . $emailOrigin{'FR'} . $html;
    }
	&env_email($stmp, $mailsSender, $email[$j], $sbu, $type, $html);
}

$dbh->disconnect;

sub datemois {
	my ($sec, $min, $hr, $jour, $mois, $an, $joursm, $jouran, $ete) = localtime;
	my($jrdate, $msdate, $andate) = ($jour, $mois, $an);
	$andate += 1900;
	$msdate += 1;
	$an += 1900;
	$mois += 1;
	my $debutms = "$an:$mois:01";
	my $bisexar = &bisextile($an);
 	my $ms = $mois % 2;
	my $finmois;

	if ($mois <= 7) {
		if ($ms == 1 || $mois == 7) {
			$finmois = 31;
		}
		else {
			if ($mois == 2) {
				if ($bisexar == 1) {
					$finmois = 29;
				}
				else {
					$finmois = 28;
				}
			}
			else {
				$finmois = 30;
			}
		}
	}
	else {
		if (($ms == 1 || $mois == 7) && $mois >= 8) {
			$finmois = 30;
		}
	  	else {
	  		$finmois = 31;
	  	}
	}

	my $finms = "$an:$mois:$finmois";
	my $dt = "$an:$mois:$jour";
	my $dtaff = "$jour/$mois/$an";

	return ($debutms, $finms, $dt, $dtaff);
}

sub statjr {
	my @param   = @_;
	my $ag      = $param[0];
	my $db      = $param[1];
	my $debutms = $param[2];
	my $finms   = $param[3];
	my $type    = $param[4];
	my $pays    = $agpys{$ag};


    # R�cup�ration des formats Sage (quantit�, prix et montant)
    my $charac;
    my $req = '
SELECT IFNULL(ccy_value, cgl_value)
FROM AUTH.p_characteristic
LEFT JOIN AUTH.p_characteristic_global ON chr_id = cgl_chr_id
LEFT JOIN AUTH.p_characteristic_country ON (cgl_chr_id = ccy_chr_id AND ccy_cty_id = "' . $pays . '")
WHERE chr_code = "FORMATSSAGE"';

    my $sth = $db->prepare($req);
    $sth->execute();

    while (my @data = $sth->fetchrow_array())
    {
        $charac = $data[0];
    }

    $sth->finish();

    my $tabSageFormats = {'quantity' => 2, 'price' => 4, 'amount' => 2};
    if ($charac ne '')
    {
        $tabSageFormats = JSON::XS->new->decode($charac);
    }


	my $req     = $requete;
	$req =~ s/agence/$ag/;
	my ($andm, $msdm, $jrdm) = split(/:/, $debutms);
	my $dm = Delta_Days(1900, 1, 1, $andm, $msdm, $jrdm);
	my ($anfm, $msfm, $jrfm) = split(/:/, $finms);
	my $fm = Delta_Days(1900, 1, 1, $anfm, $msfm, $jrfm);
	my $jourmois  = Delta_Days($andm, $msdm, $jrdm, $anfm, $msfm, $jrfm);
	my $jourmoisd = $jourmois + 1;
	my $jourmsouv = $jourmois;
	my $uvfer;
	$uvfer = &jour_fer_ag_cnx($pays, $ag, $jrdm, $msdm, $andm, $jourmsouv, $db);
	$jourmsouv += 1;
	my $ouvrwe = &jour_we($jourmsouv, $jrdm, $msdm, $andm);
	$jourmsouv = $jourmsouv - $ouvrwe - $uvfer;
	$req =~ s/finms/$finms/;
	$req =~ s/departms/$debutms/;
	my $sth = $db->prepare($req);
	$sth->execute();
	my $mtloc = 0;
	my $nbcont = 0;
    my $nbcontlivr = 0;
	my $nbcontcours = 0;
	my $nbcontarret = 0;
	my $nbdureems = 0;
	my $nbdureeplus = 0;
	my $nbtarif = 0;
	my $nbasscomp = 0;
	my $total = 0.0;
	my @mtcontrat;
	my @mtass;
	my $nbcarb = 0;
	my $nbtr = 0;
	my $durr = 0;
	my $temp = Days_in_Month($anfm, $msfm);

	while (my @data = $sth->fetchrow_array()) {
	    my $tauxE = tauxLocalToEuro($pays, $dteFinMois);
		my $contratdd = $data[0];
		my $contratdr = $data[1];
		my $contratjoursplus = $data[2];
		my $contratjoursmoins = $data[3];
		my $mopxjourfr = $data[4] * $tauxE;
		my $motarifmsfr = $data[5] * $tauxE;
		my $contrattypeassurance = $data[6];
		my $insuranceFlags = $data[16];
        my $appealTypeId = $data[17];
        my $appealRate = $data[18];
		my $motransportdfr = $data[7] * $tauxE;
		my $motransportrfr = $data[8] * $tauxE;
		my $camontantfr = $data[9] * $tauxE;
		my $lietcontratauto = $data[10];
		my $moforfaitfr = $data[11] * $tauxE;
		my $momagrp = $data[12];
		my $etatfacture = $data[13];
		my ($jrdd, $msdd, $andd) = &datetojjmsan($contratdd);
		my $dd = Delta_Days(1900, 1, 1, $andd, $msdd, $jrdd);
		my ($jrdf, $msdf, $andf) = &datetojjmsan($contratdr);
		my $df = Delta_Days(1900, 1, 1, $andf, $msdf, $jrdf);
		my $dureecont = $df - $dd + 1;
		my $boolms = 0;
		my $ta = 0.0;
		my $tr = 0.0;
		my $carbu = 0.0;
		my $ddf = $contratdd;
		my $dff = $contratdr;
		my $jrplus = 0.0;
		my $jrmoins = 0.0;
		my $tarif = $mopxjourfr;
		my $tarifm = $motarifmsfr;
		my $forfait = $moforfaitfr;
		my $boole = 0;
		my $dff2;

		# Contrat Mensuel
		if ($tarifm != 0) {
			$boole = 1;
			$tarif = $tarifm;
			# Calculer le nb de jours ouvrables
			my $nbjourmois = $temp;
			my $nbnbjrwe   = &jour_we($nbjourmois, 01, $msfm, $anfm);
			my $nbnbferie  = &jour_fer_ag_cnx($pays, $ag, 01, $msfm, $anfm, $nbjourmois-1, $db);
			my $joursupmensuel = 0;

			if ($df > $fm) {$joursupmensuel = 0;}
			else {
				my $testjoursupmensuel = &num_jour($jrdf, $msdf, $andf);

				if ($testjoursupmensuel == 6) {$joursupmensuel = 1;}
				if ($testjoursupmensuel == 7) {$joursupmensuel = 2;}
			}

			$jourmois = $nbjourmois - $nbnbjrwe - $nbnbferie + $joursupmensuel;
		}
		if ($forfait != 0) {
			$boole = 2;
			$tarif = $forfait;
			my $dureecont = Delta_Days($andd, $msdd, $jrdd, $andf, $msdf, $jrdf);
			my $nbferieforfait = &jour_fer_ag_cnx($pays, $ag, $jrdd, $msdd, $andd, $dureecont, $db);
	  		my $joursupforfait = 0;
	  		my $testjoursupforfait;

			if ($df > $fm) {$joursupforfait = 0;}
			else {
				$testjoursupforfait = &num_jour($jrdf, $msdf, $andf);

				if ($testjoursupforfait == 6) {$joursupforfait = 1;}
				if ($testjoursupforfait == 7) {$joursupforfait = 2;}
			}

			$dureecont++;

			if ($dureecont == 1 && $testjoursupforfait == 7) {$joursupforfait = 1;}

			my $nbjrweforfait = &jour_we($dureecont, $jrdd, $msdd, $andd);
	  		$dureecont = $dureecont - $nbferieforfait - $nbjrweforfait + $joursupforfait;
			$jourmois = $dureecont;
		}
		# Ici en fonction de la date de d�part, si elle est dans le mois en cours
		# on comptabilise le transport aller sinon non
		if ($etatfacture eq "P") {$ddf = $debutms;}
		elsif (!$etatfacture || $etatfacture eq "") {$ta = $motransportdfr;}
		# Ici en fonction de la date de fin, si elle est dans le mois en cours
		# on comptabilise le transport retour et le carburant sinon non
		if ($df > $fm) {
			$dff = $finms;
			$dff2 = "$anfm:$msfm:$temp";
			$boolms = 1;
		}
		else {
		 	$carbu = $camontantfr;
		 	$dff2 = $dff;
		 	$tr = $motransportrfr;
		}

		$jrplus  = $contratjoursplus; # CONTRATJOURSPLUS
		$jrmoins = $contratjoursmoins; # CONTRATJOURSMOINS
		my ($dda, $ddm, $ddj);
		my ($dfa, $dfm, $dfj);
		my ($dfa2, $dfm2, $dfj2);

		if ($ddf =~ /:/) {($dda, $ddm, $ddj) = split(/:/, $ddf);}
		else {($dda, $ddm, $ddj) = split(/-/, $ddf);}
		if ($dff =~ /:/) {($dfa, $dfm, $dfj) = split(/:/, $dff);}
		else {($dfa, $dfm, $dfj) = split(/-/, $dff);}
		if ($dff2 =~ /:/) {($dfa2,$dfm2,$dfj2) = split(/:/,$dff2);}
		else {($dfa2,$dfm2,$dfj2) = split(/-/,$dff2);}

		my $duree = Delta_Days($dda,$ddm,$ddj,$dfa,$dfm,$dfj);
		my $duree2 = Delta_Days($dda,$ddm,$ddj,$dfa2,$dfm2,$dfj2);

		if ($duree < 0) {next;}

		# $duree2 ici sert � calculer les contrats chevauchant la fin de mois
		# avec -4j ds le mois : d� � la nouvelle facturation qui ne prend pas en compte ces
		# contrats l�
		my $nbferie2 = &jour_fer_ag_cnx($pays, $ag, $ddj, $ddm, $dda, $duree2, $db);
		my $nbjrwe2  = 0;
  		my $joursup2 = 0;
  		my $testjoursup2;

		if (Delta_Days($anfm, $msfm, $temp, $andf, $msdf, $jrdf) > 0) {$joursup2 = 0;}
		else {
			$testjoursup2 = &num_jour($dfj2,$dfm2,$dfa2);

			if ($testjoursup2 == 6) {$joursup2 = 1;}
			if ($testjoursup2 == 7) {$joursup2 = 2;}
		}

		$duree2 += 1;

		if ($duree2 == 1 && $testjoursup2 == 7) {$joursup2 = 1;}

		$nbjrwe2 = &jour_we($duree2, $ddj, $ddm, $dda);
		$duree2 = $duree2 - $nbferie2 - $nbjrwe2 + $joursup2 + $jrplus - $jrmoins;

		if (Delta_Days($anfm, $msfm, $temp, $andf, $msdf, $jrdf) > 0 && $duree2 <= 4) {
			$nbcont++;

            if ($lietcontratauto eq "1") {$nbcontlivr++;}
			elsif ($lietcontratauto eq "2") {$nbcontcours++;}
			else {$nbcontarret++;}

			next;
		}

		# Nouveau calcul, dur�e de jours ouvrables pour tous
        $duree = &LOC::Date::getRentDuration(
            sprintf('%04d-%02d-%02d', $dda, $ddm, $ddj),
            sprintf('%04d-%02d-%02d', $dfa, $dfm, $dfj),
            $ag,
            $data[1]
        ) + $jrplus - $jrmoins;
		$durr += $duree;

		# Le calcul de la dur�e s'effectue en fonction du nombre
		# de jours ouvrables dans le mois ainsi que de sa dur�e et
		# des j+ j-, jours f�ri�s et w-e
		# si dur�e sup au mois, joursouvrables, sinon dur�e et le reste
		# Surtout important c'est le calcul des contrats de -5j CONTRATDD et CONTRATDR
		# Et non pas le nb de contrats dont la dur�e ds le mois < 5
		# $duree (nb de jours du contrat ds le mois) <> $dureereel (nb de jours du contrat)
		my ($dareel, $dmreel, $djreel) = split(/-/, $contratdd);
		my ($fareel, $fmreel, $fjreel) = split(/-/, $contratdr);
		my $dureereel = Delta_Days($dareel, $dmreel, $djreel, $fareel, $fmreel, $fjreel);
		my $nbferiereel = &jour_fer_ag_cnx($pays, $ag, $djreel, $dmreel, $dareel, $dureereel, $db);
		my $testjoursupreel = &num_jour($fjreel, $fmreel, $fareel);
		my $joursupreel = 0;

		if ($testjoursupreel == 6) {$joursupreel = 1};
		if ($testjoursupreel == 7) {$joursupreel = 2};
		if ($boolms != 0) {$joursupreel = 0;}

		$dureereel += 1;

		if ($dureereel == 1 && $testjoursupreel == 7) {$joursupreel = 1;}

		my $nbjrwereel = &jour_we($dureereel, $djreel, $dmreel, $dareel);
		$dureereel = $dureereel + $jrplus - $jrmoins - $nbferiereel - $nbjrwereel + $joursupreel;

		if ($dureereel < 5) {$nbdureems++;}
		else {$nbdureeplus++;}
		if ($boole == 1 || $boole == 2) {$tarif = $tarif / $jourmois;}

		my $montantass = 0.0;
        my $tx = $data[15];

        my $beginDate = sprintf('%04d-%02d-%02d', $dda, $ddm, $ddj);
        my $endDate   = sprintf('%04d-%02d-%02d', $dfa, $dfm, $dfj);
        $montantass = &LOC::Common::Rent::calculateInsurance($pays, $ag, $contrattypeassurance, $tx, $tarif,
                                                             $beginDate, $endDate, $jrplus, $jrmoins);

		if ($insuranceFlags & LOC::Insurance::Type::FLAG_ISINVOICED) {
			$mtass[$nbasscomp] = $montantass;
			$nbasscomp++;
		}

		# Gestion de recours
        my $appealAmount = 0.0;
        if ($appealTypeId)
        {
            $appealAmount = &LOC::Common::Rent::calculateAppeal($pays, $ag, $appealTypeId, $appealRate, $tarif,
                                                                $beginDate, $endDate, $jrplus, $jrmoins);
        }

		$duree = sprintf("%.2f", $duree);
		my $montant = $tarif * $duree;
		$mtloc += $montant + $montantass + $appealAmount;
		my $montotal = $montant + $ta + $tr + $carbu + $montantass + $appealAmount;
		$mtcontrat[$nbcont] = $montotal;
		$total += $montotal;
		
		if ($data[14] == -1 && $type == 0) {
			$cout_hp_per{$pays}{$ag} += ($montant + $montantass + $appealAmount) / 2;
		}
		elsif ($data[14] == -1 && $type == 1) {
			$cout_hp{$pays}{$ag} += ($montant + $montantass + $appealAmount) / 2;
		}

        if ($lietcontratauto eq "1") {$nbcontlivr++;}
		elsif ($lietcontratauto eq "2") {$nbcontcours++;}
		else {$nbcontarret++;}

		$nbcont++;
	}

	$sth->finish;
	my $sth = $db->prepare(qq{
	select MOTARIFMSFR, CONTRATDUREE
	from CONTRAT
	left join AUTH.`MODELEMACHINE` on CONTRAT.MOMAAUTO=MODELEMACHINE.MOMAAUTO
	left join FAMILLEMACHINE on MODELEMACHINE.FAMAAUTO=FAMILLEMACHINE.FAMAAUTO
	left join MONTANT on CONTRAT.MOAUTO=MONTANT.MOAUTO
	where CONTRAT.AGAUTO="$ag"
	and CONTRATOK in ("-1","-2")
	and CONTRAT.LIETCONTRATAUTO in ("2","3","7")
	and DATE(CONTRATDD) <= "$finms"
	and DATE(CONTRATDR) >= "$debutms"
	and ETATFACTURE="F"
	and SOLOAUTO=0
	and FAMACOMPTAAUTO!=10
	});
	$sth->execute();

	while (my @data = $sth->fetchrow_array()) {
		if ($data[1] < 5 && $data[0] == 0) {$nbdureems++;}
		else {$nbdureeplus++;}

		$nbcontarret++;
		$nbcont++;
	}

	$sth->finish();
	my $sth = $db->prepare(qq|
	select LIGNEREFARTICLE,
	   sum(ROUND(ROUND(LIGNEPRIX, |.$tabSageFormats->{'price'}.qq|)*ROUND(LIGNEQTITE, |.$tabSageFormats->{'quantity'}.qq|), |.$tabSageFormats->{'amount'}.qq|)),
	   sum(LIGNEQTITE)
	from ENTETELIGNE
	left join ENTETEFACTURE on ENTETELIGNE.ENTETEAUTO=ENTETEFACTURE.ENTETEAUTO
	left join LIGNEFACTURE on LIGNEFACTURE.LIGNEAUTO=ENTETELIGNE.LIGNEAUTO
	left join CONTRAT on ENTETEFACTURE.CONTRATAUTO=CONTRAT.CONTRATAUTO
	left join AUTH.`MODELEMACHINE` ON MODELEMACHINE.MOMAAUTO=CONTRAT.MOMAAUTO
	left join FAMILLEMACHINE ON FAMILLEMACHINE.FAMAAUTO=MODELEMACHINE.FAMAAUTO
    where ENTETEFACTURE.AGAUTO="$ag"
    AND ((DATE(`DATE`)>="$debutms" and DATE(`DATE`)<="$finms")
    OR (ENTETEIDFACTU IS NULL AND DATE(CONTRATDD) <= "$finms" and DATE(CONTRATDR) >= "$debutms"))
	and SOLOAUTO=0
	and FAMACOMPTAAUTO!=10
	group by LIGNEREFARTICLE
	|);
	$sth->execute();
    my $tauxE = tauxLocalToEuro($pays, $dteFinMois);
	while (my @data = $sth->fetchrow_array()) {
		$total += $data[1] * $tauxE;

		if ($data[0] eq "LOC" || $data[0] eq "REM" || $data[0] eq "J+" || $data[0] eq "J-") {
            if ($data[0] ne "REM") {
			     $durr += $data[2];
			}
			$mtloc += $data[1] * $tauxE;
		}
		if ($data[0] eq "ASS") {$mtloc += $data[1] * $tauxE;}
        if ($data[0] eq "GDR")
        {
            $mtloc += $data[1] * $tauxE;
        }
	}

	$sth->finish();

	my $ca      = sprintf("%.2f", $total);
	my $moycont = &moyenne(@mtcontrat);
	my $varcont = &variance(@mtcontrat);
	$moycont    = sprintf("%.2f", $moycont);
	$varcont    = sprintf("%.2f", $varcont);

	return ($nbcont, $ca, $moycont, $varcont, $nbdureems, $nbdureeplus, $nbcontcours, $nbcontarret, $durr, $jourmsouv, $mtloc, $nbcontlivr);
}

sub nb_mach {
	my @par = @_;
	my $ps  = $par[0];
	my $nbm = 0;
	my $rq  = $reqmach;
	my $dbg = $dbh;

	if ($ps eq "EXT") {$dbg = $dbh6};

	$rq =~ s/pays/$ps/;
	my $stma = $dbg->prepare($rq);
	$stma->execute();

	while (my @data = $stma->fetchrow_array()) {
		$nmach{$data[0]}++;
	}

	$stma->finish;
}

sub cout {
	my $stma = $dbh->prepare(qq{select AGAUTO, PACODE FROM AUTH.AGENCE});
	$stma->execute();

	while (my @data = $stma->fetchrow_array()) {
		$cout{$data[1]}{$data[0]} = 0;
		$cout_hp{$data[1]}{$data[0]} = 0;
	}

	$stma->finish;
	my $req  = $reqcout;
	my $stma = $dbh->prepare($req);
	$stma->execute();

	while (my @data = $stma->fetchrow_array()) {
		if ($data[3] != -1) {
			$cout{$data[1]}{$data[0]} += $data[2];
		}
	}

	$stma->finish;
}

sub corp_mail {
	my @param = @_;
	my $db  = shift(@param);
	my $pays = shift(@param);
	my $totca  = 0.0;
	my $totca1 = 0.0;
	my $totcaloc = 0.0;
	my $totcaloc1 = 0.0;
	my $durca = 0.0;
	my $durca1 = 0.0;
	my $ouvr = 0;
	my $ouvr1 = 0;
	my $totcont = 0;
	my $totcrs = 0;
	my $tot4j = 0;
	my $totarr = 0;
	my $totmac = 0;
    my $totliv = 0;
	my $corps = "";
	my $ct = 0;
	my $totcout = 0;

	my $index_color = 0;
	my @color = ("", "#D6D6D6");
	for(my $i=0;$i<@param;$i++) {
		my $tab = "";
		my @result    = &statjr($param[$i], $db, $deb, $fin, 1);
		my @result1   = &statjr($param[$i], $db, $deb, $dtjour, 0);
		my $nbm = $nmach{$param[$i]};
		my $jours_ouvr = &jours_ouvrables($deb, $dtjour, $pays, $param[$i], $db);
		$totmac += $nbm;
        $totliv += $result[11];

		# FA 8964 : Test sur le nb de parc et sur le CA total fin de mois
		if ($nbm == 0 && $result[10] == 0) {next;}

		$ouvr1glob += $jours_ouvr * $nbm;
		$tab .= qq|
	<tr bgColor='$color[$index_color]' onmouseover="bgColor='#FFCC33'" onmouseout="bgColor='$color[$index_color]'">
		<td nowrap align="LEFT"><font class="PT"><b>$param[$i]</b></font></td>
		<td nowrap align="RIGHT"><font class="PT"><b>$jours_ouvr</b></font></td>
		<td nowrap align="RIGHT"><font class="PT"><b>$nbm</b></font></td>
		<td nowrap align="RIGHT"><font class="PT">$result[0]</font></td>
		<td nowrap align="RIGHT"><font class="PT">$result[7]</font></td>
		<td nowrap align="RIGHT"><font class="PT">$result[6]</font></td>
        <td nowrap align="RIGHT"><font class="PT">$result[11]</font></td>
		<td nowrap align="RIGHT"><font class="PT">$result[4]</font></td>
		|;
		$totcont += $result[0];
		$totarr  += $result[7];
		$totcrs  += $result[6];
		$tot4j   += $result[4];
		my $util = 0;

		if ($nbm != 0) {$util = (($result[6] + $result[11]) / $nbm) * 100;}

		$util=sprintf("%.2f",$util);
		$tab .=qq|<td nowrap align="RIGHT"><font class="PT">$util</font></td>\n|;
		$totca1 += $result1[1];
		$totca  += $result[1];
		$totcaloc += $result[10];
		$totcaloc1 += $result1[10];
		$durca1 += $result1[8];
		$durca += $result[8];
		my $ut1 = 0;
		$ouvr1 = $result1[9];

		if ($nbm && $result1[9]) {
			$ut1 = $result1[8] * 100 / ($nbm * $result1[9]);
		}

		$ut1 = sprintf("%.2f", $ut1);
		my $ut = 0;
		my $ut2 = 0;
		$ouvr = $result[9];
		my $u = 0;
		my $u2 = 0;

		if ($result[8]) {
			$ut = $result[1] / $result[8];
			$ut2 = $result[10] / $result[8];
			$u = $ut;
			$u2 = $ut2;
		}

		$ut = sprintf("%.1f", $ut);
		$ut2 = sprintf("%.1f", $ut2);
		$result1[1] = sprintf("%.0f", $result1[1]);
		$result[1] = sprintf("%.0f", $result[1]);
		$result1[1] = &fnombre2($result1[1], 3, 0);
		$result[1] = &fnombre2($result[1], 3, 0);
		$result1[8] = sprintf("%.0f", $result1[8]);
		$result[8] = sprintf("%.1f", $result[8]);
		$result[10] = sprintf("%.0f", $result[10]);
		$result1[10] = sprintf("%.0f", $result1[10]);
		$result1[8] = &fnombre2($result1[8], 3, 0);
		$result[10] = &fnombre2($result[10], 3, 0);
		# Co�t
		# Co�t pour le mois
		my $affc = $cout{$pays}{$param[$i]} * $ctaux / 100;
		# Co�t pour la p�riode
		my $cout = $affc / $jrmy * $jours_ouvr + $cout_hp_per{$pays}{$param[$i]};
		$affc += $cout_hp{$pays}{$param[$i]};
		$totcout += $cout;
		# Ratio 2
		my $ratio2 = ($cout == 0) ? 0 : $result1[10] / $cout;
		$result1[10]=&fnombre2($result1[10],3,0);
		# Formattage
		$cout = sprintf("%.0f", $cout);
		$cout = &fnombre2($cout, 3, 0);
		$ratio2 = sprintf("%.2f", $ratio2);
		$tab .= qq|<td nowrap align="RIGHT"><font class="PT">$result1[1]</font></td>\n|;
		$tab .= qq|<td nowrap align="RIGHT"><font class="PT">$result1[10]</font></td>\n|;
		$tab .= qq|<td nowrap align="RIGHT"><font class="PT">$result1[8]</font></td>\n|;
		$tab .= qq|<td nowrap align="RIGHT"><font class="PT">$ut1</font></td>\n|;
		$tab .= qq|<td nowrap align="RIGHT"><font class="PT">$cout</font></td>\n|;
		$tab .= qq|<td nowrap align="RIGHT"><font class="PT">$ratio2</font></td>\n|;
		$tab .= qq|<td nowrap align="RIGHT"><font class="PT">$result[1]</font></td>\n|;
		$tab .= qq|<td nowrap align="RIGHT"><font class="PT">$result[10]</font></td>\n|;
		$tab .= qq|<td nowrap align="RIGHT"><font class="PT">$result[8]</font></td>\n|;
		$tab .= qq|<td nowrap align="RIGHT"><font class="PT">$ut</font></td>\n|;
		$tab .= qq|<td nowrap align="RIGHT"><font class="PT">$ut2</font></td>\n|;
		my $ctt = $affc;
		$ct += $affc;
		$affc = sprintf("%.0f", $affc);
		$affc = &fnombre2($affc, 3, 0);
		$tab .= qq|<td nowrap align="RIGHT"><font class="PT">$affc</font></td>\n|;
		my $ctmj = 0;
		my $pc = 0;

		if ($nbm > 0) {
			$ctmj = $ctt / ($nbm * $jrmy * $coef);

			if ($ctmj > 0) {
				$pc = $u2 / $ctmj;
			}
		}

		$ctmj = sprintf("%.1f", $ctmj);
		$pc = sprintf("%.2f", $pc);
		$tab .= qq|<td nowrap align="RIGHT"><font class="PT">$ctmj</font></td>\n|;
		$tab .= qq|<td nowrap align="RIGHT"><font class="PT">$pc</font></td>\n|;
		$tab .= qq{</tr>\n};
		$corps .= $tab;
		$index_color = 1 - $index_color;
	}

	#totaux pays
	my $pied = "";

	$pied  = qq{<tr bgcolor="orange">\n<td nowrap align="LEFT" colspan="2"><font class="PT"><b>TOTAL $pays</b></font></td>\n};
	$pied .= qq{<td nowrap align="RIGHT"><font class="PT"><b>$totmac</b></font></td>\n};
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totcont</b></font></td>\n|;
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totarr</b></font></td>\n|;
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totcrs</b></font></td>\n|;
    $pied .= qq{<td nowrap align="RIGHT"><font class="PT"><b>$totliv</b></font></td>\n};
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$tot4j</b></font></td>\n|;
	my $totutil = 0;

	if ($totmac != 0) {
		$totutil = (($totcrs + $totliv) / $totmac) * 100;
	}

	$totutil = sprintf("%.2f", $totutil);
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totutil</b></font></td>\n|;
	my $ut1 = 0;

	if ($totmac && $ouvr1) {
		$ut1 = ($durca1 * 100) / ($totmac * $ouvr1);
	}

	$ut1 = sprintf("%.2f", $ut1);
	my $ut = 0;
	my $u = 0;
	my $ut2 = 0;
	my $u2 = 0;

	if ($durca) {
		$ut = $totca / $durca;
		$ut2 = $totcaloc / $durca;
		$u2 = $ut2;
		$u = $ut;
	}

	my $totratio2 = ($totcout) ? ($totcaloc1 / $totcout) : 0;
	$totratio2 = sprintf("%.2f", $totratio2);
	$ut = sprintf("%.1f", $ut);
	$ut2 = sprintf("%.1f", $ut2);
	$totcaglob += $totca;
	$totca = sprintf("%.0f", $totca);
	$totca1glob += $totca1;
	$totca1 = sprintf("%.0f", $totca1);
	$totcalocglob += $totcaloc;
	$totcaloc = sprintf("%.0f", $totcaloc);
	$totcaloc1glob += $totcaloc1;
	$totcaloc1 = sprintf("%.0f", $totcaloc1);
	$durca1glob += $durca1;
	$durca1 = sprintf("%.1f", $durca1);
	$durcaglob += $durca;
	$durca = sprintf("%.1f", $durca);
	$totcoutglob += $totcout;
	$totcout = sprintf("%.0f", $totcout);
	$totca = &fnombre2($totca, 3, 0);
	$totca1 = &fnombre2($totca1, 3, 0);
	$totcaloc = &fnombre2($totcaloc, 3, 0);
	$totcaloc1 = &fnombre2($totcaloc1, 3, 0);
	$totcout = &fnombre2($totcout, 3, 0);
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totca1</b></font></td>\n|;
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totcaloc1</b></font></td>\n|;
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$durca1</b></font></td>\n|;
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$ut1</b></font></td>\n|;
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totcout</b></font></td>\n|;
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totratio2</b></font></td>\n|;
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totca</b></font></td>\n|;
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$totcaloc</b></font></td>\n|;
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$durca</b></font></td>\n|;
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$ut</b></font></td>\n|;
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$ut2</b></font></td>\n|;
	my $affc = $ct;
	my $ctt = $affc;
	$affc = sprintf("%.0f", $affc);
	$affc = &fnombre2($affc, 3, 0);
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$affc</b></font></td>\n|;
	my $ctmj = 0;
	my $pc = 0;

	if ($totmac > 0) {
		$ctmj = $ctt / ($totmac * $jrmy * $coef);

		if ($ctmj > 0) {
			$pc = $u2 / $ctmj;
		}
	}

	$ctmj = sprintf("%.1f", $ctmj);
	$pc = sprintf("%.2f", $pc);
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$ctmj</b></font></td>\n|;
	$pied .= qq|<td nowrap align="RIGHT"><font class="PT"><b>$pc</b></font></td>\n|;
	$pied .= qq{</tr>\n};

	$ouvrglob += $ouvr;
	$totcontglob += $totcont;
	$totcrsglob += $totcrs;
	$tot4jglob += $tot4j;
	$totarrglob += $totarr;
	$totmacglob += $totmac;
    $totlivglob += $totliv;
	$ctglob += $ct;

	$corps .= $pied;
	return $corps;
}

sub cout_taux {
	my ($dtt) = @_;
	my ($ant, $mst, $jrt) = split(/:/, $dtt);
	my @tx = ();
	my @dt = ();

	# ordonner de facon croissante de date
	$dt[0] = qq{13/01/2003};
	$tx[0] = 1.92;

	my $t = @tx;
	$t--;
	my $tx = $tx[$t];

	for (my $i=$t; $i>=0; $i--) {
		my ($jrc, $msc, $anc) = split(/\//, $dt[$i]);
		my $diff1 = Delta_Days(1900, 1, 1, $ant, $mst, $jrt);
		my $diff2 = Delta_Days(1900, 1, 1, $anc, $msc, $jrc);
		my $diff  = $diff1 - $diff2;

		if ($diff <= 0) {
			$tx = $tx[$i];
		}
		else {
			next;
		}
	}

	return $tx;
}

sub fnombre2 {
	my ($nb, $sep, $dec) = @_;
	my ($long);
	my $negatif = ($nb =~ tr/-//);
	$nb .= ".";
	# ----- suppression des caracteres <> chiffres et '.'
	$nb =~ tr/(0-9.)//cd;
	# ----- longueur doit etre un multiple de 3
	$long = length($nb);
	$long = $long + (6 - $long % 3);
	$nb = sprintf("%$long.2f", $nb);
	# -----  remplace les espaces par des 'X'
	$nb =~ s/ /X/g;

	if ($sep) {
		# ----- un espace tous les 3 caracteres
		$nb =~ s/(\w{3})/$1 /g;
		# ----- suppression de l'espace devant le point decimal
		$nb =~ s/ \././;
	}

	# ----- suppression des 'non chiffres' de debut
	$nb =~ s/( ?X ?)*//g;
	# ----- suppression des decimales si = '.00'

	if (!$dec) {$nb=~s/\.00//;}

	$nb = "-".$nb if $negatif;

	return ($nb);
}

# D�but de page HTML (envoi par mail)
sub Debut_HTML {
	return qq{
<html>
<head>
<style type="text/css">
BODY {
	background: #EDEDE0;
	color:#000000;
	font-size:10pt;
	font-family: Arial, Helvetica, sans-serif;
FONT {font-size:10pt;}
FONT.PT {font-size:8pt;}
}
</style>
</head>
};
}
