# $Id: stockter.pm,v 1.1.1.1 2006/08/01 07:48:35 clliot Exp $
# $Log: stockter.pm,v $
# Revision 1.1.1.1  2006/08/01 07:48:35  clliot
# no message
#
# Revision 2.2  2006/02/06 16:52:56  julien
# V. 1.4 : Tarification v2
#
# Revision 2.1.2.1  2005/12/13 15:46:55  julien
# no message
#
# Revision 2.1.4.1  2005/12/13 10:32:06  julien
# Inclusion du r�pertoire lib dans chaque fichier.
#
# Revision 2.1  2005/08/26 07:19:34  julien
# Pr�fixage des tables de AUTH.
#
# Revision 2.0  2004/09/16 15:08:26  julien
# Version r�cup�r�e de Lancelot et nettoy�e
#
# Revision 1.2  2004/09/16 14:33:50  julien
# Ajout des identifiants
#

package stockter;
require Exporter;
@ISA =qw(Exporter);
@EXPORT =qw(maj_stockmodele lst_agence connex $txt_aff);
use vars qw($txt_aff);
use strict;
use lib '../lib';

use DBI;
use CGI;
use accesbd;




my @agence=();
my $BD;
my %rend;






# Eventuellement faire un foreach pour chaque pacode

sub maj_stockmodele {
	
my @param=@_;
@agence=();
&lst_agence($param[0]);
&connex($param[0]);
$txt_aff="";
#&machine_rendu;
my ($sec,$min,$hour,$mday,$mon,$year) = (localtime(time))[0,1,2,3,4,5];
$sec = sprintf("%02d",$sec);
$min = sprintf("%02d",$min);
$hour = sprintf("%02d",$hour);
$mon++;
$mon = sprintf("%02d",$mon);
$year += 1900;
$txt_aff .=qq{################################################## $mday/$mon/$year, $hour:$min:$sec ##################################################\n};

my $temp = $BD->prepare(qq|select count(MOMAAUTO) from AUTH.MODELEMACHINE|);
$temp->execute();
my $nbmod = $temp->fetchrow_array;
$temp->finish;
foreach my $lag (@agence) {
	my $i = 0;
	my $j = 1;
	my $temp = $BD->prepare(qq|select MOMAAUTO,STMORESERVABLE,STMORESERVE,STMOTOTAL,STMOLOU,STMODIS,STMOREC,STMOREV,STMOCON,STMOPAN,STMOVER,STMOTRA,STMOLIV from AUTH.`STOCKMODELE` where AGAUTO="$lag" order by MOMAAUTO|);
	$temp->execute();
	while (my @temp = $temp->fetchrow_array()) 
		{
		my $verif = 0;
		my %etat;
		$etat{"MAC01"} = 0;
		$etat{"MAC02"} = 0;
		$etat{"MAC03"} = 0;
		$etat{"MAC04"} = 0;
		$etat{"MAC05"} = 0;
		$etat{"MAC06"} = 0;
		$etat{"MAC07"} = 0;
		$etat{"MAC09"} = 0;
		$etat{"MAC10"} = 0;
        $etat{"MAC12"} = 0;
		$etat{"GRU11"} = 0;
		$etat{"GRU12"} = 0;
		$etat{"GRU13"} = 0;
		$etat{"GRU14"} = 0;
		$etat{"GRU15"} = 0;
		$etat{"GRU16"} = 0;
		$etat{"GRU17"} = 0;
		$etat{"GRU19"} = 0;
		my $tabReservable = {};
		my $nbreserve = 0;
		my $nbreservable = 0;
		my $nbtotal = 0;
		my $loue = 0;
		my $disp = 0;
		my $recu = 0;
		my $revi = 0;
		my $cont = 0;
		my $pann = 0;
		my $veri = 0;
		my $tran = 0;
		my $livr = 0;
		my $tempbis = $BD->prepare(qq|select ETCODE,count(*) from AUTH.`MACHINE` where AGAUTO="$lag" and MOMAAUTO=$temp[0] and ETCODE not in ("MAC08","GRU18") AND LIETCODE is not NULL AND LIETCODE<20 group by ETCODE|);
		$tempbis->execute();
		while (my @tempbis = $tempbis->fetchrow_array()) 
		{
			$etat{$tempbis[0]} = $tempbis[1] if $tempbis[0] =~ /MAC\d+/ || $tempbis[0] =~ /GRU\d+/;
			$verif = 1 if $tempbis[0] =~ /MAC\d+/ && $verif == 0;
            $verif = 2 if $tempbis[0] =~ /GRU\d+/ && $verif == 0;
		}
		$tempbis->finish;
		
        # Calcul du r�servable : 
        # - le mod�le de machine est sur l'agence concern�e
        #       et il n'est pas visible sur une autre agence
        #       et il n'est pas dans l'�tat "Transfert"
        # - Ou le mod�le de machine n'est pas sur l'agence concern�e mais il est visible sur cette agence
        my $queryReservable = "
        SELECT 
            ETCODE, 
            COUNT(*) 
        FROM AUTH.MACHINE 
        WHERE
            (AGAUTO = '" . $lag . "' AND MASUPPAGAUTO IS NULL AND ETCODE != 'MAC09'
                OR MASUPPAGAUTO = '" . $lag . "') 
        AND MOMAAUTO = " . $temp[0] . " 
        AND ETCODE NOT IN ('MAC08','GRU18') 
        AND LIETCODE < 20 GROUP BY ETCODE";

        my $result = $BD->prepare($queryReservable);
        $result->execute;
        while (my @tempReservable = $result->fetchrow_array()) 
        {
            if ($tempReservable[0] =~ /MAC\d+/ || $tempReservable[0] =~ /GRU\d+/)
            {
                $tabReservable->{$tempReservable[0]} = $tempReservable[1];
            }
            $verif = 1 if $tempReservable[0] =~ /MAC\d+/ && $verif == 0;
            $verif = 2 if $tempReservable[0] =~ /GRU\d+/ && $verif == 0;
        }
		
		foreach (keys %etat) {
			if ($verif == 1) {
				$tempbis = $BD->prepare(qq|select count(MOMAAUTO) FROM CONTRAT WHERE MOMAAUTO=$temp[0] AND CONTRATOK="-1" AND LIETCONTRATAUTO="1" AND MAAUTO="0" AND SOLOAUTO="0" AND AGAUTO="$lag"|);
				$tempbis->execute();
				($nbreserve) = $tempbis->fetchrow_array();
				$tempbis->finish;
			}

            if (($verif == 1 && $_ =~ /\w+02/) || 
                ($verif == 2 && $_ =~ /\w+[0-9]2/) || 
                ($_ =~ /\w+[0-9]3/) || 
                ($_ =~ /\w+[0-9]4/) || 
                ($_ =~ /\w+[0-9]7/) || 
                ($_ =~ /\w+[0-9]9/))
            {
                $nbreservable += (exists $tabReservable->{$_} ? $tabReservable->{$_} : 0);
            }

			$nbtotal += $etat{$_};
		}
		
		if ($verif == 1) 
		{
			$loue = $etat{"MAC01"};
			$disp = $etat{"MAC02"};
			$recu = $etat{"MAC03"};
			$revi = $etat{"MAC04"};
			$cont = $etat{"MAC05"};
			$pann = $etat{"MAC06"};
			$veri = $etat{"MAC07"};
			$tran = $etat{"MAC09"};
			$livr = $etat{"MAC12"};
		}
		if ($verif == 2) 
		{
			$loue = $etat{"GRU11"};
			$disp = $etat{"GRU12"};
			$recu = $etat{"GRU13"};
			$revi = $etat{"GRU14"};
			$cont = $etat{"GRU15"};
			$pann = $etat{"GRU16"};
			$veri = $etat{"GRU17"};
			$tran = $etat{"GRU19"};
		}
		if ($rend{$temp[0]}{$lag} > 0)
		{
			$nbtotal -= $rend{$temp[0]}{$lag};
			$tran    -= $rend{$temp[0]}{$lag};
		}
		if ($BD->do(qq|update AUTH.`STOCKMODELE` set STMORESERVABLE=$nbreservable,STMORESERVE=$nbreserve,STMOTOTAL=$nbtotal,STMOLOU=$loue,STMODIS=$disp,STMOREC=$recu,STMOREV=$revi,STMOCON=$cont,STMOPAN=$pann,STMOVER=$veri,STMOTRA=$tran, STMOLIV=$livr where MOMAAUTO=$temp[0] and AGAUTO="$lag"|) ne "0E0") 
		{
			$i++;
			
			#print qq|RESERVABLE = $nbreservable  RESERVE = $nbreserve TOTAL = $nbtotal\nLOU = $loue  DIS = $disp  REC = $recu TRA = $tran\nREV = $revi  CON = $cont  PAN = $pann  VER = $veri\n|;
			$txt_aff .=qq{MOMAAUTO = $temp[0]  AGAUTO = $lag\n};
			$txt_aff .=qq|AVANT : RESERVABLE = $temp[1], RESERVE = $temp[2], TOTAL = $temp[3], LOU = $temp[4], DIS = $temp[5], REC = $temp[6], REV = $temp[7], CON = $temp[8], PAN = $temp[9], VER = $temp[10], TRA = $temp[11], LIV = $temp[12]\n|;
			$txt_aff .=qq|APRES : RESERVABLE = $nbreservable, RESERVE = $nbreserve, TOTAL = $nbtotal, LOU = $loue, DIS = $disp, REC = $recu, REV = $revi, CON = $cont, PAN = $pann, VER = $veri, TRA = $tran, LIV = $livr\n\n|;
		}
		$j++;
		
	}
	$temp->finish;
	#print "\nIl y a eu $i update a $lag\n\n";
  #print $txt_aff;
}

$BD->disconnect;
};

sub lst_agence 
	{
	my @param=@_;
	my $BDAG=&Environnement("LOCATION","FR");
	my $temp = $BDAG->prepare(qq|select AGAUTO from AUTH.`AGENCE` where PACODE="$param[0]" order by AGAUTO|);
	$temp->execute();
	while (my@temp = $temp->fetchrow_array()) 
		{	
		push @agence,"$temp[0]";
		}
	$temp->finish;
	$BDAG->disconnect;
}

sub connex {
my @param=@_;
$BD=&Environnement("LOCATION","$param[0]");
}

sub machine_rendu
	{
	my $req=qq{SELECT MOMAAUTO, TRANSFERTAGD,count(*) FROM 
AUTH.`TRANSFERT` 
LEFT JOIN AUTH.`MACHINE` ON MACHINE.MAAUTO=TRANSFERT.MAAUTO 
WHERE TRANSFERTDATEAR ="0000-000-00" AND TRANSFERT.ETCODE in ("MAC08","GRU18","MAC21","MAC22","MAC23","MAC24") GROUP BY  TRANSFERTAGD, MOMAAUTO};
 	my $sth = $BD->prepare($req);
	$sth->execute();
	while (my @data = $sth->fetchrow_array()) 
		{
		$rend{$data[0]}{$data[1]}=$data[2];	
		}
		$sth->finish;
	}

1;
