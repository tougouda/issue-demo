#!/usr/bin/perl

use lib '../lib';

use strict;
use DBI;
use clemail;
use accesbd;
use varglob;

my $tva = 0.196;

# Date du jour
my $jour = substr("00".(localtime)[3], -2);
my $mois = substr("00".((localtime)[4] + 1), -2);
my $annee = substr("00".((localtime)[5] + 1900), -4);
my $date = $jour."/".$mois."/".$annee;

my $heure = substr("00".(localtime)[2], -2).":"
	.substr("00".(localtime)[1], -2).":"
	.substr("00".(localtime)[0], -2);
#print "Debut : ".$heure."\n";
my @email;
# Adresse des destinataires
if ($G_ENVIR eq '') {
	@email=qw( );
}
else {
	@email   = qw(archive.dev@acces-industrie.com);

}
#my @email=qw(archive.dev@acces-industrie.com);

# Corps du message
my $html = &Debut_HTML.qq{<body bgcolor="#FFFFFF">
Voici en pi&egrave;ce jointe sous forme de tableau Excel l'&eacute;tat hebdomadaire de l'en-cours au $date.
</body>
</html>
};

# Connexion aux bases
my $dbh = &Environnement("LOCATION","FR");
if (&test_facturation($dbh)) {
	exit;
}

# Ouverture pi�ce jointe
my $sep = ";";
my $fich = "encours$jour$mois$annee.csv";
open(FIC,">$fich") or die "Impossible d'ouvrir le fichier $fich";
print FIC "Code client".$sep."Client".$sep."Code collectif".$sep."Siret".$sep
	."Agence".$sep."Contrats en cours".$sep."Montant TTC\n";

# R�cup�ration des donn�es
my $req=qq{
	select CONTRAT.CLAUTO, CLIENT.CLCODE, CLIENT.CLSTE, CLIENT.CLCOLLECTIF, CLIENT.CLSIREN,
		CONTRAT.AGAUTO, count(*) as NBCONTRAT
	from CONTRAT
	left join CLIENT
		on CONTRAT.CLAUTO=CLIENT.CLAUTO
	where CONTRATOK=-1
		and ETATFACTURE!='F'
		and LIETCONTRATAUTO in ('1','2','3','7')
	group by CONTRAT.CLAUTO
	order by CONTRAT.AGAUTO, CLIENT.CLSTE
};

my $sth=$dbh->prepare($req);
$sth->execute();

my $nbrows = $sth->rows();
my $index = 1;

#print "Debut piece jointe\n";
# Construction pi�ce jointe
while (my @data=$sth->fetchrow_array()) {
#	print $index."/".$nbrows."\r";
	for (my $i=1; $i<@data; $i++) {
		$data[$i] =~ s/\$sep/\s/g;
		$data[$i] =~ s/\r/\s/g;
		$data[$i] =~ s/\n/\s/g;
		print FIC $data[$i].$sep;
	}
	
	# Calcul du total de l'en-cours
	my $req2=qq{
		select MOHTFRS-IFNULL(sum(ENTETEMONTANTFR), 0)
		from CONTRAT
		left join MONTANT
			on CONTRAT.MOAUTO=MONTANT.MOAUTO
		left join ENTETEFACTURE
			on CONTRAT.CONTRATCODE=ENTETEFACTURE.CONTRATCODE
		where CONTRATOK=-1
			and CONTRAT.ETATFACTURE!='F'
			and CLAUTO='$data[0]'
			and LIETCONTRATAUTO in ('1','2','3','7')
		group by CONTRAT.CONTRATCODE
	};
	
	my $sth2=$dbh->prepare($req2);
	$sth2->execute();
	
	my $total = 0;
	while (my ($temp) = $sth2->fetchrow_array()) {
		$total += $temp;
	}	
	
	print FIC sprintf("%.2f", $total * (1 + $tva))."\n";
	$sth2->finish;
	$index++;
}
$sth->finish;

# Fermeture pi�ce jointe
close(FIC);

# Infos du mail
my $sbj=qq{Etat hebdomadaire de l'en-cours au $date};
#my $sbj=qq{*�* TEST *�* Etat hebdomadaire de l'en-cours au $date};
# Exp�diteur (juste l'adresse mail)
my $mailsSender = (keys(%{&LOC::Globals::get('mailsSender')}))[0];
my $stmp=$ENV{'SMTP_HOST'};
my $type="HTML";

# Envoi du mail � chacun des destinataires
for (my $i=0; $i<@email; $i++) {
	&env_email($stmp, $mailsSender, $email[$i], $sbj, $type, $html, $fich);
}
unlink($fich);

# D�connexion des bases
$dbh->disconnect;

$heure = substr("00".(localtime)[2], -2).":"
	.substr("00".(localtime)[1], -2).":"
	.substr("00".(localtime)[0], -2);
#print "\nFin : ".$heure."\n";


# D�but de page HTML (envoi par mail)
sub Debut_HTML {
	return qq{
<html>
<head>
<title>&Eacute;tat hebdomadaire de l'en-cours au $date</title>
<style type="text/css">
BODY {
	background: #EDEDE0;
	color:#000000;
	font-size:10pt;
	font-family: Arial, Helvetica, sans-serif;
FONT {font-size:10pt;}
FONT.PT {font-size:8pt;}
}
</style>
</head>
};
}


# Teste si la facturation a d�j� �t� faite
sub test_facturation {
	my ($DB) = @_;
	my $req = qq{SELECT count(*) FROM ENTETELIGNE WHERE TO_DAYS(DATEAUTO)=TO_DAYS(now())-1};
	my $sth = $DB->prepare("$req");
	$sth->execute();
	my ($nbre) = $sth->fetchrow_array();
	my $test = 0;
	$sth->finish;
	if ($nbre == 0) {
		# Ne pas ex�cuter
		$DB->disconnect;
		$test = 1;
	}
	
	return $test;
}
