#!/usr/bin/perl

use lib '../lib', '../../inc', '../../lib';

use strict;
use DBI;
use clemail;
use accesbd;
use varglob;
use message;
use mymath;
use LOC::Characteristic;

use JSON::XS;
use Tie::IxHash;

# Environnement
my $env = $G_ENVIR;
$env =~ s/ //g;

# Pays
my @tabCountry = qw(FR);

my @mess = &message("FR");

# Nombre de d�cimales pour les taux d'utilisation
my $nbDecimals = 0;

# Connexion � la base de donn�es
my $db = &Environnement('LOCATION', 'FR');
my $req;
my $sth;

# Liste des familles tarifaires � exclure (FAMTARAUTO)
my $charac;
my $req = '
SELECT IFNULL(ccy_value, cgl_value)
FROM AUTH.p_characteristic
LEFT JOIN AUTH.p_characteristic_global ON chr_id = cgl_chr_id
LEFT JOIN AUTH.p_characteristic_country ON (cgl_chr_id = ccy_chr_id AND ccy_cty_id = "' . $G_PAYS . '")
WHERE chr_code = "VISUTU"';

my $sth = $db->prepare($req);
$sth->execute();

while (my @data = $sth->fetchrow_array())
{
    $charac = $data[0];
}

$sth->finish();

my @tabTarFam = ();
if ($charac ne '')
{
    my $decodedCarac = JSON::XS->new->decode($charac);
    @tabTarFam = @{$decodedCarac->{'tariffFamiliesToExclude'}};
}

# R�cup�ration des r�gions
$req =qq{
SELECT are_id AS `id`, are_label AS `label`, agc_id AS `agency.id`, agc_label AS `agency.label`,
    usr_firstname AS `user.firstname`, usr_name AS `user.name`, usr_mail AS `user.email`
FROM frmwrk.p_area
LEFT JOIN frmwrk.p_agency ON are_id = agc_are_id
LEFT JOIN frmwrk.f_user ON p_area.are_usr_id = f_user.usr_id
WHERE are_sta_id = 'GEN01' AND are_cty_id IN ("} . join('", "', @tabCountry). qq{")
    AND agc_sta_id = 'GEN01' AND agc_type = 'AGC'
ORDER BY `label`, `agency.label`
};

$sth = $db->prepare($req);
$sth->execute();

tie(my %tabArea, 'Tie::IxHash');
my @tabAgency= ();
while (my $data = $sth->fetchrow_hashref())
{
	$tabArea{$data->{'id'}}->{'label'}   = $data->{'label'};
	$tabArea{$data->{'id'}}->{'manager'} = $data->{'user.firstname'} . ' ' . $data->{'user.name'};
	$tabArea{$data->{'id'}}->{'email'}   = $data->{'user.email'};
	$tabArea{$data->{'id'}}->{'agencies'}->{$data->{'agency.id'}} = $data->{'agency.label'};

	push(@tabAgency, $data->{'agency.id'});
}
my $tabArea = \%tabArea;

$sth->finish;

# R�cup�ration des familles tarifaires
$req =qq{
SELECT FAMTARAUTO AS `id`, FAMTARLIBELLE AS `label`, FAMTARSOUSLIBELLE AS `sublabel`
FROM FAMILLETARIFAIRE};
if (@tabTarFam > 0)
{
    $req .= qq{
WHERE FAMTARAUTO NOT IN (} . join(', ', @tabTarFam) . qq{)
};
}
$req .= qq{
ORDER BY FAMTARORDRE
};

$sth = $db->prepare($req);
$sth->execute();

tie(my %tabTariffFamily, 'Tie::IxHash');
while (my $data = $sth->fetchrow_hashref())
{
	$tabTariffFamily{$data->{'id'}}->{'label'}    = $data->{'label'};
	$tabTariffFamily{$data->{'id'}}->{'sublabel'} = $data->{'sublabel'};
}
my $tabTariffFamily = \%tabTariffFamily;

$sth->finish;

# R�cup�ration des taux d'utilisation
$req =qq{
SELECT SM.AGAUTO AS `agency.id`, MM.FAMTARAUTO AS `tariffFamily.id`,
    SUM(STMOLOU) + SUM(STMOLIV) AS `rented`,
    SUM(STMOTOTAL) AS `total`, 
    agc_are_id AS `area.id`,
    SUM(STMODIS) AS `available`, 
    SUM(STMOVER) AS `check`,
    SUM(STMOPAN) AS `outOfOrder`, 
    SUM(STMOREC) AS `recovery`,
    SUM(STMOCON) AS `control`,
    SUM(STMOREV) AS `revision`
FROM AUTH.STOCKMODELE SM
INNER JOIN AUTH.MODELEMACHINE MM ON SM.MOMAAUTO = MM.MOMAAUTO
LEFT JOIN frmwrk.p_agency ON CONVERT(SM.AGAUTO USING utf8) = agc_id
WHERE SM.AGAUTO IN ("} . join('", "', @tabAgency). qq{")
AND MM.FAMTARAUTO NOT IN ("} . join('", "', @tabTarFam) . qq{") 
GROUP BY SM.AGAUTO, MM.FAMTARAUTO
};

$sth = $db->prepare($req);
$sth->execute();

my $tabAgencyData   = {};
my $tabAreaData     = {};
my $tabNationalData = {};
while (my $data = $sth->fetchrow_hashref())
{
    # Donn�es de l'agence
    $tabAgencyData->{$data->{'agency.id'}}->{$data->{'tariffFamily.id'}}->{'rented'} = $data->{'rented'};
    $tabAgencyData->{$data->{'agency.id'}}->{$data->{'tariffFamily.id'}}->{'total'}  = $data->{'total'};
    $tabAgencyData->{$data->{'agency.id'}}->{'rented'}     += $data->{'rented'};
    $tabAgencyData->{$data->{'agency.id'}}->{'available'}  += $data->{'available'};
    $tabAgencyData->{$data->{'agency.id'}}->{'check'}      += $data->{'check'};
    $tabAgencyData->{$data->{'agency.id'}}->{'outOfOrder'} += $data->{'outOfOrder'};
    $tabAgencyData->{$data->{'agency.id'}}->{'recovery'}   += $data->{'recovery'};
    $tabAgencyData->{$data->{'agency.id'}}->{'control'}    += $data->{'control'};
    $tabAgencyData->{$data->{'agency.id'}}->{'revision'}   += $data->{'revision'};
    $tabAgencyData->{$data->{'agency.id'}}->{'total'}      += $data->{'total'};

    # Donn�es r�gionales
    $tabAreaData->{$data->{'area.id'}}->{$data->{'tariffFamily.id'}}->{'rented'} += $data->{'rented'};
    $tabAreaData->{$data->{'area.id'}}->{$data->{'tariffFamily.id'}}->{'total'}  += $data->{'total'};
    $tabAreaData->{$data->{'area.id'}}->{'rented'}     += $data->{'rented'};
    $tabAreaData->{$data->{'area.id'}}->{'available'}  += $data->{'available'};
    $tabAreaData->{$data->{'area.id'}}->{'check'}      += $data->{'check'};
    $tabAreaData->{$data->{'area.id'}}->{'outOfOrder'} += $data->{'outOfOrder'};
    $tabAreaData->{$data->{'area.id'}}->{'recovery'}   += $data->{'recovery'};
    $tabAreaData->{$data->{'area.id'}}->{'control'}    += $data->{'control'};
    $tabAreaData->{$data->{'area.id'}}->{'revision'}   += $data->{'revision'};
    $tabAreaData->{$data->{'area.id'}}->{'total'}      += $data->{'total'};

    # Donn�es nationales
    $tabNationalData->{$data->{'tariffFamily.id'}}->{'rented'} += $data->{'rented'};
    $tabNationalData->{$data->{'tariffFamily.id'}}->{'total'}  += $data->{'total'};
    $tabNationalData->{'rented'}     += $data->{'rented'};
    $tabNationalData->{'available'}  += $data->{'available'};
    $tabNationalData->{'check'}      += $data->{'check'};
    $tabNationalData->{'outOfOrder'} += $data->{'outOfOrder'};
    $tabNationalData->{'recovery'}   += $data->{'recovery'};
    $tabNationalData->{'control'}    += $data->{'control'};
    $tabNationalData->{'revision'}   += $data->{'revision'};
    $tabNationalData->{'total'}      += $data->{'total'};
}

$sth->finish;

# D�connexion de la base de donn�es
$db->disconnect;


# Envoi des e-mails par r�gion
foreach my $areaId (keys(%$tabArea))
{
    &sendEmail([$areaId], [$tabArea{$areaId}->{'email'}]);
}

# Envoi de l'e-mail g�n�ral

my @tabRecipients = split(',', &LOC::Characteristic::getCountryValueByCode('STAUTILISATION', 'FR'));
my @tabAreaId = keys(%$tabArea);
&sendEmail(\@tabAreaId, \@tabRecipients);


# Fonction d'envoi des e-mails
sub sendEmail
{
    my ($tabAreaId, $tabRecipients) = @_;

    # Serveur SMTP
    my $smtp = $ENV{'SMTP_HOST'};

    # Exp�diteur (juste l'adresse mail)
    my $mailsSender = (keys(%{&LOC::Globals::get('mailsSender')}))[0];

    # Objet
    my @tabLocaltime = localtime();
    my $date = sprintf('%02d/%02d/%04d', $tabLocaltime[3], $tabLocaltime[4] + 1, $tabLocaltime[5] + 1900);
    my $subject = sprintf('Taux d\'utilisation au %s', $date);
    if ($G_ENVIR ne '')
    {
        $subject = '[' . uc($env) . ']' . $subject;
    }

    # Corps
    my $html = qq{<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr_FR" xml:lang="fr_FR">
<head>
<title>$subject</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15" />
<style>
html, body
{
    font-family: 'Segoe UI', Arial, Helvetica, Sans-serif;
    font-size: 10pt;
}
h1
{
    font-size: 17pt;
    /* color: #007AC2; */
    color: #5E81BC;
    /* border-bottom: 1px solid grey; */
    font-weight: normal;
}
div.recipient
{
    font-weight: bold;
    font-size: 9pt;
    margin-bottom: 2em;
}
div.recipient, div.recipient a
{
    color: grey;
}
table.data
{
    border-collapse: collapse;
    border-bottom: 1px solid grey;
}
table.data th, td.total
{
    border-bottom: 1px solid grey;
    border-top: 1px solid grey;
}
table.data th.left
{
    text-align: left;
}
table.data th, table.data td
{
    padding: 0px 5px;
}
table.data td.useRate, table.data td.nbMach
{
    text-align: right;
    white-space: nowrap;
}
table.data td.none
{
    color: lightgrey;
}
table.data td.total
{
    font-weight: bold;
    font-style: italic;
}
table.data td.addLines
{
    font-style: italic;
}
</style>
</head>

<body>
};

    if ($env ne '')
    {
        my $recipients = '';
        foreach my $email (@$tabRecipients)
        {
            $recipients .= $email . '; ';
        }
        $recipients = substr($recipients, 0, -2);
        
        $html .= qq{
<div class="recipient">E-mail envoy� � $recipients</div>
};
    }

    foreach my $areaId (@$tabAreaId)
    {
        $html .= qq{
<h1>$tabArea->{$areaId}->{'label'}</h1>};

        my $nbAgencies = keys(%{$tabArea->{$areaId}->{'agencies'}});
        my $colspan = 2 * ($nbAgencies + 2);

        $html .= qq{
<table class="data">
<thead>
<tr>
<th class="left" rowspan="2">Famille tarifaire</th>
<th colspan="$colspan">% util. inst. (nb mach.)</th>
</tr>
<tr>};

        foreach my $agencyId (sort(keys(%{$tabArea->{$areaId}->{'agencies'}})))
        {
            $html .= qq{
<th class="left" colspan="2">$agencyId</th>};
        }

        $html .= qq{
<th class="left" colspan="2">R�gional</th>
<th class="left" colspan="2">National</th>
</tr>
</thead>
<tbody>};
        
#     };
        
        ########################################################################
        # Ligne des dispo
        ########################################################################
        $html .= qq{
<tr>
<td class="addLines">$mess[4351]</td>};
        # Taux par agence
        foreach my $agencyId (sort(keys(%{$tabArea->{$areaId}->{'agencies'}})))
        {
            my $available = $tabAgencyData->{$agencyId}->{'available'};
            my $total     = $tabAgencyData->{$agencyId}->{'total'};

            my $useRate = &arrondi(($total == 0 ? 0 : $available / $total) * 100, $nbDecimals) . ' %';

            $html .= qq{
<td class="useRate addLines">$useRate</td><td class="nbMach addLines">$available</td>};
        }

        # Taux r�gional
        my $available = $tabAreaData->{$areaId}->{'available'};
        my $total     = $tabAreaData->{$areaId}->{'total'};

        my $useRate = &arrondi(($total == 0 ? 0 : $available / $total) * 100, $nbDecimals) . ' %';

        $html .= qq{
<td class="useRate addLines">$useRate</td><td class="nbMach addLines">$available</td>};

        # Taux national
        $available = $tabNationalData->{'available'};
        $total     = $tabNationalData->{'total'};

        $useRate = &arrondi(($total == 0 ? 0 : $available / $total) * 100, $nbDecimals) . ' %';

        $html .= qq{
<td class="useRate addLines">$useRate</td><td class="nbMach addLines">$available</td>};

        $html .= qq{
</tr>};
        
        ########################################################################
        # Ligne des r�cup
        ########################################################################
        $html .= qq{
<tr>
<td class="addLines">$mess[4361]</td>};
        # Taux par agence
        foreach my $agencyId (sort(keys(%{$tabArea->{$areaId}->{'agencies'}})))
        {
            my $recovery = $tabAgencyData->{$agencyId}->{'recovery'};
            my $total    = $tabAgencyData->{$agencyId}->{'total'};

            my $useRate = &arrondi(($total == 0 ? 0 : $recovery / $total) * 100, $nbDecimals) . ' %';

            $html .= qq{
<td class="useRate addLines">$useRate</td><td class="nbMach addLines">$recovery</td>};
        }

        # Taux r�gional
        my $recovery = $tabAreaData->{$areaId}->{'recovery'};
        my $total    = $tabAreaData->{$areaId}->{'total'};

        my $useRate = &arrondi(($total == 0 ? 0 : $recovery / $total) * 100, $nbDecimals) . ' %';

        $html .= qq{
<td class="useRate addLines">$useRate</td><td class="nbMach addLines">$recovery</td>};

        # Taux national
        $recovery = $tabNationalData->{'recovery'};
        $total    = $tabNationalData->{'total'};

        $useRate = &arrondi(($total == 0 ? 0 : $recovery / $total) * 100, $nbDecimals) . ' %';

        $html .= qq{
<td class="useRate addLines">$useRate</td><td class="nbMach addLines">$recovery</td>};

        $html .= qq{
</tr>};
        
        ########################################################################
        # Ligne des controles
        ########################################################################
        $html .= qq{
<tr>
<td class="addLines">$mess[4350]</td>};
        # Taux par agence
        foreach my $agencyId (sort(keys(%{$tabArea->{$areaId}->{'agencies'}})))
        {
            my $control = $tabAgencyData->{$agencyId}->{'control'};
            my $total   = $tabAgencyData->{$agencyId}->{'total'};

            my $useRate = &arrondi(($total == 0 ? 0 : $control / $total) * 100, $nbDecimals) . ' %';

            $html .= qq{
<td class="useRate addLines">$useRate</td><td class="nbMach addLines">$control</td>};
        }

        # Taux r�gional
        my $control = $tabAreaData->{$areaId}->{'control'};
        my $total   = $tabAreaData->{$areaId}->{'total'};

        my $useRate = &arrondi(($total == 0 ? 0 : $control / $total) * 100, $nbDecimals) . ' %';

        $html .= qq{
<td class="useRate addLines">$useRate</td><td class="nbMach addLines">$control</td>};

        # Taux national
        $control = $tabNationalData->{'control'};
        $total   = $tabNationalData->{'total'};

        $useRate = &arrondi(($total == 0 ? 0 : $control / $total) * 100, $nbDecimals) . ' %';

        $html .= qq{
<td class="useRate addLines">$useRate</td><td class="nbMach addLines">$control</td>};

        $html .= qq{
</tr>};
        
        ########################################################################
        # Ligne des v�rification
        ########################################################################
        $html .= qq{
<tr>
<td class="addLines">$mess[4356]</td>};
        # Taux par agence
        foreach my $agencyId (sort(keys(%{$tabArea->{$areaId}->{'agencies'}})))
        {
            my $check = $tabAgencyData->{$agencyId}->{'check'};
            my $total = $tabAgencyData->{$agencyId}->{'total'};

            my $useRate = &arrondi(($total == 0 ? 0 : $check / $total) * 100, $nbDecimals) . ' %';

            $html .= qq{
<td class="useRate addLines">$useRate</td><td class="nbMach addLines">$check</td>};
        }

        # Taux r�gional
        my $check = $tabAreaData->{$areaId}->{'check'};
        my $total = $tabAreaData->{$areaId}->{'total'};

        my $useRate = &arrondi(($total == 0 ? 0 : $check / $total) * 100, $nbDecimals) . ' %';

        $html .= qq{
<td class="useRate addLines">$useRate</td><td class="nbMach addLines">$check</td>};

        # Taux national
        $check = $tabNationalData->{'check'};
        $total = $tabNationalData->{'total'};

        $useRate = &arrondi(($total == 0 ? 0 : $check / $total) * 100, $nbDecimals) . ' %';

        $html .= qq{
<td class="useRate addLines">$useRate</td><td class="nbMach addLines">$check</td>};

        $html .= qq{
</tr>};
        
        ########################################################################
        # Ligne des r�vision
        ########################################################################
        $html .= qq{
<tr>
<td class="addLines">$mess[4355]</td>};
        # Taux par agence
        foreach my $agencyId (sort(keys(%{$tabArea->{$areaId}->{'agencies'}})))
        {
            my $revision = $tabAgencyData->{$agencyId}->{'revision'};
            my $total    = $tabAgencyData->{$agencyId}->{'total'};

            my $useRate = &arrondi(($total == 0 ? 0 : $revision / $total) * 100, $nbDecimals) . ' %';

            $html .= qq{
<td class="useRate addLines">$useRate</td><td class="nbMach addLines">$revision</td>};
        }

        # Taux r�gional
        my $revision = $tabAreaData->{$areaId}->{'revision'};
        my $total    = $tabAreaData->{$areaId}->{'total'};

        my $useRate = &arrondi(($total == 0 ? 0 : $revision / $total) * 100, $nbDecimals) . ' %';

        $html .= qq{
<td class="useRate addLines">$useRate</td><td class="nbMach addLines">$revision</td>};

        # Taux national
        $revision = $tabNationalData->{'revision'};
        $total    = $tabNationalData->{'total'};

        $useRate = &arrondi(($total == 0 ? 0 : $revision / $total) * 100, $nbDecimals) . ' %';

        $html .= qq{
<td class="useRate addLines">$useRate</td><td class="nbMach addLines">$revision</td>};

        $html .= qq{
</tr>};
        
        ########################################################################
        # Ligne des pannes
        ########################################################################
        $html .= qq{
<tr>
<td class="addLines">$mess[4353]</td>};
        # Taux par agence
        foreach my $agencyId (sort(keys(%{$tabArea->{$areaId}->{'agencies'}})))
        {
            my $outOfOrder = $tabAgencyData->{$agencyId}->{'outOfOrder'};
            my $total      = $tabAgencyData->{$agencyId}->{'total'};

            my $useRate = &arrondi(($total == 0 ? 0 : $outOfOrder / $total) * 100, $nbDecimals) . ' %';

            $html .= qq{
<td class="useRate addLines">$useRate</td><td class="nbMach addLines">$outOfOrder</td>};
        }

        # Taux r�gional
        my $outOfOrder = $tabAreaData->{$areaId}->{'outOfOrder'};
        my $total      = $tabAreaData->{$areaId}->{'total'};

        my $useRate = &arrondi(($total == 0 ? 0 : $outOfOrder / $total) * 100, $nbDecimals) . ' %';

        $html .= qq{
<td class="useRate addLines">$useRate</td><td class="nbMach addLines">$outOfOrder</td>};

        # Taux national
        $outOfOrder = $tabNationalData->{'outOfOrder'};
        $total      = $tabNationalData->{'total'};

        $useRate = &arrondi(($total == 0 ? 0 : $outOfOrder / $total) * 100, $nbDecimals) . ' %';

        $html .= qq{
<td class="useRate addLines">$useRate</td><td class="nbMach addLines">$outOfOrder</td>};

        $html .= qq{
</tr>};
        
        ########################################################################
        # Ligne de Total
        ########################################################################
        $html .= qq{
<tr>
<td class="total">TOTAL</td>};
        # Taux par agence
        foreach my $agencyId (sort(keys(%{$tabArea->{$areaId}->{'agencies'}})))
        {
            my $rented = $tabAgencyData->{$agencyId}->{'rented'};
            my $total  = $tabAgencyData->{$agencyId}->{'total'};

            my $useRate = &arrondi(($total == 0 ? 0 : $rented / $total) * 100, $nbDecimals) . ' %';

            $html .= qq{
<td class="useRate total">$useRate</td><td class="nbMach total">($total)</td>};
        }

        # Taux r�gional
        my $rented = $tabAreaData->{$areaId}->{'rented'};
        my $total  = $tabAreaData->{$areaId}->{'total'};

        my $useRate = &arrondi(($total == 0 ? 0 : $rented / $total) * 100, $nbDecimals) . ' %';

        $html .= qq{
<td class="useRate total">$useRate</td><td class="nbMach total">($total)</td>};

        # Taux national
        $rented = $tabNationalData->{'rented'};
        $total  = $tabNationalData->{'total'};

        $useRate = &arrondi(($total == 0 ? 0 : $rented / $total) * 100, $nbDecimals) . ' %';

        $html .= qq{
<td class="useRate total">$useRate</td><td class="nbMach total">($total)</td>};

        $html .= qq{
</tr>};
        

        ########################################################################
        # D�tail par famille tarifaire
        ########################################################################
        foreach my $tariffFamilyId (keys(%$tabTariffFamily))
        {
            # Ne pas afficher s'il n'y a aucune machine de cette famille tarifaire dans le parc national
            if ($tabNationalData->{$tariffFamilyId}->{'total'} == 0)
            {
                next;
            }
            
            $html .= qq{
<tr>
<td>$tabTariffFamily->{$tariffFamilyId}->{'label'} $tabTariffFamily->{$tariffFamilyId}->{'sublabel'}</td>};

            # Taux par agence
            foreach my $agencyId (sort(keys(%{$tabArea->{$areaId}->{'agencies'}})))
            {
                my $rented = $tabAgencyData->{$agencyId}->{$tariffFamilyId}->{'rented'};
                my $total  = $tabAgencyData->{$agencyId}->{$tariffFamilyId}->{'total'};

                my $useRate = &arrondi(($total == 0 ? 0 : $rented / $total) * 100, $nbDecimals) . ' %';
                my $class = ($total == 0 ? 'none' : '');

                $html .= qq{
<td class="useRate $class">$useRate</td><td class="nbMach $class">($total)</td>};
            }

            # Taux r�gional
            my $rented = $tabAreaData->{$areaId}->{$tariffFamilyId}->{'rented'};
            my $total  = $tabAreaData->{$areaId}->{$tariffFamilyId}->{'total'};

            my $useRate = &arrondi(($total == 0 ? 0 : $rented / $total) * 100, $nbDecimals) . ' %';
            my $class = ($total == 0 ? 'none' : '');

            $html .= qq{
<td class="useRate $class">$useRate</td><td class="nbMach $class">($total)</td>};

            # Taux national
            $rented = $tabNationalData->{$tariffFamilyId}->{'rented'};
            $total  = $tabNationalData->{$tariffFamilyId}->{'total'};
            $class = ($total == 0 ? 'none' : '');

            $useRate = &arrondi(($total == 0 ? 0 : $rented / $total) * 100, $nbDecimals) . ' %';

            $html .= qq{
<td class="useRate $class">$useRate</td><td class="nbMach $class">($total)</td>};

            $html .= qq{
</tr>};
        }

        $html .= qq{
</tbody>
</table>

</body>
</html>};
    }

    # Changement de destinataires si on n'est pas sur l'exploit
    if ($env ne '')
    {
        $tabRecipients = ['archive.dev@acces-industrie.com'];
    }
    
    # Construction de la cha�ne des destinataires
    my $recipients = '';
    foreach my $email (@$tabRecipients)
    {
        $recipients .= sprintf('%s, ', $email);
    }
    $recipients = substr($recipients, 0, -2);
    
    # Envoi du mail
    &env_email($smtp, $mailsSender, $recipients, $subject, 'HTML', $html);
}