#!/usr/bin/perl

use utf8;    # Tout le code du scope est considéré comme encodé en UTF-8
use open (':encoding(UTF-8)', ':std');    # Tous les flux d'entrée et de sortie sont considérés comme encodés en UTF-8
                                          # - :std affecte STDIN, STDOUT et STDERR et le changement est global
                                          # - pour les autres flux, le changement ne concerne que le scope

use strict;

use lib 'inc', 'lib';

# Traitement des variables d'environnement
use LOC::Env;

# Interactions avec le navigateur web (notamment l'affichage des erreurs)
use LOC::Browser;

&LOC::Browser::init();


# Modules internes
use Tie::IxHash;
use LOC::Globals;
use LOC::Controller::Front;
use LOC::Util;
use LOC::Db;
use LOC::Locale;

# Autres modules
use File::Basename;


# C'est parti
&LOC::Controller::Front::setDefaultModuleName('default');
&LOC::Controller::Front::setDefaultControllerName('home');
&LOC::Controller::Front::setDefaultActionName('view');

&LOC::Controller::Front::dispatch(dirname($0));

close(STDOUT);
exit;
