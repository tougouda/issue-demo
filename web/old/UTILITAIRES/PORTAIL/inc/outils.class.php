<?php

/**
 * La classe Outils fournit un ensemble de fonctions utilitaires du framework
 */
Class Outils
{
	/**
	 * Constructeur de la classe, vide.
	 */
	function __construct()
	{
	}

    /**
     * D�code un texte crypt�
     * @param string $encryptedText Texte crypt�
     * @param string $encryptKey    Cl� de chiffrement
     * @param string $iv            Vecteur d'initialisation
     */
    public static function decrypt($encryptedText, $encryptKey, $iv)
    {
        return self::pkcs7Unpad(openssl_decrypt($encryptedText, 'AES-256-CBC', $encryptKey, 0, $iv));
    }

    /**
     * Supprime le remplissage PKCS7
     * @param string $data Donn�es
     * @return string
     */
    public static function pkcs7Unpad($data)
    {
        return substr($data, 0, -ord($data[strlen($data) - 1]));
    }
}
?>