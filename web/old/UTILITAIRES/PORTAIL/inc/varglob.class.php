<?php

require_once('inc/outils.class.php');

if (!class_exists('Varglob'))
{

/**
 * Classe contenant toutes les configurations n�cessaire
 */
class Varglob
{
    /**
     * Tableau des serveurs de base de donn�es
     *
     * @var array
     */
    var $SERVER;

    /**
     * Tableau des connexions aux bases de donn�es par pays
     *
     * @var array
     */
    var $CONNECTION;

    /**
     * Environnement
     * @var string
     */
    var $ENV;

    /**
     * Serveur de d�v
     * @var string
     */
    var $DEV;

    /**
     * Emplacements
     * @var string
     */
    var $PATH_SRVCONF;
    var $PATH_COMMONFILES;
    var $PATH_COMMONFILES_TEMP;

    /**
     * URL pour les �l�ments web
     * @var string
     */
    var $pweb;

    /**
     * Constructeur de la classe, initialise toutes les variables
     */
    function Varglob($devdir = '')
    {
        if ($_SERVER['ENV'] == 'dev')
        {
            // Environement de d�v
            $this->DEV = (isset($_GET['dev']) ? $_GET['dev'] : '1');
        }

        $dbInfos = json_decode($_SERVER['DB']);
        if (isset($this->DEV))
        {
            if (isset($dbInfos->{$this->DEV}))
            {
                $dbInfos = $dbInfos->{$this->DEV};
            }
            else if ($this->DEV != '1')
            {
                die('L\'environnement de DEV demand� n\'est pas configur�');
            }
        }

        // Informations de crytage
        $tabCryptInfos = [
            'DB' => [
                'key' => base64_decode('nNxA8CDOi2Ikt9EbIFamyT2RojFejf0s2ra2Y/bXYN0='),
                'iv'  => base64_decode('3/iCm+PaSayitFx9Sf9HsQ==')
            ]
        ];

        $outils = new Outils();

        // Serveurs pour les connections aux bases de donn�es
        $this->SERVER = array(
                             'mysql' => array(
                                            'type'     => 'database',
                                            'host'     => $dbInfos->host,
                                            'configs'  => array(
                                                              'dbtype'   => 'MySQL',
                                                              'version'  => 5,
                                                              'username' => $dbInfos->user,
                                                              'password' => $outils->decrypt($dbInfos->password, $tabCryptInfos['DB']['key'], $tabCryptInfos['DB']['iv'])
                                                          )
                                        ),
                         );

        $this->pweb = $_SERVER['LOC_URL'] . '/web/old';

        // Environnement
        $this->ENV = strtoupper($_SERVER['ENV']);

        // Connexions pour chaque pays
        $this->CONNECTION = array(
                                'statistics' => array(
                                                'serverid' => 'mysql',
                                                'dbname'   => 'statistics',
                                                'charset'  => 'utf8'
                                            ),
                            );

        $this->PATH_COMMONFILES      = $_SERVER['COMMONFILES_HOME'] . '/sta';
        $this->PATH_COMMONFILES_TEMP = $this->PATH_COMMONFILES . '/temp';
    }

    /**
     * R�cup�ration de l'instance du Varglob (Singleton)
     *
     * @param string $devdir R�pertoire de d�veloppement (facultatif)
     * @return Varglob Instance du Varglob
     */
    public static function getInstance($devdir = '')
    {
        /**
         * Instance de la classe Varglob
         * @var Varglob
         */
        static $instance = NULL;

        if ($instance === NULL)
        {
            $class = __CLASS__;
            $instance = new $class($devdir);
        }

        return $instance;
    }
}

} // Class Exists
