<?php

require_once('inc/varglob.class.php');

$varglob = Varglob::getInstance();

$tabMsg = array();
// Import des CA SAV
if (!empty($_POST['caSavBtn']))
{
    $tabMsg['caSav'] = array('ok' => '', 'ko' => '');
    if (isset($_FILES['caSavFile']) && is_uploaded_file($_FILES['caSavFile']["tmp_name"]))
    {
        $tabData = array();
        if (preg_match('/\.csv$/', $_FILES['caSavFile']['name']))
        {
            if ($fh = fopen($_FILES['caSavFile']["tmp_name"], 'r'))
            {
                $row = 1;
                while (($data = fgetcsv($fh, 1000, ";")) !== FALSE)
                {
                    $num = count($data);
                    if ($num == 4)
                    {
                        $agency = $data[0];
                        $year = $data[1];
                        $month = $data[2];
                        $value = preg_replace('/\s/', '', $data[3]);
                        $value = preg_replace('/,/', '.', $value);
                        if (empty($value)) {
                            $value = 0;
                        }

                        if (!empty($agency) && !is_numeric($agency) && strlen($agency) == 3 &&
                            is_numeric($year) && ($year / 2000) >= 1 &&
                            is_numeric($month) && $month >= 1  && $month <= 12 &&
                            is_numeric($value))
                        {
                            $tabData[] = array(
                                'agency' => $agency,
                                'year' => $year,
                                'month' => $month,
                                'value' => $value);
                        }
                        else if ($row > 1)
                        {
                            $tabMsg['caSav']['ko'] = 'Le fichier comporte des erreurs, en ligne '.$row.' : mauvais format des donn�es';
                            break;
                        }
                    }
                    else
                    {
                        $tabMsg['caSav']['ko'] = 'Le fichier doit comporter 4 colonnes ('.$num.' en ligne '.$row.').';
                        break;
                    }
                    $row++;
                }
                fclose($fh);
            }
            else {
                $tabMsg['caSav']['ko'] = 'Impossible d\'ouvrir le fichier';
            }
        }
        else {
            $tabMsg['caSav']['ko'] = 'Le fichier n\'a pas la bonne extension';
        }

        if (empty($tabMsg['caSav']['ko']) && !empty($tabData))
        {
            if (importDb('amtbreakage', $tabData)) {
                $tabMsg['caSav']['ok'] = 'Le fichier a �t� correctement import� ('.($row - 1).' lignes trait�es)';
            }
            else {
                $tabMsg['caSav']['ko'] = 'Une erreur SQL s\'est produite, aucune ligne import�e';
            }
        }
    }
}
// Import des VGP
else if (!empty($_POST['vgpBtn']))
{
    $tabMsg['vgp'] = array('ok' => '', 'ko' => '');
    if (isset($_FILES['vgpFile']) && is_uploaded_file($_FILES['vgpFile']["tmp_name"]))
    {
        $tabData = array();
        if (preg_match('/\.csv$/', $_FILES['vgpFile']['name']))
        {
            if ($fh = fopen($_FILES['vgpFile']["tmp_name"], 'r'))
            {
                $row = 1;
                while (($data = fgetcsv($fh, 1000, ";")) !== FALSE)
                {
                    $num = count($data);
                    if ($num == 6)
                    {
                        $agency = $data[0];
                        $type = $data[1];
                        $year = $data[2];
                        $month = $data[3];
                        $value1 = preg_replace('/\s/', '', $data[4]);
                        $value1 = preg_replace('/,/', '.', $value1);
                        $value2 = preg_replace('/\s/', '', $data[5]);
                        $value2 = preg_replace('/,/', '.', $value2);
                        if (empty($value1)) {
                            $value1 = 0;
                        }
                        if (empty($value2)) {
                            $value1 = 0;
                        }

                        if (!empty($agency) && !is_numeric($agency) && strlen($agency) == 3 &&
                            ($type == 'AGENCE' || $type == 'CHANTIER') &&
                            is_numeric($year) && ($year / 2000) >= 1 &&
                            is_numeric($month) && $month >= 1  && $month <= 12 &&
                            is_numeric($value1) && is_numeric($value2))
                        {
                            if ($type == 'AGENCE') {
                                $type = 'agc';
                            }
                            else if ($type == 'CHANTIER') {
                                $type = 'site';
                            }
                            $tabData['cntvgp'.$type][] = array(
                                'agency' => $agency,
                                'year' => $year,
                                'month' => $month,
                                'value' => $value1);
                            $tabData['amtvgp'.$type][] = array(
                                'agency' => $agency,
                                'year' => $year,
                                'month' => $month,
                                'value' => $value2);
                        }
                        else if ($row > 1)
                        {
                            $tabMsg['vgp']['ko'] = 'Le fichier comporte des erreurs, en ligne '.$row.' : mauvais format des donn�es';
                            break;
                        }
                    }
                    else
                    {
                        $tabMsg['vgp']['ko'] = 'Le fichier doit comporter 6 colonnes ('.$num.' en ligne '.$row.').';
                        break;
                    }
                    $row++;
                }
                fclose($fh);
            }
            else {
                $tabMsg['vgp']['ko'] = 'Impossible d\'ouvrir le fichier';
            }
        }
        else {
            $tabMsg['vgp']['ko'] = 'Le fichier n\'a pas la bonne extension';
        }

        if (empty($tabMsg['vgp']['ko']) && !empty($tabData))
        {
            foreach ($tabData as $indicator => $values)
            {
                if (!importDb($indicator, $values)) {
                    $tabMsg['vgp']['ko'] = 'Une erreur SQL s\'est produite, aucune ligne ne sera import�e';
                    break;
                }
            }
            if (empty($tabMsg['vgp']['ko'])) {
                $tabMsg['vgp']['ok'] = 'Le fichier a �t� correctement import� ('.($row - 1).' lignes trait�es)';
            }
        }
    }
}

// Import des budgets
else if (!empty($_POST['budgetBtn']))
{
    $tabMsg['budget'] = array('ok' => '', 'ko' => '');
    if (isset($_FILES['budgetFile']) && is_uploaded_file($_FILES['budgetFile']["tmp_name"]))
    {

        $tabData = array();
        if (preg_match('/\.csv$/', $_FILES['budgetFile']['name']))
        {
            if ($fh = fopen($_FILES['budgetFile']["tmp_name"], 'r'))
            {
                $row = 1;
                while (($data = fgetcsv($fh, 1000, ";")) !== FALSE)
                {
                    $num = count($data);
                    if ($num == 12)
                    {
                        $agency = $data[0];
                        $year = $data[1];
                        $month = $data[2];
                        $value1 = preg_replace('/\s/', '', $data[3]);
                        $value1 = preg_replace('/,/', '.', $value1);
                        $value2 = preg_replace('/\s/', '', $data[4]);
                        $value2 = preg_replace('/,/', '.', $value2);
                        $value3 = preg_replace('/\s/', '', $data[5]);
                        $value3 = preg_replace('/,/', '.', $value3);
                        $value4 = preg_replace('/\s/', '', $data[6]);
                        $value4 = preg_replace('/,/', '.', $value4);
                        $value5 = preg_replace('/\s/', '', $data[7]);
                        $value5 = preg_replace('/,/', '.', $value5);
                        $value6 = preg_replace('/\s/', '', $data[8]);
                        $value6 = preg_replace('/,/', '.', $value6);
                        $value7 = preg_replace('/\s/', '', $data[9]);
                        $value7 = preg_replace('/,/', '.', $value7);
                        $value8 = preg_replace('/\s/', '', $data[10]);
                        $value8 = preg_replace('/,/', '.', $value8);
                        $value9 = preg_replace('/\s/', '', $data[11]);
                        $value9 = preg_replace('/,/', '.', $value9);

                        if (empty($value1)) {
                            $value1 = 0;
                        }
                        if (empty($value2)) {
                            $value2 = 0;
                        }
                        if (empty($value3)) {
                            $value3 = 0;
                        }
                        if (empty($value4)) {
                            $value4 = 0;
                        }
                        if (empty($value5)) {
                            $value5 = 0;
                        }
                        if (empty($value6)) {
                            $value6 = 0;
                        }
                        if (empty($value7)) {
                            $value7 = 0;
                        }
                        if (empty($value8)) {
                            $value8 = 0;
                        }
                        if (empty($value9)) {
                            $value9 = 0;
                        }

                        if (!empty($agency) && !is_numeric($agency) && strlen($agency) == 3 &&
                            is_numeric($year) && ($year / 2000) >= 1 &&
                            is_numeric($month) && $month >= 1  && $month <= 12 &&
                            is_numeric($value1) && is_numeric($value2) && is_numeric($value3) &&
                            is_numeric($value4) && is_numeric($value5) && is_numeric($value6) &&
                            is_numeric($value7) && is_numeric($value8) && is_numeric($value9))
                        {
                            $tabData['budgetEqt'][] = array(
                                                         'agency' => $agency,
                                                         'year' => $year,
                                                         'month' => $month,
                                                         'value' => $value1
                                                        );

                            $tabData['budgetSafety'][] = array(
                                                         'agency' => $agency,
                                                         'year' => $year,
                                                         'month' => $month,
                                                         'value' => $value2
                                                         );

                            $tabData['budgetPrestaParkLogi'][] = array(
                                                        'agency' => $agency,
                                                        'year' => $year,
                                                        'month' => $month,
                                                        'value' => $value3
                                                        );
                            $tabData['budgetTransp'][] = array(
                                                         'agency' => $agency,
                                                         'year' => $year,
                                                         'month' => $month,
                                                         'value' => $value4
                                                        );

                            $tabData['budgetCarbGaz'][] = array(
                                                         'agency' => $agency,
                                                         'year' => $year,
                                                         'month' => $month,
                                                         'value' => $value5
                                                         );

                            $tabData['budgetPrestaAg'][] = array(
                                                        'agency' => $agency,
                                                        'year' => $year,
                                                        'month' => $month,
                                                        'value' => $value6
                                                        );
                            $tabData['budgetVGP'][] = array(
                                                         'agency' => $agency,
                                                         'year' => $year,
                                                         'month' => $month,
                                                         'value' => $value7
                                                        );

                            $tabData['budgetSpareParts'][] = array(
                                                         'agency' => $agency,
                                                         'year' => $year,
                                                         'month' => $month,
                                                         'value' => $value8
                                                         );

                            $tabData['budgetPrestaParkMach'][] = array(
                                                        'agency' => $agency,
                                                        'year' => $year,
                                                        'month' => $month,
                                                        'value' => $value9
                                                        );

                        }
                        else if ($row > 1)
                        {
                            $tabMsg['budget']['ko'] = 'Le fichier comporte des erreurs, en ligne '.$row.' : mauvais format des donn�es';
                            break;
                        }
                    }
                    else
                    {
                        $tabMsg['budget']['ko'] = 'Le fichier doit comporter 12 colonnes ('.$num.' en ligne '.$row.').';
                        break;
                    }
                    $row++;
                }
                fclose($fh);
            }
            else {
                $tabMsg['budget']['ko'] = 'Impossible d\'ouvrir le fichier';
            }
        }
        else {
            $tabMsg['budget']['ko'] = 'Le fichier n\'a pas la bonne extension';
        }

        if (empty($tabMsg['budget']['ko']) && !empty($tabData))
        {
            foreach ($tabData as $indicator => $values)
            {
                if (!importDb($indicator, $values)) {
                    $tabMsg['budget']['ko'] = 'Une erreur SQL s\'est produite, aucune ligne ne sera import�e';
                    break;
                }
            }
            if (empty($tabMsg['budget']['ko'])) {
                $tabMsg['budget']['ok'] = 'Le fichier a �t� correctement import� ('.($row - 1).' lignes trait�es)';
            }
        }
    }
    else
    {
    	$tabMsg['budget']['ko'] = 'test';
    }
}
//import nombre colis
else if (!empty($_POST['colisBtn']))
{
    $tabMsg['colis'] = array('ok' => '', 'ko' => '');
    if (isset($_FILES['colisFile']) && is_uploaded_file($_FILES['colisFile']["tmp_name"]))
    {
        $tabData = array();
        if (preg_match('/\.csv$/', $_FILES['colisFile']['name']))
        {
            if ($fh = fopen($_FILES['colisFile']["tmp_name"], 'r'))
            {
                $row = 1;
                while (($data = fgetcsv($fh, 1000, ";")) !== FALSE)
                {
                    $num = count($data);
                    if ($num == 6)
                    {
                        $agency = $data[0];
                        $type = $data[1];
                        $year = $data[2];
                        $month = $data[3];
                        $value1 = preg_replace('/\s/', '', $data[4]);
                        $value1 = preg_replace('/,/', '.', $value1);
                        $value2 = preg_replace('/\s/', '', $data[5]);
                        $value2 = preg_replace('/,/', '.', $value2);
                     if (empty($value1)) {
                            $value1 = 0;
                        }
                        if (empty($value2)) {
                            $value1 = 0;
                        }

                        if (!empty($agency) && !is_numeric($agency) && strlen($agency) == 3 &&
                            ($type == 'express' || $type == 'messagerie') &&
                            is_numeric($year) && ($year / 2000) >= 1 &&
                            is_numeric($month) && $month >= 1  && $month <= 12 &&
                            is_numeric($value1) && is_numeric($value2))
                        {
                            if ($type == 'express') {
                                $type = 'exp';
                            }
                            else if ($type == 'messagerie') {
                                $type = 'msg';
                            }
                            $tabData['nbcolis'.$type][] = array(
                                'agency' => $agency,
                                'year' => $year,
                                'month' => $month,
                                'value' => $value1);
                            $tabData['amtcolis'.$type][] = array(
                                'agency' => $agency,
                                'year' => $year,
                                'month' => $month,
                                'value' => $value2);
                        }
                        else if ($row > 1)
                        {
                            $tabMsg['colis']['ko'] = 'Le fichier comporte des erreurs, en ligne '.$row.' : mauvais format des donn�es';
                            break;
                        }
                    }
                    else
                    {
                        $tabMsg['colis']['ko'] = 'Le fichier doit comporter 6 colonnes ('.$num.' en ligne '.$row.').';
                        break;
                    }
                    $row++;
                }
                fclose($fh);
            }
            else {
                $tabMsg['colis']['ko'] = 'Impossible d\'ouvrir le fichier';
            }
        }
        else {
            $tabMsg['colis']['ko'] = 'Le fichier n\'a pas la bonne extension';
        }

        if (empty($tabMsg['colis']['ko']) && !empty($tabData))
        {
            foreach ($tabData as $indicator => $values)
            {
                if (!importDb($indicator, $values)) {
                    $tabMsg['colis']['ko'] = 'Une erreur SQL s\'est produite, aucune ligne ne sera import�e';
                    break;
                }
            }
            if (empty($tabMsg['colis']['ko'])) {
                $tabMsg['colis']['ok'] = 'Le fichier a �t� correctement import� ('.($row - 1).' lignes trait�es)';
            }
        }
    }
}
// Import du nombre de machines
else if (!empty($_POST['cntMacBtn']))
{
    $tabMsg['cntMac'] = array('ok' => '', 'ko' => '');
    if (isset($_FILES['cntMacFile']) && is_uploaded_file($_FILES['cntMacFile']["tmp_name"]))
    {
        $tabData = array();
        if (preg_match('/\.csv$/', $_FILES['cntMacFile']['name']))
        {
            if ($fh = fopen($_FILES['cntMacFile']["tmp_name"], 'r'))
            {
                $row = 1;
                while (($data = fgetcsv($fh, 1000, ";")) !== FALSE)
                {
                    $num = count($data);
                    if ($num == 4)
                    {
                        $agency = $data[0];
                        $year = $data[1];
                        $month = $data[2];
                        $value = preg_replace('/\s/', '', $data[3]);
                        $value = preg_replace('/,/', '.', $value);
                        if (empty($value)) {
                            $value = 0;
                        }

                        if (!empty($agency) && !is_numeric($agency) && strlen($agency) == 3 &&
                            is_numeric($year) && ($year / 2000) >= 1 &&
                            is_numeric($month) && $month >= 1  && $month <= 12 &&
                            is_numeric($value))
                        {
                            $tabData[] = array(
                                'agency' => $agency,
                                'year' => $year,
                                'month' => $month,
                                'value' => $value);
                        }
                        else if ($row > 1)
                        {
                            $tabMsg['cntMac']['ko'] = 'Le fichier comporte des erreurs, en ligne '.$row.' : mauvais format des donn�es';
                            break;
                        }
                    }
                    else
                    {
                        $tabMsg['cntMac']['ko'] = 'Le fichier doit comporter 4 colonnes ('.$num.' en ligne '.$row.').';
                        break;
                    }
                    $row++;
                }
                fclose($fh);
            }
            else {
                $tabMsg['cntMac']['ko'] = 'Impossible d\'ouvrir le fichier';
            }
        }
        else {
            $tabMsg['cntMac']['ko'] = 'Le fichier n\'a pas la bonne extension';
        }

        if (empty($tabMsg['cntMac']['ko']) && !empty($tabData))
        {
            if (importDb('cntmachine', $tabData)) {
                $tabMsg['cntMac']['ok'] = 'Le fichier a �t� correctement import� ('.($row - 1).' lignes trait�es)';
            }
            else {
                $tabMsg['cntMac']['ko'] = 'Une erreur SQL s\'est produite, aucune ligne import�e';
            }
        }
    }
}
// Import des Effectifs Atelier
else if (!empty($_POST['effAtlBtn']))
{
    $tabMsg['effAtl'] = array('ok' => '', 'ko' => '');
    if (isset($_FILES['effAtlFile']) && is_uploaded_file($_FILES['effAtlFile']["tmp_name"]))
    {
        $tabData = array();
        if (preg_match('/\.csv$/', $_FILES['effAtlFile']['name']))
        {
            if ($fh = fopen($_FILES['effAtlFile']["tmp_name"], 'r'))
            {
                $row = 1;
                while (($data = fgetcsv($fh, 1000, ";")) !== FALSE)
                {
                    $num = count($data);
                    if ($num == 6)
                    {
                        $agency   = $data[0];
                        $year    = $data[1];
                        $month   = $data[2];
                        $value1 = preg_replace('/\s/', '', $data[3]);
                        $value1 = preg_replace('/,/', '.', $value1);
                        $value2 = preg_replace('/\s/', '', $data[4]);
                        $value2 = preg_replace('/,/', '.', $value2);
                        $value3 = preg_replace('/\s/', '', $data[5]);
                        $value3 = preg_replace('/,/', '.', $value3);

                        if (empty($value1)) {
                            $value1 = 0;
                        }
                        if (empty($value2)) {
                            $value2 = 0;
                        }
                        if (empty($value3)) {
                            $value3 = 0;
                        }

                        if (!empty($agency) && !is_numeric($agency) && strlen($agency) == 3 &&
                            is_numeric($year) && ($year / 2000) >= 1 &&
                            is_numeric($month) && $month >= 1  && $month <= 12 &&
                            is_numeric($value1) && is_numeric($value2) && is_numeric($value3))
                        {
                            $tabData['effAtl'][] = array(
                                                         'agency' => $agency,
                                                         'year' => $year,
                                                         'month' => $month,
                                                         'value' => $value1
                                                        );

                            $tabData['effApp'][] = array(
                                                         'agency' => $agency,
                                                         'year' => $year,
                                                         'month' => $month,
                                                         'value' => $value2
                                                         );

                            $tabData['effIti'][] = array(
                                                        'agency' => $agency,
                                                        'year' => $year,
                                                        'month' => $month,
                                                        'value' => $value3
                                                        );

                        }
                        else if ($row > 1)
                        {
                            $tabMsg['effAtl']['ko'] = 'Le fichier comporte des erreurs, en ligne '.$row.' : mauvais format des donn�es';
                            break;
                        }
                    }
                    else
                    {
                        $tabMsg['effAtl']['ko'] = 'Le fichier doit comporter 6 colonnes ('.$num.' en ligne '.$row.').';
                        break;
                    }
                    $row++;
                }
                fclose($fh);
            }
            else {
                $tabMsg['effAtl']['ko'] = 'Impossible d\'ouvrir le fichier';
            }
        }
        else {
            $tabMsg['effAtl']['ko'] = 'Le fichier n\'a pas la bonne extension';
        }

        if (empty($tabMsg['effAtl']['ko']) && !empty($tabData))
        {
            foreach ($tabData as $indicator => $values)
            {
                if (!importDb($indicator, $values)) {
                    $tabMsg['effAtl']['ko'] = 'Une erreur SQL s\'est produite, aucune ligne ne sera import�e';
                    break;
                }
            }
            if (empty($tabMsg['effAtl']['ko'])) {
                $tabMsg['effAtl']['ok'] = 'Le fichier a �t� correctement import� ('.($row - 1).' lignes trait�es)';
            }
        }
    }
}
// Import des montants de garanties constructeurs
else if (!empty($_POST['garantieBtn']))
{
    $tabMsg['garantie'] = array('ok' => '', 'ko' => '');
    if (isset($_FILES['garantieFile']) && is_uploaded_file($_FILES['garantieFile']["tmp_name"]))
    {
        $tabData = array();
        if (preg_match('/\.csv$/', $_FILES['garantieFile']['name']))
        {
            if ($fh = fopen($_FILES['garantieFile']["tmp_name"], 'r'))
            {
                $row = 1;
                while (($data = fgetcsv($fh, 1000, ";")) !== FALSE)
                {
                    $num = count($data);
                    if ($num == 4)
                    {
                        $agency = $data[0];
                        $year = $data[1];
                        $month = $data[2];
                        $value = preg_replace('/\s/', '', $data[3]);
                        $value = preg_replace('/,/', '.', $value);
                        if (empty($value)) {
                            $value = 0;
                        }

                        if (!empty($agency) && !is_numeric($agency) && strlen($agency) == 3 &&
                            is_numeric($year) && ($year / 2000) >= 1 &&
                            is_numeric($month) && $month >= 1  && $month <= 12 &&
                            is_numeric($value))
                        {
                            $tabData[] = array(
                                'agency' => $agency,
                                'year' => $year,
                                'month' => $month,
                                'value' => $value);
                        }
                        else if ($row > 1)
                        {
                            $tabMsg['garantie']['ko'] = 'Le fichier comporte des erreurs, en ligne '.$row.' : mauvais format des donn�es';
                            break;
                        }
                    }
                    else
                    {
                        $tabMsg['garantie']['ko'] = 'Le fichier doit comporter 4 colonnes ('.$num.' en ligne '.$row.').';
                        break;
                    }
                    $row++;
                }
                fclose($fh);
            }
            else {
                $tabMsg['garantie']['ko'] = 'Impossible d\'ouvrir le fichier';
            }
        }
        else {
            $tabMsg['garantie']['ko'] = 'Le fichier n\'a pas la bonne extension';
        }

        if (empty($tabMsg['garantie']['ko']) && !empty($tabData))
        {
            if (importDb('amtGuarantee', $tabData)) {
                $tabMsg['garantie']['ok'] = 'Le fichier a �t� correctement import� ('.($row - 1).' lignes trait�es)';
            }
            else {
                $tabMsg['garantie']['ko'] = 'Une erreur SQL s\'est produite, aucune ligne import�e';
            }
        }
    }
}
// Import des montant inventaires
else if (!empty($_POST['inventaireBtn']))
{
    $tabMsg['inventaire'] = array('ok' => '', 'ko' => '');
    if (isset($_FILES['inventaireFile']) && is_uploaded_file($_FILES['inventaireFile']["tmp_name"]))
    {
        $tabData = array();
        if (preg_match('/\.csv$/', $_FILES['inventaireFile']['name']))
        {
            if ($fh = fopen($_FILES['inventaireFile']["tmp_name"], 'r'))
            {
                $row = 1;
                while (($data = fgetcsv($fh, 1000, ";")) !== FALSE)
                {
                    $num = count($data);
                    if ($num == 4)
                    {
                        $agency = $data[0];
                        $year = $data[1];
                        $month = $data[2];
                        $value = preg_replace('/\s/', '', $data[3]);
                        $value = preg_replace('/,/', '.', $value);
                        if (empty($value)) {
                            $value = 0;
                        }

                        if (!empty($agency) && !is_numeric($agency) && strlen($agency) == 3 &&
                            is_numeric($year) && ($year / 2000) >= 1 &&
                            is_numeric($month) && $month >= 1  && $month <= 12 &&
                            is_numeric($value))
                        {
                            $tabData[] = array(
                                'agency' => $agency,
                                'year' => $year,
                                'month' => $month,
                                'value' => $value);
                        }
                        else if ($row > 1)
                        {
                            $tabMsg['inventaire']['ko'] = 'Le fichier comporte des erreurs, en ligne '.$row.' : mauvais format des donn�es';
                            break;
                        }
                    }
                    else
                    {
                        $tabMsg['inventaire']['ko'] = 'Le fichier doit comporter 4 colonnes ('.$num.' en ligne '.$row.').';
                        break;
                    }
                    $row++;
                }
                fclose($fh);
            }
            else {
                $tabMsg['inventaire']['ko'] = 'Impossible d\'ouvrir le fichier';
            }
        }
        else {
            $tabMsg['inventaire']['ko'] = 'Le fichier n\'a pas la bonne extension';
        }

        if (empty($tabMsg['inventaire']['ko']) && !empty($tabData))
        {
            if (importDb('amtInventory', $tabData)) {
                $tabMsg['inventaire']['ok'] = 'Le fichier a �t� correctement import� ('.($row - 1).' lignes trait�es)';
            }
            else {
                $tabMsg['inventaire']['ko'] = 'Une erreur SQL s\'est produite, aucune ligne import�e';
            }
        }
    }
}



function importDb($indicator, $data)
{
    global $varglob;

    $cfg = $varglob->SERVER[$varglob->CONNECTION['statistics']['serverid']];
    $db = mysql_connect($cfg['host'], $cfg['configs']['username'], $cfg['configs']['password']);
    mysql_select_db($varglob->CONNECTION['statistics']['dbname'], $db);
    //mysql_set_charset($varglob->CONNECTION['statistics']['charset']);

    $qry = "
        DELETE FROM i_sav_indicator
        WHERE ind_name = '$indicator' AND
            ind_is_imported = 0";
    mysql_query($qry, $db);

    $err = false;
    if (!empty($data))
    {
        foreach ($data as $values)
        {
            $agc_id = $values['agency'];
            $year = $values['year'];
            $month = $values['month'];
            $value = $values['value'];

            $qry = "
                INSERT INTO i_sav_indicator
                    (ind_agc_id, ind_year, ind_month, ind_name, ind_value, ind_date_submitted)
                VALUES
                    ('$agc_id', $year, $month, '$indicator', $value, NOW())";
            if (mysql_query($qry, $db))
            {
                $importedRows[] = mysql_insert_id();
            }
            else
            {
                $err = true;
                break;
            }
        }

        if ($err && !empty($importedRows))
        {
            $qry = "DELETE FROM i_sav_indicator WHERE ind_id IN (".implode(', ', $importedRows).")";
            mysql_query($qry, $db);
        }
    }

    mysql_close($db);

    return (!$err);
}

$faviconsUrl = $varglob->pweb . '/web/img/icons/' . ($varglob->ENV == '' ? '' : 'test/');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
<title>Import de donn�es chiffres cl�s</title>
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $faviconsUrl; ?>LOC_favicon_16x16.png" />
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $faviconsUrl; ?>LOC_favicon_32x32.png" />
<link rel="icon" type="image/png" sizes="128x128" href="<?php echo $faviconsUrl; ?>LOC_favicon_128x128.png" />
<link rel="apple-touch-icon" href="<?php echo $faviconsUrl; ?>LOC_favicon_152x152_precomposed.png" />
<META http-equiv="Content-Type" Content="text/html; charset=ISO-8859-1">
<meta http-equiv="Expires" content="no-cache"/>
<link rel="stylesheet" type="text/css" href="<?=$varglob->pweb.'/c/base.css'?>">
<style>
    fieldset
    {
        border: 1px solid black;
    }

    legend
    {
        font-size: 20px;
        font-weight: bold;
    }

    div.format
    {
        display: none;
        width:450px;
        margin-top: 10px;
        padding: 5px;
        color:green;
    }

    p.msgKo
    {
        font-weight: bold;
        font-style: italic;
        color: red;
    }

    p.msgOk
    {
        font-weight: normal;
        font-style: italic;
        color: blue;
    }
</style>
<SCRIPT Language="Javascript">
    function showHide(id)
    {
        if (document.getElementById(id).style.display == 'block') {
            document.getElementById(id).style.display = 'none';
        }
        else {
            document.getElementById(id).style.display = 'block';
        }
    }
</SCRIPT>
</head>

<body>

<TABLE bgcolor="C6CDC5" border="0" cellspacing="0" cellpadding="0" width="100%">
	<TR>
		<TD align="left" width="27" style="padding-left: 5px"><IMG src="<?=$varglob->pweb.'/i/minilogo.svg'?>" alt=""></TD>
		<TD align="left" style="padding-left: 10px"><FONT class="TITRE">Chiffres cl�s du SAV</FONT></TD>
        <TD align="right"><SPAN id="tps_exec"></SPAN></TD>
		<TD width="54"><a href="javascript:history.back(1)"><IMG src="<?=$varglob->pweb.'/i/img_2.gif'?>" align="MIDDLE" border="0" width="54" height="36" alt="Retour Arri�re"></a>

		<SCRIPT Language="Javascript">
		function printit()
		{
			window.print() ;
		}

		var NS = (navigator.appName == "Netscape");
		var VERSION = parseInt(navigator.appVersion);

		if (VERSION > 3)
		{
			document.write('</td><td width="54"><a href="javascript:printit()"><img src="<?=$varglob->pweb."/i/img_3.gif"?>" align="MIDDLE" border="0" width="54" height="36" alt="Imprimer"></a>');
		}
		</SCRIPT>

		</TD>
		<TD width="48"><a href="javascript:window.close()"><IMG src="<?=$varglob->pweb.'/i/img_4.gif'?>" align="MIDDLE" border="0" width="48" height="36" alt="Fermer"></a></TD>
	</TR>
</TABLE>

<br />

<form method="POST" action="" enctype="multipart/form-data">
<fieldset>
    <legend>Importer le fichier des CA SAV</legend><br />
    <a href="#" onclick="showHide('divCaSav');">Cliquer ici pour voir le format du fichier</a>
    <div id="divCaSav" class="format">
        Fichier csv avec colonnes s�par�es par un point-virgule dans l'ordre suivant :
        <ol>
            <li>Code agence sur trois lettres
            <li>Ann�e sur 4 chiffres
            <li>Mois num�rique sur 1 ou 2 chiffres
            <li>Montant
        </ol>
    </div>
    <p>
        <input name="caSavFile" type="file" size="50" maxlength="100000" accept="text/*"> &nbsp;
        <input name="caSavBtn" type="submit" value="Go!">
    </p>
    <p class="msgKo">
        <?=(isset($tabMsg['caSav']['ko']) && !empty($tabMsg['caSav']['ko']) ? $tabMsg['caSav']['ko'] : '')?>
    </p>
    <p class="msgOk">
        <?=(isset($tabMsg['caSav']['ok']) && !empty($tabMsg['caSav']['ok']) ? $tabMsg['caSav']['ok'] : '')?>
    </p>
</fieldset>
<br />
<fieldset>
    <legend>Importer le fichier des VGP</legend><br />
    <a href="#" onclick="showHide('divVgp');">Cliquer ici pour voir le format du fichier</a>
    <div id="divVgp" class="format">
        Fichier csv avec colonnes s�par�es par un point-virgule dans l'ordre suivant :
        <ol>
            <li>Code agence sur trois lettres
            <li>Type de contr�le: CHANTIER ou AGENCE
            <li>Ann�e sur 4 chiffres
            <li>Mois num�rique sur 1 ou 2 chiffres
            <li>Nombre de contr�les
            <li>Somme totale des montants
        </ol>
    </div>
    <p>
        <input name="vgpFile" type="file" size="50" maxlength="100000" accept="text/*"> &nbsp;
        <input name="vgpBtn" type="submit" value="Go!">
    </p>
    <p class="msgKo">
        <?=(isset($tabMsg['vgp']['ko']) && !empty($tabMsg['vgp']['ko']) ? $tabMsg['vgp']['ko'] : '')?>
    </p>
    <p class="msgOk">
        <?=(isset($tabMsg['vgp']['ok']) && !empty($tabMsg['vgp']['ok']) ? $tabMsg['vgp']['ok'] : '')?>
    </p>
</fieldset>
<br />
<fieldset>
    <legend>Importer le fichier du nombre de machines par agence</legend><br />
    <a href="#" onclick="showHide('divCntMac');">Cliquer ici pour voir le format du fichier</a>
    <div id="divCntMac" class="format">
        Fichier csv avec colonnes s�par�es par un point-virgule dans l'ordre suivant :
        <ol>
            <li>Code agence sur trois lettres
            <li>Ann�e sur 4 chiffres
            <li>Mois num�rique sur 1 ou 2 chiffres
            <li>Nombre de machines
        </ol>
    </div>
    <p>
        <input name="cntMacFile" type="file" size="50" maxlength="100000" accept="text/*"> &nbsp;
        <input name="cntMacBtn" type="submit" value="Go!">
    </p>
    <p class="msgKo">
        <?=(isset($tabMsg['cntMac']['ko']) && !empty($tabMsg['cntMac']['ko']) ? $tabMsg['cntMac']['ko'] : '')?>
    </p>
    <p class="msgOk">
        <?=(isset($tabMsg['cntMac']['ok']) && !empty($tabMsg['cntMac']['ok']) ? $tabMsg['cntMac']['ok'] : '')?>
    </p>
</fieldset>
<br/>
<fieldset>
    <legend>Importer le fichier des effectifs atelier</legend><br />
    <a href="#" onclick="showHide('divEffAtl');">Cliquer ici pour voir le format du fichier</a>
    <div id="divEffAtl" class="format">
        Fichier csv avec colonnes s�par�es par un point-virgule dans l'ordre suivant :
        <ol>
            <li>Code agence sur trois lettres
            <li>Ann�e sur 4 chiffres
            <li>Mois num�rique sur 1 ou 2 chiffres
            <li>Effectif
            <li>Apprenti
            <li>Itin�rant
        </ol>
    </div>
    <p>
        <input name="effAtlFile" type="file" size="50" maxlength="100000" accept="text/*"> &nbsp;
        <input name="effAtlBtn" type="submit" value="Go!">
    </p>
    <p class="msgKo">
        <?=(isset($tabMsg['effAtl']['ko']) && !empty($tabMsg['effAtl']['ko']) ? $tabMsg['effAtl']['ko'] : '')?>
    </p>
    <p class="msgOk">
        <?=(isset($tabMsg['effAtl']['ok']) && !empty($tabMsg['effAtl']['ok']) ? $tabMsg['effAtl']['ok'] : '')?>
    </p>
</fieldset>
<br />
<fieldset>
    <legend>Importer le fichier des colis</legend><br />
    <a href="#" onclick="showHide('divColis');">Cliquer ici pour voir le format du fichier</a>
    <div id="divColis" class="format">
        Fichier csv avec colonnes s�par�es par un point-virgule dans l'ordre suivant :
        <ol>
            <li>Code agence sur trois lettres
            <li>Type d'envoi : express ou messagerie
            <li>Ann�e sur 4 chiffres
            <li>Mois num�rique sur 1 ou 2 chiffres
            <li>Nombre d'envois
            <li>Somme totale des envois
        </ol>
    </div>
    <p>
        <input name="colisFile" type="file" size="50" maxlength="100000" accept="text/*"> &nbsp;
        <input name="colisBtn" type="submit" value="Go!">
    </p>
    <p class="msgKo">
        <?=(isset($tabMsg['colis']['ko']) && !empty($tabMsg['colis']['ko']) ? $tabMsg['colis']['ko'] : '')?>
    </p>
    <p class="msgOk">
        <?=(isset($tabMsg['colis']['ok']) && !empty($tabMsg['colis']['ok']) ? $tabMsg['colis']['ok'] : '')?>
    </p>
</fieldset>
<br />
<fieldset>
    <legend>Importer le fichier des garanties constructeurs</legend><br />
    <a href="#" onclick="showHide('divGarantie');">Cliquer ici pour voir le format du fichier</a>
    <div id="divGarantie" class="format">
        Fichier csv avec colonnes s�par�es par un point-virgule dans l'ordre suivant :
        <ol>
            <li>Code agence sur trois lettres
            <li>Ann�e sur 4 chiffres
            <li>Mois num�rique sur 1 ou 2 chiffres
            <li>Montant
        </ol>
    </div>
    <p>
        <input name="garantieFile" type="file" size="50" maxlength="100000" accept="text/*"> &nbsp;
        <input name="garantieBtn" type="submit" value="Go!">
    </p>
    <p class="msgKo">
        <?=(isset($tabMsg['garantie']['ko']) && !empty($tabMsg['garantie']['ko']) ? $tabMsg['garantie']['ko'] : '')?>
    </p>
    <p class="msgOk">
        <?=(isset($tabMsg['garantie']['ok']) && !empty($tabMsg['garantie']['ok']) ? $tabMsg['garantie']['ok'] : '')?>
    </p>
</fieldset>
<br />
<fieldset>
    <legend>Importer le fichier de l'inventaire</legend><br />
    <a href="#" onclick="showHide('divInventaire');">Cliquer ici pour voir le format du fichier</a>
    <div id="divInventaire" class="format">
        Fichier csv avec colonnes s�par�es par un point-virgule dans l'ordre suivant :
        <ol>
            <li>Code agence sur trois lettres
            <li>Ann�e sur 4 chiffres
            <li>Mois num�rique sur 1 ou 2 chiffres
            <li>Montant
        </ol>
    </div>
    <p>
        <input name="inventaireFile" type="file" size="50" maxlength="100000" accept="text/*"> &nbsp;
        <input name="inventaireBtn" type="submit" value="Go!">
    </p>
    <p class="msgKo">
        <?=(isset($tabMsg['inventaire']['ko']) && !empty($tabMsg['inventaire']['ko']) ? $tabMsg['inventaire']['ko'] : '')?>
    </p>
    <p class="msgOk">
        <?=(isset($tabMsg['inventaire']['ok']) && !empty($tabMsg['inventaire']['ok']) ? $tabMsg['inventaire']['ok'] : '')?>
    </p>
</fieldset>
<br />
<fieldset>
    <legend>Importer le fichier des budgets</legend><br />
    <a href="#" onclick="showHide('divBudget');">Cliquer ici pour voir le format du fichier</a>
    <div id="divBudget" class="format">
        Fichier csv avec colonnes s�par�es par un point-virgule dans l'ordre suivant :
        <ol>
            <li>Code agence sur trois lettres
            <li>Ann�e sur 4 chiffres
            <li>Mois num�rique sur 1 ou 2 chiffres
            <li>Budget outillage
            <li>Budget s�curit�
            <li>Budget prestation logistique
            <li>Budget transport
            <li>Budget carburant/gaz
            <li>Budget prestation agence
            <li>Budget VGP
            <li>Budget pi�ces d�tach�es
            <li>Budget prestation machine
        </ol>
    </div>
    <p>
        <input name="budgetFile" type="file" size="50" maxlength="100000" accept="text/*"> &nbsp;
        <input name="budgetBtn" type="submit" value="Go!">
    </p>
    <p class="msgKo">
        <?=(isset($tabMsg['budget']['ko']) && !empty($tabMsg['budget']['ko']) ? $tabMsg['budget']['ko'] : '')?>
    </p>
    <p class="msgOk">
        <?=(isset($tabMsg['budget']['ok']) && !empty($tabMsg['budget']['ok']) ? $tabMsg['budget']['ok'] : '')?>
    </p>
</fieldset>
</form>

</body>
</html>
