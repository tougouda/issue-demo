var remote = null;

function rs(n,u,w,h)
{
    remote = window.open('', n, 'width=' + w + ',height=' + h +',resizable=yes,scrollbars=yes');
    if (remote)
    {
        remote.document.location = u;
        remote.focus();
        return false;
    }
}

function fullrs(n,u)
{
    remote = window.open(u, n);
    if (remote)
    {
        remote.focus();
        return false;
    }
}
 
function taille(tv,t) {
	var tl=eval(tv.length);
	if (tl < t) {
		alert('Il faut saisir au moins ' + t + ' caracteres ');
		return 0;
	}
	return 1;
}

function nombre(tv) {
	if (isNaN(tv)) alert('Veuillez saisir des nombres');
}

function nombremyst(tv) {
	if (isNaN(tv)) {
		alert('Veuillez saisir des nombres');
	}
}

function focus_vide () {
	var i=0;
	while (eval("document.forms[i]")) {
		form_cour = "document.forms[i]";
		var j=0;
		while (eval(form_cour+".elements[j]")) {
			champ = form_cour+".elements[j]";
			if ((!eval(champ+".value"))&&(eval(champ+".type")!="select-one")&&(eval(champ+".type")!="hidden")) {
				eval(champ).focus()
				return;
			}
			j++;
		}
		i++;
	}
}

function jours_ouvrables(jour1, mois1, annee1, jour2, mois2, annee2, feries) {
	// Récupération des jours fériés
	for (i=0; i<feries.length; i++) {
		feries[i] = new Date(feries[i]);
	}
	
	// Dates de début et de fin
	var date1 = new Date(annee1 + '/' + mois1 + '/' + jour1);
	var date2 = new Date(annee2 + '/' + mois2 + '/' + jour2);
	
	// Dates non valides
	if (isNaN(date1) || isNaN(date2)) {
		return '';
	}
	
	var result = (date2 - date1
						- (date2.getTimezoneOffset()
							- date1.getTimezoneOffset())
						* 60 * 1000)
					/ (1000 * 60 * 60 * 24) + 1;
	
	// Déduction des jours f�ri�s
	for (i=0; i<feries.length; i++) {
		if (date1 <= feries[i] && feries[i] <= date2
			&& feries[i].getDay() != 6 && feries[i].getDay() != 0) {
			result--;
		}
	}
	
	// Déduction des week-ends
	date_courante = new Date(date1);
	while (date2-date_courante >= 0) {
		if (date_courante.getDay() == 6 || date_courante.getDay() == 0) {
			result--;
		}
		date_courante.setDate(date_courante.getDate() + 1);
	}
	
	// La date de fin est un samedi
	if (date2.getDay() == 6) {
		result++;
	}
	
	// La date de fin est un dimanche
	if (date2.getDay() == 0) {
		if (date2-date1 != 0) {
			result += 2;
		}
		else {
			result++;
		}
	}
	
	// Ajout des jours plus et soustraction des jours moins
	if (frm.plus != undefined) {
		jours_plus = parseFloat(frm.plus.value);
		if (!isNaN(jours_plus)) {
			result += jours_plus;
		}
	}
	if (frm.moins != undefined) {
		jours_moins = parseFloat(frm.moins.value);
		if (!isNaN(jours_moins)) {
			result -= jours_moins;
		}
	}
	
	// Résultat
	if (result <= 0) {
		return '';
	}
	if (result < 2) {
		return result + ' jour';
	}
	return result + ' jours';
}

function toEuro(val,taux)
{
	val2 =  Math.round(parseFloat(val.replace(/\ /g, ''))*taux*100)/100;
	if (isNaN(val2))
 	{
 		val2 = parseFloat(0.00);
 	}
 	return val2;
}

function dblclick() {
	window.scrollTo(0,0)
}

function couleur(coul1,coul2) {
	if (coul1 == '#64c5ff' || coul1 == coul2) {
		return '#94a5c4';
	} else {
		return coul2;
	}
}

function persist_couleur(coul,nb,coul2) {
	if (coul != '#94a5c4') {
		if (nb == 0) {
			return "#64c5ff";
		} else {
			return coul2;
		}
	}
}

/*
 * Affichage de la gestion de recours suivant les types d'assurance
 * 
 * array tabInsuranceTypes  - Tableau des types d'assurances
 * int   oldInsuranceTypeId - Type d'assurance actuel
 * int   newInsuranceTypeId - Type d'assurance souhaité
 */
function displayAppeal(tabInsuranceTypes, oldInsuranceTypeId, newInsuranceTypeId)
{
    var tabOldInsuranceTypeInfos = tabInsuranceTypes.find(function(item){
        return (item.id == oldInsuranceTypeId);
    });

    var tabNewInsuranceTypeInfos = tabInsuranceTypes.find(function(item){
        return (item.id == newInsuranceTypeId);
    });

    var display = (tabNewInsuranceTypeInfos && tabNewInsuranceTypeInfos.isAppealActivated || !tabNewInsuranceTypeInfos && tabOldInsuranceTypeInfos.isAppealActivated ? "" : "none");

    var trAppeal = document.getElementsByClassName("appeal");
    for (var i = 0; i < trAppeal.length; i++)
    {
        trAppeal[i].style.display = display;
    }
}
