var layers = false;
var ie4 = (document.all) ? true : false;
var ns6 = (document.getElementById&&!document.all) ? true : false;
if (ie4 || ns6) {layers = true;}

function test_f5(e, pays) {
    var val = "";
    var message = Array("Merci de ne pas appuyer sur la touche F5", "Gracias de no apoyar en la tecla F5", "Obrigado n�o apoiar sobre o toque F5","Vi preghiamo di non premere il tasto F5");
    var mess = "";
    if (pays == "FR" || pays == "MA") {
        mess = message[0];
    }
    else if (pays == "ES") {
        mess = message[1];
    }
    else if (pays == "PT") {
        mess = message[2];
    }
    else if (pays == "IT") {
        mess = message[3];
    }
    if(ie4) {
        val = "K" + e.keyCode;
        if (val == "K116") {
            alert(mess);
            e.keyCode = 0;
            return false;
        }
        else {
            return true;
        }
    }
    if (ns6) {
        val = "K" + e.which;
        if (val == "K116") {
            alert(mess);
            return false;
        }
        else {
            return true;
        }
    }
}
