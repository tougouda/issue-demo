/**
 * $Revision: 3111 $
 * $Date: 2009-12-08 09:36:22 +0100 (mar., 08 déc. 2009) $
 * $Author: jodesous $
 */

if (!window.Location)
{
    var Location = new Object();
}

if (!Location.commonObj) alert('Erreur Location');


/* Classe Location.Notification */
Location.Notification = function(pluginObj, id, type, message, tabButtons, tabOptions, tabSubMessages)
{
    this._id = id;

    this._isClosedByUser = false;

    this._pluginObj = pluginObj;

    this._el = null;
    this._msgEl = null;
    this._btnsEl = null;

    this._fadeTimer = null;

    this._type = type || 'info';
    this._message = message || '';
    this._tabSubMessages = tabSubMessages || [];
    this._tabButtons = tabButtons || [];
    this._tabOptions = tabOptions || {};

    this.ondisplaychange = null;
}

Location.Notification.prototype = {


        /**
         * Remplacer les variables dans un message
         *
         * @param string message Message contenant les variables
         * @param Object tabMessageVars Objet contenant les valeurs des variables sous la forme {'varName' : 'varValue'}
         * @return string Message contenant les variables remplacées
         */
        _replaceMessageVars : function(message, tabMessageVars)
        {
            if (tabMessageVars)
            {
                for (var varName in tabMessageVars)
                {
                    message = message.replace('\<\%' + varName + '\>', tabMessageVars[varName]);
                }
            }

            return message;
        },

        /**
         * Afficher la notification
         *
         * @param Object tabOptions Options ('forceFirst', 'forceLast', 'messageVars', 'disabled')
         * @return Location.Notification Instance courante
         */
        show : function(tabOptions)
        {
            if (this._isClosedByUser)
            {
                return this;
            }

            var mySelf = this;
            var containerEl = null;
            var display = true;
            if (this._el)
            {
                // Récupère le conteneur de la notification
                containerEl = this._el.parentNode;
                // On affiche la notification si la notification n'est pas affichée ou
                // qu'elle doit être mise en première ou en dernière position
                display = (this._el.style.display == 'none' ||
                           tabOptions && (tabOptions.forceFirst || tabOptions.forceLast));
                this._el.style.display = '';
            }
            else
            {
                this._el = window.document.createElement('div');
                this._el.className = 'locPlgNotifElt type_' + this._type +
                                     (typeof this._tabOptions.isClosable == 'undefined' ||
                                      this._tabOptions.isClosable ? ' closable' : '');
                this._el.style.opacity = 0;

                // Icône
                var el = window.document.createElement('div');
                el.className = 'icon';
                this._el.appendChild(el);
                // Boutons
                this._btnsEl = window.document.createElement('div');
                this._btnsEl.className = 'btns';

                // Ajout des boutons
                for (var i = 0; i < this._tabButtons.length; i++)
                {
                    var el = window.document.createElement('button');
                    el.className = 'action';
                    el.setAttribute('data-index', i);
                    el.setAttribute('data-id', this._tabButtons[i].id);

                    // Fonction utilisateur exécutée au moment du clic sur le bouton
                    el.onclick = function()
                    {

                        var result = false;
                        var index = this.getAttribute('data-index');
                        if (mySelf._tabButtons[index].onclick)
                        {
                            switch (typeof mySelf._tabButtons[index].onclick)
                            {
                                case 'string':
                                    var func = new Function(mySelf._tabButtons[index].onclick);
                                    result = func();
                                    break;
                                case 'function':
                                    result = mySelf._tabButtons[index].onclick();
                                    break;
                            }
                        }
                        // Masquage de la notification en cas de succès de la fonction associée au bouton
                        if (result)
                        {
                            mySelf.hide();
                        }
                    }
                    el.appendChild(window.document.createElement('span'));
                    el.firstChild.innerHTML = this._tabButtons[i].title;

                    this._btnsEl.appendChild(el);
                }

                el = window.document.createElement('button');
                el.className = 'close';
                el.onclick = function()
                {
                    mySelf._isClosedByUser = true;
                    mySelf.hide();
                }
                this._btnsEl.appendChild(el);


                this._el.appendChild(this._btnsEl);

                // Message
                this._msgEl = window.document.createElement('div');
                this._msgEl.className = 'msg';
                this._el.appendChild(this._msgEl);

                // Ajout de la notification
                if (this._tabOptions.containerEl)
                {
                    containerEl = Location.commonObj.getElt(this._tabOptions.containerEl);
                }
                else
                {
                    containerEl = this._pluginObj.getDefaultContainer();
                }
            }

            // Messages multiples
            if (this._tabSubMessages && this._tabSubMessages.length > 0 &&
                tabOptions.messagesId && tabOptions.messagesId.length > 0)
            {
                // On demande à afficher un seul des messages
                // On va donc afficher la version "single" du message demandé
                if (tabOptions.messagesId.length == 1)
                {
                    // Recherche du message demandé parmi les messages possibles
                    var msg = '';
                    var nbSubMessages = this._tabSubMessages.length;
                    for (var i = 0; i < nbSubMessages; i++)
                    {
                        if (this._tabSubMessages[i].id == tabOptions.messagesId[0])
                        {
                            msg = this._tabSubMessages[i].single;
                        }
                    }

                    this._msgEl.innerHTML = this._replaceMessageVars(msg, tabOptions.messageVars);
                }
                // On demande à afficher plusieurs des messages
                // On va donc construire une liste à puces avec les messages demandés
                else
                {
                    // Remplacement des templates dans le message standard
                    this._msgEl.innerHTML = this._replaceMessageVars(this._message, tabOptions.messageVars);

                    var nbSubMessages = this._tabSubMessages.length;
                    if (nbSubMessages > 0)
                    {
                        var ulEl = window.document.createElement('ul');
                        this._msgEl.appendChild(ulEl);

                        // Parcours des messages possibles pour trouver ceux que l'on souhaite afficher
                        for (var i = 0; i < nbSubMessages; i++)
                        {
                            if (in_array(this._tabSubMessages[i].id, tabOptions.messagesId))
                            {
                                var liEl = window.document.createElement('li');
                                liEl.innerHTML = this._replaceMessageVars(this._tabSubMessages[i].multiple,
                                                                          tabOptions.messageVars);
                                ulEl.appendChild(liEl);
                            }
                        }
                    }
                }
            }
            // Message simple
            else
            {
                // Remplacement des templates dans le message standard
                this._msgEl.innerHTML = this._replaceMessageVars(this._message, tabOptions.messageVars);
            }

            // Classe CSS appropriée selon qu'il y a des boutons à afficher ou non
            if (this._tabButtons.length > 0 &&
                (typeof tabOptions.buttonsId == 'undefined' ||
                    typeof tabOptions.buttonsId == 'object' && tabOptions.buttonsId.length > 0))
            {
                if (!this._el.className.match(/actionable/))
                {
                    this._el.className += ' actionable';
                }
            }
            else
            {
                this._el.className = this._el.className.replace(/ ?actionable/, '');
            }
            // Affichage ou masquage des boutons
            for (var i = 0; i < this._tabButtons.length; i++)
            {
                // On affiche seulement si on n'a pas demandé d'afficher des boutons en particulier
                // ou seulement ceux qu'on a demandé spécifiquement
                if (typeof tabOptions.buttonsId == 'undefined' ||
                    typeof tabOptions.buttonsId == 'object' &&
                        in_array(this._tabButtons[i].id, tabOptions.buttonsId))
                {
                    this.showButton(this._tabButtons[i].id, tabOptions.disabled);
                }
                // Sinon on masque
                else
                {
                    this.hideButton(this._tabButtons[i].id);
                }
            }

            // Si on doit afficher la notification et qu'on a un conteneur 
            if (display && containerEl)
            {
                // Si on doit l'afficher comme première notification
                if (tabOptions && tabOptions.forceFirst)
                {
                    containerEl.insertBefore(this._el, containerEl.firstChild);
                }
                // Sinon on l'affiche à la fin
                else
                {
                    containerEl.appendChild(this._el);
                }
            }

            if (this.ondisplaychange)
            {
                this.ondisplaychange();
            }

            if (this._fadeTimer)
            {
                window.clearTimeout(this._fadeTimer);
            }
            this._fadeTimer = window.setTimeout(function() { mySelf._el.style.opacity = 100; }, 5);

            return this;
        },

        /**
         * Faire apparaître ou disparaître un bouton de la notification
         *
         * @param string id Identifiant du bouton
         * @param displayType id 'show' pour faire apparaître le bouton, 'hide' pour le faire disparaître
         * @return Location.Notification Instance courante
         */
        displayButton : function(id, displayType, isDisabled)
        {
            var i = 0;
            while (i < this._btnsEl.childNodes.length && this._btnsEl.childNodes[i].getAttribute('data-id') != id)
            {
                i++;
            }
            if (i < this._btnsEl.childNodes.length)
            {
                this._btnsEl.childNodes[i].style.display = (displayType == 'show' ? '' : 'none');
                this._btnsEl.childNodes[i].disabled = (isDisabled ? 'disabled' : false);
            }
            return this;
        },

        /**
         * Faire disparaître un bouton de la notification
         *
         * @param string id Identifiant du bouton
         * @return Location.Notification Instance courante
         */
        hideButton : function(id)
        {
            return this.displayButton(id, 'hide');
        },

        /**
         * Faire apparaître un bouton dans la notification
         *
         * @param string id Identifiant du bouton
         * @return Location.Notification Instance courante
         */
        showButton : function(id, isDisabled)
        {
            return this.displayButton(id, 'show', isDisabled);
        },

        /**
         * Faire disparaître la notification
         *
         * @param Object tabOptions Options
         * @return Location.Notification Instance courante
         */
        hide : function(tabOptions)
        {
            var mySelf = this;

            if (this._el && this._el.style.display != 'none')
            {
                this._el.style.opacity = 0;
                if (this._fadeTimer)
                {
                    window.clearTimeout(this._fadeTimer);
                }
                this._fadeTimer = window.setTimeout(function() {
                    mySelf._el.style.display = 'none';

                    if (mySelf.ondisplaychange)
                    {
                        mySelf.ondisplaychange();
                    }
                }, 400);
            }
            return this;
        },

        /**
         * Est-ce que la notification est affichée
         *
         * @return bool
         */
        isShown : function()
        {
            return (this._el && this._el.style.display != 'none' ? true : false);
        }
}


/**
 * Objet Location.notificationsManager
 *
 */
Location.notificationsManager = {

    notificationsList : [],
    _defaultContainerEl : null,


    /**
     * Définir le conteneur des notifications par défaut
     *
     * @param mixed el
     */
    setDefaultContainer : function(el)
    {
        this._defaultContainerEl = Location.commonObj.getElt(el);
    },

    /**
     * Récupérer le conteneur des notifications par défaut
     *
     * @return HTMLElement el
     */
    getDefaultContainer : function(el)
    {
        return (this._defaultContainerEl ? this._defaultContainerEl : window.document.body);
    },

    /**
     * Récupérer une notification
     *
     * @param string id Identifiant de la notification
     * @return Location.Notification
     */
    get : function(id)
    {
        if (!this.notificationsList[id])
        {
            return null;
        }
        return this.notificationsList[id];
    },

    /**
     * Créer une notification
     *
     * @param string id         Identifiant de la notification
     * @param string type       Type
     * @param string message    Message
     * @param string tabButtons Buttons
     * @param string tabOptions Options
     * @return Location.Notification
     */
    create : function(id, type, message, tabButtons, tabOptions, tabSubMessages)
    {
        if (!this.notificationsList[id])
        {
            this.notificationsList[id] = new Location.Notification(this, id, type, message, tabButtons, tabOptions,
                                                                   tabSubMessages);
        }
        return this.notificationsList[id];
    }
}
