if (!window.Location)
{
    var Location = new Object();
}

if (!Location.commonObj) alert('Erreur Location');

if (!Location.HTTPRequest)
{

/* Classe Location.HTTPRequest */
Location.HTTPRequest = function(file)
{
    this._httpObj = null;
    this._method = 'POST';
    this._requestFile = file;
    this._vars = {};
    this._isActive = false;
    this._data = null;
    this._validResponseStatusRegExp = new RegExp('^200$', 'i');
    this._unauthaurizedResponseStatusRegExp = new RegExp('^401$', 'i');
    this._maintenanceResponseStatusRegExp = new RegExp('^503$', 'i');

    this._timeOutHandle = null;

    this.responseStatus = '';
    this.response = null;
    this.responseXML = null;

    this.onloading       = null;
    this.onloaded        = null;
    this.oninteractive   = null;
    this.oncompletion    = null;
    this.onerror         = null;
    this.ontimeout       = null;
    this.onunauthaurized = this._redirect;
    this.onmaintenance   = this._redirect;

    this.upload = {
        onprogress: null,
        onload    : null,
        onerror   : null,
        onabort   : null
    };
}

Location.HTTPRequest.prototype = {

    /**
     * Récupérer le résultat (au format JSON) de la requête HTTP
     *
     * @return object|false Résultat
     */
    getJSONResponse : function()
    {
        var checkFormat = this.response.charAt(0) + this.response.charAt(this.response.length - 1);
        if (checkFormat == '{}' || checkFormat == '[]')
        {
            return (typeof JSON != 'undefined' ? JSON.parse(this.response) : eval('(' + this.response + ')'));
        }
        return false;
    },

    /**
     * Récupérer le résultat de la requête HTTP
     *
     * @param bool isXML Indique si les données sont au format XML ou non
     * @return string Résultat
     */
    getResponse : function(isXML)
    {
        return (isXML ? this.responseXML : this.response);
    },

    /**
     * Récupérer le code de retour HTTP
     *
     * @return int Code de retour HTTP
     */
    getResponseStatus : function()
    {
        return this.responseStatus;
    },

    /**
     * Récupérer la valeur d'une variable
     *
     * @param string name Nom de la variable
     * @return string
     */
    getVar : function(name)
    {
        if (this._vars[name])
        {
            return this._vars[name];
        }
        return '';
    },

    /**
     * Indique si une requête HTTP est en cours
     *
     * @return bool
     */
    isActive : function()
    {
        return this._isActive;
    },

    /**
     * Définir l'URL pour la requête
     *
     */
    setFile : function(file)
    {
        this._requestFile = file;
    },

    /**
     * Définir l'expression régulière des codes de réponse valides
     *
     * @param string exp Expression régulière
     */
    setValidResponseStatusRegExp : function(exp)
    {
        this._validResponseStatusRegExp = new RegExp(exp, 'i');
    },

    /**
     * Annuler la requête Http en cours
     *
     * @return bool
     */
    abort : function()
    {
        if (!this._httpObj)
        {
            return false;
        }

        if (this._timeOutHandle)
        {
            window.clearTimeout(this._timeOutHandle);
            this._timeOutHandle = null;
        }

        try
        {
            this._httpObj.abort();
        }
        catch(e)
        {
            return false;
        }
        return true;
    },

    /**
     * Envoyer la requête Http
     *
     * @param bool isAsynchronous Mode synchrone ou asynchrone (asynchrone par défaut)
     * @param bool isJSON         Indique si il s'agit d'une demande JSON
     * @param int  timeout        Timeout
     */
    send : function(isAsynchronous, isJSON, timeout)
    {
        if (typeof isAsynchronous == 'undefined')
        {
            isAsynchronous = true;
        }

        if (!this._httpObj)
        {
            this._httpObj = Location.commonObj.getHTTPObject();
            if (!this._httpObj)
            {
                return false;
            }
        }

        try
        {
            this.abort();
            this._httpObj.open(this._method, this._requestFile, isAsynchronous);
            if (isJSON)
            {
                this._httpObj.setRequestHeader('Content-Type', 'application/json');
            }
            else if (!this._data)
            {
                this._httpObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            }
            this._httpObj.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        }
        catch (e)
        {
            if (this.onerror)
            {
                this.onerror();
            }
            return false;
        }

        // Mode Asynchrone
        if (isAsynchronous)
        {
            var mySelf = this;
            this._httpObj.onreadystatechange = function()
            {
                mySelf._onReadyStateChange.call(mySelf);
            };

            if (typeof timeout != 'undefined' && timeout > 0)
            {
                this._timeOutHandle = window.setTimeout(function() { mySelf.abort(); }, timeout);
            }

            if (this.upload.onprogress)
            {
                this._httpObj.upload.addEventListener('progress', function(e) {
                    return mySelf.upload.onprogress.call(mySelf, e);
                });
            }

            if (this.upload.onload)
            {
                this._httpObj.upload.addEventListener('load', function(e) {
                    return mySelf.upload.onload.call(mySelf, e);
                });
            }

            if (this.upload.onerror)
            {
                this._httpObj.upload.addEventListener('error', function(e) {
                    return mySelf.upload.onerror.call(mySelf, e);
                });
            }

            if (this.upload.onabort)
            {
                this._httpObj.upload.addEventListener('abort', function(e) {
                    return mySelf.upload.onabort.call(mySelf, e);
                });
            }
        }

        var postData = this._data;
        if (!postData)
        {
            if (isJSON)
            {
                this._vars['_fwRandomValue'] = new Date().getTime();
                postData = JSON.stringify(this._vars);
            }
            else
            {
                postData = '';
                for (var key in this._vars)
                {
                    postData += this._toURIParams(key, this._vars[key]);
                }
                // Technique contre le Cache de la page
                postData += '_fwRandomValue=' + new Date().getTime();
            }
        }

        try
        {
            this._httpObj.send(postData);
        }
        catch (e)
        {
            if (this.onerror)
            {
                this.onerror();
            }
            return false;
        }
        this._isActive = true;

        // Mode Synchrone
        if (!isAsynchronous)
        {
            this.response = this._httpObj.responseText;
            this.responseXML = this._httpObj.responseXML;
            if (this.oncompletion)
            {
                this.oncompletion();
            }
            this._isActive = false;
        }
        return true;
    },

    /**
     * Définir les données à envoyer
     *
     * @param mixed data Données
     */
    setData : function(data)
    {
        this._data = data;
        return this;
    },

    /**
     * Définir la méthode HTTP
     *
     * @param string method Nom de la méthode
     */
    setMethod : function(method)
    {
        this._method = method;
        return this;
    },

    /**
     * Définir une variable
     *
     * @param string name  Nom de la variable
     * @param string value Valeur
     */
    setVar : function(name, value)
    {
        this._vars[name] = value;
        return this;
    },

    /**
     * Supprimer une variable
     *
     * @param string name Nom de la variable
     * @return bool
     */
    unsetVar : function(name)
    {
        if (this._vars[name])
        {
            delete this._vars[name];
            return true;
        }
        return false;
    },

    _toURIParams : function(name, value)
    {
        var str = '';
        if (value == null)
        {
            str += name + '=&';
        }
        else
        {
            switch (typeof value)
            {
                case 'function':
                    break;

                case 'object':
                    for (name2 in value)
                    {
                        str += this._toURIParams(name + '[' + name2 + ']', value[name2]);
                    }
                    break;

                default:
                    str += name + '=' + encodeURIComponent(value) + '&';
            }
        }
        return str;
    },

    _onReadyStateChange : function()
    {
        switch (this._httpObj.readyState)
        {
            case 1:
                if (this.onloading)
                {
                    this.onloading();
                }
                break;
            case 2:
                if (this.onloaded)
                {
                    this.onloaded();
                }
                break;
            case 3:
                if (this.oninteractive)
                {
                    this.oninteractive();
                }
                break;
            case 4:
                if (this._timeOutHandle)
                {
                    window.clearTimeout(this._timeOutHandle);
                    this._timeOutHandle = null;
                }

                this.response = this._httpObj.responseText;
                this.responseXML = this._httpObj.responseXML;
                this.responseStatus = this._httpObj.status;

                this._isActive = false;

                if (this._validResponseStatusRegExp.test(this.responseStatus))
                {
                    if (this.oncompletion)
                    {
                        this.oncompletion();
                    }
                }
                else if (this._unauthaurizedResponseStatusRegExp.test(this.responseStatus))
                {
                    if (this.onunauthaurized)
                    {
                        this.onunauthaurized();
                    }
                }
                else if (this._maintenanceResponseStatusRegExp.test(this.responseStatus))
                {
                    if (this.onmaintenance)
                    {
                        this.onmaintenance();
                    }
                }
                else
                {
                    if (this.onerror)
                    {
                        this.onerror();
                    }
                }
                break;
        }
    },

    /**
     * Redirige vers une page dont l'URL est spécifiée dans la réponse.
     */
    _redirect : function()
    {
        var json = this.getJSONResponse();
        if (typeof json.redirectUrl != 'undefined')
        {
            window.document.location.href = json.redirectUrl;
        }
    }
}

}
