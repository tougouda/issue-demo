if (!window.Location)
{
    var Location = new Object();
}

if (!Location.commonObj) alert('Erreur Location');


/**
 * Objet Location.modalWindowManager
 *
 */
Location.modalWindowManager = {

    _isOpened : false,
    _el : null,
    _popupEl : null,
    _contentEl : null,
    _iframeEl : null,
    _userEl : null,
    _ajaxContentObj : null,
    _lastUrl : '',
    _onclose: null,

    /**
     * Récupérer le contenu courant de la fenêtre
     *
     * @return string
     */
    getCurrentContent : function()
    {
        return (this._isOpened ? this._lastUrl : '');
    },

    /**
     * Changer le style de la fenêtre
     *
     * @param string style
     */
    changeStyle : function(style)
    {
        if (this._el)
        {
            this._el.className = style;
        }
    },

    /**
     * Afficher la fenêtre
     *
     * @param string url        URL
     * @param Object tabOptions Options
     */
    show : function(url, tabOptions)
    {
        var mySelf = this;

        if (typeof tabOptions == 'undefined' || !(tabOptions instanceof Object))
        {
            tabOptions = {};
        }
        // Évènement onclose
        this._onclose = (tabOptions['onclose'] ? tabOptions['onclose'] : null);

        // Dimensions
        tabOptions['width']  = String(tabOptions['width'] ?  tabOptions['width']  : 500);
        tabOptions['height'] = String(tabOptions['height'] ? tabOptions['height'] : 350);
        tabOptions['style']  = tabOptions['style'] ? tabOptions['style'] : '';
        if (tabOptions['width'].match(/^[0-9]*$/))
        {
            tabOptions['width'] += 'px';
        }
        if (tabOptions['height'].match(/^[0-9]*$/))
        {
            tabOptions['height'] += 'px';
        }

        var contentType = (typeof tabOptions['contentType'] == 'undefined' ? 1 : tabOptions['contentType']);

        if (contentType == 1 && this._iframeEl && this._iframeEl.src == url && !tabOptions['isForced'])
        {
            return;
        }

        if (!this._el)
        {
            var close = function()
            {
                if (mySelf._spinnerEl.style.display == 'none' && mySelf._el.className != 'loading')
                {
                    var onclose = mySelf._onclose;
                    mySelf.hide();
                    if (onclose)
                    {
                        onclose();
                    }
                }
                return false;
            };

            this._el = window.document.createElement('DIV');
            this._el.id = 'locPlgModalWindow';
            this._el.style.display = 'none';

            // Fermeture lors du clic en dehors de la modale
            this._el.onclick = close;

            // Fermeture sur la touche Echap
            window.document.onkeyup = function(event)
            {
                event = event || window.event;
                var keyCode = (event.keyCode || event.which);

                if (keyCode == 27)
                {
                    return close();
                }
                return true;
            };

            this._spinnerEl = window.document.createElement('DIV');
            this._spinnerEl.className = 'spinner';
            this._el.appendChild(this._spinnerEl);

            var bounceEl = window.document.createElement('DIV');
            bounceEl.className = 'bounce1';
            this._spinnerEl.appendChild(bounceEl);
            bounceEl = window.document.createElement('DIV');
            bounceEl.className = 'bounce2';
            this._spinnerEl.appendChild(bounceEl);
            bounceEl = window.document.createElement('DIV');
            bounceEl.className = 'bounce3';
            this._spinnerEl.appendChild(bounceEl);

            this._popupEl = window.document.createElement('DIV');
            this._popupEl.className = 'popup';
            this._popupEl.onclick = function(event)
            {
                event = event || window.event;
                event.stopPropagation();
            };
            this._el.appendChild(this._popupEl);

            this._closeButEl = window.document.createElement('A');
            this._closeButEl.className = 'close';
            this._closeButEl.href = '#';
            this._closeButEl.onclick = close;
            this._popupEl.appendChild(this._closeButEl);

            this._contentEl = window.document.createElement('DIV');
            this._contentEl.className = 'content';
            this._popupEl.appendChild(this._contentEl);

            window.document.body.appendChild(this._el);
        }
        else if (this._isOpened)
        {
            this.hide();
        }

        // Affichage du bouton fermé ?
        this._closeButEl.style.display = (tabOptions['isNoCloseBtn'] ? 'none' : '');

        this._popupEl.style.display = 'none';
        this._popupEl.style.width = tabOptions['width'];
        this._popupEl.style.height = tabOptions['height'];
        this.changeStyle(tabOptions['style']);
        this._el.style.display = 'block';
        this._spinnerEl.style.display = 'block';

        this._lastUrl = url;
        this._isOpened = true;
        if (contentType == 1)
        {
            // Destruction de l'iframe
            if (this._iframeEl)
            {
                this._iframeEl.src = 'about:blank';
                this._contentEl.removeChild(this._iframeEl);
            }

            this._iframeEl = window.document.createElement('IFRAME');
            this._iframeEl.className = 'iframeContent';
            this._iframeEl.frameBorder = this._iframeEl.scrolling = 'no';
            this._contentEl.appendChild(this._iframeEl);

            Location.commonObj.addEltEvent(this._iframeEl, 'load', function(e){mySelf._loaded(false);});

            this._iframeEl.src = url;
        }
        else if (contentType == 2)
        {
            if (!this._ajaxContentObj)
            {
                this._ajaxContentObj = new Location.AjaxContent(this._contentEl, url);
                this._ajaxContentObj.oncompletion = function(){ mySelf._loaded(); };
                this._ajaxContentObj.onerror = function(){ mySelf.hide(); };
            }
            this._ajaxContentObj.load();
        }
        else
        {
            var oldUserEl = this._userEl;
            this._userEl = window.document.getElementById(url);
            if (oldUserEl && oldUserEl != this._userEl)
            {
                oldUserEl.style.display = 'none';
                window.document.body.appendChild(oldUserEl);
            }
            if (this._userEl)
            {
                this._contentEl.appendChild(this._userEl);
                this._userEl.style.display = 'block';
                this._loaded();
            }
        }
    },

    /**
     * Fermer la fenêtre
     *
     */
    hide : function()
    {
        if (this._el)
        {
            this._popupEl.style.display = 'none';
            this._el.style.display = 'none';

            // Destruction de l'iframe
            if (this._iframeEl)
            {
                this._iframeEl.src = 'about:blank';
                this._contentEl.removeChild(this._iframeEl);
                this._iframeEl = null;
            }
            if (this._userEl)
            {
                this._userEl.style.display = 'none';
            }
        }
        this._isOpened = false;
        this._onclose = null;
    },

    /**
     * Récupérer l'élément DIV principal
     *
     * @return object L'élément
     */
    getEl : function()
    {
        return this._el;
    },

    _loaded : function()
    {
        if (this._isOpened)
        {
            this._popupEl.style.display = 'block';
            this._spinnerEl.style.display = 'none';
            this._popupEl.style.marginTop = -((this._popupEl.offsetHeight / 2).toFixed(0)) + 'px';
        }
    }
}
