/**
 */

if (!window.Location)
{
    var Location = new Object();
}

if (!Location.commonObj) alert('Erreur Location');


/**
 * Classe Location.Currency
 * 
 * @param Location.currenciesManager pluginObj
 * @param string name nom de l'élément de formulaire
 * @param integer mode, 0 : input, 1 : text
 * @param string currencySymbol symbole monétaire
 * @param float euroRate taux de conversion en euro, facultatif
 */
Location.Currency = function(pluginObj, name, mode, currencySymbol, euroRate, precision)
{
    this._pluginObj  = pluginObj;
    this.name        = name;
    this._precision  = (precision == null ? null : parseInt(precision));
    this._el         = null;
    this._spanEl     = null;
    this._spanTextEl = null;
    this._curEl1     = null;
    this._curEl2     = null;
    this._isDisabled = true;

    this._mode           = mode;
    this._currencySymbol = currencySymbol;
    this._euroRate       = (euroRate == null ? null : parseFloat(euroRate));
    this._hasTooltip     = (euroRate == null ? false : true);
    this._isTooltipShown = false;

    this._isSpcKeyPressed = false;

    // Evénements
    this.onchange   = null;
    this.onblur     = null;
    this.onfocus    = null;
    this.onkeydown  = null;
    this.onkeypress = null;
    this.onkeyup    = null;

    // Initialisation
    this._init();
}

Location.Currency.prototype = {

    /**
     * Mettre le focus sur l'élément
     *
     * @return bool
     */
    blur : function()
    {
        if (this._isDisabled)
        {
            return false;
        }
        this._el.blur();
        return true;
    },

    /**
     * Désactiver le currency
     *
     * @return bool
     */
    disable : function()
    {
        if (this._isDisabled)
        {
            return false;
        }
        this._spanEl.className = 'locPlgCurrenciesElt disabled';
        this._el.disabled = true;
        this._isDisabled = true;
        return true;
    },

    /**
     * Activer le currency
     *
     * @return bool
     */
    enable : function()
    {
        if (!this._isDisabled)
        {
            return false;
        }
        this._spanEl.className = 'locPlgCurrenciesElt';
        this._el.disabled = false;
        this._isDisabled  = false;
        return true;
    },

    /**
     * Mettre le focus sur l'élément
     *
     * @return bool
     */
    focus : function()
    {
        if (this._isDisabled)
        {
            return false;
        }
        this._el.focus();
        return true;
    },

    /**
     * Retourne le symbole du currency
     */
    getCurrencySymbol : function()
    {
        return this._currencySymbol;
    },

    /**
     * Retourne l'élément HTML contenant la valeur
     */
    getElement : function()
    {
        return (this._el ? this._el : false);
    },

    /**
     * Retourne le taux de conversion en euro du currency
     */
    getEuroRate : function()
    {
        return this._euroRate;
    },

    /**
     * Retourne le mode du currency
     */
    getMode : function()
    {
        return this._mode;
    },

    /**
     * Retourne le nom du currency
     */
    getName : function()
    {
        return this.name;
    },

    /**
     * Retourne le nombre de décimales
     */
    getPrecision : function()
    {
        return this._precision;
    },

    /**
     * Retourne l'élément SPAN contenant les éléments HTML du widget
     */
    getSpanElement : function()
    {
        return (this._spanEl ? this._spanEl : false);
    },

    /**
     * Récupérer le tabIndex du currency
     *
     * @return int
     */
    getTabIndex : function()
    {
        var tabIndex = this._el.getAttribute('tabIndex');
        return (tabIndex == null || tabIndex == '' ? null : Number(tabIndex));
    },

    /**
     * Récupérer la valeur
     */
    getValue : function()
    {
        return Number(this._el.value);
    },

    /**
     * Indique si l'élément est désactivé
     *
     * @return bool
     */
    isDisabled : function()
    {
        return this._isDisabled;
    },

    /**
     * Afficher l'info-bulle de conversion monétaire
     */
    _displayTooltip : function()
    {
        rate = parseFloat(this._euroRate);
        if (isNaN(rate) || !isFinite(rate))
        {
            rate = 0;
        }

        var value = parseFloat(this._el.value * rate);
        if (isNaN(value) || !isFinite(value))
        {
            value = 0;
        }
        value = LOC_View.getNumberFormat(value);

        if (Location.toolTipsObj)
        {
            this._isTooltipShown = true;
            Location.toolTipsObj.show(LOC_View.getVar('currencyFormat').replace('%f', value),
                                      LOC_View.getVar('doesCurrencySymbolPrecedes') ? {corner:'tr'} : {corner:'tl'});
        }
    },

    /**
     * Masquer l'info-bulle de conversion monétaire
     */
    _hideTooltip : function()
    {
        if (Location.toolTipsObj)
        {
            this._isTooltipShown = false;
            Location.toolTipsObj.hide();
        }
    },

    _init : function()
    {
        var mySelf = this;

        this._el = window.document.getElementById(this.name);
        if (!this._el)
        {
            return;
        }

        // active la saisie
        this._isDisabled = this._el.disabled;

        // création du span le plus englobant
        this._spanEl = window.document.createElement('SPAN');
        this._spanEl.id = this.name + '.span';
        this._spanEl.className = 'locPlgCurrenciesElt' + (this._isDisabled ? ' disabled' : '');
        this._spanEl.style.whiteSpace = 'nowrap';
        this._el.parentNode.insertBefore(this._spanEl, this._el);
        this._spanEl.appendChild(this._el);

        // si en mode input
        if (this._mode == 0)
        {
            // création du span intérieur en mode text
            this._spanTextEl = window.document.createElement('SPAN');
            this._spanTextEl.id = this.name + '.text';
            this._spanEl.appendChild(this._spanTextEl);
        }
        else
        {
            // formatage suivant la locale
            var format = LOC_View.getVar('currencyFormat').replace(LOC_View.getVar('currencySymbol'), this._currencySymbol);
            var pos = format.search('%f');

            if (pos != -1)
            {
                this._curEl1 = window.document.createTextNode(format.substr(0, pos));
                this._curEl2 = window.document.createTextNode(format.substr(pos + 2));
                this._spanEl.insertBefore(this._curEl1, this._el);
                this._spanEl.appendChild(this._curEl2);
            }

            // Evénement onkeydown
            this._el._oldonkeydown = this._el.onkeydown;
            this._el.onkeydown = function(event)
            {
                event = event || window.event;
                var keyCode = (event.keyCode || event.which);

                // Détection des touches spéciales authorisées
                mySelf._isSpcKeyPressed = false;
                if ((keyCode == 35) || (keyCode == 36) || // DEBUT - FIN
                    (keyCode == 37) || (keyCode == 39) || // FLECHE DROITE - FLECHE GAUCHE
                    (keyCode == 33) || (keyCode == 34) || // PAGE HAUT - PAGE BAS
                    (keyCode == 38) || (keyCode == 40) || // FLECHE HAUT - FLECHE BAS
                    (keyCode == 8)  || (keyCode == 46) || // DEL - SUPPR
                    (keyCode == 9)  || (keyCode == 13))   // TAB - ENTREE
                {
                    mySelf._isSpcKeyPressed = true;
                }

                var result = true;
                if (mySelf._el._oldonkeydown && mySelf._el._oldonkeydown(event) == false)
                {
                    result = false;
                }
                if (mySelf.onkeydown && mySelf.onkeydown(event) == false)
                {
                    result = false;
                }
                return result;
            }

            // Evénement onkeypress
            this._el._oldonkeypress = this._el.onkeypress;
            this._el.onkeypress = function(event)
            {
                event = event || window.event;
                var keyCode = (event.keyCode || event.which);

                // Touches interdites !
                if (!mySelf._isSpcKeyPressed)
                {
                    var keyChar = String.fromCharCode(keyCode);
                    if (!((keyChar >= '0' && keyChar <= '9') ||
                          (keyChar == '.' && this.value.indexOf('.') == -1)))
                    {
                        if (mySelf._mode != 2 && keyChar == '-' && this.value.indexOf('-') == -1)
                        {
                            // Le caractère '-' ne peut être qu'au début
                            Location.commonObj.setCursorPos(this, 0);
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                var result = true;
                if (mySelf._el._oldonkeypress && mySelf._el._oldonkeypress(event) == false)
                {
                    result = false;
                }
                if (mySelf.onkeypress && mySelf.onkeypress(event) == false)
                {
                    result = false;
                }
                return result;
            }

            // Evénement onkeyup
            this._el._oldonkeyup = this._el.onkeyup;
            this._el.onkeyup = function(event)
            {
                // Mise à jour du tooltip si visible
                if (mySelf._isTooltipShown)
                {
                    mySelf._displayTooltip();
                }

                var result = true;
                if (mySelf._el._oldonkeyup && mySelf._el._oldonkeyup(event) == false)
                {
                    result = false;
                }
                if (mySelf.onkeyup && mySelf.onkeyup(event) == false)
                {
                    result = false;
                }
                return result;
            }

            // Evénement onchange
            this._el._oldonchange = this._el.onchange;
            this._el.onchange = function(event)
            {
                mySelf.setValue(this.value, false);

                var result = true;
                if (this._oldonchange && this._oldonchange(event) == false)
                {
                    result = false;
                }
                if (mySelf.onchange && mySelf.onchange(event) == false)
                {
                    result = false;
                }
                return result;
            }

            // Evénement onblur
            this._el._oldonblur = this._el.onblur;
            this._el.onblur = function(event)
            {
                var result = true;
                if (this._oldonblur && this._oldonblur(event) == false)
                {
                    result = false;
                }
                if (mySelf.onblur && mySelf.onblur(event) == false)
                {
                    result = false;
                }
                return result;
            }

            // Evénement onfocus
            this._el._oldonfocus = this._el.onfocus;
            this._el.onfocus = function(event)
            {
                var result = true;
                if (this._oldonfocus && this._oldonfocus(event) == false)
                {
                    result = false;
                }
                if (mySelf.onfocus && mySelf.onfocus(event) == false)
                {
                    result = false;
                }
                var el = this;
                window.setTimeout(function () { el.select(); }, 50);
                return result;
            }
        }

        // gestion de l'affichage du tooltip sur les mouvements de la souris
        if (this._hasTooltip)
        {
            this._spanEl.onmousemove = function(event)
            {
                mySelf._displayTooltip();
            }
            this._spanEl.onmouseout = function(event)
            {
                mySelf._hideTooltip();
            }
        }

        // Mise à jour de la valeur de l'élément
        this.setValue(this._el.value, true);
    },

    /**
     * Définir le tabIndex du currency
     *
     * @param int|null tabIndex
     * @return int
     */
    setTabIndex : function(tabIndex)
    {
        if (typeof tabIndex == 'undefined' || tabIndex == null)
        {
            this._el.removeAttribute('tabIndex');
        }
        else
        {
            this._el.setAttribute('tabIndex', Number(tabIndex));
        }
        return this.getTabIndex();
    },

    /**
     * Désactiver/Activer le currency
     *
     * @param bool isDisabled
     * @return bool
     */
    setDisabled : function(isDisabled)
    {
        if (isDisabled)
        {
            return this.disable();
        }
        else
        {
            return this.enable();
        }
    },

    /**
     * Définir la valeur
     *
     * @param float  value          Valeur
     * @param bool   isEventActived Activer l'évenement onchange ?
     * @return float Valeur réellement définie
     */
    setValue : function(value, isEventActived)
    {
        value = parseFloat(value);
        if (isNaN(value))
        {
            value = 0;
        }

        if (this._mode == 0)
        {
            this._el.value = value;
            var currencyFormat = LOC_View.getVar('currencyFormat');
            var currencySymbol = LOC_View.getVar('currencySymbol');

            // Formatage du nombre sans le signe
            var formattedValue = LOC_View.getNumberFormat(Math.abs(value), this._precision);
            formattedValue = currencyFormat.replace(currencySymbol, this._currencySymbol).replace('%f', formattedValue);
            // Ajout du signe - pour les valeurs négatives
            formattedValue = (value < 0 ? '-' : '') + formattedValue;

            this._spanTextEl.innerHTML = formattedValue;
        }
        else
        {
            var oldValue = this._el.value;
            this._el.value = value;
            if (Number(value) != Number(oldValue))
            {
                if (typeof isEventActived == 'undefined' || isEventActived)
                {
                    var mySelf = this;
                    window.setTimeout(function(){ Location.commonObj.fireEvent(mySelf._el, 'change'); }, 0);
                }
            }
        }
        return Number(value);
    }
}

/**
 * Objet Location.currenciesManager
 */
Location.currenciesManager = {

    _tabObj : [],

    /**
     * Créer un Currency
     *
     * @param string name nom de l'élément de formulaire
     * @param integer mode, 0 : input, 1 : text
     * @param string currencySymbol symbole monétaire
     * @param float euroRate taux de conversion en euro, facultatif
     * @return Location.Currency Instance du Currency
     */
    createCurrency : function(name, mode, currencySymbol, euroRate, precision)
    {
        if (this._tabObj[name])
        {
            return this._tabObj[name];
        }

        this._tabObj[name] = new Location.Currency(this, name, mode, currencySymbol, euroRate, precision);
        return this._tabObj[name];
    },

    /**
     * Récupérer l'instance d'un Currency
     *
     * @param string name Nom du Currency
     * @return Location.Currency
     */
    getCurrency : function(name)
    {
        if (!this._tabObj[name])
        {
            return null;
        }
        return this._tabObj[name];
    },

    /**
     * Supprimer l'instance d'un Currency
     *
     * @param string name Nom du Currency
     * @return bool
     */
    deleteCurrency : function(name)
    {
        if (!this._tabObj[name])
        {
            return false;
        }
        delete this._tabObj[name];
        return true;
    },

    /**
     * Cloner l'instance d'un Currency
     *
     * @param string name Nom du Currency
     * @param string srcName Nom du Currency à cloner
     * @return Location.Currency
     */
    cloneCurrency : function(name, srcName)
    {
        if (srcObj = this.getCurrency(srcName))
        {
            return this.createCurrency(name, srcObj.getMode(), srcObj.getCurrencySymbol(), srcObj.getEuroRate(), srcObj.getPrecision());
        }
    }
}
