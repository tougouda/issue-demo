if (!window.Location)
{
    var Location = new Object();
}

if (!Location.commonObj) alert('Erreur Location');


/**
 * Classe Location.SpinControl
 * 
 * @param Location.spinControlsManager pluginObj  Manager associé
 * @param string                       name       Nom de l'élément de formulaire
 * @param float                        minValue   Valeur minimale
 * @param float                        maxValue   Valeur maximale
 * @param float                        step       Petit pas
 * @param float                        bigStep    Grand pas
 * @param int                          nbDecimals Nombre de décimales à afficher
 */
Location.SpinControl = function(pluginObj, name, minValue, maxValue, step, bigStep, nbDecimals)
{
    this._pluginObj  = pluginObj;
    this.name        = name;
    this._el         = null;
    this._spanEl     = null;
    this._isDisabled = true;
    this._isReadOnly = true;

    this._minValue   = null;
    this._maxValue   = null;
    this._step       = (step || 1);
    this._bigStep    = (bigStep || (10 * this._step));
    this._nbDecimals = (nbDecimals ? parseInt(nbDecimals) : 0);

    this._isSpcKeyPressed = false;

    // Evénements
    this.onchange   = null;
    this.onblur     = null;
    this.onfocus    = null;
    this.onkeydown  = null;
    this.onkeypress = null;
    this.onkeyup    = null;

    // Initialisation
    this._init(minValue, maxValue);
}

Location.SpinControl.prototype = {

    /**
     * Mettre le focus sur l'élément
     *
     * @return bool
     */
    blur : function()
    {
        if (this._isDisabled)
        {
            return false;
        }
        this._el.blur();
        return true;
    },

    /**
     * Décrémenter la valeur de l'élément
     *
     * @param bool isBigStep
     * @return float
     */
    decrementValue : function(isBigStep)
    {
        if (this._isReadOnly || this._isDisabled)
        {
            return false;
        }

        var step = (isBigStep ? this._bigStep : this._step);
        return this.setValue(this.getValue() - step, true);
    },

    /**
     * Désactiver le spinControl
     *
     * @return bool
     */
    disable : function()
    {
        if (this._isDisabled)
        {
            return false;
        }
        this._spanEl.className = 'locPlgSpinControlsElt disabled' + (this._isReadOnly ? ' readonly' : '');
        this._el.disabled = true;
        this._isDisabled  = true;
        return true;
    },

    /**
     * Activer le spinControl
     *
     * @return bool
     */
    enable : function()
    {
        if (!this._isDisabled)
        {
            return false;
        }
        this._spanEl.className = 'locPlgSpinControlsElt' + (this._isReadOnly ? ' readonly' : '');
        this._el.disabled = false;
        this._isDisabled  = false;
        return true;
    },

    /**
     * Mettre le focus sur l'élément
     *
     * @return bool
     */
    focus : function()
    {
        if (this._isDisabled)
        {
            return false;
        }
        try
        {
            this._el.focus();
        }
        catch(ex)
        {
            return false;
        }
        return true;
    },

    /**
     * Récupérer le nombre de décimales
     * 
     * @return int
     */
    getDecimals: function()
    {
        return this._nbDecimals;
    },

    /**
     * Récupérer la valeur maximale
     *
     * @return float|null Valeur maximale
     */
    getMaximumValue : function(value)
    {
        return this._maxValue;
    },

    /**
     * Récupérer la valeur minimale
     *
     * @return float|null Valeur minimale
     */
    getMinimumValue : function(value)
    {
        return this._minValue;
    },
    
    /**
     * Récupérer la valeur du grand pas
     * 
     * @return float|null
     */
    getStep: function()
    {
        return this._step;
    },
    
    /**
     * Récupérer la valeur du grand pas
     * 
     * @return float|null
     */
    getBigStep: function()
    {
        return this._bigStep;
    },

    /**
     * Récupérer le tabIndex du spinControl
     *
     * @return int
     */
    getTabIndex : function()
    {
        var tabIndex = this._el.getAttribute('tabIndex');
        return (tabIndex == null || tabIndex == '' ? null : Number(tabIndex));
    },

    /**
     * Récupérer la valeur de l'élément
     *
     * @return float
     */
    getValue : function()
    {
        return Number(this._el.value);
    },

    /**
     * Retourne l'élément SPAN contenant les éléments HTML du widget
     */
    getSpanElement : function()
    {
        return (this._spanEl ? this._spanEl : false);
    },

    /**
     * Incrémenter la valeur de l'élément
     *
     * @param bool isBigStep
     * @return float
     */
    incrementValue : function(isBigStep)
    {
        if (this._isReadOnly || this._isDisabled)
        {
            return false;
        }

       var step = (isBigStep ? this._bigStep : this._step);
       return this.setValue(this.getValue() * 1 + step * 1, true);
    },

    /**
     * Indique si l'élément est désactivé
     *
     * @return bool
     */
    isDisabled : function()
    {
        return this._isDisabled;
    },

    /**
     * Indique si la valeur est au maximum
     *
     * @return bool
     */
    isMaximumValue : function()
    {
       return (this.getValue() == this._maxValue);
    },

    /**
     * Indique si la valeur est au minimum
     *
     * @return bool
     */
    isMinimumValue : function()
    {
       return (this.getValue() == this._minValue);
    },

    /**
     * Indique si l'élément est en lecture seule
     *
     * @return bool
     */
    isReadOnly : function()
    {
        return this._isReadOnly;
    },

    /**
     * Définir le nombre de décimales
     *
     * @param int nbDecimals Nombre de décimales
     * @return float Valeur de l'élément
     */
    setDecimalsCount : function(nbDecimals)
    {
        this._nbDecimals = parseInt(nbDecimals);
        return this.getValue();
    },

    /**
     * Désactiver/Activer le spinControl
     *
     * @param bool isDisabled
     * @return bool
     */
    setDisabled : function(isDisabled)
    {
        if (isDisabled || typeof isDisabled == 'undefined')
        {
            return this.disable();
        }
        else
        {
            return this.enable();
        }
    },

    /**
     * Définir la valeur maximale
     *
     * @param float|null value Valeur maximale
     * @return float Valeur de l'élément
     */
    setMaximumValue : function(value)
    {
        if (value == null)
        {
            this._maxValue = null;
        }
        else
        {
            this._maxValue = parseFloat(value);
            if (this._minValue != null && this._maxValue < this._minValue)
            {
                this._maxValue = this._minValue;
            }
        }
        return this.getValue();
    },

    /**
     * Définir la valeur minimale
     *
     * @param float|null value Valeur minimale
     * @return float Valeur de l'élément
     */
    setMinimumValue : function(value)
    {
        if (value == null)
        {
            this._minValue = null;
        }
        else
        {
            this._minValue = parseFloat(value);
            if (this._maxValue != null && this._minValue > this._maxValue)
            {
                this._minValue = this._maxValue;
            }
        }
        return this.getValue();
    },

    /**
     * Mettre ou non le spinControl en lecture seule
     *
     * @param bool isReadOnly
     * @return bool
     */
    setReadOnly : function(isReadOnly)
    {
        isReadOnly = (isReadOnly || typeof isReadOnly == 'undefined' ? true : false);
        if (this._isReadOnly == isReadOnly)
        {
            return false;
        }
        this._spanEl.className = 'locPlgSpinControlsElt' + (this._isDisabled ? ' disabled' : (isReadOnly ? ' readonly' : ''));
        this._el.readOnly = isReadOnly;
        this._isReadOnly = isReadOnly;
        return true;
    },

    /**
     * Définir le petit pas
     *
     * @param int step Pas
     */
    setStep : function(step)
    {
        this._step = parseFloat(step);
    },

    /**
     * Définir le tabIndex du spinControl
     *
     * @param int|null tabIndex
     * @return int
     */
    setTabIndex : function(tabIndex)
    {
        if (typeof tabIndex == 'undefined' || tabIndex == null)
        {
            this._el.removeAttribute('tabIndex');
        }
        else
        {
            this._el.setAttribute('tabIndex', Number(tabIndex));
        }
        return this.getTabIndex();
    },

    /**
     * Définir la valeur de l'élément
     *
     * @param float  value          Valeur
     * @param bool   isEventActived Activer l'évenement onchange ?
     * @return float Valeur réellement définie
     */
    setValue : function(value, isEventActived)
    {
        value = parseFloat(value);
        if (isNaN(value))
        {
            value = 0.0;
        }
        if (this._minValue != null && value < this._minValue)
        {
            value = this._minValue;
        }
        if (this._maxValue != null && value > this._maxValue)
        {
            value = this._maxValue;
        }

        if (this._nbDecimals != null)
        {
            value = value.toFixed(this._nbDecimals);
        }

        var oldValue = this._el.value;
        this._el.value = value;
        if (Number(value) != Number(oldValue))
        {
            if (typeof isEventActived == 'undefined' || isEventActived)
            {
                var mySelf = this;
                window.setTimeout(function(){ Location.commonObj.fireEvent(mySelf._el, 'change'); }, 0);
            }
        }
        return Number(value);
    },

    /**
     * Définir l'interval des valeurs possibles
     *
     * @param float minValue Valeur minimale
     * @param float maxValue Valeur maximale
     * @return float Valeur de l'élément
     */
    setValueInterval : function(minValue, maxValue)
    {
        if (minValue == null || maxValue == null || minValue < maxValue)
        {
            this.setMinimumValue(minValue);
            return this.setMaximumValue(maxValue);
        }
        else
        {
            this.setMinimumValue(maxValue);
            return this.setMaximumValue(minValue);
        }
    },

    /**
     * Définir la valeur de l'élément au maximum
     *
     * @param bool   isEventActived Activer l'évenement onchange ?
     * @return float Valeur réellement définie
     */
    setValueToMaximum : function(isEventActived)
    {
        var result;
        if (this._maxValue != null)
        {
            result = this.setValue(this._maxValue, isEventActived);
        }
        else
        {
            result = this.getValue();
        }
        return result;
    },

    /**
     * Définir la valeur de l'élément au minimum
     *
     * @param bool   isEventActived Activer l'évenement onchange ?
     * @return float Valeur réellement définie
     */
    setValueToMinimum : function(isEventActived)
    {
        var result;
        if (this._minValue != null)
        {
            result = this.setValue(this._minValue, isEventActived);
        }
        else
        {
            result = this.getValue();
        }
        return result;
    },

    /**
     * Constructeur
     *
     */
    _init : function(minValue, maxValue)
    {
        var mySelf = this;

        this._el = window.document.getElementById(this.name);
        if (!this._el)
        {
            return;
        }

        // Evénement onkeydown
        this._el._oldonkeydown = this._el.onkeydown;
        this._el.onkeydown = function(e)
        {
            e = e || window.event;
            var keyCode = (e.keyCode || e.which);
            var isCtrlPressed = e.ctrlKey;

            // Détection des touches spéciales authorisées
            mySelf._isSpcKeyPressed = false;
            switch (keyCode)
            {
                // Page HAUT
                case 33:
                // Fléche HAUT
                case 38:
                    if (isCtrlPressed && mySelf.getMaximumValue() != null)
                    {
                        mySelf.setValueToMaximum();
                    }
                    else
                    {
                        mySelf.incrementValue((keyCode == 33));
                    }
                    return false;
                // Page BAS
                case 34:
                // Fléche BAS
                case 40:
                    if (isCtrlPressed && mySelf.getMinimumValue() != null)
                    {
                        mySelf.setValueToMinimum();
                    }
                    else
                    {
                        mySelf.decrementValue((keyCode == 34));
                    }
                    return false;
                default:
                    if ((keyCode == 35) || (keyCode == 36) || // DEBUT - FIN
                        (keyCode == 37) || (keyCode == 39) || // FLECHE DROITE - FLECHE GAUCHE
                        (keyCode == 8)  || (keyCode == 46) || // DEL - SUPPR
                        (keyCode == 9)  || (keyCode == 13))   // TAB - ENTREE
                    {
                        mySelf._isSpcKeyPressed = true;
                    }
            }

            var result = true;
            if (this._oldonkeydown && this._oldonkeydown(e) == false)
            {
                result = false;
            }
            if (mySelf.onkeydown && mySelf.onkeydown(e) == false)
            {
                result = false;
            }
            return result;
        }

        // Evénement onkeypress
        this._el._oldonkeypress = this._el.onkeypress;
        this._el.onkeypress = function(e)
        {
            e = e || window.event;
            var keyCode = (e.keyCode || e.which);

            // Touches interdites !
            if (!mySelf._isSpcKeyPressed)
            {
                var keyChar = String.fromCharCode(keyCode);
                if (!((keyChar >= '0' && keyChar <= '9') ||
                      (mySelf._nbDecimals > 0 && keyChar == '.' && this.value.indexOf('.') == -1)))
                {
                    if ((mySelf._minValue < 0 || mySelf._minValue == null) && keyChar == '-' && this.value.indexOf('-') == -1)
                    {
                        // Le caractère '-' ne peut être qu'au début
                        Location.commonObj.setCursorPos(this, 0);
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            var result = true;
            if (this._oldonkeypress && this._oldonkeypress(e) == false)
            {
                result = false;
            }
            if (mySelf.onkeypress && mySelf.onkeypress(e) == false)
            {
                result = false;
            }
            return result;
        }

        // Evénement onkeyup
        this._el._oldonkeyup = this._el.onkeyup;
        this._el.onkeyup = function(e)
        {
            var result = true;
            if (this._oldonkeyup && this._oldonkeyup(e) == false)
            {
                result = false;
            }
            if (mySelf.onkeyup && mySelf.onkeyup(e) == false)
            {
                result = false;
            }
            return result;
        }

        // Evénement onchange
        this._el._oldonchange = this._el.onchange;
        this._el.onchange = function(e)
        {
            mySelf.setValue(this.value, false);

            var result = true;
            if (this._oldonchange && this._oldonchange(e) == false)
            {
                result = false;
            }
            if (mySelf.onchange && mySelf.onchange(e) == false)
            {
                result = false;
            }
            return result;
        }

        // Evénement onfocus
        this._el._oldonfocus = this._el.onfocus;
        this._el.onfocus = function(e)
        {
            var result = true;
            if (this._oldonfocus && this._oldonfocus(e) == false)
            {
                result = false;
            }
            if (mySelf.onfocus && mySelf.onfocus(e) == false)
            {
                result = false;
            }
            var el = this;
            window.setTimeout(function () { el.select(); }, 50);
            return result;
        }

        // Evénement onblur
        this._el._oldonblur = this._el.onblur;
        this._el.onblur = function(e)
        {
            var result = true;
            if (this._oldonblur && this._oldonblur(e) == false)
            {
                result = false;
            }
            if (mySelf.onblur && mySelf.onblur(e) == false)
            {
                result = false;
            }
            return result;
        }

        this._isDisabled = this._el.disabled;
        this._isReadOnly = this._el.readOnly;

        this._spanEl = window.document.createElement('SPAN');
        this._spanEl.className = 'locPlgSpinControlsElt' + (this._isDisabled ? ' disabled' : '') + (this._isReadOnly ? ' readonly' : '');
        this._el.parentNode.insertBefore(this._spanEl, this._el);
        this._spanEl.appendChild(this._el);

        this._incBtnEl = window.document.createElement('A');
        this._incBtnEl.className = 'button increment';
        this._incBtnEl.href = '#';
        this._incBtnEl.innerHTML = '';
        this._incBtnEl.onclick = function()
        {
            if (!mySelf._isDisabled && !mySelf._isReadOnly)
            {
                mySelf._el.focus();
                mySelf.incrementValue(false);
            }
            return false;
        }
        this._decBtnEl = window.document.createElement('A');
        this._decBtnEl.className = 'button decrement';
        this._decBtnEl.href = '#';
        this._decBtnEl.innerHTML = '';
        this._decBtnEl.onclick = function()
        {
            if (!mySelf._isDisabled && !mySelf._isReadOnly)
            {
                mySelf._el.focus();
                mySelf.decrementValue(false);
            }
            return false;
        }

        this._spanEl.appendChild(this._incBtnEl);
        this._spanEl.appendChild(this._decBtnEl);

        this.setValueInterval(minValue, maxValue);
    }
}


/**
 * Objet Location.spinControlsManager
 *
 */
Location.spinControlsManager = {

    _tabObj : [],

    /**
     * Créer un SpinControl
     *
     * @param string name Nom
     * @return Location.SpinControl Instance du SpinControl
     */
    createSpinControl : function(name, minValue, maxValue, step, bigStep, nbDecimals)
    {
        if (this._tabObj[name])
        {
            return this._tabObj[name];
        }

        this._tabObj[name] = new Location.SpinControl(this, name, minValue, maxValue, step, bigStep, nbDecimals);
        return this._tabObj[name];
    },

    /**
     * Récupérer l'instance d'un SpinControl
     *
     * @param string name Nom du SpinControl
     * @return Location.SpinControl
     */
    getSpinControl : function(name)
    {
        if (!this._tabObj[name])
        {
            return null;
        }
        return this._tabObj[name];
    },

    /**
     * Supprimer l'instance d'un SpinControl
     *
     * @param string name Nom du SpinControl
     * @return bool
     */
    deleteSpinControl : function(name)
    {
        if (!this._tabObj[name])
        {
            return false;
        }
        delete this._tabObj[name];
        return true;
    },

    /**
     * Cloner l'instance d'un spinControl
     *
     * @param string name Nom du spinControl
     * @param string srcName Nom du spinControl à cloner
     * @return Location.SpinControl
     */
    cloneSpinControl : function(name, srcName)
    {
        if (srcObj = this.getSpinControl(srcName))
        {
            return this.createSpinControl(name, srcObj.getMinimumValue(), srcObj.getMaximumValue(),
                                          srcObj.getStep(), srcObj.getBigStep(), srcObj.getDecimals());
        }
    }
}
