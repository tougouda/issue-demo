/*
 * Fonction de soumission du formulaire 
*/
function submitForm(id, startCodeId)
{
    var form = window.document.forms[0];
    $ge('idToRegenerate').value = id;
    $ge('startCodeIdToRegenerate').value = startCodeId;
    //Image de chargement
    LOC_View.displayLoading(true);
    //Soumission du formulaire
    form.submit();

   return false;
}

