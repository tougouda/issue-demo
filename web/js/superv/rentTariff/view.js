var tariffs = {

    data: {},
    tabUpdated: {},

    /**
     * Initialisation
     */
    init: function (data, defaultFilter)
    {
        // Initialisation du tableau d'origine
        for (var i = 0; i < data.length; i++)
        {
            this.data[data[i].id] = {};
            for (var j = 0; j < data[i].tabTariffs.length; j++)
            {
                var durationRangeInfos = data[i].tabTariffs[j];

                this.data[data[i].id][durationRangeInfos['durationRange.id']] = {
                    amount      : durationRangeInfos.amount,
                    pricingPrice: durationRangeInfos.pricingPrice,
                    costPrice   : durationRangeInfos.costPrice
                };
            }
        }

        /**
         * Modification tarifs
         */
        $('#rentTariff > tbody > tr > td.duration-range input').change(function() {
            var [col, tariffFamilyId, durationRange] = $(this).attr('id').split('_');
            tariffs._setUpdatedRows(col, tariffFamilyId, durationRange, $(this).val());
        });

        /**
         * Filtre les lignes suivant la famille tarifaire
         */
        $('#tariffFamily-filter').keyup(function() {
            var filter = $(this).val().toUpperCase();

            $('#rentTariff > tbody > tr').each(function() {
                var content = $(this).data('content').toUpperCase();
                $(this).toggle(content.indexOf(filter) > -1);
            });
        });

        /**
         * Affichage de la colonne "Tarif de base"
         */
        $('#baseTariff-filter').click(function() {
            tariffs._displayBaseTariffCol();
            tariffs._displayDurationRangeCols();
        });

        /**
         * Affichage de la colonne "Prix pricing"
         */
        $('#pricingPrice-filter').click(function() {
            tariffs._displayPricingPriceCol();
            tariffs._displayDurationRangeCols();
        });

        /**
         * Affichage de la colonne "Prix de revient"
         */
        $('#costPrice-filter').click(function() {
            tariffs._displayCostPriceCol();
            tariffs._displayDurationRangeCols();
        });

        $('#tariffFamily-filter').keyup();
        this._displayBaseTariffCol();
        this._displayPricingPriceCol();
        this._displayCostPriceCol();
        this._displayDurationRangeCols(defaultFilter);

    },

    valid: function()
    {
        //Image de chargement
        LOC_View.displayLoading(true);


        $('#valid').val(JSON.stringify({
            valid       : 'ok',
            tabTariffs  : tariffs.tabUpdated
        }));
        $('form').submit();

        return false;

    },


    /**
     * Affichage de la colonne "Tarif de base"
     */
    _displayBaseTariffCol: function()
    {
        var isChecked = $('#baseTariff-filter').is(':checked');
        $('#rentTariff > tbody > tr > td.tb, #rentTariff > thead > tr > th.tb').each(function() {
            $(this).toggle(isChecked);
        });
    },


    /**
     * Affichage de la colonne "Prix pricing"
     */
    _displayPricingPriceCol: function()
    {
        var isChecked = $('#pricingPrice-filter').is(':checked');
        $('#rentTariff > tbody > tr > td.pp, #rentTariff > thead > tr > th.pp').each(function() {
            $(this).toggle(isChecked);
        });
    },

    /**
     * Affichage de la colonne "Prix de revient"
     */
    _displayCostPriceCol: function()
    {
        var isChecked = $('#costPrice-filter').is(':checked');
        $('#rentTariff > tbody > tr > td.pr, #rentTariff > thead > tr > th.pr').each(function() {
            $(this).toggle(isChecked);
        });
    },

    /**
     * Affichage des colonnes "Tranches de durée"
     */
    _displayDurationRangeCols: function(defaultFilter)
    {
        var durationRangeLabels = $('.duration-range.label');

        var cols = $('.chb-filter > input:checked').length;
        durationRangeLabels.attr('colspan', cols || 1);

        $('#rentTariff > tbody > tr > td.duration-range').toggleClass('filtered', cols == 1);

        // Filtre par défaut
        if (cols == 0 && defaultFilter)
        {
            $('#' + defaultFilter + '-filter').click();
        }
    },

    /**
     * Mise à jour des lignes
     */
    _setUpdatedRows: function(col, tariffFamilyId, durationRange, value)
    {
        var oldData = this.data[tariffFamilyId][durationRange][col];

        // Tableau des mises à jour
        if (oldData != value)
        {
            if (!this.tabUpdated[tariffFamilyId])
            {
                this.tabUpdated[tariffFamilyId] = {};
            }
            if (!this.tabUpdated[tariffFamilyId][durationRange])
            {
                this.tabUpdated[tariffFamilyId][durationRange] = {};
            }
            this.tabUpdated[tariffFamilyId][durationRange][col] = value;
        }
        else
        {
            if (this.tabUpdated[tariffFamilyId][durationRange][col])
            {
                delete this.tabUpdated[tariffFamilyId][durationRange][col];
            }
        }

        // Nettoyage du tableau des mises à jour + affichage
        $.each(this.tabUpdated, function(tariffFamilyId, durationRangeInfos) {
            $.each(durationRangeInfos, function(durationRangeId, cols) {
                if ($.isEmptyObject(cols))
                {
                    delete durationRangeInfos[durationRangeId];
                }
            });

            if ($.isEmptyObject(durationRangeInfos))
            {
                delete tariffs.tabUpdated[tariffFamilyId];
                $('#line_' + tariffFamilyId).removeClass('isUpdated');
            }
            else
            {
                $('#line_' + tariffFamilyId).addClass('isUpdated');
            }
        });

    },

};

