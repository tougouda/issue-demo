var tabSelectRequired = ['businessFamilyId', 'accountFamilyId', 'energy'];
var tabInputRequired  = ['elevation'];

/*
 * Validation du formulaire d'édition de modèle
 */
function submitForm()
{
    var form = $('form[name="manageForm"]');
    var chmp = $('#valid');

    // Vérification des champs obligatoires
    // - Ajout de la classe error pour les champs obligatoires de type "select"
    for (var i = 0 ; i < tabSelectRequired.length ; i++)
    {
        // au chargement du formulaire
        $('#' + tabSelectRequired[i]).toggleClass('error', $('#' + tabSelectRequired[i]).val() == 0);
        // à la modification du champ
        $('#' + tabSelectRequired[i]).change(function(){
            $(this).toggleClass('error', $(this).val() == 0);
        });
    }
    // - Ajout de la classe error pour les champs obligatoires de type "input text"
    for (var i = 0 ; i < tabInputRequired.length ; i++)
    {
        var id = tabInputRequired[i].replace("[", "\\[").replace("]", "\\]");

        // au chargement du formulaire
        $('#' + id).toggleClass('error', $.trim($('#' + id).val()) == '');
        // à la modification du champ
        $('#' + id).change(function(){
            $(this).toggleClass('error', $(this).val() == 0);
        });
    }

    // Nombre d'erreurs
    var countErrors = $('.error:not(div), .malformed:not(div)').length;

    if (countErrors == 0)
    {
        // Actualisation de la liste si enregistrement
        var myParent = parent;
        parent.Location.popupsManager.getPopup("machinesFamily").onclose = function() {
            myParent.LOC_View.displayLoading(true);
            myParent.document.location.reload();
        };

        chmp.val('ok');
        form.submit();
    }
}
