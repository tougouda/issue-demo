var tabInputRequired  = ['familyLabel', 'label'];

/*
 * Validation du formulaire d'édition de modèle
 */
function submitForm()
{
    var form = $('form[name="manageForm"]');
    var chmp = $('#valid');

    // Vérification des champs obligatoires
    // - Ajout de la classe error pour les champs obligatoires de type "input text"
    for (var i = 0 ; i < tabInputRequired.length ; i++)
    {
        // au chargement du formulaire
        $('#' + tabInputRequired[i]).toggleClass('error', $.trim($('#' + tabInputRequired[i]).val()) == '');
        // à la modification du champ
        $('#' + tabInputRequired[i]).change(function(){
            $(this).toggleClass('error', $(this).val() == 0);
        });
    }
    // - Ajout de la classe error pour les champs mal formatés
    $('.malformed > td > input').addClass('error');

    // Nombre d'erreurs
    var countErrors = $('.error:not(div), .malformed:not(div)').length;

    if (countErrors == 0)
    {
        // Actualisation de la liste si enregistrement
        var myParent = parent;
        parent.Location.popupsManager.getPopup("modelSubrent").onclose = function() {
            myParent.LOC_View.displayLoading(true);
            myParent.document.location.reload();
        };

        chmp.val('ok');
        form.submit();
    }
}