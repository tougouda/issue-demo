var tabSelectRequired = ['familyId', 'tariffFamilyId', 'manufacturerId'];
var tabInputRequired  = ['label'];

$(document).ready(function(){
    // Vérification du champ "Dimension de la plateforme"
    // - Exemple : 0,98 x 1,25
    //             1,20 x 1,20 x 0,95
    var regExp = /^(([0-9]+,[0-9]{2}){1}( x [0-9]+,[0-9]{2}){1,2}){0,1}$/;
    // - Chargement
    $('#platformLength').parent().parent().toggleClass('malformed', !($('#platformLength').val().match(regExp)));
    // - Au changement
    $('#platformLength').keyup(function(){
        $(this).parent().parent().toggleClass('malformed', !($(this).val().match(regExp)));
        if ($(this).val().match(regExp))
        {
            $(this).removeClass('error');
        }
    });

    $('#type\\[' + TYPE_ACCESSORY + '\\]\\[1\\]').change(checkTypes);
    $('#type\\[' + TYPE_CATEGORYB + '\\]\\[1\\]').change(checkTypes);
    $('#type\\[' + TYPE_SUBRENT + '\\]\\[1\\]').change(checkTypes);
    $('#type\\[' + TYPE_SALE + '\\]\\[1\\]').change(checkTypes);

    checkTypes();
});

function checkTypes()
{
    // Désactivation du typage "Accessoire"
    var isAccessoryDisabled = $('#type\\[' + TYPE_CATEGORYB + '\\]\\[1\\]').prop('checked') ||
                              $('#type\\[' + TYPE_SALE + '\\]\\[1\\]').prop('checked');
    $('#type\\[' + TYPE_ACCESSORY + '\\]\\[1\\]').prop('disabled', isAccessoryDisabled);

    // Désactivation du typage "Catégorie B"
    var isBDisabled = $('#type\\[' + TYPE_ACCESSORY + '\\]\\[1\\]').prop('checked') ||
                      $('#type\\[' + TYPE_SUBRENT + '\\]\\[1\\]').prop('checked') ||
                      $('#type\\[' + TYPE_SALE + '\\]\\[1\\]').prop('checked');
    $('#type\\[' + TYPE_CATEGORYB + '\\]\\[1\\]').prop('disabled', isBDisabled);

    // Désactivation du typage "Sous-location"
    var isSubrentDisabled = $('#type\\[' + TYPE_CATEGORYB + '\\]\\[1\\]').prop('checked') ||
                            $('#type\\[' + TYPE_SALE + '\\]\\[1\\]').prop('checked');
    $('#type\\[' + TYPE_SUBRENT + '\\]\\[1\\]').prop('disabled', isSubrentDisabled);
    
    // Désactivation du typage "Vente"
    var isSaleDisabled = $('#type\\[' + TYPE_ACCESSORY + '\\]\\[1\\]').prop('checked') ||
                         $('#type\\[' + TYPE_SUBRENT + '\\]\\[1\\]').prop('checked') ||
                         $('#type\\[' + TYPE_CATEGORYB + '\\]\\[1\\]').prop('checked');
    $('#type\\[' + TYPE_SALE + '\\]\\[1\\]').prop('disabled', isSaleDisabled);
}

/*
 * Validation du formulaire d'édition de modèle
 */
function submitForm()
{
    var form = $('form[name="manageForm"]');
    var chmp = $('#valid');

    // Vérification des champs obligatoires
    // - Ajout de la classe error pour les champs obligatoires de type "select"
    for (var i = 0 ; i < tabSelectRequired.length ; i++)
    {
        // au chargement du formulaire
        $('#' + tabSelectRequired[i]).toggleClass('error', $('#' + tabSelectRequired[i]).val() == 0);
        // à la modification du champ
        $('#' + tabSelectRequired[i]).change(function(){
            $(this).toggleClass('error', $(this).val() == 0);
        });
    }
    // - Ajout de la classe error pour les champs obligatoires de type "input text"
    for (var i = 0 ; i < tabInputRequired.length ; i++)
    {
        // au chargement du formulaire
        $('#' + tabInputRequired[i]).toggleClass('error', $.trim($('#' + tabInputRequired[i]).val()) == '');
        // à la modification du champ
        $('#' + tabInputRequired[i]).change(function(){
            $(this).toggleClass('error', $(this).val() == 0);
        });
    }
    // - Ajout de la classe error pour les champs mal formatés
    $('.malformed > td > input').addClass('error');

    // Nombre d'erreurs
    var countErrors = $('.error:not(div), .malformed:not(div)').length;

    if (countErrors == 0)
    {
        // Actualisation de la liste si enregistrement
        var myParent = parent;
        parent.Location.popupsManager.getPopup("model").onclose = function() {
            myParent.LOC_View.displayLoading(true);
            myParent.document.location.reload();
        };

        chmp.val('ok');
        form.submit();
    }
}