
/**
 * Initialisation de la page
 *
 * @param Object tableOptions Options du tableau Ajax
 */
function pageInit(tabOptions)
{
    // Mises à jour lors du scroll ou le redimensionnement de la fenêtre
    var scrollFunc = function()
    {
        var content = $('#locStandardContent');
        var toolbar = $('#table-toolbar-container');

        if (toolbar.parent().position().top < 0)
        {
            $('html > body').addClass('table-toolbar-fixed');

            var padding = parseInt(toolbar.css('padding-left')) + parseInt(toolbar.css('padding-right'));
            toolbar.offset({top : content.offset().top}).
                    width(content[0].clientWidth - padding);
        }
        else
        {
            $('html > body').removeClass('table-toolbar-fixed');
            toolbar.width('');
        }
    };

    $('#locStandardContent').scroll(scrollFunc);
    $(window).resize(scrollFunc);


    // Initialisation du tableau
    var table = $('#table-container').ajaxTable(tabOptions.tableOptions);
    table.oninsertcell = function (table, rowIndex, colIndex, tdEl, trEl, rowData) {

        var cfgs = table.getConfigs();
        var col = cfgs.columns[colIndex];

        switch (col.id)
        {
            case 'contract.endDate':
                $(tdEl).html('');
                var divEl =  $('<div />').addClass('endDate').html(rowData['contract.endDate']);
                if (rowData['contract.url'])
                {
                    var aEl = $('<a />').attr('target', '_blank').html(rowData['contract.code']).attr('href', rowData['contract.url']);
                    $(tdEl).append(aEl).append(divEl);
                }
                else if (rowData['contract.code'])
                {
                    $(tdEl).html(rowData['contract.code']).append(divEl);
                }
                break;

            case 'contract.isAsbestos':
                $(tdEl).html('');
                if (rowData['contract.isAsbestos'] == 1)
                {
                    var spanEl = $('<span />');
                    spanEl.addClass('asbestos');
                    spanEl.attr('title', LOC_View.getTranslation('isAsbestos'));
                    $(tdEl).append(spanEl);
                }

                break;

            case 'isOutOfPark':
                tdEl.innerHTML = '<div class="locBoolean ' + (rowData['isOutOfPark'] == 1 ? 'yes' : 'no') + '"></div>';
                break;

            case 'parkNumber':
                var tabClasses = ['locStandardParkNumber'];
                var tabTitles = [];
                if (isTelematActived && rowData['isWithTelematic'] == 1)
                {
                    tabClasses.push('special');
                    tabTitles.push(LOC_View.getTranslation('isMachineWithTelematic'));
                }
                tdEl.innerHTML = '<div class="' + tabClasses.join(' ') + '" title="' + tabTitles.join(' ') + '"><a target="_blank" href="' + rowData['url'] + '">' + rowData['parkNumberPrefix'] + rowData['parkNumber'] + '</a></div>';
                break;

            case 'state.label':
                tdEl.innerHTML = '<div class="state ' + rowData['state.id'] + '"><div>' + rowData['state.label'] + '</div></div>';
                break;

            case 'agency.id' :
                tdEl.innerHTML = rowData['agency.id'] + (rowData['visibleOnAgency.id'] ? ' > ' + rowData['visibleOnAgency.id'] : '');
                break;
        }
    };

    var updateCtrls = function (isDisabled, data) {

        // Bouton pour l'export au format Excel
        if (isDisabled || (tabOptions.excelRowsLimit && data.totalRowsCount > tabOptions.excelRowsLimit))
        {
            $('#excel-btn').addClass('disabled').
                            attr('href', '#').
                            unbind('click').
                            click(false);
        }
        else
        {
            $('#excel-btn').removeClass('disabled').
                            attr('href', data.excelUrl).
                            unbind('click');
        }

        // Bouton de réinisatialisation des filtres
        $('#reset-filters-btn').toggleClass('disabled', isDisabled);
    };

    table.onbeforeupdatedata = function () {
        updateCtrls(true, null);
    };
    table.onupdatedata = function (data) {
        updateCtrls(false, data);
    };

    table.init();


    // Bouton d'actualisation
    $('#refresh-btn').click(function() {
        table.update();
    });

    // Bouton pour revenir en haut de page
    $('#back-top-btn').click(function() {
        $('#locStandardContent').animate({scrollTop: 0}, 800);
        return false;
    });

    // Bouton d'ajout de filtres
    var toggleFilters = function() {
        $('#additional-filters').toggle(400);
        $('#more-filters-btn').toggle();
        $('#less-filters-btn').toggle();
    };
    $('#more-filters-btn').click(toggleFilters);
    $('#less-filters-btn').click(toggleFilters);

    // Bouton de réinisatialisation des filtres
    $('#reset-filters-btn').click(function() {
        table.resetFilters();
    });
}


