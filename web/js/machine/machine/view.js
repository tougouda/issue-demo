var machine = {

    _httpObj : null,

    httpRequestUrl : null,
    countryId : null,
    machineId : null,
    localeId : null,
    contractIdToUnaffect : 0,
    modificationDate : null,
    deallocateAction : null,

    number : null,
    stepNumber : null,

    tabAgencies: null,

    tabModifications : {
            'control'         : false,
            'siteBlocked'     : false,
            'outOfPark'       : false,
            'state'           : false,
            'visibility'      : false,
            'transferCreation': false,
            'transferActions' : false
        },
    hasModification : false,


    setModifications : function(action, hasModification)
    {
        // Affectation de l'action demandée
        if (action in this.tabModifications)
        {
            this.tabModifications[action] = hasModification;

            // Recalcul général
            this.hasModification = false;
            for (var key in this.tabModifications)
            {
                this.hasModification = (this.hasModification || this.tabModifications[key]);
            }
        }
    },

    /**
     * Initialisation
     */
    init : function(httpRequestUrl, machineId, localeId, initialNumber, stepNumber, tabAgencies)
    {
        this.httpRequestUrl = httpRequestUrl;
        this.machineId      = machineId;
        this.localeId       = localeId;

        this.number     = initialNumber;
        this.stepNumber = stepNumber;

        this.tabAgencies = tabAgencies;

        this._listCloneDelegated = null;

        var mySelf = this;

        // Action pour la désattribution de machine
        $('#deallocateBtn').click(function() {
            // Ouverture de la popup
            mySelf.contractIdToUnaffect = $(this).attr('data-value');
            mySelf.modificationDate = $(this).attr('data-modificationDate');
            Location.modalWindowManager.show('deallocatePopup', {contentType: 3, width: 600, height: 'auto', 'isNoCloseBtn': true});
        });
        // - Boutons de la popup de confirmation
        $('#deallocatePopup li button').click(function() {
            mySelf.deallocateAction = $(this).attr('class');
            Location.modalWindowManager.hide('deallocatePopup');

            // Validation du formulaire
            if (mySelf.deallocateAction != 'none')
            {
                //Image de chargement
                LOC_View.displayLoading(true);

                var tabContent = {
                    valid  :           'ok',
                    id     :           mySelf.contractIdToUnaffect,
                    action :           mySelf.deallocateAction,
                    modificationDate : mySelf.modificationDate
                };
                $('#valid').val(JSON.stringify(tabContent));
                $('form').submit();
            }
        });


        // Action sur la date de dernière VGP
        var lastControlDateElt = $ge('lastControlDate');
        if (lastControlDateElt)
        {
            lastControlDateElt.onchange = function()
            {
                mySelf.setModifications('control', this.value != '');

                // Activation / désactivation de la demande de transfert inter-agences
                mySelf._enableTransferActionsElt(!mySelf.hasModification);

                // Activation / désactivation de la demande de transfert inter-agences
                mySelf._enableTransferCreationElt(!mySelf.hasModification);
            };
        }

        // Action sur le bloquée sur chantier
        var isSiteBlockedElt = $ge('isSiteBlocked[1]');
        if (isSiteBlockedElt)
        {
            isSiteBlockedElt.onchange = function()
            {
                var hasChanged = (this.getAttribute('initialvalue') != this.checked);
                mySelf.setModifications('siteBlocked', hasChanged);

                // Activation / désactivation du commentaire "bloquée sur chantier"
                mySelf._enableSiteBlockedCommentElt(this.checked);

                // Activation / désactivation de la demande de transfert inter-agences
                mySelf._enableTransferActionsElt(!mySelf.hasModification);

                // Activation / désactivation de la demande de transfert inter-agences
                mySelf._enableTransferCreationElt(!mySelf.hasModification);
            };
        }

        // Action sur le bloquée sur chantier
        var isOutOfParkElt = $ge('isOutOfPark[1]');
        if (isOutOfParkElt)
        {
            isOutOfParkElt.onchange = function()
            {
                var hasChanged = (this.getAttribute('initialvalue') != this.checked);
                mySelf.setModifications('outOfPark', hasChanged);

                // Activation / désactivation de la demande de transfert inter-agences
                mySelf._enableTransferActionsElt(!mySelf.hasModification);

                // Activation / désactivation de la demande de transfert inter-agences
                mySelf._enableTransferCreationElt(!mySelf.hasModification);
            };
        }

        // Action de la liste des états
        var stateElt = $ge('state.id');
        if (stateElt)
        {
            stateElt.onchange = function()
            {
                mySelf.setModifications('state', this.value != '');

                // Activation / désactivation de la demande de transfert inter-agences
                mySelf._enableTransferActionsElt(!mySelf.hasModification);

                // Activation / désactivation de la demande de transfert inter-agences
                mySelf._enableTransferCreationElt(!mySelf.hasModification);

                // Activation / désactivation du changement de visibilité
                mySelf._enableVisibleOnAgencyActionElt(!mySelf.hasModification);
            };
        }

        // Action sur la visibilité
        var isVisibleOnAgencyElt = $ge('isVisibleOnAgency[1]');
        var visibleOnAgencyElt   = $ge('visibleOnAgency.id');
        if (isVisibleOnAgencyElt && visibleOnAgencyElt)
        {
            isVisibleOnAgencyElt.onchange = function()
            {
                var hasChanged = (this.getAttribute('initialvalue') != this.checked);
                mySelf.setModifications('visibility', hasChanged);

                // Agence de visibilité
                mySelf._enableVisibleOnAgencyElt(this.checked);

                // Activation / désactivation de la demande de transfert inter-agences
                mySelf._enableTransferActionsElt(!mySelf.hasModification);

                // Activation / désactivation de la demande de transfert inter-agences
                mySelf._enableTransferCreationElt(!mySelf.hasModification);

                // Activation / désactivation du changement d'état
                mySelf._enableStateElt(!mySelf.hasModification);
            };

            visibleOnAgencyElt.onchange = function()
            {
                var hasChanged = (this.getAttribute('initialvalue') != this.value);
                mySelf.setModifications('visibility', hasChanged);

                // Activation / désactivation de la demande de transfert inter-agences
                mySelf._enableTransferCreationElt(!mySelf.hasModification);

                // Activation / désactivation du changement d'état
                mySelf._enableStateElt(!mySelf.hasModification);
            }
        }

        // Actions des boutons de réalisation et annulation de réalisation
        var doBtn     = $ge('doBtn');
        var undoBtn   = $ge('undoBtn');
        if (doBtn && undoBtn)
        {
            doBtn.onclick = function()
            {
                if (!Location.commonObj.hasEltClass(this, 'disabled'))
                {
                    mySelf.setModifications('transferActions', true);

                    // Désactivation de l'annulation de réalisation de transfert inter-agences
                    mySelf._enableDoTransferElt(!mySelf.hasModification);

                    // Désactivation de la date de dernière VGP
                    mySelf._enableLastControlDateElt(!mySelf.hasModification);

                    // Désactivation du bloquée sur chantier
                    mySelf._enableSiteBlockedElt(!mySelf.hasModification);

                    // Désactivation du hors parc
                    mySelf._enableOutOfParkElt(!mySelf.hasModification);

                    // Désactivation du changement d'état
                    mySelf._enableStateElt(!mySelf.hasModification);

                    // Désactivation de la demande de transfert inter-agences
                    mySelf._enableTransferCreationElt(!mySelf.hasModification);

                    // Activation / désactivation du changement de visibilité
                    mySelf._enableVisibleOnAgencyActionElt(!mySelf.hasModification);

                    // Récupération de l'action
                    $ge('transferAction').value = 'do';

                    Location.viewObj.displayNotification('doTransfer', 'show', {});
                }
            };

            undoBtn.onclick = function()
            {
                if (!Location.commonObj.hasEltClass(this, 'disabled'))
                {
                    mySelf.setModifications('transferActions', true);

                    // Désactivation de l'annulation de réalisation de transfert inter-agences
                    mySelf._enableUndoTransferElt(!mySelf.hasModification);

                    // Désactivation de la date de dernière VGP
                    mySelf._enableLastControlDateElt(!mySelf.hasModification);

                    // Désactivation du bloquée sur chantier
                    mySelf._enableSiteBlockedElt(!mySelf.hasModification);

                    // Désactivation du hors parc
                    mySelf._enableOutOfParkElt(!mySelf.hasModification);

                    // Désactivation du changement d'état
                    mySelf._enableStateElt(!mySelf.hasModification);

                    // Désactivation de la demande de transfert inter-agences
                    mySelf._enableTransferCreationElt(!mySelf.hasModification);

                    // Activation / désactivation du changement de visibilité
                    mySelf._enableVisibleOnAgencyActionElt(!mySelf.hasModification);

                    // Récupération de l'action
                    $ge('transferAction').value = 'undo';

                    Location.viewObj.displayNotification('undoTransfer', 'show', {});
                }
            };
        }

        // Action du bouton d'annulation de transfert
        var cancelBtn = $ge('cancelBtn');
        if (cancelBtn)
        {
            cancelBtn.onclick = function()
            {
                if (!Location.commonObj.hasEltClass(this, 'disabled'))
                {
                    mySelf.setModifications('transferActions', true);

                    // Désactivation des actions sur le transfert inter-agences
                    mySelf._enableTransferActionsElt(!mySelf.hasModification);

                    // Désactivation de la date de dernière VGP
                    mySelf._enableLastControlDateElt(!mySelf.hasModification);

                    // Désactivation du bloquée sur chantier
                    mySelf._enableSiteBlockedElt(!mySelf.hasModification);

                    // Désactivation du hors parc
                    mySelf._enableOutOfParkElt(!mySelf.hasModification);

                    // Désactivation du changement d'état
                    mySelf._enableStateElt(!mySelf.hasModification);

                    // Désactivation de la demande de transfert inter-agences
                    mySelf._enableTransferCreationElt(!mySelf.hasModification);

                    // Activation / désactivation du changement de visibilité
                    mySelf._enableVisibleOnAgencyActionElt(!mySelf.hasModification);

                    // Récupération de l'action
                    $ge('transferAction').value = 'cancel';

                    Location.viewObj.displayNotification('doTransfer', 'hide', {});
                    Location.viewObj.displayNotification('undoTransfer', 'hide', {});
                    Location.viewObj.displayNotification('cancelTransfer', 'show', {});
                }
            };
        }

        // Action de la liste des agences et de la date de transfert inter-agences
        var destinationAgencyElt = $ge('destinationAgency.id');
        var dateElt   = $ge('transferDate');
        if (destinationAgencyElt && dateElt)
        {
            // Agence
            destinationAgencyElt.onchange = function()
            {
                var isTransferAsked = (this.value != '' || dateElt.value != '');
                mySelf.setModifications('transferCreation', isTransferAsked);

                // Date de dernière VGP
                mySelf._enableLastControlDateElt(!mySelf.hasModification);

                // Bloquée sur chantier
                mySelf._enableSiteBlockedElt(!mySelf.hasModification);

                // Hors parc
                mySelf._enableOutOfParkElt(!mySelf.hasModification);

                // Changement d'état
                mySelf._enableStateElt(!mySelf.hasModification);

                // Boutons d'action du transfert inter-agences
                mySelf._enableTransferActionsElt(!mySelf.hasModification);

                // Délégation
                mySelf._enableDelegationActionElt(mySelf.hasModification);

                // Activation / désactivation du changement de visibilité
                mySelf._enableVisibleOnAgencyActionElt(!mySelf.hasModification);
            };

            // Date
            dateElt.onchange = function()
            {
                var isTransferAsked = (destinationAgencyElt.value != '' || this.value != '');
                mySelf.setModifications('transferCreation', isTransferAsked);

                // Date de dernière VGP
                mySelf._enableLastControlDateElt(!mySelf.hasModification);

                // Bloquée sur chantier
                mySelf._enableSiteBlockedElt(!mySelf.hasModification);

                // Hors parc
                mySelf._enableOutOfParkElt(!mySelf.hasModification);

                // Changement d'état
                mySelf._enableStateElt(!mySelf.hasModification);

                // Boutons d'action du transfert inter-agences
                mySelf._enableTransferActionsElt(!isTransferAsked);

                // Activation / désactivation du changement de visibilité
                mySelf._enableVisibleOnAgencyActionElt(!mySelf.hasModification);
            };
        }

        // Action de la case à cocher de délégation
        var isDelegatedElt      = $ge('isDelegated[1]');
        var delegationAgencyElt = $ge('delegationAgency.id');
        if (isDelegatedElt && delegationAgencyElt)
        {
            // Agence
            isDelegatedElt.onchange = function()
            {
                // Agence déléguée
                mySelf._enableDelegationAgencyElt(this.checked);
            };
        }

        // "Voir plus" de l'historique
        var moreHistoryBtn = $ge('moreHistoryBtn');
        if (moreHistoryBtn)
        {
            moreHistoryBtn.onclick = function()
            {
                mySelf.number += mySelf.stepNumber;
                mySelf.getHistory(true);
            };
        }

        // "Voir tout" de l'historique
        var allHistoryBtn = $ge('allHistoryBtn');
        if (allHistoryBtn)
        {
            allHistoryBtn.onclick = function()
            {
                delete mySelf.number;
                mySelf.getHistory(true);
            };
        }

        // Chargement de l'historique des modifications
        mySelf.getHistory(false);
    },

    /**
     * Annulation de la demande de réalisation
     */
    cancelDo : function()
    {
        this.setModifications('transferActions', false);

        // Réactivation de l'annulation de réalisation de transfert inter-agences
        this._enableDoTransferElt();

        // Réactivation de la date de dernière VGP
        this._enableLastControlDateElt();

        // Réactivation du bloquée sur chantier
        this._enableSiteBlockedElt();

        // Réactivation du hors parc
        this._enableOutOfParkElt();

        // Réactivation du changement d'état
        this._enableStateElt();

        // Réactivation de la demande de transfert inter-agences
        this._enableTransferCreationElt();

        // Réactivation du changement de visibilité
        this._enableVisibleOnAgencyActionElt();

        // Réinitialisation de l'action
        $ge('transferAction').value = '';

        Location.viewObj.displayNotification('doTransfer', 'hide', {});
    },

    /**
     * Annulation de la demande d'annulation de réalisation
     */
    cancelUndo : function()
    {
        this.setModifications('transferActions', false);

        // Réactivation de l'annulation de réalisation de transfert inter-agences
        this._enableUndoTransferElt();

        // Réactivation de la date de dernière VGP
        this._enableLastControlDateElt();

        // Réactivation du bloquée sur chantier
        this._enableSiteBlockedElt();

        // Réactivation du hors parc
        this._enableOutOfParkElt();

        // Réactivation du changement d'état
        this._enableStateElt();

        // Réactivation de la demande de transfert inter-agences
        this._enableTransferCreationElt();

        // Réactivation du changement de visibilité
        this._enableVisibleOnAgencyActionElt();

        // Réinitialisation de l'action
        $ge('transferAction').value = '';

        Location.viewObj.displayNotification('undoTransfer', 'hide', {});
    },

    /**
     * Annulation de la demande d'annulation
     */
    cancelCancel : function()
    {
        this.setModifications('transferActions', false);

        // Réactivation des actions sur le transfert inter-agences
        this._enableTransferActionsElt();

        // Réactivation de la date de dernière VGP
        this._enableLastControlDateElt();

        // Réactivation du bloquée sur chantier
        this._enableSiteBlockedElt();

        // Réactivation du hors parc
        this._enableOutOfParkElt();

        // Réactivation du changement d'état
        this._enableStateElt();

        // Réactivation de la demande de transfert inter-agences
        this._enableTransferCreationElt();

        // Réactivation du changement de visibilité
        this._enableVisibleOnAgencyActionElt();

        // Réinitialisation de l'action
        $ge('transferAction').value = '';

        Location.viewObj.displayNotification('doTransfer', 'hide', {});
        Location.viewObj.displayNotification('undoTransfer', 'hide', {});
        Location.viewObj.displayNotification('cancelTransfer', 'hide', {});
    },

    /**
     * Activation/désactivation de la saisie de dernière date de VGP
     */
    _enableLastControlDateElt : function(enabled)
    {
        enabled = (typeof(enabled) === 'undefined' ? true : enabled);

        var lastControlDateElt = $ge('lastControlDate.masked');

        if (lastControlDateElt)
        {
            if (enabled && lastControlDateElt.getAttribute('possible') == 1)
            {
                lastControlDateElt.removeAttribute('disabled');
            }
            else
            {
                lastControlDateElt.disabled = true;
            }
        }
    },

    /**
     * Activation/désactivation du bloquée sur chantier
     */
    _enableSiteBlockedElt : function(enabled)
    {
        enabled = (typeof(enabled) === 'undefined' ? true : enabled);

        var isSiteBlockedElt      = $ge('isSiteBlocked[1]');

        if (isSiteBlockedElt)
        {
            if (enabled && isSiteBlockedElt.getAttribute('possible') == 1)
            {
                isSiteBlockedElt.removeAttribute('disabled');
            }
            else
            {
                isSiteBlockedElt.disabled = true;
            }
        }
        this._enableSiteBlockedCommentElt(enabled);
    },

    /**
     * Activation/désactivation du commentaire du bloquée sur chantier
     */
    _enableSiteBlockedCommentElt : function(enabled)
    {
        enabled = (typeof(enabled) === 'undefined' ? true : enabled);

        var siteBlockedCommentElt = $ge('siteBlockedComment');

        if (siteBlockedCommentElt)
        {
            if (enabled && siteBlockedCommentElt.getAttribute('possible') == 1)
            {
                siteBlockedCommentElt.removeAttribute('disabled');
            }
            else
            {
                siteBlockedCommentElt.disabled = true;
            }
        }
    },

    /**
     * Activation/désactivation du hors parc
     */
    _enableOutOfParkElt : function(enabled)
    {
        enabled = (typeof(enabled) === 'undefined' ? true : enabled);

        var isOutOfParkElt = $ge('isOutOfPark[1]');
        if (isOutOfParkElt)
        {
            if (enabled && isOutOfParkElt.getAttribute('possible') == 1)
            {
                isOutOfParkElt.removeAttribute('disabled');
            }
            else
            {
                isOutOfParkElt.disabled = true;
            }
        }
    },

    /**
     * Activation/désactivation du changement d'état
     */
    _enableStateElt : function(enabled)
    {
        enabled = (typeof(enabled) === 'undefined' ? true : enabled);

        var stateElt = $ge('state.id');

        if (stateElt)
        {
            if (enabled && stateElt.getAttribute('possible') == 1)
            {
                stateElt.removeAttribute('disabled');
            }
            else
            {
                stateElt.disabled = true;
            }
        }
    },

    /**
     * Activation/désactivation de la réalisation de transfert inter-agences
     */
    _enableDoTransferElt : function(enabled)
    {
        enabled = (typeof(enabled) === 'undefined' ? true : enabled);

        var doBtn     = $ge('doBtn');

        if (doBtn)
        {
            if (enabled && doBtn.getAttribute('possible') == 1)
            {
                Location.commonObj.removeEltClass(doBtn, 'disabled');
            }
            else
            {
                Location.commonObj.addEltClass(doBtn, 'disabled');
            }
        }
    },

    /**
     * Activation/désactivation de l'annulation de réalisation de transfert inter-agences
     */
    _enableUndoTransferElt : function(enabled)
    {
        enabled = (typeof(enabled) === 'undefined' ? true : enabled);

        var undoBtn   = $ge('undoBtn');

        if (undoBtn)
        {
            if (enabled && undoBtn.getAttribute('possible') == 1)
            {
                Location.commonObj.removeEltClass(undoBtn, 'disabled');
            }
            else
            {
                Location.commonObj.addEltClass(undoBtn, 'disabled');
            }
        }
    },

    /**
     * Activation/désactivation de l'annulation de transfert inter-agences
     */
    _enableCancelTransferElt : function(enabled)
    {
        enabled = (typeof(enabled) === 'undefined' ? true : enabled);

        var cancelBtn = $ge('cancelBtn');

        if (cancelBtn)
        {
            if (enabled && cancelBtn.getAttribute('possible') == 1)
            {
                Location.commonObj.removeEltClass(cancelBtn, 'disabled');
            }
            else
            {
                Location.commonObj.addEltClass(cancelBtn, 'disabled');
            }
        }
    },

    /**
     * Activation/désactivation des actions sur le transfert inter-agences
     */
    _enableTransferActionsElt : function(enabled)
    {
        this._enableDoTransferElt(enabled);
        this._enableUndoTransferElt(enabled);
        this._enableCancelTransferElt(enabled);
    },

    /**
     * Activation/désactivation des actions de création de transfert inter-agences
     */
    _enableTransferCreationElt : function(enabled)
    {
        enabled = (typeof(enabled) === 'undefined' ? true : enabled);

        var destinationAgencyElt = $ge('destinationAgency.id');
        var dateElt = $ge('transferDate.masked');
        var isDelegatedElt = $ge('isDelegated[1]');
        var delegationAgencyElt = $ge('delegationAgency.id');

        if (destinationAgencyElt)
        {
            destinationAgencyElt.disabled = (enabled && destinationAgencyElt.getAttribute('possible') == 1 ? false : true);
        }
        if (dateElt)
        {
            dateElt.disabled = (enabled && dateElt.getAttribute('possible') == 1 ? false : true);
        }
        // Lors de l'activation ou désactivation pour la création d'un transfert,
        // les informations de délégation sont toujours désactivées
        if (isDelegatedElt)
        {
            isDelegatedElt.disabled = true;
        }
        if (delegationAgencyElt)
        {
            delegationAgencyElt.disabled = true;
        }
    },

    /**
     * Activation/désactivation de l'agence déléguée
     */
    _enableDelegationAgencyElt : function(enabled)
    {
        enabled = (typeof(enabled) === 'undefined' ? true : enabled);

        var delegationAgencyElt = $ge('delegationAgency.id');
        var selectedAgency = delegationAgencyElt.value;

        // -- remplissage de la liste des agences dispo pour délégation
        if (!this._listCloneDelegated)
        {
            this._listCloneDelegated = delegationAgencyElt.cloneNode(true);
        }
        while (delegationAgencyElt.options[0])
        {
            delegationAgencyElt.removeChild(delegationAgencyElt.options[0]);
        }
        var index = 0;
        for (var i = 0; i < this._listCloneDelegated.options.length; i++)
        {
            var transferAgencyId = $ge('destinationAgency.id').value;
            // agence différente de l'agence de transfert
            // - et même pays que l'agence de transfert
            if (transferAgencyId != this._listCloneDelegated.options[i].value &&
                (this._listCloneDelegated.options[i].value == '' ||
                this.tabAgencies[transferAgencyId]['country.id'] == this.tabAgencies[this._listCloneDelegated.options[i].value]['country.id']))
            {
                delegationAgencyElt.appendChild(this._listCloneDelegated.options[i].cloneNode(true));
                // Si une valeur était déjà sélectionnée, on la resélectionne si elle fait toujours partie de la liste
                if (selectedAgency == this._listCloneDelegated.options[i].value)
                {
                    delegationAgencyElt.selectedIndex = index;
                }
                index++;
            }
        }

        if (delegationAgencyElt)
        {
            if (enabled && delegationAgencyElt.getAttribute('possible') == 1)
            {
                delegationAgencyElt.removeAttribute('disabled');
            }
            else
            {
                delegationAgencyElt.disabled = true;
            }
        }
    },

    /**
     * Activation/désactivation des actions sur le transfert inter-agences
     */
    _enableDelegationActionElt : function(enabled)
    {
        enabled = (typeof(enabled) === 'undefined' ? true : enabled);

        var isDelegatedElt = $ge('isDelegated[1]');

        if (isDelegatedElt)
        {
            if (enabled && isDelegatedElt.getAttribute('possible') == 1)
            {
                isDelegatedElt.removeAttribute('disabled');
            }
            else
            {
                isDelegatedElt.disabled = true;
            }
        }

        this._enableDelegationAgencyElt(!isDelegatedElt.disabled && isDelegatedElt.checked);
    },

    /**
     * Activation/désactivation de l'agence déléguée
     */
    _enableVisibleOnAgencyElt : function(enabled)
    {
        enabled = (typeof(enabled) === 'undefined' ? true : enabled);

        var visibleOnAgencyElt = $ge('visibleOnAgency.id');

        if (visibleOnAgencyElt)
        {
            if (enabled && visibleOnAgencyElt.getAttribute('possible') == 1)
            {
                visibleOnAgencyElt.removeAttribute('disabled');
            }
            else
            {
                visibleOnAgencyElt.disabled = true;
            }
        }
    },

    /**
     * Activation/désactivation des actions sur le transfert inter-agences
     */
    _enableVisibleOnAgencyActionElt : function(enabled)
    {
        enabled = (typeof(enabled) === 'undefined' ? true : enabled);

        var isVisibleOnAgencyElt = $ge('isVisibleOnAgency[1]');
        var isVisibleOnAgencyHiddenElt = $ge('isVisibleOnAgency');

        if (isVisibleOnAgencyElt)
        {
            if (enabled && isVisibleOnAgencyElt.getAttribute('possible') == 1)
            {
                isVisibleOnAgencyElt.removeAttribute('disabled');
                isVisibleOnAgencyHiddenElt.removeAttribute('disabled');
            }
            else
            {
                isVisibleOnAgencyElt.disabled       = true;
                isVisibleOnAgencyHiddenElt.disabled = true;
            }
        }

        this._enableVisibleOnAgencyElt(!isVisibleOnAgencyElt.disabled && isVisibleOnAgencyElt.checked);
    },


    /**
     * Récupération de l'historique
     */
    getHistory : function(scrollIntoView)
    {
        if (!$ge('history.result'))
        {
            return;
        }
        // Masquage du bouton "Voir plus"
        Location.commonObj.removeEltClass($ge('waitingHistory'), 'hidden');
        Location.commonObj.addEltClass($ge('moreHistoryBtn'), 'hidden');
        Location.commonObj.addEltClass($ge('allHistoryBtn'), 'hidden');

        var mySelf = this;

        // Initialisation de l'objet HTTPRequest
        this._httpObj = new Location.HTTPRequest(this.httpRequestUrl);
        this._httpObj.oncompletion = function() { mySelf.getHistoryCompletion(scrollIntoView); };
        this._httpObj.onerror = function() { mySelf.getHistoryError(); };

        // Paramètres
        this._httpObj.setVar('machineId', this.machineId);
        this._httpObj.setVar('localeId',  this.localeId);
        if (typeof this.number != 'undefined')
        {
            this._httpObj.setVar('number', this.number);
        }

        // Envoi de la requête
        this._httpObj.send();
    },


    /**
     * Mise à jour des compteurs
     */
    getHistoryCompletion : function(scrollIntoView)
    {
        // Réponse JSON
        var tabHistory = this._httpObj.getJSONResponse();

        // Réponse
        if (tabHistory)
        {
            if (!tabHistory.tabData)
            {
                tabHistory.tabData = [];
            }

            // Affichage du résultat
            this.displayResult(tabHistory.tabData)

            var moreHistoryBtn = $ge('moreHistoryBtn');
            var allHistoryBtn = $ge('allHistoryBtn');

            // On a affiché l'ensemble de l'historique
            if (tabHistory.tabData.length == tabHistory.nbTotal)
            {
                // Désactivation du bouton "Voir plus"
                if (moreHistoryBtn)
                {
                    moreHistoryBtn.getElementsByTagName('a')[0].className = 'locCtrlButton disabled';
                    moreHistoryBtn.onclick = false;
                }
                if (allHistoryBtn)
                {
                    allHistoryBtn.getElementsByTagName('a')[0].className = 'locCtrlButton disabled';
                    allHistoryBtn.onclick = false;
                }
            }

            // Réaffichage du bouton "Voir plus"
            Location.commonObj.addEltClass($ge('waiting'), 'hidden');
            Location.commonObj.addEltClass($ge('waitingHistory'), 'hidden');
            Location.commonObj.removeEltClass($ge('moreHistoryBtn'), 'hidden');
            Location.commonObj.removeEltClass($ge('allHistoryBtn'), 'hidden');

            // Scroll jusqu'au bouton
            if (scrollIntoView)
            {
                if (moreHistoryBtn)
                {
                    moreHistoryBtn.scrollIntoView(false);
                }
                if (allHistoryBtn)
                {
                    allHistoryBtn.scrollIntoView(false);
                }
            }
        }
        // Erreur
        else
        {
            this.getHistoryError();
        }
    },


    /**
     * Erreur
     */
    getHistoryError : function()
    {
        Location.commonObj.addEltClass($ge('waiting'), 'hidden');
        Location.commonObj.addEltClass($ge('waitingHistory'), 'hidden');
        Location.commonObj.addEltClass($ge('moreHistoryBtn'), 'hidden');
        Location.commonObj.addEltClass($ge('allHistoryBtn'), 'hidden');
        Location.commonObj.addEltClass($ge('history.result'), 'hidden');
        Location.commonObj.removeEltClass($ge('history.error'), 'hidden');
    },

    /**
     * Affichage des données
     */
    displayResult : function(tabData)
    {
        if (tabData)
        {
            var nbLines = tabData.length;

            if (nbLines == 0)
            {
                $ge('history.result').className = 'hidden';
                $ge('history.noresult').removeAttribute('class');
            }
            else
            {
                var tableElt = $ge('history').getElementsByTagName('table')[0];

                var tbodyElt = tableElt.getElementsByTagName('tbody')[0];
                while (tbodyElt.hasChildNodes())
                {
                    tbodyElt.removeChild(tbodyElt.firstChild);
                }

                var nbElements = tabData.length;
                var previousDate = '';
                var previousTime = '';
                for (var i = 0; i < nbElements; i++)
                {
                    var trElt = window.document.createElement('tr');

                    // Date
                    var tdElt = window.document.createElement('td');
                    if (tabData[i].date != previousDate)
                    {
                        tdElt.innerHTML = tabData[i].date;
                    }
                    trElt.appendChild(tdElt);

                    // Heure
                    var tdElt = window.document.createElement('td');
                    if (tabData[i].time != previousTime)
                    {
                        tdElt.innerHTML = tabData[i].time;
                    }
                    trElt.appendChild(tdElt);

                    // Utilisateur
                    var tdElt = window.document.createElement('td');
                    tdElt.innerHTML = tabData[i]['user.fullName'];
                    if (tabData[i]['user.state.id'] == 'GEN03')
                    {
                        tdElt.className = 'deleted';
                    }
                    trElt.appendChild(tdElt);

                    // Événement
                    var tdElt = window.document.createElement('td');
                    tdElt.innerHTML = tabData[i]['eventType.label'];
                    trElt.appendChild(tdElt);

                    // Description
                    var tdElt = window.document.createElement('td');
                    tdElt.innerHTML = tabData[i].content;
                    trElt.appendChild(tdElt);

                    tbodyElt.appendChild(trElt);

                    previousDate = tabData[i].date;
                    previousTime = tabData[i].time;
                }

                $ge('history.result').removeAttribute('class');
                $ge('history.noresult').className = 'hidden';
            }
        }
    },

    /*
     * Enregistrer les modifications sur la fiche
     */
    valid : function()
    {
        var form = window.document.forms['actionsForm'];

        var controlDateElt        = $ge('lastControlDate');
        var destinationAgencyElt  = $ge('destinationAgency.id');
        var transferDateElt       = $ge('transferDate');
        var isDelegatedElt        = $ge('isDelegated[1]');
        var delegationAgencyElt   = $ge('delegationAgency.id');
        var isSiteBlockedElt      = $ge('isSiteBlocked[1]');
        var siteBlockedCommentElt = $ge('siteBlockedComment');
        var isVisibleOnAgencyElt  = $ge('isVisibleOnAgency[1]');
        var visibleOnAgencyElt    = $ge('visibleOnAgency.id');

        if (controlDateElt)
        {
            // Vérification de la date de dernier contrôle
            if (controlDateElt.value > new Date().toJSON())
            {
                Location.commonObj.addEltClass($ge('lastControlDate.masked'), 'error');
                return false;
            }

            if (Location.commonObj.hasEltClass($ge('lastControlDate.masked'), 'error'))
            {
                return false;
            }
        }

        // Vérification des champs
        // Agence de destination et date du transfert inter-agences
        if (destinationAgencyElt && transferDateElt)
        {
            // Agence de transfert sans date
            if (destinationAgencyElt.value != '' && transferDateElt.value == '')
            {
                Location.commonObj.addEltClass($ge('transferDate.masked'), 'error');
                return false;
            }
            // Date de transfert sans agence
            if (destinationAgencyElt.value == '' && transferDateElt.value != '')
            {
                Location.commonObj.addEltClass(destinationAgencyElt, 'error');
                return false;
            }
        }

        // Agence déléguée du transfert inter-agences
        if (isDelegatedElt && delegationAgencyElt)
        {
            // Option activée mais pas d'agence sélectionnée
            if (isDelegatedElt.checked && delegationAgencyElt.value == '')
            {
                Location.commonObj.addEltClass(delegationAgencyElt, 'error');
                return false;
            }
        }

        // Bloquée sur chantier
        if (isSiteBlockedElt && siteBlockedCommentElt)
        {
            // Blocage sur chantier sans commentaire
            if (isSiteBlockedElt.checked && siteBlockedCommentElt.value.trim() == '')
            {
                Location.commonObj.addEltClass(siteBlockedCommentElt, 'error');
                return false;
            }
        }

        // Visibilité agence
        if (isVisibleOnAgencyElt && visibleOnAgencyElt)
        {
            // Option activée mais pas d'agence sélectionnée
            if (isVisibleOnAgencyElt.checked && visibleOnAgencyElt.value == '')
            {
                Location.commonObj.addEltClass(visibleOnAgencyElt, 'error');
                return false;
            }
        }

        $ge('valid').value = 'ok';

        //Image de chargement
        LOC_View.displayLoading(true);

        //Soumission du formulaire
        form.submit();

        return false;
    },

    /*
     * Envoyer le digicode par SMS
     */
    sendStartCodeSMS : function(contractId)
    {
        var form = window.document.forms['telematicsForm'];

        $ge('sendStartCodeSMS').value = contractId;

        // Image de chargement
        LOC_View.displayLoading(true);

        // Soumission du formulaire
        form.submit();

        return false;
    },

    /*
     * Désactiver la télématique
     */
    disableTelematics : function()
    {
        if (confirm(LOC_View.getTranslation('answerAreYouSureDisableTelematics')))
        {
            var form = window.document.forms['telematicsForm'];

            $ge('disableTelematics').value = 'ok';

            // Image de chargement
            LOC_View.displayLoading(true);

            // Soumission du formulaire
            form.submit();
        }

        return false;
    },

    /*
     * Suspend la télématique.
     */
    suspendTelematics : function()
    {
        if (confirm(LOC_View.getTranslation('answerAreYouSureSuspendTelematics')))
        {
            var form = window.document.forms['telematicsForm'];

            $ge('suspendTelematics').value = 'ok';

            // Image de chargement
            LOC_View.displayLoading(true);

            // Soumission du formulaire
            form.submit();
        }

        return false;
    },

    /*
     * Activer la génération des codes contrats
     */
    activateTelematicsRentContractScope : function()
    {
        if (confirm(LOC_View.getTranslation('answerAreYouSureActivateTelematicsRentContractScope')))
        {
            var form = window.document.forms['telematicsForm'];

            $ge('activateTelematicsRentContractScope').value = 'ok';

            // Image de chargement
            LOC_View.displayLoading(true);

            // Soumission du formulaire
            form.submit();
        }

        return false;
    },

    /*
     * Initialiser le boîtier télématique suite à un changement
     */
    changeTelematicsEquipment: function()
    {
        if (confirm(LOC_View.getTranslation('answerAreYouSureChangeTelematicsEquipment')))
        {
            var form = window.document.forms['telematicsForm'];

            $ge('changeTelematicsEquipment').value = 'ok';

            // Image de chargement
            LOC_View.displayLoading(true);

            //Soumission du formulaire
            form.submit();
        }
    },

    /*
     * Activation du digicode
     */
    activateDigiKey : function()
    {
        if (confirm(LOC_View.getTranslation('answerAreYouSureActivateDigiKey')))
        {
            var form = window.document.forms['telematicsForm'];

            $ge('activateDigiKey').value = 'ok';

            // Image de chargement
            LOC_View.displayLoading(true);

            // Soumission du formulaire
            form.submit();
        }

        return false;
    },

    /*
     * Désactivation du digicode
     */
    deactivateDigiKey : function()
    {
        if (confirm(LOC_View.getTranslation('answerAreYouSureDeactivateDigiKey')))
        {
            var form = window.document.forms['telematicsForm'];

            $ge('deactivateDigiKey').value = 'ok';

            // Image de chargement
            LOC_View.displayLoading(true);

            // Soumission du formulaire
            form.submit();
        }

        return false;
    },

    /*
     * Régénéréer les codes contrats
     */
    regenerateTelematicsRentContractCodes : function(contractId)
    {
        if (confirm(LOC_View.getTranslation('answerAreYouSureRegenerateTelematicsRentContractCodes')))
        {
            var form = window.document.forms['telematicsForm'];

            $ge('regenerateTelematicsRentContractCodes').value = contractId;

            // Image de chargement
            LOC_View.displayLoading(true);

            // Soumission du formulaire
            form.submit();
        }

        return false;
    }

};