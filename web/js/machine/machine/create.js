/*
 * Fonction de soumission du formulaire 
*/
function submitForm()
{
    var form = window.document.forms[0];

    var modelElt          = $ge('model.id');
    var serialNumberElt   = $ge('serialNumber');
    var controlDateElt    = $ge('control.date');
    var creationAgencyElt = $ge('agency.id');

    if (controlDateElt)
    {
        if (Location.commonObj.hasEltClass($ge('control.date.masked'), 'error'))
        {
            return false;
        }
    }

    // Vérification des champs
    // - Modèle de machine
    if (modelElt.value == '')
    {
        Location.commonObj.addEltClass(modelElt, 'error');
        Location.commonObj.addEltClass($ge('model.id.search'), 'error');
        return false;
    }

    // - Numéro de série
    if (serialNumberElt.value.trim() == '')
    {
        Location.commonObj.addEltClass(serialNumberElt, 'error');
        return false;
    }
    
    // - Date Apave
    if (controlDateElt.value == '')
    {
        Location.commonObj.addEltClass($ge('control.date.masked'), 'error');
        return false;
    }
    
    // - Agence de création
    if (creationAgencyElt.value == '')
    {
        Location.commonObj.addEltClass(creationAgencyElt, 'error');
        return false;
    }
    
    $ge('valid').value = 'ok';

    //Image de chargement
    LOC_View.displayLoading(true);

    //Soumission du formulaire
    form.submit();

    return false;
}