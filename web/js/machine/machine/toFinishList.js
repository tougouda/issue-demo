var machines = {

    list: [],
    ids: [],

    init: function(tabOptions)
    {
        var mySelf = this;

        this.list = tabOptions.list;

        // Mises à jour lors du scroll ou le redimensionnement de la fenêtre
        var scrollFunc = function()
        {
            var content = $('#locStandardContent');
            var toolbar = $('#table-toolbar-container');

            if (toolbar.parent().position().top < 0)
            {
                $('html > body').addClass('table-toolbar-fixed');

                var padding = parseInt(toolbar.css('padding-left')) + parseInt(toolbar.css('padding-right'));
                toolbar.offset({top : content.offset().top}).
                        width(content[0].clientWidth - padding);
            }
            else
            {
                $('html > body').removeClass('table-toolbar-fixed');
                toolbar.width('');
            }
        };

        $('#locStandardContent').scroll(scrollFunc);
        $(window).resize(scrollFunc);

        // Bouton pour revenir en haut de page
        $('#back-top-btn').click(function() {
            $('#locStandardContent').animate({scrollTop: 0}, 800);
            return false;
        });


        $("#table-main").bind("sortEnd", function(sorter) {
            currentSort = sorter.target.config.sortList;
        });

        // Bouton pour le mode d'affichage
        var detachedRows = null;
        $('#viewmode-btn').click(function() {

            var wrapperEl = $('#table-wrapper');
            if (!$(this).hasClass('disabled') && wrapperEl.hasClass('viewmode-full'))
            {
                wrapperEl.removeClass('viewmode-full').addClass('viewmode-checked');

                detachedRows = $('#table-main > tbody > tr:not(.selected)').detach();
                $("#table-main").trigger("update");
            }
            else if (detachedRows)
            {
                wrapperEl.removeClass('viewmode-checked').addClass('viewmode-full');

                detachedRows.appendTo('#table-main > tbody');
                detachedRows = null;
                $("#table-main").trigger("update");
            }
            return false;
        });


        // --------------
        //   Evènements
        // --------------

        $('table.basic.create > tbody > tr > td.checkbox input').change(function() {
            var checked = $(this).prop('checked');
            $(this).parents('tr').toggleClass('selected', checked);

            var id = $(this).prop('value');
            if (checked)
            {
                machines.add(id);
            }
            else
            {
                machines.remove(id);
            }
            mySelf.display();

        });

        // Bouton de prise en compte
        $('#do-btn').click(function() {

            if (!$(this).hasClass('disabled'))
            {
                if (mySelf.ids.length > 0)
                {
                    LOC_View.displayLoading(true);

                    var tabMachines = [];
                    for (var i = 0; i < mySelf.ids.length; i++)
                    {
                        var machineInfos = mySelf.list.find(function(item) { return item.id == mySelf.ids[i];});
                        if (machineInfos)
                        {
                            tabMachines.push({
                                id: mySelf.ids[i],
                                parkNumber: machineInfos.parkNumber
                            });
                        }
                    }

                    $(this).addClass('disabled');
                    $('#valid').val(JSON.stringify({
                        valid       : 'ok',
                        tabMachines : tabMachines
                    }));
                    $('form').submit();
                }
            }

        });

        this.display();
    },

    /**
     * Ajout d'une machine à créer
     */
    add : function(id)
    {
        if (this.ids.indexOf(id) == -1)
        {
            this.ids.push(id);
        }
    },


    /**
     * Suppression d'une machine à créer
     */
    remove : function(id)
    {
        var index = this.ids.indexOf(id);
        if (index != -1)
        {
            this.ids.splice(index, 1);
        }
    },

    display : function()
    {
        var mySelf = this;

        var nbUpdated = mySelf.ids.length;
        var nb = mySelf.list.length;

        var tabParkNumbers = [];

        for (var i = 0; i < mySelf.ids.length; i++)
        {
            var machineInfos = mySelf.list.find(function(item) { return item.id == mySelf.ids[i];});
            if (machineInfos)
            {
                tabParkNumbers.push(machineInfos.parkNumber);
            }
        }

        // Récapitulatifs des lignes à prendre en compte
        $('#header-updated-recap').css('visibility', (nbUpdated > 0 ? 'visible' : 'hidden')).
                                       html(tabParkNumbers.sort().join(', '));

        // Nombre de ligne à prendre en compte
        $('#do-checked').html(( nbUpdated > 0 ? '(' + nbUpdated + ')' : ''));

        // Bouton de prise en compte
        $('#do-btn').toggleClass('disabled', (nbUpdated == 0));

        // Bouton de mode d'affichage
        $('#viewmode-btn').toggleClass('disabled', (nbUpdated == 0 || nbUpdated == nb));
        if (nbUpdated == 0)
        {
            $('#table-wrapper').removeClass('viewmode-checked').addClass('viewmode-full');
        }
    }
};