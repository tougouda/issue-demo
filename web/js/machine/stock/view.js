/**
 * Définir le modèle de machine sur le contrat ou la ligne de devis active
 *
 * @param int modelId
 */
function setDocumentModelId(modelId)
{
    var documentObj = parent.getDocumentObject();
    if (documentObj)
    {
        if (documentObj.getObjectType() == "contract")
        {
            documentObj.setModelId(modelId);
        }
        else if (documentObj.getObjectType() == "estimate")
        {
            documentObj = documentObj.getActiveLineObject();
            if (documentObj)
            {
                documentObj.setModelId(modelId);
            }
        }
    }
    parent.Location.popupsManager.getPopup("viewStock").close();
}