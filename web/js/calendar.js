if (!window.Location)
{
    var Location = new Object();
}

if (!Location.commonObj) alert('Erreur Location');


/**
 * Objet Location.CalendarManager
 *
 */
Location.CalendarManager = {

    showCalendar : function(e, name, url)
    {
        if (!e) e = window.event;

        var inputEl = window.document.getElementById(name + '.masked');
        if (inputEl && !inputEl.disabled)
        {
            var width = 280;
            var height = 230;

            if (window.showModalDialog)
            {
                var myArguments = new Object();
                myArguments.pluginObj = this;
                myArguments.opener = self;
                window.showModalDialog(url, myArguments, 'status: no; scroll: no; resizable: no; center: yes; dialogWidth=' + width + ' px; dialogHeight=' + height + 'px;');
            }
            else
            {
                // Position de la fenêtre
                var dualScreenLeft = (window.screenLeft != undefined ? window.screenLeft : screen.left);
                var dualScreenTop = (window.screenTop != undefined ? window.screenTop : screen.top);

                // Taille de la fenêtre
                windowWidth = (window.innerWidth ? window.innerWidth : (document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width));
                windowHeight = (window.innerHeight ? window.innerHeight : (document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height));

                // Position de la pop-up
                var left = windowWidth / 2 - width / 2 + dualScreenLeft;
                var top = windowHeight / 2 - height / 2 + dualScreenTop;

                // Ouverture de la pop-up
                window.open(url, name, 'width=' + width + ',height=' + height + ',top=' + top + ', left=' + left + ',status=no,resizable=no,scrollbars=no');
            }
        }
        Location.commonObj.stopEventBubble(e);
    },

    setCalendar : function(name, d, m, y, pattern)
    {
        if (window.dialogArguments)
        {
            var opener = window.dialogArguments.opener;
        }
        else
        {
            var opener = window.opener;
        }

        if (pattern)
        {
            var dateObj = opener.document.getElementById(name);

            // Anciennes valeurs
            var old_date = dateObj.value;
            if (old_date)
            {
                var old_d = old_date.substr(8, 2);
                var old_m = old_date.substr(5, 2);
                var old_y = old_date.substr(0, 4);
            }
            
            // Nouvelles valeurs
            var date = y + '-' + m + '-' + d;
            var date_formatted = pattern;
            date_formatted = date_formatted.replace('%G', y).replace('%Y', y)
                                           .replace('%g', y % 100).replace('%y', y % 100);
            date_formatted = date_formatted.replace('%m', m);
            date_formatted = date_formatted.replace('%d', d);

            // Mise à jour des valeurs
            dateObj.value = date;
            opener.setMaskedInputValue(name, date_formatted);

            // Changements
            if (old_y != y || old_m != m || old_d != d)
            {
                opener.updateDateHidden(date_formatted, name, pattern);
                Location.commonObj.fireEvent(dateObj, 'change');
            }
        }
    },

    closeCalendar : function(e)
    {
        if (this._calendarPopup)
        {
            this._calendarPopup.close();
        }
    }
}
