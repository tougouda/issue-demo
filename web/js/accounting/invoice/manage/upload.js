/**
 * Fonction lancée à la fin du chargement de la page
 */
function pageInit(type, hasSucceeded, readFileUrl)
{
    // Mise à jour des indicateurs
    window.parent.updateCustomersIndicators(type, hasSucceeded, hasSucceeded, !hasSucceeded);
    
    // Lancement de la lecture du fichier clients
    if (hasSucceeded)
    {
        window.parent.readFile(type, readFileUrl);
    }
}

function pageInitSubRent(hasSucceeded, readFileUrl)
{
	// Lancement de la lecture du fichier clients
    if (hasSucceeded)
    {
        window.parent.readFileSubRent(readFileUrl);
    }
}
