/**
 * Afficher l'état d'un onglet
 *
 * @param string state Etat
 */
Location.TabBoxTab.prototype.setState = function(state)
{
    this.getTitleElement().className = state;
}


var httpObj;
var httpHistoryObj;
var httpProgressionObj;
var httpSubrentObj;
var checkProgressionFirstDelay = 1500; // Délai entre le lancement de la facturation et la première mise à jour de la progression
var checkProgressionDelay = 2000; // Délai de mise à jour de la progression


function init()
{
    $('rentNumber').onkeypress = function(event)
    {
        event = event || window.event;
        var keyCode = (event.keyCode || event.which);

        return (keyCode >= 48 && keyCode <= 57);
    };

    $('repairNumber').onkeypress = $('rentNumber').onkeypress;
}



/* CHARGEMENT DE L'HISTORIQUE */

/**
 * Récupération de l'historique des facturations
 */
function getHistory(httpRequestUrl)
{
    $('history.loading').className = '';
    
    httpHistoryObj = new Location.HTTPRequest(httpRequestUrl);
    httpHistoryObj.oncompletion = getHistoryCompletion;
    httpHistoryObj.onerror = function()
    {
        $('history.loading').className = 'hidden';
    };
    
    httpHistoryObj.send();
}

/**
 * Récupération du code HTML de l'historique des facturations
 */
function getHistoryCompletion()
{
    var tab = httpHistoryObj.getJSONResponse();
    $('history.loading').className = 'hidden';
    $('history.content').className = '';
    $('history.content').innerHTML = tab['html'];
}

/* PARTIE CONTRÔLE DES CONTRATS */

/**
 * Lancement du contrôle des contrats
 */
function checkContracts(httpRequestUrl)
{
    type = 'rent';

    var button = $(type + 'ContractsCtrl.btn');
    if (button.className.match(/disabled/))
    {
        return false;
    }

    // Masquage du bouton "Rapport"
    $('rentContractsCtrl.reportBtn').className = 'locCtrlButton hidden';
    // Désactivation des commandes de lancement de la facturation
    updateInvoicingControls('rent', 0);
    // Mise à jour des indicateurs
    updateContractsIndicators(true, 0, 0);

    httpObj = new Location.HTTPRequest(httpRequestUrl);
    httpObj.oncompletion = checkContractsCompletion;
    httpObj.onerror = function()
    {
        // Désactivation des commandes de lancement de la facturation
        updateInvoicingControls('rent', 0);
        // Mise à jour des indicateurs
        updateContractsIndicators(0, 0, 1);
    };

    httpObj.send();
}

/**
 * Récupération des informations lues dans le fichier clients
 */
function checkContractsCompletion()
{
    var tab = httpObj.getJSONResponse();
    
    // Mise à jour des indicateurs
    if (tab == 0)
    {
        // Désactivation des commandes de lancement de la facturation
        updateInvoicingControls('rent', 0);
        // Mise à jour des indicateurs
        updateContractsIndicators(0, 0, 1);
    }
    else
    {
        // (Dés)activation des commandes de lancement de la facturation
        updateInvoicingControls('rent', tab['readyToInvoice']);
        // Mise à jour des indicateurs
        updateContractsIndicators(0, tab['control'], 1);

        // Affichage du rapport
        if (tab['html']['fuelsToType'] != 'undefined')
        {
            $('contractsCtrl.fuelsToType').innerHTML = tab['html']['fuelsToType'];
        }
        else
        {
            $('contractsCtrl.fuelsToType').innerHTML = '';
        }
        if (tab['html']['contractsToStop'] != 'undefined')
        {
            $('contractsCtrl.contractsToStop').innerHTML = tab['html']['contractsToStop'];
        }
        else
        {
            $('contractsCtrl.contractsToStop').innerHTML = '';
        }

        var tabBoxes = Location.tabBoxesManager.getTabBox('contractsCtrl.report.tabs');
        var nbTabs = tabBoxes.getTabsCount();
        for (var i = 0; i < nbTabs; i++)
        {
            tabBoxes.getTab(i).setState('');
        }

        if (!tab['control'])
        {
            tabBoxes.setActiveTabIndex(0);
            tabBoxes.getTab(0).setState('error');
        }

        // Affichage du bouton "Rapport"
        $('rentContractsCtrl.reportBtn').className = 'locCtrlButton';
    }

    // Affichage du bouton de lancement des contrôles des clients
    $('rentCustomersCtrl.btn').className = 'locCtrlButton';
}

/**
 * Mise à jour des indicateurs du contrôle des contrats
 */
function updateContractsIndicators(isWaiting, hasSucceeded, enableButton)
{
    if (typeof(message) == 'undefined' || message == '')
    {
        message = '&nbsp;'
    }

    $('rentContractsCtrl.yes').className      = 'result' + (hasSucceeded && !isWaiting ? '' : ' hidden');
    $('rentContractsCtrl.no').className       = 'result' + (hasSucceeded || isWaiting ? ' hidden' : '');
    $('rentContractsCtrl.waiting').className  = 'waiting' + (isWaiting ? '' : ' hidden');
    $('rentContractsCtrl.btn').className      = 'locCtrlButton' + (enableButton ? '' : ' disabled');
}

/**
 * Mise à jour des indicateurs du contrôle des contrats
 */
function updateInvoicingControls(type, isActivated)
{
    // Désactivation du bouton de lancement des contrôles des contrats
    if ($(type.toLowerCase() + 'ContractsCtrl.btn'))
    {
        $(type.toLowerCase() + 'ContractsCtrl.btn').className = (isActivated ? 'locCtrlButton' : 'locCtrlButton disabled');
    }
    // Désactivation du bouton de lancement des contrôles des clients
    $(type.toLowerCase() + 'CustomersCtrl.btn').className = (isActivated ? 'locCtrlButton' : 'locCtrlButton disabled');
    // Désactivation de la saisie du numéro de pièce
    $(type.toLowerCase() + 'Number').readOnly = (isActivated ? '' : 'readonly');
    // Désactivation du bouton "Facturer"
    $(type.toLowerCase() + 'InvoiceBtn').className = (isActivated ? 'locCtrlButton' : 'locCtrlButton disabled');
}

/**
 * Affichage du rapport du contrôle des contrats
 */
function showContractsReport()
{
    Location.modalWindowManager.show('contractsCtrl.report', {contentType: 3, width: 700, height: 'auto'});
    document.getElementById('contractsCtrl.report.tabs').focus();
    return false;
}



/* PARTIE CONTRÔLE DES CLIENTS */

/**
 * Upload du fichier clients
 */
function upload(type)
{
    type = type.toLowerCase();

    var button = $(type + 'CustomersCtrl.btn');
    if (button.className.match(/disabled/))
    {
        return false;
    }

    var iframeName = type + 'UploadFrame';
    if (window.frames[iframeName] && window.frames[iframeName].$('file').form)
    {
        // Masquage du bouton "Rapport"
        $(type + 'CustomersCtrl.reportBtn').className = 'locCtrlButton hidden';
        
        // Mise à jour des indicateurs
        updateCustomersIndicators(type, true, 0, 0, LOC_View.getTranslation('fileSend'));
        window.frames[iframeName].$('file').form.submit();
    }
}

/**
 * Lecture du fichier clients
 */
function readFile(type, httpRequestUrl)
{
    type = type.toLowerCase();

    updateCustomersIndicators(type, 1, 1, 0, LOC_View.getTranslation('fileRead'));

    httpObj = new Location.HTTPRequest(httpRequestUrl);
    httpObj.oncompletion = function() { readFileCompletion(type); };
    httpObj.onerror = function()
    {
        // Mise à jour des indicateurs
        updateCustomersIndicators(type, 0, 0, 1, LOC_View.getTranslation('fileReadError'));
    };

    httpObj.send();
}

/**
 * Récupération des informations lues dans le fichier clients
 */
function readFileCompletion(type)
{
    type = type.toLowerCase();

    var tab = httpObj.getJSONResponse();
    
    if (tab == 0)
    {
        // Mise à jour des indicateurs
        updateCustomersIndicators(type, 0, 0, 1, LOC_View.getTranslation('fileReadError'));
    }
    else
    {
        // Mise à jour des indicateurs
        updateCustomersIndicators(type, 1, tab['customersToCreate'].length == 0, 0);
        // Mise à jour des clients à créer dans le rapport
        $('customersCtrl.customersToCreate').innerHTML = tab['html'];
        // Lancement de la mise à jour des clients dans la Gestion des locations
        updateCustomers(type, tab['updateCustomersUrl'], tab['customersToUpdate'], tab['customersToCreate'].length);
    }
}

/**
 * Mise à jour des clients dans la Gestion des locations
 */
function updateCustomers(type, httpRequestUrl, tabCustomers, nbCustomersToCreate)
{
    type = type.toLowerCase();

    updateCustomersIndicators(type, 1, nbCustomersToCreate == 0, 0, LOC_View.getTranslation('customersUpdate'));

    httpObj = new Location.HTTPRequest(httpRequestUrl);
    httpObj.oncompletion = function() { updateCustomersCompletion(type, nbCustomersToCreate); };
    httpObj.onerror = function()
    {
        // Mise à jour des indicateurs
        updateCustomersIndicators(type, 0, 0, 1, LOC_View.getTranslation('customersUpdateError'));
    };
    
    httpObj.setVar('customersToUpdate', tabCustomers);
    httpObj.send();
}

/**
 * Récupération des informations lues dans le fichier clients
 */
function updateCustomersCompletion(type, nbCustomersToCreate)
{
    type = type.toLowerCase();

    var tab = httpObj.getJSONResponse();
    
    // Mise à jour des indicateurs
    if (tab == 0)
    {
        updateCustomersIndicators(type, 0, 0, 1, LOC_View.getTranslation('customersUpdateError'));
    }
    else
    {
        updateCustomersIndicators(type, 0, nbCustomersToCreate == 0, 1);
        
        // Mise à jour des clients mis à jour dans le rapport
        if (typeof(tab['html']) != 'undefined')
        {
            $('customersCtrl.customersToUpdate').innerHTML = tab['html'];
        }
        else
        {
            $('customersCtrl.customersToUpdate').innerHTML = '';
        }
        $(type + 'CustomersCtrl.reportBtn').className = 'locCtrlButton';
    }
}

/**
 * Mise à jour des indicateurs du contrôle des clients
 */
function updateCustomersIndicators(type, isWaiting, hasSucceeded, enableButton, message)
{
    type = type.toLowerCase();

    if (typeof(message) == 'undefined' || message == '')
    {
        message = '&nbsp;'
    }
    
    $(type + 'CustomersCtrl.yes').className      = 'result' + (hasSucceeded && !isWaiting ? '' : ' hidden');
    $(type + 'CustomersCtrl.no').className       = 'result' + (hasSucceeded || isWaiting ? ' hidden' : '');
    $(type + 'CustomersCtrl.waiting').className  = 'waiting' + (isWaiting ? '' : ' hidden');
    $(type + 'CustomersCtrl.btn').className      = 'locCtrlButton' + (enableButton ? '' : ' disabled');
    $(type + 'CustomersCtrl.messages').innerHTML = message;
}

/**
 * Affichage du rapport du contrôle des clients
 */
function showCustomersReport(type)
{
    Location.modalWindowManager.show('customersCtrl.report', {contentType: 3, width: 700, height: 'auto'});
    document.getElementById(type + 'CustomersCtrl.reportBtn').onkeydown = function(e)
    {
        e = e || window.event;
        var keyCode = (e.keyCode || e.which);
        if (keyCode == 9)
        {
            if(e.preventDefault) {
                e.preventDefault();
            }
            return false;
        }
    }
    return false;
}

/*
 * PARTIE SOUS LOCATION
 */
/**
 * Upload du fichier
 */
function uploadSubRent()
{

    var iframeName = 'subRentUploadFrame';
    if (window.frames[iframeName] && window.frames[iframeName].$('file').form)
    {
        // Masquage du bouton "Rapport"
    	$('subrent.waiting').className = 'waiting';
        
        // Récupération du numéro de pièce et validation du formulaire
    	window.frames[iframeName].$('nopiece').value = $('subRentNumber').value;
        window.frames[iframeName].$('file').form.submit();
    }
}

/**
 * Lecture du fichier clients
 */
function readFileSubRent(httpRequestUrl)
{
    httpSubrentObj = new Location.HTTPRequest(httpRequestUrl);
    httpSubrentObj.oncompletion = function() { readFileSubRentCompletion(); };
    httpSubrentObj.onerror = function()
    {
    	$('subRent.errorBlock').style.display = '';
    };

    httpSubrentObj.send();
}

/**
 * Récupération des informations lues dans le fichier clients
 */
function readFileSubRentCompletion()
{
    var tab = httpSubrentObj.getJSONResponse();
    if (tab['returnSubRent'] == '' || tab['returnSubRent'] == 0)
    {
    	$('subRent.errorBlock').style.display = '';
    }
    else
    {
    	var spanLink = $('fileToImport');
    	var link     = spanLink.getElementsByTagName('a')[0];
    	link.href    = link.href + '&file=' + tab['returnSubRent'];
    	link.style.display = '';
    	$('subrent.waiting').className = 'waiting hidden';
    }
    
}



/*
 * PARTIE FACTURATION
 */

/**
 * Lancement de la facturation
 */
function invoice(type, httpRequestUrl, httpProgressionUrl)
{
    type = type.toLowerCase();

    var button = $(type + 'InvoiceBtn');
    if (button.className.match(/disabled/))
    {
        return false;
    }

    $(type + '.invoiceBtn').className = 'hidden';
    $(type + '.progression').className = '';

    httpObj = new Location.HTTPRequest(httpRequestUrl);
    httpObj.oncompletion = function() { invoiceCompletion(type, httpProgressionUrl); };
    httpObj.onerror = function()
    {
        $(type + '.invoiceBtn').className = '';
        $(type + '.progression').className = 'hidden';
        $('invoice.errorBlock').style.display = '';
    };

    httpObj.setVar('no', $(type + 'Number').value);
    httpObj.send();

    updateInvoicingControls(type, 0);
}

/**
 * Retour de la facturation
 */
function invoiceCompletion(type, httpProgressionUrl)
{
    type = type.toLowerCase();

    var tab = httpObj.getJSONResponse();
    if (!tab || tab['requestId'] == 0)
    {
        $(type + '.invoiceBtn').className = '';
        $(type + '.progression').className = 'hidden';
        $('invoice.errorBlock').style.display = '';
    }
    else
    {
        // Planification de la prochaine vérification de la progression
        setTimeout('checkProgression("' + type + '", ' + tab['requestId'] + ', "' + httpProgressionUrl + '")',
                   checkProgressionFirstDelay);
    }
}

/**
 * Vérifie la progression de la facturation
 */
function checkProgression(type, requestId, httpProgressionUrl)
{
    httpProgressionObj = new Location.HTTPRequest(httpProgressionUrl);
    httpProgressionObj.oncompletion = function() { checkProgressionCompletion(type, requestId, httpProgressionUrl); };
    httpProgressionObj.onerror = function() { };

    httpProgressionObj.setVar('requestId', requestId);
    httpProgressionObj.send();
}

/**
 * Retour de la facturation
 */
function checkProgressionCompletion(type, requestId, httpProgressionUrl)
{
    type = type.toLowerCase();

    var tab = httpProgressionObj.getJSONResponse();
    $(type + '.progression.bar').style.width = tab['progression'] + '%';

    if (tab['stateId'] == 'REQ01' || tab['stateId'] == 'REQ02')
    {
        updateInvoicingControls(type, 0);

        // Planification de la prochaine vérification de la progression
        setTimeout('checkProgression("' + type + '", ' + requestId + ', "' + httpProgressionUrl + '")',
                   checkProgressionDelay);
    }
    else
    {
        if (tab['stateId'] == 'REQ03')
        {
            var message;
            if (tab['generatedInvoicesNo'] == 1)
            {
                message = $('invoice.validBlock.singular').innerHTML;
            }
            else if (tab['generatedInvoicesNo'] >= 2)
            {
                message = $('invoice.validBlock.plural').innerHTML;
            }
            else
            {
                message = $('invoice.validBlock.none').innerHTML;
            }
            message = message.replace(/&lt;/g, '<');
            message = message.replace(/&gt;/g, '>');
            message = message.replace(/<%nb>/g, tab['generatedInvoicesNo']);
            $('invoice.validBlock.message').innerHTML = message;

            $('invoice.validBlock').style.display = '';
        }
        else
        {
            $(type + '.invoiceBtn').className = '';
            $(type + '.progression').className = 'hidden';

            $('invoice.errorBlock').style.display = '';
            if (tab['tabResult'] != null && !tab['tabResult']['readyToInvoice'])
            {
                $('invoice.errorBlock.contractsControlError').style.display = '';
            }
            else
            {
                $('invoice.errorBlock.standardError').style.display = '';
            }
        }
    }
}

/**
 * Annulation de la facturation
 */
function cancel(httpRequestUrl)
{
    $('cancel.invoiceBtn').className = 'hidden';
    $('cancel.cancelBtn').className = 'hidden';
    $('cancel.loading').className = 'waiting';

    httpObj = new Location.HTTPRequest(httpRequestUrl);
    httpObj.oncompletion = cancelCompletion;
    httpObj.onerror = function()
    {
        $('cancel.invoiceBtn').className = '';
        $('cancel.loading').className = 'waiting hidden';
    };

    httpObj.send();
}

/**
 * Retour de l'annulation
 */
function cancelCompletion()
{
    var tab = httpObj.getJSONResponse();

    Location.modalWindowManager.hide();

    if (tab['canceledInvoicesNo'] == -1)
    {
        $('cancel.errorBlock').style.display = '';
    }
    else
    {
        var message;
        if (tab['canceledInvoicesNo'] == 1)
        {
            message = $('cancel.validBlock.singular').innerHTML;
        }
        else if (tab['canceledInvoicesNo'] >= 2)
        {
            message = $('cancel.validBlock.plural').innerHTML;
        }
        else
        {
            message = $('cancel.validBlock.none').innerHTML;
        }
        message = message.replace(/&lt;/g, '<');
        message = message.replace(/&gt;/g, '>');
        message = message.replace(/<%nb>/g, tab['canceledInvoicesNo']);
        $('cancel.validBlock.message').innerHTML = message;

        $('cancel.validBlock').style.display = '';
    }
}

/**
 * Annulation d'une facturation
 */
function checkCancel(i, httpRequestUrl)
{
    $('cancel[' + i + '].btn').className     = 'locCtrlButton hidden';
    $('cancel[' + i + '].waiting').className = 'waiting';

    httpObj = new Location.HTTPRequest(httpRequestUrl);
    httpObj.oncompletion = function() { checkCancelCompletion(i); };
    httpObj.onerror = function() { };

    httpObj.send();
}

/**
 * Récupération des informations lues dans le fichier clients
 */
function checkCancelCompletion(i)
{
    var tab = httpObj.getJSONResponse();
    
    if (tab == 0)
    {
        // Mise à jour des indicateurs
        alert('Erreur dans l\'appel de la vérification de l\'annulation');
    }
    else
    {
        // Mise à jour des clients à créer dans le rapport
        $('cancel.report').innerHTML = tab['html'];
        // Affichage de la prévisualisation de l'annulation
        showCancelReport();
    }

    $('cancel[' + i + '].btn').className     = 'locCtrlButton';
    $('cancel[' + i + '].waiting').className = 'waiting hidden';
}

/**
 * Affichage du rapport du contrôle des clients
 */
function showCancelReport()
{
    Location.modalWindowManager.show('cancel.report', {contentType: 3, width: 1100, height: 'auto'});
    return false;
}
