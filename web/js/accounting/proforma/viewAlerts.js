var tabUpdates = [];


// Objet pour gérer le compteur de réactualisation automatique
var refreshTimeObj = {

    _delay: 0,
    _count: -1,
    _timer: null,
    _divEl: null,

    display : function ()
    {
        this._divEl.style.display = (this._timer ? '' : 'none');
        this._divEl.lastChild.innerHTML = (this._count == 0 ? '---' : (this._count < 10 ? '0' : '') + this._count + '"');
        var p = this._count * 100 / this._delay;
        if (p > 0 && p < 35)
        {
            $(this._divEl.childNodes).fadeOut(250).fadeIn(250);
        }
    },

    stop : function()
    {
        if (this._timer)
        {
            window.clearTimeout(this._timer);
            this._timer = null;
            this._count = -1;
            this.display();
        }
    },

    start : function()
    {
        if (this._delay > 0 && !this._timer)
        {
            this._count = this._delay;

            var mySelf = this;
            this._timer = window.setInterval(function() {

                if (mySelf._count > 0)
                {
                    mySelf._count--;
                    mySelf.display();
                }
                if (mySelf._count == 0)
                {
                    window.clearTimeout(mySelf._timer);
                    mySelf._timer = null;
                    LOC_View.reload();
                }

            }, 1000);

            this.display();
        }
    },

    init : function(tabOptions)
    {
        this._divEl = window.document.createElement('DIV');
        this._divEl.id = 'refreshTimeDiv';
        var el = window.document.createElement('SPAN');
        el.className = 'title';
        el.innerHTML = tabOptions.title;
        this._divEl.appendChild(el);
        var el = window.document.createElement('SPAN');
        el.className = 'count';
        this._divEl.appendChild(el);

        $ge('locStandardFooter').appendChild(this._divEl);

        this._delay = tabOptions.delay;
    }
};


/*
 * Fonction de soumission du formulaire 
*/
function submitForm()
{
    var form = window.document.forms[0];
    $ge('valid').value = 'ok';

    //Image de chargement
    LOC_View.displayLoading(true);

    //Soumission du formulaire
    form.submit();

    return false;
}


function setUpdatesRow(proformaId, tabRefValue)
{
    var rowEl = $ge('proforma_' + proformaId);

    // Récupération des valeurs des champs
    var isJustified  = $('#proforma_' + proformaId + ' input[type=checkbox][name*="isJustified"]').is(':checked');
    var isValidated  = $('#proforma_' + proformaId + ' input[type=checkbox][name*="isValidated"]').is(':checked');
    var validComment = $('#proforma_' + proformaId + ' textarea[name*="validComment"]').val();
    var encashmentId = $('#proforma_' + proformaId + ' select[name*="encashment.id"]').val();
    var closureId    = $('#proforma_' + proformaId + ' select[name*="closure.id"]').val();

    var remainDueEl  = $('#proforma_' + proformaId + ' input[type=text][name*="remainDue"]');
    remainDue = (remainDueEl.val() == '' ? 0 : parseFloat(remainDueEl.val()));
    var remainDueRef = (tabRefValue.remainDue == '' ? 0 : parseFloat(tabRefValue.remainDue));
    var total = parseFloat(tabRefValue.total);

    if (remainDue > 0 && remainDue < total)
    {
        remainDueEl.removeClass('error');
    }
    else
    {
        remainDueEl.addClass('error');
    }

    rowEl.className = rowEl.className.replace(/encashment_[(a-zA-Z)*3(0-9)*2]+/, 'encashment_' + encashmentId);
    $(rowEl).removeClass('validate');
    if (isValidated)
    {
        $(rowEl).addClass('validate');
    }

    if (isJustified != tabRefValue.isJustified ||
        isValidated != tabRefValue.isValidated ||
        (isValidated && validComment != tabRefValue.validComment) ||
        encashmentId != tabRefValue.encashmentId ||
        (encashmentId == 'ECM03' && closureId != tabRefValue.closureId) ||
        (encashmentId == 'ECM04' && remainDue != remainDueRef))
    {
        $(rowEl).addClass('isUpdate');
        $(rowEl).removeClass('isError');
        $(rowEl).removeClass('isUpdated');
        $('#proforma_' + proformaId + ' input[type=hidden][name*="toUpdate"]').val(1);

        array_push_unique(tabUpdates, proformaId);
        refreshTimeObj.stop();
    }
    else
    {
        $(rowEl).removeClass('isUpdate');
        $('#proforma_' + proformaId + ' input[type=hidden][name*="toUpdate"]').val(0);
        if (tabRefValue.state != '')
        {
            $(rowEl).addClass(tabRefValue.state);
        }

        var pos = array_search(proformaId, tabUpdates);
        if (pos !== false)
        {
            tabUpdates.splice(pos, 1);
            if (tabUpdates.length == 0)
            {
                refreshTimeObj.start();
            }
        }
    }
    rowEl.className = rowEl.className.replace(/^\s*/, '').replace(/\s*$/, '').replace(/\s\s+/g, ' ');
}
