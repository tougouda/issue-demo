var httpObj;
var timeObj;


function init(httpRequestUrl)
{
    getAlerts(httpRequestUrl);
}



/**
 * Récupération des compteurs
 */
function getAlerts(httpRequestUrl)
{
    // Affichage de l'animation d'attente
    displayWaitBlock();

    // Initialisation de l'objet HTTPRequest
    httpObj = new Location.HTTPRequest(httpRequestUrl);
    httpObj.oncompletion = getAlertsCompletion;
    httpObj.onerror = getAlertsError;

    // Envoi de la requête
    httpObj.send();

    // Re-planification de la récupération des compteurs pour dans 5 minutes
    if (timeObj)
    {
        clearTimeout(timeObj);
    }
    timeObj = setTimeout('getAlerts("' + httpRequestUrl + '")', 5 * 60 * 1000);
}

/**
 * Mise à jour des compteurs
 */
function getAlertsCompletion()
{
    // Réponse JSON
    var tabControl = httpObj.getJSONResponse();

    if (tabControl)
    {
        // Mise à jour des compteurs
        for (var control in tabControl)
        {
            var sum = 0;
            var number = 0;
            for (var agencyId in tabControl[control])
            {
                sum += parseInt(tabControl[control][agencyId]);
                number++;
            }

            var average = sum / number;
            for (var agencyId in tabControl[control])
            {
                var alertObj = $ge(control + '_alert_' + agencyId);
                var numberObj = $ge(control + '_number_' + agencyId);
                
                if (alertObj && numberObj)
                {
                    // Étoile clignotante si le compteur n'est pas à 0
                    if (tabControl[control][agencyId] > average / 2)
                    {
                        alertObj.className = 'alert hot';
                    }
                    else
                    {
                        alertObj.className = 'alert';
                    }
                    // Mise à jour du nombre
                    numberObj.innerHTML = tabControl[control][agencyId];
                }
            }
        }

        // Masquage de l'animation de chargement
        hideWaitBlock();
    }
    else
    {
        getAlertsError();
    }
}

/**
 * Erreur
 */
function getAlertsError()
{
    // Vidage des compteurs
    clearAlerts();
    
    // Affichage du panneau d'erreur
    displayErrorBlock();
}

/**
 * Affichage de l'animation d'attente
 */
function displayWaitBlock()
{
    var tabObj = document.getElementsByName('loading');

    var nbObj = tabObj.length;
    for (var i = 0; i < nbObj; i++)
    {
        tabObj[i].className = 'loading';
    }
}

/**
 * Affichage de l'animation d'attente
 */
function displayErrorBlock()
{
    var tabObj = document.getElementsByName('loading');
    
    var nbObj = tabObj.length;
    for (var i = 0; i < nbObj; i++)
    {
        tabObj[i].className = 'loading error';
    }
}

/**
 * Affichage de l'animation d'attente
 */
function hideWaitBlock()
{
    var tabObj = document.getElementsByName('loading');
    
    var nbObj = tabObj.length;
    for (var i = 0; i < nbObj; i++)
    {
        tabObj[i].className = 'loading hidden';
    }
}

/**
 * Vidage des compteurs
 */
function clearAlerts()
{
    var tabObj = document.getElementsByName('alert');
    var nbObj = tabObj.length;

    for (var i = 0; i < nbObj; i++)
    {
        tabObj[i].className = 'alert';
    }

    tabObj = document.getElementsByName('number');
    nbObj = tabObj.length;
    
    for (var i = 0; i < nbObj; i++)
    {
    	tabObj[i].innerHTML = '';
    }
}
