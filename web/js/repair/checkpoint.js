var httpObj1;
var curSearchRowNum = -1;
var firstLoad = -1;

function setFirstLoad(v)
{
    firstLoad = v;
}

function changeFixAllowed(obj)
{
    if (obj.checked)
    {
        document.getElementById('spanFixCost').style.visibility = 'visible';
    }
    else
    {
        document.getElementById('spanFixCost').style.visibility = 'hidden';
        Location.currenciesManager.getCurrency('fixCost').setValue(0);
    }
}

function changeRepairAllowed(obj)
{
    if (obj.checked)
    {
        $('spanRepairCost').style.visibility = 'visible';
    }
    else
    {
        $('spanRepairCost').style.visibility = 'hidden';
        Location.currenciesManager.getCurrency('repairCost').setValue(0);
    }
}

function changeIsChgPartAllowed(obj)
{
    if (obj.checked)
    {
        $('spanIsArticleRequired').style.visibility = 'visible';
        LOC_Common.fireEvent('cbIsArticleRequired', 'click');
        
        $('spanIsMarginAllowed').style.visibility = 'visible';
        if (firstLoad == 1)
        {
            $('cbIsMarginAllowed').checked = true;
            setFirstLoad(0);
        }
    }
    else
    {
        $('spanIsArticleRequired').style.visibility = 'hidden';
        $('cbIsArticleRequired').checked = false;
        
        $('spanIsMarginAllowed').style.visibility = 'hidden';
        $('cbIsMarginAllowed').checked = false;
        
        setArticlesActivation(-1);
    }
    
}

function changeIsArticleRequired(obj)
{
    if (obj.checked)
    {
        setArticlesActivation(-1);
    }
    else
    {
        setArticlesActivation(1);
    }
}

function setArticlesActivation(mode)
{
    if (mode > 0)
    {
        $('articlesDisabled').style.display = 'none';
    }
    else
    {
        $('articlesDisabled').style.display = 'block';
    }
}

function addRowArticle()
{
    var table = document.getElementById('tableArticle');
    
    if (table != null)
    {
        var newRowNum = table.rows.length - 3; // n° prochaine ligne
        var cntCols = table.rows[0].cells.length; // nombre de colonnes

        // nouveau tr
        var row = table.insertRow(table.rows.length - 2);
        row.setAttribute('id', 'trArticle_' + newRowNum);

        i = 0;

        // nouveau td : code article
        var cell = row.insertCell(i++);
        cell.setAttribute('id', 'tdArticleCode_' + newRowNum);
        node = $('tabArticleCode_blank').cloneNode(true);
        node.setAttribute('id', 'tabArticle[' + newRowNum + '][code]');
        node.setAttribute('name', 'tabArticle[' + newRowNum + '][code]');
        node.onchange = Function('searchArticle(' + newRowNum + ')');
        cell.appendChild(node);

        // nouveau td : prix article
        var cell = row.insertCell(i++);
        cell.setAttribute('id', 'tdArticleCost_' + newRowNum);
        cell.setAttribute('style', 'text-align: right;');
        node = $('tabArticlePurchaseCost_blank').cloneNode(true);
        node.setAttribute('id', 'tabArticle[' + newRowNum + '][purchaseCost]');
        node.setAttribute('name', 'tabArticle[' + newRowNum + '][purchaseCost]');
        cell.appendChild(node);

        Location.currenciesManager.cloneCurrency('tabArticle[' + newRowNum + '][purchaseCost]',
                                                 'tabArticlePurchaseCost_blank');

        // nouveau td : bouton suppression
        var cell = row.insertCell(i++);
        cell.setAttribute('id', 'tdArticleRbtn_' + newRowNum);
        cell.setAttribute('style', 'text-align: right;');
        node = document.getElementById('spanArticleRbtn').getElementsByTagName('a')[0].cloneNode(true);
        node.onclick = Function('removeRowArticle(' + newRowNum + ')');
        cell.appendChild(node);

        // champ caché id externe article
        node = $('tabArticleExternalId_blank').cloneNode(true);
        node.setAttribute('id', 'tabArticle[' + newRowNum + '][externalId]');
        node.setAttribute('name', 'tabArticle[' + newRowNum + '][externalId]');
        document.getElementById('chpHiddens').appendChild(node);
    }
}

function removeRowArticle(rowNum)
{
    var obj;
    
    if (confirm(LOC_View.getTranslation('confirmRemoveArticle')))
    {
        if ($('tabArticle[' + rowNum + '][id]'))
        {
            document.getElementById('tdArticleCode_' + rowNum).style.textDecoration = 'line-through';
            document.getElementById('tdArticleCost_' + rowNum).style.textDecoration = 'line-through';
            document.getElementById('tdArticleRbtn_' + rowNum).getElementsByTagName('a')[0].style.visibility = 'hidden';
        }
        else
        {
            document.getElementById('trArticle_' + rowNum).style.display = 'none';
        }

        if (obj = $('tabArticle[' + rowNum + '][id]'))
        {
            obj.disabled = true;
        }
        if (obj = $('tabArticle[' + rowNum + '][externalId]'))
        {
            obj.disabled = true;
        }
        if (obj = $('tabArticle[' + rowNum + '][code]'))
        {
            obj.disabled = true;
        }
        if (obj = Location.currenciesManager.getCurrency('tabArticle[' + rowNum + '][purchaseCost]'))
        {
            obj.disable();
        }
    }

    return false;
}

function searchArticle(rowNum)
{
    if (!httpObj1)
    {
        httpObj1 = new Location.HTTPRequest($('urlSearchArticle').value);
        httpObj1.oncompletion = searchArticleCompletion;
    }

    obj = $('tabArticle[' + rowNum + '][code]');
    if (obj.value != '')
    {
        curSearchRowNum = rowNum;
        httpObj1.setVar('countryId', $('countryId').value);
        httpObj1.setVar('articleCode', obj.value);
        httpObj1.send();
    }
}

function searchArticleCompletion()
{
    var tab = httpObj1.getJSONResponse();

    rowNum = curSearchRowNum;
    curSearchRowNum = -1;
    if (tab['articles']['id'] != null)
    {
        if (rowNum >= 0)
        {
            $('tabArticle[' + rowNum + '][code]').value = tab['articles']['code'];
            $('tabArticle[' + rowNum + '][externalId]').value = tab['articles']['id'];
            var purchaseCostInput = Location.currenciesManager.getCurrency('tabArticle[' + rowNum + '][purchaseCost]');
            purchaseCostInput.setValue(tab['articles']['localCost']);
        }
    }
    else
    {
        $('tabArticle[' + rowNum + '][code]').value = '';
        $('tabArticle[' + rowNum + '][externalId]').value = '';
        Location.currenciesManager.getCurrency('tabArticle[' + rowNum + '][purchaseCost]').setValue('');
        alert(LOC_View.getTranslation(tab['errorMsgs']));
    }
}

function submitForm()
{
    var form = document.getElementsByTagName('form')[0];

    $('valid').value = 'ok';
    form.submit();
}

function resetForm()
{
    var form = document.getElementsByTagName('form')[0];

    form.reset();
}

function refresh()
{
    var form = document.getElementsByTagName('form')[0];

    if ($('valid'))
    {
        $('valid').value = '';
    }
    form.submit();
}

function showHideDeleteButton(cbName)
{
    var checked = 0;    

    var cbList = window.document.getElementsByTagName('input');
    var i = 0;
    var exp = new RegExp('^' + cbName + '$|^' + cbName + '\\[(.*)\\]$');
    for (i = 0; i < cbList.length; i++)
    {
        if (cbList[i].getAttribute('type') == 'checkbox' &&
            exp.test(cbList[i].getAttribute('name')) &&
            !cbList[i].disabled &&
            cbList[i].checked)
        {
            checked = 1;
            break;
        }
    }

    if (checked == 1)
    {
        $('deleteBtn').style.visibility = 'visible';
    }
    else
    {
        $('deleteBtn').style.visibility = 'hidden';
    }
}