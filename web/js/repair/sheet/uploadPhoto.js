var photoCpt = 0;

/*
 * Fonction d'ajout de la photo dans la vue 
 */

function upload(form)
{
    //On selectionne le container et on creer un div pour l'image
    var imagesContainer = $ge('imagesContainer');
    var newImageDiv = document.createElement('div');
    newImageDiv.id = 'image[' + photoCpt + ']';

    //reinitialisation du div erreur
    var errorDiv = $ge('error');
    errorDiv.innerHTML = "";
    errorDiv.style.display = 'none';

    //gif d'attente
    var waitImage = document.createElement('img');
    waitImage.src = '../web/img/loading2.gif';
    newImageDiv.appendChild(waitImage);
    imagesContainer.appendChild(newImageDiv);

    window.setTimeout(function () {form.submit();}, 1500);
}


/*
 * Fonction d'affichage de l'image uploadée (dans le bloc imagesContainer)
 */
function setUploadedImage(imgSrc, fileTempName) 
{
    addImage(imgSrc, fileTempName, true, null);
}


function addImage(imgSrc, imgName, isNew, rights)
{
    // Selection du container
    var images = $ge('imagesContainer');

    // Suppression image attente
    if (isNew)
    {
        var imgDiv = $ge('image[' + photoCpt + ']');
        var image = imgDiv.getElementsByTagName('img')[0];
        imgDiv.removeChild(image);
    }
    else
    {
        var imgDiv = document.createElement('div');
        imgDiv.id = 'image[' + photoCpt + ']';
    }
    

    var thumbImgSrc;
    if (imgName.substr(0, 5) != 'thumb_')
    {
        thumbImgSrc = imgSrc.substr(0, imgSrc.lastIndexOf("/") +1) + 'thumb_' + imgName.substr(0, imgName.lastIndexOf(".")) + '.png';
    }
    else
    {
        thumbImgSrc = imgSrc.substr(0, imgSrc.lastIndexOf("/") + 1) +  imgName.substr(0, imgName.lastIndexOf(".")) + '.png';
    }

    // Insertion croix suppression image
    if (isNew || rights.match(/^(|.*\|)delete(|\|.*)$/))
    {
        var removeElement = document.createElement('a');
        removeElement.href = '#';
        removeElement.onclick = Function('deleteImage("' + LOC_View.getTranslation('confirmRemovePhoto') + '", ' + photoCpt + ');');
        imgDiv.appendChild(removeElement);
    }

    // Apercu de l'image
    var newImage = document.createElement('img');
    newImage.src = thumbImgSrc;
    newImage.style.height = '48px';
    newImage.style.width = 'auto';
    newImage.className = 'image';
    newImage.onclick = Function('popupImage("'+ imgSrc + '");');
    imgDiv.appendChild(newImage);

    // Champ caché image[id]
    var imageHidden = document.createElement('input');
    imageHidden.id = 'image[' + photoCpt + '][name]';
    imageHidden.name = 'image[' + photoCpt + '][name]';
    imageHidden.value = imgName;
    imageHidden.type = 'hidden';
    imgDiv.appendChild(imageHidden);

    // Champ caché savedImage[id]
    var savedImageHidden = document.createElement('input');
    savedImageHidden.id = 'image[' + photoCpt + '][saved]';
    savedImageHidden.name = 'image[' + photoCpt + '][saved]';
    savedImageHidden.value = (isNew ? 0 : 1);
    savedImageHidden.type = 'hidden';
    imgDiv.appendChild(savedImageHidden);

    if (!isNew)
    {
        // Case à cocher
        var checkElement = document.createElement('input');
        checkElement.type = 'checkbox';
        checkElement.name = 'imageCb[]';
        checkElement.setAttribute('imgName', imgName);
        checkElement.value = 0;
        checkElement.onclick = function()
        {
            var zipBtnEl = $ge('zipBtn');
            if (zipBtnEl)
            {
                var tabPhotos = [];
                var tabCbs = document.getElementsByName('imageCb[]');
                for (var i = 0; i < tabCbs.length; i++)
                {
                    if (tabCbs[i].checked)
                    {
                        tabPhotos.push(tabCbs[i].getAttribute('imgName'));
                    }
                }
                //Affichage du bouton Zip si au moins une photo est cochée
                if (tabPhotos.length > 0)
                {
                    zipBtnEl.style.visibility = 'visible';
                    zipBtnEl.href = zipBtnEl.href.replace(/\&photos\=.*/, '&photos=' + encodeURIComponent(JSON.stringify(tabPhotos)));
                }
                else
                {
                    zipBtnEl.style.visibility = 'hidden';
                }
            }
        }
        imgDiv.appendChild(checkElement);
    }
    images.appendChild(imgDiv);

    // On ajoute les elements aux container
    photoCpt++;
}



/*
 * Fonction de téléchargement des photos 
 *
*/
function loadImages(imageTab, rights)
{
    //  selection du container
    var images = $ge('imagesContainer');

    //La fonction parcourt le tableau d'image encodé en JSON 
    for (var i in imageTab)
    {
        var imgSrc = imageTab[i];
        var imgName = imgSrc.substr(imgSrc.lastIndexOf("/") + 1, imgSrc.length);
        addImage(imgSrc, imgName, false, rights);
    }
}


/*
 * Fonction qui gère les erreurs d'upload
 * 
*/
function uploadError(errorCode, fileName)
{
    var images = document.getElementById('imagesContainer');
    var imgDiv = document.getElementById('image[' + photoCpt + ']');
    
    images.removeChild(imgDiv);
    
    var errorDiv = document.getElementById('error');
    
    
    if (errorCode == 0)
    {
        errorDiv.innerHTML = fileName + " : " + LOC_View.getTranslation('errorAddPhoto0') + "";
    }
    else if (errorCode == 1)
    {
        errorDiv.innerHTML = fileName + " : " + LOC_View.getTranslation('errorAddPhoto1') + "";
    }
    else if (errorCode == 2)
    {
        errorDiv.innerHTML = fileName + " : " + LOC_View.getTranslation('errorAddPhoto2') + "";
    }
    else if (errorCode == 3)
    {
        errorDiv.innerHTML = LOC_View.getTranslation('errorAddPhoto3') + "";
    }
    else
    {
        errorDiv.innerHTML = LOC_View.getTranslation('errorAddPhoto1');
    }
    
    errorDiv.style.display = '';
}

//Affichage de l'image sur le clic
function popupImage(imgSrc) 
{
    LOC_View.openWindow('image', imgSrc, 's');
}

//Suppression de l'image
function deleteImage(confirmMsg, blockId)
{
    if (confirm(confirmMsg))
    {  
        var image = $ge('image[' + blockId + ']');
        image.className = 'deleted';
        //Suppression de l'appel de la fonction popupImage() sur l'évènement onClick
        image.getElementsByTagName('img')[0].onclick = 'return false;';

        var saved = $ge('image[' + blockId + '][saved]').value;

//      On associe la valeur -1 au champ hidden saved, pour signaler la suppression
        $ge('image[' + blockId + '][saved]').value = -1;
    }
}

//Gestion du bouton d'ajout
function manageBtn(fileInput, addBtn)
{
    if (fileInput.value == '')
    {
        addBtn.style.visibility = 'hidden';
    }
    else
    {
        addBtn.style.visibility = 'visible';
    }
}