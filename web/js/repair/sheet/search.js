var search = {

    _httpObj : null,

    httpRequestUrl : null,
    countryId : null,
    tabFilters: null,

    offset : null,
    stepNumber : null,
    count: 0,

    /**
     * Initialisation
     */
    init : function(httpRequestUrl, tabFilters, tabData, initialOffset, stepNumber)
    {
        this.httpRequestUrl = httpRequestUrl;
        this.tabFilters      = tabFilters;

        this.offset     = initialOffset;
        this.stepNumber = stepNumber;

        var mySelf = this;

        // "Voir plus" de résultats
        var moreResultBtn = $ge('search-more');
        if (moreResultBtn)
        {
            moreResultBtn.onclick = function()
            {
                mySelf.offset += mySelf.stepNumber;
                mySelf.getResult(true);
            };
        }

        // Chargement des contrats
        var tfootElt = $ge('sheetsList').getElementsByTagName('tfoot')[0];
        Location.commonObj.toggleEltClass(tfootElt, 'no-filters', (Object.keys(this.tabFilters).length == 0));
        mySelf.displayResult(tabData);
    },

    /**
     * Récupération de l'historique
     */
    getResult : function(scrollIntoView)
    {
        var mySelf = this;
        var tableElt = $ge('sheetsList');
        var tfootElt = tableElt.getElementsByTagName('tfoot')[0];
        tfootElt.className = '';

        Location.commonObj.addEltClass(tfootElt, 'pending-search');

        // Initialisation de l'objet HTTPRequest
        this._httpObj = new Location.HTTPRequest(this.httpRequestUrl);
        this._httpObj.oncompletion = function() { mySelf.getResultCompletion(scrollIntoView); };
        this._httpObj.onerror = function() { mySelf.getResultError(); };

        // Paramètres
        this._httpObj.setVar('tabFilters', JSON.stringify(this.tabFilters));
        this._httpObj.setVar('offset', this.offset);
        this._httpObj.setVar('stepNumber', this.stepNumber);

        // Envoi de la requête
        this._httpObj.send();
    },


    /**
     * Mise à jour des compteurs
     */
    getResultCompletion : function(scrollIntoView)
    {
        // Réponse JSON
        var tabResult = this._httpObj.getJSONResponse();

        // Réponse
        if (tabResult)
        {
            // Affichage du résultat
            this.displayResult(tabResult);
        }
        // Erreur
        else
        {
            this.getResultError();
        }
    },


    /**
     * Erreur
     */
    getResultError : function()
    {
        var tfootElt = $ge('sheetsList').getElementsByTagName('tfoot')[0];
        Location.commonObj.removeEltClass(tfootElt, 'pending-search');
        Location.commonObj.addEltClass(tfootElt, 'error');
    },

    /**
     * Affichage des données
     */
    displayResult : function(tabData)
    {
        if (tabData)
        {
            var tabResult = tabData.result;
            var nbElements = tabResult.length;

            this.count += nbElements;

            var total = tabData.total;

            var tableElt = $ge('sheetsList');
            var tfootElt = tableElt.getElementsByTagName('tfoot')[0];
            var tbodyElt = tableElt.getElementsByTagName('tbody')[0];

            if (this.count > 0)
            {
                if (this.count == 1)
                {
                    var translation = LOC_View.getTranslation('label-result-s');
                    $ge('msg-ok').innerHTML = translation;
                    Location.commonObj.addEltClass($ge('msg-ok'), 'active');
                }
                else
                {
                    var translation = LOC_View.getTranslation('label-result-p');

                    $ge('msg-ok').innerHTML = translation.replace(/<%nb>/, this.count).replace(/<%total>/, total);
                    Location.commonObj.addEltClass($ge('msg-ok'), 'active');

                    if (total > this.count)
                    {
                        var min = Math.min(this.stepNumber, total - this.count);
    
                        var translation = LOC_View.getTranslation('label-see-more-' + (min > 1 ? 'p' : 's'));
    
                        $ge('search-more').getElementsByTagName('td')[0].innerHTML = translation.replace(/<%step>/, min);
                        Location.commonObj.addEltClass(tfootElt, 'more');
                    }
                }
            }
            else
            {
                Location.commonObj.addEltClass(tfootElt, 'no-result');
            }
            Location.commonObj.removeEltClass(tfootElt, 'pending-search');

            for (var i = 0; i < nbElements; i++)
            {
                var trElt = window.document.createElement('tr');
                Location.commonObj.addEltClass(trElt, (i % 2 == 0 ? 'odd' : 'even'));

                // Ligne de démarcation - nouveaux résultats
                if (tbodyElt.childElementCount > 0 && i == 0)
                {
                    Location.commonObj.addEltClass(trElt, 'separator');
                }

                trElt.dataset.href = tabResult[i].url;

                trElt.onclick = function() {
                    window.open(this.dataset.href, '_blank');
                };

                // Numéro de fiche
                var tdElt = window.document.createElement('td');
                tdElt.innerHTML = tabResult[i].id;
                trElt.appendChild(tdElt);

                // Client
                var tdElt = window.document.createElement('td');
                tdElt.innerHTML = tabResult[i].customerName;
                trElt.appendChild(tdElt);

                // Contrat
                var tdElt = window.document.createElement('td');
                tdElt.innerHTML = tabResult[i].contractCode;
                trElt.appendChild(tdElt);

                // Machine
                var tdElt = window.document.createElement('td');
                tdElt.innerHTML = tabResult[i].machineParkNumber;
                trElt.appendChild(tdElt);

                // État
                var tdElt = window.document.createElement('td');
                var stateLabel = LOC_View.getTranslation('state_' + tabResult[i].stateId);
                tdElt.innerHTML = stateLabel;
                trElt.appendChild(tdElt);

                // Flag
                var tdElt = window.document.createElement('td');
                if (tabResult[i].flag)
                {
                    var flagLabel = LOC_View.getTranslation('flag_' + tabResult[i].flag);
                    tdElt.innerHTML = flagLabel;
                }
                trElt.appendChild(tdElt);

                tbodyElt.appendChild(trElt);
            }
        }
    }

};