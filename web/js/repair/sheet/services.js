if (!window.Location)
{
    var Location = new Object();
}

/**
 * Objet de gestion des services
 */
Location.RepairSheet_Services = function(mainObj, tabServices, tabTempServices, tabRefServices)
{
    this._mainObj = mainObj;
    this._tabList = [];

    this._tabServices     = tabServices;
    this._tabTempServices = tabTempServices;
    this._tabRef          = tabRefServices;

    this._tabUsed = new Array();

    this._count = 0;
}

Location.RepairSheet_Services.prototype = {

    getCount: function()
    {
        return this._count;
    },

    getList: function()
    {
        return this._tabList;
    },

    getUsedList: function()
    {
        return this._tabUsed;
    },

    getRefList: function()
    {
        return this._tabRef;
    },

    getRefInfos: function(id)
    {
        return this._tabRef.find(function(item) {
            return item.id == id;
        });
    },

    /**
     * Droits
     */
    hasRight : function(right, subRight)
    {
        return this._mainObj.hasRight(right, subRight);
    },

    updateTotalAmount: function()
    {
        this._mainObj.updateTotalAmount();
    },

    init: function()
    {
        var mySelf = this;
        // Tableau des services déjà enregistrés en base avec tts les infos
        for (var i in this._tabServices)
        {
            var attributes = {
                'lineId'       : this._tabServices[i]['id'],
                'selectedType' : this._tabServices[i]['type'],
                'cost'         : this._tabServices[i]['cost'], 
                'unitCost'     : this._tabServices[i]['unitCost'],
                'amount'       : this._tabServices[i]['amount'], 
                'unitAmount'   : this._tabServices[i]['unitAmount'],
                'quantity'     : this._tabServices[i]['quantity'],
                'currency'     : this._tabServices[i]['currency.id'],
                'service.name' : this._tabServices[i]['service.name'],
                'service.infos': this._tabServices[i]['service.infos'],
                'sheetState'   : this._mainObj._sheetState
            };

            //Appel de la fonction ajout d'une ligne de service
            this.addRowService(this._tabServices[i]['service.id'], attributes, true);
        }

        //Liste des services temporaires
        for (var i in this._tabTempServices)
        {
            var id = this._tabTempServices[i]['id'];
            var isSaved = (Number(this._tabTempServices[i]['isSaved']) ?  true : false);

            if (id != 0 && !isSaved)
            {
                var attributes = {
                    'selectedType' : this._tabTempServices[i]['type'], 
                    'cost'         : this._tabTempServices[i]['cost'],
                    'unitCost'     : this._tabTempServices[i]['unitCst'],
                    'amount'       : this._tabTempServices[i]['amount'], 
                    'unitAmount'   : this._tabTempServices[i]['unitAmt'],
                    'quantity'     : this._tabTempServices[i]['qty'],
                };

                this.addRowService(id, attributes, this._tabTempServices['isSaved']);
            }
        }
        
        // Evènements
        // - Ajout d'une prestation technique
        $ge('addServiceBtn').onclick = function() {
            mySelf.addRowService();
        };

    },

    /**
     * Ajout d'une ligne de service
     */
    addRowService: function(id, attributes, isSaved)
    {
        //Cas où la ligne n'est pas vierge
        if (typeof id != 'undefined' && typeof attributes != 'undefined')
        {
            this._tabList[this._count] = new Location.RepairSheet_Services_Line(this, id, attributes, isSaved);
        }
        //Cas où la ligne est vierge
        else
        {
            this._tabList[this._count] = new Location.RepairSheet_Services_Line(this, undefined, undefined, undefined);
        }

        this._count++;
        this.updateTotalAmount();
    },

    /*
     * Fonction qui gère ttes les listes de presta à l'ajout d'une presta
     */
    addSrvListOption: function (srvLineId, srvId)
    {
        //Boucle sur tts les lignes de service 
        for(var i in this._tabList)
        {
            //Si la ligne n'est pas la ligne controlée (srvLineId)
            if (this._tabList[i].getId() != srvLineId)
            {
                var select = $('service[' + this._tabList[i].getId() + '][id]');
                //On boucle sur les services de la liste de la ligne
                for (var selectId = 0; selectId < select.length; selectId++)
                {
                    //Si le service est celui passé en paramétre, on supprime (srvId)
                    if (select.options[selectId].value == srvId)
                    {
                        select.removeChild(select.options[selectId]);
                    }
                }
            }
        }
        //Tableau des Id des services utilisés
        this._tabUsed.push(srvId);
    },

    /*
     * Fonction qui gère ttes les listes de presta à la suppression d'une presta
     */
    deleteSrvListOption: function(srvLineId, srvId)
    {
        for(var i in this._tabList)
        {
            if (this._tabList[i].getId() != srvLineId)
            {
                var select = $('service[' + this._tabList[i].getId() + '][id]');

                var option = document.createElement('option');
                option.value = this._tabRef[srvId]['id'];
                option.innerHTML = this._tabRef[srvId]['name'];
                
                select.appendChild(option);
            }
        }
        
        for (var tabId = 0; tabId < this._tabUsed.length; tabId++)
        {
            if (this._tabUsed[tabId] == srvId)
            {
                this._tabUsed.splice(tabId, 1);
            }
        }
    }
}


/**
 * Objet de gestion des lignes  de service
 *
 * @param id          Identifiant de la ligne
 * @param attributes  attributs
 */
Location.RepairSheet_Services_Line = function(mainObj, srvId, attributes, isSaved)
{
    var mySelf = this;//Instance en cours
    this._mainObj = mainObj;
    this._objType = 'serviceLine';//Type de l'objet

    this._id = this._mainObj.getCount();//Id de la ligne

    this._srvId = (srvId ? srvId : 0);//Id du service

    //Etat de la ligne (en modification ou temporaire)
    if (typeof isSaved != 'undefined')
    {
        this._isSaved = isSaved;
    }
    else//Ligne en création (Ligne vierge)
    {
        this._isSaved = false;
    }

     this._tabServiceInfos = this._mainObj.getRefInfos(this._srvId);

    // Si la presta fait partie du paramétrage, elle est active sinon elle est inactive
    if (this._tabServiceInfos)
    {
        this._isActivated = true;
    }
    else
    {
        this._isActivated = false;
    }

    if (typeof attributes != 'undefined')
    {
        this._lineId            = (typeof attributes['lineId']       != 'undefined' ? attributes['lineId'] : '');
        this._cost              = (typeof attributes['cost']         != 'undefined' ? LOC_Common.round(attributes['cost'], 2) : 0);
        this._amount            = (typeof attributes['amount']       != 'undefined' ? LOC_Common.round(attributes['amount'], 2) : 0);
        this._sheetState        = attributes['sheetState'];
        // Cas où la prestation n'a pas été supprimée
        if (this._tabServiceInfos)
        {
            // Définit le type
            if (this._sheetState == '')
            {
                if (typeof attributes['selectedType']     != 'undefined')
                {
                    if (this._tabServiceInfos['type'] == 'I/E')
                    {
                        this._selectedType = attributes['selectedType'];
                    }
                    else
                    {
                        this._selectedType = this._tabServiceInfos['type'];
                    }
                }
                else
                {
                    this._selectedType = '';
                }
            }
            else
            {
                this._tabServiceInfos['type'] = attributes['selectedType'];
                this._selectedType = attributes['selectedType'];
            }
            // Définit le cout unitaire
            if (typeof attributes['unitCost']     != 'undefined')
            {
                if (this._selectedType == 'INM' || this._sheetState == 'fixed')
                {
                    this._unitCost = LOC_Common.round(attributes['unitCost'], 2);
                }
                else
                {
                    this._unitCost = LOC_Common.round(this._tabServiceInfos['cost'], 2);
                }
            }
            else
            {
                this._unitCost = 0;
            }
            // Définit le montant unitaire
            if (typeof attributes['unitAmount']     != 'undefined')
            {
                if (this._selectedType == 'INM' || this._sheetState == 'fixed')
                {
                    this._unitAmount = LOC_Common.round(attributes['unitAmount'], 2);
                }
                else
                {
                    this._unitAmount = LOC_Common.round(this._tabServiceInfos['amount'], 2);
                }
            }
            else
            {
                this._unitAmount = 0;
            }
            
        }
        else
        {
            this._unitCost     = attributes['unitCost'];
            this._unitAmount   = attributes['unitAmount'];
            this._selectedType = attributes['selectedType'];
        }
        this._quantity          = (typeof attributes['quantity']     != 'undefined' ? attributes['quantity'] : 0);
        (this._selectedType == 'INM' ? this._selectedType = 'INT' : '');
        this._serviceName       = (typeof attributes['service.name'] != 'undefined' ? attributes['service.name'] : '');
        this._serviceInfos      = (typeof attributes['service.infos'] != 'undefined' ? attributes['service.infos'] : '');

    }
    else
    {
        this._lineId            = 0;
        this._selectedType      = '';
        this._cost              = 0;
        this._amount            = 0;
        this._unitCost          = 0;
        this._unitAmount        = 0;
        this._quantity          = 1;
        this._serviceName       = '';
        this._serviceInfos      = null;
    }

    // Initialisation
    this._init();
}



Location.RepairSheet_Services_Line.prototype = {

    /**
     * Récupérer le type de l'objet courant
     *
     * @return string ('estimateLine')
     */
    getObjectType : function()
    {
        return this._objType;
    },

    /**
     * Récuperer l'id de la ligne
     * 
     * @return int
     */
    getLineId : function()
    {
        return this._lineId;
    },
    
    /**
     * Récuperer le n° de la ligne
     * 
     * @return int
     */
    getId : function()
    {
        return this._id;
    },
    
    /**
     * Récuperer l'id du service de la ligne
     * 
     * @return int
     */
    getSrvId : function()
    {
        return this._srvId;
    },
    
    /**
     * Récuperer le type du service
     * 
     * @return string
     */
    getType : function()
    {
        return this._type;
    },
    
    /**
     * Récuperer le type du service selectionné
     * 
     * @return string
     */
    getSelectedType : function()
    {
        return this._selectedType;
    },
    
    /**
     * Récuperer le nom du service
     * 
     * @return string
     */
    getName : function()
    {
        return this._name;
    },
    
    /**
     * Récuperer l'id currency du service
     * 
     * @return string
     */
    getCurrency : function()
    {
        return this._currency;
    },
    
    /**
     * Récuperer le cout du service int
     * 
     * @return float
     */
    getIntCost : function()
    {
        return this._unitCost;
    },
    
    /**
     * Récuperer le cout du service
     * 
     * @return float
     */
    getCost : function()
    {
        return this._cost;
    },
    
    /**
     * Récuperer le montant du service int
     * 
     * @return float
     */
    getUnitAmount : function()
    {
        return this._unitAmount;
    },
    
    /**
     * Récuperer le montant du service
     * 
     * @return float
     */
    getAmount : function()
    {
        return this._amount;
    },

    /**
     * Droits
     */
    hasRight : function(right, subRight)
    {
        return this._mainObj.hasRight(right, subRight);
    },

    /**
     * Définir l'id de la ligne
     * 
     * @param int id 
     */
    setLineId : function(id)
    {
        this._lineId = id;
    },
    
    /**
     * Définir le n° de la ligne
     * 
     * @param int id 
     */
    setId : function(id)
    {
        this._id = id;
    },
    
    /**
     * Définir l'id du service de la ligne
     * 
     * @param int srvId
     * @return bool
     */
    setSrvId : function(srvId)
    {
        //Cas où on sélectionne le même service
        if (srvId == this._srvId)
        {
            return false;
        }
        
        var oldSrvId = this._srvId;//Ancien Id du service conservé
        this._srvId = srvId;//Id du service de l'objet == Id du service sélectionné
        if (this._srvId == 0)
        {
            this._name              = '';
            this._cost              = 0;
            this._unitCost            = 0;
            this._amount            = 0;
            this._unitAmount            = 0;
            this._currency          = '';
            this._type              = '';
            this._selectedType      = '';
            this._hasQty            = 0;
            this._quantity          = 0;
            this._isMarginAllowed   = 0;
            this._isActivated       = 0;
        }
        else//Affectation des attributs de la classe
        {
            this._tabServiceInfos = this._mainObj.getRefInfos(this._srvId);
            this._name          = this._tabServiceInfos['name'];
            this._type          = this._tabServiceInfos['type'];
            //On détermine le type sélectionné, si le service est 'I/E' ou 'INM' on selection 'INT' par défaut
            if (this._type == 'I/E' || this._type == 'INM')
            {
                this._selectedType = 'INT';
            }
            else
            {
                this._selectedType  = this._type; 
            }
            this._cost              = 0;
            this._unitCost          = LOC_Common.round(this._tabServiceInfos['cost'], 2);
            this._unitAmount        = LOC_Common.round(this._tabServiceInfos['amount'], 2);
            this._amount            = 0;
            this._currency          = this._tabServiceInfos['currency.id'];
            this._hasQty            = this._tabServiceInfos['isQuantityAllowed'];
            this._quantity          = 1;
            this._isMarginAllowed   = this._tabServiceInfos['isMarginAllowed'];
            this._isActivated       = 1;
            //Enlève dans les autres listes l'Id du service saisi
            this._mainObj.addSrvListOption(this._id, this._srvId);
        }
        if (oldSrvId != 0)
        {
           //Rajoute dans les autres listes l'ancien Id du service
            this._mainObj.deleteSrvListOption(this._id, oldSrvId);
        }
        this.applyActions();
        this._mainObj.updateTotalAmount();
        return true;
    },

    /**
     * Définir le type du service
     * 
     * @param string type
     */
    setType : function(type)
    {
        this._type = type;
    },
    
    /**
     * Définir le type du service selectionné
     * 
     * @param string selectedType
     */
    setSelectedType : function(selectedType)
    {
        if (this._selectedType == selectedType)
        {
            return false;
        }
        this._selectedType = selectedType;
        detailSpan      = $('service_' + this._id + '_details');
        costInput       = $('service[' + this._id + '][cost]');
        unitAmountInput = Location.currenciesManager.getCurrency('service_' + this._id + '_unitAmount');
        qtyInput        = Location.spinControlsManager.getSpinControl('service[' + this._id + '][qty]');
        costUnitInput   = $('service[' + this._id + '][unitCst]');
        amountInput     = Location.currenciesManager.getCurrency('service[' + this._id + '][amount]');
        unitAmtInput    = $('service[' + this._id + '][unitAmt]');
        typeIntInput    = $('service[' + this._id + '][type][INT]');
        typeExtInput    = $('service[' + this._id + '][type][EXT]');

        if (this._selectedType == 'INT')
        {
            amountInput.getElement().disabled = true;
            this._unitCost = LOC_Common.round(this._tabServiceInfos['cost'], 2);
            this._unitAmount = LOC_Common.round(this._tabServiceInfos['amount'], 2);
            if (this._hasQty == 1)
            {
                qtyInput.setValue(this._quantity);
                unitAmountInput.setValue(this._unitAmount);
                unitAmountInput.getElement().disabled = true;
                detailSpan.style.visibility = 'visible';
                this._cost = qtyInput.getValue() * LOC_Common.round(this._unitCost, 2);
                this._amount = qtyInput.getValue() * LOC_Common.round(unitAmountInput.getValue(), 2);
                amountInput.setValue(this._amount);
                amountInput.getElement().disabled = true;
                unitAmtInput.value = this._unitAmount;
                costUnitInput.value = this._unitCost; 
                costInput.value = this._cost;
            }
            else
            {
                this._cost = this._unitCost;
                this._amount = this._unitAmount;
                detailSpan.style.visibility = 'hidden';
                costInput.value = this._cost;
                amountInput.setValue(this._amount);
                unitAmtInput.value = this._unitAmount;
            }
        }
        else
        {
            this._amount = 0;
            this._cost   = 0;
            amountInput.setValue(this._amount);
            amountInput.getElement().disabled = false;
            costInput.value = this._cost;
            detailSpan.style.visibility = 'hidden';
        }


        this._mainObj.updateTotalAmount();
    },
    
    /**
     * Récuperer Si le service a une quantité
     * 
     * @return int
     */
    getHasQty : function()
    {
        return this._hasQty;
    },
    
    /**
     * Récuperer Si le service a a une marge externe autorisée
     * 
     * @return int
     */
    getIsMarginAllowed : function()
    {
        return this._isMarginAllowed;
    },
    
    /**
     * Récuperer Si la quantité
     * 
     * @return int
     */
    getQuantity : function()
    {
        return this._quantity;
    },
    
    /**
     * Définir le nom du service
     * 
     * @param string name
     */
    setName : function(name)
    {
        this._name = name;
    },
    
    /**
     * Définir l'id currency du service
     * 
     * @param string currencyId
     */
    setCurrency : function(currencyId)
    {
        this._currency = currencyId;
    },

    /**
     * Définir le montant du service
     * 
     * @param float amount
     */
    setAmount : function(amount)
    {
        if (this._amount == amount)
        {
            return false;
        }
        this._amount = amount;

        costInput       = $('service[' + this._id + '][cost]');
        unitAmountInput = $('service[' + this._id + '][unitAmt]');
        unitCostInput   = $('service[' + this._id + '][unitCst]');

        if (this._type =='INM')
        {
            this._cost = this._amount;
            this._unitCost = this._cost;
            this._unitAmount = this._amount;

            costInput.value = this._cost;
            unitAmountInput.value = this._unitAmount;
            unitCostInput.value = this._unitCost;
        }
        else if (this._type == 'EXT' || (this._type == 'I/E' && this._selectedType == 'EXT'))
        {
            if (this._isMarginAllowed == 1)
            {
                this._cost = this._amount / (1 + $('extSrvMargin').value * 1);
                this._unitCost = this._cost;
                this._unitAmount = this._amount;
            }
            else
            {
                this._cost = this._amount;
                this._unitCost = this._cost;
                this._unitAmount = this._amount;
            }

            costInput.value = this._cost;
            unitAmountInput.value = this._unitAmount;
            unitCostInput.value = this._unitCost;
        }
        this._mainObj.updateTotalAmount();

        return true;
    },
    
    /**
     * Définir si le service a une quantité
     * 
     * @param int 0 ou 1
     */
    setHasQty : function(hasQty)
    {
        this._hasQty = hasQty;
    },
    
    /**
     * Définir si le service a une marge sur service externe
     * 
     * @param int 0 ou 1
     */
    setIsMarginAllowed : function(isMarginAllowed)
    {
        this._isMarginAllowed = isMarginAllowed;
    },
    
    /**
     * Définir la quantité
     * 
     * @param int quantité
     */
    setQuantity : function(quantity)
    {
        if (this._quantity == quantity)
        {
            return false;
        }

        this._quantity = quantity;

        if ((this._type == 'INT' && this._hasQty) || this._type == 'I/E')
        {
            amountInput = Location.currenciesManager.getCurrency('service[' + this._id + '][amount]');
            costInput = $('service[' + this._id + '][cost]');
            qtyInput  = Location.spinControlsManager.getSpinControl('service[' + this._id + '][qty]');

            qtyInput.setValue(this._quantity);
            this._cost = this._quantity * LOC_Common.round(this._unitCost, 2);
            this._cost = LOC_Common.round(this._cost, 2);
            this._amount = this._quantity * LOC_Common.round(this._unitAmount, 2);

            costInput.value = this._cost;
            amountInput.setValue(this._amount);
        }
        else if ((this._type == 'INM') && this._hasQty)
        {
            amountInput = Location.currenciesManager.getCurrency('service[' + this._id + '][amount]');
            costInput = $('service[' + this._id + '][cost]');
            qtyInput  = Location.spinControlsManager.getSpinControl('service[' + this._id + '][qty]');
            qtyInput.setValue(this._quantity);
            this._cost = this._quantity * LOC_Common.round(this._unitCost, 2);
            this._cost = LOC_Common.round(this._cost, 2);
            this._amount = this._quantity * LOC_Common.round(this._unitCost, 2);

            costInput.value = this._cost;
            amountInput.setValue(this._amount);
        }

        this._mainObj.updateTotalAmount();

        return true;
    },

    /**
     * Définir le prix unitaire (cas service INM)
     * 
     * @param int unitAmount
     */
    setUnitAmount : function(unitAmount)
    {
        if (this._unitAmount == unitAmount)
        {
            return false;
        }

        this._unitAmount = unitAmount;

        if (this._type == 'INM' && this._hasQty)
        {
            costInput = $('service[' + this._id + '][cost]')
            unitCstInput = $('service[' + this._id + '][unitCst]');
            amountInput = Location.currenciesManager.getCurrency('service[' + this._id + '][amount]');
            unitAmtInput = $('service[' + this._id + '][unitAmt]');

            this._unitCost = this._unitAmount;
            unitCstInput.value = this._unitCost;
            unitAmtInput.value = this._unitAmount;
            this._cost = LOC_Common.round(this._quantity * this._unitCost, 2);
            this._amount = this._cost;
            costInput.value = this._cost;
            amountInput.setValue(this._amount);
        }
        
        this._mainObj.updateTotalAmount();

        return true;
    },
    
    /**
     * Initialisation
     *
     */
    _init : function()
    {
        var mySelf = this;

        //Ligne en création
        if (this._srvId == 0)
        {
            this._name = '-- ' + LOC_View.getTranslation('noneSelected') + ' --';
            this._unitCost = 0;
        }
        else 
        {
            if (this._tabServiceInfos)
            {
                this._name              = this._tabServiceInfos['name'];
                this._type              = this._tabServiceInfos['type'];
                this._currency          = this._tabServiceInfos['currency.id'];
                this._hasQty            = this._tabServiceInfos['isQuantityAllowed'];
                this._isMarginAllowed   = this._tabServiceInfos['isMarginAllowed'];
            }
            else
            {
                this._name              = this._serviceName;
                this._type              = this._selectedType;
                this._hasQty            = (this._quantity > 1 ? 1 : 0);
            }
        }
        if (this._hasQty == 0 && this._sheetState == '')
        {
            
            this._quantity = 1;
        }
        
        //Création de la première ligne ou les lignes qui sont en modif ou temporaire
        this.createLine();
        
        //Maj de ttes les listes de service (suppression de l'Id du service dans les autres listes)
        //Si Id du service <> de 0 (soit --Aucun--)
        (this._srvId != 0) ? this._mainObj.addSrvListOption(this._id, this._srvId) : '';
    },


    /**
     * Création et affichage de la ligne
     */
    createLine : function()
    {
        var mySelf = this;
        var table = $('tableService');

        if (table != null)
        {
            // Nouvelle ligne
            var srvCount = this._mainObj.getCount();
            var row = table.tBodies[0].insertRow(-1);
            row.id = 'trService_' + srvCount;

            // Désignation
            var cell = document.createElement('td');
            cell.id = 'tdSrvId_' + srvCount;
            row.appendChild(cell);
            // - Champ caché id de ligne
            var cellContent = document.createElement('input');
            cellContent.id = 'service[' + srvCount + '][lineId]';
            cellContent.name = 'service[' + srvCount + '][lineId]';
            cellContent.type = 'hidden';
            cellContent.value = this._lineId;
            cell.appendChild(cellContent);
            // - Liste déroulante des services
            var cellContent = document.createElement('select');
            var option = document.createElement('option');
            option.value = 0;
            option.innerHTML = '-- ' + LOC_View.getTranslation('noneSelected') + ' --';
            cellContent.appendChild(option);
            // La prestation a t-elle était supprimée du paramétrage?
            var isDeleted =  (this._srvId ? true : false);
            //On boucle sur tous les services existants 
            var tabUsedList = this._mainObj.getUsedList();
            var tabRefList  = this._mainObj.getRefList();
            for (var i = 0; i < tabRefList.length; i++) 
            {
                var tabRefInfos = tabRefList[i];
                //Ajout des Id de service non utilisé
                if (!in_array(tabRefInfos.id, tabUsedList))
                {
                    var option = document.createElement('option');
                    option.value = tabRefInfos.id;
                    option.innerHTML = tabRefInfos.name;
                    option.setAttribute('data-infos', tabRefInfos.infos);

                    if (tabRefInfos.id == this._srvId)
                    {
                        option.selected = true;
                        isDeleted = false;
                    }

                    cellContent.appendChild(option);
                }
            }

            // Dans le cas où la prestation a été supprimée du paramétrage, on la rajoute dans la liste déroulante
            if (isDeleted)
            {
                var option = document.createElement('option');
                option.value = this._srvId;
                option.innerHTML = this._serviceName;
                option.setAttribute('data-infos', this._serviceInfos);
                option.selected = true;
                cellContent.appendChild(option);
            }

            cellContent.id = 'service[' + srvCount + '][id]';
            cellContent.name = 'service[' + srvCount + '][id]';
            cellContent.onchange = function()
            {
                //Changement de l'Id du service (et donc des différents elements associés à l'Id)
                mySelf.setSrvId(this.value);//affectation des attributs
            }
            cellContent.value = this._srvId;
            if (!this.hasRight('services') || !this.hasRight('services', 'label'))
            {
                cellContent.setAttribute('disabled', 'disabled');
            }
            cell.appendChild(cellContent);

            // - Champ caché pour envoyer l'id si pas le droit sur la liste déroulante
            if (!this.hasRight('services') || !this.hasRight('services', 'label'))
            {
                var cellContent = document.createElement('input');
                cellContent.type  = 'hidden';
                cellContent.id    = 'service[' + srvCount + '][id]';
                cellContent.name  = 'service[' + srvCount + '][id]';
                cellContent.value = this._srvId;
                cell.appendChild(cellContent);
            }
            // - Infobulle sur le service
            var toolTipContent =  $ge('srvTooltip').cloneNode(true);
            toolTipContent.id = 'service_' + srvCount + '_tooltip';
            Location.commonObj.addEltClass(toolTipContent, 'hidden');
            cell.appendChild(toolTipContent);

            // Type : INT
            var cell = document.createElement('td');
            cell.id = 'tdServiceInt_' + srvCount;
            cell.style.textAlign = 'center';
            var cellContent = document.createElement('input');
            cellContent.type = 'radio';
            cellContent.setAttribute('class', 'radio');
            cellContent.id = 'service[' + srvCount + '][type][INT]';
            cellContent.name = 'service[' + srvCount + '][type]';
            cellContent.onclick = function()
            {
                mySelf.setSelectedType(this.value);
            }
            cellContent.value = 'INT';
            if (!this.hasRight('services') || !this.hasRight('services', 'internal'))
            {
                cellContent.setAttribute('readonly', 'readonly');
            }
            cell.appendChild(cellContent);
            row.appendChild(cell);

            // Type : EXT
            var cell = document.createElement('td');
            cell.id = 'tdServiceExt_' + srvCount;
            cell.style.textAlign = 'center';
            var cellContent = document.createElement('input');
            cellContent.type = 'radio';
            cellContent.setAttribute('class', 'radio');
            cellContent.id = 'service[' + srvCount + '][type][EXT]';
            cellContent.name = 'service[' + srvCount + '][type]';
            cellContent.onclick = function()
            {
                //Changement du type sélectionné (uniquement dans le cas I/E)
                mySelf.setSelectedType(this.value);
            }
            cellContent.value = 'EXT';
            if (!this.hasRight('services') || !this.hasRight('services', 'external'))
            {
                cellContent.setAttribute('readonly', 'readonly');
            }
            cell.appendChild(cellContent);
            row.appendChild(cell);

            // Détails
            var cell = document.createElement('td');
            cell.id = 'tdServiceDetails_' + srvCount;
            cell.style.textAlign = 'right';
            var cellContent = document.createElement('span');
            cellContent.id = 'service_' + srvCount + '_details';

            // - Montant unitaire
            var cellContentUnitAmount = document.createElement('input');
            cellContentUnitAmount.id = 'service_' + srvCount + '_unitAmount';
            cellContentUnitAmount.size = 5
            cellContentUnitAmount.type = 'text';
            cellContentUnitAmount.setAttribute('class', 'costInput');
            cellContentUnitAmount.onchange = function()
            {
                mySelf.setUnitAmount(this.value);
            }
            cellContent.appendChild(cellContentUnitAmount);

            // - Quantité
            var cellContentQty = document.createElement('input');
            cellContentQty.id = 'service[' + srvCount + '][qty]';
            cellContentQty.name = 'service[' + srvCount + '][qty]';
            cellContentQty.size = 2;
            cellContentQty.type = 'text';
            cellContentQty.setAttribute('class', 'spinInput');
            cellContentQty.style.marginLeft = '15px';
            cellContent.appendChild(cellContentQty);

            cell.appendChild(cellContent);
            row.appendChild(cell);

            Location.currenciesManager.cloneCurrency('service_' + srvCount + '_unitAmount', 'serviceCst_blank');
            var spinQtyObj = Location.spinControlsManager.createSpinControl('service[' + srvCount + '][qty]', 1, null, 1, 10, 0);
            spinQtyObj.onchange = function()
            {
                mySelf.setQuantity(this.getValue());
            }

            if (!this.hasRight('services') || !this.hasRight('services', 'quantity'))
            {
                spinQtyObj.disable();
                var cellContent = document.createElement('input');
                cellContent.type  = 'hidden';
                cellContent.id    = 'service[' + srvCount + '][qty]';
                cellContent.name  = 'service[' + srvCount + '][qty]';
                cellContent.value = this._quantity;
                cell.appendChild(cellContent);
            }

            // - Champ caché quantité possible
            var cellContent = document.createElement('input');
            cellContent.id = 'service[' + srvCount + '][hasQty]';
            cellContent.name = 'service[' + srvCount + '][hasQty]';
            cellContent.type = 'hidden';
            cell.appendChild(cellContent);


            // Montant
            var cell = document.createElement('td');
            cell.id = 'tdServiceCst_' + srvCount;
            cell.style.textAlign = 'right';
            cellContent = document.createElement('input');
            cellContent.id = 'service[' + srvCount + '][amount]';
            cellContent.name = 'service[' + srvCount + '][amount]';
            cellContent.size = 5;
            cellContent.type = 'text';
            cellContent.setAttribute('class', 'costInput');
            
            cell.appendChild(cellContent);
            row.appendChild(cell);

            var amountObj = Location.currenciesManager.cloneCurrency('service[' + srvCount + '][amount]', 'serviceCst_blank');
            amountObj.onchange = function()
            {
                //Changement du coût 
                mySelf.setAmount(this.getValue());
            }

            // Suppression et champs cachés supplémentaires
            var cell = document.createElement('td');
            cell.id = 'tdServiceRbtn_' + srvCount;
            cell.style.textAlign = 'right';
            if (this.hasRight('services', 'delete'))
            {
                var cellContent = document.createElement('a');
                cellContent.href = '#';
                cellContent.setAttribute('class', 'deleteServiceLine');
                cellContent.onclick = function ()
                {
                    if (confirm(LOC_View.getTranslation('confirmRemoveService')))
                    {
                        mySelf.remove();
                    }
                }
                cell.appendChild(cellContent);
            }

            // - Champ caché coût unitaire
            var cellContent = document.createElement('input');
            cellContent.id = 'service[' + srvCount + '][unitCst]';
            cellContent.name = 'service[' + srvCount + '][unitCst]';
            cellContent.type = 'hidden';
            cell.appendChild(cellContent);
            row.appendChild(cell);

            // - Champ caché montant unitaire
            var cellContent = document.createElement('input');
            cellContent.id = 'service[' + srvCount + '][unitAmt]';
            cellContent.name = 'service[' + srvCount + '][unitAmt]';
            cellContent.type = 'hidden';
            cell.appendChild(cellContent);
            row.appendChild(cell);

            // - Champ caché coût
            var cellContent = document.createElement('input');
            cellContent.id = 'service[' + srvCount + '][cost]';
            cellContent.name = 'service[' + srvCount + '][cost]';
            cellContent.type = 'hidden';
            cell.appendChild(cellContent);
            row.appendChild(cell);

            // - Champ caché marge autorisée
            var cellContent = document.createElement('input');
            cellContent.id = 'service[' + srvCount + '][isMarginAllowed]';
            cellContent.name = 'service[' + srvCount + '][isMarginAllowed]';
            cellContent.type = 'hidden';
            cell.appendChild(cellContent);
            row.appendChild(cell);

            // - Champ caché "est enregistré"
            var cellContent = document.createElement('input');
            cellContent.id = 'service[' + srvCount + '][isSaved]';
            cellContent.name = 'service[' + srvCount + '][isSaved]';
            cellContent.type = 'hidden';
            cellContent.value = (this._isSaved ? 1 : 0);
            cell.appendChild(cellContent);
            row.appendChild(cell);

            // - Champ caché "est activé"
            var cellContent = document.createElement('input');
            cellContent.id = 'service[' + srvCount + '][isActivated]';
            cellContent.name = 'service[' + srvCount + '][isActivated]';
            cellContent.type = 'hidden';
            cellContent.value = (this._isActivated ? 1 : 0);
            cell.appendChild(cellContent);
            row.appendChild(cell);

            // Affectation des données
            this.applyActions();
        }
    },
    /**
     * Suppression de la ligne
     */
    remove : function()
    {
        if (this._isSaved == true)
        {
            Location.commonObj.addEltClass('trService_' + this._id, 'removed');

            $('tdServiceRbtn_' + this._id).getElementsByTagName('a')[0].style.visibility = 'hidden';

            if (obj = $('service[' + this._id + '][lineId]')) {
                obj.disabled = true;
            }
            if (obj = $('service[' + this._id + '][id]')) {
                obj.disabled = true;
            }
            if (obj = $('service[' + this._id + '][type][INT]')) {
                obj.disabled = true;
            }
            if (obj = $('service[' + this._id + '][type][EXT]')) {
                obj.disabled = true;
            }
            if (obj = Location.currenciesManager.getCurrency('service_' + this._id + '_unitAmount')) {
                obj.disable();
            }
            if (obj = Location.currenciesManager.getCurrency('service[' + this._id + '][unitAmt]')) {
                obj.disable();
            }
            if (obj = Location.spinControlsManager.getSpinControl('service[' + this._id + '][qty]')) {
                obj.disable();
            }
            if (obj = $('service[' + this._id + '][hasQty]')) {
                obj.disabled = true;
            }
            if (obj = Location.currenciesManager.getCurrency('service[' + this._id + '][cost]')) {
                obj.disable();
            }
            if (obj = $('service[' + this._id + '][unitCst]')) {
                obj.disabled = true;
            }
            if (obj = $('service[' + this._id + '][isSaved]')) {
                obj.disabled = true;
            }

        }
        else//Ligne n'existe pas en BDD
        {
            var row = $('trService_' + this._id);
            row.parentNode.deleteRow(row.sectionRowIndex);
        }
        /* Appel de la fonction pour remettre l'Id du service "en service" dans les autres listes dans le cas où la presta n'a pas 
        été supprimée du paramétrage */
        (this._srvId != 0 && this._isActivated) ? this._mainObj.deleteSrvListOption(this._id, this._srvId) : '';
        //Suppression de l'objet dans le tableau des lignes prestas 
        delete (this._mainObj._tabList[this._id]);
        this._mainObj.updateTotalAmount();
    },

    /**
     * Reformatage des champs apres changement
     */
    applyActions : function()
    {
        //Récupération de l'élément currency cost (obligé de passetr par currenciesManager)
        idSelect                    = $('service[' + this._id + '][id]');
        costInput                   = $('service[' + this._id + '][cost]');
        costUnitInput               = $('service[' + this._id + '][unitCst]');
        unitAmtInput                = $('service[' + this._id + '][unitAmt]');
        amountInput                 = Location.currenciesManager.getCurrency('service[' + this._id + '][amount]');
        typeIntInput                = $('service[' + this._id + '][type][INT]');
        typeExtInput                = $('service[' + this._id + '][type][EXT]');
        hasQtyInput                 = $('service[' + this._id + '][hasQty]');
        isMarginAllowedInput        = $('service[' + this._id + '][isMarginAllowed]');
        isActivatedInput            = $('service[' + this._id + '][isActivated]');
        unitAmountInput             = Location.currenciesManager.getCurrency('service_' + this._id + '_unitAmount');
        qtyInput                    = Location.spinControlsManager.getSpinControl('service[' + this._id + '][qty]');
        detailSpan                  = $('service_' + this._id + '_details');
        detailSpan.style.visibility = 'hidden';
        tooltipEl                   = $('service_' + this._id + '_tooltip');
        hasQtyInput.value           = this._hasQty; 
        isMarginAllowedInput.value  = this._isMarginAllowed;
        isActivatedInput.value      = this._isActivated;

        //Cas où le service sélectionné est --Aucune--
        if (this._srvId == 0)
        {
            costInput.value = this._cost;
            costUnitInput.value = this._unitCost;
            amountInput.setValue(this._amount);
            amountInput.getElement().disabled = true;
            unitAmtInput.value = this._unitAmount;
            typeIntInput.checked = false;
            typeExtInput.checked = false;
            typeIntInput.style.visibility = 'visible';
            typeIntInput.disabled = true;
            typeExtInput.style.visibility = 'visible';
            typeExtInput.disabled = true;

            unitAmountInput.setValue(0);
            qtyInput.setValue(0);

            Location.commonObj.addEltClass(tooltipEl, 'hidden');
        }
        else //Affectation des valeurs dans les différents champs (dans le cas d'un chargement d'une ligne en BDD et TEMPORAIRE)
        {
            costUnitInput.value = this._unitCost;
            unitAmtInput.value = this._unitAmount;
            if (this._type == 'INT')
            {
                typeIntInput.value = this._type;
                typeIntInput.checked = true;
                typeIntInput.disabled = false;
                typeExtInput.checked = false;
                typeExtInput.style.visibility = 'hidden';
                typeIntInput.style.visibility = 'visible';
                if (this._hasQty == 1 || this._sheetState == 'fixed')
                {
                    qtyInput.setValue(this._quantity);
                    unitAmountInput.setValue(this._unitAmount);
                    unitAmountInput.getElement().disabled = true

                    this._cost = qtyInput.getValue() * this._unitCost;
                    this._cost = LOC_Common.round(this._cost, 2);
                    this._amount = qtyInput.getValue() * LOC_Common.round(this._unitAmount, 2);
                    amountInput.setValue(this._amount);
                    amountInput.getElement().disabled = true;
                    costInput.value = this._cost;
                    detailSpan.style.visibility = 'visible';
                }
                else
                {
                    qtyInput.setValue(this._quantity);
                    this._amount = this._unitAmount; 
                    this._cost = this._unitCost;
                    amountInput.setValue(this._amount);
                    amountInput.getElement().disabled = true;
                    costInput.value = this._unitCost;
                }
            }
            else if (this._type == 'INM')
            {
                typeIntInput.value = this._type;
                typeIntInput.checked = true;
                typeIntInput.disabled = false;
                typeExtInput.checked = false;
                typeExtInput.style.visibility = 'hidden';
                typeIntInput.style.visibility = 'visible';
                if (this._hasQty == 1)
                {
                    qtyInput.setValue(this._quantity);
                    unitAmountInput.setValue(this._unitAmount);
                    unitAmountInput.getElement().disabled = false;

                    this._amount = qtyInput.getValue() * LOC_Common.round(unitAmountInput.getValue(), 2);
                    this._cost = this._amount;
                    costInput.value = this._cost;
                    amountInput.setValue(this._amount);
                    amountInput.getElement().disabled = true;
                    detailSpan.style.visibility = 'visible';
                }
                else
                {
                    this._cost = this._unitCost;
                    this._amount = this._cost;
                    costInput.value = this._unitCost;
                    amountInput.setValue(this._amount);
                    amountInput.getElement().disabled = false;
                }
            }
            else if (this._type == 'EXT')
            {
                costInput.value = this._cost;
                amountInput.setValue(this._amount);
                amountInput.getElement().disabled = false;
                typeIntInput.checked = false;
                typeExtInput.checked = true;
                typeExtInput.disabled = false;
                typeIntInput.style.visibility = 'hidden';
                typeExtInput.style.visibility = 'visible';
            }
            else if (this._type == 'I/E')
            {
                typeIntInput.disabled = false;
                typeExtInput.disabled = false;
                typeIntInput.style.visibility = 'visible';
                typeExtInput.style.visibility = 'visible';
                if (this._selectedType == 'EXT')
                {
                    costInput.value = this._cost; 
                    amountInput.setValue(this._amount);
                    amountInput.getElement().disabled = false;
                    typeIntInput.checked = false;
                    typeExtInput.checked = true;
                }
                else
                {
                    if (this._hasQty == 1)
                    {
                        qtyInput.setValue(this._quantity);
                        unitAmountInput.setValue(this._unitAmount);
                        unitAmountInput.getElement().disabled = true;

                        this._cost = qtyInput.getValue() * LOC_Common.round(this._unitCost, 2);
                        this._amount = qtyInput.getValue() * LOC_Common.round(unitAmountInput.getValue(), 2);
                        
                        amountInput.setValue(this._amount);
                        amountInput.getElement().disabled = true;
                        costInput.value = this._cost;
                        detailSpan.style.visibility = 'visible';
                    }
                    else
                    {
                        this._cost = this._unitCost;
                        this._amount = this._unitAmount;
                        amountInput.setValue(this._amount);
                        amountInput.getElement().disabled = true;
                        costInput.value = this._unitCost;
                    }
                    typeIntInput.checked = true;
                    typeExtInput.checked = false;
                }
             }

            var selectedElt = idSelect.selectedOptions[0];
            if (selectedElt && selectedElt.getAttribute('data-infos') != 'null' && selectedElt.getAttribute('data-infos') != '')
            {
                Location.commonObj.removeEltClass(tooltipEl, 'hidden');
                tooltipEl.setAttribute('data-text', selectedElt.getAttribute('data-infos'));
            }
            else
            {
                Location.commonObj.addEltClass(tooltipEl, 'hidden');
            }
        }

        if (!this.hasRight('services') || !this.hasRight('services', 'internal'))
        {
            typeIntInput.readOnly = true;
        }
        if (!this.hasRight('services') || !this.hasRight('services', 'external'))
        {
            typeExtInput.readOnly = true;
        }
        if (!this.hasRight('services') || !this.hasRight('services', 'unitAmount'))
        {
            unitAmountInput.getElement().readOnly = true;
        }
        if (!this.hasRight('services') || !this.hasRight('services', 'totalAmount'))
        {
            amountInput.getElement().readOnly = true;
        }
    },
    
    /**
     * Changement de type
     */
    changeType : function()
    {
    },
    
    disable : function()
    {
    }
};


