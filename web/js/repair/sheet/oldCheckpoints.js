if (!window.Location)
{
    var Location = new Object();
}

/**
 * Objet de gestion des anciens points de vérification
 */
Location.RepairSheet_OldCheckpoints = function(mainObj, tabOldCheckpoints)
{
    this._mainObj = mainObj;
    this._tabList = tabOldCheckpoints;
}

Location.RepairSheet_OldCheckpoints.prototype = {

    init: function()
    {
        //
    },

    getList: function()
    {
        return this._tabList;
    },

    getInfos: function(id)
    {
        return this._tabList.find(function(item) {
            return item.id == id;
        });
    },

    /**
     * Droits
     */
    hasRight : function(right, subRight)
    {
        return this._mainObj.hasRight(right, subRight);
    },

    /*
     * Affichage des anciens points de vérification
     */
    display: function()
    {
        var tableEl = $ge('oldCheckpoints');
        var tbodyEl = tableEl.tBodies[0];

        // Suppression des lignes
        while (tbodyEl.rows.length > 0)
        {
            Location.currenciesManager.deleteCurrency('amount_' + tbodyEl.rows[0].getAttribute('data-id'));
            tbodyEl.deleteRow(0);
        }

        var tabCheckpoints = this.getList();

        if (tabCheckpoints.length == 0)
        {
            tableEl.style.display = 'none';
        }

        for (var i = 0; i < tabCheckpoints.length; i++)
        {
            var checkpoint = tabCheckpoints[i];
            var rowEl = tbodyEl.insertRow(-1);
            rowEl.id = 'chp[' + checkpoint.id + ']';
            rowEl.setAttribute('data-id', checkpoint.id);

            this._displayLine(rowEl, checkpoint);
        }
    },

    remove: function(id)
    {
        var tabInfos = this.getInfos(id);
        tabInfos.state = 'removed';

        this.display();
        this._mainObj.updateTotalAmount();
    },

    _displayLine: function(rowEl, checkpoint)
    {
        var mySelf = this;
        var id = checkpoint.id;
        var tabInfos = checkpoint['checkpoint.tabInfos'];

        var isRemoved = false;

        if (checkpoint.state == 'removed')
        {
            Location.commonObj.addEltClass(rowEl, 'removed');
            isRemoved = true;
        }

        rowEl.id = 'row_' + id;

        // - Type
        var cellEl = rowEl.insertCell(-1);
        cellEl.className = 'type';
        cellEl.innerHTML = LOC_View.getTranslation(checkpoint.repairType == TYPE_FIX ? 'typeFix' :
                                                    (checkpoint.repairType == TYPE_REPAIR ? 'typeRepair' : 'typeChangement'));

        // - Désignation
        var cellEl = rowEl.insertCell(-1);
        cellEl.className = 'label';
        cellEl.innerHTML = tabInfos.label;

        // - Montant
        var cellEl = rowEl.insertCell(-1);
        cellEl.className = 'amount';
        el = window.document.createElement('INPUT');
        el.type = 'hidden';
        el.id = 'amount_' + id;
        cellEl.appendChild(el);
        el = Location.currenciesManager.cloneCurrency(el.id, 'amount_clone');
        el.setValue(checkpoint.unitAmount);

        // - Bouton d'action
        var cellEl = rowEl.insertCell(-1);
        cellEl.className = 'ctrls';

        cellEl.onclick = function(e) {
            e.stopPropagation();
        };
        // -- affichage si droit de remettre à vide (suppression du point de vérif)
        if (this.hasRight('references', 'delete'))
        {
            var el = window.document.createElement('A');
            el.href = '#';
            el.className = 'remove';
            el.title = LOC_View.getTranslation('removeReferenceBtn');
            el.onclick = function(e) {
                e.stopPropagation();
                if (confirm(LOC_View.getTranslation('confirmRemoveReference')))
                {
                    mySelf.remove(id);
                    return false;
                };
            }
            cellEl.appendChild(el);
        }
    }

};