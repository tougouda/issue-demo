if (!window.Location)
{
    var Location = new Object();
}

/**
 * Objet de gestion des références
 */
Location.RepairSheet_References = function(mainObj, tabReferences)
{
    this._mainObj = mainObj;
    this._tabList = tabReferences;
}

Location.RepairSheet_References.prototype = {

    _count: 0,

    init: function()
    {
        var mySelf = this;

        Location.RepairSheet_References.searchManager.init(this, this._mainObj._tabCfgs);

        // Evènements
        //  - Ajout d'une référence
        $ge('refAddBtn').onclick = function() {
            mySelf.add();
        };

        // Cacher les recherches d'articles
        document.onclick = function() {
            var tab = this.querySelectorAll('div.dynamicSearch');
            for (var i = 0; i < tab.length ; i++)
            {
                tab[i].style.display = 'none';
            }
        };
    },

    getList: function()
    {
        return this._tabList;
    },

    getReferenceInfos: function(id)
    {
        return this._tabList.find(function(item) {
            return item.id == id;
        });
    },

    /**
     * Droits
     */
    hasRight : function(right, subRight)
    {
        return this._mainObj.hasRight(right, subRight);
    },

    setQuantity: function(id, quantity)
    {
        var referenceInfos = this.getReferenceInfos(id);
        var cost = referenceInfos.cost;

        if (referenceInfos.unitCost)
        {
            cost = quantity * referenceInfos.unitCost;
        }
        if (this._mainObj._tabCfgs.isMarginAllowed)
        {
            referenceInfos.margin = referenceInfos.unitMargin * quantity;
        }

        referenceInfos.quantity = quantity;
        referenceInfos.cost = cost;
        referenceInfos.amount = (this._mainObj._tabCfgs.isMarginAllowed ? cost + referenceInfos.margin : cost);

        this._mainObj.updateTotalAmount();

        mySelf.display();

    },

    setCode: function(id, code)
    {
        var referenceInfos = this.getReferenceInfos(id);
        referenceInfos.artCode = code;
    },

    setLabel: function(id, label)
    {
        var referenceInfos = this.getReferenceInfos(id);
        referenceInfos['label'] = label;
    },

    add: function()
    {
        this._tabList.push({
            id              : 'add_' + this._count,
            'sheet.id'      : this._mainObj._id,
            'checkpoint.id' : 0,
            'currency.id'   : this._mainObj._tabCfgs.currencyId,
            quantity        : 1,
            artCode         : null,
            label           : '',
            repairType      : TYPE_CHANGEMENT,
            state           : 'added'
        })
        this._count++;
        this._mainObj.updateTotalAmount();
        this.display();
    },

    remove: function(id)
    {
        if (id.match(/^add_/))
        {
            var index = this._tabList.findIndex(function(item) { return item.id == id; });
            if (index > -1)
            {
                this._tabList.splice(index, 1);
            }
        }
        else
        {
            var referenceInfos = this.getReferenceInfos(id);
            referenceInfos.state = 'removed';
        }
        this.display();
        this._mainObj.updateTotalAmount();
    },

    display: function()
    {
        var tableEl = $ge('tableReferences');
        var tbodyEl = tableEl.tBodies[0];

        // Suppression des lignes
        while (tbodyEl.rows.length > 0)
        {
            Location.spinControlsManager.deleteSpinControl('qty_' + tbodyEl.rows[0].getAttribute('data-id'));
            Location.currenciesManager.deleteCurrency('amount_' + tbodyEl.rows[0].getAttribute('data-id'));
            tbodyEl.deleteRow(0);
        }

        var tab = this.getList();

        for (var i = 0; i < tab.length; i++)
        {
            var reference = tab[i];
            var rowEl = tbodyEl.insertRow(-1);
            rowEl.id = 'chp[' + reference.id + ']';
            rowEl.setAttribute('data-id', reference.id);

            this._displayLine(rowEl, reference);
        }
    },

    _displayLine: function(rowEl, reference)
    {
        var mySelf = this;
        var tabConfigs = this._mainObj._tabCfgs;
        var id = rowEl.getAttribute('data-id');


        var isRemoved = (reference.state == 'removed');

        Location.commonObj.toggleEltClass(rowEl, 'removed', isRemoved);

        // Changement de pièce
        var cellEl = rowEl.insertCell(-1);
        cellEl.className = 'chg';

        // - Quantité
        var qtyEl = window.document.createElement('INPUT');
        qtyEl.id   = 'qty_' + id;
        qtyEl.type = 'text';
        qtyEl.className = 'spinInput';
        cellEl.appendChild(qtyEl);
        qtyEl = Location.spinControlsManager.cloneSpinControl('qty_' + id, 'quantity_clone');
        qtyEl.setValue(reference.quantity, false);
        if (isRemoved || (!this.hasRight('references', 'edit') && reference.state != 'added'))
        {
            qtyEl.disable();
        }
        qtyEl.onchange = function()
        {
            mySelf.setQuantity(id, this.getValue());
        }

        // - Code Kimoce
        var codeEl = window.document.createElement('INPUT');
        codeEl.id   = 'artCode_' + id;
        codeEl.value = reference.artCode;
        codeEl.type = 'text';
        codeEl.placeholder = LOC_View.getTranslation('placeholderCode');
        codeEl.setAttribute('class', 'referenceCode');
        codeEl.readOnly = !this.hasRight('references', 'edit') && reference.state != 'added'; // Modification d'une nouvelle ligne
        codeEl.disabled = isRemoved;
        codeEl.onkeyup = function(e)
        {
            mySelf.setCode(id, this.value);
            Location.RepairSheet_References.searchManager.search(e, this, id);
        };
        cellEl.appendChild(codeEl);
        if (!reference.artCode)
        {
            codeEl.focus();
        }
        // - Résultats de recherche
        var searchResultEl = $('searchResult_clone').cloneNode(true);
        searchResultEl.id = 'artCode_' + id + '_search';
        cellEl.appendChild(searchResultEl);

        // Désignation
        var cellEl = rowEl.insertCell(-1);
        cellEl.className = 'label';
        var labelEl   = window.document.createElement('INPUT');
        labelEl.id    = 'label_' + id;
        labelEl.type = 'text';
        labelEl.setAttribute('class', 'labelReference');
        labelEl.value = reference.label;
        labelEl.disabled = (!this.hasRight('references', 'edit') && reference.state != 'added') || !reference.artCode;
        labelEl.onchange = function()
        {
            mySelf.setLabel(id, this.value);
        }
        cellEl.appendChild(labelEl);

        // Montant
        var cellEl = rowEl.insertCell(-1);
        cellEl.className = 'amount';
        var amountEl  = document.createElement('input');
        amountEl.id    = 'amount_' + id;
        amountEl.type = 'hidden';
        cellEl.appendChild(amountEl);

        amountEl = Location.currenciesManager.cloneCurrency(amountEl.id, 'amount_clone');
        amountEl.setValue(reference.amount);

        // Actions
        var cellEl = rowEl.insertCell(-1);
        cellEl.className = 'ctrls';
        if (this.hasRight('references', 'delete'))
        {
            buttonEl = window.document.createElement('a');
            buttonEl.href= '#';
            buttonEl.setAttribute('class', 'remove');
            buttonEl.onclick = function ()
            {
                if (confirm(LOC_View.getTranslation('confirmRemoveReference')))
                {
                    mySelf.remove(id);
                }
            }
            cellEl.appendChild(buttonEl);
        }

    }
};

/**
 * Manager de recherche de références
 */
Location.RepairSheet_References.searchManager = {

    _mainObj: null,
    searchDelay: 500,
    maxResultsCount: 8,
    _previousText : [],
    _timeout: null,
    _httpSearchArtObj: null,

    init: function(mainObj, tabCfgs)
    {
        this._mainObj = mainObj;
        this._tabCfgs = tabCfgs;
    },

    getSearchArticleUrl: function()
    {
        return this._tabCfgs.searchArticleUrl;
    },

    /**
     * Recherche d'article :
     * - Gère les touches spéciales : Entrée.
     * @param Object e L’événement source.
     * @param HTMLInputElement input L'élément qui a déclenché la recherche.
     * @param int id Identifiant de la référence
     */
    search : function(e, input, id)
    {
        var code = e.which || e.keyCode;
        var liSelectedElt = document.querySelector('#' + input.id + '_search li.selected');

        // On empêche l'action par défaut pour la touche Entrée
        if (code == 13)
        {
            e.preventDefault();
        }

        // Échap ou séparateur défini
        if (code == 27)
        {
            this._hideSearchResultPanel(input.id);
        }
        // Entrée
        else if (code == 13)
        {
            if (liSelectedElt)
            {
                liSelectedElt.onclick();
            }
        }
        // Flèche haut
        else if (code == 38)
        {
            var liPrevElt = liSelectedElt.previousSibling;
            if (liPrevElt && liPrevElt.length == 0)
            {
                liPrevElt = document.querySelector('#' + input.id + '_search li:last-child');
            }

            if (liPrevElt && liPrevElt != liSelectedElt)
            {
                Location.commonObj.addEltClass(liPrevElt, 'selected');
                Location.commonObj.removeEltClass(liSelectedElt, 'selected');
            }
        }
        // Flèche bas
        else if (code == 40)
        {
            var liNextElt = liSelectedElt.nextSibling;
            if (liNextElt && liNextElt.length == 0)
            {
                liNextElt = document.querySelector('#' + input.id + '_search li:first-child');
            }

            if (liNextElt && liNextElt != liSelectedElt)
            {
                Location.commonObj.addEltClass(liNextElt, 'selected');
                Location.commonObj.removeEltClass(liSelectedElt, 'selected');
            }
        }
        else
        {
            this._delaySearch(input, id);
        }

        // Stockage de la nouvelle valeur du champ
        this._previousText[input.id] = input.value;
    },

    /**
     * Masque le panneau de résultat de la recherche.
     */
    _hideSearchResultPanel : function(inputId)
    {
        if (this._httpSearchArtObj != null)
        {
            this._httpSearchArtObj.abort();
            this._httpSearchArtObj = null;
        }
        this._hide(document.querySelector('#' + inputId + '_search'));
    },


    /**
     * Planifie le lancement de la recherche.
     * @param HTMLInputElement input L'élément qui a déclenché la recherche.
     * @param id input L'identifiant de la référence.
     */
    _delaySearch : function(input, id)
    {
        if (this._previousText[input.id] != input.value)
        {
            // Chaîne de recherche
            var search = input.value;

            // Annulation de la précédente recherche planifiée
            if (this._timeout != null)
            {
                window.clearTimeout(this._timeout);
            }

            // Événement à planifier
            var mySelf = this;
            var event;
            if (search == '')
            {
                // Masquage du panneau de résultat
                event = function() {
                    mySelf._hideSearchResultPanel(input.id)
                };
            }
            else
            {
                // Lancement de la recherche
                event = function() {
                    mySelf._launch(id, search, function() {
                        mySelf._loadingResults(input.id);
                    }, function(response) {
                        if (response.errorMsgs)
                        {
                            mySelf._displayResultsError(input.id, response.errorMsgs);
                        }
                        else if (response.articles.length == 0)
                        {
                            mySelf._displayResultsError(input.id, 'searchArticle_noResult');
                        }
                        else
                        {
                            mySelf._resultReceived(id, search, response.articles, input.id);
                        }
                    }, function(response) {
                        mySelf._displayResultsError(input.id, response);
                    });
                };
            }

            // Planification de l'événement
            this._timeout = window.setTimeout(event, this.searchDelay);
        }
    },

    /**
     * Exécute la recherche.
     * @param int id Identifiant de la référence
     * @param string search Le texte recherché.
     * @param function beforeCallback La fonction exécutée avant le lancement de la recherche.
     * @param function doneCallback La fonction exécutée à la réception des résultats de la recherche.
     * @param function failCallback La fonction exécutée en cas d'erreur à la réception des résultats de la recherche.
     */
    _launch : function(id, search, beforeCallback, doneCallback, failCallback)
    {
        var mySelf = this;

        beforeCallback();

        new Promise(function(resolve, reject) {

            var referenceInfos = mySelf._mainObj.getReferenceInfos(id);

            if (mySelf._httpSearchArtObj != null)
            {
                mySelf._httpSearchArtObj.abort();
                mySelf._httpSearchArtObj = null;
            }

            if (!mySelf._httpSearchArtObj)
            {
                mySelf._httpSearchArtObj = new Location.HTTPRequest(mySelf.getSearchArticleUrl());
            }

            mySelf._httpSearchArtObj.setVar('articleCode', search);
            mySelf._httpSearchArtObj.setVar('maxResultsCount', mySelf.maxResultsCount);
            mySelf._httpSearchArtObj.setVar('type', 'multiple');

            mySelf._httpSearchArtObj.oncompletion = function() 
            {
                var tab = mySelf._httpSearchArtObj.getJSONResponse();
                resolve(tab);
            }
            mySelf._httpSearchArtObj.onerror = function ()
            {
                // Renvoie l'erreur si la requête n'a pas été abandonnée
                if (mySelf._httpSearchArtObj.getResponseStatus())
                {
                    reject();
                }
            }

            mySelf._httpSearchArtObj.send();
            
        }).then(function(response) {
            doneCallback(response);
        }, function(error) {
            failCallback(error);
        });
    },

    /**
     * Affiche le message d’attente du résultat de la recherche.
     * @param string elId L'identifiant de l'élément qui a déclenché la recherche.
     */
    _loadingResults : function(elId)
    {
        // Masquage des résultats précédents
        var ulElt = document.querySelector('#' + elId + '_search ul');
        this._hide(ulElt);

        // Affichage du message d'attente
        var loadingElt = document.querySelector('#' + elId + '_search .loading');
        this._show(loadingElt);

        // Masquage du message d'erreur
        var errorElt = document.querySelector('#' + elId + '_search .error');
        this._hide(errorElt);

        // Affichage du panneau de résultat
        var resultObj = document.querySelector('#' + elId + '_search');
        this._show(resultObj);
    },

    /**
     * Affiche le message d’erreur lors de la recherche de contact.
     * @param string elId L'identifiant de l'élément qui a déclenché la recherche.
     * @param string errorMsg Code du message d'erreur
     */
    _displayResultsError : function(elId, errorMsg)
    {
        // Masquage des résultats précédents
        var ulElt = document.querySelector('#' + elId + '_search ul');
        this._hide(ulElt);

        // Masquage du message d'attente
        var loadingElt = document.querySelector('#' + elId + '_search .loading');
        this._hide(loadingElt);

        // Affichage du message d'erreur
        var errorElt = document.querySelector('#' + elId + '_search .error');
        if (errorMsg)
        {
            errorElt.innerHTML = LOC_View.getTranslation(errorMsg);
        }
        else
        {
            errorElt.innerHTML = LOC_View.getTranslation('searchArticle_error');
        }
        this._show(errorElt);

        // Affichage du panneau de résultat
        var resultObj = document.querySelector('#' + elId + '_search');
        this._show(resultObj);
    },


    /**
     * Traite le résultat de la recherche.
     * @param int id L'identifiant de la référence
     * @param string search La recherche
     * @param array tabArticles Résultat de la recherche
     * @param string elId L'identifiant de l'élément qui a déclenché la recherche.
     */
    _resultReceived : function(id, search, tabArticles, elId)
    {
        var resultObj = document.querySelector('#' + elId + '_search');

        var nbArticles = tabArticles.length;
        if (nbArticles == 0)
        {
            // Masquage du panneau de résultat
           this._hide(resultObj);
        }
        else
        {
            var mySelf = this;

            // Vidage des résultats précédents
            var ulElt = document.querySelector('#' + elId + '_search ul');
            while (ulElt.childNodes.length > 0 )
            {
                //Suppression
                ulElt.removeChild(ulElt.firstChild);
            }
            var textElt = document.querySelector('#' + elId);

            for (var i = 0; i < nbArticles && i < this.maxResultsCount; i++)
            {
                var liElt = window.document.createElement('LI');
                liElt.setAttribute('data-value', JSON.stringify(tabArticles[i]));
                liElt.innerHTML = this._highlight(tabArticles[i]['code'], search) + ' - ' + tabArticles[i]['label'];
                if (tabArticles[i]['supplierCode'] && tabArticles[i]['supplierCode'] != tabArticles[i]['code'])
                {
                    var suppCodeEl = window.document.createElement('SPAN');
                    var suppCodeElContent = LOC_View.getTranslation('labelSupplierCode');
                    suppCodeEl.innerHTML = this._highlight(suppCodeElContent.replace(/<%code>/, tabArticles[i]['supplierCode']), search);
                    liElt.appendChild(suppCodeEl);
                }
                liElt.onmouseover = function() {
                    var selectedEl = document.querySelector('#' + elId + '_search ul > li.selected');
                    if (selectedEl)
                    {
                        Location.commonObj.removeEltClass(selectedEl, 'selected');
                    }
                    Location.commonObj.addEltClass(this, 'selected');
                };

                if (i == 0)
                {
                    Location.commonObj.addEltClass(liElt, 'selected');
                }

                liElt.onclick = function(e) {

                    var referenceInfos = mySelf._mainObj.getReferenceInfos(id);

                    var data = JSON.parse(this.getAttribute('data-value'));

                    var isMarginApplied = (mySelf._tabCfgs.isMarginAllowed || mySelf._tabCfgs.isMarginApplied);
                    referenceInfos.label        = data.label;
                    referenceInfos.artCode      = data.code;
                    referenceInfos.artExtId     = data.id;
                    referenceInfos.unitCost     = LOC_Common.round(data['localCost'], 2);
                    referenceInfos.unitMargin   = data.margin;
                    referenceInfos.margin       = data.margin;
                    referenceInfos.unitAmount   = (isMarginApplied && referenceInfos.unitCost != 0 ? 
                                                        LOC_Common.round(referenceInfos.unitCost + referenceInfos.unitMargin, 2) : referenceInfos.unitCost);

                    mySelf._mainObj.setQuantity(id, referenceInfos.quantity);

                    // Masquage du panneau de résultat
                    mySelf._hide(resultObj);

                    // Stockage de la nouvelle valeur du champ
                    mySelf._previousText[elId] = data.code;
                };

                ulElt.append(liElt);

                // Affichage des résultats
                mySelf._show(ulElt);
            }

            // Masquage du message d'attente
            var loadingElt = document.querySelector('#' + elId + '_search .loading');
            mySelf._hide(loadingElt);

            // Affichage du panneau de résultat
            mySelf._show(resultObj);
        }
    },

    /**
     * Met en évidence du texte dans un autre.
     * @param string text Le texte de base.
     * @param string highlight Le texte à mettre en évidence.
     * @return string Le texte de base avec le texte mis en évidence.
     */
    _highlight : function(text, highlight)
    {
        var tabComponents = highlight.split(' ');
        for (var i = 0; i < tabComponents.length; i++)
        {
            var regexp = new RegExp('(' + tabComponents[i].replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&") + ')', 'ig');
            text = text.replace(regexp, '<b>$1</b>');
        }
        return text;
    },

    _show: function(elt)
    {
        elt.style.display = 'inherit';
    },

    _hide: function(elt)
    {
        elt.style.display = 'none';
    }
};