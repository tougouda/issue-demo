var httpObj1;

var onLoadReducMode = -1;
var reducMode = -1; // 0 : remise globale, 1 : remise à la ligne
var srvTabIndex = -1; // index de tabulation des services (car ajout dynamique de ligne)

var tabReducWarning = [];

if (!window.Location)
{
    var Location = new Object();
}

function setOnLoadReducMode(val)
{
    reducMode = val;
    onLoadReducMode = val;
}

function setReducMode(val)
{
    reducMode = val;
}

function checkLinesReducPercent()
{
    var tabLines = document.querySelectorAll('#tableLine [id^="trLine_"]');

    for (var i = 0; i < tabLines.length; i++)
    {
        var type = tabLines[i].querySelector('input[id^="line\["][id$="\[type\]"]').value;
        checkLineReducPercent(i);
    }

}

function unsetLineReductions()
{
    var rdcPctObj;
    var count;
    
    count = document.getElementById('tableLine').rows.length - 1;
    for (var i = 0; i < count; i++)
    {
        rdcPctObj = Location.spinControlsManager.getSpinControl('line[' + i + '][reductionPercent]');
        rdcPctObj.setValue(0);
        $('line[' + i + '][finalAmount]').value = $('line[' + i + '][amount]').value;
    }

    count = document.getElementById('tableService').rows.length - 2;
    for (var i = 0; i < count; i++)
    {
        if ($('service[' + i + '][serviceId]').disabled == false)
        {
            rdcPctObj = Location.spinControlsManager.getSpinControl('service[' + i + '][reductionPercent]');
            if (rdcPctObj)
            {
                rdcPctObj.setValue(0);
            }
            $('service[' + i + '][finalAmount]').value = $('service[' + i + '][amount]').value;
        }
    }
}

/*
 * Fonction qui gère le barrage de la ligne (invoiced or not) 
 */
function changeLineInvoicing(lnNumber, type)
{
    type = (typeof type == 'undefined' ? 'line' : type);

    var toInv = $ge(type + '[' + lnNumber + '][isToInvoice]');
    var lineElt = $ge('tr' + capitalizeFirstLetter(type) + '_' + lnNumber);
    var flagToInvElt = $ge('sp' + capitalizeFirstLetter(type) + 'FlagToInvc_' + lnNumber);
    var flagNoInvElt = $ge('sp' + capitalizeFirstLetter(type) + 'FlagNoInvc_' + lnNumber);
    var btnToInvElt = $ge('sp' + capitalizeFirstLetter(type) + 'BtnToInvc_' + lnNumber);
    var btnNoInvElt = $ge('sp' + capitalizeFirstLetter(type) + 'BtnNoInvc_' + lnNumber);
    var reductionElt = Location.spinControlsManager.getSpinControl(type + '[' + lnNumber + '][reductionPercent]');

    // Désactivation
    if (toInv.value == 1)
    {
        // Valeur du champ "isToInvoice"
        toInv.value = 0;

        // Désactivation du spin control
        if (reductionElt)
        {
            reductionElt.disable();
        }

        // Barrage de la ligne
        lineElt.className = 'uninvoiced';

        // Changement des boutons
        flagToInvElt.style.display = 'none';
        flagNoInvElt.style.display = 'block';
        btnToInvElt.style.display = 'block';
        btnNoInvElt.style.display = 'none';
    }
    // Activation
    else
    {
        // Valeur du champ "isToInvoice"
        toInv.value = 1;

        // Activation du spin control
        if (reductionElt)
        {
            reductionElt.enable();
        }

        // Débarrage de la ligne
        lineElt.removeAttribute('class');

        // Changement des boutons
        flagToInvElt.style.display = 'block';
        flagNoInvElt.style.display = 'none';
        btnToInvElt.style.display = 'none';
        btnNoInvElt.style.display = 'block';
    }

    // Vérification des remises
    checkLineReducPercent(lnNumber);

    calculateAmounts();
}

function changeLineReduction(lnNumber)
{
    var qtyObj       = $('line[' + lnNumber + '][quantity]');
    var untAmtObj    = Location.currenciesManager.getCurrency('line[' + lnNumber + '][unitAmount]');
    var amtObj       = $('line[' + lnNumber + '][amount]');
    var fnlUntAmtObj = Location.currenciesManager.getCurrency('line[' + lnNumber + '][finalUnitAmount]');
    var fnlAmtObj    = Location.currenciesManager.getCurrency('line[' + lnNumber + '][finalAmount]');
    var rdcPctObj    = Location.spinControlsManager.getSpinControl('line[' + lnNumber + '][reductionPercent]');
    var rdcPctGblObj = Location.spinControlsManager.getSpinControl('reductionPercent');

    //Si remise globale et si saisi d'une remise à la ligne
    if (reducMode == 0 && parseFloat(rdcPctObj.getValue()) > 0)
    {
        //Si remise globale> 0
        if (rdcPctGblObj.getValue() > 0)
        {
            if (confirm(LOC_View.getTranslation('confirmResetGlobalReduc')))
            {
                //Suppression de la réduction globale
                unsetGlobalReduction();
            }
            else
            {
                rdcPctObj.setValue(0);
            }
        }
        else
        {
           //Suppression de la réduction globale
            unsetGlobalReduction();
        }
    }

    var rdcPct = parseFloat(rdcPctObj.getValue());
    if (rdcPct > 0)
    {
        //Applique la réduction à la ligne
        setReducMode(1);
    }

    // Affichage de l'alerte Réduction max atteinte
    checkLineReducPercent(lnNumber);

    //Remise à jour des différents champs
    var untAmt = parseFloat(untAmtObj.getValue());
    var fnlUntAmt = LOC_Common.round(untAmt * (1 - rdcPct / 100), 2);
    var fnlAmt = parseFloat(qtyObj.value) * fnlUntAmt;

    fnlUntAmtObj.setValue(fnlUntAmt);
    fnlAmtObj.setValue(fnlAmt);

    //Calcul montant final
    calculateAmounts();
}

/*
 * Remise à jour des champs et des totaux finaux sur modification d'une ligne de presta
 * */
function changeService(lnNumber)
{
    var qtyObj       = $('service[' + lnNumber + '][quantity]');
    var untAmtObj    = Location.currenciesManager.getCurrency('service[' + lnNumber + '][unitAmount]');
    var amtObj       = $('service[' + lnNumber + '][amount]');
    //PU Net
    var fnlUntAmtObj = Location.currenciesManager.getCurrency('service[' + lnNumber + '][finalUnitAmount]');
    //Prix facturé
    var fnlAmtObj    = Location.currenciesManager.getCurrency('service[' + lnNumber + '][finalAmount]');
    // Coût
    var costObj      = $('service[' + lnNumber + '][cost]');
    //Remise
    var rdcPctObj    = Location.spinControlsManager.getSpinControl('service[' + lnNumber + '][reductionPercent]');
    
    var qty = parseFloat($('service[' + lnNumber + '][quantity]').value);
    var untAmt = parseFloat(untAmtObj.getValue());

    var rdcPctGblObj = Location.spinControlsManager.getSpinControl('reductionPercent');
    
    //
    if (reducMode == 0 && parseFloat(rdcPctObj.getValue()) > 0)
    {
        if (rdcPctGblObj.getValue() > 0)
        {
            if (confirm(LOC_View.getTranslation('confirmResetGlobalReduc')))
            {
                unsetGlobalReduction();
                setReducMode(1);
            }
            else
            {
                rdcPctObj.setValue(0);
            }
        }
        else
        {
            unsetGlobalReduction();
            setReducMode(1);
        }
    }
    
    var rdcPct = parseFloat(rdcPctObj.getValue());
    if (rdcPct > 0)
    {
        setReducMode(1);
    }

    var untAmt = parseFloat(untAmtObj.getValue());
    var fnlUntAmt = LOC_Common.round(untAmt * (1 - rdcPct / 100), 2);
    var qty = parseFloat(qtyObj.value);
    var fnlAmt = qty * fnlUntAmt;
    var amt = qty * untAmt;

    fnlUntAmtObj.setValue(fnlUntAmt);
    fnlAmtObj.setValue(fnlAmt);
    amtObj.value = amt;
    costObj.value = amt;

    calculateAmounts();
}


/**
 * Vérifie les remises appliquées et affiche des alertes si besoin
 * @param int|null id - si null, ne recalcule que sur la remise globale
 */
function checkLineReducPercent(id)
{
    if (Number.isInteger(id))
    {
        var type           = $ge('line[' + id + '][type]').value;
        if (type == TYPE_CHANGEMENT)
        {
            var warningCellEl  = $ge('tdLineWarning_' + id);
            var reductionValue = Location.spinControlsManager.getSpinControl('line[' + id + '][reductionPercent]').getValue();
            // Cas des lignes non facturables
            if ($ge('line[' + id + '][isToInvoice]').value == 0)
            {
                reductionValue = 0;
            }

            LOC_Common.toggleEltClass(warningCellEl, 'warning', reductionValue > MAXREDUCTIONPERCENT_CHANGEMENT);
            _saveWarnings(reductionValue, id);
        }
    }

    // Dans tous les cas, vérification la remise globale
    var tabLines = document.querySelectorAll('#tableLine [id^="trLine_"]');
    var warningCellEl  = document.querySelector('#total td.warning');

    var hasChangementLine = false;
    var reductionValue;
    for (var i = 0; i < tabLines.length; i++)
    {
        var type        = tabLines[i].querySelector('input[id^="line\["][id$="\[type\]"]').value;
        var isToInvoice = tabLines[i].querySelector('input[id^="line\["][id$="\[isToInvoice\]"]').value;

        if (type == TYPE_CHANGEMENT && isToInvoice == 1)
        {
            hasChangementLine = true;
            break;
        }
    }
    if (hasChangementLine)
    {
        reductionValue = Location.spinControlsManager.getSpinControl('reductionPercent').getValue();
    }
    else
    {
        // Pas de ligne de type "changement de pièces" ou lignes non facturables
        reductionValue = 0;
    }

    LOC_Common.toggleEltClass(warningCellEl, 'on', reductionValue > MAXREDUCTIONPERCENT_CHANGEMENT);
    _saveWarnings(reductionValue, 'global');

}



/*
 * Fonction d'Ajout d'une ligne de services
 */
function addRowService()
{
    var table = document.getElementById('tableService');
    
    if (table != null)
    {
        var newlnNumber = table.rows.length - 2; // n° prochaine ligne
        var cntCols = table.rows[0].cells.length; // nombre de colonnes
        
        // intialise index de tabulation
        if (srvTabIndex == -1)
        {
            srvTabIndex = $('serviceServiceId_blank').getAttribute('tabindex');
        }
        
        // nouveau tr
        var row = table.insertRow(table.rows.length - 1);
        row.setAttribute('id', 'trService_' + newlnNumber);
        
        i = 0;
        
        // nouveau td : désignation
        var cell = row.insertCell(i++);
        cell.setAttribute('id', 'tdServiceLbl_' + newlnNumber);
        //Clonage de son élément html SELECT (depuis la vue)
        node = $('serviceServiceId_blank').cloneNode(true);
        node.setAttribute('id', 'service[' + newlnNumber + '][serviceId]');
        node.setAttribute('name', 'service[' + newlnNumber + '][serviceId]');
        node.setAttribute('tabindex', srvTabIndex++);
        //Affectation dans le td
        cell.appendChild(node);
        
        //Clonage du champ caché (Type de service = SRV)
        node = $('serviceType_blank').cloneNode(true);
        node.setAttribute('id', 'service[' + newlnNumber + '][type]');
        node.setAttribute('name', 'service[' + newlnNumber + '][type]');
        //Affectation du noeud
        cell.appendChild(node);
        
        //Clonage du champ caché (IsDeleted)
        node = $('serviceIsDeleted_blank').cloneNode(true);
        node.setAttribute('id', 'service[' + newlnNumber + '][isDeleted]');
        node.setAttribute('name', 'service[' + newlnNumber + '][isDeleted]');
        //Affectation du noeud
        cell.appendChild(node);

        // nouveau td : quantité
        var cell = row.insertCell(i++);
        cell.setAttribute('id', 'tdServiceQty_' + newlnNumber);
        node = $('serviceQuantity_blank').cloneNode(true);
        cell.setAttribute('style', 'text-align: right;');
        node.setAttribute('id', 'service[' + newlnNumber + '][quantity]');
        node.setAttribute('name', 'service[' + newlnNumber + '][quantity]');
        node.setAttribute('style', 'width: 40px;');
        node.setAttribute('maxlength', 5);
        //Affecte au td le noeud INPUT TEXT
        cell.appendChild(node);
        
        //Création d'un SPINCONTROL sur l'INPUT TEXT quantité
        var qtySpin = Location.spinControlsManager.createSpinControl('service[' + newlnNumber + '][quantity]', 1, null, 1, 10, 0);
        qtySpin.onchange = Function('changeService(' + newlnNumber + ')');
        qtySpin.setTabIndex(srvTabIndex++);
        
        // nouveau td : prix unitaire
        var cell = row.insertCell(i++);
        cell.setAttribute('id', 'tdServiceUntAmt_' + newlnNumber);
        node = $('serviceUnitAmount_blank').cloneNode(true);
        cell.setAttribute('style', 'text-align: right;');
        node.setAttribute('id', 'service[' + newlnNumber + '][unitAmount]');
        node.setAttribute('name', 'service[' + newlnNumber + '][unitAmount]');
        node.setAttribute('tabindex', srvTabIndex++);
        node.onchange = Function('changeService(' + newlnNumber + ')');
        cell.appendChild(node);

        //Création du currency
        var cur1 = Location.currenciesManager.cloneCurrency('service[' + newlnNumber + '][unitAmount]', 'serviceUnitAmount_blank');
        cur1.onchange = Function('changeService(' + newlnNumber + ')');

        // nouveau td : remise
        var cell = row.insertCell(i++);
        cell.setAttribute('id', 'tdServiceRdcPct_' + newlnNumber);
        cell.setAttribute('style', 'text-align: right;');
        node = $('serviceReductionPercent_blank').cloneNode(true);
        node.setAttribute('id', 'service[' + newlnNumber + '][reductionPercent]');
        node.setAttribute('name', 'service[' + newlnNumber + '][reductionPercent]');
        cell.appendChild(node);
        cell.appendChild(window.document.createTextNode(' %'));

        var spin1 = Location.spinControlsManager.createSpinControl('service[' + newlnNumber + '][reductionPercent]', 0, 100, 1, 10, 2);
        spin1.onchange = Function('changeService(' + newlnNumber + ')');
        spin1.setTabIndex(srvTabIndex++);
        
        // nouveau td : PU Net
        var cell = row.insertCell(i++);
        cell.setAttribute('id', 'tdServiceFnlUntAmt_' + newlnNumber);
        node = $('serviceFinalUnitAmount_blank').cloneNode(true);
        cell.setAttribute('style', 'text-align: right;');
        node.setAttribute('id', 'service[' + newlnNumber + '][finalUnitAmount]');
        node.setAttribute('name', 'service[' + newlnNumber + '][finalUnitAmount]');
        cell.appendChild(node);
        
        var cur1 = Location.currenciesManager.cloneCurrency('service[' + newlnNumber + '][finalUnitAmount]', 'serviceFinalUnitAmount_blank');
        
        // nouveau td : prix facturé
        var cell = row.insertCell(i++);
        cell.setAttribute('id', 'tdServiceFnlAmt_' + newlnNumber);
        node = $('serviceFinalAmount_blank').cloneNode(true);
        cell.setAttribute('style', 'text-align: right;');
        node.setAttribute('id', 'service[' + newlnNumber + '][finalAmount]');
        node.setAttribute('name', 'service[' + newlnNumber + '][finalAmount]');
        cell.appendChild(node);
        
        Location.currenciesManager.cloneCurrency('service[' + newlnNumber + '][finalAmount]', 'serviceFinalAmount_blank');
        
        // prix non remisé
        node = $('serviceAmount_blank').cloneNode(true);
        node.setAttribute('id', 'service[' + newlnNumber + '][amount]');
        node.setAttribute('name', 'service[' + newlnNumber + '][amount]');
        cell.appendChild(node);

        // coût
        var costEl = document.createElement('INPUT');
        costEl.type = 'hidden';
        costEl.setAttribute('id', 'service[' + newlnNumber + '][cost]');
        costEl.setAttribute('name', 'service[' + newlnNumber + '][cost]');
        costEl.value = 0;
        cell.appendChild(costEl);

        // nouveau td : flag état
        var cell = row.insertCell(i++);
        cell.setAttribute('id', 'tdServiceSta_' + newlnNumber);
        node = document.getElementById('spanServiceFlagToInvc').cloneNode(true);
        cell.appendChild(node);
        node = document.getElementById('serviceToInvc_blank').cloneNode(true);
        node.setAttribute('id', 'service[' + newlnNumber + '][isToInvoice]');
        node.setAttribute('name', 'service[' + newlnNumber + '][isToInvoice]');
        cell.appendChild(node);

        // nouveau td : bouton suppression
        var cell = row.insertCell(i++);
        cell.setAttribute('id', 'tdServiceRemBtn_' + newlnNumber);
        cell.setAttribute('style', 'text-align: right;');
        node = document.getElementById('spanServiceRembtn').getElementsByTagName('a')[0].cloneNode(true);
        node.onclick = Function('removeRowService(' + newlnNumber + ')');
        cell.appendChild(node);
    }
}

/*
 * Suppression des lignes "Autres prestations"
*/
function removeRowService(lnNumber)
{
    // 
    if (confirm(LOC_View.getTranslation('confirmRemoveService')))
    {
        
        //Dans les cas où la ligne est déjà enregistrée
        if ($('service[' + lnNumber + '][id]'))
        {
            Location.commonObj.addEltClass($('trService_' + lnNumber), 'removed');
        }
        //Dans le cas où la ligne n'est pas enregistrée (la ligne disparaît)
        else {
            document.getElementById('trService_' + lnNumber).style.display = 'none';
        }
        
        /*if (obj = $('service[' + lnNumber + '][id]'))
        {
            obj.disabled = true;
        }*/
        
        if (obj = $('service[' + lnNumber + '][serviceId]'))
        {
            obj.disabled = true;
            obj.style.textDecoration = 'line-through';
        }
        if (obj = $('service[' + lnNumber + '][type]'))
        {
            obj.disabled = true;
        }
        if (obj = Location.spinControlsManager.getSpinControl('service[' + lnNumber + '][quantity]'))
        {
            obj.setReadOnly();
        }
        if (obj = $('service[' + lnNumber + '][unitAmount]'))
        {
            obj.disabled = true;
        }
        if (obj = $('service[' + lnNumber + '][amount]'))
        {
            obj.disabled = true;
        }
        if (obj = $('service[' + lnNumber + '][cost]'))
        {
            obj.disabled = true;
        }
        if (obj = Location.spinControlsManager.getSpinControl('service[' + lnNumber + '][reductionPercent]'))
        {
            obj.setReadOnly();
        }
        if (obj = Location.currenciesManager.getCurrency('service[' + lnNumber + '][finalUnitAmount]'))
        {
            obj.disable();
        }
        if (obj = Location.currenciesManager.getCurrency('service[' + lnNumber + '][finalAmount]'))
        {
            obj.disable();
        }

        $('service[' + lnNumber + '][isDeleted]').value = 1;
     }
    
    calculateAmounts();
}


/*
 * Fonction qui change la réduction globale
 * */
function changeGlobalReduction()
{

    var rdcPctObj = Location.spinControlsManager.getSpinControl('reductionPercent');
    
    //On regarde si une remise sur ligne ou sur service est presente.
    var lineCpt = 0;
    var serviceCpt = 0;
    var currentObj = Location.spinControlsManager.getSpinControl('line[' + lineCpt + '][reductionPercent]');
    var lineReduction = false;
    
    //On boucle sur les lignes
    while(currentObj && !lineReduction)
    {
        if (currentObj.getValue() > 0)
        {
            lineReduction = true;
        }
        
        lineCpt++;
        
        currentObj = Location.spinControlsManager.getSpinControl('line[' + lineCpt + '][reductionPercent]');
    }
    
    var currentObj = Location.spinControlsManager.getSpinControl('service[' + serviceCpt + '][reductionPercent]');

    //on boucle sur les services
    while(currentObj && !lineReduction)
    {
        if (currentObj.getValue() > 0)
        {
            lineReduction = true;
        }
        
        serviceCpt++;
        
        currentObj = Location.spinControlsManager.getSpinControl('service[' + serviceCpt + '][reductionPercent]');
    }
    
   
    if (lineReduction)
    {
        if (reducMode == 1 && parseFloat(rdcPctObj.getValue()) > 0)
        {
            if (confirm(LOC_View.getTranslation('confirmResetLinesReduc')))
            {
                unsetLineReductions();
            }
            else
            {
                rdcPctObj.setValue(0);
            }
        }
    }
    else
    {
        unsetLineReductions();
    }
    
    var rdcPct = parseFloat(rdcPctObj.getValue());
    if (rdcPct > 0)
    {
        setReducMode(0);
    }

    // Si on a des lignes de changements de pièces
    checkLineReducPercent();

    var totAmt = parseFloat(Location.currenciesManager.getCurrency('totalAmount').getValue());
    
    if (Location.currenciesManager.getCurrency('totalAmount').getValue() == '' || totAmt <= 0)
    {
        calculateAmounts();
        totAmt = parseFloat(Location.currenciesManager.getCurrency('totalAmount').getValue());
    }
   
    
    calculateAmounts();

    Location.currenciesManager.getCurrency('finalAmount').setValue(LOC_Common.round(totAmt * (1 - rdcPct / 100), 2));
    Location.currenciesManager.getCurrency('reductionAmount').setValue(LOC_Common.round(- parseFloat(Location.currenciesManager.getCurrency('totalAmount').getValue()) * rdcPct / 100, 2));

   
}


/*
 * Fonction qui remet à zéro le % de remise globale
 * */
function unsetGlobalReduction()
{
    var rdcPctObj = Location.spinControlsManager.getSpinControl('reductionPercent');
    rdcPctObj.setValue(0);
    
    Location.currenciesManager.getCurrency('reductionAmount').setValue(0);
}


/*
 * Fonction qui calcule le montant total
 */
function calculateAmounts()
{
    var totCst = 0;
    var totAmt = 0;
    var totFnlAmt = 0;
    var residuesBaseAmount = 0;

    //Compte le nb de ligne du tableau des lignes de devis
    var nbLines = document.getElementById('tableLine').rows.length - 1;
    for (var i = 0; i < nbLines; i++)
    {
        var lineCost = parseFloat($('line[' + i + '][cost]').value);
        totCst += lineCost;

        // Si la ligne est à facturer
        if ($('line[' + i + '][isToInvoice]').value == 1)
        {
            var lineAmount      = parseFloat($('line[' + i + '][amount]').value);
            var lineFinalAmount = parseFloat(Location.currenciesManager.getCurrency('line[' + i + '][finalAmount]').getValue());

            totAmt             += lineAmount;
            totFnlAmt          += lineFinalAmount;
            residuesBaseAmount += lineFinalAmount;
        }
    }

    var residuesMode  = $ge('residuesMode').value;
    var residuesValue = $ge('residuesValue').value;
    var residuesAmount = (residuesMode == MODE_PERCENT ? residuesBaseAmount * residuesValue : residuesValue);

    //On compte le nb d'éléments de la table des Autres prestations
    var nbServices = document.getElementById('tableService').rows.length - 2;
    
    for (var i = 0; i < nbServices; i++)
    {
        var isResidues = $ge('trService_' + i).getAttribute('residues');

        // Mise à jour du montant de la participation au recyclage
        if (isResidues)
        {
            Location.currenciesManager.getCurrency('service[' + i + '][unitAmount]').setValue(residuesAmount);
            Location.currenciesManager.getCurrency('service[' + i + '][finalUnitAmount]').setValue(residuesAmount);
            Location.currenciesManager.getCurrency('service[' + i + '][finalAmount]').setValue(residuesAmount);
        }

        // Si la ligne n'est pas supprimée et marquée facturable,
        // on la comptabilise dans les totaux (isDeleted == 0)
        if ($('service[' + i + '][isToInvoice]').value == 1 && $('service[' + i + '][isDeleted]').value == 0)
        {
            totCst += parseFloat($('service[' + i + '][cost]').value);
            totAmt += parseFloat($('service[' + i + '][amount]').value);
            totFnlAmt += parseFloat(Location.currenciesManager.getCurrency('service[' + i + '][finalAmount]').getValue());
        }
    }

    $('totalCost').value = totCst;
    Location.currenciesManager.getCurrency('totalAmount').setValue(totAmt);


    var rdcPct = parseFloat(Location.spinControlsManager.getSpinControl('reductionPercent').getValue());
    // réduction globale
    if (rdcPct > 0)
    {
        Location.currenciesManager.getCurrency('finalAmount').setValue(LOC_Common.round(totAmt * (1 - rdcPct / 100), 2));
        //Mise a jour du montant de la réduction
        Location.currenciesManager.getCurrency('reductionAmount').setValue(LOC_Common.round(totAmt * rdcPct / 100, 2));
    }
    // réduction à la ligne
    else
    {
        Location.currenciesManager.getCurrency('totalAmount').setValue(totFnlAmt);
        Location.currenciesManager.getCurrency('finalAmount').setValue(totFnlAmt);

    }

}

function submitForm()
{
    var form = document.getElementsByTagName('form')[1];
    $('valid').value = 'ok';

    // Vérification des warnings sur les réductions
    if (tabReducWarning.length > 0)
    {
        if (confirm(LOC_View.getTranslation('confirmIgnoreReducWarning')))
        {
            //Chargement de l'image d'attente
            LOC_View.displayLoading(true);
            form.submit();
        }
    }
    else
    {
        //Image de chargement
        LOC_View.displayLoading(true);
        form.submit();        
    }

    return false;
}

/*
*
* Fonction du passage du devis en Abandon
*/
function abortEstimate(sheetId)
{
    var form = document.getElementsByTagName('form')[1];
    
    $('valid').value = 'abort';
    if (sheetId)
    {
    	$('reactivateSheet').value = '1';
    }
    //Chargement de l'image d'attente
    LOC_View.displayLoading(true);
    form.submit();
    
    return false;
}

/*
*
* Fonction du passage du devis en Facturé
*/
function invoiceEstimate()
{
    var form = document.getElementsByTagName('form')[1];
    
    $('valid').value = 'invoice';
    if (confirm(LOC_View.getTranslation('confirmInvoice')))
    {
        //Chargement de l'image d'attente
        LOC_View.displayLoading(true);
        form.submit();
    }
    return false;
}

/*
*
* Fonction pour remettre en cours le devis
*/
function reactivateEstimate(sheetId)
{
    var form = document.getElementsByTagName('form')[1];
    
    $('valid').value = 'reactivated';
    if (confirm(LOC_View.getTranslation('confirmReactivate')))
    {
        // Chargement de l'image d'attente
        LOC_View.displayLoading(true);
        form.submit();
    }
    return false;
}

/*
 * Fonction de passage du devis en Services
 * */
function turnIntoServices()
{

    var form = document.getElementsByTagName('form')[1];
    
    $('valid').value = 'services';

    if (confirm(LOC_View.getTranslation('confirmTurnIntoServices')))
    {
        //Chargement de l'image d'attente
        LOC_View.displayLoading(true);
        form.submit();
    }
    return false;
}

/*
 * 
 * Fonction de passage du devis en Jours
 * */
function turnIntoDays()
{
    var form = document.getElementsByTagName('form')[1];

    $('valid').value = 'days';
    
    if (confirm(LOC_View.getTranslation('confirmTurnIntoDays')))
    {
        //Chargement de l'image d'attente
        LOC_View.displayLoading(true);
        form.submit();
    }
    return false;
}

function resetForm()
{
    var form = document.getElementsByTagName('form')[0];
    
    form.reset();
    
    reducMode = onLoadReducMode;
}


function refresh()
{
    var form = document.getElementsByTagName('form')[1];
    $('valid').value = '';
    form.submit();
    
    return false;
}

/**
 * Rouvrir une fiche de remise en état
 *
 * @param int    sheetId - Identifiant de la fiche de remise en état
 * @param string url     - URL de la page de modification de la fiche de remise en état
 *
 * @return bool
 */
function showAbortEstimatePopup(estimateId, sheetId)
{
    // Mise à jour du numéro de la fiche dans le contenu de la popup
    $('estimateNo').innerHTML = estimateId;

    // Assignation des onclick aux boutons
    var mySelf = this;
    // Bouton "Oui et réactiver la FRE"
    obj = $('estimateAbortYesReopenSheetBtn');
    if (obj)
    {
        obj.onclick = function()
        {
            abortEstimate(sheetId);
            Location.modalWindowManager.hide();
        }
    }
    obj = $('estimateAbortYesBtn');
    // Bouton "Oui sans réactiver la fiche"
    if (obj)
    {
        obj.onclick = function()
        {
        	abortEstimate();
            Location.modalWindowManager.hide();
        }
    }
    obj = $('estimateAbortNoBtn');
    // Bouton "Non"
    if (obj)
    {
        obj.onclick = function()
        {
            Location.modalWindowManager.hide();
        }
    }

    // Apparition de la popup
    Location.modalWindowManager.show('abortEstimate', {contentType: 3, width: 470, height: 190});
}



/**
 * Cocher une photo pour la compression ZIP
 *
 * @return bool
 */
function onCheckPhoto()
{
    // Récupération des noms des fichiers des photos sélectionnées
    var tabPhotos = [];
    var tabElts = window.document.getElementsByName('photos[]');
    for (var i = 0; i < tabElts.length; i++)
    {
        if (tabElts[i].checked)
        {
            tabPhotos.push(tabElts[i].value);
        }
    }

    //Affichage du bouton Zip si au moins une photo est cochée
    var zipBtnEl = window.document.getElementById('zipBtn');
    if (tabPhotos.length > 0)
    {
        zipBtnEl.style.visibility = 'visible';
        zipBtnEl.href = zipBtnEl.href.replace(/\&photos\=.*/, '&photos=' + encodeURIComponent(JSON.stringify(tabPhotos)));
    }
    else
    {
        zipBtnEl.style.visibility = 'hidden';
    }
}

/**
 * Enregistrer les éléments avec un warning
 * @param reducPercent - Valeur de la réduction en pourcentage
 * @param id           - Identifiant à enregistrer
 */
function _saveWarnings(reducPercent, id)
{
    var index = tabReducWarning.indexOf(id);

    if (reducPercent > MAXREDUCTIONPERCENT_CHANGEMENT)
    {
        if (index == -1)
        {
            tabReducWarning.push(id);
        }
    }
    else if (index > -1)
    {
        tabReducWarning.splice(index, 1);
    }
}
