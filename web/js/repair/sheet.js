if (!window.Location)
{
    var Location = new Object();
}

Location.RepairSheet = function(countryId, tabCfgs, tabRights)
{
    this._countryId = countryId;
    this._tabCfgs = tabCfgs;
    this._tabRights = tabRights;

    this._id                  = null;
    this._agencyId            = null;
    this._contractId          = null;
    this._sheetState          = null;
    this._finalSheetCst       = 0;
    this._finalSheetAmt       = 0;
    this._toTurnInto          = null;
    this._isMarginAllowed     = 0;
    this._isMarginApplied     = 0;
    this._searchArticleUrl    = '';
    this._uploadPhotoUrl      = '';
    this._searchMachineUrl    = '';
    this._tabImages           = [];
    this._tabContractInfos    = [];

    this._httpSearchMachineInfosObj = null;

    this.oldCheckpointsObj = null;
    this.referencesObj     = null;
    this.servicesObj       = null;

    this.load();
}
Location.RepairSheet.prototype = {

    /**
     * Initialisation
     */
    load: function()
    {
        var tabInfos = this._tabCfgs.tabInfos;

        this._id                = tabInfos.id;
        this._agencyId          = tabInfos.agencyId;
        this._contractId        = tabInfos.contractId;
        this._sheetState        = tabInfos.sheetState;
        this._finalSheetCst     = tabInfos.finalSheetCst;
        this._finalSheetAmt     = tabInfos.finalSheetAmt;
        this._toTurnInto        = tabInfos.toTurnInto;
        this._isMarginAllowed   = this._tabCfgs.isMarginAllowed;
        this._isMarginApplied   = this._tabCfgs.isMarginApplied;
        this._searchArticleUrl  = this._tabCfgs.searchArticleUrl;
        this._uploadPhotoUrl    = this._tabCfgs.uploadPhotoUrl;
        this._searchMachineUrl  = this._tabCfgs.searchMachineUrl;
        this._tabImages         = this._tabCfgs.tabImages;

        this._tabContractInfos = tabInfos['tabMachineContracts'] ? tabInfos['tabMachineContracts'] : [];

        // Formatage des points de vérification existants
        // - Points de vérif "petite rénovation" ou "grosse réparation"
        var tabOldCheckpoints = this._tabCfgs.tabOldCheckpoints.filter(function(checkpoint) {
            return (!checkpoint.artCode && checkpoint.repairType);
        });

        // - Points de vérif avec référence => déplacé dans le tableau des références
        var tabOldReferences = this._tabCfgs.tabOldCheckpoints.filter(function(item) {
            return (item.artCode ? true : false);
        }).map(function(item) {
            item['checkpoint.id'] = null;
            if (item['checkpoint.tabInfos'])
            {
                item.label = item['checkpoint.tabInfos'].label;
            }
            return item;
        });
        var tabMergedReferences = this._tabCfgs.tabReferences.concat(tabOldReferences);

        this.oldCheckpointsObj = new Location.RepairSheet_OldCheckpoints(this, tabOldCheckpoints);
        this.referencesObj     = new Location.RepairSheet_References(this, tabMergedReferences);
        this.servicesObj       = new Location.RepairSheet_Services(this, this._tabCfgs.tabServices, this._tabCfgs.tabTempServices, this._tabCfgs.tabRefServices);

        this.displayDescriptionBlock(this._contractId);

        this.oldCheckpointsObj.init();
        this.referencesObj.init();
        this.servicesObj.init();

        $("sheetUsrId").value = $("sheetUserId").value;

        //Appel de la fonction qui charge les images
        parent.loadImages(this._tabImages, this._tabRights['photos']);

        this._initEvents();

        // Affichage
        this.display();
    },
    
    _initEvents: function()
    {
        mySelf = this;
        // Searchbox machine
        var searchBoxObj = Location.searchBoxesManager.getSearchBox('searchMachineId');
        searchBoxObj.onchange = function()
        {
            if (this.getValue() != 0)
            {
                this.displayNullItem(false);
            }
            mySelf.searchMachineInfos(
                               this.getValue(),
                               mySelf._searchMachineUrl,
                               mySelf._countryId,
                               mySelf._agencyId);
        };

        // Modification temps de travaux
        $ge('labHour').onchange = function() {
            mySelf.changeLabourTime();
        };
        $ge('labMinutes').onchange = function() {
            mySelf.changeLabourTime();
        };

        // Modification temps de trajet
        $ge('tvtHour').onchange = function() {
            mySelf.changeTravelTime();
        };
        $ge('tvtMinutes').onchange = function() {
            mySelf.changeTravelTime();
        };

        // Modification déplacement
        $ge('dspKm').onchange = function() {
            mySelf.changeDisplacement();
        };

        // Boutons d'actions
        $ge('validBtn').onclick = function() {
            mySelf.submitForm();
        };

        $ge('turnInServicesBtn').onclick = function() {
            mySelf.submitForm('services');
        };

        $ge('turnInDaysBtn').onclick = function() {
            mySelf.openTurnIntoDaysPopup();
        };

        $ge('validTurnInDaysBtn').onclick = function() {
            mySelf.submitForm('days');
        };

        $ge('daysNumber').onchange = function() {
            mySelf.calculTurnIntoDaysAmount();
        };

        $ge('turnInEstimateBtn').onclick = function() {
            mySelf.submitForm('estimate');
        };

        $ge('abortBtn').onclick = function() {
            mySelf.openAbortSheetPopup();
        };

        $ge('validAbortBtn').onclick = function() {
            mySelf.abortSheet();
        };

        $ge('reactivateBtn').onclick = function() {
            mySelf.reactivateSheet();
        };

        $ge('cancelBtn').onclick = function() {
            mySelf.cancelSheet();
        };
    },
    
    /**
     * Récupérer les droits
     */
    getRights: function()
    {
        return this._tabRights;
    },

    /**
     * Droits
     */
    hasRight : function(right, subRight)
    {
        var result = this._tabRights[right];
        if (result && typeof(subRight) != 'undefined')
        {
            var regex = new RegExp('^(|.*\|)' + subRight + '(|\|.*)$');
            return result.match(regex);
        }

        return (result != '' ? true : false);
    },

    /**
     * Affichage
     */
    display: function()
    {
        var mySelf = this;

        // IFrame pour l'upload des photos (Optimisation du onload)
        $('photosIframe').src = this._uploadPhotoUrl;


        // si on est dans le cas d'une fiche basculée ou d'une bascule en jours
        if (this._sheetState == 'fixed')
        {
            this.fixTotalAmount(this._finalSheetCst, this._finalSheetAmt);
        }

        this.changeLabourTime();
        this.changeTravelTime();
        this.changeDisplacement();

        this.oldCheckpointsObj.display();
        this.referencesObj.display();

        // ouverture de la popup de bascule en jour
        if (this._toTurnInto == 'days')
        {
            this.openTurnIntoDaysPopup();
        }

    },

    /**
     * Changement des heures de main d'oeuvre
     */
    changeLabourTime: function()
    {
        if ($('labHour').value != 0) 
        {
            labHour = parseInt($('labHour').value, 10);
        }
        else 
        {
            labHour = 0;
        }
        if ($('labMinutes').value != 0 && $('labMinutes').value <= 59) 
        {
            labMin = parseInt($('labMinutes').value, 10);
        }
        else 
        {
            labMin = 0;
            $('labMinutes').value = 0;
        }
        
        labHourCst = parseFloat($('labTimeCst').value.replace(",","."));
        labHourAmount = parseFloat($('labTimeAmount').value.replace(",","."));
        cost = LOC_Common.round((labHour + labMin / 60), 2) * labHourCst;
        amount = LOC_Common.round((labHour + labMin / 60), 2) * labHourAmount;
        Location.currenciesManager.getCurrency('labAmount').setValue(amount);
        $('labCst').value = cost;
        this.updateTotalAmount();
    },

    /**
     * Changement des heures de trajet
     */
    changeTravelTime: function()
    {
        if ($('tvtHour').value != 0) 
        {
            tvtHour = parseInt($('tvtHour').value, 10);
        }
        else 
        {
            tvtHour = 0;
        }
        if ($('tvtMinutes').value != 0 && $('tvtMinutes').value <= 59) 
        {
            tvtMin = parseInt($('tvtMinutes').value, 10);
        }
        else 
        {
            tvtMin = 0;
            $('tvtMinutes').value = 0;
        }
        
        tvtHourCst = parseFloat($('tvtHourCst').value.replace(",","."));
        tvtHourAmount = parseFloat($('tvtHourAmount').value.replace(",","."));
        cost = LOC_Common.round((tvtHour + tvtMin / 60), 2) * tvtHourCst;
        amount = LOC_Common.round((tvtHour + tvtMin / 60), 2) * tvtHourAmount;
        Location.currenciesManager.getCurrency('tvtAmount').setValue(amount);
        $('tvtCst').value = cost;
        this.updateTotalAmount();
    },

    /**
     * Changement kilometrage 
     */
    changeDisplacement: function()
    {
        if ($('dspKm').value != 0) 
        {
            dspKm = parseInt($('dspKm').value, 10);
        }
        else 
        {
            dspKm = 0;
        }
        dspKmCst = parseFloat($('dspKmCst').value.replace(",","."));
        dspKmAmount = parseFloat($('dspKmAmount').value.replace(",","."));
        cost = LOC_Common.round(dspKm * dspKmCst, 2);
        amount = LOC_Common.round(dspKm * dspKmAmount, 2)
        Location.currenciesManager.getCurrency('dspAmount').setValue(amount);
        $('dspCst').value = cost;
        this.updateTotalAmount();
    },

    /**
     * Fixer le coût total et le montant total
     * (utilisé dans le cas d'une FRE basculée ou abandonnée
     */
    fixTotalAmount: function(cost, amount)
    {
        $('sheetCst').value = cost;//Maj du champ Coût total de la FRE
        Location.currenciesManager.getCurrency('sheetAmt').setValue(amount);//Maj du champ Montant total de la FRE
    },


    /**
     * Affichage client et chantier du contrat
     */
    displayDescriptionBlock: function(contractId)
    {
        var mySelect = $('contractId');
        var oldContractId = contractId;

        while (mySelect.options && mySelect.options[0])
        {
            mySelect.removeChild(mySelect.options[0]);
        }

        var nbContracts = this._tabContractInfos.length;
        if (nbContracts > 0)
        {
            var isContractInList = false;
            for (var i = 0; i < nbContracts; i++)
            {
                var option = new Option(this._tabContractInfos[i]['code'], this._tabContractInfos[i]['id']);
        
                //Alimentation de la liste déroulante des contrats
                try
                {
                    mySelect.add(option, null);
                }
                catch(ex)
                {
                    mySelect.add(option);
                }
        
                // Vérification que l'Id du contrat est dans la liste des contrats 
                if (!isContractInList && oldContractId == this._tabContractInfos[i]['id'])
                {
                  isContractInList = true;
                }
            }

            //Si le contrat est dans la liste, on le préselectionne
            var selectedContract;
            if (isContractInList)
            {
                mySelect.value = oldContractId;
                selectedContract = mySelect.selectedIndex;
            }
            else
            {
                selectedContract = 0;
            }


            var mySelf = this;
            mySelect.onchange = function() {
                mySelf._contractId = this.value;
                mySelf.displayContractInfo(this.selectedIndex);

                var unitDayAmtInput = Location.currenciesManager.getCurrency('unitDayAmt');
                if (unitDayAmtInput)
                {
                    unitDayAmtInput.setValue(mySelf._tabContractInfos[this.selectedIndex]['dayAmount']);
                }
                $('unitDayAmtHidden').value = mySelf._tabContractInfos[this.selectedIndex]['dayAmount'];

                //Cas de bascule en jours depuis le DRE (toTurnInto = Days)
                mySelf.updateTotalAmount();
            };

            mySelect.onchange();

        }
        else
        {
            this.displayContractInfo(-1);

            if ($('servicesDisabled'))
            {
                $('servicesDisabled').style.visibility = 'visible';
            }
            if ($('otherServicesDisabled'))
            {
                $('otherServicesDisabled').style.visibility = 'visible';
            }
            if ($('photosDisabled'))
            {
                $('photosDisabled').style.visibility = 'visible';
            }
        }
    },

    displayContractInfo: function(index)
    {
        if(index != -1)
        {
            $('customerName').value = this._tabContractInfos[index]['customerName'];
            $('siteLabel').value = this._tabContractInfos[index]['siteLabel'];
            $('siteAddress').value = this._tabContractInfos[index]['siteAddress'];
            $('sitePostalLabel').value = this._tabContractInfos[index]['sitePostalLabel'];
            $('invoiceState').value = this._tabContractInfos[index]['invoiceState'];
            $('proformaFlags').value = this._tabContractInfos[index]['proformaFlags'];
            $('contractLink').href = this._tabContractInfos[index]['url'];
            $('contractLink').onclick = function(){ return true; };
            flagsValue = this._tabContractInfos[index]['proformaFlags'];
            if ((this._tabContractInfos[index]['proformaFlags'] & DOCFLAG_EXISTACTIVED) || this._tabContractInfos[index]['proformaFlags'] & DOCFLAG_CT_ISCUSTOMERPFREQONCREATION == 1)
            {
                $('proformaGenerated').style.visibility = '';
            }
            else
            {
                $('proformaGenerated').style.visibility = 'hidden';
            }
        }
        else
        {
            $('customerName').value = '';
            $('siteLabel').value = '';
            $('siteAddress').value = '';
            $('sitePostalLabel').value = '';
            if ($('contractLink'))
            {
                $('contractLink').style.visibility = 'hidden';
            }
            $('proformaGenerated').style.visibility = 'hidden';
        }
        
    },
    /**
     * Fonction de recherche info machine
     */
    searchMachineInfos: function(id, url, countryId, agencyId)
    {
        var mySelf = this;
        //Cas où on a aucune machine de sélectionnée
        if (id == 0)
        {
            this._tabContractInfos = [];
            this.displayDescriptionBlock();
        }
        //Cas où les blocs s'affichent
        else
        {
            var imgDiv = $('alertsLoading');
            imgDiv.style.visibility = 'visible';

            if (!this._httpSearchMachineInfosObj)
            {
                this._httpSearchMachineInfosObj = new Location.HTTPRequest(url);
            }
            this._httpSearchMachineInfosObj.setVar('machineId', id );
            this._httpSearchMachineInfosObj.setVar('countryId', countryId);
            this._httpSearchMachineInfosObj.setVar('agencyId', agencyId);
            this._httpSearchMachineInfosObj.setVar('familyId', $('familyId').value);
            this._httpSearchMachineInfosObj.oncompletion = function() 
            {
                var tab = this.getJSONResponse();
                if (tab['errorMsg'] != '')
                {
                    alert(tab['errorMsg']);
                    Location.searchBoxesManager.getSearchBox("searchMachineId").setValue($('machineId').value, false);
                }
                else
                {
                    $('machineId').value = tab['machineId'];
                    //Affectation => valid eq changeFamily (on recharge car changement de famille)
                    if (tab['isFamilyChanged'] )
                    {
                        $('familyId').value = 0;
                        $('valid').value = 'changeFamily'
                        document.forms[1].submit();
                    }
                    
                    if (tab['tabMachineContracts'])
                    {
                        mySelf._tabContractInfos = tab['tabMachineContracts'];//Récupération des infos du contrat
                        mySelf.displayDescriptionBlock();
                    }
                }
                var imgDiv = $('alertsLoading');
                imgDiv.style.visibility = 'hidden';
            }
            this._httpSearchMachineInfosObj.send();
       }


    },


    /**
     * Recalcul du cout total
     */
    updateTotalAmount: function()
    {
        var cost = 0;
        var amount = 0;

        //Parcourt la liste des anciens points de vérif
        var oldCheckpointsList = this.oldCheckpointsObj.getList();
        for(var i = 0; i < oldCheckpointsList.length; i++)
        {
            if (oldCheckpointsList[i].state != 'removed')
            {
                cost   += oldCheckpointsList[i].cost;
                amount += oldCheckpointsList[i].amount;
            }

            cost = LOC_Common.round(cost, 4);
            amount = LOC_Common.round(amount, 4);
        }

        // Parcourt la liste des références
        var referencesList = this.referencesObj.getList();
        for (var i = 0; i < referencesList.length; i++)
        {
            var referenceInfos = referencesList[i];
            if (referenceInfos['label'] != '' && referenceInfos.artCode != '' && referenceInfos.state != 'removed')
            {
                cost   += parseFloat(referenceInfos.cost);
                amount += referenceInfos.cost;

                if ((this._tabCfgs.isMarginAllowed || this._tabCfgs.isMarginApplied) && cost != 0)
                {
                    amount += referenceInfos.unitMargin * referenceInfos.quantity;
                }

                cost = LOC_Common.round(cost, 4);
                amount = LOC_Common.round(amount, 4);
            }
        }

        //Parcourt la liste des services (prestas techniques)
        var servicesList = this.servicesObj.getList();
        for(var i in servicesList)
        {
            if( servicesList[i] != null)
            {
                var tabServiceInfos = this.servicesObj.getRefInfos(servicesList[i].getSrvId());
                // Coût de la ligne du service paramétré
                var lineCost;
                if (servicesList[i].getSelectedType() == 'EXT')
                {
                    lineCost = parseFloat(servicesList[i].getCost());
                    if (tabServiceInfos)
                    {
                        if (servicesList[i].getIsMarginAllowed() == 1)
                        {
                            lineAmount = lineCost + lineCost * $('extSrvMargin').value;
                        }
                        else if (servicesList[i].getIsMarginAllowed() == 0)
                        {
                            lineAmount = lineCost;
                        }
                    }
                    else
                    {
                        lineAmount = parseFloat(servicesList[i].getAmount());
                    }
                    cost += lineCost;
                    amount += lineAmount;
                }
                else if (servicesList[i].getSelectedType() == 'INT')
                {
                    lineQuantity = parseFloat(servicesList[i].getQuantity());
                    if (tabServiceInfos)
                    {
                        // Cas où la presta est de type INM
                        if (servicesList[i].getType() == 'INM')
                        {
                            lineCost   = parseFloat(servicesList[i].getIntCost());
                            lineAmount = parseFloat(servicesList[i].getAmount());
                        }
                        else
                        {
                            lineCost   = parseFloat(tabServiceInfos['cost']);
                            lineAmount = parseFloat(tabServiceInfos['amount']) * lineQuantity;
                        }
                    }
                    else
                    {
                        lineCost   = parseFloat(servicesList[i].getIntCost());
                        lineAmount = parseFloat(servicesList[i].getAmount());
                    }
                    cost += lineCost * lineQuantity;
                    amount += lineAmount;
                }
                else
                {
                    lineCost = parseFloat(servicesList[i].getCost());
                    amount += lineCost;
                }
                cost   = LOC_Common.round(cost, 4);
                amount = LOC_Common.round(amount, 4);
            }
        }
        //Rajout des Autres Prestas
        cost   += parseFloat($('labCst').value);
        cost   += parseFloat($('tvtCst').value);
        cost   += parseFloat($('dspCst').value);
        amount += parseFloat(Location.currenciesManager.getCurrency('labAmount').getValue());
        amount += parseFloat(Location.currenciesManager.getCurrency('tvtAmount').getValue());
        amount += parseFloat(Location.currenciesManager.getCurrency('dspAmount').getValue());
        cost   = LOC_Common.round(cost, 2);
        amount = LOC_Common.round(amount, 2);
        $('sheetCst').value = cost;//Maj du champ Coût total de la FRE
        Location.currenciesManager.getCurrency('sheetAmt').setValue(amount);//Maj du champ Montant total de la FRE

        var amountType = '';
        var selectedContract;
        if ($('contractId') && $('contractId').selectedIndex != -1)
        {
            selectedContract = $('contractId').selectedIndex;
            amountType = this._tabContractInfos[selectedContract]['amountType'];
        }

        // Bouton de passage en services
        if ($('turnInServicesBtn'))
        {
            var isVisible = (this.hasRight('actions', 'services') &&
                             amount <= $('sheetMaxAmount').value && $('invoiceState').value != 'F' && amount != 0 && 
                             !($('proformaFlags').value & DOCFLAG_EXISTACTIVED) && this._tabContractInfos[selectedContract]['customerIsProformaRequired'] == 0);
            $('turnInServicesBtn').style.visibility = (isVisible ? 'visible' : 'hidden');
        }
        // Bouton de passage en jours
        if ($('turnInDaysBtn'))
        {
            var isVisible = (this.hasRight('actions', 'days') &&
                             amount <= $('sheetMaxAmount').value && $('invoiceState').value != 'F' &&
                             $('unitDayAmtHidden').value != 0 && amountType == 'day' && amount != 0 && 
                             !($('proformaFlags').value & DOCFLAG_EXISTACTIVED) && this._tabContractInfos[selectedContract]['customerIsProformaRequired'] == 0);
            $('turnInDaysBtn').style.visibility = (isVisible ? 'visible' : 'hidden');
        }
        // Bouton d'annulation
        if ($('cancelBtn'))
        {
            var isVisible = this.hasRight('actions', 'cancel');
            $('cancelBtn').style.visibility = (isVisible ? 'visible' : 'hidden');
        }
        // Bouton d'abandon
        if ($('abortBtn'))
        {
            var isVisible = this.hasRight('actions', 'abort');
            $('abortBtn').style.visibility = (isVisible ? 'visible' : 'hidden');
        }
        // Bouton Passage en DRE
        if ($('turnInEstimateBtn'))
        {
            var isVisible = (this.hasRight('actions', 'repairEstimate') &&
                             amount != 0);
            $('turnInEstimateBtn').style.visibility = (isVisible ? 'visible' : 'hidden');
        }
    },
    

    /*
     * Fonction de soumission du formulaire 
    */
    submitForm: function(turnIn, action)
    {
        if (typeof(action) == 'undefined')
        {
            action = 'ok';
        }
        var errorsTab = new Array();

        // Vérification de l'utilisateur
        if ($('sheetUserId').value == '')
        {
            errorsTab.push('errorUser');
        }

        // Vérification de la date
        var sheetDate = $('sheetDate').value;
        if (sheetDate == '')
        {
            errorsTab.push('errorDate');
        }
        else if (!this._isDateValid(sheetDate))
        {
            errorsTab.push('errorDateSup');
        }

        // Vérification des références
        var tabReferences = this.referencesObj.getList();
        for (var i = 0; i < tabReferences.length; i++)
        {
            if (tabReferences[i].artCode == '')
            {
                array_push_unique(errorsTab, 'errorKimoce');
            }
        }

        // vérification du label non vide pour les champs libres
        for (var i = 0; i < tabReferences.length; i++)
        {
            if (tabReferences[i]['label'] == '')
            {
                array_push_unique(errorsTab, 'errorLabel');
            }
        }

        if (errorsTab.length == 0)
        {
            var form = window.document.forms[1];

            $('valid').value = action;

            $('tabOldCheckpoints').value = JSON.stringify(this.oldCheckpointsObj.getList());
            $('tabReferences').value = JSON.stringify(this.referencesObj.getList());

            if (typeof(turnIn) != 'undefined')
            {
                //Validation de la bascule en devis
                if(turnIn == 'estimate')
                {
                    $('turnIn').value = turnIn;

                    if (confirm(LOC_View.getTranslation('confirmTurnInEstimate')))
                    {
                        //Image de chargement
                        LOC_View.displayLoading(true);
                        form.submit();
                    }
                }
                //Validation de la bascule en services
                else if (turnIn == 'services')
                {
                    $('turnIn').value = turnIn;
                    
                    if (confirm(LOC_View.getTranslation('confirmTurnInServices')))
                    {
                        //Image de chargement
                        LOC_View.displayLoading(true);
                        form.submit();
                    }
                }
                //Validation de la bascule en jours
                else if (turnIn == 'days')
                {
                    $('turnIn').value = turnIn;
                    
                    if (confirm(LOC_View.getTranslation('confirmTurnInDays')))
                    {
                        //Image de chargement
                        LOC_View.displayLoading(true);
                        form.submit();
                    }
                }
            }
            //Validation de la FRE
            else
            {
                $('turnIn').value = '';
                //Image de chargement
                LOC_View.displayLoading(true);
                form.submit();
            }

            return false;
        }
        //Affichage des erreurs
        else
        {
            this._showErrorBlock(errorsTab);
        }
    },


    /*
     * Ouverture de la popup d'abandon de la FRE
     */
    openAbortSheetPopup: function()
    {
        //Création de la popup
        Location.modalWindowManager.show('abortSheet', {contentType: 3, width: 400, height: 180});
    },

    /*
     * 
     * Abandon de la FRE
     * */
    abortSheet: function()
    {
        var errorsTab = new Array();

        //Commentaire d'abandon saisi en supprimant les espaces de début et de fin de chaine
        $('abortCommentHidden').value = $('abortComment').value.replace(/(^\s*)|(\s*$)/g,'');
        if ($('abortCommentHidden').value == '')
        {
            errorsTab.push('errorAbortSheet');
        }

        if (errorsTab.length == 0)
        {
            if (confirm(LOC_View.getTranslation('confirmAbortSheet')))
            {
                this.submitForm(undefined, 'abort');
            }
            return false;
        }
        //Affichage des erreurs
        else
        {
            Location.modalWindowManager.hide();
            this._showErrorBlock(errorsTab);
        }
    },

    /**
     * Annulation de la fiche
     */
    cancelSheet: function()
    {
        if (confirm(LOC_View.getTranslation('confirmCancelSheet')))
        {
            this.submitForm(undefined, 'cancel');
        }
        return false;
    },


     /*
      * Remise en cours d'une FRE abandonnée
      */ 
     reactivateSheet: function()
     {
         var form = document.getElementsByTagName('form')[1];
         $('valid').value = 'reactivate';
         if (confirm(LOC_View.getTranslation('confirmReactivateSheet')))
         {
             //Image de chargement
             LOC_View.displayLoading(true);
             form.submit();
         }
         return false;
     },

    /*
     * Ouverture de la popup Bascule en jours sur le contrat
     */
    openTurnIntoDaysPopup: function()
    {
        var sheetAmt = Location.currenciesManager.getCurrency('sheetAmt').getValue();
        var unitDayAmt = Location.currenciesManager.getCurrency('unitDayAmt').getValue();
        var daysNumber = sheetAmt / unitDayAmt;

        Location.currenciesManager.getCurrency("turnInDaysSheetAmt").setValue(sheetAmt);
        Location.spinControlsManager.getSpinControl("daysNumber").setValue(LOC_Common.round(daysNumber, 0));

        Location.currenciesManager.getCurrency('contractAmt').setValue(unitDayAmt * daysNumber);
        
        $('daysNumberHidden').value = LOC_Common.round(daysNumber, 0);
        $('contractAmtHidden').value = unitDayAmt * daysNumber;
        
        //Création de la popup
        Location.modalWindowManager.show('turnIntoDays', {contentType: 3, width: 300, height: 160});
    },

    /*
     * Bascule en jours : Calcul du prix sur le contrat
     */
    calculTurnIntoDaysAmount: function()
    {
        var amount = Location.currenciesManager.getCurrency("unitDayAmt").getValue() *
                        Location.spinControlsManager.getSpinControl("daysNumber").getValue();

        Location.currenciesManager.getCurrency("contractAmt").setValue(LOC_Common.round(amount,2));
        
        $('daysNumberHidden').value = Location.spinControlsManager.getSpinControl("daysNumber").getValue();
        $('contractAmtHidden').value = LOC_Common.round(amount,2);
    },

    /**
     * Fonction qui renvoit true si la date est valide (inférieure à la date du jour)
     */
    _isDateValid: function(date)
    {
        sheetDateObj  = new Date(date);
        currentDateObj = new Date();

        return (sheetDateObj <= currentDateObj ? true : false);;
    },

    /*
     * Fonction qui affiche le bloc (rouge) de toutes les erreurs de la FRE
     */

    _showErrorBlock: function(errorsTab)
    {
        var errorBlock = $("errorBlock");
        var errorsTable = $("errorsTable");
        
        //Dernière ligne
        var buttonRow = $('buttonRow').cloneNode(true);

        //Boucle sur la table errorsTable et suppression de tous les éléments (tr, etc)
        while (errorsTable.childNodes.length > 0 )
        {
            //Suppression
            errorsTable.removeChild(errorsTable.firstChild);
        }
        
        //Création de la liste
        var ulElt = window.document.createElement('ul');
        ulElt.style.fontWeight = 'bold';

        //Parcours des messages d'erreur 
        for (var i in errorsTab)
        {
            ulElt.appendChild(window.document.createElement('br'));

            var liElt = window.document.createElement('li');
            liElt.style.fontWeight = 'bold';
            liElt.innerHTML = LOC_View.getTranslation(errorsTab[i]);//Affichage du message entre les bornes li

            //Affectation du li au ul
            ulElt.appendChild(liElt);

            // Affichage du bloc en erreur
            if (errorsTab[i] == 'errorUser')
            {
                var searchBox = Location.searchBoxesManager.getSearchBox('sheetUserId');
                searchBox.setRequired(true);
            }
            if (in_array(errorsTab[i], ['errorDate', 'errorDateSup']))
            {
                Location.commonObj.addEltClass($('sheetDate.masked'), 'error');
            }
            if (in_array(errorsTab[i], ['errorKimoce', 'errorLabel']))
            {
                Location.commonObj.addEltClass($('tableReferences'), 'error');
            }
        }

        var tdElt = window.document.createElement('td');
        tdElt.appendChild(ulElt);//On met le ul dans le td

        var trElt = window.document.createElement('tr');
        trElt.appendChild(tdElt);//On met le td dans le tr

        errorsTable.appendChild(trElt);//On met le tr dans la table
        errorsTable.appendChild(buttonRow);//On met le tr du bouton dans la table

        errorBlock.style.display = 'block';
    }


};
