$(document).ready(function() {
    jQuery.expr[':'].contains = function(a, i, m) {
        return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
    };
});


(function($)
{
    /**
     * Fonction de recherche d'un élément dans un tableau
     *
     * @param string elt  Element à rechercher
     * @param Array  tab  Tableau
     * @param string prop Propriété dans le tableau (facultatif)
     */
    var inArray = function(elt, tab, prop)
    {
        var i = 0;
        var nb = tab.length;
        while (i < nb && (prop ? tab[i][prop] : tab[i]) != elt)
        {
            i++;
        }
        return (i < nb ? i : false);
    }

    /**
     * Classe principale
     *
     */
    var ajaxTable = function(containerEl, tabCfgs)
    {
        this._containerEl = containerEl;
        this._tabCfgs = tabCfgs;
        this._tableEl = null;
        this._isLoading = false;

        this._tabSorts   = (tabCfgs.sorts || []);
        this._tabDefaultActivesFilters = (tabCfgs.defaultActivesFilters || {});
        this._tabActivesFilters = (tabCfgs.activesFilters || $.extend(true, {}, this._tabDefaultActivesFilters));
        this._startIndex = (tabCfgs.startIndex || 0);
        this._pageSize   = (tabCfgs.pageSize || 10);

        this._tabData = null;

        this.oninsertcell       = null;
        this.onbeforeupdatedata = null;
        this.onupdatedata       = null;
    }

    ajaxTable.prototype = {

        /**
         * Récupérer les configurations du tableau
         *
         * @return Object
         */
        getConfigs : function()
        {
            return this._tabCfgs;
        },

        /**
         * Récupérer les données du tableau
         *
         * @return Object
         */
        getData : function()
        {
            return this._tabData;
        },

        /**
         * Récupérer la liste des tris
         *
         * @return Array
         */
        getSorts : function()
        {
            return this._tabSorts;
        },

        /**
         * Récupérer le nombre de lignes par page
         *
         * @return int
         */
        getPageSize : function()
        {
            return this._pageSize;
        },

        /**
         * Récupérer l'index de la ligne de départ
         *
         * @return int
         */
        getStartIndex : function()
        {
            return this._startIndex;
        },

        /**
         * Réinitialiser les filtres à leurs valeurs par défaut
         *
         * @param Object tabFiltersValues Définir des valeurs de filtres
         * @return bool
         */
        resetFilters : function(tabFiltersValues)
        {
            if (this._isLoading)
            {
                return false;
            }

            var tabValues = {};
            $.extend(true, tabValues, this._tabDefaultActivesFilters);
            if (typeof tabFiltersValues == 'object')
            {
                $.extend(true, tabValues, tabFiltersValues);
            }

            return this.clearFilters(tabValues);
        },

        /**
         * Supprimer tous les filtres
         *
         * @param Object tabFiltersValues Définir des valeurs de filtres
         * @return bool
         */
        clearFilters : function(tabFiltersValues)
        {
            if (this._isLoading)
            {
                return false;
            }

            this._tabActivesFilters = {};
            if (typeof tabFiltersValues == 'object')
            {
                $.extend(true, this._tabActivesFilters, tabFiltersValues);
            }

            // Mise à jour du tableau
            this._startIndex = 0;
            this.update();

            return true;
        },

        /**
         * Changer de page
         *
         * @param int i Index de la page
         * @return bool
         */
        setCurrentPage : function(i)
        {
            var startIndex = this._pageSize * i;
            if (this._isLoading || startIndex == this._startIndex)
            {
                return false;
            }
            this._startIndex = startIndex;

            // Mise à jour du tableau
            this.update();

            return true;
        },

        /**
         * Définir les valeurs d'un filtre
         *
         * @param string id        Identifiant du filtre
         * @param Array  tabValues Valeurs
         * @return bool
         */
        setFilterValues : function(id, tabValues)
        {
            var idx = this.getFilterIndexById(id);
            if (this._isLoading || idx === false)
            {
                return false;
            }
            if (!(typeof tabValues == 'object' && tabValues instanceof Array))
            {
                // Convertion du type scalaire en Array
                tabValues = (typeof tabValues != 'undefined' && tabValues !== null ? [tabValues] : []);
            }

            // Détection des modifications
            if (JSON.stringify(this.getFilterValues(id).sort()) == JSON.stringify(tabValues.sort()))
            {
                return false;
            }

            if (tabValues.length == 0)
            {
                // Filtre inactif
                if (this._tabActivesFilters[id])
                {
                    // On supprime le filtre actif si il existait
                    delete (this._tabActivesFilters[id]);
                }
            }
            else
            {
                // Mise à jour du filtre actif
                this._tabActivesFilters[id] = tabValues;
            }

            // Mise à jour du tableau
            this._startIndex = 0;
            this.update();

            return true;
        },

        /**
         * Récupérer l'index d'une colonne grâce à son identifiant
         *
         * @param string id Identifiant
         * @return int|false
         */
        getColumnIndexById : function(id)
        {
            return inArray(id, this._tabCfgs.columns, 'id');
        },

        /**
         * Récupérer l'index d'un filtre grâce à son identifiant
         *
         * @param string id Identifiant
         * @return int|false
         */
        getFilterIndexById : function(id)
        {
            return inArray(id, this._tabCfgs.filters, 'id');
        },

        /**
         * Récupérer les valeurs d'un filtre
         *
         * @param string id Identifiant du filtre
         * @return Array
         */
        getFilterValues : function(id)
        {
            var idx = this.getFilterIndexById(id);
            if (idx === false)
            {
                return false;
            }

            return (this._tabActivesFilters[id] ? this._tabActivesFilters[id] : []);
        },

        /**
         * Mettre à jour les données du tableau
         *
         * @param Object data Données
         * @return ajaxTable
         */
        updateData : function(data)
        {
            var mySelf = this;

            // Sauvegarde des données
            this._tabData = data;

            // Mise à jour du tableau
            var nbRows = data.rows.length;
            var nbCols = this._tabCfgs.columns.length;

            // On vide les lignes existantes
            $('tbody', this._tableEl).remove();
            var tBody = $('<tbody />');

            // Création des lignes du tableau
            for (var r = 0; r < nbRows; r++)
            {
                var tr = $('<tr />').addClass((r % 2 == 0 ? 'odd' : 'even'));
                for (var c = 0; c < nbCols; c++)
                {
                    var col = this._tabCfgs.columns[c];
                    var tdEl = document.createElement('td');
                    // Classe CSS
                    if (col.cssClasses)
                    {
                        tdEl.className = col.cssClasses;
                    }
                    // Contenu
                    tdEl.innerHTML = data.rows[r][col.id];

                    // Evénement de création d'une cellule
                    if (this.oninsertcell)
                    {
                        this.oninsertcell(this, r, c, tdEl, tr[0], data.rows[r]);
                    }

                    tr.append(tdEl);
                }
                tBody.append(tr);
            }

            // Si il n'y a pas de résultat
            if (data.totalRowsCount == 0)
            {
                $(this._containerEl).addClass('no-result');
                tBody.append($('<tr />').append($('<td />').attr('colspan', nbCols).html(this._tabCfgs.labels.noResult)));
            }
            else
            {
                $(this._containerEl).removeClass('no-result');
            }

            // Ajout du corps du tableau
            $(this._tableEl).append(tBody);


            // Mise à jour des paginations
            $(this._tabCfgs.paginations).each(function() {
                mySelf._displayPagination(this, data.totalRowsCount);
            });

            // Mise à jour des filtres et des recaps
            $(this._tabCfgs.filtersRecaps).empty();

            this._tabActivesFilters = {};

            $.each(this._tabCfgs.filters, function(i, filter) {

                if (typeof data.filters[filter.id] != 'undefined')
                {
                    // Récupération des éléments de la liste
                    var filterData = data.filters[filter.id];

                    // Sauvegarde des valeurs sélectionnés
                    if (filterData.values && filterData.values.length > 0)
                    {
                        mySelf._tabActivesFilters[filter.id] = [];
                        $.each(filterData.values, function(i, row) {
                            mySelf._tabActivesFilters[filter.id].push(row.value);
                        });
                    }
                    else if (filterData.search)
                    {
                        mySelf._tabActivesFilters[filter.id] = [filterData.search];
                    }


                    if (filter.container)
                    {
                        // Création des éléments de la liste
                        var tabLabels = [];

                        // Activation/Désactivation
                        switch (filter.type)
                        {
                            /* Type Liste */
                            case 'list':

                                var filterWrapper = $('> div.content > div.wrapper', filter.container);

                                // On affiche le message si pas d'élément disponible
                                $(filter.container).toggleClass('no-item', (filterData.list.length == 0));


                                // Création de la liste des valeurs
                                var itemsList = $('> div.list-container > ul', filterWrapper).empty();

                                var selectedItemsList = [];
                                var notSelectedItemsList = [];

                                $.each(filterData.list, function(i, row) {
                                    var li = $('<li />');
                                    var label = (row.label ? row.label : row.value);
                                    if (label == '')
                                    {
                                        // Elément "Vide"
                                        label = filter.labels.emptyValue;
                                        li.addClass('empty');
                                    }

                                    // Recherche si cette valeur est sélectionnée
                                    if (filterData.values && inArray(row.value, filterData.values, 'value') !== false)
                                    {
                                        li.addClass('selected');
                                        selectedItemsList.push(li.html(label).attr('data-val', row.value));
                                    }
                                    else
                                    {
                                        notSelectedItemsList.push(li.html(label).attr('data-val', row.value));
                                    }
                                });

                                // Création des éléments dans la liste
                                itemsList.append(selectedItemsList);    // Eléments sélectionnés
                                itemsList.append(notSelectedItemsList); // Eléments non sélectionnés

                                // Le filtre de recherche est-il actif ?
                                // - si oui, on n'affiche que les éléments qui correspondent à la recherche
                                if (filter.options.search)
                                {
                                    var searchContainer = $('> div.search', filterWrapper);

                                    // Affichage du champ de recherche interne
                                    if (filterData.list.length > 0)
                                    {
                                        searchContainer.show();
                                        mySelf._displayFltList(filterWrapper, true);
                                    }
                                    else
                                    {
                                        searchContainer.hide();
                                    }
                                }

                                // Libellé des valeurs du filtre si il est activé
                                if (filterData.values)
                                {
                                    $.each(filterData.values, function(i, row) {

                                        var label = (row.label ? row.label : row.value);
                                        if (label == '')
                                        {
                                            label = filter.labels.emptyValue;
                                        }
                                        tabLabels.push(label);
                                    });
                                }

                                // Sélection des éléments et affichage du résumé
                                var selectBtn = $('> div.content > span', filter.container);
                                if (tabLabels.length > 1)
                                {
                                    // Plusieurs éléments sont sélectionnés
                                    selectBtn.html(filter.labels.selections.replace('<%values>', tabLabels.join(', ')));
                                }
                                else if (tabLabels.length == 1)
                                {
                                    // Un seul élément est sélectionné
                                    selectBtn.html(filter.labels.selection.replace('<%value>', tabLabels[0]));
                                }
                                else
                                {
                                    // Tous les éléments sont sélectionnés
                                    selectBtn.html(filter.labels.noSelection);
                                }

                                // Affichage des trois petits points (On utilise un timer pour attendre le reflow navigateur)
                                selectBtn.parent().removeClass('more');
                                window.setTimeout(function () {
                                    selectBtn.parent().toggleClass('more', (selectBtn[0].offsetHeight < selectBtn[0].scrollHeight));
                                    mySelf._displayFltList(filterWrapper, false);
                                }, 200);

                                break;

                            /* Type Liste */
                            case 'search':

                                var filterWrapper = $('> div.content > div.wrapper', filter.container);

                                // Recherche
                                var selectBtn = $('> div.content > span', filter.container);
                                var searchInput = $('> div.search > input', filterWrapper);

                                if (filterData.search)
                                {
                                    searchInput.val(filterData.search);

                                    tabLabels.push(filterData.search);
                                    selectBtn.html(filter.labels.selection.replace('<%search>', filterData.search));
                                }
                                else
                                {
                                    searchInput.val('');

                                    // Tous les éléments sont sélectionnés
                                    selectBtn.html(filter.labels.noSelection);
                                }

                                break;

                            /* Type Radio */
                            case 'radio':

                                $('> ul > li > span', filter.container).each(function(i, el) {

                                    var value = $(el).attr('data-val');

                                    // Recherche si cette valeur est possible, sinon on la désactive
                                    if (inArray(value, filterData.list, 'value') !== false)
                                    {
                                        $(el).removeClass('disabled');

                                        // Recherche si cette valeur est sélectionnée
                                        if (filterData.values && inArray(value, filterData.values, 'value') !== false)
                                        {
                                            tabLabels.push($(el).html());
                                            $(el).addClass('selected');
                                        }
                                        else
                                        {
                                            $(el).removeClass('selected');
                                        }
                                    }
                                    else
                                    {
                                        $(el).addClass('disabled');
                                    }
                                });

                                break;
                        }

                        // Activation/désactivation du filtre
                        if (tabLabels.length > 0)
                        {
                            $(filter.container).addClass('active');

                            // Ajout des sélections dans les récapitulatifs
                            var text = tabLabels.join(', ');
                            var title = (filter.labels.title ? filter.labels.title + ': ' : '') + text;
                            var recap = $('<span />').html(text).attr('title', title);
                            $(mySelf._tabCfgs.filtersRecaps).append(recap);
                        }
                        else
                        {
                            $(filter.container).removeClass('active');
                        }
                    }
                }
            });

            // Evénement de mise à jour des données
            if (this.onupdatedata)
            {
                this.onupdatedata(data);
            }
        },

        /**
         * Création du tableau vide
         */
        init : function()
        {
            var mySelf = this;

            // **************************
            // Création du tableau
            // **************************
            this._tableEl = document.createElement('table');

            // Classes CSS
            if (this._tabCfgs.cssClasses)
            {
                this._tableEl.className = this._tabCfgs.cssClasses;
            }

            // Création des colonnes dans l'entête
            var tr = $('<tr />');

            var setSort = function(id) {
                return function(e) {
                    mySelf.sortBy(id, e.shiftKey);
                };
            };
            $(this._tabCfgs.columns).each(function(c, col) {
                var th = $('<th />');
                th.html($('<div />').html(col.title));
                // Infobulle
                if (col.tip)
                {
                    th.attr('title', col.tip);
                }
                // Class CSS
                if (col.cssClasses)
                {
                    th.addClass(col.cssClasses);
                }
                // Tri
                if (col.sort)
                {
                    th.click(setSort(col.id));
                    th.addClass('sort');
                    if (typeof col.sort != 'object')
                    {
                        col.sort = {};
                    }
                    if (col.sort.dir && (col.sort.dir == 'asc' || col.sort.dir == 'desc'))
                    {
                        mySelf._tabSorts.push({
                            col : col.id,
                            dir : col.sort.dir
                        });
                    }
                }
                tr.append(th);
            });

            $(this._tableEl).append($('<thead />').append(tr));

            // Ajout des éléments dans le conteneur
            $(this._containerEl).addClass('ajaxtable-container').
                                 append($('<div />').addClass('loading-mask')).
                                 append(this._tableEl);


            // *************************************************
            // Mise à jour de l'affichage des tris
            // *************************************************
            this._displaySorts();


            // *************************************************
            // Création des éléments pour les paginations
            // *************************************************
            $(this._tabCfgs.paginations).each(function() {
                $(this).addClass('ajaxtable-pagination-container').
                        append($('<div />').addClass('loading-mask')).
                        append($('<ul />')).
                        append($('<span />').addClass('results'));
            });


            // *************************************************
            // Création des filtres
            // *************************************************
            var clkEvt = ('ontouchend' in document.documentElement ? 'touchend' : 'click');

            $.each(this._tabCfgs.filters, function(i, filter) {

                if (filter.container)
                {
                    // Construction de la base du filtre (Conteneur avec le bouton pour tout sélectionner)
                    var selectAllBtn = $('<span />').addClass('btn').html(filter.labels.inactive);
                    $(filter.container).addClass('ajaxtable-filter-container type-' + filter.type).
                                        append($('<div />').addClass('loading-mask')).
                                        append(selectAllBtn);

                    // Construction du contenu du filtre suivant son type (Liste, Radio, etc.)
                    switch (filter.type)
                    {
                        /* Type Liste */
                        case 'list':

                            // Conteneur de la liste déroulante
                            var filterWrapper = $('<div />').addClass('wrapper').
                                                             append($('<div />').addClass('loading-mask')).
                                                             on(clkEvt, false).
                                                             hide();
                            if (filter.options.width)
                            {
                                filterWrapper.width(filter.options.width);
                            }

                            // Si on a un champ de recherche dans le filtre
                            var searchInput = null;
                            var clearSearchBtn = null;
                            if (filter.options.search)
                            {
                                searchInput = $('<input />').attr('type', 'text');
                                if (filter.labels.search)
                                {
                                    searchInput.attr('placeholder', filter.labels.search);
                                }

                                // Bouton du suppression de la recherche
                                clearSearchBtn = $('<div />').addClass('btn-clear').hide();
                                if (filter.labels.clearSearch)
                                {
                                    clearSearchBtn.attr('title', filter.labels.clearSearch);
                                }

                                // Champ de recherche
                                searchInput.keyup(function (e) {
                                    mySelf._displayFltList(filterWrapper, true);
                                });

                                clearSearchBtn.click(function() {
                                    searchInput.val('').focus();
                                    mySelf._displayFltList(filterWrapper, true);
                                });

                                filterWrapper.append($('<div />').addClass('search').
                                                                  append(clearSearchBtn).
                                                                  append(searchInput).
                                                                  append($('<span />').html(filter.labels.noSearchRslt)));
                            }

                            // Liste d'éléments
                            var itemsList = $('<ul />');

                            filterWrapper.append($('<div />').addClass('list-container').append(itemsList));

                            // Bouton pour vider la sélection
                            var clearSelBtn = null;
                            if (!filter.options.isSingle)
                            {
                                clearSelBtn = $('<a />').html(mySelf._tabCfgs.labels.clearSel);
                                filterWrapper.append($('<div />').addClass('clear-sel').append(clearSelBtn));
                            }

                            // Ajout du message "Aucun élément disponible"
                            filterWrapper.append($('<span />').addClass('no-item-msg').
                                                               html(filter.labels.noItemAvlb));

                            // Bouton pour faire apparaître la liste déroulante
                            var selectBtn = $('<span />').addClass('btn').
                                                          html(filter.labels.noSelection);

                            selectBtn.on(clkEvt, function(e) {
                                // On s'assure que les éléments sont bien sélectionnés
                                var tabValues = mySelf.getFilterValues(filter.id);

                                $('> li', itemsList).each(function(i, liEl) {
                                    var value = $(this).attr('data-val');
                                    $(this).toggleClass('selected', inArray(value, tabValues) !== false);
                                });
                                if (!filter.options.isSingle)
                                {
                                    filterWrapper.toggleClass('selected-items', $('> li.selected', itemsList).size() > 0);
                                }
                                filterWrapper.slideDown(100, function() {
                                    if (searchInput)
                                    {
                                        searchInput.val('').focus();
                                    }
                                    mySelf._displayFltList(filterWrapper, false);
                                });
                            });


                            // Fonction de mise à jour du tableau après un délai choisi
                            var updateFilterTimer = null;
                            var updateFilterFunc = function(delay) {

                                if (updateFilterTimer)
                                {
                                    window.clearTimeout(updateFilterTimer);
                                    updateFilterTimer = null;
                                }
                                if (delay)
                                {
                                    updateFilterTimer = window.setTimeout(function() {
                                        updateFilterFunc(false);
                                    }, (filter.options.delay ? filter.options.delay : 1000));
                                    return;
                                }

                                // Si la sélection a été modifiée
                                if ($('> li.updated', itemsList).size() > 0)
                                {
                                    // Récupération de la liste des valeurs sélectionnées
                                    var tabSelected = $('> li.selected', itemsList);

                                    var tabValues = [];
                                    if (tabSelected.size() != itemsList.children().size())
                                    {
                                        $('> li.selected', itemsList).each(function(i, liEl) {
                                            tabValues.push($(liEl).attr('data-val'));
                                        });
                                    }

                                    mySelf.setFilterValues(filter.id, tabValues);
                                }
                            };

                            // Evenement au click d'un élément
                            var touchMoving = false;
                            if (clkEvt == 'touchend')
                            {
                                // Prise en compte du scroll sur l'ipad
                                itemsList.on('touchmove', function(e) {
                                    touchMoving = true;
                                });
                            }
                            itemsList.on(clkEvt, function(e) {
                                if (touchMoving)
                                {
                                    touchMoving = false;
                                    return false;
                                }
                                if (e.target.nodeName.toLowerCase() == 'li')
                                {
                                    var li = $(e.target);
                                    // Pour les listes à sélection unique, on décoche l'ancienne valeur
                                    if (filter.options.isSingle)
                                    {
                                        var isSelected = li.hasClass('selected');
                                        $('> li.selected', itemsList).each(function(i, liEl) {
                                            $(liEl).removeClass('selected').toggleClass('updated');
                                        });
                                        if (!isSelected)
                                        {
                                            li.toggleClass('selected').toggleClass('updated');
                                        }
                                        updateFilterFunc(false);
                                    }
                                    else
                                    {
                                        li.toggleClass('selected').toggleClass('updated');
                                        filterWrapper.toggleClass('selected-items', $('> li.selected', itemsList).size() > 0);
                                        updateFilterFunc(true);
                                    }
                                }
                            });
                            // Quand l'utilisateur scroll dans la liste, la prochaine réactualisation programmée est décalée
                            itemsList.on('scroll', function(e) {
                                mySelf._displayFltList(filterWrapper, false);
                                updateFilterFunc(true);
                            });
                            // Quand l'utilisateur bouge la souris dans la liste, la prochaine réactualisation programmée est décalée
                            itemsList.on('mousemove', function(e) {
                                updateFilterFunc(true);
                            });
                            // Bouton pour vider la sélection
                            if (clearSelBtn)
                            {
                                clearSelBtn.on(clkEvt, function(e) {
                                    $('> li.selected', itemsList).removeClass('selected').toggleClass('updated');
                                    filterWrapper.removeClass('selected-items');
                                    updateFilterFunc(true);
                                });
                            }

                            // Fermeture automatique de la liste déroulante en cliquant à l'extérieur
                            $(document).on(clkEvt, function(e) {
                                if (!$.contains(selectBtn[0].parentNode, e.target) && filterWrapper.css('display') != 'none')
                                {
                                    filterWrapper.fadeOut(300);
                                    itemsList.animate({scrollTop: '0'}, 300);
                                    if (searchInput)
                                    {
                                        searchInput.val('');
                                        mySelf._displayFltList(filterWrapper, true);
                                    }
                                    updateFilterFunc(false);
                                }
                            });
                            // Evenement sur le bouton pour tout sélectionner
                            selectAllBtn.on(clkEvt, function(e) {
                                if (mySelf.setFilterValues(filter.id, null) && filterWrapper.css('display') != 'none')
                                {
                                    if (updateFilterTimer)
                                    {
                                        window.clearTimeout(updateFilterTimer);
                                        updateFilterTimer = null;
                                    }
                                    filterWrapper.hide();
                                }
                            });


                            // Ajout des éléments au filtre
                            $(filter.container).append($('<div />').addClass('content').
                                                                    append(filterWrapper).
                                                                    append(selectBtn));

                            break;

                        /* Type recherche direct */
                        case 'search':

                            // Conteneur de la liste déroulante
                            var filterWrapper = $('<div />').addClass('wrapper').
                                                             append($('<div />').addClass('loading-mask')).
                                                             on(clkEvt, false).
                                                             hide();

                            // Champ de recherche
                            var searchInput = $('<input />').attr('type', 'text').
                                                             keydown(function (e) {
                                                                 var value = $.trim(this.value);
                                                                 clearSearchBtn.toggle(value != '');
                                                                 if (e.keyCode == 13 && mySelf.setFilterValues(filter.id, (value != '' ? value : null)))
                                                                 {
                                                                     filterWrapper.hide();
                                                                 }
                                                             });
                            if (filter.labels.search)
                            {
                                searchInput.attr('placeholder', filter.labels.search);
                            }

                            // Bouton du suppression de la recherche
                            var clearSearchBtn = $('<div />').addClass('btn-clear').hide();
                            if (filter.labels.clearSearch)
                            {
                                clearSearchBtn.attr('title', filter.labels.clearSearch);
                            }

                            clearSearchBtn.click(function() {
                                searchInput.val('').focus();
                                $(this).hide();
                            });

                            filterWrapper.append($('<div />').addClass('search').
                                                              append(clearSearchBtn).
                                                              append(searchInput));

                            // Bouton pour faire apparaître la liste déroulante
                            var selectBtn = $('<span />').addClass('btn').
                                                          html(filter.labels.noSelection).
                                                          on(clkEvt, function(e) {
                                                              // Recherche courante
                                                              var tabValues = mySelf.getFilterValues(filter.id);
                                                              searchInput.val((tabValues.length == 1 ? tabValues[0] : ''));

                                                              clearSearchBtn.toggle(searchInput.val() != '');
                                                              filterWrapper.slideDown(100, function() {
                                                                  // et on lui met le focus
                                                                  searchInput.focus();
                                                              });
                                                          });

                            // Fermeture automatique de la recherche cliquant à l'extérieur
                            $(document).on(clkEvt, function(e) {
                                if (!$.contains(selectBtn[0].parentNode, e.target) && filterWrapper.css('display') != 'none')
                                {
                                    filterWrapper.fadeOut(300);
                                    // Mise à jour du filtre
                                    var value = $.trim(searchInput.val());
                                    mySelf.setFilterValues(filter.id, (value != '' ? value : null));
                                }
                            });
                            // Evenement sur le bouton pour tout sélectionner
                            selectAllBtn.on(clkEvt, function(e) {
                                searchInput.val('');
                                if (mySelf.setFilterValues(filter.id, null) && filterWrapper.css('display') != 'none')
                                {
                                    filterWrapper.hide();
                                }
                            });

                            // Ajout des éléments au filtre
                            $(filter.container).append($('<div />').addClass('content').
                                                                    append(filterWrapper).
                                                                    append(selectBtn));

                            break;

                        /* Type Radio */
                        case 'radio':
                            // Liste d'éléments
                            var itemsList = $('<ul />');

                            // Création de la liste d'éléments
                            $.each(filter.labels.items, function() {
                                itemsList.append($('<li />').
                                          append($('<span />').addClass('btn').
                                                               attr('data-val', this.value).
                                                               html(this.text).
                                                               on(clkEvt, function(e) {
                                                                   if (!$(this).hasClass('disabled'))
                                                                   {
                                                                       mySelf.setFilterValues(filter.id, [$(this).attr('data-val')]);
                                                                   }
                                                               })));
                            });
                            // Evenement sur le bouton pour tout sélectionner
                            selectAllBtn.on(clkEvt, function(e) {
                                mySelf.setFilterValues(filter.id, null);
                            });

                            // Ajout des éléments au filtre
                            $(filter.container).append(itemsList);

                            break;
                    }
                }
            });

            // Affichage des trois petits points (recalcul si redimensionnement)
            $(window).resize(function () {
                $('.ajaxtable-filter-container.type-list > div.content > span').each(function () {
                    $(this).parent().toggleClass('more', (this.offsetHeight < this.scrollHeight));
                });
            });


            // ************************************************************
            // Création des éléments pour récapitulatifs les paginations
            // ************************************************************
            $(this._tabCfgs.filtersRecaps).each(function() {
                $(this).addClass('ajaxtable-filters-recap-container');
            });



            // *************************************************
            // Mise à jour du tableau
            // *************************************************
            if (this._tabCfgs.data)
            {
                this.updateData(this._tabCfgs.data);
            }
            else
            {
                this.update();
            }
        },

        /**
         * Mettre à jour l'affichage d'une pagination
         *
         * @param HTMLElement containerEl Conteneur de la pagination
         * @param int         nbTotalRows Nombre total de lignes
         */
        _displayPagination : function(containerEl, nbTotalRows)
        {
            // Calcul du nombre de pages et de la page courante
            var nbPages = Math.ceil(nbTotalRows / this._pageSize);
            var curPage = Math.floor(this._startIndex / this._pageSize);

            var ul = $('> ul', containerEl).empty();

            /* Fonction "Closure" pour le changement de page */
            var setPage = function(table, i) {
                return function() { table.setCurrentPage(i); };
            };

            // Page précédente
            var li = $('<li />').addClass('btn').html('&lt;&nbsp;Préc');
            if (curPage > 0)
            {
                li.addClass('active').click(setPage(this, curPage - 1));
            }
            else
            {
                li.addClass('disabled');
            }
            ul.append(li);

            // Pages
            if (nbPages > 0)
            {
                for (var i = 0; i < nbPages; i++)
                {
                    if (i == 0 || i == nbPages - 1 || (i >= curPage - 2 && i <= curPage + 2))
                    {
                        li = $('<li />').addClass('btn').html(i + 1);
                        if (i != curPage)
                        {
                            li.addClass('active').click(setPage(this, i));
                        }
                        else
                        {
                            li.addClass('current');
                        }
                        ul.append(li);
                    }
                    else if (!li.hasClass('sep'))
                    {
                        li = $('<li />').addClass('sep').html('...');
                        ul.append(li);
                    }
                }
            }
            else
            {
                ul.append($('<li />').addClass('btn current').html(1));
            }

            // Page suivante
            li = $('<li />').addClass('btn').html('Suiv.&nbsp;&gt;');
            if (curPage < nbPages - 1)
            {
                li.addClass('active').click(setPage(this, curPage + 1));
            }
            else
            {
                li.addClass('disabled');
            }

            ul.append(li);

            // Nombre de résultats
            var text = '';
            if (nbTotalRows > 1)
            {
                var start = this._startIndex + 1;
                var end   = this._startIndex + this._pageSize;
                text = this._tabCfgs.labels.pagination.results.replace('<%start>', start).
                                                               replace('<%end>', (end > nbTotalRows ? nbTotalRows : end)).
                                                               replace('<%total>', nbTotalRows);
            }
            else if (nbTotalRows == 1)
            {
                text = this._tabCfgs.labels.pagination.result;
            }
            else
            {
                text = this._tabCfgs.labels.pagination.noResult;
            }
            $('> span.results', containerEl).html(text);
        },

        /**
         * Mettre à jour l'affichage des tris
         *
         */
        _displaySorts : function()
        {
            var mySelf = this;
            var tr = $('thead > tr', this._tableEl);

            // Suppression des tris existants
            $('th', tr).removeClass('sort-asc sort-desc');

            // Ajout des tris
            $.each(this._tabSorts, function(i, sort) {
                $('th:eq(' + mySelf.getColumnIndexById(sort.col) + ')', tr).addClass('sort-' + sort.dir);
            });
        },

        /**
         * Mettre à jour l'affichage (style) d'un filtre liste
         */
        _displayFltList: function (filterWrapper, isSearchUpdated)
        {
            var itemsList = $('> div.list-container > ul', filterWrapper);

            if (isSearchUpdated)
            {
                var searchContainer = $('> div.search', filterWrapper);
                var searchInput = $('> input', searchContainer);
                var clearSearchBtn = $('> div.btn-clear', searchContainer);

                var searchValue = $.trim(searchInput.val()).toLowerCase();
                clearSearchBtn.toggle(searchValue != '');

                /* recherche dans la liste */
                var nbVisibles = 0;
                $('> li', itemsList).each(function() {
                    var isVisible = (searchValue == '' || $(this).text().toLowerCase().indexOf(searchValue) != -1);
                    $(this).toggle(isVisible);
                    if (isVisible)
                    {
                        nbVisibles++;
                    }
                });
                searchContainer.toggleClass('no-result', !nbVisibles);
            }

            itemsList.parent().toggleClass('scroll-top', itemsList.scrollTop() > 0);
            itemsList.parent().toggleClass('scroll-btm', itemsList.scrollTop() < (itemsList.prop('scrollHeight') - itemsList.prop('clientHeight')));
        },

        /**
         * Tri du tableau
         *
         *
         */
        sortBy : function(id, isAdded)
        {
            if (this._isLoading)
            {
                return false;
            }
            var idx = this.getColumnIndexById(id);
            var col = this._tabCfgs.columns[idx];

            var pos = inArray(col.id, this._tabSorts, 'col');
            var foundedSort = (pos !== false ? this._tabSorts[pos] : null);

            var dir = (foundedSort && foundedSort.dir == 'asc' ? 'desc' : 'asc');

            if (!isAdded)
            {
                this._tabSorts = [{
                    col : col.id,
                    dir : dir
                }];
            }
            else if (foundedSort)
            {
                foundedSort.dir = dir;
            }
            else
            {
                this._tabSorts.push({
                    col : col.id,
                    dir : dir
                });
            }

            // Mise à jour de l'affichage des tris
            this._displaySorts();

            // Mise à jour du tableau
            this.update();
        },

        update : function()
        {
            var mySelf = this;

            // Evénement avant la mise à jour des données
            if (this.onbeforeupdatedata)
            {
                this.onbeforeupdatedata();
            }

            // Activation des animations de chargement
            this._isLoading = true;

            $(this._containerEl).addClass('loading');
            $(this._tabCfgs.paginations).each(function() {
                $(this).addClass('loading');
            });
            $.each(this._tabCfgs.filters, function(i, filter) {
                $(filter.container).addClass('loading');
            });

            // Requête HTTP Asynchrone
            $.ajax({
                url : this._tabCfgs.url,
                method : 'post',
                dataType : 'json',
                data : JSON.stringify({
                    startIndex : this._startIndex,
                    pageSize : this._pageSize,
                    sorts : this._tabSorts,
                    filters : this._tabActivesFilters
                }),
                contentType: 'application/json',
            }).done(function(data) {
                // Désactivation des animations de chargement
                $(mySelf._containerEl).removeClass('loading');
                $(mySelf._tabCfgs.paginations).each(function() {
                    $(this).removeClass('loading');
                });
                $.each(mySelf._tabCfgs.filters, function(i, filter) {
                    $(filter.container).removeClass('loading');
                });

                mySelf._isLoading = false;

                // Mise à jour du contenu du tableau
                mySelf.updateData(data);
            });
        }
    };


    $.fn.ajaxTable = function(tabParams)
    {
        return new ajaxTable($(this), tabParams);
    };

})(jQuery);