function addValue(id)
{
    $('tabAdd[add]').value = 1;
    submitForm();
}

function removeValue(id)
{
    $('tabData[' + id + '][remove]').value = 1;
    submitForm();
}

function submitForm()
{
    var form = document.getElementsByTagName('form')[0];
    
    $('valid').value = 'ok';
    form.submit();
}

function refresh()
{
    var form = document.getElementsByTagName('form')[0];

    form.submit();
}