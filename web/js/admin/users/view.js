
var userManager = {

    _currentUserObj : null,
    _tabUsers : [],
    tabConfigs : {},
    _tabClipboard : null,
    _loadHttpObj : null,
    _saveHttpObj : null,
    _state : 'ok',

    /**
     * Ajouter un utilisateur
     *
     * @param int id Id de l'utilisateur a ajouter
     * @return userManager.User | false
     */
    addUser : function (id)
    {
        id = Number(id);
        if (id <= 0)
        {
            return false;
        }

        // Si l'utilisateur existe déjà on arrête
        var nbUsers = this._tabUsers.length;
        for (var i = 0; i < nbUsers; i++)
        {
            if (this._tabUsers[i].getId() == id)
            {
                this._currentUserObj = this._tabUsers[i];
                this.display();
                return false;
            }
        }
        // Impossible de créer plus de 5 onglets
        if (this._tabUsers.length >= 5)
        {
            alert('Vous avez atteint le nombre maximal d\'onglet.');
            return false;
        }

        var userObj = new this.User(id);
        this._currentUserObj = userObj;
        this._tabUsers.push(userObj);

        var divEl = document.querySelector('#userTabBox > div.tabs');

        var mySelf = this;

        // Création de l'onglet
        var tabEl = document.createElement('DIV');
        tabEl.className = 'active loading';
        tabEl.id = 'tabUser_' + id;
        tabEl.setAttribute('_userId', id);
        tabEl.onclick = function()
        {
            var userId = tabEl.getAttribute('_userId');
            var i = mySelf._getUserIndex(userId);
            if (i != -1)
            {
                mySelf._currentUserObj = mySelf._tabUsers[i];
                mySelf.display();
            }
        }

        var titleEl = document.createElement('SPAN');
        titleEl.innerHTML = id + ':';
        tabEl.appendChild(titleEl);

        var closeBtnEl = document.createElement('A');
        closeBtnEl.className = 'close loading';
        closeBtnEl.onclick = function()
        {
            var tabEl = this.parentNode;
            var userId = tabEl.getAttribute('_userId');

            var i = mySelf._getUserIndex(userId);

            if (i != -1)
            {
                if (!mySelf._tabUsers[i].hasModifications() || confirm('Les modifications de cet utilisateur n\'ont pas été enregistrées, voulez-vous vraiment quitter ?'))
                {
                    mySelf._tabUsers.splice(i, 1);
                    if (mySelf._tabUsers.length > 0)
                    {
                        if (mySelf._currentUserObj.getId() == userId)
                        {
                            mySelf._currentUserObj = mySelf._tabUsers[(i == 0 ? 0 : i - 1)];
                        }
                    }
                    else
                    {
                        mySelf._currentUserObj = null;
                    }
                    tabEl.parentNode.removeChild(tabEl);
                }
            }

            mySelf.display();
        }
        tabEl.appendChild(closeBtnEl);

        divEl.appendChild(tabEl);

        this._loadUser(userObj);

        return userObj;
    },

    _getUserIndex : function(id)
    {
        var i = 0;
        var nb = this._tabUsers.length;
        while (i < nb && this._tabUsers[i].getId() != id)
        {
            i++;
        }
        return (i < nb ? i : -1);
    },

    /**
     * Charge un utilisateur
     *
     * @param int userObj Instance de l'utilisateur a charger
     */
    _loadUser : function(userObj)
    {
        this._state = 'loading';
        this.display();

        if (!this._loadHttpObj)
        {
            this._loadHttpObj = new XMLHttpRequest();
        }

        var mySelf = this;
        this._loadHttpObj.onreadystatechange = function()
        {
            if (mySelf._loadHttpObj.readyState == 4)
            {
                var trameJson = mySelf._loadHttpObj.responseText;
                var tabInfos = eval('(' + trameJson + ')');

                userObj.loadByArray(tabInfos);
                mySelf._state = 'ok';
                mySelf.display();
            }
        }

        this._loadHttpObj.open('POST', userManager.tabConfigs['loadUrl'], true);
        this._loadHttpObj.setRequestHeader('Content-Type', 'application/json');
        this._loadHttpObj.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        this._loadHttpObj.send(JSON.stringify({'id':userObj.getId()}));
    },


    /**
     * Récupérer tous les utilisateurs
     *
     * @return Array
     */
    getUsers : function()
    {
        return this._tabUsers;
    },

    /**
     * Récupérer l'utilisateur courant
     *
     * @return userManager.User
     */
    getCurrentUser : function()
    {
        return this._currentUserObj;
    },

    /**
     * Définir l'agence de l'utilisateur
     *
     * @param string id
     * @return bool
     */
    setAgencyId : function(id)
    {
        if (this._currentUserObj && this._currentUserObj.setAgencyId(id))
        {
            this.display();
            return true;
        }
        return false;
    },

    /**
     * Définir si l'utilisateur est un Admin
     *
     * @param int level
     * @return bool
     */
    setLevel : function(level)
    {
        if (this._currentUserObj && this._currentUserObj.setLevel(level))
        {
            this.display();
            return true;
        }
        return false;
    },

    /**
     * Définir si l'utilisateur à le Nomade
     *
     * @return bool
     */
    toggleNomad : function()
    {
        if (this._currentUserObj)
        {
            var isNomad = !this._currentUserObj.isNomad();
            if (this._currentUserObj.setNomad(isNomad))
            {
                this.display();
                return true;
            }
        }
        return false;
    },

    /**
     * Sélectionne ou déselectionne un groupe
     *
     * @param int groupId Identifiant du groupe
     * @return bool
     */
    switchGroup : function(groupId)
    {
        if (this._currentUserObj && this._currentUserObj.switchGroup(groupId))
        {
            this.display();
            return true;
        }
        return false;
    },

    /**
     * Sélectionne ou déselectionne une agence nomade
     *
     * @param int agencyId Identifiant de l'agence
     * @return bool
     */
    switchNomadAgency : function(id)
    {
        if (this._currentUserObj && this._currentUserObj.switchNomadAgency(id))
        {
            this.display();
            return true;
        }
        return false;
    },

    /**
     * Sélectionne ou déselectionne les agences nomade d'un pays ou d'un région
     *
     * @param Aray tabNomadAgencies Tableau des agences nomades
     * @return bool
     */
    switchNomadAgencies : function(tabNomadAgencies)
    {
        if (this._currentUserObj && this._currentUserObj.switchNomadAgencies(tabNomadAgencies))
        {
            this.display();
            return true;
        }
        return false;
    },

    /**
     * Définir le profil de l'utilisateur
     *
     * @param bool isNomad
     * @return bool
     */
    setProfileId : function(profileId)
    {
        if (this._currentUserObj && this._currentUserObj.setProfileId(profileId))
        {
            this.display();
            return true;
        }
        return false;
    },

    /**
     * Copie les informations de l'utilisateur courant
     *
     * @return bool
     */
    copy : function()
    {
        if (!this.isCopyPossible())
        {
            return false;
        }

        this._tabClipboard = this._currentUserObj.copy();
        this.display();

        return true;
    },

    /**
     * Colle les informations précédemment copiées sur l'utilisateur courant
     *
     * @return bool
     */
    paste : function()
    {
        if (this.isPastePossible() && this._currentUserObj.paste(this._tabClipboard))
        {
            this.display();
            return true;
        }
        return false;
    },

    /**
     * Enregistre les modifications apportées à l'utilisateur courant
     *
     * @return bool
     */
    save : function()
    {
        if (!this.isSavePossible())
        {
            return false;
        }

        var userObj = this._currentUserObj;
        this._state = 'loading';
        this.display();

        if (!this._saveHttpObj)
        {
            this._saveHttpObj = new XMLHttpRequest();
        }

        var mySelf = this;

        this._saveHttpObj.onreadystatechange = function()
        {
            if (mySelf._saveHttpObj.readyState == 4)
            {
                var trameJson = mySelf._saveHttpObj.responseText;
                var tabInfos = eval('(' + trameJson + ')');

                userObj.loadByArray(tabInfos);
                mySelf._state = 'ok';
                mySelf.display();
            }
        }

        this._saveHttpObj.open('POST', this.tabConfigs['saveUrl'], true);
        this._saveHttpObj.setRequestHeader('Content-Type', 'application/json');
        this._saveHttpObj.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        this._saveHttpObj.send(JSON.stringify(userObj.getData()));
        return true;
    },

    /**
     * Réinitialise les informations de l'utilisateur courant
     *
     * @return userManager.User | false
     */
    reset : function()
    {
        if (!this.isResetPossible())
        {
            return false;
        }

        return this._loadUser(this._currentUserObj);
    },

    /**
     * Teste si la copie est possible
     *
     * @return bool
     */
    isCopyPossible : function()
    {
        return (this._state == 'ok' && this._currentUserObj ? true : false);
    },

    /**
     * Teste si le collage est possible
     *
     * @return bool
     */
    isPastePossible : function()
    {
        return (this._state == 'ok' && this._tabClipboard ? true : false);
    },

    /**
     * Teste si la sauvegarde est possible
     *
     * @return bool
     */
    isSavePossible : function()
    {
        return (this._state == 'ok' && this._currentUserObj ? true : false);
    },

    /**
     * Teste si la réinitialisation est possible
     *
     * @return bool
     */
    isResetPossible : function()
    {
        return (this._state == 'ok' && this._currentUserObj ? true : false);
    },

    /**
     * Initialise la page avec les configurations passées en parametres
     *
     * @param Array tabConfigs Tableau des configurations de la page
     */
    init : function(tabConfigs)
    {
        this.tabConfigs = tabConfigs;

        LOC_Common.cancelTextSelection('userIU');

        this.display();
    },

    /**
     * Permet l'affichage de la page
     *
     */
    display : function()
    {
        window.document.getElementById('user_form').className = this._state;
        window.document.getElementById('userIU').className = (this._currentUserObj ? 'ok' : 'none');

        // Mise à jour de l'affichage des onglets
        var nbUsers = this._tabUsers.length;
        for (var i = 0; i < nbUsers; i++)
        {
            var tabClasses = [];
            if (this._currentUserObj && this._tabUsers[i].getId() == this._currentUserObj.getId())
            {
                tabClasses.push('active');
                if (this._state == 'loading')
                {
                    tabClasses.push('loading');
                }
            }
            if (this._tabUsers[i].hasModifications())
            {
                tabClasses.push('modified');
            }

            var tabEl = document.getElementById('tabUser_' + this._tabUsers[i].getId());
            tabEl.firstChild.innerHTML = this._tabUsers[i].getId() + ': ' + this._tabUsers[i].getFullName();
            tabEl.className = tabClasses.join(' ');
        }

        if (this._state == 'ok')
        {
            var userObj = this._currentUserObj;

            window.document.getElementById('user_profile').disabled = (userObj ? false : true);
            window.document.getElementById('user_profile').value = (userObj ? userObj.getProfileId() : 0);
            window.document.getElementById('user_login').innerHTML = (userObj ? userObj.getLogin() : '');


            window.document.getElementById('user_nomad').className = (userObj && userObj.isNomad() ? 'checkbox yes' : 'checkbox');
            window.document.getElementById('user_level').value = (userObj ? userObj.getLevel() : 0);

            window.document.querySelector('#user_table > tbody > tr.nomadAgencies').style.display = (userObj && userObj.isNomad() ? '' : 'none');

            var tabSpansAgencies = window.document.querySelectorAll('table.nomadAgencies > tbody > tr.country > td.areas > table > tbody > tr.area > td.agencies > span.agency');
            var tabNomadAgencies = (userObj ? userObj.getNomadAgencies() : []);
            var agencyId = (userObj ? userObj.getAgencyId() : '');
            for (var i = 0; i < tabSpansAgencies.length; i++)
            {
                tabSpansAgencies[i].className = 'agency ' + (in_array(tabSpansAgencies[i].innerHTML, tabNomadAgencies) ? 'on' : 'off');
                if (tabSpansAgencies[i].innerHTML == agencyId)
                {
                    tabSpansAgencies[i].className += ' current';
                }
            }
            window.document.getElementById('user_agency').value = agencyId;

            var tabSpansGroups = window.document.querySelectorAll('div.groups > span.group');
            var tabGroups      = (userObj ? userObj.getGroups() : []);
            for (var i = 0; i < tabSpansGroups.length; i++)
            {
                tabSpansGroups[i].className = 'group ' + (in_array(tabSpansGroups[i].getAttribute('value'), tabGroups) ? 'on' : 'off');
            }

            Location.searchBoxesManager.getSearchBox('search_user_id').enable();
            document.getElementById('searchBar').className = 'ok';
        }
        else
        {
            Location.searchBoxesManager.getSearchBox('search_user_id').disable();
            document.getElementById('searchBar').className = 'loading';
        }

        window.document.getElementById('user_savebtn').className  = 'locCtrlButton' + (this.isSavePossible() ? '' : ' disabled');
        window.document.getElementById('user_resetbtn').className = 'locCtrlButton' + (this.isResetPossible() ? '' : ' disabled');
        window.document.getElementById('user_copybtn').className  = 'locCtrlButton' + (this.isCopyPossible() ? '' : ' disabled');
        window.document.getElementById('user_pastebtn').className = 'locCtrlButton' + (this.isPastePossible() ? '' : ' disabled');
        window.document.getElementById('user_addmebtn').className = 'locCtrlButton' + (this._state == 'ok' ? '' : ' disabled');
    }

};

/**
 * @constructor
 */
userManager.User = function(id)
{
    this._id = Number(id);

    this._fullName = '';
    this._login = '';
    this._isNomad = false;
    this._level = 0;
    this._tabGroups = [];
    this._tabNomadAgencies = [];
    this._profileId = 0;
    this._agencyId = '';
    this._tabOldValues = null;
}

userManager.User.prototype = {

    /**
     * Indique si l'utilisateur à le Nomade
     *
     * @return bool
     */
    isNomad : function()
    {
        return (this.getProfileId() != 0 && this._isNomad);
    },

    /**
     * Indique si l'utilisateur est Admin
     *
     * @return int
     */
    getLevel : function()
    {
        return (this.getProfileId() != 0 ? this._level : 0);
    },

    /**
     * Indique l'indentifiant de l'utilisateur courant
     *
     * @return int
     */
    getId : function()
    {
        return this._id;
    },

    /**
     * Récupérer l'agence courante
     *
     * @return int
     */
    getAgencyId : function()
    {
        return this._agencyId;
    },

    /**
     * Indique le nom complet de l'utilisateur courant
     *
     * @return string
     */
    getFullName : function()
    {
        return this._fullName;
    },

    /**
     * Indique les groupes de l'utilisateur courant
     *
     * @return Array
     */
    getGroups : function()
    {
        return (this.getProfileId() != 0 ? this._tabGroups : []);
    },

    /**
     * Indique les agences nomades de l'utilisateur courant
     *
     * @return int
     */
    getNomadAgencies : function()
    {
        return (this.isNomad() ? this._tabNomadAgencies : []);
    },

    /**
     * Indique le login de l'utilisateur courant
     *
     * @return string
     */
    getLogin : function()
    {
        return this._login;
    },

    /**
     * Indique le profil de l'utilisateur courant
     *
     * @return int
     */
    getProfileId : function()
    {
        return this._profileId;
    },

    /**
     * Copie les informations de l'utilisateur courant
     *
     * @return hash Tableau contenant les informations copiées
     */
    copy : function()
    {
        return {
            'profileId'        : LOC_Common.clone(this.getProfileId()),
            'agencyId'         : LOC_Common.clone(this.getAgencyId()),
            'level'            : LOC_Common.clone(this.getLevel()),
            'isNomad'          : LOC_Common.clone(this.isNomad()),
            'tabGroups'        : LOC_Common.clone(this.getGroups()),
            'tabNomadAgencies' : LOC_Common.clone(this.getNomadAgencies())
        };
    },

    /**
     * Colle les informations précédemment copiées sur l'utilisateur courant
     *
     * @return true
     */
    paste : function(tabData)
    {
        tabData = LOC_Common.clone(tabData);

        this._profileId        = tabData['profileId'];
        this._agencyId         = tabData['agencyId'];
        this._level            = tabData['level'];
        this._isNomad          = tabData['isNomad'];
        this._tabGroups        = tabData['tabGroups'];
        this._tabNomadAgencies = tabData['tabNomadAgencies'];

        return true;
    },

    /**
     * Définir le nomade de l'utilisateur courant
     *
     * @param bool isNomad
     * @return bool
     */
    setNomad : function(isNomad)
    {
        isNomad = (isNomad ? true : false);
        if (isNomad == this._isNomad)
        {
            return false;
        }
        this._isNomad = isNomad;
        return true;
    },

    /**
     * Définir le profil de l'utilisateur courant
     *
     * @param int profileId Profil de l'utilisateur
     * @return bool
     */
    setProfileId : function(profileId)
    {
        profileId = Number(profileId);
        if (profileId == this._profileId)
        {
            return false;
        }
        this._profileId = profileId;
        return true;
    },

    /**
     * Définir l'agence de l'utilisateur
     *
     * @param string id
     * @return bool
     */
    setAgencyId : function(id)
    {
        if (id == this._agencyId)
        {
            return false;
        }
        this._agencyId = id;
        return true;
    },

    /**
     * Définir si l'utilisateur courant est un admin
     *
     * @param int level
     * @return bool
     */
    setLevel : function(level)
    {
        level = Number(level);
        if (level == this._level)
        {
            return false;
        }
        this._level = level;
        return true;
    },

    /**
     * Ajoute une agence nomade à l'utilisateur courant
     *
     * @param int id Identifiant de l'agence nomade à ajouter
     * @return bool
     */
    addNomadAgency : function(id)
    {
        if (!in_array(id, this._tabNomadAgencies))
        {
            if (this._agencyId == '')
            {
                this._agencyId = id;
            }
            this._tabNomadAgencies.push(id);
            return true;
        }
        return false;
    },

    /**
     * Supprime une agence nomade à l'utilisateur courant
     *
     * @param int id Identifiant de l'agence nomade à supprimer
     * @return bool
     */
    removeNomadAgency : function(id)
    {
        if (in_array(id, this._tabNomadAgencies))
        {
            var tabTmp = [];
            for (var i = 0; i < this._tabNomadAgencies.length; i++)
            {
                if (this._tabNomadAgencies[i] != id)
                {
                    tabTmp.push(this._tabNomadAgencies[i]);
                }
            }
            this._tabNomadAgencies = tabTmp;

            return true;
        }
        return false;
    },

    /**
     * Sélectionne ou déselectionne une agence nomade à l'utilisateur courant
     *
     * @param int id Identifiant de l'agence nomade à modifier
     * @return int
     */
    switchNomadAgency : function(id)
    {
        if (this.addNomadAgency(id))
        {
            return 1;
        }
        else if (this.removeNomadAgency(id))
        {
            return -1;
        }
        return 0;
    },

    /**
     * Ajoute des agences nomade par pays ou régions à l'utilisateur courant
     *
     * @param Array tabNomadAgencies Tableau des agences nomade à ajouter
     * @return int
     */
    addNomadAgencies : function(tabNomadAgencies)
    {
        var cpt = 0;
        for (var i = 0; i < tabNomadAgencies.length; i++)
        {
            cpt += (this.addNomadAgency(tabNomadAgencies[i]) ? 1 : 0);
        }
        return cpt;
    },

    /**
     * Ajoute des agences nomade par pays ou régions à l'utilisateur courant
     *
     * @param Array tabNomadAgencies Tableau des agences nomade à ajouter
     * @return int
     */
    removeNomadAgencies : function(tabNomadAgencies)
    {
        var cpt = 0;
        for (var i = 0; i < tabNomadAgencies.length; i++)
        {
            cpt += (this.removeNomadAgency(tabNomadAgencies[i]) ? 1 : 0);
        }
        return cpt;
    },

    /**
     * Sélectionne ou déselectionne des agences nomade par pays ou région à l'utilisateur courant
     *
     * @param Array tabNomadAgencies tableau des agences nomade à modifier
     * @return int
     */
    switchNomadAgencies : function(tabNomadAgencies)
    {
        var i = 0;
        while (i < tabNomadAgencies.length && in_array(tabNomadAgencies[i], this._tabNomadAgencies))
        {
            i++;
        }
        if (i == tabNomadAgencies.length)
        {
            return -this.removeNomadAgencies(tabNomadAgencies);
        }
        else
        {
            return -this.addNomadAgencies(tabNomadAgencies);
        }
        return 0;
    },

    /**
     * Ajoute un groupe à l'utilisateur courant
     *
     * @param int groupId Identifiant du groupe à ajouter
     * @return bool
     */
    addGroup : function(groupId)
    {
        if (!in_array(groupId, this._tabGroups))
        {
            this._tabGroups.push(groupId);
            return true;
        }
        return false;
    },

    /**
     * Supprime un groupe à l'utilisateur courant
     *
     * @param int groupId Identifiant du groupe à ajouter
     * @return bool
     */
    removeGroup : function(groupId)
    {
        if (in_array(groupId, this._tabGroups))
        {
            var tabTmp = [];
            for (var i = 0; i < this._tabGroups.length; i++)
            {
                if (this._tabGroups[i] != groupId)
                {
                    tabTmp.push(this._tabGroups[i]);
                }
            }
            this._tabGroups = tabTmp;

            return true;
        }
        return false;
    },

    /**
     * Modifie le groupe de l'utilisateur courant
     *
     * @param int groupId Identifiant du groupe à modifier
     * @return bool
     */
    switchGroup : function(groupId)
    {
        if (this.addGroup(groupId))
        {
            return 1;
        }
        else if (this.removeGroup(groupId))
        {
            return -1;
        }
        return 0;
    },

    /**
     * Charge les données de l'utilisateur à partir d'un tableau
     *
     * @param Array tabInfos Tableau des infos à charger
     */
    loadByArray : function(tabInfos)
    {
        this._login = tabInfos['login'];
        this._id = tabInfos['id'];
        this._fullName = tabInfos['fullName'];
        this._isNomad = (tabInfos['isNomad'] ? true : false);
        this._level = Number(tabInfos['level']);
        this._tabGroups = tabInfos['tabGroups'];
        this._tabNomadAgencies = tabInfos['tabNomadAgencies'];
        this._profileId = tabInfos['profile.id'];
        this._agencyId = tabInfos['agency.id'];

        this._tabOldValues = this.copy();
    },

    /**
     * Teste si l'utilisateur a été modifié
     *
     * @return bool
     */
    hasModifications : function()
    {
        if (!this._tabOldValues)
        {
            return false;
        }
        if (this.isNomad() == this._tabOldValues['isNomad'] &&
            this.getLevel() == this._tabOldValues['level'] &&
            this.getGroups().sort().join(";") == this._tabOldValues['tabGroups'].sort().join(";") &&
            this.getNomadAgencies().sort().join(";") == this._tabOldValues['tabNomadAgencies'].sort().join(";") &&
            this.getProfileId() == this._tabOldValues['profileId'] &&
            this.getAgencyId() == this._tabOldValues['agencyId'])
        {
            return false;
        }
        return true;
    },

    /**
     * Récupère les données de l'utilisateur
     *
     * @return hash
     */
    getData : function()
    {
        return {
            'id'               : this.getId(),
            'profileId'        : this.getProfileId(),
            'agencyId'         : this.getAgencyId(),
            'level'            : this.getLevel(),
            'isNomad'          : this.isNomad(),
            'tabGroups'        : this.getGroups(),
            'tabNomadAgencies' : this.getNomadAgencies()
        };
    }
};


/**
 * Récupère les agences nomades de l'utilisateur courant
 *
 * @param Array tabSpans Tableau des spans (agences nomade)
 * @return Array
 */
function getNomadAgenciesBySpans(tabSpans)
{
    var tab = [];
    for (var i = 0; i < tabSpans.length; i++)
    {
        tab.push(tabSpans[i].innerHTML);
    }
    return tab;
}

/**
 * Réinitialise la recherche
 *
 */
function clearSearch()
{
    Location.searchBoxesManager.getSearchBox('search_user_id').clear();
}






