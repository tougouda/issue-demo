if (!window.Location)
{
    var Location = new Object();
}

var charac = {

    _data: [],
    _tabCountries: [],
    _tabAgencies: [],

    /**
     * Initialisation de la page
     */
    init: function(tabData, tabOptions)
    {
        var mySelf = this;

        this._data = tabData;
        this._tabCountries = tabOptions.tabCountries;
        this._tabAgencies  = tabOptions.tabAgencies;
        
        // Évènements
        $('#filter').keyup(function(e) {
            mySelf.search($(this).val());
        });

        this.display();
    },

    /**
     * Affichage du tableau
     */
    display: function()
    {
        var mySelf = this;
        var tbody = $('table.basic > tbody')[0];
        var cpt = 0;

        $.each(this._data, function(code, data) {

            var lineClassName = (cpt % 2 == 0 ? 'odd' : 'even');
            var tabValues = data[0].concat(data[1]).concat(data[2]);

            var newLine = $('<tr />');
            newLine.addClass(lineClassName);
            if (data[4] == STATE_DELETED)
            {
                newLine.addClass('expired');
            }

            // État
            var stateCell = $('<td />');
            stateCell.addClass('state');
            stateCell.addClass(data[4] == STATE_ACTIVE ? 'active' : 'deleted');
            newLine.append(stateCell);

            // Identifiant
            var idCell = $('<td />');
            idCell.addClass('id');
            idCell.html(data[5]);
            newLine.append(idCell);

            // Code
            var codeCell = $('<td />');
            codeCell.addClass('code');
            codeCell.html(code);
            newLine.append(codeCell);

            // Libellé
            var labelCell = $('<td />');
            labelCell.addClass('label');
            labelCell.html(data[3]);
            newLine.append(labelCell);

            // Valeurs
            var valuesCell = $('<td />');
            valuesCell.addClass('values');
            valuesCell.attr('colspan', 4);

            var tableEl = $('<table />');
            tableEl.addClass('basic');
            mySelf._displayDetailsCell(code, tableEl, tabValues, true);
            valuesCell.append(tableEl);
            newLine.append(valuesCell);

            // Historique
            var historyCell = $('<td />');
            historyCell.addClass('history');
            historyCell.click(function(e) {
                mySelf.showHistoryPopup(code, tabValues);
                return false;
            });
            newLine.append(historyCell);

            $(tbody).append(newLine);

            cpt++;
        });
    },

    search: function(search)
    {
        var tabTrs = $('#list > tbody > tr');

        for (i = 0; i < tabTrs.length; i++)
        {
            td = $(tabTrs[i]).find('td.code:contains("' + search + '"), td.label:contains("' + search + '")');//.getElementsByTagName("td")[0];
            $(tabTrs[i]).toggleClass('no-result', td.length == 0);
        }
    },

    /**
     * Affichage des cellules du tableau pour une carac
     */
    _displayDetailsCell: function(code, table, tabValues, isCurrent)
    {
        var mySelf = this;

        for (var i = 0; i < tabValues.length; i++)
        {
            if (tabValues[i] && isCurrent && tabValues[i][3])
            {
                continue;
            }
            var newLine = $('<tr />');
            if (tabValues[i][3])
            {
                newLine.addClass('expired');
            }

            var type = this._getType(tabValues[i][0]);
            // Type
            var typeCell = $('<td />');
            typeCell.addClass('type');
            typeCell.attr('title', LOC_View.getTranslation('type_' + type));

            var spanType = $('<span />');
            spanType.addClass('t_' + type);
            typeCell.append(spanType);
            newLine.append(typeCell);

            // Identifiant externe
            var externIdCell = $('<td />');
            externIdCell.addClass('externId');
            externIdCell.html(tabValues[i][0]);
            newLine.append(externIdCell);

            // Date de début
            var beginDateCell = $('<td />');
            beginDateCell.addClass('beginDate');
            beginDateCell.html(tabValues[i][2]);
            newLine.append(beginDateCell);

            if (!isCurrent)
            {
                // Date de fin
                var endDateCell = $('<td />');
                endDateCell.addClass('endDate');
                endDateCell.html(tabValues[i][3]);
                newLine.append(endDateCell);
            }

            // Valeur
            var valueCell = $('<td />');
            valueCell.addClass('value');
            var contentPre = $('<pre />');
            contentPre.html(this._format(tabValues[i][1]));

            valueCell.append(contentPre);
            valueCell.click(function(e) {
                $(this).toggleClass('expand');
            });
            newLine.append(valueCell);

            table.append(newLine);
        }

    },

    showHistoryPopup: function(code, tabValues)
    {
        $('#history-popup > h1').html(code);
        var tableEl = $('#history-popup > table > tbody');
        tableEl.html('');
        this._displayDetailsCell(code, tableEl, tabValues, false);

        Location.modalWindowManager.show('history-popup', {contentType: 3, width: 600});
    },

    /**
     * Récupération du type de carac (Global, Pays, Agence)
     */
    _getType: function(type)
    {
        if (!type)
        {
            return 'G';
        }
        if (this._tabCountries.indexOf(type) != -1)
        {
            return 'P';
        }
        if (this._tabAgencies.indexOf(type) != -1)
        {
            return 'A';
        }
    },

    /**
     * Formatage de la valeur (JSON)
     */
    _format: function(string)
    {
        try
        {
            JSON.parse(string);
        }
        catch (e) {
            return string;
        }
        return JSON.stringify(JSON.parse(string), null, 4);
    }
};

// OVERWRITES old selector
jQuery.expr[':'].contains = function(a, i, m) {
  return jQuery(a).text().toUpperCase()
      .indexOf(m[3].toUpperCase()) >= 0;
};