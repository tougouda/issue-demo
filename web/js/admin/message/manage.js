function removeMessage(id)
{
    $ge('tabData[' + id + '][remove]').value = 1;
    submitForm();
}

function submitForm()
{
    var form = document.getElementsByTagName('form')[0];
    $ge('subject').className = '';
    $ge('countryTA[fr_FR]').className = '';
    if ($ge('subject').value == '' || $ge('countryTA[fr_FR]').value == '')
    {
        $ge('valid').value = 'required';
        alert(LOC_View.getTranslation('fieldRequired'));
        if ($ge('subject').value == '')
        {
            $ge('subject').className = 'error';
        }
        if ($ge('countryTA[fr_FR]').value == '')
        {
            $ge('countryTA[fr_FR]').className = 'error';
        }
        return false;
    }
    else
    {
        $ge('valid').value = 'ok';
        form.submit();
    }
}

function refresh()
{
    var form = document.getElementsByTagName('form')[0];

    form.submit();
}