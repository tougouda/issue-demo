if (!window.Location)
{
    var Location = new Object();
}

if (!Location.viewObj)
{

if (!Location.commonObj) alert('Erreur Location (Common)');


/* Objet Location.viewObj */
Location.viewObj = {

    isCtrlPressed       : false,
    isShiftPressed      : false,
    isMouseWheelPressed : false,
    tabInitFuncs        : [],
    _isInitialized      : false,
    _tabTranslations    : {},
    _tabVars            : {},
    _tabPageInputs      : [],
    _tooltipElt         : null,
    _tooltipTimeout     : null,

    /**
     * Initialisation de l'objet
     */
    init : function()
    {
        var mySelf = this;

        // Affichage de l'heure
        this._displayTime();

        var nb = this.tabInitFuncs.length;
        for (var i = 0; i < nb; i++)
        {
            this.tabInitFuncs[i].call(this);
        }

        // Détection des touches "Control" et "Shift"
        Location.commonObj.addEltEvent(window.document.documentElement, 'keydown', function(e)
        {
            e = e || window.event;
            var keyCode = (e.keyCode || e.which);

            mySelf.isCtrlPressed  = (keyCode == 17);
            mySelf.isShiftPressed = (keyCode == 16);
        });
        Location.commonObj.addEltEvent(window.document.documentElement, 'keyup', function()
        {
            mySelf.isCtrlPressed  = false;
            mySelf.isShiftPressed = false;
        });
        
        // Détection du clic sur la molette
        Location.commonObj.addEltEvent(window.document.documentElement, 'mousedown', function(e)
        {
            e = e || window.event;

            mySelf.isMouseWheelPressed = (e.button == 1);
        });
        Location.commonObj.addEltEvent(window.document.documentElement, 'click', function()
        {
            mySelf.isMouseWheelPressed = false;
        });

        // Gestion des tabulations sur le champs de la page
        Location.commonObj.addEltEvent(window.document.documentElement, 'keydown', function(e)
                {
            e = e || window.event;
            var keyCode = (e.keyCode || e.which);
            if (keyCode == 9)
            {
                var targetElt = (e.target ? e.target : e.srcElement);
                
                var nbInputs = mySelf._tabPageInputs.length;
                for (var i = 0; i < nbInputs; i++)
                {
                    var tabElt = window.document.querySelectorAll(mySelf._tabPageInputs[i]['sel']);
                    if (in_array(targetElt, tabElt))
                    {
                        break;
                    }
                }
                if (i < nbInputs)
                {
                    var sens = (e.shiftKey ? -1 : 1);
                    mySelf.activatePageInput(i + sens, sens, i);
                    Location.commonObj.cancelEvent(e);
                    return false;
                }
            }
                });

        this._isInitialized = true;
    },

    /**
     * Récupérer la valeur d'une variable
     *
     * @param string name Nom de la variable
     * @return string
     */
    getVar : function(name)
    {
        return this._tabVars[name];
    },

    /**
     * Fonction de lancement d'un script dans une nouvelle fenêtre.
     *
     * @param string name nom(id) de la fenetre. Utiliser que des lettres.
     * @param string url  url du script à lancer.
     * @param bool   s    indique si on veut le scroll ou non
     */
    openWindow : function(name, url, s)
    {
        // Suppression des caractères spéciaux
        name = name.replace(/[\-\.\*\+]/g, '_');

        var remote = window.open(url, name);
        if (remote)
        {
            remote.focus();
        }
    },

    /**
     * Bloquer certaines touches du clavier (notamment la touche F5)
     *
     * @param Event   Evènement
     * @param tabKeys Liste des touches à bloquer
     * @param msg     Message à afficher
     * @return bool
     */
    lockKeys : function(e, tabKeys, msg)
    {
        var i = 0;
        var nb = tabKeys.length;
        var keyCode = Location.commonObj.getEventKeyCode(e);

        while (i < nb && tabKeys[i] != keyCode)
        {
            i++;
        }

        if (i < nb)
        {
            Location.commonObj.cancelEvent(e);
            alert(msg);
            return false;
        }
        return true;
    },

    /**
     * Afficher l'heure
     *
     */
    _displayTime : function()
    {
        var elt = window.document.getElementById('locStandardTime');
        if (elt)
        {
            var dt = new Date();
            var hour = dt.getHours();
            if (hour < 10)
            {
                hour = '0' + hour;
            }
            var min = dt.getMinutes();
            if (min < 10)
            {
                min = '0' + min;
            }

            elt.innerHTML = hour + ':' + min;
            var mySelf = this;
            window.setTimeout(function(){mySelf._displayTime()}, 1000);
        }
    },

    /**
     * Récupérer l'url de la balise IMG (ou SPAN)
     *
     * @param mixed elt Elément
     * @return string Url de l'image
     */
    getImageUrl : function(el)
    {
        var src = '';
        el = Location.commonObj.getElt(el);

        if (el)
        {
            var tagName = el.nodeName.toLowerCase();
            if (tagName == 'img')
            {
                src = el.src;
            }
            else if (tagName == 'span' && el.filters)
            {
                // PNG IE 6
                src = el.firstChild.src;
            }
        }
        return src;
    },

    /**
     * Définir l'url de la balise IMG (ou SPAN)
     *
     * @param mixed el Elément
     * @param string src URL de l'image
     * @return bool TRUE si c'est un succès, sinon FALSE
     */
    setImageUrl : function(el, src)
    {
        el = Location.commonObj.getElt(el);
        if (el)
        {
            var tagName = el.nodeName.toLowerCase();
            if (tagName == 'img' || tagName == 'input')
            {
                el.src = src;
                return true;
            }
            else if (el.firstChild)
            {
                // Recherche de la première image dans les fils
                el = el.firstChild;
                while (el)
                {
                    if (this.setImageUrl(el, src))
                    {
                        return true;
                    }
                    el = el.nextSibling;
                }
            }
        }
        return false;
    },

    /**
     * Limiter la taille d'un textArea
     *
     * @param e Evenement
     * @return bool
     */
    limitTextArea : function(e, maxLength)
    {
        var textAreaEl = (e.target || e.srcElement);
        var type = e.type;
        var keyCode = (e.keyCode || e.which);
        if (!maxLength)
        {
            maxLength = textAreaEl.getAttribute('maxlength');
        }

        switch (type)
        {
            case 'change':
                textAreaEl.value = textAreaEl.value.substr(0, maxLength);
                break;

            case 'keydown':
                if (textAreaEl.value.length > maxLength &&
                    keyCode != 37 && keyCode != 38 &&
                    keyCode != 39 && keyCode != 40 &&
                    keyCode != 8 && keyCode != 46 &&
                    (keyCode != 13 || (keyCode == 13 && e.shiftKey)) && keyCode != 16 && keyCode != 17)
                {
                    return false;
                }
                break;
        }
        return true;
    },

    /**
     * Récupérer l'instance de la popup courante (contenant l'iframe courante)
     *
     * @return Location.Popup
     */
    getCurrentPopup : function()
    {
        var tabFrames = window.parent.document.getElementsByTagName('IFRAME');
        for (var i = 0; i < tabFrames.length; i++)
        {
            if (tabFrames[i].contentWindow == window)
            {
                if (parent.Location && parent.Location.popupsManager)
                {
                    var popupName = tabFrames[i].parentNode.parentNode.parentNode.id;
                    var myPopup = parent.Location.popupsManager.getPopup(popupName);
                    if (myPopup && myPopup._contentType && myPopup._contentType == 1)
                    {
                        return myPopup;
                    }
                }
            }
        }
        return false;
    },

    /**
     * Définir la taille de la popup courante
     *
     * @param int height
     */
    setCurrentPopupHeight : function(height)
    {
        var contentElt = Location.commonObj.getElt('locStandardContent');
        if (contentElt)
        {
            contentElt.style.overflow = "hidden";
        }
        var myPopup = this.getCurrentPopup();
        if (myPopup)
        {
            myPopup.setHeight(height);
        }
    },

    /**
     * Formatter un nombre
     *
     * @param float  value          Nombre à formater
     * @param int    precision      Nombre de décimales
     * @param string currencySymbol
     * @return string Nombre au format monétaire
     */
    getCurrencyFormat : function(value, precision, currencySymbol)
    {
        if (typeof currencySymbol == 'undefined')
        {
            currencySymbol = this._tabVars['currencySymbol'];
        }
        if (typeof precision == 'undefined')
        {
            precision = this._tabVars['fracDigits'];
        }
        return this.getNumberFormat(value, precision) + ' ' + currencySymbol;
    },

    /**
     * Retourne le nombre d'octets formaté suivant la grandeur
     *
     * @param  int   bytes   - Nombre à formater
     * @param  int   precision - Nombre de décimales à afficher
     * @return string
     */
    getBytesFormat: function(bytes, precision)
    {
        var tabSizes = ['o', 'Ko', 'Mo', 'Go', 'To', 'Po', 'Eo', 'Zo', 'Yo'];

        for (var i = 0; i < tabSizes.length; i++)
        {
            if (bytes < 1024)
            {
                return this.getNumberFormat(bytes, precision) + ' ' + tabSizes[i];
            }
            bytes = bytes / 1024;
        }
    },

    /**
     * Formatter une date
     *
     * @param string value      la date au format MySQL
     * @param bool   isDateTime Indique si il s'agit du format date/heure (sinon le format date sera sélectionné)
     * @return string Date au format de la locale
     */
    getDateFormat : function(value, isDateTime)
    {
        if (typeof(value) != 'string' || value == '')
        {
            return '';
        }
        var year  = value.substr(0, 4);
        var month = value.substr(5, 2);
        var day   = value.substr(8, 2);
        var time  = value.substr(11);
        if (isDateTime && time == '')
        {
            time = '00:00:00';
        }

        // pattern de date
        var pattern = this._tabVars[(isDateTime ? 'dateTimePattern' : 'datePattern')];
        var result = pattern;

        // Paramètres de chacun des éléments de la date
        var tabParams = {year: {}, month: {}, day: {}};

        // Récupération des paramètres de l'année
        var tabYearPatterns = ['%g', '%G', '%y', '%Y'];
        var yearPattern;
        for (var ind in tabYearPatterns)
        {
            var index = pattern.indexOf(tabYearPatterns[ind]);
            if (index != -1)
            {
                yearPattern = tabYearPatterns[ind];
            }
        }

        // Application de la date sur le pattern
        result = result.replace('%d', day);
        result = result.replace('%m', month);
        result = result.replace(yearPattern, year);
        if (isDateTime)
        {
            result = result.replace('%T', time);
        }

        return result;
    },

    /**
     * Formatter un nombre
     *
     * @param float value          Nombre à formater
     * @param int   precision      Nombre de décimales
     * @param bool  trailingZeroes Affichage des zéros dans la partie décimale
     * @return string Nombre formatté
     */
    getNumberFormat : function(value, precision)
    {
        // Nombre sous forme exponentielle (ex : 2.05e7) non modifié
        if (String(value).match('e'))
        {
            return value;
        }

        value = parseFloat(value);
        if (isNaN(value) || !isFinite(value))
        {
            value = 0;
        }

        // Valeur formatée (sans le signe)
        var formatedValue = Math.abs(value);

        // Valeur arrondie
        if (typeof precision == 'undefined')
        {
            precision = this._tabVars['fracDigits'];
        }
        formatedValue = String(Location.commonObj.round(formatedValue, precision));

        // Ajout de zéros décimaux
        var searchPoint = formatedValue.search(/\./);
        var nbDecimals = precision;

        if (nbDecimals == 0)
        {
            searchPoint = formatedValue.length;
        }
        else
        {
            if (searchPoint >= 0 || nbDecimals < 0)
            {
                nbDecimals = formatedValue.length - searchPoint - 1;
                nbDecimals = precision - nbDecimals;
            }
            else
            {
                formatedValue += '.';
            }
            for (var i = 0; i < nbDecimals; i++)
            {
                formatedValue += '0';
            }
            searchPoint = formatedValue.search(/\./);
        }

        // Remplacement du séparateur décimal
        formatedValue = formatedValue.replace('.', this._tabVars['decimalPoint']);

        // Ajout des séparateurs de milliers
        for (var i = searchPoint - this._tabVars['grouping'][0]; i > 0; i -= this._tabVars['grouping'][0])
        {
            formatedValue = formatedValue.substr(0, i) + this._tabVars['thousandsSep'] + formatedValue.substr(i);
        }

        // Ajout du signe pour les valeurs négatives
        return (value < 0 ? '-' : '') + formatedValue;
    },

    /**
     * Récupérer la traduction d'un message
     *
     * @param string Id du message
     * @return string
     */
    getTranslation : function(msgId)
    {
        return (this._tabTranslations[msgId] ? this._tabTranslations[msgId] : '[' + msgId + ']');
    },

    /**
     * Définir les traductions
     *
     * @param Object tabTranslations
     */
    setTranslations : function(tabTranslations)
    {
        this._tabTranslations = tabTranslations;
    },

    /**
     * Définir les champs de la page
     *
     * @param Array tabPageInputs
     */
    setPageInputs : function(tabPageInputs)
    {
        this._tabPageInputs = tabPageInputs;
    },

    /**
     * Définir les variables
     *
     * @param Object tabVars
     */
    setVars : function(tabVars)
    {
        this._tabVars = tabVars;
    },

    /**
     * Fonction de rafraichissent de la page (sans soumission)
     *
     */
    reload : function()
    {
        // Réactualisation de la page
        var url = window.location.href;
        var paramStr = '_locRandomValue=' + new Date().getTime();
        if (url.indexOf('_locRandomValue=') != -1)
        {
            url = url.replace(/_locRandomValue=[0-9]*/, paramStr);
        }
        else
        {
            var strEnd = '';
            var index = url.indexOf('#');
            if (index != -1)
            {
                strEnd = url.substr(index);
                url = url.substr(0, index);
            }

            if (url.indexOf('?') == -1)
            {
                url += '?';
            }
            else
            {
                url += '&';
            }
            url += paramStr + strEnd;
        }
        window.location.href = url;
    },

    /**
     * Activer un bloc de boutons d'impression
     *
     * @param string id Identifiant du bloc de boutons
     */
    enablePrintButtons : function(id)
    {
        window.document.getElementById(id).className = 'printButtons';
    },

    /**
     * Désactiver un bloc de boutons d'impression
     *
     * @param string id Identifiant du bloc de boutons
     */
    disablePrintButtons : function(id)
    {
        window.document.getElementById(id).className = 'printButtons disabled';
    },

    /**
     * Activer un champ de la page
     *
     * @param int i    Index du champ
     * @param int sens Sens (1 ou -1)
     * @param
     */
    activatePageInput : function(i, sens, origin)
    {
        var nbInputs = this._tabPageInputs.length;
        if (i >= nbInputs)
        {
            i = 0;
        }
        else if (i < 0)
        {
            i = nbInputs - 1;
        }
        if (i == origin)
        {
            return false;
        }
        var tab = this._tabPageInputs[i];
        var el = (tab['get'] ? eval(tab['get']) : window.document.querySelectorAll(tab['sel'])[0]);
        var isActivable = false;
        if (el)
        {
            isActivable = true;
            try
            {
                if (tab['*if'])
                {
                    isActivable = (new Function('return (' + tab['*if'] + ');')).call(el);
                }
                if (isActivable)
                {
                    if (tab['if'])
                    {
                        isActivable = (new Function('return (' + tab['if'] + ');')).call(el);
                    }
                    else
                    {
                        isActivable = ((el.isDisabled && typeof el.isDisabled == 'function' ? el.isDisabled() : el.disabled) ? false : true);
                    }

                    if (isActivable && tab['if*'])
                    {
                        isActivable = (new Function('return ' + tab['if*'] + ';')).call(el);
                    }
                }
            }
            catch (e)
            {
                isActivable = false;
            }
        }
        if (!isActivable)
        {
            return this.activatePageInput(i + sens, sens, origin);
        }

        var funcStr = tab['set'];
        if (!funcStr)
        {
            funcStr = 'this.focus();';
        }
        if (tab['*set'])
        {
            funcStr = tab['*set'] + ';' + funcStr;
        }
        if (tab['set*'])
        {
            funcStr += ';' + tab['set*'];
        }

        try
        {
            (new Function(funcStr)).call(el);
        }
        catch (e)
        {
            return this.activatePageInput(i + sens, sens, origin);
        }
        return true;
    },

    /**
     * Afficher l'info-bulle
     *
     * @param Object position Position (propriétés: x et y)
     * @param string text     Texte de l'info-bulle
     * @param string style    Style de l'info bulle
     */
    showTooltip : function(position, text, style)
    {
        var obj = this._tooltipElt;
        if (!obj)
        {
            obj = document.createElement('div');
            obj.id = 'helpToolTip';
            document.body.appendChild(obj);
            this._tooltipElt = obj;
        }
        window.clearTimeout(this._tooltipTimeout);
        this._tooltipTimeout = null;
        obj.innerHTML  = text;
        obj.className  = 'tooltip' + (style ? ' ' + style : '');
        obj.style.display = '';
        var x = Math.min(document.body.clientWidth - obj.offsetWidth, Math.max(0, position.x - 50));
        var y = Math.max(0, position.y - obj.offsetHeight - 10);
        if (y == 0)
        {
            y = Math.min(document.body.clientHeight, position.y + 20);
        }
        obj.style.left = x + 'px';
        obj.style.top  = y + 'px';
    },

    /**
     * Cacher l'info-bulle
     *
     */
    hideTooltip : function(timeout)
    {
        var obj = this._tooltipElt;
        if (obj)
        {
            window.clearTimeout(this._tooltipTimeout);
            this._tooltipTimeout = window.setTimeout(function() { obj.style.display = 'none'; }, timeout);
        }
    }
}


//Champ de type numérique (0123456789./-)
function inputTextNumeric(e)
{
    if (window.document.all)
    {
        e = event;
    }

    if (e.type == 'keypress')
    {
        var myexp = new RegExp('[0-9\.\/\-]');

        var keyCode = Location.commonObj.getEventKeyCode(e);
        var keyChar = Location.commonObj.getEventKeyChar(e);
        var test = (keyCode < 32 || myexp.test(keyChar));
        if (!test) Location.commonObj.cancelEvent(e);
        return test;
    }
    else
    {
        var elt = Location.commonObj.getEventSrcEl(e);
        var str = elt.value.replace(/[^0-9\.\/\-]/g, '');
        if (str != elt.value) elt.value = str;
    }
    return true;
}

// Champ de type numérique (0123456789)
function inputTextDigit(e)
{
    if (window.document.all)
    {
        e = event;
    }
    if (e.type == 'keypress')
    {
        var myexp = new RegExp('[0-9]');

        var keyCode = Location.commonObj.getEventKeyCode(e);
        var keyChar = Location.commonObj.getEventKeyChar(e);
        var test = (keyCode < 32 || myexp.test(keyChar));
        if (!test) Location.commonObj.cancelEvent(e);
        return test;
    }
    else
    {
        var elt = Location.commonObj.getEventSrcEl(e);
        var str = elt.value.replace(/[^0-9]/g, '');
        if (str != elt.value) elt.value = str;
    }
    return true;
}

//Champ de type texte en majuscules ou minuscules
function inputTextCase(e, eventCase)
{
    if (window.document.all)
    {
        e = event;
    }

    if (e.type == 'keyup')
    {
        var ctrl = e.target;

        var caretPos = 0;
        if (document.selection)
        {
            ctrl.focus();
            var range = document.selection.createRange();
            range.moveStart('character', -ctrl.value.length);
            caretPos = range.text.length;
        }
        else if (ctrl.selectionStart || ctrl.selectionStart == '0')
        {
            caretPos = ctrl.selectionStart;
        }

        if (eventCase == 'upper')
        {
            ctrl.value = ctrl.value.toLocaleUpperCase();
        }
        else
        {
            ctrl.value = ctrl.value.toLocaleLowerCase();
        }


        if (ctrl.setSelectionRange)
        {
            ctrl.focus();
            ctrl.setSelectionRange(caretPos, caretPos);
        }
        else if (ctrl.createTextRange)
        {
            var range = ctrl.createTextRange();
            range.collapse(true);
            range.moveEnd('character', caretPos);
            range.moveStart('character', caretPos);
            range.select();
        }
    }
    else if (e.type == 'paste')
    {
        var inputElt = Location.commonObj.getEventSrcEl(e);
        if (eventCase == 'upper')
        {
            window.setTimeout(function(){inputElt.value = inputElt.value.toUpperCase();}, 50);
        }
        else
        {
            window.setTimeout(function(){inputElt.value = inputElt.value.toLowerCase();}, 50);
        }
    }
    return true;
}


// Retourne toutes les propriétés d'un élément
function get_props(elm)
{
    str = '';
    for(var prop in elm)
        str += prop + ' = ' + elm[prop]+ '              <br/>\n\n';

    w = window.open();
    w.document.write(str);
    w.document.close();
}

// Pertes de mémoire Internet Explorer 6 !!!
if (window.attachEvent)
{
    window.attachEvent('onunload', function()
                                   {
                                       var clearElementProps = ['data', 'onmouseover', 'onmouseout',
                                                                'onmousedown', 'onmouseup', 'ondblclick',
                                                                'onclick', 'onselectstart', 'oncontextmenu'];

                                       var el;
                                       for(var d = window.document.all.length; d--;)
                                       {
                                           el = window.document.all[d];
                                           for(var c = clearElementProps.length; c--;)
                                           {
                                               el[clearElementProps[c]] = null;
                                           }
                                       }
                                   } );
}

var LOC_View = Location.viewObj;

} // IF EXIST : Location.viewObj
