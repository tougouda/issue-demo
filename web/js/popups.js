if (!window.Location)
{
    var Location = new Object();
}

if (!Location.commonObj) alert('Erreur Location');


/* Classe Location.Popup */
Location.Popup = function(pluginObj, name, title, left, top, width, height, tabOptions)
{
    this.name = name;
    this._title = title;
    this._left = left;
    this._top = top;
    this._width = width;
    this._height = height;
    this._tabOptions = (tabOptions ? tabOptions : {});

    this._pluginObj = pluginObj;
    this._isCreated = false;
    this._isLoading = false;
    this._isFirstLoad = true;
    this._rect = {x:0, y:0, w:0, h:0};
    this._el = null;
    this._topEl = null;
    this._titleEl = null;
    this._middleEl = null;
    this._contentEl = null;
    this._moveMaskEl = null;
    this._loadingMaskEl = null;

    this._fadeTimer = null;
    this._oldWidth = false;

    this._ajaxContentObj = null;
    this._iframeEl = null;

    this._createIndex = (this._tabOptions['createIndex'] ? this._tabOptions['createIndex'] : 0);
    this._contentType = (this._tabOptions['contentType'] ? this._tabOptions['contentType'] : 1);


    this.onminimize = null;
    this.onmaximize = null;
    this.onopen = null;
    this.onclose = null;
    this.onfocus = null;
    this.oncompletion = null;
}

Location.Popup.prototype = {

    /**
     * Vérifier la position de la popup
     *
     */
    checkPos : function()
    {
        this._create();

        this.getRect();
        this.setPosition(parseInt(this._el.style.left), parseInt(this._el.style.top), true);
    },

    /**
     * Fermer la popup
     */
    close : function()
    {
        if (this._isCreated)
        {
            // Exécution de la fonction utilisateur
            if (this.onclose)
            {
                var result = this.onclose();
                if (typeof result != 'undefined' && !result)
                {
                    // Fermeture de la popup bloquée par l'utilisateur
                    return;
                }
            }

            // Destruction de l'iframe
            if (this._iframeEl)
            {
                this._iframeEl.src = 'about:blank';
                this._contentEl.removeChild(this._iframeEl);
                this._iframeEl = null;
            }

            if (this._isCreated)
            {
                this._el.style.display = 'none';
                this._pluginObj._checkForegroundPopup();
            }

            this._isLoading = false;
        }
    },

    /**
     * Stopper le mouvement de la popup
     *
     */
    endMove : function()
    {
        if (this._el.releaseCapture)
        {
            this._el.releaseCapture();
        }
        // Cache les masks
        this._pluginObj._displayMasks(false);
        this._el.className = 'locPlgPopupsWindow';
    },

    /**
     * Récupérer l'élément DIV du contenu de la popup
     *
     * @return object L'élément
     */
    getContentEl : function()
    {
        this._create();
        return this._contentEl;
    },

    /**
     * Récupérer l'élément DIV principal de la popup
     *
     * @return object L'élément
     */
    getEl : function()
    {
        this._create();
        return this._el;
    },

    /**
     * Récupérer la position de la popup
     *
     * @return object Position
     */
    getPos : function()
    {
        this._create();

        return {x:parseInt(this._el.style.left), y:parseInt(this._el.style.top)};
    },

    /**
     * Recalculer et récupérer la région de la popup
     *
     * @return object Région
     */
    getRect : function()
    {
        this._create();

        this._rect.x = parseInt(this._el.style.left);
        this._rect.y = parseInt(this._el.style.top);
        this._rect.w = this._el.offsetWidth;
        this._rect.h = this._el.offsetHeight;

        return this._rect;
    },

    /**
     * Récupérer le titre de la popup
     *
     * @return string Titre de la popup
     */
    getTitle : function()
    {
        return this._title;
    },

    /**
     * Récupérer le Z-index de la popup
     *
     * @return int Index
     */
    getZIndex : function()
    {
        this._create();
        return this._el.style.zIndex;
    },

    /**
     * Indique si la popup est visible ou non
     *
     * @return bool
     */
    isVisible : function()
    {
        if (this._isCreated && this._el.style.display != 'none')
        {
            return this._el.style.zIndex;
        }
        return false;
    },

    /**
     * Ouvrir la popup
     *
     * @param url Adresse URL
     */
    open : function(url, minMaxMode)
    {
        // Exécution de la fonction utilisateur
        if (this.onopen)
        {
            var result = this.onopen();
            if (typeof result != 'undefined' && !result)
            {
                // Ouverture de la popup bloquée par l'utilisateur
                return;
            }
        }

        this._create();

        if (url)
        {
            this.changeUrl(url);
        }


        this._el.style.display = '';
        this.setFocus();
        this.checkPos();

        if (!minMaxMode)
        {
            minMaxMode = 1;
        }
        this.setMinMax(minMaxMode);

        if (this._isFirstLoad)
        {
            this.setPosition(this._left, this._top);
        }
    },

    /**
     * Définir la hauteur du contenu de la popup
     *
     * @param int height Hauteur du contenu
     */
    setContentHeight : function(height)
    {
        this._create();
        this._contentEl.style.height = parseInt(height) + 'px';
        this.checkPos();
    },

    /**
     * Définir les dimensions de la popup
     *
     * @param int width  Largeur
     * @param int height Hauteur
     */
    setDimensions : function(width, height)
    {
        this.setWidth(width);
        this.setHeight(height);
    },

    /**
     * Mettre la popup au premier plan
     *
     */
    setFocus : function()
    {
        if (!this._isCreated)
        {
            return false;
        }

        // Changement du Z-index
        var fgZIndex = this._pluginObj.zIndexStart + this._pluginObj.getPopupsCount();
        if (this._el.style.zIndex != fgZIndex)
        {
            var zIndex;
            var popupsList = this._pluginObj.popupsList;
            for (var popupName in popupsList)
            {
                if (popupsList[popupName]._isCreated)
                {
                    zIndex = popupsList[popupName]._el.style.zIndex;
                    if (zIndex > this._el.style.zIndex)
                    {
                        popupsList[popupName]._el.style.zIndex--;
                    }
                }
            }
            this._el.style.zIndex = fgZIndex;
        }

        // Focus sur l'iframe
        /*if(this._iframeEl && this._contentEl.style.display != 'none')
        {
            this._iframeEl.focus();
        }*/

        this._pluginObj._checkForegroundPopup();

        // Exécution de la fonction utilisateur
        if (this.onfocus)
        {
            this.onfocus();
        }

        return true;
    },

    /**
     * Définir la hauteur de la popup
     *
     * @param int height Hauteur
     */
    setHeight : function(height)
    {
        this.setContentHeight(height);
    },

    /**
     * Maximiser/Minimiser la popup
     *
     * @param int mode 0: maximiser ou minimiser selon l'état courant (défaut),
     *                 1: forcer maximiser,
     *                 2: forcer minimiser
     */
    setMinMax : function(mode)
    {
        this._create();

        if (!mode)
        {
            mode = 0;
        }

        if (mode == 1 || (mode == 0 && this._contentEl.style.display == 'none'))
        {
            // Exécution de la fonction utilisateur
            if (this.onmaximize)
            {
                var result = this.onmaximize();
                if (typeof result != 'undefined' && !result)
                {
                    // Opération bloquée par l'utilisateur
                    return;
                }
            }

            if (this._oldWidth)
            {
                this._oldWidth = false;
                this._el.style.width = '';
            }
            this._contentEl.style.display = '';
        }
        else if (mode == 2 || mode == 0)
        {
            // Exécution de la fonction utilisateur
            if (this.onminimize)
            {
                var result = this.onminimize();
                if (typeof result != 'undefined' && !result)
                {
                    // Opération bloquée par l'utilisateur
                    return;
                }
            }

            if (this._el.style.width == '')
            {
                this._oldWidth = true;
                this._el.style.width = this._el.offsetWidth + 'px';
            }
            this._contentEl.style.display = 'none';
            this._middleEl.style.paddingTop = '2px';
        }

        this.checkPos();
    },

    /**
     * Définir la position de la popup
     *
     * @param int  posX            Position en abscisses
     * @param int  posY            Position en ordonnées
     * @param bool isWithoutScroll Indique si il ne faut pas prendre en compte les scroll du conteneur
     */
    setPosition : function(posX, posY, isWithoutScroll)
    {
        this._create();
        this.getRect();

        var containerEl = this._pluginObj.getContainerEl();
        var zoneWidth, zoneHeight, zoneLeft, zoneTop;

        if (!isWithoutScroll)
        {
            zoneLeft = containerEl.scrollLeft;
            zoneTop = containerEl.scrollTop;
            zoneWidth = containerEl.scrollWidth > containerEl.clientWidth ? containerEl.scrollWidth : containerEl.clientWidth;
            zoneHeight = containerEl.scrollHeight > containerEl.clientHeight ? containerEl.scrollHeight : containerEl.clientHeight;
        }
        else
        {
            zoneLeft = 0;
            zoneTop = 0;
            zoneWidth = containerEl.scrollWidth;
            zoneHeight = containerEl.scrollHeight;
        }

        // Positionnement centré
        if (posX == 'center')
        {
            posX = Number((containerEl.offsetWidth - this._rect.w) / 2 + zoneLeft);
        }
        if (posY == 'center')
        {
            posY = Number((containerEl.offsetHeight - this._rect.h) / 2 + zoneTop);
        }
        posX = Number(posX);
        posY = Number(posY);

        // Popups aimentées
        var epsilon = this._pluginObj.epsilon;
        var margin  = this._pluginObj.margin;

        var popupsList = this._pluginObj.popupsList;
        var foundX = false, foundY = false;
        var myRect;
        for(var popupName in popupsList)
        {
            if (popupName != this.name && popupsList[popupName]._isCreated &&
                                          popupsList[popupName]._el.style.display != 'none')
            {
                myRect = popupsList[popupName].getRect();
                if (!foundY && (posX <= (myRect.x + myRect.w)) && ((posX + this._rect.w) >= myRect.x))
                {
                    if ((posY > (myRect.y + myRect.h + margin) - epsilon) &&
                        (posY < (myRect.y + myRect.h + margin) + epsilon))
                    {
                        posY = (myRect.y + myRect.h + margin);
                        foundY = true;
                    }
                    else if (((posY + this._rect.h) > (myRect.y - margin) - epsilon) &&
                             ((posY + this._rect.h) < (myRect.y - margin) + epsilon))
                    {
                        posY = (myRect.y - margin) - this._rect.h;
                        foundY = true;
                    }
                }

                if (!foundX && (posY <= (myRect.y + myRect.h)) && ((posY + this._rect.h) >= myRect.y))
                {
                    if ((posX > (myRect.x + myRect.w + margin) - epsilon) &&
                        (posX < (myRect.x + myRect.w + margin) + epsilon))
                    {
                        posX = (myRect.x + myRect.w + margin);
                        foundX = true;
                    }
                    else if (((posX + this._rect.w) > (myRect.x - margin) - epsilon) &&
                             ((posX + this._rect.w) < (myRect.x - margin) + epsilon))
                    {
                        posX = (myRect.x - margin) - this._rect.w;
                        foundX = true;
                    }
                }
            }
        }

        // La popup ne doit pas sortir de la région du conteneur
        if (posX + this._rect.w > zoneWidth)
        {
            posX = zoneWidth - this._rect.w;
        }
        if (posY + this._rect.h > zoneHeight)
        {
            posY = zoneHeight - this._rect.h;
        }

        if (posX < 0)
        {
            posX = 0;
        }
        if (posY < 0)
        {
            posY = 0;
        }

        this._el.style.left = posX + 'px';
        this._el.style.top  = posY + 'px';

        // Recalcul de la région de la popup
        this.getRect();
    },

    /**
     * Définir le titre de la popup
     *
     * @param string title Titre de la popup
     */
    setTitle : function(title)
    {
        this._create();
        this._title = title;
        this._titleEl.innerHTML = this._title;
    },

    /**
     * Définir la largeur de la popup
     *
     * @param int width Largeur
     */
    setWidth : function(width)
    {
        this._create();
        this._el.style.width =  parseInt(width) + 'px';
        this.checkPos();
    },

    /**
     * Définir le Z-index de la popup
     *
     * @param int index Index
     */
    setZIndex : function(index)
    {
        this._create();
        this._el.style.zIndex = index;
    },

    /**
     * Démarrer le mouvement de la popup
     *
     */
    startMove : function()
    {
        this._el.className = 'locPlgPopupsWindow move';
        if (this._el.setCapture)
        {
            this._el.setCapture();
        }
        // Affiche les masks
        this._pluginObj._displayMasks(true);
    },

    changeUrl : function(url, isForced)
    {
        this._create();

        if (this._contentType == 3 ||
            (this._contentType == 1 && this._iframeEl && this._iframeEl.src == url && !isForced))
        {
            return;
        }
        this._isLoading = true;

        var mySelf = this;

        // Affiche le masque de chargement
        this._loadingMaskEl.style.display = 'block';
        this._setMaskOpacity(1);
        if (this._fadeTimer)
        {
            window.clearTimeout(this._fadeTimer);
            this._fadeTimer = null;
        }

        this._setTitleMsg('Chargement ...');

        if (this._contentType == 1)
        {
            if (this._iframeEl)
            {
                this._iframeEl.src = 'about:blank';
                this._contentEl.removeChild(this._iframeEl);
            }

            this._iframeEl = window.document.createElement('IFRAME');
            this._iframeEl.className = 'iframeContent';
            this._iframeEl.frameBorder = this._iframeEl.scrolling = 'no';
            this._contentEl.appendChild(this._iframeEl);

            var mySelf = this;
            Location.commonObj.addEltEvent(this._iframeEl, 'load', function(e){mySelf._loaded(false);});

            this._iframeEl.src = url;
        }
        else
        {
            if (!this._ajaxContentObj)
            {
                this._ajaxContentObj = new Location.AjaxContent(this._contentEl, url);
                this._ajaxContentObj.oncompletion = function(){mySelf._loaded(false);};
                this._ajaxContentObj.onerror = function(){mySelf._loaded(true);};
            }
            else
            {
                this._ajaxContentObj.setUrl(url);
            }

            // Contenu Ajax
            this._ajaxContentObj.load();
        }
    },

    /**
     * Génération de la popup
     *
     */
    _create : function()
    {
        if (this._isCreated)
        {
            return false;
        }
        this._isCreated = true;

        var mySelf = this;
        var containerEl = this._pluginObj.getContainerEl();

        // Création de la popup
        var contentEl = window.document.getElementById(this.name);
        if (contentEl)
        {
            contentEl.removeAttribute('id');
            if (contentEl.nodeName.toLowerCase() == 'div')
            {
                this._title = contentEl.title;
                contentEl.removeAttribute('title');
                contentEl.style.display = 'none';
                this._contentEl = contentEl;
                this._contentType = 3;
            }
        }
        this._el = window.document.createElement('DIV');
        this._el.className = 'locPlgPopupsWindow';
        this._el.id = this.name;
        this._el.style.display = 'none';
        containerEl.appendChild(this._el);
        Location.commonObj.addEltEvent(this._el, 'click', function(e){ Location.commonObj.stopEventBubble(e); });


        // * Barre de titre (titre, minimizer, fermer)
        this._topEl = window.document.createElement('DIV');
        this._topEl.className = 'top';
        this._topEl.setAttribute('popupName', name);
        this._el.appendChild(this._topEl);
        Location.commonObj.cancelTextSelection(this._topEl);
        Location.commonObj.addEltEvent(this._topEl, 'mousedown', function(e){ mySelf.setFocus(); mySelf._pluginObj._startMoveEvent(e, mySelf); Location.commonObj.cancelEvent(e); });
        Location.commonObj.addEltEvent(this._topEl, 'dblclick', function(e){ mySelf.setMinMax(); });

        //   - Titre de la popup
        this._titleEl = window.document.createElement('DIV');
        this._titleEl.className = 'title';
        this._titleEl.innerHTML = this._title;
        this._topEl.appendChild(this._titleEl);

        //   - Image de fermeture
        img = window.document.createElement('A');
        img.href = '#';
        img.className = 'closeBtn';
        this._topEl.appendChild(img);
        img.onclick = function(e){ mySelf.close(); return false; };
        img.onmousedown = img.ondblclick = function(e){ Location.commonObj.stopEventBubble(e); };
        Location.commonObj.cancelTextSelection(img);

        // * Milieu de la popup
        this._middleEl = window.document.createElement('DIV');
        this._middleEl.className = 'middle';
        this._el.appendChild(this._middleEl);

        // Masque pour le chargement
        this._loadingMaskEl = window.document.createElement('DIV');
        this._loadingMaskEl.className = 'loadingMask';
        this._middleEl.appendChild(this._loadingMaskEl);

        // Pour le déplacement de la popup
        this._moveMaskEl = window.document.createElement('DIV');
        this._moveMaskEl.className = 'moveMask';
        this._middleEl.appendChild(this._moveMaskEl);

        //   - Contenu de la popup
        if (!this._contentEl)
        {
            this._contentEl = window.document.createElement('DIV');
        }
        this._contentEl.className = 'content';
        this._middleEl.appendChild(this._contentEl);

        // Astuce pour la hauteur minimale au premier chargement
        if (this._contentType == 2)
        {
            var divEl = window.document.createElement('DIV');
            divEl.style.height = '50px';
            divEl.style.width = '150px';
            this._contentEl.appendChild(divEl);
        }

        // Taille de la popup
        this._el.style.width = (String(this._width).match(/^[0-9]+$/) ? this._width + 'px' : this._width);
        this._contentEl.style.height = (String(this._height).match(/^[0-9]+$/) ? this._height + 'px' : this._height);
        this._el.style.left = '0px';
        this._el.style.top = '0px';
        this._isFirstLoad = true;

        // Ajoute cette popup dans le conteneur
        this._el.style.zIndex = this._pluginObj.zIndexStart + this._createIndex;

        return true;
    },

    _loaded : function(hasErrors)
    {
        // Enlève le masque de chargement
        if (this._loadingMaskEl.style.display != 'none')
        {
            if (LOC_ClientInfo.isMSIE)
            {
                this._loadingMaskEl.style.display = 'none';
            }
            else
            {
                this._maskFade(1, 0.65);
            }
        }

        if (hasErrors)
        {
            this._setTitleMsg('Erreur ...');
            this.setMinMax(2);
        }
        else
        {
            if (this._contentType == 1)
            {
                if (this._iframeEl)
                {
                    var mySelf = this;
                    // Pour IE
                    this._iframeEl.onfocus = function(e){mySelf.setFocus();};
                    // Pour FireFox
                    var doc = Location.commonObj.getIFrameDocument(this._iframeEl);
                    if (doc)
                    {
                        Location.commonObj.addEltEvent(doc, 'click', function(e){mySelf.setFocus();});
                        // Modification du titre de la popup
                        if (doc.title && doc.title.trim() != '')
                        {
                            this._title = doc.title;
                        }
                    }
                }
            }
            else
            {
                // Force à recalculer la taille du div top (IE-7)
                if (LOC_ClientInfo.isMSIE)
                {
                    var navVersion = LOC_ClientInfo.navigatorVersion;
                    if (navVersion == 7)
                    {
                        this._topEl.style.width = 'auto';
                        this._topEl.style.width = '';
                    }
                }

                var response = this._ajaxContentObj.getResponse();
                if (typeof response['title'] != 'undefined')
                {
                    this._title = response['title'];
                }

                // Evénements
                if (response['events'])
                {
                    var tabEvents = response['events'];
                    if (tabEvents['completion'])
                    {
                        this.oncompletion = Function(tabEvents['completion']);
                    }
                    if (tabEvents['minimize'])
                    {
                        this.onminimize = Function(tabEvents['minimize']);
                    }
                    if (tabEvents['maximize'])
                    {
                        this.onmaximize = Function(tabEvents['maximize']);
                    }
                    if (tabEvents['open'])
                    {
                        this.onopen = Function(tabEvents['open']);
                    }
                    if (tabEvents['close'])
                    {
                        this.onclose = Function(tabEvents['close']);
                    }
                    if (tabEvents['focus'])
                    {
                        this.onfocus = Function(tabEvents['focus']);
                    }
                }

                if (this._isFirstLoad)
                {
                    this.setPosition(left, top);
                }
                else
                {
                    this.checkPos();
                }
            }
            this.setTitle(this._title);
        }

        // Exécution de la fonction utilisateur
        if (this.oncompletion)
        {
            this.oncompletion();
        }

        // Chargement terminé
        this._isLoading = false;
        this._isFirstLoad = false;
    },

    _maskFade : function(opacity, velocity)
    {
        opacity *= velocity;
        if (opacity <= 0.2)
        {
            this._fadeTimer = null;
            this._loadingMaskEl.style.display = 'none';
            this._setMaskOpacity(1);
        }
        else
        {
            this._setMaskOpacity(opacity);

            var mySelf = this;
            this._fadeTimer = window.setTimeout(function (){mySelf._maskFade(opacity, velocity);}, 50);
        }
    },

    _setMaskOpacity : function(opacity)
    {
        if (LOC_ClientInfo.isMSIE)
        {
            this._loadingMaskEl.style.filter = (opacity == 1 ? 'none' : 'alpha(opacity=' + (opacity * 100) + ')');
        }
        else
        {
            this._loadingMaskEl.style.opacity = opacity;
        }
    },

    _setTitleMsg : function(msg)
    {
        this.setTitle(this._title);
        var text = '';
        if (this._titleEl.innerHTML != '')
        {
            text = this._titleEl.innerHTML + ' - ';
        }
        text += msg;
        this._titleEl.innerHTML = text;
    }
}


/**
 * Objet Location.popupsManager
 *
 */
Location.popupsManager = {

    popupsList : [],
    zIndexStart : 100,
    epsilon : 15,
    margin : 1,

    _isCreated : false,
    _containerEl : null,
    _startEventPos : {x:0, y:0},
    _startPosWindow : {x:0, y:0},
    _activePopup : null,
    _nbPopups : 0,


    setContainer : function(containerEl)
    {
        this._containerEl = containerEl;
    },

    /**
     * Evénement
     *
     * @param event e Evénement
     */
    _startMoveEvent : function(e, popup)
    {
        if (!e)
        {
            e = window.event;
        }

        this._activePopup = popup;
        this._startEventPos.x = e.clientX;
        this._startEventPos.y = e.clientY;

        this._startPosWindow = this._activePopup.getPos();
        this._activePopup.startMove();
        Location.commonObj.stopEventBubble(e);
    },

    /**
     * Evénement
     *
     * @param event e Evénement
     */
    _endMoveEvent : function(e)
    {
        if (!e)
        {
            e = window.event;
        }

        if (this._activePopup)
        {
            this._activePopup.endMove();
            this._activePopup = null;
        }
    },

    /**
     * Evénement
     *
     * @param event e Evénement
     */
    _moveEvent : function(e)
    {
        if (this._activePopup)
        {
            if (!e)
            {
                e = window.event;
            }

            this._activePopup.setPosition(this._startPosWindow.x + e.clientX - this._startEventPos.x,
                                          this._startPosWindow.y + e.clientY - this._startEventPos.y);
            Location.commonObj.stopEventBubble(e);
        }
    },

    /**
     * Afficher les masks sur les iframe (Pour Firefox)
     *
     * @param bool isShown Afficher ou cacher ?
     */
    _displayMasks : function(isShown)
    {
        for (var popupName in this.popupsList)
        {
            popup = this.popupsList[popupName];
            if (popup._moveMaskEl)
            {
                popup._moveMaskEl.style.display = (isShown ? 'block' : 'none');
            }
        }
    },

    _checkForegroundPopup : function()
    {
        var popupName, fgPopupName = '';
        var maxZIndex = -1, zIndex;
        for (popupName in this.popupsList)
        {
            zIndex = this.popupsList[popupName].isVisible();
            if (zIndex)
            {
                if (maxZIndex < zIndex)
                {
                    maxZIndex = zIndex;
                    fgPopupName = popupName;
                }
            }
        }

        if (fgPopupName != '')
        {
            for (popupName in this.popupsList)
            {
                zIndex = this.popupsList[popupName].isVisible();
                if (zIndex)
                {
                    this.popupsList[popupName]._el.className = 'locPlgPopupsWindow' + (popupName != fgPopupName ? ' blur' : '');
                }
            }
        }
    },

    /**
     * Créer une popup
     *
     * @param string name        Nom de la popup
     * @param string title       Titre de la popup
     * @param int    left        Position de gauche
     * @param int    top         Position du haut
     * @param int    width       Largeur
     * @param int    height      Hauteur
     * @param int    tabOptions  Options
     * @return Location.Popup Instance de la popup crée
     */
    createPopup : function(name, title, left, top, width, height, tabOptions)
    {
        if (this.popupsList[name])
        {
            return this.popupsList[name];
        }
        if (!this._isCreated)
        {
            var mySelf = this;
            Location.commonObj.addEltEvent(window.document.documentElement, 'mousemove', function(e){ mySelf._moveEvent(e); });
            Location.commonObj.addEltEvent(window.document.documentElement, 'mouseup', function(e){ mySelf._endMoveEvent(e); });
            this._isCreated = true;
        }

        tabOptions = (tabOptions ? tabOptions : {});
        tabOptions['createIndex'] = this._nbPopups;
        this.popupsList[name] = new Location.Popup(this, name, title, left, top, width, height, tabOptions);
        this._nbPopups++;
        return this.popupsList[name];
    },

    /**
     * Récupérer l'instance d'une popup
     *
     * @param string name Nom de la popup
     * @return Location.Popup
     */
    getPopup : function(name)
    {
        if (!this.popupsList[name])
        {
            return null;
        }
        return this.popupsList[name];
    },

    /**
     * Récupérer l'élément DIV du conteneur
     *
     * @return object L'élément
     */
    getContainerEl : function()
    {
        var mySelf = this;

        if (!this._containerEl)
        {
            this._containerEl = window.document.body;
        }
        else
        {
            this._containerEl = Location.commonObj.getElt(this._containerEl);
        }

        if (Location.clientInfoObj.isMSIE)
        {
            Location.commonObj.addEltEvent(this._containerEl, 'resize', function(e){ mySelf.onResizeFunc(e); });
        }

        return this._containerEl;
    },

    /**
     * Récupérer le nombre de popups
     *
     * @return int
     */
    getPopupsCount : function()
    {
        return this._nbPopups;
    },

    /**
     * Evénement de redimentionnement du conteneur
     *
     * @param event e Evénement
     */
    onResizeFunc : function(e)
    {
        for (var i = 0; i < this.popupsList.length; i++)
        {
            this.popupsList[i].checkPos();
        }
    }
}
