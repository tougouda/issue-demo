if (!window.Location)
	var Location = new Object();

if (Location.viewObj)
{

Location.viewObj._myCalendar = {name: '', yearMin: false, yearMax: false, pattern: ''};

/* Surcharge de l'objet Location.viewObj */

Location.viewObj.initCalendar = function(name, currentDate, yearMin, yearMax, pattern)
{
    if (window.dialogArguments)
    {
        var myArguments = window.dialogArguments;
        if (!myArguments || !myArguments.pluginObj)
        {
           
            window.close();
            return;
        }
    }
    
    this._myCalendar.name     = name;
    this._myCalendar.yearMin  = yearMin;
    this._myCalendar.yearMax  = yearMax;
    this._myCalendar.pattern  = pattern;

    var mySelf = this;
    this._myCalendar.instance = Calendar.setup({
        cont: "calendarContent",
        weekNumbers: true,
        min: parseInt(yearMin + '0101'),
        max: parseInt(yearMax + '1231'),
        date: parseInt(currentDate),
        selectionType: Calendar.SEL_SINGLE,
        showTime: false,
        onSelect      : function()
        {
            var date = this.selection.get();
            date = Calendar.intToDate(date);
            mySelf.selectDay(Calendar.printDate(date, '%d'),
                             Calendar.printDate(date, '%m'),
                             Calendar.printDate(date, '%Y'));
        }
    });
    this._myCalendar.instance.focus();
}

Location.viewObj.selectDay = function(d, m, y)
{
    Location.CalendarManager.setCalendar(this._myCalendar.name, d, m, y, this._myCalendar.pattern);
    window.close();
}

}
