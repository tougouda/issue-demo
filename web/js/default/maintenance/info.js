var NBMAXERRORS = 3;

var info = {

    _httpObj : null,

    httpRequestUrl : null,
    period : 0,
    nbErrors: 0,

    /**
     * Initialise l'objet.
     * @param string httpRequestUrl - L'URL de vérification du mode maintenance.
     * @param int period            - La période (en secondes) de vérification du mode maintenance.
     */
    init : function(httpRequestUrl, period)
    {
        this.httpRequestUrl = httpRequestUrl;
        this.period         = period * 1000;

        this.planCheck();
    },

    /**
     * Planifie la prochaine vérification du mode maintenance.
     */
    planCheck : function()
    {
        var mySelf = this;
        setTimeout(function() { mySelf.checkMaintenance(); }, this.period);
    },

    /**
     * Vérifie si le mode maintenance est toujours activé ou non.
     */
    checkMaintenance : function()
    {
        var mySelf = this;

        // Initialisation de l'objet HTTPRequest
        this._httpObj = new Location.HTTPRequest(this.httpRequestUrl);
        this._httpObj.oncompletion = function() { mySelf.checkMaintenanceCompletion(); };
        this._httpObj.onerror = function() { mySelf.checkMaintenanceError(); };

        // Envoi de la requête
        this._httpObj.send();
    },

    /**
     * La fonction exécutée lors de la réponse à l'appel en Ajax.
     */
    checkMaintenanceCompletion : function()
    {
        // Réponse JSON
        var tabResult = this._httpObj.getJSONResponse();

        // Réponse
        if (tabResult)
        {
            // Mode maintnance activé, on replanifie une vérification
            if (tabResult.isActivated)
            {
                this.planCheck();
            }
            //  Sinon on informe l'utilisateur
            else
            {
                this.displayMessage();
            }
        }
        // Erreur
        else
        {
            this.checkMaintenanceError();
        }
    },

    /**
     * La fonction exécutée en cas d'erreur sur l'appel Ajax.
     */
    checkMaintenanceError : function()
    {
        this.nbErrors++;

        if (this.nbErrors < NBMAXERRORS)
        {
            this.planCheck();
        }
        else
        {
            console.warn('Unable to check Gesloc maintenance status (' + this.nbErrors + ' unsuccessful attempts).');
        }
    },

    /**
     * Affiche le message de sortie du mode maintenance.
     */
    displayMessage : function()
    {
        LOC_Common.addEltClass($ge('info'), 'complete');
    }

};