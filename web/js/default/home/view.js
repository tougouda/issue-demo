var httpObjAlerts = null;
var httpObjMessages = null;
var httpObjMachine = null;


/**
 * Gestion de l'affichage des sous-menus
 */
var tabOpenMenus = [];
function menuClick(e, elt, nbMaxOpened)
{
    if (elt.className == 'menuItem arrow')
    {
        elt.className = 'menuItem arrowOpen';
        tabOpenMenus.push(elt);
        if (tabOpenMenus.length > nbMaxOpened)
        {
            var firstElt = tabOpenMenus.shift();
            firstElt.className = 'menuItem arrow';
        }
    }
    else if (elt.className == 'menuItem arrowOpen')
    {
        elt.className = 'menuItem arrow';
        var tabTmp = [];
        for (i = 0; i < tabOpenMenus.length; i++)
        {
            if (tabOpenMenus[i] != elt)
            {
                tabTmp.push(tabOpenMenus[i]);
            }
        }
        tabOpenMenus = tabTmp;
    }
}

/**
 * Affiche ou cache la fenêtre de changement de profil
 * @param isShown état de de la fenêtre
 */
function displayChangeProfile(isShown)
{
    var divElt = window.document.getElementById('homeChangeProfileWindow');
    var butElt = window.document.getElementById('homeChangeProfileButton');

    if (isShown)
    {
        butElt.style.visibility = 'hidden';
        divElt.style.display = 'block';
    }
    else
    {
        divElt.style.display = 'none';
        butElt.style.visibility = 'visible';
    }
}

/**
 * Lancement du raffraîchissement des alertes et des mails
 */
function refreshAlerts()
{
    var loading = LOC_Common.getElt('alertLoading');
    loading.style.display = 'block';

    var url = LOC_Common.getElt('urlAlerts').value;

    if (!httpObjAlerts)
    {
        httpObjAlerts = new Location.HTTPRequest(url);
        httpObjAlerts.oncompletion = function()
        {
            var tab = this.getJSONResponse();
            var alerts = tab['alerts'];
            var user = tab['user'];

            var alertContainer = LOC_Common.getElt('alertContainer');
            var loading = LOC_Common.getElt('alertLoading');
            var urlAlert = LOC_Common.getElt('urlAlert').value;

            // mise à jour des alertes
            var html = '';
            if (alerts)
            {
                var curGroup =  '';
                for (var i in alerts)
                {
                    var alt = alerts[i];
                    if (curGroup != alt['group.name'])
                    {
                        if (curGroup != '')
                        {
                            html += '</div></div></div>';
                        }
                        html += '<div class="alertBlock">';
                        html += '<div class="top">';
                        html += '<div class="title">' + alt['group.name'] + '</div>';
                        html += '</div>';
                        html += '<div class="content">';
                        html += '<div class="alerts">';
                    }

                    var className = '';
                    if (alt['count'] > 0)
                    {
                        className = 'alert hot';
                    }
                    else
                    {
                        className = 'alert';
                    }

                    html += '<a class="' + className + '" href="' + alt['url'] + '" target="_blank">';
                    html += '<span class="count">' + alt['count'] + '</span>';
                    html += '<span class="title">' + alt['label'] + '</span>';
                    html += '</a>';

                    curGroup = alt['group.name'];
                }
                html += '</div></div></div>';
            }

            // Mise à jour de l'agence nomade
            var elt = $ge('nomadAgency');
            if (elt)
            {
                elt.innerHTML = user['nomadAgency.label'] + ' [' + user['nomadAgency.id'] + ']';
            }
            elt = $ge('agencyId');
            if (elt)
            {
                elt.value = user['nomadAgency.id'];
            }

            loading.style.display = 'none';
            alertContainer.innerHTML = html;
        }
    }
    httpObjAlerts.send();
}

/**
 * Lancement du raffraîchissement des messages
 */
function refreshMessages()
{
    var loading = LOC_Common.getElt('newsLoading');
    loading.style.display = 'block';

    var url = LOC_Common.getElt('urlMessages').value;

    if (!httpObjMessages)
    {
        httpObjMessages = new Location.HTTPRequest(url);
        httpObjMessages.oncompletion = function()
        {
            var tab = this.getJSONResponse();

            var newsContainer = LOC_Common.getElt('newsContainer');
            var newsContent = LOC_Common.getElt('newsContent');
            var loading = LOC_Common.getElt('newsLoading');

            var html = '';
            if (tab)
            {
                for (var i in tab)
                {
                    var msg = tab[i];

                    var className = '';
                    if (msg['priority'] == 1)
                    {
                        className = ' breaking';
                    }
                    html += '<div class="title' + className + '">' + msg['subject'] + '</div>';
                    html += '<div class="date">' + msg['dateCreation'] + '</div>';
                    html += '<div class="message' + className + '">' + msg['content'] + '</div>';
                }
            }

            loading.style.display = 'none';
            newsContent.innerHTML = html;

            // affiche ou cache le bloc de message
            if (html != '')
            {
                newsContainer.style.display = 'block';
            }
            else
            {
                newsContainer.style.display = 'none';
            }
        };
    }

    httpObjMessages.send();
}

/**
 * Déclencher la recherche lors de l'appui sur la touche Entrée
 * @param event Événement
 */
function enterKey(event)
{
    if (event.keyCode == 13)
    {
        doSearch();
        return false;
    }
    return true;
}

/**
 * Ouvre l'interface de recherche correspondant au type d'élément recherché
 */
function doSearch()
{
    var url = '';
    if ($ge('customerName').value)
    {
        url = $ge('urlSrchCustomer').value + '?rech_client=' + $ge('customerName').value;
    }
    else if ($ge('city').value)
    {
        url = $ge('urlSrchCustomer').value + '?chant=' + $ge('city').value;
    }
    else if ($ge('parcNumber').value)
    {
        var urlMachine = $ge('urlSrchMachine').value + '&parkNumber=' + $ge('parcNumber').value;
        LOC_View.openWindow('machine' + $ge('parcNumber').value, urlMachine, true);
    }

    if (url != '')
    {
        LOC_View.openWindow('searchResults', url, true);
    }
}

/**
 * Vide les champs de recherche
 */
function clearSearch()
{
    $ge('customerName').value = '';
    $ge('city').value = '';
    $ge('parcNumber').value = '';
}



Location.viewObj.tabInitFuncs.push(function()
{
    // Redimentionnement
    var setHeightFunc = function()
    {
        var footerElt = window.document.getElementById('homeFooter');
        var middleElt = window.document.getElementById('homeMiddle');
        var windowHeight = Location.commonObj.getWindowHeight();

        if (windowHeight > 0 && middleElt)
        {
            var contentElt = window.document.getElementById('homeContent');

            var newH = windowHeight - Location.commonObj.getTopPos(middleElt);
            if (footerElt)
            {
                newH -= Location.commonObj.getHeight(footerElt);
            }
            if (newH < 0)
            {
                newH = 0;
            }
            middleElt.style.height = newH + 'px';

            // Hauteur du contenu sans les marges et les paddings
            var contentHeight  = newH;
            contentHeight -= parseInt(Location.commonObj.getStyle(contentElt, 'marginTop'));
            contentHeight -= parseInt(Location.commonObj.getStyle(contentElt, 'marginBottom'));
            contentHeight -= parseInt(Location.commonObj.getStyle(contentElt, 'paddingTop'));
            contentHeight -= parseInt(Location.commonObj.getStyle(contentElt, 'paddingBottom'));
            contentElt.style.height = contentHeight + 'px';
        }
    }

    Location.commonObj.addEltEvent(window, 'resize', setHeightFunc);
    setHeightFunc();

    // Désactivation de la sélection sur les éléments suivant :
    Location.commonObj.cancelTextSelection('homeMenu');
    Location.commonObj.cancelTextSelection('homeFooter');
});

/**
 * Initialisation de la page
 *
 * @param tabOptions
 */
function pageInit(tabOptions)
{
    window.setInterval(function(){ refreshAlerts(); }, tabOptions.refreshAlertsDelay);
    window.setInterval(function(){ refreshMessages(); }, tabOptions.refreshMessagesDelay);
}
