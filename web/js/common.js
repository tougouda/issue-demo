if (!window.Location)
{
    var Location = new Object();
}

if (!Location.commonObj)
{
// Polyfills pour IE
if (!String.prototype.trim) {
  String.prototype.trim = function () {
    return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
  };
}

if (!Array.prototype.find) {
  Object.defineProperty(Array.prototype, "find", {
    value: function(predicate) {
     'use strict';
     if (this == null) {
       throw new TypeError('Array.prototype.find called on null or undefined');
     }
     if (typeof predicate !== 'function') {
       throw new TypeError('predicate must be a function');
     }
     var list = Object(this);
     var length = list.length >>> 0;
     var thisArg = arguments[1];
     var value;

     for (var i = 0; i < length; i++) {
       value = list[i];
       if (predicate.call(thisArg, value, i, list)) {
         return value;
       }
     }
     return undefined;
    }
  });
}

if (!Array.prototype.findIndex) {
  Object.defineProperty(Array.prototype, 'findIndex', {
    value: function(predicate) {
      'use strict';
      if (this == null) {
        throw new TypeError('Array.prototype.findIndex called on null or undefined');
      }
      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      }
      var list = Object(this);
      var length = list.length >>> 0;
      var thisArg = arguments[1];
      var value;

      for (var i = 0; i < length; i++) {
        value = list[i];
        if (predicate.call(thisArg, value, i, list)) {
          return i;
        }
      }
      return -1;
    },
    enumerable: false,
    configurable: false,
    writable: false
  });
}

// Créer une nouvelle instance d'Array à partir d'un objet itérable ou semblable à un tableau
if (!Array.from) {
    Array.from = (function () {
        var toStr = Object.prototype.toString;
        var isCallable = function (fn) {
            return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
        };
        var toInteger = function (value) {
            var number = Number(value);
            if (isNaN(number)) { return 0; }
            if (number === 0 || !isFinite(number)) { return number; }
            return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
        };
        var maxSafeInteger = Math.pow(2, 53) - 1;
        var toLength = function (value) {
            var len = toInteger(value);
            return Math.min(Math.max(len, 0), maxSafeInteger);
        };

        // La propriété length de la méthode vaut 1.
        return function from(arrayLike/*, mapFn, thisArg */) {
            // 1. Soit C, la valeur this
            var C = this;

            // 2. Soit items le ToObject(arrayLike).
            var items = Object(arrayLike);

            // 3. ReturnIfAbrupt(items).
            if (arrayLike == null) {
                throw new TypeError("Array.from doit utiliser un objet semblable à un tableau - null ou undefined ne peuvent pas être utilisés");
            }

            // 4. Si mapfn est undefined, le mapping sera false.
            var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
            var T;
            if (typeof mapFn !== 'undefined') {
                // 5. sinon
                // 5. a. si IsCallable(mapfn) est false, on lève une TypeError.
                if (!isCallable(mapFn)) {
                    throw new TypeError('Array.from: lorsqu il est utilisé le deuxième argument doit être une fonction');
                }

                // 5. b. si thisArg a été fourni, T sera thisArg ; sinon T sera undefined.
                if (arguments.length > 2) {
                    T = arguments[2];
                }
            }

            // 10. Soit lenValue pour Get(items, "length").
            // 11. Soit len pour ToLength(lenValue).
            var len = toLength(items.length);

            // 13. Si IsConstructor(C) vaut true, alors
            // 13. a. Soit A le résultat de l'appel à la méthode interne [[Construct]] avec une liste en argument qui contient l'élément len.
            // 14. a. Sinon, soit A le résultat de ArrayCreate(len).
            var A = isCallable(C) ? Object(new C(len)) : new Array(len);

            // 16. Soit k égal à 0.
            var k = 0;  // 17. On répète tant que k < len…
            var kValue;
            while (k < len) {
                kValue = items[k];
                if (mapFn) {
                    A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
                } else {
                    A[k] = kValue;
                }
                k += 1;
            }
            // 18. Soit putStatus égal à Put(A, "length", len, true).
            A.length = len;  // 20. On renvoie A.
            return A;
        };
    }());
}


if (!String.toHTMLEntities)
{
    // Version simplifié de htmlentities() de PHP
    String.prototype.toHTMLEntities = function()
    {
        // http://www.commentcamarche.net/html/htmlcarac.php3

        var str = this;
        str = str.replace(/&/g, '&amp;'); // 38 // Doit-être en première position !

        str = str.replace(/\"/g, '&quot;'); // 34
        str = str.replace(/\'/g, '&#39;'); // 39
        str = str.replace(/</g, '&lt;'); // 60
        str = str.replace(/>/g, '&gt;'); // 62
        str = str.replace(/\^/g, '&circ;'); // 94
        return str;
    }
}

/* Promises Polyfill */
(function (root) {

  // Store setTimeout reference so promise-polyfill will be unaffected by
  // other code modifying setTimeout (like sinon.useFakeTimers())
  var setTimeoutFunc = setTimeout;

  function noop() {}

  // Polyfill for Function.prototype.bind
  function bind(fn, thisArg) {
    return function () {
      fn.apply(thisArg, arguments);
    };
  }

  function Promise(fn) {
    if (typeof this !== 'object') throw new TypeError('Promises must be constructed via new');
    if (typeof fn !== 'function') throw new TypeError('not a function');
    this._state = 0;
    this._handled = false;
    this._value = undefined;
    this._deferreds = [];

    doResolve(fn, this);
  }

  function handle(self, deferred) {
    while (self._state === 3) {
      self = self._value;
    }
    if (self._state === 0) {
      self._deferreds.push(deferred);
      return;
    }
    self._handled = true;
    Promise._immediateFn(function () {
      var cb = self._state === 1 ? deferred.onFulfilled : deferred.onRejected;
      if (cb === null) {
        (self._state === 1 ? resolve : reject)(deferred.promise, self._value);
        return;
      }
      var ret;
      try {
        ret = cb(self._value);
      } catch (e) {
        reject(deferred.promise, e);
        return;
      }
      resolve(deferred.promise, ret);
    });
  }

  function resolve(self, newValue) {
    try {
      // Promise Resolution Procedure: https://github.com/promises-aplus/promises-spec#the-promise-resolution-procedure
      if (newValue === self) throw new TypeError('A promise cannot be resolved with itself.');
      if (newValue && (typeof newValue === 'object' || typeof newValue === 'function')) {
        var then = newValue.then;
        if (newValue instanceof Promise) {
          self._state = 3;
          self._value = newValue;
          finale(self);
          return;
        } else if (typeof then === 'function') {
          doResolve(bind(then, newValue), self);
          return;
        }
      }
      self._state = 1;
      self._value = newValue;
      finale(self);
    } catch (e) {
      reject(self, e);
    }
  }

  function reject(self, newValue) {
    self._state = 2;
    self._value = newValue;
    finale(self);
  }

  function finale(self) {
    if (self._state === 2 && self._deferreds.length === 0) {
      Promise._immediateFn(function() {
        if (!self._handled) {
          Promise._unhandledRejectionFn(self._value);
        }
      });
    }

    for (var i = 0, len = self._deferreds.length; i < len; i++) {
      handle(self, self._deferreds[i]);
    }
    self._deferreds = null;
  }

  function Handler(onFulfilled, onRejected, promise) {
    this.onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : null;
    this.onRejected = typeof onRejected === 'function' ? onRejected : null;
    this.promise = promise;
  }

  /**
   * Take a potentially misbehaving resolver function and make sure
   * onFulfilled and onRejected are only called once.
   *
   * Makes no guarantees about asynchrony.
   */
  function doResolve(fn, self) {
    var done = false;
    try {
      fn(function (value) {
        if (done) return;
        done = true;
        resolve(self, value);
      }, function (reason) {
        if (done) return;
        done = true;
        reject(self, reason);
      });
    } catch (ex) {
      if (done) return;
      done = true;
      reject(self, ex);
    }
  }

  Promise.prototype['catch'] = function (onRejected) {
    return this.then(null, onRejected);
  };

  Promise.prototype.then = function (onFulfilled, onRejected) {
    var prom = new (this.constructor)(noop);

    handle(this, new Handler(onFulfilled, onRejected, prom));
    return prom;
  };

  Promise.all = function (arr) {
    var args = Array.prototype.slice.call(arr);

    return new Promise(function (resolve, reject) {
      if (args.length === 0) return resolve([]);
      var remaining = args.length;

      function res(i, val) {
        try {
          if (val && (typeof val === 'object' || typeof val === 'function')) {
            var then = val.then;
            if (typeof then === 'function') {
              then.call(val, function (val) {
                res(i, val);
              }, reject);
              return;
            }
          }
          args[i] = val;
          if (--remaining === 0) {
            resolve(args);
          }
        } catch (ex) {
          reject(ex);
        }
      }

      for (var i = 0; i < args.length; i++) {
        res(i, args[i]);
      }
    });
  };

  Promise.resolve = function (value) {
    if (value && typeof value === 'object' && value.constructor === Promise) {
      return value;
    }

    return new Promise(function (resolve) {
      resolve(value);
    });
  };

  Promise.reject = function (value) {
    return new Promise(function (resolve, reject) {
      reject(value);
    });
  };

  Promise.race = function (values) {
    return new Promise(function (resolve, reject) {
      for (var i = 0, len = values.length; i < len; i++) {
        values[i].then(resolve, reject);
      }
    });
  };

  // Use polyfill for setImmediate for performance gains
  Promise._immediateFn = (typeof setImmediate === 'function' && function (fn) { setImmediate(fn); }) ||
    function (fn) {
      setTimeoutFunc(fn, 0);
    };

  Promise._unhandledRejectionFn = function _unhandledRejectionFn(err) {
    if (typeof console !== 'undefined' && console) {
      console.warn('Possible Unhandled Promise Rejection:', err); // eslint-disable-line no-console
    }
  };

  /**
   * Set the immediate function to execute callbacks
   * @param fn {function} Function to execute
   * @deprecated
   */
  Promise._setImmediateFn = function _setImmediateFn(fn) {
    Promise._immediateFn = fn;
  };

  /**
   * Change the function to execute on unhandled rejection
   * @param {function} fn Function to execute on unhandled rejection
   * @deprecated
   */
  Promise._setUnhandledRejectionFn = function _setUnhandledRejectionFn(fn) {
    Promise._unhandledRejectionFn = fn;
  };

  if (typeof module !== 'undefined' && module.exports) {
    module.exports = Promise;
  } else if (!root.Promise) {
    root.Promise = Promise;
  }

})(this);


/* Fonctions sur les tableaux */
function array_search(elt, tab)
{
    var i = 0, nb = tab.length;
    while (i < nb && tab[i] != elt)
    {
        i++;
    };
    return (i < nb ? i : false);
}

function in_array(elt, tab)
{
    return (array_search(elt, tab) !== false);
}

function array_push_unique(tab, elt)
{
    var exists = in_array(elt, tab);
    if (!exists)
    {
        tab.push(elt);
    }
    return !exists;
}

function capitalizeFirstLetter(text)
{
    return text.charAt(0).toUpperCase() + text.slice(1);
}

/**
 * Calcul l'intersection entre 2 tableaux
 *
 * @param Array tab1
 * @param Array tab2
 * @returns Array
 */
function array_intersect(tab1, tab2)
{
    var tab = [];
    var n1 = tab1.length;
    var n2 = tab2.length;
    for (var i = 0; i < n1; i++)
    {
        var j = 0;
        while (j < n2 && tab1[i] != tab2[j])
        {
            j++;
        }
        if (j < n2)
        {
            tab.push(tab1[i]);
        }
    }
    return tab;
}



/* Classe Location.clientInfoObj */
Location.clientInfoObj = {

    isFirefox : (navigator.userAgent.toLowerCase().indexOf('firefox') >= 0) ? true : false,
    isMSIE : (navigator.userAgent.toLowerCase().indexOf('msie') >= 0) ? true : false,
    navigatorVersion : navigator.appVersion.replace(/.*?MSIE\s(\d\.\d).*/g, '$1')/1,

    /**
     * Récupérer la largeur du navigateur
     *
     * @return int Largeur du navigateur
     */
    getBrowserWidth : function()
    {
        if (self.innerWidth)
        {
            return self.innerWidth;
        }
        return window.document.documentElement.offsetWidth;
    },

    /**
     * Récupérer la hauteur du navigateur
     *
     * @return int Hauteur du navigateur
     */
    getBrowserHeight : function()
    {
        if (self.innerHeight)
        {
            return self.innerHeight;
        }
        return window.document.documentElement.offsetHeight;
    }
}

/* Raccourci Objet */
LOC_ClientInfo = Location.clientInfoObj;

/* Object Location.commonObj */
Location.commonObj = {

    /**
     * Récupérer l'instance d'un élément HTML
     *
     * @param mixed elRef Identifiant de l'élément
     * @return Object Référence de l'élément
     */
    getElt : function(elRef)
    {
        if (typeof elRef == 'string')
        {
            if (window.document.getElementById(elRef))
            {
                return window.document.getElementById(elRef);
            }
            if (window.document.getElementsByName(elRef)[0])
            {
                return window.document.getElementsByName(elRef)[0];
            }
            if (window.document.forms[elRef])
            {
                return window.document.forms[elRef];
            }
            if (window.document[elRef])
            {
                return window.document[elRef];
            }
            if (window[elRef])
            {
                return window[elRef];
            }
            return false;
        }
        return elRef;
    },

    /**
     * Récupérer le style d'un élément
     *
     * @param mixed  el   L'élément
     * @param string prop Propriété à retourner
     * @return string Style de l'élément
     */
    getStyle : function(el, prop)
    {
        el = this.getElt(el);
        if (el.style[prop])
        {
            return el.style[prop];
        }
        else
        {
            if (window.document.defaultView && window.getComputedStyle)
            {
                var comp = window.getComputedStyle(el, '');
                if (comp)
                {
                    return comp[prop];
                }
            }
            else if (window.document.documentElement.currentStyle && el.currentStyle[prop])
            {
                return el.currentStyle[prop];
            }
        }
        return null;
    },

    /**
     * Récupérer la coordonnée X d'un élément HTML
     *
     * @param mixed el L'élément
     * @return int Coordonnée X de l'élément
     */
    getLeftPos : function(el)
    {
        el = this.getElt(el);
        if (el.getBoundingClientRect)
        {
            return el.getBoundingClientRect().left;
        }

        var returnValue = el.offsetLeft;
        while((el = el.offsetParent) != null)
        {
            if (el.tagName != 'HTML')
            {
                returnValue += el.offsetLeft;
                if (window.document.all)
                {
                    returnValue += el.clientLeft;
                }
            }
        }
        return returnValue
    },

    /**
     * Récupérer la coordonnée Y d'un élément HTML
     *
     * @param mixed el L'élément
     * @return int Coordonnée Y de l'élément
     */
    getTopPos : function(el)
    {
        el = this.getElt(el);
        if (el.getBoundingClientRect)
        {
            return el.getBoundingClientRect().top;
        }

        var returnValue = el.offsetTop;
        while((el = el.offsetParent) != null)
        {
            if (el.tagName != 'HTML')
            {
                returnValue += (el.offsetTop - el.scrollTop);
                if (window.document.all)
                {
                    returnValue += el.clientTop;
                }
            }
        }
        return returnValue
    },

    /**
     * Récupérer la largeur d'un élément
     *
     * @param mixed el L'élément
     * @return int Largeur de l'élément
     */
    getWidth : function(el)
    {
        el = this.getElt(el);
        return el.offsetWidth;
    },

    /**
     * Récupérer la hauteur d'un élément
     *
     * @param mixed el L'élément
     * @return int Hauteur de l'élément
     */
    getHeight : function(el)
    {
        el = this.getElt(el);
        return el.offsetHeight;
    },

    /**
     * Récupérer la hauteur de la fenêtre
     *
     * @return int Hauteur de la fenêtre
     */
    getWindowHeight : function()
    {
        var windowHeight = 0;
        if (typeof(window.innerHeight) == 'number') windowHeight = window.innerHeight;
        else
        {
            if (window.document.documentElement && window.document.documentElement.clientHeight)
            {
                windowHeight = window.document.documentElement.clientHeight;
            }
            else if (window.document.body && window.document.body.clientHeight)
            {
                windowHeight = window.document.body.clientHeight;
            }
        }
        return windowHeight;
    },

    /**
     * Retourne TRUE si la variable passée en paramètre est un Array, sinon FALSE
     *
     * @param mixed v Variable à tester
     */
    isArray : function(v)
    {
        return (v.constructor.toString().indexOf('Array') != -1);
    },

    /**
     * Récupérer l'élément qui a déclenché l'évènement
     *
     * @param Event e Evénement
     * @return Object Référence de l'objet qui a déclenché l'évènement
     */
    getEventSrcEl : function(e)
    {
        var el;
        if (e.target)
        {
            el = e.target;
        }
        else if (e.srcElement)
        {
            el = e.srcElement;
        }
        if (el.nodeType == 3)
        {
            el = el.parentNode;
        }
        return el;
    },

    /**
     * Récupérer le code de la touche pressée
     *
     * @param Event e Evénement
     * @return int Code de la touche pressée
     */
    getEventKeyCode : function(e)
    {
        return (LOC_ClientInfo.isMSIE ? e.keyCode : e.which);
    },

    /**
     * Récupérer le caractère de la touche pressée
     *
     * @param Event e Evénement
     * @return string Caractère de la touche pressée
     */
    getEventKeyChar : function(e)
    {
        var code = this.getEventKeyCode(e);
        return String.fromCharCode(code);
    },

    /**
     * Ajouter un évènement sur un élément
     *
     * @param mixed    el       L'élément
     * @param string   type     Type d'évènement ('click', 'mousedown', 'mouseover', 'mouseout'...)
     * @param callback listener Fonction à exécuter au déclenchement de cet évènement
     */
    addEltEvent : function(el, type, listener)
    {
        el = this.getElt(el);
        // Firefox
        if (el.addEventListener)
        {
            if (type == 'propertychange')
            {
                type = 'DOMAttrModified';
            }
            el.addEventListener(type, listener, false);
        }
        // IE
        else if (el.attachEvent)
        {
            el.attachEvent('on' + type, listener);
        }
    },

    /**
     * Supprimer un évènement sur un élément
     *
     * @param mixed    el       L'élément
     * @param string   type     Type d'évènement ('click', 'mousedown', 'mouseover', 'mouseout'...)
     * @param callback listener Fonction qui était exécutée au déclenchement de cet évènement
     */
    removeEltEvent : function(el, type, listener)
    {
        el = this.getElt(el);
        if (el.removeEventListener)
        {
            el.removeEventListener(type, listener, false);
        }
        else if (el.detachEvent)
        {
            el.detachEvent('on' + type, listener);
        }
    },

    /**
     * Indique si l'élément contient une classe
     *
     * @param mixed  el  L'élément
     * @param string cls Nom de la classe
     * @return bool
     */
    hasEltClass : function(el, cls)
    {
        el = this.getElt(el);
        el.className.replace(/^\s*/, '').replace(/\s*$/, '').replace(/\s\s+/g, ' ');
        return ((' ' + el.className + ' ').indexOf(' ' + cls + ' ') != -1);
    },

    /**
     * Ajoute une classe à l'élément si il ne l'a pas
     *
     * @param mixed  el  L'élément
     * @param string cls Nom de la classe
     * @return bool
     */
    addEltClass : function(el, cls)
    {
        el = this.getElt(el);
        if (!this.hasEltClass(el, cls))
        {
            el.className += ' ' + cls;
            return true;
        }
        return false;
    },

    /**
     * Supprime une classe de l'élément si il l'a
     *
     * @param mixed  el  L'élément
     * @param string cls Nom de la classe
     * @return bool
     */
    removeEltClass : function(el, cls)
    {
        el = this.getElt(el);
        if (this.hasEltClass(el, cls))
        {
            var reg = new RegExp('(^|\\s)' + cls + '(\\s|$)', 'g');
            el.className = el.className.replace(reg, ' ');
            return true;
        }
        return false;
    },

    /**
     * Ajoute ou supprime une classe de l'élément
     *
     * @param mixed  el    L'élément
     * @param string cls   Nom de la classe
     * @param bool   state Indique si la classe doit être ajoutée ou supprimée
     * @return int
     */
    toggleEltClass : function(el, cls, state)
    {
        el = this.getElt(el);
        var state = (typeof state != 'undefined' ? state : !this.hasEltClass(el, cls));
        if (state)
        {
            return (this.addEltClass(el, cls) * 1);
        }
        else
        {
            return -(this.removeEltClass(el, cls) * 1);
        }
    },

    /**
     * Arrêter la propagation d'un évènement sur les éléments parent
     *
     * @param Event e Evénement
     */
    stopEventBubble : function(e)
    {
        if (window.document.all && typeof e == 'undefined')
        {
            e = event;
        }

        if (e.stopPropagation)
        {
            e.stopPropagation();
        }
        else
        {
            e.cancelBubble = true;
        }
    },

    /**
     * Cloner une variable
     *
     * @param mixed srcInstance
     * @return mixed
     */
    clone : function(srcInstance)
    {
        // Si l'instance source n'est pas un objet ou qu'elle ne vaut rien c'est une feuille donc on la retourne
        if (typeof(srcInstance) != 'object' || srcInstance == null)
        {
            return srcInstance;
        }
        // On appel le constructeur de l'instance source pour crée une nouvelle instance de la même classe
        var newInstance = srcInstance.constructor();
        // On parcourt les propriétés de l'objet et on les recopies dans la nouvelle instance
        for (var i in srcInstance)
        {
            newInstance[i] = this.clone(srcInstance[i]);
        }
        // On retourne la nouvelle instance
        return newInstance;
    },

    /**
     * Annuler un évènement
     *
     * @param Event e Evénement
     */
    cancelEvent : function(e)
    {
        if (e.preventDefault)
        {
            e.preventDefault();
        }
        else
        {
            e.returnValue = false;
            if (e.keyCode)
            {
                e.keyCode = 0;
            }
        }
    },

    /**
     * Provoquer un évènement sur un élément
     *
     * @param mixed  el   Eléments
     * @param string type Type d'évènement à provoquer
     * @return bool
     */
    fireEvent : function(el, type)
    {
        el = this.getElt(el);

        if (window.document.createEventObject)
        {
            var evt = window.document.createEventObject();
            return el.fireEvent('on' + type, evt)
        }
        else
        {
            var evt = window.document.createEvent('HTMLEvents');
            evt.initEvent(type, true, true);
            return !el.dispatchEvent(evt);
        }
    },

    /**
     * Annuler la sélection du texte sur un élément
     *
     * @param mixed el L'élément
     */
    cancelTextSelection : function(el)
    {
        var myself = this;
        el = this.getElt(el);
        if (el)
        {
            if (typeof el.onselectstart != 'undefined')
            {
                el.onselectstart = function() { return false; };
            }
            else if (typeof el.style.MozUserSelect != 'undefined')
            {
                el.style.MozUserSelect = 'none';
            }
            else
            {
                el.onmousedown = function() { return false; };
            }
        }
    },

    /**
     * Lire le contenu d'un cookie
     *
     * @param string name Nom du cookie à lire
     */
    getCookie : function(name)
    {
        var start = window.document.cookie.indexOf(name + '=');
        var len = start + name.length + 1;
        if ((!start) && (name != window.document.cookie.substring(0, name.length)))
        {
            return null;
        }
        if (start == -1)
        {
            return null;
        }
        var end = window.document.cookie.indexOf(';', len);
        if (end == -1)
        {
            end = window.document.cookie.length;
        }
        return unescape(window.document.cookie.substring(len, end));
    },

    /**
     * Ecrire dans un cookie
     *
     * @param string name    Nom du cookie à supprimer
     * @param string value   Valeur à écrire
     * @param string expires Expiration du cookie
     * @param string path    Répertoire
     * @param string domain  Domaine
     * @param string secure  Sécurité
     */
    setCookie : function(name, value, expires, path, domain, secure)
    {
        expires = expires * 60 * 60 * 24 * 1000;
        var today = new Date();
        var expires_date = new Date(today.getTime() + (expires));
        var cookieString = name + '=' + escape(value) +
            ((expires) ? ';expires=' + expires_date.toGMTString() : '') +
            ((path) ? ';path=' + path : '') +
            ((domain) ? ';domain=' + domain : '') +
            ((secure) ? ';secure' : '');
        window.document.cookie = cookieString;
    },

    /**
     * Supprimer un cookie
     *
     * @param string name Nom du cookie à supprimer
     */
    deleteCookie : function(name)
    {
        var expdate = new Date();
        expdate.setTime(expdate.getTime() - (86400 * 1000 * 1));
        this.setCookie(name, '', expdate);
    },

    /**
     * Récupérer l'objet HTTPRequest
     *
     * @return XMLHttpRequest Instance de XMLHttpRequest
     */
    getHTTPObject : function()
    {
        if (window.XMLHttpRequest)
        {
            return new XMLHttpRequest();
        }
        if (window.ActiveXObject)
        {
            var names = [
                'Msxml2.XMLHTTP.6.0',
                'Msxml2.XMLHTTP.3.0',
                'Msxml2.XMLHTTP',
                'Microsoft.XMLHTTP'
            ];
            for(var i in names)
            {
                try
                {
                    return new ActiveXObject(names[i]);
                }
                catch(e)
                {
                }
            }
        }
        return null;
    },

    /**
     * Positionner le curseur
     *
     * @param mixed el  Elément
     * @param int   pos Position
     */
    setCursorPos : function(el, pos)
    {
        if (pos == -1)
        {
            if (el.nodeName.toLowerCase() == 'textarea')
            {
                pos = el.innerHTML.length;
            }
            else if (el.nodeName.toLowerCase() == 'input')
            {
                pos = el.value.length;
            }
        }

        el.focus();
        if (typeof el.selectionStart != 'undefined')
        {
            el.setSelectionRange(pos, pos);
        }
        else
        {
            var txtRangeObj = el.createTextRange();
            txtRangeObj.moveStart('character', pos);
            txtRangeObj.collapse();
            txtRangeObj.select();
        }
    },

    /**
     * Récupérer la position du curseur
     *
     * @param mixed el  Elément
     * @return int Position
     */
    getCursorPos : function(el)
    {
        el.focus();
        if (typeof el.selectionStart != 'undefined')
        {
            return el.selectionStart;
        }
        else
        {
            var rangeObj = document.selection.createRange();
            if (!rangeObj)
            {
                return 0;
            }
            var txtRangeObj    = el.createTextRange();
            var txtRangeCpyObj = txtRangeObj.duplicate();
            txtRangeObj.moveToBookmark(rangeObj.getBookmark());
            txtRangeCpyObj.setEndPoint('EndToStart', txtRangeObj);

            return txtRangeCpyObj.text.length;
        }
    },

    /**
     * Arrondi
     *
     * @param double nb           Valeur
     * @param int    nbDecimals   Nombre de décimales
     * @param int    nbSrcMaxDecs Nombre de décimales maximum en entrée (facultatif) (pour les problèmes d'arrondis)
     * @return double
     */
    round : function(nb, nbDecimals, nbSrcMaxDecs)
    {
        if (typeof nbDecimals == 'undefined')
        {
            nbDecimals = 0;
        }
        nb = Number(nb);
        var neg = (nb >= 0 ? 1 : -1);
        if (nbSrcMaxDecs)
        {
            nb += Math.pow(10, -(nbSrcMaxDecs + 1)) * neg;
        }
        return (Math.round(Math.abs(nb) * Math.pow(10, nbDecimals)) / Math.pow(10, nbDecimals)) * neg;
    },

    /**
     * Récupérer l'instance de HTMLDocument d'un IFrame
     *
     * @param mixed frameElt Elément IFrame
     * @return HTMLDocument|false
     */
    getIFrameDocument : function(frameElt)
    {
        frameElt = Location.commonObj.getElt(frameElt);
        var tagName = frameElt.nodeName.toLowerCase();
        if (tagName == 'iframe')
        {
            try
            {
                var docElt = frameElt.contentWindow || frameElt.contentDocument;
                if (docElt.document)
                {
                    docElt = docElt.document;
                }
                return docElt;
            }
            catch (e)
            {
                return false;
            }
        }
        return false;
    },

    /**
     * Indique si l'adresse mail est valide ou non
     *
     * @param string email Adresse e-mail
     * @return bool
     */
    isValidMail : function(email)
    {
        var regExp = /^([a-zA-Z0-9._-]+)@([a-zA-Z0-9.-]+)\.[a-zA-Z]{2,4}$/;
        return regExp.test(email);
    },

    /**
     * Indique les 2 variables passées en paramètre sont identiques ou non
     * (Fonction récupérée de Angular 1: angular.equals(o1, o2))
     *
     * @param mixed o1 Variable 1
     * @param mixed o2 Variable 2
     * @return bool
     */
    isEqual : function(o1, o2)
    {
        if (o1 === o2)
        {
            return true;
        }
        if (o1 === null || o2 === null)
        {
            return false;
        }
        if (o1 !== o1 && o2 !== o2)
        {
            return true; // NaN === NaN
        }
        var t1 = typeof o1;
        var t2 = typeof o2;
        if (t1 === t2 && t1 === 'object')
        {
            var toString = Object.prototype.toString;
            if (Array.isArray(o1))
            {
                if (!Array.isArray(o2))
                {
                    return false;
                }
                var length = o1.length;
                if (length === o2.length)
                {
                    for (var key = 0; key < length; key++)
                    {
                        if (!this.isEqual(o1[key], o2[key]))
                        {
                            return false;
                        }
                    }
                    return true;
                }
            }
            else if (toString.call(o1) === '[object Date]')
            {
                return (toString.call(o2) === '[object Date]' ? this.isEqual(o1.getTime(), o2.getTime()) : false);
            }
            else if (toString.call(o1) === '[object RegExp]')
            {
                return (toString.call(o2) === '[object RegExp]' ? o1.toString() === o2.toString() : false);
            }
            else
            {
                var keySet = Object.create(null);
                for (var key in o1)
                {
                    if (typeof o1[key] !== 'function')
                    {
                        if (!this.isEqual(o1[key], o2[key]))
                        {
                            return false;
                        }
                        keySet[key] = true;
                    }
                }
                for (key in o2)
                {
                    if (!(key in keySet) &&
                        typeof o2[key] !== 'undefined' &&
                        typeof o2[key] !== 'function')
                    {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

}

/* Raccourci Objet */
var LOC_Common = Location.commonObj;

/**
 * Récupérer l'instance d'un élément HTML (Fonction raccourci)
 *
 * @param mixed elRef Référence de l'élément
 * @return Object Référence de l'élément
 */
function $(elRef)
{
    return Location.commonObj.getElt(elRef);
}
function $ge(elRef)
{
    return Location.commonObj.getElt(elRef);
}

} // IF EXIST : Location.Common
