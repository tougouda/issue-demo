if (!window.Location)
{
    var Location = new Object();
}

if (!Location.commonObj) alert('Erreur Location');


// Ajout des événements permettant d'afficher le menu des boutons déroulants
Location.commonObj.addEltEvent(window, 'load', function(e)
    {
        e = e || window.event;

        var tabElts = window.document.getElementsByClassName('dropdown-toggle-split');
        var nbElts = tabElts.length;

        for (var i = 0; i < nbElts; i++)
        {
            Location.commonObj.addEltEvent(tabElts[i], 'click', function(e)
                {
                    // Rien à faire si le bouton est désactivé
                    if (Location.commonObj.hasEltClass(this.parentElement, 'disabled'))
                    {
                        return;
                    }

                    LOC_Common.toggleEltClass(this.parentElement, 'show');
                });
        }
    });


// Masquage du menu de tous les boutons déroulants lorsqu'on clique n'importe où sur la page
Location.commonObj.addEltEvent(window.document, 'click', function(e)
    {
        e = e || window.event;

        var eventSrcElt = Location.commonObj.getEventSrcEl(e);

        // Rien à faire si on a cliqué sur le bouton d'affichage du menu du bouton déroulant
        if (Location.commonObj.hasEltClass(eventSrcElt, 'dropdown-toggle-split'))
        {
            return;
        }

        // Masquage du menu de tous les boutons déroulants
        var tabElts = window.document.getElementsByClassName('dropdown');
        var nbElts = tabElts.length;

        for (var i = 0; i < nbElts; i++)
        {
            Location.commonObj.removeEltClass(tabElts[i], 'show');
        }
    });
