var printerHttpObj;
var printHttpObj;

/**
 * Mise à jour des informations de l'imprimante sélectionnée
 *
 * @param int    id  Identifiant de l'imprimante
 * @param string url URL permettant de récupérer les informations
 */
function updatePrinterInfos(id, url)
{
    clearFields();

    if (!printerHttpObj)
    {
        printerHttpObj = new Location.HTTPRequest(url);
    }
    
    printerHttpObj.oncompletion = function()
    {
        var tabInfos = printerHttpObj.getJSONResponse();
        if (tabInfos)
        {
            $ge('printer.status').innerHTML   = LOC_View.getTranslation(tabInfos['status']);
            $ge('agency.label').innerHTML     = tabInfos['agency.label'];
            $ge('printer.location').innerHTML = tabInfos['location'];
            $ge('printer.label').value        = tabInfos['label'];
        }
        else
        {
            // Erreur
            this.onerror();
        }
    };
    printerHttpObj.onerror = function()
    {
        // Erreur
        clearFields();
    };
    
    // Passage de l'id du client en paramètre
    printerHttpObj.setVar('printerId', id);
    
    // Envoi de la requête HTTP
    printerHttpObj.send(true);
    
    return true;
}

/**
 * Vidage des champs d'informations
 */
function clearFields()
{
    $ge('printer.status').innerHTML   = '';
    $ge('agency.label').innerHTML     = '';
    $ge('printer.location').innerHTML = '';
}

/**
 * Envoi de l'impression du document
 *
 * @param string url     URL permettant de lancer l'impression
 * @param string printer Nom de l'imprimante
 * @param string file    Fichier à imprimer (sous la forme module:contrôleur:action)
 * @param int    number  Nombre de copies
 */
function printDocument(url, printer, file, number)
{
    document.body.style.cursor = 'progress';
    if (!number)
    {
        number = 1;
    }
    
    if (!printHttpObj)
    {
        printHttpObj = new Location.HTTPRequest(url);
        // Codes de retour valide :
        // 204 : pas de contenu (code de retour normal)
        // 1223 : code transformé par IE
        // 0 : code transformé par Google Chrome Frame
        printHttpObj.setValidResponseStatusRegExp('^0|204|1223$');
    }
    
    printHttpObj.oncompletion = function()
    {
        submitForm();
    };
    printHttpObj.onerror = function()
    {
        // Erreur
        alert(LOC_View.getTranslation('error').replace('%s', printHttpObj.getResponseStatus()));
        document.body.style.cursor = 'auto';
    };
    
    // Passage de l'id du client en paramètre
    printHttpObj.setVar('printer', printer);
    printHttpObj.setVar('url', file);
    printHttpObj.setVar('number', number);
    
    // Envoi de la requête HTTP
    printHttpObj.send(true);
    
    return true;
}

/**
 * Soumission du formulaire général
 */
function submitForm()
{
    var frm = document.getElementsByTagName("form")[0]
    if (frm)
    {
        $ge('valid').value = 'ok';
        frm.submit();
    }
    return false;
}
