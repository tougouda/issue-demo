var email = {

    _timeout: null,
    _searchUrl : null,
    _request : null,
    _previousText : [],
    separator : {char: ';', code: 190},
    searchDelay: 500,
    maxResultsCount: 8,
    tabCustomAttachments: {},
    _cpt: 0,

    /**
     * Gère les touches spéciales : Entrée.
     * @param Object e L’événement source.
     * @param HTMLInputElement input L'élément qui a déclenché la recherche.
     */
    _manageKeys : function(e, input)
    {
        var code = e.which || e.keyCode;

        // On empêche l'action par défaut pour la touche Entrée
        if (code == 13)
        {
            e.preventDefault();
        }

        // Échap ou séparateur défini
        if (code == 27 || code == this.separator.code)
        {
            this.hideSearchResultPanel(input.id);
        }
        // Entrée
        else if (code == 13)
        {
            $('#' + input.id + '_search li.selected').click();
        }
        // Flèche haut
        else if (code == 38)
        {
            var liSelectedElt = $('#' + input.id + '_search li.selected');
            var liPrevElt = liSelectedElt.prev();
            if (liPrevElt.length == 0)
            {
                liPrevElt = $('#' + input.id + '_search li').last();
            }

            if (!liPrevElt.is(liSelectedElt))
            {
                liPrevElt.addClass('selected');
                liSelectedElt.removeClass('selected');
            }
        }
        // Flèche bas
        else if (code == 40)
        {
            var liSelectedElt = $('#' + input.id + '_search li.selected');
            var liNextElt = liSelectedElt.next();
            if (liNextElt.length == 0)
            {
                liNextElt = $('#' + input.id + '_search li').first();
            }

            if (!liNextElt.is(liSelectedElt))
            {
                liNextElt.addClass('selected');
                liSelectedElt.removeClass('selected');
            }
        }
        else
        {
            this._delaySearch(input);
        }

        // Stockage de la nouvelle valeur du champ
        this._previousText[input.id] = input.value;
    },

    /**
     * Planifie le lancement de la recherche.
     * @param HTMLInputElement input L'élément qui a déclenché la recherche.
     */
    _delaySearch : function(input)
    {
        if (this._previousText[input.id] != input.value)
        {
            // Chaînes à exclure
            // (basé sur l'ensemble des adresses saisies, puis on enlève l'adresse en cours de saisie)
            var tabExcludedEmails = this._extractEmailsFromText(input.value);

            // Indices des éléments concernés
            var index = 0;
            var length = 0;
            for (var i = 0; i < tabExcludedEmails.length; i++)
            {
                if (i > 0)
                {
                    length += this.separator.char.length;
                }
                length += tabExcludedEmails[i].length;
                if (length >= input.selectionStart - 1)
                {
                    index = i;
                    break;
                }
            }

            // Chaîne de recherche
            var search = tabExcludedEmails.splice(index, 1).join().trim();

            // Annulation de la précédente recherche planifiée
            if (this._timeout != null)
            {
                window.clearTimeout(this._timeout);
            }

            // Événement à planifier
            var mySelf = this;
            var event;
            if (search == '')
            {
                // Masquage du panneau de résultat
                event = function() {
                    mySelf.hideSearchResultPanel(input.id)
                };
            }
            else
            {
                // Lancement de la recherche
                event =function() {
                    mySelf._search(search, tabExcludedEmails, function() {
                        mySelf._loadingResults(input.id);
                    }, function(response) {
                        if (response.errorMsgs)
                        {
                            mySelf._displayResultsError(input.id);
                        }
                        else
                        {
                            mySelf._resultReceived(search, index, response.result, input.id);
                        }
                    }, function(response) {
                        console.log(response);
                        mySelf._displayResultsError(input.id);
                    });
                };
            }

            // Planification de l'événement
            this._timeout = window.setTimeout(event, this.searchDelay);
        }
    },

    /**
     * Exécute la recherche.
     * @param string search Le texte recherché.
     * @param array exclude La liste des e-mails à exclure de la recherche.
     * @param function beforeCallback La fonction exécutée avant le lancement de la recherche.
     * @param function doneCallback La fonction exécutée à la réception des résultats de la recherche.
     * @param function failCallback La fonction exécutée en cas d'erreur à la réception des résultats de la recherche.
     */
    _search : function(search, exclude, beforeCallback, doneCallback, failCallback)
    {
        var mySelf = this;

        beforeCallback();

        this._request = $.ajax({
            url : this._searchUrl,
            method : 'post',
            dataType : 'json',
            data : JSON.stringify({
                search : search,
                exclude : exclude,
                maxResultsCount: this.maxResultsCount
            }),
            contentType: 'application/json',
        })
        .done(doneCallback)
        .fail(failCallback)
        .always(function() {
            mySelf._request = null;
        });
    },

    /**
     * Affiche le message d’attente du résultat de la recherche.
     * @param string type Le type de recherche.
     */
    _loadingResults : function(type)
    {
        // Masquage des résultats précédents
        var ulElt = $('#' + type + '_search ul');
        ulElt.hide();

        // Affichage du message d'attente
        var loadingElt = $('#' + type + '_search .loading');
        loadingElt.show();

        // Masquage du message d'erreur
        var errorElt = $('#' + type + '_search .error');
        errorElt.hide();

        // Affichage du panneau de résultat
        var resultObj = $('#' + type + '_search');
        resultObj.show();
    },

    /**
     * Affiche le message d’erreur lors de la recherche de contact.
     * @param string type Le type de recherche.
     */
    _displayResultsError : function(type)
    {
        // Masquage des résultats précédents
        var ulElt = $('#' + type + '_search ul');
        ulElt.hide();

        // Masquage du message d'attente
        var loadingElt = $('#' + type + '_search .loading');
        loadingElt.hide();

        // Affichage du message d'erreur
        var errorElt = $('#' + type + '_search .error');
        errorElt.show();

        // Affichage du panneau de résultat
        var resultObj = $('#' + type + '_search');
        resultObj.show();
    },

    /**
     * Met en évidence du texte dans un autre.
     * @param string text Le texte de base.
     * @param string highlight Le texte à mettre en évidence.
     * @return string Le texte de base avec le texte mis en évidence.
     */
    _highlight : function(text, highlight)
    {
        var tabComponents = highlight.split(' ');
        for (var i = 0; i < tabComponents.length; i++)
        {
            var regexp = new RegExp('(' + tabComponents[i].replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&") + ')', 'ig');
            text = text.replace(regexp, '<b>$1</b>');
        }
        return text;
    },

    /**
     * Extrait les adresses e-mail d'une chaîne de caractères en fonction du séparateur défini.
     * @param string text Le texte.
     * @return array La liste des adresses e-mails extraites.
     */
    _extractEmailsFromText : function(text)
    {
        // Suppression des séparateurs au début et à la fin
        var regexp = new RegExp('^' + this.separator.char + '+|' + this.separator.char + '+$', 'g');
        return text.replace(regexp, '').split(this.separator.char);
    },

    /**
     * Position le curseur dans une zone de texte.
     * @param object element La zone de texte.
     * @param int position La position du curseur.
     */
    _setCursorPosition : function(element, position)
    {
        element = element[0];
        if (element.setSelectionRange)
        {
            element.setSelectionRange(position, position);
        }
        else if (element.createTextRange)
        {
            var range = element.createTextRange();
            range.collapse(true);
            range.moveEnd('character', position);
            range.moveStart('character', position);
            range.select();
        }
    },

    /**
     * Traite le résultat de la recherche.
     * @param array tabEmails Le résultat de la recherche.
     * @param string type Le type de recherche.
     */
    _resultReceived : function(search, index, tabEmails, type)
    {
        var resultObj = $('#' + type + '_search');

        var nbEmails = tabEmails.length;
        if (nbEmails == 0)
        {
            // Masquage du panneau de résultat
            resultObj.hide();
        }
        else
        {
            var mySelf = this;

            // Vidage des résultats précédents
            var ulElt = $('#' + type + '_search ul');
            ulElt.empty();

            var textElt = $('#' + type);

            for (var i = 0; i < nbEmails && i < this.maxResultsCount; i++)
            {
                var liElt = $('<li>');
                liElt.attr('data-value', tabEmails[i].email);
                liElt.addClass('type');
                liElt.addClass(tabEmails[i].type);

                var fullName = this._highlight(tabEmails[i].fullName, search);
                var email = this._highlight(tabEmails[i].email, search);
                var html = (fullName == '' ? email : fullName + ' &lt;' + email + '&gt;');
                liElt.html(html);

                liElt.hover(function() {
                    $('#' + type + '_search ul > li.selected').removeClass('selected');
                    $(this).addClass('selected');
                });

                if (i == 0)
                {
                    liElt.addClass('selected');
                }

                liElt.click(function(e) {
                    // Extraction de la liste des e-mails
                    var tabEmails = mySelf._extractEmailsFromText(textElt.val());
                    var isLast = (index == tabEmails.length - 1);

                    // Remplacement de la recherche par l'adresse sélectionnée
                    if (tabEmails.length > index)
                    {
                        tabEmails[index] = $(this).attr('data-value');
                    }

                    // Détermination de la nouvelle valeur de la zone de texte
                    var value = tabEmails.splice(0, index + 1).join(mySelf.separator.char);
                    var cursorPosition = value.length;
                    value += mySelf.separator.char;
                    if (tabEmails.length > 0)
                    {
                        value += tabEmails.join(mySelf.separator.char);
                        value += mySelf.separator.char;
                    }
                    if (isLast)
                    {
                        cursorPosition = value.length;
                    }

                    // Remplissage du champ avec sa nouvelle valeur
                    textElt.val(value);

                    // Positionnement du curseur
                    mySelf._setCursorPosition(textElt, cursorPosition);

                    // Masquage du panneau de résultat
                    resultObj.hide();

                    // Stockage de la nouvelle valeur du champ
                    mySelf._previousText[type] = textElt.val();
                });

                ulElt.append(liElt);

                // Affichage des résultats
                ulElt.show();
            }

            // Masquage du message d'attente
            var loadingElt = $('#' + type + '_search .loading');
            loadingElt.hide();

            // Affichage du panneau de résultat
            resultObj.show();
        }
    },

    /**
     * Initialise les composants.
     * @param Object tabOptions Les options sous forme d'objet pouvant contenir les attributs suivants : searchUrl.
     */
    init : function(tabOptions)
    {
        // URL de réalisation
        this._searchUrl = tabOptions.searchUrl;

        // Stockage des destinataires
        this._previousText['to'] = $('#to').val();
        this._previousText['cc'] = $('#cc').val();

        // Ajout des événements
        var mySelf = this;

        $('#to').keyup(function(e) {
            mySelf._manageKeys(e, this);
        });

        $('#to_search').click(function(e) {
            e.stopPropagation();
        });

        $('#cc').keyup(function(e) {
            mySelf._manageKeys(e, this);
        });

        $('#cc_search').click(function(e) {
            e.stopPropagation();
        });

        $('body').click(function(e) {
            mySelf.hideSearchResultPanel('to');
            mySelf.hideSearchResultPanel('cc');
        });

        $('#addAttachmentBtn').click(function(e) {
            $('#addAttachment').click();
        });
        
        // Sélection du fichier dans l'input
        $('#addAttachment').change(function(event) {
            var tabFiles = event.target.files || event.dataTransfer.files;
            for (var i = 0; i < tabFiles.length; i++)
            {
                // Ajout au tableau
                mySelf.tabCustomAttachments[mySelf._cpt] = tabFiles[i];
                mySelf._cpt++;
            }
            // Affichage
            mySelf._displayCustomAttachments();
            $(this).val('');
        });
        if (tabOptions.tabCustomAttachments)
        {
            for (var i = 0; i < tabOptions.tabCustomAttachments.length; i++)
            {
                // Ajout au tableau
                mySelf.tabCustomAttachments[mySelf._cpt] = tabOptions.tabCustomAttachments[i];
                mySelf._cpt++;
            }
            mySelf._displayCustomAttachments();
        }
    },

    /**
     * Masque le panneau de résultat de la recherche.
     */
    hideSearchResultPanel : function(type)
    {
        if (this._request != null)
        {
            this._request.abort();
            this._request = null;
        }
        $('#' + type + '_search').hide();
    },

    /**
     * Envoie l'e-mail.
     */
    send : function()
    {
        var mySelf = this;
        // Image de chargement
        LOC_View.displayLoading(true);

        $('#valid').val('ok');

        // Récupération des pièces jointes ajoutées
        var tabFiles = [];

        var getFileContent = function(file){
            return new Promise(function(resolve, reject) {

                if (file.content)
                {
                    tabFiles.push(file);
                    resolve();
                }

                var reader = new FileReader();
                reader.onload = function(e) {
                    tabFiles.push({
                        name: file.name,
                        content: e.target.result
                    });
                    resolve();
                }
                reader.onerror = function(error){
                    reject(error);
                }
                reader.readAsDataURL(file);
            });
        }

        var promises = [];
        for (fileId in mySelf.tabCustomAttachments)
        {
            promises.push(getFileContent(mySelf.tabCustomAttachments[fileId]));
        }

        Promise.all(promises).then(function() {
            // Envoi de la liste des pièces jointes supplémentaires
             $('#tabCustomAttachments').val(JSON.stringify(tabFiles));

             // Soumission du formulaire
             $('form').submit();
           });

        return false;
    },

    /**
     * Affiche le nom des fichiers ajoutés dynamiquement
     */
    _displayCustomAttachments: function()
    {
        var mySelf = this;
        var customContainerElt = $('#customAttachments');
        customContainerElt.html('');

        for (fileId in this.tabCustomAttachments)
        {
            // Affichage
            var fileElt = $('<span>');
            fileElt.addClass('attachment');
            fileElt.addClass('temp');

            var extensionElt = $('<div>');
            extensionElt.addClass('fileExtension');
            fileElt.append(extensionElt);
            fileElt.append(this.tabCustomAttachments[fileId].name);

            var removeElt = $('<img>');
            removeElt.attr('src', '../web/img/removeOver.png');
            removeElt.attr('data-id', fileId);
            removeElt.attr('title', LOC_View.getTranslation('delete'));

            removeElt.click(function(e) {
                delete(mySelf.tabCustomAttachments[$(this).attr('data-id')]);
                mySelf._displayCustomAttachments();
            });

            fileElt.append(removeElt);

            customContainerElt.append(fileElt);
        }
    }

};
