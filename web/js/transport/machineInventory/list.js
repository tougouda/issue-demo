var HIDING_DELAY = 5000;

var machineInventories = {

    _table : null,
    _isWindowFocused : true,

    _tabDocumentEvents : {},

    countryId : null,

    /**
     * Initialisation de la page
     *
     * @param Object tableOptions Options du tableau Ajax
     */
    init : function(tabOptions)
    {

        // Pays
        this.countryId = tabOptions.countryId;

        // Mises à jour lors du scroll ou le redimensionnement de la fenêtre
        var scrollFunc = function()
        {
            var content = $('#locStandardContent');
            var toolbar = $('#table-toolbar-container');

            if (toolbar.parent().position().top <= 0)
            {
                $('html > body').addClass('table-toolbar-fixed');

                var padding = parseInt(toolbar.css('padding-left')) + parseInt(toolbar.css('padding-right'));
                toolbar.offset({top : content.offset().top}).
                        width(content[0].clientWidth - padding);
            }
            else
            {
                $('html > body').removeClass('table-toolbar-fixed');
                toolbar.width('');
            }
        };

        $('#locStandardContent').scroll(scrollFunc);
        $(window).resize(scrollFunc);


        // Initialisation du tableau
        this._table = $('#table-container').ajaxTable(tabOptions.tableOptions);

        var mySelf = this;

        // Fonction permettant l'ajout dynamique de title lorsque le texte est tronqué
        var setDynamicTitle = function(obj, title)
        {
            var jObj = $(obj);
            var currentTitle = jObj.attr('title');

            if (!currentTitle)
            {
                if (obj.offsetWidth < obj.scrollWidth)
                {
                    jObj.attr('title', title);
                }
            }
            else
            {
                if (obj.offsetWidth >= obj.scrollWidth && currentTitle == title)
                {
                    jObj.removeAttr('title');
                }
            }
        };

        var table = this._table;

        this._table.oninsertcell = function (table, rowIndex, colIndex, tdEl, trEl, rowData) {

            // Réinitialisation du style sur le TH de la dernière colonne
            // juste avant la mise à jour de la première ligne
            if (rowIndex == 0)
            {
                $('th.data').removeAttr('style');
            }

            var cfgs = table.getConfigs();
            var col = cfgs.columns[colIndex];

            switch (col.id)
            {
                case 'type':
                    $(tdEl).html('');
                    $(tdEl).append($('<div />').addClass('icon type_' + rowData['type']).attr('title', rowData['type.label']));
                    break;

                case 'date':
                        $(tdEl).html('');

                        if (rowData['date'])
                        {
                            var contentEl = $('<ul />').addClass('rows');

                            var liEl = $('<li />').addClass('top double').html(rowData['date']);
                            $(contentEl).append(liEl);
                            if (rowData['hour'])
                            {
                                liEl.removeClass('double');
                                var liEl = $('<li />').addClass('top hour').html(rowData['hour']);
                                $(contentEl).append(liEl);
                            }

                            $(tdEl).append(contentEl);
                        }

                        break;

                case 'status':
                    $(tdEl).html('');
                    for (var i = 0; i < rowData['status.list'].length; i++)
                    {
                        var errorType = tabErrorTypes.find(function(item) {
                            return item.value == rowData['status.list'][i];
                        });
                        if (errorType)
                        {
                            $(tdEl).append($('<div />').addClass('icon ' + errorType['code']).attr('title', errorType['title']));
                        }
                    }
                    break;

                case 'data':
                    $(tdEl).html('');

                    // N° de parc
                    var containerEl = $('<div />');
                    var labelEl = $('<span />').html(LOC_View.getTranslation('parkNumber'));
                    $(containerEl).append(labelEl);

                    if (rowData['machine.parkNumber'])
                    {
                        var tabClasses = ['locStandardParkNumber'];
                        var tabTitles = [];
                        if (isTelematActived && rowData['isWithTelematic'] == 1)
                        {
                            tabClasses.push('special');
                            tabTitles.push(LOC_View.getTranslation('isMachineWithTelematic'));
                        }

                        var divEl = $('<div />')
                                        .attr('class', tabClasses.join(' '))
                                        .attr('title', tabTitles.join(' '));
                        var aEl = $('<a />')
                                        .attr('target', '_blank')
                                        .attr('href', rowData['machine.url'])
                                        .html(rowData['machine.parkNumber']);

                        $(divEl).append(aEl);
                        $(containerEl).append(divEl);

                    }
                    else
                    {
                        var emptyEl = $('<span />')
                                        .attr('class', 'no-value')
                                        .html(LOC_View.getTranslation('noValue'));
                        $(containerEl).append(emptyEl);
                    }
                    // - Vérification de la saisie
                    if (rowData['machine.enteredParkNumber'])
                    {
                        var value = LOC_View.getTranslation('differentValue');

                        var originEl = $('<span />')
                                            .attr('class', 'original')
                                            .html(value.replace(/<%value>/, rowData['machine.enteredParkNumber']));
                        $(containerEl).append(originEl);
                    }
                    $(tdEl).append(containerEl);

                    // N° de contrat
                    var containerEl = $('<div />');
                    var labelEl = $('<span />').html(LOC_View.getTranslation('contract'));
                    $(containerEl).append(labelEl);

                    if (rowData['contract.code'])
                    {
                        var aEl = $('<a />')
                                    .attr('target', '_blank')
                                    .attr('href', rowData['contract.url'])
                                    .html(rowData['contract.code']);
                        $(containerEl).append(aEl);

                    }
                    else
                    {
                        var emptyEl = $('<span />')
                                        .attr('class', 'no-value')
                                        .html(LOC_View.getTranslation('noValue'));
                        $(containerEl).append(emptyEl);
                    }
                    // - Vérification de la saisie
                    if (rowData['contract.enteredCode'])
                    {
                        var value = LOC_View.getTranslation('differentValue');

                        var originEl = $('<span />')
                                            .attr('class', 'original')
                                            .html(value.replace(/<%value>/, rowData['contract.enteredCode']));
                        $(containerEl).append(originEl);
                    }
                    $(tdEl).append(containerEl);

                    break;

                case 'edit':
                    $(tdEl).html('');
                    if (rowData.tabRights.actions.update)
                    {
                        var divEl = $('<div />')
                                        .attr('title', LOC_View.getTranslation('edit'))
                                        .click(function() {
                                            // Ouvrir la popup de modification
                                            Location.modalWindowManager.show(rowData['url'], {contentType: 1, width: 680, height: 480});
                                        });
                        $(tdEl).append(divEl);
                    }

                    break;
            }
        };

        var updateCtrls = function (isDisabled, data) {

            // Bouton de réinisatialisation des filtres
            $('#reset-filters-btn').toggleClass('disabled', isDisabled);
        };

        this._table.onbeforeupdatedata = function () {
            updateCtrls(true, null);
        };
        this._table.onupdatedata = function (data) {
            updateCtrls(false, data);
        };

        this._table.init();


        // Savoir si la fenêtre a le focus
        $(window).focus(function()
        {
            mySelf._isWindowFocused = true;
        });

        $(window).blur(function()
        {
            mySelf._isWindowFocused = false;
        });

        // Stockage des événements au chargement de la page
        $(document).ready(function()
        {
            mySelf._tabDocumentEvents = {
                    "keypress"  : LOC_Common.clone($._data(document).events.keypress),
                    "mousemove" : LOC_Common.clone($._data(document).events.mousemove),
                    "click"     : LOC_Common.clone($._data(document).events.click)
                };
        });

        // Bouton d'actualisation
        $('#refresh-btn').click(function() {
            mySelf._table.update();
        });

        // Bouton pour revenir en haut de page
        $('#back-top-btn').click(function() {
            $('#locStandardContent').animate({scrollTop: 0}, 800);
            return false;
        });

        // Bouton de réinisatialisation des filtres
        $('#reset-filters-btn').click(function() {
            var tabIds = mySelf._table.getFilterValues('id');
            mySelf._table.resetFilters((tabIds ? {'id' : tabIds} : {}));
        });

        // Empêcher la sélection du texte dans le tableau
        $('.list').on('selectstart', false);
    },

    /**
     * Met à jour la table
     */
    updateTable: function()
    {
        this._table.update();
    }

};

