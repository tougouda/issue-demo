var machineInventory = {

    _containerEl: null,
    _tabInfos   : null,
    _from       : null,
    _photosUrl  : null,
    _historyUrl : null,
    _mapsUrl    : null,
    _tabValid   : [],

    init: function(tabOptions) {

        this._containerEl = $ge('locStandardContent');
        this._tabInfos    = tabOptions.tabInfos;
        this._from        = tabOptions.from;
        this._photosUrl   = tabOptions.photosUrl;
        this._historyUrl  = tabOptions.historyUrl;
        this._mapsUrl     = tabOptions.mapsUrl;
        this._tabValid    = tabOptions.tabValid;

        // Type d'état des lieux
        Location.commonObj.addEltClass(this._containerEl, 'type_' + this._tabInfos.type.toLowerCase());

        // État de l'état des lieux
        Location.commonObj.addEltClass(this._containerEl, this._tabInfos.stateId);

        // Chargement des photos
        this._loadPhotos();

        // Chargement de l'historique
        this._loadHistory();

        // Action sur les boutons
        var slidesEl = document.querySelector('div.slides');
        // - Voir l'historique
        $ge('machineInventoryHistoryBtn').onclick = function()
        {
            Location.commonObj.addEltClass(slidesEl, 'history');
            return false;
        };
        // - Retourner sur l'onglet principal
        $ge('machineInventoryBackBtn').onclick = function()
        {
            Location.commonObj.removeEltClass(slidesEl, 'history');
            return false;
        };
        // - Détacher un état des lieux
        var el = $ge('machineInventoryDetachBtn');
        if (el)
        {
            el.onclick = function() {
                if (confirm(LOC_View.getTranslation('answerAreYouSureDetach')))
                {
                    // Image de chargement
                    LOC_View.displayLoading(true);

                    var form = window.document.forms[0];
                    $ge('action').value = 'detach';

                    //Image de chargement
                    LOC_View.displayLoading(true);

                    //Soumission du formulaire
                    form.submit();
                }

                return false;
            };
        }
        // - Mettre à jour et rattacher un état des lieux
        el = $ge('machineInventoryAttachBtn');
        if (el)
        {
            el.onclick = function() {
                // Image de chargement
                LOC_View.displayLoading(true);

                var form = window.document.forms[0];
                $ge('action').value = 'attach';

                //Image de chargement
                LOC_View.displayLoading(true);

                //Soumission du formulaire
                form.submit();

                return false;
            };
        }
        // - Suppression d'un état des lieux
        el = $ge('machineInventoryDeleteBtn');
        if (el)
        {
            el.onclick = function() {
                if (confirm(LOC_View.getTranslation('answerAreYouSureDelete')))
                {
                    // Image de chargement
                    LOC_View.displayLoading(true);

                    var form = window.document.forms[0];
                    $ge('action').value = 'delete';

                    //Image de chargement
                    LOC_View.displayLoading(true);

                    //Soumission du formulaire
                    form.submit();
                }

                return false;
            };
        }
        // - Envoyer par e-mail
        var mailBtn = $ge('machineInventorySendMailBtn');
        Location.commonObj.toggleEltClass(mailBtn, 'disabled', !this._tabInfos.tabPossibilities.actions.sendMail);

        // Mises à jour externes
        if (this._tabValid.length > 0)
        {
            // - Sur le contrat
            if (this._from == 'rentContract')
            {
                // --- Liste des documents : détachement d'un état des lieux
                if (this._tabValid.indexOf('detach') != -1)
                {
                    var documentsObj = parent.getDocumentDocumentsObject();
                    documentsObj.list.setState(parent.DOC_PREFIX_MACHINEINVENTORY + ':' + this._tabInfos.id, this._tabInfos.stateId);
                }
            }

            // - Sur l'alerte des états des lieux à traiter
            if (this._from == 'alert')
            {
                // --- État des lieux supprimé
                if (this._tabValid.indexOf('delete') != -1 || this._tabValid.indexOf('attach') != -1)
                {
                    var listObj = parent.getListObj();
                    listObj.updateTable();
                }
            }
        }


    },

    /**
     * Chargement des photos
     */
    _loadPhotos: function() {

        var mySelf = this;

        var galleryEl = document.querySelector('.gallery');
        var photoLoadingEl = document.querySelector('.gallery > .photo-loading');
        var photoErrorEl = document.querySelector('.gallery > .photo-error');

        // - Ajout des miniatures des photos liées
        var httpObj = new Location.HTTPRequest(this._photosUrl);
        httpObj.setVar('id', this._tabInfos.id);

        httpObj.oncompletion = function() {
            var response = JSON.parse(this.response);

            // On cache l'image par défaut de chargement
            Location.commonObj.addEltClass(photoLoadingEl, 'hidden');

            if (response.tabPhotos.length > 0)
            {
                for (var i = 0; i < response.tabPhotos.length; i++)
                {
                    var tabPhotoInfos = response.tabPhotos[i];

                    if (tabPhotoInfos.content)
                    {
                        var elDownload = window.document.createElement('A');
                        elDownload.className = 'download';
                        elDownload.title = LOC_View.getTranslation('button_view_photo');
                        elDownload.href = tabPhotoInfos.url;
                        elDownload.target = '_blank';

                        elDownload.onclick = function(e) {
                            var elt = this;
                            e.stopPropagation();
                            if (Location.commonObj.hasEltClass(elt, 'downloading'))
                            {
                                return false;
                            }
                            Location.commonObj.addEltClass(elt, 'downloading');
                            setTimeout(function() {
                                Location.commonObj.removeEltClass(elt, 'downloading');
                            }, 2000);
                        };

                        // - photo
                        var elImg = window.document.createElement('IMG');
                        elImg.src = 'data:' + tabPhotoInfos.mimeType + ';base64,' + tabPhotoInfos.content;

                        elDownload.appendChild(elImg);

                        // - calque de téléchargement
                        var layerEl = $ge('layer-downloading').cloneNode(true);
                        Location.commonObj.removeEltClass(layerEl, 'hidden');
                        layerEl.removeAttribute('id');

                        elDownload.appendChild(layerEl);

                        // - calque d'infos
                        if (tabPhotoInfos.date || tabPhotoInfos.latitude && tabPhotoInfos.longitude)
                        {
                            var elInfoLayer = window.document.createElement('SPAN');
                            Location.commonObj.addEltClass(elInfoLayer, 'info');
                            elInfoLayer.title = LOC_View.getTranslation('button_view_photo_infos');

                            if (tabPhotoInfos.date)
                            {
                                var elDateInfo = window.document.createElement('SPAN');
                                var dateLabel = LOC_View.getTranslation('label_photo_date');
                                dateLabel = dateLabel.replace(/<%date>/, tabPhotoInfos.date);
                                elDateInfo.innerHTML = dateLabel;

                                elInfoLayer.appendChild(elDateInfo);
                            }

                            if (tabPhotoInfos.latitude && tabPhotoInfos.longitude)
                            {
                                var elPositionInfo = window.document.createElement('SPAN');
                                elPositionInfo.innerHTML = LOC_View.getTranslation('label_photo_position');

                                positionValue = LOC_View.getTranslation('format_position');
                                positionValue = positionValue.replace(/<%lat>/, tabPhotoInfos.latitude);
                                positionValue = positionValue.replace(/<%lng>/, tabPhotoInfos.longitude);

                                var linkEl = window.document.createElement('A');
                                var href = mySelf._mapsUrl;
                                href = href.replace(/<%lat>/, tabPhotoInfos.latitude);
                                href = href.replace(/<%lng>/, tabPhotoInfos.longitude);
                                linkEl.href = href;
                                linkEl.innerHTML = positionValue;
                                linkEl.target = '_blank';
                                linkEl.title = LOC_View.getTranslation('label_open_map');
                                linkEl.onclick = function(e) {
                                    e.stopPropagation();
                                };

                                elPositionInfo.appendChild(linkEl);
                                elInfoLayer.appendChild(elPositionInfo);

                            }

                            elInfoLayer.onclick = function(e) {
                                e.stopPropagation();
                                Location.commonObj.toggleEltClass(this, 'active');
                                return false;
                            };

                            elDownload.appendChild(elInfoLayer);
                        }

                        galleryEl.appendChild(elDownload);
                    }
                    else
                    {
                        Location.commonObj.removeEltClass(photoErrorEl, 'hidden');
                    }
                }
            }
            else
            {
                Location.commonObj.removeEltClass(photoErrorEl, 'hidden');
            }

        };

        // Erreur
        httpObj.onerror = function()
        {
            Location.commonObj.addEltClass(photoLoadingEl, 'hidden');
            Location.commonObj.removeEltClass(photoErrorEl, 'hidden');
        };

        // Envoi de la requête HTTP
        httpObj.send(true, true);
    },

    /**
     * Chargement de l'historique
     */
    _loadHistory: function() {

        var historyWaitingEl  = document.querySelector('#machineInventory-historyWaiting');
        var historyResultEl   = document.querySelector('#machineInventory-historyResult');
        var historyNoResultEl = document.querySelector('#machineInventory-historyNoResult');
        var historyErrorEl    = document.querySelector('#machineInventory-historyError');

        // - Récupération de l'historique
        var httpHistoryObj = new Location.HTTPRequest(this._historyUrl);
        httpHistoryObj.setVar('id', this._tabInfos.id);

        httpHistoryObj.oncompletion = function() {
            var response = JSON.parse(this.response);
            if (response.tabHistory.length > 0)
            {
                var tableElt = historyResultEl.getElementsByTagName('table')[0];
                var tbodyElt = tableElt.getElementsByTagName('tbody')[0];
                while (tbodyElt.hasChildNodes())
                {
                    tbodyElt.removeChild(tbodyElt.firstChild);
                }
                var tabData = response.tabHistory;
                var nbElements = tabData.length;
                var previousDate = '';
                var previousTime = '';
                for (var i = 0; i < nbElements; i++)
                {
                    var trElt = window.document.createElement('tr');
                    // Date
                    var tdElt = window.document.createElement('td');
                    if (tabData[i].date != previousDate)
                    {
                        tdElt.innerHTML = tabData[i].date;
                    }
                    trElt.appendChild(tdElt);
                    // Heure
                    var tdElt = window.document.createElement('td');
                    if (tabData[i].time != previousTime)
                    {
                        tdElt.innerHTML = tabData[i].time;
                    }
                    trElt.appendChild(tdElt);
                    // Utilisateur
                    var tdElt = window.document.createElement('td');
                    tdElt.innerHTML = tabData[i]['user.fullName'];
                    if (tabData[i]['user.state.id'] == 'GEN03')
                    {
                        tdElt.className = 'deleted';
                    }
                    trElt.appendChild(tdElt);
                    // Événement
                    var tdElt = window.document.createElement('td');
                    tdElt.innerHTML = tabData[i]['eventType.label'];
                    trElt.appendChild(tdElt);
                    // Description
                    var tdElt = window.document.createElement('td');
                    tdElt.innerHTML = tabData[i].content;
                    trElt.appendChild(tdElt);
                    tbodyElt.appendChild(trElt);
                    previousDate = tabData[i].date;
                    previousTime = tabData[i].time;
                }
                Location.commonObj.removeEltClass(historyResultEl, 'hidden');
            }
            else
            {
                Location.commonObj.removeEltClass(historyNoResultEl, 'hidden');
            }
            Location.commonObj.addEltClass(historyWaitingEl, 'hidden');
        };
        // Erreur
        httpHistoryObj.onerror = function()
        {
            Location.commonObj.addEltClass(historyWaitingEl, 'hidden');
            Location.commonObj.removeEltClass(historyErrorEl, 'hidden');
        };
        // Envoi de la requête HTTP
        httpHistoryObj.send(true, true);

    }
};

