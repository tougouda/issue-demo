var HIDING_DELAY = 5000;

var transports = {

    _hideTimeout : null,
    _table : null,
    _isWindowFocused : true,
    _viewOnlyChecked : false,

    _tabDocumentEvents : {},

    countryId : null,
    tabSelected : [],
    tabCancelable : [],
    lastSelectedLineNo : 0,
    doUrl : null,

    isSelected : function(id, type, subtype)
    {
        var tabGrep = $.grep(this.tabSelected, function(element, index)
        {
            return (element.id == id && element.type == type && element.subtype == subtype);
        });
        return (tabGrep.length > 0);
    },

    addElement : function(id, code, type, subtype, self, contractId, machineId)
    {
        if (type == TYPE_SITESTRANSFER)
        {
            if (subtype == TYPE_DELIVERY && this.isSelected(id, type, TYPE_RECOVERY) ||
                subtype == TYPE_RECOVERY && this.isSelected(id, type, TYPE_DELIVERY))
            {
                // On coche la livraison après avoir coché la récupération
                if (subtype == TYPE_DELIVERY)
                {
                    // récupération du contrat de l'élément déjà sélectionné (1er contrat à réaliser)
                    oldElement = this.getElement(id, type, TYPE_RECOVERY);
                    contractId = (oldElement ? oldElement['contract.id'] : contractId);
                }
                this.removeElement(id, type, (subtype == TYPE_DELIVERY ? TYPE_RECOVERY : TYPE_DELIVERY));
                this.tabSelected.push({"id": id, "code": code, "type": type, "self": self, "contract.id" : contractId,
                    "machine.id" : machineId, "country.id" : this.countryId});
            }
            else
            {
                this.removeElement(id, type);
                if (!this.isSelected(id, type, subtype))
                {
                    this.tabSelected.push({"id": id, "code": code, "type": type, "subtype": subtype, "self": self,
                                           "contract.id" : contractId, "machine.id" : machineId,
                                           "country.id" : this.countryId});
                }
            }
        }
        else
        {
            if (!this.isSelected(id, type, subtype))
            {
                this.tabSelected.push({"id": id, "code": code, "type": type, "subtype": subtype, "self": self,
                                       "contract.id" : contractId, "machine.id" : machineId,
                                       "country.id" : this.countryId});
            }
        }
        this.updateCheckedNumber();
    },

    getElement : function (id, type, subtype)
    {
        var tabGrep = $.grep(this.tabSelected, function(element, index)
        {
            return (element.id == id && element.type == type && element.subtype == subtype);
        });
        if (tabGrep.length == 1)
        {
            return tabGrep[0];
        }
        else
        {
            return false;
        }
    },

    removeElement : function(id, type, subtype)
    {
        if (type == TYPE_SITESTRANSFER)
        {
            // Cas où le transfert entier était sélectionné : on ne garde que l'autre partie
            var nbSelected = this.tabSelected.length;
            for (var i = 0; i < nbSelected; i++)
            {
                if (this.tabSelected[i].id == id && this.tabSelected[i].type == TYPE_SITESTRANSFER &&
                    typeof this.tabSelected[i].subtype == 'undefined')
                {
                    this.tabSelected[i].subtype = (subtype == TYPE_DELIVERY ? TYPE_RECOVERY : TYPE_DELIVERY);
                    break;
                }
            }

            // Cas où seul un côté du transfert était sélectionné : on le supprime
            this.tabSelected = $.grep(this.tabSelected, function(element, index)
            {
                return (element.id == id && element.type == type && element.subtype == subtype);
            }, true);
        }
        else
        {
            this.tabSelected = $.grep(this.tabSelected, function(element, index)
            {
                return (element.id == id && element.type == type && element.subtype == subtype);
            }, true);
        }
        this.updateCheckedNumber();
    },

    updateCheckedNumber : function()
    {
        // Tri par code
        this.tabSelected.sort(function(a, b)
        {
            return ((a.code < b.code) ? -1 : ((a.code > b.code) ? 1 : 0))
        });

        // Comptage du nombre de lignes cochées
        var nbChecked = 0;
        var nbSelected = this.tabSelected.length;
        for (var i = 0; i < nbSelected; i++)
        {
            if (this.tabSelected[i].type == TYPE_SITESTRANSFER && typeof this.tabSelected[i].subtype == 'undefined')
            {
                nbChecked += 2;
            }
            else
            {
                nbChecked++;
            }
        }

        var number = '';
        if (nbChecked > 0)
        {
            number = '(' + nbChecked + ')';
        }
        $('#do-checked').html(number);

        var list = '';
        for (var i in this.tabSelected)
        {
            list += this.tabSelected[i].code
            if (this.tabSelected[i].type == TYPE_SITESTRANSFER && typeof this.tabSelected[i].subtype != 'undefined')
            {
                list += ' (' + this.tabSelected[i].subtype + ')';
            }
            list += ', ';
        }
        list = list.substr(0, list.length - 2);
        $('#header-checked-recap').html(list);

        if (nbChecked > 0)
        {
            $('#do-btn').removeClass('disabled');
            $('#change-viewMode-full').removeClass('disabled')
        }
        else
        {
            $('#do-btn').addClass('disabled');
            $('#change-viewMode-full').addClass('disabled');
        }
    },

    do : function()
    {
        // Bouton désactivé
        if ($('#do-btn').hasClass('disabled'))
        {
            return false;
        }

        // Masque d'attente
        $('.action-mask').show();

        // Masquage des notifications
        this._hideNotifications();

        var mySelf = this;

        $.ajax({
            url : this.doUrl,
            method : 'post',
            dataType : 'json',
            data : JSON.stringify({
                action : 'do',
                transports : this.tabSelected
            }),
            contentType: 'application/json',
        }).done(function(data)
        {
            // Marquage des transports réalisés
            var nbDone = data.done.length;
            for (var i = 0; i < nbDone; i++)
            {
                var checkbox;

                // Cas du transfert inter-chantiers
                if (data.done[i].type == TYPE_SITESTRANSFER)
                {
                    var type = (typeof data.done[i].subtype == 'undefined' ? TYPE_RECOVERY : data.done[i].subtype);
                    checkbox = $('#transport-' + data.done[i].id + '-' + type);
                    checkbox.closest('tr').removeClass('selected').addClass('done');
                    checkbox.replaceWith('<div class="done"></div>');

                    // Case à cocher supplémentaire dans le cas où il s'agit du transfert complet
                    if (typeof data.done[i].subtype == 'undefined')
                    {
                        $('#transport-' + data.done[i].id + '-' + TYPE_DELIVERY).replaceWith('<div class="done"></div>');
                    }
                }
                else
                {
                    checkbox = $('#transport-' + data.done[i].id + '-' + data.done[i].type);
                    checkbox.closest('tr').removeClass('selected').addClass('done');
                    checkbox.replaceWith('<div class="done"></div>');
                }
            }

            // Marquage des transports en erreur
            var nbErrors = data.errors.length;
            for (var i = 0; i < nbErrors; i++)
            {
                var checkbox;

                // Cas du transfert inter-chantiers
                if (data.errors[i].type == TYPE_SITESTRANSFER)
                {
                    var type = (typeof data.errors[i].subtype == 'undefined' ? TYPE_RECOVERY : data.errors[i].subtype);
                    checkbox = $('#transport-' + data.errors[i].id + '-' + type);
                    checkbox.closest('tr').removeClass('selected').removeClass('done').addClass('error');
                    checkbox.replaceWith('<div class="error"></div>');

                    // Case à cocher supplémentaire dans le cas où il s'agit du transfert complet
                    if (typeof data.errors[i].subtype == 'undefined')
                    {
                        $('#transport-' + data.errors[i].id + '-' + TYPE_DELIVERY).replaceWith('<div class="error"></div>');
                    }
                }
                // Cas général
                else
                {
                    checkbox = $('#transport-' + data.errors[i].id + '-' + data.errors[i].type);
                    checkbox.closest('tr').removeClass('selected').removeClass('done').addClass('error');
                    checkbox.replaceWith('<div class="error"></div>');
                }
            }

            // Réalisation annulables
            var nbUndoable = data.undoable.length;

            // Affichage/Masquage des notifications
            Location.viewObj.displayNotification('error-single', (nbErrors == 1 ? 'show' : 'hide'), {});
            Location.viewObj.displayNotification('error-plural', (nbErrors >= 2 ? 'show' : 'hide'), {});
            Location.viewObj.displayNotification('cancel-single-noundo', (nbDone == 1 && !nbUndoable ? 'show' : 'hide'), {});
            Location.viewObj.displayNotification('cancel-single', (nbDone == 1 && nbUndoable ? 'show' : 'hide'), {});
            Location.viewObj.displayNotification('cancel-plural-noundo', (nbDone >= 2 && !nbUndoable ? 'show' : 'hide'), {});
            Location.viewObj.displayNotification('cancel-plural', (nbDone >= 2 && nbUndoable ? 'show' : 'hide'), {});

            // Vidage des transports sélectionnés
            mySelf.tabCancelable = mySelf.tabSelected;
            mySelf.tabSelected = [];
            mySelf.updateCheckedNumber();

            // Programmation du masquage des notifications et des transports réalisés
            mySelf._setHidingTimeout();

            // Retardement du masquage sur une action de l'utilisateur
            $(document).keypress(function(e) { mySelf._setHidingTimeout(); });
            $(document).mousemove(function(e) { mySelf._setHidingTimeout(); });
            $(document).click(function(e) { mySelf._setHidingTimeout(); });
        }).fail(function()
        {
            var nbSelected = mySelf.tabSelected.length;

            // Affichage/Masquage des notifications
            Location.viewObj.displayNotification('error-single', (nbSelected == 1 ? 'show' : 'hide'), {});
            Location.viewObj.displayNotification('error-plural', (nbSelected >= 2 ? 'show' : 'hide'), {});
        }).always(function()
        {
            // Masque d'attente
            $('.action-mask').hide();
        });
    },

    undo : function()
    {
        // Annulation du masquage programmé
        if (this._hideTimeout != null)
        {
            clearTimeout(this._hideTimeout);
            this._hideTimeout = null;
        }

        // Fin du retardement du masquage sur une action de l'utilisateur
        $._data(document).events.keypress  = LOC_Common.clone(this._tabDocumentEvents.keypress);
        $._data(document).events.mousemove = LOC_Common.clone(this._tabDocumentEvents.mousemove);
        $._data(document).events.click     = LOC_Common.clone(this._tabDocumentEvents.click);

        // Masque d'attente
        $('.action-mask').show();

        // Masquage des notifications
        this._hideNotifications();

        var mySelf = this;

        $.ajax({
            url : this.doUrl,
            method : 'post',
            dataType : 'json',
            data : JSON.stringify({
                action : 'undo',
                transports : this.tabCancelable
            }),
            contentType: 'application/json',
        }).done(function(data)
        {
            // Vidage des transports sélectionnés
            mySelf.tabSelected = mySelf.tabCancelable;
            mySelf.tabCancelable = [];

            mySelf.updateCheckedNumber();
            mySelf._hideDoneTransports();
        }).fail(function()
        {
            var nbCancelable = mySelf.tabCancelable.length;

            // Affichage/Masquage des notifications
            Location.viewObj.displayNotification('error-single', (nbCancelable == 1 ? 'show' : 'hide'), {});
            Location.viewObj.displayNotification('error-plural', (nbCancelable >= 2 ? 'show' : 'hide'), {});
        }).always(function()
        {
            // Masque d'attente
            $('.action-mask').hide();
        });
    },


    /**
     * Initialisation de la page
     *
     * @param Object tableOptions Options du tableau Ajax
     */
    init : function(tabOptions)
    {
        // URL de réalisation
        this.doUrl = tabOptions.doUrl;

        // Pays
        this.countryId = tabOptions.countryId;

        // Mises à jour lors du scroll ou le redimensionnement de la fenêtre
        var scrollFunc = function()
        {
            var content = $('#locStandardContent');
            var toolbar = $('#table-toolbar-container');

            if (toolbar.parent().position().top <= 0)
            {
                $('html > body').addClass('table-toolbar-fixed');

                var padding = parseInt(toolbar.css('padding-left')) + parseInt(toolbar.css('padding-right'));
                toolbar.offset({top : content.offset().top}).
                        width(content[0].clientWidth - padding);
            }
            else
            {
                $('html > body').removeClass('table-toolbar-fixed');
                toolbar.width('');
            }
        };

        $('#locStandardContent').scroll(scrollFunc);
        $(window).resize(scrollFunc);


        // Initialisation du tableau
        this._table = $('#table-container').ajaxTable(tabOptions.tableOptions);

        var mySelf = this;

        // Fonction permettant l'ajout dynamique de title lorsque le texte est tronqué
        var setDynamicTitle = function(obj, title)
        {
            var jObj = $(obj);
            var currentTitle = jObj.attr('title');

            if (!currentTitle)
            {
                if (obj.offsetWidth < obj.scrollWidth)
                {
                    jObj.attr('title', title);
                }
            }
            else
            {
                if (obj.offsetWidth >= obj.scrollWidth && currentTitle == title)
                {
                    jObj.removeAttr('title');
                }
            }
        };

        var table = this._table;

        this._table.oninsertcell = function (table, rowIndex, colIndex, tdEl, trEl, rowData) {

            // Réinitialisation du style sur le TH de la dernière colonne
            // juste avant la mise à jour de la première ligne
            if (rowIndex == 0)
            {
                $('th.doable').removeAttr('style');
            }

            var cfgs = table.getConfigs();
            var col = cfgs.columns[colIndex];

            switch (col.id)
            {
                case 'type':
                    $(tdEl).html('');
                    $(tdEl).append($('<div />').addClass('icon type_' + rowData['type']).attr('title', rowData['type.label']));
                    break;

                case 'date':
                    $(tdEl).html('');

                    if (rowData['date'])
                    {
                        var contentEl = $('<ul />').addClass('rows');

                        var liEl = $('<li />').addClass('top double').html(rowData['date']);
                        $(contentEl).append(liEl);
                        if (rowData['hour'])
                        {
                            liEl.removeClass('double');
                            var liEl = $('<li />').addClass('top hour').html('&gt;&nbsp;' + rowData['hour']);
                            $(contentEl).append(liEl);
                        }

                        if (rowData['linkDate'])
                        {
                            var liEl = $('<li />').addClass('btm double').html(rowData['linkDate']);
                            $(contentEl).append(liEl);
                            if (rowData['linkHour'])
                            {
                                liEl.removeClass('double');
                                var liEl = $('<li />').addClass('btm hour').html('&gt;&nbsp;' + rowData['linkHour']);
                                $(contentEl).append(liEl);
                            }
                        }

                        $(tdEl).append(contentEl);
                    }

                    break;

                case 'document.code':
                    $(tdEl).html('');

                    if (rowData['contract.id'])
                    {
                        var contentEl = $('<ul />').addClass('rows');

                        var aEl = $('<a />').attr('target', '_blank').html(rowData['document.code']).attr('href', rowData['contract.url']);
                        $(contentEl).append($('<li />').addClass('top double').append(aEl));

                        if (rowData['linkContract.id'])
                        {
                            var aEl = $('<a />').attr('target', '_blank').html(rowData['linkContract.code']).attr('href', rowData['linkContract.url']);
                            $(contentEl).append($('<li />').addClass('btm double').append(aEl));
                        }

                        $(tdEl).append(contentEl);
                    }
                    else if (rowData['estimateLine.id'])
                    {
                        var contentEl = $('<ul />').addClass('rows');

                        var aEl = $('<a />').attr('target', '_blank').html(rowData['document.code']).attr('href', rowData['estimateLine.url']);
                        var liEl = $('<li />').addClass('top double');
                        $(contentEl).append(liEl.append(aEl));

                        if (rowData['document.index'])
                        {
                            liEl.removeClass('double');
                            var aEl = $('<a />').attr('target', '_blank').html('(' + rowData['document.index'] + ')').attr('href', rowData['estimateLine.url']);
                            var liEl = $('<li />').addClass('top index');
                            $(contentEl).append(liEl.append(aEl));
                        }

                        $(tdEl).append(contentEl);
                    }

                    break;

                case 'document.isAsbestos':
                    $(tdEl).html('');

                    if (rowData['document.isAsbestos'] != null)
                    {
                        var contentEl = $('<ul />').addClass('rows');

                        var spanEl = $('<span />');
                        if (rowData['document.isAsbestos'] == 1)
                        {
                            spanEl.addClass('asbestos');
                            spanEl.attr('title', LOC_View.getTranslation('isAsbestos'));
                        }

                        $(contentEl).append($('<li />').addClass('top double').append(spanEl));


                        if (rowData['linkContract.isAsbestos'] != null)
                        {
                            var spanEl = $('<span />');
                            if (rowData['linkContract.isAsbestos'] == 1)
                            {
                                spanEl.addClass('asbestos');
                                spanEl.attr('title', LOC_View.getTranslation('isAsbestos'));
                            }
                            $(contentEl).append($('<li />').addClass('btm double').append(spanEl));
                        }

                        $(tdEl).append(contentEl);
                    }
                    break;

                case 'from.label':
                    if (rowData['type'] == TYPE_SITESTRANSFER || rowData['type'] == TYPE_RECOVERY)
                    {
                        var labelDiv = $('<div />').html(rowData['from.label']);
                        labelDiv.mouseenter(function() { setDynamicTitle(this, rowData['from.label']); });

                        var addressDiv = $('<div />').html(rowData['from.address']);
                        addressDiv.mouseenter(function() { setDynamicTitle(this, rowData['from.address']); });

                        $(tdEl).html(labelDiv).append(addressDiv);
                    }
                    else
                    {
                        var labelDiv = $('<div />').html(rowData['from.label']);
                        labelDiv.mouseenter(function() { setDynamicTitle(this, rowData['from.label']); });

                        $(tdEl).html(labelDiv);
                    }
                    break;

                case 'arrow':
                    tdEl.innerHTML = '→';
                    break;

                case 'to.label':
                    if (rowData['type'] == TYPE_SITESTRANSFER || rowData['type'] == TYPE_DELIVERY)
                    {
                        var labelDiv = $('<div />').html(rowData['to.label']);
                        labelDiv.mouseenter(function() { setDynamicTitle(this, rowData['to.label']); });

                        var addressDiv = $('<div />').html(rowData['to.address']);
                        addressDiv.mouseenter(function() { setDynamicTitle(this, rowData['to.address']); });

                        $(tdEl).html(labelDiv).append(addressDiv);
                    }
                    else
                    {
                        var labelDiv = $('<div />').html(rowData['to.label']);
                        labelDiv.mouseenter(function() { setDynamicTitle(this, rowData['to.label']); });

                        $(tdEl).html(labelDiv);
                    }
                    break;

                case 'machine.parkNumber':
                    if (rowData['machine.parkNumber'])
                    {
                        var tabClasses = ['locStandardParkNumber'];
                        var tabTitles = [];
                        if (isTelematActived && rowData['isWithTelematic'] == 1)
                        {
                            tabClasses.push('special');
                            tabTitles.push(LOC_View.getTranslation('isMachineWithTelematic'));
                        }
                        tdEl.innerHTML = '<div class="' + tabClasses.join(' ') + '" title="' + tabTitles.join(' ') + '"><a target="_blank" href="' + rowData['machine.url'] + '">' + rowData['machine.parkNumberPrefix'] + rowData['machine.parkNumber'] + '</a></div>';
                    }
                    else
                    {
                        tdEl.innerHTML = '';
                    }
                    break;

                case 'customer.label':
                    $(tdEl).html('');

                    if (rowData['customer.id'])
                    {
                        var contentEl = $('<ul />').addClass('rows');

                        var aEl = $('<a />').attr('target', '_blank').html(rowData['customer.label']).attr('href', rowData['customer.url']);
                        var liEl = $('<li />').addClass('top double');
                        liEl.mouseenter(function() { setDynamicTitle(this, rowData['customer.label']); });
                        contentEl.append(liEl.append(aEl));

                        if (rowData['linkCustomer.id'])
                        {
                            var aEl = $('<a />').attr('target', '_blank').html(rowData['linkCustomer.label']).attr('href', rowData['linkCustomer.url']);
                            var liEl = $('<li />').addClass('btm double');
                            liEl.mouseenter(function() { setDynamicTitle(this, rowData['linkCustomer.label']); });
                            $(contentEl).append(liEl.append(aEl));
                        }

                        $(tdEl).append(contentEl);
                    }

                    break;

                case 'isDoable':
                    $(tdEl).html('');

                    if (rowData['isDoable'] == 1)
                    {
                        if (rowData['isDone'] == 0 && rowData['type'] == TYPE_SITESTRANSFER &&
                                !mySelf.isSelected(rowData['id'], rowData['type'], TYPE_RECOVERY) &&
                                mySelf.isSelected(rowData['id'], rowData['type'], TYPE_DELIVERY))
                        {
                            mySelf.addElement(rowData['id'], rowData['order.code'], rowData['type'],
                                    TYPE_RECOVERY, rowData.isSelf, rowData['contract.id'],
                                    rowData['machine.id']);
                        }

                        var contentEl = $('<ul />').addClass('rows');

                        // Première case à cocher (tous transports sauf transferts inter-chantiers)
                        var checkbox = $('<input type="checkbox">');
                        checkbox.val(rowData['id']);
                        checkbox.attr('id',
                                      'transport-' + rowData['id'] + '-' +
                                          (rowData['type'] == TYPE_SITESTRANSFER ? TYPE_RECOVERY : rowData['type']));
                        $.data(checkbox[0], 'code', rowData['order.code']);
                        $.data(checkbox[0], 'type', rowData['type']);
                        if (rowData['type'] == TYPE_SITESTRANSFER)
                        {
                            $.data(checkbox[0], 'subtype', TYPE_RECOVERY);
                        }
                        $.data(checkbox[0], 'contract.id', rowData['contract.id']);
                        $.data(checkbox[0], 'machine.id', rowData['machine.id']);
                        if (rowData['isDone'] == 1 || rowData['isLinkDone'] == 1 ||
                            rowData['type'] == TYPE_SITESTRANSFER &&
                                (mySelf.isSelected(rowData['id'], rowData['type'], TYPE_RECOVERY) ||
                                    mySelf.isSelected(rowData['id'], rowData['type'], TYPE_DELIVERY)) ||
                            mySelf.isSelected(rowData['id'], rowData['type']))
                        {
                            checkbox.attr('checked', true);
                            if (rowData['isDone'] != 1)
                            {
                                $(trEl).addClass('selected');
                            }
                        }
                        if (rowData['isDone'] == 1)
                        {
                            checkbox.attr('disabled', true);
                        }
                        // Droits suivant le type de transport
                        // - transferts inter-agences
                        // - transports D, R, TS standard
                        // - transports D, R, TS enlèvement
                        var isSelf = (rowData.isSelf == 1 ? true : false);
                        if ((rowData.type == TYPE_AGENCIESTRANSFER && !rowData.tabRights.actions['do-agtsf']) ||
                            (rowData.type != TYPE_AGENCIESTRANSFER && !isSelf && !rowData.tabRights.actions['do-std']) ||
                            (rowData.type != TYPE_AGENCIESTRANSFER && isSelf && !rowData.tabRights.actions['do-self']))
                        {
                            checkbox.attr('disabled', true);
                        }

                        checkbox.change(function()
                        {
                            if (this.checked)
                            {
                                mySelf.addElement(this.value, $.data(this, 'code'), $.data(this, 'type'),
                                        $.data(this, 'subtype'), rowData.isSelf, $.data(this, 'contract.id'),
                                        $.data(this, 'machine.id'));
                                $(trEl).addClass('selected');
                            }
                            else
                            {
                                mySelf.removeElement(this.value, $.data(this, 'type'), $.data(this, 'subtype'));
                                $(trEl).removeClass('selected');
                            }
                        });
                        if (rowData['type'] == TYPE_SITESTRANSFER)
                        {
                            checkbox.click(function()
                            {
                                var deliveryChb = $('#transport-' + rowData['id'] + '-' + TYPE_DELIVERY);
                                if (rowData['isSelf'] == 0 && !this.checked && deliveryChb[0].checked ||
                                    rowData['isSelf'] == 1 && this.checked != deliveryChb[0].checked &&
                                        !deliveryChb[0].disabled)
                                {
                                    deliveryChb.click();
                                }
                            });
                        }

                        contentEl.append($('<li />').addClass('top double').html('&nbsp;').append(checkbox));

                        // Seconde case à cocher (transferts inter-chantiers)
                        if (rowData['type'] == TYPE_SITESTRANSFER)
                        {
                            var checkbox = $('<input type="checkbox">');
                            checkbox.val(rowData['id']);
                            checkbox.attr('id', 'transport-' + rowData['id'] + '-' + TYPE_DELIVERY);
                            $.data(checkbox[0], 'code', rowData['order.code']);
                            $.data(checkbox[0], 'type', rowData['type']);
                            $.data(checkbox[0], 'subtype', TYPE_DELIVERY);
                            $.data(checkbox[0], 'contract.id', rowData['linkContract.id']);
                            $.data(checkbox[0], 'machine.id', rowData['machine.id']);
                            if (rowData['isLinkDone'] == 1 ||
                                rowData['type'] == TYPE_SITESTRANSFER &&
                                    mySelf.isSelected(rowData['id'], rowData['type'], TYPE_DELIVERY) ||
                                mySelf.isSelected(rowData['id'], rowData['type']))
                            {
                                checkbox.attr('checked', true);
                                $(trEl).addClass('selected');
                            }
                            if (rowData['isLinkDone'] == 1)
                            {
                                checkbox.attr('disabled', true);
                            }

                            // Droits suivant le type de transport
                            // - transports TS standard
                            // - transports TS enlèvement
                            var isSelf = (rowData.isSelf == 1 ? true : false);
                            if ((!isSelf && !rowData.tabRights.actions['do-std']) ||
                                (isSelf && !rowData.tabRights.actions['do-self']))
                            {
                                checkbox.attr('disabled', true);
                            }

                            checkbox.change(function()
                            {
                                if (this.checked)
                                {
                                    mySelf.addElement(this.value, $.data(this, 'code'), $.data(this, 'type'),
                                            $.data(this, 'subtype'), rowData.isSelf, $.data(this, 'contract.id'),
                                            $.data(this, 'machine.id'));
                                    $(trEl).addClass('selected');
                                }
                                else
                                {
                                    mySelf.removeElement(this.value, $.data(this, 'type'), $.data(this, 'subtype'));
                                    $(trEl).removeClass('selected');
                                }
                            });
                            checkbox.click(function()
                            {
                                var recoveryChb = $('#transport-' + rowData['id'] + '-' + TYPE_RECOVERY);
                                if (rowData['isSelf'] == 0 && this.checked && !recoveryChb.is(':checked') ||
                                    rowData['isSelf'] == 1 && this.checked != recoveryChb[0].checked &&
                                        !recoveryChb[0].disabled)
                                {
                                    recoveryChb.click();
                                }
                            });

                            contentEl.append($('<li />').addClass('btm double').html('&nbsp;').append(checkbox));
                        }

                        $(tdEl).append(contentEl);
                    }
                    else if (rowData['type'] == TYPE_RECOVERY && rowData['document.state.id'] == STATE_CONTRACT)
                    {
                        // Affichage de l'icône de l'état du contrat
                        var contentEl = $('<div />').addClass('non-doable recovery-contract')
                                                    .attr('title', tabNonDoabilityTypes['recovery-contract']);

                        $(tdEl).append(contentEl);
                    }
                    else if (rowData['document.state.id'] == STATE_ESTIMATELINE)
                    {
                        // Affichage de l'icône de l'état du contrat
                        var contentEl = $('<div />').addClass('non-doable estimateLine')
                                                    .attr('title', tabNonDoabilityTypes['estimateLine']);

                        $(tdEl).append(contentEl);
                    }
                    else
                    {
                        // Affichage des erreurs
                        var contentEl = $('<div />').addClass('errors');

                        if (Number(rowData['errors']) == 0)
                        {
                            contentEl.append($('<div />').addClass('errorImg'));
                        }
                        else
                        {
                            var nbErrors = 0;
                            for (var i in tabErrorTypes)
                            {
                                if (Number(rowData['errors']) & tabErrorTypes[i]['value'])
                                {
                                    contentEl.append($('<div />').addClass('errorImg ' + tabErrorTypes[i]['code']).
                                            attr('title', tabErrorTypes[i]['title']));
                                    nbErrors++;
                                }
                            }

                            // Ajustement de la largeur de la colonne en fonction du nombre maximum d'erreur à afficher
                            if (nbErrors)
                            {
                                var width = $('th.doable').css('width');
                                width = Number(width.substr(0, width.length - 2));
                                var cellWidth = nbErrors * 20;
                                if (cellWidth > width)
                                {
                                    $('th.doable').css('width', cellWidth + 'px');
                                }
                            }
                        }

                        $(tdEl).append(contentEl);
                        $(trEl).addClass('error');
                    }

                    $(trEl).click(function(e)
                    {
                        var targetTagName = e.target.tagName.toLowerCase();
                        var currentLineNo = $(this).parent().children().index($(this));

                        if (targetTagName == 'input')
                        {
                            // Stockage du numéro de ligne de début de sélection
                            mySelf.lastSelectedLineNo = currentLineNo;
                        }
                        else if (targetTagName != 'a')
                        {
                            if (e.shiftKey)
                            {
                                // Sélection de la plage de lignes
                                var tableRows = $(this).parent().children();

                                var begin = Math.min(currentLineNo, mySelf.lastSelectedLineNo);
                                var end   = Math.max(currentLineNo, mySelf.lastSelectedLineNo);

                                for (var i = begin; i <= end; i++)
                                {
                                    $.each($(tableRows[i]).find('input:checkbox'), function(key, value)
                                    {
                                        if (!$(value).is(':checked'))
                                        {
                                            $(value).click();
                                        }
                                    });
                                }
                            }
                            else
                            {
                                // Stockage du numéro de ligne de début de sélection
                                mySelf.lastSelectedLineNo = currentLineNo;

                                // Cases à cocher de la ligne
                                var tabChbx = $(tdEl).find('input:checkbox:not(disabled)');
                                if (tabChbx.length == 2)
                                {
                                    var tabIsChecked = [$(tabChbx[0]).is(':checked'), $(tabChbx[1]).is(':checked')];
                                    if (rowData['isSelf'] == 1 || tabIsChecked[0] == tabIsChecked[1])
                                    {
                                        if ($(tabChbx[0]).is(':disabled'))
                                        {
                                            $(tabChbx[1]).click();
                                        }
                                        else
                                        {
                                            $(tabChbx[0]).click();
                                        }
                                    }
                                    if (rowData['isSelf'] == 0 && !tabIsChecked[1])
                                    {
                                        $(tabChbx[1]).click();
                                    }
                                }
                                else
                                {
                                    $(tabChbx[0]).click();
                                }
                            }
                        }
                    });

                    break;
            }
        };

        var updateCtrls = function (isDisabled, data) {

            // Bouton pour l'export au format Excel
            if (isDisabled || (tabOptions.excelRowsLimit && data.totalRowsCount > tabOptions.excelRowsLimit))
            {
                $('#excel-btn').addClass('disabled').
                                attr('href', '#').
                                unbind('click').
                                click(false);
            }
            else
            {
                $('#excel-btn').removeClass('disabled').
                                attr('href', data.excelUrl).
                                unbind('click');
            }

            // Bouton de réinisatialisation des filtres
            $('#reset-filters-btn').toggleClass('disabled', isDisabled);
        };

        this._table.onbeforeupdatedata = function () {
            updateCtrls(true, null);
        };
        this._table.onupdatedata = function (data) {
            updateCtrls(false, data);
        };

        this._table.init();


        // Savoir si la fenêtre a le focus
        $(window).focus(function()
        {
            mySelf._isWindowFocused = true;
        });

        $(window).blur(function()
        {
            mySelf._isWindowFocused = false;
        });

        // Stockage des événements au chargement de la page
        $(document).ready(function()
        {
            if ($._data(document).events)
            {
                mySelf._tabDocumentEvents = {
                    "keypress"  : LOC_Common.clone($._data(document).events.keypress),
                    "mousemove" : LOC_Common.clone($._data(document).events.mousemove),
                    "click"     : LOC_Common.clone($._data(document).events.click)
                };
            }
        });

        // Bouton d'actualisation
        $('#refresh-btn').click(function() {
            mySelf._hideDoneTransports();
        });

        // Bouton pour revenir en haut de page
        $('#back-top-btn').click(function() {
            $('#locStandardContent').animate({scrollTop: 0}, 800);
            return false;
        });

        // Bouton de réinisatialisation des filtres
        $('#reset-filters-btn').click(function() {
            var tabIds = mySelf._table.getFilterValues('id');
            mySelf._table.resetFilters((tabIds ? {'id' : tabIds} : {}));
        });

        // Bouton de réalisation
        $('#do-btn').click(function(e) {
            mySelf.do();
        });

        // Bouton pour afficher tous les transports
        $('#change-viewMode-checked').click(function() { mySelf.viewAllTransports(); });

        // Bouton pour afficher seulement les transports cochés
        $('#change-viewMode-full').click(function() { mySelf.viewCheckedTransports(); });

        // Bouton pour désélectionner tous les transports
        $('#uncheck-btn').click(function() {
            $.each($('#table-container input[type=checkbox]'), function(key, value)
            {
                if ($(value).is(':checked'))
                {
                    $(value).click();
                }
            });
        });

        // Empêcher la sélection du texte dans le tableau
        $('.list').on('selectstart', false);
    },


    /**
     * Affichage des transports cochés seulement
     */
    viewCheckedTransports : function()
    {
        // Bouton désactivé
        if ($('#change-viewMode-full').hasClass('disabled'))
        {
            return false;
        }

        var tabIds = [];
        for (var i = 0; i < this.tabSelected.length; i++)
        {
            tabIds.push(this.tabSelected[i].id);
        }
        this._table.clearFilters({'id' : tabIds});

        $('#change-viewMode-full').hide();
        $('#change-viewMode-checked').show();

        this._viewOnlyChecked = true;
    },

    /**
     * Affichage de tous les transports
     */
    viewAllTransports : function()
    {
        this._table.setFilterValues('id', []);

        $('#change-viewMode-full').show();
        $('#change-viewMode-checked').hide();

        this._viewOnlyChecked = false;
    },


    /**
     * Masquage des transports venant d'être réalisés
     */
    _hideDoneTransports : function()
    {
        if (this._viewOnlyChecked)
        {
            this.viewAllTransports();
        }
        else
        {
            this._table.update();
        }
    },

    /**
     * Masquage de toutes les notifications
     */
    _hideNotifications : function()
    {
        Location.viewObj.displayNotification('error-single', 'hide', {});
        Location.viewObj.displayNotification('error-plural', 'hide', {});
        Location.viewObj.displayNotification('cancel-single-noundo', 'hide', {});
        Location.viewObj.displayNotification('cancel-single', 'hide', {});
        Location.viewObj.displayNotification('cancel-plural-noundo', 'hide', {});
        Location.viewObj.displayNotification('cancel-plural', 'hide', {});
    },

    /**
     * Masquage des notifications et des transports réalisés après une action de l'utilisateur
     */
    // Programmation du masquage des notifications et des transports réalisés
    _hideDoneTransportsAndNotifications : function()
    {
        this._hideDoneTransports();
        this._hideNotifications();
        this.tabCancelable = [];
    },

    /**
     * Programmation du masquage des notifications et des transports réalisés
     *
     * @param bool force - Forcer la programmation du masquage (true) ou non (false). False par défaut
     */
    _setHidingTimeout : function()
    {
        if (this._hideTimeout != null)
        {
            clearTimeout(this._hideTimeout);
        }

        var mySelf = this;

        // Programmation du masquage des notifications et des transports réalisés
        this._hideTimeout = setTimeout(function()
        {
            if (mySelf._isWindowFocused)
            {
                // Fin du retardement du masquage sur une action de l'utilisateur
                $._data(document).events.keypress  = LOC_Common.clone(mySelf._tabDocumentEvents.keypress);
                $._data(document).events.mousemove = LOC_Common.clone(mySelf._tabDocumentEvents.mousemove);
                $._data(document).events.click     = LOC_Common.clone(mySelf._tabDocumentEvents.click);

                mySelf._hideDoneTransportsAndNotifications();
                mySelf._hideTimeout = null;
            }
            else
            {
                mySelf._setHidingTimeout();
            }
        }, HIDING_DELAY);
    }

};

