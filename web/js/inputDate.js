function updateDateHidden(value, objName, pattern)
{
    // Paramètres de chacun des éléments de la date
    var tabParams = {year: {}, month: {}, day: {}};
    
    // Récupération des paramètres de l'année
    var tabYearPatterns = {'%g': 2, '%G': 4, '%y': 2, '%Y': 4};
    for (var pat in tabYearPatterns)
    {
        var index = pattern.indexOf(pat);
        if (index != -1)
        {
            tabParams['year']['position'] = index;
            tabParams['year']['length']   = tabYearPatterns[pat];
        }
    }
    // Récupération des paramètres du mois
    index = pattern.indexOf('%m');
    if (index != -1)
    {
        tabParams['month']['position'] = index;
        tabParams['month']['length']   = 2;
    }
    // Récupération des paramètres du mois
    index = pattern.indexOf('%d');
    if (index != -1)
    {
        tabParams['day']['position'] = index;
        tabParams['day']['length']   = 2;
    }

    var year  = value.substr(tabParams['year']['position'], tabParams['year']['length']);
    var month = value.substr(tabParams['month']['position'], tabParams['month']['length']);
    var day   = value.substr(tabParams['day']['position'], tabParams['day']['length']);

    var result = '';
    var isValid = false;
    if (day.length == tabParams['day']['length'] &&
        month.length == tabParams['month']['length'] &&
        year.length == tabParams['year']['length'])
    {
        if (tabParams['year']['length'] == 2)
        {
            if (year >= 50)
            {
                year = year * 1 + 1900;
            }
            else
            {
                year = year * 1 + 2000;
            }
        }
        var dateValue = new Date(year * 1, month * 1 - 1, day * 1);
        var result = dateValue.getFullYear() + '-';
        if (dateValue.getMonth() + 1 < 10)
        {
            result += '0';
        }
        result += (dateValue.getMonth() + 1) + '-';
        if (dateValue.getDate() < 10)
        {
            result += '0';
        }
        result += dateValue.getDate();

        isValid = (result == year + '-' + month + '-' + day);
    }

    var obj = window.document.getElementById(objName + '.masked');
    if (obj)
    {
        if (isValid)
        {
            obj.removeAttribute('class');
            if (obj.onvalidate)
            {
                obj.onvalidate(result, value);
            }
        }
        else
        {
            obj.className = 'error';
        }
    }
    return (isValid ? result : '');
}