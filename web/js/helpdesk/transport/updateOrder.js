var tabToUnlock = [];

function onKeyUpField(eventObj)
{
    var keyCode = Location.commonObj.getEventKeyCode(eventObj);
    if (keyCode == 13)
    {
        submitForm("formVerif", "validVerif");
        return false;
    }
    return true;
}

function submitForm(formName, chpValid)
{
    var form = $('form[name="' + formName + '"]');
    var chmp = $('#' + chpValid);
    chmp.val('ok');

    var activeTabIndex = Location.tabBoxesManager.getTabBox("TabBox").getActiveTabIndex();
    $('input[name="activeTabIndex"]').val(activeTabIndex);

    // Récupération des agences sélectionnées
    var tabAgencies = [];
    $('#choiceAgencies > table.searchAgencies > tbody > tr.area > td.agencies > span.agency.on').each(function(){
        tabAgencies.push($(this).html());
    });
    $('#selectedAgencies').val(tabAgencies.join(','));

    if (chpValid == 'validUnlock')
    {
        //Image de chargement
        LOC_View.displayLoading(true);
        $('#validVerif').val('ok');
        $('#tspToUnlock').val(tabToUnlock.join(','));
    }
    form.submit();

}

function checkAll()
{
    var isCheckAll = $('input[name=\'cbCheckAll\']').is(':checked');
    $('tr[id^=\'line_\']:not(.locked) td.selection input').each(function(){
        $(this).prop('checked', (isCheckAll ? true : false))
    });

}

function clearSearch(prefix, index)
{

    var activeTabIndex = Location.tabBoxesManager.getTabBox("TabBox").getActiveTabIndex();

    // On efface la recherche uniquement si on est sur l'onglet actif
    if (index == activeTabIndex)
    {
        $ge(prefix  + "search.no").value = "";
        $ge(prefix  + "search.state.id").selectedIndex = 0;
        if ($ge(prefix  + "search.user.id"))
        {
            Location.searchBoxesManager.getSearchBox(prefix  + "search.user.id").clear();
        }
        if ($ge(prefix  + "search.customer.id"))
        {
            Location.searchBoxesManager.getSearchBox(prefix  + "search.customer.id").clear();
        }
        if ($ge(prefix  + "search.negotiator.id"))
        {
            Location.searchBoxesManager.getSearchBox(prefix  + "search.negotiator.id").clear();
        }
        $ge(prefix  + "search.parkNo").value = "";
        $ge(prefix  + "search.site.label").value = "";
        $ge(prefix  + "search.site.department").value = "";
        clearMaskedInputValue(prefix  + "search.beginDate");
        clearMaskedInputValue(prefix  + "search.endDate");
        clearMaskedInputValue(prefix  + "search.transferDate");
        $ge(prefix  + "search.agencyFrom.id").selectedIndex = 0;
        $ge(prefix  + "search.agencyTo.id").selectedIndex = 0;
    }
}

function updateBtnsDisplay()
{
    var nbCheck = $('input[name^=\'tsp\']:enabled:checked').length;
    if (nbCheck > 0)
    {
        $('#launchRenewBtn').removeClass('disabled');
        $('#forceUpdate').removeAttr('disabled');
        $('td.isLocked.on').each(function(){
            $(this).removeClass('on').addClass('disabled');
        });
    }
    else
    {
        $('#launchRenewBtn').addClass('disabled');
        $('#forceUpdate').attr('disabled', 'disabled');
        $('td.isLocked.disabled').each(function(){
            $(this).removeClass('disabled').addClass('on');
        });
    }
}

$(document).ready(function(){
    /*
     * Désactive les coches pour les transports verrouillés
     */
    $('tr.locked > td.selection > input').attr('disabled', 'disabled');
    /*
     * Active le bouton de lancement si des transports sont sélectionnés
     */
    $('input[name^=\'tsp\'], input[name=\'cbCheckAll\']').click(function(){
        updateBtnsDisplay();
    });

    /*
     * Sélection des agences par région
     */
    $('#choiceAgencies > table.searchAgencies > tbody > tr.area > td.name').click(function() {
        tabAgencies = $(this).parent().find('td.agencies > span.agency');

        var countActiveAgenciesInArea = $(this).parent().find('td.agencies > span.agency.on').length;
        if (countActiveAgenciesInArea > 0)
        {
            tabAgencies.removeClass('on')
                       .addClass('off');
        }
        else
        {
            tabAgencies.removeClass('off')
                       .addClass('on');
        }
    });

    /*
     * Sélection des agences à l'unité
     */
    $('#choiceAgencies > table.searchAgencies > tbody > tr.area > td.agencies > span.agency').click(function(){
        $(this).toggleClass('on')
               .toggleClass('off');
    });

    /*
     * Afficher le choix des agences
     */
    $('#agenciesSelect > a').click(function(){
        $('#choiceAgencies').css('display', 'block');
    });

    /*
     * Sélection des transports à déverrouiller
     */
    $('td.on > span[id^="tspLocked"], td.off > span[id^="tspLocked"]').click(function(){
        $(this).parent().toggleClass('on off');
        var tspId = $(this).attr('id').substr(10);

        if ($(this).parent().hasClass('on'))
        {
            var index = tabToUnlock.indexOf(tspId);
            if (index > -1)
            {
                tabToUnlock.splice(index, 1);
            }
        }
        else if ($(this).parent().hasClass('off'))
        {
            tabToUnlock.push(tspId);
        }
        if (tabToUnlock.length > 0)
        {
            $('#unlockBtn').removeClass('disabled');
            // Blocage de la mise à jour
            $('tr:not(.locked) > td.selection > input').attr('disabled', 'disabled');
            $('#cbCheckAll\\[\\]').attr('disabled', 'disabled');
        }
        else
        {
            $('#unlockBtn').addClass('disabled');
         // Déblocage de la mise à jour
            $('tr:not(.locked) > td.selection > input').removeAttr('disabled');
            $('#cbCheckAll\\[\\]').removeAttr('disabled');
        }
        updateBtnsDisplay();
    });
});

function displayChoiceAgenciesLabel()
{
 // Récupération des agences sélectionnées
    var tabAgencies = [];
    $('#choiceAgencies > table.searchAgencies > tbody > tr.area > td.agencies > span.agency.on').each(function(){
        tabAgencies.push($(this).html());
    });

    var hasAgenciesSelected = false;
    if (tabAgencies.length > 0)
    {
        hasAgenciesSelected = true;
        $('#agenciesSelect > a.agenciesLabel > span').html(tabAgencies.join(', '));
    }

    $('#agenciesSelect').toggleClass('choice', hasAgenciesSelected);
    $('#choiceAgencies').css('display', 'none');
}