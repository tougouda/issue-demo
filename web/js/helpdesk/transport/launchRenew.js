
var httpObj;
var httpProgressionObj;
var checkProgressionFirstDelay = 1000; // Délai entre le lancement de la mise à jour et la première mise à jour de la progression
var checkProgressionDelay = 1000; // Délai de mise à jour de la progression

/**
 * Lancement de la mise à jour des transports
 */
function renew(httpRequestUrl, httpProgressionUrl)
{
    var button = $('#launchRenewBtn');
    if (button.hasClass('disabled'))
    {
        return false;
    }

    button.addClass('disabled');
    $('#renew-progression').attr('class', '');
    $('#fld-updating').css('display', 'block');
    $('#renew-progression-bar').css('width', 0);

    // Récupération des transports à mettre à jour
    var tab = [];
    $('input[name=tsp]:checked:not(:disabled)').each(function(){
        var val = $(this).val();
        var tspIds = val.split(',');
        for (i = 0 ; i < tspIds.length ; i++)
        {
            if (tspIds[i] != '') 
            {
                tab.push(tspIds[i]);
            }
        }
    });

    var ignoreHash = ($('#forceUpdate').is(':checked') ? 1 : 0);

    var isOnlySelectedCountry = $('#isOnlySelectedCountry').val();
    // Tableau contenant les id des demandes de requêtes de mise à jour 
    var tabRequestId = [];

    $.ajax({
        url: httpRequestUrl,
        type: 'POST',
        cache: false,
        data: JSON.stringify({
                tabTransports: tab,
                ignoreHash: ignoreHash,
                isOnlySelectedCountry : isOnlySelectedCountry
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function(tab){
            for (var prop in tab.requestId)
            {
                tabRequestId.push(tab.requestId[prop]);
            }

            if (!tabRequestId || tabRequestId.length == 0)
            {
                $('#renew-progression').attr('class', 'hidden');
            }
            else
            {
                var val = JSON.stringify(tab.requestId);
                // Planification de la prochaine vérification de la progression
                setTimeout('checkProgression(' + val + ', "' + httpProgressionUrl + '")',
                           checkProgressionFirstDelay);
            }
        },
        error: function(){
            $('#renew-progression').attr('class', 'hidden');
        }
    });
}

/**
 * Vérifie la progression de la mise à jour
 */
function checkProgression(concatRequestId, httpProgressionUrl)
{
    if ($('#fld-updating').css('display') != 'block')
    {
        $('#fld-updating').css('display', 'block');
    }

    $.ajax({
        url: httpProgressionUrl,
        type: 'POST',
        cache: false,
        data: JSON.stringify({
            requestId: concatRequestId
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function(tab){
            // Tableau des états des différentes requêtes de mise à jour
            var tabState = [];
            for (var prop in tab.stateId)
            {
                tabState.push(tab.stateId[prop])
            }

            // Etat terminé
            var nbStateEnded = 0;
            // Etat demandé ou en cours
            var nbStateCurrent = 0;
            for (i = 0 ; i < tabState.length ; i++)
            {
                if (tabState[i] == 'REQ03') 
                {
                    nbStateEnded++;
                }
                else if ((tabState[i] == 'REQ02')|| (tabState[i] == 'REQ01'))
                {
                    nbStateCurrent++;
                }
            }
            // Tableau des progressions des différentes requêtes
            var tabProgression = [];
            for (var prop in tab.progression)
            {
                tabProgression.push(tab.progression[prop]);
            }

            // Tri du tableau par ordre croissant
            tabProgression.sort(function(a, b)
                    {
                        return a -b;
                    });

            $('#renew-progression-bar').css('width', tabProgression[0] + '%');

            if (nbStateCurrent != 0)
            {
                var val = JSON.stringify(concatRequestId);
                // Planification de la prochaine vérification de la progression
                setTimeout('checkProgression(' + val + ', "' + httpProgressionUrl + '")',
                           checkProgressionDelay);
            }
            else
            {
                $('#launchRenewBtn').attr('class', 'locCtrlButton');

              if (nbStateEnded == tabState.length)
              { 
                  $('#renew-progression').addClass('finished');
              }
              else
              {
                  $('#renew-progression').addClass('error');
              }

              var nbCheck = $('input[name^=\'tsp\']:checked').length;
              var isOnlySelectedCountry = $('#isOnlySelectedCountry').val();

              if ((nbCheck > 0) || (isOnlySelectedCountry == 1))
              {
                  $('#launchRenewBtn').removeClass('disabled');
              }
              else
              {
                  $('#launchRenewBtn').addClass('disabled');
              }

            }
    },
        error: function(){
        }
    });
}
function compare(x, y) {
    return x - y;
}