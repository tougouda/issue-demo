/*
 * Fonction de soumission du formulaire 
*/
function submitForm(type)
{

    var form = window.document.forms[0];
    if (type == 'country')
    {
        $ge('check').value = 'changeCountry';
    }
    else if(type == 'valide')
    {
        if ($ge('check').value == '')
        {
            $ge('check').value = 'toCheck';
        }
        else if ($ge('check').value == 'noError')
        {
            $ge('check').value = 'toChange';
        }
    }

    //Image de chargement
    LOC_View.displayLoading(true);
    //Soumission du formulaire
    form.submit();

   return false;
}

$(document).ready(function()
{
    $('[id^="toDateChange"]').each(function(i)
    {
        $('#divDateChange\\[' + $(this).val() + '\\]').addClass('noChange');
        $(this).change(function(i)
        {
            $('[id^="toDateChange"]').each(function(i)
            {
                $('#divDateChange\\[' + $(this).val() + '\\]').addClass('noChange');
            });
            $('#divDateChange\\[' + $(this).val() + '\\]').removeClass('noChange');
        });
        
    });
});