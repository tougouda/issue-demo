if (!window.Location)
{
    var Location = new Object();
}

if (!Location.commonObj) alert('Erreur Location');


/* Classe Location.SearchBox */
Location.SearchBox = function(pluginObj, name, tabOptions)
{
    this._pluginObj = pluginObj;
    this.name = name;
    this._spanEl = null;
    this._el = null;
    this._isDisabled = false;

    this._timer = null;
    this._tabOptions = (tabOptions ? tabOptions : {});
    this._delay = (this._tabOptions['delay'] ? this._tabOptions['delay'] : 750);
    this._isFirstSearch = true;
    this._oldSearchValue = (this._tabOptions['searchValue'] ? this._tabOptions['searchValue'] :
                            (this._tabOptions['search'] ? this._tabOptions['search'] : ''));
    this.oncreate = this._tabOptions['oncreate'];
    this._returnCode = 'ok';
    this._title = '';
    this._isRequired = false;
    this._emptyValue = '';
    this._docFragment = null;
    this._nullItemEl = null;
    this._selectedItemEl = null;
    this._httpObj = null;

    // Evénements
    this.onchange        = null;
    this.onkeydown       = null;
    this.onsearchchange  = null;
    this.onsearchkeydown = null;
    this.onsearchkeyup   = null;
    this.onsearchpaste   = null;
    this.onsearchstart   = null;
    this.onsearchend     = null;

    // Initialisation
    this._init();
}

Location.SearchBox.prototype = {

    /**
     * Mettre le focus sur l'élément
     *
     * @return bool
     */
    blur : function()
    {
        if (this._isDisabled)
        {
            return false;
        }
        this._inputEl.blur();
        this._el.blur();
        return true;
    },

    /**
     * Vider les éléments de la searchBox
     *
     * @return bool
     */
    clear : function()
    {
        this._inputEl.value = '';
        this._oldSearchValue = '';
        this._nullItemEl = null;

        var nb = this._el.options.length;
        while (nb > 0)
        {
            nb--;
            this._el.removeChild(this._el.options[nb]);
        }

        return true;
    },

    /**
     * Supprimer tous les éléments de la liste de valeur
     *
     */
    clearElements : function()
    {
        var nb = this._el.options.length;
        while (nb > 0)
        {
            nb--;
            // Ne supprime pas l'élement sélectionné
            if (nb == this._el.selectedIndex)
            {
                this._el.options[nb].style.fontStyle = 'italic';
                this._el.options[nb].style.borderBottom = '1px solid gray';
            }
            else
            {
                this._el.removeChild(this._el.options[nb]);
            }
        }
    },

    /**
     * Fonction  (par défaut) de création d'un élément pour la liste
     *
     * @param string key
     * @param mixed  value
     */
    createElement : function(key, value)
    {
        return new Option(value, key);
    },

    /**
     * Désactiver la searchBox
     *
     * @return bool
     */
    disable : function()
    {
        if (this._isDisabled)
        {
            return false;
        }
        this._isDisabled = true;
        this._spanEl.className = this._getClassName(false);
        this._inputEl.disabled = true;
        this._el.disabled = true;
        return true;
    },

    /**
     * Afficher/Cacher l'élément vide
     *
     */
    displayNullItem : function(isDisplayed, value, text)
    {
        if (isDisplayed)
        {
            if (!this._nullItemEl)
            {
                this._nullItemEl = new Option((text || '---'), value);
                this._nullItemEl._searchBoxOrderIndex = -1;
                var beforeEl = (this._el.options.length > 0 ? this._el.options[0] : null);
                this._el.add(this._nullItemEl, beforeEl);
            }
        }
        else if (this._nullItemEl)
        {
            var parentEl = this._nullItemEl.parentNode;
            if (parentEl.nodeName.toLowerCase() == 'select')
            {
                // L'élément est dans le select
                parentEl.remove(this._nullItemEl);
            }
            else
            {
                // L'élément est dans le documentFragment
                parentEl.removeChild(this._nullItemEl);
            }
            this._nullItemEl = null;
        }
    },

    /**
     * Activer la searchBox
     *
     * @return bool
     */
    enable : function()
    {
        if (!this._isDisabled)
        {
            return false;
        }
        this._isDisabled  = false;
        this._spanEl.className = this._getClassName(false);
        this._inputEl.disabled = false;
        this._el.disabled = false;
        return true;
    },

    /**
     * Mettre le focus sur l'élément
     *
     * @return bool
     */
    focus : function(elIndex)
    {
        if (this._isDisabled)
        {
            return false;
        }
        if (!elIndex)
        {
            this._inputEl.focus();
        }
        else
        {
            this._el.focus();
        }
        return true;
    },

    /**
     * Récupérer l'élément correspondant à la zone de recherche
     *
     * @return Input
     */
    getInputElement : function()
    {
        return this._inputEl;
    },

    /**
     * Récupérer l'élément correspondant à la liste déroulante
     *
     * @return Select
     */
    getSelectElement : function()
    {
        return this._el;
    },

    /**
     * Récupérer l'option sélectionnée de la liste
     *
     * @return Option
     */
    getSelectedOption : function()
    {
        if (this._el.selectedIndex != -1)
        {
            return this._el.options[this._el.selectedIndex];
        }
        return null;
    },

    /**
     * Récupérer la valeur
     *
     * @return string
     */
    getValue : function()
    {
        return this._el.value;
    },

    /**
     * Récupérer la valeur de recherche
     *
     * @return string Valeur
     */
    getSearchValue : function(value)
    {
        return this._inputEl.value;
    },

    /**
     * Indique si l'élément est désactivé
     *
     * @return bool
     */
    isDisabled : function()
    {
        return this._isDisabled;
    },

    /**
     * Fonction (par défaut) de comparaison des éléments
     *
     * @param string searchValue Valeur recherchée
     * @param string value       Chaîne
     * @return bool
     */
    search : function(searchValue, str)
    {
        searchValue = searchValue.toLowerCase();
        if (str == null)
        {
            str = '';
        }

        var tabSearch = searchValue.split(' ');

        for (var i in tabSearch)
        {
            // Transformation en expression rationnelle
            // Recherche du type %XXX -> XXX($|\n)
            if (/^%/.test(tabSearch[i]) && !/%$/.test(tabSearch[i]))
            {
                tabSearch[i] += '($|\n)';
            }
            // Recherche du type XXX% -> (^|\n)XXX
            if (!/^%/.test(tabSearch[i]) && /%$/.test(tabSearch[i]))
            {
                tabSearch[i] = '(^|\n)' + tabSearch[i];
            }
            // Suppression des %
            tabSearch[i] = tabSearch[i].replace('%', '');
            // Echappement du caractère *
            tabSearch[i] = tabSearch[i].replace('*', '/\*/');

            var reg = new RegExp(tabSearch[i], 'i');
            if (!reg.test(str))
            {
                return false;
            }
        }

        return true;
    },

    /**
     * Définir la fonction de création d'un élément
     *
     * @param function createEltFunc Fonction de comparaison
     */
    setCreateElementCallback : function(createEltFunc)
    {
        this.createElement = createEltFunc;
    },

    /**
     * Récupérer la valeur
     *
     * @return string
     */
    setDelay : function(delay)
    {
        this._delay = delay;
    },

    /**
     * Désactiver/Activer la searchBox
     *
     * @param bool isDisabled
     * @return bool
     */
    setDisabled : function(isDisabled)
    {
        if (isDisabled)
        {
            return this.disable();
        }
        else
        {
            return this.enable();
        }
    },

    /**
     * Définir les élements de la liste de valeur
     *
     * @param Object tabElements
     * @param format Format de la liste
     */
    setElements : function(tabElements, format)
    {
        this.clearElements();
        if (format == 1) // Format du type [[id, valeur][id, valeur]...]
        {
            for (var key in tabElements)
            {
                this._el.add(new Option(tabElements[key][1], tabElements[key][0]), null);
            }
        }
        else // Format libre
        {
            for (var key in tabElements)
            {
                this._el.add(this.createElement(key, tabElements[key]), null);
            }
        }
    },

    /**
     * Définir la zone de recherche comme requise
     *
     * @param bool isRequired
     * @param bool isFocus
     */
    setRequired : function(isRequired, isFocus)
    {
        isRequired = (isRequired ? true : false);
        if (this._isRequired == isRequired)
        {
            return false;
        }
        this._isRequired = isRequired;
        if (!this._isDisabled && isFocus && this._isRequired)
        {
            this._inputEl.focus();
        }
        this._spanEl.className = this._getClassName(false);
        return true;
    },

    /**
     * Définir la fonction de comparaison
     *
     * @param function func Fonction de comparaison
     */
    setSearchCallback : function(searchFunc)
    {
        this.search = searchFunc;
    },

    /**
     * Définir la valeur de recherche
     *
     * @param string value Valeur
     * @return bool
     */
    setSearchValue : function(value)
    {
        if (this._inputEl.value == value)
        {
            return false;
        }
        this._inputEl.value = value;
        this._goSearch(value, 0);
        return true;
    },

    /**
     * Récupérer la valeur
     *
     * @return string
     */
    setUrl : function(url)
    {
        if (!this._httpObj)
        {
            // Instanciation de l'objet httpRequest
            this._httpObj = new Location.HTTPRequest(url);
            this._isFirstSearch = true;
        }
        else
        {
            // Modification de l'URL
            this._httpObj.setFile(url);
        }
    },

    /**
     * Définir la valeur
     *
     * @param string value          Valeur
     * @param bool   isEventActived Activer l'évenement onchange ?
     */
    setValue : function(value, isEventActived)
    {
        if (typeof isEventActived == 'undefined')
        {
            isEventActived = true;
        }

        var oldValue = this._el.value;
        this._el.value = value;
        if (!this._httpObj && this._el.value != value)
        {
            this.setSearchValue('');
            this._el.value = value;
        }
        if (isEventActived && this._el.onchange && this._el.value != oldValue)
        {
            this._el.onchange();
        }
    },

    _endSearch : function(returnCode, title, oldValue)
    {
        if (typeof returnCode != 'undefined')
        {
            this._returnCode = returnCode;
        }
        if (typeof title != 'undefined')
        {
            this._title = title;
        }

        // Style par défaut
        this._spanEl.className = this._getClassName(false);
        this._inputEl.title = this._title;
        this._el.disabled = false;

        // Evènement de fin de recherche
        if (this.onsearchend)
        {
            this.onsearchend();
        }

        if (typeof oldValue != 'undefined')
        {
            // Force la même valeur qu'avant le filtre
            this._el.value = oldValue;
            if (this._el.selectedIndex == -1 && this._el.options.length > 0)
            {
                this._el.selectedIndex = 0;
            }

            // Déclenchement de l'événement 'onChange'
            if (this._el.onchange && oldValue != this._el.value)
            {
                this._el.onchange();
            }
        }
    },

    /**
     * Recherche dans la liste déjà définie
     *
     * @param string value Chaîne à rechercher
     * @param int    time  Temps d'attente avant recherche (facultatif)
     */
    _goSearch : function(searchValue, time)
    {
        if (this._isDisabled)
        {
            return false;
        }
        var mySelf = this;
        searchValue = searchValue.toLowerCase();

        // Désactivation du timer
        if (this._timer)
        {
            clearTimeout(this._timer);
            this._timer = null;
        }

        // Evènement de lancement de recherche
        if (this.onsearchstart)
        {
            this.onsearchstart();
        }

        if (!this._isFirstSearch && searchValue == this._oldSearchValue)
        {
            this._endSearch();
            return;
        }

        // Style pour le chargement
        this._spanEl.className = this._getClassName(true);
        this._el.disabled = true;

        // Lancement en décalé
        if (time && time > 0)
        {
            // Délai d'attente pour la recherche
            this._timer = window.setTimeout(function(){mySelf._goSearch(searchValue, 0);}, time);
        }
        else
        {
            // Sauvegarde de la valeur courante de la liste
            var oldValue = this.getValue();

            // Requête HTTP ou liste statique ?
            if (this._httpObj)
            {
                if (this._httpObj.isActive())
                {
                    this._httpObj.abort();
                    this._spanEl.className = this._getClassName(true);
                }

                this._httpObj.setVar('searchValue', searchValue);
                this._httpObj.oncompletion = function()
                {
                    var tabResults = this.getJSONResponse();

                    if (tabResults)
                    {
                        mySelf._oldSearchValue = searchValue;
                        mySelf._isFirstSearch = false;

                        if (tabResults['returnCode'] == 'warning')
                        {
                            mySelf._endSearch('warning', tabResults['title'], oldValue);
                        }
                        else if (tabResults['returnCode'] == 'ok')
                        {
                            var returnCode = 'error';
                            for (var prop in tabResults['tabData'])
                            {
                                returnCode = 'ok';
                                break;
                            }
                            mySelf.setElements(tabResults['tabData'], tabResults['format']);
                            mySelf._endSearch(returnCode, tabResults['title'], oldValue);
                        }
                        else
                        {
                            mySelf._endSearch('error', tabResults['title'], oldValue);
                        }

                    }
                    else
                    {
                        this.onerror();
                    }
                }
                this._httpObj.onerror = function()
                {
                    mySelf._endSearch('error', '', oldValue);
                }
                this._httpObj.send(true);
            }
            else
            {
                // Sauvegarde de la valeur courante de la liste
                this._oldSearchValue = searchValue;
                var isFirst = this._isFirstSearch;
                this._isFirstSearch = false;

                // Sauvegarde de tous les élements
                var isEmptySearch = (searchValue == '' || searchValue == this._emptyValue);
                var i, curEl;
                var nb = this._el.options.length;
                if (this._selectedItemEl)
                {
                    this._selectedItemEl.style.fontStyle = '';
                    this._selectedItemEl.style.borderBottom = '';
                    this._selectedItemEl = null;
                }
                while (nb > 0)
                {
                    nb--;
                    curEl = this._el.options[nb];
                    if (isFirst)
                    {
                        curEl._searchBoxOrderIndex = nb;
                    }
                    // Ne déplace pas l'élement sélectionné
                    if (curEl != this._nullItemEl)
                    {
                        if (!isEmptySearch && nb == this._el.selectedIndex)
                        {
                            curEl.style.fontStyle = 'italic';
                            curEl.style.borderBottom = '1px solid gray';
                            this._selectedItemEl = curEl;
                        }
                        else
                        {
                            this._docFragment.appendChild(curEl);
                            curEl.removeAttribute('selected');
                        }
                    }
                }

                // Sélection des éléments à afficher dans la liste
                var curObj = this._docFragment.firstChild;
                var tab = [];
                var searchTarget;
                var nbResults = 0;
                // On compte l'élément déjà sélectionné si il correspond à la recherche
                if (this._selectedItemEl)
                {
                    searchTarget = (this._selectedItemEl.getAttribute('_search') || this._selectedItemEl.innerHTML);
                    if (isEmptySearch || this.search(searchValue, searchTarget))
                    {
                        nbResults++;
                    }
                }
                // Recherche des autres élements correspondant à la recherche
                while (curObj)
                {
                    searchTarget = (curObj.getAttribute('_search') || curObj.innerHTML);
                    if (isEmptySearch || this.search(searchValue, searchTarget))
                    {
                        tab.push(curObj);
                        nbResults++;
                    }
                    curObj = curObj.nextSibling;
                }

                // Affichage
                var i, j, index;
                for (i = 0; i < tab.length; i++)
                {
                    index = Number(tab[i]._searchBoxOrderIndex);
                    j = 0;
                    while (j < this._el.options.length &&
                           ((!isEmptySearch && this._selectedItemEl == this._el.options[j]) ||
                            index > Number(this._el.options[j]._searchBoxOrderIndex)))
                    {
                        j++;
                    }
                    if (j >= this._el.options.length)
                    {
                        this._el.appendChild(tab[i]);
                    }
                    else
                    {
                        this._el.insertBefore(tab[i], this._el.options[j]);
                    }
                }

                // Si l'élement NULL est sélectionné alors on choisi le premier élément de la liste
                var isAutoSelected = false;
                if (this._el.selectedIndex != -1 &&
                    this._el.options[this._el.selectedIndex] == this._nullItemEl)
                {
                    i = 0;
                    nb = this._el.options.length;
                    while (i < nb && this._el.options[i] == this._nullItemEl)
                    {
                        i++;
                    }
                    if (this._el.options[i])
                    {
                        this._el.selectedIndex = i;
                        this._endSearch((nbResults > 0 ? 'ok' : 'error'), '');
                        if (this._el.onchange)
                        {
                            this._el.onchange();
                        }
                        isAutoSelected = true;
                    }
                }

                if (!isAutoSelected)
                {
                    this._endSearch((nbResults > 0 ? 'ok' : 'error'), '', oldValue);
                }
            }
        }
    },

    _getClassName : function(isLoading)
    {
        var className = 'locPlgSearchBoxesElt';
        if (isLoading)
        {
            className += ' loading';
        }
        else
        {
            if (this._returnCode != 'ok')
            {
                className += ' ' + this._returnCode;
            }
            if (this._isRequired)
            {
                className += ' required';
            }
        }
        return className;
    },

    /**
     * Constructeur de la classe
     *
     */
    _init : function()
    {
        var mySelf = this;

        this._el = window.document.getElementById(this.name);
        if (!this._el)
        {
            return;
        }

        this._spanEl = window.document.createElement('SPAN');
        this._spanEl.className = this._getClassName(false);
        this._el.parentNode.insertBefore(this._spanEl, this._el);
        this._spanEl.appendChild(this._el);

        this._isDisabled = this._el.disabled;
        this._inputEl = window.document.createElement('INPUT');
        this._inputEl.id = this._el.id + '.search';
        this._inputEl.type = 'text';
        this._inputEl.disabled = this._isDisabled;
        this._inputEl.value = this._oldSearchValue;
        this._el.parentNode.insertBefore(this._inputEl, this._el);
        if (this._el.tabIndex != 0)
        {
            this._inputEl.tabIndex = this._el.tabIndex;
            this._el.tabIndex++;
        }

        // Evénement onkeydown
        this._el._oldonkeydown = this._el.onkeydown;
        this._el.onkeydown = function(e)
        {
            if (!mySelf._isDisabled)
            {
                e = e || window.event;
                var keyCode = (e.keyCode || e.which);

                if (!((keyCode >= 33 && keyCode <= 40) || keyCode == 13 || keyCode == 9))
                {
                    var cursorPos = (keyCode == 46 ? 0 : -1);
                    mySelf._inputEl.focus();
                    Location.commonObj.setCursorPos(mySelf._inputEl, cursorPos);
                    return true;
                }

                var result = true;
                if (this._oldonkeydown && this._oldonkeydown() == false)
                {
                    result = false;
                }
                if (mySelf.onkeydown && mySelf.onkeydown(e) == false)
                {
                    result = false;
                }
                return result;
            }
            return false;
        }

        // Evénement onchange
        this._el._oldonchange = this._el.onchange;
        this._el.onchange = function(e)
        {
            if (!mySelf._isDisabled)
            {
                var result = true;
                if (this._oldonchange && this._oldonchange(e) == false)
                {
                    result = false;
                }
                if (mySelf.onchange && mySelf.onchange(e) == false)
                {
                    result = false;
                }
                return result;
            }
            return false;
        }

        // Evénement onkeydown
        this._inputEl._oldonkeydown = this._inputEl.onkeydown;
        this._inputEl.onkeydown = function(e)
        {
            if (!mySelf._isDisabled)
            {
                e = e || window.event;
                var keyCode = (e.keyCode || e.which);

                if (keyCode == 13)
                {
                    mySelf._goSearch(this.value, 0);
                    return false;
                }
                else if (keyCode >= 33 && keyCode <= 40 && keyCode != 35 && keyCode != 36 &&
                                                           keyCode != 37 && keyCode != 39)
                {
                    mySelf._el.focus();
                }

                var result = true;
                if (this._oldonkeydown && this._oldonkeydown(e) == false)
                {
                    result = false;
                }
                if (mySelf.onsearchkeydown && mySelf.onsearchkeydown(e) == false)
                {
                    result = false;
                }
                return result;
            }
            return false;
        }

        // Evénement onkeyup
        this._inputEl._oldonkeyup = this._inputEl.onkeyup;
        this._inputEl.onkeyup = function(e)
        {
            if (!mySelf._isDisabled)
            {
                e = e || window.event;
                var keyCode = (e.keyCode || e.which);
                if (keyCode != 9)
                {
                    mySelf._goSearch(this.value, mySelf._delay);
                }

                var result = true;
                if (this._oldonkeyup && this._oldonkeyup(e) == false)
                {
                    result = false;
                }
                if (mySelf.onsearchkeyup && mySelf.onsearchkeyup(e) == false)
                {
                    result = false;
                }
                return result;
            }
            return false;
        }

        // Evénement onchange
        this._inputEl._oldonchange = this._inputEl.onchange;
        this._inputEl.onchange = function(e)
        {
            var result = true;
            if (this._oldonchange && this._oldonchange(e) == false)
            {
                result = false;
            }
            if (mySelf.onsearchchange && mySelf.onsearchchange(e) == false)
            {
                result = false;
            }
            return result;
        }
        this._inputEl._oldonpaste = this._inputEl.onpaste;
        this._inputEl.onpaste = function(e)
        {
            if (this._oldonpaste && this._oldonpaste(e) == false)
            {
                return false;
            }
            if (mySelf.onsearchpaste && mySelf.onsearchpaste(e) == false)
            {
                return false;
            }

            window.setTimeout(function() { mySelf._goSearch(mySelf._inputEl.value, mySelf._delay); }, 50);

            return true;
        }

        // DocumentFragment
        this._docFragment = window.document.createDocumentFragment();
        window.document.body.appendChild(this._docFragment);

        // Fonction exécutée à la creation de la SearchBox
        if (this.oncreate)
        {
            this.oncreate();
        }
        // Liste prédéfinie
        if (this._tabOptions['url'])
        {
            // URL de la requête Http
            this.setUrl(this._tabOptions['url']);
        }
    }
}

/**
 * Objet Location.searchBoxesManager
 *
 */
Location.searchBoxesManager = {

    _tabObj : [],

    /**
     * Créer une zone de recherche
     *
     * @param string name       Nom
     * @param array  tabOptions Options
     * @return Location.SearchBox Instance du groupe d'onglets créé
     */
    createSearchBox : function(name, tabOptions)
    {
        if (this._tabObj[name])
        {
            return this._tabObj[name];
        }

        this._tabObj[name] = new Location.SearchBox(this, name, tabOptions);
        return this._tabObj[name];
    },

    /**
     * Récupérer l'instance d'une zone de recherche
     *
     * @param string name Nom de la zone de recherche
     * @return Location.SearchBox
     */
    getSearchBox : function(name)
    {
        if (!this._tabObj[name])
        {
            return null;
        }
        return this._tabObj[name];
    }
}
