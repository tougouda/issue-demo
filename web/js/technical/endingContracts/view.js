
var transportsUI = {

    nbConsidersMax: 0,
    list : [],

    init : function(tabOptions)
    {
        var mySelf = this;

        this.nbConsidersMax = tabOptions.nbConsidersMax;
        this.list = tabOptions.list;


        // Mises à jour lors du scroll ou le redimensionnement de la fenêtre
        var scrollFunc = function()
        {
            var content = $('#locStandardContent');
            var toolbar = $('#table-toolbar-container');

            if (toolbar.parent().position().top < 0)
            {
                $('html > body').addClass('table-toolbar-fixed');

                var padding = parseInt(toolbar.css('padding-left')) + parseInt(toolbar.css('padding-right'));
                toolbar.offset({top : content.offset().top}).
                        width(content[0].clientWidth - padding);
            }
            else
            {
                $('html > body').removeClass('table-toolbar-fixed');
                toolbar.width('');
            }
        };

        $('#locStandardContent').scroll(scrollFunc);
        $(window).resize(scrollFunc);

        // Bouton pour revenir en haut de page
        $('#back-top-btn').click(function() {
            $('#locStandardContent').animate({scrollTop: 0}, 800);
            return false;
        });


        $("#table-main").bind("sortEnd", function(sorter) {
            currentSort = sorter.target.config.sortList;
        });

        // Bouton pour le mode d'affichage
        var detachedRows = null;
        $('#viewmode-btn').click(function() {

            var wrapperEl = $('#table-wrapper');
            if (!$(this).hasClass('disabled') && wrapperEl.hasClass('viewmode-full'))
            {
                wrapperEl.removeClass('viewmode-full').addClass('viewmode-checked');

                detachedRows = $('#table-main > tbody > tr:not(.updated)').detach();
                $("#table-main").trigger("update");
            }
            else if (detachedRows)
            {
                wrapperEl.removeClass('viewmode-checked').addClass('viewmode-full');

                detachedRows.appendTo('#table-main > tbody');
                detachedRows = null;
                $("#table-main").trigger("update");
            }
            return false;
        });

        // --------------
        //   Evènements
        // --------------

        // - Boutons "Prendre en compte"
        $('div.consider-btn > div').click(function() {

            var action = this.className;
            var tsp = mySelf.get($(this).parent().attr('data-tspid'));

            if (tsp && !$(this).parent().hasClass('disabled-' + action))
            {
                var nb = mySelf.list.filter(function(tsp) {
                    return tsp.considerAction;
                }).length;

                if (tsp.considerAction || mySelf.nbConsidersMax <= 0 || nb < mySelf.nbConsidersMax)
                {
                    tsp.considerAction = (tsp.considerAction == action ? false : this.className);
                    mySelf.display();
                    LOC_View.displayNotification("max-considers-reached", "hide", {});
                }
                else
                {
                    LOC_View.displayNotification("max-considers-reached", "show", {});
                }
            }
        });

        // Notification pour le dépassement du compteur horaire
        $('.input-timeCounter').change(function() {
            var tspId = $(this).attr('data-tspid');
            var min   = $(this).attr('data-min');
            var max   = $(this).attr('data-max');
            var iconWarningMin = $('#tsp_' + tspId + ' > .timeCounter > .warning > .warning-min');
            var iconWarningMax = $('#tsp_' + tspId + ' > .timeCounter > .warning > .warning-max');

            var value = parseFloat($(this).val());
            iconWarningMin.toggle(value < min);
            iconWarningMax.toggle(value > max);
        });

        // Bouton de prise en compte
        $('#do-btn').click(function() {

            if (!$(this).hasClass('disabled'))
            {
                var tabActions = [];
                var nb = mySelf.list.length;
                for (var i = 0; i < nb; i++)
                {
                    var tsp = mySelf.list[i];

                    if (tsp.considerAction)
                    {
                        tabActions.push({
                            id           : tsp.id,
                            parkNumber   : tsp.parkNumber,
                            timeCounter  : parseFloat($('#timeCounter_' + tsp.id).val()),
                            considerType : (tsp.considerAction == 'kim' ? CONSIDERTYPE_KIMOCE : CONSIDERTYPE_NOKIMOCE)
                        });
                    }
                }

                if (tabActions.length > 0)
                {
                    LOC_View.displayLoading(true);
                    $(this).addClass('disabled');
                    $('#valid').val(JSON.stringify({
                        valid   : 'ok',
                        actions : tabActions
                    }));
                    $('form').submit();
                }
            }

        });

        this.display();
    },

    get : function(id)
    {
        var i = 0;
        var nb = this.list.length;
        while (i < nb && this.list[i].id != id)
        {
            i++;
        }
        return (i < nb ? this.list[i] : false);
    },

    display : function()
    {
        var nbUpdated = 0;
        var considersRecap = {
            kim : [],
            nokim : []
        };

        var nb = this.list.length;
        for (var i = 0; i < nb; i++)
        {
            var tsp = this.list[i];
            var trEl = $('#tsp_' + tsp.id);
            var btnEl = trEl.find('div.consider-btn');

            if (tsp.considerAction)
            {
                nbUpdated++;
                trEl.addClass('updated');
                btnEl.attr('data-value', tsp.considerAction);

                considersRecap[tsp.considerAction].push(tsp.parkNumber);
            }
            else
            {
                trEl.removeClass('updated');
                btnEl.attr('data-value', tsp.considerAction);
            }
        }

        // Récapitulatifs des lignes à prendre en compte
        $('#header-updated-recap-kim').css('visibility', (considersRecap.kim.length > 0 ? 'visible' : 'hidden')).
                                       html(considersRecap.kim.sort().join(', '));
        $('#header-updated-recap-nokim').css('visibility', (considersRecap.nokim.length > 0 ? 'visible' : 'hidden')).
                                         html(considersRecap.nokim.sort().join(', '));

        // Nombre de ligne à prendre en compte
        $('#do-checked').html((nbUpdated > 0 ? '(' + nbUpdated + ')' : ''));

        // Bouton de prise en compte
        $('#do-btn').toggleClass('disabled', (nbUpdated == 0));

        // Bouton de mode d'affichage
        $('#viewmode-btn').toggleClass('disabled', (nbUpdated == 0 || nbUpdated == nb));
        if (nbUpdated == 0)
        {
            $('#table-wrapper').removeClass('viewmode-checked').addClass('viewmode-full');
        }
    }
};