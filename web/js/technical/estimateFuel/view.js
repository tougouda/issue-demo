/**
 * Vérifie si on met à jour la qte de carburant
 * @param idLine       ligne à vérifier
 * @param oldValue     ancienne valeur
 * @param newValue     nouvelle valeur
 */
function isUpdateEstimateFuel(idLine, oldValue, newValue)
{
    var lineToUpdate = document.getElementById('imgIsUpdate_' + idLine);
    var lineError    = document.getElementById('imgIsError_' + idLine);

    if (oldValue != newValue || newValue == '')
    {
        lineToUpdate.className = 'update';
        lineError.className    = 'noError';
    }
    else
    {
        lineToUpdate.className = 'noUpdate';
    }
}

function setUpdatesRow(rowEl, tabRefValue, isForcedToUpdate)
{
    // Récupération des valeurs des champs
    qtyByDay     = parseFloat(rowEl.children[1].children[0].value, 2);
    qtyMax       = parseFloat(rowEl.children[2].children[0].value, 2);
    
    
    qtyByDay = (qtyByDay ? qtyByDay : '');
    qtyMax   = (qtyMax ? qtyMax : '');

    if (isForcedToUpdate || 
        qtyByDay != tabRefValue.qtyByDay ||
        qtyMax != tabRefValue.qtyMax)
    {
        LOC_Common.removeEltClass(rowEl, "isError");
        LOC_Common.removeEltClass(rowEl, "isUpdated");
        LOC_Common.addEltClass(rowEl, "isUpdate");
        rowEl.children[0].children[0].value = 1;
    }
    else
    {
    	LOC_Common.removeEltClass(rowEl, "isUpdate");
        rowEl.children[0].children[0].value = 0;
        if (tabRefValue.state != '')
        {
            LOC_Common.addEltClass(rowEl, tabRefValue.state);
        }
        if (LOC_Common.hasEltClass(rowEl, 'isError'))
        {
            rowEl.children[0].children[0].value = 1;
        }
    }
}

/*
 * Fonction de soumission du formulaire 
*/
function submitForm()
{

    var form = window.document.forms[0];
    $ge('valid').value = 'ok';

    //Image de chargement
    LOC_View.displayLoading(true);
    //Soumission du formulaire
    form.submit();

   return false;

 
}