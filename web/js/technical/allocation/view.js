var tabMachineAllocCfgs = [];

/*
 * Fonction de soumission du formulaire 
*/
function submitForm()
{
    var form = window.document.forms[0];
    $ge('valid').value = 'ok';
    //Image de chargement
    LOC_View.displayLoading(true);
    //Soumission du formulaire
    form.submit();

   return false;
}


/**
 * Indique si une attribution est en cours ou non
 *
 * @returns bool
 */
function hasMachineAlloc()
{
    var i = 0;
    var cfgsCount = tabMachineAllocCfgs.length;
    while (i < cfgsCount && !tabMachineAllocCfgs[i].machineId)
    {
        i++;
    }
    return (i < cfgsCount);
}


/**
 * Attribution de la machine sur une ligne de contrat
 *
 * @param int contractId Id du contrat
 * @param int machineId  Id de la machine
 * @return bool
 */
function setMachineId(contractId, machineId)
{
    var i = 0;
    var cfgsCount = tabMachineAllocCfgs.length;
    while (i < cfgsCount && tabMachineAllocCfgs[i].id != contractId)
    {
        i++;
    }
    if (i < cfgsCount)
    {
        machineId = Number(machineId);
        if (tabMachineAllocCfgs[i].machineId != machineId)
        {
            // On s'assure que la machine est attribuable pour ce contrat
            if (machineId != 0)
            {
                var j = 0;
                var tabMachines = tabMachineAllocCfgs[i].tabMachines;
                var machinesCount = tabMachines.length;
                while (j < machinesCount && (!tabMachines[j].isActived || tabMachines[j].id != machineId))
                {
                    j++;
                }
                if (j >= machinesCount)
                {
                    return false;
                }
            }

            // On désactive les numéros de parcs identiques pour les autres contrat et on réactive le précédent si c'est nécessaire
            for (var k = 0; k < cfgsCount; k++)
            {
                if (k != i)
                {
                    tabMachines = tabMachineAllocCfgs[k].tabMachines;
                    machinesCount = tabMachines.length;
                    for (j = 0; j < machinesCount; j++)
                    {
                        if (tabMachines[j].id == tabMachineAllocCfgs[i].machineId)
                        {
                            tabMachines[j].isActived = true;
                        }
                        else if (tabMachines[j].id == machineId)
                        {
                            tabMachines[j].isActived = false;
                        }
                    }
                }
            }

            tabMachineAllocCfgs[i].machineId = machineId;

            displayMachineAlloc();
            return true;
        }
    }
    return false;
}

/**
 * Mise à jour de l'affichage des listes de machines sur chacunes des lignes de contrat
 *
 * @return void
 */
function displayMachineAlloc()
{
    // Pour chaque ligne de contrat:
    // - on vide la select
    // - on y réinsère les machines associées qui ne sont pas éventuellement attribuées sur une autre ligne (isActived à true)
    // - on met à jour l'affichage de la liste (verte ou bleue)
    var cfgsCount = tabMachineAllocCfgs.length;
    for (var i = 0; i < cfgsCount; i++)
    {
        var tab = tabMachineAllocCfgs[i];
        var el = $ge('machineList_' + tab.id);

        while (el.options[0])
        {
            el.removeChild(el.options[0]);
        }
        el.add(new Option('------', 0), null);

        var machinesCount = tab.tabMachines.length;
        for (var j = 0; j < machinesCount; j++)
        {
            var machineId = tab.tabMachines[j].id;
            if (tab.tabMachines[j].isActived)
            {
                var optEl = new Option(tab.tabMachines[j].label, machineId);
                el.add(optEl, null);
                if (tab.tabMachines[j].isWished)
                {
                    optEl.className = 'wished';
                }
            }
        }
        el.value = tab.machineId;
        el.parentNode.className = 'parkNumber' + (tab.machineId ? ' ok' : ' none');
    }
}


var wishedMachineTimer = null;
/**
 * Afficher l'infobulle de la machine souhaitée
 *
 * @param elt
 * @param time
 */
function displayWishedMachineTooltip(elt, time)
{
    elt.className = 'wished on';
    if (wishedMachineTimer)
    {
        window.clearTimeout(wishedMachineTimer);
    }
    wishedMachineTimer = window.setTimeout(function(){ elt.className = 'wished'; }, time);
}

