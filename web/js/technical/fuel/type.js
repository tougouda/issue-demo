
/*
 * Fonction calcul carburant pour les contrats cadres
*/
function calcul_fuel(i)
{

    //Prix de base
    var pxbase = $ge('fuelUnitPrice').value;

    //Prix au litre du carburant du contrat cadre concerné
    var carbuPrice = $ge('calculate[' + i + '][fuelAmount]').value;

    //Montant saisi dans le champ texte
    var nbLitre = $ge('fuel[' + i + '][qty]').value;

    //Quantité au litre devant être saisie
    var quantity = Math.round((nbLitre * carbuPrice) / Number(pxbase));

    //Mise à jour du champ Quantité Carburant
    $ge('fuel[' + i + '][qty]').value = quantity;
}


/*
 * Fonction de soumission du formulaire 
*/
function submitForm()
{

    var form = window.document.forms[0];
    $ge('valid').value = 'ok';

    //Image de chargement
    LOC_View.displayLoading(true);
    //Soumission du formulaire
    form.submit();

   return false;

 
}

function checkFuelEstimate(line, carburant)
{
    //valeur de l'estimation
    var fuelEstimate = $ge('estimate_' + line).value;
    carburant = parseFloat(carburant);
    if (carburant != fuelEstimate)
    {
        $ge('estimateWarning_' + line).style.visibility = 'visible';
    }
    else
    {
        $ge('estimateWarning_' + line).style.visibility = 'hidden';
    }
    
}