var contracts = {

    type : null,
    userId : null,
    ids : [],
    _urlsHttpObj : null,


    /**
     * Ajout d'un contrat à éditer
     */
    add : function(id)
    {
        if (this.ids.indexOf(id) == -1)
        {
            this.ids.push(id);
        }
    },


    /**
     * Suppression d'un contrat à éditer
     */
    remove : function(id)
    {
        var index = this.ids.indexOf(id);
        if (index != -1)
        {
            this.ids.splice(index, 1);

            // État des boutons d'impression
            this._updatePrintBtnsStatus();
        }
    },


    /**
     * Fonction de soumission du formulaire
     */
    submitForm : function()
    {
        // Image de chargement
        LOC_View.displayLoading(true);

        // Soumission du formulaire
        window.document.location.reload();

        return false;
    },


    /**
     * Initialisation
     */
    init : function(type, userId)
    {
        this.type = type;
        this.userId = userId;

        // État des boutons d'impression
        this._updatePrintBtnsStatus();

        // Intéractions sur les lignes du tableau
        $('table.basic.list > tbody > tr > td.contract a').click(function(event) {
            event.stopPropagation();
        });

        $('table.basic.list > tbody > tr > td.customer a').click(function(event) {
            event.stopPropagation();
        });

        $('table.basic.list > tbody > tr > td.checkbox input').click(function(event) {
            event.stopPropagation();
        });

        $('table.basic.list > tbody > tr > td.checkbox input').change(function() {
            var checked = $(this).prop('checked');
            $(this).parents('tr').toggleClass('selected', checked);

            var id = $(this).prop('value');
            if (checked)
            {
                contracts.add(id);
            }
            else
            {
                contracts.remove(id);
            }

            contracts._updatePrintBtnsStatus(true);
            contracts._updatePrintBtnsHref();
        });

        $('table.basic.list > tbody > tr').click(function() {
            $(this).find('td.checkbox input').click();
        });

        // Rechargement de la page lorsqu'on imprime
        if (this.type == TYPE_FIRSTEDIT)
        {
            Location.popupsManager.getPopup('printPopup').onclose = function() { contracts.submitForm(); };
            $('#printBtns\\.pdf').click(function() {
                setTimeout(function() { contracts.submitForm(); }, 1000)
            });
        }
    },


    _updatePrintBtnsStatus : function(disable)
    {
        if (disable)
        {
            $('#printBtns').addClass('disabled');
        }
        else
        {
            var hasHref = ($('#printBtns\\.print').attr('href') != '#' || $('#printBtns\\.pdf').attr('href') != '#');
            $('#printBtns').toggleClass('disabled', this.ids == 0 || !hasHref);
        }
    },


    /**
     * Mise à jour des boutons d'éditions en fonction des contrats sélectionnés
     */
    _updatePrintBtnsHref : function()
    {
        // Initialisation de l'objet HTTPRequest
        if (!this._urlsHttpObj)
        {
            this._urlsHttpObj = new Location.HTTPRequest(URL_PRINTURLS);
        }

        // Passage des paramètres
        this._urlsHttpObj.setVar('ids', this.ids);

        var completionFunc = function(tab)
        {
            if (tab && tab.print && tab.print != '')
            {
                $('#printBtns\\.print').attr('href', tab.print);
            }
            else
            {
                $('#printBtns\\.print').attr('href', '#');
            }

            if (tab && tab.pdf && tab.pdf != '')
            {
                $('#printBtns\\.pdf').attr('href', tab.pdf);
            }
            else
            {
                $('#printBtns\\.pdf').attr('href', '#');
            }

            // État des boutons d'impression
            contracts._updatePrintBtnsStatus();
        }

        // Réception des données
        this._urlsHttpObj.oncompletion = function()
        {
            completionFunc(this.getJSONResponse());
        };

        // Erreur
        this._urlsHttpObj.onerror = function()
        {
            completionFunc(false);
        };

        // Envoi de la requête HTTP
        this._urlsHttpObj.send(true, true);
    }

};
