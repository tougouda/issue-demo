
if (!window.Location)
{
    var Location = new Object();
}


/*
 * Objet de gestion du chantier
 *
 */
Location.Rent_Helper_Site = function(documentObj, tabCfgs)
{
    this._documentObj = documentObj;
    this._tabCfgs = tabCfgs;

    this._id                 = 0;
    this._label              = '';
    this._address            = '';
    this._localityId         = 0;
    this._zone               = 0;
    this._contactFullName    = '';
    this._contactTelephone   = '';
    this._contactEmail       = '';
    this._isAsbestos         = false;
    this._oldIsAsbestos      = false;
    this._deliveryHour       = '';
    this._deliveryTimeSlotId = null;
    this._recoveryHour       = '';
    this._recoveryTimeSlotId = null;
    this._isHasWharf         = false;
    this._isAnticipated      = false;
    this._comments           = '';

    this._tabRights = null;

    this.onchange = null;
    this.onload   = null;
    this.onzonechange = null; // function(newZone, oldZone)

    this._tabExtParams = {
        deliveryLimitHour: null,
        recoveryLimitHour: null,
        disabledComponents: {}
    };

}

Location.Rent_Helper_Site.prototype = {


    /**
     * Chargement de données
     *
     * @param Object tab          Données
     * @param Object tabExtParams Paramètres externes
     * @param Object tabRights    Droits
     */
    load : function(tab, tabExtParams, tabRights)
    {

        this._id                 = tab.id;
        this._label              = tab.label;
        this._address            = tab.address;
        this._localityId         = tab.localityId;
        this._localityLabel      = tab.localityLabel;
        this._zone               = tab.zone;
        this._contactFullName    = tab.contactFullName;
        this._contactTelephone   = tab.contactTelephone;
        this._contactEmail       = tab.contactEmail;
        this._isAsbestos         = (tab.isAsbestos ? true : false);
        this._deliveryHour       = tab.deliveryHour;
        this._deliveryTimeSlotId = tab.deliveryTimeSlotId;
        this._recoveryHour       = tab.recoveryHour;
        this._recoveryTimeSlotId = tab.recoveryTimeSlotId;
        this._isHasWharf         = (tab.isHasWharf ? true : false);
        this._isAnticipated      = (tab.isAnticipated ? true : false);
        this._comments           = tab.comments;

        this._oldIsAsbestos      = this._isAsbestos;

        for (var name in tabExtParams)
        {
            if (this._tabExtParams.hasOwnProperty(name))
            {
                this._tabExtParams[name] = tabExtParams[name];
            }
        }

        this._tabRights = tabRights;

        if (this.onload)
        {
            this.onload();
        }
    },

    /**
     * Récupérer l'adresse du chantier
     *
     * @return string
     */
    getAddress : function()
    {
        return this._address;
    },

    /**
     * Récupérer les commentaires du chantier
     *
     * @return string
     */
    getComments : function()
    {
        return this._comments;
    },

    /**
     * Récupérer le nom du contact du chantier
     *
     * @return string
     */
    getContactFullName : function()
    {
        return this._contactFullName;
    },

    /**
     * Récupérer l'adresse mail du contact du chantier
     * 
     * @return string
     */
    getContactEmail: function()
    {
        return this._contactEmail;
    },

    /**
     * Récupérer le téléphone du contact du chantier
     *
     * @return string
     */
    getContactTelephone : function()
    {
        return this._contactTelephone;
    },

    /**
     * Récupérer les données du tarif transport
     *
     * @return Object
     */
    getData : function()
    {
        return {
            'id' :                  this.getId(),
            'label' :               this.getLabel(),
            'address' :             this.getAddress(),
            'locality.id' :         this.getLocalityId(),
            'contact.fullName'  :   this.getContactFullName(),
            'contact.telephone' :   this.getContactTelephone(),
            'contact.email'     :   this.getContactEmail(),
            'isAsbestos' :          (this.isAsbestos() ? 1 : 0),
            'deliveryHour' :        this.getDeliveryHour(),
            'deliveryTimeSlot.id' : this.getDeliveryTimeSlotId(),
            'recoveryHour' :        this.getRecoveryHour(),
            'recoveryTimeSlot.id' : this.getRecoveryTimeSlotId(),
            'comments' :            this.getComments(),
            'zone' :                this.getZone(),
            'isHasWharf' :          (this.isHasWharf() ? 1 : 0),
            'isAnticipated' :       (this.isAnticipated() ? 1 : 0)
        };
    },

    /**
     * Récupérer l'heure de livraison du chantier
     *
     * @return string
     */
    getDeliveryHour : function()
    {
        return this._deliveryHour;
    },

    /**
     * Récupérer l'id de la plage horaire de livraison
     *
     * @return int|null
     */
    getDeliveryTimeSlotId : function()
    {
        return this._deliveryTimeSlotId;
    },

    /**
     * Récupérer l'identifiant du chantier
     *
     * @return int
     */
    getId : function()
    {
        return this._id;
    },

    /**
     * Récupérer l'intitulé du chantier
     *
     * @return int
     */
    getLabel : function()
    {
        return this._label;
    },

    /**
     * Récupérer l'identifiant de la ville du chantier
     *
     * @return int
     */
    getLocalityId : function()
    {
        return this._localityId;
    },

    /**
     * Récupérer le libellé de la ville du chantier
     *
     * @return string
     */
    getLocalityLabel : function()
    {
        return this._localityLabel;
    },

    /**
     * Récupérer le type de l'objet courant
     *
     * @return string ('site')
     */
    getObjectType : function()
    {
        return 'site';
    },

    /**
     * Récupérer l'heure de récupération du chantier
     *
     * @return string
     */
    getRecoveryHour : function()
    {
        return this._recoveryHour;
    },

    /**
     * Récupérer l'id de la plage horaire de récupération
     *
     * @return int|null
     */
    getRecoveryTimeSlotId : function()
    {
        return this._recoveryTimeSlotId;
    },

    /**
     * Récupérer la zone
     *
     * @return int
     */
    getZone : function()
    {
        return this._zone;
    },

    /**
     * quai présent ou pas
     *
     * @return bool
     */
    isHasWharf : function()
    {
        return this._isHasWharf;
    },

    /**
     * anticipation présent ou pas
     *
     * @return bool
     */
    isAnticipated : function()
    {
        return this._isAnticipated;
    },

    /**
     * amiante présente ou pas
     *
     * @return bool
     */
    isAsbestos : function()
    {
        return this._isAsbestos;
    },

    /**
     * Définir l'adresse du chantier
     *
     * @param string address
     * @return bool
     */
    setAddress : function(address)
    {
        address = (address + '').trim();
        if (this._address == address || !this.hasRight('address'))
        {
            return false;
        }
        this._address = address;

        if (this.onchange)
        {
            this.onchange(['address']);
        }

        return true;
    },

    /**
     * Définir les commentaires du chantier
     *
     * @param string comments
     * @return bool
     */
    setComments : function(comments)
    {
        comments = (comments + '').trim();
        if (this._comments == comments || !this.hasRight('comments'))
        {
            return false;
        }
        this._comments = comments;

        if (this.onchange)
        {
            this.onchange(['comments']);
        }

        return true;
    },

    /**
     * Définir le contact du chantier
     *
     * @param string name
     * @return bool
     */
    setContactFullName : function(name)
    {
        name = (name + '').trim();
        if (this._contactFullName == name || !this.hasRight('contact'))
        {
            return false;
        }
        this._contactFullName = name;

        if (this.onchange)
        {
            this.onchange(['contactFullName']);
        }

        return true;
    },

    /**
     * Définir le téléphone du contact du chantier
     *
     * @param string telephone
     * @return bool
     */
    setContactTelephone : function(telephone)
    {
        if (this._contactTelephone == telephone || !this.hasRight('contact'))
        {
            return false;
        }
        this._contactTelephone = telephone;

        if (this.onchange)
        {
            this.onchange(['contactTelephone']);
        }

        return true;
    },

    /**
     * Définir le mail du contact du chantier
     *
     * @param string name
     * @return bool
     */
    setContactEmail : function(mail)
    {
        mail = (mail + '').trim();
        if (this._contactEmail == mail || !this.hasRight('contact'))
        {
            return false;
        }
        this._contactEmail = mail;

        if (this.onchange)
        {
            this.onchange(['contactEmail']);
        }

        return true;
    },

    /**
     * Définir l'heure de livraison du chantier
     *
     * @param string hour
     * @return bool
     */
    setDeliveryHour : function(hour)
    {
        hour = this._toHour(hour);
        if (this._deliveryHour == hour || !this.hasRight('hours') ||
            this.getExternalParamValue('disabledComponents').deliveryHourMasked)
        {
            return false;
        }
        this._deliveryHour = hour;

        if (this.onchange)
        {
            this.onchange(['deliveryHour']);
        }

        return true;
    },

    /**
     * Définir l'heure de livraison du chantier
     *
     * @param string recoveryHour
     * @return bool
     */
    setDeliveryTimeSlotId : function(id)
    {
        id = (Number(id) || null)
        if (this._deliveryTimeSlotId == id || !this.hasRight('hours') ||
            this.getExternalParamValue('disabledComponents').deliveryTimeSlotId)
        {
            return false;
        }
        this._deliveryTimeSlotId = id;

        if (this.onchange)
        {
            this.onchange(['deliveryTimeSlotId']);
        }

        return true;
    },

    /**
     * Définir si il y a un quai
     *
     * @param bool isHasWharf
     * @return bool
     */
    setHasWharf : function(isHasWharf)
    {
        isHasWharf = (isHasWharf ? true : false);
        if (this._isHasWharf == isHasWharf || !this.hasRight('wharf'))
        {
            return false;
        }
        this._isHasWharf = isHasWharf;

        if (this.onchange)
        {
            this.onchange(['isHasWharf']);
        }

        return true;
    },

    /**
     * Définir si il y a une anticipation
     *
     * @param bool isHasAnticipation
     * @return bool
     */
    setAnticipated : function(isAnticipated)
    {
        isAnticipated = (isAnticipated ? true : false);
        if (this._isAnticipated == isAnticipated ||
            !this.hasRight('anticipated') ||
            this.getExternalParamValue('disabledComponents').isAnticipated)
        {
            return false;
        }
        this._isAnticipated = isAnticipated;

        if (this.onchange)
        {
            this.onchange(['isAnticipated']);
        }

        return true;
    },

    /**
     * Définir si il y a de l'amiante
     *
     * @param bool isAsbestos
     * @return bool
     */
    setIsAsbestos : function(isAsbestos)
    {
        isAsbestos = (isAsbestos ? true : false);
        if (this._isAsbestos == isAsbestos ||
            (this._isAsbestos && this._oldIsAsbestos && !this.hasRight('deactivateAsbestos')) ||
            (!this.isAsbestos() && !this.hasRight('activateAsbestos')))
        {
            return false;
        }
        this._isAsbestos = isAsbestos;

        if (this.onchange)
        {
            this.onchange(['isAsbestos']);
        }

        return true;
    },

    /**
     * Définir l'intitulé du chantier
     *
     * @param string label
     * @return bool
     */
    setLabel : function(label)
    {
        label = (label + '').trim();
        if (this._label == label || !this.hasRight('label'))
        {
            return false;
        }
        this._label = label;

        if (this.onchange)
        {
            this.onchange(['label']);
        }

        return true;
    },

    /**
     * Définir l'identifiant de la ville du chantier
     *
     * @param int localityId
     * @return bool
     */
    setLocalityId : function(localityId, zone)
    {

        localityId = Number(localityId);
        if (this._localityId == localityId || !this.hasRight('locality'))
        {
            return false;
        }

        this._localityId = localityId;
        var tabChanges = ['localityId'];

        var oldZone = this._zone;
        this._zone  = zone;

        if (oldZone != zone)
        {
            tabChanges.push('zone');
        }

        if (tabChanges.length > 0 && this.onchange)
        {
            this.onchange(tabChanges);
        }

        return true;
    },

    /**
     * Définir l'heure de livraison du chantier
     *
     * @param string hour
     * @return bool
     */
    setRecoveryHour : function(hour)
    {
        hour = this._toHour(hour);
        if (this._recoveryHour == hour || !this.hasRight('hours') ||
            this.getExternalParamValue('disabledComponents').recoveryHourMasked)
        {
            return false;
        }
        this._recoveryHour = hour;

        if (this.onchange)
        {
            this.onchange(['recoveryHour']);
        }

        return true;
    },

    /**
     * Définir l'heure de livraison du chantier
     *
     * @param string recoveryHour
     * @return bool
     */
    setRecoveryTimeSlotId : function(id)
    {
        id = (Number(id) || null)
        if (this._recoveryTimeSlotId == id || !this.hasRight('hours') ||
            this.getExternalParamValue('disabledComponents').recoveryTimeSlotId)
        {
            return false;
        }
        this._recoveryTimeSlotId = id;

        if (this.onchange)
        {
            this.onchange(['recoveryTimeSlotId']);
        }

        return true;
    },

    /**
     * Vérifier le droit à un champ
     *
     * @return bool
     */
    hasRight : function(fieldName)
    {
        return (this._tabRights === null || this._tabRights[fieldName] ? true : false);
    },


    _toHour : function(hourStr)
    {
        var tab = hourStr.replace(/[^0-9\:]/g, '').split(':');
        if (tab.length == 2 && (tab[0] != '' || tab[1] != ''))
        {
            var hours = (tab[0] > 23 ? 23 : (tab[0] < 10 ? '0' + tab[0] * 1 : tab[0] * 1));
            var minutes = (tab[1] > 59 ? 59 : (tab[1] < 10 ? '0' + tab[1] * 1 : tab[1] * 1));
            return hours + ':' + minutes;
        }
        return '';
    },


    /**
     * Récupérer un paramètre externe
     *
     * @param string name
     * @return mixed
     */
    getExternalParamValue : function(name)
    {
        return this._tabExtParams[name];
    },

    /**
     * Définir un ou plusieurs paramètres externes
     *
     * @param Object tab Paramètres à définir
     * @return bool
     */
    setExternalParams : function(tab)
    {
        var tabChanges = [];
        for (var name in tab)
        {
            if (this._tabExtParams.hasOwnProperty(name))
            {
                if (!LOC_Common.isEqual(tab[name], this._tabExtParams[name]))
                {
                    this._tabExtParams[name] = tab[name];
                    tabChanges.push('extParam_' + name);
                }
            }
        }

        if (tabChanges.length > 0 && this.onchange)
        {
            this.onchange(tabChanges);
        }
        return (tabChanges.length > 0);
    },

    /**
     * Vérification du format de mail du contact chantier
     */
    checkContactEmail: function()
    {
        if(this.getContactEmail() && !LOC_Common.isValidMail(this.getContactEmail()))
        {
            this._documentObj.setErrors([121], 1);
            return false;
        }
        return true;
    }

}


Location.Rent_Helper_SiteView = {


    /**
     * Affichage de la plage horaire
     *
     * @param Object el       Liste
     * @param int    value    Valeur
     * @param bool   hasRight A-t-on les droits de la modifier ?
     */
    _displayTimeSlot : function(el, value, hasRight, obj)
    {
        var i = 0, nb = el.options.length;
        while (i < nb && value != el.options[i].value)
        {
            i++;
        }
        if (i == nb)
        {
            while (el.options[0])
            {
                el.removeChild(el.options[0]);
            }
            el.add(new Option('---', 0), null);
            var tab = obj._tabCfgs.tabTransportTimeSlots;
            for (var i = 0; i < tab.length; i++)
            {
                if (tab[i].isActive || value == tab[i].id)
                {
                    el.add(new Option(tab[i].label, tab[i].id), null);
                }
            }
        }
        el.value = value;
        el.disabled = !hasRight;
    },


    /**
     * Mise à jour de l'affichage
     *
     */
    display : function(pfx, obj, tabErrors)
    {
        $ge(pfx + 'Block').className = 'ok';
        $ge(pfx + 'DeliveryLimitHour').className = '';
        $ge(pfx + 'RecoveryLimitHour').className = '';


        $ge(pfx + 'Label').className = (tabErrors.noLabel ? 'error' : '');
        $ge(pfx + 'DeliveryHour.masked').className = (tabErrors.noDeliveryHour ? 'error' : '');
        $ge(pfx + 'DeliveryTimeSlotId').className  = (tabErrors.noDeliveryHour ? 'error' : '');

        $ge(pfx + 'Label').value = obj.getLabel();
        $ge(pfx + 'Address').value = obj.getAddress();
        $ge(pfx + 'ContactEmail').className = (tabErrors.invalidContactEmail ? 'error' : '');

        // Affichage de la ville
        var el = Location.searchBoxesManager.getSearchBox(pfx + 'LocalityId');

        if (obj.getLocalityId())
        {
            var opt =  el.getSelectedOption();
            if (!opt || opt.value != obj.getLocalityId() || opt.getAttribute('data-zone') != obj.getZone())
            {
                el.clear();
                el.setElements([{'id' : obj.getLocalityId(), 'fullName' : obj.getLocalityLabel(), 'zone' : obj.getZone()}], 2);
                el.setValue(obj.getLocalityId(), false);
            }
        }

        $ge(pfx + 'ContactFullName').value = obj.getContactFullName();
        $ge(pfx + 'ContactEmail').value = obj.getContactEmail();
        $ge(pfx + 'ContactTelephone').value = obj.getContactTelephone();

        // Icône de mail sur le contact chantier
        var aEl = $ge('site.email.send');
        var contactEmail = obj.getContactEmail();
        if (contactEmail && obj.checkContactEmail())
        {
            var title = aEl.getAttribute('mytitle');
            aEl.href  = 'mailto:' + contactEmail;
            aEl.title = title.replace(/%s/, contactEmail);
            aEl.removeAttribute('class');
        }
        else
        {
            aEl.removeAttribute('href');
            aEl.className = 'hidden';
        }

        var checkBoxEl = $ge(pfx + 'IsAsbestos');
        if (checkBoxEl)
        {
            checkBoxEl.checked = obj.isAsbestos();
        }


        $ge(pfx + 'Comments').value = obj.getComments();

        var spanEl = $ge(pfx + 'Zone');
        if (spanEl)
        {
            var zone = obj.getZone();
            spanEl.innerHTML = (zone ? zone : '?');
        }
        var checkBoxEl = $ge(pfx + 'IsHasWharf');
        if (checkBoxEl)
        {
            checkBoxEl.checked = obj.isHasWharf();
        }

        var checkBoxEl = $ge(pfx + 'IsAnticipated');
        if (checkBoxEl)
        {
            checkBoxEl.checked = obj.isAnticipated();
        }

        var hour = obj.getDeliveryHour();
        $ge(pfx + 'DeliveryHour').value = hour;
        setMaskedInputValue(pfx + 'DeliveryHour', (hour == '' ? '__:__' : hour));
        this._displayTimeSlot($ge(pfx + 'DeliveryTimeSlotId'), (obj.getDeliveryTimeSlotId() || 0),
                              (obj.hasRight('hours') && !obj.getExternalParamValue('disabledComponents').deliveryTimeSlotId), obj);

        // Affichage de la limite d'heure pour la livraison
        if (obj.getExternalParamValue('deliveryLimitHour'))
        {
            $ge(pfx + 'DeliveryLimitHour').className = 'isActive';
            $ge(pfx + 'DeliveryLimitHour_content').innerHTML = obj.getExternalParamValue('deliveryLimitHour');
        }

        var hour = obj.getRecoveryHour();
        $ge(pfx + 'RecoveryHour').value = hour;
        setMaskedInputValue(pfx + 'RecoveryHour', (hour == '' ? '__:__' : hour));
        this._displayTimeSlot($ge(pfx + 'RecoveryTimeSlotId'), (obj.getRecoveryTimeSlotId() || 0),
                              (obj.hasRight('hours') && !obj.getExternalParamValue('disabledComponents').recoveryTimeSlotId), obj);

        // Affichage de la limite d'heure pour la récupération
        if (obj.getExternalParamValue('recoveryLimitHour'))
        {
            $ge(pfx + 'RecoveryLimitHour').className = 'isActive';
            $ge(pfx + 'RecoveryLimitHour_content').innerHTML = obj.getExternalParamValue('recoveryLimitHour');
        }


        // Droits sur les champs
        $ge(pfx + 'Label').disabled = !obj.hasRight('label');
        $ge(pfx + 'Address').disabled = !obj.hasRight('address');
        Location.searchBoxesManager.getSearchBox(pfx + 'LocalityId').setDisabled(!obj.hasRight('locality'));
        $ge(pfx + 'ContactFullName').disabled = !obj.hasRight('contact');
        $ge(pfx + 'ContactTelephone.code').disabled = !obj.hasRight('contact');
        $ge(pfx + 'ContactTelephone.masked').disabled = !obj.hasRight('contact');
        $ge(pfx + 'IsAsbestos').disabled = ((obj.isAsbestos() && obj._oldIsAsbestos && !obj.hasRight('deactivateAsbestos')) ||
                                            (!obj.isAsbestos() && !obj.hasRight('activateAsbestos')));
        $ge(pfx + 'DeliveryHour.masked').disabled = (!obj.hasRight('hours') ||
                                                     obj.getExternalParamValue('disabledComponents').deliveryHourMasked);
        $ge(pfx + 'RecoveryHour.masked').disabled = (!obj.hasRight('hours')  ||
                                                     obj.getExternalParamValue('disabledComponents').recoveryHourMasked);
        $ge(pfx + 'IsHasWharf').disabled = !obj.hasRight('wharf');
        $ge(pfx + 'IsAnticipated').disabled = (!obj.hasRight('anticipated') ||
                                               obj.getExternalParamValue('disabledComponents').isAnticipated);
        $ge(pfx + 'Comments').disabled = !obj.hasRight('comments');
    },

    /**
     * Initialisation
     *
     */
    initEvents : function(pfx, obj)
    {

        // Modification du libellé
        var el = $ge(pfx + 'Label');
        if (el)
        {
            el.onchange = function()
            {
                obj.setLabel(this.value);
                this.value = obj.getLabel();
            }
        }

        // Modification de l'adresse
        el = $ge(pfx + 'Address');
        if (el)
        {
            el.onchange = function()
            {
                obj.setAddress(this.value);
                this.value = obj.getAddress();
            }

            // Firefox
            el.oninput = function()
            {
                if (this.value.length > this.getAttribute('data-maxlength'))
                {
                    alert(LOC_View.getTranslation('addressCapacityOver'));
                    obj.setAddress(this.value.substr(0, this.getAttribute('data-maxlength')));
                    this.value = obj.getAddress();
                }
                return false;
            }

            // Internet Explorer
            el.onpaste = function()
            {
                if (window.clipboardData)
                {
                    var newText = this.value + window.clipboardData.getData("Text");
                    if (newText.length > this.getAttribute('data-maxlength'))
                    {
                        alert(LOC_View.getTranslation('addressCapacityOver'));
                        obj.setAddress(newText.substr(0, this.getAttribute('data-maxlength')));
                        this.value = obj.getAddress();
                        return false;
                    }
                }
            }

            if (LOC_ClientInfo.isMSIE)
            {
                 el.onkeydown = function()
                {
                    if (!LOC_View.limitTextArea(event, this.getAttribute('data-maxlength')))
                    {
                        alert(LOC_View.getTranslation('addressCapacityOver'));
                        return false;
                    }
                }
            }
        }

        // Modification de la ville
        el = Location.searchBoxesManager.getSearchBox(pfx + 'LocalityId');
        if (el)
        {
            el.onchange = function()
            {
                var el = this.getSelectElement();
                obj.setLocalityId(this.getValue(), el.options[el.selectedIndex].getAttribute('data-zone'));
            }
        }

        // Modification du nom du contact
        el = $ge(pfx + 'ContactFullName');
        if (el)
        {
            el.onchange = function()
            {
                obj.setContactFullName(this.value);
                this.value = obj.getContactFullName();
            }
        }

        // Modification du téléphone du contact
        el = $ge(pfx + 'ContactTelephone.masked');
        if (el)
        {
            $ge(pfx + 'ContactTelephone.code').onblur = el.onblur = function()
            {
                obj.setContactTelephone($ge(pfx + 'ContactTelephone').value);
            }
        }

        // Modification du mail du contact
        el = $ge(pfx + 'ContactEmail');
        if (el)
        {
            el.onchange = function()
            {
                obj.setContactEmail(this.value);
                this.value = obj.getContactEmail();
            }
        }
        // Modification de la coche amiante
        el = $ge(pfx + 'IsAsbestos');
        if (el)
        {
            el.onchange = function()
            {
                obj.setIsAsbestos(this.checked);
                this.checked = obj.isAsbestos();
            }
        }

        // Modification du commentaire
        el = $ge(pfx + 'Comments');
        if (el)
        {
            el.onchange = function()
            {
                obj.setComments(this.value);
                this.value = obj.getComments();
            }
        }

        // Livraison
        el = $ge(pfx + 'DeliveryHour.masked');
        if (el)
        {
            el.onblur = function()
            {
                obj.setDeliveryHour($ge(pfx + 'DeliveryHour.masked').value);
                var hour = obj.getDeliveryHour();
                $ge(pfx + 'DeliveryHour').value = hour;
                setMaskedInputValue(pfx + 'DeliveryHour', (hour == '' ? '__:__' : hour));
            }
        }

        // Plage horaire de livraison
        el = $ge(pfx + 'DeliveryTimeSlotId');
        if (el)
        {
            el.onchange = function()
            {
                obj.setDeliveryTimeSlotId(this.value);
            }
        }

        // Récupération
        el = $ge(pfx + 'RecoveryHour.masked');
        if (el)
        {
            el.onblur = function()
            {
                obj.setRecoveryHour($ge(pfx + 'RecoveryHour.masked').value);
                var hour = obj.getRecoveryHour();
                $ge(pfx + 'RecoveryHour').value = hour;
                setMaskedInputValue(pfx + 'RecoveryHour', (hour == '' ? '__:__' : hour));
            }
        }

        // Plage horaire de récupération
        el = $ge(pfx + 'RecoveryTimeSlotId');
        if (el)
        {
            el.onchange = function()
            {
                obj.setRecoveryTimeSlotId(this.value);
            }
        }

        // Modification de la coche quai
        el = $ge(pfx + 'IsHasWharf');
        if (el)
        {
            el.onchange = function()
            {
                obj.setHasWharf(this.checked);
                this.checked = obj.isHasWharf();
            }
        }
        // Modification de la coche anticipation
        el = $ge(pfx + 'IsAnticipated');
        if (el)
        {
            el.onchange = function()
            {
                obj.setAnticipated(this.checked);
                this.checked = obj.isAnticipated();
            }
        }

    }

}
