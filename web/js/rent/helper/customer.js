/*
 * Objet de gestion du client
 *
 */
Location.Rent_Helper_Customer = function(documentObj, tabCfgs)
{
    this._tabCfgs = tabCfgs;
    this._httpObj = null;

    this._documentObj = documentObj;

    this._id         = null;
    this._name       = '';
    this._code       = '';
    this._url        = '#';
    this._historyUrl = '#';
    this._address    = '';
    this._lockLevel  = 0;
    this._isUnderSurveillance = 0;
    this._isTempUnlock = 0;
    this._unlockRequestUrl  = '#';
    this._unlockRequestStatus = '';
    this._telephone  = '';
    this._fax        = '';
    this._email      = '';
    this._isProformaRequired = false;
    this._isJoinDoc  = false;
    this._isOrderNoOnInvoice = false;
    this._isOrderRequired = false;
    this._isCleaningInvoiced = false;
    this._isResiduesInvoiced = false;
    this._class      = '';
    this._potential  = '';
    this._rentTariffGridLabel = '';
    this._rentTariffGridUrl   = '#';
    this._transportTariffGridLabel = '';
    this._insuranceTypeId = 0;
    this._insuranceRate = 0;
    this._appealTypeId = null;
    this._appealValue = 0;
    this._frmwrkContractId = 0;
    this._frmwrkContractLabel = '';
    this._frmwrkContractUrl = '#';
    this._frmwrkContractFuelUnitPrice = null;
    this._rentTariffGroupId = 0;
    this._rentTariffGroupLabel = '';
    this._transportTariffGroupId = 0;
    this._transportTariffGroupLabel = '';
    this._paymentModeId = 0;
    this._paymentModeLabel = '';
    this._isNewAccount = false;
    this._tabFlags = [];
    this._appCRMUrl = '#';
    this._isVatInvoiced = false;
    this._hasAreaExemption = false;
    this._refAgencyId = null;

    this._orderContactFullName  = '';
    this._orderContactTelephone = '';
    this._orderContactEmail     = '';

    this._tabExtParams = {
        disabledComponents: {}
    };
    this._tabRights = null;

    this.onload   = null;
    this.onchange = null;

}

Location.Rent_Helper_Customer.prototype = {

    /**
     * Récupérer l'adresse du client
     *
     * @return string
     */
    getAddress : function()
    {
        return this._address;
    },

    /**
     * Récupérer le type de gestion de recours
     *
     * @return int
     */
    getAppealTypeId : function()
    {
        // Vérification avec le type d'assurance
        var insuranceTypeInfos = this.getInsuranceTypeInfos();
        if (insuranceTypeInfos && !insuranceTypeInfos.isAppealActivated)
        {
            return null;
        }
        return this._appealTypeId;
    },

    /**
     * Récupérer les informations sur le type de gestion de recours
     *
     * @return obj
     */
    getAppealTypeInfos: function()
    {
        var mySelf = this;
        return this._tabCfgs.tabAppealTypesList.find(function(item) {
            return item.id == mySelf.getAppealTypeId();
        });
    },


    /**
     * Récupérer la classe du client
     *
     * @return string
     */
    getClass : function()
    {
        return this._class;
    },

    /**
     * Récupérer le code client
     *
     * @return string
     */
    getCode : function()
    {
        return this._code;
    },

    /**
     * Récupérer les données du client
     *
     * @return Object
     */
    getData : function()
    {
        return {
            'id'      : this.getId(),
            // Champs qui n'ont rien avoir avec le client !! (donc à l'arrache!)
            'orderNo' : this.getOrderNo(),
            'orderContact.fullName'  : this.getOrderContactFullName(),
            'orderContact.telephone' : this.getOrderContactTelephone(),
            'orderContact.email'     : this.getOrderContactEmail()
        };
    },

    /**
     * Récupérer l'adresse e-mail
     *
     * @return string
     */
    getEmail : function()
    {
        return this._email;
    },

    /**
     * Récupérer le numéro de fax
     *
     * @return string
     */
    getFax : function()
    {
        return this._fax;
    },

    /**
     * Récupérer les flags du client
     *
     * @return Array
     */
    getFlags : function()
    {
        return this._tabFlags;
    },

    /**
     * Récupérer le montant du carburant pour le contrat cadre
     *
     * @return float|null
     */
    getFwkCttFuelUnitPrice : function()
    {
        return this._frmwrkContractFuelUnitPrice;
    },

    /**
     * Récupérer l'id interne du client
     *
     * @return int
     */
    getId : function()
    {
        return this._id;
    },

    /**
     * Récupérer le taux d'assurances
     *
     * @return float
     */
    getInsuranceRate : function()
    {
        var insuranceInfos = this.getInsuranceTypeInfos();
        return (insuranceInfos && insuranceInfos.isInvoiced ? this._insuranceRate : null);
    },

    /**
     * Récupérer le type d'assurances
     *
     * @return int
     */
    getInsuranceTypeId : function()
    {
        return this._insuranceTypeId;
    },

    /**
     * Récupérer les informations sur le type d'assurance
     *
     * @return obj
     */
    getInsuranceTypeInfos: function()
    {
        var mySelf = this;
        return this._tabCfgs.tabInsuranceTypesList.find(function(item) {
            return item.id == mySelf.getInsuranceTypeId();
        });
    },

    /**
     * Récupérer le niveau de blocage
     *
     * @return int
     */
    getLockLevel : function()
    {
        return this._lockLevel;
    },

    /**
     * Récupérer la raison sociale
     *
     * @return string
     */
    getName : function()
    {
        return this._name;
    },

    /**
     * Récupérer le numéro de commande
     *
     * @return string
     */
    getOrderNo : function()
    {
        return ($ge('orderNo').value || '');
    },

    /**
     * Récupérer le nom complet du contact commande
     *
     * @return string
     */
    getOrderContactFullName : function()
    {
        return this._orderContactFullName;
    },

    /**
     * Récupérer le téléphone du contact commande
     *
     * @return string
     */
    getOrderContactTelephone : function()
    {
        return this._orderContactTelephone;
    },

    /**
     * Récupérer l'adresse email du contact commande
     *
     * @return string
     */
    getOrderContactEmail : function ()
    {
        return this._orderContactEmail;
    },

    /**
     * Récupérer l'agence de référence CRC
     *
     * @return string
     */
    getRefAgencyId : function()
    {
        return this._refAgencyId;
    },

    /**
     * Récupérer le mode de paiement du client
     *
     * @return int
     */
    getPaymentId : function()
    {
        return this._paymentModeId;
    },

    /**
     * Récupérer le potentiel
     *
     * @return string
     */
    getPotential : function()
    {
        return this._potential;
    },

    /**
     * Récupérer le numéro de téléphone
     *
     * @return string
     */
    getTelephone : function()
    {
        return this._telephone;
    },

    /**
     * Récupérer l'url du client
     *
     * @return string
     */
    getUrl : function()
    {
        return this._url;
    },

    /**
     * Récupérer l'url de l'historique client
     *
     * @return string
     */
    getHistoryUrl : function()
    {
        return this._historyUrl;
    },

    /**
     * Récupérer l'url de la fiche client CRM
     *
     * @return string
     */
    getAppCRMUrl : function()
    {
        return this._appCRMUrl;
    },

    /**
     * Indique si le client paye ou non la TVA
     *
     * @return bool
     */
    hasAreaExemption : function()
    {
        return this._hasAreaExemption;
    },

    /**
     * Indique si le client paye ou non la TVA
     *
     * @return bool
     */
    isVatInvoiced : function()
    {
        return this._isVatInvoiced;
    },

    /**
     * Indique si le client nécessite une pro forma
     *
     * @return bool
     */
    isProformaRequired : function()
    {
        return this._isProformaRequired;
    },

    /**
     * Savoir si le nettoyage est facturé par défaut ou non
     *
     * @return bool
     */
    isCleaningInvoiced : function()
    {
        return this._isCleaningInvoiced;
    },

    /**
     * Le client souhaite avec les documents machine
     *
     * @return bool
     */
    isJoinDoc : function()
    {
        return this._isJoinDoc;
    },

    /**
     * Savoir si la participation au recyclage est facturée par défaut ou non
     *
     * @return bool
     */
    isResiduesInvoiced : function()
    {
        return this._isResiduesInvoiced;
    },

    /**
     * Savoir s'il s'agit d'un nouveau compte ou non
     *
     * @return bool
     */
    isNewAccount : function()
    {
        return this._isNewAccount;
    },

    /**
     * Indique si le client est en déblocage temporaire
     *
     * @return bool
     */
    isTempUnlock : function()
    {
        return this._isTempUnlock;
    },

    /**
     * Indique si le client est sous surveillance
     *
     * @return bool
     */
    isUnderSurveillance : function()
    {
        return this._isUnderSurveillance;
    },

    /**
     * Indique si une demande de déblocage client a eu lieu
     *
     * @return bool
     */
    getUnlockRequestStatus : function()
    {
        return this._unlockRequestStatus;
    },

    /**
     * Récupérer un paramètre externe
     *
     * @param string name
     * @return mixed
     */
    getExternalParamValue : function(name)
    {
        return this._tabExtParams[name];
    },

    /**
     * Définir l'état d'envoi de la demande de déblocage du contrat
     *
     * @param string status
     * @return mixed
     */
    setUnlockRequestStatus: function(status)
    {
        if (!this._unlockRequestStatus != status)
        {
            this._unlockRequestStatus = status;
            if (this.onchange)
            {
                this.onchange(['unlockRequestStatus']);
            }
        }
    },

    /**
     * Définir le contact commande
     *
     * @param string name
     * @return bool
     */
    setOrderContactFullName : function(name)
    {
        name = (name + '').trim();
        if (this._orderContactFullName == name || !this.hasRight('orderContact'))
        {
            return false;
        }
        this._orderContactFullName = name;

        if (this.onchange)
        {
            this.onchange(['orderContactFullName']);
        }

        return true;
    },

    /**
     * Définir le téléphone du contact commande
     *
     * @param string telephone
     * @return bool
     */
    setOrderContactTelephone : function(telephone)
    {
        if (this._orderContactTelephone == telephone || !this.hasRight('orderContact'))
        {
            return false;
        }
        this._orderContactTelephone = telephone;

        if (this.onchange)
        {
            this.onchange(['orderContactTelephone']);
        }

        return true;
    },

    /**
     * Définir le mail du contact commande
     *
     * @param string name
     * @return bool
     */
    setOrderContactEmail : function(mail)
    {
        mail = (mail + '').trim();
        if (this._orderContactEmail == mail || !this.hasRight('orderContact'))
        {
            return false;
        }
        this._orderContactEmail = mail;

        if (this.onchange)
        {
            this.onchange(['orderContactEmail']);
        }

        return true;
    },

    /**
     * Définir un ou plusieurs paramètres externes
     *
     * @param Object tab Paramètres à définir
     * @return bool
     */
    setExternalParams : function(tab)
    {
        var tabChanges = [];
        for (var name in tab)
        {
            if (this._tabExtParams.hasOwnProperty(name))
            {
                if (!LOC_Common.isEqual(tab[name], this._tabExtParams[name]))
                {
                    this._tabExtParams[name] = tab[name];
                    tabChanges.push('extParam_' + name);
                }
            }
        }

        if (tabChanges.length > 0 && this.onchange)
        {
            this.onchange(tabChanges);
        }
        return (tabChanges.length > 0);
    },

    /**
     * Vérifier le droit à un champ
     *
     * @return bool
     */
    hasRight : function(fieldName)
    {
        return (this._tabRights === null || this._tabRights[fieldName] ? true : false);
    },

    loadById : function(id, onCompletion)
    {
        var mySelf = this;
        if (!this._httpObj)
        {
            this._httpObj = new Location.HTTPRequest(this._tabCfgs['customerInfosUrl']);
        }

        // Passage de l'id du client en paramètre
        this._httpObj.setVar('countryId', this._documentObj._countryId);
        this._httpObj.setVar('customerId', id);

        this._httpObj.oncompletion = function()
        {
            var tabInfos = this.getJSONResponse();
            if (tabInfos)
            {
                var oldId = mySelf._id;
                var tabOldInsurance = {
                    'typeId': mySelf.getInsuranceTypeId(),
                    'rate': mySelf.getInsuranceRate()
                };
                var oldAppealTypeId = mySelf.getAppealTypeId();
                var oldIsCleaningInvoiced = mySelf.isCleaningInvoiced();
                var oldIsResiduesInvoiced = mySelf.isResiduesInvoiced();
                var oldFwkFuelUnitPrice = mySelf.getFwkCttFuelUnitPrice();

                mySelf._loadInfos(tabInfos);

                var tabChanges = [];
                if (oldId != mySelf._id)
                {
                    tabChanges.push('id');
                }
                if (tabOldInsurance['typeId'] != mySelf.getInsuranceTypeId() ||
                    tabOldInsurance['rate'] != mySelf.getInsuranceRate())
                {
                    tabChanges.push('insurance');
                }
                if (oldAppealTypeId != mySelf.getAppealTypeId())
                {
                    tabChanges.push('appeal');
                }
                if (oldIsCleaningInvoiced != mySelf.isCleaningInvoiced())
                {
                    tabChanges.push('isCleaningInvoiced');
                }
                if (oldIsResiduesInvoiced != mySelf.isResiduesInvoiced())
                {
                    tabChanges.push('isResiduesInvoiced');
                }
                if (oldFwkFuelUnitPrice != mySelf.getFwkCttFuelUnitPrice())
                {
                    tabChanges.push('fwkCttFuelUnitPrice');
                }

                // TODO: Calculer les changements pouvant impacter le reste
                // pour le moment on fait comme si le client avait été modifié dans tous les cas
                if (mySelf.onchange)
                {
                    mySelf.onchange(tabChanges);
                }

                onCompletion(tabInfos);
            }
            else
            {
                // Erreur
                this.onerror();
            }
        };
        this._httpObj.onerror = function()
        {
            // Erreur
            onCompletion(false);
        };

        // Envoi de la requête HTTP
        this._httpObj.send(true, true);
        return true;
    },

    /**
     * Chargement de données
     *
     * @param Object tab          Données
     * @param Object tabExtParams Paramètres externes
     * @param Object tabRights    Droits
     */
    load : function (tabInfos, tabExtParams, tabRights)
    {
        this._loadInfos(tabInfos);

        for (var name in tabExtParams)
        {
            if (this._tabExtParams.hasOwnProperty(name))
            {
                this._tabExtParams[name] = tabExtParams[name];
            }
        }

        this._tabRights = tabRights;

        // Evénement
        if (this.onload)
        {
            this.onload();
        }
    },

    _loadInfos: function(tabInfos)
    {
        this._id   = Number(tabInfos['id']);
        this._name = tabInfos['name'];
        this._code = (tabInfos['code'] || '');
        this._url  = tabInfos['url'];
        this._historyUrl  = tabInfos['historyUrl'];
        this._address = (tabInfos['address'] ? tabInfos['address'].trim().replace(/\n+/g, '<br />') : '');
        this._lockLevel = Number(tabInfos['lockLevel']);
        this._isTempUnlock = (tabInfos['isTempUnlock'] ? true : false);
        this._unlockRequestUrl = tabInfos['unlockRequestUrl'];
        this._isUnderSurveillance = Number(tabInfos['isUnderSurveillance']);
        this._telephone = (tabInfos['telephone'] || '');
        this._fax   = (tabInfos['fax'] || '');
        this._email = (tabInfos['email'] || '');
        this._isProformaRequired = (tabInfos['isProformaRequired'] == 1);
        this._isJoinDoc = (tabInfos['isJoinDoc'] == 1);
        this._isOrderNoOnInvoice = (tabInfos['isOrderNoOnInvoice'] == 1);
        this._isOrderRequired = (tabInfos['isOrderRequired'] == 1);
        this._isCleaningInvoiced = (tabInfos['isCleaningInvoiced'] == 1);
        this._isResiduesInvoiced = (tabInfos['isResiduesInvoiced'] == 1);
        this._class      = (tabInfos['class'] ? tabInfos['class'] : '');
        this._potential  = (tabInfos['potential'] ? tabInfos['potential'] : '');
        this._rentTariffGridLabel = (tabInfos['rentTariff.grid.label'] || '');
        this._rentTariffGridUrl   = (tabInfos['rentTariff.grid.url'] || '#');
        this._transportTariffGridLabel = (tabInfos['transportTariff.grid.type'] == 'PERSO' ? LOC_View.getTranslation('transportTariff.personal') : tabInfos['transportTariff.grid.label'] || '');
        this._insuranceTypeId = Number(tabInfos['insuranceType.id']);
        this._insuranceRate = (tabInfos['insurance.rate'] ? Number(tabInfos['insurance.rate']) : null);
        this._appealTypeId = Number(tabInfos['appealType.id']);
        this._appealValue = this._tabCfgs.defaultAppealRate;
        this._frmwrkContractId = Number(tabInfos['frameworkContract.id']);
        this._frmwrkContractLabel = (tabInfos['frameworkContract.label'] || '');
        this._frmwrkContractUrl = (tabInfos['frameworkContract.url'] || '#');
        this._frmwrkContractFuelUnitPrice = (tabInfos['frameworkContract.fuelUnitPrice'] || null);
        this._rentTariffGroupId = Number(tabInfos['rentTariffGroup.id']);
        this._rentTariffGroupLabel = (tabInfos['rentTariffGroup.label'] || '');
        this._transportTariffGroupId = Number(tabInfos['transportTariffGroup.id']);
        this._transportTariffGroupLabel = (tabInfos['transportTariffGroup.label'] || '');
        this._paymentModeId = (tabInfos['paymentMode.id'] || 0);
        this._paymentModeLabel = (tabInfos['paymentMode.label'] || '');
        this._isNewAccount = (tabInfos['isNewAccount'] == 1);
        this._tabFlags = [];
        if (tabInfos['isElectric'])
        {
            this._tabFlags.push('electric');
        }
        if (tabInfos['isTemporaryGrid'])
        {
            this._tabFlags.push('tempGrid');
        }
        this._isVatInvoiced    = (tabInfos['accountingCategory'] ? true : false);
        this._hasAreaExemption = (tabInfos['hasAreaExemption'] ? true : false);
        this._refAgencyId      = tabInfos['refAgencyId'];

        // 1er chargement, récupération de la trame
        // si rechargement du bloc client, récupération du contenu existant des champs
        this._orderContactFullName    = (tabInfos['orderContact.fullName'] || $ge('orderContact.fullName').value);
        this._orderContactTelephone   = (tabInfos['orderContact.telephone'] || $ge('orderContact.telephone').value);
        this._orderContactEmail       = (tabInfos['orderContact.email'] || $ge('orderContact.email').value);

        this._appCRMUrl = (tabInfos['appCRM.url'] || '#');
    },
    /**
     * Vérification du format de mail du contact commande
     */
    checkOrderContactEmail: function()
    {
        if(this.getOrderContactEmail() && !LOC_Common.isValidMail(this.getOrderContactEmail()))
        {
            this._documentObj.setErrors([31], 1);
            return false;
        }
        return true;
    }
}

Location.Rent_Helper_CustomerView = {

    _httpUnlockObj: null,

    /**
     * Mise à jour de l'affichage
     *
     */
    display : function(pfx, obj, tabErrors)
    {
        var tabBoxObj = Location.tabBoxesManager.getTabBox('rentCustomerTabBox');

        // Raison sociale
        $ge('customer.name').innerHTML = obj.getName();

        // Code
        var id   = obj.getId();
        var code = obj.getCode();
        var isNewAccount = obj.isNewAccount();
        var lockLevel = obj.getLockLevel();
        if (id == 0)
        {
            tabBoxObj.getTab(1).disable();
            $ge('customer.code').innerHTML      = '';
            $ge('customer.code').href           = '';
            $ge('customer.code').className      = 'hidden';
            $ge('customer.code.none').className = 'hidden';
            $ge('customer.unlockingTempPopupBtn').style.display = 'none';
            $ge('customer.historyBtn').style.display       = 'none';
            $ge('customer.appCRMBtn').style.display        = 'none';
            $ge('customer.refreshBtn').style.display       = 'none';
        }
        else
        {
            tabBoxObj.getTab(1).enable();
            $ge('customer.code').innerHTML = (code != '' ? code : $ge('customer.code.none').innerHTML);
            $ge('customer.code').href           = obj.getUrl();
            $ge('customer.code').className      = 'visible';
            $ge('customer.code.none').className = 'hidden';
            $ge('customer.historyBtn').href     = obj.getHistoryUrl();
            $ge('customer.historyBtn').style.display = (obj.hasRight('history') ? 'inline' : 'none');
            $ge('customer.appCRMBtn').href      = obj.getAppCRMUrl();
            $ge('customer.appCRMBtn').style.display = 'inline';
            $ge('customer.refreshBtn').style.display = 'inline';

            var displayUnlockingTempPopupBtn = 'none';
            // si contrat et bloqué et pas débloqué temporairement
            // ou devis et blocage devis et pas débloqué temporairement
            if (!obj.isTempUnlock() &&
                (obj._documentObj.getObjectType() == 'contract' && lockLevel != 0 ||
                 obj._documentObj.getObjectType() == 'estimate' && (lockLevel == 3 || lockLevel == 4)))
            {
                displayUnlockingTempPopupBtn = 'inline';
            }
            $ge('customer.unlockingTempPopupBtn').style.display = displayUnlockingTempPopupBtn;
        }

        if (isNewAccount)
        {
            $ge('customer.accountOpening').className = 'visible';
        }
        else
        {
            $ge('customer.accountOpening').className = 'hidden';
        }

        // Niveau de blocage
        for (var i = 0; i <= 4; i++)
        {
            var customerLockedLevelElt = $ge('customerLockedLevel' + i);
            customerLockedLevelElt.className = (i == lockLevel ? '' : 'hidden');

            // Affichage du déblocage temporaire
            if (i == lockLevel && obj.isTempUnlock())
            {
                customerLockedLevelElt.title = LOC_View.getTranslation('customer_temp_unlock');
                var customerStatusElt = document.querySelector('#customerLockedLevel' + i + ' .locStandardCustomerStatus');
                if (customerStatusElt)
                {
                    customerStatusElt.className += ' unlock-temp';
                }
            }
            else
            {
                customerLockedLevelElt.title = '';
                var elt = document.querySelector('#customerLockedLevel' + i + ' .locStandardCustomerStatus');
                if (elt)
                {
                    var oldClassName = elt.className;
                    var reg = new RegExp('(^|\\s)unlock-temp(\\s|$)', 'g');
                    elt.className = oldClassName.replace(reg, ' ');
                }
            }
        }
        $ge('customerUnderSurveillance').style.display = (obj.isUnderSurveillance() ? 'inline' : 'none');

        var areaExemptionEl = $ge('customerHasAreaExemption');
        if (areaExemptionEl)
        {
            Location.commonObj.toggleEltClass(areaExemptionEl, 'active', obj.hasAreaExemption());
            var title = LOC_View.getTranslation('customer_hasAreaExemption');
            areaExemptionEl.title = title.replace(/<%refAgencyId>/, obj.getRefAgencyId());
        }

        // Adresse
        $ge('customer.address').innerHTML = obj.getAddress();

        // Téléphone, fax et e-mail
        var email = obj.getEmail();
        $ge('customer.telephone').innerHTML = obj.getTelephone();
        $ge('customer.fax').innerHTML       = obj.getFax();
        $ge('customer.email').value = email;

        var aEl = $ge('customer.email.send');
        if (email != '')
        {
            var title = aEl.getAttribute('mytitle');
            aEl.href  = 'mailto:' + email;
            aEl.title = title.replace(/%s/, email);
            aEl.removeAttribute('class');
        }
        else
        {
            aEl.removeAttribute('href');
            aEl.className = 'hidden';
        }

        // N° commande obligatoire
        $ge('customer.isOrderNoOnInvoice[' + obj._isOrderNoOnInvoice * 1 + ']').removeAttribute('class');
        $ge('customer.isOrderNoOnInvoice[' + (1 - obj._isOrderNoOnInvoice * 1) + ']').className = 'hidden';

        // BC à fournir
        $ge('customer.isOrderRequired[' + obj._isOrderRequired * 1 + ']').removeAttribute('class');
        $ge('customer.isOrderRequired[' + (1 - obj._isOrderRequired * 1) + ']').className = 'hidden';

        // Proforma
        var isProformaRequired = obj.isProformaRequired();
        $ge('customer.isProformaRequired[' + isProformaRequired * 1 + ']').removeAttribute('class');
        $ge('customer.isProformaRequired[' + (1 - isProformaRequired * 1) + ']').className = 'hidden';

        // Nettoyage
        var isCleaningInvoiced = obj.isCleaningInvoiced();
        $ge('customer.isCleaningInvoiced[' + isCleaningInvoiced * 1 + ']').removeAttribute('class');
        $ge('customer.isCleaningInvoiced[' + (1 - isCleaningInvoiced * 1) + ']').className = 'hidden';

        // Participation au recyclage
        var isResiduesInvoiced = obj.isResiduesInvoiced();
        if ($ge('customer.isResiduesInvoiced[' + isResiduesInvoiced * 1 + ']'))
        {
            $ge('customer.isResiduesInvoiced[' + isResiduesInvoiced * 1 + ']').removeAttribute('class');
            $ge('customer.isResiduesInvoiced[' + (1 - isResiduesInvoiced * 1) + ']').className = 'hidden';
        }

        // Classe et potentiel
        $ge('customer.class').innerHTML     = obj.getClass();
        $ge('customer.potential').innerHTML = obj.getPotential();

        // Grilles tarifaires de location et de transport
        $ge('customer.rentTariff.grid').innerHTML      = obj._rentTariffGridLabel;
        $ge('customer.rentTariff.grid').href           = obj._rentTariffGridUrl;
        $ge('customer.transportTariff.grid').innerHTML = obj._transportTariffGridLabel;

        // Assurance
        var tabSpans = $ge('customer.insurance').childNodes;
        var type = (obj.getInsuranceTypeId() || 0) + ':' + (obj.getInsuranceRate() || 0);
        for (var i = 0; i < tabSpans.length; i++)
        {
            tabSpans[i].style.display = (tabSpans[i].getAttribute('data-type') == type ? '' : 'none');
        }

        // Gestion de recours
        var tabSpans = $ge('customer.appeal').childNodes;
        var type = obj.getAppealTypeId() || 0;
        for (var i = 0; i < tabSpans.length; i++)
        {
            tabSpans[i].style.display = (tabSpans[i].getAttribute('data-type') == type ? '' : 'none');
        }

        // Contrat-cadre
        if (obj._frmwrkContractId != 0)
        {
            $ge('customer.frameworkContract[0]').className = 'hidden';
            $ge('customer.frameworkContract[1]').innerHTML = obj._frmwrkContractLabel;
            $ge('customer.frameworkContract[1]').href = obj._frmwrkContractUrl;
            $ge('customer.frameworkContract[1]').removeAttribute('class');
        }
        else
        {
            $ge('customer.frameworkContract[0]').removeAttribute('class');
            $ge('customer.frameworkContract[1]').innerHTML = '';
            $ge('customer.frameworkContract[1]').removeAttribute('href');
            $ge('customer.frameworkContract[1]').className = 'hidden';
        }

        // Groupes tarifaires de location et de transport
        if (obj._rentTariffGroupId != 0)
        {
            $ge('customer.rentTariffGroup[0]').className = 'hidden';
            $ge('customer.rentTariffGroup[1]').innerHTML = obj._rentTariffGroupLabel;
            $ge('customer.rentTariffGroup[1]').removeAttribute('class');
        }
        else
        {
            $ge('customer.rentTariffGroup[0]').removeAttribute('class');
            $ge('customer.rentTariffGroup[1]').innerHTML = '';
            $ge('customer.rentTariffGroup[1]').className = 'hidden';
        }

        if (obj._transportTariffGroupId != 0)
        {
            $ge('customer.transportTariffGroup[0]').className = 'hidden';
            $ge('customer.transportTariffGroup[1]').innerHTML = obj._transportTariffGroupLabel;
            $ge('customer.transportTariffGroup[1]').removeAttribute('class');
        }
        else
        {
            $ge('customer.transportTariffGroup[0]').removeAttribute('class');
            $ge('customer.transportTariffGroup[1]').innerHTML = '';
            $ge('customer.transportTariffGroup[1]').className = 'hidden';
        }

        // Mode de paiement
        $ge('customer.paymentMode.label').innerHTML = obj._paymentModeLabel;

        // Contact commande
        $ge('orderContact.fullName').value = obj.getOrderContactFullName();
        $ge('orderContact.telephone').value = obj.getOrderContactTelephone();
        $ge('orderContact.email').value = obj.getOrderContactEmail();

        var aEl = $ge('orderContact.email.send');
        var contactOrderEmail = obj.getOrderContactEmail();
        if (contactOrderEmail && obj.checkOrderContactEmail())
        {
            var title = aEl.getAttribute('mytitle');
            aEl.href  = 'mailto:' + contactOrderEmail;
            aEl.title = title.replace(/%s/, contactOrderEmail);
            aEl.removeAttribute('class');
        }
        else
        {
            aEl.removeAttribute('href');
            aEl.className = 'hidden';
        }
        $ge('orderContact.email').className = (tabErrors.invalidOrderContactEmail ? 'error' : '');

        // Droits sur les champs
        var customerSearchBoxObj = Location.searchBoxesManager.getSearchBox('customer.id');
        var disabledComponents = obj.getExternalParamValue('disabledComponents');
        customerSearchBoxObj.setDisabled(disabledComponents.select || !obj.hasRight('name'));
        var isNewEnabled = (!disabledComponents.select && obj.hasRight('new'));
        $ge('customer.newBtn').style.display = (isNewEnabled ? 'inline' : 'none');
        $ge('orderNo').disabled = (id == 0 || !obj.hasRight('orderNo'));
        $ge('orderContact.fullName').disabled = (id == 0 || !obj.hasRight('orderContact'));
        $ge('orderContact.telephone.code').disabled = (id == 0 || !obj.hasRight('orderContact'));
        $ge('orderContact.telephone.masked').disabled = (id == 0 || !obj.hasRight('orderContact'));
        $ge('orderContact.email').disabled = (id == 0 || !obj.hasRight('orderContact'));

        // Fournir les documents de la machine pave matériel
        var el = $('rentContractIsJoinDoc');
        el.checked = (obj._isJoinDoc ? 1 : 0);

        // Flags
        var tabFlags = obj.getFlags();
        if (tabFlags.length > 0)
        {
            $ge('rentCustomerFlags').setAttribute('class', tabFlags.join(' '));
        }
        else
        {
            $ge('rentCustomerFlags').removeAttribute('class');
        }
    },

    /**
     * Initialisation
     *
     */
    initEvents : function(pfx, obj)
    {
        var mySelf = this;

        // Modification de la ville
        el = Location.searchBoxesManager.getSearchBox('customer.id');
        if (el)
        {
            el.onchange = function()
            {
                $ge('rentCustomerBlock').className = 'loading';
                var el = this.getSelectElement();
                obj.loadById(this.getValue(), function(result) {
                    $ge('rentCustomerBlock').className = (!result ? 'error' : '');
                });
            }
        }

        $ge('customer.refreshBtn').onclick = function()
        {
            $ge('rentCustomerBlock').className = 'loading';
            obj.loadById(obj.getId(), function(result) {
                $ge('rentCustomerBlock').className = (!result ? 'error' : '');
            });
            return false;
        }

        $ge('customer.unlockingTempPopupBtn').onclick = function()
        {
            mySelf.showUnlockingTempPopup(pfx, obj);
            return false;
        }

        // Modification du nom du contact commande
        el = $ge('orderContact.fullName');
        if (el)
        {
            el.onchange = function()
            {
                obj.setOrderContactFullName(this.value);
                this.value = obj.getOrderContactFullName();
            }
        }

        // Modification du téléphone du contact commande
        el = $ge('orderContact.telephone.masked');
        if (el)
        {
            $ge('orderContact.telephone.code').onblur = el.onblur = function()
            {
                obj.setOrderContactTelephone($ge('orderContact.telephone').value);
            }
        }

        // Modification du mail du contact commande
        el = $ge('orderContact.email');
        if (el)
        {
            el.onchange = function()
            {
                obj.setOrderContactEmail(this.value);
                this.value = obj.getOrderContactEmail();
            }
        }
    },

    showUnlockingTempPopup : function(pfx, obj)
    {
        if (obj.getLockLevel() == 0 || obj.isTempUnlock())
        {
            return false;
        }

        // Commentaire
        $ge(pfx + 'unlockComment').value = '';

        Location.modalWindowManager.show(pfx + 'unlockingTempPopup', {contentType: 3, width: 450, height: 'auto'});

        $ge(pfx + 'unlockingTempBtn').onclick = function()
        {
            LOC_View.displayLoading(true);
            // Fermeture de la popup
            Location.modalWindowManager.hide(pfx + 'unlockingTempPopup');

            // Traitement : envoi du mail
            if (!this._httpUnlockObj)
            {
                this._httpUnlockObj = new Location.HTTPRequest(obj._unlockRequestUrl);
            }
            var documentObj = obj._documentObj;

            var tabDocumentInfos = {
                agencyId: documentObj.getAgencyId(),
                type: documentObj.getObjectType(),
                id: documentObj.getId(),
                code: documentObj.getCode(),
                orderNo: obj.getOrderNo()
            };

            // Cas du contrat
            if (documentObj.getObjectType() == 'contract')
            {
                tabDocumentInfos.newBeginDate = documentObj._durationObj.getBeginDate();
                tabDocumentInfos.newEndDate   = documentObj._durationObj.getEndDate();
                tabDocumentInfos.newDuration  = documentObj._durationObj.getDuration();
                tabDocumentInfos.modelId      = documentObj.getModelId();
                tabDocumentInfos.amount       = documentObj.getTotal();
            }
            else
            {
                var tabLines = documentObj.getLines();
                var amount = 0;
                for (var i = 0; i < tabLines.length; i++)
                {
                    amount += tabLines[i].getTotal();
                }
                tabDocumentInfos.amount = amount;
            }

            // Passage des paramètres
            this._httpUnlockObj.setVar('type', documentObj.getEditMode());
            this._httpUnlockObj.setVar('comment', $ge(pfx + 'unlockComment').value);
            this._httpUnlockObj.setVar('documentInfos', tabDocumentInfos);

            this._httpUnlockObj.oncompletion = function()
            {
                LOC_View.displayLoading(false);
                var tabInfos = this.getJSONResponse();
                if (tabInfos && tabInfos['result'])
                {
                    obj.setUnlockRequestStatus('sended');
                }
                else
                {
                    // Erreur
                    this.onerror();
                }

                return false;
            };
            this._httpUnlockObj.onerror = function()
            {
                obj.setUnlockRequestStatus('error');
                LOC_View.displayLoading(false);
            };

            // Envoi de la requête HTTP
            this._httpUnlockObj.send(true, true);
        }
        return true;
    }

};
