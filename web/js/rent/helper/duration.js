if (!window.Location)
{
    var Location = new Object();
}


/**
 * Classe de gestion de la partie durée
 *
 * @param Object documentObj Objet javascript du document (ligne de devis ou contrat)
 * @param Object tabCfgs     Configurations globales
 */
Location.Rent_Helper_Duration = function(documentObj, tabCfgs)
{
    this._prefix = 'durationHlp_';
    this._documentObj = documentObj;
    this._tabCfgs = tabCfgs;

    this._httpObj = null;

    this._beginDate = '';
    this._endDate = '';
    this._theorEndDate = '';
    this._durationStr = '';
    this._realDuration = 0;
    this._calendarDuration = 0;
    this._isRealDurationChanged = 0;

    this._tabDays = {};
    this._tabDays[DAY_TYPE_ADDITIONAL] = {
        list:                [],
        nbUnbilled:          0,
        nbBilled:            0,
        comment:             null,
        authorizedPeriod:    [],
        tabDeltasPeriodDays: []
    };
    this._tabDays[DAY_TYPE_DEDUCTED] = {
        list:                [],
        nbUnbilled:          0,
        nbBilled:            0,
        comment:             null,
        authorizedPeriod:    [],
        tabDeltasPeriodDays: []
    };

    this._isDatesToConfirm       = false;

    this._requiredBeginDate = '';
    this._requiredEndDate = '';

    this._requiredRealDuration = 0;
    this._requiredCalendarDuration = 0;
    this._requiredRentTariffDurRangeLabel = '';
    this._requiredDurationStr = '';
    this._rentTariffDurRangeLabel = '';

    this._tabExtParams = {
        isProformaRequired: false,
        isProformaActived: false,
        hasBaseAmountChanged: false,
        beginDateWindow: [null, null],
        endDateWindow: [null, null],
        disabledComponents: {}
    };

    this._tabRights = null;

    this._tabDaysActions = [];

    this._getDayExtraInfosHttpObj = null;

    this.onchange = null;
    this.onupdaterequiredperiod = null;
    this.onload   = null;
}

//Propriété statique pour le compteur jours ajoutés
Location.Rent_Helper_Duration._addCpt = 0;

Location.Rent_Helper_Duration.prototype = {

    /**
     * Copier les informations d'une ligne vers la courante
     *
     * @param Location.Rent_Helper_Duration sourceObj
     * @return bool
     */
    copy : function(sourceObj)
    {
        var mySelf = this;
        var tabProps = ['tabExtParams', 'beginDate', 'endDate', 'theorEndDate', 'durationStr',
                        'realDuration', 'calendarDuration',
                        'requiredBeginDate', 'requiredEndDate', 'requiredRealDuration',
                        'isDatesToConfirm', 'isRealDurationChanged'];

        var i, prop;
        var nb = tabProps.length;
        for (i = 0; i < nb; i++)
        {
            prop = '_' + tabProps[i];
            this[prop] = LOC_Common.clone(sourceObj[prop]);
        }

        // Récupération des périodes autorisées
        mySelf._tabDays[DAY_TYPE_ADDITIONAL].authorizedPeriod = sourceObj.getAuthorizedPeriodAddDays();
        mySelf._tabDays[DAY_TYPE_DEDUCTED].authorizedPeriod   = sourceObj.getAuthorizedPeriodDedDays();

        var tabDays = LOC_Common.clone(sourceObj.getDaysList());
        // Liste des jours existants : création des actions d'ajout
        tabDays.forEach(function(day, index) {
            var action = sourceObj.getActionById(day.id);
            var actionType = (action ? action.type : '');
            if (actionType != 'delete')
            {
                // Edition en cours
                if (actionType == 'edit')
                {
                    day.date              = action.data.date;
                    day.invoiceDuration   = mySelf._getDayInvoiceDuration(action.data.invoiceDurationId);
                    day.reason            = mySelf._getDayReason(action.data.reasonId);
                    day.comment           = action.data.comment;
                    day.beginDateMax      = action.beginDateMax;
                    day.beginDateMin      = action.endDateMin;
                    day.appliedToContract = action.appliedToContract;
                }
                mySelf._tabDays[day.type].list.unshift(day);

                mySelf._moveToAdded(day);
            }
        });

        return true;
    },

    /**
     * Initialisation
     *
     */
    load : function(tab, tabExtParams, tabRights)
    {
        this._tabPeriodLimitDates = [];
        this._tabDaysActions = [];

        this._beginDate              = tab.beginDate;
        this._endDate                = tab.endDate;
        this._realDuration           = Number(tab.realDuration);
        this._calendarDuration       = Number(tab.calendarDuration);
        this._isRealDurationChanged  = (tab.isRealDurationChanged == 1 ? true : false);

        this._tabDays = tab.days;

        // Actions
        var mySelf = this;
        this._tabDays[DAY_TYPE_ADDITIONAL].list.concat(this._tabDays[DAY_TYPE_DEDUCTED].list).filter(function(day) {
            return !day.id;
        }).forEach(function(day) {
            // le jour dans la liste n'est plus compté dans la liste des non facturés
            mySelf._tabDays[day.type].nbUnbilled -= day.invoiceDuration.duration;
            mySelf._moveToAdded(day);
        });

        this._isDatesToConfirm       = (tab.isDatesToConfirm == 1 ? true : false);

        this._theorEndDate           = tab.theorEndDate || '';
        this._requiredBeginDate      = tab.beginDate;
        this._requiredEndDate        = tab.endDate;
        this._requiredRealDuration   = this._realDuration;

        for (var name in tabExtParams)
        {
            if (this._tabExtParams.hasOwnProperty(name))
            {
                this._tabExtParams[name] = tabExtParams[name];
            }
        }

        this._tabRights = tabRights;

        if (this.onload)
        {
            this.onload();
        }
    },

    /**
     * Récupérer instance du document associé
     *
     * @return Object
     */
    getDocumentObject : function()
    {
        return this._documentObj;
    },

    /**
     * Récupérer l'URL de récup des infos complémentaires par rapport à un jour+/-
     * - périodes autorisées
     * - jours existants si lié à un contrat
     */
    getDayExtraInfosUrl: function()
    {
        return this._tabCfgs.dayExtraInfosUrl;
    },

    /**
     * Récupérer la liste des actions en cours
     */
    getDaysActions: function()
    {
        return this._tabDaysActions;
    },

    /**
     * Récupérer la date de début
     *
     * @return string
     */
    getBeginDate : function()
    {
        return this._beginDate;
    },

    /**
     * Récupérer la date de fin
     *
     * @return string
     */
    getEndDate : function()
    {
        return this._endDate;
    },

    /**
     * Récupérer la date de fin théorique
     *
     * @return string
     */
    getTheoreticalEndDate : function()
    {
        return this._theorEndDate;
    },

    /**
     * Récupérer la durée du contrat en jours calendaires
     *
     * @return int
     */
    getCalendarDuration : function()
    {
        return this._calendarDuration;
    },

    /**
     * Récupérer le nombre de jours supplémentaires facturés
     *
     * @return string
     */
    getAdditionalDaysBilled: function()
    {
        return this._tabDays[DAY_TYPE_ADDITIONAL].nbBilled;
    },

    /**
     * Récupérer le commentaire lié aux jours supplémentaires
     *
     * @return string
     */
    getAdditionalDaysComment : function()
    {
        return this._tabDays[DAY_TYPE_ADDITIONAL].comment;
    },

    /**
     * Récupérer la liste des jours supplémentaires
     */
    getAdditionalDaysList: function()
    {
        return this._tabDays[DAY_TYPE_ADDITIONAL].list;
    },

    /**
     * Récupérer la période possible pour les jours +
     */
    getAuthorizedPeriodAddDays: function()
    {
        return this._tabDays[DAY_TYPE_ADDITIONAL].authorizedPeriod;
    },

    /**
     * Récupérer l'intervalle de jours pour la période pour les jours +
     */
    getDeltasPeriodAddDays: function()
    {
        return this._tabDays[DAY_TYPE_ADDITIONAL].tabDeltasPeriodDays;
    },

    /**
     * Récupérer la valeur d'une configuration
     *
     * @return mixed
     */
    getConfigValue : function(name)
    {
        return this._tabCfgs[name];
    },

    /**
     * Récupérer le nombre de jours déduits facturés
     *
     * @return string
     */
    getDeductedDaysBilled  : function()
    {
        return this._tabDays[DAY_TYPE_DEDUCTED].nbBilled;
    },

    /**
     * Récupérer le commentaire lié aux jours déduits
     *
     * @return string
     */
    getDeductedDaysComment : function()
    {
        return this._tabDays[DAY_TYPE_DEDUCTED].comment;
    },

    /**
     * Récupérer la liste des jours supplémentaires
     */
    getDeductedDaysList: function()
    {
        return this._tabDays[DAY_TYPE_DEDUCTED].list;
    },

    /**
     * Récupérer la période possible pour les jours +
     */
    getAuthorizedPeriodDedDays: function()
    {
        return this._tabDays[DAY_TYPE_DEDUCTED].authorizedPeriod;
    },

    /**
     * Récupérer l'intervalle de jours pour la période pour les jours +
     */
    getDeltasPeriodDedDays: function()
    {
        return this._tabDays[DAY_TYPE_DEDUCTED].tabDeltasPeriodDays;
    },

    /**
     * Récupérer la liste complète de tous les jours +/-
     */
    getDaysList: function()
    {
        return this.getAdditionalDaysList().concat(this.getDeductedDaysList());
    },

    getDayInfos: function(id)
    {
        return this.getDaysList().find(function(item) {
            return item.id == id;
        });
    },

    /**
     * Récupérer le nombre total de jours supplémentaires
     *
     * @return float
     */
    getTotalAdditionalDays: function()
    {
        return this._tabDays[DAY_TYPE_ADDITIONAL].nbBilled +
               this._tabDays[DAY_TYPE_ADDITIONAL].nbUnbilled +
               this._getActionsDiffDuration(DAY_TYPE_ADDITIONAL);
    },

    /**
     * Récupérer le nombre total de jours supplémentaires et déduits
     *
     * @return float
     */
    getTotalDays: function()
    {
        return this.getTotalAdditionalDays() - this.getTotalDeductedDays();
    },

    /**
     * Récupérer le nombre total de jours déduits
     *
     * @return float
     */
    getTotalDeductedDays : function()
    {
        return this._tabDays[DAY_TYPE_DEDUCTED].nbBilled +
               this._tabDays[DAY_TYPE_DEDUCTED].nbUnbilled +
               this._getActionsDiffDuration(DAY_TYPE_DEDUCTED);
    },

    /**
     * Récupérer la durée de location en jours ouvrés (sans J+/-)
     */
    getRealDuration: function()
    {
        return this._realDuration;
    },

    /**
     * Récupérer la durée de location (en jours)
     *
     * @return int
     */
    getDuration : function()
    {
        return this._realDuration + this.getTotalDays();
    },

    /**
     * Récupérer la liste des motifs possibles pour les jours+/-
     */
    getDayReasonsList : function(dayType, isAppliedToContract)
    {
        if (dayType)
        {
            var documentObj = this.getDocumentObject();
            var useFlag = (dayType == DAY_TYPE_ADDITIONAL ? DAY_REASON_USEFLAG_ADDITIONALDAY : DAY_REASON_USEFLAG_DEDUCTEDDAY);
            if (documentObj.getObjectType() == 'contract' || isAppliedToContract)
            {
                useFlag |= DAY_REASON_USEFLAG_CONTRACT;
            }
            else
            {
                useFlag |= DAY_REASON_USEFLAG_ESTIMATE;
            }
            return this._tabCfgs.tabDayReasonsList.filter(function(item) {
                return (item.useFlags & useFlag) == useFlag;
            });
        }
        return this._tabCfgs.tabDayReasonsList;
    },

    /**
     * Récupérer la liste des durées possibles pour les jours +/-
     */
    getDayInvoiceDurationsList : function(dayType)
    {
        var flag = (dayType == DAY_TYPE_ADDITIONAL ? DAY_INVOICEDURATION_FLAG_ADDITIONALDAY : DAY_INVOICEDURATION_FLAG_DEDUCTEDDAY);
        return this._tabCfgs.tabDayInvoiceDurationsList.filter(function(item) {
            return (item.flags & flag) == flag;
        });
    },

    /**
     * Récupération des dates début max et fin min possible pour le document
     */
    getPeriodLimitDates: function()
    {
        var tabDays = this.getDaysList();

        var tabBeginDates = [];
        var tabEndDates   = [];
        for (var i = 0; i < tabDays.length; i++)
        {
            var day = tabDays[i];
            var action = this.getActionById(day.id);

            // - Jour non lié à un contrat ET
            // - pas d'action en cours OU action de type add ou setInvoiceable ou setNonInvoiceable
            if (!day.appliedToContract &&
                (!action || in_array(action.type, ['add', 'setInvoiceable', 'setNonInvoiceable'])))
            {
                if (day.beginDateMax)
                {
                    tabBeginDates.push(new Date(day.beginDateMax));
                }
                if (day.endDateMin)
                {
                    tabEndDates.push(new Date(day.endDateMin));
                }
            }
            // - Action de type edit en cours ET
            // - pas de contrat lié à l'enregistrement de la modif
            else if (action && action.type == 'edit' && !action.data.appliedToContractId)
            {
                if (action.beginDateMax)
                {
                    tabBeginDates.push(new Date(action.beginDateMax));
                }
                if (action.endDateMin)
                {
                    tabEndDates.push(new Date(action.endDateMin));
                }
            }
        }

        return {
            beginDateMax: (tabBeginDates.length > 0 ? new Date(Math.min.apply(Math, tabBeginDates)) : null),
            endDateMin:   (tabEndDates.length > 0 ? new Date(Math.max.apply(Math, tabEndDates)) : null)
        }
    },

    /**
     * Le tarif de base a-t-il été modifié ?
     *
     * @return bool
     */
    hasBaseAmountChanged : function()
    {
        // on n'est pas en création et le tarif de base a changé
        return (this._documentObj.getEditMode() == 'update' &&
                this.getExternalParamValue('hasBaseAmountChanged'));
    },

    /**
     * La date est à confimer ?
     *
     * @return bool
     */
    isDatesToConfirm : function()
    {
        return this._isDatesToConfirm;
    },

    /**
     * La durée de location a changé ?
     *
     * @return bool
     */
    isRealDurationChanged : function()
    {
        return this._isRealDurationChanged;
    },

    /**
     * Définir si la date est à confirmer
     *
     * @param bool isDatesToConfirm
     * @return bool
     */
    setDatesToConfirm : function(isActived)
    {
        isActived = (isActived ? true : false);
        if (!this.hasRight('datesToConfirm') || this._isDatesToConfirm == isActived)
        {
            return false;
        }
        this._isDatesToConfirm = isActived;

        if (this.onchange)
        {
            this.onchange(['isDatesToConfirm']);
        }
        return true;
    },

    /**
     * Récupérer un paramètre externe
     *
     * @param string name
     * @return mixed
     */
    getExternalParamValue : function(name)
    {
        return this._tabExtParams[name];
    },

    /**
     * Définir un ou plusieurs paramètres externes
     *
     * @param Object tab Paramètres à définir
     * @return bool
     */
    setExternalParams : function(tab)
    {
        var tabChanges = [];
        for (var name in tab)
        {
            if (this._tabExtParams.hasOwnProperty(name))
            {
                if (!LOC_Common.isEqual(tab[name], this._tabExtParams[name]))
                {
                    this._tabExtParams[name] = tab[name];
                    tabChanges.push('extParam_' + name);
                }
            }
        }
        if (tabChanges.length > 0 && this.onchange)
        {
            this.onchange(tabChanges);
        }
        return (tabChanges.length > 0);
    },

    /**
     * Vérifier le droit à un champ
     *
     * @return bool
     */
    hasRight : function(fieldName)
    {
        return (this._tabRights === null || this._tabRights[fieldName] ? true : false);
    },

    /**
     * Ajoute une action de type 'add'
     */
    addDay: function(type, isAppliedToContract, appliedToContractId, date, invoiceDurationId, reasonId, comment, isInvoiceable)
    {
        var mySelf = this;
        return new Promise(function(resolve, reject) {
            if (!mySelf.hasRight((type == DAY_TYPE_ADDITIONAL ? 'additionalDays' : 'deductedDays')))
            {
                reject([]);
            }
            else
            {
                mySelf._getDayExtraInfos(type, date, appliedToContractId).then(function(extraInfos) {

                    var periodLimitDates       = extraInfos ? extraInfos.periodLimitDates : null;
                    var appliedToContractInfos = extraInfos ? extraInfos.appliedToContractInfos : null;

                    // - Vérification des données
                    var tabErrors = [];
                    var tabReturn = {};
                    // - Contrat lié
                    if (isAppliedToContract && !appliedToContractId)
                    {
                        tabErrors.push('noAppliedToContract');
                    }
                    // -- Date
                    if (date == '' && (mySelf.getDocumentObject().getObjectType() == 'contract' || appliedToContractId))
                    {
                        tabErrors.push('noDate');
                    }
                    else if (date != '' && !mySelf.isDateDayPossible(null, type, date, appliedToContractInfos, tabReturn))
                    {
                        tabErrors.push(tabReturn.error);
                    }
                    // -- Motif
                    if (reasonId == 0)
                    {
                        tabErrors.push('noReason');
                    }

                    if (tabErrors.length > 0)
                    {
                        reject(tabErrors);
                    }
                    else
                    {
                        // - Liste des ajouts en cours
                        var day = {
                            id:                null,
                            type:              type,
                            appliedToContract: appliedToContractInfos,
                            date:              date,
                            invoiceDuration:   mySelf._getDayInvoiceDuration(invoiceDurationId),
                            reason:            mySelf._getDayReason(reasonId),
                            comment:           comment,
                            beginDateMax:      (periodLimitDates ? periodLimitDates.beginDateMax : null),
                            endDateMin:        (periodLimitDates ? periodLimitDates.endDateMin : null),
                            isInvoiceable:     isInvoiceable,
                            isInvoiced:        false
                        };
                        mySelf._tabDays[type].list.unshift(day);

                        var action = mySelf._moveToAdded(day);
                        if (mySelf.onchange)
                        {
                            mySelf.onchange(['addDay']);
                        }

                        resolve(action);
                    }
                }, function() {
                    reject([]);
                });
            }
        });
    },

    /**
     * Ajoute une action de type 'edit'
     */
    editDay: function(id, isAppliedToContract, appliedToContractId, date, invoiceDurationId, reasonId, comment, isInvoiceable)
    {
        var mySelf = this;
        return new Promise(function(resolve, reject) {
            if (!mySelf.isEditable(id))
            {
                reject([]);
            }
            else
            {
                var day = mySelf.getDayInfos(id);

                mySelf._getDayExtraInfos(day.type, date, appliedToContractId).then(function(extraInfos) {

                    var periodLimitDates       = (extraInfos ? extraInfos.periodLimitDates : null);
                    var appliedToContractInfos = (extraInfos ? extraInfos.appliedToContractInfos : null);

                    // - Vérification des données
                    var tabErrors = [];
                    var tabReturn = {};
                    // - Contrat lié
                    if (isAppliedToContract && !appliedToContractId)
                    {
                        tabErrors.push('noAppliedToContract');
                    }
                    // -- Date
                    if (date == '' && (mySelf.getDocumentObject().getObjectType() == 'contract' || appliedToContractId))
                    {
                        tabErrors.push('noDate');
                    }
                    else if (date != '' && !mySelf.isDateDayPossible(id, day.type, date, appliedToContractInfos, tabReturn))
                    {
                        tabErrors.push(tabReturn.error);
                    }
                    // -- Motif
                    if (reasonId == 0)
                    {
                        tabErrors.push('noReason');
                    }

                    if (tabErrors.length > 0)
                    {
                        reject(tabErrors);
                    }
                    else
                    {
                        var action = mySelf.getActionById(id);

                        var data = {
                            type                : day.type,
                            appliedToContractId : (appliedToContractInfos ? appliedToContractInfos.id : null),
                            date                : date,
                            invoiceDurationId   : invoiceDurationId,
                            reasonId            : reasonId,
                            comment             : comment,
                            isInvoiceable       : isInvoiceable
                        };
                        var beginDateMax = (periodLimitDates ? periodLimitDates.beginDateMax: null);
                        var endDateMin   = (periodLimitDates ? periodLimitDates.endDateMin:   null);

                        if (action)
                        {
                            // Modification de l'action d'ajout existante et de la ligne du jour
                            action.data              = data;
                            action.beginDateMax      = beginDateMax;
                            action.endDateMin        = endDateMin;
                            action.appliedToContract = appliedToContractInfos;

                            day.appliedToContract = appliedToContractInfos;
                            day.date              = date;
                            day.invoiceDuration   = mySelf._getDayInvoiceDuration(invoiceDurationId);
                            day.reason            = mySelf._getDayReason(reasonId);
                            day.comment           = comment;
                            day.beginDateMax      = beginDateMax;
                            day.endDateMin        = endDateMin;
                            day.isInvoiceable     = isInvoiceable;
                        }
                        else
                        {
                            // Nouvelle action d'édition
                            action = {
                                type:              'edit',
                                id:                id,
                                data:              data,
                                beginDateMax:      beginDateMax,
                                endDateMin:        endDateMin,
                                appliedToContract: appliedToContractInfos
                            };
                            mySelf._tabDaysActions.push(action);
                        }

                        if (mySelf.onchange)
                        {
                            mySelf.onchange(['editDay']);
                        }

                        resolve(action);
                    }
                }, function() {
                    reject([]);
                });
            }
        });
    },

    /**
     * Ajoute une action de type 'delete'
     */
    removeDay: function(id)
    {
        var mySelf = this;
        return new Promise(function(resolve, reject) {
            if (!mySelf.isDeletable(id))
            {
                reject([]);
            }
            else
            {
                // - Tableau des actions
                var action = {
                    type: 'remove',
                    id:   id
                };
                mySelf._tabDaysActions.push(action);

                if (mySelf.onchange)
                {
                    mySelf.onchange(['removeDay']);
                }

                resolve(action);
            }
        });
    },

    /**
     * Facturer un jour
     */
    setInvoiceableDay: function(id)
    {
        var mySelf = this;
        return new Promise(function(resolve, reject) {
            if (!mySelf.isInvoiceablePossible(id))
            {
                reject([]);
            }
            else
            {
                // - Tableau des actions
                var action = {
                    type: 'setInvoiceable',
                    id:   id
                };
                mySelf._tabDaysActions.push(action);

                if (mySelf.onchange)
                {
                    mySelf.onchange(['setInvoiceableDay']);
                }

                resolve(action);
            }
        });
    },

    /**
     * Ne pas facturer un jour
     */
    setNonInvoiceableDay: function(id)
    {
        var mySelf = this;
        return new Promise(function(resolve, reject) {
            if (!mySelf.isNonInvoiceablePossible(id))
            {
                reject([]);
            }
            else
            {
                // - Tableau des actions
                var action = {
                    type: 'setNonInvoiceable',
                    id:   id
                }
                mySelf._tabDaysActions.push(action);

                if (mySelf.onchange)
                {
                    mySelf.onchange(['setNonInvoiceableDay']);
                }

                resolve(action);
            }
        });
    },

    /**
     * Annuler les actions sur un jour
     */
    undoDay: function(id)
    {
        var mySelf = this;
        return new Promise(function(resolve, reject) {
            var index = mySelf._tabDaysActions.findIndex(function(item) { return item.id == id; });
            if (index != -1)
            {
                var action = mySelf._tabDaysActions[index];

                // Annuler un ajout
                if (action.type == 'add')
                {
                    var list = mySelf._tabDays[action.data.type].list;
                    var dayIndex = list.findIndex(function(item) { return item.id == id; });
                    if (dayIndex != -1)
                    {
                        list.splice(dayIndex, 1);
                    }
                }

                mySelf._tabDaysActions.splice(index, 1);

                if (mySelf.onchange)
                {
                    mySelf.onchange(['undoDay']);
                }

                resolve(action);
            }
            else
            {
                reject();
            }
        });
    },

    isEditable: function(id)
    {
        var action = this.getActionById(id);
        if (action && action.type != 'add')
        {
            return false;
        }

        var day = this.getDayInfos(id);
        return this.hasRight((day.type == DAY_TYPE_ADDITIONAL ? 'additionalDays' : 'deductedDays')) && !day.isInvoiced;
    },

    isDeletable: function(id)
    {
        if (this.getActionById(id))
        {
            return false;
        }

        var day = this.getDayInfos(id);
        return this.hasRight((day.type == DAY_TYPE_ADDITIONAL ? 'additionalDays' : 'deductedDays')) && !day.isInvoiced;
    },

    isInvoiceablePossible: function(id)
    {
        if (this.getActionById(id))
        {
            return false;
        }

        var day = this.getDayInfos(id);
        return this.hasRight((day.type == DAY_TYPE_ADDITIONAL ? 'additionalDays' : 'deductedDays')) &&
               !this.getExternalParamValue('disabledComponents').setInvoiceable &&
               !day.isInvoiced &&
               !day.isInvoiceable &&
               (!day.date || this.isDateDayPossible(id, day.type, day.date, day.appliedToContract));
    },

    isNonInvoiceablePossible: function(id)
    {
        if (this.getActionById(id))
        {
            return false;
        }

        var day = this.getDayInfos(id);
        return this.hasRight((day.type == DAY_TYPE_ADDITIONAL ? 'additionalDays' : 'deductedDays')) &&
               !this.getExternalParamValue('disabledComponents').setNonInvoiceable &&
               !day.isInvoiced && day.isInvoiceable;
    },

    isUndoable: function(id)
    {
        var day = this.getDayInfos(id);
        var action = this.getActionById(id);
        // Vérification des dates pour savoir si annuler l'action ne provoque pas un conflit de date
        return !!action &&
                (day && (!day.date || this.isDateDayPossible(id, day.type, day.date, day.appliedToContract)) ||
                 in_array(action.type, ['add', 'setInvoiceable']));
    },

    // Est-ce que la date est possible par rapport à la période et aux jours déjà saisis ?
    isDateDayPossible: function(id, dayType, dayDate, appliedToContractInfos, tabReturn)
    {
        if (!tabReturn)
        {
            tabReturn = {};
        }
        var beginDate, endDate;
        var hasDay    = false;
        var tabDays = [];
        var currentAppliedToContractId = (appliedToContractInfos ? appliedToContractInfos.id : null);
        var tabDaysDate = [];

        // Faut-il vérifier l'existence de jours ?
        // - on est sur un contrat
        // - OU cela ne concerne pas un contrat lié
        var isCheckExistingDays = (this.getDocumentObject().getObjectType() == 'contract' || !currentAppliedToContractId);

        if (dayType == DAY_TYPE_ADDITIONAL)
        {
            var period = currentAppliedToContractId ? appliedToContractInfos.authorizedPeriodAddDays :
                                                      this.getAuthorizedPeriodAddDays();
            beginDate = new Date(period[0]);
            endDate   = new Date(period[1]);
            tabDays   = this.getAdditionalDaysList();
            if (currentAppliedToContractId && isCheckExistingDays)
            {
                for (var i = 0; i < appliedToContractInfos.tabDays[DAY_TYPE_ADDITIONAL].length; i++)
                {
                    if (appliedToContractInfos.tabDays[DAY_TYPE_ADDITIONAL][i].id != id)
                    {
                        tabDaysDate.push(appliedToContractInfos.tabDays[DAY_TYPE_ADDITIONAL][i].date);
                    }
                }
            }
        }
        else
        {
            var period = currentAppliedToContractId ? appliedToContractInfos.authorizedPeriodDedDays :
                                                      this.getAuthorizedPeriodDedDays();
            beginDate = new Date(period[0]);
            endDate   = new Date(period[1]);
            tabDays   = this.getDeductedDaysList();
            if (currentAppliedToContractId && isCheckExistingDays)
            {
                for (var i = 0; i < appliedToContractInfos.tabDays[DAY_TYPE_DEDUCTED].length; i++)
                {
                    if (appliedToContractInfos.tabDays[DAY_TYPE_DEDUCTED][i].id != id)
                    {
                        tabDaysDate.push(appliedToContractInfos.tabDays[DAY_TYPE_DEDUCTED][i].date);
                    }
                }
            }
        }

        for (var i = 0; i < tabDays.length; i++)
        {
            // Vérification des jours
            if (id != tabDays[i].id)
            {
                var date                = tabDays[i].date;
                var isInvoiceable       = tabDays[i].isInvoiceable;
                var appliedToContractId = (tabDays[i].appliedToContract ? tabDays[i].appliedToContract.id : null);
                var isDeleted           = false;

                var action = this.getActionById(tabDays[i].id);
                if (action)
                {
                    switch(action.type)
                    {
                        case 'edit':
                            appliedToContractId = action.data.appliedToContractId;
                            date                = action.data.date;
                            break;

                        case 'setInvoiceable':
                            isInvoiceable = true;
                            break;

                        case 'setNonInvoiceable':
                            isInvoiceable = false;
                            break;

                        case 'remove':
                            isDeleted = true;
                            break
                    }
                }
                if (isInvoiceable && !isDeleted && currentAppliedToContractId == appliedToContractId)
                {
                    tabDaysDate.push(date);
                }
            }
        }

        hasDay = (tabDaysDate.indexOf(dayDate) > -1);
        var dateValue = new Date(dayDate);

        if (hasDay)
        {
            tabReturn.error = 'existingDay';
        }
        else if (dateValue < beginDate || dateValue > endDate)
        {
            tabReturn.error = 'dateNotInPeriod';
        }

        return !tabReturn.error;
    },

    /**
     * Récupérer l'action d'un jour
     */
    getActionById: function(id)
    {
        return this._tabDaysActions.find(function(item){
            return (item.id == id);
        });
    },

    _getDayInvoiceDuration: function(id) {
        var item = this._tabCfgs.tabDayInvoiceDurationsList.find(function(item) {
            return item.id == id;
        });
        return (item ? item : null);
    },

    _getDayReason: function(id) {
        var item = this._tabCfgs.tabDayReasonsList.find(function(item) {
            return item.id == id;
        });
        return (item ? item : null);
    },

    _getActionsDiffDuration : function(type)
    {
        var mySelf = this;
        return this._tabDaysActions.reduce(function(sum, action) {
            var day = mySelf._tabDays[type].list.find(function(day) { return day.id == action.id; });
            if (day)
            {
                if (action.type == 'setInvoiceable' || (action.type == 'add' && day.isInvoiceable))
                {
                    sum += day.invoiceDuration.duration || 0;
                }
                else if (action.type == 'setNonInvoiceable' || ((action.type == 'remove' || action.type == 'edit') && day.isInvoiceable))
                {
                    sum -= day.invoiceDuration.duration || 0;
                }

                if (action.type == 'edit' && day.isInvoiceable)
                {
                    var oldInvoiceDuration = mySelf._getDayInvoiceDuration(action.data.invoiceDurationId);
                    sum += oldInvoiceDuration.duration || 0;
                }
            }
            return sum;
        }, 0);
    },

    /**
     * Mets à jour les informations de durée, durée calendaire et tranche de durée demandées
     *
     * @param bool isToOpen
     * @return bool
     */
    _updateRequiredPeriodInfos : function(isToOpen, mode)
    {
        if ((!this.hasRight('beginDate') && !this.hasRight('endDate')) ||
            (this.getExternalParamValue('disabledComponents').beginDate && this.getExternalParamValue('disabledComponents').endDate) ||
            this.getExternalParamValue('disabledComponents').period)
        {
            return false;
        }

        var pfx = this._prefix;
        var isOpened = this._isModalOpened();
        if ((isToOpen ? isOpened : !isOpened))
        {
            return false;
        }
        var mySelf = this;
        if (isToOpen)
        {
            Location.modalWindowManager.show(pfx + 'period', {contentType: 3, width: 370, height: 'auto'});
            Location.Rent_Helper_DurationView.displayRentalPeriod(this._prefix, this);
            try
            {
                $ge(pfx + 'periodRequiredBeginDate.masked').focus();
            }
            catch(ex)
            {
            }
        }
        if (this._requiredBeginDate == '' || ((mode == 1 && this._requiredEndDate == '') || (mode == 2 && this._requiredRealDuration <= 0)))
        {
            return false;
        }
        $ge(pfx + 'periodValidBtn').className = 'locCtrlButton disabled loading';
        $ge(pfx + 'periodValidBtn').style.visibility = 'visible';
        $ge(pfx + 'periodRequiredRealDurationBtn').style.visibility = 'hidden';
        Location.modalWindowManager.changeStyle('loading');
        if (!this._httpObj)
        {
            this._httpObj = new Location.HTTPRequest(this.getConfigValue('rentalPeriodInfosUrl'));
        }
        this._httpObj.setVar('beginDate', this.getBeginDate());
        this._httpObj.setVar('endDate', this.getEndDate());
        this._httpObj.setVar('requiredBeginDate', this._requiredBeginDate);
        if (mode == 1)
        {
            this._httpObj.setVar('requiredEndDate', this._requiredEndDate);
            this._httpObj.setVar('requiredRealDuration', 0);
        }
        else
        {
            this._httpObj.setVar('requiredEndDate', '');
            this._httpObj.setVar('requiredRealDuration', this._requiredRealDuration);
        }
        this._httpObj.setVar('beginDateWindow', this.getExternalParamValue('beginDateWindow'));
        this._httpObj.setVar('endDateWindow', this.getExternalParamValue('endDateWindow'));
        this._httpObj.setVar('adjustDays', this.getTotalDays());
        var docObj = (this._documentObj.getObjectType() == 'estimateLine' ? this._documentObj.getEstimateObject() : this._documentObj);
        this._httpObj.setVar('agencyId', docObj.getAgencyId());
        this._httpObj.setVar('countryId', docObj.getCountryId());
        this._httpObj.setVar('entityId', docObj.getId());
        this._httpObj.setVar('valueDate', docObj.getValueDate());
        this._httpObj.oncompletion = function()
        {
            var tab = this.getJSONResponse();
            if (tab)
            {
                // calcul de la durée réelle en prenant compte les j+ et j-
                mySelf._durationStr                     = tab['durationStr'];
                mySelf._requiredBeginDate               = tab['requiredBeginDate'];
                mySelf._requiredEndDate                 = tab['requiredEndDate'];
                mySelf._requiredRealDuration            = tab['requiredRealDuration'];
                mySelf._requiredDurationStr             = tab['requiredDurationStr'];
                mySelf._requiredCalendarDuration        = tab['requiredCalendarDuration'];
                mySelf._rentTariffDurRangeLabel         = tab['rentTariffDurRangeLabel'];
                mySelf._requiredRentTariffDurRangeLabel = tab['requiredRentTariffDurRangeLabel'];

                mySelf._tabDays[DAY_TYPE_ADDITIONAL].authorizedPeriod = tab['authorizedPeriodAddDays'];
                mySelf._tabDays[DAY_TYPE_DEDUCTED].authorizedPeriod   = tab['authorizedPeriodDedDays'];

                Location.Rent_Helper_DurationView.displayRentalPeriod(mySelf._prefix, mySelf);

                Location.modalWindowManager.changeStyle('');
                $ge(pfx + 'periodValidBtn').className = 'locCtrlButton';
                $ge(pfx + 'periodValidBtn').style.visibility = 'visible';
                $ge(pfx + 'periodRequiredEndDateRow').className = 'dateMode';
                $ge(pfx + 'periodRequiredRealDurationBtn').style.visibility = 'visible';

                // Evénement
                if (mySelf.onupdaterequiredperiod)
                {
                    mySelf.onupdaterequiredperiod();
                }
            }
            else
            {
                this.onerror();
            }
        }
        this._httpObj.onerror = function()
        {
            if (this.responseStatus != 0)
            {
                Location.modalWindowManager.changeStyle('error');
                $ge(pfx + 'periodValidBtn').style.visibility = 'hidden';
            }
            return false;
        }
        this._httpObj.send(true, true);
        return true;
    },

    _validate : function()
    {
        if (this._httpObj && this._httpObj.isActive())
        {
            return false;
        }

        // Fermeture de la popup de modification de la période de location
        Location.modalWindowManager.hide();

        if (this._requiredBeginDate == '' || this._requiredEndDate == '')
        {
            return false;
        }

        var tabChanges = [];

        if (this._requiredBeginDate != this._beginDate)
        {
            tabChanges.push('beginDate');
            this._beginDate = this._requiredBeginDate;
        }
        if (this._requiredEndDate != this._endDate)
        {
            tabChanges.push('endDate');
            this._endDate = this._requiredEndDate;
        }
        if (this._realDuration != this._requiredRealDuration)
        {
            tabChanges.push('realDuration');
            this._realDuration = this._requiredRealDuration;
        }
        if (this._calendarDuration != this._requiredCalendarDuration)
        {
            tabChanges.push('calendarDuration');
            this._calendarDuration     = this._requiredCalendarDuration;
        }
        this._durationStr             = this._requiredDurationStr;
        this._rentTariffDurRangeLabel = this._requiredRentTariffDurRangeLabel;

        if (tabChanges.length > 0)
        {
            if (this.onchange)
            {
                this.onchange(tabChanges);
            }
        }
        return true;
    },

    _isModalOpened : function()
    {
        return (Location.modalWindowManager.getCurrentContent() == this._prefix + 'period');
    },


    /**
     * Récupération de date début max et fin min pour un jour
     */
    _getDayExtraInfos: function(type, date, appliedToContractId)
    {
        var mySelf = this;
        return new Promise(function(resolve, reject) {
            if (date != '')
            {
                // Initialisation de l'objet HTTPRequest
                if (!mySelf._getDayExtraInfosHttpObj)
                {
                    mySelf._getDayExtraInfosHttpObj = new Location.HTTPRequest(mySelf.getDayExtraInfosUrl());
                }

                mySelf._getDayExtraInfosHttpObj.setVar('type', type);
                mySelf._getDayExtraInfosHttpObj.setVar('date', date);
                mySelf._getDayExtraInfosHttpObj.setVar('appliedToContractId', appliedToContractId);
                var docObj = (mySelf._documentObj.getObjectType() == 'estimateLine' ? mySelf._documentObj.getEstimateObject() : mySelf._documentObj);
                mySelf._getDayExtraInfosHttpObj.setVar('agencyId', docObj.getAgencyId());
                mySelf._getDayExtraInfosHttpObj.setVar('valueDate', docObj.getValueDate());

                // Réception des données
                mySelf._getDayExtraInfosHttpObj.oncompletion = function()
                {
                    resolve(this.getJSONResponse());
                };

                // Erreur
                mySelf._getDayExtraInfosHttpObj.onerror = function()
                {
                    reject();
                };

                // Envoi de la requête HTTP
                mySelf._getDayExtraInfosHttpObj.send(true, true);
            }
            else
            {
                resolve(null);
            }
        });
    },

    _moveToAdded: function(day)
    {
        day.id = 'add-' + Location.Rent_Helper_Duration._addCpt++;
        var action = {
            type: 'add',
            id: day.id,
            data: {
                type                : day.type,
                appliedToContractId : (day.appliedToContract ? day.appliedToContract.id : null),
                date                : day.date,
                invoiceDurationId   : day.invoiceDuration.id,
                reasonId            : day.reason.id,
                comment             : day.comment,
                isInvoiceable       : day.isInvoiceable
            }
        };
        this._tabDaysActions.push(action);
        return action;
    }
}


Location.Rent_Helper_DurationView = {

    _isMoreDisplayed: false,
    _tabPasses : {},
    _listInvoiceDurationsClone: null,
    _listReasonsClone: null,

    display : function(pfx, obj)
    {
        var disabledComponents = obj.getExternalParamValue('disabledComponents');

        var realDuration = obj.getRealDuration();

        // Badge du bloc "Période de location"
        var title = (realDuration > 1 ? LOC_View.getTranslation(pfx + 'title_rentalPeriod_p') : LOC_View.getTranslation(pfx + 'title_rentalPeriod_s'));
        $ge(pfx + 'badgeDuration').title = title.replace(/<%duration>/, realDuration);

        // Affichage
        $ge(pfx + 'badgeDuration').innerHTML    = LOC_View.getNumberFormat(realDuration, (realDuration % 1 == 0 ? 0 : 1));
        $ge(pfx + 'duration').innerHTML         = LOC_View.getNumberFormat(obj.getDuration(), (obj.getDuration() % 1 == 0 ? 0 : 1));
        $ge(pfx + 'calendarDuration').innerHTML = LOC_View.getNumberFormat(obj.getCalendarDuration(), (obj.getCalendarDuration() % 1 == 0 ? 0 : 1));
        $ge(pfx + 'beginDate').innerHTML        = LOC_View.getDateFormat(obj.getBeginDate());
        $ge(pfx + 'endDate').innerHTML          = LOC_View.getDateFormat(obj.getEndDate());

        // - Affichage flèche entre date de début et date de fin
        if (obj.getBeginDate() && obj.getEndDate())
        {
            Location.commonObj.addEltClass($ge(pfx + 'endDate'), 'hasDate');
        }

        var isDatesLocked = ((!obj.hasRight('beginDate') && !obj.hasRight('endDate')) ||
                             (disabledComponents.beginDate && disabledComponents.endDate) ||
                             disabledComponents.period ? true : false);
        var isDatesOK = (obj.getBeginDate() != '' && obj.getEndDate() != '');

        $ge(pfx + 'periodBtn').style.visibility = (isDatesLocked ? 'hidden' : '');

        // Affichage du flag "Durée de location a changé"
        $ge(pfx + 'durationChanged').style.display = (obj.isRealDurationChanged() ? 'inline-block' : 'none');

        // Affichage du message "La tranche de durée a changé"
        if ($ge(pfx + 'hasChangedBaseAmount'))
        {
            $ge(pfx + 'hasChangedBaseAmount').style.display = (obj.hasBaseAmountChanged() ? 'inline-block' : 'none');
        }

        // Dates à confirmer
        $ge(pfx + 'datesToConfirm').checked = obj.isDatesToConfirm();
        $ge(pfx + 'datesToConfirm').disabled = (!isDatesOK || !obj.hasRight('datesToConfirm'));

        // Jours + et -
        var disabledAdditionalDays = (!isDatesOK || !obj.hasRight('additionalDays') || disabledComponents.additionalDays ? true : false);
        var disabledDeductedDays = (!isDatesOK || !obj.hasRight('deductedDays') || disabledComponents.deductedDays ? true : false);
        Location.commonObj.toggleEltClass($ge(pfx + 'addAdditionalDayDropdown'), 'disabled', disabledAdditionalDays);
        Location.commonObj.toggleEltClass($ge(pfx + 'addAdditionalDayBtn'), 'disabled', disabledAdditionalDays);
        Location.commonObj.toggleEltClass($ge(pfx + 'addAdditionalDayOnContractBtn'), 'disabled', disabledAdditionalDays);
        Location.commonObj.toggleEltClass($ge(pfx + 'addDeductedDayDropdown'), 'disabled', disabledDeductedDays);
        Location.commonObj.toggleEltClass($ge(pfx + 'addDeductedDayBtn'), 'disabled', disabledDeductedDays);
        Location.commonObj.toggleEltClass($ge(pfx + 'addDeductedDayOnContractBtn'), 'disabled', disabledDeductedDays);

        // - Badge du bloc "Jours supplémentaires" : nombre total de jours
        var totalAdditionalDays = obj.getTotalAdditionalDays();
        var title = (totalAdditionalDays > 1 ? LOC_View.getTranslation(pfx + 'title_totalAdditionalDays_p') : LOC_View.getTranslation(pfx + 'title_totalAdditionalDays_s'));
        $ge(pfx + 'badgeAdditionalDays').title      = title.replace(/<%additionalDays>/, totalAdditionalDays);
        $ge(pfx + 'badgeAdditionalDays').innerHTML  = LOC_View.getNumberFormat(totalAdditionalDays, (totalAdditionalDays % 1 == 0 ? 0 : 1));;

        // - Badge du bloc "Jours supplémentaires" : nombre de jours facturés
        if($ge(pfx + 'badgeAdditionalDaysInvoiced'))
        {
            var additionalDaysBilled = obj.getAdditionalDaysBilled();
            var title = (additionalDaysBilled > 1 ? LOC_View.getTranslation(pfx + 'title_additionalDaysInvoiced_p') : LOC_View.getTranslation(pfx + 'title_additionalDaysInvoiced_s'));
            $ge(pfx + 'badgeAdditionalDaysInvoiced').title      = title.replace(/<%additionalDaysInvoiced>/, additionalDaysBilled);
            $ge(pfx + 'badgeAdditionalDaysInvoiced').innerHTML  = LOC_View.getNumberFormat(additionalDaysBilled, (additionalDaysBilled % 1 == 0 ? 0 : 1));
        }

        // Jours plus
        var additionalDaysComment = obj.getAdditionalDaysComment();
        var commentEl = $ge(pfx + 'additionalDaysComment');
        Location.commonObj.toggleEltClass(commentEl, 'active', (additionalDaysComment ? true : false));
        if (additionalDaysComment)
        {
            additionalDaysComment = additionalDaysComment.replace(/\n/g, '<br />');
            commentEl.onmouseover = function() {
                Location.toolTipsObj.show(additionalDaysComment, {corner:'tl'});
            };
            commentEl.onmouseout = function() {
                Location.toolTipsObj.hide();
            };
        }

        this._displayTabDays(pfx, obj, obj.getAdditionalDaysList(), DAY_TYPE_ADDITIONAL);

        // Jours moins
        // - Badge du bloc "Jours déduits" : nombre total de jours
        var totalDeductedDays = obj.getTotalDeductedDays();
        var title = (totalDeductedDays > 1 ? LOC_View.getTranslation(pfx + 'title_totalDeductedDays_p') : LOC_View.getTranslation(pfx + 'title_totalDeductedDays_s'));
        $ge(pfx + 'badgeDeductedDays').title      = title.replace(/<%deductedDays>/, totalDeductedDays);
        $ge(pfx + 'badgeDeductedDays').innerHTML  = LOC_View.getNumberFormat(totalDeductedDays, (totalDeductedDays % 1 == 0 ? 0 : 1));;

        // - Badge du bloc "Jours déduits" : nombre de jours facturés
        if ($ge(pfx + 'badgeDeductedDaysInvoiced'))
        {
            var deductedDaysBilled = obj.getDeductedDaysBilled();
            var title = (deductedDaysBilled > 1 ? LOC_View.getTranslation(pfx + 'title_deductedDaysInvoiced_p') : LOC_View.getTranslation(pfx + 'title_deductedDaysInvoiced_s'));
            $ge(pfx + 'badgeDeductedDaysInvoiced').title      = title.replace(/<%deductedDaysInvoiced>/, deductedDaysBilled);
            $ge(pfx + 'badgeDeductedDaysInvoiced').innerHTML  = LOC_View.getNumberFormat(deductedDaysBilled, (deductedDaysBilled % 1 == 0 ? 0 : 1));;
        }

        var deductedDaysComment = obj.getDeductedDaysComment();
        var commentEl = $ge(pfx + 'deductedDaysComment');
        Location.commonObj.toggleEltClass(commentEl, 'active', (deductedDaysComment ? true : false));
        if (deductedDaysComment)
        {
            deductedDaysComment = deductedDaysComment.replace(/\n/g, '<br />');
            commentEl.onmouseover = function() {
                Location.toolTipsObj.show(deductedDaysComment, {corner:'tl'});
            };
            commentEl.onmouseout = function() {
                Location.toolTipsObj.hide();
            };
        }

        this._displayTabDays(pfx, obj, obj.getDeductedDaysList(), DAY_TYPE_DEDUCTED);

    },

    initEvents : function(pfx, obj)
    {
        var mySelf = this;

        var tabExternalDaysParams = {contentType: 3, width: 600, height: 'auto'};

        // - Ouverture de la popup des jours supplémentaires saisis sur un autre contrat
        $ge(pfx + 'externalAddDaysNotif').onclick = function()
        {
            Location.modalWindowManager.show(pfx + 'externalAddDaysPopup', tabExternalDaysParams);
        };

        // - Ouverture de la popup des jours déduits saisis sur un autre contrat
        $ge(pfx + 'externalDedDaysNotif').onclick = function()
        {
            Location.modalWindowManager.show(pfx + 'externalDedDaysPopup', tabExternalDaysParams);
        };

        // - Ouverture de la popup d'ajout d'un jour supplémentaire
        $ge(pfx + 'addAdditionalDayBtn').onclick = function()
        {
            if (!Location.commonObj.hasEltClass(this, 'disabled'))
            {
                mySelf._showAddDayPopup(pfx, obj, DAY_TYPE_ADDITIONAL, false);
            }
        };

        // - Ouverture de la popup d'ajout d'un jour supplémentaire
        $ge(pfx + 'addAdditionalDayOnContractBtn').onclick = function()
        {
            if (!Location.commonObj.hasEltClass(this, 'disabled'))
            {
                mySelf._showAddDayPopup(pfx, obj, DAY_TYPE_ADDITIONAL, true);
            }
        };

        // - Ouverture de la popup d'ajout d'un jour déduit
        $ge(pfx + 'addDeductedDayBtn').onclick = function()
        {
            if (!Location.commonObj.hasEltClass(this, 'disabled'))
            {
                mySelf._showAddDayPopup(pfx, obj, DAY_TYPE_DEDUCTED, false);
            }
        };

        // - Ouverture de la popup d'ajout d'un jour déduit
        $ge(pfx + 'addDeductedDayOnContractBtn').onclick = function()
        {
            if (!Location.commonObj.hasEltClass(this, 'disabled'))
            {
                mySelf._showAddDayPopup(pfx, obj, DAY_TYPE_DEDUCTED, true);
            }
        };

        // - Bouton de modification de la période de location
        $ge(pfx + 'periodBtn').onclick = function()
        {
            $ge(pfx + 'periodRequiredEndDateRow').className = (obj.getConfigValue('rentalPeriodEndDateMode') == 'days' && obj._requiredEndDate == '' ? 'daysMode' : 'dateMode');
            obj._updateRequiredPeriodInfos(true, 1);
        }
        // - Modification de la date de début demandée pour la popup
        var el = $ge(pfx + 'periodRequiredBeginDate.masked');
        el.onvalidate = function(date, dateFormated)
        {
            if (obj._requiredBeginDate != date)
            {
                // si la date de début n'est pas désactivée
                if (!obj.getExternalParamValue('disabledComponents').beginDate)
                {
                    obj._requiredBeginDate = date;
                }
                // dans tous les cas on vérifie les dates (permet de remettre la bonne date si celle-ci était désactivée)
                obj._updateRequiredPeriodInfos(false, 1);
            }
        }

        // - Modification de la coche date à confirmer
        $ge(pfx + 'datesToConfirm').onchange = function()
        {
            obj.setDatesToConfirm(this.checked);
            this.checked = obj.isDatesToConfirm();
        }

        // - Modification de la date de fin demandée pour la popup
        el = $ge(pfx + 'periodRequiredEndDate.masked');
        el.onvalidate = function(date, dateFormated)
        {
            if (obj._requiredEndDate != date)
            {
                // si la date de fin n'est pas désactivée
                if (!obj.getExternalParamValue('disabledComponents').endDate)
                {
                    obj._requiredEndDate = date;
                }
                // dans tous les cas, on vérifie les dates (permet de remettre la bonne date si celle-ci était désactivée)
                obj._updateRequiredPeriodInfos(false, 1);
            }
        }
        // - Bouton de validation de modification la période de location
        el = $ge(pfx + 'periodRequiredRealDurationBtn').onclick = function()
        {
            obj._requiredRealDuration = Location.spinControlsManager.getSpinControl(pfx + 'periodRequiredRealDuration').getValue();
            obj._updateRequiredPeriodInfos(false, 2);
        }
        Location.spinControlsManager.getSpinControl(pfx + 'periodRequiredRealDuration').onkeydown = function(e)
        {
            e = e || window.event;
            var keyCode = (e.keyCode || e.which);
            if (keyCode == 9)
            {
                try
                {
                    $ge(pfx + 'periodRequiredBeginDate.masked').focus();
                }
                catch(ex)
                {
                }
                return false;
            }
        }
        // - Bouton de validation de modification la période de location
        el = $ge(pfx + 'periodValidBtn').onclick = function()
        {
            obj._validate();
        }

        // On affecte ces évenements qu'une seule fois
        if (!this._tabPasses[pfx])
        {
            Location.commonObj.addEltEvent(pfx + 'periodRequiredBeginDate.masked', 'keydown', function(e)
            {
                e = e || window.event;
                var keyCode = (e.keyCode || e.which);
                if (keyCode == 9)
                {
                    Location.spinControlsManager.getSpinControl(pfx + 'periodRequiredRealDuration').focus();
                    try
                    {
                        $ge(pfx + 'periodRequiredEndDate.masked').focus();
                    }
                    catch(ex)
                    {
                    }
                    Location.commonObj.cancelEvent(e);
                    return false;
                }
            });
            Location.commonObj.addEltEvent(pfx + 'periodRequiredEndDate.masked', 'keydown', function(e)
            {
                e = e || window.event;
                var keyCode = (e.keyCode || e.which);
                if (keyCode == 9)
                {
                    try
                    {
                        $ge(pfx + 'periodRequiredBeginDate.masked').focus();
                    }
                    catch(ex)
                    {
                    }
                    Location.commonObj.cancelEvent(e);
                    return false;
                }
            });
        }
        this._tabPasses[pfx] = true;
    },

    /**
     * Mettre à jour l'affichage pour la popup de modification de la période de location
     */
    displayRentalPeriod : function (pfx, obj)
    {
        $ge(pfx + 'periodBeginDate').innerHTML = LOC_View.getDateFormat(obj.getBeginDate());
        $ge(pfx + 'periodEndDate').innerHTML   = LOC_View.getDateFormat(obj.getEndDate());
        $ge(pfx + 'periodDurationStr').innerHTML = obj._durationStr;
        $ge(pfx + 'periodDurationRange').innerHTML = obj._rentTariffDurRangeLabel;

        $ge(pfx + 'beginDate_window').style.display = 'none';
        $ge(pfx + 'endDate_window').style.display   = 'none';
        $ge(pfx + 'endDate_min').style.display      = 'none';

        // Affichage de la date de fin théorique
        var date = obj.getTheoreticalEndDate();
        var el = $ge(pfx + 'theorEndDate');
        el.style.display = (date != '' ? '' : 'none');
        el.innerHTML = LOC_View.getDateFormat(date);

        // Date de début souhaitée
        $ge(pfx + 'periodRequiredBeginDate').value = obj._requiredBeginDate;
        $ge(pfx + 'periodRequiredBeginDate.masked').disabled = (!obj.hasRight('beginDate') ||
                                                                obj.getExternalParamValue('disabledComponents').beginDate);
        setMaskedInputValue(pfx + 'periodRequiredBeginDate', LOC_View.getDateFormat(obj._requiredBeginDate));

        // plage de modification de la date de début
        var beginDateWindow = obj.getExternalParamValue('beginDateWindow');
        if (beginDateWindow[1])
        {
            if (beginDateWindow[0])
            {
                $ge(pfx + 'beginDate_window').style.display = 'block';
                $ge(pfx + 'beginDate_window_1').innerHTML = LOC_View.getDateFormat(beginDateWindow[0]);
                $ge(pfx + 'beginDate_window_2').innerHTML = LOC_View.getDateFormat(beginDateWindow[1]);
            }
            else
            {
                $ge(pfx + 'beginDate_max').style.display = 'block';
                $ge(pfx + 'beginDate_window_0').innerHTML = LOC_View.getDateFormat(beginDateWindow[1]);
            }
        }

        // Date de fin souhaitée
        $ge(pfx + 'periodRequiredEndDate').value = obj._requiredEndDate;
        $ge(pfx + 'periodRequiredEndDate.masked').disabled = (!obj.hasRight('endDate') ||
                                                              obj.getExternalParamValue('disabledComponents').endDate);
        setMaskedInputValue(pfx + 'periodRequiredEndDate', LOC_View.getDateFormat(obj._requiredEndDate));
        el = Location.spinControlsManager.getSpinControl(pfx + 'periodRequiredRealDuration');
        el.setValue(obj._requiredRealDuration);
        el.setDisabled(!obj.hasRight('endDate'));
        $ge(pfx + 'periodRequiredRealDurationBtn').style.display = (obj.hasRight('endDate') ? '' : 'none');
        $ge(pfx + 'periodRequiredDurationStr').innerHTML   = obj._requiredDurationStr;
        $ge(pfx + 'periodRequiredDurationRange').innerHTML = obj._requiredRentTariffDurRangeLabel;

        // plage de modification de la date de fin
        var endDateWindow = obj.getExternalParamValue('endDateWindow');
        if (endDateWindow[0])
        {
            if (endDateWindow[1])
            {
                $ge(pfx + 'endDate_window').style.display = 'block';
                $ge(pfx + 'endDate_window_1').innerHTML = LOC_View.getDateFormat(endDateWindow[0]);
                $ge(pfx + 'endDate_window_2').innerHTML = LOC_View.getDateFormat(endDateWindow[1]);
            }
            else
            {
                $ge(pfx + 'endDate_min').style.display = 'block';
                $ge(pfx + 'endDate_window_0').innerHTML = LOC_View.getDateFormat(endDateWindow[0]);
            }
       }
    },


    /**
     * Affichage du tableau des jours supplémentaires ou déduits
     */
    _displayTabDays: function(pfx, obj, tabDays, type)
    {
        var mySelf = this;
        var typeLabel = (type == DAY_TYPE_ADDITIONAL ? 'additional' : 'deducted');

        var tableEl     = $ge(pfx + typeLabel + 'DaysList');
        var tBodyAddEl  = tableEl.tBodies[0];
        var tBodyMainEl = tableEl.tBodies[1];
        var tBodySuppEl = tableEl.tBodies[2];

        // Suppression des lignes
        while (tBodyAddEl.rows.length > 0)
        {
            tBodyAddEl.deleteRow(0);
        }
        while (tBodyMainEl.rows.length > 0)
        {
            tBodyMainEl.deleteRow(0);
        }
        while (tBodySuppEl.rows.length > 0)
        {
            tBodySuppEl.deleteRow(0);
        }

        var nb = tabDays.length;

        Location.commonObj.toggleEltClass(tableEl, 'noResult', (nb == 0));
        var min = (this._isMoreDisplayed ? nb : Math.min(nb, DAY_MAXDISPLAY));

        for (var i = 0; i < min; i++)
        {
            var rEl = tBodyMainEl.insertRow(-1);
            mySelf._displayDayLine(pfx, obj, rEl, tabDays[i]);
        }

        // - Affichage de documents supplémentaires
        if (nb > min)
        {
            var diff = nb - min;

            Location.commonObj.addEltClass(tableEl, 'moreResult');
            rMoreEl = tBodySuppEl.insertRow(-1);
            var cEl = rMoreEl.insertCell(-1);
            cEl.colSpan = 6;
            var message = diff > 1 ? LOC_View.getTranslation(pfx + 'label_' + typeLabel + 'More_p') : LOC_View.getTranslation(pfx + 'label_' + typeLabel + 'More_s');
            cEl.innerHTML = message.replace(/<%nb>/, diff);

            cEl.onclick = function() {
                mySelf._isMoreDisplayed = true;
                // Affichage des lignes supplémentaires
                Location.commonObj.removeEltClass(tableEl, 'moreResult');

                for (var i = min; i < nb; i++)
                {
                    var rEl = tBodyMainEl.insertRow(-1);
                    mySelf._displayDayLine(pfx, obj, rEl, tabDays[i]);
                }
            };
        }

    },

    _displayDayLine: function(pfx, obj, rowEl, day)
    {
        var mySelf = this;

        var action = obj.getActionById(day.id);

        // Style de la ligne
        if (action)
        {
            Location.commonObj.addEltClass(rowEl, 'updated action-' + action.type);
            if (action.type == 'add' && !day.isInvoiceable)
            {
                Location.commonObj.addEltClass(rowEl, 'action-setNonInvoiceable');
            }
        }
        else
        {
            Location.commonObj.toggleEltClass(rowEl, 'invoiced', day.isInvoiced || day.isDocumentClosed);
            Location.commonObj.toggleEltClass(rowEl, 'toComplete', day.isToComplete);
            Location.commonObj.toggleEltClass(rowEl, 'nonInvoiceable', (!day.isInvoiced && !day.isInvoiceable));
            Location.commonObj.toggleEltClass(rowEl, 'migrated', (day.isMigrated));
        }

        var isUndoable               = obj.isUndoable(day.id);
        var isEditable               = obj.isEditable(day.id);
        var isDeletable              = obj.isDeletable(day.id);
        var isInvoiceablePossible    = obj.isInvoiceablePossible(day.id);
        var isNonInvoiceablePossible = obj.isNonInvoiceablePossible(day.id);

        // Date
        var isGetFromMigrationAndInvoiced = false;
        var cEl = rowEl.insertCell(-1);
        cEl.className = 'date';

        if (day.date)
        {
            cEl.innerHTML = LOC_View.getDateFormat(day.date);
        }
        else
        {
            if (day.isToComplete)
            {
                cEl.innerHTML = LOC_View.getTranslation(pfx + 'label_to_complete');
            }
            else if (day.isMigrated)
            {
                isGetFromMigrationAndInvoiced = true;
                cEl.colSpan = 3;
                cEl.innerHTML = LOC_View.getTranslation(pfx + 'label_get_from_migration');

                if (day.invoiceDuration.duration != 1)
                {
                    var el = window.document.createElement('SPAN');
                    var className = (day.invoiceDuration.duration == 0.5 ? 'half' : (day.invoiceDuration.duration == 2 ? 'double' : ''));
                    Location.commonObj.addEltClass(el, className);
                    cEl.appendChild(el);
                }
            }
            else
            {
                cEl.innerHTML = LOC_View.getTranslation(pfx + 'label_unknown_date');
            }
        }

        if (!isGetFromMigrationAndInvoiced)
        {
            // Durée spéciale
            cEl = rowEl.insertCell(-1);
            cEl.className = 'invoiceDuration';

            if (day.invoiceDuration.duration != 1)
            {
                var el = window.document.createElement('SPAN');
                var className = (day.invoiceDuration.duration == 0.5 ? 'half' : (day.invoiceDuration.duration == 2 ? 'double' : ''));
                Location.commonObj.addEltClass(el, className);
                cEl.appendChild(el);
            }

            // Motif
            cEl = rowEl.insertCell(-1);
            cEl.className = 'reason';
            if (day.reason)
            {
                cEl.innerHTML = day.reason.label;
            }
        }

        // Contrat lié
        cEl = rowEl.insertCell(-1);
        cEl.className = 'appliedToContract';
        if (day.appliedToContract)
        {
            var el = window.document.createElement('A');
            el.href = day.appliedToContract.url;
            el.target = '_blank';
            el.title = day.appliedToContract.code;
            cEl.appendChild(el);
        }

        // Commentaire
        cEl = rowEl.insertCell(-1);
        cEl.className = 'comment';
        if (day.comment)
        {
            var el = window.document.createElement('DIV');
            var comment = day.comment.replace(/\n/g, '<br />');
            el.onmouseover = function() {
                Location.toolTipsObj.show(comment, {corner:'tl'});
            };
            el.onmouseout = function() {
                Location.toolTipsObj.hide();
            };
            cEl.appendChild(el);
        }

        // Actions
        cEl = rowEl.insertCell(-1);
        cEl.className = 'actions';
        if (day.isInvoiced)
        {
            cEl.innerHTML = LOC_View.getTranslation(pfx + 'label_invoiced');
        }
        else if (day.isDocumentClosed)
        {
            cEl.innerHTML = LOC_View.getTranslation(pfx + 'label_non_invoiced');
        }

        // - Passage en facturé
        if (isInvoiceablePossible)
        {
            var el = window.document.createElement('A');
            el.href = '#';
            el.className = 'invoice';
            el.title = LOC_View.getTranslation(pfx + 'button_invoice');
            el.onclick = function(e) {
                obj.setInvoiceableDay(day.id);
                return false;
            };
            cEl.appendChild(el);
        }
        // - Passage en non facturé
        if (isNonInvoiceablePossible)
        {
            var el = window.document.createElement('A');
            el.href = '#';
            el.className = 'uninvoice';
            el.title = LOC_View.getTranslation(pfx + 'button_uninvoice');
            el.onclick = function(e) {
                obj.setNonInvoiceableDay(day.id);
                return false;
            };
            cEl.appendChild(el);
        }
        // - Modification
        if (isEditable)
        {
            var el = window.document.createElement('A');
            el.href = '#';
            el.className = 'edit';
            el.title = LOC_View.getTranslation(pfx + 'button_edit');
            el.onclick = function(e) {
                mySelf._showEditDayPopup(pfx, obj, day);
                return false;
            };
            cEl.appendChild(el);
        }
        // - Annulation
        if (isUndoable)
        {
            var el = window.document.createElement('A');
            el.href = '#';
            el.className = 'undo';
            el.title = LOC_View.getTranslation(pfx + (action.type != 'add' ? 'button_undo' : 'button_delete'));
            el.onclick = function() {
                obj.undoDay(day.id);
                return false;
            };
            cEl.appendChild(el);
        }
        // - Suppression
        if (isDeletable)
        {
            var el = window.document.createElement('A');
            el.href = '#';
            el.className = 'delete';
            el.title = LOC_View.getTranslation(pfx + 'button_delete');
            el.onclick = function(e) {
                obj.removeDay(day.id);
                return false;
            };
            cEl.appendChild(el);
        }

    },


    /**
     * Ouvrir la popup d'ajout d'un jour supplémentaire ou déduit
     */
    _showAddDayPopup: function(pfx, obj, type, isAppliedToContract)
    {
        var mySelf = this;
        var popupEl = $ge(pfx + 'addPopup');
        var documentObj = obj.getDocumentObject();

        popupEl.className = pfx + 'popup type-' + type;

        Location.commonObj.removeEltClass(popupEl, 'loading');
        Location.commonObj.toggleEltClass(popupEl, 'opt-appliedToContract', isAppliedToContract);

        var dateEl              = $ge(pfx + 'addDate');
        var dateMaskEl          = $ge(pfx + 'addDate.masked');
        var invoiceDurationEl   = $ge(pfx + 'addInvoiceDurationId');
        var reasonEl            = $ge(pfx + 'addReasonId');
        var appliedToContractEl = Location.searchBoxesManager.getSearchBox(pfx + 'addAppliedToContractId');
        var commentEl           = $ge(pfx + 'addComment');
        var isNonInvoiceableEl  = $ge(pfx + 'addIsNonInvoiceable');
        var btnEl               = $ge(pfx + 'addBtn');

        // Affichage de la coche "Ne pas facturer"
        Location.commonObj.toggleEltClass($ge(pfx + 'addIsNonInvoiceableLine'), 'hidden', type == DAY_TYPE_DEDUCTED ||
                                                                                          documentObj.getObjectType() != 'contract');

        // Affichage du contrat lié ?
        Location.commonObj.toggleEltClass(popupEl.querySelector('tr.appliedToContract'), 'active', isAppliedToContract);
        Location.commonObj.removeEltClass($ge(pfx + 'addPeriodInfos'), 'active');

        // Désactivation de champs si contrat lié
        dateMaskEl.disabled        = isAppliedToContract;
        invoiceDurationEl.disabled = isAppliedToContract;
        reasonEl.disabled          = isAppliedToContract;

        dateEl.value = '';
        clearMaskedInputValue(pfx + 'addDate');
        invoiceDurationEl.value = 0;
        reasonEl.value = 0;
        appliedToContractEl.clear();
        commentEl.value = '';
        isNonInvoiceableEl.checked = false;

        var initErrors = function()
        {
            appliedToContractEl.setRequired(false);
            Location.commonObj.removeEltClass(dateMaskEl, 'error');
            Location.commonObj.removeEltClass(reasonEl, 'error');

            var tabErrorsEl = document.querySelectorAll('.' + pfx + 'popupErrors > span');
            for (var i = 0; i < tabErrorsEl.length; i++)
            {
                Location.commonObj.removeEltClass(tabErrorsEl[i], 'active');
            }
        };
        initErrors();

        // Liste des types de facturation (demi-journée, journée, double)
        while (invoiceDurationEl.options[0])
        {
            invoiceDurationEl.removeChild(invoiceDurationEl.options[0]);
        }
        // Liste des durées dispo pour ce type de jour
        obj.getDayInvoiceDurationsList(type).forEach(function(item, index) {
            var option = new Option(item.label, item.id);
            invoiceDurationEl.add(option);
            if (item.isDefault)
            {
                invoiceDurationEl.selectedIndex = index;
            }
        });

        // Liste des motifs
        while (reasonEl.options[0])
        {
            reasonEl.removeChild(reasonEl.options[0]);
        }
        // - Choix par défaut
        var defaultOption = new Option(LOC_View.getTranslation(pfx + 'select_defaultChoice'), 0);
        reasonEl.add(defaultOption);
        // Liste des motifs dispo pour ce type de jour
        obj.getDayReasonsList(type, isAppliedToContract).forEach(function(item) {
            var option = new Option(item.label, item.id);
            reasonEl.add(option);
        });

        // - Affichage de la période possible (période du document par défaut) si ce n'est pas un ajout de contrat lié
        if (!isAppliedToContract)
        {
            Location.commonObj.addEltClass($ge(pfx + 'addPeriodInfos'), 'active');
            var authorizedPeriod = (type == DAY_TYPE_ADDITIONAL ? obj.getAuthorizedPeriodAddDays() : obj.getAuthorizedPeriodDedDays());
            $ge(pfx + 'addPeriodInfos_beginDate').innerHTML = LOC_View.getDateFormat(authorizedPeriod[0]);
            $ge(pfx + 'addPeriodInfos_endDate').innerHTML   = LOC_View.getDateFormat(authorizedPeriod[1]);

            var tabDeltas = (type == DAY_TYPE_ADDITIONAL ? obj.getDeltasPeriodAddDays() : obj.getDeltasPeriodDedDays());
            var tabTitles = mySelf._getPeriodTitle(pfx, tabDeltas, false);
            $ge(pfx + 'addPeriodInfos').title = tabTitles.join("\n");
        }

        // Action sur le bouton "vider" du contrat lié
        $ge(pfx + 'addAppliedToContractIdClear').onclick = function()
        {
            Location.commonObj.removeEltClass($ge(pfx + 'addPeriodInfos'), 'active');
            dateMaskEl.disabled        = true;
            invoiceDurationEl.disabled = true;
            reasonEl.disabled          = true;

            appliedToContractEl.clear();
            return true;
        }
        // - Action au lancement de la recherche
        appliedToContractEl.onsearchstart = function()
        {
            Location.commonObj.addEltClass(btnEl, 'disabled');
        }
        // - Action à la fin de la recherche
        appliedToContractEl.onsearchend = function()
        {
            Location.commonObj.removeEltClass(btnEl, 'disabled');
        }
        // - Action au changement du contrat lié
        appliedToContractEl.onchange = function()
        {
            // -- Modification d'affichage
            Location.commonObj.addEltClass($ge(pfx + 'addPeriodInfos'), 'active');
            dateMaskEl.disabled        = false;
            invoiceDurationEl.disabled = false;
            reasonEl.disabled          = false;

            var opt = this.getSelectedOption();
            var appliedToContract = JSON.parse(opt.dataset.data);
            var authorizedPeriod = (type == DAY_TYPE_ADDITIONAL ? appliedToContract.authorizedPeriodAddDays : appliedToContract.authorizedPeriodDedDays);
            $ge(pfx + 'addPeriodInfos_beginDate').innerHTML = LOC_View.getDateFormat(authorizedPeriod[0]);
            $ge(pfx + 'addPeriodInfos_endDate').innerHTML   = LOC_View.getDateFormat(authorizedPeriod[1]);

            var tabDeltas = (type == DAY_TYPE_ADDITIONAL ? appliedToContract.tabDeltasPeriodAddDays : appliedToContract.tabDeltasPeriodDedDays);
            var tabTitles = mySelf._getPeriodTitle(pfx, tabDeltas, true);
            $ge(pfx + 'addPeriodInfos').title = tabTitles.join("\n");
        }
        // - Action sur le bouton "Historique client"
        documentObj = (documentObj.getObjectType() == 'estimateLine' ? documentObj.getEstimateObject() : documentObj);
        $ge(pfx + 'addCustomerHistoryBtn').href     = documentObj.getCustomerObject().getHistoryUrl();

        // Action sur le bouton ajouter
        btnEl.onclick = function() {

            Location.commonObj.addEltClass(popupEl, 'loading');
            initErrors();

            var isInvoiceable = !isNonInvoiceableEl.checked;
            // - Prise en compte du nouveau jour
            obj.addDay(type, isAppliedToContract, appliedToContractEl.getValue(), dateEl.value, invoiceDurationEl.value, reasonEl.value, commentEl.value, isInvoiceable).then(function() {
                Location.modalWindowManager.hide(pfx + 'addPopup');
            }, function(tabErrors) {

                for (var i = 0; i < tabErrors.length; i++)
                {
                    if (in_array(tabErrors[i], ['noAppliedToContract']))
                    {
                        appliedToContractEl.setRequired(true);
                    }
                    else if (in_array(tabErrors[i], ['noDate', 'dateNotInPeriod', 'existingDay']))
                    {
                        dateMaskEl.className = 'error';
                    }
                    else if (in_array(tabErrors[i], ['noReason']))
                    {
                        Location.commonObj.addEltClass(reasonEl, 'error');
                    }
                    Location.commonObj.addEltClass(document.querySelector('#' + pfx + 'addPopup .' + pfx + 'error-' + tabErrors[i]), 'active');
                }

                Location.commonObj.removeEltClass(popupEl, 'loading');
                return false;
            });
        };

        Location.modalWindowManager.show(pfx + 'addPopup', {contentType: 3, width: 450, height: 'auto'});
    },

    /**
     * Ouvrir la popup de modification d'un jour supplémentaire ou déduit
     */
    _showEditDayPopup: function(pfx, obj, day)
    {
        var mySelf = this;
        var popupEl = $ge(pfx + 'editPopup');
        var documentObj = obj.getDocumentObject();

        var type = day.type;
        var isAppliedToContract = (day.appliedToContract == undefined ? false : true);

        popupEl.className = pfx + 'popup type-' + type;
        Location.commonObj.removeEltClass(popupEl, 'loading');
        Location.commonObj.toggleEltClass(popupEl, 'opt-appliedToContract', isAppliedToContract);

        var dateEl              = $ge(pfx + 'editDate');
        var dateMaskEl          = $ge(pfx + 'editDate.masked');
        var invoiceDurationEl   = $ge(pfx + 'editInvoiceDurationId');
        var reasonEl            = $ge(pfx + 'editReasonId');
        var appliedToContractEl = Location.searchBoxesManager.getSearchBox(pfx + 'editAppliedToContractId');
        var commentEl           = $ge(pfx + 'editComment');
        var isNonInvoiceableEl  = $ge(pfx + 'editIsNonInvoiceable');
        var btnEl               = $ge(pfx + 'editBtn');

        // Affichage de la coche "Ne pas facturer"
        Location.commonObj.toggleEltClass($ge(pfx + 'editIsNonInvoiceableLine'), 'hidden', Number.isInteger(day.id) || type == DAY_TYPE_DEDUCTED);

        // Affichage du contrat lié ?
        Location.commonObj.toggleEltClass(popupEl.querySelector('tr.appliedToContract'), 'active', isAppliedToContract);

        var initErrors = function()
        {
            appliedToContractEl.setRequired(false);
            Location.commonObj.removeEltClass($ge(pfx + 'editDate.masked'), 'error');
            Location.commonObj.removeEltClass(reasonEl, 'error');

            var tabErrorsEl = document.querySelectorAll('.' + pfx + 'popupErrors > span');
            for (var i = 0; i < tabErrorsEl.length; i++)
            {
                Location.commonObj.removeEltClass(tabErrorsEl[i], 'active');
            }
        };
        initErrors();

        // Liste des types de facturation (demi-journée, journée, double)
        while (invoiceDurationEl.options[0])
        {
            invoiceDurationEl.removeChild(invoiceDurationEl.options[0]);
        }
        // Liste des durées dispo pour ce type de jour
        obj.getDayInvoiceDurationsList(type).forEach(function(item, index) {
            var option = new Option(item.label, item.id);
            invoiceDurationEl.add(option);
            if (item.isDefault)
            {
                invoiceDurationEl.selectedIndex = index;
            }
        });

        // Liste des motifs dispo pour ce type de jour
        while (reasonEl.options[0])
        {
            reasonEl.removeChild(reasonEl.options[0]);
        }
        // - Choix par défaut
        var defaultOption = new Option(LOC_View.getTranslation(pfx + 'select_defaultChoice'), 0);
        reasonEl.add(defaultOption);
        obj.getDayReasonsList(type, isAppliedToContract).forEach(function(item) {
            var option = new Option(item.label, item.id);
            reasonEl.add(option);
        });

        // - Affichage de la période possible
        Location.commonObj.addEltClass($ge(pfx + 'editPeriodInfos'), 'active');
        var authorizedPeriod = (type == DAY_TYPE_ADDITIONAL ? obj.getAuthorizedPeriodAddDays() : obj.getAuthorizedPeriodDedDays());
        $ge(pfx + 'editPeriodInfos_beginDate').innerHTML = LOC_View.getDateFormat(authorizedPeriod[0]);
        $ge(pfx + 'editPeriodInfos_endDate').innerHTML   = LOC_View.getDateFormat(authorizedPeriod[1]);

        var tabDeltas = (type == DAY_TYPE_ADDITIONAL ? obj.getDeltasPeriodAddDays() : obj.getDeltasPeriodDedDays());
        var tabTitles = mySelf._getPeriodTitle(pfx, tabDeltas, isAppliedToContract);
        $ge(pfx + 'editPeriodInfos').title = tabTitles.join("\n");

        appliedToContractEl.clear();
        // - Action au lancement de la recherche
        appliedToContractEl.onsearchstart = function()
        {
            Location.commonObj.addEltClass(btnEl, 'disabled');
        }
        // - Action à la fin de la recherche
        appliedToContractEl.onsearchend = function()
        {
            Location.commonObj.removeEltClass(btnEl, 'disabled');
        }
        // - Action au changement du contrat lié
        appliedToContractEl.onchange = function()
        {
            // -- Modification d'affichage
            dateMaskEl.disabled        = false;
            invoiceDurationEl.disabled = false;
            reasonEl.disabled          = false;

            var opt = this.getSelectedOption();
            var appliedToContract = JSON.parse(opt.dataset.data);
            var authorizedPeriod = (type == DAY_TYPE_ADDITIONAL ? appliedToContract.authorizedPeriodAddDays : appliedToContract.authorizedPeriodDedDays);
            $ge(pfx + 'editPeriodInfos_beginDate').innerHTML = LOC_View.getDateFormat(authorizedPeriod[0]);
            $ge(pfx + 'editPeriodInfos_endDate').innerHTML   = LOC_View.getDateFormat(authorizedPeriod[1]);

            var tabDeltas = (type == DAY_TYPE_ADDITIONAL ? appliedToContract.tabDeltasPeriodAddDays : appliedToContract.tabDeltasPeriodDedDays);
            var tabTitles = mySelf._getPeriodTitle(pfx, tabDeltas, true);
            $ge(pfx + 'editPeriodInfos').title = tabTitles.join("\n");
        }
        if (isAppliedToContract)
        {
            appliedToContractEl.setElements([[day.appliedToContract.id, day.appliedToContract.code]], 1);
            appliedToContractEl.setValue(day.appliedToContract.id, false);
            appliedToContractEl.getSelectedOption().dataset.data = JSON.stringify(day.appliedToContract);
            appliedToContractEl.onchange();
        }
        // - Action sur le bouton "Historique client"
        documentObj = (documentObj.getObjectType() == 'estimateLine' ? documentObj.getEstimateObject() : documentObj);
        $ge(pfx + 'editCustomerHistoryBtn').href     = documentObj.getCustomerObject().getHistoryUrl();

        dateEl.value            = day.date;
        setMaskedInputValue(pfx + 'editDate', LOC_View.getDateFormat(day.date));
        invoiceDurationEl.value = day.invoiceDuration.id;
        reasonEl.value          = (day.reason ? day.reason.id : 0);
        commentEl.value         = (day.comment || '');
        isNonInvoiceableEl.checked = !day.isInvoiceable;

        // Action sur le bouton "vider" du contrat lié
        $ge(pfx + 'editAppliedToContractIdClear').onclick = function()
        {
            Location.commonObj.removeEltClass($ge(pfx + 'editPeriodInfos'), 'active');
            dateMaskEl.disabled        = true;
            invoiceDurationEl.disabled = true;
            reasonEl.disabled          = true;

            appliedToContractEl.clear();
            return true;
        }

        // Action sur le bouton Modifier
        btnEl.onclick = function() {

            Location.commonObj.addEltClass(popupEl, 'loading');
            initErrors();

            var isInvoiceable = !isNonInvoiceableEl.checked;
            // - Prise en compte de la modif
            obj.editDay(day.id, isAppliedToContract, appliedToContractEl.getValue(), dateEl.value, invoiceDurationEl.value, reasonEl.value, commentEl.value, isInvoiceable).then(function() {
                Location.modalWindowManager.hide(pfx + 'editPopup');
            }, function(tabErrors) {

                for (var i = 0; i < tabErrors.length; i++)
                {
                    if (in_array(tabErrors[i], ['noAppliedToContract']))
                    {
                        appliedToContractEl.setRequired(true);
                    }
                    else if (in_array(tabErrors[i], ['noDate', 'dateNotInPeriod', 'existingDay']))
                    {
                        $ge(pfx + 'editDate.masked').className = 'error';
                    }
                    else if (in_array(tabErrors[i], ['noReason']))
                    {
                        Location.commonObj.addEltClass(reasonEl, 'error');
                    }
                    Location.commonObj.addEltClass(document.querySelector('#' + pfx + 'editPopup .' + pfx + 'error-' + tabErrors[i]), 'active');
                }

                Location.commonObj.removeEltClass(popupEl, 'loading');
                return false;
            });
        };

        Location.modalWindowManager.show(pfx + 'editPopup', {contentType: 3, width: 450, height: 'auto'});
    },

    /**
     * Récupère les titles à afficher sur la période possible sur les J+/-
     */
    _getPeriodTitle: function(pfx, tabDeltas, isAppliedToContract)
    {
        var tabTitles = [];
        if (tabDeltas[0] != 0)
        {
            var title = LOC_View.getTranslation(pfx + 'title_authorizedPeriod_beginDate_' + (Math.abs(tabDeltas[0]) > 1 ? 'p' : 's'));

            title = title.replace(/<%appliedToContract>/, (isAppliedToContract ? LOC_View.getTranslation(pfx + 'title_authorizedPeriod_appliedToContract') : ''));
            title = title.replace(/<%deltaBeginDate>/, (tabDeltas[0] > 0 ? '+' + tabDeltas[0] : tabDeltas[0]));
            tabTitles.push(title);
        }
        if (tabDeltas[1] != 0)
        {
            var title = LOC_View.getTranslation(pfx + 'title_authorizedPeriod_endDate_' + (Math.abs(tabDeltas[1]) > 1 ? 'p' : 's'));
            title = title.replace(/<%appliedToContract>/, (isAppliedToContract ? LOC_View.getTranslation(pfx + 'title_authorizedPeriod_appliedToContract') : ''));
            title = title.replace(/<%deltaEndDate>/, (tabDeltas[1] > 0 ? '+' + tabDeltas[1] : tabDeltas[1]));
            tabTitles.push(title);
        }

        return tabTitles;
    },
};


