if (!window.Location)
{
    var Location = new Object();
}


/**
 * Classe de gestion de la remise en état
 *
 * @param Object documentObj Objet javascript du document (ligne de devis ou contrat)
 * @param Object tabCfgs     Configurations globales
 */
Location.Rent_Helper_Repair = function(documentObj, tabCfgs)
{
    this._documentObj = documentObj;
    this._tabCfgs = tabCfgs;

    this._tabRepairDays     = [];
    this._tabRepairServices = [];
    this._tabRepairSheets   = [];

    this._tabExtParams = {
        isProformaRequired: false,
        isProformaActived: false,
        disabledComponents: {}
    };

    this._tabRights = null;

    this.onchange = null;
    this.onload   = null;
}


Location.Rent_Helper_Repair.prototype = {

    /**
     * Chargement de données
     *
     */
    load : function(tab, tabExtParams, tabRights)
    {
        // Services de remise en état
        // TODO: Optimiser cette récupération
        this._tabRepairServices = [];
        for (var i in tab.services)
        {
            var nb = tab.services[i].length;
            for (var j = 0; j < nb; j++)
            {
                this._tabRepairServices.push({
                    'id':       tab.services[i][j]['id'],
                    'amount':   tab.services[i][j]['amount'],
                    'state.id': tab.services[i][j]['state.id']
                });
            }
        }

        // Jours de remise en état
        this._tabRepairDays = [];
        var index = 0;
        for (var i in tab.days)
        {
            var nbDays = tab.days[i].length;
            for (var j = 0; j < nbDays; j++)
            {
                this._tabRepairDays[index] = {'id':       tab.days[i][j]['id'],
                                              'amount':   tab.days[i][j]['amount'],
                                              'state.id': tab.days[i][j]['state.id']};
                index++;
            }
        }

        // Fiches de remise en état
        this._tabRepairSheets = tab.sheets;

        for (var name in tabExtParams)
        {
            if (this._tabExtParams.hasOwnProperty(name))
            {
                this._tabExtParams[name] = tabExtParams[name];
            }
        }

        this._tabRights = tabRights;

        if (this.onload)
        {
            this.onload();
        }
    },

    /**
     * Récupérer les jours de remise en état
     */
    hasInvoicedRepairDays: function()
    {
        var i = 0, nb = this._tabRepairDays.length;
        while (i < nb && this._tabRepairDays[i]['state.id'] == REPAIR_STATE_CANCELED)
        {
            i++;
        }
        return (i < nb);
    },

    /**
     * Récupérer les services de remise en état
     *
     * @return Array
     */
    hasInvoicedRepairServices: function()
    {
        var i = 0, nb = this._tabRepairServices.length;
        while (i < nb && this._tabRepairServices[i]['state.id'] == REPAIR_STATE_CANCELED)
        {
            i++;
        }
        return (i < nb);
    },

    /**
     * Récupérer les fiches de remise en état actives
     */
    getActiveRepairSheetsList: function()
    {
        return this._tabRepairSheets.filter(function(item) {
            return item['state.id'] == REPAIRSHEET_STATE_WAITING;
        })
    },

    /**
     * Récupérer le nombre de devis de remise en état en attente
     */
    getPendingRepairEstimatesCount: function()
    {
        var tab = this._tabRepairSheets.filter(function(item) {
            return (item.repairEstimate && item.repairEstimate['state.id'] == REPAIRESTIMATE_STATE_WAITING);
        });
        return tab.length;
    },

    /**
     * Récupérer les jours de remise en état
     *
     * @return Array
     */
    getRepairDays : function()
    {
        return this._tabRepairDays;
    },

    /**
     * Récupérer le montant des jours de remise en état
     *
     * @return array
     */
    getRepairDaysAmount : function()
    {
        var amount = 0;
        var nbDays = this._tabRepairDays.length;
        for (var i = 0; i < nbDays; i++)
        {
            if (this._tabRepairDays[i]['state.id'] != REPAIR_STATE_CANCELED)
            {
                amount += this._tabRepairDays[i]['amount'] * 1;
            }
        }
        return amount;
    },

    /**
     * Récupérer les informations d'un service de remise en état
     *
     * @return Object|null
     */
    getRepairService : function(id)
    {
        var i = 0, nb = this._tabRepairServices.length;
        while (i < nb && this._tabRepairServices[i]['id'] != id)
        {
            i++;
        }
        return (i < nb ? this._tabRepairServices[i] : null);
    },

    /**
     * Récupérer les services de remise en état
     *
     * @return Array
     */
    getRepairServices : function()
    {
        return this._tabRepairServices;
    },

    /**
     * Récupérer le montant des services de remise en état
     *
     * @return array
     */
    getRepairServicesAmount : function()
    {
        var amount = 0;
        var nb = this._tabRepairServices.length;
        for (var i = 0; i < nb; i++)
        {
            if (this._tabRepairServices[i]['state.id'] != REPAIR_STATE_CANCELED)
            {
                amount += this._tabRepairServices[i]['amount'] * 1;
            }
        }
        return amount;
    },


    /**
     * Mettre des jours de remise en état en facturation ou pas
     *
     * @param int repairDayId Identifiant des jours de remise en état
     * @param bool isInvoiced
     * @return bool
     */
    invoiceRepairDay : function(repairDayId, isInvoiced)
    {
        if (!this.isInvoiceRepairDaysPossible())
        {
            return false;
        }

        var stateId = (isInvoiced ? REPAIR_STATE_APPROVED : REPAIR_STATE_CANCELED);
        var i = 0, nb = this._tabRepairDays.length;
        while (i < nb && (this._tabRepairDays[i]['id'] != repairDayId ||
                          this._tabRepairDays[i]['state.id'] == REPAIR_STATE_INVOICED ||
                          this._tabRepairDays[i]['state.id'] == stateId))
        {
            i++;
        }
        if (i == nb)
        {
            return false;
        }
        this._tabRepairDays[i]['state.id'] = stateId;

        if (this.onchange)
        {
            this.onchange(['invoiceRepairDay']);
        }
        return true;
    },

    /**
     * Est-il possible de facturer des jours de remise en état ?
     *
     * @return bool
     */
    isInvoiceRepairDaysPossible : function()
    {
        return (this.hasRight('invoiceDays') &&
                !this.getExternalParamValue('isProformaRequired') &&
                !this.getExternalParamValue('isProformaActived'));
    },

    /**
     * Facturer ou ne pas facturer un service de remise en état
     *
     * @param int id
     * @param bool isInvoiced
     * @return bool
     */
    invoiceRepairService : function(id, isInvoiced)
    {
        if (!this.isInvoiceRepairServicesPossible())
        {
            return false;
        }
        var obj = this.getRepairService(id);
        var stateId = (isInvoiced ? REPAIR_STATE_APPROVED : REPAIR_STATE_CANCELED);
        if (!obj || obj['state.id'] == REPAIR_STATE_INVOICED || obj['state.id'] == stateId)
        {
            return false;
        }
        obj['state.id'] = stateId;

        if (this.onchange)
        {
            this.onchange(['invoiceRepairService']);
        }
        return true;
    },


    /**
     * Est-il possible de facturer des services de remise en état ?
     *
     * @return bool
     */
    isInvoiceRepairServicesPossible : function()
    {
        return (this.hasRight('invoiceServices') &&
                !this.getExternalParamValue('isProformaRequired') &&
                !this.getExternalParamValue('isProformaActived'));
    },

    /**
     * Récupérer un paramètre externe
     *
     * @param string name
     * @return mixed
     */
    getExternalParamValue : function(name)
    {
        return this._tabExtParams[name];
    },

    /**
     * Définir un ou plusieurs paramètres externes
     *
     * @param Object tab Paramètres à définir
     * @return bool
     */
    setExternalParams : function(tab)
    {
        var tabChanges = [];
        for (var name in tab)
        {
            if (this._tabExtParams.hasOwnProperty(name))
            {
                if (!LOC_Common.isEqual(tab[name], this._tabExtParams[name]))
                {
                    this._tabExtParams[name] = tab[name];
                    tabChanges.push('extParam_' + name);
                }
            }
        }

        if (tabChanges.length > 0 && this.onchange)
        {
            this.onchange(tabChanges);
        }
        return (tabChanges.length > 0);
    },

    /**
     * Vérifier le droit à un champ
     *
     * @return bool
     */
    hasRight : function(fieldName)
    {
        return (this._tabRights === null || this._tabRights[fieldName] ? true : false);
    }
}

Location.Rent_Helper_RepairView = {


    display : function(pfx, obj, tabErrors)
    {
        var mySelf = this;
        var disabledComponents  = obj.getExternalParamValue('disabledComponents');
        var scrollTop = $ge(pfx + 'container').scrollTop;


        $ge(pfx + 'container').scrollTop = scrollTop;

        // Jours de remise en état
        el = $ge(pfx + 'repairDays');
        el.className = 'standard' + (obj.isInvoiceRepairDaysPossible() ? ' update' : '');

        var tab = obj.getRepairDays();
        var nbDays = tab.length;
        for (var i = 0; i < nbDays; i++)
        {
            var id = tab[i]['id'];
            el = $ge(pfx + 'repairDay[' + tab[i]['id'] + ']');
            el.className = (tab[i]['state.id'] == REPAIR_STATE_CANCELED ? 'canceled' : (tab[i]['state.id'] == REPAIR_STATE_APPROVED ? 'approved' : ''));

            this._setDayEventButtons(id, pfx, obj);
        }

        // Services de remise en état
        el = $ge(pfx + 'repairSrvs');
        el.className = 'standard' + (obj.isInvoiceRepairServicesPossible() ? ' update' : '');

        var tab = obj.getRepairServices();
        var nb = tab.length;
        for (var i = 0; i < nb; i++)
        {
            var id = tab[i]['id'];

            el = $ge(pfx + 'repairSrv[' + tab[i]['id'] + ']');
            el.className = (tab[i]['state.id'] == REPAIR_STATE_CANCELED ? 'canceled' : (tab[i]['state.id'] == REPAIR_STATE_APPROVED ? 'approved' : ''));

            this._setServiceEventButtons(id, pfx, obj);
        }
    },

    /**
     * Ajouter les évènements sur les boutons des services de remise en état
     */
    _setServiceEventButtons: function(id, pfx, obj)
    {
        // Facturer
        var invoiceServiceBtnEl = $ge(pfx + 'invoiceServiceBtn[' + id + ']');
        invoiceServiceBtnEl.onclick = function() {
            obj.invoiceRepairService(id, true);
            return false;
        };

        // Ne pas facturer
        var uninvoiceServiceBtnEl = $ge(pfx + 'uninvoiceServiceBtn[' + id + ']');
        uninvoiceServiceBtnEl.onclick = function() {
            obj.invoiceRepairService(id, false);
            return false;
        };
    },

    /**
     * Ajouter les évènements sur les boutons des jours de remise en état
     */
    _setDayEventButtons: function(id, pfx, obj)
    {
        // Facturer
        var invoiceDayBtnEl = $ge(pfx + 'invoiceDayBtn[' + id + ']');
        invoiceDayBtnEl.onclick = function() {
            obj.invoiceRepairDay(id, true);
            return false;
        };

        // Ne pas facturer
        var uninvoiceDayBtnEl = $ge(pfx + 'uninvoiceDayBtn[' + id + ']');
        uninvoiceDayBtnEl.onclick = function() {
            obj.invoiceRepairDay(id, false);
            return false;
        };
    },

    initEvents : function(pfx, obj)
    {
    }

}