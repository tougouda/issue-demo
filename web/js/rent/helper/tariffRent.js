if (!window.Location)
{
    var Location = new Object();
}


/**
 * Classe de gestion de la tarification location
 *
 * @param Object documentObj Objet javascript du document (ligne de devis ou contrat)
 * @param Object tabCfgs     Configurations globales
 */
Location.Rent_Helper_RentTariff = function(documentObj, tabCfgs)
{
    this._documentObj = documentObj;
    this._tabCfgs = tabCfgs;

    this._tabTariffs = [];

    this._days = 0;
    this._amountType = 'day';

    this._oldBaseAmount = 0;
    this._oldAmount     = 0;
    this._oldSpecialReductionBillLabel     = '';
    this._oldSpecialReductionJustification = '';

    this._isTariffChange = false;
    this._hasSpecialReduction = false;
    this._hasSpecialReductionJustificationChanged = false;
    this._hasSpecialReductionBillLabelChanged = false;
    this._hasBaseAmountChangedFromDuration = false;

    this._baseAmount = 0;
    this._maxiReductionAmount  = 0;
    this._maxiReductionPercent = 0;
    this._recoReductionAmount  = 0;
    this._recoReductionPercent = 0;
    this._reductionAmount  = 0;
    this._reductionPercent = 0;
    this._standardReductionAmount  = 0;
    this._standardReductionPercent = 0;
    this._specialReductionAmount  = 0;
    this._specialReductionBillLabel = '';
    this._specialReductionJustification = '';
    this._specialReductionPercent = 0;
    this._specialReductionStatus = '';
    this._amount = 0;

    this._isKeepAmount = false;

    this._tabExtParams = {
        isProformaRequired: false,
        isProformaActived: false,
        disabledComponents: {}
    };

    this._tabRights = null;

    this.onchange = null;
    this.onload   = null;
}


Location.Rent_Helper_RentTariff.prototype = {

    /**
     * Copier les informations d'une ligne vers la courante
     *
     * @param Location.RentEstimateLine lineObj
     * @return bool
     */
    copy : function(sourceObj)
    {
        var tabProps = ['tabExtParams', 'tabTariffs', 'isTariffChange', 'hasSpecialReduction',
                        'days', 'amountType', 'oldBaseAmount', 'baseAmount',
                        'maxiReductionAmount', 'maxiReductionPercent',
                        'recoReductionAmount', 'recoReductionPercent',
                        'reductionAmount', 'reductionPercent',
                        'standardReductionAmount', 'standardReductionPercent',
                        'specialReductionAmount', 'specialReductionPercent',
                        'specialReductionBillLabel', 'specialReductionJustification',
                        'oldSpecialReductionBillLabel', 'oldSpecialReductionJustification',
                        'oldAmount', 'amount',
                        'case', 'mode'];

        var i, prop;
        var nb = tabProps.length;
        for (i = 0; i < nb; i++)
        {
            prop = '_' + tabProps[i];
            this[prop] = LOC_Common.clone(sourceObj[prop]);
        }
        this._hasSpecialReductionJustificationChanged = false;
        this._hasSpecialReductionBillLabelChanged = false;
        this._specialReductionStatus = '';
        this._isKeepAmount = false;
        this._calculateTariff(false);

        return true;
    },

    /**
     * Chargement de données
     *
     * @param string tab          Données
     * @param float  days         Jours
     * @param Array  tabTariff    Liste des tarifs par tranches de durée
     * @param Object tabExtParams Paramètres externes
     * @param Object tabRights    Droits
     */
    load : function(tab, days, tabTariffs, tabExtParams, tabRights)
    {
        this._days           = Number(days);
        this._tabTariffs     = tabTariffs;
        this._isTariffChange = false;
        this._amountType     = tab.amountType;
        this._baseAmount     = LOC_Common.round(Number(tab.baseAmount), 0);
        this._maxiReductionPercent = tab.maxiReductionPercent;
        this._isKeepAmount   = (tab.isKeepAmount ? true : false);

        // Récupération de la remise recommandée (non stockée)
        var tabInfos = this._getTariffInfos();
        if (tabInfos.recoReductionPercent > this._maxiReductionPercent)
        {
            this._recoReductionPercent = this._maxiReductionPercent;
        }
        else
        {
            this._recoReductionPercent = tabInfos.recoReductionPercent;
        }

        this._standardReductionPercent = this._toPercent(tab.standardReductionPercent, true);
        this._standardReductionAmount  = LOC_Common.round(this._standardReductionPercent * this._baseAmount / 100, 0);
        var refAmount                  = this._baseAmount - this._standardReductionAmount;
        this._specialReductionPercent  = this._toPercent(tab.specialReductionPercent);
        this._specialReductionAmount   = LOC_Common.round(this._specialReductionPercent * refAmount / 100, 0);
        this._reductionAmount          = this._standardReductionAmount + this._specialReductionAmount;
        this._reductionPercent         = LOC_Common.round(this._reductionAmount * 100 / this._baseAmount, 3);
        this._maxiReductionAmount      = LOC_Common.round(this._maxiReductionPercent * this._baseAmount / 100, 0);
        this._amount                   = this._baseAmount - this._reductionAmount;

        this._specialReductionStatus                  = tab.specialReductionStatus;
        this._specialReductionBillLabel               = tab.specialReductionBillLabel;
        this._hasSpecialReductionBillLabelChanged     = false;
        this._specialReductionJustification           = tab.specialReductionJustification;
        this._hasSpecialReductionJustificationChanged = false;

        this._oldBaseAmount = this._baseAmount;
        this._oldAmount     = this._amount;
        this._oldSpecialReductionBillLabel     = this._specialReductionBillLabel;
        this._oldSpecialReductionJustification = this._specialReductionJustification;

        if (this._isKeepAmount)
        {
            this._hasSpecialReduction = this._standardReductionAmount < this._baseAmount &&
                                        (this._standardReductionAmount >= this._maxiReductionAmount ||
                                         this._specialReductionPercent > 0 ||
                                         this._specialReductionStatus !=  '');
        }
        else
        {
            this._hasSpecialReduction = this._standardReductionAmount < this._baseAmount &&
                                        (this._standardReductionAmount >= this._maxiReductionAmount);
        }

        for (var name in tabExtParams)
        {
            if (this._tabExtParams.hasOwnProperty(name))
            {
                this._tabExtParams[name] = tabExtParams[name];
            }
        }

        this._tabRights = tabRights;

        if (this.onload)
        {
            this.onload();
        }
    },

    /**
     * Indique si le tarif de base a changé suite à un changement de durée
     *
     * @return bool
     */
    hasBaseAmountChangedFromDuration : function()
    {
        return this._hasBaseAmountChangedFromDuration;
    },


    /**
     * Indique si le tarif de location a changé
     *
     * @return bool
     */
    isTariffChange : function()
    {
        return this._isTariffChange;
    },

    /**
     * Récupérer le montant du tarif de location final
     *
     * @return float
     */
    getAmount : function()
    {
        return this._amount;
    },

    /**
     * Récupérer le type de montant du tarif de location
     *
     * @return string
     */
    getAmountType : function()
    {
        return this._amountType;
    },

    /**
     * Récupérer le tarif de base de location
     *
     * @return float
     */
    getBaseAmount : function()
    {
        return this._baseAmount;
    },

    /**
     * Récupérer le montant du tarif de location par jour
     *
     * @return float
     */
    getDayPrice : function()
    {
        var type = this.getAmountType();
        if (type == 'day')
        {
            return this.getAmount();
        }
        else if (type == 'month')
        {
            return (this.getAmount() / 21);
        }
        return 0;
    },

    /**
     * Récupérer un paramètre externe
     *
     * @param string name
     * @return mixed
     */
    getExternalParamValue : function(name)
    {
        return this._tabExtParams[name];
    },

    /**
     * Définir un ou plusieurs paramètres externes
     *
     * @param Object tab Paramètres à définir
     * @return bool
     */
    setExternalParams : function(tab)
    {
        var tabChanges = [];
        for (var name in tab)
        {
            if (this._tabExtParams.hasOwnProperty(name))
            {
                if (!LOC_Common.isEqual(tab[name], this._tabExtParams[name]))
                {
                    this._tabExtParams[name] = tab[name];
                    tabChanges.push('extParam_' + name);
                }
            }
        }

        if (tabChanges.length > 0 && this.onchange)
        {
            this.onchange(tabChanges);
        }
        return (tabChanges.length > 0);
    },

    /**
     * Récupérer les données du tarif de location
     *
     * @return Object
     */
    getData : function()
    {
        return {
            'days'       :                    this._days,
            'amountType' :                    this.getAmountType(),
            'baseAmount' :                    this.getBaseAmount(),
            'maxiReductionPercent' :          this.getMaxiReductionPercent(),
            'standardReductionPercent' :      this.getStandardReductionPercent(),
            'specialReductionPercent' :       this.getSpecialReductionPercent(),
            'specialReductionJustification' : this.getSpecialReductionJustification(),
            'specialReductionBillLabel' :     this.getSpecialReductionBillLabel(),
            'amount' :                        this.getAmount(),
            'isKeepAmount' :                  (this._isKeepAmount ? 1 : 0)
        };
    },

    /**
     * Récupérer le montant de remise maximium sur le tarif de location
     *
     * @return float
     */
    getMaxiReductionAmount : function()
    {
        return this._maxiReductionAmount;
    },

    /**
     * Récupérer le pourcentage de remise maximium sur le tarif de location
     *
     * @return float
     */
    getMaxiReductionPercent : function()
    {
        return this._maxiReductionPercent;
    },

    /**
     * Récupérer le montant de remise recommandé sur le tarif de location
     *
     * @return float
     */
    getRecoReductionAmount : function()
    {
        return this._recoReductionAmount;
    },

    /**
     * Récupérer le pourcentage de remise recommandé sur le tarif de location
     *
     * @return float
     */
    getRecoReductionPercent : function()
    {
        return this._recoReductionPercent;
    },

    /**
     * Récupérer le montant de remise globale sur le tarif de location
     *
     * @return float
     */
    getReductionAmount : function()
    {
        return this._reductionAmount;
    },

    /**
     * Récupérer le pourcentage de remise globale sur le tarif de location
     *
     * @return float
     */
    getReductionPercent : function()
    {
        return this._reductionPercent;
    },

    /**
     * Récupérer le montant de remise exceptionnelle sur le tarif de location
     *
     * @return float
     */
    getSpecialReductionAmount : function()
    {
        return (this._hasSpecialReduction ? this._specialReductionAmount : 0);
    },

    /**
     * Récupérer le libellé de la facture de la remise exceptionnelle sur le tarif de location
     *
     * @return string
     */
    getSpecialReductionBillLabel : function()
    {
        if (!this._hasSpecialReduction)
        {
            return '';
        }
        var status = this.getSpecialReductionStatus();
        if (status == RENTTARIFF_REDUCTIONSTATE_VALIDATED ||
            status == RENTTARIFF_REDUCTIONSTATE_REFUSED)
        {
            return this._oldSpecialReductionBillLabel;
        }
        if (((!this._isKeepAmount && this._oldBaseAmount != this._baseAmount) || this._oldAmount != this._amount) &&
            this._specialReductionBillLabel == this._oldSpecialReductionBillLabel &&
            !this._hasSpecialReductionBillLabelChanged)
        {
            return '';
        }
        return this._specialReductionBillLabel;
    },

    /**
     * Récupérer les informations sur la remise exceptionnelle
     *
     * @return array
     */
    getSpecialReductionDetails : function()
    {
        var status = this.getSpecialReductionStatus();
        var isJustifActived = (status == RENTTARIFF_REDUCTIONSTATE_VALIDATED || status == RENTTARIFF_REDUCTIONSTATE_REFUSED);
        return {
            isActived                : this._hasSpecialReduction,
            isComplementInfosVisible : (status == RENTTARIFF_REDUCTIONSTATE_REFUSED || this.getSpecialReductionPercent() > 0),
            isJustificationActived   : isJustifActived,
            isBillLabelActived       : isJustifActived
        };
    },

    /**
     * Récupérer le justificatif de la remise exceptionnelle sur le tarif de location
     *
     * @return string
     */
    getSpecialReductionJustification : function()
    {
        if (!this._hasSpecialReduction)
        {
            return '';
        }
        var status = this.getSpecialReductionStatus();
        if (status == RENTTARIFF_REDUCTIONSTATE_VALIDATED ||
            status == RENTTARIFF_REDUCTIONSTATE_REFUSED)
        {
            return this._oldSpecialReductionJustification;
        }
        if (((!this._isKeepAmount && this._oldBaseAmount != this._baseAmount) || this._oldAmount != this._amount) &&
            this._specialReductionJustification == this._oldSpecialReductionJustification &&
            !this._hasSpecialReductionJustificationChanged)
        {
            return '';
        }
        return this._specialReductionJustification;
    },

    /**
     * Récupérer le pourcentage de remise exceptionnelle sur le tarif de location
     *
     * @return float
     */
    getSpecialReductionPercent : function()
    {
        return (this._hasSpecialReduction ? this._specialReductionPercent : 0);
    },

    /**
     * Récupérer le justificatif de la remise exceptionnelle sur le tarif de location
     *
     * @return string
     */
    getSpecialReductionStatus : function()
    {
        if ((!this._isKeepAmount && this._oldBaseAmount != this._baseAmount) || this._oldAmount != this._amount)
        {
            return '';
        }
        return this._specialReductionStatus;
    },

    /**
     * Récupérer le montant de remise standard sur le tarif de location
     *
     * @return float
     */
    getStandardReductionAmount : function()
    {
        return this._standardReductionAmount;
    },

    /**
     * Récupérer le pourcentage de remise standard sur le tarif de location
     *
     * @return float
     */
    getStandardReductionPercent : function()
    {
        return this._standardReductionPercent;
    },

    /**
     * Définir le montant du tarif de location final
     *
     * @param float amount
     * @return bool TRUE si il y eu une modification sinon FALSE
     */
    setAmount : function(amount)
    {
        amount = LOC_Common.round(parseFloat(amount), 0);
        var baseAmount = this.getBaseAmount();
        if (!this.hasRight('reduc') ||
                isNaN(amount) ||
                baseAmount == 0 ||
                this.getExternalParamValue('disabledComponents').allAmounts)
        {
            return false;
        }
        if (amount < 0)
        {
            amount = 0;
        }
        if (this._amount == amount)
        {
            return false;
        }
        return this.setReductionAmount(baseAmount - amount);
    },

    /**
     * Définir le type de montant
     *
     * @param string type Type de montant
     * @return bool TRUE si il y eu une modification sinon FALSE
     */
    setAmountType : function(type)
    {
        type = (type.toLowerCase() == 'month' ? 'month' : 'day');
        if (!this.hasRight('amountType') || this._amountType == type || this.getExternalParamValue('disabledComponents').amountType)
        {
            return false;
        }
        this._amountType = type;
        this._calculateTariff(true);

        if (this.onchange)
        {
            this.onchange(['amountType', 'dayPrice']);
        }
        return true;
    },

    /**
     * Définir la durée de location
     *
     * @param float days Durée
     * @return bool TRUE si il y eu une modification sinon FALSE
     */
    setDays : function(days)
    {
        var tabChanges = [];
        days = Number(days);
        if (this._days == days)
        {
            return false;
        }
        this._days = days;
        if (this._calculateTariff(false) && this.onchange)
        {
            tabChanges.push('days');
            this._hasBaseAmountChangedFromDuration = (this._baseAmount != this._oldBaseAmount);
        }
        if (tabChanges.length > 0)
        {
            this.onchange(tabChanges);
        }
        return true;
    },

    /**
     * Définir le montant de remise globale sur le tarif de location
     *
     * @param float amount Montant
     * @return bool TRUE si il y eu une modification sinon FALSE
     */
    setReductionAmount : function(amount)
    {
        amount = LOC_Common.round(parseFloat(amount), 0);
        var baseAmount = this.getBaseAmount();
        if (!this.hasRight('reduc') ||
                isNaN(amount) ||
                baseAmount == 0 ||
                this.getExternalParamValue('disabledComponents').allAmounts)
        {
            return false;
        }
        return this.setReductionPercent(100 * amount / baseAmount);
    },

    /**
     * Définir le pourcentage de remise globale sur le tarif de location
     *
     * @param float percent Pourcentage
     * @return bool TRUE si il y eu une modification sinon FALSE
     */
    setReductionPercent : function(percent)
    {
        var baseAmount = this.getBaseAmount();
        percent = (Math.abs(percent) == Infinity ? 0 : percent);
        percent = this._toPercent(percent, true);
        if (!this.hasRight('reduc') ||
                baseAmount == 0 ||
                this._reductionPercent == percent ||
                this.getExternalParamValue('disabledComponents').allAmounts)
        {
            return false;
        }
        // Vérification du tarif de base
        if (this._calculateTariff(false))
        {
            this._isTariffChange = true;
            if (this.onchange)
            {
                this.onchange(['tariff']);
            }
            return true;
        }

        this._reductionPercent         = percent;
        this._reductionAmount          = LOC_Common.round(this._reductionPercent * baseAmount / 100, 0);
        if (this._reductionAmount >= this.getMaxiReductionAmount())
        {
            this._standardReductionAmount = this.getMaxiReductionAmount();
            if (!this.getExternalParamValue('disabledComponents').specialReduction)
            {
                this._hasSpecialReduction = (this._standardReductionAmount < baseAmount);
            }
            else
            {
                this._reductionPercent = this.getMaxiReductionPercent();
                this._reductionAmount  = this.getMaxiReductionAmount();
                this._hasSpecialReduction = false;
            }
        }
        else
        {
            this._standardReductionAmount = this._reductionAmount;
            this._hasSpecialReduction = false;
        }
        this._standardReductionPercent = LOC_Common.round(this._standardReductionAmount * 100 / baseAmount, 3);
        if (this._reductionAmount > this._standardReductionAmount)
        {
            var refAmount                 = baseAmount - this._standardReductionAmount;
            if (!this.getExternalParamValue('disabledComponents').specialReduction)
            {
                this._specialReductionAmount  = this._reductionAmount - this._standardReductionAmount;
                this._specialReductionPercent = LOC_Common.round(this._specialReductionAmount * 100 / refAmount, 3);
            }
            else
            {
                this._specialReductionAmount  = 0;
                this._specialReductionPercent = 0;
            }
        }
        else
        {
            this._specialReductionAmount  = 0;
            this._specialReductionPercent = 0;
        }
        this._amount = baseAmount - this._reductionAmount;
        this._isKeepAmount = false;

        if (this.onchange)
        {
            this.onchange(['reduction']);
        }
        return true;
    },

    /**
     * Définir le montant de remise standard sur le tarif de location
     *
     * @param float amount Montant
     * @return bool TRUE si il y eu une modification sinon FALSE
     */
    setSpecialReductionAmount : function(amount)
    {
        amount = LOC_Common.round(parseFloat(amount), 0);
        var baseAmount = this.getBaseAmount();
        if (!this.hasRight('reduc') ||
                isNaN(amount) ||
                baseAmount == 0 ||
                this.getExternalParamValue('disabledComponents').allAmounts ||
                this.getExternalParamValue('disabledComponents').specialReduction)
        {
            return false;
        }
        if (amount < 0)
        {
            amount = 0;
        }
        if (this._specialReductionAmount == amount)
        {
            return false;
        }
        var percent = 100 * amount / (baseAmount * (1 - this.getStandardReductionPercent() / 100));
        return this.setSpecialReductionPercent(percent);
    },

    /**
     * Définir le libellé de la facture de la remise exceptionnelle sur le tarif de location
     *
     * @param string text Libellé de la facture
     * @return bool TRUE si il y eu une modification sinon FALSE
     */
    setSpecialReductionBillLabel : function(text)
    {
        if (!this.hasRight('reduc') ||
                this.getExternalParamValue('disabledComponents').allAmounts ||
                this.getExternalParamValue('disabledComponents').specialReduction)
        {
            return false;
        }
        this._hasSpecialReductionBillLabelChanged = true;
        text = text + '';
        if (this._specialReductionBillLabel == text)
        {
            return false;
        }
        this._specialReductionBillLabel = text.trim();

        if (this.onchange)
        {
            this.onchange(['billLabel']);
        }
        return true;
    },

    /**
     * Définir le justificatif de la remise exceptionnelle sur le tarif de location
     *
     * @param string text Justificatif
     * @return bool TRUE si il y eu une modification sinon FALSE
     */
    setSpecialReductionJustification : function(text)
    {
        if (!this.hasRight('reduc') ||
                this.getExternalParamValue('disabledComponents').allAmounts ||
                this.getExternalParamValue('disabledComponents').specialReduction)
        {
            return false;
        }
        this._hasSpecialReductionJustificationChanged = true;
        text = text + '';
        if (this._specialReductionJustification == text)
        {
            return false;
        }
        this._specialReductionJustification = text.trim();

        if (this.onchange)
        {
            this.onchange(['justification']);
        }
        return true;
    },

    /**
     * Définir le pourcentage de remise exceptionnelle sur le tarif de location
     *
     * @param float percent Pourcentage
     * @return bool TRUE si il y eu une modification sinon FALSE
     */
    setSpecialReductionPercent : function(percent)
    {
        var baseAmount = this.getBaseAmount();
        percent = (Math.abs(percent) == Infinity ? 0 : percent);
        percent = this._toPercent(percent);
        if (!this.hasRight('reduc') ||
                baseAmount == 0 ||
                this._specialReductionPercent == percent ||
                this.getExternalParamValue('disabledComponents').allAmounts ||
                this.getExternalParamValue('disabledComponents').specialReduction)
        {
            return false;
        }
        // Vérification du tarif de base
        if (this._calculateTariff(false))
        {
            this._isTariffChange = true;
            if (this.onchange)
            {
                this.onchange(['tariff']);
            }
            return true;
        }

        this._standardReductionAmount  = this.getMaxiReductionAmount();
        this._standardReductionPercent = LOC_Common.round(this._standardReductionAmount * 100 / baseAmount, 3);
        var refAmount                  = baseAmount - this._standardReductionAmount;
        this._specialReductionPercent  = percent;
        this._specialReductionAmount   = LOC_Common.round(this._specialReductionPercent * refAmount / 100, 0);
        this._reductionAmount          = this._standardReductionAmount + this._specialReductionAmount;
        this._reductionPercent         = LOC_Common.round(this._reductionAmount * 100 / baseAmount, 3);
        this._amount                   = baseAmount - this._reductionAmount;
        this._isKeepAmount             = false;

        this._hasSpecialReduction      = (this._standardReductionAmount < baseAmount);

        if (this.onchange)
        {
            this.onchange(['reduction']);
        }
        return true;
    },

    /**
     * Définir le montant de remise standard sur le tarif de location
     *
     * @param float amount Montant
     * @return bool TRUE si il y eu une modification sinon FALSE
     */
    setStandardReductionAmount : function(amount)
    {
        amount = LOC_Common.round(parseFloat(amount), 0);
        var baseAmount = this.getBaseAmount();
        if (!this.hasRight('reduc') ||
                isNaN(amount) ||
                baseAmount == 0 ||
                this._standardReductionAmount == amount ||
                this.getExternalParamValue('disabledComponents').allAmounts
                )
        {
            return false;
        }
        return this.setStandardReductionPercent(100 * amount / baseAmount);
    },

    /**
     * Définir le pourcentage de remise standard sur le tarif de location
     *
     * @param float percent Pourcentage
     * @return bool TRUE si il y eu une modification sinon FALSE
     */
    setStandardReductionPercent : function(percent)
    {
        var baseAmount = this.getBaseAmount();
        percent = (Math.abs(percent) == Infinity ? 0 : percent);
        percent = this._toPercent(percent, true);
        var maxiReducPercent = this.getMaxiReductionPercent();
        if (percent >= maxiReducPercent)
        {
            percent = maxiReducPercent;
        }
        if (!this.hasRight('reduc') ||
                baseAmount == 0 ||
                this._standardReductionPercent == percent ||
                this.getExternalParamValue('disabledComponents').allAmounts)
        {
            return false;
        }
        // Vérification du tarif de base
        if (this._calculateTariff(false))
        {
            this._isTariffChange = true;
            if (this.onchange)
            {
                this.onchange(['tariff']);
            }
            return true;
        }

        this._standardReductionPercent = percent;
        this._standardReductionAmount  = LOC_Common.round(this._standardReductionPercent * baseAmount / 100, 0);
        this._hasSpecialReduction      = (this._standardReductionAmount < baseAmount) &&
                                         (this._standardReductionAmount >= this.getMaxiReductionAmount());

        var refAmount                  = baseAmount - this._standardReductionAmount;
        this._specialReductionAmount   = LOC_Common.round(this.getSpecialReductionPercent() * refAmount / 100, 0);

        this._reductionAmount          = this._standardReductionAmount + this.getSpecialReductionAmount();
        this._reductionPercent         = LOC_Common.round(this._reductionAmount * 100 / baseAmount, 3);
        this._amount                   = baseAmount - this._reductionAmount;
        this._isKeepAmount             = false;

        if (this.onchange)
        {
            this.onchange(['reduction']);
        }
        return true;
    },

    /**
     * Définir le tableau des tarifs
     *
     * @param Array tabTariffs
     * @return bool TRUE si il y eu une modification sinon FALSE
     */
    setTariffs : function(tabTariffs)
    {
        if (LOC_Common.isEqual(tabTariffs, this._tabTariffs))
        {
            return false;
        }
        this._tabTariffs = tabTariffs;
        if (this._calculateTariff(false) && this.onchange)
        {
            this.onchange(['tariffs']);
        }
        return true;
    },

    /**
     * Récupérer le tarif de base de location pour un nombre de jours
     *
     * @return bool
     */
    _calculateTariff : function(isResetForced)
    {
        var tabInfos = this._getTariffInfos();

        var hasChanges = isResetForced ||
                         (this._baseAmount != tabInfos.baseAmount ||
                          this._maxiReductionPercent != tabInfos.maxiReductionPercent);
        if (hasChanges)
        {
            // Changement des tarifs de location
            this._baseAmount           = tabInfos.baseAmount;
            this._maxiReductionAmount  = tabInfos.maxiReductionAmount;
            this._maxiReductionPercent = tabInfos.maxiReductionPercent;
            this._recoReductionAmount  = tabInfos.recoReductionAmount;
            this._recoReductionPercent = tabInfos.recoReductionPercent;

            if (this._baseAmount == 0)
            {
                // Dans le cas où le prix de base est à zéro
                this._standardReductionPercent = 0;
                this._standardReductionAmount  = 0;
                this._specialReductionPercent  = 0;
                this._specialReductionAmount   = 0;
                this._reductionAmount          = 0;
                this._reductionPercent         = 0;
                this._amount                   = 0;

                this._hasSpecialReduction      = false;
                this._isKeepAmount             = false;
            }
            else if (!this._isKeepAmount || isResetForced)
            {
                // Par défaut la remise standard est égale à la remise recommandée
                this._standardReductionPercent = (tabInfos.isNegotiate || this._tabCfgs.defaultStdReducMode == 0 ? this._recoReductionPercent : 0);//Gerer par pays en caracteristique
                this._standardReductionAmount  = LOC_Common.round(this._standardReductionPercent * this._baseAmount / 100, 0);
                this._specialReductionPercent  = 0;
                this._specialReductionAmount   = 0;
                this._reductionAmount          = this._standardReductionAmount;
                this._reductionPercent         = this._standardReductionPercent;
                this._amount                   = this._baseAmount - this._reductionAmount;

                this._hasSpecialReduction      = (this._standardReductionAmount < this._baseAmount && this._standardReductionAmount >= this._maxiReductionAmount);
                this._isKeepAmount             = false;
            }
            else
            {
                // Dans le cas d'un contrat en modification, on doit toujours maintenir le prix final
                this._reductionAmount  = this._baseAmount - this._amount;
                this._reductionPercent = LOC_Common.round(this._reductionAmount * 100 / this._baseAmount, 3);

                var x = (this._reductionAmount - (this._baseAmount * this._specialReductionPercent / 100)) /
                        (1 - this._specialReductionPercent / 100);
                this._standardReductionAmount  = LOC_Common.round(x, 0);
                this._standardReductionPercent = LOC_Common.round(this._standardReductionAmount * 100 / this._baseAmount, 3);
                this._specialReductionAmount   = this._reductionAmount - this._standardReductionAmount;
            }
        }
        return hasChanges;
    },

    _getTariffInfos : function()
    {
        var days = this._days;

        var multiplier = (this._amountType == 'day' ? 1 : 21);
        var i = 0;
        while (i < this._tabTariffs.length)
        {
            if ((this._tabTariffs[i].minDays == null || days >= Number(this._tabTariffs[i].minDays)) &&
                (this._tabTariffs[i].maxDays == null || days <  Number(this._tabTariffs[i].maxDays)))
            {
                return {
                    baseAmount           : Number(this._tabTariffs[i].baseAmount) * multiplier,
                    maxiReductionAmount  : LOC_Common.round(Number(this._tabTariffs[i].maxiAmount) * multiplier, 0),
                    maxiReductionPercent : Number(this._tabTariffs[i].maxiReduction),
                    recoReductionAmount  : LOC_Common.round(Number(this._tabTariffs[i].recoReduction) * multiplier, 0),
                    recoReductionPercent : Number(this._tabTariffs[i].recoReduction),
                    isNegotiate          : (this._tabTariffs[i].isNegotiate == 1)
                };
            }
            i++;
        }

        return {
            baseAmount           : 0,
            maxiReductionAmount  : 0,
            maxiReductionPercent : 0,
            recoReductionAmount  : 0,
            recoReductionPercent : 0,
            isNegotiate          : false
        };
    },

    _toPercent : function(percent, isNegativePossible)
    {
        percent = Number(percent);
        if (!isNegativePossible && percent < 0)
        {
            return 0;
        }
        else if (percent > 100)
        {
            return 100;
        }
        return LOC_Common.round(percent, 3);
    },

    /**
     * Vérifier le droit à un champ
     *
     * @return bool
     */
    hasRight : function(fieldName)
    {
        return (this._tabRights === null || this._tabRights[fieldName] ? true : false);
    }
}



Location.Rent_Helper_RentTariffView = {

    display : function(pfx, obj, tabErrors, hasMaxStdReduc)
    {
        var disabledComponents = obj.getExternalParamValue('disabledComponents');

        var selectElt = $ge(pfx + 'AmountType');
        while (selectElt.options[0])
        {
        	selectElt.removeChild(selectElt.options[0]);
        }
        for (var i = 0; i < obj._tabCfgs.tabAmountTypes.length; i++)
        {
            if (obj.getAmountType() == 'month' || !(obj._tabCfgs.tabAmountTypes[i].id == 'month' &&
                (obj.getExternalParamValue('isProformaRequired') || obj.getExternalParamValue('isProformaActived'))))
            {
                var option = new Option(obj._tabCfgs.tabAmountTypes[i].label, obj._tabCfgs.tabAmountTypes[i].id);
                try
                {
                    selectElt.add(option, null);
                }
                catch(ex)
                {
                    selectElt.add(option);
                }
            }
        }

        selectElt.value = obj.getAmountType();
        if (selectElt.selectedIndex != -1)
        {
            var typeLabel = selectElt.options[selectElt.selectedIndex].innerHTML;
            $ge(pfx + 'AmountTypeLabel').innerHTML = typeLabel;
        }
        selectElt.disabled = (!obj.hasRight('amountType') || disabledComponents.amountType ? true : false);

        var baseAmount = obj.getBaseAmount();
        var isReadOnly = (baseAmount == 0 || !obj.hasRight('reduc') || disabledComponents.allAmounts ? true : false);

        Location.currenciesManager.getCurrency(pfx + 'BaseAmount').setValue(baseAmount, false);
        var maxiReduction = obj.getMaxiReductionPercent();
        $ge(pfx + 'RecoReduc').innerHTML = obj.getRecoReductionPercent().toFixed(3);
        $ge(pfx + 'MaxiReduc').innerHTML = maxiReduction.toFixed(3);
        Location.currenciesManager.getCurrency(pfx + 'MaxiAmount').setValue(obj.getMaxiReductionAmount(), false);

        var el = Location.spinControlsManager.getSpinControl(pfx + 'StdReduc');
        if (hasMaxStdReduc)
        {
            el.setMaximumValue(maxiReduction);
        }
        el.setValue(obj.getStandardReductionPercent(), false);
        el.setDisabled(isReadOnly);

        el = Location.currenciesManager.getCurrency(pfx + 'StdReducAmount');
        el.setValue(obj.getStandardReductionAmount(), false);
        el.setDisabled(isReadOnly);

        var tabSpecialReductionDetails = obj.getSpecialReductionDetails();
        el = Location.spinControlsManager.getSpinControl(pfx + 'SpcReduc');
        el.setValue(obj.getSpecialReductionPercent(), false);
        el.setDisabled((isReadOnly || !tabSpecialReductionDetails.isActived || disabledComponents.specialReduction ? true : false));

        el = Location.currenciesManager.getCurrency(pfx + 'SpcReducAmount');
        el.setValue(obj.getSpecialReductionAmount(), false);
        el.setDisabled((isReadOnly || !tabSpecialReductionDetails.isActived || disabledComponents.specialReduction ? true : false));

        if (tabSpecialReductionDetails.isComplementInfosVisible)
        {
            el = $ge(pfx + 'SpcReducJustification');
            el.className = 'spcReducJustification' + (tabErrors.noJustification ? ' error' : '');
            el.value = obj.getSpecialReductionJustification();
            el.readOnly = (isReadOnly || tabSpecialReductionDetails.isJustificationActived);
            el.disabled = false;

            el = $ge(pfx + 'SpcReducBillLabel');
            el.value = obj.getSpecialReductionBillLabel();
            el.readOnly = (isReadOnly || tabSpecialReductionDetails.isBillLabelActived);
            el.disabled = false;

            $ge(pfx + 'SpcReducStatus').className = 'spcReducStatus ' + obj.getSpecialReductionStatus();
            $ge(pfx + 'SpcReducInfos').style.visibility = 'visible';
        }
        else
        {
            $ge(pfx + 'SpcReducJustification').disabled = true;
            $ge(pfx + 'SpcReducBillLabel').disabled = true;
            $ge(pfx + 'SpcReducInfos').style.visibility = 'hidden';
        }
        var reducPercent = obj.getReductionPercent();
        el = Location.spinControlsManager.getSpinControl(pfx + 'Reduc');
        el.setValue(reducPercent, false);
        el.setDisabled(isReadOnly);
        // Affichage du message de remise négative
        $ge(pfx + 'ReducNegativeMsg').style.display = (reducPercent < 0 ? '' : 'none');
        $ge(pfx + 'TariffChangeMsg').style.visibility = (obj.isTariffChange() ? '' : 'hidden');

        el = Location.currenciesManager.getCurrency(pfx + 'ReducAmount');
        el.setValue(obj.getReductionAmount(), false);
        el.setDisabled(isReadOnly);

        el = Location.currenciesManager.getCurrency(pfx + 'Amount');
        el.setValue(obj.getAmount(), false);
        el.setDisabled(isReadOnly);
    },

    initEvents : function(pfx, obj)
    {
        // - Modification du type de montant du tarif de location
        $ge(pfx + 'AmountType').onchange = function()
        {
            obj.setAmountType(this.value);
        }

        // - Modification du pourcentage de remise standard
        Location.spinControlsManager.getSpinControl(pfx + 'StdReduc').onchange = function()
        {
            obj.setStandardReductionPercent(this.getValue());
            this.setValue(obj.getStandardReductionPercent());
        }

        // - Modification du montant de remise standard
        Location.currenciesManager.getCurrency(pfx + 'StdReducAmount').onchange = function()
        {
            obj.setStandardReductionAmount(this.getValue());
            this.setValue(obj.getStandardReductionAmount(), false);
        }

        // - Modification du pourcentage de remise exceptionnelle
        Location.spinControlsManager.getSpinControl(pfx + 'SpcReduc').onchange = function()
        {
            obj.setSpecialReductionPercent(this.getValue());
            this.setValue(obj.getSpecialReductionPercent());
        }

        // - Modification du montant de remise exceptionnelle
        Location.currenciesManager.getCurrency(pfx + 'SpcReducAmount').onchange = function()
        {
            obj.setSpecialReductionAmount(this.getValue());
            this.setValue(obj.getSpecialReductionAmount(), false);
        }

        // - Modification du justiticatif de remise exceptionnelle
        $ge(pfx + 'SpcReducJustification').onchange = function()
        {
            obj.setSpecialReductionJustification(this.value);
            this.value = obj.getSpecialReductionJustification();
        }

        // - Modification du libellé de la facture de remise exceptionnelle
        $ge(pfx + 'SpcReducBillLabel').onchange = function()
        {
            obj.setSpecialReductionBillLabel(this.value);
            this.value = obj.getSpecialReductionBillLabel();
        }

        // - Modification du pourcentage de remise globale
        Location.spinControlsManager.getSpinControl(pfx + 'Reduc').onchange = function()
        {
            obj.setReductionPercent(this.getValue());
        }

        // - Modification du montant de remise globale
        Location.currenciesManager.getCurrency(pfx + 'ReducAmount').onchange = function()
        {
            obj.setReductionAmount(this.getValue());
            this.setValue(obj.getReductionAmount(), false);
        }

        // - Modification du montant du tarif de location
        Location.currenciesManager.getCurrency(pfx + 'Amount').onchange = function()
        {
            obj.setAmount(this.getValue());
            this.setValue(obj.getAmount(), false);
        }
    }

}