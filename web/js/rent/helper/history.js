
if (!window.Location)
{
    var Location = new Object();
}


/*
 * Objet de gestion des historiques
 *
 */
Location.Rent_Helper_History = function(documentObj, tabCfgs)
{
    this._documentObj = documentObj;
    this._tabCfgs = tabCfgs;

    this._tab = [];

    this._tabRights = null;

    this.onchange = null;
    this.onload   = null;

    this._tabExtParams = {};

}

Location.Rent_Helper_History.prototype = {


    /**
     * Chargement de données
     *
     * @param Object tab          Données
     * @param Object tabExtParams Paramètres externes
     * @param Object tabRights    Droits
     */
    load : function(tab, tabExtParams, tabRights)
    {

        this._tab = tab;

        for (var name in tabExtParams)
        {
            if (this._tabExtParams.hasOwnProperty(name))
            {
                this._tabExtParams[name] = tabExtParams[name];
            }
        }

        this._tabRights = tabRights;

        if (this.onload)
        {
            this.onload();
        }
    },

    /**
     * Récupérer les historiques
     *
     * @return Array
     */
    getList : function()
    {
        return this._tab;
    }

}


Location.Rent_Helper_HistoryView = {


    /**
     * Mise à jour de l'affichage
     *
     */
    display : function(pfx, obj, tabErrors)
    {
        var tableEl = $ge(pfx + 'list');
        var tBodyEl = tableEl.tBodies[0];
        var viewExtraBtn = $ge(pfx + 'viewExtraBtn');
        var modalTBodyEl = $ge(pfx + 'modalTable').tBodies[0];
        var mySelf = this;

        // Suppression des lignes
        while (tBodyEl.rows.length > 0)
        {
            tBodyEl.deleteRow(0);
        }

        var tab = obj.getList();

        var nb = tab.length;
        tableEl.className = 'basic' + (nb == 0 ? ' msg noResult' : '');

        for (var i = 0; i < nb; i++)
        {
            var rEl, cEl;

            rEl = tBodyEl.insertRow(-1);
            rEl.className = 'entity_' + tab[i].entity;

            // Date
            cEl = rEl.insertCell(-1);
            cEl.className = 'date';
            cEl.innerHTML = tab[i].date;
            // Utilisateur
            cEl = rEl.insertCell(-1);
            cEl.className = 'user' + (!tab[i].isUserActive ? ' inactive' : '');
            cEl.innerHTML = tab[i].userName;
            // Entité
            cEl = rEl.insertCell(-1);
            cEl.className = 'entity';
            cEl.appendChild(window.document.createElement('div'));
            cEl.firstChild.title = LOC_View.getTranslation(pfx + 'entity_' + tab[i].entity);
            // Evénement
            cEl = rEl.insertCell(-1);
            cEl.className = 'event';
            cEl.innerHTML = tab[i].event;
            // Boutons
            cEl = rEl.insertCell(-1);
            cEl.className = 'buttons';
            if (tab[i].extra != null)
            {
                var btn = window.document.createElement('a');
                btn.className = 'locCtrlButton extra';
                btn.title = LOC_View.getTranslation(pfx + 'viewExtra');
                btn.setAttribute('data-param', i);
                btn.onclick = function()
                {
                    var modalREl, modalCEl;

                    // Suppression de toutes les lignes
                    while (modalTBodyEl.rows.length)
                    {
                        modalTBodyEl.deleteRow(0);
                    }

                    var tabData = eval('(' + tab[this.getAttribute('data-param')].extra + ')');
                    var nbData = tabData.length;
                    for (var i = 0; i < nbData; i++)
                    {
                        // Ajout d'une nouvelle ligne
                        modalREl = modalTBodyEl.insertRow(-1);

                        // Titre du champ
                        modalCEl = modalREl.insertCell(-1);
                        modalCEl.className = 'label';
                        modalCEl.innerHTML = LOC_View.getTranslation(pfx + 'extra_' + tabData[i].prop);

                        // Valeur du champ
                        modalCEl = modalREl.insertCell(-1);
                        modalCEl.className = 'content';
                        modalCEl.innerHTML = tabData[i].value;
                    }

                    Location.modalWindowManager.show(pfx + 'modal', {contentType: 3, width: 450, height: 'auto'});
                };

                cEl.appendChild(btn);
            }
            // Description
            cEl = rEl.insertCell(-1);
            cEl.className = 'desc';
            cEl.innerHTML = tab[i].description || '';
        }
    },

    /**
     * Initialisation
     *
     */
    initEvents : function(pfx, obj)
    {
    }

}
