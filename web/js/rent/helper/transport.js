/**
 * $Revision: 841 $
 * $Date: 2008-09-08 12:40:04 +0200 (lun., 08 sept. 2008) $
 * $Author: jodesous $
 */


if (!window.Location)
{
    var Location = new Object();
}

/**
 * Classe de gestion de la tarification transport
 *
 * @param Object documentObj Objet javascript du document (ligne de devis ou contrat)
 * @param Object tabCfgs     Configurations globales
 */
Location.Rent_Helper_Transport = function(documentObj, tabCfgs)
{
    this._documentObj = documentObj;
    this._tabCfgs = tabCfgs;

    this._cttsHttpObj = null;
    this._doHttpObj = null;

    this._isEstimationChange = false;

    // Le montant de base de l'estimation
    this._baseAmount = 0;
    // Le statut de la remise exceptionnelle
    this._specialReductionStatus = '';
    // Le pourcentage de remise standard
    this._standardReductionPercent = 0;
    // Le montant de réduction standard
    this._standardReductionAmount = 0;
    // Le pourcentage de remise recommandée
    this._recoReductionPercent = 0;
    // Le pourcentage de remise maximum
    this._maxiReductionPercent = 0;
    // Le montant de remise maximum
    this._maxiReductionAmount = 0;
    // Le poucentage de remise exceptionnelle
    this._specialReductionPercent = 0;
    // Le montant de remise exceptionnelle
    this._specialReductionAmount = 0;
    // Le pourcentage de réduction globale
    this._reductionPercent = 0;
    // Le montant de réduction globale
    this._reductionAmount = 0;
    // Le montant de l'estimation
    this._amount = 0;
    // Identifiant de l'estimation transport
    this._id = 0;
    // Identifiant de la remise exceptionnelle
    this._specialReductionId = 0;
    // Zone et client de l'estimation
    this._zone = '';
    this._customerId = 0;
    // Possibilités de modif
    this._possibilities = {};

    this._tabSides = {'d' : {}, 'r' : {}};
    this._tabOldSides = null;
    this._tabOldIsEditable = {'d' : null, 'r' : null};

    this._tabAction  = null;

    this._tabExtParams = {
        duration: 0,
        isDeliveryAnticipatable : false,
        deliveryHour: '',
        recoveryHour: '',
        beginDate: '',
        endDate: '',
        tabMachineInfos: null,
        disabledComponents: {'d' : {}, 'r' : {}}
    };
    this._tabRights = null;

    this.onchange = null;
    this.onload   = null;
}

Location.Rent_Helper_Transport.prototype = {


    /**
     * Fonction de verification de la validité de l'estimation
     *
     * @return array Le tableau contenant les codes d'erreurs
     */
    checkEstimation : function()
    {
        var tabErrors = [];
        // si on est dans un pays qui utilise la tarification transport
        if (this.isEstimationActived())
        {
            // si on a une estimation transport
            if (this._id != 0)
            {
                var customerId, zone;
                // si c'est une ligne de devis
                if (this._documentObj.getObjectType() == 'estimateLine')
                {
                    var estimateObj = this._documentObj.getEstimateObject();
                    customerId = estimateObj.getCustomerId();
                    zone       = estimateObj.getZone();
                }
                // si c'est un contrat
                else if (this._documentObj.getObjectType() == 'contract')
                {
                    customerId = this._documentObj.getCustomerId();
                    zone       = this._documentObj.getZone();
                }

                // vérification du client
                if (this._customerId != customerId)
                {
                    tabErrors.push(303);
                }
                // vérification de la zone
                if (this._zone != zone)
                {
                    tabErrors.push(302);
                }
                // vérification que le prix est correct
                if (this._possibilities.delivery.amount && !this._tabSides['d'].isAddMachine &&
                    !this._tabSides['d'].isSelf && this._tabSides['d'].amount < this._amount)
                {
                    tabErrors.push(304);
                }
                if (this._possibilities.recovery.amount && !this._tabSides['r'].isAddMachine &&
                    !this._tabSides['r'].isSelf && this._tabSides['r'].amount < this._amount)
                {
                    tabErrors.push(305);
                }
            }
            else
            {
                tabErrors.push(301);
            }
        }

        if (this._possibilities.delivery.amount && this._tabSides['d'].isAddMachine &&
            this._tabSides['d'].amount < this.getAddMachineMinPrice())
        {
            tabErrors.push(308);
        }
        if (this._possibilities.recovery.amount && this._tabSides['r'].isAddMachine &&
            this._tabSides['r'].amount < this.getAddMachineMinPrice())
        {
            tabErrors.push(309);
        }

        this._documentObj.setErrors([301, 302, 303, 304, 305, 308, 309], 2);
        this._documentObj.setErrors(tabErrors, 1);
        return (tabErrors.length == 0);
    },

    /**
     * Copier les informations d'une ligne vers la courante
     *
     * @param Location.Rent_Helper_Transport sourceObj
     * @return bool
     */
    copy : function(sourceObj)
    {
        var tabProps = ['tabExtParams', 'baseAmount',
                        'maxiReductionAmount', 'maxiReductionPercent', 'recoReductionPercent',
                        'reductionAmount', 'reductionPercent',
                        'standardReductionAmount', 'standardReductionPercent',
                        'specialReductionAmount', 'specialReductionPercent',
                        'amount',
                        'zone', 'customerId', 'isEstimationChange', 'tabSides'];

        // Sauvegarde des possibilités pour chacun des côtés
        var tabSidesPossibilities = {
                'd' : LOC_Common.clone(this._tabSides.d.possibilities),
                'r' : LOC_Common.clone(this._tabSides.r.possibilities)
        };

        var i, prop;
        var nb = tabProps.length;
        for (i = 0; i < nb; i++)
        {
            prop = '_' + tabProps[i];
            this[prop] = LOC_Common.clone(sourceObj[prop]);
        }

        this._specialReductionStatus = '';

        this._tabSides.d.possibilities = tabSidesPossibilities.d;
        this._tabSides.r.possibilities = tabSidesPossibilities.r;

        this._tabOldSides = LOC_Common.clone(this._tabSides);
        this._tabOldIsEditable = {
            'd': this.isSideEditable('d'),
            'r': this.isSideEditable('r')
        };

        var estimationId = sourceObj.getId();
        if (estimationId > 0)
        {
            var duplicateHttpObj = new Location.HTTPRequest(this._tabCfgs.transportWebServicesUrl);

            // Passage des paramètres
            duplicateHttpObj.setVar('id', 'duplicate_estimation');
            if (this._documentObj.getObjectType() == 'estimateLine')
            {
                duplicateHttpObj.setVar('estimationId', estimationId);
                duplicateHttpObj.setVar('tempId', this._documentObj.getId());
                duplicateHttpObj.setVar('generalTempId', 0);
            }
            else
            {
                duplicateHttpObj.setVar('estimationId', estimationId);
                duplicateHttpObj.setVar('tempId', 0);
                duplicateHttpObj.setVar('generalTempId', this._documentObj.getId());
            }

            // Envoi de la requête HTTP
            duplicateHttpObj.send(false);

            // Récupération des informations
            var response = duplicateHttpObj.getResponse();
            var newId = Number(response);
            if (!isNaN(newId) && newId > 0)
            {
                this._id = newId;

                // Vérification de l'estimation
                this.checkEstimation();
            }
            else
            {
                return false;
            }
        }

        return true;
    },


    /**
     * Réalise ou "déréalise" le transport de livraison ou de récupération
     *
     * @param str  side   Dans quel bloc se situe-t-on (livraison = D, récupération = R)
     * @param str  typeAction ("current" : simple, "linked" : avec un contrat lié)
     */
    doSide : function (side, choice)
    {
        var tabSide = this.getSideInfos(side);

        var typeRight = (tabSide.isSelf ? 'self' : 'std');

        if (this._tabAction || !tabSide.action || !this.hasRight(tabSide.action.type + '-' + typeRight))
        {
            return false;
        }

        // mise à jour loading/unloading done
        var tabAction = tabSide.action.msgs[choice].action;
        if (typeof tabAction.loading != 'undefined')
        {
            this._tabSides[side].isLoadingDone = tabAction.loading;
        }
        if (typeof tabAction.unloading != 'undefined')
        {
            this._tabSides[side].isUnloadingDone = tabAction.unloading;
        }

        this._tabAction = {
            side   : side,
            type   : tabSide.action.type,
            doLink : (choice == 'linked' ? 1 : 0)
        };

        if (this.onchange)
        {
            this.onchange([side == 'd' ? 'delivery.action' : 'recovery.action']);
        }
    },

    /**
     * Chargement de données
     *
     * @param Object tab          Données
     * @param Object tabExtParams Paramètres externes
     * @param Object tabRights    Droits
     */
    load : function(tab, tabExtParams, tabRights)
    {
        this._id = (tab.tabTariff && tab.tabTariff.id ? Number(tab.tabTariff.id) : 0);
        if (this.isEstimationActived() && this._id != 0)
        {
            this._baseAmount = tab.tabTariff.baseAmount;
            this._recoReductionPercent = tab.tabTariff.recoStandardPercent;
            this._maxiReductionPercent = tab.tabTariff.maxiStandardPercent;
            this._maxiReductionAmount = tab.tabTariff.maxiStandardAmount;
            this._specialReductionPercent = tab.tabTariff.specialReductionPercent;
            this._specialReductionAmount = tab.tabTariff.specialReductionAmount;
            this._specialReductionStatus = tab.tabTariff.specialReductionStatus;
            this._standardReductionAmount = tab.tabTariff.standardReductionAmount;
            this._standardReductionPercent = tab.tabTariff.standardReductionPercent;
            this._reductionAmount = tab.tabTariff.reductionAmount;
            this._reductionPercent = tab.tabTariff.reductionPercent;
            this._amount = tab.tabTariff.amount;
            this._specialReductionId = tab.tabTariff.specialReductionId;
            this._customerId = tab.tabTariff.customerId;
            this._zone = tab.tabTariff.zone;
            this._isEstimationChange = (tab.tabTariff.isNoSaved == 1);
        }
        else
        {
            this._baseAmount = 0;
            this._recoReductionPercent = 0;
            this._maxiReductionPercent = 0;
            this._maxiReductionAmount = 0;
            this._specialReductionPercent = 0;
            this._specialReductionAmount = 0;
            this._specialReductionStatus = '';
            this._standardReductionAmount = 0;
            this._standardReductionPercent = 0;
            this._reductionAmount = 0;
            this._reductionPercent = 0;
            this._amount = 0;
            this._specialReductionId = 0;
            this._customerId = 0;
            this._zone = '';
        }
        this._possibilities = tab.tabTariff.possibilities;

        this._tabSides = {
            'd': tab.tabDelivery,
            'r': tab.tabRecovery
        };
        this._tabSides['d'].nbContractsForSiteTransfer = 0;
        this._tabSides['r'].nbContractsForSiteTransfer = 0;

        for (var name in tabExtParams)
        {
            if (this._tabExtParams.hasOwnProperty(name))
            {
                this._tabExtParams[name] = tabExtParams[name];
            }
        }

        this._tabAction = null;

        this._tabRights = tabRights;

        // Sauvegarde des données
        this._tabOldSides = LOC_Common.clone(this._tabSides);
        this._tabOldIsEditable = {
            'd': this.isSideEditable('d'),
            'r': this.isSideEditable('r')
        };
        // Vérification de l'estimation
        this.checkEstimation();

        if (this.onload)
        {
            this.onload();
        }
    },

    /**
     * Récupérer le prix minimum pour une machine supplémentaire
     *
     * @return float
     */
    getAddMachineMinPrice : function()
    {
        return this._tabCfgs.addMachineMinPrice;
    },

    /**
     * Fonction de récupération du montant de l'estimation
     *
     * @return int
     */
    getAmount : function()
    {
        return this._amount;
    },

    /**
     * Récupérer la liste des contrats pour les transferts de chantier
     *
     * @param string side             Coté ('d' : livraison, 'r' : récupération)
     * @param obj    tabInfos         Informations sur le transport en cours
     *
     * @return bool
     */
    getContractsForSiteTransfer : function(side, tabInfos, onCompletion)
    {
        var mySelf = this;
        var completionFunc = function(tab)
        {
            // Stockage du nombre de contrats disponibles pour transfert dans l'objet du transport
            var oldNbCtts = mySelf._tabSides[side].nbContractsForSiteTransfer;
            var newNbCtts = (tab ? tab.length : 0);

            mySelf._tabSides[side].nbContractsForSiteTransfer = newNbCtts;
            if (oldNbCtts != newNbCtts)
            {
                mySelf.onchange([(side == 'r' ? 'recovery' : 'delivery') + '.nbContractsForSiteTransfer']);
            }

            if (onCompletion)
            {
                onCompletion(tab);
            }
        }

        if (!this._tabCfgs.transportContractsTsfUrl)
        {
            completionFunc([]);
            return;
        }
        if (!this._cttsHttpObj)
        {
            this._cttsHttpObj = new Location.HTTPRequest(this._tabCfgs.transportContractsTsfUrl);
        }

        var modelId = (this._documentObj.getMachineId() ? this._documentObj.getMachineModelId()
                                                        : this._documentObj.getModelId());
        this._cttsHttpObj.setVar('countryId', this._documentObj.getCountryId());
        this._cttsHttpObj.setVar('agencyId', this._documentObj.getAgencyId());
        this._cttsHttpObj.setVar('modelId', modelId);
        this._cttsHttpObj.setVar('idToExclude', this._documentObj.getId());
        this._cttsHttpObj.setVar('idToInclude', (this._tabOldSides[side].contract ? this._tabOldSides[side].contract.id : null));
        this._cttsHttpObj.setVar('side', side.toUpperCase());
        this._cttsHttpObj.setVar('isSelf', (tabInfos.self.isActive ? 1 : 0));

        // Livraison
        if (side == 'd')
        {
            this._cttsHttpObj.setVar('isAnticipatable', (this.getExternalParamValue('isDeliveryAnticipatable') ? 1 : 0));
            this._cttsHttpObj.setVar('referenceDate', this.getExternalParamValue('beginDate'));
            this._cttsHttpObj.setVar('referenceHour', this.getExternalParamValue('deliveryHour'));
        }
        // Récupération
        else if (side == 'r')
        {
            this._cttsHttpObj.setVar('referenceDate', this.getExternalParamValue('endDate'));
            this._cttsHttpObj.setVar('referenceHour', this.getExternalParamValue('recoveryHour'));

            // Y a-t-il une visibilité machine ?
            var visibleOnAgencyId;
            var tabMachineInfos = this.getExternalParamValue('tabMachineInfos');
            if (tabInfos.transferToAgency.isActive)
            {
                visibleOnAgencyId = (tabInfos.transferToAgency.value ? tabInfos.transferToAgency.value.id : null);
            }
            else if (tabMachineInfos.visibilityMode == MACHINE_VISIBILITYMODE_MACHINE)
            {
                visibleOnAgencyId = tabMachineInfos.visibleOnAgencyId;
            }
            this._cttsHttpObj.setVar('visibleOnAgencyId',visibleOnAgencyId);
        }

        this._cttsHttpObj.oncompletion = function()
        {
            completionFunc(this.getJSONResponse());
        };
        this._cttsHttpObj.onerror = function()
        {
            completionFunc(false);
        };

        // Envoi de la requête HTTP
        this._cttsHttpObj.send(true, true);
    },

    /**
     * Fonction de récupération du montant de base
     *
     * @return int
     */
    getBaseAmount : function()
    {
        return this._baseAmount;
    },

    /**
     * Récupérer les données du tarif transport
     *
     * @return Object
     */
    getData : function()
    {
        var tabSideD = this._tabSides['d'];
        var tabSideR = this._tabSides['r'];

        return {
            'tabTariff' : {
                'id' :                   this.getId(),
                'deliveryAmount' :       tabSideD.amount,
                'isNoDelivery' :         (tabSideD.isSelf ? 1 : 0),
                'isAddMachineDelivery' : (tabSideD.isAddMachine ? 1 : 0),
                'addMachineDeliveryLnkCtt.id' : (tabSideD.addMachineLnkCtt ? tabSideD.addMachineLnkCtt.id : null),
                'isTransferDelivery' :   (tabSideD.contract ? 1 : 0),
                'recoveryAmount' :       tabSideR.amount,
                'isNoRecovery' :         (tabSideR.isSelf ? 1 : 0),
                'amount' :               this.getAmount(),
                'isAddMachineRecovery' : (tabSideR.isAddMachine ? 1 : 0),
                'addMachineRecoveryLnkCtt.id' : (tabSideR.addMachineLnkCtt ? tabSideR.addMachineLnkCtt.id : null),
                'isTransferRecovery' :   (tabSideR.contract ? 1 : 0)
            },
            'tabDelivery' : {
                'data' : {
                    'isSelf' : (tabSideD.isSelf ? 1 : 0),
                    'delegatedAgency.id' : tabSideD.delegatedAgencyId,
                    'transferToAgency.id' : null,
                    'contract.id' : (tabSideD.contract ? tabSideD.contract.id : null),
                    'isSynchroDisabled' : (tabSideD.isSynchroDisabled ? 1 : 0)
                    },
                'action' : this.getSideAction('d')
            },
            'tabRecovery' : {
                'data' : {
                    'isSelf' : (tabSideR.isSelf ? 1 : 0),
                    'delegatedAgency.id' : tabSideR.delegatedAgencyId,
                    'transferToAgency.id' : (tabSideR.transferToAgency ? tabSideR.transferToAgency.id : null),
                    'contract.id' :  (tabSideR.contract ? tabSideR.contract.id : null),
                    'isSynchroDisabled' : (tabSideR.isSynchroDisabled ? 1 : 0)
                },
                'action' : this.getSideAction('r')
            }
        };
    },

    /**
     * Récupérer les actions en cours d'un côté du transport (bloc livraison ou récupération)
     *
     * @return Object
     */
    getSideAction : function(side)
    {
        return (this._tabAction && this._tabAction.side == side ? this._tabAction : null);
    },

    /**
     * Fonction de récupération du montant du transport aller
     *
     * @return int
     */
    getDeliveryAmount : function()
    {
        return this._tabSides['d'].amount;
    },

    /**
     * Récupérer instance du document associé
     *
     * @return Object
     */
    getDocumentObject : function()
    {
        return this._documentObj;
    },

    /**
     * Récupérer un paramètre externe
     *
     * @param string name
     * @return mixed
     */
    getExternalParamValue : function(name)
    {
        return this._tabExtParams[name];
    },

    /**
     * Définir un ou plusieurs paramètres externes
     *
     * @param Object tab Paramètres à définir
     * @return bool
     */
    setExternalParams : function(tab)
    {
        var tabChanges = [];
        for (var name in tab)
        {
            if (this._tabExtParams.hasOwnProperty(name))
            {
                if (!LOC_Common.isEqual(tab[name], this._tabExtParams[name]))
                {
                    this._tabExtParams[name] = tab[name];
                    tabChanges.push('extParam_' + name);
                }
            }
        }

        if (tabChanges.length > 0 && this.onchange)
        {
            this.onchange(tabChanges);
        }
        return (tabChanges.length > 0);
    },

    /**
     * Fonction de récupération de l'identifiant de l'estimation transport
     *
     * @return int
     */
    getId : function()
    {
        return this._id;
    },

    /**
     * Fonction de récupération du montant de remise maximum
     *
     * @return int
     */
    getMaxiReductionAmount : function()
    {
        return this._maxiReductionAmount;
    },

    /**
     * Fonction de récupération du pourcentage de remise maximum
     *
     * @return int
     */
    getMaxiReductionPercent : function()
    {
        return this._maxiReductionPercent;
    },

    /**
     * Fonction de récupération du poucentage de remise recommandée
     *
     * @return int
     */
    getRecoReductionPercent : function()
    {
        return this._recoReductionPercent;
    },

    /**
     * Fonction de récupération du montant du transport retour
     *
     * @return int
     */
    getRecoveryAmount : function()
    {
        return this._tabSides['r'].amount;
    },

    /**
     * Fonction de récupération de montant de remise globale
     *
     * @return int
     */
    getReductionAmount : function()
    {
        return this._reductionAmount;
    },

    /**
     * Fonction de récupération du pourcentage de remise globale
     *
     *    @return int
     */
    getReductionPercent : function()
    {
        return this._reductionPercent;
    },

    /**
     * Récupérer les infos d'un côté du transport (bloc livraison ou récupération)
     *
     * @param string  side       Côté
     * @param boolean isOldInfos Indique si on souhaite les anciennes valeurs
     * @return Object
     */
    getSideInfos : function(side, isOldInfos)
    {
        return (isOldInfos ? this._tabOldSides[side] : this._tabSides[side]);
    },

    /**
     * Récupérer les configs d'un côté du transport (bloc livraison ou récupération)
     *
     * @param string  side       Côté
     * @return Object
     */
    getSideCfgs : function(side)
    {
        return this._getSideCfgs(this.getSideInfos(side));
    },

    /**
     * Fonction de récupération du montant de remise exceptionnelle
     *
     * @return int
     */
    getSpecialReductionAmount : function()
    {
        return this._specialReductionAmount;
    },

    /**
     * Fonction de récupération de l'identifiant de la remise exceptionnelle transport
     *
     * @return int
     */
    getSpecialReductionId : function()
    {
        return this._specialReductionId;
    },

    /**
     * Fonction de récupération du poucentage de remise exceptionnelle
     *
     * @return int
     */
    getSpecialReductionPercent : function()
    {
        return this._specialReductionPercent;
    },

    /**
     * Fonction de récupération du statut de la remise exceptionnelle
     *
     * @return String
     */
    getSpecialReductionStatus : function()
    {
        return this._specialReductionStatus;
    },

    /**
     * Fonction de récupération du montant de remise standard
     *
     * @return int
     */
    getStandardReductionAmount : function()
    {
        return this._standardReductionAmount;
    },

    /**
     * Fonction de récupération du pourcentage de remise standard
     *
     * @return int
     */
    getStandardReductionPercent : function()
    {
        return this._standardReductionPercent;
    },

    /**
     * Indique si il y a un transfert ou une reprise sur chantier active
     *
     * @return bool
     */
    isTransferExists : function()
    {
        return (this._tabSides['d'].contract || this._tabSides['r'].contract);
    },

    /**
     * Fonction de récupération du marqueur de tarification transport
     *
     * @return bool
     */
    isEstimationActived : function()
    {
        return (this._tabCfgs.isTrspTarification == 1);
    },

    /**
     * Fonction de récupération du marqueur de tarification transport
     *
     * @return bool
     */
    isEstimationChange : function()
    {
        return this._isEstimationChange;
    },

    /**
     * Fonction de récupération du marqueur de tarification transport
     *
     * @return bool
     */
    isEstimationPossible : function()
    {
        return (this._possibilities.estimation &&
                this.hasRight('estimation') &&
                !(this.getExternalParamValue('disabledComponents').estimation));
    },

    /**
     * Indique si la livraison ou la récupération est réalisable
     *
     * @param string side Côté : 'd' pour une livraison, 'r' pour une récupération
     *
     * @return bool
     */
    isSideDoable : function(side)
    {
        var typeRight = (this._tabSides[side].isSelf ? 'self' : 'std');
        var otherSide = (side == 'd' ? 'r' : 'd');
        var disabledComponents = this.getExternalParamValue('disabledComponents');

               // L'utilisateur a le droit de réaliser
        return this.hasRight('do-' + typeRight) &&
               // le composant de réalisation/annulation n'est pas désactivé
               !disabledComponents[side].doSide &&
               // Action de réalisation permise par l'état du contrat et du transport
               this._tabSides[side].action && this._tabSides[side].action.type == 'do' &&
               // Pas de modification en attente de validation sur les 2 côtés
               !this.isSideUpdated(side) && !this.isSideUpdated(otherSide) &&
               // Pas d'action en cours
               !this.getSideAction(side);
    },

    /**
     * Indique si la livraison ou la récupération est modifiable (par la popup)
     *
     * @return bool
     */
    isSideEditable : function(side, isOldInfos)
    {
        if (isOldInfos)
        {
            return this._tabOldIsEditable[side];
        }

        var otherSide = (side == 'd' ? 'r' : 'd');
        // Si une action est en cours
        // ou si une modification est cours sur l'autre côté
        if (this.getSideAction(side) || this.getSideAction(otherSide) || this.isSideUpdated(otherSide))
        {
            return false;
        }

        var sideTariff = (side == 'd' ? 'delivery' : 'recovery');
        var t = this._possibilities[sideTariff];
        var tabSideInfos = this.getSideInfos(side);
        var isDone = (side == 'd' ? tabSideInfos.isUnloadingDone : tabSideInfos.isLoadingDone);

                // pas dans le cas d'un enlèvement standard réalisé
        return (!(isDone && tabSideInfos.isSelf && !tabSideInfos.contract) &&
                // droits et possibilités montant
                (((t.amount && this.hasRight('amount')) ||
                // droits et possibilités machine sup
                 (t.addMachine && this.hasRight('addMachine'))) ||
                // possibilités d'édition popup et droits éléments popup
                (tabSideInfos.possibilities.edit && (this.hasRight('self') ||
                        this.hasRight('transfer') ||
                        this.hasRight('delegate') ||
                        this.hasRight('transferToAgency') ||
                        this.hasRight('synchro')))) ? true : false)
    },

    /**
     * Indique si la réalisation de la livraison ou de la récupération peut être annulée
     *
     * @param string side Côté : 'd' pour une livraison, 'r' pour une récupération
     *
     * @return bool
     */
    isSideUndoable : function(side)
    {
        var typeRight = (this._tabSides[side].isSelf ? 'self' : 'std');
        var otherSide = (side == 'd' ? 'r' : 'd');
        var disabledComponents = this.getExternalParamValue('disabledComponents');

               // L'utilisateur a le droit d'annuler la réalisation
        return this.hasRight('undo-' + typeRight) &&
               // le composant de réalisation/annulation n'est pas désactivé
               !disabledComponents[side].doSide &&
               // Action d'annulation permise par l'état du contrat et du transport
               this._tabSides[side].action && this._tabSides[side].action.type == 'undo' &&
            // Pas de modification en attente de validation sur les 2 côtés
               !this.isSideUpdated(side) && !this.isSideUpdated(otherSide) &&
               // Pas d'action en cours
               !this.getSideAction(side);
    },

    /**
     * Indique si la livraison ou la récupération a changé
     *
     * @return bool
     */
    isSideUpdated : function(side)
    {
        return (this._documentObj.getEditMode() == 'update' &&
                this._getSideCfgs(this._tabOldSides[side]) != this._getSideCfgs(this._tabSides[side]));
    },

    /**
     * Récupérer l'url de la page d'estimation de transport avec les paramètres suivants:
     *    - type           : type de document ('DEVIS' ou 'CONTRAT');
     *    - zone           : la zone de transport;
     *    - client         : l'id du client;
     *    - encombrement   : l'encombrement;
     *    - agence         : le code agence;
     *    - num               : le numéro temporaire de la ligne de devis;
     *    - idtransport    : l'id de l'estimation transport si il existe;
     *    - duree           : la durée de location (en jours);
     *    - idmoma         : l'id de modèle de machine;
     *    - valeur         : l'id de ligne de devis ou de contrat suivant le type de document;
     *    - numtempgeneral : l'id temporaire général du devis ou du contrat.
     *
     * Sinon retourne une erreur :
     *    - 0  : pas de client renseigné;
     *    - 1  : pas de zone ou zone incorrecte;
     *    - 2  : modèle de machine non renseigné;
     *    - 3  : code transport non défini pour le modèle de machine sélectionné;
     *    - 4  : durée de location incorrecte ou non renseignée
     *
     * @return bool TRUE si aucune erreur sinon FALSE
     */
    getEstimationUrl : function()
    {
        if (!this.isEstimationPossible())
        {
            return false;
        }
        var url = this._tabCfgs.transportEstimationUrl + '?';

        var type;       // type de document ('DEVIS' ou 'CONTRAT')
        var agency;     // le code agence
        var duration;   // la durée de location (en jours)
        var model;      // l'id de modèle de machine
        var customerId; // l'id du client
        var zone;       // la zone de transport
        var size;       // l'encombrement
        var value;      // l'id de ligne de devis ou de contrat suivant le type de document
        var tempValGen; // l'id temporaire général du devis ou du contrat
        var tempValue;  // le numéro temporaire de la ligne de devis;
        var trspId;     // l'id de l'estimation transport si il existe;
        var estimateId; // Id du devis

        // si c'est une ligne de devis
        if (this._documentObj.getObjectType() == 'estimateLine')
        {
            var estimateObj = this._documentObj.getEstimateObject();
            type       = 'DEVIS';
            agency     = estimateObj.getAgencyId();
            duration   = this._documentObj.getDuration();
            model      = this._documentObj.getModelId();
            customerId = estimateObj.getCustomerId();
            zone       = estimateObj.getZone();
            size       = this._documentObj.getModelLoad();
            if (this._documentObj.getEditMode() == 'create')
            {
                tempValue  = this._documentObj.getId();
                tempValGen = estimateObj.getId();
            }
            else
            {
                value = this._documentObj.getId();
            }
            // si le
            if (estimateObj.getEditMode() == 'update')
            {
                estimateId = estimateObj.getId();
            }

            trspId     = this.getId();
        }
        // si c'est un contrat
        else if (this._documentObj.getObjectType() == 'contract')
        {
            type       = 'CONTRAT';
            agency     = this._documentObj.getAgencyId();
            duration   = this.getExternalParamValue('duration');
            model      = this._documentObj.getModelId();
            customerId = this._documentObj.getCustomerId();
            zone       = this._documentObj.getZone();
            size       = this._documentObj.getModelLoad();
            if (this._documentObj.getEditMode() == 'create')
            {
                tempValGen = this._documentObj.getId();
            }
            else
            {

                value = this._documentObj.getId();
            }
            trspId     = this.getId();
        }

        //passage des paramètre à l'url
        url += 'type=' + type + '&agence=' + agency + '&duree=' + duration;

        // Client sélectionné?
        if (!customerId || customerId == 0)
        {
            alert(LOC_View.getTranslation('transportNoCustomer'));
            return false;
        }
        url += '&client=' + customerId;

        // Zone présente et correcte?
        if (!zone || zone == 0 || zone > 6)
        {
            alert(LOC_View.getTranslation('transportNoZone'));
            return false;
        }
        url += '&zone=' + zone ;

        // modèle de machine sélectionné?
        if (!model || model == 0)
        {
            alert(LOC_View.getTranslation('transportNoModelId'));
            return false;
        }
        url += '&idmoma=' + model ;

        // encombrement correct et présent?
        if (!size || size == 0)
        {
            alert(LOC_View.getTranslation('transportNoMachineSize'));
            return false;
        }
        url += '&encombrement=' + size ;

        // passage de l'id d'estimation de transport si elle existe
        if (trspId && trspId != 0 && this.checkEstimation())
        {
            url += '&idtransport=' + trspId;
        }
        // si c'est un devis passage du numéro temporaire de ligne de devis
        if (type == 'DEVIS' && tempValue && tempValue != '')
        {
            url += '&num=' + tempValue;
        }
        // passage du numéro temporaire général
        if (tempValGen && tempValGen != '')
        {
            url += '&numtempgeneral=' + tempValGen;
        }
        // passage du numéro de contrat ou de ligne de devis
        if (value && value != 0)
        {
            url += '&valeur=' + value;
        }
        // passage du numéro de devis si celui-ci à déjà été enregistré
        if (estimateId && estimateId != 0)
        {
            url += '&estimateId=' + estimateId;
        }
        return url;
    },

    /**
     * Modifier l'estimation transport
     *
     * @param Object Données de l'estimation
     * @return bool
     */
    setEstimation : function(tab)
    {
        if (!this.isEstimationPossible())
        {
            return false;
        }

        this._zone = tab.zone;
        this._customerId = tab.customer;

        this._isEstimationChange = (this._id != tab.id || this._amount != tab.amount);
        var tabChanges = [];
        if (this._isEstimationChange)
        {
            tabChanges.push('estimation');
        }

        this._id = tab.id;
        this._baseAmount = tab.baseAmount;
        this._recoReductionPercent = tab.recoReductionPercent;
        this._maxiReductionPercent = tab.maxiReductionPercent;
        this._standardReductionAmount = tab.standardReductionAmount;
        this._specialReductionStatus = tab.specialReductionStatus;
        this._specialReductionPercent = tab.specialReductionPercent;
        this._specialReductionAmount = tab.specialReductionAmount;
        this._standardReductionPercent = tab.standardReductionPercent;
        this._reductionAmount = tab.reductionAmount;
        this._amount = tab.amount;

        this._maxiReductionAmount = LOC_Common.round(this._baseAmount * (this._maxiReductionPercent / 100), 2);
        this._reductionPercent = LOC_Common.round((this._reductionAmount / this._baseAmount) * 100, 2);


        // Quantité de machine (pour rétrocompatibilité sur devis)
        var amount = this._amount;
        if (this.getDocumentObject().getObjectType() == 'estimateLine')
        {
            amount *= this.getDocumentObject().getQuantity();
        }

        if (this._possibilities.delivery.amount && !this._tabSides['d'].isSelf &&
            !this._tabSides['d'].isAddMachine && this._tabSides['d'].amount < amount)
        {
            this._tabSides['d'].amount = amount;
            tabChanges.push('delivery');
        }
        if (this._possibilities.recovery.amount && !this._tabSides['r'].isSelf &&
            !this._tabSides['r'].isAddMachine && this._tabSides['r'].amount < amount)
        {
            this._tabSides['r'].amount = amount;
            tabChanges.push('recovery');
        }

        this.checkEstimation();

        if (tabChanges.length > 0 && this.onchange)
        {
            this.onchange(tabChanges);
        }
        return true;
    },

    /**
     * Récupérer les infos d'un côté du transport (bloc livraison ou récupération)
     *
     * @param string side Coté à mettre à jour
     * @param Object tab  Mises à jour
     * @return bool
     */
    setSideInfos : function(side, tab)
    {
        if (this.getSideAction(side))
        {
            return false;
        }
        var tabSide = this._tabSides[side];
        var tabChanges = [];

        for (var prop in tab)
        {
            var oldType = typeof tabSide[prop];
            if (oldType != 'undefined')
            {
                var newType = typeof tab[prop];
                if (newType != oldType || (newType == 'object' && tab[prop] ? tab[prop].id : tab[prop]) !=
                                          (oldType == 'object' && tabSide[prop] ? tabSide[prop].id : tabSide[prop]))
                {
                    tabChanges.push((side == 'r' ? 'recovery' : 'delivery') + '.' + prop);
                }

                tabSide[prop] = LOC_Common.clone(tab[prop]);
            }
        }

        if (tabChanges.length == 0)
        {
            return false;
        }

        this.checkEstimation();

        if (this.onchange)
        {
            this.onchange(tabChanges);
        }
        return true;
    },

    /**
     * Réinitialiser un côté du transport (bloc livraison ou récupération)
     *
     * @param string side Coté à réinitialiser
     * @return bool
     */
    resetSide : function(side)
    {
        if (!this.isSideUpdated(side) && !this.getSideAction(side))
        {
            return false;
        }
        this._tabSides[side] = LOC_Common.clone(this._tabOldSides[side]);
        this._tabAction = null;

        this.checkEstimation();

        if (this.onchange)
        {
            this.onchange([(side == 'r' ? 'recovery' : 'delivery')]);
        }
        return true;
    },

    /**
     * Vérifier le droit à un champ
     *
     * @return bool
     */
    hasRight : function(fieldName)
    {
        return (this._tabRights === null || this._tabRights[fieldName] ? true : false);
    },

    _getSideCfgs : function(tab)
    {
        return [tab.amount, (tab.isAddMachine ? '1' : '0'), (tab.isSelf ? '1' : '0'),
                (tab.addMachineLnkCtt ? tab.addMachineLnkCtt.id : ''),
                (tab.delegatedAgencyId ? tab.delegatedAgencyId : ''),
                (tab.transferToAgency ? tab.transferToAgency.id : ''),
                (tab.contract ? tab.contract.id : ''),
                (tab.isSynchroDisabled ? '1' : '0')].join('-');
    }
}


Location.Rent_Helper_TransportView = {

    _workflowImg : null,

    /**
     * Gestion de la popup de modification du transport
     *
     */
    _popupDspObj : {
        _pfx : '',
        _side : '',
        _oldRefreshTime : -1,
        _listCloneDelegated: null,
        _documentAgencyId: null,

        amount : {
            value : 0,
            error : false
        },
        addMachine : {
            isActive : false,
            value : null
        },
        self : {
            isActive : false,
            value : 'agency'
        },
        transfer : {
            isActive : false
        },
        delegate : {
            isActive : false,
            value : null,
            error : false
        },
        transferToAgency : {
            isActive : false,
            value : null,
            error : false
        },
        contracts : {
            tab : [],
            value : null,
            getContractInfos : function (contractId)
            {
                for (i = 0 ; i < this.tab.length; i++)
                {
                    if (this.tab[i].id == contractId)
                    {
                        return this.tab[i];
                    }
                }
                return null;
            },
            state : '',
            error : false,
            time : 0
        },
        synchroDisabled : {
            isActive : false
        },
        possibilities : null,
        tabMachineInfos: null,

        /**
         * Récupération des possibilités
         */
        getComponentsActivation : function()
        {
            var tab = {
                    amount : (this.possibilities.amount && this.hasRight('amount') ? true : false),
                    addMachine : (this.possibilities.addMachine && this.hasRight('addMachine') ? true : false),
                    self : {agency : (this.possibilities.edit && this.possibilities.self && this.hasRight('self') ? true : false),
                            site : (this.possibilities.edit && this.possibilities.self &&
                                    this.hasRight('self') && this.hasRight('transfer') &&
                                    !this.disabledComponents.sitesTransfer ? true : false)},
                    transfer : (this.possibilities.edit && this.hasRight('transfer') && !this.disabledComponents.sitesTransfer ? true : false),
                    delegate : (this.possibilities.edit && this.hasRight('delegate') ? true : false),
                    transferToAgency : (this.possibilities.edit && this.hasRight('transferToAgency') && !this.disabledComponents.transferToAgency ? true : false),
                    synchroDisabled : (this.possibilities.edit && this.hasRight('synchro') ? true : false)
            };
            if (this.addMachine.isActive)
            {
                tab.self.agency = tab.self.site = false;
            }
            if (this.self.isActive)
            {
                tab.addMachine = tab.transfer = tab.delegate = tab.synchroDisabled = false;
                if (this.self.value == 'agency')
                {
                    tab.amount = false;
                }
                if (this.self.value == 'site')
                {
                    tab.transferToAgency = false;
                    if (this.transferToAgency.isActive)
                    {
                        tab.self.agency = tab.transferToAgency = false;
                    }
                }
            }
            if (this.transfer.isActive)
            {
                tab.self.agency = tab.self.site = tab.transferToAgency = false;
                if (this.transferToAgency.isActive)
                {
                    tab.transferToAgency = false;
                }
            }
            if (this.delegate.isActive)
            {
                tab.self.agency = tab.self.site = false;
            }
            if (this.transferToAgency.isActive)
            {
                tab.self.site = tab.transfer = false;
            }
            if (this.contracts.tab.length == 0)
            {
                tab.self.site = tab.transfer = false;
            }
            return tab;
        },

        /**
         * Mise à jour de l'affichage de la popup
         *
         */
        display : function()
        {
            var mySelf = this;
            var tabCmpts = this.getComponentsActivation();
            var pfx = this._pfx;

            var contractForSiteTransferInfos    =  this.contracts.getContractInfos(this.contracts.value);
            var contractForSiteTransferAgencyId = (contractForSiteTransferInfos ? contractForSiteTransferInfos.agency.id : null);

            $ge(pfx + 'modal').className = 'transportHlp_popup side_' + this._side;

            // - Montant
            var el = Location.currenciesManager.getCurrency(pfx + 'amount');
            el.setValue(this.amount.value);
            el.setDisabled(!tabCmpts.amount);
            el.getElement().className = (this.amount.error ? 'error' : '');

            // - Machine supplémentaire
            el = $ge(pfx + 'addMachine');
            el.checked = this.addMachine.isActive;
            el.disabled = !tabCmpts.addMachine;

            el = Location.searchBoxesManager.getSearchBox(pfx + 'addMachineContractId');
            if (el)
            {
                el.setDisabled(!this.addMachine.isActive || !tabCmpts.addMachine);
                $ge(pfx + 'addMachineClear').style.display = (el.isDisabled() ? 'none' : '');
                if (this.addMachine.isActive && this.addMachine.value)
                {
                    if (!el.getSelectedOption())
                    {
                        el.clear();
                        el.setElements([[this.addMachine.value.id, this.addMachine.value.code]], 1);
                    }
                    el.setValue(this.addMachine.value.id, false);

                    el = $ge(pfx + 'viewContractAddMachine');
                    el.style.display = '';
                    el.href = this.addMachine.value.url;
                }
                else
                {
                    el.clear();
                    $ge(pfx + 'viewContractAddMachine').style.display = 'none';
                }
            }

            // - Enlèvement sur place
            cbEl = $ge(pfx + 'self');
            cbEl.checked = this.self.isActive;
            cbEl.disabled = !tabCmpts.self.agency && !tabCmpts.self.site || (this.contracts.state == 'loading');

            el = $ge(pfx + 'selfAgency');
            el.checked = (this.self.isActive && this.self.value == 'agency');
            el.disabled = !cbEl.checked || !tabCmpts.self.agency;
            el = $ge(pfx + 'selfSite');
            el.checked = (this.self.isActive && this.self.value == 'site');
            el.disabled = !cbEl.checked || !tabCmpts.self.site || (this.contracts.state == 'loading');

            // - Transfert inter-chantiers
            el = $ge(pfx + 'transfer');
            el.checked = this.transfer.isActive;
            el.disabled = !tabCmpts.transfer || (this.contracts.state == 'loading');

            // - Agence déléguée
            cbEl = $ge(pfx + 'delegate');
            cbEl.checked = this.delegate.isActive &&                                            // délégation active ET
                            ((!this.transferToAgency.isActive ||                                // - pas de "transférée à"
                              this.transferToAgency.isActive && !this.transferToAgency.value ||
                              this.transferToAgency.value.id != this.delegate.value) &&         // - "transférée à " différent de l'agence de délégation
                             (!this.transfer.isActive ||                                        // - pas de transfert inter-chantiers
                              this.transfer.isActive && !this.contracts.value ||
                              contractForSiteTransferAgencyId != this.delegate.value));         // - agence du contrat de transfert diff. agence délégation
            cbEl.disabled = !tabCmpts.delegate || (this.contracts.state == 'loading');

            el = $ge(pfx + 'delegatedAgencyId');

            // -- remplissage de la liste des agences dispo pour délégation
            if (!this._listCloneDelegated)
            {
                this._listCloneDelegated = el.cloneNode(true);
            }
            while (el.options[0])
            {
                el.removeChild(el.options[0]);
            }
            for (var i = 0; i < this._listCloneDelegated.options.length; i++)
            {
                if (// agence différente de l'agence de transfert
                    this.transferToAgency.isActive && this.transferToAgency.value &&
                     this._listCloneDelegated.options[i].value != this.transferToAgency.value.id ||
                    // agence différente de l'agence du contrat de transfert inter-chantiers si on est côté récup
                    this.transfer.isActive && this._side == 'r' && this._listCloneDelegated.options[i].value != contractForSiteTransferAgencyId ||
                    // agence différente de l'agence du document si pas de "transférée à" et pas de transfert inter-chantiers ou transfert côté livraison
                    (!this.transferToAgency.isActive || !this.transferToAgency.value) &&
                        (!this.transfer.isActive || this.transfer.isActive && this._side == 'd') &&
                        this._listCloneDelegated.options[i].value != this._documentAgencyId)
                {
                    el.appendChild(this._listCloneDelegated.options[i].cloneNode(true));
                }
            }

            el.value = (cbEl.checked && this.delegate.value ? this.delegate.value : '');
            el.disabled = !cbEl.checked || cbEl.disabled;
            el.className = (this.delegate.error ? 'error' : '');

            // - Transfert de machine à l'agence
            cbEl = $ge(pfx + 'transferToAgency');
            if (cbEl)
            {
                cbEl.checked = this.transferToAgency.isActive;
                cbEl.disabled = !tabCmpts.transferToAgency;
            }

            el = $ge(pfx + 'transferToAgencyId');
            if (el)
            {
                el.value = (cbEl.checked && this.transferToAgency.value ? this.transferToAgency.value.id : '');
                el.disabled = !cbEl.checked || cbEl.disabled;
                el.className = (this.transferToAgency.error ? 'error' : '');
            }

            // Coche "ne pas synchroniser avec OTD"
            el = $ge(pfx + 'synchroDisabled');
            if (el)
            {
                el.checked = (!this.self.isActive && this.synchroDisabled.isActive);
                el.disabled = !tabCmpts.synchroDisabled;
            }

            // - Liste des contrats pour le transferts inter-chantiers ou la reprise sur chantier
            var tableEl = $ge(pfx + 'ctts');
            if (tableEl)
            {
                var isCttsActive = false;
                var tabClasses = [];
                if (this.transfer.isActive)
                {
                    tabClasses.push('txt_std');
                    isCttsActive = tabCmpts.transfer;
                }
                else if (this.self.isActive && this.self.value == 'site')
                {
                    tabClasses.push('txt_self');
                    isCttsActive = tabCmpts.self.site;
                }
                if (!isCttsActive)
                {
                    tabClasses.push('disabled');
                }
                if (this.contracts.error)
                {
                    tabClasses.push('error');
                }
                // Affichage du message de visibilité pour les transferts inter-chantiers
                // - côté récupération
                // - machine dans la même agence physique que le contrat
                // - machine visible sur une autre agence
                if (this._side == 'r' && mySelf.tabMachineInfos.isContractSameAgency)
                {
                    var visibleOnAgencyId;
                    if (this.transferToAgency.isActive)
                    {
                        visibleOnAgencyId = (this.transferToAgency.value ? this.transferToAgency.value.id : null);
                    }
                    else if (mySelf.tabMachineInfos.visibilityMode == MACHINE_VISIBILITYMODE_MACHINE)
                    {
                        visibleOnAgencyId = mySelf.tabMachineInfos.visibleOnAgencyId;
                    }
                    if (visibleOnAgencyId)
                    {
                        var msgParkNumber         = $ge(pfx + 'msgParkNumber');
                        var msgVisibleOnAgencyId1 = $ge(pfx + 'msgVisibleOnAgencyId1');
                        var msgVisibleOnAgencyId2 = $ge(pfx + 'msgVisibleOnAgencyId2');

                        msgParkNumber.innerHTML         = mySelf.tabMachineInfos.parkNumber;
                        msgVisibleOnAgencyId1.innerHTML = visibleOnAgencyId;
                        msgVisibleOnAgencyId2.innerHTML = visibleOnAgencyId;

                        tabClasses.push('msgVisibility');
                    }
                }

                tableEl.className = 'basic' + (!isCttsActive ? ' disabled' : '') +
                                    (this.contracts.state != '' ? ' msg ' + this.contracts.state : '');

                var tBodyEl = tableEl.tBodies[0];
                if (this.contracts.time >= this._oldRefreshTime)
                {
                    // Vidage du tableau
                    while (tBodyEl.childNodes.length > 0 )
                    {
                        tBodyEl.removeChild(tBodyEl.firstChild);
                    }

                    for (var i = 0; i < this.contracts.tab.length; i++)
                    {
                        var rEl, cEl, el;
                        var tabInfos = this.contracts.tab[i];
                        rEl = tBodyEl.insertRow(-1);
                        // si dans le tableau erreur -> erreur ouverture de compte, alors ajouter "error" à la classe
                        rEl.className = (i % 2 == 0 ? 'even' : 'odd');

                        cEl = rEl.insertCell(-1);
                        cEl.className = 'check';
                        el = window.document.createElement('INPUT');
                        el.type     = 'radio';
                        el.disabled = true;
                        el.value    = tabInfos.id;
                        el.checked  = false;
                        if (tabInfos.isFatalError)
                        {
                            rEl.className += ' fatalError';
                        }
                        else
                        {
                            el.onclick  = function()
                            {
                                if (!this.disabled)
                                {
                                    mySelf.contracts.error = false;
                                    mySelf.contracts.value = this.value;
                                    mySelf.display();
                                }
                            }
                        }
                        cEl.appendChild(el);

                        // colonne infos
                        cEl = rEl.insertCell(-1);
                        cEl.className = 'infosCpl';
                        for (var j = 0; j < tabInfos.tabInfosCpl.length; j++)
                        {
                            el = window.document.createElement('DIV');
                            el.className = 'infosCplIcon ' + tabInfos.tabInfosCpl[j];
                            cEl.appendChild(el);
                        }

                        cEl = rEl.insertCell(-1);
                        cEl.className = 'code';
                        el = window.document.createElement('A');
                        el.target = '_blank';
                        el.href   = tabInfos.url;
                        el.innerHTML = tabInfos.code;
                        cEl.appendChild(el);

                        cEl = rEl.insertCell(-1);
                        cEl.className = 'site';
                        cEl.innerHTML = tabInfos.site;

                        cEl = rEl.insertCell(-1);
                        cEl.className = 'parkNumber';
                        cEl.innerHTML = tabInfos.parkNumber;

                        cEl = rEl.insertCell(-1);
                        cEl.className = 'date';
                        var dateToPrint = LOC_View.getDateFormat(tabInfos.limitDate);
                        if (tabInfos.limitHour)
                        {
                            dateToPrint += ' ' + tabInfos.limitHour;
                        }
                        cEl.innerHTML = dateToPrint;

                        cEl = rEl.insertCell(-1);
                        cEl.className = 'errors';
                        for (var j = 0; j < tabInfos.tabErrors.length; j++)
                        {
                            el = window.document.createElement('DIV');
                            el.className = 'errorIcon ' + tabInfos.tabErrors[j];
                            cEl.appendChild(el);
                        }
                    }
                }

                var tabElts = tBodyEl.getElementsByTagName('INPUT');
                for (var i = 0; i < tabElts.length; i++)
                {
                    tabElts[i].checked = (// transfert ou reprise sur chantier actif
                                          ((this.transfer.isActive || (this.self.isActive && this.self.value == 'site')) &&
                                           // contrat sélectionné
                                           this.contracts.value == tabElts[i].value &&
                                           // le contrat n'est pas en erreur
                                           !this.contracts.tab[i].isFatalError) ? true : false);
                    tabElts[i].disabled = (// liste des contrats en cours de chargement
                                           this.contracts.state == 'loading' ||
                                           // erreur fatale sur le contrat
                                           this.contracts.tab[i].isFatalError ||
                                           // liste des contrats non active
                                           !isCttsActive ||
                                           // transfert à moitié réalisé et pas le contrat concerné par le transfert
                                           (this.possibilities.contracts && tabElts[i].value != this.possibilities.contracts));
                }

                $ge(pfx + 'cttsRow').className = tabClasses.join(' ');

                // Bouton de réactualisation de la liste des contrats
                el = $ge(pfx + 'refreshBtn');
                el.className = 'locCtrlButton' + (this.contracts.state == 'loading' ? ' disabled' : '');
            }
            // Bouton de validation
            el = $ge(pfx + 'validBtn');
            el.className = 'locCtrlButton' + (this.contracts.state == 'loading' ? ' disabled' : '');


            this._oldRefreshTime = new Date().getTime();
        },

        /**
         * Initialisation des événements
         *
         */
        initEvents : function(pfx, obj)
        {
            var mySelf = this;

            // - Boutons de la popup
            var el = Location.currenciesManager.getCurrency(pfx + 'amount');
            el.onchange = function()
            {
                if (!this.isDisabled())
                {
                    mySelf.amount.error = false;
                    mySelf.amount.value = this.getValue();
                    mySelf.display();
                    return true;
                }
                return false;
            }
            el = $ge(pfx + 'addMachine');
            el.onchange = el.onclick = function()
            {
                if (!this.disabled)
                {
                    mySelf.addMachine.isActive = this.checked;
                    if (mySelf.possibilities.amount)
                    {
                        mySelf.amount.value = (this.checked ? obj._tabCfgs.addMachinePrice : obj._amount);
                    }
                    mySelf.display();
                    return true;
                }
                return false;
            }
            el = Location.searchBoxesManager.getSearchBox(pfx + 'addMachineContractId');
            if (el)
            {
                el.onchange = function()
                {
                    if (!this.isDisabled())
                    {
                        var opt = this.getSelectedOption();
                        if (opt)
                        {
                            mySelf.addMachine.value = {
                                id : opt.getAttribute('value'),
                                code : opt.innerHTML,
                                url : opt.getAttribute('data-url')
                            };
                            mySelf.display();
                            return true;
                        }
                    }
                    return false;
                }

                $ge(pfx + 'addMachineClear').onclick = function()
                {
                    if (this.style.display != 'none')
                    {
                        mySelf.addMachine.value = null;
                        mySelf.display();
                    }
                    return true;
                }
            }
            el = $ge(pfx + 'self');
            el.onchange = el.onclick = function()
            {
                if (!this.disabled)
                {
                    mySelf.self.isActive = this.checked;
                    if (mySelf.possibilities.amount)
                    {
                        mySelf.amount.value = (this.checked ? 0 : obj._amount);
                    }
                    // Mise à jour des contrats disponibles pour transfert
                    if (mySelf.contracts.state != 'loading')
                    {
                        mySelf.refreshContracts(obj);
                    }
                    mySelf.display();
                    return true;
                }
                return false;
            }
            el = $ge(pfx + 'selfAgency');
            el.onclick = function()
            {
                if (!this.disabled)
                {
                    mySelf.self.value = (this.checked ? 'agency' : 'site');
                    mySelf.amount.value = (this.checked ? 0 : obj._amount);
                    mySelf.display();
                    return true;
                }
                return false;
            }
            el = $ge(pfx + 'selfSite');
            if (el)
            {
                el.onclick = function()
                {
                    if (!this.disabled)
                    {
                        mySelf.self.value = (this.checked ? 'site' : 'agency');
                        mySelf.display();
                        return true;
                    }
                    return false;
                }
            }
            el = $ge(pfx + 'transfer');
            if (el)
            {
                el.onchange = el.onclick = function()
                {
                    if (!this.disabled)
                    {
                        mySelf.transfer.isActive = this.checked;
                        // Mise à jour des contrats disponibles pour transfert
                        if (mySelf.contracts.state != 'loading')
                        {
                            mySelf.refreshContracts(obj);
                        }
                        mySelf.display();
                        return true;
                    }
                    return false;
                }
            }
            el = $ge(pfx + 'delegate');
            el.onchange = el.onclick = function()
            {
                if (!this.disabled)
                {
                    mySelf.delegate.isActive = this.checked;
                    // si "transférée à" est actif et que l'agence de transfert est la même que l'agence de délégation,
                    // ou qu'un transfert inter-chantiers est en cours et que l'ag. du contrat de transf. est la même que l'ag. délégation
                    // on vide l'agence de délégation quand on recoche la délégation
                    var contractForSiteTransferInfos = mySelf.contracts.getContractInfos(mySelf.contracts.value);
                    if (mySelf.delegate.isActive &&
                        ((mySelf.transferToAgency.isActive && mySelf.transferToAgency.value &&
                          mySelf.transferToAgency.value.id == mySelf.delegate.value) ||
                         (mySelf.transfer.isActive && mySelf.contracts.value && contractForSiteTransferInfos &&
                          contractForSiteTransferInfos.agency.id == mySelf.delegate.value)))
                    {
                        mySelf.delegate.value = null;
                    }
                    mySelf.display();
                    return true;
                }
                return false;
            }
            el = $ge(pfx + 'delegatedAgencyId');
            el.onchange = function()
            {
                if (!this.disabled)
                {
                    mySelf.delegate.value = (this.value != '' ? this.value : null);
                    mySelf.delegate.error = false;
                    mySelf.display();
                    return true;
                }
                return false;
            }
            el = $ge(pfx + 'transferToAgency');
            el.onchange = el.onclick = function()
            {
                if (!this.disabled)
                {
                    mySelf.transferToAgency.isActive = this.checked;
                    // Mise à jour des contrats disponibles pour transfert
                    if (mySelf.contracts.state != 'loading')
                    {
                        mySelf.refreshContracts(obj);
                    }
                    mySelf.display();
                    return true;
                }
                return false;
            }
            el = $ge(pfx + 'transferToAgencyId');
            el.onchange = function()
            {
                if (!this.disabled)
                {
                    mySelf.transferToAgency.value = (this.value != '' ? {id : this.value, label : this.options[this.selectedIndex].innerHTML} : null);
                    mySelf.transferToAgency.error = false;
                    // Mise à jour des contrats disponibles pour transfert
                    if (mySelf.contracts.state != 'loading')
                    {
                        mySelf.refreshContracts(obj);
                    }

                    mySelf.display();
                    return true;
                }
                return false;
            }
            el = $ge(pfx + 'synchroDisabled');
            if (el)
            {
                el.onchange = el.onclick = function()
                {
                    if (!this.disabled)
                    {
                        mySelf.synchroDisabled.isActive = this.checked;
                        mySelf.display();
                        return true;
                    }
                    return false;
                }
            }
            if ($ge(pfx + 'refreshBtn'))
            {
                $ge(pfx + 'refreshBtn').onclick = function()
                {
                    if (mySelf.contracts.state != 'loading')
                    {
                        mySelf.refreshContracts(obj);
                        return true;
                    }
                    return false;
                }
            }
            $ge(pfx + 'validBtn').onclick = function()
            {
                if (mySelf.contracts.state != 'loading')
                {
                    mySelf.valid(obj);
                    return true;
                }
                return false;
            }
        },

        /**
         * Rafraichissement de la liste des contrats
         *
         */
        refreshContracts : function(obj)
        {
            var mySelf = this;

            this.contracts.state = 'loading';
            this.display();

            var tabInfosSup = {
                transferToAgency : this.transferToAgency,
                self             : this.self
            };

            obj.getContractsForSiteTransfer(this._side, tabInfosSup, function(tab) {
                if (tab)
                {
                    mySelf.contracts.tab = tab;
                    mySelf.contracts.state = (tab.length == 0 ? 'noResult' : '');
                    mySelf.contracts.time = new Date().getTime();
                }
                else
                {
                    mySelf.contracts.tab = [];
                    mySelf.contracts.state = 'error';
                }
                mySelf.display();
            });
        },

        /**
         * Affichage de la popup
         *
         * @return bool
         */
        show : function(pfx, obj, side)
        {
            this._pfx = pfx;
            this._side = side;
            this._oldRefreshTime = -1;
            // - Récupération de l'agence du document (devis ou contrat)
            var documentObj = obj.getDocumentObject();
            if (documentObj.getObjectType() == 'estimateLine')
            {
                documentObj = documentObj.getEstimateObject();
            }
            this._documentAgencyId = documentObj.getAgencyId();

            // Récupération des données
            var tabSide = obj.getSideInfos(side);

            if (!obj.isSideEditable(side))
            {
                return false;
            }
            var sideTariff = (side == 'd' ? 'delivery' : 'recovery');
            this.possibilities = {
                edit      :       tabSide.possibilities.edit,
                amount    :       obj._possibilities[sideTariff].amount,
                addMachine:       obj._possibilities[sideTariff].addMachine,
                transferToAgency: tabSide.possibilities.transferToAgency,
                // possibilités de choix de contrats :
                // - si null, on peut choisir tous les contrats
                // - sinon uniquement celui choisi
                contracts :       (tabSide.contract && tabSide.isLoadingDone ? tabSide.contract.id : null),
                // possibilité de suppression de reprise sur chantier :
                // - si le contrat concerné par la reprise n'est pas d'une autre agence avec un montant non nul
                self      :       !(tabSide.isSelf && tabSide.contract &&
                                    tabSide.contract.agency.id != this._documentAgencyId && tabSide.contract.amount != 0)
            };
            this.hasRight = function (fn) { return obj.hasRight(fn); };
            var disabledComponents = obj.getExternalParamValue('disabledComponents');
            this.disabledComponents = (disabledComponents[side] ? disabledComponents[side] : {});

            this.tabMachineInfos = obj.getExternalParamValue('tabMachineInfos');

            // - Montant
            this.amount.value = tabSide.amount;
            this.amount.error = false;
            // - Machine supp
            this.addMachine.isActive = tabSide.isAddMachine;
            this.addMachine.value = (tabSide.addMachineLnkCtt ? LOC_Common.clone(tabSide.addMachineLnkCtt) : null);
            // - Enlèvement sur place
            this.self.isActive = tabSide.isSelf;
            this.self.value = (tabSide.isSelf && tabSide.contract ? 'site' : 'agency');
            // - Transfert inter-chantiers
            this.transfer.isActive = (!tabSide.isSelf && tabSide.contract ? true : false);
            this.contracts.value = (tabSide.contract ? tabSide.contract.id : null);
            this.contracts.tab = [];
            this.contracts.error = false;
            this.contracts.time = 0;
            // - Agence déléguée
            this.delegate.isActive = (tabSide.delegatedAgencyId ? true : false);
            this.delegate.value = (tabSide.delegatedAgencyId ? LOC_Common.clone(tabSide.delegatedAgencyId) : null);
            this.delegate.error = false;
            // - Transfert de machine à une agence
            this.transferToAgency.isActive = (tabSide.transferToAgency ? true : false);
            this.transferToAgency.value = (tabSide.transferToAgency ? LOC_Common.clone(tabSide.transferToAgency) : null);
            this.transferToAgency.error = false;
            // - Coche "ne pas synchroniser avec OTD"
            this.synchroDisabled.isActive = (tabSide.isSynchroDisabled ? true : false);

            // Mise à jour de l'affichage
            this.display();
            this.refreshContracts(obj);

            // Affichage de la popup
            Location.modalWindowManager.show(pfx + 'modal', {contentType: 3, width: 625, height: 'auto'});

            return true;
        },

        /**
         * Modification de la popup
         *
         */
        valid : function(obj)
        {
            var tabSideInfo = obj.getSideInfos(this._side);
            var sideTariff = (this._side == 'd' ? 'delivery' : 'recovery');
            // Erreurs
            var errorsCount = 0;
            if (obj._possibilities[sideTariff].amount &&
                ((this.addMachine.isActive && this.amount.value < obj.getAddMachineMinPrice()) ||
                 (this.self.isActive && this.self.value == 'agency' && this.amount.value > 0) ||
                 (!this.addMachine.isActive && !this.self.isActive && this.amount.value < obj._amount)))
            {
                this.amount.error = true;
                errorsCount++;
            }
            // si reprise sur chantier ou transfert inter-chantiers
            // - et pas de contrat sélectionné ou le contrat sélectionné est en erreur
            if (((this.self.isActive && this.self.value == 'site') || this.transfer.isActive) &&
                (!this.contracts.value || this.contracts.getContractInfos(this.contracts.value).isFatalError))
            {
                this.contracts.error = true;
                errorsCount++;
            }
            // si pas d'agence de délégation choisie ou l'agence de délégation est la même que l'agence de transfert
            if (this.delegate.isActive && !this.delegate.value)
            {
                this.delegate.error = true;
                errorsCount++;
            }
            if (this.transferToAgency.isActive && !this.transferToAgency.value)
            {
                this.transferToAgency.error = true;
                errorsCount++;
            }
            if (errorsCount > 0)
            {
                this.display();
                return false;
            }
            var tabUpdates = {
                agency : obj.getSideInfos('d').agency,
                amount : this.amount.value,
                isAddMachine : this.addMachine.isActive,
                addMachineLnkCtt : this.addMachine.value,
                isSelf : this.self.isActive,
                contract : null,
                delegatedAgencyId : (this.delegate.isActive && this.delegate.value ? this.delegate.value : null),
                transferToAgency : (this.transferToAgency.isActive && this.transferToAgency.value ? this.transferToAgency.value : null),
                isSynchroDisabled : (!this.self.isActive && this.synchroDisabled.isActive ? 1 : 0)
            };

            // Agence d'arrivée de la machine
            if (this._side == 'r' && !tabUpdates.contract)
            {
                // Option "transférée à"
                if (this.transferToAgency.isActive && this.transferToAgency.value)
                {
                    tabUpdates.agency = this.transferToAgency.value;
                }
            }

            if (this.transfer.isActive || (this.self.isActive && this.self.value == 'site'))
            {
                var contractInfos = this.contracts.getContractInfos(this.contracts.value);
                if (contractInfos)
                {
                    tabUpdates.contract = contractInfos;
                    if (this._side == 'r')
                    {
                        tabUpdates.agency   = contractInfos.agency;
                    }
                }
            }

            // Agence déléguée
            // - vidée si l'agence du transport est la même que l'agence déléguée
            if (tabUpdates.agency.id == tabUpdates.delegatedAgencyId)
            {
                tabUpdates.delegatedAgencyId = null;
            }

            obj.setSideInfos(this._side, tabUpdates);

            // Fermeture de la popup
            Location.modalWindowManager.hide(this._pfx + 'popup');
        }
    },

    /**
     * Affichage du workflow
     *
     */
    _displayWorkflow : function(pfx, obj)
    {
        var mySelf = this;

        // Au premier affichage, on charge les images
        if (!this._workflowImg)
        {
            this._workflowImg = new Image();
            this._workflowImg.src = '../web/img/rent/helper/transport/icons.png';
            this._workflowImg.onload = function() { mySelf._displayWorkflow(pfx, obj); }
            return;
        }
        if (!this._workflowImg.complete)
        {
            return;
        }

        // Récupération des informations sur les transports de livraison et récupération
        var tabSides = {
                'd' : {infos : obj.getSideInfos('d'),
                       isUpdated : obj.isSideUpdated('d'),
                       action : obj.getSideAction('d')},
                'r' : {infos : obj.getSideInfos('r'),
                       isUpdated : obj.isSideUpdated('r'),
                       action : obj.getSideAction('r')}
        };
        // - Récupération de l'agence du document (devis ou contrat)
        var documentObj = obj.getDocumentObject();
        if (documentObj.getObjectType() == 'estimateLine')
        {
            documentObj = documentObj.getEstimateObject();
        }
        var documentAgencyId = documentObj.getAgencyId();

        // Affichage
        var canvasEl = $ge(pfx + 'workflow');
        var ctx = canvasEl.getContext('2d');

        // On vide le canvas
        ctx.clearRect(0, 0, canvasEl.width, canvasEl.height);

        var padding = 25;
        var rect = {x : padding, y : 36, w : canvasEl.width - (padding * 2), h : 30};
        var arrows = {};

        var tabCases = ['d', 'r'];
        for (var i = 0; i < tabCases.length; i++)
        {
            var c = tabCases[i];

            arrows[c] = {lw : 3, lc : '#6a6a6a', rw : 3, rc : '#6a6a6a', err : false};

            if (!tabSides[c].isUpdated)
            {
                arrows[c].err = (tabSides[c].infos.tabErrors.length > 0);
                if (!tabSides[c].infos.action || arrows[c].err)
                {
                    arrows[c].lw = arrows[c].rw = 2;
                    arrows[c].lc = arrows[c].rc = '#dadada';
                }
            }
            if (tabSides[c].infos.isLoadingDone)
            {
                arrows[c].lw = 4;
                arrows[c].lc = '#99C900';
            }
            if (tabSides[c].infos.isUnloadingDone)
            {
                arrows[c].rw = 4;
                arrows[c].rc = '#99C900';
                arrows[c].err = false;
            }
        }


        var arrowW = (rect.w - 54) / 2;
        var rArrowX = rect.x + rect.w - arrowW;

        var drawUndoableCross = function(x, y, s)
        {
            ctx.lineWidth = 4;
            ctx.strokeStyle = '#c60000';

            ctx.beginPath();
            ctx.moveTo(x - s, y + s);
            ctx.lineTo(x + s, y - s);
            ctx.stroke();
            ctx.beginPath();
            ctx.moveTo(x - s, y - s);
            ctx.lineTo(x + s, y + s);
            ctx.stroke();
        }


        ctx.shadowColor = '#aaa';
        ctx.shadowBlur = 0.5;
        ctx.shadowOffsetX = ctx.shadowOffsetY = 1;

        // Annulation de l'action de réalisation et d'annulation du transport
        if (tabSides['d'].action)
        {
            ctx.beginPath();
            ctx.rect(rect.x - 19.5, rect.y + 1.5, arrowW + 20, rect.h + 30);
            ctx.lineWidth = 1;
            ctx.strokeStyle = '#b3d0e1';
            ctx.stroke();
            ctx.fillStyle = '#dfeef8';
            ctx.fill();
            // Petit crayon
            ctx.drawImage(this._workflowImg, 20, 30, 13, 13, rect.x - 16.5, rect.y + rect.h + 16, 13, 13);
        }
        if (tabSides['r'].action)
        {
            ctx.beginPath();
            ctx.rect(rArrowX + 0.5, rect.y + 1.5, arrowW + 20, rect.h + 30);
            ctx.lineWidth = 1;
            ctx.strokeStyle = '#b3d0e1';
            ctx.stroke();
            ctx.fillStyle = '#dfeef8';
            ctx.fill();
            // Petit crayon
            ctx.drawImage(this._workflowImg, 20, 30, 13, 13, rArrowX + 3.5, rect.y + rect.h + 16, 13, 13);
        }

        ctx.shadowBlur = 2;

        ctx.lineJoin = 'round';
        ctx.lineCap = 'butt';

        // Flèche de gauche
        ctx.beginPath();
        ctx.lineWidth = arrows['d'].lw;
        ctx.strokeStyle = arrows['d'].lc;
        ctx.moveTo(rect.x, rect.y);
        ctx.lineTo(rect.x, rect.y + rect.h);

        if (arrows['d'].lc == arrows['d'].rc)
        {
            // - ligne entière
            ctx.lineTo(rect.x + arrowW - 8, rect.y + rect.h);
            ctx.stroke();
        }
        else
        {
            // - première moitié
            ctx.lineTo(rect.x + (arrowW / 4), rect.y + rect.h);
            ctx.stroke();

            // - deuxième moitié
            ctx.beginPath();
            ctx.lineWidth = arrows['d'].rw;
            ctx.strokeStyle = arrows['d'].rc;
            ctx.moveTo(rect.x + (arrowW / 4), rect.y + rect.h);
            ctx.lineTo(rect.x + arrowW - 8, rect.y + rect.h);
            ctx.stroke();

            // - petit cercle
            ctx.beginPath();
            ctx.lineWidth = 2;
            ctx.fillStyle = 'white';
            ctx.strokeStyle = arrows['d'].lc;
            ctx.arc(rect.x + (arrowW / 4), rect.y + rect.h, 3, 0, Math.PI * 2, false);
            ctx.fill();
            ctx.stroke();
        }
        // - bout de la flèche
        ctx.beginPath();
        ctx.fillStyle = arrows['d'].rc;
        ctx.moveTo(rect.x + arrowW - 8, rect.y + rect.h - 9);
        ctx.lineTo(rect.x + arrowW - 8, rect.y + rect.h + 9);
        ctx.lineTo(rect.x + arrowW, rect.y + rect.h);
        ctx.fill();

        if (arrows['d'].err)
        {
            drawUndoableCross(rect.x + (arrowW / 2), rect.y + rect.h, 8);
        }


        // Flèche de droite
        // - bout de la flèche
        ctx.beginPath();
        ctx.moveTo(rArrowX + arrowW - 9, rect.y + 8);
        ctx.lineTo(rArrowX + arrowW + 9, rect.y + 8);
        ctx.lineTo(rArrowX + arrowW, rect.y);
        ctx.fillStyle = arrows['r'].rc;
        ctx.fill();

        ctx.beginPath();
        ctx.strokeStyle = arrows['r'].lc;
        ctx.lineWidth = arrows['r'].lw;
        ctx.moveTo(rArrowX, rect.y + rect.h);

        if (arrows['r'].lc == arrows['r'].rc)
        {
            // - ligne entière
            ctx.lineTo(rArrowX + arrowW, rect.y + rect.h);
            ctx.lineTo(rArrowX + arrowW, rect.y + 8);
            ctx.stroke();
        }
        else
        {
            // - première moitié
            ctx.lineTo(rArrowX + (3 * arrowW / 4), rect.y + rect.h);
            ctx.stroke();
            // - deuxième moitié
            ctx.beginPath();
            ctx.strokeStyle = arrows['r'].rc;
            ctx.lineWidth = arrows['r'].rw;
            ctx.moveTo(rArrowX + (3 * arrowW / 4), rect.y + rect.h);
            ctx.lineTo(rArrowX + arrowW, rect.y + rect.h);
            ctx.lineTo(rArrowX + arrowW, rect.y + 8);
            ctx.stroke();

            // - petit cercle
            ctx.beginPath();
            ctx.lineWidth = 2;
            ctx.fillStyle = 'white';
            ctx.strokeStyle = arrows['r'].lc;
            ctx.arc(rArrowX + (3 * arrowW / 4), rect.y + rect.h, 3, 0, Math.PI * 2, false);
            ctx.fill();
            ctx.stroke();
        }

        if (arrows['r'].err)
        {
            drawUndoableCross(rect.x + rect.w - (arrowW / 2), rect.y + rect.h, 8);
        }


        // Cercles
        // - sur le point de départ
        ctx.beginPath();
        ctx.arc(rect.x, rect.y - 10 - 5, 13, 0, Math.PI * 2, false);
        ctx.strokeStyle = '#99C900';
        ctx.lineWidth = 2;
        ctx.stroke();
        // - sur le point courant
        ctx.beginPath();
        ctx.arc(rect.x + (rect.w / 2), rect.y + rect.h, 24, 0, Math.PI * 2, false);
        ctx.strokeStyle = arrows['d'].rc;
        ctx.lineWidth = 3;
        ctx.stroke();
        // - sur le point d'arrivée
        ctx.beginPath();
        ctx.arc(rect.x + rect.w, rect.y - 10 - 5, 13, 0, Math.PI * 2, false);
        ctx.strokeStyle = arrows['r'].rc;
        ctx.lineWidth = 2;
        ctx.stroke();


        // Affichage des images
        ctx.shadowBlur = ctx.shadowOffsetX = ctx.shadowOffsetY = 0;

        // Point de départ
        ctx.drawImage(this._workflowImg, 0, (tabSides['d'].infos.contract ? 20 : 0), 20, 20,
                      rect.x - 10, rect.y - 20 - 5, 20, 20);
        // Type de transport de livraison
        ctx.drawImage(this._workflowImg, 54, (tabSides['d'].infos.isSelf ? 0 : 26), 26, 26,
                      rect.x + (arrowW / 2) - 13, rect.y - 25, 26, 26);
        // Chantier en cours
        var pos = rect.x + (rect.w / 2);
        ctx.drawImage(this._workflowImg, 20, 0, 32, 24, pos - 16, rect.y + rect.h - 12, 32, 24);
        // Point d'arrivée
        ctx.drawImage(this._workflowImg, 0, (tabSides['r'].infos.contract ? 20 : 0), 20, 20,
                      rect.x + rect.w - 10, rect.y - 20 - 5, 20, 20);
        // Type de transport de récupération
        ctx.drawImage(this._workflowImg, 54, (tabSides['r'].infos.isSelf ? 0 : 26), 26, 26,
                      rect.x + rect.w - (arrowW / 2) - 13, rect.y - 25, 26, 26);

        // Agences responsables du transport
        ctx.font = 'bold 9px Tahoma';
        ctx.fillStyle = '#555';
        // - agence déléguée si elle existe
        // - sinon si l'agence du transport est différente de l'agence du contrat
        if (!tabSides['d'].infos.isSelf)
        {
            var agcId = (tabSides['d'].infos.delegatedAgencyId ? tabSides['d'].infos.delegatedAgencyId : tabSides['d'].infos.agency.id);
            if (agcId != documentAgencyId)
            {
                ctx.fillText(agcId, rect.x + (arrowW / 2) - ctx.measureText(agcId).width / 2, rect.y - 25);
            }
        }
        if (!tabSides['r'].infos.isSelf)
        {
            var agcId = (tabSides['r'].infos.delegatedAgencyId ? tabSides['r'].infos.delegatedAgencyId : tabSides['r'].infos.agency.id);
            if (agcId != documentAgencyId)
            {
                ctx.fillText(agcId, rect.x + rect.w - (arrowW / 2) - ctx.measureText(agcId).width / 2, rect.y - 25);
            }
        }
    },


    /**
     * Mise à jour de l'affichage du contenu de l'onglet
     *
     */
    display : function(pfx, obj, tabErrors)
    {
        var mySelf = this;
        var el;
        if (obj.isEstimationActived())
        {
            el = Location.currenciesManager.getCurrency(pfx + 'BaseAmount');
            el.setValue(obj.getBaseAmount(), false);

            el = Location.spinControlsManager.getSpinControl(pfx + 'StdReduc');
            el.setValue(obj.getStandardReductionPercent(), false);
            el.disable();

            el = Location.currenciesManager.getCurrency(pfx + 'StdReducAmount');
            el.setValue(obj.getStandardReductionAmount(), false);

            $ge(pfx + 'Reco').innerHTML = obj.getRecoReductionPercent();
            $ge(pfx + 'Maxi').innerHTML = obj.getMaxiReductionPercent();

            el = Location.currenciesManager.getCurrency(pfx + 'MaxiAmount');
            el.setValue(obj.getMaxiReductionAmount(), false);

            el = Location.currenciesManager.getCurrency(pfx + 'SpcReducAmount');
            el.setValue(obj.getSpecialReductionAmount(), false);

            el = Location.spinControlsManager.getSpinControl(pfx + 'SpcReduc');
            el.setValue(obj.getSpecialReductionPercent(), false);
            el.disable();

            el = Location.currenciesManager.getCurrency(pfx + 'ReducAmount');
            el.setValue(obj.getReductionAmount(), false);

            el = Location.spinControlsManager.getSpinControl(pfx + 'Reduc');
            el.setValue(obj.getReductionPercent(), false);
            el.disable();

            el = Location.currenciesManager.getCurrency(pfx + 'Amount');
            el.setValue(obj.getAmount(), false);
            $ge(pfx + 'ReducStatus').className = 'spcReducStatus ' + obj.getSpecialReductionStatus();

            // Bouton d'estimation
            $ge(pfx + 'Btn').style.visibility = (obj.isEstimationPossible() ? 'visible' : 'hidden');

            // Affichage du message de sauvegarde du contrat
            $ge(pfx + 'ValidateEstimation').style.display = (obj.isEstimationChange() ? '' : 'none');
        }

        // Eléments pour la préparation du transport
        var className = '';
        var disabledComponents = obj.getExternalParamValue('disabledComponents');

        var tabClasses = [];
        var tabCases = [{c: 'd', l: 'delivery'}, {c: 'r', l: 'recovery'}];
        for (var i = 0; i < tabCases.length; i++)
        {
            var c = tabCases[i].c;
            var l = tabCases[i].l;
            var isUpdated = obj.isSideUpdated(c);
            var action = obj.getSideAction(c);

            var tabSide = obj.getSideInfos(c);
            var sideDisabledComponents = disabledComponents[c];

            $ge(pfx + l + 'EditBtn').className = 'locCtrlButton' + (obj.isSideEditable(c) ? '' : ' disabled');

            // id du transport
            el = Location.currenciesManager.getCurrency(pfx + l + 'Amount');
            el.setValue(tabSide.amount);
            el.getSpanElement().style.color = (tabErrors[l + 'Amount'] ? 'red' : '');

            if (isUpdated)
            {
                tabClasses.push(c + '-updated');
            }
            if (tabSide.contract)
            {
                tabClasses.push(c + '-fromto-site');
                $ge(pfx + l + 'Site').href = tabSide.contract.url;
                $ge(pfx + l + 'Site').innerHTML = tabSide.contract.code;
                $ge(pfx +  l + 'SiteInfo').innerHTML = tabSide.contract.siteInfos;
            }
            else
            {
                var label = tabSide.agency.label;
                if (c == 'r' && tabSide.transferToAgency)
                {
                    label = tabSide.transferToAgency.label;
                }
                else if (c == 'd' && tabSide.transferFromAgency)
                {
                    label = tabSide.transferFromAgency.label;
                }
                $ge(pfx + l + 'Agency').innerHTML = label;
            }
            if (action)
            {
                tabClasses.push(c + '-btn-reset');
            }
            else if (!isUpdated && tabSide.action)
            {
                if (tabSide.action.type == 'undo')
                {
                    tabClasses.push(c + '-btn-undo');
                    var isDsp = obj.isSideUndoable(c);
                    $ge(pfx + l + 'UndoBtn').className = 'btn undo' + (isDsp ? '' : ' disabled') +
                                                         (!tabSide.isUnloadingDone ? ' partial' : '');
                }
                else
                {
                    tabClasses.push(c + '-btn-do');
                    var isDsp = obj.isSideDoable(c);
                    $ge(pfx + l + 'DoBtn').className = 'btn do' + (isDsp ? '' : ' disabled') +
                                                       (tabSide.isLoadingDone ? ' partial' : '');
                }
            }
            if (!isUpdated && tabSide.selfFormUrls)
            {
                tabClasses.push(c + '-btn-selfform');

                var tabElts = $ge(pfx + l + 'SelfFormBtn').getElementsByTagName('a');
                for (var j = 0; j < tabElts.length; j++)
                {
                    tabElts[j].href = tabSide.selfFormUrls[tabElts[j].className];
                }
            }
            if (!isUpdated && tabSide.tabMachineInventoryInfos)
            {
                tabClasses.push(c + '-btn-machineinventory');
            }

            for (var j = 0; j < tabSide.tabErrors.length; j++)
            {
                tabClasses.push(c + '-err-' + tabSide.tabErrors[j]);
            }
            if (tabSide.extraInfos)
            {
                tabClasses.push(c + '-infos');
                $ge(pfx + l + 'OrderInfo').innerHTML = tabSide.extraInfos;
            }
            if (tabSide.isSynchroDisabled)
            {
                tabClasses.push(c + '-no-sync');
            }
            if (tabSide.isAddMachine)
            {
                tabClasses.push(c + '-addmach');
                if (tabSide.addMachineLnkCtt)
                {
                    el = $ge(pfx + l + 'AddMachLnkCtt');
                    tabClasses.push(c + '-addmach-ctt');
                    el.href = tabSide.addMachineLnkCtt.url;
                    el.innerHTML = tabSide.addMachineLnkCtt.code;
                }
            }
        }

        $ge(pfx + 'main').className = tabClasses.join(' ');


        // Dessin du workflow
        this._displayWorkflow(pfx, obj);
    },

    initEvents : function(pfx, obj)
    {
        var mySelf = this;
        var disabledComponents = obj.getExternalParamValue('disabledComponents');

        // - Bouton d'ouverture d'estimation transport
        $ge(pfx + 'Btn').onclick = function()
        {
            if (!disabledComponents.estimation)
            {
                var url = obj.getEstimationUrl();
                if (url)
                {
                    // ouverture de la popup
                    Location.modalWindowManager.show(url, {'contentType': 1, 'isNoCloseBtn': true});
                }
            }
            return false;
        }

        // - Bouton d'ouverture de la modification de la livraison/récupération
        $ge(pfx + 'deliveryEditBtn').onclick = function(e)
        {
            mySelf._popupDspObj.show(pfx + 'popup_', obj, 'd');
            return false;
        }

        $ge(pfx + 'recoveryEditBtn').onclick = function(e)
        {
            mySelf._popupDspObj.show(pfx + 'popup_', obj, 'r');
            return false;
        }

        // - Bouton de réalisation transport
        $ge(pfx + 'deliveryDoBtn').onclick = function()
        {
            mySelf._showConfirmPopup(this, pfx, obj, 'd', 'do');
            return false;
        }
        $ge(pfx + 'recoveryDoBtn').onclick = function()
        {
            mySelf._showConfirmPopup(this, pfx, obj, 'r', 'do');
            return false;
        }
        $ge(pfx + 'deliveryUndoBtn').onclick = function()
        {
            mySelf._showConfirmPopup(this, pfx, obj, 'd', 'undo');
            return false;
        }
        $ge(pfx + 'recoveryUndoBtn').onclick = function()
        {
            mySelf._showConfirmPopup(this, pfx, obj, 'r', 'undo');
            return false;
        }
        $ge(pfx + 'deliveryResetBtn').onclick = $ge(pfx + 'deliveryGeneralResetBtn').onclick = function()
        {
            obj.resetSide('d');
            return false;
        }
        $ge(pfx + 'recoveryResetBtn').onclick = $ge(pfx + 'recoveryGeneralResetBtn').onclick = function()
        {
            obj.resetSide('r');
            return false;
        }

        // - Visualisation d'un état des lieux
        $ge(pfx + 'deliveryViewMachineInventoryBtn').onclick = function()
        {
            mySelf._showMachineInventoryPopup(obj, 'd');
            return false;
        }
        $ge(pfx + 'recoveryViewMachineInventoryBtn').onclick = function()
        {
            mySelf._showMachineInventoryPopup(obj, 'r');
            return false;
        }

        // - Gestion de la popup
        this._popupDspObj.initEvents(pfx + 'popup_', obj);
    },

    /**
     * Etapes de confirmation de réalisation d'un transport
     */
    _showConfirmPopup : function(elBtn, pfx, obj, side, action)
    {
        var mySelf = this;
        var tabSide = obj.getSideInfos(side);

        // recherche droits
        var typeRight = (tabSide.isSelf ? 'self' : 'std');

        if (!tabSide.action || tabSide.action.type != action || !obj.hasRight(action + '-' + typeRight) ||
            (action == 'do' && !obj.isSideDoable(side)))
        {
            return false;
        }

        var confirmPopupEl = $ge(pfx + 'doUndoConfirmPopup');

        var currentLiEl = confirmPopupEl.querySelector('ul.choices > li.current');
        var linkedLiEl  = confirmPopupEl.querySelector('ul.choices > li.linked');
        var noneLiEl    = confirmPopupEl.querySelector('ul.choices > li.none');

        if (tabSide.action.msgs.current)
        {
            currentLiEl.style.display = '';
            currentLiEl.children[1].innerHTML = tabSide.action.msgs.current.title;
            currentLiEl.children[2].firstChild.onclick = function()
            {
                obj.doSide(side, 'current');
                Location.modalWindowManager.hide(pfx + 'doUndoConfirmPopup');
            }
        }
        else
        {
            currentLiEl.style.display = 'none';
        }
        if (tabSide.action.msgs.linked)
        {
            linkedLiEl.style.display = '';
            linkedLiEl.children[1].innerHTML = tabSide.action.msgs.linked.title;
            linkedLiEl.children[2].firstChild.onclick = function()
            {
                obj.doSide(side, 'linked');
                Location.modalWindowManager.hide(pfx + 'doUndoConfirmPopup');
            }
        }
        else
        {
            linkedLiEl.style.display = 'none';
        }

        // Evènements des boutons
        noneLiEl.children[2].firstChild.onclick = function()
        {
            Location.modalWindowManager.hide(pfx + 'doUndoConfirmPopup');
        }

        Location.modalWindowManager.show(pfx + 'doUndoConfirmPopup', {contentType: 3, width: 600, height: 'auto', 'isNoCloseBtn': true});
    },


    /**
     * Ouvrir la popup de visualisation d'un état des lieux
     */
    _showMachineInventoryPopup: function(obj, side)
    {
        var tabSide = obj.getSideInfos(side);
        Location.modalWindowManager.show(tabSide.tabMachineInventoryInfos.url, {contentType: 1, width: 680, height: 355});
    },


}
