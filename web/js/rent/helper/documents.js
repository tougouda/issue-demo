
if (!window.Location)
{
    var Location = new Object();
}


/*
 * Objet de gestion des documents
 *
 */
Location.Rent_Helper_Documents = function(documentObj, tabCfgs)
{
    this._documentObj = documentObj;
    this._tabCfgs = tabCfgs;

    this.list       = new Location.Rent_Helper_Documents_List(this);
    this.addList    = new Location.Rent_Helper_Documents_AddList(this);
    this.toMailList = new Location.Rent_Helper_Documents_ToMailList(this);

    this._tabActions = [];

    this._tabRights = null;

    this.onchange = null;
    this.onload   = null;

    this._tabExtParams = {};
}

Location.Rent_Helper_Documents.prototype = {

    /**
     * Copier les informations d'une ligne vers la courante
     *
     * @param Location.Rent_Helper_Proforma lineObj
     * @return bool
     */
    copy : function(sourceObj)
    {
        var tabProps = ['tabExtParams'];

        var i, prop;
        var nb = tabProps.length;
        for (i = 0; i < nb; i++)
        {
            prop = '_' + tabProps[i];
            this[prop] = LOC_Common.clone(sourceObj[prop]);
        }

        var sourceList    = sourceObj.list.getList();
        var sourceAddList = sourceObj.addList.getList();

        // Liste des documents existants : duplication
        for (var i = 0; i < sourceList.length; i++)
        {
            var srcItem = sourceList[i];
            var action = sourceObj.getActionById(srcItem.id);
            var actionType = (action ? action.type : '');
            if (actionType != 'remove')
            {
                var item = {
                    id: srcItem.id,
                    title: (actionType == 'edit' ? action.data.title : srcItem.title),
                    description: (actionType == 'edit' ? action.data.description : srcItem.description),
                    fileName: srcItem.fileName,
                    uploadAttach: null,
                    progress: 100,
                    loaded: true,
                    error: null
                };
                this.addList.getList().push(item);
                this._tabActions.push({
                    type: 'duplicate',
                    id: item.id,
                    data: {
                        title: item.title,
                        fileName: item.fileName
                    }
                });
            }
        }

        // Liste des ajouts en cours : à copier
        for (var i = 0; i < sourceAddList.length; i++)
        {
            var srcItem = sourceAddList[i];
            if (!srcItem.error)
            {
                var item = {
                    id: srcItem.id,
                    title: srcItem.title,
                    description: srcItem.description,
                    fileName: srcItem.fileName,
                    uploadAttach: null,
                    progress: srcItem.progress,
                    loaded: srcItem.loaded,
                    error: null
                };
                if (srcItem.loaded)
                {
                    var action = sourceObj.getActionById(srcItem.id);
                    this._tabActions.push({
                        type: action.type,
                        id: item.id,
                        data: {
                            title: item.title,
                            fileName: item.fileName
                        }
                    });
                }
                else
                {
                    item.id = 'upload_' + Location.Rent_Helper_Documents_AddList._uploadCpt++;
                    this.addList._attachToUpload(item, srcItem.uploadAttach.upload);
                }
                this.addList.getList().push(item);
            }
        }

        return true;
    },

    /**
     * Chargement de données
     *
     * @param Object tab          Données
     * @param Object tabExtParams Paramètres externes
     * @param Object tabRights    Droits
     */
    load : function(tab, tabExtParams, tabRights)
    {
        this._tabActions = [];
        this.list.load(tab);
        this.addList.load();

        for (var name in tabExtParams)
        {
            if (this._tabExtParams.hasOwnProperty(name))
            {
                this._tabExtParams[name] = tabExtParams[name];
            }
        }

        this._tabRights = tabRights;

        if (this.onload)
        {
            this.onload();
        }
    },

    /**
     * Récupérer instance du document associé
     *
     * @return Object
     */
    getDocumentObject : function()
    {
        return this._documentObj;
    },

    /**
     * Récupérer la taille maximum autorisée pour les documents
     */
    getMaxFileSize: function()
    {
        return this._tabCfgs.maxFileSize;
    },

    /**
     * Récupérer la taille maximum des pièces jointes par mail
     */
    getMaxMailAttachmentsSize: function()
    {
        return this._tabCfgs.maxMailAttachmentsSize;
    },

    /**
     * Récupérer l'URL de génération de la liste des docs à envoyer par mail
     */
    getMailUrl: function()
    {
        return this._tabCfgs.generateDocumentsMailUrl;
    },

    /**
     * Récupérer l'URL d'affichage d'un état des lieux
     */
    getMachineInventoryPhotosUrl: function()
    {
        return this._tabCfgs.machineInventoryPhotosUrl;
    },

    /**
     * Récupérer l'URL de l'historique d'un état des lieux
     */
    getMachineInventoryHistoryUrl: function()
    {
        return this._tabCfgs.machineInventoryHistoryUrl;
    },

    /**
     * Récupérer le tableau des actions en cours
     *
     * @return Array
     */
    getActions: function()
    {
        return this._tabActions;
    },

    /**
     * Ajouter une action
     *
     * @return void
     */
    addAction: function(action)
    {
        this._tabActions.push(action);

        if (this.onchange)
        {
            this.onchange(['actionAdded']);
        }
    },

    /**
     * Récupérer l'action d'un document
     */
    getActionById: function(id)
    {
        return this._tabActions.find(function(item){
            return (item.id == id);
        });
    },

    /**
     * Vérifier le droit à un champ
     *
     * @return bool
     */
    hasRight : function(fieldName)
    {
        return (this._tabRights === null || this._tabRights[fieldName] ? true : false);
    },

    /**
     * Annuler les actions sur un document
     */
    undo: function(id)
    {
        var index = this._tabActions.findIndex(function(item) { return item.id == id; });
        if (index > -1)
        {
            this._tabActions.splice(index, 1);
        }

        if (this.onchange)
        {
            this.onchange(['actionUndone']);
        }

        return true;
    }

}

/**
 * Liste des documents
 */
Location.Rent_Helper_Documents_List = function(mainObj) {

    this._mainObj = mainObj;
    this._tabList = [];
}

Location.Rent_Helper_Documents_List.prototype = {

    /**
     * Chargement des données
     */
    load: function(tab)
    {
        this._tabList = tab;
    },

    /**
     * Récupérer les documents
     *
     * @return Array
     */
    getList : function()
    {
        return this._tabList;
    },

    /**
     * Récupérer un élément par son id
     *
     * @param int id Identifiant du document
     * @return array|undefined
     */
    getById: function(id)
    {
        return this._tabList.find(function(item) { return item.id == id; });
    },

    /**
     * Modifier un document
     */
    edit: function(id, title, description)
    {
        if (this._mainObj.getActionById(id))
        {
            return false;
        }

        var action = {
            type: 'edit',
            id: id,
            data: {
                title: title,
                description: description
            }
        };

        this._mainObj.addAction(action);
        return true;
    },

    /**
     * Supprimer un document de la liste
     */
    remove: function(id)
    {
        if (this._mainObj.getActionById(id))
        {
            return false;
        }

        var action = {
            type: 'remove',
            id: id
        };
        this._mainObj.addAction(action);
        return true;
    },

    /**
     * Modifier l'état d'un document de la liste
     */
    setState: function(id, state)
    {
        var item = this.getById(id);

        if (!item)
        {
            return false;
        }

        item.stateId = state;

        if (this._mainObj.onchange)
        {
            this._mainObj.onchange(['list.changeState']);
        }
        return true;
    },

    /**
     * Droits sur les actions
     */
    isDownloadable: function(id)
    {
        return !(this._mainObj.getActionById(id));
    },

    isEditable: function(id)
    {
        return (this._mainObj.hasRight('edit') && !(this._mainObj.getActionById(id)));
    },

    isDeletable: function(id)
    {
        return (this._mainObj.hasRight('remove') && !(this._mainObj.getActionById(id)));
    },

    isUndoable: function(id)
    {
        return !!this._mainObj.getActionById(id);
    }

}


/**
 * Liste des documents en cours d'ajout
 */
Location.Rent_Helper_Documents_AddList = function(mainObj) {

    this._mainObj = mainObj;
    this._tabList = [];
}

// Propriété statique pour le compteur des fichiers uploadés
Location.Rent_Helper_Documents_AddList._uploadCpt = 0;

// Gestionnaire des uploads de documents
Location.Rent_Helper_Documents_AddList.uploadManager = {

    _tabCfgs: null,

    init: function(tabCfgs) {
        this._tabCfgs = tabCfgs;
    },

    create: function(file) {

        var tabCfgs = this._tabCfgs;

        var xhr = null;
        var tabAttached = [];

        var uploadObj = {
            file: file,
            status: 'new',
            progress: 0,
            errorMsg: null,
            reponse: null,

            attach: function(item, onStatusChange) {
                var mySelf = this;

                var container = {
                    attached: true,
                    func: onStatusChange
                };

                container.obj = {
                    item: item,
                    upload: mySelf,
                    detach: function() {
                        if (container.attached)
                        {
                            container.attached = false;
                            if (xhr)
                            {
                                var nbAttached = 0;
                                for (var i = 0; i < tabAttached.length; i++)
                                {
                                    nbAttached += (tabAttached[i].attached ? 1 : 0);
                                }

                                if (nbAttached == 0 && mySelf.status != 'completed')
                                {
                                    xhr.abort();
                                }
                            }
                        }
                    }
                };
                tabAttached.push(container);
                return container.obj;
            },

            getAttachedList: function() {
                return tabAttached.filter(function(attach) {
                    return attach.attached;
                }).map(function(attach) {
                    return attach.obj;
                });
            },

            send: function(onStatusChange) {

                if (tabAttached.length == 0)
                {
                    return false;
                }

                var mySelf = this;
                var statusChange = function() {
                    onStatusChange(mySelf);
                    for (var i = 0; i < tabAttached.length; i++)
                    {
                        if (tabAttached[i].attached)
                        {
                            tabAttached[i].func(mySelf);
                        }
                    }
                }

                if (this.file.size > tabCfgs.maxFileSize)
                {
                    this.status = 'error';
                    this.errorMsg = 'maxSize';
                    statusChange();
                    return false;
                }

                xhr = new Location.HTTPRequest(tabCfgs.uploadDocumentUrl);

                xhr.oncompletion = function() {
                    mySelf.status = 'completed';
                    mySelf.response = JSON.parse(xhr.response);
                    statusChange();
                };
                xhr.onerror = function() {
                    if (xhr.responseStatus == 400)
                    {
                        var response = JSON.parse(xhr.response);
                        mySelf.errorMsg = response.errors;
                    }
                    else
                    {
                        mySelf.errorMsg = 'fatalError';
                    }
                    mySelf.status = 'error';
                    statusChange();
                };
                xhr.upload.onprogress = function(e) {
                    mySelf.status = 'pending';
                    mySelf.progress = (e.total == 0 ?  0 : e.loaded * 100 / e.total);
                    statusChange();
                };

                var form = new FormData();
                form.append('file', this.file, this.file.name);
                xhr.setData(form).send();

                return true;
            }

        };

        return uploadObj;
    }

};

Location.Rent_Helper_Documents_AddList.prototype = {

    /**
     * Chargement des données
     */
    load: function()
    {
        this._tabList = [];
    },

    /**
     * Récupérer les documents en cours d'ajout
     *
     * @return Array
     */
    getList: function()
    {
        return this._tabList;
    },

    /**
     * Indique si au moins un document ajouté est en erreur
     *
     * @return bool
     */
    hasError: function()
    {
        return this._tabList.some(function(item) {
            return item.error;
        });
    },

    /**
     * Indique si au moins un document est en cours de téléversement
     *
     * @return bool
     */
    hasPending: function()
    {
        return this._tabList.some(function(item) {
            return !item.loaded && !item.error;
        });
    },

    /**
     * Annuler tous les documents est en cours de téléversement
     *
     * @return bool
     */
    cancelPendings: function()
    {
        var mySelf = this;
        // Attention aux références (car .cancel supprime l'item du tableau) !
        this._tabList.filter(function(item) {
            return (!item.loaded && !item.error);
        }).forEach(function(item) {
            mySelf.cancel(item.id);
        });
    },

    _attachToUpload: function(item, uploadObj) {

        var mySelf = this;
        item.uploadAttach = uploadObj.attach(item, function(upload) {
            switch (upload.status) {
                case 'pending':
                    item.progress = upload.progress;
                    break;
                case 'completed':
                    item.progress = 100;
                    item.loaded = true;
                    var action = {
                        type: 'add',
                        id: item.id,
                        data: {
                            title: item.title,
                            description: item.description,
                            fileName: item.fileName,
                            tempName: upload.response.tempName
                        }
                    };
                    mySelf._mainObj.addAction(action);
                    break;
                case 'error':
                    item.error = upload.errorMsg;

                    if (mySelf._mainObj.onchange)
                    {
                        mySelf._mainObj.onchange(['addList.failed']);
                    }
                    break;
            }
        });

        return item;
    },

    /**
     * Ajoute à la liste
     */
    add: function(title, description, fileName, uploadObj)
    {
        // Récupération de la liste des fichiers déjà présents
        var tabFileNames = this._mainObj.list.getList().map(function(item) {
            return item.fileName;
        }).concat(this._tabList.map(function(item) {
            return item.fileName;
        }));

        // Le nouveau a-t-il un nom de fichier déjà existant ?
        var cpt = 0;
        var renameFunc = function() {
            var newFileName = fileName;
            if (cpt > 0)
            {
                var pos = fileName.lastIndexOf('.');
                if (pos != -1)
                {
                    newFileName = fileName.substring(0, pos) + ' (' + cpt + ')' + fileName.substring(pos);
                }
                else
                {
                    newFileName +=  ' (' + cpt + ')';
                }
            }
            if (tabFileNames.indexOf(newFileName) != -1)
            {
                cpt++;
                return renameFunc();
            }
            return newFileName;
        };

        var item = {
            id: 'upload_' + Location.Rent_Helper_Documents_AddList._uploadCpt++,
            title: title,
            description: description,
            fileName: renameFunc(),
            uploadAttach: null,
            progress: 0,
            loaded: false,
            error: null
        };

        this._attachToUpload(item, uploadObj);
        this._tabList.push(item);

        if (this._mainObj.onchange)
        {
            this._mainObj.onchange(['addList.added']);
        }

        return item.id;
    },

    /**
     * Supprime un document en cours d'ajout
     */
    cancel: function(id)
    {
        // Supprime du tableau d'affichage des ajouts en cours
        var index = this._tabList.findIndex(function(item) { return item.id == id; });
        if (index == -1)
        {
            return false;
        }

        var item = this._tabList[index];
        if (item.uploadAttach)
        {
            item.uploadAttach.detach();
        }

        this._tabList.splice(index, 1);

        // Supprime du tableau d'actions
        this._mainObj.undo(id);

        if (this._mainObj.onchange)
        {
            this._mainObj.onchange(['addList.cancelled']);
        }
        return true;
    },

    /**
     * Récupérer un élément par son id
     *
     * @param string id Identifiant du document ajouté
     * @return array|undefined
     */
    getById: function(id)
    {
        return this._tabList.find(function(item) { return item.id == id; });
    },

    /**
     * L'ajout est-il possible ?
     */
    isAddPossible: function()
    {
        return this._mainObj.hasRight('add');
    },
}

/**
 * Liste des documents à envoyer par mail
 */
Location.Rent_Helper_Documents_ToMailList = function(mainObj) {

    this._mainObj = mainObj;
    this._tabList = [];

    this._totalSize = 0;
    this._href = null;
    this._mailUrlHttpObj = null;
}

Location.Rent_Helper_Documents_ToMailList.prototype = {

    /**
     * Récupérer les documents à envoyer
     */
    getList: function()
    {
        return this._tabList;
    },

    /**
     * Récupérer la taille totale des documents à envoyer
     */
    getTotalSize: function()
    {
        return this._totalSize;
    },

    /**
     * Récupérer l'url de la popup d'envoi
     */
    getHref: function()
    {
        return this._href;
    },

    /**
     * Ajoute à la liste des documents à envoyer par mail
     */
    add: function(tabInfos)
    {
        if (!this.getById(tabInfos.id))
        {
            this._tabList.push(tabInfos);
            this._totalSize += tabInfos.size;

            this._updateHref();
            if (this._mainObj.onchange)
            {
                this._mainObj.onchange(['toMailList.added']);
            }
        }
    },

    /**
     * Retire de la liste des documents à envoyer par mail
     */
    remove: function(id)
    {
        var index = this._tabList.findIndex(function(item) { return item.id == id; });
        if (index != -1)
        {
            this._totalSize -= this._tabList[index].size;
            this._tabList.splice(index, 1);

            this._updateHref();
            if (this._mainObj.onchange)
            {
                this._mainObj.onchange(['toMailList.removed']);
            }
        }
    },

    /**
     * Met à jour l'url du bouton d'envoi de mail
     */
    _updateHref: function()
    {
        var mySelf = this;

        if (this._mailUrlHttpObj)
        {
            this._mailUrlHttpObj.abort();
        }

        if (this._tabList.length == 0 || this._mainObj.getActions().length > 0)
        {
            mySelf._href = null;
            return false;
        }
        mySelf._href = '#';

        // Initialisation de l'objet HTTPRequest
        if (!this._mailUrlHttpObj)
        {
            this._mailUrlHttpObj = new Location.HTTPRequest(this._mainObj.getMailUrl());
        }

        // Passage des paramètres
        var recipientMail;
        var documentObj = this._mainObj.getDocumentObject();
        if (documentObj.getObjectType() == 'estimateLine')
        {
            recipientMail = documentObj.getEstimateObject().getCustomerObject().getOrderContactEmail();
        }
        else if (documentObj.getObjectType() == 'contract')
        {
            recipientMail = documentObj.getCustomerObject().getOrderContactEmail();
        }

        this._mailUrlHttpObj.setVar('documents', this._tabList);
        this._mailUrlHttpObj.setVar('recipientMail', recipientMail);
        this._mailUrlHttpObj.setVar('entityId', documentObj.getId());

        var completionFunc = function(data)
        {
            if (data && data.url != '')
            {
                mySelf._href = data.url;
            }

            if (mySelf._mainObj.onchange)
            {
                mySelf._mainObj.onchange(['toMailList.hrefUpdated']);
            }
        }

        // Réception des données
        this._mailUrlHttpObj.oncompletion = function()
        {
            completionFunc(this.getJSONResponse());
        };

        // Erreur
        this._mailUrlHttpObj.onerror = function()
        {
            completionFunc(false);
        };

        // Envoi de la requête HTTP
        this._mailUrlHttpObj.send(true, true);
    },

    /**
     * Récupérer un élément par son id
     *
     * @param string id Identifiant du document ajouté
     * @return array|undefined
     */
    getById: function(id)
    {
        return this._tabList.find(function(item) { return item.id == id; });
    }
}


Location.Rent_Helper_DocumentsView = {

    _isMoreDisplayed: false,
    _tabHttpObjs: [],

    /**
     * Mise à jour de l'affichage
     *
     */
    display : function(pfx, obj, tabErrors)
    {
        var mySelf = this;

        /**
         * Tableau des documents
         */
        var tableEl = $ge(pfx + 'list');
        var tBodyMainEl = tableEl.tBodies[0];
        var tBodySuppEl = tableEl.tBodies[1];

        // Suppression des lignes
        while (tBodyMainEl.rows.length > 0)
        {
            tBodyMainEl.deleteRow(0);
        }
        while (tBodySuppEl.rows.length > 0)
        {
            tBodySuppEl.deleteRow(0);
        }

        var tab = obj.list.getList();

        var nb = tab.length;
        Location.commonObj.toggleEltClass(tableEl, 'noResult', (nb == 0));

        var displayLine = function(tabInfos) {
            var rEl = tBodyMainEl.insertRow(-1);
            var docType = tabInfos.id.substring(0, 3);
            if (docType == DOC_PREFIX_ATTACHMENT)
            {
                mySelf._displayLine(pfx, obj, rEl, tabInfos);
            }
            else if (docType == DOC_PREFIX_MACHINEINVENTORY)
            {
                mySelf._displayMachineInventoryLine(pfx, obj, rEl, tabInfos);
            }
        }

        var min = (this._isMoreDisplayed ? nb : Math.min(nb, DOC_MAXDISPLAY));
        for (var i = 0; i < min; i++)
        {
            displayLine(tab[i]);
        }

        // - Affichage de documents supplémentaires
        if (nb > min)
        {
            var diff = nb - min;

            Location.commonObj.addEltClass(tableEl, 'moreResult');
            var cEl = tBodySuppEl.insertRow(-1).insertCell(-1);
            cEl.colSpan = 7;
            var message = diff > 1 ? LOC_View.getTranslation(pfx + 'label_more_p') : LOC_View.getTranslation(pfx + 'label_more_s');
            cEl.innerHTML = message.replace(/<%nb>/, diff);

            cEl.onclick = function() {
                mySelf._isMoreDisplayed = true;
                // Affichage des lignes supplémentaires
                Location.commonObj.removeEltClass(tableEl, 'moreResult');

                for (var i = min; i < nb; i++)
                {
                    displayLine(tab[i]);
                }
            };
        }

        /**
         * Tableau des ajouts en cours
         */
        if (!obj.addList.isAddPossible())
        {
            Location.commonObj.removeEltClass(document.querySelector('fieldset.add-list'), 'active');
        }
        else
        {
            Location.commonObj.addEltClass(document.querySelector('fieldset.add-list'), 'active');
            var tableAddEl = $ge(pfx + 'list-add');
            var tBodyAddEl = tableAddEl.tBodies[0];

            // Suppression des lignes
            while (tBodyAddEl.rows.length > 0)
            {
                tBodyAddEl.deleteRow(0);
            }

            var tabAdd = obj.addList.getList();

            var nbAdd = tabAdd.length;
            Location.commonObj.toggleEltClass(tableAddEl, 'noResult', (nbAdd == 0));

            for (var i = 0; i < nbAdd; i++)
            {
                var rEl, cEl;

                rEl = tBodyAddEl.insertRow(-1);
                mySelf._displayAddLine(pfx, obj, rEl, tabAdd[i]);
            }
        }

        /**
         * Drag and drop de fichiers
         */
        if (window.File && window.FileList && window.FileReader)
        {
            mySelf._initDragAndDrop($ge(pfx + 'container'), pfx, obj, function(tabFiles) {
                mySelf._showAddPopup(pfx, obj, tabFiles, 0);
            });
        }

        /**
         * Bouton d'ajout
         */
        var addPopupBtnEl = $ge(pfx + 'addPopupBtn');
        Location.commonObj.toggleEltClass(addPopupBtnEl, 'disabled', !obj.addList.isAddPossible());

        /**
         * Bouton d'envoi de mail
         */
        var mailBtnEl = $ge(pfx + 'mailBtn');
        var href = obj.toMailList.getHref();

        // Vérification de la taille des pièces jointes
        var isMaxExceeded  = (obj.toMailList.getTotalSize() > obj.getMaxMailAttachmentsSize());

        var hasHref = (href && href != '#');
        // Désactiver si
        // - pas d'objet dans la liste
        // - pas de lien
        // - poids dépassé
        // - pas d'action
        var isDisabled = (obj.toMailList.getList().length == 0 || !hasHref || isMaxExceeded || obj.getActions().length > 0);
        Location.commonObj.toggleEltClass(mailBtnEl, 'disabled', isDisabled);

        // Affichage "en cours de récupération de l'url"
        Location.commonObj.toggleEltClass(mailBtnEl, 'loading', (href == '#'));

        // Affichage du warning ?
        Location.commonObj.toggleEltClass(mailBtnEl, 'error', isMaxExceeded);

        mailBtnEl.title = mailBtnEl.getAttribute('data-' + (isMaxExceeded ? 'errortitle' : 'title'));

        mailBtnEl.href = (!href || isDisabled ? '#' : href);
    },

    _initDragAndDrop: function(container, pfx, obj, onFileSelectCallback)
    {
        var mySelf = this;
        var layerElt = container.querySelector('.drag-layer');
        var hoverClass = obj.addList.isAddPossible() ? 'hoverClass' : 'hoverClass off';

        var counter = 0;
        container.ondragover = function(event)
        {
            event.preventDefault();
        }
        // enter
        container.ondragenter = function(event) {
            event.preventDefault();
            if (counter == 0)
            {
                Location.commonObj.addEltClass(this, 'drag-and-drop-on');
                Location.commonObj.addEltClass(layerElt, hoverClass);
            }
            counter++;
        }
        // leave
        container.ondragleave = function(event) {
            counter--;
            if (counter == 0)
            {
                Location.commonObj.removeEltClass(this, 'drag-and-drop-on');
                Location.commonObj.removeEltClass(layerElt, hoverClass);
            }
        }
        // drop
        container.ondrop = function(event) {
            event.preventDefault();
            counter--;
            if (counter == 0)
            {
                Location.commonObj.removeEltClass(this, 'drag-and-drop-on');
                Location.commonObj.removeEltClass(layerElt, hoverClass);
            }
            if (obj.addList.isAddPossible())
            {
                var tabFiles = event.target.files || event.dataTransfer.files;
                if (tabFiles.length > 0)
                {
                    onFileSelectCallback(tabFiles);
                }
            }
        }
    },

    /**
     * Initialisation
     *
     */
    initEvents : function(pfx, obj)
    {
        var mySelf = this;

        // Ajouter un document
        $ge(pfx + 'addPopupBtn').onclick = function()
        {
            if (!Location.commonObj.hasEltClass(this, 'disabled'))
            {
                mySelf._showAddPopup(pfx, obj, [], 0);
            }
            return false;
        };

        // Envoyer par e-mail
        $ge(pfx + 'mailBtn').onclick = function()
        {
            var emailPopup = Location.popupsManager.createPopup('docEmailPopup', '', 'center', 'center', 500, 450);
            var url = this.getAttribute('href');

            if (url != '#' && !Location.commonObj.hasEltClass(this, 'disabled'))
            {
                emailPopup.open(url);
            }
            return false;
        };
    },

    /**
     * Affiche un document dans le tableau
     */
    _displayLine: function(pfx, obj, rowElement, tabInfos)
    {
        var mySelf = this;
        var tabAction = obj.getActionById(tabInfos.id);

        var isDownloadable = obj.list.isDownloadable(tabInfos.id);
        var isEditable     = (obj.list.isEditable(tabInfos.id) && tabInfos.isEditable);
        var isDeletable    = (obj.list.isDeletable(tabInfos.id) && tabInfos.isDeletable);
        var isUndoable     = obj.list.isUndoable(tabInfos.id);
        var isSendable     = (obj.getActions().length == 0);
        var isInToMailList = !!obj.toMailList.getById(tabInfos.id);

        if (tabAction)
        {
            rowElement.className = 'updated action_' + tabAction.type;
        }
        if (tabInfos.isExtern)
        {
            rowElement.className += ' isExtern';
        }

        // Type
        var cEl = rowElement.insertCell(-1);
        cEl.className = 'type';
        var fileExtensionEl = $ge(pfx + 'fileExtension').cloneNode(true);
        fileExtensionEl.removeAttribute('id');
        Location.commonObj.addEltClass(fileExtensionEl, tabInfos.extension);
        fileExtensionEl.title = tabInfos.extension;
        cEl.appendChild(fileExtensionEl);

        // Titre
        cEl = rowElement.insertCell(-1);
        cEl.className = 'title';
        // - Affichage principal
        var titleEl = cEl;
        var el = window.document.createElement('SPAN');
        if (isDownloadable)
        {
            titleEl = window.document.createElement('A');
            this._addDownloadHref(titleEl, rowElement, tabInfos);
            cEl.appendChild(titleEl);
        }
        el.innerHTML = tabInfos.title || tabInfos.fileName;
        titleEl.appendChild(el);

        if (tabInfos.title && tabInfos.fileName)
        {
            // - Affichage secondaire
            var el = window.document.createElement('SPAN');
            Location.commonObj.addEltClass(el, 'fileName');
            el.innerHTML = tabInfos.fileName;
            titleEl.appendChild(el);
        }

        // Poids
        cEl = rowElement.insertCell(-1);
        cEl.className = 'size';
        if (tabInfos.size)
        {
            cEl.innerHTML = LOC_View.getBytesFormat(tabInfos.size, 2);
        }

        // Date
        cEl = rowElement.insertCell(-1);
        cEl.className = 'date';
        cEl.innerHTML = tabInfos.date;

        // Origine du document
        cEl = rowElement.insertCell(-1);
        cEl.className = 'origin';
        if (tabInfos.origin)
        {
            var elOrigin = window.document.createElement('A');
            if (tabInfos.moduleName == ATTACHMENTS_MODULE_RENTESTIMATELINEDOCUMENT)
            {
                elOrigin.className = 'action-btn openRentEstimate';
                elOrigin.title = LOC_View.getTranslation(pfx + 'button_view_origin_estimate_line');
            }
            elOrigin.href = tabInfos.origin;
            elOrigin.target = '_blank';
            cEl.appendChild(elOrigin);
        }

        // Actions
        cEl = rowElement.insertCell(-1);
        cEl.className = 'ctrls';
        cEl.onclick = function(e) { e.stopPropagation(); };
        // - Télécharger
        if (isDownloadable)
        {
            var isDownloading = false;
            var elDownload = window.document.createElement('A');
            elDownload.className = 'download';
            elDownload.title = LOC_View.getTranslation(pfx + 'button_download');
            this._addDownloadHref(elDownload, rowElement, tabInfos);
            cEl.appendChild(elDownload);
        }

        // - Modifier
        if (isEditable)
        {
            el = window.document.createElement('A');
            el.href = '#';
            el.className = 'edit';
            el.title = LOC_View.getTranslation(pfx + 'button_edit');
            el.onclick = function(e) {
                e.stopPropagation();
                mySelf._showEditPopup(pfx, obj, tabInfos, true);
                return false;
            };
            cEl.appendChild(el);
        }
        else
        {
            el = window.document.createElement('A');
            el.href = '#';
            el.className = 'view';
            el.title = LOC_View.getTranslation(pfx + 'button_view');
            el.onclick = function(e) {
                e.stopPropagation();
                mySelf._showEditPopup(pfx, obj, tabInfos, false);
                return false;
            };
            cEl.appendChild(el);
        }

        // - Supprimer
        if (isDeletable)
        {
            el = window.document.createElement('A');
            el.href = '#';
            el.className = 'delete';
            el.title = LOC_View.getTranslation(pfx + 'button_delete');
            el.onclick = function(e) {
                e.stopPropagation();
                obj.list.remove(tabInfos.id);
                return false;
            };
            cEl.appendChild(el);
        }
        else
        {
            el = window.document.createElement('SPAN');
            cEl.appendChild(el);
        }

        // - Annulation de la suppression
        if (isUndoable)
        {
            el = window.document.createElement('A');
            el.href = '#';
            el.className = 'undo';
            el.title = LOC_View.getTranslation(pfx + 'button_undo');
            el.onclick = function() {
                obj.undo(tabInfos.id);
                return false;
            };
            cEl.appendChild(el);
        }

        // Checkbox envoyer par mail
        cEl = rowElement.insertCell(-1);
        cEl.onclick = function(e) { e.stopPropagation(); };
        cEl.className = 'editCtrls';

        el = window.document.createElement('INPUT');
        el.type = 'checkbox';
        el.title = LOC_View.getTranslation(pfx + 'checkbox_sendmail');
        el.value = tabInfos.id;
        el.checked = isInToMailList;
        if (!isSendable)
        {
            el.disabled = true;
        }
        el.onclick = function(e) {
            e.stopPropagation();
            if (this.checked)
            {
                obj.toMailList.add(tabInfos);
            }
            else
            {
                obj.toMailList.remove(tabInfos.id);
            }
        };
        cEl.appendChild(el);
    },

    /**
     * Affiche un document de type état des lieux dans le tableau
     */
    _displayMachineInventoryLine: function(pfx, obj, rowElement, tabInfos)
    {
        var mySelf = this;
        var tabAction = obj.getActionById(tabInfos.id);

        if (tabAction)
        {
            rowElement.className = 'updated action_' + tabAction.type;
        }
        Location.commonObj.addEltClass(rowElement, tabInfos.stateId);

        // Type
        var cEl = rowElement.insertCell(-1);
        cEl.className = 'type';
        var el = window.document.createElement('SPAN');
        Location.commonObj.addEltClass(el, 'machineInventory');
        cEl.appendChild(el);

        // Titre
        cEl = rowElement.insertCell(-1);
        cEl.className = 'title';
        // - Affichage principal
        var titleEl = cEl;
        var el = window.document.createElement('SPAN');
        titleEl = window.document.createElement('A');
        cEl.appendChild(titleEl);
        var title = LOC_View.getTranslation(pfx + 'title_machineInventory_' + tabInfos.type);
        el.innerHTML = title.replace(/<%id>/, tabInfos.id.substr(4));

        titleEl.onclick = function() {
            // ouverture de la popup
            mySelf._showMachineInventoryPopup(pfx, obj, tabInfos);
            return false;
        };
        titleEl.appendChild(el);

        // Poids
        cEl = rowElement.insertCell(-1);
        cEl.className = 'size';

        // Date
        cEl = rowElement.insertCell(-1);
        cEl.className = 'date';
        cEl.innerHTML = tabInfos.date;

        // Origine du document
        cEl = rowElement.insertCell(-1);
        cEl.className = 'origin';

        // Actions
        cEl = rowElement.insertCell(-1);
        cEl.className = 'ctrls';
        cEl.onclick = function(e) { e.stopPropagation(); };
        // - Voir
        el = window.document.createElement('A');
        el.href = '#';
        el.className = 'view';
        el.title = LOC_View.getTranslation(pfx + 'button_view');
        el.onclick = function(e) {
            e.stopPropagation();
            // ouverture de la popup
            mySelf._showMachineInventoryPopup(pfx, obj, tabInfos);
            return false;
        };
        cEl.appendChild(el);
        // - décalage affichage
        el = window.document.createElement('SPAN');
        cEl.appendChild(el);

        // Cellule vide (envoi par mail)
        cEl = rowElement.insertCell(-1);
    },


    /**
     * Affiche un document dans le tableau des ajouts en cours
     */
    _displayAddLine: function(pfx, obj, rowElement, tabInfos)
    {
        var mySelf = this;
        rowElement.id = 'add_' + tabInfos.id;

        if (tabInfos.error)
        {
            rowElement.className = 'error';
        }

        // Type
        cEl = rowElement.insertCell(-1);
        cEl.className = 'type';
        var fileExtensionEl = $ge(pfx + 'fileExtension').cloneNode(true);
        fileExtensionEl.removeAttribute('id');
        Location.commonObj.addEltClass(fileExtensionEl, tabInfos.extension);
        cEl.appendChild(fileExtensionEl);

        // Titre
        cEl = rowElement.insertCell(-1);
        cEl.className = 'title';
        // - Affichage principal
        var el = window.document.createElement('SPAN');
        el.innerHTML = tabInfos.title || tabInfos.fileName;
        cEl.appendChild(el);
        if (tabInfos.title && tabInfos.fileName)
        {
            // - Affichage secondaire
            var el = window.document.createElement('SPAN');
            Location.commonObj.addEltClass(el, 'fileName');
            el.innerHTML = tabInfos.fileName;
            cEl.appendChild(el);
        }

        // Progression
        cEl = rowElement.insertCell(-1);
        cEl.className = 'progress';
        if (tabInfos.error)
        {
            el = window.document.createElement('SPAN');
            el.innerHTML = LOC_View.getTranslation(pfx + 'error_' + tabInfos.error);
        }
        else if (!tabInfos.loaded)
        {
            el = window.document.createElement('PROGRESS');
            el.max = 100;
            el.value = tabInfos.progress;
        }
        else
        {
            el = window.document.createElement('SPAN');
        }

        cEl.appendChild(el);

        // Actions
        cEl = rowElement.insertCell(-1);
        cEl.className = 'ctrls';
        // - Télécharger
        el = window.document.createElement('A');
        el.href = '#';
        el.className = 'remove';
        el.title = LOC_View.getTranslation(pfx + 'button_remove');
        el.onclick = function() {
            // Annulation
            obj.addList.cancel(tabInfos.id);
            return false;
        };
        cEl.appendChild(el);
    },

    /**
     * Met à jour l'avancement sur une ligne en cours d'ajout
     */
    _displayLineProgress: function(pfx, id, progress)
    {
        var progressEl = $ge(pfx + 'list-add').querySelector('#add_' + id + ' > td.progress > progress');
        if (progressEl)
        {
            progressEl.value = progress;
        }
    },

    /**
     * Ouvre la popup d'ajout
     */
    _showAddPopup: function(pfx, obj, tabFiles, fileIndex)
    {
        var mySelf = this;

        var isNextPossible = (fileIndex + 1 < tabFiles.length);

        var popupEl = $ge(pfx + 'addPopup');

        var elInputFile    = $ge(pfx + 'addFile');
        var elInputFileBtn = $ge(pfx + 'addFileBtn');
        var elFileName     = $ge(pfx + 'addFileName');
        var elTitle        = $ge(pfx + 'addTitle');
        var elDescription  = $ge(pfx + 'addDescription');
        var elOnAllEstimateLines = $ge(pfx + 'addOnAllEstimateLines');
        var elBtn          = $ge(pfx + 'addBtn');
        var elMsgError     = $ge(pfx + 'error-noFile');

        Location.commonObj.removeEltClass(popupEl, 'loading');

        // Affichage du bouton
        var replaceTxt = (tabFiles.length > 1 ? (fileIndex + 1) + '/' + tabFiles.length : '');
        elBtn.querySelector('span.displayCounter').innerHTML = replaceTxt;

        elFileName.innerHTML = '';
        elTitle.value = '';
        elDescription.value = '';
        Location.commonObj.removeEltClass(elMsgError, 'active');

        // Affichage de la ligne "Fichier"
        var file = null;
        var displayFileItem = function() {
            if (!file)
            {
                file = tabFiles[fileIndex];
            }
            if (file)
            {
                elFileName.innerHTML = file.name;
            }
            else
            {
                elInputFile.value = null;
                elFileName.innerHTML = '';
            }
        };

        displayFileItem();

        // Initialisation du drag and drop
        if (window.File && window.FileList && window.FileReader)
        {
            mySelf._initDragAndDrop(popupEl, pfx, obj, function(uploadFiles) {
                file = uploadFiles[0];
                displayFileItem();
            });
        }

        // Ouverture de la fenêtre pour choisir un fichier lors du clic sur le bouton
        elInputFileBtn.onclick = function() {
            elInputFile.click();
        };

        // Sélection du fichier dans l'input
        elInputFile.addEventListener('change', function() {
            var files = event.target.files || event.dataTransfer.files;
            // limite à 1 seul fichier
            if (files.length > 0)
            {
                file = files[0];
                displayFileItem();
            }
        }, false);

        // Ouverture de la popup
        var showNextPopup = null;
        if (isNextPossible)
        {
            showNextPopup = function() {
                mySelf._showAddPopup(pfx, obj, tabFiles, fileIndex + 1);
            };
        }

        Location.modalWindowManager.show(pfx + 'addPopup', {
            contentType: 3,
            width: 450,
            height: 'auto',
            onclose: showNextPopup
        });

        // Validation de la popup
        elBtn.onclick = function()
        {
            if (!file)
            {
                Location.commonObj.addEltClass(elMsgError, 'active');
                return false;
            }

            Location.commonObj.addEltClass(popupEl, 'loading');

            // - Fonction d'ajout du document sur toutes les entités données
            var addFunc = function(tabDocsObj, title, description, file) {

                var upload = Location.Rent_Helper_Documents_AddList.uploadManager.create(file);

                // Téléchargement du fichier en arrière plan
                for (var i = 0; i < tabDocsObj.length; i++)
                {
                    tabDocsObj[i].addList.add(title, description, file.name, upload);
                }

                upload.send(function(upload) {
                    upload.getAttachedList().forEach(function(attach) {
                        mySelf._displayLineProgress(pfx, attach.item.id, upload.progress);
                    });
                });

                // Ajouter un nouveau fichier ?
                if (showNextPopup)
                {
                    showNextPopup();
                }
                else
                {
                    elOnAllEstimateLines.checked = false;
                    Location.modalWindowManager.hide(pfx + 'addPopup');
                }

            };

            // Ajout au tableau d'affichage
            var title = (elTitle.value || '');
            var description = (elDescription.value || '');

            if (elOnAllEstimateLines && elOnAllEstimateLines.checked)
            {
                var estimateObj = obj.getDocumentObject().getEstimateObject();
                var tabLines = [];

                // - Lignes non chargées
                var tabNotLoadedLines = estimateObj.getNotLoadedLinesList(function(lineObj) {
                    return (lineObj.getStateId() == RENTESTIMATELINE_STATE_ACTIVE);
                });
                estimateObj.loadLines(tabNotLoadedLines, function() {
                    tabLines = estimateObj.getLines();
                    var tabDocsObj = [];
                    for (var i = 0; i < tabLines.length; i++)
                    {
                        if (tabLines[i].getLoadStatus() != '' && tabLines[i].getStateId() == RENTESTIMATELINE_STATE_ACTIVE)
                        {
                            tabDocsObj.push(tabLines[i].getDocumentsObject());
                        }
                    }
                    addFunc(tabDocsObj, title, description, file);
                });
            }
            else
            {
                addFunc([obj], title, description, file);
            }

            return false;
        }
    },


    /**
     * Ouvre la popup de modification
     */
    _showEditPopup: function(pfx, obj, tabInfos, isEdit)
    {
        var mySelf = this;

        Location.commonObj.toggleEltClass($ge(pfx + 'editPopup'), 'readonly', !isEdit);

        var elFileName    = $ge(pfx + 'editFileName');
        var elTitle       = $ge(pfx + 'editTitle');
        var elDescription = $ge(pfx + 'editDescription');

        elFileName.innerHTML = tabInfos.fileName;
        elTitle.value = tabInfos.title || '';
        elTitle.readOnly = !isEdit;
        elDescription.value = tabInfos.description || '';
        elDescription.readOnly = !isEdit;

        // Ouverture de la popup
        Location.modalWindowManager.show(pfx + 'editPopup', {contentType: 3, width: 450, height: 'auto'});

        // Validation de la popup
        var btnEl = $ge(pfx + 'editBtn');
        if (isEdit)
        {
            btnEl.onclick = function()
            {
                // Ajout au tableau d'actions
                var title = (elTitle.value || '');
                var description = (elDescription.value || '');

                obj.list.edit(tabInfos.id, title, description);
                Location.modalWindowManager.hide(pfx + 'editPopup');
                return false;
            }
        }
        else
        {
            btnEl.onclick = null;
        }
    },

    /**
     * Ouvre la popup de visualisation d'un état des lieux
     */
    _showMachineInventoryPopup: function(pfx, obj, tabInfos)
    {
        Location.modalWindowManager.show(tabInfos.url, {contentType: 1, width: 680, height: 480});
    },

    /**
     * Ajout du lien de téléchargement sur une balise A
     */
    _addDownloadHref: function(el, rowElement, tabInfos) {
        el.href = tabInfos.url;
        if (typeof el.download != 'undefined')
        {
            el.download = tabInfos.fileName;
        }
        else
        {
            el.target = '_blank';
        }
        el.onclick = function(e) {
            e.stopPropagation();
            if (Location.commonObj.hasEltClass(rowElement, 'downloading'))
            {
                return false;
            }
            Location.commonObj.addEltClass(rowElement, 'downloading');
            setTimeout(function() {
                Location.commonObj.removeEltClass(rowElement, 'downloading');
            }, 2000);
        };
    },

    checkPendingUploads: function(pfx, obj, callBackFunc, closeCallBack, tabMessages)
    {
        var mySelf = this;

        var tabObjs = (Array.isArray(obj) ? obj : [obj]);

        if (!tabObjs.some(function(obj) { return obj.addList.hasPending(); }))
        {
            return false;
        }

        var modalId = pfx + 'pendingModal';
        var modalEl = $ge(modalId);

        var forceLiEl = modalEl.querySelector('ul.choices > li.force');
        var forceBtnEl = forceLiEl.children[2].firstChild;
        $ge(pfx + 'pendingModalForceMsg').innerHTML = tabMessages.force;

        var waitLiEl = modalEl.querySelector('ul.choices > li.wait');
        var waitProgressEl = modalEl.querySelector('ul.choices > li.wait progress');
        var waitBtnEl  = waitLiEl.children[2].firstChild;
        $ge(pfx + 'pendingModalWaitMsg').innerHTML = tabMessages.wait;

        var noneLiEl  = modalEl.querySelector('ul.choices > li.none');
        $ge(pfx + 'pendingModalBackMsg').innerHTML = tabMessages.back;

        // Affichage
        Location.commonObj.addEltClass(waitLiEl, 'disabled');
        waitBtnEl.disabled = true;
        Location.commonObj.removeEltClass(forceLiEl, 'disabled');
        forceBtnEl.disabled = false;

        var progressTimer = null;
        var checkProgress = function() {
            // - récupère la progression
            var tabItems = tabObjs.reduce(function(list, obj) {
                return list.concat(obj.addList.getList());
            }, []).filter(function(item) {
                return !item.error;
            });

            if (tabObjs.some(function(obj) { return obj.addList.hasPending(); }))
            {
                var sum = 0;
                tabItems.forEach(function(item) {
                    sum += item.progress;
                });
                waitProgressEl.value = sum / tabItems.length;

                progressTimer = window.setTimeout(checkProgress, 100);
            }
            else
            {
                Location.commonObj.removeEltClass(waitLiEl, 'disabled');
                waitBtnEl.disabled = false;
                // Désactive la ligne pour forcer
                Location.commonObj.addEltClass(forceLiEl, 'disabled');
                forceLiEl.children[2].firstChild.disabled = true;
            }
        };


        // Forcer la validation
        forceBtnEl.onclick = function() {
            // Annuler les téléversements en cours
            tabObjs.forEach(function(obj) {
                obj.addList.cancelPendings();
            });
            if (progressTimer)
            {
                window.clearTimeout(progressTimer);
            }
            Location.modalWindowManager.hide(modalId);
            callBackFunc(true);
        }

        waitBtnEl.onclick = function()
        {
            Location.modalWindowManager.hide(modalId);
            if (progressTimer)
            {
                window.clearTimeout(progressTimer);
            }
            callBackFunc(false);
        }

        // Ne rien faire
        noneLiEl.children[2].firstChild.onclick = function()
        {
            Location.modalWindowManager.hide(modalId);
            if (progressTimer)
            {
                window.clearTimeout(progressTimer);
            }
            if (closeCallBack)
            {
                closeCallBack();
            }
        }

        // Attendre la fin des téléversements
        // - met à jour la barre de progression globale
        checkProgress();

        Location.modalWindowManager.show(modalId, {contentType: 3, width: 600, height: 'auto', 'isNoCloseBtn': true});

        return true;
    }
}