if (!window.Location)
{
    var Location = new Object();
}


/**
 * Classe de gestion de la pro forma
 *
 * @param Object documentObj Objet javascript du document (ligne de devis ou contrat)
 * @param Object tabCfgs     Configurations globales
 */
Location.Rent_Helper_Proforma = function(documentObj, tabCfgs)
{
    this._documentObj = documentObj;
    this._tabCfgs = tabCfgs;

    this._tabList = [];
    this._paidAmount = 0;
    this._deposit = 0;
    this._tabTypes = [];

    this._tabAction = null;

    this._tabExtParams = {
        duration: 1,
        fuelQtyEstimByDay: 0,
        fuelQtyMax: 0,
        isFuelNeeded: false,
        endDateDoc: '',
        maxExtensionEndDate: false,
        isCalcDisplayed: false,
        disabledComponents: {},
        displayMessages: []
    };

    this._tabRights = null;

    this.onchange = null;
    this.onload   = null;
}


Location.Rent_Helper_Proforma.prototype = {

    /**
     * Copier les informations d'une ligne vers la courante
     *
     * @param Location.Rent_Helper_Proforma lineObj
     * @return bool
     */
    copy : function(sourceObj)
    {
        var tabProps = ['tabExtParams'];

        var i, prop;
        var nb = tabProps.length;
        for (i = 0; i < nb; i++)
        {
            prop = '_' + tabProps[i];
            this[prop] = LOC_Common.clone(sourceObj[prop]);
        }

        return true;
    },

    /**
     * Chargement de données
     *
     */
    load : function(tab, tabExtParams, tabRights)
    {
        this._tabAction  = null;
        this._tabList    = tab.tabList;
        this._tabTypes   = tab.tabTypes;
        this._paidAmount = tab.paidAmount;
        this._deposit    = tab.deposit;

        for (var name in tabExtParams)
        {
            if (this._tabExtParams.hasOwnProperty(name))
            {
                this._tabExtParams[name] = tabExtParams[name];
            }
        }

        this._tabRights = tabRights;

        if (this.onload)
        {
            this.onload();
        }
    },

    /**
     * Récupérer l'instance du client
     *
     * @return Location.Rent_Helper_Customer
     */
    getCustomerObject : function()
    {
        if (this._documentObj.getObjectType() == 'contract')
        {
            return this._documentObj.getCustomerObject();
        }
        else
        {
            return this._documentObj.getEstimateObject().getCustomerObject();
        }
    },

    /**
     * Récupérer la liste des types autorisés
     *
     * @return Array
     */
    getAuthorizedTypes : function()
    {
        var tabTypes = [];

        for (var i = 0; i < this._tabTypes.length; i++)
        {
            if (!this._tabExtParams.disabledComponents.add ||
                (this._tabExtParams.disabledComponents.add.length > 0 &&
                 !in_array(this._tabTypes[i], this._tabExtParams.disabledComponents.add)))
            {
                tabTypes.push(this._tabTypes[i]);
            }
        }

        return tabTypes;
    },

    /**
     * Récupérer les pro formas
     *
     * @return Array
     */
    getList : function()
    {
        return this._tabList;
    },

    /**
     * Récupérer les informations d'une pro forma
     *
     * @return Object
     */
    getInfos : function(id)
    {
        id = Number(id);
        var i = 0, n = this._tabList.length;
        while (i < n && this._tabList[i].id != id)
        {
            i++;
        }
        return (i < n ? this._tabList[i] : null);
    },

    getPaidAmount : function()
    {
        return this._paidAmount;
    },

    getDeposit : function()
    {
        return this._deposit;
    },

    getDefaultAdvanceAmount : function()
    {
        return LOC_Common.round(this._tabCfgs.proformaAdvancePercent * this._documentObj.getTotal() / 100, 2);
    },

    getFuelUnitPrice : function()
    {
        return this._tabCfgs.fuelUnitPrice;
    },

    isAddPossible : function()
    {
        // Si aucune action n'est en cours, que l'utilisateur à les droits et que cette action n'est pas désactivée,
        // alors il est possible d'ajouter une pro forma
        return (!this._tabAction && this.hasRight('add') &&
                this.getAuthorizedTypes().length > 0 ? true : false);
    },

    isEditable : function(id)
    {
        // Si aucune action n'est en cours, que l'utilisateur à les droits et que cette action n'est pas désactivée,
        // alors il est possible d'éditer cette pro forma
        return (!this._tabAction && this.hasRight('edit') && (tab = this.getInfos(id)) &&
                !(this._tabExtParams.disabledComponents.editable &&
                  (this._tabExtParams.disabledComponents.editable.length == 0 ||
                   in_array(tab.typeId, this._tabExtParams.disabledComponents.editable))) &&
                tab.isEditable ? true : false);
    },

    isReactivable : function(id)
    {
        // Si aucune action n'est en cours, que l'utilisateur à les droits et que cette action n'est pas désactivée,
        // alors il est possible de réactiver cette pro forma
        return (!this._tabAction && this.hasRight('reactivate') && (tab = this.getInfos(id)) &&
                !(this._tabExtParams.disabledComponents.reactivate &&
                  (this._tabExtParams.disabledComponents.reactivate.length == 0 ||
                   in_array(tab.typeId, this._tabExtParams.disabledComponents.reactivate))) &&
                tab.isReactivable ? true : false);
    },

    isAbortable : function(id)
    {
        // Si aucune action n'est en cours, que l'utilisateur à les droits et que cette action n'est pas désactivée,
        // alors il est possible d'abandonner cette pro forma
        return (!this._tabAction && this.hasRight('abort') && (tab = this.getInfos(id)) &&
                !(this._tabExtParams.disabledComponents.abortable &&
                  (this._tabExtParams.disabledComponents.abortable.length == 0 ||
                   in_array(tab.typeId, this._tabExtParams.disabledComponents.abortable))) &&
                tab.isAbortable ? true : false);
    },

    /**
     * Ajouter une pro forma
     *
     * @param
     */
    add : function(typeId, advanceAmount, paidAmount, deposit, paymentModeId, endDate, fuelQuantity, comment,
                   isVatInvoiced)
    {
        if (!this.isAddPossible())
        {
            return false;
        }
        this._tabAction = {
                type : 'add',
                id : null,
                tab : {
                    'type.id': Number(typeId),
                    'amount': Number(advanceAmount),
                    'paidAmount': Number(paidAmount),
                    'deposit': Number(deposit),
                    'paymentMode.id': Number(paymentModeId),
                    'endDate': endDate,
                    'fuelQuantity': Number(fuelQuantity),
                    'comment': (comment || ''),
                    'isVatInvoiced': (isVatInvoiced ? true : false)
                }
        };

        if (this.onchange)
        {
            this.onchange(['added']);
        }
        return true;
    },

    edit : function(id, advanceAmount, paidAmount, deposit, paymentModeId, fuelQuantity, comment, isVatInvoiced)
    {
        if (!this.isEditable(id))
        {
            return false;
        }
        this._tabAction = {
                type : 'edit',
                id : id,
                tab : {
                    'paidAmount': Number(paidAmount),
                    'amount': Number(advanceAmount),
                    'deposit': Number(deposit),
                    'paymentMode.id': Number(paymentModeId),
                    'fuelQuantity': Number(fuelQuantity),
                    'comment': (comment || ''),
                    'isVatInvoiced': (isVatInvoiced ? true : false)
                }
        };

        if (this.onchange)
        {
            this.onchange(['edited']);
        }
    },

    undo : function(id)
    {
        if (!this._tabAction)
        {
            return false;
        }
        this._tabAction = null;

        if (this.onchange)
        {
            this.onchange(['undo']);
        }
        return true;
    },

    reactivate : function(id)
    {
        if (!this.isReactivable(id))
        {
            return false;
        }
        this._tabAction = {type: 'reactivate', id: tab.id};

        if (this.onchange)
        {
            this.onchange(['reactived']);
        }
        return true;
    },

    abort : function(id)
    {
        if (!this.isAbortable(id))
        {
            return false;
        }
        this._tabAction = {type: 'abort', id: tab.id};

        if (this.onchange)
        {
            this.onchange(['aborted']);
        }
        return true;
    },

    getAction : function()
    {
        return this._tabAction;
    },

    getDefaultFuelQuantity : function()
    {
        return this._tabExtParams.fuelQtyEstimByDay * this._tabExtParams.duration;
    },

    getMaxFuelQuantity : function()
    {
    	return this._tabExtParams.fuelQtyMax;
    },

    /**
     * Récupérer un paramètre externe
     *
     * @param string name
     * @return mixed
     */
    getExternalParamValue : function(name)
    {
        return this._tabExtParams[name];
    },

    /**
     * Définir un ou plusieurs paramètres externes
     *
     * @param Object tab Paramètres à définir
     * @return bool
     */
    setExternalParams : function(tab)
    {
        var tabChanges = [];
        for (var name in tab)
        {
            if (this._tabExtParams.hasOwnProperty(name))
            {
                if (!LOC_Common.isEqual(tab[name], this._tabExtParams[name]))
                {
                    this._tabExtParams[name] = tab[name];
                    tabChanges.push('extParam_' + name);
                }
            }
        }

        if (tabChanges.length > 0 && this.onchange)
        {
            this.onchange(tabChanges);
        }
        return (tabChanges.length > 0);
    },

    /**
     * Vérifier le droit à un champ
     *
     * @return bool
     */
    hasRight : function(fieldName)
    {
        return (this._tabRights === null || this._tabRights[fieldName] ? true : false);
    },

    /**
     * Compte le nombre de proforma actives
     * array tabFilters - tableau de filtres
     *
     * @return int
     */
    getActiveCount : function(tabFilters)
    {
        var count = 0;
        for (i = 0; i < this._tabList.length; i++)
        {
            // critères :
            // - pro forma active ou en cours de réactivation
            // - aucun filtre
            // - si on a des filtres
            // -- pas de filtre sur le type OU pro forma du type filtré
            // -- pas de filtre sur l'état de finalisation OU pro forma dans l'état de finalisation filtré
            // -- pas de filtre sur l'état d'expiration OU pro forma dans l'état d'expiration filtré

            if ((this._tabList[i].state == 'active' ||
                        (this._tabAction && this._tabAction.id == this._tabList[i].id && this._tabAction.type == 'reactivate'))
                && (!tabFilters ||
                        (tabFilters &&
                                (!tabFilters.type || (tabFilters && tabFilters.type && this._tabList[i].typeId == tabFilters.type)) &&
                                (tabFilters.isFinalized == null || this._tabList[i].isFinalized == tabFilters.isFinalized) &&
                                (tabFilters.isExpired == null || this._tabList[i].isExpired == tabFilters.isExpired))))
            {
                count++;
            }
        }
        // critères supplémentaires
        // - une action pro forma en cours de type "ajout"
        // - aucun filtre
        // - si on a un filtre sur le type et pas sur la finalisation (ou filtre sur non finalisée) : type de pro forma filtré
        if (this._tabAction &&
                this._tabAction.type == 'add' &&
                (!tabFilters ||
                        (tabFilters.type && this._tabAction.tab['type.id'] == tabFilters.type && !tabFilters.isFinalized)))
        {
            count++;
        }
        return count;
    }
}



Location.Rent_Helper_ProformaView = {

    _tabCaches : {'types': {}, 'paymentModes': {}},
    _listClone : null,

    display : function(pfx, obj)
    {
        var mySelf = this;
        var tHeadEl = $ge(pfx + 'list').tHead;
        var tBodyEl = $ge(pfx + 'list').tBodies[0];
        var tFootEl = $ge(pfx + 'list').tFoot;


        // Liste des messages à afficher
        var tabMsgs = obj.getExternalParamValue('displayMessages');
        var tabClasses = [];
        for (var i = 0; i < tabMsgs.length; i++)
        {
            tabClasses.push('msg_' + tabMsgs[i]);
        }
        $ge(pfx + 'infoError').className = tabClasses.join(' ');

        // Suppression des lignes
        while (tBodyEl.rows.length > 0)
        {
            Location.currenciesManager.deleteCurrency(pfx + 'amount[' + tBodyEl.rows[0].getAttribute('data-id') + ']');
            Location.currenciesManager.deleteCurrency(pfx + 'amountWithVat[' + tBodyEl.rows[0].getAttribute('data-id') + ']');
            Location.currenciesManager.deleteCurrency(pfx + 'total[' + tBodyEl.rows[0].getAttribute('data-id') + ']');
            Location.currenciesManager.deleteCurrency(pfx + 'cashed[' + tBodyEl.rows[0].getAttribute('data-id') + ']');
            Location.currenciesManager.deleteCurrency(pfx + 'remainDue[' + tBodyEl.rows[0].getAttribute('data-id') + ']');
            tBodyEl.deleteRow(0);
        }

        var tabAction = obj.getAction();

        // Création des lignes
        var tab = obj.getList();
        for (var i = 0; i < tab.length; i++)
        {
            var rEl, cEl, el, id = tab[i].id;

            rEl = tBodyEl.insertRow(-1);

            var isUpdated = (tabAction && tabAction.id == id);
            var isAbortable = obj.isAbortable(id);
            var isReactivable = obj.isReactivable(id);
            var isEditable = obj.isEditable(id);

            rEl.className = 'type_' + tab[i].typeId + ' editable state_' + tab[i].state +
                            ' encashment_' + tab[i].encashmentId +
                            (isAbortable ? ' abortable' : '') +
                            (isReactivable ? ' reactivable' : '') +
                            (isUpdated ? ' updated action_' + tabAction.type : '') +
                            (tab[i].isExpired ? ' expired' : '') +
                            (i % 2 == 0 ? ' even' : '');
            rEl.setAttribute('data-id', id);

            cEl = rEl.insertCell(-1);
            cEl.className = 'icon';
            cEl.title = LOC_View.getTranslation(pfx + 'state_' + tab[i].state) +
                        (tab[i].isExpired ? ', ' + LOC_View.getTranslation(pfx + 'expired') : '') +
                        (isUpdated ? ' [' + LOC_View.getTranslation(pfx + 'action_' + tabAction.type) + ']' : '');
            cEl.appendChild(window.document.createElement('DIV'));

            cEl = rEl.insertCell(-1);
            cEl.className = 'id';
            cEl.title = tab[i].date;
            cEl.innerHTML = tab[i].id;

            cEl = rEl.insertCell(-1);
            cEl.className = 'type';
            el = window.document.createElement('DIV');
            el.innerHTML = tab[i].typeLabel;
            cEl.appendChild(el);

            cEl = rEl.insertCell(-1);
            cEl.className = 'paiement';
            cEl.innerHTML = tab[i].paymentModeLabel;

            cEl = rEl.insertCell(-1);
            cEl.className = 'amount';
            el = window.document.createElement('INPUT');
            el.type = 'hidden';
            el.id = pfx + 'amount[' + id + ']';
            cEl.appendChild(el);
            el = Location.currenciesManager.cloneCurrency(el.id, pfx + 'amount_clone');
            el.setValue(tab[i].amount);

            cEl = rEl.insertCell(-1);
            cEl.className = 'amountWithVat';
            el = window.document.createElement('INPUT');
            el.type = 'hidden';
            el.id = pfx + 'amountWithVat[' + id + ']';
            cEl.appendChild(el);
            el = Location.currenciesManager.cloneCurrency(el.id, pfx + 'amountWithVat_clone');
            el.setValue(tab[i].amountWithVat);

            cEl = rEl.insertCell(-1);
            cEl.className = 'total';
            el = window.document.createElement('INPUT');
            el.type = 'hidden';
            el.id = pfx + 'total[' + id + ']';
            cEl.appendChild(el);
            el = Location.currenciesManager.cloneCurrency(el.id, pfx + 'total_clone');
            el.setValue(tab[i].total);

            cEl = rEl.insertCell(-1);
            cEl.className = 'cashed';
            el = window.document.createElement('INPUT');
            el.type = 'hidden';
            el.id = pfx + 'cashed[' + id + ']';
            cEl.appendChild(el);
            el = Location.currenciesManager.cloneCurrency(el.id, pfx + 'cashed_clone');
            el.setValue(tab[i].cashed);

            cEl = rEl.insertCell(-1);
            cEl.className = 'remainDue';
            el = window.document.createElement('INPUT');
            el.type = 'hidden';
            el.id = pfx + 'remainDue[' + id + ']';
            cEl.appendChild(el);
            el = Location.currenciesManager.cloneCurrency(el.id, pfx + 'remainDue_clone');
            el.setValue(tab[i].remainDue);

            cEl = rEl.insertCell(-1);
            cEl.className = 'beginDate';
            cEl.innerHTML = tab[i].beginDate;

            if (tHeadEl.rows[0].cells.length > 8)
            {
                cEl = rEl.insertCell(-1);
                cEl.className = 'justified' + (tab[i].isJustified ? ' yes' : '');

                cEl = rEl.insertCell(-1);
                cEl.className = 'validated' + (tab[i].isValidated ? ' yes' : '');

                cEl = rEl.insertCell(-1);
                cEl.className = 'encashment';
                cEl.innerHTML = tab[i].encashment;
            }

            cEl = rEl.insertCell(-1);

            if (!isUpdated)
            {
                el = window.document.createElement('A');
                el.href = '#';
                el.className = (isEditable ? 'edit' : 'view');
                el.title = LOC_View.getTranslation(pfx + 'button_' + el.className);
                el.onclick = function()
                {
                    mySelf._showEditPopup(pfx, obj, this.parentNode.parentNode.getAttribute('data-id'),
                                          (this.className == 'edit'));
                    return false;
                }
                cEl.appendChild(el);
            }

            if (isAbortable)
            {
                el = window.document.createElement('A');
                el.href = '#';
                el.className = 'abort';
                el.title = LOC_View.getTranslation(pfx + 'button_abort');
                el.onclick = function()
                {
                    obj.abort(this.parentNode.parentNode.getAttribute('data-id'));
                    return false;
                }
                cEl.appendChild(el);
            }
            if (isReactivable)
            {
                el = window.document.createElement('A');
                el.href = '#';
                el.className = 'reactivate';
                el.title = LOC_View.getTranslation(pfx + 'button_reactivate');
                el.onclick = function()
                {
                    obj.reactivate(this.parentNode.parentNode.getAttribute('data-id'));
                    return false;
                }
                cEl.appendChild(el);
            }
            if (isUpdated)
            {
                el = window.document.createElement('A');
                el.href = '#';
                el.className = 'undo';
                el.title = LOC_View.getTranslation(pfx + 'button_undo');
                el.onclick = function()
                {
                    obj.undo();
                    return false;
                }
                cEl.appendChild(el);
            }
            cEl.className = 'ctrls';

            cEl = rEl.insertCell(-1);
            cEl.className = 'editCtrls';
            // Pas de RE en attente ET
            //     - imprimable avec le droit print
            //     - OU non imprimable avec le droit forcePrint
            if (!tab[i].isPendingSpcReducsExists &&
                    (tab[i].isPrintable && obj.hasRight('print') ||
                     !tab[i].isPrintable && obj.hasRight('forcePrint')))
            {
                // Clonage des boutons d'impression
                var printBtns = $ge(pfx + 'printBtns').cloneNode(true);
                printBtns.removeAttribute('id');
                var tabElts = printBtns.getElementsByTagName('a');
                for (var j = 0; j < tabElts.length; j++)
                {
                    tabElts[j].removeAttribute('id');
                    tabElts[j].href = tab[i].tabUrls[tabElts[j].className];
                }
                cEl.appendChild(printBtns);
            }
        }

        if (tab.length == 0)
        {
            tFootEl.style.display = 'table-footer-group';
        }

        // Ligne d'ajout
        var tBodyEl = $ge(pfx + 'list').tBodies[1];
        if (tabAction && tabAction.type == 'add')
        {
            tBodyEl.style.display = '';
            tFootEl.style.display = 'none';

            var rEl = tBodyEl.rows[0];
            rEl.className = 'type_' + tabAction.tab['type.id'];
            rEl.cells[2].firstChild.innerHTML = this._tabCaches.types[tabAction.tab['type.id']];
            rEl.cells[3].innerHTML = this._tabCaches.paymentModes[tabAction.tab['paymentMode.id']];
            rEl.cells[5].firstChild.onclick = function() { obj.undo(); return false; };
        }
        else
        {
            tBodyEl.style.display = 'none';
        }

        $ge(pfx + 'addPopupBtn').className = 'locCtrlButton' + (obj.isAddPossible() ? '' : ' disabled');
    },

    initEvents : function(pfx, obj)
    {
        var mySelf = this;

        $ge(pfx + 'expandBtn').onclick = function()
        {
            el = $ge(pfx + 'list');
            el.className = (el.className == 'basic' ? 'basic light' : 'basic');
            return false;
        }

        $ge(pfx + 'addPopupBtn').onclick = function()
        {
            mySelf._showAddPopup(pfx, obj);
            return false;
        }
    },

    _showEditPopup : function(pfx, obj, pfmId, isEdit)
    {
        var mySelf = this;
        pfx += 'edit';

        $ge(pfx + 'Popup').className = 'proformaHlp_popup' + (!isEdit ? ' readonly' : '');

        var tab = obj.getInfos(pfmId);
        $ge(pfx + 'TypeLabel').innerHTML  = tab.typeLabel;
        // - Montant de l'acompte
        var el = Location.currenciesManager.getCurrency(pfx + 'AdvanceAmt');
        el.getSpanElement().parentNode.parentNode.style.display = (tab.typeId != PFMTYPE_ID_ADVANCE ? 'none' : 'table-row');
        el.setValue(tab.amount);
        el.setDisabled(!isEdit);
        // - Montant déjà versé
        var el = Location.currenciesManager.getCurrency(pfx + 'PaidAmt');
        el.getSpanElement().parentNode.parentNode.style.display = (tab.typeId == PFMTYPE_ID_ADVANCE ? 'none' : 'table-row');
        el.setValue(tab.paidAmount);
        el.setDisabled(!isEdit);
        el.getElement().className = '';
        // - Dépôt de garantie
        el = Location.currenciesManager.getCurrency(pfx + 'Deposit');
        el.getSpanElement().parentNode.parentNode.style.display = (tab.typeId == PFMTYPE_ID_ADVANCE ||
                tab.typeId == PFMTYPE_ID_EXTENSION ? 'none' : 'table-row');
        el.setValue(tab.deposit);
        el.setDisabled(!isEdit);
        // - Mode de règlement
        el = $ge(pfx + 'PmtModeId');
        el.value = tab.paymentModeId;
        el.disabled = !isEdit;
        // - Date de fin souhaitée
        el = $ge(pfx + 'EndDate');
        if (el)
        {
            el.parentNode.parentNode.style.display = (tab.typeId != PFMTYPE_ID_EXTENSION ? 'none' : 'table-row');
            el.innerHTML = tab.endDate;
            el.disabled = !isEdit;
        }
        // - Estimation carburant
        var fuelEl = Location.spinControlsManager.getSpinControl(pfx + 'FuelQty');
        fuelEl.getSpanElement().parentNode.parentNode.style.display = (tab.typeId == PFMTYPE_ID_ADVANCE ? 'none' : 'table-row');
        var maxQty = obj.getMaxFuelQuantity();
        fuelEl.setMaximumValue(tab.fuelQuantity > maxQty ? tab.fuelQuantity : maxQty);
        fuelEl.setValue(tab.fuelQuantity);
        fuelEl.setDisabled(!obj.getExternalParamValue('isFuelNeeded'));
        fuelEl.setReadOnly(!isEdit);

        el = $ge(pfx + 'FuelQtyEstimByDay');
        el.parentNode.style.display = (isEdit ? '' : 'none');
        if (isEdit)
        {
            el.parentNode.style.display = (obj.getExternalParamValue('isFuelNeeded') ? '' : 'none');
            el.innerHTML = obj.getExternalParamValue('fuelQtyEstimByDay');
            el = $ge(pfx + 'FuelQtyMax');
            el.innerHTML = obj.getExternalParamValue('fuelQtyMax');
        }

        // - Calculette contrat cadre
        $ge(pfx + 'Calc').style.visibility = (obj.getExternalParamValue('isCalcDisplayed') && !fuelEl.isDisabled() ? 'visible': 'hidden');
        // - Commentaire
        el = $ge(pfx + 'Comment');
        el.value = (tab.comment || '');
        el.readOnly = !isEdit;
        // - TVA Facturée
        el = $ge(pfx + 'IsVat');
        el.checked = (tab.isVatInvoiced ? true : false);
        el.disabled = !isEdit;
        // - Justificatif
        el = $ge(pfx + 'Justification');
        el.childNodes[0].style.display = (!tab.isJustified ? '' : 'none');
        el.childNodes[1].style.display = (tab.isJustified ? '' : 'none');
        // - Validation par agence
        el = $ge(pfx + 'Validation');
        el.childNodes[0].style.display = (!tab.isValidated ? '' : 'none');
        el.childNodes[1].style.display = (tab.isValidated ? '' : 'none');
        el = $ge(pfx + 'ValidComment');
        el.style.display = (tab.validComment ? '' : 'none');
        el.setAttribute('data-text', tab.validComment);
        // - Encaissement
        $ge(pfx + 'Encashment').innerHTML = tab.encashment;

        Location.modalWindowManager.show(pfx + 'Popup', {contentType: 3, width: 450, height: 'auto'});

        // Calculette pour les contrats cadres
        el = $ge(pfx + 'Calc');
        if (el)
        {
            el.onclick = function()
            {
                var fuelQuantity = Location.spinControlsManager.getSpinControl(pfx + 'FuelQty').getValue();
                if (fuelQuantity !== null && fuelQuantity > 0)
                {
                    var customerObj = obj.getCustomerObject();
                    var fwCttFuelUnitPrice = customerObj.getFwkCttFuelUnitPrice();
                    var fuelUnitPrice = obj.getFuelUnitPrice();
                    if (fwCttFuelUnitPrice != fuelUnitPrice)
                    {
                        var fuelQty = LOC_Common.round(fuelQuantity * fwCttFuelUnitPrice / fuelUnitPrice);
                        Location.spinControlsManager.getSpinControl(pfx + 'FuelQty').setValue(fuelQty);
                    }
                }
            }
        }

        $ge(pfx + 'Btn').onclick = function()
        {
            var advanceAmount = Location.currenciesManager.getCurrency(pfx + 'AdvanceAmt').getValue();
            $ge(pfx + 'AdvanceAmt').className = '';
            if (tab.typeId == PFMTYPE_ID_ADVANCE && advanceAmount < 1)
            {
                $ge(pfx + 'AdvanceAmt').className = 'error';
                return false;
            }
            var paidAmount = Location.currenciesManager.getCurrency(pfx + 'PaidAmt').getValue();
            $ge(pfx + 'PaidAmt').className = '';
            if ((tab.typeId == PFMTYPE_ID_PROFORMA && paidAmount < obj.getPaidAmount()) ||
                (tab.typeId == PFMTYPE_ID_EXTENSION && paidAmount < 0))
            {
                $ge(pfx + 'PaidAmt').className = 'error';
                return false;
            }
            var deposit = Location.currenciesManager.getCurrency(pfx + 'Deposit').getValue();
            el = $ge(pfx + 'PmtModeId');
            el = el.options[el.selectedIndex];
            var paymentModeId = el.value;
            mySelf._tabCaches.paymentModes[paymentModeId] = el.innerHTML;
            var fuelQty = Location.spinControlsManager.getSpinControl(pfx + 'FuelQty').getValue();
            var comment = ($ge(pfx + 'Comment').value || '');
            var isVatInvoiced = $ge(pfx + 'IsVat').checked;

            obj.edit(tab.id, advanceAmount, paidAmount, deposit, paymentModeId, fuelQty, comment, isVatInvoiced);

            Location.modalWindowManager.hide(pfx + 'Popup');
            return false;
        }
        return true;
    },

    _showAddPopup : function(pfx, obj)
    {
        if (!obj.isAddPossible())
        {
            return false;
        }
        var mySelf = this;
        var customerObj = obj.getCustomerObject();
        pfx += 'add';

        var updateFields = function(obj, typeId)
        {
            var el, isDisabled;
            // - Montant de l'acompte
            el = Location.currenciesManager.getCurrency(pfx + 'AdvanceAmt');
            isDisabled = (!typeId || typeId != PFMTYPE_ID_ADVANCE);
            el.setValue(isDisabled ? 0 : obj.getDefaultAdvanceAmount());
            el.setDisabled(isDisabled);
            // - Montant déjà versé
            el = Location.currenciesManager.getCurrency(pfx + 'PaidAmt');
            isDisabled = (!typeId || typeId == PFMTYPE_ID_ADVANCE);
            el.setValue(isDisabled || typeId == PFMTYPE_ID_EXTENSION ? 0  : obj.getPaidAmount());
            el.setDisabled(isDisabled);
            el.getElement().className = '';
            // - Dépôt de garantie
            el = Location.currenciesManager.getCurrency(pfx + 'Deposit');
            el.setValue((!typeId || typeId == PFMTYPE_ID_ADVANCE || typeId == PFMTYPE_ID_EXTENSION) ? 0 : obj.getDeposit());
            el.setDisabled(!typeId || typeId == PFMTYPE_ID_ADVANCE || typeId == PFMTYPE_ID_EXTENSION);
            // - Mode de règlement
            el = $ge(pfx + 'PmtModeId');
            var paymentModeId = customerObj.getPaymentId();
            var i = 0;
            while (i < el.options.length && el.options[i].value != paymentModeId)
            {
                i++;
            }
            el.selectedIndex = (i < el.options.length ? i : 0);
            el.disabled = !typeId;
            el.className = '';
            // - Date de fin souhaitée
            el = $ge(pfx + 'EndDate');
            if (el)
            {
                el.value = '';
                setMaskedInputValue(pfx + 'EndDate', '');
                el = $ge(pfx + 'EndDate.masked');
                el.disabled = (typeId != PFMTYPE_ID_EXTENSION);
                el.className = '';

                $ge(pfx + 'EndDateDoc').innerHTML = LOC_View.getDateFormat(obj.getExternalParamValue('endDateDoc'));
                if (obj.getExternalParamValue('maxExtensionEndDate'))
                {
                    $ge(pfx + 'MaxDate').style.display = 'block';
                    $ge(pfx + 'MaxEndDate').innerHTML = LOC_View.getDateFormat(obj.getExternalParamValue('maxExtensionEndDate'));
                }
            }

            // - Estimation carburant
            var fuelEl = Location.spinControlsManager.getSpinControl(pfx + 'FuelQty');
            fuelEl.setMaximumValue(obj.getMaxFuelQuantity());
            fuelEl.setValue((!typeId || typeId == PFMTYPE_ID_EXTENSION ||
                    typeId == PFMTYPE_ID_ADVANCE ? 0 : obj.getDefaultFuelQuantity()));
            fuelEl.setDisabled(!typeId || !obj.getExternalParamValue('isFuelNeeded') || typeId == PFMTYPE_ID_ADVANCE);
            el = $ge(pfx + 'FuelQtyEstimByDay');
            el.parentNode.style.display = (obj.getExternalParamValue('isFuelNeeded') ? '' : 'none');
            el.innerHTML = obj.getExternalParamValue('fuelQtyEstimByDay');
            el = $ge(pfx + 'FuelQtyMax');
            el.innerHTML = obj.getExternalParamValue('fuelQtyMax');
            // - Calculette contrat cadre
            $ge(pfx + 'Calc').style.visibility = (obj.getExternalParamValue('isCalcDisplayed') && !fuelEl.isDisabled()  ? 'visible': 'hidden');
            // - Warning estimation carburant pour les prolongation
            $ge(pfx + 'WarningFuelEstimationExtension').style.visibility = (obj.getExternalParamValue('isFuelNeeded') &&
                    typeId == PFMTYPE_ID_EXTENSION ? 'visible': 'hidden');
            // - Commentaire
            $ge(pfx + 'Comment').disabled = !typeId;
            // - TVA Facturée
            $ge(pfx + 'IsVat').disabled = !typeId;
        }

        // Initialisation de l'affichage
        // - Type
        var el = $ge(pfx + 'TypeId');
        if (!mySelf._listClone)
        {
            mySelf._listClone = el.cloneNode(true);
        }
        while (el.options[0])
        {
            el.removeChild(el.options[0]);
        }
        var tabAuthorizedTypes = obj.getAuthorizedTypes();
        for (var i = 0; i < mySelf._listClone.options.length; i++)
        {
            if ((in_array(mySelf._listClone.options[i].value, tabAuthorizedTypes) || mySelf._listClone.options[i].value == 0)
                    && !(customerObj.isProformaRequired() &&
                         mySelf._listClone.options[i].value == PFMTYPE_ID_ADVANCE))
            {
                el.appendChild(mySelf._listClone.options[i].cloneNode(true));
            }
        }
        el.selectedIndex = 0;
        el.className = '';
        el.onchange = function()
        {
            updateFields(obj, this.value * 1);
        }

        // Calculette pour les contrats cadres
        el = $ge(pfx + 'Calc');
        if (el)
        {
            el.onclick = function()
            {
                var fuelQuantity = Location.spinControlsManager.getSpinControl(pfx + 'FuelQty').getValue();
                if (fuelQuantity !== null && fuelQuantity > 0)
                {
                    var customerObj = obj.getCustomerObject();
                    var fwCttFuelUnitPrice = customerObj.getFwkCttFuelUnitPrice();
                    var fuelUnitPrice = obj.getFuelUnitPrice();
                    if (fwCttFuelUnitPrice != fuelUnitPrice)
                    {
                        var fuelQty = LOC_Common.round(fuelQuantity * fwCttFuelUnitPrice / fuelUnitPrice);
                        Location.spinControlsManager.getSpinControl(pfx + 'FuelQty').setValue(fuelQty);
                    }
                }
            }
        }

        // Commentaire
        $ge(pfx + 'Comment').value = '';
        // TVA Facturée
        $ge(pfx + 'IsVat').checked = customerObj.isVatInvoiced();

        updateFields(obj);

        Location.modalWindowManager.show(pfx + 'Popup', {contentType: 3, width: 450, height: 'auto'});

        $ge(pfx + 'Btn').onclick = function()
        {
            var el = $ge(pfx + 'TypeId');
            el = el.options[el.selectedIndex];
            var typeId = el.value;
            mySelf._tabCaches.types[typeId] = el.innerHTML;
            var advanceAmount = Location.currenciesManager.getCurrency(pfx + 'AdvanceAmt').getValue();
            var paidAmount = Location.currenciesManager.getCurrency(pfx + 'PaidAmt').getValue();
            var hasErrors = false;
            $ge(pfx + 'TypeId').className = '';
            $ge(pfx + 'AdvanceAmt').className = '';
            $ge(pfx + 'PaidAmt').className = '';
            $ge(pfx + 'PmtModeId').className = '';
            if ($ge(pfx + 'TypeId').value == 0)
            {
                $ge(pfx + 'TypeId').className = 'error';
                hasErrors = true;
            }
            else
            {
                if (typeId == PFMTYPE_ID_ADVANCE && advanceAmount < 1)
                {
                    $ge(pfx + 'AdvanceAmt').className = 'error';
                    hasErrors = true;
                }
                if ((typeId == PFMTYPE_ID_PROFORMA && paidAmount < obj.getPaidAmount()) ||
                        (typeId == PFMTYPE_ID_EXTENSION && paidAmount < 0))
                {
                    $ge(pfx + 'PaidAmt').className = 'error';
                    hasErrors = true;
                }
                if ($ge(pfx + 'PmtModeId').value == 0)
                {
                    $ge(pfx + 'PmtModeId').className = 'error';
                    hasErrors = true;
                }
                if (typeId == PFMTYPE_ID_EXTENSION && $ge(pfx + 'EndDate').value == '')
                {
                    $ge(pfx + 'EndDate.masked').className = 'error';
                    hasErrors = true;
                }
                if (typeId == PFMTYPE_ID_EXTENSION && obj.getExternalParamValue('maxExtensionEndDate') &&
                        $ge(pfx + 'EndDate').value > obj.getExternalParamValue('maxExtensionEndDate'))
                {
                    $ge(pfx + 'EndDate.masked').className = 'error';
                    hasErrors = true;
                }
            }
            if (hasErrors)
            {
                return false;
            }
            var deposit = Location.currenciesManager.getCurrency(pfx + 'Deposit').getValue();
            el = $ge(pfx + 'PmtModeId');
            el = el.options[el.selectedIndex];
            var paymentModeId = el.value;
            mySelf._tabCaches.paymentModes[paymentModeId] = el.innerHTML;
            el = $ge(pfx + 'EndDate');
            var endDate = (el ? el.value : null);
            var fuelQty = Location.spinControlsManager.getSpinControl(pfx + 'FuelQty').getValue();
            var comment = ($ge(pfx + 'Comment').value || '');
            var isVatInvoiced = $ge(pfx + 'IsVat').checked;

            obj.add(typeId, advanceAmount, paidAmount, deposit, paymentModeId, endDate, fuelQty, comment, isVatInvoiced);

            Location.modalWindowManager.hide(pfx + 'Popup');
            return false;
        }
        return true;
    }
}