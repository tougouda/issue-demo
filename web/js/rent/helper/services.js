if (!window.Location)
{
    var Location = new Object();
}


/**
 * Classe de gestion des autres services
 *
 * @param Object documentObj Objet javascript du document (ligne de devis ou contrat)
 * @param Object tabCfgs     Configurations globales
 */
Location.Rent_Helper_Services = function(documentObj, tabCfgs)
{
    this._documentObj = documentObj;
    this._tabCfgs = tabCfgs;

    this._insuranceTypeId = null;
    this._insuranceRate   = null;

    this._tabInsuranceTypesList = tabCfgs.tabInsuranceTypesList;

    this._appealTypeId    = null;
    this._appealValue     = null;

    this._tabAppealTypesList = tabCfgs.tabAppealTypesList;

    this._cleaningTypeId   = 0;
    this._cleaningAmount   = 0;
    this._tabCleaningTypes = [];

    this._residuesMode      = null;
    this._residuesInitMode  = null;
    this._residuesValue     = null;
    this._residuesInitValue = null;

    this._fuelQuantity  = null;
    this._fuelUnitPrice = 0;
    this._fuelCurrentUnitPrice = 0;
    this._tabFuelEstimation = {quantity : null, unitPrice : 0, amount : 0};

    this._tabRentServices      = [];
    this._oldServicesAmount    = 0;
    this._rentServiceMinAmount = tabCfgs.rentServiceMinAmount;

    this._tabExtParams = {
        isFuelNeeded: false,
        isProformaRequired: false,
        isProformaActived: false,
        rentDayPrice: 0,
        totalDays: 0,
        duration: 0,
        calendarDuration: 0,
        isFullService: false,
        isModelAccessory: false,
        customerInsurance: null,
        customerAppeal: null,
        isCustomerResiduesInvoiced: null,
        residuesBaseAmount: 0,
        isCalcDisplayed: false,
        disabledComponents: {}
    };

    this._tabRights = null;

    this.onchange = null;
    this.onload   = null;
}


Location.Rent_Helper_Services.prototype = {

    /**
     * Ajouter une ligne de service loc/transport
     */
    addRentService: function(tab)
    {
        if (!this.isAddRentServicesPossible())
        {
            return false;
        }
        this._tabRentServices.push({
            id : 0,
            amount : Number(tab.amount),
            comment : tab.comment || '',
            'rentService.id' : tab['rentService.id'],
            'rentService.name' : tab['rentService.name'] || '',
            'state.id' : RENTSRV_STATE_UNBILLED,
            'state.label' : '',
            isRemovable : 1
        });

        if (this.onchange)
        {
            this.onchange(['addRentService']);
        }
        return true;
    },

    editRentService : function(id, tab)
    {
        if (!this.isEditRentServicesPossible(id))
        {
            return false;
        }

        var tabChanges = [];
        for (var prop in tab)
        {
            // Vérification du montant : si le montant est supérieur au minimum autorisé pour l'utilisateur
            if (prop == 'amount')
            {
                var amount = tab.amount;
                if (this._rentServiceMinAmount != null &&
                        amount < Number(this._rentServiceMinAmount) &&
                        amount != Number(this._tabRentServices[id]['amount']))
                {
                    tab.amount = Number(this._tabRentServices[id]['amount']);
                    throw 'minAmountError';
                }
            }
            if (this._tabRentServices[id][prop] != tab[prop])
            {
                this._tabRentServices[id][prop] = tab[prop];
                tabChanges.push(prop);
            }
        }

        if (tabChanges.length == 0)
        {
            return false;
        }

        if (this.onchange)
        {
            this.onchange(['editRentService']);
        }
    },

    /**
     * Copier les informations d'une ligne vers la courante
     *
     * @param Location.RentEstimateLine lineObj
     * @return bool
     */
    copy : function(serviceObj)
    {
        var tabProps = ['tabExtParams', 'insuranceTypeId', 'insuranceRate',
                        'appealTypeId', 'cleaningTypeId', 'cleaningAmount', 'tabCleaningTypes',
                        'residuesMode', 'fuelCurrentUnitPrice'];

        var i, prop;
        var nb = tabProps.length;
        for (i = 0; i < nb; i++)
        {
            prop = '_' + tabProps[i];
            this[prop] = LOC_Common.clone(serviceObj[prop]);
        }
        this._fuelUnitPrice = this._fuelCurrentUnitPrice;


        // Vérification de l'assurance de la ligne de devis par rapport à l'assurance actuelle du client
        var customerInsurance = this.getExternalParamValue('customerInsurance');
        if (customerInsurance !== null)
        {
            var insuranceTypeInfos = this.getInsuranceTypeInfos();
            if ((customerInsurance.rate !== null && customerInsurance.rate != this._tabCfgs.defaultInsuranceRate) ||
                (insuranceTypeInfos && insuranceTypeInfos.isInvoiced &&
                 this._insuranceRate !== null && this._insuranceRate != this._tabCfgs.defaultInsuranceRate))
            {
                this._insuranceTypeId = null;
                this._insuranceRate = null;
            }
        }

        // Vérification de la participation au recyclage
        if (this._residuesMode !== null && this._residuesMode != RESIDUES_MODE_NONE)
        {
            this._residuesMode = this._tabCfgs.defaultResiduesMode;
            this._residuesValue = null;
        }

        // Services de loc et transport
        var tabRentServices = serviceObj.getRentServices();
        for (var i = 0; i < tabRentServices.length; i++)
        {
            if (tabRentServices[i]['state.id'] != RENTSRV_STATE_DELETED)
            {
                this.addRentService(tabRentServices[i]);
            }
        }

        return true;
    },

    /**
     * Chargement de données
     *
     */
    load : function(tab, tabCleaningTypes, tabRentServices, oldServicesAmount, tabExtParams, tabRights)
    {
        this._insuranceTypeId = (tab.insuranceTypeId === null ? null : Number(tab.insuranceTypeId));
        this._insuranceRate = (tab.insuranceRate ? Number(tab.insuranceRate) : null);

        this._appealTypeId = (tab.appealTypeId === null ? null : Number(tab.appealTypeId));

        this._cleaningTypeId = Number(tab.cleaningTypeId);
        this._cleaningAmount = Number(tab.cleaningAmount);

        this._residuesMode  = (tab.residuesMode === null ? null : tab.residuesMode);
        this._residuesInitMode = this._residuesMode;
        this._residuesInitValue = (tab.residuesValue === null ? null : tab.residuesValue);

        this._fuelQuantity           = tab.fuelQuantity;
        this._fuelUnitPrice          = Number(tab.fuelUnitPrice);
        this._fuelCurrentUnitPrice   = Number(tab.fuelCurrentUnitPrice);
        this._tabFuelEstimation      = tab.tabFuelEstimation;

        this._tabCleaningTypes = tabCleaningTypes;

        this._tabRentServices = tabRentServices;

        // Montant total des anciens services complémentaires
        this._oldServicesAmount = oldServicesAmount;

        for (var name in tabExtParams)
        {
            if (this._tabExtParams.hasOwnProperty(name))
            {
                this._tabExtParams[name] = tabExtParams[name];
            }
        }

        this._tabRights = tabRights;

        if (this.onload)
        {
            this.onload();
        }
    },

    getTotal : function()
    {
        return this.getInsuranceAmount() + this.getAppealAmount() + this.getCleaningAmount() +
               this.getResiduesAmount() + this.getFuelAmount() + this.getRentServicesAmount();
    },

    /**
     * Renvoi une chaîne représentant la config actuelle
     *
     * @return string
     */
    getConfigs : function()
    {
        return [
            this.getInsuranceTypeId(),
            this.getInsuranceAmount() * 100,
            this.getAppealTypeId(),
            this.getAppealAmount() * 100,
            this.getCleaningTypeId(),
            this.getCleaningAmount() * 100,
            this.getResiduesMode(),
            this.getResiduesAmount() * 100,
            this.getFuelQuantity()
        ].join('_');
    },

    /**
     * Récupérer le montant d'assurance
     *
     * @return float
     */
    getInsuranceAmount : function()
    {
        var insuranceTypeInfos = this.getInsuranceTypeInfos();
        if (!insuranceTypeInfos || !insuranceTypeInfos.isInvoiced)
        {
            return 0;
        }

        // Ajustement de la durée d'assurance par rapport au mode de calcul
        var insuranceDuration = 0;
        // - Calcul sur les jours ouvrés
        if (insuranceTypeInfos.mode == INSURANCEMODE_WORKING)
        {
            insuranceDuration = this._tabExtParams.duration;
        }
        else if (in_array(insuranceTypeInfos.mode, [INSURANCEMODE_CALENDARADDAYS, INSURANCEMODE_CALENDAR]))
        {
            // - Durée calendaire
            insuranceDuration = this._tabExtParams.calendarDuration;
            if (insuranceDuration == 0)
            {
                // Retro compatibilité pour les lignes de devis
                insuranceDuration = this._tabExtParams.duration * 7 / 5;
            }

            if (insuranceTypeInfos.mode == INSURANCEMODE_CALENDARADDAYS)
            {
                // La durée d'assurance est égale à la durée calendaire + les jours plus - les jours moins
                insuranceDuration += this._tabExtParams.totalDays;
            }
        }

        var insuranceAmount = insuranceDuration * this._tabExtParams.rentDayPrice * this.getInsuranceRate();

        return LOC_Common.round(insuranceAmount, 2);
    },

    /**
     * Récupérer le taux d'assurance
     *
     * @return float
     */
    getInsuranceRate : function()
    {
        var insuranceTypeInfos = this.getInsuranceTypeInfos();
        // La condition sur le mode Création sert pour les contrats/lignes de devis en full service anciennement créés
        // avec une assurance différente de "Assurance incluse"
        if (this._documentObj.getEditMode() == 'create' && this.getExternalParamValue('isFullService'))
        {
            return null;
        }
        else if (insuranceTypeInfos && !insuranceTypeInfos.isInvoiced)
        {
            return null;
        }
        else if (this._insuranceRate !== null)
        {
            return this._insuranceRate;
        }
        else
        {
            var customerInsurance = this.getExternalParamValue('customerInsurance');
            if (customerInsurance !== null && customerInsurance.rate !== null)
            {
                return customerInsurance.rate;
            }
            else
            {
                return this._tabCfgs.defaultInsuranceRate;
            }
        }
    },

    /**
     * Récupérer le type d'assurance
     *
     * @return int
     */
    getInsuranceTypeId : function()
    {
        // La condition sur le mode Création sert pour les contrats/lignes de devis en full service anciennement créés
        // avec une assurance différente de "Assurance incluse"
        if (this._documentObj.getEditMode() == 'create' && this.getExternalParamValue('isFullService'))
        {
            var result = this._tabInsuranceTypesList.find(function(item) {
                return item.isIncluded;
            });
            return (result ? result.id : null);
        }
        else if (this._insuranceTypeId !== null)
        {
            return this._insuranceTypeId;
        }
        else
        {
            var customerInsurance = this.getExternalParamValue('customerInsurance');
            if (customerInsurance !== null && customerInsurance.typeId !== null)
            {
                return customerInsurance.typeId;
            }
            else
            {
                return this._tabCfgs.defaultInsuranceTypeId;
            }
        }
    },

    /**
     * Récupérer les informations sur un type d'assurance
     *
     */
    getInsuranceTypeInfos: function()
    {
        var mySelf = this;
        return this._tabInsuranceTypesList.find(function(item) {
            return item.id == mySelf.getInsuranceTypeId();
        });
    },

    /**
     * Récupérer la liste des types d'assurance
     */
    getInsuranceTypesList: function()
    {
        return this._tabInsuranceTypesList;
    },

    /**
     * Récupérer le mode de calcul de la gestion de recours
     *
     * @return int
     */
    getAppealTypeId : function()
    {
        // Vérification avec le type d'assurance
        var insuranceTypeInfos = this.getInsuranceTypeInfos();
        if (insuranceTypeInfos && !insuranceTypeInfos.isAppealActivated)
        {
            return null;
        }

        if (this._appealTypeId !== null)
        {
            return this._appealTypeId;
        }
        else
        {
            var customerAppeal = this.getExternalParamValue('customerAppeal');
            if (customerAppeal !== null && customerAppeal.typeId !== null)
            {
                return customerAppeal.typeId;
            }
            else
            {
                return this._tabCfgs.defaultAppealTypeId;
            }
        }
    },

    /**
     * Récupérer les informations sur un type de gestion de recours
     *
     */
    getAppealTypeInfos: function()
    {
        var mySelf = this;
        return this._tabAppealTypesList.find(function(item) {
            return item.id == mySelf.getAppealTypeId();
        });
    },

    /**
     * Récupérer la liste des types de gestion de recours
     */
    getAppealTypesList: function()
    {
        return this._tabAppealTypesList;
    },


    /**
     * Récupérer la valeur de la gestion de recours pour le calcul (pourcentage)
     *
     * @return float
     */
    getAppealValue : function()
    {
        return this._tabCfgs.defaultAppealRate;
    },

    /**
     * Récupérer le montant de la gestion de recours
     *
     * @return float
     */
    getAppealAmount : function()
    {
        var insuranceTypeInfos = this.getInsuranceTypeInfos();
        if (!insuranceTypeInfos || !insuranceTypeInfos.isAppealActivated)
        {
            return 0;
        }

        var appealTypeInfos = this.getAppealTypeInfos();
        var amount = 0;
        var appealDuration = 0;

        // Ajustement de la durée d'assurance par rapport au mode de calcul
        if (appealTypeInfos && in_array(appealTypeInfos.mode, [APPEALMODE_CALENDAR]))
        {
            // - Durée calendaire
            appealDuration = this._tabExtParams.calendarDuration;
            if (appealDuration == 0)
            {
                // Retro compatibilité pour les lignes de devis
                appealDuration = this._tabExtParams.duration * 7 / 5;
            }
        }

        amount = appealDuration * this._tabExtParams.rentDayPrice * this.getAppealValue();
        return LOC_Common.round(amount, 2);
    },

    /**
     * Récupérer le montant de nettoyage
     *
     * @return float
     */
    getCleaningAmount : function()
    {
        return (this.getCleaningTypeId() != 0 ? this._cleaningAmount : 0);
    },

    /**
     * Récupérer l'id du type de nettoyage
     *
     * @return int
     */
    getCleaningTypeId : function()
    {
        return this._cleaningTypeId;
    },

    /**
     * Récupérer l'instance du client
     *
     * @return Location.Rent_Helper_Customer
     */
    getCustomerObject : function()
    {
        if (this._documentObj.getObjectType() == 'contract')
        {
            return this._documentObj.getCustomerObject();
        }
        else
        {
            return this._documentObj.getEstimateObject().getCustomerObject();
        }
    },

    /**
     * Récupérer le montant de carburant
     *
     * @return float
     */
    getFuelAmount : function()
    {
        var fuelQuantity = this.getFuelQuantity();
        return (fuelQuantity === null ? 0 : fuelQuantity * this.getFuelUnitPrice());
    },

    /**
     * Récupérer la quantité de carburant
     *
     * @return float
     */
    getFuelQuantity : function()
    {
        return this._fuelQuantity;
    },

    /**
     * Récupérer le montant unitaire (litre) du carburant
     *
     * @return float
     */
    getFuelUnitPrice : function()
    {
        return this._fuelUnitPrice;
    },

    /**
     * Récupérer la quantité de carburant estimée pour le contrat
     *
     * @return array : quantity, unitPrice, amout
     */
    getFuelEstimationInfos : function()
    {
        return this._tabFuelEstimation;
    },

    /**
     * Récupérer le montant de la participation au recyclage
     *
     * @return float
     */
    getResiduesAmount : function()
    {
        var amount = 0;
        var residuesMode = this.getResiduesMode();
        if (residuesMode == RESIDUES_MODE_PRICE)
        {
            amount = LOC_Common.round(this.getResiduesValue(), 2);
        }
        else if (residuesMode == RESIDUES_MODE_PERCENT)
        {
            // Arrondi de la participation au recyclage à 2 décimales
            // /!\ Attention à l'arrondi avec un chiffre à plus de 2 décimales /!\
            // (10 * 95 x 0.0195 = 18.525 (=18.524999999998 pour la machine) et donc son arrondi à 2 décimales = 18.52 au lieu de 18.53)
            // Il faut donc ajouter un epsilon. Ici, nous admettons que le chiffre en entrée ne peut pas avoir plus de 6 décimales
            amount = LOC_Common.round(this.getExternalParamValue('residuesBaseAmount') * this.getResiduesValue(), 2, 6);
        }

        return amount;
    },

    /**
     * Récupérer la liste des services de location et de transport
     *
     * @param Array
     */
    getRentServices : function()
    {
        var tab = [];
        var count = this._tabRentServices.length;
        for (var i = 0; i < count; i++)
        {
            if (this._tabRentServices[i]['rentService.id'] != 0)
            {
                tab.push(this._tabRentServices[i]);
            }
        }
        return tab;
    },

    /**
     * Récupérer le montant des services de location et de transport
     *
     * @return float
     */
    getRentServicesAmount : function()
    {
        var amount = 0;
        var nb = this._tabRentServices.length;
        for (var i = 0; i < nb; i++)
        {
            if (this._tabRentServices[i]['state.id'] != RENTSRV_STATE_DELETED)
            {
                amount += this._tabRentServices[i]['amount'] * 1;
            }
        }
        return amount + this._oldServicesAmount;
    },

    /**
     * Compte le nombre de services de location saisis
     *
     * @param int
     */
    getRentServicesCount : function()
    {
        return this._tabRentServices.length;
    },


    /**
     * Récupérer le mode de calcul de la participation au recyclage
     *
     * @return int
     */
    getResiduesMode : function()
    {
        if (!this.isResiduesActived())
        {
            return RESIDUES_MODE_NONE;
        }

        if (this._residuesMode !== null)
        {
            return this._residuesMode;
        }
        else if (this._tabCfgs.useResiduesForAccessories == 0 && this.getExternalParamValue('isModelAccessory'))
        {
            return RESIDUES_MODE_NONE;
        }
        else
        {
            var isCustomerResiduesInvoiced = this.getExternalParamValue('isCustomerResiduesInvoiced');
            if (isCustomerResiduesInvoiced !== null)
            {
                return (isCustomerResiduesInvoiced ? this._tabCfgs.defaultResiduesMode : RESIDUES_MODE_NONE);
            }
            else
            {
                return RESIDUES_MODE_NONE;
            }
        }
    },

    /**
     * Récupérer le mode de calcul de la participation au recyclage au chargement
     *
     * @return int
     */
    getResiduesInitMode : function()
    {
        return this._residuesInitMode;
    },

    /**
     * Récupérer la valeur de la participation au recyclage pour le calcul (montant forfaitaire ou pourcentage)
     *
     * @return float
     */
    getResiduesValue : function()
    {
        var value = null;
        var residuesMode = this.getResiduesMode();
        if (residuesMode == this._residuesInitMode && this._residuesInitValue !== null)
        {
            value = this._residuesInitValue;
        }
        else if (residuesMode == RESIDUES_MODE_PRICE)
        {
            value = this._tabCfgs.defaultResiduesAmount;
        }
        else if (residuesMode == RESIDUES_MODE_PERCENT)
        {
            value = this._tabCfgs.defaultResiduesRate;
        }
        return value;
    },


    /**
     * Indique si le carburant saisie est egal à l'estimation
     */
    isFuelEqualEstimation : function()
    {
        if (!this._tabFuelEstimation ||
            this._tabFuelEstimation.quantity == null ||
            this._fuelQuantity == null ||
            this._fuelQuantity == this._tabFuelEstimation.quantity )
        {
            return true
        }

        return false
    },

    /**
     * Indique si la participation est activée ou non
     *
     * @return bool
     */
    isResiduesActived : function()
    {
        return (this._tabCfgs.defaultResiduesMode != RESIDUES_MODE_NONE ||
                (this._residuesInitMode !== null && this._residuesInitMode != RESIDUES_MODE_NONE));
    },

    /**
     * Est-il possible d'ajouter des services loc et transport ?
     *
     * @return bool
     */
    isAddRentServicesPossible : function()
    {
        return (this.hasRight('addRentServices') && !this.getExternalParamValue('disabledComponents').rentServices);
    },

    isEditRentServicesPossible : function(id)
    {
        return (this.hasRight('editRentServices') &&
                this._tabRentServices[id]['state.id'] != RENTSRV_STATE_DELETED &&
                !this.getExternalParamValue('disabledComponents').rentServices);
    },

    /**
     * Indique si la gestion de recours est activée ou non
     *
     * @return bool
     */
    isAppealActivated : function()
    {
        // mode activé
        return (this._tabCfgs.defaultAppealMode != APPEAL_MODE_NONE);
    },

    /**
     * Supprime un service de loc/transp
     */
    removeRentService : function(id)
    {
        //Dans les cas où la ligne est déjà enregistrée
        if (this._tabRentServices[id]['id'] != 0)
        {
            this._tabRentServices[id]['state.id'] = RENTSRV_STATE_DELETED;
        }
        //Dans le cas où la ligne n'est pas enregistrée (la ligne disparaît)
        else
        {
            this._tabRentServices.splice(id, 1);
        }

        if (this.onchange)
        {
            this.onchange(['removeRentService']);
        }
        return true;
    },

    /**
     * Définir le taux d'assurance
     *
     * @param float rate
     * @return bool
     */
    setInsuranceRate : function(rate)
    {
        rate = parseFloat(rate);
        var insuranceTypeInfos = this.getInsuranceTypeInfos();
        if (insuranceTypeInfos && !insuranceTypeInfos.isInvoiced)
        {
            rate = 0;
        }

        if (this._insuranceRate != rate)
        {
            if (!this.hasRight('insurance') || this.getExternalParamValue('disabledComponents').insuranceRate ||
                 this.getExternalParamValue('isFullService'))
            {
                return false;
            }

            this._insuranceRate = rate;

            if (this.onchange)
            {
                this.onchange(['insuranceRate']);
            }
        }
        return true;
    },

    /**
     * Définir le type d'assurance
     *
     * @param int typeId
     * @return bool
     */
    setInsuranceTypeId : function(typeId, appealTypeId)
    {
        typeId = (typeId ? Number(typeId) : this._tabCfgs.defaultInsuranceTypeId);

        if (this._insuranceTypeId != typeId)
        {
            if (!this.hasRight('insurance') || this.getExternalParamValue('disabledComponents').insuranceType ||
                 this.getExternalParamValue('isFullService'))
            {
                return false;
            }

            this._insuranceTypeId = typeId;

            var insuranceTypeInfos = this.getInsuranceTypeInfos();
            if (!insuranceTypeInfos.isAppealActivated)
            {
                this._appealTypeId = null;
            }
            else
            {
                if (typeof appealTypeId != 'undefined')
                {
                    this._appealTypeId = appealTypeId;
                }
                else if (this._appealTypeId == null)
                {
                    var customerAppeal = this.getExternalParamValue('customerAppeal');
                    this._appealTypeId = (customerAppeal !== null ? customerAppeal.typeId : null);
                }
            }

            if (this.onchange)
            {
                this.onchange(['insuranceTypeId']);
            }
        }
        return true;
    },

    /**
     * Définir le type de gestion de recours
     *
     * @param int typeId
     * @return bool
     */
    setAppealTypeId : function(typeId)
    {
        if (this._appealTypeId != typeId)
        {
            if (!this.hasRight('appeal') || this.getExternalParamValue('disabledComponents').appealType)
            {
                return false;
            }

            this._appealTypeId = typeId;

            if (this.onchange)
            {
                this.onchange(['appealTypeId']);
            }
        }
        return true;
    },

    /**
     * Remet l'assurance avec ses valeurs par défaut
     *
     */
    resetInsurance: function()
    {
        this._insuranceTypeId = null;
        this._insuranceRate = null;
    },

    /**
     * Remet la gestion de recours avec ses valeurs par défaut
     *
     */
    resetAppeal: function()
    {
        this._appealTypeId = null;
    },

    /**
     * Remet la participation au recyclage avec ses valeurs par défaut
     *
     */
    resetResidues: function()
    {
        this._residuesMode  = null;
    },

    /**
     * Définir le montant du nettoyage
     *
     * @param float amount Montant
     * @return bool
     */
    setCleaningAmount : function(amount)
    {
        amount = parseFloat(amount);
        if (amount < 0)
        {
            amount = 0;
        }

        if (this._cleaningAmount != amount)
        {
            if (!this.hasRight('cleaning') || this.getExternalParamValue('disabledComponents').cleaning)
            {
                return false;
            }

            this._cleaningAmount = amount;

            if (this.onchange)
            {
                this.onchange(['cleaningAmount']);
            }
        }
        return true;
    },

    /**
     * Définir le type de nettoyage
     *
     * @param int id Id du type
     * @param bool isUpdatedAmount Mise à jour du montant ? (true par défaut)
     * @return bool
     */
    setCleaningTypeId : function(id, isUpdatedAmount)
    {
        var tabInfos = this.getCleaningTypeInfos(id);
        id = (tabInfos ? tabInfos.id : 0);
        if (!this.hasRight('cleaning') || this.getExternalParamValue('disabledComponents').cleaning)
        {
            return false;
        }
        var tabChanges = [];
        if (this._cleaningTypeId != id)
        {
            this._cleaningTypeId = id;
            tabChanges.push('cleaningTypeId');
        }

        if (id != 0 && (typeof isUpdatedAmount == 'undefined' || isUpdatedAmount) &&
            this._cleaningAmount != tabInfos.amount)
        {
            this._cleaningAmount = tabInfos.amount;
            tabChanges.push('cleaningAmount');
        }

        if (tabChanges.length > 0 && this.onchange)
        {
            this.onchange(tabChanges);
        }

        return true;
    },

    getCleaningTypeInfos : function(id)
    {
        var i = 0, n = this._tabCleaningTypes.length;
        if (id == 'std')
        {
            while (i < n && !this._tabCleaningTypes[i].isStandard)
            {
                i++;
            }
        }
        else
        {
            id = Number(id);
            if (id == 0)
            {
                return false;
            }
            while (i < n && this._tabCleaningTypes[i].id != id)
            {
                i++;
            }
        }
        return (i < n ? this._tabCleaningTypes[i] : null);
    },

    setCleaningTypes : function(tab, isStdForced)
    {
        if (LOC_Common.isEqual(tab, this._tabCleaningTypes))
        {
            return false;
        }

        var tabInfos = this.getCleaningTypeInfos(this._cleaningTypeId);

        this._tabCleaningTypes = tab;

        var tabChanges = ['cleaningTypes'];

        if (isStdForced || (this._cleaningTypeId != 0 && !this.getCleaningTypeInfos(this._cleaningTypeId)))
        {
            tabInfos = this.getCleaningTypeInfos('std');
            var id = (tabInfos ? tabInfos.id : 0);
            if (this._cleaningTypeId != id)
            {
                this._cleaningTypeId = id;

                tabChanges.push('cleaningTypeId');
            }

            if (id != 0 && this._cleaningAmount != tabInfos.amount)
            {
                this._cleaningAmount = tabInfos.amount;
                tabChanges.push('cleaningAmount');
            }
        }

        if (this.onchange)
        {
            this.onchange(tabChanges);
        }
        return true;
    },

    /**
     * Définir la quantité de carburant
     *
     * @param float|null Quantité
     * @return bool
     */
    setFuelQuantity : function(quantity)
    {
        if (quantity === null && this._fuelQuantity !== null)
        {
            quantity = 0;
        }
        quantity = parseFloat(quantity);
        if (quantity < 0)
        {
            quantity = 0;
        }
        if (this._fuelQuantity === quantity || !this._tabExtParams.isFuelNeeded || !this.hasRight('fuel'))
        {
            return false;
        }
        // Si il y a une modification de la quantité alors on prend toujours le prix unitaire courant
        this._fuelUnitPrice = this._fuelCurrentUnitPrice;
        this._fuelQuantity = quantity;

        if (this.onchange)
        {
            this.onchange(['fuelQuantity']);
        }
        return true;
    },

    /**
     * Indique si la participation au recyclage est facturée ou non
     *
     * @return int
     */
    setResiduesMode : function(mode)
    {
        mode = (mode != RESIDUES_MODE_PRICE && mode != RESIDUES_MODE_PERCENT ? RESIDUES_MODE_NONE : Number(mode));

        if (this._residuesMode != mode)
        {
            if (!this.hasRight('residues') || this.getExternalParamValue('disabledComponents').residues)
            {
                return false;
            }
            this._residuesMode = mode;

            if (this.onchange)
            {
                this.onchange(['residuesMode']);
            }
        }
        return true;
    },


    /**
     * Récupérer un paramètre externe
     *
     * @param string name
     * @return mixed
     */
    getExternalParamValue : function(name)
    {
        return this._tabExtParams[name];
    },

    /**
     * Définir un ou plusieurs paramètres externes
     *
     * @param Object tab Paramètres à définir
     * @return bool
     */
    setExternalParams : function(tab)
    {
        var tabChanges = [];
        for (var name in tab)
        {
            if (this._tabExtParams.hasOwnProperty(name))
            {
                if (!LOC_Common.isEqual(tab[name], this._tabExtParams[name]))
                {
                    this._tabExtParams[name] = tab[name];
                    tabChanges.push('extParam_' + name);
                }
            }
        }

        if (tabChanges.length > 0 && this.onchange)
        {
            this.onchange(tabChanges);
        }
        return (tabChanges.length > 0);
    },

    /**
     * Vérifier le droit à un champ
     *
     * @return bool
     */
    hasRight : function(fieldName)
    {
        return (this._tabRights === null || this._tabRights[fieldName] ? true : false);
    }
}



Location.Rent_Helper_ServicesView = {

    _oldCountServices: null,
    _residuesSelectCloneEl : null,
    _appealSelectCloneEl : null,

    /**
     * Afficher une ligne de service de location
     */
    _displayLineRentService : function(pfx, obj, table, index)
    {
        var mySelf = this;
        var line = obj.getRentServices()[index];
        // nouveau tr
        var row = table.insertRow(index);
        row.setAttribute('id', 'trRentService_' + index);
        row.setAttribute('data-ln', index);

        if (line['state.id'] == RENTSRV_STATE_DELETED)
        {
            row.className = 'removed';
        }

        var typeEl = $ge(pfx + 'rentSrvsAddTypeId').cloneNode(true);
        var tabOptions = Array.from(typeEl.options);

        // nouveau td : infobulle
        var cell = row.insertCell(-1);
        cell.className = 'info';
        var selectedElt = tabOptions.find(function(option) {
            return option.value == line['rentService.id'];
        });
        if (selectedElt && selectedElt.getAttribute('data-infos') != '')
        {
            var tipEl = $ge(pfx + 'rentSrvsAddTooltip').cloneNode(true);
            tipEl.id = pfx + 'rentSrvsTooltip_' + index;
            tipEl.setAttribute('data-text', selectedElt.getAttribute('data-infos'));
            cell.appendChild(tipEl);
        }

        // nouveau td : désignation
        var cell = row.insertCell(-1);
        cell.className = 'name';

        //Clonage de son élément html SELECT (depuis la vue)
        typeEl.name  = typeEl.id = pfx + 'rentSrvsTypeId_' + index;
        // - Le service a été supprimé de la liste possible
        var isRentServiceExists = tabOptions.some(function(option) {
            return option.value == line['rentService.id'];
        });
        if (!isRentServiceExists)
        {
            var option = new Option(line['rentService.name'], line['rentService.id']);
            typeEl.add(option);
        }
        typeEl.value = line['rentService.id'];
        typeEl.removeChild(typeEl.options[0]);
        var typeName = typeEl.options[typeEl.selectedIndex].text;
        //Affectation dans le td
        cell.appendChild(typeEl);

        // nouveau td : commentaire
        var cell = row.insertCell(-1);
        cell.className = 'comment';
        var commentEl = $ge(pfx + 'rentSrvsAddComment').cloneNode(true);
        commentEl.name = commentEl.id = pfx + 'rentSrvsComment_' + index;
        commentEl.value = line.comment;
        //Affecte au td le noeud INPUT TEXT
        cell.appendChild(commentEl);

        // nouveau td : montant
        var cell = row.insertCell(-1);
        cell.className = 'amount';
        var amountEl = $ge(pfx + 'rentSrvsAddAmount').cloneNode(true);
        amountEl.name = amountEl.id = pfx + 'rentSrvsAmount_' + index;
        //Affecte au td le noeud
        cell.appendChild(amountEl);

        // Création du currency
        var amountCurrency = Location.currenciesManager.cloneCurrency(pfx + 'rentSrvsAmount_' + index, pfx + 'rentSrvsAddAmount');
        amountCurrency.setValue(line.amount, false);

        if (obj._documentObj.getObjectType() == 'contract')
        {
            // nouveau td : Etat
            var cell = row.insertCell(-1);
            cell.className = 'state';
            var stateEl = document.createTextNode(line['state.label']);
            cell.appendChild(stateEl);
        }

        // nouveau td : bouton suppression
        var cell = row.insertCell(-1);
        cell.className = 'ctrls';
        if (obj.isEditRentServicesPossible(index) && line.isRemovable)
        {
            var btnEl = $ge(pfx + 'rentSrvsAddLine').cells[row.cells.length - 1].firstChild.cloneNode(true);
            cell.appendChild(btnEl);
        }

        // Droits et possibilités
        typeEl.disabled    = (!obj.isEditRentServicesPossible(index));
        commentEl.disabled = (!obj.isEditRentServicesPossible(index));
        amountCurrency.setDisabled(!obj.isEditRentServicesPossible(index));

        // Evènements
        // - modification de type
        typeEl.onchange = function()
        {
            var index = this.parentNode.parentNode.getAttribute("data-ln");
            var tabUpdates = {
                'rentService.id': this.value,
                'rentService.name': this.options[this.selectedIndex].text,
                'amount': this.options[this.selectedIndex].getAttribute('data-amount'),
                'info': this.options[this.selectedIndex].getAttribute('data-info'),
                'comment': ''
            };
            obj.editRentService(index, tabUpdates);
        }

        // - modification de commentaire
        commentEl.onchange = function()
        {
            var index = this.parentNode.parentNode.getAttribute("data-ln");
            obj.editRentService(index, {'comment': this.value.trim()});
        }

        // - modification de montant
        amountCurrency.onchange = function ()
        {
            var index = this.getSpanElement().parentNode.parentNode.getAttribute("data-ln");
            try
            {
                obj.editRentService(index, {amount: this.getValue()});
            }
            catch (exception)
            {
                if (exception == 'minAmountError')
                {
                    alert(LOC_View.getTranslation('rentServiceUnderMinAmountError') + obj._rentServiceMinAmount);
                }
            }
        }

        // - suppression de ligne
        if (btnEl)
        {
            btnEl.onclick = function()
            {
                if (confirm(LOC_View.getTranslation('confirmRemoveRentService')))
                {
                    var index = this.parentNode.parentNode.getAttribute("data-ln");
                    obj.removeRentService(index);
                }
            }
        }
    },

    display : function(pfx, obj, tabErrors)
    {
        var customerInsurance  = obj.getExternalParamValue('customerInsurance');
        var mySelf = this;
        var disabledComponents  = obj.getExternalParamValue('disabledComponents');
        var scrollTop = $ge(pfx + 'container').scrollTop;
        var isFullService    = obj.getExternalParamValue('isFullService');

        var isInsuranceTypeReadOnly = (!obj.hasRight('insurance') || disabledComponents.insuranceType || isFullService ? true : false);
        var isInsuranceRateReadOnly = (!obj.hasRight('insurance') || disabledComponents.insuranceRate || isFullService ? true : false);
        var isAppealTypeReadOnly = (!obj.hasRight('appeal') || disabledComponents.appealType || isFullService ? true : false);
        var isCleaningReadOnly  = (!obj.hasRight('cleaning') || disabledComponents.cleaning ? true : false);
        var isResiduesReadOnly  = (!obj.hasRight('residues') || disabledComponents.residues ? true : false);
        var isFuelReadOnly      = !(obj.getExternalParamValue('isFuelNeeded') && obj.hasRight('fuel'));

        // Affichage de l'icone d'information sur "Assurance non modifiable car le contrat est en Full service"
        $ge(pfx + 'infoNotEditableInsurance').style.display = (isFullService ? 'inline-block' : 'none');

        // Affichage de l'icone d'information : conditions d'assurance de la ligne différente de celle du client
        $ge(pfx + 'insuranceChanged').style.display = (tabErrors.isInsuranceChanged ? '' : 'none');

        // Type d'assurance
        var insuranceTypeSelectElt = $ge(pfx + 'insuranceType');
        var tabInsuranceTypesList = obj.getInsuranceTypesList();
        while (insuranceTypeSelectElt.options[0])
        {
            insuranceTypeSelectElt.removeChild(insuranceTypeSelectElt.options[0]);
        }
        for (var i = 0; i < tabInsuranceTypesList.length; i++)
        {
            if (tabInsuranceTypesList[i]['state.id'] == INSURANCE_STATE_ACTIVE || tabInsuranceTypesList[i].id == obj.getInsuranceTypeId())
            {
                var option = new Option(tabInsuranceTypesList[i].label, tabInsuranceTypesList[i].id);
                if (obj.getInsuranceTypeId() == tabInsuranceTypesList[i].id)
                {
                    option.selected = true;
                }
                try
                {
                    insuranceTypeSelectElt.add(option, null);
                }
                catch(ex)
                {
                    insuranceTypeSelectElt.add(option);
                }
            }
        }
        insuranceTypeSelectElt.disabled = isInsuranceTypeReadOnly;

        // Taux d'assurance
        var rate = obj.getInsuranceRate();
        var rateSelectElt = $ge(pfx + 'insuranceRate');
        rateSelectElt.value = rate;
        rateSelectElt.disabled = (rate == 0 ? true : isInsuranceRateReadOnly);
        // Montant total d'assurance
        var el = Location.currenciesManager.getCurrency(pfx + 'insuranceAmount');
        el.setValue(obj.getInsuranceAmount(), false);

        if (insuranceTypeSelectElt._oldonchange)
        {
            insuranceTypeSelectElt._oldonchange();
        }

        // Gestion de recours
        var isAppealActivated = obj.isAppealActivated();
        $ge(pfx + 'appealRow').style.display = (isAppealActivated ? '' : 'none');

        if (isAppealActivated)
        {
            var isDisplayList = (obj.getAppealTypeId() ? true : false);
            $ge(pfx + 'nonInvoiceableAppeal').style.display = (isDisplayList ? 'none' : '');
            $ge(pfx + 'appealType').style.display = (isDisplayList ? '' : 'none');

            var appealTypeSelectElt = $ge(pfx + 'appealType');
            var tabAppealTypesList = obj.getAppealTypesList();
            while (appealTypeSelectElt.options[0])
            {
                appealTypeSelectElt.removeChild(appealTypeSelectElt.options[0]);
            }
            for (var i = 0; i < tabAppealTypesList.length; i++)
            {
                if (tabAppealTypesList[i]['state.id'] == INSURANCE_STATE_ACTIVE || tabAppealTypesList[i].id == obj.getAppealTypeId())
                {
                    var option = new Option(tabAppealTypesList[i].label, tabAppealTypesList[i].id);
                    if (obj.getAppealTypeId() == tabAppealTypesList[i].id)
                    {
                        option.selected = true;
                    }
                    try
                    {
                        appealTypeSelectElt.add(option, null);
                    }
                    catch(ex)
                    {
                        appealTypeSelectElt.add(option);
                    }
                }
            }
            appealTypeSelectElt.disabled = isAppealTypeReadOnly;


            // Montant gestion de recours
            el = Location.currenciesManager.getCurrency(pfx + 'appealAmount');
            el.setValue(obj.getAppealAmount(), false);

            if (appealTypeSelectElt._oldonchange)
            {
                appealTypeSelectElt._oldonchange();
            }
        }

        // Nettoyage
        var selectEl = $ge(pfx + 'cleaningType');
        // Mise à jour de la liste des nettoyages
        var nb = selectEl.options.length;
        while (nb > 0)
        {
            nb--;
            if (selectEl.options[nb].value != 0)
            {
                selectEl.removeChild(selectEl.options[nb]);
            }
        }
        nb = obj._tabCleaningTypes.length;
        for (var i = 0; i < nb; i++)
        {
            var option = new Option(obj._tabCleaningTypes[i].label, obj._tabCleaningTypes[i].id);
            try
            {
                selectEl.add(option, null);
            }
            catch(ex)
            {
                selectEl.add(option);
            }
        }

        var cleaningTypeId = obj.getCleaningTypeId();
        selectEl.value = cleaningTypeId;
        selectEl.disabled = isCleaningReadOnly;

        el = Location.currenciesManager.getCurrency(pfx + 'cleaningAmount');
        el.setValue(obj.getCleaningAmount(), false);
        el.setDisabled(isCleaningReadOnly || cleaningTypeId == 0);


        // Participation au recyclage
        var isResiduesActived = obj.isResiduesActived();
        $ge(pfx + 'residuesRow').style.display = (isResiduesActived ? '' : 'none');

        if (isResiduesActived)
        {
            var residuesInitMode = obj.getResiduesInitMode();
            var defaultResiduesMode = obj._tabCfgs.defaultResiduesMode;
            var residuesMode = obj.getResiduesMode();

            // Affichage de l'icone d'information : conditions d'assurance de la ligne différente de celle du client
            $ge(pfx + 'residuesModeChanged').style.display = (tabErrors.isResiduesModeChanged ? '' : 'none');

            var selectEl = $ge(pfx + 'residuesMode');
            // Copie de la liste
            if (!mySelf._residuesSelectCloneEl)
            {
                mySelf._residuesSelectCloneEl = selectEl.cloneNode(true);
            }
            while (selectEl.options[0])
            {
                selectEl.removeChild(selectEl.options[0]);
            }
            for (var i = 0; i < mySelf._residuesSelectCloneEl.options.length; i++)
            {
                var item = mySelf._residuesSelectCloneEl.options[i];
                if (item.value == RESIDUES_MODE_NONE ||
                    item.value == residuesInitMode ||
                    item.value == defaultResiduesMode)
                {
                    selectEl.appendChild(item.cloneNode(true));
                }

                // Message
                el = $ge(pfx + 'residuesMsg_' + item.value);
                if (el)
                {
                    el.style.visibility = 'hidden';
                    elValue = $ge(pfx + 'residuesValue_' + item.value);
                    if (item.value == RESIDUES_MODE_PERCENT)
                    {
                        elValue.innerHTML = LOC_View.getNumberFormat(obj.getResiduesValue() * 100, 2);
                    }
                }
            }

            selectEl.value = residuesMode;
            selectEl.disabled = isResiduesReadOnly;

            // Message
            el = $ge(pfx + 'residuesMsg_' + residuesMode);
            if (el)
            {
                el.style.visibility = 'visible';
            }

            // Montant participation au recyclage
            el = Location.currenciesManager.getCurrency(pfx + 'residuesAmount');
            el.setValue(obj.getResiduesAmount(), false);
        }


        // Carburant
        var fuelQuantity = obj.getFuelQuantity();
        el = $ge(pfx + 'fuelQty');
        el.value = (fuelQuantity === null ? '' : fuelQuantity);
        el.disabled = isFuelReadOnly;

        el = Location.currenciesManager.getCurrency(pfx + 'fuelUnitPrice');
        el.setValue(obj.getFuelUnitPrice(), false);

        el = Location.currenciesManager.getCurrency(pfx + 'fuelAmount');
        el.setValue(obj.getFuelAmount(), false);

        el = $ge(pfx + 'fuelEstimQty');
        if (el)
        {
            fuelInfo = obj.getFuelEstimationInfos();
            if (fuelInfo.quantity)
            {
                el.innerHTML = fuelInfo.quantity;
                $ge(pfx + 'fuelEstimQtyZone').style.visibility = 'visible';
            }
            else
            {
                $ge(pfx + 'fuelEstimQtyZone').style.visibility = 'hidden';
            }
        }

        warning = $ge(pfx + 'warningFuelEstimation');
        if (!obj.isFuelEqualEstimation())
        {
            warning.style.visibility = 'visible';
        }
        else
        {
            warning.style.visibility = 'hidden';
        }

        el = Location.currenciesManager.getCurrency(pfx + 'fuelEstimUnitP');
        if (el)
        {
            fuelInfo = obj.getFuelEstimationInfos();
            el.setValue(fuelInfo.unitPrice, false);
        }

        // Services complémentaires
        el = $ge(pfx + 'rentSrvsBlock');
        if (el)
        {
            var servicesCount = obj.getRentServicesCount();
            el.className = 'sub';
            if (obj.isAddRentServicesPossible())
            {
                el.className += ' add';
            }

            var table = $ge(pfx + 'rentSrvsTable').tBodies[0];
            if (table != null)
            {
                // Suppression des lignes
                while (table.rows.length > 0)
                {
                    Location.currenciesManager.deleteCurrency(pfx + 'rentSrvsAmount_' + table.rows[0].getAttribute('data-ln'));
                    table.deleteRow(0);
                }
                for (var i = 0; i < servicesCount ; i++)
                {
                    mySelf._displayLineRentService(pfx, obj, table, i);
                }

                // focus sur le service nouvellement inséré
                if (this._oldCountServices !== null && servicesCount > this._oldCountServices)
                {
                    $ge(pfx + 'rentSrvsComment_' + (servicesCount - 1)).focus();
                }
            }
            if (servicesCount == 0)
            {
                el.className += ' noresult';
            }
            this._oldCountServices = servicesCount;
        }

        $ge(pfx + 'container').scrollTop = scrollTop;

        // Calculette contrat cadre
        $ge(pfx + 'calc').style.visibility = (!isFuelReadOnly && obj.getExternalParamValue('isCalcDisplayed') ? 'visible': 'hidden');

    },

    initEvents : function(pfx, obj)
    {
        // - Type d'assurance
        var el = $ge(pfx + 'insuranceType');
        if (el)
        {
            if (!el._oldonchange)
            {
                el._oldonchange = el.onchange;
            }
            el.onchange = function(e)
            {
                var value = this.value;
                if (this._oldonchange)
                {
                    this._oldonchange(e);
                }
                obj.setInsuranceTypeId(value);
            }
        }

        // - Taux d'assurance
        el = $ge(pfx + 'insuranceRate');
        if (el)
        {
            el.onchange = function()
            {
                obj.setInsuranceRate(this.value);
            }
        }

        // - Type de gestion de recours
        el = $ge(pfx + 'appealType');
        if (el)
        {
            if (!el._oldonchange)
            {
                el._oldonchange = el.onchange;
            }
            el.onchange = function(e)
            {
                var value = this.value;
                if (this._oldonchange)
                {
                    this._oldonchange(e);
                }
                obj.setAppealTypeId(value);
            }
        }


        // - Type de nettoyage
        el = $ge(pfx + 'cleaningType');
        if (el)
        {
            el.onchange = function()
            {
                obj.setCleaningTypeId(this.value, true);
            }
        }

        // - Montant de nettoyage
        el = Location.currenciesManager.getCurrency(pfx + 'cleaningAmount');
        if (el)
        {
            el.onchange = function()
            {
                var value = parseFloat(this.getValue());
                if (!isNaN(value))
                {
                    obj.setCleaningAmount(value);
                }
                this.setValue(obj.getCleaningAmount(), false);
            }
        }


        // - Type de participation au recyclage
        el = $ge(pfx + 'residuesMode');
        if (el)
        {
            el.onchange = function()
            {
                obj.setResiduesMode(this.value);
            }
        }

        // - Quantité de carburant
        el = $ge(pfx + 'fuelQty');
        el.onchange = function()
        {
            var value = (this.value.trim() == '' ? null : parseFloat(this.value));
            if (!isNaN(value))
            {
                obj.setFuelQuantity(value);
            }
            this.value = obj.getFuelQuantity();
        }


        // - Calculette pour les contrats cadres
        el = $ge(pfx + 'calc');
        if (el)
        {
            el.style.visibility = 'visible';
            el.onclick = function()
            {
                var fuelQuantity = obj.getFuelQuantity();
                if (fuelQuantity !== null && fuelQuantity > 0)
                {
                    var customerObj = obj.getCustomerObject();
                    var fwCttFuelUnitPrice = customerObj.getFwkCttFuelUnitPrice();
                    var fuelUnitPrice = obj.getFuelUnitPrice();
                    if (fwCttFuelUnitPrice != fuelUnitPrice)
                    {
                        obj.setFuelQuantity(LOC_Common.round(fuelQuantity * fwCttFuelUnitPrice / fuelUnitPrice));
                    }
                }
            }
        }

        // - ajout nouveau service loc/transp
        el = $ge(pfx + 'rentSrvsAddTypeId');
        if (el)
        {
            el.onchange = function()
            {
                var typeName = this.options[this.selectedIndex].text;
                var amount = this.options[this.selectedIndex].getAttribute('data-amount');
                obj.addRentService({
                    'rentService.id': this.value,
                    'rentService.name': typeName,
                    amount: amount});
                this.value = 0;
            }
        }
    }

}