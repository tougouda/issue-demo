var httpObj;
var tabLines;
var urlUnaccept;
var countryId;

var tabToUpdate = [];
var timer;

/**
 * Initialisation des variables globales
 * @param url - Url de désacceptation des lignes
 * @param tab - Tableau des lignes
 */
function init(country, url, tab)
{
    countryId = country;
    urlUnaccept = url;
    tabLines = tab;
}

/**
 * Insère la ligne de devis dans le tableau des lignes à mettre à jour
 * @param estimateLineId
 */
function doAction(estimateLineId)
{
    tabToUpdate.push(getEstimateLine(estimateLineId));
    $('#line_' + estimateLineId).removeClass('unacceptUndoLine unacceptLine');
    $('#line_' + estimateLineId).addClass('loading');

    if (timer)
    {
        clearTimeout(timer);
    }
    timer = setTimeout(function() { unacceptLine(tabToUpdate); }, 500);

}


/**
 * Récupère l'objet correspondant à la ligne
 * @param estimateLineId
 * @returns {Array}
 */
function getEstimateLine(estimateLineId)
{
    for (var i in tabLines)
    {
        if (tabLines[i]['id'] == estimateLineId)
        {
            return tabLines[i];
        }
    }
}

/**
 * Met à jour les paramètres dates sur une ligne
 * @param estimateLineId
 * @param dateModif
 * @param estimateDateModif
 */
function updateEstimateLine(estimateLine)
{
    for (var i in tabLines)
    {
        if (tabLines[i]['id'] == estimateLine['id'])
        {
            tabLines[i]['estimate.modificationDate'] = estimateLine['estimate.modificationDate'];
            tabLines[i]['modificationDate'] = estimateLine['modificationDate'];
            tabLines[i]['action'] = (estimateLine['action'] == 'accept' ? 'unaccept' : 'accept');
        }
    }
}


/**
 * Désaccepter une ligne
 */
function unacceptLine(tabLinesToUpdate)
{
    // réinitialisation du tableau des lignes à mettre à jour
    tabToUpdate = [];
    
    for (var i in tabLinesToUpdate)
    {
        $('#line_' + tabLinesToUpdate[i]['id']).removeClass('unacceptUndoLine unacceptLine');
        $('#line_' + tabLinesToUpdate[i]['id']).addClass('loading');
    }

    $.ajax({
        url: urlUnaccept,
        type: 'POST',
        cache: false,
        data: JSON.stringify({
            countryId: countryId,
            tabEstimateLines: tabLinesToUpdate
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function(tabResult){
            for (var i in tabResult)
            {
                var lines = $('#line_' + tabResult[i]['id']);
                lines.removeClass('unacceptUndoLine unacceptLine removed loading');
                if (tabResult[i]['result'])
                {
                    lines.removeClass('error');
                    if (tabResult[i]['action'] == 'unaccept')
                    {
                        lines.addClass('removed');
                        lines.addClass('unacceptUndoLine');
                    }
                    else
                    {
                        lines.addClass('unacceptLine');
                    }
                    // Mise à jour de la date de modification de la ligne et de l'action
                    updateEstimateLine(tabResult[i]);
                }
                else
                {
                    $('#line_' + tabResult[i]['id'] + ' > td.returnError').attr('title', tabResult[i]['textError']);
                    lines.addClass('error');
                    if (tabResult[i]['action'] == 'unaccept')
                    {
                        lines.addClass('unacceptLine');
                    }
                    else
                    {
                        lines.addClass('unacceptUndoLine');
                    }
                }
            }
        },
        error: function(){
            
        }
    });
}
