if (!window.Location)
{
    var Location = new Object();
}

/**
 * Afficher l'état d'un onglet
 *
 * @param string state Etat
 */
Location.TabBoxTab.prototype.setRentEstimateState = function(state)
{
    this.getTitleElement().className = state;
}


/**
 * Classe de gestion des ligne du devis
 *
 * @param estimateObj Instance du devis
 * @param id          Identifiant de la ligne
 */
Location.RentEstimateLine = function(estimateObj, id, tabPreloadInfos, tabCfgs)
{
    var mySelf = this;
    // Instance du devis associé
    this._estimateObj = estimateObj;
    // Instance de la gestion de la tarification location
    this._rentTariffObj = null;
    // Instance de la gestion de la tarification transport
    this._transportObj = null;
    // Instance de la gestion de la durée
    this._durationObj = null;
    // Instance de la gestion des autres services
    this._servicesObj = null;
    // Instance de la gestion des pro formas
    this._proformaObj = null;
    // Instance de la gestion des documents
    this._documentsObj = null;
    // Instance de gestion des historiques
    this._historyObj = null;

    // Instance HttpRequest pour le chargement des informations sur le modèle de machine
    this._modelHttpObj = null;
    // Liste des erreurs
    this._tabErrors = [];
    this._tabErrorsDetails = [];
    this._tabWarnings = [];
    this._tabDocumentsActions = [];
    // Configurations du devis
    this._tabCfgs = tabCfgs;
    // Ligne du tableau associée
    this._rowEl = null;
    // Indique si la ligne est supprimée
    this._isRemoved = false;

    // Liste des noms des éléments de type currency
    this._tabCurrencies = {rentAmount: null, rentTotal: null,
                           transportDeliveryAmount: null, transportRecoveryAmount: null,
                           otherServicesAmount: null, total :null};
    this._tabProperties = {
            modelId         : function(){return mySelf.getModelId();},
            modelLabel      : function(){return mySelf.getModelLabel();},
            isImperativeModel : function(tab){return (tab ? tab.isImperativeModel : mySelf.isImperativeModel());},
            quantity        : function(tab){return (tab ? tab.quantity : mySelf.getQuantity());},
            datesToConfirm  : function(){return mySelf.isDatesToConfirm();},
            beginDate       : function(){return mySelf.getBeginDate();},
            endDate         : function(){return mySelf.getEndDate();},
            duration        : function(tab){return (tab ? tab.duration : mySelf.getDuration());},
            rentAmountType  : function(){return mySelf.getRentAmountType();},
            rentSpcReducJustif  : function(){return mySelf.getRentSpecialReductionJustification();},
            rentSpcReducBillLbl : function(){return mySelf.getRentSpecialReductionBillLabel();},
            rentBaseAmount  : function(){return mySelf.getRentBaseAmount();},
            rentAmount      : function(tab){return (tab ? tab.rentAmount : mySelf.getRentAmount());},
            rentTotal       : function(tab){return (tab ? tab.rentTotal : mySelf.getRentTotal());},
            transportDeliveryAmount : function(tab){return (tab ? tab.tabTransport.tabDelivery.amount : mySelf.getTransportDeliveryAmount());},
            transportRecoveryAmount : function(tab){return (tab ? tab.tabTransport.tabRecovery.amount : mySelf.getTransportRecoveryAmount());},
            transportDelivery: function(tab){return mySelf._transportObj.getSideCfgs('d');},
            transportRecovery: function(tab){return mySelf._transportObj.getSideCfgs('r');},
            otherServicesAmount : function(tab){return mySelf._servicesObj.getTotal();},
            otherServicesCfgs   : function(tab){return mySelf._servicesObj.getConfigs();},
            total           : function(tab){return (tab ? tab.total : mySelf.getTotal());},
            proforma        : function(tab){return (!tab && mySelf._proformaObj.getAction() ? 1 : 0);},
            documents       : function(tab){return (!tab && mySelf._documentsObj.getActions().length > 0 ? 1 : 0);}
    };
    this._tabPropertiesKeys = [];
    for (var p in this._tabProperties)
    {
        this._tabPropertiesKeys.push(p);
    }
    this._tabColumnsProperties = {
            datesToConfirm  : ['datesToConfirm'],
            beginDate       : ['beginDate'],
            endDate         : ['endDate'],
            duration        : ['duration', 'rentAmountType'],
            modelLabel      : ['modelId'],
            rentAmount      : ['rentBaseAmount', 'rentAmount', 'rentSpcReducJustif', 'rentSpcReducBillLbl'],
            rentTotal       : ['rentTotal'],
            transportDeliveryAmount : ['transportDelivery'],
            transportRecoveryAmount : ['transportRecovery'],
            otherServicesAmount : ['otherServicesAmount', 'otherServicesCfgs'],
            total           : ['total'],
            proforma        : ['proforma']
    };

    // Données de la ligne
    if (id)
    {
        this._id = Number(id);
        this._editMode = 'update';
    }
    else
    {
        this._id = this._estimateObj.generateTempLineId();
        this._editMode = 'create';
    }
    this._stateId        = (tabPreloadInfos.stateId || RENTESTIMATELINE_STATE_ACTIVE);
    this._proformaFlags  = (tabPreloadInfos.proformaFlags || 0);
    this._total          = (tabPreloadInfos.total || 0);
    this._isAccepted     = (tabPreloadInfos.isAccepted == 1);
    this._linkedContract = (tabPreloadInfos.linkedContract || null);

    this._quantity            = 1;

    this._model = {
        id : 0,
        label : '',
        isFuelNeeded : false,
        tariffFamilyLabel : '',
        tabModelUseRates : [[[0, 0], [0, 0], [0, 0]], [[0, 0], [0, 0], [0, 0]]],
        load : '',
        isCleaningPossible : false,
        machinesFamilyLabel : '',
        isAccessory: false
    };
    this._modelLoadStatus = '';

    // Indique si le modèle est impératif
    this._isImperativeModel   = false;

    this._isAcceptable        = false;
    this._convertUrl          = '#';
    this._tabRights           = null;
    this._possibilities       = null;
    this._modificationDate    = '';
    this._tabValorizationUrls = null;

    // Valeurs initiales des propriétés de la ligne
    this._tabInitValues = {};

    // Etat pour le chargement
    this._loadStatus = ''; // loading, ok, error
    // Indique si les données de la ligne dans le tableau doivent être réaffichées
    this._isToUpdate = false;


    this.onload        = null; // function(id)
    this.onchangemodel = null; // function(id, oldId)


    // Initialisation
    this._init();
}

Location.RentEstimateLine.prototype = {

    /**
     * Activer la ligne
     *
     */
    active : function()
    {
        if (!this.isActive())
        {
            var estimateObj = this.getEstimateObject();
            var oldLineObj = estimateObj._currentLineObj;
            estimateObj._currentLineObj = this;
            if (oldLineObj)
            {
                oldLineObj.display();
            }

            // Evénements
            Location.Rent_Helper_DurationView.initEvents('durationHlp_', this._durationObj);
            Location.Rent_Helper_RentTariffView.initEvents('rentTariffHlp_', this._rentTariffObj);
            Location.Rent_Helper_TransportView.initEvents('transportHlp_', this._transportObj);
            Location.Rent_Helper_ServicesView.initEvents('servicesHlp_', this._servicesObj);
            Location.Rent_Helper_ProformaView.initEvents('proformaHlp_', this._proformaObj);
            Location.Rent_Helper_DocumentsView.initEvents('documentsHlp_', this._documentsObj);
            Location.Rent_Helper_HistoryView.initEvents('historyHlp_', this._historyObj);

            if (this._loadStatus == '')
            {
                // Chargement des données
                this._load();
            }
            else
            {
                this.display();
            }
        }
    },

    /**
     * Copier les informations d'une ligne vers la courante
     *
     * @param Location.RentEstimateLine lineObj
     * @return bool
     */
    copy : function(lineObj)
    {
        if (lineObj.getEstimateObject() != this.getEstimateObject())
        {
            return false;
        }

        var tabProps = ['quantity', 'model', 'isImperativeModel'];

        var i, prop;
        var nb = tabProps.length;
        for (i = 0; i < nb; i++)
        {
            prop = '_' + tabProps[i];
            this[prop] = LOC_Common.clone(lineObj[prop]);
        }
        // Copie des objets de tarifications location et transport
        if (!this._rentTariffObj.copy(lineObj._rentTariffObj) ||
            !this._transportObj.copy(lineObj._transportObj) ||
            !this._durationObj.copy(lineObj._durationObj) ||
            !this._servicesObj.copy(lineObj._servicesObj) ||
            !this._proformaObj.copy(lineObj._proformaObj) ||
            !this._documentsObj.copy(lineObj._documentsObj))
        {
            return false;
        }

        this.display();
        return true;
    },

    /**
     * Mettre à jour l'affichage de la ligne
     *
     */
    display : function()
    {
        // Mise à jour de la ligne dans le tableau
        this._displayLine();
        if (this._loadStatus == '' || !this._rowEl)
        {
            return false;
        }
        this._displayControls();
        this._displayTabBoxes();

        return true;
    },

    /**
     * Récupérer la date de début de location
     *
     * @return string
     */
    getBeginDate : function()
    {
        return this._durationObj.getBeginDate();
    },

    /**
     * Récupérer la date de fin de location
     *
     * @return string
     */
    getEndDate : function()
    {
        return this._durationObj.getEndDate();
    },

    /**
     * Indique si la date est à confirmer
     *
     * @return bool
     */
    isDatesToConfirm : function()
    {
        return this._durationObj.isDatesToConfirm();
    },


    /**
     * Récupérer la durée du contrat en jours calendaires
     *
     * @return int
     */
    getCalendarDuration : function()
    {
        return this._durationObj.getCalendarDuration();
    },

    /**
     * Récupérer le montant de nettoyage
     *
     * @return float
     */
    getCleaningAmount : function()
    {
        return this._servicesObj.getCleaningAmount();
    },

    /**
     * Récupérer l'id du type de nettoyage
     *
     * @return int
     */
    getCleaningTypeId : function()
    {
        return this._servicesObj.getCleaningTypeId();
    },

    /**
     * Récupérer les données de la ligne de devis
     *
     * @return Object
     */
    getData : function(subAction)
    {
        var tab;
        if (this.isRemoved())
        {
            tab = {
                '@action' : 'delete',

                'id' :      this.getId()
            };
        }
        else
        {
            tab = {
                '@action' : (this.getEditMode() == 'create' ? 'insert' : 'update'),
                '@subAction' : (subAction || ''),

                'id' :      this.getId(),

                // Matériel
                'model.id' : this.getModelId(),
                'quantity' : this.getQuantity(),
                'isImperativeModel' : this.isImperativeModel(),

                // Durée
                'beginDate'        : this.getBeginDate(),
                'endDate'          : this.getEndDate(),
                'duration'         : this.getDuration(),
                'calendarDuration' : this.getCalendarDuration(),
                'isDatesToConfirm' : this.isDatesToConfirm(),

                'tabDaysActions'    : this._durationObj.getDaysActions(),

                // Location
                'tabRentTariff' : this._rentTariffObj.getData(),

                // Transport
                'tabTransport' : this._transportObj.getData(),

                // Assurance
                'tabInsurance' : {
                    'typeId' : this.getInsuranceTypeId(),
                    'rate' : this.getInsuranceRate()
                },

                // Gestion de recours
                'tabAppeal' : {
                    'typeId' : this._servicesObj.getAppealTypeId(),
                    'value'  : this._servicesObj.getAppealValue()
                },

                // Nettoyage
                'tabCleaning' : {
                    'typeId' : this.getCleaningTypeId(),
                    'amount' : this.getCleaningAmount()
                },

                // Participation au recyclage
                'residues' : {
                    'mode'  : this._servicesObj.getResiduesMode(),
                    'value' : this._servicesObj.getResiduesValue()
                },

                // Services de location et transport
                'tabRentServices' : this._servicesObj.getRentServices(),

                // Pro formas
                'tabProformaAction' : this._proformaObj.getAction(),

                // Documents
                'tabDocumentsActions' : this._documentsObj.getActions(),

                // Autres
                'modificationDate' : this._modificationDate
            };
        }
        return tab;
    },

    /**
     * Récupérer la durée de location
     *
     * @return int
     */
    getDuration : function()
    {
        return this._durationObj.getDuration();
    },

    /**
     * Récupérer le mode d'édition (Création ou modification)
     *
     * @return string ('create', 'update')
     */
    getEditMode : function()
    {
        return this._editMode;
    },

    /**
     * Récupérer l'instance du devis associé
     *
     * @return Location.RentEstimate
     */
    getEstimateObject : function()
    {
        return this._estimateObj;
    },

    /**
     * Récupérer le montant unitaire (litre) du carburant
     *
     * @return float
     */
    getFuelUnitPrice : function()
    {
        return this._servicesObj.getFuelUnitPrice();
    },

    /**
     * Récupérer l'identifiant de la ligne du devis
     *
     * @return int
     */
    getId : function()
    {
        return this._id;
    },

    /**
     * Récupérer l'index de la ligne
     *
     * @return int
     */
    getIndex : function()
    {
        return this._rowEl.rowIndex - 1;
    },

    /**
     * Récupérer le montant d'assurance
     *
     * @return float
     */
    getInsuranceAmount : function()
    {
        return this._servicesObj.getInsuranceAmount();
    },

    /**
     * Récupérer le taux d'assurance
     *
     * @return float
     */
    getInsuranceRate : function()
    {
        return this._servicesObj.getInsuranceRate();
    },

    /**
     * Récupérer le type d'assurance
     *
     * @return int
     */
    getInsuranceTypeId : function()
    {
        return this._servicesObj.getInsuranceTypeId();
    },

    /**
     * Récupérer le libellé de la famille de machines
     *
     * @return string
     */
    getMachinesFamilyLabel : function()
    {
        return this._model.machinesFamilyLabel;
    },

    /**
     * Récupérer l'id du modèle de machine
     *
     * @return int
     */
    getModelId : function()
    {
        return this._model.id;
    },

    /**
     * Récupérer le libellé du modèle de machine
     *
     * @return string
     */
    getModelLabel : function()
    {
        return this._model.label;
    },

    /**
     * Récupérer l'encombrement du modèle de machine
     *
     * @return int
     */
    getModelLoad : function()
    {
        return this._model.load;
    },

    /**
     * Récupérer le type de l'objet courant
     *
     * @return string ('estimateLine')
     */
    getObjectType : function()
    {
        return 'estimateLine';
    },

    /**
     * Récupérer la quantité
     *
     * @return int
     */
    getQuantity : function()
    {
        return this._quantity;
    },

    /**
     * Récupérer le montant du tarif de location final
     *
     * @return float
     */
    getRentAmount : function()
    {
        return this._rentTariffObj.getAmount();
    },

    /**
     * Récupérer le type de montant pour le tarif de location
     *
     * @return string ('day' = par jour, 'month' = par mois)
     */
    getRentAmountType : function()
    {
        return this._rentTariffObj.getAmountType();
    },

    /**
     * Récupérer le tarif de base de location
     *
     * @return float
     */
    getRentBaseAmount : function()
    {
        return this._rentTariffObj.getBaseAmount();
    },

    /**
     * Récupérer le montant de remise maximium sur le tarif de location
     *
     * @return float
     */
    getRentMaxiReductionAmount : function()
    {
        return this._rentTariffObj.getMaxiReductionAmount();
    },

    /**
     * Récupérer le pourcentage de remise maximium sur le tarif de location
     *
     * @return float
     */
    getRentMaxiReductionPercent : function()
    {
        return this._rentTariffObj.getMaxiReductionPercent();
    },

    /**
     * Récupérer le pourcentage de remise recommandé sur le tarif de location
     *
     * @return float
     */
    getRentRecoReductionPercent : function()
    {
        return this._rentTariffObj.getRecoReductionPercent();
    },

    /**
     * Récupérer le montant de remise globale sur le tarif de location
     *
     * @return float
     */
    getRentReductionAmount : function()
    {
        return this._rentTariffObj.getReductionAmount();
    },

    /**
     * Récupérer le pourcentage de remise globale sur le tarif de location
     *
     * @return float
     */
    getRentReductionPercent : function()
    {
        return this._rentTariffObj.getReductionPercent();
    },

    /**
     * Récupérer le montant de remise exceptionnelle sur le tarif de location
     *
     * @return float
     */
    getRentSpecialReductionAmount : function()
    {
        return this._rentTariffObj.getSpecialReductionAmount();
    },

    /**
     * Récupérer le libellé de la facture de la remise exceptionnelle sur le tarif de location
     *
     * @return string
     */
    getRentSpecialReductionBillLabel : function()
    {
        return this._rentTariffObj.getSpecialReductionBillLabel();
    },

    /**
     * Récupérer le justificatif de la remise exceptionnelle sur le tarif de location
     *
     * @return string
     */
    getRentSpecialReductionJustification : function()
    {
        return this._rentTariffObj.getSpecialReductionJustification();
    },

    /**
     * Récupérer le pourcentage de remise exceptionnelle sur le tarif de location
     *
     * @return float
     */
    getRentSpecialReductionPercent : function()
    {
        return this._rentTariffObj.getSpecialReductionPercent();
    },

    /**
     * Récupérer l'état de la remise exceptionnelle sur le tarif de location
     *
     * @return string
     */
    getRentSpecialReductionStatus : function()
    {
        return this._rentTariffObj.getSpecialReductionStatus();
    },

    /**
     * Récupérer le montant de remise standard sur le tarif de location
     *
     * @return float
     */
    getRentStandardReductionAmount : function()
    {
        return this._rentTariffObj.getStandardReductionAmount();
    },

    /**
     * Récupérer le pourcentage de remise standard sur le tarif de location
     *
     * @return float
     */
    getRentStandardReductionPercent : function()
    {
        return this._rentTariffObj.getStandardReductionPercent();
    },

    /**
     * Calcule le montant total de location.
     *
     * @return float
     */
    _calculateRentTotal : function()
    {
        var amountType = this.getRentAmountType();
        var amount = this.getRentAmount();
        if (amountType == 'day')
        {
            amount = amount * this.getDuration();
        }
        else if (amountType == 'month')
        {
            amount = amount * (this.getDuration() / 21);
        }
        return amount;
    },

    /**
     * Récupérer le montant total de location
     *
     * @return float
     */
    getRentTotal : function()
    {
        return LOC_Common.round(this._calculateRentTotal(), 2);
    },

    /**
     * Récupérer le montant de base pour le calcul de la participation au recyclage
     *
     * @return float
     */
    getResiduesBaseAmount : function()
    {
        return this._calculateRentTotal();
    },

    /**
     * Récupérer l'instance de la ligne dans le tableau des lignes de devis
     *
     * @return Object
     */
    getRowElt : function()
    {
        return this._rowEl;
    },

    /**
     * Récupérer le contrat associé si la ligne a été convertit
     *
     * @return Object
     */
    getLinkedContract : function()
    {
        return this._linkedContract;
    },

    /**
     * Récupérer l'état de chargement de la ligne
     *
     * @return string
     */
    getLoadStatus : function()
    {
        return this._loadStatus;
    },

    /**
     * Récupérer l'état du devis
     *
     * @return string
     */
    getStateId : function()
    {
        return this._stateId;
    },

    /**
     * Récupérer le libellé de la famille tarifaire
     *
     * @return string
     */
    getTariffFamilyLabel : function()
    {
        return this._model.tariffFamilyLabel;
    },

    /**
     * Récupérer le montant total de la ligne du devis
     *
     * @return float
     */
    getTotal : function()
    {
        if (this._loadStatus == '')
        {
            return this._total;
        }
        // le total normal rajoutait des chiffres après la virgule (ex 2410.720000003) à cause de l'imprécision lors de l'addition de nombres à virgule flottante
        // solution : arrondir à 2 chiffres après la virgule : LOC_Common.round(nombre, nb de décimales)
        return LOC_Common.round(Number(this.getRentTotal() +
                      this.getTransportDeliveryAmount() +
                      this.getTransportRecoveryAmount() +
                      this._servicesObj.getTotal()), 2);
    },

    /**
     * Récupérer le nombre total de jours supplémentaires
     *
     * @return string
     */
    getTotalAdditionalDays  : function()
    {
        return this._durationObj.getTotalAdditionalDays();
    },

    /**
     * Récupérer le nombre total de jours déduits
     *
     * @return string
     */
    getTotalDeductedDays : function()
    {
        return this._durationObj.getTotalDeductedDays();
    },

    /**
     * Récupérer l'objet estimation du transport
     *
     * @return Location.Rent_Helper_Transport
     */
    getTransportObject : function()
    {
        return this._transportObj;
    },

    /**
     * Récupérer le montant final de l'estimation du transport
     *
     * @return int
     */
    getTransportAmount : function()
    {
        return this._transportObj.getAmount();
    },

    /**
     * Récupérer le montant du tarif transport de base
     *
     * @return int
     */
    getTransportBaseAmount : function()
    {
        return this._transportObj.getBaseAmount();
    },

    /**
     * Récupérer le montant du transport Aller
     *
     * @return int
     */
    getTransportDeliveryAmount : function()
    {
        return this._transportObj.getDeliveryAmount();
    },

    /**
     * Récupérer le montant de la remise commerciale maximum du transport
     *
     * @return int
     */
    getTransportMaxiReductionAmount : function()
    {
        return this._transportObj.getMaxiReductionAmount();
    },

    /**
     * Récupérer le pourcentage de la remise commerciale maximum du transport
     *
     * @return int
     */
    getTransportMaxiReductionPercent : function()
    {
        return this._transportObj.getMaxiReductionPercent();
    },

    /**
     * Récupérer le pourcentage de la remise commerciale recommandée du transport
     *
     * @return int
     */
    getTransportRecoReductionPercent : function()
    {
        return this._transportObj.getRecoReductionPercent();
    },

    /**
     * Récupérer le montant du transport Retour
     *
     * @return int
     */
    getTransportRecoveryAmount : function()
    {
        return this._transportObj.getRecoveryAmount();
    },

    /**
     * Récupérer le montant de remise globale du transport
     *
     * @return int
     */
    getTransportReductionAmount : function()
    {
        return this._transportObj.getReductionAmount();
    },

    /**
     * Récupérer le poucentage de remise globale du transport
     *
     * @return int
     */
    getTransportReductionPercent : function()
    {
        return this._transportObj.getReductionPercent();
    },

    /**
     * Récupérer le montant de la remise exceptionnelle du transport
     *
     * @return int
     */
    getTransportSpecialReductionAmount : function()
    {
        return this._transportObj.getSpecialReductionAmount();
    },

    /**
     * Récupérer le poucentage de remise exceptionnelle du transport
     *
     * @return int
     */
    getTransportSpecialReductionPercent : function()
    {
        return this._transportObj.getSpecialReductionPercent();
    },

    /**
     * Récupérer le code statut de la remise exceptionnelle de transport
     *
     * @return string
     */
    getTransportSpecialReductionStatus : function()
    {
        return this._transportObj.getSpecialReductionStatus();
    },

    /**
     * Récupérer le montant de la remise commerciale du transport
     *
     * @return int
     */
    getTransportStandardReductionAmount : function()
    {
        return this._transportObj.getStandardReductionAmount();
    },

    /**
     * Récupérer le pourcentage de la remise commerciale appliquée du transport
     *
     * @return int
     */
    getTransportStandardReductionPercent : function()
    {
        return this._transportObj.getStandardReductionPercent();
    },

    /**
     * Récupérer l'instance de l'onglet Documents
     *
     * @return Location.Rent_Helper_Documents
     */
    getDocumentsObject : function()
    {
        return this._documentsObj;
    },

    /**
     * Indique si la ligne de devis contient des erreurs
     *
     * @return bool
     */
    hasErrors : function()
    {
        return (this._tabErrors.length > 0);
    },

    /**
     * Indique si la ligne de devis a été modifiée ou non
     *
     * @return bool
     */
    hasModifications : function(tabProperties)
    {
        if (typeof tabProperties == 'undefined')
        {
            tabProperties = this._tabPropertiesKeys;
        }

        for (var i = 0; i < tabProperties.length; i++)
        {
            var p = tabProperties[i];
            if (this._tabProperties[p]() != this._tabInitValues[p])
            {
                return true;
            }
        }
        return false;
    },

    /**
     * Indique si la ligne de devis est acceptable ou désacceptable
     *
     * @return bool
     */
    isAcceptable : function()
    {
        return (this._loadStatus == 'ok' && this._modelLoadStatus == 'ok'
                && this.getEstimateObject().getEditMode() == 'update'
                && this._isAcceptable && !this.isRemoved()
                && this.hasRight('actions', 'accept'));
    },

    /**
     * Indique si la ligne de devis est acceptée
     *
     * @return bool
     */
    isAccepted : function()
    {
        return this._isAccepted;
    },

    /**
     * Indique si la ligne de devis est active
     *
     * @return bool
     */
    isActive : function()
    {
        return (this === this._estimateObj._currentLineObj);
    },

    /**
     * Indique si la ligne est dupliquable ou non
     *
     * @retrun bool
     */
    isDuplicable : function()
    {
        var estimateObj = this.getEstimateObject();
        return (this._loadStatus == 'ok' && this._modelLoadStatus == 'ok' && estimateObj.hasRight('actions', 'addLine') &&
                estimateObj.getLinesCount(2) < estimateObj.getMaxLinesCount());
    },

    /**
     * Indique si le modèle est impératif
     *
     * @return bool
     */
    isImperativeModel : function()
    {
        return this._isImperativeModel;
    },

    /**
     * Est ce que le momdèle de machine est changeable ?
     *
     * @return bool
     */
    isModelChangeable : function()
    {
        return (this.getEstimateObject().getCustomerObject().getId() != 0 && this.hasRight('machine', 'model'));
    },

    /**
     * Indique si la ligne est supprimable ou non
     *
     * @retrun bool
     */
    isRemovable : function()
    {
        if (this._loadStatus == 'ok' && this._modelLoadStatus == 'ok' && !this.isRemoved() && this.hasRight('actions', 'remove'))
        {
            var estimateObj = this.getEstimateObject();
            if (estimateObj.getEditMode() == 'update')
            {
                return (this.getEditMode() == 'create' || estimateObj.getLinesCount(1) > 1);
            }
            else
            {
                return (estimateObj.getLinesCount() > 1);
            }
        }
        return false;
    },

    /**
     * Indique si la ligne est supprimée
     *
     * @return bool
     */
    isRemoved : function()
    {
        return this._isRemoved;
    },

    /**
     * Indique si la ligne est annulable ou non
     *
     * @retrun bool
     */
    isResetable : function()
    {
        if (this.getEditMode() == 'update')
        {
            return (this.isRemoved() || this._loadStatus == 'error' || (this._loadStatus == 'ok' && this._modelLoadStatus == 'ok'));
        }
        return false;
    },

    /**
     * Indique si la ligne peut être passée en contrat ou non
     *
     * @retrun bool
     */
    isConvertible : function()
    {
        return (this._loadStatus == 'ok' && this._modelLoadStatus == 'ok'
                && this._convertUrl != '#' && !this.isRemoved()
                && !this.hasModifications() && this.hasRight('actions', 'convert'));
    },

    /**
     * Indique si il y a un forfait machine supplémentaire pour le transport Aller
     *
     * @return bool
     */
    isTransportAddMachineDelivery : function()
    {
        return this._transportObj.isAddMachineDelivery();
    },

    /**
     * Indique si la ligne est validable ou non
     *
     * @retrun bool
     */
    isValidable : function()
    {
        return (this._loadStatus == 'ok' && this._modelLoadStatus == 'ok' &&
                this.getEstimateObject().getEditMode() == 'update' && this.hasRight('actions', 'valid'));
    },

    /**
     * Est-il possible d'imprimer le bon de valorisation ?
     *
     * @return bool
     */
    isValorizationPrintable : function()
    {
        return (this.hasRight('actions', 'valorizationPrint') && this._tabValorizationUrls && !this.isRemoved() &&
                this._proformaObj.getActiveCount() == 0 && !this.getEstimateObject().getCustomerObject().isProformaRequired() &&
                this.getStateId() != RENTESTIMATELINE_STATE_INACTIVE);
    },

    /**
     * Supprimer la ligne du devis
     *
     */
    remove : function()
    {
        if (this.isRemovable())
        {
            this._isRemoved = true;
            this.display();
        }
    },

    /**
     * Annuler les modifications apportées sur la ligne du devis
     *
     */
    reset : function()
    {
        var mySelf = this;
        if (this.isResetable())
        {
            if (this.isRemoved())
            {
                this._isRemoved = false;
                this.display();
            }
            else
            {
                if ((!this.hasModifications() && !this._documentsObj.addList.hasPending()) ||
                    confirm(LOC_View.getTranslation('answerAreYouSureResetLine')))
                {
                    // Annulation des téléversements en cours
                    this._documentsObj.addList.cancelPendings();

                    this._modelLoadStatus = '';
                    this._loadStatus = '';
                    this._load();
                }
            }
        }
    },

    /**
     * Détermine les interactions entre les modules/helpers
     *
     */
    setAllExternalParams : function()
    {
        // calcul de paramètres
        var estimateObj = this.getEstimateObject();
        var customerObj = estimateObj.getCustomerObject();

        var beginDateObj = new Date(this._durationObj.getBeginDate());
        var endDateObj   = new Date(this._durationObj.getEndDate());

        var servicesDisabledComponents = {};
        var durationDisabledComponents = {};

        // Désactivation d'action sur les jours
        if (this._possibilities && this._possibilities.duration)
        {
            if (!this._possibilities.duration.setInvoiceableDay)
            {
                durationDisabledComponents.setInvoiceable = true;
            }
            if (!this._possibilities.duration.setNonInvoiceableDay)
            {
                durationDisabledComponents.setNonInvoiceable = true;
            }
        }
        // Assurance
        // - type et taux non modifiables pour les clients en assurance facturée négociée
        // - taux non modifiable et taux par défaut est le seul taux possible
        var insuranceTypeInfos = this._servicesObj.getInsuranceTypeInfos();
        if (insuranceTypeInfos && insuranceTypeInfos.isInvoiced)
        {
            servicesDisabledComponents.insuranceRate = true;
            if (this._servicesObj.getInsuranceRate() != this._tabCfgs.defaultInsuranceRate)
            {
                servicesDisabledComponents.insuranceType = true;
            }
        }
        // - Gestion de recours désactivée si assurance non flaguée "client"
        if (insuranceTypeInfos && !insuranceTypeInfos.isCustomer)
        {
            servicesDisabledComponents.appeal = true;
        }

        var proformaDispMsgs = [];
        var proformaDisabledComponents   = {};
        var isProformaRequired = customerObj.isProformaRequired();
        var isProformaActived  = (this._proformaObj.getActiveCount() > 0);

        var d = this._durationObj.getDuration();

        if (this._rentTariffObj.getAmountType() == 'month')
        {
            proformaDispMsgs.push('rentAmountType');
            proformaDisabledComponents.add        = [PFMTYPE_ID_PROFORMA, PFMTYPE_ID_ADVANCE, PFMTYPE_ID_EXTENSION];
            proformaDisabledComponents.reactivate = [PFMTYPE_ID_PROFORMA, PFMTYPE_ID_ADVANCE, PFMTYPE_ID_EXTENSION];
        }

        var fuelUnitPrice = customerObj.getFwkCttFuelUnitPrice();
        var isCalcDisplayed = (fuelUnitPrice !== null && fuelUnitPrice != this._servicesObj.getFuelUnitPrice());

        var tabPeriodLimitDates = this._durationObj.getPeriodLimitDates();
        // - beginDateWindow : limite droite et gauche pour la date de début de la ligne de devis
        var beginDateWindowFrom    = null;
        var beginDateWindowTill = tabPeriodLimitDates.beginDateMax ? tabPeriodLimitDates.beginDateMax.toISOString().substring(0, 10) : null;

        // - endDateWindow : limite droite et gauche pour la date de fin du contrat
        var endDateWindowFrom = tabPeriodLimitDates.endDateMin ? tabPeriodLimitDates.endDateMin.toISOString().substring(0, 10) : null;
        var endDateWindowTill    = null;

        // Mise à jour des paramètres externes
        this._rentTariffObj.setDays(d);
        this._rentTariffObj.setExternalParams({
            isProformaRequired: isProformaRequired,
            isProformaActived: isProformaActived
        });

        this._durationObj.setExternalParams({
            beginDateWindow: [beginDateWindowFrom, beginDateWindowTill],
            endDateWindow: [endDateWindowFrom, endDateWindowTill],
            disabledComponents: durationDisabledComponents
        });

        this._servicesObj.setExternalParams({
            isFuelNeeded: this._model.isFuelNeeded,
            totalDays: this._durationObj.getTotalDays(),
            duration: d,
            calendarDuration: this._durationObj.getCalendarDuration(),
            rentDayPrice: this._rentTariffObj.getDayPrice(),
            isFullService : estimateObj.isFullService(),
            isModelAccessory: this._model.isAccessory,
            customerInsurance : (customerObj.getId() != 0 ? {
                typeId: customerObj.getInsuranceTypeId(),
                rate: customerObj.getInsuranceRate()
            } : null),
            customerAppeal : (customerObj.getId() != 0 ? {
                typeId: customerObj.getAppealTypeId()
            } : null),
            isCustomerResiduesInvoiced: (customerObj.getId() != 0 ? customerObj.isResiduesInvoiced() : null),
            residuesBaseAmount: this.getResiduesBaseAmount(),
            isCalcDisplayed: isCalcDisplayed,
            disabledComponents: servicesDisabledComponents
        });

        this._proformaObj.setExternalParams({
            duration: d,
            fuelQtyEstimByDay: this._model.fuelQtyEstimByDay,
            fuelQtyMax: this._model.fuelQtyMax,
            isFuelNeeded: this._model.isFuelNeeded,
            isCalcDisplayed: isCalcDisplayed,
            displayMessages: proformaDispMsgs,
            disabledComponents: proformaDisabledComponents
        });

        // Recalcul des éventuelles erreurs
        estimateObj._checkErrors();
    },

    /**
     * Définir le montant du nettoyage
     *
     * @param float amount Montant
     * @return bool
     */
    setCleaningAmount : function(amount)
    {
        return this._servicesObj.setCleaningAmount(amount);
    },

    /**
     * Définir le type de nettoyage
     *
     * @param int id Id du type
     * @param bool isUpdatedAmount Mise à jour du montant ? (true par défaut)
     * @return bool
     */
    setCleaningTypeId : function(id, isUpdatedAmount)
    {
        return this._servicesObj.setCleaningTypeId(id, isUpdatedAmount);
    },

    /**
     * Définir si la ligne de devis contient des erreurs
     *
     * @param Array|false tabErrors   Liste des erreurs
     * @param int         mode        (0: set, 1: add, 2: del)
     */
    setErrors : function(tabErrors, mode)
    {
        var nb = (tabErrors ? tabErrors.length : 0);
        if (mode == 0)
        {
            this._tabErrors = [];
        }
        if (mode == 2)
        {
            var pos;
            for (var i = 0; i < nb; i++)
            {
                pos = array_search(tabErrors[i], this._tabErrors);
                if (pos !== false)
                {
                    this._tabErrors.splice(pos, 1);
                }
            }
        }
        else
        {
            for (var i = 0; i < nb; i++)
            {
                array_push_unique(this._tabErrors, tabErrors[i]);
            }
        }
    },

    /**
     * Définir si le modèle est impératif ou non
     *
     * @param bool isImperative
     * @return bool
     */
    setImperativeModel : function(isImperative)
    {
        isImperative = (isImperative ? true : false);

        if (this._isImperativeModel == isImperative || !this.hasRight('machine', 'isImperativeModel'))
        {
            return false;
        }
        this._isImperativeModel = isImperative;
        this.display();
        return true;
    },

    /**
     * Définir l'id du modèle de machine
     *
     * @param int  modelId  Identifiant du modèle de machine
     * @param bool isForced Forcer la mise à jour
     * @return bool
     */
    setModelId : function(modelId, isForced)
    {
        if (!modelId || (this._model.id == modelId && !isForced))
        {
            return false;
        }
        modelId = Number(modelId);
        if (!this.isModelChangeable())
        {
            return false;
        }
        var mySelf = this;
        this._modelLoadStatus = 'loading';
        this.display();

        if (!this._modelHttpObj)
        {
            this._modelHttpObj = new Location.HTTPRequest(this._tabCfgs.modelInfosUrl);
        }
        var estimateObj = this.getEstimateObject();
        this._modelHttpObj.setVar('modelId', modelId);
        this._modelHttpObj.setVar('customerId', estimateObj.getCustomerId());
        this._modelHttpObj.setVar('agencyId', estimateObj.getAgencyId());
        this._modelHttpObj.setVar('countryId', estimateObj.getCountryId());
        this._modelHttpObj.oncompletion = function()
        {
            var tabModelInfos = this.getJSONResponse();
            if (tabModelInfos)
            {
                var tabOldInfos = {id: mySelf._model.id, isCleaningPossible: mySelf._model.isCleaningPossible};
                // Mise à jour des informations du modèle de machines
                mySelf._loadModelInfos(tabModelInfos);
                if (mySelf._model.id != 0)
                {
                    mySelf._rentTariffObj.setTariffs(tabModelInfos.tabTariffs);

                    // Si le nouveau modèle peut avoir du nettoyage facturé
                    // et que le client est facturé
                    // et que le nettoyage n'était pas facturé précédemment, alors on facture le nettoyage standard
                    var isStdForced = (estimateObj.getCustomerObject().isCleaningInvoiced() &&
                                       mySelf._servicesObj.getCleaningTypeId() == 0 &&
                                       !tabOldInfos.isCleaningPossible && mySelf._model.isCleaningPossible);

                    mySelf._servicesObj.setCleaningTypes(tabModelInfos.tabCleaningTypes, isStdForced);
                    mySelf.setAllExternalParams();
                }

                mySelf.display();

                // Evénement
                if (mySelf.onchangemodel)
                {
                    mySelf.onchangemodel(mySelf._model.id, tabOldInfos.id);
                }
            }
            else
            {
                this.onerror();
            }
        }
        this._modelHttpObj.onerror = function()
        {
            mySelf._modelLoadStatus = 'error';
            mySelf.display();
        }
        this._modelHttpObj.send(true, true);
        return true;
    },

    /**
     * Définir le montant du tarif de location final
     *
     * @param float amount
     * @return bool
     */
    setRentAmount : function(amount)
    {
        return this._rentTariffObj.setAmount(amount);
    },

    /**
     * Définir le type de montant pour le tarif de location
     *
     * @param string type ('day' = par jour, 'month' = par mois)
     * @return bool
     */
    setRentAmountType : function(type)
    {
        return this._rentTariffObj.setAmountType(type);
    },

    /**
     * Définir le montant de remise globale sur le tarif de location
     *
     * @param float amount Montant
     * @return bool
     */
    setRentReductionAmount : function(amount)
    {
        return this._rentTariffObj.setReductionAmount(amount);
    },

    /**
     * Définir le pourcentage de remise globale sur le tarif de location
     *
     * @param float percent Pourcentage
     * @return bool
     */
    setRentReductionPercent : function(percent)
    {
        return this._rentTariffObj.setReductionPercent(percent);
    },

    /**
     * Définir le montant de remise exceptionnelle sur le tarif de location
     *
     * @param float amount Montant
     * @return bool
     */
    setRentSpecialReductionAmount : function(amount)
    {
        return this._rentTariffObj.setSpecialReductionAmount(amount);
    },

    /**
     * Définir le libellé de la facture de la remise exceptionnelle sur le tarif de location
     *
     * @param string text Libellé de la facture
     * @return bool
     */
    setRentSpecialReductionBillLabel : function(text)
    {
        return this._rentTariffObj.setSpecialReductionBillLabel(text);
    },

    /**
     * Définir le justificatif de la remise exceptionnelle sur le tarif de location
     *
     * @param string text Justificatif
     * @return bool
     */
    setRentSpecialReductionJustification : function(text)
    {
        return this._rentTariffObj.setSpecialReductionJustification(text);
    },

    /**
     * Définir le pourcentage de remise standard sur le tarif de location
     *
     * @param float percent Pourcentage
     * @return bool
     */
    setRentSpecialReductionPercent : function(percent)
    {
        return this._rentTariffObj.setSpecialReductionPercent(percent);
    },

    /**
     * Définir le montant de remise standard sur le tarif de location
     *
     * @param float amount Montant
     * @return bool
     */
    setRentStandardReductionAmount : function(amount)
    {
        return this._rentTariffObj.setStandardReductionAmount(amount);
    },

    /**
     * Définir le pourcentage de remise standard sur le tarif de location
     *
     * @param float percent Pourcentage
     * @return bool
     */
    setRentStandardReductionPercent : function(percent)
    {
        return this._rentTariffObj.setStandardReductionPercent(percent);
    },

    /**
     * Valider les modifications apportées sur la ligne du devis
     *
     * @return bool
     */
    validate : function(subAction)
    {
        var estimateObj = this.getEstimateObject();
        if (estimateObj.getAbortReasonId() != 0 || !this.isValidable())
        {
            return false;
        }
        return estimateObj.validate([this.getId()], subAction);
    },

    /**
     * Mise à jour de l'affichage des boutons de contrôles
     *
     */
    _displayControls : function()
    {
        if (!this.isActive())
        {
            return;
        }
        var estimateObj = this.getEstimateObject();
        var mySelf = this;

        var pfx = 'rentEstimateLine';
        var isUpdateBtnsVisibles = (estimateObj.getEditMode() == 'update');
        var isAborted = (estimateObj.getAbortReasonId() != 0);
        // Bouton de validation
        $ge(pfx + 'ValidBtn').className = 'locCtrlButton' + (!isAborted && this.isValidable() ? '' : ' disabled');
        $ge(pfx + 'ValidBtn').style.display = (isUpdateBtnsVisibles ? '' : 'none');
        // Bouton d'annulation
        $ge(pfx + 'ResetBtn').className = 'locCtrlButton' + (this.isResetable() ? '' : ' disabled');
        $ge(pfx + 'ResetBtn').style.display = (isUpdateBtnsVisibles ? '' : 'none');
        // Bouton de suppression
        $ge(pfx + 'RemoveBtn').className = 'locCtrlButton' + (this.isRemovable() ? '' : ' disabled');
        // Bouton de duplication
        $ge(pfx + 'DuplicateBtn').className = 'locCtrlButton' + (this.isDuplicable() ? '' : ' disabled');

        // Groupe d'icônes "Actions"
        var btnsGrp1El = $ge(pfx + 'ActionBtns');
        if (btnsGrp1El)
        {
            btnsGrp1El.style.display = 'none';

            // Boutons d'acceptations
            var acceptEls = [$ge(pfx + 'Accept'), $ge(pfx + 'CancelAccept')];
            acceptEls[0].style.display = 'none';
            acceptEls[1].style.display = 'none';
            if (this.isAcceptable())
            {
                btnsGrp1El.style.display = '';
                acceptEls[(this.isAccepted() ? 1 : 0)].style.display = '';
            }

            // Bouton de passage en contrat
            var toCttEl = $ge(pfx + 'ToContractBtn');
            toCttEl.style.display = 'none';
            if (this.isConvertible())
            {
                btnsGrp1El.style.display = '';
                toCttEl.style.display = '';
                toCttEl.onclick = function() {
                    if (Location.Rent_Helper_DocumentsView.checkPendingUploads('documentsHlp_', mySelf._documentsObj, function() {
                            toCttEl.onclick();
                        }, null, {
                            force: LOC_View.getTranslation('ignorePendingDocsAndConvertLine'),
                            wait:  LOC_View.getTranslation('waitPendingDocsAndConvertLine'),
                            back:  LOC_View.getTranslation('goBackToEstimate')
                        }))
                    {
                        return false;
                    }
                    window.open(mySelf._convertUrl);
                }
            }
        }

        // Groupe d'icônes "Bon de valorisation"
        var btnsGrp2El = $ge(pfx + 'VlzPrintBtns');
        if (btnsGrp2El)
        {
            // Boutons d'impression du bon de valorisation
            var isDisp = this.isValorizationPrintable();
            if (isDisp)
            {
                btnsGrp2El.style.display = '';
                btnsGrp2El.className = 'cpanel_grp' + (btnsGrp1El.style.display != 'none' ? ' cpanel_sep_l' : '');
                var printBtns = $ge('valorizationPrintBtns');

                var tabElts = printBtns.getElementsByTagName('a');
                for (var i = 0; i < tabElts.length; i++)
                {
                    tabElts[i].href = this._tabValorizationUrls[tabElts[i].className];
                }

                printBtns.style.visibility = 'visible';
            }
            else
            {
                btnsGrp2El.style.display = 'none';
            }
        }
    },

    /**
     * Récupérer les informations sur les onglets: activé ou non, état, tableau d'erreurs
     *
     * @return Array
     */
    _getTabsInfos : function()
    {
        var customerObj = this.getEstimateObject().getCustomerObject();

        var tab = [];

        tab[TABINDEX_EQUIPMENT] = [true,  '', {}];
        tab[TABINDEX_DURATION]  = [false, '', {}];
        tab[TABINDEX_RENT]      = [false, '', {}];
        tab[TABINDEX_TRANSPORT] = [false, '', {}];
        tab[TABINDEX_SERVICES]  = [false, '', {}];
        tab[TABINDEX_PROFORMA]  = [false, '', {}];
        tab[TABINDEX_DOCUMENTS] = [false, '', {}];
        tab[TABINDEX_HISTORY]   = [false, '', {}];

        if (this.getModelId() != 0)
        {
            tab[TABINDEX_DURATION][0] = true;
        }
        if (this.getModelId() != 0 && this.getDuration() != '' && this.getRentAmountType() != '')
        {
            tab[TABINDEX_RENT][0] = true;
            tab[TABINDEX_TRANSPORT][0] = true;
            tab[TABINDEX_SERVICES][0] = true;
            tab[TABINDEX_DOCUMENTS][0] = true;
            tab[TABINDEX_PROFORMA][0] = true;
            tab[TABINDEX_HISTORY][0] = (this.getEditMode() == 'update');
        }

        // Warnings
        // - Le nombre de jours ouvrés a changé
        if (this._durationObj.isRealDurationChanged())
        {
            tab[TABINDEX_DURATION][1] = 'warning';
        }
        // - Remise négative ou tarif a changé
        if (this.getRentReductionPercent() < 0 || this._rentTariffObj.isTariffChange())
        {
            tab[TABINDEX_RENT][1] = 'warning';
        }
        if (this._transportObj.isEstimationChange())
        {
            tab[TABINDEX_TRANSPORT][1] = 'warning';
        }

        // Affichage d'un message d'information sur les lignes de devis vérifiant les cas suivants sur l'assurance
        var insuranceTypeInfos = this._servicesObj.getInsuranceTypeInfos();
        var isNegotiatedInsuranceLine = insuranceTypeInfos && insuranceTypeInfos.isInvoiced &&
                                        this._servicesObj.getInsuranceRate() != this._tabCfgs.defaultInsuranceRate;

        var customerInsuranceTypeInfos = customerObj.getInsuranceTypeInfos();
        var isNegotiatedInsuranceCustomer = customerInsuranceTypeInfos && customerInsuranceTypeInfos.isInvoiced &&
                                            customerObj.getInsuranceRate() != this._tabCfgs.defaultInsuranceRate;

        var isDiffNegotiatedInsuranceCustomer = customerInsuranceTypeInfos && customerInsuranceTypeInfos.isInvoiced &&
                                                customerObj.getInsuranceRate() != this._servicesObj.getInsuranceRate();

        if (// Ligne de devis non passée en contrat
            !this.getLinkedContract() &&
            // Devis non Full Service ET
            !this.getEstimateObject().isFullService() && (
                // -- Assurance de la ligne négociée et assurance du client non négociée
                //    OU Assurance de la ligne négociée et assurance du client négociée à des taux différents
                isNegotiatedInsuranceLine && (!isNegotiatedInsuranceCustomer || isDiffNegotiatedInsuranceCustomer) ||
                // -- OU Assurance de la ligne non négociée et assurance du client négociée
                !isNegotiatedInsuranceLine && isNegotiatedInsuranceCustomer))
        {
            tab[TABINDEX_SERVICES][1] = 'warning';
            tab[TABINDEX_SERVICES][2].isInsuranceChanged = true;
        }

        // Affichage d'un message d'information sur les lignes de devis vérifiant les cas suivants sur la participation au recyclage
        if (// Ligne de devis non passée en contrat ET
            !this.getLinkedContract() &&
            // facturation de la participation au recyclage ET
            this._servicesObj.getResiduesMode() != RESIDUES_MODE_NONE &&
            (// mode enregistré sur la ligne différent du mode par défaut à la date de valeur
             this._servicesObj.getResiduesMode() != this._tabCfgs.defaultResiduesMode ||
             // OU valeur enregistrée sur la ligne différente de la valeur par défaut
             (this._servicesObj.getResiduesMode() == RESIDUES_MODE_PRICE && this._servicesObj.getResiduesValue() != this._tabCfgs.defaultResiduesAmount) ||
             (this._servicesObj.getResiduesMode() == RESIDUES_MODE_PERCENT && this._servicesObj.getResiduesValue() != this._tabCfgs.defaultResiduesRate)
            ))
        {
            tab[TABINDEX_SERVICES][1] = 'warning';
            tab[TABINDEX_SERVICES][2].isResiduesModeChanged = true;
        }

        // Errors
        var nbErrors = this._tabErrors.length;
        for (var i = 0; i < nbErrors; i++)
        {
            var error = this._tabErrors[i];

            // Modèle non renseigné
            if (error == 105)
            {
                tab[TABINDEX_EQUIPMENT][1] = 'error';
            }
            // Durée incorrecte
            else if (error == 107)
            {
                tab[TABINDEX_DURATION][1] = 'error';
            }
            // Date de début non renseignée
            else if (error == 108)
            {
                tab[TABINDEX_DURATION][1] = 'error';
            }
            // Nombre de jours ouvrés à recalculer
            if (error == 110)
            {
                tab[TABINDEX_DURATION][1] = 'error';
            }
            // Tarif de location non défini
            else if (error == 201)
            {
                tab[TABINDEX_RENT][1] = 'error';
            }
            // Justificatif de la remise exceptionnelle obligatoire
            else if (error == 202)
            {
                tab[TABINDEX_RENT][1] = 'error';
                tab[TABINDEX_RENT][2].noJustification = true;
            }
            // Erreurs tarification transport
            else if (error == 104 || error == 106 || (error >= 300 && error < 400))
            {
                tab[TABINDEX_TRANSPORT][1] = 'error';
                if (error == 304 || error == 308)
                {
                    tab[TABINDEX_TRANSPORT][2].deliveryAmount = true;
                }
                if (error == 305 || error == 309)
                {
                    tab[TABINDEX_TRANSPORT][2].recoveryAmount = true;
                }
            }
            // Erreurs documents
            else if (error == 700)
            {
                tab[TABINDEX_DOCUMENTS][1] = 'error';
            }
        }

        // Indicateur pour la pro forma à générer
        tab[TABINDEX_PROFORMA][1] = (isProformaToGen({lineStateId:this.getStateId(),isCustomerRequired:customerObj.isProformaRequired(),totalAmount:this.getTotal(),flags:this._proformaFlags}) ? 'warning' : '');

        // Indicateur nb de documents
         tab[TABINDEX_DOCUMENTS][2].badge = this._documentsObj.list.getList().length;

        return tab;
    },

    /**
     * Mise à jour de l'affichage de la ligne
     *
     */
    _displayLine : function()
    {
        var lineId      = this.getId();
        var estimateObj = this.getEstimateObject();
        var customerObj = estimateObj.getCustomerObject();
        var editMode    = this.getEditMode();

        var className = (this._rowEl.rowIndex % 2 ? '' : 'even ') +
                        'state_' + this.getStateId() +
                        (this.isActive() ? ' selected' : '') +
                        (this.isRemoved() ? ' removed' : '') +
                        (this.isAccepted() && estimateObj.getStateId() != RENTESTIMATE_STATE_ABORTED ? ' accepted' : '') +
                        (this.hasErrors() ? ' errors' : '') +
                        ' editmode_' + editMode +
                        (this._loadStatus != '' ? ' loadstatus_' + this._loadStatus : '');

        var tdElt;
        if (this._loadStatus == 'ok')
        {
            // Date
            if (this._isToUpdate || editMode == 'create')
            {
                // Dates à confirmer
                tdElt = estimateObj._getTabLinesCell(this._rowEl, 'datesToConfirm');
                tdElt.firstChild.className = (this.isDatesToConfirm() ? 'on' : 'off');
                // Dates de début et de fin de loc
                tdElt = estimateObj._getTabLinesCell(this._rowEl, 'beginDate');
                tdElt.innerHTML = LOC_View.getDateFormat(this.getBeginDate());
                tdElt = estimateObj._getTabLinesCell(this._rowEl, 'endDate');
                tdElt.innerHTML = LOC_View.getDateFormat(this.getEndDate());
                // Durée
                tdElt = estimateObj._getTabLinesCell(this._rowEl, 'duration');
                var duration = this.getDuration();
                tdElt.firstChild.innerHTML = (duration != 0 ? duration : '');
                tdElt.lastChild.className = 'changed ' + (this._durationObj.isRealDurationChanged() ? 'on' : 'off');
                // Modèle de machine
                tdElt = estimateObj._getTabLinesCell(this._rowEl, 'modelLabel');
                var el = tdElt.firstChild.firstChild;
                el.title = el.innerHTML = this.getModelLabel();

                // Prix de loc, Total de loc, Transport Aller, Transport Retour, Assurance, Total
                for (var currencyName in this._tabCurrencies)
                {
                    tdElt = estimateObj._getTabLinesCell(this._rowEl, currencyName);
                    this._tabCurrencies[currencyName].setValue(this._tabProperties[currencyName](), false);
                }

                // Pro forma
                tdElt = estimateObj._getTabLinesCell(this._rowEl, 'proforma');
                var a = this._proformaObj.getAction();
                tdElt.firstChild.className = 'icon ' + (a && a.type == 'add' ? ' active' : '');

                // Affichage de la ligne mise à jour
                this._isToUpdate = false;
            }

            // Affichage des petits crayons
            if (editMode == 'update')
            {
                var isEditedField;
                for (var columnName in this._tabColumnsProperties)
                {
                    tdElt = estimateObj._getTabLinesCell(this._rowEl, columnName);
                    if (tdElt)
                    {
                        isEditedField = this.hasModifications(this._tabColumnsProperties[columnName]);
                        tdElt.className = columnName + (isEditedField ?  ' edited' : '');
                        tdElt.title = '';
                        var reducState = '';
                        var reducType = '';
                        if (columnName == 'rentAmount')
                        {
                            reducType = 'RENT';
                            reducState = this.getRentSpecialReductionStatus();
                        }
                        else if (columnName == 'transportDeliveryAmount' || columnName == 'transportRecoveryAmount')
                        {
                            reducType = 'TRSP';
                            reducState = this.getTransportSpecialReductionStatus();
                        }
                        if (reducState != '')
                        {
                            tdElt.className += ' ' + reducState;
                            tdElt.title = LOC_View.getTranslation('spcReducTypeLabel_' + reducType) + ' : ' +
                                          LOC_View.getTranslation('spcReducStateLabel_' + reducState);
                        }
                    }
                }
                if (this.hasModifications())
                {
                    className += ' edited';
                }
            }
        }

        if (editMode == 'update')
        {
            // Etat des pro formas
            var cn = 'icon';
            if (this._proformaFlags & PFMDOCFLAG_EXISTACTIVEDEXPIRED)
            {
                cn += ' expired';
            }
            else if (this._proformaFlags & PFMDOCFLAG_EXISTACTIVEDFINALIZED)
            {
                cn += ' finalized';
            }
            else if (this._proformaFlags & PFMDOCFLAG_EXISTACTIVED)
            {
                cn += ' actived';
            }
            tdElt = estimateObj._getTabLinesCell(this._rowEl, 'proforma');
            tdElt.firstChild.className = cn;
        }

        // Indicateur de pro forma à générer
        if (isProformaToGen({lineStateId:this.getStateId(),isCustomerRequired:customerObj.isProformaRequired(),totalAmount:this.getTotal(),flags:this._proformaFlags}))
        {
            className += ' proforma_togenerate';
        }

        // Bouton de visualisation du contrat
        var linkedContract = this.getLinkedContract();
        if (linkedContract)
        {
            tdElt = estimateObj._getTabLinesCell(this._rowEl, 'ctrls');
            tdElt.firstChild.href = linkedContract.url;
            tdElt.firstChild.title = linkedContract.title;
        }


        // Style de la ligne
        this._rowEl.className = className;
    },

    /**
     * Mise à jour de l'affichage du groupe d'onglets
     *
     */
    _displayTabBoxes : function()
    {
        // Mise à jour du groupe d'onglet
        if (!this.isActive())
        {
            return;
        }
        var editMode = this.getEditMode();

        // Mise à jour des informations dans les onglets
        if (this._loadStatus != 'loading')
        {
            var lineId = this.getId();

            // Activation/Désactivation des onglets
            // ************************************
            var tabBoxObj = Location.tabBoxesManager.getTabBox('rentEstimateLineTabBox');

            // Informations sur les onglets
            var tabTabsInfos = this._getTabsInfos();

            for (var i = 0; i < tabTabsInfos.length; i++)
            {
                var tabObj = tabBoxObj.getTab(i);
                if (tabObj)
                {
                    tabObj.setRentEstimateState(tabTabsInfos[i][1]);
                    tabObj.setDisabled(!tabTabsInfos[i][0]);
                    // Badge
                    if (tabTabsInfos[i][2].badge > 0)
                    {
                        tabObj.setBadge(tabTabsInfos[i][2].badge);
                    }
                    else
                    {
                        // Vide le badge
                        tabObj.setBadge(null);
                    }
                }
            }

            // Mise à jour des informations des différents onglets
            // ***************************************************
            switch (tabBoxObj.getActiveTabIndex())
            {
                // - Onglet "Matériel"
                case TABINDEX_EQUIPMENT:
                    var searchBoxObj = Location.searchBoxesManager.getSearchBox('rentEstimateLineModelId');
                    if (this._modelLoadStatus != 'loading')
                    {
                        var modelId = this.getModelId();
                        searchBoxObj.displayNullItem((modelId == 0), 0, '---');
                        if (!searchBoxObj._oldSearchLineId || searchBoxObj._oldSearchLineId != lineId)
                        {
                            searchBoxObj.setSearchValue('');
                            searchBoxObj._oldSearchLineId = lineId;
                        }
                        searchBoxObj.setValue(modelId, false);
                        searchBoxObj.setDisabled(!this.isModelChangeable());

                        // Affichage des familles
                        $ge('rentEstimateLineMachinesFamily').innerHTML = this.getMachinesFamilyLabel();
                        $ge('rentEstimateLineTariffFamily').innerHTML   = this.getTariffFamilyLabel();

                        // Affichage des taux d'utilisation
                        var useRateTableEl = $ge('rentEstimateLineModelUseRateTable');
                        useRateTableEl = useRateTableEl.tBodies[0];
                        var row, col, rate;
                        for (row = 0; row < 3; row++)
                        {
                            for (col = 0; col < 4; col++)
                            {
                                if (col % 2 == 0)
                                {
                                    rate = this._model.tabModelUseRates[col / 2][row][0];
                                    if (col == 0)
                                    {
                                        useRateTableEl.rows[row].style.display = (rate == null ? 'none' : '');
                                    }
                                    useRateTableEl.rows[row].cells[col + 1].innerHTML = LOC_View.getNumberFormat(rate * 100, 2) + ' %';
                                }
                                else
                                {
                                    var nbTotal = this._model.tabModelUseRates[(col - 1) / 2][row][1];
                                    useRateTableEl.rows[row].cells[col + 1].innerHTML = '(' + nbTotal + ')';
                                }
                            }
                        }
                    }
                    else
                    {
                        searchBoxObj.setDisabled(true);
                    }

                    $ge('rentEstimateLineModelUseRateBlock').className = this._modelLoadStatus;

                    var obj = $ge('rentEstimateLineIsImperativeModel');
                    obj.checked = this.isImperativeModel();
                    obj.disabled = (modelId == 0 || !this.hasRight('machine', 'isImperativeModel'));

                    var isQuantityDisplayed = (this.getQuantity() > 1);
                    $ge('rentEstimateLineQuantityBlock').style.visibility = (isQuantityDisplayed ? 'visible' : 'hidden');
                    if (isQuantityDisplayed)
                    {
                        var el = Location.spinControlsManager.getSpinControl('rentEstimateLineQuantity');
                        el.setValue(this.getQuantity(), false);
                        el.disable();
                    }
                    break;

                // - Onglet "Durée"
                case TABINDEX_DURATION:
                    Location.Rent_Helper_DurationView.display('durationHlp_', this._durationObj);
                    break;

                // - Onglet "Location"
                case TABINDEX_RENT:
                    Location.Rent_Helper_RentTariffView.display('rentTariffHlp_', this._rentTariffObj, tabTabsInfos[TABINDEX_RENT][2], true);
                    break;

                // - Onglet "Transport"
                case TABINDEX_TRANSPORT:
                    Location.Rent_Helper_TransportView.display('transportHlp_', this._transportObj, tabTabsInfos[TABINDEX_TRANSPORT][2]);
                    break;

                // - Onglet "Autres services"
                case TABINDEX_SERVICES:
                    Location.Rent_Helper_ServicesView.display('servicesHlp_', this._servicesObj, tabTabsInfos[TABINDEX_SERVICES][2]);
                    break;

                // - Onglet "Pro forma"
                case TABINDEX_PROFORMA:
                    Location.Rent_Helper_ProformaView.display('proformaHlp_', this._proformaObj);
                    break;

                // - Onglet "Documents"
                case TABINDEX_DOCUMENTS:
                    Location.Rent_Helper_DocumentsView.display('documentsHlp_', this._documentsObj);

                // - Onglet "Historique"
                case TABINDEX_HISTORY:
                    Location.Rent_Helper_HistoryView.display('historyHlp_', this._historyObj);
                    break;
            }
        }

        // Affichage de l'état de la ligne
        $ge('rentEstimateLineBlock').className = 'loadstatus_' + this._loadStatus + (this.isRemoved() ? ' removed' : '');
    },

    /**
     * Vérifier le droit à un champ
     *
     * @param string partName  Nom de la partie
     * @param string fieldName Nom du champ
     * @return bool
     */
    hasRight : function(partName, fieldName)
    {
        return (this._tabRights === null || (this._tabRights[partName] && this._tabRights[partName][fieldName]) ? true : false);
    },

    /**
     * Initialisation
     *
     */
    _init : function()
    {
        var mySelf = this;
        var estimateObj = this.getEstimateObject();
        var customerObj = estimateObj.getCustomerObject();

        // Onglet "Durée"
        this._durationObj = new Location.Rent_Helper_Duration(this, this._tabCfgs);
        this._durationObj.onchange = function(tabChanges)
        {
            mySelf.setAllExternalParams();
            mySelf.display();
        }

        // Onglet "Location"
        this._rentTariffObj = new Location.Rent_Helper_RentTariff(this, this._tabCfgs);
        this._rentTariffObj.onchange = function(tabChanges)
        {
            mySelf.setAllExternalParams();
            mySelf.display();
        }

        // Onglet "Transport"
        this._transportObj = new Location.Rent_Helper_Transport(this, this._tabCfgs);
        this._transportObj.onchange = function(tabChanges)
        {
            if (in_array('estimation', tabChanges))
            {
                // Si l'estimation tranport change alors on désactive l'impression du devis
                estimateObj.setPrintable(false);
            }
            mySelf.setAllExternalParams();
            mySelf.display();
        }

        // Onglet "Autres services"
        this._servicesObj = new Location.Rent_Helper_Services(this, this._tabCfgs);
        this._servicesObj.onchange = function(tabChanges)
        {
            if (mySelf.getEditMode() == 'create' && array_intersect(tabChanges, ['extParam_customerInsurance', 'extParam_customerAppeal']).length > 0)
            {
                this.resetInsurance();
                this.resetAppeal();
            }
            if ((mySelf.getEditMode() == 'create' && in_array('extParam_isCustomerResiduesInvoiced', tabChanges)) ||
                (mySelf._tabCfgs.useResiduesForAccessories == 0 && in_array('extParam_isModelAccessory', tabChanges)))
            {
                this.resetResidues();
            }
            mySelf.setAllExternalParams();
            mySelf.display();
        }

        // Onglet "Pro forma"
        this._proformaObj = new Location.Rent_Helper_Proforma(this, this._tabCfgs);
        this._proformaObj.onchange = function()
        {
            mySelf.setAllExternalParams();
            mySelf.display();
        }

        // Onglet "Documents"
        this._documentsObj = new Location.Rent_Helper_Documents(this, this._tabCfgs);
        this._documentsObj.onchange = function()
        {
            mySelf.setAllExternalParams();
            mySelf.display();
        }


        // Onglet "Historique"
        this._historyObj = new Location.Rent_Helper_History(this, this._tabCfgs);
        this._historyObj.onchange = function()
        {
            mySelf.display();
        }

        if (this.getEditMode() == 'create')
        {
            // Création de la ligne de devis
            var tableEl = $ge('rentEstimateLinesTable').tBodies[0];
            var cloneRowEl = $ge('rentEstimateLineClone');
            this._rowEl = cloneRowEl.cloneNode(true);
            tableEl.appendChild(this._rowEl);

            // Clonages des currencies
            var tdElt, currencyId, node;
            for (var currencyName in this._tabCurrencies)
            {
                tdElt = estimateObj._getTabLinesCell(this._rowEl, currencyName);
                currencyId = currencyName + '[' + this.getId() + ']';
                // Clonage de la zone de texte
                node = $ge(currencyName + '_clone').cloneNode(true);
                node.setAttribute('id', currencyId);
                node.setAttribute('name', currencyId);
                tdElt.appendChild(node);
                // Clonage du widget
                this._tabCurrencies[currencyName] = Location.currenciesManager.cloneCurrency(currencyId, currencyName + '_clone');
            }

            // Copie des informations de base
            var tabInfos = LOC_Common.clone(this._tabCfgs.tabNewLineInfos);
            tabInfos.id = this.getId();

            tabInfos.tabOtherServices.cleaningTypeId     = (customerObj.isCleaningInvoiced() ? -1 : 0);
            tabInfos.tabOtherServices.isResiduesInvoiced = (customerObj.isResiduesInvoiced() ? 1 : 0);

            this._loadInfos(tabInfos);
        }
        else
        {
            this._rowEl = $ge('rentEstimateLine_' + this._id);
            // Attribution des currencies
            var currencyId;
            for (var currencyName in this._tabCurrencies)
            {
                currencyId = currencyName + '[' + this.getId() + ']';
                this._tabCurrencies[currencyName] = Location.currenciesManager.getCurrency(currencyId);
            }

            this._loadStatus = '';
            this._modelLoadStatus = '';
        }




        // Evenements
        this._rowEl.onclick = function()
        {
            mySelf.active();
        }

        // Onglet actif
        if (this._tabCfgs.tabIndex != null)
        {
            var tabBoxObj = Location.tabBoxesManager.getTabBox('rentEstimateLineTabBox');
            tabBoxObj.setActiveTabIndex(this._tabCfgs.tabIndex);
        }
    },

    /**
     * Chargement des données
     *
     */
    _load : function()
    {
        if (this._loadStatus != '')
        {
            return false;
        }
        return this.getEstimateObject().loadLines([this.getId()]);
    },

    _loadModelInfos : function(tabModelInfos)
    {
        this._model.id = Number((tabModelInfos ? (tabModelInfos.id || 0) : 0));
        if (this._model.id != 0)
        {
            this._model.label               = (tabModelInfos.label || '');
            this._model.machinesFamilyLabel = (tabModelInfos.machinesFamilyLabel || '');
            this._model.isFuelNeeded        = (tabModelInfos.isFuelNeeded == 1);
            this._model.tariffFamilyLabel   = (tabModelInfos.tariffFamilyLabel || '');
            this._model.tabModelUseRates    = tabModelInfos.tabUseRates;
            this._model.load                = Number(tabModelInfos.load);
            this._model.isCleaningPossible  = (tabModelInfos.isCleaningPossible == 1);
            this._model.fuelQtyEstimByDay   = tabModelInfos.fuelQtyEstimByDay;
            this._model.fuelQtyMax          = tabModelInfos.fuelQtyMax;
            this._model.isAccessory         = tabModelInfos.isAccessory;
        }
        this._modelLoadStatus = 'ok';
    },

    _loadInfos : function(tabInfos)
    {
        this._tabErrors = [];
        this._tabWarnings = tabInfos['@warnings'] || [];
        this._tabErrorsDetails = tabInfos['@details'] || [];
        this._tabDocumentsActions = tabInfos['@documentsActions'] || [];

        var estimateObj = this.getEstimateObject();
        var customerObj = estimateObj.getCustomerObject();

        this._id = tabInfos.id;

        // Gestion des droits
        this._tabRights = tabInfos.tabRights;
        for (var part in this._tabRights)
        {
            var tabTmp = (this._tabRights[part] != '' ? this._tabRights[part].split('|') : []);
            this._tabRights[part] = {};
            for (var i = 0; i < tabTmp.length; i++)
            {
                this._tabRights[part][tabTmp[i]] = true;
            }
        }
        this._possibilities = tabInfos.possibilities;

        // Matériel
        this._loadModelInfos(tabInfos.tabModelInfos);
        this._quantity = Number(tabInfos.quantity);
        this._isAcceptable = (tabInfos.isAcceptable == 1);
        this._isAccepted = (tabInfos.isAccepted == 1);

        this._isImperativeModel = (tabInfos.isImperativeModel == 1);

        // Durée
        this._durationObj.load(tabInfos.tabDuration, {}, this._tabRights.duration);

        // Calculs Tarification Location
        this._rentTariffObj.load(tabInfos.tabRentTariff, this.getDuration(), tabInfos.tabModelInfos.tabTariffs,
                                 {isProformaRequired: customerObj.isProformaRequired(),
                                  isProformaActived: (tabInfos.tabProforma.isActived == 1)
                                 },
                                 this._tabRights.rentTariff);

        // Calculs Tarification Transport
        this._transportObj.load(tabInfos.tabTransport, {}, this._tabRights.transport);

        // Autres services
        this._servicesObj.load(tabInfos.tabOtherServices, tabInfos.tabModelInfos.tabCleaningTypes,
                               tabInfos.tabRentServices, 0,
                               {isFuelNeeded: this._model.isFuelNeeded,
                                isProformaActived: (tabInfos.tabProforma.isActived == 1),
                                rentDayPrice: this._rentTariffObj.getDayPrice(),
                                totalDays: this._durationObj.getTotalDays(),
                                duration: this._durationObj.getDuration(),
                                calendarDuration: this._durationObj.getCalendarDuration(),
                                isFullService : estimateObj.isFullService(),
                                isModelAccessory: this._model.isAccessory,
                                customerInsurance: (customerObj.getId() != 0 ? {
                                    typeId: customerObj.getInsuranceTypeId(),
                                    rate: customerObj.getInsuranceRate()
                                } : null),
                                customerAppeal: (customerObj.getId() != 0 ? {
                                    typeId: customerObj.getAppealTypeId()
                                } : null),
                                isCustomerResiduesInvoiced: (customerObj.getId() != 0 ? customerObj.isResiduesInvoiced() : null),
                                residuesBaseAmount: this.getResiduesBaseAmount()
                               },
                               this._tabRights.services);

        // Pro forma
        var proformaDispMsgs = [];
        var proformaDisabledComponents   = {};
        if (this._rentTariffObj.getAmountType() == 'month')
        {
            proformaDispMsgs.push('rentAmountType');
            proformaDisabledComponents.add        = [PFMTYPE_ID_PROFORMA, PFMTYPE_ID_ADVANCE, PFMTYPE_ID_EXTENSION];
            proformaDisabledComponents.reactivate = [PFMTYPE_ID_PROFORMA, PFMTYPE_ID_ADVANCE, PFMTYPE_ID_EXTENSION];
        }
        this._proformaObj.load(tabInfos.tabProforma,
                               {duration: this.getDuration(),
                                fuelQtyEstimByDay: this._model.fuelQtyEstimByDay,
                                fuelQtyMax: this._model.fuelQtyMax,
                                isFuelNeeded: this._model.isFuelNeeded,
                                displayMessages: proformaDispMsgs,
                                disabledComponents: proformaDisabledComponents},
                               this._tabRights.proforma);
        this._proformaFlags = tabInfos.proformaFlags;

        // Documents
        this._documentsObj.load(tabInfos.tabDocuments, {}, this._tabRights.documents);

        // Historique
        this._historyObj.load(tabInfos.tabHistory, {}, this._tabRights.history);

        // Calcul des paramètres externes pour tous les modules
        this.setAllExternalParams();

        // Autres informations
        this._stateId             = tabInfos.stateId;
        this._modificationDate    = tabInfos.modificationDate;
        this._convertUrl          = tabInfos.convertUrl;
        this._linkedContract      = tabInfos.linkedContract;
        this._tabValorizationUrls = tabInfos.tabValorizationUrls;

        this._rowEl.id = 'rentEstimateLine_' + this._id;

        // Sauvegarde des informations par défaut
        var p, nbProps = this._tabPropertiesKeys.length;
        for (var i = 0; i < nbProps; i++)
        {
            p = this._tabPropertiesKeys[i];
            this._tabInitValues[p] = this._tabProperties[p](tabInfos);
        }

        this._loadStatus = 'ok';
        this._isToUpdate = true;

        // Evénement
        if (this.onload)
        {
            this.onload(this._id);
        }
    }
}


/**
 * Classe de gestion du devis
 *
 * @param string countryId Pays
 * @param int    id        Identifiant du devis
 * @param Object tabCfgs   Configurations
 */
Location.RentEstimate = function(countryId, id, tabCfgs)
{
    // Liste des erreurs
    this._tabErrors = [];
    this._tabWarnings = [];
    // Configurations du devis
    this._tabCfgs = tabCfgs;
    // Instance de la ligne active
    this._currentLineObj = null;

    // Instance de la gestion du client
    this._customerObj = null;
    // Instance de la gestion du chantier
    this._siteObj = null;
    // Liste des instances de ligne de devis
    this._tabLinesObj = [];

    // Optimisations
    this._oldLinesCount = -1;   // Sauvegarde du nombre de lignes
    this._tabColumnsIndex = {}; // Index des colonnes dans le tableau (voir _getTabLinesCell())
    this._newLineCpt = 0;       // Compteur pour la génération de l'id temporaire des lignes (voir generateTempLineId())

    // Nombre de lignes max
    this._maxLinesCount = (tabCfgs.nbLinesMax || 4);
    this._tempId = (tabCfgs.tempId || 'TMP' + new Date().getTime());

    // Instance HttpRequest pour la validation (sauvegarde)
    this._saveHttpObj = null;
    this._getUrlHttpObj = null;
    this._loadStatus = 'ok';
    this._isLoadingLines = false;

    this._countryId = countryId;
    this._agencyId  = tabCfgs.tabInfos['agency.id'];

    // Données du devis
    this._id                = 0;
    this._code              = '';
    this._tracking          = '';
    this._comments          = '';
    this._abortReasonId     = 0;
    this._abortComments     = '';
    this._isFullService     = false;
    this._negotiatorId      = null;
    this._negotiatorFullName = '';
    this._modificationDate  = '';
    this._valueDate         = '';
    this._stateId           = RENTESTIMATE_STATE_ACTIVE;
    this._isPrintable       = false;
    this._tabErroneousLines = [];
    this._tabRights         = null;

    // Paramétrage de l'affichage des notifications
    this._tabNotifications = {};

    // Données de la ligne
    if (id)
    {
        this._id = Number(id);
        this._editMode = 'update';
    }
    else
    {
        this._editMode = 'create';
    }

    this._init();
}

Location.RentEstimate.prototype = {

    /**
     * Créer une nouvelle ligne de devis
     *
     * @return Location.RentEstimateLine
     */
    addLine : function()
    {
        // Impossible d'ajouter une ligne si le nombre maximum est atteind
        if (!this.hasRight('actions', 'addLine') || this.getLinesCount(2) >= this._maxLinesCount)
        {
            return false;
        }
        var lineObj = new Location.RentEstimateLine(this, null, {}, this._tabCfgs);
        this._tabLinesObj.push(lineObj);
        lineObj.active();
        this.display(2);
        return lineObj;
    },

    /**
     * Accepter de toutes les lignes
     *
     * @return bool
     */
    acceptAllLines : function()
    {
        var mySelf = this;

        var tabLines = this.getNotLoadedLinesList(function(lineObj) {
            return (lineObj.getStateId() == RENTESTIMATELINE_STATE_ACTIVE && !lineObj.isAccepted());
        });
        this.loadLines(tabLines, function() {
            mySelf.validate(null, 'accept');
        });

        return true;
    },

    /**
     * Annuler l'acceptation de toutes les lignes
     *
     * @return bool
     */
    unacceptAllLines : function()
    {
        var mySelf = this;

        var tabLines = this.getNotLoadedLinesList(function(lineObj) {
            return (lineObj.getStateId() == RENTESTIMATELINE_STATE_ACTIVE && lineObj.isAccepted());
        });
        this.loadLines(tabLines, function() {
            mySelf.validate(null, 'unaccept');
        });

        return true;
    },

    /**
     * Mettre à jour l'affichage du devis
     *
     * @param Object tabOptions
     */
    display : function(displayMode)
    {
        var editMode = this.getEditMode();
        var pfx = 'rentEstimate';

        // Mise à jour des styles des lignes et leurs index
        var linesCount = this.getLinesCount();
        if (linesCount != this._oldLinesCount)
        {
            var rowEl, lineIndex, nb = this._tabLinesObj.length;
            for (var i = 0; i < nb; i++)
            {
                rowEl = this._tabLinesObj[i].getRowElt();
                Location.commonObj.toggleEltClass(rowEl, 'even', (rowEl.rowIndex % 2 == 0));
                this._getTabLinesCell(rowEl, 'index').firstChild.innerHTML = this._tabLinesObj[i].getIndex();
            }
            this._oldLinesCount = linesCount;
        }

        // Bouton d'ajout de lignes
        var nbActiveLines = this.getLinesCount(2);
        var isBtnActived = (this.hasRight('actions', 'addLine') && nbActiveLines < this._maxLinesCount);
        $ge(pfx + 'AddLineBtnDiv').style.display = (isBtnActived ? '' : 'none');


        // Mise à jour des lignes
        if (displayMode == 1)
        {
            var nb = this._tabLinesObj.length;
            for (var i = 0; i < nb; i++)
            {
                this._tabLinesObj[i].display();
            }
        }
        // Mise à jour la ligne courante seulement
        else if (!displayMode && this._currentLineObj)
        {
            this._currentLineObj.display();
        }

        // Client
        Location.Rent_Helper_CustomerView.display('customerHlp_', this._customerObj, {});

        // Chantier
        Location.Rent_Helper_SiteView.display('rentSite', this._siteObj, {});

        // Affichage des commentaires
        $ge(pfx + 'Status').className = 'status ' + this.getStateId();
        $ge(pfx + 'ModificationDate').innerHTML = LOC_View.getDateFormat(this.getModificationDate(), true);

        // Informations complémentaires
        var el = $ge(pfx + 'Comments');
        el.value = this.getComments();
        el.disabled = !this.hasRight('infoscpl', 'comments');

        el = $ge(pfx + 'Tracking');
        el.value = this.getTracking();
        el.disabled = !this.hasRight('infoscpl', 'tracking');

        // Abandon de devis
        var isActived = (editMode == 'update' && this.getStateId() != RENTESTIMATE_STATE_CONTRACT);
        $ge('rentAbortFieldset').className = (isActived && this.hasRight('abort', 'reason') ? '' : 'disabled');
        if (isActived)
        {
            var selectElt = $ge(pfx + 'AbortReasonId');
            var textAreaElt = $ge(pfx + 'AbortComments');
            selectElt.value = this.getAbortReasonId();
            textAreaElt.value = this.getAbortComments();
            textAreaElt.disabled = (selectElt.value == 0);
            var isRequired = (selectElt.options[selectElt.selectedIndex].getAttribute('_requiredcom') == 1);
        }

        // Informations d'entête
        $ge(pfx + 'Code').value = this.getCode();

        el = $ge(pfx + 'IsFullService');
        el.checked = this.isFullService();
        el.disabled = (editMode == 'update' || !this.hasRight('infos', 'isFullService'));

        // Négociateur
        el = Location.searchBoxesManager.getSearchBox('rentEstimateNegotiatorId');
        if (this._negotiatorId != el.getValue())
        {
            el.clear();
            if (this._negotiatorId)
            {
                el.setElements([[this._negotiatorId, this._negotiatorFullName]], 1);
                el.setValue(this._negotiatorId);
            }
        }
        el.setDisabled(!this.hasRight('infos', 'negotiator'));

        // Traitement des erreurs
        var customerSearchBoxObj = Location.searchBoxesManager.getSearchBox('customer.id');
        customerSearchBoxObj.setRequired(false);
        var siteSearchBoxObj = Location.searchBoxesManager.getSearchBox('rentSiteLocalityId');
        siteSearchBoxObj.setRequired(false);
        var negotiatorSearchBoxObj = Location.searchBoxesManager.getSearchBox('rentEstimateNegotiatorId');
        negotiatorSearchBoxObj.setRequired(false);
        $ge('rentSiteLabel').className = '';
        $ge(pfx + 'AbortComments').className = '';
        $ge('rentSiteDeliveryHour.masked').className = '';
        $ge('rentSiteDeliveryTimeSlotId').className = '';

        // Tableau contenant les erreurs sur les onglets du site
        var tabSiteTabBoxError = [[[''], ['']], {}];
        var tabCustomerErrors = {};

        var nbErrors = this._tabErrors.length;
        for (var i = 0; i < nbErrors; i++)
        {
            var error = this._tabErrors[i];

            // Traitement de chaque erreur
            // Client non renseigné ou bloqué au devis
            if (error == 101 || error == 20)
            {
                customerSearchBoxObj.setRequired(true);
            }
            // Format mail contact commande
            else if (error == 31)
            {
                tabCustomerErrors.invalidOrderContactEmail = true;
            }
            // Libellé chantier non renseigné
            else if (error == 102)
            {
                $ge('rentSiteLabel').className = 'error';
            }
            // Ville chantier non renseignéde
            else if (error == 103)
            {
                siteSearchBoxObj.setRequired(true);
            }
            // Format mail contact chantier
            else if (error == 121)
            {
                tabSiteTabBoxError[0][SITETABINDEX_GENERAL]  = 'error';
                tabSiteTabBoxError[1].invalidContactEmail = true;
            }
            // Commentaire d'abandon vide
            else if (error == 109)
            {
                $ge(pfx + 'AbortComments').className = 'error';
            }
            // Négociateur non renseigné
            else if (error == 15)
            {
                negotiatorSearchBoxObj.setRequired(true);
            }
            // Horaire de livraison non renseigné
            else if (error == 125)
            {
                tabSiteTabBoxError[0][SITETABINDEX_INFOS]  = 'error';
                tabSiteTabBoxError[1].noDeliveryHour = true;
            }
            else if (error == 100)
            {
                var nbLines = this._tabLinesObj.length;
                for (var i = 0; i < nbLines; i++)
                {
                    var tabLineError = this._tabLinesObj[i]._tabErrors;
                    var nbErrors = tabLineError.length;
                    for (var j = 0 ; j < nbErrors ; j++)
                    {
                        var errorLine = tabLineError[j];
                        if (errorLine == 135)
                        {
                            tabSiteTabBoxError[0][SITETABINDEX_INFOS]  = 'error';
                            tabSiteTabBoxError[1].noDeliveryHour = true;
                        }
                    }
                }
            }
        }

        // Mise à jour des onglets du site
        var siteTabBoxObj = Location.tabBoxesManager.getTabBox('rentSiteTabBox');
        for (var i = 0; i < tabSiteTabBoxError[0].length; i++)
        {
            var tabObj = siteTabBoxObj.getTab(i);
            if (tabObj)
            {
                tabObj.setRentEstimateState(tabSiteTabBoxError[0][i]);
            }
        }
        // Mise à jour de l'affichage du site
        Location.Rent_Helper_SiteView.display('rentSite', this._siteObj, tabSiteTabBoxError[1]);

        // Mise à jour de l'affichage du client
        Location.Rent_Helper_CustomerView.display('customerHlp_', this._customerObj, tabCustomerErrors);

        // Bouton de validation
        $ge(pfx + 'ValidBtn').className = 'locCtrlButton' + (this.isValidable() ? '' : ' disabled');
        // Bouton d'annulation
        $ge(pfx + 'ResetBtn').className = 'locCtrlButton' + (this.isResetable() ? '' : ' disabled');

        // Groupe d'icônes "Actions"
        var btnsGrp1El = $ge(pfx + 'ActionBtns');
        if (btnsGrp1El)
        {
            btnsGrp1El.style.display = 'none';

            // Boutons d'acceptations
            var acceptEls = [$ge(pfx + 'AcceptAll'), $ge(pfx + 'CancelAcceptAll')];
            acceptEls[0].style.display = 'none';
            acceptEls[1].style.display = 'none';
            if (this.hasRight('actions', 'acceptAll'))
            {
                var nb = this._tabLinesObj.length;
                for (var i = 0; i < nb; i++)
                {
                    if (this._tabLinesObj[i].getStateId() == RENTESTIMATELINE_STATE_ACTIVE)
                    {
                        btnsGrp1El.style.display = '';
                        acceptEls[(this._tabLinesObj[i].isAccepted() ? 1 : 0)].style.display = '';
                    }
                }
            }
        }

        // Groupe d'icônes "Devis" (pour l'impression)
        var btnsGrp2El = $ge(pfx + 'PrintBtns');
        if (btnsGrp2El)
        {
            if (this.isPrintable())
            {
                btnsGrp2El.style.display = '';
                btnsGrp2El.className = 'cpanel_grp' + (btnsGrp1El.style.display != 'none' ? ' cpanel_sep_l' : '');
                $ge('printBtns').className = 'printButtons' + (this.hasErrors() ? ' disabled' : '');
            }
            else
            {
                btnsGrp2El.style.display = 'none';
            }
        }


        // Affichage de l'état du devis
        $ge(pfx + 'Block').className = this._loadStatus;

        // Affichage des notifications
        this._displayNotifications();

        return true;
    },

    /**
     * Dupliquer une ligne du devis
     *
     * @param string lineId Id de la ligne à dupliquer
     * @return Location.RentEstimateLine
     */
    duplicateLine : function(lineId)
    {
        if (!this.hasRight('actions', 'addLine'))
        {
            return false;
        }
        var lineObj = this.getLineObject(lineId);
        if (!lineObj || lineObj.getEstimateObject() != this || !lineObj.isDuplicable())
        {
            return false;
        }
        var newLineObj = new Location.RentEstimateLine(this, null, {}, this._tabCfgs);
        if (!newLineObj.copy(lineObj))
        {
            delete newLineObj;
            return false;
        }

        this._tabLinesObj.push(newLineObj);
        newLineObj.active();
        this.display(2);
        return newLineObj;
    },

    /**
     * Récupérer la liste des lignes non chargées
     *
     * @return Array
     */
    getNotLoadedLinesList : function(filtersFunc)
    {
        var tab = [];
        var nb = this._tabLinesObj.length;
        for (var i = 0; i < nb; i++)
        {
            var lineObj = this._tabLinesObj[i];
            if (lineObj.getLoadStatus() == '' && (!filtersFunc || filtersFunc(lineObj)))
            {
                tab.push(lineObj.getId());
            }
        }
        return tab;
    },

    /**
     * Récupérer le type de l'objet courant
     *
     * @return string ('estimate')
     */
    getObjectType : function()
    {
        return 'estimate';
    },

    /**
     * Générer un id de ligne de devis temporaire
     *
     * @return string
     */
    generateTempLineId : function()
    {
        return (this._tempId + '_' + this._newLineCpt++);
    },

    /**
     * Récupérer le mode d'édition (Création ou modification)
     *
     * @return string ('create', 'update')
     */
    getEditMode : function()
    {
        return this._editMode;
    },

    /**
     * Récupérer l'agence du devis
     *
     * @return string
     */
    getAgencyId : function()
    {
        return this._agencyId;
    },

    /**
     * Récupérer le code du devis
     *
     * @return string
     */
    getCode : function()
    {
        return this._code.replace(/(\*{1,})(FS|)$/, '$1' + (this._isFullService ? 'FS' : ''));
    },

    /**
     * Récupérer le commentaire du devis
     *
     * @return string
     */
    getComments : function()
    {
        return this._comments;
    },

    /**
     * Récupérer le pays du devis
     *
     * @return string
     */
    getCountryId : function()
    {
        return this._countryId;
    },

    /**
     * Récupérer les commentaires d'abandon du devis
     *
     * @return string
     */
    getAbortComments : function()
    {
        if (this._abortReasonId == 0)
        {
            return '';
        }
        return this._abortComments;
    },

    /**
     * Récupérer le motif d'abandon du devis
     *
     * @return int
     */
    getAbortReasonId : function()
    {
        return this._abortReasonId;
    },

    /**
     * Récupérer la ligne courante
     *
     * @return Location.RentEstimateLine
     */
    getActiveLineObject : function()
    {
        return this._currentLineObj;
    },

    /**
     * Récupérer l'identifiant du client du devis
     *
     * @return int
     */
    getCustomerId : function()
    {
        return this._customerObj.getId();
    },

    /**
     * Récupérer l'identifiant du client du devis
     *
     * @return int
     */
    getCustomerObject : function()
    {
        return this._customerObj;
    },

    /**
     * Récupérer les données du devis
     *
     * @param Array tabLines Id des lignes (si vide alors toutes les lignes sont sélectionnées)
     * @return Object
     */
    getData : function(tabLines, subAction)
    {
        var tabLinesData = [], i, nb;
        var lineObj;
        var isAllLines;
        if (tabLines)
        {
            isAllLines = false;
            nb = tabLines.length;
            for (i = 0; i < nb; i++)
            {
                lineObj = this.getLineObject(tabLines[i]);
                if (lineObj && lineObj.isValidable())
                {
                    tabLinesData.push(lineObj.getData(subAction));
                }
            }
        }
        else
        {
            nb = this._tabLinesObj.length;
            var editMode = this.getEditMode();
            for (i = 0; i < nb; i++)
            {
                lineObj = this._tabLinesObj[i];
                if (lineObj && (editMode == 'create' || lineObj.isValidable()))
                {
                    tabLinesData.push(lineObj.getData(subAction));
                }
            }
            isAllLines = true;
        }


        return {
            'id'            : this.getId(),
            '@action'       : (this.getEditMode() == 'create' ? 'insert' : 'update'),

            'tabCustomer'   : this._customerObj.getData(),
            'country.id'    : this.getCountryId(),
            'agency.id'     : this.getAgencyId(),

            'negotiator.id' : this.getNegotiatorId(),

            'tabSite'       : this._siteObj.getData(),

            'comments'      : this.getComments(),
            'tracking'      : this.getTracking(),

            'abortReason.id' : this.getAbortReasonId(),
            'abortComments'  : this.getAbortComments(),

            'isFullService' : (this.isFullService() ? 1 : 0),

            'tabLines'      : tabLinesData,
            'isAllLines'    : (isAllLines ? 1 : 0),

            'modificationDate' : this.getModificationDate()
        };
    },

    /**
     * Récupérer l'identifiant du devis
     *
     * @return int
     */
    getId : function()
    {
        return this._id;
    },

    /**
     * Récupérer l'id du négociateur
     *
     * @return int
     */
    getNegotiatorId : function()
    {
        return this._negotiatorId;
    },

    /**
     * Récupérer une instance d'une ligne
     *
     * @return Location.RentEstimateLine|null
     */
    getLineObject : function(id)
    {
        var nb = this._tabLinesObj.length;
        for (var i = 0; i < nb; i++)
        {
            if (this._tabLinesObj[i].getId() == id)
            {
                return this._tabLinesObj[i];
            }
        }
        return null;
    },

    /**
     * Récupérer la liste des lignes du devis
     *
     * @return Array
     */
    getLines : function()
    {
        return this._tabLinesObj;
    },

    /**
     * Récupérer le nombre de lignes du devis
     *
     * @param int mode Type de compte
     * @return int
     */
    getLinesCount : function(mode)
    {
        var nb = this._tabLinesObj.length;
        if (mode == 1)
        {
            // Nombre de lignes en modifications et non supprimées
            var cpt = 0;
            for (var i = 0; i < nb; i++)
            {
                if (this._tabLinesObj[i].getEditMode() == 'update' && !this._tabLinesObj[i].isRemoved())
                {
                    cpt++;
                }
            }
            return cpt;
        }
        if (mode == 2)
        {
            // Nombre de lignes non désactivées
            var cpt = 0;
            for (var i = 0; i < nb; i++)
            {
                if (this._tabLinesObj[i].getStateId() != RENTESTIMATELINE_STATE_INACTIVE)
                {
                    cpt++;
                }
            }
            return cpt;
        }
        return nb;
    },

    /**
     * Récupérer le nombre maximum de lignes du devis
     *
     * @return int
     */
    getMaxLinesCount : function()
    {
        return this._maxLinesCount;
    },

    /**
     * Récupérer la date de modification du devis
     *
     * @return string
     */
    getModificationDate : function()
    {
        return this._modificationDate;
    },

    /**
     * Récupérer la date de valeur du devis
     *
     * @return string
     */
    getValueDate : function()
    {
        return this._valueDate;
    },

    /**
     * Récupérer l'instance du chantier
     *
     * @return Location.Rent_Helper_Site
     */
    getSiteObject : function()
    {
        return this._siteObj;
    },

    /**
     * Récupérer l'état du devis
     *
     * @return string
     */
    getStateId : function()
    {
        return this._stateId;
    },

    /**
     * Récupérer les commentaires de suivi du devis
     *
     * @return string
     */
    getTracking : function()
    {
        return this._tracking;
    },

    /**
     * Récupérer la zone du devis
     *
     * @return int
     */
    getZone : function()
    {
        return this._siteObj.getZone();
    },

    /**
     * Indique si le devis contient des erreurs ou non
     *
     * @return bool
     */
    hasErrors : function()
    {
        return (this._tabErrors.length > 0);
    },

    /**
     * Indique si le devis est en FullService ou non
     *
     * @return bool
     */
    isFullService : function()
    {
        return this._isFullService;
    },

    /**
     * Indique si le devis est imprimable ou non
     *
     * @return bool
     */
    isPrintable : function()
    {
        return this._isPrintable;
    },

    /**
     * Indique si le contrat peut être rétabli ou non
     *
     * @return bool
     */
    isResetable : function()
    {
        return this.isValidable();
    },

    /**
     * Indique si le devis est validable ou non
     *
     * @retrun bool
     */
    isValidable : function()
    {
        return (this._loadStatus != 'loading' && this.hasRight('actions', 'valid') && !this._isLoadingLines);
    },

    /**
     * Chargement des données des lignes du devis
     *
     * @param Array tabLines Id des lignes à charger
     * @return bool
     */
    loadLines : function(tabLines, onCompletion)
    {
        var i = 0, nb = tabLines.length;
        var lineObj;
        var tabLinesAssociatedObj = [];
        var tabLinesId = [];
        for (i = 0; i < nb; i++)
        {
            lineObj = this.getLineObject(tabLines[i]);
            if (lineObj && lineObj.getLoadStatus() != 'loading')
            {
                lineObj._loadStatus = 'loading';
                lineObj.display();

                tabLinesAssociatedObj.push(lineObj);
                tabLinesId.push(lineObj.getId());
            }
        }
        if (tabLinesId.length == 0)
        {
            if (onCompletion)
            {
                onCompletion();
            }
            return true;
        }

        var mySelf = this;
        this._isLoadingLines = true;
        this.display(2);


        // Instanciation de l'objet HTTPRequest pour la récupération des données de la ligne de devis
        var httpObj = new Location.HTTPRequest(this._tabCfgs.loadLinesUrl);
        httpObj.setVar('countryId', this.getCountryId());
        httpObj.setVar('tabLines', tabLinesId);
        httpObj.setVar('estimateId', this.getId());

        httpObj.oncompletion = function()
        {
            var tabInfos = this.getJSONResponse();
            if (tabInfos)
            {
                var i = 0, nb = tabLinesAssociatedObj.length;
                var lineObj;
                for (i = 0; i < nb; i++)
                {
                    lineObj = tabLinesAssociatedObj[i];
                    if (lineObj)
                    {
                        if (tabInfos.tabLines[i]['@state'] == 'ok')
                        {
                            lineObj._loadInfos(tabInfos.tabLines[i]);
                        }
                        else
                        {
                            lineObj._loadStatus = 'error';
                        }
                        lineObj.display();
                    }
                }
                mySelf._isLoadingLines = false;

                if (onCompletion)
                {
                    onCompletion();
                }

                mySelf.display(2);
            }
            else
            {
                // Erreur
                this.onerror();
            }
        }
        httpObj.onerror = function()
        {
            // Erreur
            var i = 0, nb = tabLinesAssociatedObj.length;
            var lineObj;
            for (i = 0; i < nb; i++)
            {
                lineObj = tabLinesAssociatedObj[i];
                if (lineObj)
                {
                    lineObj._loadStatus = 'error';
                    lineObj.display();
                }
            }
            mySelf._isLoadingLines = false;
            mySelf.display(2);
        }
        httpObj.send(true, true);
        return true;
    },

    /**
     * Supprimer une ligne du devis
     *
     * @param string lineId Id de la ligne à supprimer
     * @return bool
     */
    removeLine : function(lineId, isForced)
    {
        var lineObj = this.getLineObject(lineId);
        if (!lineObj || lineObj.getEstimateObject() != this || (!isForced && !lineObj.isRemovable()))
        {
            return false;
        }

        isRemove = true;
        if (!isForced)
        {
            if (lineObj.getEditMode() == 'update')
            {
                isRemove = false;
                lineObj.remove();
            }
            else if (!confirm(LOC_View.getTranslation('answerAreYouSureRemoveLine')))
            {
                isRemove = false;
            }
        }

        if (isRemove)
        {
            // Suppression de l'objet associé
            var i = 0, nb = this._tabLinesObj.length;
            while (i < nb && this._tabLinesObj[i].getId() != lineId)
            {
                i++;
            }
            if (i < nb)
            {
                delete lineObj;
                this._tabLinesObj.splice(i, 1);
                nb--;

                // Suppression de la ligne du tableau
                var tableEl = $ge('rentEstimateLinesTable').tBodies[0];
                var rowEl = $ge('rentEstimateLine_' + lineId);
                tableEl.removeChild(rowEl);

                // Activation de la première ligne précédente valide
                while (i >= nb)
                {
                    i--;
                }
                if (i >= 0)
                {
                    this._tabLinesObj[i].active();
                }
                else
                {
                    // Plus de lignes
                    this._currentLineObj = null;
                }
            }

            this.display(2);
        }

        return true;
    },

    /**
     * Annuler les modifications du devis
     *
     */
    reset : function()
    {
        var mySelf = this;

        if (!this.isResetable())
        {
            return false;
        }

        if (confirm(LOC_View.getTranslation('answerAreYouSureReset')))
        {
            // Annulation des téléversements en cours
            this.getLines().forEach(function(line) {
                line._documentsObj.addList.cancelPendings();
            });

            LOC_View.reload();
        }
    },

    /**
     * Définir les commentaires d'abandon du devis
     *
     * @param string comments
     * @return bool
     */
    setAbortComments : function(comments)
    {
        comments = (comments + '').trim();
        if (this._abortComments == comments)
        {
            return false;
        }
        this._abortComments = comments;
        this.display(2);
        return true;
    },

    /**
     * Définir le motif d'abandon du devis
     *
     * @param int id
     * @return bool
     */
    setAbortReasonId : function(id)
    {
        id = Number(id);
        if (this._abortReasonId == id)
        {
            return false;
        }
        this._abortReasonId = id;
        this.display(0);
        return true;
    },

    /**
     * Définir la ligne courante
     *
     * @param int Id de la ligne de devis
     * @return Location.RentEstimateLine
     */
    setActiveLine : function(lineId)
    {
        if (this._currentLineObj && this._currentLineObj.getId() == lineId)
        {
            return this._currentLineObj;
        }

        var lineObj = this.getLineObject(lineId);
        if (!lineObj)
        {
            return null;
        }
        lineObj.active();
        return this._currentLineObj;
    },

    /**
     * Détermine les interactions entre les modules/helpers
     *
     */
    setAllExternalParams : function()
    {
        for (var i = 0; i < this._tabLinesObj.length; i++)
        {
            this._tabLinesObj[i].setAllExternalParams();
        }
    },

    /**
     * Définir les commentaires du devis
     *
     * @param string comments Commentaires
     * @return bool
     */
    setComments : function(comments)
    {
        comments = (comments + '').trim();
        if (this._comments == comments || !this.hasRight('infoscpl', 'comments'))
        {
            return false;
        }
        this._comments = comments;
        this.display(2);
        return true;
    },

    /**
     * Définir si le devis contient des erreurs
     *
     * @param Array|false tabErrors   Liste des erreurs
     * @param int         mode        (0: set, 1: add, 2: del)
     */
    setErrors : function(tabErrors, mode)
    {
        var nb = (tabErrors ? tabErrors.length : 0);
        if (mode == 0)
        {
            this._tabErrors = [];
        }
        if (mode == 2)
        {
            var pos;
            for (var i = 0; i < nb; i++)
            {
                pos = array_search(tabErrors[i], this._tabErrors);
                if (pos !== false)
                {
                    this._tabErrors.splice(pos, 1);
                }
            }
        }
        else
        {
            for (var i = 0; i < nb; i++)
            {
                array_push_unique(this._tabErrors, tabErrors[i]);
            }
        }
    },

    /**
     * Définir si le devis est en FullService ou non
     *
     * @param bool isFullService
     * @return bool
     */
    setFullService : function(isFullService)
    {
        isFullService = (isFullService ? true : false);
        if (this._isFullService == isFullService)
        {
            return false;
        }
        this._isFullService = isFullService;

        this.setAllExternalParams();
        return true;
    },

    /**
     * Définir l'id du négociateur
     *
     * @param int id
     * @return bool
     */
    setNegotiatorId : function(id)
    {
        id = parseInt(id);
        if (isNaN(id) || this._negotiatorId == id)
        {
            return false;
        }
        this._negotiatorId = id;
        this.display(2);
        return true;
    },

    /**
     * Définir le devis imprimable ou non
     *
     * @param bool isPrintable
     * @return bool
     */
    setPrintable : function(isPrintable)
    {
        isPrintable = (isPrintable ? true : false);
        if (this._isPrintable == isPrintable)
        {
            return false;
        }
        this._isPrintable = isPrintable;
        this.display(2);
        return true;
    },

    /**
     * Définir les commentaires de suivi du devis
     *
     * @param string comments Commentaires de suivi
     * @return bool
     */
    setTracking : function(comments)
    {
        comments = (comments + '').trim();
        if (this._tracking == comments || !this.hasRight('infoscpl', 'tracking'))
        {
            return false;
        }
        this._tracking = comments;
        this.display(2);
        return true;
    },

    /**
     * Valider les modifications apportées au devis
     *
     * @param Array tabLines Id des lignes à valider (si vide alors toutes les lignes sont sélectionnées)
     * @return bool
     */
    validate : function(tabLines, subAction)
    {
        if (!this.isValidable())
        {
            return false;
        }

        var mySelf = this;

        // Instanciation de l'objet HTTPRequest pour la récupération des données de la ligne de devis
        if (!this._saveHttpObj)
        {
            this._saveHttpObj = new Location.HTTPRequest(this._tabCfgs.saveUrl);
        }

        // Récupération des données du devis
        var tabData = this.getData(tabLines, subAction);
        this._saveHttpObj.setVar('tabData', tabData);
        this._saveHttpObj.setVar('activeLineId', this._currentLineObj.getId());
        this._saveHttpObj.setVar('tabIndex', Location.tabBoxesManager.getTabBox('rentEstimateLineTabBox').getActiveTabIndex());

        var tabLinesAssociatedObj = tabData.tabLines.map(function(line) {
            return mySelf.getLineObject(line.id);
        });

        // Vérification des documents en cours de téléversement
        var docMsgSuffix = '';
        switch (subAction)
        {
            case 'accept':
                docMsgSuffix += 'Accept';
                break;
            case 'unaccept':
                docMsgSuffix += 'Unaccept';
                break;
            default:
                docMsgSuffix += 'Valid';
        }
        if (tabLines)
        {
            docMsgSuffix += 'Line';
        }
        if (Location.Rent_Helper_DocumentsView.checkPendingUploads('documentsHlp_', tabLinesAssociatedObj.map(function(lineObj) {
                return lineObj._documentsObj;
            }), function() {
                mySelf.validate(tabLines, subAction);
            }, null, {
                force: LOC_View.getTranslation('ignorePendingDocsAnd' + docMsgSuffix),
                wait:  LOC_View.getTranslation('waitPendingDocsAnd' + docMsgSuffix),
                back:  LOC_View.getTranslation('goBackToEstimate')
            }))
        {
            var entityType = (tabLines && tabLines.length == 1 ? 'estimateLine' : 'estimate');
            this._loadStatus = 'ok';
            return false;
        }

        this._checkErrors();
        if (this._tabErrors.length > 0)
        {
            this._displayValidateMsg('', []);
            return false;
        }

        this._loadStatus = 'loading';
        this.display(2);


        this._saveHttpObj.oncompletion = function()
        {
            var tabData = this.getJSONResponse();
            if (tabData)
            {
                var url = '';
                if (tabData['@state'] == 'ok')
                {
                    url = tabData['@url'];

                    // Mise à jour des données du devis
                    mySelf._editMode = 'update';
                    mySelf._loadInfos(tabData);

                    // Mise à jour des lignes
                    var nb = tabLinesAssociatedObj.length;
                    var lineObj, lineAction, lineId;
                    for (var i = 0; i < nb; i++)
                    {
                        lineObj = tabLinesAssociatedObj[i];
                        if (lineObj)
                        {
                            lineAction = tabData.tabLines[i]['@action'];
                            lineId     = tabData.tabLines[i].id;

                            if (lineAction == 'update' || lineAction == 'insert')
                            {
                                lineObj._editMode = 'update';
                                lineObj._loadInfos(tabData.tabLines[i]);
                            }
                            else if (lineAction == 'delete')
                            {
                                // Suppression de la ligne du devis
                                mySelf.removeLine(lineId, true);
                            }
                        }
                    }

                    // Mise à jour du client
                    var customerObj = mySelf.getCustomerObject();
                    customerObj.load(tabData.tabCustomer, {
                        disabledComponents: {
                            select: (mySelf.getEditMode() != 'create')
                        }
                    }, mySelf._tabRights.customer);

                    // Mise à jour du chantier
                    var siteObj = mySelf.getSiteObject();
                    siteObj.load(tabData.tabSite, {}, mySelf._tabRights.site);

                    mySelf.setAllExternalParams();

                    // Erreur
                    mySelf._tabWarnings = tabData['@warnings'];
                    mySelf._tabErrors = [];
                    mySelf._loadStatus = 'ok';
                }
                else
                {
                    // Mise à jour des lignes
                    var nb = tabLinesAssociatedObj.length;
                    var lineObj;
                    for (var i = 0; i < nb; i++)
                    {
                        lineObj = tabLinesAssociatedObj[i];
                        if (lineObj)
                        {
                            lineObj.setErrors(tabData.tabLines[i]['@errors'], 0);
                            lineObj._tabErrorsDetails = tabData.tabLines[i]['@details'];
                        }
                    }

                    // Récupération des erreurs
                    mySelf.setErrors(tabData['@errors'], 0);
                    mySelf._loadStatus = 'ok';
                }
                // Mise à jour des informations du devis
                mySelf._checkTransportEstimations(false);
                mySelf.display(1);
                mySelf._displayValidateMsg(url, tabLinesAssociatedObj);
            }
            else
            {
                // Erreur
                this.onerror();
            }
        }
        this._saveHttpObj.onerror = function()
        {
            // Erreur
            mySelf._loadStatus = 'error';
            mySelf.setErrors([-1], 0); // Erreur fatale
            mySelf._displayValidateMsg('', []);
            mySelf.display(0);
        }
        this._saveHttpObj.send(true, true);

        return true;
    },

    /**
     * Réactualiser le devis
     *
     * @return string
     */
    refresh : function()
    {
        var mySelf = this;

        // Instanciation de l'objet HTTPRequest pour la récupération de l'URl du devis
        if (!this._getUrlHttpObj)
        {
            this._getUrlHttpObj = new Location.HTTPRequest(this._tabCfgs['getUrlUrl']);
        }

        // Envoi des paramètres
        this._getUrlHttpObj.setVar('estimateId', this._id);
        this._getUrlHttpObj.setVar('estimateLineId', this._currentLineObj.getId());
        this._getUrlHttpObj.setVar('tabIndex', Location.tabBoxesManager.getTabBox('rentEstimateLineTabBox').getActiveTabIndex());


        this._getUrlHttpObj.oncompletion = function()
        {
            var tabData = this.getJSONResponse();
            if (tabData)
            {
                window.location.href = tabData['url'];
            }
            else
            {
                // Erreur
                this.onerror();
            }
        }
        this._getUrlHttpObj.onerror = function()
        {
            // Erreur
            mySelf._loadStatus = 'error';
            mySelf.setErrors([-1], 0); // Erreur fatale
            mySelf._displayValidateMsg('', '');
            mySelf.display();
        }
        this._getUrlHttpObj.send(true, true);

        return true;
    },

    _checkTransportEstimations : function(isLoadLines)
    {
        var nb = this._tabLinesObj.length;
        for (var i = 0; i < nb; i++)
        {
            var lineObj = this._tabLinesObj[i];
            if (lineObj.getLoadStatus() != '')
            {
                lineObj._transportObj.checkEstimation();
                lineObj.display();
            }
        }
        if (isLoadLines)
        {
            this.loadLines(this.getNotLoadedLinesList());
        }
    },

    /**
     * Affichage du message d'erreur ou de succès de l'enregistrement du devis
     *
     * @param string url
     * @param Array  tabLinesAssociatedObj
     */
    _displayValidateMsg : function(url, tabLinesAssociatedObj)
    {
        var nbErrors = this._tabErrors.length;
        var nbLines = tabLinesAssociatedObj.length;

        $ge('errorBlock').style.display = 'none';
        $ge('validBlock').style.display = 'none';
        if (nbErrors > 0)
        {
            // Masquage des erreurs
            var tabLiObj = $ge('errors').getElementsByTagName('li');
            var i, nb = tabLiObj.length;
            for (i = 0; i < nb; i++)
            {
                tabLiObj[i].style.display = 'none';
            }
            // Affichage des erreurs du devis
            for (i = 0; i < nbErrors; i++)
            {
                // Affichage du message correspondant à l'erreur
                var errorObj = $ge('error.' + this._tabErrors[i]);
                if (errorObj)
                {
                    errorObj.style.display = '';
                }
            }
            // Affichage des erreurs des lignes du devis
            for (i = 0; i < nbLines; i++)
            {
                var lineObj = tabLinesAssociatedObj[i];
                var nbLineErrors = lineObj._tabErrors.length;
                for (var k = 0; k < nbLineErrors; k++)
                {
                    // Affichage du message correspondant à l'erreur
                    var errorObj = $ge('error.' + lineObj._tabErrors[k]);
                    if (errorObj)
                    {
                        var aElt = window.document.createElement('A');
                        aElt.href = '#';
                        aElt.innerHTML = lineObj.getIndex();
                        aElt._lineObj = lineObj;

                        aElt.onclick = function()
                        {
                            $ge('errorBlock').style.display = 'none';
                            this._lineObj.active();
                            return false;
                        }
                        if (errorObj.style.display == 'none')
                        {
                            errorObj.lastChild.innerHTML = '';
                        }
                        else
                        {
                            errorObj.lastChild.appendChild(window.document.createTextNode(', '));
                        }
                        errorObj.lastChild.appendChild(aElt);
                        errorObj.style.display = '';
                    }
                }

                // Affichage des détails des erreurs ?
                var nbDetails = lineObj._tabErrorsDetails.length;
                for (j = 0; j < nbDetails; j++)
                {
                    var errorDetailsObj = $ge('error.' + lineObj._tabErrorsDetails[j].module + '.details');
                    if (errorDetailsObj)
                    {
                        errorDetailsObj.style.display = '';
                        if (lineObj._tabErrorsDetails[j].module == 'days')
                        {
                            var divEl = errorDetailsObj.querySelector('div');
                            if (divEl)
                            {
                                var tabInfos = lineObj._tabErrorsDetails[j].tabInfos;
                                var text = LOC_View.getTranslation('days.details');
                                text = text.replace(/<%type>/, tabInfos.type ? LOC_View.getTranslation('days.type_' + tabInfos.type) : '');
                                text = text.replace(/<%date>/, tabInfos.date ? LOC_View.getDateFormat(tabInfos.date) : LOC_View.getTranslation('days.noDate'));
                                text = text.replace(/<%appliedToContract>/, (tabInfos.appliedToContractId ?
                                                        LOC_View.getTranslation('days.appliedToContract').replace(/<%appliedToContractId>/, tabInfos.appliedToContractId) : ''));
                                divEl.innerHTML = text;
                            }
                        }
                    }
                }
            }

            $ge('errorBlock').style.display = 'block';
            $ge('errorBlockBtn').focus();
        }
        else
        {
            var aEl = $ge('validBlockBtn');
            aEl.style.visibility = 'visible';

            var successMsgEstimate     = document.querySelector('#validBlock #success > span.estimate');
            var successMsgEstimateLine = document.querySelector('#validBlock #success > span.estimateLine');

            Location.commonObj.removeEltClass(successMsgEstimate, 'active');
            Location.commonObj.removeEltClass(successMsgEstimateLine, 'active');

            if (url != '')
            {
                Location.commonObj.addEltClass(successMsgEstimate, 'active');
                aEl.onclick = function()
                {
                    this.style.visibility = 'hidden';
                    return true;
                }
                aEl.href = url;
            }
            else
            {
                Location.commonObj.addEltClass(successMsgEstimateLine, 'active');
                aEl.href = '#';
                aEl.onclick = function()
                {
                    $ge('validBlock').style.display = 'none';
                    return false;
                }
            }

            // Affichage des warnings - Devis
            var hasWarning = false;
            var nbWarnings = this._tabWarnings.length;
            var tabLiObj = $ge('warnings').getElementsByTagName('li');
            var i, nb;
            nb = tabLiObj.length;
            for (i = 0; i < nb; i++)
            {
                tabLiObj[i].style.display = 'none';
            }
            for (i = 0; i < nbWarnings; i++)
            {
                hasWarning = true;
                var errorObj = $ge('warning.' + this._tabWarnings[i].module + '.' + this._tabWarnings[i].code);
                if (errorObj)
                {
                    errorObj.style.display = '';
                }
            }

            var tabDocs = {};
            // Affichage des warnings - Ligne de devis
            for (i = 0; i < nbLines; i++)
            {
                var lineObj = tabLinesAssociatedObj[i];
                var nbWarnings = lineObj._tabWarnings.length;
                var j, k;
                var tabDocumentsActions = lineObj._tabDocumentsActions;
                for (j = 0; j < nbWarnings; j++)
                {
                    hasWarning = true;
                    if (!tabDocs[lineObj._tabWarnings[j].module])
                    {
                        tabDocs[lineObj._tabWarnings[j].module] = {};
                    }
                    if (lineObj._tabWarnings[j].module == 'documents')
                    {
                        for (var k = 0; k < lineObj._tabWarnings[j].indexes.length; k++)
                        {
                            var index = lineObj._tabWarnings[j].indexes[k];
                            var action = tabDocumentsActions[index];
                            if (action)
                            {
                                if (!tabDocs[lineObj._tabWarnings[j].module][action.type])
                                {
                                    tabDocs[lineObj._tabWarnings[j].module][action.type] = [];
                                }
                                switch (action.type)
                                {
                                    case 'add':
                                    case 'duplicate':
                                        tabDocs[lineObj._tabWarnings[j].module][action.type].push({
                                            lineIndex: i +1,
                                            title: action.data.title,
                                            fileName: action.data.fileName
                                        });
                                        break;
                                    case 'edit':
                                    case 'remove':
                                        var item = lineObj._documentsObj.list.getById(action.id);
                                        if (item)
                                        {
                                            tabDocs[lineObj._tabWarnings[j].module][action.type].push({
                                                lineIndex: i +1,
                                                title: item.title,
                                                fileName: item.fileName
                                            });
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }

            LOC_Common.removeEltClass('validBlock', 'warning');
            if (hasWarning)
            {
                for (var module in tabDocs)
                {
                    for (var code in tabDocs[module])
                    {
                        var tabContent = [];
                        var errorObj = $ge('warning.' + module + '.' + code);
                        if (errorObj)
                        {
                            errorObj.style.display = '';
                            var ulEl = errorObj.querySelector('div.details');
                            if (ulEl)
                            {
                                var tabLines = {};
                                tabDocs[module][code].map(function(doc) {
                                    if (!tabLines[doc.lineIndex])
                                    {
                                        tabLines[doc.lineIndex] = [];
                                    }
                                    tabLines[doc.lineIndex].push((doc.title != '' ? doc.title + ' (' + doc.fileName + ')' : doc.fileName));
                                });

                                for (var lineIndex in tabLines)
                                {
                                    tabContent.push(lineIndex + ' : ' + tabLines[lineIndex].join(', '));
                                }
                            }
                            ulEl.innerHTML = tabContent.join('<br />');
                        }
                    }
                }

                LOC_Common.addEltClass('validBlock', 'warning');
            }

            $ge('validBlock').style.display = 'block';
            $ge('validBlockBtn').focus();
        }
    },

    /**
     * Récupérer la cellule associée à un identifiant de ligne du devis et un nom de colonne
     *
     * @param Object rowEl   Instance de la ligne du tableau
     * @param string colName Nom de la colonne
     * @return HTMLTableCellElement
     */
    _getTabLinesCell : function(rowEl, colName)
    {
        // Optimisation
        if (this._tabColumnsIndex[colName])
        {
            return rowEl.cells[this._tabColumnsIndex[colName]];
        }

        var nbCells = rowEl.cells.length;
        for (var i = 0; i < nbCells; i++)
        {
            if (rowEl.cells[i].className.indexOf(colName) == 0)
            {
                this._tabColumnsIndex[colName] = i;
                return rowEl.cells[i];
            }
        }
        return false;
    },

    /**
     * Détecter les erreurs
     *
     * @return bool
     */
    _checkErrors : function()
    {
        var tabErrors = [];

        this.setErrors(tabErrors, 0);

        // Erreur sur le contact commande
        this._customerObj.checkOrderContactEmail();

        // Erreur sur le contact chantier
        this._siteObj.checkContactEmail();

        return (tabErrors.length == 0);
    },

    /**
     * Vérifier le droit à un champ
     *
     * @return bool
     */
    hasRight : function(partName, fieldName)
    {
        return (this._tabRights === null || (this._tabRights[partName] && this._tabRights[partName][fieldName]) ? true : false);
    },

    /**
     * Initialisation
     *
     */
    _init : function()
    {
        var mySelf = this;
        var el;
        var pfx = 'rentEstimate';

        // ************************************************************
        // Ajout des événements sur toutes les entrées de l'interface
        // ************************************************************

        // Client
        this._customerObj = new Location.Rent_Helper_Customer(this, this._tabCfgs);
        this._customerObj.onchange = function(tabChanges)
        {
            var nb = mySelf._tabLinesObj.length;
            if (mySelf.getEditMode() == 'create' && in_array('id', tabChanges))
            {
                for (var i = 0; i < nb; i++)
                {
                    // Mise à jour du nettoyage et de la participation au recyclage
                    mySelf._tabLinesObj[i].setCleaningTypeId((this.isCleaningInvoiced() ? 'std' : 0), true);
                }
            }
            mySelf.setAllExternalParams();
            mySelf.display();

            if (this.getId() && mySelf.getEditMode() == 'create')
            {
                for (var i = 0; i < nb; i++)
                {
                    // Modèle de machine pour récupérer les nouveaux tarifs de loc
                    mySelf._tabLinesObj[i].setModelId(mySelf._tabLinesObj[i].getModelId(), true);
                    if (this.isProformaRequired())
                    {
                        mySelf._tabLinesObj[i]._rentTariffObj.setAmountType('day');
                    }
                }
                mySelf._checkTransportEstimations(true);
            }
        }
        Location.Rent_Helper_CustomerView.initEvents('customerHlp_', this._customerObj);


        // Chantier
        this._siteObj = new Location.Rent_Helper_Site(this, this._tabCfgs);
        this._siteObj.onchange = function(tabChanges)
        {
            if (in_array('zone', tabChanges))
            {
                mySelf._checkTransportEstimations(true);
            }
            mySelf.setAllExternalParams();
            mySelf.display();
        }
        Location.Rent_Helper_SiteView.initEvents('rentSite', this._siteObj);


        // - Onglet activé
        Location.tabBoxesManager.getTabBox(pfx + 'LineTabBox').onaftertabchange = function(index)
        {
            if (mySelf._currentLineObj)
            {
                mySelf._currentLineObj.display();
            }
        }

        // - Modification du modèle de machine
        Location.searchBoxesManager.getSearchBox(pfx + 'LineModelId').onchange = function()
        {
            if (mySelf._currentLineObj)
            {
                mySelf._currentLineObj.setModelId(this.getValue());
            }
        }

        // - Modèle impératif ?
        $ge(pfx + 'LineIsImperativeModel').onchange = function()
        {
            if (mySelf._currentLineObj)
            {
                mySelf._currentLineObj.setImperativeModel(this.checked);
                mySelf.checked = mySelf._currentLineObj.isImperativeModel();
            }
        }
        $ge(pfx + 'LineIsImperativeModel').onclick = $ge(pfx + 'LineIsImperativeModel').onchange;

        // - Affectation des commandes sur les boutons de la barre de contrôles de la ligne de devis
        $ge(pfx + 'LineValidBtn').onclick = function()
        {
            if (mySelf._currentLineObj)
            {
                mySelf._currentLineObj.validate();
            }
            return false;
        }
        $ge(pfx + 'LineResetBtn').onclick = function()
        {
            if (mySelf._currentLineObj)
            {
                mySelf._currentLineObj.reset();
            }
            return false;
        }
        $ge(pfx + 'LineRemoveBtn').onclick = function()
        {
            if (mySelf._currentLineObj)
            {
                mySelf.removeLine(mySelf._currentLineObj.getId(), false);
            }
            return false;
        }
        $ge(pfx + 'LineDuplicateBtn').onclick = function()
        {
            if (mySelf._currentLineObj)
            {
                mySelf.duplicateLine(mySelf._currentLineObj.getId());
            }
            return false;
        }

        // Acceptation
        $ge(pfx + 'LineAccept').onclick = function()
        {
            if (mySelf._currentLineObj)
            {
                mySelf._currentLineObj.validate('accept');
            }
            return false;
        };
        $ge(pfx + 'LineCancelAccept').onclick = function()
        {
            if (mySelf._currentLineObj)
            {
                mySelf._currentLineObj.validate('unaccept');
            }
            return false;
        };

        // Bouton d'actualisation
        var refresh = function()
        {
            mySelf._loadStatus = 'loading';
            mySelf.display();

            mySelf.refresh();
        };

        obj = $ge('rentEstimateRefreshBtn');
        if (obj)
        {
            obj.onclick = refresh;
        }

        obj = $ge('rentEstimateErrorBlockRefreshBtn');
        if (obj)
        {
            obj.onclick = function()
            {
                $ge('errorBlock').style.display = 'none';
                refresh();
            }
        }

        // - Modification des commentaires du devis
        el = $ge(pfx + 'Comments');
        el.onchange = function()
        {
            mySelf.setComments(this.value);
            this.value = mySelf.getComments();
        }
        // Firefox
        el.oninput = function()
        {
            if (this.value.length > this.getAttribute('data-maxlength'))
            {
                alert(LOC_View.getTranslation('commentCapacityOver'));
                mySelf.setComments(this.value.substr(0, this.getAttribute('data-maxlength')));
                this.value = mySelf.getComments();
            }
            return false;
        }
        el.onpaste = function()
        {
            if (window.clipboardData)
            {
                var newText = this.value + window.clipboardData.getData("Text");
                if (newText.length > this.getAttribute('data-maxlength'))
                {
                    alert(LOC_View.getTranslation('commentCapacityOver'));
                    mySelf.setComments(newText.substr(0, this.getAttribute('data-maxlength')));
                    this.value = mySelf.getComments();
                    return false;
                }
            }
        }
        if (LOC_ClientInfo.isMSIE)
        {
            el.onkeydown = function()
            {
                if (!LOC_View.limitTextArea(event, this.getAttribute('data-maxlength')))
                {
                    alert(LOC_View.getTranslation('commentCapacityOver'));
                    return false;
                }
            }
        }

        // - Modification du suivi du devis
        el = $ge(pfx + 'Tracking');
        el.onchange = function()
        {
            mySelf.setTracking(this.value);
            this.value = mySelf.getTracking();
        }
        // Firefox
        el.oninput = function()
        {
            if (this.value.length > this.getAttribute('data-maxlength'))
            {
                alert(LOC_View.getTranslation('trackingCapacityOver'));
                mySelf.setTracking(this.value.substr(0, this.getAttribute('data-maxlength')));
                this.value = mySelf.getTracking();
            }
            return false;
        }
        el.onpaste = function()
        {
            if (window.clipboardData)
            {
                var newText = this.value + window.clipboardData.getData("Text");
                if (newText.length > this.getAttribute('data-maxlength'))
                {
                    alert(LOC_View.getTranslation('trackingCapacityOver'));
                    mySelf.setTracking(newText.substr(0, this.getAttribute('data-maxlength')));
                    this.value = mySelf.getTracking();
                    return false;
                }
            }
        }
        if (LOC_ClientInfo.isMSIE)
        {
            el.onkeydown = function()
            {
                if (!LOC_View.limitTextArea(event, this.getAttribute('data-maxlength')))
                {
                    alert(LOC_View.getTranslation('trackingCapacityOver'));
                    return false;
                }
            }
        }
        // - Modification du motif d'abandon de devis
        $ge(pfx + 'AbortReasonId').onchange = function()
        {
            mySelf.setAbortReasonId(this.value);
            this.value = mySelf.getAbortReasonId();
        }
        // - Modification des commentaire d'abandon du devis
        $ge(pfx + 'AbortComments').onchange = function()
        {
            mySelf.setAbortComments(this.value);
            this.value = mySelf.getAbortComments();
        }
        // - Devis FullService ?
        el = $ge(pfx + 'IsFullService');
        el.onchange = function()
        {
            mySelf.setFullService(this.checked);
            this.checked = mySelf.isFullService();
        }
        el.onclick = el.onchange;
        // - Négociateur
        Location.searchBoxesManager.getSearchBox(pfx + 'NegotiatorId').onchange = function()
        {
            mySelf.setNegotiatorId(this.getValue());
        }
        $ge(pfx + 'ClearNegotiatorBtn').onclick = function()
        {
            mySelf.setNegotiatorId(0);
        }

        // Client
        Location.rentCustomerObj = new Location.Rent_Helper_Customer(this, this._tabCfgs);

        // - Affectation des commandes sur les boutons de la barre de contrôles du devis
        $ge(pfx + 'AddLineBtn').onclick = function()
        {
            mySelf.addLine();
            return false;
        }
        $ge(pfx + 'ValidBtn').onclick = function()
        {
            mySelf.validate();
            return false;
        }
        $ge(pfx + 'ResetBtn').onclick = function()
        {
            mySelf.reset();
            return false;
        }

        // Acceptation de toutes les lignes
        $ge(pfx + 'AcceptAll').onclick = function()
        {
            mySelf.acceptAllLines();
            return false;
        };

        $ge(pfx + 'CancelAcceptAll').onclick = function()
        {
            mySelf.unacceptAllLines();
            return false;
        };

        // Initialisation du gestionnaire d'upload des documents
        Location.Rent_Helper_Documents_AddList.uploadManager.init(this._tabCfgs);

        // *******************************************
        // Chargement des données du devis
        // *******************************************
        this._loadInfos(this._tabCfgs.tabInfos);
        if (this.getEditMode() == 'update')
        {
            // Initialisation des lignes
            var tabLines = this._tabCfgs.tabInfos.tabLines;
            var tabLinesInfos = this._tabCfgs.tabInfos.tabLinesPreloadInfos;
            var i, nb = tabLines.length;
            for (i = 0; i < nb; i++)
            {
                this._tabLinesObj.push(new Location.RentEstimateLine(this, Number(tabLines[i]), tabLinesInfos[i], this._tabCfgs));
            }

            // Activation de la ligne courante pour une modification
            var activeLineId = this._tabCfgs.activeLineId;
            if (this._tabErroneousLines.length > 0)
            {
                array_push_unique(this._tabErroneousLines, activeLineId);
                this.loadLines(this._tabErroneousLines);
            }
            this.setActiveLine(activeLineId);
        }
        else
        {
            this._id = this._tempId;
            // Ajout d'une ligne vide dans le cas d'une création
            var lineObj = new Location.RentEstimateLine(this, null, {}, this._tabCfgs);
            this._tabLinesObj.push(lineObj);
            lineObj.active();
        }
        this.display(1);
    },

    _loadInfos : function (tabInfos)
    {
        this._id                = Number(tabInfos.id);
        this._code              = tabInfos.code;
        this._tracking          = (tabInfos.tracking || '');
        this._comments          = (tabInfos.comments || '');
        this._abortReasonId     = Number(tabInfos['abortReason.id']);
        this._abortComments     = (tabInfos.abortComments || '');
        this._isFullService     = (tabInfos.isFullService == 1);
        this._negotiatorId      = (tabInfos['negotiator.id'] || null);
        this._negotiatorFullName = (tabInfos['negotiator.fullName'] || '');
        this._modificationDate  = tabInfos.modificationDate;
        this._valueDate         = tabInfos.valueDate;
        this._stateId           = tabInfos['state.id'];
        this._isPrintable       = (tabInfos.isPrintable == 1);
        this._tabErroneousLines = (tabInfos.tabErroneousLines || []);
        this._tabRights         = tabInfos.tabRights;
        for (var part in this._tabRights)
        {
            var tabTmp = (this._tabRights[part] != '' ? this._tabRights[part].split('|') : []);
            this._tabRights[part] = {};
            var i, nb = tabTmp.length;
            for (var i = 0; i < nb; i++)
            {
                this._tabRights[part][tabTmp[i]] = true;
            }
        }

        // Client
        this._customerObj.load(tabInfos.tabCustomer, {
            disabledComponents: {
                select: (this.getEditMode() != 'create')
            }
        }, this._tabRights.customer);

        // Chantier
        this._siteObj.load(tabInfos.tabSite, {}, this._tabRights.site);
    },

    /**
     * Ajouter un bouton à la liste des boutons affichés d'une notification
     *
     * @param string notificationId Identifiant de la notification
     * @param string buttonId Identifiant du bouton
     */
    _showNotificationButton : function(notificationId, buttonId)
    {
        if (typeof this._tabNotifications[notificationId].buttons == 'undefined')
        {
            this._tabNotifications[notificationId].buttons = [];
        }
        if (!in_array(buttonId, this._tabNotifications[notificationId].buttons))
        {
            this._tabNotifications[notificationId].buttons.push(buttonId);
        }
    },

    /**
     * Ajouter un sous-message à la liste des sous-messages affichés d'une notification
     *
     * @param string notificationId Identifiant de la notification
     * @param string subMessageId Identifiant du sous-message
     */
    _showNotificationSubMessage : function(notificationId, subMessageId)
    {
        if (typeof this._tabNotifications[notificationId].messages == 'undefined')
        {
            this._tabNotifications[notificationId].messages = [];
        }
        if (!in_array(subMessageId, this._tabNotifications[notificationId].messages))
        {
            this._tabNotifications[notificationId].messages.push(subMessageId);
        }
    },

    /**
     * Supprimer un bouton de la liste des boutons affichés d'une notification
     *
     * @param string notificationId Identifiant de la notification
     * @param string buttonId Identifiant du bouton
     */
    _hideNotificationButton : function(notificationId, buttonId)
    {
        if (typeof this._tabNotifications[notificationId].buttons == 'object')
        {
            var index = this._tabNotifications[notificationId].buttons.indexOf(buttonId);
            if (index != -1)
            {
                this._tabNotifications[notificationId].buttons.splice(index, 1);
            }
        }
    },

    /**
     * Supprimer un sous-message de la liste des sous-messages affichés d'une notification
     *
     * @param string notificationId Identifiant de la notification
     * @param string subMessageId Identifiant du sous-message
     */
    _hideNotificationSubMessage : function(notificationId, subMessageId)
    {
        if (typeof this._tabNotifications[notificationId].messages == 'object')
        {
            var index = this._tabNotifications[notificationId].messages.indexOf(subMessageId);
            if (index != -1)
            {
                this._tabNotifications[notificationId].messages.splice(index, 1);
            }
            if (this._tabNotifications[notificationId].messages == [])
            {
                delete this._tabNotifications[notificationId].messages;
            }
        }
        if (typeof this._tabNotifications[notificationId].messages == 'undefined')
        {
            this._tabNotifications[notificationId].displayType = 'hide';
        }
    },

    /**
     * Affichage des notifications
     */
    _displayNotifications : function()
    {
        this._initNotifications();

        // Objets
        var documentsObj = this._documentsObj;

        // Notification d'envoi de la demande de déblocage client
        var custUnlockRequestStatus = this.getCustomerObject().getUnlockRequestStatus();
        if (custUnlockRequestStatus == 'sended')
        {
            this._tabNotifications['unlockCustomerRequest-send'].displayType = 'show';
        }
        else if (custUnlockRequestStatus == 'error')
        {
            this._tabNotifications['unlockCustomerRequest-error'].displayType = 'show';
        }

        // Affichage ou masquage des notifications
        for (var id in this._tabNotifications)
        {
            var displayType = 'hide';
            var tabOptions = {};

            // Type d'affichage
            if (this._tabNotifications[id].displayType)
            {
                displayType = this._tabNotifications[id].displayType;
            }

            // Options d'affichage
            if (typeof this._tabNotifications[id].options == 'object')
            {
                var nbOptions = this._tabNotifications[id].options.length;
                for (var i = 0; i < nbOptions; i++)
                {
                    tabOptions[this._tabNotifications[id].options[i]] = true;
                }
            }

            // Variables de message
            if (typeof this._tabNotifications[id].vars == 'object')
            {
                tabOptions['messageVars'] = this._tabNotifications[id].vars;
            }

            // Liste des messages à afficher
            if (typeof this._tabNotifications[id].messages == 'object')
            {
                tabOptions['messagesId'] = this._tabNotifications[id].messages;
            }

            // Liste des boutons à afficher
            if (typeof this._tabNotifications[id].buttons == 'object')
            {
                tabOptions['buttonsId'] = this._tabNotifications[id].buttons;
            }

            Location.viewObj.displayNotification(id, displayType, tabOptions);
        }
    },

    _initNotifications : function()
    {
        this._tabNotifications = {
            'unlockCustomerRequest-send': {'displayType' : 'hide'},
            'unlockCustomerRequest-error': {'displayType': 'hide'}
        };
    },
}

