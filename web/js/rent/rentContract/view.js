if (!window.Location)
{
    var Location = new Object();
}


var wishedMachineTimer = null;
/**
 * Afficher l'infobulle de la machine souhaitée
 *
 * @param elt
 * @param time
 */
function displayWishedMachineTooltip(elt, time)
{
    elt.className = 'wished on';
    if (wishedMachineTimer)
    {
        window.clearTimeout(wishedMachineTimer);
    }
    wishedMachineTimer = window.setTimeout(function(){ elt.className = 'wished'; }, time);
}

/**
 * Afficher l'état d'un onglet
 *
 * @param string state Etat
 */
Location.TabBoxTab.prototype.setRentContractState = function(state)
{
    this.getTitleElement().className = state;
}

/**
 * Classe de gestion du contrat
 *
 * @param string countryId Pays
 * @param int    id        Identifiant du contrat
 * @param Object tabCfgs   Configurations
 */
Location.RentContract = function(countryId, id, tabCfgs)
{
    var mySelf = this;
    this._tabErrors = [];
    this._tabWarnings = [];
    this._tabErrorsDetails = [];
    this._tabCfgs = tabCfgs;
    this._tempId = (tabCfgs['tempId'] || 'TMP' + new Date().getTime());
    this._saveHttpObj = null;
    this._getUrlHttpObj = null;
    this._loadStatus = 'ok';

    // Paramétrage de l'affichage des notifications
    this._tabNotifications = {};

    // Paramètrage de l'affichage des totaux
    this._tabTotals = {
        'rentTotal'               : function(){ return mySelf.getRentTotal(); },
        'insuranceAmount'         : function(){ return mySelf._servicesObj.getInsuranceAmount(); },
        'appealAmount'            : function(){ return mySelf._servicesObj.getAppealAmount(); },
        'transportDeliveryAmount' : function(){ return mySelf._transportObj.getDeliveryAmount(); },
        'transportRecoveryAmount' : function(){ return mySelf._transportObj.getRecoveryAmount(); },
        'fuelAmount'              : function(){ return mySelf._servicesObj.getFuelAmount(); },
        'cleaningAmount'          : function(){ return mySelf._servicesObj.getCleaningAmount(); },
        'residuesAmount'          : function(){ return mySelf._servicesObj.getResiduesAmount(); },
        'repairAmount'            : function(){ return mySelf.getRepairAmount(); },
        'otherServices'           : function(){ return mySelf._servicesObj.getRentServicesAmount(); },
    };
    this._tabInitTotals = {};

    // Instance de la gestion du client
    this._customerObj = null
    // Instance de la gestion du chantier
    this._siteObj = null
    // Instance de la gestion de la tarification location
    this._rentTariffObj = null;
    // Instance de la gestion de la tarification transport
    this._transportObj = null;
    // Instance de la gestion de la durée
    this._durationObj = null;
    // Instance de la gestion de la remise en état
    this._repairObj = null;
    // Instance de la gestion des autres services
    this._servicesObj = null;
    // Instance de la gestion des pro formas
    this._proformaObj = null;
    // Instance de la gestion des documents
    this._documentsObj = null;
    // Instance de gestion des historiques
    this._historyObj = null;

    // Données du contrat
    this._countryId   = countryId;
    this._agencyId    = tabCfgs['tabInfos']['agency.id'];

    this._linkedEstimateLineId = 0;

    this._id                = 0;
    this._index             = 0;
    this._code              = '';
    this._tracking          = '';
    this._comments          = '';
    this._isFullService     = false;
    this._negotiatorId      = null;
    this._negotiatorFullName = '';
    this._stateId           = '';
    this._isPrintable       = false;
    this._possibilities     = null;
    this._tabRights         = null;

    this._tabRepairSheetsToReopen = [];

    this._model = {
        id : 0,
        label : '',
        isFuelNeeded : false,
        tariffFamilyLabel : '',
        tabModelUseRates : [[[0, 0], [0, 0], [0, 0]], [[0, 0], [0, 0], [0, 0]]],
        load : '',
        isCleaningPossible : false,
        machinesFamilyLabel : '',
        tabWishedMachines : [],
        fuelQtyEstimByDay : 0,
        fuelQtyMax : 0,
        tabTypes : [],
        isRentable : null,
        isAccessory: false
    };
    this._modelLoadStatus = '';

    this._wishedMachineId = null;
    this._isWishedMachineUpdDsp = true;

    this._machine = {
        id : null,
        stateId : null,
        modelId: null,
        modelLabel: null,
        parkNumber : '',
        agencyId : null,
        visibleOnAgencyId : null,
        visibleOnAgencyLabel : null,
        visibilityMode : 0,
        isRentableModel : null,
        contractId : null
    };
    this._isFinalAllocation = 0;

    this._isImperativeModel = false;
    this._isJoinDoc = false;

    this._proformaFlags = 0;

    this._modificationDate = '';

    this._lastInvoiceDate = '';

    this._valueDate = '';

    this._actionsDates = {};

    if (id)
    {
        this._id = Number(id);
        this._editMode = 'update';
    }
    else
    {
        this._editMode = 'create';
    }

    this.onload        = null; // function(id)
    this.onchangemodel = null; // function(id, oldId)

    this._hasChangedComponents = {};

    this._init();
}

Location.RentContract.prototype = {

    /**
     * Mettre à jour l'affichage du contrat
     *
     * @param Object tabOptions
     */
    display : function(tabOptions)
    {
        if (!tabOptions)
        {
            tabOptions = {};
        }
        var editMode = this.getEditMode();


        // Traitement des erreurs
        var customerSearchBoxObj = Location.searchBoxesManager.getSearchBox('customer.id');
        customerSearchBoxObj.setRequired(false);
        var siteSearchBoxObj = Location.searchBoxesManager.getSearchBox('rentSiteLocalityId');
        siteSearchBoxObj.setRequired(false);
        var negotiatorSearchBoxObj = Location.searchBoxesManager.getSearchBox('rentContractNegotiatorId');
        negotiatorSearchBoxObj.setRequired(false);
        $ge('rentSiteLabel').className = '';
        $ge('rentContractAbortComments').className = '';
        $ge('rentSiteDeliveryHour.masked').className = '';
        $ge('rentSiteDeliveryTimeSlotId').className = '';

        // Tableau contenant les erreurs sur les onglets du site
        var tabSiteTabBoxError = [[[''], ['']], {}];
        var tabCustomerErrors = {};

        var nbErrors = this._tabErrors.length;
        for (var i = 0; i < nbErrors; i++)
        {
            var error = this._tabErrors[i];

            // Traitement de chaque erreur
            // Client non renseigné ou bloqué au devis
            if (error == 101 || error == 20)
            {
                customerSearchBoxObj.setRequired(true);
            }
            // Format mail contact commande
            else if (error == 31)
            {
                tabCustomerErrors.invalidOrderContactEmail = true;
            }
            // Libellé chantier non renseigné
            else if (error == 102)
            {
                tabSiteTabBoxError[0][SITETABINDEX_GENERAL]  = 'error';
                tabSiteTabBoxError[1].noLabel = true;
            }
            // Ville chantier non renseigné
            else if (error == 103)
            {
                tabSiteTabBoxError[0][SITETABINDEX_GENERAL]  = 'error';
                siteSearchBoxObj.setRequired(true);
            }
            // Format mail contact chantier
            else if (error == 121)
            {
                tabSiteTabBoxError[0][SITETABINDEX_GENERAL]  = 'error';
                tabSiteTabBoxError[1].invalidContactEmail = true;
            }
            // Commentaire d'abandon vide
            else if (error == 109)
            {
                $ge('rentContractAbortComments').className = 'error';
            }
            // Négociateur non renseigné
            else if (error == 15)
            {
                negotiatorSearchBoxObj.setRequired(true);
            }
            else if (error == 24)
            {
                tabSiteTabBoxError[0][SITETABINDEX_INFOS]  = 'error';
                tabSiteTabBoxError[1].noDeliveryHour = true;
            }
        }

        // Affichage des commentaires
        $ge('rentContractStatus').className = 'status ' + this.getStateId();
        $ge('rentContractModificationDate').innerHTML = LOC_View.getDateFormat(this.getModificationDate(), true);

        // Informations d'entête
        $ge('rentContractIndex').innerHTML = this._index;
        $ge('rentContractCode').value = this.getCode();

        var fullServiceCbElt = $ge('rentContractIsFullService');
        fullServiceCbElt.checked = this.isFullService();
        fullServiceCbElt.disabled = (editMode == 'update' || !this.hasRight('infos', 'isFullService'));

        // Négociateur
        obj = Location.searchBoxesManager.getSearchBox('rentContractNegotiatorId');
        if (this._negotiatorId != obj.getValue())
        {
            obj.clear();
            if (this._negotiatorId)
            {
                obj.setElements([[this._negotiatorId, this._negotiatorFullName]], 1);
                obj.setValue(this._negotiatorId);
            }
        }
        obj.setDisabled(!this.hasRight('infos', 'negotiator'));

        // Mise à jour des onglets du site
        var siteTabBoxObj = Location.tabBoxesManager.getTabBox('rentSiteTabBox');
        for (var i = 0; i < tabSiteTabBoxError[0].length; i++)
        {
            var tabObj = siteTabBoxObj.getTab(i);
            if (tabObj)
            {
                tabObj.setRentContractState(tabSiteTabBoxError[0][i]);
            }
        }

        // Mise à jour de l'affichage du client
        Location.Rent_Helper_CustomerView.display('customerHlp_', this._customerObj, tabCustomerErrors);

        // Mise à jour de l'affichage du site
        Location.Rent_Helper_SiteView.display('rentSite', this._siteObj, tabSiteTabBoxError[1]);

        this._displayControls();
        this._displayTabBoxes();

        // Informations complémentaires
        obj = $ge('rentContractComments');
        obj.value = this.getComments();
        obj.disabled = !this.hasRight('infoscpl', 'comments');

        obj = $ge('rentContractTracking');
        obj.value = this.getTracking();
        obj.disabled = !this.hasRight('infoscpl', 'tracking');

        // Affichage des totaux
        this._displayTotals();

        // Affichage de l'état du contrat
        $ge('rentContractBlock').className = 'loadstatus_' + this._loadStatus;


        // Affichage des notifications
        this._displayNotifications();

        return true;
    },

    /**
     * Récupérer l'agence du devis
     *
     * @return string
     */
    getAgencyId : function()
    {
        return this._agencyId;
    },

    /**
     * Récupérer le code du devis
     *
     * @return string
     */
    getCode : function()
    {
        // Le code ne peut changer dynamiquement qu'en création de contrat
        if (this.getEditMode() != 'create')
        {
            return this._code;
        }

        // Suppression de tous les suffixes du code existant
        var code = this._code.replace(/[^0-9*]+$/, '');

        // Détermination des suffixes du code en fonction du typage du modèle choisi
        // et de l'option "Full service" du contrat
        var tabSuffixes = [];
        if (this._model.tabTypes && in_array(TYPE_SUBRENT, this._model.tabTypes))
        {
            tabSuffixes.push(SUFFIX_SUBRENT);
        }
        if (this._model.tabTypes && in_array(TYPE_CATEGORYB, this._model.tabTypes))
        {
            tabSuffixes.push(SUFFIX_CATEGORYB);
        }
        if (this.isFullService())
        {
            tabSuffixes.push(SUFFIX_FULLSERVICE);
        }

        // Construction du nouveau code
        this._code = code + tabSuffixes.join('');

        return this._code;
    },

    /**
     * Récupérer le commentaire du devis
     *
     * @return string
     */
    getComments : function()
    {
        return this._comments;
    },

    /**
     * Récupérer le pays du devis
     *
     * @return string
     */
    getCountryId : function()
    {
        return this._countryId;
    },

    /**
     * Récupérer l'identifiant du client du devis
     *
     * @return int
     */
    getCustomerId : function()
    {
        return this._customerObj.getId();
    },

    /**
     * Récupérer l'identifiant du client du devis
     *
     * @return int
     */
    getCustomerObject : function()
    {
        return this._customerObj;
    },

    /**
     * Récupérer les données du contrat
     *
     * @param string subAction
     * @return Object
     */
    getData : function(subAction)
    {
        return {
            'id'            : this.getId(),
            '@action'       : (this.getEditMode() == 'create' ? 'insert' : 'update'),
            'subAction'     : subAction,

            'linkedEstimateLine.id' : this._linkedEstimateLineId,

            'tabCustomer'   : this._customerObj.getData(),
            'country.id'    : this.getCountryId(),
            'agency.id'     : this.getAgencyId(),

            'negotiator.id' : this.getNegotiatorId(),

            'tabSite'       : this._siteObj.getData(),

            'comments'      : this.getComments(),
            'tracking'      : this.getTracking(),

            'isFullService' : (this.isFullService() ? 1 : 0),

            // Matériel
            'model.id' : this.getModelId(),

            'isImperativeModel' : this.isImperativeModel(),
            'isJoinDoc'         : this.isJoinDoc(),

            'wishedMachine.id' : this.getWishedMachineId(),

            // Durée
            'beginDate'             : this._durationObj.getBeginDate(),
            'endDate'               : this._durationObj.getEndDate(),
            'duration'              : this._durationObj.getDuration(),
            'calendarDuration'      : this._durationObj.getCalendarDuration(),

            'tabDaysActions' : this._durationObj.getDaysActions(),

            // Location
            'tabRentTariff' : this._rentTariffObj.getData(),

            // Transport
            'tabTransport' : this._transportObj.getData(),

            // Assurance
            'tabInsurance' : {
                'typeId' : this._servicesObj.getInsuranceTypeId(),
                'rate' : this._servicesObj.getInsuranceRate()
            },

            // Gestion de recours
            'tabAppeal' : {
                'typeId' : this._servicesObj.getAppealTypeId(),
                'value'  : this._servicesObj.getAppealValue()
            },

            // Carburant
            'fuelQuantity' : this._servicesObj.getFuelQuantity(),

            // Nettoyage
            'tabCleaning' : {
                'typeId' : this._servicesObj.getCleaningTypeId(),
                'amount' : this._servicesObj.getCleaningAmount()
            },

            // Participation au recyclage
            'residues' : {
                'mode'  : this._servicesObj.getResiduesMode(),
                'value' : this._servicesObj.getResiduesValue()
            },

            // Pro formas
            'tabProformaAction' : this._proformaObj.getAction(),

            // Documents
            'tabDocumentsActions' : this._documentsObj.getActions(),

            'modificationDate' : this.getModificationDate(),

            // Jours de remise en état
            'tabRepairDays' : this._repairObj.getRepairDays(),

            // Services de remise en état
            'tabRepairServices' : this._repairObj.getRepairServices(),

            // Fiches de remise en état à rouvrir
            'tabRepairSheetsToReopen' : (subAction.match(/^reopenRepairSheets/) ? this._tabRepairSheetsToReopen : []),

            // Services de location et transport
            'tabRentServices' : this._servicesObj.getRentServices()
        };
    },

    /**
     * Récupérer le mode d'édition (Création ou modification)
     *
     * @return string ('create', 'update')
     */
    getEditMode : function()
    {
        return this._editMode;
    },

    /**
     * Récupérer l'identifiant du devis
     *
     * @return int
     */
    getId : function()
    {
        return this._id;
    },

    /**
     * Récupérer l'id du négociateur
     *
     * @return int
     */
    getNegotiatorId : function()
    {
        return this._negotiatorId;
    },

    /**
     * Récupérer l'état de facturation courant
     *
     * @return int
     */
    getInvoiceState : function()
    {
        return this._invoiceState;
    },

    /**
     * Récupérer l'identifiant de la machine
     *
     * @return int
     */
    getMachineId : function()
    {
        return this._machine.id;
    },


    /**
     * Récupérer le libellé de la famille de machines
     *
     * @return string
     */
    getMachinesFamilyLabel : function()
    {
        return this._model.machinesFamilyLabel;
    },

    /**
     * Récupérer l'id du modèle de machine
     *
     * @return int
     */
    getModelId : function()
    {
        return this._model.id;
    },

    /**
     * Récupérer l'id du modèle de la machine attribuée
     *
     * @return int
     */
    getMachineModelId : function()
    {
        return this._machine.modelId;
    },

    /**
     *  Récupérer l'id de la machine souhaitée
     *
     *  @return int
     */
    getWishedMachineId : function()
    {
        return this._wishedMachineId;
    },

    /**
     * Récupérer l'encombrement du modèle de machine
     *
     * @return int
     */
    getModelLoad : function()
    {
        return this._model.load;
    },

    /**
     * Récupérer la date de modification du devis
     *
     * @return string
     */
    getModificationDate : function()
    {
        return this._modificationDate;
    },


    /**
     * Récupérer la date de dernière facturation du contrat
     *
     * @return string
     */
    getLastInvoiceDate : function()
    {
        return this._lastInvoiceDate;
    },


    /**
     * Récupérer la date de valeur du contrat
     *
     * @return string
     */
    getValueDate : function()
    {
        return this._valueDate;
    },

    /**
     * Récupérer le type de l'objet courant
     *
     * @return string ('contract')
     */
    getObjectType : function()
    {
        return 'contract';
    },

    /**
     * Récupérer le numéro de parc de la machine
     *
     * @return string
     */
    getParkNumber : function()
    {
        return this._machine.parkNumber;
    },

    /**
     * Calcule le montant total de location.
     *
     * @return float
     */
    _calculateRentTotal : function()
    {
        var amountType = this._rentTariffObj.getAmountType();
        var amount = this._rentTariffObj.getAmount();
        if (amountType == 'day')
        {
            amount = amount * this._durationObj.getDuration();
        }
        else if (amountType == 'month')
        {
            amount = amount * (this._durationObj.getDuration() / 21);
        }
        return amount;
    },

    /**
     * Récupérer le montant total de location
     *
     * @return float
     */
    getRentTotal : function()
    {
        return LOC_Common.round(this._calculateRentTotal(), 2);
    },

    /**
     * Récupérer le montant de remise en état
     *
     * @return float
     */
    getRepairAmount : function()
    {
        return this._repairObj.getRepairDaysAmount() + this._repairObj.getRepairServicesAmount();
    },

    /**
     * Récupérer le montant de base pour le calcul de la participation au recyclage
     *
     * @return float
     */
    getResiduesBaseAmount : function()
    {
        return this._calculateRentTotal() + this.getRepairAmount();
    },

    /**
     * Récupérer l'état du devis
     *
     * @return string
     */
    getStateId : function()
    {
        return this._stateId;
    },

    /**
     * Récupérer le libellé de la famille tarifaire
     *
     * @return string
     */
    getTariffFamilyLabel : function()
    {
        return this._model.tariffFamilyLabel;
    },

    /**
     * Récupérer le montant total du contrat
     *
     * @return float
     */
    getTotal : function()
    {
        return this.getRentTotal() +
               this._servicesObj.getInsuranceAmount() +
               this._servicesObj.getAppealAmount() +
               this._transportObj.getDeliveryAmount() +
               this._transportObj.getRecoveryAmount() +
               this._servicesObj.getFuelAmount() +
               this._servicesObj.getCleaningAmount() +
               this._servicesObj.getResiduesAmount() +
               this._repairObj.getRepairDaysAmount() + this._repairObj.getRepairServicesAmount() +
               this._servicesObj.getRentServicesAmount();
    },

    /**
     * Récupérer les commentaires de suivi du devis
     *
     * @return string
     */
    getTracking : function()
    {
        return this._tracking;
    },

    /**
     * Récupérer l'objet estimation du transport
     *
     * @return Location.Rent_Helper_Transport
     */
    getTransportObject : function()
    {
        return this._transportObj;
    },

    /**
     * Récupérer l'instance du chantier
     *
     * @return Location.Rent_Helper_Site
     */
    getSiteObject : function()
    {
        return this._siteObj;
    },

    /**
     * Récupérer l'instance de l'onglet Documents
     *
     * @return Location.Rent_Helper_Documents
     */
    getDocumentsObject : function()
    {
        return this._documentsObj;
    },


    /**
     * Récupérer la zone du contrat
     *
     * @return int
     */
    getZone : function()
    {
        return this._siteObj.getZone();
    },

    /**
     * Indique si le devis contient des erreurs ou non
     *
     * @return bool
     */
    hasErrors : function()
    {
        return (this._tabErrors.length > 0);
    },

    /**
     * Retourne la date de clôture.
     *
     * @return bool
     */
    getClosureDate : function()
    {
        return this._closureDate;
    },


    /**
     * Indique si l'attribution de la machine sur le contrat est définitive
     *
     * return bool
     */
    isFinalAllocation : function()
    {
        return this._isFinalAllocation;
    },

    /**
     * Indique si le contrat peut être remise en cours ou non
     *
     * return bool
     */
    isReactivable : function()
    {
        // - droits et possibilités
        // - pas d'action de réalisation/annulation de transport en cours
        // - coche "transférée à" sur la récup non cochée
        return (this._loadStatus != 'loading' &&
                this._possibilities.actions.reactivate &&
                this.hasRight('actions', 'reactivate') &&
                !(this._transportObj.getSideAction('d') && this._transportObj.getSideAction('d').type) &&
                !(this._transportObj.getSideAction('r') && this._transportObj.getSideAction('r').type) &&
                !(this._transportObj.getSideInfos('r').transferToAgency) ? true : false);
    },

    /**
     * Indique si le contrat peut être annuler ou non
     *
     * return bool
     */
    isCancelable : function()
    {
        return (this._loadStatus != 'loading' &&
                this._possibilities.actions.cancel &&
                this.hasRight('actions', 'cancel') &&
                !(this._transportObj.getSideAction('d') && this._transportObj.getSideAction('d').type) &&
                !(this._transportObj.getSideAction('r') && this._transportObj.getSideAction('r').type) ? true : false);
    },

    /**
     * Indique si le devis est en FullService ou non
     *
     * @return bool
     */
    isFullService : function()
    {
        return this._isFullService;
    },

    /**
     * Indique si le modèle est impératif
     *
     * @return bool
     */
    isImperativeModel : function()
    {
        return this._isImperativeModel;
    },

    /**
     * Indique si on founir les documents de la machine au client
     *
     * @return bool
     */
    isJoinDoc : function()
    {
        return this._isJoinDoc;
    },

    /**
     * Indique si le contrat peut être mise en facturation ou non
     *
     * return bool
     */
    isInvoiceable : function()
    {
        return (this._loadStatus != 'loading' &&
                this._possibilities.actions.invoice &&
                this.hasRight('actions', 'invoice') &&
                !(this._transportObj.getSideAction('d') && this._transportObj.getSideAction('d').type) &&
                !(this._transportObj.getSideAction('r') && this._transportObj.getSideAction('r').type) ? true : false);
    },

    /**
     * Indique si le devis est imprimable ou non
     *
     * @return bool
     */
    isPrintable : function()
    {
        return (this._isPrintable && !this.hasErrors() && this.hasRight('actions', 'print') ? true : false);
    },

    /**
     * Est ce que le momdèle de machine est changeable ?
     *
     * @return bool
     */
    isModelChangeable : function()
    {
        return (this.getCustomerId() != 0 &&
                this._possibilities.equipment.model &&
                this.hasRight('machine', 'model') &&
                !this._transportObj.isTransferExists() &&
                !(this._transportObj.getSideAction('d') && this._transportObj.getSideAction('d').type == 'do') &&
                !(this._transportObj.getSideAction('r') && this._transportObj.getSideAction('r').type == 'do') ? true : false);
    },

    /**
     * Indique si le contrat peut être rétabli ou non
     *
     * @return bool
     */
    isResetable : function()
    {
        return this.isValidable();
    },

    /**
     * Indique si le contrat peut être remis en arrêt
     *
     * @return bool
     */
    isRestoppable : function()
    {
        return (this._loadStatus != 'loading' && this._possibilities.actions.restop && this.hasRight('actions', 'restop') ? true : false);
    },

    /**
     * Indique si le contrat peut être arrêté ou non
     *
     * @return bool
     */
    isStoppable : function()
    {
        return (this._loadStatus != 'loading' &&
                this._possibilities.actions.stop &&
                this.hasRight('actions', 'stop') &&
                !(this._transportObj.getSideAction('d') && this._transportObj.getSideAction('d').type) &&
                !(this._transportObj.getSideAction('r') && this._transportObj.getSideAction('r').type) ? true : false);
    },

    /**
     * Indique si le contrat peut être validé ou non
     *
     * @return bool
     */
    isValidable : function()
    {
        return (this._loadStatus != 'loading' &&
                this.hasRight('actions', 'valid') ? true : false);
    },

    /**
     * Est-il possible d'imprimer le bon de valorisation ?
     *
     * @return bool
     */
    isValorizationPrintable : function()
    {
        return (this.hasRight('actions', 'valorizationPrint') &&
                this._proformaObj.getActiveCount() == 0 && !this._customerObj.isProformaRequired() &&
                this.getStateId() != RENTCONTRACT_STATE_CANCELED ? true : false);
    },

    /**
     * Définir le montant du nettoyage
     *
     * @param float amount Montant
     * @return bool
     */
    reopenRepairSheet : function(sheetId)
    {
        this._tabRepairSheetsToReopen.push(sheetId);
        this.display();
        return true;
    },

    /**
     * Annuler le contrat
     *
     * @return bool
     */
    reset : function()
    {
        if (!this.isResetable())
        {
            return false;
        }
        if (confirm(LOC_View.getTranslation('answerAreYouSureReset')))
        {
            // Annulation des téléversements en cours
            this._documentsObj.addList.cancelPendings();

            this._loadStatus = 'loading';
            this.display();

            this.refresh();
        }
        return true;
    },

    /**
     * Détermine les interactions entre les modules/helpers
     *
     */
    setAllExternalParams : function()
    {
        // calcul de paramètres
        var d = this._durationObj.getDuration();
        var isProformaActived = (this._proformaObj.getActiveCount() > 0);
        var isProformaRequired = (this.getEditMode() == 'create' || this._proformaFlags & PFMDOCFLAG_CT_ISCUSTOMERPFREQONCREATION) &&
                                  this._customerObj.isProformaRequired();

        var beginDateObj     = new Date(this._durationObj.getBeginDate());
        var endDateObj       = new Date(this._durationObj.getEndDate());
        var tabDeliveryInfos = this._transportObj.getSideInfos('d');
        var tabRecoveryInfos = this._transportObj.getSideInfos('r');
        var deliveryAction   = this._transportObj.getSideAction('d');
        var recoveryAction   = this._transportObj.getSideAction('r');

        var deliveryLimitHour = (tabDeliveryInfos.contract && tabDeliveryInfos.contract.limitDate == this._durationObj.getBeginDate() ?
                                    tabDeliveryInfos.contract.limitHour : null);
        var recoveryLimitHour = (tabRecoveryInfos.contract && tabRecoveryInfos.contract.limitDate == this._durationObj.getEndDate() ?
                                    tabRecoveryInfos.contract.limitHour : null);

        var siteDisabledComponents       = {};
        var proformaDisabledComponents   = {};
        var transportDisabledComponents  = {d: {}, r: {}};
        var rentTariffDisabledComponents = {};
        var durationDisabledComponents   = {};
        var servicesDisabledComponents   = {};

        // Désactivation d'action sur les jours
        if (this._possibilities.duration)
        {
            if (!this._possibilities.duration.setInvoiceableDay)
            {
                durationDisabledComponents.setInvoiceable = true;
            }
            if (!this._possibilities.duration.setNonInvoiceableDay)
            {
                durationDisabledComponents.setNonInvoiceable = true;
            }
        }

        // Fonction retournant les types de proforma non autorisés de l'action concernée
        var disabledProformaComponents = function (action, tabTypes)
        {
            if (!proformaDisabledComponents[action])
            {
                proformaDisabledComponents[action] = tabTypes;
            }
            else
            {
                var nb = tabTypes.length;
                for (var i = 0; i < nb; i++)
                {
                    if (!in_array(tabTypes[i], proformaDisabledComponents[action]))
                    {
                        proformaDisabledComponents[action].push(tabTypes[i]);
                    }
                }
            }
            return proformaDisabledComponents[action];
        }

        // - blocage des transferts/reprises sur chantiers
        if (this._proformaObj.getActiveCount({type: PFMTYPE_ID_EXTENSION, isFinalized: 0}) > 0 ||
            !this.isFinalAllocation() || this._customerObj.isNewAccount())
        {
            transportDisabledComponents.r.sitesTransfer = true;
        }

        // - blocage des pro forma de prolongation si contrat en PF
        if (this.getInvoiceState() == RENTCONTRACT_INVOICESTATE_FINALPARTIAL)
        {
            proformaDisabledComponents.add        = disabledProformaComponents('add', [PFMTYPE_ID_EXTENSION]);
            proformaDisabledComponents.reactivate = disabledProformaComponents('reactivate', [PFMTYPE_ID_EXTENSION]);
        }


        // - blocage de la réalisation d'un transport
        // -- modifications bloquant la réalisation :
        // --- RE de location, RE de transport, action pro forma, modèle de machine
        // --- si il y a une PF active, les modifications suivantes sont à prendre en compte :
        // ---- onglet durée, prix de location, onglet autres services
        if ((this._rentTariffObj.getSpecialReductionStatus() == '' && this._rentTariffObj.getSpecialReductionAmount() > 0) ||
             this._transportObj.getSpecialReductionStatus() == TRSPTARIFF_REDUCTIONSTATE_PENDING ||
             this._proformaObj.getAction() != null ||
             this._hasChangedComponents.model ||
             (this._proformaObj.getActiveCount() > 0 &&
                 (this._hasChangedComponents.durationObj || this._hasChangedComponents.rentTariffAmount ||
                  this._hasChangedComponents.servicesObj || this._hasChangedComponents.transportTariffAmount)))
        {
            transportDisabledComponents.d.doSide = true;
            transportDisabledComponents.r.doSide = true;
        }

        // - blocage d'éléments du contrat par rapport à la réalisation du transport
        // -- modèle de machine, RE location, estim transport, actions pro forma
        // -- si une pro forma est active : onglet durée, onglet tarification location, onglet autres services
        // -- boutons d'action du contrat
        if ((deliveryAction && deliveryAction.type == 'do') || (recoveryAction && recoveryAction.type == 'do'))
        {
            // modèle de machine : voir isModelChangeable
            rentTariffDisabledComponents.specialReduction = true;
            transportDisabledComponents.estimation        = true;
            proformaDisabledComponents.add                = disabledProformaComponents('add', [PFMTYPE_ID_PROFORMA, PFMTYPE_ID_ADVANCE, PFMTYPE_ID_EXTENSION]);
            proformaDisabledComponents.reactivate         = disabledProformaComponents('reactivate', [PFMTYPE_ID_PROFORMA, PFMTYPE_ID_ADVANCE, PFMTYPE_ID_EXTENSION]);
            proformaDisabledComponents.editable           = disabledProformaComponents('editable', [PFMTYPE_ID_PROFORMA, PFMTYPE_ID_ADVANCE, PFMTYPE_ID_EXTENSION]);
            proformaDisabledComponents.abortable          = disabledProformaComponents('abortable', [PFMTYPE_ID_PROFORMA, PFMTYPE_ID_ADVANCE, PFMTYPE_ID_EXTENSION]);

            if (this._proformaObj.getActiveCount() > 0)
            {
                durationDisabledComponents.period         = true;
                durationDisabledComponents.additionalDays = true;
                durationDisabledComponents.deductedDays   = true;

                rentTariffDisabledComponents.amountType = true;
                rentTariffDisabledComponents.allAmounts = true;

                servicesDisabledComponents.insuranceType   = true;
                servicesDisabledComponents.insuranceRate   = true;
                servicesDisabledComponents.cleaning        = true;
                servicesDisabledComponents.residues        = true;
                servicesDisabledComponents.rentServices    = true;
            }
            // boutons d'action : voir isReactivable, isStoppable, isInvoiceable
        }

        // Livraison réalisée : l'horaire de livraison et la date de début ne sont plus modifiables
        if ((tabDeliveryInfos.isUnloadingDone && !deliveryAction)|| (deliveryAction && deliveryAction.type == 'do'))
        {
            siteDisabledComponents.deliveryHourMasked = true;
            siteDisabledComponents.deliveryTimeSlotId = true;

            durationDisabledComponents.beginDate = true
        }

        // Récupération réalisée : l'horaire de récupération et la date de fin ne sont plus modifiables
        if ((tabRecoveryInfos.isLoadingDone && !recoveryAction) || (recoveryAction && recoveryAction.type == 'do'))
        {
            siteDisabledComponents.recoveryHourMasked = true;
            siteDisabledComponents.recoveryTimeSlotId = true;

            durationDisabledComponents.endDate = true
        }

        // - types de pro forma interdits
        var proformaDispMsgs = [];
        if (this._repairObj.hasInvoicedRepairServices())
        {
            proformaDispMsgs.push('repairServices');
            proformaDisabledComponents.add        = disabledProformaComponents('add', [PFMTYPE_ID_PROFORMA, PFMTYPE_ID_ADVANCE, PFMTYPE_ID_EXTENSION]);
            proformaDisabledComponents.reactivate = disabledProformaComponents('reactivate', [PFMTYPE_ID_PROFORMA, PFMTYPE_ID_ADVANCE, PFMTYPE_ID_EXTENSION]);
        }
        else if (this._repairObj.hasInvoicedRepairDays())
        {
            proformaDispMsgs.push('repairDays');
            proformaDisabledComponents.add = disabledProformaComponents('add', [PFMTYPE_ID_PROFORMA, PFMTYPE_ID_ADVANCE, PFMTYPE_ID_EXTENSION]);
            proformaDisabledComponents.reactivate = disabledProformaComponents('reactivate', [PFMTYPE_ID_PROFORMA, PFMTYPE_ID_ADVANCE, PFMTYPE_ID_EXTENSION]);
        }
        else if (this._rentTariffObj.getAmountType() == 'month')
        {
            proformaDispMsgs.push('rentAmountType');
            proformaDisabledComponents.add        = disabledProformaComponents('add', [PFMTYPE_ID_PROFORMA, PFMTYPE_ID_ADVANCE, PFMTYPE_ID_EXTENSION]);
            proformaDisabledComponents.reactivate = disabledProformaComponents('reactivate', [PFMTYPE_ID_PROFORMA, PFMTYPE_ID_ADVANCE, PFMTYPE_ID_EXTENSION]);
        }
        else if (tabRecoveryInfos.contract)
        {
            proformaDispMsgs.push('activeTransfer');
            proformaDisabledComponents.add        = disabledProformaComponents('add', [PFMTYPE_ID_EXTENSION]);
            proformaDisabledComponents.reactivate = disabledProformaComponents('reactivate', [PFMTYPE_ID_EXTENSION]);
        }

        // - coche Livraison anticipée possible
        if (tabDeliveryInfos.contract)
        {
            var delay = (this._siteObj.isAnticipated() ? this._tabCfgs.maxSitesTsfIntervals.base : this._tabCfgs.maxSitesTsfIntervals.anticipate);
            var limitDateObj = new Date(tabDeliveryInfos.contract.limitDate);
            var deltaBetweenDates = (beginDateObj.getTime() - limitDateObj.getTime()) / (1000 * 60 * 60 * 24);
            if (deltaBetweenDates > delay)
            {
                siteDisabledComponents.isAnticipated = true;
            }
        }

        // - attribution de machine définitive
        // -- impossible de faire un transfert inter-chantiers côté livraison si une machine est déjà attribuée sur le contrat
        if ((this.isFinalAllocation() && !tabDeliveryInfos.contract) || this._customerObj.isNewAccount())
        {
            transportDisabledComponents.d.sitesTransfer = true;
        }

        var tabPeriodLimitDates = this._durationObj.getPeriodLimitDates();
        // - beginDateWindow : limite droite et gauche pour la date de début du contrat
        var beginDateWindowFrom    = (tabDeliveryInfos.contract ? tabDeliveryInfos.contract.limitDate : null);
        var beginDateWindowTill    = null;
        var beginDateWindowTillObj = null;

        // -- par rapport aux J+/-
        var beginDateMaxForDays = tabPeriodLimitDates.beginDateMax;

        // -- par rapport au contrat en transfert inter-chantiers sur la livraison
        if (tabDeliveryInfos.contract)
        {
            var delay = (this._siteObj.isAnticipated() ? this._tabCfgs.maxSitesTsfIntervals.anticipate : this._tabCfgs.maxSitesTsfIntervals.base);

            var limitDateObj = new Date(tabDeliveryInfos.contract.limitDate);
            limitDateObj.setDate(limitDateObj.getDate() + delay);

            var tempDateObj = (endDateObj < limitDateObj ? endDateObj : limitDateObj);
            beginDateWindowTillObj = new Date(Math.max(beginDateObj, tempDateObj));
        }
        // -- Combinaison des différentes dates
        if (beginDateWindowTillObj)
        {
            beginDateWindowTillObj = !beginDateMaxForDays ? beginDateWindowTillObj :
                                                            new Date(Math.min(beginDateWindowTillObj, beginDateMaxForDays));
        }
        else
        {
            beginDateWindowTillObj = beginDateMaxForDays;
        }
        beginDateWindowTill = beginDateWindowTillObj ? beginDateWindowTillObj.toISOString().substring(0, 10) : null;

        // - endDateWindow : limite droite et gauche pour la date de fin du contrat
        var endDateWindowFrom    = null;
        var endDateWindowFromObj = null;
        var endDateWindowTill    = (tabRecoveryInfos.contract ? tabRecoveryInfos.contract.limitDate : null);

        // -- par rapport aux J+/-
        var endDateMinForDays   = tabPeriodLimitDates.endDateMin;

        // -- par rapport au contrat en transfert inter-chantiers sur la récupération
        if (tabRecoveryInfos.contract)
        {
            var delay = (tabRecoveryInfos.contract.isAnticipatable ? this._tabCfgs.maxSitesTsfIntervals.anticipate : this._tabCfgs.maxSitesTsfIntervals.base);

            var limitDateObj   = new Date(tabRecoveryInfos.contract.limitDate);
            var invoiceDateObj = (this._lastInvoiceDate != '' ? new Date(this._lastInvoiceDate) : null);
            limitDateObj.setDate(limitDateObj.getDate() - delay);

            var tempDateObj = (beginDateObj > limitDateObj ? beginDateObj : limitDateObj);
            tempDateObj     = (invoiceDateObj && invoiceDateObj > tempDateObj ? invoiceDateObj : tempDateObj);

            endDateWindowFromObj = new Date(Math.min(beginDateObj, tempDateObj));
        }
        else if (this._lastInvoiceDate != '')
        {
            endDateWindowFromObj = new Date(this._lastInvoiceDate);
        }

        // -- Combinaison des différentes dates
        if (endDateWindowFromObj)
        {
            endDateWindowFromObj = !endDateMinForDays ? endDateWindowFromObj :
                                                            new Date(Math.max(endDateWindowFromObj, endDateMinForDays));
        }
        else
        {
            endDateWindowFromObj = endDateMinForDays;
        }
        endDateWindowFrom = endDateWindowFromObj ? endDateWindowFromObj.toISOString().substring(0, 10) : null;

        // Récupération des horaires de livraison et de récupération
        var deliveryHour = null;
        var recoveryHour = null;
        // Tableau contenant la liste des plages horaires de transport
        var tabTransportTimeSlots = this._tabCfgs.tabTransportTimeSlots;
        // Horaire de la livraison
        if (this._siteObj.getDeliveryHour() != '')
        {
            deliveryHour = this._siteObj.getDeliveryHour();
        }
        else if (this._siteObj.getDeliveryTimeSlotId() != '')
        {
            for (var i = 0; i < tabTransportTimeSlots.length; i++)
            {
                if (this._siteObj.getDeliveryTimeSlotId() == tabTransportTimeSlots[i].id)
                {
                    // On prend le Max de la plage horaire de livraison
                    deliveryHour = tabTransportTimeSlots[i].endHour;
                }
            }
        }
        // Horaire de récupération
        if (this._siteObj.getRecoveryHour() != '')
        {
            recoveryHour = this._siteObj.getRecoveryHour();
        }
        else if (this._siteObj.getRecoveryTimeSlotId() != '')
        {
            for (var i = 0; i < tabTransportTimeSlots.length; i++)
            {
                if (this._siteObj.getRecoveryTimeSlotId() == tabTransportTimeSlots[i].id)
                {
                    // On prend le Min de la plage horaire de récupération
                    recoveryHour = tabTransportTimeSlots[i].beginHour;
                }
            }
        }

        // Date de fin maximale possible sur les pro forma de prolongation pour les contrats en arrêt
        var maxExtensionEndDate = false;
        if (this.getStateId() == RENTCONTRACT_STATE_STOPPED)
        {
            var tempMaxDateObj = new Date(this._durationObj.getEndDate());
            tempMaxDateObj.setDate(tempMaxDateObj.getDate() + this._tabCfgs.nbDaysMaxExtensionStopCtt);
            maxExtensionEndDate = tempMaxDateObj.toISOString().substring(0, 10);
        }

        // Transport : "Transférer à" : désactivation
        // - si on a une visibilité machine
        // - si la machine n'est pas sur le contrat
        // - si la machine n'est pas en récup
        // - si la machine est en récup et que le modèle de machine n'est pas louable (sur agence physique ou de visibilité suivant la visibilité existante)
        if (this._machine.visibilityMode == MACHINE_VISIBILITYMODE_MACHINE ||
             this.getId() != this._machine.contractId ||
             this._machine.stateId != MACHINE_STATE_RECOVERY ||
             this._machine.stateId == MACHINE_STATE_RECOVERY && !this._machine.isRentableModel)
        {
            transportDisabledComponents.r.transferToAgency = true;
        }

        // La machine est-elle dans la même agence que le contrat ?
        var tabMachineInfos = this._machine;
        tabMachineInfos.isContractSameAgency = (this._machine.agencyId == this._agencyId);


        // Assurance
        // - type et taux non modifiables pour les clients en assurance facturée négociée
        // - taux non modifiable et taux par défaut est le seul taux possible
        var insuranceTypeInfos = this._servicesObj.getInsuranceTypeInfos();
        if (insuranceTypeInfos && insuranceTypeInfos.isInvoiced)
        {
            servicesDisabledComponents.insuranceRate = true;
            if (this._servicesObj.getInsuranceRate() != this._tabCfgs.defaultInsuranceRate)
            {
                servicesDisabledComponents.insuranceType = true;
            }
        }
        // - Gestion de recours désactivé si assurance non flaguée "client"
        if (insuranceTypeInfos && !insuranceTypeInfos.isCustomer)
        {
            servicesDisabledComponents.appeal = true;
        }

        var fuelUnitPrice = this._customerObj.getFwkCttFuelUnitPrice();
        var isCalcDisplayed = (fuelUnitPrice !== null && fuelUnitPrice != this._servicesObj.getFuelUnitPrice());


        // Mise à jour des paramètres externes
        this._durationObj.setExternalParams({
            isProformaRequired: isProformaRequired,
            isProformaActived: isProformaActived,
            beginDateWindow: [beginDateWindowFrom, beginDateWindowTill],
            endDateWindow: [endDateWindowFrom, endDateWindowTill],
            hasBaseAmountChanged: this._rentTariffObj.hasBaseAmountChangedFromDuration(),
            disabledComponents: durationDisabledComponents
        });

        this._rentTariffObj.setDays(d);
        this._rentTariffObj.setExternalParams({
            isProformaRequired: isProformaRequired,
            isProformaActived: isProformaActived,
            disabledComponents: rentTariffDisabledComponents
        });

        this._servicesObj.setExternalParams({
            isFuelNeeded: this._model.isFuelNeeded,
            totalDays: this._durationObj.getTotalDays(),
            duration: d,
            calendarDuration: this._durationObj.getCalendarDuration(),
            rentDayPrice: this._rentTariffObj.getDayPrice(),
            isProformaRequired: isProformaRequired,
            isProformaActived: isProformaActived,
            isFullService: this.isFullService(),
            isModelAccessory: this._model.isAccessory,
            customerInsurance: (this._customerObj.getId() != 0 ? {
                                    typeId: this._customerObj.getInsuranceTypeId(),
                                    rate: this._customerObj.getInsuranceRate()
                                } : null),
            customerAppeal: (this._customerObj.getId() != 0 ? {
                                    typeId: this._customerObj.getAppealTypeId()
                                } : null),
            isCustomerResiduesInvoiced: (this._customerObj.getId() != 0 ? this._customerObj.isResiduesInvoiced() : null),
            residuesBaseAmount: this.getResiduesBaseAmount(),
            isCalcDisplayed: isCalcDisplayed,
            disabledComponents: servicesDisabledComponents
        });

        this._proformaObj.setExternalParams({
            duration: d,
            fuelQtyEstimByDay: this._model.fuelQtyEstimByDay,
            fuelQtyMax: this._model.fuelQtyMax,
            isFuelNeeded: this._model.isFuelNeeded,
            endDateDoc: this._durationObj.getEndDate(),
            maxExtensionEndDate: maxExtensionEndDate,
            isCalcDisplayed: isCalcDisplayed,
            displayMessages: proformaDispMsgs,
            disabledComponents: proformaDisabledComponents
        });

        this._transportObj.setExternalParams({
            duration: d,
            isDeliveryAnticipatable: this._siteObj.isAnticipated(),
            deliveryHour: deliveryHour,
            recoveryHour: recoveryHour,
            beginDate: this._durationObj.getBeginDate(),
            endDate: this._durationObj.getEndDate(),
            tabMachineInfos: tabMachineInfos,
            disabledComponents: transportDisabledComponents
        });

        this._repairObj.setExternalParams({
            isProformaRequired: isProformaRequired,
            isProformaActived: isProformaActived
        });

        this._siteObj.setExternalParams({
            deliveryLimitHour: deliveryLimitHour,
            recoveryLimitHour: recoveryLimitHour,
            disabledComponents: siteDisabledComponents
        });

        // Recalcul des éventuelles erreurs
        this._checkErrors();
    },

    /**
     * Définir les commentaires du devis
     *
     * @param string comments Commentaires
     * @return bool
     */
    setComments : function(comments)
    {
        comments = (comments + '').trim();
        if (this._comments == comments || !this.hasRight('infoscpl', 'comments'))
        {
            return false;
        }
        this._comments = comments;
        this.display();
        return true;
    },

    /**
     * Définir si le devis contient des erreurs
     *
     * @param Array|false tabErrors   Liste des erreurs
     * @param int         mode        (0: set, 1: add, 2: del)
     */
    setErrors : function(tabErrors, mode)
    {
        var nb = (tabErrors ? tabErrors.length : 0);
        if (mode == 0)
        {
            this._tabErrors = [];
        }
        if (mode == 2)
        {
            var pos;
            for (var i = 0; i < nb; i++)
            {
                pos = array_search(tabErrors[i], this._tabErrors);
                if (pos !== false)
                {
                    this._tabErrors.splice(pos, 1);
                }
            }
        }
        else
        {
            for (var i = 0; i < nb; i++)
            {
                array_push_unique(this._tabErrors, tabErrors[i]);
            }
        }
    },

    /**
     * Définir si le devis est en FullService ou non
     *
     * @param bool isFullService
     * @return bool
     */
    setFullService : function(isFullService)
    {
        isFullService = (isFullService ? true : false);
        if (this._isFullService == isFullService)
        {
            return false;
        }
        this._isFullService = isFullService;

        this.setAllExternalParams();
        this.display();
        return true;
    },

    /**
     * Définir si le modèle est impératif ou non
     *
     * @param bool isImperative
     * @return bool
     */
    setImperativeModel : function(isImperative)
    {
        isImperative = (isImperative ? true : false);
        if (this._isImperativeModel == isImperative || !this.hasRight('machine', 'isImperativeModel'))
        {
            return false;
        }
        this._isImperativeModel = isImperative;
        this.display();
        return true;
    },

    /**
     * Définir s'il faut founrir les documents de la machine ou non au client
     *
     * @param bool isJoin
     * @return bool
     */
    setJoinDoc : function(isJoin)
    {
        isJoin = (isJoin ? true : false);
        if (this._isJoinDoc == isJoin || !this.hasRight('machine', 'isJoinDoc'))
        {
            return false;
        }
        this._isJoinDoc = isJoin;
        this.display();
        return true;
    },

    /**
     * Définir l'id du négociateur
     *
     * @param int id
     * @return bool
     */
    setNegotiatorId : function(id)
    {
        id = parseInt(id);
        if (isNaN(id) || this._negotiatorId == id)
        {
            return false;
        }
        this._negotiatorId = id;
        this.display();
        return true;
    },

    /**
     * Définir l'id du modèle de machine
     *
     * @param int  modelId  Identifiant du modèle de machine
     * @param bool isForced Forcer la mise à jour
     * @return bool
     */
    setModelId : function(modelId, isForced)
    {
        if (!modelId || (this._model.id == modelId && !isForced))
        {
            return false;
        }
        modelId = Number(modelId);
        if (!this.isModelChangeable())
        {
            return false;
        }
        var mySelf = this;
        this._modelLoadStatus = 'loading';
        this.display();

        if (!this._modelHttpObj)
        {
            this._modelHttpObj = new Location.HTTPRequest(this._tabCfgs['modelInfosUrl']);
        }
        this._modelHttpObj.setVar('modelId', modelId);
        this._modelHttpObj.setVar('customerId', this.getCustomerId());
        this._modelHttpObj.setVar('agencyId', this.getAgencyId());
        this._modelHttpObj.setVar('countryId', this.getCountryId());
        this._modelHttpObj.setVar('tabOptions', {getWishedMachines : true});
        this._modelHttpObj.oncompletion = function()
        {
            var tabModelInfos = this.getJSONResponse();
            if (tabModelInfos)
            {
                var tabOldInfos = {id: mySelf._model.id, isCleaningPossible: mySelf._model.isCleaningPossible};
                // Mise à jour des informations du modèle de machines
                mySelf._loadModelInfos(tabModelInfos);
                if (mySelf._model.id != 0)
                {
                    mySelf._rentTariffObj.setTariffs(tabModelInfos.tabTariffs);

                    // Si le nouveau modèle peut avoir du nettoyage facturé
                    // et que le client est facturé
                    // et que le nettoyage n'était pas facturé précédemment, alors on facture le nettoyage standard
                    var isStdForced = (mySelf.getCustomerObject().isCleaningInvoiced() &&
                                       mySelf._servicesObj.getCleaningTypeId() == 0 &&
                                       !tabOldInfos.isCleaningPossible && mySelf._model.isCleaningPossible);

                    mySelf._servicesObj.setCleaningTypes(tabModelInfos.tabCleaningTypes, isStdForced);
                    mySelf._hasChangedComponents.model = true;
                    mySelf.setAllExternalParams();

                    // Suppression du souhait de machine
                    mySelf._wishedMachineId = null;
                }

                mySelf.display();

                // Evénement
                if (mySelf.onchangemodel)
                {
                    mySelf.onchangemodel(mySelf._model.id, tabOldInfos.id);
                }
            }
            else
            {
                this.onerror();
            }
        }
        this._modelHttpObj.onerror = function()
        {
            mySelf._modelLoadStatus = 'error';
            mySelf.display();
        }
        this._modelHttpObj.send(true, true);

        return true;
    },

    /**
     * Définier l'id de la machine souhaitée
     *
     * @param int wishedMachineId
     * @return bool
     */
    setWishedMachineId : function(id)
    {
        id = (id ? Number(id) : null);
        if (this._wishedMachineId === id)
        {
            return false;
        }
        this._wishedMachineId = id;
        this.display();
        return true;
    },

    /**
     * Définir le devis imprimable ou non
     *
     * @param bool isPrintable
     * @return bool
     */
    setPrintable : function(isPrintable)
    {
        isPrintable = (isPrintable ? true : false);
        if (this._isPrintable == isPrintable)
        {
            return false;
        }
        this._isPrintable = isPrintable;
        this.display();
        return true;
    },

    /**
     * Définir les commentaires de suivi du devis
     *
     * @param string comments Commentaires de suivi
     * @return bool
     */
    setTracking : function(comments)
    {
        comments = (comments + '').trim();
        if (this._tracking == comments || !this.hasRight('infoscpl', 'tracking'))
        {
            return false;
        }
        this._tracking = comments;
        this.display();
        return true;
    },

    /**
     * Ajouter un bouton à la liste des boutons affichés d'une notification
     *
     * @param string notificationId Identifiant de la notification
     * @param string buttonId Identifiant du bouton
     */
    _showNotificationButton : function(notificationId, buttonId)
    {
        if (typeof this._tabNotifications[notificationId].buttons == 'undefined')
        {
            this._tabNotifications[notificationId].buttons = [];
        }
        if (!in_array(buttonId, this._tabNotifications[notificationId].buttons))
        {
            this._tabNotifications[notificationId].buttons.push(buttonId);
        }
    },

    /**
     * Ajouter un sous-message à la liste des sous-messages affichés d'une notification
     *
     * @param string notificationId Identifiant de la notification
     * @param string subMessageId Identifiant du sous-message
     */
    _showNotificationSubMessage : function(notificationId, subMessageId)
    {
        if (typeof this._tabNotifications[notificationId].messages == 'undefined')
        {
            this._tabNotifications[notificationId].messages = [];
        }
        if (!in_array(subMessageId, this._tabNotifications[notificationId].messages))
        {
            this._tabNotifications[notificationId].messages.push(subMessageId);
        }
    },

    /**
     * Rouvrir une fiche de remise en état
     *
     * @param int    sheetId - Identifiant de la fiche de remise en état
     * @param string url     - URL de la page de modification de la fiche de remise en état
     *
     * @return bool
     */
    showReopenRepairSheetPopup : function(sheetId, url)
    {
        // Mise à jour du numéro de la fiche dans le contenu de la popup
        $ge('rentContractReopenSheetNo').innerHTML = sheetId;

        // Assignation des onclick aux boutons
        var mySelf = this;
        // Bouton "Oui et rester sur le contrat"
        obj = $ge('rentContractReopenSheetYesBtn');
        if (obj)
        {
            obj.onclick = function()
            {
                mySelf.reopenRepairSheet(sheetId);
                mySelf.validate('reopenRepairSheets');
                Location.modalWindowManager.hide();
            }
        }
        obj = $ge('rentContractReopenSheetYesSwitchBtn');
        // Bouton "Oui et basculer sur la fiche"
        if (obj)
        {
            obj.onclick = function()
            {
                mySelf.reopenRepairSheet(sheetId);
                mySelf.validate('reopenRepairSheetsAndSwitch');
                Location.modalWindowManager.hide();
            }
        }
        obj = $ge('rentContractReopenSheetNoBtn');
        // Bouton "Non"
        if (obj)
        {
            obj.onclick = function()
            {
                Location.modalWindowManager.hide();
            }
        }

        // Apparition de la popup
        Location.modalWindowManager.show('rentContractReopenSheet', {contentType: 3, width: 470, height: 130});
    },

    /**
     * Valider les modifications apportées au contrat
     *
     * @return bool
     */
    validate : function(subAction)
    {
        var mySelf = this;

        // Vérification des documents en cours de téléversement
        var docMsgSuffix = '';
        switch (subAction)
        {
            case 'stop':
                docMsgSuffix += 'Stop';
                break;
            case 'reactivate':
                docMsgSuffix += 'Reactivate';
                break;
            case 'invoice':
                docMsgSuffix += 'Invoice';
                break;
            case 'restop':
                docMsgSuffix += 'Restop';
                break;
            case 'cancel':
                docMsgSuffix += 'Cancel';
                break;
            default:
                docMsgSuffix += 'Valid';
        }
        if (Location.Rent_Helper_DocumentsView.checkPendingUploads('documentsHlp_', this._documentsObj, function() {
                mySelf.validate(subAction);
            }, null, {
                force: LOC_View.getTranslation('ignorePendingDocsAnd' + docMsgSuffix),
                wait:  LOC_View.getTranslation('waitPendingDocsAnd' + docMsgSuffix),
                back:  LOC_View.getTranslation('goBackToContract')
            }))
        {
            return false;
        }

        this._checkErrors();
        if (this._tabErrors.length > 0)
        {
            this._displayValidateMsg('', subAction);
            return false;
        }

        var result = false;
        if (typeof subAction != 'undefined')
        {
            switch (subAction)
            {
                case 'stop':
                    result = (this.isStoppable() && confirm(LOC_View.getTranslation('answerAreYouSureStop')));
                    break;

                case 'reactivate':
                    result = (this.isReactivable() && confirm(LOC_View.getTranslation('answerAreYouSureReactivate')));
                    break;

                case 'invoice':
                    result = (this.isInvoiceable() && confirm(LOC_View.getTranslation('answerAreYouSureInvoice')));
                    break;

                case 'restop':
                    result = (this.isRestoppable() && confirm(LOC_View.getTranslation('answerAreYouSureRestop')));
                    break;

                case 'cancel':
                    result = (this.isCancelable() && confirm(LOC_View.getTranslation('answerAreYouSureCancel')));
                    break;

                case 'reopenRepairSheets':
                case 'reopenRepairSheetsAndSwitch':
                    result = true;
                    break;
            }
        }
        else
        {
            subAction = '';
            result = this.isValidable();
        }
        if (!result)
        {
            return false;
        }

        this._loadStatus = 'loading';
        this.display();

        // Instanciation de l'objet HTTPRequest pour la récupération des données de la ligne de devis
        if (!this._saveHttpObj)
        {
            this._saveHttpObj = new Location.HTTPRequest(this._tabCfgs['saveUrl']);
        }

        // Récupération des données du contrat
        var tabData = this.getData(subAction);
        this._saveHttpObj.setVar('tabData', tabData);
        this._saveHttpObj.setVar('tabIndex', Location.tabBoxesManager.getTabBox('rentContractTabBox').getActiveTabIndex());


        this._saveHttpObj.oncompletion = function()
        {
            var tabData = this.getJSONResponse();
            if (tabData)
            {
                var url = '';
                if (tabData['@state'] == 'ok')
                {
                    url = tabData['@url'];

                    // Erreur
                    mySelf._tabErrors = [];
                    mySelf._loadStatus = 'ok';

                    mySelf._tabWarnings = tabData['@warnings'];
                }
                else
                {
                    // Récupération des erreurs
                    mySelf.setErrors(tabData['@errors'], 0);
                    mySelf._tabErrorsDetails = tabData['@details'];
                    mySelf._loadStatus = 'ok';
                }
                mySelf.display();
                mySelf._displayValidateMsg(url, tabData['@subAction']);
            }
            else
            {
                // Erreur
                this.onerror();
            }
        }
        this._saveHttpObj.onerror = function()
        {
            // Erreur
            mySelf._loadStatus = 'error';
            mySelf.setErrors([-1], 0); // Erreur fatale
            mySelf._displayValidateMsg('', '');
            mySelf.display();
        }
        this._saveHttpObj.send(true, true);

        return true;
    },

    /**
     * Réactualiser le contrat
     *
     * @return string
     */
    refresh : function()
    {
        var mySelf = this;

        // Instanciation de l'objet HTTPRequest pour la récupération de l'URl du contrat
        if (!this._getUrlHttpObj)
        {
            this._getUrlHttpObj = new Location.HTTPRequest(this._tabCfgs['getUrlUrl']);
        }

        // Envoi des paramètres
        this._getUrlHttpObj.setVar('contractId', this._id);
        this._getUrlHttpObj.setVar('tabIndex', Location.tabBoxesManager.getTabBox('rentContractTabBox').getActiveTabIndex());


        this._getUrlHttpObj.oncompletion = function()
        {
            var tabData = this.getJSONResponse();
            if (tabData)
            {
                window.location.href = tabData['url'];
            }
            else
            {
                // Erreur
                this.onerror();
            }
        }
        this._getUrlHttpObj.onerror = function()
        {
            // Erreur
            mySelf._loadStatus = 'error';
            mySelf.setErrors([-1], 0); // Erreur fatale
            mySelf._displayValidateMsg('', '');
            mySelf.display();
        }
        this._getUrlHttpObj.send(true, true);

        return true;
    },

    /**
     * Mise à jour de l'affichage des boutons de contrôles
     *
     */
    _displayControls : function()
    {
        var isUpdateBtnsVisibles = (this.getEditMode() == 'update');
        // Bouton de validation
        $ge('rentContractValidBtn').className = 'locCtrlButton' + (this.isValidable() ? '' : ' disabled');
        // Bouton d'annulation
        $ge('rentContractResetBtn').className = 'locCtrlButton' + (this.isResetable() ? '' : ' disabled');
        $ge('rentContractResetBtn').style.display = (isUpdateBtnsVisibles ? '' : 'none');

        // Groupe des icônes d'actions
        var el = $ge('rentContractActionBtns');
        if (el)
        {
            var t = [this.isStoppable(), this.isReactivable(), this.isInvoiceable(), this.isRestoppable(), this.isCancelable()];
            if (in_array(true, t))
            {
                el.style.display = '';
                // Bouton d'arrêt
                $ge('rentContractStopBtn').className = 'locCtrlButton' + (t[0] ? '' : ' disabled');
                // Bouton de remise en cours
                $ge('rentContractReactivateBtn').className = 'locCtrlButton' + (t[1] ? '' : ' disabled');
                // Bouton de mise en facturation
                $ge('rentContractInvoiceBtn').className = 'locCtrlButton' + (t[2] ? '' : ' disabled');
                // Bouton de remise en arrêt
                $ge('rentContractRestopBtn').className = 'locCtrlButton' + (t[3] ? '' : ' disabled');
                // Bouton d'annulation du contrat
                $ge('rentContractCancelBtn').className = 'locCtrlButton' + (t[4] ? '' : ' disabled');
            }
            else
            {
                el.style.display = 'none';
            }
        }

        // Boutons d'impression
        el = $ge('rentContractPrintBtns');
        if (el)
        {
            el.style.display = (this.isPrintable() ? '' : 'none');
        }
        // Boutons d'impression du bon de valorisation
        el = $ge('rentContractVlzPrintBtns');
        if (el)
        {
            el.style.display = (this.isValorizationPrintable() ? '' : 'none');
        }
    },

    /**
     * Détecter les erreurs
     *
     * @return bool
     */
    _checkErrors : function()
    {
        var tabErrors = [];

        // Document ajouté en erreur ?
        if (this._documentsObj.addList.hasError())
        {
            tabErrors.push(700);
        }

        this.setErrors(tabErrors, 0);

        // Erreur sur la tarification transport
        this._transportObj.checkEstimation();

        // Erreur sur le contact commande
        this._customerObj.checkOrderContactEmail();

        // Erreur sur le contact chantier
        this._siteObj.checkContactEmail();

        return (tabErrors.length == 0);
    },

    /**
     * Récupérer les informations sur les onglets: activé ou non, état, tableau d'erreurs
     *
     * @return Array
     */
    _getTabsInfos : function()
    {
        var tab = [];

        tab[TABINDEX_EQUIPMENT] = [true,  '', {}];
        tab[TABINDEX_DURATION]  = [false, '', {}];
        tab[TABINDEX_RENT]      = [false, '', {}];
        tab[TABINDEX_TRANSPORT] = [false, '', {}];
        tab[TABINDEX_REPAIR]    = [false, '', {}];
        tab[TABINDEX_SERVICES]  = [false, '', {}];
        tab[TABINDEX_PROFORMA]  = [false, '', {}];
        tab[TABINDEX_DOCUMENTS] = [false, '', {}];
        tab[TABINDEX_INVOICES]  = [false, '', {}];
        tab[TABINDEX_HISTORY]   = [false, '', {}];

        if (this.getModelId() != 0)
        {
            tab[TABINDEX_DURATION][0] = true;
        }
        if (this.getModelId() != 0 && (this._durationObj.getBeginDate() != '' && this._durationObj.getEndDate() != ''))
        {
            tab[TABINDEX_RENT][0] = true;
            tab[TABINDEX_TRANSPORT][0] = true;
            tab[TABINDEX_SERVICES][0] = tab[TABINDEX_PROFORMA][0] = (this._durationObj.getCalendarDuration() > 0);
            tab[TABINDEX_DOCUMENTS][0] = true;
            tab[TABINDEX_REPAIR][0] = tab[TABINDEX_INVOICES][0] = tab[TABINDEX_HISTORY][0] = (this.getEditMode() == 'update');
        }

        // Warnings
        // Le nombre de jours ouvrés a changé
        if (this._durationObj.isRealDurationChanged())
        {
            tab[TABINDEX_DURATION][1] = 'warning';
        }
        // Le tarif de base a changé suite à une modification dans les dates
        if (this._durationObj.hasBaseAmountChanged())
        {
            tab[TABINDEX_DURATION][1] = 'warning';
        }
        // Remise négative ou tarif a changé
        if (this._rentTariffObj.getReductionPercent() < 0 || this._rentTariffObj.isTariffChange())
        {
            tab[TABINDEX_RENT][1] = 'warning';
        }
        // Changement de l'estimation transport ou du modèle de machine sur un contrat en TIC
        if (this._transportObj.isEstimationChange())
        {
            tab[TABINDEX_TRANSPORT][1] = 'warning';
        }

        // Errors
        var nbErrors = this._tabErrors.length;
        for (var i = 0; i < nbErrors; i++)
        {
            var error = this._tabErrors[i];

            // Modèle non renseigné
            if (error == 105 || error == 400)
            {
                tab[TABINDEX_EQUIPMENT][1] = 'error';
                tab[TABINDEX_EQUIPMENT][2].noModel = true;
            }
            // Nombre de jours ouvrés à recalculer (136)
            if (error == 136)
            {
                tab[TABINDEX_DURATION][1] = 'error';
            }
            // Tarif de location non défini
            else if (error == 201)
            {
                tab[TABINDEX_RENT][1] = 'error';
            }
            // Justificatif de la remise exceptionnelle obligatoire
            else if (error == 202)
            {
                tab[TABINDEX_RENT][1] = 'error';
                tab[TABINDEX_RENT][2].noJustification = true;
            }
            // Erreurs tarification transport
            else if (error == 104 || error == 106 || (error >= 300 && error < 400))
            {
                tab[TABINDEX_TRANSPORT][1] = 'error';
                if (error == 304 || error == 308)
                {
                    tab[TABINDEX_TRANSPORT][2].deliveryAmount = true;
                }
                if (error == 305 || error == 309)
                {
                    tab[TABINDEX_TRANSPORT][2].recoveryAmount = true;
                }
            }
            // Erreurs documents
            else if (error == 700)
            {
                tab[TABINDEX_DOCUMENTS][1] = 'error';
            }
        }
        var searchBoxObj = Location.searchBoxesManager.getSearchBox('rentContractModelId');
        searchBoxObj.setValue(this.getModelId(), false); // TODO: Modifier ce système avec l'attribut "norentable"
        var selectElt = searchBoxObj.getSelectElement();
        if (selectElt.options.length == 0 || selectElt.selectedIndex == -1 || selectElt.options[selectElt.selectedIndex].hasAttribute('norentable'))
        {
            tab[TABINDEX_EQUIPMENT][1] = 'error';
            tab[TABINDEX_EQUIPMENT][2].noModel = true;
        }

        // Indicateur pour la pro forma à générer
        tab[TABINDEX_PROFORMA][1] = (isProformaToGen({docEditMode: this.getEditMode(), stateId: this.getStateId(),
                                          isCustomerRequired: this._customerObj.isProformaRequired(),
                                          totalAmount: this.getTotal(),
                                          flags: this._proformaFlags,
                                          isRequiredOnCreation : (this._proformaFlags & PFMDOCFLAG_CT_ISCUSTOMERPFREQONCREATION)}) ? 'warning' : '');

        // Indicateur si le carburant est different de l'estimation (onglet autres services)
        tab[TABINDEX_SERVICES][1] = (!this._servicesObj.isFuelEqualEstimation() ? 'warning' : '');

        // Indicateur nb de documents
        tab[TABINDEX_DOCUMENTS][2].badge = this._documentsObj.list.getList().length;

        // Indicateur nb de fiches / devis de remise en état en attente
        tab[TABINDEX_REPAIR][1] = (this._repairObj.getActiveRepairSheetsList().length > 0 ||
                                   this._repairObj.getPendingRepairEstimatesCount() > 0 ? 'warning' : '');

        return tab;
    },

    /**
     * Supprimer un bouton de la liste des boutons affichés d'une notification
     *
     * @param string notificationId Identifiant de la notification
     * @param string buttonId Identifiant du bouton
     */
    _hideNotificationButton : function(notificationId, buttonId)
    {
        if (typeof this._tabNotifications[notificationId].buttons == 'object')
        {
            var index = this._tabNotifications[notificationId].buttons.indexOf(buttonId);
            if (index != -1)
            {
                this._tabNotifications[notificationId].buttons.splice(index, 1);
            }
        }
    },

    /**
     * Supprimer un sous-message de la liste des sous-messages affichés d'une notification
     *
     * @param string notificationId Identifiant de la notification
     * @param string subMessageId Identifiant du sous-message
     */
    _hideNotificationSubMessage : function(notificationId, subMessageId)
    {
        if (typeof this._tabNotifications[notificationId].messages == 'object')
        {
            var index = this._tabNotifications[notificationId].messages.indexOf(subMessageId);
            if (index != -1)
            {
                this._tabNotifications[notificationId].messages.splice(index, 1);
            }
            if (this._tabNotifications[notificationId].messages == [])
            {
                delete this._tabNotifications[notificationId].messages;
            }
        }
        if (typeof this._tabNotifications[notificationId].messages == 'undefined')
        {
            this._tabNotifications[notificationId].displayType = 'hide';
        }
    },

    /**
     * Affichage des notifications
     */
    _displayNotifications : function()
    {
        this._initNotifications();


        // Objets
        var transportObj = this.getTransportObject();
        var proformaObj = this._proformaObj;
        var documentsObj = this._documentsObj;
        var rentTariffObj = this._rentTariffObj;
        var repairObj = this._repairObj;

        // Informations du transport avant modifications
        var oldDeliveryInfos = transportObj.getSideInfos('d', true);
        var oldRecoveryInfos = transportObj.getSideInfos('r', true);

        // Informations du transport après modifications
        var deliveryInfos = transportObj.getSideInfos('d');
        var recoveryInfos = transportObj.getSideInfos('r');

        // Actions en cours
        var deliveryAction = transportObj.getSideAction('d');
        var recoveryAction = transportObj.getSideAction('r');
        var documentsAction = documentsObj.getActions();

        // Transport modifiable hors action en cours
        var isOldDeliveryEditable = transportObj.isSideEditable('d', true);
        var isOldRecoveryEditable = transportObj.isSideEditable('r', true);

        // Notification d'envoi de la demande de déblocage client
        var custUnlockRequestStatus = this.getCustomerObject().getUnlockRequestStatus();
        if (custUnlockRequestStatus == 'sended')
        {
            this._tabNotifications['unlockCustomerRequest-send'].displayType = 'show';
        }
        else if (custUnlockRequestStatus == 'error')
        {
            this._tabNotifications['unlockCustomerRequest-error'].displayType = 'show';
        }

        // Notification d'impact sur le montant du contrat lié dans le cas
        // où on passe sur une livraison de type "Reprise sur chantier" et le contrat lié n'est pas déjà en enlèvement
        // ou qu'on change une livraison "Reprise sur chantier" en "Transfert inter-chantiers"
        // ou qu'on passe sur une livraison de type "Transfert inter-chantiers" et que le contrat lié est en enlèvement
        if (deliveryInfos.contract &&
              (!(oldDeliveryInfos.isSelf && oldDeliveryInfos.contract) && deliveryInfos.isSelf && !deliveryInfos.contract.isSelf ||
                  oldDeliveryInfos.isSelf && oldDeliveryInfos.contract && !deliveryInfos.isSelf ||
                  deliveryInfos.contract.isSelf && !deliveryInfos.isSelf))
        {
            this._tabNotifications['amountImpactOnDeliveryLinkedContract'].displayType = 'show';
            var link = '<a href="' + deliveryInfos.contract.url + '" target="_blank">' +
                       deliveryInfos.contract.code + '</a>';
            this._tabNotifications['amountImpactOnDeliveryLinkedContract'].vars = {'contractCode' : link};
        }
        // ou qu'on change une livraison "Reprise sur chantier" (sur une même agence) en livraison std (avec ou sans enlèvement)
        if (!deliveryInfos.contract && oldDeliveryInfos.isSelf && oldDeliveryInfos.contract &&
                oldDeliveryInfos.contract.agency.id == this.getAgencyId())
        {
            this._tabNotifications['amountImpactOnDeliveryLinkedContract'].displayType = 'show';
            var link = '<a href="' + oldDeliveryInfos.contract.url + '" target="_blank">' +
            oldDeliveryInfos.contract.code + '</a>';
            this._tabNotifications['amountImpactOnDeliveryLinkedContract'].vars = {'contractCode' : link};
        }


        // Notification d'impact sur le montant du contrat lié dans le cas
        // où on passe sur une récupération de type "Reprise sur chantier" et le contrat lié n'est pas déjà en enlèvement
        // ou qu'on change une récupération "Reprise sur chantier" en "Transfert inter-chantiers"
        // ou qu'on passe sur une récupération de type "Transfert inter-chantiers" et que le contrat lié est en enlèvement
        if (recoveryInfos.contract &&
            (!(oldRecoveryInfos.isSelf && oldRecoveryInfos.contract) && recoveryInfos.isSelf && !recoveryInfos.contract.isSelf ||
                oldRecoveryInfos.isSelf && oldRecoveryInfos.contract && !recoveryInfos.isSelf ||
                recoveryInfos.contract.isSelf && !recoveryInfos.isSelf))
        {
            this._tabNotifications['amountImpactOnRecoveryLinkedContract'].displayType = 'show';
            var link = '<a href="' + recoveryInfos.contract.url + '" target="_blank">' +
                       recoveryInfos.contract.code + '</a>';
            this._tabNotifications['amountImpactOnRecoveryLinkedContract'].vars = {'contractCode' : link};
        }
        // ou qu'on change une récupération "Reprise sur chantier" (sur une même agence) en récupération std (avec ou sans enlèvement)
        if (!recoveryInfos.contract && oldRecoveryInfos.isSelf && oldRecoveryInfos.contract &&
                oldRecoveryInfos.contract.agency.id == this.getAgencyId())
        {
            this._tabNotifications['amountImpactOnRecoveryLinkedContract'].displayType = 'show';
            var link = '<a href="' + oldRecoveryInfos.contract.url + '" target="_blank">' +
            oldRecoveryInfos.contract.code + '</a>';
            this._tabNotifications['amountImpactOnRecoveryLinkedContract'].vars = {'contractCode' : link};
        }

        // Notification de blocage de la suppression d'une reprise sur chantier
        // si le contrat concerné par la reprise est sur une autre agence et que le montant de son transport est à 0
        if (this.hasRight('transport', 'self') && deliveryInfos.isSelf && deliveryInfos.contract &&
                deliveryInfos.contract.agency.id != this.getAgencyId() && deliveryInfos.contract.amount != 0)
        {
            this._tabNotifications['deliverySelfSiteTransferNotEditable'].displayType = 'show';
            var link = '<a href="' + deliveryInfos.contract.url + '" target="_blank">' +
            deliveryInfos.contract.code + '</a>';
            this._tabNotifications['deliverySelfSiteTransferNotEditable'].vars = {'contractCode' : link};
        }

        if (this.hasRight('transport', 'self') && recoveryInfos.isSelf && recoveryInfos.contract &&
                recoveryInfos.contract.agency.id != this.getAgencyId() && recoveryInfos.contract.amount != 0)
        {
            this._tabNotifications['recoverySelfSiteTransferNotEditable'].displayType = 'show';
            var link = '<a href="' + recoveryInfos.contract.url + '" target="_blank">' +
            recoveryInfos.contract.code + '</a>';
            this._tabNotifications['recoverySelfSiteTransferNotEditable'].vars = {'contractCode' : link};
        }

        // Notifications d'impossibilité de modifier la coche "Transférée à"
        // si le contrat est en récupération et que la récup n'est pas encore réalisée
        if (this.hasRight('transport', 'transferToAgency') && this.getStateId() == RENTCONTRACT_STATE_STOPPED &&
            !recoveryInfos.isLoadingDone)
        {
            // Machine mise en visibilité depuis la fiche machine
            if (this._machine.visibilityMode == MACHINE_VISIBILITYMODE_MACHINE)
            {
                this._tabNotifications['transferToAgency-yetVisible'].displayType = 'show';
                this._tabNotifications['transferToAgency-yetVisible'].vars =
                        {'visibleOnAgencyLabel' : this._machine.visibleOnAgencyLabel};
            }
            // Pas de visibilité
            // et aucune machine du modèle de la machine attribuée n'est louable dans l'agence physique
            // - et pas de "transférée à sur la récup" (cas de l'annulation de la récup où la machine est déjà dans l'autre agence)
            if (this._machine.visibilityMode == MACHINE_VISIBILITYMODE_NONE && !this._machine.isRentableModel && !recoveryInfos.transferToAgency)
            {
                this._tabNotifications['transferToAgency-unrentable'].displayType = 'show';
                this._tabNotifications['transferToAgency-unrentable'].vars = {'modelLabel' : this._machine.modelLabel};
            }
            // Pas d'action en cours (annulation de la réalisation)
            // et coche sélectionnée
            // et aucune machine du modèle de la machine attribuée n'est louable dans l'agence de visibilité
            if (!recoveryAction && recoveryInfos.transferToAgency && !this._machine.isRentableModel)
            {
                this._tabNotifications['transferToAgency-unrentableInVisibilityAgency'].displayType = 'show';
                this._tabNotifications['transferToAgency-unrentableInVisibilityAgency'].vars =
                        {'modelLabel' : this._machine.modelLabel,
                         'visibleOnAgencyLabel' : this._machine.visibleOnAgencyLabel};
            }
        }

        // Modification de la livraison
        if (transportObj.isSideUpdated('d'))
        {
            // Notification de réalisation ou d'annulation de réalisation impossible
            // suite à la modification de la livraison
            if ((oldDeliveryInfos.isSelf && this.hasRight('transport', 'do-self') ||
                        !oldDeliveryInfos.isSelf && this.hasRight('transport', 'do-std')) &&
                    oldDeliveryInfos.possibilities.action && oldDeliveryInfos.possibilities.action == 'do')
            {
                var notification = 'delivery' + (oldDeliveryInfos.isUnloadingDone ? 'NotUndoable' : 'NotDoable');
                this._tabNotifications[notification].displayType = 'show';
                this._showNotificationSubMessage(notification, 'editionPending');
            }
            if (!this._hasChangedComponents.deliveryIsSynchroDisabled && deliveryInfos.isSynchroDisabled)
            {
                this._tabNotifications['synchroDisabled'].displayType = 'show';
            }
            this._tabNotifications['edit'].displayType = 'show';
        }

        // Modification de la récupération
        if (transportObj.isSideUpdated('r'))
        {
            // Notification de réalisation ou d'annulation de réalisation impossible
            // suite à la modification de la récupération
            if ((oldRecoveryInfos.isSelf && this.hasRight('transport', 'do-self') ||
                        !oldRecoveryInfos.isSelf && this.hasRight('transport', 'do-std')) &&
                    oldRecoveryInfos.possibilities.action && oldRecoveryInfos.possibilities.action == 'do')
            {
                var notification = 'recovery' + (oldRecoveryInfos.isLoadingDone ? 'NotUndoable' : 'NotDoable');
                this._tabNotifications[notification].displayType = 'show';
                this._showNotificationSubMessage(notification, 'editionPending');
            }
            if (!this._hasChangedComponents.recoveryIsSynchroDisabled && recoveryInfos.isSynchroDisabled)
            {
                this._tabNotifications['synchroDisabled'].displayType = 'show';
            }
            this._tabNotifications['edit'].displayType = 'show';
        }

        // Notification de modification de la livraison impossible
        // suite à une réalisation ou une annulation de réalisation en attente
        if (deliveryAction)
        {
            // Champs bloqués s'il y a une pro forma non finalisée
            if ((this.hasRight('machine', 'model') || this.hasRight('duration') || this.hasRight('rentTariff') ||
                        this.hasRight('transport', 'amount') || this.hasRight('services')) &&
                    deliveryAction.type == 'do' && proformaObj.getActiveCount() > 0)
            {
                this._tabNotifications['uneditableFields-unfinalizedProforma'].displayType = 'show';
            }
            if (isOldDeliveryEditable &&
                    (this.hasRight('transport', 'addMachine') || this.hasRight('transport', 'amount') ||
                     this.hasRight('transport', 'delegate') || this.hasRight('transport', 'self') ||
                     this.hasRight('transport', 'synchro') || this.hasRight('transport', 'transfer') ||
                     this.hasRight('transport', 'transferToAgency')))
            {
                // Aucune pro forma active
                // Modification de la livraison interdite
                if (proformaObj.getActiveCount() == 0 || oldDeliveryInfos.isUnloadingDone)
                {
                    var notification = 'deliveryNotEditable-' +
                                       (oldDeliveryInfos.isUnloadingDone ? 'undoPending' : 'doPending');
                    this._tabNotifications[notification].displayType = 'show';
                }
                // Une pro forma active (donc risque d'expiration si on modifie le montant de la récupération)
                // Modifications de la livraison et de la récupération interdites
                else
                {
                    this._tabNotifications['transportNotEditable-doDeliveryPending'].displayType = 'show';
                }
            }
            this._tabNotifications['edit'].displayType = 'show';
        }

        // Notification de modification de la récupération impossible
        // suite à une réalisation ou une annulation de réalisation en attente
        if (recoveryAction)
        {
            if (isOldRecoveryEditable &&
                    (this.hasRight('transport', 'addMachine') || this.hasRight('transport', 'amount') ||
                     this.hasRight('transport', 'delegate') || this.hasRight('transport', 'self') ||
                     this.hasRight('transport', 'synchro') || this.hasRight('transport', 'transfer') ||
                     this.hasRight('transport', 'transferToAgency')))
            {
                // Aucune pro forma active
                // Modification de la récupération interdite
                if (proformaObj.getActiveCount() == 0 || oldRecoveryInfos.isLoadingDone)
                {
                    var notification = 'recoveryNotEditable-' +
                                       (oldRecoveryInfos.isLoadingDone ? 'undoPending' : 'doPending');
                    this._tabNotifications[notification].displayType = 'show';
                }
                // Une pro forma active (donc risque d'expiration si on modifie le montant de la livraison)
                // Modifications de la livraison et de la récupération interdites
                else
                {
                    this._tabNotifications['transportNotEditable-doRecoveryPending'].displayType = 'show';
                }
            }
            this._tabNotifications['edit'].displayType = 'show';
        }

        // Remise en état en attente
        var pendingRepairSheets = repairObj.getActiveRepairSheetsList().length;
        var pendingRepairEstimates = repairObj.getPendingRepairEstimatesCount();

        if (pendingRepairSheets > 0 || pendingRepairEstimates > 0)
        {
            this._tabNotifications['pendingRepair'].displayType = 'show';
            this._tabNotifications['pendingRepair'].vars = {};

            if (pendingRepairSheets > 0)
            {
                var text = (pendingRepairSheets <= 1 ? LOC_View.getTranslation('pendingRepairSheets.single') :
                                                       LOC_View.getTranslation('pendingRepairSheets.plural'));

                text = text.replace(/<%nbPendingRepairSheets>/, pendingRepairSheets);
                this._tabNotifications['pendingRepair'].vars['pendingRepairSheets'] = text;
                this._showNotificationSubMessage('pendingRepair', 'sheet');
            }

            // Devis de remise en état en attente
            var pendingRepairEstimates = repairObj.getPendingRepairEstimatesCount();
            if (pendingRepairEstimates > 0)
            {
                var text = LOC_View.getTranslation('pendingRepairEstimates');

                text = text.replace(/<%nbPendingRepairEstimates>/, pendingRepairEstimates);
                this._tabNotifications['pendingRepair'].vars['pendingRepairEstimates'] = text;
                this._showNotificationSubMessage('pendingRepair', 'estimate');
            }
        }

        // Actions en cours sur les documents
        if (documentsAction.length > 0)
        {
            this._tabNotifications['edit'].displayType = 'show';
        }

        // Changements d'état du contrat
        // Le contrat est à l'état "Contrat"
        if (this.getStateId() == RENTCONTRACT_STATE_CONTRACT)
        {
            // Arrêt
            if (this.hasRight('actions', 'stop'))
            {
                // Date de fin dans plus de 4 jours
                if (!this._isEndDateNear)
                {
                    this._tabNotifications['cantStop'].displayType = 'show';
                    this._showNotificationSubMessage('cantStop', 'endDate');
                }
                // Remise exceptionnelle de transport en attente
                if (rentTariffObj.getSpecialReductionStatus() == RENTTARIFF_REDUCTIONSTATE_PENDING)
                {
                    this._tabNotifications['cantStop'].displayType = 'show';
                    this._showNotificationSubMessage('cantStop', 'rentExcepReduction');
                }
                // Remise exceptionnelle de location en attente
                if (transportObj.getSpecialReductionStatus() == TRSPTARIFF_REDUCTIONSTATE_PENDING)
                {
                    this._tabNotifications['cantStop'].displayType = 'show';
                    this._showNotificationSubMessage('cantStop', 'transportExcepReduction');
                }
                // Pro forma non finalisées
                if (proformaObj.getActiveCount({'isFinalized' : false}) > 0)
                {
                    this._tabNotifications['cantStop'].displayType = 'show';
                    this._showNotificationSubMessage('cantStop', 'unfinalizedProforma');
                }
                // Pro forma expirées
                if (proformaObj.getActiveCount({'isExpired' : true}) > 0)
                {
                    this._tabNotifications['cantStop'].displayType = 'show';
                    this._showNotificationSubMessage('cantStop', 'expiredProforma');
                }
            }
        }
        // Le contrat est à l'état "Arrêt"
        if (this.getStateId() == RENTCONTRACT_STATE_STOPPED)
        {
            // Remise en cours
            if (this.hasRight('actions', 'reactivate'))
            {
                // Récupération déjà réalisée (hors action de réalisation en cours)
                if (!recoveryAction && recoveryInfos.isLoadingDone)
                {
                    this._tabNotifications['cantReactivate'].displayType = 'show';
                    this._showNotificationSubMessage('cantReactivate', 'doneRecovery');
                    if (transportObj.isSideUndoable('r'))
                    {
                        this._showNotificationButton('cantReactivate', 'undoRecovery');
                    }
                }
                // Location entièrement facturée
                if (this._invoiceState == RENTCONTRACT_INVOICESTATE_FINALPARTIAL ||
                    this._invoiceState == RENTCONTRACT_INVOICESTATE_FINAL)
                {
                    this._tabNotifications['cantReactivate'].displayType = 'show';
                    this._showNotificationSubMessage('cantReactivate', 'allRentInvoiced');
                }
                // Machine visible (toujours sur le contrat) sur une autre agence (visibilité machine)
                if (this._machine.id && this.getId() == this._machine.contractId &&
                    this._machine.visibilityMode == MACHINE_VISIBILITYMODE_MACHINE)
                {
                    this._tabNotifications['cantReactivate'].displayType = 'show';
                    this._showNotificationSubMessage('cantReactivate', 'visibleMachine');
                }
                // Coche "Transférer à" active sur la récupération
                // - et récupération non réalisée
                // - et non en cours d'annulation
                if (recoveryInfos.transferToAgency && !recoveryInfos.isLoadingDone && !oldRecoveryInfos.isLoadingDone)
                {
                    this._tabNotifications['cantReactivate'].displayType = 'show';
                    this._showNotificationSubMessage('cantReactivate', 'machineToTransfer');
                }
                // Toutes les machines du modèle demandé sur le contrat sont réservées
                // et la machine n'est pas mise en visibilité
                if (this._machine.visibilityMode == MACHINE_VISIBILITYMODE_NONE && !this._model.isRentable)
                {
                    this._tabNotifications['cantReactivate'].displayType = 'show';
                    this._tabNotifications['cantReactivate'].vars = {'modelLabel' : this._model.label};
                    this._showNotificationSubMessage('cantReactivate', 'modelUnrentable');
                }
                // Le modèle demandé sur le contrat ne correspond pas au modèle de la machie attribuée
                if (this._machine.modelId != this._model.id)
                {
                    this._tabNotifications['cantReactivate'].displayType = 'show';
                    this._showNotificationSubMessage('cantReactivate', 'incoherentModels');
                }
            }
            // Mise en facturation
            if (this.hasRight('actions', 'invoice'))
            {
                // Récupération non réalisée (hors action de réalisation en cours)
                if (!recoveryAction && !recoveryInfos.isLoadingDone)
                {
                    this._tabNotifications['cantInvoice'].displayType = 'show';
                    this._showNotificationSubMessage('cantInvoice', 'undoneRecovery');
                    if (transportObj.isSideDoable('r'))
                    {
                        this._showNotificationButton('cantInvoice', 'doRecovery');
                    }
                }
                // Remise exceptionnelle de transport en attente
                if (rentTariffObj.getSpecialReductionStatus() == RENTTARIFF_REDUCTIONSTATE_PENDING)
                {
                    this._tabNotifications['cantInvoice'].displayType = 'show';
                    this._showNotificationSubMessage('cantInvoice', 'rentExcepReduction');
                }
                // Remise exceptionnelle de location en attente
                if (transportObj.getSpecialReductionStatus() == TRSPTARIFF_REDUCTIONSTATE_PENDING)
                {
                    this._tabNotifications['cantInvoice'].displayType = 'show';
                    this._showNotificationSubMessage('cantInvoice', 'transportExcepReduction');
                }
                // Pro forma non finalisées
                if (proformaObj.getActiveCount({'isFinalized' : false}) > 0)
                {
                    this._tabNotifications['cantInvoice'].displayType = 'show';
                    this._showNotificationSubMessage('cantInvoice', 'unfinalizedProforma');
                }
                // Pro forma expirées
                if (proformaObj.getActiveCount({'isExpired' : true}) > 0)
                {
                    this._tabNotifications['cantInvoice'].displayType = 'show';
                    this._showNotificationSubMessage('cantInvoice', 'expiredProforma');
                }
                // Carburant non saisi (si nécessaire)
                if (this._model.isFuelNeeded && this._servicesObj.getFuelQuantity() == null)
                {
                    this._tabNotifications['cantInvoice'].displayType = 'show';
                    this._showNotificationSubMessage('cantInvoice', 'fuel');
                    if (this.hasRight('services', 'fuel'))
                    {
                        this._showNotificationButton('cantInvoice', 'enterFuel');
                    }
                }
            }
        }
        // Le contrat est à l'état "Mis en facturation"
        if (this.getStateId() == RENTCONTRACT_STATE_BILLED)
        {
            // Remise en arrêt
            if (this.hasRight('actions', 'restop'))
            {
                // Dernière facture de location générée
                if (this._invoiceState != RENTCONTRACT_INVOICESTATE_FINAL || this.getClosureDate() != '')
                {
                    this._tabNotifications['cantRestop'].displayType = 'show';
                    this._showNotificationSubMessage('cantRestop', 'invoiced');
                }
                else
                {
                    // Transport de récupération de type "enlèvement agence" avec montant supérieur à 0 : le montant de la récup va être modifié
                    if (oldRecoveryInfos.isSelf && !oldRecoveryInfos.contract && oldRecoveryInfos.amount > 0)
                    {
                        this._tabNotifications['restop-selfRecoveryWithAmount'].displayType = 'show';
                    }
                }
            }
        }

        // Pas de nouvelle pro forma de prolongation
        // s'il y a des pro forma actives finalisées et non expirées
        // lorsque la récupération est une reprise sur chantier ou un transfert inter-chantiers
        if (this.hasRight('proforma', 'add') &&
                proformaObj.getActiveCount({'isFinalized' : true, 'isExpired' : false}) > 0 && recoveryInfos.contract)
        {
            this._tabNotifications['recoveryTransfer-noExtensionProforma'].displayType = 'show';
        }

        // Pas de récupération en reprise sur chantier ou transfert inter-chantiers
        // s'il y a une pro forma de prolongation non finalisée
        if ((this.hasRight('transport', 'self') || this.hasRight('transport', 'transfer')) &&
                proformaObj.getActiveCount({'type' : PFMTYPE_ID_EXTENSION, 'isFinalized' : false}) > 0)
        {
            this._tabNotifications['extensionProforma-noRecoveryTransfer'].displayType = 'show';
        }

        // Pas de transfert inter-chantiers ou de reprise sur chantier
        if (transportObj.isSideEditable('d') &&
            (this.hasRight('transport', 'self') || this.hasRight('transport', 'transfer')) &&
            !deliveryInfos.contract && deliveryInfos.nbContractsForSiteTransfer > 0)
        {
            // s'il y a une machine attribuée
            if (this.isFinalAllocation() && this.getMachineId())
            {
                this._tabNotifications['deliveryTransferUnavailable'].displayType = 'show';
                this._showNotificationSubMessage('deliveryTransferUnavailable', 'assignedMachine');
            }

            // s'il y a une ouverture de compte en attente
            if (this.getCustomerObject().isNewAccount())
            {
                this._tabNotifications['deliveryTransferUnavailable'].displayType = 'show';
                this._showNotificationSubMessage('deliveryTransferUnavailable', 'customerAccountPending');
            }
        }

        // Pas de changement de contrat associé au transfert inter-chantiers
        // si la récupération de l'autre contrat est réalisée
        if (transportObj.isSideEditable('d') &&
            this.hasRight('transport', 'transfer') &&
            deliveryInfos.contract && deliveryInfos.isLoadingDone &&
            deliveryInfos.nbContractsForSiteTransfer > 1)
        {
            this._tabNotifications['transferRecoveryYetRealized'].displayType = 'show';
        }

        // Notification de réalisation de livraison impossible
        if (transportObj.isSideDoable('d') &&
            (deliveryInfos.isSelf && this.hasRight('transport', 'do-self') ||
                    !deliveryInfos.isSelf && this.hasRight('transport', 'do-std')))
        {
            // RE de location en attente de validation
            if (rentTariffObj.getSpecialReductionStatus() == '' &&
                rentTariffObj.getSpecialReductionAmount() > 0)
            {
                this._tabNotifications['deliveryNotDoable'].displayType = 'show';
                this._showNotificationSubMessage('deliveryNotDoable', 'rentExcepReduction');
            }
            // RE de transport en attente de validation
            if (transportObj.getSpecialReductionStatus() == TRSPTARIFF_REDUCTIONSTATE_PENDING)
            {
                this._tabNotifications['deliveryNotDoable'].displayType = 'show';
                this._showNotificationSubMessage('deliveryNotDoable', 'transportExcepReduction');
            }
            // Action pro forma en cours
            if (proformaObj.getAction() != null)
            {
                this._tabNotifications['deliveryNotDoable'].displayType = 'show';
                this._showNotificationSubMessage('deliveryNotDoable', 'proformaEdition');
            }
            // Changement de modèle de machines
            if (this._hasChangedComponents.model)
            {
                this._tabNotifications['deliveryNotDoable'].displayType = 'show';
                this._showNotificationSubMessage('deliveryNotDoable', 'modelEdition');
            }
            // S'il y a une pro forma active, modification sur la durée, le prix de location, ou les autres services
            if (proformaObj.getActiveCount({'isExpired' : false}) > 0 &&
                (this._hasChangedComponents.durationObj || this._hasChangedComponents.rentTariffAmount ||
                    this._hasChangedComponents.servicesObj || this._hasChangedComponents.transportTariffAmount))
            {
                this._tabNotifications['deliveryNotDoable'].displayType = 'show';
                this._showNotificationSubMessage('deliveryNotDoable', 'expiredProforma');
            }
        }

        // Pas de transfert inter-chantiers ou de reprise sur chantier
        if (transportObj.isSideEditable('r') &&
            (this.hasRight('transport', 'self') || this.hasRight('transport', 'transfer')) &&
            !recoveryInfos.contract && recoveryInfos.nbContractsForSiteTransfer > 0)
        {
            // s'il n'y a pas de machine attribuée
            if (!this.isFinalAllocation())
            {
                this._tabNotifications['recoveryTransferUnavailable'].displayType = 'show';
                this._showNotificationSubMessage('recoveryTransferUnavailable', 'notFinalAllocation');
            }
        }

        // Notification de réalisation de récupération impossible
        if (transportObj.isSideDoable('r') &&
            (recoveryInfos.isSelf && this.hasRight('transport', 'do-self') ||
                    !recoveryInfos.isSelf && this.hasRight('transport', 'do-std')))
        {
            // RE de location en attente de validation
            if (rentTariffObj.getSpecialReductionStatus() == '' &&
                rentTariffObj.getSpecialReductionAmount() > 0)
            {
                this._tabNotifications['recoveryNotDoable'].displayType = 'show';
                this._showNotificationSubMessage('recoveryNotDoable', 'rentExcepReduction');
            }
            // RE de transport en attente de validation
            if (transportObj.getSpecialReductionStatus() == TRSPTARIFF_REDUCTIONSTATE_PENDING)
            {
                this._tabNotifications['recoveryNotDoable'].displayType = 'show';
                this._showNotificationSubMessage('recoveryNotDoable', 'transportExcepReduction');
            }
            // Action pro forma en cours
            if (proformaObj.getAction() != null)
            {
                this._tabNotifications['recoveryNotDoable'].displayType = 'show';
                this._showNotificationSubMessage('recoveryNotDoable', 'proformaEdition');
            }
            // S'il y a une pro forma active, modification sur la durée, le prix de location, ou les autres services
            if (proformaObj.getActiveCount({'isExpired' : false}) > 0 &&
                    (this._hasChangedComponents.durationObj || this._hasChangedComponents.rentTariffAmount ||
                            this._hasChangedComponents.servicesObj || this._hasChangedComponents.transportTariffAmount))
            {
                this._tabNotifications['recoveryNotDoable'].displayType = 'show';
                this._showNotificationSubMessage('recoveryNotDoable', 'expiredProforma');
            }
        }

        // Notification d'annulation de livraison impossible
        if ((deliveryInfos.isSelf && this.hasRight('transport', 'undo-self') ||
                !deliveryInfos.isSelf && this.hasRight('transport', 'undo-std')) &&
                !deliveryAction && !recoveryAction)
        {
            // suite à la facturation du transport aller
            if (in_array('invoicedDelivery', deliveryInfos.tabErrors))
            {
                this._tabNotifications['deliveryNotUndoable-invoicedDelivery'].displayType = 'show';
            }

            // il existe un transfert inter-chantiers sur la récupération
            if (in_array('recoveryTransfer', deliveryInfos.tabErrors))
            {
                var link = '<a href="' + oldRecoveryInfos.contract.url + '" target="_blank">' + oldRecoveryInfos.contract.code + '</a>';

                this._tabNotifications['deliveryNotUndoable-recoveryTransfer'].vars = {
                        parkNumber: $ge('rentContractParkNumber').outerHTML.replace(' id="rentContractParkNumber"', ''),
                        contractCode : link
                };
                this._tabNotifications['deliveryNotUndoable-recoveryTransfer'].displayType = 'show';
            }
        }

        // Notification d'annulation de récupération impossible
        if ((recoveryInfos.isSelf && this.hasRight('transport', 'undo-self') ||
                !recoveryInfos.isSelf && this.hasRight('transport', 'undo-std')) &&
                !recoveryAction)
        {
            // suite au transfert de la machine sur une autre agence
            if (in_array('transferredMachine', recoveryInfos.tabErrors))
            {
                this._tabNotifications['recoveryNotUndoable-transferredMachine'].displayType = 'show';
            }

            // car la machine est mise en visibilité
            if (in_array('visibleMachine', recoveryInfos.tabErrors))
            {
                this._tabNotifications['recoveryNotUndoable-visibleMachine'].vars = {
                        parkNumber: $ge('rentContractParkNumber').outerHTML.replace(' id="rentContractParkNumber"', ''),
                        agencyId : this._machine.visibleOnAgencyId
                };
                this._tabNotifications['recoveryNotUndoable-visibleMachine'].displayType = 'show';
            }
        }

        // Alerte pour renvoi de contrat si amiante
        if (this._siteObj.isAsbestos() &&
            this._actionsDates.asbestos &&
            (!this._actionsDates.lastPrint || this._actionsDates.asbestos > this._actionsDates.lastPrint))
        {
            this._tabNotifications['isAsbestos-sendContract'].displayType = 'show';
        }

        // Affichage ou masquage des notifications
        for (var id in this._tabNotifications)
        {
            var displayType = 'hide';
            var tabOptions = {};

            // Type d'affichage
            if (this._tabNotifications[id].displayType)
            {
                displayType = this._tabNotifications[id].displayType;
            }

            // Options d'affichage
            if (typeof this._tabNotifications[id].options == 'object')
            {
                var nbOptions = this._tabNotifications[id].options.length;
                for (var i = 0; i < nbOptions; i++)
                {
                    tabOptions[this._tabNotifications[id].options[i]] = true;
                }
            }

            // Variables de message
            if (typeof this._tabNotifications[id].vars == 'object')
            {
                tabOptions['messageVars'] = this._tabNotifications[id].vars;
            }

            // Liste des messages à afficher
            if (typeof this._tabNotifications[id].messages == 'object')
            {
                tabOptions['messagesId'] = this._tabNotifications[id].messages;
            }

            // Liste des boutons à afficher
            if (typeof this._tabNotifications[id].buttons == 'object')
            {
                tabOptions['buttonsId'] = this._tabNotifications[id].buttons;
            }

            Location.viewObj.displayNotification(id, displayType, tabOptions);
        }
    },

    /**
     * Mise à jour de l'affichage du groupe d'onglets
     *
     */
    _displayTabBoxes : function()
    {
        var editMode = this.getEditMode();

        // Mise à jour des informations dans les onglets
        if (this._loadStatus != 'loading')
        {
            // Activation/Désactivation des onglets
            // ************************************
            var tabBoxObj = Location.tabBoxesManager.getTabBox('rentContractTabBox');

            // Onglets à activer
            var tabTabsInfos = this._getTabsInfos();

            for (var i = 0; i < tabTabsInfos.length; i++)
            {
                var tabObj = tabBoxObj.getTab(i);
                if (tabObj)
                {
                    tabObj.setRentContractState(tabTabsInfos[i][1]);
                    tabObj.setDisabled(!tabTabsInfos[i][0]);
                    // Badge
                    if (tabTabsInfos[i][2].badge)
                    {
                        tabObj.setBadge(tabTabsInfos[i][2].badge);
                    }
                }
            }

            // Mise à jour des informations des différents onglets
            // ***************************************************
            switch (tabBoxObj.getActiveTabIndex())
            {
                // - Onglet "Matériel"
                case TABINDEX_EQUIPMENT:
                    var searchBoxObj = Location.searchBoxesManager.getSearchBox('rentContractModelId');
                    if (this._modelLoadStatus != 'loading')
                    {
                        var modelId = this.getModelId();
                        searchBoxObj.displayNullItem((modelId == 0), 0, '---');
                        searchBoxObj.setValue(modelId, false);
                        searchBoxObj.setDisabled(!this.isModelChangeable());
                        searchBoxObj.setRequired(tabTabsInfos[TABINDEX_EQUIPMENT][2].noModel);

                        // Affichage de la machine souhaitée
                        var obj = Location.searchBoxesManager.getSearchBox('rentContractWishedMachineId');
                        if (obj && !this._machine.parkNumber)
                        {
                            if (this._isWishedMachineUpdDsp)
                            {
                                obj.clear();
                                obj.displayNullItem(true, 0, '---');
                                obj.setElements(this._model.tabWishedMachines, 1);
                                this._isWishedMachineUpdDsp = false;
                            }
                            var v = this.getWishedMachineId();
                            obj.setValue((v ? v : 0), false);
                        }

                        // Affichage des familles
                        $ge('rentContractMachinesFamily').innerHTML = this.getMachinesFamilyLabel();
                        $ge('rentContractTariffFamily').innerHTML   = this.getTariffFamilyLabel();

                        // Affichage des taux d'utilisation
                        var useRateTableEl = $ge('rentContractModelUseRateTable');
                        useRateTableEl = useRateTableEl.tBodies[0];
                        var row, col, rate;
                        for (row = 0; row < 3; row++)
                        {
                            for (col = 0; col < 4; col++)
                            {
                                if (col % 2 == 0)
                                {
                                    rate = this._model.tabModelUseRates[col / 2][row][0];
                                    if (col == 0)
                                    {
                                        useRateTableEl.rows[row].style.display = (rate == null ? 'none' : '');
                                    }
                                    useRateTableEl.rows[row].cells[col + 1].innerHTML = LOC_View.getNumberFormat(rate * 100, 2) + ' %';
                                }
                                else
                                {
                                    var nbTotal = this._model.tabModelUseRates[(col - 1) / 2][row][1];
                                    useRateTableEl.rows[row].cells[col + 1].innerHTML = '(' + nbTotal + ')';
                                }
                            }
                        }
                    }
                    else
                    {
                        searchBoxObj.setDisabled(true);
                    }

                    // Affichage de l'état de la ligne
                    $ge('rentContractModelUseRateBlock').className = this._modelLoadStatus;

                    var obj = Location.searchBoxesManager.getSearchBox('rentContractWishedMachineId');
                    if (obj)
                    {
                        obj.setDisabled((this._modelLoadStatus == 'loading' || modelId == 0));
                    }
                    var obj = $ge('rentContractIsImperativeModel');
                    obj.checked = this.isImperativeModel();
                    obj.disabled = (modelId == 0 || !this.hasRight('machine', 'isImperativeModel'));

                    var obj = $('rentContractIsJoinDoc');
                    obj.checked = this.isJoinDoc();
                    obj.disabled = (modelId == 0 || !this.hasRight('machine', 'isJoinDoc'));

                    break;

                // - Onglet "Durée"
                case TABINDEX_DURATION:
                    Location.Rent_Helper_DurationView.display('durationHlp_', this._durationObj);
                    break;

                // - Onglet "Location"
                case TABINDEX_RENT:
                    Location.Rent_Helper_RentTariffView.display('rentTariffHlp_', this._rentTariffObj, tabTabsInfos[TABINDEX_RENT][2], false);

                    break;

                // - Onglet "Transport"
                case TABINDEX_TRANSPORT:
                    Location.Rent_Helper_TransportView.display('transportHlp_', this._transportObj, tabTabsInfos[TABINDEX_TRANSPORT][2]);

                    break;

                // - Onglet "Remise en état"
                case TABINDEX_REPAIR:
                    Location.Rent_Helper_RepairView.display('repairHlp_', this._repairObj, tabTabsInfos[TABINDEX_REPAIR][2]);

                // - Onglet "Autres services"
                case TABINDEX_SERVICES:
                    Location.Rent_Helper_ServicesView.display('servicesHlp_', this._servicesObj, tabTabsInfos[TABINDEX_SERVICES][2]);

                    break;

                // - Onglet "Pro forma"
                case TABINDEX_PROFORMA:
                    Location.Rent_Helper_ProformaView.display('proformaHlp_', this._proformaObj);
                    break;

                // - Onglet "Documents"
                case TABINDEX_DOCUMENTS:
                    Location.Rent_Helper_DocumentsView.display('documentsHlp_', this._documentsObj);

                // - Onglet "Historique"
                case TABINDEX_HISTORY:
                    Location.Rent_Helper_HistoryView.display('historyHlp_', this._historyObj);
                    break;
            }
        }
    },

    _displayTotals : function()
    {
        var currencyObj;
        var value, total = 0;
        for (var propName in this._tabTotals)
        {
            var trEl = $ge('rentContractTotal_' + propName);
            if (trEl)
            {
                var value = this._tabTotals[propName]() * 1;
                var className = 'line ' + propName;
                className = 'line ' + propName;
                if (propName == 'rentTotal')
                {
                    className += ' ' + this._rentTariffObj.getSpecialReductionStatus();
                }
                else if (propName == 'transportDeliveryAmount' || propName == 'transportRecoveryAmount')
                {
                    className += ' ' + this._transportObj.getSpecialReductionStatus();
                }
                if (this.getEditMode() == 'update')
                {
                    if (value != this._tabInitTotals[propName])
                    {
                        className += ' edited';
                    }
                    currencyObj = Location.currenciesManager.getCurrency('rentContractInitTotal[' + propName + ']');
                    if (currencyObj)
                    {
                        currencyObj.setValue(this._tabInitTotals[propName], false);
                    }
                }
                currencyObj = Location.currenciesManager.getCurrency('rentContractTotal[' + propName + ']');
                if (currencyObj)
                {
                    currencyObj.setValue(value, false);
                }
                total += value;
                trEl.className = className;
            }
        }
        // Participation au recyclage
        $ge('rentContractTotal_residuesAmount').style.display = (this._servicesObj.isResiduesActived() ? '' : 'none');

        // Total global
        currencyObj = Location.currenciesManager.getCurrency('rentContractTotal[total]');
        if (currencyObj)
        {
            currencyObj.setValue(total, false);
        }
    },

    /**
     * Affichage du message d'erreur ou de succès de l'enregistrement du devis
     *
     * @param string url
     */
    _displayValidateMsg : function(url, subAction)
    {
        var nbErrors = this._tabErrors.length;

        $ge('errorBlock').style.display = 'none';
        $ge('validBlock').style.display = 'none';
        if (nbErrors > 0)
        {
            // Masquage des erreurs
            var tabLiObj = $ge('errors').getElementsByTagName('li');
            var i, nb;
            nb = tabLiObj.length;
            for (i = 0; i < nb; i++)
            {
                tabLiObj[i].style.display = 'none';
            }
            // Affichage des erreurs du contrat
            for (i = 0; i < nbErrors; i++)
            {
                // Affichage du message correspondant à l'erreur
                var errorObj = $ge('error.' + this._tabErrors[i]);
                if (errorObj)
                {
                    errorObj.style.display = '';
                }
            }

            // Affichage des détails des erreurs ?
            var nbDetails = this._tabErrorsDetails.length;
            for (i = 0; i < nbDetails; i++)
            {
                var errorDetailsObj = $ge('error.' + this._tabErrorsDetails[i].module + '.details');
                if (errorDetailsObj)
                {
                    errorDetailsObj.style.display = '';
                    if (this._tabErrorsDetails[i].module == 'days')
                    {
                        var divEl = errorDetailsObj.querySelector('div');
                        if (divEl)
                        {
                            var tabInfos = this._tabErrorsDetails[i].tabInfos;
                            var text = LOC_View.getTranslation('days.details');
                            text = text.replace(/<%type>/, tabInfos.type ? LOC_View.getTranslation('days.type_' + tabInfos.type) : '');
                            text = text.replace(/<%date>/, tabInfos.date ? LOC_View.getDateFormat(tabInfos.date) : LOC_View.getTranslation('days.noDate'));
                            text = text.replace(/<%appliedToContract>/, (tabInfos.appliedToContractId ?
                                                    LOC_View.getTranslation('days.appliedToContract').replace(/<%appliedToContractId>/, tabInfos.appliedToContractId) : ''));
                            divEl.innerHTML = text;
                        }
                    }
                }
            }

            $ge('errorBlock').style.display = 'block';
            $ge('errorBlockBtn').focus();
        }
        else
        {
            subAction = (subAction == '' ? 'valid' : subAction);

            // Masquage des messages
            var tabSpanObj = $ge('success').getElementsByTagName('span');
            var i, nb;
            nb = tabSpanObj.length;
            for (i = 0; i < nb; i++)
            {
                tabSpanObj[i].style.display = 'none';
            }
            $ge('success.' + subAction).style.display = 'block';


            var aEl = $ge('validBlockBtn');
            aEl.style.visibility = 'visible';
            if (url != '')
            {
                aEl.onclick = function()
                {
                    this.style.visibility = 'hidden';
                    return true;
                }
                aEl.href = url;
            }
            else
            {
                aEl.href = '#';
                aEl.onclick = function()
                {
                    $ge('validBlock').style.display = 'none';
                    return false;
                }
            }

            // Affichage des warnings
            var nbWarnings = this._tabWarnings.length;
            var tabLiObj = $ge('warnings').getElementsByTagName('li');
            var i, nb;
            nb = tabLiObj.length;
            for (i = 0; i < nb; i++)
            {
                tabLiObj[i].style.display = 'none';
            }
            var tabDocumentsActions = this._documentsObj.getActions();
            for (i = 0; i < nbWarnings; i++)
            {
                var errorObj = $ge('warning.' + this._tabWarnings[i].module + '.' + this._tabWarnings[i].code);
                if (errorObj)
                {
                    errorObj.style.display = '';
                    if (this._tabWarnings[i].module == 'documents')
                    {
                        var tabDocs = [];
                        for (var j = 0; j < this._tabWarnings[i].indexes.length; j++)
                        {
                            var index = this._tabWarnings[i].indexes[j];
                            var action = tabDocumentsActions[index];
                            if (action)
                            {
                                switch (action.type)
                                {
                                    case 'add':
                                        tabDocs.push({
                                            title: action.data.title,
                                            fileName: action.data.fileName
                                        });
                                        break;
                                    case 'edit':
                                    case 'remove':
                                        var item = this._documentsObj.list.getById(action.id);
                                        if (item)
                                        {
                                            tabDocs.push({
                                                title: item.title,
                                                fileName: item.fileName
                                            });
                                        }
                                        break;
                                }
                            }
                        }

                        var ulEl = errorObj.querySelector('div.details');
                        if (ulEl)
                        {
                            ulEl.innerHTML = tabDocs.map(function(doc) {
                                return (doc.title != '' ? doc.title + ' (' + doc.fileName + ')' : doc.fileName);
                            }).join(', ');
                        }
                    }

                }
            }

            LOC_Common.removeEltClass('validBlock', 'warning');
            if (this._tabWarnings.length > 0)
            {
                LOC_Common.addEltClass('validBlock', 'warning');
            }

            $ge('validBlock').style.display = 'block';
            $ge('validBlockBtn').focus();
        }
    },

    /**
     * Vérifier le droit à un champ
     *
     * @param string partName  Nom de la partie
     * @param string fieldName Nom du champ
     * @return bool
     */
    hasRight : function(partName, fieldName)
    {
        if (this._tabRights === null)
        {
            return true;
        }

        // Pas de fieldName : on vérifie qu'au moins un des éléments de partName soit autorisé
        if (typeof fieldName == 'undefined' && this._tabRights[partName])
        {
            var result = false;
            for (var field in this._tabRights[partName])
            {
                result = result || this._tabRights[partName][field];
            }
            return result;
        }

        return (this._tabRights[partName] && this._tabRights[partName][fieldName]);
    },

    /**
     * Initialisation
     *
     */
    _init : function()
    {
        // Initialisation des notifications
        this._initNotifications();

        var mySelf = this;
        var obj;

        // ************************************************************
        // Ajout des événements sur toutes les entrées de l'interface
        // ************************************************************

        Location.searchBoxesManager.getSearchBox('rentContractModelId').onchange = function()
        {
            mySelf.setModelId(this.getValue());
        }
        // - Modification de la machine souhaitée
        obj = Location.searchBoxesManager.getSearchBox('rentContractWishedMachineId');
        if (obj)
        {
            obj.onchange = function()
            {
                mySelf.setWishedMachineId(this.getValue());
            }
        }


        $ge('rentContractClearWishedMachineBtn').onclick = function()
        {
            mySelf._isWishedMachineUpdDsp = true;
            mySelf.setWishedMachineId(0);
        }

        // - Modèle impératif ?
        obj = $ge('rentContractIsImperativeModel');
        obj.onchange = function()
        {
            mySelf.setImperativeModel(this.checked);
            this.checked = mySelf.isImperativeModel();
        }
        obj.onclick = obj.onchange;

        // - Fournir les documents machine ?
        obj = $('rentContractIsJoinDoc');
        obj.onchange = function()
        {
            mySelf.setJoinDoc(this.checked);
            this.checked = mySelf.isJoinDoc();
        }
        obj.onclick = obj.onchange;

        // - Contrat FullService ?
        obj = $ge('rentContractIsFullService');
        obj.onchange = function()
        {
            mySelf.setFullService(this.checked);
            this.checked = mySelf.isFullService();
        }
        obj.onclick = obj.onchange;
        // - Négociateur
        Location.searchBoxesManager.getSearchBox('rentContractNegotiatorId').onchange = function()
        {
            mySelf.setNegotiatorId(this.getValue());
        }
        $ge('rentContractClearNegotiatorBtn').onclick = function()
        {
            mySelf.setNegotiatorId(0);
        }

        // Client
        this._customerObj = new Location.Rent_Helper_Customer(this, this._tabCfgs);
        this._customerObj.onchange = function(tabChanges)
        {
            if (mySelf.getEditMode() == 'create' && in_array('id', tabChanges))
            {
                // Mise à jour du nettoyage et de la participation au recyclage
                mySelf._servicesObj.setCleaningTypeId((this.isCleaningInvoiced() ? 'std' : 0), true);
                mySelf._isJoinDoc = this.isJoinDoc();
            }
            mySelf.setAllExternalParams();
            mySelf.display();

            if (this.getId() && mySelf.getEditMode() == 'create')
            {
                // Modèle de machine pour récupérer les nouveaux tarifs de loc
                mySelf.setModelId(mySelf.getModelId(), true);
                if (this.isProformaRequired())
                {
                    mySelf._rentTariffObj.setAmountType('day');
                }
            }
        }
        Location.Rent_Helper_CustomerView.initEvents('customerHlp_', this._customerObj);

        // Chantier
        this._siteObj = new Location.Rent_Helper_Site(this, this._tabCfgs);
        this._siteObj.onchange = function(tabChanges)
        {
            mySelf.setAllExternalParams();
            mySelf.display();
        }
        Location.Rent_Helper_SiteView.initEvents('rentSite', this._siteObj);

        // Onglet "Durée"
        this._durationObj = new Location.Rent_Helper_Duration(this, this._tabCfgs);
        this._durationObj.onchange = function(tabChanges)
        {
            var tabChangesCompare = ['additionalDays', 'deductedDays',
                                     'beginDate', 'endDate',
                                     'realDuration', 'calendarDuration'];

            if ((array_intersect(tabChanges, tabChangesCompare)).length > 0)
            {
                mySelf._hasChangedComponents.durationObj = true;
            }
            mySelf.setAllExternalParams();
            mySelf.display();
        }
        Location.Rent_Helper_DurationView.initEvents('durationHlp_', this._durationObj);

        // Onglet "Location"
        this._rentTariffObj = new Location.Rent_Helper_RentTariff(this, this._tabCfgs);
        this._rentTariffObj.onchange = function(tabChanges)
        {
            var tabChangesCompare = ['reduction', 'tariff'];

            if ((array_intersect(tabChanges, tabChangesCompare)).length > 0)
            {
                mySelf._hasChangedComponents.rentTariffAmount = true;
            }
            mySelf.setAllExternalParams();
            mySelf.display();
        }
        Location.Rent_Helper_RentTariffView.initEvents('rentTariffHlp_', this._rentTariffObj);

        // Onglet "Transport"
        this._transportObj = new Location.Rent_Helper_Transport(this, this._tabCfgs);
        this._transportObj.onchange = function(tabChanges)
        {
            if (in_array('estimation', tabChanges))
            {
                // Si l'estimation tranport change alors on désactive l'impression du contrat
                mySelf.setPrintable(false);
            }
            // modification du montant d'un transport
            var tabChangesCompare = ['delivery.amount', 'recovery.amount'];
            if ((array_intersect(tabChanges, tabChangesCompare)).length > 0)
            {
                mySelf._hasChangedComponents.transportTariffAmount = true;
            }
            // modification de la synchronisation de la livraison avec OTD
            var tabChangesCompare = ['delivery.isSynchroDisabled'];
            mySelf._hasChangedComponents.deliveryIsSynchroDisabled =
                    (array_intersect(tabChanges, tabChangesCompare).length > 0);
            // modification de la synchronisation de la récupération avec OTD
            var tabChangesCompare = ['recovery.isSynchroDisabled'];
            mySelf._hasChangedComponents.recoveryIsSynchroDisabled =
                    (array_intersect(tabChanges, tabChangesCompare).length > 0);
            mySelf.setAllExternalParams();
            mySelf.display();
        }
        Location.Rent_Helper_TransportView.initEvents('transportHlp_', this._transportObj);

        // Remise en état
        this._repairObj = new Location.Rent_Helper_Repair(this, this._tabCfgs);
        this._repairObj.onchange = function(tabChanges)
        {
            mySelf.setAllExternalParams();
            mySelf.display();
        }
        Location.Rent_Helper_RepairView.initEvents('repairHlp_', this._repairObj);

        // Onglet "Autres services"
        this._servicesObj = new Location.Rent_Helper_Services(this, this._tabCfgs);
        this._servicesObj.onchange = function(tabChanges)
        {
            var tabChangesCompare = ['insuranceRate', 'cleaningAmount',
                                     'isResiduesInvoiced', 'editRentService',
                                     'addRentService', 'removeRentService'];

            if ((array_intersect(tabChanges, tabChangesCompare)).length > 0)
            {
                mySelf._hasChangedComponents.servicesObj = true;
            }
            if (mySelf.getEditMode() == 'create' && array_intersect(tabChanges, ['extParam_customerInsurance', 'extParam_customerAppeal']).length > 0)
            {
                this.resetInsurance();
                this.resetAppeal();
            }
            if ((mySelf.getEditMode() == 'create' && in_array('extParam_isCustomerResiduesInvoiced', tabChanges)) ||
                (mySelf._tabCfgs.useResiduesForAccessories == 0 && in_array('extParam_isModelAccessory', tabChanges)))
            {
                this.resetResidues();
            }
            mySelf.setAllExternalParams();
            mySelf.display();
        }
        Location.Rent_Helper_ServicesView.initEvents('servicesHlp_', this._servicesObj);

        // Onglet "Pro forma"
        this._proformaObj = new Location.Rent_Helper_Proforma(this, this._tabCfgs);
        this._proformaObj.onchange = function()
        {
            mySelf.setAllExternalParams();
            mySelf.display();
        }
        Location.Rent_Helper_ProformaView.initEvents('proformaHlp_', this._proformaObj);

        // Onglet "Documents"
        this._documentsObj = new Location.Rent_Helper_Documents(this, this._tabCfgs);
        this._documentsObj.onchange = function()
        {
            mySelf.setAllExternalParams();
            mySelf.display();
        }
        Location.Rent_Helper_DocumentsView.initEvents('documentsHlp_', this._documentsObj);

        // Onglet "Historique"
        this._historyObj = new Location.Rent_Helper_History(this, this._tabCfgs);
        this._historyObj.onchange = function()
        {
            mySelf.setAllExternalParams();
            mySelf.display();
        }
        Location.Rent_Helper_HistoryView.initEvents('historyHlp_', this._historyObj);


        // Initialisation du gestionnaire d'upload des documents
        Location.Rent_Helper_Documents_AddList.uploadManager.init(this._tabCfgs);

        // - Modification des commentaires
        obj = $ge('rentContractComments');
        obj.onchange = function()
        {
            mySelf.setComments(this.value);
            this.value = mySelf.getComments();
        }
        // Firefox
        obj.oninput = function()
        {
            if (this.value.length > this.getAttribute('data-maxlength'))
            {
                alert(LOC_View.getTranslation('commentCapacityOver'));
                mySelf.setComments(this.value.substr(0, this.getAttribute('data-maxlength')));
                this.value = mySelf.getComments();
            }
            return false;
        }
        // Internet Explorer
        obj.onpaste = function()
        {
            if (window.clipboardData)
            {
                var newText = this.value + window.clipboardData.getData("Text");
                if (newText.length > this.getAttribute('data-maxlength'))
                {
                    alert(LOC_View.getTranslation('commentCapacityOver'));
                    mySelf.setComments(newText.substr(0, this.getAttribute('data-maxlength')));
                    this.value = mySelf.getComments();
                    return false;
                }
            }
        }
        if (LOC_ClientInfo.isMSIE)
        {
            obj.onkeydown = function()
            {
                if (!LOC_View.limitTextArea(event, this.getAttribute('data-maxlength')))
                {
                    alert(LOC_View.getTranslation('commentCapacityOver'));
                    return false;
                }
            }
        }

        // - Modification du suivi
        obj = $ge('rentContractTracking');
        obj.onchange = function()
        {
            mySelf.setTracking(this.value);
            this.value = mySelf.getTracking();
        }
        // Firefox
        obj.oninput = function()
        {
            if (this.value.length > this.getAttribute('data-maxlength'))
            {
                alert(LOC_View.getTranslation('trackingCapacityOver'));
                mySelf.setTracking(this.value.substr(0, this.getAttribute('data-maxlength')));
                this.value = mySelf.getTracking();
            }
            return false;
        }
        obj.onpaste = function()
        {
            if (window.clipboardData)
            {
                var newText = this.value + window.clipboardData.getData("Text");
                if (newText.length > this.getAttribute('data-maxlength'))
                {
                    alert(LOC_View.getTranslation('trackingCapacityOver'));
                    mySelf.setTracking(newText.substr(0, this.getAttribute('data-maxlength')));
                    this.value = mySelf.getTracking();
                    return false;
                }
            }
        }
        if (LOC_ClientInfo.isMSIE)
        {
            obj.onkeydown = function()
            {
                if (!LOC_View.limitTextArea(event, this.getAttribute('data-maxlength')))
                {
                    alert(LOC_View.getTranslation('trackingCapacityOver'));
                    return false;
                }
            }
        }

        // - Onglet activé
        Location.tabBoxesManager.getTabBox('rentContractTabBox').onaftertabchange = function(index)
        {
            mySelf.display();
        }

        obj = $ge('rentContractValidBtn');
        if (obj)
        {
            obj.onclick = function()
            {
                mySelf.validate();
            }
        }
        obj = $ge('rentContractResetBtn');
        if (obj)
        {
            obj.onclick = function()
            {
                mySelf.reset();
            }
        }
        obj = $ge('rentContractStopBtn');
        if (obj)
        {
            obj.onclick = function()
            {
                mySelf.validate('stop');
            }
        }
        obj = $ge('rentContractReactivateBtn');
        if (obj)
        {
            obj.onclick = function()
            {
                mySelf.validate('reactivate');
            }
        }
        obj = $ge('rentContractInvoiceBtn');
        if (obj)
        {
            obj.onclick = function()
            {
                mySelf.validate('invoice');
            }
        }
        obj = $ge('rentContractRestopBtn');
        if (obj)
        {
            obj.onclick = function()
            {
                mySelf.validate('restop');
            }
        }
        obj = $ge('rentContractCancelBtn');
        if (obj)
        {
            obj.onclick = function()
            {
                mySelf.validate('cancel');
            }
        }

        // Boutons d'actualisation
        var refresh = function()
        {
            mySelf._loadStatus = 'loading';
            mySelf.display();

            mySelf.refresh();
        };

        obj = $ge('rentContractRefreshBtn');
        if (obj)
        {
            obj.onclick = refresh;
        }

        obj = $ge('rentContractErrorBlockRefreshBtn');
        if (obj)
        {
            obj.onclick = function()
            {
                $ge('errorBlock').style.display = 'none';
                refresh();
            }
        }

        // *******************************************
        // Chargement des données du contrat
        // *******************************************
        this._loadInfos(this._tabCfgs['tabInfos']);
        if (this.getEditMode() == 'create')
        {
            this._id = this._tempId;
        }
        this.display();

        // Onglet actif
        if (this._tabCfgs.tabIndex != null)
        {
            var tabBoxObj = Location.tabBoxesManager.getTabBox('rentContractTabBox');
            tabBoxObj.setActiveTabIndex(this._tabCfgs.tabIndex);
        }
    },

    _initNotifications : function()
    {
        this._tabNotifications = {
            'cantStop'       : {'displayType' : 'hide'},
            'cantReactivate' : {'displayType' : 'hide', 'buttons' : []},
            'cantInvoice'    : {'displayType' : 'hide', 'buttons' : []},
            'cantRestop'     : {'displayType' : 'hide'},

            'unlockCustomerRequest-send': {'displayType' : 'hide'},
            'unlockCustomerRequest-error': {'displayType': 'hide'},

            'restop-selfRecoveryWithAmount'                 : {'displayType' : 'hide'},

            'recoveryTransfer-noExtensionProforma'          : {'displayType' : 'hide'},
            'extensionProforma-noRecoveryTransfer'          : {'displayType' : 'hide'},
            'amountImpactOnDeliveryLinkedContract'          : {'displayType' : 'hide', 'options' : ['forceFirst']},
            'amountImpactOnRecoveryLinkedContract'          : {'displayType' : 'hide', 'options' : ['forceFirst']},
            'deliverySelfSiteTransferNotEditable'           : {'displayType' : 'hide'},
            'recoverySelfSiteTransferNotEditable'           : {'displayType' : 'hide'},
            'deliveryNotDoable'                             : {'displayType' : 'hide'},
            'deliveryNotUndoable-invoicedDelivery'          : {'displayType' : 'hide'},
            'deliveryNotUndoable-recoveryTransfer'          : {'displayType' : 'hide'},
            'recoveryNotDoable'                             : {'displayType' : 'hide'},
            'recoveryNotUndoable-transferredMachine'        : {'displayType' : 'hide'},
            'recoveryNotUndoable-visibleMachine'            : {'displayType' : 'hide'},
            'deliveryNotEditable-doPending'                 : {'displayType' : 'hide'},
            'recoveryNotEditable-doPending'                 : {'displayType' : 'hide'},
            'deliveryNotEditable-undoPending'               : {'displayType' : 'hide'},
            'recoveryNotEditable-undoPending'               : {'displayType' : 'hide'},
            'transportNotEditable-doDeliveryPending'        : {'displayType' : 'hide'},
            'transportNotEditable-doRecoveryPending'        : {'displayType' : 'hide'},
            'deliveryTransferUnavailable'                   : {'displayType' : 'hide'},
            'recoveryTransferUnavailable'                   : {'displayType' : 'hide'},
            'transferRecoveryYetRealized'                   : {'displayType' : 'hide'},
            'synchroDisabled'                               : {'displayType' : 'hide'},
            'transferToAgency-yetVisible'                   : {'displayType' : 'hide'},
            'transferToAgency-unrentable'                   : {'displayType' : 'hide'},
            'transferToAgency-unrentableInVisibilityAgency' : {'displayType' : 'hide'},

            'pendingRepair'                                 : {'displayType' : 'hide'},

            'edit'                                          : {'displayType' : 'hide', 'options' : ['forceFirst']},
            'uneditableFields-unfinalizedProforma'          : {'displayType' : 'hide'},

            'isAsbestos-sendContract'                      : {'displayType' : 'hide'},
        };
    },

    _loadInfos : function (tabInfos)
    {
        this._tabErrors = [];

        // Gestion des droits
        this._tabRights = tabInfos.tabRights;

        this._id                      = Number(tabInfos.id);
        this._linkedEstimateLineId    = (tabInfos.linkedEstimateLine ? Number(tabInfos.linkedEstimateLine.id) : null);
        this._index                   = Number(tabInfos.index);
        this._code                    = (tabInfos.code || '');
        this._tracking                = (tabInfos.tracking || '');
        this._comments                = (tabInfos.comments || '');
        this._isFullService           = (tabInfos.isFullService == 1);
        this._negotiatorId            = (tabInfos['negotiator.id'] || null);
        this._negotiatorFullName      = (tabInfos['negotiator.fullName'] || '');
        this._modificationDate        = (tabInfos.modificationDate || '');
        this._stateId                 = tabInfos['state.id'];
        this._isPrintable             = (tabInfos.isPrintable == 1);
        this._isEndDateNear           = (tabInfos.isEndDateNear == 1);
        this._closureDate             = (tabInfos.closureDate == 1);
        this._possibilities           = tabInfos.possibilities;
        this._invoiceState            = tabInfos.invoiceState;
        this._lastInvoiceDate         = (tabInfos.lastInvoiceDate || '');
        this._proformaFlags           = tabInfos.proformaFlags;
        this._valueDate               = tabInfos.valueDate;
        this._actionsDates            = tabInfos.actionsDates;

        // Client
        this._customerObj.load(tabInfos.tabCustomer, {
            disabledComponents: {
                select: (this.getEditMode() != 'create')
            }
        }, this._tabRights.customer);

        // Chantier
        this._siteObj.load(tabInfos.tabSite, {}, this._tabRights.site);

        // Durée
        this._durationObj.load(tabInfos.tabDuration, {}, this._tabRights.duration);

        this._isImperativeModel = (tabInfos.isImperativeModel == 1);
        this._isJoinDoc         = (tabInfos.isJoinDoc == 1);

        // Matériel
        this._loadModelInfos(tabInfos.tabModelInfos);
        this._loadMachineInfos(tabInfos.tabMachineInfos);
        this._wishedMachineId = (tabInfos.tabWishedMachineInfos ? tabInfos.tabWishedMachineInfos.id : null);
        this._isFinalAllocation = tabInfos.isFinalAllocation;

        // Calculs Tarification Location
        this._rentTariffObj.load(tabInfos.tabRentTariff, this._durationObj.getDuration(), tabInfos.tabModelInfos.tabTariffs,
                                 {}, this._tabRights.rentTariff);

        // Calculs Tarification Transport
        this._transportObj.load(tabInfos.tabTransport, {}, this._tabRights.transport);

        // Remise en état
        this._repairObj.load(tabInfos.tabRepair, {}, this._tabRights.repair);

        // Autres services
        this._servicesObj.load(tabInfos.tabOtherServices, tabInfos.tabModelInfos.tabCleaningTypes,
                               tabInfos.tabRentServices, tabInfos.oldServicesAmount,
                               {
                                   isFullService : this.isFullService(),
                                   isModelAccessory: this._model.isAccessory,
                                   customerInsurance : (this._customerObj.getId() != 0 ? {
                                       typeId: this._customerObj.getInsuranceTypeId(),
                                       rate: this._customerObj.getInsuranceRate()
                                   } : null),
                                   customerAppeal : (this._customerObj.getId() != 0 ? {
                                       typeId: this._customerObj.getAppealTypeId()
                                   } : null),
                                   isCustomerResiduesInvoiced: (this._customerObj.getId() != 0 ? this._customerObj.isResiduesInvoiced() : null),
                                   residuesBaseAmount: this.getResiduesBaseAmount()
                               },
                               this._tabRights.services);

        // Pro forma
        this._proformaObj.load(tabInfos.tabProforma,
                               {fuelQtyEstimByDay: this._model.fuelQtyEstimByDay,
                                fuelQtyMax: this._model.fuelQtyMax},
                               this._tabRights.proforma);

        // Documents
        this._documentsObj.load(tabInfos.tabDocuments, {}, this._tabRights.documents);

        // Historique
        this._historyObj.load(tabInfos.tabHistory, {}, this._tabRights.history);


        // Calcul des paramètres externes pour tous les modules
        this.setAllExternalParams();

        // Totaux
        for (var propName in this._tabTotals)
        {
            this._tabInitTotals[propName] = this._tabTotals[propName]();
        }

        this._loadStatus = 'ok';

        // Evénement
        if (this.onload)
        {
            this.onload(this._id);
        }
    },

    _loadMachineInfos : function(tabMachineInfos)
    {
        this._machine.id = Number((tabMachineInfos ? tabMachineInfos.id : null));
        if (this._machine.id)
        {
            this._machine.stateId              = tabMachineInfos.stateId;
            this._machine.modelId              = tabMachineInfos.modelId;
            this._machine.modelLabel           = tabMachineInfos.modelLabel;
            this._machine.parkNumber           = tabMachineInfos.parkNumber;
            this._machine.agencyId             = tabMachineInfos.agencyId;
            this._machine.visibleOnAgencyId    = tabMachineInfos.visibleOnAgencyId;
            this._machine.visibleOnAgencyLabel = tabMachineInfos.visibleOnAgencyLabel;
            this._machine.visibilityMode       = tabMachineInfos.visibilityMode;
            this._machine.isRentableModel      = tabMachineInfos.isRentableModel;
            this._machine.contractId           = tabMachineInfos.contractId;
        }
    },

    _loadModelInfos : function(tabModelInfos)
    {
        this._model.id = Number((tabModelInfos ? (tabModelInfos.id || 0) : 0));
        if (this._model.id != 0)
        {
            this._model.label               = (tabModelInfos.label || '');
            this._model.machinesFamilyLabel = (tabModelInfos.machinesFamilyLabel || '');
            this._model.isFuelNeeded        = (tabModelInfos.isFuelNeeded == 1);
            this._model.tariffFamilyLabel   = (tabModelInfos.tariffFamilyLabel || '');
            this._model.tabModelUseRates    = tabModelInfos.tabUseRates;
            this._model.load                = Number(tabModelInfos.load);
            this._model.isCleaningPossible  = (tabModelInfos.isCleaningPossible == 1);
            this._model.tabWishedMachines   = (tabModelInfos.tabWishedMachines || []);
            this._model.fuelQtyEstimByDay   = tabModelInfos.fuelQtyEstimByDay;
            this._model.fuelQtyMax          = tabModelInfos.fuelQtyMax;
            this._model.tabTypes            = tabModelInfos.tabTypes;
            this._model.isRentable          = tabModelInfos.isRentable;
            this._model.isAccessory         = tabModelInfos.isAccessory;

            this._isWishedMachineUpdDsp = true;
        }
        this._modelLoadStatus = 'ok';
    }
}