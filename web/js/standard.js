if (!window.Location)
{
    var Location = new Object();
}

if (Location.viewObj)
{

/* Surcharge l'objet Location.viewObj */

/**
 * Afficher l'animation de chargement de la page
 *
 * @param bool isEnabled
 */
Location.viewObj.displayLoading = function(isEnabled)
{
    var middleElt = window.document.getElementById('locStandardMiddle');
    middleElt.className = (isEnabled ? 'loading' : '');
}

/**
 * Récupérer le titre de la page
 *
 * @return string Titre de la page
 */
Location.viewObj.getTitle = function()
{
    return window.document.getElementById('locStandardTitleText').innerHTML;
}

/**
 * Définir le titre de la page
 *
 * @param string title Titre de la page
 */
Location.viewObj.setTitle = function(title)
{
    window.document.getElementById('locStandardTitleText').innerHTML = title;
}

/**
 * Ajouter des notifications
 *
 * @param Object tabNotifs Liste des notifications
 */
Location.viewObj.addNotifications = function(tabNotifs)
{
    // Initialisation des notifications
    for (var i = 0; i < tabNotifs.length; i++)
    {
        var t = tabNotifs[i];

        var options = t.options || {};
        options.containerEl = 'locStandardNotifs';

        Location.notificationsManager.create(t.id, t.type, t.message, t.buttons, options, t.subMessages);
    }
}

/**
 * Afficher ou cacher une notification
 *
 * @param string notifId     Identifiant de la notification
 * @param string displayType Type d'affichage : 'show' pour afficher, 'hide' pour cacher, 'toggle' pour mettre l'inverse
 * @param Object tabOptions  Options supplémentaires
 * @return bool
 */
Location.viewObj.displayNotification = function(notifId, displayType, tabOptions)
{
    var notif = Location.notificationsManager.get(notifId);
    if (notif)
    {
        if (displayType == 'toggle')
        {
            displayType = (notif.isShown() ? 'hide' : 'show');
        }

        notif.ondisplaychange = function() {
            var evt = window.document.createEvent('Event');
            evt.initEvent('resize', true, true);
            window.dispatchEvent(evt);
        };

        if (displayType == 'show')
        {
            notif.show(tabOptions);
        }
        else if (displayType == 'hide')
        {
            notif.hide(tabOptions);
        }
        else
        {
            return false;
        }
        return true;
    }
    return false;
}


Location.viewObj.tabInitFuncs.push(function()
{
    var contentElt = window.document.getElementById('locStandardContent');
    var middleElt = window.document.getElementById('locStandardMiddle');

    var onScroll = function()
    {
        if (contentElt.scrollTop > 0)
        {
            Location.commonObj.addEltClass(middleElt, 'scroll-top');
        }
        else
        {
            Location.commonObj.removeEltClass(middleElt, 'scroll-top');
        }
        if (contentElt.scrollTop < (contentElt.scrollHeight - contentElt.clientHeight))
        {
            Location.commonObj.addEltClass(middleElt, 'scroll-btm');
        }
        else
        {
            Location.commonObj.removeEltClass(middleElt, 'scroll-btm');
        }
    };

    // Redimentionnement
    var setHeightFunc = function()
    {
        var footerElt = window.document.getElementById('locStandardFooter');
        var windowHeight = Location.commonObj.getWindowHeight();

        if (windowHeight > 0 && middleElt)
        {
            var newH = windowHeight - Location.commonObj.getTopPos(middleElt);
            if (footerElt)
            {
                newH -= Location.commonObj.getHeight(footerElt);
            }
            if (newH < 0)
            {
                newH = 0;
            }
            middleElt.style.height = newH + 'px';

            // Hauteur du contenu sans les marges et les paddings
            var contentHeight = newH;
            contentHeight -= parseInt(Location.commonObj.getStyle(contentElt, 'marginTop'));
            contentHeight -= parseInt(Location.commonObj.getStyle(contentElt, 'marginBottom'));
            contentHeight -= parseInt(Location.commonObj.getStyle(contentElt, 'paddingTop'));
            contentHeight -= parseInt(Location.commonObj.getStyle(contentElt, 'paddingBottom'));
            contentElt.style.height = contentHeight + 'px';
        }
        onScroll();
    }

    Location.commonObj.addEltEvent(contentElt, 'scroll', onScroll);
    Location.commonObj.addEltEvent(window, 'resize', setHeightFunc);
    window.setInterval(setHeightFunc, 1000);
    setHeightFunc();

    // Désactivation de la sélection sur les éléments suivant :
    Location.commonObj.cancelTextSelection('locStandardTitle');
    Location.commonObj.cancelTextSelection('locStandardFooter');
});

}

