/**
 * $Revision$
 * $Date$
 * $Author$
 */



function getPositionCursor(node)
{
  try {
    if (node.selectionStart >= 0) { return node.selectionStart; }
    else if (document.selection) {// IE
      var ntxt = node.value; // getting starting text
      var rng = document.selection.createRange();
      // Partie modifiée
      if (rng == null)
          return 0;
      var rnge = node.createTextRange();
      var rngc = rnge.duplicate();
      rnge.moveToBookmark(rng.getBookmark());
      rngc.setEndPoint('EndToStart', rnge);
      return rngc.text.length;
      // Fin de partie modifiée
    } return -1;
  } catch(e) { return false; }
}
function setPositionCursor(node, pos)
{
  try {
    if (node.selectionStart) {
      node.focus();
      node.setSelectionRange(pos,pos);
    }
    else if (node.createTextRange) { // IE
      var rng = node.createTextRange();
      rng.move('character', pos);
      rng.select();
    }
  } catch(e) { return false; }
}


/**
 * Traitement lorsque l'on fait un "coller" dans un champ téléphone
 */
function onPastePhone(e, chmp, phoneFormat, localPhoneFormat, oldValue)
{
    if (e && e.clipboardData && e.clipboardData.getData)
    {
        formatPhonePasteText(chmp, e.clipboardData.getData('text/plain'), phoneFormat, localPhoneFormat);
        return false;
    }
    if (window.clipboardData && window.clipboardData.getData)
    {
        // formate le numéro de téléphone
        formatPhonePasteText(chmp, window.clipboardData.getData('Text'), phoneFormat, localPhoneFormat);
        // return false pour garder le formatage dans le champ et pas le contenu du presse-papiers
        return false;
    }

//    formatPhonePasteText(chmp, e.target.value, phoneFormat, localPhoneFormat, oldValue);
//    return true;
    return false;
}


/**
 * Formate le contenu collé dans un champ téléphone
 * @param chmp
 * @param pasteText
 * @param phoneFormat
 * @param oldValue
 */
function formatPhonePasteText(chmp, pasteText, phoneFormat, localPhoneFormat, oldValue)
{
    var chmpName = chmp.name.substr(0, chmp.name.lastIndexOf('.'));

    // Position du curseur au moment où l'utilisateur fait le "coller"
    var startInput = getPositionCursor(chmp);

    // Masque de saisie
    var phoneMask = chmp.getAttribute('_format');

    // Construction du texte à coller
    var realPasteText = chmp.value.substr(0, startInput);

    // Position du curseur après le "coller"
    var currentPasteTextPos = startInput;

    if (phoneFormat == localPhoneFormat)
    {
        for (var i = startInput; i < phoneMask.length; i++)
        {
            var charToAdd = '';
            for (j = currentPasteTextPos; j < pasteText.length && charToAdd == ''; j++)
            {
                // Le caractère à coller est-il un chiffre ?
                var isPasteCharDigit = pasteText.charAt(j).match(/[0-9]/);

                // Le caractère à coller est un chiffre et le caractère du masque est un _ ou un chiffre
                // ou le caractère à coller est le même que le caractère du masque (espace, paranthèse, etc.)
                // => le caractère à coller est ajouté à la chaîne
                // => la prochaine analyse de la chaîne à coller se fera à partir du caractère suivant
                if ((phoneMask.charAt(i).match(/[0-9_]/) && isPasteCharDigit) || phoneMask.charAt(i) == pasteText.charAt(j))
                {
                    charToAdd = pasteText.charAt(j);
                    currentPasteTextPos = j + 1;
                }
                // Le caractère de masque n'est pas un _
                // ou le caractère à coller n'est pas un chiffre
                // => on ajoute le caractère du masque à la chaîne
                else if (!phoneMask.charAt(i).match(/[0-9_]/) || isPasteCharDigit)
                {
                    charToAdd = phoneMask.charAt(i);
                }
            }

            // Pas de caractère à ajouter => on ajouter le caractère de masque
            if (charToAdd == '')
            {
                charToAdd = phoneMask.charAt(i);
            }
            realPasteText += charToAdd;
        }
    }
    else
    {
        var textToPaste = pasteText.replace(/[^0-9]/g, '');
        realPasteText += textToPaste;
        currentPasteTextPos += textToPaste.length;
    }

    // Mise à jour de la valeur de la zone de texte
    chmp.value = realPasteText;

    // Mise à jour du champ caché
    updateMaskedInputHidden(chmpName, '_', function(value)
    {
        return updatePhoneHidden(value, chmpName);
    });

    // Replacement du curseur
    var afterPasteStartInput = realPasteText.substr(startInput).search(/_/);
    afterPasteStartInput = (afterPasteStartInput == -1 ? realPasteText.length : afterPasteStartInput + startInput);
    setTimeout(function() { setPositionCursor(chmp, afterPasteStartInput); }, 10);
}


function getPhonePrefix(name)
{
    var namePattern = getNamePattern(name);
    var codeObj = window.document.getElementById(namePattern.replace('%s', 'code'));
    if (codeObj)
    {
        return (codeObj.value == '' ? '' : '+' + codeObj.value + ' ');
    }
    return '';
}

// Fonction de vérification du préfixe international saisi.
// Si le code saisi correspond au code du pays de la locale,
// le masque de saisie apparaît dans la zone dédiée au numéro de téléphone,
// sinon celle-ci est vidée.
// Si le code est vidé, le code du pays de la locale réapparaît.
function checkIntlCode(name, localCode, phoneFormat)
{
    var namePattern = getNamePattern(name);

    var codeObj = window.document.getElementById(namePattern.replace('%s', 'code'));
    var phoneNoObj = window.document.getElementById(namePattern.replace('%s', 'masked'));

    if (codeObj && phoneNoObj)
    {
        var className = 'locStandardPhoneNumber';
        
        if (codeObj.value == '')
        {
            codeObj.value = localCode;
        }

        phoneNoObj.value = '';
        // Suppression du masque de saisie
        if (codeObj.value != localCode)
        {
            phoneNoObj.removeAttribute('maxlength');
            if (phoneNoObj._oldOnKeyUp)
            {
                phoneNoObj.onkeyup = phoneNoObj._oldOnKeyUp;
            }
            phoneNoObj.onkeydown = phoneNoObj._oldOnKeyDown;
            phoneNoObj._oldOnKeyDown = null;
        }
        // Rétablissement du masque de saisie
        else
        {
            phoneNoObj.onkeyup    = function()
                                    {
                                        updateMaskedInputHidden(name, '_', function(value)
                                                                           {
                                                                               return updatePhoneHidden(value, name);
                                                                           });
                                    };
            phoneNoObj.onkeydown  = null;
            phoneNoObj.onkeypress = null;
            phoneNoObj.onpaste = function(e)
            {
                return onPastePhone(e, phoneNoObj, codeObj.value, localCode, $ge(name).value);
            };
            MaskedInput({elm: phoneNoObj, format: phoneFormat, allowed: '0123456789', separator: '+() ', typeon: '_'});
        }
        
        codeObj.className = className;
        phoneNoObj.className = className;
    }
}

function updatePhoneHidden(value, name)
{
    return (value == '' ? '' : getPhonePrefix(name) + value);
}