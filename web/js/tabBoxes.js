if (!window.Location)
{
    var Location = new Object();
}

if (!Location.commonObj) alert('Erreur Location');


/* Classe Location.TabBox */
Location.TabBox = function(pluginObj, name, width, height)
{
    this.name = name;
    this._tabTabs = [];
    this._pluginObj = pluginObj;
    this._el = null;
    this._tabsContainerEl = null;

    this.ontabchange = null;
    this.onaftertabchange = null;

    // Initialisation
    this._init(width, height);
}

Location.TabBox.prototype = {

    /**
     * Fermer un onglet
     *
     * @param int index Index
     */
    closeTab : function(index)
    {
        if (this._tabTabs[index] && this._tabTabs[index].isClosable())
        {
            var tabObj = this._tabTabs[index];
            var isActive = tabObj.isActive();

            // Exécution de la fonction utilisateur
            if (tabObj.onclose)
            {
                var result = tabObj.onclose();
                if (typeof result != 'undefined' && !result)
                {
                    // Fermeture de l'onglet bloquée par l'utilisateur
                    return;
                }
            }

            // Suppression des éléments
            this._tabsContainerEl.removeChild(tabObj.getTitleElement());
            this._el.removeChild(tabObj._contentEl);
            delete(tabObj);

            // Reconstruction du tableau et affectation des nouveaux index
            var temp = [];
            var cpt = 0;
            for (var i = 0; i < this._tabTabs.length; i++)
            {
                if (index != i)
                {
                    temp[cpt] = this._tabTabs[i];
                    temp[cpt]._index = cpt;
                    cpt++;
                }
            }
            this._tabTabs = temp;

            // Si cet onglet était activé, alors on active le premier
            if (isActive)
            {
                this._activeNextTab(0);
            }
        }
    },

    /**
     * Récupérer l'index de l'onglet actif
     *
     * @return int Index
     */
    getActiveTabIndex : function()
    {
        for (var i = 0; i < this._tabTabs.length; i++)
        {
            if (this._tabTabs[i].isActive())
            {
                return i;
            }
        }
        return -1;
    },

    /**
     * Récupérer l'instance d'un onglet par rapport à son index
     *
     * @param int index
     * @return Location.TabBoxTab Instance d'un onglet
     */
    getTab : function(index)
    {
        if (this._tabTabs[index])
        {
            return this._tabTabs[index];
        }
        return null;
    },

    /**
     * Récupérer le nombre d'onglets
     *
     * @return int
     */
    getTabsCount : function()
    {
        return this._tabTabs.length;
    },

    /**
     * Activer l'onglet suivant
     *
     * @return int Index de l'onglet activé
     */
    nextTab : function()
    {
        var tabIndex = this.getActiveTabIndex();
        tabIndex = this._activeNextTab(tabIndex + 1);
        if (tabIndex == -1)
        {
            tabIndex = this._activeNextTab(0);
        }
        return tabIndex;
    },

    /**
     * Activer l'onglet précédent
     *
     * @return int Index de l'onglet activé
     */
    previousTab : function()
    {
        var tabIndex = this.getActiveTabIndex();
        tabIndex = this._activePreviousTab(tabIndex - 1);
        if (tabIndex == -1)
        {
            tabIndex = this._activePreviousTab(this._tabTabs.length - 1);
        }
        return tabIndex;
    },

    /**
     * Définir l'onglet actif par rapport à son index
     *
     * @param int index Index
     * @return bool
     */
    setActiveTabIndex : function(index)
    {
        if (this._tabTabs[index])
        {
            return this._tabTabs[index].active();
        }
        return false;
    },

    /**
     * Définir la hauteur des contenus des onglets
     *
     * @param int height Hauteur
     */
    setHeight : function(height)
    {
        var i, nb = this._tabTabs.length;
        for (i = 0; i < nb; i++)
        {
            this._tabTabs[i]._contentEl.style.height = height;
        }
    },

    /**
     * Définir la largeur du groupe d'onglets
     *
     * @param string width Largeur
     */
    setWidth : function(width)
    {
        this._el.style.width = width;
    },

    _activeNextTab : function(startIndex)
    {
        var i = startIndex;
        var nb = this._tabTabs.length;
        while (i < nb && this._tabTabs[i] && !this._tabTabs[i].active())
        {
            i++;
        }
        return (i == nb ? -1 : i);
    },

    _activePreviousTab : function(startIndex)
    {
        var i = startIndex;
        while (i >= 0 && this._tabTabs[i] && !this._tabTabs[i].active())
        {
            i--;
        }
        return (i < 0 ? -1 : i);
    },

    /**
     * Constructeur de la classe
     *
     */
    _init : function(width, height)
    {
        var mySelf = this;

        this._el = window.document.getElementById(this.name);
        if (!this._el)
        {
            return;
        }

        // N'affiche pas le contenu pendant la transformation
        this._el.style.display = 'none';

        this._el.className = 'locPlgTabBoxesDiv';
        this._el.setAttribute('tabindex', '0');
        this._el.onkeydown = function(e)
        {
            e = e || window.event;
            var targetElt = Location.commonObj.getEventSrcEl(e);
            if (targetElt.nodeName.toLowerCase() == 'div')
            {
                var keyCode = Location.commonObj.getEventKeyCode(e);
                if (keyCode == 9)
                {
                    if (e.shiftKey)
                    {
                        mySelf.previousTab();
                    }
                    else
                    {
                        mySelf.nextTab();
                    }

                    Location.commonObj.cancelEvent(e);
                }
            }
        }

        this._tabsContainerEl = window.document.createElement('DIV');
        this._tabsContainerEl.className = 'tabsContainer';

        // Recherche des onglets
        var index = 0;
        var titleEl, contentEl;
        var tabObj;

        var i, nb = this._el.childNodes.length;
        for (i = 0; i < nb; i++)
        {
            contentEl = this._el.childNodes[i];
            if (contentEl.nodeName.toLowerCase() == 'div')
            {
                contentEl.className = 'content';

                // Onglet associé
                titleEl = window.document.createElement('A');
                titleEl.className = 'tab';
                titleEl.href = '#';

                // Titre de l'onglet
                var spanEl = window.document.createElement('SPAN');
                spanEl.innerHTML = (contentEl.getAttribute('title') ? contentEl.getAttribute('title') : 'Onglet ' + (index + 1));
                titleEl.appendChild(spanEl);

                // Badge
                var badgeEl = window.document.createElement('SPAN');
                badgeEl.className = 'badge';
                titleEl.appendChild(badgeEl);

                // Bouton de fermeture
                var closeEl = window.document.createElement('A');
                closeEl.href = '#';
                titleEl.appendChild(closeEl);

                this._tabsContainerEl.appendChild(titleEl);
                contentEl.removeAttribute('title');

                // Création de l'onglet
                tabObj = new Location.TabBoxTab(this, index, titleEl, contentEl);
                if (index == 0)
                {
                    tabObj.active();
                }

                this._tabTabs[index++] = tabObj;
            }
        }

        this._el.insertBefore(this._tabsContainerEl, this._el.firstChild);

        // Définition de la hauteur et de la largeur
        this.setHeight(height);
        this.setWidth(width);

        // Affiche le tabBox
        this._el.style.display = '';
    }
}

/* Classe Location.TabBoxTab */
Location.TabBoxTab = function(tabBoxObj, index, titleEl, contentEl)
{
    this._tabBoxObj = tabBoxObj;
    this._titleEl = titleEl;
    this._contentEl = contentEl;

    this._index = index;
    this._isClosable = false;
    this._isActive = false;
    this._isDisabled = false;

    this.onclose  = null;
    this.onactive = null;

    // Initialisation
    this._init();
}

Location.TabBoxTab.prototype = {

    /**
     * Désactiver l'onglet
     *
     * @return bool
     */
    disable : function()
    {
        if (this._isDisabled)
        {
            return false;
        }
        this._titleEl.className = 'tab disabled';
        this._contentEl.className = 'content';
        this._isDisabled = true;
        if (this.isActive())
        {
            this._tabBoxObj.nextTab();
        }
        return true;
    },

    /**
     * Activer l'onglet
     *
     * @return bool
     */
    enable : function()
    {
        if (!this._isDisabled)
        {
            return false;
        }
        var isActive = this.isActive();
        this._titleEl.className = 'tab' + (isActive ? ' active' : '');
        this._contentEl.className = 'content' + (isActive ? ' active' : '');
        this._isDisabled  = false;
        return true;
    },

    /**
     * Fermer l'onglet
     *
     */
    close : function()
    {
        this._tabBoxObj.closeTab(this.getIndex());
    },

    /**
     * Récupérer l'index de l'onglet
     *
     * @return int Index
     */
    getIndex : function()
    {
        return this._index;
    },

    /**
     * Récupérer l'élément du contenu de l'onglet
     *
     * @return Object
     */
    getContentElement : function()
    {
        return this._contentEl;
    },

    /**
     * Récupérer le titre de l'onglet
     *
     * @return string
     */
    getTitle : function()
    {
        return this._titleEl.firstChild.innerHTML;
    },

    /**
     * Récupérer l'élément du titre de l'onglet
     *
     * @return Object
     */
    getTitleElement : function()
    {
        return this._titleEl.firstChild;
    },

    /**
     * Récupérer le contenu du badge
     *
     * @return string
     */
    getBadge : function()
    {
        return this.getBadgeElement().innerHTML;
    },

    getBadgeElement: function()
    {
        return this.getTitleElement().nextSibling;
    },

    /**
     * Indique si l'onglet est actif ou non
     *
     * @return bool
     */
    isActive : function()
    {
        return this._isActive;
    },

    /**
     * Indique si l'onglet est fermable ou non
     *
     * @return bool
     */
    isClosable : function()
    {
        return this._isClosable;
    },

    /**
     * Indique si l'onglet est désactivé
     *
     * @return bool
     */
    isDisabled : function()
    {
        return this._isDisabled;
    },

    /**
     * Définir l'onglet actif
     *
     * @return bool
     */
    active : function()
    {
        if (this.isDisabled())
        {
            return false;
        }
        if (this.isActive())
        {
            return true;
        }

        var oldActiveTabIndex = this._tabBoxObj.getActiveTabIndex();

        // Fonction utilisateur au changement de l'onglet actif
        if (oldActiveTabIndex != this.getIndex())
        {
            // Exécution de la fonction utilisateur
            if (this.onactive)
            {
                var result = this.onactive();
                if (typeof result != 'undefined' && !result)
                {
                    // Activation de l'onglet bloquée par l'utilisateur
                    return false;
                }
            }
            // Exécution de la fonction utilisateur
            if (this._tabBoxObj.ontabchange)
            {
                var result = this._tabBoxObj.ontabchange(this.getIndex(), oldActiveTabIndex);
                if (typeof result != 'undefined' && !result)
                {
                    // Activation de l'onglet bloquée par l'utilisateur
                    return false;
                }
            }
        }

        var tabObj;
        for (var i = 0; i < this._tabBoxObj._tabTabs.length; i++)
        {
            tabObj = this._tabBoxObj._tabTabs[i];
            tabObj._titleEl.className = 'tab' + (tabObj.isDisabled() ? ' disabled' : '');
            tabObj._contentEl.className = 'content';
            tabObj._isActive = false;
        }

        this._titleEl.className = 'tab active';
        this._contentEl.className = 'content active';
        this._isActive = true;
        // Exécution de la fonction utilisateur
        if (this._tabBoxObj.onaftertabchange)
        {
            this._tabBoxObj.onaftertabchange(this.getIndex(), oldActiveTabIndex);
        }
        return true;
    },

    /**
     * Définir l'onglet fermable
     *
     * @param bool isClosable
     */
    setClosable : function(isClosable)
    {
        this._titleEl.lastChild.style.display = (isClosable ? 'block' : 'none');
        this._isClosable = (isClosable ? true : false);
    },

    /**
     * Définir le contenu de l'onglet
     *
     * @param string content Contenu
     */
    setContent : function(content)
    {
        this._contentEl.innerHTML = content;
    },

    /**
     * Désactiver/Activer l'onglet
     *
     * @param bool isDisabled
     * @return bool
     */
    setDisabled : function(isDisabled)
    {
        if (isDisabled)
        {
            return this.disable();
        }
        else
        {
            return this.enable();
        }
    },

    /**
     * Définir le titre de l'onglet
     *
     * @param string title Titre
     */
    setTitle : function(title)
    {
        this._titleEl.firstChild.innerHTML = title;
    },

    /**
     * Définir le badge de l'onglet
     */
    setBadge: function(value)
    {
        var badgeElement = this.getBadgeElement();
        if (value !== null)
        {
            Location.commonObj.addEltClass(badgeElement, 'active');
        }
        else
        {
            Location.commonObj.removeEltClass(badgeElement, 'active');
        }
        badgeElement.innerHTML = value;
    },

    /**
     * Constructeur de la classe
     *
     */
    _init : function()
    {
        var mySelf = this;

        this._titleEl.onclick = function()
        {
            mySelf.active();
            return true;
        };
        this._titleEl.lastChild.onclick = function(e){ mySelf.close(); Location.commonObj.stopEventBubble(e); return true; };
    }
}

/**
 * Objet Location.tabBoxesManager
 *
 */
Location.tabBoxesManager = {

    _tabObj : [],

    /**
     * Créer un groupe d'onglets
     *
     * @param string name   Nom
     * @param int    height Hauteur
     * @return Location.TabBox Instance du groupe d'onglets créé
     */
    createTabBox : function(name, width, height)
    {
        if (this._tabObj[name])
        {
            return this._tabObj[name];
        }
        if (typeof height == 'undefined')
        {
            height = '';
        }
        if (typeof width == 'undefined')
        {
            width = '';
        }

        this._tabObj[name] = new Location.TabBox(this, name, width, height);
        return this._tabObj[name];
    },

    /**
     * Récupérer l'instance d'un groupe d'onglets
     *
     * @param string name Nom du groupe d'onglets
     * @return Location.TabBox
     */
    getTabBox : function(name)
    {
        if (!this._tabObj[name])
        {
            return null;
        }
        return this._tabObj[name];
    }
}
