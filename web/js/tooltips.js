if (!window.Location)
{
    var Location = new Object();
}

if (!Location.commonObj) alert('Erreur Location');

/* Objet Location.toolTipsObj */
Location.toolTipsObj = {

    _el            : null,
    _mousePos      : {x:0, y:0},
    _tabOptions    : {},
    _tabDefOptions : {offsetX:12, offsetY:4, corner:'tl',
                      moveX:true, moveY:true, isHidedIfEmpty:true,
                      zIndex: 900000000, opacity:0.93, width:'auto', height:'auto',
                      maxWidth:'none', maxHeight:'none',
                      overflow: 'hidden',
                      borderWidth: '1px', borderStyle: 'solid', borderColor: '#005F96',
                      backgroundColor: '#F2F8FB', padding: '2px', margin: '0px',
                      fontWeight: 'normal', fontStyle: 'normal', fontSize: '8pt', fontFamily: '',
                      textAlign: 'left', textDecoration: 'none', className: ''},

    _create : function()
    {
        if (!this._el)
        {
            var isDomReady = true;
            if (Location.clientInfoObj.isMSIE)
            {
                isDomReady = (window.document.readyState == 'loaded' || window.document.readyState == 'complete');
            }
            if (isDomReady)
            {
                // Création de la popup
                this._el = window.document.createElement('DIV');
                this._el.id = 'frmwrkToolTip';
                this._el.style.position = 'absolute';
                this._el.style.left = '0px';
                this._el.style.top = '0px';
                this._el.style.display = 'none';
                window.document.body.appendChild(this._el);

                var myself = this;
                Location.commonObj.addEltEvent(window.document.documentElement, 'mousemove', function(e){ myself._move(e, false); });
                Location.commonObj.addEltEvent(window, 'unload', function(e){ myself.hide(); });
            }
        }
    },

    _move : function(e, isDisp)
    {
        e = e || window.event;
        if (e)
        {
            this._mousePos.x = e.clientX;
            this._mousePos.y = e.clientY;
        }

        if (this._el)
        {
            var posX, posY;
            switch (this._tabOptions.corner)
            {
                case 'tr':
                case 'rt':
                    posX = this._mousePos.x - this._tabOptions.offsetX - this._el.offsetWidth + 'px';
                    posY = this._mousePos.y + this._tabOptions.offsetY + 'px';
                    break;
                case 'br':
                case 'rb':
                    posX = this._mousePos.x - this._tabOptions.offsetX - this._el.offsetWidth + 'px';
                    posY = this._mousePos.y - this._tabOptions.offsetY - this._el.offsetHeight + 'px';
                    break;
                case 'bl':
                case 'lb':
                    posX = this._mousePos.x + this._tabOptions.offsetX + 'px';
                    posY = this._mousePos.y - this._tabOptions.offsetY - this._el.offsetHeight + 'px';
                    break;
                default:
                    posX = this._mousePos.x + this._tabOptions.offsetX + 'px';
                    posY = this._mousePos.y + this._tabOptions.offsetY + 'px';
            }

            if (isDisp || this._tabOptions.moveX)
            {
                this._el.style.left = posX;
            }
            if (isDisp || this._tabOptions.moveY)
            {
                this._el.style.top  = posY;
            }
        }
    },

    show : function(text, tabOptions)
    {
        this._create();

        if (!this._el)
        {
            return;
        }

        if (typeof tabOptions != 'object')
        {
            tabOptions = {};
        }

        // Options par défaut et personnalisées
        this._tabOptions = {};
        for (var prop in this._tabDefOptions)
        {
            if (typeof tabOptions[prop] != 'undefined')
            {
                this._tabOptions[prop] = tabOptions[prop];
            }
            else
            {
                this._tabOptions[prop] = this._tabDefOptions[prop];
            }
        }

        // Vérifications des options
        if (this._tabOptions.offsetX == 0)
        {
            this._tabOptions.offsetX = this._tabDefOptions.offsetX;
        }
        if (this._tabOptions.offsetY == 0)
        {
            this._tabOptions.offsetY = this._tabDefOptions.offsetY;
        }

        // Styles
        this._el.style.zIndex = this._tabOptions.zIndex;
        if (LOC_ClientInfo.isMSIE)
        {
            this._el.style.filter = (this._tabOptions.opacity != 1 ? 'alpha(opacity=' + this._tabOptions.opacity * 100 + ')' : '');
        }
        else
        {
            this._el.style.opacity = this._tabOptions.opacity;
        }
        this._el.className   = this._tabOptions.className;
        if (this._tabOptions.className == '')
        {
            this._el.style.width       = this._tabOptions.width;
            this._el.style.height      = this._tabOptions.height;
            this._el.style.maxWidth    = this._tabOptions.maxWidth;
            this._el.style.maxHeight   = this._tabOptions.maxHeight;
            this._el.style.overflow    = this._tabOptions.overflow;
            this._el.style.borderColor = this._tabOptions.borderColor;
            this._el.style.borderWidth = this._tabOptions.borderWidth;
            this._el.style.borderStyle = this._tabOptions.borderStyle;
            this._el.style.backgroundColor = this._tabOptions.backgroundColor;
            this._el.style.padding     = this._tabOptions.padding;
            this._el.style.margin      = this._tabOptions.margin;
            this._el.style.fontWeight  = this._tabOptions.fontWeight;
            this._el.style.fontStyle   = this._tabOptions.fontStyle;
            this._el.style.fontSize    = this._tabOptions.fontSize;
            this._el.style.fontFamily  = this._tabOptions.fontFamily;
            this._el.style.textAlign   = this._tabOptions.textAlign;
            this._el.style.textDecoration = this._tabOptions.textDecoration;
        }

        if (this._el.innerHTML != text)
        {
            this._el.innerHTML = text;
        }
        this._move(null, true);
        if (!this._tabOptions.isHidedIfEmpty || this._el.innerHTML != '')
        {
            this._el.style.display = 'block';
        }
        else
        {
            this._el.style.display = 'none';
        }
    },

    hide : function()
    {
        if (this.isShown())
        {
            this._el.style.display = 'none';
            this._el.innerHTML = '';
        }
    },

    /**
     * Est-ce que le toolip est affiché
     *
     * @return bool
     */
    isShown : function()
    {
        return (this._el && this._el.style.display != 'none' ? true : false);
    }
}
