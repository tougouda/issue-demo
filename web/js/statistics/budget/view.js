var budget = {

    collipsableTransitionDelay: 400,

    /**
     * Fonction de soumission du formulaire
     */
    submitForm : function()
    {
        //Image de chargement
        LOC_View.displayLoading(true);

        //Soumission du formulaire
        $('[name=mainForm]').submit();

        return false;
    },


    /**
     * Initialisation
     */
    init : function()
    {
        // Calendrier
        var selectedIndex = $('#month').prop("selectedIndex");

        $('#previousMonth').addClass(selectedIndex == $('#month option').length - 1 ? 'disabled' : 'active');
        $('#nextMonth').addClass(selectedIndex == 0 ? 'disabled' : 'active');

        // Changement de mois
        $('#month').change(function(event) {
            budget.submitForm();
        });

        // Mois précédent
        $('#previousMonth').click(function() {
            var selectedIndex = $('#month').prop("selectedIndex");
            if (selectedIndex < $('#month option').length - 1)
            {
                $('#month').prop("selectedIndex", selectedIndex + 1);
                $('#month').change();
            }
        });

        // Mois suivant
        $('#nextMonth').click(function() {
            var selectedIndex = $('#month').prop("selectedIndex");
            if (selectedIndex > 0)
            {
                $('#month').prop("selectedIndex", selectedIndex - 1);
                $('#month').change();
            }
        });


        // Boutons d'extension/repli des lignes
        $('.collipsable').each(function() {
            // Statut de la région
            $(this).attr('status', ($(this).hasClass('collapse') ? 'expanded' : 'collapsed'));

            // Repli/extension de la région
            $(this).bind('toggle', function()
            {
                var nextSeparator = $(this).closest('tr').next('tr.separator');

                nextSeparator.toggle(budget.collipsableTransitionDelay);
                nextSeparator.nextUntil('tr.separator').each(function() {
                    $(this).toggle(budget.collipsableTransitionDelay);
                });

                $(this).toggleClass('collapse');
                $(this).toggleClass('expand');

                $(this).attr('status', ($(this).attr('status') == 'collapsed' ? 'expanded' : 'collapsed'));

                return false;
            });

            // Extension de la région
            $(this).bind('expand', function()
            {
                if ($(this).attr('status') == 'collapsed')
                {
                    $(this).trigger('toggle');
                }
            });

            // Repli de la région
            $(this).bind('collapse', function()
            {
                if ($(this).attr('status') == 'expanded')
                {
                    $(this).trigger('toggle');
                }
            });
        });

        // Action au clic sur la région
        $('.collipsable').click(function(event) {
            // Ctrl+clic étend/replie toutes les région
            if (event.ctrlKey)
            {
                var isExpanded = true;
                $('.collipsable').each(function() {
                    if ($(this).attr('status') == 'collapsed')
                    {
                        isExpanded = false;
                    }
                });
                $('.collipsable').trigger(isExpanded ? 'collapse' : 'expand');
            }
            // Ctrl+clic étend/replie la région cliquée
            else
            {
                $(this).trigger('toggle');
            }
        });

        // Empêcher la soumission du formulaire lorsqu'on plie/déplie une région
        $('.collipsable > button').click(function(event) {
            event.preventDefault();
        });

        // Info-bulle pour le Ctrl+clic
        var nbCollipsable = $('.collipsable').length;
        if (nbCollipsable > 1)
        {
            $('.collipsable').attr('title', LOC_View.getTranslation('CtrlClick'));
        }
    },

};
